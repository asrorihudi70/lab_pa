// Data Source ExtJS # --------------

/**
*	Nama File 		: TRApotekReturRWJ.js
*	Menu 			: APOTEK
*	Model id 		: 
*	Keterangan 		: Retur RWJ dan IGD
*	Di buat tanggal : 04 Juni 2015
*	Oleh 			: SDY_RI
*/

// Deklarasi Variabel pada Apotek Perencanaan # --------------
var KdFormReturRWJ=9; // kd_form RETUR RWJ
var dataSource_viApotekReturRWJ;
var selectCount_viApotekReturRWJ=50;
var dsprinter_retur_rwi;
var NamaForm_viApotekReturRWJ="Retur Rawat Jalan / Gawat Darurat";
var selectCountStatusPostingApotekReturRWJ='Semua';
var mod_name_viApotekReturRWJ="viApotekReturRWJ";
var cbopasienorder_printer_retur_rwi;
var now_viApotekReturRWJ= new Date();
var addNew_viApotekReturRWJ;
var rowSelected_viApotekReturRWJ;
var setLookUps_viApotekReturRWJ;
var mNoKunjungan_viApotekReturRWJ='';
var selectSetUnit;
var selectSetDokter;
var selectSetPilihankelompokPasien;
var tanggal = now_viApotekReturRWJ.format("d/M/Y");
var cellSelecteddeskripsiReturRWJ;
var tmpkriteria;
var gridDTLTRHistoryApotekReturRWJ;
var kd_pasien_order;
var kd_unit_order;
var tgl_masuk_order;
var urut_masuk_order;
var UnitFarAktif_ReturRWJ;
var kd_pay_retur;

var CurrentHistoryReturRWJ =
{
    data: Object,
    details: Array,
    row: 0
};

var CurrentData_viApotekReturRWJ =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viApotekReturRWJ(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

var ReturRWJ={};
ReturRWJ.form={};
ReturRWJ.func={};
ReturRWJ.vars={};
ReturRWJ.func.parent=ReturRWJ;
ReturRWJ.form.ArrayStore={};
ReturRWJ.form.ComboBox={};
ReturRWJ.form.DataStore={};
ReturRWJ.form.Record={};
ReturRWJ.form.Form={};
ReturRWJ.form.Grid={};
ReturRWJ.form.Panel={};
ReturRWJ.form.TextField={};
ReturRWJ.form.Button={};

ReturRWJ.form.ArrayStore.noResep=new Ext.data.ArrayStore({
		id: 0,
		fields: [	
					'no_resep', 'no_out', 'tgl_out','kd_unit','nama_unit', 
					'dokter', 'nama_dokter', 'kd_pasienapt', 'nmpasien', 
					'no_out', 'tgl_out','kd_customer','jenis_pasien','kd_unit_far',
					'apt_kd_kasir','apt_no_transaksi'
				],
		data: []
	});

// End Deklarasi Variabel pada Apotek Perencanaan # --------------

// Start Project Apotek Perencanaan # --------------

// --------------------------------------- # Start Function # ---------------------------------------
// ## Silahkan tempatkan semua fungsi eksekusi disini

// --------------------------------------- # End Function # ---------------------------------------

// --------------------------------------- # Start Form # ---------------------------------------

/**
*	Function : dataGrid_viApotekReturRWJ
*	
*	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
*/

function dataGrid_viApotekReturRWJ(mod_id_viApotekReturRWJ)
{	
    // Field kiriman dari Project Net.
    var FieldMaster_viApotekReturRWJ = 
	[
		'STATUS_POSTING','NO_FAKTUR','NO_BUKTI','NO_OUT','TGL_OUT', 'KD_PASIENAPT', 'NMPASIEN', 'DOKTER', 
		'NAMA_DOKTER', 'KD_UNIT', 'NAMA_UNIT', 'APT_NO_TRANSAKSI',
		'APT_KD_KASIR', 'KD_CUSTOMER','ADMRACIK','JUMLAH','JML_TERIMA_UANG','SISA','ADMPRHS',
		'JASA','ADMRESEP','JASA', 'ADMPRHS','ADMRESEP','CUSTOMER','JENIS_PASIEN', 'KD_UNIT_FAR'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viApotekReturRWJ = new WebApp.DataStore
	({
        fields: FieldMaster_viApotekReturRWJ
    });
    refreshReturApotekRWJ();
	getUnitFar_ReturRWJ();
    // Grid Apotek Perencanaan # --------------
	var GridDataView_viApotekReturRWJ = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viApotekReturRWJ,
			autoScroll: true,
			columnLines: true,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viApotekReturRWJ = undefined;
							rowSelected_viApotekReturRWJ = dataSource_viApotekReturRWJ.getAt(row);
							CurrentData_viApotekReturRWJ
							CurrentData_viApotekReturRWJ.row = row;
							CurrentData_viApotekReturRWJ.data = rowSelected_viApotekReturRWJ.data;
							if (rowSelected_viApotekReturRWJ.data.STATUS_POSTING==='0')
							{
								Ext.getCmp('btnHapusTrx_viApotekReturRWJ').enable();
							}
							else
							{
								Ext.getCmp('btnHapusTrx_viApotekReturRWJ').disable();
							}
							
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					Ext.Ajax.request(
					{
						   
						url: baseURL + "index.php/apotek/currentShiftAPT/getcurrentshift",
						 params: {
							
							command: '0'
						
						},
						failure: function(o)
						{
							 var cst = Ext.decode(o.responseText);
							
						},	    
						success: function(o) {
							var cst = Ext.decode(o.responseText);
				
							tampungshiftsekarang=cst.shift
						}	
					
					});
					rowSelected_viApotekReturRWJ = dataSource_viApotekReturRWJ.getAt(ridx);
					if (rowSelected_viApotekReturRWJ != undefined)
					{
						setLookUp_viApotekReturRWJ(rowSelected_viApotekReturRWJ.data);
					}
					else
					{
						setLookUp_viApotekReturRWJ();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Apotek perencanaan
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header		: 'Status Posting',
						width		: 20,
						sortable	: false,
						hideable	: true,
						hidden		: false,
						menuDisabled: true,
						dataIndex	: 'STATUS_POSTING',
						id			: 'colStatusPosting_viApotekReturRWJ',
						renderer	: function(value, metaData, record, rowIndex, colIndex, store){
							 switch (value){
								 case '1':
									 metaData.css = 'StatusHijau'; 
									 break;
								 case '0':
									 metaData.css = 'StatusMerah';
									 break;
							 }
							 return '';
						}
					},
					{
						id: 'colNoMedrec_viApotekReturRWJ',
						header: 'No. Out',
						dataIndex: 'NO_OUT',
						sortable: true,
						width: 25
						
					},
					//-------------- ## --------------
					{
						id: 'colTgl_viApotekReturRWJ',
						header:'Tgl Retur',
						dataIndex: 'TGL_OUT',						
						width: 30,
						sortable: true,
						hideable:false,
                        menuDisabled:true,
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.TGL_OUT);
						}
					},
					//-------------- ## --------------
					{
						id: 'colNoMedrec_viApotekReturRWJ',
						header: 'No. Retur',
						dataIndex: 'NO_FAKTUR',
						sortable: true,
						width: 35
						
					},
					//-------------- ## --------------
					{
						id: 'colNoMedrec_viApotekReturRWJ',
						header: 'No Medrec',
						dataIndex: 'KD_PASIENAPT',
						sortable: true,
						hideable:false,
                        menuDisabled:true,
						width: 30
					},
					//-------------- ## --------------
					{
						id: 'colNamaPasien_viApotekReturRWJ',
						header: 'Nama Pasien',
						dataIndex: 'NMPASIEN',
						sortable: true,
						width: 50
					},
					//-------------- ## --------------
					{
						id: 'colPoliklinik_viApotekReturRWJ',
						header: 'Poliklinik',
						dataIndex: 'NAMA_UNIT',
						sortable: true,
						width: 40
					}
					//-------------- ## --------------
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viApotekReturRWJ',
				items: 
				[
				{
						xtype: 'button',
						text: 'Add Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Tambah Data',
						id: 'btnAddData_viApotekReturRWJ',
						handler: function(sm, row, rec)
						{
							Ext.Ajax.request(
							{
								   
								url: baseURL + "index.php/apotek/currentShiftAPT/getcurrentshift",
								 params: {
									
									command: '0'
								
								},
								failure: function(o)
								{
									 var cst = Ext.decode(o.responseText);
									
								},	    
								success: function(o) {
									var cst = Ext.decode(o.responseText);
						
									tampungshiftsekarang=cst.shift
								}	
							
							});
							Ext.Ajax.request
							(
								{
									url: baseURL + "index.php/apotek/functionApotekReturRWJ/cekBulan",
									params: {a:"no_out"},
									success: function(o) 
									{
										var cst = Ext.decode(o.responseText);
										if (cst.success === true) 
										{
											setLookUp_viApotekReturRWJ();
										} else{
											if(cst.pesan=='Periode Bulan ini sudah Ditutup'){
												ShowPesanErrorReturRWJ('Periode bulan ini sudah diTutup, tidak dapat melakukan transaksi','Error');
											} else{
												ShowPesanErrorReturRWJ('Periode Bulan Lalu Harap Ditutup, tidak dapat melakukan transaksi','Error');
											}
											
										}
									}
								}
							);
						}
					},
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viApotekReturRWJ',
						handler: function(sm, row, rec)
						{
							Ext.Ajax.request(
							{
								   
								url: baseURL + "index.php/apotek/currentShiftAPT/getcurrentshift",
								 params: {
									
									command: '0'
								
								},
								failure: function(o)
								{
									 var cst = Ext.decode(o.responseText);
									
								},	    
								success: function(o) {
									var cst = Ext.decode(o.responseText);
						
									tampungshiftsekarang=cst.shift
								}	
							
							});
							if (rowSelected_viApotekReturRWJ != undefined)
							{
								setLookUp_viApotekReturRWJ(rowSelected_viApotekReturRWJ.data)
							}
							else
							{								
								ShowPesanWarningReturRWJ('Data belum dipilih, pilih data yang akan di edit','Warning');
							}
						}
					},
					{
						xtype: 'button',
						text: 'Hapus Transaksi',
						iconCls: 'remove',
						tooltip: 'Hapus Data',
						disabled:true,
						id: 'btnHapusTrx_viApotekReturRWJ',
						handler: function(sm, row, rec)
						{
							var datanya=rowSelected_viApotekReturRWJ.data;
							if (datanya===undefined){
								ShowPesanWarningReturRWJ('Belum ada data yang dipilih','Retur RWJ');
							}
							else
							{
								 Ext.Msg.show({
									title: 'Hapus Transaksi',
									msg: 'Anda yakin akan menghapus data transaksi ini ?',
									buttons: Ext.MessageBox.YESNO,
									fn: function (btn) {
										if (btn == 'yes')
										{
											
											console.log(datanya);
											 var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan Pembatalan Transaksi:', function (btn, combo) {
														if (btn == 'ok')
														{
															var variablebatalhistori_rwj = combo;
															if (variablebatalhistori_rwj != '')
															{
																 Ext.Ajax.request({
										   
																		url: baseURL + "index.php/apotek/functionAPOTEK/hapusTrxResep",
																		 params: {
																			
																			noout: datanya.NO_OUT,
																			tglout: datanya.TGL_OUT,
																			jenis:'RETUR',
																			apaini:"returrwj",
																			alasan: variablebatalhistori_rwj
																		
																		},
																		failure: function(o)
																		{
																			 var cst = Ext.decode(o.responseText);
																			ShowPesanErrorReturRWJ('Data transaksi tidak dapat dihapus','Retur RWJ');
																		},	    
																		success: function(o) {
																			var cst = Ext.decode(o.responseText);
																			if (cst.success===true)
																			{
																				tmpkriteria = getCriteriaCariApotekReturRWJ();
																				refreshReturApotekRWJ(tmpkriteria);
																				ShowPesanInfoReturRWJ('Data transaksi Berhasil dihapus','Retur RWJ');
																				Ext.getCmp('btnHapusTrx_viApotekReturRWJ').disable();
																			}
																			else
																			{
																				ShowPesanErrorReturRWJ('Gagal hapus transaksi. '+ cst.pesan,'Retur RWJ');
																			}
																			
																		}
																
																})
															} else
															{
																ShowPesanWarningReturRWJ('Silahkan isi alasan terlebih dahaulu', 'Keterangan');

															}
														}

													}); 
											/*  */
										}
									},
									icon: Ext.MessageBox.QUESTION
								});
							}
						}
					},
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viApotekReturRWJ, selectCount_viApotekReturRWJ, dataSource_viApotekReturRWJ),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )
	
	var pencarianApotekReturRWJ = new Ext.FormPanel({
        labelAlign: 'top',
        frame:true,
        title: '',
        bodyStyle:'padding:5px 5px 0',
        //width: 600,
        items: [
		{
			layout: 'column',
			border: false,
			items:
			[
				{
					columnWidth:.98,
					layout: 'absolute',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					width: 500,
					height: 90,
					anchor: '100% 100%',
					items:
					[
						{
							x: 10,
							y: 0,
							xtype: 'label',
							text: 'No. Retur'
						},
						{
							x: 120,
							y: 0,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 0,
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_RoNumber_viApotekReturRWJ',
							name: 'TxtFilterGridDataView_RoNumber_viApotekReturRWJ',
							emptyText: 'No. Retur',
							width: 130,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariApotekReturRWJ();
										refreshReturApotekRWJ(tmpkriteria);
									} 						
								}
							}
						},
						{
							x: 10,
							y: 30,
							xtype: 'label',
							text: 'Kode/Nama Pasien'
						},
						{
							x: 120,
							y: 30,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 30,
							xtype: 'textfield',
							id: 'txtKdNamaPasienReturRWJ',
							name: 'txtKdNamaPasienReturRWJ',
							emptyText: 'Kode/Nama Pasien',
							width: 130,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariApotekReturRWJ();
										refreshReturApotekRWJ(tmpkriteria);
									} 						
								}
							}
						},
						{
							x: 10,
							y: 60,
							xtype: 'label',
							text: 'Poliklinik'
						},
						{
							x: 120,
							y: 60,
							xtype: 'label',
							text: ':'
						},
						ComboUnitApotekReturRWJ(),	
						
						//-------------- ## --------------
						{
							x: 310,
							y: 0,
							xtype: 'label',
							text: 'Tanggal Resep'
						},
						{
							x: 400,
							y: 0,
							xtype: 'label',
							text: ':'
						},
						{
							x: 410,
							y: 0,
							xtype: 'datefield',
							id: 'dfTglAwalApotekReturRWJ',
							format: 'd/M/Y',
							width: 120,
							tabIndex:3,
							value:now_viApotekReturRWJ,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariApotekReturRWJ();
										refreshReturApotekRWJ(tmpkriteria);
									} 						
								}
							}
						},
						{
							x: 540,
							y: 0,
							xtype: 'label',
							text: 's/d'
						},
						{
							x: 568,
							y: 0,
							xtype: 'datefield',
							id: 'dfTglAkhirApotekReturRWJ',
							format: 'd/M/Y',
							width: 120,
							tabIndex:4,
							value:now_viApotekReturRWJ,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariApotekReturRWJ();
										refreshReturApotekRWJ(tmpkriteria);
									} 						
								}
							}
						},
						{
							x: 310,
							y: 30,
							xtype: 'label',
							text: 'Posting'
						},
						{
							x: 400,
							y: 30,
							xtype: 'label',
							text: ':'
						},
						mComboStatusPostingApotekReturRWJ(),
						//----------------------------------------
						{
							x: 568,
							y: 60,
							xtype: 'button',
							text: 'Cari',
							iconCls: 'refresh',
							tooltip: 'Cari',
							style:{paddingLeft:'30px'},
							//rowspan: 3,
							width:150,
							id: 'BtnFilterGridDataView_viApotekReturRWJ',
							handler: function() 
							{					
								tmpkriteria = getCriteriaCariApotekReturRWJ();
								refreshReturApotekRWJ(tmpkriteria);
							}                        
						}
					]
				}
			]
		}
		]	
						//-------------- ## --------------
	})

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viApotekReturRWJ = new Ext.Panel
    (
		{
			title: NamaForm_viApotekReturRWJ,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viApotekReturRWJ,
			region: 'center',
			layout: 'form', 
			closable: true,        
			border: false,  
			margins: '0 5 5 0',
			items: [ pencarianApotekReturRWJ,
					GridDataView_viApotekReturRWJ],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viApotekReturRWJ,
		            columns: 21,
		            defaults: {
					scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viApotekReturRWJ;
    //-------------- # End form filter # --------------
}
// End Function dataGrid_viApotekReturRWJ # --------------

/**
*	Function : setLookUp_viApotekReturRWJ
*	
*	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
*/

function setLookUp_viApotekReturRWJ(rowdata)
{
    var lebar = 985;
    setLookUps_viApotekReturRWJ = new Ext.Window
    ({
        id: 'SetLookUps_viApotekReturRWJ',
		name: 'SetLookUps_viApotekReturRWJ',
        title: NamaForm_viApotekReturRWJ, 
        closeAction: 'destroy',        
        width: 900,
        height: 555,
        resizable:false,
		autoScroll: false,
        border: true,
        constrain : true,    
        iconCls: 'resep',
        modal: true,		
        items: getFormItemEntry_viApotekReturRWJ(lebar,rowdata), //1
        listeners:
        {
            activate: function()
            {
				
            },
            afterShow: function()
            {
                this.activate();
            },
            deactivate: function()
            {
                rowSelected_viApotekReturRWJ=undefined;
                //datarefresh_viApotekReturRWJ();
				mNoKunjungan_viApotekReturRWJ = '';
            }
        }
    }
    );

    
    
	setLookUps_viApotekReturRWJ.show();
	
    if (rowdata == undefined)
    {
        // dataaddnew_viApotekReturRWJ();
		// Ext.getCmp('btnDelete_viApotekReturRWJ').disable();	
    }
    else
    {
        datainit_viApotekReturRWJ(rowdata);
    }
	shortcut.set({
		code:'lookup',
		list:[
			{
				key:'ctrl+s',
				fn:function(){
					Ext.getCmp('btnSimpan_viApotekReturRWJ').el.dom.click();
				}
			},
			{
				key:'ctrl+d',
				fn:function(){
					Ext.getCmp('btnDelete_viApotekReturRWJ').el.dom.click();
				}
			},
			{
				key:'f4',
				fn:function(){
					Ext.getCmp('btnbayar_viApotekReturRWJ').el.dom.click();
				}
			},
			{
				key:'f6',
				fn:function(){
					Ext.getCmp('btnunposting_viApotekReturRWJ').el.dom.click();
				}
			},
			{
				key:'f12',
				fn:function(){
					Ext.getCmp('btnprintbill_retur_rwjReturRWJ').el.dom.click();
				}
			},
			{
				key:'esc',
				fn:function(){
					setLookUps_viApotekReturRWJ.close();
				}
			}
		]
	});
}
// End Function setLookUpGridDataView_viApotekReturRWJ # --------------

/**
*	Function : getFormItemEntry_viApotekReturRWJ
*	
*	Sebuah fungsi untuk menampilkan penataan menu dan isian form edit
*/

function getFormItemEntry_viApotekReturRWJ(lebar,rowdata)
{
    var pnlFormDataBasic_viApotekReturRWJ = new Ext.FormPanel
    (
		{
			title: '',
			region: 'north',
			layout: 'form',
			bodyStyle: 'padding:10px 10px 10px 10px',
			anchor: '100%',
			labelWidth: 1,
			autoWidth: true,
			width: lebar,
			border: false,
			//-------------- #items# --------------
			items:
			[
				getItemPanelInputBiodata_viApotekReturRWJ(lebar),
				//-------------- ## -------------- 				
				getItemGridTransaksi_viApotekReturRWJ(lebar),
				//-------------- ## --------------
				getItemGridHistoryBayar_viApotekReturRWJ(lebar),
				//-------------- ## --------------
				{
					xtype: 'compositefield',
					fieldLabel: ' ',
					labelSeparator: '',
					name: 'compChkPosted_viApotekReturRWJ',
					id: 'compChkPosted_viApotekReturRWJ',
					items: 
					[
						/* {
							xtype: 'checkbox',
							id: 'chkPostedApotekReturRWJ',
							name: 'chkPostedApotekReturRWJ',
							disabled: false,
							autoWidth: false,		
							style: { 'margin-top': '2px' },							
							boxLabel: 'Posted ',
							width: 70
						}, */

						ReturRWJ.form.Panel.a=new Ext.Panel ({
						    region: 'north',
						    border: false,
						    html: '<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>'
						}),
						{
							columnWidth	: .08,
							layout		: 'form',
							anchor		: '100% 8.0001%',
							style		: {'margin-top':'1px'},
							border		: false,
							html		: " Status Post"
						},
						
									
						{
							xtype: 'displayfield',				
							width: 50,								
							value: 'Total:',
							fieldLabel: 'Label',
							style:{'text-align':'right','margin-left':'629px'}
						},
						{
		                    xtype: 'textfield',
		                    id: 'txtDRGridJmlEditData_viApotekReturRWJ',
		                    name: 'txtDRGridJmlEditData_viApotekReturRWJ',
							style:{'text-align':'right','margin-left':'629px'},
		                    width: 80,
		                    value: 0,
		                    readOnly: true
		                }												
		            ]
		        },
				{
					xtype: 'compositefield',
					fieldLabel: ' ',
					labelSeparator: '',
					name: 'compChkUpdateHB_viApotekReturRWJ',
					id: 'compChkUpdateHB_viApotekReturRWJ',
					items: 
					[
						{
							xtype: 'displayfield',				
							width: 65,								
							value: 'Tanggal :',
							fieldLabel: 'Label',
							style:{'text-align':'left','margin-left':'0px'}
						},
						{
							xtype: 'displayfield',				
							width: 100,								
							value: tanggal,
							format:'d/M/Y',
							fieldLabel: 'Label',
							style:{'text-align':'left','margin-left':'0px'}
						},
						{
							xtype: 'displayfield',				
							width: 65,								
							value: 'Reduksi :',
							fieldLabel: 'Label',
							style:{'text-align':'right','margin-left':'350px'}							
						},						
						{
		                    xtype: 'numberfield',
		                    id: 'txttmpReduksiEditData_viApotekReturRWJ',
		                    name: 'txttmpReduksiEditData_viApotekReturRWJ',
							style:{'text-align':'right','margin-left':'350px'},
		                    width: 80,
		                    value: 0,
		                    readOnly: true,
							hidden:true,
		                },
						{
		                    xtype: 'textfield',
		                    id: 'txtReduksiEditData_viApotekReturRWJ',
		                    name: 'txtReduksiEditData_viApotekReturRWJ',
							style:{'text-align':'right','margin-left':'350px'},
		                    width: 80,
		                    value: 0,
		                    readOnly: true
		                },
						{
							xtype: 'displayfield',				
							width: 90,								
							value: 'Grand Total :',
							fieldLabel: 'Label',
							style:{'text-align':'right','margin-left':'350px','font-weight':'bold'}
							
						},						
						{
		                    xtype: 'textfield',
		                    id: 'txtTotalEditData_viApotekReturRWJ',
		                    name: 'txtTotalEditData_viApotekReturRWJ',
							style:{'text-align':'right','margin-left':'350px'},
		                    width: 80,
		                    value: 0,
		                    readOnly: true
		                }					
						
		            ]
		        }
                //-------------- ## --------------
			],
			//-------------- #End items# --------------
			fileUpload: true,
			// Tombol pada tollbar Edit Data Pasien
			tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						id: 'btnAdd_viApotekReturRWJ',
						//hidden:true,
						handler: function(){
							dataaddnew_viApotekReturRWJ();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						disabled:true,
						id: 'btnSimpan_viApotekReturRWJ',
						handler: function()
						{
							datasave_viApotekReturRWJ();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						id: 'btnSimpanExit_viApotekReturRWJ',
						disabled:true,
						hidden:true,
						handler: function()
						{
							datasave_viApotekReturRWJ();
							setLookUps_viApotekReturRWJ.close();
						}
					},
					//-------------- ## --------------
					{
						xtype:'tbseparator',
						hidden:true,
					},
					{
						xtype: 'button',
						text: 'Paid',
						id:'btnbayar_viApotekReturRWJ',
						iconCls: 'gantidok',
						disabled:true,
						handler: function(){
							setLookUp_bayarResepRWJ();
						}
					},
					{
						xtype:'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Unposting',
						id:'btnunposting_viApotekReturRWJ',
						iconCls: 'gantidok',
						disabled:true,
						handler:function()
						{
							Ext.Ajax.request
							(
								{
									url: baseURL + "index.php/apotek/functionApotekReturRWJ/cekBulan",
									params: {a:"no_out"},
									success: function(o) 
									{
										var cst = Ext.decode(o.responseText);
										if (cst.success === true) 
										{
											Ext.Msg.confirm('Warning', 'Apakah data ini akan diUnposting?', function(button){
												if (button == 'yes'){
													Ext.Ajax.request
													(
														{
															url: baseURL + "index.php/apotek/functionApotekReturRWJ/unpostingReturRWJ",
															params: getParamUnpostingReturRWJ(),
															failure: function(o)
															{
																ShowPesanErrorReturRWJ('Hubungi Admin', 'Error');
															},	
															success: function(o) 
															{
																var cst = Ext.decode(o.responseText);
																if (cst.success === true) 
																{
																	ShowPesanInfoReturRWJ('UnPosting Berhasil','Information');
																	Ext.getCmp('btnunposting_viApotekReturRWJ').disable();
																	Ext.getCmp('btnbayar_viApotekReturRWJ').disable();
																	Ext.getCmp('btnAdd_viApotekReturRWJ').enable();
																	Ext.getCmp('btnSimpan_viApotekReturRWJ').disable();
																	Ext.getCmp('btnSimpanExit_viApotekReturRWJ').disable();
																	Ext.getCmp('btnPrint_viReturRWJ').disable();
																	Ext.getCmp('btnHapusResepReturRWJ').enable();
																	Ext.getCmp('btnDeleteHistory_viApotekReturRWJ').enable();
																	Ext.getCmp('txtTmpStatusPostReturRWIL').setValue('0');
																	ReturRWJ.form.Panel.a.update('<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>');
																	refreshReturApotekRWJ();
																	ReturRWJ.form.Grid.a.setDisabled(false);
																	
																	//unposting_mrobat();
																}
																else 
																{
																	ShowPesanErrorReturRWJ('Gagal melakukan unPosting. ' + cst.pesan, 'Error');
																};
															}
														}
														
													)
												}
											});
										} else{
											if(cst.pesan=='Periode Bulan ini sudah Ditutup'){
												ShowPesanErrorReturRWJ('Periode bulan ini sudah diTutup, tidak dapat melakukan transaksi','Error');
											} else{
												ShowPesanErrorReturRWJ('Periode Bulan Lalu Harap Ditutup, tidak dapat melakukan transaksi','Error');
											}
											
										}
									}
								}
							);
							
						}  
					},
					{
						xtype:'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete Paid',
						id:'btnDeleteHistory_viApotekReturRWJ',
						iconCls: 'remove',
						disabled:true,
						handler:function()
						{
							if(dsTRDetailHistoryBayarList.getCount()>0){
								Ext.Msg.confirm('Warning', 'Apakah data pembayaran ini akan dihapus?', function(button){
									if (button == 'yes'){
										Ext.Ajax.request
										(
											{
												url: baseURL + "index.php/apotek/functionApotekReturRWJ/deleteHistoryReturRWJ",
												params: getParamDeleteHistoryReturRWJ(),
												failure: function(o)
												{
													ShowPesanErrorReturRWJ('Hubungi Admin', 'Error');
												},	
												success: function(o) 
												{
													var cst = Ext.decode(o.responseText);
													if (cst.success === true) 
													{
														ShowPesanInfoReturRWJ('Penghapusan berhasil','Information');
														ViewDetailPembayaranObat(Ext.getCmp('txtTmpNooutApotekReturRWJL').getValue(),Ext.getCmp('txtTmpTgloutApotekReturRWJL').getValue());
														gridDTLTRHistoryApotekReturRWJ.getView().refresh();
														Ext.getCmp('btnbayar_viApotekReturRWJ').enable();
													}
													else 
													{
														ShowPesanErrorReturRWJ('Gagal menghapus pembayaran', 'Error');
														ViewDetailPembayaranObat(Ext.getCmp('txtTmpNooutApotekReturRWJL').getValue(),Ext.getCmp('txtTmpNooutApotekReturRWJL').getValue());
														gridDTLTRHistoryApotekReturRWJ.getView().refresh();
													};
												}
											}
											
										)
									}
								});
							} else {
								ShowPesanErrorReturRWJ('Belum melakukan pembayaran','Error');
							}
						}  
					},
					//-------------- ## --------------
					{
						xtype:'tbseparator'
					},
					{
						xtype:'splitbutton',
						text:'Cetak',
						iconCls:'print',
						id:'btnPrint_viReturRWJ',
						disabled:true,
						handler:function()
						{							
							
						},
						menu: new Ext.menu.Menu({
						items: [
							// these items will render as dropdown menu items when the arrow is clicked:
							{
								xtype: 'button',
								text: 'Print Bill',
								id: 'btnprintbill_retur_rwjReturRWJ',
								handler: function()
								{
									printbill_retur_rwj();
									// panelnew_window_printer_ReturRWJ();
								}
							},
							{
								xtype: 'button',
								text: 'Print Resep Pengganti',
								id: 'btnprint_Resep_Pengganti_ReturRWJ',
								handler: function()
								{
									printbill_resep_pengganti_rwj();
									// panelnew_window_printer_ReturRWJ();
								}
							}
						]
						})
					},
					{
						xtype:'tbseparator'
					}
					
				]
			}//,items:
		}
    )

    return pnlFormDataBasic_viApotekReturRWJ;
}
// End Function getFormItemEntry_viApotekReturRWJ # --------------

/**
*	Function : getItemPanelInputBiodata_viApotekReturRWJ
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemPanelInputBiodata_viApotekReturRWJ(lebar) 
{
    
    var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		labelWidth: 100,
		autoWidth: true,
		border:true,
		items:
		[			
			{
                xtype: 'compositefield',
                fieldLabel: 'No. Retur ',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					{
						xtype: 'textfield',
						flex: 1,
						width : 130,	
						readOnly: true,
						name: 'txtNoReturRWJ_viApotekReturRWJ',
						id: 'txtNoReturRWJ_viApotekReturRWJ',
						emptyText: 'No Retur',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},                    
                    //-------------- ## --------------  
					{
						xtype: 'displayfield',
						flex: 1,
						width: 110,
						name: '',
						value: 'No Medrec :',
						fieldLabel: 'Label'
					},	
					{
						xtype: 'textfield',
						width : 120,	
						id: 'txtTmpKdPasienApotekReturRWJL',
						readOnly: true,
						emptyText: 'Kode pasien'
					},
					{
						xtype: 'displayfield',
						flex: 1,
						width: 75,
						name: '',
					},
					{
						xtype: 'displayfield',
						flex: 1,
						width: 70,
						name: '',
						value: 'Poliklinik :',
						fieldLabel: 'Label'
					},
					ComboUnitApotekReturRWJLookup(), 
					
                ]
            },
			//-------------- ## --------------  
			{
                xtype: 'compositefield',
                fieldLabel: 'Tanggal ',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					{
						xtype: 'datefield',
						flex: 1,
						width : 130,	
						format: 'd/M/Y',
						name: 'dftanggalReturRWJ',
						id: 'dftanggalReturRWJ',
						value:now_viApotekReturRWJ
					},
					/* {
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						name: 'txtNoFakturRWJ_viApotekReturRWJ',
						id: 'txtNoFakturRWJ_viApotekReturRWJ',
						emptyText: 'No Resep',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},     */  	
					
                    //-------------- ## --------------  
					{
						xtype: 'displayfield',
						flex: 1,
						width: 110,
						name: '',
						value: 'Nama Pasien:',
						fieldLabel: 'Label'
					},					
					{
						xtype: 'textfield',
						flex: 1,
						width : 200,	
						name: 'txtNamaRWJ_viApotekReturRWJ',
						id: 'txtNamaRWJ_viApotekReturRWJ',
						emptyText: 'Nama Pasien',
						readOnly: true,
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},
					{
						xtype: 'displayfield',
						flex: 1,
						width: 70,
						name: '',
						value: 'Dokter :',
						fieldLabel: 'Label'
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 200,	
						name: 'txtDokterRWJ_viApotekReturRWJ',
						id: 'txtDokterRWJ_viApotekReturRWJ',
						emptyText: 'Dokter',
						readOnly: true,
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					} 
					
                ]
            },
            //-------------- ## -------------- 
						
			{
                xtype: 'compositefield',
                fieldLabel: 'No Resep ',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					ReturRWJ.form.ComboBox.noResep= new Nci.form.Combobox.autoComplete({
						store	: ReturRWJ.form.ArrayStore.noResep,
						select	: function(a,b,c){
							ReturRWJ.form.Grid.a.getView().refresh();
							Ext.getCmp('txtNamaRWJ_viApotekReturRWJ').setValue(b.data.nmpasien);
							Ext.getCmp('txtDokterRWJ_viApotekReturRWJ').setValue(b.data.nama_dokter);
							Ext.getCmp('txtJenisPasienRWJ_viApotekReturRWJ').setValue(b.data.customer);
							Ext.getCmp('cbo_UnitLookupReturRWJ').setValue(b.data.nama_unit);
							Ext.getCmp('txtTmpKdCustomerApotekReturRWJL').setValue(b.data.kd_customer);
							Ext.getCmp('txtTmpKdDokterApotekReturRWJL').setValue(b.data.dokter);
							Ext.getCmp('txtTmpKdUnitApotekReturRWJL').setValue(b.data.kd_unit);
							Ext.getCmp('txtTmpNooutLamaApotekReturRWJL').setValue(b.data.no_out);
							Ext.getCmp('txtTmpTgloutLamaApotekReturRWJL').setValue(b.data.tgl_out);
							Ext.getCmp('txtTmpKdUnitFarApotekReturRWJL').setValue(b.data.kd_unit_far);
							Ext.getCmp('txtTmpKdPasienApotekReturRWJL').setValue(b.data.kd_pasienapt);
							Ext.getCmp('txtTmpNoTrApotekReturRWJL').setValue(b.data.apt_no_transaksi);
							Ext.getCmp('txtTmpKdKasirApotekReturRWJL').setValue(b.data.apt_kd_kasir);
							
							Ext.getCmp('btnSimpan_viApotekReturRWJ').enable();
							Ext.getCmp('btnSimpanExit_viApotekReturRWJ').enable();
							Ext.getCmp('btnbayar_viApotekReturRWJ').disable();
							 //ReturRWJ.form.Grid.a.get
							//ReturRWJ.form.Grid.a.getView().refresh();
							//Store.loadData([],false);
							//dsDataGrdJab_viApotekReturRWJ.reload();
							dsDataGrdJab_viApotekReturRWJ.removeAll();
							dataGridObatApotekReturRWJ(b.data.no_out,b.data.tgl_out,b.data.kd_unit_far);
							getReduksi();
							
						},
						width	: 130,
						insert	: function(o){
							return {
								
								no_out				:o.no_out,
								tgl_out				:o.tgl_out,
								kd_pasien			:o.kd_pasien,
								kd_unit				:o.kd_unit,
								nama_unit			:o.nama_unit,	
								dokter				:o.dokter,
								nama_dokter			:o.nama_dokter,
								kd_pasienapt		:o.kd_pasienapt,
								nmpasien			:o.nmpasien,							
								no_out				:o.no_out,
								tgl_out				:o.tgl_out,
								kd_customer			:o.kd_customer,
								jenis_pasien		:o.jenis_pasien,
								kd_unit_far			:o.kd_unit_far,
								apt_no_transaksi	:o.apt_no_transaksi,
								apt_kd_kasir		:o.apt_kd_kasir,
								customer			:o.customer,
								text				:  '<table style="font-size: 11px;"><tr><td width="80">'+o.no_resep+'</td><td width="180">'+o.nmpasien+'</td></tr></table>',
								no_resep			:o.no_resep	
							}
						},
						param:function(){
							return {
								tanggal:Ext.getCmp('dftanggalReturRWJ').getValue()
							}
						},
						url		: baseURL + "index.php/apotek/functionApotekReturRWJ/getNoResepRWJ",
						valueField: 'no_resep',
						displayField: 'text',
						listWidth: 260,
						emptyText: 'No Resep'
					}),                 
                    //-------------- ## --------------  
					{
						xtype: 'displayfield',
						flex: 1,
						width: 110,
						name: '',
						value: 'Jenis Pasien :',
						fieldLabel: 'Label'
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 200,	
						name: 'txtJenisPasienRWJ_viApotekReturRWJ',
						id: 'txtJenisPasienRWJ_viApotekReturRWJ',
						emptyText: 'Jenis Pasien',
						readOnly: true,
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},  
					//------------------HIDDEN------------------
					{
						xtype: 'textfield',
						width : 120,	
						id: 'txtTmpNooutLamaApotekReturRWJL',
						readOnly: true,
						emptyText: 'No out',
						hidden:true
					},
					{
						xtype: 'textfield',
						width : 120,	
						id: 'txtTmpTgloutLamaApotekReturRWJL',
						readOnly: true,
						emptyText: 'tanggal out',
						hidden:true
					},
					{
						xtype: 'textfield',
						width : 120,	
						id: 'txtTmpNooutApotekReturRWJL',
						readOnly: true,
						emptyText: 'No out',
						hidden:true
					},
					{
						xtype: 'textfield',
						width : 120,	
						id: 'txtTmpTgloutApotekReturRWJL',
						readOnly: true,
						emptyText: 'tanggal out',
						hidden:true
					},
					{
						xtype: 'textfield',
						width : 120,	
						id: 'txtTmpKdDokterApotekReturRWJL',
						readOnly: true,
						emptyText: 'Kode dokter',
						hidden:true
					},
					{
						xtype: 'textfield',
						width : 120,	
						id: 'txtTmpKdUnitApotekReturRWJL',
						readOnly: true,
						emptyText: 'Kode unit',
						hidden:true
					},
					{
						xtype: 'textfield',
						width : 120,	
						id: 'txtTmpKdCustomerApotekReturRWJL',
						readOnly: true,
						emptyText: 'Kode customer',
						hidden:true
					},
					{
						xtype: 'textfield',
						width : 120,	
						id: 'txtTmpKdUnitFarApotekReturRWJL',
						readOnly: true,
						emptyText: 'Kode unit far',
						hidden:true
					},
					{
						xtype: 'textfield',
						width : 120,	
						id: 'txtTmpTotQtyApotekReturRWJL',
						readOnly: true,
						emptyText: 'Kode unit far',
						hidden:true
					},
					/* {
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtTmpDiscAllReturRWIL',
						id: 'txtTmpDiscAllReturRWIL',
						emptyText: 'Status post',
						hidden:true
					}, */
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtTmpStatusPostReturRWIL',
						id: 'txtTmpStatusPostReturRWIL',
						emptyText: 'Status post',
						hidden:true
					},
					{
						xtype: 'textfield',
						width : 120,	
						id: 'txtTmpNoTrApotekReturRWJL',
						readOnly: true,
						emptyText: 'No Transaksi',
						hidden:true
					},
					{
						xtype: 'textfield',
						width : 120,	
						id: 'txtTmpKdKasirApotekReturRWJL',
						readOnly: true,
						emptyText: 'Kode kasir',
						hidden:true
					}
					
                ]
            }
		]
	};
    return items;
};
// End Function getItemPanelInputBiodata_viApotekReturRWJ # --------------


/**
*	Function : getItemGridTransaksi_viApotekReturRWJ
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemGridTransaksi_viApotekReturRWJ(lebar) 
{
    var items =
	{
		//title: 'Detail Transaksi', 
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:1px 1px 1px 1px',
		border:true,
		width: lebar-80,
		height: 235,//225, 
		tbar:
		[
			{
				text	: 'Delete',
				id		: 'btnHapusResepReturRWJ',
				tooltip	: nmLookup,
				iconCls	: 'remove',
				handler	: function(){
					Ext.Msg.confirm('Warning', 'Apakah resep obat ini akan dihapus?', function(button){
						if (button == 'yes'){
							var line = ReturRWJ.form.Grid.a.getSelectionModel().selection.cell[0];
							var o = dsDataGrdJab_viApotekReturRWJ.getRange()[line].data;
							if(dsDataGrdJab_viApotekReturRWJ.getCount() > 1){
								dsDataGrdJab_viApotekReturRWJ.removeAt(line);
								ReturRWJ.form.Grid.a.getView().refresh();
								hasilJumlah();
							} else{
								ShowPesanErrorReturRWJ('Data tidak bisa dihapus karena minimal resep 1 obat','Error');
							}
						}
					});
					/* var records = new Array();
					records.push(new dsDataGrdJab_viApotekReturRWJ.recordType());
					dsDataGrdJab_viApotekReturRWJ.add(records); */
				}
			}	
		],
	    items:
		[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:
				[
					gridDataViewEdit_viApotekReturRWJ()
				]	
			}
			//-------------- ## --------------
		]
	};
    return items;
};
// End Function getItemGridTransaksi_viApotekReturRWJ # --------------

/**
*	Function : gridDataViewEdit_viApotekReturRWJ
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function gridDataViewEdit_viApotekReturRWJ()
{
    
    /* chkSelected_viApotekReturRWJ = new Ext.grid.CheckColumn
	(
		{
			id: 'chkSelected_viApotekReturRWJ',
			header: '',
			align: 'center',						
			dataIndex: 'SELECTED',			
			width: 20
		}
	); */

    var FieldGrdKasir_viApotekReturRWJ = [];
	
    dsDataGrdJab_viApotekReturRWJ= new WebApp.DataStore
	({
        fields: FieldGrdKasir_viApotekReturRWJ
    });
    
    ReturRWJ.form.Grid.a =new Ext.grid.EditorGridPanel({
        store: dsDataGrdJab_viApotekReturRWJ,
        height: 210,//220,
		stripeRows: true,
		columnLines: true,
		selModel: new Ext.grid.CellSelectionModel({
	            singleSelect: true,
	            listeners: {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
        }),
        
        columns: 
		[	
			new Ext.grid.RowNumberer(),
			{
				dataIndex: 'kd_milik',
				header: 'M',
				// hidden: true,
				width: 30
			},
			{			
				dataIndex: 'kd_prd',
				header: 'Kode',
				sortable: true,
				width: 70
			},
			//-------------- ## --------------
			{
				dataIndex: 'nama_obat',
				header: 'Uraian',
				sortable: true,
				width: 255
			},
			//-------------- ## --------------
			{
				dataIndex: 'kd_satuan',
				header: 'Satuan',
				sortable: true,
				width: 70
			},
			//-------------- ## --------------
			{
				dataIndex: 'harga_jual',
				header: 'Harga Satuan',
				xtype:'numbercolumn',
				sortable: true,
				align:'right',
				width: 90
			},
			//-------------- ## --------------
			{
				dataIndex: 'jml',
				header: 'Qty resep',
				xtype:'numbercolumn',
				sortable: true,
				align:'right',
				//hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'qty',
				header: 'Qty',
				xtype:'numbercolumn',
				sortable: true,
				width: 50,
				align:'right',
				editor: new Ext.form.NumberField({
					allowBlank: false,
					listeners:{
						blur: function(a){
							var line	= this.index;
							if(a.getValue()=='' || a.getValue()==undefined){
								ShowPesanWarningReturRWJ('Qty obat yang akan diretur belum di isi', 'Warning');
							}else{
								dsDataGrdJab_viApotekReturRWJ.getRange()[line].data.qty=a.getValue();
								hasilJumlah();
							}
						},
						focus: function(a){
							this.index=ReturRWJ.form.Grid.a.getSelectionModel().selection.cell[0]
						}
					}
				})	
			},
			//-------------- ## --------------
			/* {
				dataIndex: 'disc',
				header: 'Disc',
				sortable: true,
				xtype:'numbercolumn',
				hidden:true,
				align:'right',
				width: 90,
				editor: new Ext.form.NumberField({
					allowBlank: false,
					listeners:{
						blur: function(a){
							var line	= this.index;
							dsDataGrdJab_viApotekReturRWJ.getRange()[line].data.disc=a.getValue();
							hasilJumlah();
						},
						focus: function(a){
							this.index=ReturRWJ.form.Grid.a.getSelectionModel().selection.cell[0]
						}
					}
				})	
			}, */
			{
				dataIndex: 'jumlah',
				header: 'Sub Total',
				sortable: true,
				xtype:'numbercolumn',
				align:'right',
				width: 120
			},
			{
				dataIndex: 'reduksi',
				header: 'Reduksi',
				sortable: true,
				xtype:'numbercolumn',
				align:'right',
				width: 90
			},
			//-------------- ## --------------
			//------------------HIDDEN-------------------------
			{
				dataIndex: 'no_out',
				header: 'No Out',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'no_urut',
				header: 'No Urut',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'tgl_out',
				header: 'tgl out',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'kd_pabrik',
				header: 'kd pabrik',
				hidden: true,
				width: 80
			},
			{
				dataIndex: 'dosis',
				header: 'dosis',
				hidden: true,
				width: 80
			},
			{
				dataIndex: 'markup',
				header: 'markup',
				hidden: true,
				width: 80
			},
			{
				dataIndex: 'harga_beli',
				header: 'Harga Pokok',
				xtype:'numbercolumn',
				hidden: true,
				align:'right',
				width: 90
			}
			//-------------- ## --------------
        ]
       // plugins:chkSelected_viApotekReturRWJ,
    });
    return  ReturRWJ.form.Grid.a;
}
// End Function gridDataViewEdit_viApotekReturRWJ # --------------



/**
*	Function : setLookUp_viApotekResepRWJ
*	
*	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
*/

function setLookUp_bayarResepRWJ(rowdata)
{
    var lebar = 450;
	var stmpnoOut=0;
	var stmptgl;
    setLookUpApotek_bayarReturRWJ = new Ext.Window
    (
    {
        id: 'setLookUpApotek_bayarReturRWJ',
		name: 'setLookUpApotek_bayarReturRWJ',
        title: 'Pembayaran Retur Rawat Jalan', 
        closeAction: 'destroy',        
        width: 523,
        height: 230,
        resizable:false,
		emptyText:'Pilih Jenis Pembayaran...',
		autoScroll: false,
        border: true,
        constrain : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: [ getItemPanelBiodataPembayaran_viApotekReturRWJ(lebar,rowdata),
				 getItemPanelBiodataUang_viApotekReturRWJ(lebar,rowdata)
			   ],//1
        listeners:
        {
            activate: function()
            {
				
            },
            afterShow: function()
            {
                this.activate();
				
				// ;
            },
            deactivate: function()
            {
                rowSelected_viApotekReturRWJ=undefined;
            }
        }
    }
    );

    setLookUpApotek_bayarReturRWJ.show();
	Ext.getCmp('txtNamaPasien_PembayaranReturRWJ').setValue(Ext.get('txtNamaRWJ_viApotekReturRWJ').getValue());
	Ext.getCmp('txtkdPasien_PembayaranReturRWJ').setValue(Ext.get('txtTmpKdPasienApotekReturRWJL').getValue());
	Ext.getCmp('txtNoReturRWJ_Pembayaran').setValue(Ext.get('txtNoReturRWJ_viApotekReturRWJ').getValue());
	Ext.getCmp('dftanggalReturRWJ_Pembayaran').setValue(now_viApotekReturRWJ);
	stmpnoOut=Ext.getCmp('txtTmpNooutApotekReturRWJL').getValue();
	stmptgl=Ext.getCmp('txtTmpTgloutApotekReturRWJL').getValue();
	getKdPayRetur_ReturRWJ();
	if(Ext.getCmp('txtTmpNooutApotekReturRWJL').getValue() == 'No out' || Ext.getCmp('txtTmpNooutApotekReturRWJL').getValue() == ''){
		Ext.getCmp('txtTotalReturRWJ_Pembayaran').setValue(Ext.getCmp('txtTotalEditData_viApotekReturRWJ').getValue());
		Ext.getCmp('txtBayarReturRWJ_Pembayaran').setValue(Ext.getCmp('txtTotalEditData_viApotekReturRWJ').getValue());
	}else{
		getSisaAngsuran(stmpnoOut,stmptgl);
	}
	
}
// End Function setLookUpGridDataView_viApotekReturRWJ # --------------

function getItemPanelBiodataPembayaran_viApotekReturRWJ(lebar,rowdata) 
{ 
	var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		labelWidth: 100,
		autoWidth: true,
		border:true,
		items:
		[			
			{
                xtype: 'compositefield',
                fieldLabel: 'No. Retur ',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtNoReturRWJ_Pembayaran',
						id: 'txtNoReturRWJ_Pembayaran',
						emptyText: 'No Retur',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},
					{
						xtype: 'displayfield',
						flex: 1,
						width: 70,
						name: '',
						value: 'Tanggal :',
						fieldLabel: 'Label'
					},
					{
						xtype: 'datefield',
						flex: 1,
						width : 150,	
						format: 'd/M/Y',
						name: 'dftanggalReturRWJ_Pembayaran',
						id: 'dftanggalReturRWJ_Pembayaran',
						readOnly:true
					}
				
                ]
            },
            //-------------- ## --------------  
			{
                xtype: 'compositefield',
                fieldLabel: 'Kode Pasien ',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					//-------------- ## --------------  
					{
						xtype: 'textfield',
						width : 150,	
						name: 'txtkdPasien_PembayaranReturRWJ',
						id: 'txtkdPasien_PembayaranReturRWJ',
						readOnly:true
					},	
					{
						xtype: 'textfield',
						width : 225,	
						name: 'txtNamaPasien_PembayaranReturRWJ',
						id: 'txtNamaPasien_PembayaranReturRWJ',
						readOnly:true
					}
					
                ]
            },
			{
				xtype: 'compositefield',
				fieldLabel: 'Pembayaran ',
				anchor: '100%',
				//labelSeparator: '',
				width: 199,
				items: 
				[
					mComboJenisByrReturRWJ(),
					mComboPembayaran()
				]
			}
					
		]
	};
    return items;
};



function getItemPanelBiodataUang_viApotekReturRWJ(lebar,rowdata) 
{ 
	var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		labelWidth: 100,
		autoWidth: true,
		border:true,
		items:
		[			
			{
                xtype: 'compositefield',
                fieldLabel: ' ',
				labelSeparator: '',
                anchor: '100%',
                width: 250,
                items: 
                [         
					{
						xtype: 'displayfield',
						flex: 1,
						width: 150,
						name: '',
						value: ''
					},	
					{
						xtype: 'displayfield',
						flex: 1,
						width: 70,
						name: '',
						value: 'Total :',
						fieldLabel: 'Label'
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						style:{'text-align':'right'},
						name: 'txtTotalReturRWJ_Pembayaran',
						id: 'txtTotalReturRWJ_Pembayaran',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					}
					
                ]
            },
			{
                xtype: 'compositefield',
                fieldLabel: ' ',
				labelSeparator: '',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					//-------------- ## -------------- 
					{
						xtype: 'displayfield',
						flex: 1,
						width: 150,
						name: '',
						value: ''
					},	
					{
						xtype: 'displayfield',
						flex: 1,
						width: 70,
						name: '',
						value: 'Bayar :',
						fieldLabel: 'Label'
					},						
					{
						xtype: 'numberfield',
						flex: 1,
						width : 150,	
						style:{'text-align':'right'},
						name: 'txtBayarReturRWJ_Pembayaran',
						id: 'txtBayarReturRWJ_Pembayaran',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									pembayaranReturRWJ();
								};
							}
						}
					},
					{
						xtype:'button',
						text:'1/2 Resep',
						width:70,
						hideLabel:true,
						id: 'btn1/2resep_viApotekReturRWJ',
						handler:function()
						{
						}   
					}
                ]
            },
			{
                xtype: 'compositefield',
                fieldLabel: ' ',
				labelSeparator: '',
                anchor: '100%',
                width: 230,
                items: 
                [
				{
						xtype: 'displayfield',
						flex: 1,
						width: 305,
						name: '',
						value: ''
					},
					{
						xtype:'button',
						text:'Paid',
						width:70,
						hideLabel:true,
						id: 'btnBayar_viApotekReturRWJL',
						handler:function()
						{
							pembayaranReturRWJ();
						}   
					}
				]
            }
		]
	};
    return items;
};



/**
*	Function : getItemGridHistoryBayar_viApotekReturRWJ
*	
*	Sebuah fungsi untuk menampilkan isian form Histori pembayaran
*/
function getItemGridHistoryBayar_viApotekReturRWJ(lebar) 
{
    var items =
	{
		title: 'Paid History', 
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
		bodyStyle: 'margin-top: -1px;',
	    //bodyStyle: 'padding:1px 1px 1px 1px',
		border:true,
		width: lebar-80,
		autoScroll:true,
		height: 100,//300, 
	    items:
		[
		
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
				items:
				[
					gridDataViewHistoryBayar_viApotekReturRWJ()
				]	
			}
			//-------------- ## --------------
		]
	};
    return items;
};
// End Function getItemGridHistoryBayar_viApotekReturRWJ # --------------

/**
*	Function : gridDataViewHistoryBayar_viApotekReturRWJ
*	
*	Sebuah fungsi untuk menampilkan isian histori pembayaran
*/
function gridDataViewHistoryBayar_viApotekReturRWJ() 
{

    var fldDetail =  ['TUTUP','KD_PASIENAPT','TGL_OUT','NO_OUT','URUT','TGL_BAYAR','KD_PAY','URAIAN','JUMLAH','JML_TERIMA_UANG','SISA'];
	
    dsTRDetailHistoryBayarList = new WebApp.DataStore({ fields: fldDetail })
		 
    gridDTLTRHistoryApotekReturRWJ = new Ext.grid.EditorGridPanel
    (
        {
            //title: 'History Bayar',
            stripeRows: true,
            store: dsTRDetailHistoryBayarList,
            border: false,
            columnLines: true,
			autoScroll:true,
			height: 72,
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                            /* cellSelecteddeskripsiReturRWJ = dsTRDetailHistoryBayarList.getAt(row);
                            CurrentHistoryReturRWJ.row = row;
                            CurrentHistoryReturRWJ.data = cellSelecteddeskripsiRWJ; */
                        }
                    }
                }
            ),
            columns: [
            			{
							header: 'Status Posting',
							dataIndex: 'TUTUP',
							width:100,
							hidden:true
						},
						{
							header: 'Kode Pasien',
							dataIndex: 'KD_PASIENAPT',
							width:100,
							hidden:true
						},
						{
							header: 'No out',
							dataIndex: 'NO_OUT',
							width:100,
							hidden:true
						},
						{
							header: 'Urut Bayar',
							dataIndex: 'URUT',
							align :'center',
							width:90
							
						},
						{
							header: 'Tanggal Resep',
							dataIndex: 'TGL_OUT',
							align :'center',
							width:130,
							renderer: function(v, params, record)
							{
							   return ShowDate(record.data.TGL_OUT);

							} 
						},
						{
							header: 'Pembayaran',
							dataIndex: 'URAIAN',
							align :'center',
							width:130,
							hidden:false
							
						},
						{
							header: 'Tanggal Bayar',
							dataIndex: 'TGL_BAYAR',
							align :'center',
							width:130,
							renderer: function(v, params, record)
							{
							   return ShowDate(record.data.TGL_BAYAR);

							} 
						},
						{
							header: 'Total Bayar',
							width:130,
							xtype:'numbercolumn',
							align :'right',
							dataIndex: 'JUMLAH',
							renderer: function(v, params, record)
							{
							   return formatCurrency(record.data.JUMLAH);

							}
							
						},
						{
							header: 'Jumlah Angsuran',
							width:130,
							xtype:'numbercolumn',
							align :'right',
							dataIndex: 'JML_TERIMA_UANG',
							renderer: function(v, params, record)
							{
							   return formatCurrency(record.data.JML_TERIMA_UANG);

							}
							
						},
						{
							header: 'Sisa',
							width:120,
							xtype:'numbercolumn',
							align :'right',
							dataIndex: 'SISA',
							renderer: function(v, params, record)
							{
							   return formatCurrency(record.data.SISA);

							}
							
						}
        			],
//            cm: TRHistoryColumModelApotekReturRWJ(),
					viewConfig: {forceFit: true}
		}
    );
    return gridDTLTRHistoryApotekReturRWJ;
};

function TRHistoryColumModelApotekReturRWJ() 
{
    return new Ext.grid.ColumnModel
    (
        [
           new Ext.grid.RowNumberer(),
            {
                id: 'colKdTransaksi',
                header: 'No. Transaksi',
                dataIndex: 'NO_TRANSAKSI',
                width:100,
					menuDisabled:true,
                hidden:false
            },
			{
                id: 'colTGlbayar',
                header: 'Tgl Bayar',
                dataIndex: 'TGL_BAYAR',
					menuDisabled:true,
				width:100,
				renderer: function(v, params, record)
                {
                   return ShowDate(record.data.TGL_BAYAR);

                }
                
            },
			{
                id: 'coleurutmasuk',
                header: 'urut Bayar',
                dataIndex: 'URUT'
				//hidden:true
                
            }
            
            ,
			{
                id: 'colePembayaran',
                header: 'Pembayaran',
                dataIndex: 'DESKRIPSI',
				width:150,
				hidden:false
                
            }
			,
			{
                id: 'colJumlah',
                header: 'Jumlah',
				width:150,
				align :'right',
                dataIndex: 'BAYAR',
				hidden:false,
				renderer: function(v, params, record)
                {
                   return formatCurrency(record.data.BAYAR);

                }
                
            }
			,
			
			{
                id: 'coletglmasuk',
                header: 'tgl masuk',
                dataIndex: '',
				hidden:true
                
            }
            ,
            {
                id: 'colStatHistory',
                header: 'History',
                width:130,
				menuDisabled:true,
                dataIndex: '',
				hidden:true
              
				
				
              
            },
            {
                id: 'colPetugasHistory',
                header: 'Petugas',
                width:130,
				menuDisabled:true,
                dataIndex: 'USERNAME'	
				//hidden:true
                
				
				
              
            }
			

        ]
    )
};
//----------------------------End GetDTLTRHistoryGrid------------------ 


function mComboStatusPostingApotekReturRWJ()
{
  var cboStatusPostingApotekReturRWJ = new Ext.form.ComboBox
	(
		{
                    id:'cboStatusPostingApotekReturRWJ',
                    x: 410,
                    y: 30,
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender:true,
                    mode: 'local',
                    width: 110,
                    emptyText:'',
                    fieldLabel: 'JENIS',
					tabIndex:5,
					editable: false,
                    store: new Ext.data.ArrayStore
                    (
						{
								id: 0,
								fields:
								[
									'Id',
									'displayText'
								],
						data: [[1, 'Semua'],[2, 'Posting'], [3, 'Belum Posting']]
						}
                    ),
                    valueField: 'Id',
                    displayField: 'displayText',
                    value:selectCountStatusPostingApotekReturRWJ,
                    listeners:
                    {
						'select': function(a,b,c)
						{
							selectCountStatusPostingApotekReturRWJ=b.data.displayText;
							tmpkriteria = getCriteriaCariApotekReturRWJ();
							refreshReturApotekRWJ(tmpkriteria);

						}
                    }
		}
	);
	return cboStatusPostingApotekReturRWJ;
};

function ComboUnitApotekReturRWJ()
{
    var Field_Vendor = ['KD_UNIT', 'NAMA_UNIT'];
    ds_unit = new WebApp.DataStore({fields: Field_Vendor});
    ds_unit.load
	(
            {
                params:
                    {
                        Skip: 0,
                        Take: 1000,
                        Sort: 'kd_unit',
                        Sortdir: 'ASC',
                        target: 'ComboUnitApotek',
                        param: "parent='2'"
                    }
            }
	);
	
    var cbo_UnitReturRWJ = new Ext.form.ComboBox
    (
        {
			x:130,
			y:60,
            flex: 1,
			id: 'cbo_UnitReturRWJ',
            fieldLabel: 'Poliklinik',
			valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
			emptyText:'Poliklinik',
			store: ds_unit,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:150,
			tabIndex:2,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					selectSetUnit=b.data.displayText ;
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						tmpkriteria = getCriteriaCariApotekReturRWJ();
						refreshReturApotekRWJ(tmpkriteria);
					} 						
				}
			}
        }
    )    
    return cbo_UnitReturRWJ;
}

function ComboUnitApotekReturRWJLookup()
{
    var cbo_UnitLookupReturRWJ = new Ext.form.ComboBox
    (
        {
			x:130,
			y:60,
            flex: 1,
			id: 'cbo_UnitLookupReturRWJ',
			readOnly:true,
            fieldLabel: 'Poliklinik',
			valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
			emptyText:'Poliklinik',
			store: ds_unit,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:150,
			tabIndex:2,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					selectSetUnit=b.data.displayText ;
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_UnitLookupReturRWJ;
}

function mComboJenisByrReturRWJ() 
{
	var Field = ['JENIS_PAY','DESKRIPSI','TYPE_DATA'];

    dsJenisbyrView = new WebApp.DataStore({ fields: Field });
    dsJenisbyrView.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000, Sort: 'jenis_pay',
			    Sortdir: 'ASC',
			    target: 'ViewJenisPay',
				param: "TYPE_DATA IN (0,1,3) AND DB_CR='1' ORDER BY Type_data"
			  
			}
		}
	);
	
    var cboJenisByrReturRWJ = new Ext.form.ComboBox
	(
		{
	
		    id: 'cboJenisByrReturRWJ',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: 'Jenis Bayar',
			fieldLabel: 'Pembayaran',
		    align: 'Right',
		    width:150,
		    store: dsJenisbyrView,
		    valueField: 'JENIS_PAY',
		    displayField: 'DESKRIPSI',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
					loaddatastorePembayaran(b.data.JENIS_PAY);
					// Ext.getCmp('cboPembayaranReturRWJ').enable();
			   
				}
				  

			}
		}
	);
	
    return cboJenisByrReturRWJ;
};
function loaddatastorePembayaran(jenis_pay)
{
    dsComboBayar.load
	(
		{
		 params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'nama',
					Sortdir: 'ASC',
					target: 'ViewComboBayar',
					param: 'jenis_pay=~'+ jenis_pay+ '~'
				}
	   }
	)      
}

function mComboPembayaran()
{
    var Field = ['KD_PAY','JENIS_PAY','PAYMENT'];

    dsComboBayar = new WebApp.DataStore({fields: Field});
	
    var cboPembayaranReturRWJ = new Ext.form.ComboBox
	(
		{
		    id: 'cboPembayaranReturRWJ',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
            forceSelection: true,
		    emptyText:'Pilih Pembayaran...',
		    labelWidth:80,
		    align: 'Right',
		    store: dsComboBayar,
		    valueField: 'KD_PAY',
		    displayField: 'PAYMENT',
			// disabled:true,
			width:225,
			listeners:
			{
			    'select': function(a, b, c) 
				{
				
					kd_pay_retur=b.data.KD_PAY;
				}
				  

			}
		}
	);

    return cboPembayaranReturRWJ;
};

function hasilJumlah(){
	var total=0;
	//var totdisc=0;
	var totalall=0;
	var reduksi=0;
	var nilaiReduksi = Ext.getCmp('txttmpReduksiEditData_viApotekReturRWJ').getValue();
	var totqty=0;
	
	for(var i=0; i < dsDataGrdJab_viApotekReturRWJ.getCount() ; i++){
		var jumlahGrid=0;
		var subJumlah=0;
		//var disc=0;
		//var reduksi=0;
		
	
		var o=dsDataGrdJab_viApotekReturRWJ.getRange()[i].data;
		if(o.qty != undefined){
			if(o.qty > 0){
				console.log(o.jml);
				console.log(o.qty);
				if(o.jml >= o.qty){
					jumlahGrid=(parseFloat(o.qty) * parseFloat(o.harga_jual));
					o.jumlah = jumlahGrid;
					o.reduksi = (parseFloat(o.jumlah) * parseFloat(nilaiReduksi)) / 100;
					total += jumlahGrid;
					totqty += o.qty;
					reduksi += o.reduksi;
				} else{
					ShowPesanWarningReturRWJ('Jumlah yang diretur tidak boleh lebih dari jumlah pembelian resep','Warning');
					o.qty=o.jml;
				}
			} else{
				ShowPesanWarningReturRWJ('Qty tidak boleh 0','Warning');
			}
			
		}

	}
	
	//reduksi +=parseFloat(total)*(parseFloat(Ext.getCmp('txttmpReduksiEditData_viApotekReturRWJ').getValue())/100);
	totalall += parseFloat(total)-parseFloat(reduksi);
	Ext.getCmp('txtReduksiEditData_viApotekReturRWJ').setValue(toFormat(reduksi));
	Ext.getCmp('txtTmpTotQtyApotekReturRWJL').setValue(totqty);
	Ext.getCmp('txtDRGridJmlEditData_viApotekReturRWJ').setValue(aptpembulatan(total));
	Ext.getCmp('txtTotalEditData_viApotekReturRWJ').setValue(aptpembulatan(totalall));
	//Ext.getCmp('txtTmpDiscAllReturRWIL').setValue(toFormat(disc));
	//Ext.get('txtTotalEditData_viApotekReturRWJ').dom.value=totalall.toLocaleString('id', { style: 'currency', currency: 'IDR' });
	ReturRWJ.form.Grid.a.getView().refresh();
}

function refreshReturApotekRWJ(kriteria)
{
    dataSource_viApotekReturRWJ.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'ViewApotekReturRWJ',
                    param : kriteria 
                }			
            }
        );   
    return dataSource_viApotekReturRWJ;
}

function ViewDetailPembayaranObat(no_out,tgl_out) 
{	
    var strKriteriaRWJ='';
    strKriteriaRWJ = "a.no_out = '" + no_out + "'" + " And a.tgl_out ='" + tgl_out + "' order by a.urut";
   
    dsTRDetailHistoryBayarList.load({
	    params:{
		    Skip: 0,
		    Take: 1000,
		    Sort: 'tgl_transaksi',
		    Sortdir: 'ASC',
		    target: 'ViewHistoryPembayaran',
		    param: strKriteriaRWJ
		}
	}); 
	return dsTRDetailHistoryBayarList;
	
    
};


function dataGridObatApotekReturRWJ(no_out,tgl_out,kd){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/viewdetailobatretur/read",
			params: {query:"o.no_out = " + no_out + "" + " And o.tgl_out ='" + tgl_out + "' "},// and s.kd_unit_far='"+ kd +"'
			// url: baseURL + "index.php/apotek/functionApotekReturRWJ/getDetailObat",
			// params: {no_out:no_out,tgl_out:tgl_out,kd_unit_far:kd,no_bukti:no_bukti},
			failure: function(o)
			{
				ShowPesanErrorReturRWJ('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dsDataGrdJab_viApotekReturRWJ.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
						dsDataGrdJab_viApotekReturRWJ.add(recs);
					
					
					
					ReturRWJ.form.Grid.a.getView().refresh();
					/* Ext.getCmp('txtAdmRacikReturRWJL').setValue(admracik); */
					
					hasilJumlah();
					/* alert(status_posting +'pisah'+ dsTRDetailHistoryBayarList.getCount());
					if(dsTRDetailHistoryBayarList.getCount() == 0 && status_posting =='1'){
						Ext.getCmp('btnDeleteHistory_viApotekReturRWJ').disable();
					} else{
					
						Ext.getCmp('btnDeleteHistory_viApotekReturRWJ').enable();
					} */
				}
				else 
				{
					ShowPesanErrorReturRWJ('Gagal membaca data obat', 'Error');
				};
			}
		}
		
	)
	
}

function datasave_viApotekReturRWJ(){
	//if (ValidasiEntryReturRWJ(nmHeaderSimpanData,false) == 1 )
	//{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionApotekReturRWJ/saveReturRWJ",
				params: getParamReturRWJ(),
				failure: function(o)
				{
					ShowPesanErrorReturRWJ('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoReturRWJ(nmPesanSimpanSukses,nmHeaderSimpanData);
						Ext.get('txtNoReturRWJ_viApotekReturRWJ').dom.value=cst.noresep;
						Ext.get('txtTmpNooutApotekReturRWJL').dom.value=cst.noout;
						Ext.get('txtTmpTgloutApotekReturRWJL').dom.value=cst.tgl;
						Ext.getCmp('btnbayar_viApotekReturRWJ').enable();
						Ext.getCmp('btnSimpan_viApotekReturRWJ').disable();
						Ext.getCmp('btnSimpanExit_viApotekReturRWJ').disable();
						Ext.getCmp('btnHapusResepReturRWJ').disable();
						/* refreshReturApotekRWJ();
						dataGridObatApotekReturRWJ(Ext.getCmp('txtTmpNooutApotekReturRWJL').getValue(),Ext.getCmp('txtTmpTgloutApotekReturRWJL').getValue());
						if(mBol === false)
						{
							
						}; */
					}
					else 
					{
						ShowPesanErrorReturRWJ('Gagal Menyimpan Data ini', 'Error');
						refreshRespApotekRWI();
					};
				}
			}
			
		)
	//}
}

function pembayaranReturRWJ(){
	if(ValidasiBayarReturRWJ(nmHeaderSimpanData,false) == 1 ){
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionApotekReturRWJ/bayarSaveReturRWJ",
				params: getParamBayarReturRWJ(),
				failure: function(o)
				{
					ShowPesanErrorReturRWJ('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						refreshReturApotekRWJ();
						if(toInteger(Ext.getCmp('txtBayarReturRWJ_Pembayaran').getValue()) >= toInteger(Ext.getCmp('txtTotalReturRWJ_Pembayaran').getValue())){
							ShowPesanInfoReturRWJ('Berhasil melakukan pembayaran dan pembayaran telah lunas','Information');
							Ext.getCmp('txtNoReturRWJ_viApotekReturRWJ').setValue(cst.noretur);
							Ext.getCmp('txtTmpNooutApotekReturRWJL').setValue(cst.noout);
							Ext.getCmp('txtTmpTgloutApotekReturRWJL').setValue(cst.tgl);
							Ext.getCmp('btnunposting_viApotekReturRWJ').disable();
							Ext.getCmp('btnPrint_viReturRWJ').enable();
							Ext.getCmp('btnbayar_viApotekReturRWJ').disable();
							//Ext.getCmp('btnAdd_viApotekResepRWJ').disable();
							Ext.getCmp('btnSimpan_viApotekReturRWJ').disable();
							Ext.getCmp('btnSimpanExit_viApotekReturRWJ').disable();
							Ext.getCmp('btnHapusResepReturRWJ').disable();
							Ext.getCmp('btnDeleteHistory_viApotekReturRWJ').disable();
							Ext.getCmp('btnbayar_viApotekReturRWJ').disable();
							ReturRWJ.form.Panel.a.update('<img src="'+baseURL+'ui/images/icons/16x16/hijau.png"/>');
							ViewDetailPembayaranObat(cst.noout,cst.tgl);
							gridDTLTRHistoryApotekReturRWJ.getView().refresh();
							refreshReturApotekRWJ();
							//update_mrobat();
							setLookUpApotek_bayarReturRWJ.close();
							
						} else {
							ShowPesanInfoReturRWJ('Berhasil melakukan pembayaran','Information');
							
							Ext.getCmp('btnSimpan_viApotekReturRWJ').disable();
							Ext.getCmp('btnSimpanExit_viApotekReturRWJ').disable();
							Ext.getCmp('btnunposting_viApotekReturRWJ').disable();
							Ext.getCmp('btnDeleteHistory_viApotekReturRWJ').enable();
							Ext.getCmp('btnPrint_viReturRWJ').disable();
							refreshReturApotekRWJ();
							setLookUpApotek_bayarReturRWJ.close();
							ViewDetailPembayaranObat(Ext.getCmp('txtTmpNooutApotekReturRWJL').getValue(),Ext.getCmp('txtTmpTgloutApotekReturRWJL').getValue());
							gridDTLTRHistoryApotekReturRWJ.getView().refresh();
						}
					}
					else 
					{
						ShowPesanErrorReturRWJ('Gagal melakukan pembayaran', 'Error');
						refreshReturApotekRWJ();
					};
				}
			}
			
		)
	}
};

function update_mrobat(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionInsertMrObat/retur_mrobat",
			params: getParamUpdateMrObat(),
			failure: function(o)
			{
				ShowPesanErrorReturRWJ('Error update MR! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				
			}
		}
		
	)
}


function unposting_mrobat(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionInsertMrObat/retur_unpost_mrobat",
			params: getParamUnPostingMrObat(),
			failure: function(o)
			{
				ShowPesanErrorReturRWJ('Error update MR! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				
			}
		}
		
	)
}


function printbill_retur_rwj()
{
    Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/main/CreateDataObj",
			params: datacetakbill(),
			failure: function(o)
			{	
				ShowPesanErrorReturRWJ('Error hubungi admin' ,'Error');
			},
			success: function(o)
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					
				}
				else if  (cst.success === false && cst.pesan===0)
				{
					ShowPesanWarningReturRWJ('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
				}
				else
				{
					ShowPesanErrorReturRWJ('Gagal print '  + 'Error');
				}
			}
		}
	)
};

function datacetakbill(){
	var tmpkode;
	var tmpdokter;
	
	if(Ext.getCmp('txtTmpKdPasienApotekReturRWJL').getValue() === '' ||Ext.getCmp('txtTmpKdPasienApotekReturRWJL').getValue() === 'Kode pasien'){
		tmpkode='-';
	} else{
		tmpkode=Ext.getCmp('txtTmpKdPasienApotekReturRWJL').getValue();
	}
	
	if(Ext.get('txtDokterRWJ_viApotekReturRWJ').getValue() == '' || Ext.get('txtDokterRWJ_viApotekReturRWJ').getValue()=='Dokter'){
		tmpdokter='-';
	} else{
		tmpdokter=Ext.get('txtDokterRWJ_viApotekReturRWJ').getValue();
	}
	
	var unit="";
	if(Ext.get('cbo_UnitLookupReturRWJ').getValue() == '' || Ext.get('cbo_UnitLookupReturRWJ').getValue() == 'Poliklinik'){
		unit="-";
	} else{
		unit=Ext.get('cbo_UnitLookupReturRWJ').getValue();
	}
	
	var params =
	{
		Table: 'billprintingreturrwj',
		NoRetur:Ext.getCmp('txtNoReturRWJ_viApotekReturRWJ').getValue(),
		NoResep:ReturRWJ.form.ComboBox.noResep.getValue(),
		NoOut:Ext.getCmp('txtTmpNooutApotekReturRWJL').getValue(),
		TglOut:Ext.getCmp('txtTmpTgloutApotekReturRWJL').getValue(),
		KdPasien:tmpkode,
		NamaPasien:Ext.getCmp('txtNamaRWJ_viApotekReturRWJ').getValue(),
		JenisPasien:Ext.getCmp('txtJenisPasienRWJ_viApotekReturRWJ').getValue(),
		Dokter:tmpdokter,
		Reduksi:Ext.getCmp('txtReduksiEditData_viApotekReturRWJ').getValue(),
		SubTotal:Ext.getCmp('txtDRGridJmlEditData_viApotekReturRWJ').getValue(),
		Total:Ext.getCmp('txtTotalEditData_viApotekReturRWJ').getValue(),
		Tot:toInteger(Ext.getCmp('txtTotalEditData_viApotekReturRWJ').getValue()),
		// printer:Ext.getCmp('cbopasienorder_printer_retur_rwi').getValue(),
		kd_form:KdFormReturRWJ,
		Poli:unit
		
	}
	params['jumlah']=dsDataGrdJab_viApotekReturRWJ.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viApotekReturRWJ.getCount();i++)
	{
		params['nama_obat-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.nama_obat;
		params['qty-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.qty;
	}
	
    return params
}

function printbill_resep_pengganti_rwj()
{
    Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/main/CreateDataObj",
			params: datacetakbill_resep_pengganti(),
			failure: function(o)
			{	
				ShowPesanErrorReturRWJ('Error hubungi admin' ,'Error');
			},
			success: function(o)
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					
				}
				else if  (cst.success === false && cst.pesan===0)
				{
					ShowPesanWarningReturRWJ('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
				}
				else
				{
					ShowPesanErrorReturRWJ('Gagal print '  + 'Error');
				}
			}
		}
	)
};
function datacetakbill_resep_pengganti(){
	var tmpkode;
	var tmpdokter;
	
	if(Ext.getCmp('txtTmpKdPasienApotekReturRWJL').getValue() === '' ||Ext.getCmp('txtTmpKdPasienApotekReturRWJL').getValue() === 'Kode pasien'){
		tmpkode='-';
	} else{
		tmpkode=Ext.getCmp('txtTmpKdPasienApotekReturRWJL').getValue();
	}
	
	if(Ext.get('txtDokterRWJ_viApotekReturRWJ').getValue() == '' || Ext.get('txtDokterRWJ_viApotekReturRWJ').getValue()=='Dokter'){
		tmpdokter='-';
	} else{
		tmpdokter=Ext.get('txtDokterRWJ_viApotekReturRWJ').getValue();
	}
	
	var unit="";
	if(Ext.get('cbo_UnitLookupReturRWJ').getValue() == '' || Ext.get('cbo_UnitLookupReturRWJ').getValue() == 'Poliklinik'){
		unit="-";
	} else{
		unit=Ext.get('cbo_UnitLookupReturRWJ').getValue();
	}
	
	var params =
	{
		Table: 'billprintingreturrwj_resep_pengganti_rwj',
		NoRetur:Ext.getCmp('txtNoReturRWJ_viApotekReturRWJ').getValue(),
		NoResep:ReturRWJ.form.ComboBox.noResep.getValue(),
		NoOut:Ext.getCmp('txtTmpNooutApotekReturRWJL').getValue(),
		TglOut:Ext.getCmp('txtTmpTgloutApotekReturRWJL').getValue(),
		KdPasien:tmpkode,
		NamaPasien:Ext.getCmp('txtNamaRWJ_viApotekReturRWJ').getValue(),
		JenisPasien:Ext.getCmp('txtJenisPasienRWJ_viApotekReturRWJ').getValue(),
		Dokter:tmpdokter,
		Reduksi:Ext.getCmp('txtReduksiEditData_viApotekReturRWJ').getValue(),
		SubTotal:Ext.getCmp('txtDRGridJmlEditData_viApotekReturRWJ').getValue(),
		Total:Ext.getCmp('txtTotalEditData_viApotekReturRWJ').getValue(),
		Tot:toInteger(Ext.getCmp('txtTotalEditData_viApotekReturRWJ').getValue()),
		// printer:Ext.getCmp('cbopasienorder_printer_retur_rwi').getValue(),
		kd_form:KdFormReturRWJ,
		Poli:unit
		
	}
	params['jumlah']=dsDataGrdJab_viApotekReturRWJ.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viApotekReturRWJ.getCount();i++)
	{
		params['kd_prd-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.kd_prd;
		params['nama_obat-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.nama_obat;
		params['qty-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.qty;
	}
	
    return params
}

function getParamDeleteHistoryReturRWJ() 
{
	var urut=dsTRDetailHistoryBayarList.getRange()[gridDTLTRHistoryApotekReturRWJ.getSelectionModel().selection.cell[0]].data.URUT;
	
	var params =
	{
		NoOut:Ext.getCmp('txtTmpNooutApotekReturRWJL').getValue(),
		TglOut:Ext.getCmp('txtTmpTgloutApotekReturRWJL').getValue(),
		urut:urut
	}
	
	
	
    return params
};

function getReduksi(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionApotekReturRWJ/getReduksi",
			params: {a:0},
			failure: function(o)
			{
				ShowPesanErrorReturRWJ('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					Ext.getCmp('txttmpReduksiEditData_viApotekReturRWJ').setValue(cst.reduksi);
				}
			}
		}
		
	)
	
}

function getSisaAngsuran(stmpnoOut,stmptgl){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionApotekReturRWJ/getSisaAngsuran",
			params: {no_out:stmpnoOut, tgl_out:stmptgl},
			failure: function(o)
			{
				ShowPesanErrorReturRWJ('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				//console.log(cst);
				if (cst.success === true) 
				{
					if(cst.sisa ==''){
						ShowPesanInfoReturRWJ('Pembayaran sebelumnya telah lunas, jika data ini sudah pernah diposting maka hapus terlebih dahulu history pembayaran untuk mendapatkan total pembayaran','Information');
					} else{
						Ext.getCmp('txtTotalReturRWJ_Pembayaran').setValue(toFormat(cst.sisa));
						Ext.getCmp('txtBayarReturRWJ_Pembayaran').setValue(cst.sisa);
					}
					
				}
				else 
				{
					Ext.getCmp('txtTotalReturRWJ_Pembayaran').setValue(Ext.getCmp('txtTotalEditData_viApotekReturRWJ').getValue());
					Ext.getCmp('txtBayarReturRWJ_Pembayaran').setValue(Ext.getCmp('txtTotalEditData_viApotekReturRWJ').getValue());
				};
			}
		}
		
	)
};

function datainit_viApotekReturRWJ(rowdata)
{
	
	dataisi=1;
	ViewDetailPembayaranObat(rowdata.NO_OUT,rowdata.TGL_OUT);
	Ext.getCmp('txtNoReturRWJ_viApotekReturRWJ').setValue(rowdata.NO_FAKTUR);
	Ext.getCmp('cbo_UnitLookupReturRWJ').setValue(rowdata.NAMA_UNIT);
	Ext.getCmp('txtNamaRWJ_viApotekReturRWJ').setValue(rowdata.NMPASIEN);
	Ext.getCmp('txtDokterRWJ_viApotekReturRWJ').setValue(rowdata.NAMA_DOKTER);
	Ext.getCmp('txtJenisPasienRWJ_viApotekReturRWJ').setValue(rowdata.CUSTOMER);
	ReturRWJ.form.ComboBox.noResep.setValue(rowdata.NO_BUKTI);
	
	
	Ext.getCmp('txtTmpKdCustomerApotekReturRWJL').setValue(rowdata.KD_CUSTOMER);
	Ext.getCmp('txtTmpKdDokterApotekReturRWJL').setValue(rowdata.DOKTER);
	Ext.getCmp('txtTmpKdUnitApotekReturRWJL').setValue(rowdata.KD_UNIT);
	Ext.getCmp('txtTmpNooutApotekReturRWJL').setValue(rowdata.NO_OUT);
	Ext.getCmp('txtTmpTgloutApotekReturRWJL').setValue(rowdata.TGL_OUT);
	Ext.getCmp('txtTmpKdUnitFarApotekReturRWJL').setValue(rowdata.KD_UNIT_FAR);
	Ext.getCmp('txtTmpKdPasienApotekReturRWJL').setValue(rowdata.KD_PASIENAPT);
	Ext.getCmp('txtTmpNoTrApotekReturRWJL').setValue(rowdata.APT_NO_TRANSAKSI);
	Ext.getCmp('txtTmpKdKasirApotekReturRWJL').setValue(rowdata.APT_KD_KASIR);
	Ext.getCmp('txtTmpStatusPostReturRWIL').setValue(rowdata.STATUS_POSTING);
	if(rowdata.STATUS_POSTING === '1'){
		Ext.getCmp('btnunposting_viApotekReturRWJ').enable();
		Ext.getCmp('btnPrint_viReturRWJ').enable();
		Ext.getCmp('btnbayar_viApotekReturRWJ').disable();
		//Ext.getCmp('btnAdd_viApotekReturRWJ').disable();
		Ext.getCmp('btnSimpan_viApotekReturRWJ').disable();
		Ext.getCmp('btnSimpanExit_viApotekReturRWJ').disable();
		Ext.getCmp('btnHapusResepReturRWJ').disable();
		ReturRWJ.form.ComboBox.noResep.setReadOnly(true);
		ReturRWJ.form.Panel.a.update('<img src="'+baseURL+'ui/images/icons/16x16/hijau.png"/>');
	} else{
		ReturRWJ.form.ComboBox.noResep.setReadOnly(true);
		Ext.getCmp('btnSimpan_viApotekReturRWJ').disable();
		Ext.getCmp('btnSimpanExit_viApotekReturRWJ').disable();
		Ext.getCmp('btnunposting_viApotekReturRWJ').disable();
		Ext.getCmp('btnPrint_viReturRWJ').disable();
		Ext.getCmp('btnbayar_viApotekReturRWJ').enable();
		Ext.getCmp('btnHapusResepReturRWJ').enable();
		Ext.getCmp('btnDeleteHistory_viApotekReturRWJ').enable();
		ReturRWJ.form.Panel.a.update('<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>');
	}
	getReduksi();
	dataGridObatApotekReturRWJ(rowdata.NO_OUT,rowdata.TGL_OUT,rowdata.KD_UNIT_FAR);
	
};



function getParamReturRWJ() 
{
	var KdCust='';
	var TmpCustoLama='';
	var tmpNonResep='';
	var tmpNamaPasien='';
	if(Ext.get('txtTmpKdCustomerApotekReturRWJL').getValue()=='Perseorangan'){
			KdCust='0000000001';
	}else if(Ext.get('txtTmpKdCustomerApotekReturRWJL').getValue()=='Perusahaan'){
		KdCust=Ext.getCmp('txtTmpKdCustomerApotekReturRWJL').getValue();
	}else {
		KdCust=Ext.getCmp('txtTmpKdCustomerApotekReturRWJL').getValue();
	}
	
	var ubah=0;
	if(Ext.getCmp('txtNoReturRWJ_viApotekReturRWJ').getValue() === '' || Ext.getCmp('txtNoReturRWJ_viApotekReturRWJ').getValue()=='No Retur'){
		ubah=0;
	} else{
		ubah=1;
	}
	
    var params =
	{
		NoRetur:Ext.getCmp('txtNoReturRWJ_viApotekReturRWJ').getValue(),
		KdPasien:Ext.getCmp('txtTmpKdPasienApotekReturRWJL').getValue(),
		NmPasien:Ext.getCmp('txtNamaRWJ_viApotekReturRWJ').getValue(),
		KdUnit: Ext.getCmp('txtTmpKdUnitApotekReturRWJL').getValue(),
		KdDokter:Ext.getCmp('txtTmpKdDokterApotekReturRWJL').getValue(),
		NoResepAsal:ReturRWJ.form.ComboBox.noResep.getValue(),
		NoOutAsal:Ext.getCmp('txtTmpNooutLamaApotekReturRWJL').getValue(),
		TglOutAsal:Ext.getCmp('txtTmpTgloutLamaApotekReturRWJL').getValue(),
		StatusPost:Ext.getCmp('txtTmpStatusPostReturRWIL').getValue(),
		Tanggal:tanggal,
		//DiscountAll:toInteger(Ext.getCmp('txtTmpDiscAllReturRWIL').getValue()),
		Kdcustomer:KdCust,
		NoTransaksiAsal:Ext.getCmp('txtTmpNoTrApotekReturRWJL').getValue(),
		KdKasirAsal:Ext.getCmp('txtTmpKdKasirApotekReturRWJL').getValue(),
		SubTotal:toInteger(Ext.getCmp('txtDRGridJmlEditData_viApotekReturRWJ').getValue()),
		Total:toInteger(Ext.getCmp('txtTotalEditData_viApotekReturRWJ').getValue()),
		JumlahItem:Ext.getCmp('txtTmpTotQtyApotekReturRWJL').getValue(),
		Shift: tampungshiftsekarang,
		Ubah:ubah,
		Posting:0
		
	};
	
	params['jumlah']=dsDataGrdJab_viApotekReturRWJ.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viApotekReturRWJ.getCount();i++)
	{
		params['kd_prd-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.kd_prd
		params['nama_obat-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.nama_obat
		params['kd_satuan-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.kd_satuan
		params['harga_jual-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.harga_jual
		params['harga_beli-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.harga_beli
		params['jml-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.jml
		params['reduksi-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.reduksi
		params['no_out-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.no_out
		params['no_urut-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.no_urut
		params['tgl_out-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.tgl_out
		params['kd_milik-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.kd_milik
		params['kd_pabrik-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.kd_pabrik
		params['qty-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.qty
		params['dosis-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.dosis
		params['markup-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.markup
		params['kd_milik-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.kd_milik
		
	}
	
    return params
};

function getParamBayarReturRWJ() 
{
	var KdCust='';
	var TmpCustoLama='';
	var tmpNonResep='';
	var tmpNamaPasien='';
	if(Ext.get('txtTmpKdCustomerApotekReturRWJL').getValue()=='Perseorangan'){
			KdCust='0000000001';
	}else if(Ext.get('txtTmpKdCustomerApotekReturRWJL').getValue()=='Perusahaan'){
		KdCust=Ext.getCmp('txtTmpKdCustomerApotekReturRWJL').getValue();
	}else {
		KdCust=Ext.getCmp('txtTmpKdCustomerApotekReturRWJL').getValue();
	}
	
	var ubah=0;
	if(Ext.getCmp('txtNoReturRWJ_viApotekReturRWJ').getValue() === '' || Ext.getCmp('txtNoReturRWJ_viApotekReturRWJ').getValue()=='No Retur'){
		ubah=0;
	} else{
		ubah=1;
	}
	
	//Pembayaran
	var LangsungPost = 0;
	var tmpNoretur='';
	if(Ext.getCmp('txtNoReturRWJ_Pembayaran').getValue() === '' || Ext.getCmp('txtNoReturRWJ_Pembayaran').getValue() === 'No Retur' ){
		LangsungPost = 1;
		tmpNoretur='';
	}else {
		LangsungPost = 0;
		tmpNoretur = Ext.getCmp('txtNoReturRWJ_Pembayaran').getValue();
	}
	
    var params =
	{
		NoRetur:tmpNoretur,
		NmPasien:Ext.getCmp('txtNamaRWJ_viApotekReturRWJ').getValue(),
		KdUnit: Ext.getCmp('txtTmpKdUnitApotekReturRWJL').getValue(),
		KdDokter:Ext.getCmp('txtTmpKdDokterApotekReturRWJL').getValue(),
		NoResepAsal:ReturRWJ.form.ComboBox.noResep.getValue(),
		NoOutAsal:Ext.getCmp('txtTmpNooutApotekReturRWJL').getValue(),
		TglOutAsal:Ext.getCmp('txtTmpTgloutApotekReturRWJL').getValue(),
		StatusPost:Ext.getCmp('txtTmpStatusPostReturRWIL').getValue(),
		Tanggal:tanggal,
		//DiscountAll:toInteger(Ext.getCmp('txtTmpDiscAllReturRWIL').getValue()),
		Kdcustomer:KdCust,
		NoTransaksiAsal:Ext.getCmp('txtTmpNoTrApotekReturRWJL').getValue(),
		KdKasirAsal:Ext.getCmp('txtTmpKdKasirApotekReturRWJL').getValue(),
		SubTotal:toInteger(Ext.getCmp('txtDRGridJmlEditData_viApotekReturRWJ').getValue()),
		Total:toInteger(Ext.getCmp('txtTotalEditData_viApotekReturRWJ').getValue()),
		JumlahItem:Ext.getCmp('txtTmpTotQtyApotekReturRWJL').getValue(),
		Shift: tampungshiftsekarang,
		Ubah:ubah,
		Reduksi:Ext.getCmp('txtReduksiEditData_viApotekReturRWJ').getValue(),
		
		
		//Pembayaran
		KdPasien:Ext.getCmp('txtkdPasien_PembayaranReturRWJ').getValue(),
		KdPay:kd_pay_retur,
		JumlahTotal:toInteger(Ext.getCmp('txtTotalReturRWJ_Pembayaran').getValue()),
		JumlahTerimaUang:toInteger(Ext.getCmp('txtBayarReturRWJ_Pembayaran').getValue()),
		TanggalBayar:Ext.getCmp('dftanggalReturRWJ_Pembayaran').getValue(),
		Posting:LangsungPost
		
	};
	
	params['jumlah']=dsDataGrdJab_viApotekReturRWJ.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viApotekReturRWJ.getCount();i++)
	{
		params['kd_prd-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.kd_prd
		params['nama_obat-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.nama_obat
		params['kd_satuan-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.kd_satuan
		params['harga_jual-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.harga_jual
		params['harga_beli-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.harga_beli
		params['jml-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.jml
		params['reduksi-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.reduksi
		params['no_out-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.no_out
		params['no_urut-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.no_urut
		params['tgl_out-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.tgl_out
		params['kd_milik-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.kd_milik
		params['kd_pabrik-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.kd_pabrik
		params['qty-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.qty
		params['dosis-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.dosis
		params['markup-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.markup
		params['kd_milik-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.kd_milik
		
	}
	
    return params
};

function getParamUpdateMrObat(){
	var params =
	{
		kd_pasien: Ext.getCmp('txtTmpKdPasienApotekReturRWJL').getValue(),
		kd_unit: Ext.getCmp('txtTmpKdUnitApotekReturRWJL').getValue(),
		no_transaksi: Ext.getCmp('txtTmpNoTrApotekReturRWJL').getValue(),
		kd_kasir: Ext.getCmp('txtTmpKdKasirApotekReturRWJL').getValue(),
		
	}
	params['jumlah']=dsDataGrdJab_viApotekReturRWJ.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viApotekReturRWJ.getCount();i++)
	{
		params['kd_prd-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.kd_prd
		params['tgl_out-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.tgl_out
		params['qty-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.qty
	}
	
	return params
}

function getParamUnPostingMrObat(){
	var params =
	{
		kd_pasien: Ext.getCmp('txtTmpKdPasienApotekReturRWJL').getValue(),
		kd_unit: Ext.getCmp('txtTmpKdUnitApotekReturRWJL').getValue(),
		no_transaksi: Ext.getCmp('txtTmpNoTrApotekReturRWJL').getValue(),
		kd_kasir: Ext.getCmp('txtTmpKdKasirApotekReturRWJL').getValue(),
	}
	params['jumlah']=dsDataGrdJab_viApotekReturRWJ.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viApotekReturRWJ.getCount();i++)
	{
		params['kd_prd-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.kd_prd
		params['tgl_out-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.tgl_out
		params['qty-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.qty
	}
	
	return params
}

function getParamUnpostingReturRWJ() 
{
	var params =
	{
		NoResep:Ext.getCmp('txtNoReturRWJ_viApotekReturRWJ').getValue(),
		NoOut:Ext.getCmp('txtTmpNooutApotekReturRWJL').getValue(),
		TglOut:Ext.getCmp('txtTmpTgloutApotekReturRWJL').getValue()
	}
	
	params['jumlah']=dsDataGrdJab_viApotekReturRWJ.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viApotekReturRWJ.getCount();i++)
	{
		params['kd_prd-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.kd_prd
		params['nama_obat-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.nama_obat
		params['kd_satuan-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.kd_satuan
		params['harga_jual-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.harga_jual
		params['harga_beli-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.harga_beli
		params['kd_pabrik-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.kd_pabrik
		params['jml-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.jml
		params['qty-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.qty
		params['markup-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.markup
		params['reduksi-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.reduksi
		params['dosis-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.dosis
		params['no_urut-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.no_urut
		params['kd_milik-'+i]=dsDataGrdJab_viApotekReturRWJ.data.items[i].data.kd_milik
	}
	
    return params
}

function getCriteriaCariApotekReturRWJ()//^^^
{
      	 var strKriteria = "";

           if (Ext.get('TxtFilterGridDataView_RoNumber_viApotekReturRWJ').getValue() != "" && Ext.get('TxtFilterGridDataView_RoNumber_viApotekReturRWJ').getValue()!=='No. Retur')
            {
                strKriteria = " o.no_resep " + "LIKE upper('%" + Ext.get('TxtFilterGridDataView_RoNumber_viApotekReturRWJ').getValue() +"%')";
            }
            
            if (Ext.get('txtKdNamaPasienReturRWJ').getValue() != "" && Ext.get('txtKdNamaPasienReturRWJ').getValue() !== 'Kode/Nama Pasien')//^^^
            {
				if(Ext.get('txtKdNamaPasienReturRWJ').getValue().substring(0,1)==='0'){
					if (strKriteria == "")
                    {
                        strKriteria = " o.kd_pasienapt " + "LIKE upper('" + Ext.get('txtKdNamaPasienReturRWJ').getValue() +"%')" ;
                    }
                    else {
						strKriteria += " and o.kd_pasienapt " + "LIKE upper('" + Ext.get('txtKdNamaPasienReturRWJ').getValue() +"%')";
					}
				} else {
					 if (strKriteria == "")
                    {
                        strKriteria = " upper(o.nmpasien) " + "LIKE upper('" + Ext.get('txtKdNamaPasienReturRWJ').getValue() +"%')" ;
                    }
                    else {
						strKriteria += " and upper(o.nmpasien) " + "LIKE upper('" + Ext.get('txtKdNamaPasienReturRWJ').getValue() +"%')";
					}
				}
            }
			if (Ext.get('cboStatusPostingApotekReturRWJ').getValue() != "")
            {
				if (Ext.get('cboStatusPostingApotekReturRWJ').getValue()==='Belum Posting')
				{
					if (strKriteria == "")
                    {
                        strKriteria = " o.tutup " + "= 0" ;
                    }
                    else {
						strKriteria += " and o.tutup " + "= 0";
				    }
				}
				if (Ext.get('cboStatusPostingApotekReturRWJ').getValue()==='Posting')
				{
					if (strKriteria == "")
                    {
                        strKriteria = " o.tutup " + "=1" ;
                    }
                    else {
						strKriteria += " and o.tutup " + "=1";
				    }
				}
				if (Ext.get('cboStatusPostingApotekReturRWJ').getValue()==='Semua')
				{
					
				}
                 
            }
			if (Ext.get('dfTglAwalApotekReturRWJ').getValue() != "" && Ext.get('dfTglAkhirApotekReturRWJ').getValue() != "")
            {
                if (strKriteria == "")
				{
					strKriteria = " o.tgl_out between '" + Ext.get('dfTglAwalApotekReturRWJ').getValue() + "' and '" + Ext.get('dfTglAkhirApotekReturRWJ').getValue() + "'" ;
				}
				else {
					strKriteria += " and o.tgl_out between '" + Ext.get('dfTglAwalApotekReturRWJ').getValue() + "' and '" + Ext.get('dfTglAkhirApotekReturRWJ').getValue() + "'" ;
				}
                
            }
			
			if (Ext.get('cbo_UnitReturRWJ').getValue() != "" && Ext.get('cbo_UnitReturRWJ').getValue() != 'Poliklinik')
            {
				if (strKriteria == "")
				{
					strKriteria = " o.kd_unit ='" + Ext.getCmp('cbo_UnitReturRWJ').getValue() + "'"  ;
				}
				else {
					strKriteria += " and o.kd_unit ='" + Ext.getCmp('cbo_UnitReturRWJ').getValue() + "'";
			    }
                
            }
	 strKriteria = strKriteria +" and left(o.kd_unit,1)<>'1' and returapt=1 and o.kd_unit_far='" + UnitFarAktif_ReturRWJ + "' order by o.tgl_out limit 20"
	 return strKriteria;
}

function ValidasiEntryReturRWJ(modul,mBolHapus)
{
	var x = 1;
	if((Ext.getCmp('txtNamaRWJ_viApotekReturRWJ').getValue() === '' || Ext.getCmp('txtNamaRWJ_viApotekReturRWJ').getValue() === 'Nama Pasien') || 
		dsDataGrdJab_viApotekReturRWJ.getCount() === 0 || (ReturRWJ.form.ComboBox.noResep.getValue() === '' || ReturRWJ.form.ComboBox.noResep.getValue() === 'No Resep')){
		if(Ext.getCmp('txtNamaRWJ_viApotekReturRWJ').getValue() === '' || Ext.getCmp('txtNamaRWJ_viApotekReturRWJ').getValue() === 'Nama Pasien'){
			ShowPesanWarningReturRWJ('Nama pasien kosong, mohon isi no resep dengan benar', 'Warning');
			x = 0;
		} else if(dsDataGrdJab_viApotekResepRWI.getCount() == 0){
			ShowPesanWarningReturRWJ('Daftar resep obat belum di isi', 'Warning');
			x = 0;
		} else {
			ShowPesanWarningReturRWJ('No Resep belum di isi', 'Warning');
			x = 0;
		} 
	}
	
	if((Ext.getCmp('txtDRGridJmlEditData_viApotekReturRWJ').getValue() === '0' || Ext.getCmp('txtDRGridJmlEditData_viApotekReturRWJ').getValue() === 0) ||
		(Ext.getCmp('txtTotalEditData_viApotekReturRWJ').getValue() === '0' || Ext.getCmp('txtTotalEditData_viApotekReturRWJ').getValue() === 0)){
		if(Ext.getCmp('txtDRGridJmlEditData_viApotekReturRWJ').getValue() === '0' || Ext.getCmp('txtDRGridJmlEditData_viApotekReturRWJ').getValue() === 0){
			ShowPesanWarningReturRWJ('Total harga obat kosong, harap periksa kelengkapan pengisian obat', 'Warning');
			x = 0;
		} else{
			ShowPesanWarningReturRWJ('Grand total bayar kosong, harap periksa kelengkapan pengisian obat', 'Warning');
			x = 0;
		}
	}
	
	for(var i=0; i<dsDataGrdJab_viApotekReturRWJ.getCount() ; i++){
		var o=dsDataGrdJab_viApotekReturRWJ.getRange()[i].data;
		if(o.qty == undefined || o.jumlah == undefined || o.qty == 0){
			if(o.qty == undefined){
				ShowPesanWarningReturRWJ('Qty yang di retur belum di isi, qty tidak boleh kosong', 'Warning');
				x = 0;
			}else if(o.jumlah == undefined){
				ShowPesanWarningReturRWJ('Sub total obat kosong, harap periksa qty dan kelengkapan pengisian obat', 'Warning');
				x = 0;
			} else if(o.qty == 0){
				ShowPesanWarningReturRWJ('Qty tidak boleh 0, qty tidak boleh kosong', 'Warning');
				x = 0;
			}
		}
		if(o.qty > o.jml){
			ShowPesanWarningReturRWJ('Qty retur melebihi jumlah pembelian resep', 'Warning');
			o.qty=o.jml;
			x = 0;
		}
	}
	
	return x;
};

function ValidasiBayarReturRWJ(modul,mBolHapus)
{
	var x = 1;
	if(Ext.getCmp('cboJenisByrReturRWJ').getValue() === '' || Ext.getCmp('cboJenisByrReturRWJ').getValue() === 'Jenis Bayar'){
		ShowPesanWarningReturRWJ('Pembayaran belum dipilih', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('cboPembayaranReturRWJ').getValue() === '' || Ext.getCmp('cboPembayaranReturRWJ').getValue() === 'Pilih Pembayaran...'){
		ShowPesanWarningReturRWJ('Jenis pembayaran belum dipilih', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('txtBayarReturRWJ_Pembayaran').getValue() === ''){
		ShowPesanWarningReturRWJ('Jumlah pembayaran belum di isi', 'Warning');
		x = 0;
	}
	
	return x;
};


function ShowPesanWarningReturRWJ(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorReturRWJ(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:290
		}
	);
};


function ShowPesanInfoReturRWJ(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};

function load_data_printer_resep_rwi(param)
{

	Ext.Ajax.request(
	{
		url: baseURL + "index.php/apotek/functionApotekReturRWJ/group_printer",
		params:{
			kriteria: 'apt_printer_bill_'+UnitFarAktif_ReturRWJ
		} ,
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
		success: function(o) {
			cbopasienorder_printer_retur_rwi.store.removeAll();
				var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsprinter_retur_rwi.recordType;
				var o=cst['listData'][i];
				
				recs.push(new recType(o));
				dsprinter_retur_rwi.add(recs);
				console.log(o);
			}
		}
	});
}

function mCombo_printer_retur_rwj()
{ 
 
	var Field = ['alamat_printer'];
    dsprinter_retur_rwi = new WebApp.DataStore({ fields: Field });
	load_data_printer_resep_rwi();
	cbopasienorder_printer_retur_rwi= new Ext.form.ComboBox
	(
		{
			id: 'cbopasienorder_printer_retur_rwi',
			typeAhead		: true,
			triggerAction	: 'all',
			lazyRender		: true,
			mode			: 'local',
			emptyText: '',
			fieldLabel:  'Daftar Printer',
			align: 'Right',
			//width: 100,
			anchor:'100%',
			store: dsprinter_retur_rwi,
			valueField: 'alamat_printer',
			displayField: 'alamat_printer',
			//hideTrigger		: true,
			listeners:
			{
								
			}
		}
	);return cbopasienorder_printer_retur_rwi;
};

function panelnew_window_printer_ReturRWJ()
{
    win_printer_retur_rwj = new Ext.Window
    (
        {
            id: 'win_printer_retur_rwj',
            title: 'Printer',
            closeAction: 'destroy',
            width:320,
            height: 120,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [Itempanel_printer_returRWI()],
			fbar:[
				{
					xtype: 'button',
					text: nmBtnOK,
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnOkPrinterBill_ReturRWJ',
					handler: function()
					{		
						if (Ext.getCmp('cbopasienorder_printer_retur_rwi').getValue()===""){
							ShowPesanWarningResepRWJ('Pilih dulu print sebelum cetak', 'Warning');
						}else{
							printbill_retur_rwj();	
							win_printer_retur_rwj.close();
						} 
					}
				},
				{
					xtype: 'button',
					text: nmBtnCancel ,
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnCancelLapRWJPasswordDulu',
					handler: function()
					{
							win_printer_retur_rwj.close();
					}
				} 
			]

        }
    );

    win_printer_retur_rwj.show();
};

function Itempanel_printer_returRWI()
{
    var panel_printer_returRWI = new Ext.Panel
    (
        {
            id: 'panel_printer_returRWI',
            fileUpload: true,
            layout: 'form',
            width:170,
            height: 120,
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                mCombo_printer_retur_rwj(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                    anchor: '100%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                        
                    ]
                }
            ]
        }
    );

    return panel_printer_returRWI;
};

function dataaddnew_viApotekReturRWJ(){
	Ext.getCmp('txtNoReturRWJ_viApotekReturRWJ').setValue('');
	Ext.getCmp('cbo_UnitLookupReturRWJ').setValue('');
	Ext.getCmp('txtNamaRWJ_viApotekReturRWJ').setValue('');
	Ext.getCmp('txtDokterRWJ_viApotekReturRWJ').setValue('');
	Ext.getCmp('txtJenisPasienRWJ_viApotekReturRWJ').setValue('');
	
	Ext.getCmp('txtTmpKdCustomerApotekReturRWJL').setValue('');
	Ext.getCmp('txtTmpKdDokterApotekReturRWJL').setValue('');
	Ext.getCmp('txtTmpKdUnitApotekReturRWJL').setValue('');
	Ext.getCmp('txtTmpNooutApotekReturRWJL').setValue('');
	Ext.getCmp('txtTmpTgloutApotekReturRWJL').setValue('');
	Ext.getCmp('txtTmpKdUnitFarApotekReturRWJL').setValue('');
	Ext.getCmp('txtTmpKdPasienApotekReturRWJL').setValue('');
	Ext.getCmp('txtTmpNoTrApotekReturRWJL').setValue('');
	Ext.getCmp('txtTmpKdKasirApotekReturRWJL').setValue('');
	Ext.getCmp('txtTmpStatusPostReturRWIL').setValue('');
	
	Ext.getCmp('txtDRGridJmlEditData_viApotekReturRWJ').setValue(0);
	Ext.getCmp('txtReduksiEditData_viApotekReturRWJ').setValue(0);
	Ext.getCmp('txtTotalEditData_viApotekReturRWJ').setValue(0);
	Ext.getCmp('txtTmpTotQtyApotekReturRWJL').setValue(0);
	
	
	ReturRWJ.form.ComboBox.noResep.setValue('');
	ReturRWJ.form.ComboBox.noResep.setReadOnly(false);
	
	Ext.getCmp('btnHapusResepReturRWJ').enable();
	Ext.getCmp('btnunposting_viApotekReturRWJ').disable();
	
	dsDataGrdJab_viApotekReturRWJ.removeAll();
	dsTRDetailHistoryBayarList.removeAll();
	ReturRWJ.form.Panel.a.update('<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>');

};

function getUnitFar_ReturRWJ(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionApotekReturRWJ/getUnitFar",
			params: {query:''},
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorReturRWJ('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					UnitFarAktif_ReturRWJ = cst.kd_unit_far;
				}
				else 
				{
					loadMask.hide();
					ShowPesanErrorReturRWJ('Gagal membaca unitfar', 'Error');
				};
			}
		}
		
	)
	
}

function getKdPayRetur_ReturRWJ(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionApotekReturRWJ/getKdpay",
			params: {
				query:''
			},
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorReturRWJ('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					kd_pay_retur = cst.kd_pay;
					Ext.getCmp('cboPembayaranReturRWJ').setValue(cst.payment);
					Ext.getCmp('cboJenisByrReturRWJ').setValue(cst.jenis_pay);
					loaddatastorePembayaran(cst.jenis_pay);
				}
				else 
				{
					loadMask.hide();
					ShowPesanErrorReturRWJ('Gagal membaca unitfar', 'Error');
				};
			}
		}
		
	)
	
}



