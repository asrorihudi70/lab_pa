var dataSource_viSetupPabrik;
var selectCount_viSetupPabrik=50;
var NamaForm_viSetupPabrik="Setup Pabrik";
var mod_name_viSetupPabrik="Setup Pabrik";
var now_viSetupPabrik= new Date();
var rowSelected_viSetupPabrik;
var setLookUps_viSetupPabrik;
var tanggal = now_viSetupPabrik.format("d/M/Y");
var jam = now_viSetupPabrik.format("H/i/s");
var tmpkriteria;
var DataGridJenisObat;
var GridDataView_viSetupPabrik;


var CurrentData_viSetupPabrik =
{
	data: Object,
	details: Array,
	row: 0
};



var SetupPabrik={};
SetupPabrik.form={};
SetupPabrik.func={};
SetupPabrik.vars={};
SetupPabrik.func.parent=SetupPabrik;
SetupPabrik.form.ArrayStore={};
SetupPabrik.form.ComboBox={};
SetupPabrik.form.DataStore={};
SetupPabrik.form.Record={};
SetupPabrik.form.Form={};
SetupPabrik.form.Grid={};
SetupPabrik.form.Panel={};
SetupPabrik.form.TextField={};
SetupPabrik.form.Button={};

SetupPabrik.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_pabrik', 'pabrik', 'contact', 'alamat', 'kota', 
			'telepon1', 'telepon2', 'fax', 'kd_pos', 'negara', 'term', 'norek'],
	data: []
});
CurrentPage.page = dataGrid_viSetupPabrik(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
shortcut.set({
	code:'main',
	list:[
		{
			key:'ctrl+s',
			fn:function(){
				Ext.getCmp('btnSimpan_viSetupPabrik').el.dom.click();
			}
		},
		{
			key:'ctrl+d',
			fn:function(){
				Ext.getCmp('btnDelete_viSetupPabrik').el.dom.click();
			}
		}
	]
});
function dataGrid_viSetupPabrik(mod_id_viSetupPabrik){	
 	// Field kiriman dari Project Net.
    var FieldMaster_viSetupPabrik = 
	[
		'kd_pabrik', 'pabrik', 'contact', 'alamat', 'kota', 
		'telepon1', 'telepon2', 'fax', 'kd_pos', 'negara', 'term', 'norek'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viSetupPabrik = new WebApp.DataStore
	({
        fields: FieldMaster_viSetupPabrik
    });
    dataGriSetupPabrik();
    // Grid Apotek Perencanaan # --------------
	GridDataView_viSetupPabrik = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viSetupPabrik,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:true,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viSetupPabrik = undefined;
							rowSelected_viSetupPabrik = dataSource_viSetupPabrik.getAt(row);
							CurrentData_viSetupPabrik
							CurrentData_viSetupPabrik.row = row;
							CurrentData_viSetupPabrik.data = rowSelected_viSetupPabrik.data;
							//DataInitSetupPabrik(rowSelected_viSetupPabrik.data);
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viSetupPabrik = dataSource_viSetupPabrik.getAt(ridx);
					if (rowSelected_viSetupPabrik != undefined)
					{
						DataInitSetupPabrik(rowSelected_viSetupPabrik.data);
						//setLookUp_viSetupPabrik(rowSelected_viSetupPabrik.data);
					}
					else
					{
						//setLookUp_viSetupPabrik();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Setup pabrik
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colKode_viSetupPabrik',
						header: 'Kode',
						dataIndex: 'kd_pabrik',
						hideable:false,
						menuDisabled: true,
						width: 40
						
					},
					//-------------- ## --------------
					{
						id: 'colpabrik_viSetupPabrik',
						header: 'Nama pabrik',
						dataIndex: 'pabrik',
						hideable:false,
						menuDisabled: true,
						width: 90
					},
					//-------------- ## --------------
					{
						id: 'colContact_viSetupPabrik',
						header: 'Contact',
						dataIndex: 'contact',
						hideable:false,
						menuDisabled: true,
						width: 50
						
					},
					//-------------- ## --------------
					{
						id: 'colAlamat_viSetupPabrik',
						header:'Alamat pabrik',
						dataIndex: 'alamat',						
						width: 130,
						hideable:false,
                        menuDisabled:true
					},
					//-------------- ## --------------
					{
						id: 'colKota_viSetupPabrik',
						header: 'Kota',
						dataIndex: 'kota',
						hideable:false,
                        menuDisabled:true,
						width: 50
					},
					//-------------- ## --------------
					{
						id: 'colNorek_viSetupPabrik',
						header: 'Norek',
						dataIndex: 'norek',
						hideable:false,
						menuDisabled: true,
						width: 50
					}
					//-------------- ## --------------
					
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viSetupPabrik',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viSetupPabrik',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viSetupPabrik != undefined)
							{
								DataInitSetupPabrik(rowSelected_viSetupPabrik.data);
							}
							
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viSetupPabrik, selectCount_viSetupPabrik, dataSource_viSetupPabrik),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)	
	var PanelTabSetupPabrik = new Ext.FormPanel({
        labelAlign: 'top',
		layout:'form',
		border: false,  
		autoHeight:true,
        title: '',
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [PanelInputpabrik()]
		
	})
	// Kriteria filter pada Grid # --------------
    var FrmData_viSetupPabrik = new Ext.Panel
    (
		{
			title: NamaForm_viSetupPabrik,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viSetupPabrik,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ PanelTabSetupPabrik,
					GridDataView_viSetupPabrik],
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAdd_viSetupPabrik',
						handler: function(){
							AddNewSetupPabrik();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viSetupPabrik',
						handler: function()
						{
							loadMask.show();
							dataSave_viSetupPabrik();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viSetupPabrik',
						handler: function()
						{
							Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
								if (button == 'yes'){
									loadMask.show();
									dataDelete_viSetupPabrik();
								}
							});
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_viSetupPabrik',
						handler: function()
						{
							dataSource_viSetupPabrik.removeAll();
							dataGriSetupPabrik();
						}
					},
					{
						xtype: 'tbseparator'
					}
					
				]
			}
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_viSetupPabrik;
    //-------------- # End form filter # --------------
}

function PanelInputpabrik(rowdata){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 170,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'Kode Pabrik'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 0,
								xtype: 'textfield',
								id: 'txtKdPabrik_SetupPabrik',
								name: 'txtKdPabrik_SetupPabrik',
								width: 130,
								readOnly:true,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									}
								}
							},
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'Nama pabrik'
							},
							{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							SetupPabrik.vars.nama=new Nci.form.Combobox.autoComplete({
								x: 130,
								y: 30,
								tabIndex:2,
								id		: 'txtComboNama_SetupPabrik',
								store	: SetupPabrik.form.ArrayStore.a,
								select	: function(a,b,c){
									
									Ext.getCmp('txtKdPabrik_SetupPabrik').setValue(b.data.kd_pabrik);
									Ext.getCmp('txtKontak_SetupPabrik').setValue(b.data.contact);
									Ext.getCmp('txtAlamat_SetupPabrik').setValue(b.data.alamat);
									Ext.getCmp('txtKota_SetupPabrik').setValue(b.data.kota);
									Ext.getCmp('txtKodePos_SetupPabrik').setValue(b.data.kd_pos);
									Ext.getCmp('txtNegara_SetupPabrik').setValue(b.data.negara);
									Ext.getCmp('txtTelepon_SetupPabrik').setValue(b.data.telepon1);
									Ext.getCmp('txtTelepon2_SetupPabrik').setValue(b.data.telepon2);
									Ext.getCmp('txtFax_SetupPabrik').setValue(b.data.fax);
									Ext.getCmp('txtNorek_SetupPabrik').setValue(b.data.norek);
									Ext.getCmp('txtTempo_SetupPabrik').setValue(b.data.term);
									
									GridDataView_viSetupPabrik.getView().refresh();
									
								},
								onShowList:function(a){
									dataSource_viSetupPabrik.removeAll();
									
									var recs=[],
									recType=dataSource_viSetupPabrik.recordType;
										
									for(var i=0; i<a.length; i++){
										recs.push(new recType(a[i]));
									}
									dataSource_viSetupPabrik.add(recs);
									
								},
								insert	: function(o){
									return {
										kd_pabrik      	: o.kd_pabrik,
										pabrik 			: o.pabrik,
										contact			: o.contact,
										alamat			: o.alamat,
										kota			: o.kota,
										telepon1		: o.telepon1,
										telepon2		: o.telepon2,
										fax				: o.fax,
										kd_pos			: o.kd_pos,
										negara			: o.negara,
										term			: o.term,
										norek			: o.norek,
										text			:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_pabrik+'</td><td width="200">'+o.pabrik+'</td></tr></table>'
									}
								},
								url		: baseURL + "index.php/apotek/functionSetupPabrik/getPabrikGrid",
								valueField: 'pabrik',
								displayField: 'text',
								listWidth: 280
							}),
							{
								x: 10,
								y: 60,
								xtype: 'label',
								text: 'Kontak Person'
							},
							{
								x: 120,
								y: 60,
								xtype: 'label',
								text: ':'
							},
							{
								x:130,
								y:60,	
								xtype: 'textfield',
								name: 'txtKontak_SetupPabrik',
								id: 'txtKontak_SetupPabrik',
								tabIndex:3,
								width: 180
							},
							{
								x: 10,
								y: 90,
								xtype: 'label',
								text: 'Alamat'
							},
							{
								x: 120,
								y: 90,
								xtype: 'label',
								text: ':'
							},
							{
								x:130,
								y:90,	
								xtype: 'textarea',
								name: 'txtAlamat_SetupPabrik',
								id: 'txtAlamat_SetupPabrik',
								width : 280,
								height : 50,
								tabIndex:4
							},
							{
								x: 10,
								y: 148,
								xtype: 'label',
								text: 'Kota'
							},
							{
								x: 120,
								y: 148,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 148,
								xtype: 'textfield',
								name: 'txtKota_SetupPabrik',
								id: 'txtKota_SetupPabrik',
								tabIndex:5,
								width: 150
							},
							
							//-------------- ## --------------
							{
								x: 420,
								y: 0,
								xtype: 'label',
								text: 'Kode Pos'
							},
							{
								x: 510,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 520,
								y: 0,
								xtype: 'numberfield',
								name: 'txtKodePos_SetupPabrik',
								id: 'txtKodePos_SetupPabrik',
								tabIndex:6,
								width: 70
							},
							{
								x: 420,
								y: 30,
								xtype: 'label',
								text: 'Negara'
							},
							{
								x: 510,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							{
								x: 520,
								y: 30,
								xtype: 'textfield',
								name: 'txtNegara_SetupPabrik',
								id: 'txtNegara_SetupPabrik',
								tabIndex:7,
								width: 150
							},
							{
								x: 420,
								y: 60,
								xtype: 'label',
								text: 'Telepon'
							},
							{
								x: 510,
								y: 60,
								xtype: 'label',
								text: ':'
							},
							{
								x: 520,
								y: 60,
								xtype: 'textfield',
								name: 'txtTelepon_SetupPabrik',
								id: 'txtTelepon_SetupPabrik',
								tabIndex:8,
								width: 100
							},
							{
								x: 630,
								y: 60,
								xtype: 'textfield',
								name: 'txtTelepon2_SetupPabrik',
								id: 'txtTelepon2_SetupPabrik',
								tabIndex:9,
								width: 100
							},
							{
								x: 740,
								y: 60,
								xtype: 'label',
								text: 'Fax'
							},
							{
								x: 770,
								y: 60,
								xtype: 'label',
								text: ':'
							},
							{
								x: 780,
								y: 60,
								xtype: 'textfield',
								name: 'txtFax_SetupPabrik',
								id: 'txtFax_SetupPabrik',
								tabIndex:10,
								width: 100
							},
							{
								x: 420,
								y: 90,
								xtype: 'label',
								text: 'No Rek Bank'
							},
							{
								x: 510,
								y: 90,
								xtype: 'label',
								text: ':'
							},
							{
								x: 520,
								y: 90,
								xtype: 'textfield',
								name: 'txtNorek_SetupPabrik',
								id: 'txtNorek_SetupPabrik',
								tabIndex:11,
								width: 210
							},
							{
								x: 420,
								y: 120,
								xtype: 'label',
								text: 'Jatuh Tempo'
							},
							{
								x: 510,
								y: 120,
								xtype: 'label',
								text: ':'
							},
							{
								x: 520,
								y: 120,
								xtype: 'numberfield',
								name: 'txtTempo_SetupPabrik',
								id: 'txtTempo_SetupPabrik',
								tabIndex:12,
								width: 70
							}
							
							
						]
					}
				]
			}
		]		
	};
        return items;
}
//------------------end---------------------------------------------------------


function dataGriSetupPabrik(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionSetupPabrik/getPabrikGrid",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorSetupPabrik('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dataSource_viSetupPabrik.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dataSource_viSetupPabrik.add(recs);
					
					
					
					GridDataView_viSetupPabrik.getView().refresh();
				}
				else 
				{
					ShowPesanErrorSetupPabrik('Gagal membaca data obat', 'Error');
				};
			}
		}
		
	)
	
}

function dataSave_viSetupPabrik(){
	if (ValidasiSaveSetupPabrik(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionSetupPabrik/save",
				params: getParamSaveSetupPabrik(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupPabrik('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupPabrik('Berhasil menyimpan data ini','Information');
						Ext.getCmp('txtKdPabrik_SetupPabrik').setValue(cst.kdpabrik);
						dataSource_viSetupPabrik.removeAll();
						dataGriSetupPabrik();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupPabrik('Gagal menyimpan data ini', 'Error');
						dataSource_viSetupPabrik.removeAll();
						dataGriSetupPabrik();
					};
				}
			}
			
		)
	}
}

function dataDelete_viSetupPabrik(){
	if (ValidasiSaveSetupPabrik(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionSetupPabrik/delete",
				params: getParamDeleteSetupPabrik(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupPabrik('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupPabrik('Berhasil menghapus data ini','Information');
						AddNewSetupPabrik()
						dataSource_viSetupPabrik.removeAll();
						dataGriSetupPabrik();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupPabrik('Gagal menghapus data ini', 'Error');
						dataSource_viSetupPabrik.removeAll();
						dataGriSetupPabrik();
					};
				}
			}
			
		)
	}
}

function AddNewSetupPabrik(){
	Ext.getCmp('txtKdPabrik_SetupPabrik').setValue('');
	SetupPabrik.vars.nama.setValue('');
	Ext.getCmp('txtKontak_SetupPabrik').setValue('');
	Ext.getCmp('txtAlamat_SetupPabrik').setValue('');
	Ext.getCmp('txtKota_SetupPabrik').setValue('');
	Ext.getCmp('txtKodePos_SetupPabrik').setValue('');
	Ext.getCmp('txtNegara_SetupPabrik').setValue('');
	Ext.getCmp('txtTelepon_SetupPabrik').setValue('');
	Ext.getCmp('txtTelepon2_SetupPabrik').setValue('');
	Ext.getCmp('txtFax_SetupPabrik').setValue('');
	Ext.getCmp('txtNorek_SetupPabrik').setValue('');
	Ext.getCmp('txtTempo_SetupPabrik').setValue('');
};

function DataInitSetupPabrik(rowdata){
	Ext.getCmp('txtKdPabrik_SetupPabrik').setValue(rowdata.kd_pabrik);
	SetupPabrik.vars.nama.setValue(rowdata.pabrik);
	Ext.getCmp('txtKontak_SetupPabrik').setValue(rowdata.contact);
	Ext.getCmp('txtAlamat_SetupPabrik').setValue(rowdata.alamat);
	Ext.getCmp('txtKota_SetupPabrik').setValue(rowdata.kota);
	Ext.getCmp('txtKodePos_SetupPabrik').setValue(rowdata.kd_pos);
	Ext.getCmp('txtNegara_SetupPabrik').setValue(rowdata.negara);
	Ext.getCmp('txtTelepon_SetupPabrik').setValue(rowdata.telepon1);
	Ext.getCmp('txtTelepon2_SetupPabrik').setValue(rowdata.telepon2);
	Ext.getCmp('txtFax_SetupPabrik').setValue(rowdata.fax);
	Ext.getCmp('txtNorek_SetupPabrik').setValue(rowdata.norek);
	Ext.getCmp('txtTempo_SetupPabrik').setValue(rowdata.term);
};

function getParamSaveSetupPabrik(){
	var term;
	if(Ext.getCmp('txtTempo_SetupPabrik').getValue() === ''){
		term=0;
	} else{
		term=Ext.getCmp('txtTempo_SetupPabrik').getValue();
	}
	var	params =
	{
		KdPabrik:Ext.getCmp('txtKdPabrik_SetupPabrik').getValue(),
		Nama:SetupPabrik.vars.nama.getValue(),
		Kontak:Ext.getCmp('txtKontak_SetupPabrik').getValue(),
		Alamat:Ext.getCmp('txtAlamat_SetupPabrik').getValue(),
		Kota:Ext.getCmp('txtKota_SetupPabrik').getValue(),
		KodePos:Ext.getCmp('txtKodePos_SetupPabrik').getValue(),
		Negara:Ext.getCmp('txtNegara_SetupPabrik').getValue(),
		Telepon:Ext.getCmp('txtTelepon_SetupPabrik').getValue(),
		Telepon2:Ext.getCmp('txtTelepon2_SetupPabrik').getValue(),
		Fax:Ext.getCmp('txtFax_SetupPabrik').getValue(),
		Norek:Ext.getCmp('txtNorek_SetupPabrik').getValue(),
		Tempo:term
	}
   
    return params
};

function getParamDeleteSetupPabrik(){
	var	params =
	{
		KdPabrik:Ext.getCmp('txtKdPabrik_SetupPabrik').getValue()
	}
   
    return params
};

function ValidasiSaveSetupPabrik(modul,mBolHapus){
	var x = 1;
	if(SetupPabrik.vars.nama.getValue() === ''){
		loadMask.hide();
		ShowPesanWarningSetupPabrik('Nama pabrik masih kosong', 'Warning');
		x = 0;
	} 
	return x;
};



function ShowPesanWarningSetupPabrik(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorSetupPabrik(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoSetupPabrik(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};