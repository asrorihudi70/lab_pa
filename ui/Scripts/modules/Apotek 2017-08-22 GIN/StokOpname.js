var dataSource_viStokOpname;
var selectCount_viStokOpname=50;
var NamaForm_viStokOpname="Stok Opname";
var selectCountStatusPostingStokOpname='Semua';
var mod_name_viStokOpname="viStokOpname";
var now_viStokOpname= new Date();
var addNew_viStokOpname;
var rowSelected_viStokOpname;
var setLookUps_viStokOpname;
var tanggal = now_viStokOpname.format("d/M/Y");
var jam = now_viStokOpname.format("H/i/s");
var tampungshiftsekarang;
var tmpkriteria;
var UnitFarAktif;
var rowsdataTransaksiStokOpname;


var CurrentData_viStokOpname =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viStokOpname(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

var StokOpname={};
StokOpname.form={};
StokOpname.func={};
StokOpname.vars={};
StokOpname.func.parent=StokOpname;
StokOpname.form.ArrayStore={};
StokOpname.form.ComboBox={};
StokOpname.form.DataStore={};
StokOpname.form.Record={};
StokOpname.form.Form={};
StokOpname.form.Grid={};
StokOpname.form.Panel={};
StokOpname.form.TextField={};
StokOpname.form.Button={};

StokOpname.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_prd','nama_obat','kd_milik','kd_unit_far','min_stok','milik'],
	data: []
});

function dataGrid_viStokOpname(mod_id_viStokOpname){	
    // Field kiriman dari Project Net.
    var FieldMaster_viStokOpname = 
	[
		'KD_USER','FULL_NAME','NO_SO','TGL_SO','APPROVE','NO_BA_SO','KET_SO','KD_UNIT_FAR','NM_UNIT_FAR'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viStokOpname = new WebApp.DataStore
	({
        fields: FieldMaster_viStokOpname
    });
    refreshStokOpname();
	getUnitFar();
    // Grid Apotek Perencanaan # --------------
	var GridDataView_viStokOpname = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viStokOpname,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viStokOpname = undefined;
							rowSelected_viStokOpname = dataSource_viStokOpname.getAt(row);
							CurrentData_viStokOpname
							CurrentData_viStokOpname.row = row;
							CurrentData_viStokOpname.data = rowSelected_viStokOpname.data;
							rowsdataTransaksiStokOpname = CurrentData_viStokOpname.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viStokOpname = dataSource_viStokOpname.getAt(ridx);
					if (rowSelected_viStokOpname != undefined)
					{
						setLookUp_viStokOpname(rowSelected_viStokOpname.data);
					}
					else
					{
						setLookUp_viStokOpname();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Apotek perencanaan
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header		: 'Status Posting',
						width		: 20,
						sortable	: false,
						hideable	: false,
						hidden		: false,
						menuDisabled: true,
						dataIndex	: 'APPROVE',
						id			: 'colStatusPosting_viStokOpname',
						renderer	: function(value, metaData, record, rowIndex, colIndex, store){
							 switch (value){
								 case 't':
									 metaData.css = 'StatusHijau'; 
									 break;
								 case 'f':
									 metaData.css = 'StatusMerah';
									 break;
							 }
							 return '';
						}
					},
					{
						id: 'colNoSo_viStokOpname',
						header: 'No Stok Opname',
						dataIndex: 'NO_SO',
						hideable:false,
						menuDisabled: true,
						width: 25
						
					},
					//-------------- ## --------------
					{
						id: 'colTgl_viStokOpname',
						header: 'Tanggal SO',
						dataIndex: 'TGL_SO',
						hideable:false,
						menuDisabled: true,
						width: 30,
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.TGL_SO);
						}
					},
					//-------------- ## --------------
					{
						id: 'colNMUNITFAR_viStokOpname',
						header: 'Unit Farmasi',
						dataIndex: 'NM_UNIT_FAR',
						hideable:false,
						menuDisabled: true,
						width: 25
						
					},
					//-------------- ## --------------
					{
						id: 'colNOBA_viStokOpname',
						header: 'No BA',
						dataIndex: 'NO_BA_SO',
						hideable:false,
						menuDisabled: true,
						width: 25
						
					},
					//-------------- ## --------------
					{
						id: 'colNama_viStokOpname',
						header:'Petugas BA',
						dataIndex: 'KET_SO',						
						width: 50,
						hideable:false,
                        menuDisabled:true
					},
					//-------------- ## --------------
					{
						id: 'colSatBesar_viStokOpname',
						header: 'User Login',
						dataIndex: 'FULL_NAME',
						hideable:false,
						align:'right',
                        menuDisabled:true,
						width: 25
					},
					//-------------- ## --------------
					
					//-------------- ## --------------
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viStokOpname',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add Data [F1]',
						iconCls: 'Edit_Tr',
						tooltip: 'Add Data',
						id: 'btnTambah_viStokOpname',
						handler: function(sm, row, rec)
						{
							setLookUp_viStokOpname();
							
						}
					},
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viStokOpname',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viStokOpname != undefined)
							{
								setLookUp_viStokOpname(rowSelected_viStokOpname.data)
							}
							
						}
					},
					{
						xtype: 'button',
						text: 'Hapus Transaksi',
						iconCls: 'remove',
						tooltip: 'btnHapusTransaksiEdit_viStokOpname',
						handler: function(sm, row, rec)
						{
							Ext.Msg.confirm('Konfirmasi', 'Apakah transaksi akan dihapus?', function(button){
								if (button == 'yes'){
									deleteTransaksiStokOpname(rowsdataTransaksiStokOpname);
								}
							});
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viStokOpname, selectCount_viStokOpname, dataSource_viStokOpname),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )
	
	var pencarianStokOpname = new Ext.FormPanel({
        labelAlign: 'top',
        frame:true,
        title: '',
        bodyStyle:'padding:5px 5px 0',
        //width: 600,
        items: [
		{
			layout: 'column',
			border: false,
			items:
			[
				{
					columnWidth:.98,
					layout: 'absolute',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					width: 500,
					height: 30,
					anchor: '100% 100%',
					items:
					[
						{
							x: 10,
							y: 0,
							xtype: 'label',
							text: 'No Stok Opname'
						},
						{
							x: 120,
							y: 0,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 0,
							xtype: 'textfield',
							id: 'TxtFilterGridNoSOStokOpname',
							name: 'TxtFilterGridNoSOStokOpname',
							emptyText: 'No Stok Opname',
							width: 350,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariStokOpname();
										refreshStokOpname(tmpkriteria);
									} 						
								}
							}
						},
						//----------------------------------------
						{
							x: 483,
							y: 0,
							xtype: 'label',
							text: '* Enter untuk mencari'
						},
						//----------------------------------------
						{
							x: 580,
							y: 0,
							xtype: 'button',
							text: 'Refresh',
							iconCls: 'refresh',
							tooltip: 'Refresh',
							style:{paddingLeft:'30px'},
							width:150,
							id: 'BtnFilterGridCari_viStokOpname',
							handler: function() 
							{					
								refreshStokOpname();
							}                        
						}
					]
				}
			]
		}
		]	
		
	})

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viStokOpname = new Ext.Panel
    (
		{
			title: NamaForm_viStokOpname,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viStokOpname,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ pencarianStokOpname,
					GridDataView_viStokOpname],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viStokOpname,
		            columns: 21,
		            defaults: {
					scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viStokOpname;
    //-------------- # End form filter # --------------
}

function refreshStokOpname(kriteria)
{
    dataSource_viStokOpname.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'ViewStokOpname',
                    param : kriteria
                }			
            }
        );   
    return dataSource_viStokOpname;
}

function setLookUp_viStokOpname(rowdata){
    var lebar = 785;
    setLookUps_viStokOpname = new Ext.Window({
        id: Nci.getId(),
        title: NamaForm_viStokOpname, 
        closeAction: 'destroy',        
        width: 750,
        height: 555,
		//width: 650,
		constrain:true,
       // height: 240,
		autoHeight:true,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viStokOpname(lebar,rowdata),
        listeners:{
            activate: function(){
				
            },
            afterShow: function(){
                this.activate();
            },
            deactivate: function(){
                rowSelected_viStokOpname=undefined;
            },
			close: function (){
				shortcut.remove('lookup');
			},
        }
    });

    setLookUps_viStokOpname.show();
	Ext.getCmp('txtPetugasBA_StokOpnameL').focus(true,10);
    if (rowdata == undefined){
	
    }
    else
    {
        datainit_viStokOpname(rowdata);
    }
	shortcut.set({
		code:'lookup',
		list:[
			{
				key:'ctrl+s',
				fn:function(){
					Ext.getCmp('btnSimpan_viStokOpname').el.dom.click();
				}
			},{
				key:'ctrl+d',
				fn:function(){
					Ext.getCmp('btnHapusResepStokOpname').el.dom.click();
				}
			},{
				key:'f4',
				fn:function(){
					Ext.getCmp('btnPosting_viStokOpname').el.dom.click();
				}
			},{
				key:'f6',
				fn:function(){
					Ext.getCmp('btnunPosting_viStokOpname').el.dom.click();
				}
			},
			{
				key:'esc',
				fn:function(){
					setLookUps_viStokOpname.close();
				}
			}
		]
	});
}

function getFormItemEntry_viStokOpname(lebar,rowdata){
    var pnlFormDataBasic_viStokOpname = new Ext.FormPanel({
		title: '',
		region: 'north',
		layout: 'form',
		bodyStyle: 'padding:10px 10px 10px 10px',
		anchor: '100%',
		labelWidth: 1,
		autoWidth: true,
		width: lebar,
		border: false,
		items:[
				getItemPanelInputBiodata_viStokOpname(lebar),
				getItemGridTransaksi_viStokOpname(lebar)
			],
			fileUpload: true,
		tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						//disabled: true,
						iconCls: 'add',
						id: 'btnAdd_viStokOpname',
						handler: function(){
							dataaddnew_viStokOpname();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save [Ctrl+s]',
						iconCls: 'save',
						disabled: true,
						id: 'btnSimpan_viStokOpname',
						handler: function()
						{
							datasave_viStokOpname();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save & Close',
						disabled: true,
						iconCls: 'saveexit',
						hidden:true,
						id: 'btnSimpanExit_viStokOpname',
						handler: function()
						{
							datasave_viStokOpname();
							refreshStokOpname();
							setLookUps_viStokOpname.close();
						}
					},
					{
						xtype: 'tbseparator',
						hidden:true,
					},
					{
						xtype: 'button',
						text: 'Posting [F4]',
						disabled: true,
						iconCls: 'gantidok',
						id: 'btnPosting_viStokOpname',
						handler: function()
						{
							dataposting_viStokOpname();
						}
					},
					{
						xtype: 'tbseparator'
					},{
						xtype: 'button',
						text: 'UnPosting [F6]',
						disabled: true,
						iconCls: 'gantidok',
						id: 'btnunPosting_viStokOpname',
						handler: function()
						{
							dataunposting_viStokOpname();
						}
					},
					{
						xtype: 'tbseparator'
					}
					
				]
			}
		}
    )

    return pnlFormDataBasic_viStokOpname;
}

function getItemPanelInputBiodata_viStokOpname(lebar) {
    var items =
	{
		title:'',
		layout:'column',
		//height:150,
		items:
		[
			{
				columnWidth: .50,
				layout: 'form',
				labelWidth:110,
				bodyStyle:'padding: 10px',
				border: false,
				items:
				[
					{
						xtype: 'textfield',
						fieldLabel: 'No Stok Opname ',
						name: 'txtNoSO_StokOpnameL',
						id: 'txtNoSO_StokOpnameL',
						readOnly:true,
						tabIndex:0,
						width: 150
					},
					{
						xtype: 'datefield',
						fieldLabel: 'Tanggal ',
						name: 'dfTglSO_StokOpnameL',
						id	: 'dfTglSO_StokOpnameL',
						format: 'd/M/Y',
						readOnly: true,
						value:now_viStokOpname,
						tabIndex:1,
						width: 180
					},
					{
						xtype: 'checkbox',
						boxLabel: 'Menambahkan Stok Sebelumnya',
						id: 'cbAktifMaterObat',
						name: 'cbAktifMaterObat',
						width: 200,
						checked: false,
						hidden:true,
						handler:function(a,b) 
						{
							if(a.checked==true){
								
							}else{
								
							}
						}
					}
				]
			},
			
			{
				columnWidth: .50,
				layout: 'form',
				labelWidth:100,
				bodyStyle:'padding: 10px',
				border: false,
				items:
				[
					{
						xtype: 'textfield',
						fieldLabel: 'No. BA',
						name: 'txtNoBA_StokOpnameL',
						id: 'txtNoBA_StokOpnameL',
						readOnly:true,
						width: 150
					},
					{
						xtype: 'textarea',
						fieldLabel: 'Petugas BA',
						name: 'txtPetugasBA_StokOpnameL',
						id: 'txtPetugasBA_StokOpnameL',
						//grow: true,
						width : 200,
						height : 50,
						tabIndex:2,
						listeners:{
							'specialkey': function(me, e){
								if (Ext.EventObject.getKey() === 13){
								}
								if (e.getKey() == e.TAB) {
									if(dsDataGrdJab_viApotekStokOpname.getCount()==0){
										var records = new Array();
										records.push(new dsDataGrdJab_viApotekStokOpname.recordType());
										dsDataGrdJab_viApotekStokOpname.add(records);
										var row = dsDataGrdJab_viApotekStokOpname.getCount()-1; 
										StokOpname.form.Grid.a.startEditing(row, 2);
										Ext.getCmp('btnHapusResepStokOpname').enable(true);
										Ext.getCmp('btnSimpan_viStokOpname').enable(true);
										Ext.getCmp('btnSimpanExit_viStokOpname').enable(true);
										Ext.getCmp('btnAdd_viStokOpname').enable(true);
									}
								}
							}
						}
					}
				]
			}
		
		]
	};
    return items;
};

function getItemGridTransaksi_viStokOpname(lebar) 
{
    var items =
	{
		//title: 'Detail Transaksi', 
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:1px 1px 1px 1px',
		border:true,
		width: lebar-80,
		height: 235,//225, 
		tbar:
		[
			{
				text	: 'Add Obat',
				id		: 'btnAddObatRWI',
				tooltip	: nmLookup,
				disabled: false,
				iconCls	: 'find',
				handler	: function(){
					if(dsDataGrdJab_viApotekStokOpname.getCount() > 100){
						ShowPesanWarningStokOpname('Jumlah maksimal item 100!','Warning')
					} else{
						var records = new Array();
						records.push(new dsDataGrdJab_viApotekStokOpname.recordType());
						dsDataGrdJab_viApotekStokOpname.add(records);
						Ext.getCmp('btnHapusResepStokOpname').enable(true);
						Ext.getCmp('btnSimpan_viStokOpname').enable(true);
						Ext.getCmp('btnSimpanExit_viStokOpname').enable(true);
						Ext.getCmp('btnAdd_viStokOpname').enable(true);
						
						var row = dsDataGrdJab_viApotekStokOpname.getCount()-1; 
						StokOpname.form.Grid.a.startEditing(row, 2);
					}
				}
			},	
			{
				text	: 'Delete',
				id		: 'btnHapusResepStokOpname',
				tooltip	: nmLookup,
				iconCls	: 'remove',
				disabled: true,
				handler	: function(){
					Ext.Msg.confirm('Hapus obat', 'Apakah obat ini akan dihapus?', function(button){
						if (button == 'yes'){
							var line = StokOpname.form.Grid.a.getSelectionModel().selection.cell[0];
							var o = dsDataGrdJab_viApotekStokOpname.getRange()[line].data;
							if(dsDataGrdJab_viApotekStokOpname.getCount() > 1){
								if(o.urut == "" || o.urut == undefined){
									dsDataGrdJab_viApotekStokOpname.removeAt(line);
									StokOpname.form.Grid.a.getView().refresh();
								} else{
									Ext.Ajax.request({
										url: baseURL + "index.php/apotek/functionStokOpname/deletedetail",
										params: {
											no_so:Ext.getCmp('txtNoSO_StokOpnameL').getValue(),
											kd_prd:o.kd_prd,
											kd_milik:o.kd_milik,
											kd_unit_far:o.kd_unit_far,
											urut:o.urut
										},
										failure: function(o)
										{
											loadMask.hide();
											ShowPesanErrorStokOpname('Hubungi Admin', 'Error');
										},	
										success: function(o) 
										{
											var cst = Ext.decode(o.responseText);
											if (cst.success === true) 
											{
												loadMask.hide();
												dsDataGrdJab_viApotekStokOpname.removeAt(line);
												StokOpname.form.Grid.a.getView().refresh();
											}
											else 
											{
												loadMask.hide();
												ShowPesanErrorStokOpname('Gagal menghapus obat ini. Hubungi Admin!', 'Error');
											};
										}
									})
								}
								
							} else{
								ShowPesanErrorStokOpname('Data tidak bisa dihapus karena minimal stok opname 1 obat','Error');
							}
						}
					});
				}
			}	
		],
	    items:
		[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:
				[
					gridDataViewEdit_viApotekStokOpname()
				]	
			}
			//-------------- ## --------------
		]
	};
    return items;
};

function gridDataViewEdit_viApotekStokOpname()
{
    var FieldGrdKasir_viApotekStokOpname = [];
	
    dsDataGrdJab_viApotekStokOpname= new WebApp.DataStore
	({
        fields: FieldGrdKasir_viApotekStokOpname
    });
    
    StokOpname.form.Grid.a =new Ext.grid.EditorGridPanel({
        store: dsDataGrdJab_viApotekStokOpname,
        height: 210,//220,
		stripeRows: true,
		columnLines: true,
		selModel: new Ext.grid.CellSelectionModel({
	            singleSelect: true,
	            listeners: {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
        }),
        
        columns: 
		[	
			new Ext.grid.RowNumberer(),
			{			
				dataIndex: 'kd_prd',
				header: 'Kode',
				sortable: true,
				width: 100
			},
			//-------------- ## --------------
			{			
				dataIndex: 'nama_obat',
				header: 'Nama Obat',
				sortable: true,
				width: 310,
				editor:new Nci.form.Combobox.autoComplete({
					store	: StokOpname.form.ArrayStore.a,
					select	: function(a,b,c){
						var line	= StokOpname.form.Grid.a.getSelectionModel().selection.cell[0];
						dsDataGrdJab_viApotekStokOpname.getRange()[line].data.nama_obat=b.data.nama_obat;
						dsDataGrdJab_viApotekStokOpname.getRange()[line].data.kd_prd=b.data.kd_prd;
						dsDataGrdJab_viApotekStokOpname.getRange()[line].data.jml_stok_apt=b.data.jml_stok_apt;
						dsDataGrdJab_viApotekStokOpname.getRange()[line].data.kd_milik=b.data.kd_milik;
						dsDataGrdJab_viApotekStokOpname.getRange()[line].data.kd_unit_far=b.data.kd_unit_far;
						dsDataGrdJab_viApotekStokOpname.getRange()[line].data.min_stok=b.data.min_stok;
						
						StokOpname.form.Grid.a.getView().refresh();
						StokOpname.form.Grid.a.startEditing(line, 4);
					},
					insert	: function(o){
						return {
							kd_prd        	: o.kd_prd,
							nama_obat 		: o.nama_obat,
							kd_milik		: o.kd_milik,
							jml_stok_apt	: o.jml_stok_apt,
							min_stok		: o.min_stok,
							kd_unit_far		: o.kd_unit_far,
							milik			: o.milik,
							text			:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_prd+'</td><td width="200">'+o.nama_obat+'</td><td width="40" align="right">'+o.jml_stok_apt+'</td><td width="40" align="left">'+o.milik+'</td></tr></table>'
						}
					},
					url		: baseURL + "index.php/apotek/functionStokOpname/getObat",
					valueField: 'nama_obat',
					displayField: 'text',
					listWidth: 390
				})
			},
			//-------------- ## --------------
			{
				dataIndex: 'jml_stok_apt',
				header: 'Stok',
				sortable: true,
				align:'right',
				width: 110
			},
			//-------------- ## --------------
			{
				dataIndex: 'stok_opname',
				header: 'Stok Opname',
				sortable: true,
				align:'right',
				width: 110,
				editor: new Ext.form.NumberField({
					allowBlank: false,
					listeners:{
						blur: function(a){
							var line	= this.index;
							dsDataGrdJab_viApotekStokOpname.getRange()[line].data.stok_opname=a.getValue();
							if(a.getValue() < 0){
								dsDataGrdJab_viApotekStokOpname.getRange()[line].data.stok_opname=0;
								StokOpname.form.Grid.a.getView().refresh();
								ShowPesanWarningStokOpname('Stok opname tidak boleh -', 'Warning');
							}
						},
						focus: function(a){
							this.index=StokOpname.form.Grid.a.getSelectionModel().selection.cell[0]
						},
						specialkey:function(){
							if(Ext.EventObject.getKey() ==13){
								
								var records = new Array();
								records.push(new dsDataGrdJab_viApotekStokOpname.recordType());
								dsDataGrdJab_viApotekStokOpname.add(records);
								var nextRow = dsDataGrdJab_viApotekStokOpname.getCount()-1; 
								StokOpname.form.Grid.a.startEditing(nextRow, 2);
							}
						}
					}
				})	
			},
			//-------------- ## --------------
			{
				dataIndex: 'kd_milik',
				header: 'kd milik',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'kd_unit_far',
				header: 'kd unit far',
				hidden: true,
				width: 80
			},
			{
				dataIndex: 'urut',
				header: 'urut',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
        ]
       // plugins:chkSelected_viApotekStokOpname,
    });
    return  StokOpname.form.Grid.a;
}

function dataaddnew_viStokOpname(){
	Ext.getCmp('txtNoSO_StokOpnameL').setValue('');
	Ext.getCmp('txtNoBA_StokOpnameL').setValue('');
	Ext.getCmp('txtPetugasBA_StokOpnameL').setValue('');
	dsDataGrdJab_viApotekStokOpname.removeAll();
	
	Ext.getCmp('btnSimpan_viStokOpname').enable();
	Ext.getCmp('btnSimpanExit_viStokOpname').enable();
	Ext.getCmp('btnHapusResepStokOpname').enable();
	Ext.getCmp('btnPosting_viStokOpname').disable();
	Ext.getCmp('btnunPosting_viStokOpname').disable();
	Ext.getCmp('btnAddObatRWI').enable();
	Ext.getCmp('txtNoBA_StokOpnameL').focus();
}

function datainit_viStokOpname(rowdata)
{
	if(rowdata.APPROVE == 't'){
		Ext.getCmp('btnSimpan_viStokOpname').disable(true);
		Ext.getCmp('btnSimpanExit_viStokOpname').disable(true);
		Ext.getCmp('btnHapusResepStokOpname').disable(true);
		Ext.getCmp('btnPosting_viStokOpname').disable(true);
		Ext.getCmp('btnunPosting_viStokOpname').enable(true);
		Ext.getCmp('btnAddObatRWI').disable(true);
	} else{
		Ext.getCmp('btnSimpan_viStokOpname').enable(true);
		Ext.getCmp('btnSimpanExit_viStokOpname').enable(true);
		Ext.getCmp('btnHapusResepStokOpname').enable(true);
		Ext.getCmp('btnPosting_viStokOpname').enable(true);
		Ext.getCmp('btnunPosting_viStokOpname').disable(true);
		Ext.getCmp('btnAddObatRWI').enable(true);
	}

	Ext.getCmp('txtNoSO_StokOpnameL').setValue(rowdata.NO_SO);
	Ext.getCmp('dfTglSO_StokOpnameL').setValue(ShowDate(rowdata.TGL_SO));
	Ext.getCmp('txtNoBA_StokOpnameL').setValue(rowdata.NO_BA_SO);
	Ext.getCmp('txtPetugasBA_StokOpnameL').setValue(rowdata.KET_SO);
	dataGridObatStokOpname(rowdata.NO_SO);
};

function datasave_viStokOpname(){
	if (ValidasiEntryStokOpname(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionStokOpname/save",
				params: getParamStokOpname(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorStokOpname('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoStokOpname(nmPesanSimpanSukses,nmHeaderSimpanData);
						Ext.get('txtNoSO_StokOpnameL').dom.value=cst.noso;
						refreshStokOpname();
						dsDataGrdJab_viApotekStokOpname.removeAll();
						dataGridObatStokOpname(cst.noso);
						Ext.getCmp('txtNoBA_StokOpnameL').setValue(cst.no_ba); 
						Ext.getCmp('btnSimpan_viStokOpname').disable();
						Ext.getCmp('btnSimpanExit_viStokOpname').enable();
						Ext.getCmp('btnHapusResepStokOpname').enable();
						Ext.getCmp('btnPosting_viStokOpname').enable();
						loadMask.hide();
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorStokOpname('Gagal Menyimpan Data ini', 'Error');
						refreshStokOpname();
					};
				}
			}
			
		)
	}
}

function dataposting_viStokOpname(){
	Ext.Msg.confirm('Warning', 'Apakah data ini akan diPosting? Pastikan semua data sudah benar sebelum diPosting', function(button){
		if (button == 'yes'){
			Ext.Ajax.request
			(
				{
					url: baseURL + "index.php/apotek/functionStokOpname/postingStokOpname",
					params: getParamPostingStokOpname(),
					failure: function(o)
					{
						loadMask.hide();
						ShowPesanErrorStokOpname('Hubungi Admin', 'Error');
					},	
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoStokOpname('Posting Berhasil','Information');
							Ext.getCmp('btnSimpan_viStokOpname').disable(true);
							Ext.getCmp('btnSimpanExit_viStokOpname').disable(true);
							Ext.getCmp('btnHapusResepStokOpname').disable(true);
							Ext.getCmp('btnPosting_viStokOpname').disable(true);
							Ext.getCmp('btnunPosting_viStokOpname').enable();
							refreshStokOpname();
							loadMask.hide();
						}
						else 
						{
							loadMask.hide();
							ShowPesanErrorStokOpname('Gagal melakukan Posting. ' + cst.pesan, 'Error');
						};
					}
				}
				
			)
		}
	});
}
function dataunposting_viStokOpname(){
	Ext.Msg.confirm('Warning', 'Apakah data ini akan diunPosting?', function(button){
		if (button == 'yes'){
			Ext.Ajax.request
			(
				{
					url: baseURL + "index.php/apotek/functionStokOpname/unpostingStokOpname",
					params: getParamPostingStokOpname(),
					failure: function(o)
					{
						loadMask.hide();
						ShowPesanErrorStokOpname('Hubungi Admin', 'Error');
					},	
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoStokOpname('UnPosting Berhasil','Information');
							Ext.getCmp('btnSimpan_viStokOpname').enable();
							Ext.getCmp('btnSimpanExit_viStokOpname').enable();
							Ext.getCmp('btnHapusResepStokOpname').enable();
							Ext.getCmp('btnPosting_viStokOpname').enable();
							Ext.getCmp('btnunPosting_viStokOpname').disable();
							refreshStokOpname();
							loadMask.hide();
						}
						else 
						{
							loadMask.hide();
							ShowPesanErrorStokOpname('Gagal melakukan Posting. ' + cst.pesan, 'Error');
						};
					}
				}
				
			)
		}
	});
}

function dataGridObatStokOpname(no_so){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionStokOpname/getObatGrid",
			params: {query:no_so},
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorStokOpname('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dsDataGrdJab_viApotekStokOpname.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
						dsDataGrdJab_viApotekStokOpname.add(recs);
					
					
					
					StokOpname.form.Grid.a.getView().refresh();
				}
				else 
				{
					loadMask.hide();
					ShowPesanErrorStokOpname('Gagal membaca data obat', 'Error');
				};
			}
		}
		
	)
	
}

function getUnitFar(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionStokOpname/getUnitFar",
			params: {query:''},
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorStokOpname('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					UnitFarAktif = cst.kd_unit_far;
				}
				else 
				{
					loadMask.hide();
					ShowPesanErrorStokOpname('Gagal membaca unitfar', 'Error');
				};
			}
		}
		
	)
	
}

function getParamPostingStokOpname(){
	
	var	params =
	{
		NoSo:Ext.getCmp('txtNoSO_StokOpnameL').getValue(),
	}
	
	params['jumlah']=dsDataGrdJab_viApotekStokOpname.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viApotekStokOpname.getCount();i++)
	{
		params['kd_prd-'+i]=dsDataGrdJab_viApotekStokOpname.data.items[i].data.kd_prd
		params['nama_obat-'+i]=dsDataGrdJab_viApotekStokOpname.data.items[i].data.nama_obat
		params['kd_milik-'+i]=dsDataGrdJab_viApotekStokOpname.data.items[i].data.kd_milik
		params['jml_stok_apt-'+i]=dsDataGrdJab_viApotekStokOpname.data.items[i].data.jml_stok_apt
		params['stok_opname-'+i]=dsDataGrdJab_viApotekStokOpname.data.items[i].data.stok_opname
		params['kd_unit_far-'+i]=dsDataGrdJab_viApotekStokOpname.data.items[i].data.kd_unit_far
		params['min_stok-'+i]=dsDataGrdJab_viApotekStokOpname.data.items[i].data.min_stok
		
	}
    return params
}

function getParamStokOpname() 
{
	var	params =
	{
		NoSo:Ext.getCmp('txtNoSO_StokOpnameL').getValue(),
		TanggalSo:Ext.getCmp('dfTglSO_StokOpnameL').getValue(),
		NoBaSo:Ext.getCmp('txtNoBA_StokOpnameL').getValue(),
		KetSo:Ext.getCmp('txtPetugasBA_StokOpnameL').getValue()
	}
	
	params['jumlah']=dsDataGrdJab_viApotekStokOpname.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viApotekStokOpname.getCount();i++)
	{
		params['kd_prd-'+i]=dsDataGrdJab_viApotekStokOpname.data.items[i].data.kd_prd
		params['nama_obat-'+i]=dsDataGrdJab_viApotekStokOpname.data.items[i].data.nama_obat
		params['kd_milik-'+i]=dsDataGrdJab_viApotekStokOpname.data.items[i].data.kd_milik
		params['jml_stok_apt-'+i]=dsDataGrdJab_viApotekStokOpname.data.items[i].data.jml_stok_apt
		params['stok_opname-'+i]=dsDataGrdJab_viApotekStokOpname.data.items[i].data.stok_opname
		params['kd_unit_far-'+i]=dsDataGrdJab_viApotekStokOpname.data.items[i].data.kd_unit_far
		params['min_stok-'+i]=dsDataGrdJab_viApotekStokOpname.data.items[i].data.min_stok
		params['urut-'+i]=i+1
		
	}
    return params
};


function getCriteriaCariStokOpname()//^^^
{
	var strKriteria = "";

	if (Ext.get('TxtFilterGridNoSOStokOpname').getValue() != "" && Ext.get('TxtFilterGridNoSOStokOpname').getValue()!=='No Stok Opname')
	{
		strKriteria = " upper(so.no_so) " + "LIKE upper('" + Ext.get('TxtFilterGridNoSOStokOpname').getValue() +"%') ";
	} else{
		strKriteria = " so.no_so <> '' and sod.kd_unit_far='" + UnitFarAktif + "'";
	}
		strKriteria= strKriteria + "group by so.no_so,zu.full_name,sod.kd_unit_far,u.nm_unit_far order by so.no_so desc limit 30"
	return strKriteria;
}


function ValidasiEntryStokOpname(modul,mBolHapus)
{
	var x = 1;
	if(Ext.getCmp('txtPetugasBA_StokOpnameL').getValue() === ''){
		ShowPesanWarningStokOpname('Petugas BA masih kosong', 'Warning');
		x = 0;
	} 
	if(dsDataGrdJab_viApotekStokOpname.getCount() > 100){
		ShowPesanWarningStokOpname('Jumlah item melebihi jumlah maksimal! Jumlah maksimal item adalah 100', 'Warning');
		x = 0;
	} else{
		for(var i=0; i<dsDataGrdJab_viApotekStokOpname.getCount() ; i++){
			if(dsDataGrdJab_viApotekStokOpname.getCount() > 100){
				ShowPesanWarningStokOpname('Jumlah item melebihi maksimal! Maksimal item adalah 100', 'Warning');
				x = 0;
			} else{
				
			}
			var o=dsDataGrdJab_viApotekStokOpname.getRange()[i].data;
			var tampKd="";
			if(o.stok_opname == undefined || o.stok_opname < 0 || o.kd_prd == undefined ){
				if(o.kd_prd == undefined){
					// ShowPesanWarningStokOpname('Daftar obat yang akan stok opname masih kosong', 'Warning');
					// x = 0;
					dsDataGrdJab_viApotekStokOpname.removeAt(i);
				}else if(o.stok_opname == undefined){
					ShowPesanWarningStokOpname('Stok opname masih kosong, stok opname tidak boleh kosong', 'Warning');
					x = 0;
				} else if(o.stok_opname < 0){
					ShowPesanWarningStokOpname('Stok opname tidak boleh -', 'Warning');
					x = 0;
				} else if(o.min_stok < 0){
					ShowPesanWarningStokOpname('Minimum stok tidak boleh -', 'Warning');
					x = 0;
				}
			}
			
			for(var j=0; j<dsDataGrdJab_viApotekStokOpname.getCount() ; j++){
				var p=dsDataGrdJab_viApotekStokOpname.getRange()[j].data;
				if(i != j && o.kd_prd == p.kd_prd){
					ShowPesanWarningStokOpname('Tidak boleh ada obat yang sama, periksa kembali daftar obat', 'Warning');
					x = 0;
					break;
				}
			}
			
		}
	}
	
	
	return x;
};


function deleteTransaksiStokOpname(data){
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/functionStokOpname/deletetransaksi",
		params: {
			no_so:data.NO_SO
		},
		failure: function(o)
		{
			loadMask.hide();
			ShowPesanErrorStokOpname('Hubungi Admin', 'Error');
		},	
		success: function(o) 
		{
			var cst = Ext.decode(o.responseText);
			if (cst.success === true) 
			{
				ShowPesanErrorStokOpname('Transaksi stok opname berhasil diHapus.', 'Error');
				dataSource_viStokOpname.removeAll();
				refreshStokOpname();
			}
			else 
			{
				loadMask.hide();
				ShowPesanErrorStokOpname('Gagal menghapus transaksi! '+cst.pesan, 'Error');
			};
		}
	})
}


function ShowPesanWarningStokOpname(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorStokOpname(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};


function ShowPesanInfoStokOpname(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};

shortcut.set({
	code:'main',
	list:[
		{
			key:'f1',
			fn:function(){
				Ext.getCmp('btnTambah_viStokOpname').el.dom.click();
			}
		},
	]
});