// Data Source ExtJS # --------------

/**
*	Nama File 		: TRApotekResepRWI.js
*	Menu 			: APOTEK
*	Model id 		: 
*	Keterangan 		: Resep RWI
*	Di buat tanggal : 04 Juni 2015
*	Oleh 			: M
*	Edit			: AGUNG
*/

var KdFormResepRWI=8; // kd_form RESEP RWI
var dsprinter_rwi;
var cbopasienorder_printer_rwi;
var dataSource_viApotekResepRWI;
var selectCount_viApotekResepRWI=50;
var NamaForm_viApotekResepRWI="Resep Rawat Inap ";
var selectCountStatusPostingApotekResepRWI='Semua';
var mod_name_viApotekResepRWI="viApotekResepRWI";
var now_viApotekResepRWI= new Date();
var addNew_viApotekResepRWI;
var rowSelected_viApotekResepRWI;
var setLookUps_viApotekResepRWI;
var mNoKunjungan_viApotekResepRWI='';
var selectSetUnit;
var cellSelecteddeskripsiRWI;
var tanggal = now_viApotekResepRWI.format("d/M/Y");
var jam = now_viApotekResepRWI.format("H/i/s");
var tampungshiftsekarang;
var tmpkriteria;
var gridDTLTRHistoryApotekRWI;
var cbopasienorder_mng_apotek_rwi;
var dsDataGrdJab_viApotekResepRWI;
var setLookUpApotek_TransferResepRWI;
var kd_pasien_obbt_rwi;
var kd_unit_obbt_rwi;
var tgl_masuk_obbt_rwi;
var urut_masuk_obbt_rwi; 
var GridDataViewOrderManagement_viApotekResepRWI;
var rowSelectedOrderManajemen_viApotekResepRWI;
var dataSourceGridOrder_viApotekResepRWI;
var CurrentGridOrderRWI;
var detailorderrwi=false;
var CurrentIdMrResepRwi='';
var kd_spesial_tr;
var	kd_unit_kamar_tr;
var	no_kamar_tr;
var ordermanajemenrwi=false;
var integrated;
var UnitFarAktif_ResepRWI;
var CurrentObat_viApotekResepRWI;
var CellSelected_viApotekResepRWI;
var currentKdPrdRacik;
var currentNamaObatRacik;
var currentHargaRacik;
var currentJumlah;
var curentIndexsSelection;
var PrintBillResepRWI;



var CurrentHistoryRWI =
{
    data: Object,
    details: Array,
    row: 0
};

var CurrentData_viApotekResepRWI =
{
	data: Object,
	details: Array,
	row: 0
};

var CurrentDataOrderManajemen_viApotekResepRWI = 
{
	data: Object,
	details: Array,
	row: 0
};


var dspasienorder_mng_apotek_rwi;
CurrentPage.page = dataGrid_viApotekResepRWI(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

var ResepRWI={};
ResepRWI.form={};
ResepRWI.func={};
ResepRWI.vars={};
ResepRWI.func.parent=ResepRWI;
ResepRWI.form.ArrayStore={};
ResepRWI.form.ComboBox={};
ResepRWI.form.DataStore={};
ResepRWI.form.Record={};
ResepRWI.form.Form={};
ResepRWI.form.Grid={};
ResepRWI.form.Panel={};
ResepRWI.form.TextField={};
ResepRWI.form.Button={};


ResepRWI.form.ArrayStore.kodepasien=new Ext.data.ArrayStore({
		id: 0,
		fields: [	
					'kd_kasir','no_transaksi','no_kamar','kd_pasien','nama',
					'nama_keluarga','jenis_kelamin','nama_unit','kd_unit',
					'no_kamar','nama_kamar', 'kd_dokter', 'kd_customer','kd_spesial','kd_unit_kamar','customer'
				],
		data: []
	});

ResepRWI.form.ArrayStore.pasien=new Ext.data.ArrayStore({
		id: 0,
		fields: [	
					'kd_kasir','no_transaksi','no_kamar','kd_pasien','nama',
					'nama_keluarga','jenis_kelamin','nama_unit','kd_unit',
					'nama_kamar', 'kd_dokter', 'kd_customer','kd_spesial',
					'kd_unit_kamar','urut_masuk','nama_dokter','nama_unit','customer'
				],
		data: []
	});

ResepRWI.form.ArrayStore.a	= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_prd','kd_satuan','nama_obat','kd_sat_besar','harga_jual','harga_beli','kd_pabrik','markup','tuslah','adm_racik'],
	data: []
});

function dataGrid_viApotekResepRWI(mod_id_viApotekResepRWI){	
    // Field kiriman dari Project Net.
    var FieldMaster_viApotekResepRWI = 
	[
		 'STATUS_POSTING','NO_RESEP','NO_OUT','TGL_OUT', 'KD_PASIENAPT', 'NMPASIEN', 'DOKTER', 
		'NAMA_DOKTER', 'KD_UNIT', 'NAMA_UNIT', 'APT_NO_TRANSAKSI','TGL_TRANSAKSI',
		'APT_KD_KASIR', 'KD_CUSTOMER','ADMRACIK','JUMLAH','JML_TERIMA_UANG','SISA','ADMPRHS',
		'JASA','ADMRESEP','NO_KAMAR','NAMA_KAMAR','CUSTOMER','JENIS_PASIEN','TGL_MASUK','URUT_MASUK',
		'KD_SPESIAL','KD_UNIT_KAMAR','TGL_RESEP'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viApotekResepRWI = new WebApp.DataStore
	({
        fields: FieldMaster_viApotekResepRWI
    });
	
    refreshRespApotekRWI();
	total_pasien_order_mng_obtrwi();
	total_pasien_dilayani_order_mng_obtrwi();
	viewGridOrderAll_RASEPRWI();
	loadDataKodePasienResepRWI();
	getUnitFar_ResepRWI();
	
    // Grid Apotek Perencanaan # --------------
	var GridDataView_viApotekResepRWI = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: 'Daftar Resep Telah Dibuat',
			store: dataSource_viApotekResepRWI,
			autoScroll: true,
			columnLines: true,
			border:false,
			anchor: '100% 51.1%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viApotekResepRWI = undefined;
							rowSelected_viApotekResepRWI = dataSource_viApotekResepRWI.getAt(row);
							CurrentData_viApotekResepRWI
							CurrentData_viApotekResepRWI.row = row;
							CurrentData_viApotekResepRWI.data = rowSelected_viApotekResepRWI.data;
							if (rowSelected_viApotekResepRWI.data.STATUS_POSTING==='0')
							{
								Ext.getCmp('btnHapusTrx_viApotekResepRWI').enable();
							}
							else
							{
								Ext.getCmp('btnHapusTrx_viApotekResepRWI').disable();
							}
							
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viApotekResepRWI = dataSource_viApotekResepRWI.getAt(ridx);
					if (rowSelected_viApotekResepRWI != undefined)
					{
						setLookUp_viApotekResepRWI(rowSelected_viApotekResepRWI.data);
					}
					else
					{
						setLookUp_viApotekResepRWI();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Apotek perencanaan
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header		: 'Status Posting',
						width		: 20,
						sortable	: false,
						hideable	: true,
						hidden		: false,
						menuDisabled: true,
						dataIndex	: 'STATUS_POSTING',
						id			: 'colStatusPosting_viApotekResepRWI',
						renderer	: function(value, metaData, record, rowIndex, colIndex, store){
							 switch (value){
								 case '1':
									 metaData.css = 'StatusHijau'; 
									 break;
								 case '0':
									 metaData.css = 'StatusMerah';
									 break;
							 }
							 return '';
						}
					},
					{
						id: 'colNoMedrec_viApotekResepRWI',
						header: 'No. Resep',
						dataIndex: 'NO_RESEP',
						sortable: true,
						width: 35
						
					},
					//-------------- ## --------------
					{
						id: 'colTgl_viApotekResepRWI',
						header:'Tgl Resep',
						dataIndex: 'TGL_OUT',						
						width: 30,
						sortable: true,
						hideable:false,
                        menuDisabled:true,
						// format: 'd/M/Y',
						//filter: {},
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.TGL_OUT);
						}
					},
					//-------------- ## --------------
					{
						id: 'colNoMedrec_viApotekResepRWI',
						header: 'No Medrec',
						dataIndex: 'KD_PASIENAPT',
						sortable: true,
						hideable:false,
                        menuDisabled:true,
						width: 30
					},
					//-------------- ## --------------
					{
						id: 'colNamaPasien_viApotekResepRWI',
						header: 'Nama Pasien',
						dataIndex: 'NMPASIEN',
						sortable: true,
						width: 50
					},
					//-------------- ## --------------
					{
						id: 'colUnit_viApotekResepRWI',
						header: 'Ruangan',
						dataIndex: 'NAMA_UNIT',
						sortable: true,
						width: 40
					},
					//-------------- ## --------------
					{
						id: 'colNoout_viApotekResepRWI',
						header: 'No Out',
						dataIndex: 'NO_OUT',
						sortable: true,
						width: 40,
						hidden:true
					}
					//-------------- ## --------------
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viApotekResepRWI',
				items: 
				[
					{
						xtype: 'button',
						text: 'Tambah Resep',
						iconCls: 'Edit_Tr',
						tooltip: 'Add Data',
						id: 'btnTambah_viApotekResepRWI',
						handler: function(sm, row, rec)
						{
							Ext.Ajax.request(
							{
								   
								url: baseURL + "index.php/apotek/currentShiftAPT/getcurrentshift",
								 params: {
									
									command: '0'
								
								},
								failure: function(o)
								{
									 var cst = Ext.decode(o.responseText);
									
								},	    
								success: function(o) {
									var cst = Ext.decode(o.responseText);
						
									tampungshiftsekarang=cst.shift;
									ResepRWI.form.Panel.shift.update(cst.shift);
								}	
							
							});
							cekPeriodeBulanRwi();
							
							
								
						}
					},
					{
						xtype: 'button',
						text: 'Edit Resep',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viApotekResepRWI',
						handler: function(sm, row, rec)
						{
							Ext.Ajax.request(
							{
								   
								url: baseURL + "index.php/apotek/currentShiftAPT/getcurrentshift",
								 params: {
									
									command: '0'
								
								},
								failure: function(o)
								{
									 var cst = Ext.decode(o.responseText);
									
								},	    
								success: function(o) {
									var cst = Ext.decode(o.responseText);
						
									tampungshiftsekarang=cst.shift
								}	
							
							});
							if (rowSelected_viApotekResepRWI != undefined)
							{
								setLookUp_viApotekResepRWI(rowSelected_viApotekResepRWI.data)
							}
							
						}
					},
					{
						xtype: 'button',
						text: 'Hapus Transaksi',
						iconCls: 'remove',
						tooltip: 'Hapus Data',
						disabled:true,
						id: 'btnHapusTrx_viApotekResepRWI',
						handler: function(sm, row, rec)
						{
							 var datanya=rowSelected_viApotekResepRWI.data;
							if (datanya===undefined){
								ShowPesanWarningResepRWI('Belum ada data yang dipilih','Resep RWI');
							}
							else
							{
								 Ext.Msg.show({
									title: 'Hapus Transaksi',
									msg: 'Anda yakin akan menghapus data transaksi ini ?',
									buttons: Ext.MessageBox.YESNO,
									fn: function (btn) {
										if (btn == 'yes')
										{
											
											console.log(datanya);
											var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan Pembatalan Transaksi:', function (btn, combo) {
														if (btn == 'ok')
														{
															var variablebatalhistori_rwi = combo;
															if (variablebatalhistori_rwi != '')
															{
																 Ext.Ajax.request({
										   
																		url: baseURL + "index.php/apotek/functionAPOTEK/hapusTrxResep",
																		 params: {
																			
																			noout: datanya.NO_OUT,
																			tglout: datanya.TGL_OUT,
																			kdcustomer:datanya.KD_CUSTOMER,
																			namacustomer:datanya.CUSTOMER,
																			kdunit:datanya.KD_UNIT,
																			jenis:'RESEP',
																			apaini:"reseprwi",
																			alasan: variablebatalhistori_rwi
																		
																		},
																		failure: function(o)
																		{
																			 var cst = Ext.decode(o.responseText);
																			ShowPesanErrorResepRWI('Data transaksi tidak dapat dihapus','Resep RWI');
																		},	    
																		success: function(o) {
																			var cst = Ext.decode(o.responseText);
																			if (cst.success===true)
																			{
																				tmpkriteria = getCriteriaCariApotekResepRWI();
																				refreshRespApotekRWI(tmpkriteria);
																				ShowPesanInfoResepRWI('Data transaksi Berhasil dihapus','Resep RWI');
																				Ext.getCmp('btnHapusTrx_viApotekResepRWI').disable();
																			}
																			else
																			{
																				ShowPesanErrorResepRWI('Data transaksi tidak dapat dihapus','Resep RWI');
																			}
																			
																		}
																
																})
															} else
															{
																ShowPesanWarningResepRWI('Silahkan isi alasan terlebih dahaulu', 'Keterangan');

															}
														}

													});
											/*  */
										}
									},
									icon: Ext.MessageBox.QUESTION
								});
							}
						}
					},
					/* {xtype: 'tbspacer',height: 3, width:580},
					{
						xtype: 'label',
						text: 'Order Rawat Inap : ' 
					},
					{xtype: 'tbspacer',height: 3, width:5},
					{
						x: 40,
						y: 40,
						xtype: 'textfield',
						fieldLabel: 'No. Medrec',
						name: 'txtcounttr_apt_rwi',
						id: 'txtcounttr_apt_rwi',
						width: 50,
						disabled:true,
						listeners: 
						{ 
							
						}
					} */
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viApotekResepRWI, selectCount_viApotekResepRWI, dataSource_viApotekResepRWI),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )
	
	var FieldOrderManajemen_viApotekResepRWI = 
	[
		'kd_pasien', 'nama', 'kd_unit', 'nama_unit','kd_unit_kamar','kd_spesial','no_kamar'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
	dataSourceGridOrder_viApotekResepRWI = new WebApp.DataStore
	({
        fields: FieldOrderManajemen_viApotekResepRWI
    });
	
	GridDataViewOrderManagement_viApotekResepRWI = new Ext.grid.EditorGridPanel
    (
		{
			title:'Order Obat Ruangan',
			store: dataSourceGridOrder_viApotekResepRWI,
			autoScroll: true,
			columnLines: true,
			border: true, //false,
			anchor:'100% 30%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelectedOrderManajemen_viApotekResepRWI = undefined;
							rowSelectedOrderManajemen_viApotekResepRWI = dataSourceGridOrder_viApotekResepRWI.getAt(row);
							CurrentDataOrderManajemen_viApotekResepRWI
							CurrentDataOrderManajemen_viApotekResepRWI.row = row;
							CurrentDataOrderManajemen_viApotekResepRWI.data = rowSelectedOrderManajemen_viApotekResepRWI.data;
						}
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelectedOrderManajemen_viApotekResepRWI = dataSourceGridOrder_viApotekResepRWI.getAt(ridx);
					if (rowSelectedOrderManajemen_viApotekResepRWI != undefined)
					{
						CurrentGridOrderRWI=CurrentDataOrderManajemen_viApotekResepRWI.data;
						
						CurrentIdMrResepRwi=CurrentGridOrderRWI.id_mrresep;
						ordermanajemenrwi=true;
						Ext.Ajax.request(
						{
							   
							url: baseURL + "index.php/apotek/currentShiftAPT/getcurrentshift",
							 params: {
								
								command: '0'
							
							},
							failure: function(o)
							{
								 var cst = Ext.decode(o.responseText);
								
							},	    
							success: function(o) {
								var cst = Ext.decode(o.responseText);
					
								tampungshiftsekarang=cst.shift;
								ResepRWI.form.Panel.shift.update(cst.shift);
							}	
						
						});
						detailorderrwi=true;
						cekPeriodeBulanRwi(detailorderrwi,CurrentGridOrderRWI);
						
					}
					else
					{
						
					}
				}
			},
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header: 'No Medrec',
						dataIndex: 'kd_pasien',
						sortable: false,
						hideable:false,
                        menuDisabled:true,
						width: 30
					},
					
					//-------------- ## --------------
					{
						header: 'Nama',
						dataIndex: 'nama',
						sortable: false,
						width: 50
					},
					//-------------- ## --------------
					{
						header: 'Kelas - Kamar',
						dataIndex: 'nama_unit',
						sortable: false,
						hidden:true,
						width: 40
					},
					//-------------- ## --------------
					{
						header: 'Kelas - Kamar',
						dataIndex: 'kelas_kamar',
						sortable: false,
						width: 40
					},
					//-------------- ## --------------
					{
						header:'Tgl Order',
						dataIndex: 'tgl_order',						
						width: 30,
						sortable: false,
						hideable:false,
                        menuDisabled:true,
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.tgl_order);
						}
					},
					{
						header: 'Status',
						dataIndex: 'order_mng',
						sortable: false,
						width: 40
					},
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbarOrderManajemen_viApotekResepRWI',
				items: 
				[
					{
						xtype: 'label',
						text: 'Order obat ruangan : ',
						style : {
							color : 'red'
						}
					},
					{xtype: 'tbspacer',height: 3, width:5},
					{
						x: 40,
						y: 40,
						xtype: 'textfield',
						name: 'txtcounttr_apt_rwi',
						id: 'txtcounttr_apt_rwi',
						width: 50,
						disabled:true,
						listeners: 
						{ 
							
						},
						style : {
							color : 'red'
						}
					},
					{xtype: 'tbspacer',height: 3, width:10},
					{
						xtype: 'label',
						text: 'Order obat telah dilayani : ',
						style : {
							color : 'red'
						}
					},
					{xtype: 'tbspacer',height: 3, width:5},
					{
						x: 40,
						y: 40,
						xtype: 'textfield',
						name: 'txtcounttrDilayani_apt_rwi',
						id: 'txtcounttrDilayani_apt_rwi',
						width: 50,
						disabled:true,
						listeners: 
						{ 
							
						},
						style : {
							color : 'red'
						}
					},
					{xtype: 'tbspacer',height: 3, width:7},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						disabled:true,
						id: 'btnRefreshOrder_viApotekResepRWI',
						handler: function() 
						{
							viewGridOrderAll_RASEPRWI();
							total_pasien_order_mng_obtrwi();
							total_pasien_dilayani_order_mng_obtrwi();
							ordermanajemenrwi=false;
						}
					},
					{xtype: 'tbspacer',height: 3, width:10},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'label',
						text: 'Cari berdasarkan nama dan tanggal : '
					},
					{xtype: 'tbspacer',height: 3, width:5},
					{
						x: 40,
						y: 40,
						xtype: 'textfield',
						name: 'txtNamaOrder_viApotekResepRWI',
						id: 'txtNamaOrder_viApotekResepRWI',
						width: 120,
						disabled:true,
						listeners: 
						{ 
							'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										viewGridOrderAll_RASEPRWI(Ext.getCmp('txtNamaOrder_viApotekResepRWI').getValue(),Ext.getCmp('dfTglOrderApotekResepRWI').getValue());
									} 						
								}
						}
					},
					{xtype: 'tbspacer',height: 3, width:5},
					{
						xtype: 'datefield',
						id: 'dfTglOrderApotekResepRWI',
						format: 'd/M/Y',
						width: 100,
						tabIndex:3,
						disabled:true,
						value:now_viApotekResepRWI,
						listeners:
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									viewGridOrderAll_RASEPRWI(Ext.getCmp('txtNamaOrder_viApotekResepRWI').getValue(),Ext.getCmp('dfTglOrderApotekResepRWI').getValue());
								} 						
							}
						}
					},
					{xtype: 'tbspacer',height: 3, width:5},
					{
						xtype: 'label',
						text: '*) Enter untuk mencari'
					},
					{xtype: 'tbspacer',height: 3, width:580},
					
					
					//-------------- ## --------------
					
				]
			},
			viewConfig: 
			{
				forceFit: true
			}
		}
    )
	
	var pencarianApotekResepRWI = new Ext.FormPanel({
        labelAlign: 'top',
        frame:true,
        title: '',
        bodyStyle:'padding:5px 5px 0',
        //width: 600,
        items: [
		{
			layout: 'column',
			border: false,
			items:
			[
				{
					columnWidth:.98,
					layout: 'absolute',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					width: 500,
					height: 90,
					anchor: '100% 100%',
					items:
					[
						{
							x: 10,
							y: 0,
							xtype: 'label',
							text: 'No. Resep'
						},
						{
							x: 120,
							y: 0,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 0,
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_RoNumber_viApotekResepRWI',
							name: 'TxtFilterGridDataView_RoNumber_viApotekResepRWI',
							emptyText: 'No. Resep',
							width: 130,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariApotekResepRWI();
										refreshRespApotekRWI(tmpkriteria);
									} 						
								}
							}
						},
						{
							x: 10,
							y: 30,
							xtype: 'label',
							text: 'Kode/Nama Pasien'
						},
						{
							x: 120,
							y: 30,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 30,
							xtype: 'textfield',
							id: 'txtKdNamaPasienResepRWI',
							name: 'txtKdNamaPasienResepRWI',
							emptyText: 'Kode/Nama Pasien',
							width: 130,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariApotekResepRWI();
										refreshRespApotekRWI(tmpkriteria);
									} 						
								}
							}
						},
						{
							x: 10,
							y: 60,
							xtype: 'label',
							text: 'Ruangan'
						},
						{
							x: 120,
							y: 60,
							xtype: 'label',
							text: ':'
						},
						ComboUnitApotekResepRWI(),	
						
						//-------------- ## --------------
						{
							x: 310,
							y: 0,
							xtype: 'label',
							text: 'Tanggal Resep'
						},
						{
							x: 400,
							y: 0,
							xtype: 'label',
							text: ':'
						},
						{
							x: 410,
							y: 0,
							xtype: 'datefield',
							id: 'dfTglAwalApotekResepRWI',
							format: 'd/M/Y',
							width: 120,
							tabIndex:3,
							value:now_viApotekResepRWI,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariApotekResepRWI();
										refreshRespApotekRWI(tmpkriteria);
									} 						
								}
							}
						},
						{
							x: 540,
							y: 0,
							xtype: 'label',
							text: 's/d'
						},
						{
							x: 568,
							y: 0,
							xtype: 'datefield',
							id: 'dfTglAkhirApotekResepRWI',
							format: 'd/M/Y',
							width: 120,
							tabIndex:4,
							value:now_viApotekResepRWI,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariApotekResepRWI();
										refreshRespApotekRWI(tmpkriteria);
									} 						
								}
							}
						},
						{
							x: 310,
							y: 30,
							xtype: 'label',
							text: 'Posting'
						},
						{
							x: 400,
							y: 30,
							xtype: 'label',
							text: ':'
						},
						mComboStatusPostingApotekResepRWI(),
						{
							x: 310,
							y: 65,
							xtype: 'label',
							text: '*) Tekan enter untuk mencari resep'
						},
						//----------------------------------------
						{
							x: 568,
							y: 60,
							xtype: 'button',
							text: 'Cari Resep',
							iconCls: 'refresh',
							tooltip: 'Cari',
							style:{paddingLeft:'30px'},
							width:150,
							hidden : true,
							id: 'BtnFilterGridDataView_viApotekResepRWI',
							handler: function() 
							{					
								tmpkriteria = getCriteriaCariApotekResepRWI();
								refreshRespApotekRWI(tmpkriteria);
							}                        
						}
					]
				}
			]
		}
		]	
						/*				
						//-------------- ## --------------
						
						//-------------- ## --------------
				]
			} */
		
		
	})

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viApotekResepRWI = new Ext.Panel
    (
		{
			title: NamaForm_viApotekResepRWI,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viApotekResepRWI,
			region: 'center',
			layout: 'form', 
			closable: true,        
			border: false,  
			margins: '0 5 5 0',
			items: [ pencarianApotekResepRWI,
					GridDataView_viApotekResepRWI,
					GridDataViewOrderManagement_viApotekResepRWI],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viApotekResepRWI,
		            columns: 21,
		            defaults: {
					scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viApotekResepRWI;
    //-------------- # End form filter # --------------
}



function refreshRespApotekRWI(kriteria)
{
    dataSource_viApotekResepRWI.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'ViewResepApotekRWI',
                    param : kriteria
                }			
            }
        );   
    return dataSource_viApotekResepRWI;
}

function setLookUp_viApotekResepRWI(rowdata){
    var lebar = 985;
    setLookUps_viApotekResepRWI = new Ext.Window({
        id: Nci.getId(),
        title: NamaForm_viApotekResepRWI, 
        closeAction: 'destroy',        
        width: 900,
        height: 590,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'resep',
        modal: true,		
        items: getFormItemEntry_viApotekResepRWI(lebar,rowdata),
        listeners:{
            activate: function(){
				Ext.Ajax.request(
				{
					   
					url: baseURL + "index.php/apotek/currentShiftAPT/getcurrentshift",
					 params: {
						
						command: '0'
					
					},
					failure: function(o)
					{
						 var cst = Ext.decode(o.responseText);
						
					},	    
					success: function(o) {
						var cst = Ext.decode(o.responseText);
			
						tampungshiftsekarang=cst.shift
						ResepRWI.form.Panel.shift.update(cst.shift);
					}	
				
				});
				if(Ext.getCmp('cbIntegrasiResepRWI').getValue() == true){
					integrated='true';
				} else{
					integrated='false';
				}
            },
            afterShow: function(){
                this.activate();
            },
            deactivate: function(){
                rowSelected_viApotekResepRWI=undefined;
                //datarefresh_viApotekResepRWI();
				mNoKunjungan_viApotekResepRWI = '';
            }
        }
    });

    setLookUps_viApotekResepRWI.show();
	
	
    if (rowdata == undefined){
    }
    else
    {
        datainit_viApotekResepRWI(rowdata);
		ViewDetailPembayaranObatRWI(rowdata.NO_OUT,rowdata.TGL_OUT);
    }
}

function getFormItemEntry_viApotekResepRWI(lebar,rowdata){
    var pnlFormDataBasic_viApotekResepRWI = new Ext.FormPanel({
		title: '',
		region: 'north',
		layout: 'form',
		bodyStyle: 'padding:10px 10px 10px 10px',
		anchor: '100%',
		labelWidth: 1,
		autoWidth: true,
		width: lebar,
		border: false,
		items:[
				getItemPanelInputBiodata_viApotekResepRWI(lebar),
				getItemGridTransaksi_viApotekResepRWI(lebar),
				getItemGridHistoryBayar_viApotekResepRWI(lebar),
				{
					layout	: 'form',
					bodyStyle: 'margin-top: 5px;',
					border: false,
					items	: [
						{
							xtype: 'compositefield',
							fieldLabel: ' ',
							labelSeparator: '',
							items: [
									ResepRWI.form.Panel.a=new Ext.Panel ({
									region: 'north',
									border: false,
									html: '<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>'
								}),
								{
									xtype: 'displayfield',				
									width: 80,								
									value: 'Paid Status'
									//style:{'font-weight':'bold'}		
								},{
									xtype: 'displayfield',				
									width: 150,								
									value: 'Tuslah + Embalase :',
									style:{'text-align':'right','margin-left':'184px'}							
								},{
						            xtype: 'textfield',
						            id: 'txtTuslahEmbalaseL',
									style:{'text-align':'right','margin-left':'184px'},
						            width: 80,
						            value: 0,
						            readOnly: true
						        },{
									xtype: 'displayfield',				
									width: 100,								
									value: 'Adm Racik :',
									style:{'text-align':'right','margin-left':'154px'}
								},{
						            xtype: 'textfield',
						            id: 'txtAdmRacikResepRWIL',
									style:{'text-align':'right','margin-left':'154px'},
						            width: 80,
						            value: 0,
						            readOnly: true
						        },{
									xtype: 'displayfield',				
									width: 50,								
									value: 'Total :',
									style:{'text-align':'right','margin-left':'184px'}
								},{
						            xtype: 'textfield',
						            id: 'txtJumlahTotalResepRWIL',
									style:{'text-align':'right','margin-left':'184px'},
						            width: 80,
						            value: 0,
						            readOnly: true
						        }												
						    ]
						},{
							xtype: 'compositefield',
							items: [
								{ 
									xtype: 'displayfield',				
									width: 80,								
									value: 'Tanggal :',
									style:{'text-align':'right'}
								},{ 
									xtype: 'displayfield',
									id		: '',
									width: 100,								
									value: tanggal,
									format:'d/M/Y'
								},{
									xtype: 'displayfield',				
									width: 50,								
									value: 'Adm :',
									style:{'text-align':'right','margin-left':'202px'}
									
								},{
						            xtype: 'numberfield',
						            id: 'txtAdmResepRWIL',
									style:{'text-align':'right','margin-left':'202px'},
						            width: 80,
						            value: 0,
						            readOnly: true
						        },{
									xtype: 'displayfield',				
									width: 100,								
									value: 'Disc :',
									style:{'text-align':'right','margin-left':'172px'}
									
								},{
						            xtype: 'numberfield',
						            id: 'txtTotalDiscResepRWIL',
									style:{'text-align':'right','margin-left':'172px'},
						            width: 80,
						            value: 0,
						            readOnly: true
						        },{
									xtype: 'displayfield',				
									width: 90,								
									value: 'Grand Total :',
									style:{'text-align':'right','margin-left':'162px','font-weight':'bold'}
									
								},{
						            xtype: 'textfield',
						            id: 'txtTotalBayarResepRWIL',
									style:{'text-align':'right','margin-left':'162px'},
						            width: 80,
						            value: 0,
						            readOnly: true
						        }
						    ]
						},{
							xtype: 'compositefield',
							items: [
								{ 
									xtype: 'displayfield',				
									width: 80,								
									value: 'Current Shift :',
									style:{'text-align':'right'}
								},ResepRWI.form.Panel.shift=new Ext.Panel ({
									region: 'north',
									border: false
								}),{
									xtype: 'displayfield',				
									width: 150,								
									value: 'Adm Perusahaan :',
									style:{'text-align':'right','margin-left':'292px'}
									
								},{
						            xtype: 'textfield',
						            id: 'txtAdmPrshResepRWIL',
									style:{'text-align':'right','margin-left':'447px'},
						            width: 80,
						            value: 0,
						            readOnly: true
						        }
						    ]
						}
		     	   ]
				}
			],
			fileUpload: true,
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Baru',
						iconCls: 'add',
						//hidden:true,
						id: 'btnAdd_viApotekResepRWI',
						handler: function(){
							dataaddnew_viApotekResepRWI();
						}
					},{
						xtype: 'tbseparator'
					},{
						xtype: 'button',
						text: 'Dilayani',
						iconCls: 'save',
						disabled:true,
						id: 'btnSimpan_viApotekResepRWI',
						handler: function()
						{
							datasave_viApotekResepRWI();
						}
					},{
						xtype: 'tbseparator'
					},{
						xtype: 'button',
						text: 'Dilayani & Tutup',
						iconCls: 'saveexit',
						disabled:true,
						hidden:true,
						id: 'btnSimpanExit_viApotekResepRWI',
						handler: function()
						{
							/* var x = datasave_viApotekResepRWI(addNew_viApotekResepRWI);
							datarefresh_viApotekResepRWI();
							if (x===undefined)
							{
								setLookUps_viApotekResepRWI.close();
							} */
							datasave_viApotekResepRWI();
							refreshRespApotekRWI();
							setLookUps_viApotekResepRWI.close();
						}
					},{
						xtype: 'tbseparator',
						hidden:true,
					},
					{
						xtype: 'button',
						text: 'Bayar',
						id: 'btnBayar_viApotekResepRWI',
						iconCls: 'gantidok',
						disabled:true,
						handler: function(){
							setLookUp_bayarResepRWI();
						}
					},
					{
						xtype:'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Transfer',
						id:'btnTransfer_viApotekResepRWI',
						iconCls: 'gantidok',
						disabled:true,
						handler: function(){
							setLookUp_TransferResepRWI();
						}
					},
					{
						xtype:'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Unposting',
						id:'btnunposting_viApotekResepRWI',
						iconCls: 'gantidok',
						disabled:true,
						handler:function()
						{
							cekTransferRWI(Ext.getCmp('txtTmpNooutResepRWIL').getValue(),Ext.getCmp('txtTmpTgloutResepRWIL').getValue())
						}  
					},
					{
						xtype: 'button',
						text: 'Hapus Bayar',
						id:'btnDeleteHistory_viApotekResepRWI',
						iconCls: 'remove',
						disabled:true,
						handler:function()
						{
							if(dsTRDetailHistoryBayar.getCount()>0){
								Ext.Msg.confirm('Warning', 'Apakah data pembayaran ini akan di hapus?', function(button){
									if (button == 'yes'){
										Ext.Ajax.request
										(
											{
												url: baseURL + "index.php/apotek/functionAPOTEKrwi/deleteHistoryResepRWJ",
												params: getParamDeleteHistoryResepRWI(),
												failure: function(o)
												{
													ShowPesanErrorResepRWI('Hubungi Admin', 'Error');
												},	
												success: function(o) 
												{
													var cst = Ext.decode(o.responseText);
													if (cst.success === true) 
													{
														ShowPesanInfoResepRWI('Penghapusan berhasil','Information');
														ViewDetailPembayaranObatRWI(Ext.getCmp('txtTmpNooutResepRWIL').getValue(),Ext.getCmp('txtTmpTgloutResepRWIL').getValue());
														gridDTLTRHistoryApotekRWI.getView().refresh();
														Ext.getCmp('btnBayar_viApotekResepRWI').enable();
														Ext.getCmp('btnDelete_viApotekResepRWI').enable();
														//Ext.getCmp('btnTransfer_viApotekResepRWI').enable();
													}
													else 
													{
														ShowPesanErrorResepRWI('Gagal menghapus pembayaran', 'Error');
														ViewDetailPembayaranObatRWI(Ext.getCmp('txtTmpNooutResepRWIL').getValue(),Ext.getCmp('txtTmpTgloutResepRWIL').getValue());
														gridDTLTRHistoryApotekRWI.getView().refresh();
													};
												}
											}
											
										)
									}
								});
							} else{
								ShowPesanErrorResepRWI('Belum melakukan pembayaran','Error');
							}
						}  
					},
					{
						xtype:'splitbutton',
						text:'Cetak',
						iconCls:'print',
						id:'btnPrint_viResepRWI',
						disabled:true,
						handler:function()
						{							
								
						},
						menu: new Ext.menu.Menu({
						items: [
							// these items will render as dropdown menu items when the arrow is clicked:
							{
								xtype: 'button',
								text: 'Print Bill',
								id: 'btnPrintBillResepRWI',
								handler: function()
								{  
									PrintBillResepRWI='true';
									panelnew_window_printer_resepRWI();
								}
							},
							{
								xtype: 'button',
								text: 'Print Kwitansi',
								id: 'btnPrintKwitansiResepRWI',
								handler: function()
								{
									PrintBillResepRWI='false';
									panelPrintKwitansi_resepRWI()
								}
							},
						]
						})
					},
					{
						xtype:'tbseparator'
					},//mCombo_printer_rwi(),
					{
					xtype: 'label',
					text: 'Order dari Rwi : ' 
					},
					mComboorder_rwi(),
					{
						xtype: 'button',
						text: 'close order',
						id:'statusservice_apt_RWI',
						iconCls: 'gantidok',
						disabled:true,
						handler: function(){
							updatestatus_permintaan()
							load_data_pasienorder();
							//ResepRWI.form.Grid.a.store.removeAll()
							Ext.getCmp('statusservice_apt_RWI').disable();
						}
					}
					
				]
			}//,items:
		}
    )

    return pnlFormDataBasic_viApotekResepRWI;
}


function getItemPanelInputBiodata_viApotekResepRWI(lebar) {
    var items ={
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		labelWidth: 100,
		autoWidth: true,
		border:true,
		items:
		[			
			{
                xtype: 'compositefield',
                fieldLabel: 'No Resep',
                width: 730,
                items:
				[                    
					{
						xtype: 'textfield',
						id: 'txtNoResepApotekResepRWIL',
						readOnly: true,
						emptyText: 'Nomor Resep',
						width: 130
					},
					{
						xtype: 'displayfield',
						width: 45
					},
					{
						xtype: 'displayfield',
						width: 100,
						value: 'Jenis Pasien :'
					},
					ComboPilihanKelompokPasienApotekResepRWI(),
					{
						xtype: 'displayfield',
						width: 25
					},
					{
						xtype: 'displayfield',
						width: 50,
						value: 'Kamar :'
					},
					{
						xtype: 'textfield',
						id: 'txtNoKamarApotekResepRWIL',
						readOnly	: true,
						width: 50,
						emptyText: 'No kamar'
					},
					{
						xtype: 'textfield',
						width : 120,	
						id: 'txtKamarApotekResepRWIL',
						readOnly: true,
						emptyText: 'Kamar'
					}
                ]
            },
			{
				xtype: 'compositefield',
				fieldLabel: 'No. Medrec',
				anchor: '100%',
				width: 199,
				items: 
				[
					mComboKodePasienResepRWI(),
/* 					{
						xtype: 'textfield',
						id: 'cboKodePasienResepRWI',
						readOnly	: true,
						emptyText: 'No medrec',
						width: 130
					},
 */					/* ResepRWI.form.ComboBox.kodepasien= new Nci.form.Combobox.autoComplete({
						store	: ResepRWI.form.ArrayStore.kodepasien,
						select	: function(a,b,c){
							Ext.getCmp('cbo_DokterApotekRes
							epRWI').setValue(b.data.kd_dokter);
							ResepRWI.form.ComboBox.namaPasien.setValue(b.data.nama);
							Ext.getCmp('cbo_UnitResepRWIL').setValue(b.data.kd_unit);
							Ext.getCmp('cboPilihankelompokPasienAptResepRWI').setValue(b.data.kd_customer);
							Ext.getCmp('txtNoKamarApotekResepRWIL').setValue(b.data.no_kamar);
							Ext.getCmp('txtKamarApotekResepRWIL').setValue(b.data.nama_kamar);
							ResepRWI.vars.no_transaksi=b.data.no_transaksi;
							ResepRWI.vars.kd_kasir=b.data.kd_kasir;
							Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').setValue(b.data.kd_customer);
							Ext.getCmp('txtTmpKdDokterApotekResepRWIL').setValue(b.data.kd_dokter);
							Ext.getCmp('txtTmpKdUnitApotekResepRWIL').setValue(b.data.kd_unit);
							
						},
						width	: 130,
						insert	: function(o){
							return {
								kd_pasien        	:o.kd_pasien,
								nama        		:o.nama,
								kd_dokter			:o.kd_dokter,
								kd_unit				:o.kd_unit,
								kd_customer			:o.kd_customer,
								no_transaksi		:o.no_transaksi,
								kd_kasir			:o.kd_kasir,
								nama_kamar			:o.nama_kamar,
								no_kamar			:o.no_kamar,
								text				:  '<table style="font-size: 11px;"><tr><td width="80">'+o.kd_pasien+'</td><td width="180">'+o.nama+'</td></tr></table>',
								nama	 			:o.nama
							}
						},
						url		: baseURL + "index.php/apotek/functionAPOTEKrwi/getKodePasienResepRWI",
						valueField: 'kd_pasien',
						displayField: 'text',
						listWidth: 260,
						emptyText: 'No medrec'
					}), */
					{
						xtype: 'displayfield',
						width: 45
					},
					{
						xtype: 'displayfield',
						width: 100,
						value: 'Dokter :'
					},
					ComboDokterApotekResepRWI(),
					{
						xtype: 'displayfield',
						width: 25
					},
					{
						xtype: 'checkbox',
						boxLabel: 'Integrasi',
						id: 'cbIntegrasiResepRWI',
						name: 'cbIntegrasiResepRWI',
						width: 80,
						checked: true,
						handler:function(a,b) 
						{
							if(a.checked==true){
								
							}else{
							}
						}
					},
					{
						xtype: 'displayfield',
						width: 80
					},
					
				]
			},{
				xtype: 'compositefield',
				fieldLabel: 'Nama Pasien',
				anchor: '100%',
				width: 199,
				items: 
				[
					ResepRWI.form.ComboBox.namaPasien= new Nci.form.Combobox.autoComplete({
						store	: ResepRWI.form.ArrayStore.pasien,
						select	: function(a,b,c){
							Ext.getCmp('cbo_DokterApotekResepRWI').setValue(b.data.nama_dokter);
							Ext.getCmp('cboKodePasienResepRWI').setValue(b.data.kd_pasien);
							Ext.getCmp('cbo_UnitResepRWIL').setValue(b.data.nama_unit);
							Ext.getCmp('cboPilihankelompokPasienAptResepRWI').setValue(b.data.customer);
							Ext.getCmp('txtNoKamarApotekResepRWIL').setValue(b.data.no_kamar);
							Ext.getCmp('txtKamarApotekResepRWIL').setValue(b.data.nama_kamar);
							ResepRWI.vars.no_transaksi=b.data.no_transaksi;
							ResepRWI.vars.tgl_transaksi=b.data.tgl_transaksi;
							ResepRWI.vars.kd_kasir=b.data.kd_kasir;
							ResepRWI.vars.urut_masuk=b.data.urut_masuk;
							Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').setValue(b.data.kd_customer);
							Ext.getCmp('txtTmpKdDokterApotekResepRWIL').setValue(b.data.kd_dokter);
							Ext.getCmp('txtTmpKdUnitApotekResepRWIL').setValue(b.data.kd_unit);
							Ext.getCmp('btnAddObatRWI').enable();
							Ext.getCmp('btnSimpan_viApotekResepRWI').enable();
							Ext.getCmp('btnSimpanExit_viApotekResepRWI').enable();
							Ext.getCmp('btnDelete_viApotekResepRWI').enable();
							
							/* kd_spesial_tr=b.data.kd_spesial;
							kd_unit_kamar_tr=b.data.kd_unit_kamar;
							no_kamar_tr=b.data.no_kamar;
							ordermanajemenrwi=false; */
							
							var records = new Array();
							records.push(new dsDataGrdJab_viApotekResepRWI.recordType());
							dsDataGrdJab_viApotekResepRWI.add(records);
							row=dsDataGrdJab_viApotekResepRWI.getCount()-1; // menujukan jumlah baris,startEditing(baris dimulai dari 1, kolom 1)
							ResepRWI.form.Grid.a.startEditing(row, 3);	
							Ext.getCmp('btnSimpan_viApotekResepRWI').enable();
							Ext.getCmp('btnSimpanExit_viApotekResepRWI').enable();
							Ext.getCmp('btnBayar_viApotekResepRWI').disable();
							Ext.getCmp('btnDelete_viApotekResepRWI').enable();
							
						},
						width	: 180,
						insert	: function(o){
							return {
								kd_pasien        	:o.kd_pasien,
								nama        		:o.nama,
								kd_dokter			:o.kd_dokter,
								nama_unit			:o.nama_unit,
								nama_dokter			:o.nama_dokter,
								kd_unit				:o.kd_unit,
								kd_customer			:o.kd_customer,
								no_transaksi		:o.no_transaksi,
								kd_kasir			:o.kd_kasir,
								nama_kamar			:o.nama_kamar,
								no_kamar			:o.no_kamar,
								tgl_transaksi		:o.tgl_transaksi,
								kd_spesial     		:o.kd_spesial,
								kd_unit_kamar      	:o.kd_unit_kamar,
								urut_masuk			:o.urut_masuk,
								customer			:o.customer,
								text				:  '<table style="font-size: 11px;"><tr><td width="80">'+o.kd_pasien+'</td><td width="180">'+o.nama+'</td></tr></table>',
								nama	 			:o.nama
							}
						},
						param:function(){
							if(Ext.getCmp('cbIntegrasiResepRWI').getValue() == true){
								integrated='true';
							} else{
								integrated='false';
							}
							return {
								integrasi:integrated
							}
						},
						url		: baseURL + "index.php/apotek/functionAPOTEKrwi/getPasienResepRWI",
						valueField: 'nama',
						displayField: 'text',
						listWidth: 260,
						emptyText: 'Nama Pasien'
					}),
					{
						xtype: 'displayfield',
						width: 100,
						value: 'Nama Unit :'
					},
					ComboUnitResepRWILookup(),
					{
						xtype: 'displayfield',
						width: 25
					},
					{
						xtype:'button',
						text:'1/2 Resep',
						width:70,
						hideLabel:true,
						//hidden:true,
						id: 'btn1/2resep_viApotekResepRWI',
						handler:function()
						{
							hitungSetengahResepRWI();
						}   
					},
					{
						xtype: 'displayfield',
						width: 10
					},
					{
						xtype:'button',
						text:'Racikan',
						width:70,
						hideLabel:true,
						//hidden:true,
						disabled:true,
						id: 'btnRacikan_viApotekResepRWI',
						handler:function()
						{
							formulaRacikanResepRWI();
						}   
					},
					/* {
						xtype: 'button',
						width : 70,	
						text	: '1/2 Resep',
						id: Nci.getId()
					}, */
					//HIDDEN
					{
						xtype: 'textfield',
						width : 120,	
						id: 'txtTmpKdCustomerApotekResepRWIL',
						readOnly: true,
						emptyText: 'Kode Pasien',
						hidden:true
					},
					{
						xtype: 'textfield',
						width : 120,	
						id: 'txtTmpKdDokterApotekResepRWIL',
						readOnly: true,
						emptyText: 'kode Dokter',
						hidden:true
					},
					{
						xtype: 'textfield',
						width : 120,	
						id: 'txtTmpKdUnitApotekResepRWIL',
						readOnly: true,
						emptyText: 'Kode Unit',
						hidden:true
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtTmpNooutResepRWIL',
						id: 'txtTmpNooutResepRWIL',
						emptyText: 'No out',
						hidden:true
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtTmpTgloutResepRWIL',
						id: 'txtTmpTgloutResepRWIL',
						emptyText: 'tgl out',
						hidden:true
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtTmpStatusPostResepRWIL',
						id: 'txtTmpStatusPostResepRWIL',
						emptyText: 'Status post',
						hidden:true
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtTmpSisaAngsuranResepRWIL',
						id: 'txtTmpSisaAngsuranResepRWIL',
						emptyText: 'sisa angsuran',
						hidden:true
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtTmpTotQtyResepRWIL',
						id: 'txtTmpTotQtyResepRWIL',
						emptyText: 'total qty',
						hidden:true
					}
				]
			},
			{
                xtype: 'compositefield',
                fieldLabel: 'Tanggal Resep',
                width: 730,
                items:
				[                    
					{
						xtype: 'datefield',
						id:'dfTglResepSebenarnyaResepRWI',
						width : 130,	
						format: 'd/M/Y',
						value: now_viApotekResepRWI
					},
					{
						xtype: 'label',
						text:'*) Jika resep di input bukan pada tanggal di buatnya resep, isi Tanggal Resep sesuai dengan tanggal dibuatnya resep',
						width : 300,
						style : {
							color : 'darkblue',
							'font-size':'10px'
						}
					},
                ]
            },
		]
	};
    return items;
};

function getItemGridTransaksi_viApotekResepRWI(lebar){
    var items ={
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'margin-top: -1px;',
		border:false,
		width: lebar-80,
		height: 205,//300, 
	    tbar:
		[
			{
				text	: 'Tambah Obat',
				id		: 'btnAddObatRWI',
				tooltip	: nmLookup,
				disabled: true,
				iconCls	: 'find',
				handler	: function(){
					var records = new Array();
					Ext.getCmp('btnSimpan_viApotekResepRWI').enable();
					Ext.getCmp('btnSimpanExit_viApotekResepRWI').enable();
					Ext.getCmp('btnBayar_viApotekResepRWI').disable();
					Ext.getCmp('btnDelete_viApotekResepRWI').enable();
					records.push(new dsDataGrdJab_viApotekResepRWI.recordType());
					dsDataGrdJab_viApotekResepRWI.add(records);
					var kd_customer=Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').getValue();
					if(kd_customer ==='' ||kd_customer ==='Kelompok Pasien'){
						
					}else{
						getAdm(kd_customer);
					}
					Ext.getCmp('btnRacikan_viApotekResepRWI').disable();
					var line = dsDataGrdJab_viApotekResepRWI.getCount()-1;
					ResepRWI.form.Grid.a.startEditing(line, 3);	
				}
			},{
				xtype: 'tbseparator'
			},{
				xtype: 'button',
				text: 'Hapus',
				iconCls: 'remove',
				disabled:true,
				id: 'btnDelete_viApotekResepRWI',
				handler: function()
				{
					var line = ResepRWI.form.Grid.a.getSelectionModel().selection.cell[0];
					var o = dsDataGrdJab_viApotekResepRWI.getRange()[line].data;
					if(dsDataGrdJab_viApotekResepRWI.getCount()>1){
						Ext.Msg.confirm('Warning', 'Apakah data resep obat ini akan dihapus?', function(button){
							if (button == 'yes'){
								if(dsDataGrdJab_viApotekResepRWI.getRange()[line].data.no_out != undefined){
									Ext.Ajax.request
									(
										{
											url: baseURL + "index.php/apotek/functionAPOTEKrwi/hapusBarisGridResepRWI",
											params:{no_out:o.no_out, tgl_out:o.tgl_out, kd_prd:o.kd_prd, kd_milik:o.kd_milik, no_urut:o.no_urut} ,
											failure: function(o)
											{
												ShowPesanErrorResepRWI('Hubungi Admin', 'Error');
											},	
											success: function(o) 
											{
												var cst = Ext.decode(o.responseText);
												if (cst.success === true) 
												{
													dsDataGrdJab_viApotekResepRWI.removeAt(line);
													ResepRWI.form.Grid.a.getView().refresh();
													hasilJumlahResepRWI();
													Ext.getCmp('btnSimpan_viApotekResepRWI').enable();
													Ext.getCmp('btnSimpanExit_viApotekResepRWI').enable();
													Ext.getCmp('btnBayar_viApotekResepRWI').disable();
													hasilJumlahResepRWI();
													
												}
												else 
												{
													ShowPesanErrorResepRWI('Gagal melakukan penghapusan', 'Error');
												};
											}
										}
										
									)
								}else{
									dsDataGrdJab_viApotekResepRWI.removeAt(line);
									ResepRWI.form.Grid.a.getView().refresh();
									Ext.getCmp('btnSimpan_viApotekResepRWI').enable();
									Ext.getCmp('btnSimpanExit_viApotekResepRWI').enable();
								}
							} 
							
						});
					} else{
						ShowPesanErrorResepRWI('Data tidak bisa dihapus karena minimal resep 1 obat','Error');
					}
					
				}
			}	
		],
		items:[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:[
					gridDataViewEdit_viApotekResepRWI()
				]	
			}
		]
	};
    return items;
};

function setLookUp_bayarResepRWI(rowdata)
{
    var lebar = 450;
	var stmpnoOut=0;
	var stmptgl;
    setLookUpApotek_bayarResepRWI = new Ext.Window
    (
		{
			id: 'setLookUpApotek_bayarResepRWI',
			name: 'setLookUpApotek_bayarResepRWI',
			title: 'Pembayaran Resep Rawat Inap', 
			closeAction: 'destroy',        
			width: 523,
			height: 230,
			resizable:false,
			emptyText:'Pilih Jenis Pembayaran...',
			autoScroll: false,
			border: true,
			constrain : true,    
			iconCls: 'Studi_Lanjut',
			modal: true,		
			items: [ getItemPanelBiodataPembayaran_viApotekResepRWI(lebar,rowdata),
					 getItemPanelBiodataUang_viApotekResepRWI(lebar,rowdata)
				   ],//1
			listeners:
			{
				activate: function()
				{
					
				},
				afterShow: function()
				{
					this.activate();
					
					// ;
				},
				deactivate: function()
				{
					//rowSelected_viApotekResepRWI=undefined;
				}
			}
		}
    );

    setLookUpApotek_bayarResepRWI.show();
	
	
	Ext.getCmp('txtNamaPasien_PembayaranRWI').setValue(ResepRWI.form.ComboBox.namaPasien.getValue());
	
	Ext.getCmp('txtkdPasien_PembayaranRWI').setValue(Ext.getCmp('cboKodePasienResepRWI').getValue());
	Ext.getCmp('txtNoResepRWI_Pembayaran').setValue(Ext.get('txtNoResepApotekResepRWIL').getValue());
	Ext.getCmp('dftanggalResepRWI_Pembayaran').setValue(now_viApotekResepRWI);
	stmpnoOut=Ext.getCmp('txtTmpNooutResepRWIL').getValue();
	stmptgl=Ext.getCmp('txtTmpTgloutResepRWIL').getValue();
	if(Ext.getCmp('txtTmpNooutResepRWIL').getValue() == 'No out' || Ext.getCmp('txtTmpNooutResepRWIL').getValue() == ''){
		Ext.getCmp('txtTotalResepRWI_Pembayaran').setValue(Ext.getCmp('txtTotalBayarResepRWIL').getValue());
		Ext.getCmp('txtBayarResepRWI_Pembayaran').setValue(Ext.getCmp('txtTotalBayarResepRWIL').getValue());
	}else{
		getSisaAngsuran(stmpnoOut,stmptgl);
	}
	if(Ext.getCmp('cboPilihankelompokPasienAptResepRWI').getValue() == 'BPJS'){
		Ext.getCmp('cboJenisByrResepRWI').setValue('ASURANSI');
		Ext.getCmp('cboPembayaranRWI').setValue('BPJS');
	} 
}
// End Function setLookUpGridDataView_viApotekResepRWJ # --------------

function getItemPanelBiodataPembayaran_viApotekResepRWI(lebar,rowdata) 
{ 
	var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		labelWidth: 100,
		autoWidth: true,
		border:true,
		items:
		[			
			{
                xtype: 'compositefield',
                fieldLabel: 'No. Resep ',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtNoResepRWI_Pembayaran',
						id: 'txtNoResepRWI_Pembayaran',
						emptyText: 'No Resep',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},
					{
						xtype: 'displayfield',
						flex: 1,
						width: 70,
						name: '',
						value: 'Tanggal :',
						fieldLabel: 'Label'
					},
					{
						xtype: 'datefield',
						flex: 1,
						width : 150,	
						format: 'd/M/Y',
						name: 'dftanggalResepRWI_Pembayaran',
						id: 'dftanggalResepRWI_Pembayaran',
						readOnly:true
					}
				
                ]
            },
            //-------------- ## --------------  
			{
                xtype: 'compositefield',
                fieldLabel: 'Kode Pasien ',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					//-------------- ## --------------  
					{
						xtype: 'textfield',
						width : 150,	
						name: 'txtkdPasien_PembayaranRWI',
						id: 'txtkdPasien_PembayaranRWI',
						readOnly:true
					},	
					{
						xtype: 'textfield',
						width : 225,	
						name: 'txtNamaPasien_PembayaranRWI',
						id: 'txtNamaPasien_PembayaranRWI',
						readOnly:true
					}
					
                ]
            },
			{
				xtype: 'compositefield',
				fieldLabel: 'Pembayaran ',
				anchor: '100%',
				//labelSeparator: '',
				width: 199,
				items: 
				[
					mComboJenisByrResepRWI(),
					mComboPembayaranRWI()
				]
			}
					
		]
	};
    return items;
};



function getItemPanelBiodataUang_viApotekResepRWI(lebar,rowdata) 
{ 
	var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		labelWidth: 100,
		autoWidth: true,
		border:true,
		items:
		[			
			{
                xtype: 'compositefield',
                fieldLabel: ' ',
				labelSeparator: '',
                anchor: '100%',
                width: 250,
                items: 
                [         
					{
						xtype: 'displayfield',
						flex: 1,
						width: 150,
						name: '',
						value: ''
					},	
					{
						xtype: 'displayfield',
						flex: 1,
						width: 70,
						name: '',
						value: 'Total :',
						fieldLabel: 'Label'
					},
					{
						xtype: 'textfield',
						width : 150,	
						readOnly: true,
						style:{'text-align':'right'},
						name: 'txtTotalResepRWI_Pembayaran',
						id: 'txtTotalResepRWI_Pembayaran',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					}
					
                ]
            },
			{
                xtype: 'compositefield',
                fieldLabel: ' ',
				labelSeparator: '',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					//-------------- ## -------------- 
					{
						xtype: 'displayfield',
						flex: 1,
						width: 150,
						name: '',
						value: ''
					},	
					{
						xtype: 'displayfield',
						flex: 1,
						width: 70,
						name: '',
						value: 'Bayar :',
						fieldLabel: 'Label'
					},						
					{
						xtype: 'numberfield',
						width : 150,	
						//readOnly: true,
						style:{'text-align':'right'},
						name: 'txtBayarResepRWI_Pembayaran',
						id: 'txtBayarResepRWI_Pembayaran',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									pembayaranResepRWI();
								};
							}
						}
					},
					{
						xtype:'button',
						text:'1/2 Resep',
						width:70,
						//style:{'margin-left':'190px','margin-top':'7px'},
						hideLabel:true,
						hidden:true,
						id: 'btn1/2resep_viApotekResepRWI',
						handler:function()
						{
						}   
					}
                ]
            },
			{
                xtype: 'compositefield',
                fieldLabel: ' ',
				labelSeparator: '',
                anchor: '100%',
                width: 230,
                items: 
                [
				{
						xtype: 'displayfield',
						flex: 1,
						width: 305,
						name: '',
						value: ''
					},
					{
						xtype:'button',
						text:'Paid',
						width:70,
						//style:{'margin-left':'190px','margin-top':'7px'},
						hideLabel:true,
						id: 'btnBayar_ResepRWILookupBayar',
						handler:function()
						{
							pembayaranResepRWI();
						}   
					}
				]
            }
		]
	};
    return items;
};


function setLookUp_TransferResepRWI(rowdata)
{
    var lebar = 450;
	var stmpnoOut=0;
	var stmptgl;
    setLookUpApotek_TransferResepRWI = new Ext.Window
    (
    {
        id: 'setLookUpApotek_transferResepRWI',
		name: 'setLookUpApotek_transferResepRWI',
        title: 'Transfer Pembayaran Resep', 
        closeAction: 'destroy',        
        width: 523,
        height: 240,
        resizable:false,
		autoScroll: false,
        border: true,
        constrain : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: [ 
			getItemPanelBiodataTransfer_viApotekResepRWI(),
			getItemPanelTotalBayar_ApotekResepRWI()
		],//1
		fbar:[
			{
				xtype:'button',
				text:'Transfer',
				width:70,
				hideLabel:true,
				id: 'btnTransfer_viApotekResepRWIL',
				handler:function()
				{
					transferResepRWI();
				}   
			},
			{
				xtype:'button',
				text:'Batal',
				width:70,
				hideLabel:true,
				id: 'btnBatalTransfer_viApotekResepRWIL',
				handler:function()
				{
					setLookUpApotek_TransferResepRWI.close();
				}   
			}
		],
        listeners:
        {
            activate: function()
            {
				
            },
            afterShow: function()
            {
                this.activate();
				
				// ;
            },
            deactivate: function()
            {
                rowSelected_viApotekResepRWI=undefined;
            }
        }
    }
    );

    setLookUpApotek_TransferResepRWI.show();
	Ext.getCmp('txtNamaPasienTransfer_ResepRWI').setValue(ResepRWI.form.ComboBox.namaPasien.getValue());
	
	Ext.getCmp('txtNoTransaksiTransfer_ResepRWI').setValue(ResepRWI.vars.no_transaksi);
	Ext.getCmp('dftanggalTransaksi_ResepRWI').setValue(ShowDate(ResepRWI.vars.tgl_transaksi));
	
	Ext.getCmp('txtkdPasienTransfer_ResepRWI').setValue(Ext.getCmp('cboKodePasienResepRWI').getValue());
	Ext.getCmp('txtNoResepTransfer_ResepRWI').setValue(Ext.get('txtNoResepApotekResepRWIL').getValue());
	Ext.getCmp('dftanggalTransfer_ResepRWI').setValue(now_viApotekResepRWI);
	stmpnoOut=Ext.getCmp('txtTmpNooutResepRWIL').getValue();
	stmptgl=Ext.getCmp('txtTmpTgloutResepRWIL').getValue();
	Ext.getCmp('txtTotalBayarTransfer_ResepRWI').setValue(Ext.getCmp('txtTotalBayarResepRWIL').getValue());
	Ext.getCmp('txtTotalTransfer_ResepRWI').setValue(toInteger(Ext.getCmp('txtTotalBayarResepRWIL').getValue()));
	
}
// End Function setLookUpGridDataView_viApotekResepRWJ # --------------

function getItemPanelBiodataTransfer_viApotekResepRWI(lebar,rowdata) 
{ 
	var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		labelWidth: 100,
		autoWidth: true,
		border:true,
		items:
		[			
			{
                xtype: 'compositefield',
                fieldLabel: 'No. Transaksi ',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtNoTransaksiTransfer_ResepRWI',
						id: 'txtNoTransaksiTransfer_ResepRWI',
						emptyText: 'No Transaksi',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},
					{
						xtype: 'displayfield',
						flex: 1,
						width: 90,
						name: '',
						value: 'Tgl Transaksi :',
						fieldLabel: 'Label'
					},
					{
						xtype: 'datefield',
						flex: 1,
						width : 130,	
						format: 'd/M/Y',
						name: 'dftanggalTransaksi_ResepRWI',
						id: 'dftanggalTransaksi_ResepRWI',
						readOnly:true
					}
				
                ]
            },
			{
                xtype: 'compositefield',
                fieldLabel: 'No. Resep ',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtNoResepTransfer_ResepRWI',
						id: 'txtNoResepTransfer_ResepRWI',
						emptyText: 'No Resep',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},
					{
						xtype: 'displayfield',
						flex: 1,
						width: 70,
						name: '',
						value: 'Tanggal :',
						fieldLabel: 'Label'
					},
					{
						xtype: 'datefield',
						flex: 1,
						width : 150,	
						format: 'd/M/Y',
						name: 'dftanggalTransfer_ResepRWI',
						id: 'dftanggalTransfer_ResepRWI',
						readOnly:true
					}
				
                ]
            },
            //-------------- ## --------------  
			{
                xtype: 'compositefield',
                fieldLabel: 'Pasien ',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					//-------------- ## --------------  
					{
						xtype: 'textfield',
						width : 150,	
						name: 'txtkdPasienTransfer_ResepRWI',
						id: 'txtkdPasienTransfer_ResepRWI',
						readOnly:true
					},	
					{
						xtype: 'textfield',
						width : 225,	
						name: 'txtNamaPasienTransfer_ResepRWI',
						id: 'txtNamaPasienTransfer_ResepRWI',
						readOnly:true
					}
					
                ]
            }
					
		]
	};
    return items;
};



function getItemPanelTotalBayar_ApotekResepRWI(lebar,rowdata) 
{ 
	var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		labelWidth: 100,
		autoWidth: true,
		border:true,
		items:
		[			
			{
                xtype: 'compositefield',
                fieldLabel: ' ',
				labelSeparator: '',
                anchor: '100%',
                width: 250,
                items: 
                [         
					{
						xtype: 'displayfield',
						flex: 1,
						width: 150,
						name: '',
						value: ''
					},	
					{
						xtype: 'displayfield',
						flex: 1,
						width: 90,
						name: '',
						value: 'Total biaya:',
						fieldLabel: 'Label'
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 130,	
						readOnly: true,
						style:{'text-align':'right'},
						name: 'txtTotalBayarTransfer_ResepRWI',
						id: 'txtTotalBayarTransfer_ResepRWI',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									transferResepRWJ();
								};
							}
						}
					}
					
                ]
            },
			{
                xtype: 'compositefield',
                fieldLabel: ' ',
				labelSeparator: '',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					//-------------- ## -------------- 
					{
						xtype: 'displayfield',
						flex: 1,
						width: 150,
						name: '',
						value: ''
					},	
					{
						xtype: 'displayfield',
						flex: 1,
						width: 90,
						name: '',
						value: 'Total transfer :',
						fieldLabel: 'Label'
					},						
					{
						xtype: 'textfield',
						flex: 1,
						width : 130,	
						readOnly: true,
						style:{'text-align':'right'},
						name: 'txtTotalTransfer_ResepRWI',
						id: 'txtTotalTransfer_ResepRWI',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									transferResepRWI();
								};
							}
						}
					},
					
                ]
            },
		]
	};
    return items;
};



function getItemGridHistoryBayar_viApotekResepRWI(lebar) 
{
    var items =
	{
		title: 'Histori Bayar', 
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
		bodyStyle: 'margin-top: -1px;',
	    //bodyStyle: 'padding:1px 1px 1px 1px',
		border:true,
		width: lebar-80,
		height: 95,//300, 
	    items:
		[
		
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
				items:
				[
					gridDataViewHistoryBayar_viApotekResepRWI()
				]	
			}
			//-------------- ## --------------
		]
	};
    return items;
};

function gridDataViewHistoryBayar_viApotekResepRWI() 
{

    var fldDetail = ['TUTUP','KD_PASIENAPT','TGL_OUT','NO_OUT','URUT','TGL_BAYAR','KD_PAY','URAIAN','JUMLAH','JML_TERIMA_UANG','SISA'];
	
    dsTRDetailHistoryBayar = new WebApp.DataStore({ fields: fldDetail })
		 
    gridDTLTRHistoryApotekRWI = new Ext.grid.EditorGridPanel
    (
        {
            store: dsTRDetailHistoryBayar,
            border: false,
            columnLines: true,
            height: 72,
			autoScroll:true,
            selModel: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                            //cellSelecteddeskripsiRWI = dsTRDetailHistoryBayarList.getAt(row);
                            //CurrentHistoryRWI.row = row;
                            //CurrentHistoryRWI.data = cellSelecteddeskripsiRWI;
                        }
                    }
                }
            ),
            stripeRows: true,
            columns:[
            			{
							id: 'colStatPost',
							header: 'Status Posting',
							dataIndex: 'TUTUP',
							width:100,
							hidden:true
						},
						{
							id: 'colKdPsien',
							header: 'Kode Pasien',
							dataIndex: 'KD_PASIENAPT',
							width:100,
							hidden:true
						},
						{
							id: 'colNoOut',
							header: 'No out',
							dataIndex: 'NO_OUT',
							width:100,
							hidden:true
						},
						{
							id: 'coleurutmasuk',
							header: 'Urut Bayar',
							dataIndex: 'URUT',
							align :'center',
							width:90
							
						},
						{
							id: 'colTGlout',
							header: 'Tanggal Resep',
							dataIndex: 'TGL_OUT',
							align :'center',
							width:130,
							renderer: function(v, params, record)
							{
							   return ShowDate(record.data.TGL_OUT);
				
							} 
						},
						{
							id: 'colePembayaran',
							header: 'Pembayaran',
							dataIndex: 'URAIAN',
							align :'center',
							width:120,
							hidden:false
							
						},
						{
							id: 'colTGlBayar',
							header: 'Tanggal Bayar',
							dataIndex: 'TGL_BAYAR',
							align :'center',
							width:130,
							renderer: function(v, params, record)
							{
							   return ShowDate(record.data.TGL_BAYAR);
				
							} 
						},
						{
							id: 'colJumlah',
							header: 'Total Bayar',
							width:130,
							xtype:'numbercolumn',
							align :'right',
							dataIndex: 'JUMLAH',
							renderer: function(v, params, record)
							{
							   return formatCurrency(record.data.JUMLAH);
				
							}
							
						},
						{
							id: 'colJumlahTerimaUang',
							header: 'Jumlah Angsuran',
							width:130,
							xtype:'numbercolumn',
							align :'right',
							dataIndex: 'JML_TERIMA_UANG',
							renderer: function(v, params, record)
							{
							   return formatCurrency(record.data.JML_TERIMA_UANG);
				
							}
							
						},
						{
							id: 'colSisa',
							header: 'Sisa',
							width:130,
							xtype:'numbercolumn',
							align :'right',
							dataIndex: 'SISA',
							renderer: function(v, params, record)
							{
							   return formatCurrency(record.data.SISA);
				
							}
							
						}
            ],
			viewConfig: {forceFit: true}
            //cm: TRHistoryColumModelApotekRWI(),
			
	 
		}
    );
    return gridDTLTRHistoryApotekRWI;
};

function TRHistoryColumModelApotekRWI() 
{
    return new Ext.grid.ColumnModel
    (
        [
          	{
				id: 'colStatPost',
				header: 'Status Posting',
				dataIndex: 'TUTUP',
				width:100,
				hidden:true
			},
			{
				id: 'colKdPsien',
				header: 'Kode Pasien',
				dataIndex: 'KD_PASIENAPT',
				width:100,
				hidden:true
			},
			{
				id: 'colNoOut',
				header: 'No out',
				dataIndex: 'NO_OUT',
				width:100,
				hidden:true
			},
			{
				id: 'coleurutmasuk',
				header: 'Urut Bayar',
				dataIndex: 'URUT',
				align :'center',
				width:90
				
			},
			{
				id: 'colTGlout',
				header: 'Tanggal Resep',
				dataIndex: 'TGL_OUT',
				align :'center',
				width:130,
				renderer: function(v, params, record)
				{
				   return ShowDate(record.data.TGL_BAYAR);
	
				} 
			},
			{
				id: 'colePembayaran',
				header: 'Pembayaran',
				dataIndex: 'URAIAN',
				align :'center',
				width:100,
				hidden:false
				
			},
			{
				id: 'colTGlout',
				header: 'Tanggal Bayar',
				dataIndex: 'TGL_BAYAR',
				align :'center',
				width:130,
				renderer: function(v, params, record)
				{
				   return ShowDate(record.data.TGL_BAYAR);
	
				} 
			},
			{
				id: 'colJumlah',
				header: 'Total Bayar',
				width:130,
				xtype:'numbercolumn',
				align :'right',
				dataIndex: 'JUMLAH',
				renderer: function(v, params, record)
				{
				   return formatCurrency(record.data.JUMLAH);
	
				}
				
			},
			{
				id: 'colJumlah',
				header: 'Jumlah Angsuran',
				width:130,
				xtype:'numbercolumn',
				align :'right',
				dataIndex: 'JML_TERIMA_UANG',
				renderer: function(v, params, record)
				{
				   return formatCurrency(record.data.JML_TERIMA_UANG);
	
				}
				
			},
			{
				id: 'colJumlah',
				header: 'Sisa',
				width:130,
				xtype:'numbercolumn',
				align :'right',
				dataIndex: 'SISA',
				renderer: function(v, params, record)
				{
				   return formatCurrency(record.data.SISA);
	
				}
				
			}
			

        ]
    )
};

var a={};
function gridDataViewEdit_viApotekResepRWI(){
    chkSelected_viApotekResepRWI = new Ext.grid.CheckColumn({
		id: Nci.getId(),
		header: 'C',
		align: 'center',						
		dataIndex: 'cito',			
		width: 20
	});
	
    var FieldGrdKasir_viApotekResepRWI = [];
    dsDataGrdJab_viApotekResepRWI= new WebApp.DataStore({
        fields: FieldGrdKasir_viApotekResepRWI
    });
    
    ResepRWI.form.Grid.a =new Ext.grid.EditorGridPanel({
        store: dsDataGrdJab_viApotekResepRWI,
        height: 210,
        columnLines: true,
		selModel: new Ext.grid.CellSelectionModel ({
            singleSelect: true,
            listeners:{
                cellselect: function(sm, row, rec){
					CellSelected_viApotekResepRWI = dsDataGrdJab_viApotekResepRWI.getAt(row);
					
					currentKdPrdRacik=CellSelected_viApotekResepRWI.data.kd_prd;
					currentNamaObatRacik=CellSelected_viApotekResepRWI.data.nama_obat;
					currentHargaRacik=CellSelected_viApotekResepRWI.data.harga_jual;
					currentJumlah=CellSelected_viApotekResepRWI.data.jml;
					curentIndexsSelection= ResepRWI.form.Grid.a.getSelectionModel().selection.cell[0];
				}
            }
        }),
        stripeRows: true,
        cm:new Ext.grid.ColumnModel([
        	new Ext.grid.RowNumberer(),	
			chkSelected_viApotekResepRWI,
			{
				dataIndex: 'kd_prd',
				header: 'Kode',
				sortable: true,
				width: 70
			},
			{			
				dataIndex: 'nama_obat',
				header: 'Uraian',
				sortable: true,
				width: 250,
				editor:new Nci.form.Combobox.autoComplete({
					store	: ResepRWI.form.ArrayStore.a,
					select	: function(a,b,c){
						//'harga_beli','kd_pabrik','markup','tuslah','adm_racik'
						var line	= ResepRWI.form.Grid.a.getSelectionModel().selection.cell[0];
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.nama_obat=b.data.nama_obat;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.kd_prd=b.data.kd_prd;
						//dsDataGrdJab_viApotekResepRWI.getRange()[line].data.kd_sat_besar=b.data.kd_sat_besar;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.kd_satuan=b.data.kd_satuan;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.fractions=b.data.fractions;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.harga_jual=b.data.harga_jual;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.harga_beli=b.data.harga_beli;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.kd_pabrik=b.data.kd_pabrik;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.markup=b.data.markup;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.adm_racik=b.data.adm_racik;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.jasa=b.data.jasa;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.no_out=b.data.no_out;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.no_urut=b.data.no_urut;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.tgl_out=b.data.tgl_out;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.kd_milik=b.data.kd_milik;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.jml_stok_apt=b.data.jml_stok_apt;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.min_stok=b.data.min_stok;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.disc=0;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.racik="";
						
						ResepRWI.form.Grid.a.getView().refresh();
						ResepRWI.form.Grid.a.startEditing(line, 5);	
						
						if(b.data.jml_stok_apt <= 10){
							ShowPesanInfoResepRWI('Stok obat hampir habis, jumlah stok tersedia adalah '+b.data.jml_stok_apt,'Information')
						}
						//Ext.getCmp('txtAdmRacikEditData_viApotekResepRWI').setValue(b.data.adm_racik);
					},
					insert	: function(o){
						return {
							kd_prd        	: o.kd_prd,
							nama_obat 		: o.nama_obat,
							kd_sat_besar	: o.kd_sat_besar,
							kd_satuan		: o.kd_satuan,
							fractions		: o.fractions,
							harga_jual		: o.harga_jual,
							harga_beli		: o.harga_beli,
							kd_pabrik		: o.kd_pabrik,
							tuslah			: o.tuslah,
							adm_racik		: o.adm_racik,
							markup			: o.markup,
							jasa			: o.jasa,
							no_out			: o.no_out,
							no_urut			: o.no_urut,
							tgl_out			: o.tgl_out,
							kd_milik		: o.kd_milik,
							jml_stok_apt	: o.jml_stok_apt,
							min_stok		: o.min_stok,
							//text			:  '<table style="font-size: 11px;"><tr><td width="50">'+o.kd_prd+'</td><td width="200">'+o.nama_obat+'</td><td width="150" align="right">'+parseInt(o.harga_jual).toLocaleString('id', { style: 'currency', currency: 'IDR' })+'</td><td width="50">&nbsp;&nbsp;'+o.min_stok+'</td></tr></table>'
							text			:  '<table style="font-size: 11px;"><tr><td width="50">'+o.kd_prd+'</td><td width="200">'+o.nama_obat+'</td><td width="150" align="right">'+parseInt(o.harga_jual).toLocaleString('id', { style: 'currency', currency: 'IDR' })+'</td></tr></table>'
						}
					},
					param:function(){
							return {
								kdcustomer:Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').getValue()
							}
					},
					url		: baseURL + "index.php/apotek/functionAPOTEKrwi/getObat",
					valueField: 'nama_obat',
					displayField: 'text',
					listWidth: 380
				})
			},
			{
				dataIndex: 'kd_satuan',
				header: 'Satuan',
				width: 60
			},{
				dataIndex: 'racik',
				header: 'Racikan',
				width: 70,
				align:'center',
				editor: new Ext.form.ComboBox ( {
					id				: 'gridcboRacik_ResepRWI',
					typeAhead		: true,
					triggerAction	: 'all',
					lazyRender		: true,
					mode			: 'local',
					selectOnFocus	: true,
					forceSelection	: true,
					width			: 50,
					anchor			: '95%',
					value			: 1,
					store			: new Ext.data.ArrayStore({
						id		: 0,
						fields	:['Id','displayText'],
						data	: [[0, 'Tidak'],[1, 'Ya']]
					}),
					valueField	: 'displayText',
					displayField: 'displayText',
					value		: '',
					listeners	: {
						select	: function(a,b,c){
							if(b.data.Id == 1) {
								Ext.getCmp('btnRacikan_viApotekResepRWI').enable();
								formulaRacikanResepRWI();
							} else{
								Ext.getCmp('btnRacikan_viApotekResepRWI').disable();
								var line	= ResepRWI.form.Grid.a.getSelectionModel().selection.cell[0];
								ResepRWI.form.Grid.a.startEditing(line, 8);	
								
							}
						}
					}
				})
				/* editor: new Ext.form.NumberField({
					allowBlank: false,
					listeners:{
						blur: function(a){
							var line	= this.index;
							dsDataGrdJab_viApotekResepRWI.getRange()[line].data.racik=a.getValue();
							hasilJumlahResepRWI();
						},
						focus: function(a){
							this.index=ResepRWI.form.Grid.a.getSelectionModel().selection.cell[0]
						}
					}
				})	 */
			},{
				dataIndex: 'harga_jual',
				header: 'Harga Sat',
				sortable: true,
				xtype:'numbercolumn',
				align:'right',
				width: 85
			},{
				dataIndex: 'min_stok',
				header: 'min stok',
				sortable: true,
				xtype:'numbercolumn',
				hidden: true,
				align:'right',
				width: 85
			},{
				dataIndex: 'jml',
				header: 'Qty',
				sortable: true,
				xtype:'numbercolumn',
				width: 45,
				align:'right',
				editor: new Ext.form.NumberField({
					allowBlank: false,
					listeners:{
						blur: function(a){
							var line	= this.index;
							if(a.getValue()==''){
								ShowPesanWarningResepRWI('Qty obat belum di isi', 'Warning');
							}else{
								var o=dsDataGrdJab_viApotekResepRWI.getRange()[line].data;
								if ( parseFloat(a.getValue())   >= parseFloat(o.min_stok))
								{
									
									Ext.Msg.alert('Perhatian','Qty sudah mencapai atau melebihi minimum stok.');
									a.setValue(o.min_stok);
									//o.jml=o.jml_stok_apt;
									//alert(o.jml);
									
								}
								else (parseFloat(a.getValue()) < parseFloat(o.min_stok))
								{
									o.jml=a.getValue();
								}
								
								hasilJumlahResepRWI();
								ResepRWI.form.Grid.a.startEditing(line, 11);
							}
							
						},
						focus: function(a){
							this.index=ResepRWI.form.Grid.a.getSelectionModel().selection.cell[0]
						},
					}
				})	
			},
			{
				dataIndex: 'disc',
				header: 'Diskon',
				sortable: true,
				xtype:'numbercolumn',
				width: 65,
				align:'right',
				editor: new Ext.form.NumberField({
					allowBlank: false,
					listeners:{
						blur: function(a){
							var line	= this.index;
							dsDataGrdJab_viApotekResepRWI.getRange()[line].data.disc=a.getValue();
							hasilJumlahResepRWI();
						},
						focus: function(a){
							this.index=ResepRWI.form.Grid.a.getSelectionModel().selection.cell[0]
						}
						
					}
				})
			},	
			{
				dataIndex: 'jumlah',
				header: 'Sub Total',
				sortable: true,
				width: 100,
				xtype:'numbercolumn',
				align:'right'
				/*renderer: function(v, params, record) {
				}	*/
			},
			{
				dataIndex: 'dosis',
				header: 'Dosis',
				width: 150,
				editor: new Ext.form.TextField({
					allowBlank: false,
					listeners:{
						specialkey : function()
						{
							if (Ext.EventObject.getKey() === 13) 
							{
								var records = new Array();
								records.push(new dsDataGrdJab_viApotekResepRWI.recordType());
								dsDataGrdJab_viApotekResepRWI.add(records);
								var nextRow = dsDataGrdJab_viApotekResepRWI.getCount()-1; 
								ResepRWI.form.Grid.a.startEditing(nextRow, 3);
							} 						
						}
					}
				})
			},
			//-------------- HIDDEN --------------
			{
				dataIndex: 'harga_beli',
				header: 'Harga Beli',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'kd_pabrik',
				header: 'Kode Pabrik',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'markup',
				header: 'Markup',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'adm_racik',
				header: 'Adm Racik',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'jasa',
				header: 'Jasa Tuslah',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'no_out',
				header: 'No Out',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'no_urut',
				header: 'No Urut',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'tgl_out',
				header: 'tgl out',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'kd_milik',
				header: 'kd milik',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'jml_stok_apt',
				header: 'stok tersedia',
				hidden: true,
				width: 80
			}
			//-------------- ## --------------
        ]),
        plugins:chkSelected_viApotekResepRWI,
		viewConfig:{
			forceFit: true
		}
    });
    return ResepRWI.form.Grid.a;
}

function hasilJumlahResepRWI(){
	var total=0;
	var admRacik=0;
	var totdisc=0;
	var totalall=0;
	var admprs=0;
	var adm=0;
	var totqty=0;
	
	for(var i=0; i < dsDataGrdJab_viApotekResepRWI.getCount() ; i++){
		var jumlahGrid=0;
		var subJumlah=0;
		var disc=0;
	
		var o=dsDataGrdJab_viApotekResepRWI.getRange()[i].data;
		if(o.jml != undefined){
			if(parseFloat(o.jml) <= parseFloat(o.jml_stok_apt)){
				jumlahGrid=((parseFloat(o.jml) * parseFloat(o.harga_jual)) - parseFloat(o.disc));
			
				if(o.racik != undefined || o.racik != ""){
					if(isNaN(admRacik)){
						admRacik +=0;
					} else {
						if(o.racik == 'Ya' ){
							if(o.adm_racik != undefined || o.adm_racik != null || o.adm_racik != 0){
								admRacik += parseFloat(o.adm_racik) * 1;
							} else{
								admRacik +=0;
							}
							
						} else{
							admRacik +=0;
						}
						
					}
				}
				totdisc += parseFloat(o.disc);
				o.jumlah =jumlahGrid;
				total +=jumlahGrid;
			} else{
				ShowPesanWarningResepRWI('Jumlah obat melebihi stok yang tersedia','Warning');
				o.jml=o.jml_stok_apt;
			}
			totqty +=o.jml;
		}

	}
	
	Ext.getCmp('txtTmpTotQtyResepRWIL').setValue(totqty);
	Ext.getCmp('txtJumlahTotalResepRWIL').setValue(aptpembulatan(total));
	Ext.getCmp('txtAdmRacikResepRWIL').setValue(toFormat(admRacik));
	Ext.getCmp('txtTotalDiscResepRWIL').setValue(toFormat(totdisc));
	admprs=total*parseFloat(Ext.getCmp('txtAdmResepRWIL').getValue());
	Ext.getCmp('txtAdmPrshResepRWIL').setValue(toFormat(admprs));
	totalall += parseFloat(total) + parseFloat(admRacik) + parseFloat(Ext.get('txtTuslahEmbalaseL').getValue()) + parseFloat(Ext.get('txtAdmResepRWIL').getValue()) + parseFloat(admprs) - parseInt(totdisc);
	Ext.getCmp('txtTotalBayarResepRWIL').setValue(aptpembulatan(totalall));

	//Ext.get('txtTotalEditData_viApotekResepRWJ').dom.value=totalall.toLocaleString('id', { style: 'currency', currency: 'IDR' });
	ResepRWI.form.Grid.a.getView().refresh();
}

function hasilJumlahResepRWILoad(){
	var total=0;
	var admRacik=0;
	var totdisc=0;
	var totalall=0;
	var admprs=0;
	var adm=0;
	var totqty=0;
	
	for(var i=0; i < dsDataGrdJab_viApotekResepRWI.getCount() ; i++){
		var jumlahGrid=0;
		var subJumlah=0;
		var disc=0;
	
		var o=dsDataGrdJab_viApotekResepRWI.getRange()[i].data;
		if(o.jml != undefined){
			if(parseFloat(o.jml) <= parseFloat(o.jml_stok_apt)){
				jumlahGrid=((parseFloat(o.jml) * parseFloat(o.harga_jual)) - parseFloat(o.disc));
			
				totdisc += parseFloat(o.disc);
				o.jumlah =jumlahGrid;
				total +=jumlahGrid;
			} else{
				ShowPesanWarningResepRWI('Jumlah obat melebihi stok yang tersedia','Warning');
				o.jml=o.jml_stok_apt;
			}
			totqty +=o.jml;
		}

	}
	admRacik = parseFloat(Ext.getCmp('txtAdmRacikResepRWIL').getValue())
	Ext.getCmp('txtTmpTotQtyResepRWIL').setValue(totqty);
	total=aptpembulatan(total);
	Ext.getCmp('txtJumlahTotalResepRWIL').setValue(total);
	Ext.getCmp('txtAdmRacikResepRWIL').setValue(toFormat(admRacik));
	Ext.getCmp('txtTotalDiscResepRWIL').setValue(toFormat(totdisc));
	admprs=toInteger(total)*parseFloat(Ext.getCmp('txtAdmResepRWIL').getValue());
	Ext.getCmp('txtAdmPrshResepRWIL').setValue(toFormat(admprs));
	totalall += toInteger(total) + parseFloat(admRacik) + parseFloat(Ext.getCmp('txtTuslahEmbalaseL').getValue()) + parseFloat(Ext.getCmp('txtAdmResepRWIL').getValue()) + parseFloat(admprs) - parseInt(totdisc);
	Ext.getCmp('txtTotalBayarResepRWIL').setValue(aptpembulatan(totalall));
	console.log(totalall);
	//Ext.get('txtTotalEditData_viApotekResepRWJ').dom.value=totalall.toLocaleString('id', { style: 'currency', currency: 'IDR' });
	ResepRWI.form.Grid.a.getView().refresh();
}


function ComboPilihanKelompokPasienApotekResepRWI()
{
	var Field_Customer = ['KD_CUSTOMER', 'JENIS_PASIEN'];
	ds_kelpas = new WebApp.DataStore({fields: Field_Customer});
    ds_kelpas.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: '',
					Sortdir: 'ASC',
					target: 'ComboKelPasApotek',
					param: ''
				}
		}
	);
    var cboPilihankelompokPasienAptResepRWI = new Ext.form.ComboBox
	(
		{
			x: 110,
			y: 100,
			id:'cboPilihankelompokPasienAptResepRWI',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			readOnly:true,
			emptyText:'Kelompok Pasien',
			width: 150,
			store: ds_kelpas,
			valueField: 'KD_CUSTOMER',
			displayField: 'JENIS_PASIEN',
			//value:selectSetPilihankelompokPasien,
			listeners:
			{
				'select': function(a,b,c)
				{
					//selectSetPilihankelompokPasien=b.data.displayText;
				}
			}
		}
	);
	return cboPilihankelompokPasienAptResepRWI;
};

function ComboDokterApotekResepRWI()
{
    var Field_Dokter = ['KD_DOKTER', 'NAMA'];
    ds_dokter = new WebApp.DataStore({fields: Field_Dokter});
    ds_dokter.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'kd_dokter',
					Sortdir: 'ASC',
					target: 'ComboDokterApotek',
					param: ''
				}
		}
	);
	
    var cbo_DokterApotekResepRWI = new Ext.form.ComboBox
    (
        {
			x:130,
			y:60,
            flex: 1,
			id: 'cbo_DokterApotekResepRWI',
			valueField: 'KD_DOKTER',
            displayField: 'NAMA',
			emptyText:'Dokter',
			store: ds_dokter,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			//readOnly:true,
			width:150,
			tabIndex:2,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					selectSetDokter=b.data.displayText ;
					Ext.getCmp('txtTmpKdDokterApotekResepRWIL').setValue(b.data.KD_DOKTER);
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_DokterApotekResepRWI;
};

function mComboStatusPostingApotekResepRWI(){
  var cboStatusPostingApotekResepRWI = new Ext.form.ComboBox({
        id:'cboStatusPostingApotekResepRWI',
        x: 410,
        y: 30,
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:true,
		editable: false,
        mode: 'local',
        width: 110,
        emptyText:'',
        fieldLabel: 'JENIS',
		tabIndex:5,
        store: new Ext.data.ArrayStore( {
            id: 0,
            fields:[
                'Id',
                'displayText'
            ],
            data: [[1, 'Semua'],[2, 'Posting'], [3, 'Belum Posting']]
        }),
        valueField: 'Id',
        displayField: 'displayText',
        value:selectCountStatusPostingApotekResepRWI,
        listeners:{
			'select': function(a,b,c){
				selectCountStatusPostingApotekResepRWI=b.data.displayText ;
				tmpkriteria = getCriteriaCariApotekResepRWI();
				refreshRespApotekRWI(tmpkriteria);
			}
        }
	});
	return cboStatusPostingApotekResepRWI;
};

function ComboUnitApotekResepRWI(){
    var Field_Vendor = ['KD_UNIT', 'NAMA_UNIT'];
    ds_unit = new WebApp.DataStore({fields: Field_Vendor});
    ds_unit.load({
        params:{
	        Skip: 0,
	        Take: 1000,
	        Sort: 'kd_unit',
	        Sortdir: 'ASC',
	        target: 'ComboUnitApotek',
	        param: "parent='100'"
        }
    });
    var cbo_UnitRWI = new Ext.form.ComboBox({
			x:130,
			y:60,
            flex: 1,
			id: 'cbo_UnitResepRWI',
            fieldLabel: 'Poliklinik',
			valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
			emptyText:'Unit/Ruang',
			store: ds_unit,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:150,
			tabIndex:2,
			listeners:{ 
				'select': function(a,b,c){
					selectSetUnit=b.data.displayText ;
					tmpkriteria = getCriteriaCariApotekResepRWI();
					refreshRespApotekRWI(tmpkriteria);
				},
				'specialkey' : function(){
					if (Ext.EventObject.getKey() === 13) {
						tmpkriteria = getCriteriaCariApotekResepRWI();
						refreshRespApotekRWI(tmpkriteria);
					} 						
				}
			}
        }
    )    
    return cbo_UnitRWI;
}

function viCombo_VendorLookup(){
    var cbo_UnitRWILookup = new Ext.form.ComboBox({
        flex: 1,
		id: 'cbo_UnitRWILookup',
        fieldLabel: 'Vendor',
		valueField: 'KD_VENDOR',
        displayField: 'VENDOR',
		emptyText:'PBF',
		store: ds_unit,
        mode: 'local',
        typeAhead: true,
        triggerAction: 'all',
        lazyRender: true,
		width:190,
		tabIndex:2,
		listeners:{ 
			'select': function(a,b,c){
				selectSetUnit=b.data.displayText ;
			},
			'specialkey' : function(){
				if (Ext.EventObject.getKey() === 13) {
					
				} 						
			}
		}
    })  ;  
    return cbo_UnitRWILookup;
}

function ComboUnitResepRWILookup()
{
    var Field_Vendor = ['KD_UNIT', 'NAMA_UNIT'];
    ds_unitL = new WebApp.DataStore({fields: Field_Vendor});
    ds_unitL.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'kd_unit',
					Sortdir: 'ASC',
					target: 'ComboUnitApotek',
					param: "parent='100'"
				}
		}
	);
	
    var cbo_Unit = new Ext.form.ComboBox
    (
        {
			id: 'cbo_UnitResepRWIL',
            fieldLabel: 'Unit',
			valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
			emptyText:'Unit',
			store: ds_unitL,
			valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
            mode: 'local',
            typeAhead: true,
			readOnly: true,
            triggerAction: 'all',
            lazyRender: true,
			width:150,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					//selectSetUnit=b.data.valueField;
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						/* tmpkriteria = getCriteriaCariApotekResepRWJ();
						refeshRespApotekRWJ(tmpkriteria); */
					} 						
				}
			}
        }
    )    
    return cbo_Unit;
}

function mComboJenisByrResepRWI() 
{
	var Field = ['JENIS_PAY','DESKRIPSI','TYPE_DATA'];

    dsJenisbyrView = new WebApp.DataStore({ fields: Field });
    dsJenisbyrView.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000, Sort: 'jenis_pay',
			    Sortdir: 'ASC',
			    target: 'ViewJenisPay',
				param: "TYPE_DATA IN (0,1,3) AND DB_CR='0' ORDER BY Type_data"
			  
			}
		}
	);
	
    var cboJenisByr = new Ext.form.ComboBox
	(
		{
	
		    id: 'cboJenisByrResepRWI',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
			fieldLabel: 'Pembayaran',
		    align: 'Right',
		    width:150,
		    store: dsJenisbyrView,
		    valueField: 'JENIS_PAY',
		    displayField: 'DESKRIPSI',
			value:'TUNAI',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
					loaddatastorePembayaran(b.data.JENIS_PAY);
				}
				  

			}
		}
	);
	
    return cboJenisByr;
};
function loaddatastorePembayaran(jenis_pay)
{
    dsComboBayar.load
	(
		{
		 params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'nama',
					Sortdir: 'ASC',
					target: 'ViewComboBayar',
					param: 'jenis_pay=~'+ jenis_pay+ '~'
				}
	   }
	)      
}

function mComboPembayaranRWI()
{
    var Field = ['KD_PAY','JENIS_PAY','PAYMENT'];

    dsComboBayar = new WebApp.DataStore({fields: Field});
	
    var cboPembayaran = new Ext.form.ComboBox
	(
		{
		    id: 'cboPembayaranRWI',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
            forceSelection: true,
		    emptyText:'Pilih Pembayaran...',
		    labelWidth:80,
		    align: 'Right',
		    store: dsComboBayar,
		    valueField: 'KD_PAY',
		    displayField: 'PAYMENT',
			value: 'TUNAI',
			width:225,
			listeners:
			{
			    'select': function(a, b, c) 
				{
					tapungkd_pay=b.data.KD_PAY;
				}
				  

			}
		}
	);

    return cboPembayaran;
};

function ViewDetailPembayaranObatRWI(no_out,tgl_out) 
{	
    var strKriteriaRWI='';
    strKriteriaRWI = "a.no_out = " + no_out + "" + " And a.tgl_out ='" + tgl_out + "' order by a.urut";
   
    dsTRDetailHistoryBayar.load({
	    params:{
		    Skip: 0,
		    Take: 1000,
		    Sort: 'tgl_transaksi',
		    Sortdir: 'ASC',
		    target: 'ViewHistoryPembayaran',
		    param: strKriteriaRWI
		}
	}); 
	return dsTRDetailHistoryBayar;
	
    
};

function mComboorder_rwi()
{ 
 
 var Field = ['kd_pasien','nama','kd_dokter','kd_unit','kd_customer','no_transaksi','kd_kasir','id_mrresep','urut_msauk','tgl_masuk'];

    dspasienorder_mng_apotek_rwi = new WebApp.DataStore({ fields: Field });
	
	load_data_pasienorder();
	cbopasienorder_mng_apotek_rwi= new Ext.form.ComboBox
	(
		{
			id: 'cbopasienorder_mng_apotek_rwi',
			typeAhead		: true,
			triggerAction	: 'all',
			lazyRender		: true,
			disabled		: true,
			mode			: 'local',
			emptyText: '',
			fieldLabel:  ' ',
			align: 'Right',
			width: 190,
			store: dspasienorder_mng_apotek_rwi,
			valueField: 'display',
			displayField: 'display',
			//hideTrigger		: true,
			listeners:
			{
				select	: function(a,b,c){
							ordermanajemenrwi=true;
				            kd_pasien_obbt_rwi =b.data.kd_pasien;
							kd_unit_obbt_rwi=b.data.kd_unit;
							tgl_masuk_obbt_rwi =b.data.tgl_masuk;
							urut_masuk_obbt_rwi=b.data.urut_masuk; 
							Ext.getCmp('cbo_DokterApotekResepRWI').setValue(b.data.kd_dokter);
							Ext.getCmp('cboKodePasienResepRWI').setValue(b.data.kd_pasien);
							Ext.getCmp('cbo_UnitResepRWIL').setValue(b.data.kd_unit);
							Ext.getCmp('cboPilihankelompokPasienAptResepRWI').setValue(b.data.kd_customer);
							Ext.getCmp('txtNoKamarApotekResepRWIL').setValue(b.data.no_kamar);
							Ext.getCmp('txtKamarApotekResepRWIL').setValue(b.data.nama_kamar);
							ResepRWI.form.ComboBox.namaPasien.setValue(b.data.nama);
							ResepRWI.vars.no_transaksi=b.data.no_transaksi;
							ResepRWI.vars.tgl_transaksi=b.data.tgl_transaksi;
							ResepRWI.vars.kd_kasir=b.data.kd_kasir;
							Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').setValue(b.data.kd_customer);
							Ext.getCmp('txtTmpKdDokterApotekResepRWIL').setValue(b.data.kd_dokter);
							Ext.getCmp('txtTmpKdUnitApotekResepRWIL').setValue(b.data.kd_unit);
							Ext.getCmp('btnAddObatRWI').enable();
							Ext.getCmp('btnSimpan_viApotekResepRWI').enable();
							Ext.getCmp('btnSimpanExit_viApotekResepRWI').enable();
							Ext.getCmp('btnDelete_viApotekResepRWI').enable();
							Ext.getCmp('statusservice_apt_RWI').enable();
							dataGridObatApotekResep_penatarwi(b.data.id_mrresep,b.data.kd_customer);
							CurrentIdMrResepRwi=b.data.id_mrresep;
							
							cekDilayani(b.data.id_mrresep,b.data.order_mng);
					},
				   keyUp: function(a,b,c){
				    	$this1=this;
				    	if((b.getKey()>=65 && b.getKey()<=90)|| (b.getKey()>=48 && b.getKey()<=57) || b.getKey()==32 || b.getKey()==8 || b.getKey()==46){
				    		clearTimeout(this.time);
				    	
				    		this.time=setTimeout(function(){
			    				if($this1.lastQuery != '' ){
				    				var value=$this1.lastQuery;
				    				var param={};
		        		    		param['text']=$this1.lastQuery;
		        		    		load_data_pasienorder($this1.lastQuery);

		        		    		
			    				}
		    		    	},1000);
				    	}
				    }
			}
		}
	);return cbopasienorder_mng_apotek_rwi;
};


function datainit_viApotekResepRWI(rowdata)
{
    //AddNewPenJasRad = false;
	//console.log(rowdata);
	dataisi=1;
	Ext.getCmp('txtNoResepApotekResepRWIL').setValue(rowdata.NO_RESEP);
	Ext.getCmp('cbo_UnitResepRWIL').setValue(rowdata.NAMA_UNIT);
	Ext.getCmp('cbo_DokterApotekResepRWI').setValue(rowdata.NAMA_DOKTER);
	Ext.getCmp('cboPilihankelompokPasienAptResepRWI').setValue(rowdata.CUSTOMER);
	Ext.getCmp('cboKodePasienResepRWI').setValue(rowdata.KD_PASIENAPT);
	ResepRWI.form.ComboBox.namaPasien.setValue(rowdata.NMPASIEN);
	Ext.getCmp('txtNoKamarApotekResepRWIL').setValue(rowdata.NO_KAMAR);
	Ext.getCmp('txtKamarApotekResepRWIL').setValue(rowdata.NAMA_KAMAR);
	Ext.getCmp('dfTglResepSebenarnyaResepRWI').setValue(ShowDate(rowdata.TGL_RESEP));
	
	Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').setValue(rowdata.KD_CUSTOMER);
	Ext.getCmp('txtTmpKdDokterApotekResepRWIL').setValue(rowdata.DOKTER);
	Ext.getCmp('txtTmpKdUnitApotekResepRWIL').setValue(rowdata.KD_UNIT);
	Ext.getCmp('txtTmpNooutResepRWIL').setValue(rowdata.NO_OUT);
	Ext.getCmp('txtTmpTgloutResepRWIL').setValue(rowdata.TGL_OUT);
	Ext.getCmp('txtTmpSisaAngsuranResepRWIL').setValue(rowdata.SISA);
	Ext.getCmp('txtTmpStatusPostResepRWIL').setValue(rowdata.STATUS_POSTING);
	
	ResepRWI.vars.tgl_transaksi=rowdata.TGL_TRANSAKSI;
	ResepRWI.vars.no_transaksi=rowdata.APT_NO_TRANSAKSI;
	ResepRWI.vars.kd_kasir=rowdata.APT_KD_KASIR;
	ResepRWI.vars.urut_masuk=rowdata.URUT_MASUK;
	
	kd_pasien_obbt_rwi=rowdata.KD_PASIENAPT;
	kd_unit_obbt_rwi=rowdata.KD_UNIT;
	tgl_masuk_obbt_rwi=rowdata.TGL_MASUK;
	urut_masuk_obbt_rwi=rowdata.URUT_MASUK; 
	
	/* kd_spesial_tr=rowdata.KD_SPESIAL;
	kd_unit_kamar_tr=rowdata.KD_UNIT_KAMAR;
	no_kamar_tr=rowdata.NO_KAMAR; */
		
	
	
	if(rowdata.ADMPRHS ==null){
		Ext.getCmp('txtAdmPrshResepRWIL').setValue(0);
	}else{
		Ext.getCmp('txtAdmPrshResepRWIL').setValue(rowdata.ADMPRHS);
	}
	if(rowdata.ADMRESEP == null){
		Ext.getCmp('txtAdmResepRWIL').setValue(0);
	}else{
		Ext.getCmp('txtAdmResepRWIL').setValue(rowdata.ADMRESEP);
	}
	if(rowdata.JASA == null){
		Ext.getCmp('txtTuslahEmbalaseL').setValue(0);
	}else{
		Ext.getCmp('txtTuslahEmbalaseL').setValue(rowdata.JASA);
	}
	
	if(rowdata.STATUS_POSTING === '1'){
		Ext.getCmp('btnunposting_viApotekResepRWI').enable();
		Ext.getCmp('btnPrint_viResepRWI').enable();
		Ext.getCmp('btnBayar_viApotekResepRWI').disable();
		Ext.getCmp('btnAdd_viApotekResepRWI').enable();
		Ext.getCmp('btnSimpan_viApotekResepRWI').disable();
		Ext.getCmp('btnSimpanExit_viApotekResepRWI').disable();
		Ext.getCmp('btnDelete_viApotekResepRWI').disable();
		Ext.getCmp('btnAddObatRWI').disable();
		ResepRWI.form.Panel.a.update('<img src="'+baseURL+'ui/images/icons/16x16/hijau.png"/>');
			
	} else{
		Ext.getCmp('btnSimpan_viApotekResepRWI').disable();
		Ext.getCmp('btnSimpanExit_viApotekResepRWI').disable();
		Ext.getCmp('btnunposting_viApotekResepRWI').disable();
		Ext.getCmp('btnPrint_viResepRWI').disable();
		Ext.getCmp('btnBayar_viApotekResepRWI').enable();
		//Ext.getCmp('btnTransfer_viApotekResepRWI').enable();
		Ext.getCmp('btnDelete_viApotekResepRWI').enable();
		Ext.getCmp('btnDeleteHistory_viApotekResepRWI').enable();
		Ext.getCmp('btnAddObatRWI').enable();
		ResepRWI.form.Panel.a.update('<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>');
	}
	//dataGridObatApotekResepRWJ(rowdata.NO_OUT,rowdata.TGL_OUT,rowdata.ADMRACIK);
	getSeCoba(rowdata.NO_OUT,rowdata.TGL_OUT,rowdata.ADMRACIK);
	Ext.Ajax.request(
	{
	   
		url: baseURL + "index.php/apotek/currentShiftAPT/getcurrentshift",
		 params: {
			
			command: '0'
		
		},
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);
			ResepRWI.form.Panel.shift.update(cst.shift);
		}
	
	});
	
	//Ext.getCmp('tshift').setValue(tampungshiftsekarang)
};

function viewDetailGridOrderRWI(CurrentGridOrderRWI){
	
	
	kd_spesial_tr=CurrentGridOrderRWI.kd_spesial;
	kd_unit_kamar_tr=CurrentGridOrderRWI.kd_unit_kamar;
	no_kamar_tr=CurrentGridOrderRWI.no_kamar;
	
	kd_pasien_obbt_rwi =CurrentGridOrderRWI.kd_pasien;
	kd_unit_obbt_rwi=CurrentGridOrderRWI.kd_unit;
	tgl_masuk_obbt_rwi = CurrentGridOrderRWI.tgl_masuk;
	urut_masuk_obbt_rwi=CurrentGridOrderRWI.urut_masuk; 
	Ext.getCmp('cbo_DokterApotekResepRWI').setValue(CurrentGridOrderRWI.kd_dokter);
	Ext.getCmp('cboKodePasienResepRWI').setValue(CurrentGridOrderRWI.kd_pasien);
	Ext.getCmp('cbo_UnitResepRWIL').setValue(CurrentGridOrderRWI.kd_unit);
	Ext.getCmp('cboPilihankelompokPasienAptResepRWI').setValue(CurrentGridOrderRWI.customer);
	Ext.getCmp('txtNoKamarApotekResepRWIL').setValue(CurrentGridOrderRWI.no_kamar);
	Ext.getCmp('txtKamarApotekResepRWIL').setValue(CurrentGridOrderRWI.nama_kamar);
	ResepRWI.form.ComboBox.namaPasien.setValue(CurrentGridOrderRWI.nama);
	ResepRWI.vars.no_transaksi=CurrentGridOrderRWI.no_transaksi;
	ResepRWI.vars.tgl_transaksi=CurrentGridOrderRWI.tgl_transaksi;
	ResepRWI.vars.kd_kasir=CurrentGridOrderRWI.kd_kasir;
	ResepRWI.vars.urut_masuk=CurrentGridOrderRWI.urut_masuk;
	Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').setValue(CurrentGridOrderRWI.kd_customer);
	Ext.getCmp('txtTmpKdDokterApotekResepRWIL').setValue(CurrentGridOrderRWI.kd_dokter);
	Ext.getCmp('txtTmpKdUnitApotekResepRWIL').setValue(CurrentGridOrderRWI.kd_unit);
	
	dataGridObatApotekResep_penatarwi(CurrentGridOrderRWI.id_mrresep,CurrentGridOrderRWI.kd_customer,CurrentGridOrderRWI.order_mng);
	
	
	
	
	/* hasilJumlahResepRWILoad();
	getAdm(Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').getValue()); */
}

function getAdm(kd_customer){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEKrwi/getAdm",
			params: {kd_customer:kd_customer},
			failure: function(o)
			{
				ShowPesanErrorResepRWI('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					Ext.getCmp('txtAdmResepRWIL').setValue(toFormat(cst.adm));
					Ext.getCmp('txtTuslahEmbalaseL').setValue(toFormat(cst.tuslah));
					hasilJumlahResepRWILoad();
				}
				else 
				{
					Ext.getCmp('txtAdmResepRWIL').setValue(toFormat(cst.adm));
					Ext.getCmp('txtTuslahEmbalaseL').setValue(toFormat(cst.tuslah));
					hasilJumlahResepRWILoad();
					//ShowPesanErrorResepRWI('Gagal mengambil tuslah dan adm', 'Error');
				};
			}
		}
		
	)
	
}
function getSeCoba(no_out,tgl_out,admracik){//get kd_unit_far from session
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEKrwi/sess",
			params: {query:"no_out = " + no_out},
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					kd=cst.session;
					dataGridObatApotekResepRWI(no_out,tgl_out,admracik,kd);
				}
			}
		}
	);
}


function cekTransferRWI(no_out,tgl_out){//get kd_unit_far from session
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEKrwi/cekTransfer",
			params: {
				no_out: no_out,
				tgl_out: tgl_out
			},
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true && cst.ada === 0) 
				{
					unpostingRESEPRWI();
				} else if (cst.success === true && cst.ada === 1) 
				{
					unpostingRESEPRWI();
				}
				else{
					ShowPesanWarningResepRWI('Pembayaran dilakukan dengan transfer. Unposting tidak dapat dilakukan!','Warning');
				}
			}
		}
	);
}

function pembayaranResepRWI(){
	if(ValidasiBayarResepRWI(nmHeaderSimpanData,false) == 1 ){
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionAPOTEKrwi/bayarSaveResepRWI",
				params: getParamBayarResepRWI(),
				failure: function(o)
				{
					ShowPesanErrorResepRWI('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						
						if(toInteger(Ext.getCmp('txtBayarResepRWI_Pembayaran').getValue()) >= toInteger(Ext.getCmp('txtTotalResepRWI_Pembayaran').getValue())){
							ShowPesanInfoResepRWI('Berhasil melakukan pembayaran dan pembayaran telah lunas','Information');
							Ext.getCmp('btnBayar_ResepRWILookupBayar').disable();
							Ext.getCmp('txtNoResepApotekResepRWIL').setValue(cst.noresep);
							Ext.getCmp('txtTmpNooutResepRWIL').setValue(cst.noout);
							Ext.getCmp('txtTmpTgloutResepRWIL').setValue(cst.tgl);
							Ext.getCmp('btnunposting_viApotekResepRWI').disable();
							Ext.getCmp('btnPrint_viResepRWI').enable();
							Ext.getCmp('btnBayar_viApotekResepRWI').disable();
							Ext.getCmp('btnAdd_viApotekResepRWI').enable();
							Ext.getCmp('btnSimpan_viApotekResepRWI').disable();
							Ext.getCmp('btnSimpanExit_viApotekResepRWI').disable();
							Ext.getCmp('btnDelete_viApotekResepRWI').disable();
							//Ext.getCmp('btnTransfer_viApotekResepRWI').enable();
							//Ext.getCmp('txtBayarResepRWI_Pembayaran').disable();
							Ext.getCmp('btnDeleteHistory_viApotekResepRWI').disable();
							ResepRWI.form.Panel.a.update('<img src="'+baseURL+'ui/images/icons/16x16/hijau.png"/>');
							ViewDetailPembayaranObatRWI(cst.noout,cst.tgl);
							gridDTLTRHistoryApotekRWI.getView().refresh();
							refreshRespApotekRWI();
							
							if(ordermanajemenrwi==true){
								updatestatus_permintaan();
								load_data_pasienorder();
							}
							
							setLookUpApotek_bayarResepRWI.close();
							total_pasien_order_mng_obtrwi();
							total_pasien_dilayani_order_mng_obtrwi();
							viewGridOrderAll_RASEPRWI();
							
							//insert_mrobatRWI();
							
						} else {
							refreshRespApotekRWI();
							ShowPesanInfoResepRWI('Berhasil melakukan pembayaran','Information');
							setLookUpApotek_bayarResepRWI.close();
							Ext.getCmp('btnBayar_viApotekResepRWI').enable();
							Ext.getCmp('btnunposting_viApotekResepRWI').disable();
							Ext.getCmp('btnPrint_viResepRWI').disable();
							ViewDetailPembayaranObatRWI(Ext.getCmp('txtTmpNooutResepRWIL').getValue(),Ext.getCmp('txtTmpTgloutResepRWIL').getValue());
							gridDTLTRHistoryApotekRWI.getView().refresh();
							total_pasien_order_mng_obtrwi();
							total_pasien_dilayani_order_mng_obtrwi();
							viewGridOrderAll_RASEPRWI();
						}
					}
					else 
					{
						ShowPesanErrorResepRWI('Gagal melakukan pembayaran', 'Error');
						refreshRespApotekRWI();
					};
				}
			}
			
		)
	}
}


function transferResepRWI(){
	if(ValidasiTransferResepRWI(nmHeaderSimpanData,false) == 1 ){
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionAPOTEKrwi/saveTransfer",
				params: getParamTransferResepRWI(),
				failure: function(o)
				{
					ShowPesanErrorResepRWI('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						refreshRespApotekRWI();
						
							ShowPesanInfoResepRWI('Transfer berhasil dilakukan','Information');
							Ext.getCmp('btnunposting_viApotekResepRWI').disable();
							Ext.getCmp('btnTransfer_viApotekResepRWI').disable();
							Ext.getCmp('btnPrint_viResepRWI').enable();
							Ext.getCmp('btnBayar_viApotekResepRWI').disable();
							Ext.getCmp('btnDeleteHistory_viApotekResepRWI').disable();
							setLookUpApotek_TransferResepRWI.close();
							ViewDetailPembayaranObatRWI(Ext.getCmp('txtTmpNooutResepRWIL').getValue(),Ext.getCmp('txtTmpTgloutResepRWIL').getValue());
							gridDTLTRHistoryApotekRWI.getView().refresh();
							
							if(ordermanajemenrwi==true){
								updatestatus_permintaan()
								load_data_pasienorder();
							}
							
							//ResepRWI.form.Grid.a.store.removeAll()
							Ext.getCmp('statusservice_apt_RWI').disable();
							total_pasien_order_mng_obtrwi();
							total_pasien_dilayani_order_mng_obtrwi();
							viewGridOrderAll_RASEPRWI();
							
							//insert_mrobatRWI()
					}
					else 
					{
						ShowPesanErrorResepRWI('Gagal melakukan transfer', 'Error');
						refreshRespApotekRWI();
					};
				}
			}
			
		)
	}
}

function insert_mrobatRWI(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionInsertMrObat/save_mrobat",
			params: getParamInsertMrObatRWI(),
			failure: function(o)
			{
				ShowPesanErrorResepRWI('Error simpan MR! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				
			}
		}
		
	)
}

function dataGridObatApotekResepRWI(no_out,tgl_out,admracik,kd){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEK/readGridObat",
			params: {query:"o.no_out = " + no_out + "" + " And o.tgl_out ='" + tgl_out + "' and b.kd_unit_far='"+kd+"' "},
			failure: function(o)
			{
				ShowPesanErrorResepRWI('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dsDataGrdJab_viApotekResepRWI.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
					dsDataGrdJab_viApotekResepRWI.add(recs);
					
					ResepRWI.form.Grid.a.getView().refresh();
					Ext.getCmp('txtAdmRacikResepRWIL').setValue(toFormat(admracik));
					hasilJumlahResepRWILoad();
					
				}
				else 
				{
					ShowPesanErrorResepRWI('Gagal membaca Data ini', 'Error');
				};
			}
		}
		
	)
	
}

function getSisaAngsuran(stmpnoOut,stmptgl){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEKrwi/getSisaAngsuran",
			params: {no_out:stmpnoOut, tgl_out:stmptgl},
			failure: function(o)
			{
				ShowPesanErrorResepRWI('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				//console.log(cst);
				if (cst.success === true) 
				{
					console.log(cst.sisa);
					if(cst.sisa ==''){
						ShowPesanInfoResepRWI('Pembayaran sebelumnya telah lunas, jika data ini sudah pernah diposting maka hapus terlebih dahulu history pembayaran untuk mendapatkan total pembayaran','Information');
					} else{
						var sisa=aptpembulatan(cst.sisa);
						Ext.getCmp('txtTotalResepRWI_Pembayaran').setValue(toFormat(sisa));
						Ext.getCmp('txtBayarResepRWI_Pembayaran').setValue(toInteger(sisa));
						
					}
					
				}
				else 
				{
					Ext.getCmp('txtTotalResepRWJ_Pembayaran').setValue(Ext.getCmp('txtTotalBayarResepRWIL').getValue());
					Ext.getCmp('txtBayarResepRWI_Pembayaran').setValue(Ext.getCmp('txtTotalBayarResepRWIL').getValue());
					//ShowPesanErrorResepRWI('Gagal membaca Data ini', 'Error');
					alert();
				};
			}
		}
		
	)
}


function datasave_viApotekResepRWI(){
	if (ValidasiEntryResepRWI(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionAPOTEKrwi/saveResepRWI",
				params: getParamResepRWI(),
				failure: function(o)
				{
					ShowPesanErrorResepRWI('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoResepRWI(nmPesanSimpanSukses,nmHeaderSimpanData);
						Ext.get('txtNoResepApotekResepRWIL').dom.value=cst.noresep;
						Ext.get('txtTmpNooutResepRWIL').dom.value=cst.noout;
						Ext.get('txtTmpTgloutResepRWIL').dom.value=cst.tgl;
						Ext.getCmp('btnSimpan_viApotekResepRWI').disable();
						Ext.getCmp('btnSimpanExit_viApotekResepRWI').disable();
						//Ext.getCmp('btnTransfer_viApotekResepRWI').enable();
						refreshRespApotekRWI();
						Ext.getCmp('btnBayar_viApotekResepRWI').enable();
					}
					else 
					{
						ShowPesanErrorResepRWI('Gagal Menyimpan Data ini', 'Error');
						refreshRespApotekRWI();
					};
				}
			}
			
		)
	}
}

function unpostingRESEPRWI(){
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/functionAPOTEKrwi/cekBulanPOST",
		params: {tgl:Ext.getCmp('txtTmpTgloutResepRWIL').getValue()},
		success: function(o) {
			var cst = Ext.decode(o.responseText);
			if (cst.success === true){
				Ext.Msg.confirm('Warning', 'Apakah data ini akan diUnposting?', function(button){
					if (button == 'yes'){
						Ext.Ajax.request({
							url: baseURL + "index.php/apotek/functionAPOTEKrwi/unpostingResepRWI",
							params: getParamUnpostingResepRWI(),
							failure: function(o)
							{
								ShowPesanErrorResepRWI('Hubungi Admin', 'Error');
							},	
							success: function(o) 
							{
								var cst = Ext.decode(o.responseText);
								if (cst.success === true) 
								{
									ShowPesanInfoResepRWI('UnPosting Berhasil','Information');
									Ext.getCmp('txtTmpNooutResepRWIL').setValue(cst.noout);
									Ext.getCmp('txtTmpTgloutResepRWIL').setValue(cst.tgl);
									Ext.getCmp('btnunposting_viApotekResepRWI').disable();
									Ext.getCmp('btnBayar_viApotekResepRWI').disable();
									Ext.getCmp('btnSimpan_viApotekResepRWI').enable();
									Ext.getCmp('btnSimpanExit_viApotekResepRWI').enable();
									Ext.getCmp('btnDelete_viApotekResepRWI').enable();
									Ext.getCmp('btnPrint_viResepRWI').disable();
									Ext.getCmp('btnAddObatRWI').enable();
									Ext.getCmp('btnDeleteHistory_viApotekResepRWI').enable();
									Ext.getCmp('txtTmpStatusPostResepRWIL').setValue('0');
									ResepRWI.form.Panel.a.update('<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>');
									refreshRespApotekRWI();
									
									unposting_mrobatRWI();
								}
								else 
								{
									ShowPesanErrorResepRWI('Gagal melakukan unPosting', 'Error');
								};
							}
						})
					}
				});
			} else{
				if(cst.pesan=='Periode sudah ditutup'){
					ShowPesanErrorResepRWI('Periode sudah diTutup, tidak dapat melakukan transaksi','Error');
				} else{
					ShowPesanErrorResepRWI('Periode Bulan Lalu Harap Ditutup, tidak dapat melakukan transaksi','Error');
				}
			}
		}
	});
}

function unposting_mrobatRWI(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionInsertMrObat/unposting_mrobat",
			params: getParamUnPostingMrObatRWI(),
			failure: function(o)
			{
				ShowPesanErrorResepRWJ('Error unPosting MR! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				
			}
		}
		
	)
}

function cekPeriodeBulanRwi(detailorderrwi,CurrentGridOrderRWI){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEKrwi/cekBulan",
			params: {a:"no_out"},
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					if(detailorderrwi==true){
						setLookUp_viApotekResepRWI();
						viewDetailGridOrderRWI(CurrentGridOrderRWI);
						
					} else{
						setLookUp_viApotekResepRWI();
					}
					
				} else{
					if(cst.pesan=='Periode Bulan ini sudah Ditutup'){
						ShowPesanErrorResepRWI('Periode bulan ini sudah diTutup, tidak dapat melakukan transaksi','Error');
					} else{
						ShowPesanErrorResepRWI('Periode Bulan Lalu Harap Ditutup, tidak dapat melakukan transaksi','Error');
					}
					
				}
			}
		}
	);
}

function total_pasien_order_mng_obtrwi()
{
	Ext.Ajax.request(
	{
		url: baseURL + "index.php/apotek/functionAPOTEKrwi/countpasienmr_resep_rwi",
			params: {
			command: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
		},
		failure: function(o)
		{
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);

			Ext.getCmp('txtcounttr_apt_rwi').setValue(cst.countpas);
			console.log(cst);
		}
	});
};

function total_pasien_dilayani_order_mng_obtrwi(){
	Ext.Ajax.request(
	{
		url: baseURL + "index.php/apotek/functionAPOTEKrwi/countpasienmrdilayani_resep_rwi",
			params: {
			command: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
		},
		failure: function(o)
		{
			var cst = Ext.decode(o.responseText);
			
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);

			Ext.getCmp('txtcounttrDilayani_apt_rwi').setValue(cst.countpas);
			console.log(cst);
		}
	});
}


function updatestatus_permintaan(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEKrwi/update_obat_mng_rwi",
			params: getParamCloseOrder(),
			failure: function(o)
			{
				ShowPesanErrorResepRWI('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				ShowPesanInfoResepRWI('Obat sudah selesai dilayani','order')
			}
		}
		
	)
	
}



function dataGridObatApotekResep_penatarwi(id_mrresep,cuss,order_mng){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEKrwi/getobatdetail_frompoli",
			params: {query:id_mrresep,
					 cus :cuss
			},
			failure: function(o)
			{
				//ShowPesanErrorResepRWI('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{    ResepRWI.form.Grid.a.store.removeAll();
				//dsDataGrdJab_viApotekResepRWI.loadData([],false);
				var cst = Ext.decode(o.responseText);

				if (cst.success === true) 
				{
					console.log(cst);
					
					var recs=[],
						recType=dsDataGrdJab_viApotekResepRWI.recordType;

					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
						
					}
					dsDataGrdJab_viApotekResepRWI.add(recs);
					
					ResepRWI.form.Grid.a.getView().refresh();
					
					
					getAdm(Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').getValue());
					cekDilayani(id_mrresep,order_mng)
				} 
				else 
				{
					ShowPesanErrorResepRWI('Gagal membaca Data ini', 'Error');
				};
			}
		}
		
	)
	
}

function load_data_pasienorder(param)
{
	Ext.Ajax.request(
	{
		url: baseURL + "index.php/apotek/functionAPOTEKrwi/getPasienorder_mng_rwi",
		params:{
			command: param
		} ,
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cbopasienorder_mng_apotek_rwi.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dspasienorder_mng_apotek_rwi.recordType;
				var o=cst['listData'][i];
			
				recs.push(new recType(o));
				dspasienorder_mng_apotek_rwi.add(recs);
				console.log(o);
			}
		}
	});
}

function viewGridOrderAll_RASEPRWI(nama,tgl){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEKrwi/vieworderall_rwi",
			params: {
				nama:nama,
				tgl:tgl
			},
			failure: function(o)
			{
				ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{   
				//dataSourceGridOrder_viApotekResepRWI.loadData([],false);
				dataSourceGridOrder_viApotekResepRWI.removeAll();
				var cst = Ext.decode(o.responseText);

				if (cst.success === true) 
				{
					console.log(cst);
					
					var recs=[],
						recType=dataSourceGridOrder_viApotekResepRWI.recordType;

					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));

						
					}
					dataSourceGridOrder_viApotekResepRWI.add(recs);
					
					
					GridDataViewOrderManagement_viApotekResepRWI.getView().refresh();
			
				} 
				else 
				{
					ShowPesanErrorResepRWJ('Gagal membaca Data ini', 'Error');
				};
			}
		}
		
	);
}

function printbill_ResepRWI()
{
    Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/main/CreateDataObj",
			params: datacetakbill(),
			failure: function(o)
			{	
				ShowPesanErrorResepRWI('Error hubungi admin' ,'Error');
			},
			success: function(o)
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					
				}
				else if  (cst.success === false && cst.pesan===0)
				{
					ShowPesanWarningResepRWI('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
				}
				else
				{
					ShowPesanErrorResepRWI('Gagal print '  + 'Error');
				}
			}
		}
	)
}

function printkwitansi_resep_rwi()
{
    Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/cetakKwitansi/save",
			params: {
				Table: 'cetakKwitansi',
				kd_pasien:Ext.getCmp('txtKd_pasienPrintKwitansi_viApotekResepRWI').getValue(),
				pembayar:Ext.getCmp('txtNamaPembayarPrintKwitansi_viApotekResepRWI').getValue(),
				jumlah_bayar:Ext.getCmp('txtJumlahBayarPrintKwitansi_viApotekResepRWI').getValue(),
				nama:ResepRWI.form.ComboBox.namaPasien.getValue(),
				keterangan_bayar:Ext.getCmp('txtKeteranganPembayaranPrintKwitansi_viApotekResepRWI').getValue(),
				no_resep:Ext.getCmp('txtNoResepApotekResepRWIL').getValue(),
				printer:Ext.getCmp('cbopasienorder_printer_rwi').getValue(),
				kd_form:KdFormResepRWI
			},
			failure: function(o)
			{	
				ShowPesanErrorResepRWI('Error hubungi admin' ,'Error');
			},
			success: function(o)
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					
				}
				else if  (cst.success === false && cst.pesan===0)
				{
					ShowPesanWarningResepRWI('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
				}
				else
				{
					ShowPesanErrorResepRWI('Gagal print '  + 'Error');
				}
			}
		}
	)
}

function cekDilayani(id_mrresep,order_mng){
	if(order_mng == 'Dilayani'){
		ShowPesanInfoResepRWI('Resep pasien ini sudah dilayani dan pembayaran/transfer sudah dilakukan. Untuk melihat detail resep ini dapat di lihat di daftar resep telah di buat!','Information');
		setLookUps_viApotekResepRWI.close();
	} else if(order_mng == ''){
		
	}else{
		Ext.Ajax.request(
		{
			url: baseURL + "index.php/apotek/functionAPOTEKrwi/cekDilayani",
			params: getParamCekDilayani(id_mrresep),
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)  {
					
					/* 	ShowPesanInfoResepRWI('Resep pasien ini sudah dilayani dan pembayaran/transfer sudah dilakukan. Untuk melihat detail resep ini dapat di lihat di daftar resep telah di buat!','Information');
						setLookUps_viApotekResepRWI.close(); */
					
						Ext.getCmp('btnAddObatRWI').enable();
						Ext.getCmp('btnSimpan_viApotekResepRWI').enable();
						Ext.getCmp('btnSimpanExit_viApotekResepRWI').enable();
						Ext.getCmp('btnDelete_viApotekResepRWI').enable();
						Ext.getCmp('statusservice_apt_RWI').enable();
						Ext.getCmp('btnBayar_viApotekResepRWI').disable();
						Ext.getCmp('btnTransfer_viApotekResepRWI').disable();
					
				} else{
					ShowPesanInfoResepRWI('Resep pasien ini sudah dibuat. Pembayaran belum dilakukan, untuk pembayaran/transfer resep ini dapat di lihat di daftar resep diatas!','Warning');
					setLookUps_viApotekResepRWI.close();
					Ext.getCmp('btnAddObatRWI').disable();
					Ext.getCmp('btnSimpan_viApotekResepRWI').disable();
					Ext.getCmp('btnSimpanExit_viApotekResepRWI').disable();
					Ext.getCmp('btnDelete_viApotekResepRWI').disable();
					Ext.getCmp('statusservice_apt_RWI').disable();
					Ext.getCmp('btnBayar_viApotekResepRWI').enable();
					//Ext.getCmp('btnTransfer_viApotekResepRWI').enable();
					
				}
			}
		});
	}
	
	
}


function getParamResepRWI() 
{
	var KdCust='';
	var TmpCustoLama='';
	var tmpNonResep='';
	var tmpNamaPasien='';
	if(Ext.get('txtTmpKdCustomerApotekResepRWIL').getValue()=='Perseorangan'){
			KdCust='0000000001';
	}else if(Ext.get('txtTmpKdCustomerApotekResepRWIL').getValue()=='Perusahaan'){
		KdCust=Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').getValue();
	}else {
		KdCust=Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').getValue();
	}
	
	/* if(Ext.getCmp('cbNonResep').getValue() == false){
		tmpNonResep = 1;
		tmpNamaPasien = AptResepRWJ.form.ComboBox.namaPasien.getValue();
	}else{
		tmpNonResep = 0;
		tmpNamaPasien = Ext.getCmp('txtNamaPasienNon').getValue();
	} */
	
	var ubah=0;
	if(Ext.getCmp('txtNoResepApotekResepRWIL').getValue() === '' || Ext.getCmp('txtNoResepApotekResepRWIL').getValue()=='Nomor Resep'){
		ubah=0;
	} else{
		ubah=1;
	}
	
    var params =
	{
		KdPasien:Ext.getCmp('cboKodePasienResepRWI').getValue(),
		NmPasien:ResepRWI.form.ComboBox.namaPasien.getValue(),		
		KdUnit: Ext.getCmp('txtTmpKdUnitApotekResepRWIL').getValue(),
		KdDokter:Ext.getCmp('txtTmpKdDokterApotekResepRWIL').getValue(),
		//NonResep:tmpNonResep,
		NoResepAsal:Ext.getCmp('txtNoResepApotekResepRWIL').getValue(),
		NoOutAsal:Ext.getCmp('txtTmpNooutResepRWIL').getValue(),
		TglOutAsal:Ext.getCmp('txtTmpTgloutResepRWIL').getValue(),
		StatusPost:Ext.getCmp('txtTmpStatusPostResepRWIL').getValue(),
		Tanggal:tanggal,
		JamOut:jam,
		DiscountAll:toInteger(Ext.getCmp('txtTotalDiscResepRWIL').getValue()),
		AdmRacikAll:toInteger(Ext.getCmp('txtAdmRacikResepRWIL').getValue()),
		JasaTuslahAll:toInteger(Ext.getCmp('txtTuslahEmbalaseL').getValue()),
		Adm:toInteger(Ext.getCmp('txtAdmResepRWIL').getValue()),
		Admprsh:toInteger(Ext.getCmp('txtAdmPrshResepRWIL').getValue()),
		Kdcustomer:KdCust,
		NoTransaksiAsal:ResepRWI.vars.no_transaksi,
		KdKasirAsal:ResepRWI.vars.kd_kasir,
		SubTotal:toInteger(Ext.getCmp('txtJumlahTotalResepRWIL').getValue()),
		Total:toInteger(Ext.getCmp('txtTotalBayarResepRWIL').getValue()),
		NoKamar:Ext.getCmp('txtNoKamarApotekResepRWIL').getValue(),
		JumlahItem:Ext.getCmp('txtTmpTotQtyResepRWIL').getValue(),
		Shift: tampungshiftsekarang,
		Ubah:ubah,
		Posting:0,
		IdMrResep:CurrentIdMrResepRwi, //id_mrresep untuk update status resep = sedang dilayani
		TglResep:Ext.getCmp('dfTglResepSebenarnyaResepRWI').getValue()
		
	};
	
	params['jumlah']=dsDataGrdJab_viApotekResepRWI.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viApotekResepRWI.getCount();i++)
	{
		if(dsDataGrdJab_viApotekResepRWI.data.items[i].data.racik == 'Ya'){
			params['racik-'+i]=1;
		} else{
			params['racik-'+i]=0;
		}
		params['cito-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.cito;
		params['kd_prd-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_prd
		params['nama_obat-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.nama_obat
		params['kd_satuan-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_satuan
		params['harga_jual-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.harga_jual
		params['harga_beli-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.harga_beli
		params['kd_pabrik-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_pabrik
		params['jml-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.jml
		params['markup-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.markup
		params['disc-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.disc
		params['dosis-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.dosis
		params['jasa-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.jasa
		params['no_out-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.no_out
		params['no_urut-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.no_urut
		
	}
	
    return params
};

function getParamBayarResepRWI() 
{
	
	var KdCust='';
	var TmpCustoLama='';
	var tmpNonResep='';
	var tmpNamaPasien='';
	if(Ext.get('txtTmpKdCustomerApotekResepRWIL').getValue()=='Perseorangan'){
			KdCust='0000000001';
	}else if(Ext.get('txtTmpKdCustomerApotekResepRWIL').getValue()=='Perusahaan'){
		KdCust=Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').getValue();
	}else {
		KdCust=Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').getValue();
	}
	
	var ubah=0;
	if(Ext.getCmp('txtNoResepApotekResepRWIL').getValue() === '' || Ext.getCmp('txtNoResepApotekResepRWIL').getValue()=='Nomor Resep'){
		ubah=0;
	} else{
		ubah=1;
	}
	
	
	//Pembayaran
	var LangsungPost = 0;
	var tmpNoresep='';
	if(Ext.getCmp('txtNoResepApotekResepRWIL').getValue() === '' || Ext.getCmp('txtNoResepApotekResepRWIL').getValue() === 'No Resep' ){
		LangsungPost = 1;
		tmpNoresep='';
	}else {
		LangsungPost = 0;
		tmpNoresep = Ext.getCmp('txtNoResepRWI_Pembayaran').getValue();
	}
	
	var kd_pay;
	if(Ext.getCmp('cboPembayaranRWI').getValue() == 'TUNAI'){
		kd_pay='TU';
	} else{
		kd_pay=Ext.getCmp('cboPembayaranRWI').getValue();
	}

	var params =
	{
		NmPasien:ResepRWI.form.ComboBox.namaPasien.getValue(),
		KdUnit: Ext.getCmp('txtTmpKdUnitApotekResepRWIL').getValue(),
		KdDokter:Ext.getCmp('txtTmpKdDokterApotekResepRWIL').getValue(),
		//NonResep:tmpNonResep,
		JamOut:jam,
		DiscountAll:toInteger(Ext.getCmp('txtTotalDiscResepRWIL').getValue()),
		AdmRacikAll:toInteger(Ext.getCmp('txtAdmRacikResepRWIL').getValue()),
		JasaTuslahAll:toInteger(Ext.getCmp('txtTuslahEmbalaseL').getValue()),
		Adm:toInteger(Ext.getCmp('txtAdmResepRWIL').getValue()),
		Admprsh:toInteger(Ext.getCmp('txtAdmPrshResepRWIL').getValue()),
		Kdcustomer:KdCust,
		NoTransaksiAsal:ResepRWI.vars.no_transaksi,
		KdKasirAsal:ResepRWI.vars.kd_kasir,
		SubTotal:toInteger(Ext.getCmp('txtJumlahTotalResepRWIL').getValue()),
		Total:toInteger(Ext.getCmp('txtTotalBayarResepRWIL').getValue()),
		NoOut:Ext.getCmp('txtTmpNooutResepRWIL').getValue(),
		TglOut:Ext.getCmp('txtTmpTgloutResepRWIL').getValue(),
		
		//PembayaranKdPasienBayar:Ext.getCmp('txtkdPasien_Pembayaran').getValue(),
		KdPasien:Ext.getCmp('txtkdPasien_PembayaranRWI').getValue(),
		KdPay:kd_pay,
		JumlahTotal:toInteger(Ext.getCmp('txtTotalResepRWI_Pembayaran').getValue()),
		JumlahTerimaUang:toInteger(Ext.getCmp('txtBayarResepRWI_Pembayaran').getValue()),
		NoResep:tmpNoresep,
		TanggalBayar:Ext.getCmp('dftanggalResepRWI_Pembayaran').getValue(),
		NoKamar:Ext.getCmp('txtNoKamarApotekResepRWIL').getValue(),
		JumlahItem:Ext.getCmp('txtTmpTotQtyResepRWIL').getValue(),
		Posting:LangsungPost,
		Shift: tampungshiftsekarang,
		Tanggal:tanggal,
		Ubah:ubah
	};
	
	params['jumlah']=dsDataGrdJab_viApotekResepRWI.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viApotekResepRWI.getCount();i++)
	{
		params['cito-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.C;
		params['kd_prd-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_prd
		params['nama_obat-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.nama_obat
		params['kd_satuan-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_satuan
		params['harga_jual-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.harga_jual
		params['harga_beli-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.harga_beli
		params['kd_pabrik-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_pabrik
		params['jml-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.jml
		params['markup-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.markup
		params['disc-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.disc
		params['racik-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.racik
		params['dosis-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.dosis
		params['jasa-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.jasa
		params['no_out-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.no_out
		params['no_urut-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.no_urut
	}
	
    return params
};


function getParamTransferResepRWI(){
	var params =
	{
		Kdcustomer:Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').getValue(),
		NoTransaksi:ResepRWI.vars.no_transaksi,
		TglTransaksi:ResepRWI.vars.tgl_transaksi,
		KdKasir:ResepRWI.vars.kd_kasir,
		KdSpesial:kd_spesial_tr,
		KdUnitKamar:kd_unit_kamar_tr,
		NoKamar:no_kamar_tr,
		NoOut:Ext.getCmp('txtTmpNooutResepRWIL').getValue(),
		TglOut:Ext.getCmp('txtTmpTgloutResepRWIL').getValue(),
		
		//PembayaranKdPasienBayar:Ext.getCmp('txtkdPasien_Pembayaran').getValue(),
		KdPasien:Ext.getCmp('txtkdPasienTransfer_ResepRWI').getValue(),
		
		JumlahTotal:toInteger(Ext.getCmp('txtTotalBayarTransfer_ResepRWI').getValue()),
		JumlahTerimaUang:toInteger(Ext.getCmp('txtTotalTransfer_ResepRWI').getValue()),
		TanggalBayar:Ext.getCmp('dftanggalTransfer_ResepRWI').getValue(),
		JumlahItem:Ext.getCmp('txtTmpTotQtyResepRWIL').getValue(),
		Shift: tampungshiftsekarang,
		Tanggal:tanggal
	};
	
	params['jumlah']=dsDataGrdJab_viApotekResepRWI.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viApotekResepRWI.getCount();i++)
	{
		params['kd_prd-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_prd
		params['jml-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.jml
		params['no_urut-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.no_urut
	}
	
    return params
}

function getParamInsertMrObatRWI(){
	var params =
	{
		kd_pasien: Ext.getCmp('cboKodePasienResepRWI').getValue(),
		kd_unit:   Ext.getCmp('txtTmpKdUnitApotekResepRWIL').getValue(),
		tgl_masuk: ResepRWI.vars.tgl_transaksi,
		urut_masuk: ResepRWI.vars.urut_masuk,
		tgl_out:Ext.getCmp('txtTmpTgloutResepRWIL').getValue(),
		no_out:Ext.getCmp('txtTmpNooutResepRWIL').getValue(),
	}
	
	params['jumlah']=dsDataGrdJab_viApotekResepRWI.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viApotekResepRWI.getCount();i++)
	{
		params['kd_prd-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_prd
		params['kd_satuan-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_satuan
		params['jml-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.jml
	}
	
	return params
}


function getParamUnpostingResepRWI() 
{
	var params =
	{
		NoResep:Ext.getCmp('txtNoResepApotekResepRWIL').getValue(),
		NoOut:Ext.getCmp('txtTmpNooutResepRWIL').getValue(),
		TglOut:Ext.getCmp('txtTmpTgloutResepRWIL').getValue()
	}
	
	params['jumlah']=dsDataGrdJab_viApotekResepRWI.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viApotekResepRWI.getCount();i++)
	{
		params['cito-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.C;
		params['kd_prd-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_prd
		params['nama_obat-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.nama_obat
		params['kd_satuan-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_satuan
		params['harga_jual-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.harga_jual
		params['harga_beli-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.harga_beli
		params['kd_pabrik-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_pabrik
		params['jml-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.jml
		params['markup-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.markup
		params['disc-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.disc
		params['racik-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.racik
		params['dosis-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.dosis
		params['no_urut-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.no_urut
	}
	
    return params
}

function getParamUnPostingMrObatRWI(){
	var params =
	{
		kd_pasien: kd_pasien_obbt_rwi,
		kd_unit:   kd_unit_obbt_rwi,
		tgl_masuk: tgl_masuk_obbt_rwi,
		urut_masuk: urut_masuk_obbt_rwi,
		tgl_out:Ext.getCmp('txtTmpTgloutResepRWIL').getValue(),
		no_out:Ext.getCmp('txtTmpNooutResepRWIL').getValue(),
	}
	
	params['jumlah']=dsDataGrdJab_viApotekResepRWI.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viApotekResepRWI.getCount();i++)
	{
		params['kd_prd-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_prd
		params['kd_satuan-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_satuan
		params['jml-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.jml
	}
	
	return params
}

function getParamDeleteHistoryResepRWI() 
{
	var urut=dsTRDetailHistoryBayar.getRange()[gridDTLTRHistoryApotekRWI.getSelectionModel().selection.cell[0]].data.URUT;
	
	var params =
	{
		NoOut:Ext.getCmp('txtTmpNooutResepRWIL').getValue(),
		TglOut:Ext.getCmp('txtTmpTgloutResepRWIL').getValue(),
		urut:urut
	}
	
	
	
    return params
}

function getParamCloseOrder(){
	var params =
	{
		kd_pasien: kd_pasien_obbt_rwi,
		kd_unit:   kd_unit_obbt_rwi,
		tgl_masuk: tgl_masuk_obbt_rwi,
		urut_masuk: urut_masuk_obbt_rwi,
	};
	
	params['jumlah']=dsDataGrdJab_viApotekResepRWI.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viApotekResepRWI.getCount();i++)
	{
		params['kd_prd-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_prd
		params['no_urut-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.no_urut
	}
	
    return params
}

function getParamCekDilayani(id_mrresep){
	//params['jumlah']=dsDataGrdJab_viApotekResepRWI.getCount();
	var urutdtl='';
	var kdprddtl='';
	for(var i = 0 ; i < dsDataGrdJab_viApotekResepRWI.getCount();i++)
	{
		urutdtl += dsDataGrdJab_viApotekResepRWI.data.items[i].data.no_urut + ",";
		kdprddtl += "'" + dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_prd + "',";
	}
	
	
	var params =
	{
		id_mrresep: id_mrresep,
		urut:urutdtl,
		kd_prd:kdprddtl
	};
	
	
	
    return params
}


function datacetakbill(){
	var unit="";
	if(Ext.get('cbo_UnitResepRWIL').getValue() == '' || Ext.get('cbo_UnitResepRWIL').getValue() == 'Unit'){
		unit = "-";
	} else{
		unit = Ext.get('cbo_UnitResepRWIL').getValue();
	}
	
	var kamar="";
	if(Ext.getCmp('txtKamarApotekResepRWIL').getValue() == ''){
		kamar = "-";
	} else{
		kamar = Ext.get('txtKamarApotekResepRWIL').getValue();
	}
	
	var params =
	{
		Table: 'billprintingreseprwi',
		NoResep:Ext.getCmp('txtNoResepApotekResepRWIL').getValue(),
		NoOut:Ext.getCmp('txtTmpNooutResepRWIL').getValue(),
		TglOut:Ext.getCmp('txtTmpTgloutResepRWIL').getValue(),
		KdPasien:Ext.getCmp('cboKodePasienResepRWI').getValue(),
		NamaPasien:ResepRWI.form.ComboBox.namaPasien.getValue(),
		JenisPasien:Ext.get('cboPilihankelompokPasienAptResepRWI').getValue(),
		Kelas:unit,
		Kamar:kamar,
		Dokter:Ext.get('cbo_DokterApotekResepRWI').getValue(),
		Total:Ext.get('txtTotalBayarResepRWIL').getValue(),
		Tot:toInteger(Ext.get('txtTotalBayarResepRWIL').getValue()),
		printer:Ext.getCmp('cbopasienorder_printer_rwi').getValue(),
		kd_form:KdFormResepRWI
		
	}
	params['jumlah']=dsDataGrdJab_viApotekResepRWI.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viApotekResepRWI.getCount();i++)
	{
		params['nama_obat-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.nama_obat;
		params['jml-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.jml;
	}
	
    return params
}

function getCriteriaCariApotekResepRWI()//^^^
{
      	 var strKriteria = "";

           if (Ext.get('TxtFilterGridDataView_RoNumber_viApotekResepRWI').getValue() != "" && Ext.get('TxtFilterGridDataView_RoNumber_viApotekResepRWI').getValue()!=='No. Resep')
            {
                strKriteria = " o.no_resep " + "LIKE upper('%'" + Ext.get('TxtFilterGridDataView_RoNumber_viApotekResepRWI').getValue() +"%')";
            }
            
            if (Ext.get('txtKdNamaPasienResepRWI').getValue() != "" && Ext.get('txtKdNamaPasienResepRWI').getValue() !== 'Kode/Nama Pasien')//^^^
            {
				if(Ext.get('txtKdNamaPasienResepRWI').getValue().substring(0,1)==='0'){
					if (strKriteria == "")
                    {
                        strKriteria = " o.kd_pasienapt " + "LIKE lower('" + Ext.get('txtKdNamaPasienResepRWI').getValue() +"%')" ;
                    }
                    else {
						strKriteria += " o.kd_pasienapt " + "LIKE lower('" + Ext.get('txtKdNamaPasienResepRWI').getValue() +"%')";
					}
				} else {
					 if (strKriteria == "")
                    {
                        strKriteria = " lower(o.nmpasien) " + "LIKE lower('" + Ext.get('txtKdNamaPasienResepRWI').getValue() +"%')" ;
                    }
                    else {
						strKriteria += " lower(o.nmpasien) " + "LIKE lower('" + Ext.get('txtKdNamaPasienResepRWI').getValue() +"%')";
					}
				}
            }
			if (Ext.get('cboStatusPostingApotekResepRWI').getValue() != "")
            {
				if (Ext.get('cboStatusPostingApotekResepRWI').getValue()==='Belum Posting')
				{
					if (strKriteria == "")
                    {
                        strKriteria = " o.tutup " + "= 0" ;
                    }
                    else {
						strKriteria += " and o.tutup " + "= 0";
				    }
				}
				if (Ext.get('cboStatusPostingApotekResepRWI').getValue()==='Posting')
				{
					if (strKriteria == "")
                    {
                        strKriteria = " o.tutup " + "=1" ;
                    }
                    else {
						strKriteria += " and o.tutup " + "=1";
				    }
				}
				if (Ext.get('cboStatusPostingApotekResepRWI').getValue()==='Semua')
				{
					
				}
                 
            }
			if (Ext.get('dfTglAwalApotekResepRWI').getValue() != "" && Ext.get('dfTglAkhirApotekResepRWI').getValue() != "")
            {
                if (strKriteria == "")
				{
					strKriteria = " o.tgl_out between '" + Ext.get('dfTglAwalApotekResepRWI').getValue() + "' and '" + Ext.get('dfTglAkhirApotekResepRWI').getValue() + "'" ;
				}
				else {
					strKriteria += " and o.tgl_out between '" + Ext.get('dfTglAwalApotekResepRWI').getValue() + "' and '" + Ext.get('dfTglAkhirApotekResepRWI').getValue() + "'" ;
				}
                
            }
			
			if (Ext.get('cbo_UnitResepRWI').getValue() != "" && Ext.get('cbo_UnitResepRWI').getValue() != 'Unit/Ruang')
            {
				if (strKriteria == "")
				{
					strKriteria = " o.kd_unit ='" + Ext.getCmp('cbo_UnitResepRWI').getValue() + "'"  ;
				}
				else {
					strKriteria += " and o.kd_unit ='" + Ext.getCmp('cbo_UnitResepRWI').getValue() + "'";
			    }
                
            }
	
		strKriteria= strKriteria + " and left(u.parent,3)='100' and o.returapt=0 and o.kd_unit_far='" + UnitFarAktif_ResepRWI + "' order by o.tgl_out limit 500"
	 return strKriteria;
}


function ValidasiEntryResepRWI(modul,mBolHapus)
{
	var x = 1;
	if(ResepRWI.form.ComboBox.namaPasien.getValue() === '' || dsDataGrdJab_viApotekResepRWI.getCount() === 0 || Ext.getCmp('cboKodePasienResepRWI').getValue() === ''){
		if(ResepRWI.form.ComboBox.namaPasien.getValue() === ''){
			ShowPesanWarningResepRWI('Nama Pasien', 'Warning');
			x = 0;
		} else if(dsDataGrdJab_viApotekResepRWI.getCount() == 0){
			ShowPesanWarningResepRWI('Daftar resep obat belum di isi', 'Warning');
			x = 0;
		} else {
			ShowPesanWarningResepRWI('Kode Pasien', 'Warning');
			x = 0;
		} 
	}
	
	if((Ext.getCmp('txtJumlahTotalResepRWIL').getValue() === '0' || Ext.getCmp('txtJumlahTotalResepRWIL').getValue() === 0) ||
		(Ext.getCmp('txtTotalBayarResepRWIL').getValue() === '0' || Ext.getCmp('txtTotalBayarResepRWIL').getValue() === 0)){
		if(Ext.getCmp('txtJumlahTotalResepRWIL').getValue() === '0' || Ext.getCmp('txtJumlahTotalResepRWIL').getValue() === 0){
			ShowPesanWarningResepRWI('Total harga obat kosong, harap periksa kelengkapan pengisian obat', 'Warning');
			x = 0;
		} else{
			ShowPesanWarningResepRWI('Total bayar kosong, harap periksa kelengkapan pengisian obat', 'Warning');
			x = 0;
		}
	}
	
	for(var i=0; i<dsDataGrdJab_viApotekResepRWI.getCount() ; i++){
		var o=dsDataGrdJab_viApotekResepRWI.getRange()[i].data;
		if(o.kd_prd == undefined || o.kd_prd == ""){
			ShowPesanWarningResepRWI('Obat masih kosong, harap isi baris kosong atau hapus untuk melanjutkan', 'Warning');
			x = 0;
		} else{
			if(o.jml == undefined || o.jumlah == undefined){
				if(o.jml == undefined){
					ShowPesanWarningResepRWI('Qty belum di isi, qty tidak boleh kosong', 'Warning');
					x = 0;
				}else if(o.jumlah == undefined){
					ShowPesanWarningResepRWI('Total obat kosong, harap periksa qty dan kelengkapan pengisian obat', 'Warning');
					x = 0;
				}
			}
			if(parseFloat(o.jml) > parseFloat(o.jml_stok_apt)){
				ShowPesanWarningResepRWI('Qty melebihi stok', 'Warning');
				o.jml=o.jml_stok_apt;
				x = 0;
			}

		}
		
		for(var j=0; j<dsDataGrdJab_viApotekResepRWI.getCount() ; j++){
			var p=dsDataGrdJab_viApotekResepRWI.getRange()[j].data;
			if(i != j && o.kd_prd == p.kd_prd){
				ShowPesanWarningResepRWI('Tidak boleh ada obat yang sama, periksa kembali daftar obat!', 'Warning');
				x = 0;
				break;
			}
		}
	}
	
	return x;
};

function ValidasiBayarResepRWI(modul,mBolHapus)
{
	var x = 1;
	if(Ext.getCmp('cboJenisByrResepRWI').getValue() === '' ){
		ShowPesanWarningResepRWI('Pembayaran belum dipilih', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('cboPembayaranRWI').getValue() === '' || Ext.getCmp('cboPembayaranRWI').getValue() === 'Pilih Pembayaran...'){
		ShowPesanWarningResepRWI('Jenis pembayaran belum dipilih', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('txtBayarResepRWI_Pembayaran').getValue() === ''){
		ShowPesanWarningResepRWI('Jumlah pembayaran belum di isi', 'Warning');
		x = 0;
	}
	
	return x;
};


function ValidasiTransferResepRWI(modul,mBolHapus)
{
	var x = 1;
	if(Ext.getCmp('txtNoTransaksiTransfer_ResepRWI').getValue() === '' || Ext.getCmp('txtNoTransaksiTransfer_ResepRWI').getValue() === 'No Transaksi'){
		ShowPesanWarningResepRWI('No transaksi kosong, harap periksa kembali', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('dftanggalTransaksi_ResepRWI').getValue() === '' ){
		ShowPesanWarningResepRWI('Tanggal transaksi kosong, harap periksa kembali', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('txtNoResepTransfer_ResepRWI').getValue() === '' || Ext.getCmp('txtNoResepTransfer_ResepRWI').getValue() === 'No Resep'){
		ShowPesanWarningResepRWI('No. resep tidak boleh kosong', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('dftanggalTransfer_ResepRWI').getValue() === ''){
		ShowPesanWarningResepRWI('Tanggal tidak boleh kosong', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('txtkdPasienTransfer_ResepRWI').getValue() === ''){
		ShowPesanWarningResepRWI('Kode pasien tidak boleh kosong', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('txtTotalTransfer_ResepRWI').getValue() === ''){
		ShowPesanWarningResepRWI('Jumlah transfer tidak boleh kosong', 'Warning');
		x = 0;
	}
	
	return x;
};

function ShowPesanWarningResepRWI(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorResepRWI(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:290
		}
	);
};


function ShowPesanInfoResepRWI(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:350
		}
	);
};

function load_data_printer_resep_rwi()
{
	var kriteriaPrint_ResepRWI;
	if(PrintBillResepRWI == 'true'){
		kriteriaPrint_ResepRWI='apt_printer_bill_'+UnitFarAktif_ResepRWI;
	} else{
		kriteriaPrint_ResepRWI='apt_printer_kwitansi_'+UnitFarAktif_ResepRWI;
	}

	Ext.Ajax.request(
	{
		url: baseURL + "index.php/apotek/functionAPOTEKrwi/group_printer",
		params:{
			kriteria: kriteriaPrint_ResepRWI
		} ,
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
		success: function(o) {
			cbopasienorder_printer_rwi.store.removeAll();
				var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsprinter_rwi.recordType;
				var o=cst['listData'][i];
				
				recs.push(new recType(o));
				dsprinter_rwi.add(recs);
				console.log(o);
			}
		}
	});
}


function mCombo_printer_rwi()
{ 
 
	var Field = ['alamat_printer'];
    dsprinter_rwi = new WebApp.DataStore({ fields: Field });
	load_data_printer_resep_rwi();
	cbopasienorder_printer_rwi= new Ext.form.ComboBox
	(
		{
			id: 'cbopasienorder_printer_rwi',
			typeAhead		: true,
			triggerAction	: 'all',
			lazyRender		: true,
			mode			: 'local',
			emptyText: '',
			fieldLabel:  'Daftar Printer',
			align: 'Right',
			anchor:'100%',
			store: dsprinter_rwi,
			valueField: 'alamat_printer',
			displayField: 'alamat_printer',
			listeners:
			{
				
			}
		}
	);return cbopasienorder_printer_rwi;
};

function loadDataKodePasienResepRWI(param){
	if (param==='' || param===undefined) {
		param={
			text: '0',
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/functionAPOTEKrwi/getKodePasienResepRWI",
		params: {
			text:param,
			integrasi:integrated
		},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			dsKodePasien_ResepRWI.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsKodePasien_ResepRWI.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsKodePasien_ResepRWI.add(recs);
				console.log(o);
			}
		}
	});
};

function mComboKodePasienResepRWI(){
	var Field = ['kd_kasir','no_transaksi','no_kamar','kd_pasien','nama',
					'nama_keluarga','jenis_kelamin','nama_unit','kd_unit',
					'nama_kamar', 'kd_dokter', 'kd_customer','kd_spesial',
					'kd_unit_kamar','urut_masuk','nama_dokter','nama_unit'];
	dsKodePasien_ResepRWI = new WebApp.DataStore({fields: Field});
	loadDataKodePasienResepRWI();
	cboKodePasienResepRWI = new Ext.form.ComboBox
	(
		{
			id: 'cboKodePasienResepRWI',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			hideTrigger		: true,
			store: dsKodePasien_ResepRWI,
			valueField: 'kd_pasien',
			displayField: 'kd_pasien',
			emptyText: 'No. Medrec',
			width:130,
			listeners:
			{
				'select': function(a, b, c)
				{
					Ext.getCmp('cbo_DokterApotekResepRWI').setValue(b.data.nama_dokter);
					Ext.getCmp('cboKodePasienResepRWI').setValue(b.data.kd_pasien);
					ResepRWI.form.ComboBox.namaPasien.setValue(b.data.nama);
					Ext.getCmp('cbo_UnitResepRWIL').setValue(b.data.nama_unit);
					Ext.getCmp('cboPilihankelompokPasienAptResepRWI').setValue(b.data.kd_customer);
					Ext.getCmp('txtNoKamarApotekResepRWIL').setValue(b.data.no_kamar);
					Ext.getCmp('txtKamarApotekResepRWIL').setValue(b.data.nama_kamar);
					ResepRWI.vars.no_transaksi=b.data.no_transaksi;
					ResepRWI.vars.tgl_transaksi=b.data.tgl_transaksi;
					ResepRWI.vars.kd_kasir=b.data.kd_kasir;
					ResepRWI.vars.urut_masuk=b.data.urut_masuk;
					Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').setValue(b.data.kd_customer);
					Ext.getCmp('txtTmpKdDokterApotekResepRWIL').setValue(b.data.kd_dokter);
					Ext.getCmp('txtTmpKdUnitApotekResepRWIL').setValue(b.data.kd_unit);
					Ext.getCmp('btnAddObatRWI').enable();
					Ext.getCmp('btnSimpan_viApotekResepRWI').enable();
					Ext.getCmp('btnSimpanExit_viApotekResepRWI').enable();
					Ext.getCmp('btnDelete_viApotekResepRWI').enable();
					
					var records = new Array();
					records.push(new dsDataGrdJab_viApotekResepRWI.recordType());
					dsDataGrdJab_viApotekResepRWI.add(records);
					row=dsDataGrdJab_viApotekResepRWI.getCount()-1; // menujukan jumlah baris,startEditing(baris dimulai dari 1, kolom 1)
					ResepRWI.form.Grid.a.startEditing(row, 3);	
					Ext.getCmp('btnDelete_viApotekResepRWI').enable();
					Ext.getCmp('btnSimpan_viApotekResepRWI').enable();
					Ext.getCmp('btnSimpanExit_viApotekResepRWI').enable();
					Ext.getCmp('btnBayar_viApotekResepRWI').disable();
					Ext.getCmp('btnDelete_viApotekResepRWI').enable();
					
				},
				keyUp: function(a,b,c){
					
					if(  b.getKey()!=127 ){
						clearTimeout(this.time);
				
						this.time=setTimeout(function(){
							if(cboKodePasienResepRWI.lastQuery != '' ){
								var value="";
								
								if (value!=cboKodePasienResepRWI.lastQuery)
								{
									if (a.rendered && a.innerList != null) {
										a.innerList.update(a.loadingText ? '<div class="loading-indicator">' + a.loadingText + '</div>' : '');
										a.restrictHeight();
										a.selectedIndex = 0;
									}
									a.expand();
									Ext.Ajax.request({
										url: baseURL + "index.php/apotek/functionAPOTEKrwi/getKodePasienResepRWI",
										params: {
											text:cboKodePasienResepRWI.lastQuery,
											integrasi:integrated
										},
										failure: function(o){
											var cst = Ext.decode(o.responseText);
										},	    
										success: function(o) {
											cboKodePasienResepRWI.store.removeAll();
											var cst = Ext.decode(o.responseText);

											for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
												var recs    = [],recType = dsKodePasien_ResepRWI.recordType;
												var o=cst['listData'][i];
										
												recs.push(new recType(o));
												dsKodePasien_ResepRWI.add(recs);
											}
											a.expand();
											if(dsKodePasien_ResepRWI.onShowList != undefined)
												dsKodePasien_ResepRWI.onShowList(cst[showVar]);
											if(cst['listData'].length>0){
													
												a.doQuery(a.allQuery, true);
												a.expand();
												a.selectText(value.length,value.length);
											}else{
											//	if (a.rendered && a.innerList != null) {
													a.innerList.update(a.loadingText ? '&nbsp; Data Tidak Ada' : '');
													a.restrictHeight();
													a.selectedIndex = 0;
												//}
											}
										}
									});
									value=cboKodePasienResepRWI.lastQuery;
								}
							}
						},1000);
					}
				} 
			}
		}
	)
	return cboKodePasienResepRWI;
};

function panelnew_window_printer_resepRWI()
{
    win_printer = new Ext.Window
    (
        {
            id: 'win_printer',
            title: 'Printer',
            closeAction: 'destroy',
            width:320,
            height: 120,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [Itempanel_printer_ResepRWI()],
			fbar:[
				{
					xtype: 'button',
					text: nmBtnOK,
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnOkCetakBill_ResepRWI',
					handler: function()
					{		
						if (Ext.getCmp('cbopasienorder_printer_rwi').getValue()===""){
							ShowPesanWarningResepRWI('Pilih dulu print sebelum cetak', 'Warning');
						}else{
							if(PrintBillResepRWI == 'true'){
								printbill_ResepRWI();
								win_printer.close();
							} else{
								printkwitansi_resep_rwi();
								win_printer.close();
								panelWindowPrintKwitansi_ResepRWI.close();
							}
							
						} 
					}
				},
				{
					xtype: 'button',
					text: nmBtnCancel ,
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnCancelLapRWJPasswordDulu',
					handler: function()
					{
						win_printer.close();
					}
				},
			]

        }
    );

    win_printer.show();
};

function Itempanel_printer_ResepRWI()
{
    var panel_printer = new Ext.Panel
    (
        {
            id: 'panel_printer',
            fileUpload: true,
            layout: 'form',
            width:170,
            height: 120,
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                mCombo_printer_rwi(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                   // style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '100%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
						
                    ]
                }
            ]
        }
    );

    return panel_printer;
};

function panelPrintKwitansi_resepRWI()
{
    panelWindowPrintKwitansi_ResepRWI = new Ext.Window
    (
        {
            id: 'panelWindowPrintKwitansi_ResepRWI',
            title: 'Print Kwitansi',
           // closeAction: 'destroy',
            width:440,
            height: 280,
            border: false,
            resizable:false,
            plain: true,
           // layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [Itempanel_PrintKwitansiResepRWJ()],
			fbar:[
				{
					xtype: 'button',
					text: nmBtnOK,
					width: 70,
					hideLabel: true,
					id: 'btnOkPrintKwitansi_ResepRWI',
					handler: function()
					{
						/* var params={
							kd_pasien:Ext.getCmp('txtKd_pasienPrintKwitansi_viApotekResepRWI').getValue(),
							pembayar:Ext.getCmp('txtNamaPembayarPrintKwitansi_viApotekResepRWI').getValue(),
							jumlah_bayar:Ext.getCmp('txtJumlahBayarPrintKwitansi_viApotekResepRWI').getValue(),
							nama:ResepRWI.form.ComboBox.namaPasien.getValue(),
							keterangan_bayar:Ext.getCmp('txtKeteranganPembayaranPrintKwitansi_viApotekResepRWI').getValue(),
						} 
						var form = document.createElement("form");
						form.setAttribute("method", "post");
						form.setAttribute("target", "_blank");
						form.setAttribute("action", baseURL + "index.php/apotek/cetakKwitansi/cetak");
						var hiddenField = document.createElement("input");
						hiddenField.setAttribute("type", "hidden");
						hiddenField.setAttribute("name", "data");
						hiddenField.setAttribute("value", Ext.encode(params));
						form.appendChild(hiddenField);
						document.body.appendChild(form);
						form.submit();		
						panelWindowPrintKwitansi_ResepRWI.close(); */
						PrintBillResepRWI='false';
						panelnew_window_printer_resepRWI();
					}
				},
				{
					xtype: 'button',
					text: nmBtnCancel ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelPrintKwitansi_ResepRWI',
					handler: function()
					{
						panelWindowPrintKwitansi_ResepRWI.close();
					}
				} 
			]

        }
    );

    panelWindowPrintKwitansi_ResepRWI.show();
	getDataPrintKwitansiResepRWI();
};

function Itempanel_PrintKwitansiResepRWJ()
{
    var items=
    (
        {
            id: 'panelItemPrintKwitansiRWJ',
			layout:'form',
			border: true,
			bodyStyle:'padding: 5px',
			height: 215,
			items:
			[
				{
					layout: 'absolute',
					bodyStyle: 'padding: 5px ',
					border: true,
					//width: 300,
					//height: 200,
					anchor: '100% 100%',
					items:
					[
						{
							x: 10,
							y: 10,
							xtype: 'label',
							text: 'No. Medrec'
						},
						{
							x: 140,
							y: 10,
							xtype: 'label',
							text: ':'
						},
						{
							x: 150,
							y: 10,
							xtype: 'textfield',
							id: 'txtKd_pasienPrintKwitansi_viApotekResepRWI',
							name: 'txtKd_pasienPrintKwitansi_viApotekResepRWI',
							width: 80,
							readOnly: true
						},
						{
							x: 10,
							y: 40,
							xtype: 'label',
							text: 'Nama Pembayar'
						},
						{
							x: 140,
							y: 40,
							xtype: 'label',
							text: ':'
						},
						{
							x: 150,
							y: 40,
							xtype: 'textfield',
							id: 'txtNamaPembayarPrintKwitansi_viApotekResepRWI',
							name: 'txtNamaPembayarPrintKwitansi_viApotekResepRWI',
							width: 180,
						},
						{
							x: 10,
							y: 70,
							xtype: 'label',
							text: 'Untuk Pembayaran'
						},
						{
							x: 140,
							y: 70,
							xtype: 'label',
							text: ':'
						},
						{
							x: 150,
							y: 70,
							xtype: 'textarea',
							id: 'txtKeteranganPembayaranPrintKwitansi_viApotekResepRWI',
							name: 'txtKeteranganPembayaranPrintKwitansi_viApotekResepRWI',
							width: 250,
							height: 62,
						},
						{
							x: 10,
							y: 140,
							xtype: 'label',
							text: 'No. Resep'
						},
						{
							x: 140,
							y: 140,
							xtype: 'label',
							text: ':'
						},
						{
							x: 150,
							y: 140,
							xtype: 'textfield',
							id: 'txtNoResepPrintKwitansi_viApotekResepRWI',
							name: 'txtNoResepPrintKwitansi_viApotekResepRWI',
							width: 150,
						},
						{
							x: 10,
							y: 170,
							xtype: 'label',
							text: 'Jumlah Bayar (Rp)',
							style:{'font-weight':'bold'},
						},
						{
							x: 140,
							y: 170,
							xtype: 'label',
							text: ':',
							style:{'font-weight':'bold'},
						},
						{
							x: 150,
							y: 170,
							xtype: 'textfield',
							id: 'txtJumlahBayarPrintKwitansi_viApotekResepRWI',
							name: 'txtJumlahBayarPrintKwitansi_viApotekResepRWI',
							width: 150,
							style:{'text-align':'right','font-weight':'bold'},
							readOnly:true
						},
					]
				}
				
			]	
        }
    );

    return items;
};

function getDataPrintKwitansiResepRWI(){
	Ext.getCmp('txtKd_pasienPrintKwitansi_viApotekResepRWI').setValue(Ext.getCmp('cboKodePasienResepRWI').getValue());
	Ext.getCmp('txtNamaPembayarPrintKwitansi_viApotekResepRWI').setValue(ResepRWI.form.ComboBox.namaPasien.getValue());
	Ext.getCmp('txtNoResepPrintKwitansi_viApotekResepRWI').setValue(Ext.getCmp('txtNoResepApotekResepRWIL').getValue());
	Ext.getCmp('txtJumlahBayarPrintKwitansi_viApotekResepRWI').setValue(Ext.getCmp('txtTotalBayarResepRWIL').getValue());
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEK/getTemplateKwitansi",
			params: {query:''},
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					Ext.getCmp('txtKeteranganPembayaranPrintKwitansi_viApotekResepRWI').setValue(cst.template +" "+ ResepRWI.form.ComboBox.namaPasien.getValue());
				}
				else 
				{
					loadMask.hide();
					ShowPesanErrorResepRWJ('Gagal membaca template kwitansi', 'Error');
				};
			}
		}
		
	)
}

function dataaddnew_viApotekResepRWI() 
{
	Ext.getCmp('txtNoResepApotekResepRWIL').setValue('');
	Ext.getCmp('cboKodePasienResepRWI').setValue('');
	Ext.getCmp('txtTmpKdUnitApotekResepRWIL').setValue('');
	Ext.getCmp('txtTmpKdDokterApotekResepRWIL').setValue('');
	Ext.getCmp('txtTmpNooutResepRWIL').setValue('');
	Ext.getCmp('txtTmpTgloutResepRWIL').setValue('');
	Ext.getCmp('txtTmpStatusPostResepRWIL').setValue('');
	Ext.getCmp('txtTmpTotQtyResepRWIL').setValue('');
	Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').setValue("");
	Ext.getCmp('txtTmpSisaAngsuranResepRWIL').setValue("");
	Ext.getCmp('txtNoKamarApotekResepRWIL').setValue('');
	Ext.getCmp('cboPilihankelompokPasienAptResepRWI').setValue('');
	Ext.getCmp('cbo_DokterApotekResepRWI').setValue('');
	Ext.getCmp('cbo_UnitResepRWIL').setValue('');
	Ext.getCmp('txtKamarApotekResepRWIL').setValue('');
	Ext.getCmp('txtTuslahEmbalaseL').setValue(0);
	Ext.getCmp('txtAdmResepRWIL').setValue(0);
	Ext.getCmp('txtAdmPrshResepRWIL').setValue(0);
	Ext.getCmp('txtAdmRacikResepRWIL').setValue(0);
	Ext.getCmp('txtTotalDiscResepRWIL').setValue(0);
	Ext.getCmp('txtJumlahTotalResepRWIL').setValue(0);
	Ext.getCmp('txtTotalBayarResepRWIL').setValue(0);
	Ext.getCmp('dfTglResepSebenarnyaResepRWI').setValue(now_viApotekResepRWI);
	
	ResepRWI.form.ComboBox.namaPasien.focus();
	ResepRWI.form.ComboBox.namaPasien.setValue('');
	ResepRWI.vars.no_transaksi="";
	ResepRWI.vars.kd_kasir="";
	ResepRWI.vars.tgl_transaksi="";
	ResepRWI.vars.urut_masuk="";
	kd_pasien_obbt_rwi="";
	kd_unit_obbt_rwi="";
	tgl_masuk_obbt_rwi="";
	urut_masuk_obbt_rwi="";
	
	Ext.getCmp('btnSimpan_viApotekResepRWI').disable();
	Ext.getCmp('btnSimpanExit_viApotekResepRWI').disable();
	Ext.getCmp('btnunposting_viApotekResepRWI').disable();
	Ext.getCmp('btnPrint_viResepRWI').disable();
	Ext.getCmp('btnBayar_viApotekResepRWI').disable();
	Ext.getCmp('btnDeleteHistory_viApotekResepRWI').disable();
	Ext.getCmp('btnDelete_viApotekResepRWI').disable();
	Ext.getCmp('btnAddObatRWI').disable();
	dsTRDetailHistoryBayar.removeAll();
	dsDataGrdJab_viApotekResepRWI.removeAll();
	ResepRWI.form.Panel.a.update('<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>');
	
}

function getUnitFar_ResepRWI(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEKrwi/getUnitFar",
			params: {query:''},
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorResepRWI('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					UnitFarAktif_ResepRWI = cst.kd_unit_far;
				}
				else 
				{
					loadMask.hide();
					ShowPesanErrorResepRWI('Gagal membaca unitfar', 'Error');
				};
			}
		}
		
	)
	
}

function hitungSetengahResepRWI(){
	var total=0;
	var admRacik=0;
	var totdisc=0;
	var totalall=0;
	var admprs=0;
	var adm=0;
	var totqty=0;
	
	for(var i=0; i<dsDataGrdJab_viApotekResepRWI.getCount() ; i++){
		var jumlahGrid=0;
		var qtygrid=0;
		var subJumlah=0;
		var sisa=0;
		
		
	
		var o=dsDataGrdJab_viApotekResepRWI.getRange()[i].data;
		if(o.jml != undefined || o.jml != ""){
			// Jika qty tidak melebihi stok tersedia
			if(parseFloat(o.jml) <= parseFloat(o.jml_stok_apt)){
				// Cek qty ganjil atau tidak
				if((parseInt(o.jml) % 2) == 1){
					sisa = parseInt(o.jml) - 1; //Dikurangi 1 supaya genap
					qtygrid = parseInt(sisa) / 2; //Setelah dikurang lalu di bagi 2
					qtygrid = qtygrid + 1; //Setelah dibagi 2 lalu di tambahkan 1 dari pengurangan menjadi genap sebelumnya
					o.jml = qtygrid;
				} else{
					qtygrid = parseInt(o.jml) / 2;
					o.jml = qtygrid;
				}
				
				if(o.racik != undefined || o.racik != ""){
					if(isNaN(admRacik)){
						admRacik +=0;
					} else {
						if(o.racik == 'Ya'){
							admRacik += parseFloat(o.adm_racik) * 1;
						} else{
							admRacik +=0;
						}
						
					}
				}
				
				jumlahGrid = ((parseFloat(o.jml) * parseFloat(o.harga_jual)) - parseFloat(o.disc));
				totdisc += parseFloat(o.disc);
				o.jumlah =jumlahGrid;
				total +=jumlahGrid;
			}else {
				ShowPesanWarningResepRWI('Jumlah obat melebihi stok yang tersedia','Warning');
				o.jml=o.jml_stok_apt;
			}
			totqty +=parseInt(o.jml);
			
		}
	}
	//admRacik=parseFloat(Ext.getCmp('txtAdmRacikResepRWIL').getValue())// * parseFloat(o.racik);
	Ext.get('txtTmpTotQtyResepRWIL').dom.value=totqty;
	total=aptpembulatan(total);
	Ext.getCmp('txtJumlahTotalResepRWIL').setValue(total);
	Ext.getCmp('txtAdmRacikResepRWIL').setValue(toFormat(admRacik));
	Ext.get('txtTotalDiscResepRWIL').dom.value=toFormat(totdisc);
	admprs=toInteger(total)*parseFloat(Ext.getCmp('txtAdmResepRWIL').getValue());
	Ext.getCmp('txtAdmPrshResepRWIL').setValue(toFormat(admprs));
	totalall +=toInteger(total) + admRacik + parseInt(Ext.get('txtTuslahEmbalaseL').getValue()) + parseFloat(Ext.get('txtAdmResepRWIL').getValue()) + admprs - totdisc;
	totalall=aptpembulatan(totalall);
	Ext.getCmp('txtTotalBayarResepRWIL').setValue(toFormat(totalall));
	ResepRWI.form.Grid.a.getView().refresh();
}

function formulaRacikanResepRWI(){
	var lebar = 450;
	var stmpnoOut=0;
	var stmptgl;
    setLookUpRacikan_ResepRWI = new Ext.Window
    ({
        id: 'setLookUpRacikan_ResepRWI',
		name: 'setLookUpRacikan_ResepRWI',
        title: 'Formulasi Racikan Obat', 
        closeAction: 'destroy',        
        width: 523,
        height: 260,
        resizable:false,
		autoScroll: false,
        border: true,
        constrain : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: [ 
			PanelFormulasiRacikan_ResepRWI()
			/* getItemPanelBiodataTransfer_viApotekResepRWI(),
			getItemPanelTotalBayar_ApotekResepRWI() */
		],
		fbar:[
			{
				xtype:'button',
				text:'OK',
				width:70,
				hideLabel:true,
				id: 'btnOKRacikan_viApotekResepRWIL',
				handler:function()
				{
					setHasilFormula();
				}   
			},
			{
				xtype:'button',
				text:'Batal',
				width:70,
				hideLabel:true,
				id: 'btnBatalRacikan_viApotekResepRWIL',
				handler:function()
				{
					setLookUpRacikan_ResepRWI.close();
				}   
			}
		],
        listeners:
        {
            activate: function()
            {
				Ext.getCmp('txtPersediaanObatRacikan_viApotekResepRWI').focus();
            },
            afterShow: function()
            {
                this.activate();
				Ext.getCmp('txtPersediaanObatRacikan_viApotekResepRWI').focus();
				// ;
            },
            deactivate: function()
            {
             //   rowSelected_viApotekResepRWI=undefined;
            }
        }
    });

    setLookUpRacikan_ResepRWI.show();
	Ext.getCmp('txtPersediaanObatRacikan_viApotekResepRWI').focus();
	Ext.getCmp('txtkodeObatRacikan_viApotekResepRWI').setValue(currentKdPrdRacik);
	Ext.getCmp('txtNamaObatRacikan_viApotekResepRWI').setValue(currentNamaObatRacik);
	Ext.getCmp('txtaHargaObatRacikan_viApotekResepRWI').setValue(currentHargaRacik);
	Ext.getCmp('txtQtyResepRacikanRacikan_viApotekResepRWI').setValue(currentJumlah);
}

function PanelFormulasiRacikan_ResepRWI(){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		height: 195,
		items:
		[
			/* {
				layout: 'column',
				border: false,
				items:
				[ */
					{
						
						layout: 'absolute',
						bodyStyle: 'padding: 10px ',
						border: true,
						width: 495,
						height: 40,
						anchor: '100% 23%',
						items:
						[
							
							{
								x: 10,
								y: 10,
								xtype: 'label',
								text: 'Nama obat'
							},
							{
								x: 70,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 80,
								y: 10,
								xtype: 'textfield',
								id: 'txtkodeObatRacikan_viApotekResepRWI',
								name: 'txtkodeObatRacikan_viApotekResepRWI',
								width: 70,
								readOnly: true
							},
							{
								x: 155,
								y: 10,
								xtype: 'textfield',
								id: 'txtNamaObatRacikan_viApotekResepRWI',
								name: 'txtNamaObatRacikan_viApotekResepRWI',
								width: 160,
								readOnly: true
							},
							{
								x: 328,
								y: 10,
								xtype: 'label',
								text: 'Harga satuan'
							},
							{
								x: 400,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 410,
								y: 10,
								xtype: 'numberfield',
								id: 'txtaHargaObatRacikan_viApotekResepRWI',
								name: 'txtaHargaObatRacikan_viApotekResepRWI',
								style:{'text-align':'right'},
								width: 75,
								readOnly: true
							},
						]
					},
					{
						
						layout: 'absolute',
						bodyStyle: 'padding: 5px ',
						border: false,
						width: 495,
						height: 1,
						items:
						[
							
							{
								xtype: 'label',
								text: ''
							},
						]
					},
					{
						
						layout: 'absolute',
						bodyStyle: 'padding: 5px ',
						border: true,
						width: 500,
						height: 50,
						anchor: '100% 72%',
						items:
						[
							
							{
								x: 10,
								y: 10,
								xtype: 'label',
								text: 'Dosis persediaan obat farmasi'
							},
							{
								x: 170,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 180,
								y: 10,
								xtype: 'numberfield',
								id: 'txtPersediaanObatRacikan_viApotekResepRWI',
								name: 'txtPersediaanObatRacikan_viApotekResepRWI',
								style:{'text-align':'right'},
								width: 75,
							},
							{
								x: 10,
								y: 40,
								xtype: 'label',
								text: 'Dosis resep / permintaan dokter'
							},
							{
								x: 170,
								y: 40,
								xtype: 'label',
								text: ':'
							},
							{
								x: 180,
								y: 40,
								xtype: 'numberfield',
								id: 'txtDosisPermintaanDokterRacikan_viApotekResepRWI',
								name: 'txtDosisPermintaanDokterRacikan_viApotekResepRWI',
								style:{'text-align':'right'},
								width: 75
							},
							
							{
								x: 10,
								y: 70,
								xtype: 'label',
								text: 'Quantity resep racikan'
							},
							{
								x: 170,
								y: 70,
								xtype: 'label',
								text: ':'
							},
							{
								x: 180,
								y: 70,
								xtype: 'numberfield',
								id: 'txtQtyResepRacikanRacikan_viApotekResepRWI',
								name: 'txtQtyResepRacikanRacikan_viApotekResepRWI',
								style:{'text-align':'right'},
								width: 75,
								listeners: {
									specialkey: function(){
										if(Ext.EventObject.getKey()==13){
											HitungRacikanObat();
										}
									}
								}
							},
							{
								x: 10,
								y: 100,
								xtype: 'label',
								text: 'Quantity obat yang dikeluarkan'
							},
							{
								x: 170,
								y: 100,
								xtype: 'label',
								text: ':'
							},
							{
								x: 180,
								y: 100,
								xtype: 'numberfield',
								id: 'txtQtyObatDiKeluarkanRacikan_viApotekResepRWI',
								name: 'txtQtyObatDiKeluarkanRacikan_viApotekResepRWI',
								style:{'text-align':'right'},
								width: 75,
								readOnly: true
							},
							{
								x: 328,
								y: 10,
								xtype: 'label',
								text: 'Sub Total'
							},
							{
								x: 400,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 410,
								y: 10,
								xtype: 'numberfield',
								id: 'txtSubTOtalRacikan_viApotekResepRWI',
								name: 'txtSubTOtalRacikan_viApotekResepRWI',
								style:{'text-align':'right'},
								width: 75,
								readOnly: true
							},
							{
								x: 260,
								y: 75,
								xtype: 'label',
								text: '*) Enter untuk menghitung'
							},
						]
					}
			/* 	]
			}  */
							
		]		
	};
        return items;
}

function HitungRacikanObat(){
	var totqty=0;
	var totharga=0;
	Ext.getCmp('txtPersediaanObatRacikan_viApotekResepRWI').getValue();
	Ext.getCmp('txtDosisPermintaanDokterRacikan_viApotekResepRWI').getValue();
	Ext.getCmp('txtQtyResepRacikanRacikan_viApotekResepRWI').getValue();
	Ext.getCmp('txtQtyObatDiKeluarkanRacikan_viApotekResepRWI').getValue();
	Ext.getCmp('txtSubTOtalRacikan_viApotekResepRWI').getValue();
	
	totqty = (parseFloat(Ext.getCmp('txtDosisPermintaanDokterRacikan_viApotekResepRWI').getValue()) / parseFloat(Ext.getCmp('txtPersediaanObatRacikan_viApotekResepRWI').getValue())) * parseFloat(Ext.getCmp('txtQtyResepRacikanRacikan_viApotekResepRWI').getValue());
	
	Ext.getCmp('txtQtyObatDiKeluarkanRacikan_viApotekResepRWI').setValue(totqty);
	totharga = parseFloat(Ext.getCmp('txtQtyObatDiKeluarkanRacikan_viApotekResepRWI').getValue()) * parseFloat(Ext.getCmp('txtaHargaObatRacikan_viApotekResepRWI').getValue());
	Ext.getCmp('txtSubTOtalRacikan_viApotekResepRWI').setValue(totharga);
}

function setHasilFormula(){
	var o = dsDataGrdJab_viApotekResepRWI.getRange()[curentIndexsSelection].data;
	
	o.jml = Ext.getCmp('txtQtyObatDiKeluarkanRacikan_viApotekResepRWI').getValue();
	o.jumlah = Ext.getCmp('txtSubTOtalRacikan_viApotekResepRWI').getValue();
	ResepRWI.form.Grid.a.getView().refresh();
	ResepRWI.form.Grid.a.startEditing(curentIndexsSelection, 10);
	setLookUpRacikan_ResepRWI.close();
	hasilJumlahResepRWI();
}

function getParamTanggal(tgl)
{
	var param;
	var tglPisah=tgl.split("-");
	var tglAsli;
	if (tglPisah[1]==="Jan")
	{
		tglAsli=tglPisah[0]+"-"+"01"+"-"+tglPisah[2];
	}
	else if (tglPisah[1]==="Feb")
	{
		tglAsli=tglPisah[0]+"-"+"02"+"-"+tglPisah[2];
	}
	else if (tglPisah[1]==="Mar")
	{
		tglAsli=tglPisah[0]+"-"+"03"+"-"+tglPisah[2];
	}
	else if (tglPisah[1]==="Apr")
	{
		tglAsli=tglPisah[0]+"-"+"04"+"-"+tglPisah[2];
	}
	else if (tglPisah[1]==="May")
	{
		tglAsli=tglPisah[0]+"-"+"05"+"-"+tglPisah[2];
	}
	else if (tglPisah[1]==="Jun")
	{
		tglAsli=tglPisah[0]+"-"+"06"+"-"+tglPisah[2];
	}
	else if (tglPisah[1]==="Jul")
	{
		tglAsli=tglPisah[0]+"-"+"07"+"-"+tglPisah[2];
	}
	else if (tglPisah[1]==="Aug")
	{
		tglAsli=tglPisah[0]+"-"+"08"+"-"+tglPisah[2];
	}
	else if (tglPisah[1]==="Sep")
	{
		tglAsli=tglPisah[0]+"-"+"09"+"-"+tglPisah[2];
	}
	else if (tglPisah[1]==="Oct")
	{
		tglAsli=tglPisah[0]+"-"+"10"+"-"+tglPisah[2];
	}
	else if (tglPisah[1]==="Nov")
	{
		tglAsli=tglPisah[0]+"-"+"11"+"-"+tglPisah[2];
	}
	else if (tglPisah[1]==="Dec")
	{
		tglAsli=tglPisah[0]+"-"+"12"+"-"+tglPisah[2];
	}
	else
	{
		tglAsli=tgl;
	}
	return tglAsli;
}


