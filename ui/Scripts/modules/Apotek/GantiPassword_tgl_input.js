var variable_apotek = {};

variable_apotek.form_windows = function(){
    // console.log("Cek");
    var panel = new Ext.Window
    (
        {
            title: 'Ganti password',
            closeAction: 'destroy',
            width:500,
            border: false,
            resizable:false,
            bodyStyle   : 'padding:5px;',
            plain: true,
            layout: 'form',
            iconCls: 'icon_lapor',
            modal: true,
            items: [
                {
                    xtype       : 'textfield',
                    inputType   : 'password',
                    fieldLabel  : 'Password Lama',
                    width       : '100%',
                },{
                    xtype       : 'textfield',
                    inputType   : 'password',
                    fieldLabel  : 'Password Baru',
                    width       : '100%',
                },{
                    xtype       : 'textfield',
                    inputType   : 'password',
                    fieldLabel  : 'Password Konfirmasi',
                    width       : '100%',
                },
            ],
            listeners:
			{
				activate: function()
				{
				}
			},
			fbar:[
				{
					xtype:'button',
					text: 'Ok',
					handler:function(btn){
                        Ext.Ajax.request({
                            url         : baseURL + "index.php/main/Controller_kunjungan/get_custom_data",
                            params      : {
                                select  : " count(*) as nilai ",
                                where   : " setting ilike '%"+panel.items.items[0].getValue()+"%' ",
                                table   : " sys_setting ",
                            },
                            success: function(o){
                                var cst     = Ext.decode(o.responseText);
                                if (cst[0].nilai > 0) {
                                    if (panel.items.items[1].getValue() != panel.items.items[2].getValue()) {
                                        variable_apotek.show_pesan('Password konfirmasi salah', 'Peringatan');
                                    }else{
                                        Ext.Ajax.request({
                                            url         : baseURL + "index.php/apotek/Apotek/ganti_password_apotek",
                                            params      : {
                                                password_baru : panel.items.items[1].getValue(),
                                            },
                                            success: function(o){
                                                var cst     = Ext.decode(o.responseText);
                                                if (cst.status === true) {
                                                    variable_apotek.show_pesan('Password berhasil diganti', 'Peringatan');
                                                }
                                            }
                                        });
                                    }
                                }else{
                                    variable_apotek.show_pesan('Password lama salah', 'Peringatan');
                                }
                                // Ext.getCmp('txtNoKwitansi').setValue(no_kwitansi);
                            }
                        });
                        // console.log(panel.items.items[0].getValue());
					}
				},{
					xtype:'button',
					text: 'Close',
					handler:function()
					{
						panel.close();
					}
				},
			]
        }
    );

    return panel;
};

variable_apotek.show_pesan = function (str, modul) {
    Ext.MessageBox.show
    (
        {
            title: modul,
            msg: str,
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.INFO,
            width:250
        }
    );
};
variable_apotek.window= variable_apotek.form_windows();
variable_apotek.window.show();