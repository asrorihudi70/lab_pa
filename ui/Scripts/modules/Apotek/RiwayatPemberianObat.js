var CurrentRiwayatPemberianObat =
{
    data: Object,
    details: Array,
    row: 0
};

var dsUnitFar;
var dsTRRiwayatPemberianObatList;
var grListRiwayatPemberianObat;
var AddNewDiagnosa = true;
var now = new Date();
var tigaharilalu = new Date().add(Date.DAY, -3);
var rowSelectedRiwayatPemberianObat;
var FormLookUpsdetailRiwayatPemberianObat;
var cboUnitFar;
var gridDTRiwayatPemberianObat_pradialisa;
var gridDTRiwayatPemberianObat_dialisa;
var gridDTRiwayatPemberianObat_pascadialisa;
var rowSelected_HDPradialisa;
var rowSelected_HDDialisa;
var rowSelected_HDPascadialisa;
var dsTRDetailRiwayatPemberianObat_pradialisa;
var dsTRDetailRiwayatPemberianObat_dialisa;
var dsTRDetailRiwayatPemberianObat_pascadialisa;
var kd_unit_far_aktif;
var nm_unit_far_aktif;
var tmpnama_unit_asal;
var tmpkd_dokter_asal;
var tmpjam_masuk;
var tmpnama_dokter_asal;

var CurrentData_HDPradialisa=
{
	data: Object,
	details: Array,
	row: 0
};
var CurrentData_HDDialisa=
{
	data: Object,
	details: Array,
	row: 0
};
var CurrentData_HDPascadialisa=
{
	data: Object,
	details: Array,
	row: 0
};

var SetupRiwayatPemberianObat={};
SetupRiwayatPemberianObat.form={};
SetupRiwayatPemberianObat.func={};
SetupRiwayatPemberianObat.vars={};
SetupRiwayatPemberianObat.func.parent=SetupRiwayatPemberianObat;
SetupRiwayatPemberianObat.form.ArrayStore={};
SetupRiwayatPemberianObat.form.ComboBox={};
SetupRiwayatPemberianObat.form.DataStore={};
SetupRiwayatPemberianObat.form.Record={};
SetupRiwayatPemberianObat.form.Form={};
SetupRiwayatPemberianObat.form.Grid={};
SetupRiwayatPemberianObat.form.Panel={};
SetupRiwayatPemberianObat.form.TextField={};
SetupRiwayatPemberianObat.form.Button={};

SetupRiwayatPemberianObat.form.ArrayStore.a = new Ext.data.ArrayStore({
	id: 0,
	fields:['status_posting','no_resep','no_out','tgl_out', 'kd_pasienapt', 'nmpasien', 'dokter', 
			'nama_dokter', 'kd_unit', 'nama_unit', 'apt_no_transaksi','tgl_transaksi',
			'apt_kd_kasir', 'kd_customer','admracik','jumlah','jml_terima_uang','sisa','admprhs',
			'jasa','admresep','jasa', 'admprhs','admresep','customer','jenis_pasien','tgl_masuk',
			'urut_masuk','telepon','catatandr','no_sjp','kd_pay','jenis_pay','payment','payment_type','tgl_resep'],
	data: []
});

CurrentPage.page = getPanelHasilHD(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
//membuat form
function getPanelHasilHD(mod_id) 
{
    var Field = [	'status_posting','no_resep','no_out','tgl_out', 'kd_pasienapt', 'nmpasien', 'dokter', 
					'nama_dokter', 'kd_unit', 'nama_unit', 'apt_no_transaksi','tgl_transaksi',
					'apt_kd_kasir', 'kd_customer','admracik','jumlah','jml_terima_uang','sisa','admprhs',
					'jasa','admresep','jasa', 'admprhs','admresep','customer','jenis_pasien','tgl_masuk',
					'urut_masuk','telepon','catatandr','no_sjp','kd_pay','jenis_pay','payment','payment_type','tgl_resep'];
    dsTRRiwayatPemberianObatList = new WebApp.DataStore({ fields: Field }); 
	getcurrentUnitFar();
    dataGridRiwayatPemberianObat(kd_unit_far_aktif);
	grListRiwayatPemberianObat = new Ext.grid.EditorGridPanel 
    (
        {
            stripeRows: true,
            store: dsTRRiwayatPemberianObatList, //data store yang digunakan dalam grid
            //anchor: '100% 91.9999%',
            columnLines: false,
            autoScroll:true,
            border: false,
			sort :false,
			height:390,
            sm: new Ext.grid.RowSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        rowselect: function(sm, row, rec)
                        {
							//alert(Ext.getCmp('dtpTglAwalFilterRiwayatPemberianObat').getValue());
                            rowSelectedRiwayatPemberianObat = dsTRRiwayatPemberianObatList.getAt(row);
							rowSelectedRiwayatPemberianObat = undefined;
							rowSelectedRiwayatPemberianObat = dsTRRiwayatPemberianObatList.getAt(row);
							CurrentRiwayatPemberianObat
							CurrentRiwayatPemberianObat.row = row;
							CurrentRiwayatPemberianObat.data = rowSelectedRiwayatPemberianObat.data;
                        }
                    }
                }
            ),
            listeners:
            {
				//action jika isi grid di double klik
                rowdblclick: function (sm, ridx, cidx)
                {

                    rowSelectedRiwayatPemberianObat = dsTRRiwayatPemberianObatList.getAt(ridx);
					console.log(rowSelectedRiwayatPemberianObat.data);
                    if (rowSelectedRiwayatPemberianObat !== undefined)
                    {
						// alert(rowSelectedRiwayatPemberianObat.data.kd_dokter_asal);
						PenHasilLookUp(rowSelectedRiwayatPemberianObat.data); //memanggil fungsi untuk menampilkan window
						dataDetailResep(rowSelectedRiwayatPemberianObat.data.no_out, rowSelectedRiwayatPemberianObat.data.tgl_out); //load data hd_item_kunjungan berdasarkan no_dia dan jenis_dialisa (pradialisa=0, dialisa=1, pascadialisa=2)
						
                    }
                    else
                    {
                        PenHasilLookUp();
                    }
                }
            },
        cm: new Ext.grid.ColumnModel
            (
			
                [
                    new Ext.grid.RowNumberer(),
					{
                        id: 'colViewNo_out_Resep',
                        header: 'NO BUKTI',
                        dataIndex: 'no_out',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 75
                    },
					{
                        id: 'colViewTgloutResep',
                        header: 'TGL TRANS',
                        dataIndex: 'tgl_out',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 100,
                            renderer: function(v, params, record)
                            {
                                    return ShowDate(record.data.tgl_out);

							}
                    },
                    {
                        id: 'colViewNoResepResep',
                        header: 'NO RESEP',
                        dataIndex: 'no_resep',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 110
                    },
                    {
                        id: 'colViewKdPasienaptResep',
                        header: 'NO MEDREC',
                        dataIndex: 'kd_pasienapt',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 110
                    },
					{
                        id: 'colViewNmPasienResep',
                        header: 'NAMA',
                        dataIndex: 'nmpasien',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 130
                    },
					{
                        id: 'colViewAlamatResep',
						header: 'Alamat',
                        width: 170,
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        dataIndex: 'alamat'                        
                    },
                    {
                        id: 'colViewAlamatResep',
                        header: 'Poliklinik',
                        dataIndex: 'nama_unit',
						sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 130
                    },
					
                   
                ]
            ),
            viewConfig: {forceFit: true},
            tbar:
                [
                    {
                        id: 'btnEditRiwayatPemberianObat',
                        text: 'Lihat Detail Data',
                        tooltip: nmEditData,
                        iconCls: 'Edit_Tr',
                        handler: function(sm, row, rec)
                        {
                            if (rowSelectedRiwayatPemberianObat != undefined)
                            {
								PenHasilLookUp(rowSelectedRiwayatPemberianObat.data);
								dataDetailResep(rowSelectedRiwayatPemberianObat.data.no_out, rowSelectedRiwayatPemberianObat.data.tgl_out); //load data hd_item_kunjungan berdasarkan no_dia dan jenis_dialisa (pradialisa=0, dialisa=1, pascadialisa=2)
								
                            }
                            else
                            {
								ShowPesanWarningRiwayatPemberianObat('Pilih data data tabel  ','Edit data');
                            }
                        }
                    }
                ]
            }
	);
	
	
	//form depan awal dan judul tab
    var FormDepanRiwayatPemberianObat = new Ext.Panel
    (
        {
            id: mod_id,
            closable: true,
            region: 'center',
            layout: 'form',
            title: 'Riwayat Pemberian Obat',
            border: false,
            shadhow: true,
            autoScroll:false,
            iconCls: 'Request',
            margins: '0 5 5 0',
			//height:200,
			autoHeight:true,
            items: [
                    getItemPanelRiwayatPemberianObat(), //panel input
                    grListRiwayatPemberianObat //panel data grid
                   ],
            listeners:
            {
                'afterrender': function()
                {
				}
            }
        }
    );
	
   return FormDepanRiwayatPemberianObat;

};

//mengatur lookup edit data 
function PenHasilLookUp(rowdata) 
{
    FormLookUpsdetailRiwayatPemberianObat = new Ext.Window //window yang berisi data pasien dan grid editor hasil apotek
    (
        {
            id: 'gridRiwayatPemberianObat',
            title: 'FORMULIR DETAIL OBAT',
            closeAction: 'destroy',
            height: 500,
			width:600,
			border: false,
            resizable: false,
            plain: true,
            layout: 'fit',
            constrain: true,
            iconCls: 'Request',
            modal: true,
            items: getFormEntryRiwayatPemberianObat(rowdata),
            listeners:
            {
                
            }
        }
    );

    FormLookUpsdetailRiwayatPemberianObat.show();
    if (rowdata === undefined) {
		
	}
	else{
		DataInitRiwayatPemberianObat(rowdata);
	}

};
function DataInitRiwayatPemberianObat(rowdata){
	Ext.getCmp('TxtNoBukti').setValue(rowdata.no_out);
	Ext.getCmp('dPopupTglTransaksi').setValue(ShowDate(rowdata.tgl_out));
	Ext.getCmp('TxtPopupNoResep').setValue(rowdata.no_resep);
	Ext.getCmp('TxtNamaDetail').setValue(rowdata.nmpasien);
}

//mengatur lookup toolbar
function getFormEntryRiwayatPemberianObat(data) 
{
    var pnlRiwayatPemberianObat = new Ext.FormPanel //panel untuk menampilkan data pasien
    (
        {
            id: 'PanelRiwayatPemberianObat',
            fileUpload: true,
            bodyStyle: 'padding:0px 0px 0px 0px',
            height:110,
            width: 600,
            border: false,
            items: [viewlookupedit()], // item yang berisi data pasien,
			tbar:
			[
				/* {
					id:'btnCetakHasilHD',
					text: 'Cetak',
					tooltip: 'Cetak Hasil',
					iconCls:'print',
					handler: function()
					{
					   getDataHasilHDCetak();
					}
				}, */
				
			],
			
        }
    );
	// toolbar grid lookup
	var x;
	

	var pnlRiwayatPemberianObat2 = new Ext.FormPanel //panel untuk menampilkan tab grid hasil apotek
    (
        {
            id: 'PanelRiwayatPemberianObat2',
            fileUpload: true,
            region: 'center',
            layout: 'fit',
			border:true,
            bodyStyle: 'padding:0px 0px px 0px',
			//autoHeight: true,
			//autoScroll: true,
            anchor: '100%',
            width: 600,
            border: false,
            items: [	GetDTGridRiwayatPemberianObat() //isi panel adalah tab panel
			
			]
        }
    );

    var FormDepanRiwayatPemberianObat = new Ext.Panel //panel yang menampung panel untuk menampilkan data pasien dan panel tab hd
	(
		{
		    id: 'FormDepanRiwayatPemberianObat',
		    width: 600,
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
			//autoScroll: true,
		    items: 
			[ 
				pnlRiwayatPemberianObat,pnlRiwayatPemberianObat2
			]

		}
	);
	
    return FormDepanRiwayatPemberianObat
};

 
//LOOKUP hasil 
function viewlookupedit(){ // fungsi untuk menampilkan data pasien
	var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: 500,
	    labelAlign: 'right',
	    bodyStyle: 'padding:1px 1px 1px 1px',
	    border:true,
		title:'Data Transaksi',
	    height:140,
	    items:
		[
			{
				columnWidth: .99,
				layout: 'absolute',
				bodyStyle: 'padding: 0px 0px 0px 0px',
				border: false,
				width: 100,
				height: 150,
				anchor: '100% 100%',
				items:
				[
					{
						x: 10,
						y: 10,
						xtype: 'label',
						text: 'No. Bukti '
					},
					{
						x: 110,
						y: 10,
						xtype: 'label',
						text: ' : '
					},
					{   
						x : 120,
						y : 10,
						xtype: 'textfield',
						name: 'TxtNoBukti',
						id: 'TxtNoBukti',
						width: 100,
						readOnly:true
					},
					{
						x: 250,
						y: 10,
						xtype: 'label',
						text: 'Tgl Transaksi '
					},
					{
						x: 320,
						y: 10,
						xtype: 'label',
						text: ' : '
					},
					{   //cobacoba
						x : 330,
						y : 10,
						xtype: 'datefield',
						name: 'dPopupTglTransaksi',
						id: 'dPopupTglTransaksi',
						width: 110,
						format: 'd/M/Y',
						readOnly:true
					},
					{
						x: 10,
						y: 40,
						xtype: 'label',
						text: 'No. Resep '
					},
					{
						x: 110,
						y: 40,
						xtype: 'label',
						text: ' : '
					},
					{   
						x : 120,
						y : 40,
						xtype: 'textfield',
						name: 'TxtPopupNoResep',
						id: 'TxtPopupNoResep',
						width: 100,
						readOnly:true
					},
					{
						x: 250,
						y: 40,
						xtype: 'label',
						text: 'Nama '
					},
					{
						x: 320,
						y: 40,
						xtype: 'label',
						text: ' : '
					},
					{   
						x : 330,
						y : 40,
						xtype: 'textfield',
						name: 'TxtNamaDetail',
						id: 'TxtNamaDetail',
						width: 220,
						readOnly:true
					},
					
				]
			},
		]
	};
	return items;
}

//------------GRID DALAM LOOK UP TAB PRADIALISA, DIALISA, PASCADIALISA--------------------------------------------------------------------------
function GetDTGridRiwayatPemberianObat() 
{
	var fm = Ext.form;
    var fldDetailRiwayatPemberianObat_pradialisa = ['kd_item','item_hd','type_item','kd_dia','dialisa','no_urut','nilai','no_ref','deskripsi','nilai'];
	dsTRDetailRiwayatPemberianObat_pradialisa = new WebApp.DataStore({ fields: fldDetailRiwayatPemberianObat_pradialisa })
    
	gridDTRiwayatPemberianObat_pradialisa = new Ext.grid.EditorGridPanel
    (
        {
            title: 'Daftar Obat',
			width: 600,
			height:360,
            stripeRows: true,
			layout: 'fit',
            store: dsTRDetailRiwayatPemberianObat_pradialisa,// DATASTORE
            border: true,
            columnLines: true,
            frame: false,
            autoScroll:true,
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
							rowSelected_HDPradialisa = undefined;
							rowSelected_HDPradialisa = dsTRDetailRiwayatPemberianObat_pradialisa.getAt(row);
							CurrentData_HDPradialisa
							CurrentData_HDPradialisa.row = row;
							CurrentData_HDPradialisa.data = rowSelected_HDPradialisa.data;
                        }
                    }
                }
            ),
           cm: new Ext.grid.ColumnModel
            (
			[
				
				new Ext.grid.RowNumberer(),
				{
					header:'NAMA OBAT',
					dataIndex: 'nama_obat',
					sortable: false,
					hidden:false,
					menuDisabled:true,
					width:100
					
				},
				{
					header:'QTY',
					dataIndex: 'jml_out',
					sortable: false,
					hidden:false,
					menuDisabled:true,
					width:50
					
				},
				{
					header:'HARGA',
					dataIndex: 'harga_jual',
					format : '0,000',
					xtype:'numbercolumn',
					sortable: false,
					hidden:false,
					menuDisabled:true,
					width:50
					
				},
			]
			),
			viewConfig:{forceFit: true},
			tbar:
			[
				
			]
        }
		
    );
	
    return gridDTRiwayatPemberianObat_pradialisa;
};


//AWAL INPUT
function getItemPanelRiwayatPemberianObat() // isi item pada panel pencarian data hd kunjungan
{
    var items =
    {
        layout:'column',
		bodyStyle: 'padding: 5px 0px 5px 10px',
        border:true,
        items:
        [
			//--------------------------------------------------
            {
                columnWidth:.99,
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 600,
                height: 105,
                anchor: '100% 100%',
                items:
                [
					{
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'Unit Farmasi  '
                    },
					{
                        x: 145,
                        y: 10,
                        xtype: 'label',
                        text: ':'
                    },
					comboUnitFar(),
					{
                        x: 370,
                        y: 10,
                        xtype: 'label',
                        text: 'Tanggal Transaksi '
                    },
					{
                        x: 500,
                        y: 10,
                        xtype: 'label',
                        text: ':'
                    },
                    {
                        x: 510,
                        y: 10,
                        xtype: 'datefield',
                        id: 'dtpTglAwalFilterRiwayatPemberianObat',
                        format: 'd/M/Y',
                        value: now,
                        listeners:
                        { 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) //ketika di enter akan ditampilkan data sesuai inputan tanggal
								{
									dataGridRiwayatPemberianObat(
										kd_unit_far_aktif,
										Ext.getCmp('dtpTglAwalFilterRiwayatPemberianObat').getValue(),
										Ext.getCmp('dtpTglAkhirFilterRiwayatPemberianObat').getValue(),
										Ext.getCmp('cbPosting').getValue(),
										Ext.getCmp('TxtKodePasien').getValue(),
										Ext.getCmp('TxtNamaPasien').getValue()
									);
								} 						
							}
                        }
                    },
					{
                        x: 630,
                        y: 10,
                        xtype: 'label',
                        text: 's/d '
                    },
					{
                        x: 660,
                        y: 10,
                        xtype: 'datefield',
                        id: 'dtpTglAkhirFilterRiwayatPemberianObat',
                        format: 'd/M/Y',
                        value: now,
                        width: 100,
                        listeners:
                        { 
                                'specialkey' : function()
                                {
                                        if (Ext.EventObject.getKey() === 13) //ketika di enter akan ditampilkan data sesuai inputan tanggal
                                        {
											dataGridRiwayatPemberianObat(
												kd_unit_far_aktif,
												Ext.getCmp('dtpTglAwalFilterRiwayatPemberianObat').getValue(),
												Ext.getCmp('dtpTglAkhirFilterRiwayatPemberianObat').getValue(),
												Ext.getCmp('cbPosting').getValue(),
												Ext.getCmp('TxtKodePasien').getValue(),
												Ext.getCmp('TxtNamaPasien').getValue()
											);
                                        } 						
                                }
                        }
                    },
					{
						x: 370,
						y: 70,
						id: 'cbPosting',
						xtype:'checkbox',
						name: 'cbPosting',
						labelSeparator: '',
						hideLabel: true,
						boxLabel: 'Posting',
						fieldLabel: 'Posting'   
					},
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'No. Medrec '
                    },
					{
                        x: 145,
                        y: 40,
                        xtype: 'label',
                        text: ':'
                    },
                    {   
                        x : 155,
                        y : 40,
                        xtype: 'textfield',
                        name: 'TxtKodePasien',
                        id: 'TxtKodePasien',
                        width: 200,
                        enableKeyEvents: true,
                        listeners:
                        { 
                                'specialkey' : function()
                                {
                                        if (Ext.EventObject.getKey() === 13) //ketika nama pasien di enter, maka akan ditampilkan data sesuai pencarian
                                        {
											var tmpNoIIMedrec = Ext.get('TxtKodePasien').getValue();
											if (tmpNoIIMedrec.length !== 0 && tmpNoIIMedrec.length < 10)
											{
												var tmpgetNoIIMedrec = formatnomedrec(Ext.get('TxtKodePasien').getValue())
												Ext.getCmp('TxtKodePasien').setValue(tmpgetNoIIMedrec);
												dataGridRiwayatPemberianObat(
													kd_unit_far_aktif,
													Ext.getCmp('dtpTglAwalFilterRiwayatPemberianObat').getValue(),
													Ext.getCmp('dtpTglAkhirFilterRiwayatPemberianObat').getValue(),
													Ext.getCmp('cbPosting').getValue(),
													tmpgetNoIIMedrec,
													Ext.getCmp('TxtNamaPasien').getValue()
												);
											} else{
												if (tmpNoIIMedrec.length === 10)
												{
													dataGridRiwayatPemberianObat(
														kd_unit_far_aktif,
														Ext.getCmp('dtpTglAwalFilterRiwayatPemberianObat').getValue(),
														Ext.getCmp('dtpTglAkhirFilterRiwayatPemberianObat').getValue(),
														Ext.getCmp('cbPosting').getValue(),
														tmpNoIIMedrec,
														Ext.getCmp('TxtNamaPasien').getValue()
													);
												}												
											}
											
                                        } 						
                                }
                        }
                    },
					{
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Nama '
                    },
					{
                        x: 145,
                        y: 70,
                        xtype: 'label',
                        text: ':'
                    },
                    {   
                        x : 155,
                        y : 70,
                        xtype: 'textfield',
                        name: 'TxtNamaPasien',
                        id: 'TxtNamaPasien',
                        width: 200,
                        enableKeyEvents: true,
                        listeners:
                        { 
                                'specialkey' : function()
                                {
                                        if (Ext.EventObject.getKey() === 13) //ketika nama pasien di enter, maka akan ditampilkan data sesuai pencarian
                                        {
											dataGridRiwayatPemberianObat(
												kd_unit_far_aktif,
												Ext.getCmp('dtpTglAwalFilterRiwayatPemberianObat').getValue(),
												Ext.getCmp('dtpTglAkhirFilterRiwayatPemberianObat').getValue(),
												Ext.getCmp('cbPosting').getValue(),
												Ext.getCmp('TxtKodePasien').getValue(),
												Ext.getCmp('TxtNamaPasien').getValue()
											);
                                        } 						
                                }
                        }
                    },
					{
                        x: 370,
                        y: 40,
                        xtype: 'label',
                        text: 'Alamat '
                    },
					{
                        x: 500,
                        y: 40,
                        xtype: 'label',
                        text: ':'
                    },
                    {
                        x: 510,
                        y: 40,
                         xtype: 'textfield',
                        name: 'TxtAlamatPasien',
                        id: 'TxtAlamatPasien',
                        width: 250,
                        enableKeyEvents: true,
                    },
					{
                        x: 510,
                        y: 70,
                        xtype:'button',
                        text: 'Refresh',
                        iconCls: 'refresh',
                        anchor: '25%',
                        width: 70,
                        height: 20,
                        hideLabel: false,
                        handler: function(sm, row, rec)
                        {
							dsTRRiwayatPemberianObatList.removeAll();
							dataGridRiwayatPemberianObat(
								kd_unit_far_aktif,
								Ext.getCmp('dtpTglAwalFilterRiwayatPemberianObat').getValue(),
								Ext.getCmp('dtpTglAkhirFilterRiwayatPemberianObat').getValue(),
								Ext.getCmp('cbPosting').getValue(),
								Ext.getCmp('TxtKodePasien').getValue(),
								Ext.getCmp('TxtNamaPasien').getValue()
							);
                        }
                    } 
                ]
            },
        ]
    }
    return items;
};
function comboUnitFar(){
	var Field = ['kd_unit_far','nm_unit_far'];

    dsUnitFar = new WebApp.DataStore({ fields: Field });
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/functionRiwayatPemberianObat/getUnitFar",
		params: {text:''},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cboUnitFar.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsUnitFar.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsUnitFar.add(recs);
				console.log(o);
			}
		}
	});

	cboUnitFar = new Ext.form.ComboBox
	(
		{
			id:'cboUnitFar',
			x: 155,
			y: 10,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			tabIndex:3,
			selectOnFocus:true,
			forceSelection: true,
			fieldLabel: 'Unit HD ',
			width:200,
			editable: false,
			store: dsUnitFar,
			valueField: 'kd_unit_far',
			displayField: 'nm_unit_far',
			value:nm_unit_far_aktif,
			listeners:
			{
				'select': function(a,b,c)
				{
					kd_unit_far_aktif = b.data.kd_unit_far;
				},

			}
		}
	);
	return cboUnitFar;
}
//fungsi untuk menampilkan datagrid hd kunjungan
function dataGridRiwayatPemberianObat(kd_unit_far,tgl_awal,tgl_akhir,posting,kd_pasien,nama){
	// alert(Ext.getCmp('cboUnitFar').getValue());
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionRiwayatPemberianObat/getGridRiwayatPemberianObat",
			params: { 
				kd_unit_far		: kd_unit_far , 
				kd_pasien		: kd_pasien, 
				nama			: nama, 
				posting			: posting, 
				tgl_awal		: tgl_awal, 
				tgl_akhir		: tgl_akhir
				
			},
			failure: function(o)
			{
				ShowPesanErrorRPO('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{

				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dsTRRiwayatPemberianObatList.removeAll();
					var recs=[],
						recType=dsTRRiwayatPemberianObatList.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						recs.push(new recType(cst.listData[i]));
					}
					dsTRRiwayatPemberianObatList.add(recs);
					grListRiwayatPemberianObat.getView().refresh();
				}
				else 
				{
					ShowPesanErrorRPO('Gagal membaca data ', 'Error');
				};
			}
		}
	)  
}
//fungsi untuk menampilkan datagrid hd pradialisa
function dataDetailResep(NoOut,TglOut){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionRiwayatPemberianObat/dataDetailResep", // memanggil query 
			params: { //parameter 
						NoOut:NoOut,
						TglOut:TglOut
					},
			failure: function(o)
			{
				ShowPesanErrorRPO('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dsTRDetailRiwayatPemberianObat_pradialisa.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						recs.push(new recType(cst.listData[i]));
					}
					dsTRDetailRiwayatPemberianObat_pradialisa.add(recs); //data ditampung ke datastore
					gridDTRiwayatPemberianObat_pradialisa.getView().refresh(); //untuk menampilkan data ke grid
				}
				else 
				{
					ShowPesanErrorRPO('Gagal membaca data obat', 'Error');
				};
			}
		}
	)
}

function getcurrentUnitFar(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionRiwayatPemberianObat/getcurrentUnitFar", // memanggil query 
			params: { text:''},
			failure: function(o)
			{
				ShowPesanErrorRPO('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					kd_unit_far_aktif = cst.listData[0].kd_unit_far;
					nm_unit_far_aktif = cst.listData[0].nm_unit_far;
					Ext.getCmp('cboUnitFar').setValue(nm_unit_far_aktif);
				}
				else 
				{
					ShowPesanErrorRPO('Gagal membaca data obat', 'Error');
				};
			}
		}
	)
}

function ShowPesanWarningRPO(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};
function ShowPesanInfoRPO(str, modul) {
  Ext.MessageBox.show({
	    title: modul,
	    msg: str,
	    buttons: Ext.MessageBox.OK,
	    icon: Ext.MessageBox.INFO,
		width:250
	});
};

function ShowPesanErrorRPO(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};
