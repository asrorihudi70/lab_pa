var dataSource_viSetupObatAlkes;
var selectCount_viSetupObatAlkes=50;
var NamaForm_viSetupObatAlkes="Setup Obat/Alat Kesehatan";
var selectCountStatusPostingSetupObatAlkes='Semua';
var mod_name_viSetupObatAlkes="viSetupObatAlkes";
var now_viSetupObatAlkes= new Date();
var addNew_viSetupObatAlkes;
var rowSelected_viSetupObatAlkes;
var setLookUps_viSetupObatAlkes;
var tanggal = now_viSetupObatAlkes.format("d/M/Y");
var jam = now_viSetupObatAlkes.format("H/i/s");
var tmpkriteria;
var DataGridJenisObat;
var DataGridSubJenisObat;
var DataGridJenisTerapiObat;
var DataGridSatuanBesar;
var DataGridSatuan;
var DataGridGolongan;
var kriteriajenis;


var CurrentData_viSetupObatAlkes =
{
	data: Object,
	details: Array,
	row: 0
};


var SetupObatAlkes={};
SetupObatAlkes.form={};
SetupObatAlkes.func={};
SetupObatAlkes.vars={};
SetupObatAlkes.func.parent=SetupObatAlkes;
SetupObatAlkes.form.ArrayStore={};
SetupObatAlkes.form.ComboBox={};
SetupObatAlkes.form.DataStore={};
SetupObatAlkes.form.Record={};
SetupObatAlkes.form.Form={};
SetupObatAlkes.form.Grid={};
SetupObatAlkes.form.Panel={};
SetupObatAlkes.form.TextField={};
SetupObatAlkes.form.Button={};

SetupObatAlkes.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_milik', 'milik'],
	data: []
});
SetupObatAlkes.form.ArrayStore.subJenis= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_sub_jns', 'sub_jenis'],
	data: []
});
SetupObatAlkes.form.ArrayStore.jenisTerapi= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_jns_terapi', 'jenis_terapi'],
	data: []
});

SetupObatAlkes.form.ArrayStore.golongan= new Ext.data.ArrayStore({
	id: 0,
	fields:['apt_kd_golongan', 'apt_golongan'],
	data: []
});

SetupObatAlkes.form.ArrayStore.satuan= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_satuan', 'satuan'],
	data: []
});

SetupObatAlkes.form.ArrayStore.satuanBesar= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_sat_besar', 'keterangan'],
	data: []
});


CurrentPage.page = dataGrid_viSetupObatAlkes(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
loadDataComboJenisObat_SetupObatAlkes();
loadGridJenisObat();

function dataGrid_viSetupObatAlkes(mod_id_viSetupObatAlkes){	
 	var PanelTabSetupObatAlkes = new Ext.FormPanel({
        labelAlign: 'top',
		layout:'form',
        frame:true,
        title: '',
        bodyStyle:'padding:5px 5px 0',
        //width: 600,
        items: [getPenelItemSetup_viSetupObatAlkes()]	
		
	})
	// Kriteria filter pada Grid # --------------
    var FrmData_viSetupObatAlkes = new Ext.Panel
    (
		{
			title: NamaForm_viSetupObatAlkes,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viSetupObatAlkes,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ PanelTabSetupObatAlkes],
					//GridDataView_viSetupObatAlkes],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viSetupObatAlkes,
		            columns: 21,
		            defaults: {
					scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_viSetupObatAlkes;
    //-------------- # End form filter # --------------
}

function getPenelItemSetup_viSetupObatAlkes(lebar)
{
    var items =
	{
		xtype:'tabpanel',
		plain:true,
		activeTab: 0,
		height:600,
		defaults: {
			bodyStyle:'padding:5px',
			autoScroll: false
	    },
	    items:[
				PanelJenisObat(),
				PanelSubJenisObat(),
				PanelJenisTerapiObat(),
				PanelGolonganObat(),
				PanelSatuan(),
				PanelSatuanBesar()			
		],
		listeners: {
			'tabchange': function(panel, tab) {
					var a = panel.getActiveTab();
					var idx = panel.items.indexOf(a);
					if ( idx == 0){
						shortcut.remove('save_subjenisobat');
						shortcut.remove('save_golonganobat');
						shortcut.remove('save_satuan');
						shortcut.remove('save_satuanbesar');
						shortcut.set({
							code:'save_jenisobat',
							list:[
								{
									key:'ctrl+s',
									fn:function(){
										Ext.getCmp('btnSimpanJenis_viSetupObatAlkes').el.dom.click();
									}
								},
								{
									key:'ctrl+d',
									fn:function(){
										Ext.getCmp('btnHapusJenis_viSetupObatAlkes').el.dom.click();
									}
								}
							]
						});
						
					}
					if ( idx == 1){
						shortcut.remove('save_jenisobat');
						shortcut.remove('save_golonganobat');
						shortcut.remove('save_satuan');
						shortcut.remove('save_satuanbesar');
						shortcut.set({
							code:'save_subjenisobat',
							list:[
								{
									key:'ctrl+s',
									fn:function(){
										Ext.getCmp('btnSimpanSubJenis_viSetupObatAlkes').el.dom.click();
									}
								},
								{
									key:'ctrl+d',
									fn:function(){
										Ext.getCmp('btnHapusSubJenis_viSetupObatAlkes').el.dom.click();
									}
								}
							]
						});
					}
					if ( idx == 2){
						shortcut.remove('save_subjenisobat');
						shortcut.remove('save_jenisobat');
						shortcut.remove('save_satuan');
						shortcut.remove('save_satuanbesar');
						shortcut.set({
							code:'save_golonganobat',
							list:[
								{
									key:'ctrl+s',
									fn:function(){
										Ext.getCmp('btnSimpanGolongan_viSetupObatAlkes').el.dom.click();
									}
								},
								{
									key:'ctrl+d',
									fn:function(){
										Ext.getCmp('btnHapusGolongan_viSetupObatAlkes').el.dom.click();
									}
								}
							]
						});
					}
					if ( idx == 3){
						shortcut.remove('save_subjenisobat');
						shortcut.remove('save_golonganobat');
						shortcut.remove('save_jenisobat');
						shortcut.remove('save_satuanbesar');
						shortcut.set({
							code:'save_satuan',
							list:[
								{
									key:'ctrl+s',
									fn:function(){
										Ext.getCmp('btnSimpanSatuan_viSetupObatAlkes').el.dom.click();
									}
								},
								{
									key:'ctrl+d',
									fn:function(){
										Ext.getCmp('btnHapusSatuan_viSetupObatAlkes').el.dom.click();
									}
								}
							]
						});
						
					}
					if ( idx == 4){
						shortcut.remove('save_subjenisobat');
						shortcut.remove('save_golonganobat');
						shortcut.remove('save_satuan');
						shortcut.remove('save_jenisobat');
						shortcut.set({
							code:'save_satuanbesar',
							list:[
								{
									key:'ctrl+s',
									fn:function(){
										Ext.getCmp('btnSimpanSatuanBesar_viSetupObatAlkes').el.dom.click();
									}
								},
								{
									key:'ctrl+d',
									fn:function(){
										Ext.getCmp('btnHapusSatuanBesar_viSetupObatAlkes').el.dom.click();
									}
								}
							]
						});
						
					}
			}
		}
	}
    return items;
};

//sub form tab1
function PanelJenisObat(rowdata){
	var items = 
	{
		title:'Jenis Obat',
		layout:'form',
		border: false,
		items:
		[
			{
				xtype: 'fieldset',
				items: [
					{
						layout: 'column',
						bodyStyle:'padding: 5px',
						border: false,
						items:
						[
							{
								xtype: 'displayfield',
								//flex: 1,
								width: 100,
								name: '',
								value: 'Kode Jenis :',
								fieldLabel: 'Label'
							},
							{
								xtype: 'textfield',
								name: 'txtKdJenisObat_SetupObatAlkesL',
								id: 'txtKdJenisObat_SetupObatAlkesL',
								readOnly:true,
								tabIndex:1,
								width: 150
							}
						]
					},
					{
						layout: 'column',
						bodyStyle:'padding: 5px',
						border: false,
						items:
						[
							{
								xtype: 'displayfield',
								flex: 1,
								width: 100,
								name: '',
								value: 'Nama Jenis :',
								fieldLabel: 'Label'
							},
							SetupObatAlkes.vars.nama=new Nci.form.Combobox.autoCompleteId({
								id	: 'txtNamaJenisObat_SetupAlkesL',
								store	: SetupObatAlkes.form.ArrayStore.a,
								select	: function(a,b,c){
									
									Ext.getCmp('txtKdJenisObat_SetupObatAlkesL').setValue(b.data.kd_jns_obt);
									
									DataGridSubJenisObat.getView().refresh();
									
								},
								onShowList:function(a){
									dsDataGrdJenis_viSetupMasterObat.removeAll();
									
									var recs=[],
									recType=dsDataGrdJenis_viSetupMasterObat.recordType;
										
									for(var i=0; i<a.length; i++){
										recs.push(new recType(a[i]));
									}
									dsDataGrdJenis_viSetupMasterObat.add(recs);
									
								},
								insert	: function(o){
									return {
										kd_jns_obt      	: o.kd_jns_obt,
										nama_jenis 			: o.nama_jenis,
										text				:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_jns_obt+'</td><td width="200">'+o.nama_jenis+'</td></tr></table>'
									}
								},
								url		: baseURL + "index.php/apotek/functionSetupObatAlkes/getJenisGrid",
								valueField: 'nama_jenis',
								displayField: 'text',
								listWidth: 280
							})
							//mComboSetupJenisObat_SetupObatAlkes()
						]
					}
				]
			},
			{
				xtype: 'fieldset',
				items: [
					{
						layout: 'form',
						bodyStyle:'padding: 5px',
						border: true,
						items:
						[
							gridJenisObat_viSetupMasterObat()
						]
					}
				
				]
			}
		],
		tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAddJenis_viSetupObatAlkes',
						handler: function(){
							dataaddnewJenis_viSetupObatAlkes();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpanJenis_viSetupObatAlkes',
						handler: function()
						{
							datasaveJenis_viSetupObatAlkes();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnHapusJenis_viSetupObatAlkes',
						handler: function()
						{
							datahapusJenis_viSetupObatAlkes();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefreshJenis_viSetupObatAlkes',
						handler: function()
						{
							dsDataGrdJenis_viSetupMasterObat.removeAll();
							loadGridJenisObat();
						}
					},
					{
						xtype: 'tbseparator'
					}
					
				]
			}
    };
        return items;
}

//sub form tab2
function PanelSubJenisObat(rowdata){
	var items = 
	{
		title:'Sub Jenis Obat',
		layout:'form',
		border: false,
		items:
		[
			{
				xtype: 'fieldset',
				items: [
					{
						layout: 'column',
						bodyStyle:'padding: 5px',
						border: false,
						items:
						[
							{
								xtype: 'displayfield',
								//flex: 1,
								width: 100,
								name: '',
								value: 'Kode Sub Jenis :',
								fieldLabel: 'Label'
							},
							{
								xtype: 'textfield',
								name: 'txtKdSubJenisObat_SetupObatAlkesL',
								id: 'txtKdSubJenisObat_SetupObatAlkesL',
								readOnly:true,
								tabIndex:1,
								width: 150
							}
						]
					},
					{
						layout: 'column',
						bodyStyle:'padding: 5px',
						border: false,
						items:
						[
							{
								xtype: 'displayfield',
								flex: 1,
								width: 100,
								name: '',
								value: 'Sub Jenis :',
								fieldLabel: 'Label'
							},
							SetupObatAlkes.vars.subJenis=new Nci.form.Combobox.autoCompleteId({
								id: 'txtSubJenis_SetupObatAlkesL',
								store	: SetupObatAlkes.form.ArrayStore.subJenis,
								select	: function(a,b,c){
									
									Ext.getCmp('txtKdSubJenisObat_SetupObatAlkesL').setValue(b.data.kd_sub_jns);
									
									DataGridSubJenisObat.getView().refresh();
									
								},
								onShowList:function(a){
									dsDataGrdSubJenis_viSetupMasterObat.removeAll();
									
									var recs=[],
									recType=dsDataGrdSubJenis_viSetupMasterObat.recordType;
										
									for(var i=0; i<a.length; i++){
										recs.push(new recType(a[i]));
									}
									dsDataGrdSubJenis_viSetupMasterObat.add(recs);
									
								},
								insert	: function(o){
									return {
										kd_sub_jns      	: o.kd_sub_jns,
										sub_jenis 			: o.sub_jenis,
										text				:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_sub_jns+'</td><td width="200">'+o.sub_jenis+'</td></tr></table>'
									}
								},
								url		: baseURL + "index.php/apotek/functionSetupObatAlkes/getSubJenisGrid",
								valueField: 'sub_jenis',
								displayField: 'text',
								listWidth: 280
							})
							/* {
								xtype: 'textfield',
								name: 'txtSubJenis_SetupObatAlkesL',
								id: 'txtSubJenis_SetupObatAlkesL',
								tabIndex:2,
								width: 180
							} */
						]
					}
				]
			},
			{
				xtype: 'fieldset',
				items: [
					{
						layout: 'form',
						bodyStyle:'padding: 5px',
						border: true,
						items:
						[
							gridSubJenisObat_viSetupMasterObat()
						]
					}
				]
			}
		],
		tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAddSubJenis_viSetupObatAlkes',
						handler: function(){
							dataaddnewSubJenis_viSetupObatAlkes();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpanSubJenis_viSetupObatAlkes',
						handler: function()
						{
							datasaveSubJenis_viSetupObatAlkes();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnHapusSubJenis_viSetupObatAlkes',
						handler: function()
						{
							dataHapusSubJenis_viSetupObatAlkes();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefreshSubJenis_viSetupObatAlkes',
						handler: function()
						{
							dsDataGrdSubJenis_viSetupMasterObat.reload();
						}
					},
					{
						xtype: 'tbseparator'
					}
					
				]
			}
    };
        return items;
}

function PanelJenisTerapiObat(rowdata){
	var items = 
	{
		title:'Jenis Terapi Obat',
		layout:'form',
		border: false,
		items:
		[
			{
				xtype: 'fieldset',
				items: [
					{
						layout: 'column',
						bodyStyle:'padding: 5px',
						border: false,
						items:
						[
							{
								xtype: 'displayfield',
								//flex: 1,
								width: 100,
								name: '',
								value: 'Kode Jenis Terapi :',
								fieldLabel: 'Label'
							},
							{
								xtype: 'textfield',
								name: 'txtKdJenisTerapiObat_SetupObatAlkesL',
								id: 'txtKdJenisTerapiObat_SetupObatAlkesL',
								readOnly:true,
								tabIndex:1,
								width: 150
							}
						]
					},
					{
						layout: 'column',
						bodyStyle:'padding: 5px',
						border: false,
						items:
						[
							{
								xtype: 'displayfield',
								flex: 1,
								width: 100,
								name: '',
								value: 'Jenis Terapi :',
								fieldLabel: 'Label'
							},
							SetupObatAlkes.vars.jenisTerapi=new Nci.form.Combobox.autoCompleteId({
								id: 'txtJenisTerapi_SetupObatAlkesL',
								store	: SetupObatAlkes.form.ArrayStore.jenisTerapi,
								select	: function(a,b,c){
									
									Ext.getCmp('txtKdJenisTerapiObat_SetupObatAlkesL').setValue(b.data.kd_jns_terapi);
									
									DataGridJenisTerapiObat.getView().refresh();
									
								},
								onShowList:function(a){
									dsDataGrdJenisTerapi_viSetupMasterObat.removeAll();
									
									var recs=[],
									recType=dsDataGrdJenisTerapi_viSetupMasterObat.recordType;
										
									for(var i=0; i<a.length; i++){
										recs.push(new recType(a[i]));
									}
									dsDataGrdJenisTerapi_viSetupMasterObat.add(recs);
									
								},
								insert	: function(o){
									return {
										kd_jns_terapi      	: o.kd_jns_terapi,
										jenis_terapi 		: o.jenis_terapi,
										text				:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_jns_terapi+'</td><td width="200">'+o.jenis_terapi+'</td></tr></table>'
									}
								},
								url		: baseURL + "index.php/apotek/functionSetupObatAlkes/getJenisTerapiGrid",
								valueField: 'jenis_terapi',
								displayField: 'text',
								listWidth: 280
							})
							/* {
								xtype: 'textfield',
								name: 'txtSubJenis_SetupObatAlkesL',
								id: 'txtSubJenis_SetupObatAlkesL',
								tabIndex:2,
								width: 180
							} */
						]
					}
				]
			},
			{
				xtype: 'fieldset',
				items: [
					{
						layout: 'form',
						bodyStyle:'padding: 5px',
						border: true,
						items:
						[
							gridJenisTerapiObat_viSetupMasterObat()
						]
					}
				]
			}
		],
		tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAddJenisTerapi_viSetupObatAlkes',
						handler: function(){
							dataaddnewJenisTerapi_viSetupObatAlkes();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpanJenisTerapi_viSetupObatAlkes',
						handler: function()
						{
							datasaveJenisTerapi_viSetupObatAlkes();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnHapusJenisTerapi_viSetupObatAlkes',
						handler: function()
						{
							dataHapusJenisTerapi_viSetupObatAlkes();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefreshJenisTerapi_viSetupObatAlkes',
						handler: function()
						{
							dsDataGrdJenisTerapi_viSetupMasterObat.reload();
						}
					},
					{
						xtype: 'tbseparator'
					}
					
				]
			}
    };
        return items;
}

//sub form tab3
function PanelGolonganObat(rowdata){
	var items = 
	{
		title:'Golongan Obat',
		layout:'form',
		border: false,
		items:
		[
			{
				xtype: 'fieldset',
				items: [
					{
						layout: 'column',
						bodyStyle:'padding: 5px',
						border: false,
						items:
						[
							{
								xtype: 'displayfield',
								//flex: 1,
								width: 100,
								name: '',
								value: 'Kode Golongan :',
								fieldLabel: 'Label'
							},
							{
								xtype: 'textfield',
								name: 'txtKdGolObat_SetupObatAlkesL',
								id: 'txtKdGolObat_SetupObatAlkesL',
								readOnly:true,
								tabIndex:1,
								width: 150
							}
						]
					},
					{
						layout: 'column',
						bodyStyle:'padding: 5px',
						border: false,
						items:
						[
							{
								xtype: 'displayfield',
								flex: 1,
								width: 100,
								name: '',
								value: 'Golongan :',
								fieldLabel: 'Label'
							},
							SetupObatAlkes.vars.golongan=new Nci.form.Combobox.autoCompleteId({
								id		: 'txtGolongan_SetupObatAlkesL',
								store	: SetupObatAlkes.form.ArrayStore.golongan,
								select	: function(a,b,c){
									
									Ext.getCmp('txtKdGolObat_SetupObatAlkesL').setValue(b.data.apt_kd_golongan);
									
									DataGridGolongan.getView().refresh();
									
								},
								onShowList:function(a){
									dsDataGrdGolongan_viSetupMasterObat.removeAll();
									
									var recs=[],
									recType=dsDataGrdGolongan_viSetupMasterObat.recordType;
										
									for(var i=0; i<a.length; i++){
										recs.push(new recType(a[i]));
									}
									dsDataGrdGolongan_viSetupMasterObat.add(recs);
									
								},
								insert	: function(o){
									return {
										apt_kd_golongan      	: o.apt_kd_golongan,
										apt_golongan 			: o.apt_golongan,
										text				:  '<table style="font-size: 11px;"><tr><td width="70">'+o.apt_kd_golongan+'</td><td width="200">'+o.apt_golongan+'</td></tr></table>'
									}
								},
								url		: baseURL + "index.php/apotek/functionSetupObatAlkes/getGolonganGrid",
								valueField: 'apt_golongan',
								displayField: 'text',
								listWidth: 280
							})
							/* {
								xtype: 'textfield',
								name: 'txtGolongan_SetupObatAlkesL',
								id: 'txtGolongan_SetupObatAlkesL',
								tabIndex:2,
								width: 180
							} */
						]
					}
				]
			},
			{
				xtype: 'fieldset',
				items: [
					{
						layout: 'form',
						bodyStyle:'padding: 5px',
						border: true,
						items:
						[
							gridGolonganObat_viSetupMasterObat()
						]
					}
				]
			}
		],
		tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAddGolongan_viSetupObatAlkes',
						handler: function(){
							dataaddnewGolongan_viSetupObatAlkes();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpanGolongan_viSetupObatAlkes',
						handler: function()
						{
							datasaveGolongan_viSetupObatAlkes();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnHapusGolongan_viSetupObatAlkes',
						handler: function()
						{
							datahapusGolongan_viSetupObatAlkes();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefreshGolongan_viSetupObatAlkes',
						handler: function()
						{
							dsDataGrdGolongan_viSetupMasterObat.reload();
						}
					},
					{
						xtype: 'tbseparator'
					}
					
				]
			}
    };
        return items;
}

//sub form tab4
function PanelSatuan(rowdata)
{
	var items = 
	{
		title:'Satuan Kecil',
		layout:'form',
		border: false,
		items:
		[
			{
				xtype: 'fieldset',
				items: [
					{
						layout: 'column',
						bodyStyle:'padding: 5px',
						border: false,
						items:
						[
							{
								xtype: 'displayfield',
								//flex: 1,
								width: 100,
								name: '',
								value: 'Kode Satuan :',
								fieldLabel: 'Label'
							},
							{
								xtype: 'textfield',
								name: 'txtKdSatuan_SetupObatAlkesL',
								id: 'txtKdSatuan_SetupObatAlkesL',
								//readOnly:true,
								tabIndex:1,
								width: 150,
								allowBlank: false,
								maxLength:10,
								listeners:
								{ 
									blur: function(a){
										if(a.getValue().length > 10){
											ShowPesanWarningSetupObatAlkes('Kode satuan kecil tidak boleh lebih dari 10 huruf', 'Warning');
										}
									}
								}
							}
						]
					},
					{
						layout: 'column',
						bodyStyle:'padding: 5px',
						border: false,
						items:
						[
							{
								xtype: 'displayfield',
								flex: 1,
								width: 100,
								name: '',
								value: 'Satuan :',
								fieldLabel: 'Label'
							},
							SetupObatAlkes.vars.satuan=new Nci.form.Combobox.autoCompleteId({
								id		: 'txtSatuan_SetupObatAlkesL',
								store	: SetupObatAlkes.form.ArrayStore.satuan,
								select	: function(a,b,c){
									
									Ext.getCmp('txtKdSatuan_SetupObatAlkesL').setValue(b.data.kd_satuan);
									
									DataGridSatuan.getView().refresh();
									
								},
								onShowList:function(a){
									dsDataGrdSatuan_viSetupMasterObat.removeAll();
									
									var recs=[],
									recType=dsDataGrdSatuan_viSetupMasterObat.recordType;
										
									for(var i=0; i<a.length; i++){
										recs.push(new recType(a[i]));
									}
									dsDataGrdSatuan_viSetupMasterObat.add(recs);
									
								},
								insert	: function(o){
									return {
										kd_satuan      	: o.kd_satuan,
										satuan 			: o.satuan,
										text			:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_satuan+'</td><td width="200">'+o.satuan+'</td></tr></table>'
									}
								},
								url		: baseURL + "index.php/apotek/functionSetupObatAlkes/getSatuanGrid",
								valueField: 'satuan',
								displayField: 'text',
								listWidth: 280
							})
							/* {
								xtype: 'textfield',
								name: 'txtSatuan_SetupObatAlkesL',
								id: 'txtSatuan_SetupObatAlkesL',
								allowBlank: false,
								tabIndex:2,
								width: 180
							} */
						]
					}
				]
			},
			{
				xtype: 'fieldset',
				items: [
					{
						layout: 'form',
						bodyStyle:'padding: 5px',
						border: true,
						items:
						[
							gridSatuan_viSetupMasterObat()
						]
					}
				]
			}
		],
		tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAddSatuan_viSetupObatAlkes',
						handler: function(){
							dataaddnewSatuan_viSetupObatAlkes();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpanSatuan_viSetupObatAlkes',
						handler: function()
						{
							datasaveSatuan_viSetupObatAlkes();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnHapusSatuan_viSetupObatAlkes',
						handler: function()
						{
							datahapusSatuan_viSetupObatAlkes();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefreshSatuan_viSetupObatAlkes',
						handler: function()
						{
							dsDataGrdSatuan_viSetupMasterObat.reload();
						}
					},
					{
						xtype: 'tbseparator'
					}
					
				]
			}
    };
        return items;
}

//sub form tab5
function PanelSatuanBesar(rowdata)
{
	var items = 
	{
		title:'Satuan Besar',
		layout:'form',
		border: false,
		items:
		[
			{
				xtype: 'fieldset',
				items: [
					{
						layout: 'column',
						bodyStyle:'padding: 5px',
						border: false,
						items:
						[
							{
								xtype: 'displayfield',
								//flex: 1,
								width: 100,
								name: '',
								value: 'Kode Satuan :',
								fieldLabel: 'Label'
							},
							{
								xtype: 'textfield',
								name: 'txtKdSatuanBesar_SetupObatAlkesL',
								id: 'txtKdSatuanBesar_SetupObatAlkesL',
								//readOnly:true,
								tabIndex:1,
								width: 150,
								allowBlank: false,
								maxLength:10,
								listeners:
								{ 
									blur: function(a){
										if(a.getValue().length > 10){
											ShowPesanWarningSetupObatAlkes('Kode satuan besar tidak boleh lebih dari 10 huruf', 'Warning');
										}
									}
								}
							}
						]
					},
					{
						layout: 'column',
						bodyStyle:'padding: 5px',
						border: false,
						items:
						[
							{
								xtype: 'displayfield',
								flex: 1,
								width: 100,
								name: '',
								value: 'Keterangan :',
								fieldLabel: 'Label'
							},
							SetupObatAlkes.vars.satuanBesar=new Nci.form.Combobox.autoCompleteId({
								id		: 'txtKeterangan_SetupObatAlkesL',
								store	: SetupObatAlkes.form.ArrayStore.satuanBesar,
								select	: function(a,b,c){
									
									Ext.getCmp('txtKdSatuanBesar_SetupObatAlkesL').setValue(b.data.kd_sat_besar);
									
									DataGridSatuanBesar.getView().refresh();
									
								},
								onShowList:function(a){
									dsDataGrdSatuanBesar_viSetupMasterObat.removeAll();
									
									var recs=[],
									recType=dsDataGrdSatuanBesar_viSetupMasterObat.recordType;
										
									for(var i=0; i<a.length; i++){
										recs.push(new recType(a[i]));
									}
									dsDataGrdSatuanBesar_viSetupMasterObat.add(recs);
									
								},
								insert	: function(o){
									return {
										kd_sat_besar      	: o.kd_sat_besar,
										keterangan 			: o.keterangan,
										text			:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_sat_besar+'</td><td width="200">'+o.keterangan+'</td></tr></table>'
									}
								},
								url		: baseURL + "index.php/apotek/functionSetupObatAlkes/getSatuanBesarGrid",
								valueField: 'keterangan',
								displayField: 'text',
								listWidth: 280
							})
							/* {
								xtype: 'textfield',
								name: 'txtKeterangan_SetupObatAlkesL',
								id: 'txtKeterangan_SetupObatAlkesL',
								allowBlank: false,
								tabIndex:2,
								width: 180
							} */
						]
					}
				]
			},
			{
				xtype: 'fieldset',
				items: [
					{
						layout: 'form',
						bodyStyle:'padding: 5px',
						border: true,
						items:
						[
							gridSatuanBesar_viSetupMasterObat()
						]
					}
				]
			}
		],
		tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAddSatuanBesar_viSetupObatAlkes',
						handler: function(){
							dataaddnewSatuanBesar_viSetupObatAlkes();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpanSatuanBesar_viSetupObatAlkes',
						handler: function()
						{
							datasaveSatuanBesar_viSetupObatAlkes();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnHapusSatuanBesar_viSetupObatAlkes',
						handler: function()
						{
							datahapusSatuanBesar_viSetupObatAlkes();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefreshSatuanBesar_viSetupObatAlkes',
						handler: function()
						{
							dsDataGrdSatuanBesar_viSetupMasterObat.reload();
						}
					},
					{
						xtype: 'tbseparator'
					}
					
				]
			}
    };
        return items;
}
//------------------end---------------------------------------------------------

//------------------GRID JENIS OBAT---------------------------------------------------------
function loadGridJenisObat(kriteriajenis){
	dsDataGrdJenis_viSetupMasterObat.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: '',
					Sortdir: 'ASC',
					target: 'ViGridJenisObat',
					param: kriteriajenis
				}
		}
	); 

	/* Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionSetupObatAlkes/getJenisGrid",
			params: {text:kriteriajenis},
			failure: function(o)
			{
				ShowPesanErrorGzVendor('Error get data Jenis Obat! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dsDataGrdJenis_viSetupMasterObat.removeAll();
					var recs=[],
						recType=dsDataGrdJenis_viSetupMasterObat.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dsDataGrdJenis_viSetupMasterObat.add(recs);
					
					
					
					DataGridJenisObat.getView().refresh();
				}
				else 
				{
					ShowPesanErrorGzVendor('Gagal membaca data', 'Error');
				};
			}
		}
		
	) */
	
}

function gridJenisObat_viSetupMasterObat(){
    var FieldGrdJenis_viSetupMasterObat = ['kd_jns_obt', 'nama_jenis'];
    
	dsDataGrdJenis_viSetupMasterObat= new WebApp.DataStore({
		fields: FieldGrdJenis_viSetupMasterObat
    });
    
   
    DataGridJenisObat =new Ext.grid.EditorGridPanel({
        xtype: 'editorgrid',
		title: 'List Jenis Obat',
		store: dsDataGrdJenis_viSetupMasterObat,
        height: 300,
		width:550,
        columnLines: true,
		border: true,
		flex:1,
		listeners:
		{
			// Function saat ada event double klik maka akan muncul form view # --------------
			rowdblclick: function (sm, ridx, cidx)
			{
				rowSelected_viSetupObatAlkes = dsDataGrdJenis_viSetupMasterObat.getAt(ridx);
				if (rowSelected_viSetupObatAlkes != undefined)
				{
					DataInitPanelJenisObat(rowSelected_viSetupObatAlkes.data);
				}
			}
			// End Function # --------------
		},
		selModel: new Ext.grid.CellSelectionModel ({
            singleSelect: true,
            listeners:{
                rowselect: function(sm, row, rec){
					DataInitPanelJenisObat(rowSelected_viSetupObatAlkes.data);
                }
            }
        }),
        stripeRows: true,
        cm:new Ext.grid.ColumnModel([
        	new Ext.grid.RowNumberer(),	
			{
				dataIndex: 'kd_jns_obt',
				header: 'Kode Jenis',
				sortable: true,
				width: 70
			},
			{
				dataIndex: 'nama_jenis',
				header: 'Jenis obat',
				width: 70
			}
			//-------------- ## --------------
        ]),
		viewConfig:{
			forceFit: true
		}
    });
    return DataGridJenisObat;
}

//------------------GRID SUB JENIS OBAT---------------------------------------------------------
function gridSubJenisObat_viSetupMasterObat(){
    var FieldGrdSubJenis_viSetupMasterObat = ['kd_sub_jns', 'sub_jenis'];
    
	dsDataGrdSubJenis_viSetupMasterObat= new WebApp.DataStore({
		fields: FieldGrdSubJenis_viSetupMasterObat
    });
	dsDataGrdSubJenis_viSetupMasterObat.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: '',
					Sortdir: 'ASC',
					target: 'ViGridSubJenisObat',
					param: ''
				}
		}
	);  
    
   
    DataGridSubJenisObat =new Ext.grid.EditorGridPanel({
        xtype: 'editorgrid',
		title: 'List Sub Jenis Obat',
		store: dsDataGrdSubJenis_viSetupMasterObat,
        height: 300,
		width:500,
        columnLines: true,
		border: true,
		flex:1,
		listeners:
		{
			// Function saat ada event double klik maka akan muncul form view # --------------
			rowdblclick: function (sm, ridx, cidx)
			{
				rowSelected_viSetupObatAlkes = dsDataGrdSubJenis_viSetupMasterObat.getAt(ridx);
				if (rowSelected_viSetupObatAlkes != undefined)
				{
					DataInitPanelSubJenisObat(rowSelected_viSetupObatAlkes.data);
				}
			}
			// End Function # --------------
		},
		selModel: new Ext.grid.CellSelectionModel ({
            singleSelect: true,
            listeners:{
                rowselect: function(sm, row, rec){
					DataInitPanelSubJenisObat(rowSelected_viSetupObatAlkes.data);
                }
            }
        }),
        stripeRows: true,
        cm:new Ext.grid.ColumnModel([
        	new Ext.grid.RowNumberer(),	
			{
				dataIndex: 'kd_sub_jns',
				header: 'Kode Sub Jenis',
				sortable: true,
				width: 70
			},
			{
				dataIndex: 'sub_jenis',
				header: 'Sub Jenis obat',
				width: 70
			}
			//-------------- ## --------------
        ]),
		viewConfig:{
			forceFit: true
		}
    });
    return DataGridSubJenisObat;
}

function gridJenisTerapiObat_viSetupMasterObat(){
    var FieldGrdJenisTerapi_viSetupMasterObat = ['kd_jns_terapi', 'jenis_terapi'];
    
	dsDataGrdJenisTerapi_viSetupMasterObat= new WebApp.DataStore({
		fields: FieldGrdJenisTerapi_viSetupMasterObat
    });
	dsDataGrdJenisTerapi_viSetupMasterObat.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: '',
					Sortdir: 'ASC',
					target: 'ViGridJenisTerapiObat',
					param: ''
				}
		}
	);  
    
   
    DataGridJenisTerapiObat =new Ext.grid.EditorGridPanel({
        xtype: 'editorgrid',
		title: 'List Jenis Terapi Obat',
		store: dsDataGrdJenisTerapi_viSetupMasterObat,
        height: 300,
		width:500,
        columnLines: true,
		border: true,
		flex:1,
		listeners:
		{
			// Function saat ada event double klik maka akan muncul form view # --------------
			rowdblclick: function (sm, ridx, cidx)
			{
				rowSelected_viSetupObatAlkes = dsDataGrdJenisTerapi_viSetupMasterObat.getAt(ridx);
				if (rowSelected_viSetupObatAlkes != undefined)
				{
					DataInitPanelJenisTerapiObat(rowSelected_viSetupObatAlkes.data);
				}
			}
			// End Function # --------------
		},
		selModel: new Ext.grid.CellSelectionModel ({
            singleSelect: true,
            listeners:{
                rowselect: function(sm, row, rec){
					DataInitPanelJenisTerapiObat(rowSelected_viSetupObatAlkes.data);
                }
            }
        }),
        stripeRows: true,
        cm:new Ext.grid.ColumnModel([
        	new Ext.grid.RowNumberer(),	
			{
				dataIndex: 'kd_jns_terapi',
				header: 'Kode Jenis Terapi',
				sortable: true,
				width: 70
			},
			{
				dataIndex: 'jenis_terapi',
				header: 'Jenis Terapi obat',
				width: 70
			}
			//-------------- ## --------------
        ]),
		viewConfig:{
			forceFit: true
		}
    });
    return DataGridJenisTerapiObat;
}
//------------------GRID GOLONGAN OBAT---------------------------------------------------------
function gridGolonganObat_viSetupMasterObat(){
    var FieldGrdGolongan_viSetupMasterObat = ['apt_kd_golongan', 'apt_golongan'];
    
	dsDataGrdGolongan_viSetupMasterObat= new WebApp.DataStore({
		fields: FieldGrdGolongan_viSetupMasterObat
    });
	dsDataGrdGolongan_viSetupMasterObat.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: '',
					Sortdir: 'ASC',
					target: 'ViGridGolObat',
					param: ''
				}
		}
	);  
    
   
    DataGridGolongan =new Ext.grid.EditorGridPanel({
        xtype: 'editorgrid',
		title: 'List Golongan Obat',
		store: dsDataGrdGolongan_viSetupMasterObat,
        height: 300,
		width:550,
        columnLines: true,
		border: true,
		flex:1,
		listeners:
		{
			// Function saat ada event double klik maka akan muncul form view # --------------
			rowdblclick: function (sm, ridx, cidx)
			{
				rowSelected_viSetupObatAlkes = dsDataGrdGolongan_viSetupMasterObat.getAt(ridx);
				if (rowSelected_viSetupObatAlkes != undefined)
				{
					DataInitPanelGolongan(rowSelected_viSetupObatAlkes.data);
				}
			}
			// End Function # --------------
		},
		selModel: new Ext.grid.CellSelectionModel ({
            singleSelect: true,
            listeners:{
                rowselect: function(sm, row, rec){
					DataInitPanelGolongan(rowSelected_viSetupObatAlkes.data);
                }
            }
        }),
        stripeRows: true,
        cm:new Ext.grid.ColumnModel([
        	new Ext.grid.RowNumberer(),	
			{
				dataIndex: 'apt_kd_golongan',
				header: 'Kode Golongan',
				sortable: true,
				width: 70
			},
			{
				dataIndex: 'apt_golongan',
				header: 'Golongan',
				width: 70
			}
			//-------------- ## --------------
        ]),
		viewConfig:{
			forceFit: true
		}
    });
    return DataGridGolongan;
}

//------------------GRID SATUAN OBAT---------------------------------------------------------
function gridSatuan_viSetupMasterObat(){
    var FieldGrdSatuan_viSetupMasterObat = ['kd_satuan', 'satuan'];
    
	dsDataGrdSatuan_viSetupMasterObat= new WebApp.DataStore({
		fields: FieldGrdSatuan_viSetupMasterObat
    });
	dsDataGrdSatuan_viSetupMasterObat.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: '',
					Sortdir: 'ASC',
					target: 'ViGridSatuan',
					param: ''
				}
		}
	);  
    
   
    DataGridSatuan =new Ext.grid.EditorGridPanel({
        xtype: 'editorgrid',
		title: 'List Satuan Kecil',
		store: dsDataGrdSatuan_viSetupMasterObat,
        height: 300,
		width:550,
        columnLines: true,
		border: true,
		flex:1,
		listeners:
		{
			// Function saat ada event double klik maka akan muncul form view # --------------
			rowdblclick: function (sm, ridx, cidx)
			{
				rowSelected_viSetupObatAlkes = dsDataGrdSatuan_viSetupMasterObat.getAt(ridx);
				if (rowSelected_viSetupObatAlkes != undefined)
				{
					DataInitPanelSatuan(rowSelected_viSetupObatAlkes.data);
				}
			}
			// End Function # --------------
		},
		selModel: new Ext.grid.CellSelectionModel ({
            singleSelect: true,
            listeners:{
                rowselect: function(sm, row, rec){
					DataInitPanelSatuan(rowSelected_viSetupObatAlkes.data);
                }
            }
        }),
        stripeRows: true,
        cm:new Ext.grid.ColumnModel([
        	new Ext.grid.RowNumberer(),	
			{
				dataIndex: 'kd_satuan',
				header: 'Kode Satuan',
				sortable: true,
				width: 70
			},
			{
				dataIndex: 'satuan',
				header: 'Satuan',
				width: 70
			}
			//-------------- ## --------------
        ]),
		viewConfig:{
			forceFit: true
		}
    });
    return DataGridSatuan;
}

//------------------GRID SATUAN BESAR OBAT---------------------------------------------------------
function gridSatuanBesar_viSetupMasterObat(){
    var FieldGrdSatuanBesar_viSetupMasterObat = ['kd_sat_besar', 'keterangan'];
    
	dsDataGrdSatuanBesar_viSetupMasterObat= new WebApp.DataStore({
		fields: FieldGrdSatuanBesar_viSetupMasterObat
    });
	dsDataGrdSatuanBesar_viSetupMasterObat.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: '',
					Sortdir: 'ASC',
					target: 'ViGridSatuanBesar',
					param: ''
				}
		}
	);  
    
   
    DataGridSatuanBesar =new Ext.grid.EditorGridPanel({
        xtype: 'editorgrid',
		title: 'List Satuan Besar',
		store: dsDataGrdSatuanBesar_viSetupMasterObat,
        height: 300,
		width:550,
        columnLines: true,
		border: true,
		flex:1,
		listeners:
		{
			// Function saat ada event double klik maka akan muncul form view # --------------
			rowdblclick: function (sm, ridx, cidx)
			{
				rowSelected_viSetupObatAlkes = dsDataGrdSatuanBesar_viSetupMasterObat.getAt(ridx);
				if (rowSelected_viSetupObatAlkes != undefined)
				{
					DataInitPanelSatuanBesar(rowSelected_viSetupObatAlkes.data);
				}
			}
			// End Function # --------------
		},
		selModel: new Ext.grid.CellSelectionModel ({
            singleSelect: true,
            listeners:{
                rowselect: function(sm, row, rec){
					DataInitPanelSatuanBesar(rowSelected_viSetupObatAlkes.data);
                }
            }
        }),
        stripeRows: true,
        cm:new Ext.grid.ColumnModel([
        	new Ext.grid.RowNumberer(),	
			{
				dataIndex: 'kd_sat_besar',
				header: 'Kode Satuan',
				sortable: true,
				width: 70
			},
			{
				dataIndex: 'keterangan',
				header: 'Keterangan',
				width: 70
			}
			//-------------- ## --------------
        ]),
		viewConfig:{
			forceFit: true
		}
    });
    return DataGridSatuanBesar;
}


//------------------------------Jenis Obat----------------------------------------------
function DataInitPanelJenisObat(rowdata){
	Ext.getCmp('txtKdJenisObat_SetupObatAlkesL').setValue(rowdata.kd_jns_obt);
	Ext.getCmp('txtNamaJenisObat_SetupAlkesL').setValue(rowdata.nama_jenis);
};

function dataaddnewJenis_viSetupObatAlkes(){
	Ext.getCmp('txtKdJenisObat_SetupObatAlkesL').setValue('');
	Ext.getCmp('txtNamaJenisObat_SetupAlkesL').setValue('');
}

function datasaveJenis_viSetupObatAlkes(){
	if (ValidasiEntryJenisObat(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionSetupObatAlkes/saveJenis",
				params: getParamSetupJenisObat(),
				failure: function(o)
				{
					ShowPesanErrorSetupObatAlkes('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoSetupObatAlkes(nmPesanSimpanSukses,nmHeaderSimpanData);
						Ext.get('txtKdJenisObat_SetupObatAlkesL').dom.value=cst.kdjenis;
						dsDataGrdJenis_viSetupMasterObat.removeAll();
						loadGridJenisObat();
					}
					else 
					{
						ShowPesanErrorSetupObatAlkes('Gagal Menyimpan Data ini', 'Error');
						dsDataGrdJenis_viSetupMasterObat.removeAll();
						loadGridJenisObat();
					};
				}
			}
			
		)
	}
}
function datahapusJenis_viSetupObatAlkes(){
	if (ValidasiEntryJenisObat(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionSetupObatAlkes/deleteJenis",
				params: {
					kd_jns_obt:Ext.getCmp('txtKdJenisObat_SetupObatAlkesL').getValue()
				},
				failure: function(o)
				{
					ShowPesanErrorSetupObatAlkes('Data sudah digunakan dalam data obat, data ini tidak dapat di hapus! Hubungi Admin', 'Error');
					loadMask.hide();
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoSetupObatAlkes('Data berhasil dihapus','Informasi');
						dataaddnewJenis_viSetupObatAlkes();
						dsDataGrdJenis_viSetupMasterObat.removeAll();
						loadGridJenisObat();
					}
					else 
					{
						ShowPesanErrorSetupObatAlkes('Data gagal di hapus', 'Error');
						dsDataGrdJenis_viSetupMasterObat.removeAll();
						loadGridJenisObat();
					};
				}
			}
			
		)
	}
	//alert(Ext.getCmp('cboJenisObat_SetupObatAlkes').getValue())
}

function getParamSetupJenisObat(){
	var	params =
	{
		KdJnsObt:Ext.getCmp('txtKdJenisObat_SetupObatAlkesL').getValue(),
		NamaJenis:Ext.get('txtNamaJenisObat_SetupAlkesL').getValue()
	}
   
    return params
};

function ValidasiEntryJenisObat(modul,mBolHapus){
	var x = 1;
	if(Ext.getCmp('txtNamaJenisObat_SetupAlkesL').getValue() === ''){
		ShowPesanWarningSetupObatAlkes('Nama jenis obat masih kosong', 'Warning');
		x = 0;
	} 
	return x;
};


//------------------------------Sub Jenis Obat----------------------------------------------
function DataInitPanelSubJenisObat(rowdata){
	Ext.getCmp('txtKdSubJenisObat_SetupObatAlkesL').setValue(rowdata.kd_sub_jns);
	SetupObatAlkes.vars.subJenis.setValue(rowdata.sub_jenis);
};

function dataaddnewSubJenis_viSetupObatAlkes(){
	Ext.getCmp('txtKdSubJenisObat_SetupObatAlkesL').setValue('');
	SetupObatAlkes.vars.subJenis.setValue('');
}

function datasaveSubJenis_viSetupObatAlkes(){
	if (ValidasiEntrySubJenisObat(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionSetupObatAlkes/saveSubJenis",
				params: getParamSetupSubJenisObat(),
				failure: function(o)
				{
					ShowPesanErrorSetupObatAlkes('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoSetupObatAlkes(nmPesanSimpanSukses,nmHeaderSimpanData);
						Ext.get('txtKdSubJenisObat_SetupObatAlkesL').dom.value=cst.kdsubjenis;
						dsDataGrdSubJenis_viSetupMasterObat.reload();
					}
					else 
					{
						ShowPesanErrorSetupObatAlkes('Gagal Menyimpan Data ini', 'Error');
						dsDataGrdSubJenis_viSetupMasterObat.reload();
					};
				}
			}
			
		)
	}
}

function dataHapusSubJenis_viSetupObatAlkes(){
	if (ValidasiEntrySubJenisObat(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionSetupObatAlkes/deleteSubJenis",
				params: {
					kd_sub_jns:Ext.getCmp('txtKdSubJenisObat_SetupObatAlkesL').getValue()
				},
				failure: function(o)
				{
					ShowPesanErrorSetupObatAlkes('Data sudah digunakan dalam data obat, data ini tidak dapat di hapus! Hubungi Admin', 'Error');
					loadMask.hide();
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoSetupObatAlkes('Data berhasil dihapus','Informasi');
						dataaddnewSubJenis_viSetupObatAlkes();
						dsDataGrdSubJenis_viSetupMasterObat.reload();
					}
					else 
					{
						ShowPesanErrorSetupObatAlkes('Gagal menghapus data ini', 'Error');
						dsDataGrdSubJenis_viSetupMasterObat.reload();
					};
				}
			}
			
		)
	}
}

function getParamSetupSubJenisObat() {
	var	params =
	{
		KdSubJns:Ext.getCmp('txtKdSubJenisObat_SetupObatAlkesL').getValue(),
		SubJenis:SetupObatAlkes.vars.subJenis.getValue()
	}
   
    return params
};

function ValidasiEntrySubJenisObat(modul,mBolHapus){
	var x = 1;
	if(SetupObatAlkes.vars.subJenis.getValue() === ''){
		ShowPesanWarningSetupObatAlkes('Sub jenis obat masih kosong', 'Warning');
		x = 0;
	} 
	return x;
};

//------------------------------Jenis Terapi-----------------------------------------------
function DataInitPanelJenisTerapiObat(rowdata){
	Ext.getCmp('txtKdJenisTerapiObat_SetupObatAlkesL').setValue(rowdata.kd_jns_terapi);
	SetupObatAlkes.vars.jenisTerapi.setValue(rowdata.jenis_terapi);
};
function dataaddnewJenisTerapi_viSetupObatAlkes(){
	Ext.getCmp('txtKdJenisTerapiObat_SetupObatAlkesL').setValue('');
	SetupObatAlkes.vars.jenisTerapi.setValue('');
}

function datasaveJenisTerapi_viSetupObatAlkes(){
	if (ValidasiEntryJenisTerapiObat(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionSetupObatAlkes/saveJenisTerapi",
				params: getParamSetupJenisTerapiObat(),
				failure: function(o)
				{
					ShowPesanErrorSetupObatAlkes('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoSetupObatAlkes(nmPesanSimpanSukses,nmHeaderSimpanData);
						Ext.get('txtKdJenisTerapiObat_SetupObatAlkesL').dom.value=cst.kdjenisterapi;
						dsDataGrdJenisTerapi_viSetupMasterObat.reload();
					}
					else 
					{
						ShowPesanErrorSetupObatAlkes('Gagal Menyimpan Data ini', 'Error');
						dsDataGrdJenisTerapi_viSetupMasterObat.reload();
					};
				}
			}
			
		)
	}
}

function dataHapusJenisTerapi_viSetupObatAlkes(){
	if (ValidasiEntryJenisTerapiObat(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionSetupObatAlkes/deleteJenisTerapi",
				params: {
					kd_jns_terapi:Ext.getCmp('txtKdJenisTerapiObat_SetupObatAlkesL').getValue()
				},
				failure: function(o)
				{
					ShowPesanErrorSetupObatAlkes('Data sudah digunakan dalam data obat, data ini tidak dapat di hapus! Hubungi Admin', 'Error');
					loadMask.hide();
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoSetupObatAlkes('Data berhasil dihapus','Informasi');
						dataaddnewJenisTerapi_viSetupObatAlkes();
						dsDataGrdJenisTerapi_viSetupMasterObat.reload();
					}
					else 
					{
						ShowPesanErrorSetupObatAlkes('Gagal menghapus data ini', 'Error');
						dsDataGrdJenisTerapi_viSetupMasterObat.reload();
					};
				}
			}
			
		)
	}
}

function getParamSetupJenisTerapiObat() {
	var	params =
	{
		KdJnsTerapi:Ext.getCmp('txtKdJenisTerapiObat_SetupObatAlkesL').getValue(),
		JenisTerapi:SetupObatAlkes.vars.jenisTerapi.getValue()
	}
   
    return params
};

function ValidasiEntryJenisTerapiObat(modul,mBolHapus){
	var x = 1;
	if(SetupObatAlkes.vars.jenisTerapi.getValue() === ''){
		ShowPesanWarningSetupObatAlkes('jenis terapi obat masih kosong', 'Warning');
		x = 0;
	} 
	return x;
};


//------------------------------GOLONGAN Obat----------------------------------------------
function DataInitPanelGolongan(rowdata){
	Ext.getCmp('txtKdGolObat_SetupObatAlkesL').setValue(rowdata.apt_kd_golongan);
	SetupObatAlkes.vars.golongan.setValue(rowdata.apt_golongan);
};

function dataaddnewGolongan_viSetupObatAlkes(){
	Ext.getCmp('txtKdGolObat_SetupObatAlkesL').setValue('');
	SetupObatAlkes.vars.golongan.setValue('');
}

function datasaveGolongan_viSetupObatAlkes(){
	if (ValidasiEntryGolongan(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionSetupObatAlkes/saveGolongan",
				params: getParamSetupGolongan(),
				failure: function(o)
				{
					ShowPesanErrorSetupObatAlkes('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoSetupObatAlkes(nmPesanSimpanSukses,nmHeaderSimpanData);
						Ext.get('txtKdGolObat_SetupObatAlkesL').dom.value=cst.kdgolongan;
						dsDataGrdGolongan_viSetupMasterObat.reload();
					}
					else 
					{
						ShowPesanErrorSetupObatAlkes('Gagal Menyimpan Data ini', 'Error');
						dsDataGrdGolongan_viSetupMasterObat.reload();
					};
				}
			}
			
		)
	}
}

function datahapusGolongan_viSetupObatAlkes(){
	if (ValidasiEntryGolongan(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionSetupObatAlkes/deleteGolongan",
				params: {
					apt_kd_golongan:Ext.getCmp('txtKdGolObat_SetupObatAlkesL').getValue()
				},
				failure: function(o)
				{
					ShowPesanErrorSetupObatAlkes('Hubungi Admin', 'Error');
					loadMask.hide();
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoSetupObatAlkes('Data berhasil dihapus','Informasi');
						dataaddnewGolongan_viSetupObatAlkes();
						dsDataGrdGolongan_viSetupMasterObat.reload();
					}
					else 
					{
						ShowPesanErrorSetupObatAlkes('Data sudah digunakan dalam data obat, data ini tidak dapat di hapus! Hubungi admin', 'Error');
						dsDataGrdGolongan_viSetupMasterObat.reload();
					};
				}
			}
			
		)
	}
}

function getParamSetupGolongan() {
	var	params =
	{
		KdGolongan:Ext.getCmp('txtKdGolObat_SetupObatAlkesL').getValue(),
		Golongan:SetupObatAlkes.vars.golongan.getValue()
	}
   
    return params
};

function ValidasiEntryGolongan(modul,mBolHapus){
	var x = 1;
	if(SetupObatAlkes.vars.golongan.getValue() === ''){
		ShowPesanWarningSetupObatAlkes('Golongan obat masih kosong', 'Warning');
		x = 0;
	} 
	return x;
};



//------------------------------SATUAN ----------------------------------------------
function DataInitPanelSatuan(rowdata){
	Ext.getCmp('txtKdSatuan_SetupObatAlkesL').setValue(rowdata.kd_satuan);
	SetupObatAlkes.vars.satuan.setValue(rowdata.satuan);
};

function dataaddnewSatuan_viSetupObatAlkes(){
	Ext.getCmp('txtKdSatuan_SetupObatAlkesL').setValue('');
	SetupObatAlkes.vars.satuan.setValue('');
}

function datasaveSatuan_viSetupObatAlkes(){
	if (ValidasiEntrySatuan(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionSetupObatAlkes/saveSatuan",
				params: getParamSetupSatuan(),
				failure: function(o)
				{
					ShowPesanErrorSetupObatAlkes('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoSetupObatAlkes(nmPesanSimpanSukses,nmHeaderSimpanData);
						dsDataGrdSatuan_viSetupMasterObat.reload();
					} 
					else 
					{
						ShowPesanErrorSetupObatAlkes('Gagal Menyimpan Data ini', 'Error');
						dsDataGrdSatuan_viSetupMasterObat.reload();
					};
				}
			}
			
		)
	}
}

function datahapusSatuan_viSetupObatAlkes(){
	if (ValidasiEntrySatuan(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionSetupObatAlkes/deleteSatuan",
				params: {
					kd_satuan:Ext.getCmp('txtKdSatuan_SetupObatAlkesL').getValue()
				},
				failure: function(o)
				{
					ShowPesanErrorSetupObatAlkes('Data sudah digunakan dalam data obat, data ini tidak dapat di hapus! Hubungi Admin', 'Error');
					loadMask.hide();
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoSetupObatAlkes('Data berhasil dihapus','Informasi');
						dataaddnewSatuan_viSetupObatAlkes();
						dsDataGrdSatuan_viSetupMasterObat.reload();
					}
					else 
					{
						ShowPesanErrorSetupObatAlkes('Gagal menghapus data ini', 'Error');
						dsDataGrdSatuan_viSetupMasterObat.reload();
					};
				}
			}
			
		)
	}
}

function getParamSetupSatuan(){
	var	params =
	{
		KdSatuan:Ext.getCmp('txtKdSatuan_SetupObatAlkesL').getValue(),
		Satuan:SetupObatAlkes.vars.satuan.getValue()
	}
   
    return params
};

function ValidasiEntrySatuan(modul,mBolHapus){
	var x = 1;
	if(Ext.getCmp('txtKdSatuan_SetupObatAlkesL').getValue() === ''){
		ShowPesanWarningSetupObatAlkes('Kode Satuan masih kosong', 'Warning');
		x = 0;
	}
	if(SetupObatAlkes.vars.satuan.getValue() === ''){
		ShowPesanWarningSetupObatAlkes('Satuan masih kosong', 'Warning');
		x = 0;
	} 
	if(Ext.getCmp('txtKdSatuan_SetupObatAlkesL').getValue().length > 10){
		ShowPesanWarningSetupObatAlkes('Kode satuan kecil tidak boleh lebih dari 10 huruf', 'Warning');
		x = 0;
	}
	return x;
};


//------------------------------SATUAN BESAR----------------------------------------------
function DataInitPanelSatuanBesar(rowdata){
	Ext.getCmp('txtKdSatuanBesar_SetupObatAlkesL').setValue(rowdata.kd_sat_besar);
	SetupObatAlkes.vars.satuanBesar.setValue(rowdata.keterangan);
};

function dataaddnewSatuanBesar_viSetupObatAlkes(){
	Ext.getCmp('txtKdSatuanBesar_SetupObatAlkesL').setValue('');
	SetupObatAlkes.vars.satuanBesar.setValue('');
}

function datasaveSatuanBesar_viSetupObatAlkes(){
	if (ValidasiEntrySatuanBesar(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionSetupObatAlkes/saveSatuanBesar",
				params: getParamSetupSatuanBesar(),
				failure: function(o)
				{
					ShowPesanErrorSetupObatAlkes('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoSetupObatAlkes(nmPesanSimpanSukses,nmHeaderSimpanData);
						//Ext.get('txtKdSatuanBesar_SetupObatAlkesL').dom.value=cst.kdsatbesar;
						dsDataGrdSatuanBesar_viSetupMasterObat.reload();
					}
					else 
					{
						ShowPesanErrorSetupObatAlkes('Gagal Menyimpan Data ini', 'Error');
						dsDataGrdSatuanBesar_viSetupMasterObat.reload();
					};
				}
			}
			
		)
	}
}


function datahapusSatuanBesar_viSetupObatAlkes(){
	if (ValidasiEntrySatuanBesar(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionSetupObatAlkes/deleteSatuanBesar",
				params: {
					kd_sat_besar:Ext.getCmp('txtKdSatuanBesar_SetupObatAlkesL').getValue()
				},
				failure: function(o)
				{
					ShowPesanErrorSetupObatAlkes('Hubungi Admin', 'Error');
					loadMask.hide();
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoSetupObatAlkes('Data berhasil dihapus','Informasi');
						dataaddnewSatuanBesar_viSetupObatAlkes();
						dsDataGrdSatuanBesar_viSetupMasterObat.reload();
					}
					else 
					{
						ShowPesanErrorSetupObatAlkes('Data sudah digunakan dalam data obat, data ini tidak dapat di hapus! Hubungi admin', 'Error');
						dsDataGrdSatuanBesar_viSetupMasterObat.reload();
					};
				}
			}
			
		)
	}
}


function getParamSetupSatuanBesar(){
	var	params =
	{
		KdSatBesar:Ext.getCmp('txtKdSatuanBesar_SetupObatAlkesL').getValue(),
		Keterangan:SetupObatAlkes.vars.satuanBesar.getValue()
	}
   
    return params
};

function ValidasiEntrySatuanBesar(modul,mBolHapus){
	var x = 1;
	if(Ext.getCmp('txtKdSatuanBesar_SetupObatAlkesL').getValue() === ''){
		ShowPesanWarningSetupObatAlkes('Kode satuan besar satuan masih kosong', 'Warning');
		x = 0;
	}
	if(SetupObatAlkes.vars.satuanBesar.getValue() === ''){
		ShowPesanWarningSetupObatAlkes('Keterangan satuan masih kosong', 'Warning');
		x = 0;
	}	
	if(Ext.getCmp('txtKdSatuanBesar_SetupObatAlkesL').getValue().length > 10){
		ShowPesanWarningSetupObatAlkes('Kode satuan besar tidak boleh lebih dari 10 huruf', 'Warning');
		x = 0;
	}
	return x;
};


function loadDataComboJenisObat_SetupObatAlkes(param){
	if (param==='' || param===undefined) {
		param={
			text: '0',
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/functionSetupObatAlkes/getJenisGrid",
		params: {text:param},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cboJenisObat_SetupObatAlkes.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsJenisObat_SetupObatAlkes.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsJenisObat_SetupObatAlkes.add(recs);
				console.log(o);
			}
		}
	});
}

function mComboSetupJenisObat_SetupObatAlkes()
{
    var Field = ['kd_jns_obt','nama_jenis'];
    dsJenisObat_SetupObatAlkes = new WebApp.DataStore({fields: Field});
	loadDataComboJenisObat_SetupObatAlkes();
    cboJenisObat_SetupObatAlkes = new Ext.form.ComboBox
    (
        {
            id: 'cboJenisObat_SetupObatAlkes',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
			hideTrigger		: true,
            store: dsJenisObat_SetupObatAlkes,
            valueField: 'nama_jenis',
            displayField: 'nama_jenis',
            width:150,
            listeners:
			{
				'select': function(a, b, c)
				{
					Ext.getCmp('txtKdJenisObat_SetupObatAlkesL').setValue(b.data.kd_jns_obt);
				},
				keyUp: function(a,b,c){
					
					if(  b.getKey()!=127 ){
						clearTimeout(this.time);
				
						this.time=setTimeout(function(){
							if(cboJenisObat_SetupObatAlkes.lastQuery != '' ){
								var value="";
								
								if (value!=cboJenisObat_SetupObatAlkes.lastQuery)
								{
									if (a.rendered && a.innerList != null) {
										a.innerList.update(a.loadingText ? '<div class="loading-indicator">' + a.loadingText + '</div>' : '');
										a.restrictHeight();
										a.selectedIndex = 0;
									}
									a.expand();
									Ext.Ajax.request({
										url: baseURL + "index.php/apotek/functionSetupObatAlkes/getJenisGrid",
										params: {text:cboJenisObat_SetupObatAlkes.lastQuery},
										failure: function(o){
											var cst = Ext.decode(o.responseText);
										},	    
										success: function(o) {
											loadGridJenisObat(cboJenisObat_SetupObatAlkes.lastQuery);
										
											cboJenisObat_SetupObatAlkes.store.removeAll();
											
											var cst = Ext.decode(o.responseText);

											for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
												var recs    = [],recType = dsJenisObat_SetupObatAlkes.recordType;
												var o=cst['listData'][i];
										
												recs.push(new recType(o));
												dsJenisObat_SetupObatAlkes.add(recs);
											}
											a.expand();
											if(dsJenisObat_SetupObatAlkes.onShowList != undefined)
												dsJenisObat_SetupObatAlkes.onShowList(cst[showVar]);
											if(cst['listData'].length>0){
													
												a.doQuery(a.allQuery, true);
												a.expand();
												a.selectText(value.length,value.length);
											}else{
											//	if (a.rendered && a.innerList != null) {
													a.innerList.update(a.loadingText ? '&nbsp; Data Tidak Ada' : '');
													a.restrictHeight();
													a.selectedIndex = 0;
												//}
											}
											
										}
									});
									value=cboJenisObat_SetupObatAlkes.lastQuery;
								}
							}
						},1000);
					}
				}, 
				blur: function(a,b,c){
					clearTimeout(this.time);
					//cboJenisObat_SetupObatAlkes.store.loadData([],false);
					a.setValue(a.value);
					a.setRawValue(a.value);
					if(cboJenisObat_SetupObatAlkes.blur != undefined)cboJenisObat_SetupObatAlkes.blur(a,b,c);
					console.log(a);console.log(b);console.log(c);
				}
			}
        }
    )
    return cboJenisObat_SetupObatAlkes;
};



function ShowPesanWarningSetupObatAlkes(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorSetupObatAlkes(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoSetupObatAlkes(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};