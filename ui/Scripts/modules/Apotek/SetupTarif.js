var dataSource_viSetupTarif;
var NamaForm_viSetupTarif="Setup Tarif";
var mod_name_viSetupTarif="Setup Tarif";
var now_viSetupTarif= new Date();
var rowSelected_viSetupTarif;
var rowSelected_viSetupTarifObat;
var setLookUps_viSetupTarif;


var GridDataView_viSetupTarif;
var dscombogolongan_setupTarif;
var cbogolongan_setupTarif;
var cbojenistarif_setupTarif;
var gridcbojenistarif_setupTarif;
var gridcbogolongan_setupTarif;
var dsgridcombogolongan_setupTarif;
var dsgridcombojenistarif_setupTarif;
var cGol;
var cJenis;
var cUnit;
var dGol;
var dJenis;
var dUnit;
var dUrut;
var dKdUnitFar;


var CurrentData_viSetupTarif =
{
	data: Object,
	details: Array,
	row: 0
};
var CurrentData_viSetupTarifObat =
{
	data: Object,
	details: Array,
	row: 0
};

var SetupTarif={};
SetupTarif.form={};
SetupTarif.func={};
SetupTarif.vars={};
SetupTarif.func.parent=SetupTarif;
SetupTarif.form.ArrayStore={};
SetupTarif.form.ComboBox={};
SetupTarif.form.DataStore={};
SetupTarif.form.Record={};
SetupTarif.form.Form={};
SetupTarif.form.Grid={};
SetupTarif.form.Panel={};
SetupTarif.form.TextField={};
SetupTarif.form.Button={};

SetupTarif.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_milik', 'milik'],
	data: []
});

CurrentPage.page = dataGrid_viSetupTarif(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

shortcut.set({
	code:'main',
	list:[
		{
			key:'ctrl+s',
			fn:function(){
				Ext.getCmp('btnSimpan_viSetupTarif').el.dom.click();
			}
		},
		{
			key:'ctrl+d',
			fn:function(){
				Ext.getCmp('btnDelete_viSetupTarif').el.dom.click();
			}
		}
	]
});

function dataGrid_viSetupTarif(mod_id_viSetupTarif){	
	
    var FieldMaster_viSetupTarif =  [  ];
    dataSource_viSetupTarif = new WebApp.DataStore ({ fields: FieldMaster_viSetupTarif });
	
	var Fieldgol = ['kd_gol','ket_gol'];
    dsgridcombogolongan_setupTarif = new WebApp.DataStore({ fields: Fieldgol });
	
	var Fieldjenis = ['kd_jenis','ket_jenis'];
	dsgridcombojenistarif_setupTarif = new WebApp.DataStore({ fields: Fieldjenis });
	
    loaddatacombogolongan_setupTarif();
	loaddatacombojenistarif_setupTarif();
	loaddatagridcombogolongan_setupTarif();
	loaddatagridcombojenistarif_setupTarif();
	
	dataGriSetupTarif();
	
    // Grid Apotek Perencanaan # --------------
	GridDataView_viSetupTarif = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viSetupTarif,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:true,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.CellSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						cellselect: function(sm, row, rec)
						{
							rowSelected_viSetupTarif = undefined;
							rowSelected_viSetupTarif = dataSource_viSetupTarif.getAt(row);
							CurrentData_viSetupTarif
							CurrentData_viSetupTarif.row = row;
							CurrentData_viSetupTarif.data = rowSelected_viSetupTarif.data;
							Ext.getCmp('btnDelete_viSetupTarif').enable();
							Ext.getCmp('btnSimpan_viSetupTarif').enable();
							
							dGol = CurrentData_viSetupTarif.data.kd_gol;
							dJenis = CurrentData_viSetupTarif.data.kd_jenis;
							dUnit = CurrentData_viSetupTarif.data.kd_unit_tarif;
							dUrut = CurrentData_viSetupTarif.data.urut;
							dKdUnitFar= CurrentData_viSetupTarif.data.kd_unit_far;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viSetupTarif = dataSource_viSetupTarif.getAt(ridx);
					if (rowSelected_viSetupTarif != undefined)
					{
						console.log(rowSelected_viSetupTarif.data)
						setLookUp_viSetupTarif(rowSelected_viSetupTarif.data);
					}
					else
					{
						setLookUp_viSetupTarif();
					}
				},
				render: function(){
					AddNewSetupTarif(true);
					Ext.getCmp('btnAddRowTarif_viSetupTarif').disable();
					Ext.getCmp('btnSimpan_viSetupTarif').disable();
					Ext.getCmp('btnDelete_viSetupTarif').disable();
					
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Setup Tarif
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header		: 'Golongan',
						width		: 130,
						menuDisabled: true,
						dataIndex	: 'ket_gol',
						editor		: gridcbogolongan_setupTarif= new Ext.form.ComboBox({
							id				: 'gridcbogolongan_setupTarif',
							typeAhead		: true,
							triggerAction	: 'all',
							lazyRender		: true,
							mode			: 'local',
							emptyText		: '',
							fieldLabel		: ' ',
							align			: 'Right',
							width			: 200,
							store			: dsgridcombogolongan_setupTarif,
							valueField		: 'ket_gol',
							displayField	: 'ket_gol',
							listeners		:
							{
								select	: function(a,b,c){
									var line	= GridDataView_viSetupTarif.getSelectionModel().selection.cell[0];
									dataSource_viSetupTarif.getRange()[line].data.kd_gol=b.data.kd_gol;
								},
								keyUp: function(a,b,c){
									$this1=this;
									if((b.getKey()>=65 && b.getKey()<=90)|| (b.getKey()>=48 && b.getKey()<=57) || b.getKey()==32 || b.getKey()==8 || b.getKey()==46){
										clearTimeout(this.time);

										this.time=setTimeout(function(){
											if($this1.lastQuery != '' ){
												var value=$this1.lastQuery;
												var param={};
												param['text']=$this1.lastQuery;
												loaddatagridcombogolongan_setupTarif(param);
											}
										},1000);
									}
								}
							}
						})
					},
					//-------------- ## --------------
					{
						header: 'Jenis Tarif',
						dataIndex: 'ket_jenis',
						hideable:false,
						menuDisabled: true,
						width: 90,
						editor		: gridcbojenistarif_setupTarif= new Ext.form.ComboBox({
								id				: 'gridcbojenistarif_setupTarif',
								typeAhead		: true,
								triggerAction	: 'all',
								lazyRender		: true,
								mode			: 'local',
								emptyText		: '',
								fieldLabel		: ' ',
								align			: 'Right',
								width			: 200,
								store			: dsgridcombojenistarif_setupTarif,
								valueField		: 'ket_jenis',
								displayField	: 'ket_jenis',
								listeners		:
								{
									select	: function(a,b,c){
										var line	= GridDataView_viSetupTarif.getSelectionModel().selection.cell[0];
										dataSource_viSetupTarif.getRange()[line].data.kd_jenis=b.data.kd_jenis;
									},
									keyUp: function(a,b,c){
										$this1=this;
										if((b.getKey()>=65 && b.getKey()<=90)|| (b.getKey()>=48 && b.getKey()<=57) || b.getKey()==32 || b.getKey()==8 || b.getKey()==46){
											clearTimeout(this.time);

											this.time=setTimeout(function(){
												if($this1.lastQuery != '' ){
													var value=$this1.lastQuery;
													var param={};
													param['text']=$this1.lastQuery;
													loaddatagridcombojenistarif_setupTarif(param);
												}
											},1000);
										}
									}
								}
							}
						)
					},
					//-------------- ## --------------
					{
						header: 'Unit',
						dataIndex: 'unit',
						hideable:false,
						menuDisabled: true,
						width: 90,
						editor: new Ext.form.ComboBox ( {
							id				: 'gridcboUnit_setupTarif',
							typeAhead		: true,
							triggerAction	: 'all',
							lazyRender		: true,
							mode			: 'local',
							selectOnFocus	: true,
							forceSelection	: true,
							anchor			: '95%',
							value			: 1,
							store			: new Ext.data.ArrayStore({
								id		: 0,
								fields	:['Id','displayText'],
								data	: [[0, 'Rawat Jalan / UGD'],[1, 'Rawat Inap'],[2, 'Non Resep']]
							}),
							valueField	: 'displayText',
							displayField: 'displayText',
							value		: '',
							listeners	: {}
						})
					},
					{
						header: 'Batas bawah harga',
						dataIndex: 'BATASHRG_BAWAH',
						hideable:false,
						menuDisabled: true,
						align: 'right',
						width: 90,
						editor: new Ext.form.NumberField({
						})
					},
					{
						header: 'Batas atas harga',
						dataIndex: 'BATASHRG_ATAS',
						hideable:false,
						menuDisabled: true,
						align: 'right',
						width: 90,
						editor: new Ext.form.NumberField({
						})
					},
					//-------------- ## --------------
					{
						header: 'Jumlah',
						dataIndex: 'JUMLAH',
						hideable:false,
						menuDisabled: true,
						align: 'right',
						width: 90,
						editor: new Ext.form.NumberField({
						})
					},
					//-------------- ## --------------
					{
						header: 'Faktor',
						dataIndex: 'faktor',
						hideable:false,
						menuDisabled: true,
						width: 90,
						editor: new Ext.form.ComboBox ( {
							id				: 'gridcboFaktor_setupTarif',
							typeAhead		: true,
							triggerAction	: 'all',
							lazyRender		: true,
							mode			: 'local',
							selectOnFocus	: true,
							forceSelection	: true,
							width			: 50,
							anchor			: '95%',
							value			: 1,
							store			: new Ext.data.ArrayStore({
								id		: 0,
								fields	:['Id','displayText'],
								data	: [[0, 'Penjumlah'],[1, 'Pengali']]
							}),
							valueField	: 'displayText',
							displayField: 'displayText',
							value		: '',
							listeners	: {}
						})
					},
					//-------------- ## --------------
					{
						header: 'kd_gol',
						dataIndex: 'KD_GOL',
						hideable:false,
						menuDisabled: true,
						hidden:true,
						width: 90
					},
					//-------------- ## --------------
					{
						header: 'kd_jenis',
						dataIndex: 'KD_JENIS',
						hideable:false,
						hidden:true,
						menuDisabled: true,
						width: 90
					},
					//-------------- ## --------------
					{
						header: 'kd_unit_far',
						dataIndex: 'KD_UNIT_FAR',
						hideable:false,
						hidden:true,
						menuDisabled: true,
						width: 90
					},
					//-------------- ## --------------
					{
						header: 'urut',
						dataIndex: 'URUT',
						hideable:false,
						hidden:true,
						menuDisabled: true,
						width: 90
					},
					//-------------- ## --------------
					
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viSetupTarif',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add Row',
						iconCls: 'Edit_Tr',
						id: 'btnAddRowTarif_viSetupTarif',
						handler: function(){
							var records = new Array();
							records.push(new dataSource_viSetupTarif.recordType());
							dataSource_viSetupTarif.add(records);
							DataClearSetupTarif();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viSetupTarif',
						handler: function()
						{
							loadMask.show();
							dataSave_viSetupTarif();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viSetupTarif',
						handler: function()
						{
							if((dGol == '' || dGol == undefined) && (dJenis == '' || dJenis == undefined) && (dUnit == '' || dUnit == undefined) && 
								(dUrut == '' || dUrut == undefined) && (dKdUnitFar == '' || dKdUnitFar == undefined)){
								ShowPesanWarningSetupTarif('Pilih data yang akan di hapus', 'Warning');
							} else{
								Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
									if (button == 'yes'){
										loadMask.show();
										dataDelete_viSetupTarif();
									}
								});
							}
							
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_viSetupTarif',
						handler: function()
						{
							dataSource_viSetupTarif.removeAll();
							DataClearSetupTarif();
							dataGriSetupTarif();
							AddNewSetupTarif(true);
							Ext.getCmp('btnAddRowTarif_viSetupTarif').disable();
							Ext.getCmp('btnSimpan_viSetupTarif').disable();
							Ext.getCmp('btnDelete_viSetupTarif').disable();
						}
					},
					{
						xtype: 'tbseparator'
					}
					//-------------- ## --------------
				]
			},
			viewConfig: 
			{
				forceFit: true
			}
		}
	)	
	var PanelTabSetupTarif = new Ext.FormPanel({
        labelAlign: 'top',
		layout:'form',
		border: false,  
		autoHeight:true,
        title: '',
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [PanelPencarianTarif_setupTarif()]
		
	})
	// Kriteria filter pada Grid # --------------
    var FrmData_viSetupTarif = new Ext.Panel
    (
		{
			title: NamaForm_viSetupTarif,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viSetupTarif,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ PanelTabSetupTarif,
					GridDataView_viSetupTarif],
			tbar:[
			{
				xtype: 'button',
				text: 'Add New',
				iconCls: 'add',
				id: 'btnAddNewTarif_viSetupTarif',
				handler: function(){
					GridDataView_viSetupTarif.store.removeAll();
					var records = new Array();
					records.push(new dataSource_viSetupTarif.recordType());
					dataSource_viSetupTarif.add(records);
					AddNewSetupTarif(false);
					Ext.getCmp('btnAddRowTarif_viSetupTarif').enable();
					Ext.getCmp('btnSimpan_viSetupTarif').enable();
					Ext.getCmp('btnDelete_viSetupTarif').enable();
				}
			},
			
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_viSetupTarif;
    //-------------- # End form filter # --------------
}
//hani 2023-02-21
function setLookUp_viSetupTarif(rowdata) {
	var lebar = 985;
	setLookUps_viSetupTarif = new Ext.Window({
		id: Nci.getId(),
		title: NamaForm_viSetupTarif,
		closeAction: 'destroy',
		width: 750,
		constrain: true,
		height: 540,
		autoHeight: true,
		resizable: false,
		autoScroll: false,
		border: true,
		constrainHeader: true,
		// iconCls: 'capsule',
		modal: true,
		items: getFormItemEntry_viSetupTarif(lebar, rowdata),
		listeners: {
			activate: function () {

			},
			afterShow: function () {
				this.activate();
			},
			deactivate: function () {
				rowSelected_viSetupTarif = undefined;
			},
			close: function () {
				shortcut.remove('lookup');
			},
		}
	});

	loadDataComboJenisObat();
	setLookUps_viSetupTarif.show();

	if (rowdata == undefined) {

	} else {
		datainit_viSetupTarif(rowdata);
	}
	shortcut.set({
		code: 'lookup',
		list: [{
				key: 'ctrl+s',
				fn: function () {
					Ext.getCmp('btnSimpan_viMasterObat').el.dom.click();
				}
			},
			{
				key: 'ctrl+d',
				fn: function () {
					Ext.getCmp('btnHapusObat_viMasterObat').el.dom.click();
				}
			},
			{
				key: 'esc',
				fn: function () {
					setLookUps_viSetupTarif.close();
				}
			}
		]
	});
}

function getFormItemEntry_viSetupTarif(lebar, rowdata) {
	var pnlFormDataBasic_viMasterObat = new Ext.FormPanel({
		title: '',
		region: 'north',
		layout: 'form',
		bodyStyle: 'padding:10px 10px 10px 10px',
		anchor: '100%',
		labelWidth: 1,
		autoWidth: true,
		width: lebar,
		border: false,
		items: [getItemPanelInputBiodata_viMasterObat(lebar), dataGrid_viSetupTarifObat(rowdata)],
		fileUpload: true,
		tbar: {
			xtype: 'toolbar',
			items: [{
					xtype: 'button',
					text: 'Add New',
					iconCls: 'add',
					id: 'btnAdd_viMasterObat',
					handler: function () {
						dataaddnew_viTarifObat();
					}
				},
				{
					xtype: 'tbseparator'
				},
				{
					xtype: 'button',
					text: 'Save',
					iconCls: 'save',
					id: 'btnSimpan_viSetupTarifObat',
					handler: function () {
						loadMask.show();
						dataSave_viSetupTarifGolongan();
					}
				},
				{
					xtype: 'tbseparator'
				},
				{
					xtype: 'button',
					text: 'Save & Close',
					iconCls: 'saveexit',
					hidden: true,
					id: 'btnSimpanExit_viMasterObat',
					handler: function () {
						loadMask.show();
						dataSave_viSetupTarifGolongan();
						refreshMasterObat();
						setLookUp_viSetupTarif.close();
					}
				},
				{
					xtype: 'tbseparator',
					hidden: true,
				},
				{
					xtype: 'button',
					text: 'Remove',
					iconCls: 'remove',
					id: 'btnDelete_viSetupTarifObat',
					handler: function () {
						loadMask.show();
						hapusobat_viMasterObat();
					}
				},
				{
					xtype: 'tbseparator'
				}

			]
		}
	})

	return pnlFormDataBasic_viMasterObat;
}
function dataaddnew_viTarifObat() {
	Ext.getCmp('txtHargaBawahGolongan').setValue('');
	Ext.getCmp('txtHargaAtasGolongan').setValue('');
	Ext.getCmp('txtMarkupGolongan').setValue('');
	Ext.getCmp('cboJenisObat').setValue('');
	Ext.getCmp('cboFaktor').setValue('');
}
function getItemPanelInputBiodata_viMasterObat(lebar) {
	var items = {
		title: '',
		layout: 'column',
		//height:150,
		items: [{
				columnWidth: .50,
				layout: 'form',
				labelWidth: 100,
				bodyStyle: 'padding: 10px',
				border: false,
				items: [{
						xtype: 'textfield',
						fieldLabel: 'Golongan Cust',
						name: 'txtGolonganCust',
						id: 'txtGolonganCust',
						readOnly: true,
						tabIndex: 0,
						width: 100
					},
					{
						xtype: 'textfield',
						fieldLabel: 'Jenis Tarif',
						readOnly: true,
						name: 'txtJenisTarif',
						id: 'txtJenisTarif',
						tabIndex: 1,
						width: 180
					},
					{
						xtype: 'textfield',
						fieldLabel: 'Unit ',
						readOnly: true,
						name: 'txtUnit',
						id: 'txtUnit',
						tabIndex: 5,
						width: 180
					},
					cbo_JenisObat(),
				]
			},
			{
				columnWidth: .50,
				layout: 'form',
				labelWidth: 100,
				bodyStyle: 'padding: 10px',
				border: false,
				items: [
					{
						xtype: 'numberfield',
						fieldLabel: 'Batas Harga Bawah',
						name: 'txtHargaBawahGolongan',
						id: 'txtHargaBawahGolongan',
						tabIndex: 9,
						width: 180
					},
					{
						xtype: 'textfield',
						fieldLabel: 'Batas Harga Atas ',
						name: 'txtHargaAtasGolongan',
						id: 'txtHargaAtasGolongan',
						tabIndex: 9,
						// hidden: true,
						width: 180
					},
					{
						xtype: 'textfield',
						fieldLabel: 'Markup',
						name: 'txtMarkupGolongan',
						id: 'txtMarkupGolongan',
						tabIndex: 9,
						// hidden: true,
						width: 180
					},
					cbo_Faktor(),
				]
			}

		]
	};
	return items;
};
function dataGrid_viSetupTarifObat(rowdata){	
	
    var FieldMaster_viSetupTarifObat =  [  ];
    dataSource_viSetupTarifObat = new WebApp.DataStore ({ fields: FieldMaster_viSetupTarifObat });

	dataGriSetupTarifGolongan(rowdata.KD_GOL, rowdata.KD_JENIS, rowdata.KD_UNIT_TARIF, rowdata.KD_UNIT_FAR);
	
    // Grid Apotek Perencanaan # --------------
	GridDataView_viSetupTarifObat = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viSetupTarifObat,
			autoScroll: true,
			columnLines: true,
			height: 300,
			flex:1,
			border:true,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.CellSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						cellselect: function(sm, row, rec)
						{
							rowSelected_viSetupTarifObat = undefined;
							rowSelected_viSetupTarifObat = dataSource_viSetupTarifObat.getAt(row);
							CurrentData_viSetupTarifObat
							CurrentData_viSetupTarifObat.row = row;
							CurrentData_viSetupTarifObat.data = rowSelected_viSetupTarifObat.data;
							Ext.getCmp('btnDelete_viSetupTarifObat').enable();
							Ext.getCmp('btnSimpan_viSetupTarifObat').enable();
							console.log(CurrentData_viSetupTarifObat.data);
							
							Ext.getCmp('txtGolonganCust').setValue(CurrentData_viSetupTarifObat.data.ket_gol);
							Ext.getCmp('txtJenisTarif').setValue(CurrentData_viSetupTarifObat.data.ket_jenis);
							Ext.getCmp('txtUnit').setValue(CurrentData_viSetupTarifObat.data.unit);
							Ext.getCmp('cboJenisObat').setValue(CurrentData_viSetupTarifObat.data.SUB_JENIS);
							Ext.getCmp('txtHargaBawahGolongan').setValue(CurrentData_viSetupTarifObat.data.BATASHRG_BAWAH);
							Ext.getCmp('txtHargaAtasGolongan').setValue(CurrentData_viSetupTarifObat.data.BATASHRG_ATAS);
							Ext.getCmp('txtMarkupGolongan').setValue(CurrentData_viSetupTarifObat.data.JUMLAH);
							Ext.getCmp('cboFaktor').setValue(CurrentData_viSetupTarifObat.data.faktor);

							dGol = CurrentData_viSetupTarifObat.data.KD_GOL;
							dkd_sub_jenis = CurrentData_viSetupTarifObat.data.KD_SUB_JNS;
							dJenis = CurrentData_viSetupTarifObat.data.KD_JENIS;
							dUnit = CurrentData_viSetupTarifObat.data.KD_UNIT_FAR;
							dUrut = CurrentData_viSetupTarifObat.data.URUT;
							dKdUnitFar= CurrentData_viSetupTarifObat.data.KD_UNIT_FAR;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viSetupTarifObat = dataSource_viSetupTarifObat.getAt(ridx);
					if (rowSelected_viSetupTarifObat != undefined)
					{
						console.log(rowSelected_viSetupTarifObat.data)
						// setLookUp_viSetupTarif(rowSelected_viSetupTarif.data);
					}
					else
					{
						// setLookUp_viSetupTarif();
					}
				},
				render: function(){
					AddNewSetupTarif(true);
					// Ext.getCmp('btnAddRowTarifObat_viSetupTarif').disable();
					// Ext.getCmp('btnSimpan_viSetupTarifObat').disable();
					// Ext.getCmp('btnDelete_viSetupTarifObat').disable();
					
				}
				// End Function # --------------
			},
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header		: 'Golongan Customer',
						width		: 130,
						menuDisabled: true,
						dataIndex	: 'ket_gol',
					},
					//-------------- ## --------------
					{
						header: 'Jenis Tarif',
						dataIndex: 'ket_jenis',
						hideable:false,
						menuDisabled: true,
						width: 90,
					},
					//-------------- ## --------------
					{
						header: 'Unit',
						dataIndex: 'unit',
						hideable:false,
						menuDisabled: true,
						width: 90,
					},
					{
						header: 'Urut',
						dataIndex: 'URUT',
						hideable:false,
						menuDisabled: true,
						width: 90,
					},
					{
						header: 'Gol.Obat',
						dataIndex: 'SUB_JENIS',
						hideable:false,
						menuDisabled: true,
						width: 90,
					},
					{
						header: 'Batas Hrg Bawah',
						dataIndex: 'BATASHRG_BAWAH',
						hideable:false,
						menuDisabled: true,
						align: 'right',
						width: 90,
					},
					{
						header: 'Batas Hrg Atas',
						dataIndex: 'BATASHRG_ATAS',
						hideable:false,
						menuDisabled: true,
						align: 'right',
						width: 90,
					},
					{
						header: 'Jumlah',
						dataIndex: 'JUMLAH',
						hideable:false,
						menuDisabled: true,
						align: 'right',
						width: 90,
					},
					{
						header: 'Faktor',
						dataIndex: 'faktor',
						hideable:false,
						menuDisabled: true,
						width: 90,
					},
					//-------------- ## --------------
					{
						header: 'kd_gol',
						dataIndex: 'KD_GOL',
						hideable:false,
						menuDisabled: true,
						hidden:true,
						width: 90
					},
					//-------------- ## --------------
					{
						header: 'kd_jenis',
						dataIndex: 'KD_JENIS',
						hideable:false,
						hidden:true,
						menuDisabled: true,
						width: 90
					},
					//-------------- ## --------------
					{
						header: 'kd_unit_far',
						dataIndex: 'KD_UNIT_FAR',
						hideable:false,
						hidden:true,
						menuDisabled: true,
						width: 90
					},
				]
			),
			viewConfig: 
			{
				forceFit: true
			}
		}
	)
    return GridDataView_viSetupTarifObat;
    //-------------- # End form filter # --------------
}

function dataGriSetupTarifGolongan(gol,jenis,unit, unit_far){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionSetupTarif/getItemGridObat",
			params: {
				gol : gol,
				jenis : jenis,
				unit : unit,
				unit_far : unit_far
			},
			failure: function(o)
			{
				loadMask.hide()
				ShowPesanErrorSetupTarif('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dataSource_viSetupTarifObat.removeAll();
					var recs=[],
						recType=dataSource_viSetupTarifObat.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dataSource_viSetupTarifObat.add(recs);
					
					
					
						GridDataView_viSetupTarifObat.getView().refresh();
				}
				else 
				{
					loadMask.hide()
					ShowPesanErrorSetupTarif('Gagal membaca data obat', 'Error');
				};
			}
		}
		
	)
	
}
function datainit_viSetupTarif(rowdata) {
	loadDataComboJenisObat();
	//alert(rowdata.AKTIF);
	Ext.getCmp('txtGolonganCust').setValue(rowdata.ket_gol);
	Ext.getCmp('txtJenisTarif').setValue(rowdata.ket_jenis);
	Ext.getCmp('txtUnit').setValue(rowdata.unit);
};

function dataSave_viSetupTarifGolongan() {
	if (ValidasiEntryTarifObat(nmHeaderSimpanData, false) == 1) {
		Ext.Ajax.request({
				url: baseURL + "index.php/apotek/functionSetupTarif/saveTarifObat",
				params: getParamTarifGolongan(),
				failure: function (o) {
					loadMask.hide();
					ShowPesanWarningSetupTarif('Hubungi Admin', 'Error');
				},
				success: function (o) {
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) {
						loadMask.hide();
						ShowPesanInfoSetupTarif(nmPesanSimpanSukses, nmHeaderSimpanData);
						// Ext.get('txtKdPrd_MasterObatL').dom.value = cst.kdprd;
						// refreshMasterObat();
						dataGriSetupTarifGolongan(CurrentData_viSetupTarifObat.data.KD_GOL, CurrentData_viSetupTarifObat.data.KD_JENIS, CurrentData_viSetupTarifObat.data.KD_UNIT_TARIF, CurrentData_viSetupTarifObat.data.KD_UNIT_FAR);

					} else {
						loadMask.hide();
						ShowPesanWarningSetupTarif('Gagal Menyimpan Data ini', 'Error');
						// refreshMasterObat();
						dataGriSetupTarifGolongan(CurrentData_viSetupTarifObat.data.KD_GOL, CurrentData_viSetupTarifObat.data.KD_JENIS, CurrentData_viSetupTarifObat.data.KD_UNIT_TARIF, CurrentData_viSetupTarifObat.data.KD_UNIT_FAR);

					};
				}
			}

		)
	}
}
function ValidasiEntryTarifObat(modul, mBolHapus) {
	var x = 1;
	if (Ext.getCmp('txtGolonganCust').getValue() === '') {
		loadMask.hide();
		ShowPesanWarningSetupTarif('Golongan masih kosong', 'Warning');
		x = 0;
	}
	if (Ext.getCmp('txtJenisTarif').getValue() === '') {
		loadMask.hide();
		ShowPesanWarningSetupTarif('Jenis tarif obat masih kosong', 'Warning');
		x = 0;
	}
	if (Ext.getCmp('txtUnit').getValue() === '') {
		loadMask.hide();
		ShowPesanWarningSetupTarif('Unit masih kosong', 'Warning');
		x = 0;
	}
	if (Ext.getCmp('txtHargaBawahGolongan').getValue() === '') {
		loadMask.hide();
		ShowPesanWarningSetupTarif('Batas Harga Bawah masih kosong', 'Warning');
		x = 0;
	}
	if (Ext.getCmp('txtHargaAtasGolongan').getValue() === '') {
		loadMask.hide();
		ShowPesanWarningSetupTarif('Batas Harga Atasr masih kosong', 'Warning');
		x = 0;
	}
	if (Ext.getCmp('txtMarkupGolongan').getValue() === '') {
		loadMask.hide();
		ShowPesanWarningSetupTarif('Markup masih kosong', 'Warning');
		x = 0;
	}
	if (Ext.getCmp('cboFaktor').getValue() === '') {
		loadMask.hide();
		ShowPesanWarningSetupTarif('Faktor masih kosong', 'Warning');
		x = 0;
	}
	if (Ext.getCmp('cboJenisObat').getValue() === '') {
		loadMask.hide();
		ShowPesanWarningSetupTarif('Jenis Obat masih kosong', 'Warning');
		x = 0;
	}
	return x;
};

function getParamTarifGolongan() {
	var params = {
		BATASHRG_BAWAH: Ext.getCmp('txtHargaBawahGolongan').getValue(),
		BATASHRG_ATAS: Ext.getCmp('txtHargaAtasGolongan').getValue(),
		JUMLAH: Ext.getCmp('txtMarkupGolongan').getValue(),
		KD_SUB_JNS: dkd_sub_jenis,
		TAG: Ext.getCmp('cboFaktor').getValue(),
		KD_GOL: rowSelected_viSetupTarif.data.KD_GOL,
		KD_JENIS: rowSelected_viSetupTarif.data.KD_JENIS,
		KD_UNIT_TARIF: rowSelected_viSetupTarif.data.KD_UNIT_TARIF,
		URUT: rowSelected_viSetupTarif.data.URUT,
	}

	return params
};

function hapusobat_viTarifGolongan() {
	if (Ext.getCmp('txtKdPrd_MasterObatL').getValue() != "") {
		Ext.Ajax.request({
				url: baseURL + "index.php/apotek/functionSetupTarif/hapus",
				params: {
					kd_prd: Ext.getCmp('txtKdPrd_MasterObatL').getValue()
				},
				failure: function (o) {
					loadMask.hide();
					ShowPesanWarningSetupTarif('Hubungi Admin', 'Error');
				},
				success: function (o) {
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) {
						loadMask.hide();
						ShowPesanInfoSetupTarif('Obat berhasil diHapus', 'Information');
						dataGriSetupTarifGolongan(rowdata.KD_GOL, rowdata.KD_JENIS, rowdata.KD_UNIT_TARIF, rowdata.KD_UNIT_FAR);
						setLookUps_viMasterObat.close();
					} else {
						loadMask.hide();
						ShowPesanWarningSetupTarif(cst.pesan, 'Error');
						dataGriSetupTarifGolongan(rowdata.KD_GOL, rowdata.KD_JENIS, rowdata.KD_UNIT_TARIF, rowdata.KD_UNIT_FAR);
						
					};
				}
			}

		)
	} else {
		loadMask.hide();
		ShowPesanWarningSetupTarif('Pilih obat yang akan diHapus', 'Error');
	}
}
function loadDataComboJenisObat(param) {
	if (param === '' || param === undefined) {
		param = {
			text: '0',
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/functionSetupTarif/getSubJenis",
		params: param,
		failure: function (o) {
			var cst = Ext.decode(o.responseText);
		},
		success: function (o) {
			ds_JenisObat.removeAll();
			var cst = Ext.decode(o.responseText);

			for (var i = 0, iLen = cst['listData'].length; i < iLen; i++) {
				var recs = [],
					recType = ds_JenisObat.recordType;
				var o = cst['listData'][i];

				recs.push(new recType(o));
				ds_JenisObat.add(recs);
				// console.log(o);
			}
		}
	});
}

function cbo_JenisObat() {
	var Field = ['KD_SUB_JNS', 'SUB_JENIS'];
	ds_JenisObat = new WebApp.DataStore({
		fields: Field
	});
	cboJenisObat = new Ext.form.ComboBox({
		id: 'cboJenisObat',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Jenis Obat',
		fieldLabel: 'Jenis Obat ',
		width: 180,
		store: ds_JenisObat,
		valueField: 'KD_SUB_JNS',
		displayField: 'SUB_JENIS',
		value: '',
		tabIndex: 4,
		listeners: {
			'select': function (a, b, c) {
				dkd_sub_jenis = b.data.KD_SUB_JNS;
				// Ext.getCmp('txttmpJenisObatL').setValue(b.data.KD_SUB_JNS);
			}
		}
	});
	return cboJenisObat;
};

function cbo_Faktor() {

	cboFaktor = new Ext.form.ComboBox({
		id: 'cboFaktor',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Faktor',
		fieldLabel: 'Faktor',
		width: 180,
		store			: new Ext.data.ArrayStore({
			id		: 0,
			fields	:['Id','displayText'],
			data	: [[0, 'Penjumlah'],[1, 'Pengali']]
		}),
		valueField: 'displayText',
		displayField: 'displayText',
		value: '',
		tabIndex: 4,
		listeners: {
			'select': function (a, b, c) {
				// Ext.getCmp('txttmpJenisObatL').setValue(b.data.KD_SUB_JNS);
			}
		}
	});
	return cboFaktor;
};
//=================================================
function PanelPencarianTarif_setupTarif(){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		height: 125,
		title:"Pencarian",
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 5px ',
						border: false,
						width: 500,
						height: 125,
						anchor: '100% 100%',
						items:
						[
							
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'Golongan'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							comboGolongan_SetupTarif(),
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'Jenis Tarif'
							},
							{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							comboJenisTarif_SetupTarif(),
							{
								x: 10,
								y: 60,
								xtype: 'label',
								text: 'Unit'
							},
							{
								x: 120,
								y: 60,
								xtype: 'label',
								text: ':'
							},
							comboUnit_setupTarif(),
							{
								x: 350,
								y: 60,
								xtype: 'button',
								text: 'Cari',
								iconCls: 'find',
								width: 70,
								id: 'btnCari_viSetupTarif',
								handler: function()
								{
									cGol = Ext.getCmp('cbogolongan_setupTarif').getValue();
									cJenis = Ext.getCmp('cbojenistarif_setupTarif').getValue();
									cUnit = Ext.getCmp('cboUnit_setupTarif').getValue();
									dataSource_viSetupTarif.removeAll();
									dataGriSetupTarif(cGol,cJenis,cUnit);
									AddNewSetupTarif(true);
									Ext.getCmp('btnAddRowTarif_viSetupTarif').disable();
									Ext.getCmp('btnSimpan_viSetupTarif').enable();
									Ext.getCmp('btnDelete_viSetupTarif').enable();
								}
							},
						]
					}
				]
			} 
							
		]		
	};
        return items;
}

function comboGolongan_SetupTarif()
{
 var Field = ['kd_gol','ket_gol'];
    dscombogolongan_setupTarif = new WebApp.DataStore({ fields: Field });
	loaddatacombogolongan_setupTarif();
	cbogolongan_setupTarif= new Ext.form.ComboBox
	(
		{
			id				: 'cbogolongan_setupTarif',
			x				: 130,
			y				: 0,
			typeAhead		: true,
			triggerAction	: 'all',
			lazyRender		: true,
			mode			: 'local',
			align			: 'Right',
			width			: 200,
			store			: dscombogolongan_setupTarif,
			valueField		: 'kd_gol',
			displayField	: 'ket_gol',
			emptyText			: 'SEMUA',
			listeners		:
			{
				select	: function(a,b,c){
					
				},
				keyUp: function(a,b,c){
					$this1=this;
					if((b.getKey()>=65 && b.getKey()<=90)|| (b.getKey()>=48 && b.getKey()<=57) || b.getKey()==32 || b.getKey()==8 || b.getKey()==46){
						clearTimeout(this.time);

						this.time=setTimeout(function(){
							if($this1.lastQuery != '' ){
								var value=$this1.lastQuery;
								var param={};
								param['text']=$this1.lastQuery;
								loaddatacombogolongan_setupTarif(param);
							}
						},1000);
					}
				}
			}
		}
	);return cbogolongan_setupTarif;
};

function comboJenisTarif_SetupTarif()
{
	var Field = ['kd_jenis','ket_jenis'];
    dscombojenistarif_setupTarif = new WebApp.DataStore({ fields: Field });
	loaddatacombojenistarif_setupTarif();
	cbojenistarif_setupTarif= new Ext.form.ComboBox
	(
		{
			id				: 'cbojenistarif_setupTarif',
			x				: 130,
			y				: 30,
			typeAhead		: true,
			triggerAction	: 'all',
			lazyRender		: true,
			mode			: 'local',
			align			: 'Right',
			width			: 200,
			store			: dscombojenistarif_setupTarif,
			valueField		: 'kd_jenis',
			displayField	: 'ket_jenis',
			emptyText			: 'SEMUA',
			listeners		:
			{
				select	: function(a,b,c){
					
				},
				keyUp: function(a,b,c){
					$this1=this;
					if((b.getKey()>=65 && b.getKey()<=90)|| (b.getKey()>=48 && b.getKey()<=57) || b.getKey()==32 || b.getKey()==8 || b.getKey()==46){
						clearTimeout(this.time);

						this.time=setTimeout(function(){
							if($this1.lastQuery != '' ){
								var value=$this1.lastQuery;
								var param={};
								param['text']=$this1.lastQuery;
								loaddatacombojenistarif_setupTarif(param);
							}
						},1000);
					}
				}
			}
		}
	);return cbojenistarif_setupTarif;
};

function comboUnit_setupTarif()
{
    var cboUnit_setupTarif = new Ext.form.ComboBox
	(
		{
			id: 'cboUnit_setupTarif',
			x: 130,
			y: 60,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			tabIndex: 3,
			selectOnFocus: true,
			forceSelection	: true,
			width: 200,
			emptyText			: 'SEMUA',
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
					data: [[0, 'Rawat Jalan / UGD'], [1, 'Rawat Inap'], [2, 'Non Resep']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			listeners:
			{
				'select': function (a, b, c)
				{
					
				},
				'render': function (c)
				{
					
				}
			}
		}
	);
    return cboUnit_setupTarif;
};
//------------------end---------------------------------------------------------

function dataGriSetupTarif(gol,jenis,unit){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionSetupTarif/getItemGrid",
			params: {
				gol : gol,
				jenis : jenis,
				unit : unit
			},
			failure: function(o)
			{
				loadMask.hide()
				ShowPesanErrorSetupTarif('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dataSource_viSetupTarif.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dataSource_viSetupTarif.add(recs);
					
					
					
					GridDataView_viSetupTarif.getView().refresh();
				}
				else 
				{
					loadMask.hide()
					ShowPesanErrorSetupTarif('Gagal membaca data obat', 'Error');
				};
			}
		}
		
	)
	
}

function dataSave_viSetupTarif(){
	if (ValidasiSaveSetupTarif(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionSetupTarif/save",
				params: getParamSaveSetupTarif(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupTarif('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupTarif('Berhasil menyimpan data ini','Information');
						
						dataSource_viSetupTarif.removeAll();
						dataGriSetupTarif();
						Ext.getCmp('btnAddRowTarif_viSetupTarif').disable();
						Ext.getCmp('btnSimpan_viSetupTarif').disable();
						Ext.getCmp('btnDelete_viSetupTarif').disable();
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupTarif('Gagal menyimpan data ini', 'Error');
						dataSource_viSetupTarif.removeAll();
						dataGriSetupTarif();
					};
				}
			}
			
		)
	}
}

function dataDelete_viSetupTarif(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionSetupTarif/delete",
			params: getParamDeleteSetupTarif(),
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorSetupTarif('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					loadMask.hide();
					ShowPesanInfoSetupTarif('Berhasil menghapus data ini','Information');
					
					dataSource_viSetupTarif.removeAll();
					dataGriSetupTarif();
					
					Ext.getCmp('btnAddRowTarif_viSetupTarif').disable();
					Ext.getCmp('btnSimpan_viSetupTarif').disable();
					Ext.getCmp('btnDelete_viSetupTarif').disable();
				}
				else 
				{
					loadMask.hide();
					ShowPesanErrorSetupTarif('Gagal menghapus data ini', 'Error');
					dataSource_viSetupTarif.removeAll();
					dataGriSetupTarif();
				};
			}
		}
		
	)
}

function loaddatacombogolongan_setupTarif(param)
{
	if (param==='' || param===undefined)
	{
		param={
			text: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/functionSetupTarif/getComboGolongan",
		params: param,
		failure: function(o)
		{
			var cst = Ext.decode(o.responseText);
		},
		success: function(o) {
			cbogolongan_setupTarif.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dscombogolongan_setupTarif.recordType;
				var o=cst['listData'][i];

				recs.push(new recType(o));
				dscombogolongan_setupTarif.add(recs);
				//console.log(o);
			}
		}
	});
}

function loaddatagridcombogolongan_setupTarif(param)
{
	if (param==='' || param===undefined)
	{
		param={
			text: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/functionSetupTarif/getComboGolongan",
		params: param,
		failure: function(o)
		{
			var cst = Ext.decode(o.responseText);
		},
		success: function(o) {
			gridcbogolongan_setupTarif.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsgridcombogolongan_setupTarif.recordType;
				var o=cst['listData'][i];

				recs.push(new recType(o));
				dsgridcombogolongan_setupTarif.add(recs);
				//console.log(o);
			}
		}
	});
}

function loaddatacombojenistarif_setupTarif(param)
{
	if (param==='' || param===undefined)
	{
		param={
			text: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/functionSetupTarif/getComboJenisTarif",
		params: param,
		failure: function(o)
		{
			var cst = Ext.decode(o.responseText);
		},
		success: function(o) {
			cbojenistarif_setupTarif.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dscombojenistarif_setupTarif.recordType;
				var o=cst['listData'][i];

				recs.push(new recType(o));
				dscombojenistarif_setupTarif.add(recs);
				//console.log(o);
			}
		}
	});
}

function loaddatagridcombojenistarif_setupTarif(param)
{
	if (param==='' || param===undefined)
	{
		param={
			text: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/functionSetupTarif/getComboJenisTarif",
		params: param,
		failure: function(o)
		{
			var cst = Ext.decode(o.responseText);
		},
		success: function(o) {
			gridcbojenistarif_setupTarif.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsgridcombojenistarif_setupTarif.recordType;
				var o=cst['listData'][i];

				recs.push(new recType(o));
				dsgridcombojenistarif_setupTarif.add(recs);
				//console.log(o);
			}
		}
	});
}

function AddNewSetupTarif(set){
	Ext.getCmp('gridcbogolongan_setupTarif').setReadOnly(set);
	Ext.getCmp('gridcbojenistarif_setupTarif').setReadOnly(set);
	Ext.getCmp('gridcboUnit_setupTarif').setReadOnly(set);
	
};

function ValidasiButton(set){
	
};

function DataClearSetupTarif(){
	Ext.getCmp('cbogolongan_setupTarif').setValue('');
	Ext.getCmp('cbojenistarif_setupTarif').setValue('');
	Ext.getCmp('cboUnit_setupTarif').setValue('');
};

function getParamSaveSetupTarif(){
	var	params =
	{
		
	}
	params['jml']=dataSource_viSetupTarif.getCount();
	console.log(dataSource_viSetupTarif.data.items);
	for(var i = 0 ; i < dataSource_viSetupTarif.getCount();i++)
	{
		params['kd_gol-'+i]=dataSource_viSetupTarif.data.items[i].data.KD_GOL;
		params['kd_jenis-'+i]=dataSource_viSetupTarif.data.items[i].data.KD_JENIS
		params['unit-'+i]=dataSource_viSetupTarif.data.items[i].data.unit
		params['batashrg_bawah-'+i]=dataSource_viSetupTarif.data.items[i].data.BATASHRG_BAWAH
		params['batashrg_atas-'+i]=dataSource_viSetupTarif.data.items[i].data.BATASHRG_ATAS
		params['jumlah-'+i]=dataSource_viSetupTarif.data.items[i].data.JUMLAH
		params['faktor-'+i]=dataSource_viSetupTarif.data.items[i].data.faktor	
		params['urut-'+i]=dataSource_viSetupTarif.data.items[i].data.URUT
		params['kd_unit_far-'+i]=dataSource_viSetupTarif.data.items[i].data.KD_UNIT_FAR		
	}
   
    return params
};

function getParamDeleteSetupTarif(){
	var	params =
	{
		kd_gol : dGol,
		kd_jenis : dJenis,
		kd_unit_tarif : dUnit,
		kd_unit_far : dKdUnitFar,
		urut : dUrut
	}
   
    return params
};

function ValidasiSaveSetupTarif(modul,mBolHapus){
	var x = 1;
	console.log(dataSource_viSetupTarif);
	if(dataSource_viSetupTarif.getCount() <= 0){
		loadMask.hide();
		ShowPesanWarningSetupTarif('Jumlah baris minimal 1', 'Warning');
		x = 0;
	} else{
		for(var i=0; i<dataSource_viSetupTarif.getCount() ; i++){
			var o=dataSource_viSetupTarif.getRange()[i].data;
			if(o.ket_gol == undefined || o.ket_gol == ''){
				loadMask.hide();
				ShowPesanWarningSetupTarif('Golongan tidak boleh kosong', 'Warning');
				x = 0;
			}
			if(o.ket_jenis == undefined || o.ket_jenis == ''){
				loadMask.hide();
				ShowPesanWarningSetupTarif('Jenis tarif tidak boleh kosong', 'Warning');
				x = 0;
			}
			if(o.unit == undefined || o.unit == ''){
				loadMask.hide();
				ShowPesanWarningSetupTarif('Unit tidak boleh kosong', 'Warning');
				x = 0;
			}
			if(o.JUMLAH == undefined || o.JUMLAH == 0){
				loadMask.hide();
				ShowPesanWarningSetupTarif('Jumlah tidak boleh kosong', 'Warning');
				x = 0;
			}
			if(o.faktor == undefined || o.faktor == ''){
				loadMask.hide();
				ShowPesanWarningSetupTarif('Faktor tidak boleh kosong', 'Warning');
				x = 0;
			}
		}
	}
	
	return x;
};



function ShowPesanWarningSetupTarif(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorSetupTarif(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoSetupTarif(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};