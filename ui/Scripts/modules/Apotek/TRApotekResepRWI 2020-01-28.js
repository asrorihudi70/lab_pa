// Data Source ExtJS # --------------

/**
*	Nama File 		: TRApotekResepRWI.js
*	Menu 			: APOTEK
*	Model id 		: 
*	Keterangan 		: Resep RWI
*	Di buat tanggal : 04 Juni 2015
*	Oleh 			: M
*	Edit			: AGUNG
*/
var default_nilai_adm_racik = 0;
var cbopasienorder_printer;
var tampung_ceklis_obat;
var jml_tampung_ceklis_obat;
var tampung_waktu_dosis_obat;
var jenis_obat='';
var jenis_etiket='';
var qty_default=1;
/*----------------------------*/
var statusRacikan_ResepRWI=0;

var KdFormResepRWI=8; // kd_form RESEP RWI
var dsprinter_rwi;
var cbopasienorder_printer_rwi;
var dataSource_viApotekResepRWI;
var selectCount_viApotekResepRWI=50;
var NamaForm_viApotekResepRWI="Resep Rawat Inap ";
var selectCountStatusPostingApotekResepRWI='Semua';
var mod_name_viApotekResepRWI="viApotekResepRWI";
var now_viApotekResepRWI= new Date();
var addNew_viApotekResepRWI;
var rowSelected_viApotekResepRWI;
var setLookUps_viApotekResepRWI;
var mNoKunjungan_viApotekResepRWI='';
var selectSetUnit;
var cellSelecteddeskripsiRWI;
var tanggal = now_viApotekResepRWI.format("d/M/Y");
var tanggal_resep_sebernarnyaRWI = now_viApotekResepRWI.format("Y-M-d");
var jam = now_viApotekResepRWI.format("H/i/s");
var tampungshiftsekarang;
var tmpkriteria;
var gridDTLTRHistoryApotekRWI;
var cbopasienorder_mng_apotek_rwi;
var dsDataGrdJab_viApotekResepRWI= new Ext.data.ArrayStore({
	id: 0,
	fields: ['jml_order','jumlah','takaran','result','signa','racikan_text','kd_prd','kd_satuan','nama_obat','jml','disc','kd_satuan','harga_jual','harga_beli','racikan','kd_pabrik','markup','tuslah','adm_racik','dosis','jasa','no_out','no_urut','aturan_racik','aturan_pakai','no_racik','id_mrresep'],
	data: []
});
var setLookUpApotek_TransferResepRWI;
var kd_pasien_obbt_rwi;
var kd_unit_obbt_rwi;
var tgl_masuk_obbt_rwi;
var urut_masuk_obbt_rwi; 
var GridDataViewOrderManagement_viApotekResepRWI;
var rowSelectedOrderManajemen_viApotekResepRWI;
var dataSourceGridOrder_viApotekResepRWI;
var CurrentGridOrderRWI;
var detailorderrwi=false;
var CurrentIdMrResepRwi='';
var kd_spesial_tr;
var	kd_unit_kamar_tr;
var	no_kamar_tr;
var ordermanajemenrwi=false;
var integrated;
var UnitFarAktif_ResepRWI;
var CurrentObat_viApotekResepRWI;
var CellSelected_viApotekResepRWI;
var currentKdPrdRacik;
var currentNamaObatRacik;
var currentHargaRacik;
var currentJumlah;
var curentIndexsSelection;
var PrintBillResepRWI;
var gridPanelFormulasiCito_ResepRWI;
var currentHargaJualObatResepRWI;
var currentCitoNamaObatResepRWI;
var currentCitoKdPrdResepRWI;
var currentCitoTarifLamaResepRWI;
var currentCitoTarifBaruResepRWI=0;
var cellSelectedGridCito_ResepRWI;
var CurrentDataGridCito_ResepRWI;
var dsGridTakaranDosisObat_ResepRWI;
var dsGridJamDosisObat_ResepRWI;
var tmp_kd_spesial;
var currentRowSelectionPencarianObatResepRWI;
var GridListPencarianObatColumnModel;
var PencarianLookupResepRWI; // Variabel pembeda getdata u/ pencarian nama obat dan semua kepemilikan atau bukan. False => pencarian berdasarkan nama_obat dan kepemilikan, True => pencarian berdasarkan nama_obat saja
var FocusExitResepRWI=false;
var jumlah_tuslah = 0;

var CurrentHistoryRWI =
{
    data: Object,
    details: Array,
    row: 0
};

var CurrentData_viApotekResepRWI =
{
	data: Object,
	details: Array,
	row: 0
};

var CurrentDataOrderManajemen_viApotekResepRWI = 
{
	data: Object,
	details: Array,
	row: 0
};


var dspasienorder_mng_apotek_rwi;
CurrentPage.page = dataGrid_viApotekResepRWI(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

var ResepRWI={};
ResepRWI.form={};
ResepRWI.func={};
ResepRWI.vars={};
ResepRWI.func.parent=ResepRWI;
ResepRWI.form.ArrayStore={};
ResepRWI.form.ComboBox={};
ResepRWI.form.DataStore={};
ResepRWI.form.Record={};
ResepRWI.form.Form={};
ResepRWI.form.Grid={};
ResepRWI.form.Panel={};
ResepRWI.form.TextField={};
ResepRWI.form.Button={};


ResepRWI.form.ArrayStore.kodepasien=new Ext.data.ArrayStore({
		id: 0,
		fields: [	
					'kd_kasir','no_transaksi','no_kamar','kd_pasien','nama',
					'nama_keluarga','jenis_kelamin','nama_unit','kd_unit',
					'nama_kamar', 'kd_dokter', 'kd_customer','kd_spesial',
					'kd_unit_kamar','urut_masuk','nama_dokter','nama_unit','customer','no_sjp','telepon',
					'kd_pay','payment','jenis_pay','payment_type'
				],
		data: []
	});

ResepRWI.form.ArrayStore.pasien=new Ext.data.ArrayStore({
		id: 0,
		fields: [	
					'kd_kasir','no_transaksi','no_kamar','kd_pasien','nama',
					'nama_keluarga','jenis_kelamin','nama_unit','kd_unit',
					'nama_kamar', 'kd_dokter', 'kd_customer','kd_spesial',
					'kd_unit_kamar','urut_masuk','nama_dokter','nama_unit','customer','no_sjp','telepon',
					'kd_pay','payment','jenis_pay','payment_type'
				],
		data: []
	});

ResepRWI.form.ArrayStore.a	= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_prd','kd_satuan','nama_obat','kd_sat_besar','harga_jual','harga_beli','kd_pabrik','markup','tuslah','adm_racik','milik'],
	data: []
});

function dataGrid_viApotekResepRWI(mod_id_viApotekResepRWI){	
    // Field kiriman dari Project Net.
    var FieldMaster_viApotekResepRWI = 
	[
		 'STATUS_POSTING','NO_RESEP','NO_OUT','TGL_OUT', 'KD_PASIENAPT', 'NMPASIEN', 'DOKTER', 
		'NAMA_DOKTER', 'KD_UNIT', 'NAMA_UNIT', 'APT_NO_TRANSAKSI','TGL_TRANSAKSI',
		'APT_KD_KASIR', 'KD_CUSTOMER','ADMRACIK','JUMLAH','JML_TERIMA_UANG','SISA','ADMPRHS',
		'JASA','ADMRESEP','NO_KAMAR','NAMA_KAMAR','CUSTOMER','JENIS_PASIEN','TGL_MASUK','URUT_MASUK',
		'KD_SPESIAL','SPESIALISASI','KD_UNIT_KAMAR','TGL_RESEP','TELEPON','CATATANDR',
		'NO_SJP','KD_PAY','JENIS_PAY','PAYMENT','PAYMENT_TYPE','ID_MRRESEP','JAM','CHECK_TUSLAH'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viApotekResepRWI = new WebApp.DataStore
	({
        fields: FieldMaster_viApotekResepRWI
    });
	
    refreshRespApotekRWI();
	total_pasien_order_mng_obtrwi();
	total_pasien_dilayani_order_mng_obtrwi();
	viewGridOrderAll_RASEPRWI();
	loadDataKodePasienResepRWI();
	getUnitFar_ResepRWI();
	var GridDataView_viApotekResepRWI = new Ext.grid.EditorGridPanel({
		xtype: 'editorgrid',
		store: dataSource_viApotekResepRWI,
		autoScroll: true,
		style:'padding: 4px 0px;',
		columnLines: true,
		flex:1.5,
		plugins: [new Ext.ux.grid.FilterRow()],
		selModel: new Ext.grid.RowSelectionModel({
			singleSelect: true,
			listeners:{
				rowselect: function(sm, row, rec){
					rowSelected_viApotekResepRWI = undefined;
					rowSelected_viApotekResepRWI = dataSource_viApotekResepRWI.getAt(row);
					CurrentData_viApotekResepRWI
					CurrentData_viApotekResepRWI.row = row;
					CurrentData_viApotekResepRWI.data = rowSelected_viApotekResepRWI.data;
					console.log(rowSelected_viApotekResepRWI.data);
					tmp_kd_spesial = rowSelected_viApotekResepRWI.data.KD_SPESIAL;
					console.log(tmp_kd_spesial);
					if (rowSelected_viApotekResepRWI.data.STATUS_POSTING==='0'){
						Ext.getCmp('btnHapusTrx_viApotekResepRWI').enable();
					}else{
						Ext.getCmp('btnHapusTrx_viApotekResepRWI').disable();
					}
					
				}
			}
		}),
		listeners:{
			rowdblclick: function (sm, ridx, cidx){
				rowSelected_viApotekResepRWI = dataSource_viApotekResepRWI.getAt(ridx);
				if (rowSelected_viApotekResepRWI != undefined){
					setLookUp_viApotekResepRWI(rowSelected_viApotekResepRWI.data);
				}else{
					setLookUp_viApotekResepRWI();
				}
			}
		},
		colModel: new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(),
			{
				header		: 'Status Posting',
				width		: 20,
				sortable	: false,
				hideable	: true,
				hidden		: false,
				menuDisabled: true,
				dataIndex	: 'STATUS_POSTING',
				id			: 'colStatusPosting_viApotekResepRWI',
				renderer	: function(value, metaData, record, rowIndex, colIndex, store){
					 switch (value){
						 case '1':
							 metaData.css = 'StatusHijau'; 
							 break;
						 case '0':
							 metaData.css = 'StatusMerah';
							 break;
					 }
					 return '';
				}
			},{
				id: 'colNoout_viApotekResepRWI',
				header: 'No Out',
				dataIndex: 'NO_OUT',
				sortable: true,
				width: 20,
				// hidden:true
			},{
				id: 'colTgl_viApotekResepRWI',
				header:'Tgl Resep',
				dataIndex: 'TGL_OUT',						
				width: 30,
				sortable: true,
				hideable:false,
				menuDisabled:true,
				// format: 'd/M/Y',
				//filter: {},
				renderer: function(v, params, record)
				{
					return ShowDate(record.data.TGL_OUT);
				}
			},{
				id: 'colTgl_viApotekResepRWI',
				header:'Jam',
				dataIndex: 'JAM',						
				width: 25,
				sortable: true,
				hideable:false,
				menuDisabled:true,
			},{
				id: 'colNoMedrec_viApotekResepRWI',
				header: 'No. Resep',
				dataIndex: 'NO_RESEP',
				sortable: true,
				width: 35
				
			},{
				id: 'colNoMedrec_viApotekResepRWI',
				header: 'No Medrec',
				dataIndex: 'KD_PASIENAPT',
				sortable: true,
				hideable:false,
				menuDisabled:true,
				width: 30
			},{
				id: 'colNamaPasien_viApotekResepRWI',
				header: 'Nama Pasien',
				dataIndex: 'NMPASIEN',
				sortable: true,
				width: 50
			},{
				id: 'colUnit_viApotekResepRWI',
				header: 'Ruangan',
				dataIndex: 'NAMA_UNIT',
				sortable: true,
				width: 40
			},
		]),
		tbar:{
			xtype: 'toolbar',
			id: 'toolbar_viApotekResepRWI',
			items:[
				{
					xtype: 'button',
					text: 'Tambah Resep[F1]',
					iconCls: 'Edit_Tr',
					tooltip: 'Add Data',
					id: 'btnTambah_viApotekResepRWI',
					handler: function(sm, row, rec){
						CurrentIdMrResepRwi = "";
						dsDataGrdJab_viApotekResepRWI.removeAll();
						Ext.Ajax.request({
							url: baseURL + "index.php/apotek/currentShiftAPT/getcurrentshift",
							params: {
								command: '0'
							},
							failure: function(o){
								var cst = Ext.decode(o.responseText);
							},	    
							success: function(o) {
								var cst = Ext.decode(o.responseText);
								tampungshiftsekarang=cst.shift;
								ResepRWI.form.Panel.shift.update(cst.shift);
							}	
						});
						cekPeriodeBulanRwi();
						Ext.getCmp('txtTmpStatusPostResepRWIL').setValue(0);
					}
				},{
					xtype: 'button',
					text: 'Edit Resep',
					iconCls: 'Edit_Tr',
					tooltip: 'Edit Data',
					id: 'btnEdit_viApotekResepRWI',
					handler: function(sm, row, rec){
						Ext.Ajax.request({
							url: baseURL + "index.php/apotek/currentShiftAPT/getcurrentshift",
							params: {
								command: '0'
							},
							failure: function(o){
								 var cst = Ext.decode(o.responseText);
							},	    
							success: function(o) {
								var cst = Ext.decode(o.responseText);
								tampungshiftsekarang=cst.shift
							}	
						});
						if (rowSelected_viApotekResepRWI != undefined){
							setLookUp_viApotekResepRWI(rowSelected_viApotekResepRWI.data)
						}
					}
				},{
					xtype: 'button',
					text: 'Hapus Transaksi',
					iconCls: 'remove',
					tooltip: 'Hapus Data',
					disabled:true,
					id: 'btnHapusTrx_viApotekResepRWI',
					handler: function(sm, row, rec){
						var datanya=rowSelected_viApotekResepRWI.data;
						console.log(datanya);
						if (datanya===undefined){
							ShowPesanWarningResepRWI('Belum ada data yang dipilih','Resep RWI');
						}else{
							Ext.Msg.show({
								title: 'Hapus Transaksi',
								msg: 'Anda yakin akan menghapus data transaksi ini ?',
								buttons: Ext.MessageBox.YESNO,
								fn: function (btn) {
									if (btn == 'yes'){
										var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan Pembatalan Transaksi:', function (btn, combo) {
											if (btn == 'ok'){
												var variablebatalhistori_rwi = combo;
												if (variablebatalhistori_rwi != ''){
													Ext.Ajax.request({
														url: baseURL + "index.php/apotek/functionAPOTEK/hapusTrxResep",
														params: {
															noout: datanya.NO_OUT,
															tglout: datanya.TGL_OUT,
															kdcustomer:datanya.KD_CUSTOMER,
															namacustomer:datanya.CUSTOMER,
															kdunit:datanya.KD_UNIT,
															jenis:'RESEP',
															apaini:"reseprwi",
															alasan: variablebatalhistori_rwi,
															id_mrresep:datanya.ID_MRRESEP
														},
														failure: function(o){
															var cst = Ext.decode(o.responseText);
															ShowPesanErrorResepRWI('Data transaksi tidak dapat dihapus','Resep RWI');
														},	    
														success: function(o) {
															var cst = Ext.decode(o.responseText);
															if (cst.success===true){
																tmpkriteria = getCriteriaCariApotekResepRWI();
																refreshRespApotekRWI(tmpkriteria);
																ShowPesanInfoResepRWI('Data transaksi Berhasil dihapus','Resep RWI');
																Ext.getCmp('btnHapusTrx_viApotekResepRWI').disable();
																viewGridOrderAll_RASEPRWI();
															}else{
																ShowPesanErrorResepRWI('Data transaksi tidak dapat dihapus. '+ cst.pesan,'Resep RWI');
															}
														}
													});
												} else{
													ShowPesanWarningResepRWI('Silahkan isi alasan terlebih dahaulu', 'Keterangan');
												}
											}
										});
									}
								},
								icon: Ext.MessageBox.QUESTION
							});
						}
					}
				},
				/* {xtype: 'tbspacer',height: 3, width:580},
				{
					xtype: 'label',
					text: 'Order Rawat Inap : ' 
				},
				{xtype: 'tbspacer',height: 3, width:5},
				{
					x: 40,
					y: 40,
					xtype: 'textfield',
					fieldLabel: 'No. Medrec',
					name: 'txtcounttr_apt_rwi',
					id: 'txtcounttr_apt_rwi',
					width: 50,
					disabled:true,
					listeners: 
					{ 
						
					}
				} */
				//-------------- ## --------------
			]
		},
		//bbar : bbar_paging(mod_name_viApotekResepRWI, selectCount_viApotekResepRWI, dataSource_viApotekResepRWI),
		viewConfig:{
			forceFit: true
		}
	});
	
	var FieldOrderManajemen_viApotekResepRWI =[
		'kd_pasien', 'nama', 'kd_unit', 'nama_unit','kd_unit_kamar','kd_spesial','no_kamar','kd_dokter','kd_customer','no_transaksi','kd_kasir','id_mrresep','urut_masuk','tgl_masuk','tgl_transaksi','nama_dokter','customer','spesialisasi'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
	dataSourceGridOrder_viApotekResepRWI = new WebApp.DataStore({
        fields: FieldOrderManajemen_viApotekResepRWI
    });
	GridDataViewOrderManagement_viApotekResepRWI = new Ext.grid.EditorGridPanel({
		title:'Order Obat Ruangan',
		store: dataSourceGridOrder_viApotekResepRWI,
		autoScroll: true,
		flex:1,
		columnLines: true,
		border: true,
		plugins: [new Ext.ux.grid.FilterRow()],
		selModel: new Ext.grid.RowSelectionModel({
			singleSelect: true,
			listeners:{
				rowselect: function(sm, row, rec){
					rowSelectedOrderManajemen_viApotekResepRWI = undefined;
					rowSelectedOrderManajemen_viApotekResepRWI = dataSourceGridOrder_viApotekResepRWI.getAt(row);
					CurrentDataOrderManajemen_viApotekResepRWI
					CurrentDataOrderManajemen_viApotekResepRWI.row = row;
					CurrentDataOrderManajemen_viApotekResepRWI.data = rowSelectedOrderManajemen_viApotekResepRWI.data;
					
				}
			}
		}),
		listeners:{
			rowdblclick: function (sm, ridx, cidx){
				rowSelectedOrderManajemen_viApotekResepRWI = dataSourceGridOrder_viApotekResepRWI.getAt(ridx);
				if (rowSelectedOrderManajemen_viApotekResepRWI != undefined){
					CurrentGridOrderRWI=CurrentDataOrderManajemen_viApotekResepRWI.data;
					console.log(CurrentGridOrderRWI);
					CurrentIdMrResepRwi=CurrentGridOrderRWI.id_mrresep;
					ordermanajemenrwi=true;
					Ext.Ajax.request({
						url: baseURL + "index.php/apotek/currentShiftAPT/getcurrentshift",
						params: {
							command: '0'
						},
						failure: function(o){
							var cst = Ext.decode(o.responseText);
						},	    
						success: function(o) {
							var cst = Ext.decode(o.responseText);
							tampungshiftsekarang=cst.shift;
							ResepRWI.form.Panel.shift.update(cst.shift);
						}	
					});
					detailorderrwi=true;
					cekPeriodeBulanRwi(detailorderrwi,CurrentGridOrderRWI);
				}
			}
		},
		colModel: new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(),
			{
				header: 'No Medrec',
				dataIndex: 'kd_pasien',
				sortable: false,
				hideable:false,
				menuDisabled:true,
				width: 30
			},{
				header: 'Nama',
				dataIndex: 'nama',
				sortable: false,
				width: 50
			},{
				header: 'Kelas - Kamar',
				dataIndex: 'nama_unit',
				sortable: false,
				hidden:true,
				width: 40
			},{
				header: 'Kelas - Kamar',
				dataIndex: 'kelas_kamar',
				sortable: false,
				width: 40
			},{
				header:'Tgl Order',
				dataIndex: 'tgl_order',						
				width: 30,
				sortable: false,
				hideable:false,
				menuDisabled:true,
				renderer: function(v, params, record){
					return ShowDate(record.data.tgl_order);
				}
			},{
				header: 'Status',
				dataIndex: 'order_mng',
				sortable: false,
				width: 40
			},
		]),
		tbar: [
			{
				xtype: 'label',
				text: 'Order obat ruangan : ',
				style : {
					color : 'red'
				}
			},
			{xtype: 'tbspacer',height: 3, width:5},
			{
				x: 40,
				y: 40,
				xtype: 'textfield',
				name: 'txtcounttr_apt_rwi',
				id: 'txtcounttr_apt_rwi',
				width: 50,
				disabled:true,
				style : {
					color : 'red'
				}
			},
			{xtype: 'tbspacer',height: 3, width:10},
			{
				xtype: 'label',
				text: 'Order obat telah dilayani : ',
				style : {
					color : 'red'
				}
			},
			{xtype: 'tbspacer',height: 3, width:5},
			{
				x: 40,
				y: 40,
				xtype: 'textfield',
				name: 'txtcounttrDilayani_apt_rwi',
				id: 'txtcounttrDilayani_apt_rwi',
				width: 50,
				disabled:true,
				style : {
					color : 'red'
				}
			},
			{xtype: 'tbspacer',height: 3, width:7},
			{
				xtype: 'button',
				text: 'Refresh',
				iconCls: 'refresh',
				// disabled:true,
				id: 'btnRefreshOrder_viApotekResepRWI',
				handler: function(){
					viewGridOrderAll_RASEPRWI();
					total_pasien_order_mng_obtrwi();
					total_pasien_dilayani_order_mng_obtrwi();
					ordermanajemenrwi=false;
				}
			},
			{xtype: 'tbspacer',height: 3, width:10},
			{
				xtype: 'tbseparator'
			},{
				xtype: 'label',
				text: 'Cari berdasarkan nama dan tanggal : '
			},
			{xtype: 'tbspacer',height: 3, width:5},
			{
				x: 40,
				y: 40,
				xtype: 'textfield',
				name: 'txtNamaOrder_viApotekResepRWI',
				id: 'txtNamaOrder_viApotekResepRWI',
				width: 120,
				// disabled:true,
				listeners: { 
					'specialkey' : function(){
						if (Ext.EventObject.getKey() === 13) {
							viewGridOrderAll_RASEPRWI(Ext.getCmp('txtNamaOrder_viApotekResepRWI').getValue(),Ext.getCmp('dfTglOrderApotekResepRWI').getValue());
						} 						
					}
				}
			},
			{xtype: 'tbspacer',height: 3, width:5},
			{
				xtype: 'datefield',
				id: 'dfTglOrderApotekResepRWI',
				format: 'd/M/Y',
				width: 100,
				tabIndex:3,
				// disabled:true,
				value:now_viApotekResepRWI,
				listeners:{ 
					'specialkey' : function(){
						if (Ext.EventObject.getKey() === 13){
							viewGridOrderAll_RASEPRWI(Ext.getCmp('txtNamaOrder_viApotekResepRWI').getValue(),Ext.getCmp('dfTglOrderApotekResepRWI').getValue());
						} 						
					}
				}
			},
			{xtype: 'tbspacer',height: 3, width:5},
			{
				xtype: 'label',
				text: '*) Enter untuk mencari'
			},
			{xtype: 'tbspacer',height: 3, width:580},
		],
		viewConfig:{
			forceFit: true
		}
	});
	
	var pencarianApotekResepRWI = new Ext.FormPanel({
        bodyStyle:'padding:4px;',
		height: 62,
        items: [
			{
				layout: 'column',
				border: false,
				items:[
					{
						layout: 'form',
						border: false,
						labelWidth: 60,
						items:[
							{
								xtype: 'textfield',
								fieldLabel:'No. Resep',
								id: 'TxtFilterGridDataView_RoNumber_viApotekResepRWI',
								name: 'TxtFilterGridDataView_RoNumber_viApotekResepRWI',
								emptyText: 'No. Resep',
								width: 130,
								tabIndex:1,
								listeners:{ 
									'specialkey' : function(){
										if (Ext.EventObject.getKey() === 13){
											tmpkriteria = getCriteriaCariApotekResepRWI();
											refreshRespApotekRWI(tmpkriteria);
										} 						
									}
								}
							},{
								xtype: 'textfield',
								fieldLabel:'Mr/Nama',
								id: 'txtKdNamaPasienResepRWI',
								name: 'txtKdNamaPasienResepRWI',
								emptyText: 'Kode/Nama Pasien',
								width: 130,
								tabIndex:1,
								listeners:{ 
									'specialkey' : function(){
										if (Ext.EventObject.getKey() === 13){
											tmpkriteria = getCriteriaCariApotekResepRWI();
											refreshRespApotekRWI(tmpkriteria);
										} 						
									}
								}
							},
						]
					},{
						layout: 'form',
						border: false,
						width: 550,
						style:'padding-left: 4px;',
						labelWidth: 60,
						items:[
							{
								layout:'column',
								border:false,
								items:[
									{
										layout: 'form',
										border: false,
										labelWidth: 60,
										items:ComboUnitApotekResepRWI()
									},{
										layout: 'form',
										border: false,
										style:'padding-left: 4px;',
										labelWidth: 80,
										items:{
											xtype: 'datefield',
											id: 'dfTglAwalApotekResepRWI',
											format: 'd/M/Y',
											width: 100,
											tabIndex:3,
											fieldLabel:'Tgl. Resep',
											value:now_viApotekResepRWI,
											listeners:{ 
												'specialkey' : function(){
													if (Ext.EventObject.getKey() === 13) {
														tmpkriteria = getCriteriaCariApotekResepRWI();
														refreshRespApotekRWI(tmpkriteria);
													} 						
												}
											}
										},
									},{
										xtype:'displayfield',
										value:'&nbsp;s/d&nbsp;'
									},{
										xtype: 'datefield',
										id: 'dfTglAkhirApotekResepRWI',
										format: 'd/M/Y',
										width: 100,
										tabIndex:4,
										value:now_viApotekResepRWI,
										listeners:{ 
											'specialkey' : function(){
												if (Ext.EventObject.getKey() === 13){
													tmpkriteria = getCriteriaCariApotekResepRWI();
													refreshRespApotekRWI(tmpkriteria);
												} 						
											}
										}
									},
								]
							},{
								layout:'column',
								border:false,
								items:[
									{
										layout: 'form',
										border: false,
										labelWidth: 60,
										items:mComboStatusPostingApotekResepRWI()
									},{
										layout: 'form',
										border: false,
										style:'padding-left: 4px;',
										labelWidth: 60,
										items:mComboJumlahDataApotekResepRWI()
									},	{
										xtype: 'displayfield',
										value: '&nbsp;&nbsp;*) Tekan enter untuk mencari resep'
									}
								]
							},{
								x: 568,
								y: 60,
								xtype: 'button',
								text: 'Cari Resep',
								iconCls: 'refresh',
								tooltip: 'Cari',
								style:{paddingLeft:'30px'},
								width:150,
								hidden : true,
								id: 'BtnFilterGridDataView_viApotekResepRWI',
								handler: function(){					
									tmpkriteria = getCriteriaCariApotekResepRWI();
									refreshRespApotekRWI(tmpkriteria);
								}                        
							}
						]
					}
				]
			}
		]	
	});
    var FrmFilterGridDataView_viApotekResepRWI = new Ext.Panel({
		title: NamaForm_viApotekResepRWI,
		id: mod_id_viApotekResepRWI,
		style:'padding:4px;',
		layout: {
			type:'vbox',
			align:'stretch'
		}, 
		closable: true,        
		items: [
			pencarianApotekResepRWI,
			GridDataView_viApotekResepRWI,
			GridDataViewOrderManagement_viApotekResepRWI
		]
	});
	return FrmFilterGridDataView_viApotekResepRWI;
}



function refreshRespApotekRWI(kriteria)
{
    dataSource_viApotekResepRWI.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'ViewResepApotekRWI',
                    param : kriteria
                }			
            }
        );   
    return dataSource_viApotekResepRWI;
}

function mComboJumlahDataApotekResepRWI(){
	var limit='';
	var cboJumlahDataApotekResepRWI = new Ext.form.ComboBox({
		id:'cboJumlahDataApotekResepRWI',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender:true,
		mode: 'local',
		width: 50,
		editable: false,
		emptyText:'',
		fieldLabel: 'Jumlah',
		tabIndex:5,
		store: new Ext.data.ArrayStore({
			id: 0,
			fields:[
				'Id',
				'displayText'
			],
			data: [[1, 5],[2,10], [3, 20],[4,25],[5,50],[6,100]]
		}),
		valueField: 'Id',
		displayField: 'displayText',
		value:1,
		listeners:{
			'select': function(a,b,c){
				tmpkriteria = getCriteriaCariApotekResepRWI();
				limit=tmpkriteria+" limit " + Ext.get('cboJumlahDataApotekResepRWI').getValue()+" ";
				
			},
			'specialkey' : function(){
				if (Ext.EventObject.getKey() === 13){
					refreshRespApotekRWI(limit);
				} 						
			}
		}
	});
	return cboJumlahDataApotekResepRWI;
}
function setLookUp_viApotekResepRWI(rowdata,order_poli){
    var lebar = 985;
    setLookUps_viApotekResepRWI = new Ext.Window({
        title: NamaForm_viApotekResepRWI, 
        closeAction: 'destroy', 
		maximized:true,		
        resizable:false,
		autoScroll: false,
        border: true,
		layout:'fit',
        constrainHeader : true,    
        iconCls: 'resep',
        modal: true,		
        items: getFormItemEntry_viApotekResepRWI(lebar,rowdata),
        listeners:{
            activate: function(){
				shortcuts();
				Ext.Ajax.request({  
					url: baseURL + "index.php/apotek/currentShiftAPT/getcurrentshift",
					params: {
						command: '0'
					},
					failure: function(o){
						var cst = Ext.decode(o.responseText);
					},	    
					success: function(o) {
						var cst = Ext.decode(o.responseText);
						tampungshiftsekarang=cst.shift
						ResepRWI.form.Panel.shift.update(cst.shift);
					}	
				});
				Ext.Ajax.request({				   
					url: baseURL + "index.php/apotek/functionAPOTEKrwi/getModelHitungRacik",
					params: {
						text: '0'
					},
					failure: function(o){
						var cst = Ext.decode(o.responseText);
					},	    
					success: function(o) {
						var cst = Ext.decode(o.responseText);
						Ext.getCmp('txtModelRacik_viApotekResepRWI').setValue(cst.model);
					}
				});
            },
            afterShow: function(){
                this.activate();
            },
            deactivate: function(){
                rowSelected_viApotekResepRWI=undefined;
                //datarefresh_viApotekResepRWI();
				mNoKunjungan_viApotekResepRWI = '';
				shortcut.remove('lookup');
            },
			close: function (){
				shortcut.remove('lookup');
			},
        }
    });
    setLookUps_viApotekResepRWI.show();
    if (rowdata == undefined){
		Ext.getCmp('btnAddObatRWI').enable();
		getDefaultCustomer();
		Ext.getCmp('txtKdPasienResepRWI_viApotekResepRWI').focus(true,10);
    }else{
		if(order_poli != undefined){
			viewDetailGridOrderRWI(order_poli);
		}else{
			datainit_viApotekResepRWI(rowdata);
			ViewDetailPembayaranObatRWI(rowdata.NO_OUT,rowdata.TGL_OUT);
		}
		if(rowdata.STATUS_POSTING=='0'){
			Ext.getCmp('btnTransfer_viApotekResepRWI').enable();
			Ext.getCmp('btnBayar_viApotekResepRWI').enable();
			Ext.getCmp('btnSimpan_viApotekResepRWI').enable();
			// Ext.getCmp('btnSimpanExit_viApotekResepRWJ').enable();
		}else{
			Ext.getCmp('btnTransfer_viApotekResepRWI').disable();
			Ext.getCmp('btnBayar_viApotekResepRWI').disable();
			Ext.getCmp('btnSimpan_viApotekResepRWI').disable();
			// Ext.getCmp('btnSimpanExit_viApotekResepRWJ').disable();
		}
    }
	shortcuts();
}

function getDefaultCustomer(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEK/getDefaultCustomer",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').setValue(cst.kd_customer);
				Ext.getCmp('cboPilihankelompokPasienAptResepRWI').setValue(cst.customer);
				
			}
		}
		
	)
	
}
function shortcuts(){
	shortcut.set({
		code:'lookup',
		list:[
			{
				key:'f8',
				fn:function(){
					Ext.getCmp('btnAdd_viApotekResepRWI').el.dom.click();
				}
			},
			{
				key:'ctrl+s',
				fn:function(){
					Ext.getCmp('btnSimpan_viApotekResepRWI').el.dom.click();
				}
			},
			{
				key:'ctrl+d',
				fn:function(){
					Ext.getCmp('btnDelete_viApotekResepRWI').el.dom.click();
				}
			},
			{
				key:'f9',
				fn:function(){
					Ext.getCmp('btnBayar_viApotekResepRWI').el.dom.click();
				}
			},
			{
				key:'f10',
				fn:function(){
					Ext.getCmp('btnTransfer_viApotekResepRWI').el.dom.click();
				}
			},
			{
				key:'f6',
				fn:function(){
					Ext.getCmp('btnunposting_viApotekResepRWI').el.dom.click();
				}
			},
			{
				key:'f12',
				fn:function(){
					// Ext.getCmp('btnPrintBillResepRWJ').el.dom.click();
					if (Ext.getCmp('txtTmpStatusPostResepRWIL').getValue() == 1){
						Ext.getCmp('btnPrintBillResepRWI').handler();	
					}else{
						ShowPesanWarningResepRWI('Pembayaran belum dilakukan, tidak dapat cetak bill!','Warning');
					}
				}
			},
			{
				key:'f11',
				fn:function(){
					// Ext.getCmp('btnPrintKwitansiResepRWI').handler();
					Ext.Ajax.request({   
						url: baseURL + "index.php/apotek/functionAPOTEKrwi/getjenispembayaran",
						 params: {
							no_out:Ext.getCmp('txtTmpNooutResepRWIL').getValue(),
							tgl_out:Ext.getCmp('txtTmpTgloutResepRWIL').getValue()
						},
						failure: function(o)
						{
							 var cst = Ext.decode(o.responseText);
							
						},	    
						success: function(o) {
							var cst = Ext.decode(o.responseText);
							if(cst.jenis_pay == 'TU'){
								Ext.getCmp('btnPrintKwitansiResepRWI').handler();
							}else{
								ShowPesanWarningResepRWI('Pembayaran bukan TUNAI, kwitansi tidak dapat dicetak!','Warning');
							}
						}	
					
					});
				}
			},
			{
				key:'ctrl+o',
				fn:function(){
					// Ext.getCmp('btnPrintLabelDosisObatResepRWI').el.dom.click();
					Ext.getCmp('btnPrintLabelDosisObatResepRWI').handler();
				}
			},
			{
				key:'esc',
				fn:function(){
					setLookUps_viApotekResepRWI.close();
				}
			}
		]
	});
} 
function getFormItemEntry_viApotekResepRWI(lebar,rowdata){
	ResepRWI.form.Panel.a = {
						xtype: 'displayfield',				
						width: 15,								
						html: '<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>',
						fieldLabel: 'Label',
						style:{'text-align':'left','margin-left':'0px'}
					};
    var pnlFormDataBasic_viApotekResepRWI = new Ext.FormPanel({
		layout: {
			type:'vbox',
			align:'stretch'
		},
		bodyStyle: 'padding:4px;',
		border: false,
		items:[
				getItemPanelInputBiodata_viApotekResepRWI(lebar),
				getItemGridTransaksi_viApotekResepRWI(lebar),
				getItemGridHistoryBayar_viApotekResepRWI(lebar),
				{
					layout	: 'form',
					style:'padding-top: 4px;',
					bodyStyle: 'padding: 4px;',
					items	: [
						{
							xtype: 'compositefield',
							fieldLabel: ' ',
							labelSeparator: '',
							items: [
								ResepRWI.form.Panel.a,
								{
									columnWidth	: .08,
									layout		: 'form',
									anchor		: '100% 8.0001%',
									style		: {'margin-top':'1px'},
									border		: true,
									html		: " Status Posting"
								},{
									xtype: 'displayfield',				
									width: 100,								
									value: 'Model  Racik :',
									fieldLabel: 'Label',
									style:{'text-align':'right','margin-left':'100px'}							
								},{
									xtype: 'displayfield',
									id: 'txtModelRacik_viApotekResepRWI',
									name: 'txtModelRacik_viApotekResepRWI',
									style:{'text-align':'left','margin-left':'100px'},
									width: 40
								},{
									xtype	: 'displayfield',		
									width 	: 150,								
									value 	: 'Tuslah + Embalase :',
									style 	:{'text-align':'right','margin-left':'58px'}							
								},{
						            xtype: 'textfield',
						            id: 'txtTuslahEmbalaseL',
									style:{'text-align':'right','margin-left':'58px'},
						            width: 100,
						            value: 0,
						            readOnly: true
						        },{
									xtype: 'checkbox',
									id: 'Chck_tuslah_viApotekResepRWI',
									name: 'Chck_tuslah_viApotekResepRWI',
									style:{'text-align':'right','margin-left':'60px'},
									checked : true,
									listeners: {
										check: function(a, b){
											var _total 	= Ext.getCmp('txtTotalBayarResepRWIL').getValue();
											_total = _total.replace(/,/g, "");
											console.log(_total);
											if (b === true) {
												Ext.getCmp('txtTuslahEmbalaseL').setValue(rowdata.JASA);
												_total 	= (parseInt(_total) + parseInt(rowdata.JASA));
											}else{
												Ext.getCmp('txtTuslahEmbalaseL').setValue('0.00');
												_total 	= (parseInt(_total) - parseInt(rowdata.JASA));
											}
											// console.log(_total);
											Ext.getCmp('txtTotalBayarResepRWIL').setValue(toFormat(_total));
											// hitungSetengahResep();
										}
									}
								},{
									xtype: 'displayfield',				
									width: 74,								
									value: 'Adm Racik :',
									style:{'text-align':'right','margin-left':'0px'}
								},{
						            xtype: 'textfield',
						            id: 'txtAdmRacikResepRWIL',
									style:{'text-align':'right','margin-left':'0px'},
						            width: 100,
						            value: 0,
						            readOnly: true
						        },{
									xtype 	: 'checkbox',
									id 		: 'Chck_racik_viApotekResepRWI',
									name 	: 'Chck_racik_viApotekResepRWI',
									style:{'text-align':'right','margin-left':'0px'},
									checked : true,
									listeners: {
										check: function(a, b){
											if (b === true) {
												Ext.getCmp('txtAdmRacikResepRWIL').setValue(rowdata.ADMRACIK);
											}else{
												Ext.getCmp('txtAdmRacikResepRWIL').setValue('0.00');
											}
											// hitungSetengahResep();
										}
									}
								},{
									xtype: 'displayfield',				
									width: 90,								
									value: 'Total :',
									style:{'text-align':'right','margin-left':'0px'}
								},{
						            xtype: 'textfield',
						            id: 'txtJumlahTotalResepRWIL',
									style:{'text-align':'right','margin-left':'5px'},
						            width: 110,
						            value: 0,
						            readOnly: true
						        }												
						    ]
						},{
							xtype: 'compositefield',
							items: [
								{ 
									xtype: 'displayfield',				
									width: 80,								
									value: 'Tanggal :',
									style:{'text-align':'right'}
								},{ 
									xtype: 'displayfield',
									id		: '',
									width: 100,								
									value: tanggal,
									format:'d/M/Y'
								},
								
								{
									xtype: 'displayfield',				
									width: 120,								
									value: 'Jumlah Grup Racik :',
									style:{'text-align':'right'}							
								},						 
								{
									xtype: 'displayfield',
									id: 'txtJumlahGrupRacik_viApotekResepRWI',
									name: 'txtJumlahGrupRacik_viApotekResepRWI',
									style:{'text-align':'left'},
									width: 40,
									value: 0,
									readOnly: true
								},
								{
									xtype: 'displayfield',				
									width: 50,								
									value: 'Adm :',
									style:{'text-align':'right','margin-left':'55px'}
									
								},{
						            xtype: 'numberfield',
						            id: 'txtAdmResepRWIL',
									style:{'text-align':'right','margin-left':'55px'},
						            width: 100,
						            value: 0,
						            readOnly: true
						        },{
									xtype: 'displayfield',				
									width: 100,								
									value: 'Disc :',
									style:{'text-align':'right','margin-left':'55px'}
									
								},{
						            xtype: 'numberfield',
						            id: 'txtTotalDiscResepRWIL',
									style:{'text-align':'right','margin-left':'55px'},
						            width: 100,
						            value: 0,
						            readOnly: true
						        },{
									xtype: 'displayfield',				
									width: 90,								
									value: 'Grand Total :',
									style:{'text-align':'right','margin-left':'90px','font-weight':'bold'}
									
								},{
						            xtype: 'textfield',
						            id: 'txtTotalBayarResepRWIL',
									style:{'text-align':'right','margin-left':'90px'},
						            width: 110,
						            value: 0,
						            readOnly: true
						        }
						    ]
						},{
							xtype: 'compositefield',
							items: [
								{ 
									xtype: 'displayfield',				
									width: 80,								
									value: 'Current Shift :',
									style:{'text-align':'right'}
								},ResepRWI.form.Panel.shift=new Ext.Panel ({
									region: 'north',
									border: false
								}),{
									xtype: 'displayfield',				
									width: 150,								
									value: 'Adm Perusahaan :',
									style:{'text-align':'right','margin-left':'316px'}
									
								},{
						            xtype: 'textfield',
						            id: 'txtAdmPrshResepRWIL',
									style:{'text-align':'right','margin-left':'471px'},
						            width: 100,
						            value: 0,
						            readOnly: true
						        }
						    ]
						}
		     	   ]
				}
			],
			fileUpload: true,
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Baru [F8]',
						iconCls: 'add',
						//hidden:true,
						id: 'btnAdd_viApotekResepRWI',
						handler: function(){
							CurrentIdMrResepRwi = '';
							dataaddnew_viApotekResepRWI();
						}
					},{
						xtype: 'tbseparator'
					},{
						xtype: 'button',
						text: 'Dilayani [CTRL+S]',
						iconCls: 'save',
						// disabled:true,
						id: 'btnSimpan_viApotekResepRWI',
						handler: function(){
							datasave_viApotekResepRWI();
						}
					},{
						xtype: 'button',
						text: 'Dilayani & Tutup',
						iconCls: 'saveexit',
						disabled:true,
						hidden:true,
						id: 'btnSimpanExit_viApotekResepRWI',
						handler: function()
						{
							/* var x = datasave_viApotekResepRWI(addNew_viApotekResepRWI);
							datarefresh_viApotekResepRWI();
							if (x===undefined)
							{
								setLookUps_viApotekResepRWI.close();
							} */
							datasave_viApotekResepRWI();
							refreshRespApotekRWI();
							setLookUps_viApotekResepRWI.close();
						}
					},'-',
					{
						xtype: 'button',
						text: 'Bayar [F9]',
						id: 'btnBayar_viApotekResepRWI',
						iconCls: 'gantidok',
						// disabled:true,
						handler: function(){
							if (ValidasiEntryResepRWI(nmHeaderSimpanData,false) == 1 ){
								Ext.Ajax.request({
									url: baseURL + "index.php/apotek/functionAPOTEKrwi/saveResepRWI",
									params: getParamResepRWI(),
									failure: function(o){
										ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
									},	
									success: function(o) {
										var cst = Ext.decode(o.responseText);
										if (cst.success === true) {
											Ext.get('txtNoResepApotekResepRWIL').dom.value=cst.noresep;
											Ext.get('txtTmpNooutResepRWIL').dom.value=cst.noout;
											// Ext.get('txtNoTrApotekResepRWIL').dom.value=cst.noout;
											Ext.get('txtTmpTgloutResepRWIL').dom.value=cst.tgl;
											Ext.getCmp('btnTransfer_viApotekResepRWI').disable();
											Ext.getCmp('btnSimpan_viApotekResepRWI').disable();
											// Ext.getCmp('btnSimpanExit_viApotekResepRWJ').disable();
											Ext.getCmp('btnunposting_viApotekResepRWI').enable();
											
											
											getGridDetailObatApotekResepRWI(cst.noout,cst.tgl,function(){
												var tmpNonResep = 0;
												// if(Ext.getCmp('cbNonResep').getValue() == false){
													// tmpNonResep = 1;
												// }else{
													// tmpNonResep = 0;
												// }
												var tmpNoresep='';
												if(Ext.getCmp('txtNoResepApotekResepRWIL').getValue() === '' || Ext.getCmp('txtNoResepApotekResepRWIL').getValue() === 'No Resep' ){
													LangsungPost = 1;
													tmpNoresep='';
												}else {
													LangsungPost = 0;
													tmpNoresep = Ext.getCmp('txtNoResepApotekResepRWIL').getValue();
												}
												var ubah=0;
												if(Ext.getCmp('txtNoResepApotekResepRWIL').getValue() === '' || Ext.getCmp('txtNoResepApotekResepRWIL').getValue()=='No Resep'){
													ubah=0;
												} else{
													ubah=1;
												}
												var parameter ={
													//NmPasien:rowSelected_viApotekResepRWJ_.data.NMPASIEN,		
													NmPasien:Ext.getCmp('txtNamaPasienResepRWI_viApotekResepRWI').getValue(),		
													//KdUnit:rowSelected_viApotekResepRWJ_.data.KD_UNIT,		
													KdUnit:Ext.getCmp('txtTmpKdUnitApotekResepRWIL').getValue(),		
													//KdDokter:rowSelected_viApotekResepRWJ_.data.DOKTER,	
													KdDokter:Ext.getCmp('txtTmpKdDokterApotekResepRWIL').getValue(),	
													NonResep:tmpNonResep,
													JamOut:jam,
													DiscountAll:toInteger(Ext.getCmp('txtTotalDiscResepRWIL').getValue()),
													AdmRacikAll:0,//toInteger(Ext.getCmp('txtAdmRacikEditData_viApotekResepRWJ').getValue()),
													// JasaTuslahAll:toInteger(Ext.getCmp('txtTuslahEmbalaseL').getValue()),
													JasaTuslahAll:toInteger(jumlah_tuslah),
													Adm:0,//toInteger(Ext.getCmp('txtAdmEditData_viApotekResepRWJ').getValue()),
													Admprsh:0,//toInteger(Ext.getCmp('txtAdmPerusahaanEditData_viApotekResepRWJ').getValue()),
													Kdcustomer:Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').getValue(),
													NoTransaksiAsal:ResepRWI.vars.no_transaksi,
													KdKasirAsal:ResepRWI.vars.kd_kasir,
													SubTotal:toInteger(Ext.getCmp('txtJumlahTotalResepRWIL').getValue()),
													Total:toInteger(Ext.getCmp('txtTotalBayarResepRWIL').getValue()),
													NoOut:Ext.getCmp('txtTmpNooutResepRWIL').getValue(),
													TglOut:Ext.getCmp('txtTmpTgloutResepRWIL').getValue(),
													
													//PembayaranKdPasienBayar:Ext.getCmp('txtkdPasien_Pembayaran').getValue(),
													//KdPasien:rowSelected_viApotekResepRWJ_.data.KD_PASIENAPT,
													KdPasien:Ext.getCmp('txtKdPasienResepRWI_viApotekResepRWI').getValue(),
													//KdPay:rowSelected_viApotekResepRWJ_.data.KD_PAY,
													KdPay:Ext.getCmp('txtTmpKdPayResepRWIL').getValue(),
													JumlahTotal:toInteger(Ext.getCmp('txtTotalBayarResepRWIL').getValue()),
													JumlahTerimaUang:toInteger(Ext.getCmp('txtTotalBayarResepRWIL').getValue()),
													NoResep:tmpNoresep,
													TanggalBayar:now_viApotekResepRWI.format('Y-m-d'),
													JumlahItem:Ext.getCmp('txtTmpJmlItem').getValue(),
													Posting:LangsungPost,
													Shift: tampungshiftsekarang,
													Tanggal:Ext.getCmp('txtTmpTglResepSebenarnyaResepRWIL').getValue(),
													Ubah:ubah
												};
												parameter['jumlah']=dsDataGrdJab_viApotekResepRWI.getCount();
												var line=0;
												for(var i = 0 ; i < dsDataGrdJab_viApotekResepRWI.getCount();i++){
													var o=dsDataGrdJab_viApotekResepRWI.data.items[i].data;
													// if(o.racikan ==false){
														// parameter['cito-'+line]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.C;
														// parameter['kd_prd-'+line]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.kd_prd
														// parameter['nama_obat-'+line]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.nama_obat
														// parameter['kd_satuan-'+line]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.kd_satuan
														// parameter['harga_jual-'+line]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.harga_jual
														// parameter['harga_beli-'+line]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.harga_beli
														// parameter['kd_pabrik-'+line]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.kd_pabrik
														// parameter['jml-'+line]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.jml
														// parameter['markup-'+line]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.markup
														// parameter['disc-'+line]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.disc
														// parameter['racik-'+line]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.racik
														// parameter['dosis-'+line]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.dosis
														// parameter['jasa-'+line]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.jasa
														// parameter['no_out-'+line]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.no_out
														// parameter['no_urut-'+line]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.no_urut
														// parameter['kd_milik-'+line]=dsDataGrdJab_viApotekResepRWJ.data.items[i].data.kd_milik
														parameter['racik-'+line]=0;
														if(dsDataGrdJab_viApotekResepRWI.data.items[i].data.cito == 'Ya'){
															parameter['cito-'+line]=1;
															parameter['hargaaslicito-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.hargaaslicito;
															parameter['nilai_cito-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.nilai_cito;
														} else{
															parameter['cito-'+line]=0;
															parameter['hargaaslicito-'+line]=0;
															parameter['nilai_cito-'+line]=0;
														}
														parameter['kd_prd-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_prd
														parameter['nama_obat-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.nama_obat
														parameter['kd_satuan-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_satuan
														parameter['harga_jual-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.harga_jual
														parameter['harga_beli-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.harga_beli
														parameter['kd_pabrik-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_pabrik
														parameter['jml-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.jml
														parameter['signa-'+line]= o.signa;
														parameter['takaran-'+line]= o.takaran;
														parameter['cara_pakai-'+line]	= o.cara_pakai;
														parameter['markup-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.markup
														parameter['disc-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.disc
														parameter['dosis-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.dosis
														parameter['jasa-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.jasa
														parameter['no_out-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.no_out
														parameter['no_urut-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.no_urut
														parameter['kd_milik-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_milik
														parameter['no_racik-'+line]=0;
														parameter['aturan_racik-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.aturan_racik
														parameter['aturan_pakai-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.aturan_pakai
														parameter['jumlah_racik-' +line]=0;
														parameter['satuan_racik-' +line]='';
														parameter['catatan_racik-' +line]='';
														line++;
													/*}else{
														var parse=JSON.parse(o.result);
														parameter.jumlah=(parameter.jumlah-1)+parse.length;
														for(var j=0,jLen=parse.length; j<jLen;j++){
															var k=parse[j];
															parameter['racik-'+line]=1;
															if(k.cito == 'Ya'){
																parameter['cito-'+line]=1;
																parameter['hargaaslicito-'+line]=k.hargaaslicito;
																parameter['nilai_cito-'+line]=k.nilai_cito;
															} else{
																parameter['cito-'+line]=0;
																parameter['hargaaslicito-'+line]=0;
																parameter['nilai_cito-'+line]=0;
															}
															parameter['kd_prd-'+line]=k.kd_prd;
															parameter['nama_obat-'+line]=k.nama_obat;
															parameter['kd_satuan-'+line]=k.kd_satuan;
															parameter['harga_jual-'+line]=k.harga_jual;
															parameter['harga_beli-'+line]=k.harga_beli;
															parameter['kd_pabrik-'+line]=k.kd_pabrik;
															parameter['jml-'+line]=k.jml;
															parameter['markup-'+line]=k.markup;
															parameter['disc-'+line]=k.disc;
															parameter['signa-'+line]= o.signa;
															parameter['takaran-'+line]= o.takaran;
															parameter['cara_pakai-'+line]	= o.cara_pakai;
															parameter['jasa-'+line]=k.jasa
															parameter['no_out-'+line]=k.no_out
															parameter['no_urut-'+line]=k.no_urut
															parameter['kd_milik-'+line]=k.kd_milik
															parameter['no_racik-'+line]=o.kd_prd;
															parameter['aturan_racik-'+line]=o.aturan_racik;
															parameter['aturan_pakai-'+line]=o.aturan_pakai;
															parameter['jumlah_racik-' +line]=o.jml;
															parameter['satuan_racik-' +line]=o.kd_satuan;
															parameter['catatan_racik-' +line]=k.catatan_racik;
															line++;
														}
													}*/
												}
												// if(ValidasiBayarResepRWJ(nmHeaderSimpanData,false) == 1 ){
													Ext.Ajax.request({
														url: baseURL + "index.php/apotek/functionAPOTEK/bayarSaveResepRWJ",
														params: parameter,
														failure: function(o){
															ShowPesanErrorResepRWI('Hubungi Admin', 'Error');
														},	
														success: function(o) {
															var cst = Ext.decode(o.responseText);
															if (cst.success === true) {
																refreshRespApotekRWI();
																if(toInteger(Ext.getCmp('txtTotalBayarResepRWIL').getValue()) >= toInteger(Ext.getCmp('txtTotalBayarResepRWIL').getValue())){
																	ShowPesanInfoResepRWI('Berhasil melakukan pembayaran dan pembayaran telah lunas','Information');
																	Ext.getCmp('txtNoResepApotekResepRWIL').setValue(cst.noresep);
																	Ext.getCmp('txtTmpNooutResepRWIL').setValue(cst.noout);
																	Ext.getCmp('txtTmpTgloutResepRWIL').setValue(cst.tgl);
																	Ext.getCmp('btnunposting_viApotekResepRWI').enable();
																	Ext.getCmp('btnPrint_viResepRWI').enable();
																	Ext.getCmp('btnBayar_viApotekResepRWI').disable();
																	Ext.getCmp('btnAdd_viApotekResepRWI').enable();
																	Ext.getCmp('btnSimpan_viApotekResepRWI').disable();
																	// Ext.getCmp('btnSimpanExit_viApotekResepRWJ').disable();
																	Ext.getCmp('btnSimpan_viApotekResepRWI').disable();
																	Ext.getCmp('btnDelete_viApotekResepRWI').disable();
																	Ext.getCmp('btnDeleteHistory_viApotekResepRWI').disable();
																	//Ext.getCmp('btnAddObat').disable();
																	Ext.getCmp('btnTransfer_viApotekResepRWI').disable();
																	Ext.getCmp('txtTmpStatusPostResepRWIL').setValue(1);
																	// ResepRWI.form.Panel.a.update('<img src="'+baseURL+'ui/images/icons/16x16/hijau.png"/>');
																	ResepRWI.form.Panel.a.html = '<img src="'+baseURL+'ui/images/icons/16x16/hijau.png"/>';
																	ViewDetailPembayaranObatRWI(cst.noout,cst.tgl);
																	gridDTLTRHistoryApotekRWI.getView().refresh();
																	// setLookUpApotek_bayarResepRWJ.close();
																	nonaktivRWI(true);
																	
																	total_pasien_order_mng_obtrwi();
																	total_pasien_dilayani_order_mng_obtrwi();
																	viewGridOrderAll_RASEPRWI();
																} else {
																	ShowPesanInfoResepRWI('Berhasil melakukan pembayaran','Information');
																	// setLookUpApotek_bayarResepRWJ.close();
																	Ext.getCmp('btnunposting_viApotekResepRWI').disable();
																	Ext.getCmp('btnPrint_viResepRWI').disable();
																	ViewDetailPembayaranObatRWI(Ext.getCmp('txtTmpNoout').getValue(),Ext.getCmp('txtTmpTglout').getValue());
																	gridDTLTRHistoryApotekRWI.getView().refresh();
																	nonaktivRWI(true);
																	
																	total_pasien_order_mng_obtrwi();
																	total_pasien_dilayani_order_mng_obtrwi();
																	viewGridOrderAll_RASEPRWI();
																}
															}else{
																// ShowPesanErrorResepRWJ('Gagal melakukan pembayaran. ' + cst.pesan, 'Error');
																Ext.Msg.show({
																	title: 'Error',
																	msg: 'Gagal melakukan pembayaran. ' + cst.pesan,
																	buttons: Ext.MessageBox.OK,
																	fn: function (btn) {
																		if (btn == 'ok'){
																			Ext.getCmp('txtTotalTransfer_ResepRWI').focus(false,10);
																		}
																	}
																});
																refreshRespApotekRWI();
															};
														}
													});
												// }
											});
											
											
										}
									}
								});
							}
							
							
							// setLookUp_bayarResepRWI();
						}
					},
					{
						xtype:'tbseparator'
					},{
						xtype: 'button',
						text: 'Transfer',
						iconCls: 'save',
						// disabled:false,
						id: 'btnTransfer_viApotekResepRWI',
						iconCls: 'gantidok',
						handler: function(){
							// getSisaAngsuran(Ext.getCmp('txtTmpNoout').getValue(), Ext.getCmp('dfTglResepSebenarnyaResepRWJ').getValue());
							if (ValidasiEntryResepRWI(nmHeaderSimpanData,false) == 1 ){
								Ext.Ajax.request({
									url: baseURL + "index.php/apotek/functionAPOTEKrwi/saveResepRWI",
									params: getParamResepRWI(),
									failure: function(o){
										ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
									},	
									success: function(o) {
										var cst = Ext.decode(o.responseText);
										if (cst.success === true) {
											Ext.get('txtNoResepApotekResepRWIL').dom.value=cst.noresep;
											Ext.get('txtTmpNooutResepRWIL').dom.value=cst.noout;
											// Ext.get('txtNoTrApotekResepRWIL').dom.value=cst.noout;
											Ext.get('txtTmpTgloutResepRWIL').dom.value=cst.tgl;
											Ext.getCmp('btnTransfer_viApotekResepRWI').disable();
											Ext.getCmp('btnSimpan_viApotekResepRWI').disable();
											// Ext.getCmp('btnSimpanExit_viApotekResepRWJ').disable();
											Ext.getCmp('btnunposting_viApotekResepRWI').enable();
											/*ShowPesanInfoResepRWJ('Resep berhasil dilayani','Information');
											Ext.get('txtNoResepRWJ_viApotekResepRWJ').dom.value =cst.noresep;
											Ext.get('txtTmpNoout').dom.value                    =cst.noout;
											Ext.get('txtTmpTglout').dom.value                   =cst.tgl;
											Ext.getCmp('btnSimpan_viApotekResepRWJ').disable();
											Ext.getCmp('btnSimpanExit_viApotekResepRWJ').disable();
											Ext.getCmp('btnbayar_viApotekResepRWJ').enable();
											if(Ext.getCmp('cbNonResep').getValue() == true){
												Ext.getCmp('btnTransfer_viApotekResepRWJ').disable();
											} else{
												Ext.getCmp('btnTransfer_viApotekResepRWJ').enable();	
											}
											refreshRespApotekRWJ();*/
											// getGridDetailObatApotekResepRWI(cst.noout,cst.tgl,function(){
												var parameter ={
													Kdcustomer 		:Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').getValue(),
													NoTransaksi 	:ResepRWI.vars.no_transaksi,
													TglTransaksi	:ResepRWI.vars.tgl_transaksi,
													KdKasir 		:ResepRWI.vars.kd_kasir,
													NoOut 			:Ext.getCmp('txtTmpNooutResepRWIL').getValue(),
													TglOut 			:Ext.getCmp('txtTmpTgloutResepRWIL').getValue(),
													NoResep 		:Ext.getCmp('txtNoResepApotekResepRWIL').getValue(),
													KdUnitAsal 		:Ext.getCmp('txtTmpKdUnitApotekResepRWIL').getValue(),
													KdPasien 		:Ext.getCmp('txtKdPasienResepRWI_viApotekResepRWI').getValue(),
													JumlahTotal 	:totalall_pembayaran,
													JumlahTerimaUang:totalall_pembayaran,
													TanggalBayar 	:tanggal,
													JumlahItem 		:Ext.getCmp('txtTmpJmlItem').getValue(),
													Shift 			:tampungshiftsekarang,
													Tanggal 		:tanggal
												};
												parameter['jumlah']=dsDataGrdJab_viApotekResepRWI.getCount();
												var line=0;
												for(var i = 0 ; i < dsDataGrdJab_viApotekResepRWI.getCount();i++){
													var o=dsDataGrdJab_viApotekResepRWI.data.items[i].data;
													// if(o.racikan ==false){
														parameter['kd_prd-'+line]     = dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_prd;
														parameter['jml-'+line]        = dsDataGrdJab_viApotekResepRWI.data.items[i].data.jml;
														parameter['no_urut-'+line]    = dsDataGrdJab_viApotekResepRWI.data.items[i].data.no_urut;
														parameter['kd_milik-'+line]   = dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_milik;
														parameter['id_mrresep-'+line] = dsDataGrdJab_viApotekResepRWI.data.items[i].data.id_mrresep;
														line++;
													// }else{
														// var parse=JSON.parse(o.result);
														// parameter.jumlah=(parameter.jumlah-1)+parse.length;
														// for(var j=0,jLen=parse.length; j<jLen;j++){
															// var k=parse[j];
															// parameter['kd_prd-'+line]=k.kd_prd;
															// parameter['jml-'+line]=k.jml;
															// parameter['no_urut-'+line]=k.no_urut;
															// parameter['kd_milik-'+line]=k.kd_milik;
															// parameter['id_mrresep-'+line] =dsDataGrdJab_viApotekResepRWI.data.items[i].data.id_mrresep;
															// line++;
														// }
													// }
												}
												Ext.Ajax.request({
													url: baseURL + "index.php/apotek/functionAPOTEK/saveTransfer",
													params: parameter,
													failure: function(o){
														ShowPesanErrorResepRWI('Hubungi Admin', 'Error');
													},	
													success: function(o) {
														var cst = Ext.decode(o.responseText);
														if (cst.success === true) {
															refreshRespApotekRWI();
															ShowPesanInfoResepRWI('Transfer berhasil dilakukan','Information');
															Ext.getCmp('btnunposting_viApotekResepRWI').enable();
															Ext.getCmp('btnTransfer_viApotekResepRWI').disable();
															Ext.getCmp('btnPrint_viResepRWI').enable();
															Ext.getCmp('btnBayar_viApotekResepRWI').disable();
															ResepRWI.form.Grid.a.disable();
															// Ext.getCmp('btnDeleteHistory_viApotekResepRWJ').disable();
															Ext.getCmp('txtTmpStatusPostResepRWIL').setValue(1);
															ViewDetailPembayaranObatRWI(Ext.getCmp('txtTmpNooutResepRWIL').getValue(),Ext.getCmp('txtTmpTgloutResepRWIL').getValue());
															gridDTLTRHistoryApotekRWI.getView().refresh();
															nonaktivRWI(true);
															// ResepRWI.form.Panel.a.update('<img src="'+baseURL+'ui/images/icons/16x16/hijau.png"/>');
															ResepRWI.form.Panel.a.html = '<img src="'+baseURL+'ui/images/icons/16x16/hijau.png"/>';
															total_pasien_order_mng_obtrwi();
															total_pasien_dilayani_order_mng_obtrwi();
															viewGridOrderAll_RASEPRWI();
														}else {
															Ext.Msg.show({
																title: 'Error',
																msg: 'Gagal melakukan transfer. '+ cst.pesan,
																buttons: Ext.MessageBox.OK,
																fn: function (btn) {
																	if (btn == 'ok')
																	{
																		Ext.getCmp('txtTotalTransfer_ResepRWI').focus(false,10);
																	}
																}
															});
															refreshRespApotekRWJ();
														};
													}
												});
											// });
										}else {
											ShowPesanErrorResepRWI('Resep gagal dilayani', 'Error');
											refreshRespApotekRWI();
										};
									}
								});
							}
							// transferResepRWJ();
						}
					},{
						xtype: 'button',
						text: 'Transfer [F10]',
						// id:'btnTransfer_viApotekResepRWI',
						iconCls: 'gantidok',
						disabled:true,
						hidden:true,
						handler: function(){
							setLookUp_TransferResepRWI();
						}
					},
					{
						xtype:'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Unposting [F6]',
						id:'btnunposting_viApotekResepRWI',
						iconCls: 'gantidok',
						disabled:true,
						handler:function(){
							cekTransferRWI(Ext.getCmp('txtTmpNooutResepRWIL').getValue(),Ext.getCmp('txtTmpTgloutResepRWIL').getValue(),function(){
								if(dsTRDetailHistoryBayar.getCount()>0){
									Ext.Ajax.request({
										url: baseURL + "index.php/apotek/functionAPOTEK/deleteHistoryResepRWJ",
										params: getParamDeleteHistoryResepRWI(0),
										failure: function(o){
											ShowPesanErrorResepRWI('Hubungi Admin', 'Error');
										},	
										success: function(o){
											var cst = Ext.decode(o.responseText);
											if (cst.success === true){
												ShowPesanInfoResepRWI('Penghapusan berhasil','Information');
												ViewDetailPembayaranObatRWI(Ext.getCmp('txtTmpNooutResepRWIL').getValue(),Ext.getCmp('txtTmpTgloutResepRWIL').getValue());
												gridDTLTRHistoryApotekRWI.getView().refresh();
												Ext.getCmp('btnBayar_viApotekResepRWI').enable();
												Ext.getCmp('btnTransfer_viApotekResepRWI').enable();
												Ext.getCmp('btnSimpan_viApotekResepRWI').enable();
												// Ext.getCmp('btnDelete_viApotekResepRWJ').enable();
												// Ext.getCmp('btnDeleteHistory_viApotekResepRWJ').disable();
												// Ext.getCmp('btnSimpanData_viApotekResepRWJ').enable();
												// if(Ext.getCmp('cbNonResep').getValue() == true){															
													// Ext.getCmp('btnTransfer_viApotekResepRWJ').disable();
												// } else{
													// Ext.getCmp('btnTransfer_viApotekResepRWJ').enable();
												// }
											}else{
												ShowPesanErrorResepRWI('Gagal menghapus pembayaran', 'Error');
												ViewDetailPembayaranObatRWI(Ext.getCmp('txtTmpNooutResepRWIL').getValue(),Ext.getCmp('txtTmpTgloutResepRWIL').getValue());
												gridDTLTRHistoryApotekRWI.getView().refresh();
											}
										}
									});
								}
							})
						}  
					},
					{
						xtype: 'button',
						text: 'Hapus Bayar',
						id:'btnDeleteHistory_viApotekResepRWI',
						iconCls: 'remove',
						disabled:true,
						hidden:true,
						handler:function()
						{
							if(dsTRDetailHistoryBayar.getCount()>0){
								Ext.Msg.confirm('Warning', 'Apakah data pembayaran ini akan di hapus?', function(button){
									if (button == 'yes'){
										Ext.Ajax.request
										(
											{
												url: baseURL + "index.php/apotek/functionAPOTEKrwi/deleteHistoryResepRWJ",
												params: getParamDeleteHistoryResepRWI(),
												failure: function(o)
												{
													ShowPesanErrorResepRWI('Hubungi Admin', 'Error');
												},	
												success: function(o) 
												{
													var cst = Ext.decode(o.responseText);
													if (cst.success === true) 
													{
														ShowPesanInfoResepRWI('Penghapusan berhasil','Information');
														ViewDetailPembayaranObatRWI(Ext.getCmp('txtTmpNooutResepRWIL').getValue(),Ext.getCmp('txtTmpTgloutResepRWIL').getValue());
														gridDTLTRHistoryApotekRWI.getView().refresh();
														Ext.getCmp('btnBayar_viApotekResepRWI').enable();
														Ext.getCmp('btnDelete_viApotekResepRWI').enable();
														Ext.getCmp('btnTransfer_viApotekResepRWI').enable();
														Ext.getCmp('btnSimpanExit_viApotekResepRWI').disable();
														Ext.getCmp('btnSimpan_viApotekResepRWI').disable();
													}
													else 
													{
														ShowPesanErrorResepRWI('Gagal menghapus pembayaran', 'Error');
														ViewDetailPembayaranObatRWI(Ext.getCmp('txtTmpNooutResepRWIL').getValue(),Ext.getCmp('txtTmpTgloutResepRWIL').getValue());
														gridDTLTRHistoryApotekRWI.getView().refresh();
													};
												}
											}
											
										)
									}
								});
							} else{
								ShowPesanErrorResepRWI('Belum melakukan pembayaran','Error');
							}
						}  
					},
					{
						xtype:'splitbutton',
						text:'Cetak',
						iconCls:'print',
						id:'btnPrint_viResepRWI',
						disabled:true,
						handler:function()
						{							
								
						},
						menu: new Ext.menu.Menu({
						items: [
							// these items will render as dropdown menu items when the arrow is clicked:
							{
								xtype: 'button',
								text: 'Print Bill [F12]',
								id: 'btnPrintBillResepRWI',
								handler: function()
								{  
									PrintBillResepRWI='true';
									console.log(CurrentData_viApotekResepRWI);
									// panelnew_window_printer_resepRWI();
									var url_laporan = baseURL + "index.php/laporan/lap_billing_penunjang/";
									// dfTglResepSebenarnyaResepRWI
									GetDTLPreviewBilling(url_laporan+"preview_apotek_pdf_", CurrentData_viApotekResepRWI.data.APT_NO_TRANSAKSI,  Ext.getCmp('txtTmpNooutResepRWIL').getValue(), Ext.getCmp('txtNoResepApotekResepRWIL').getValue());
									printbill_ResepRWI();
								}
							},
							{
								xtype: 'button',
								text: 'Print Kwitansi [F11]',
								id: 'btnPrintKwitansiResepRWI',
								handler: function()
								{
									PrintBillResepRWI='false';
									// panelPrintKwitansi_resepRWI()
									Ext.Ajax.request({   
										url: baseURL + "index.php/apotek/functionAPOTEKrwi/getjenispembayaran",
										 params: {
											no_out:Ext.getCmp('txtTmpNooutResepRWIL').getValue(),
											tgl_out:Ext.getCmp('txtTmpTgloutResepRWIL').getValue()
										},
										failure: function(o)
										{
											 var cst = Ext.decode(o.responseText);
											
										},	    
										success: function(o) {
											var cst = Ext.decode(o.responseText);
											if(cst.jenis_pay == 'TU'){
												panelPrintKwitansi_resepRWI();
											}else{
												ShowPesanWarningResepRWI('Pembayaran bukan TUNAI, kwitansi tidak dapat dicetak!','Warning');
											}
										}	
									
									});
								}
							},
							{
								xtype: 'button',
								text: 'Print Label Dosis Obat [CTRL+O]',
								id: 'btnPrintLabelDosisObatResepRWI',
								handler: function()
								{
									panelPrintLabelDosisObat_resepRWI();
									
								}
							},
						]
						})
					},
					{
						xtype:'tbseparator'
					},//mCombo_printer_rwi(),
					{
					xtype: 'label',
					text: 'Order dari Rwi : ' 
					},
					mComboorder_rwi(),
					{
						xtype: 'button',
						text: 'close order',
						id:'statusservice_apt_RWI',
						iconCls: 'gantidok',
						// disabled:true,
						handler: function(){
							updatestatus_permintaan()
							load_data_pasienorder();
							//ResepRWI.form.Grid.a.store.removeAll()
							// Ext.getCmp('statusservice_apt_RWI').disable();
						}
					}
					
				]
			}//,items:
		}
    )

    return pnlFormDataBasic_viApotekResepRWI;
}

	function GetDTLPreviewBilling(url, no_transaksi, no_tr, no_resep) {
		var str_tgl_out = Ext.getCmp('dfTglResepSebenarnyaResepRWI').getValue().format("Y-m-d");
		// console.log(str_tgl_out);
		// str_tgl_out = str_tgl_out.replace("/", "~");
		// str_tgl_out = str_tgl_out.replace("/", "~");
		// str_tgl_out = str_tgl_out.replace("/", "~");
		new Ext.Window({
			title: 'Preview Billing',
			width: 1000,
			height: 600,
			constrain: true,
			modal: true,
			html: "<iframe  style='width: 100%; height: 100%;' src='" + url+"/"+no_transaksi+"/"+no_tr+"/"+no_resep+"/"+str_tgl_out+"'></iframe>",
			tbar : [
				{
					xtype 	: 'button',
					text 	: 'Cetak Direct',
					iconCls	: 'print',
					handler : function(){
						window.open(baseURL + "index.php/laporan/lap_billing_penunjang/print_apotek_pdf/"+no_transaksi+"/"+no_tr+"/"+no_resep+"/"+str_tgl_out,'_blank');
					}
				}
			]
		}).show();
	};

function getListObatDosisObat_ResepRWI(){
	Ext.Ajax.request ({
		url: baseURL + "index.php/apotek/functionAPOTEKrwi/getListDosisObat",
		params: {
			no_out:Ext.getCmp('txtTmpNooutResepRWIL').getValue(),
			tgl_out:Ext.getCmp('txtTmpTgloutResepRWIL').getValue(),
		},
		failure: function(o)
		{
			ShowPesanErrorResepRWI('Error list dosis obat. Hubungi Admin!', 'Error');
		},	
		success: function(o) 
		{   
			dsGridListDosisObat_ResepRWI.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsGridListDosisObat_ResepRWI.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsGridListDosisObat_ResepRWI.add(recs);
				GridListDosisObat_ResepRWI.getView().refresh();
				GridListDosisObat_ResepRWI.startEditing(0, 1);
			} else {
				ShowPesanErrorResepRWI('Gagal membaca data list dosis obat', 'Error');
			};
		}
	});
}
function gridListObatResepRWI(){
	var fldDetail = [];
	dsGridListDosisObat_ResepRWI = new WebApp.DataStore({ fields: fldDetail });

	/*  ============================= PEMILIHAN OBAT MENGGUNAKAN CHECKBOX===================================
	var sm = new Ext.grid.CheckboxSelectionModel({
				dataIndex:'check',
				listeners: {
					selectionchange: function(sm, selected, opts) {
						var SelectedCheckbox=GridListDosisObat_ResepRWI.getSelectionModel();
						//alert(SelectedCheckbox.selections.length);
						tampung_ceklis_obat={};
						jml_tampung_ceklis_obat = SelectedCheckbox.selections.length;
						if(SelectedCheckbox.selections.length >0){
							for(i=0;i<SelectedCheckbox.selections.length;i++){
								tampung_ceklis_obat['kd_prd-'+i]=( SelectedCheckbox.selections.items[i].data.kd_prd)
							}
							Ext.getCmp('cboJenisEtiket').focus(true, 20);
						}else{
							// dataSource_kamar.removeAll();
						}
					}
				}
			});    
	GridListDosisObat_ResepRWI = new Ext.grid.GridPanel({
	id				 : 'GridListDosisObat_ResepRWI',
	store            : dsGridListDosisObat_ResepRWI,
	autoScroll       : true,
	columnLines      : true,
	border           : true,
	width			 : 589,
	height           : 150,
	stripeRows       : true,
	title            : '',
	anchor           : '100% 100%',
	plugins          : [new Ext.ux.grid.FilterRow()],
	columns         :
						[
							new Ext.grid.RowNumberer(),
							sm,
							{
								dataIndex		: 'kd_prd',
								header			: 'Kode',
								width			: 100,
								menuDisabled	: true,
							},
							{
								dataIndex		: 'nama_obat',
								header			: 'Nama Obat',
								width			: 200,
								menuDisabled	: true,
							},
							{
								dataIndex		: 'jml_out',
								header			: 'Qty',
								align			: 'center',
								width			: 30,
								menuDisabled	: true,
							},
						],
	sm: sm,	
	viewConfig: 
	{
		forceFit: true
	}
	});
	
	return GridListDosisObat_ResepRWI;*/
	
	GridListDosisObat_ResepRWI = new Ext.grid.EditorGridPanel({
		id				 : 'GridListDosisObat_ResepRWI',
		store            : dsGridListDosisObat_ResepRWI,
		autoScroll       : true,
		columnLines      : true,
		border           : true,
		width			 : 589,
		height           : 130,
		stripeRows       : true,
		title            : '',
		anchor           : '100% 100%',
		plugins          : [new Ext.ux.grid.FilterRow()],
		selModel		 : new Ext.grid.CellSelectionModel
							// Tanda aktif saat salah satu baris dipilih # --------------
							({
								singleSelect: true,
								listeners:
								{
									cellselect: function(sm, row, rec)
									{}
								}
							}),
		colModel         :new Ext.grid.ColumnModel(
							[
								new Ext.grid.RowNumberer(),
								// sm, 
								{
									dataIndex		: 'pilih',
									header			: 'Pilih',
									width			: 20,
									menuDisabled	: true,
									align			: 'center',
									editor			: new Ext.form.TextField({
										allowBlank: false,
										enableKeyEvents:true,
										maxLength: 1,
										readOnly:true,
										listeners:{
												keyDown: function(a,b,c){
													if(b.getKey()==32){
														var line	= GridListDosisObat_ResepRWI.getSelectionModel().selection.cell[0];
														var o = dsGridListDosisObat_ResepRWI.getRange()[line].data;
														console.log(o.pilih);
														if(o.pilih == undefined || o.pilih == ''){
															o.pilih = 'V';
														}else{
															o.pilih = '';
														}
														GridListDosisObat_ResepRWI.getView().refresh();
														if (line+1 == dsGridListDosisObat_ResepRWI.getCount()){
															
															GridListDosisObat_ResepRWI.startEditing(0, 1);
														}else{
															GridListDosisObat_ResepRWI.startEditing(line+1, 1);
														}
													}
													
													if(b.getKey()==13){
														Ext.getCmp('cboJenisEtiket').focus(true, 100);
													} 
												}
										}
									})
								},
								{
									dataIndex		: 'kd_prd',
									header			: 'Kode',
									width			: 100,
									menuDisabled	: true,
								},
								{
									dataIndex		: 'nama_obat',
									header			: 'Nama Obat',
									width			: 200,
									menuDisabled	: true,
								},
								{
									dataIndex		: 'jml_out',
									header			: 'Qty',
									align			: 'center',
									width			: 30,
									menuDisabled	: true,
									editor			: new Ext.form.TextField({
									})
								},
							]),
		// sm: sm,	
		listeners	: {
			'keydown' : function(e){
					if(e.getKey() == 13){
						var line	= GridListDosisObat_ResepRWI.getSelectionModel().selection.cell[0];
						GridListDosisObat_ResepRWI.startEditing(line, 1);
					}
			},
			'cellclick': function(grd, rowIndex, colIndex, e) {
				console.log(grd);
				console.log(rowIndex);
				console.log(colIndex);
				
				if(colIndex == 4){
					GridListDosisObat_ResepRWI.startEditing(rowIndex, 4);
				}else{
					var line	= GridListDosisObat_ResepRWI.getSelectionModel().selection.cell[0];
					var o = dsGridListDosisObat_ResepRWI.getRange()[line].data;
					console.log(o.pilih);
					if(o.pilih == undefined || o.pilih == ''){
						o.pilih = 'V';
					}else{
						o.pilih = '';
					}
					GridListDosisObat_ResepRWI.getView().refresh();
				}
		    }
		},
		viewConfig: 
		{
			forceFit: true
		}
	});
	
	return GridListDosisObat_ResepRWI;
}

function gridWaktuDosisObatResepRWI(){
	var fldDetail = ['kd_waktu','waktu','kd_jam','jam'];
	var fldDetail2 = ['kd_jam','jam'];
	
	//getAturanObat();
	//getJamDosis();
    dsGridWaktuDosisObat_ResepRWI = new WebApp.DataStore({ fields: fldDetail });
    dsGridJamDosisObat_ResepRWI = new WebApp.DataStore({ fields: fldDetail2 });
	var fldDetail3 = ['kd_jenis_takaran','jenis_takaran'];
	dsGridTakaranDosisObat_ResepRWI = new WebApp.DataStore({ fields: fldDetail3 });
	
				
    GridWaktuDosisObatColumnModel =  new Ext.grid.ColumnModel([
		{
			dataIndex		: 'kd_waktu',
			header			: 'Waktu',
			width			: 40,
			menuDisabled	: true,
			hidden 			: true
        },
		{
			dataIndex		: 'waktu',
			header			: 'Waktu',
			width			: 40,
			menuDisabled	: true,
			editor: new Ext.form.TextField({
					allowBlank: false,
					listeners:{
						specialkey: function(){
							if(Ext.EventObject.getKey() == 13){
								// alert("a");
									// var line	= GridWaktuDosisObat_ResepRWI.getSelectionModel().selection.cell[0];
									// GridWaktuDosisObat_ResepRWI.startEditing(line, 2);
								
							}
						}
					}
				})
			
        },
		{
			dataIndex		: 'jam',
			header			: 'Jam',
			width			: 40,
			menuDisabled	: true,
			editor: new Ext.form.ComboBox 
			({
				id				: 'gridcbojam_etiket',
				typeAhead		: true,
				triggerAction	: 'all',
				lazyRender		: true,
				mode			: 'local',
				selectOnFocus	: true,
				forceSelection	: true,
				width			: 50,
				anchor			: '95%',
				store			: dsGridJamDosisObat_ResepRWI,
				valueField		: 'jam',
				displayField	: 'jam',
				listeners		: {
					select	: function(a,b,c){
						
					},
					specialkey: function(){
						if(Ext.EventObject.getKey() == 13){
							// alert("a");
								// var line	= GridWaktuDosisObat_ResepRWI.getSelectionModel().selection.cell[0];
								// GridWaktuDosisObat_ResepRWI.startEditing(line, 3);
						}
					}
				}
			})
        },
		{
			dataIndex		: 'qty',
			header			: 'Qty',
			align			: 'right',
			width			: 30,
			menuDisabled	: true,
			editor: new Ext.form.TextField({
					allowBlank: false,
					enableKeyEvents:true,
					listeners:{
						keyDown: function(a,b,c){
							if(b.getKey()==13){
								if (GridWaktuDosisObat_ResepRWI.getSelectionModel().selection.cell[0] == 3){
									if(kd_jenis_etiket == 2 || kd_jenis_etiket == 3){
										Ext.getCmp('cboAturanMinumEtiket').focus(true, 20);
									}else if(kd_jenis_etiket == 5){
										Ext.getCmp('txtCatatanListLabelObat').focus(true, 20);
									}
								}else{
									var line	= GridWaktuDosisObat_ResepRWI.getSelectionModel().selection.cell[0];
									GridWaktuDosisObat_ResepRWI.startEditing(line+1, 3);
								}
							} 
						}
					}
			})
        },
		{
			dataIndex		: 'jenis_takaran',
			header			: 'Jns.Takaran',
			width			: 80,
			menuDisabled	: true,
			editor: new Ext.form.ComboBox 
			({
				id				: 'gridcbojenis_takaran_etiket',
				typeAhead		: true,
				triggerAction	: 'all',
				lazyRender		: true,
				mode			: 'local',
				selectOnFocus	: true,
				forceSelection	: true,
				width			: 50,
				anchor			: '95%',
				store			: dsGridTakaranDosisObat_ResepRWI,
				valueField		: 'jenis_takaran',
				displayField	: 'jenis_takaran',
				value			: 1,
				listeners		: {
					select	: function(a,b,c){
						
					},
					'specialkey' : function()
					{
						if(Ext.EventObject.getKey() == 13 || Ext.EventObject.getKey() == 9){
							if (GridWaktuDosisObat_ResepRWI.getSelectionModel().selection.cell[0] == 3){
								if(kd_jenis_etiket == 2 || kd_jenis_etiket == 3){
									Ext.getCmp('cboAturanMinumEtiket').focus(true, 20);
								}else if(kd_jenis_etiket == 5){
									Ext.getCmp('txtCatatanListLabelObat').focus(true, 20);
								}
							}else{
								// var line	= GridWaktuDosisObat_ResepRWI.getSelectionModel().selection.cell[0];
								// GridWaktuDosisObat_ResepRWI.startEditing(line+1, 1);
							}
						}			
					}
				}
			})
        }
        ]
		
    )
	
	
	GridWaktuDosisObat_ResepRWI= new Ext.grid.EditorGridPanel({
		id			: 'GridWaktuDosisObat_ResepRWI',
		stripeRows	: true,
		width		: 589,
		height		: 110,
        store		: dsGridWaktuDosisObat_ResepRWI,
        border		: true,
        frame		: false,
        autoScroll	: true,
        cm			: GridWaktuDosisObatColumnModel,
		listeners	: {
			rowclick: function( $this, rowIndex, e )
			{
				trcellCurrentTindakan_KasirRWI = rowIndex;
    		},
			celldblclick: function(gridView,htmlElement,columnIndex,dataRecord){
				
			}
		},
		viewConfig	: {forceFit: true},
    });
	return GridWaktuDosisObat_ResepRWI;
}

function Itempanel_PrintLabelDosisObatResepRWI()
{
    var items=
    (
        {
            id: 'panelItemPrintLabelObatRWI',
			layout:'form',
			border: true,
			bodyStyle:'padding: 3px',
			height: 110,
			items:
			
			[
			
				{
					layout: 'absolute',
					bodyStyle: 'padding: 3px ',
					border: true,
					//width: 300,
					//height: 200,
					anchor: '100% 100%',
					items:
					[
						
						{
							x: 140,
							y: 10,
							xtype: 'label',
							text: ':',
							id:'TD_awal'
						},
						/*-------------- 2 ----------------*/
						{
							id:'lblCaraMinumListLabelObat',
							x: 10,
							y: 10,
							xtype: 'label',
							text: 'Diminum'
						},
						{
							x: 140,
							y: 10,
							xtype: 'label',
							text: ':',
							id:'TD_lblCaraMinumListLabelObat'
						},
						mComboCaraMinum(),
						
						{
							id:'lblJenisHariListLabelObat',
							x: 10,
							y: 10,
							xtype: 'label',
							text: 'Jenis Hari'
						},
						mComboJenisHari(),
						
						{
							id:'lblKeteranganObatLuarListLabelObat',
							x: 10,
							y: 10,
							xtype: 'label',
							text: 'Keterangan'
						},
						mComboKeteranganObatLuar(),
						/*-------------- 3 ----------------*/
						{
							id:'lblJamListLabelObat',
							x: 10,
							y: 40,
							xtype: 'label',
							text: 'Jam'
						},
						{
							x: 140,
							y: 40,
							xtype: 'label',
							text: ':'
						},
						mComboJam(),
						{
							id:'lblCatatanListLabelObat',
							x: 10,
							y: 40,
							xtype: 'label',
							text: 'Catatan'
						},
						{
							x: 140,
							y: 40,
							xtype: 'label',
							text: ':'
						},
						{
							id:'txtCatatanListLabelObat',
							x: 160,
							y: 40,
							width: 350,
							xtype: 'textfield',
							listeners:
							{ 
								'specialkey' : function()
								{
									 if(Ext.EventObject.getKey()==13 || Ext.EventObject.getKey() == 9){
										 if(kd_jenis_etiket == 2){
											 Ext.getCmp('btnOkPrintLabelDosisObat_ResepRWI').el.dom.click();
										 }else{ 
											Ext.getCmp('txtCaraPakaiListLabelObat').focus(false, 20);
										 }
									}		
								}
							}
						},
						
						{
							id:'lblKeteranganListLabelObat',
							x: 10,
							y: 40,
							xtype: 'label',
							text: 'Keterangan'
						},
						{
							x: 140,
							y: 40,
							xtype: 'label',
							text: ':'
						},
						{
							id:'txtKeteranganListLabelObat',
							x: 160,
							y: 40,
							width: 350,
							xtype: 'textfield',
							value:'KOCOK DAHULU SEBELUM DIMINUM',
							listeners:{
								'specialkey' : function()
								{
									 if(Ext.EventObject.getKey()==13 || Ext.EventObject.getKey() == 9){
										if(kd_jenis_etiket == 3){
											Ext.getCmp('txtCatatanListLabelObat_SIRUP').focus(false, 20);
										}else{
											Ext.getCmp('btnOkPrintLabelDosisObat_ResepRWI').el.dom.click();
										}
									}		
								}
							}
						},
						/*-------------- 4 ----------------*/
						{
							id:'lblTanggalListLabelObat',
							x: 10,
							y: 70,
							xtype: 'label',
							text: 'Tanggal'
						},
						{
							id:'TD_TanggalListLabelObat',
							x: 140,
							y: 70,
							xtype: 'label',
							text: ':'
						},
						{
							x: 160,
							y: 70,
							xtype: 'datefield',
							id: 'dfTglListLabelObat',
							format: 'd/M/Y',
							width: 100,
							tabIndex:3,
							disabled:false,
							value:now_viApotekResepRWI,
							listeners:
							{ 
								'specialkey' : function()
								{
									 if(Ext.EventObject.getKey()==13 || Ext.EventObject.getKey() == 9){
										Ext.getCmp('btnOkPrintLabelDosisObat_ResepRWI').el.dom.click();
									}		
								}
							}
						},
						{
							id:'lblCaraPakaiListLabelObat',
							x: 10,
							y: 70,
							xtype: 'label',
							text: 'Cara Pakai'
						},
						{
							x: 140,
							y: 70,
							xtype: 'label',
							text: ':',
							id:'TD_CaraPakaiListLabelObat',
						},
						{
							id:'txtCaraPakaiListLabelObat',
							x: 160,
							y: 70,
							width: 350,
							xtype: 'textfield',
							listeners:{
								'specialkey' : function()
									{
										 if(Ext.EventObject.getKey()==13 || Ext.EventObject.getKey() == 9){
											Ext.getCmp('btnOkPrintLabelDosisObat_ResepRWI').el.dom.click();
											
										}	
									}
								
							}
						},
						/**************CATATAN SYRUP ***********/
						{
							id:'lblCatatanListLabelObat_SIRUP',
							x: 10,
							y: 70,
							xtype: 'label',
							text: 'Catatan'
						},
						{
							x: 140,
							y: 70,
							xtype: 'label',
							text: ':',
							id:'TD_CatatanListLabelObat_SIRUP'
						},
						{
							id:'txtCatatanListLabelObat_SIRUP',
							x: 160,
							y: 70,
							width: 350,
							xtype: 'textfield',
							listeners:
							{ 
								'specialkey' : function()
								{
									 if(Ext.EventObject.getKey()==13 || Ext.EventObject.getKey() == 9){
										 if(kd_jenis_etiket == 3){
											 Ext.getCmp('btnOkPrintLabelDosisObat_ResepRWI').el.dom.click();
										 }
									}		
								}
							}
						}
						
					]
				}
				
			]	
        }
    );

    return items;
};

function getItemPanelInputBiodata_viApotekResepRWI(lebar) {
    var items ={
	    layout: 'Form',
	    labelAlign: 'Left',
	    bodyStyle: 'padding:4px;',
		labelWidth: 100,
		// autoWidth: true,
		border:true,
		items:[	
			{
				layout: 'absolute',
				bodyStyle: 'padding: 0px 0px 0px 0px',
				border: false,
				width: 500,
				height: 100,
				anchor: '100% 100%',
				items:[            
					{
						x:0,
						y:0,
						xtype: 'label',
						text:'No. Tr'
					},{
						x:65,
						y:0,
						xtype: 'label',
						text:':'
					},{
						x:75,
						y:0,
						xtype: 'textfield',
						width : 130,	
						readOnly: true,
						name: 'txtTmpNooutResepRWIL',
						id: 'txtTmpNooutResepRWIL',
					},{
						x:0,
						y:25,
						xtype: 'label',
						text:'No. Resep'
					},{
						x:65,
						y:25,
						xtype: 'label',
						text:':'
					},{
						x:75,
						y:25,
						xtype: 'textfield',
						id: 'txtNoResepApotekResepRWIL',
						readOnly: true,
						emptyText: 'Nomor Resep',
						width: 130
					},{
						x:0,
						y:50,
						xtype: 'label',
						text:'Jml Resep'
					},{
						x:65,
						y:50,
						xtype: 'label',
						text:':'
					},{
						x:75,
						y:50,
						xtype: 'numberfield',
						width : 130,	
						name: 'txtCatatanResepRWI_viResepRWI',
						id: 'txtCatatanResepRWI_viResepRWI',
						listeners:{
							specialkey:function(){
								if(Ext.EventObject.getKey() == 13 || Ext.EventObject.getKey() == 9){
									Ext.getCmp('cboPilihankelompokPasienAptResepRWI').focus(true,10);
									
								}
							}
						}
					},{
						x:0,
						y:75,
						xtype: 'label',
						text:'Tgl. Resep'
					},{
						x:65,
						y:75,
						xtype: 'label',
						text:':'
					},{
						x:75,
						y:75,
						xtype: 'datefield',
						id:'dfTglResepSebenarnyaResepRWI',
						width : 130,	
						format: 'd/M/Y',
						value: now_viApotekResepRWI
					},{
						x:210,
						y:75,
						xtype: 'label',
						text:'*) Jika resep di input bukan pada tanggal di buatnya resep, isi Tanggal Resep sesuai dengan tanggal dibuatnya resep',
						width : 350,
						style : {
							color : 'darkblue',
							'font-size':'10px'
						}
					},{
						x:230,
						y:0,
						xtype: 'label',
						text:'Pasien'
					},{
						x:295,
						y:0,
						xtype: 'label',
						text:':'
					},{
						x:305,
						y:0,
						xtype: 'textfield',
						flex: 1,
						width : 100,	
						name: 'txtKdPasienResepRWI_viApotekResepRWI',
						id: 'txtKdPasienResepRWI_viApotekResepRWI',
						// emptyText: 'No Resep',
						listeners:{
							'specialkey': function(){
								if (Ext.EventObject.getKey() === 13){
									getPasienbyKodeNama(Ext.getCmp('txtKdPasienResepRWI_viApotekResepRWI').getValue(),'');
								};
							}
						}
					},{
						x:410,
						y:0,
						xtype: 'textfield',
						flex: 1,
						width : 180,	
						name: 'txtNamaPasienResepRWI_viApotekResepRWI',
						id: 'txtNamaPasienResepRWI_viApotekResepRWI',
						// emptyText: 'No Resep',
						listeners:{
							'specialkey': function(){
								if (Ext.EventObject.getKey() === 13){
									getPasienbyKodeNama('',Ext.getCmp('txtNamaPasienResepRWI_viApotekResepRWI').getValue());
								}
							}
						}
					},
					/* ResepRWI.form.ComboBox.kodePasien = new Nci.form.Combobox.autoComplete({
						store	: ResepRWI.form.ArrayStore.kodepasien,
						select	: function(a,b,c){
							console.log(b.data);
							Ext.getCmp('cbo_DokterApotekResepRWI').setValue(b.data.kd_dokter);
							ResepRWI.form.ComboBox.namaPasien.setValue(b.data.nama);
							Ext.getCmp('cbo_UnitResepRWIL').setValue(b.data.nama_unit);
							Ext.getCmp('txtNoSep_viResepRWI').setValue(b.data.no_sjp);
							Ext.getCmp('txtNoTlp_viResepRWI').setValue(b.data.telepon);
							Ext.getCmp('cboPilihankelompokPasienAptResepRWI').setValue(b.data.customer);
							Ext.getCmp('txtNoKamarApotekResepRWIL').setValue(b.data.no_kamar);
							Ext.getCmp('txtKamarApotekResepRWIL').setValue(b.data.nama_kamar);
							ResepRWI.vars.no_transaksi=b.data.no_transaksi;
							ResepRWI.vars.tgl_transaksi=b.data.tgl_transaksi;
							ResepRWI.vars.kd_kasir=b.data.kd_kasir;
							ResepRWI.vars.urut_masuk=b.data.urut_masuk;
							ResepRWI.vars.payment=b.data.payment;
							ResepRWI.vars.payment_type=b.data.payment_type;
							Ext.getCmp('txtSpesialApotekResepRWIL').setValue(b.data.spesialisasi);
							Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').setValue(b.data.kd_customer);
							Ext.getCmp('txtTmpKdDokterApotekResepRWIL').setValue(b.data.kd_dokter);
							Ext.getCmp('txtTmpKdUnitApotekResepRWIL').setValue(b.data.kd_unit);
							Ext.getCmp('txtTmpKdPayResepRWIL').setValue(b.data.kd_pay);
							Ext.getCmp('txtTmpJenisPayResepRWIL').setValue(b.data.jenis_pay);
							Ext.getCmp('btnAddObatRWI').enable();
							Ext.getCmp('btnSimpan_viApotekResepRWI').enable();
							Ext.getCmp('btnSimpanExit_viApotekResepRWI').enable();
							Ext.getCmp('btnDelete_viApotekResepRWI').enable();
							
							kd_spesial_tr=b.data.kd_spesial;
							kd_unit_kamar_tr=b.data.kd_unit_kamar;
							no_kamar_tr=b.data.no_kamar;
							ordermanajemenrwi=false;
						
							Ext.getCmp('btnSimpan_viApotekResepRWI').enable();
							Ext.getCmp('btnSimpanExit_viApotekResepRWI').enable();
							Ext.getCmp('btnBayar_viApotekResepRWI').disable();
							Ext.getCmp('btnDelete_viApotekResepRWI').enable();
							Ext.getCmp('txtCatatanResepRWI_viResepRWI').focus(true,10);
						},
						width	: 80,
						x:305,
						y:0,
						insert	: function(o){
							return {
								kd_pasien        	:o.kd_pasien,
								nama        		:o.nama,
								kd_dokter			:o.kd_dokter,
								nama_unit			:o.nama_unit,
								nama_dokter			:o.nama_dokter,
								kd_unit				:o.kd_unit,
								kd_customer			:o.kd_customer,
								no_transaksi		:o.no_transaksi,
								kd_kasir			:o.kd_kasir,
								nama_kamar			:o.nama_kamar,
								no_kamar			:o.no_kamar,
								tgl_transaksi		:o.tgl_transaksi,
								kd_spesial     		:o.kd_spesial,
								kd_unit_kamar      	:o.kd_unit_kamar,
								urut_masuk			:o.urut_masuk,
								customer			:o.customer,
								telepon				:o.telepon,
								no_sjp				:o.no_sjp,
								kd_pay				:o.kd_pay,
								payment				:o.payment,
								jenis_pay			:o.jenis_pay,
								payment_type		:o.payment_type,
								spesialisasi		:o.spesialisasi,
								text			:  '<table style="font-size: 11px;"><tr><td width="90" align="left">'+o.kd_pasien+'</td><td width="180" align="left">'+o.nama+'</td><td width="100" align="left">'+ShowDate(o.tgl_transaksi)+'</td><td width="150" align="left">'+o.nama_unit+'</td></tr></table>'
							}
						},
						url		: baseURL + "index.php/apotek/functionAPOTEKrwi/getKodePasienResepRWI",
						valueField: 'kd_pasien',
						displayField: 'text',
						listWidth: 420,
					}),	 */
					/* ResepRWI.form.ComboBox.namaPasien= new Nci.form.Combobox.autoComplete({
						store	: ResepRWI.form.ArrayStore.pasien,
						select	: function(a,b,c){
							ResepRWI.form.ComboBox.kodePasien.setValue(b.data.kd_pasien);
							Ext.getCmp('cbo_DokterApotekResepRWI').setValue(b.data.kd_dokter);
							// Ext.getCmp('cboKodePasienResepRWI').setValue(b.data.kd_pasien);
							Ext.getCmp('cbo_UnitResepRWIL').setValue(b.data.nama_unit);
							Ext.getCmp('txtNoSep_viResepRWI').setValue(b.data.no_sjp);
							Ext.getCmp('txtNoTlp_viResepRWI').setValue(b.data.telepon);
							Ext.getCmp('cboPilihankelompokPasienAptResepRWI').setValue(b.data.customer);
							Ext.getCmp('txtNoKamarApotekResepRWIL').setValue(b.data.no_kamar);
							Ext.getCmp('txtKamarApotekResepRWIL').setValue(b.data.nama_kamar);
							ResepRWI.vars.no_transaksi=b.data.no_transaksi;
							ResepRWI.vars.tgl_transaksi=b.data.tgl_transaksi;
							ResepRWI.vars.kd_kasir=b.data.kd_kasir;
							ResepRWI.vars.urut_masuk=b.data.urut_masuk;
							ResepRWI.vars.payment=b.data.payment;
							ResepRWI.vars.payment_type=b.data.payment_type;
							Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').setValue(b.data.kd_customer);
							Ext.getCmp('txtSpesialApotekResepRWIL').setValue(b.data.spesialisasi);
							Ext.getCmp('txtTmpKdDokterApotekResepRWIL').setValue(b.data.kd_dokter);
							Ext.getCmp('txtTmpKdUnitApotekResepRWIL').setValue(b.data.kd_unit);
							Ext.getCmp('txtTmpKdPayResepRWIL').setValue(b.data.kd_pay);
							Ext.getCmp('txtTmpJenisPayResepRWIL').setValue(b.data.jenis_pay);
							Ext.getCmp('btnAddObatRWI').enable();
							Ext.getCmp('btnSimpan_viApotekResepRWI').enable();
							Ext.getCmp('btnSimpanExit_viApotekResepRWI').enable();
							Ext.getCmp('btnDelete_viApotekResepRWI').enable();
							
							kd_spesial_tr=b.data.kd_spesial;
							kd_unit_kamar_tr=b.data.kd_unit_kamar;
							no_kamar_tr=b.data.no_kamar;
							ordermanajemenrwi=false;
							
							
							Ext.getCmp('btnSimpan_viApotekResepRWI').enable();
							Ext.getCmp('btnSimpanExit_viApotekResepRWI').enable();
							Ext.getCmp('btnBayar_viApotekResepRWI').disable();
							Ext.getCmp('btnDelete_viApotekResepRWI').enable();
							Ext.getCmp('txtCatatanResepRWI_viResepRWI').focus(true,10);
						},
						width	: 200,
						x:390,
						y:0,
						insert	: function(o){
							return {
								kd_pasien        	:o.kd_pasien,
								nama        		:o.nama,
								kd_dokter			:o.kd_dokter,
								nama_unit			:o.nama_unit,
								nama_dokter			:o.nama_dokter,
								kd_unit				:o.kd_unit,
								kd_customer			:o.kd_customer,
								no_transaksi		:o.no_transaksi,
								kd_kasir			:o.kd_kasir,
								nama_kamar			:o.nama_kamar,
								no_kamar			:o.no_kamar,
								tgl_transaksi		:o.tgl_transaksi,
								kd_spesial     		:o.kd_spesial,
								kd_unit_kamar      	:o.kd_unit_kamar,
								urut_masuk			:o.urut_masuk,
								customer			:o.customer,
								telepon				:o.telepon,
								no_sjp				:o.no_sjp,
								kd_pay				:o.kd_pay,
								payment				:o.payment,
								jenis_pay			:o.jenis_pay,
								payment_type		:o.payment_type,
								spesialisasi		:o.spesialisasi,
								text				:  '<table style="font-size: 11px;"><tr><td width="80">'+o.kd_pasien+'</td><td width="180">'+o.nama+'</td><td width="80">'+ShowDate(o.tgl_transaksi)+'</td></tr></table>',
								nama	 			:o.nama
							}
						},
						url		: baseURL + "index.php/apotek/functionAPOTEKrwi/getPasienResepRWI",
						valueField: 'nama',
						displayField: 'text',
						listWidth: 340,
						emptyText: 'Nama Pasien'
					}), */
					{
						x:230,
						y:25,
						xtype: 'label',
						text:'Jenis Pasien'
					},{
						x:295,
						y:25,
						xtype: 'label',
						text:':'
					},
					ComboPilihanKelompokPasienApotekResepRWI(),
					{
						x:230,
						y:50,
						xtype: 'label',
						text:'No. SEP'
					},{
						x:295,
						y:50,
						xtype: 'label',
						text:':'
					},{
						x:305,
						y:50,
						xtype: 'textfield',
						width : 135,	
						id: 'txtNoSep_viResepRWI',
						readOnly: true,
					},{
						x:445,
						y:50,
						xtype: 'label',
						text:'No. Tlp :'
					},{
						x:490,
						y:50,
						xtype: 'textfield',
						id: 'txtNoTlp_viResepRWI',
						readOnly	: true,
						width: 100,
					},{
						x:620,
						y:0,
						xtype: 'label',
						text:'Kamar'
					},{
						x:690,
						y:0,
						xtype: 'label',
						text:':'
					},{
						x:700,
						y:0,
						xtype: 'textfield',
						width : 230,	
						id: 'txtKamarApotekResepRWIL',
						readOnly: true,
						emptyText: 'Kamar'
					},{
						x:620,
						y:25,
						xtype: 'label',
						text:'Nama Unit'
					},{
						x:690,
						y:25,
						xtype: 'label',
						text:':'
					},
					ComboUnitResepRWILookup(),
					{
						x:700,
						y:25,
						xtype: 'textfield',
						id: 'txtNoKamarApotekResepRWIL',
						readOnly	: true,
						width: 75,
						emptyText: 'No kamar',
						hidden:true
					},{
						x:810,
						y:25,
						xtype: 'textfield',
						width : 120,	
						id: 'txtSpesialApotekResepRWIL',
						readOnly: true,
						emptyText: 'Spesialisasi'
					},{
						x:620,
						y:50,
						xtype: 'label',
						text:'Dokter'
					},{
						x:690,
						y:50,
						xtype: 'label',
						text:':'
					},
					ComboDokterApotekResepRWI(),
					{
						x:850,
						y:75,
						xtype:'button',
						text:'1/2 Resep',
						width:70,
						hideLabel:true,
						hidden:true,
						id: 'btn1/2resep_viApotekResepRWI',
						handler:function(){
							hitungSetengahResepRWI();
						}   
					},
					
					// mComboKodePasienResepRWI(),
					
					
					
 			/* ResepRWI.form.ComboBox.kodepasien= new Nci.form.Combobox.autoComplete({
						store	: ResepRWI.form.ArrayStore.kodepasien,
						select	: function(a,b,c){
							Ext.getCmp('cbo_DokterApotekRes
							epRWI').setValue(b.data.kd_dokter);
							ResepRWI.form.ComboBox.namaPasien.setValue(b.data.nama);
							Ext.getCmp('cbo_UnitResepRWIL').setValue(b.data.kd_unit);
							Ext.getCmp('cboPilihankelompokPasienAptResepRWI').setValue(b.data.kd_customer);
							Ext.getCmp('txtNoKamarApotekResepRWIL').setValue(b.data.no_kamar);
							Ext.getCmp('txtKamarApotekResepRWIL').setValue(b.data.nama_kamar);
							ResepRWI.vars.no_transaksi=b.data.no_transaksi;
							ResepRWI.vars.kd_kasir=b.data.kd_kasir;
							Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').setValue(b.data.kd_customer);
							Ext.getCmp('txtTmpKdDokterApotekResepRWIL').setValue(b.data.kd_dokter);
							Ext.getCmp('txtTmpKdUnitApotekResepRWIL').setValue(b.data.kd_unit);
							
						},
						width	: 130,
						insert	: function(o){
							return {
								kd_pasien        	:o.kd_pasien,
								nama        		:o.nama,
								kd_dokter			:o.kd_dokter,
								kd_unit				:o.kd_unit,
								kd_customer			:o.kd_customer,
								no_transaksi		:o.no_transaksi,
								kd_kasir			:o.kd_kasir,
								nama_kamar			:o.nama_kamar,
								no_kamar			:o.no_kamar,
								text				:  '<table style="font-size: 11px;"><tr><td width="80">'+o.kd_pasien+'</td><td width="180">'+o.nama+'</td></tr></table>',
								nama	 			:o.nama
							}
						},
						url		: baseURL + "index.php/apotek/functionAPOTEKrwi/getKodePasienResepRWI",
						valueField: 'kd_pasien',
						displayField: 'text',
						listWidth: 260,
						emptyText: 'No medrec'
					}), */
					
					// {
						// xtype: 'checkbox',
						// boxLabel: 'Integrasi',
						// id: 'cbIntegrasiResepRWI',
						// name: 'cbIntegrasiResepRWI',
						// width: 80,
						// checked: true,
						// handler:function(a,b) 
						// {
							// if(a.checked==true){
								
							// }else{
							// }
						// }
					// },
					// {
						// xtype: 'displayfield',
						// width: 80
					// },
					
					//--------HIDDEN---------------
					{
						xtype:'button',
						text:'Racikan',
						width:70,
						hideLabel:true,
						hidden:true,
						disabled:true,
						id: 'btnRacikan_viApotekResepRWI',
						handler:function()
						{
							formulaRacikanResepRWI();
						}   
					},
					{
						xtype: 'textfield',
						width : 120,	
						id: 'txtTmpKdCustomerApotekResepRWIL',
						readOnly: true,
						emptyText: 'Kode Pasien',
						hidden:true
					},{
						xtype: 'textfield',
						width : 150,	
						readOnly: true,
						name: 'txtTmpJmlItem',
						id: 'txtTmpJmlItem',
						emptyText: 'No out',
						hidden:true
					},{
						xtype: 'textfield',
						width : 120,	
						id: 'txtTmpKdDokterApotekResepRWIL',
						readOnly: true,
						emptyText: 'kode Dokter',
						hidden:true
					},
					{
						xtype: 'textfield',
						width : 120,	
						id: 'txtTmpKdUnitApotekResepRWIL',
						readOnly: true,
						emptyText: 'Kode Unit',
						hidden:true
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtTmpTglResepSebenarnyaResepRWIL',
						id: 'txtTmpTglResepSebenarnyaResepRWIL',
						emptyText: 'Tgl resep sebenarnya',
						hidden:true,
						value: tanggal_resep_sebernarnyaRWI
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtTmpTgloutResepRWIL',
						id: 'txtTmpTgloutResepRWIL',
						emptyText: 'tgl out',
						hidden:true
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtTmpStatusPostResepRWIL',
						id: 'txtTmpStatusPostResepRWIL',
						emptyText: 'Status post',
						hidden:true
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtTmpSisaAngsuranResepRWIL',
						id: 'txtTmpSisaAngsuranResepRWIL',
						emptyText: 'sisa angsuran',
						hidden:true
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtTmpTotQtyResepRWIL',
						id: 'txtTmpTotQtyResepRWIL',
						emptyText: 'total qty',
						hidden:true
					},{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtTmpJenisPayResepRWIL',
						id: 'txtTmpJenisPayResepRWIL',
						emptyText: 'Jenis pay',
						hidden:true
					},{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtTmpKdPayResepRWIL',
						id: 'txtTmpKdPayResepRWIL',
						emptyText: 'KD pay',
						hidden:true
					}
                ]
            },
		
			 
		]
	};
    return items;
};

function getPasienbyKodeNama(kd_pasien,nama){
	
	Ext.Ajax.request
	(
		{
			url :baseURL + "index.php/apotek/functionAPOTEKrwi/getKodeNamaPasienResepRWI",
			params: {kd_pasien:kd_pasien,nama:nama},
			failure: function(o)
			{
				ShowPesanErrorResepRWI('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				console.log(cst.count);
				if (cst.count == 1){
					Ext.getCmp('cbo_DokterApotekResepRWI').setValue(cst.listData[0].kd_dokter);
					Ext.getCmp('txtKdPasienResepRWI_viApotekResepRWI').setValue(cst.listData[0].kd_pasien);
					Ext.getCmp('txtNamaPasienResepRWI_viApotekResepRWI').setValue(cst.listData[0].nama);
					Ext.getCmp('cbo_UnitResepRWIL').setValue(cst.listData[0].nama_unit);
					Ext.getCmp('txtNoSep_viResepRWI').setValue(cst.listData[0].no_sjp);
					Ext.getCmp('txtNoTlp_viResepRWI').setValue(cst.listData[0].telepon);
					Ext.getCmp('cboPilihankelompokPasienAptResepRWI').setValue(cst.listData[0].customer);
					Ext.getCmp('txtNoKamarApotekResepRWIL').setValue(cst.listData[0].no_kamar);
					Ext.getCmp('txtKamarApotekResepRWIL').setValue(cst.listData[0].nama_kamar);
					ResepRWI.vars.no_transaksi=cst.listData[0].no_transaksi;
					ResepRWI.vars.tgl_transaksi=cst.listData[0].tgl_transaksi;
					ResepRWI.vars.kd_kasir=cst.listData[0].kd_kasir;
					ResepRWI.vars.urut_masuk=cst.listData[0].urut_masuk;
					ResepRWI.vars.payment=cst.listData[0].payment;
					ResepRWI.vars.payment_type=cst.listData[0].payment_type;
					Ext.getCmp('txtSpesialApotekResepRWIL').setValue(cst.listData[0].spesialisasi);
					Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').setValue(cst.listData[0].kd_customer);
					Ext.getCmp('txtTmpKdDokterApotekResepRWIL').setValue(cst.listData[0].kd_dokter);
					Ext.getCmp('txtTmpKdUnitApotekResepRWIL').setValue(cst.listData[0].kd_unit);
					Ext.getCmp('txtTmpKdPayResepRWIL').setValue(cst.listData[0].kd_pay);
					Ext.getCmp('txtTmpJenisPayResepRWIL').setValue(cst.listData[0].jenis_pay);
					Ext.getCmp('btnAddObatRWI').enable();
					Ext.getCmp('btnSimpan_viApotekResepRWI').enable();
					Ext.getCmp('btnSimpanExit_viApotekResepRWI').enable();
					Ext.getCmp('btnDelete_viApotekResepRWI').enable();
					
					kd_spesial_tr=cst.listData[0].kd_spesial;
					kd_unit_kamar_tr=cst.listData[0].kd_unit_kamar;
					no_kamar_tr=cst.listData[0].no_kamar;
					ordermanajemenrwi=false;
				
					Ext.getCmp('btnSimpan_viApotekResepRWI').enable();
					Ext.getCmp('btnSimpanExit_viApotekResepRWI').enable();
					// Ext.getCmp('btnBayar_viApotekResepRWI').disable();
					Ext.getCmp('btnDelete_viApotekResepRWI').enable();
					Ext.getCmp('txtCatatanResepRWI_viResepRWI').focus(true,10);
				
				}else if (cst.count > 1){
					LookUpKunjungan_resepRWI(kd_pasien,nama);
				}else if( cst.count == 0){
					Ext.Msg.show({
						title: 'Perhatian',
						//msg: 'Kriteria huruf pencarian obat minimal 3 huruf!',
						msg: 'Data pasien tidak ditemukan!',
						buttons: Ext.MessageBox.OK,
						fn: function (btn) {
							if (btn == 'ok')
							{
								Ext.getCmp('txtKdPasienResepRWI_viApotekResepRWI').focus(true,10);
							}
						}
					});
				}
				
			}
		}
	)
}

//---------------------------
function LookUpKunjungan_resepRWI(kd_pasien,nama)
{
	
	WindowLookUpKunjungan_ResepRWI = new Ext.Window
    (
        {
            id: 'idWindowLookUpKunjungan_ResepRWI',
            title: 'Daftar Kunjungan',
            width:620,
            height: 235,
            border: false,
            resizable:false,
            plain: true,
            iconCls: 'icon_lapor',
            modal: true,
            items: [
				gridListkunjunganResepRWI() // grid kunjungan pasien
			],
			listeners:
			{             
				activate: function()
				{
						
				},
				afterShow: function()
				{
					this.activate();

				},
				deactivate: function()
				{
					
				},
				close: function (){
					/* if(FocusExitResepRWJ == false){
						var line	= AptResepRWJ.form.Grid.a.getSelectionModel().selection.cell[0];
						AptResepRWJ.form.Grid.a.startEditing(line, 4);	
					} */
				}
			}
        }
    );

    WindowLookUpKunjungan_ResepRWI.show();
	getListKunjungan_ResepRWI(kd_pasien,nama); 
};

function getListKunjungan_ResepRWI(kd_pasien,nama){
	Ext.Ajax.request ({
		url: baseURL + "index.php/apotek/functionAPOTEKrwi/getKodeNamaPasienResepRWI",
		params: {
			kd_pasien:kd_pasien,
			nama:nama
		},
		failure: function(o)
		{
			ShowPesanErrorResepRWI('Error menampilkan list kunjungan. Hubungi Admin!', 'Error');
		},	
		success: function(o) 
		{   
			dsGridListKunjungan_ResepRWI.removeAll();
			var cst = Ext.decode(o.responseText);
			var recs=[],
				recType=dsGridListKunjungan_ResepRWI.recordType;
			for(var i=0; i<cst.listData.length; i++){
				recs.push(new recType(cst.listData[i]));						
			}
			dsGridListKunjungan_ResepRWI.add(recs);
			GridListKunjungan_ResepRWI.getView().refresh();
			GridListKunjungan_ResepRWI.getSelectionModel().selectRow(0);
			GridListKunjungan_ResepRWI.getView().focusRow(0);
		}
	});
}
function gridListkunjunganResepRWI(){
	var fldDetail = ['kd_kasir','no_transaksi','tgl_transaksi','kd_pasien','nama', 'nama_keluarga','jenis_kelamin','nama_unit','kd_unit', 
	'no_kamar', 'nama_unit' , 'nama_kamar', 'kd_dokter', 'kd_customer','kd_spesial','kd_unit_kamar','urut_masuk','customer', 
	'telepon','no_sjp','kd_pay','payment','jenis_pay','payment_type','spesialisasi','alamat'];
	dsGridListKunjungan_ResepRWI = new WebApp.DataStore({ fields: fldDetail });
	
    GridListKunjunganColumnModel =  new Ext.grid.ColumnModel([
        new Ext.grid.RowNumberer(),
		{
			dataIndex		: 'kd_pasien',
			header			: 'No. Medrec',
			width			: 70,
			menuDisabled	: true,
        },
		{
			dataIndex		: 'nama',
			header			: 'Nama',
			width			: 70,
			menuDisabled	: true,
        },
		{
			dataIndex		: 'alamat',
			header			: 'Alamat',
			width			: 70,
			menuDisabled	: true,
        },
		{
			dataIndex		: 'nama_unit',
			header			: 'Unit',
			width			: 70,
			menuDisabled	: true,
        },
		{
			dataIndex		: 'tgl_transaksi',
			header			: 'Tanggal Kunjungan',
			width			: 70,
			menuDisabled	: true,
        },
	]);
	
	
	GridListKunjungan_ResepRWI= new Ext.grid.EditorGridPanel({
		id			: 'GridListKunjungan_ResepRWI',
		stripeRows	: true,
		width		: 610,
		height		: 195,
        store		: dsGridListKunjungan_ResepRWI,
        border		: true,
        frame		: false,
        autoScroll	: true,
        cm			: GridListKunjunganColumnModel,
		selModel: new Ext.grid.RowSelectionModel({
			singleSelect: true,
			listeners:
			{
				rowselect: function(sm, row, rec)
				{
					currentRowSelectionListKunjunganResepRWI = undefined;
					currentRowSelectionListKunjunganResepRWI = dsGridListKunjungan_ResepRWI.getAt(row);
				}
			}
		}),
		listeners	: {
			rowclick: function( $this, rowIndex, e )
			{
				// trcellCurrentTindakan_KasirRWJ = rowIndex;
    		},
			celldblclick: function(gridView,htmlElement,columnIndex,dataRecord){
			},
			'keydown' : function(e){
				if(e.getKey() == 13){
					Ext.getCmp('cbo_DokterApotekResepRWI').setValue(currentRowSelectionListKunjunganResepRWI.data.kd_dokter);
					Ext.getCmp('txtNamaPasienResepRWI_viApotekResepRWI').setValue(currentRowSelectionListKunjunganResepRWI.data.nama);
					Ext.getCmp('txtKdPasienResepRWI_viApotekResepRWI').setValue(currentRowSelectionListKunjunganResepRWI.data.kd_pasien);
					Ext.getCmp('cbo_UnitResepRWIL').setValue(currentRowSelectionListKunjunganResepRWI.data.nama_unit);
					Ext.getCmp('txtNoSep_viResepRWI').setValue(currentRowSelectionListKunjunganResepRWI.data.no_sjp);
					Ext.getCmp('txtNoTlp_viResepRWI').setValue(currentRowSelectionListKunjunganResepRWI.data.telepon);
					Ext.getCmp('cboPilihankelompokPasienAptResepRWI').setValue(currentRowSelectionListKunjunganResepRWI.data.customer);
					Ext.getCmp('txtNoKamarApotekResepRWIL').setValue(currentRowSelectionListKunjunganResepRWI.data.no_kamar);
					Ext.getCmp('txtKamarApotekResepRWIL').setValue(currentRowSelectionListKunjunganResepRWI.data.nama_kamar);
					ResepRWI.vars.no_transaksi=currentRowSelectionListKunjunganResepRWI.data.no_transaksi;
					ResepRWI.vars.tgl_transaksi=currentRowSelectionListKunjunganResepRWI.data.tgl_transaksi;
					ResepRWI.vars.kd_kasir=currentRowSelectionListKunjunganResepRWI.data.kd_kasir;
					ResepRWI.vars.urut_masuk=currentRowSelectionListKunjunganResepRWI.data.urut_masuk;
					ResepRWI.vars.payment=currentRowSelectionListKunjunganResepRWI.data.payment;
					ResepRWI.vars.payment_type=currentRowSelectionListKunjunganResepRWI.data.payment_type;
					Ext.getCmp('txtSpesialApotekResepRWIL').setValue(currentRowSelectionListKunjunganResepRWI.data.spesialisasi);
					Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').setValue(currentRowSelectionListKunjunganResepRWI.data.kd_customer);
					Ext.getCmp('txtTmpKdDokterApotekResepRWIL').setValue(currentRowSelectionListKunjunganResepRWI.data.kd_dokter);
					Ext.getCmp('txtTmpKdUnitApotekResepRWIL').setValue(currentRowSelectionListKunjunganResepRWI.data.kd_unit);
					Ext.getCmp('txtTmpKdPayResepRWIL').setValue(currentRowSelectionListKunjunganResepRWI.data.kd_pay);
					Ext.getCmp('txtTmpJenisPayResepRWIL').setValue(currentRowSelectionListKunjunganResepRWI.data.jenis_pay);
					Ext.getCmp('btnAddObatRWI').enable();
					Ext.getCmp('btnSimpan_viApotekResepRWI').enable();
					Ext.getCmp('btnSimpanExit_viApotekResepRWI').enable();
					Ext.getCmp('btnDelete_viApotekResepRWI').enable();
					
					kd_spesial_tr=currentRowSelectionListKunjunganResepRWI.data.kd_spesial;
					kd_unit_kamar_tr=currentRowSelectionListKunjunganResepRWI.data.kd_unit_kamar;
					no_kamar_tr=currentRowSelectionListKunjunganResepRWI.data.no_kamar;
					ordermanajemenrwi=false;
				
					Ext.getCmp('btnSimpan_viApotekResepRWI').enable();
					Ext.getCmp('btnSimpanExit_viApotekResepRWI').enable();
					Ext.getCmp('btnBayar_viApotekResepRWI').disable();
					Ext.getCmp('btnDelete_viApotekResepRWI').enable();
					Ext.getCmp('txtCatatanResepRWI_viResepRWI').focus(true,10);
					
					WindowLookUpKunjungan_ResepRWI.close();
				}
			},
		},
		viewConfig	: {forceFit: true}
    });
	return GridListKunjungan_ResepRWI;
}
//---------------------------

function getItemGridTransaksi_viApotekResepRWI(lebar){
    var items ={
	    layout: 'fit',
		flex:2,
	    labelAlign: 'Left',
		style:'padding-top: 4px;',
	    tbar:[
			{
				text	: 'Tambah Obat',
				id		: 'btnAddObatRWI',
				tooltip	: nmLookup,
				disabled: false,
				iconCls	: 'find',
				handler	: function(){
					var kd_customer=Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').getValue();
					if(kd_customer ==='' ||kd_customer ==='Kelompok Pasien'){
						
					}else{
						getAdm(kd_customer);
					}
					var records = new Array();
					Ext.getCmp('btnSimpan_viApotekResepRWI').enable();
					Ext.getCmp('btnSimpanExit_viApotekResepRWI').enable();
					// Ext.getCmp('btnBayar_viApotekResepRWI').disable();
					Ext.getCmp('btnDelete_viApotekResepRWI').enable();
					
					setTimeout(function(){ 
						records.push(new dsDataGrdJab_viApotekResepRWI.recordType());
						dsDataGrdJab_viApotekResepRWI.add(records);
						Ext.getCmp('btnRacikan_viApotekResepRWI').disable();
						var line = dsDataGrdJab_viApotekResepRWI.getCount()-1;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].set('racikan',false);
						dsDataGrdJab_viApotekResepRWI.getRange()[line].set('racikan_text','Tidak');
						ResepRWI.form.Grid.a.startEditing(line, 6);	
					}, 200);
				}
			},'-',{
				xtype: 'button',
				text: 'Hapus',
				iconCls: 'remove',
				disabled:true,
				id: 'btnDelete_viApotekResepRWI',
				handler: function(){
					var line = ResepRWI.form.Grid.a.getSelectionModel().selection.cell[0];
					var o = dsDataGrdJab_viApotekResepRWI.getRange()[line].data;
					if(dsDataGrdJab_viApotekResepRWI.getCount()>1){
						Ext.Msg.confirm('Warning', 'Apakah data resep obat ini akan dihapus?', function(button){
							if (button == 'yes'){
								if(dsDataGrdJab_viApotekResepRWI.getRange()[line].data.no_out != undefined){
									Ext.Ajax.request({
										url: baseURL + "index.php/apotek/functionAPOTEKrwi/hapusBarisGridResepRWI",
										params:{no_out:o.no_out, tgl_out:o.tgl_out, kd_prd:o.kd_prd, kd_milik:o.kd_milik, no_urut:o.no_urut} ,
										failure: function(o){
											ShowPesanErrorResepRWI('Hubungi Admin', 'Error');
										},	
										success: function(o){
											var cst = Ext.decode(o.responseText);
											if (cst.success === true){
												dsDataGrdJab_viApotekResepRWI.removeAt(line);
												ResepRWI.form.Grid.a.getView().refresh();
												hasilJumlahResepRWI();
												Ext.getCmp('btnSimpan_viApotekResepRWI').enable();
												Ext.getCmp('btnSimpanExit_viApotekResepRWI').enable();
												Ext.getCmp('btnBayar_viApotekResepRWI').disable();
												hasilJumlahResepRWI();
												
											}else{
												ShowPesanErrorResepRWI('Gagal melakukan penghapusan', 'Error');
											}
										}
									});
								}else{
									dsDataGrdJab_viApotekResepRWI.removeAt(line);
									ResepRWI.form.Grid.a.getView().refresh();
									hasilJumlahResepRWI();
									Ext.getCmp('btnSimpan_viApotekResepRWI').enable();
									Ext.getCmp('btnSimpanExit_viApotekResepRWI').enable();
								}

								if (o.racikan === true || o.racikan == 'true') {
									var tmp_total_racik =parseInt(Ext.getCmp('txtJumlahGrupRacik_viApotekResepRWI').getValue()) - 1;
									Ext.getCmp('txtJumlahGrupRacik_viApotekResepRWI').setValue(tmp_total_racik);

									Ext.Ajax.request({
										url: baseURL + "index.php/apotek/functionAPOTEK/hitung_nilai_adm_racik",
										params: {jml_grup_racik:tmp_total_racik,jml_item_racik:tmp_total_racik},
										failure: function(o)
										{
											ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
										},	
										success: function(o) 
										{
											var cst = Ext.decode(o.responseText);
											if (cst.success === true) 
											{
												Ext.getCmp('txtAdmRacikResepRWIL').setValue(cst.nilai_adm_racik);
												hasilJumlahResepRWI();
											}
											else 
											{
												ShowPesanWarningResepRWJ('Gagal mendapatkan konfigurasi perhitungan racik!','Warning');
												
											};
										}
									});
								}
							} 
						});
					} else{
						ShowPesanErrorResepRWI('Data tidak bisa dihapus karena minimal resep 1 obat','Error');
					}
				}
			}	
		],
		items:gridDataViewEdit_viApotekResepRWI()
	};
    return items;
}
function setLookUp_bayarResepRWI(rowdata){
    var lebar = 450;
    setLookUpApotek_bayarResepRWI = new Ext.Window({
		id: 'setLookUpApotek_bayarResepRWI',
		name: 'setLookUpApotek_bayarResepRWI',
		title: 'Pembayaran Resep Rawat Inap', 
		closeAction: 'destroy',  
		width: 523,
		height: 230,		
		resizable:false,
		emptyText:'Pilih Jenis Pembayaran...',
		autoScroll: false,
		border: true,
		constrain : true,    
		iconCls: 'Studi_Lanjut',
		modal: true,		
		items: [ 
			getItemPanelBiodataPembayaran_viApotekResepRWI(lebar,rowdata),
			getItemPanelBiodataUang_viApotekResepRWI(lebar,rowdata)
		],
		listeners:{
			deactivate: function(){
				//rowSelected_viApotekResepRWI=undefined;
			}
		}
	});
    setLookUpApotek_bayarResepRWI.show();
	initLookupPembayaranResepRWI();
}
function initLookupPembayaranResepRWI(){
	var stmpnoOut=0;
	var stmptgl;
	Ext.getCmp('txtNamaPasien_PembayaranRWI').setValue(Ext.getCmp('txtNamaPasienResepRWI_viApotekResepRWI').getValue());
	
	// Ext.getCmp('txtkdPasien_PembayaranRWI').setValue(Ext.getCmp('cboKodePasienResepRWI').getValue());
	Ext.getCmp('txtkdPasien_PembayaranRWI').setValue(Ext.getCmp('txtKdPasienResepRWI_viApotekResepRWI').getValue());
	Ext.getCmp('txtNoResepRWI_Pembayaran').setValue(Ext.get('txtNoResepApotekResepRWIL').getValue());
	Ext.getCmp('dftanggalResepRWI_Pembayaran').setValue(Ext.getCmp('dfTglResepSebenarnyaResepRWI').getValue());
	stmpnoOut=Ext.getCmp('txtTmpNooutResepRWIL').getValue();
	stmptgl=Ext.getCmp('txtTmpTgloutResepRWIL').getValue();
	if(Ext.getCmp('txtTmpNooutResepRWIL').getValue() == 'No out' || Ext.getCmp('txtTmpNooutResepRWIL').getValue() == ''){
		Ext.getCmp('txtTotalResepRWI_Pembayaran').setValue(Ext.getCmp('txtTotalBayarResepRWIL').getValue());
		Ext.getCmp('txtBayarResepRWI_Pembayaran').setValue(Ext.getCmp('txtTotalBayarResepRWIL').getValue());
	}else{
		getSisaAngsuran(stmpnoOut,stmptgl);
	}
	Ext.getCmp('cboJenisByrResepRWI').setValue(ResepRWI.vars.payment_type);
	Ext.getCmp('cboPembayaranRWI').setValue(ResepRWI.vars.payment);
	Ext.getCmp('txtBayarResepRWI_Pembayaran').focus(false,10);
}


// End Function setLookUpGridDataView_viApotekResepRWJ # --------------

function getItemPanelBiodataPembayaran_viApotekResepRWI(lebar,rowdata) 
{ 
	var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		labelWidth: 100,
		autoWidth: true,
		border:true,
		items:
		[			
			{
                xtype: 'compositefield',
                fieldLabel: 'No. Resep ',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtNoResepRWI_Pembayaran',
						id: 'txtNoResepRWI_Pembayaran',
						emptyText: 'No Resep',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},
					{
						xtype: 'displayfield',
						flex: 1,
						width: 70,
						name: '',
						value: 'Tanggal :',
						fieldLabel: 'Label'
					},
					{
						xtype: 'datefield',
						flex: 1,
						width : 150,	
						format: 'd/M/Y',
						name: 'dftanggalResepRWI_Pembayaran',
						id: 'dftanggalResepRWI_Pembayaran',
						// readOnly:true
					}
				
                ]
            },
            //-------------- ## --------------  
			{
                xtype: 'compositefield',
                fieldLabel: 'Kode Pasien ',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					//-------------- ## --------------  
					{
						xtype: 'textfield',
						width : 150,	
						name: 'txtkdPasien_PembayaranRWI',
						id: 'txtkdPasien_PembayaranRWI',
						readOnly:true
					},	
					{
						xtype: 'textfield',
						width : 225,	
						name: 'txtNamaPasien_PembayaranRWI',
						id: 'txtNamaPasien_PembayaranRWI',
						readOnly:true
					}
					
                ]
            },
			{
				xtype: 'compositefield',
				fieldLabel: 'Pembayaran ',
				anchor: '100%',
				//labelSeparator: '',
				width: 199,
				items: 
				[
					mComboJenisByrResepRWI(),
					mComboPembayaranRWI()
				]
			}
					
		]
	};
    return items;
};



function getItemPanelBiodataUang_viApotekResepRWI(lebar,rowdata) 
{ 
	var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		labelWidth: 100,
		autoWidth: true,
		border:true,
		items:
		[			
			{
                xtype: 'compositefield',
                fieldLabel: ' ',
				labelSeparator: '',
                anchor: '100%',
                width: 250,
                items: 
                [         
					{
						xtype: 'displayfield',
						flex: 1,
						width: 150,
						name: '',
						value: ''
					},	
					{
						xtype: 'displayfield',
						flex: 1,
						width: 70,
						name: '',
						value: 'Total :',
						fieldLabel: 'Label'
					},
					{
						xtype: 'textfield',
						width : 150,	
						readOnly: true,
						style:{'text-align':'right'},
						name: 'txtTotalResepRWI_Pembayaran',
						id: 'txtTotalResepRWI_Pembayaran',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					}
					
                ]
            },
			{
                xtype: 'compositefield',
                fieldLabel: ' ',
				labelSeparator: '',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					//-------------- ## -------------- 
					{
						xtype: 'displayfield',
						flex: 1,
						width: 150,
						name: '',
						value: ''
					},	
					{
						xtype: 'displayfield',
						flex: 1,
						width: 70,
						name: '',
						value: 'Bayar :',
						fieldLabel: 'Label'
					},						
					{
						xtype: 'numberfield',
						width : 150,	
						//readOnly: true,
						style:{'text-align':'right'},
						name: 'txtBayarResepRWI_Pembayaran',
						id: 'txtBayarResepRWI_Pembayaran',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									pembayaranResepRWI();
								};
							}
						}
					},
					{
						xtype:'button',
						text:'1/2 Resep',
						width:70,
						//style:{'margin-left':'190px','margin-top':'7px'},
						hideLabel:true,
						hidden:true,
						id: 'btn1/2resep_viApotekResepRWI',
						handler:function()
						{
						}   
					}
                ]
            },
			{
                xtype: 'compositefield',
                fieldLabel: ' ',
				labelSeparator: '',
                anchor: '100%',
                width: 230,
                items: 
                [
				{
						xtype: 'displayfield',
						flex: 1,
						width: 305,
						name: '',
						value: ''
					},
					{
						xtype:'button',
						text:'Paid',
						width:70,
						//style:{'margin-left':'190px','margin-top':'7px'},
						hideLabel:true,
						id: 'btnBayar_ResepRWILookupBayar',
						handler:function()
						{
							pembayaranResepRWI();
						}   
					}
				]
            }
		]
	};
    return items;
};


function setLookUp_TransferResepRWI(rowdata)
{
    var lebar = 450;
	var stmpnoOut=0;
	var stmptgl;
    setLookUpApotek_TransferResepRWI = new Ext.Window
    (
    {
        id: 'setLookUpApotek_transferResepRWI',
		name: 'setLookUpApotek_transferResepRWI',
        title: 'Transfer Pembayaran Resep', 
        closeAction: 'destroy',        
        width: 523,
        height: 240,
        resizable:false,
		autoScroll: false,
        border: true,
        constrain : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: [ 
			getItemPanelBiodataTransfer_viApotekResepRWI(),
			getItemPanelTotalBayar_ApotekResepRWI()
		],//1
		fbar:[
			{
				xtype:'button',
				text:'Transfer',
				width:70,
				hideLabel:true,
				id: 'btnTransfer_viApotekResepRWIL',
				handler:function()
				{
					transferResepRWI();
				}   
			},
			{
				xtype:'button',
				text:'Batal',
				width:70,
				hideLabel:true,
				id: 'btnBatalTransfer_viApotekResepRWIL',
				handler:function()
				{
					setLookUpApotek_TransferResepRWI.close();
				}   
			}
		],
        listeners:
        {
            activate: function()
            {
				
            },
            afterShow: function()
            {
                this.activate();
				
				// ;
            },
            deactivate: function()
            {
                rowSelected_viApotekResepRWI=undefined;
            }
        }
    }
    );

    setLookUpApotek_TransferResepRWI.show();
	Ext.getCmp('txtNamaPasienTransfer_ResepRWI').setValue(Ext.getCmp('txtNamaPasienResepRWI_viApotekResepRWI').getValue());
	
	Ext.getCmp('txtNoTransaksiTransfer_ResepRWI').setValue(ResepRWI.vars.no_transaksi);
	Ext.getCmp('dftanggalTransaksi_ResepRWI').setValue(ShowDate(ResepRWI.vars.tgl_transaksi));
	
	// Ext.getCmp('txtkdPasienTransfer_ResepRWI').setValue(Ext.getCmp('cboKodePasienResepRWI').getValue());
	Ext.getCmp('txtkdPasienTransfer_ResepRWI').setValue(Ext.getCmp('txtKdPasienResepRWI_viApotekResepRWI').getValue());
	Ext.getCmp('txtNoResepTransfer_ResepRWI').setValue(Ext.get('txtNoResepApotekResepRWIL').getValue());
	Ext.getCmp('dftanggalTransfer_ResepRWI').setValue(Ext.getCmp('dfTglResepSebenarnyaResepRWI').getValue());
	stmpnoOut=Ext.getCmp('txtTmpNooutResepRWIL').getValue();
	stmptgl=Ext.getCmp('txtTmpTgloutResepRWIL').getValue();
	Ext.getCmp('txtTotalBayarTransfer_ResepRWI').setValue(Ext.getCmp('txtTotalBayarResepRWIL').getValue());
	Ext.getCmp('txtTotalTransfer_ResepRWI').setValue(toInteger(Ext.getCmp('txtTotalBayarResepRWIL').getValue()));
	Ext.getCmp('txtTotalTransfer_ResepRWI').focus(false,10);
	
}
// End Function setLookUpGridDataView_viApotekResepRWJ # --------------

function getItemPanelBiodataTransfer_viApotekResepRWI(lebar,rowdata) 
{ 
	var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		labelWidth: 100,
		autoWidth: true,
		border:true,
		items:
		[			
			{
                xtype: 'compositefield',
                fieldLabel: 'No. Transaksi ',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtNoTransaksiTransfer_ResepRWI',
						id: 'txtNoTransaksiTransfer_ResepRWI',
						emptyText: 'No Transaksi',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},
					{
						xtype: 'displayfield',
						flex: 1,
						width: 90,
						name: '',
						value: 'Tgl. Masuk :',
						fieldLabel: 'Label'
					},
					{
						xtype: 'datefield',
						flex: 1,
						width : 130,	
						format: 'd/M/Y',
						name: 'dftanggalTransaksi_ResepRWI',
						id: 'dftanggalTransaksi_ResepRWI',
						readOnly:true
					}
				
                ]
            },
			{
                xtype: 'compositefield',
                fieldLabel: 'No. Resep ',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					{
						xtype: 'textfield',
						flex: 1,
						width : 150,	
						readOnly: true,
						name: 'txtNoResepTransfer_ResepRWI',
						id: 'txtNoResepTransfer_ResepRWI',
						emptyText: 'No Resep',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},
					{
						xtype: 'displayfield',
						flex: 1,
						width: 90,
						name: '',
						value: 'Tgl. Transfer :',
						fieldLabel: 'Label'
					},
					{
						xtype: 'datefield',
						flex: 1,
						width : 130,	
						format: 'd/M/Y',
						name: 'dftanggalTransfer_ResepRWI',
						id: 'dftanggalTransfer_ResepRWI',
						// readOnly:true
					}
				
                ]
            },
            //-------------- ## --------------  
			{
                xtype: 'compositefield',
                fieldLabel: 'Pasien ',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					//-------------- ## --------------  
					{
						xtype: 'textfield',
						width : 150,	
						name: 'txtkdPasienTransfer_ResepRWI',
						id: 'txtkdPasienTransfer_ResepRWI',
						readOnly:true
					},	
					{
						xtype: 'textfield',
						width : 225,	
						name: 'txtNamaPasienTransfer_ResepRWI',
						id: 'txtNamaPasienTransfer_ResepRWI',
						readOnly:true
					}
					
                ]
            }
					
		]
	};
    return items;
};



function getItemPanelTotalBayar_ApotekResepRWI(lebar,rowdata) 
{ 
	var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		labelWidth: 100,
		autoWidth: true,
		border:true,
		items:
		[			
			{
                xtype: 'compositefield',
                fieldLabel: ' ',
				labelSeparator: '',
                anchor: '100%',
                width: 250,
                items: 
                [         
					{
						xtype: 'displayfield',
						flex: 1,
						width: 150,
						name: '',
						value: ''
					},	
					{
						xtype: 'displayfield',
						flex: 1,
						width: 90,
						name: '',
						value: 'Total biaya:',
						fieldLabel: 'Label'
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 130,	
						readOnly: true,
						style:{'text-align':'right'},
						name: 'txtTotalBayarTransfer_ResepRWI',
						id: 'txtTotalBayarTransfer_ResepRWI',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									transferResepRWI();
								};
							}
						}
					}
					
                ]
            },
			{
                xtype: 'compositefield',
                fieldLabel: ' ',
				labelSeparator: '',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					//-------------- ## -------------- 
					{
						xtype: 'displayfield',
						flex: 1,
						width: 150,
						name: '',
						value: ''
					},	
					{
						xtype: 'displayfield',
						flex: 1,
						width: 90,
						name: '',
						value: 'Total transfer :',
						fieldLabel: 'Label'
					},						
					{
						xtype: 'textfield',
						flex: 1,
						width : 130,	
						readOnly: true,
						style:{'text-align':'right'},
						name: 'txtTotalTransfer_ResepRWI',
						id: 'txtTotalTransfer_ResepRWI',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									transferResepRWI();
								};
							}
						}
					},
					
                ]
            },
		]
	};
    return items;
}
function getItemGridHistoryBayar_viApotekResepRWI(lebar) {
    var items ={
		title: 'Histori Bayar', 
	    layout: 'fit',
		flex:1,
		style: 'padding-top: 4px;',
		border:true,
	    items:gridDataViewHistoryBayar_viApotekResepRWI()
	};
    return items;
}
function gridDataViewHistoryBayar_viApotekResepRWI() {
    var fldDetail = ['TUTUP','KD_PASIENAPT','TGL_OUT','NO_OUT','URUT','TGL_BAYAR','KD_PAY','URAIAN','JUMLAH','JML_TERIMA_UANG','SISA'];
    dsTRDetailHistoryBayar = new WebApp.DataStore({ fields: fldDetail })
    gridDTLTRHistoryApotekRWI = new Ext.grid.EditorGridPanel({
		store: dsTRDetailHistoryBayar,
		border: false,
		columnLines: true,
		autoScroll:true,
		selModel: new Ext.grid.CellSelectionModel({
			singleSelect: true,
			listeners:{
				cellselect: function(sm, row, rec){
					//cellSelecteddeskripsiRWI = dsTRDetailHistoryBayarList.getAt(row);
					//CurrentHistoryRWI.row = row;
					//CurrentHistoryRWI.data = cellSelecteddeskripsiRWI;
				}
			}
		}),
		stripeRows: true,
		columns:[
			{
				id: 'colStatPost',
				header: 'Status Posting',
				dataIndex: 'TUTUP',
				width:100,
				hidden:true
			},{
				id: 'colKdPsien',
				header: 'Kode Pasien',
				dataIndex: 'KD_PASIENAPT',
				width:100,
				hidden:true
			},{
				id: 'colNoOut',
				header: 'No out',
				dataIndex: 'NO_OUT',
				width:100,
				hidden:true
			},{
				id: 'coleurutmasuk',
				header: 'Urut Bayar',
				dataIndex: 'URUT',
				align :'center',
				width:90
				
			},{
				id: 'colTGlout',
				header: 'Tanggal Resep',
				dataIndex: 'TGL_OUT',
				align :'center',
				width:130,
				renderer: function(v, params, record){
				   return ShowDate(record.data.TGL_OUT);
				} 
			},{
				id: 'colePembayaran',
				header: 'Pembayaran',
				dataIndex: 'URAIAN',
				align :'center',
				width:120,
				hidden:false
			},{
				id: 'colTGlBayar',
				header: 'Tanggal Bayar',
				dataIndex: 'TGL_BAYAR',
				align :'center',
				width:130,
				renderer: function(v, params, record){
				   return ShowDate(record.data.TGL_BAYAR);
				} 
			},{
				id: 'colJumlah',
				header: 'Total Bayar',
				width:130,
				format : '0,000',
				xtype:'numbercolumn',
				align :'right',
				dataIndex: 'JUMLAH',
				renderer: function(v, params, record){
				   return formatCurrency(record.data.JUMLAH);
				}
			},{
				id: 'colJumlahTerimaUang',
				header: 'Jumlah Angsuran',
				width:130,
				xtype:'numbercolumn',
				format : '0,000',
				align :'right',
				dataIndex: 'JML_TERIMA_UANG',
				renderer: function(v, params, record){
				   return formatCurrency(record.data.JML_TERIMA_UANG);
				}
			},{
				id: 'colSisa',
				header: 'Sisa',
				width:130,
				xtype:'numbercolumn',
				align :'right',
				format : '0,000',
				dataIndex: 'SISA',
				renderer: function(v, params, record){
				   return formatCurrency(record.data.SISA);
				}
			},{
				id: 'colKDPAY',
				header: 'kd_pay',
				dataIndex: 'KD_PAY',
				align :'center',
				width:120,
				hidden:true
			},
		],
		//cm: TRHistoryColumModelApotekRWI(),
	});
    return gridDTLTRHistoryApotekRWI;
}
function TRHistoryColumModelApotekRWI() 
{
    return new Ext.grid.ColumnModel
    (
        [
          	{
				id: 'colStatPost',
				header: 'Status Posting',
				dataIndex: 'TUTUP',
				width:100,
				hidden:true
			},
			{
				id: 'colKdPsien',
				header: 'Kode Pasien',
				dataIndex: 'KD_PASIENAPT',
				width:100,
				hidden:true
			},
			{
				id: 'colNoOut',
				header: 'No out',
				dataIndex: 'NO_OUT',
				width:100,
				hidden:true
			},
			{
				id: 'coleurutmasuk',
				header: 'Urut Bayar',
				dataIndex: 'URUT',
				align :'center',
				width:90
				
			},
			{
				id: 'colTGlout',
				header: 'Tanggal Resep',
				dataIndex: 'TGL_OUT',
				align :'center',
				width:130,
				renderer: function(v, params, record)
				{
				   return ShowDate(record.data.TGL_BAYAR);
	
				} 
			},
			{
				id: 'colePembayaran',
				header: 'Pembayaran',
				dataIndex: 'URAIAN',
				align :'center',
				width:100,
				hidden:false
				
			},
			{
				id: 'colTGlout',
				header: 'Tanggal Bayar',
				dataIndex: 'TGL_BAYAR',
				align :'center',
				width:130,
				renderer: function(v, params, record)
				{
				   return ShowDate(record.data.TGL_BAYAR);
	
				} 
			},
			{
				id: 'colJumlah',
				header: 'Total Bayar',
				width:130,
				xtype:'numbercolumn',
				align :'right',
				dataIndex: 'JUMLAH',
				renderer: function(v, params, record)
				{
				   return formatCurrency(record.data.JUMLAH);
	
				}
				
			},
			{
				id: 'colJumlah',
				header: 'Jumlah Angsuran',
				width:130,
				xtype:'numbercolumn',
				align :'right',
				dataIndex: 'JML_TERIMA_UANG',
				renderer: function(v, params, record)
				{
				   return formatCurrency(record.data.JML_TERIMA_UANG);
	
				}
				
			},
			{
				id: 'colJumlah',
				header: 'Sisa',
				width:130,
				xtype:'numbercolumn',
				align :'right',
				dataIndex: 'SISA',
				renderer: function(v, params, record)
				{
				   return formatCurrency(record.data.SISA);
	
				}
				
			}
			

        ]
    )
};
function showSignatureRWJ(line){
	var signa=dsDataGrdJab_viApotekResepRWI.data.items[line].data.signa;
	var spSigna=[];
	if(signa != undefined){
		spSigna=signa.split('X');
	}
	var signa1='';
	var signa2='';
	if(spSigna.length>1){
		signa1=spSigna[0];
		signa2=spSigna[1];
	}
	var showSignatureIGD=new Ext.Window({
		title:'Signature',
		closeAction:'destroy',
		modal:true,
		layout:'fit',
		constraint:true,
		items:[
			{
				xtype:'panel',
				layout:'column',
				height:30,
				style:'background:#ffffff;',
				bodyStyle:'padding: 4px;',
				width: 200,
				border:false,
				items:[
					{
						xtype:'displayfield',
						value:'Signature :&nbsp;'
					},{
						xtype:'numberfield',
						id:'txtSignatureIGD1',
						width: 50,
						value:signa1,
						listeners       :{
							'specialkey' : function(a){
								if (Ext.EventObject.getKey() === 13) {
									if(a.getValue() != '' && a.getValue() != 0){
										Ext.getCmp('txtSignatureIGD2').focus();	
									}
								} 						
							}
						}
					},{
						xtype:'displayfield',
						value:'&nbsp;X&nbsp;'
					},{
						xtype:'numberfield',
						id:'txtSignatureIGD2',
						width: 50,
						value:signa2,
						listeners       :{
							'specialkey' : function(a){
								if (Ext.EventObject.getKey() === 13) {
									if(a.getValue() != '' && a.getValue() != 0){
										dsDataGrdJab_viApotekResepRWI.getRange()[line].set('signa',Ext.getCmp('txtSignatureIGD1').getValue()+'X'+a.getValue());
										showSignatureIGD.close();		
										ResepRWI.form.Grid.a.startEditing(line, 15);	
									}
								} 						
							}
						}
					}
				]
			}
		],
		fbar:[
			{
				text:'Ok',
				handler:function(){
					var err=false;
					if(Ext.getCmp('txtSignatureIGD1').getValue() == '' || Ext.getCmp('txtSignatureIGD1').getValue() == 0){
						err=true;
						Ext.getCmp('txtSignatureIGD1').focus();
					}
					if(err==false && (Ext.getCmp('txtSignatureIGD2').getValue() == '' || Ext.getCmp('txtSignatureIGD2').getValue() == 0)){
						err=true;
						Ext.getCmp('txtSignatureIGD2').focus();
					}
					if(err==false){
						dsDataGrdJab_viApotekResepRWI.getRange()[line].set('signa',Ext.getCmp('txtSignatureIGD1').getValue()+'X'+Ext.getCmp('txtSignatureIGD2').getValue());
						showSignatureIGD.close();		
						ResepRWI.form.Grid.a.startEditing(line, 15);	
					}
				}
			}
		]
	}).show();
	Ext.getCmp('txtSignatureIGD1').focus(true,100);
}
function showRacikRWJ(line, edited){
	var fldDetail = ['catatan_racik','cito','kd_prd','nama_obat','kd_satuan','fractions','harga_jual','harga_beli','kd_pabrik','markup','adm_racik','jasa','no_out','no_urut','tgl_out','kd_milik','jml_stok_apt','milik','hargaaslicito','nilai_cito','disc'];
	dsRacikan_IGD = new WebApp.DataStore({ fields: fldDetail });
	gridRacikanIgd=null;
	if(dsDataGrdJab_viApotekResepRWI.data.items[line].data.result != undefined && dsDataGrdJab_viApotekResepRWI.data.items[line].data.result != '' && dsDataGrdJab_viApotekResepRWI.data.items[line].data.result!== null){
		var parse=JSON.parse(dsDataGrdJab_viApotekResepRWI.data.items[line].data.result);
		for(var i=0,iLen=parse.length; i<iLen;i++){
			var records = new Array();
			records.push(new dsRacikan_IGD.recordType(parse[i]));
			dsRacikan_IGD.add(records);
		}
		
	}
	var showSignatureIGD=new Ext.Window({
		title:'Racikan',
		closeAction:'destroy',
		modal:true,
		width: 800,
		height: 400,
		layout:{
			type:'vbox',
			align:'stretch'
		},
		constrain:true,
		items:[
			{
				xtype:'panel',
				layout:'form',
				style:'padding: 4px;',
				border:false,
				items:[
					new Ext.form.ComboBox ( {
						id				: 'gridcbo_aturan_racik',
						typeAhead		: true,
						triggerAction	: 'all',
						lazyRender		: true,
						fieldLabel		:'Aturan Racik',
						mode			: 'local',
						selectOnFocus	: true,
						forceSelection	: true,
						store			: ds_cbo_aturan_racik,
						valueField		: 'kd_racik_atr',
						displayField	: 'racik_aturan',
						value		: dsDataGrdJab_viApotekResepRWI.data.items[line].data.aturan_racik,
						listeners	: {
							select	: function(a,b,c){
								if(a.getValue() != undefined){
									Ext.getCmp('gridcbo_aturan_pakai').focus(true,10);
								}
							}
						}
					}),
					new Ext.form.ComboBox ( {
						id				: 'gridcbo_aturan_pakai',
						typeAhead		: true,
						triggerAction	: 'all',
						lazyRender		: true,
						mode			: 'local',
						fieldLabel		:'Aturan Pakai',
						selectOnFocus	: true,
						forceSelection	: true,
						store			: ds_cbo_aturan_pakai,
						valueField		: 'kd_racik_atr_pk',
						displayField	: 'singkatan',
						value		: dsDataGrdJab_viApotekResepRWI.data.items[line].data.aturan_pakai,
						listeners	: {
							select	: function(a,b,c){
								if(a.getValue() != undefined){
									Ext.getCmp('BtnTambahObatTrKasirIGD_racikan').focus(true,100);
								}
							}
						}
					})
				]
			},
			gridRacikanIgd=new Ext.grid.EditorGridPanel({
				stripeRows: true,
				flex:1,
				border:false,
				style:'margin-top:-1px;',
				store: dsRacikan_IGD,
				autoScroll:true,
				cm: new Ext.grid.ColumnModel([
					new Ext.grid.RowNumberer(),
					{
						dataIndex: 'kd_milik',
						header: 'M',
						hidden:true,
						width: 30
					},{
						dataIndex: 'cito',
						header: 'C',
						width: 50,
						hidden:true,
						align:'center',
						editor: new Ext.form.ComboBox ( {
							id				: 'gridcboCito_ResepRWJ',
							typeAhead		: true,
							triggerAction	: 'all',
							lazyRender		: true,
							mode			: 'local',
							selectOnFocus	: true,
							forceSelection	: true,
							width			: 50,
							anchor			: '95%',
							value			: 'Tidak',
							store			: new Ext.data.ArrayStore({
								id		: 0,
								fields	:['Id','displayText'],
								data	: [[0, 'Tidak'],[1, 'Ya']]
							}),
							valueField	: 'displayText',
							displayField: 'displayText',
							listeners	: {
								select	: function(a,b,c){
									var line	= gridRacikanIgd.getSelectionModel().selection.cell[0];
									var o = dsRacikan_IGD.data.items[line].data;
									if(b.data.Id == 1) {
										if(o.harga_jual=='' ||o.harga_jual==undefined){
											ShowPesanWarningResepRWJ("Obat yang akan di Cito masih kosong!","Warning");
										} else{
											currentHargaJualObat=o.harga_jual;
											formulacitoResepRWJ();
										}
									} else{
										var line	= gridRacikanIgd.getSelectionModel().selection.cell[0];
										var o = dsRacikan_IGD.data.items[line].data;
										o.harga_jual=o.hargaaslicito;
										o.nilai_cito=0;
										o.cito="Tidak";
										hasilJumlahRacik();
										gridRacikanIgd.startEditing(line,4);	
									}
								}
							}
						})
					},{
						header		: 'Kd. Obat',
						dataIndex	: 'kd_prd',
						width		: 60,
						menuDisabled: true,
					},{
						header				: 'Nama Obat',
						dataIndex			: 'nama_obat',
						flex:1,
						menuDisabled		: true,
						editor: new Ext.form.TextField({
							allowBlank: false,
							enableKeyEvents:true,
							listeners:{
								keyDown: function(a,b,c){
									if(b.getKey()==13){
										var line	= gridRacikanIgd.getSelectionModel().selection.cell[0];
										if(a.getValue().length < 1){
											if(a.getValue().length != 0){
												Ext.Msg.show({
													title: 'Perhatian',
													msg: 'Kriteria Pencarian Obat Tidak Boleh Kosong!',
													buttons: Ext.MessageBox.OK,
													fn: function (btn) {
														if (btn == 'ok'){
															gridRacikanIgd.startEditing(line, 4);
														}
													}
												});
											}
										} else{		
											PencarianLookupResep = true;// Variabel pembeda getdata u/nama obat dan semua kepemilikan atau bukan. False => pencarian berdasarkan nama_obat dan kepemilikan, True => pencarian berdasarkan nama_obat saja
											FocusExitResepRWJ = false;
											LookUpSearchListGetObat_resepRWI(a.getValue(),'Y');
										}
									}
								}
							}
						})
					},{
						dataIndex: 'kd_satuan',
						header: 'Satuan',
						width: 60,
					},{
						dataIndex: 'min_stok',
						header: 'min stok',
						sortable: true,
						xtype:'numbercolumn',
						hidden: true,
						align:'right',
						width: 85
					},{
						dataIndex: 'harga_jual',
						header: 'Harga Sat',
						xtype:'numbercolumn',
						sortable: true,
						align:'right',
						format : '0,000',
						width: 85
					},{
						dataIndex: 'jml',
						header: 'Qty',
						sortable: true,
						width: 50,
						align:'right',
						editor: new Ext.form.NumberField({
							allowBlank: false,
							enableKeyEvents:true,
							listeners:{
								keyDown: function(a,b,c){
									if(b.getKey()==13){
										var line	= this.index;
										if(a.getValue()==''){
											Ext.Msg.show({
												title: 'Perhatian',
												msg: 'Qty obat belum di isi!',
												buttons: Ext.MessageBox.OK,
												fn: function (btn) {
													if (btn == 'ok'){
														var thisrow = dsRacikan_IGD.getCount()-1; 
														gridRacikanIgd.startEditing(thisrow, 8);
													}
												}
											});
										}else{
											statusRacikan_ResepRWJ=0;
											var o=dsRacikan_IGD.getRange()[line].data;
											var tmp_jml_stok=0;
											var tmp_min_stok=0;
											Ext.Ajax.request({
												url: baseURL + "index.php/apotek/functionAPOTEK/getStokObat",
												params: {kd_prd: o.kd_prd, kd_milik :o.kd_milik},
												failure: function(o)
												{
													 var cst = Ext.decode(o.responseText);
												},	    
												success: function(o) {
													var cst = Ext.decode(o.responseText);
													if (cst.success==true)
													{
														tmp_jml_stok = cst.jml_stok;
														tmp_min_stok = cst.min_stok;
														console.log( parseFloat(tmp_jml_stok));
														console.log( parseFloat(tmp_min_stok));
														if ( parseFloat(tmp_min_stok) == parseFloat(tmp_jml_stok)){
															Ext.Msg.show({
																title: 'Perhatian',
																msg: 'Sisa stok sudah mencapai atau melebihi minimum stok!',
																buttons: Ext.MessageBox.OK,
																fn: function (btn) {
																	if (btn == 'ok'){
																		o.jml=a.getValue();
																		hasilJumlahRacik();
																		var thisrow = dsRacikan_IGD.getCount()-1; 
																		gridRacikanIgd.startEditing(thisrow, 8);
																	}
																}
															});
														} else if(parseFloat(a.getValue()) > parseFloat(tmp_jml_stok)){
															Ext.Msg.show({
																title: 'Perhatian',
																msg: 'Qty melebihi sisa stok tersedia,  Stok hanya tersedia ' +cst.jml_stok+ '!',
																buttons: Ext.MessageBox.OK,
																fn: function (btn) {
																	if (btn == 'ok'){
																		o.jml=tmp_jml_stok;
																		hasilJumlahRacik();
																		var thisrow = dsRacikan_IGD.getCount()-1; 
																		gridRacikanIgd.startEditing(thisrow, 8);
																	}
																}
															});
														} else{
															o.jml=a.getValue();
															hasilJumlahRacik();
														
															// var records = new Array();
															// records.push(new dsRacikan_IGD.recordType());
															// dsRacikan_IGD.add(records);
															var nextRow = dsRacikan_IGD.getCount()-1; 
															gridRacikanIgd.startEditing(nextRow, 11);
														}
														// Ext.getCmp('btnSimpan_viApotekResepRWJ').enable();
													}
												}
											})
											
											// AptResepRWJ.form.Grid.a.startEditing(line, 12);	
										}
										
									}
								},
								focus: function(a){
									this.index=gridRacikanIgd.getSelectionModel().selection.cell[0]
								}
							}
						})
					},{
						dataIndex: 'disc',
						header: 'Diskon',
						sortable: true,
						xtype:'numbercolumn',
						width: 70,
						align:'right',
						editor: new Ext.form.NumberField({
							allowBlank: false,
							listeners:{
								blur: function(a){
									var line	= this.index;
									dsRacikan_IGD.data.items[line].data.disc=a.getValue();
									hasilJumlahRacik();
								},
								focus: function(a){
									this.index=gridRacikanIgd.getSelectionModel().selection.cell[0]
								}
								
							}
						})
					},{
						dataIndex: 'jumlah',
						header: 'Sub total',
						sortable: true,
						xtype:'numbercolumn',
						width: 110,
						format : '0,000',
						align:'right'
					},{
						header		: 'Catatan',
						menuDisabled: true,
						dataIndex	: 'catatan_racik',
						width		: 100,
						editor		: new Ext.form.TextField({
							selectOnFocus	: true,
							width			: 40,
						})
					},{
						dataIndex: 'harga_beli',
						header: 'Harga Beli',
						hidden: true,
						width: 80
					},{ 
						dataIndex: 'kd_pabrik',
						header: 'Kode Pabrik',
						hidden: true,
						width: 80
					},{
						dataIndex: 'markup',
						header: 'Markup',
						hidden: true,
						width: 80
					},{
						dataIndex: 'adm_racik',
						header: 'Adm Racik',
						hidden: true,
						width: 80
					},{
						dataIndex: 'jasa',
						header: 'Jasa Tuslah',
						hidden: true,
						width: 80
					},{
						dataIndex: 'no_out',
						header: 'No Out',
						hidden: true,
						width: 80
					},{
						dataIndex: 'no_urut',
						header: 'No Urut',
						hidden: true,
						width: 80
					},{
						dataIndex: 'tgl_out',
						header: 'tgl out',
						hidden: true,
						width: 80
					},{
						dataIndex: 'jml_stok_apt',
						header: 'stok tersedia',
						hidden: true,
						width: 80
					},{
						dataIndex: 'hargaaslicito',
						header: 'hargaaslicito',
						hidden: true,
						width: 80
					},{
						dataIndex: 'nilai_cito',
						header: 'nilai_cito',
						hidden: true,
						width: 80
					},{
						dataIndex: 'milik',
						header: 'milik',
						hidden: true,
						width: 80
					}
				]),
				viewConfig:{forceFit: true,},
				tbar:[
					{
						text: 'Tambah Obat',
						id: 'BtnTambahObatTrKasirIGD_racikan',
						iconCls: 'add',
						handler: function(){
							var records = new Array();
							records.push(new dsRacikan_IGD.recordType({kd_milik:1}));
							dsRacikan_IGD.add(records);
						}
					},{
						text: 'Hapus',
						id: 'BtnHapusObatTrKasirIGD_racikan',
						iconCls: 'RemoveRow',
						handler: function(){
							Ext.Msg.show({
							   title:nmHapusBaris,
							   msg: 'Anda yakin akan menghapus data Obat ini?',
							   buttons: Ext.MessageBox.YESNO,
							   fn: function (btn){
									if (btn =='yes'){
										// console.log('data');
										// console.log(data);
										var data=rowSelected_viApotekResepRWI.data;
										var line=gridRacikanIgd.getSelectionModel().selection.cell[0];
										var order_mng = dsRacikan_IGD.getRange()[line].data.order_mng;
										var kd_obat = dsRacikan_IGD.getRange()[line].data.kd_prd;
										var no_racik = dsRacikan_IGD.getRange()[line].data.no_racik;
										var urut= dsRacikan_IGD.getRange()[line].data.urut;
										if(dsRacikan_IGD.getRange()[line].data.kd_prd != undefined && dsRacikan_IGD.getRange()[line].data.urut != undefined && dsRacikan_IGD.getRange()[line].data.urut != null && dsRacikan_IGD.getRange()[line].data.urut != ''){
											Ext.Ajax.request({
												url : baseURL + "index.php/main/functionIGD/cekdataobat",
												params: {
													kd_pasien : data.KD_PASIEN,
													kd_unit	: data.KD_UNIT,
													tgl_trx	: data.TANGGAL_TRANSAKSI,
													kd_obat : kd_obat,
													no_racik : 0,
													urut: urut
												},
												success: function(o){
													var cst = Ext.decode(o.responseText);
													if (cst.success === true){
														if (order_mng==='Dilayani'){
															ShowPesanWarningDiagnosa_igd('Obat tidak dapat dihapus karena obat sudah dilayani','Obat');
														}else{
															dsRacikan_IGD.removeAt(line);
															gridRacikanIgd.getView().refresh();
															hapusdataobat_IGD(data.KD_PASIEN, data.KD_UNIT,data.TANGGAL_TRANSAKSI,kd_obat, urut, data.URUT_MASUK,0,false);
														}
													}else if (cst.success === false){
														dsRacikan_IGD.removeAt(line);
														gridRacikanIgd.getView().refresh();
													}
												}
											});
										}else{
											dsRacikan_IGD.removeAt(line);
											gridRacikanIgd.getView().refresh();
										}
									}
							   },
							   icon: Ext.MessageBox.QUESTION
							});
						}
					}
				]
			})
		],
		fbar:[
			{
				text:'Ok',
				handler:function(){
					var tmpDats=dsRacikan_IGD.data.items;
					var arrDat=[];
					var jumlah=0;
					var disc=0;
					for(var i=0,iLen=tmpDats.length; i<iLen;i++){
						if(tmpDats[i].data.kd_prd != '' && tmpDats[i].data.kd_prd != null){
							arrDat.push(tmpDats[i].data);
							disc+=tmpDats[i].data.disc;
							jumlah+=tmpDats[i].data.jumlah;
						}
					}
					var err=false;
					if(Ext.getCmp('gridcbo_aturan_racik').getValue() == '' || Ext.getCmp('gridcbo_aturan_racik').getValue() == null){
						err=true;
						Ext.getCmp('gridcbo_aturan_racik').focus();
					}
					if(err==false && (Ext.getCmp('gridcbo_aturan_pakai').getValue() == '' || Ext.getCmp('gridcbo_aturan_pakai').getValue() == 0)){
						err=true;
						Ext.getCmp('gridcbo_aturan_pakai').focus();
					}
					if(err==false && arrDat.length==0){
						err=true;
						ShowPesanWarningDiagnosa_igd('Obat tidak boleh kosong','Obat');
					}
					if(err==false){
						dsDataGrdJab_viApotekResepRWI.data.items[line].set('result',JSON.stringify(arrDat));
						dsDataGrdJab_viApotekResepRWI.data.items[line].set('aturan_racik',Ext.getCmp('gridcbo_aturan_racik').getValue());
						dsDataGrdJab_viApotekResepRWI.data.items[line].set('aturan_pakai',Ext.getCmp('gridcbo_aturan_pakai').getValue());
						dsDataGrdJab_viApotekResepRWI.data.items[line].set('jumlah',jumlah);
						dsDataGrdJab_viApotekResepRWI.data.items[line].set('disc',disc);
						showSignatureIGD.close();		
						// DISINI
						if (edited===false) {
							var tmp_total_racik =parseInt(Ext.getCmp('txtJumlahGrupRacik_viApotekResepRWI').getValue()) + 1;
							Ext.getCmp('txtJumlahGrupRacik_viApotekResepRWI').setValue(tmp_total_racik);

							Ext.Ajax.request({
								url: baseURL + "index.php/apotek/functionAPOTEK/hitung_nilai_adm_racik",
								params: {jml_grup_racik:tmp_total_racik,jml_item_racik:tmp_total_racik},
								failure: function(o)
								{
									ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
								},	
								success: function(o) 
								{
									var cst = Ext.decode(o.responseText);
									if (cst.success === true) 
									{
										Ext.getCmp('txtAdmRacikResepRWIL').setValue(cst.nilai_adm_racik);
										hasilJumlahResepRWI();
									}
									else 
									{
										ShowPesanWarningResepRWJ('Gagal mendapatkan konfigurasi perhitungan racik!','Warning');
										
									};
								}
							});
						}else{
							hasilJumlahResepRWI();
						}

						ResepRWI.form.Grid.a.startEditing(line, 7);	
					}
				}
			}
		]
	}).show();
	hasilJumlahRacik();
	Ext.getCmp('gridcbo_aturan_racik').focus(true,10);
}

function HasilJumlahRacik(){
	var total           = 0;
	var admRacik        = 0;
	var totdisc         = 0;
	var admprs          = 0;
	var adm             = 0;
	var totqty          = 0;
	totalall            = 0;
	totalall_pembayaran = 0;
	// console.log(dsDataGrdJab_viApotekResepRWJ);
	for(var i=0; i<dsDataGrdJab_viApotekResepRWI.getCount() ; i++){
		var jumlahGrid=0;
		var subJumlah=0;
		var disc=0;
	
		var o=dsDataGrdJab_viApotekResepRWI.getRange()[i].data;
		if(o.jml != undefined || o.racikan==true){
				if(o.racikan==true){
					jumlahGrid=parseFloat(o.jumlah);
				}else{
					jumlahGrid=((parseFloat(o.jml) * parseFloat(o.harga_jual)) - parseFloat(o.disc));
				}

				if (o.racikan == "V" || o.racikan == "1" || o.racikan == 1) {
					jumlahGrid=((parseFloat(o.jml) * parseFloat(o.harga_jual)) - parseFloat(o.disc)) + parseFloat(o.biaya_racik);
				}else{
					jumlahGrid=((parseFloat(o.jml) * parseFloat(o.harga_jual)) - parseFloat(o.disc));
				}
				
				if(o.racik != undefined || o.racik != ""){
					if(isNaN(admRacik)){
						admRacik +=0;
					} else {
						if(o.racik == 'Ya' ){
							
							if(o.adm_racik == undefined || o.adm_racik == null || o.adm_racik == 'null' || o.adm_racik == 0){
								admRacik += 0;
							} else{
								admRacik += parseFloat(o.adm_racik);
								
							}
							
						} else{
							admRacik += 0;
						}
						
					}
				} 
				
				totdisc += parseFloat(o.disc);
				o.jumlah =jumlahGrid;
				total +=jumlahGrid;
			totqty +=parseFloat(o.jml);
		}
		

	}
	admRacik = 0;
	Ext.get('txtTmpJmlItem').dom.value=totqty;
	total=aptpembulatan(total);
	Ext.getCmp('txtDRGridJmlEditData_viApotekResepRWI').setValue(total);
	Ext.getCmp('txtDiscEditData_viApotekResepRWJ').setValue(toFormat(totdisc));
	admprs = 0;
	Ext.getCmp('txtAdmPerusahaanEditData_viApotekResepRWJ').setValue(toFormat(admprs));
	totalall +=toInteger(total) + parseInt(Ext.getCmp('txtAdmRacikEditData_viApotekResepRWJ').getValue())  + parseInt(Ext.getCmp('txtTuslahEditData_viApotekResepRWJ').getValue()) + parseFloat(Ext.getCmp('txtAdmEditData_viApotekResepRWJ').getValue()) + admprs - totdisc;
	totalall_pembayaran = totalall;
	totalall = aptpembulatan(totalall);
	Ext.getCmp('txtTotalEditData_viApotekResepRWJ').setValue(toFormat(totalall));
}
var a={};
function gridDataViewEdit_viApotekResepRWI(){
    // chkSelected_viApotekResepRWI = new Ext.grid.CheckColumn({
		// id: Nci.getId(),
		// header: 'C',
		// align: 'center',						
		// dataIndex: 'cito',			
		// width: 20
	// });
	
    // var FieldGrdKasir_viApotekResepRWI = [];
    // dsDataGrdJab_viApotekResepRWI= new WebApp.DataStore({
        // fields: FieldGrdKasir_viApotekResepRWI
    // });
	
	var fldDetail_ds_aturan_racik = ['kd_racik_atr','racik_aturan'];
	ds_cbo_aturan_racik = new WebApp.DataStore({ fields: fldDetail_ds_aturan_racik });
	Ext.Ajax.request({
		url: baseURL + "index.php/main/functionRWJ/getAturanRacik",
		params:{
			kriteria: ''
		} ,
		failure: function(o){
			 var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);
			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = ds_cbo_aturan_racik.recordType;
				var o=cst['listData'][i];
				recs.push(new recType(o));
				ds_cbo_aturan_racik.add(recs);
			}
		}
	});
	var fldDetail_ds_aturan_pakai = ['kd_racik_atr_pk','singkatan','kepanjangan','arti','keterangan'];
	ds_cbo_aturan_pakai = new WebApp.DataStore({ fields: fldDetail_ds_aturan_pakai });
	Ext.Ajax.request({
		url: baseURL + "index.php/main/functionRWJ/getAturanPakai",
		params:{
			kriteria: ''
		} ,
		failure: function(o){
			 var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);
			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = ds_cbo_aturan_pakai.recordType;
				var o=cst['listData'][i];
				recs.push(new recType(o));
				ds_cbo_aturan_pakai.add(recs);
			}
		}
	});
	
	var fldDetail_ds_satuan = ['kd_satuan','satuan'];
	ds_cbo_satuan = new WebApp.DataStore({ fields: fldDetail_ds_satuan });
	Ext.Ajax.request({
		url: baseURL + "index.php/main/functionRWJ/getSatuanRacik",
		params:{
			kriteria: ''
		} ,
		failure: function(o){
			 var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);
			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = ds_cbo_satuan.recordType;
				var o=cst['listData'][i];
				recs.push(new recType(o));
				ds_cbo_satuan.add(recs);
			}
		}
	});
	
	var fldDetail_ds_cara_pakai = ['kd_aturan','nama_aturan'];
	ds_cbo_cara_pakai = new WebApp.DataStore({ fields: fldDetail_ds_cara_pakai });
	Ext.Ajax.request({
		url: baseURL + "index.php/main/functionRWJ/getCaraPakai",
		params:{
			kriteria: ''
		} ,
		failure: function(o){
			 var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);
			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = ds_cbo_cara_pakai.recordType;
				var o=cst['listData'][i];
				recs.push(new recType(o));
				ds_cbo_cara_pakai.add(recs);
			}
		}
	});
	
	var fldDetail_ds_takaran = ['kd_takaran','nama_takaran'];
	ds_cbo_takaran = new WebApp.DataStore({ fields: fldDetail_ds_takaran });
	Ext.Ajax.request({
		url: baseURL + "index.php/main/functionRWJ/getTakaran",
		params:{
			kriteria: ''
		} ,
		failure: function(o){
			 var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);
			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = ds_cbo_takaran.recordType;
				var o=cst['listData'][i];
				recs.push(new recType(o));
				ds_cbo_takaran.add(recs);
			}
		}
	});
    
    ResepRWI.form.Grid.a =new Ext.grid.EditorGridPanel({
        store: dsDataGrdJab_viApotekResepRWI,
        columnLines: true,
		border:false,
		selModel: new Ext.grid.CellSelectionModel ({
            singleSelect: true,
            listeners:{
                cellselect: function(sm, row, rec){
					cell_select_index_rwj=rec;
					CellSelected_viApotekResepRWI = dsDataGrdJab_viApotekResepRWI.getAt(row);
					
					currentKdPrdRacik=CellSelected_viApotekResepRWI.data.kd_prd;
					currentNamaObatRacik=CellSelected_viApotekResepRWI.data.nama_obat;
					currentHargaRacik=CellSelected_viApotekResepRWI.data.harga_jual;
					currentJumlah=CellSelected_viApotekResepRWI.data.jml;
					currentHargaJualObatResepRWI=CellSelected_viApotekResepRWI.data.harga_jual;
					currentCitoNamaObatResepRWI=CellSelected_viApotekResepRWI.data.nama_obat;
					currentCitoKdPrdResepRWI=CellSelected_viApotekResepRWI.data.kd_prd;
					curentIndexsSelection= ResepRWI.form.Grid.a.getSelectionModel().selection.cell[0];
				}
            }
        }),
        stripeRows: true,
		listeners:{
			beforeedit:function(plugin, edit){
				if(plugin.field=='nama_obat' && plugin.record.data.racikan==true){
                    return false;
                }
				if(plugin.field=='satuan' && plugin.record.data.racikan!==true){
                    return false;
                }
			},
			rowdblclick: function (sm, ridx, cidx){
				if(cell_select_index_rwj==14){
					var line = ResepRWI.form.Grid.a.getSelectionModel().selection.cell[0];
					showSignatureRWJ(line);
				}
				if(cell_select_index_rwj==6){
					var line = ResepRWI.form.Grid.a.getSelectionModel().selection.cell[0];
					if(dsDataGrdJab_viApotekResepRWI.data.items[line].data.racikan==true){
						showRacikRWJ(line, true);
					}
				}
			}
		},
        cm:new Ext.grid.ColumnModel([
        	new Ext.grid.RowNumberer(),	
			{
				// column 1
				header		: 'Racik',
				menuDisabled: true,
				hidden 		: true,
				align:'center',
				dataIndex	: 'racikan_text',
				width:60,
				editor:new Ext.form.ComboBox({
					typeAhead		: true,
					triggerAction	: 'all',
					lazyRender		: true,
					mode			: 'local',
					emptyText		: '',
					store			: new Ext.data.ArrayStore({
						id		: 0,
						fields	: ['id','displayText'],
						data	: [[true, 'Ya'],[false, 'Tidak']]
					}),
					valueField		: 'displayText',
					displayField	: 'displayText',
					listeners	: {
						select	: function(a,b,c){
							var line = ResepRWI.form.Grid.a.getSelectionModel().selection.cell[0];
							dsDataGrdJab_viApotekResepRWI.getRange()[line].set('racikan',b.data.id); 
							if(b.data.id==true){
								dsDataGrdJab_viApotekResepRWI.getRange()[line].set('nama_obat','[RACIKAN]'); 
								dsDataGrdJab_viApotekResepRWI.getRange()[line].set('kd_prd',''); 
								dsDataGrdJab_viApotekResepRWI.getRange()[line].set('satuan',''); 
								dsDataGrdJab_viApotekResepRWI.getRange()[line].set('harga_jual',''); 
								dsDataGrdJab_viApotekResepRWI.getRange()[line].set('disc',0); 
								dsDataGrdJab_viApotekResepRWI.getRange()[line].set('jumlah',0); 
								dsDataGrdJab_viApotekResepRWI.getRange()[line].set('jml_stok_apt',0); 
							}else{
								dsDataGrdJab_viApotekResepRWI.getRange()[line].set('nama_obat',''); 
								dsDataGrdJab_viApotekResepRWI.getRange()[line].set('kd_prd',''); 
								dsDataGrdJab_viApotekResepRWI.getRange()[line].set('satuan',''); 
								dsDataGrdJab_viApotekResepRWI.getRange()[line].set('harga_jual',''); 
								dsDataGrdJab_viApotekResepRWI.getRange()[line].set('disc',0); 
								dsDataGrdJab_viApotekResepRWI.getRange()[line].set('jumlah',0);
								dsDataGrdJab_viApotekResepRWI.getRange()[line].set('jml_stok_apt',0); 
							}
							var no_racik=0;
							for(var i=0,iLen=dsDataGrdJab_viApotekResepRWI.getRange().length; i<iLen;i++){
								dsDataGrdJab_viApotekResepRWI.getRange()[i].set('no_racik','0'); 
								if(dsDataGrdJab_viApotekResepRWI.getRange()[i].data.racikan==true){
									no_racik++;
									dsDataGrdJab_viApotekResepRWI.getRange()[i].set('kd_prd',no_racik);
									dsDataGrdJab_viApotekResepRWI.getRange()[i].set('no_racik',no_racik); 
								}
							}
							if(b.data.id==true){
								showRacikRWJ(line, false);
							}else{
								ResepRWI.form.Grid.a.startEditing(line, 6);
							}
						}
					}
				})
			},{
				// column 2
				hidden: true,
				dataIndex	: 'racikan',
			},{
				// column 3
				dataIndex: 'kd_milik',
				header: 'M',
				hidden:true,
				// hidden: true,
				width: 30
			},
			//-------------- ## --------------
			{
				// column 4
				dataIndex: 'cito',
				header: 'C',
				width: 50,
				hidden:true,
				align:'center',
				editor: new Ext.form.ComboBox ( {
					id				: 'gridcboCito_ResepRWI',
					typeAhead		: true,
					triggerAction	: 'all',
					lazyRender		: true,
					mode			: 'local',
					selectOnFocus	: true,
					forceSelection	: true,
					width			: 50,
					anchor			: '95%',
					value			: 'Tidak',
					store			: new Ext.data.ArrayStore({
						id		: 0,
						fields	:['Id','displayText'],
						data	: [[0, 'Tidak'],[1, 'Ya']]
					}),
					valueField	: 'displayText',
					displayField: 'displayText',
					listeners	: {
						select	: function(a,b,c){
							var line	= ResepRWI.form.Grid.a.getSelectionModel().selection.cell[0];
							var o = dsDataGrdJab_viApotekResepRWI.getRange()[line].data;
							if(b.data.Id == 1) {
								if(o.harga_jual=='' ||o.harga_jual==undefined){
									ShowPesanWarningResepRWI("Obat yang akan di Cito masih kosong!","Warning");
								} else{
									currentHargaJualObatResepRWI=o.harga_jual;
									formulacitoResepRWI();
								}
							} else{
								var line	= ResepRWI.form.Grid.a.getSelectionModel().selection.cell[0];
								var o = dsDataGrdJab_viApotekResepRWI.getRange()[line].data;
								o.harga_jual=o.hargaaslicito;
								o.nilai_cito=0;
								o.cito="Tidak";
								hasilJumlahResepRWI();
								ResepRWI.form.Grid.a.startEditing(line,4);	
							}
						}
					}
				})
			},{
				// column 5
				dataIndex: 'kd_prd',
				header: 'Kode/No. Racik',
				sortable: true,
				width: 70
			},{			
				// column 6
				dataIndex: 'nama_obat',
				header: 'Uraian',
				sortable: true,
				width: 170,
				editor: new Ext.form.TextField({
					allowBlank: false,
					enableKeyEvents:true,
					listeners:{
						keyDown: function(a,b,c){
							if(b.getKey()==13){
								var line	= ResepRWI.form.Grid.a.getSelectionModel().selection.cell[0];
								console.log(a.getValue());
								if(a.getValue().length < 1){
								//if(a.getValue().length < 1){
									if(a.getValue().length != 0){
										Ext.Msg.show({
											title: 'Perhatian',
											//msg: 'Kriteria huruf pencarian obat minimal 3 huruf!',
											msg: 'Kriteria Obat Tidak Boleh Kosong!',
											buttons: Ext.MessageBox.OK,
											fn: function (btn) {
												if (btn == 'ok'){
													ResepRWI.form.Grid.a.startEditing(line, 4);
												}
											}
										});
									}
									
								} else{			
									PencarianLookupResepRWI = true;// Variabel pembeda getdata u/nama obat dan semua kepemilikan atau bukan. False => pencarian berdasarkan nama_obat dan kepemilikan, True => pencarian berdasarkan nama_obat saja
									FocusExitResepRWI = false;
									LookUpSearchListGetObat_resepRWI(a.getValue());
								}
							}
						}
					}
				})
			},
			/* {			
				dataIndex: 'nama_obat',
				header: 'Uraian',
				sortable: true,
				width: 250,
				editor:new Nci.form.Combobox.autoComplete({
					store	: ResepRWI.form.ArrayStore.a,
					select	: function(a,b,c){
						//'harga_beli','kd_pabrik','markup','tuslah','adm_racik'
						var line	= ResepRWI.form.Grid.a.getSelectionModel().selection.cell[0];
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.cito="Tidak";
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.nama_obat=b.data.nama_obat;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.kd_prd=b.data.kd_prd;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.kd_satuan=b.data.kd_satuan;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.fractions=b.data.fractions;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.harga_jual=b.data.harga_jual;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.harga_beli=b.data.harga_beli;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.kd_pabrik=b.data.kd_pabrik;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.markup=b.data.markup;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.adm_racik=b.data.adm_racik;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.jasa=b.data.jasa;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.no_out=b.data.no_out;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.no_urut=b.data.no_urut;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.tgl_out=b.data.tgl_out;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.kd_milik=b.data.kd_milik;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.jml_stok_apt=b.data.jml_stok_apt;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.milik=b.data.milik;
						//dsDataGrdJab_viApotekResepRWI.getRange()[line].data.min_stok=b.data.min_stok;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.hargaaslicito=b.data.hargaaslicito;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.nilai_cito=0;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.disc=0;
						dsDataGrdJab_viApotekResepRWI.getRange()[line].data.racik="Tidak";
						
						ResepRWI.form.Grid.a.getView().refresh();
						ResepRWI.form.Grid.a.startEditing(line, 6);	
						
						if(b.data.jml_stok_apt <= 10){
							// ShowPesanInfoResepRWI('Stok obat hampir habis, jumlah stok tersedia adalah '+b.data.jml_stok_apt,'Information')
							Ext.Msg.show({
								title: 'Information',
								msg: 'Stok obat hampir habis, jumlah stok tersedia adalah '+b.data.jml_stok_apt,
								buttons: Ext.MessageBox.OK,
								fn: function (btn) {
									if (btn == 'ok')
									{
										ResepRWI.form.Grid.a.startEditing(line, 6);	
									}
								}
							});
						}
						Ext.getCmp('txtAdmRacikEditData_viApotekResepRWI').setValue(b.data.adm_racik);
					},
					insert	: function(o){
						return {
							kd_prd        	: o.kd_prd,
							nama_obat 		: o.nama_obat,
							kd_sat_besar	: o.kd_sat_besar,
							kd_satuan		: o.kd_satuan,
							fractions		: o.fractions,
							harga_jual		: o.harga_jual,
							harga_beli		: o.harga_beli,
							kd_pabrik		: o.kd_pabrik,
							tuslah			: o.tuslah,
							adm_racik		: o.adm_racik,
							markup			: o.markup,
							jasa			: o.jasa,
							no_out			: o.no_out,
							no_urut			: o.no_urut,
							tgl_out			: o.tgl_out,
							kd_milik		: o.kd_milik,
							jml_stok_apt	: o.jml_stok_apt,
							hargaaslicito	: o.hargaaslicito,
							milik			: o.milik,
							//text			:  '<table style="font-size: 11px;"><tr><td width="50">'+o.kd_prd+'</td><td width="200">'+o.nama_obat+'</td><td width="150" align="right">'+parseInt(o.harga_jual).toLocaleString('id', { style: 'currency', currency: 'IDR' })+'</td><td width="50">&nbsp;&nbsp;'+o.min_stok+'</td></tr></table>'
							text			:  '<table style="font-size: 11px;"><tr><td width="60">'+o.kd_prd+'</td><td width="250">'+o.nama_obat+'</td><td width="60" align="left">'+o.kd_satuan+'</td><td width="40" align="right">'+o.jml_stok_apt+'</td><td width="80" align="right">'+parseInt(o.harga_jual).toLocaleString('id', { style: 'currency', currency: 'IDR' })+'</td><td width="10" align="right"> </td><td width="50"  align="left">'+o.milik+'</td></tr></table>'
						}
					},
					param:function(){
							return {
								kdcustomer:Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').getValue()
							}
					},
					url		: baseURL + "index.php/apotek/functionAPOTEKrwi/getObat",
					valueField: 'nama_obat',
					displayField: 'text',
					listWidth: 450
				})
			}, */
			{
				// column 7
				dataIndex: 'kd_satuan',
				header: 'Satuan',
				width: 60,
				editor		: new Ext.form.ComboBox ( {
					id				: 'gridcbo_satuan',
					typeAhead		: true,
					triggerAction	: 'all',
					lazyRender		: true,
					mode			: 'local',
					selectOnFocus	: true,
					forceSelection	: true,
					store			: ds_cbo_satuan,
					valueField		: 'satuan',
					displayField	: 'satuan',
					listeners	: {
						select	: function(a,b,c){
							var line = ResepRWI.form.Grid.a.getSelectionModel().selection.cell[0];
							dsDataGrdJab_viApotekResepRWI.getRange()[line].set('kd_satuan',b.data.kd_satuan); 
							ResepRWI.form.Grid.a.startEditing(line, 11);
						}
					}
				})
			},
			// {
				// dataIndex: 'racik',
				// header: 'Racikan',
				// width: 70,
				// align:'center',
				// editor: new Ext.form.ComboBox ( {
					// id				: 'gridcboRacik_ResepRWI',
					// typeAhead		: true,
					// triggerAction	: 'all',
					// lazyRender		: true,
					// mode			: 'local',
					// selectOnFocus	: true,
					// forceSelection	: true,
					// width			: 50,
					// anchor			: '95%',
					// value			: 1,
					// store			: new Ext.data.ArrayStore({
						// id		: 0,
						// fields	:['Id','displayText'],
						// data	: [[0, 'Tidak'],[1, 'Ya']]
					// }),
					// valueField	: 'displayText',
					// displayField: 'displayText',
					// value		: '',
					// listeners	: {
						// select	: function(a,b,c){
							// if(b.data.Id == 1) {
								// Ext.getCmp('btnRacikan_viApotekResepRWI').enable();
								// formulaRacikanResepRWI();
								// statusRacikan_ResepRWI = b.data.Id;
							// } else{
								// statusRacikan_ResepRWI = b.data.Id;
								// Ext.getCmp('btnRacikan_viApotekResepRWI').disable();
								// var line	= ResepRWI.form.Grid.a.getSelectionModel().selection.cell[0];
								// ResepRWI.form.Grid.a.startEditing(line, 10);	
							// }
						// },
						// specialkey: function(){
							// if(Ext.EventObject.getKey() == 13){
								// if(statusRacikan_ResepRWI == 0){
									// statusRacikan_ResepRWI=0;
									// var line	= ResepRWI.form.Grid.a.getSelectionModel().selection.cell[0];
									// ResepRWI.form.Grid.a.startEditing(line, 10);	
								// } 
							// }
						// }
					// }
				// })
				// /* editor: new Ext.form.NumberField({
					// allowBlank: false,
					// listeners:{
						// blur: function(a){
							// var line	= this.index;
							// dsDataGrdJab_viApotekResepRWI.getRange()[line].data.racik=a.getValue();
							// hasilJumlahResepRWI();
						// },
						// focus: function(a){
							// this.index=ResepRWI.form.Grid.a.getSelectionModel().selection.cell[0]
						// }
					// }
				// })	 */
			// },
			{
				// column 8
				dataIndex: 'no_racik',
				header: 'No. Rck',
				width: 70,
				hidden:true,
				editor: new Ext.form.TextField({
					allowBlank: false,
					listeners:{
						specialkey: function(){
							if(Ext.EventObject.getKey() == 13){
								var records = new Array();
								records.push(new dsDataGrdJab_viApotekResepRWI.recordType());
								dsDataGrdJab_viApotekResepRWI.add(records);
								var nextRow = dsDataGrdJab_viApotekResepRWI.getCount()-1; 
								ResepRWI.form.Grid.a.startEditing(nextRow, 4);
								hitung_adm_racik();
							}
						}
					}
				})
			},
			{
				// column 9
				dataIndex: 'harga_jual',
				header: 'Harga Sat',
				sortable: true,
				xtype:'numbercolumn',
				align:'right',
				format : '0,000',
				width: 85
			},{
				// column 10
				dataIndex: 'jml_order',
				header: 'Order',
				// xtype:'numbercolumn',
				sortable: true,
				align:'right',
				// format : '0,000',
				width: 50
			},{
				// column 11
				dataIndex: 'jml',
				header: 'diBerikan',
				sortable: true,
				// xtype:'numbercolumn',
				width: 45,
				align:'right',
				// format : '0,000',
				editor: new Ext.form.NumberField({
					allowBlank: false,
					enableKeyEvents:true,
					listeners:{
						keyDown: function(a,b,c){
							if(b.getKey()==13){
								// var line = ResepRWI.form.Grid.a.getSelectionModel().selection.cell[0];
								var line	= this.index;
								if(dsDataGrdJab_viApotekResepRWI.data.items[line].data.racikan==false){
									var o = dsDataGrdJab_viApotekResepRWI.getRange()[line].data;
									if(a.getValue()=='' || a.getValue()==undefined || a.getValue()==0){
										// ShowPesanWarningResepRWI('Qty obat belum di isi', 'Warning');
										Ext.Msg.show({
											title: 'Perhatian',
											msg: 'Qty obat belum di isi!',
											buttons: Ext.MessageBox.OK,
											fn: function (btn) {
												if (btn == 'ok')
												{
													var thisrow = dsDataGrdJab_viApotekResepRWI.getCount()-1; 
													ResepRWI.form.Grid.a.startEditing(thisrow, 9);
												}
											}
										});
									}else{
										statusRacikan_ResepRWI=0;
										var tmp_jml_stok;
										var tmp_min_stok;
										var o=dsDataGrdJab_viApotekResepRWI.getRange()[line].data;
										//console.log(o.kd_milik);
										Ext.Ajax.request({
											url: baseURL + "index.php/apotek/functionAPOTEKrwi/getStokObat",
											params: {kd_prd: o.kd_prd, kd_milik :o.kd_milik},
											failure: function(o){
												 var cst = Ext.decode(o.responseText);
											},	    
											success: function(o) {
												var cst = Ext.decode(o.responseText);
												if (cst.success===true){
													tmp_jml_stok = cst.jml_stok;
													tmp_min_stok= cst.min_stok;
													if ( parseInt(cst.min_stok) == parseInt(cst.jml_stok)){
														// Ext.Msg.alert('Perhatian','Sisa stok sudah mencapai atau melebihi minimum stok!');
														Ext.Msg.show({
															title: 'Perhatian',
															msg: 'Sisa stok sudah mencapai atau melebihi minimum stok!',
															buttons: Ext.MessageBox.OK,
															fn: function (btn) {
																if (btn == 'ok')
																{
																	o.jml=o.jml;
																	hasilJumlahResepRWI();
																	var thisrow = dsDataGrdJab_viApotekResepRWI.getCount()-1; 
																	ResepRWI.form.Grid.a.startEditing(thisrow, 9);
																}
															}
														});
													} else if(parseFloat(a.getValue()) > parseFloat(cst.jml_stok)){
														// Ext.Msg.alert('Perhatian','Qty melebihi sisa stok tersedia!');
														Ext.Msg.show({
															title: 'Perhatian',
															msg: 'Qty melebihi sisa stok tersedia, Stok hanya tersedia '+cst.jml_stok+'!',
															buttons: Ext.MessageBox.OK,
															fn: function (btn) {
																if (btn == 'ok')
																{
																	o.jml=cst.jml_stok;
																	hasilJumlahResepRWI();
																	var thisrow = dsDataGrdJab_viApotekResepRWI.getCount()-1; 
																	ResepRWI.form.Grid.a.startEditing(thisrow, 9);
																}
															}
														});
													} else{
														Ext.getCmp('btnAddObatRWI').el.dom.click();
														o.jml=o.jml;
														hasilJumlahResepRWI();
														// showSignatureRWJ(line);
														// statusRacikan_ResepRWI=0;
														// var records = new Array();
														// records.push(new dsDataGrdJab_viApotekResepRWI.recordType());
														// dsDataGrdJab_viApotekResepRWI.add(records);
														// var nextRow = dsDataGrdJab_viApotekResepRWI.getCount()-1; 
														// ResepRWI.form.Grid.a.startEditing(nextRow, 4);
													}
													Ext.getCmp('btnSimpan_viApotekResepRWI').enable();
												}
											}
										})
									} 
								}else{
									// showSignatureRWJ(line);
								}	
							}
						},
						focus: function(a){
							this.index=ResepRWI.form.Grid.a.getSelectionModel().selection.cell[0]
						},
					}
				})	
			},
			{
				// column 12
				dataIndex: 'disc',
				header: 'Diskon',
				sortable: true,
				xtype:'numbercolumn',
				width: 65,
				align:'right',
				editor: new Ext.form.NumberField({
					allowBlank: false,
					listeners:{
						blur: function(a){
							var line	= this.index;
							dsDataGrdJab_viApotekResepRWI.getRange()[line].data.disc=a.getValue();
							hasilJumlahResepRWI();
						},
						focus: function(a){
							this.index=ResepRWI.form.Grid.a.getSelectionModel().selection.cell[0]
						}
						
					}
				})
			},{
				// column 13
				dataIndex: 'jumlah',
				header: 'Sub Total',
				sortable: true,
				width: 100,
				xtype:'numbercolumn',
				align:'right',
				format : '0,000',
				/*renderer: function(v, params, record) {
				}	*/
			},{
				// column 14
				dataIndex: 'signa',
				header: 'Signa',
				hidden : true,
				width: 100,
			},{
				// column 15
				header		: 'Takaran',
				hidden		: false,
				menuDisabled: true,
				hidden : true,
				width: 80,
				dataIndex	: 'takaran'	,
				editor		: new Ext.form.ComboBox ( {
					typeAhead		: true,
					triggerAction	: 'all',
					lazyRender		: true,
					mode			: 'local',
					selectOnFocus	: true,
					forceSelection	: true,
					store			: ds_cbo_takaran,
					valueField		: 'nama_takaran',
					displayField	: 'nama_takaran',
					listeners	: {
						select	: function(a,b,c){
							var line = ResepRWI.form.Grid.a.getSelectionModel().selection.cell[0];
							ResepRWI.form.Grid.a.startEditing(line, 16);
							
							// var line = PenataJasaIGD.gridObat.getSelectionModel().selection.cell[0];
							// dsPjTrans2_IGD.getRange()[line].set('kd_takaran',b.data.kd_takaran); 
						}
					}
				})
			},{
				dataIndex: 'cara_pakai',
				header: 'Aturan Pakai',
				hidden : true,
				width: 100,
				editor		: new Ext.form.ComboBox ( {
					typeAhead		: true,
					triggerAction	: 'all',
					lazyRender		: true,
					mode			: 'local',
					selectOnFocus	: true,
					forceSelection	: true,
					store			: ds_cbo_cara_pakai,
					valueField		: 'nama_aturan',
					displayField	: 'nama_aturan',
					listeners	: {
						select	: function(a,b,c){
							// var line = AptResepRWJ.form.Grid.a.getSelectionModel().selection.cell[0];
							// // dsPjTrans2_IGD.getRange()[line].set('kd_aturan_pakai',b.data.kd_aturan); 
							// AptResepRWJ.form.Grid.a.startEditing(line, 13);
							var records = new Array();
							records.push(new dsDataGrdJab_viApotekResepRWI.recordType());
							dsDataGrdJab_viApotekResepRWI.add(records);
							var nextRow = dsDataGrdJab_viApotekResepRWI.getCount()-1; 
							ResepRWI.form.Grid.a.startEditing(nextRow, 1);
							dsDataGrdJab_viApotekResepRWI.getRange()[nextRow].set('racikan',false);
							dsDataGrdJab_viApotekResepRWI.getRange()[nextRow].set('racikan_text','Tidak');
						}
					}
				})
			},{
				dataIndex: 'aturan_racik',
				header: 'Aturan Racik Obat',
				width: 120,
				hidden:true,
			},
			//-------------- ## --------------
			{
				dataIndex: 'aturan_pakai',
				header: 'Aturan Pakai Obat',
				width: 120,
				hidden:true,
			}, 
			//-------------- HIDDEN --------------
			{
				dataIndex: 'harga_beli',
				header: 'Harga Beli',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'kd_pabrik',
				header: 'Kode Pabrik',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'markup',
				header: 'Markup',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'adm_racik',
				header: 'Adm Racik',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'jasa',
				header: 'Jasa Tuslah',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'no_out',
				header: 'No Out',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'no_urut',
				header: 'No Urut',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'tgl_out',
				header: 'tgl out',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'jml_stok_apt',
				header: 'stok tersedia',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'nilai_cito',
				header: 'nilai_cito',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'hargaaslicito',
				header: 'hargaaslicito',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'milik',
				header: 'milik',
				hidden: true,
				width: 80
			},{
				dataIndex: 'min_stok',
				header: 'min stok',
				sortable: true,
				xtype:'numbercolumn',
				hidden: true,
				align:'right',
				width: 85
			},{
				dataIndex: 'id_mrresep',
				header: 'ID MR Resep',
				sortable: true,
				hidden: true,
				width: 85
			},{
				dataIndex		: 'racikan',
				header			: 'Racikan',
				width			: 60,
				menuDisabled	: true,
				align			: 'center',
				renderer: function(v, params, record){
					if (v === true || (v == '1' || v == 1)) {
						return 'V';
					}else if(v === false || (v == '0' || v == 0)){
						return '';
					}else{
						return v;
					}
				},
			}
			//-------------- ## --------------
        ]),
		listeners : {
			'cellclick': function(grd, rowIndex, colIndex, e) {
				console.log(colIndex);
				// ResepRWI.form.Grid.a
				// dsDataGrdJab_viApotekResepRWI
				if (colIndex == 33) {
					var line	= ResepRWI.form.Grid.a.getSelectionModel().selection.cell[0];
					var o = dsDataGrdJab_viApotekResepRWI.getRange()[line].data;
					if(o.racikan == undefined || o.racikan == "" || o.racikan == "0" || o.racikan === false){
						o.racikan  	= "V";
						o.jumlah 	= (parseInt(o.harga_jual) * parseInt(o.jml))+parseInt(o.biaya_racik);
					}else{
						o.jumlah 	= parseInt(o.harga_jual) * parseInt(o.jml);
						o.racikan  	= "";
					}
					ResepRWI.form.Grid.a.getView().refresh();
					hasilJumlahResepRWI();
				}
		    }
		}
    });
    return ResepRWI.form.Grid.a;
}

function hasilJumlahRacikan(){
	var total=0;
	var admRacik=0;
	var totdisc=0;
	var totalall=0;
	var admprs=0;
	var adm=0;
	var totqty=0;
	totalall=0;
	totalall_pembayaran=0;
	
	for(var i=0; i < dsDataGrdJab_viApotekResepRWI.getCount() ; i++){
		var jumlahGrid=0;
		var subJumlah=0;
		var disc=0;
	
		var o=dsDataGrdJab_viApotekResepRWI.getRange()[i].data;
		if(o.jml != undefined){
			// if(parseFloat(o.jml) <= parseFloat(o.jml_stok_apt)){
				if(o.racikan==true){
					jumlahGrid=parseFloat(o.jumlah);
				}else{
					jumlahGrid=((parseFloat(o.jml) * parseFloat(o.harga_jual)) - parseFloat(o.disc));
				}
			
				if (o.racikan == "V" || o.racikan == "1" || o.racikan == 1) {
					jumlahGrid=((parseFloat(o.jml) * parseFloat(o.harga_jual)) - parseFloat(o.disc)) + parseFloat(o.biaya_racik);
				}else{
					jumlahGrid=((parseFloat(o.jml) * parseFloat(o.harga_jual)) - parseFloat(o.disc));
				}
				
				if(o.racik != undefined || o.racik != ""){
					if(isNaN(admRacik)){
						admRacik +=0;
					} else {
						if(o.racik == 'Ya' ){
							if(o.adm_racik == undefined || o.adm_racik == null ||o.adm_racik == 'null' || o.adm_racik == 0){
								admRacik +=0;
							} else{
								admRacik += parseFloat(o.adm_racik);
							}
							
						} else{
							admRacik +=0;
						}
						
					}
				}
				totdisc += parseFloat(o.disc);
				o.jumlah =jumlahGrid;
				total +=jumlahGrid;
			totqty +=parseFloat(o.jml);
		}

	}
	total=aptpembulatan(total);
	console.log(total);
	Ext.getCmp('txtJumlahTotalResepRWIL').setValue(total);
	Ext.get('txtTmpJmlItem').dom.value=totqty;
	admRacik = Ext.getCmp('txtAdmRacikResepRWIL').getValue();
	Ext.getCmp('txtTmpTotQtyResepRWIL').setValue(totqty);
	
	Ext.getCmp('txtAdmRacikResepRWIL').setValue(toFormat(admRacik));
	Ext.getCmp('txtTotalDiscResepRWIL').setValue(toFormat(totdisc));
	// admprs=total*parseFloat(Ext.getCmp('txtAdmResepRWIL').getValue());
	admprs = 0;
	Ext.getCmp('txtAdmPrshResepRWIL').setValue(toFormat(admprs));
	console.log(toInteger(total));
	totalall += toInteger(total) + parseFloat(admRacik) + parseFloat(Ext.get('txtTuslahEmbalaseL').getValue()) + parseFloat(Ext.get('txtAdmResepRWIL').getValue()) + parseFloat(admprs) - parseInt(totdisc);
	totalall_pembayaran = totalall;
	console.log(totalall_pembayaran);
	totalall = aptpembulatan(totalall);
	Ext.getCmp('txtTotalBayarResepRWIL').setValue(toFormat(totalall));

	//Ext.get('txtTotalEditData_viApotekResepRWJ').dom.value=totalall.toLocaleString('id', { style: 'currency', currency: 'IDR' });
	ResepRWI.form.Grid.a.getView().refresh();
}

function hitung_adm_racik(){
	var arr_racik=[];
	var jml_item_racik=0;
	var j=0;
	for(var i=0; i<dsDataGrdJab_viApotekResepRWI.getCount() ; i++){
		var o=dsDataGrdJab_viApotekResepRWI.getRange()[i].data;
		console.log(o);
		if(o.no_racik !== undefined  && o.no_racik !== null && o.no_racik !== ""){
			if(parseInt(o.no_racik) !== 0 ){
				arr_racik[j]=o.no_racik;
				j++;
			}
		}
		
		if(o.racik != undefined){
			if(o.racik == 'Ya'){
				jml_item_racik=jml_item_racik+1;
			}
		}
	} 
	console.log(arr_racik);
	var uniqueItems = Array.from(new Set(arr_racik));// mendapatkan banyaknya grup racik berdasarkan  array unik
	var jml_grup_racik = uniqueItems.length; //menampung banyaknya grup racik
	Ext.getCmp('txtJumlahGrupRacik_viApotekResepRWI').setValue(jml_grup_racik);
	
	/* MENGAMBIL MODEL PERHITUNGAN RACIK DARI KONFIGURASI DI SYS_SETTING */
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEKrwi/hitung_nilai_adm_racik",
			params: {jml_grup_racik:jml_grup_racik,jml_item_racik:jml_item_racik},
			failure: function(o)
			{
				ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					Ext.getCmp('txtAdmRacikResepRWIL').setValue(cst.nilai_adm_racik);
					default_nilai_adm_racik = cst.nilai_adm_racik;
					console.log(cst.nilai_adm_racik);
					hasilJumlahResepRWI();
					if(Ext.getCmp('txtTmpStatusPostResepRWIL').getValue() == 0){
						var nextRow = dsDataGrdJab_viApotekResepRWI.getCount()-1; 
						ResepRWI.form.Grid.a.startEditing(nextRow, 4);
					} 
					
				}
				else 
				{
					ShowPesanWarningResepRWI('Gagal mendapatkan konfigurasi perhitungan racik!','Warning');
					
				};
			}
		}
		
	)
}

function hasilJumlahResepRWI(){
	var total=0;
	var admRacik=0;
	var totdisc=0;
	var totalall=0;
	var admprs=0;
	var adm=0;
	var totqty=0;
	totalall=0;
	totalall_pembayaran=0;
	
	for(var i=0; i < dsDataGrdJab_viApotekResepRWI.getCount() ; i++){
		var jumlahGrid=0;
		var subJumlah=0;
		var disc=0;
	
		var o=dsDataGrdJab_viApotekResepRWI.getRange()[i].data;
		if(o.jml != undefined){
			// if(parseFloat(o.jml) <= parseFloat(o.jml_stok_apt)){
				if(o.racikan==true){
					jumlahGrid=parseFloat(o.jumlah);
				}else{
					jumlahGrid=((parseFloat(o.jml) * parseFloat(o.harga_jual)) - parseFloat(o.disc));
				}
			
				if (o.racikan == "V" || o.racikan == "1" || o.racikan == 1) {
					jumlahGrid=((parseFloat(o.jml) * parseFloat(o.harga_jual)) - parseFloat(o.disc)) + parseFloat(o.biaya_racik);
				}else{
					jumlahGrid=((parseFloat(o.jml) * parseFloat(o.harga_jual)) - parseFloat(o.disc));
				}
				
				if(o.racik != undefined || o.racik != ""){
					if(isNaN(admRacik)){
						admRacik +=0;
					} else {
						if(o.racik == 'Ya' ){
							if(o.adm_racik == undefined || o.adm_racik == null ||o.adm_racik == 'null' || o.adm_racik == 0){
								admRacik +=0;
							} else{
								admRacik += parseFloat(o.adm_racik);
							}
							
						} else{
							admRacik +=0;
						}
						
					}
				}
				totdisc += parseFloat(o.disc);
				o.jumlah =jumlahGrid;
				total +=jumlahGrid;
			// } else{
				// Ext.Msg.show({
					// title: 'Perhatian',
					// msg: 'Jumlah obat melebihi stok yang tersedia',
					// buttons: Ext.MessageBox.OK,
					// fn: function (btn) {
						// if (btn == 'ok')
						// {
							// var nextRow = dsDataGrdJab_viApotekResepRWI.getCount()-1; 
							// ResepRWI.form.Grid.a.startEditing(nextRow, 9);
						// }
					// }
				// });
				// o.jml=o.jml_stok_apt;
			// }
			totqty +=parseFloat(o.jml);
		}

	}
	// admRacik = 0;
	console.log(total);
	total=aptpembulatan(total);
	console.log(total);
	Ext.getCmp('txtJumlahTotalResepRWIL').setValue(total);
	Ext.get('txtTmpJmlItem').dom.value=totqty;
	admRacik = Ext.getCmp('txtAdmRacikResepRWIL').getValue();
	Ext.getCmp('txtTmpTotQtyResepRWIL').setValue(totqty);
	
	Ext.getCmp('txtAdmRacikResepRWIL').setValue(toFormat(admRacik));
	Ext.getCmp('txtTotalDiscResepRWIL').setValue(toFormat(totdisc));
	// admprs=total*parseFloat(Ext.getCmp('txtAdmResepRWIL').getValue());
	admprs = 0;
	Ext.getCmp('txtAdmPrshResepRWIL').setValue(toFormat(admprs));
	console.log(toInteger(total));
	var tmp_tuslah = Ext.get('txtTuslahEmbalaseL').getValue();
	totalall += toInteger(total) + parseFloat(admRacik) + parseFloat(tmp_tuslah.replace(/,/g, "")) + parseFloat(Ext.get('txtAdmResepRWIL').getValue()) + parseFloat(admprs) - parseInt(totdisc);
	totalall_pembayaran = totalall;
	console.log(parseFloat(Ext.get('txtTuslahEmbalaseL').getValue()));
	console.log(totalall_pembayaran);
	totalall = aptpembulatan(totalall);
	Ext.getCmp('txtTotalBayarResepRWIL').setValue(toFormat(totalall));

	//Ext.get('txtTotalEditData_viApotekResepRWJ').dom.value=totalall.toLocaleString('id', { style: 'currency', currency: 'IDR' });
	ResepRWI.form.Grid.a.getView().refresh();
}

function hasilJumlahResepRWILoad(){
	var total=0;
	var admRacik=0;
	var totdisc=0;
	var totalall=0;
	var admprs=0;
	var adm=0;
	var totqty=0;
	for(var i=0; i < dsDataGrdJab_viApotekResepRWI.getCount() ; i++){
		var jumlahGrid=0;
		var subJumlah=0;
		var disc=0;
	
		var o=dsDataGrdJab_viApotekResepRWI.getRange()[i].data;
		if(o.jml != undefined){
			// if(parseFloat(o.jml) <= parseFloat(o.jml_stok_apt)){
				// jumlahGrid=((parseFloat(o.jml) * parseFloat(o.harga_jual)) - parseFloat(o.disc));
			
				// totdisc += parseFloat(o.disc);
				// o.jumlah =jumlahGrid;
				// total +=jumlahGrid;
			// } else{
				// ShowPesanWarningResepRWI('Jumlah obat melebihi stok yang tersedia','Warning');
				// o.jml=o.jml_stok_apt;
			// }
			// totqty +=parseFloat(o.jml);
			
			if(o.racikan==false){
				jumlahGrid=((parseFloat(o.jml) * parseFloat(o.harga_jual)) - parseFloat(o.disc));
			}else{
				jumlahGrid=parseFloat(o.jumlah)
			}
			totdisc += parseFloat(o.disc);
			o.jumlah =jumlahGrid;
			total +=jumlahGrid;
			totqty +=parseFloat(o.jml);
		}

	}
	// admRacik = 0;
	admRacik = parseFloat(Ext.getCmp('txtAdmRacikResepRWIL').getValue());
	Ext.getCmp('txtTmpTotQtyResepRWIL').setValue(totqty);
	console.log(total);
	total=aptpembulatan(total);
	console.log(total);
	Ext.getCmp('txtJumlahTotalResepRWIL').setValue(total);
	Ext.getCmp('txtAdmRacikResepRWIL').setValue(toFormat(admRacik));
	Ext.getCmp('txtTotalDiscResepRWIL').setValue(toFormat(totdisc));
	// admprs=toInteger(total)*parseFloat(Ext.getCmp('txtAdmResepRWIL').getValue());
	admprs = 0;
	Ext.getCmp('txtAdmPrshResepRWIL').setValue(toFormat(admprs));
	totalall += toInteger(total) + parseFloat(admRacik) + parseFloat(Ext.getCmp('txtTuslahEmbalaseL').getValue()) + parseFloat(Ext.getCmp('txtAdmResepRWIL').getValue()) + parseFloat(admprs) - parseInt(totdisc);
	totalall=aptpembulatan(totalall);
	// totalall_pembayaran = aptpembulatan(totalall);
	Ext.getCmp('txtTotalBayarResepRWIL').setValue(toFormat(totalall));
	console.log(totalall);
	//Ext.get('txtTotalEditData_viApotekResepRWJ').dom.value=totalall.toLocaleString('id', { style: 'currency', currency: 'IDR' });
	ResepRWI.form.Grid.a.getView().refresh();
}
function ComboPilihanKelompokPasienApotekResepRWI(){
	// var Field_Customer = ['KD_CUSTOMER', 'JENIS_PASIEN'];
	var Field_Customer = ['KD_CUSTOMER', 'CUSTOMER'];
	ds_kelpas = new WebApp.DataStore({fields: Field_Customer});
    ds_kelpas.load({
		params:{
			Skip: 0,
			Take: 1000,
			Sort: '',
			Sortdir: 'ASC',
			target: 'ComboKelPasApotek',
			param: "status='t' order by customer"
		}
	});
    var cboPilihankelompokPasienAptResepRWI = new Ext.form.ComboBox({
		x:305,
		y:25,
		id:'cboPilihankelompokPasienAptResepRWI',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender:true,
		mode: 'local',
		selectOnFocus:true,
		// readOnly:true,
		emptyText:'Kelompok Pasien',
		width: 285,
		store: ds_kelpas,
		valueField: 'KD_CUSTOMER',
		displayField: 'CUSTOMER',
		//value:selectSetPilihankelompokPasien,
		listeners:{
			'select': function(a,b,c){
				Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').setValue(b.data.KD_CUSTOMER);
			},
			'specialkey': function(){
				if (Ext.EventObject.getKey() === 13){
					Ext.getCmp('cbo_DokterApotekResepRWI').focus(true, 20);
				}
			}
		}
	});
	return cboPilihankelompokPasienAptResepRWI;
}
function ComboDokterApotekResepRWI(){
    var Field_Dokter = ['KD_DOKTER', 'NAMA'];
    ds_dokter = new WebApp.DataStore({fields: Field_Dokter});
    ds_dokter.load({
		params:{
			Skip: 0,
			Take: 1000,
			Sort: 'kd_dokter',
			Sortdir: 'ASC',
			target: 'ComboDokterApotek',
			param: ''
		}
	});
    var cbo_DokterApotekResepRWI = new Ext.form.ComboBox({
		x:700,
		y:50,
		flex: 1,
		id: 'cbo_DokterApotekResepRWI',
		valueField: 'KD_DOKTER',
		displayField: 'NAMA',
		emptyText:'Dokter',
		store: ds_dokter,
		mode: 'local',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		//readOnly:true,
		width:220,
		tabIndex:2,
		listeners:{ 
			'select': function(a,b,c){
				selectSetDokter=b.data.displayText ;
				Ext.getCmp('txtTmpKdDokterApotekResepRWIL').setValue(b.data.KD_DOKTER);
			},
			'specialkey' : function(){
				if (Ext.EventObject.getKey() === 13){
					var records = new Array();
					records.push(new dsDataGrdJab_viApotekResepRWI.recordType());
					dsDataGrdJab_viApotekResepRWI.add(records);
					row=dsDataGrdJab_viApotekResepRWI.getCount()-1; // menujukan jumlah baris,startEditing(baris dimulai dari 1, kolom 1)
					ResepRWI.form.Grid.a.startEditing(row, 4);	
				} 						
			}
		}
	});    
    return cbo_DokterApotekResepRWI;
}
function mComboStatusPostingApotekResepRWI(){
  var cboStatusPostingApotekResepRWI = new Ext.form.ComboBox({
        id:'cboStatusPostingApotekResepRWI',
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:true,
		editable: false,
        mode: 'local',
        width: 110,
        emptyText:'',
        fieldLabel: 'Posting',
		tabIndex:5,
        store: new Ext.data.ArrayStore( {
            id: 0,
            fields:[
                'Id',
                'displayText'
            ],
            data: [[1, 'Semua'],[2, 'Posting'], [3, 'Belum Posting']]
        }),
        valueField: 'Id',
        displayField: 'displayText',
        value:selectCountStatusPostingApotekResepRWI,
        listeners:{
			'select': function(a,b,c){
				selectCountStatusPostingApotekResepRWI=b.data.displayText ;
				tmpkriteria = getCriteriaCariApotekResepRWI();
				refreshRespApotekRWI(tmpkriteria);
			}
        }
	});
	return cboStatusPostingApotekResepRWI;
};

function ComboUnitApotekResepRWI(){
    var Field_Vendor = ['KD_UNIT', 'NAMA_UNIT'];
    ds_unit = new WebApp.DataStore({fields: Field_Vendor});
    ds_unit.load({
        params:{
	        Skip: 0,
	        Take: 1000,
	        Sort: 'kd_unit',
	        Sortdir: 'ASC',
	        target: 'ComboUnitApotek',
	        param: "parent='100'"
        }
    });
    var cbo_UnitRWI = new Ext.form.ComboBox({
			id: 'cbo_UnitResepRWI',
            fieldLabel: 'Ruangan',
			valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
			emptyText:'Unit/Ruang',
			store: ds_unit,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:150,
			tabIndex:2,
			listeners:{ 
				'select': function(a,b,c){
					selectSetUnit=b.data.displayText ;
					tmpkriteria = getCriteriaCariApotekResepRWI();
					refreshRespApotekRWI(tmpkriteria);
				},
				'specialkey' : function(){
					if (Ext.EventObject.getKey() === 13) {
						tmpkriteria = getCriteriaCariApotekResepRWI();
						refreshRespApotekRWI(tmpkriteria);
					} 						
				}
			}
        }
    )    
    return cbo_UnitRWI;
}

function viCombo_VendorLookup(){
    var cbo_UnitRWILookup = new Ext.form.ComboBox({
        flex: 1,
		id: 'cbo_UnitRWILookup',
        fieldLabel: 'Vendor',
		valueField: 'KD_VENDOR',
        displayField: 'VENDOR',
		emptyText:'PBF',
		store: ds_unit,
        mode: 'local',
        typeAhead: true,
        triggerAction: 'all',
        lazyRender: true,
		width:190,
		tabIndex:2,
		listeners:{ 
			'select': function(a,b,c){
				selectSetUnit=b.data.displayText ;
			},
			'specialkey' : function(){
				if (Ext.EventObject.getKey() === 13) {
					
				} 						
			}
		}
    })  ;  
    return cbo_UnitRWILookup;
}

function ComboUnitResepRWILookup(){
    var Field_Vendor = ['KD_UNIT', 'NAMA_UNIT'];
    ds_unitL = new WebApp.DataStore({fields: Field_Vendor});
    ds_unitL.load({
		params:{
			Skip: 0,
			Take: 1000,
			Sort: 'kd_unit',
			Sortdir: 'ASC',
			target: 'ComboUnitApotek',
			param: "parent='100'"
		}
	});
    var cbo_Unit = new Ext.form.ComboBox({
		x:700,
		y:25,
		id: 'cbo_UnitResepRWIL',
		fieldLabel: 'Unit',
		valueField: 'KD_UNIT',
		displayField: 'NAMA_UNIT',
		emptyText:'Unit',
		store: ds_unitL,
		valueField: 'KD_UNIT',
		displayField: 'NAMA_UNIT',
		mode: 'local',
		typeAhead: true,
		readOnly: true,
		triggerAction: 'all',
		lazyRender: true,
		width:100,
		listeners:{ 
			'select': function(a,b,c){
				//selectSetUnit=b.data.valueField;
			},
			'specialkey' : function(){
				if (Ext.EventObject.getKey() === 13) {
					/* tmpkriteria = getCriteriaCariApotekResepRWJ();
					refeshRespApotekRWJ(tmpkriteria); */
				} 						
			}
		}
	});    
    return cbo_Unit;
}

function mComboJenisByrResepRWI() 
{
	var Field = ['JENIS_PAY','DESKRIPSI','TYPE_DATA'];

    dsJenisbyrView = new WebApp.DataStore({ fields: Field });
    dsJenisbyrView.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000, Sort: 'jenis_pay',
			    Sortdir: 'ASC',
			    target: 'ViewJenisPay',
				param: "TYPE_DATA IN (0,1,3) AND DB_CR='0' ORDER BY Type_data"
			  
			}
		}
	);
	
    var cboJenisByr = new Ext.form.ComboBox
	(
		{
	
		    id: 'cboJenisByrResepRWI',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
			fieldLabel: 'Pembayaran',
		    align: 'Right',
		    width:150,
		    store: dsJenisbyrView,
		    valueField: 'JENIS_PAY',
		    displayField: 'DESKRIPSI',
			value:'TUNAI',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
					loaddatastorePembayaran(b.data.JENIS_PAY);
				}
				  

			}
		}
	);
	
    return cboJenisByr;
};
function loaddatastorePembayaran(jenis_pay)
{
    dsComboBayar.load
	(
		{
		 params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'nama',
					Sortdir: 'ASC',
					target: 'ViewComboBayar',
					param: 'jenis_pay=~'+ jenis_pay+ '~'
				}
	   }
	)      
}

function mComboPembayaranRWI()
{
    var Field = ['KD_PAY','JENIS_PAY','PAYMENT'];

    dsComboBayar = new WebApp.DataStore({fields: Field});
	
    var cboPembayaran = new Ext.form.ComboBox
	(
		{
		    id: 'cboPembayaranRWI',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
            forceSelection: true,
		    emptyText:'Pilih Pembayaran...',
		    labelWidth:80,
		    align: 'Right',
		    store: dsComboBayar,
		    valueField: 'KD_PAY',
		    displayField: 'PAYMENT',
			value: 'TUNAI',
			width:225,
			listeners:
			{
			    'select': function(a, b, c) 
				{
					tapungkd_pay=b.data.KD_PAY;
					Ext.getCmp('txtTmpKdPayResepRWIL').setValue(b.data.KD_PAY);
				}
				  

			}
		}
	);

    return cboPembayaran;
};

function ViewDetailPembayaranObatRWI(no_out,tgl_out) 
{	
    var strKriteriaRWI='';
    strKriteriaRWI = "a.no_out = '" + no_out + "'" + " And a.tgl_out ='" + tgl_out + "' order by a.urut";
   
    dsTRDetailHistoryBayar.load({
	    params:{
		    Skip: 0,
		    Take: 1000,
		    Sort: 'tgl_transaksi',
		    Sortdir: 'ASC',
		    target: 'ViewHistoryPembayaran',
		    param: strKriteriaRWI
		}
	}); 
	return dsTRDetailHistoryBayar;
	
    
};

function mComboorder_rwi()
{ 
 
 var Field = ['kd_pasien','nama','kd_dokter','kd_unit','kd_customer','no_transaksi','kd_kasir','id_mrresep','urut_msauk','tgl_masuk'];

    dspasienorder_mng_apotek_rwi = new WebApp.DataStore({ fields: Field });
	
	load_data_pasienorder();
	cbopasienorder_mng_apotek_rwi= new Ext.form.ComboBox
	(
		{
			id: 'cbopasienorder_mng_apotek_rwi',
			typeAhead		: true,
			triggerAction	: 'all',
			lazyRender		: true,
			disabled		: true,
			mode			: 'local',
			emptyText: '',
			fieldLabel:  ' ',
			align: 'Right',
			width: 160,
			store: dspasienorder_mng_apotek_rwi,
			valueField: 'display',
			displayField: 'display',
			//hideTrigger		: true,
			listeners:
			{
				select	: function(a,b,c){
							ordermanajemenrwi=true;
				            kd_pasien_obbt_rwi =b.data.kd_pasien;
							kd_unit_obbt_rwi=b.data.kd_unit;
							tgl_masuk_obbt_rwi =b.data.tgl_masuk;
							urut_masuk_obbt_rwi=b.data.urut_masuk; 
							Ext.getCmp('cbo_DokterApotekResepRWI').setValue(b.data.kd_dokter);
							Ext.getCmp('cboKodePasienResepRWI').setValue(b.data.kd_pasien);
							Ext.getCmp('cbo_UnitResepRWIL').setValue(b.data.kd_unit);
							Ext.getCmp('cboPilihankelompokPasienAptResepRWI').setValue(b.data.kd_customer);
							Ext.getCmp('txtNoKamarApotekResepRWIL').setValue(b.data.no_kamar);
							Ext.getCmp('txtKamarApotekResepRWIL').setValue(b.data.nama_kamar);
							Ext.getCmp('txtNamaPasienResepRWI_viApotekResepRWI').setValue(b.data.nama);
							ResepRWI.vars.no_transaksi=b.data.no_transaksi;
							ResepRWI.vars.tgl_transaksi=b.data.tgl_transaksi;
							ResepRWI.vars.kd_kasir=b.data.kd_kasir;
							Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').setValue(b.data.kd_customer);
							Ext.getCmp('txtTmpKdDokterApotekResepRWIL').setValue(b.data.kd_dokter);
							Ext.getCmp('txtTmpKdUnitApotekResepRWIL').setValue(b.data.kd_unit);
							Ext.getCmp('btnAddObatRWI').enable();
							Ext.getCmp('btnSimpan_viApotekResepRWI').enable();
							Ext.getCmp('btnSimpanExit_viApotekResepRWI').enable();
							Ext.getCmp('btnDelete_viApotekResepRWI').enable();
							Ext.getCmp('statusservice_apt_RWI').enable();
							dataGridObatApotekResep_penatarwi(b.data.id_mrresep,b.data.kd_customer);
							CurrentIdMrResepRwi=b.data.id_mrresep;
							
							cekDilayani(b.data.id_mrresep,b.data.order_mng);
					},
				   keyUp: function(a,b,c){
				    	$this1=this;
				    	if((b.getKey()>=65 && b.getKey()<=90)|| (b.getKey()>=48 && b.getKey()<=57) || b.getKey()==32 || b.getKey()==8 || b.getKey()==46){
				    		clearTimeout(this.time);
				    	
				    		this.time=setTimeout(function(){
			    				if($this1.lastQuery != '' ){
				    				var value=$this1.lastQuery;
				    				var param={};
		        		    		param['text']=$this1.lastQuery;
		        		    		load_data_pasienorder($this1.lastQuery);

		        		    		
			    				}
		    		    	},1000);
				    	}
				    }
			}
		}
	);return cbopasienorder_mng_apotek_rwi;
};


function datainit_viApotekResepRWI(rowdata)
{
	jumlah_tuslah = rowdata.JASA;
    // AddNewPenJasRad = false;
	// console.log(rowdata.KD_UNIT_KAMAR);
	// console.log(rowdata.KD_UNIT);
	dataisi=1;
	// Ext.getCmp('txtNoTrApotekResepRWIL').setValue(rowdata.NO_OUT);
	Ext.getCmp('txtSpesialApotekResepRWIL').setValue(rowdata.SPESIALISASI);
	Ext.getCmp('txtNoSep_viResepRWI').setValue(rowdata.NO_SJP);
	Ext.getCmp('txtNoTlp_viResepRWI').setValue(rowdata.TELEPON);
	Ext.getCmp('txtNoResepApotekResepRWIL').setValue(rowdata.NO_RESEP);
	Ext.getCmp('txtCatatanResepRWI_viResepRWI').setValue(rowdata.CATATANDR);
	Ext.getCmp('cbo_UnitResepRWIL').setValue(rowdata.NAMA_UNIT);
	Ext.getCmp('cbo_DokterApotekResepRWI').setValue(rowdata.NAMA_DOKTER);
	Ext.getCmp('cboPilihankelompokPasienAptResepRWI').setValue(rowdata.CUSTOMER);
	// Ext.getCmp('cboKodePasienResepRWI').setValue(rowdata.KD_PASIENAPT);
	Ext.getCmp('txtKdPasienResepRWI_viApotekResepRWI').setValue(rowdata.KD_PASIENAPT);
	Ext.getCmp('txtNamaPasienResepRWI_viApotekResepRWI').setValue(rowdata.NMPASIEN);
	Ext.getCmp('txtNoKamarApotekResepRWIL').setValue(rowdata.NO_KAMAR);
	Ext.getCmp('txtKamarApotekResepRWIL').setValue(rowdata.NAMA_KAMAR);
	Ext.getCmp('dfTglResepSebenarnyaResepRWI').setValue(ShowDate(rowdata.TGL_OUT));
	
	Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').setValue(rowdata.KD_CUSTOMER);
	Ext.getCmp('txtTmpKdDokterApotekResepRWIL').setValue(rowdata.DOKTER);
	Ext.getCmp('txtTmpKdUnitApotekResepRWIL').setValue(rowdata.KD_UNIT);
	Ext.getCmp('txtTmpNooutResepRWIL').setValue(rowdata.NO_OUT);
	Ext.getCmp('txtTmpTgloutResepRWIL').setValue(rowdata.TGL_OUT);
	Ext.getCmp('txtTmpSisaAngsuranResepRWIL').setValue(rowdata.SISA);
	Ext.getCmp('txtTmpStatusPostResepRWIL').setValue(rowdata.STATUS_POSTING);
	Ext.getCmp('txtTmpJenisPayResepRWIL').setValue(rowdata.JENIS_PAY);
	Ext.getCmp('txtTmpKdPayResepRWIL').setValue(rowdata.KD_PAY);
	Ext.getCmp('txtTmpTglResepSebenarnyaResepRWIL').setValue(rowdata.TGL_RESEP);
	
	ResepRWI.vars.tgl_transaksi=rowdata.TGL_TRANSAKSI;
	ResepRWI.vars.no_transaksi=rowdata.APT_NO_TRANSAKSI;
	ResepRWI.vars.kd_kasir=rowdata.APT_KD_KASIR;
	ResepRWI.vars.urut_masuk=rowdata.URUT_MASUK;
	
	ResepRWI.vars.payment=rowdata.PAYMENT;
	ResepRWI.vars.payment_type=rowdata.PAYMENT_TYPE;
	
	kd_pasien_obbt_rwi=rowdata.KD_PASIENAPT;
	kd_unit_obbt_rwi=rowdata.KD_UNIT;
	tgl_masuk_obbt_rwi=rowdata.TGL_MASUK;
	urut_masuk_obbt_rwi=rowdata.URUT_MASUK; 
	
	kd_spesial_tr=rowdata.KD_SPESIAL;
	kd_unit_kamar_tr=rowdata.KD_UNIT_KAMAR;
	no_kamar_tr=rowdata.NO_KAMAR;
		
	if(rowdata.ADMPRHS ==null){
		Ext.getCmp('txtAdmPrshResepRWIL').setValue(0);
	}else{
		Ext.getCmp('txtAdmPrshResepRWIL').setValue(rowdata.ADMPRHS);
	}
	if(rowdata.ADMRESEP == null){
		Ext.getCmp('txtAdmResepRWIL').setValue(0);
	}else{
		Ext.getCmp('txtAdmResepRWIL').setValue(rowdata.ADMRESEP);
	}
	if(rowdata.JASA == null){
		Ext.getCmp('txtTuslahEmbalaseL').setValue(0);
		jumlah_tuslah = 0;
	}else{
		Ext.getCmp('txtTuslahEmbalaseL').setValue(toFormat(rowdata.JASA));
		jumlah_tuslah = rowdata.JASA;
	}
	
	if (rowdata.CHECK_TUSLAH == 1) {
		Ext.getCmp('Chck_tuslah_viApotekResepRWI').setValue(true);
	}else{
		Ext.getCmp('Chck_tuslah_viApotekResepRWI').setValue(false);
	}
	
	if(rowdata.STATUS_POSTING === '1'){
		Ext.getCmp('btnunposting_viApotekResepRWI').enable();
		Ext.getCmp('btnPrint_viResepRWI').enable();
		Ext.getCmp('btnBayar_viApotekResepRWI').disable();
		Ext.getCmp('btnAdd_viApotekResepRWI').enable();
		Ext.getCmp('btnSimpan_viApotekResepRWI').disable();
		Ext.getCmp('btnSimpanExit_viApotekResepRWI').disable();
		Ext.getCmp('btnDelete_viApotekResepRWI').disable();
		Ext.getCmp('btnAddObatRWI').disable();
		nonaktivRWI(true);
		Ext.getCmp('btn1/2resep_viApotekResepRWI').disable();
		ResepRWI.form.Grid.a.disable();
		// ResepRWI.form.Panel.a.update('<img src="'+baseURL+'ui/images/icons/16x16/hijau.png"/>');
		ResepRWI.form.Panel.a.html = '<img src="'+baseURL+'ui/images/icons/16x16/hijau.png"/>';
			
	} else{
		Ext.getCmp('btnSimpan_viApotekResepRWI').disable();
		Ext.getCmp('btnSimpanExit_viApotekResepRWI').disable();
		Ext.getCmp('btnunposting_viApotekResepRWI').disable();
		Ext.getCmp('btnPrint_viResepRWI').disable();
		Ext.getCmp('btnBayar_viApotekResepRWI').enable();
		Ext.getCmp('btnTransfer_viApotekResepRWI').enable();
		Ext.getCmp('btnDelete_viApotekResepRWI').enable();
		Ext.getCmp('btnDeleteHistory_viApotekResepRWI').enable();
		Ext.getCmp('btnAddObatRWI').enable();
		nonaktivRWI(false);
		Ext.getCmp('btn1/2resep_viApotekResepRWI').enable();
		// ResepRWI.form.Panel.a.update('<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>');
		ResepRWI.form.Panel.a.html = '<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>';
	}
	//dataGridObatApotekResepRWJ(rowdata.NO_OUT,rowdata.TGL_OUT,rowdata.ADMRACIK);
	getSeCoba(rowdata.NO_OUT,rowdata.TGL_OUT,rowdata.ADMRACIK);
	Ext.Ajax.request(
	{
	   
		url: baseURL + "index.php/apotek/currentShiftAPT/getcurrentshift",
		 params: {
			
			command: '0'
		
		},
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);
			ResepRWI.form.Panel.shift.update(cst.shift);
		}
	
	});
	
	//Ext.getCmp('tshift').setValue(tampungshiftsekarang)
};

function viewDetailGridOrderRWI(CurrentGridOrderRWI){
	
	
	kd_spesial_tr=CurrentGridOrderRWI.kd_spesial;
	kd_unit_kamar_tr=CurrentGridOrderRWI.kd_unit_kamar;
	no_kamar_tr=CurrentGridOrderRWI.no_kamar;
	
	kd_pasien_obbt_rwi =CurrentGridOrderRWI.kd_pasien;
	kd_unit_obbt_rwi=CurrentGridOrderRWI.kd_unit;
	tgl_masuk_obbt_rwi = CurrentGridOrderRWI.tgl_masuk;
	urut_masuk_obbt_rwi=CurrentGridOrderRWI.urut_masuk; 
	
	console.log(tgl_masuk_obbt_rwi,urut_masuk_obbt_rwi);
	Ext.getCmp('cbo_DokterApotekResepRWI').setValue(CurrentGridOrderRWI.kd_dokter);
	Ext.getCmp('txtKdPasienResepRWI_viApotekResepRWI').setValue(CurrentGridOrderRWI.kd_pasien);
	Ext.getCmp('cbo_UnitResepRWIL').setValue(CurrentGridOrderRWI.kd_unit);
	Ext.getCmp('cboPilihankelompokPasienAptResepRWI').setValue(CurrentGridOrderRWI.customer);
	Ext.getCmp('txtNoKamarApotekResepRWIL').setValue(CurrentGridOrderRWI.no_kamar);
	Ext.getCmp('txtKamarApotekResepRWIL').setValue(CurrentGridOrderRWI.nama_kamar);
	Ext.getCmp('txtNamaPasienResepRWI_viApotekResepRWI').setValue(CurrentGridOrderRWI.nama);
	Ext.getCmp('txtSpesialApotekResepRWIL').setValue(CurrentGridOrderRWI.spesialisasi);
	ResepRWI.vars.no_transaksi=CurrentGridOrderRWI.no_transaksi;
	ResepRWI.vars.tgl_transaksi=CurrentGridOrderRWI.tgl_transaksi;
	ResepRWI.vars.kd_kasir=CurrentGridOrderRWI.kd_kasir;
	ResepRWI.vars.urut_masuk=CurrentGridOrderRWI.urut_masuk;
	ResepRWI.vars.payment_type=CurrentGridOrderRWI.payment_type;
	ResepRWI.vars.payment=CurrentGridOrderRWI.payment_type;
	
	Ext.getCmp('txtTmpKdPayResepRWIL').setValue(CurrentGridOrderRWI.kd_pay);
	Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').setValue(CurrentGridOrderRWI.kd_customer);
	Ext.getCmp('txtTmpKdDokterApotekResepRWIL').setValue(CurrentGridOrderRWI.kd_dokter);
	Ext.getCmp('txtTmpKdUnitApotekResepRWIL').setValue(CurrentGridOrderRWI.kd_unit);
	Ext.getCmp('txtTmpJenisPayResepRWIL').setValue(CurrentGridOrderRWI.jenis_pay);
	
	dataGridObatApotekResep_penatarwi(CurrentGridOrderRWI.id_mrresep,CurrentGridOrderRWI.kd_customer,CurrentGridOrderRWI.order_mng);
	
	
	
	
	/* hasilJumlahResepRWILoad();
	getAdm(Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').getValue()); */
}

function getAdm(kd_customer){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEKrwi/getAdm",
			params: {kd_customer:kd_customer},
			failure: function(o)
			{
				ShowPesanErrorResepRWI('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					Ext.getCmp('txtAdmResepRWIL').setValue(toFormat(cst.adm));
					Ext.getCmp('txtTuslahEmbalaseL').setValue(toFormat(cst.tuslah));
					jumlah_tuslah = toFormat(cst.tuslah);
					hasilJumlahResepRWI();
				}
				else 
				{
					Ext.getCmp('txtAdmResepRWIL').setValue(toFormat(cst.adm));
					Ext.getCmp('txtTuslahEmbalaseL').setValue(toFormat(cst.tuslah));
					jumlah_tuslah = toFormat(cst.tuslah);
					hasilJumlahResepRWI();
					//ShowPesanErrorResepRWI('Gagal mengambil tuslah dan adm', 'Error');
				};
			}
		}
		
	)
	
}
function getSeCoba(no_out,tgl_out,admracik){//get kd_unit_far from session
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEKrwi/sess",
			params: {query:"no_out = " + no_out},
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					kd=cst.session;
					dataGridObatApotekResepRWI(no_out,tgl_out,admracik,kd);
				}
			}
		}
	);
}


function cekTransferRWI(no_out,tgl_out,callback){//get kd_unit_far from session
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEKrwi/cekTransfer",
			params: {
				no_out: no_out,
				tgl_out: tgl_out
			},
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					unpostingRESEPRWI(callback);
				} 
				else{
					ShowPesanWarningResepRWI(cst.pesan,'Warning');
				}
			}
		}
	);
}

function pembayaranResepRWI(){
	if(ValidasiBayarResepRWI(nmHeaderSimpanData,false) == 1 ){
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionAPOTEKrwi/bayarSaveResepRWI",
				params: getParamBayarResepRWI(),
				failure: function(o)
				{
					ShowPesanErrorResepRWI('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						
						if(toInteger(Ext.getCmp('txtBayarResepRWI_Pembayaran').getValue()) >= toInteger(Ext.getCmp('txtTotalResepRWI_Pembayaran').getValue())){
							ShowPesanInfoResepRWI('Berhasil melakukan pembayaran dan pembayaran telah lunas','Information');
							Ext.getCmp('btnBayar_ResepRWILookupBayar').disable();
							Ext.getCmp('txtNoResepApotekResepRWIL').setValue(cst.noresep);
							Ext.getCmp('txtTmpNooutResepRWIL').setValue(cst.noout);
							Ext.getCmp('txtTmpTgloutResepRWIL').setValue(cst.tgl);
							Ext.getCmp('btnunposting_viApotekResepRWI').enable();
							Ext.getCmp('btnPrint_viResepRWI').enable();
							Ext.getCmp('btnBayar_viApotekResepRWI').disable();
							Ext.getCmp('btnAdd_viApotekResepRWI').enable();
							Ext.getCmp('btnSimpan_viApotekResepRWI').disable();
							Ext.getCmp('btnSimpanExit_viApotekResepRWI').disable();
							Ext.getCmp('btnDelete_viApotekResepRWI').disable();
							Ext.getCmp('btnTransfer_viApotekResepRWI').disable();
							//Ext.getCmp('txtBayarResepRWI_Pembayaran').disable();
							Ext.getCmp('btnDeleteHistory_viApotekResepRWI').disable();
							// ResepRWI.form.Panel.a.update('<img src="'+baseURL+'ui/images/icons/16x16/hijau.png"/>');
							ResepRWI.form.Panel.a.html = '<img src="'+baseURL+'ui/images/icons/16x16/hijau.png"/>';
							ViewDetailPembayaranObatRWI(cst.noout,cst.tgl);
							gridDTLTRHistoryApotekRWI.getView().refresh();
							refreshRespApotekRWI();
							setLookUpApotek_bayarResepRWI.close();
							nonaktivRWI(true);
							Ext.getCmp('btn1/2resep_viApotekResepRWI').disable();
							ResepRWI.form.Grid.a.disable();
							
							if(ordermanajemenrwi==true){
								updatestatus_permintaan();
								load_data_pasienorder();
							}
							
							setLookUpApotek_bayarResepRWI.close();
							total_pasien_order_mng_obtrwi();
							total_pasien_dilayani_order_mng_obtrwi();
							viewGridOrderAll_RASEPRWI();
							
							//insert_mrobatRWI();
							
						} else {
							refreshRespApotekRWI();
							ShowPesanInfoResepRWI('Berhasil melakukan pembayaran','Information');
							setLookUpApotek_bayarResepRWI.close();
							Ext.getCmp('btnBayar_viApotekResepRWI').enable();
							Ext.getCmp('btnunposting_viApotekResepRWI').disable();
							Ext.getCmp('btnPrint_viResepRWI').disable();
							ViewDetailPembayaranObatRWI(Ext.getCmp('txtTmpNooutResepRWIL').getValue(),Ext.getCmp('txtTmpTgloutResepRWIL').getValue());
							gridDTLTRHistoryApotekRWI.getView().refresh();
							total_pasien_order_mng_obtrwi();
							total_pasien_dilayani_order_mng_obtrwi();
							viewGridOrderAll_RASEPRWI();
							nonaktivRWI(true);
							Ext.getCmp('btn1/2resep_viApotekResepRWI').disable();
							ResepRWI.form.Grid.a.disable();
						}
					}
					else 
					{
						// ShowPesanErrorResepRWI('Gagal melakukan pembayaran. ' + cst.pesan, 'Error');
						Ext.Msg.show({
							title: 'Error',
							msg: 'Gagal melakukan pembayaran. ' + cst.pesan,
							buttons: Ext.MessageBox.OK,
							fn: function (btn) {
								if (btn == 'ok')
								{
									Ext.getCmp('txtBayarResepRWI_Pembayaran').focus(false,10);
								}
							}
						});
						refreshRespApotekRWI();
					};
				}
			}
			
		)
	}
}


function transferResepRWI(){
	if(ValidasiTransferResepRWI(nmHeaderSimpanData,false) == 1 ){
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionAPOTEKrwi/saveTransfer",
				params: getParamTransferResepRWI(),
				failure: function(o)
				{
					ShowPesanErrorResepRWI('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						refreshRespApotekRWI();
						
						ShowPesanInfoResepRWI('Transfer berhasil dilakukan','Information');
						Ext.getCmp('btnunposting_viApotekResepRWI').enable();
						Ext.getCmp('btnTransfer_viApotekResepRWI').disable();
						Ext.getCmp('btnPrint_viResepRWI').enable();
						Ext.getCmp('btnBayar_viApotekResepRWI').disable();
						Ext.getCmp('btnDeleteHistory_viApotekResepRWI').disable();
						Ext.getCmp('txtTmpStatusPostResepRWIL').setValue(1);
						setLookUpApotek_TransferResepRWI.close();
						ViewDetailPembayaranObatRWI(Ext.getCmp('txtTmpNooutResepRWIL').getValue(),Ext.getCmp('txtTmpTgloutResepRWIL').getValue());
						gridDTLTRHistoryApotekRWI.getView().refresh();
						nonaktivRWI(true);
						Ext.getCmp('btn1/2resep_viApotekResepRWI').disable();
						ResepRWI.form.Grid.a.disable();
						if(ordermanajemenrwi==true){
							updatestatus_permintaan()
							load_data_pasienorder();
						}
						
						//ResepRWI.form.Grid.a.store.removeAll()
						// ResepRWI.form.Panel.a.update('<img src="'+baseURL+'ui/images/icons/16x16/hijau.png"/>');
						ResepRWI.form.Panel.a.html = '<img src="'+baseURL+'ui/images/icons/16x16/hijau.png"/>';

						// Ext.getCmp('statusservice_apt_RWI').disable();
						total_pasien_order_mng_obtrwi();
						total_pasien_dilayani_order_mng_obtrwi();
						viewGridOrderAll_RASEPRWI();
						
						//insert_mrobatRWI()
					}
					else 
					{
						// ShowPesanErrorResepRWI('Gagal melakukan transfer. ' + cst.pesan, 'Error');
						Ext.Msg.show({
							title: 'Error',
							msg: 'Gagal melakukan transfer. '+ cst.pesan,
							buttons: Ext.MessageBox.OK,
							fn: function (btn) {
								if (btn == 'ok')
								{
									Ext.getCmp('txtTotalTransfer_ResepRWI').focus(false,10);
								}
							}
						});
						refreshRespApotekRWI();
					};
				}
			}
			
		)
	}
}

function insert_mrobatRWI(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionInsertMrObat/save_mrobat",
			params: getParamInsertMrObatRWI(),
			failure: function(o)
			{
				ShowPesanErrorResepRWI('Error simpan MR! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				
			}
		}
		
	)
}

function dataGridObatApotekResepRWI(no_out,tgl_out,admracik,kd){
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/functionAPOTEK/readGridObat",
		params: {query:"o.no_out = " + no_out + "" + " And o.tgl_out ='" + tgl_out + "' and b.kd_unit_far='"+kd+"' ",resep:'Y'},
		failure: function(o){
			ShowPesanErrorResepRWI('Hubungi Admin', 'Error');
		},	
		success: function(o) {
			var cst = Ext.decode(o.responseText);
			if (cst.success === true) {
				ResepRWI.form.Grid.a.store.removeAll();
				var recs=[],
					recType=dsDataGrdJab_viApotekResepRWI.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					if (cst.ListDataObj[i].racikan == '1' || cst.ListDataObj[i].racikan == 1 || cst.ListDataObj[i].racikan === true) {
						cst.ListDataObj[i].jumlah = (parseInt(cst.ListDataObj[i].harga_jual) *  parseInt(cst.ListDataObj[i].jml)) + parseInt(cst.ListDataObj[i].biaya_racik);
					}else{
						cst.ListDataObj[i].jumlah = parseInt(cst.ListDataObj[i].harga_jual) *  parseInt(cst.ListDataObj[i].jml);
					}
					recs.push(new recType(cst.ListDataObj[i]));
				}
				dsDataGrdJab_viApotekResepRWI.add(recs);
				ResepRWI.form.Grid.a.getView().refresh();
				// Ext.getCmp('txtAdmRacikResepRWIL').setValue(toFormat(admracik));
				Ext.getCmp('txtAdmRacikResepRWIL').setValue(toFormat(0));
				hasilJumlahResepRWI();
				hitung_adm_racik();
			}else{
				ShowPesanErrorResepRWI('Gagal membaca Data ini', 'Error');
			}
		}
	});
}

function getGridDetailObatApotekResepRWI(no_out,tgl_out,callback){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEKrwi/readGridDetailObat",
			params: {
				no_out:no_out,
				tgl_out:tgl_out,
				resep:'Y',
			},
			failure: function(o)
			{
				ShowPesanErrorResepRWI('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				
				if (cst.success === true) 
				{
					dsDataGrdJab_viApotekResepRWI.removeAll();
					var recs=[],
						recType=dsDataGrdJab_viApotekResepRWI.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						if (cst.ListDataObj[i].racikan == '1' || cst.ListDataObj[i].racikan == 1 || cst.ListDataObj[i].racikan === true) {
							cst.ListDataObj[i].jumlah = (parseInt(cst.ListDataObj[i].harga_jual) *  parseInt(cst.ListDataObj[i].jml)) + parseInt(cst.ListDataObj[i].biaya_racik);
						}else{
							cst.ListDataObj[i].jumlah = parseInt(cst.ListDataObj[i].harga_jual) *  parseInt(cst.ListDataObj[i].jml);
						}
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
					dsDataGrdJab_viApotekResepRWI.add(recs);
					
					ResepRWI.form.Grid.a.getView().refresh();
					// Ext.getCmp('txtAdmRacikResepRWIL').setValue(toFormat(admracik));
					hasilJumlahResepRWI();
					if(callback != undefined){
						callback();
					}
				}
				else 
				{
					ShowPesanErrorResepRWI('Gagal membaca Data ini', 'Error');
				};
			}
		}
		
	)
	
}

function getSisaAngsuran(stmpnoOut,stmptgl){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEKrwi/getSisaAngsuran",
			params: {no_out:stmpnoOut, tgl_out:stmptgl},
			failure: function(o)
			{
				ShowPesanErrorResepRWI('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				//console.log(cst);
				if (cst.success === true) 
				{
					console.log(cst.sisa);
					if(cst.sisa ==''){
						ShowPesanInfoResepRWI('Pembayaran sebelumnya telah lunas, jika data ini sudah pernah diposting maka hapus terlebih dahulu history pembayaran untuk mendapatkan total pembayaran','Information');
					} else{
						var sisa=aptpembulatan(cst.sisa);
						Ext.getCmp('txtTotalResepRWI_Pembayaran').setValue(toFormat(sisa));
						Ext.getCmp('txtBayarResepRWI_Pembayaran').setValue(toInteger(sisa));
						
					}
					
				}
				else 
				{
					Ext.getCmp('txtTotalResepRWJ_Pembayaran').setValue(Ext.getCmp('txtTotalBayarResepRWIL').getValue());
					Ext.getCmp('txtBayarResepRWI_Pembayaran').setValue(Ext.getCmp('txtTotalBayarResepRWIL').getValue());
					//ShowPesanErrorResepRWI('Gagal membaca Data ini', 'Error');
				};
			}
		}
		
	)
}


function datasave_viApotekResepRWI(){
	if (ValidasiEntryResepRWI(nmHeaderSimpanData,false) == 1 ){
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionAPOTEKrwi/saveResepRWI",
				params: getParamResepRWI(),
				failure: function(o)
				{
					ShowPesanErrorResepRWI('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoResepRWI(nmPesanSimpanSukses,nmHeaderSimpanData);
						Ext.get('txtNoResepApotekResepRWIL').dom.value=cst.noresep;
						Ext.get('txtTmpNooutResepRWIL').dom.value=cst.noout;
						// Ext.get('txtNoTrApotekResepRWIL').dom.value=cst.noout;
						Ext.get('txtTmpTgloutResepRWIL').dom.value=cst.tgl;
						Ext.getCmp('btnSimpan_viApotekResepRWI').disable();
						Ext.getCmp('btnSimpanExit_viApotekResepRWI').disable();
						Ext.getCmp('btnTransfer_viApotekResepRWI').enable();
						refreshRespApotekRWI();
						getGridDetailObatApotekResepRWI(cst.noout,cst.tgl);
						Ext.getCmp('btnBayar_viApotekResepRWI').enable();
						hasilJumlahRacik();
					}
					else 
					{
						ShowPesanErrorResepRWI('Gagal Menyimpan Data ini', 'Error');
						refreshRespApotekRWI();
					};
				}
			}
			
		)
	}
}

function unpostingRESEPRWI(callback){
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/functionAPOTEKrwi/cekBulanPOST",
		params: {tgl:Ext.getCmp('txtTmpTgloutResepRWIL').getValue()},
		success: function(o) {
			var cst = Ext.decode(o.responseText);
			if (cst.success === true){
				Ext.Msg.confirm('Warning', 'Apakah data ini akan diUnposting?', function(button){
					if (button == 'yes'){
						Ext.Ajax.request({
							url: baseURL + "index.php/apotek/functionAPOTEKrwi/unpostingResepRWI",
							params: getParamUnpostingResepRWI(),
							failure: function(o)
							{
								ShowPesanErrorResepRWI('Hubungi Admin', 'Error');
							},	
							success: function(o) 
							{
								var cst = Ext.decode(o.responseText);
								if (cst.success === true) 
								{
									ShowPesanInfoResepRWI('UnPosting Berhasil','Information');
									Ext.getCmp('txtTmpNooutResepRWIL').setValue(cst.noout);
									Ext.getCmp('txtTmpTgloutResepRWIL').setValue(cst.tgl);
									Ext.getCmp('btnunposting_viApotekResepRWI').disable();
									Ext.getCmp('btnBayar_viApotekResepRWI').disable();
									Ext.getCmp('btnSimpan_viApotekResepRWI').disable();
									Ext.getCmp('btnSimpanExit_viApotekResepRWI').disable();
									Ext.getCmp('btnDelete_viApotekResepRWI').enable();
									Ext.getCmp('btnPrint_viResepRWI').disable();
									Ext.getCmp('btnAddObatRWI').enable();
									Ext.getCmp('btnDeleteHistory_viApotekResepRWI').enable();
									Ext.getCmp('txtTmpStatusPostResepRWIL').setValue(0);
									// ResepRWI.form.Panel.a.update('<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>');
									ResepRWI.form.Panel.a.html = '<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>';
									refreshRespApotekRWI();
									nonaktivRWI(false);
									Ext.getCmp('btn1/2resep_viApotekResepRWI').enable();
									ResepRWI.form.Grid.a.enable();
									// unposting_mrobatRWI();
									if(callback != undefined){
										callback();
									}
								}
								else 
								{
									ShowPesanErrorResepRWI('Gagal melakukan unPosting. ' + cst.pesan, 'Error');
								};
							}
						})
					}
				});
			} else{
				if(cst.pesan=='Periode sudah ditutup'){
					ShowPesanErrorResepRWI('Periode sudah diTutup, tidak dapat melakukan transaksi','Error');
				} else{
					ShowPesanErrorResepRWI('Periode Bulan Lalu Harap Ditutup, tidak dapat melakukan transaksi','Error');
				}
			}
		}
	});
}

function unposting_mrobatRWI(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionInsertMrObat/unposting_mrobat",
			params: getParamUnPostingMrObatRWI(),
			failure: function(o)
			{
				ShowPesanErrorResepRWI('Error unPosting MR! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				
			}
		}
		
	)
}

function cekPeriodeBulanRwi(detailorderrwi,CurrentGridOrderRWI){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEKrwi/cekBulan",
			params: {a:"no_out"},
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					if(detailorderrwi==true){
						setLookUp_viApotekResepRWI('',CurrentGridOrderRWI);
						// viewDetailGridOrderRWI(CurrentGridOrderRWI);
						
					} else{
						setLookUp_viApotekResepRWI();
					}
					
				} else{
					if(cst.pesan=='Periode Bulan ini sudah Ditutup'){
						ShowPesanErrorResepRWI('Periode bulan ini sudah diTutup, tidak dapat melakukan transaksi','Error');
					} else{
						ShowPesanErrorResepRWI('Periode Bulan Lalu Harap Ditutup, tidak dapat melakukan transaksi','Error');
					}
					
				}
			}
		}
	);
}

function total_pasien_order_mng_obtrwi()
{
	Ext.Ajax.request(
	{
		url: baseURL + "index.php/apotek/functionAPOTEKrwi/countpasienmr_resep_rwi",
			params: {
			command: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
		},
		failure: function(o)
		{
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);

			Ext.getCmp('txtcounttr_apt_rwi').setValue(cst.countpas);
			console.log(cst);
		}
	});
};

function total_pasien_dilayani_order_mng_obtrwi(){
	Ext.Ajax.request(
	{
		url: baseURL + "index.php/apotek/functionAPOTEKrwi/countpasienmrdilayani_resep_rwi",
			params: {
			command: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
		},
		failure: function(o)
		{
			var cst = Ext.decode(o.responseText);
			
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);

			Ext.getCmp('txtcounttrDilayani_apt_rwi').setValue(cst.countpas);
			console.log(cst);
		}
	});
}


function updatestatus_permintaan(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEKrwi/update_obat_mng_rwi",
			params: getParamCloseOrder(),
			failure: function(o)
			{
				ShowPesanErrorResepRWI('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				ShowPesanInfoResepRWI('Obat sudah selesai dilayani','order')
				viewGridOrderAll_RASEPRWI();
			}
		}
		
	)
	
}



function dataGridObatApotekResep_penatarwi(id_mrresep,cuss,order_mng){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEKrwi/getobatdetail_frompoli",
			params: {query:id_mrresep,
					 cus :cuss,
					 resep:'Y'
			},
			failure: function(o)
			{
				//ShowPesanErrorResepRWI('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{    ResepRWI.form.Grid.a.store.removeAll();
			
				//dsDataGrdJab_viApotekResepRWI.loadData([],false);
				var cst = Ext.decode(o.responseText);

				if (cst.success === true) 
				{
					console.log(cst);
					
					var recs=[],
						recType=dsDataGrdJab_viApotekResepRWI.recordType;

					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
						
					}
					dsDataGrdJab_viApotekResepRWI.add(recs);
					
					ResepRWI.form.Grid.a.getView().refresh();
					
					
					getAdm(Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').getValue());
					cekDilayani(id_mrresep,order_mng);
					hitung_adm_racik();
				} 
				else 
				{
					ShowPesanErrorResepRWI('Gagal membaca Data ini', 'Error');
				};
			}
		}
		
	)
	
}

function load_data_pasienorder(param)
{
	Ext.Ajax.request(
	{
		url: baseURL + "index.php/apotek/functionAPOTEKrwi/getPasienorder_mng_rwi",
		params:{
			command: param
		} ,
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cbopasienorder_mng_apotek_rwi.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dspasienorder_mng_apotek_rwi.recordType;
				var o=cst['listData'][i];
			
				recs.push(new recType(o));
				dspasienorder_mng_apotek_rwi.add(recs);
				console.log(o);
			}
		}
	});
}
function viewGridOrderAll_RASEPRWI(nama,tgl){
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/functionAPOTEKrwi/vieworderall_rwi",
		params: {
			nama:nama,
			tgl:tgl
		},
		failure: function(o){
			ShowPesanErrorResepRWI('Hubungi Admin', 'Error');
		},	
		success: function(o){   
			//dataSourceGridOrder_viApotekResepRWI.loadData([],false);
			dataSourceGridOrder_viApotekResepRWI.removeAll();
			var cst = Ext.decode(o.responseText);
			if (cst.success === true){
				var recs=[],
					recType=dataSourceGridOrder_viApotekResepRWI.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));
				}
				dataSourceGridOrder_viApotekResepRWI.add(recs);
				GridDataViewOrderManagement_viApotekResepRWI.getView().refresh();
			}else{
				ShowPesanErrorResepRWI('Gagal membaca Data ini', 'Error');
			}
		}
	});
}
function printbill_ResepRWI()
{
    Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/main/CreateDataObj",
			params: datacetakbill(),
			failure: function(o)
			{	
				ShowPesanErrorResepRWI('Error hubungi admin' ,'Error');
			},
			success: function(o)
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					
				}
				else if  (cst.success === false && cst.pesan===0)
				{
					ShowPesanWarningResepRWI('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
				}
				else
				{
					ShowPesanErrorResepRWI('Gagal print '  + 'Error');
				}
			}
		}
	)
}

function printkwitansi_resep_rwi()
{
    Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/cetakKwitansi/save",
			params: {
				Table: 'cetakKwitansi',
				kd_pasien:Ext.getCmp('txtKd_pasienPrintKwitansi_viApotekResepRWI').getValue(),
				pembayar:Ext.getCmp('txtNamaPembayarPrintKwitansi_viApotekResepRWI').getValue(),
				jumlah_bayar: toInteger(Ext.getCmp('txtJumlahBayarPrintKwitansi_viApotekResepRWI').getValue()),
				nama:Ext.getCmp('txtNamaPasienResepRWI_viApotekResepRWI').getValue(),
				keterangan_bayar:Ext.getCmp('txtKeteranganPembayaranPrintKwitansi_viApotekResepRWI').getValue(),
				no_out:Ext.getCmp('txtTmpNooutResepRWIL').getValue(),
				tgl_out:Ext.getCmp('txtTmpTgloutResepRWIL').getValue(),
				no_resep:Ext.getCmp('txtNoResepApotekResepRWIL').getValue(),
				// printer:Ext.getCmp('cbopasienorder_printer_rwi').getValue(),
				kd_form:KdFormResepRWI
			},
			failure: function(o)
			{	
				ShowPesanErrorResepRWI('Error hubungi admin' ,'Error');
			},
			success: function(o)
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					
				}
				else if  (cst.success === false && cst.pesan===0)
				{
					ShowPesanWarningResepRWI('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
				}
				else
				{
					ShowPesanErrorResepRWI('Gagal print '  + 'Error');
				}
			}
		}
	)
}

function cekDilayani(id_mrresep,order_mng){
	if(order_mng == 'Dilayani'){
		ShowPesanInfoResepRWI('Resep pasien ini sudah dilayani dan pembayaran/transfer sudah dilakukan. Untuk melihat detail resep ini dapat di lihat di daftar resep telah di buat!','Information');
		setLookUps_viApotekResepRWI.close();
	} else if(order_mng == ''){
		
	}else{
		Ext.Ajax.request(
		{
			url: baseURL + "index.php/apotek/functionAPOTEKrwi/cekDilayani",
			params: getParamCekDilayani(id_mrresep),
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)  {
					
					/* 	ShowPesanInfoResepRWI('Resep pasien ini sudah dilayani dan pembayaran/transfer sudah dilakukan. Untuk melihat detail resep ini dapat di lihat di daftar resep telah di buat!','Information');
						setLookUps_viApotekResepRWI.close(); */
					
						// Ext.getCmp('btnAddObatRWI').enable();
						Ext.getCmp('btnSimpan_viApotekResepRWI').enable();
						Ext.getCmp('btnSimpanExit_viApotekResepRWI').enable();
						Ext.getCmp('btnDelete_viApotekResepRWI').enable();
						Ext.getCmp('statusservice_apt_RWI').enable();
						Ext.getCmp('btnBayar_viApotekResepRWI').enable();
						Ext.getCmp('btnTransfer_viApotekResepRWI').enable();
					
				} else{
					ShowPesanInfoResepRWI('Resep pasien ini sudah dibuat. Pembayaran belum dilakukan, untuk pembayaran/transfer resep ini dapat di lihat di daftar resep diatas!','Warning');
					setLookUps_viApotekResepRWI.close();
					// Ext.getCmp('btnAddObatRWI').disable();
					Ext.getCmp('btnSimpan_viApotekResepRWI').enable();
					// Ext.getCmp('btnSimpanExit_viApotekResepRWI').disable();
					Ext.getCmp('btnDelete_viApotekResepRWI').disable();
					// Ext.getCmp('statusservice_apt_RWI').disable();
					Ext.getCmp('btnBayar_viApotekResepRWI').enable();
					Ext.getCmp('btnTransfer_viApotekResepRWI').enable();
					
				}
			}
		});
	}
	
	
}


function getParamResepRWI() 
{
	var KdCust='';
	var TmpCustoLama='';
	var tmpNonResep='';
	var tmpNamaPasien='';
	// if(Ext.get('txtTmpKdCustomerApotekResepRWIL').getValue()=='Perseorangan'){
			// KdCust='0000000001';
	// }else if(Ext.get('txtTmpKdCustomerApotekResepRWIL').getValue()=='Perusahaan'){
		// KdCust=Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').getValue();
	// }else {
		// KdCust=Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').getValue();
	// }
	
	/* if(Ext.getCmp('cbNonResep').getValue() == false){
		tmpNonResep = 1;
		tmpNamaPasien = AptResepRWJ.form.ComboBox.namaPasien.getValue();
	}else{
		tmpNonResep = 0;
		tmpNamaPasien = Ext.getCmp('txtNamaPasienNon').getValue();
	} */
	
	var ubah=0;
	if(Ext.getCmp('txtNoResepApotekResepRWIL').getValue() === '' || Ext.getCmp('txtNoResepApotekResepRWIL').getValue()=='Nomor Resep'){
		ubah=0;
	} else{
		ubah=1;
	}
	
    var params =
	{
		KdPasien:Ext.getCmp('txtKdPasienResepRWI_viApotekResepRWI').getValue(),
		NmPasien:Ext.getCmp('txtNamaPasienResepRWI_viApotekResepRWI').getValue(),		
		KdUnit: Ext.getCmp('txtTmpKdUnitApotekResepRWIL').getValue(),
		KdDokter:Ext.getCmp('txtTmpKdDokterApotekResepRWIL').getValue(),
		//NonResep:tmpNonResep,
		NoResepAsal:Ext.getCmp('txtNoResepApotekResepRWIL').getValue(),
		NoOutAsal:Ext.getCmp('txtTmpNooutResepRWIL').getValue(),
		TglOutAsal:Ext.getCmp('txtTmpTgloutResepRWIL').getValue(),
		StatusPost:Ext.getCmp('txtTmpStatusPostResepRWIL').getValue(),
		Tanggal:Ext.getCmp('dfTglResepSebenarnyaResepRWI').getValue(),
		JamOut:jam,
		DiscountAll:toInteger(Ext.getCmp('txtTotalDiscResepRWIL').getValue()),
		// AdmRacikAll:0,//toInteger(Ext.getCmp('txtAdmRacikResepRWIL').getValue()),
		AdmRacikAll:toInteger(Ext.getCmp('txtAdmRacikResepRWIL').getValue()),
		// JasaTuslahAll:toInteger(Ext.getCmp('txtTuslahEmbalaseL').getValue()),
		JasaTuslahAll:toInteger(jumlah_tuslah),
		Adm:0,//toInteger(Ext.getCmp('txtAdmResepRWIL').getValue()),
		Admprsh:0,//toInteger(Ext.getCmp('txtAdmPrshResepRWIL').getValue()),
		Kdcustomer:Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').getValue(),
		NoTransaksiAsal:ResepRWI.vars.no_transaksi,
		KdKasirAsal:ResepRWI.vars.kd_kasir,
		SubTotal:toInteger(Ext.getCmp('txtJumlahTotalResepRWIL').getValue()),
		Total:toInteger(Ext.getCmp('txtTotalBayarResepRWIL').getValue()),
		NoKamar:Ext.getCmp('txtNoKamarApotekResepRWIL').getValue(),
		JumlahItem:Ext.getCmp('txtTmpTotQtyResepRWIL').getValue(),
		Shift: tampungshiftsekarang,
		Ubah:ubah,
		Posting:0,
		IdMrResep:CurrentIdMrResepRwi, //id_mrresep untuk update status resep = sedang dilayani
		TglResep:Ext.getCmp('txtTmpTglResepSebenarnyaResepRWIL').getValue(),
		Catatandr:Ext.getCmp('txtCatatanResepRWI_viResepRWI').getValue(),
		KdSpesial:kd_spesial_tr,
		check_tuslah : Ext.getCmp('Chck_tuslah_viApotekResepRWI').getValue(),
	};
	
	params['jumlah']=dsDataGrdJab_viApotekResepRWI.getCount();
	// for(var i = 0 ; i < dsDataGrdJab_viApotekResepRWI.getCount();i++)
	// {
		// if(dsDataGrdJab_viApotekResepRWI.data.items[i].data.racik == 'Ya'){
			// params['racik-'+i]=1;
		// } else{
			// params['racik-'+i]=0;
		// }
		
		// if(dsDataGrdJab_viApotekResepRWI.data.items[i].data.cito == 'Ya'){
			// params['cito-'+i]=1;
			// params['hargaaslicito-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.hargaaslicito;
			// params['nilai_cito-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.nilai_cito;
		// } else{
			// params['cito-'+i]=0;
			// params['hargaaslicito-'+i]=0;
			// params['nilai_cito-'+i]=0;
		// }
		
		// params['kd_prd-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_prd
		// params['nama_obat-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.nama_obat
		// params['kd_satuan-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_satuan
		// params['harga_jual-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.harga_jual
		// params['harga_beli-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.harga_beli
		// params['kd_pabrik-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_pabrik
		// params['jml-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.jml
		// params['markup-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.markup
		// params['disc-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.disc
		// params['dosis-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.dosis
		// params['jasa-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.jasa
		// params['no_out-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.no_out
		// params['no_urut-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.no_urut
		// params['kd_milik-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_milik
		// if (dsDataGrdJab_viApotekResepRWI.data.items[i].data.no_racik == undefined || dsDataGrdJab_viApotekResepRWI.data.items[i].data.no_racik == ''){
			// params['no_racik-'+i]=0
		// }else{
			// params['no_racik-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.no_racik
		// }
		// params['aturan_racik-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.aturan_racik
		// params['aturan_pakai-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.aturan_pakai
	// }
	var line=0;
	for(var i = 0 ; i < dsDataGrdJab_viApotekResepRWI.getCount();i++){
		var o=dsDataGrdJab_viApotekResepRWI.data.items[i].data;
		// if(o.racikan ==false){
			params['racik-'+line]=0;
			if(dsDataGrdJab_viApotekResepRWI.data.items[i].data.cito == 'Ya'){
				params['cito-'+line]=1;
				params['hargaaslicito-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.hargaaslicito;
				params['nilai_cito-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.nilai_cito;
			} else{
				params['cito-'+line]=0;
				params['hargaaslicito-'+line]=0;
				params['nilai_cito-'+line]=0;
			}
			params['kd_prd-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_prd
			params['nama_obat-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.nama_obat
			params['kd_satuan-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_satuan
			params['harga_jual-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.harga_jual
			params['harga_beli-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.harga_beli
			params['kd_pabrik-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_pabrik
			params['jml-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.jml
			params['signa-'+line]= o.signa;
			params['takaran-'+line]= o.takaran;
			params['cara_pakai-'+line]	= o.cara_pakai;
			params['markup-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.markup
			params['disc-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.disc
			params['jml_order-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.jml_order
			params['dosis-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.dosis
			params['jasa-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.jasa
			params['no_out-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.no_out
			params['no_urut-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.no_urut
			params['kd_milik-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_milik
			params['no_racik-'+line]=0;
			params['aturan_racik-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.aturan_racik
			params['aturan_pakai-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.aturan_pakai
			params['jumlah_racik-' +line]=0;
			params['satuan_racik-' +line]='';
			params['catatan_racik-' +line]='';

			if (o.racikan == "V" || o.racikan == 1 || o.racikan == "1") {
				params['racikan-' +line] = true;
			}else{
				params['racikan-' +line] = false;
			}

			line++;
		/*}else{
			console.log(o);
			var parse=JSON.parse(o.result);
			params.jumlah=(params.jumlah-1)+parse.length;
			for(var j=0,jLen=parse.length; j<jLen;j++){
				var k=parse[j];
				params['racik-'+line]=1;
				if(k.cito == 'Ya'){
					params['cito-'+line]=1;
					params['hargaaslicito-'+line]=k.hargaaslicito;
					params['nilai_cito-'+line]=k.nilai_cito;
				} else{
					params['cito-'+line]=0;
					params['hargaaslicito-'+line]=0;
					params['nilai_cito-'+line]=0;
				}
				params['kd_prd-'+line]=k.kd_prd;
				params['nama_obat-'+line]=k.nama_obat;
				params['kd_satuan-'+line]=k.kd_satuan;
				params['harga_jual-'+line]=k.harga_jual;
				params['harga_beli-'+line]=k.harga_beli;
				params['kd_pabrik-'+line]=k.kd_pabrik;
				params['jml-'+line]=k.jml;
				params['markup-'+line]=k.markup;
				params['jml_order-'+line]=k.jml_order;
				params['disc-'+line]=k.disc;
				params['signa-'+line]= o.signa;
				params['takaran-'+line]= o.takaran;
				params['cara_pakai-'+line]	= o.cara_pakai;
				params['jasa-'+line]=k.jasa
				params['no_out-'+line]=k.no_out
				params['no_urut-'+line]=k.no_urut
				params['kd_milik-'+line]=k.kd_milik
				params['no_racik-'+line]=o.kd_prd;
				params['aturan_racik-'+line]=o.aturan_racik;
				params['aturan_pakai-'+line]=o.aturan_pakai;
				params['jumlah_racik-' +line]=o.jml;
				params['satuan_racik-' +line]=o.kd_satuan;
				params['catatan_racik-' +line]=k.catatan_racik;
				line++;
			}
		}*/
	}
    return params
};

function getParamBayarResepRWI() 
{
	
	var KdCust='';
	var TmpCustoLama='';
	var tmpNonResep='';
	var tmpNamaPasien='';
	// if(Ext.get('txtTmpKdCustomerApotekResepRWIL').getValue()=='Perseorangan'){
		// KdCust=Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').getValue();
	// }else if(Ext.get('txtTmpKdCustomerApotekResepRWIL').getValue()=='Perusahaan'){
		// KdCust=Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').getValue();
	// }else {
		// KdCust=Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').getValue();
	// }
	
	var ubah=0;
	if(Ext.getCmp('txtNoResepApotekResepRWIL').getValue() === '' || Ext.getCmp('txtNoResepApotekResepRWIL').getValue()=='Nomor Resep'){
		ubah=0;
	} else{
		ubah=1;
	}
	
	
	//Pembayaran
	var LangsungPost = 0;
	var tmpNoresep='';
	if(Ext.getCmp('txtNoResepApotekResepRWIL').getValue() === '' || Ext.getCmp('txtNoResepApotekResepRWIL').getValue() === 'No Resep' ){
		LangsungPost = 1;
		tmpNoresep='';
	}else {
		LangsungPost = 0;
		tmpNoresep = Ext.getCmp('txtNoResepRWI_Pembayaran').getValue();
	}
	
	var kd_pay;
	if(Ext.getCmp('cboPembayaranRWI').getValue() == 'TUNAI'){
		kd_pay='TU';
	} else{
		kd_pay=Ext.getCmp('cboPembayaranRWI').getValue();
	}

	var params =
	{
		NmPasien:Ext.getCmp('txtNamaPasienResepRWI_viApotekResepRWI').getValue(),
		KdUnit: Ext.getCmp('txtTmpKdUnitApotekResepRWIL').getValue(),
		KdDokter:Ext.getCmp('txtTmpKdDokterApotekResepRWIL').getValue(),
		//NonResep:tmpNonResep,
		JamOut:jam,
		DiscountAll:toInteger(Ext.getCmp('txtTotalDiscResepRWIL').getValue()),
		// AdmRacikAll:0,//toInteger(Ext.getCmp('txtAdmRacikResepRWIL').getValue()),
		AdmRacikAll:toInteger(Ext.getCmp('txtAdmRacikResepRWIL').getValue()),
		// JasaTuslahAll:toInteger(Ext.getCmp('txtTuslahEmbalaseL').getValue()),
		JasaTuslahAll:toInteger(jumlah_tuslah),
		Adm:0,//toInteger(Ext.getCmp('txtAdmResepRWIL').getValue()),
		Admprsh:0,//toInteger(Ext.getCmp('txtAdmPrshResepRWIL').getValue()),
		Kdcustomer:Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').getValue(),
		NoTransaksiAsal:ResepRWI.vars.no_transaksi,
		KdKasirAsal:ResepRWI.vars.kd_kasir,
		SubTotal:toInteger(Ext.getCmp('txtJumlahTotalResepRWIL').getValue()),
		Total:toInteger(Ext.getCmp('txtTotalBayarResepRWIL').getValue()),
		NoOut:Ext.getCmp('txtTmpNooutResepRWIL').getValue(),
		TglOut:Ext.getCmp('txtTmpTgloutResepRWIL').getValue(),
		
		//PembayaranKdPasienBayar:Ext.getCmp('txtkdPasien_Pembayaran').getValue(),
		KdPasien:Ext.getCmp('txtkdPasien_PembayaranRWI').getValue(),
		KdPay:Ext.getCmp('txtTmpKdPayResepRWIL').getValue(),
		JumlahTotal:toInteger(Ext.getCmp('txtTotalResepRWI_Pembayaran').getValue()),
		JumlahTerimaUang:toInteger(Ext.getCmp('txtBayarResepRWI_Pembayaran').getValue()),
		NoResep:tmpNoresep,
		TanggalBayar:Ext.getCmp('dftanggalResepRWI_Pembayaran').getValue(),
		NoKamar:Ext.getCmp('txtNoKamarApotekResepRWIL').getValue(),
		JumlahItem:Ext.getCmp('txtTmpTotQtyResepRWIL').getValue(),
		Posting:LangsungPost,
		Shift: tampungshiftsekarang,
		Tanggal:Ext.getCmp('txtTmpTglResepSebenarnyaResepRWIL').getValue(),
		Ubah:ubah
	};
	
	params['jumlah']=dsDataGrdJab_viApotekResepRWI.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viApotekResepRWI.getCount();i++)
	{
		params['cito-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.C;
		params['kd_prd-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_prd
		params['nama_obat-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.nama_obat
		params['kd_satuan-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_satuan
		params['harga_jual-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.harga_jual
		params['harga_beli-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.harga_beli
		params['kd_pabrik-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_pabrik
		params['jml-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.jml
		params['markup-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.markup
		params['disc-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.disc
		params['racik-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.racik
		params['dosis-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.dosis
		params['jasa-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.jasa
		params['no_out-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.no_out
		params['no_urut-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.no_urut
		params['kd_milik-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_milik
	}
	
    return params
};


function getParamTransferResepRWI(){
	var params =
	{
		Kdcustomer:Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').getValue(),
		NoTransaksi:ResepRWI.vars.no_transaksi,
		TglTransaksi:ResepRWI.vars.tgl_transaksi,
		KdKasir:ResepRWI.vars.kd_kasir,
		KdSpesial:kd_spesial_tr,
		KdUnitKamar:kd_unit_kamar_tr,
		NoKamar:no_kamar_tr,
		NoOut:Ext.getCmp('txtTmpNooutResepRWIL').getValue(),
		TglOut:Ext.getCmp('txtTmpTgloutResepRWIL').getValue(),
		KdUnitAsal:Ext.getCmp('txtTmpKdUnitApotekResepRWIL').getValue(),
		NoResep:Ext.getCmp('txtNoResepApotekResepRWIL').getValue(),
		
		//PembayaranKdPasienBayar:Ext.getCmp('txtkdPasien_Pembayaran').getValue(),
		KdPasien:Ext.getCmp('txtkdPasienTransfer_ResepRWI').getValue(),
		
		JumlahTotal:toInteger(Ext.getCmp('txtTotalBayarTransfer_ResepRWI').getValue()),
		JumlahTerimaUang:toInteger(Ext.getCmp('txtTotalTransfer_ResepRWI').getValue()),
		TanggalBayar:Ext.getCmp('dftanggalTransfer_ResepRWI').getValue(),
		JumlahItem:Ext.getCmp('txtTmpTotQtyResepRWIL').getValue(),
		Shift: tampungshiftsekarang,
		Tanggal:tanggal
	};
	
	params['jumlah']=dsDataGrdJab_viApotekResepRWI.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viApotekResepRWI.getCount();i++)
	{
		params['kd_prd-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_prd
		params['jml-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.jml
		params['no_urut-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.no_urut
		params['kd_milik-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_milik
	}
	
    return params
}

function getParamInsertMrObatRWI(){
	var params =
	{
		kd_pasien: Ext.getCmp('cboKodePasienResepRWI').getValue(),
		kd_unit:   Ext.getCmp('txtTmpKdUnitApotekResepRWIL').getValue(),
		tgl_masuk: ResepRWI.vars.tgl_transaksi,
		urut_masuk: ResepRWI.vars.urut_masuk,
		tgl_out:Ext.getCmp('txtTmpTgloutResepRWIL').getValue(),
		no_out:Ext.getCmp('txtTmpNooutResepRWIL').getValue(),
	}
	
	params['jumlah']=dsDataGrdJab_viApotekResepRWI.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viApotekResepRWI.getCount();i++)
	{
		params['kd_prd-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_prd
		params['kd_satuan-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_satuan
		params['jml-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.jml
	}
	
	return params
}


function getParamUnpostingResepRWI() 
{
	var params =
	{
		NoResep:Ext.getCmp('txtNoResepApotekResepRWIL').getValue(),
		NoOut:Ext.getCmp('txtTmpNooutResepRWIL').getValue(),
		TglOut:Ext.getCmp('txtTmpTgloutResepRWIL').getValue()
	}
	
	params['jumlah']=dsDataGrdJab_viApotekResepRWI.getCount();
	// for(var i = 0 ; i < dsDataGrdJab_viApotekResepRWI.getCount();i++)
	// {
		// params['cito-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.C;
		// params['kd_prd-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_prd
		// params['nama_obat-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.nama_obat
		// params['kd_satuan-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_satuan
		// params['harga_jual-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.harga_jual
		// params['harga_beli-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.harga_beli
		// params['kd_pabrik-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_pabrik
		// params['jml-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.jml
		// params['markup-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.markup
		// params['disc-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.disc
		// params['racik-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.racik
		// params['dosis-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.dosis
		// params['no_urut-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.no_urut
		// params['kd_milik-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_milik
	// }
	// params['jumlah']=dsDataGrdJab_viApotekResepRWJ.getCount();
	var line=0;
	for(var i = 0 ; i < dsDataGrdJab_viApotekResepRWI.getCount();i++){
		var o=dsDataGrdJab_viApotekResepRWI.data.items[i].data;
		// if(o.racikan ==false){
			params['racik-'+line]=0;
			if(dsDataGrdJab_viApotekResepRWI.data.items[i].data.cito == 'Ya'){
				params['cito-'+line]=1;
				params['hargaaslicito-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.hargaaslicito;
				params['nilai_cito-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.nilai_cito;
			} else{
				params['cito-'+line]=0;
				params['hargaaslicito-'+line]=0;
				params['nilai_cito-'+line]=0;
			}
			params['kd_prd-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_prd
			params['nama_obat-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.nama_obat
			params['kd_satuan-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_satuan
			params['harga_jual-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.harga_jual
			params['harga_beli-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.harga_beli
			params['kd_pabrik-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_pabrik
			params['jml-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.jml
			params['signa-'+line]= o.signa;
			params['takaran-'+line]= o.takaran;
			params['cara_pakai-'+line]	= o.cara_pakai;
			params['markup-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.markup
			params['disc-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.disc
			params['dosis-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.dosis
			params['jasa-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.jasa
			params['no_out-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.no_out
			params['no_urut-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.no_urut
			params['kd_milik-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_milik
			params['no_racik-'+line]=0;
			params['aturan_racik-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.aturan_racik
			params['aturan_pakai-'+line]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.aturan_pakai
			params['jumlah_racik-' +line]=0;
			params['satuan_racik-' +line]='';
			params['catatan_racik-' +line]='';
			line++;
		/*}else{
			console.log(o);
			var parse=JSON.parse(o.result);
			params.jumlah=(params.jumlah-1)+parse.length;
			for(var j=0,jLen=parse.length; j<jLen;j++){
				var k=parse[j];
				params['racik-'+line]=1;
				if(k.cito == 'Ya'){
					params['cito-'+line]=1;
					params['hargaaslicito-'+line]=k.hargaaslicito;
					params['nilai_cito-'+line]=k.nilai_cito;
				} else{
					params['cito-'+line]=0;
					params['hargaaslicito-'+line]=0;
					params['nilai_cito-'+line]=0;
				}
				params['kd_prd-'+line]=k.kd_prd;
				params['nama_obat-'+line]=k.nama_obat;
				params['kd_satuan-'+line]=k.kd_satuan;
				params['harga_jual-'+line]=k.harga_jual;
				params['harga_beli-'+line]=k.harga_beli;
				params['kd_pabrik-'+line]=k.kd_pabrik;
				params['jml-'+line]=k.jml;
				params['markup-'+line]=k.markup;
				params['disc-'+line]=k.disc;
				params['signa-'+line]= o.signa;
				params['takaran-'+line]= o.takaran;
				params['cara_pakai-'+line]	= o.cara_pakai;
				params['jasa-'+line]=k.jasa
				params['no_out-'+line]=k.no_out
				params['no_urut-'+line]=k.no_urut
				params['kd_milik-'+line]=k.kd_milik
				params['no_racik-'+line]=o.kd_prd;
				params['aturan_racik-'+line]=o.aturan_racik;
				params['aturan_pakai-'+line]=o.aturan_pakai;
				params['jumlah_racik-' +line]=o.jml;
				params['satuan_racik-' +line]=o.kd_satuan;
				params['catatan_racik-' +line]=k.catatan_racik;
				line++;
			}
		}*/
	}
    return params
}

function getParamUnPostingMrObatRWI(){
	var params =
	{
		kd_pasien: kd_pasien_obbt_rwi,
		kd_unit:   kd_unit_obbt_rwi,
		tgl_masuk: tgl_masuk_obbt_rwi,
		urut_masuk: urut_masuk_obbt_rwi,
		tgl_out:Ext.getCmp('txtTmpTgloutResepRWIL').getValue(),
		no_out:Ext.getCmp('txtTmpNooutResepRWIL').getValue(),
	}
	
	params['jumlah']=dsDataGrdJab_viApotekResepRWI.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viApotekResepRWI.getCount();i++)
	{
		params['kd_prd-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_prd
		params['kd_satuan-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_satuan
		params['jml-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.jml
	}
	
	return params
}

function getParamDeleteHistoryResepRWI(urut) {
	if(urut== undefined){
		urut=gridDTLTRHistoryApotekRWI.getSelectionModel().selection.cell[0];
	}
	var o = dsTRDetailHistoryBayar.getRange()[urut].data;
	var params ={
		NoOut:Ext.getCmp('txtTmpNooutResepRWIL').getValue(),
		TglOut:Ext.getCmp('txtTmpTgloutResepRWIL').getValue(),
		urut:o.URUT,
		kd_pay:o.KD_PAY
	}
	
	
	
    return params
}

function getParamCloseOrder(){
	var params =
	{
		kd_pasien: kd_pasien_obbt_rwi,
		kd_unit:   kd_unit_obbt_rwi,
		tgl_masuk: tgl_masuk_obbt_rwi,
		urut_masuk: urut_masuk_obbt_rwi,
	};
	
	params['jumlah']=dsDataGrdJab_viApotekResepRWI.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viApotekResepRWI.getCount();i++)
	{
		params['kd_prd-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_prd
		params['no_urut-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.no_urut
	}
	
    return params
}

function getParamCekDilayani(id_mrresep){
	//params['jumlah']=dsDataGrdJab_viApotekResepRWI.getCount();
	var urutdtl='';
	var kdprddtl='';
	for(var i = 0 ; i < dsDataGrdJab_viApotekResepRWI.getCount();i++)
	{
		urutdtl += dsDataGrdJab_viApotekResepRWI.data.items[i].data.no_urut + ",";
		kdprddtl += "'" + dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_prd + "',";
	}
	
	
	var params =
	{
		id_mrresep: id_mrresep,
		urut:urutdtl,
		kd_prd:kdprddtl
	};
	
	
	
    return params
}


function datacetakbill(){
	var unit="";
	if(Ext.get('cbo_UnitResepRWIL').getValue() == '' || Ext.get('cbo_UnitResepRWIL').getValue() == 'Unit'){
		unit = "-";
	} else{
		unit = Ext.get('cbo_UnitResepRWIL').getValue();
	}
	
	var kamar="";
	if(Ext.getCmp('txtKamarApotekResepRWIL').getValue() == ''){
		kamar = "-";
	} else{
		kamar = Ext.get('txtKamarApotekResepRWIL').getValue();
	}
	
	
	var params =
	{
		Table: 'billprintingreseprwi',
		NoResep:Ext.getCmp('txtNoResepApotekResepRWIL').getValue(),
		NoOut:Ext.getCmp('txtTmpNooutResepRWIL').getValue(),
		TglOut:Ext.getCmp('txtTmpTgloutResepRWIL').getValue(),
		KdPasien:Ext.getCmp('txtKdPasienResepRWI_viApotekResepRWI').getValue(),
		NamaPasien:Ext.getCmp('txtNamaPasienResepRWI_viApotekResepRWI').getValue(),
		JenisPasien:Ext.get('cboPilihankelompokPasienAptResepRWI').getValue(),
		Kelas:unit,
		Kamar:kamar,
		// KdSpesial:tmp_kd_spesial,
		KdSpesial:kd_spesial_tr,
		NoSEP:Ext.getCmp('txtNoSep_viResepRWI').getValue(),
		// AdmRacik:0,//Ext.getCmp('txtAdmRacikResepRWIL').getValue(),
		AdmRacik:Ext.getCmp('txtAdmRacikResepRWIL').getValue(),
		//Adm:Ext.getCmp('txtAdmEditData_viApotekResepRWJ').getValue(),
		Shift:tampungshiftsekarang,
		Dokter:Ext.get('cbo_DokterApotekResepRWI').getValue(),
		Total:Ext.get('txtTotalBayarResepRWIL').getValue(),
		Tot:toInteger(Ext.get('txtTotalBayarResepRWIL').getValue()),
		// printer:Ext.getCmp('cbopasienorder_printer_rwi').getValue(),
		kd_form:KdFormResepRWI
		
	}
	params['jumlah']=dsDataGrdJab_viApotekResepRWI.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viApotekResepRWI.getCount();i++)
	{
		params['kd_prd-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.kd_prd;
		params['nama_obat-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.nama_obat;
		params['jml-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.jml;
		params['harga_sat-'+i]=dsDataGrdJab_viApotekResepRWI.data.items[i].data.harga_jual;
	}
	
    return params
}

function getCriteriaCariApotekResepRWI()
{
      	 var strKriteria = "";

           if (Ext.get('TxtFilterGridDataView_RoNumber_viApotekResepRWI').getValue() != "" && Ext.get('TxtFilterGridDataView_RoNumber_viApotekResepRWI').getValue()!=='No. Resep')
            {
                strKriteria = " o.no_resep " + "LIKE upper('%" + Ext.get('TxtFilterGridDataView_RoNumber_viApotekResepRWI').getValue() +"%')";
            }
            
            if (Ext.get('txtKdNamaPasienResepRWI').getValue() != "" && Ext.get('txtKdNamaPasienResepRWI').getValue() !== 'Kode/Nama Pasien')//^^^
            {
				if(Ext.get('txtKdNamaPasienResepRWI').getValue().substring(0,1)==='0'){
					if (strKriteria == "")
                    {
                        strKriteria = " o.kd_pasienapt " + "LIKE lower('" + Ext.get('txtKdNamaPasienResepRWI').getValue() +"%')" ;
                    }
                    else {
						strKriteria += " o.kd_pasienapt " + "LIKE lower('" + Ext.get('txtKdNamaPasienResepRWI').getValue() +"%')";
					}
				} else {
					 if (strKriteria == "")
                    {
                        strKriteria = " lower(o.nmpasien) " + "LIKE lower('" + Ext.get('txtKdNamaPasienResepRWI').getValue() +"%')" ;
                    }
                    else {
						strKriteria += " lower(o.nmpasien) " + "LIKE lower('" + Ext.get('txtKdNamaPasienResepRWI').getValue() +"%')";
					}
				}
            }
			if (Ext.get('cboStatusPostingApotekResepRWI').getValue() != "")
            {
				if (Ext.get('cboStatusPostingApotekResepRWI').getValue()==='Belum Posting')
				{
					if (strKriteria == "")
                    {
                        strKriteria = " o.tutup " + "= 0" ;
                    }
                    else {
						strKriteria += " and o.tutup " + "= 0";
				    }
				}
				if (Ext.get('cboStatusPostingApotekResepRWI').getValue()==='Posting')
				{
					if (strKriteria == "")
                    {
                        strKriteria = " o.tutup " + "=1" ;
                    }
                    else {
						strKriteria += " and o.tutup " + "=1";
				    }
				}
				if (Ext.get('cboStatusPostingApotekResepRWI').getValue()==='Semua')
				{
					
				}
                 
            }
			if (Ext.get('dfTglAwalApotekResepRWI').getValue() != "" && Ext.get('dfTglAkhirApotekResepRWI').getValue() != "")
            {
                if (strKriteria == "")
				{
					strKriteria = " o.tgl_out between '" + Ext.get('dfTglAwalApotekResepRWI').getValue() + "' and '" + Ext.get('dfTglAkhirApotekResepRWI').getValue() + "'" ;
				}
				else {
					strKriteria += " and o.tgl_out between '" + Ext.get('dfTglAwalApotekResepRWI').getValue() + "' and '" + Ext.get('dfTglAkhirApotekResepRWI').getValue() + "'" ;
				}
                
            }
			
			if (Ext.get('cbo_UnitResepRWI').getValue() != "" && Ext.get('cbo_UnitResepRWI').getValue() != 'Unit/Ruang')
            {
				if (strKriteria == "")
				{
					strKriteria = " o.kd_unit ='" + Ext.getCmp('cbo_UnitResepRWI').getValue() + "'"  ;
				}
				else {
					strKriteria += " and o.kd_unit ='" + Ext.getCmp('cbo_UnitResepRWI').getValue() + "'";
			    }
                
            }
	
		strKriteria= strKriteria + " and left(u.parent,3)='100' and o.returapt=0 and o.kd_unit_far='" + UnitFarAktif_ResepRWI + "' order by o.tgl_out "
	 return strKriteria;
}


function ValidasiEntryResepRWI(modul,mBolHapus)
{
	var x = 1;
	if(Ext.getCmp('txtNamaPasienResepRWI_viApotekResepRWI').getValue() === '' || dsDataGrdJab_viApotekResepRWI.getCount() === 0 || Ext.getCmp('txtKdPasienResepRWI_viApotekResepRWI').getValue() === ''){
		if(Ext.getCmp('txtNamaPasienResepRWI_viApotekResepRWI').getValue() === ''){
			ShowPesanWarningResepRWI('Nama Pasien', 'Warning');
			x = 0;
		} else if(dsDataGrdJab_viApotekResepRWI.getCount() == 0){
			ShowPesanWarningResepRWI('Daftar resep obat belum di isi', 'Warning');
			x = 0;
		} else {
			ShowPesanWarningResepRWI('Kode Pasien', 'Warning');
			x = 0;
		} 
	}
	
	if((Ext.getCmp('txtJumlahTotalResepRWIL').getValue() === '0' || Ext.getCmp('txtJumlahTotalResepRWIL').getValue() === 0) ||
		(Ext.getCmp('txtTotalBayarResepRWIL').getValue() === '0' || Ext.getCmp('txtTotalBayarResepRWIL').getValue() === 0)){
		if(Ext.getCmp('txtJumlahTotalResepRWIL').getValue() === '0' || Ext.getCmp('txtJumlahTotalResepRWIL').getValue() === 0){
			ShowPesanWarningResepRWI('Total harga obat kosong, harap periksa kelengkapan pengisian obat', 'Warning');
			x = 0;
		} else{
			ShowPesanWarningResepRWI('Total bayar kosong, harap periksa kelengkapan pengisian obat', 'Warning');
			x = 0;
		}
	}
	
	/* if(Ext.getCmp('txtCatatanResepRWI_viResepRWI').getValue() < 1){
		ShowPesanWarningResepRWI('Jumlah resep tidak boleh kurang dari 1!.', 'Warning');
		x = 0;
	}  */
	
	
	for(var i=0; i<dsDataGrdJab_viApotekResepRWI.getCount() ; i++){
		var o=dsDataGrdJab_viApotekResepRWI.getRange()[i].data;
		if(o.kd_prd == undefined || o.kd_prd == ""){
			// ShowPesanWarningResepRWI('Obat masih kosong, harap isi baris kosong atau hapus untuk melanjutkan', 'Warning');
			// x = 0;
			/* var line = ResepRWI.form.Grid.a.getSelectionModel().selection.cell[0];
			*/
			dsDataGrdJab_viApotekResepRWI.removeAt(i);
			ResepRWI.form.Grid.a.getView().refresh(); 
		} else{
			if(o.jml == undefined || o.jumlah == undefined){
				if(o.jml == undefined){
					ShowPesanWarningResepRWI('Qty belum di isi, qty tidak boleh kosong', 'Warning');
					x = 0;
				}else if(o.jumlah == undefined){
					ShowPesanWarningResepRWI('Total obat kosong, harap periksa qty dan kelengkapan pengisian obat', 'Warning');
					x = 0;
				}
			}
			// if(parseFloat(o.jml) > parseFloat(o.jml_stok_apt)){
				// ShowPesanWarningResepRWI('Qty melebihi stok', 'Warning');
				// o.jml=o.jml_stok_apt;
				// x = 0;
			// }

		}
		
		for(var j=0; j<dsDataGrdJab_viApotekResepRWI.getCount() ; j++){
			var p=dsDataGrdJab_viApotekResepRWI.getRange()[j].data;
			var kd_prd_milik1 = o.kd_prd+''+o.kd_milik;
			var kd_prd_milik2 = p.kd_prd+''+p.kd_milik;
			console.log(kd_prd_milik1);
			console.log(kd_prd_milik2);
			// if(i != j && o.kd_prd == p.kd_prd){
			if(i != j && kd_prd_milik1 == kd_prd_milik2){
				// ShowPesanWarningResepRWI('Item obat '+ p.nama_obat +' untuk kepemilikan obat : '+ p.milik +' sudah diInput. Proses tidak dapat dilajutkan!', 'Warning');
				ShowPesanWarningResepRWI('Item obat '+ p.nama_obat +' untuk kepemilikan obat : '+ p.milik +' sudah diInput. Proses tidak dapat dilajutkan!', 'Warning');
				x = 0;
				break;
			}
			
		}
	}
	
	return x;
};

function ValidasiBayarResepRWI(modul,mBolHapus)
{
	var x = 1;
	if(Ext.getCmp('cboJenisByrResepRWI').getValue() === '' ){
		ShowPesanWarningResepRWI('Pembayaran belum dipilih', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('cboPembayaranRWI').getValue() === '' || Ext.getCmp('cboPembayaranRWI').getValue() === 'Pilih Pembayaran...'){
		ShowPesanWarningResepRWI('Jenis pembayaran belum dipilih', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('txtBayarResepRWI_Pembayaran').getValue() === ''){
		ShowPesanWarningResepRWI('Jumlah pembayaran belum di isi', 'Warning');
		x = 0;
	}
	
	return x;
};


function ValidasiTransferResepRWI(modul,mBolHapus)
{
	var x = 1;
	if(Ext.getCmp('txtNoTransaksiTransfer_ResepRWI').getValue() === '' || Ext.getCmp('txtNoTransaksiTransfer_ResepRWI').getValue() === 'No Transaksi'){
		ShowPesanWarningResepRWI('No transaksi kosong, harap periksa kembali', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('dftanggalTransaksi_ResepRWI').getValue() === '' ){
		ShowPesanWarningResepRWI('Tanggal transaksi kosong, harap periksa kembali', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('txtNoResepTransfer_ResepRWI').getValue() === '' || Ext.getCmp('txtNoResepTransfer_ResepRWI').getValue() === 'No Resep'){
		ShowPesanWarningResepRWI('No. resep tidak boleh kosong', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('dftanggalTransfer_ResepRWI').getValue() === ''){
		ShowPesanWarningResepRWI('Tanggal tidak boleh kosong', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('txtkdPasienTransfer_ResepRWI').getValue() === ''){
		ShowPesanWarningResepRWI('Kode pasien tidak boleh kosong', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('txtTotalTransfer_ResepRWI').getValue() === ''){
		ShowPesanWarningResepRWI('Jumlah transfer tidak boleh kosong', 'Warning');
		x = 0;
	}
	
	return x;
};

function ShowPesanWarningResepRWI(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:320
		}
	);
};

function ShowPesanErrorResepRWI(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:290
		}
	);
};


function ShowPesanInfoResepRWI(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:350
		}
	);
};

function load_data_printer_resep_rwi()
{
	var kriteriaPrint_ResepRWI;
	if(PrintBillResepRWI == 'true'){
		kriteriaPrint_ResepRWI='apt_printer_bill_'+UnitFarAktif_ResepRWI;
	} else{
		kriteriaPrint_ResepRWI='apt_printer_kwitansi_'+UnitFarAktif_ResepRWI;
	}

	Ext.Ajax.request(
	{
		url: baseURL + "index.php/apotek/functionAPOTEKrwi/group_printer",
		params:{
			kriteria: kriteriaPrint_ResepRWI
		} ,
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
		success: function(o) {
			cbopasienorder_printer_rwi.store.removeAll();
				var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsprinter_rwi.recordType;
				var o=cst['listData'][i];
				
				recs.push(new recType(o));
				dsprinter_rwi.add(recs);
				console.log(o);
			}
		}
	});
}


function mCombo_printer_rwi()
{ 
 
	var Field = ['alamat_printer'];
    dsprinter_rwi = new WebApp.DataStore({ fields: Field });
	load_data_printer_resep_rwi();
	cbopasienorder_printer_rwi= new Ext.form.ComboBox
	(
		{
			id: 'cbopasienorder_printer_rwi',
			typeAhead		: true,
			triggerAction	: 'all',
			lazyRender		: true,
			mode			: 'local',
			emptyText: '',
			fieldLabel:  'Daftar Printer',
			align: 'Right',
			anchor:'100%',
			store: dsprinter_rwi,
			valueField: 'alamat_printer',
			displayField: 'alamat_printer',
			listeners:
			{
				
			}
		}
	);return cbopasienorder_printer_rwi;
};

function loadDataKodePasienResepRWI(param){
	if (param==='' || param===undefined) {
		param={
			text: '0',
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/functionAPOTEKrwi/getKodePasienResepRWI",
		params: {
			text:param,
		},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			dsKodePasien_ResepRWI.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsKodePasien_ResepRWI.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsKodePasien_ResepRWI.add(recs);
				console.log(o);
			}
		}
	});
};

function mComboKodePasienResepRWI(){
	var Field = ['kd_kasir','no_transaksi','no_kamar','kd_pasien','nama',
					'nama_keluarga','jenis_kelamin','nama_unit','kd_unit',
					'nama_kamar', 'kd_dokter', 'kd_customer','kd_spesial',
					'kd_unit_kamar','urut_masuk','nama_dokter','nama_unit'];
	dsKodePasien_ResepRWI = new WebApp.DataStore({fields: Field});
	loadDataKodePasienResepRWI();
	cboKodePasienResepRWI = new Ext.form.ComboBox
	(
		{
			id: 'cboKodePasienResepRWI',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			hideTrigger		: true,
			store: dsKodePasien_ResepRWI,
			valueField: 'kd_pasien',
			displayField: 'kd_pasien',
			emptyText: 'No. Medrec',
			width:130,
			listeners:
			{
				'select': function(a, b, c)
				{
					Ext.getCmp('cbo_DokterApotekResepRWI').setValue(b.data.kd_dokter);
					Ext.getCmp('cboKodePasienResepRWI').setValue(b.data.kd_pasien);
					Ext.getCmp('txtNamaPasienResepRWI_viApotekResepRWI').setValue(b.data.nama);
					Ext.getCmp('cbo_UnitResepRWIL').setValue(b.data.nama_unit);
					Ext.getCmp('cboPilihankelompokPasienAptResepRWI').setValue(b.data.kd_customer);
					Ext.getCmp('txtNoKamarApotekResepRWIL').setValue(b.data.no_kamar);
					Ext.getCmp('txtKamarApotekResepRWIL').setValue(b.data.nama_kamar);
					ResepRWI.vars.no_transaksi=b.data.no_transaksi;
					ResepRWI.vars.tgl_transaksi=b.data.tgl_transaksi;
					ResepRWI.vars.kd_kasir=b.data.kd_kasir;
					ResepRWI.vars.urut_masuk=b.data.urut_masuk;
					Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').setValue(b.data.kd_customer);
					Ext.getCmp('txtTmpKdDokterApotekResepRWIL').setValue(b.data.kd_dokter);
					Ext.getCmp('txtTmpKdUnitApotekResepRWIL').setValue(b.data.kd_unit);
					Ext.getCmp('btnAddObatRWI').enable();
					Ext.getCmp('btnSimpan_viApotekResepRWI').enable();
					Ext.getCmp('btnSimpanExit_viApotekResepRWI').enable();
					Ext.getCmp('btnDelete_viApotekResepRWI').enable();
					
					var records = new Array();
					records.push(new dsDataGrdJab_viApotekResepRWI.recordType());
					dsDataGrdJab_viApotekResepRWI.add(records);
					row=dsDataGrdJab_viApotekResepRWI.getCount()-1; // menujukan jumlah baris,startEditing(baris dimulai dari 1, kolom 1)
					ResepRWI.form.Grid.a.startEditing(row, 4);	
					Ext.getCmp('btnDelete_viApotekResepRWI').enable();
					Ext.getCmp('btnSimpan_viApotekResepRWI').enable();
					Ext.getCmp('btnSimpanExit_viApotekResepRWI').enable();
					Ext.getCmp('btnBayar_viApotekResepRWI').disable();
					Ext.getCmp('btnDelete_viApotekResepRWI').enable();
					
				},
				keyUp: function(a,b,c){
					
					if(  b.getKey()!=127 ){
						clearTimeout(this.time);
				
						this.time=setTimeout(function(){
							if(cboKodePasienResepRWI.lastQuery != '' ){
								var value="";
								
								if (value!=cboKodePasienResepRWI.lastQuery)
								{
									if (a.rendered && a.innerList != null) {
										a.innerList.update(a.loadingText ? '<div class="loading-indicator">' + a.loadingText + '</div>' : '');
										a.restrictHeight();
										a.selectedIndex = 0;
									}
									a.expand();
									Ext.Ajax.request({
										url: baseURL + "index.php/apotek/functionAPOTEKrwi/getKodePasienResepRWI",
										params: {
											text:cboKodePasienResepRWI.lastQuery,
										},
										failure: function(o){
											var cst = Ext.decode(o.responseText);
										},	    
										success: function(o) {
											cboKodePasienResepRWI.store.removeAll();
											var cst = Ext.decode(o.responseText);

											for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
												var recs    = [],recType = dsKodePasien_ResepRWI.recordType;
												var o=cst['listData'][i];
										
												recs.push(new recType(o));
												dsKodePasien_ResepRWI.add(recs);
											}
											a.expand();
											if(dsKodePasien_ResepRWI.onShowList != undefined)
												dsKodePasien_ResepRWI.onShowList(cst[showVar]);
											if(cst['listData'].length>0){
													
												a.doQuery(a.allQuery, true);
												a.expand();
												a.selectText(value.length,value.length);
											}else{
											//	if (a.rendered && a.innerList != null) {
													a.innerList.update(a.loadingText ? '&nbsp; Data Tidak Ada' : '');
													a.restrictHeight();
													a.selectedIndex = 0;
												//}
											}
										}
									});
									value=cboKodePasienResepRWI.lastQuery;
								}
							}
						},1000);
					}
				} 
			}
		}
	)
	return cboKodePasienResepRWI;
};

function panelnew_window_printer_resepRWI()
{
    win_printer = new Ext.Window
    (
        {
            id: 'win_printer',
            title: 'Printer',
            closeAction: 'destroy',
            width:320,
            height: 120,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [Itempanel_printer_ResepRWI()],
			fbar:[
				{
					xtype: 'button',
					text: 'OK',
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnOkCetakBill_ResepRWI',
					handler: function()
					{		
						if (Ext.getCmp('cbopasienorder_printer_rwi').getValue()===""){
							ShowPesanWarningResepRWI('Pilih dulu print sebelum cetak', 'Warning');
						}else{
							if(PrintBillResepRWI == 'true'){
								printbill_ResepRWI();
								win_printer.close();
							} else{
								printkwitansi_resep_rwi();
								win_printer.close();
								panelWindowPrintKwitansi_ResepRWI.close();
							}
							
						} 
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					style:{'margin-left':'5px','margin-top':'0px'},
					id: 'btnCancelLapRWJPasswordDulu',
					handler: function()
					{
						win_printer.close();
					}
				},
			]

        }
    );

    win_printer.show();
};

function Itempanel_printer_ResepRWI()
{
    var panel_printer = new Ext.Panel
    (
        {
            id: 'panel_printer',
            fileUpload: true,
            layout: 'form',
            width:170,
            height: 120,
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                mCombo_printer_rwi(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                   // style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '100%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
						
                    ]
                }
            ]
        }
    );

    return panel_printer;
};

function panelPrintKwitansi_resepRWI()
{
    panelWindowPrintKwitansi_ResepRWI = new Ext.Window
    (
        {
            id: 'panelWindowPrintKwitansi_ResepRWI',
            title: 'Print Kwitansi',
           // closeAction: 'destroy',
            width:440,
            height: 280,
            border: false,
            resizable:false,
            plain: true,
           // layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [Itempanel_PrintKwitansiResepRWJ()],
			fbar:[
				{
					xtype: 'button',
					text: 'OK',
					width: 70,
					hideLabel: true,
					id: 'btnOkPrintKwitansi_ResepRWI',
					handler: function()
					{
						/* var params={
							kd_pasien:Ext.getCmp('txtKd_pasienPrintKwitansi_viApotekResepRWI').getValue(),
							pembayar:Ext.getCmp('txtNamaPembayarPrintKwitansi_viApotekResepRWI').getValue(),
							jumlah_bayar:Ext.getCmp('txtJumlahBayarPrintKwitansi_viApotekResepRWI').getValue(),
							nama:ResepRWI.form.ComboBox.namaPasien.getValue(),
							keterangan_bayar:Ext.getCmp('txtKeteranganPembayaranPrintKwitansi_viApotekResepRWI').getValue(),
						} 
						var form = document.createElement("form");
						form.setAttribute("method", "post");
						form.setAttribute("target", "_blank");
						form.setAttribute("action", baseURL + "index.php/apotek/cetakKwitansi/cetak");
						var hiddenField = document.createElement("input");
						hiddenField.setAttribute("type", "hidden");
						hiddenField.setAttribute("name", "data");
						hiddenField.setAttribute("value", Ext.encode(params));
						form.appendChild(hiddenField);
						document.body.appendChild(form);
						form.submit();		
						panelWindowPrintKwitansi_ResepRWI.close(); */
						PrintBillResepRWI='false';
						// panelnew_window_printer_resepRWI();
						printkwitansi_resep_rwi();
						panelWindowPrintKwitansi_ResepRWI.close();
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelPrintKwitansi_ResepRWI',
					handler: function()
					{
						panelWindowPrintKwitansi_ResepRWI.close();
					}
				} 
			]

        }
    );

    panelWindowPrintKwitansi_ResepRWI.show();
	getDataPrintKwitansiResepRWI();
};

function Itempanel_PrintKwitansiResepRWJ()
{
    var items=
    (
        {
            id: 'panelItemPrintKwitansiRWJ',
			layout:'form',
			border: true,
			bodyStyle:'padding: 5px',
			height: 215,
			items:
			[
				{
					layout: 'absolute',
					bodyStyle: 'padding: 5px ',
					border: true,
					//width: 300,
					//height: 200,
					anchor: '100% 100%',
					items:
					[
						{
							x: 10,
							y: 10,
							xtype: 'label',
							text: 'No. Medrec'
						},
						{
							x: 140,
							y: 10,
							xtype: 'label',
							text: ':'
						},
						{
							x: 150,
							y: 10,
							xtype: 'textfield',
							id: 'txtKd_pasienPrintKwitansi_viApotekResepRWI',
							name: 'txtKd_pasienPrintKwitansi_viApotekResepRWI',
							width: 80,
							readOnly: true
						},
						{
							x: 10,
							y: 40,
							xtype: 'label',
							text: 'Nama Pembayar'
						},
						{
							x: 140,
							y: 40,
							xtype: 'label',
							text: ':'
						},
						{
							x: 150,
							y: 40,
							xtype: 'textfield',
							id: 'txtNamaPembayarPrintKwitansi_viApotekResepRWI',
							name: 'txtNamaPembayarPrintKwitansi_viApotekResepRWI',
							width: 180,
						},
						{
							x: 10,
							y: 70,
							xtype: 'label',
							text: 'Untuk Pembayaran'
						},
						{
							x: 140,
							y: 70,
							xtype: 'label',
							text: ':'
						},
						{
							x: 150,
							y: 70,
							xtype: 'textarea',
							id: 'txtKeteranganPembayaranPrintKwitansi_viApotekResepRWI',
							name: 'txtKeteranganPembayaranPrintKwitansi_viApotekResepRWI',
							width: 250,
							height: 62,
						},
						{
							x: 10,
							y: 140,
							xtype: 'label',
							text: 'No. Resep'
						},
						{
							x: 140,
							y: 140,
							xtype: 'label',
							text: ':'
						},
						{
							x: 150,
							y: 140,
							xtype: 'textfield',
							id: 'txtNoResepPrintKwitansi_viApotekResepRWI',
							name: 'txtNoResepPrintKwitansi_viApotekResepRWI',
							width: 150,
							listeners:{
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										Ext.getCmp('btnOkPrintKwitansi_ResepRWI').el.dom.click();
										panelWindowPrintKwitansi_ResepRWI.close();	
										Ext.getCmp('txtCatatanResepRWI_viResepRWI').focus(false,10);
									}
								}
							}
						},
						{
							x: 10,
							y: 170,
							xtype: 'label',
							text: 'Jumlah Bayar (Rp)',
							style:{'font-weight':'bold'},
						},
						{
							x: 140,
							y: 170,
							xtype: 'label',
							text: ':',
							style:{'font-weight':'bold'},
						},
						{
							x: 150,
							y: 170,
							xtype: 'textfield',
							id: 'txtJumlahBayarPrintKwitansi_viApotekResepRWI',
							name: 'txtJumlahBayarPrintKwitansi_viApotekResepRWI',
							width: 150,
							style:{'text-align':'right','font-weight':'bold'},
							readOnly:true
						},
					]
				}
				
			]	
        }
    );

    return items;
};

function getDataPrintKwitansiResepRWI(){
	Ext.getCmp('txtKd_pasienPrintKwitansi_viApotekResepRWI').setValue(Ext.getCmp('txtKdPasienResepRWI_viApotekResepRWI').getValue());
	Ext.getCmp('txtNamaPembayarPrintKwitansi_viApotekResepRWI').setValue(Ext.getCmp('txtNamaPasienResepRWI_viApotekResepRWI').getValue());
	Ext.getCmp('txtNoResepPrintKwitansi_viApotekResepRWI').setValue(Ext.getCmp('txtNoResepApotekResepRWIL').getValue());
	Ext.getCmp('txtJumlahBayarPrintKwitansi_viApotekResepRWI').setValue(Ext.getCmp('txtTotalBayarResepRWIL').getValue());
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEK/getTemplateKwitansi",
			params: {query:''},
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorResepRWI('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					Ext.getCmp('txtKeteranganPembayaranPrintKwitansi_viApotekResepRWI').setValue(cst.template +" di "+cst.nm_unit_far );
					
				}
				else 
				{
					loadMask.hide();
					ShowPesanErrorResepRWI('Gagal membaca template kwitansi', 'Error');
				};
				Ext.getCmp('txtNoResepPrintKwitansi_viApotekResepRWI').focus();// .focus();
					
			}
		}
		
	)
}

function dataaddnew_viApotekResepRWI() 
{
	// Ext.getCmp('txtNoTrApotekResepRWIL').setValue('');
	getDefaultCustomer();
	nonaktivRWI(false);
	Ext.getCmp('btn1/2resep_viApotekResepRWI').enable();
	ResepRWI.form.Grid.a.enable();
	Ext.getCmp('txtNoSep_viResepRWI').setValue('');
	Ext.getCmp('txtNoTlp_viResepRWI').setValue('');
	Ext.getCmp('txtNoResepApotekResepRWIL').setValue('');
	Ext.getCmp('txtTmpKdUnitApotekResepRWIL').setValue('');
	Ext.getCmp('txtTmpKdDokterApotekResepRWIL').setValue('');
	Ext.getCmp('txtTmpNooutResepRWIL').setValue('');
	Ext.getCmp('txtTmpTgloutResepRWIL').setValue('');
	Ext.getCmp('txtTmpTglResepSebenarnyaResepRWIL').setValue(tanggal_resep_sebernarnyaRWI);
	Ext.getCmp('txtTmpStatusPostResepRWIL').setValue(0);
	Ext.getCmp('txtTmpTotQtyResepRWIL').setValue('');
	Ext.getCmp('txtTmpKdPayResepRWIL').setValue('');
	Ext.getCmp('txtTmpJenisPayResepRWIL').setValue('');
	Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').setValue("");
	Ext.getCmp('txtTmpSisaAngsuranResepRWIL').setValue("");
	Ext.getCmp('txtNoKamarApotekResepRWIL').setValue('');
	Ext.getCmp('cboPilihankelompokPasienAptResepRWI').setValue('');
	Ext.getCmp('cbo_DokterApotekResepRWI').setValue('');
	Ext.getCmp('cbo_UnitResepRWIL').setValue('');
	Ext.getCmp('txtKamarApotekResepRWIL').setValue('');
	Ext.getCmp('txtTuslahEmbalaseL').setValue(0);
	Ext.getCmp('txtAdmResepRWIL').setValue(0);
	Ext.getCmp('txtAdmPrshResepRWIL').setValue(0);
	Ext.getCmp('txtAdmRacikResepRWIL').setValue(0);
	Ext.getCmp('txtTotalDiscResepRWIL').setValue(0);
	Ext.getCmp('txtJumlahTotalResepRWIL').setValue(0);
	Ext.getCmp('txtTotalBayarResepRWIL').setValue(0);
	Ext.getCmp('dfTglResepSebenarnyaResepRWI').setValue(now_viApotekResepRWI);
	
	Ext.getCmp('txtKdPasienResepRWI_viApotekResepRWI').focus();
	Ext.getCmp('txtNamaPasienResepRWI_viApotekResepRWI').setValue('');
	Ext.getCmp('txtKdPasienResepRWI_viApotekResepRWI').setValue('');
	ResepRWI.vars.no_transaksi="";
	ResepRWI.vars.kd_kasir="";
	ResepRWI.vars.tgl_transaksi="";
	ResepRWI.vars.urut_masuk="";
	ResepRWI.vars.payment="";
	ResepRWI.vars.payment_type="";
	kd_pasien_obbt_rwi="";
	kd_unit_obbt_rwi="";
	tgl_masuk_obbt_rwi="";
	urut_masuk_obbt_rwi="";
	kd_spesial_tr="";
	FocusExitResepRWI= "";
	PencarianLookupResepRWI = "";
	currentRowSelectionPencarianObatResepRWI = "";
	
	Ext.getCmp('btnSimpan_viApotekResepRWI').disable();
	Ext.getCmp('btnSimpanExit_viApotekResepRWI').disable();
	Ext.getCmp('btnunposting_viApotekResepRWI').disable();
	Ext.getCmp('btnPrint_viResepRWI').disable();
	Ext.getCmp('btnBayar_viApotekResepRWI').disable();
	Ext.getCmp('btnDeleteHistory_viApotekResepRWI').disable();
	Ext.getCmp('btnDelete_viApotekResepRWI').disable();
	Ext.getCmp('btnAddObatRWI').enable();
	dsTRDetailHistoryBayar.removeAll();
	dsDataGrdJab_viApotekResepRWI.removeAll();
	// ResepRWI.form.Panel.a.update('<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>');
	ResepRWI.form.Panel.a.html = '<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>';
	Ext.getCmp('btnSimpan_viApotekResepRWI').enable();
	Ext.getCmp('btnBayar_viApotekResepRWI').enable();
	Ext.getCmp('btnTransfer_viApotekResepRWI').enable();
	
}

function getUnitFar_ResepRWI(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEKrwi/getUnitFar",
			params: {query:''},
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorResepRWI('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					UnitFarAktif_ResepRWI = cst.kd_unit_far;
				}
				else 
				{
					loadMask.hide();
					ShowPesanErrorResepRWI('Gagal membaca unitfar', 'Error');
				};
			}
		}
		
	)
	
}

function hitungSetengahResepRWI(){
	var total=0;
	var admRacik=0;
	var totdisc=0;
	var totalall=0;
	var admprs=0;
	var adm=0;
	var totqty=0;
	
	for(var i=0; i<dsDataGrdJab_viApotekResepRWI.getCount() ; i++){
		var jumlahGrid=0;
		var qtygrid=0;
		var subJumlah=0;
		var sisa=0;
		
		
	
		var o=dsDataGrdJab_viApotekResepRWI.getRange()[i].data;
		if(o.jml != undefined || o.jml != ""){
			// Jika qty tidak melebihi stok tersedia
			// if(parseFloat(o.jml) <= parseFloat(o.jml_stok_apt)){
				// Cek qty ganjil atau tidak
				if((parseInt(o.jml) % 2) == 1){
					sisa = parseInt(o.jml) - 1; //Dikurangi 1 supaya genap
					qtygrid = parseInt(sisa) / 2; //Setelah dikurang lalu di bagi 2
					qtygrid = qtygrid + 1; //Setelah dibagi 2 lalu di tambahkan 1 dari pengurangan menjadi genap sebelumnya
					o.jml = qtygrid;
				} else{
					qtygrid = parseInt(o.jml) / 2;
					o.jml = qtygrid;
				}
				
				if(o.racik != undefined || o.racik != ""){
					if(isNaN(admRacik)){
						admRacik +=0;
					} else {
						if(o.racik == 'Ya'){
							admRacik += parseFloat(o.adm_racik) * 1;
						} else{
							admRacik +=0;
						}
						
					}
				}
				
				jumlahGrid = ((parseFloat(o.jml) * parseFloat(o.harga_jual)) - parseFloat(o.disc));
				totdisc += parseFloat(o.disc);
				o.jumlah =jumlahGrid;
				total +=jumlahGrid;
			// }else {
				// ShowPesanWarningResepRWI('Jumlah obat melebihi stok yang tersedia','Warning');
				// o.jml=o.jml_stok_apt;
			// }
			totqty +=parseFloat(o.jml);
			
		}
	}
	admRacik=parseFloat(Ext.getCmp('txtAdmRacikResepRWIL').getValue());
	// admRacik = 0;
	Ext.get('txtTmpTotQtyResepRWIL').dom.value=totqty;
	total=aptpembulatan(total);
	Ext.getCmp('txtJumlahTotalResepRWIL').setValue(total);
	Ext.getCmp('txtAdmRacikResepRWIL').setValue(toFormat(admRacik));
	Ext.get('txtTotalDiscResepRWIL').dom.value=toFormat(totdisc);
	// admprs=toInteger(total)*parseFloat(Ext.getCmp('txtAdmResepRWIL').getValue());
	admprs=0;
	Ext.getCmp('txtAdmPrshResepRWIL').setValue(toFormat(admprs));
	totalall +=toInteger(total) + admRacik + parseInt(Ext.get('txtTuslahEmbalaseL').getValue()) + parseFloat(Ext.get('txtAdmResepRWIL').getValue()) + admprs - totdisc;
	totalall=aptpembulatan(totalall);
	Ext.getCmp('txtTotalBayarResepRWIL').setValue(toFormat(totalall));
	ResepRWI.form.Grid.a.getView().refresh();
}

function formulaRacikanResepRWI(){
	statusRacikan_ResepRWI=0;
	var lebar = 450;
	var stmpnoOut=0;
	var stmptgl;
    setLookUpRacikan_ResepRWI = new Ext.Window
    ({
        id: 'setLookUpRacikan_ResepRWI',
		name: 'setLookUpRacikan_ResepRWI',
        title: 'Formulasi Racikan Obat', 
        closeAction: 'destroy',        
        width: 523,
        height: 260,
        resizable:false,
		autoScroll: false,
        border: true,
        constrain : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: [ 
			PanelFormulasiRacikan_ResepRWI()
			/* getItemPanelBiodataTransfer_viApotekResepRWI(),
			getItemPanelTotalBayar_ApotekResepRWI() */
		],
		fbar:[
			{
				xtype:'button',
				text:'OK',
				width:70,
				hideLabel:true,
				id: 'btnOKRacikan_viApotekResepRWIL',
				handler:function()
				{
					setHasilFormula();
				}   
			},
			{
				xtype:'button',
				text:'Batal',
				width:70,
				hideLabel:true,
				id: 'btnBatalRacikan_viApotekResepRWIL',
				handler:function()
				{
					setLookUpRacikan_ResepRWI.close();
				}   
			}
		],
        listeners:
        {
            activate: function()
            {
				Ext.getCmp('txtPersediaanObatRacikan_viApotekResepRWI').focus();
            },
            afterShow: function()
            {
                this.activate();
				Ext.getCmp('txtPersediaanObatRacikan_viApotekResepRWI').focus();
				// ;
            },
            deactivate: function()
            {
             //   rowSelected_viApotekResepRWI=undefined;
            }
        }
    });

    setLookUpRacikan_ResepRWI.show();
	Ext.getCmp('txtPersediaanObatRacikan_viApotekResepRWI').focus(true,10);
	Ext.getCmp('txtkodeObatRacikan_viApotekResepRWI').setValue(currentKdPrdRacik);
	Ext.getCmp('txtNamaObatRacikan_viApotekResepRWI').setValue(currentNamaObatRacik);
	Ext.getCmp('txtaHargaObatRacikan_viApotekResepRWI').setValue(currentHargaRacik);
	Ext.getCmp('txtQtyResepRacikanRacikan_viApotekResepRWI').setValue(currentJumlah);
}

function PanelFormulasiRacikan_ResepRWI(){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		height: 195,
		items:
		[
			/* {
				layout: 'column',
				border: false,
				items:
				[ */
					{
						
						layout: 'absolute',
						bodyStyle: 'padding: 10px ',
						border: true,
						width: 495,
						height: 40,
						anchor: '100% 23%',
						items:
						[
							
							{
								x: 10,
								y: 10,
								xtype: 'label',
								text: 'Nama obat'
							},
							{
								x: 70,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 80,
								y: 10,
								xtype: 'textfield',
								id: 'txtkodeObatRacikan_viApotekResepRWI',
								name: 'txtkodeObatRacikan_viApotekResepRWI',
								width: 70,
								readOnly: true
							},
							{
								x: 155,
								y: 10,
								xtype: 'textfield',
								id: 'txtNamaObatRacikan_viApotekResepRWI',
								name: 'txtNamaObatRacikan_viApotekResepRWI',
								width: 160,
								readOnly: true
							},
							{
								x: 328,
								y: 10,
								xtype: 'label',
								text: 'Harga satuan'
							},
							{
								x: 400,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 410,
								y: 10,
								xtype: 'numberfield',
								id: 'txtaHargaObatRacikan_viApotekResepRWI',
								name: 'txtaHargaObatRacikan_viApotekResepRWI',
								style:{'text-align':'right'},
								width: 75,
								readOnly: true
							},
						]
					},
					{
						
						layout: 'absolute',
						bodyStyle: 'padding: 5px ',
						border: false,
						width: 495,
						height: 1,
						items:
						[
							
							{
								xtype: 'label',
								text: ''
							},
						]
					},
					{
						
						layout: 'absolute',
						bodyStyle: 'padding: 5px ',
						border: true,
						width: 500,
						height: 50,
						anchor: '100% 72%',
						items:
						[
							
							{
								x: 10,
								y: 10,
								xtype: 'label',
								text: 'Dosis persediaan obat farmasi'
							},
							{
								x: 170,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 180,
								y: 10,
								xtype: 'numberfield',
								id: 'txtPersediaanObatRacikan_viApotekResepRWI',
								name: 'txtPersediaanObatRacikan_viApotekResepRWI',
								style:{'text-align':'right'},
								width: 75,
								listeners: {
									specialkey: function(){
										if(Ext.EventObject.getKey()==13){
											Ext.getCmp('txtDosisPermintaanDokterRacikan_viApotekResepRWI').focus();
										}
									}
								}
							},
							{
								x: 10,
								y: 40,
								xtype: 'label',
								text: 'Dosis resep / permintaan dokter'
							},
							{
								x: 170,
								y: 40,
								xtype: 'label',
								text: ':'
							},
							{
								x: 180,
								y: 40,
								xtype: 'numberfield',
								id: 'txtDosisPermintaanDokterRacikan_viApotekResepRWI',
								name: 'txtDosisPermintaanDokterRacikan_viApotekResepRWI',
								style:{'text-align':'right'},
								width: 75,
								listeners: {
									specialkey: function(){
										if(Ext.EventObject.getKey()==13){
											Ext.getCmp('txtQtyResepRacikanRacikan_viApotekResepRWI').focus();
										}
									}
								}
							},
							
							{
								x: 10,
								y: 70,
								xtype: 'label',
								text: 'Quantity resep racikan'
							},
							{
								x: 170,
								y: 70,
								xtype: 'label',
								text: ':'
							},
							{
								x: 180,
								y: 70,
								xtype: 'numberfield',
								id: 'txtQtyResepRacikanRacikan_viApotekResepRWI',
								name: 'txtQtyResepRacikanRacikan_viApotekResepRWI',
								style:{'text-align':'right'},
								width: 75,
								listeners: {
									specialkey: function(){
										if(Ext.EventObject.getKey()==13){
											HitungRacikanObat();
										}
									}
								}
							},
							{
								x: 10,
								y: 100,
								xtype: 'label',
								text: 'Quantity obat yang dikeluarkan'
							},
							{
								x: 170,
								y: 100,
								xtype: 'label',
								text: ':'
							},
							{
								x: 180,
								y: 100,
								xtype: 'numberfield',
								id: 'txtQtyObatDiKeluarkanRacikan_viApotekResepRWI',
								name: 'txtQtyObatDiKeluarkanRacikan_viApotekResepRWI',
								style:{'text-align':'right'},
								width: 75,
								readOnly: true
							},
							{
								x: 328,
								y: 10,
								xtype: 'label',
								text: 'Sub Total'
							},
							{
								x: 400,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 410,
								y: 10,
								xtype: 'numberfield',
								id: 'txtSubTOtalRacikan_viApotekResepRWI',
								name: 'txtSubTOtalRacikan_viApotekResepRWI',
								style:{'text-align':'right'},
								width: 75,
								readOnly: true
							},
							{
								x: 260,
								y: 75,
								xtype: 'label',
								text: '*) Enter untuk menghitung'
							},
						]
					}
			/* 	]
			}  */
							
		]		
	};
        return items;
}

function HitungRacikanObat(){
	var totqty=0;
	var totharga=0;
	Ext.getCmp('txtPersediaanObatRacikan_viApotekResepRWI').getValue();
	Ext.getCmp('txtDosisPermintaanDokterRacikan_viApotekResepRWI').getValue();
	Ext.getCmp('txtQtyResepRacikanRacikan_viApotekResepRWI').getValue();
	Ext.getCmp('txtQtyObatDiKeluarkanRacikan_viApotekResepRWI').getValue();
	Ext.getCmp('txtSubTOtalRacikan_viApotekResepRWI').getValue();
	
	totqtyawal = (parseFloat(Ext.getCmp('txtDosisPermintaanDokterRacikan_viApotekResepRWI').getValue()) / parseFloat(Ext.getCmp('txtPersediaanObatRacikan_viApotekResepRWI').getValue())) * parseFloat(Ext.getCmp('txtQtyResepRacikanRacikan_viApotekResepRWI').getValue());
	totqty = Math.ceil(totqtyawal).toFixed(0);
	Ext.getCmp('txtQtyObatDiKeluarkanRacikan_viApotekResepRWI').setValue(totqty);
	totharga = parseFloat(Ext.getCmp('txtQtyObatDiKeluarkanRacikan_viApotekResepRWI').getValue()) * parseFloat(Ext.getCmp('txtaHargaObatRacikan_viApotekResepRWI').getValue());
	Ext.getCmp('txtSubTOtalRacikan_viApotekResepRWI').setValue(totharga);
	setHasilFormula();
}

function setHasilFormula(){
	statusRacikan_ResepRWI=0;
	var o = dsDataGrdJab_viApotekResepRWI.getRange()[curentIndexsSelection].data;
	
	o.jml = Ext.getCmp('txtQtyObatDiKeluarkanRacikan_viApotekResepRWI').getValue();
	o.jumlah = Ext.getCmp('txtSubTOtalRacikan_viApotekResepRWI').getValue();
	o.racik = "Ya";
	ResepRWI.form.Grid.a.getView().refresh();
	// ResepRWI.form.Grid.a.startEditing(curentIndexsSelection, 10);
	hasilJumlahResepRWI();
	setLookUpRacikan_ResepRWI.close();
	/* var records = new Array();
	records.push(new dsDataGrdJab_viApotekResepRWI.recordType());
	dsDataGrdJab_viApotekResepRWI.add(records);
	var nextRow = dsDataGrdJab_viApotekResepRWI.getCount()-1; 
	ResepRWI.form.Grid.a.startEditing(nextRow, 4); */
	var nextRow = dsDataGrdJab_viApotekResepRWI.getCount()-1; 
	ResepRWI.form.Grid.a.startEditing(nextRow, 7);
	
}

function getParamTanggal(tgl)
{
	var param;
	var tglPisah=tgl.split("-");
	var tglAsli;
	if (tglPisah[1]==="Jan")
	{
		tglAsli=tglPisah[0]+"-"+"01"+"-"+tglPisah[2];
	}
	else if (tglPisah[1]==="Feb")
	{
		tglAsli=tglPisah[0]+"-"+"02"+"-"+tglPisah[2];
	}
	else if (tglPisah[1]==="Mar")
	{
		tglAsli=tglPisah[0]+"-"+"03"+"-"+tglPisah[2];
	}
	else if (tglPisah[1]==="Apr")
	{
		tglAsli=tglPisah[0]+"-"+"04"+"-"+tglPisah[2];
	}
	else if (tglPisah[1]==="May")
	{
		tglAsli=tglPisah[0]+"-"+"05"+"-"+tglPisah[2];
	}
	else if (tglPisah[1]==="Jun")
	{
		tglAsli=tglPisah[0]+"-"+"06"+"-"+tglPisah[2];
	}
	else if (tglPisah[1]==="Jul")
	{
		tglAsli=tglPisah[0]+"-"+"07"+"-"+tglPisah[2];
	}
	else if (tglPisah[1]==="Aug")
	{
		tglAsli=tglPisah[0]+"-"+"08"+"-"+tglPisah[2];
	}
	else if (tglPisah[1]==="Sep")
	{
		tglAsli=tglPisah[0]+"-"+"09"+"-"+tglPisah[2];
	}
	else if (tglPisah[1]==="Oct")
	{
		tglAsli=tglPisah[0]+"-"+"10"+"-"+tglPisah[2];
	}
	else if (tglPisah[1]==="Nov")
	{
		tglAsli=tglPisah[0]+"-"+"11"+"-"+tglPisah[2];
	}
	else if (tglPisah[1]==="Dec")
	{
		tglAsli=tglPisah[0]+"-"+"12"+"-"+tglPisah[2];
	}
	else
	{
		tglAsli=tgl;
	}
	return tglAsli;
}

function formulacitoResepRWI(){
	var lebar = 450;
	var stmpnoOut=0;
	var stmptgl;
    setLookUpCito_ResepRWI = new Ext.Window
    ({
        id: 'setLookUpCito_ResepRWI',
		name: 'setLookUpCito_ResepRWI',
        title: 'Formulasi Racikan Obat', 
        closeAction: 'destroy',        
        width: 543,
        height: 310,
        resizable:false,
		autoScroll: false,
        border: true,
        constrain : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: [ 
			PanelFormulasiCito_ResepRWI()
		],
		fbar:[
			{
				xtype:'button',
				text:'OK',
				width:70,
				hideLabel:true,
				id: 'btnOKCito_viApotekResepRWIL',
				handler:function()
				{
					var line =ResepRWI.form.Grid.a.getSelectionModel().selection.cell[0];
					var o = dsDataGrdJab_viApotekResepRWI.getRange()[line].data;
					o.harga_jual=currentCitoTarifBaruResepRWI;
					o.nilai_cito=Ext.getCmp('txtaPersentaseCito_viApotekResepRWI').getValue();
					o.hargaaslicito=Ext.getCmp('txtaHargaBeliCito_viApotekResepRWI').getValue();
					hasilJumlahResepRWI();
					setLookUpCito_ResepRWI.close();
				}   
			},
			{
				xtype:'button',
				text:'Batal',
				width:70,
				hideLabel:true,
				id: 'btnBatalRacikan_viApotekResepRWIL',
				handler:function()
				{
					setLookUpCito_ResepRWI.close();
				}   
			}
		],
        listeners:
        {
            activate: function()
            {
				
            },
            afterShow: function()
            {
                this.activate();
            },
            deactivate: function()
            {
				// shortcut.remove('lookupcito');
            },
			close: function (){
				// shortcut.remove('lookupcito');
			},
        }
    });

    setLookUpCito_ResepRWI.show();
	Ext.getCmp('txtProdukCito_viApotekResepRWI').setValue(currentCitoNamaObatResepRWI);
	Ext.getCmp('txtaHargaBeliCito_viApotekResepRWI').setValue(currentHargaJualObatResepRWI);
	Ext.getCmp('txtaPersentaseCito_viApotekResepRWI').setValue("50");
	// viewGridCito_ResepRWI();
	
	// shortcut.set({
		// code:'lookupcito',
		// list:[
			// {
				// key:'ctrl+d',
				// fn:function(){
					// Ext.getCmp('btnDeleteComponent_ResepRWI').el.dom.click();
				// }
			// },
			// {
				// key:'esc',
				// fn:function(){
					// setLookUpCito_ResepRWI.close();
				// }
			// }
		// ]
	// });
}

function PanelFormulasiCito_ResepRWI(){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		height: 245,
		items:
		[
			{
				
				layout: 'absolute',
				bodyStyle: 'padding: 10px ',
				border: true,
				width: 525,
				height: 110,
				anchor: '100% 40%',
				items:
				[
					
					{
						x: 10,
						y: 10,
						xtype: 'label',
						text: 'Produk'
					},
					{
						x: 110,
						y: 10,
						xtype: 'label',
						text: ':'
					},
					{
						x: 120,
						y: 10,
						xtype: 'textfield',
						id: 'txtProdukCito_viApotekResepRWI',
						name: 'txtProdukCito_viApotekResepRWI',
						width: 240,
						readOnly: true
					},
					{
						x: 368,
						y: 10,
						xtype: 'label',
						text: 'Harga beli'
					},
					{
						x: 420,
						y: 10,
						xtype: 'label',
						text: ':'
					},
					{
						x: 430,
						y: 10,
						xtype: 'numberfield',
						id: 'txtaHargaBeliCito_viApotekResepRWI',
						name: 'txtaHargaBeliCito_viApotekResepRWI',
						style:{'text-align':'right'},
						width: 75,
						readOnly: true
					},
					{
						x: 10,
						y: 40,
						xtype: 'label',
						text: 'Persentasi kenaikan'
					},
					{
						x: 110,
						y: 40,
						xtype: 'label',
						text: ':'
					},
					{
						x: 120,
						y: 40,
						xtype: 'numberfield',
						id: 'txtaPersentaseCito_viApotekResepRWI',
						name: 'txtaPersentaseCito_viApotekResepRWI',
						style:{'text-align':'right'},
						width: 75,
						listeners:
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									// if(currentCitoTarifLamaResepRWI==undefined || currentCitoTarifLamaResepRWI==''){
										// ShowPesanWarningResepRWI("Pilih komponent yang akan di cito!","Warning");
									// } else{	
										hitungpersentasecito_ResepRWI();								
										// var line	= gridPanelFormulasiCito_ResepRWI.getSelectionModel().selection.cell[0];
										// var o = dsDataGridFormulasiCito_ResepRWI.getRange()[line].data;										
										// o.tarif_baru=parseFloat(o.tarif_lama) + ((parseFloat(o.tarif_lama)*parseFloat(Ext.getCmp('txtaPersentaseCito_viApotekResepRWI').getValue()))/100);
										// currentCitoTarifBaruResepRWI=o.tarif_baru;
										// gridPanelFormulasiCito_ResepRWI.getView().refresh();
									// }
								}
							}
						}
					},
					{
						x: 200,
						y: 45,
						xtype: 'label',
						text: '%'
					},{
						x: 120,
						y: 63,
						xtype: 'label',
						style:{'font-size':'9px'},
						text: '*) enter untuk hitung'
					},
				]
			},
			gridFormulasiCito_ResepRWI()
							
		]		
	};
        return items;
}
function gridFormulasiCito_ResepRWI(){
    var FieldFormulasiCito_ResepRWJ = ['kd_prd'];
    dsDataGridFormulasiCito_ResepRWI= new WebApp.DataStore({
        fields: FieldFormulasiCito_ResepRWJ
    });
	var Fieldcomponent = ['kd_component','component','tarif_lama','tarif_baru'];
	dsgridcombocomponentcito_ResepRWI = new WebApp.DataStore({ fields: Fieldcomponent });
	
	loaddatagridcombocito_ResepRWI();
    
    gridPanelFormulasiCito_ResepRWI =new Ext.grid.EditorGridPanel({
        store: dsDataGridFormulasiCito_ResepRWI,
        height: 140,
        columnLines: true,
		selModel: new Ext.grid.CellSelectionModel ({
            singleSelect: true,
            listeners:{
                cellselect: function(sm, row, rec){
					cellSelectedGridCito_ResepRWI = undefined;
					cellSelectedGridCito_ResepRWI = dsDataGridFormulasiCito_ResepRWI.getAt(row);
					console.log(cellSelectedGridCito_ResepRWI.data)
					currentCitoTarifLamaResepRWI=cellSelectedGridCito_ResepRWI.data.tarif_lama;
                },
            }
        }),
		tbar:
		[
			{
				text	: 'Tambah component',
				id		: 'btnAddComponent',
				tooltip	: nmLookup,
				iconCls	: 'find',
				handler	: function(){
					var records = new Array();
					records.push(new dsDataGridFormulasiCito_ResepRWI.recordType());
					dsDataGridFormulasiCito_ResepRWI.add(records);
					var row =dsDataGridFormulasiCito_ResepRWI.getCount()-1;
					gridPanelFormulasiCito_ResepRWI.startEditing(row, 2);
				}
			},
			{
				text	: 'Delete',
				id		: 'btnDeleteComponent_ResepRWI',
				tooltip	: nmLookup,
				iconCls	: 'find',
				handler	: function(){
					var line = gridPanelFormulasiCito_ResepRWI.getSelectionModel().selection.cell[0];
					dsDataGridFormulasiCito_ResepRWI.removeAt(line);
					gridPanelFormulasiCito_ResepRWI.getView().refresh();
				}
			}
		],
        stripeRows: true,
        cm:new Ext.grid.ColumnModel([
        	new Ext.grid.RowNumberer(),	
			{			
				dataIndex: 'kd_component',
				header: 'kd_component',
				hidden: true,
				width: 50
			},
			// {			
				// dataIndex: 'component',
				// header: 'Komponent',
				// sortable: true,
				// width: 150
			// },
			{
				header: 'Komponent',
				dataIndex: 'component',
				hideable:false,
				menuDisabled: true,
				width: 90,
				editor		: gridcbocomponentcito_ResepRWI= new Ext.form.ComboBox({
						id				: 'gridcbocomponentcito_ResepRWI',
						typeAhead		: true,
						triggerAction	: 'all',
						lazyRender		: true,
						mode			: 'local',
						emptyText		: '',
						fieldLabel		: ' ',
						align			: 'Right',
						width			: 200,
						store			: dsgridcombocomponentcito_ResepRWI,
						valueField		: 'component',
						displayField	: 'component',
						listeners		:
						{
							select	: function(a,b,c){
								var line	= gridPanelFormulasiCito_ResepRWI.getSelectionModel().selection.cell[0];
								dsDataGridFormulasiCito_ResepRWI.getRange()[line].data.component=b.data.component;
								dsDataGridFormulasiCito_ResepRWI.getRange()[line].data.kd_component=b.data.kd_component;
								dsDataGridFormulasiCito_ResepRWI.getRange()[line].data.tarif_lama=b.data.tarif_lama;
								dsDataGridFormulasiCito_ResepRWI.getRange()[line].data.tarif_baru=b.data.tarif_baru;
								gridPanelFormulasiCito_ResepRWI.getView().refresh();
								
								var records = new Array();
								records.push(new dsDataGridFormulasiCito_ResepRWI.recordType());
								dsDataGridFormulasiCito_ResepRWI.add(records);
								var row =dsDataGridFormulasiCito_ResepRWI.getCount()-1;
								gridPanelFormulasiCito_ResepRWI.startEditing(row, 2);
							},
							keyUp: function(a,b,c){
								$this1=this;
								if((b.getKey()>=65 && b.getKey()<=90)|| (b.getKey()>=48 && b.getKey()<=57) || b.getKey()==32 || b.getKey()==8 || b.getKey()==46){
									clearTimeout(this.time);

									this.time=setTimeout(function(){
										if($this1.lastQuery != '' ){
											var value=$this1.lastQuery;
											var param={};
											param['text']=$this1.lastQuery;
											loaddatagridcombocito_ResepRWI(param);
										}
									},1000);
								}
							}
						}
					}
				)
			},
			{
				dataIndex: 'tarif_lama',
				header: 'Tarif Lama',
				xtype: 'numbercolumn',
				sortable: true,
				width: 50,
				align:'right'
			},
			{
				dataIndex: 'tarif_baru',
				header: 'Tarif Baru',
				xtype: 'numbercolumn',
				sortable: true,
				width: 50,
				align:'right'
			},
        ]),
		// plugins:chkSelected_viApotekResepRWI,
		viewConfig:{
			forceFit: true
		}
    });
    return gridPanelFormulasiCito_ResepRWI;
}
function viewGridCito_ResepRWI(){
	Ext.Ajax.request ({
		url: baseURL + "index.php/apotek/functionAPOTEKrwi/viewkomponentcito",
		params: {
			kd_unit:Ext.getCmp('txtTmpKdUnitApotekResepRWIL').getValue(),
			tarif:currentHargaJualObatResepRWI
		},
		failure: function(o)
		{
			ShowPesanErrorResepRWI('Hubungi Admin', 'Error');
		},	
		success: function(o) 
		{   
			dsDataGridFormulasiCito_ResepRWI.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsDataGridFormulasiCito_ResepRWI.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsDataGridFormulasiCito_ResepRWI.add(recs);
				gridPanelFormulasiCito_ResepRWI.getView().refresh();
			} else {
				ShowPesanErrorResepRWI('Gagal membaca Data ini', 'Error');
			};
		}
	});
}

function loaddatagridcombocito_ResepRWI(param)
{
	if (param==='' || param===undefined)
	{
		param={
			text: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/functionAPOTEKrwi/viewkomponentcito",
		params: {
			kd_unit:Ext.getCmp('txtTmpKdUnitApotekResepRWIL').getValue(),
			tarif:currentHargaJualObatResepRWI
		},
		failure: function(o)
		{
			var cst = Ext.decode(o.responseText);
		},
		success: function(o) {
			gridcbocomponentcito_ResepRWI.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['ListDataObj'].length; i<iLen; i++){
				var recs    = [],recType = dsgridcombocomponentcito_ResepRWI.recordType;
				var o=cst['ListDataObj'][i];

				recs.push(new recType(o));
				dsgridcombocomponentcito_ResepRWI.add(recs);
				//console.log(o);
			}
		}
	});
}

function hitungpersentasecito_ResepRWI(){
	for(var i=0; i < dsDataGridFormulasiCito_ResepRWI.getCount() ; i++){
		var tarifbaru=0;
		var o=dsDataGridFormulasiCito_ResepRWI.getRange()[i].data;
		if(o.component == '' || o.component == undefined){
			dsDataGridFormulasiCito_ResepRWI.removeAt(i);
		} else{
			if(o.tarif_lama != undefined || o.tarif_lama != ''){
				o.tarif_baru=parseFloat(o.tarif_lama) + ((parseFloat(o.tarif_lama)*parseFloat(Ext.getCmp('txtaPersentaseCito_viApotekResepRWI').getValue()))/100);
				currentCitoTarifBaruResepRWI += o.tarif_baru;
			} else{
				ShowPesanWarningResepRWI("Tarif masih kosong!","Warning");
			}
		}
	}
	gridPanelFormulasiCito_ResepRWI.getView().refresh(); 
}


function getTakaran(kd_jenis_etiket){
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/functionAPOTEKrwi/getTakaran",
		params: {kd_jenis_etiket:kd_jenis_etiket},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);
			Ext.getCmp('gridcbojenis_takaran_etiket').getStore().removeAll();
			// dsGridTakaranDosisObat_ResepRWJ.removeAll();
			// console.log(dsGridTakaranDosisObat_ResepRWJ.getRange());
			for(var i=0,iLen=cst['ListDataObj'].length; i<iLen; i++){
				var recs    = [],recType =   Ext.getCmp('gridcbojenis_takaran_etiket').getStore().recordType;
				var o=cst['ListDataObj'][i];
				recs.push(new recType(o));
				Ext.getCmp('gridcbojenis_takaran_etiket').getStore().add(recs);
				//alert('a');
				
			}
				console.log(dsGridTakaranDosisObat_ResepRWI.getRange());
				GridWaktuDosisObat_ResepRWI.getView().refresh();
				// Ext.getCmp('gridcbojenis_takaran_etiket').getView().refresh();
		}
	});
} 

function  getJamAturanObat(kd_waktu){
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/functionAPOTEK/getJamEtiket",
		params: {kd_waktu:kd_waktu},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);
			for(var i=0,iLen=cst['ListDataObj'].length; i<iLen; i++){
				var recs    = [],recType =   dsGridJamDosisObat_ResepRWI.recordType;
				var o=cst['ListDataObj'][i];
				recs.push(new recType(o));
				dsGridJamDosisObat_ResepRWI.add(recs);
				console.log(o);
			}
		}
	});
}

function getAturanObat(kd_jenis_etiket){
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/functionAPOTEKrwi/getAturanObat",
		params: {kd_jenis_etiket:kd_jenis_etiket},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);
			dsGridWaktuDosisObat_ResepRWI.removeAll();
			for(var i=0,iLen=cst['ListDataObj'].length; i<iLen; i++){
				var recs    = [],recType =   dsGridWaktuDosisObat_ResepRWI.recordType;
				var o=cst['ListDataObj'][i];
				recs.push(new recType(o));
				dsGridWaktuDosisObat_ResepRWI.add(recs);
				console.log(o);
				getJamAturanObat(o.kd_waktu);
				dsGridJamDosisObat_ResepRWI.removeAll();
			}
		}
	});
}

function mComboCaraMinum(){
	var Field = ['kd_aturan_minum','aturan_minum'];

	ds_cara_minum_etiket = new WebApp.DataStore({ fields: Field });
	getAturanMinumEtiket();
	
  var cboAturanMinumEtiket= new Ext.form.ComboBox
	(
		{
			id:'cboAturanMinumEtiket',
			x: 160,
			y: 10,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			width: 350,
			editable: false,
			emptyText:'',
			//fieldLabel: 'JENIS',
			tabIndex:5,
			store: ds_cara_minum_etiket,
			valueField: 'kd_aturan_minum',
			displayField: 'aturan_minum',
			value:'Sebelum Makan',
			listeners:
			{
				'select': function(a,b,c)
				{
					

				},
				'specialkey' : function()
				{
					if(Ext.EventObject.getKey() == 13 || Ext.EventObject.getKey() == 9){
						if(kd_jenis_etiket == 2){
							Ext.getCmp('txtCatatanListLabelObat').focus(true, 20);
						}else if(kd_jenis_etiket == 3){
							Ext.getCmp('txtKeteranganListLabelObat').focus(true, 20);
						}
						
					}					
				}
			}
		}
	);
	return cboAturanMinumEtiket;
}

shortcut.set({
	code:'main',
	list:[
		{
			key:'f1',
			fn:function(){
				Ext.getCmp('btnTambah_viApotekResepRWI').el.dom.click();
			}
		},
	]
})

function panelPrintLabelDosisObat_resepRWI(){
	panelWindowPrintLabelDosisObat_ResepRWI = new Ext.Window
    (
        {
            id: 'panelWindowPrintLabelDosisObat_ResepRWI',
            title: 'List Label Obat',
            width:600,
            height: 500,
            border: false,
            resizable:false,
            plain: true,
            iconCls: 'icon_lapor',
            modal: true,
            items: [
				gridListObatResepRWI(),
				Itempanel_PrintJenisEtiket(),
				gridWaktuDosisObatResepRWI(),
				Itempanel_PrintLabelDosisObatResepRWI()
			],
			fbar:[
				{
					xtype: 'button',
					text: 'OK',
					width: 70,
					hideLabel: true,
					id: 'btnOkPrintLabelDosisObat_ResepRWI',
					handler: function()
					{
						// CetakLabelObatApotekResepRWI(function(hasil){
							// var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
							// WinPrint.document.write(hasil);
							// WinPrint.document.close();
							// WinPrint.focus();
							// WinPrint.print();
							// WinPrint.close();
						// });
						cetak_label_obat();
					}
				},
				{
					xtype: 'button',
					text: 'Cancel' ,
					width: 70,
					hideLabel: true,
					id: 'btnCancelPrintLabelDosisObat_ResepRWI',
					handler: function()
					{
						panelWindowPrintLabelDosisObat_ResepRWI.close();
					}
				} 
			],
			listeners:
			{
				activate: function()
				{
						/*------------------ UDD ---------------------------*/
						Ext.getCmp('lblJenisHariListLabelObat').hide();
						Ext.getCmp('lblJamListLabelObat').hide();
						Ext.getCmp('lblTanggalListLabelObat').hide();
						Ext.getCmp('cboJenisHari').hide();
						Ext.getCmp('cboJam').hide();
						Ext.getCmp('dfTglListLabelObat').hide();
						Ext.getCmp('TD_TanggalListLabelObat').hide();
						
						/*------------------ TABLET ---------------------------*/
						Ext.getCmp('lblCaraMinumListLabelObat').hide();
						Ext.getCmp('cboAturanMinumEtiket').hide();
						Ext.getCmp('lblKeteranganListLabelObat').hide();
						Ext.getCmp('txtKeteranganListLabelObat').hide();
						Ext.getCmp('txtCatatanListLabelObat_SIRUP').hide();
						Ext.getCmp('lblCatatanListLabelObat_SIRUP').hide();
						Ext.getCmp('TD_CatatanListLabelObat_SIRUP').hide();
						/*------------------ OBAT LUAR ---------------------------*/
						
				},
				afterShow: function()
				{
					this.activate();

				},
				deactivate: function()
				{
					
				},
				close: function (){
					Ext.getCmp('txtCatatanResepRWI_viResepRWI').focus(false,10);
				}
			}

        }
    );

    panelWindowPrintLabelDosisObat_ResepRWI.show();
	getListObatDosisObat_ResepRWI();
}
function Itempanel_PrintJenisEtiket(){
	 var items=
    (
        {
            id: 'Itempanel_PrintJenisEtiket',
			layout:'form',
			border: true,
			bodyStyle:'padding: 2px',
			height: 50,
			items:
			[
				{
					layout: 'absolute',
					bodyStyle: 'padding: 2px ',
					border: true,
					//width: 300,
					//height: 200,
					anchor: '100% 100%',
					items:
					[
						{
							id:'lblJenisEtiketListLabelObat',
							x: 10,
							y: 10,
							xtype: 'label',
							text: 'Jenis Etiket'
						},
						{
							x: 140,
							y: 10,
							xtype: 'label',
							text: ':'
						},
						mComboJenisEtiket(),
						{
							x: 380,
							y: 15,
							xtype: 'checkbox',
							id: 'cek_jml_etiket_udd',
							hideLabel:false,
							boxLabel: '',
							checked: false,
							listeners: 
							{
								check: function()
								{
								   if(Ext.getCmp('cek_jml_etiket_udd').getValue()===true)
									{
										Ext.getCmp('cboQtyEtiket').enable();
									}
									else
									{
										Ext.getCmp('cboQtyEtiket').disable();
									}
								}
							}
						},
						mComboQtyEtiket()
					]
				}
				
			]	
        }
    );

    return items;
}

function mComboQtyEtiket(){
  var cboQtyEtiket= new Ext.form.ComboBox
	(
		{
			id:'cboQtyEtiket',
			x: 400,
			y: 10,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			width: 50,
			editable: false,
			emptyText:'',
			tabIndex:5,
			disabled:true,
			store:new Ext.data.ArrayStore({
						id: 0,
						fields:[
							'Id',
							'displayText'
						],
						data: [[1, 1],[2, 2], [3, 3], [4, 4], [5, 5]]
					}),
			valueField: 'Id',
			displayField: 'displayText',
			value:'1',
			listeners:
			{
				'select': function(a,b,c)
				{
					Ext.getCmp('cboJenisHari').focus(true, 20);

				},
				'specialkey' : function()
				{
					if(Ext.EventObject.getKey() == 13 || Ext.EventObject.getKey() == 9){
						Ext.getCmp('cboJenisHari').focus(true, 20);
					}				
				}
			}
		}
	);
	return cboQtyEtiket;
}
function getAturanMinumEtiket(){
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/functionAPOTEKrwi/getAturanMinumEtiket",
		params: {text:'0'},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['ListDataObj'].length; i<iLen; i++){
				var recs    = [],recType =   ds_cara_minum_etiket.recordType;
				var o=cst['ListDataObj'][i];
				recs.push(new recType(o));
				ds_cara_minum_etiket.add(recs);
				console.log(o);
			}
		}
	});
}

function mComboKeteranganObatLuar(){
	var Field = ['kd_ket','ket_obat_luar'];

	ds_KeteranganObatLuar_etiket = new WebApp.DataStore({ fields: Field });
	getKeteranganObatLuar();
	
  var cboKeteranganObatLuarEtiket= new Ext.form.ComboBox
	(
		{
			id:'cboKeteranganObatLuarEtiket',
			x: 160,
			y: 10,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			width: 350,
			editable: false,
			emptyText:'',
			//fieldLabel: 'JENIS',
			tabIndex:5,
			store: ds_KeteranganObatLuar_etiket,
			valueField: 'kd_ket',
			displayField: 'ket_obat_luar',
			value:'Obat Luar',
			listeners:
			{
				'select': function(a,b,c)
				{
					

				},
				'specialkey' : function()
				{
					if(Ext.EventObject.getKey() == 13 || Ext.EventObject.getKey() == 9){
						Ext.getCmp('txtCatatanListLabelObat').focus(true, 20);
					}				
				}
			}
		}
	);
	return cboKeteranganObatLuarEtiket;
}

function getKeteranganObatLuar(){
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/functionAPOTEKrwi/getKeteranganObatLuar",
		params: {text:'0'},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['ListDataObj'].length; i<iLen; i++){
				var recs    = [],recType =   ds_KeteranganObatLuar_etiket.recordType;
				var o=cst['ListDataObj'][i];
				recs.push(new recType(o));
				ds_KeteranganObatLuar_etiket.add(recs);
				console.log(o);
			}
		}
	});
}
function mComboJenisEtiket()
{
	var Field = ['id_etiket','jenis_etiket'];
	ds_jenis_etiket = new WebApp.DataStore({ fields: Field });
	getJenisEtiket();
	
	var fldDetail3 = ['kd_jenis_takaran','jenis_takaran'];
	dsGridTakaranDosisObat_ResepRWI = new WebApp.DataStore({ fields: fldDetail3 });
	console.log(dsGridTakaranDosisObat_ResepRWI);
	
	var cboJenisEtiket = new Ext.form.ComboBox
	(
		{
			id:'cboJenisEtiket',
			x: 160,
			y: 10,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			width: 200,
			editable: false,
			emptyText:'',
			//fieldLabel: 'JENIS',
			tabIndex:5,
			store: ds_jenis_etiket,
			valueField: 'id_etiket',
			displayField: 'jenis_etiket',
			value:'Etiket UDD',
			listeners:
			{
				'select': function(a,b,c)
				{
					kd_jenis_etiket = b.data.id_etiket;
					jenis_etiket =kd_jenis_etiket;
					dsGridTakaranDosisObat_ResepRWI.removeAll();
					getTakaran(kd_jenis_etiket);
					getAturanObat(kd_jenis_etiket);
					GridWaktuDosisObat_ResepRWI.getView().refresh();
					// alert ("a");
					//alert(b.data.id_etiket);
					if(b.data.id_etiket == 1 || b.data.id_etiket == 4 ){
						// GridWaktuDosisObat_ResepRWJ.hide();
						GridWaktuDosisObat_ResepRWI.setDisabled(true);
					}else{
						GridWaktuDosisObat_ResepRWI.setDisabled(false);
					}
					
					
					/*----------- Etiket UDD -------------*/
					if(kd_jenis_etiket == 1){
						Ext.getCmp('lblKeteranganObatLuarListLabelObat').hide();
						Ext.getCmp('cboKeteranganObatLuarEtiket').hide();
						Ext.getCmp('lblCatatanListLabelObat').hide();
						Ext.getCmp('txtCatatanListLabelObat').hide(); 
						Ext.getCmp('lblCaraPakaiListLabelObat').hide(); 
						Ext.getCmp('txtCaraPakaiListLabelObat').hide(); 
						Ext.getCmp('lblCaraMinumListLabelObat').hide();
						Ext.getCmp('cboAturanMinumEtiket').hide();
						Ext.getCmp('lblKeteranganListLabelObat').hide();
						Ext.getCmp('txtKeteranganListLabelObat').hide();
						Ext.getCmp('TD_TanggalListLabelObat').show();
						Ext.getCmp('TD_lblCaraMinumListLabelObat').show();
						Ext.getCmp('lblJenisHariListLabelObat').show();
						Ext.getCmp('cboJenisHari').show();
						Ext.getCmp('lblJamListLabelObat').show();
						Ext.getCmp('cboJam').show();
						Ext.getCmp('lblTanggalListLabelObat').show();
						Ext.getCmp('dfTglListLabelObat').show();
						jenis_obat='';
						Ext.getCmp('txtCatatanListLabelObat_SIRUP').hide();
						Ext.getCmp('lblCatatanListLabelObat_SIRUP').hide();
						Ext.getCmp('TD_CatatanListLabelObat_SIRUP').hide();
					}
					
					/*----------- Etiket TABLET -------------*/
					if(kd_jenis_etiket == 2){
						Ext.getCmp('lblKeteranganObatLuarListLabelObat').hide();
						Ext.getCmp('cboKeteranganObatLuarEtiket').hide();
						Ext.getCmp('lblCatatanListLabelObat').hide();
						Ext.getCmp('txtCatatanListLabelObat').hide(); 
						Ext.getCmp('lblCaraPakaiListLabelObat').hide(); 
						Ext.getCmp('txtCaraPakaiListLabelObat').hide(); 
						Ext.getCmp('lblJenisHariListLabelObat').hide();
						Ext.getCmp('cboJenisHari').hide();
						Ext.getCmp('lblJamListLabelObat').hide();
						Ext.getCmp('cboJam').hide();
						Ext.getCmp('lblTanggalListLabelObat').hide();
						Ext.getCmp('dfTglListLabelObat').hide();
						Ext.getCmp('TD_CaraPakaiListLabelObat').hide();
						Ext.getCmp('lblKeteranganListLabelObat').hide();
						Ext.getCmp('txtKeteranganListLabelObat').hide();
					
						Ext.getCmp('TD_lblCaraMinumListLabelObat').show();
						Ext.getCmp('TD_TanggalListLabelObat').hide();
						Ext.getCmp('lblCaraMinumListLabelObat').show();
						Ext.getCmp('cboAturanMinumEtiket').show();
						Ext.getCmp('lblCatatanListLabelObat').show();
						Ext.getCmp('txtCatatanListLabelObat').show();
						jenis_obat='tab';
						Ext.getCmp('gridcbojenis_takaran_etiket').setValue(jenis_obat);
						// Ext.get('gridcbojenis_takaran_etiket').dom.value='tab';
						// alert(Ext.getCmp('gridcbojenis_takaran_etiket').getValue());
						Ext.getCmp('txtCatatanListLabelObat_SIRUP').hide();
						Ext.getCmp('lblCatatanListLabelObat_SIRUP').hide();
						Ext.getCmp('TD_CatatanListLabelObat_SIRUP').hide();
					}
					
						/*----------- Etiket Sirup --------------*/
					if(kd_jenis_etiket == 3){
						Ext.getCmp('lblKeteranganObatLuarListLabelObat').hide();
						Ext.getCmp('cboKeteranganObatLuarEtiket').hide();
						Ext.getCmp('lblCatatanListLabelObat').hide();
						Ext.getCmp('txtCatatanListLabelObat').hide(); 
						Ext.getCmp('lblCaraPakaiListLabelObat').hide(); 
						Ext.getCmp('txtCaraPakaiListLabelObat').hide(); 
						Ext.getCmp('lblJenisHariListLabelObat').hide();
						Ext.getCmp('cboJenisHari').hide();
						Ext.getCmp('lblJamListLabelObat').hide();
						Ext.getCmp('cboJam').hide();
						Ext.getCmp('lblTanggalListLabelObat').hide();
						Ext.getCmp('dfTglListLabelObat').hide();
						Ext.getCmp('TD_CaraPakaiListLabelObat').hide();
						Ext.getCmp('TD_TanggalListLabelObat').hide();
						Ext.getCmp('lblCaraMinumListLabelObat').show();
						Ext.getCmp('cboAturanMinumEtiket').show();
						Ext.getCmp('lblKeteranganListLabelObat').show();
						Ext.getCmp('txtKeteranganListLabelObat').show();
						jenis_obat='Sendok Teh';
						Ext.getCmp('txtCatatanListLabelObat_SIRUP').show();
						Ext.getCmp('lblCatatanListLabelObat_SIRUP').show();
						Ext.getCmp('TD_CatatanListLabelObat_SIRUP').show();
					}
						/*----------- Etiket Obat Luar -------------*/
					if(kd_jenis_etiket == 4){
						Ext.getCmp('lblJenisHariListLabelObat').hide();
						Ext.getCmp('cboJenisHari').hide();
						Ext.getCmp('lblJamListLabelObat').hide();
						Ext.getCmp('cboJam').hide();
						Ext.getCmp('lblTanggalListLabelObat').hide();
						Ext.getCmp('dfTglListLabelObat').hide();
						Ext.getCmp('TD_CaraPakaiListLabelObat').show();
						Ext.getCmp('lblCaraMinumListLabelObat').hide();
						Ext.getCmp('cboAturanMinumEtiket').hide();
						Ext.getCmp('lblKeteranganListLabelObat').hide();
						Ext.getCmp('txtKeteranganListLabelObat').hide();
						
						Ext.getCmp('lblKeteranganObatLuarListLabelObat').show();
						Ext.getCmp('cboKeteranganObatLuarEtiket').show();
						Ext.getCmp('lblCatatanListLabelObat').show();
						Ext.getCmp('txtCatatanListLabelObat').show(); 
						Ext.getCmp('lblCaraPakaiListLabelObat').show(); 
						Ext.getCmp('txtCaraPakaiListLabelObat').show(); 
						Ext.getCmp('txtCaraPakaiListLabelObat').setValue('');
						jenis_obat='';
						Ext.getCmp('txtCatatanListLabelObat_SIRUP').hide();
						Ext.getCmp('lblCatatanListLabelObat_SIRUP').hide();
						Ext.getCmp('TD_CatatanListLabelObat_SIRUP').hide();
					}
						/*----------- Etiket Racikan -------------*/
					if(kd_jenis_etiket == 5){
						Ext.getCmp('lblJenisHariListLabelObat').hide();
						Ext.getCmp('cboJenisHari').hide();
						Ext.getCmp('lblJamListLabelObat').hide();
						Ext.getCmp('cboJam').hide();
						Ext.getCmp('lblTanggalListLabelObat').hide();
						Ext.getCmp('dfTglListLabelObat').hide();
						Ext.getCmp('TD_CaraPakaiListLabelObat').show();
						Ext.getCmp('lblCaraMinumListLabelObat').hide();
						Ext.getCmp('cboAturanMinumEtiket').hide();
						Ext.getCmp('lblKeteranganListLabelObat').hide();
						Ext.getCmp('txtKeteranganListLabelObat').hide();
						Ext.getCmp('TD_awal').hide();
						
						Ext.getCmp('lblKeteranganObatLuarListLabelObat').hide();
						Ext.getCmp('cboKeteranganObatLuarEtiket').hide();
						Ext.getCmp('TD_lblCaraMinumListLabelObat').hide();
						Ext.getCmp('lblCatatanListLabelObat').show();
						Ext.getCmp('txtCatatanListLabelObat').show(); 
						Ext.getCmp('lblCaraPakaiListLabelObat').show(); 
						Ext.getCmp('txtCaraPakaiListLabelObat').show(); 
						Ext.getCmp('txtCaraPakaiListLabelObat').setValue('');
						jenis_obat='tab';
						Ext.getCmp('txtCatatanListLabelObat_SIRUP').hide();
						Ext.getCmp('lblCatatanListLabelObat_SIRUP').hide();
						Ext.getCmp('TD_CatatanListLabelObat_SIRUP').hide();
					}
					
					// alert(jenis_obat);
					if(kd_jenis_etiket == 1){
						Ext.getCmp('cboJenisHari').focus(true, 20);
					}
					//etiket tablet
					if(kd_jenis_etiket == 2 || kd_jenis_etiket == 3 || kd_jenis_etiket == 5){
						setTimeout(
							function(){ 
								GridWaktuDosisObat_ResepRWI.startEditing(0, 3); 
							}, 500);
							
							//1000 => 1detik
					}
					if(kd_jenis_etiket == 4){
						Ext.getCmp('cboKeteranganObatLuarEtiket').focus(true, 20);
					}
				},
				'specialkey' : function()
				{
					if(Ext.EventObject.getKey() == 13 || Ext.EventObject.getKey() == 9){
						//etiket udd
						if(kd_jenis_etiket == 1){
							Ext.getCmp('cboJenisHari').focus(true, 20);
						}
						//etiket tablet
						if(kd_jenis_etiket == 2 || kd_jenis_etiket == 3 || kd_jenis_etiket == 5){
							setTimeout(
								function(){ 
									GridWaktuDosisObat_ResepRWI.startEditing(0, 3); 
								}, 500);
								
								//1000 => 1detik
						}
						if(kd_jenis_etiket == 4){
							Ext.getCmp('cboKeteranganObatLuarEtiket').focus(true, 20);
						}
					}			
				}
			}
		}
	);
	return cboJenisEtiket;
};

function getJenisEtiket(){
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/functionAPOTEKrwi/getJenisEtiket",
		params: {text:'0'},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['ListDataObj'].length; i<iLen; i++){
				var recs    = [],recType =   ds_jenis_etiket.recordType;
				var o=cst['ListDataObj'][i];
				recs.push(new recType(o));
				ds_jenis_etiket.add(recs);
				console.log(o);
			}
		}
	});
}
function mComboJenisHari()
{
	var Field = ['kd_waktu','waktu'];

	ds_waktu_etiket = new WebApp.DataStore({ fields: Field });
	getWaktuEtiket();
	var cboJenisHari = new Ext.form.ComboBox
		(
			{
				id:'cboJenisHari',
				x: 160,
				y: 10,
				typeAhead: true,
				triggerAction: 'all',
				lazyRender:true,
				mode: 'local',
				width: 350,
				editable: false,
				emptyText:'',
				//fieldLabel: 'JENIS',
				tabIndex:5,
				store: ds_waktu_etiket ,
				valueField: 'kd_waktu',
				displayField: 'waktu',
				value:'Pagi',
				listeners:
				{
					'select': function(a,b,c)
					{
						if(b.data.kd_waktu == 1){
							Ext.getCmp('cboJam').setValue("07.00");
						}else if(b.data.kd_waktu == 2){
							Ext.getCmp('cboJam').setValue("13.00");
						}else if(b.data.kd_waktu == 3){
							Ext.getCmp('cboJam').setValue("16.00");
						}else{
							Ext.getCmp('cboJam').setValue("24.00");
						}
						getJamEtiket(b.data.kd_waktu);
					},
					'specialkey' : function()
					{
						if(Ext.EventObject.getKey()==13 || Ext.EventObject.getKey() == 9){
							Ext.getCmp('cboJam').focus(true, 20);
						}
					}
				}
			}
		);
		return cboJenisHari;
};
function getJamEtiket(kd_waktu){
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/functionAPOTEKrwi/getJamEtiket",
		params: {kd_waktu:kd_waktu},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);
			 ds_jam_etiket.removeAll();
			for(var i=0,iLen=cst['ListDataObj'].length; i<iLen; i++){
				var recs    = [],recType =   ds_jam_etiket.recordType;
				var o=cst['ListDataObj'][i];
				recs.push(new recType(o));
				ds_jam_etiket.add(recs);
				console.log(o);
			}
		}
	});
}
function mComboJam()
{
	var Field = ['kd_jam','jam'];

	ds_jam_etiket = new WebApp.DataStore({ fields: Field });
	
  var cboJam = new Ext.form.ComboBox
	(
		{
			id:'cboJam',
			x: 160,
			y: 40,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			width: 350,
			editable: false,
			emptyText:'',
			//fieldLabel: 'JENIS',
			tabIndex:5,
			store: ds_jam_etiket,
			valueField: 'kd_jam',
			displayField: 'jam',
			value:'07.00',
			listeners:
			{
				'select': function(a,b,c)
				{
					

				},
				'specialkey' : function()
				{
					if(Ext.EventObject.getKey()==13 || Ext.EventObject.getKey() == 9){
							Ext.getCmp('dfTglListLabelObat').focus(true, 20);
					}						
				}
			}
		}
	);
	return cboJam;
};
function getWaktuEtiket(){
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/functionAPOTEKrwi/getWaktuEtiket",
		params: {text:'0'},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['ListDataObj'].length; i<iLen; i++){
				var recs    = [],recType =   ds_waktu_etiket.recordType;
				var o=cst['ListDataObj'][i];
				recs.push(new recType(o));
				ds_waktu_etiket.add(recs);
				console.log(o);
			}
		}
	});
}

function CetakLabelObatApotekResepRWI(callback){
	if (jml_tampung_ceklis_obat >= 1){
		Ext.Ajax.request
		( 
			{
				url: baseURL + "index.php/apotek/functionAPOTEKrwi/CetakLabelObatApotekResepRWI",
				params: getParamCetakLabelObatResepRWI(),
				failure: function(o)
				{
					ShowPesanErrorResepRWJ('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					callback(o.responseText);
					//jml_tampung_ceklis_obat=0;
					
				}
			}
			
		)
	}else{
		ShowPesanInfoResepRWI('Pilih obat terlebih dahulu!','Information');
	}
	
	
}


function getParamCetakLabelObatResepRWI(){
	var tmp_aturan_minum='';
	var tmp_keterangan='';
	var tmp_catatan='';
	var tmp_jenis_hari='';
	var tmp_jam='';
	var tmp_tgl_udd='';
	var unit_kamar= Ext.getCmp('txtKamarApotekResepRWIL').getValue();
	var qty_etiket= Ext.getCmp('cboQtyEtiket').getValue();
	
	if(jenis_etiket == 2 || jenis_etiket == 3 || jenis_etiket == 5){
		tmp_aturan_minum = Ext.get('cboAturanMinumEtiket').getValue();
		//obat tablet
		if(jenis_etiket == 2 || jenis_etiket == 5){
			tmp_catatan = Ext.getCmp('txtCatatanListLabelObat').getValue();
			if(jenis_etiket == 5){
				tmp_aturan_minum = Ext.get('txtCaraPakaiListLabelObat').getValue();
			}
		}else{
		//obat sirup
			tmp_catatan = Ext.getCmp('txtCatatanListLabelObat_SIRUP').getValue();
			tmp_keterangan = Ext.getCmp('txtKeteranganListLabelObat').getValue();
		}
	}else{
		//obat luar
		if(jenis_etiket == 4){
			tmp_aturan_minum = Ext.get('txtCaraPakaiListLabelObat').getValue();
			tmp_keterangan = Ext.get('cboKeteranganObatLuarEtiket').getValue();
			tmp_catatan = Ext.getCmp('txtCatatanListLabelObat').getValue();
		}
		
		if(jenis_etiket == 1){
			tmp_jenis_hari = Ext.get('cboJenisHari').getValue();
			tmp_jam = Ext.get('cboJam').getValue();
			tmp_tgl_udd = Ext.getCmp('dfTglListLabelObat').getValue().format('Y-m-d');
		}
		
	}
	
	var params =
	{
		NoResep:Ext.getCmp('txtNoResepApotekResepRWIL').getValue(),
		TglOut:Ext.getCmp('txtTmpTgloutResepRWIL').getValue(),
		KdPasien:Ext.getCmp('txtKdPasienResepRWI_viApotekResepRWI').getValue(),
		Jenis_obat:jenis_obat,
		Jenis_etiket:jenis_etiket,
		Aturan_minum:tmp_aturan_minum,
		Keterangan:tmp_keterangan,
		Catatan:tmp_catatan,
		JenisHari:tmp_jenis_hari,
		Jam:tmp_jam,
		TglUDD:tmp_tgl_udd,
		unit_kamar:unit_kamar,
		qty_etiket:qty_etiket
	}
	//==================== PARAM TAMPUNG LIST OBAT ETIKET JIKA MENGGUNAKAN CHECKBOX =====================
	
	/* params['jml_tampung_ceklis_obat']=jml_tampung_ceklis_obat;
	for(var i = 0 ; i < jml_tampung_ceklis_obat ; i++)
	{
		params['kd_prd-'+i]=tampung_ceklis_obat['kd_prd-'+i]
		
	} */
	
	//==================== PARAM TAMPUNG LIST OBAT ETIKET JIKA MENGGUNAKAN EVENT KEYBOARD =====================
	
	var jml_grid_obat = dsGridListDosisObat_ResepRWI.getCount();
	var i_tmp=0;
	for(var i = 0 ; i < jml_grid_obat ; i++)
	{
		if(dsGridListDosisObat_ResepRWI.data.items[i].data.pilih == 'V'){
			params['kd_prd-'+i_tmp] = dsGridListDosisObat_ResepRWI.data.items[i].data.kd_prd;
			params['nama_obat-'+i_tmp] = dsGridListDosisObat_ResepRWI.data.items[i].data.nama_obat.slice(0, 18);
			params['qty_obat-'+i_tmp] = dsGridListDosisObat_ResepRWI.data.items[i].data.jml_out;
			i_tmp++;
		}
	} 
	params['jml_tampung_ceklis_obat']=i_tmp;
	// tampung_waktu_dosis_obat
	params['jumlah_dosis_obat']=dsGridWaktuDosisObat_ResepRWI.getCount();
	for(var i = 0 ; i < dsGridWaktuDosisObat_ResepRWI.getCount() ; i++)
	{
		params['kd_waktu-'+i]=dsGridWaktuDosisObat_ResepRWI.data.items[i].data.kd_waktu;
		params['waktu-'+i]=dsGridWaktuDosisObat_ResepRWI.data.items[i].data.waktu;
		params['jam-'+i]=dsGridWaktuDosisObat_ResepRWI.data.items[i].data.jam;
		params['qty-'+i]=dsGridWaktuDosisObat_ResepRWI.data.items[i].data.qty;
		
		if(dsGridWaktuDosisObat_ResepRWI.data.items[i].data.jenis_takaran == '' || dsGridWaktuDosisObat_ResepRWI.data.items[i].data.jenis_takaran == undefined){
			params['jenis_takaran-'+i]=jenis_obat;
		}else{
			params['jenis_takaran-'+i]=dsGridWaktuDosisObat_ResepRWI.data.items[i].data.jenis_takaran;
		}
		
	}
	return params;
}

function cetak_label_obat(){
	
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionAPOTEKrwi/CetakLabelObat",
			params: getParamCetakLabelObatResepRWI(),
			failure: function(o)
			{
				ShowPesanErrorResepRWI('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				GridListDosisObat_ResepRWI.getView().refresh();
				GridListDosisObat_ResepRWI.startEditing(0, 1);
			}
		}
		
	)
}
function nonaktivRWI(parms){
	Ext.getCmp('txtCatatanResepRWI_viResepRWI').setReadOnly(parms);
	Ext.getCmp('dfTglResepSebenarnyaResepRWI').setReadOnly(parms);
	Ext.getCmp('cboPilihankelompokPasienAptResepRWI').setReadOnly(parms);
	Ext.getCmp('cbo_DokterApotekResepRWI').setReadOnly(parms);
	Ext.getCmp('txtKdPasienResepRWI_viApotekResepRWI').setReadOnly(parms);
	Ext.getCmp('txtNamaPasienResepRWI_viApotekResepRWI').setReadOnly(parms);
	if(parms == true){
		
	} else{
		Ext.getCmp('btn1/2resep_viApotekResepRWI').enable();
		ResepRWI.form.Grid.a.enable();
	}
	
}

function LookUpSearchListGetObat_resepRWI(nama_obat,resep){	
	WindowLookUpSearchListGetObat_ResepRWI = new Ext.Window({
            id: 'pnlLookUpSearchListGetObat_ResepRWI',
            title: 'List Pencarian Obat',
            width:620,
            height: 250,
            border: false,
            resizable:false,
            plain: true,
            iconCls: 'icon_lapor',
            modal: true,
            items: [
				ItempanelListPencarianObatKepemilikan_ResepRWI(nama_obat),
				gridListObatPencarianResepRWI(resep)
			],
			listeners:{             
				activate: function(){
				},
				afterShow: function(){
					this.activate();
				},
				deactivate: function(){
				},
				close: function (){
					// if(FocusExitResepRWI == false){
					// 	console.log(resep);
					// 	if(resep != undefined && resep =='Y'){
					// 		var line	= gridRacikanIgd.getSelectionModel().selection.cell[0];
					// 		gridRacikanIgd.startEditing(line, 8);
					// 	}else{
					// 		var line	= ResepRWI.form.Grid.a.getSelectionModel().selection.cell[0];
					// 		ResepRWI.form.Grid.a.startEditing(line, 11);	
					// 	}
						
					// }
				}
			}
        }
    );

    WindowLookUpSearchListGetObat_ResepRWI.show();
	getListObatSearch_ResepRWI(nama_obat,'');
};

function gridListObatPencarianResepRWI(racik){
	var fldDetail = ['kd_prd','nama_obat','satuan','sub_jenis','jml_stok_apt','harga_jual','milik','biaya_racik', 'formularium'];
	dsGridListPencarianObat_ResepRWI = new WebApp.DataStore({ fields: fldDetail });
	
    GridListPencarianObatColumnModelRWI =  new Ext.grid.ColumnModel([
        new Ext.grid.RowNumberer(),
		{
			dataIndex		: 'kd_prd',
			header			: 'Kode',
			width			: 70,
			menuDisabled	: true,
        },
		{
			dataIndex		: 'nama_obat',
			header			: 'Nama Obat',
			width			: 180,
			menuDisabled	: true,
        },
		{
			dataIndex		: 'satuan',
			header			: 'Satuan',
			width			: 60,
			menuDisabled	: true,
        },{
			dataIndex		: 'sub_jenis',
			header			: 'Sub Jenis',
			width			: 100,
			menuDisabled	: true,
        },{
			dataIndex		: 'jml_stok_apt',
			header			: 'Stok',
			width			: 50,
			align			: 'right',
			menuDisabled	: true,
        },
		{
			dataIndex		: 'harga_jual',
			header			: 'Harga',
			xtype			: 'numbercolumn',
			align			: 'right',
			format 			: '0,000',
			width			: 60,
			menuDisabled	: true,
			hidden	: true,
        },
		{
			dataIndex		: 'formularium',
			header			: 'Formularium',
			width			: 80,
			menuDisabled	: true,
        },
		{
			dataIndex		: 'milik',
			header			: 'Kepemilikan',
			width			: 80,
			hidden 			: true,
			menuDisabled	: true,
        }
	]);
	
	
	GridListPencarianObat_ResepRWI= new Ext.grid.EditorGridPanel({
		id			: 'GrdListPencarianObat_ResepRWI',
		stripeRows	: true,
		width		: 610,
		height		: 170,
        store		: dsGridListPencarianObat_ResepRWI,
        border		: true,
        frame		: false,
        autoScroll	: true,
        cm			: GridListPencarianObatColumnModelRWI,
		selModel: new Ext.grid.RowSelectionModel({
			singleSelect: true,
			listeners:
			{
				rowselect: function(sm, row, rec)
				{
					currentRowSelectionPencarianObatResepRWI = undefined;
					currentRowSelectionPencarianObatResepRWI = dsGridListPencarianObat_ResepRWI.getAt(row);
				}
			}
		}),
		listeners	: {
			rowclick: function( $this, rowIndex, e )
			{
				// trcellCurrentTindakan_KasirRWJ = rowIndex;
    		},
			celldblclick: function(gridView,htmlElement,columnIndex,dataRecord){
			},
			'keydown' : function(e){
				if(e.getKey() == 13){
					if(racik != undefined && racik =='Y'){
						var line	=gridRacikanIgd.getSelectionModel().selection.cell[0];
						// GET HARGA JUAL DENGAN MARKUP 2018-10-09
						// ------ START CEK MARKUP
						console.log('enter');
						Ext.Ajax.request({
							url: baseURL + "index.php/apotek/functionAPOTEKrwi/getHargaBeliObat",
							params: {
								kd_prd		: currentRowSelectionPencarianObatResepRWI.data.kd_prd,
								kd_milik	: currentRowSelectionPencarianObatResepRWI.data.kd_milik,
								kdcustomer	: Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').getValue(),
							
							},
							failure: function(o)
							{
								var cst = Ext.decode(o.responseText);
								// ShowPesanErrorResepRWJ('Data transaksi tidak dapat dihapus','Resep RWJ');
							},	    
							success: function(o) {
								var cst = Ext.decode(o.responseText);
								if (cst.success===true)
								{
									//console.log(cst.ListDataObj[0]);
									dsRacikan_IGD.data.items[line].data.harga_beli		=cst.ListDataObj[0].harga_beli;
									dsRacikan_IGD.data.items[line].data.markup			=cst.ListDataObj[0].markup;
									dsRacikan_IGD.data.items[line].data.harga_jual		=cst.ListDataObj[0].harga_jual;
									dsRacikan_IGD.data.items[line].data.hargaaslicito	=cst.ListDataObj[0].hargaaslicito;
									currentHargaJualObat=cst.ListDataObj[0].harga_jual;
									
									dsRacikan_IGD.data.items[line].data.cito="Tidak";
									dsRacikan_IGD.data.items[line].data.nama_obat=currentRowSelectionPencarianObatResepRWI.data.nama_obat;
									dsRacikan_IGD.data.items[line].data.kd_prd=currentRowSelectionPencarianObatResepRWI.data.kd_prd;
									dsRacikan_IGD.data.items[line].data.kd_satuan=currentRowSelectionPencarianObatResepRWI.data.kd_satuan;
									dsRacikan_IGD.data.items[line].data.fractions=currentRowSelectionPencarianObatResepRWI.data.fractions;
									dsRacikan_IGD.data.items[line].data.kd_pabrik=currentRowSelectionPencarianObatResepRWI.data.kd_pabrik;
									dsRacikan_IGD.data.items[line].data.adm_racik=currentRowSelectionPencarianObatResepRWI.data.adm_racik;
									dsRacikan_IGD.data.items[line].data.jasa=currentRowSelectionPencarianObatResepRWI.data.jasa;
									dsRacikan_IGD.data.items[line].data.no_out=currentRowSelectionPencarianObatResepRWI.data.no_out;
									dsRacikan_IGD.data.items[line].data.no_urut=currentRowSelectionPencarianObatResepRWI.data.no_urut;
									dsRacikan_IGD.data.items[line].data.tgl_out=currentRowSelectionPencarianObatResepRWI.data.tgl_out;
									dsRacikan_IGD.data.items[line].data.kd_milik=currentRowSelectionPencarianObatResepRWI.data.kd_milik;
									dsRacikan_IGD.data.items[line].data.jml_stok_apt=currentRowSelectionPencarianObatResepRWI.data.jml_stok_apt;
									dsRacikan_IGD.data.items[line].data.milik=currentRowSelectionPencarianObatResepRWI.data.milik;
									dsRacikan_IGD.data.items[line].data.biaya_racik=currentRowSelectionPencarianObatResepRWI.data.biaya_racik;
									dsRacikan_IGD.data.items[line].data.nilai_cito=0;
									dsRacikan_IGD.data.items[line].data.disc=0;
									dsRacikan_IGD.data.items[line].data.racik="Tidak";
									gridRacikanIgd.getView().refresh();
									gridRacikanIgd.startEditing(line, 8);	
									if(currentRowSelectionPencarianObatResepRWI.data.jml_stok_apt <= 10){
										Ext.Msg.show({
											title: 'Information',
											msg: 'Stok obat hampir habis, jumlah stok tersedia adalah '+currentRowSelectionPencarianObatResepRWI.data.jml_stok_apt,
											buttons: Ext.MessageBox.OK,
											fn: function (btn) {
												if (btn == 'ok'){
													gridRacikanIgd.startEditing(line, 8);
												}
											}
										});
									}
									
									WindowLookUpSearchListGetObat_ResepRWI.close();
								}else{
									 if(cst.cek_markup === false){
										Ext.Msg.show({
											title: 'Information',
											msg: cst.pesan,
											buttons: Ext.MessageBox.OK,
											fn: function (btn) {
												
											}
										});
									}
								}
								
							}
					
						})
						// ------ END CEK MARKUP
						
					}else{
						var line	= ResepRWI.form.Grid.a.getSelectionModel().selection.cell[0];
						// --- START CEK MARKUP
						Ext.Ajax.request({
							url: baseURL + "index.php/apotek/functionAPOTEKrwi/getHargaBeliObat",
							params: {
								kd_prd		: currentRowSelectionPencarianObatResepRWI.data.kd_prd,
								kd_milik	: currentRowSelectionPencarianObatResepRWI.data.kd_milik,
								kdcustomer	: Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').getValue(),
							
							},
							failure: function(o)
							{
								var cst = Ext.decode(o.responseText);
								// ShowPesanErrorResepRWJ('Data transaksi tidak dapat dihapus','Resep RWJ');
							},	    
							success: function(o) {
								var cst = Ext.decode(o.responseText);
								if (cst.success===true)
								{
									//console.log(cst.ListDataObj[0]);
									dsDataGrdJab_viApotekResepRWI.getRange()[line].data.harga_beli		= cst.ListDataObj[0].harga_beli;
									dsDataGrdJab_viApotekResepRWI.getRange()[line].data.markup			= cst.ListDataObj[0].markup;
									dsDataGrdJab_viApotekResepRWI.getRange()[line].data.harga_jual		= cst.ListDataObj[0].harga_jual;
									dsDataGrdJab_viApotekResepRWI.getRange()[line].data.hargaaslicito	= cst.ListDataObj[0].hargaaslicito;
									// currentHargaJualObat = cst.ListDataObj[0].harga_jual;
									
									
									dsDataGrdJab_viApotekResepRWI.getRange()[line].data.cito="Tidak";
									dsDataGrdJab_viApotekResepRWI.getRange()[line].data.nama_obat=currentRowSelectionPencarianObatResepRWI.data.nama_obat;
									dsDataGrdJab_viApotekResepRWI.getRange()[line].data.kd_prd=currentRowSelectionPencarianObatResepRWI.data.kd_prd;
									dsDataGrdJab_viApotekResepRWI.getRange()[line].data.kd_satuan=currentRowSelectionPencarianObatResepRWI.data.kd_satuan;
									dsDataGrdJab_viApotekResepRWI.getRange()[line].data.fractions=currentRowSelectionPencarianObatResepRWI.data.fractions;
									dsDataGrdJab_viApotekResepRWI.getRange()[line].data.kd_pabrik=currentRowSelectionPencarianObatResepRWI.data.kd_pabrik;
									dsDataGrdJab_viApotekResepRWI.getRange()[line].data.adm_racik=currentRowSelectionPencarianObatResepRWI.data.adm_racik;
									dsDataGrdJab_viApotekResepRWI.getRange()[line].data.jasa=currentRowSelectionPencarianObatResepRWI.data.jasa;
									dsDataGrdJab_viApotekResepRWI.getRange()[line].data.no_out=currentRowSelectionPencarianObatResepRWI.data.no_out;
									dsDataGrdJab_viApotekResepRWI.getRange()[line].data.no_urut=currentRowSelectionPencarianObatResepRWI.data.no_urut;
									dsDataGrdJab_viApotekResepRWI.getRange()[line].data.tgl_out=currentRowSelectionPencarianObatResepRWI.data.tgl_out;
									dsDataGrdJab_viApotekResepRWI.getRange()[line].data.kd_milik=currentRowSelectionPencarianObatResepRWI.data.kd_milik;
									dsDataGrdJab_viApotekResepRWI.getRange()[line].data.jml_stok_apt=currentRowSelectionPencarianObatResepRWI.data.jml_stok_apt;
									dsDataGrdJab_viApotekResepRWI.getRange()[line].data.milik=currentRowSelectionPencarianObatResepRWI.data.milik;
									dsDataGrdJab_viApotekResepRWI.getRange()[line].data.biaya_racik=currentRowSelectionPencarianObatResepRWI.data.biaya_racik;
									dsDataGrdJab_viApotekResepRWI.getRange()[line].data.nilai_cito=0;
									dsDataGrdJab_viApotekResepRWI.getRange()[line].data.disc=0;
									dsDataGrdJab_viApotekResepRWI.getRange()[line].data.racik="Tidak";
								
									ResepRWI.form.Grid.a.getView().refresh();
									
									ResepRWI.form.Grid.a.startEditing(line, 6);	
									
									if(currentRowSelectionPencarianObatResepRWI.data.jml_stok_apt <= 10){
										Ext.Msg.show({
											title: 'Information',
											msg: 'Stok obat hampir habis, jumlah stok tersedia adalah '+currentRowSelectionPencarianObatResepRWI.data.jml_stok_apt,
											buttons: Ext.MessageBox.OK,
											fn: function (btn) {
												if (btn == 'ok')
												{
													ResepRWI.form.Grid.a.startEditing(line, 6);
												}
											}
										});
									}
									FocusExitResepRWI = true;
									WindowLookUpSearchListGetObat_ResepRWI.close();
									
								}else{
									 if(cst.cek_markup === false){
										Ext.Msg.show({
											title: 'Information',
											msg: cst.pesan,
											buttons: Ext.MessageBox.OK,
											fn: function (btn) {
												
											}
										});
									}
								}
								
							}
						})
						// --- END CEK MARKUP
						
					}
				}
			}
		},
		viewConfig	: {forceFit: true}
    });
	return GridListPencarianObat_ResepRWI;
}

function ItempanelListPencarianObatKepemilikan_ResepRWI(nama_obat){
	 var items=
    (
        {
            id: 'panelListPencarianObatKepemilikan_ResepRWI',
			layout:'form',
			border: true,
			bodyStyle:'padding: 2px',
			height: 50,
			items:
			[
				{
					layout: 'absolute',
					bodyStyle: 'padding: 2px ',
					border: true,
					anchor: '100% 100%',
					items:
					[
						{
							x: 10,
							y: 10,
							xtype: 'label',
							text: 'Kepemilikan'
						},
						{
							x: 140,
							y: 10,
							xtype: 'label',
							text: ':'
						},
						ComboKepemilikanPencarianObat_ResepRWI(nama_obat),
					]
				}
				
			]	
        }
    );

    return items;
}

function ComboKepemilikanPencarianObat_ResepRWI(nama_obat)
{
	var Field = ['kd_milik','milik'];
	ds_kepemilikan_pencarianobat_reseprwi = new WebApp.DataStore({ fields: Field });
	getKepemilikanPencarianObatResepRWI();
	
	var cboKepemilikanPencarianObatResepRWI = new Ext.form.ComboBox
	(
		{
			id:'cbKepemilikanPencarianObatResepRWI',
			x: 160,
			y: 10,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			width: 350,
			// editable: false,
			tabIndex:5,
			store: ds_kepemilikan_pencarianobat_reseprwi,
			valueField: 'kd_milik',
			displayField: 'milik',
			value:'SEMUA KEPEMILIKAN',
			listeners:
			{
				'select': function(a,b,c)
				{
					PencarianLookupResepRWI = false; // Variabel pembeda getdata u/nama obat dan semua kepemilikan atau bukan. False => pencarian berdasarkan nama_obat dan kepemilikan, True => pencarian berdasarkan nama_obat saja
					getListObatSearch_ResepRWI(nama_obat,b.data.kd_milik);
				},
				'specialkey' : function()
				{
					if(Ext.EventObject.getKey() == 13 || Ext.EventObject.getKey() == 9){
					}			
				}
			}
		}
	);
	return cboKepemilikanPencarianObatResepRWI;
};

function getListObatSearch_ResepRWI(nama_obat,kd_milik){
	Ext.Ajax.request ({
		url: baseURL + "index.php/apotek/functionAPOTEKrwi/getListObat",
		params: {
			nama_obat:nama_obat,
			kdcustomer:Ext.getCmp('txtTmpKdCustomerApotekResepRWIL').getValue(),
			kd_milik:kd_milik
		},
		failure: function(o)
		{
			ShowPesanErrorResepRWI('Error menampilkan pencarian obat. Hubungi Admin!', 'Error');
		},	
		success: function(o) 
		{   
			dsGridListPencarianObat_ResepRWI.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				if(cst.totalrecords == 0 ){
					if(PencarianLookupResepRWI == true){						
						Ext.Msg.show({
							title: 'Information',
							msg: 'Tidak ada obat yang sesuai atau kriteria obat kurang!',
							buttons: Ext.MessageBox.OK,
							fn: function (btn) {
								if (btn == 'ok')
								{
									var line	= ResepRWI.form.Grid.a.getSelectionModel().selection.cell[0];
									ResepRWI.form.Grid.a.startEditing(line, 6);
									WindowLookUpSearchListGetObat_ResepRWI.close();
								}
							}
						});
					}
				} else{
					var recs=[],
						recType=dsGridListPencarianObat_ResepRWI.recordType;
					for(var i=0; i<cst.ListDataObj.length; i++){
						recs.push(new recType(cst.ListDataObj[i]));						
					}
					dsGridListPencarianObat_ResepRWI.add(recs);
					GridListPencarianObat_ResepRWI.getView().refresh();
					GridListPencarianObat_ResepRWI.getSelectionModel().selectRow(0);
					GridListPencarianObat_ResepRWI.getView().focusRow(0);
				}
				
			} else {
				ShowPesanErrorResepRWI('Gagal membaca data list pencarian obat', 'Error');
			};
		}
	});
}

function getKepemilikanPencarianObatResepRWI(){
	Ext.Ajax.request({
		url: baseURL + "index.php/apotek/functionAPOTEKrwi/getKepemilikan",
		params: {text:'0'},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['ListDataObj'].length; i<iLen; i++){
				var recs    = [],recType =   ds_kepemilikan_pencarianobat_reseprwi.recordType;
				var o=cst['ListDataObj'][i];
				recs.push(new recType(o));
				ds_kepemilikan_pencarianobat_reseprwi.add(recs);
			}
		}
	});
}