var dataSource_viSetupUnit;
var selectCount_viSetupUnit=50;
var NamaForm_viSetupUnit="Setup Unit";
var mod_name_viSetupUnit="Setup Unit";
var now_viSetupUnit= new Date();
var rowSelected_viSetupUnit;
var setLookUps_viSetupUnit;
var tanggal = now_viSetupUnit.format("d/M/Y");
var jam = now_viSetupUnit.format("H/i/s");
var tmpkriteria;
var DataGridJenisObat;
var GridDataView_viSetupUnit;


var CurrentData_viSetupUnit =
{
	data: Object,
	details: Array,
	row: 0
};

var SetupUnit={};
SetupUnit.form={};
SetupUnit.func={};
SetupUnit.vars={};
SetupUnit.func.parent=SetupUnit;
SetupUnit.form.ArrayStore={};
SetupUnit.form.ComboBox={};
SetupUnit.form.DataStore={};
SetupUnit.form.Record={};
SetupUnit.form.Form={};
SetupUnit.form.Grid={};
SetupUnit.form.Panel={};
SetupUnit.form.TextField={};
SetupUnit.form.Button={};

SetupUnit.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_unit_far', 'nm_unit_far', 'nomor_ro_unit', 'nomorawal','nomor_out_milik'],
	data: []
});

CurrentPage.page = dataGrid_viSetupUnit(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dataGrid_viSetupUnit(mod_id_viSetupUnit){	
 	// Field kiriman dari Project Net.
    var FieldMaster_viSetupUnit = 
	[
		'kd_unit_far', 'nm_unit_far', 'nomor_ro_unit', 'nomorawal','nomor_out_milik' 
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viSetupUnit = new WebApp.DataStore
	({
        fields: FieldMaster_viSetupUnit
    });
    dataGriSetupUnit();
    // Grid Apotek Perencanaan # --------------
	GridDataView_viSetupUnit = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viSetupUnit,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:true,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viSetupUnit = undefined;
							rowSelected_viSetupUnit = dataSource_viSetupUnit.getAt(row);
							CurrentData_viSetupUnit
							CurrentData_viSetupUnit.row = row;
							CurrentData_viSetupUnit.data = rowSelected_viSetupUnit.data;
							//DataInitSetupUnit(rowSelected_viSetupUnit.data);
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viSetupUnit = dataSource_viSetupUnit.getAt(ridx);
					if (rowSelected_viSetupUnit != undefined)
					{
						DataInitSetupUnit(rowSelected_viSetupUnit.data);
						//setLookUp_viSetupUnit(rowSelected_viSetupUnit.data);
					}
					else
					{
						//setLookUp_viSetupUnit();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Setup Unit
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colKode_viSetupUnit',
						header: 'Kode Unit',
						dataIndex: 'kd_unit_far',
						hideable:false,
						menuDisabled: true,
						width: 40
						
					},
					//-------------- ## --------------
					{
						id: 'colNamaUnit_viSetupUnit',
						header: 'Nama unit',
						dataIndex: 'nm_unit_far',
						hideable:false,
						menuDisabled: true,
						width: 90
					},
					//-------------- ## --------------
					{
						id: 'colNoRO_viSetupUnit',
						header: 'Nomor RO unit',
						dataIndex: 'nomor_ro_unit',
						hideable:false,
						hidden:true,
						menuDisabled: true,
						align:'right',
						width: 70
						
					},
					//-------------- ## --------------
					{
						id: 'colNomorAwal_viSetupUnit',
						header:'Nomor awal',
						dataIndex: 'nomorawal',						
						width: 70,
						hideable:false,
						hidden:true,
						align:'right',
                        menuDisabled:true
					},
					//-------------- ## --------------
					{
						id: 'colKota_viSetupUnit',
						header: 'Nomor out milik',
						dataIndex: 'nomor_out_milik',
						align:'right',
						hideable:false,
						hidden:true,
                        menuDisabled:true,
						width: 70
					}
					//-------------- ## --------------
					
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viSetupUnit',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viSetupUnit',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viSetupUnit != undefined)
							{
								DataInitSetupUnit(rowSelected_viSetupUnit.data);
							}
							
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viSetupUnit, selectCount_viSetupUnit, dataSource_viSetupUnit),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)	
	var PanelTabSetupUnit = new Ext.FormPanel({
        labelAlign: 'top',
		layout:'form',
		border: false,  
		autoHeight:true,
        title: '',
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [PanelInputUnit()]
		
	})
	// Kriteria filter pada Grid # --------------
    var FrmData_viSetupUnit = new Ext.Panel
    (
		{
			title: NamaForm_viSetupUnit,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viSetupUnit,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ PanelTabSetupUnit,
					GridDataView_viSetupUnit],
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAddJenis_viSetupUnit',
						handler: function(){
							AddNewSetupUnit();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viSetupUnit',
						handler: function()
						{
							loadMask.show();
							dataSave_viSetupUnit();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viSetupUnit',
						handler: function()
						{
							Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
								if (button == 'yes'){
									loadMask.show();
									dataDelete_viSetupUnit();
								}
							});
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_viSetupPabrik',
						handler: function()
						{
							dataSource_viSetupUnit.removeAll();
							dataGriSetupUnit();
						}
					},
					{
						xtype: 'tbseparator'
					}
					
				]
			}
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_viSetupUnit;
    //-------------- # End form filter # --------------
}

function PanelInputUnit(rowdata){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 55,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'Kode unit'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 0,
								xtype: 'textfield',
								id: 'txtKdUnit_SetupUnit',
								name: 'txtKdUnit_SetupUnit',
								width: 100,
								allowBlank: false,
								maxLength:3,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									},
									blur: function(a){
										if(a.getValue().length > 3){
											ShowPesanWarningSetupUnit('Kode unit tidak boleh lebih dari 3 huruf', 'Warning');
										}
									}
								}
							},
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'Nama unit'
							},
							{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							SetupUnit.vars.nama=new Nci.form.Combobox.autoComplete({
								x: 130,
								y: 30,
								tabIndex:2,
								id		: 'txtComboNama_SetupUnit',
								store	: SetupUnit.form.ArrayStore.a,
								select	: function(a,b,c){
									
									Ext.getCmp('txtKdUnit_SetupUnit').setValue(b.data.kd_unit_far);
									
									GridDataView_viSetupUnit.getView().refresh();
									
								},
								onShowList:function(a){
									dataSource_viSetupUnit.removeAll();
									
									var recs=[],
									recType=dataSource_viSetupUnit.recordType;
										
									for(var i=0; i<a.length; i++){
										recs.push(new recType(a[i]));
									}
									dataSource_viSetupUnit.add(recs);
									
								},
								insert	: function(o){
									return {
										kd_unit_far      	: o.kd_unit_far,
										nm_unit_far 		: o.nm_unit_far,
										text			:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_unit_far+'</td><td width="200">'+o.nm_unit_far+'</td></tr></table>'
									}
								},
								url		: baseURL + "index.php/apotek/functionSetupUnit/getUnitGrid",
								valueField: 'nm_unit_far',
								displayField: 'text',
								listWidth: 280
							})
							
							
						]
					}
				]
			}
		]		
	};
        return items;
}
//------------------end---------------------------------------------------------

function dataGriSetupUnit(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionSetupUnit/getUnitGrid",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorSetupUnit('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dataSource_viSetupUnit.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dataSource_viSetupUnit.add(recs);
					
					
					
					GridDataView_viSetupUnit.getView().refresh();
				}
				else 
				{
					ShowPesanErrorSetupUnit('Gagal membaca data obat', 'Error');
				};
			}
		}
		
	)
	
}

function dataSave_viSetupUnit(){
	if (ValidasiSaveSetupUnit(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionSetupUnit/save",
				params: getParamSaveSetupUnit(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupUnit('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupUnit('Berhasil menyimpan data ini','Information');
						dataSource_viSetupUnit.removeAll();
						dataGriSetupUnit();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupUnit('Gagal menyimpan data ini', 'Error');
						dataSource_viSetupUnit.removeAll();
						dataGriSetupUnit();
					};
				}
			}
			
		)
	}
}

function dataDelete_viSetupUnit(){
	if (ValidasiSaveSetupUnit(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionSetupUnit/delete",
				params: getParamDeleteSetupUnit(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupUnit('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupUnit('Berhasil menghapus data ini','Information');
						AddNewSetupUnit()
						dataSource_viSetupUnit.removeAll();
						dataGriSetupUnit();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupUnit('Gagal menghapus data ini', 'Error');
						dataSource_viSetupUnit.removeAll();
						dataGriSetupUnit();
					};
				}
			}
			
		)
	}
}

function AddNewSetupUnit(){
	Ext.getCmp('txtKdUnit_SetupUnit').setValue('');
	SetupUnit.vars.nama.setValue('');
};

function DataInitSetupUnit(rowdata){
	Ext.getCmp('txtKdUnit_SetupUnit').setValue(rowdata.kd_unit_far);
	SetupUnit.vars.nama.setValue(rowdata.nm_unit_far);
};

function getParamSaveSetupUnit(){
	var	params =
	{
		KdUnit:Ext.getCmp('txtKdUnit_SetupUnit').getValue(),
		Nama:SetupUnit.vars.nama.getValue()
	}
   
    return params
};

function getParamDeleteSetupUnit(){
	var	params =
	{
		KdUnit:Ext.getCmp('txtKdUnit_SetupUnit').getValue()
	}
   
    return params
};

function ValidasiSaveSetupUnit(modul,mBolHapus){
	var x = 1;
	if(SetupUnit.vars.nama.getValue() === '' || Ext.getCmp('txtKdUnit_SetupUnit').getValue() ===''){
		if(SetupUnit.vars.nama.getValue() === ''){
			loadMask.hide();
			ShowPesanWarningSetupUnit('Nama unit masih kosong', 'Warning');
			x = 0;
		} else{
			loadMask.hide();
			ShowPesanWarningSetupUnit('Kode unit masih kosong', 'Warning');
			x = 0;
		}
		
	} 
	if(Ext.getCmp('txtKdUnit_SetupUnit').getValue().length > 3){
		ShowPesanWarningSetupUnit('Kode unit tidak boleh lebih dari 3 huruf', 'Warning');
		x = 0;
	}
	return x;
};



function ShowPesanWarningSetupUnit(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorSetupUnit(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoSetupUnit(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};