var dataSource_viSetupVendor;
var selectCount_viSetupVendor=50;
var NamaForm_viSetupVendor="Setup Vendor";
var mod_name_viSetupVendor="Setup Vendor";
var now_viSetupVendor= new Date();
var rowSelected_viSetupVendor;
var setLookUps_viSetupVendor;
var tanggal = now_viSetupVendor.format("d/M/Y");
var jam = now_viSetupVendor.format("H/i/s");
var tmpkriteria;
var DataGridJenisObat;
var GridDataView_viSetupVendor;


var CurrentData_viSetupVendor =
{
	data: Object,
	details: Array,
	row: 0
};

var SetupVendor={};
SetupVendor.form={};
SetupVendor.func={};
SetupVendor.vars={};
SetupVendor.func.parent=SetupVendor;
SetupVendor.form.ArrayStore={};
SetupVendor.form.ComboBox={};
SetupVendor.form.DataStore={};
SetupVendor.form.Record={};
SetupVendor.form.Form={};
SetupVendor.form.Grid={};
SetupVendor.form.Panel={};
SetupVendor.form.TextField={};
SetupVendor.form.Button={};

SetupVendor.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_vendor', 'vendor', 'contact', 'alamat', 'kota', 'telepon1', 'telepon2',
			'fax', 'kd_pos', 'negara', 'norek', 'term'],
	data: []
});

CurrentPage.page = dataGrid_viSetupVendor(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dataGrid_viSetupVendor(mod_id_viSetupVendor){	
 	// Field kiriman dari Project Net.
    var FieldMaster_viSetupVendor = 
	[
		'kd_vendor', 'vendor', 'contact', 'alamat', 'kota', 'telepon1', 'telepon2',
		'fax', 'kd_pos', 'negara', 'norek', 'term'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viSetupVendor = new WebApp.DataStore
	({
        fields: FieldMaster_viSetupVendor
    });
    dataGriSetupVendor();
    // Grid Apotek Perencanaan # --------------
	GridDataView_viSetupVendor = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viSetupVendor,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:true,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viSetupVendor = undefined;
							rowSelected_viSetupVendor = dataSource_viSetupVendor.getAt(row);
							CurrentData_viSetupVendor
							CurrentData_viSetupVendor.row = row;
							CurrentData_viSetupVendor.data = rowSelected_viSetupVendor.data;
							//DataInitSetupvendor(rowSelected_viSetupVendor.data);
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viSetupVendor = dataSource_viSetupVendor.getAt(ridx);
					if (rowSelected_viSetupVendor != undefined)
					{
						DataInitSetupvendor(rowSelected_viSetupVendor.data);
						//setLookUp_viSetupVendor(rowSelected_viSetupVendor.data);
					}
					else
					{
						//setLookUp_viSetupVendor();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Setup vendor
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colKode_viSetupVendor',
						header: 'Kode',
						dataIndex: 'kd_vendor',
						hideable:false,
						menuDisabled: true,
						width: 40
						
					},
					//-------------- ## --------------
					{
						id: 'colVendor_viSetupVendor',
						header: 'Nama vendor',
						dataIndex: 'vendor',
						hideable:false,
						menuDisabled: true,
						width: 90
					},
					//-------------- ## --------------
					{
						id: 'colContact_viSetupVendor',
						header: 'Contact',
						dataIndex: 'contact',
						hideable:false,
						menuDisabled: true,
						width: 50
						
					},
					//-------------- ## --------------
					{
						id: 'colAlamat_viSetupVendor',
						header:'Alamat vendor',
						dataIndex: 'alamat',						
						width: 130,
						hideable:false,
                        menuDisabled:true
					},
					//-------------- ## --------------
					{
						id: 'colKota_viSetupVendor',
						header: 'Kota',
						dataIndex: 'kota',
						hideable:false,
                        menuDisabled:true,
						width: 50
					},
					//-------------- ## --------------
					{
						id: 'colNorek_viSetupVendor',
						header: 'Norek',
						dataIndex: 'norek',
						hideable:false,
						menuDisabled: true,
						width: 50
					}
					//-------------- ## --------------
					
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viSetupVendor',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viSetupVendor',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viSetupVendor != undefined)
							{
								DataInitSetupvendor(rowSelected_viSetupVendor.data);
							}
							
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viSetupVendor, selectCount_viSetupVendor, dataSource_viSetupVendor),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)	
	var PanelTabSetupVendor = new Ext.FormPanel({
        labelAlign: 'top',
		layout:'form',
		border: false,  
		autoHeight:true,
        title: '',
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [PanelInputVendor()]
		
	})
	// Kriteria filter pada Grid # --------------
    var FrmData_viSetupVendor = new Ext.Panel
    (
		{
			title: NamaForm_viSetupVendor,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viSetupVendor,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ PanelTabSetupVendor,
					GridDataView_viSetupVendor],
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAddJenis_viSetupVendor',
						handler: function(){
							AddNewSetupVendor();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viSetupVendor',
						handler: function()
						{
							loadMask.show();
							dataSave_viSetupVendor();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viSetupVendor',
						handler: function()
						{
							Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
								if (button == 'yes'){
									loadMask.show();
									dataDelete_viSetupVendor();
								}
							});
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_viSetupVendor',
						handler: function()
						{
							dataSource_viSetupVendor.removeAll();
							dataGriSetupVendor();
						}
					},
					{
						xtype: 'tbseparator'
					}
					
				]
			}
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_viSetupVendor;
    //-------------- # End form filter # --------------
}

function PanelInputVendor(rowdata){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 170,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'Kode Vendor'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 0,
								xtype: 'textfield',
								id: 'txtKdVendor_SetupVendor',
								name: 'txtKdVendor_SetupVendor',
								allowBlank: false,
								width: 130,
								readOnly:true,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									}
								}
							},
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'Nama Vendor'
							},
							{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							
							SetupVendor.vars.nama=new Nci.form.Combobox.autoCompleteId({
								x: 130,
								y: 30,
								tabIndex:2,
								id		: 'txtComboNama_SetupVendor',
								store	: SetupVendor.form.ArrayStore.a,
								select	: function(a,b,c){
									
									Ext.getCmp('txtKdVendor_SetupVendor').setValue(b.data.kd_vendor);
									Ext.getCmp('txtKontak_SetupVendor').setValue(b.data.contact);
									Ext.getCmp('txtAlamat_SetupVendor').setValue(b.data.alamat);
									Ext.getCmp('txtKota_SetupVendor').setValue(b.data.kota);
									Ext.getCmp('txtKodePos_SetupVendor').setValue(b.data.kd_pos);
									Ext.getCmp('txtNegara_SetupVendor').setValue(b.data.negara);
									Ext.getCmp('txtTelepon_SetupVendor').setValue(b.data.telepon1);
									Ext.getCmp('txtTelepon2_SetupVendor').setValue(b.data.telepon2);
									Ext.getCmp('txtFax_SetupVendor').setValue(b.data.fax);
									Ext.getCmp('txtNorek_SetupVendor').setValue(b.data.norek);
									Ext.getCmp('txtTempo_SetupVendor').setValue(b.data.term);
									GridDataView_viSetupVendor.getView().refresh();
									
								},
								onShowList:function(a){
									dataSource_viSetupVendor.removeAll();
									
									var recs=[],
									recType=dataSource_viSetupVendor.recordType;
										
									for(var i=0; i<a.length; i++){
										recs.push(new recType(a[i]));
									}
									dataSource_viSetupVendor.add(recs);
									
								},
								insert	: function(o){
									return {
										kd_vendor      	: o.kd_vendor,
										vendor 			: o.vendor,
										contact			: o.contact,
										alamat			: o.alamat,
										kota			: o.kota,
										telepon1		: o.telepon1,
										telepon2		: o.telepon2,
										fax				: o.fax,
										kd_pos			: o.kd_pos,
										negara			: o.negara,
										term			: o.term,
										norek			: o.norek,
										text			:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_vendor+'</td><td width="200">'+o.vendor+'</td></tr></table>'
									}
								},
								url		: baseURL + "index.php/apotek/functionSetupVendor/getVendorGrid",
								valueField: 'vendor',
								displayField: 'text',
								listWidth: 280
							}),
							{
								x: 10,
								y: 60,
								xtype: 'label',
								text: 'Kontak Person'
							},
							{
								x: 120,
								y: 60,
								xtype: 'label',
								text: ':'
							},
							{
								x:130,
								y:60,	
								xtype: 'textfield',
								name: 'txtKontak_SetupVendor',
								id: 'txtKontak_SetupVendor',
								tabIndex:3,
								width: 180
							},
							{
								x: 10,
								y: 90,
								xtype: 'label',
								text: 'Alamat'
							},
							{
								x: 120,
								y: 90,
								xtype: 'label',
								text: ':'
							},
							{
								x:130,
								y:90,	
								xtype: 'textarea',
								name: 'txtAlamat_SetupVendor',
								id: 'txtAlamat_SetupVendor',
								width : 280,
								height : 50,
								tabIndex:4
							},
							{
								x: 10,
								y: 148,
								xtype: 'label',
								text: 'Kota'
							},
							{
								x: 120,
								y: 148,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 148,
								xtype: 'textfield',
								name: 'txtKota_SetupVendor',
								id: 'txtKota_SetupVendor',
								tabIndex:5,
								width: 150
							},
							
							//-------------- ## --------------
							{
								x: 420,
								y: 0,
								xtype: 'label',
								text: 'Kode Pos'
							},
							{
								x: 510,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 520,
								y: 0,
								xtype: 'numberfield',
								name: 'txtKodePos_SetupVendor',
								id: 'txtKodePos_SetupVendor',
								tabIndex:6,
								width: 70
							},
							{
								x: 420,
								y: 30,
								xtype: 'label',
								text: 'Negara'
							},
							{
								x: 510,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							{
								x: 520,
								y: 30,
								xtype: 'textfield',
								name: 'txtNegara_SetupVendor',
								id: 'txtNegara_SetupVendor',
								tabIndex:7,
								width: 150
							},
							{
								x: 420,
								y: 60,
								xtype: 'label',
								text: 'Telepon'
							},
							{
								x: 510,
								y: 60,
								xtype: 'label',
								text: ':'
							},
							{
								x: 520,
								y: 60,
								xtype: 'textfield',
								name: 'txtTelepon_SetupVendor',
								id: 'txtTelepon_SetupVendor',
								tabIndex:8,
								width: 100
							},
							{
								x: 630,
								y: 60,
								xtype: 'textfield',
								name: 'txtTelepon2_SetupVendor',
								id: 'txtTelepon2_SetupVendor',
								tabIndex:9,
								width: 100
							},
							{
								x: 740,
								y: 60,
								xtype: 'label',
								text: 'Fax'
							},
							{
								x: 770,
								y: 60,
								xtype: 'label',
								text: ':'
							},
							{
								x: 780,
								y: 60,
								xtype: 'textfield',
								name: 'txtFax_SetupVendor',
								id: 'txtFax_SetupVendor',
								tabIndex:10,
								width: 100
							},
							{
								x: 420,
								y: 90,
								xtype: 'label',
								text: 'No Rek Bank'
							},
							{
								x: 510,
								y: 90,
								xtype: 'label',
								text: ':'
							},
							{
								x: 520,
								y: 90,
								xtype: 'textfield',
								name: 'txtNorek_SetupVendor',
								id: 'txtNorek_SetupVendor',
								tabIndex:11,
								width: 210
							},
							{
								x: 420,
								y: 120,
								xtype: 'label',
								text: 'Jatuh Tempo'
							},
							{
								x: 510,
								y: 120,
								xtype: 'label',
								text: ':'
							},
							{
								x: 520,
								y: 120,
								xtype: 'numberfield',
								name: 'txtTempo_SetupVendor',
								id: 'txtTempo_SetupVendor',
								tabIndex:12,
								width: 70
							}
							
							
						]
					}
				]
			}
		]		
	};
        return items;
}
//------------------end---------------------------------------------------------


function dataGriSetupVendor(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionSetupVendor/getVendorGrid",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorSetupVendor('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dataSource_viSetupVendor.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dataSource_viSetupVendor.add(recs);
					
					
					
					GridDataView_viSetupVendor.getView().refresh();
				}
				else 
				{
					ShowPesanErrorSetupVendor('Gagal membaca data obat', 'Error');
				};
			}
		}
		
	)
	
}

function dataSave_viSetupVendor(){
	if (ValidasiSaveSetupVendor(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionSetupVendor/save",
				params: getParamSaveSetupVendor(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupVendor('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupVendor('Berhasil menyimpan data ini','Information');
						Ext.getCmp('txtKdVendor_SetupVendor').setValue(cst.kdvendor);
						dataSource_viSetupVendor.removeAll();
						dataGriSetupVendor();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupVendor('Gagal menyimpan data ini', 'Error');
						dataSource_viSetupVendor.removeAll();
						dataGriSetupVendor();
					};
				}
			}
			
		)
	}
}

function dataDelete_viSetupVendor(){
	if (ValidasiSaveSetupVendor(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionSetupVendor/delete",
				params: getParamDeleteSetupVendor(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupVendor('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupVendor('Berhasil menghapus data ini','Information');
						AddNewSetupVendor()
						dataSource_viSetupVendor.removeAll();
						dataGriSetupVendor();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupVendor('Gagal menghapus data ini', 'Error');
						dataSource_viSetupVendor.removeAll();
						dataGriSetupVendor();
					};
				}
			}
			
		)
	}
}

/* function refreshSetupVendor(kriteria)
{
    dataSource_viSetupVendor.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'ViewSetupVendor',
                    param : kriteria
                }			
            }
        );   
    return dataSource_viSetupVendor;
} */
function AddNewSetupVendor(){
	Ext.getCmp('txtKdVendor_SetupVendor').setValue('');
	SetupVendor.vars.nama.setValue('');
	Ext.getCmp('txtKontak_SetupVendor').setValue('');
	Ext.getCmp('txtAlamat_SetupVendor').setValue('');
	Ext.getCmp('txtKota_SetupVendor').setValue('');
	Ext.getCmp('txtKodePos_SetupVendor').setValue('');
	Ext.getCmp('txtNegara_SetupVendor').setValue('');
	Ext.getCmp('txtTelepon_SetupVendor').setValue('');
	Ext.getCmp('txtTelepon2_SetupVendor').setValue('');
	Ext.getCmp('txtFax_SetupVendor').setValue('');
	Ext.getCmp('txtNorek_SetupVendor').setValue('');
	Ext.getCmp('txtTempo_SetupVendor').setValue('');
};

function DataInitSetupvendor(rowdata){
	Ext.getCmp('txtKdVendor_SetupVendor').setValue(rowdata.kd_vendor);
	SetupVendor.vars.nama.setValue(rowdata.vendor);
	Ext.getCmp('txtKontak_SetupVendor').setValue(rowdata.contact);
	Ext.getCmp('txtAlamat_SetupVendor').setValue(rowdata.alamat);
	Ext.getCmp('txtKota_SetupVendor').setValue(rowdata.kota);
	Ext.getCmp('txtKodePos_SetupVendor').setValue(rowdata.kd_pos);
	Ext.getCmp('txtNegara_SetupVendor').setValue(rowdata.negara);
	Ext.getCmp('txtTelepon_SetupVendor').setValue(rowdata.telepon1);
	Ext.getCmp('txtTelepon2_SetupVendor').setValue(rowdata.telepon2);
	Ext.getCmp('txtFax_SetupVendor').setValue(rowdata.fax);
	Ext.getCmp('txtNorek_SetupVendor').setValue(rowdata.norek);
	Ext.getCmp('txtTempo_SetupVendor').setValue(rowdata.term);
};

function getParamSaveSetupVendor(){
	var term;
	if(Ext.getCmp('txtTempo_SetupVendor').getValue() ===''){
		term=0;
	} else{
		term=Ext.getCmp('txtTempo_SetupVendor').getValue();
	}
	var	params =
	{
		KdVendor:Ext.getCmp('txtKdVendor_SetupVendor').getValue(),
		Nama:SetupVendor.vars.nama.getValue(),
		Kontak:Ext.getCmp('txtKontak_SetupVendor').getValue(),
		Alamat:Ext.getCmp('txtAlamat_SetupVendor').getValue(),
		Kota:Ext.getCmp('txtKota_SetupVendor').getValue(),
		KodePos:Ext.getCmp('txtKodePos_SetupVendor').getValue(),
		Negara:Ext.getCmp('txtNegara_SetupVendor').getValue(),
		Telepon:Ext.getCmp('txtTelepon_SetupVendor').getValue(),
		Telepon2:Ext.getCmp('txtTelepon2_SetupVendor').getValue(),
		Fax:Ext.getCmp('txtFax_SetupVendor').getValue(),
		Norek:Ext.getCmp('txtNorek_SetupVendor').getValue(),
		Tempo:term
	}
   
    return params
};

function getParamDeleteSetupVendor(){
	var	params =
	{
		KdVendor:Ext.getCmp('txtKdVendor_SetupVendor').getValue()
	}
   
    return params
};

function ValidasiSaveSetupVendor(modul,mBolHapus){
	var x = 1;
	if(SetupVendor.vars.nama.getValue() === ''){
		loadMask.hide();
		ShowPesanWarningSetupVendor('Nama vendor masih kosong', 'Warning');
		x = 0;
	} 
	return x;
};



function ShowPesanWarningSetupVendor(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorSetupVendor(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoSetupVendor(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};