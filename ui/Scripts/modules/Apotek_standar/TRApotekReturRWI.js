var AprotekReturInap={
	test:function(){
		var $this=this;
		
	},
	vars:{
		selectData:null,
		title:'Retur Rawat Inap ',
		no_out:null,
		gridInputIndeks:null,
		data:null,
		kd_pay:null,
		jml_terima_uang:0,
		selectDetail:null,
		reduksi:0,
		post:false,
		tanggal : new Date().format("d/M/Y"),
		noOut:null,
		tglOut:null,
		kdUnitMr:null,
		kdKasirMr:null,
		noTransaksiMR:null
	},
	ArrayStore:{
		main:new Ext.data.ArrayStore({fields:[]}),
		comboMain:new Ext.data.ArrayStore({fields:['kd_unit', 'nama_unit']}),
		pasien:new Ext.data.ArrayStore({fields:['text']}),
		gridInput:new Ext.data.ArrayStore({fields:[]}),
		dsprinter_returrwi : new Ext.data.ArrayStore({ fields: ['kd_pasien','nama','kd_dokter','kd_unit','kd_customer','no_transaksi','kd_kasir','id_mrresep','urut_masuk','tgl_masuk','tgl_transaksi']}),
		gridDetail:new Ext.data.ArrayStore({fields:['NO_TRANSAKSI','TGL_BAYAR','DESKRIPSI','URUT','BAYAR','USERNAME']}),
		comboBayar1:null,
		comboBayar2:null
	},
	Button:{
		bayar:null,
		deleteHistory:null,
		posting:null,
		deleted:null,
		save:null,
		save2:null,
		cetak:null
	},
	Grid:{
		main:null,
		input:null,
		detail:null
	},
	Panel:{
		main:null,
		search:null,
		btn:null
	},
	ComboBox:{
		unit:null,
		posting:null,
		noFaktur:null,
		srchPosting:null,
		srchRuangan:null,
		bayar1:null,
		bayar2:null,
		cbopasienorder_printer_returrwi:null
	},
	DateField:{
		srchStartDate:null,
		srchEndDate:null,
		byrDate:null
	},
	DisplayField:{
		posting:null,
		shift:null,
		tanggal:null
	},
	TextField:{
		noRetur:null,
		srchNoRetur:null,
		srchKodePasien:null,
		namaUnit:null,
		namaPasien:null,
		noKamar:null,
		unitKamar:null,
		dokter:null,
		noRm:null,
		JnsPasien:null,
		byrNoRetur:null,
		byrKdPasien:null,
		byrNamaPasien:null
	},
	NumberField:{
		adm:null,
		reduksi:null,
		jumlah:null,
		total:null,
		byr:null,
		byrTotal:null
	},
	Window:{
		input:null,
		bayar:null
	},
	
	load_data_printer:function(){
		var $this=this;
		Ext.Ajax.request(
			{
				url: baseURL + "index.php/apotek/functionAPOTEK/printer",
				params:{
					command: ""
				} ,
				failure: function(o)
				{
					 var cst = Ext.decode(o.responseText);
					
				},	    
				success: function(o) {
					$this.ArrayStore.dsprinter_returrwi.loadData([],false);
		
						var cst = Ext.decode(o.responseText);

					for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
						var recs    = [],recType = $this.ArrayStore.dsprinter_returrwi.recordType;
						var o=cst['listData'][i];
						
						recs.push(new recType(o));
						$this.ArrayStore.dsprinter_returrwi.add(recs);
						console.log(o);
					}
				}
			});
	},
	
	refreshBayar:function(){
		var $this=this;
		loadMask.show();
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			url:baseURL + "index.php/apotek/functionApotekReturRWI/getBayar",
			data:{no_retur:$this.TextField.noRetur.getValue()},
			success: function(r){
				loadMask.hide();
				if(r.processResult=='SUCCESS'){
					$this.ArrayStore.gridDetail.loadData([],false);
					$this.vars.jml_terima_uang=0;
					for(var i=0,iLen=r.listData.length; i<iLen ;i++){
						var records=[];
						var bayar=parseFloat(r.listData[i].jumlah)-parseFloat(r.listData[i].jml_terima_uang);
						if(bayar>0){
							r.listData[i]['sisa']=bayar;
						}else{
							r.listData[i]['sisa']=0;
						}	
						records.push(new $this.ArrayStore.gridDetail.recordType(r.listData[i]));
						$this.vars.jml_terima_uang+=parseFloat(r.listData[i].jml_terima_uang);
						$this.ArrayStore.gridDetail.add(records);
					}
					
					if($this.vars.jml_terima_uang>=toInteger($this.NumberField.total.getValue()) || $this.vars.post == true){
						$this.Button.bayar.disable();
						$this.Button.posting.enable();
						
					}else{
						$this.Button.bayar.enable();
								$this.Button.deleted.disable();
								$this.Button.posting.disable();
								$this.Button.save.disable();
								$this.Button.save2.disable();
								$this.Button.deleteHistory.disable();
					}
					/* if(parseFloat($this.NumberField.byr.getValue())>=parseFloat($this.NumberField.byrTotal.getValue())){
						$this.Button.bayar.disable();
						
					}else{
						$this.Button.bayar.enable();
						$this.DisplayField.posting.setValue('<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>');
					} */
					if(r.listData.length==0 || $this.vars.post==true){
						$this.Button.deleteHistory.disable();
					}else{
						$this.Button.deleteHistory.enable();
					}
					$this.vars.selectDetail=null;
					$this.Grid.detail.getView().refresh();
				}else{
					Ext.Msg.alert('Gagal',r.processMessage);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
	},
	refresh:function(){
		var $this=this;
		loadMask.show();
		var a=[];
		$this.vars.selectData=null;
		a.push({name: 'no_retur',value:$this.TextField.srchNoRetur.getValue()});
		a.push({name: 'kd_pasien',value:$this.TextField.srchKodePasien.getValue()});
		a.push({name: 'ruangan',value:$this.ComboBox.unit.getValue()});
		a.push({name: 'posting',value:$this.ComboBox.posting.getValue()});
		a.push({name: 'startDate',value:$this.DateField.srchStartDate.value});
		a.push({name: 'lastDate',value:$this.DateField.srchEndDate.value});
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			url:baseURL + "index.php/apotek/functionApotekReturRWI/initList",
			data:a,
			success: function(r){
				loadMask.hide();
				if(r.processResult=='SUCCESS'){
					$this.ArrayStore.main.loadData([],false);
					for(var i=0,iLen=r.listData.length; i<iLen ;i++){
						var records=[];
						records.push(new $this.ArrayStore.main.recordType());
						$this.ArrayStore.main.add(records);
						$this.ArrayStore.main.data.items[i].data.tutup=r.listData[i].tutup;
						$this.ArrayStore.main.data.items[i].data.no_resep=r.listData[i].no_resep;
						$this.ArrayStore.main.data.items[i].data.no_out=r.listData[i].no_out;
						$this.ArrayStore.main.data.items[i].data.tgl_out=r.listData[i].tgl_out;
						$this.ArrayStore.main.data.items[i].data.kd_pasienapt=r.listData[i].kd_pasienapt;
						$this.ArrayStore.main.data.items[i].data.nmpasien=r.listData[i].nmpasien;
						$this.ArrayStore.main.data.items[i].data.no_kamar=r.listData[i].no_kamar;
					}
					$this.Grid.main.getView().refresh();
				}else{
					Ext.Msg.alert('Gagal',r.processMessage);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
	},
	save:function(callback){
		var $this=this;
		if($this.getParams() != undefined){
			if($this.TextField.noRetur.getValue()==''){
				Ext.Msg.confirm('Konfirmasi', 'Apakah Anda Ingin Menyimpan Retur Ini?', function (id, value) { 
					if (id === 'yes') { 
						loadMask.show();
						$.ajax({
							url:baseURL + "index.php/apotek/functionApotekReturRWI/save",
							dataType:'JSON',
							type: 'POST',
							data:$this.getParams(),
							success: function(r){
								loadMask.hide();
								if(r.processResult=='SUCCESS'){
									$this.Button.bayar.enable();
									$this.TextField.noRetur.setValue(r.data);
									$this.ComboBox.noFaktur.disable();
									$this.Button.bayar.enable();
									$this.Button.save.disable();
									$this.Button.save2.disable();
									$this.Button.posting.disable();
									$this.refreshBayar();
									Ext.Msg.alert('Sukses','Data Berhasi Disimpan.');
									if(callback != undefined){
										callback();
									}
								}else{
									Ext.Msg.alert('Gagal',r.processMessage);
								}
							},
							error: function(jqXHR, exception) {
								loadMask.hide();
								Nci.ajax.ErrorMessage(jqXHR, exception);
							}
						});
					} 
				}, this);
			}else{
				Ext.Msg.confirm('Konfirmasi', 'Apakah Anda Ingin Menyimpan Retur Ini?', function (id, value) { 
					if (id === 'yes') { 
						loadMask.show();
						$.ajax({
							url:baseURL + "index.php/apotek/functionApotekReturRWI/update",
							dataType:'JSON',
							type: 'POST',
							data:$this.getParams(),
							success: function(r){
								loadMask.hide();
								if(r.processResult=='SUCCESS'){
									Ext.Msg.alert('Sukses','Data Berhasi Diubah.');
									if(callback != undefined){
										callback();
									}
								}else{
									Ext.Msg.alert('Gagal',r.processMessage);
								}
							},
							error: function(jqXHR, exception) {
								loadMask.hide();
								Nci.ajax.ErrorMessage(jqXHR, exception);
							}
						});
					} 
				}, this);
			}
		}
	},
	datacetakbill:function() {
		var $this=this;
		var params =
		{
			Table: 'billprintingreturrwi',
			NoRetur:$this.TextField.noRetur.getValue(),
			NoResep:$this.ComboBox.noFaktur.getValue(),
			NoOut:$this.vars.noOut,
			TglOut:$this.vars.tglOut,
			KdPasien:$this.TextField.noRm.getValue(),
			NamaPasien: $this.TextField.namaPasien.getValue(),
			JenisPasien:$this.TextField.JnsPasien.getValue(),
			Dokter:$this.TextField.dokter.getValue(),
			Unit:$this.TextField.namaUnit.getValue(),
			Reduksi:$this.NumberField.reduksi.getValue(),
			SubTotal:$this.NumberField.jumlah.getValue(),
			Total:$this.NumberField.total.getValue(),
			Tot:toInteger($this.NumberField.total.getValue()),
			printer : Ext.getCmp('cbopasienorder_printer_returrwi').getValue()
			
		}
		params['jumlah']=$this.ArrayStore.gridInput.getCount();
		for(var i = 0 ; i < $this.ArrayStore.gridInput.getCount();i++)
		{
			var o=$this.ArrayStore.gridInput.getRange()[i].data;
			params['nama_obat-'+i]=o.nama_obat;
			params['qty-'+i]=o.qty;
		}
		
		return params
	},
	getParams:function(){
		var $this=this;
		var a=[];
		if($this.ComboBox.noFaktur.getValue()==''){
			Ext.Msg.alert('Error','Harap Isi Nomor Faktur.');
			return;
		}else{
			a.push({name: 'no_resep',value:$this.ComboBox.noFaktur.getValue()});
		}
		a.push({name: 'shift',value:$this.DisplayField.shift.getValue()});
		a.push({name: 'kd_dokter',value:$this.vars.data.kd_dokter});
		a.push({name: 'kd_pasienapt',value:$this.vars.data.kd_pasienapt});
		a.push({name: 'nmpasien',value:$this.vars.data.nmpasien});
		a.push({name: 'kd_unit',value:$this.vars.data.kd_unit});
		a.push({name: 'no_retur',value:$this.TextField.noRetur.getValue()});
		a.push({name: 'jml_bayar',value:toInteger($this.NumberField.total.getValue())});
		a.push({name: 'kd_customer',value:$this.vars.data.kd_customer});
		a.push({name: 'kd_unit_far',value:$this.vars.data.kd_unit_far});
		a.push({name: 'apt_kd_kasir',value:$this.vars.data.apt_kd_kasir});
		a.push({name: 'apt_no_transaksi',value:$this.vars.data.apt_no_transaksi});
		a.push({name: 'jml_obat',value:$this.vars.data.jml_obat});
		a.push({name: 'jml_item',value:$this.vars.data.jml_item});
		a.push({name: 'no_kamar',value:$this.vars.data.no_kamar});
		a.push({name: 'tgl_out',value:$this.vars.data.tgl_out});
		a.push({name: 'reduksi',value:toInteger($this.NumberField.reduksi.getValue())});
		a.push({name: 'adm',value:$this.NumberField.adm.getValue()});
		if($this.ArrayStore.gridInput.getCount()==0){
			Ext.Msg.alert('Error','Didak ada Obat untuk diRetur.');
			return;
		}	
		for(var i=0; i<$this.ArrayStore.gridInput.getCount() ; i++){
			var o=$this.ArrayStore.gridInput.getRange()[i].data;
			if(parseInt(o.qty)==0){
				Ext.Msg.alert('Error','Hapus Obat Pada Baris Ke-'+(i+1)+' Jika Tidak Akan Di Retur.');
				return;
			}else{
				a.push({name: 'kd_prd[]',value:o.kd_prd});
				a.push({name: 'qty[]',value:o.qty});
				a.push({name: 'harga_pokok[]',value:o.harga_pokok});
				a.push({name: 'markup[]',value:o.markup});
				a.push({name: 'dosis[]',value:o.dosis});
				a.push({name: 'harga_jual[]',value:o.harga_satuan});
				a.push({name: 'reduksi[]',value:o.reduksi});
			}
		}
		return a;
	},
	getCounting:function(){
		var $this=this;
		var jumlah=0;
		var item=0;
		var reduksi=0;
		for(var i=0; i<$this.ArrayStore.gridInput.getCount();i++){
			var a=$this.ArrayStore.gridInput.getRange()[i].data;
			if(a.jml_out<a.qty){
				a.qty=a.jml_out;
			}
			item+=a.jml_out;
			a.jumlah=parseFloat(a.qty)*parseFloat(a.harga_satuan);
			a.reduksi=(parseFloat(a.jumlah)*parseFloat($this.vars.reduksi))/100;
			reduksi += a.reduksi;
			jumlah+=a.jumlah;
			console.log(a.jumlah)
		}
		
		$this.NumberField.jumlah.setValue(toFormat(jumlah));
		if($this.vars.data==undefined){
			$this.vars.data={
				jml_obat:jumlah
			}
		}else{
			$this.vars.data.jml_obat=jumlah;
		}
		
		$this.vars.data.jml_item=item;
		//var reduksi=jumlah*(parseFloat($this.vars.reduksi)/100);
		var total=jumlah-reduksi;
		//parseFloat($this.NumberField.adm.getValue()))/100)*;
		$this.NumberField.reduksi.setValue(toFormat(reduksi));
		$this.NumberField.total.setValue(toFormat(total));
		$this.Grid.input.getView().refresh();
	},
	resetInput:function(){
		var $this=this;
		$this.ComboBox.noFaktur.enable();
		$this.Button.bayar.disable();
		$this.Button.deleteHistory.disable();
		$this.Button.posting.disable();
    	$this.Button.save.enable();
    	$this.Button.save2.enable();
    	$this.Button.deleted.enable();
		$this.ComboBox.noFaktur.setValue('');
		$this.TextField.noRetur.setValue('');
		$this.TextField.namaUnit.setValue('');
		$this.TextField.namaPasien.setValue('');
		$this.TextField.noKamar.setValue('');
		$this.TextField.unitKamar.setValue('');
		$this.TextField.dokter.setValue('');
		$this.TextField.noRm.setValue('');
		$this.TextField.JnsPasien.setValue('');
		$this.ArrayStore.gridInput.loadData([],false);
		$this.Grid.input.getView().refresh();
		$this.ArrayStore.gridDetail.loadData([],false);
		$this.Grid.detail.getView().refresh();
		$this.NumberField.adm.setValue(0);
		$this.NumberField.reduksi.setValue(0);
		$this.NumberField.jumlah.setValue(0);
		$this.NumberField.total.setValue(0);
		btn.update('<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>');//$this.DisplayField.posting.setValue('<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>');
	},
	getcbopasienorder_printer_returrwi:function(){
		var $this=this;
		$this.load_data_printer();
		$this.ComboBox.cbopasienorder_printer_returrwi= new Ext.form.ComboBox
			(
				{
					typeAhead		: true,
					triggerAction	: 'all',
					lazyRender		: true,
					mode			: 'local',
					emptyText: '',
					fieldLabel:  '',
					align: 'Right',
					width: 100,
					store: $this.ArrayStore.dsprinter_returrwi,
					valueField: 'name',
					displayField: 'name',
					//hideTrigger		: true,
					listeners:
					{
										
					}
				}
			);
			return $this.ComboBox.cbopasienorder_printer_returrwi;
	},
	getComboBayar1: function() {
		var $this=this;
		var Field = ['JENIS_PAY','DESKRIPSI','TYPE_DATA'];
    	$this.ArrayStore.comboBayar1 = new WebApp.DataStore({ fields: Field });
   	 	$this.ArrayStore.comboBayar1.load({
		    params:{
			    Skip: 0,
			    Take: 1000, Sort: 'jenis_pay',
			    Sortdir: 'ASC',
			    target: 'ViewJenisPay',
				param: "TYPE_DATA IN (0,1,3) AND DB_CR='1' ORDER BY Type_data"
			}
		});
    	$this.ComboBox.bayar1 = new Ext.form.ComboBox({
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
			fieldLabel: 'Pembayaran',
		    align: 'Right',
		    width:150,
		    store: $this.ArrayStore.comboBayar1,
		    valueField: 'JENIS_PAY',
		    displayField: 'DESKRIPSI',
			value:'PENGEMBALIAN',
		    listeners:{
			    'select': function(a, b, c){
					$this.loadPembayaran(b.data.JENIS_PAY);
					$this.ComboBox.bayar2.enable()
				}
			}
		});
    	return $this.ComboBox.bayar1;
	},
	loadPembayaran:function(jenis_pay){
		var $this=this;
	    $this.ArrayStore.comboBayar2.load({
		 	params:{
				Skip: 0,
				Take: 1000,
				Sort: 'nama',
				Sortdir: 'ASC',
				target: 'ViewComboBayar',
				param: 'jenis_pay=~'+ jenis_pay+ '~'
			}
	   })  ;    
	},
	getComboBayar2:function(){
		var $this=this;
	    var Field = ['KD_PAY','JENIS_PAY','PAYMENT'];
	    $this.ArrayStore.comboBayar2 = new WebApp.DataStore({fields: Field});
	    $this.ComboBox.bayar2 = new Ext.form.ComboBox({
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
            forceSelection: true,
		    emptyText:'Pilih Pembayaran...',
		    labelWidth:80,
		    align: 'Right',
		    store: $this.ArrayStore.comboBayar2,
		    valueField: 'KD_PAY',
		    displayField: 'PAYMENT',
			disabled:true,
			width:225,
			listeners:{
			    'select': function(a, b, c){
					$this.vars.kd_pay=b.data.KD_PAY;
				}
			}
		});
	    return $this.ComboBox.bayar2;
	},
	showform_print :function(){
		var $this=this;
		var cbopasienorder_printer_returrwi;
	    var Field = ['kd_pasien','nama','kd_dokter','kd_unit','kd_customer','no_transaksi','kd_kasir','id_mrresep','urut_masuk','tgl_masuk','tgl_transaksi'];
		var dsprinter_returrwi = new WebApp.DataStore({ fields: Field });
		Ext.Ajax.request(
			{
				url: baseURL + "index.php/apotek/functionAPOTEK/printer",
				params:{
					command: ''
				} ,
				failure: function(o)
				{
					 var cst = Ext.decode(o.responseText);
					
				},	    
				success: function(o) {
					cbopasienorder_printer_returrwi.store.removeAll();
						var cst = Ext.decode(o.responseText);

					for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
						var recs    = [],recType = dsprinter_returrwi.recordType;
						var o=cst['listData'][i];
						
						recs.push(new recType(o));
						dsprinter_returrwi.add(recs);
						console.log(o);
					}
				}
			});
		var cbopasienorder_printer_returrwi= new Ext.form.ComboBox
		(
			{
				id: 'cbopasienorder_printer_returrwi',
				typeAhead		: true,
				triggerAction	: 'all',
				lazyRender		: true,
				mode			: 'local',
				emptyText: '',
				fieldLabel:  'Daftar Printer',
				align: 'Right',
				//width: 100,
				anchor:'100%',
				store: dsprinter_returrwi,
				valueField: 'name',
				displayField: 'name',
				//hideTrigger		: true,
				listeners:
				{
									
				}
			}
		);
		var panel_printer_returrwi = new Ext.Panel
		(
			{
				id: 'panel_printer_returrwi',
				fileUpload: true,
				layout: 'form',
				width:170,
				height: 120,
				bodyStyle: 'padding:15px',
				border: true,
				items:
				[
					cbopasienorder_printer_returrwi,
					{
						layout: 'hBox',
						border: false,
						defaults: { margins: '0 5 0 0' },
					   // style:{'margin-left':'30px','margin-top':'5px'},
						anchor: '100%',
						layoutConfig:
						{
							padding: '3',
							pack: 'end',
							align: 'middle'
						},
						items:
						[
							 {
								xtype: 'button',
								text: nmBtnOK,
								width: 70,
								hideLabel: true,
								style:{'margin-left':'5px','margin-top':'0px'},
								id: 'btnOkLapRWJPasswordDulu',
								handler: function()
								{		if (Ext.getCmp('cbopasienorder_printer_returrwi').getValue()===""){
										Ext.Msg.alert('Error','Harap Isi Jumlah Pembayaran.');ShowPesanWarningResepRWJ('Pilih dulu print sebelum cetak', 'Warning');
										}else{
										
									if ( Ext.getCmp('cbopasienorder_printer_returrwi').getValue()!=""){
									Ext.Ajax.request
									(
										{
											url: baseURL + "index.php/main/CreateDataObj",
											params: $this.datacetakbill(),
											failure: function(o)
											{	
												Ext.Msg.alert('Error hubungi admin' ,'Error');
											},
											success: function(o)
											{
												var cst = Ext.decode(o.responseText);
												if (cst.success === true)
												{
													
												}
												else if  (cst.success === false && cst.pesan===0)
												{
													Ext.Msg.alert('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
												}
												else
												{
													Ext.Msg.alert('Gagal print '  + 'Error');
												}
											}
										}
									)  
								  }else{
									  Ext.Msg.alert('Pilih Printer terlebih dulu '  , 'Warning');
								  }  
									 
										} 
										 
								}
							},
							{
								xtype: 'button',
								text: nmBtnCancel ,
								width: 70,
								hideLabel: true,
								style:{'margin-left':'5px','margin-top':'0px'},
								id: 'btnCancelLapRWJPasswordDulu',
								handler: function()
								{
										win_printer_returrwi.close();
								}
							} 
						]
					}
				]
			}
		);
	var win_printer_returrwi = new Ext.Window
    (
        {
            id: 'win_printer_returrwi',
            title: 'Printer',
            closeAction: 'destroy',
            width:320,
            height: 120,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [panel_printer_returrwi]

        }
    );

    win_printer_returrwi.show();
	},
	showBayar:function(){
		var $this=this;
		$this.Window.bayar=new Ext.Window({
	        title: 'Pembayaran Retur Rawat Jalan', 
	        closeAction: 'destroy',        
	        width: 523,
	        height: 230,
	        resizable:false,
			emptyText:'Pilih Jenis Pembayaran...',
			autoScroll: false,
	        border: true,
	        constrain : true,    
	        iconCls: 'Studi_Lanjut',
	        modal: true,		
	        items: [{
			    layout: 'Form',
			    anchor: '100%',
			    //width: lebar- 80,
			    labelAlign: 'Left',
			    bodyStyle: 'padding:10px 10px 10px 10px',
				labelWidth: 100,
				autoWidth: true,
				border:true,
				items:[			
					{
		                xtype: 'compositefield',
		                fieldLabel: 'No. Retur ',
		                anchor: '100%',
		                width: 250,
		                items: [                    
							$this.TextField.byrNoRetur= new Ext.form.TextField({
								flex: 1,
								width : 150,	
								readOnly: true,
								emptyText: 'No Retur',
								listeners:{
									'specialkey': function() {
										if (Ext.EventObject.getKey() === 13){
										}
									}
								}
							}),{
								xtype: 'displayfield',
								flex: 1,
								width: 70,
								name: '',
								value: 'Tanggal :',
								fieldLabel: 'Label'
							},$this.DateField.byrDate=new Ext.form.DateField({
								flex: 1,
								width : 150,	
								format: 'd/M/Y',
								value:$this.vars.tanggal,
								readOnly:true
							})
		                ]
		            },{
		                xtype: 'compositefield',
		                fieldLabel: 'Kode Pasien ',
		                anchor: '100%',
		                width: 250,
		                items:  [                    
							$this.TextField.byrKdPasien= new Ext.form.TextField({
								width : 150,	
								readOnly:true
							}),	$this.TextField.byrNamaPasien= new Ext.form.TextField({
								width : 225,	
								readOnly:true
							})
		                ]
		            },{
						xtype: 'compositefield',
						fieldLabel: 'Pembayaran ',
						anchor: '100%',
						width: 199,
						items: [
							$this.getComboBayar1(),
							$this.getComboBayar2()
						]
					}
							
				]
			},{
			    layout: 'Form',
			    anchor: '100%',
			    labelAlign: 'Left',
			    bodyStyle: 'padding:10px 10px 10px 10px',
				labelWidth: 100,
				autoWidth: true,
				border:true,
				items:[			
					{
		                xtype: 'compositefield',
		                fieldLabel: ' ',
						labelSeparator: '',
		                anchor: '100%',
		                width: 250,
		                items:[         
							{
								xtype: 'displayfield',
								flex: 1,
								width: 150,
								name: '',
								value: ''
							},{
								xtype: 'displayfield',
								flex: 1,
								width: 70,
								name: '',
								value: 'Total :',
								fieldLabel: 'Label'
							},$this.NumberField.byrTotal=new Ext.form.TextField({
								xtype: 'textfield',
								flex: 1,
								width : 150,	
								readOnly: true,
								style:{'text-align':'right'},
								listeners:{
									'specialkey': function(){
										if (Ext.EventObject.getKey() === 13){
										}
									}
								}
							})
		                ]
		            },{
		                xtype: 'compositefield',
		                fieldLabel: ' ',
						labelSeparator: '',
		                anchor: '100%',
		                width: 250,
		                items:[                    
							{
								xtype: 'displayfield',
								flex: 1,
								width: 150,
								name: '',
								value: ''
							},{
								xtype: 'displayfield',
								flex: 1,
								width: 70,
								name: '',
								value: 'Bayar :',
								fieldLabel: 'Label'
							},$this.NumberField.byr=new Ext.form.NumberField({
								flex: 1,
								width : 150,	
								style:{'text-align':'right'},
								listeners:{
									'specialkey': function(){
										if (Ext.EventObject.getKey() === 13){
											$this.bayar();
										}
									}
								}
							}),{
								xtype:'button',
								text:'1/2 Resep',
								width:70,
								hideLabel:true,
								id: 'btn1/2resep_viApotekReturRWJ',
								handler:function(){
								}   
							}
		                ]
		            },{
		                xtype: 'compositefield',
		                fieldLabel: ' ',
						labelSeparator: '',
		                anchor: '100%',
		                width: 230,
		                items: [
							{
								xtype: 'displayfield',
								flex: 1,
								width: 305,
								name: '',
								value: ''
							},{
								xtype:'button',
								text:'Paid',
								width:70,
								hideLabel:true,
								handler:function(){
									$this.bayar();
								}   
							}
						]
		            }
				]
			}
		   ],
	        listeners: {
	            activate: function(){
	            },
	            afterShow: function(){
	                this.activate();
	            },
	            deactivate: function(){
	              //  rowSelected_viApotekReturRWJ=undefined;
	            }
	        }
	    });
	    
	    $this.Window.bayar.show();
		
	    
	    $this.TextField.byrNoRetur.setValue($this.TextField.noRetur.getValue());
	    $this.TextField.byrKdPasien.setValue($this.TextField.noRm.getValue());
	    $this.TextField.byrNamaPasien.setValue($this.TextField.namaPasien.getValue())
		$this.TextField.byrNamaPasien.setValue($this.TextField.namaPasien.getValue())
	    
		if($this.vars.jml_terima_uang == 0 || $this.vars.jml_terima_uang =='0'){
			$this.NumberField.byr.setValue(toInteger($this.NumberField.total.getValue()));
			$this.NumberField.byrTotal.setValue($this.NumberField.total.getValue());
		} else{
			$this.NumberField.byr.setValue(toInteger($this.NumberField.total.getValue())-$this.vars.jml_terima_uang);
			$this.NumberField.byrTotal.setValue(toFormat(toInteger($this.NumberField.total.getValue())-$this.vars.jml_terima_uang));
		}
	/* 	alert($this.NumberField.total.getValue());
		alert($this.vars.jml_terima_uang); */
	},
	bayar:function(){
		var $this=this;
		if($this.ComboBox.bayar2.getValue()==''){
			Ext.Msg.alert('Error','Harap Pilih Jenis Pembayaran.');
			return;
		}
		var jum=parseInt($this.NumberField.byr.getValue());
		if(isNaN(jum) || jum==0){
			Ext.Msg.alert('Error','Harap Isi Jumlah Pembayaran.');
			return;
		}
		var a=[];
		a.push({name:'no_retur',value:$this.TextField.noRetur.getValue()});
		a.push({name:'total',value:toInteger($this.NumberField.byrTotal.getValue())});
		a.push({name:'bayar',value:toInteger($this.NumberField.byr.getValue())});
		a.push({name:'urut',value:$this.ArrayStore.gridDetail.getCount()+1});
		a.push({name:'kd_pay',value:$this.vars.kd_pay});
		a.push({name:'shift',value:$this.DisplayField.shift.getValue()});
		for(var i=0; i<$this.ArrayStore.gridInput.getCount() ; i++){
			var o=$this.ArrayStore.gridInput.getRange()[i].data;
			if(parseInt(o.qty)==0){
				Ext.Msg.alert('Error','Hapus Obat Pada Baris Ke-'+(i+1)+' Jika Tidak Akan Di Retur.');
				return;
			}else{
				a.push({name: 'kd_prd[]',value:o.kd_prd});
				a.push({name: 'qty[]',value:o.qty});
				a.push({name: 'harga_pokok[]',value:o.harga_pokok});
				a.push({name: 'markup[]',value:o.markup});
				a.push({name: 'dosis[]',value:o.dosis});
				a.push({name: 'harga_jual[]',value:o.harga_satuan});
				a.push({name: 'reduksi[]',value:o.reduksi});
			}
		}
		Ext.Msg.confirm('Konfirmasi', 'Apakah Akan Melanjutkan Pembayaran?', function (id, value) { 
			if (id === 'yes') { 
				loadMask.show();
				$.ajax({
					url:baseURL + "index.php/apotek/functionApotekReturRWI/bayar",
					dataType:'JSON',
					type: 'POST',
					data:a,
					success: function(r){
						loadMask.hide();
						if(r.processResult=='SUCCESS'){
							if(r.posting==true){
							if(toInteger($this.NumberField.byr.getValue()) >= toInteger($this.NumberField.byrTotal.getValue())){
								$this.Button.bayar.disable();
								$this.Button.deleted.disable();
								$this.Button.posting.enable();
								$this.Button.save.disable();
								$this.Button.save2.disable();
								$this.Button.deleteHistory.disable();
								$this.Button.cetak.enable();
								$this.vars.post=true;
								Ext.Msg.alert('Sukses','Pembayaran berhasil dilakukan dan pembayaran sudah lunas.');
								btn.update('<img src="'+baseURL+'ui/images/icons/16x16/hijau.png"/>');
								$this.update_mrobatRWI();
								
							} else{
								$this.Button.bayar.enable();
								$this.Button.deleteHistory.disable();
								$this.Button.save.disable();
								$this.Button.save2.disable();
								$this.Button.deleted .disable();
								$this.Button.cetak.disable();
								Ext.Msg.alert('Sukses','Pembayaran berhasil dilakukan.');
								btn.update('<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>');
							}
								/* $this.Button.bayar.disable();
								$this.Button.deleted.disable();
								$this.Button.posting.enable();
								$this.Button.save.disable();
								$this.Button.save2.disable();
								$this.Button.deleteHistory.disable(); */
								
							}else{
								$this.vars.post=false;
							}
							
							$this.refreshBayar();
							$this.Window.bayar.close();
						}else{
							Ext.Msg.alert('Gagal',r.processMessage);
						}
					},
					error: function(jqXHR, exception) {
						loadMask.hide();
						Nci.ajax.ErrorMessage(jqXHR, exception);
					}
				});
				
			} 
		}, this);
	},
	getCmpInput3:function(){
		var $this=this;
		
		$this.Grid.detail = new Ext.grid.EditorGridPanel({
	        title: 'Paid History',
	        stripeRows: true,
	        store: $this.ArrayStore.gridDetail,
	        flex:2,
	        columnLines: true,
			autoScroll:true,
	        sm: new Ext.grid.CellSelectionModel({
	            singleSelect: true,
	            listeners:{
	                cellselect: function(sm, row, rec){
	                	$this.vars.selectDetail=$this.ArrayStore.gridDetail.getAt(row);
	                }
	            }
	        }),
	         columns: [
			{
				id: 'colStatPost',
				header: 'Status Posting',
				dataIndex: 'TUTUP',
				width:100,
				hidden:true
			},{
				id: 'colKdPsien',
				header: 'Kode Pasien',
				dataIndex: 'KD_PASIENAPT',
				width:100,
				hidden:true
			},{
				id: 'colNoOut',
				header: 'No out',
				dataIndex: 'NO_OUT',
				width:100,
				hidden:true
			},{
				id: 'coleurutmasuk',
				header: 'Urut Bayar',
				dataIndex: 'urut',
				align :'center',
				width:90
				
			},{
				id: 'colTGlout',
				header: 'Tanggal Resep',
				dataIndex: 'tgl_out',
				align :'center',
				width:130,
				renderer: function(v, params, record)
				{
				   return ShowDate(record.data.tgl_out);

				} 
			},{
				id: 'colePembayaran',
				header: 'Pembayaran',
				dataIndex: 'uraian',
				align :'center',
				width:120,
				hidden:false
			},{
				id: 'colTGlout',
				header: 'Tanggal Bayar',
				dataIndex: 'tgl_bayar',
				align :'center',
				width:130,
				renderer: function(v, params, record){
				   return ShowDate(record.data.tgl_bayar);
				} 
			},{
				id: 'colJumlah',
				header: 'Total Bayar',
				width:130,
				xtype:'numbercolumn',
				align :'right',
				dataIndex: 'jumlah',
				renderer: function(v, params, record){
				   return formatCurrency(record.data.jumlah);
				}
			},{
				header: 'Jumlah Angsuran',
				width:130,
				xtype:'numbercolumn',
				align :'right',
				dataIndex: 'jml_terima_uang',
				renderer: function(v, params, record){
				   return formatCurrency(record.data.jml_terima_uang);
				}
			},{
				id: 'colJumlah',
				header: 'Sisa',
				width:130,
				xtype:'numbercolumn',
				align :'right',
				dataIndex: 'sisa',
				renderer: function(v, params, record){
				   return formatCurrency(record.data.sisa);
				}
			}
		],
		/* tbar: {
				xtype: 'toolbar',
				items: [
					
				]
			}, */
			viewConfig: {forceFit: true}
		});
	    return $this.Grid.detail;
	},
	getCmpInput2:function(){
		var $this=this;
		$this.Grid.input =new Ext.grid.EditorGridPanel({
	        store: $this.ArrayStore.gridInput,
	        stripeRows: true,
	        columnLines: true,
			autoScroll:true,
	        flex:3,
			selModel: new Ext.grid.RowSelectionModel({
	            singleSelect: true,
	            listeners:{
	                rowselect: function(sm, row, rec){
	                	$this.vars.gridInputIndeks=row;
	                }
	            }
	        }),
			tbar:[
				$this.Button.deleted=new Ext.Button({
					text: 'Delete',
					iconCls: 'remove',
					handler: function(){
						if($this.vars.gridInputIndeks != null){
							if($this.ArrayStore.gridInput.getCount()>1){
								var dat=toInteger($this.NumberField.total.getValue())-toInteger($this.ArrayStore.gridInput.getAt($this.vars.gridInputIndeks).data.jumlah);
								console.log(dat);
								if(dat<$this.vars.jml_terima_uang){
									Ext.Msg.alert('Informasi','Obat tidak dapat dihapus karena tidak mencukupi.');
									return;
								}
								Ext.Msg.confirm('Konfirmasi', 'Apakah ingin menghapus obat ini?', function (id, value) { 
									if (id === 'yes') { 
										$this.ArrayStore.gridInput.removeAt($this.vars.gridInputIndeks);
										$this.Grid.input.getView().refresh();
										$this.Button.bayar.disable();
										$this.vars.gridInputIndeks=null;
									} 
								}, this); 
							}else{
								Ext.Msg.alert('Informasi','Faktur Minimal 1 Obat.');
							}
						}
					}
				}),
			],
	        columns:[
				new Ext.grid.RowNumberer(),			
				{			
					dataIndex: 'kd_prd',
					header: 'Kode',
					sortable: true,
					width: 80
				},{			
					dataIndex: 'nama_obat',
					header: 'Uraian',
					sortable: true,
					width: 250
				},{
					dataIndex: 'kd_satuan',
					header: 'Satuan',
					sortable: true,
					width: 80
				},{
					dataIndex: 'racikan',
					header: 'Racikan',
					sortable: true,
					align:'right',
					width: 70
				},{
					xtype:'numbercolumn',
					align:'right',
					dataIndex: 'harga_satuan',
					header: 'Harga Sat',
					sortable: true,
					width: 100
				},
				{
					dataIndex: 'jml_out',
					header: 'Qty Resep',
					sortable: true,
					align:'right',
					width: 80
				},{
					xtype:'numbercolumn',
					align:'right',
					dataIndex: 'qty',
					header: 'Qty',
					sortable: true,
					width: 90,
					editor:new Ext.form.NumberField({
						listeners:{
							blur: function(a){
								var line	= this.indeks;
								var b=$this.ArrayStore.gridInput.getRange()[line].data;
								b.qty=a.getValue();
								
								if(parseFloat(a.getValue()) != parseFloat(this.lastValue)){
									$this.Button.bayar.disable();
								}
								if(parseFloat(b.jml_out) < parseFloat(a.getValue())){
									Ext.Msg.alert('Warning','Qty tidak boleh melebihi qty resep');
									//b.qty=a.getValue();
									// b.qty=this.lastValue;
									// $this.getCounting();
								}
								$this.getCounting();
							},focus:function(a){
								this.indeks=$this.vars.gridInputIndeks;
								this.lastValue=a.getValue();
							}
						}
					})
				},{
					xtype:'numbercolumn',
					align:'right',
					dataIndex: 'jumlah',
					header: 'Sub Total',
					sortable: true,
					width: 130
				},{
					xtype:'numbercolumn',
					align:'right',
					dataIndex: 'reduksi',
					header: 'Reduksi',
					sortable: true,
					width: 90
				}
	        ],
				viewConfig:{
					forceFit:true
				}
	    }); 
	    return $this.Grid.input;
	},
	getCmpInput1:function(){
		var $this=this;
		var items =new Ext.Panel({
		    border:false,
		    flex:1,
		    height: 85,
		    bodyStyle:'padding: 3px;',
		    autoScroll:true,
			items:[	
				{
					layout:'column',
					border:false,
					items:[
						{
							layout:'column',
							border:false,
							bodyStyle:'margin: 2px',
							items:[
								{
									xtype: 'displayfield',
									width : 80,	
									value: 'No. Retur :'
								},
								$this.TextField.noRetur= new Ext.form.TextField({
									width : 120,	
									readOnly: true,
									emptyText: 'No Retur'
								})
								
							]
						},{
							xtype:'displayfield',
							value:'&nbsp;',
							width: 10
						},{
							layout:'column',
							border:false,
							width: 274,
							bodyStyle:'margin: 2px;',
							items:[
								{
									xtype: 'displayfield',
									width: 80,
									value: 'No Medrec :',
									fieldLabel: 'Label'
								},$this.TextField.noRm= new Ext.form.TextField({
									width : 120,	
									readOnly: true,
									emptyText: 'No RM'
								})
								
							]
						},{
							layout:'column',
							border:false,
							bodyStyle:'margin: 2px',
							items:[
								{
									xtype: 'displayfield',
									width : 80,	
									value: 'Nama Unit :'
								}
								 ,$this.TextField.namaUnit=new Ext.form.TextField({
									width : 120,	
									readOnly: true,
									emptyText: 'Nama Unit'
								})
								
							]
						}
					]
				},{
					layout:'column',
					border:false,
					items:[
						{	
							layout:'column',
							border:false,
							bodyStyle:'margin: 2px',
							items:[
								{
									xtype: 'displayfield',
									width : 80,	
									value: 'Tanggal :'
								},
								{
									xtype: 'datefield',	
									id: 'dftglresep',
									width: 120,								
									value: $this.vars.tanggal,
									format:'d/M/Y',
									fieldLabel: 'Label',
									style:{'text-align':'left','margin-left':'0px'}
								}
								
							]
						},{
							xtype:'displayfield',
							value:'&nbsp;',
							width: 10
						},{
							layout:'column',
							border:false,
							bodyStyle:'margin: 2px',
							items:[
								{
									xtype:'displayfield',
									value:'Nama Pasien :',
									width: 80
								},$this.TextField.namaPasien=new Ext.form.TextField({
									width : 180,	
									readOnly: true,
									emptyText: 'Nama'
								})
								
							]
						},{
							xtype: 'displayfield',
							value:'&nbsp;',
							width: 10
						},{
							layout:'column',
							border:false,
							bodyStyle:'margin: 2px',
							items:[
								{
									xtype:'displayfield',
									value:'No Kamar :',
									width: 80
								},$this.TextField.noKamar= new Ext.form.TextField({
									width : 60,	
									readOnly: true,
									emptyText: 'No Kamar'
								}),
								{
									xtype:'displayfield',
									value:'&nbsp;',
									width: 9
								},
								$this.TextField.unitKamar= new Ext.form.TextField({
									width : 80,	
									readOnly: true,
									emptyText: 'Unit/Kamar'
								})
							]
						}
					]
				},{
					layout:'column',
					border:false,
					items:[
						{
							layout:'column',
							border:false,
							bodyStyle:'margin: 2px',
							items:[
								{
									xtype:'displayfield',
									value:'No Resep :',
									width: 80
								},
								$this.ComboBox.noFaktur=new Nci.form.Combobox.autoComplete({
									store	: $this.ArrayStore.pasien,
									select	: function(a,b,c){
										$this.vars.data=b.data;
										$this.TextField.namaPasien.setValue(b.data.nmpasien);
										$this.TextField.noRm.setValue(b.data.kd_pasienapt);
										$this.TextField.noKamar.setValue(b.data.no_kamar);
										$this.TextField.unitKamar.setValue(b.data.nama_kamar);
										$this.TextField.JnsPasien.setValue(b.data.jenis_pasien);
										$this.TextField.namaUnit.setValue(b.data.nama_unit);
										$this.TextField.dokter.setValue(b.data.nama_dokter);
										$this.vars.kdUnitMr=b.data.kd_unit;
										$this.vars.kdKasirMr=b.data.apt_kd_kasir;
										$this.vars.noTransaksiMR=b.data.apt_no_transaksi;
										
										loadMask.show();
										$.ajax({
											type: 'POST',
											dataType:'JSON',
											url:baseURL + "index.php/apotek/functionApotekReturRWI/getDetail",
											data:{no_out: b.data.no_out,kd_customer:b.data.kd_customer,tgl_out:b.data.tgl_out},
											success: function(r){
												loadMask.hide();
												if(r.processResult=='SUCCESS'){
													$this.ArrayStore.gridInput.loadData([],false);
													for(var i=0,iLen=r.listData.detail.length; i<iLen ;i++){
														var records=[];
														var list=r.listData.detail;
														records.push(new $this.ArrayStore.gridInput.recordType());
														$this.ArrayStore.gridInput.add(records);
														$this.ArrayStore.gridInput.data.items[i].data.kd_prd=list[i].kd_prd
														$this.ArrayStore.gridInput.data.items[i].data.nama_obat=list[i].nama_obat
														$this.ArrayStore.gridInput.data.items[i].data.kd_satuan=list[i].kd_satuan
														$this.ArrayStore.gridInput.data.items[i].data.racikan=list[i].racikan
														$this.ArrayStore.gridInput.data.items[i].data.harga_satuan=list[i].harga_satuan
														$this.ArrayStore.gridInput.data.items[i].data.jml_out=list[i].jml_out;
														$this.ArrayStore.gridInput.data.items[i].data.qty=list[i].jml_out;
														$this.ArrayStore.gridInput.data.items[i].data.harga_pokok=list[i].harga_pokok;
														$this.ArrayStore.gridInput.data.items[i].data.markup=list[i].markup;
														$this.ArrayStore.gridInput.data.items[i].data.dosis=list[i].dosis;
														$this.ArrayStore.gridInput.data.items[i].data.jumlah=0;
													}
													$this.NumberField.adm.setValue(r.listData.adm);
													$this.vars.reduksi=r.listData.reduksi;
													$this.getCounting();
												}else{
													Ext.Msg.alert('Gagal',r.processMessage);
												}
											},
											error: function(jqXHR, exception) {
												loadMask.hide();
												Nci.ajax.ErrorMessage(jqXHR, exception);
											}
										});
									},
									width	: 120,
									insert	: function(o){
										console.log(o);
										return {
											no_resep        	:o.no_resep,
											nama_unit			: o.nama_unit,
											nama_dokter			: o.nama_dokter,
											nmpasien			: o.nmpasien,
											jenis_pasien		: o.jenis_pasien,
											kd_pasienapt		: o.kd_pasienapt,
											no_kamar			: o.no_kamar,
											nama_kamar			: o.nama_kamar,
											no_out				: o.no_out,
											kd_customer			: o.kd_customer,
											kd_dokter			: o.dokter,
											kd_unit				: o.kd_unit,
											kd_unit_far			: o.kd_unit_far,
											apt_kd_kasir		: o.apt_kd_kasir,
											apt_no_transaksi	: o.apt_no_transaksi,
											tgl_out				: o.tgl_out,
											text				:  '<table style="font-size: 11px;"><tr><td width="80">'+o.no_resep+'</td><td width="200">'+o.nmpasien+'</td></tr></table>'
										}
									},
									param:function(){
										return {
											tanggal:Ext.getCmp('dftglresep').getValue()
										}
									},
									url		: baseURL + "index.php/apotek/functionApotekReturRWI/getNoResepRWI",
									valueField: 'no_resep',
									displayField: 'text',
									listWidth: 280
								})
								
							]
						},{
							xtype:'displayfield',
							value:'&nbsp;',
							width: 10
						},{
							layout:'column',
							border:false,
							bodyStyle:'margin: 2px',
							items:[
								{
									xtype: 'displayfield',
									width: 80,
									value: 'Jenis Pasien :',
									fieldLabel: 'Label'
								},$this.TextField.JnsPasien= new Ext.form.TextField({
									width : 180,	
									readOnly: true,
									emptyText: 'Jns. Pasien'
								})
								
							]
						},
						{
							xtype:'displayfield',
							value:'&nbsp;',
							width: 10
						},{
							layout:'column',
							border:false,
							bodyStyle:'margin: 2px',
							items:[
								{
									xtype:'displayfield',
									value:'Dokter :',
									width: 80
								},$this.TextField.dokter=new Ext.form.TextField({
									width : 150,	
									readOnly: true,
									emptyText: 'Dokter'
								})
							]
						}
					]
				}
			]
		});
	    return items;
	},
	getCmpInput:function(){
		var $this=this;
		
		var panel = new Ext.FormPanel({
			title: '',
			anchor: '100%',
			layout:{
				type: 'vbox',
				align:'stretch'
			},
			autoScroll:true,
			labelWidth: 1,
			border: false,
			items:[
				$this.getCmpInput1(),
				$this.getCmpInput2(),
				$this.getCmpInput3(),
				new Ext.Panel({
			 		layout	: 'hbox',
			 		bodyStyle	: 'padding: 5px',
			 		border	: false,
			 		items:[
			 			new Ext.Panel({
			 				flex:1,
			 				border:false,
			 				items:[
				 				{
				 					layout:'column',
				 					border:false,
				 					items:[
				 						btn=new Ext.Panel ({
											region: 'north',
											border: false,
											html: '<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>'
										}),{
						 					xtype:'displayfield',
						 					style:{'text-align':'right','font-weight':'bold','padding-top':'3px'},
						 					value:' Post'
						 				}
				 					]
				 				},{
				 					layout:'column',
				 					border:false,
				 					items:[
				 						new Ext.form.DisplayField({
						 					style:{'text-align':'right'},
						 					width: 100,
						 					value:'Current Shift :&nbsp;'
						 				}),$this.DisplayField.shift = new Ext.form.DisplayField({
						 					value:''
						 				})
				 					]
				 				},{
				 					layout:'column',
				 					border:false,
				 					items:[
				 						new Ext.form.DisplayField({
						 					xtype:'displayfield',
						 					value:'Tanggal :&nbsp;',
						 					style:{'text-align':'right'},
						 					width: 100
						 				}),$this.DisplayField.tanggal = new Ext.form.DisplayField({
						 					xtype:'displayfield',
						 					value:new Date().getDate()+'-'+(new Date().getMonth()+1)+'-'+new Date().getFullYear()
						 				})
				 					]
				 				}
			 				]
			 			}),
			 			new Ext.Panel({
			 				flex:1,
			 				layout:'form',
			 				width: 320,
			 				border:false,
			 				items:[
				 				{
				 					layout:'column',
				 					bodyStyle:'margin: 2px',
				 					border:false,
				 					items:[
				 						{
											xtype: 'displayfield',				
											width: 140,	
											style:{'text-align':'right','padding-top':'5px'}
										},$this.NumberField.adm= new Ext.form.TextField({
								            width: 80,
								            style:{'text-align':'right'},
								            value: 0,
											hidden:true,
								            readOnly: true
								        }),{
											xtype: 'displayfield',
											style:{'text-align':'right','padding-top':'5px'},
											width: 90,								
											value: 'Total :&nbsp'
										},$this.NumberField.jumlah= new Ext.form.TextField({
								            width: 80,
								            style:{'text-align':'right'},
								            value: 0,
								            readOnly: true
								        })
				 					]
				 				},{
				 					layout:'column',
				 					bodyStyle:'margin: 2px',
				 					border:false,
				 					items:[
				 						{
											xtype: 'displayfield',	
											style:{'text-align':'right','padding-top':'5px'},
											width: 60,								
											value: 'Reduksi :&nbsp;'
										},$this.NumberField.reduksi=new Ext.form.TextField({
								            width: 80,
								            style:{'text-align':'right'},
								            value: 0,
								            readOnly: true
								        }),{
											xtype: 'displayfield',
											style:{'text-align':'right','font-weight':'bold','padding-top':'5px'},
											width: 90,								
											value: 'Grand Total :&nbsp'
											
										},$this.NumberField.total=new Ext.form.TextField({
								            width: 80,
								            style:{'text-align':'right'},
								            value: 0,
								            readOnly: true
								        })
				 					]
				 				}
			 				]
			 			})
	 		       ]
			 	})
			],
			fileUpload: true,
			tbar: {
				xtype: 'toolbar',
				items: [
					{
						xtype: 'button',
						text: 'Add New Retur',
						iconCls: 'add',
						hidden:true,
						handler: function(){
							Ext.Msg.confirm('Konfirmasi', 'Apakah Ingin Menambah Retur Baru ?', function (id, value) { 
								if (id === 'yes') { 
									$this.resetInput();
								} 
							}, this);
						}
					},{
						xtype: 'tbseparator'
					},$this.Button.save=new Ext.Button({
						text: 'Save',
						iconCls: 'save',
						handler: function(){
							$this.save();
						}
					}),{
						xtype: 'tbseparator'
					},$this.Button.save2=new Ext.Button({
						text: 'Save & Close',
						iconCls: 'saveexit',
						hidden:true,
						handler: function(){
							$this.save(function(){
								$this.Window.input.close();
							});
						}
					}),{
						xtype: 'tbseparator',
						hidden:true,
					},/* $this.Button.deleted=new Ext.Button({
						text: 'Delete',
						iconCls: 'remove',
						handler: function(){
							if($this.vars.gridInputIndeks != null){
								if($this.ArrayStore.gridInput.getCount()>1){
									var dat=toInteger($this.NumberField.total.getValue())-toInteger($this.ArrayStore.gridInput.getAt($this.vars.gridInputIndeks).data.jumlah);
									console.log(dat);
									if(dat<$this.vars.jml_terima_uang){
										Ext.Msg.alert('Informasi','Obat tidak dapat dihapus karena tidak mencukupi.');
										return;
									}
									Ext.Msg.confirm('Konfirmasi', 'Apakah ingin menghapus obat ini?', function (id, value) { 
										if (id === 'yes') { 
											$this.ArrayStore.gridInput.removeAt($this.vars.gridInputIndeks);
											$this.Grid.input.getView().refresh();
											$this.Button.bayar.disable();
											$this.vars.gridInputIndeks=null;
										} 
									}, this); 
								}else{
									Ext.Msg.alert('Informasi','Faktur Minimal 1 Obat.');
								}
							}
						}
					}),
					{
						xtype:'tbseparator'
					}, */$this.Button.bayar=new Ext.Button({
						text: 'Paid',
						iconCls: 'gantidok',
						handler: function(){
							$this.showBayar();
						}
					}),
					{
						xtype:'tbseparator'
					},$this.Button.posting=new Ext.Button({
						xtype: 'button',
						text: 'Unposting',
						iconCls: 'gantidok',
						disabled:true,
						handler: function(){
							Ext.Msg.confirm('Konfirmasi', 'Apakah Akan Unposting data ini ?', function (id, value) { 
								if (id === 'yes') { 
									loadMask.show();
									$.ajax({
										url:baseURL + "index.php/apotek/functionApotekReturRWI/unposting",
										dataType:'JSON',
										type: 'POST',
										data:{no_retur:$this.TextField.noRetur.getValue()},
										success: function(r){
											loadMask.hide();
											if(r.processResult=='SUCCESS'){
												Ext.Msg.alert('Sukses','Berhasil di Unposting.');
												$this.Button.posting.disable();
										    	$this.Button.save.disable();
										    	$this.Button.save2.disable();
										    	$this.Button.deleted.disable();
												$this.Button.cetak.disable();
										    	$this.Button.deleteHistory.enable();
										    	$this.vars.post=false;
												$this.unposting_mrobatRWI();
												btn.update('<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>');
											}else{
												Ext.Msg.alert('Gagal',r.processMessage);
											}
										},
										error: function(jqXHR, exception) {
											loadMask.hide();
											Nci.ajax.ErrorMessage(jqXHR, exception);
										}
									});
								} 
							}, this);
						}
					}),
					{
						xtype:'tbseparator'
					},
					$this.Button.deleteHistory=new Ext.Button({
						text: 'Delete History',
						iconCls: 'remove',
						handler: function(){
							if($this.vars.selectDetail != null){
								Ext.Msg.confirm('Konfirmasi', 'Apakah Ingin Hapus History Bayar ini ?', function (id, value) { 
									if (id === 'yes') { 
										$.ajax({
											url:baseURL + "index.php/apotek/functionApotekReturRWI/deleteBayar",
											dataType:'JSON',
											type: 'POST',
											data:{no_retur:$this.TextField.noRetur.getValue(),line:$this.vars.selectDetail.data.urut},
											success: function(r){
												loadMask.hide();
												if(r.processResult=='SUCCESS'){
													Ext.Msg.alert('Sukses','Pembayaran Berhasil dihapus.');
													$this.refreshBayar();
												}else{
													Ext.Msg.alert('Error',r.processMessage);
												}
											},
											error: function(jqXHR, exception) {
												loadMask.hide();
												Nci.ajax.ErrorMessage(jqXHR, exception);
											}
										});
									} 
								}, this);
							}
						}
					}),
					{
						xtype:'tbseparator'
					},
					$this.Button.cetak=new Ext.SplitButton({
						text:'Cetak',
						iconCls:'print',
						disabled:true,
						handler: function()
						{							
							
						},
						menu: new Ext.menu.Menu({
						items: [
							// these items will render as dropdown menu items when the arrow is clicked:
							{
								xtype: 'button',
								text: 'Print Bill',
								id: 'btnPrintBillReturRWI',
								handler: function()
								{
									$this.showform_print()								
								  
								}
							},
						]
						})
					}),
					{
						xtype:'tbseparator'
					}//,
					//$this.getcbopasienorder_printer_returrwi()
					
				]
			}
		});
	    return panel;
	},
	showInput:function(data){
		var $this=this;
	    $this.Window.input = new Ext.Window({
	        id: Nci.getId(),
	        title: $this.vars.title, 
	        closeAction: 'destroy',  
	        width: 900,
	        height: 555,
	        resizable:true,
			autoScroll: false,
	        border: true,
	        layout: 'fit',
	        constrain : true,    
	        iconCls: 'Studi_Lanjut',
	        modal: true,		
	        items: $this.getCmpInput(), //1
	        listeners:{
	            activate: function(){
					loadMask.show();
	                $.ajax({
						url: baseURL + "index.php/apotek/currentShiftAPT/getcurrentshift",
						//url:baseURL + "index.php/main/getcurrentshift",
						dataType:'JSON',
						data:{command:0},
						type: 'POST',
						success: function(r){
							loadMask.hide();
							$this.DisplayField.shift.setValue(r.shift);
						},
						error: function(jqXHR, exception) {
							loadMask.hide();
							Nci.ajax.ErrorMessage(jqXHR, exception);
						}
					});
	            },
	            afterShow: function(){
	                this.activate();
	                
	            },
	            deactivate: function(){
	                //rowSelected_viApotekReturRWI=undefined;
					//mNoKunjungan_viApotekReturRWI = '';
	            }
	        }
	    });
	    loadMask.show();
	    if (data == null){
	    	$.ajax({
				url:baseURL + "index.php/apotek/functionApotekReturRWI/initTransaksi",
				dataType:'JSON',
				type: 'GET',
				success: function(r){
					loadMask.hide();
					if(r.processResult=='SUCCESS'){
						$this.Window.input.show();
						$this.resetInput();
						$this.ComboBox.noFaktur.enable();
					    $this.ArrayStore.gridInput.loadData([],false);
					    $this.Grid.input.getView().refresh();
					    $this.vars.post=false;
					}else{
						Ext.Msg.alert('Error',r.processMessage);
					}
				},
				error: function(jqXHR, exception) {
					loadMask.hide();
					Nci.ajax.ErrorMessage(jqXHR, exception);
				}
			});
	    }else{
	    	$.ajax({
	 			url:baseURL + "index.php/apotek/functionApotekReturRWI/getForEdit",
	 			dataType:'JSON',
	 			type: 'POST',
	 			data: {'no_retur':data.data.no_resep},
	 			success: function(r){
	 				loadMask.hide();
	 				if(r.processResult=='SUCCESS'){
	 					$this.Window.input.show();
	 					$this.resetInput();
	 					$this.ArrayStore.gridInput.loadData([],false);
					    $this.Grid.input.getView().refresh();
					    var o=r.listData.object;
					    $this.ComboBox.noFaktur.setValue(o.no_bukti);
					    $this.TextField.noRetur.setValue(o.no_resep);
					    $this.TextField.namaPasien.setValue(o.nmpasien);
					    $this.TextField.noRm.setValue(o.kd_pasienapt);
					    $this.TextField.JnsPasien.setValue(o.jenis_pasien);
					    $this.TextField.dokter.setValue(o.nama);
						$this.vars.kdUnitMr=o.kd_unit;
						$this.vars.kdKasirMr=o.apt_kd_kasir;
						$this.vars.noTransaksiMR=o.apt_no_transaksi;
					    $this.vars.post=false;
						$this.vars.noOut=o.no_out;
						$this.vars.tglOut=o.tgl_out;
					    if(o.tutup==1){
					    	$this.Button.posting.enable();
					    	$this.vars.post=true;
					    	$this.Button.save.disable();
					    	$this.Button.save2.disable();
					    	$this.Button.deleted.disable();
							$this.Button.cetak.enable();
							btn.update('<img src="'+baseURL+'ui/images/icons/16x16/hijau.png"/>');
					    } else{
							$this.Button.save.disable();
					    	$this.Button.save2.disable();
					    	$this.Button.deleted.disable();
							$this.Button.cetak.disable();
						}
					   
					    $this.TextField.noKamar.setValue(o.no_kamar);
					    $this.TextField.unitKamar.setValue(o.nama_kamar);
					    $this.TextField.namaUnit.setValue(o.nama_unit);
					    $this.NumberField.adm.setValue(o.admresep);
					    $this.NumberField.reduksi.setValue(o.discount);
						$this.vars.reduksi=r.listData.reduksi;
						console.log(r.listData.reduksi);
						
					    $this.ArrayStore.gridInput.loadData([],false);
	 					var recs = [],
	 					recordType=$this.ArrayStore.gridInput.recordType;
	 					for(var i=0, iLen=r.listData.detail.length ; i<iLen ; i++){
	 						recs.push(new recordType(r.listData.detail[i]));
							console.log(r.listData.detail[i].reduksi)
	 					}
	 					$this.ArrayStore.gridInput.add(recs);
	 					$this.Grid.detail.getView().refresh();
	 					$this.getCounting();
	 					$this.ComboBox.noFaktur.disable();
	 					$this.Button.bayar.enable();
	 					$this.refreshBayar();
	 				}else{
	 					Ext.Msg.alert('Error',r.processMessage);
	 				}
	 			},
	 			error: function(jqXHR, exception) {
	 				loadMask.hide();
	 				Nci.ajax.ErrorMessage(jqXHR, exception);
	 			}
	 		});
	    	console.log(data);
	    }
	},
	getComboboxUnit:function(){
		var $this=this;
		$.ajax({
			url:baseURL + "index.php/apotek/functionApotekReturRWI/getUnitCombobox",
			dataType:'JSON',
			type: 'GET',
			success: function(r){
				loadMask.hide();
				if(r.processResult=='SUCCESS'){
					$this.ArrayStore.comboMain.loadData([],false);
					for(var i=0,iLen=r.listData.length; i<iLen ;i++){
						var records=[];
						records.push(new $this.ArrayStore.comboMain.recordType(r.listData[i]));
						$this.ArrayStore.comboMain.add(records);
					}
				}else{
					Ext.Msg.alert('Error',r.processMessage);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
	    $this.ComboBox.unit = new Ext.form.ComboBox({
			x:130,
			y:60,
	        flex: 1,
	        fieldLabel: 'Poliklinik',
			valueField: 'kd_unit',
	        displayField: 'nama_unit',
			emptyText:'Unit/Ruang',
			store: $this.ArrayStore.comboMain,
	        mode: 'local',
	        typeAhead: true,
	        triggerAction: 'all',
	        lazyRender: true,
			width:150,
			tabIndex:2,
			listeners:{ 
				'select': function(a,b,c){
				},
				'specialkey' : function(){
					if (Ext.EventObject.getKey() === 13) {
						$this.refresh();
					} 						
				}
			}
	    });    
	    return $this.ComboBox.unit;
	},
	getComboboxStatus:function(){
		var $this=this;
		$this.ComboBox.posting=new Ext.form.ComboBox({
	        x: 410,
	        y: 30,
	        typeAhead: true,
	        triggerAction: 'all',
	        lazyRender:true,
	        mode: 'local',
	        width: 110,
	        emptyText:'',
	        fieldLabel: 'JENIS',
			tabIndex:5,
	        store: new Ext.data.ArrayStore({
	            id: 0,
	            fields:['Id','displayText'],
	        	data: [[1, 'Semua'],[2, 'Posting'], [3, 'Belum Posting']]
	        }),
	        valueField: 'displayText',
	        displayField: 'displayText',
	        value:'Belum Posting',
	        listeners:{
				'select': function(a,b,c){
//					selectCountStatusPostingApotekReturRWI=b.data.displayText ;
					$this.refresh();
				},
				listeners:{ 
					'select': function(a,b,c){
					},
					'specialkey' : function(){
						if (Ext.EventObject.getKey() === 13) {
							$this.refresh();
						} 						
					}
				}

	        }
		});
		return $this.ComboBox.posting;
	},
	 ShowPesanWarningReturRWI:function(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
},

ShowPesanErrorReturRWI: function(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:290
		}
	);
},


ShowPesanInfoReturRWI:function(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
},
	main:function(mod_id){
		var $this=this;
		
		$this.Grid.main=new Ext.grid.EditorGridPanel({
			xtype: 'editorgrid',
			title: '',
			flex:1,
			store: $this.ArrayStore.main,
			autoScroll: true,
			columnLines: true,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel({
				singleSelect: true,
				listeners:{
					rowselect: function(sm, row, rec){
						$this.vars.selectData = $this.ArrayStore.main.getAt(row);
						if ($this.vars.selectData.data.tutup==='0')
						{
							Ext.getCmp('btnHapusTrx_viApotekReturRWI').enable();
						}
						else
						{
							Ext.getCmp('btnHapusTrx_viApotekReturRWI').disable();
						}
					}
				}
			}),
			listeners:{
				rowdblclick: function (sm, row, cidx){
					$this.vars.selectData = $this.ArrayStore.main.getAt(row);
					$this.showInput($this.vars.selectData);
				}
			},
			colModel: new Ext.grid.ColumnModel([
				new Ext.grid.RowNumberer(),
				{
					header		: 'Status Posting',
					width		: 20,
					sortable	: false,
					hideable	: true,
					hidden		: false,
					menuDisabled: true,
					dataIndex	: 'tutup',
					renderer	: function(value, metaData, record, rowIndex, colIndex, store){
						 switch (value){
							 case '1':
								 metaData.css = 'StatusHijau'; 
								 break;
							 case '0':
								 metaData.css = 'StatusMerah';
								 break;
						 }
						 return '';
					}
				},{
					header: 'No. Resep',
					dataIndex: 'no_resep',
					sortable: true,
					width: 35
					
				},{
					header:'No out',
					dataIndex: 'no_out',						
					width: 30,
					sortable: true,
					hideable:false,
					hidden: true,
	                menuDisabled:true
				},{
					header:'Tgl Resep',
					dataIndex: 'tgl_out',						
					width: 30,
					sortable: true,
					hideable:false,
	                menuDisabled:true
				},{
					header: 'No Medrec',
					dataIndex: 'kd_pasienapt',
					sortable: true,
					hideable:false,
	                menuDisabled:true,
					width: 30
				},{
					header: 'Nama Pasien',
					dataIndex: 'nmpasien',
					sortable: true,
					width: 50
				},{
					header: 'Ruangan',
					dataIndex: 'no_kamar',
					sortable: true,
					width: 40
				}
			]),
			tbar:{
				xtype: 'toolbar',
				items:[
					{
						xtype: 'button',
						text: 'Add Data',
						iconCls: 'add',
						tooltip: 'Add Data',
						handler: function(sm, row, rec){
							$this.showInput();
						}
					},{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						handler: function(sm, row, rec){
							if($this.vars.selectData != null){
								$this.showInput($this.vars.selectData);
							}
						}
					},
					{
						xtype: 'button',
						text: 'Hapus Transaksi',
						iconCls: 'remove',
						tooltip: 'Hapus Data',
						disabled:true,
						id: 'btnHapusTrx_viApotekReturRWI',
						handler: function(sm, row, rec)
						{
							var datanya=$this.vars.selectData.data;
							if (datanya===undefined){
								$this.ShowPesanWarningReturRWI('Belum ada data yang dipilih','Resep RWI');
							}
							else
							{
								 Ext.Msg.show({
									title: 'Hapus Transaksi',
									msg: 'Anda yakin akan menghapus data transaksi ini ?',
									buttons: Ext.MessageBox.YESNO,
									fn: function (btn) {
										if (btn == 'yes')
										{
											
											console.log(datanya);
											var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan Pembatalan Transaksi:', function (btn, combo) {
														if (btn == 'ok')
														{
															var variablebatalhistori_rwi = combo;
															if (variablebatalhistori_rwi != '')
															{
																 Ext.Ajax.request({
										   
																		url: baseURL + "index.php/apotek/functionAPOTEK/hapusTrxResep",
																		 params: {
																			
																			noout: datanya.no_out,
																			tglout: datanya.tgl_out,
																			jenis:'RETUR',
																			apaini:"returrwi",
																			alasan: variablebatalhistori_rwi
																		
																		},
																		failure: function(o)
																		{
																			 var cst = Ext.decode(o.responseText);
																			$this.ShowPesanErrorReturRWI('Data transaksi tidak dapat dihapus','Retur RWI');
																		},	    
																		success: function(o) {
																			var cst = Ext.decode(o.responseText);
																			if (cst.success===true)
																			{
																				$this.refresh();
																				$this.ShowPesanInfoReturRWI('Data transaksi Berhasil dihapus','Retur RWI');
																				Ext.getCmp('btnHapusTrx_viApotekReturRWI').disable();
																			}
																			else
																			{
																				$this.ShowPesanErrorReturRWI('Data transaksi tidak dapat dihapus','Retur RWI');
																			}
																			
																		}
																
																})
															} else
															{
																$this.ShowPesanWarningReturRWI('Silahkan isi alasan terlebih dahaulu', 'Keterangan');

															}
														}

													}); 
											
										}
									},
									icon: Ext.MessageBox.QUESTION
								});
							} 
						}
					},
				]
			},
			viewConfig: {
				forceFit: true
			}
		});
		$this.Panel.search=new Ext.Panel({
	        labelAlign: 'top',
	        title: '',
			frame:true,
	        bodyStyle:'padding:5px 5px 0',
	        items: [
			{
				layout: 'column',
				border: false,
				items:[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 90,
						anchor: '100% 100%',
						items:[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'No. Retur'
							},{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},$this.TextField.srchNoRetur=new Ext.form.TextField({
								x: 130,
								y: 0,
								emptyText: 'No. Retur',
								width: 130,
								tabIndex:1,
								listeners:{ 
									'specialkey' : function(){
										if (Ext.EventObject.getKey() === 13){
											$this.refresh();
										} 						
									}
								}
							}),{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'Kode/Nama Pasien'
							},{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},$this.TextField.srchKodePasien=new Ext.form.TextField({
								x: 130,
								y: 30,
								emptyText: 'Kode/Nama Pasien',
								width: 130,
								tabIndex:1,
								listeners:{ 
									'specialkey' : function(){
										if (Ext.EventObject.getKey() === 13){
											$this.refresh();
										} 						
									}
								}
							}),{
								x: 10,
								y: 60,
								xtype: 'label',
								text: 'Ruangan'
							},{
								x: 120,
								y: 60,
								xtype: 'label',
								text: ':'
							},$this.getComboboxUnit(),	
							{
								x: 310,
								y: 0,
								xtype: 'label',
								text: 'Tanggal Resep '
							},{
								x: 400,
								y: 0,
								xtype: 'label',
								text: ':'
							},$this.DateField.srchStartDate= new Ext.form.DateField({
								x: 410,
								y: 0,
								format: 'd/M/Y',
								width: 120,
								tabIndex:3,
								value:new Date(),
								listeners:{ 
									'select': function(a,b,c){
									},
									'specialkey' : function(){
										if (Ext.EventObject.getKey() === 13) {
											$this.refresh();
										} 						
									}
								}
							}),{
								x: 540,
								y: 0,
								xtype: 'label',
								text: 's/d'
							},$this.DateField.srchEndDate=new Ext.form.DateField({
								x: 568,
								y: 0,
								format: 'd/M/Y',
								width: 120,
								tabIndex:4,
								value:new Date(),
								listeners:{ 
									'select': function(a,b,c){
									},
									'specialkey' : function(){
										if (Ext.EventObject.getKey() === 13) {
											$this.refresh();
										} 						
									}
								}
							}),{
								x: 310,
								y: 30,
								xtype: 'label',
								text: 'Posting'
							},{
								x: 400,
								y: 30,
								xtype: 'label',
								text: ':'
							},$this.getComboboxStatus(),
							{
								x: 568,
								y: 60,
								xtype: 'button',
								text: 'Cari',
								iconCls: 'refresh',
								tooltip: 'Cari',
								style:{paddingLeft:'30px'},
								width:150,
								id: 'BtnFilterGridDataView_viApotekReturRWI',
								handler: function(){	
									$this.refresh();
								}                        
							}
						]
					}
				]
			}
			]	
		});
	    $this.Panel.main = new Ext.Panel({
			title: $this.vars.title,
			iconCls: 'Studi_Lanjut',
			id: mod_id,
			layout: {
				type:'vbox',
				align:'stretch'
			}, 
			closable: true,        
			border: false,  
			margins: '0 5 5 0',
			items: [$this.Panel.search,$this.Grid.main],
			tbar:[
				{
		            xtype: 'buttongroup',
		            columns: 21,
		            defaults: {
						scale: 'small'
	        		},
					frame: false,
	            	items:[]
				}
			]
	   	});
	    return $this.Panel.main;
	},
	init:function(){
		var $this=this;
		CurrentPage.page = $this.main(CurrentPage.id);
		mainPage.add(CurrentPage.page);
		mainPage.setActiveTab(CurrentPage.id);
		$this.refresh();
	},
	update_mrobatRWI:function(){
		var $this=this;
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionInsertMrObat/retur_mrobat",
				params: $this.getParamUpdateMrObatRWI(),
				failure: function(o)
				{
					Ext.Msg.alert('Error','Error update MR! Hubungi Admin');
				},	
				success: function(o) 
				{
					
				}
			}
			
		)
	},
	getParamUpdateMrObatRWI:function(){
		var $this=this;
		var params =
		{
			kd_pasien: $this.TextField.noRm.getValue(),
			kd_unit: $this.vars.kdUnitMr,
			kd_kasir: $this.vars.kdKasirMr,
			no_transaksi: $this.vars.noTransaksiMR
		}
		
		params['jumlah']=$this.ArrayStore.gridInput.getCount();
		for(var i = 0 ; i < $this.ArrayStore.gridInput.getCount();i++)
		{
			var a=$this.ArrayStore.gridInput.getRange()[i].data;
			params['kd_prd-'+i]=a.kd_prd
			params['tgl_out-'+i]=a.tgl_out
			params['qty-'+i]=a.qty
			params['reduksi-'+i]=a.reduksi
		}
		
		return params
	},
	unposting_mrobatRWI:function(){
		var $this=this;
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionInsertMrObat/retur_unpost_mrobat",
				params: $this.getParamUnpostingMrObatRWI(),
				failure: function(o)
				{
					Ext.Msg.alert('Error','Error update MR! Hubungi Admin');
				},	
				success: function(o) 
				{
					
				}
			}
			
		)
	},
	getParamUnpostingMrObatRWI:function(){
		var $this=this;
		var params =
		{
			kd_pasien: $this.TextField.noRm.getValue(),
			kd_unit: $this.vars.kdUnitMr,
			kd_kasir: $this.vars.kdKasirMr,
			no_transaksi: $this.vars.noTransaksiMR
		}
		
		params['jumlah']=$this.ArrayStore.gridInput.getCount();
		for(var i = 0 ; i < $this.ArrayStore.gridInput.getCount();i++)
		{
			var a=$this.ArrayStore.gridInput.getRange()[i].data;
			params['kd_prd-'+i]=a.kd_prd
			params['tgl_out-'+i]=a.tgl_out
			params['qty-'+i]=a.qty
			params['reduksi-'+i]=a.reduksi
		}
		
		return params
	}
}
AprotekReturInap.init();