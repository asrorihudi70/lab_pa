// <reference path="file://D:/projects_net35/rnd/extjs/ext-3.0.0/ext-all.js" />
// <reference path="file://D:/projects_net35/rnd/extjs/ext-3.0.0/adapter/ext/ext-base.js" />
// revisi : 2009-10-18
/*!
 * Ext JS Library 3.0.0
 * Copyright(c) 2006-2009 Ext JS, LLC
 * licensing@extjs.com
 * http://www.extjs.com/license
 */
/**
 * Ext.App
 * @extends Ext.util.Observable
 * @author Chris Scott
 */
Ext.namespace('WebApp');

Ext.App = function(config) {
  //
    Ext.Direct.addProvider(WebApp.REMOTING_API);
    // set up StateProvider
    this.initStateProvider();

    // array of views
    this.views = [];
  

    Ext.Direct.on('exception', function(e) {
        Ext.Msg.alert('Direct Exception', e.message);
    });
    Ext.apply(this, config);
    if (!this.api.actions) { this.api.actions = {}; }

    // init when onReady fires.
    Ext.onReady(this.onReady, this);

    Ext.App.superclass.constructor.apply(this, arguments);
};
Ext.extend(Ext.App, Ext.util.Observable, {

    /***
     * response status codes.
     */
    STATUS_EXCEPTION :          'exception',
    STATUS_VALIDATION_ERROR :   "validation",
    STATUS_ERROR:               "error",
    STATUS_NOTICE:              "notice",
    STATUS_OK:                  "ok",
    STATUS_HELP:                "help",

//    /**
//     * @cfg {Object} api
//     * remoting api.  should be defined in your own config js.
//     */
//    api: {
//        url: null,
//        type: null,
//        actions: {}
//    },

    // private, ref to message-box Element.
    msgCt : null,

    // @protected, onReady, executes when Ext.onReady fires.
    onReady : function() {
        // create the msgBox container.  used for App.setAlert
        this.msgCt = Ext.DomHelper.insertFirst(document.body, {id:'msg-div'}, true);
        this.msgCt.setStyle('position', 'absolute');
        this.msgCt.setStyle('z-index', 9999);
        this.msgCt.setWidth(300);
    },

    initStateProvider : function() {
        /*
         * set days to be however long you think cookies should last
         */
        var days = '';        // expires when browser closes
        if(days){
            var date = new Date();
            date.setTime(date.getTime()+(days*24*60*60*1000));
            var exptime = "; expires="+date.toGMTString();
        } else {
            var exptime = null;
        }

        // register provider with state manager.
        Ext.state.Manager.setProvider(new Ext.state.CookieProvider({
            path: '/',
            expires: exptime,
            domain: null,
            secure: false
        }));
    },

    /**
     * registerView
     * register an application view component.
     * @param {Object} view
     */
    registerView : function(view) {
        this.views.push(view);
    },

    /**
     * getViews
     * return list of registered views
     */
    getViews : function() {
        return this.views;
    },

    /**
     * registerActions
     * registers new actions for API
     * @param {Object} actions
     */
    registerActions : function(actions) {
        Ext.apply(this.api.actions, actions);
    },

    /**
     * getAPI
     * return Ext Remoting api
     */
    getAPI : function() {
        return this.api;
    },

    /***
     * setAlert
     * show the message box.  Aliased to addMessage
     * @param {String} msg
     * @param {Bool} status
     */
    setAlert : function(status, msg) {
        this.addMessage(status, msg);
    },

    /***
     * adds a message to queue.
     * @param {String} msg
     * @param {Bool} status
     */
    addMessage : function(status, msg) {
        var delay = 3;    // <-- default delay of msg box is 1 second.
        if (status == false) {
            delay = 5;    // <-- when status is error, msg box delay is 3 seconds.
        }
        // add some smarts to msg's duration (div by 13.3 between 3 & 9 seconds)
        delay = msg.length / 13.3;
        if (delay < 3) {
            delay = 3;
        }
        else if (delay > 9) {
            delay = 9;
        }

        this.msgCt.alignTo(document, 't-t');
        Ext.DomHelper.append(this.msgCt, {html:this.buildMessageBox(status, String.format.apply(String, Array.prototype.slice.call(arguments, 1)))}, true).slideIn('t').pause(delay).ghost("t", {remove:true});
    },

    /***
     * buildMessageBox
     */
    buildMessageBox : function(title, msg) {
        switch (title) {
            case true:
                title = this.STATUS_OK;
                break;
            case false:
                title = this.STATUS_ERROR;
                break;
        }
        return [
            '<div class="app-msg">',
            '<div class="x-box-tl"><div class="x-box-tr"><div class="x-box-tc"></div></div></div>',
            '<div class="x-box-ml"><div class="x-box-mr"><div class="x-box-mc"><h3 class="x-icon-text icon-status-' + title + '">', title, '</h3>', msg, '</div></div></div>',
            '<div class="x-box-bl"><div class="x-box-br"><div class="x-box-bc"></div></div></div>',
            '</div>'
        ].join('');
    },

    /**
     * decodeStatusIcon
     * @param {Object} status
     */
    decodeStatusIcon : function(status) {
        iconCls = '';
        switch (status) {
            case true:
            case this.STATUS_OK:
                iconCls = this.ICON_OK;
                break;
            case this.STATUS_NOTICE:
                iconCls = this.ICON_NOTICE;
                break;
            case false:
            case this.STATUS_ERROR:
                iconCls = this.ICON_ERROR;
                break;
            case this.STATUS_HELP:
                iconCls = this.ICON_HELP;
                break;
        }
        return iconCls;
    },

    /***
     * setViewState, alias for Ext.state.Manager.set
     * @param {Object} key
     * @param {Object} value
     */
    setViewState : function(key, value) {
        Ext.state.Manager.set(key, value);
    },

    /***
     * getViewState, aliaz for Ext.state.Manager.get
     * @param {Object} cmd
     */
    getViewState : function(key) {
        return Ext.state.Manager.get(key);
    },

    /**
     * t
     * translation function.  needs to be implemented.  simply echos supplied word back currently.
     * @param {String} to translate
     * @return {String} translated.
     */
    t : function(words) {
        return words;
    },

    handleResponse : function(res) {
        if (res.type == this.STATUS_EXCEPTION) {
            return this.handleException(res);
        }
        if (res.message.length > 0) {
            this.setAlert(res.status, res.message);
        }
    },

    handleException : function(res) {
        Ext.MessageBox.alert(res.type.toUpperCase(), res.message);
    }
});


/**
 * Modified Pagging Bar to acomodate supplied WebApp.DataStore
 * @extends Ext.PagingToolbar
 * @author Fully.S.A
 */
WebApp.PaggingBar = function(config) {
    WebApp.PaggingBar.superclass.constructor.call(this, Ext.apply({
        paramNames: {
            "start": "Skip",
            "limit": "Take",
            "sort": "Sort",
            "dir": "SortDir",
            "target": "target",
            "param": "param"
        },
		 // private
    doLoad : function(start){
         var o = {}, pn = this.getParams();
		// //alert("pagging " + this.store.lastOptions.params.target + " from " + start + " to " +this.pageSize);
		o[pn.start] = start;
		o[pn.limit] = this.pageSize;
		o[pn.sort] = this.store.lastOptions.params.Sort;
		o[pn.dir] = this.store.lastOptions.params.SortDir;
		o[pn.target] =  this.store.lastOptions.params.target;
		o[pn.param] =  this.store.lastOptions.params.param;
		// o["SID"] =  Math.random();
        if(this.fireEvent('beforechange', this, o) !== false){
		  //this.store.load({params: o});
			//alert("pagging " + this.store.lastOptions.params.target + " from " + start + " to " +this.pageSize);
            this.store.load({params: 
			{
				Skip: start,
				Take:  this.pageSize,
				Sort: '',
				Sortdir: this.store.lastOptions.params.dir,
				target:this.store.lastOptions.params.target,
				param: this.store.lastOptions.params.param
			}
			});
			
        }
	}

    }, config));
};
Ext.extend(WebApp.PaggingBar, Ext.PagingToolbar, {});

var WebAppUrl={
	UrlReadData:baseURL + "index.php/main/ReadDataObj",
	UrlSaveData:baseURL + "index.php/main/CreateDataObj",
	UrlUpdateData:baseURL + "index.php/main/CreateDataObj",
	UrlDeleteData:baseURL + "index.php/main/DeleteDataObj",
	UrlExecReport:baseURL + "index.php/main/ExecReport.",
	UrlExecProcess:baseURL + "index.php/main/ExecProc",
	UrlGetTrustee:baseURL + "index.php/main/getTrustee",
	UrlGetModule:baseURL + "index.php/main/getModule",
	UrlGetLanguage:baseURL + "index.php/main/getLanguage",
	UrlGetReport:baseURL + "index.php/main/getReport",
	UrlChartSWF:baseURL + 'ui/resources/charts.swf',
	UrlChartSWF:baseURL + 'ui/resources/charts.swf',
	UrlImg:baseURL + 'ui/',
    UrlLogOff:baseURL + "index.php/main/Logoff",
	UrlMenu:baseURL + "index.php/main/menu",
};

// proxy for data all

WebApp.DataProxy = function(config) {
    WebApp.DataProxy.superclass.constructor.call(this, Ext.apply({
        api: {
            //read: './Datapool.mvc/ReadDataObj',
            read: baseURL + "index.php/main/ReadDataObj",
            create: 'CreateCustomers',
            update: 'UpdateCustomers',
            destroy: 'DeleteCustomers'
        }
    }, config));
};
Ext.extend(WebApp.DataProxy, Ext.data.HttpProxy, {});


WebApp.DataStore = function(config) {
	WebApp.DataStore.superclass.constructor.call(this, Ext.apply({
        paramNames: {
            "start": "Skip",
            "limit": "Take",
            "sort": "Sort",
            "dir": "SortDir",
            "target": "target",
            "param": "params"
        },
        proxy: new WebApp.DataProxy(),
//        reader: Customersreader,
//        writer: Customerswriter,  // <-- plug a DataWriter into the store just as you would a Reader
        autoSave: true,
        listeners: {
            write: function(store, action, result, res, rs) {
                App.setAlert(res.success, res.message); // <-- show user-feedback for all write actions
            },
            exception: function(proxy, type, action, options, res, arg) {
                if (type === 'remote') {
                    Ext.Msg.show({
                        title: 'REMOTE EXCEPTION',
                        msg: res.message,
                        icon: Ext.MessageBox.ERROR,
                        buttons: Ext.Msg.OK
                    });
                }
            }
        },
        totalProperty: 'totalrecords',
        root: 'ListDataObj',
        remoteSort: true,
        autoLoad: false ,
        restful: true,
		timeout:100000000,
        //id: "KD_CUSTOMER",
        method: "POST"
    }, config));
};

Ext.extend(WebApp.DataStore, Ext.data.JsonStore, {});