var dataSource_viSetupTarifCustomer;
var selectCount_viSetupTarifCustomer=50;
var NamaForm_viSetupTarifCustomer="Setup Tarif Customer";
var selectCountStatusPostingSetupTarifCustomer='Semua';
var mod_name_viSetupTarifCustomer="viSetupTarifCustomer";
var now_viSetupTarifCustomer= new Date();
var addNew_viSetupTarifCustomer;
var rowSelected_viSetupTarifCustomer;
var setLookUps_viSetupTarifCustomer;
var tanggal = now_viSetupTarifCustomer.format("d/M/Y");
var jam = now_viSetupTarifCustomer.format("H/i/s");
var tampungshiftsekarang;
var tmpkriteria;
var DataGridCustomer;
var DataGridTarifCustomer;
var DataGridMasterCustomer;
var titleCustomer='Customer';


var CurrentData_viSetupTarifCustomer =
{
	data: Object,
	details: Array,
	row: 0
};



var SetupTarifCustomer={};
SetupTarifCustomer.form={};
SetupTarifCustomer.func={};
SetupTarifCustomer.vars={};
SetupTarifCustomer.func.parent=SetupTarifCustomer;
SetupTarifCustomer.form.ArrayStore={};
SetupTarifCustomer.form.ComboBox={};
SetupTarifCustomer.form.DataStore={};
SetupTarifCustomer.form.Record={};
SetupTarifCustomer.form.Form={};
SetupTarifCustomer.form.Grid={};
SetupTarifCustomer.form.Panel={};
SetupTarifCustomer.form.TextField={};
SetupTarifCustomer.form.Button={};

SetupTarifCustomer.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_gol', 'ket_gol'],
	data: []
});

SetupTarifCustomer.form.ArrayStore.data1=new Ext.data.ArrayStore({fields:[]});
SetupTarifCustomer.form.ArrayStore.data2=new Ext.data.ArrayStore({fields:[]});

CurrentPage.page = data_viSetupTarifCustomer(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function data_viSetupTarifCustomer(mod_id_viSetupTarifCustomer){
getTarifCust_viSetupTarifCustomer();
getMasterCustomer_viSetupTarifCustomer();
   	var panelSetupTarifCustomer = new Ext.FormPanel({
        labelAlign: 'top',
        title: 'Setup Kode Tarif',
        bodyStyle:'padding:5px 5px 0',
		border: false,
        items: [
		{
			layout: 'hbox',
			border: false,
			items:
			[
				{
					layout: 'absolute',
					title:'',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: true,
					width: 450,
					height: 80,
					anchor: '100% 100%',
					items:
					[
						{
							x: 10,
							y: 10,
							xtype: 'label',
							text: 'Kode tarif'
						},
						{
							x: 120,
							y: 10,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 10,
							xtype: 'textfield',
							id: 'TxtKdTarifSetupTarifCust',
							name: 'TxtKdTarifSetupTarifCust',
							readOnly: true,
							emptyText: '',
							width: 100,
							tabIndex:1
						},
						//----------------------------------------
						{
							x: 10,
							y: 40,
							xtype: 'label',
							text: 'Jenis tarif'
						},
						{
							x: 120,
							y: 40,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 40,
							xtype: 'textfield',
							id: 'TxtJenisTarifSetupTarifCust',
							name: 'TxtJenisTarifSetupTarifCust',
							emptyText: '',
							width: 200,
							tabIndex:2
						}
					]
				}
			]
		},
		/* {
			layout: 'column',
			border: false,
			items:
			[
				{
					layout: 'absolute',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					width: 50,
					height: 5,
					anchor: '100% 100%'
				}
			]
		}, */
		{
			layout: 'hbox',
			title:'List data',
			align:'stretch',
			border: false,
			items:
			[
				{
					layout: 'absolute',
					title:'Tarif',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: true,
					width: 250,
					height: 295,
					anchor: '100% 100%',
					items:
					[
						gridTarif_viSetupTarifCustomer()
					]
				},
				{
					layout: 'absolute',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					width: 20,
					height: 295,
					anchor: '100% 100%'
				},
				{
					layout: 'absolute',
					title:titleCustomer,
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: true,
					width: 270,
					height: 295,
					anchor: '100% 100%',
					items:
					[
						gridCustomer_viSetupTarifCustomer()
					]
				},
				{
					layout: 'absolute',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					width: 20,
					height: 295,
					anchor: '100% 100%'
				},
				{
					layout: 'absolute',
					title:'Master Customer',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: true,
					width: 370,
					height: 295,
					anchor: '100% 100%',
					items:
					[
						gridMasterCustomer_viSetupTarifCustomer()
					]
				},{
					layout: 'absolute',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					width: 20,
					height: 295,
					anchor: '100% 100%'
				},
				{
                                       xtype: 'checkbox',
                                       id: 'CekLapPilihSemuaSetupTarifCustApotek',
                                       hideLabel:false,
                                       boxLabel: 'Pilih Semua',
                                       checked: false,
                                       listeners: 
                                       {
                                            check: function()
                                            {
                                               if(Ext.getCmp('CekLapPilihSemuaSetupTarifCustApotek').getValue()===true)
                                                {
                                                     DataGridMasterCustomer.getSelectionModel().selectAll();
                                                }
                                                else
                                                {
                                                    DataGridMasterCustomer.getSelectionModel().clearSelections();
                                                }
                                            }
                                       }
                                    },
			],
			tbar: 
			{
				xtype: 'toolbar',
				border:true,
				items: 
				[
					{
						xtype: 'button',
						text: 'Save Customer',
						iconCls: 'save',
						id: 'btnSimpanCustomer_viSetupTarifCustomer',
						handler: function()
						{
							loadMask.show();
							dataSaveCustomer_viSetupTarifCustomer();
						}
					},
					{
						xtype: 'tbseparator'
					}
					
				]
			
			},
		
		}
		
		]	
		
	})

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viSetupTarifCustomer = new Ext.Panel
    (
		{
			title: NamaForm_viSetupTarifCustomer,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viSetupTarifCustomer,
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ panelSetupTarifCustomer],
			tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAddJenis_viSetupTarifCustomer',
						handler: function(){
							dataAddNewTarifCustomer();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viSetupTarifCustomer',
						handler: function()
						{
							loadMask.show();
							dataSave_viSetupTarifCustomer();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viSetupTarifCustomer',
						handler: function()
						{
							Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
								if (button == 'yes'){
									loadMask.show();
									dataDelete_viSetupTarifCustomer();
								}
							});
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_viSetupTarifCustomer',
						handler: function()
						{
							dsDataGrdTarifCustomer_viSetupTarifCustomer.removeAll();
							getTarifCust_viSetupTarifCustomer();
							SetupTarifCustomer.form.ArrayStore.data1.removeAll();
							SetupTarifCustomer.form.ArrayStore.data2.removeAll();
							getMasterCustomer_viSetupTarifCustomer();
							
						}
					},
					{
						xtype: 'tbseparator'
					}
					
				]
			
			}
       }
    )
    return FrmFilterGridDataView_viSetupTarifCustomer;
    //-------------- # End form filter # --------------
}

function gridTarif_viSetupTarifCustomer(){
    var FieldGrdTransaksi_viSetupTarifCustomer = ['kd_gol', 'ket_gol'];
    
	dsDataGrdTarifCustomer_viSetupTarifCustomer= new WebApp.DataStore({
		fields: FieldGrdTransaksi_viSetupTarifCustomer
    });   
   
    DataGridTarifCustomer =new Ext.grid.GridPanel({
       xtype: 'editorgrid',
		stripeRows       : true,
		autoScroll       : true,
		store: dsDataGrdTarifCustomer_viSetupTarifCustomer,
        height: 270,
		width:248,
        columnLines: true,
		border: false,
		trackMouseOver   : true,
		enableDragDrop   : true,
		flex:1,
		singleSelect: true,
		listeners:
		{
			// Function saat ada event double klik maka akan muncul form view # --------------
			rowdblclick: function (sm, ridx, cidx)
			{
				rowSelectedTarifCustomer_viSetupObatAlkes = dsDataGrdTarifCustomer_viSetupTarifCustomer.getAt(ridx);
				if (rowSelectedTarifCustomer_viSetupObatAlkes != undefined)
				{
					datainitTarifCustomer(rowSelectedTarifCustomer_viSetupObatAlkes.data);
				}
			}
			// End Function # --------------
		},
		selModel: new Ext.grid.CellSelectionModel ({
            singleSelect: true,
            listeners:{
                rowselect: function(sm, row, rec){
					
                }
            }
        }),
        stripeRows: true,
        cm:new Ext.grid.ColumnModel([
        	new Ext.grid.RowNumberer(),	
			{
				dataIndex: 'kd_gol',
				header: 'Kode Tarif',
				width: 30
			},
			{
				dataIndex: 'ket_gol',
				header: 'Jenis Tarif',
				width: 75
			}
			//-------------- ## --------------
        ]),
		viewConfig:{
			forceFit: true
		}
    });
    return DataGridTarifCustomer;
}

function gridCustomer_viSetupTarifCustomer(){
    DataGridCustomer =new Ext.grid.GridPanel({
		ddGroup          : 'secondGridDDGroup',
		store			 : SetupTarifCustomer.form.ArrayStore.data1,
		autoScroll       : true,
        height			 : 270,
		width			 : 268,
        columnLines		 : true,
		stripeRows       : true,
		trackMouseOver   : true,
		enableDragDrop   : true,
		border			 : false,
		listeners : {
			afterrender : function(comp) {
				var secondGridDropTargetEl = DataGridCustomer.getView().scroller.dom;
				var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
					ddGroup    : 'firstGridDDGroup',
					notifyDrop : function(ddSource, e, data){
						var records =  ddSource.dragData.selections;
						Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
						DataGridCustomer.store.add(records);
						DataGridCustomer.store.sort('kd_customer', 'ASC');
						return true
					}
				});
			}
		},
        cm:new Ext.grid.ColumnModel([
        	new Ext.grid.RowNumberer(),	
			{
				dataIndex: 'kd_customer',
				header: 'Kode customer',
				hidden:true,
				width: 40
			},
			{
				dataIndex: 'customer',
				header: 'Customer',
				width: 60
			}
			//-------------- ## --------------
        ]),
		viewConfig:{
			forceFit: true
		}
    });
    return DataGridCustomer;
}

function gridMasterCustomer_viSetupTarifCustomer(){
    DataGridMasterCustomer =new Ext.grid.GridPanel({
		ddGroup          : 'firstGridDDGroup',
		autoScroll       : true,
		store			 : SetupTarifCustomer.form.ArrayStore.data2,
        height		 	 : 270,
		width			 : 368,
        columnLines		 : true,
		stripeRows       : true,
		trackMouseOver   : true,
		enableDragDrop   : true,
		border			 : false,
		flex			 :1,
		listeners : {
			afterrender : function(comp) {
				var secondGridDropTargetEl = DataGridMasterCustomer.getView().scroller.dom;
				var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
					ddGroup    : 'secondGridDDGroup',
					notifyDrop : function(ddSource, e, data){
						var records =  ddSource.dragData.selections;
						Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
						DataGridMasterCustomer.store.add(records);
						DataGridMasterCustomer.store.sort('kd_customer', 'ASC');
						return true
					}
				});
			}
		},
        cm:new Ext.grid.ColumnModel([
        	new Ext.grid.RowNumberer(),	
			{
				dataIndex: 'kd_customer',
				header: 'Kode customer',
				width: 30
			},
			{
				dataIndex: 'customer',
				header: 'Customer',
				width: 80
			}
			//-------------- ## --------------
        ]),
		viewConfig:{
			forceFit: true
		}
    });
    return DataGridMasterCustomer;
}

function getTarifCust_viSetupTarifCustomer(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionSetupTarifCustomer/getTarifCustomerGrid",
			params: {text:''},
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorSetupTarifCustomer('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					loadMask.hide();
					
					var recs=[],
						recType=dsDataGrdTarifCustomer_viSetupTarifCustomer.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
					dsDataGrdTarifCustomer_viSetupTarifCustomer.add(recs);
					/* for(var i=0 ; i<cst.listData.length ; i++){
						SetupTarifCustomer.form.ArrayStore.data1.add(new SetupTarifCustomer.form.ArrayStore.data1.recordType(cst.listData[i]));
					} */
				}
				
			}
		}
		
	)
}

function getCustomer_viSetupTarifCustomer(kdgol){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionSetupTarifCustomer/getCustomerGrid",
			params: {kdgol:kdgol},
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorSetupTarifCustomer('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					loadMask.hide();
					
					for(var i=0 ; i<cst.listData.length ; i++){
						SetupTarifCustomer.form.ArrayStore.data1.add(new SetupTarifCustomer.form.ArrayStore.data1.recordType(cst.listData[i]));
					}
				
					/* var recs=[],
						recType=dsDataGrdCustomer_viSetupTarifCustomer.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
					dsDataGrdCustomer_viSetupTarifCustomer.add(recs) */;
				}
				
			}
		}
		
	)
}

function getMasterCustomer_viSetupTarifCustomer(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionSetupTarifCustomer/getMasterCustomerGrid",
			params: {kdgol:''},
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorSetupTarifCustomer('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					loadMask.hide();
					
					for(var i=0 ; i<cst.listData.length ; i++){
						SetupTarifCustomer.form.ArrayStore.data2.add(new SetupTarifCustomer.form.ArrayStore.data2.recordType(cst.listData[i]));
					}
					/* var recs=[],
						recType=dsDataGrdCustomer_viSetupTarifCustomer.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
					dsDataGrdCustomer_viSetupTarifCustomer.add(recs) */;
				}
				
			}
		}
		
	)
}

function dataSave_viSetupTarifCustomer(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionSetupTarifCustomer/save",
			params: getParamSaveSetupTarifCustomer(),
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorSetupTarifCustomer('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					loadMask.hide();
					ShowPesanInfoSetupTarifCustomer('Berhasil menyimpan data ini','Information');
					dsDataGrdTarifCustomer_viSetupTarifCustomer.removeAll();
					getTarifCust_viSetupTarifCustomer();
				}else 
				{
					loadMask.hide();
					ShowPesanErrorSetupTarifCustomer('Gagal menyimpan data ini', 'Error');
					dsDataGrdTarifCustomer_viSetupTarifCustomer.removeAll();
					getTarifCust_viSetupTarifCustomer();
				};
			}
		}
		
	)
}

function dataDelete_viSetupTarifCustomer(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionSetupTarifCustomer/delete",
			params: getParamDeleteSetupTarifCustomer(),
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorSetupTarifCustomer('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					loadMask.hide();
					ShowPesanInfoSetupTarifCustomer('Berhasil menghapus data ini','Information');
					dataAddNewTarifCustomer();
					dsDataGrdTarifCustomer_viSetupTarifCustomer.removeAll();
					getTarifCust_viSetupTarifCustomer();
				}else 
				{
					loadMask.hide();
					ShowPesanErrorSetupTarifCustomer('Gagal menghapus data ini', 'Error');
					dsDataGrdTarifCustomer_viSetupTarifCustomer.removeAll();
					getTarifCust_viSetupTarifCustomer();
				};
			}
		}
		
	)
}

function dataSaveCustomer_viSetupTarifCustomer(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/apotek/functionSetupTarifCustomer/saveCustomer",
			params: getParamSaveCustomerSetupTarifCustomer(),
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorSetupTarifCustomer('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					loadMask.hide();
					ShowPesanInfoSetupTarifCustomer('Berhasil menyimpan data customer ini','Information');
					SetupTarifCustomer.form.ArrayStore.data2.removeAll();
					getMasterCustomer_viSetupTarifCustomer();
				}else 
				{
					loadMask.hide();
					ShowPesanErrorSetupTarifCustomer('Gagal menyimpan data customer ini', 'Error');
					SetupTarifCustomer.form.ArrayStore.data2.removeAll();
					getMasterCustomer_viSetupTarifCustomer();
				};
			}
		}
		
	)
}

function dataAddNewTarifCustomer(){
	Ext.getCmp('TxtKdTarifSetupTarifCust').setValue('');
	Ext.getCmp('TxtJenisTarifSetupTarifCust').setValue('');
}

function datainitTarifCustomer(rowdata){
	Ext.getCmp('TxtKdTarifSetupTarifCust').setValue(rowdata.kd_gol);
	Ext.getCmp('TxtJenisTarifSetupTarifCust').setValue(rowdata.ket_gol);
	SetupTarifCustomer.form.ArrayStore.data1.removeAll();
	getCustomer_viSetupTarifCustomer(rowdata.kd_gol);
	//=rowdata.ket_gol;
}

function getParamSaveSetupTarifCustomer(){
	var	params =
	{
		KdGol:Ext.getCmp('TxtKdTarifSetupTarifCust').getValue(),
		KetGol:Ext.getCmp('TxtJenisTarifSetupTarifCust').getValue()
	}
   
    return params
};

function getParamDeleteSetupTarifCustomer(){
	var	params =
	{
		KdGol:Ext.getCmp('TxtKdTarifSetupTarifCust').getValue()
	}
   
    return params
};

function getParamSaveCustomerSetupTarifCustomer(){
	var	params =
	{
		KdGol:Ext.getCmp('TxtKdTarifSetupTarifCust').getValue()
	}
	
	params['jumlah']=SetupTarifCustomer.form.ArrayStore.data1.getCount();
	for(var i = 0 ; i < SetupTarifCustomer.form.ArrayStore.data1.getCount();i++)
	{
		params['kd_customer-'+i]=SetupTarifCustomer.form.ArrayStore.data1.data.items[i].data.kd_customer;
	}
	
    return params
};

function ShowPesanWarningSetupTarifCustomer(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:290
		}
	);
};

function ShowPesanErrorSetupTarifCustomer(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};


function ShowPesanInfoSetupTarifCustomer(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};