// Data Source ExtJS # --------------
// Deklarasi Variabel pada Kasir Rawat Jalan # --------------

var dataSource_viPembukuanInventaris;
var selectCount_viPembukuanInventaris=50;
var NamaForm_viPembukuanInventaris="Pembukuan Inventaris";
var mod_name_viPembukuanInventaris="viPembukuanInventaris";
var now_viPembukuanInventaris= new Date();
var addNew_viPembukuanInventaris;
var rowSelected_viPembukuanInventaris;
var setLookUps_viPembukuanInventaris;
var mNoKunjungan_viPembukuanInventaris='';

var CurrentData_viPembukuanInventaris =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viPembukuanInventaris(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

// End Deklarasi Variabel pada Kasir Rawat Jalan # --------------

// Start Project Kasir Rawat Jalan # --------------

// --------------------------------------- # Start Function # ---------------------------------------
// ## Silahkan tempatkan semua fungsi eksekusi disini

// --------------------------------------- # End Function # ---------------------------------------

// --------------------------------------- # Start Form # ---------------------------------------

/**
*	Function : dataGrid_viPembukuanInventaris
*	
*	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
*/

function dataGrid_viPembukuanInventaris(mod_id_viPembukuanInventaris)
{	
    // Field kiriman dari Project Net.
    var FieldMaster_viPembukuanInventaris = 
	[
		 'NO_KUNJUNGAN', 'KD_KELOMPOK', 'KD_UNIT', 'KD_DOKTER', 'KD_Vendor', 'KD_PASIEN', 
		 'TGL_KUNJUNGAN','JAM_KUNJUNGAN', 'TINGGI_BADAN', 'BERAT_BADAN', 'TEKANAN_DRH', 
		 'NADI','ALERGI', 'KELUHAN', 'RUJUK_RAD', 'RUJUK_LAB', 'TAHAP_PROSES', 'PASIEN_BARU',
		 'NAMA_UNIT','KELOMPOK', 'DOKTER', 'Vendor', 'PS_BARU','KD_PENDIDIKAN','KD_STS_MARITAL', 
		 'KD_AGAMA','KD_PEKERJAAN','NAMA','TEMPAT_LAHIR','TGL_LAHIR','JENIS_KELAMIN','ALAMAT', 
		 'NO_TELP','NO_HP','GOL_DARAH','PENDIDIKAN','STS_MARITAL','AGAMA','PEKERJAAN','JNS_KELAMIN',
		 'TAHUN','BULAN','HARI'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viPembukuanInventaris = new WebApp.DataStore
	({
        fields: FieldMaster_viPembukuanInventaris
    });
    
    // Grid Kasir Rawat Jalan # --------------
	var GridDataView_viPembukuanInventaris = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viPembukuanInventaris,
			autoScroll: true,
			columnLines: true,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viPembukuanInventaris = undefined;
							rowSelected_viPembukuanInventaris = dataSource_viPembukuanInventaris.getAt(row);
							CurrentData_viPembukuanInventaris
							CurrentData_viPembukuanInventaris.row = row;
							CurrentData_viPembukuanInventaris.data = rowSelected_viPembukuanInventaris.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viPembukuanInventaris = dataSource_viPembukuanInventaris.getAt(ridx);
					if (rowSelected_viPembukuanInventaris != undefined)
					{
						setLookUp_viPembukuanInventaris(rowSelected_viPembukuanInventaris.data);
					}
					else
					{
						setLookUp_viPembukuanInventaris();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Kasir Rawat Jalan
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colNoFak_viPembukuanInventaris',
						header: 'Faktur Number',
						dataIndex: ' ',
						sortable: true,
						width: 35,
						filter:
						{
							type: 'int'
						}
					},
					//-------------- ## --------------
					{
						id: 'colNMCust_viPembukuanInventaris',
						header: 'Vendor',
						dataIndex: ' ',
						sortable: true,
						width: 50,
						filter:
						{}
					},				
					{
						id: 'colTglFak_viPembukuanInventaris',
						header:'Tgl Faktur',
						dataIndex: ' ',						
						width: 35,
						sortable: true,
						// format: 'd/M/Y',
						filter: {},
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.TGL_FAKTUR);
						}
					},
					//-------------- ## --------------
					{
						id: 'colNotes_viPembukuanInventaris',
						header:'Notes',
						dataIndex: ' ',
						width: 100,
						sortable: true,
						filter: {}
					}
					//-------------- ## --------------
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viPembukuanInventaris',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viPembukuanInventaris',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viPembukuanInventaris != undefined)
							{
								setLookUp_viPembukuanInventaris(rowSelected_viPembukuanInventaris.data)
							}
							else
							{								
								setLookUp_viPembukuanInventaris();
							}
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			bbar : bbar_paging(mod_name_viPembukuanInventaris, selectCount_viPembukuanInventaris, dataSource_viPembukuanInventaris),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viPembukuanInventaris = new Ext.Panel
    (
		{
			title: NamaForm_viPembukuanInventaris,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viPembukuanInventaris,
			region: 'center',
			layout: 'form', 
			closable: true,        
			border: false,  
			margins: '0 5 5 0',
			items: [GridDataView_viPembukuanInventaris],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viPembukuanInventaris,
		            columns: 9,
		            defaults: {
		                scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
			            { 
							xtype: 'tbtext', 
							text: 'Faktur Number : ', 
							style:{'text-align':'right'},
							width: 100,
							height: 25
						},						
						{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_NoFaktur_viPembukuanInventaris',							
							emptyText: 'No. Faktur',
							width: 150,
							height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										DfltFilterBtn_viPembukuanInventaris = 1;
										DataRefresh_viPembukuanInventaris(getCriteriaFilterGridDataView_viPembukuanInventaris());
									} 						
								}
							}
						},						
						{ 
							xtype: 'tbtext', 
							text: 'Tgl Faktur : ', 
							style:{'text-align':'right'},
							width: 70,
							height: 25
						},
						{
							xtype: 'datefield',
							id: 'txtfilterTglFakAwal_viPembukuanInventaris',
							value: now_viPembukuanInventaris,
							format: 'd/M/Y',
							width: 120,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viPembukuanInventaris();								
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 's/d Tgl : ', 
							style:{'text-align':'center'},
							width: 50,
							height: 25
						},
						{
							xtype: 'datefield',
							id: 'txtfilterTglFakAkhir_viPembukuanInventaris',
							value: now_viPembukuanInventaris,
							format: 'd/M/Y',
							width: 120,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viPembukuanInventaris();								
									} 						
								}
							}
						},
						
						{
							xtype: 'button',
							text: 'Cari',
							iconCls: 'refresh',
							tooltip: 'Cari',
							style:{paddingLeft:'30px'},
							//rowspan: 3,
							width:150,
							id: 'BtnFilterGridDataView_viPembukuanInventaris',
							handler: function() 
							{					
								DfltFilterBtn_viPembukuanInventaris = 1;
								DataRefresh_viPembukuanInventaris(getCriteriaFilterGridDataView_viPembukuanInventaris());
							}                        
						},
						//-------------- ## --------------
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viPembukuanInventaris;
    //-------------- # End form filter # --------------
}
// End Function dataGrid_viPembukuanInventaris # --------------

/**
*	Function : setLookUp_viPembukuanInventaris
*	
*	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
*/

function setLookUp_viPembukuanInventaris(rowdata)
{
    var lebar = 860;
    setLookUps_viPembukuanInventaris = new Ext.Window
    (
    {
        id: 'SetLookUps_viPembukuanInventaris',
		name: 'SetLookUps_viPembukuanInventaris',
        title: NamaForm_viPembukuanInventaris, 
        closeAction: 'destroy',        
        width: 875,
        height: 580,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viPembukuanInventaris(lebar,rowdata), //1
        listeners:
        {
            activate: function()
            {
				
            },
            afterShow: function()
            {
                this.activate();
            },
            deactivate: function()
            {
                rowSelected_viPembukuanInventaris=undefined;
                //datarefresh_viPembukuanInventaris();
				mNoKunjungan_viPembukuanInventaris = '';
            }
        }
    }
    );

    setLookUps_viPembukuanInventaris.show();
    if (rowdata == undefined)
    {
        // dataaddnew_viPembukuanInventaris();
		// Ext.getCmp('btnDelete_viPembukuanInventaris').disable();	
    }
    else
    {
        // datainit_viPembukuanInventaris(rowdata);
    }
}
// End Function setLookUpGridDataView_viPembukuanInventaris # --------------

/**
*	Function : getFormItemEntry_viPembukuanInventaris
*	
*	Sebuah fungsi untuk menampilkan penataan menu dan isian form edit
*/

function getFormItemEntry_viPembukuanInventaris(lebar,rowdata)
{
    var pnlFormDataBasic_viPembukuanInventaris = new Ext.FormPanel
    (
		{
			title: '',
			region: 'north',
			layout: 'form',
			bodyStyle: 'padding:10px 10px 10px 10px',
			anchor: '100%',
			width: lebar,
			border: false,
			//-------------- #items# --------------
			items:[getItemPanelInputBiodata_viPembukuanInventaris(lebar), getItemDataKunjungan_viPembukuanInventaris(lebar)], //,getItemPanelBawah_viPembukuanInventaris(lebar)],
			//-------------- #End items# --------------
			fileUpload: true,
			// Tombol pada tollbar Edit Data Pasien
			tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						id: 'btnAdd_viPembukuanInventaris',
						handler: function(){
							dataaddnew_viPembukuanInventaris();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viPembukuanInventaris',
						handler: function()
						{
							datasave_viPembukuanInventaris(addNew_viPembukuanInventaris);
							datarefresh_viPembukuanInventaris();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						id: 'btnSimpanExit_viPembukuanInventaris',
						handler: function()
						{
							var x = datasave_viPembukuanInventaris(addNew_viPembukuanInventaris);
							datarefresh_viPembukuanInventaris();
							if (x===undefined)
							{
								setLookUps_viPembukuanInventaris.close();
							}
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viPembukuanInventaris',
						handler: function()
						{
							datadelete_viPembukuanInventaris();
							datarefresh_viPembukuanInventaris();
							
						}
					},
					
					
				]
			}//,items:
		}
    )

    return pnlFormDataBasic_viPembukuanInventaris;
}
// End Function getFormItemEntry_viPembukuanInventaris # --------------

/**
*	Function : getItemPanelInputBiodata_viPembukuanInventaris
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemPanelInputBiodata_viPembukuanInventaris(lebar) 
{
    var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		border:true,
		items:
		[			
			{
				xtype: 'compositefield',
				fieldLabel: 'Faktur Number',
				anchor: '100%',
				//labelSeparator: '',
				width: 199,
				items: 
				[
					{
						xtype: 'textfield',
						flex: 1,
						fieldLabel: 'Label',
						width: 200,
						name: 'txtFakturNum_viPembukuanInventaris',
						id: 'txtFakturNum_viPembukuanInventaris',
						style:{'text-align':'left'},
						emptyText: 'Faktur Number',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},
					//-------------- ## --------------
					{
						xtype: 'displayfield',
						flex: 1,
						width: 340,
						name: '',
						value: ' ',
						fieldLabel: 'Label'
					},
					{
						xtype: 'displayfield',
						flex: 1,
						width: 35,
						name: '',
						value: 'Date:',
						fieldLabel: 'Label'
					},
					{
						xtype: 'datefield',
						id: 'dtpTglFaktur_viPembukuanInventaris',
						value: now_viPembukuanInventaris,
						format: 'd/M/Y',
						width: 120,
						listeners:
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									datarefresh_viPembukuanInventaris();								
								} 						
							}
						}
					},
					//-------------- ## --------------
				]
			},	
			//-------------- ## --------------				
			{
				xtype: 'compositefield',
				fieldLabel: 'Vendor',
				name: 'compVendor_viPembukuanInventaris',
				id: 'compVendor_viPembukuanInventaris',
				items: 
				[
					viCombo_HakTanah(515, 'ComboCust_fakAR'),
					{
						xtype: 'displayfield',
						flex: 1,
						width: 65,
						name: '',
						value: 'Due Date:',
						fieldLabel: 'Label'
					},
					{
						xtype: 'datefield',
						id: 'dtpDueDate_viPembukuanInventaris',
						value: now_viPembukuanInventaris,
						format: 'd/M/Y',
						width: 120,
						listeners:
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									datarefresh_viPembukuanInventaris();								
								} 						
							}
						}
					},
				]
			},
			//-------------- ## --------------	
			{
				xtype: 'compositefield',
				fieldLabel: 'Notes',
				name: 'compNotes_viPembukuanInventaris',
				id: 'compNotes_viPembukuanInventaris',
				items: 
				[
					{
						xtype: 'textfield',					
						fieldLabel: '',								
						width: 515,
						name: 'txtNotes_viPembukuanInventaris',
						id: 'txtNotes_viPembukuanInventaris',
						emptyText: 'Notes....'
					},
					{
						xtype: 'displayfield',				
						width: 65,								
						value: 'Reference:',
						fieldLabel: 'Label',
						id: 'lblReference_viPembukuanInventaris',
						name: 'lblReference_viPembukuanInventaris'
					},
					{
						xtype: 'textfield',					
						fieldLabel: '',								
						width: 120,
						name: 'txtReference_viPembukuanInventaris',
						id: 'txtReference_viPembukuanInventaris',
						emptyText: 'Reference'
					}
					//-------------- ## --------------
				]
			}
			//-------------- ## --------------				
		]
	};
    return items;
};
// End Function getItemPanelInputBiodata_viPembukuanInventaris # --------------

/**
*	Function : getItemDataKunjungan_viPembukuanInventaris
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemDataKunjungan_viPembukuanInventaris(lebar) 
{
    var items =
	{
		layout: 'form',
		anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',		
		width: lebar-80,
		height: 400,
	    items:
		[				
			{
				xtype: 'panel',
				title: '',
				border:false,	
				height:375,
				items: 
				[
					{
						xtype: 'tabpanel',
						activeTab: 0,		
						height: 370,
						items: 
						[
							{
								xtype: 'panel',
								title: 'Tanah',				
								padding: '4px',
								items: 
								[
									{
										xtype: 'compositefield',
										fieldLabel: 'No. Buku',
										name: 'compNoBuku_viPembukuanInventaris',
										id: 'compNoBuku_viPembukuanInventaris',
										items: 
										[
											{
												xtype: 'displayfield',
												flex: 1,
												width: 95,
												name: '',
												value: 'No Buku:',
												style:{'text-align':'right'},
												fieldLabel: 'Label'
											},
											{
												xtype: 'textfield',
												flex: 1,
												fieldLabel: 'Label',
												width: 100,
												name: 'txtNoBuku_viPembukuanInventaris',
												id: 'txtNoBuku_viPembukuanInventaris',
												style:{'text-align':'left'},
												emptyText: 'No Buku'
											},											
											{
												xtype: 'displayfield',
												flex: 1,
												width: 65,
												name: '',
												value: 'Tgl Buku:',
												fieldLabel: 'Label'
											},
											{
												xtype: 'datefield',
												id: 'dtpTglBuku_viPembukuanInventaris',
												value: now_viPembukuanInventaris,
												format: 'd/M/Y',
												width: 120
											},
											{
												xtype: 'button',
												text: 'No Reg Barang',												
												id: 'btnRegBrg_viPembukuanInventaris',												
											},
											{
												xtype: 'textfield',
												flex: 1,
												fieldLabel: 'Label',
												width: 320,
												name: 'txtNoRegBrg_viPembukuanInventaris',
												id: 'txtNoRegBrg_viPembukuanInventaris',
												style:{'text-align':'left'},
												emptyText: 'No Reg Barang'
											},
										]
									},
									{ 
										xtype: 'tbspacer',									
										height: 5
									},
									{
										xtype: 'compositefield',
										fieldLabel: 'Kd Kelompok',
										name: 'compKdKel_viPembukuanInventaris',
										id: 'compKdKel_viPembukuanInventaris',
										items: 
										[
											{
												xtype: 'displayfield',
												flex: 1,
												width: 95,
												name: '',
												value: 'Kd Kelompok:',
												style:{'text-align':'right'},
												fieldLabel: 'Label'
											},
											{
												xtype: 'textfield',
												flex: 1,
												fieldLabel: 'Label',
												width: 100,
												name: 'txtKdKel_viPembukuanInventaris',
												id: 'txtKdKel_viPembukuanInventaris',
												style:{'text-align':'left'},
												emptyText: 'Kd Kelompok'
											},											
											{
												xtype: 'displayfield',
												flex: 1,
												width: 65,
												name: '',
												value: 'Kelompok :',
												fieldLabel: 'Label'
											},											
											{
												xtype: 'textfield',
												flex: 1,
												fieldLabel: 'Label',
												width: 530,
												name: 'txtKel_viPembukuanInventaris',
												id: 'txtKel_viPembukuanInventaris',
												style:{'text-align':'left'},
												emptyText: 'Kelompok'
											},
										]
									},
									{ 
										xtype: 'tbspacer',									
										height: 5
									},
									{
										xtype: 'compositefield',
										fieldLabel: 'Urutan',
										name: 'compUrutan_viPembukuanInventaris',
										id: 'compUrutan_viPembukuanInventaris',
										items: 
										[
											{
												xtype: 'displayfield',
												flex: 1,
												width: 95,
												name: '',
												value: 'Urutan:',
												style:{'text-align':'right'},
												fieldLabel: 'Label'
											},
											{
												xtype: 'textfield',
												flex: 1,
												fieldLabel: 'Label',
												width: 100,
												name: 'txtUrutan_viPembukuanInventaris',
												id: 'txtUrutan_viPembukuanInventaris',
												style:{'text-align':'left'},
												emptyText: 'Urutan'
											},											
											{
												xtype: 'displayfield',
												flex: 1,
												width: 65,
												name: '',
												value: 'Tanah :',
												fieldLabel: 'Label'
											},											
											{
												xtype: 'textfield',
												flex: 1,
												fieldLabel: 'Label',
												width: 530,
												name: 'txtTanah_viPembukuanInventaris',
												id: 'txtTanah_viPembukuanInventaris',
												style:{'text-align':'left'},
												emptyText: 'Tanah '
											},
										]
									},
									{ 
										xtype: 'tbspacer',									
										height: 5
									},
									{
										xtype: 'compositefield',
										fieldLabel: 'Luas Tanah',
										name: 'compLuasTanah_viPembukuanInventaris',
										id: 'compLuasTanah_viPembukuanInventaris',
										items: 
										[
											{
												xtype: 'displayfield',
												flex: 1,
												width: 95,
												name: '',
												value: 'Luas Tanah:',
												style:{'text-align':'right'},
												fieldLabel: 'Label'
											},
											{
												xtype: 'textfield',
												flex: 1,
												fieldLabel: 'Label',
												width: 75,
												name: 'txtLuasTanah_viPembukuanInventaris',
												id: 'txtLuasTanah_viPembukuanInventaris',
												style:{'text-align':'left'},
												emptyText: 'Luas Tanah'
											},
											{
												xtype: 'displayfield',
												flex: 1,
												width: 65,
												name: '',
												value: 'Satuan',
												fieldLabel: 'Label'
											},		
											{
												xtype: 'displayfield',
												flex: 1,
												width: 90,
												name: '',
												value: 'No Sertifikat :',
												fieldLabel: 'Label'
											},											
											{
												xtype: 'textfield',
												flex: 1,
												fieldLabel: 'Label',
												width: 355,
												name: 'txtNoSertifikat_viPembukuanInventaris',
												id: 'txtNoSertifikat_viPembukuanInventaris',
												style:{'text-align':'left'},
												emptyText: 'No. Sertifikat '
											},											
											{
												xtype: 'datefield',
												id: 'dtpTglSertifikat_viPembukuanInventaris',
												value: now_viPembukuanInventaris,
												format: 'd/M/Y',
												width: 100
											},
										]
									},									
									{ 
										xtype: 'tbspacer',									
										height: 5
									},
									{
										xtype: 'compositefield',
										fieldLabel: 'Alamat',
										name: 'compAlamat_viPembukuanInventaris',
										id: 'compAlamat_viPembukuanInventaris',
										items: 
										[
											{
												xtype: 'displayfield',
												flex: 1,
												width: 95,
												name: '',
												value: 'Alamat :',
												style:{'text-align':'right'},
												fieldLabel: 'Label'
											},
											{
												xtype: 'textfield',
												flex: 1,
												fieldLabel: 'Label',
												width: 705,
												name: 'txtAlamat_viPembukuanInventaris',
												id: 'txtAlamat_viPembukuanInventaris',
												style:{'text-align':'left'},
												emptyText: 'Alamat'
											},											
										]
									},
									{ 
										xtype: 'tbspacer',									
										height: 5
									},
									{
										xtype: 'compositefield',
										fieldLabel: 'Hak Atas Tanah',
										name: 'compHakTanah_viPembukuanInventaris',
										id: 'compHakTanah_viPembukuanInventaris',
										items: 
										[
											{
												xtype: 'displayfield',
												flex: 1,
												width: 95,
												name: '',
												value: 'Hak Atas Tanah :',
												style:{'text-align':'right'},
												fieldLabel: 'Label'
											},
											viCombo_HakTanah(100,'combo_HakTah'),
											{
												xtype: 'displayfield',
												flex: 1,
												width: 110,
												name: '',
												value: 'Digunakan Untuk :',
												fieldLabel: 'Label'
											},	
											{
												xtype: 'textfield',
												flex: 1,
												fieldLabel: 'Label',
												width: 486,
												name: 'txtDigunakan_viPembukuanInventaris',
												id: 'txtDigunakan_viPembukuanInventaris',
												style:{'text-align':'left'},
												emptyText: 'Digunakan untuk'
											},											
										]
									},
									{ 
										xtype: 'tbspacer',									
										height: 10
									},
									{
										xtype: 'compositefield',
										fieldLabel: 'No.Terima',
										name: 'compNoterima_viPembukuanInventaris',
										id: 'compNoterima_viPembukuanInventaris',
										items: 
										[
											{
												xtype: 'displayfield',
												flex: 1,
												width: 95,
												name: '',
												value: 'No. Terima :',
												style:{'text-align':'right'},
												fieldLabel: 'Label'
											},											
											{
												xtype: 'textfield',
												flex: 1,
												fieldLabel: 'Label',
												width: 50,
												name: 'txtNoTerima1_viPembukuanInventaris',
												id: 'txtNoTerima1_viPembukuanInventaris',
												style:{'text-align':'left'},												
											},											
											{
												xtype: 'textfield',
												flex: 1,
												fieldLabel: 'Label',
												width: 50,
												name: 'txtNoTerima2_viPembukuanInventaris',
												id: 'txtNoTerima2_viPembukuanInventaris',
												style:{'text-align':'left'},												
											},											
											{
												xtype: 'displayfield',
												flex: 1,
												width: 50,
												name: '',
												value: 'Dari :',
												style:{'text-align':'right'},
												fieldLabel: 'Label'
											},
											{
												xtype: 'textfield',
												flex: 1,
												fieldLabel: 'Label',
												width: 540,
												name: 'txtdari_viPembukuanInventaris',
												id: 'txtdari_viPembukuanInventaris',
												style:{'text-align':'left'},												
											},												
										]
									},
									{
										xtype: 'compositefield',
										fieldLabel: 'Perolehan',
										name: 'compPerolehan_viPembukuanInventaris',
										id: 'compPerolehan_viPembukuanInventaris',
										items: 
										[
											{
												xtype: 'displayfield',
												flex: 1,
												width: 95,
												name: '',
												value: 'Perolehan :',
												style:{'text-align':'right'},
												fieldLabel: 'Label'
											},											
											{
												xtype: 'textfield',
												flex: 1,
												fieldLabel: 'Label',
												width: 50,
												name: 'txtNoTerima1_viPembukuanInventaris',
												id: 'txtNoTerima1_viPembukuanInventaris',
												style:{'text-align':'left'},												
											},											
											{
												xtype: 'textfield',
												flex: 1,
												fieldLabel: 'Label',
												width: 50,
												name: 'txtNoTerima2_viPembukuanInventaris',
												id: 'txtNoTerima2_viPembukuanInventaris',
												style:{'text-align':'left'},												
											},											
											{
												xtype: 'displayfield',
												flex: 1,
												width: 50,
												name: '',
												value: 'Dari :',
												style:{'text-align':'right'},
												fieldLabel: 'Label'
											},
											{
												xtype: 'textfield',
												flex: 1,
												fieldLabel: 'Label',
												width: 540,
												name: 'txtdari_viPembukuanInventaris',
												id: 'txtdari_viPembukuanInventaris',
												style:{'text-align':'left'},												
											},												
										]
									},
								]
							},
							{
								xtype: 'panel',
								title: 'Journal',				
								padding: '4px',
								items: 
								[
									gridDataTransPntJasa_viPembukuanInventaris(),
									{ 
										xtype: 'tbspacer',									
										height: 5
									},
									{
										xtype: 'textfield',
										id: 'txttotaltranspntjasa_viPembukuanInventaris',
										name: 'txttotaltranspntjasa_viPembukuanInventaris',
										style:{'text-align':'right','margin-left':'700px'},
										width: 107,
										value: 0,
										readOnly: true,
									}
								]
							}
						]
					},									
				]
			}
		]
	};
    return items;
};
// End Function getItemDataKunjungan_viPembukuanInventaris # --------------

/**
*	Function : getItemPanelBawah_viPembukuanInventaris
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function gridDataTransPntJasa_viPembukuanInventaris()
{
    
    var FieldGrdKasir_viPembukuanInventaris = 
	[
	];
	
    dsDataGrdJab_viPembukuanInventaris= new WebApp.DataStore
	({
        fields: FieldGrdKasir_viPembukuanInventaris
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJab_viPembukuanInventaris,
        height: 300,
		selModel: new Ext.grid.RowSelectionModel
        (
	        {
	            singleSelect: true,
	            listeners:
	            {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
	        }
        ),
        columns: 
		[
			{
				dataIndex: '',
				header: 'Item#',
				sortable: true,
				width: 100
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Description',
				sortable: true,
				width: 600
			},
			//-------------- ## --------------			
			{			
				dataIndex: '',
				header: 'Amount',
				sortable: true,
				width: 100
			},
        ]
    }    
    return items;
}

function viCombo_HakTanah(lebar,Nama_ID)
{
    var Field_HakTanah = [' ', ' '];
    ds_HakTanah = new WebApp.DataStore({fields: Field_HakTanah});
    
	// viRefresh_Journal();
	
    var cbo_HakTanah = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'HakTanah',
			valueField: 'HakTanah',
            displayField: 'HakTanah',
			emptyText:'Hak Tanah...',
			store: ds_HakTanah,
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',                        
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
			listeners:
			{ 
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_HakTanah;
}