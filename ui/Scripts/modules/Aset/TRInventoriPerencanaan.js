// Data Source ExtJS # --------------

/**
*	Nama File 		: TRInventoriPerencanaan.js
*	Menu 			: 
*	Model id 		: 
*	Keterangan 		: Perencanaan / Request Order 
*	Di buat tanggal : 21 Agustus 2014
*	Oleh 			: SDY_RI
*/

// Deklarasi Variabel pada Inventori Perencanaan # --------------

var dataSource_viInventoriPerencanaan;
var selectCount_viInventoriPerencanaan=50;
var NamaForm_viInventoriPerencanaan="Request Order / Perencanaan ";
var mod_name_viInventoriPerencanaan="viInventoriPerencanaan";
var now_viInventoriPerencanaan= new Date();
var addNew_viInventoriPerencanaan;
var rowSelected_viInventoriPerencanaan;
var setLookUps_viInventoriPerencanaan;
var mNoKunjungan_viInventoriPerencanaan='';

var CurrentData_viInventoriPerencanaan =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viInventoriPerencanaan(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

// End Deklarasi Variabel pada Inventori Perencanaan # --------------

// Start Project Inventori Perencanaan # --------------

// --------------------------------------- # Start Function # ---------------------------------------
// ## Silahkan tempatkan semua fungsi eksekusi disini

// --------------------------------------- # End Function # ---------------------------------------

// --------------------------------------- # Start Form # ---------------------------------------

/**
*	Function : dataGrid_viInventoriPerencanaan
*	
*	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
*/

function dataGrid_viInventoriPerencanaan(mod_id_viInventoriPerencanaan)
{	
    // Field kiriman dari Project Net.
    var FieldMaster_viInventoriPerencanaan = 
	[
		 'NO_KUNJUNGAN', 'KD_KELOMPOK', 'KD_UNIT', 'KD_DOKTER', 'KD_CUSTOMER', 'KD_PASIEN', 
		 'TGL_KUNJUNGAN','JAM_KUNJUNGAN', 'TINGGI_BADAN', 'BERAT_BADAN', 'TEKANAN_DRH', 
		 'NADI','ALERGI', 'KELUHAN', 'RUJUK_RAD', 'RUJUK_LAB', 'TAHAP_PROSES', 'PASIEN_BARU',
		 'NAMA_UNIT','KELOMPOK', 'DOKTER', 'CUSTOMER', 'PS_BARU','KD_PENDIDIKAN','KD_STS_MARITAL', 
		 'KD_AGAMA','KD_PEKERJAAN','NAMA','TEMPAT_LAHIR','TGL_LAHIR','JENIS_KELAMIN','ALAMAT', 
		 'NO_TELP','NO_HP','GOL_DARAH','PENDIDIKAN','STS_MARITAL','AGAMA','PEKERJAAN','JNS_KELAMIN',
		 'TAHUN','BULAN','HARI'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viInventoriPerencanaan = new WebApp.DataStore
	({
        fields: FieldMaster_viInventoriPerencanaan
    });
    
    // Grid Inventori Perencanaan # --------------
	var GridDataView_viInventoriPerencanaan = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viInventoriPerencanaan,
			autoScroll: true,
			columnLines: true,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viInventoriPerencanaan = undefined;
							rowSelected_viInventoriPerencanaan = dataSource_viInventoriPerencanaan.getAt(row);
							CurrentData_viInventoriPerencanaan
							CurrentData_viInventoriPerencanaan.row = row;
							CurrentData_viInventoriPerencanaan.data = rowSelected_viInventoriPerencanaan.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viInventoriPerencanaan = dataSource_viInventoriPerencanaan.getAt(ridx);
					if (rowSelected_viInventoriPerencanaan != undefined)
					{
						setLookUp_viInventoriPerencanaan(rowSelected_viInventoriPerencanaan.data);
					}
					else
					{
						setLookUp_viInventoriPerencanaan();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Inventori perencanaan
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colNoMedrec_viInventoriPerencanaan',
						header: 'Ro Number',
						dataIndex: 'RO_NUMBER',
						sortable: true,
						width: 35,
						filter:
						{
							type: 'int'
						}
					},
					//-------------- ## --------------
					{
						id: 'colTglRO_viInventoriPerencanaan',
						header:'Tgl RO',
						dataIndex: 'TGL_RO',						
						width: 50,
						sortable: true,
						// format: 'd/M/Y',
						filter: {},
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.TGL_RO);
						}
					},
					//-------------- ## --------------
					{
						id: 'colKeterangan_viInventoriPerencanaan',
						header: 'Keterangan',
						dataIndex: 'KETERANGAN',
						sortable: true,
						width: 60,
						filter:
						{}
					}
					//-------------- ## --------------
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viInventoriPerencanaan',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viInventoriPerencanaan',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viInventoriPerencanaan != undefined)
							{
								setLookUp_viInventoriPerencanaan(rowSelected_viInventoriPerencanaan.data)
							}
							else
							{								
								setLookUp_viInventoriPerencanaan();
							}
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			bbar : bbar_paging(mod_name_viInventoriPerencanaan, selectCount_viInventoriPerencanaan, dataSource_viInventoriPerencanaan),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viInventoriPerencanaan = new Ext.Panel
    (
		{
			title: NamaForm_viInventoriPerencanaan,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viInventoriPerencanaan,
			region: 'center',
			layout: 'form', 
			closable: true,        
			border: false,  
			margins: '0 5 5 0',
			items: [GridDataView_viInventoriPerencanaan],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viInventoriPerencanaan,
		            columns: 9,
		            defaults: {
		                scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
			            { 
							xtype: 'tbtext', 
							text: 'Ro. Number : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},
						{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_RoNumber_viInventoriPerencanaan',
							emptyText: 'Ro. Number',
							width: 100,
							height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										
									} 						
								}
							}
						},								
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Tgl Ro : ', 
							style:{'text-align':'right'},
							width: 50,
							height: 25
						},
						{
							xtype: 'datefield',
							id: 'txtfilterTglAwal_viInventoriPerencanaan',
							value: now_viInventoriPerencanaan,
							format: 'd/M/Y',
							width: 120,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viInventoriPerencanaan();								
									} 						
								}
							}
						},
						{ 
							xtype: 'tbtext', 
							text: ' s.d ', 
							style:{'text-align':'center'},
							width: 30,
							height: 25
						},																		
						//-------------- ## --------------						
						{
							xtype: 'datefield',
							id: 'txtfilterTglKunjAkhir_viInventoriPerencanaan',
							value: now_viInventoriPerencanaan,
							format: 'd/M/Y',
							width: 120,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viInventoriPerencanaan();								
									} 						
								}
							}
						},	
						
						//-------------- ## --------------
						{
							xtype: 'button',
							text: 'Cari',
							iconCls: 'refresh',
							tooltip: 'Cari',
							style:{paddingLeft:'30px'},
							//rowspan: 3,
							width:150,
							id: 'BtnFilterGridDataView_viInventoriPerencanaan',
							handler: function() 
							{					
								DfltFilterBtn_viInventoriPerencanaan = 1;
								DataRefresh_viInventoriPerencanaan(getCriteriaFilterGridDataView_viInventoriPerencanaan());
							}                        
						},
						//-------------- ## --------------
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viInventoriPerencanaan;
    //-------------- # End form filter # --------------
}
// End Function dataGrid_viInventoriPerencanaan # --------------

/**
*	Function : setLookUp_viInventoriPerencanaan
*	
*	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
*/

function setLookUp_viInventoriPerencanaan(rowdata)
{
    var lebar = 985;
    setLookUps_viInventoriPerencanaan = new Ext.Window
    (
    {
        id: 'SetLookUps_viInventoriPerencanaan',
		name: 'SetLookUps_viInventoriPerencanaan',
        title: NamaForm_viInventoriPerencanaan, 
        closeAction: 'destroy',        
        width: 1000,
        height: 605,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viInventoriPerencanaan(lebar,rowdata), //1
        listeners:
        {
            activate: function()
            {
				
            },
            afterShow: function()
            {
                this.activate();
            },
            deactivate: function()
            {
                rowSelected_viInventoriPerencanaan=undefined;
                //datarefresh_viInventoriPerencanaan();
				mNoKunjungan_viInventoriPerencanaan = '';
            }
        }
    }
    );

    setLookUps_viInventoriPerencanaan.show();
    if (rowdata == undefined)
    {
        // dataaddnew_viInventoriPerencanaan();
		// Ext.getCmp('btnDelete_viInventoriPerencanaan').disable();	
    }
    else
    {
        // datainit_viInventoriPerencanaan(rowdata);
    }
}
// End Function setLookUpGridDataView_viInventoriPerencanaan # --------------

/**
*	Function : getFormItemEntry_viInventoriPerencanaan
*	
*	Sebuah fungsi untuk menampilkan penataan menu dan isian form edit
*/

function getFormItemEntry_viInventoriPerencanaan(lebar,rowdata)
{
    var pnlFormDataBasic_viInventoriPerencanaan = new Ext.FormPanel
    (
		{
			title: '',
			region: 'north',
			layout: 'form',
			bodyStyle: 'padding:10px 10px 10px 10px',
			anchor: '100%',
			width: lebar,
			border: false,
			//-------------- #items# --------------
			items:
			[
				getItemPanelInputPenerimaan_viInventoriPerencanaan(lebar),
				//-------------- ## -------------- 				
				getItemGridTransaksi_viInventoriPerencanaan(lebar),
				//-------------- ## --------------
        
			],
			//-------------- #End items# --------------
			fileUpload: true,
			// Tombol pada tollbar Edit Data Pasien
			tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						id: 'btnAdd_viInventoriPerencanaan',
						handler: function(){
							dataaddnew_viInventoriPerencanaan();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viInventoriPerencanaan',
						handler: function()
						{
							datasave_viInventoriPerencanaan(addNew_viInventoriPerencanaan);
							datarefresh_viInventoriPerencanaan();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						id: 'btnSimpanExit_viInventoriPerencanaan',
						handler: function()
						{
							var x = datasave_viInventoriPerencanaan(addNew_viInventoriPerencanaan);
							datarefresh_viInventoriPerencanaan();
							if (x===undefined)
							{
								setLookUps_viInventoriPerencanaan.close();
							}
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viInventoriPerencanaan',
						handler: function()
						{
							datadelete_viInventoriPerencanaan();
							datarefresh_viInventoriPerencanaan();
							
						}
					},
					//-------------- ## --------------
					{
						xtype:'tbseparator'
					},
									
				]
			}
		}
    )

    return pnlFormDataBasic_viInventoriPerencanaan;
}
// End Function getFormItemEntry_viInventoriPerencanaan # --------------

/**
*	Function : getItemPanelInputPenerimaan_viInventoriPerencanaan
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemPanelInputPenerimaan_viInventoriPerencanaan(lebar) 
{
    var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		border:true,
		items:
		[			
			{
                xtype: 'compositefield',
                fieldLabel: 'Ro Number',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					{
						xtype: 'textfield',
						flex: 1,
						width : 120,	
						name: 'txtRoNumber_viInventoriPerencanaan',
						id: 'txtRoNumber_viInventoriPerencanaan',
						emptyText: 'Ro Number',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},                    
                    //-------------- ## --------------  
					{
						xtype: 'displayfield',
						flex: 1,
						width: 70,
						name: '',
						value: 'Ro Date:',
						fieldLabel: 'Label'
					},					
					{
						xtype: 'datefield',
						id: 'txtDateRO_viInventoriPerencanaan',
						value: now_viInventoriPerencanaan,
						format: 'd/M/Y',
						width: 120,
						listeners:
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									datarefresh_viInventoriPerencanaan();								
								} 						
							}
						}
					}
					
                ]
            },
            //-------------- ## --------------  
			{
				xtype: 'compositefield',
				fieldLabel: 'Keterangan',
				anchor: '100%',
				//labelSeparator: '',
				width: 199,
				items: 
				[
					{
						xtype: 'textarea',
						fieldLabel: 'Keterangan',
						emptyText: 'Keterangan',
						id: 'txtKeterangan_viInventoriPerencanaan',
						name: 'txtKeterangan_viInventoriPerencanaan',
						width: 450,
						height: 50,
					}
				]
			}
		]
	};
    return items;
};
// End Function getItemPanelInputPenerimaan_viInventoriPerencanaan # --------------


/**
*	Function : getItemGridTransaksi_viInventoriPerencanaan
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemGridTransaksi_viInventoriPerencanaan(lebar) 
{
    var items =
	{
		//title: 'Detail Transaksi', 
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:1px 1px 1px 1px',
		border:true,
		width: lebar-80,
		height:425, //225, 
	    items:
		[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:
				[
					gridDataViewEdit_viInventoriPerencanaan()
				]	
			}
			//-------------- ## --------------
		]
	};
    return items;
};
// End Function getItemGridTransaksi_viInventoriPerencanaan # --------------

/**
*	Function : gridDataViewEdit_viInventoriPerencanaan
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function gridDataViewEdit_viInventoriPerencanaan()
{
    
    chkSelected_viInventoriPerencanaan = new Ext.grid.CheckColumn
	(
		{
			id: 'chkSelected_viInventoriPerencanaan',
			header: '',
			align: 'center',						
			dataIndex: 'SELECTED',			
			width: 20
		}
	);

    var FieldGrdKasir_viInventoriPerencanaan = 
	[
	];
	
    dsDataGrdJab_viInventoriPerencanaan= new WebApp.DataStore
	({
        fields: FieldGrdKasir_viInventoriPerencanaan
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJab_viInventoriPerencanaan,
        height: 420,
		selModel: new Ext.grid.RowSelectionModel
        (
	        {
	            singleSelect: true,
	            listeners:
	            {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
	        }
        ),
        columns: 
		[
			chkSelected_viInventoriPerencanaan,
			{
				dataIndex: '',
				header: 'Kd.Kelompok',
				sortable: true,
				width: 100
			},
			{
				dataIndex: '',
				header: 'Urut',
				sortable: true,
				width: 100
			},
			//-------------- ## --------------
			{			
				dataIndex: '',
				header: 'Nama Barang',
				sortable: true,
				width: 500
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Satuan',
				sortable: true,
				width: 85,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Qty',
				sortable: true,
				width: 50,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Stok',
				sortable: true,
				width: 50,
				renderer: function(v, params, record) 
				{
					
				}	
			},			
			{
				dataIndex: '',
				header: 'Ordered',
				sortable: true,
				width: 50,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
        ],
        plugins:chkSelected_viInventoriPerencanaan,
    }    
    return items;
}
// End Function gridDataViewEdit_viInventoriPerencanaan # --------------




