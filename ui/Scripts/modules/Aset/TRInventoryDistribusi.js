// Data Source ExtJS # --------------

/**
*	Nama File 		: TRInventoriDistribusi.js
*	Menu 			: 
*	Model id 		: 
*	Keterangan 		: Distribusi Inventory
*	Di buat tanggal : 21 Agustus 2014
*	Oleh 			: SDY_RI
*/

// Deklarasi Variabel pada Inventori Perencanaan # --------------

var dataSource_viInventoriDistribusi;
var selectCount_viInventoriDistribusi=50;
var NamaForm_viInventoriDistribusi="Distribusi Barang Habis Pakai";
var mod_name_viInventoriDistribusi="viInventoriDistribusi";
var now_viInventoriDistribusi= new Date();
var addNew_viInventoriDistribusi;
var rowSelected_viInventoriDistribusi;
var setLookUps_viInventoriDistribusi;
var mNoKunjungan_viInventoriDistribusi='';

var CurrentData_viInventoriDistribusi =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viInventoriDistribusi(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

/**
*	Function : dataGrid_viInventoriDistribusi
*	
*	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
*/

function dataGrid_viInventoriDistribusi(mod_id_viInventoriDistribusi)
{	
    // Field kiriman dari Project Net.
    var FieldMaster_viInventoriDistribusi = 
	[
		 'NO_KUNJUNGAN', 'KD_KELOMPOK', 'KD_UNIT', 'KD_DOKTER', 'KD_CUSTOMER', 'KD_PASIEN', 
		 'TGL_KUNJUNGAN','JAM_KUNJUNGAN', 'TINGGI_BADAN', 'BERAT_BADAN', 'TEKANAN_DRH', 
		 'NADI','ALERGI', 'KELUHAN', 'RUJUK_RAD', 'RUJUK_LAB', 'TAHAP_PROSES', 'PASIEN_BARU',
		 'NAMA_UNIT','KELOMPOK', 'DOKTER', 'CUSTOMER', 'PS_BARU','KD_PENDIDIKAN','KD_STS_MARITAL', 
		 'KD_AGAMA','KD_PEKERJAAN','NAMA','TEMPAT_LAHIR','TGL_LAHIR','JENIS_KELAMIN','ALAMAT', 
		 'NO_TELP','NO_HP','GOL_DARAH','PENDIDIKAN','STS_MARITAL','AGAMA','PEKERJAAN','JNS_KELAMIN',
		 'TAHUN','BULAN','HARI'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viInventoriDistribusi = new WebApp.DataStore
	({
        fields: FieldMaster_viInventoriDistribusi
    });
    
    // Grid Inventori Perencanaan # --------------
	var GridDataView_viInventoriDistribusi = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viInventoriDistribusi,
			autoScroll: true,
			columnLines: true,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viInventoriDistribusi = undefined;
							rowSelected_viInventoriDistribusi = dataSource_viInventoriDistribusi.getAt(row);
							CurrentData_viInventoriDistribusi
							CurrentData_viInventoriDistribusi.row = row;
							CurrentData_viInventoriDistribusi.data = rowSelected_viInventoriDistribusi.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viInventoriDistribusi = dataSource_viInventoriDistribusi.getAt(ridx);
					if (rowSelected_viInventoriDistribusi != undefined)
					{
						setLookUp_viInventoriDistribusi(rowSelected_viInventoriDistribusi.data);
					}
					else
					{
						setLookUp_viInventoriDistribusi();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Inventori perencanaan
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colNoTerima_viInventoriDistribusi',
						header: 'No. Keluar',
						dataIndex: '',
						sortable: true,
						width: 35,
						filter:
						{
							type: 'int'
						}
					},
					//-------------- ## --------------
					{
						id: 'colTglRO_viInventoriDistribusi',
						header:'Tanggal',
						dataIndex: '',						
						width: 20,
						sortable: true,
						// format: 'd/M/Y',
						filter: {},
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.TGL_RO);
						}
					},
					//-------------- ## --------------
					{
						id: 'colKeterangan_viInventoriDistribusi',
						header: 'Bagian',
						dataIndex: '',
						sortable: true,
						width: 60,
						filter:
						{}
					}
					//-------------- ## --------------
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viInventoriDistribusi',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viInventoriDistribusi',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viInventoriDistribusi != undefined)
							{
								setLookUp_viInventoriDistribusi(rowSelected_viInventoriDistribusi.data)
							}
							else
							{								
								setLookUp_viInventoriDistribusi();
							}
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			bbar : bbar_paging(mod_name_viInventoriDistribusi, selectCount_viInventoriDistribusi, dataSource_viInventoriDistribusi),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viInventoriDistribusi = new Ext.Panel
    (
		{
			title: NamaForm_viInventoriDistribusi,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viInventoriDistribusi,
			region: 'center',
			layout: 'form', 
			closable: true,        
			border: false,  
			margins: '0 5 5 0',
			items: [GridDataView_viInventoriDistribusi],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viInventoriDistribusi,
		            columns: 11,
		            defaults: {
		                scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
						//-------------- ## --------------
			            { 
							xtype: 'tbtext', 
							text: 'No. Keluar : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},						
						{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_NoTerima_viInventoriDistribusi',
							emptyText: 'No. Keluar',
							width: 100,
							height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										
									} 						
								}
							}
						},
						{	 
							xtype: 'tbspacer',
							width: 10,
							height: 25
						},						
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Tanggal : ', 
							style:{'text-align':'right'},
							width: 60,
							height: 25
						},
						{
							xtype: 'datefield',
							id: 'txtfilterTglAwal_viInventoriDistribusi',
							value: now_viInventoriDistribusi,
							format: 'd/M/Y',
							width: 120,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viInventoriDistribusi();								
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: ' s.d ', 
							style:{'text-align':'center'},
							width: 30,
							height: 25
						},																								
						//-------------- ## --------------
						{
							xtype: 'datefield',
							id: 'txtfilterTglKunjAkhir_viInventoriDistribusi',
							value: now_viInventoriDistribusi,
							format: 'd/M/Y',
							width: 120,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viInventoriDistribusi();								
									} 						
								}
							}
						},							
						{	 
							xtype: 'tbspacer',
							width: 10,
							height: 25
						},
						{ 
							xtype: 'tbtext', 
							text: 'Bagian : ', 
							style:{'text-align':'right'},
							width: 45,
							height: 25
						},		
						viCombo_Vendor(150, 'cboPbf_viInventoriDistribusiFilter'),						
						//-------------- ## --------------
						{
							xtype: 'button',
							text: 'Cari',
							iconCls: 'refresh',
							tooltip: 'Cari',
							style:{paddingLeft:'30px'},
							//rowspan: 3,
							width:150,
							id: 'BtnFilterGridDataView_viInventoriDistribusi',
							handler: function() 
							{					
								DfltFilterBtn_viInventoriDistribusi = 1;
								DataRefresh_viInventoriDistribusi(getCriteriaFilterGridDataView_viInventoriDistribusi());
							}                        
						},
						//-------------- ## --------------
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viInventoriDistribusi;
    //-------------- # End form filter # --------------
}
// End Function dataGrid_viInventoriDistribusi # --------------

/**
*	Function : setLookUp_viInventoriDistribusi
*	
*	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
*/

function setLookUp_viInventoriDistribusi(rowdata)
{
    var lebar = 985;
    setLookUps_viInventoriDistribusi = new Ext.Window
    (
    {
        id: 'SetLookUps_viInventoriDistribusi',
		name: 'SetLookUps_viInventoriDistribusi',
        title: NamaForm_viInventoriDistribusi, 
        closeAction: 'destroy',        
        width: 1000,
        height: 605,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viInventoriDistribusi(lebar,rowdata), //1
        listeners:
        {
            activate: function()
            {
				
            },
            afterShow: function()
            {
                this.activate();
            },
            deactivate: function()
            {
                rowSelected_viInventoriDistribusi=undefined;
                //datarefresh_viInventoriDistribusi();
				mNoKunjungan_viInventoriDistribusi = '';
            }
        }
    }
    );

    setLookUps_viInventoriDistribusi.show();
    if (rowdata == undefined)
    {
        // dataaddnew_viInventoriDistribusi();
		// Ext.getCmp('btnDelete_viInventoriDistribusi').disable();	
    }
    else
    {
        // datainit_viInventoriDistribusi(rowdata);
    }
}
// End Function setLookUpGridDataView_viInventoriDistribusi # --------------

/**
*	Function : getFormItemEntry_viInventoriDistribusi
*	
*	Sebuah fungsi untuk menampilkan penataan menu dan isian form edit
*/

function getFormItemEntry_viInventoriDistribusi(lebar,rowdata)
{
    var pnlFormDataBasic_viInventoriDistribusi = new Ext.FormPanel
    (
		{
			title: '',
			region: 'north',
			layout: 'form',
			bodyStyle: 'padding:10px 10px 10px 10px',
			anchor: '100%',
			labelWidth: 1,
			autoWidth: true,
			width: lebar,
			border: false,
			//-------------- #items# --------------
			items:
			[
				getItemPanelInput_viInventoriDistribusi(lebar),
				//-------------- ## -------------- 				
				getItemGridTransaksi_viInventoriDistribusi(lebar),
				//-------------- ## --------------
				
			],
			//-------------- #End items# --------------
			fileUpload: true,
			// Tombol pada tollbar Edit Data Pasien
			tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						id: 'btnAdd_viInventoriDistribusi',
						handler: function(){
							dataaddnew_viInventoriDistribusi();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viInventoriDistribusi',
						handler: function()
						{
							datasave_viInventoriDistribusi(addNew_viInventoriDistribusi);
							datarefresh_viInventoriDistribusi();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						id: 'btnSimpanExit_viInventoriDistribusi',
						handler: function()
						{
							var x = datasave_viInventoriDistribusi(addNew_viInventoriDistribusi);
							datarefresh_viInventoriDistribusi();
							if (x===undefined)
							{
								setLookUps_viInventoriDistribusi.close();
							}
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viInventoriDistribusi',
						handler: function()
						{
							datadelete_viInventoriDistribusi();
							datarefresh_viInventoriDistribusi();
							
						}
					},
					//-------------- ## --------------
					{
						xtype:'tbseparator'
					},
					
				]
			}//,items:
		}
    )

    return pnlFormDataBasic_viInventoriDistribusi;
}
// End Function getFormItemEntry_viInventoriDistribusi # --------------

/**
*	Function : getItemPanelInput_viInventoriDistribusi
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemPanelInput_viInventoriDistribusi(lebar) 
{
    
    var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		labelWidth: 100,
		autoWidth: true,
		border:true,
		items:
		[			
			{
                xtype: 'compositefield',
                fieldLabel: 'No. Keluar',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					{
						xtype: 'textfield',
						flex: 1,
						width : 120,	
						name: 'txtNoTerima_viInventoriDistribusi',
						id: 'txtNoTerima_viInventoriDistribusi',
						emptyText: 'No. Keluar',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},                    
					{
						xtype: 'checkbox',
						id: 'chkPostedInventoriDistribusi',
						name: 'chkPostedInventoriDistribusi',
						disabled: false,
						autoWidth: false,		
						style: { 'margin-top': '2px' },							
						boxLabel: 'Stok Update ',
						width: 490//90
					},
					{
						xtype: 'displayfield',
						flex: 1,
						width: 80,
						name: '',
						value: 'Tanggal :',
						fieldLabel: 'Label'
					},					
					{
						xtype: 'datefield',
						id: 'txtDateTanggal_viInventoriDistribusi',
						value: now_viInventoriDistribusi,
						format: 'd/M/Y',
						width: 120,
						listeners:
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									datarefresh_viInventoriDistribusi();								
								} 						
							}
						}
					},									                                        					
                ]
            },            
			{
				xtype: 'compositefield',
				fieldLabel: 'Bagian',
				anchor: '100%',
				//labelSeparator: '',
				width: 199,
				items: 
				[
					viCombo_Vendor(825, 'cboPbf_viInventoriDistribusi')					
				]
			},
			{
				xtype: 'compositefield',
				fieldLabel: 'Keterangan',
				anchor: '100%',
				//labelSeparator: '',
				width: 199,
				items: 
				[
					{
						xtype: 'textfield',
						flex: 1,
						width : 825,	
						name: 'txtKeterangan_viInventoriDistribusi',
						id: 'txtKeterangan_viInventoriDistribusi',
						emptyText: 'Keterangan',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},										
				]
			},
			{
				xtype: 'compositefield',
				fieldLabel: 'No. SPK',
				anchor: '100%',
				//labelSeparator: '',
				width: 199,
				items: 
				[
					{
						xtype: 'textfield',
						flex: 1,
						width : 615,	
						name: 'txtNoSPK_viInventoriDistribusi',
						id: 'txtNoSPK_viInventoriDistribusi',
						emptyText: 'No SPK',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},
					{
						xtype: 'displayfield',
						flex: 1,
						width: 80,
						name: '',
						value: 'Tanggal SPK:',
						fieldLabel: 'Label'
					},					
					{
						xtype: 'datefield',
						id: 'txtDateTanggalSPK_viInventoriDistribusi',
						value: now_viInventoriDistribusi,
						format: 'd/M/Y',
						width: 120,
						listeners:
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									datarefresh_viInventoriDistribusi();								
								} 						
							}
						}
					},									                                        				
				]
			},
		]
	};
    return items;
};
// End Function getItemPanelInput_viInventoriDistribusi # --------------


/**
*	Function : getItemGridTransaksi_viInventoriDistribusi
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemGridTransaksi_viInventoriDistribusi(lebar) 
{
    var items =
	{
		//title: 'Detail Transaksi', 
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:1px 1px 1px 1px',
		border:true,
		width: lebar-80,
		height: 400,//347,//225, 
	    items:
		[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:
				[
					gridDataViewEdit_viInventoriDistribusi()
				]	
			}
			//-------------- ## --------------
		]
	};
    return items;
};
// End Function getItemGridTransaksi_viInventoriDistribusi # --------------

/**
*	Function : gridDataViewEdit_viInventoriDistribusi
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function gridDataViewEdit_viInventoriDistribusi()
{
    
    chkSelected_viInventoriDistribusi = new Ext.grid.CheckColumn
	(
		{
			id: 'chkSelected_viInventoriDistribusi',
			header: '',
			align: 'center',						
			dataIndex: 'SELECTED',			
			width: 20
		}
	);

    var FieldGrdKasir_viInventoriDistribusi = 
	[
	];
	
    dsDataGrdJab_viInventoriDistribusi= new WebApp.DataStore
	({
        fields: FieldGrdKasir_viInventoriDistribusi
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJab_viInventoriDistribusi,
        height: 395,//220,
		selModel: new Ext.grid.RowSelectionModel
        (
	        {
	            singleSelect: true,
	            listeners:
	            {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
	        }
        ),
        columns: 
		[
			chkSelected_viInventoriDistribusi,			
			{			
				dataIndex: '',
				header: 'RO Number',
				sortable: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Kode',
				sortable: true,
				width: 80,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Uraian',
				sortable: true,
				width: 300,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Satuan',
				sortable: true,
				width: 80,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'QtyBox',
				sortable: true,
				width: 50,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Frac',
				sortable: true,
				width: 50,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Qty',
				sortable: true,
				width: 50,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Harga',
				sortable: true,
				width: 80,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Jumlah',
				sortable: true,
				width: 80,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Kepemilikan',
				sortable: true,
				width: 80,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------			
        ],
        plugins:chkSelected_viInventoriDistribusi,
    }    
    return items;
}
// End Function gridDataViewEdit_viInventoriDistribusi # --------------


function viCombo_Vendor(lebar,Nama_ID)
{
    var Field_Vendor = ['KD_VENDOR', 'KD_CUSTOMER', 'Bagian'];
    ds_Vendor = new WebApp.DataStore({fields: Field_Vendor});
    
	// viRefresh_Vendor();
	
    var cbo_Vendor = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'Bagian',
			valueField: 'KD_VENDOR',
            displayField: 'Bagian',
			emptyText:'Bagian',
			store: ds_Vendor,
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',                        
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
			listeners:
			{ 
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_Vendor;
}