// Data Source ExtJS # --------------

/**
*	Nama File 		: TRInventoriPembelian.js
*	Menu 			: 
*	Model id 		: 
*	Keterangan 		: Pembelian Inventory
*	Di buat tanggal : 21 Agustus 2014
*	Oleh 			: SDY_RI
*/

// Deklarasi Variabel pada Inventori Perencanaan # --------------

var dataSource_viInventoriPembelian;
var selectCount_viInventoriPembelian=50;
var NamaForm_viInventoriPembelian="Purchase Order/Pembelian";
var mod_name_viInventoriPembelian="viInventoriPembelian";
var now_viInventoriPembelian= new Date();
var addNew_viInventoriPembelian;
var rowSelected_viInventoriPembelian;
var setLookUps_viInventoriPembelian;
var mNoKunjungan_viInventoriPembelian='';

var CurrentData_viInventoriPembelian =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viInventoriPembelian(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

/**
*	Function : dataGrid_viInventoriPembelian
*	
*	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
*/

function dataGrid_viInventoriPembelian(mod_id_viInventoriPembelian)
{	
    // Field kiriman dari Project Net.
    var FieldMaster_viInventoriPembelian = 
	[
		 'NO_KUNJUNGAN', 'KD_KELOMPOK', 'KD_UNIT', 'KD_DOKTER', 'KD_CUSTOMER', 'KD_PASIEN', 
		 'TGL_KUNJUNGAN','JAM_KUNJUNGAN', 'TINGGI_BADAN', 'BERAT_BADAN', 'TEKANAN_DRH', 
		 'NADI','ALERGI', 'KELUHAN', 'RUJUK_RAD', 'RUJUK_LAB', 'TAHAP_PROSES', 'PASIEN_BARU',
		 'NAMA_UNIT','KELOMPOK', 'DOKTER', 'CUSTOMER', 'PS_BARU','KD_PENDIDIKAN','KD_STS_MARITAL', 
		 'KD_AGAMA','KD_PEKERJAAN','NAMA','TEMPAT_LAHIR','TGL_LAHIR','JENIS_KELAMIN','ALAMAT', 
		 'NO_TELP','NO_HP','GOL_DARAH','PENDIDIKAN','STS_MARITAL','AGAMA','PEKERJAAN','JNS_KELAMIN',
		 'TAHUN','BULAN','HARI'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viInventoriPembelian = new WebApp.DataStore
	({
        fields: FieldMaster_viInventoriPembelian
    });
    
    // Grid Inventori Perencanaan # --------------
	var GridDataView_viInventoriPembelian = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viInventoriPembelian,
			autoScroll: true,
			columnLines: true,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viInventoriPembelian = undefined;
							rowSelected_viInventoriPembelian = dataSource_viInventoriPembelian.getAt(row);
							CurrentData_viInventoriPembelian
							CurrentData_viInventoriPembelian.row = row;
							CurrentData_viInventoriPembelian.data = rowSelected_viInventoriPembelian.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viInventoriPembelian = dataSource_viInventoriPembelian.getAt(ridx);
					if (rowSelected_viInventoriPembelian != undefined)
					{
						setLookUp_viInventoriPembelian(rowSelected_viInventoriPembelian.data);
					}
					else
					{
						setLookUp_viInventoriPembelian();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Inventori perencanaan
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colPoNumber_viInventoriPembelian',
						header: 'Po. Number',
						dataIndex: '',
						sortable: true,
						width: 35,
						filter:
						{
							type: 'int'
						}
					},
					//-------------- ## --------------
					{
						id: 'colTglRO_viInventoriPembelian',
						header:'Tanggal',
						dataIndex: '',						
						width: 20,
						sortable: true,
						// format: 'd/M/Y',
						filter: {},
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.TGL_RO);
						}
					},
					//-------------- ## --------------
					{
						id: 'colKeterangan_viInventoriPembelian',
						header: 'PBF',
						dataIndex: 'PBF',
						sortable: true,
						width: 60,
						filter:
						{}
					}
					//-------------- ## --------------
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viInventoriPembelian',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viInventoriPembelian',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viInventoriPembelian != undefined)
							{
								setLookUp_viInventoriPembelian(rowSelected_viInventoriPembelian.data)
							}
							else
							{								
								setLookUp_viInventoriPembelian();
							}
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			bbar : bbar_paging(mod_name_viInventoriPembelian, selectCount_viInventoriPembelian, dataSource_viInventoriPembelian),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viInventoriPembelian = new Ext.Panel
    (
		{
			title: NamaForm_viInventoriPembelian,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viInventoriPembelian,
			region: 'center',
			layout: 'form', 
			closable: true,        
			border: false,  
			margins: '0 5 5 0',
			items: [GridDataView_viInventoriPembelian],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viInventoriPembelian,
		            columns: 11,
		            defaults: {
		                scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
						//-------------- ## --------------
			            { 
							xtype: 'tbtext', 
							text: 'Po. Number : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},						
						{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_RoNumber_viInventoriPembelian',
							emptyText: 'Po. Number',
							width: 100,
							height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										
									} 						
								}
							}
						},
						{	 
							xtype: 'tbspacer',
							width: 10,
							height: 25
						},						
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Tanggal : ', 
							style:{'text-align':'right'},
							width: 60,
							height: 25
						},
						{
							xtype: 'datefield',
							id: 'txtfilterTglAwal_viInventoriPembelian',
							value: now_viInventoriPembelian,
							format: 'd/M/Y',
							width: 120,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viInventoriPembelian();								
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: ' s.d ', 
							style:{'text-align':'center'},
							width: 30,
							height: 25
						},																								
						//-------------- ## --------------
						{
							xtype: 'datefield',
							id: 'txtfilterTglKunjAkhir_viInventoriPembelian',
							value: now_viInventoriPembelian,
							format: 'd/M/Y',
							width: 120,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viInventoriPembelian();								
									} 						
								}
							}
						},							
						{	 
							xtype: 'tbspacer',
							width: 10,
							height: 25
						},
						{ 
							xtype: 'tbtext', 
							text: 'PBF : ', 
							style:{'text-align':'right'},
							width: 30,
							height: 25
						},		
						viCombo_Vendor(150, 'cboPbf_viInventoriPembelianFilter'),						
						//-------------- ## --------------
						{
							xtype: 'button',
							text: 'Cari',
							iconCls: 'refresh',
							tooltip: 'Cari',
							style:{paddingLeft:'30px'},
							//rowspan: 3,
							width:150,
							id: 'BtnFilterGridDataView_viInventoriPembelian',
							handler: function() 
							{					
								DfltFilterBtn_viInventoriPembelian = 1;
								DataRefresh_viInventoriPembelian(getCriteriaFilterGridDataView_viInventoriPembelian());
							}                        
						},
						//-------------- ## --------------
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viInventoriPembelian;
    //-------------- # End form filter # --------------
}
// End Function dataGrid_viInventoriPembelian # --------------

/**
*	Function : setLookUp_viInventoriPembelian
*	
*	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
*/

function setLookUp_viInventoriPembelian(rowdata)
{
    var lebar = 985;
    setLookUps_viInventoriPembelian = new Ext.Window
    (
    {
        id: 'SetLookUps_viInventoriPembelian',
		name: 'SetLookUps_viInventoriPembelian',
        title: NamaForm_viInventoriPembelian, 
        closeAction: 'destroy',        
        width: 1000,
        height: 605,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viInventoriPembelian(lebar,rowdata), //1
        listeners:
        {
            activate: function()
            {
				
            },
            afterShow: function()
            {
                this.activate();
            },
            deactivate: function()
            {
                rowSelected_viInventoriPembelian=undefined;
                //datarefresh_viInventoriPembelian();
				mNoKunjungan_viInventoriPembelian = '';
            }
        }
    }
    );

    setLookUps_viInventoriPembelian.show();
    if (rowdata == undefined)
    {
        // dataaddnew_viInventoriPembelian();
		// Ext.getCmp('btnDelete_viInventoriPembelian').disable();	
    }
    else
    {
        // datainit_viInventoriPembelian(rowdata);
    }
}
// End Function setLookUpGridDataView_viInventoriPembelian # --------------

/**
*	Function : getFormItemEntry_viInventoriPembelian
*	
*	Sebuah fungsi untuk menampilkan penataan menu dan isian form edit
*/

function getFormItemEntry_viInventoriPembelian(lebar,rowdata)
{
    var pnlFormDataBasic_viInventoriPembelian = new Ext.FormPanel
    (
		{
			title: '',
			region: 'north',
			layout: 'form',
			bodyStyle: 'padding:10px 10px 10px 10px',
			anchor: '100%',
			labelWidth: 1,
			autoWidth: true,
			width: lebar,
			border: false,
			//-------------- #items# --------------
			items:
			[
				getItemPanelInputBiodata_viInventoriPembelian(lebar),
				//-------------- ## -------------- 				
				getItemGridTransaksi_viInventoriPembelian(lebar),
				//-------------- ## --------------
				{
					xtype: 'compositefield',
					fieldLabel: ' ',
					labelSeparator: '',
					name: 'compChkPosted_viInventoriPembelian',
					id: 'compChkPosted_viInventoriPembelian',
					items: 
					[
						{
							xtype: 'checkbox',
							id: 'chkPostedInventoriPembelian',
							name: 'chkPostedInventoriPembelian',
							disabled: false,
							autoWidth: false,		
							style: { 'margin-top': '2px' },							
							boxLabel: 'Posted ',
							width: 70
						},						
						{
							xtype: 'displayfield',				
							width: 70,								
							value: 'Sub Total :',
							fieldLabel: 'Label',
							style:{'text-align':'right','margin-top':'3px','margin-left':'720px'},
						},
						{
		                    xtype: 'textfield',
		                    id: 'txtDRGridJmlEditData_viInventoriPembelian',
		                    name: 'txtDRGridJmlEditData_viInventoriPembelian',
							style:{'text-align':'right','margin-top':'3px','margin-left':'730px'},
		                    width: 80,
		                    value: 0,
		                    readOnly: true,
		                },												
		            ],
		        },
				{
					xtype: 'compositefield',
					fieldLabel: ' ',
					labelSeparator: '',
					name: 'compChkUpdateHB_viInventoriPembelian',
					id: 'compChkUpdateHB_viInventoriPembelian',
					items: 
					[						
						{
							xtype: 'displayfield',				
							width: 65,								
							value: 'Total : ',
							fieldLabel: 'Label',
							style:{'text-align':'right','margin-top':'3px','margin-left':'800px'},
							
						},						
						{
		                    xtype: 'textfield',
		                    id: 'txtDRGridPpnEditData_viInventoriPembelian',
		                    name: 'txtDRGridPpnEditData_viInventoriPembelian',
							style:{'text-align':'right','margin-top':'3px','margin-left':'810px'},
		                    width: 80,
		                    value: 0,
		                    readOnly: true,
		                },																		
		            ],
		        },
                //-------------- ## --------------
			],
			//-------------- #End items# --------------
			fileUpload: true,
			// Tombol pada tollbar Edit Data Pasien
			tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						id: 'btnAdd_viInventoriPembelian',
						handler: function(){
							dataaddnew_viInventoriPembelian();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viInventoriPembelian',
						handler: function()
						{
							datasave_viInventoriPembelian(addNew_viInventoriPembelian);
							datarefresh_viInventoriPembelian();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						id: 'btnSimpanExit_viInventoriPembelian',
						handler: function()
						{
							var x = datasave_viInventoriPembelian(addNew_viInventoriPembelian);
							datarefresh_viInventoriPembelian();
							if (x===undefined)
							{
								setLookUps_viInventoriPembelian.close();
							}
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viInventoriPembelian',
						handler: function()
						{
							datadelete_viInventoriPembelian();
							datarefresh_viInventoriPembelian();
							
						}
					},
					//-------------- ## --------------
					{
						xtype:'tbseparator'
					},
					
				]
			}//,items:
		}
    )

    return pnlFormDataBasic_viInventoriPembelian;
}
// End Function getFormItemEntry_viInventoriPembelian # --------------

/**
*	Function : getItemPanelInputBiodata_viInventoriPembelian
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemPanelInputBiodata_viInventoriPembelian(lebar) 
{
    
    var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		labelWidth: 100,
		autoWidth: true,
		border:true,
		items:
		[			
			{
                xtype: 'compositefield',
                fieldLabel: 'Po. Number',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					{
						xtype: 'textfield',
						flex: 1,
						width : 120,	
						name: 'txtPoNumber_viInventoriPembelian',
						id: 'txtPoNumber_viInventoriPembelian',
						emptyText: 'Po. Number',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},                    
                    //-------------- ## --------------  
					{
						xtype: 'displayfield',
						flex: 1,
						width: 80,
						name: '',
						value: 'PBF :',
						fieldLabel: 'Label'
					},					
					viCombo_Vendor(415, 'cboPbf_viInventoriPembelian')					
                ]
            },
            //-------------- ## --------------  
			{
				xtype: 'compositefield',
				fieldLabel: 'Tanggal',
				anchor: '100%',
				//labelSeparator: '',
				width: 199,
				items: 
				[
					{
						xtype: 'datefield',
						id: 'txtDateTanggal_viInventoriPembelian',
						value: now_viInventoriPembelian,
						format: 'd/M/Y',
						width: 120,
						listeners:
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									datarefresh_viInventoriPembelian();								
								} 						
							}
						}
					},
					{
						xtype: 'displayfield',
						flex: 1,
						width: 80,
						name: '',
						value: 'Keterangan :',
						fieldLabel: 'Label'
					},					
					{
						xtype: 'textfield',
						flex: 1,
						width : 415,	
						name: 'txtKeterangan_viInventoriPembelian',
						id: 'txtKeterangan_viInventoriPembelian',
						emptyText: 'Keterangan',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},                                        					
				]
			}
		]
	};
    return items;
};
// End Function getItemPanelInputBiodata_viInventoriPembelian # --------------


/**
*	Function : getItemGridTransaksi_viInventoriPembelian
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemGridTransaksi_viInventoriPembelian(lebar) 
{
    var items =
	{
		//title: 'Detail Transaksi', 
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:1px 1px 1px 1px',
		border:true,
		width: lebar-80,
		height: 400,//225, 
	    items:
		[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:
				[
					gridDataViewEdit_viInventoriPembelian()
				]	
			}
			//-------------- ## --------------
		]
	};
    return items;
};
// End Function getItemGridTransaksi_viInventoriPembelian # --------------

/**
*	Function : gridDataViewEdit_viInventoriPembelian
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function gridDataViewEdit_viInventoriPembelian()
{
    
    chkSelected_viInventoriPembelian = new Ext.grid.CheckColumn
	(
		{
			id: 'chkSelected_viInventoriPembelian',
			header: '',
			align: 'center',						
			dataIndex: 'SELECTED',			
			width: 20
		}
	);

    var FieldGrdKasir_viInventoriPembelian = 
	[
	];
	
    dsDataGrdJab_viInventoriPembelian= new WebApp.DataStore
	({
        fields: FieldGrdKasir_viInventoriPembelian
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJab_viInventoriPembelian,
        height: 395,//220,
		selModel: new Ext.grid.RowSelectionModel
        (
	        {
	            singleSelect: true,
	            listeners:
	            {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
	        }
        ),
        columns: 
		[
			chkSelected_viInventoriPembelian,			
			{			
				dataIndex: '',
				header: 'RO Number',
				sortable: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Kode',
				sortable: true,
				width: 80,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Uraian',
				sortable: true,
				width: 300,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Satuan',
				sortable: true,
				width: 80,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'QtyBox',
				sortable: true,
				width: 50,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Frac',
				sortable: true,
				width: 50,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Qty',
				sortable: true,
				width: 50,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Harga',
				sortable: true,
				width: 80,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Jumlah',
				sortable: true,
				width: 80,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Kepemilikan',
				sortable: true,
				width: 80,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------			
        ],
        plugins:chkSelected_viInventoriPembelian,
    }    
    return items;
}
// End Function gridDataViewEdit_viInventoriPembelian # --------------


function viCombo_Vendor(lebar,Nama_ID)
{
    var Field_Vendor = ['KD_VENDOR', 'KD_CUSTOMER', 'VENDOR'];
    ds_Vendor = new WebApp.DataStore({fields: Field_Vendor});
    
	// viRefresh_Vendor();
	
    var cbo_Vendor = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'Vendor',
			valueField: 'KD_VENDOR',
            displayField: 'PBF',
			emptyText:'PBF',
			store: ds_Vendor,
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',                        
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
			listeners:
			{ 
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_Vendor;
}