// Data Source ExtJS # --------------

/**
*	Nama File 		: TRInventoriPembelian.js
*	Menu 			: 
*	Model id 		: 
*	Keterangan 		: Pembelian Inventory
*	Di buat tanggal : 21 Agustus 2014
*	Oleh 			: SDY_RI
*/

// Deklarasi Variabel pada Inventori Perencanaan # --------------

var dataSource_viInventoriPenerimaan;
var selectCount_viInventoriPenerimaan=50;
var NamaForm_viInventoriPenerimaan="Penerimaan Barang Habis Pakai";
var mod_name_viInventoriPenerimaan="viInventoriPenerimaan";
var now_viInventoriPenerimaan= new Date();
var addNew_viInventoriPenerimaan;
var rowSelected_viInventoriPenerimaan;
var setLookUps_viInventoriPenerimaan;
var mNoKunjungan_viInventoriPenerimaan='';

var CurrentData_viInventoriPenerimaan =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viInventoriPenerimaan(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

/**
*	Function : dataGrid_viInventoriPenerimaan
*	
*	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
*/

function dataGrid_viInventoriPenerimaan(mod_id_viInventoriPenerimaan)
{	
    // Field kiriman dari Project Net.
    var FieldMaster_viInventoriPenerimaan = 
	[
		 'NO_KUNJUNGAN', 'KD_KELOMPOK', 'KD_UNIT', 'KD_DOKTER', 'KD_CUSTOMER', 'KD_PASIEN', 
		 'TGL_KUNJUNGAN','JAM_KUNJUNGAN', 'TINGGI_BADAN', 'BERAT_BADAN', 'TEKANAN_DRH', 
		 'NADI','ALERGI', 'KELUHAN', 'RUJUK_RAD', 'RUJUK_LAB', 'TAHAP_PROSES', 'PASIEN_BARU',
		 'NAMA_UNIT','KELOMPOK', 'DOKTER', 'CUSTOMER', 'PS_BARU','KD_PENDIDIKAN','KD_STS_MARITAL', 
		 'KD_AGAMA','KD_PEKERJAAN','NAMA','TEMPAT_LAHIR','TGL_LAHIR','JENIS_KELAMIN','ALAMAT', 
		 'NO_TELP','NO_HP','GOL_DARAH','PENDIDIKAN','STS_MARITAL','AGAMA','PEKERJAAN','JNS_KELAMIN',
		 'TAHUN','BULAN','HARI'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viInventoriPenerimaan = new WebApp.DataStore
	({
        fields: FieldMaster_viInventoriPenerimaan
    });
    
    // Grid Inventori Perencanaan # --------------
	var GridDataView_viInventoriPenerimaan = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viInventoriPenerimaan,
			autoScroll: true,
			columnLines: true,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viInventoriPenerimaan = undefined;
							rowSelected_viInventoriPenerimaan = dataSource_viInventoriPenerimaan.getAt(row);
							CurrentData_viInventoriPenerimaan
							CurrentData_viInventoriPenerimaan.row = row;
							CurrentData_viInventoriPenerimaan.data = rowSelected_viInventoriPenerimaan.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viInventoriPenerimaan = dataSource_viInventoriPenerimaan.getAt(ridx);
					if (rowSelected_viInventoriPenerimaan != undefined)
					{
						setLookUp_viInventoriPenerimaan(rowSelected_viInventoriPenerimaan.data);
					}
					else
					{
						setLookUp_viInventoriPenerimaan();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Inventori perencanaan
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colNoTerima_viInventoriPenerimaan',
						header: 'No. Terima',
						dataIndex: '',
						sortable: true,
						width: 35,
						filter:
						{
							type: 'int'
						}
					},
					//-------------- ## --------------
					{
						id: 'colTglRO_viInventoriPenerimaan',
						header:'Tanggal',
						dataIndex: '',						
						width: 20,
						sortable: true,
						// format: 'd/M/Y',
						filter: {},
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.TGL_RO);
						}
					},
					//-------------- ## --------------
					{
						id: 'colKeterangan_viInventoriPenerimaan',
						header: 'Vendor',
						dataIndex: '',
						sortable: true,
						width: 60,
						filter:
						{}
					}
					//-------------- ## --------------
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viInventoriPenerimaan',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viInventoriPenerimaan',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viInventoriPenerimaan != undefined)
							{
								setLookUp_viInventoriPenerimaan(rowSelected_viInventoriPenerimaan.data)
							}
							else
							{								
								setLookUp_viInventoriPenerimaan();
							}
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			bbar : bbar_paging(mod_name_viInventoriPenerimaan, selectCount_viInventoriPenerimaan, dataSource_viInventoriPenerimaan),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viInventoriPenerimaan = new Ext.Panel
    (
		{
			title: NamaForm_viInventoriPenerimaan,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viInventoriPenerimaan,
			region: 'center',
			layout: 'form', 
			closable: true,        
			border: false,  
			margins: '0 5 5 0',
			items: [GridDataView_viInventoriPenerimaan],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viInventoriPenerimaan,
		            columns: 11,
		            defaults: {
		                scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
						//-------------- ## --------------
			            { 
							xtype: 'tbtext', 
							text: 'No. Terima : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},						
						{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_NoTerima_viInventoriPenerimaan',
							emptyText: 'No. Terima',
							width: 100,
							height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										
									} 						
								}
							}
						},
						{	 
							xtype: 'tbspacer',
							width: 10,
							height: 25
						},						
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Tanggal : ', 
							style:{'text-align':'right'},
							width: 60,
							height: 25
						},
						{
							xtype: 'datefield',
							id: 'txtfilterTglAwal_viInventoriPenerimaan',
							value: now_viInventoriPenerimaan,
							format: 'd/M/Y',
							width: 120,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viInventoriPenerimaan();								
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: ' s.d ', 
							style:{'text-align':'center'},
							width: 30,
							height: 25
						},																								
						//-------------- ## --------------
						{
							xtype: 'datefield',
							id: 'txtfilterTglKunjAkhir_viInventoriPenerimaan',
							value: now_viInventoriPenerimaan,
							format: 'd/M/Y',
							width: 120,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viInventoriPenerimaan();								
									} 						
								}
							}
						},							
						{	 
							xtype: 'tbspacer',
							width: 10,
							height: 25
						},
						{ 
							xtype: 'tbtext', 
							text: 'Vendor : ', 
							style:{'text-align':'right'},
							width: 45,
							height: 25
						},		
						viCombo_Vendor(150, 'cboPbf_viInventoriPenerimaanFilter'),						
						//-------------- ## --------------
						{
							xtype: 'button',
							text: 'Cari',
							iconCls: 'refresh',
							tooltip: 'Cari',
							style:{paddingLeft:'30px'},
							//rowspan: 3,
							width:150,
							id: 'BtnFilterGridDataView_viInventoriPenerimaan',
							handler: function() 
							{					
								DfltFilterBtn_viInventoriPenerimaan = 1;
								DataRefresh_viInventoriPenerimaan(getCriteriaFilterGridDataView_viInventoriPenerimaan());
							}                        
						},
						//-------------- ## --------------
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viInventoriPenerimaan;
    //-------------- # End form filter # --------------
}
// End Function dataGrid_viInventoriPenerimaan # --------------

/**
*	Function : setLookUp_viInventoriPenerimaan
*	
*	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
*/

function setLookUp_viInventoriPenerimaan(rowdata)
{
    var lebar = 985;
    setLookUps_viInventoriPenerimaan = new Ext.Window
    (
    {
        id: 'SetLookUps_viInventoriPenerimaan',
		name: 'SetLookUps_viInventoriPenerimaan',
        title: NamaForm_viInventoriPenerimaan, 
        closeAction: 'destroy',        
        width: 1000,
        height: 605,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viInventoriPenerimaan(lebar,rowdata), //1
        listeners:
        {
            activate: function()
            {
				
            },
            afterShow: function()
            {
                this.activate();
            },
            deactivate: function()
            {
                rowSelected_viInventoriPenerimaan=undefined;
                //datarefresh_viInventoriPenerimaan();
				mNoKunjungan_viInventoriPenerimaan = '';
            }
        }
    }
    );

    setLookUps_viInventoriPenerimaan.show();
    if (rowdata == undefined)
    {
        // dataaddnew_viInventoriPenerimaan();
		// Ext.getCmp('btnDelete_viInventoriPenerimaan').disable();	
    }
    else
    {
        // datainit_viInventoriPenerimaan(rowdata);
    }
}
// End Function setLookUpGridDataView_viInventoriPenerimaan # --------------

/**
*	Function : getFormItemEntry_viInventoriPenerimaan
*	
*	Sebuah fungsi untuk menampilkan penataan menu dan isian form edit
*/

function getFormItemEntry_viInventoriPenerimaan(lebar,rowdata)
{
    var pnlFormDataBasic_viInventoriPenerimaan = new Ext.FormPanel
    (
		{
			title: '',
			region: 'north',
			layout: 'form',
			bodyStyle: 'padding:10px 10px 10px 10px',
			anchor: '100%',
			labelWidth: 1,
			autoWidth: true,
			width: lebar,
			border: false,
			//-------------- #items# --------------
			items:
			[
				getItemPanelInputBiodata_viInventoriPenerimaan(lebar),
				//-------------- ## -------------- 				
				getItemGridTransaksi_viInventoriPenerimaan(lebar),
				//-------------- ## --------------
				{
					xtype: 'compositefield',
					fieldLabel: ' ',
					labelSeparator: '',
					name: 'compChkPosted_viInventoriPenerimaan',
					id: 'compChkPosted_viInventoriPenerimaan',
					items: 
					[
						{
							xtype: 'button',
							text: 'Acc. Approval',							
							id: 'btnAccApp_viInventoriPenerimaan',
							style:{'margin-top':'3px'},
							handler: function(sm, row, rec)
							{
								
							}
						},						
						{
							xtype: 'displayfield',				
							width: 70,								
							value: ' Total :',
							fieldLabel: 'Label',
							style:{'text-align':'right','margin-top':'3px','margin-left':'720px'},
						},
						{
		                    xtype: 'textfield',
		                    id: 'txtDRGridTotalData_viInventoriPenerimaan',
		                    name: 'txtDRGridTotalData_viInventoriPenerimaan',
							style:{'text-align':'right','margin-top':'3px','margin-left':'720px'},
		                    width: 80,
		                    value: 0,
		                    readOnly: true,
		                },												
		            ],
		        },				
			],
			//-------------- #End items# --------------
			fileUpload: true,
			// Tombol pada tollbar Edit Data Pasien
			tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						id: 'btnAdd_viInventoriPenerimaan',
						handler: function(){
							dataaddnew_viInventoriPenerimaan();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viInventoriPenerimaan',
						handler: function()
						{
							datasave_viInventoriPenerimaan(addNew_viInventoriPenerimaan);
							datarefresh_viInventoriPenerimaan();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						id: 'btnSimpanExit_viInventoriPenerimaan',
						handler: function()
						{
							var x = datasave_viInventoriPenerimaan(addNew_viInventoriPenerimaan);
							datarefresh_viInventoriPenerimaan();
							if (x===undefined)
							{
								setLookUps_viInventoriPenerimaan.close();
							}
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viInventoriPenerimaan',
						handler: function()
						{
							datadelete_viInventoriPenerimaan();
							datarefresh_viInventoriPenerimaan();
							
						}
					},
					//-------------- ## --------------
					{
						xtype:'tbseparator'
					},
					
				]
			}//,items:
		}
    )

    return pnlFormDataBasic_viInventoriPenerimaan;
}
// End Function getFormItemEntry_viInventoriPenerimaan # --------------

/**
*	Function : getItemPanelInputBiodata_viInventoriPenerimaan
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemPanelInputBiodata_viInventoriPenerimaan(lebar) 
{
    
    var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		labelWidth: 100,
		autoWidth: true,
		border:true,
		items:
		[			
			{
                xtype: 'compositefield',
                fieldLabel: 'No. Terima',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					{
						xtype: 'textfield',
						flex: 1,
						width : 120,	
						name: 'txtNoTerima_viInventoriPenerimaan',
						id: 'txtNoTerima_viInventoriPenerimaan',
						emptyText: 'No. Terima',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},                    
					{
						xtype: 'checkbox',
						id: 'chkPostedInventoriPenerimaan',
						name: 'chkPostedInventoriPenerimaan',
						disabled: false,
						autoWidth: false,		
						style: { 'margin-top': '2px' },							
						boxLabel: 'Posted ',
						width: 70
					},                    
                ]
            },
            //-------------- ## --------------  
			{
				xtype: 'compositefield',
				fieldLabel: 'No. Faktur',
				anchor: '100%',
				//labelSeparator: '',
				width: 199,
				items: 
				[
					{
						xtype: 'textfield',
						flex: 1,
						width : 615,	
						name: 'txtNoFaktur_viInventoriPenerimaan',
						id: 'txtNoFaktur_viInventoriPenerimaan',
						emptyText: 'No Faktur',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},
					{
						xtype: 'displayfield',
						flex: 1,
						width: 80,
						name: '',
						value: 'Tanggal :',
						fieldLabel: 'Label'
					},					
					{
						xtype: 'datefield',
						id: 'txtDateTanggal_viInventoriPenerimaan',
						value: now_viInventoriPenerimaan,
						format: 'd/M/Y',
						width: 120,
						listeners:
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									datarefresh_viInventoriPenerimaan();								
								} 						
							}
						}
					},									                                        				
				]
			},
			{
				xtype: 'compositefield',
				fieldLabel: 'Vendor',
				anchor: '100%',
				//labelSeparator: '',
				width: 199,
				items: 
				[
					viCombo_Vendor(825, 'cboPbf_viInventoriPenerimaan')					
				]
			},
			{
				xtype: 'compositefield',
				fieldLabel: 'Keterangan',
				anchor: '100%',
				//labelSeparator: '',
				width: 199,
				items: 
				[
					{
						xtype: 'textfield',
						flex: 1,
						width : 825,	
						name: 'txtKeterangan_viInventoriPenerimaan',
						id: 'txtKeterangan_viInventoriPenerimaan',
						emptyText: 'Keterangan',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},										
				]
			},
			{
				xtype: 'compositefield',
				fieldLabel: 'No. SPK',
				anchor: '100%',
				//labelSeparator: '',
				width: 199,
				items: 
				[
					{
						xtype: 'textfield',
						flex: 1,
						width : 615,	
						name: 'txtNoSPK_viInventoriPenerimaan',
						id: 'txtNoSPK_viInventoriPenerimaan',
						emptyText: 'No SPK',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},
					{
						xtype: 'displayfield',
						flex: 1,
						width: 80,
						name: '',
						value: 'Tanggal SPK:',
						fieldLabel: 'Label'
					},					
					{
						xtype: 'datefield',
						id: 'txtDateTanggalSPK_viInventoriPenerimaan',
						value: now_viInventoriPenerimaan,
						format: 'd/M/Y',
						width: 120,
						listeners:
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									datarefresh_viInventoriPenerimaan();								
								} 						
							}
						}
					},									                                        				
				]
			},
		]
	};
    return items;
};
// End Function getItemPanelInputBiodata_viInventoriPenerimaan # --------------


/**
*	Function : getItemGridTransaksi_viInventoriPenerimaan
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemGridTransaksi_viInventoriPenerimaan(lebar) 
{
    var items =
	{
		//title: 'Detail Transaksi', 
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:1px 1px 1px 1px',
		border:true,
		width: lebar-80,
		height: 347,//225, 
	    items:
		[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:
				[
					gridDataViewEdit_viInventoriPenerimaan()
				]	
			}
			//-------------- ## --------------
		]
	};
    return items;
};
// End Function getItemGridTransaksi_viInventoriPenerimaan # --------------

/**
*	Function : gridDataViewEdit_viInventoriPenerimaan
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function gridDataViewEdit_viInventoriPenerimaan()
{
    
    chkSelected_viInventoriPenerimaan = new Ext.grid.CheckColumn
	(
		{
			id: 'chkSelected_viInventoriPenerimaan',
			header: '',
			align: 'center',						
			dataIndex: 'SELECTED',			
			width: 20
		}
	);

    var FieldGrdKasir_viInventoriPenerimaan = 
	[
	];
	
    dsDataGrdJab_viInventoriPenerimaan= new WebApp.DataStore
	({
        fields: FieldGrdKasir_viInventoriPenerimaan
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJab_viInventoriPenerimaan,
        height: 395,//220,
		selModel: new Ext.grid.RowSelectionModel
        (
	        {
	            singleSelect: true,
	            listeners:
	            {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
	        }
        ),
        columns: 
		[
			chkSelected_viInventoriPenerimaan,			
			{			
				dataIndex: '',
				header: 'RO Number',
				sortable: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Kode',
				sortable: true,
				width: 80,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Uraian',
				sortable: true,
				width: 300,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Satuan',
				sortable: true,
				width: 80,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'QtyBox',
				sortable: true,
				width: 50,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Frac',
				sortable: true,
				width: 50,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Qty',
				sortable: true,
				width: 50,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Harga',
				sortable: true,
				width: 80,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Jumlah',
				sortable: true,
				width: 80,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Kepemilikan',
				sortable: true,
				width: 80,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------			
        ],
        plugins:chkSelected_viInventoriPenerimaan,
    }    
    return items;
}
// End Function gridDataViewEdit_viInventoriPenerimaan # --------------


function viCombo_Vendor(lebar,Nama_ID)
{
    var Field_Vendor = ['KD_VENDOR', 'KD_CUSTOMER', 'VENDOR'];
    ds_Vendor = new WebApp.DataStore({fields: Field_Vendor});
    
	// viRefresh_Vendor();
	
    var cbo_Vendor = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'Vendor',
			valueField: 'KD_VENDOR',
            displayField: 'VENDOR',
			emptyText:'VENDOR',
			store: ds_Vendor,
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',                        
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
			listeners:
			{ 
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_Vendor;
}