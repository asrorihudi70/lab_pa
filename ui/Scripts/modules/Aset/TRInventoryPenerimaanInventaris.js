// Data Source ExtJS # --------------

/**
*	Nama File 		: TRInventoriPenerimaanInventaris.js
*	Menu 			: 
*	Model id 		: 
*	Keterangan 		: PenerimaanInventaris Inventory
*	Di buat tanggal : 21 Agustus 2014
*	Oleh 			: SDY_RI
*/

// Deklarasi Variabel pada Inventori PenerimaanInventaris # --------------

var dataSource_viInventoriPenerimaanInventaris;
var selectCount_viInventoriPenerimaanInventaris=50;
var NamaForm_viInventoriPenerimaanInventaris="Penerimaan Barang Inventaris";
var mod_name_viInventoriPenerimaanInventaris="viInventoriPenerimaanInventaris";
var now_viInventoriPenerimaanInventaris= new Date();
var addNew_viInventoriPenerimaanInventaris;
var rowSelected_viInventoriPenerimaanInventaris;
var setLookUps_viInventoriPenerimaanInventaris;
var mNoKunjungan_viInventoriPenerimaanInventaris='';

var CurrentData_viInventoriPenerimaanInventaris =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viInventoriPenerimaanInventaris(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

/**
*	Function : dataGrid_viInventoriPenerimaanInventaris
*	
*	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
*/

function dataGrid_viInventoriPenerimaanInventaris(mod_id_viInventoriPenerimaanInventaris)
{	
    // Field kiriman dari Project Net.
    var FieldMaster_viInventoriPenerimaanInventaris = 
	[
		 'NO_KUNJUNGAN', 'KD_KELOMPOK', 'KD_UNIT', 'KD_DOKTER', 'KD_CUSTOMER', 'KD_PASIEN', 
		 'TGL_KUNJUNGAN','JAM_KUNJUNGAN', 'TINGGI_BADAN', 'BERAT_BADAN', 'TEKANAN_DRH', 
		 'NADI','ALERGI', 'KELUHAN', 'RUJUK_RAD', 'RUJUK_LAB', 'TAHAP_PROSES', 'PASIEN_BARU',
		 'NAMA_UNIT','KELOMPOK', 'DOKTER', 'CUSTOMER', 'PS_BARU','KD_PENDIDIKAN','KD_STS_MARITAL', 
		 'KD_AGAMA','KD_PEKERJAAN','NAMA','TEMPAT_LAHIR','TGL_LAHIR','JENIS_KELAMIN','ALAMAT', 
		 'NO_TELP','NO_HP','GOL_DARAH','PENDIDIKAN','STS_MARITAL','AGAMA','PEKERJAAN','JNS_KELAMIN',
		 'TAHUN','BULAN','HARI'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viInventoriPenerimaanInventaris = new WebApp.DataStore
	({
        fields: FieldMaster_viInventoriPenerimaanInventaris
    });
    
    // Grid Inventori Perencanaan # --------------
	var GridDataView_viInventoriPenerimaanInventaris = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viInventoriPenerimaanInventaris,
			autoScroll: true,
			columnLines: true,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viInventoriPenerimaanInventaris = undefined;
							rowSelected_viInventoriPenerimaanInventaris = dataSource_viInventoriPenerimaanInventaris.getAt(row);
							CurrentData_viInventoriPenerimaanInventaris
							CurrentData_viInventoriPenerimaanInventaris.row = row;
							CurrentData_viInventoriPenerimaanInventaris.data = rowSelected_viInventoriPenerimaanInventaris.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viInventoriPenerimaanInventaris = dataSource_viInventoriPenerimaanInventaris.getAt(ridx);
					if (rowSelected_viInventoriPenerimaanInventaris != undefined)
					{
						setLookUp_viInventoriPenerimaanInventaris(rowSelected_viInventoriPenerimaanInventaris.data);
					}
					else
					{
						setLookUp_viInventoriPenerimaanInventaris();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Inventori perencanaan
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colNoTerima_viInventoriPenerimaanInventaris',
						header: 'No. Terima',
						dataIndex: '',
						sortable: true,
						width: 35,
						filter:
						{
							type: 'int'
						}
					},
					//-------------- ## --------------
					{
						id: 'colTglRO_viInventoriPenerimaanInventaris',
						header:'Tanggal',
						dataIndex: '',						
						width: 20,
						sortable: true,
						// format: 'd/M/Y',
						filter: {},
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.TGL_RO);
						}
					},
					//-------------- ## --------------
					{
						id: 'colKeterangan_viInventoriPenerimaanInventaris',
						header: 'Vendor',
						dataIndex: '',
						sortable: true,
						width: 60,
						filter:
						{}
					}
					//-------------- ## --------------
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viInventoriPenerimaanInventaris',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viInventoriPenerimaanInventaris',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viInventoriPenerimaanInventaris != undefined)
							{
								setLookUp_viInventoriPenerimaanInventaris(rowSelected_viInventoriPenerimaanInventaris.data)
							}
							else
							{								
								setLookUp_viInventoriPenerimaanInventaris();
							}
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			bbar : bbar_paging(mod_name_viInventoriPenerimaanInventaris, selectCount_viInventoriPenerimaanInventaris, dataSource_viInventoriPenerimaanInventaris),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viInventoriPenerimaanInventaris = new Ext.Panel
    (
		{
			title: NamaForm_viInventoriPenerimaanInventaris,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viInventoriPenerimaanInventaris,
			region: 'center',
			layout: 'form', 
			closable: true,        
			border: false,  
			margins: '0 5 5 0',
			items: [GridDataView_viInventoriPenerimaanInventaris],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viInventoriPenerimaanInventaris,
		            columns: 11,
		            defaults: {
		                scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
						//-------------- ## --------------
			            { 
							xtype: 'tbtext', 
							text: 'No. Terima : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},						
						{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_NoTerima_viInventoriPenerimaanInventaris',
							emptyText: 'No. Terima',
							width: 100,
							height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										
									} 						
								}
							}
						},
						{	 
							xtype: 'tbspacer',
							width: 10,
							height: 25
						},						
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Tanggal : ', 
							style:{'text-align':'right'},
							width: 60,
							height: 25
						},
						{
							xtype: 'datefield',
							id: 'txtfilterTglAwal_viInventoriPenerimaanInventaris',
							value: now_viInventoriPenerimaanInventaris,
							format: 'd/M/Y',
							width: 120,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viInventoriPenerimaanInventaris();								
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: ' s.d ', 
							style:{'text-align':'center'},
							width: 30,
							height: 25
						},																								
						//-------------- ## --------------
						{
							xtype: 'datefield',
							id: 'txtfilterTglKunjAkhir_viInventoriPenerimaanInventaris',
							value: now_viInventoriPenerimaanInventaris,
							format: 'd/M/Y',
							width: 120,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viInventoriPenerimaanInventaris();								
									} 						
								}
							}
						},							
						{	 
							xtype: 'tbspacer',
							width: 10,
							height: 25
						},
						{ 
							xtype: 'tbtext', 
							text: 'Vendor : ', 
							style:{'text-align':'right'},
							width: 45,
							height: 25
						},		
						viCombo_Vendor(150, 'cboPbf_viInventoriPenerimaanInventarisFilter'),						
						//-------------- ## --------------
						{
							xtype: 'button',
							text: 'Cari',
							iconCls: 'refresh',
							tooltip: 'Cari',
							style:{paddingLeft:'30px'},
							//rowspan: 3,
							width:150,
							id: 'BtnFilterGridDataView_viInventoriPenerimaanInventaris',
							handler: function() 
							{					
								DfltFilterBtn_viInventoriPenerimaanInventaris = 1;
								DataRefresh_viInventoriPenerimaanInventaris(getCriteriaFilterGridDataView_viInventoriPenerimaanInventaris());
							}                        
						},
						//-------------- ## --------------
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viInventoriPenerimaanInventaris;
    //-------------- # End form filter # --------------
}
// End Function dataGrid_viInventoriPenerimaanInventaris # --------------

/**
*	Function : setLookUp_viInventoriPenerimaanInventaris
*	
*	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
*/

function setLookUp_viInventoriPenerimaanInventaris(rowdata)
{
    var lebar = 985;
    setLookUps_viInventoriPenerimaanInventaris = new Ext.Window
    (
    {
        id: 'SetLookUps_viInventoriPenerimaanInventaris',
		name: 'SetLookUps_viInventoriPenerimaanInventaris',
        title: NamaForm_viInventoriPenerimaanInventaris, 
        closeAction: 'destroy',        
        width: 1000,
        height: 605,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viInventoriPenerimaanInventaris(lebar,rowdata), //1
        listeners:
        {
            activate: function()
            {
				
            },
            afterShow: function()
            {
                this.activate();
            },
            deactivate: function()
            {
                rowSelected_viInventoriPenerimaanInventaris=undefined;
                //datarefresh_viInventoriPenerimaanInventaris();
				mNoKunjungan_viInventoriPenerimaanInventaris = '';
            }
        }
    }
    );

    setLookUps_viInventoriPenerimaanInventaris.show();
    if (rowdata == undefined)
    {
        // dataaddnew_viInventoriPenerimaanInventaris();
		// Ext.getCmp('btnDelete_viInventoriPenerimaanInventaris').disable();	
    }
    else
    {
        // datainit_viInventoriPenerimaanInventaris(rowdata);
    }
}
// End Function setLookUpGridDataView_viInventoriPenerimaanInventaris # --------------

/**
*	Function : getFormItemEntry_viInventoriPenerimaanInventaris
*	
*	Sebuah fungsi untuk menampilkan penataan menu dan isian form edit
*/

function getFormItemEntry_viInventoriPenerimaanInventaris(lebar,rowdata)
{
    var pnlFormDataBasic_viInventoriPenerimaanInventaris = new Ext.FormPanel
    (
		{
			title: '',
			region: 'north',
			layout: 'form',
			bodyStyle: 'padding:10px 10px 10px 10px',
			anchor: '100%',
			labelWidth: 1,
			autoWidth: true,
			width: lebar,
			border: false,
			//-------------- #items# --------------
			items:
			[
				getItemPanelInputBiodata_viInventoriPenerimaanInventaris(lebar),
				//-------------- ## -------------- 				
				getItemGridTransaksi_viInventoriPenerimaanInventaris(lebar),
				//-------------- ## --------------
				{
					xtype: 'compositefield',
					fieldLabel: ' ',
					labelSeparator: '',
					name: 'compChkPosted_viInventoriPenerimaanInventaris',
					id: 'compChkPosted_viInventoriPenerimaanInventaris',
					items: 
					[
						{
							xtype: 'button',
							text: 'Lihat No Register Barang',							
							id: 'btnNoRegister_viInventoriPenerimaanInventaris',
							style:{'margin-top':'3px'},
							handler: function(sm, row, rec)
							{
								
							}
						},						
						{
							xtype: 'button',
							text: 'Acc. Approval',							
							id: 'btnAccApp_viInventoriPenerimaanInventaris',
							style:{'margin-top':'3px'},
							handler: function(sm, row, rec)
							{
								
							}
						},						
						{
							xtype: 'displayfield',				
							width: 70,								
							value: ' Total :',
							fieldLabel: 'Label',
							// style:{'text-align':'right','margin-top':'3px','margin-left':'720px'},
							style:{'text-align':'right','margin-top':'3px','margin-left':'580px'},
						},
						{
		                    xtype: 'textfield',
		                    id: 'txtDRGridTotalData_viInventoriPenerimaanInventaris',
		                    name: 'txtDRGridTotalData_viInventoriPenerimaanInventaris',
							style:{'text-align':'right','margin-top':'3px','margin-left':'590px'},
		                    width: 80,
		                    value: 0,
		                    readOnly: true,
		                },												
		            ],
		        },				
			],
			//-------------- #End items# --------------
			fileUpload: true,
			// Tombol pada tollbar Edit Data Pasien
			tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						id: 'btnAdd_viInventoriPenerimaanInventaris',
						handler: function(){
							dataaddnew_viInventoriPenerimaanInventaris();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viInventoriPenerimaanInventaris',
						handler: function()
						{
							datasave_viInventoriPenerimaanInventaris(addNew_viInventoriPenerimaanInventaris);
							datarefresh_viInventoriPenerimaanInventaris();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						id: 'btnSimpanExit_viInventoriPenerimaanInventaris',
						handler: function()
						{
							var x = datasave_viInventoriPenerimaanInventaris(addNew_viInventoriPenerimaanInventaris);
							datarefresh_viInventoriPenerimaanInventaris();
							if (x===undefined)
							{
								setLookUps_viInventoriPenerimaanInventaris.close();
							}
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viInventoriPenerimaanInventaris',
						handler: function()
						{
							datadelete_viInventoriPenerimaanInventaris();
							datarefresh_viInventoriPenerimaanInventaris();
							
						}
					},
					//-------------- ## --------------
					{
						xtype:'tbseparator'
					},
					
				]
			}//,items:
		}
    )

    return pnlFormDataBasic_viInventoriPenerimaanInventaris;
}
// End Function getFormItemEntry_viInventoriPenerimaanInventaris # --------------

/**
*	Function : getItemPanelInputBiodata_viInventoriPenerimaanInventaris
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemPanelInputBiodata_viInventoriPenerimaanInventaris(lebar) 
{
    
    var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		labelWidth: 100,
		autoWidth: true,
		border:true,
		items:
		[			
			{
                xtype: 'compositefield',
                fieldLabel: 'No. Terima',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					{
						xtype: 'textfield',
						flex: 1,
						width : 120,	
						name: 'txtNoTerima_viInventoriPenerimaanInventaris',
						id: 'txtNoTerima_viInventoriPenerimaanInventaris',
						emptyText: 'No. Terima',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},                    
					{
						xtype: 'checkbox',
						id: 'chkPostedInventoriPenerimaanInventaris',
						name: 'chkPostedInventoriPenerimaanInventaris',
						disabled: false,
						autoWidth: false,		
						style: { 'margin-top': '2px' },							
						boxLabel: 'Posted ',
						width: 70
					},                    
                ]
            },
            //-------------- ## --------------  
			{
				xtype: 'compositefield',
				fieldLabel: 'No. Faktur',
				anchor: '100%',
				//labelSeparator: '',
				width: 199,
				items: 
				[
					{
						xtype: 'textfield',
						flex: 1,
						width : 615,	
						name: 'txtNoFaktur_viInventoriPenerimaanInventaris',
						id: 'txtNoFaktur_viInventoriPenerimaanInventaris',
						emptyText: 'No Faktur',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},
					{
						xtype: 'displayfield',
						flex: 1,
						width: 80,
						name: '',
						value: 'Tanggal :',
						fieldLabel: 'Label'
					},					
					{
						xtype: 'datefield',
						id: 'txtDateTanggal_viInventoriPenerimaanInventaris',
						value: now_viInventoriPenerimaanInventaris,
						format: 'd/M/Y',
						width: 120,
						listeners:
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									datarefresh_viInventoriPenerimaanInventaris();								
								} 						
							}
						}
					},									                                        				
				]
			},
			{
				xtype: 'compositefield',
				fieldLabel: 'Vendor',
				anchor: '100%',
				//labelSeparator: '',
				width: 199,
				items: 
				[
					viCombo_Vendor(825, 'cboPbf_viInventoriPenerimaanInventaris')					
				]
			},
			{
				xtype: 'compositefield',
				fieldLabel: 'Keterangan',
				anchor: '100%',
				//labelSeparator: '',
				width: 199,
				items: 
				[
					{
						xtype: 'textfield',
						flex: 1,
						width : 825,	
						name: 'txtKeterangan_viInventoriPenerimaanInventaris',
						id: 'txtKeterangan_viInventoriPenerimaanInventaris',
						emptyText: 'Keterangan',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},										
				]
			},
			{
				xtype: 'compositefield',
				fieldLabel: 'No. SPK',
				anchor: '100%',
				//labelSeparator: '',
				width: 199,
				items: 
				[
					{
						xtype: 'textfield',
						flex: 1,
						width : 615,	
						name: 'txtNoSPK_viInventoriPenerimaanInventaris',
						id: 'txtNoSPK_viInventoriPenerimaanInventaris',
						emptyText: 'No SPK',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},
					{
						xtype: 'displayfield',
						flex: 1,
						width: 80,
						name: '',
						value: 'Tanggal SPK:',
						fieldLabel: 'Label'
					},					
					{
						xtype: 'datefield',
						id: 'txtDateTanggalSPK_viInventoriPenerimaanInventaris',
						value: now_viInventoriPenerimaanInventaris,
						format: 'd/M/Y',
						width: 120,
						listeners:
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									datarefresh_viInventoriPenerimaanInventaris();								
								} 						
							}
						}
					},									                                        				
				]
			},
		]
	};
    return items;
};
// End Function getItemPanelInputBiodata_viInventoriPenerimaanInventaris # --------------


/**
*	Function : getItemGridTransaksi_viInventoriPenerimaanInventaris
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemGridTransaksi_viInventoriPenerimaanInventaris(lebar) 
{
    var items =
	{
		//title: 'Detail Transaksi', 
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:1px 1px 1px 1px',
		border:true,
		width: lebar-80,
		height: 347,//225, 
	    items:
		[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:
				[
					gridDataViewEdit_viInventoriPenerimaanInventaris()
				]	
			}
			//-------------- ## --------------
		]
	};
    return items;
};
// End Function getItemGridTransaksi_viInventoriPenerimaanInventaris # --------------

/**
*	Function : gridDataViewEdit_viInventoriPenerimaanInventaris
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function gridDataViewEdit_viInventoriPenerimaanInventaris()
{
    
    chkSelected_viInventoriPenerimaanInventaris = new Ext.grid.CheckColumn
	(
		{
			id: 'chkSelected_viInventoriPenerimaanInventaris',
			header: '',
			align: 'center',						
			dataIndex: 'SELECTED',			
			width: 20
		}
	);

    var FieldGrdKasir_viInventoriPenerimaanInventaris = 
	[
	];
	
    dsDataGrdJab_viInventoriPenerimaanInventaris= new WebApp.DataStore
	({
        fields: FieldGrdKasir_viInventoriPenerimaanInventaris
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJab_viInventoriPenerimaanInventaris,
        height: 395,//220,
		selModel: new Ext.grid.RowSelectionModel
        (
	        {
	            singleSelect: true,
	            listeners:
	            {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
	        }
        ),
        columns: 
		[
			chkSelected_viInventoriPenerimaanInventaris,			
			{			
				dataIndex: '',
				header: 'RO Number',
				sortable: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Kode',
				sortable: true,
				width: 80,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Uraian',
				sortable: true,
				width: 300,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Satuan',
				sortable: true,
				width: 80,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'QtyBox',
				sortable: true,
				width: 50,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Frac',
				sortable: true,
				width: 50,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Qty',
				sortable: true,
				width: 50,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Harga',
				sortable: true,
				width: 80,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Jumlah',
				sortable: true,
				width: 80,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Kepemilikan',
				sortable: true,
				width: 80,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------			
        ],
        plugins:chkSelected_viInventoriPenerimaanInventaris,
    }    
    return items;
}
// End Function gridDataViewEdit_viInventoriPenerimaanInventaris # --------------


function viCombo_Vendor(lebar,Nama_ID)
{
    var Field_Vendor = ['KD_VENDOR', 'KD_CUSTOMER', 'VENDOR'];
    ds_Vendor = new WebApp.DataStore({fields: Field_Vendor});
    
	// viRefresh_Vendor();
	
    var cbo_Vendor = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'Vendor',
			valueField: 'KD_VENDOR',
            displayField: 'VENDOR',
			emptyText:'VENDOR',
			store: ds_Vendor,
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',                        
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
			listeners:
			{ 
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_Vendor;
}