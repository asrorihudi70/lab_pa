// Data Source ExtJS # --------------

/**
*	Nama File 		: TRInventoriPermintaan.js
*	Menu 			: 
*	Model id 		: 
*	Keterangan 		: Permintaan Inventory
*	Di buat tanggal : 21 Agustus 2014
*	Oleh 			: SDY_RI
*/

// Deklarasi Variabel pada Inventori Permintaan # --------------

var dataSource_viInventoriPermintaan;
var selectCount_viInventoriPermintaan=50;
var NamaForm_viInventoriPermintaan="Permintaan Barang Inventaris";
var mod_name_viInventoriPermintaan="viInventoriPermintaan";
var now_viInventoriPermintaan= new Date();
var addNew_viInventoriPermintaan;
var rowSelected_viInventoriPermintaan;
var setLookUps_viInventoriPermintaan;
var mNoKunjungan_viInventoriPermintaan='';

var CurrentData_viInventoriPermintaan =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viInventoriPermintaan(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

/**
*	Function : dataGrid_viInventoriPermintaan
*	
*	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
*/

function dataGrid_viInventoriPermintaan(mod_id_viInventoriPermintaan)
{	
    // Field kiriman dari Project Net.
    var FieldMaster_viInventoriPermintaan = 
	[
		 'NO_KUNJUNGAN', 'KD_KELOMPOK', 'KD_UNIT', 'KD_DOKTER', 'KD_CUSTOMER', 'KD_PASIEN', 
		 'TGL_KUNJUNGAN','JAM_KUNJUNGAN', 'TINGGI_BADAN', 'BERAT_BADAN', 'TEKANAN_DRH', 
		 'NADI','ALERGI', 'KELUHAN', 'RUJUK_RAD', 'RUJUK_LAB', 'TAHAP_PROSES', 'PASIEN_BARU',
		 'NAMA_UNIT','KELOMPOK', 'DOKTER', 'CUSTOMER', 'PS_BARU','KD_PENDIDIKAN','KD_STS_MARITAL', 
		 'KD_AGAMA','KD_PEKERJAAN','NAMA','TEMPAT_LAHIR','TGL_LAHIR','JENIS_KELAMIN','ALAMAT', 
		 'NO_TELP','NO_HP','GOL_DARAH','PENDIDIKAN','STS_MARITAL','AGAMA','PEKERJAAN','JNS_KELAMIN',
		 'TAHUN','BULAN','HARI'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viInventoriPermintaan = new WebApp.DataStore
	({
        fields: FieldMaster_viInventoriPermintaan
    });
    
    // Grid Inventori Perencanaan # --------------
	var GridDataView_viInventoriPermintaan = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viInventoriPermintaan,
			autoScroll: true,
			columnLines: true,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viInventoriPermintaan = undefined;
							rowSelected_viInventoriPermintaan = dataSource_viInventoriPermintaan.getAt(row);
							CurrentData_viInventoriPermintaan
							CurrentData_viInventoriPermintaan.row = row;
							CurrentData_viInventoriPermintaan.data = rowSelected_viInventoriPermintaan.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viInventoriPermintaan = dataSource_viInventoriPermintaan.getAt(ridx);
					if (rowSelected_viInventoriPermintaan != undefined)
					{
						setLookUp_viInventoriPermintaan(rowSelected_viInventoriPermintaan.data);
					}
					else
					{
						setLookUp_viInventoriPermintaan();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Inventori perencanaan
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colNoPermintaan_viInventoriPermintaan',
						header: 'No. Permintaan',
						dataIndex: '',
						sortable: true,
						width: 35,
						filter:
						{
							type: 'int'
						}
					},
					//-------------- ## --------------
					{
						id: 'colTglRO_viInventoriPermintaan',
						header:'Tanggal',
						dataIndex: '',						
						width: 20,
						sortable: true,
						// format: 'd/M/Y',
						filter: {},
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.TGL_RO);
						}
					},
					//-------------- ## --------------
					{
						id: 'colKeterangan_viInventoriPermintaan',
						header: 'Unit',
						dataIndex: '',
						sortable: true,
						width: 60,
						filter:
						{}
					}
					//-------------- ## --------------
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viInventoriPermintaan',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viInventoriPermintaan',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viInventoriPermintaan != undefined)
							{
								setLookUp_viInventoriPermintaan(rowSelected_viInventoriPermintaan.data)
							}
							else
							{								
								setLookUp_viInventoriPermintaan();
							}
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			bbar : bbar_paging(mod_name_viInventoriPermintaan, selectCount_viInventoriPermintaan, dataSource_viInventoriPermintaan),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viInventoriPermintaan = new Ext.Panel
    (
		{
			title: NamaForm_viInventoriPermintaan,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viInventoriPermintaan,
			region: 'center',
			layout: 'form', 
			closable: true,        
			border: false,  
			margins: '0 5 5 0',
			items: [GridDataView_viInventoriPermintaan],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viInventoriPermintaan,
		            columns: 11,
		            defaults: {
		                scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
						//-------------- ## --------------
			            { 
							xtype: 'tbtext', 
							text: 'No. Permintaan : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},						
						{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_NoPermintaan_viInventoriPermintaan',
							emptyText: 'No. Permintaan',
							width: 100,
							height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										
									} 						
								}
							}
						},
						{	 
							xtype: 'tbspacer',
							width: 10,
							height: 25
						},						
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Tanggal : ', 
							style:{'text-align':'right'},
							width: 60,
							height: 25
						},
						{
							xtype: 'datefield',
							id: 'txtfilterTglAwal_viInventoriPermintaan',
							value: now_viInventoriPermintaan,
							format: 'd/M/Y',
							width: 120,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viInventoriPermintaan();								
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: ' s.d ', 
							style:{'text-align':'center'},
							width: 30,
							height: 25
						},																								
						//-------------- ## --------------
						{
							xtype: 'datefield',
							id: 'txtfilterTglKunjAkhir_viInventoriPermintaan',
							value: now_viInventoriPermintaan,
							format: 'd/M/Y',
							width: 120,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viInventoriPermintaan();								
									} 						
								}
							}
						},							
						{	 
							xtype: 'tbspacer',
							width: 10,
							height: 25
						},
						{ 
							xtype: 'tbtext', 
							text: 'Unit : ', 
							style:{'text-align':'right'},
							width: 45,
							height: 25
						},		
						viCombo_Unit(150, 'cboPbf_viInventoriPermintaanFilter'),						
						//-------------- ## --------------
						{
							xtype: 'button',
							text: 'Cari',
							iconCls: 'refresh',
							tooltip: 'Cari',
							style:{paddingLeft:'30px'},
							//rowspan: 3,
							width:150,
							id: 'BtnFilterGridDataView_viInventoriPermintaan',
							handler: function() 
							{					
								DfltFilterBtn_viInventoriPermintaan = 1;
								DataRefresh_viInventoriPermintaan(getCriteriaFilterGridDataView_viInventoriPermintaan());
							}                        
						},
						//-------------- ## --------------
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viInventoriPermintaan;
    //-------------- # End form filter # --------------
}
// End Function dataGrid_viInventoriPermintaan # --------------

/**
*	Function : setLookUp_viInventoriPermintaan
*	
*	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
*/

function setLookUp_viInventoriPermintaan(rowdata)
{
    var lebar = 985;
    setLookUps_viInventoriPermintaan = new Ext.Window
    (
    {
        id: 'SetLookUps_viInventoriPermintaan',
		name: 'SetLookUps_viInventoriPermintaan',
        title: NamaForm_viInventoriPermintaan, 
        closeAction: 'destroy',        
        width: 1000,
        height: 605,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viInventoriPermintaan(lebar,rowdata), //1
        listeners:
        {
            activate: function()
            {
				
            },
            afterShow: function()
            {
                this.activate();
            },
            deactivate: function()
            {
                rowSelected_viInventoriPermintaan=undefined;
                //datarefresh_viInventoriPermintaan();
				mNoKunjungan_viInventoriPermintaan = '';
            }
        }
    }
    );

    setLookUps_viInventoriPermintaan.show();
    if (rowdata == undefined)
    {
        // dataaddnew_viInventoriPermintaan();
		// Ext.getCmp('btnDelete_viInventoriPermintaan').disable();	
    }
    else
    {
        // datainit_viInventoriPermintaan(rowdata);
    }
}
// End Function setLookUpGridDataView_viInventoriPermintaan # --------------

/**
*	Function : getFormItemEntry_viInventoriPermintaan
*	
*	Sebuah fungsi untuk menampilkan penataan menu dan isian form edit
*/

function getFormItemEntry_viInventoriPermintaan(lebar,rowdata)
{
    var pnlFormDataBasic_viInventoriPermintaan = new Ext.FormPanel
    (
		{
			title: '',
			region: 'north',
			layout: 'form',
			bodyStyle: 'padding:10px 10px 10px 10px',
			anchor: '100%',
			labelWidth: 1,
			autoWidth: true,
			width: lebar,
			border: false,
			//-------------- #items# --------------
			items:
			[
				getItemPanelInputBiodata_viInventoriPermintaan(lebar),
				//-------------- ## -------------- 				
				getItemGridTransaksi_viInventoriPermintaan(lebar),
				//-------------- ## --------------
				{
					xtype: 'compositefield',
					fieldLabel: ' ',
					labelSeparator: '',
					name: 'compChkPosted_viInventoriPermintaan',
					id: 'compChkPosted_viInventoriPermintaan',
					items: 
					[
						{
						xtype: 'checkbox',
						id: 'chkPostedInventoriPermintaan',
						name: 'chkPostedInventoriPermintaan',
						disabled: false,
						autoWidth: false,		
						style: { 'margin-top': '2px' },							
						boxLabel: 'Posted ',
						width: 70
					},                    
		            ],
		        },				
			],
			//-------------- #End items# --------------
			fileUpload: true,
			// Tombol pada tollbar Edit Data Pasien
			tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						id: 'btnAdd_viInventoriPermintaan',
						handler: function(){
							dataaddnew_viInventoriPermintaan();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viInventoriPermintaan',
						handler: function()
						{
							datasave_viInventoriPermintaan(addNew_viInventoriPermintaan);
							datarefresh_viInventoriPermintaan();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						id: 'btnSimpanExit_viInventoriPermintaan',
						handler: function()
						{
							var x = datasave_viInventoriPermintaan(addNew_viInventoriPermintaan);
							datarefresh_viInventoriPermintaan();
							if (x===undefined)
							{
								setLookUps_viInventoriPermintaan.close();
							}
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viInventoriPermintaan',
						handler: function()
						{
							datadelete_viInventoriPermintaan();
							datarefresh_viInventoriPermintaan();
							
						}
					},
					//-------------- ## --------------
					{
						xtype:'tbseparator'
					},
					
				]
			}//,items:
		}
    )

    return pnlFormDataBasic_viInventoriPermintaan;
}
// End Function getFormItemEntry_viInventoriPermintaan # --------------

/**
*	Function : getItemPanelInputBiodata_viInventoriPermintaan
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemPanelInputBiodata_viInventoriPermintaan(lebar) 
{
    
    var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		labelWidth: 100,
		autoWidth: true,
		border:true,
		items:
		[			
			{
                xtype: 'compositefield',
                fieldLabel: 'No. Permintaan',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					{
						xtype: 'textfield',
						flex: 1,
						width : 120,	
						name: 'txtNoPermintaan_viInventoriPermintaan',
						id: 'txtNoPermintaan_viInventoriPermintaan',
						emptyText: 'No. Permintaan',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},
					{
						xtype: 'displayfield',
						flex: 1,
						width: 80,
						name: '',
						value: 'Lokasi/Unit :',
						fieldLabel: 'Label'
					},	
					viCombo_Unit(615, 'cboUnit_viInventoriPermintaan')					
					
					
                ]
            },
            //-------------- ## --------------  
			{
				xtype: 'compositefield',
				fieldLabel: 'Tanggal',
				anchor: '100%',
				//labelSeparator: '',
				width: 199,
				items: 
				[
					{
						xtype: 'datefield',
						id: 'txtDateTanggal_viInventoriPermintaan',
						value: now_viInventoriPermintaan,
						format: 'd/M/Y',
						width: 120,
						listeners:
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									datarefresh_viInventoriPermintaan();								
								} 						
							}
						}
					},								                                        									
					{
						xtype: 'displayfield',
						flex: 1,
						width: 80,
						name: '',
						value: 'Keterangan :',
						fieldLabel: 'Label'
					},					
					{
						xtype: 'textfield',
						flex: 1,
						width : 615,	
						name: 'txtKeterangan_viInventoriPermintaan',
						id: 'txtKeterangan_viInventoriPermintaan',
						emptyText: 'Keterangan',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},
				]
			},						
		]
	};
    return items;
};
// End Function getItemPanelInputBiodata_viInventoriPermintaan # --------------


/**
*	Function : getItemGridTransaksi_viInventoriPermintaan
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemGridTransaksi_viInventoriPermintaan(lebar) 
{
    var items =
	{
		//title: 'Detail Transaksi', 
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:1px 1px 1px 1px',
		border:true,
		width: lebar-80,
		height: 430,//225, 
	    items:
		[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:
				[
					gridDataViewEdit_viInventoriPermintaan()
				]	
			}
			//-------------- ## --------------
		]
	};
    return items;
};
// End Function getItemGridTransaksi_viInventoriPermintaan # --------------

/**
*	Function : gridDataViewEdit_viInventoriPermintaan
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function gridDataViewEdit_viInventoriPermintaan()
{
    
    chkSelected_viInventoriPermintaan = new Ext.grid.CheckColumn
	(
		{
			id: 'chkSelected_viInventoriPermintaan',
			header: '',
			align: 'center',						
			dataIndex: 'SELECTED',			
			width: 20
		}
	);

    var FieldGrdKasir_viInventoriPermintaan = 
	[
	];
	
    dsDataGrdJab_viInventoriPermintaan= new WebApp.DataStore
	({
        fields: FieldGrdKasir_viInventoriPermintaan
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJab_viInventoriPermintaan,
        height: 425,//220,
		selModel: new Ext.grid.RowSelectionModel
        (
	        {
	            singleSelect: true,
	            listeners:
	            {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
	        }
        ),
        columns: 
		[
			chkSelected_viInventoriPermintaan,			
			{			
				dataIndex: '',
				header: 'Kd Kelompok',
				sortable: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Kode',
				sortable: true,
				width: 80,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Uraian',
				sortable: true,
				width: 600,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Satuan',
				sortable: true,
				width: 80,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Jumlah',
				sortable: true,
				width: 50,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Ordered',
				sortable: true,
				width: 50,
				renderer: function(v, params, record) 
				{
					
				}	
			},			
        ],
        plugins:chkSelected_viInventoriPermintaan,
    }    
    return items;
}
// End Function gridDataViewEdit_viInventoriPermintaan # --------------


function viCombo_Unit(lebar,Nama_ID)
{
    var Field_Unit = ['KD_Unit', 'KD_CUSTOMER', 'Unit'];
    ds_Unit = new WebApp.DataStore({fields: Field_Unit});
    
	// viRefresh_Unit();
	
    var cbo_Unit = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'Unit',
			valueField: 'KD_Unit',
            displayField: 'Unit',
			emptyText:'Unit',
			store: ds_Unit,
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',                        
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
			listeners:
			{ 
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_Unit;
}