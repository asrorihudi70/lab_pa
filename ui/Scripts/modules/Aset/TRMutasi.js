// Data Source ExtJS # --------------
// Deklarasi Variabel pada Mutasi # --------------

var dataSource_viMutasi;
var selectCount_viMutasi=50;
var NamaForm_viMutasi="Mutasi";
var mod_name_viMutasi="viMutasi";
var now_viMutasi= new Date();
var addNew_viMutasi;
var rowSelected_viMutasi;
var setLookUps_viMutasi;
var mNoKunjungan_viMutasi='';

var CurrentData_viMutasi =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viMutasi(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

// End Deklarasi Variabel pada Kasir Rawat Jalan # --------------

// Start Project Kasir Rawat Jalan # --------------

// --------------------------------------- # Start Function # ---------------------------------------
// ## Silahkan tempatkan semua fungsi eksekusi disini

// --------------------------------------- # End Function # ---------------------------------------

// --------------------------------------- # Start Form # ---------------------------------------

/**
*	Function : dataGrid_viMutasi
*	
*	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
*/

function dataGrid_viMutasi(mod_id_viMutasi)
{	
    // Field kiriman dari Project Net.
    var FieldMaster_viMutasi = 
	[
		 'NO_KUNJUNGAN', 'KD_KELOMPOK', 'KD_UNIT', 'KD_DOKTER', 'KD_CUSTOMER', 'KD_PASIEN', 
		 'TGL_KUNJUNGAN','JAM_KUNJUNGAN', 'TINGGI_BADAN', 'BERAT_BADAN', 'TEKANAN_DRH', 
		 'NADI','ALERGI', 'KELUHAN', 'RUJUK_RAD', 'RUJUK_LAB', 'TAHAP_PROSES', 'PASIEN_BARU',
		 'NAMA_UNIT','KELOMPOK', 'DOKTER', 'CUSTOMER', 'PS_BARU','KD_PENDIDIKAN','KD_STS_MARITAL', 
		 'KD_AGAMA','KD_PEKERJAAN','NAMA','TEMPAT_LAHIR','TGL_LAHIR','JENIS_KELAMIN','ALAMAT', 
		 'NO_TELP','NO_HP','GOL_DARAH','PENDIDIKAN','STS_MARITAL','AGAMA','PEKERJAAN','JNS_KELAMIN',
		 'TAHUN','BULAN','HARI'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viMutasi = new WebApp.DataStore
	({
        fields: FieldMaster_viMutasi
    });
    
    // Grid Kasir Rawat Jalan # --------------
	var GridDataView_viMutasi = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viMutasi,
			autoScroll: true,
			columnLines: true,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viMutasi = undefined;
							rowSelected_viMutasi = dataSource_viMutasi.getAt(row);
							CurrentData_viMutasi
							CurrentData_viMutasi.row = row;
							CurrentData_viMutasi.data = rowSelected_viMutasi.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viMutasi = dataSource_viMutasi.getAt(ridx);
					if (rowSelected_viMutasi != undefined)
					{
						setLookUp_viMutasi(rowSelected_viMutasi.data);
					}
					else
					{
						setLookUp_viMutasi();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Kasir Rawat Jalan
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colNoMutasi_viMutasi',
						header: 'No. Mutasu',
						dataIndex: '',
						sortable: true,
						width: 25,
						filter:
						{
							type: 'int'
						}
					},
					//-------------- ## --------------
					{
						id: 'colKode_viMutasi',
						header: 'Kode Barang',
						dataIndex: '',
						sortable: true,
						width: 45,
						filter:
						{}
					},
					//-------------- ## --------------
					{
						id: 'colNama_viMutasi',
						header:'Nama',
						dataIndex: '',
						width: 60,
						sortable: true,
						filter: {}
					},
					//-------------- ## --------------
					{
						id: 'colTglMutasi_viMutasi',
						header:'Tgl Mutasi',
						dataIndex: '',						
						width: 50,
						sortable: true,
						// format: 'd/M/Y',
						filter: {},
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.TGL_OPERASI);
						}
					}

				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viMutasi',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viMutasi',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viMutasi != undefined)
							{
								setLookUp_viMutasi(rowSelected_viMutasi.data)
							}
							else
							{								
								setLookUp_viMutasi();
							}
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			bbar : bbar_paging(mod_name_viMutasi, selectCount_viMutasi, dataSource_viMutasi),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viMutasi = new Ext.Panel
    (
		{
			title: NamaForm_viMutasi,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viMutasi,
			region: 'center',
			layout: 'form', 
			closable: true,        
			border: false,  
			margins: '0 5 5 0',
			items: [GridDataView_viMutasi],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viMutasi,
		            columns: 8,
		            defaults: {
		                scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
			            
						{ 
							xtype: 'tbtext', 
							text: 'No. Mutasi : ', 
							style:{'text-align':'right'},
							width: 75,
							height: 25
						},						
						{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_NoMutasi_viMutasi',							
							emptyText: 'No. Mutasi',
							width: 100,
							height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										DfltFilterBtn_viMutasi = 1;
										DataRefresh_viMutasi(getCriteriaFilterGridDataView_viMutasi());
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Kode : ', 
							style:{'text-align':'right'},
							width: 45,
							height: 25
						},
						{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_Kode_viMutasi',
							emptyText: 'Kode',
							width: 150,
							height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										DfltFilterBtn_viMutasi = 1;
										DataRefresh_viMutasi(getCriteriaFilterGridDataView_viMutasi());								
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Nama : ', 
							style:{'text-align':'right'},
							width: 35,
							height: 25
						},
						{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_Nama_viMutasi',
							emptyText: 'Nama Barang',
							width: 300,
							height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										DfltFilterBtn_viMutasi = 1;
										DataRefresh_viMutasi(getCriteriaFilterGridDataView_viMutasi());								
									} 						
								}
							}
						},
						{ 
							xtype: 'tbtext', 
							text: 'Tgl Mutasi : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},
						{
							xtype: 'datefield',
							id: 'txtfilterTglMutasiAwal_viMutasi',
							value: now_viMutasi,
							format: 'd/M/Y',
							width: 100,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viMutasi();								
									} 						
								}
							}
						},						
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 's/d Tgl : ', 
							style:{'text-align':'center'},
							width: 50,
							height: 25
						},
						{
							xtype: 'datefield',
							id: 'txtfilterTglMutasiAkhir_viMutasi',
							value: now_viMutasi,
							format: 'd/M/Y',
							width: 100,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viMutasi();								
									} 						
								}
							}
						},						
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},														
						{
							xtype: 'button',
							text: 'Cari',
							iconCls: 'refresh',
							tooltip: 'Cari',
							style:{paddingLeft:'30px'},
							//rowspan: 3,
							width:50,
							id: 'BtnFilterGridDataView_viMutasi',
							handler: function() 
							{					
								DfltFilterBtn_viMutasi = 1;
								DataRefresh_viMutasi(getCriteriaFilterGridDataView_viMutasi());
							}                        
						},
						//-------------- ## --------------
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viMutasi;
    //-------------- # End form filter # --------------
}
// End Function dataGrid_viMutasi # --------------

/**
*	Function : setLookUp_viMutasi
*	
*	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
*/

function setLookUp_viMutasi(rowdata)
{
    var lebar = 860;
    setLookUps_viMutasi = new Ext.Window
    (
    {
        id: 'SetLookUps_viMutasi',
		name: 'SetLookUps_viMutasi',
        title: NamaForm_viMutasi, 
        closeAction: 'destroy',        
        width: 875,
        height: 650,//580,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viMutasi(lebar,rowdata), //1
        listeners:
        {
            activate: function()
            {
				
            },
            afterShow: function()
            {
                this.activate();
            },
            deactivate: function()
            {
                rowSelected_viMutasi=undefined;
                //datarefresh_viMutasi();
				mNoKunjungan_viMutasi = '';
            }
        }
    }
    );

    setLookUps_viMutasi.show();
    if (rowdata == undefined)
    {
        // dataaddnew_viMutasi();
		// Ext.getCmp('btnDelete_viMutasi').disable();	
    }
    else
    {
        // datainit_viMutasi(rowdata);
    }
}
// End Function setLookUpGridDataView_viMutasi # --------------

/**
*	Function : getFormItemEntry_viMutasi
*	
*	Sebuah fungsi untuk menampilkan penataan menu dan isian form edit
*/

function getFormItemEntry_viMutasi(lebar,rowdata)
{
    var pnlFormDataBasic_viMutasi = new Ext.FormPanel
    (
		{
			title: '',
			region: 'north',
			layout: 'form',
			bodyStyle: 'padding:10px 10px 10px 10px',
			anchor: '100%',
			width: lebar,
			border: false,
			//-------------- #items# --------------			
			items:[getItemDataBarang_viMutasi(lebar)], 			
			//-------------- #End items# --------------
			fileUpload: true,
			// Tombol pada tollbar Edit Data Pasien
			tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						id: 'btnAdd_viMutasi',
						handler: function(){
							dataaddnew_viMutasi();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viMutasi',
						handler: function()
						{
							datasave_viMutasi(addNew_viMutasi);
							datarefresh_viMutasi();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						id: 'btnSimpanExit_viMutasi',
						handler: function()
						{
							var x = datasave_viMutasi(addNew_viMutasi);
							datarefresh_viMutasi();
							if (x===undefined)
							{
								setLookUps_viMutasi.close();
							}
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viMutasi',
						handler: function()
						{
							datadelete_viMutasi();
							datarefresh_viMutasi();
							
						}
					},
					
					
				]
			}//,items:
		}
    )

    return pnlFormDataBasic_viMutasi;
}
// End Function getFormItemEntry_viMutasi # --------------


/**
*	Function : getItemDataBarang_viMutasi
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemDataBarang_viMutasi(lebar) 
{
    var items =
	{
		layout: 'form',
		anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',		
		width: lebar-80,
		height: 570,
	    items:
		[				
			{
				xtype: 'panel',
				title: '',
				border:false,	
				height: 560,
				items: 
				[
					{
						xtype: 'tabpanel',
						activeTab: 0,		
						height: 550, //260,
						items: 
						[
							{
								xtype: 'panel',
								title: 'Penambahan',				
								padding: '4px',
								items: 
								[										
									{
										xtype: 'compositefield',
										fieldLabel: 'Nomor',
										name: 'compNomor_viMutasi',
										id: 'compNomor_viMutasi',
										items: 
										[
											{
												xtype: 'displayfield',				
												width: 90,								
												value: 'Nomor :',
												fieldLabel: 'Label',
												id: 'lblNomorPenambahan_viMutasi',
												name: 'lblNomorPenambahan_viMutasi'
											},
											{
												xtype: 'textfield',
												id: 'txtNomorPenambahan_viMutasi',
												name: 'txtNomorPenambahan_viMutasi',												
												width: 100,												
												emptyText:'Nomor',
												readOnly: true,
											},
											{
												xtype: 'displayfield',				
												width: 90,								
												value: 'Tanggal :',
												fieldLabel: 'Label',
												id: 'lblPenambahan_viMutasi',
												name: 'lblPenambahan_viMutasi'
											},
											{
												xtype: 'datefield',
												fieldLabel: 'TanggalMasuk',
												id: 'datePenambahan_viMutasi',
												name: 'datePenambahan_viMutasi',
												width: 100,
												value: now_viMutasi,
												format: 'd/M/Y',
											},											
										],										
									},
									{ 
										xtype: 'tbspacer',									
										height: 5
									},
									{
										xtype: 'compositefield',
										fieldLabel: 'Keterangan ',
										name: 'compKetPenambahan_viMutasi',
										id: 'compKetPenambahan_viMutasi',
										items: 
										[
											{
												xtype: 'displayfield',				
												width: 90,								
												value: 'Keterangan :',
												fieldLabel: 'Label',
												id: 'lblKetPenambahan_viMutasi',
												name: 'lblKetPenambahan_viMutasi'
											},
											{
												xtype: 'textarea',						
												fieldLabel: 'Keterangan',
												width: 713,
												height: 40,
												name: 'txtKetPenambahan_viMutasi',
												id: 'txtKetPenambahan_viMutasi'
											}					
										]
									},
									{ 
										xtype: 'tbspacer',									
										height: 5
									},
									gridDataPenambahan_viMutasi(),											
									
								]
							},
							{
								xtype: 'panel',
								title: 'Pemindahan',				
								padding: '4px',
								items: 
								[
									{
										xtype: 'compositefield',
										fieldLabel: 'Nomor',
										name: 'compNomorPemindahan_viMutasi',
										id: 'compNomorPemidahan_viMutasi',
										items: 
										[
											{
												xtype: 'displayfield',				
												width: 90,								
												value: 'Nomor :',
												fieldLabel: 'Label',
												id: 'lblNomorPemindahan_viMutasi',
												name: 'lblNomorPemindahan_viMutasi'
											},
											{
												xtype: 'textfield',
												id: 'txtNomorPemindahan_viMutasi',
												name: 'txtNomorPemindahan_viMutasi',												
												width: 100,												
												emptyText:'Nomor',
												readOnly: true,
											},
											{
												xtype: 'displayfield',				
												width: 90,								
												value: 'Tanggal :',
												fieldLabel: 'Label',
												id: 'lblPemindahan_viMutasi',
												name: 'lblPemindahan_viMutasi'
											},
											{
												xtype: 'datefield',
												fieldLabel: 'TanggalMasuk',
												id: 'datePemindahan_viMutasi',
												name: 'datePemindahan_viMutasi',
												width: 100,
												value: now_viMutasi,
												format: 'd/M/Y',
											},											
										],										
									},
									{ 
										xtype: 'tbspacer',									
										height: 5
									},
									{
										xtype: 'compositefield',
										fieldLabel: 'Keterangan ',
										name: 'compKetPemindahan_viMutasi',
										id: 'compKetPemindahan_viMutasi',
										items: 
										[
											{
												xtype: 'displayfield',				
												width: 90,								
												value: 'Keterangan :',
												fieldLabel: 'Label',
												id: 'lblKetPemindahan_viMutasi',
												name: 'lblKetPemindahan_viMutasi'
											},
											{
												xtype: 'textarea',						
												fieldLabel: 'Keterangan',
												width: 713,
												height: 40,
												name: 'txtKetPemindahan_viMutasi',
												id: 'txtKetPemindahan_viMutasi'
											}					
										]
									},			
									{ 
										xtype: 'tbspacer',									
										height: 5
									},	
									gridDataPemindahan_viMutasi(),
								]
							},
							{
								xtype: 'panel',
								title: 'Pengurangan',				
								padding: '4px',
								items: 
								[	
										{
											xtype: 'compositefield',
											fieldLabel: 'Nomor',
											name: 'compNomorPengurangan_viMutasi',
											id: 'compNomorPemidahan_viMutasi',
											items: 
											[
												{
													xtype: 'displayfield',				
													width: 90,								
													value: 'Nomor :',
													fieldLabel: 'Label',
													id: 'lblNomorPengurangan_viMutasi',
													name: 'lblNomorPengurangan_viMutasi'
												},
												{
													xtype: 'textfield',
													id: 'txtNomorPengurangan_viMutasi',
													name: 'txtNomorPengurangan_viMutasi',												
													width: 100,												
													emptyText:'Nomor',
													readOnly: true,
												},
												{
													xtype: 'displayfield',				
													width: 90,								
													value: 'Tanggal :',
													fieldLabel: 'Label',
													id: 'lblPengurangan_viMutasi',
													name: 'lblPengurangan_viMutasi'
												},
												{
													xtype: 'datefield',
													fieldLabel: 'TanggalMasuk',
													id: 'datePengurangan_viMutasi',
													name: 'datePengurangan_viMutasi',
													width: 100,
													value: now_viMutasi,
													format: 'd/M/Y',
												},											
											],										
										},
										{ 
											xtype: 'tbspacer',									
											height: 5
										},
										{
											xtype: 'compositefield',
											fieldLabel: 'Keterangan ',
											name: 'compKetPengurangan_viMutasi',
											id: 'compKetPengurangan_viMutasi',
											items: 
											[
												{
													xtype: 'displayfield',				
													width: 90,								
													value: 'Keterangan :',
													fieldLabel: 'Label',
													id: 'lblKetPengurangan_viMutasi',
													name: 'lblKetPengurangan_viMutasi'
												},
												{
													xtype: 'textarea',						
													fieldLabel: 'Keterangan',
													width: 713,
													height: 40,
													name: 'txtKetPengurangan_viMutasi',
													id: 'txtKetPengurangan_viMutasi'
												}					
											]
										},
										{ 
											xtype: 'tbspacer',									
											height: 5
										},
										gridDataPengurangan_viMutasi(),
									
								
								]
							}
						]
					},									
				]
			}
		]
	};
    return items;
};
// End Function getItemDataBarang_viMutasi # --------------

function gridDataPemindahan_viMutasi()
{
    
    var FieldGrdPemindahan_viMutasi = 
	[
	];
	
    dsDataPemindahan_viMutasi= new WebApp.DataStore
	({
        fields: FieldGrdPemindahan_viMutasi
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataPemindahan_viMutasi,
        height: 440,
		selModel: new Ext.grid.RowSelectionModel
        (
	        {
	            singleSelect: true,
	            listeners:
	            {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
	        }
        ),
        columns: 
		[
			{
				dataIndex: '',
				header: 'Kd Kelompok',
				sortable: true,
				width: 80
			},			
			{
				dataIndex: '',
				header: 'Urutan',
				sortable: true,
				width: 50
			},			
			{
				dataIndex: '',
				header: 'Barang',
				sortable: true,
				width: 300
			},			
			{
				dataIndex: '',
				header: 'Satuan',
				sortable: true,
				width: 90
			},			
			{
				dataIndex: '',
				header: 'Jumlah',
				sortable: true,
				width: 90
			},			
			{
				dataIndex: '',
				header: 'Kelompok',
				sortable: true,
				width: 90
			},			
        ]
    }    
    return items;
}

function gridDataPenambahan_viMutasi()
{
    
    var FieldGrdPenambahan_viMutasi = 
	[
	];
	
    dsDataPenambahan_viMutasi= new WebApp.DataStore
	({
        fields: FieldGrdPenambahan_viMutasi
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataPenambahan_viMutasi,
        height: 440,
		selModel: new Ext.grid.RowSelectionModel
        (
	        {
	            singleSelect: true,
	            listeners:
	            {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
	        }
        ),
        columns: 
		[
			{
				dataIndex: '',
				header: 'Kd Kelompok',
				sortable: true,
				width: 80
			},			
			{
				dataIndex: '',
				header: 'Urutan',
				sortable: true,
				width: 50
			},			
			{
				dataIndex: '',
				header: 'Barang',
				sortable: true,
				width: 300
			},			
			{
				dataIndex: '',
				header: 'Satuan',
				sortable: true,
				width: 90
			},			
			{
				dataIndex: '',
				header: 'Jumlah',
				sortable: true,
				width: 90
			},			
			{
				dataIndex: '',
				header: 'Kelompok',
				sortable: true,
				width: 90
			},			
        ]
    }    
    return items;
}

function FormSetLookupKonsultasi_viMutasi(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryKonsultasi_viMutasi = new Ext.Window
    (
        {
            id: 'FormGrdLookupKonsultasi_viMutasi',
            title: 'Konsultasi',
            closeAction: 'destroy',
            closable:true,
            width: 390,
            height: 125,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
            //padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryLookupKonsultasi_viMutasi(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryKonsultasi_viMutasi.show();
    mWindowGridLookupKonsultasi  = vWinFormEntryKonsultasi_viMutasi; 
};

// End Function FormSetLookupKonsultasi_viMutasi # --------------

/**
*   Function : getFormItemEntryLookupKonsultasi_viMutasi
*   
*   Sebuah fungsi untuk menampilkan isian form konsultasi
*/
function getFormItemEntryLookupKonsultasi_viMutasi(subtotal,NO_KUNJUNGAN)
{
    var lebar = 500;
    var pnlFormDataKonsultasiWindowPopup_viMutasi = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                getItemPanelInputKonsultasiDataView_viMutasi(lebar),
                //-------------- ## --------------
            ],
            //-------------- #End items# --------------
        }
    )
    return pnlFormDataKonsultasiWindowPopup_viMutasi;
}
// End Function getFormItemEntryLookupKonsultasi_viMutasi # --------------

/**
*   Function : getItemPanelInputKonsultasiDataView_viMutasi
*   
*   Sebuah fungsi untuk menampilkan isian form konsultasi
*/

function viComboJenisTransaksi_viMutasi(lebar,Nama_ID)
{
  var cbo_viComboJenisTransaksi_viMutasi = new Ext.form.ComboBox
    (
    
        {
            id:Nama_ID,
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'Pilih Transaksi..',
            fieldLabel: "Transaksi",
            value:1,        
            width:lebar,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields: 
                    [
                        'IdTrnsksi',
                        'displayTextTrnsksi'
                    ],
                    data: [['all', "Semua"],[1, "Bayar"],[2, "Belum Bayar"]]
                }
            ),
            valueField: 'IdTrnsksi',
            displayField: 'displayTextTrnsksi',
            listeners:  
            {
            }
        }
    );
    return cbo_viComboJenisTransaksi_viMutasi;
};
// End Function viComboJenisTransaksi_viMutasi # --------------

function viComboJenisOperasi_viMutasi(lebar,Nama_ID)
{
  var cbo_viComboJenisOperasi_viMutasi = new Ext.form.ComboBox
    (
    
        {
            id:Nama_ID,
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'Jenis Operasi..',
            fieldLabel: "Jenis",
            value:1,        
            width:lebar,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields: 
                    [
                        'IdKdJnsOpersi',
                        'IdJnsTrans'
                    ],
                    data: [[1, "Kecil"],[2, "Sedang"],[3, "Besar"]]
                }
            ),
            valueField: 'IdKdJnsOpersi',
            displayField: 'IdJnsTrans',
            listeners:  
            {
            }
        }
    );
    return cbo_viComboJenisOperasi_viMutasi;
};

function getItemPanelInputKonsultasiDataView_viMutasi(lebar) 
{
    var items =
    {
        title: '',
        layout: 'Form',
        anchor: '100%',
        width: lebar,
        labelAlign: 'Left',
        bodyStyle: 'padding:1px 1px 1px 1px',
        border:false,
        items:
        [           
            {
                xtype: 'compositefield',
                fieldLabel: 'Asal Unit',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_viMutasi(250,'CmboAsalUnit_viMutasi'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
            {
                xtype: 'compositefield',
                fieldLabel: 'Konsultasi ke',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_viMutasi(250,'CmboKonsultasike_viMutasi'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
            {
                xtype: 'compositefield',
                fieldLabel: 'Dokter',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_viMutasi(250,'CmboDokter_viMutasi'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
        ]
    };
    return items;
}
// End Function getItemPanelInputKonsultasiDataView_viMutasi # --------------

// ## END MENU LOOKUP - LOOKUP KONSULTASI ## ------------------------------------------------------------------

// ## MENU LOOKUP - LOOKUP GANTI DOKTER ## ------------------------------------------------------------------

/**
*   Function : FormSetLookupGantiDokter_viMutasi
*   
*   Sebuah fungsi untuk menampilkan windows popup Ganti Dokter
*/

function FormSetLookupGantiDokter_viMutasi(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryGantiDokter_viMutasi = new Ext.Window
    (
        {
            id: 'FormGrdLookupGantiDokter_viMutasi',
            title: 'Ganti Dokter',
            closeAction: 'destroy',
            closable:true,
            width: 390,
            height: 125,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
            //padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryLookupGantiDokter_viMutasi(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryGantiDokter_viMutasi.show();
    mWindowGridLookupGantiDokter  = vWinFormEntryGantiDokter_viMutasi; 
};

// End Function FormSetLookupGantiDokter_viMutasi # --------------

/**
*   Function : getFormItemEntryLookupGantiDokter_viMutasi
*   
*   Sebuah fungsi untuk menampilkan isian form Ganti Dokter
*/
function getFormItemEntryLookupGantiDokter_viMutasi(subtotal,NO_KUNJUNGAN)
{
    var lebar = 500;
    var pnlFormDataGantiDokterWindowPopup_viMutasi = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                getItemPanelInputGantiDokterDataView_viMutasi(lebar),
                //-------------- ## --------------
            ],
            //-------------- #End items# --------------
        }
    )
    return pnlFormDataGantiDokterWindowPopup_viMutasi;
}
// End Function getFormItemEntryLookupGantiDokter_viMutasi # --------------

/**
*   Function : getItemPanelInputGantiDokterDataView_viMutasi
*   
*   Sebuah fungsi untuk menampilkan isian form Ganti Dokter
*/

function getItemPanelInputGantiDokterDataView_viMutasi(lebar) 
{
    var items =
    {
        title: '',
        layout: 'Form',
        anchor: '100%',
        width: lebar,
        labelAlign: 'Left',
        bodyStyle: 'padding:1px 1px 1px 1px',
        border:false,
        items:
        [           
            {
                xtype: 'compositefield',
                fieldLabel: 'Tanggal',
                anchor: '100%',
                width: 250,
                items: 
                [
                    {
                        xtype: 'textfield',
                        id: 'TxtTglGantiDokter_viMutasi',
                        emptyText: '01',
                        width: 30,
                    },
                    //-------------- ## --------------  
                    {
                        xtype: 'textfield',
                        id: 'TxtBlnGantiDokter_viMutasi',
                        emptyText: '01',
                        width: 30,
                    },
                    //-------------- ## --------------  
                    {
                        xtype: 'textfield',
                        id: 'TxtThnGantiDokter_viMutasi',
                        emptyText: '2014',
                        width: 50,
                    },
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
            {
                xtype: 'compositefield',
                fieldLabel: 'Dokter Asal',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_viMutasi(250,'CmboDokterAsal_viMutasi'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
            {
                xtype: 'compositefield',
                fieldLabel: 'Dokter Ganti',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_viMutasi(250,'CmboDokterGanti_viMutasi'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
        ]
    };
    return items;
}
// End Function getItemPanelInputGantiDokterDataView_viMutasi # --------------

// ## END MENU LOOKUP - LOOKUP GANTI DOKTER ## ------------------------------------------------------------------

// ## END MENU LOOKUP ## ------------------------------------------------------------------

// ## MENU PEMBAYARAN ## ------------------------------------------------------------------

// ## MENU PEMBAYARAN - PEMBAYARAN ## ------------------------------------------------------------------

/**
*   Function : FormSetPembayaran_viMutasi
*   
*   Sebuah fungsi untuk menampilkan windows popup Pembayaran
*/

function FormSetPembayaran_viMutasi(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryPembayaran_viMutasi = new Ext.Window
    (
        {
            id: 'FormGrdPembayaran_viMutasi',
            title: 'Pembayaran',
            closeAction: 'destroy',
            closable:true,
            width: 760,
            height: 473,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
			//padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryGridDataView_viMutasi(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryPembayaran_viMutasi.show();
    mWindowGridLookupPembayaran  = vWinFormEntryPembayaran_viMutasi; 
};

// End Function FormSetPembayaran_viMutasi # --------------

/**
*   Function : getFormItemEntryGridDataView_viMutasi
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/
function getFormItemEntryGridDataView_viMutasi(subtotal,NO_KUNJUNGAN)
{
    var lebar = 760;
    var pnlFormDataWindowPopup_viMutasi = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
			items:
			[
				getItemPanelInputBiodataGridDataView_viMutasi(lebar),
				//-------------- ## -------------- 
				getItemGridTransaksiGridDataView_viMutasi(lebar),
				//-------------- ## --------------
				{
					xtype: 'compositefield',
					fieldLabel: ' ',
					anchor: '100%',
					//labelSeparator: '',
					width: 199,
					style:{'margin-top':'7px'},
					items: 
					[
						{
		                    xtype:'button',
		                    text:'Ok',
		                    width:70,
		                    //style:{'margin-left':'190px','margin-top':'7px'},
		                    hideLabel:true,
		                    id: 'btnOkGridTransaksiPembayaranGridDataView_viMutasi',
		                    handler:function()
		                    {
		                    }   
		                },
		                //-------------- ## --------------
		                {
		                    xtype:'button',
		                    text:'Cancel',
		                    width:70,
		                    //style:{'margin-left':'190px','margin-top':'7px'},
		                    hideLabel:true,
		                    id: 'btnCancelGridTransaksiPembayaranGridDataView_viMutasi',
		                    handler:function()
		                    {
		                    }   
		                },
		                //-------------- ## --------------
		                {
                            xtype: 'textfield',
                            id: 'txtBiayaGridTransaksiPembayaranGridDataView_viMutasi',
                            name: 'txtBiayaGridTransaksiPembayaranGridDataView_viMutasi',
							style:{'text-align':'right','margin-left':'150px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
		                //-------------- ## --------------
		                {
                            xtype: 'textfield',
                            id: 'txtPiutangGridTransaksiPembayaranGridDataView_viMutasi',
                            name: 'txtPiutangGridTransaksiPembayaranGridDataView_viMutasi',
							style:{'text-align':'right','margin-left':'146px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
		                //-------------- ## --------------
		                {
                            xtype: 'textfield',
                            id: 'txtTunaiGridTransaksiPembayaranGridDataView_viMutasi',
                            name: 'txtTunaiGridTransaksiPembayaranGridDataView_viMutasi',
							style:{'text-align':'right','margin-left':'142px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
		                //-------------- ## --------------
		                {
                            xtype: 'textfield',
                            id: 'txtDiscGridTransaksiPembayaranGridDataView_viMutasi',
                            name: 'txtDiscGridTransaksiPembayaranGridDataView_viMutasi',
							style:{'text-align':'right','margin-left':'138px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
		                //-------------- ## --------------
					]
				},	
			],
			//-------------- #End items# --------------
        }
    )
    return pnlFormDataWindowPopup_viMutasi;
}
// End Function getFormItemEntryGridDataView_viMutasi # --------------

/**
*   Function : getItemPanelInputBiodataGridDataView_viMutasi
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/

function getItemPanelInputBiodataGridDataView_viMutasi(lebar) 
{
    var items =
    {
        title: 'Infromasi Pasien',
        layout: 'Form',
        anchor: '100%',
        width: lebar- 80,
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        items:
        [           
            {
				xtype: 'textfield',
				fieldLabel: 'No. Transaksi',
				id: 'txtNoTransaksiPembayaranGridDataView_viMutasi',
				name: 'txtNoTransaksiPembayaranGridDataView_viMutasi',
				emptyText: 'No. Transaksi',
				readOnly: true,
				flex: 1,
				width: 195,				
			},
			//-------------- ## --------------
			{
				xtype: 'textfield',
				fieldLabel: 'Nama Pasien',
				id: 'txtNamaPasienPembayaranGridDataView_viMutasi',
				name: 'txtNamaPasienPembayaranGridDataView_viMutasi',
				emptyText: 'Nama Pasien',
				flex: 1,
				anchor: '100%',					
			},
			//-------------- ## --------------
			{
				xtype: 'compositefield',
				fieldLabel: 'Pembayaran',
				anchor: '100%',
				width: 250,
				items: 
				[
					viComboJenisTransaksi_viMutasi(195,'CmboPembayaranGridDataView_viMutasi'),
					//-------------- ## --------------	
				]
			},
			//-------------- ## --------------
			{
				xtype: 'compositefield',
				fieldLabel: ' ',
				anchor: '100%',
				labelSeparator: '',
				width: 250,
				items: 
				[
					viComboJenisTransaksi_viMutasi(195,'CmboPembayaranDuaGridDataView_viMutasi'),
					//-------------- ## --------------	
				]
			},
			//-------------- ## --------------
        ]
    };
    return items;
}
// End Function getItemPanelInputBiodataGridDataView_viMutasi # --------------

/**
*   Function : getItemGridTransaksiGridDataView_viMutasi
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/

function getItemGridTransaksiGridDataView_viMutasi(lebar) 
{
    var items =
    {
        //title: 'Detail Transaksi', 
        layout: 'form',
        anchor: '100%',     
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        width: lebar-80,
        height:245, 
        items:
        [
            {
                labelWidth:105,
                layout: 'form',
                labelAlign: 'Left',
                border: false,
                items:
                [
                    gridDataViewEditGridDataView_viMutasi()
                ]   
            }
            //-------------- ## --------------
        ]
    };
    return items;
};
// End Function getItemGridTransaksiGridDataView_viMutasi # --------------

/**
*   Function : gridDataViewEditGridDataView_viMutasi
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/

function gridDataViewEditGridDataView_viMutasi()
{
    
    var FieldGrdKasirGridDataView_viMutasi = 
    [
    ];
    
    dsDataGrdJabGridDataView_viMutasi= new WebApp.DataStore
    ({
        fields: FieldGrdKasirGridDataView_viMutasi
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJabGridDataView_viMutasi,
        height: 220,
        selModel: new Ext.grid.RowSelectionModel
        (
            {
                singleSelect: true,
                listeners:
                {
                    rowselect: function(sm, row, rec)
                    {
                    }
                }
            }
        ),
        columns: 
        [
            {
                dataIndex: '',
                header: 'Kode',
                sortable: true,
                width: 50
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Deskripsi',
                sortable: true,
                width: 200
            },
            //-------------- ## --------------          
            {           
                dataIndex: '',
                header: 'Qty',
                sortable: true,
                width: 40
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Biaya',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }           
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Piutang',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }   
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Tunai',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }   
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Disc',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }   
            },
            //-------------- ## --------------
        ]
    }    
    return items;
}

function gridDataPengurangan_viMutasi()
{
    
    var FieldGrdPengurangan_viMutasi = 
	[
	];
	
    dsDataPengurangan_viMutasi= new WebApp.DataStore
	({
        fields: FieldGrdPengurangan_viMutasi
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataPengurangan_viMutasi,
        height: 440,
		selModel: new Ext.grid.RowSelectionModel
        (
	        {
	            singleSelect: true,
	            listeners:
	            {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
	        }
        ),
        columns: 
		[
			{
				dataIndex: '',
				header: 'Kd Kelompok',
				sortable: true,
				width: 80
			},			
			{
				dataIndex: '',
				header: 'Urutan',
				sortable: true,
				width: 50
			},			
			{
				dataIndex: '',
				header: 'Barang',
				sortable: true,
				width: 300
			},			
			{
				dataIndex: '',
				header: 'Satuan',
				sortable: true,
				width: 90
			},			
			{
				dataIndex: '',
				header: 'Jumlah',
				sortable: true,
				width: 90
			},			
			{
				dataIndex: '',
				header: 'Kelompok',
				sortable: true,
				width: 90
			},			
        ]
    }    
    return items;
}