var dataSource_viInvDistribusiBHP;
var selectCount_viInvDistribusiBHP=50;
var NamaForm_viInvDistribusiBHP="Distribusi Bahan Habis Pakai";
var mod_name_viInvDistribusiBHP="viInvDistribusiBHP";
var now_viInvDistribusiBHP= new Date();
var rowSelected_viInvDistribusiBHP;
var rowSelected_viInvDistribusiBHP_Permintaan;
var setLookUps_viInvDistribusiBHP;
var tanggal = now_viInvDistribusiBHP.format("d/M/Y");
var jam = now_viInvDistribusiBHP.format("H/i/s");
var tmpkriteria;
var GridDataView_viInvDistribusiBHP;
var gridPanelLookUpBarang_InvDistribusiBHP;
var gridPanelLookUpListBarang_InvDistribusiBHP;
var cNoKeluar_viInvDistribusiBHP;
var cTglAwal_viInvDistribusiBHP;
var cTglAkhir_viInvDistribusiBHP;
var tmp_kd_lokasi_permintaan;
var tmp_no_permintaan;
var CurrentData_viInvDistribusiBHP =
{
	data: Object,
	details: Array,
	row: 0
};

var CurrentData_viInvDistribusiBHP_Permintaan =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viInvDistribusiBHP(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

var InvDistribusiBHP={};
InvDistribusiBHP.form={};
InvDistribusiBHP.func={};
InvDistribusiBHP.vars={};
InvDistribusiBHP.func.parent=InvDistribusiBHP;
InvDistribusiBHP.form.ArrayStore={};
InvDistribusiBHP.form.ComboBox={};
InvDistribusiBHP.form.DataStore={};
InvDistribusiBHP.form.Record={};
InvDistribusiBHP.form.Form={};
InvDistribusiBHP.form.Grid={};
InvDistribusiBHP.form.Panel={};
InvDistribusiBHP.form.TextField={};
InvDistribusiBHP.form.Button={};

InvDistribusiBHP.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_bahan', 'nama_bahan', 'kd_satuan', 'satuan', 'qty'],
	data: []
});

function dataGrid_viInvDistribusiBHP(mod_id_viInvDistribusiBHP){	
    var FieldMaster_viInvDistribusiBHP = 
	[
		'no_permintaan','no_minta','tgl_minta', 'jenis_minta','jenis'
	];
	  var FieldMaster_viInvDistribusiBHP_Permintaan = 
	[
		'no_permintaan','lokasi', 'tanggal','keterangan','posting'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viInvDistribusiBHP = new WebApp.DataStore
	({
        fields: FieldMaster_viInvDistribusiBHP
    });
    dataSource_viInvDistribusiBHP_permintaan = new WebApp.DataStore
	({
        fields: FieldMaster_viInvDistribusiBHP_Permintaan
    });
    getDataGridAwalInvDistribusiBHP();
    loadDataComboLokasi_Permintaan();
    // Grid Gizi Perencanaan # --------------
	GridDataView_viInvDistribusiBHP = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: 'Daftar Distribusi',
			store: dataSource_viInvDistribusiBHP,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viInvDistribusiBHP = undefined;
							rowSelected_viInvDistribusiBHP = dataSource_viInvDistribusiBHP.getAt(row);
							CurrentData_viInvDistribusiBHP
							CurrentData_viInvDistribusiBHP.row = row;
							CurrentData_viInvDistribusiBHP.data = rowSelected_viInvDistribusiBHP.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx){
					rowSelected_viInvDistribusiBHP = dataSource_viInvDistribusiBHP.getAt(ridx);
					if (rowSelected_viInvDistribusiBHP != undefined){
						setLookUp_viInvDistribusiBHP(rowSelected_viInvDistribusiBHP.data);
					}
					else{
						setLookUp_viInvDistribusiBHP();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Gizi perencanaan
	        *	Terdiri dari : Judul, Isi dan Event
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header		: 'Status Posting',
						width		: 10,
						sortable	: false,
						hideable	: true,
						hidden		: false,
						menuDisabled: true,
						dataIndex	: 'update_stok',
						renderer	: function(value, metaData, record, rowIndex, colIndex, store){
							 switch (value){
								 case 't':
									 metaData.css = 'StatusHijau'; 
									 break;
								 case 'f':
									 metaData.css = 'StatusMerah';
									 break;
							 }
							 return '';
						}
					},
					{
						header: 'No. Keluar',
						dataIndex: 'no_keluar',
						hideable:false,
						menuDisabled: true,
						width: 25
						
					},
					{
						header: 'Jenis Lokasi',
						dataIndex: 'jns_lokasi',
						hideable:false,
						menuDisabled: true,
						width: 25
						
					},
					{
						header: 'Lokasi',
						dataIndex: 'lokasi',
						hideable:false,
						menuDisabled: true,
						width: 25
						
					},
					//-------------- ## --------------
					{
						header: 'Tanggal Distribusi',
						dataIndex: 'tanggal',
						hideable:false,
						menuDisabled: true,
						width: 30,
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.tanggal);
						}
					},
					{
						header: 'Keterangan',
						dataIndex: 'keterangan',
						hideable:false,
						menuDisabled: true,
						width: 30
					},
					//-------------- ## --------------
					
					//-------------- ## --------------
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viInvDistribusiBHP',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add Distribusi',
						iconCls: 'Edit_Tr',
						tooltip: 'Add Data',
						id: 'btnTambah_viInvDistribusiBHP',
						handler: function(sm, row, rec){
							setLookUp_viInvDistribusiBHP();
							//Ext.getCmp('btnAdd_viInvDistribusiBHP').disable();
						}
					},{
						xtype: 'button',
						text: 'Edit Pembelian',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viInvDistribusiBHP',
						handler: function(sm, row, rec){
							if (rowSelected_viInvDistribusiBHP != undefined)
							{
								setLookUp_viInvDistribusiBHP( rowSelected_viInvDistribusiBHP.data)
							}
							
						}
					},{
						xtype: 'button',
						text: 'Hapus Distribusi',
						iconCls: 'remove',
						tooltip: 'Delete Data Distribusi',
						id: 'btnDelete_viInvDistribusiBHP',
						handler: function(sm, row, rec){
							// DISINI
							console.log(rowSelected_viInvDistribusiBHP);

							Ext.Msg.confirm('Hapus distribusi', 'Apakah distribusi ini akan dihapus?', function(button){
								if (rowSelected_viInvDistribusiBHP == undefined || rowSelected_viInvDistribusiBHP == 'undefined') {

								}else{
									if (button == 'yes'){
										Ext.Ajax.request({
											url: baseURL + "index.php/bhp/functionDistribusiBHP/hapus_distribusi",
											params:{
												no_permintaan 	: rowSelected_viInvDistribusiBHP.data.no_permintaan,
												no_keluar 		: rowSelected_viInvDistribusiBHP.data.no_keluar,
											},
											failure: function(o)
											{
												ShowPesanErrorInvDistribusiBHP('Error, hapus permintaan! Hubungi Admin', 'Error');
											},	
											success: function(o) {
												var cst = Ext.decode(o.responseText);
												if (cst.status === true) {
													getDataGridAwalInvDistribusiBHP();
												}
											}
										});
									}
								}
							});
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viInvDistribusiBHP, selectCount_viInvDistribusiBHP, dataSource_viInvDistribusiBHP),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )

	GridDataView_viInvDistribusiBHP_Permintaan = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: 'Daftar Permintaan BHP',
			store: dataSource_viInvDistribusiBHP_permintaan,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viInvDistribusiBHP_Permintaan = undefined;
							rowSelected_viInvDistribusiBHP_Permintaan = dataSource_viInvDistribusiBHP.getAt(row);
							CurrentData_viInvDistribusiBHP_Permintaan
							CurrentData_viInvDistribusiBHP_Permintaan.row = row;
							CurrentData_viInvDistribusiBHP_Permintaan.data = rowSelected_viInvDistribusiBHP_Permintaan;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx){
					rowSelected_viInvDistribusiBHP_Permintaan = dataSource_viInvDistribusiBHP_permintaan.getAt(ridx);
					if (rowSelected_viInvDistribusiBHP_Permintaan != undefined){
						setLookUp_viInvDistribusiBHP( rowSelected_viInvDistribusiBHP_Permintaan.data);
					}
					else{
						setLookUp_viInvDistribusiBHP();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Gizi perencanaan
	        *	Terdiri dari : Judul, Isi dan Event
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header		: 'Status Posting',
						width		: 10,
						sortable	: false,
						hideable	: true,
						hidden		: false,
						menuDisabled: true,
						dataIndex	: 'update_stok',
						renderer	: function(value, metaData, record, rowIndex, colIndex, store){
							 switch (value){
								 case 't':
									 metaData.css = 'StatusHijau'; 
									 break;
								 case 'f':
									 metaData.css = 'StatusMerah';
									 break;
							 }
							 return '';
						}
					},
					{
						header: 'No. Permintaan',
						dataIndex: 'no_permintaan',
						hideable:false,
						menuDisabled: true,
						width: 25
						
					},
					{
						header: 'Lokasi/Unit Kerja',
						dataIndex: 'lokasi',
						hideable:false,
						menuDisabled: true,
						width: 25
						
					},
					{
						header: 'Tanggal',
						dataIndex: 'tanggal',
						hideable:false,
						menuDisabled: true,
						width: 25,
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.tanggal);
						}
						
					},
					//-------------- ## --------------
					{
						header: 'Keterangan',
						dataIndex: 'keterangan',
						hideable:false,
						menuDisabled: true,
						width: 30,
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.tanggal);
						}
					},
					
					//-------------- ## --------------
					
					//-------------- ## --------------
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viInvDistribusiBHP_permintaan',
				items: 
				[
					{
							xtype: 'datefield',
							id: 'dfTglAwalInvDistribusiBHP_Permintaan',
							format: 'd/M/Y',
							value:now_viInvDistribusiBHP,
							width: 100,
							tabIndex:1
					},
					{},
					{
						xtype: 'label',
						text: 's/d'
					},
					{},
					{
						
						xtype: 'datefield',
						id: 'dfTglAkhirInvDistribusiBHP_Permintaan', //ini coy
						format: 'd/M/Y',
						value:now_viInvDistribusiBHP,
						width: 100,
						tabIndex:1
					},
					{},
					{
						xtype: 'label',
						text: 'Lokasi'
					},
					{},
					comboJenisLokasiInvDistribusiBHP_bhp_Permintaan(),
					{},
					{
						xtype: 'button',
						text: 'Cari',
						iconCls: 'find',
						tooltip: 'Cari',
						id: 'btn_cari_permintaan_bhp',
						handler: function(sm, row, rec){
							if (tmp_kd_lokasi_permintaan != undefined){
								Ext.Ajax.request({
								url: baseURL + "index.php/bhp/functionDistribusiBHP/get_list_permintaan",
								params: {
									lokasi 		 :tmp_kd_lokasi_permintaan,
									tgl_awal 	 :Ext.getCmp('dfTglAwalInvDistribusiBHP_Permintaan').getValue(),
									tgl_akhir 	 :Ext.getCmp('dfTglAkhirInvDistribusiBHP_Permintaan').getValue()
								},
								failure: function(o){
									loadMask.hide();
									ShowPesanErrorInvDistribusiBHP('Hubungi Admin', 'Error');
								},	
								success: function(o) {
									var cst = Ext.decode(o.responseText);
										if (cst.success === true) {
											dataSource_viInvDistribusiBHP_permintaan.removeAll();
											var recs=[],
												recType=dataSource_viInvDistribusiBHP_permintaan.recordType;
											for(var i=0; i<cst.ListDataObj.length; i++){
												recs.push(new recType(cst.ListDataObj[i]));
												}
												dataSource_viInvDistribusiBHP_permintaan.add(recs);
											
											GridDataView_viInvDistribusiBHP_Permintaan.getView().refresh();
										}
										else{
											ShowPesanErrorInvDistribusiBHP('Gagal membaca data Permintaab BHP', 'Error');
										};
								}
							})

							}else{
								ShowPesanWarningInvDistribusiBHP('Lokasi/Unit Kerja Harus Di Pilih','Warning')
							}
							
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viInvDistribusiBHP, selectCount_viInvDistribusiBHP, dataSource_viInvDistribusiBHP),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )
	
	var pencarianInvDistribusiBHP = new Ext.FormPanel({
        labelAlign: 'top',
        frame:true,
        title: '',
        bodyStyle:'padding:5px 5px 0',
        items: [
		{
			layout: 'column',
			border: false,
			items:
			[
				{
					columnWidth:.98,
					layout: 'absolute',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					width: 500,
					height: 90,
					anchor: '100% 100%',
					items:
					[
						{
							x: 10,
							y: 0,
							xtype: 'label',
							text: 'No. Keluar'
						},
						{
							x: 120,
							y: 0,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 0,
							xtype: 'textfield',
							id: 'TxtFilterGridNoKeluarInvDistribusiBHP',
							name: 'TxtFilterGridNoKeluarInvDistribusiBHP',
							width: 350,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										cNoKeluar_viInvDistribusiBHP =Ext.getCmp('TxtFilterGridNoKeluarInvDistribusiBHP').getValue();
										cTglAwal_viInvDistribusiBHP=Ext.getCmp('dfTglAwalInvDistribusiBHP').getValue();
										cTglAkhir_viInvDistribusiBHP=Ext.getCmp('dfTglAkhirInvDistribusiBHP').getValue();
										getDataGridAwalInvDistribusiBHP(cNoKeluar_viInvDistribusiBHP,cTglAwal_viInvDistribusiBHP,cTglAkhir_viInvDistribusiBHP);
									} 						
								}
							}
						},
						//----------------------------------------
						{
							x: 10,
							y: 60,
							xtype: 'label',
							text: '*) Enter untuk mencari'
						},
						//----------------------------------------
						{
							x: 580,
							y: 60,
							xtype: 'button',
							text: 'Refresh',
							iconCls: 'refresh',
							tooltip: 'Refresh',
							style:{paddingLeft:'30px'},
							width:150,
							id: 'BtnFilterGridCari_viInvDistribusiBHP',
							handler: function() 
							{					
								getDataGridAwalInvDistribusiBHP();
							}                        
						},
						{
							x: 10,
							y: 30,
							xtype: 'label',
							text: 'Tanggal'
						},
						{
							x: 120,
							y: 30,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 30,
							xtype: 'datefield',
							id: 'dfTglAwalInvDistribusiBHP',
							format: 'd/M/Y',
							value:now_viInvDistribusiBHP,
							width: 150,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										cNoKeluar_viInvDistribusiBHP =Ext.getCmp('TxtFilterGridNoKeluarInvDistribusiBHP').getValue();
										cTglAwal_viInvDistribusiBHP=Ext.getCmp('dfTglAwalInvDistribusiBHP').getValue();
										cTglAkhir_viInvDistribusiBHP=Ext.getCmp('dfTglAkhirInvDistribusiBHP').getValue();
										getDataGridAwalInvDistribusiBHP(cNoKeluar_viInvDistribusiBHP,cTglAwal_viInvDistribusiBHP,cTglAkhir_viInvDistribusiBHP);
									} 						
								}
							}
						},
						{
							x: 290,
							y: 30,
							xtype: 'label',
							text: 's/d'
						},
						{
							x: 320,
							y: 30,
							xtype: 'datefield',
							id: 'dfTglAkhirInvDistribusiBHP',
							format: 'd/M/Y',
							value:now_viInvDistribusiBHP,
							width: 150,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										cNoKeluar_viInvDistribusiBHP =Ext.getCmp('TxtFilterGridNoKeluarInvDistribusiBHP').getValue();
										cTglAwal_viInvDistribusiBHP=Ext.getCmp('dfTglAwalInvDistribusiBHP').getValue();
										cTglAkhir_viInvDistribusiBHP=Ext.getCmp('dfTglAkhirInvDistribusiBHP').getValue();
										getDataGridAwalInvDistribusiBHP(cNoKeluar_viInvDistribusiBHP,cTglAwal_viInvDistribusiBHP,cTglAkhir_viInvDistribusiBHP);
									} 						
								}
							}
						}
					]
				}
			]
		}
		]	
		
	})

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viInvDistribusiBHP = new Ext.Panel
    (
		{
			title: NamaForm_viInvDistribusiBHP,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viInvDistribusiBHP,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ pencarianInvDistribusiBHP,
					GridDataView_viInvDistribusiBHP,GridDataView_viInvDistribusiBHP_Permintaan],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viInvDistribusiBHP,
		            columns: 21,
		            defaults: {
					scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viInvDistribusiBHP;
    //-------------- # End form filter # --------------
}

function refreshInvDistribusiBHP(kriteria)
{
    dataSource_viInvDistribusiBHP.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'ViewPermintaanBahan',
                    param : kriteria
                }			
            }
        );   
    return dataSource_viInvDistribusiBHP;
}

function setLookUp_viInvDistribusiBHP( rowdata){
	loadDataComboLokasi();
	console.log(rowdata);
    var lebar = 790;
    setLookUps_viInvDistribusiBHP = new Ext.Window({
        id: Nci.getId(),
        title: NamaForm_viInvDistribusiBHP, 
        closeAction: 'destroy',        
        width: 800,
        height: 655,
		constrain:true,
		autoHeight:true,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viInvDistribusiBHP(lebar,rowdata),
        listeners:{
            activate: function(){
				
            },
            afterShow: function(){
                this.activate();
            },
            deactivate: function(){
                rowSelected_viInvDistribusiBHP=undefined;
            }
        }
    });

    setLookUps_viInvDistribusiBHP.show();
    if (rowdata == undefined){
	
    }
    else{
        datainit_viInvDistribusiBHP( rowdata);
    }
}

function getFormItemEntry_viInvDistribusiBHP(lebar,rowdata){
    var pnlFormDataBasic_viInvDistribusiBHP = new Ext.FormPanel({
		title: '',
		region: 'north',
		layout: 'form',
		bodyStyle: 'padding:10px 10px 10px 10px',
		anchor: '100%',
		labelWidth: 1,
		autoWidth: true,
		width: lebar,
		border: false,
		items:[
				getItemPanelInputPerencanaan_viInvDistribusiBHP(lebar),
				getItemGridPerencanaan_viInvDistribusiBHP(lebar)
			],
			fileUpload: true,
		tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAdd_viInvDistribusiBHP',
						handler: function(){
							dataaddnew_viInvDistribusiBHP();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						disabled: true,
						id: 'btnSimpan_viInvDistribusiBHP',
						handler: function()
						{
							datasave_viInvDistribusiBHP();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save & Close',
						disabled: true,
						iconCls: 'saveexit',
						id: 'btnSimpanExit_viInvDistribusiBHP',
						handler: function()
						{
							datasave_viInvDistribusiBHP();
							refreshInvDistribusiBHP();
							setLookUps_viInvDistribusiBHP.close();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						text	: 'Delete',
						id		: 'btnHapusDistribusi_InvDistribusiBHP',
						tooltip	: nmLookup,
						iconCls	: 'remove',
						disabled: true,
						handler	: function(){
							Ext.Msg.confirm('Hapus permintaan', 'Apakah distribusi ini akan dihapus?', function(button){
								if (button == 'yes'){
									Ext.Ajax.request
									(
										{
											url: baseURL + "index.php/bhp/functionDistribusiBHP/hapusDistribusi",
											params:{no_keluar:Ext.getCmp('txtNoKeluar_InvDistribusiBHPL').getValue()} ,
											failure: function(o)
											{
												ShowPesanErrorInvDistribusiBHP('Error, hapus permintaan! Hubungi Admin', 'Error');
											},	
											success: function(o) 
											{
												var cst = Ext.decode(o.responseText);
												if (cst.success === true) 
												{
													getDataGridAwalInvDistribusiBHP();
													setLookUps_viInvDistribusiBHP.close();
												}
												else 
												{
													ShowPesanErrorInvDistribusiBHP('Gagal menghapus distribusi ini', 'Error');
												};
											}
										}
										
									)
								}
							});		
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Posting',
						disabled: true,
						iconCls: 'gantidok',
						id: 'btnPosting_viInvDistribusiBHP',
						handler: function()
						{
							Ext.Msg.confirm('Posting Penerimaan', 'Apakah anda yakin disribusi ini akan diPosting? Pastikan data sudah benar sebelum diPosting', function(button){
								if (button == 'yes'){
									dataposting_viInvDistribusiBHP();
								}
							})
							
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: '<i class="fa fa-reply" aria-hidden="true"></i> Unposting',
						disabled: true,
						id: 'btnUnPosting_viInvDistribusiBHP',
						handler: function()
						{
							Ext.Msg.confirm('Unposting Penerimaan', 'Apakah anda yakin disribusi ini akan diUnposting? ', function(button){
								if (button == 'yes'){
									dataUnposting_viInvDistribusiBHP();
								}
							})
							
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Print',
						disabled: true,
						iconCls: 'print',
						id: 'btnPrint_viInvDistribusiBHP',
						handler: function(){
							if(Ext.getCmp('txtNoKeluar_InvDistribusiBHPL').getValue() ==''){
								ShowPesanWarningInvDistribusiBHP('Mohon Save Terlebih Dahulu !','Warning');
							}else{
								Ext.Msg.confirm('Konfirmasi', 'Siap mencetak bill ?', function(button){
								if (button == 'yes'){
									var params={
										no_keluar:Ext.getCmp('txtNoKeluar_InvDistribusiBHPL').getValue()
									};
									var form = document.createElement("form");
									form.setAttribute("method", "post");
									form.setAttribute("target", "_blank");
									form.setAttribute("action", baseURL + "index.php/bhp/cetakan_faktur_bill/cetak_distribusi_bhp");
									var hiddenField = document.createElement("input");
									hiddenField.setAttribute("type", "hidden");
									hiddenField.setAttribute("name", "data");
									hiddenField.setAttribute("value", Ext.encode(params));
									form.appendChild(hiddenField);
									document.body.appendChild(form);
									form.submit();	
								}
							})	
							}
							
							
						}
					},
					
				]
			}
		}
    )

    return pnlFormDataBasic_viInvDistribusiBHP;
}

function getItemPanelInputPerencanaan_viInvDistribusiBHP(lebar) {
    var items =
	{
		title:'',
		layout:'column',
		items:
		[
			{
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: false,
				width: lebar-22,
				height: 65,
				anchor: '100% 100%',
				items:
				[
					{
						x:10,
						y:5,
						xtype: 'label',
						text:'No. Keluar'
					},
					{
						x:70,
						y:5,
						xtype: 'label',
						text:':'
					},
					{
						x:80,
						y:5,
						xtype: 'textfield',
						name: 'txtNoKeluar_InvDistribusiBHPL',
						id: 'txtNoKeluar_InvDistribusiBHPL',
						readOnly:true,
						tabIndex:0,
						width: 110
					},
					{
						x:10,
						y:30,
						xtype: 'label',
						text:'Lokasi'
					},
					{
						x:70,
						y:30,
						xtype: 'label',
						text:':'
					},
					comboJenisLokasiInvDistribusiBHP_bhp(),
					/*{
						x:10,
						y:55,
						xtype: 'label',
						text:'Lokasi'
					},
					{
						x:70,
						y:55,
						xtype: 'label',
						text:':'
					},
					comboLokasiInvDistribusiBHP(),*/
					{
						xtype: 'textfield',
						name: 'txt_tmp_kd_lokasi_permintaan',
						id: 'txt_tmp_kd_lokasi_permintaan',
						readOnly:true,
						hidden:true,
						tabIndex:0,
						width: 110
					},
					//-------------------------------------
					{
						x:200,
						y:8,
						xtype: 'checkbox',
						boxLabel: 'Posting',
						id: 'cbStokUpdate_InvDistribusiBHP',
						checked: false,
						inputValue: 'update',
						disabled:true
					},
					//----------------------------------------------
					{
						x:360,
						y:5,
						xtype: 'label',
						text:'Tanggal'
					},
					{
						x:430,
						y:5,
						xtype: 'label',
						text:':'
					},
					{
						x:440,
						y:5,
						xtype: 'datefield',
						name: 'dfTgl_InvDistribusiBHPL',
						id	: 'dfTgl_InvDistribusiBHPL',
						format: 'd/M/Y',
						value:now_viInvDistribusiBHP,
						width: 130
					},
					{
						x:360,
						y:30,
						xtype: 'label',
						text:'Keterangan'
					},
					{
						x:430,
						y:30,
						xtype: 'label',
						text:':'
					},
					{
						x:440,
						y:30,
						xtype: 'textfield',
						name: 'txtKeterangan_InvDistribusiBHPL',
						id: 'txtKeterangan_InvDistribusiBHPL',
						width: 300
					},
					{
						x:360,
						y:55,
						xtype: 'label',
						text:'No. SPK',
						hidden:true,
					},
					{
						x:430,
						y:55,
						xtype: 'label',
						text:':',
						hidden:true,
					},
					{
						x:440,
						y:55,
						xtype: 'textfield',
						hidden:true,
						name: 'txtNoSPK_InvDistribusiBHPL',
						id: 'txtNoSPK_InvDistribusiBHPL',
						tabIndex:0,
						width: 110
					},
					//-------------------------------------------------
					{
						x:560,
						y:55,
						hidden:true,
						xtype: 'label',
						text:'Tanggal SPK'
					},
					{
						x:625,
						y:55,
						hidden:true,
						xtype: 'label',
						text:':'
					},
					{
						x:635,
						y:55,
						hidden:true,
						xtype: 'datefield',
						name: 'dfTglSPK_InvDistribusiBHPL',
						id	: 'dfTglSPK_InvDistribusiBHPL',
						format: 'd/M/Y',
						value:now_viInvDistribusiBHP,
						width: 105
					},
				]
			}
		
		]
	};
    return items;
};

function getItemGridPerencanaan_viInvDistribusiBHP(lebar) 
{
    var items =
	{
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:1px 1px 1px 1px',
		border:true,
		width: lebar-55,
		height: 350,
		tbar:
		[
			{
				text	: 'Add Barang',
				id		: 'btnAddDistribusiBarang',
				tooltip	: nmLookup,
				disabled: false,
				iconCls	: 'find',
				handler	: function(){
					var records = new Array();
					records.push(new dsDataGrdBarang_viInvDistribusiBHP.recordType());
					dsDataGrdBarang_viInvDistribusiBHP.add(records);
				}
			},	
			{
				text	: 'Delete',
				id		: 'btnHapusGridBarang_InvDistribusiBHP',
				tooltip	: nmLookup,
				iconCls	: 'remove',
				disabled: true,
				handler: function()
				{
					var line =  InvDistribusiBHP.form.Grid.a.getSelectionModel().selection.cell[0];
					var o = dsDataGrdBarang_viInvDistribusiBHP.getRange()[line].data;
					if(dsDataGrdBarang_viInvDistribusiBHP.getCount()>1){
						Ext.Msg.confirm('Warning', 'Apakah barang ini akan dihapus?', function(button){
							if (button == 'yes'){
								if(dsDataGrdBarang_viInvDistribusiBHP.getRange()[line].data.no_baris != undefined){
									Ext.Ajax.request
									(
										{
											url: baseURL + "index.php/bhp/functionDistribusiBHP/hapusBarisGridBarang",
											params: {
												no_keluar:Ext.getCmp('txtNoKeluar_InvDistribusiBHPL').getValue(),
												no_baris:o.no_baris,
												kd_stok:o.kd_stok
											},
											failure: function(o)
											{
												ShowPesanErrorInvDistribusiBHP('Error, hapus barang! Hubungi Admin', 'Error');
											},	
											success: function(o) 
											{
												var cst = Ext.decode(o.responseText);
												if (cst.success === true) 
												{
													dsDataGrdBarang_viInvDistribusiBHP.removeAt(line);
													InvDistribusiBHP.form.Grid.a.getView().refresh();
													Ext.getCmp('btnSimpan_viInvDistribusiBHP').enable();
													Ext.getCmp('btnSimpanExit_viInvDistribusiBHP').enable();
												}
												else 
												{
													ShowPesanErrorInvDistribusiBHP('Gagal menghapus barang ini', 'Error');
												};
											}
										}
										
									)
								}else{
									dsDataGrdBarang_viInvDistribusiBHP.removeAt(line);
									InvDistribusiBHP.form.Grid.a.getView().refresh();
									Ext.getCmp('btnSimpan_viInvDistribusiBHP').enable();
									Ext.getCmp('btnSimpanExit_viInvDistribusiBHP').enable();
								}
							} 
							
						});
					} else{
						ShowPesanErrorInvDistribusiBHP('Data tidak bisa dihapus karena minimal barang 1','Error');
					}
					
				}
			}	
		],
	    items:
		[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:
				[
					gridDataViewEdit_viInvDistribusiBHP(),
				]	
			}
			//-------------- ## --------------
		],
	};
    return items;
};

function gridDataViewEdit_viInvDistribusiBHP()
{
    var FieldGrdKasir_viInvDistribusiBHP = [];
	
    dsDataGrdBarang_viInvDistribusiBHP= new WebApp.DataStore
	({
        fields: FieldGrdKasir_viInvDistribusiBHP
    });
    
    InvDistribusiBHP.form.Grid.a =new Ext.grid.EditorGridPanel({
        store: dsDataGrdBarang_viInvDistribusiBHP,
        height: 320,//220,
		stripeRows: true,
		columnLines: true,
		selModel: new Ext.grid.CellSelectionModel({
	            singleSelect: true,
	            listeners: {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
        }),
        
        columns: 
		[	
			new Ext.grid.RowNumberer(),
			{			
				dataIndex: 'kd_inv',
				header: 'Kode Kelompok',
				sortable: true,
				width: 80
			},
			{			
				dataIndex: 'no_urut_brg',
				header: 'No Urut',
				sortable: true,
				width: 80
			},
			{			
				dataIndex: 'nama_brg',
				header: 'Nama Barang',
				sortable: true,
				width: 200,
				editor: new Ext.form.TextField({
					allowBlank: false,
					listeners:{
						blur: function(a){
							var line =  InvDistribusiBHP.form.Grid.a.getSelectionModel().selection.cell[0];
							var o = dsDataGrdBarang_viInvDistribusiBHP.getRange()[line].data;
							InvDistribusiBHP.vars.nama=o.nama_brg;
							getDataGridBarangInvDistribusiBHP(InvDistribusiBHP.vars.nama);
							getDataGridStokBarang(InvDistribusiBHP.vars.nama);
							setLookUpBarang_viInvDistribusiBHP();
						},
						focus: function(a){
							//this.index=InvDistribusiBHP.form.Grid.a.getSelectionModel().selection.cell[0]
						}
					}
				})	
			},
			{			
				dataIndex: 'satuan',
				header: 'Satuan',
				sortable: true,
				width: 80
			},
			{
				dataIndex: 'jumlah_total',
				header: 'Jumlah',
				align:'right',
				xtype:'numbercolumn',
				sortable: true,
				width: 80,
				editor: new Ext.form.NumberField({
					allowBlank: false,
					listeners:{
						blur: function(a){
							var line	= this.index;
							if(a.getValue() == 0){
								ShowPesanWarningInvDistribusiBHP('Jumlah tidak boleh 0', 'Warning');
							}
						},
						focus: function(a){
							
						}
					}
				})	
			},
			{
				dataIndex: 'keterangan',
				header: 'Keterangan',
				sortable: true,
				width: 100,
				editor: new Ext.form.TextField({
					allowBlank: false,
					listeners:{
						blur: function(a){
							
						},
						focus: function(a){
							
						}
					}
				})	
			},
			//-------------- ## --------------
			{			
				dataIndex: 'kd_satuan',
				header: 'kd_satuan',
				sortable: true,
				width: 80,
				hidden:true
			},
			{			
				dataIndex: 'kd_stok',
				header: 'kd_stok',
				sortable: true,
				width: 80,
				hidden:true
			},
			{			
				dataIndex: 'no_baris',
				header: 'no_baris',
				sortable: true,
				width: 80,
				hidden:true
			},
			//-------------- ## --------------
        ],
		viewConfig: 
			{
				forceFit: true
			}

       // plugins:chkSelected_viInvDistribusiBHP,
    });
    return  InvDistribusiBHP.form.Grid.a;
}

function setLookUpBarang_viInvDistribusiBHP(rowdata){
    var lebar = 790;
    lookUp_viInvDistribusiBHP = new Ext.Window({
        id: Nci.getId(),
        title: 'Barang BHP', 
        closeAction: 'destroy',        
        width: 680,
        height: 655,
		constrain:true,
		autoHeight:true,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemBarang_viInvDistribusiBHP(lebar,rowdata),
        listeners:{
            activate: function(){
				
            },
            afterShow: function(){
                this.activate();
            },
            deactivate: function(){
                rowLookUpSelected_viInvDistribusiBHP=undefined;
            }
        }
    });

    lookUp_viInvDistribusiBHP.show();

   /*  if (rowdata == undefined){
	
    }
    else
    {
        datainit_viInvDistribusiBHP(rowdata);
    } */
}

function getFormItemBarang_viInvDistribusiBHP(lebar,rowdata){
    var pnlFormDataBarang_viInvDistribusiBHP = new Ext.Panel({
		title: '',
		region: 'north',
		layout: 'form',
		bodyStyle: 'padding:10px 10px 10px 10px',
		anchor: '100%',
		labelWidth: 1,
		autoWidth: true,
		width: lebar,
		border: false,
		items:[
			{
				layout: 'absolute',
				bodyStyle: 'padding: 0px 0px 0px 0px',
				border: false,
				width: 50,
				height: 33,
				anchor: '100%',
				items:
				[
					{
						x:200,
						y:0,
						xtype: 'label',
						text:'Dari Tanggal'
					},
					{
						x:280,
						y:0,
						xtype: 'label',
						text:':'
					},
					{
						x:290,
						y:0,
						xtype: 'datefield',
						name: 'dfGridTglPosting_InvDistribusiBHPL',
						id	: 'dfGridTglPosting_InvDistribusiBHPL',
						format: 'd/M/Y',
						value:now_viInvDistribusiBHP,
						width: 105
					},
					{
						x:410,
						y:0,
						xtype: 'label',
						text:'s/d'
					},
					{
						x:440,
						y:0,
						xtype: 'datefield',
						name: 'dfGridTglPosting2_InvDistribusiBHPL',
						id	: 'dfGridTglPosting2_InvDistribusiBHPL',
						format: 'd/M/Y',
						value:now_viInvDistribusiBHP,
						width: 105
					},

					{
						x:570,
						y:0,
						xtype: 'button',
						text: 'Cari',
						iconCls:'find',
						id: 'btn_cari_barang_InvDistribusiBHPL',
						handler: function(){
							console.log('cari');
							get_barang();
						}
					},
				]
			},
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.30,
						layout: 'absolute',
						bodyStyle: 'padding: 0px 10px 0px 0px',
						border: false,
						anchor: '100% 100%',
						height: 300,
						items:
						[
							ListGridBarang_viInvDistribusiBHP()
						]
					},
					{
						columnWidth:.70,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 0px 10px 10px',
						border: false,
						anchor: '100% 100%',
						height: 300,
						items:
						[
							getItemGridBarang_viInvDistribusiBHP()
						]
					}
				]
			}
		],
		fileUpload: true,
		fbar: {
				items: 
				[
					{
						xtype: 'button',
						text: 'Ok',
						id: 'btnOkLookUpBarang_viInvDistribusiBHP',
						handler: function(){
							var recs=[],
								recType=dsDataGrdBarang_viInvDistribusiBHP.recordType;
							for(var i = 0; i < dsDataGrdDetailBarang_viInvDistribusiBHP.getCount(); i++){
								if(dsDataGrdDetailBarang_viInvDistribusiBHP.data.items[i].data.checked == true){
									console.log(dsDataGrdDetailBarang_viInvDistribusiBHP.data.items[i].data);
									//recs.push(new recType(dsDataGrdDetailBarang_viInvDistribusiBHP.data.items[i].data));
									
									console.log(dsDataGrdBarang_viInvDistribusiBHP.data.items[dsDataGrdBarang_viInvDistribusiBHP.getCount()-1].data.kd_inv);
									
									if(dsDataGrdBarang_viInvDistribusiBHP.data.items[dsDataGrdBarang_viInvDistribusiBHP.getCount()-1].data.kd_inv === '' || dsDataGrdBarang_viInvDistribusiBHP.data.items[dsDataGrdBarang_viInvDistribusiBHP.getCount()-1].data.kd_inv === undefined){
										dsDataGrdBarang_viInvDistribusiBHP.removeAt(dsDataGrdBarang_viInvDistribusiBHP.getCount()-1);
									}	
									recs=new recType(dsDataGrdDetailBarang_viInvDistribusiBHP.data.items[i].data);
									dsDataGrdBarang_viInvDistribusiBHP.add(recs);
								}
							}
							gridPanelLookUpBarang_InvDistribusiBHP.getView().refresh();
							Ext.getCmp('btnSimpan_viInvDistribusiBHP').enable();
							Ext.getCmp('btnSimpanExit_viInvDistribusiBHP').enable();
							Ext.getCmp('btnHapusGridBarang_InvDistribusiBHP').enable();
							lookUp_viInvDistribusiBHP.close();
						}
					},
					{
						xtype: 'button',
						text: 'Batal',
						id: 'btnBatalLookUpBarang_viInvDistribusiBHP',
						handler: function()
						{
							lookUp_viInvDistribusiBHP.close();
						}
					},
					
				]
			}
		}
    )

    return pnlFormDataBarang_viInvDistribusiBHP;
}

function getItemGridBarang_viInvDistribusiBHP(){
	chkSelected_viInvDistribusiBHP = new Ext.grid.CheckColumn({
		id: 'checkedpilihan',
		header: '',
		align: 'center',						
		dataIndex: 'checked',			
		width: 20
	});
    var FieldGrdBArang_viInvDistribusiBHP = [];
	
    dsDataGrdDetailBarang_viInvDistribusiBHP= new WebApp.DataStore
	({
        fields: FieldGrdBArang_viInvDistribusiBHP
    });
    
    gridPanelLookUpBarang_InvDistribusiBHP =new Ext.grid.EditorGridPanel({
        store: dsDataGrdDetailBarang_viInvDistribusiBHP,
        height: 290,//220,
		stripeRows: true,
		columnLines: true,
		border:true,
		selModel: new Ext.grid.CellSelectionModel({
			singleSelect: true,
			listeners: {
				cellselect: function(sm, row, rec)
				{
					
				}
			}
        }),
        
        columns: 
		[	
			new Ext.grid.RowNumberer(),
			chkSelected_viInvDistribusiBHP,
			{			
				dataIndex: 'kd_stok',
				header: 'Kd.Stok',
				sortable: true,
				width: 90
			},
			{			
				dataIndex: 'tgl_posting',
				header: 'Tanggal',
				sortable: true,
				width: 80,
				renderer: function(v, params, record)
				{
					return ShowDate(record.data.tgl_posting);
				}
				
			},
			{			
				dataIndex: 'kd_inv',
				header: 'Kd.Kelompok',
				sortable: true,
				width: 65,
				hidden:true
			},
			{			
				dataIndex: 'no_urut_brg',
				header: 'No Urut',
				width: 80,
			},
			{
				dataIndex: 'nama_brg',
				header: 'Deskripsi',
				sortable: true,
				width: 120
			},
			{			
				dataIndex: 'jumlah_total',
				header: 'Jml',
				align:'right',
				width: 60,
			},
			{			
				dataIndex: 'satuan',
				header: 'Satuan',
				width: 80,
				hidden:true
			},
			//-------------- ## --------------
			
			//-------------- ## --------------
        ],
		plugins:chkSelected_viInvDistribusiBHP,
		viewConfig: 
			{
				forceFit: true
			}

		
    });
    return  gridPanelLookUpBarang_InvDistribusiBHP;
}

function ListGridBarang_viInvDistribusiBHP(){
    var FieldListGrdBArang_viInvDistribusiBHP = [];
	
    dsDataListGrdBarang_viInvDistribusiBHP= new WebApp.DataStore
	({
        fields: FieldListGrdBArang_viInvDistribusiBHP
    });
    
    gridPanelLookUpListBarang_InvDistribusiBHP =new Ext.grid.EditorGridPanel({
        store: dsDataListGrdBarang_viInvDistribusiBHP,
        height: 290,//220,
		stripeRows: true,
		columnLines: true,
		border:true,
		autoScroll:true,
		selModel: new Ext.grid.CellSelectionModel({
			singleSelect: true,
			listeners: {
				cellselect: function(sm, row, rec)
				{
					console.log(dsDataListGrdBarang_viInvDistribusiBHP.getAt(row).data.kd_inv);
					InvDistribusiBHP.vars.nama_brg = dsDataListGrdBarang_viInvDistribusiBHP.getAt(row).data.nama_brg;
					InvDistribusiBHP.vars.tglawal = Ext.getCmp('dfGridTglPosting_InvDistribusiBHPL').getValue();
					InvDistribusiBHP.vars.tglakhir = Ext.getCmp('dfGridTglPosting2_InvDistribusiBHPL').getValue();
					getDataGridStokBarang(InvDistribusiBHP.vars.nama_brg,InvDistribusiBHP.vars.tglawal,InvDistribusiBHP.vars.tglakhir);
				}
			}
        }),
        
        columns: 
		[	
			{			
				dataIndex: 'no_urut_brg',
				header: 'no_urut_brg',
				sortable: true,
				width: 80,
				hidden:true
			},
			{			
				dataIndex: 'kd_inv',
				header: 'kd_inv',
				sortable: true,
				width: 80,
				hidden:true
			},
			{			
				dataIndex: 'nama_brg',
				header: 's',
				sortable: true,
				width: 80
			}
			
        ],
		viewConfig: 
			{
				forceFit: true
			}

       // plugins:chkSelected_viInvDistribusiBHP,
    });
    return  gridPanelLookUpListBarang_InvDistribusiBHP;
}

function comboJenisLokasiInvDistribusiBHP_bhp(){ //disini
	var Field_Vendor = ['kd_lokasi', 'lokasi'];
    ds_JnsLokasiInvDistribusiBHP = new WebApp.DataStore({fields: Field_Vendor});
   /* ds_JnsLokasiInvDistribusiBHP.load({
        params:{
	        Skip: 0,
	        Take: 1000,
	        Sort: 'kd_jns_lokasi',
	        Sortdir: 'ASC',
	        target: 'ComboJnsLokasiBHP',
	        param: ""
        }
    });*/
	var cboJnsLokasiInvDistribusiBHP = new Ext.form.ComboBox({
		x:80,
		y:30,
        id:'cbJnsLokasiInvDistribusiBHP',
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:true,
        mode: 'local',
        width: 260,
		readOnly:false,
		fieldLabel:'PBF ',
        store: ds_JnsLokasiInvDistribusiBHP,
        valueField: 'kd_lokasi',
        displayField: 'lokasi',
        listeners:{
			'select': function(a,b,c){
				loaddatastoreLokasiInvDistribusBHP(b.data.kd_lokasi);
				Ext.getCmp('cbLokasiInvDistribusiBHP').enable();
			}
        }
	});
	return cboJnsLokasiInvDistribusiBHP;
};

function loaddatastoreLokasiInvDistribusBHP(kd_jns_lokasi)
{
    dslokasiInvDistribusiBHP.load
	(
		{
		 params:
				{
					Skip: 0,
					Take: 100,
					Sort: 'lokasi',
					Sortdir: 'ASC',
					target: 'ViewComboLokasiBHP',
					param: "kd_jns_lokasi='"+ kd_jns_lokasi+ "'"
				}
	   }
	)      
};

function comboLokasiInvDistribusiBHP()
{
    var Field = ['kd_jns_lokasi','lokasi','kd_lokasi'];

    dslokasiInvDistribusiBHP = new WebApp.DataStore({fields: Field});
	
    var cboLokasiInvDistribusiBHP = new Ext.form.ComboBox
	(
		{
			x : 80,
			y : 55,
		    id: 'cbLokasiInvDistribusiBHP',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
            forceSelection: true,
		    labelWidth:80,
		    align: 'Right',
		    store: dslokasiInvDistribusiBHP,
		    valueField: 'kd_lokasi',
		    displayField: 'lokasi',
			disabled:true,
			width:200,
			listeners:
			{
			    'select': function(a, b, c) 
				{
					Ext.getCmp('btnAddDistribusiBarang').enable();
				}
			}
		}
	);

    return cboLokasiInvDistribusiBHP;
};

function dataaddnew_viInvDistribusiBHP(){
	Ext.getCmp('dfTgl_InvDistribusiBHPL').setValue('');
	Ext.getCmp('cbJnsLokasiInvDistribusiBHP').setValue('');
	Ext.getCmp('cbLokasiInvDistribusiBHP').disable();
	Ext.getCmp('cbLokasiInvDistribusiBHP').setValue('');
	Ext.getCmp('txtKeterangan_InvDistribusiBHPL').setValue('');
	Ext.getCmp('txtNoSPK_InvDistribusiBHPL').setValue('');
	Ext.getCmp('dfTglSPK_InvDistribusiBHPL').setValue('');
	Ext.getCmp('cbStokUpdate_InvDistribusiBHP').setValue(false);
	
	
	Ext.getCmp('dfTgl_InvDistribusiBHPL').setReadOnly(false);
	Ext.getCmp('cbJnsLokasiInvDistribusiBHP').setReadOnly(false);
	Ext.getCmp('cbLokasiInvDistribusiBHP').disable();
	Ext.getCmp('cbLokasiInvDistribusiBHP').setReadOnly(false);
	Ext.getCmp('txtKeterangan_InvDistribusiBHPL').setReadOnly(false);
	Ext.getCmp('txtNoSPK_InvDistribusiBHPL').setReadOnly(false);
	Ext.getCmp('dfTglSPK_InvDistribusiBHPL').setReadOnly(false);
	
	InvDistribusiBHP.vars.nama_brg = '';
	InvDistribusiBHP.vars.tglawal = '';
	InvDistribusiBHP.vars.tglakhir = '';
	InvDistribusiBHP.vars.nama = '';
	
	dsDataGrdBarang_viInvDistribusiBHP.removeAll();
}

function datainit_viInvDistribusiBHP(rowdata){
	console.log(rowdata);
	tmp_no_permintaan=rowdata.no_permintaan;
	loadDetailPermintaanBHP(rowdata.no_permintaan);
	Ext.getCmp('txtNoKeluar_InvDistribusiBHPL').setValue(rowdata.no_keluar);
	Ext.getCmp('dfTgl_InvDistribusiBHPL').setValue(ShowDate(rowdata.tanggal));
	Ext.getCmp('cbJnsLokasiInvDistribusiBHP').setValue(rowdata.jns_lokasi);
	Ext.getCmp('cbJnsLokasiInvDistribusiBHP').setValue(rowdata.lokasi);
	Ext.getCmp('txtKeterangan_InvDistribusiBHPL').setValue(rowdata.keterangan);
	Ext.getCmp('txtNoSPK_InvDistribusiBHPL').setValue(rowdata.no_spk);
	Ext.getCmp('dfTglSPK_InvDistribusiBHPL').setValue(rowdata.tgl_spk);
	
	
	Ext.getCmp('dfTgl_InvDistribusiBHPL').setReadOnly(true);
	Ext.getCmp('cbJnsLokasiInvDistribusiBHP').setReadOnly(true);
	Ext.getCmp('cbJnsLokasiInvDistribusiBHP').enable();
	Ext.getCmp('cbJnsLokasiInvDistribusiBHP').setReadOnly(true);
	Ext.getCmp('txtKeterangan_InvDistribusiBHPL').setReadOnly(true);
	Ext.getCmp('txtNoSPK_InvDistribusiBHPL').setReadOnly(true);
	Ext.getCmp('dfTglSPK_InvDistribusiBHPL').setReadOnly(true);
	
	if(rowdata.update_stok == 't'){
		Ext.getCmp('cbStokUpdate_InvDistribusiBHP').setValue(true);
		Ext.getCmp('btnSimpan_viInvDistribusiBHP').disable();
		Ext.getCmp('btnSimpanExit_viInvDistribusiBHP').disable();
		Ext.getCmp('btnHapusDistribusi_InvDistribusiBHP').disable();
		Ext.getCmp('btnPosting_viInvDistribusiBHP').disable();
		Ext.getCmp('btnUnPosting_viInvDistribusiBHP').enable();
		Ext.getCmp('btnAddDistribusiBarang').enable();
		Ext.getCmp('btnHapusGridBarang_InvDistribusiBHP').disable();
	} else{
		Ext.getCmp('cbStokUpdate_InvDistribusiBHP').setValue(false);
		Ext.getCmp('btnSimpan_viInvDistribusiBHP').enable();
		Ext.getCmp('btnSimpanExit_viInvDistribusiBHP').enable();
		Ext.getCmp('btnHapusDistribusi_InvDistribusiBHP').enable();
		Ext.getCmp('btnPosting_viInvDistribusiBHP').enable();
		Ext.getCmp('btnUnPosting_viInvDistribusiBHP').disable();
		Ext.getCmp('btnAddDistribusiBarang').enable();
		Ext.getCmp('btnHapusGridBarang_InvDistribusiBHP').enable();
	}
	if(rowdata.no_permintaan!=''){ //disini ya
		console.log('123');
		Ext.getCmp('btnAdd_viInvDistribusiBHP').disable();
		Ext.getCmp('txt_tmp_kd_lokasi_permintaan').setValue(rowdata.kd_bagian);
		Ext.getCmp('dfTgl_InvDistribusiBHPL').setValue(now_viInvDistribusiBHP);
		Ext.getCmp('btnPrint_viInvDistribusiBHP').enable();
	}else{
		getGridBaranglInvDistribusiBHP(rowdata.no_keluar);
	}
	//cekTerimaBahanInvDistribusiBHP(rowdata.no_minta);
};

function datasave_viInvDistribusiBHP(){
	if (ValidasiEntryInvDistribusiBHP(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/bhp/functionDistribusiBHP/save",
				params: getParamInvDistribusiBHP(),
				failure: function(o)
				{
					ShowPesanErrorInvDistribusiBHP('Error, simpan data! Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) {
						ShowPesanInfoInvDistribusiBHP('Data berhasil disimpan','Information');
						Ext.getCmp('txtNoKeluar_InvDistribusiBHPL').setValue(cst.nokeluar);
						getDataGridAwalInvDistribusiBHP();
						Ext.getCmp('btnSimpan_viInvDistribusiBHP').disable();
						Ext.getCmp('btnSimpanExit_viInvDistribusiBHP').disable();
						Ext.getCmp('btnHapusDistribusi_InvDistribusiBHP').enable();
						Ext.getCmp('btnPosting_viInvDistribusiBHP').enable();
						Ext.getCmp('btnUnPosting_viInvDistribusiBHP').disable();
						Ext.getCmp('btnHapusGridBarang_InvDistribusiBHP').disable();
						Ext.Ajax.request({
								url: baseURL + "index.php/bhp/functionDistribusiBHP/get_list_permintaan",
								params: {
									lokasi 		 :tmp_kd_lokasi_permintaan,
									tgl_awal 	 :Ext.getCmp('dfTglAwalInvDistribusiBHP_Permintaan').getValue(),
									tgl_akhir 	 :Ext.getCmp('dfTglAkhirInvDistribusiBHP_Permintaan').getValue()
								},
								failure: function(o){
									loadMask.hide();
									ShowPesanErrorInvDistribusiBHP('Hubungi Admin', 'Error');
								},	
								success: function(o) {
									var cst = Ext.decode(o.responseText);
										if (cst.success === true) {
											dataSource_viInvDistribusiBHP_permintaan.removeAll();
											var recs=[],
												recType=dataSource_viInvDistribusiBHP_permintaan.recordType;
											for(var i=0; i<cst.ListDataObj.length; i++){
												recs.push(new recType(cst.ListDataObj[i]));
												}
												dataSource_viInvDistribusiBHP_permintaan.add(recs);
											
											GridDataView_viInvDistribusiBHP_Permintaan.getView().refresh();
										}
										else{
											ShowPesanErrorInvDistribusiBHP('Gagal membaca data Permintaab BHP', 'Error');
										};
								}
							})
					}
					else 
					{
						ShowPesanErrorInvDistribusiBHP('Gagal Menyimpan Data ini', 'Error');
					};
				}
			}
			
		)
	}
}

function dataposting_viInvDistribusiBHP(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/bhp/functionDistribusiBHP/updatePosting",
			params: getParamPostingiInvDistribusiBHP(),
			failure: function(o)
			{
				ShowPesanErroriInvDistribusiBHP('Error, posting! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					ShowPesanInfoInvDistribusiBHP('Posting berhasil', 'Information');
					Ext.getCmp('cbStokUpdate_InvDistribusiBHP').setValue(true);
					Ext.getCmp('btnSimpan_viInvDistribusiBHP').disable();
					Ext.getCmp('btnSimpanExit_viInvDistribusiBHP').disable();
					Ext.getCmp('btnHapusDistribusi_InvDistribusiBHP').disable();
					Ext.getCmp('btnPosting_viInvDistribusiBHP').disable();
					Ext.getCmp('btnUnPosting_viInvDistribusiBHP').enable();
					Ext.getCmp('btnAddDistribusiBarang').disable();
					Ext.getCmp('btnHapusGridBarang_InvDistribusiBHP').disable();
					getDataGridAwalInvDistribusiBHP();
				}
				else 
				{
					ShowPesanWarningInvDistribusiBHP('Gagal diPosting', 'Warning');
					Ext.getCmp('cbStokUpdate_InvDistribusiBHP').setValue(false);
					Ext.getCmp('btnSimpan_viInvDistribusiBHP').enable();
					Ext.getCmp('btnSimpanExit_viInvDistribusiBHP').enable();
					Ext.getCmp('btnHapusDistribusi_InvDistribusiBHP').enable();
					Ext.getCmp('btnPosting_viInvDistribusiBHP').enable();
					Ext.getCmp('btnUnPosting_viInvDistribusiBHP').disable();
					Ext.getCmp('btnAddDistribusiBarang').enable();
					Ext.getCmp('btnHapusGridBarang_InvDistribusiBHP').enable();
				};
			}
		}
		
	)
}

function dataUnposting_viInvDistribusiBHP(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/bhp/functionDistribusiBHP/updateUnposting",
			params: getParamPostingiInvDistribusiBHP(),
			failure: function(o)
			{
				ShowPesanErroriInvDistribusiBHP('Error, posting! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					ShowPesanInfoInvDistribusiBHP('Posting berhasil', 'Information');
					Ext.getCmp('cbStokUpdate_InvDistribusiBHP').setValue(false);
					Ext.getCmp('btnSimpan_viInvDistribusiBHP').enable();
					Ext.getCmp('btnSimpanExit_viInvDistribusiBHP').enable();
					Ext.getCmp('btnHapusDistribusi_InvDistribusiBHP').enable();
					Ext.getCmp('btnPosting_viInvDistribusiBHP').enable();
					Ext.getCmp('btnUnPosting_viInvDistribusiBHP').disable();
					Ext.getCmp('btnAddDistribusiBarang').enable();
					Ext.getCmp('btnHapusGridBarang_InvDistribusiBHP').enable();
					getDataGridAwalInvDistribusiBHP();
				}
				else 
				{
					ShowPesanWarningInvDistribusiBHP('Gagal diPosting', 'Warning');
					Ext.getCmp('cbStokUpdate_InvDistribusiBHP').setValue(true);
					Ext.getCmp('btnSimpan_viInvDistribusiBHP').disable();
					Ext.getCmp('btnSimpanExit_viInvDistribusiBHP').disable();
					Ext.getCmp('btnHapusDistribusi_InvDistribusiBHP').disable();
					Ext.getCmp('btnPosting_viInvDistribusiBHP').disable();
					Ext.getCmp('btnUnPosting_viInvDistribusiBHP').enable();
					Ext.getCmp('btnAddDistribusiBarang').disable();
					Ext.getCmp('btnHapusGridBarang_InvDistribusiBHP').disable();
				};
			}
		}
		
	)
}

function getDataGridAwalInvDistribusiBHP(nokeluar,tglawal,tglakhir){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/bhp/functionDistribusiBHP/getGridAwal",
			params: {
				nokeluar:nokeluar,
				tglawal:tglawal,
				tglakhir:tglakhir
			},
			failure: function(o)
			{
				ShowPesanErrorInvDistribusiBHP('Error, membaca grid awal! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					dataSource_viInvDistribusiBHP.removeAll();
					
					var recs=[],
						recType=dataSource_viInvDistribusiBHP.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
						dataSource_viInvDistribusiBHP.add(recs);
					
					GridDataView_viInvDistribusiBHP.getView().refresh();
				}
				else 
				{
					ShowPesanErrorInvDistribusiBHP('Gagal membaca data obat', 'Error');
				};
			}
		}
		
	)
	
}


function getGridBaranglInvDistribusiBHP(no_keluar){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/bhp/functionDistribusiBHP/getGridBarangLoad",
			params: {no_keluar:no_keluar},
			failure: function(o)
			{
				ShowPesanErrorInvDistribusiBHP('Error, membaca grid barang! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dsDataGrdBarang_viInvDistribusiBHP.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
						dsDataGrdBarang_viInvDistribusiBHP.add(recs);
					
					 InvDistribusiBHP.form.Grid.a.getView().refresh();
				}
				else 
				{
					ShowPesanErrorInvDistribusiBHP('Gagal membaca data barang', 'Error');
				};
			}
		}
		
	)
	
}

function cekTerimaBahanInvDistribusiBHP(no_minta){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionPermintaanBahan/cekTerimaBahanInvDistribusiBHP",
			params: {no_minta:no_minta},
			failure: function(o)
			{
				ShowPesanErrorInvDistribusiBHP('Error, cek! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					Ext.getCmp('btnAddDistribusiBarang').enable();
					Ext.getCmp('btnHapusGridBarang_InvDistribusiBHP').enable();
					Ext.getCmp('btnSimpan_viInvDistribusiBHP').enable();
					Ext.getCmp('btnSimpanExit_viInvDistribusiBHP').enable();
					Ext.getCmp('btnHapusDistribusi_InvDistribusiBHP').enable();
				}
				else 
				{
					ShowPesanInfoInvDistribusiBHP('Bahan-bahan dari permintaan ini sudah diterima', 'Information');
					Ext.getCmp('btnSimpan_viInvDistribusiBHP').disable(true);
					Ext.getCmp('btnSimpanExit_viInvDistribusiBHP').disable(true);
					Ext.getCmp('btnHapusDistribusi_InvDistribusiBHP').disable(true);
					Ext.getCmp('btnAddDistribusiBarang').enable();
					Ext.getCmp('btnHapusGridBarang_InvDistribusiBHP').disable(true);
				};
			}
		}
		
	)
}

function getDataGridBarangInvDistribusiBHP(nama_brg){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/bhp/functionDistribusiBHP/getGridBarang",
			params: {nama_brg:nama_brg},
			failure: function(o)
			{
				ShowPesanErrorInvDistribusiBHP('Error, membaca grid awal! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					dsDataListGrdBarang_viInvDistribusiBHP.removeAll();
					
					var recs=[],
						recType=dsDataListGrdBarang_viInvDistribusiBHP.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
						dsDataListGrdBarang_viInvDistribusiBHP.add(recs);
					
					gridPanelLookUpListBarang_InvDistribusiBHP.getView().refresh();
				}
				else 
				{
					ShowPesanErrorInvDistribusiBHP('Gagal membaca data obat', 'Error');
				};
			}
		}
		
	)
}

function getDataGridStokBarang(nama_brg,tglawal,tglakhir){
	console.log(nama_brg);
	console.log(tglawal);
	console.log(tglakhir);
	if(nama_brg==undefined || tglawal==undefined || tglakhir==undefined){
		nama_brg=InvDistribusiBHP.vars.nama_brg;
		tglawal=InvDistribusiBHP.vars.tglawal;
		tglakhir=InvDistribusiBHP.vars.tglakhir;
	}
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/bhp/functionDistribusiBHP/getGridStokBarang",
			params: {
				nama_brg:nama_brg,
				tglawal:tglawal,
				tglakhir:tglakhir
			},
			failure: function(o)
			{
				ShowPesanErrorInvDistribusiBHP('Error, membaca grid awal! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					dsDataGrdDetailBarang_viInvDistribusiBHP.removeAll();
					
					var recs=[],
						recType=dsDataGrdDetailBarang_viInvDistribusiBHP.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
						dsDataGrdDetailBarang_viInvDistribusiBHP.add(recs);
					
					gridPanelLookUpBarang_InvDistribusiBHP.getView().refresh();
				}
				else 
				{
					ShowPesanErrorInvDistribusiBHP('Gagal membaca data obat', 'Error');
				};
			}
		}
		
	)
}


function getParamPostingiInvDistribusiBHP() 
{
	var	params =
	{
		NoKeluar:Ext.getCmp('txtNoKeluar_InvDistribusiBHPL').getValue(),
	}
	
	params['jumlah']=dsDataGrdBarang_viInvDistribusiBHP.getCount();
	for(var i = 0 ; i < dsDataGrdBarang_viInvDistribusiBHP.getCount();i++)
	{
		params['kd_stok-'+i]=dsDataGrdBarang_viInvDistribusiBHP.data.items[i].data.kd_stok
		params['jumlah_total-'+i]=dsDataGrdBarang_viInvDistribusiBHP.data.items[i].data.jumlah_total
	}
    return params
};

function getParamInvDistribusiBHP() {
	var tmp_kd_lokasi='';
	var kd_lokasi=Ext.getCmp('cbJnsLokasiInvDistribusiBHP').getValue();
	if(kd_lokasi.length > 4){
		tmp_kd_lokasi=Ext.getCmp('txt_tmp_kd_lokasi_permintaan').getValue();
	}else{
		tmp_kd_lokasi=Ext.getCmp('cbJnsLokasiInvDistribusiBHP').getValue();
	}
	var	params =
	{
		NoKeluar:Ext.getCmp('txtNoKeluar_InvDistribusiBHPL').getValue(),
		no_permintaan:tmp_no_permintaan,
		KdLokasi:tmp_kd_lokasi,
		Tanggal:Ext.getCmp('dfTgl_InvDistribusiBHPL').getValue(),
		Keterangan:Ext.getCmp('txtKeterangan_InvDistribusiBHPL').getValue(),
		NoSpk:Ext.getCmp('txtNoSPK_InvDistribusiBHPL').getValue(),
		TglSpk:Ext.getCmp('dfTglSPK_InvDistribusiBHPL').getValue()
	}
	
	params['jumlah']=dsDataGrdBarang_viInvDistribusiBHP.getCount();
	for(var i = 0 ; i < dsDataGrdBarang_viInvDistribusiBHP.getCount();i++)
	{
		params['kd_stok-'+i]=dsDataGrdBarang_viInvDistribusiBHP.data.items[i].data.kd_stok
		params['keterangan-'+i]=dsDataGrdBarang_viInvDistribusiBHP.data.items[i].data.keterangan
		params['jumlah_total-'+i]=dsDataGrdBarang_viInvDistribusiBHP.data.items[i].data.jumlah_total
		params['no_baris-'+i]=dsDataGrdBarang_viInvDistribusiBHP.data.items[i].data.no_baris
		
	}
    return params
};




function ValidasiEntryInvDistribusiBHP(modul,mBolHapus)
{
	var x = 1;
	if(Ext.getCmp('cbJnsLokasiInvDistribusiBHP').getValue() === ''){
		ShowPesanWarningInvDistribusiBHP('Jenis lokasi tidak boleh kosong', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('cbJnsLokasiInvDistribusiBHP').getValue() === ''){
		ShowPesanWarningInvDistribusiBHP('Lokasi tidak boleh kosong', 'Warning');
		x = 0;
	}
	if(dsDataGrdBarang_viInvDistribusiBHP.getCount() < 0){
		ShowPesanWarningInvDistribusiBHP('Daftar distribusi tidak boleh kosong, minimal 1 barang', 'Warning');
		x = 0;
	}
	
	for(var i=0; i<dsDataGrdBarang_viInvDistribusiBHP.getCount() ; i++){
		var o=dsDataGrdBarang_viInvDistribusiBHP.getRange()[i].data;
		if(o.jumlah_total == undefined || o.jumlah_total <= 0){
			ShowPesanWarningInvDistribusiBHP('Jumlah tidak boleh 0, minimal jumlah barang 1', 'Warning');
			x = 0;
		}
		/* for(var j=i+1; j<dsDataGrdBarang_viInvDistribusiBHP.getCount() ; j++){
			var p=dsDataGrdBarang_viInvDistribusiBHP.getRange()[j].data;
			if(o.kd_stok == p.kd_stok){
				ShowPesanWarningInvDistribusiBHP('Barang tidak boleh sama, periksa kembali daftar permintaan bahan', 'Warning');
				x = 0;
				break;
			}
		} */
	}
	
	return x;
};



function ShowPesanWarningInvDistribusiBHP(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorInvDistribusiBHP(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};


function ShowPesanInfoInvDistribusiBHP(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};

function loadDataComboLokasi(){
	Ext.Ajax.request({
	url: baseURL + "index.php/bhp/function_lokasi/get_combo_lokasi",
		params: '0',
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType =  ds_JnsLokasiInvDistribusiBHP.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				ds_JnsLokasiInvDistribusiBHP.add(recs);
			}
				console.log(ds_JnsLokasiInvDistribusiBHP);
		}
	});
}

function loadDataComboLokasi_Permintaan(){
	Ext.Ajax.request({
	url: baseURL + "index.php/bhp/function_lokasi/get_combo_lokasi",
		params: '0',
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType =  ds_JnsLokasiInvDistribusiBHP_Permintaan.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				ds_JnsLokasiInvDistribusiBHP_Permintaan.add(recs);
			}
				console.log(ds_JnsLokasiInvDistribusiBHP_Permintaan);
		}
	});
}

function get_barang(){
	Ext.Ajax.request({
			url: baseURL + "index.php/bhp/functionDistribusiBHP/getGridStokBarang",
			params: {
				nama_brg:InvDistribusiBHP.vars.nama,
				tglawal:Ext.getCmp('dfGridTglPosting_InvDistribusiBHPL').getValue(),
				tglakhir:Ext.getCmp('dfGridTglPosting_InvDistribusiBHPL').getValue()
			},
			failure: function(o){
				ShowPesanErrorInvDistribusiBHP('Error, membaca grid awal! Hubungi Admin', 'Error');
			},	
			success: function(o) {
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) {
					dsDataGrdDetailBarang_viInvDistribusiBHP.removeAll();
					var recs=[],
						recType=dsDataGrdDetailBarang_viInvDistribusiBHP.recordType;
					for(var i=0; i<cst.ListDataObj.length; i++){
						recs.push(new recType(cst.ListDataObj[i]));
						}
						dsDataGrdDetailBarang_viInvDistribusiBHP.add(recs);
					
					gridPanelLookUpBarang_InvDistribusiBHP.getView().refresh();
				}
				else{
					ShowPesanErrorInvDistribusiBHP('Gagal membaca data BHP', 'Error');
				};
			}
		}
		
	)
}

function comboJenisLokasiInvDistribusiBHP_bhp_Permintaan(){ //disini
	var Field_Vendor = ['kd_lokasi', 'lokasi'];
    ds_JnsLokasiInvDistribusiBHP_Permintaan = new WebApp.DataStore({fields: Field_Vendor});
	var cboJnsLokasiInvDistribusiBHP_Permintaan = new Ext.form.ComboBox({
        id:'cbJnsLokasiInvDistribusiBHP_Permintaan',
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:true,
        mode: 'local',
        width: 130,
		readOnly:false,
		fieldLabel:'PBF ',
        store: ds_JnsLokasiInvDistribusiBHP_Permintaan,
        valueField: 'kd_lokasi',
        displayField: 'lokasi',
        listeners:{
			'select': function(a,b,c){
				console.log(b.data.kd_lokasi);
				tmp_kd_lokasi_permintaan=b.data.kd_lokasi;
			}
        }
	});
	return cboJnsLokasiInvDistribusiBHP_Permintaan;
};

function loadDetailPermintaanBHP(no_permintaan){
	Ext.Ajax.request({
			url: baseURL + "index.php/bhp/functionDistribusiBHP/getDetailPermintaanBHPUnit",
			params: {
				no_permintaan:no_permintaan
			},
			failure: function(o){
				ShowPesanErrorInvDistribusiBHP('Error, membaca grid awal! Hubungi Admin', 'Error');
			},	
			success: function(o) {
				dsDataGrdBarang_viInvDistribusiBHP.removeAll();
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) {
					var recs=[],
						recType=dsDataGrdBarang_viInvDistribusiBHP.recordType;
					for(var i=0; i<cst.ListDataObj.length; i++){
						recs.push(new recType(cst.ListDataObj[i]));
						}
						dsDataGrdBarang_viInvDistribusiBHP.add(recs);
					
					InvDistribusiBHP.form.Grid.a.getView().refresh();
				}
				else{
					ShowPesanErrorInvDistribusiBHP('Gagal membaca data BHP', 'Error');
				};
			}
		})

}