var dataSource_viInvKelompokBarang;
var selectCount_viInvKelompokBarang=50;
var NamaForm_viInvKelompokBarang="Setup Kelompok Barang";
var mod_name_viInvKelompokBarang="Setup Kelompok Barang";
var now_viInvKelompokBarang= new Date();
var rowSelected_viInvKelompokBarang;
var setLookUps_viInvKelompokBarang;
var tanggal = now_viInvKelompokBarang.format("d/M/Y");
var jam = now_viInvKelompokBarang.format("H/i/s");
var tmpkriteria;
var DataGridJenisObat;
var GridDataView_viInvKelompokBarang;
var IdKlas_produkegorySetEquipmentKlas_produkView='1000000000';
var dsLookProdukList;
var treeKlas_kelompokbarang_viInvKelompokBarang;

var CurrentData_viInvKelompokBarang =
{
	data: Object,
	details: Array,
	row: 0
};

var InvKelompokBarang={};
InvKelompokBarang.form={};
InvKelompokBarang.func={};
InvKelompokBarang.vars={};
InvKelompokBarang.func.parent=InvKelompokBarang;
InvKelompokBarang.form.ArrayStore={};
InvKelompokBarang.form.ComboBox={};
InvKelompokBarang.form.DataStore={};
InvKelompokBarang.form.Record={};
InvKelompokBarang.form.Form={};
InvKelompokBarang.form.Grid={};
InvKelompokBarang.form.Panel={};
InvKelompokBarang.form.TextField={};
InvKelompokBarang.form.Button={};

InvKelompokBarang.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_unit_far', 'nm_unit_far', 'nomor_ro_unit', 'nomorawal','nomor_out_milik'],
	data: []
});

CurrentPage.page = dataGrid_viInvKelompokBarang(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dataGrid_viInvKelompokBarang(mod_id_viInvKelompokBarang){	 
	// Field kiriman dari Project Net.
    var FieldMaster_viInvKelompokBarang = 
	[
		'kd_unit_far', 'nm_unit_far', 'nomor_ro_unit', 'nomorawal','nomor_out_milik' 
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viInvKelompokBarang = new WebApp.DataStore
	({
        fields: FieldMaster_viInvKelompokBarang
    });
   	GetStrTreeSetKelompokBarang();
    // Grid Apotek Perencanaan # --------------
	GridDataView_viInvKelompokBarang = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viInvKelompokBarang,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:true,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viInvKelompokBarang = undefined;
							rowSelected_viInvKelompokBarang = dataSource_viInvKelompokBarang.getAt(row);
							CurrentData_viInvKelompokBarang
							CurrentData_viInvKelompokBarang.row = row;
							CurrentData_viInvKelompokBarang.data = rowSelected_viInvKelompokBarang.data;
							InvKelompokBarang.vars.kode_inv=rowSelected_viInvKelompokBarang.data.kd_inv;
							console.log(InvKelompokBarang.vars.kode_inv);
							//setLookUp_viInvKelompokBarang(rowSelected_viInvKelompokBarang.data);
							//DataInitInvKelompokBarang(rowSelected_viInvKelompokBarang.data);
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viInvKelompokBarang = dataSource_viInvKelompokBarang.getAt(ridx);
					if (rowSelected_viInvKelompokBarang != undefined)
					{
						//DataInitInvKelompokBarang(rowSelected_viInvKelompokBarang.data);
						setLookUp_viInvKelompokBarang(rowSelected_viInvKelompokBarang.data);
					}
					else
					{
						//setLookUp_viInvKelompokBarang();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Setup Unit
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header: 'inv_kd_inv',
						dataIndex: 'inv_kd_inv',
						hideable:false,
						menuDisabled: true,
						width: 40,
						hidden:true
						
					},
					//-------------- ## --------------
					{
						header: 'Kode',
						dataIndex: 'kd_inv',
						hideable:false,
						menuDisabled: true,
						width: 40
					},
					{
						header: 'Bidang',
						dataIndex: 'nama_sub',
						hideable:false,
						menuDisabled: true,
						width: 90
					},
					//-------------- ## --------------
					
					
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viInvKelompokBarang',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'Edit_Tr',
						tooltip: 'Add Data',
						id: 'btnAdd_viInvKelompokBarang',
						handler: function(sm, row, rec)
						{
							//alert(InvKelompokBarang.vars.inv_kd_inv);
							setLookUp_viInvKelompokBarang();
							Ext.getCmp('txtKdInv_InvKelompokBarang').setReadOnly(false);
						}
					},
					{
						xtype: 'button',
						text: 'Edit',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viInvKelompokBarang',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viInvKelompokBarang != undefined)
							{
								//DataInitInvKelompokBarang(rowSelected_viInvKelompokBarang.data);
								setLookUp_viInvKelompokBarang(rowSelected_viInvKelompokBarang.data);
							}
							
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viInvKelompokBarang, selectCount_viInvKelompokBarang, dataSource_viInvKelompokBarang),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)	
	
	// Kriteria filter pada Grid # --------------
    var FrmData_viInvKelompokBarang = new Ext.Panel
    (
		{
			title: NamaForm_viInvKelompokBarang,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viInvKelompokBarang,
			//region: 'center',
			layout: {
				type:'hbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ itemsTreeListKelompokBarangGridDataView_viInvKelompokBarang(),
					GridDataView_viInvKelompokBarang],
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viInvKelompokBarang',
						handler: function()
						{
							Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
								if (button == 'yes'){
									loadMask.show();
									dataDelete_viInvKelompokBarang();
								}
							});
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_viSetupPabrik',
						handler: function()
						{
							dataSource_viInvKelompokBarang.removeAll();
							dataGriInvKelompokBarang(InvKelompokBarang.vars.inv_kd_inv);
						}
					},
					{
						xtype: 'tbseparator'
					}
					
				]
			},
			//GetStrTreeSetKelompokBarang();
			//-------------- # End tbar # --------------
       }
    )
	
    return FrmData_viInvKelompokBarang;
	//loadMask.show();
	
    //-------------- # End form filter # --------------
}
//------------------end---------------------------------------------------------

function setLookUp_viInvKelompokBarang(rowdata){
    var lebar = 490;
    setLookUps_viInvKelompokBarang = new Ext.Window({
        id: 'LookUps_viInvKelompokBarang',
        title: 'Edit Kelompok Barang', 
        closeAction: 'destroy',        
        width: 500,
        height: 110,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viInvKelompokBarang(lebar,rowdata),
        listeners:{
            activate: function(){
				
            },
            afterShow: function(){
                this.activate();
            },
            deactivate: function(){
                rowSelected_viInvKelompokBarang=undefined;
            }
        }
    });

    setLookUps_viInvKelompokBarang.show();
	
    if (rowdata == undefined){	
    }
    else
    {
       datainit_viInvKelompokBarang(rowdata);
    }
}

function getFormItemEntry_viInvKelompokBarang(lebar,rowdata){
    var pnlFormDataBasic_viInvKelompokBarang = new Ext.FormPanel({
		title: '',
		region: 'north',
		layout: 'form',
		bodyStyle: 'padding:10px 10px 10px 10px',
		anchor: '100%',
		labelWidth: 1,
		autoWidth: true,
		width: lebar,
		border: false,
		items:[
				{
					title:'',
					layout:'column',
					border: true,
					height: 35,
					items:
					[
						{
							layout: 'absolute',
							bodyStyle: 'padding: 10px 10px 10px 10px',
							border: false,
							width: lebar-22,
							height: 30,
							anchor: '100% 100%',
							items:
							[
								{
									x:10,
									y:5,
									xtype: 'label',
									text:'Kelompok'
								},
								{
									x:80,
									y:5,
									xtype: 'label',
									text:':'
								},
								{
									x:90,
									y:5,
									xtype: 'textfield',
									name: 'txtKdInv_InvKelompokBarang',
									id: 'txtKdInv_InvKelompokBarang',
									readOnly:true,
									width: 90
								},
								{
									x:185,
									y:5,
									xtype: 'textfield',
									name: 'txtNama_InvKelompokBarang',
									id: 'txtNama_InvKelompokBarang',
									width: 270
								},
							]
						}
					]
				}
			],
			fileUpload: true,
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'tbseparator'
					},{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						disabled:false,
						id: 'btnSimpan_viInvKelompokBarang',
						handler: function()
						{
							dataSave_viInvKelompokBarang();
						}
					},{
						xtype: 'tbseparator'
					},{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						disabled:false,
						id: 'btnSimpanExit_viInvKelompokBarang',
						handler: function()
						{
							dataSave_viInvKelompokBarang();
							setLookUps_viInvKelompokBarang.close();
						}
					},{
						xtype: 'tbseparator'
					}
					
				]
			}//,items:
		}
    )

    return pnlFormDataBasic_viInvKelompokBarang;
}
//-----------------------------------------------------------------------------------------------------------------------------------

function GetStrTreeSetKelompokBarang()
{
	 Ext.Ajax.request
	 (
		{
			url: WebAppUrl.UrlExecProcess,
			params: 
			{
				UserID: strUser, // 'Admin',
				ModuleID: 'ProsesGetKelompokBarang',
				Params:	" and kd_inv<>''"
			},
			success : function(resp) 
			{
				loadMask.hide();
				var cst = Ext.decode(resp.responseText);
				StrTreeSetKelompokBarang= cst.arr;
			},
			failure:function()
			{
			    loadMask.hide();
			}
		}
	);
};


function itemsTreeListKelompokBarangGridDataView_viInvKelompokBarang()
{	
		
	treeKlas_kelompokbarang_viInvKelompokBarang= new Ext.tree.TreePanel
	(
		{
			autoScroll: true,
			split: true,
			height:460,
			width:230,
			loader: new Ext.tree.TreeLoader(),
			listeners: 
			{
				click: function(n) 
				{
					strTreeCriteriKelompokBarang_viKelompokBarang=n.attributes
					rowSelectedTreeSetEquipmentKlas_produk=n.attributes;
					if (strTreeCriteriKelompokBarang_viKelompokBarang.id != ' 0')
					{
						if (strTreeCriteriKelompokBarang_viKelompokBarang.leaf == false)
						{
							/* var str='';
							str='substring(kd_klas,1, ' + n.attributes.id.length + ') = ~' + n
							
							.attributes.id + '~';
							kdperent=n.attributes.id;
							panjangklas=n.attributes.id.length;
							 */
						}
						else
						{
							console.log(n.attributes.id);
							dataGriInvKelompokBarang(n.attributes.id);
							InvKelompokBarang.vars.inv_kd_inv=n.attributes.id;
							
							if(n.attributes.id=='1000000000'){
								GetStrTreeSetKelompokBarang();
								rootTreeSetEquipmentKlas_produkView = new Ext.tree.AsyncTreeNode
								(
									{
										expanded: true,
										text:'MASTER',
										id:IdKlas_produkegorySetEquipmentKlas_produkView,
										children: StrTreeSetKelompokBarang,
										autoScroll: true
									}
								) 
								treeKlas_kelompokbarang_viInvKelompokBarang.setRootNode(rootTreeSetEquipmentKlas_produkView);
							} 
							
						};
					}
					else
					{
						
					};
				},
				
				
			}
		}
	);
	
	rootTreeSetEquipmentKlas_produkView = new Ext.tree.AsyncTreeNode
	(
		{
			expanded: false,
			text:'MASTER',
			id:IdKlas_produkegorySetEquipmentKlas_produkView,
			children: StrTreeSetKelompokBarang,
			autoScroll: true
		}
	)  
  
	treeKlas_kelompokbarang_viInvKelompokBarang.setRootNode(rootTreeSetEquipmentKlas_produkView);  
	
    var pnlTreeFormDataWindowPopup_viInvKelompokBarang = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                
				//-------------- ## -------------- 
                treeKlas_kelompokbarang_viInvKelompokBarang,
                //-------------- ## -------------- 
            ],
            //-------------- #End items# --------------
        }
    )

    return pnlTreeFormDataWindowPopup_viInvKelompokBarang;
}

//===================================================================================================================================================

function dataGriInvKelompokBarang(inv_kd_inv){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/bhp/functionSetupKelompokBarang/getKelompokBarangGrid",
			params: {id:inv_kd_inv},
			failure: function(o)
			{
				ShowPesanErrorInvKelompokBarang('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dataSource_viInvKelompokBarang.removeAll();
					var recs=[],
						recType=dataSource_viInvKelompokBarang.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dataSource_viInvKelompokBarang.add(recs);
					
					
					
					GridDataView_viInvKelompokBarang.getView().refresh();
				}
				else 
				{
					ShowPesanErrorInvKelompokBarang('Gagal membaca data obat', 'Error');
				};
			}
		}
		
	)
}

function dataSave_viInvKelompokBarang(){
	if (ValidasiSaveInvKelompokBarang(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/bhp/functionKelompokBarang/save",
				params: getParamSaveInvKelompokBarang(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorInvKelompokBarang('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoInvKelompokBarang('Berhasil menyimpan data ini','Information');
						dataGriInvKelompokBarang(InvKelompokBarang.vars.inv_kd_inv);
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorInvKelompokBarang('Gagal menyimpan data ini', 'Error');
					};
				}
			}
			
		)
	}
}

function dataDelete_viInvKelompokBarang(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/bhp/functionKelompokBarang/delete",
			params: getParamDeleteInvKelompokBarang(),
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorInvKelompokBarang('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					loadMask.hide();
					ShowPesanInfoInvKelompokBarang('Berhasil menghapus data ini','Information');
					dataGriInvKelompokBarang(InvKelompokBarang.vars.inv_kd_inv);
				}
				else 
				{
					loadMask.hide();
					ShowPesanErrorInvKelompokBarang('Gagal menghapus data ini', 'Error');
				};
			}
		}
		
	)
}



function datainit_viInvKelompokBarang(rowdata){
	Ext.getCmp('txtKdInv_InvKelompokBarang').setValue(rowdata.kd_inv);
	Ext.getCmp('txtNama_InvKelompokBarang').setValue(rowdata.nama_sub);
	InvKelompokBarang.vars.inv_kd_inv=rowdata.inv_kd_inv;
};

function getParamSaveInvKelompokBarang(){
	var	params =
	{
		KdInv:Ext.getCmp('txtKdInv_InvKelompokBarang').getValue(),
		NamaSub:Ext.getCmp('txtNama_InvKelompokBarang').getValue(),
		InvKdInv:InvKelompokBarang.vars.inv_kd_inv
	}
   
    return params
};

function getParamDeleteInvKelompokBarang(){
	var	params =
	{
		KdInv:InvKelompokBarang.vars.kode_inv,
		InvKdInv:InvKelompokBarang.vars.inv_kd_inv
	}
   
    return params
};

function ValidasiSaveInvKelompokBarang(modul,mBolHapus){
	var x = 1;
	if(Ext.getCmp('txtKdInv_InvKelompokBarang').getValue() === '' || Ext.getCmp('txtNama_InvKelompokBarang').getValue() ===''){
		if(Ext.getCmp('txtKdInv_InvKelompokBarang').getValue() === ''){
			loadMask.hide();
			ShowPesanWarningInvKelompokBarang('Kode kelompok tidak boleh kosong', 'Warning');
			x = 0;
		} else{
			loadMask.hide();
			ShowPesanWarningInvKelompokBarang('Kelompok barang masih kosong', 'Warning');
			x = 0;
		}
		
	} 
	return x;
};



function ShowPesanWarningInvKelompokBarang(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorInvKelompokBarang(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoInvKelompokBarang(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};