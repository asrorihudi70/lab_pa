var dataSource_viInvMasterBarang;
var selectCount_viInvMasterBarang=50;
var NamaForm_viInvMasterBarang="Barang Persediaan";
var mod_name_viInvMasterBarang="Barang Persediaan";
var now_viInvMasterBarang= new Date();
var rowSelected_viInvMasterBarang;
var setLookUps_viInvMasterBarang;
var tanggal = now_viInvMasterBarang.format("d/M/Y");
var jam = now_viInvMasterBarang.format("H/i/s");
var tmpkriteria;
var DataGridJenisObat;
var GridDataView_viInvMasterBarang;
var rowLookUpSelected_viInvMasterBarang;
var lookUp_viInvMasterBarang;
var gridPanelLookUpBarang_InvMasterBarang;
var IdRootKelompokBarang_pInvMasterBarang='1000000000';
var treeKlas_kelompokbarang_viInvMasterBarang;


var CurrentData_viInvMasterBarang =
{
	data: Object,
	details: Array,
	row: 0
};

var InvMasterBarang={};
InvMasterBarang.form={};
InvMasterBarang.func={};
InvMasterBarang.vars={};
InvMasterBarang.func.parent=InvMasterBarang;
InvMasterBarang.form.ArrayStore={};
InvMasterBarang.form.ComboBox={};
InvMasterBarang.form.DataStore={};
InvMasterBarang.form.Record={};
InvMasterBarang.form.Form={};
InvMasterBarang.form.Grid={};
InvMasterBarang.form.Panel={};
InvMasterBarang.form.TextField={};
InvMasterBarang.form.Button={};

InvMasterBarang.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_vendor', 'vendor', 'contact', 'alamat', 'kota', 'telepon1', 'telepon2',
			'fax', 'kd_pos', 'negara', 'norek', 'term'],
	data: []
});

CurrentPage.page = dataGrid_viInvMasterBarang(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dataGrid_viInvMasterBarang(mod_id_viInvMasterBarang){	
 	// Field kiriman dari Project Net.
    var FieldMaster_viInvMasterBarang = 
	[
		'kd_vendor', 'vendor', 'contact', 'alamat', 'kota', 'telepon1', 'telepon2',
		'fax', 'kd_pos', 'negara', 'norek', 'term'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viInvMasterBarang = new WebApp.DataStore
	({
        fields: FieldMaster_viInvMasterBarang
    });
    dataGriInvMasterBarang();
    // Grid Apotek Perencanaan # --------------
	GridDataView_viInvMasterBarang = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viInvMasterBarang,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:true,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viInvMasterBarang = undefined;
							rowSelected_viInvMasterBarang = dataSource_viInvMasterBarang.getAt(row);
							CurrentData_viInvMasterBarang
							CurrentData_viInvMasterBarang.row = row;
							CurrentData_viInvMasterBarang.data = rowSelected_viInvMasterBarang.data;
							//DataInitInvMasterBarang(rowSelected_viInvMasterBarang.data);
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viInvMasterBarang = dataSource_viInvMasterBarang.getAt(ridx);
					if (rowSelected_viInvMasterBarang != undefined)
					{
						DataInitInvMasterBarang(rowSelected_viInvMasterBarang.data);
						//setLookUp_viInvMasterBarang(rowSelected_viInvMasterBarang.data);
					}
					else
					{
						//setLookUp_viInvMasterBarang();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Setup vendor
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header: 'Kode Kelompok',
						dataIndex: 'kd_inv',
						hideable:false,
						menuDisabled: true,
						width: 40
						
					},
					//-------------- ## --------------
					{
						header: 'No Urut',
						dataIndex: 'no_urut_brg',
						hideable:false,
						menuDisabled: true,
						width: 30
					},
					//-------------- ## --------------
					{
						header: 'Nama Barang',
						dataIndex: 'nama_brg',
						hideable:false,
						menuDisabled: true,
						width: 90
						
					},
					//-------------- ## --------------
					{
						header: 'Stok Min',
						dataIndex: 'min_stok',
						hideable:false,
                        menuDisabled:true,
						width: 40
					},
					//-------------- ## --------------
					{
						header: 'Satuan',
						dataIndex: 'satuan',
						hideable:false,
						menuDisabled: true,
						width: 50
					},
					//-------------- ## --------------
					{
						header: 'Kelompok',
						dataIndex: 'nama_sub',
						hideable:false,
						menuDisabled: true,
						width: 50
					}
					//-------------- ## --------------
					
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viInvMasterBarang',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viInvMasterBarang',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viInvMasterBarang != undefined)
							{
								DataInitInvMasterBarang(rowSelected_viInvMasterBarang.data);
							}
							
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viInvMasterBarang, selectCount_viInvMasterBarang, dataSource_viInvMasterBarang),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)	
	var PanelTabInvMasterBarang = new Ext.FormPanel({
        labelAlign: 'top',
		layout:'form',
		border: false,  
		autoHeight:true,
        title: '',
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [PanelInputVendor()]
		
	})
	// Kriteria filter pada Grid # --------------
    var FrmData_viInvMasterBarang = new Ext.Panel
    (
		{
			title: NamaForm_viInvMasterBarang,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viInvMasterBarang,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ PanelTabInvMasterBarang,
					GridDataView_viInvMasterBarang],
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAddBarang_viInvMasterBarang',
						handler: function(){
							AddNewInvMasterBarang();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpanBarang_viInvMasterBarang',
						handler: function()
						{
							loadMask.show();
							dataSave_viInvMasterBarang();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDeleteBarang_viInvMasterBarang',
						handler: function()
						{
							Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
								if (button == 'yes'){
									loadMask.show();
									dataDelete_viInvMasterBarang();
								}
							});
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_viInvMasterBarang',
						handler: function()
						{
							dataSource_viInvMasterBarang.removeAll();
							dataGriInvMasterBarang();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Barang',
						iconCls: 'find',
						id: 'btnBarang_viInvMasterBarang',
						handler: function()
						{
							setLookUp_viInvMasterBarang();
						}
					},
					{
						xtype: 'tbseparator'
					},
					
				]
			}
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_viInvMasterBarang;
    //-------------- # End form filter # --------------
}

function PanelInputVendor(rowdata){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 90,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'Kode Kelompok'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 0,
								xtype: 'textfield',
								id: 'txtKdKelompok_InvMasterBarang',
								name: 'txtKdKelompok_InvMasterBarang',
								allowBlank: false,
								width: 110,
								disabled:true
							},
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'Sub Kelompok'
							},
							{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 30,	
								xtype: 'textfield',
								name: 'txtSubKelompok_InvMasterBarang',
								id: 'txtSubKelompok_InvMasterBarang',
								width: 250,
								disabled:true
							},
							{
								x: 10,
								y: 60,
								xtype: 'label',
								text: 'Nama Barang'
							},
							{
								x: 120,
								y: 60,
								xtype: 'label',
								text: ':'
							},
							InvMasterBarang.vars.nama=new Nci.form.Combobox.autoComplete({
								x:130,
								y:60,	
								tabIndex:2,
								store	: InvMasterBarang.form.ArrayStore.a,
								select	: function(a,b,c){
									
									Ext.getCmp('txtKdKelompok_InvMasterBarang').setValue(b.data.kd_inv);
									Ext.getCmp('txtSubKelompok_InvMasterBarang').setValue(b.data.nama_sub);
									Ext.getCmp('txtNoUrut_InvMasterBarang').setValue(b.data.no_urut_brg);
									Ext.getCmp('txtStokMinimal_InvMasterBarang').setValue(b.data.min_stok);
									Ext.getCmp('cboSatuan_InvMasterBarang').setValue(b.data.kd_satuan);
									Ext.getCmp('cboFranction_InvMasterBarang').setValue(b.data.kd_satuan_kecil);
									Ext.getCmp('txtFracKecil_InvMasterBarang').setValue(b.data.fractions);
									
									GridDataView_viInvMasterBarang.getView().refresh();
									
								},
								onShowList:function(a){
									dataSource_viInvMasterBarang.removeAll();
									
									var recs=[],
									recType=dataSource_viInvMasterBarang.recordType;
										
									for(var i=0; i<a.length; i++){
										recs.push(new recType(a[i]));
									}
									dataSource_viInvMasterBarang.add(recs);
									
								},
								insert	: function(o){
									return {
										kd_inv      		: o.kd_inv,
										no_urut_brg 		: o.no_urut_brg,
										kd_satuan			: o.kd_satuan,
										nama_brg			: o.nama_brg,
										min_stok			: o.min_stok,
										satuan				: o.satuan,
										kd_satuan_kecil		: o.kd_satuan_kecil,
										nama_sub			: o.nama_sub,
										fractions			: o.fractions,
										text				:  '<table style="font-size: 11px;" align="left"><tr><td width="100">'+o.kd_inv+'</td><td width="70">'+o.no_urut_brg+'</td><td width="200">'+o.nama_brg+'</td></tr></table>'
									}
								},
								url		: baseURL + "index.php/bhp/functionMasterBarang/getGridBarang",
								valueField: 'nama_brg',
								displayField: 'text',
								listWidth: 270,
								width: 300
							}),
							/* {
								x:130,
								y:60,	
								xtype: 'textfield',
								name: 'txtNamaBarang_InvMasterBarang',
								id: 'txtNamaBarang_InvMasterBarang',
								width: 300
							}, */
							//-------------- ## --------------
							{
								x: 250,
								y: 0,
								xtype: 'label',
								text: 'No Urut'
							},
							{
								x: 300,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 310,
								y: 0,
								xtype: 'numberfield',
								name: 'txtNoUrut_InvMasterBarang',
								id: 'txtNoUrut_InvMasterBarang',
								tabIndex:6,
								width: 70,
								disabled:true
							},
							//---------------------------------------------------------------
							{
								x: 480,
								y: 0,
								xtype: 'label',
								text: 'Satuan'
							},
							{
								x: 575,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							comboSatuan_InvMasterBarang(),
							{
								//x: 260,
								x: 710,
								y: 0,
								xtype: 'label',
								text: 'Stok Minimal'
							},
							{
								//x: 350,
								x: 800,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								//x: 360,
								x: 810,
								y: 0,
								xtype: 'numberfield',
								name: 'txtStokMinimal_InvMasterBarang',
								id: 'txtStokMinimal_InvMasterBarang',
								tabIndex:11,
								style:'text-align:right;',
								width: 110
							},
							{
								x: 480,
								y: 30,
								xtype: 'label',
								text: 'Fraction Kecil'
							},
							{
								x: 575,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							{
								x: 585,
								y: 30,
								xtype: 'numberfield',
								name: 'txtFracKecil_InvMasterBarang',
								id: 'txtFracKecil_InvMasterBarang',
								tabIndex:12,
								width: 110
							},
							{
								x: 710,
								y: 30,
								xtype: 'label',
								text: 'Satuan Frac'
							},
							{
								x: 800,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							comboFraction_InvMasterBarang()
						]
					}
				]
			}
		]		
	};
        return items;
}

function setLookUp_viInvMasterBarang(rowdata){
    var lebar = 790;
    lookUp_viInvMasterBarang = new Ext.Window({
        id: Nci.getId(),
        title: 'Barang BHP', 
        closeAction: 'destroy',        
        width: 680,
        height: 655,
		constrain:true,
		autoHeight:true,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemBarang_viInvMasterBarang(lebar,rowdata),
        listeners:{
            activate: function(){
				
            },
            afterShow: function(){
                this.activate();
            },
            deactivate: function(){
                rowLookUpSelected_viInvMasterBarang=undefined;
            }
        }
    });

    lookUp_viInvMasterBarang.show();

    if (rowdata == undefined){
	
    }
    else
    {
        datainit_viInvPenerimaanBHP(rowdata);
    }
}

function getFormItemBarang_viInvMasterBarang(lebar,rowdata){
    var pnlFormDataBarang_viInvMasterBarang = new Ext.Panel({
		title: '',
		region: 'north',
		layout: 'form',
		bodyStyle: 'padding:10px 10px 10px 10px',
		anchor: '100%',
		labelWidth: 1,
		autoWidth: true,
		width: lebar,
		border: false,
		items:[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.40,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						anchor: '100% 100%',
						height: 300,
						items:
						[
							getItemPanelTreee_viInvMasterBarang()
						]
					},
					{
						columnWidth:.60,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						anchor: '100% 100%',
						height: 300,
						items:
						[
							getItemGridBarang_viInvMasterBarang(lebar)
						]
					}
				]
			}
		],
		fileUpload: true,
		fbar: {
				items: 
				[
					{
						xtype: 'button',
						text: 'Ok',
						id: 'btnOkLookUpBarang_viInvMasterBarang',
						handler: function(){
							Ext.getCmp('txtKdKelompok_InvMasterBarang').setValue(InvMasterBarang.vars.KodeInv);
							Ext.getCmp('txtSubKelompok_InvMasterBarang').setValue(InvMasterBarang.vars.NamaSubInv);
							lookUp_viInvMasterBarang.close();
						}
					},
					{
						xtype: 'button',
						text: 'Batal',
						id: 'btnBatalLookUpBarang_viInvMasterBarang',
						handler: function()
						{
							lookUp_viInvMasterBarang.close();
						}
					},
					
				]
			}
		}
    )

    return pnlFormDataBarang_viInvMasterBarang;
}

function getItemPanelTreee_viInvMasterBarang() {
    var items =
	{
		title:'',
		layout:'column',
		border:false,
		items:
		[
			itemsTreeListKelompokBarangGridDataView_viInvMasterBarang()
		
		]
	};
    return items;
};


function getItemGridBarang_viInvMasterBarang() 
{
    var items =
	{
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:1px 1px 1px 1px',
		border:true,
		height: 292,
	    items:
		[
			{
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:
				[
					gridDataViewBarang_viInvInvMasterBarang()
				]	
			}
			//-------------- ## --------------
		],
	};
    return items;
};

function gridDataViewBarang_viInvInvMasterBarang()
{
    var FieldGrdBArang_viInvMasterBarang = [];
	
    dsDataGrdBarang_viInvMasterBarang= new WebApp.DataStore
	({
        fields: FieldGrdBArang_viInvMasterBarang
    });
    
    gridPanelLookUpBarang_InvMasterBarang =new Ext.grid.EditorGridPanel({
        store: dsDataGrdBarang_viInvMasterBarang,
        height: 290,//220,
		stripeRows: true,
		columnLines: true,
		border:false,
		selModel: new Ext.grid.CellSelectionModel({
			singleSelect: true,
			listeners: {
				cellselect: function(sm, row, rec)
				{
					//cellSelecteddeskripsiRWI = dsTRDetailHistoryBayarList.getAt(row);
					//CurrentHistoryRWI.row = row;
					//CurrentHistoryRWI.data = cellSelecteddeskripsiRWI;
					console.log(dsDataGrdBarang_viInvMasterBarang.getAt(row).data.kd_inv);
					InvMasterBarang.vars.KodeInv = dsDataGrdBarang_viInvMasterBarang.getAt(row).data.kd_inv;
					InvMasterBarang.vars.NamaSubInv = dsDataGrdBarang_viInvMasterBarang.getAt(row).data.nama_sub;
					InvMasterBarang.vars.inv_kd_inv = dsDataGrdBarang_viInvMasterBarang.getAt(row).data.inv_kd_inv;
					//alert(InvMasterBarang.vars.KodeInv);
				}
			}
        }),
        
        columns: 
		[	
			new Ext.grid.RowNumberer(),
			{			
				dataIndex: '',
				header: 'inv_kd_inv',
				sortable: true,
				width: 80,
				hidden:true
			},
			{			
				dataIndex: 'kd_inv',
				header: 'Kode',
				sortable: true,
				width: 80,
				
			},
			{
				dataIndex: 'nama_sub',
				header: 'Kelompok',
				sortable: true,
				width: 200
			},
			//-------------- ## --------------
			
			//-------------- ## --------------
        ],
		viewConfig: 
			{
				forceFit: true
			}

       // plugins:chkSelected_viInvPenerimaanBHP,
    });
    return  gridPanelLookUpBarang_InvMasterBarang;
}

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------
function GetStrTreeSetBarang()
{
	 Ext.Ajax.request
	 (
		{
			url: WebAppUrl.UrlExecProcess,
			params: 
			{
				UserID: strUser,
				ModuleID: 'ProsesGetKelompokBarang',
				Params:	" and kd_inv<>''"
			},
			success : function(resp) 
			{
				loadMask.hide();
				var cst = Ext.decode(resp.responseText);
				StrTreeSetKelompokBarang= cst.arr;
			},
			failure:function()
			{
			    loadMask.hide();
			}
		}
	);
};


function itemsTreeListKelompokBarangGridDataView_viInvMasterBarang()
{	
		
	treeKlas_kelompokbarang_viInvMasterBarang= new Ext.tree.TreePanel
	(
		{
			autoScroll: true,
			split: true,
			height:280,
			width:230,
			loader: new Ext.tree.TreeLoader(),
			listeners: 
			{
				click: function(n) 
				{
					strTreeCriteriKelompokBarang_viInvMasterBarang=n.attributes
					if (strTreeCriteriKelompokBarang_viInvMasterBarang.id != ' 0')
					{
						if (strTreeCriteriKelompokBarang_viInvMasterBarang.leaf == false)
						{
							
						}
						else
						{
							console.log(n.attributes.id);
							dataGridLookUpBarangInvMasterBarang(n.attributes.id);
							
							if(n.attributes.id=='1000000000'){
								GetStrTreeSetBarang();
								rootTreeMasterBarang_InvMasterBarang = new Ext.tree.AsyncTreeNode
								(
									{
										expanded: true,
										text:'MASTER',
										id:IdRootKelompokBarang_pInvMasterBarang,
										children: StrTreeSetKelompokBarang,
										autoScroll: true
									}
								) 
								treeKlas_kelompokbarang_viInvMasterBarang.setRootNode(rootTreeMasterBarang_InvMasterBarang);
							} 
							
						};
					}
					else
					{
						
					};
				},
				
				
			}
		}
	);
	
	rootTreeMasterBarang_InvMasterBarang = new Ext.tree.AsyncTreeNode
	(
		{
			expanded: false,
			text:'MASTER',
			id:IdRootKelompokBarang_pInvMasterBarang,
			children: StrTreeSetKelompokBarang,
			autoScroll: true
		}
	)  
  
	treeKlas_kelompokbarang_viInvMasterBarang.setRootNode(rootTreeMasterBarang_InvMasterBarang);  
	
    var pnlTreeFormDataWindowPopup_viInvKelompokBarang = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                
				//-------------- ## -------------- 
                treeKlas_kelompokbarang_viInvMasterBarang,
                //-------------- ## -------------- 
            ],
            //-------------- #End items# --------------
        }
    )

    return pnlTreeFormDataWindowPopup_viInvKelompokBarang;
}
//------------------end---------------------------------------------------------


function dataGriInvMasterBarang(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/bhp/functionMasterBarang/getGridBarang",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorInvMasterBarang('Error grid! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dataSource_viInvMasterBarang.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dataSource_viInvMasterBarang.add(recs);
					
					
					
					GridDataView_viInvMasterBarang.getView().refresh();
				}
				else 
				{
					ShowPesanErrorInvMasterBarang('Gagal membaca data obat', 'Error');
				};
			}
		}
		
	)
	
}

function dataGridLookUpBarangInvMasterBarang(inv_kd_inv){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/bhp/functionMasterBarang/getGridLookUpBarang",
			params: {inv_kd_inv:inv_kd_inv},
			failure: function(o)
			{
				ShowPesanErrorInvMasterBarang('Error grid! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dsDataGrdBarang_viInvMasterBarang.removeAll();
					var recs=[],
						recType=dsDataGrdBarang_viInvMasterBarang.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dsDataGrdBarang_viInvMasterBarang.add(recs);
					
					
					
					gridPanelLookUpBarang_InvMasterBarang.getView().refresh();
				}
				else 
				{
					ShowPesanErrorInvMasterBarang('Gagal membaca data', 'Error');
				};
			}
		}
		
	)
	
}

function dataSave_viInvMasterBarang(){
	if (ValidasiSaveInvMasterBarang(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/bhp/functionMasterBarang/save",
				params: getParamSaveInvMasterBarang(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorInvMasterBarang('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoInvMasterBarang('Berhasil menyimpan data ini','Information');
						Ext.getCmp('txtNoUrut_InvMasterBarang').setValue(cst.nourut);
						dataSource_viInvMasterBarang.removeAll();
						dataGriInvMasterBarang();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorInvMasterBarang('Gagal menyimpan data ini', 'Error');
						dataSource_viInvMasterBarang.removeAll();
						dataGriInvMasterBarang();
					};
				}
			}
			
		)
	}
}

function dataDelete_viInvMasterBarang(){
	if (ValidasiSaveInvMasterBarang(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/bhp/functionMasterBarang/delete",
				params: getParamDeleteInvMasterBarang(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorInvMasterBarang('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoInvMasterBarang('Berhasil menghapus data ini','Information');
						AddNewInvMasterBarang()
						dataSource_viInvMasterBarang.removeAll();
						dataGriInvMasterBarang();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorInvMasterBarang('Gagal menghapus data ini', 'Error');
						dataSource_viInvMasterBarang.removeAll();
						dataGriInvMasterBarang();
					};
				}
			}
			
		)
	}
}

function comboSatuan_InvMasterBarang(){
    var Field_Satuan = ['kd_satuan', 'satuan'];
    ds_satuanInvMasterBarang = new WebApp.DataStore({fields: Field_Satuan});
    ds_satuanInvMasterBarang.load({
        params:{
	        Skip: 0,
	        Take: 1000,
	        Sort: 'satuan',
	        Sortdir: 'ASC',
	        target: 'ComboSatuanMasterBarang',
	        param: ""
        }
    });
    var cbSatuan_InvMasterBarang = new Ext.form.ComboBox({
			//x:130,
			x:585,
			y:0,
            flex: 1,
			id: 'cboSatuan_InvMasterBarang',
			valueField: 'kd_satuan',
            displayField: 'satuan',
			store: ds_satuanInvMasterBarang,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:110,
			listeners:{ 
				'select': function(a,b,c){
					selectSetUnit=b.data.displayText ;
				},
				'specialkey' : function(){
					if (Ext.EventObject.getKey() === 13) {
						
					} 						
				}
			}
        }
    )    
    return cbSatuan_InvMasterBarang;
}

function comboFraction_InvMasterBarang(){
    var Field_Franction = ['kd_satuan_kecil', 'satuan_kecil'];
    ds_FranctionInvMasterBarang = new WebApp.DataStore({fields: Field_Franction});
    ds_FranctionInvMasterBarang.load({
        params:{
	        Skip: 0,
	        Take: 1000,
	        Sort: 'satuan_kecil',
	        Sortdir: 'ASC',
	        target: 'ComboFractionMasterBarang',
	        //target: 'ComboSatuanMasterBarangInventaris',
	        param: ""
        }
    });
    var cbFranction_InvMasterBarang = new Ext.form.ComboBox({
			x:810,
			y:30,
            flex: 1,
			id: 'cboFranction_InvMasterBarang',
			valueField: 'kd_satuan_kecil',
            displayField: 'satuan_kecil',
			store: ds_FranctionInvMasterBarang,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:110,
			listeners:{ 
				'select': function(a,b,c){
					
				},
				'specialkey' : function(){
					if (Ext.EventObject.getKey() === 13) {
						
					} 						
				}
			}
        }
    )    
    return cbFranction_InvMasterBarang;
}

function AddNewInvMasterBarang(){
	InvMasterBarang.vars.nama.setValue('');
	Ext.getCmp('txtKdKelompok_InvMasterBarang').setValue('');
	Ext.getCmp('txtSubKelompok_InvMasterBarang').setValue('');
	Ext.getCmp('txtNoUrut_InvMasterBarang').setValue('');
	Ext.getCmp('txtStokMinimal_InvMasterBarang').setValue('');
	Ext.getCmp('cboSatuan_InvMasterBarang').setValue('');
	Ext.getCmp('txtFracKecil_InvMasterBarang').setValue('');
	Ext.getCmp('cboFranction_InvMasterBarang').setValue('');
	InvMasterBarang.vars.inv_kd_inv='';
	InvMasterBarang.vars.KodeInv='';
	InvMasterBarang.vars.NamaSubInv='';
};

function DataInitInvMasterBarang(rowdata){
	Ext.getCmp('txtKdKelompok_InvMasterBarang').setValue(rowdata.kd_inv);
	Ext.getCmp('txtSubKelompok_InvMasterBarang').setValue(rowdata.nama_sub);
	Ext.getCmp('txtNoUrut_InvMasterBarang').setValue(rowdata.no_urut_brg);
	InvMasterBarang.vars.nama.setValue(rowdata.nama_brg);
	Ext.getCmp('txtStokMinimal_InvMasterBarang').setValue(rowdata.min_stok);
	Ext.getCmp('cboSatuan_InvMasterBarang').setValue(rowdata.kd_satuan);
	Ext.getCmp('cboFranction_InvMasterBarang').setValue(rowdata.kd_satuan_kecil);
	Ext.getCmp('txtFracKecil_InvMasterBarang').setValue(rowdata.fractions);
};

function getParamSaveInvMasterBarang(){
	var	params =
	{
		KdInv:Ext.getCmp('txtKdKelompok_InvMasterBarang').getValue(),
		InvKdInv:InvMasterBarang.vars.inv_kd_inv,
		NoUrut:Ext.getCmp('txtNoUrut_InvMasterBarang').getValue(),
		NamaBrg: InvMasterBarang.vars.nama.getValue(),//InvMasterBarang.vars.nama.getValue(),
		Satuan:Ext.getCmp('cboSatuan_InvMasterBarang').getValue(),
		StokMin:Ext.getCmp('txtStokMinimal_InvMasterBarang').getValue(),
		SatuanKecil:Ext.getCmp('cboFranction_InvMasterBarang').getValue(),
		FracKecil:Ext.getCmp('txtFracKecil_InvMasterBarang').getValue()
	}
   
    return params
};

function getParamDeleteInvMasterBarang(){
	var	params =
	{
		KdInv:Ext.getCmp('txtKdKelompok_InvMasterBarang').getValue(),
		NoUrut:Ext.getCmp('txtNoUrut_InvMasterBarang').getValue(),
	}
   
    return params
};

function ValidasiSaveInvMasterBarang(modul,mBolHapus){
	var x = 1;
	if(InvMasterBarang.vars.nama.getValue() === ''){
		loadMask.hide();
		ShowPesanWarningInvMasterBarang('Nama vendor masih kosong', 'Warning');
		x = 0;
	} 
	return x;
};



function ShowPesanWarningInvMasterBarang(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorInvMasterBarang(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoInvMasterBarang(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};