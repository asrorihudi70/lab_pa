var dataSource_viInvPembelianBHP;
var selectCount_viInvPembelianBHP=50;
var NamaForm_viInvPembelianBHP="Pembelian Bahan Habis Pakai";
var mod_name_viInvPembelianBHP="viInvPembelianBHP";
var now_viInvPembelianBHP= new Date();
var rowSelected_viInvPembelianBHP;
var setLookUps_viInvPembelianBHP;
var tanggal = now_viInvPembelianBHP.format("d/M/Y");
var jam = now_viInvPembelianBHP.format("H/i/s");
var tmpkriteria;
var GridDataView_viInvPembelianBHP;
var cPONumber_viInvPembelianBHP;
var cTglAwal_viInvPembelianBHP;
var cTglAkhir_viInvPembelianBHP;


var CurrentData_viInvPembelianBHP =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viInvPembelianBHP(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

var InvPembelianBHP={};
InvPembelianBHP.form={};
InvPembelianBHP.func={};
InvPembelianBHP.vars={};
InvPembelianBHP.func.parent=InvPembelianBHP;
InvPembelianBHP.form.ArrayStore={};
InvPembelianBHP.form.ComboBox={};
InvPembelianBHP.form.DataStore={};
InvPembelianBHP.form.Record={};
InvPembelianBHP.form.Form={};
InvPembelianBHP.form.Grid={};
InvPembelianBHP.form.Panel={};
InvPembelianBHP.form.TextField={};
InvPembelianBHP.form.Button={};

InvPembelianBHP.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_inv','no_urut_brg','nama_brg','kd_satuan','satuan','qty','req_number'],
	data: []
});

InvPembelianBHP.form.ArrayStore.ro= new Ext.data.ArrayStore({
	id: 0,
	fields:['req_number','no_urut_brg','nama_brg','qty','kd_satuan','satuan','req_line'],
	data: []
});

function dataGrid_viInvPembelianBHP(mod_id_viInvPembelianBHP){	
    var FieldMaster_viInvPembelianBHP = 
	[
		'no_minta','tgl_minta', 'jenis_minta','jenis'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viInvPembelianBHP = new WebApp.DataStore
	({
        fields: FieldMaster_viInvPembelianBHP
    });
    getDataGridAwalInvPembelianBHP();
    // Grid Gizi Perencanaan # --------------
	GridDataView_viInvPembelianBHP = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: 'Daftar Pembelian',
			store: dataSource_viInvPembelianBHP,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viInvPembelianBHP = undefined;
							rowSelected_viInvPembelianBHP = dataSource_viInvPembelianBHP.getAt(row);
							CurrentData_viInvPembelianBHP
							CurrentData_viInvPembelianBHP.row = row;
							CurrentData_viInvPembelianBHP.data = rowSelected_viInvPembelianBHP.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viInvPembelianBHP = dataSource_viInvPembelianBHP.getAt(ridx);
					if (rowSelected_viInvPembelianBHP != undefined)
					{
						setLookUp_viInvPembelianBHP(rowSelected_viInvPembelianBHP.data);
					}
					else
					{
						setLookUp_viInvPembelianBHP();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Gizi perencanaan
	        *	Terdiri dari : Judul, Isi dan Event
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header: 'PO. Number',
						dataIndex: 'po_number',
						hideable:false,
						menuDisabled: true,
						width: 25
						
					},
					//-------------- ## --------------
					{
						header: 'Tanggal',
						dataIndex: 'po_date',
						hideable:false,
						menuDisabled: true,
						width: 30,
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.po_date);
						}
					},
					{
						header: 'PBF',
						dataIndex: 'vendor',
						hideable:false,
						menuDisabled: true,
						width: 30
					},
					{
						header: 'Keterangan',
						dataIndex: 'remark',
						hideable:false,
						menuDisabled: true,
						width: 30
					},
					//-------------- ## --------------
					{
						header: 'kd_vendor',
						dataIndex: 'kd_vendor',
						hideable:false,
						menuDisabled: true,
						hidden:true,
						width: 30
					},
					{
						header: 'PPN',
						dataIndex: 'ppn',
						hideable:false,
						menuDisabled: true,
						hidden:true,
						width: 30
					},
					//-------------- ## --------------
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viInvPembelianBHP',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add Pembelian',
						iconCls: 'Edit_Tr',
						tooltip: 'Add Data',
						id: 'btnTambah_viInvPembelianBHP',
						handler: function(sm, row, rec)
						{
							
							setLookUp_viInvPembelianBHP();
						}
					},
					{
						xtype: 'button',
						text: 'Edit Pembelian',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viInvPembelianBHP',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viInvPembelianBHP != undefined)
							{
								setLookUp_viInvPembelianBHP(rowSelected_viInvPembelianBHP.data)
							}
							
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viInvPembelianBHP, selectCount_viInvPembelianBHP, dataSource_viInvPembelianBHP),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )
	
	var pencarianInvPembelianBHP = new Ext.FormPanel({
        labelAlign: 'top',
        frame:true,
        title: '',
        bodyStyle:'padding:5px 5px 0',
        items: [
		{
			layout: 'column',
			border: false,
			items:
			[
				{
					columnWidth:.98,
					layout: 'absolute',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					width: 500,
					height: 70,
					anchor: '100% 100%',
					items:
					[
						{
							x: 10,
							y: 0,
							xtype: 'label',
							text: 'Po. Number'
						},
						{
							x: 120,
							y: 0,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 0,
							xtype: 'textfield',
							id: 'TxtFilterGridPONumberInvPembelianBHP',
							name: 'TxtFilterGridPONumberInvPembelianBHP',
							width: 350,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										cPONumber_viInvPembelianBHP=Ext.getCmp('TxtFilterGridPONumberInvPembelianBHP').getValue();
										cTglAwal_viInvPembelianBHP=Ext.getCmp('dfTglAwalInvPembelianBHP').getValue();
										cTglAkhir_viInvPembelianBHP=Ext.getCmp('dfTglAkhirInvPembelianBHP').getValue();
										getDataGridAwalInvPembelianBHP(cPONumber_viInvPembelianBHP,cTglAwal_viInvPembelianBHP,cTglAkhir_viInvPembelianBHP);
									} 						
								}
							}
						},
						//----------------------------------------
						{
							x: 483,
							y: 0,
							xtype: 'label',
							text: '* Enter untuk mencari'
						},
						//----------------------------------------
						{
							x: 580,
							y: 30,
							xtype: 'button',
							text: 'Refresh',
							iconCls: 'refresh',
							tooltip: 'Refresh',
							style:{paddingLeft:'30px'},
							width:150,
							id: 'BtnFilterGridCari_viInvPembelianBHP',
							handler: function() 
							{					
								getDataGridAwalInvPembelianBHP();
							}                        
						},
						{
							x: 10,
							y: 30,
							xtype: 'label',
							text: 'Tanggal'
						},
						{
							x: 120,
							y: 30,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 30,
							xtype: 'datefield',
							id: 'dfTglAwalInvPembelianBHP',
							format: 'd/M/Y',
							value:now_viInvPembelianBHP,
							width: 150,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										cPONumber_viInvPembelianBHP=Ext.getCmp('TxtFilterGridPONumberInvPembelianBHP').getValue();
										cTglAwal_viInvPembelianBHP=Ext.getCmp('dfTglAwalInvPembelianBHP').getValue();
										cTglAkhir_viInvPembelianBHP=Ext.getCmp('dfTglAkhirInvPembelianBHP').getValue();
										getDataGridAwalInvPembelianBHP(cPONumber_viInvPembelianBHP,cTglAwal_viInvPembelianBHP,cTglAkhir_viInvPembelianBHP);
									} 						
								}
							}
						},
						{
							x: 290,
							y: 30,
							xtype: 'label',
							text: 's/d'
						},
						{
							x: 320,
							y: 30,
							xtype: 'datefield',
							id: 'dfTglAkhirInvPembelianBHP',
							format: 'd/M/Y',
							value:now_viInvPembelianBHP,
							width: 150,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										cPONumber_viInvPembelianBHP=Ext.getCmp('TxtFilterGridPONumberInvPembelianBHP').getValue();
										cTglAwal_viInvPembelianBHP=Ext.getCmp('dfTglAwalInvPembelianBHP').getValue();
										cTglAkhir_viInvPembelianBHP=Ext.getCmp('dfTglAkhirInvPembelianBHP').getValue();
										getDataGridAwalInvPembelianBHP(cPONumber_viInvPembelianBHP,cTglAwal_viInvPembelianBHP,cTglAkhir_viInvPembelianBHP);
									} 						
								}
							}
						}
					]
				}
			]
		}
		]	
		
	})

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viInvPembelianBHP = new Ext.Panel
    (
		{
			title: NamaForm_viInvPembelianBHP,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viInvPembelianBHP,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ pencarianInvPembelianBHP,
					GridDataView_viInvPembelianBHP],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viInvPembelianBHP,
		            columns: 21,
		            defaults: {
					scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viInvPembelianBHP;
    //-------------- # End form filter # --------------
}

function refreshInvPembelianBHP(kriteria)
{
    dataSource_viInvPembelianBHP.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'ViewPermintaanBahan',
                    param : kriteria
                }			
            }
        );   
    return dataSource_viInvPembelianBHP;
}

function setLookUp_viInvPembelianBHP(rowdata){
    var lebar = 785;
    setLookUps_viInvPembelianBHP = new Ext.Window({
        id: Nci.getId(),
        title: NamaForm_viInvPembelianBHP, 
        closeAction: 'destroy',        
        width: 750,
        height: 655,
		constrain:true,
		autoHeight:true,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viInvPembelianBHP(lebar,rowdata),
        listeners:{
            activate: function(){
				
            },
            afterShow: function(){
                this.activate();
            },
            deactivate: function(){
                rowSelected_viInvPembelianBHP=undefined;
            }
        }
    });

    setLookUps_viInvPembelianBHP.show();

    if (rowdata == undefined){
	
    }
    else
    {
        datainit_viInvPembelianBHP(rowdata);
    }
}

function getFormItemEntry_viInvPembelianBHP(lebar,rowdata){
    var pnlFormDataBasic_viInvPembelianBHP = new Ext.FormPanel({
		title: '',
		region: 'north',
		layout: 'form',
		bodyStyle: 'padding:10px 10px 10px 10px',
		anchor: '100%',
		labelWidth: 1,
		autoWidth: true,
		width: lebar,
		border: false,
		items:[
				getItemPanelInputPerencanaan_viInvPembelianBHP(lebar),
				getItemGridPerencanaan_viInvPembelianBHP(lebar)
			],
			fileUpload: true,
		tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAdd_viInvPembelianBHP',
						handler: function(){
							dataaddnew_viInvPembelianBHP();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						disabled: true,
						id: 'btnSimpan_viInvPembelianBHP',
						handler: function()
						{
							datasave_viInvPembelianBHP();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save & Close',
						disabled: true,
						iconCls: 'saveexit',
						id: 'btnSimpanExit_viInvPembelianBHP',
						handler: function()
						{
							datasave_viInvPembelianBHP();
							refreshInvPembelianBHP();
							setLookUps_viInvPembelianBHP.close();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						text	: 'Delete',
						id		: 'btnHapusPembelianBahan_InvPembelianBHP',
						tooltip	: nmLookup,
						iconCls	: 'remove',
						disabled: true,
						handler	: function(){
							Ext.Msg.confirm('Hapus pembelian', 'Apakah pembelian barang ini akan dihapus?', function(button){
								if (button == 'yes'){
									Ext.Ajax.request
									(
										{
											url: baseURL + "index.php/bhp/functionPembelianBHP/hapusPembelian",
											params:{po_number:Ext.getCmp('txtPONumber_InvPembelianBHPL').getValue()} ,
											failure: function(o)
											{
												ShowPesanErrorInvPembelianBHP('Error, hapus pembelian! Hubungi Admin', 'Error');
											},	
											success: function(o) 
											{
												var cst = Ext.decode(o.responseText);
												if (cst.success === true) 
												{
													getDataGridAwalInvPembelianBHP();
													setLookUps_viInvPembelianBHP.close();
												}
												else 
												{
													ShowPesanErrorInvPembelianBHP('Gagal menghapus pembelian ini', 'Error');
												};
											}
										}
									)
								}
							});		
						}
					},
					{
						xtype: 'tbseparator'
					},
					/* {
						xtype: 'button',
						text: 'Posting',
						disabled: true,
						iconCls: 'gantidok',
						id: 'btnPosting_viInvPembelianBHP',
						handler: function()
						{
							dataposting_viInvPembelianBHP();
						}
					},
					{
						xtype: 'tbseparator'
					} */
					
				]
			}
		}
    )

    return pnlFormDataBasic_viInvPembelianBHP;
}

function getItemPanelInputPerencanaan_viInvPembelianBHP(lebar) {
    var items =
	{
		title:'',
		layout:'column',
		items:
		[
			{
				columnWidth: .50,
				layout: 'form',
				labelWidth:110,
				bodyStyle:'padding: 10px',
				border: false,
				items:
				[
					{
						xtype: 'textfield',
						fieldLabel: 'PO. Number ',
						name: 'txtPONumber_InvPembelianBHPL',
						id: 'txtPONumber_InvPembelianBHPL',
						readOnly:true,
						tabIndex:0,
						width: 150
					},
					{
						xtype: 'datefield',
						fieldLabel: 'Tanggal Po ',
						name: 'dfTglRo_InvPembelianBHPL',
						id	: 'dfTglRo_InvPembelianBHPL',
						format: 'd/M/Y',
						value:now_viInvPembelianBHP,
						tabIndex:1,
						//readOnly:true,
						width: 150
					},
				]
			},
			{
				columnWidth: .50,
				layout: 'form',
				labelWidth:110,
				bodyStyle:'padding: 10px',
				border: false,
				items:
				[
					comboVendorInvPembelianBHP(),
					{
						xtype: 'textfield',
						fieldLabel: 'Keterangan ',
						name: 'txtKeterangan_InvPembelianBHPL',
						id: 'txtKeterangan_InvPembelianBHPL',
						tabIndex:0,
						width: 150
					},
				]
			}
		
		]
	};
    return items;
};

function getItemGridPerencanaan_viInvPembelianBHP(lebar) 
{
    var items =
	{
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:1px 1px 1px 1px',
		border:true,
		width: lebar-55,
		height: 380,
		tbar:
		[
			{
				text	: 'Add Barang',
				id		: 'btnAddPembeliaanBarang',
				tooltip	: nmLookup,
				disabled: false,
				iconCls	: 'find',
				handler	: function(){
					var records = new Array();
					records.push(new dsDataGrdJab_viInvPembelianBHP.recordType());
					dsDataGrdJab_viInvPembelianBHP.add(records);
				}
			},	
			{
				text	: 'Delete',
				id		: 'btnHapusGridBarang_InvPembelianBHP',
				tooltip	: nmLookup,
				iconCls	: 'remove',
				disabled: true,
				handler: function()
				{
					var lines =  InvPembelianBHP.form.Grid.a.getSelectionModel().selection.cell[0];
					var o = dsDataGrdJab_viInvPembelianBHP.getRange()[lines].data;
					if(dsDataGrdJab_viInvPembelianBHP.getCount()>1){
						Ext.Msg.confirm('Warning', 'Apakah barang ini akan dihapus?', function(button){
							if (button == 'yes'){
								if(dsDataGrdJab_viInvPembelianBHP.getRange()[lines].data.line != undefined){
									Ext.Ajax.request
									(
										{
											url: baseURL + "index.php/bhp/functionPembelianBHP/hapusBarisGridBarang",
											params: {
												po_number:Ext.getCmp('txtPONumber_InvPembelianBHPL').getValue(),
												line:o.line,
												req_number:o.req_number,
												no_urut_brg:o.no_urut_brg
												
											},
											failure: function(o)
											{
												ShowPesanErrorInvPembelianBHP('Error, hapus barang! Hubungi Admin', 'Error');
											},	
											success: function(o) 
											{
												var cst = Ext.decode(o.responseText);
												if (cst.success === true) 
												{
													dsDataGrdJab_viInvPembelianBHP.removeAt(lines);
													InvPembelianBHP.form.Grid.a.getView().refresh();
													Ext.getCmp('btnSimpan_viInvPembelianBHP').enable();
													Ext.getCmp('btnSimpanExit_viInvPembelianBHP').enable();
												}
												else 
												{
													ShowPesanErrorInvPembelianBHP('Gagal menghapus data ini', 'Error');
												};
											}
										}
										
									)
								}else{
									dsDataGrdJab_viInvPembelianBHP.removeAt(lines);
									InvPembelianBHP.form.Grid.a.getView().refresh();
									Ext.getCmp('btnSimpan_viInvPembelianBHP').enable();
									Ext.getCmp('btnSimpanExit_viInvPembelianBHP').enable();
								}
							} 
							
						});
					} else{
						ShowPesanErrorInvPembelianBHP('Data tidak bisa dihapus karena minimal 1 barang','Error');
					}
					
				}
			}	
		],
	    items:
		[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:
				[
					gridDataViewEdit_viInvPembelianBHP(),
					{
						layout: 'column',
						border: false,
						items:
						[
							{
								columnWidth:.99,
								layout: 'absolute',
								bodyStyle: 'padding: 5px 5px 5px 5px',
								border: false,
								width: 510,
								height: 70,
								anchor: '100% 100%',
								items:
								[
									{
										x:5,
										y:5,
										xtype: 'checkbox',
										boxLabel: 'Posted',
										name: 'cbPosted_InvPembelianBHP',
										checked: false,
										inputValue: 'posted',
										disabled:true
									},
									{
										x:520,
										y:5,
										xtype: 'label',
										text:'Sub Total'
									},
									{
										x:580,
										y:5,
										xtype: 'label',
										text:':'
									},
									{
										x:590,
										y:5,
										xtype: 'textfield',
										style: 'text-align:right;',
										name: 'txtSubTotal_InvPembelianBHPL',
										id: 'txtSubTotal_InvPembelianBHPL',
										readOnly:true,
										tabIndex:0,
										width: 110
									},
									//----------------------------------------------
									{
										x:350,
										y:30,
										xtype: 'label',
										text:'PPN'
									},
									{
										x:380,
										y:30,
										xtype: 'label',
										text:':'
									},
									{
										x:390,
										y:30,
										xtype: 'textfield',
										style: 'text-align:right;',
										name: 'txtPPN_InvPembelianBHPL',
										id: 'txtPPN_InvPembelianBHPL',
										readOnly:true,
										tabIndex:0,
										width: 110
									},
									{
										x:520,
										y:30,
										xtype: 'label',
										text:'Total'
									},
									{
										x:580,
										y:30,
										xtype: 'label',
										text:':'
									},
									{
										x:590,
										y:30,
										xtype: 'textfield',
										style: 'text-align:right;',
										name: 'txtGrandTotal_InvPembelianBHPL',
										id: 'txtGrandTotal_InvPembelianBHPL',
										readOnly:true,
										tabIndex:0,
										width: 110
									},
								
								]
							}
						]
					}
				]	
			}
			//-------------- ## --------------
		],
	};
    return items;
};

function gridDataViewEdit_viInvPembelianBHP()
{
    var FieldGrdKasir_viInvPembelianBHP = [];
	
    dsDataGrdJab_viInvPembelianBHP= new WebApp.DataStore
	({
        fields: FieldGrdKasir_viInvPembelianBHP
    });
    
    InvPembelianBHP.form.Grid.a =new Ext.grid.EditorGridPanel({
        store: dsDataGrdJab_viInvPembelianBHP,
        height: 290,//220,
		stripeRows: true,
		columnLines: true,
		selModel: new Ext.grid.CellSelectionModel({
	            singleSelect: true,
	            listeners: {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
        }),
        
        columns: 
		[	
			new Ext.grid.RowNumberer(),
			{			
				dataIndex: 'req_number',
				header: 'Ro. Number',
				sortable: true,
				width: 110,
				editor:new Nci.form.Combobox.autoComplete({
					store	: InvPembelianBHP.form.ArrayStore.ro,
					select	: function(a,b,c){
						//'req_number','no_urut_brg','nama_brg','qty'
						var line	= InvPembelianBHP.form.Grid.a.getSelectionModel().selection.cell[0];
						dsDataGrdJab_viInvPembelianBHP.getRange()[line].data.req_number=b.data.req_number;
						dsDataGrdJab_viInvPembelianBHP.getRange()[line].data.no_urut_brg=b.data.no_urut_brg;
						dsDataGrdJab_viInvPembelianBHP.getRange()[line].data.kd_inv=b.data.kd_inv;
						dsDataGrdJab_viInvPembelianBHP.getRange()[line].data.kd_satuan=b.data.kd_satuan;
						dsDataGrdJab_viInvPembelianBHP.getRange()[line].data.satuan=b.data.satuan;
						dsDataGrdJab_viInvPembelianBHP.getRange()[line].data.qty=b.data.qty;
						dsDataGrdJab_viInvPembelianBHP.getRange()[line].data.nama_brg=b.data.nama_brg;
						dsDataGrdJab_viInvPembelianBHP.getRange()[line].data.req_line=b.data.req_line;
						
						Ext.getCmp('btnAdd_viInvPembelianBHP').enable();
						Ext.getCmp('btnHapusGridBarang_InvPembelianBHP').enable();
						Ext.getCmp('btnSimpan_viInvPembelianBHP').enable();
						Ext.getCmp('btnSimpanExit_viInvPembelianBHP').enable();
						
						InvPembelianBHP.form.Grid.a.getView().refresh();
					},
					insert	: function(o){
						return {
							req_number      : o.req_number,
							no_urut_brg 	: o.no_urut_brg,
							kd_inv			: o.kd_inv,
							kd_satuan		: o.kd_satuan,
							satuan			: o.satuan,
							qty				: o.qty,
							nama_brg		: o.nama_brg,
							req_line		: o.req_line,
							text			:  '<table style="font-size: 11px;"><tr><td width="110">'+o.req_number+'</td><td width="150">'+o.nama_brg+'</td><td width="60">'+o.qty+'</td></tr></table>'
						}
					},
					url		: baseURL + "index.php/bhp/functionPembelianBHP/getRONumber",
					valueField: 'nama_brg',
					displayField: 'text',
					listWidth: 320
				})
			},
			{			
				dataIndex: 'kd_inv',
				header: 'Kode',
				sortable: true,
				width: 80
			},
			//-------------- ## --------------
			{			
				dataIndex: 'nama_brg',
				header: 'Uraian',
				sortable: true,
				width: 200,
				editor:new Nci.form.Combobox.autoComplete({
					store	: InvPembelianBHP.form.ArrayStore.a,
					select	: function(a,b,c){
						var line	= InvPembelianBHP.form.Grid.a.getSelectionModel().selection.cell[0];
						dsDataGrdJab_viInvPembelianBHP.getRange()[line].data.req_number=b.data.req_number;
						dsDataGrdJab_viInvPembelianBHP.getRange()[line].data.kd_inv=b.data.kd_inv;
						dsDataGrdJab_viInvPembelianBHP.getRange()[line].data.nama_brg=b.data.nama_brg;
						dsDataGrdJab_viInvPembelianBHP.getRange()[line].data.kd_satuan=b.data.kd_satuan;
						dsDataGrdJab_viInvPembelianBHP.getRange()[line].data.satuan=b.data.satuan;
						dsDataGrdJab_viInvPembelianBHP.getRange()[line].data.qty=b.data.qty;
						dsDataGrdJab_viInvPembelianBHP.getRange()[line].data.no_urut_brg=b.data.no_urut_brg;
						
						Ext.getCmp('btnAdd_viInvPembelianBHP').enable();
						Ext.getCmp('btnHapusGridBarang_InvPembelianBHP').enable();
						Ext.getCmp('btnSimpan_viInvPembelianBHP').enable();
						Ext.getCmp('btnSimpanExit_viInvPembelianBHP').enable();
						
						InvPembelianBHP.form.Grid.a.getView().refresh();
					},
					insert	: function(o){
						return {
							req_number      : o.req_number,
							kd_inv       	: o.kd_inv,
							nama_brg 		: o.nama_brg,
							kd_satuan		: o.kd_satuan,
							satuan			: o.satuan,
							qty				: o.qty,
							no_urut_brg		: o.no_urut_brg,
							text			:  '<table style="font-size: 11px;"><tr><td width="110">'+o.kd_inv+'</td><td width="150">'+o.nama_brg+'</td><td width="60">'+o.qty+'</td></tr></table>'
						}
					},
					url		: baseURL + "index.php/bhp/functionPembelianBHP/getBarang",
					valueField: 'nama_brg',
					displayField: 'text',
					listWidth: 320
				})
			},
			//-------------- ## --------------
			{
				dataIndex: 'satuan',
				header: 'Satuan',
				sortable: true,
				width: 90
			},
			//-------------- ## --------------
			{
				dataIndex: 'qty',
				header: 'Qty',
				sortable: true,
				align:'right',
				width: 60,
				editor: new Ext.form.NumberField({
					allowBlank: false,
					listeners:{
						blur: function(a){
							/* var line	= this.index;
							dsDataGrdJab_viInvPembelianBHP.getRange()[line].data.stok_opname=a.getValue(); */
							var line	= this.index;
							if(a.getValue() == 0){
								ShowPesanWarningInvPembelianBHP('Harga tidak boleh 0', 'Warning');
							}
							hasilJumlahHarga();
						},
						focus: function(a){
							this.index=InvPembelianBHP.form.Grid.a.getSelectionModel().selection.cell[0]
						}
					}
				})	
			},
			//-------------- ## --------------
			{
				dataIndex: 'harga',
				header: 'Harga',
				align:'right',
				xtype:'numbercolumn',
				sortable: true,
				width: 80,
				editor: new Ext.form.NumberField({
					allowBlank: false,
					listeners:{
						blur: function(a){
							var line	= this.index;
							if(a.getValue() == 0){
								ShowPesanWarningInvPembelianBHP('Harga tidak boleh 0', 'Warning');
							}
							hasilJumlahHarga();
						},
						focus: function(a){
							this.index=InvPembelianBHP.form.Grid.a.getSelectionModel().selection.cell[0]
						}
					}
				})	
			},
			{
				dataIndex: 'jumlah',
				header: 'Jumlah',
				align:'right',
				xtype:'numbercolumn',
				sortable: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'kd_satuan',
				header: 'kd_satuan',
				hidden: true,
				width: 80
			},
			{
				dataIndex: 'no_urut_brg',
				header: 'no_urut_brg',
				hidden: true,
				width: 80
			},
			{
				dataIndex: 'req_line',
				header: 'req_line',
				hidden: true,
				width: 80
			},
			{
				dataIndex: 'line',
				header: 'line',
				hidden: true,
				width: 80
			}
			//-------------- ## --------------
        ],
		viewConfig: 
			{
				forceFit: true
			}

    });
    return  InvPembelianBHP.form.Grid.a;
}

function hasilJumlahHarga(){
	var grandtotal=0;
	var ppn=0;
	var subtotal=0;
	
	for(var i=0; i < dsDataGrdJab_viInvPembelianBHP.getCount() ; i++){
		var subJumlah=0;
	
		var o=dsDataGrdJab_viInvPembelianBHP.getRange()[i].data;
		if(o.qty != undefined){
			subJumlah=parseFloat(o.qty) * parseFloat(o.harga);
			o.jumlah=subJumlah;
			
			if(isNaN(o.jumlah)){
				o.jumlah=0;
			} else{
				o.jumlah=o.jumlah;
			}
			
			subtotal +=subJumlah;
			ppn = subtotal* 0.1;
			grandtotal = subtotal + ppn;
			
			if(isNaN(subtotal)){
				ppn=0;
				subtotal=0;
				grandtotal=0;
			} else{
				ppn=ppn;
				subtotal=subtotal;
				grandtotal=grandtotal;
			}
		}
	}
	
	Ext.getCmp('txtPPN_InvPembelianBHPL').setValue(toFormat(ppn));
	Ext.getCmp('txtSubTotal_InvPembelianBHPL').setValue(toFormat(subtotal));
	Ext.getCmp('txtGrandTotal_InvPembelianBHPL').setValue(toFormat(grandtotal));
	
	InvPembelianBHP.form.Grid.a.getView().refresh();
}


function comboVendorInvPembelianBHP(){
	var Field_Vendor = ['kd_vendor', 'vendor'];
    ds_VendorInvPembelianBHP = new WebApp.DataStore({fields: Field_Vendor});
    ds_VendorInvPembelianBHP.load({
        params:{
	        Skip: 0,
	        Take: 1000,
	        Sort: 'kd_vendor',
	        Sortdir: 'ASC',
	        target: 'ComboVendorBHP',
	        param: ""
        }
    });
	var cboVendorInvPembelianBHP = new Ext.form.ComboBox({
        id:'cbVendorInvPembelianBHP',
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:true,
        mode: 'local',
        width: 150,
		readOnly:false,
		fieldLabel:'PBF ',
        store: ds_VendorInvPembelianBHP,
        valueField: 'kd_vendor',
        displayField: 'vendor',
        listeners:{
			'select': function(a,b,c){
				
			}
        }
	});
	return cboVendorInvPembelianBHP;
};


function dataaddnew_viInvPembelianBHP(){
	Ext.getCmp('txtPONumber_InvPembelianBHPL').setValue('');
	Ext.getCmp('dfTglRo_InvPembelianBHPL').setValue(now_viInvPembelianBHP);
	Ext.getCmp('cbVendorInvPembelianBHP').setValue('');
	Ext.getCmp('txtKeterangan_InvPembelianBHPL').setValue('');
	
	Ext.getCmp('cbVendorInvPembelianBHP').setReadOnly(false);
	Ext.getCmp('dfTglRo_InvPembelianBHPL').setReadOnly(false);
	Ext.getCmp('txtKeterangan_InvPembelianBHPL').setReadOnly(false);
	
	dsDataGrdJab_viInvPembelianBHP.removeAll();
}

function datainit_viInvPembelianBHP(rowdata)
{
	Ext.getCmp('txtPONumber_InvPembelianBHPL').setValue(rowdata.po_number);
	Ext.getCmp('dfTglRo_InvPembelianBHPL').setValue(ShowDate(rowdata.po_date));
	Ext.getCmp('cbVendorInvPembelianBHP').setValue(rowdata.vendor);
	Ext.getCmp('txtKeterangan_InvPembelianBHPL').setValue(rowdata.remark);
	
	Ext.getCmp('cbVendorInvPembelianBHP').setReadOnly(true);
	Ext.getCmp('dfTglRo_InvPembelianBHPL').setReadOnly(true);
	Ext.getCmp('txtKeterangan_InvPembelianBHPL').setReadOnly(true);
	
	getGridBarangInvPembelianBHP(rowdata.po_number);
	//cekTerimaBarang(rowdata.po_number);
	Ext.getCmp('btnSimpan_viInvPembelianBHP').enable();
	Ext.getCmp('btnSimpanExit_viInvPembelianBHP').enable();
	Ext.getCmp('btnHapusPembelianBahan_InvPembelianBHP').enable();
	Ext.getCmp('btnHapusGridBarang_InvPembelianBHP').enable();
};

function datasave_viInvPembelianBHP(){
	if (ValidasiEntryInvPembelianBHP(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/bhp/functionPembelianBHP/save",
				params: getParamInvPembelianBHP(),
				failure: function(o)
				{
					ShowPesanErrorInvPembelianBHP('Error, simpan data! Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoInvPembelianBHP('Data berhasil disimpan','Information');
						Ext.getCmp('txtPONumber_InvPembelianBHPL').setValue(cst.ponumber);
						getDataGridAwalInvPembelianBHP();
						Ext.getCmp('btnSimpan_viInvPembelianBHP').disable();
						Ext.getCmp('btnSimpanExit_viInvPembelianBHP').disable();
						Ext.getCmp('btnHapusPembelianBahan_InvPembelianBHP').disable();
						
						Ext.getCmp('cbVendorInvPembelianBHP').setReadOnly(true);
						Ext.getCmp('dfTglRo_InvPembelianBHPL').setReadOnly(true);
						Ext.getCmp('txtKeterangan_InvPembelianBHPL').setReadOnly(true);
					}
					else 
					{
						ShowPesanErrorInvPembelianBHP('Gagal Menyimpan Data ini', 'Error');
					};
				}
			}
			
		)
	}
}

function getDataGridAwalInvPembelianBHP(ponumber,tglawal,tglakhir){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/bhp/functionPembelianBHP/getGridAwal",
			params: {
				ponumber:ponumber,
				tglawal:tglawal,
				tglakhir:tglakhir
			},
			failure: function(o)
			{
				ShowPesanErrorInvPembelianBHP('Error, membaca grid awal! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					dataSource_viInvPembelianBHP.removeAll();
					
					var recs=[],
						recType=dataSource_viInvPembelianBHP.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
						dataSource_viInvPembelianBHP.add(recs);
					
					GridDataView_viInvPembelianBHP.getView().refresh();
				}
				else 
				{
					ShowPesanErrorInvPembelianBHP('Gagal membaca data obat', 'Error');
				};
			}
		}
		
	)
	
}


function getGridBarangInvPembelianBHP(ponumber){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/bhp/functionPembelianBHP/getGridBarangLoad",
			params: {ponumber:ponumber},
			failure: function(o)
			{
				ShowPesanErrorInvPembelianBHP('Error, membaca grid barang! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dsDataGrdJab_viInvPembelianBHP.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
						dsDataGrdJab_viInvPembelianBHP.add(recs);
					
					InvPembelianBHP.form.Grid.a.getView().refresh();
					hasilJumlahHarga();
				}
				else 
				{
					ShowPesanErrorInvPembelianBHP('Gagal membaca data barang', 'Error');
				};
			}
		}
		
	)
	
}

function cekTerimaBahanInvPembelianBHP(no_minta){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionPermintaanBahan/cekTerimaBahanInvPembelianBHP",
			params: {no_minta:no_minta},
			failure: function(o)
			{
				ShowPesanErrorInvPembelianBHP('Error, cek! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					Ext.getCmp('btnAddPembeliaanBarang').enable();
					Ext.getCmp('btnHapusGridBarang_InvPembelianBHP').enable();
					Ext.getCmp('btnSimpan_viInvPembelianBHP').enable();
					Ext.getCmp('btnSimpanExit_viInvPembelianBHP').enable();
					Ext.getCmp('btnHapusPembelianBahan_InvPembelianBHP').enable();
				}
				else 
				{
					ShowPesanInfoInvPembelianBHP('Bahan-bahan dari permintaan ini sudah diterima', 'Information');
					Ext.getCmp('btnSimpan_viInvPembelianBHP').disable(true);
					Ext.getCmp('btnSimpanExit_viInvPembelianBHP').disable(true);
					Ext.getCmp('btnHapusPembelianBahan_InvPembelianBHP').disable(true);
					Ext.getCmp('btnAddPembeliaanBarang').disable(true);
					Ext.getCmp('btnHapusGridBarang_InvPembelianBHP').disable(true);
				};
			}
		}
		
	)
}

function getParamInvPembelianBHP() 
{
	var	params =
	{
		PONumber:Ext.getCmp('txtPONumber_InvPembelianBHPL').getValue(),
		PODate:Ext.getCmp('dfTglRo_InvPembelianBHPL').getValue(),
		Remark:Ext.getCmp('txtKeterangan_InvPembelianBHPL').getValue(),
		KdVendor:Ext.getCmp('cbVendorInvPembelianBHP').getValue(),
		PPN:toInteger(Ext.getCmp('txtPPN_InvPembelianBHPL').getValue())
		
	}
	
	params['jumlah']=dsDataGrdJab_viInvPembelianBHP.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viInvPembelianBHP.getCount();i++)
	{
		params['qty-'+i]=dsDataGrdJab_viInvPembelianBHP.data.items[i].data.qty
		params['harga-'+i]=dsDataGrdJab_viInvPembelianBHP.data.items[i].data.harga
		params['req_number-'+i]=dsDataGrdJab_viInvPembelianBHP.data.items[i].data.req_number
		params['req_line-'+i]=dsDataGrdJab_viInvPembelianBHP.data.items[i].data.req_line
		params['no_urut_brg-'+i]=dsDataGrdJab_viInvPembelianBHP.data.items[i].data.no_urut_brg
		params['line-'+i]=dsDataGrdJab_viInvPembelianBHP.data.items[i].data.line
	}
    return params
};


function ValidasiEntryInvPembelianBHP(modul,mBolHapus)
{
	var x = 1;
	if(Ext.getCmp('cbVendorInvPembelianBHP').getValue() === ''){
		ShowPesanWarningInvPembelianBHP('PBF tidak boleh kosong', 'Warning');
		x = 0;
	}
	if(dsDataGrdJab_viInvPembelianBHP.getCount() < 0){
		ShowPesanWarningInvPembelianBHP('Daftar permintaan bahan tidak boleh kosong, minimal bahan 1', 'Warning');
		x = 0;
	}
	
	for(var i=0; i<dsDataGrdJab_viInvPembelianBHP.getCount() ; i++){
		var o=dsDataGrdJab_viInvPembelianBHP.getRange()[i].data;
		if(o.qty == undefined || o.qty <= 0){
			ShowPesanWarningInvPembelianBHP('Qty tidak boleh 0, minimal qty 1', 'Warning');
			x = 0;
		}
		for(var j=i+1; j<dsDataGrdJab_viInvPembelianBHP.getCount() ; j++){
			var p=dsDataGrdJab_viInvPembelianBHP.getRange()[j].data;
			if(o.no_urut_brg == p.no_urut_brg){
				ShowPesanWarningInvPembelianBHP('Barang tidak boleh sama, periksa kembali daftar barang', 'Warning');
				x = 0;
				break;
			}
		}
	}
	
	return x;
};



function ShowPesanWarningInvPembelianBHP(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorInvPembelianBHP(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};


function ShowPesanInfoInvPembelianBHP(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};