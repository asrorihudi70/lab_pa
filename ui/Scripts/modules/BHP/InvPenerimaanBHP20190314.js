var dataSource_viInvPenerimaanBHP;
var selectCount_viInvPenerimaanBHP=50;
var NamaForm_viInvPenerimaanBHP="Penerimaan Bahan Habis Pakai";
var mod_name_viInvPenerimaanBHP="viInvPenerimaanBHP";
var now_viInvPenerimaanBHP= new Date();
var rowSelected_viInvPenerimaanBHP;
var setLookUps_viInvPenerimaanBHP;
var tanggal = now_viInvPenerimaanBHP.format("d/M/Y");
var jam = now_viInvPenerimaanBHP.format("H/i/s");
var tmpkriteria;
var GridDataView_viInvPenerimaanBHP;
var cNoTerima_viInvPenerimaanBHP;
var cPONumber_viInvPenerimaanBHP;
var cTglAkhir_viInvPenerimaanBHP;
var cTglAwal_viInvPenerimaanBHP;
var Field_Vendor = ['PO_NUMBER','NO_TERIMA','TGL_TERIMA','NO_FAKTUR','KD_VENDOR','VENDOR','KETERANGAN','NO_SPK','TGL_SPK','STATUS_POSTING','NO_PENERIMAAN','TGL_PENERIMAAN',
'po_number','no_terima','tgl_terima','no_faktur','kd_vendor','vendor','keterangan','no_spk','tgl_spk','status_posting','no_penerimaan','tgl_penerimaan'];
// dataSource_viInvPenerimaanBHP.removeAll();
dataSource_viInvPenerimaanBHP = new WebApp.DataStore({fields: Field_Vendor});


var CurrentData_viInvPenerimaanBHP =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viInvPenerimaanBHP(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

var InvPenerimaanBHP={};
InvPenerimaanBHP.form={};
InvPenerimaanBHP.func={};
InvPenerimaanBHP.vars={};
InvPenerimaanBHP.func.parent=InvPenerimaanBHP;
InvPenerimaanBHP.form.ArrayStore={};
InvPenerimaanBHP.form.ComboBox={};
InvPenerimaanBHP.form.DataStore={};
InvPenerimaanBHP.form.Record={};
InvPenerimaanBHP.form.Form={};
InvPenerimaanBHP.form.Grid={};
InvPenerimaanBHP.form.Panel={};
InvPenerimaanBHP.form.TextField={};
InvPenerimaanBHP.form.Button={};

InvPenerimaanBHP.form.ArrayStore.ponumber= new Ext.data.ArrayStore({
	id: 0,
	fields:['po_number','kd_inv','no_urut_brg','nama_brg','jml_order','harga','jumlah'],
	data: []
});

InvPenerimaanBHP.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['po_number','kd_inv','no_urut_brg','nama_brg','jml_order','harga'],
	data: []
});

function dataGrid_viInvPenerimaanBHP(mod_id_viInvPenerimaanBHP){	
    // getDataGridAwalInvPenerimaanBHP();
    // Grid Gizi Perencanaan # --------------
	GridDataView_viInvPenerimaanBHP = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: 'Daftar Penerimaan',
			store: dataSource_viInvPenerimaanBHP,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viInvPenerimaanBHP = undefined;
							rowSelected_viInvPenerimaanBHP = dataSource_viInvPenerimaanBHP.getAt(row);
							CurrentData_viInvPenerimaanBHP
							CurrentData_viInvPenerimaanBHP.row = row;
							CurrentData_viInvPenerimaanBHP.data = rowSelected_viInvPenerimaanBHP.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viInvPenerimaanBHP = dataSource_viInvPenerimaanBHP.getAt(ridx);
					if (rowSelected_viInvPenerimaanBHP != undefined)
					{
						setLookUp_viInvPenerimaanBHP(rowSelected_viInvPenerimaanBHP.data);
					}
					else
					{
						setLookUp_viInvPenerimaanBHP();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Gizi perencanaan
	        *	Terdiri dari : Judul, Isi dan Event
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header		: 'Status Posting',
						width		: 20,
						sortable	: false,
						hideable	: true,
						hidden		: false,
						menuDisabled: true,
						dataIndex	: 'status_posting',
						renderer	: function(value, metaData, record, rowIndex, colIndex, store){
							 switch (value){
								 case 't':
									 metaData.css = 'StatusHijau'; 
									 break;
								 case 'f':
									 metaData.css = 'StatusMerah';
									 break;
							 }
							 return '';
						}
					},
					{
						header: 'No. Penerimaan',
						dataIndex: 'no_terima',
						hideable:false,
						menuDisabled: true,
						width: 25
						
					},
					{
						header: 'PO. Number',
						dataIndex: 'po_number',
						hideable:false,
						menuDisabled: true,
						width: 25
						
					},
					//-------------- ## --------------
					{
						header: 'Tanggal',
						dataIndex: 'tgl_terima',
						hideable:false,
						menuDisabled: true,
						width: 30,
						// renderer: function(v, params, record)
						// {
						// 	return ShowDate(record.data.tgl_terima);
						// }
					},
					{
						header: 'PBF',
						dataIndex: 'vendor',
						hideable:false,
						menuDisabled: true,
						width: 30
					},
					{
						header: 'Keterangan',
						dataIndex: 'keterangan',
						hideable:false,
						menuDisabled: true,
						hidden:true,
						width: 30
					},
					//-------------- ## --------------
					
					//-------------- ## --------------
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viInvPenerimaanBHP',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add Penerimaan',
						iconCls: 'Edit_Tr',
						tooltip: 'Add Data',
						id: 'btnTambah_viInvPenerimaanBHP',
						handler: function(sm, row, rec)
						{
							
							setLookUp_viInvPenerimaanBHP();
							Ext.getCmp('btnAdd_viInvPenerimaanBHP').disable();
						}
					},
					{
						xtype: 'button',
						text: 'Edit Pembelian',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viInvPenerimaanBHP',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viInvPenerimaanBHP != undefined)
							{
								setLookUp_viInvPenerimaanBHP(rowSelected_viInvPenerimaanBHP.data)
							}
							
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viInvPenerimaanBHP, selectCount_viInvPenerimaanBHP, dataSource_viInvPenerimaanBHP),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )
	
	var pencarianInvPenerimaanBHP = new Ext.FormPanel({
        labelAlign: 'top',
        frame:true,
        title: '',
        bodyStyle:'padding:5px 5px 0',
        items: [
		{
			layout: 'column',
			border: false,
			items:
			[
				{
					columnWidth:.98,
					layout: 'absolute',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					width: 500,
					height: 120,
					anchor: '100% 100%',
					items:
					[
						{
							x: 10,
							y: 0,
							xtype: 'label',
							text: 'No. Penerimaan'
						},
						{
							x: 120,
							y: 0,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 0,
							xtype: 'textfield',
							id: 'TxtFilterGridNoPenerimaanInvPenerimaanBHP',
							name: 'TxtFilterGridNoPenerimaanInvPenerimaanBHP',
							width: 350,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										loadDataPenerimaan();
										cNoTerima_viInvPenerimaanBHP=Ext.getCmp('TxtFilterGridNoPenerimaanInvPenerimaanBHP').getValue();
										cPONumber_viInvPenerimaanBHP=Ext.getCmp('TxtFilterGridPONumberInvPenerimaanBHP').getValue();
										cTglAwal_viInvPenerimaanBHP=Ext.getCmp('dfTglAwalInvPenerimaanBHP').getValue();
										cTglAkhir_viInvPenerimaanBHP=Ext.getCmp('dfTglAkhirInvPenerimaanBHP').getValue();
										// getDataGridAwalInvPenerimaanBHP(cNoTerima_viInvPenerimaanBHP,cPONumber_viInvPenerimaanBHP,cTglAwal_viInvPenerimaanBHP,cTglAkhir_viInvPenerimaanBHP);
									} 						
								}
							}
						},
						{
							x: 10,
							y: 30,
							xtype: 'label',
							text: 'Po. Number'
						},
						{
							x: 120,
							y: 30,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 30,
							xtype: 'textfield',
							id: 'TxtFilterGridPONumberInvPenerimaanBHP',
							name: 'TxtFilterGridPONumberInvPenerimaanBHP',
							width: 350,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										loadDataPenerimaan();
										cNoTerima_viInvPenerimaanBHP=Ext.getCmp('TxtFilterGridNoPenerimaanInvPenerimaanBHP').getValue();
										cPONumber_viInvPenerimaanBHP=Ext.getCmp('TxtFilterGridPONumberInvPenerimaanBHP').getValue();
										cTglAwal_viInvPenerimaanBHP=Ext.getCmp('dfTglAwalInvPenerimaanBHP').getValue();
										cTglAkhir_viInvPenerimaanBHP=Ext.getCmp('dfTglAkhirInvPenerimaanBHP').getValue();
										// getDataGridAwalInvPenerimaanBHP(cNoTerima_viInvPenerimaanBHP,cPONumber_viInvPenerimaanBHP,cTglAwal_viInvPenerimaanBHP,cTglAkhir_viInvPenerimaanBHP);
									} 						
								}
							}
						},
						//----------------------------------------
						{
							x: 10,
							y: 90,
							xtype: 'label',
							text: '*) Enter untuk mencari'
						},
						//----------------------------------------
						{
							x: 580,
							y: 90,
							xtype: 'button',
							text: 'Refresh',
							iconCls: 'refresh',
							tooltip: 'Refresh',
							style:{paddingLeft:'30px'},
							width:150,
							id: 'BtnFilterGridCari_viInvPenerimaanBHP',
							handler: function() 
							{	
								loadDataPenerimaan();
								// getDataGridAwalInvPenerimaanBHP();
							}                        
						},
						{
							x: 10,
							y: 60,
							xtype: 'label',
							text: 'Tanggal Penerimaan'
						},
						{
							x: 120,
							y: 60,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 60,
							xtype: 'datefield',
							id: 'dfTglAwalInvPenerimaanBHP',
							format: 'd/M/Y',
							value:now_viInvPenerimaanBHP,
							width: 150,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										loadDataPenerimaan();
										cNoTerima_viInvPenerimaanBHP=Ext.getCmp('TxtFilterGridNoPenerimaanInvPenerimaanBHP').getValue();
										cPONumber_viInvPenerimaanBHP=Ext.getCmp('TxtFilterGridPONumberInvPenerimaanBHP').getValue();
										cTglAwal_viInvPenerimaanBHP=Ext.getCmp('dfTglAwalInvPenerimaanBHP').getValue();
										cTglAkhir_viInvPenerimaanBHP=Ext.getCmp('dfTglAkhirInvPenerimaanBHP').getValue();
										// getDataGridAwalInvPenerimaanBHP(cNoTerima_viInvPenerimaanBHP,cPONumber_viInvPenerimaanBHP,cTglAwal_viInvPenerimaanBHP,cTglAkhir_viInvPenerimaanBHP);
									} 						
								}
							}
						},
						{
							x: 290,
							y: 60,
							xtype: 'label',
							text: 's/d'
						},
						{
							x: 320,
							y: 60,
							xtype: 'datefield',
							id: 'dfTglAkhirInvPenerimaanBHP',
							format: 'd/M/Y',
							value:now_viInvPenerimaanBHP,
							width: 150,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										loadDataPenerimaan();
										cNoTerima_viInvPenerimaanBHP=Ext.getCmp('TxtFilterGridNoPenerimaanInvPenerimaanBHP').getValue();
										cPONumber_viInvPenerimaanBHP=Ext.getCmp('TxtFilterGridPONumberInvPenerimaanBHP').getValue();
										cTglAwal_viInvPenerimaanBHP=Ext.getCmp('dfTglAwalInvPenerimaanBHP').getValue();
										cTglAkhir_viInvPenerimaanBHP=Ext.getCmp('dfTglAkhirInvPenerimaanBHP').getValue();
										// getDataGridAwalInvPenerimaanBHP(cNoTerima_viInvPenerimaanBHP,cPONumber_viInvPenerimaanBHP,cTglAwal_viInvPenerimaanBHP,cTglAkhir_viInvPenerimaanBHP);
									} 						
								}
							}
						}
					]
				}
			]
		}
		]	
		
	})

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viInvPenerimaanBHP = new Ext.Panel
    (
		{
			title: NamaForm_viInvPenerimaanBHP,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viInvPenerimaanBHP,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ pencarianInvPenerimaanBHP,
					GridDataView_viInvPenerimaanBHP],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viInvPenerimaanBHP,
		            columns: 21,
		            defaults: {
					scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viInvPenerimaanBHP;

    loadDataPenerimaan();
    //-------------- # End form filter # --------------
}

function refreshInvPenerimaanBHP(kriteria)
{
    dataSource_viInvPenerimaanBHP.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'ViewPermintaanBahan',
                    param : kriteria
                }			
            }
        );   
    return dataSource_viInvPenerimaanBHP;
}

function setLookUp_viInvPenerimaanBHP(rowdata){
    var lebar = 790;
    setLookUps_viInvPenerimaanBHP = new Ext.Window({
        id: Nci.getId(),
        title: NamaForm_viInvPenerimaanBHP, 
        closeAction: 'destroy',        
        width: 800,
        height: 655,
		constrain:true,
		autoHeight:true,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viInvPenerimaanBHP(lebar,rowdata),
        listeners:{
            activate: function(){
				
            },
            afterShow: function(){
                this.activate();
            },
            deactivate: function(){
                rowSelected_viInvPenerimaanBHP=undefined;
            }
        }
    });

    setLookUps_viInvPenerimaanBHP.show();

    if (rowdata == undefined){
	
    }
    else
    {
        datainit_viInvPenerimaanBHP(rowdata);
    }
}

function getFormItemEntry_viInvPenerimaanBHP(lebar,rowdata){
    var pnlFormDataBasic_viInvPenerimaanBHP = new Ext.FormPanel({
		title: '',
		region: 'north',
		layout: 'form',
		bodyStyle: 'padding:10px 10px 10px 10px',
		anchor: '100%',
		labelWidth: 1,
		autoWidth: true,
		width: lebar,
		border: false,
		items:[
				getItemPanelInputPerencanaan_viInvPenerimaanBHP(lebar),
				getItemGridPerencanaan_viInvPenerimaanBHP(lebar)
			],
			fileUpload: true,
		tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAdd_viInvPenerimaanBHP',
						handler: function(){
							dataaddnew_viInvPenerimaanBHP();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						// disabled: true,
						id: 'btnSimpan_viInvPenerimaanBHP',
						handler: function()
						{
							datasave_viInvPenerimaanBHP();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save & Close',
						disabled: true,
						iconCls: 'saveexit',
						id: 'btnSimpanExit_viInvPenerimaanBHP',
						handler: function()
						{
							datasave_viInvPenerimaanBHP();
							refreshInvPenerimaanBHP();
							setLookUps_viInvPenerimaanBHP.close();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						text	: 'Delete',
						id		: 'btnHapusPenerimaanBahan_InvPenerimaanBHP',
						tooltip	: nmLookup,
						iconCls	: 'remove',
						disabled: true,
						handler	: function(){
							Ext.Msg.confirm('Hapus Penerimaan', 'Apakah penerimaan ini akan dihapus?', function(button){
								if (button == 'yes'){
									Ext.Ajax.request
									(
										{
											url: baseURL + "index.php/bhp/functionPenerimaanBHP/hapusPenerimaan",
											params:{no_terima:Ext.getCmp('txtNoTerima_InvPenerimaanBHPL').getValue()} ,
											failure: function(o)
											{
												ShowPesanErrorInvPenerimaanBHP('Error, hapus penerimaan! Hubungi Admin', 'Error');
											},	
											success: function(o) 
											{
												var cst = Ext.decode(o.responseText);
												if (cst.success === true) 
												{
													getDataGridAwalInvPenerimaanBHP();
													ShowPesanInfoInvPenerimaanBHP('Penerimaan berhasil diHapus', 'Information');
													// getDataGridAwalInvPenerimaanBHP();
													setLookUps_viInvPenerimaanBHP.close();
												}
												else 
												{
													ShowPesanErrorInvPenerimaanBHP('Gagal menghapus penerimaan ini', 'Error');
												};
											}
										}
										
									)
								}
							});		
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Posting',
						disabled: true,
						iconCls: 'gantidok',
						id: 'btnPosting_viInvPenerimaanBHP',
						handler: function()
						{
							Ext.Msg.confirm('Posting Penerimaan', 'Apakah anda yakin penerimaan ini akan diPosting? Pastikan data sudah benar sebelum diPosting', function(button){
								if (button == 'yes'){
									dataposting_viInvPenerimaanBHP()
								}
							})
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: '<i class="fa fa-reply" aria-hidden="true"></i> Unposting',
						disabled: true,
						// iconCls: 'refresh',
						id: 'btnUnPosting_viInvPenerimaanBHP',
						handler: function()
						{
							Ext.Msg.confirm('Posting Penerimaan', 'Apakah anda yakin penerimaan ini akan Unposting ? ', function(button){
								if (button == 'yes'){
									Ext.Ajax.request({
										url: baseURL + "index.php/bhp/functionPenerimaanBHP/updateUnPosting",
										params: {
											noterima : Ext.getCmp('txtNoTerima_InvPenerimaanBHPL').getValue(),
										},
										failure: function(o){
											ShowPesanErrorInvPenerimaanBHP('Error, posting! Hubungi Admin', 'Error');
										},	
										success: function(o) {
											var cst = Ext.decode(o.responseText);
											if (cst.success === true) {
												// DISINI
												getDataGridAwalInvPenerimaanBHP();
												ShowPesanInfoInvPenerimaanBHP('Berhasil di unposting', 'Warning');
												Ext.getCmp('btnAddPenerimaanBarang').enable();
												Ext.getCmp('btnHapusGridBarang_InvPenerimaanBHP').enable();
												// Ext.getCmp('btnSimpan_viInvPenerimaanBHP').enable();
												Ext.getCmp('btnSimpanExit_viInvPenerimaanBHP').enable();
												Ext.getCmp('btnHapusPenerimaanBahan_InvPenerimaanBHP').enable();
												Ext.getCmp('btnPosting_viInvPenerimaanBHP').enable();
												Ext.getCmp('btnUnPosting_viInvPenerimaanBHP').disable();
												InvPenerimaanBHP.form.Grid.a.enable();
												// Ext.getCmp('txtNoFaktur_InvPenerimaanBHPL').setReadOnly(false);
												// Ext.getCmp('txtNoSPK_InvPenerimaanBHPL').setReadOnly(false);
												// Ext.getCmp('dfTglFaktur_InvPenerimaanBHPL').setReadOnly(false);
												Ext.getCmp('txtKeterangan_InvPenerimaanBHPL').setReadOnly(false);
												// Ext.getCmp('dfTglSPK_InvPenerimaanBHPL').setReadOnly(false);
											}else {
												InvPenerimaanBHP.form.Grid.a.disable();
												Ext.getCmp('txtKeterangan_InvPenerimaanBHPL').setReadOnly(true);
												// Ext.getCmp('dfTglFaktur_InvPenerimaanBHPL').setReadOnly(true);q
												// Ext.getCmp('dfTglSPK_InvPenerimaanBHPL').setReadOnly(true);
												// Ext.getCmp('txtNoFaktur_InvPenerimaanBHPL').setReadOnly(true);
												// Ext.getCmp('txtNoSPK_InvPenerimaanBHPL').setReadOnly(true);
												Ext.getCmp('btnUnPosting_viInvPenerimaanBHP').enable();
												ShowPesanInfoInvPenerimaanBHP('Gagal di unposting', 'Warning');
												// Ext.getCmp('btnSimpan_viInvPenerimaanBHP').disable();
												Ext.getCmp('btnSimpanExit_viInvPenerimaanBHP').disable();
												Ext.getCmp('btnHapusPenerimaanBahan_InvPenerimaanBHP').disable();
												Ext.getCmp('btnAddPenerimaanBarang').disable();
												Ext.getCmp('btnHapusGridBarang_InvPenerimaanBHP').disable();
												Ext.getCmp('btnPosting_viInvPenerimaanBHP').disable();
												// getDataGridAwalInvPenerimaanBHP();
											};
										}
									});
								}
							})
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Print',
						iconCls: 'print',
						id: 'btnPrint_viInvPenerimaanBHP',
						handler: function(){
							var url = baseURL + "index.php/laporan/lap_inv_penerimaan/cetak/"+Ext.getCmp('txtNoTerima_InvPenerimaanBHPL').getValue()+"/";
							new Ext.Window({
								title: 'Preview',
								width: 1000,
								height: 600,
								constrain: true,
								modal: true,
								html: "<iframe  style ='width: 100%; height: 100%;' src='" + url+"true'></iframe>",
								tbar : [
									{
										xtype   : 'button',
										text    : 'Cetak Direct',
										iconCls : 'print',
										handler : function(){
											window.open(url+"false", '_blank');
										}
									}
								]
							}).show();
						}
					}
					
				]
			}
		}
    )

    return pnlFormDataBasic_viInvPenerimaanBHP;
}

function getItemPanelInputPerencanaan_viInvPenerimaanBHP(lebar) {
    var items =
	{
		title:'',
		layout:'column',
		items:
		[
			{
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: false,
				width: lebar-22,
				height: 110,
				anchor: '100% 100%',
				items:
				[
					{
						x:10,
						y:5,
						xtype: 'label',
						text:'No. Terima'
					},
					{
						x:70,
						y:5,
						xtype: 'label',
						text:':'
					},
					{
						x:80,
						y:5,
						xtype: 'textfield',
						name: 'txtNoTerima_InvPenerimaanBHPL',
						id: 'txtNoTerima_InvPenerimaanBHPL',
						readOnly:true,
						tabIndex:0,
						width: 110
					},
					{
						x:10,
						y:30,
						xtype: 'label',
						text:'No. Faktur'
					},
					{
						x:70,
						y:30,
						xtype: 'label',
						text:':'
					},
					{
						x:80,
						y:30,
						xtype: 'textfield',
						fieldLabel: 'PO. Number ',
						name: 'txtNoFaktur_InvPenerimaanBHPL',
						id: 'txtNoFaktur_InvPenerimaanBHPL',
						tabIndex:0,
						width: 110
					},
					{
						x:10,
						y:55,
						xtype: 'label',
						text:'Vendor'
					},
					{
						x:70,
						y:55,
						xtype: 'label',
						text:':'
					},
					comboVendorInvPenerimaanBHP(),
					{
						x:10,
						y:80,
						xtype: 'label',
						text:'Penerimaan',
					},
					{
						x:70,
						y:80,
						xtype: 'label',
						text:':'
					},
					{
						x:80,
						y:80,
						xtype: 'textfield',
						fieldLabel: 'No Penerimaan ',
						name: 'txtBuktiPenerimaan_InvPenerimaanBHPL',
						id: 'txtBuktiPenerimaan_InvPenerimaanBHPL',
						tabIndex:0,
						width: 110
					},
					//-------------------------------------
					{
						x:200,
						y:8,
						xtype: 'checkbox',
						boxLabel: 'Posted',
						id: 'cbPosted_InvPenerimaanBHP',
						checked: false,
						disabled:true
					},
					{
						x:200,
						y:30,
						xtype: 'label',
						text:'Tanggal'
					},
					{
						x:240,
						y:30,
						xtype: 'label',
						text:':'
					},
					{
						x:250,
						y:30,
						xtype: 'datefield',
						name: 'dfTglFaktur_InvPenerimaanBHPL',
						id	: 'dfTglFaktur_InvPenerimaanBHPL',
						format: 'd/M/Y',
						value:now_viInvPenerimaanBHP,
						width: 110
					},
					{
						x:200,
						y:80,
						xtype: 'label',
						text:'Tanggal'
					},
					{
						x:240,
						y:80,
						xtype: 'label',
						text:':'
					},
					{
						x:250,
						y:80,
						xtype: 'datefield',
						name: 'dfTglPenerimaan_InvPenerimaanBHPL',
						id	: 'dfTglPenerimaan_InvPenerimaanBHPL',
						format: 'd/M/Y',
						value:now_viInvPenerimaanBHP,
						width: 110
					},
					//----------------------------------------------
					{
						x:380,
						y:5,
						xtype: 'label',
						text:'Keterangan'
					},
					{
						x:450,
						y:5,
						xtype: 'label',
						text:':'
					},
					{
						x:460,
						y:5,
						xtype: 'textarea',
						name: 'txtKeterangan_InvPenerimaanBHPL',
						id: 'txtKeterangan_InvPenerimaanBHPL',
						width: 285,
						height:45,
					},
					{
						x:380,
						y:55,
						xtype: 'label',
						text:'No. SPK'
					},
					{
						x:450,
						y:55,
						xtype: 'label',
						text:':'
					},
					{
						x:460,
						y:55,
						xtype: 'textfield',
						name: 'txtNoSPK_InvPenerimaanBHPL',
						id: 'txtNoSPK_InvPenerimaanBHPL',
						tabIndex:0,
						width: 110
					},
					//-------------------------------------------------
					{
						x:575,
						y:55,
						xtype: 'label',
						text:'Tanggal SPK'
					},
					{
						x:638,
						y:55,
						xtype: 'label',
						text:':'
					},
					{
						x:645,
						y:55,
						xtype: 'datefield',
						name: 'dfTglSPK_InvPenerimaanBHPL',
						id	: 'dfTglSPK_InvPenerimaanBHPL',
						format: 'd/M/Y',
						value:now_viInvPenerimaanBHP,
						width: 100
					},
				]
			}
		
		]
	};
    return items;
};

function getItemGridPerencanaan_viInvPenerimaanBHP(lebar) 
{
    var items =
	{
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:1px 1px 1px 1px',
		border:true,
		width: lebar-55,
		height: 370,
		tbar:
		[
			{
				text	: 'Add',
				id		: 'btnAddPenerimaanBarang',
				tooltip	: nmLookup,
				disabled: true,
				iconCls	: 'find',
				handler	: function(){
					var records = new Array();
					records.push(new dsDataGrdJab_viInvPenerimaanBHP.recordType());
					dsDataGrdJab_viInvPenerimaanBHP.add(records);
				}
			},	
			{
				text	: 'Delete',
				id		: 'btnHapusGridBarang_InvPenerimaanBHP',
				tooltip	: nmLookup,
				iconCls	: 'remove',
				disabled: true,
				handler: function()
				{
					var line =  InvPenerimaanBHP.form.Grid.a.getSelectionModel().selection.cell[0];
					var o = dsDataGrdJab_viInvPenerimaanBHP.getRange()[line].data;
					if(dsDataGrdJab_viInvPenerimaanBHP.getCount()>1){
						Ext.Msg.confirm('Warning', 'Apakah barang ini akan dihapus?', function(button){
							if (button == 'yes'){
								if(dsDataGrdJab_viInvPenerimaanBHP.getRange()[line].data.no_baris != undefined){
									Ext.Ajax.request
									(
										{
											url: baseURL + "index.php/bhp/functionPenerimaanBHP/hapusBarisGridBarang",
											params: {
												no_terima:Ext.getCmp('txtNoTerima_InvPenerimaanBHPL').getValue(),
												no_urut_brg:o.no_urut_brg,
												no_baris:o.no_urut_brg,
												po_number:o.po_number
											},
											failure: function(o)
											{
												ShowPesanErrorInvPenerimaanBHP('Error, hapus barang! Hubungi Admin', 'Error');
											},	
											success: function(o) 
											{
												var cst = Ext.decode(o.responseText);
												if (cst.success === true) 
												{
													dsDataGrdJab_viInvPenerimaanBHP.removeAt(line);
													InvPenerimaanBHP.form.Grid.a.getView().refresh();
													Ext.getCmp('btnSimpan_viInvPenerimaanBHP').enable();
													Ext.getCmp('btnSimpanExit_viInvPenerimaanBHP').enable();
													// getDataGridAwalInvPenerimaanBHP();
												}
												else 
												{
													ShowPesanErrorInvPenerimaanBHP('Gagal menghapus data ini', 'Error');
												};
											}
										}
										
									)
								}else{
									dsDataGrdJab_viInvPenerimaanBHP.removeAt(line);
									InvPenerimaanBHP.form.Grid.a.getView().refresh();
									Ext.getCmp('btnSimpan_viInvPenerimaanBHP').enable();
									Ext.getCmp('btnSimpanExit_viInvPenerimaanBHP').enable();
								}
							} 
							
						});
					} else{
						ShowPesanErrorInvPenerimaanBHP('Data tidak bisa dihapus karena minimal barang 1','Error');
					}
					
				}
			}	
		],
	    items:
		[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:
				[
					gridDataViewEdit_viInvPenerimaanBHP(),
					{
						layout: 'column',
						border: false,
						items:
						[
							{
								columnWidth:.99,
								layout: 'absolute',
								bodyStyle: 'padding: 5px 5px 5px 5px',
								border: false,
								width: 510,
								height: 50,
								anchor: '100% 100%',
								items:
								[
									{
										x:5,
										y:5,
										xtype: 'button',
										text: 'Acc. Approval',
										tooltip: 'Approval',
										disabled:true,
										id: 'btnAccAproval_viInvPenerimaanBHP',
										handler: function(sm, row, rec)
										{
											
										}
									},
									{
										x:580,
										y:5,
										xtype: 'label',
										text:'Total'
									},
									{
										x:630,
										y:5,
										xtype: 'label',
										text:':'
									},
									{
										x:640,
										y:5,
										xtype: 'textfield',
										style: 'text-align:right;',
										name: 'txtGrandTotal_InvPenerimaanBHPL',
										id: 'txtGrandTotal_InvPenerimaanBHPL',
										readOnly:true,
										tabIndex:0,
										width: 110
									},
								
								]
							}
						]
					}
				]	
			}
			//-------------- ## --------------
		],
	};
    return items;
};

function gridDataViewEdit_viInvPenerimaanBHP()
{
    var FieldGrdKasir_viInvPenerimaanBHP = [];
	
    dsDataGrdJab_viInvPenerimaanBHP= new WebApp.DataStore
	({
        fields: FieldGrdKasir_viInvPenerimaanBHP
    });
    
    InvPenerimaanBHP.form.Grid.a =new Ext.grid.EditorGridPanel({
        store: dsDataGrdJab_viInvPenerimaanBHP,
        height: 300,//220,
		stripeRows: true,
		columnLines: true,
		selModel: new Ext.grid.CellSelectionModel({
	            singleSelect: true,
	            listeners: {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
        }),
        
        columns: 
		[	
			new Ext.grid.RowNumberer(),
			{			
				dataIndex: 'po_number',
				header: 'Po. Number',
				sortable: true,
				width: 90,
				editor:new Nci.form.Combobox.autoComplete({
					store	: InvPenerimaanBHP.form.ArrayStore.ponumber,
					select	: function(a,b,c){
						var line	= InvPenerimaanBHP.form.Grid.a.getSelectionModel().selection.cell[0];
						dsDataGrdJab_viInvPenerimaanBHP.getRange()[line].data.po_number=b.data.po_number;
						dsDataGrdJab_viInvPenerimaanBHP.getRange()[line].data.kd_inv=b.data.kd_inv;
						dsDataGrdJab_viInvPenerimaanBHP.getRange()[line].data.no_urut_brg=b.data.no_urut_brg;
						dsDataGrdJab_viInvPenerimaanBHP.getRange()[line].data.nama_brg=b.data.nama_brg;
						dsDataGrdJab_viInvPenerimaanBHP.getRange()[line].data.jml_order=b.data.jml_order;
						dsDataGrdJab_viInvPenerimaanBHP.getRange()[line].data.harga=b.data.harga;
						dsDataGrdJab_viInvPenerimaanBHP.getRange()[line].data.jumlah=b.data.jumlah;
						
						Ext.getCmp('btnAdd_viInvPenerimaanBHP').enable();
						Ext.getCmp('btnHapusGridBarang_InvPenerimaanBHP').enable();
						Ext.getCmp('btnSimpan_viInvPenerimaanBHP').enable();
						Ext.getCmp('btnSimpanExit_viInvPenerimaanBHP').enable();
						
						InvPenerimaanBHP.form.Grid.a.getView().refresh();
						hasilJumlahPenerimaanBHP();
					},
					insert	: function(o){
						return {
							po_number       : o.po_number,
							kd_inv 			: o.kd_inv,
							no_urut_brg		: o.no_urut_brg,
							no_urut_brg		: o.no_urut_brg,
							nama_brg		: o.nama_brg,
							jml_order		: o.jml_order,
							harga			: o.harga,
							jumlah      	: o.jumlah,
							text			:  '<table style="font-size: 11px;"><tr><td width="100">'+o.po_number+'</td><td width="180">'+o.nama_brg+'</td></tr></table>'
						}
					},
					param:function(){
						return {
							kdvendor:Ext.getCmp('cbVendorInvPenerimaanBHP').getValue()
						}
					},
					url		: baseURL + "index.php/bhp/functionPenerimaanBHP/getPONumber",
					valueField: 'nama_brg',
					displayField: 'text',
					listWidth: 280
				})
			},
			{			
				dataIndex: 'kd_inv',
				header: 'Kd. Kelompok',
				sortable: true,
				width: 80
			},
			{			
				dataIndex: 'no_urut_brg',
				header: 'No Urut',
				sortable: true,
				width: 80
			},
			{			
				dataIndex: 'nama_brg',
				header: 'Nama Barang',
				sortable: true,
				width: 200,
				editor:new Nci.form.Combobox.autoComplete({
					store	: InvPenerimaanBHP.form.ArrayStore.a,
					select	: function(a,b,c){
						var line	= InvPenerimaanBHP.form.Grid.a.getSelectionModel().selection.cell[0];
						dsDataGrdJab_viInvPenerimaanBHP.getRange()[line].data.po_number=b.data.po_number;
						dsDataGrdJab_viInvPenerimaanBHP.getRange()[line].data.kd_inv=b.data.kd_inv;
						dsDataGrdJab_viInvPenerimaanBHP.getRange()[line].data.no_urut_brg=b.data.no_urut_brg;
						dsDataGrdJab_viInvPenerimaanBHP.getRange()[line].data.nama_brg=b.data.nama_brg;
						dsDataGrdJab_viInvPenerimaanBHP.getRange()[line].data.jml_order=b.data.jml_order;
						dsDataGrdJab_viInvPenerimaanBHP.getRange()[line].data.harga=b.data.harga;
						dsDataGrdJab_viInvPenerimaanBHP.getRange()[line].data.jumlah=b.data.jumlah;
						
						Ext.getCmp('btnAdd_viInvPenerimaanBHP').enable();
						Ext.getCmp('btnHapusGridBarang_InvPenerimaanBHP').enable();
						Ext.getCmp('btnSimpan_viInvPenerimaanBHP').enable();
						Ext.getCmp('btnSimpanExit_viInvPenerimaanBHP').enable();
						
						InvPenerimaanBHP.form.Grid.a.getView().refresh();
						hasilJumlahPenerimaanBHP();
					},
					insert	: function(o){
						return {
							po_number       : o.po_number,
							kd_inv 			: o.kd_inv,
							no_urut_brg		: o.no_urut_brg,
							no_urut_brg		: o.no_urut_brg,
							nama_brg		: o.nama_brg,
							jml_order		: o.jml_order,
							harga			: o.harga,
							jumlah      	: o.jumlah,
							text			:  '<table style="font-size: 11px;"><tr><td width="100">'+o.kd_inv+'</td><td width="180">'+o.nama_brg+'</td></tr></table>'
						}
					},
					url		: baseURL + "index.php/bhp/functionPenerimaanBHP/getBarang",
					valueField: 'nama_brg',
					displayField: 'text',
					listWidth: 280
				})
			},
			{
				dataIndex: 'jml_order',
				header: 'Qty',
				sortable: true,
				align:'right',
				width: 60,
				editor: new Ext.form.NumberField({
					allowBlank: false,
					listeners:{
						blur: function(a){
							var line	= this.index;
							if(a.getValue() == 0){
								ShowPesanWarningInvPenerimaanBHP('qty tidak boleh 0', 'Warning');
							}
							this.index=InvPenerimaanBHP.form.Grid.a.getSelectionModel().selection.cell[0]
							hasilJumlahPenerimaanBHP()
						},
						focus: function(a){
							this.index=InvPenerimaanBHP.form.Grid.a.getSelectionModel().selection.cell[0]
						}
					}
				})	
			},
			{
				dataIndex: 'harga',
				header: 'Harga',
				align:'right',
				xtype:'numbercolumn',
				sortable: true,
				width: 80,
				editor: new Ext.form.NumberField({
					allowBlank: false,
					listeners:{
						blur: function(a){
							var line	= this.index;
							if(a.getValue() == 0){
								ShowPesanWarningInvPenerimaanBHP('Harga tidak boleh 0', 'Warning');
							}
							hasilJumlahPenerimaanBHP()
						},
						focus: function(a){
							this.index=InvPenerimaanBHP.form.Grid.a.getSelectionModel().selection.cell[0]
						}
					}
				})
			},
			{
				dataIndex: 'jumlah',
				header: 'Jumlah',
				align:'right',
				xtype:'numbercolumn',
				sortable: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'no_baris',
				header: 'no_baris',
				align:'right',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
        ],
		viewConfig: 
			{
				forceFit: true
			}

       // plugins:chkSelected_viInvPenerimaanBHP,
    });
    return  InvPenerimaanBHP.form.Grid.a;
}

function hasilJumlahPenerimaanBHP(){
	var grandtotal=0;
	
	for(var i=0; i < dsDataGrdJab_viInvPenerimaanBHP.getCount() ; i++){
		var subJumlah=0;
	
		var o=dsDataGrdJab_viInvPenerimaanBHP.getRange()[i].data;
		if(o.jml_order != undefined){
			subJumlah=parseFloat(o.jml_order) * parseFloat(o.harga);
			o.jumlah=subJumlah;
			
			if(isNaN(o.jumlah)){
				o.jumlah=0;
			} else{
				o.jumlah=o.jumlah;
			}
			
			grandtotal +=subJumlah;
			
			if(isNaN(grandtotal)){
				grandtotal=0;
			} else{
				grandtotal=grandtotal;
			}
		}
	}
	
	Ext.getCmp('txtGrandTotal_InvPenerimaanBHPL').setValue(toFormat(grandtotal));
	
	InvPenerimaanBHP.form.Grid.a.getView().refresh();
}


function comboVendorInvPenerimaanBHP(){
	var Field_Vendor = ['kd_vendor', 'vendor'];
    ds_VendorInvPenerimaanBHP = new WebApp.DataStore({fields: Field_Vendor});
    ds_VendorInvPenerimaanBHP.load({
        params:{
	        Skip: 0,
	        Take: 1000,
	        Sort: 'kd_vendor',
	        Sortdir: 'ASC',
	        target: 'ComboVendorBHP',
	        param: ""
        }
    });
	var cboVendorInvPenerimaanBHP = new Ext.form.ComboBox({
		x:80,
		y:55,
        id:'cbVendorInvPenerimaanBHP',
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:true,
        mode: 'local',
        width: 280,
		readOnly:false,
		fieldLabel:'PBF ',
        store: ds_VendorInvPenerimaanBHP,
        valueField: 'kd_vendor',
        displayField: 'vendor',
        listeners:{
			'select': function(a,b,c){
				Ext.getCmp('btnAddPenerimaanBarang').enable();
				InvPenerimaanBHP.vars.kd_vendor=b.data.kd_vendor;
			}
        }
	});
	return cboVendorInvPenerimaanBHP;
};


function dataaddnew_viInvPenerimaanBHP(){
	Ext.getCmp('txtNoTerima_InvPenerimaanBHPL').setValue('');
	Ext.getCmp('txtNoFaktur_InvPenerimaanBHPL').setValue('');
	Ext.getCmp('dfTglFaktur_InvPenerimaanBHPL').setValue(now_viInvPenerimaanBHP);
	Ext.getCmp('cbVendorInvPenerimaanBHP').setValue('');
	Ext.getCmp('txtKeterangan_InvPenerimaanBHPL').setValue('');
	Ext.getCmp('txtNoSPK_InvPenerimaanBHPL').setValue('');
	Ext.getCmp('dfTglSPK_InvPenerimaanBHPL').setValue(now_viInvPenerimaanBHP);
	Ext.getCmp('cbPosted_InvPenerimaanBHP').setValue(false);
	Ext.getCmp('txtGrandTotal_InvPenerimaanBHPL').setValue('');
	InvPenerimaanBHP.vars.kd_vendor='';
	
	Ext.getCmp('txtNoFaktur_InvPenerimaanBHPL').setReadOnly(false);
	Ext.getCmp('dfTglFaktur_InvPenerimaanBHPL').setReadOnly(false);
	Ext.getCmp('cbVendorInvPenerimaanBHP').setReadOnly(false);
	Ext.getCmp('txtKeterangan_InvPenerimaanBHPL').setReadOnly(false);
	Ext.getCmp('txtNoSPK_InvPenerimaanBHPL').setReadOnly(false);
	Ext.getCmp('dfTglSPK_InvPenerimaanBHPL').setReadOnly(false);
	
	dsDataGrdJab_viInvPenerimaanBHP.removeAll();
}

function datainit_viInvPenerimaanBHP(rowdata)
{
	Ext.getCmp('txtNoTerima_InvPenerimaanBHPL').setValue(rowdata.no_terima);
	Ext.getCmp('txtNoFaktur_InvPenerimaanBHPL').setValue(rowdata.no_faktur);
	Ext.getCmp('dfTglFaktur_InvPenerimaanBHPL').setValue(ShowDate(rowdata.tgl_terima));
	Ext.getCmp('cbVendorInvPenerimaanBHP').setValue(rowdata.vendor);
	Ext.getCmp('txtKeterangan_InvPenerimaanBHPL').setValue(rowdata.keterangan);
	Ext.getCmp('txtNoSPK_InvPenerimaanBHPL').setValue(rowdata.no_spk);
	Ext.getCmp('dfTglSPK_InvPenerimaanBHPL').setValue(ShowDate(rowdata.tgl_spk));
	Ext.getCmp('txtBuktiPenerimaan_InvPenerimaanBHPL').setValue(rowdata.no_penerimaan);
	Ext.getCmp('dfTglPenerimaan_InvPenerimaanBHPL').setValue(ShowDate(rowdata.tgl_penerimaan));
	InvPenerimaanBHP.vars.kd_vendor=rowdata.kd_vendor;
	
	// Ext.getCmp('dfTglFaktur_InvPenerimaanBHPL').setReadOnly(true);
	Ext.getCmp('cbVendorInvPenerimaanBHP').setReadOnly(true);
	Ext.getCmp('txtKeterangan_InvPenerimaanBHPL').setReadOnly(true);
	// Ext.getCmp('dfTglSPK_InvPenerimaanBHPL').setReadOnly(true);
	Ext.getCmp('txtBuktiPenerimaan_InvPenerimaanBHPL').setReadOnly(true);
	Ext.getCmp('dfTglPenerimaan_InvPenerimaanBHPL').setReadOnly(true);
	
	getGridBarangInvPenerimaanBHP(rowdata.no_terima);
	cekStatusPosting(rowdata.no_terima);
	
};

function datasave_viInvPenerimaanBHP(){
	if (ValidasiEntryInvPenerimaanBHP(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/bhp/functionPenerimaanBHP/save",
				params: getParamInvPenerimaanBHP(),
				failure: function(o)
				{
					ShowPesanErrorInvPenerimaanBHP('Error, simpan data! Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoInvPenerimaanBHP('Data berhasil disimpan','Information');
						Ext.getCmp('txtNoTerima_InvPenerimaanBHPL').setValue(cst.noterima);
						// getDataGridAwalInvPenerimaanBHP();
						// Ext.getCmp('btnSimpan_viInvPenerimaanBHP').disable();
						Ext.getCmp('btnSimpanExit_viInvPenerimaanBHP').disable();
						Ext.getCmp('btnHapusPenerimaanBahan_InvPenerimaanBHP').enable();
						Ext.getCmp('btnPosting_viInvPenerimaanBHP').enable();
						Ext.getCmp('txtBuktiPenerimaan_InvPenerimaanBHPL').readOnly(true);
						Ext.getCmp('dfTglPenerimaan_InvPenerimaanBHPL').readOnly(true);
					}
					else 
					{
						ShowPesanErrorInvPenerimaanBHP('Gagal Menyimpan Data ini', 'Error');
					};
				}
			}
			
		)
	}
}

function set_parameter(){
	var params = "";
	params += " t.tgl_terima between '"+Ext.getCmp('dfTglAwalInvPenerimaanBHP').getValue().format("Y-m-d")+"' AND '"+Ext.getCmp('dfTglAkhirInvPenerimaanBHP').getValue().format("Y-m-d")+"'";
	if (Ext.getCmp('TxtFilterGridNoPenerimaanInvPenerimaanBHP').getValue() != "") {
		// params += " AND t.no_terima = '"+Ext.getCmp('TxtFilterGridNoPenerimaanInvPenerimaanBHP').getValue()+"'";
	}

	if (Ext.getCmp('TxtFilterGridPONumberInvPenerimaanBHP').getValue() != "") {
		// params += " AND td.po_number = '"+Ext.getCmp('TxtFilterGridPONumberInvPenerimaanBHP').getValue()+"'";
	}
	return params;
}
function loadDataPenerimaan(){
    dataSource_viInvPenerimaanBHP.load({
        params:{
	        Skip: 0,
	        Take: 1000,
	        Sort: 'kd_vendor',
	        Sortdir: 'ASC',
	        target: 'InvPenerimaanBarang',
	        param: set_parameter()
        }
    });
    // GridDataView_viInvPenerimaanBHP.getView().refresh();
}
function getDataGridAwalInvPenerimaanBHP(noterima,ponumber,tglawal,tglakhir){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/bhp/functionPenerimaanBHP/getGridAwal",
			params: {
				noterima:noterima,
				ponumber:ponumber,
				tglawal:tglawal,
				tglakhir:tglakhir
			},
			failure: function(o)
			{
				ShowPesanErrorInvPenerimaanBHP('Error, membaca grid awal! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					dataSource_viInvPenerimaanBHP.removeAll();
					
					var recs=[],
						recType=dataSource_viInvPenerimaanBHP.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
						dataSource_viInvPenerimaanBHP.add(recs);
					
					GridDataView_viInvPenerimaanBHP.getView().refresh();
				}
				else 
				{
					ShowPesanErrorInvPenerimaanBHP('Gagal membaca data obat', 'Error');
				};
			}
		}
		
	)
	
}


function getGridBarangInvPenerimaanBHP(no_terima){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/bhp/functionPenerimaanBHP/getGridBarangLoad",
			params: {no_terima:no_terima},
			failure: function(o)
			{
				ShowPesanErrorInvPenerimaanBHP('Error, membaca grid bahan! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dsDataGrdJab_viInvPenerimaanBHP.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
						dsDataGrdJab_viInvPenerimaanBHP.add(recs);
					
					InvPenerimaanBHP.form.Grid.a.getView().refresh();
					hasilJumlahPenerimaanBHP();
				}
				else 
				{
					ShowPesanErrorInvPenerimaanBHP('Gagal membaca data barang', 'Error');
				};
			}
		}
		
	)
	
}

function dataposting_viInvPenerimaanBHP(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/bhp/functionPenerimaanBHP/updatePosting",
			params: getParamPostingInvPenerimaanBHP(),
			failure: function(o)
			{
				ShowPesanErrorInvPenerimaanBHP('Error, posting! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) {
					ShowPesanInfoInvPenerimaanBHP('Posting berhasil', 'Information');
					// Ext.getCmp('btnSimpan_viInvPenerimaanBHP').disable();
					Ext.getCmp('btnSimpanExit_viInvPenerimaanBHP').disable();
					Ext.getCmp('btnHapusPenerimaanBahan_InvPenerimaanBHP').disable();
					Ext.getCmp('btnAddPenerimaanBarang').disable();
					Ext.getCmp('btnHapusGridBarang_InvPenerimaanBHP').disable();
					Ext.getCmp('btnPosting_viInvPenerimaanBHP').disable();
					getDataGridAwalInvPenerimaanBHP();
					Ext.getCmp('btnUnPosting_viInvPenerimaanBHP').enable();
					// Ext.getCmp('dfTglFaktur_InvPenerimaanBHPL').setReadOnly(true);
					// Ext.getCmp('dfTglSPK_InvPenerimaanBHPL').setReadOnly(true);
					// Ext.getCmp('txtNoFaktur_InvPenerimaanBHPL').setReadOnly(true);
					// Ext.getCmp('txtNoSPK_InvPenerimaanBHPL').setReadOnly(true);
					InvPenerimaanBHP.form.Grid.a.disable();
					Ext.getCmp('txtKeterangan_InvPenerimaanBHPL').setReadOnly(true);
				}else {
					InvPenerimaanBHP.form.Grid.a.enable();
					// Ext.getCmp('dfTglFaktur_InvPenerimaanBHPL').setReadOnly(false);
					// Ext.getCmp('dfTglSPK_InvPenerimaanBHPL').setReadOnly(false);
					// Ext.getCmp('txtNoFaktur_InvPenerimaanBHPL').setReadOnly(false);
					// Ext.getCmp('txtNoSPK_InvPenerimaanBHPL').setReadOnly(false);
					Ext.getCmp('txtKeterangan_InvPenerimaanBHPL').setReadOnly(false);
					Ext.getCmp('btnUnPosting_viInvPenerimaanBHP').disable();
					ShowPesanWarningInvPenerimaanBHP('Gagal diPosting', 'Warning');
					Ext.getCmp('btnAddPenerimaanBarang').enable();
					Ext.getCmp('btnHapusGridBarang_InvPenerimaanBHP').enable();
					// Ext.getCmp('btnSimpan_viInvPenerimaanBHP').enable();
					Ext.getCmp('btnSimpanExit_viInvPenerimaanBHP').enable();
					Ext.getCmp('btnHapusPenerimaanBahan_InvPenerimaanBHP').enable();
					Ext.getCmp('btnPosting_viInvPenerimaanBHP').enable();
				};
			}
		}
		
	)
}

function cekStatusPosting(no_terima){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/bhp/functionPenerimaanBHP/cekPosting",
			params: {no_terima:no_terima},
			failure: function(o)
			{
				ShowPesanErrorInvPenerimaanBHP('Error, cek! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					ShowPesanInfoInvPenerimaanBHP('Penerimaan ini sudah diPosting', 'Information');
					// Ext.getCmp('btnSimpan_viInvPenerimaanBHP').disable();
					Ext.getCmp('btnSimpanExit_viInvPenerimaanBHP').disable();
					Ext.getCmp('btnHapusPenerimaanBahan_InvPenerimaanBHP').disable();
					Ext.getCmp('btnAddPenerimaanBarang').disable();
					Ext.getCmp('btnHapusGridBarang_InvPenerimaanBHP').disable();
					Ext.getCmp('btnPosting_viInvPenerimaanBHP').disable();
					Ext.getCmp('btnUnPosting_viInvPenerimaanBHP').enable();
					Ext.getCmp('btnPosting_viInvPenerimaanBHP').disable();
					Ext.getCmp('cbPosted_InvPenerimaanBHP').setValue(true);
					InvPenerimaanBHP.form.Grid.a.disable();
					// Ext.getCmp('txtNoFaktur_InvPenerimaanBHPL').setReadOnly(true);
					// Ext.getCmp('txtNoSPK_InvPenerimaanBHPL').setReadOnly(true);
					// Ext.getCmp('dfTglFaktur_InvPenerimaanBHPL').setReadOnly(true);
					// Ext.getCmp('dfTglSPK_InvPenerimaanBHPL').setReadOnly(true);
					Ext.getCmp('txtKeterangan_InvPenerimaanBHPL').setReadOnly(true);
				}else {
					Ext.getCmp('txtKeterangan_InvPenerimaanBHPL').setReadOnly(false);
					InvPenerimaanBHP.form.Grid.a.enable();
					// Ext.getCmp('txtNoFaktur_InvPenerimaanBHPL').setReadOnly(false);
					// Ext.getCmp('txtNoSPK_InvPenerimaanBHPL').setReadOnly(false);
					// Ext.getCmp('dfTglFaktur_InvPenerimaanBHPL').setReadOnly(false);
					// Ext.getCmp('dfTglSPK_InvPenerimaanBHPL').setReadOnly(false);
					Ext.getCmp('btnUnPosting_viInvPenerimaanBHP').disable();
					Ext.getCmp('btnAddPenerimaanBarang').enable();
					Ext.getCmp('btnHapusGridBarang_InvPenerimaanBHP').enable();
					// Ext.getCmp('btnSimpan_viInvPenerimaanBHP').enable();
					Ext.getCmp('btnSimpanExit_viInvPenerimaanBHP').enable();
					Ext.getCmp('btnHapusPenerimaanBahan_InvPenerimaanBHP').enable();
					Ext.getCmp('btnPosting_viInvPenerimaanBHP').enable();
					Ext.getCmp('btnPosting_viInvPenerimaanBHP').enable();
					Ext.getCmp('cbPosted_InvPenerimaanBHP').setValue(false);
				};
			}
		}
		
	)
}

function getParamInvPenerimaanBHP() 
{
	var	params =
	{
		NoTerima 		: Ext.getCmp('txtNoTerima_InvPenerimaanBHPL').getValue(),
		KdVendor 		: InvPenerimaanBHP.vars.kd_vendor,
		no_penerimaan 	: Ext.getCmp('txtBuktiPenerimaan_InvPenerimaanBHPL').getValue(),
		tgl_penerimaan	: Ext.getCmp('dfTglPenerimaan_InvPenerimaanBHPL').getValue(),
		Total 			: toInteger(Ext.getCmp('txtGrandTotal_InvPenerimaanBHPL').getValue()),
		NoFaktur 		: Ext.getCmp('txtNoFaktur_InvPenerimaanBHPL').getValue(),
		NoSpk 			: Ext.getCmp('txtNoSPK_InvPenerimaanBHPL').getValue(),
		TglTerima 		: Ext.getCmp('dfTglFaktur_InvPenerimaanBHPL').getValue(),
		TglSpk 			: Ext.getCmp('dfTglSPK_InvPenerimaanBHPL').getValue(),
		Keterangan 		: Ext.getCmp('txtKeterangan_InvPenerimaanBHPL').getValue(),
	}
	
	params['jumlah']=dsDataGrdJab_viInvPenerimaanBHP.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viInvPenerimaanBHP.getCount();i++)
	{
		params['po_number-'+i]=dsDataGrdJab_viInvPenerimaanBHP.data.items[i].data.po_number
		params['no_urut_brg-'+i]=dsDataGrdJab_viInvPenerimaanBHP.data.items[i].data.no_urut_brg
		params['jml_order-'+i]=dsDataGrdJab_viInvPenerimaanBHP.data.items[i].data.jml_order
		params['harga-'+i]=dsDataGrdJab_viInvPenerimaanBHP.data.items[i].data.harga
		params['no_baris-'+i]=dsDataGrdJab_viInvPenerimaanBHP.data.items[i].data.no_baris
	}
    return params
};

function getParamPostingInvPenerimaanBHP() 
{
	var	params =
	{
		NoTerima 	: Ext.getCmp('txtNoTerima_InvPenerimaanBHPL').getValue(),
		tgl_spk 	: Ext.getCmp('dfTglSPK_InvPenerimaanBHPL').getValue(),
		tgl_terima 	: Ext.getCmp('dfTglFaktur_InvPenerimaanBHPL').getValue(),
		no_faktur 	: Ext.getCmp('txtNoFaktur_InvPenerimaanBHPL').getValue(),
		no_spk 		: Ext.getCmp('txtNoSPK_InvPenerimaanBHPL').getValue(),
	}
	
	params['jumlah']=dsDataGrdJab_viInvPenerimaanBHP.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viInvPenerimaanBHP.getCount();i++)
	{
		params['no_urut_brg-'+i]=dsDataGrdJab_viInvPenerimaanBHP.data.items[i].data.no_urut_brg
		params['jml_order-'+i]=dsDataGrdJab_viInvPenerimaanBHP.data.items[i].data.jml_order
	}
    return params
};



function ValidasiEntryInvPenerimaanBHP(modul,mBolHapus)
{
	var x = 1;
	if(Ext.getCmp('cbVendorInvPenerimaanBHP').getValue() === ''){
		ShowPesanWarningInvPenerimaanBHP('Vendor tidak boleh kosong', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('txtNoFaktur_InvPenerimaanBHPL').getValue() === ''){
		ShowPesanWarningInvPenerimaanBHP('No Faktur tidak boleh kosong', 'Warning');
		x = 0;
	}
	if(dsDataGrdJab_viInvPenerimaanBHP.getCount() < 0){
		ShowPesanWarningInvPenerimaanBHP('Daftar Penerimaan bahan tidak boleh kosong, minimal bahan 1', 'Warning');
		x = 0;
	}
	
	for(var i=0; i<dsDataGrdJab_viInvPenerimaanBHP.getCount() ; i++){
		var o=dsDataGrdJab_viInvPenerimaanBHP.getRange()[i].data;
		if(o.jml_order == undefined || o.jml_order <= 0){
			ShowPesanWarningInvPenerimaanBHP('Qty tidak boleh 0, minimal qty 1', 'Warning');
			x = 0;
		}
		for(var j=i+1; j<dsDataGrdJab_viInvPenerimaanBHP.getCount() ; j++){
			var p=dsDataGrdJab_viInvPenerimaanBHP.getRange()[j].data;
			if(o.po_number == p.po_number && o.no_urut_brg == p.no_urut_brg){
				ShowPesanWarningInvPenerimaanBHP('Barang dengan Po. Number yang sama tidak boleh berulang, periksa kembali daftar pernerimaan barang', 'Warning');
				x = 0;
				break;
			}
		}
	}
	
	return x;
};



function ShowPesanWarningInvPenerimaanBHP(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorInvPenerimaanBHP(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};


function ShowPesanInfoInvPenerimaanBHP(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};