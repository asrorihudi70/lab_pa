var dataSource_viInvPerencanaanBHP;
var selectCount_viInvPerencanaanBHP=50;
var NamaForm_viInvPerencanaanBHP="Perencanaan Bahan Habis Pakai";
var mod_name_viInvPerencanaanBHP="viInvPerencanaanBHP";
var now_viInvPerencanaanBHP= new Date();
var rowSelected_viInvPerencanaanBHP;
var setLookUps_viInvPerencanaanBHP;
var tanggal = now_viInvPerencanaanBHP.format("d/M/Y");
var jam = now_viInvPerencanaanBHP.format("H/i/s");
var tmpkriteria;
var GridDataView_viInvPerencanaanBHP;
var cRONumber_viInvPerencanaanBHP;
var cTglAwal_viInvPerencanaanBHP;
var cTglAkhir_viInvPerencanaanBHP;


var CurrentData_viInvPerencanaanBHP =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viInvPerencanaanBHP(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

var InvPerencanaanBHP={};
InvPerencanaanBHP.form={};
InvPerencanaanBHP.func={};
InvPerencanaanBHP.vars={};
InvPerencanaanBHP.func.parent=InvPerencanaanBHP;
InvPerencanaanBHP.form.ArrayStore={};
InvPerencanaanBHP.form.ComboBox={};
InvPerencanaanBHP.form.DataStore={};
InvPerencanaanBHP.form.Record={};
InvPerencanaanBHP.form.Form={};
InvPerencanaanBHP.form.Grid={};
InvPerencanaanBHP.form.Panel={};
InvPerencanaanBHP.form.TextField={};
InvPerencanaanBHP.form.Button={};

InvPerencanaanBHP.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['req_number','req_date','no_urut_brg','nama_brg',
			'qty','remark','ordered','stok','req_ket','req_line','satuan','kd_inv'],
	data: []
});

function dataGrid_viInvPerencanaanBHP(mod_id_viInvPerencanaanBHP){	
    var FieldMaster_viInvPerencanaanBHP = 
	[
		'no_minta','tgl_minta', 'jenis_minta','jenis'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viInvPerencanaanBHP = new WebApp.DataStore
	({
        fields: FieldMaster_viInvPerencanaanBHP
    });
    getDataGridAwalviInvPerencanaanBHP();
    // Grid Gizi Perencanaan # --------------
	GridDataView_viInvPerencanaanBHP = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: 'Daftar Perencanaan',
			store: dataSource_viInvPerencanaanBHP,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viInvPerencanaanBHP = undefined;
							rowSelected_viInvPerencanaanBHP = dataSource_viInvPerencanaanBHP.getAt(row);
							CurrentData_viInvPerencanaanBHP
							CurrentData_viInvPerencanaanBHP.row = row;
							CurrentData_viInvPerencanaanBHP.data = rowSelected_viInvPerencanaanBHP.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viInvPerencanaanBHP = dataSource_viInvPerencanaanBHP.getAt(ridx);
					if (rowSelected_viInvPerencanaanBHP != undefined)
					{
						setLookUp_viInvPerencanaanBHP(rowSelected_viInvPerencanaanBHP.data);
					}
					else
					{
						setLookUp_viInvPerencanaanBHP();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Gizi perencanaan
	        *	Terdiri dari : Judul, Isi dan Event
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header: 'RO. Number',
						dataIndex: 'req_number',
						hideable:false,
						menuDisabled: true,
						width: 25
						
					},
					//-------------- ## --------------
					{
						header: 'Tanggal',
						dataIndex: 'req_date',
						hideable:false,
						menuDisabled: true,
						width: 30,
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.req_date);
						}
					},
					{
						header: 'No Urut Barang',
						dataIndex: 'no_urut_brg',
						hideable:false,
						menuDisabled: true,
						width: 30
					},
					{
						header: 'Nama Barang',
						dataIndex: 'nama_brg',
						hideable:false,
						menuDisabled: true,
						width: 50
					},
					{
						header: 'Qty',
						dataIndex: 'qty',
						hideable:false,
						menuDisabled: true,
						align:'right',
						width: 30
					},
					{
						header: 'Keterangan',
						dataIndex: 'remark',
						hideable:false,
						menuDisabled: true,
						width: 50
					},
					//-------------- ## --------------
					
					//-------------- ## --------------
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viInvPerencanaanBHP',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add Perencanaan',
						iconCls: 'Edit_Tr',
						tooltip: 'Add Data',
						id: 'btnTambah_viInvPerencanaanBHP',
						handler: function(sm, row, rec)
						{
							
							setLookUp_viInvPerencanaanBHP();
						}
					},
					{
						xtype: 'button',
						text: 'Edit Perencanaan',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viInvPerencanaanBHP',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viInvPerencanaanBHP != undefined)
							{
								setLookUp_viInvPerencanaanBHP(rowSelected_viInvPerencanaanBHP.data)
							}
							
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viInvPerencanaanBHP, selectCount_viInvPerencanaanBHP, dataSource_viInvPerencanaanBHP),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )
	
	var pencarianInvPerencanaanBHP = new Ext.FormPanel({
        labelAlign: 'top',
        frame:true,
        title: '',
        bodyStyle:'padding:5px 5px 0',
        items: [
		{
			layout: 'column',
			border: false,
			items:
			[
				{
					columnWidth:.98,
					layout: 'absolute',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					width: 500,
					height: 70,
					anchor: '100% 100%',
					items:
					[
						{
							x: 10,
							y: 0,
							xtype: 'label',
							text: 'Ro. Number'
						},
						{
							x: 120,
							y: 0,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 0,
							xtype: 'textfield',
							id: 'TxtFilterGridRONumberInvPerencanaanBHP',
							name: 'TxtFilterGridRONumberInvPerencanaanBHP',
							width: 350,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										cRONumber_viInvPerencanaanBHP=Ext.getCmp('TxtFilterGridRONumberInvPerencanaanBHP').getValue();
										cTglAwal_viInvPerencanaanBHP=Ext.getCmp('dfTglAwalInvPerencanaanBHP').getValue();
										cTglAkhir_viInvPerencanaanBHP=Ext.getCmp('dfTglAkhirInvPerencanaanBHP').getValue();
										getDataGridAwalviInvPerencanaanBHP(cRONumber_viInvPerencanaanBHP,cTglAwal_viInvPerencanaanBHP,cTglAkhir_viInvPerencanaanBHP);
									} 						
								}
							}
						},
						//----------------------------------------
						{
							x: 483,
							y: 0,
							xtype: 'label',
							text: '* Enter untuk mencari'
						},
						//----------------------------------------
						{
							x: 580,
							y: 30,
							xtype: 'button',
							text: 'Refresh',
							iconCls: 'refresh',
							tooltip: 'Refresh',
							style:{paddingLeft:'30px'},
							width:150,
							id: 'BtnFilterGridCari_viInvPerencanaanBHP',
							handler: function() 
							{					
								getDataGridAwalviInvPerencanaanBHP();
							}                        
						},
						{
							x: 10,
							y: 30,
							xtype: 'label',
							text: 'Tanggal'
						},
						{
							x: 120,
							y: 30,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 30,
							xtype: 'datefield',
							id: 'dfTglAwalInvPerencanaanBHP',
							format: 'd/M/Y',
							value:now_viInvPerencanaanBHP,
							width: 150,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										cRONumber_viInvPerencanaanBHP=Ext.getCmp('TxtFilterGridRONumberInvPerencanaanBHP').getValue();
										cTglAwal_viInvPerencanaanBHP=Ext.getCmp('dfTglAwalInvPerencanaanBHP').getValue();
										cTglAkhir_viInvPerencanaanBHP=Ext.getCmp('dfTglAkhirInvPerencanaanBHP').getValue();
										getDataGridAwalviInvPerencanaanBHP(cRONumber_viInvPerencanaanBHP,cTglAwal_viInvPerencanaanBHP,cTglAkhir_viInvPerencanaanBHP);
									} 						
								}
							}
						},
						{
							x: 290,
							y: 30,
							xtype: 'label',
							text: 's/d'
						},
						{
							x: 320,
							y: 30,
							xtype: 'datefield',
							id: 'dfTglAkhirInvPerencanaanBHP',
							format: 'd/M/Y',
							value:now_viInvPerencanaanBHP,
							width: 150,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										cRONumber_viInvPerencanaanBHP=Ext.getCmp('TxtFilterGridRONumberInvPerencanaanBHP').getValue();
										cTglAwal_viInvPerencanaanBHP=Ext.getCmp('dfTglAwalInvPerencanaanBHP').getValue();
										cTglAkhir_viInvPerencanaanBHP=Ext.getCmp('dfTglAkhirInvPerencanaanBHP').getValue();
										getDataGridAwalviInvPerencanaanBHP(cRONumber_viInvPerencanaanBHP,cTglAwal_viInvPerencanaanBHP,cTglAkhir_viInvPerencanaanBHP);
									} 						
								}
							}
						}
					]
				}
			]
		}
		]	
		
	})

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viInvPerencanaanBHP = new Ext.Panel
    (
		{
			title: NamaForm_viInvPerencanaanBHP,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viInvPerencanaanBHP,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ pencarianInvPerencanaanBHP,
					GridDataView_viInvPerencanaanBHP],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viInvPerencanaanBHP,
		            columns: 21,
		            defaults: {
					scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viInvPerencanaanBHP;
    //-------------- # End form filter # --------------
}


function setLookUp_viInvPerencanaanBHP(rowdata){
    var lebar = 785;
    setLookUps_viInvPerencanaanBHP = new Ext.Window({
        id: Nci.getId(),
        title: NamaForm_viInvPerencanaanBHP, 
        closeAction: 'destroy',        
        width: 750,
        height: 555,
		constrain:true,
		autoHeight:true,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viInvPerencanaanBHP(lebar,rowdata),
        listeners:{
            activate: function(){
				
            },
            afterShow: function(){
                this.activate();
            },
            deactivate: function(){
                rowSelected_viInvPerencanaanBHP=undefined;
            }
        }
    });

    setLookUps_viInvPerencanaanBHP.show();

    if (rowdata == undefined){
	
    }
    else
    {
        datainit_viInvPerencanaanBHP(rowdata);
    }
}

function getFormItemEntry_viInvPerencanaanBHP(lebar,rowdata){
    var pnlFormDataBasic_viInvPerencanaanBHP = new Ext.FormPanel({
		title: '',
		region: 'north',
		layout: 'form',
		bodyStyle: 'padding:10px 10px 10px 10px',
		anchor: '100%',
		labelWidth: 1,
		autoWidth: true,
		width: lebar,
		border: false,
		items:[
				getItemPanelInputPerencanaan_viInvPerencanaanBHP(lebar),
				getItemGridPerencanaan_viInvPerencanaanBHP(lebar)
			],
			fileUpload: true,
		tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAdd_viInvPerencanaanBHP',
						handler: function(){
							dataaddnew_viInvPerencanaanBHP();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						disabled: true,
						id: 'btnSimpan_viInvPerencanaanBHP',
						handler: function()
						{
							datasave_viInvPerencanaanBHP();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save & Close',
						disabled: true,
						iconCls: 'saveexit',
						id: 'btnSimpanExit_viInvPerencanaanBHP',
						handler: function()
						{
							datasave_viInvPerencanaanBHP();
							refreshInvPerencanaanBHP();
							setLookUps_viInvPerencanaanBHP.close();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						text	: 'Delete',
						id		: 'btnHapusPerencanaanBarang_InvPerencanaanBHP',
						tooltip	: nmLookup,
						iconCls	: 'remove',
						disabled: true,
						handler	: function(){
							Ext.Msg.confirm('Hapus perencanaan', 'Apakah perencanaan ini akan dihapus?', function(button){
								if (button == 'yes'){
									Ext.Ajax.request
									(
										{
											url: baseURL + "index.php/bhp/functionPerencanaanBHP/hapusPerencanaan",
											params:{req_number:Ext.getCmp('txtRONumber_InvPerencanaanBHPL').getValue()} ,
											failure: function(o)
											{
												ShowPesanErrorInvPerencanaanBHP('Error, hapus perencanaan! Hubungi Admin', 'Error');
											},	
											success: function(o) 
											{
												var cst = Ext.decode(o.responseText);
												if (cst.success === true) 
												{
													getDataGridAwalviInvPerencanaanBHP();
													setLookUps_viInvPerencanaanBHP.close();
												}
												else 
												{
													ShowPesanErrorInvPerencanaanBHP('Gagal menghapus perencannan ini', 'Error');
												};
											}
										}
										
									)
								}
							});		
						}
					},
					{
						xtype: 'tbseparator'
					},
					
				]
			}
		}
    )

    return pnlFormDataBasic_viInvPerencanaanBHP;
}

function getItemPanelInputPerencanaan_viInvPerencanaanBHP(lebar) {
    var items =
	{
		title:'',
		layout:'column',
		items:
		[
			{
				columnWidth: .50,
				layout: 'form',
				labelWidth:90,
				bodyStyle:'padding: 10px',
				border: false,
				items:
				[
					{
						xtype: 'textfield',
						fieldLabel: 'Ro. Number ',
						name: 'txtRONumber_InvPerencanaanBHPL',
						id: 'txtRONumber_InvPerencanaanBHPL',
						readOnly:true,
						tabIndex:0,
						width: 150
					},
					{
						xtype: 'datefield',
						fieldLabel: 'Ro. Tanggal ',
						name: 'dfTglRo_InvPerencanaanBHPL',
						id	: 'dfTglRo_InvPerencanaanBHPL',
						format: 'd/M/Y',
						value:now_viInvPerencanaanBHP,
						tabIndex:1,
						//readOnly:true,
						width: 150
					}
				]
			},
			{
				columnWidth: .50,
				layout: 'form',
				labelWidth:80,
				bodyStyle:'padding: 10px',
				border: false,
				items:
				[
					{
						xtype: 'textarea',
						fieldLabel: 'Keterangan ',
						name: 'txtKeterangan_InvPerencanaanBHPL',
						id: 'txtKeterangan_InvPerencanaanBHPL',
						width: 170,
						height: 60
					}
				]
			}
		
		]
	};
    return items;
};

function getItemGridPerencanaan_viInvPerencanaanBHP(lebar) 
{
    var items =
	{
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:1px 1px 1px 1px',
		border:true,
		width: lebar-80,
		height: 250,
		tbar:
		[
			{
				text	: 'Add Barang',
				id		: 'btnAddPerencanaanBarang',
				tooltip	: nmLookup,
				iconCls	: 'find',
				handler	: function(){
					var records = new Array();
					records.push(new dsDataGrdJab_viInvPerencanaanBHP.recordType());
					dsDataGrdJab_viInvPerencanaanBHP.add(records);
				}
			},	
			{
				text	: 'Delete',
				id		: 'btnHapusGridBarang_InvPerencanaanBHP',
				tooltip	: nmLookup,
				iconCls	: 'remove',
				//disabled: true,
				handler: function()
				{
					var line =  InvPerencanaanBHP.form.Grid.a.getSelectionModel().selection.cell[0];
					var o = dsDataGrdJab_viInvPerencanaanBHP.getRange()[line].data;
					if(dsDataGrdJab_viInvPerencanaanBHP.getCount() > 1){
						Ext.Msg.confirm('Warning', 'Apakah barang ini akan dihapus?', function(button){
							if (button == 'yes'){
								if(dsDataGrdJab_viInvPerencanaanBHP.getRange()[line].data.req_line != undefined){
									Ext.Ajax.request
									(
										{
											url: baseURL + "index.php/bhp/functionPerencanaanBHP/hapusBarisGridBarang",
											params: {
												req_number:Ext.getCmp('txtRONumber_InvPerencanaanBHPL').getValue(),
												no_urut_brg:o.no_urut_brg,
												req_line:o.req_line
											},
											failure: function(o)
											{
												ShowPesanErrorInvPerencanaanBHP('Error, hapus barang! Hubungi Admin', 'Error');
											},	
											success: function(o) 
											{
												var cst = Ext.decode(o.responseText);
												if (cst.success === true) 
												{
													dsDataGrdJab_viInvPerencanaanBHP.removeAt(line);
													InvPerencanaanBHP.form.Grid.a.getView().refresh();
													Ext.getCmp('btnSimpan_viInvPerencanaanBHP').enable();
													Ext.getCmp('btnSimpanExit_viInvPerencanaanBHP').enable();
													Ext.getCmp('btnHapusPerencanaanBarang_InvPerencanaanBHP').enable();
													getDataGridAwalviInvPerencanaanBHP();
												}
												else 
												{
													ShowPesanErrorInvPerencanaanBHP('Gagal menghapus data ini', 'Error');
												};
											}
										}
										
									)
								}else{
									dsDataGrdJab_viInvPerencanaanBHP.removeAt(line);
									InvPerencanaanBHP.form.Grid.a.getView().refresh();
									Ext.getCmp('btnSimpan_viInvPerencanaanBHP').enable();
									Ext.getCmp('btnSimpanExit_viInvPerencanaanBHP').enable();
									Ext.getCmp('btnHapusPerencanaanBarang_InvPerencanaanBHP').enable();
								}
							} 
							
						});
					} else{
						ShowPesanErrorInvPerencanaanBHP('Data tidak bisa dihapus karena minimal 1 barang','Error');
					}
					
				}
			}	
		],
	    items:
		[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:
				[
					gridDataViewEdit_viInvPerencanaanBHP()
				]	
			}
			//-------------- ## --------------
		]
	};
    return items;
};

function gridDataViewEdit_viInvPerencanaanBHP()
{
    var FieldGrdKasir_viInvPerencanaanBHP = [];
	
    dsDataGrdJab_viInvPerencanaanBHP= new WebApp.DataStore
	({
        fields: FieldGrdKasir_viInvPerencanaanBHP
    });
    
    InvPerencanaanBHP.form.Grid.a =new Ext.grid.EditorGridPanel({
        store: dsDataGrdJab_viInvPerencanaanBHP,
        height: 220,//220,
		stripeRows: true,
		columnLines: true,
		selModel: new Ext.grid.CellSelectionModel({
	            singleSelect: true,
	            listeners: {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
        }),
        
        columns: 
		[	
			new Ext.grid.RowNumberer(),
			{			
				dataIndex: 'kd_inv',
				header: 'Kode Kelompok',
				sortable: true,
				width: 100
			},
			//-------------- ## --------------
			{			
				dataIndex: 'no_urut_brg',
				header: 'No Urut',
				sortable: true,
				width: 80
			},
			//-------------- ## --------------
			{			
				dataIndex: 'nama_brg',
				header: 'Nama barang',
				sortable: true,
				width: 200,
				editor:new Nci.form.Combobox.autoComplete({
					store	: InvPerencanaanBHP.form.ArrayStore.a,
					select	: function(a,b,c){
						var line	= InvPerencanaanBHP.form.Grid.a.getSelectionModel().selection.cell[0];
						dsDataGrdJab_viInvPerencanaanBHP.getRange()[line].data.kd_inv=b.data.kd_inv;
						dsDataGrdJab_viInvPerencanaanBHP.getRange()[line].data.no_urut_brg=b.data.no_urut_brg;
						dsDataGrdJab_viInvPerencanaanBHP.getRange()[line].data.nama_brg=b.data.nama_brg;
						dsDataGrdJab_viInvPerencanaanBHP.getRange()[line].data.kd_satuan=b.data.kd_satuan;
						dsDataGrdJab_viInvPerencanaanBHP.getRange()[line].data.satuan=b.data.satuan;
						dsDataGrdJab_viInvPerencanaanBHP.getRange()[line].data.qty=b.data.qty;
						dsDataGrdJab_viInvPerencanaanBHP.getRange()[line].data.stok=b.data.stok;
						dsDataGrdJab_viInvPerencanaanBHP.getRange()[line].data.ordered=b.data.ordered;
						dsDataGrdJab_viInvPerencanaanBHP.getRange()[line].data.req_line=b.data.req_line;
						
						Ext.getCmp('btnAdd_viInvPerencanaanBHP').enable();
						Ext.getCmp('btnHapusGridBarang_InvPerencanaanBHP').enable();
						Ext.getCmp('btnSimpan_viInvPerencanaanBHP').enable();
						Ext.getCmp('btnSimpanExit_viInvPerencanaanBHP').enable();
						
						InvPerencanaanBHP.form.Grid.a.getView().refresh();
					},
					insert	: function(o){
						return {
							kd_inv       	: o.kd_inv,
							nama_brg 		: o.nama_brg,
							no_urut_brg		: o.no_urut_brg,
							satuan			: o.satuan,
							qty				: o.qty,
							ordered			: o.ordered,
							stok			: o.stok,
							req_line		: o.req_line,
							text			:  '<table style="font-size: 11px;"><tr><td width="80">'+o.kd_inv+'</td><td width="60">'+o.no_urut_brg+'</td><td width="190">'+o.nama_brg+'</td></tr></table>'
						}
					},
					url		: baseURL + "index.php/bhp/functionPerencanaanBHP/getBarang",
					valueField: 'nama_brg',
					displayField: 'text',
					listWidth: 330
				})
			},
			//-------------- ## --------------
			{
				dataIndex: 'satuan',
				header: 'Satuan',
				sortable: true,
				width: 70
			},
			//-------------- ## --------------
			{
				dataIndex: 'qty',
				header: 'Qty',
				sortable: true,
				align:'right',
				width: 70,
				editor: new Ext.form.NumberField({
					allowBlank: false,
					listeners:{
						blur: function(a){
							/* var line	= this.index;
							dsDataGrdJab_viInvPerencanaanBHP.getRange()[line].data.stok_opname=a.getValue(); */
							var line	= this.index;
							if(a.getValue() == 0){
								ShowPesanWarningInvPerencanaanBHP('qty tidak boleh 0', 'Warning');
							}
						},
						focus: function(a){
							this.index=InvPerencanaanBHP.form.Grid.a.getSelectionModel().selection.cell[0]
						}
					}
				})	
			},
			//-------------- ## --------------
			{
				dataIndex: 'stok',
				header: 'Stok',
				sortable: true,
				align:'right',
				width: 80,
				editor: new Ext.form.NumberField({
					allowBlank: false,
					listeners:{
						blur: function(a){
							var line	= this.index;
							/* if(a.getValue() == 0){
								ShowPesanWarningInvPerencanaanBHP('Stok tidak boleh 0', 'Warning');
							} */
						},
						focus: function(a){
							this.index=InvPerencanaanBHP.form.Grid.a.getSelectionModel().selection.cell[0]
						}
					}
				})	
			},
			{
				dataIndex: 'ordered',
				header: 'Ordered',
				sortable: true,
				align:'right',
				width: 80	
			},
			//-------------- ## --------------
			{
				dataIndex: 'kd_satuan',
				header: 'kd_satuan',
				hidden: true,
				width: 80
			},
			{
				dataIndex: 'req_line',
				header: 'urut',
				hidden: true,
				width: 80
			}
			//-------------- ## --------------
        ],
		viewConfig: 
			{
				forceFit: true
			}

       // plugins:chkSelected_viInvPerencanaanBHP,
    });
    return  InvPerencanaanBHP.form.Grid.a;
}

function dataaddnew_viInvPerencanaanBHP(){
	Ext.getCmp('txtRONumber_InvPerencanaanBHPL').setValue('');
	Ext.getCmp('dfTglRo_InvPerencanaanBHPL').setValue(now_viInvPerencanaanBHP);
	Ext.getCmp('txtKeterangan_InvPerencanaanBHPL').setValue('');
	
	dsDataGrdJab_viInvPerencanaanBHP.removeAll();
	
	Ext.getCmp('dfTglRo_InvPerencanaanBHPL').setReadOnly(false);
	Ext.getCmp('txtKeterangan_InvPerencanaanBHPL').setReadOnly(false);
}

function datainit_viInvPerencanaanBHP(rowdata)
{
	Ext.getCmp('txtRONumber_InvPerencanaanBHPL').setValue(rowdata.req_number);
	Ext.getCmp('dfTglRo_InvPerencanaanBHPL').setValue(ShowDate(rowdata.req_date));
	Ext.getCmp('txtKeterangan_InvPerencanaanBHPL').setValue(rowdata.remark);
	
	Ext.getCmp('txtRONumber_InvPerencanaanBHPL').setReadOnly(true);
	Ext.getCmp('dfTglRo_InvPerencanaanBHPL').setReadOnly(true);
	Ext.getCmp('txtKeterangan_InvPerencanaanBHPL').setReadOnly(true);
	
	getGridBarangviInvPerencanaanBHP(rowdata.req_number);
	Ext.getCmp('btnSimpan_viInvPerencanaanBHP').enable();
	Ext.getCmp('btnSimpanExit_viInvPerencanaanBHP').enable();
	Ext.getCmp('btnHapusPerencanaanBarang_InvPerencanaanBHP').enable();
	cekPembelianviInvPerencanaanBHP(rowdata.req_number);
	
};

function datasave_viInvPerencanaanBHP(){
	if (ValidasiEntryInvPerencanaanBHP(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/bhp/functionPerencanaanBHP/save",
				params: getParamInvPerencanaanBHP(),
				failure: function(o)
				{
					ShowPesanErrorInvPerencanaanBHP('Error, simpan data! Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoInvPerencanaanBHP('Data berhasil disimpan','Information');
						Ext.getCmp('txtRONumber_InvPerencanaanBHPL').setValue(cst.ronumber);
						getDataGridAwalviInvPerencanaanBHP();
						Ext.getCmp('btnSimpan_viInvPerencanaanBHP').disable();
						Ext.getCmp('btnSimpanExit_viInvPerencanaanBHP').disable();
						Ext.getCmp('btnHapusPerencanaanBarang_InvPerencanaanBHP').disable();
					}
					else 
					{
						ShowPesanErrorInvPerencanaanBHP('Gagal Menyimpan Data ini', 'Error');
					};
				}
			}
			
		)
	}
}

function getDataGridAwalviInvPerencanaanBHP(ronumber,tglawal,tglakhir){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/bhp/functionPerencanaanBHP/getGridAwal",
			params: {
				ronumber:ronumber,
				tglawal:tglawal,
				tglakhir:tglakhir
			},
			failure: function(o)
			{
				ShowPesanErrorInvPerencanaanBHP('Error, membaca grid awal! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					dataSource_viInvPerencanaanBHP.removeAll();
					
					var recs=[],
						recType=dataSource_viInvPerencanaanBHP.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
						dataSource_viInvPerencanaanBHP.add(recs);
					
					GridDataView_viInvPerencanaanBHP.getView().refresh();
				}
				else 
				{
					ShowPesanErrorInvPerencanaanBHP('Gagal membaca data obat', 'Error');
				};
			}
		}
		
	)
	
}


function getGridBarangviInvPerencanaanBHP(ronumber){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/bhp/functionPerencanaanBHP/getGridBarangviInvPerencanaanBHPLoad",
			params: {ronumber:ronumber},
			failure: function(o)
			{
				ShowPesanErrorInvPerencanaanBHP('Error, membaca list barang! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dsDataGrdJab_viInvPerencanaanBHP.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
						dsDataGrdJab_viInvPerencanaanBHP.add(recs);
					
					 InvPerencanaanBHP.form.Grid.a.getView().refresh();
				}
				else 
				{
					ShowPesanErrorInvPerencanaanBHP('Gagal membaca data', 'Error');
				};
			}
		}
		
	)
	
}

function cekPembelianviInvPerencanaanBHP(req_number,no_urut_brg){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/bhp/functionPerencanaanBHP/cekPembelian",
			params: {req_number:req_number},
			failure: function(o)
			{
				ShowPesanErrorInvPerencanaanBHP('Error, cek! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					Ext.getCmp('btnAddPerencanaanBarang').enable();
					Ext.getCmp('btnHapusGridBarang_InvPerencanaanBHP').enable();
					Ext.getCmp('btnSimpan_viInvPerencanaanBHP').enable();
					Ext.getCmp('btnSimpanExit_viInvPerencanaanBHP').enable();
					Ext.getCmp('btnHapusPerencanaanBarang_InvPerencanaanBHP').enable();
				}
				else 
				{
					ShowPesanInfoInvPerencanaanBHP('Perencanaan ini sudah dilakukan pembelian', 'Information');
					Ext.getCmp('btnSimpan_viInvPerencanaanBHP').disable(true);
					Ext.getCmp('btnSimpanExit_viInvPerencanaanBHP').disable(true);
					Ext.getCmp('btnHapusPerencanaanBarang_InvPerencanaanBHP').disable(true);
					Ext.getCmp('btnAddPerencanaanBarang').disable(true);
					Ext.getCmp('btnHapusGridBarang_InvPerencanaanBHP').disable(true);
				};
			}
		}
		
	)
}

function getParamInvPerencanaanBHP() 
{
	var	params =
	{
		RONumber:Ext.getCmp('txtRONumber_InvPerencanaanBHPL').getValue(),
		TglRO:Ext.getCmp('dfTglRo_InvPerencanaanBHPL').getValue(),
		Ket:Ext.getCmp('txtKeterangan_InvPerencanaanBHPL').getValue()
	}
	
	params['jumlah']=dsDataGrdJab_viInvPerencanaanBHP.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viInvPerencanaanBHP.getCount();i++)
	{
		params['kd_inv-'+i]=dsDataGrdJab_viInvPerencanaanBHP.data.items[i].data.kd_inv
		params['no_urut_brg-'+i]=dsDataGrdJab_viInvPerencanaanBHP.data.items[i].data.no_urut_brg
		params['qty-'+i]=dsDataGrdJab_viInvPerencanaanBHP.data.items[i].data.qty
		params['ordered-'+i]=dsDataGrdJab_viInvPerencanaanBHP.data.items[i].data.ordered
		params['stok-'+i]=dsDataGrdJab_viInvPerencanaanBHP.data.items[i].data.stok
		params['urut-'+i]=dsDataGrdJab_viInvPerencanaanBHP.data.items[i].data.req_line
	}
    return params
};



function ValidasiEntryInvPerencanaanBHP(modul,mBolHapus)
{
	var x = 1;
	if(dsDataGrdJab_viInvPerencanaanBHP.getCount() < 0){
		ShowPesanWarningInvPerencanaanBHP('Daftar perencanaa tidak boleh kosong, minimal 1 barang', 'Warning');
		x = 0;
	}
	
	for(var i=0; i<dsDataGrdJab_viInvPerencanaanBHP.getCount() ; i++){
		var o=dsDataGrdJab_viInvPerencanaanBHP.getRange()[i].data;
		if(o.qty == undefined || o.qty <= 0){
			ShowPesanWarningInvPerencanaanBHP('Qty tidak boleh 0, minimal qty 1', 'Warning');
			x = 0;
		}
		for(var j=i+1; j<dsDataGrdJab_viInvPerencanaanBHP.getCount() ; j++){
			var p=dsDataGrdJab_viInvPerencanaanBHP.getRange()[j].data;
			if(o.no_urut_brg == p.no_urut_brg){
				ShowPesanWarningInvPerencanaanBHP('Barang tidak boleh sama, periksa kembali daftar perencanaan', 'Warning');
				x = 0;
				break;
			}
		}
	}
	
	return x;
};



function ShowPesanWarningInvPerencanaanBHP(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorInvPerencanaanBHP(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};


function ShowPesanInfoInvPerencanaanBHP(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};