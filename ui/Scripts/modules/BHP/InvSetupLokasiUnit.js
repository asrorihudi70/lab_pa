var dataSource_viInvSetupLokasiUnit;
var selectCount_viInvSetupLokasiUnit=50;
var NamaForm_viInvSetupLokasiUnit="Setup Lokasi/Unit";
var mod_name_viInvSetupLokasiUnit="Setup Lokasi/Unit";
var now_viInvSetupLokasiUnit= new Date();
var rowSelected_viInvSetupLokasiUnit;
var setLookUps_viInvSetupLokasiUnit;
var tanggal = now_viInvSetupLokasiUnit.format("d/M/Y");
var jam = now_viInvSetupLokasiUnit.format("H/i/s");
var tmpkriteria;
var DataGridJenisObat;
var GridDataView_viInvSetupLokasiUnit;


var CurrentData_viInvSetupLokasiUnit =
{
	data: Object,
	details: Array,
	row: 0
};

var InvSetupLokasiUnit={};
InvSetupLokasiUnit.form={};
InvSetupLokasiUnit.func={};
InvSetupLokasiUnit.vars={};
InvSetupLokasiUnit.func.parent=InvSetupLokasiUnit;
InvSetupLokasiUnit.form.ArrayStore={};
InvSetupLokasiUnit.form.ComboBox={};
InvSetupLokasiUnit.form.DataStore={};
InvSetupLokasiUnit.form.Record={};
InvSetupLokasiUnit.form.Form={};
InvSetupLokasiUnit.form.Grid={};
InvSetupLokasiUnit.form.Panel={};
InvSetupLokasiUnit.form.TextField={};
InvSetupLokasiUnit.form.Button={};

InvSetupLokasiUnit.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_jns_lokasi','jns_lokasi','lokasi','kd_lokasi'],
	data: []
});

CurrentPage.page = dataGrid_viInvSetupLokasiUnit(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dataGrid_viInvSetupLokasiUnit(mod_id_viInvSetupLokasiUnit){	
 	// Field kiriman dari Project Net.
    var FieldMaster_viInvSetupLokasiUnit = 
	[
		'kd_unit_far', 'nm_unit_far', 'nomor_ro_unit', 'nomorawal','nomor_out_milik' 
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viInvSetupLokasiUnit = new WebApp.DataStore
	({
        fields: FieldMaster_viInvSetupLokasiUnit
    });
    getDataGridAwalInvSetupLokasiUnit();
    // Grid Apotek Perencanaan # --------------
	GridDataView_viInvSetupLokasiUnit = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viInvSetupLokasiUnit,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:true,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viInvSetupLokasiUnit = undefined;
							rowSelected_viInvSetupLokasiUnit = dataSource_viInvSetupLokasiUnit.getAt(row);
							CurrentData_viInvSetupLokasiUnit
							CurrentData_viInvSetupLokasiUnit.row = row;
							CurrentData_viInvSetupLokasiUnit.data = rowSelected_viInvSetupLokasiUnit.data;
							//DataInitInvSetupLokasiUnit(rowSelected_viInvSetupLokasiUnit.data);
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viInvSetupLokasiUnit = dataSource_viInvSetupLokasiUnit.getAt(ridx);
					if (rowSelected_viInvSetupLokasiUnit != undefined)
					{
						DataInitInvSetupLokasiUnit(rowSelected_viInvSetupLokasiUnit.data);
						//setLookUp_viInvSetupLokasiUnit(rowSelected_viInvSetupLokasiUnit.data);
					}
					else
					{
						//setLookUp_viInvSetupLokasiUnit();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Setup Unit
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header: 'Jenis Lokasi',
						dataIndex: 'jns_lokasi',
						hideable:false,
						menuDisabled: true,
						width: 40
						
					},
					{
						header: 'Kode Ruang',
						dataIndex: 'kd_lokasi',
						hideable:false,
						menuDisabled: true,
						width: 40
						
					},
					//-------------- ## --------------
					{
						header: 'Nama lokasi',
						dataIndex: 'lokasi',
						hideable:false,
						menuDisabled: true,
						width: 90
					},
					//-------------- ## --------------
					//-------------- ## --------------
					{
						header: 'kd_jns_lokasi',
						dataIndex: 'kd_jns_lokasi',
						hideable:false,
						hidden: true,
						width: 90
					},
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viInvSetupLokasiUnit',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Lokasi',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viInvSetupLokasiUnit',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viInvSetupLokasiUnit != undefined)
							{
								DataInitInvSetupLokasiUnit(rowSelected_viInvSetupLokasiUnit.data);
							}
							
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viInvSetupLokasiUnit, selectCount_viInvSetupLokasiUnit, dataSource_viInvSetupLokasiUnit),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)	
	var PanelTabInvSetupLokasiUnit = new Ext.FormPanel({
        labelAlign: 'top',
		layout:'form',
		border: false,  
		autoHeight:true,
        title: '',
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [PanelInputUnit()]
		
	})
	// Kriteria filter pada Grid # --------------
    var FrmData_viInvSetupLokasiUnit = new Ext.Panel
    (
		{
			title: NamaForm_viInvSetupLokasiUnit,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viInvSetupLokasiUnit,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ PanelTabInvSetupLokasiUnit,
					GridDataView_viInvSetupLokasiUnit],
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAddJenis_viInvSetupLokasiUnit',
						handler: function(){
							AddNewInvSetupLokasiUnit();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viInvSetupLokasiUnit',
						handler: function()
						{
							loadMask.show();
							dataSave_viInvSetupLokasiUnit();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viInvSetupLokasiUnit',
						handler: function()
						{
							Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
								if (button == 'yes'){
									loadMask.show();
									dataDelete_viInvSetupLokasiUnit();
								}
							});
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_viSetupPabrik',
						handler: function()
						{
							dataSource_viInvSetupLokasiUnit.removeAll();
							Ext.getCmp('cbJnsLokasiInvSetupLokasiUnit').setReadOnly(false);
							getDataGridAwalInvSetupLokasiUnit();
						}
					},
					{
						xtype: 'tbseparator'
					}
					
				]
			}
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_viInvSetupLokasiUnit;
    //-------------- # End form filter # --------------
}

function PanelInputUnit(rowdata){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 90,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'Jenis Lokasi'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							comboJenisLokasiInvSetupLokasiUnit(),
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'ID'
							},
							{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 30,
								xtype: 'textfield',
								id: 'txtIdLokasi_InvSetupLokasiUnit',
								name: 'txtIdLokasi_InvSetupLokasiUnit',
								width: 100,
								readOnly:true
							},
							{
								x: 10,
								y: 60,
								xtype: 'label',
								text: 'Nama lokasi'
							},
							{
								x: 120,
								y: 60,
								xtype: 'label',
								text: ':'
							},
							InvSetupLokasiUnit.vars.nama=new Nci.form.Combobox.autoComplete({
								x: 130,
								y: 60,
								tabIndex:2,
								width:260,
								store	: InvSetupLokasiUnit.form.ArrayStore.a,
								select	: function(a,b,c){
									
									Ext.getCmp('txtIdLokasi_InvSetupLokasiUnit').setValue(b.data.kd_lokasi);
									Ext.getCmp('cbJnsLokasiInvSetupLokasiUnit').setValue(b.data.kd_jns_lokasi);
									
									Ext.getCmp('cbJnsLokasiInvSetupLokasiUnit').setReadOnly(true);
									
									GridDataView_viInvSetupLokasiUnit.getView().refresh();
									
								},
								onShowList:function(a){
									dataSource_viInvSetupLokasiUnit.removeAll();
									
									var recs=[],
									recType=dataSource_viInvSetupLokasiUnit.recordType;
										
									for(var i=0; i<a.length; i++){
										recs.push(new recType(a[i]));
									}
									dataSource_viInvSetupLokasiUnit.add(recs);
									
								},
								insert	: function(o){
									return {
										kd_jns_lokasi 	: o.kd_jns_lokasi,
										jns_lokasi 		: o.jns_lokasi,
										kd_lokasi      	: o.kd_lokasi,
										lokasi 			: o.lokasi,
										text			:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_lokasi+'</td><td width="200">'+o.lokasi+'</td></tr></table>'
									}
								},
								param:function(){
										return {
											kd_jns_lokasi:Ext.getCmp('cbJnsLokasiInvSetupLokasiUnit').getValue()
										}
								},
								url		: baseURL + "index.php/bhp/functionSetupLokasi/getGridAwal",
								valueField: 'lokasi',
								displayField: 'text',
								listWidth: 270
							})
							
							
						]
					}
				]
			}
		]		
	};
        return items;
}
//------------------end---------------------------------------------------------


function comboJenisLokasiInvSetupLokasiUnit(){
	var Field_Vendor = ['kd_jns_lokasi', 'jns_lokasi'];
    ds_JnsLokasiInvSetupLokasiUnit = new WebApp.DataStore({fields: Field_Vendor});
    ds_JnsLokasiInvSetupLokasiUnit.load({
        params:{
	        Skip: 0,
	        Take: 1000,
	        Sort: 'kd_jns_lokasi',
	        Sortdir: 'ASC',
	        target: 'ComboJnsLokasiBHP',
	        param: ""
        }
    });
	var cboJnsLokasiInvSetupLokasiUnit = new Ext.form.ComboBox({
		x: 130,
		y: 0,
        id:'cbJnsLokasiInvSetupLokasiUnit',
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:true,
        mode: 'local',
        width: 260,
		readOnly:false,
		fieldLabel:'PBF ',
        store: ds_JnsLokasiInvSetupLokasiUnit,
        valueField: 'kd_jns_lokasi',
        displayField: 'jns_lokasi',
        listeners:{
			'select': function(a,b,c){
				getDataGridAwalInvSetupLokasiUnit(b.data.kd_jns_lokasi);
			}
        }
	});
	return cboJnsLokasiInvSetupLokasiUnit;
};

function getDataGridAwalInvSetupLokasiUnit(kd_jns_lokasi){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/bhp/functionSetupLokasi/getGridAwal",
			params: {
				kd_jns_lokasi:kd_jns_lokasi,
				text:''
			},
			failure: function(o)
			{
				ShowPesanErrorInvSetupLokasiUnit('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dataSource_viInvSetupLokasiUnit.removeAll();
					var recs=[],
						recType=dataSource_viInvSetupLokasiUnit.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dataSource_viInvSetupLokasiUnit.add(recs);
					
					
					
					GridDataView_viInvSetupLokasiUnit.getView().refresh();
				}
				else 
				{
					ShowPesanErrorInvSetupLokasiUnit('Gagal membaca data obat', 'Error');
				};
			}
		}
		
	)
	
}

function dataSave_viInvSetupLokasiUnit(){
	if (ValidasiSaveInvSetupLokasiUnit(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/bhp/functionSetupLokasi/save",
				params: getParamSaveInvSetupLokasiUnit(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorInvSetupLokasiUnit('Error simpan data! Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoInvSetupLokasiUnit('Berhasil menyimpan data ini','Information');
						Ext.getCmp('txtIdLokasi_InvSetupLokasiUnit').setValue(cst.kdlokasi);
						dataSource_viInvSetupLokasiUnit.removeAll();
						getDataGridAwalInvSetupLokasiUnit();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorInvSetupLokasiUnit('Gagal menyimpan data ini', 'Error');
						dataSource_viInvSetupLokasiUnit.removeAll();
						getDataGridAwalInvSetupLokasiUnit();
					};
				}
			}
			
		)
	}
}

function dataDelete_viInvSetupLokasiUnit(){
	if (ValidasiSaveInvSetupLokasiUnit(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/bhp/functionSetupLokasi/delete",
				params: getParamDeleteInvSetupLokasiUnit(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorInvSetupLokasiUnit('Error hapus data! Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoInvSetupLokasiUnit('Berhasil menghapus data ini','Information');
						AddNewInvSetupLokasiUnit()
						dataSource_viInvSetupLokasiUnit.removeAll();
						getDataGridAwalInvSetupLokasiUnit();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorInvSetupLokasiUnit('Gagal menghapus data ini', 'Error');
						dataSource_viInvSetupLokasiUnit.removeAll();
						getDataGridAwalInvSetupLokasiUnit();
					};
				}
			}
			
		)
	}
}

function AddNewInvSetupLokasiUnit(){
	Ext.getCmp('txtIdLokasi_InvSetupLokasiUnit').setValue('');
	InvSetupLokasiUnit.vars.nama.setValue('');
	Ext.getCmp('cbJnsLokasiInvSetupLokasiUnit').setValue('');
	
	Ext.getCmp('cbJnsLokasiInvSetupLokasiUnit').setReadOnly(false);
};

function DataInitInvSetupLokasiUnit(rowdata){
	Ext.getCmp('txtIdLokasi_InvSetupLokasiUnit').setValue(rowdata.kd_lokasi);
	InvSetupLokasiUnit.vars.nama.setValue(rowdata.lokasi);
	Ext.getCmp('cbJnsLokasiInvSetupLokasiUnit').setValue(rowdata.kd_jns_lokasi);
	
	Ext.getCmp('cbJnsLokasiInvSetupLokasiUnit').setReadOnly(true);
};

function getParamSaveInvSetupLokasiUnit(){
	var	params =
	{
		KdJnsLokasi:Ext.getCmp('cbJnsLokasiInvSetupLokasiUnit').getValue(),
		KdLokasi:Ext.getCmp('txtIdLokasi_InvSetupLokasiUnit').getValue(),
		Lokasi:InvSetupLokasiUnit.vars.nama.getValue()
	}
   
    return params
};

function getParamDeleteInvSetupLokasiUnit(){
	var	params =
	{
		KdLokasi:Ext.getCmp('txtIdLokasi_InvSetupLokasiUnit').getValue()
	}
   
    return params
};

function ValidasiSaveInvSetupLokasiUnit(modul,mBolHapus){
	var x = 1;
	if(InvSetupLokasiUnit.vars.nama.getValue() === '' || Ext.getCmp('cbJnsLokasiInvSetupLokasiUnit').getValue() ===''){
		if(InvSetupLokasiUnit.vars.nama.getValue() === ''){
			loadMask.hide();
			ShowPesanWarningInvSetupLokasiUnit('Lokasi masih kosong', 'Warning');
			x = 0;
		} else{
			loadMask.hide();
			ShowPesanWarningInvSetupLokasiUnit('Jenis lokasi belum di pilih', 'Warning');
			x = 0;
		}
		
	} 
	return x;
};



function ShowPesanWarningInvSetupLokasiUnit(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorInvSetupLokasiUnit(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoInvSetupLokasiUnit(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};