var dataSource_viInvSetupSatuan;
var selectCount_viInvSetupSatuan=50;
var NamaForm_viInvSetupSatuan="Setup Satuan";
var mod_name_viInvSetupSatuan="Setup Satuan";
var now_viInvSetupSatuan= new Date();
var rowSelected_viInvSetupSatuan;
var setLookUps_viInvSetupSatuan;
var tanggal = now_viInvSetupSatuan.format("d/M/Y");
var jam = now_viInvSetupSatuan.format("H/i/s");
var tmpkriteria;
var DataGridJenisObat;
var GridDataView_viInvSetupSatuan;


var CurrentData_viInvSetupSatuan =
{
	data: Object,
	details: Array,
	row: 0
};

var InvSetupSatuan={};
InvSetupSatuan.form={};
InvSetupSatuan.func={};
InvSetupSatuan.vars={};
InvSetupSatuan.func.parent=InvSetupSatuan;
InvSetupSatuan.form.ArrayStore={};
InvSetupSatuan.form.ComboBox={};
InvSetupSatuan.form.DataStore={};
InvSetupSatuan.form.Record={};
InvSetupSatuan.form.Form={};
InvSetupSatuan.form.Grid={};
InvSetupSatuan.form.Panel={};
InvSetupSatuan.form.TextField={};
InvSetupSatuan.form.Button={};

InvSetupSatuan.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_unit_far', 'nm_unit_far', 'nomor_ro_unit', 'nomorawal','nomor_out_milik'],
	data: []
});

CurrentPage.page = dataGrid_viInvSetupSatuan(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dataGrid_viInvSetupSatuan(mod_id_viInvSetupSatuan){	
 	// Field kiriman dari Project Net.
    var FieldMaster_viInvSetupSatuan = 
	[
		'kd_unit_far', 'nm_unit_far', 'nomor_ro_unit', 'nomorawal','nomor_out_milik' 
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viInvSetupSatuan = new WebApp.DataStore
	({
        fields: FieldMaster_viInvSetupSatuan
    });
    dataGriInvSetupSatuan();
    // Grid Apotek Perencanaan # --------------
	GridDataView_viInvSetupSatuan = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viInvSetupSatuan,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:true,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viInvSetupSatuan = undefined;
							rowSelected_viInvSetupSatuan = dataSource_viInvSetupSatuan.getAt(row);
							CurrentData_viInvSetupSatuan
							CurrentData_viInvSetupSatuan.row = row;
							CurrentData_viInvSetupSatuan.data = rowSelected_viInvSetupSatuan.data;
							//DataInitInvSetupSatuan(rowSelected_viInvSetupSatuan.data);
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viInvSetupSatuan = dataSource_viInvSetupSatuan.getAt(ridx);
					if (rowSelected_viInvSetupSatuan != undefined)
					{
						DataInitInvSetupSatuan(rowSelected_viInvSetupSatuan.data);
						//setLookUp_viInvSetupSatuan(rowSelected_viInvSetupSatuan.data);
					}
					else
					{
						//setLookUp_viInvSetupSatuan();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Setup Unit
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header: 'Kode Satuan',
						dataIndex: 'kd_satuan',
						hideable:false,
						menuDisabled: true,
						width: 40
						
					},
					//-------------- ## --------------
					{
						header: 'Satuan',
						dataIndex: 'satuan',
						hideable:false,
						menuDisabled: true,
						width: 90
					},
					//-------------- ## --------------
					
					
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viInvSetupSatuan',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viInvSetupSatuan',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viInvSetupSatuan != undefined)
							{
								DataInitInvSetupSatuan(rowSelected_viInvSetupSatuan.data);
							}
							
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viInvSetupSatuan, selectCount_viInvSetupSatuan, dataSource_viInvSetupSatuan),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)	
	var PanelTabInvSetupSatuan = new Ext.FormPanel({
        labelAlign: 'top',
		layout:'form',
		border: false,  
		autoHeight:true,
        title: '',
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [PanelInputUnit()]
		
	})
	// Kriteria filter pada Grid # --------------
    var FrmData_viInvSetupSatuan = new Ext.Panel
    (
		{
			title: NamaForm_viInvSetupSatuan,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viInvSetupSatuan,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ PanelTabInvSetupSatuan,
					GridDataView_viInvSetupSatuan],
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAddJenis_viInvSetupSatuan',
						handler: function(){
							AddNewInvSetupSatuan();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viInvSetupSatuan',
						handler: function()
						{
							loadMask.show();
							dataSave_viInvSetupSatuan();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viInvSetupSatuan',
						handler: function()
						{
							Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
								if (button == 'yes'){
									loadMask.show();
									dataDelete_viInvSetupSatuan();
								}
							});
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_viSetupPabrik',
						handler: function()
						{
							dataSource_viInvSetupSatuan.removeAll();
							dataGriInvSetupSatuan();
						}
					},
					{
						xtype: 'tbseparator'
					}
					
				]
			}
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_viInvSetupSatuan;
    //-------------- # End form filter # --------------
}

function PanelInputUnit(rowdata){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 55,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'Kode Satuan'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 0,
								xtype: 'textfield',
								id: 'txtKdSatuan_InvSetupSatuan',
								name: 'txtKdSatuan_InvSetupSatuan',
								width: 100,
								allowBlank: false,
								readOnly:true,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									},
									blur: function(a){
										if(a.getValue().length > 3){
											ShowPesanWarningInvSetupSatuan('Kode unit tidak boleh lebih dari 3 huruf', 'Warning');
										}
									}
								}
							},
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'Satuan'
							},
							{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							InvSetupSatuan.vars.nama=new Nci.form.Combobox.autoComplete({
								x: 130,
								y: 30,
								tabIndex:2,
								store	: InvSetupSatuan.form.ArrayStore.a,
								select	: function(a,b,c){
									
									Ext.getCmp('txtKdSatuan_InvSetupSatuan').setValue(b.data.kd_satuan);
									
									GridDataView_viInvSetupSatuan.getView().refresh();
									
								},
								onShowList:function(a){
									dataSource_viInvSetupSatuan.removeAll();
									
									var recs=[],
									recType=dataSource_viInvSetupSatuan.recordType;
										
									for(var i=0; i<a.length; i++){
										recs.push(new recType(a[i]));
									}
									dataSource_viInvSetupSatuan.add(recs);
									
								},
								insert	: function(o){
									return {
										kd_satuan      	: o.kd_satuan,
										satuan 			: o.satuan,
										text			:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_satuan+'</td><td width="200">'+o.satuan+'</td></tr></table>'
									}
								},
								url		: baseURL + "index.php/bhp/functionSetupSatuan/getSatuanGrid",
								valueField: 'satuan',
								displayField: 'text',
								listWidth: 280
							})
							
							
						]
					}
				]
			}
		]		
	};
        return items;
}
//------------------end---------------------------------------------------------

function dataGriInvSetupSatuan(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/bhp/functionSetupSatuan/getSatuanGrid",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorInvSetupSatuan('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dataSource_viInvSetupSatuan.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dataSource_viInvSetupSatuan.add(recs);
					
					
					
					GridDataView_viInvSetupSatuan.getView().refresh();
				}
				else 
				{
					ShowPesanErrorInvSetupSatuan('Gagal membaca data', 'Error');
				};
			}
		}
		
	)
	
}

function dataSave_viInvSetupSatuan(){
	if (ValidasiSaveInvSetupSatuan(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/bhp/functionSetupSatuan/save",
				params: getParamSaveInvSetupSatuan(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorInvSetupSatuan('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoInvSetupSatuan('Berhasil menyimpan data ini','Information');
						Ext.getCmp('txtKdSatuan_InvSetupSatuan').setValue(cst.kdsatuan);
						dataSource_viInvSetupSatuan.removeAll();
						dataGriInvSetupSatuan();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorInvSetupSatuan('Gagal menyimpan data ini', 'Error');
						dataSource_viInvSetupSatuan.removeAll();
						dataGriInvSetupSatuan();
					};
				}
			}
			
		)
	}
}

function dataDelete_viInvSetupSatuan(){
	if (ValidasiSaveInvSetupSatuan(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/bhp/functionSetupSatuan/delete",
				params: getParamDeleteInvSetupSatuan(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorInvSetupSatuan('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoInvSetupSatuan('Berhasil menghapus data ini','Information');
						AddNewInvSetupSatuan()
						dataSource_viInvSetupSatuan.removeAll();
						dataGriInvSetupSatuan();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorInvSetupSatuan('Gagal menghapus data ini', 'Error');
						dataSource_viInvSetupSatuan.removeAll();
						dataGriInvSetupSatuan();
					};
				}
			}
			
		)
	}
}

function AddNewInvSetupSatuan(){
	Ext.getCmp('txtKdSatuan_InvSetupSatuan').setValue('');
	InvSetupSatuan.vars.nama.setValue('');
};

function DataInitInvSetupSatuan(rowdata){
	Ext.getCmp('txtKdSatuan_InvSetupSatuan').setValue(rowdata.kd_satuan);
	InvSetupSatuan.vars.nama.setValue(rowdata.satuan);
};

function getParamSaveInvSetupSatuan(){
	var	params =
	{
		KdSatuan:Ext.getCmp('txtKdSatuan_InvSetupSatuan').getValue(),
		Satuan:InvSetupSatuan.vars.nama.getValue()
	}
   
    return params
};

function getParamDeleteInvSetupSatuan(){
	var	params =
	{
		KdSatuan:Ext.getCmp('txtKdSatuan_InvSetupSatuan').getValue()
	}
   
    return params
};

function ValidasiSaveInvSetupSatuan(modul,mBolHapus){
	var x = 1;
	if(InvSetupSatuan.vars.nama.getValue() === ''){
		loadMask.hide();
		ShowPesanWarningInvSetupSatuan('Satuan masih kosong', 'Warning');
		x = 0;
	}
	
	return x;
};



function ShowPesanWarningInvSetupSatuan(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorInvSetupSatuan(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoInvSetupSatuan(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};