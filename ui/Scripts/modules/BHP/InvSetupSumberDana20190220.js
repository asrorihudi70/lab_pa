var dataSource_viInvSetupSumberDana;
var selectCount_viInvSetupSumberDana=50;
var NamaForm_viInvSetupSumberDana="Setup Sumber Dana";
var mod_name_viInvSetupSumberDana="Setup Sumber Dana";
var now_viInvSetupSumberDana= new Date();
var rowSelected_viInvSetupSumberDana;
var setLookUps_viInvSetupSumberDana;
var tanggal = now_viInvSetupSumberDana.format("d/M/Y");
var jam = now_viInvSetupSumberDana.format("H/i/s");
var tmpkriteria;
var DataGridJenisObat;
var GridDataView_viInvSetupSumberDana;


var CurrentData_viInvSetupSumberDana =
{
	data: Object,
	details: Array,
	row: 0
};

var InvSetupSumberDana={};
InvSetupSumberDana.form={};
InvSetupSumberDana.func={};
InvSetupSumberDana.vars={};
InvSetupSumberDana.func.parent=InvSetupSumberDana;
InvSetupSumberDana.form.ArrayStore={};
InvSetupSumberDana.form.ComboBox={};
InvSetupSumberDana.form.DataStore={};
InvSetupSumberDana.form.Record={};
InvSetupSumberDana.form.Form={};
InvSetupSumberDana.form.Grid={};
InvSetupSumberDana.form.Panel={};
InvSetupSumberDana.form.TextField={};
InvSetupSumberDana.form.Button={};

InvSetupSumberDana.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_unit_far', 'nm_unit_far', 'nomor_ro_unit', 'nomorawal','nomor_out_milik'],
	data: []
});

CurrentPage.page = dataGrid_viInvSetupSumberDana(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dataGrid_viInvSetupSumberDana(mod_id_viInvSetupSumberDana){	
 	// Field kiriman dari Project Net.
    var FieldMaster_viInvSetupSumberDana = 
	[
		'kd_unit_far', 'nm_unit_far', 'nomor_ro_unit', 'nomorawal','nomor_out_milik' 
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viInvSetupSumberDana = new WebApp.DataStore
	({
        fields: FieldMaster_viInvSetupSumberDana
    });
    dataGriInvSetupSumberDana();
    // Grid Apotek Perencanaan # --------------
	GridDataView_viInvSetupSumberDana = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viInvSetupSumberDana,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:true,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viInvSetupSumberDana = undefined;
							rowSelected_viInvSetupSumberDana = dataSource_viInvSetupSumberDana.getAt(row);
							CurrentData_viInvSetupSumberDana
							CurrentData_viInvSetupSumberDana.row = row;
							CurrentData_viInvSetupSumberDana.data = rowSelected_viInvSetupSumberDana.data;
							//DataInitInvSetupSumberDana(rowSelected_viInvSetupSumberDana.data);
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viInvSetupSumberDana = dataSource_viInvSetupSumberDana.getAt(ridx);
					if (rowSelected_viInvSetupSumberDana != undefined)
					{
						DataInitInvSetupSumberDana(rowSelected_viInvSetupSumberDana.data);
						//setLookUp_viInvSetupSumberDana(rowSelected_viInvSetupSumberDana.data);
					}
					else
					{
						//setLookUp_viInvSetupSumberDana();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Setup Unit
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header: 'ID',
						dataIndex: 'kd_dana',
						hideable:false,
						menuDisabled: true,
						width: 40
						
					},
					//-------------- ## --------------
					{
						header: 'Dana',
						dataIndex: 'sumber_dana',
						hideable:false,
						menuDisabled: true,
						width: 90
					},
					//-------------- ## --------------
					
					
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viInvSetupSumberDana',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Dana',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viInvSetupSumberDana',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viInvSetupSumberDana != undefined)
							{
								DataInitInvSetupSumberDana(rowSelected_viInvSetupSumberDana.data);
							}
							
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viInvSetupSumberDana, selectCount_viInvSetupSumberDana, dataSource_viInvSetupSumberDana),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)	
	var PanelTabInvSetupSumberDana = new Ext.FormPanel({
        labelAlign: 'top',
		layout:'form',
		border: false,  
		autoHeight:true,
        title: '',
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [PanelInputUnit()]
		
	})
	// Kriteria filter pada Grid # --------------
    var FrmData_viInvSetupSumberDana = new Ext.Panel
    (
		{
			title: NamaForm_viInvSetupSumberDana,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viInvSetupSumberDana,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ PanelTabInvSetupSumberDana,
					GridDataView_viInvSetupSumberDana],
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAddJenis_viInvSetupSumberDana',
						handler: function(){
							AddNewInvSetupSumberDana();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viInvSetupSumberDana',
						handler: function()
						{
							loadMask.show();
							dataSave_viInvSetupSumberDana();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viInvSetupSumberDana',
						handler: function()
						{
							Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
								if (button == 'yes'){
									loadMask.show();
									dataDelete_viInvSetupSumberDana();
								}
							});
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_viSetupPabrik',
						handler: function()
						{
							dataSource_viInvSetupSumberDana.removeAll();
							dataGriInvSetupSumberDana();
						}
					},
					{
						xtype: 'tbseparator'
					}
					
				]
			}
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_viInvSetupSumberDana;
    //-------------- # End form filter # --------------
}

function PanelInputUnit(rowdata){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 55,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'ID'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 0,
								xtype: 'textfield',
								id: 'txtKdDana_InvSetupSumberDana',
								name: 'txtKdDana_InvSetupSumberDana',
								width: 100,
								allowBlank: false,
								readOnly:true,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									},
									blur: function(a){
										if(a.getValue().length > 3){
											ShowPesanWarningInvSetupSumberDana('Kode unit tidak boleh lebih dari 3 huruf', 'Warning');
										}
									}
								}
							},
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'Satuan'
							},
							{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							InvSetupSumberDana.vars.nama=new Nci.form.Combobox.autoComplete({
								x: 130,
								y: 30,
								tabIndex:2,
								store	: InvSetupSumberDana.form.ArrayStore.a,
								select	: function(a,b,c){
									
									Ext.getCmp('txtKdDana_InvSetupSumberDana').setValue(b.data.kd_dana);
									
									GridDataView_viInvSetupSumberDana.getView().refresh();
									
								},
								onShowList:function(a){
									dataSource_viInvSetupSumberDana.removeAll();
									
									var recs=[],
									recType=dataSource_viInvSetupSumberDana.recordType;
										
									for(var i=0; i<a.length; i++){
										recs.push(new recType(a[i]));
									}
									dataSource_viInvSetupSumberDana.add(recs);
									
								},
								insert	: function(o){
									return {
										kd_dana      	: o.kd_dana,
										sumber_dana 	: o.sumber_dana,
										text			:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_dana+'</td><td width="200">'+o.sumber_dana+'</td></tr></table>'
									}
								},
								url		: baseURL + "index.php/bhp/functionSetupSumberDana/getSumberDanaGrid",
								valueField: 'sumber_dana',
								displayField: 'text',
								listWidth: 280
							})
							
							
						]
					}
				]
			}
		]		
	};
        return items;
}
//------------------end---------------------------------------------------------

function dataGriInvSetupSumberDana(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/bhp/functionSetupSumberDana/getSumberDanaGrid",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorInvSetupSumberDana('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dataSource_viInvSetupSumberDana.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dataSource_viInvSetupSumberDana.add(recs);
					
					
					
					GridDataView_viInvSetupSumberDana.getView().refresh();
				}
				else 
				{
					ShowPesanErrorInvSetupSumberDana('Gagal membaca data', 'Error');
				};
			}
		}
		
	)
	
}

function dataSave_viInvSetupSumberDana(){
	if (ValidasiSaveInvSetupSumberDana(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/bhp/functionSetupSumberDana/save",
				params: getParamSaveInvSetupSumberDana(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorInvSetupSumberDana('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoInvSetupSumberDana('Berhasil menyimpan data ini','Information');
						Ext.getCmp('txtKdDana_InvSetupSumberDana').setValue(cst.kddana);
						dataSource_viInvSetupSumberDana.removeAll();
						dataGriInvSetupSumberDana();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorInvSetupSumberDana('Gagal menyimpan data ini', 'Error');
						dataSource_viInvSetupSumberDana.removeAll();
						dataGriInvSetupSumberDana();
					};
				}
			}
			
		)
	}
}

function dataDelete_viInvSetupSumberDana(){
	if (ValidasiSaveInvSetupSumberDana(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/bhp/functionSetupSumberDana/delete",
				params: getParamDeleteInvSetupSumberDana(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorInvSetupSumberDana('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoInvSetupSumberDana('Berhasil menghapus data ini','Information');
						AddNewInvSetupSumberDana()
						dataSource_viInvSetupSumberDana.removeAll();
						dataGriInvSetupSumberDana();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorInvSetupSumberDana('Gagal menghapus data ini', 'Error');
						dataSource_viInvSetupSumberDana.removeAll();
						dataGriInvSetupSumberDana();
					};
				}
			}
			
		)
	}
}

function AddNewInvSetupSumberDana(){
	Ext.getCmp('txtKdDana_InvSetupSumberDana').setValue('');
	InvSetupSumberDana.vars.nama.setValue('');
};

function DataInitInvSetupSumberDana(rowdata){
	Ext.getCmp('txtKdDana_InvSetupSumberDana').setValue(rowdata.kd_dana);
	InvSetupSumberDana.vars.nama.setValue(rowdata.sumber_dana);
};

function getParamSaveInvSetupSumberDana(){
	var	params =
	{
		KdDana:Ext.getCmp('txtKdDana_InvSetupSumberDana').getValue(),
		SumberDana:InvSetupSumberDana.vars.nama.getValue()
	}
   
    return params
};

function getParamDeleteInvSetupSumberDana(){
	var	params =
	{
		KdDana:Ext.getCmp('txtKdDana_InvSetupSumberDana').getValue()
	}
   
    return params
};

function ValidasiSaveInvSetupSumberDana(modul,mBolHapus){
	var x = 1;
	if(InvSetupSumberDana.vars.nama.getValue() === ''){
		loadMask.hide();
		ShowPesanWarningInvSetupSumberDana('Sumber dana masih kosong', 'Warning');
		x = 0;
	} 
	
	return x;
};



function ShowPesanWarningInvSetupSumberDana(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorInvSetupSumberDana(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoInvSetupSumberDana(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};