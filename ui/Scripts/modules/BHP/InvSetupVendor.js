var dataSource_viInvSetupVendor;
var selectCount_viInvSetupVendor=50;
var NamaForm_viInvSetupVendor="Setup Vendor";
var mod_name_viInvSetupVendor="Setup Vendor";
var now_viInvSetupVendor= new Date();
var rowSelected_viInvSetupVendor;
var setLookUps_viInvSetupVendor;
var tanggal = now_viInvSetupVendor.format("d/M/Y");
var jam = now_viInvSetupVendor.format("H/i/s");
var tmpkriteria;
var DataGridJenisObat;
var GridDataView_viInvSetupVendor;


var CurrentData_viInvSetupVendor =
{
	data: Object,
	details: Array,
	row: 0
};

var InvSetupVendor={};
InvSetupVendor.form={};
InvSetupVendor.func={};
InvSetupVendor.vars={};
InvSetupVendor.func.parent=InvSetupVendor;
InvSetupVendor.form.ArrayStore={};
InvSetupVendor.form.ComboBox={};
InvSetupVendor.form.DataStore={};
InvSetupVendor.form.Record={};
InvSetupVendor.form.Form={};
InvSetupVendor.form.Grid={};
InvSetupVendor.form.Panel={};
InvSetupVendor.form.TextField={};
InvSetupVendor.form.Button={};

InvSetupVendor.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_vendor', 'vendor', 'contact', 'alamat', 'kota', 'telepon1', 'telepon2',
			'fax', 'kd_pos', 'negara', 'norek', 'term'],
	data: []
});

CurrentPage.page = dataGrid_viInvSetupVendor(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dataGrid_viInvSetupVendor(mod_id_viInvSetupVendor){	
 	// Field kiriman dari Project Net.
    var FieldMaster_viInvSetupVendor = 
	[
		'kd_vendor', 'vendor', 'contact', 'alamat', 'kota', 'telepon1', 'telepon2',
		'fax', 'kd_pos', 'negara', 'norek', 'term'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viInvSetupVendor = new WebApp.DataStore
	({
        fields: FieldMaster_viInvSetupVendor
    });
    dataGriInvSetupVendor();
    // Grid Apotek Perencanaan # --------------
	GridDataView_viInvSetupVendor = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viInvSetupVendor,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:true,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viInvSetupVendor = undefined;
							rowSelected_viInvSetupVendor = dataSource_viInvSetupVendor.getAt(row);
							CurrentData_viInvSetupVendor
							CurrentData_viInvSetupVendor.row = row;
							CurrentData_viInvSetupVendor.data = rowSelected_viInvSetupVendor.data;
							//DataInitInvSetupVendor(rowSelected_viInvSetupVendor.data);
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viInvSetupVendor = dataSource_viInvSetupVendor.getAt(ridx);
					if (rowSelected_viInvSetupVendor != undefined)
					{
						DataInitInvSetupVendor(rowSelected_viInvSetupVendor.data);
						//setLookUp_viInvSetupVendor(rowSelected_viInvSetupVendor.data);
					}
					else
					{
						//setLookUp_viInvSetupVendor();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Setup vendor
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colKode_viInvSetupVendor',
						header: 'Kode',
						dataIndex: 'kd_vendor',
						hideable:false,
						menuDisabled: true,
						width: 40
						
					},
					//-------------- ## --------------
					{
						id: 'colVendor_viInvSetupVendor',
						header: 'Nama vendor',
						dataIndex: 'vendor',
						hideable:false,
						menuDisabled: true,
						width: 90
					},
					//-------------- ## --------------
					{
						id: 'colContact_viInvSetupVendor',
						header: 'Contact',
						dataIndex: 'contact',
						hideable:false,
						menuDisabled: true,
						width: 50
						
					},
					//-------------- ## --------------
					{
						id: 'colAlamat_viInvSetupVendor',
						header:'Alamat vendor',
						dataIndex: 'alamat',						
						width: 130,
						hideable:false,
                        menuDisabled:true
					},
					//-------------- ## --------------
					{
						id: 'colKota_viInvSetupVendor',
						header: 'Kota',
						dataIndex: 'kota',
						hideable:false,
                        menuDisabled:true,
						width: 50
					},
					//-------------- ## --------------
					//-------------- ## --------------
					
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viInvSetupVendor',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viInvSetupVendor',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viInvSetupVendor != undefined)
							{
								DataInitInvSetupVendor(rowSelected_viInvSetupVendor.data);
							}
							
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viInvSetupVendor, selectCount_viInvSetupVendor, dataSource_viInvSetupVendor),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)	
	var PanelTabInvSetupVendor = new Ext.FormPanel({
        labelAlign: 'top',
		layout:'form',
		border: false,  
		autoHeight:true,
        title: '',
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [PanelInputVendor()]
		
	})
	// Kriteria filter pada Grid # --------------
    var FrmData_viInvSetupVendor = new Ext.Panel
    (
		{
			title: NamaForm_viInvSetupVendor,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viInvSetupVendor,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ PanelTabInvSetupVendor,
					GridDataView_viInvSetupVendor],
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAddJenis_viInvSetupVendor',
						handler: function(){
							AddNewInvSetupVendor();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viInvSetupVendor',
						handler: function()
						{
							loadMask.show();
							dataSave_viInvSetupVendor();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viInvSetupVendor',
						handler: function()
						{
							Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
								if (button == 'yes'){
									loadMask.show();
									dataDelete_viInvSetupVendor();
								}
							});
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_viInvSetupVendor',
						handler: function()
						{
							dataSource_viInvSetupVendor.removeAll();
							dataGriInvSetupVendor();
						}
					},
					{
						xtype: 'tbseparator'
					}
					
				]
			}
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_viInvSetupVendor;
    //-------------- # End form filter # --------------
}

function PanelInputVendor(rowdata){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 170,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'Vendor ID'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 0,
								xtype: 'textfield',
								id: 'txtKdVendor_InvSetupVendor',
								name: 'txtKdVendor_InvSetupVendor',
								allowBlank: false,
								width: 130,
								readOnly:true,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									}
								}
							},
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'Nama Vendor'
							},
							{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							
							InvSetupVendor.vars.nama=new Nci.form.Combobox.autoComplete({
								x: 130,
								y: 30,
								tabIndex:2,
								store	: InvSetupVendor.form.ArrayStore.a,
								select	: function(a,b,c){
									
									Ext.getCmp('txtKdVendor_InvSetupVendor').setValue(b.data.kd_vendor);
									Ext.getCmp('txtKontak_InvSetupVendor').setValue(b.data.contact);
									Ext.getCmp('txtAlamat_InvSetupVendor').setValue(b.data.alamat);
									Ext.getCmp('txtKota_InvSetupVendor').setValue(b.data.kota);
									Ext.getCmp('txtKodePos_InvSetupVendor').setValue(b.data.kd_pos);
									Ext.getCmp('txtNegara_InvSetupVendor').setValue(b.data.negara);
									Ext.getCmp('txtTelepon_InvSetupVendor').setValue(b.data.telepon1);
									Ext.getCmp('txtTelepon2_InvSetupVendor').setValue(b.data.telepon2);
									Ext.getCmp('txtFax_InvSetupVendor').setValue(b.data.fax);
									Ext.getCmp('txtTempo_InvSetupVendor').setValue(b.data.term);
									GridDataView_viInvSetupVendor.getView().refresh();
									
								},
								onShowList:function(a){
									dataSource_viInvSetupVendor.removeAll();
									
									var recs=[],
									recType=dataSource_viInvSetupVendor.recordType;
										
									for(var i=0; i<a.length; i++){
										recs.push(new recType(a[i]));
									}
									dataSource_viInvSetupVendor.add(recs);
									
								},
								insert	: function(o){
									return {
										kd_vendor      	: o.kd_vendor,
										vendor 			: o.vendor,
										contact			: o.contact,
										alamat			: o.alamat,
										kota			: o.kota,
										telepon1		: o.telepon1,
										telepon2		: o.telepon2,
										fax				: o.fax,
										kd_pos			: o.kd_pos,
										negara			: o.negara,
										term			: o.term,
										norek			: o.norek,
										text			:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_vendor+'</td><td width="200">'+o.vendor+'</td></tr></table>'
									}
								},
								url		: baseURL + "index.php/bhp/functionSetupVendor/getVendorGrid",
								valueField: 'vendor',
								displayField: 'text',
								listWidth: 280
							}),
							{
								x: 10,
								y: 60,
								xtype: 'label',
								text: 'Kontak Person'
							},
							{
								x: 120,
								y: 60,
								xtype: 'label',
								text: ':'
							},
							{
								x:130,
								y:60,	
								xtype: 'textfield',
								name: 'txtKontak_InvSetupVendor',
								id: 'txtKontak_InvSetupVendor',
								tabIndex:3,
								width: 180
							},
							{
								x: 10,
								y: 90,
								xtype: 'label',
								text: 'Alamat'
							},
							{
								x: 120,
								y: 90,
								xtype: 'label',
								text: ':'
							},
							{
								x:130,
								y:90,	
								xtype: 'textarea',
								name: 'txtAlamat_InvSetupVendor',
								id: 'txtAlamat_InvSetupVendor',
								width : 280,
								height : 50,
								tabIndex:4
							},
							{
								x: 10,
								y: 148,
								xtype: 'label',
								text: 'Kota'
							},
							{
								x: 120,
								y: 148,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 148,
								xtype: 'textfield',
								name: 'txtKota_InvSetupVendor',
								id: 'txtKota_InvSetupVendor',
								tabIndex:5,
								width: 150
							},
							
							//-------------- ## --------------
							{
								x: 420,
								y: 0,
								xtype: 'label',
								text: 'Kode Pos'
							},
							{
								x: 510,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 520,
								y: 0,
								xtype: 'numberfield',
								name: 'txtKodePos_InvSetupVendor',
								id: 'txtKodePos_InvSetupVendor',
								tabIndex:6,
								width: 70
							},
							{
								x: 420,
								y: 30,
								xtype: 'label',
								text: 'Negara'
							},
							{
								x: 510,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							{
								x: 520,
								y: 30,
								xtype: 'textfield',
								name: 'txtNegara_InvSetupVendor',
								id: 'txtNegara_InvSetupVendor',
								tabIndex:7,
								width: 150
							},
							{
								x: 420,
								y: 60,
								xtype: 'label',
								text: 'Telepon'
							},
							{
								x: 510,
								y: 60,
								xtype: 'label',
								text: ':'
							},
							{
								x: 520,
								y: 60,
								xtype: 'textfield',
								name: 'txtTelepon_InvSetupVendor',
								id: 'txtTelepon_InvSetupVendor',
								tabIndex:8,
								width: 100
							},
							{
								x: 630,
								y: 60,
								xtype: 'textfield',
								name: 'txtTelepon2_InvSetupVendor',
								id: 'txtTelepon2_InvSetupVendor',
								tabIndex:9,
								width: 100
							},
							{
								x: 740,
								y: 60,
								xtype: 'label',
								text: 'Fax'
							},
							{
								x: 770,
								y: 60,
								xtype: 'label',
								text: ':'
							},
							{
								x: 780,
								y: 60,
								xtype: 'textfield',
								name: 'txtFax_InvSetupVendor',
								id: 'txtFax_InvSetupVendor',
								tabIndex:10,
								width: 100
							},
							{
								x: 420,
								y: 90,
								xtype: 'label',
								text: 'Jatuh Tempo'
							},
							{
								x: 510,
								y: 90,
								xtype: 'label',
								text: ':'
							},
							{
								x: 520,
								y: 90,
								xtype: 'numberfield',
								name: 'txtTempo_InvSetupVendor',
								id: 'txtTempo_InvSetupVendor',
								tabIndex:12,
								width: 70
							}
							
							
						]
					}
				]
			}
		]		
	};
        return items;
}
//------------------end---------------------------------------------------------


function dataGriInvSetupVendor(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/bhp/functionSetupVendor/getVendorGrid",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorInvSetupVendor('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dataSource_viInvSetupVendor.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dataSource_viInvSetupVendor.add(recs);
					
					
					
					GridDataView_viInvSetupVendor.getView().refresh();
				}
				else 
				{
					ShowPesanErrorInvSetupVendor('Gagal membaca data', 'Error');
				};
			}
		}
		
	)
	
}

function dataSave_viInvSetupVendor(){
	if (ValidasiSaveInvSetupVendor(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/bhp/functionSetupVendor/save",
				params: getParamSaveInvSetupVendor(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorInvSetupVendor('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoInvSetupVendor('Berhasil menyimpan data ini','Information');
						Ext.getCmp('txtKdVendor_InvSetupVendor').setValue(cst.kdvendor);
						dataSource_viInvSetupVendor.removeAll();
						dataGriInvSetupVendor();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorInvSetupVendor('Gagal menyimpan data ini', 'Error');
						dataSource_viInvSetupVendor.removeAll();
						dataGriInvSetupVendor();
					};
				}
			}
			
		)
	}
}

function dataDelete_viInvSetupVendor(){
	if (ValidasiSaveInvSetupVendor(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/bhp/functionSetupVendor/delete",
				params: getParamDeleteInvSetupVendor(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorInvSetupVendor('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoInvSetupVendor('Berhasil menghapus data ini','Information');
						AddNewInvSetupVendor()
						dataSource_viInvSetupVendor.removeAll();
						dataGriInvSetupVendor();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorInvSetupVendor('Gagal menghapus data ini', 'Error');
						dataSource_viInvSetupVendor.removeAll();
						dataGriInvSetupVendor();
					};
				}
			}
			
		)
	}
}

/* function refreshInvSetupVendor(kriteria)
{
    dataSource_viInvSetupVendor.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'ViewInvSetupVendor',
                    param : kriteria
                }			
            }
        );   
    return dataSource_viInvSetupVendor;
} */
function AddNewInvSetupVendor(){
	Ext.getCmp('txtKdVendor_InvSetupVendor').setValue('');
	InvSetupVendor.vars.nama.setValue('');
	Ext.getCmp('txtKontak_InvSetupVendor').setValue('');
	Ext.getCmp('txtAlamat_InvSetupVendor').setValue('');
	Ext.getCmp('txtKota_InvSetupVendor').setValue('');
	Ext.getCmp('txtKodePos_InvSetupVendor').setValue('');
	Ext.getCmp('txtNegara_InvSetupVendor').setValue('');
	Ext.getCmp('txtTelepon_InvSetupVendor').setValue('');
	Ext.getCmp('txtTelepon2_InvSetupVendor').setValue('');
	Ext.getCmp('txtFax_InvSetupVendor').setValue('');
	Ext.getCmp('txtTempo_InvSetupVendor').setValue('');
};

function DataInitInvSetupVendor(rowdata){
	Ext.getCmp('txtKdVendor_InvSetupVendor').setValue(rowdata.kd_vendor);
	InvSetupVendor.vars.nama.setValue(rowdata.vendor);
	Ext.getCmp('txtKontak_InvSetupVendor').setValue(rowdata.contact);
	Ext.getCmp('txtAlamat_InvSetupVendor').setValue(rowdata.alamat);
	Ext.getCmp('txtKota_InvSetupVendor').setValue(rowdata.kota);
	Ext.getCmp('txtKodePos_InvSetupVendor').setValue(rowdata.kd_pos);
	Ext.getCmp('txtNegara_InvSetupVendor').setValue(rowdata.negara);
	Ext.getCmp('txtTelepon_InvSetupVendor').setValue(rowdata.telepon1);
	Ext.getCmp('txtTelepon2_InvSetupVendor').setValue(rowdata.telepon2);
	Ext.getCmp('txtFax_InvSetupVendor').setValue(rowdata.fax);
	Ext.getCmp('txtTempo_InvSetupVendor').setValue(rowdata.term);
};

function getParamSaveInvSetupVendor(){
	var term;
	if(Ext.getCmp('txtTempo_InvSetupVendor').getValue() ===''){
		term=0;
	} else{
		term=Ext.getCmp('txtTempo_InvSetupVendor').getValue();
	}
	var	params =
	{
		KdVendor:Ext.getCmp('txtKdVendor_InvSetupVendor').getValue(),
		Nama:InvSetupVendor.vars.nama.getValue(),
		Kontak:Ext.getCmp('txtKontak_InvSetupVendor').getValue(),
		Alamat:Ext.getCmp('txtAlamat_InvSetupVendor').getValue(),
		Kota:Ext.getCmp('txtKota_InvSetupVendor').getValue(),
		KodePos:Ext.getCmp('txtKodePos_InvSetupVendor').getValue(),
		Negara:Ext.getCmp('txtNegara_InvSetupVendor').getValue(),
		Telepon:Ext.getCmp('txtTelepon_InvSetupVendor').getValue(),
		Telepon2:Ext.getCmp('txtTelepon2_InvSetupVendor').getValue(),
		Fax:Ext.getCmp('txtFax_InvSetupVendor').getValue(),
		Tempo:term
	}
   
    return params
};

function getParamDeleteInvSetupVendor(){
	var	params =
	{
		KdVendor:Ext.getCmp('txtKdVendor_InvSetupVendor').getValue()
	}
   
    return params
};

function ValidasiSaveInvSetupVendor(modul,mBolHapus){
	var x = 1;
	if(InvSetupVendor.vars.nama.getValue() === ''){
		loadMask.hide();
		ShowPesanWarningInvSetupVendor('Nama vendor masih kosong', 'Warning');
		x = 0;
	} 
	return x;
};



function ShowPesanWarningInvSetupVendor(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorInvSetupVendor(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoInvSetupVendor(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};