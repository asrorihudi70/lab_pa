var dataSource_viInvStokBHP;
var selectCount_viInvStokBHP=50;
var NamaForm_viInvStokBHP="Stok Bahan Habis Pakai";
var mod_name_viInvStokBHP="viInvStokBHP";
var now_viInvStokBHP= new Date();
var rowSelected_viInvStokBHP;
var setLookUps_viInvStokBHP;
var bulan = now_viInvStokBHP.format("m-Y");
var jam = now_viInvStokBHP.format("H/i/s");
var tmpkriteria;
var GridDataView_viInvStokBHP;


var CurrentData_viInvStokBHP =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viInvStokBHP(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

var InvStokBHP={};
InvStokBHP.form={};
InvStokBHP.func={};
InvStokBHP.vars={};
InvStokBHP.func.parent=InvStokBHP;
InvStokBHP.form.ArrayStore={};
InvStokBHP.form.ComboBox={};
InvStokBHP.form.DataStore={};
InvStokBHP.form.Record={};
InvStokBHP.form.Form={};
InvStokBHP.form.Grid={};
InvStokBHP.form.Panel={};
InvStokBHP.form.TextField={};
InvStokBHP.form.Button={};

InvStokBHP.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_bahan', 'nama_bahan', 'kd_satuan', 'satuan', 'qty'],
	data: []
});

function dataGrid_viInvStokBHP(mod_id_viInvStokBHP){	
    var FieldMaster_viInvStokBHP = 
	[
		'no_minta','tgl_minta', 'jenis_minta','jenis'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viInvStokBHP = new WebApp.DataStore
	({
        fields: FieldMaster_viInvStokBHP
    });
    getDataGridAwal();
    // Grid Gizi Perencanaan # --------------
	GridDataView_viInvStokBHP = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: 'List Barang',
			store: dataSource_viInvStokBHP,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viInvStokBHP = undefined;
							rowSelected_viInvStokBHP = dataSource_viInvStokBHP.getAt(row);
							CurrentData_viInvStokBHP
							CurrentData_viInvStokBHP.row = row;
							CurrentData_viInvStokBHP.data = rowSelected_viInvStokBHP.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viInvStokBHP = dataSource_viInvStokBHP.getAt(ridx);
					if (rowSelected_viInvStokBHP != undefined)
					{
						setLookUp_viInvStokBHP(rowSelected_viInvStokBHP.data);
					}
					else
					{
						setLookUp_viInvStokBHP();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Gizi perencanaan
	        *	Terdiri dari : Judul, Isi dan Event
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header: 'Kode Kelompok',
						dataIndex: 'kd_inv',
						hideable:false,
						menuDisabled: true,
						width: 30
						
					},
					{
						header: 'No. Urut',
						dataIndex: 'no_urut_brg',
						hideable:false,
						menuDisabled: true,
						width: 25
						
					},
					{
						header: 'Nama Barang',
						dataIndex: 'nama_brg',
						hideable:false,
						menuDisabled: true,
						width: 60
					},
					{
						header: 'Satuan',
						dataIndex: 'satuan',
						hideable:false,
						menuDisabled: true,
						width: 30
					},
					{
						header: 'Kelompok',
						dataIndex: 'nama_sub',
						hideable:false,
						menuDisabled: true,
						width: 30
					},
					{
						header: 'Stok Min',
						dataIndex: 'min_stok',
						hideable:false,
						menuDisabled: true,
						width: 30
					},
					{
						header: 'Stok Aktual',
						dataIndex: 'jumlah',
						hideable:false,
						menuDisabled: true,
						width: 30
					},
					//---------------------------------------
					{
						header: 'kd_satuan',
						dataIndex: 'kd_satuan',
						hideable:false,
						menuDisabled: true,
						hidden:true,
						width: 60
					},
					//-------------- ## --------------
					
					//-------------- ## --------------
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viInvStokBHP',
				items: 
				[
					{
						xtype: 'button',
						text: 'Information Stok',
						iconCls: 'find',
						tooltip: 'Kartu Stok',
						id: 'btnViewKartuStok_viInvStokBHP',
						handler: function(sm, row, rec)
						{
							setLookUp_viInvStokBHP();
						}
					},
					{
						xtype: 'button',
						text: 'Edit Pembelian',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viInvStokBHP',
						hidden:true,
						handler: function(sm, row, rec)
						{
							if (rowSelected_viInvStokBHP != undefined)
							{
								setLookUp_viInvStokBHP(rowSelected_viInvStokBHP.data)
							}
							
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viInvStokBHP, selectCount_viInvStokBHP, dataSource_viInvStokBHP),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )
	
	var pencarianInvStokBHP = new Ext.FormPanel({
        labelAlign: 'top',
        title: '',
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [
		{
			layout: 'column',
			border: false,
			items:
			[
				{
					columnWidth:.98,
					layout: 'absolute',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					width: 500,
					height: 40,
					anchor: '100% 100%',
					items:
					[
						{
							x: 10,
							y: 5,
							xtype: 'label',
							text: 'Nama Barang'
						},
						{
							x: 120,
							y: 5,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 5,
							xtype: 'textfield',
							id: 'TxtFilterGridNamaBarangInvStokBHP',
							name: 'TxtFilterGridNamaBarangInvStokBHP',
							width: 350,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										getDataGridAwal(Ext.getCmp('TxtFilterGridNamaBarangInvStokBHP').getValue());
									} 						
								}
							}
						},
						{
							x: 485,
							y: 5,
							xtype: 'label',
							text: '*) Enter untuk mencari'
						},
						{
							x: 610,
							y: 5,
							xtype: 'button',
							text: 'Refresh',
							iconCls: 'refresh',
							tooltip: 'Refresh',
							style:{paddingLeft:'30px'},
							width:140,
							id: 'BtnRefresh_viInvStokBHP',
							handler: function() 
							{					
								getDataGridAwal();
							}                        
						},
					]
				}
			]
		}
		]	
		
	})

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viInvStokBHP = new Ext.Panel
    (
		{
			title: NamaForm_viInvStokBHP,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viInvStokBHP,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ pencarianInvStokBHP,
					GridDataView_viInvStokBHP],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viInvStokBHP,
		            columns: 21,
		            defaults: {
					scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viInvStokBHP;
    //-------------- # End form filter # --------------
}

function refreshInvStokBHP(kriteria)
{
    dataSource_viInvStokBHP.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'ViewPermintaanBahan',
                    param : kriteria
                }			
            }
        );   
    return dataSource_viInvStokBHP;
}

function setLookUp_viInvStokBHP(rowdata){
    var lebar = 790;
    setLookUps_viInvStokBHP = new Ext.Window({
        id: Nci.getId(),
        title: NamaForm_viInvStokBHP, 
        closeAction: 'destroy',        
        width: 800,
        height: 655,
		constrain:true,
		autoHeight:true,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viInvStokBHP(lebar,rowdata),
        listeners:{
            activate: function(){
				
            },
            afterShow: function(){
                this.activate();
            },
            deactivate: function(){
                rowSelected_viInvStokBHP=undefined;
            }
        }
    });

    setLookUps_viInvStokBHP.show();

    if (rowdata == undefined){
	
    }
    else
    {
        datainit_viInvStokBHP(rowdata);
    }
}

function getFormItemEntry_viInvStokBHP(lebar,rowdata){
    var pnlFormDataBasic_viInvStokBHP = new Ext.FormPanel({
		title: '',
		region: 'north',
		layout: 'form',
		bodyStyle: 'padding:10px 10px 10px 10px',
		anchor: '100%',
		labelWidth: 1,
		autoWidth: true,
		width: lebar,
		border: false,
		items:[
				getItemPanelInputPerencanaan_viInvStokBHP(lebar),
				getItemGridPerencanaan_viInvStokBHP(lebar)
			],
			fileUpload: true,
		tbar: {
				xtype: 'toolbar',
				items: 
				[
					/* {
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAdd_viInvStokBHP',
						handler: function(){
							dataaddnew_viInvStokBHP();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						disabled: true,
						id: 'btnSimpan_viInvStokBHP',
						handler: function()
						{
							datasave_viInvStokBHP();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save & Close',
						disabled: true,
						iconCls: 'saveexit',
						id: 'btnSimpanExit_viInvStokBHP',
						handler: function()
						{
							datasave_viInvStokBHP();
							refreshInvStokBHP();
							setLookUps_viInvStokBHP.close();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						text	: 'Delete',
						id		: 'btnHapusPermintaanBahan_InvStokBHP',
						tooltip	: nmLookup,
						iconCls	: 'remove',
						disabled: true,
						handler	: function(){
							Ext.Msg.confirm('Hapus permintaan', 'Apakah permintaan ini akan dihapus?', function(button){
								if (button == 'yes'){
									Ext.Ajax.request
									(
										{
											url: baseURL + "index.php/gizi/functionPermintaanBahan/hapusPermintaan",
											params:{no_minta:Ext.getCmp('txtPONumber_InvStokBHPL').getValue()} ,
											failure: function(o)
											{
												ShowPesanErrorInvStokBHP('Error, hapus permintaan! Hubungi Admin', 'Error');
											},	
											success: function(o) 
											{
												var cst = Ext.decode(o.responseText);
												if (cst.success === true) 
												{
													getDataGridAwal();
													setLookUps_viInvStokBHP.close();
												}
												else 
												{
													ShowPesanErrorInvStokBHP('Tidak dapat menghapus permintaan ini. Bahan dari permintaan ini sudah diterima', 'Error');
												};
											}
										}
										
									)
								}
							});		
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Posting',
						disabled: true,
						iconCls: 'gantidok',
						id: 'btnPosting_viInvStokBHP',
						handler: function()
						{
							dataposting_viInvStokBHP();
						}
					},
					{
						xtype: 'tbseparator'
					} */
					
				]
			}
		}
    )

    return pnlFormDataBasic_viInvStokBHP;
}

function getItemPanelInputPerencanaan_viInvStokBHP(lebar) {
    var items =
	{
		title:'',
		layout:'column',
		items:
		[
			{
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: false,
				width: lebar-22,
				height: 85,
				anchor: '100% 100%',
				items:
				[
					{
						x:10,
						y:5,
						xtype: 'label',
						text:'Kd Kelompok'
					},
					{
						x:80,
						y:5,
						xtype: 'label',
						text:':'
					},
					{
						x:90,
						y:5,
						xtype: 'textfield',
						name: 'txtKdKelompok_InvStokBHPL',
						id: 'txtKdKelompok_InvStokBHPL',
						readOnly:true,
						tabIndex:0,
						width: 150
					},
					{
						x:10,
						y:30,
						xtype: 'label',
						text:'Nama Barang'
					},
					{
						x:80,
						y:30,
						xtype: 'label',
						text:':'
					},
					{
						x:90,
						y:30,
						xtype: 'textfield',
						name: 'txtNamaBarang_InvStokBHPL',
						id: 'txtNamaBarang_InvStokBHPL',
						readOnly:true,
						tabIndex:0,
						width: 250
					},
					{
						x:10,
						y:55,
						xtype: 'label',
						text:'Satuan'
					},
					{
						x:80,
						y:55,
						xtype: 'label',
						text:':'
					},
					{
						x:90,
						y:55,
						xtype: 'textfield',
						name: 'txtSatuan_InvStokBHPL',
						id: 'txtSatuan_InvStokBHPL',
						readOnly:true,
						tabIndex:0,
						width: 150
					},
					//-------------------------------------
					{
						x:245,
						y:5,
						xtype: 'textfield',
						name: 'txtNoUrut_InvStokBHPL',
						id: 'txtNoUrut_InvStokBHPL',
						width: 95
					},
					{
						x:380,
						y:8,
						xtype: 'checkbox',
						boxLabel: 'Filter Bulan',
						name: 'cbFilterBulan_InvStokBHP',
						id: 'cbFilterBulan_InvStokBHP',
						checked: false,
						handler: function (field, value) {
							if (value === true){
								dsDataGrdJab_viInvStokBHP.removeAll();
								getGridStokBarangFilterBulan(InvStokBHP.vars.no_urut_brg,Ext.get('dfBulan_InvStokBHPL').getValue());
							} else{
								dsDataGrdJab_viInvStokBHP.removeAll();
								getGridStokBarang(InvStokBHP.vars.no_urut_brg);
							}
						}
					},
					{
						x:460,
						y:5,
						xtype: 'datefield',
						name: 'dfBulan_InvStokBHPL',
						id	: 'dfBulan_InvStokBHPL',
						format: 'm-Y',
						value:bulan,
						width: 100
					},
					{
						x:380,
						y:30,
						xtype: 'label',
						text:'Stok Min'
					},
					{
						x:450,
						y:30,
						xtype: 'label',
						text:':'
					},
					{
						x:460,
						y:30,
						xtype: 'textfield',
						name: 'txtStokMin_InvStokBHPL',
						id: 'txtStokMin_InvStokBHPL',
						width: 100
					},
					
					
					{
						x:255,
						y:55,
						xtype: 'label',
						text:'Kelompok'
					},
					{
						x:310,
						y:55,
						xtype: 'label',
						text:':'
					},
					{
						x:320,
						y:55,
						xtype: 'textfield',
						name: 'txtKelompok_InvStokBHPL',
						id: 'txtKelompok_InvStokBHPL',
						width: 240
					}
				]
			}
		
		]
	};
    return items;
};

function getItemGridPerencanaan_viInvStokBHP(lebar) 
{
    var items =
	{
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:1px 1px 1px 1px',
		border:true,
		width: lebar-55,
		height: 350,
		tbar:
		[
			/* {
				text	: 'Add Barang',
				id		: 'btnAddDistribusiBarang',
				tooltip	: nmLookup,
				disabled: true,
				iconCls	: 'find',
				handler	: function(){
					var records = new Array();
					records.push(new dsDataGrdJab_viInvStokBHP.recordType());
					dsDataGrdJab_viInvStokBHP.add(records);
				}
			},	
			{
				text	: 'Delete',
				id		: 'btnHapusGridBarang_InvStokBHP',
				tooltip	: nmLookup,
				iconCls	: 'remove',
				disabled: true,
				handler: function()
				{
					var line =  InvStokBHP.form.Grid.a.getSelectionModel().selection.cell[0];
					var o = dsDataGrdJab_viInvStokBHP.getRange()[line].data;
					if(dsDataGrdJab_viInvStokBHP.getCount()>1){
						Ext.Msg.confirm('Warning', 'Apakah bahan ini akan dihapus?', function(button){
							if (button == 'yes'){
								if(dsDataGrdJab_viInvStokBHP.getRange()[line].data.no_minta != undefined){
									Ext.Ajax.request
									(
										{
											url: baseURL + "index.php/gizi/functionPermintaanBahan/hapusBarisGridBahan",
											params: {
												no_minta:Ext.getCmp('txtPONumber_InvStokBHPL').getValue(),
												kd_bahan:o.kd_bahan
											},
											failure: function(o)
											{
												ShowPesanErrorInvStokBHP('Error, hapus bahan! Hubungi Admin', 'Error');
											},	
											success: function(o) 
											{
												var cst = Ext.decode(o.responseText);
												if (cst.success === true) 
												{
													dsDataGrdJab_viInvStokBHP.removeAt(line);
													InvStokBHP.form.Grid.a.getView().refresh();
													Ext.getCmp('btnSimpan_viInvStokBHP').enable();
													Ext.getCmp('btnSimpanExit_viInvStokBHP').enable();
												}
												else 
												{
													ShowPesanErrorInvStokBHP('Gagal menghapus data ini', 'Error');
												};
											}
										}
										
									)
								}else{
									dsDataGrdJab_viInvStokBHP.removeAt(line);
									InvStokBHP.form.Grid.a.getView().refresh();
									Ext.getCmp('btnSimpan_viInvStokBHP').enable();
									Ext.getCmp('btnSimpanExit_viInvStokBHP').enable();
								}
							} 
							
						});
					} else{
						ShowPesanErrorInvStokBHP('Data tidak bisa dihapus karena minimal bahan 1','Error');
					}
					
				}
			}	 */
		],
	    items:
		[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:
				[
					gridDataViewEdit_viInvStokBHP(),
					{
						layout: 'column',
						border: false,
						items:
						[
							{
								columnWidth: .25,
								layout: 'form',
								labelWidth:70,
								bodyStyle:'padding: 5px',
								border: false,
								items:
								[
									{
										xtype: 'textfield',
										fieldLabel: 'Total terima',
										name: 'txtTotalTerima_InvStokBHPL',
										id: 'txtTotalTerima_InvStokBHPL',
										style: 'text-align:right;',
										width: 100
									}
								]
							},
							{
								columnWidth: .25,
								layout: 'form',
								labelWidth:70,
								bodyStyle:'padding: 5px',
								border: false,
								items:
								[
									{
										xtype: 'textfield',
										fieldLabel: 'Aktual Stok',
										name: 'txtAktualSok_InvStokBHPL',
										id: 'txtAktualSok_InvStokBHPL',
										width: 100,
										style: 'text-align:right;'
									},
								]
							},
							{
								columnWidth: .10,
								layout: 'form',
								labelWidth:0,
								bodyStyle:'padding: 5px',
								border: false,
								items:
								[
									{
										xtype: 'label',
										text: 'Status'
									},
								]
							},
							{
								columnWidth: .10,
								layout: 'form',
								bodyStyle:'padding: 5px',
								border: false,
								items:
								[
									InvStokBHP.form.Panel.a=new Ext.Panel ({
										region: 'north',
										border: false,
										text: 'Status',
										html: '<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>'
									}),
								]
							}
						]
					}
				]	
			}
			//-------------- ## --------------
		],
	};
    return items;
};

function gridDataViewEdit_viInvStokBHP()
{
    var FieldGrdKasir_viInvStokBHP = [];
	
    dsDataGrdJab_viInvStokBHP= new WebApp.DataStore
	({
        fields: FieldGrdKasir_viInvStokBHP
    });
    
    InvStokBHP.form.Grid.a =new Ext.grid.EditorGridPanel({
        store: dsDataGrdJab_viInvStokBHP,
        height: 290,//220,
		stripeRows: true,
		columnLines: true,
		selModel: new Ext.grid.CellSelectionModel({
	            singleSelect: true,
	            listeners: {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
        }),
        
        columns: 
		[	
			new Ext.grid.RowNumberer(),
			{			
				dataIndex: 'kd_stok',
				header: 'Stok',
				sortable: true,
				width: 70
			},
			{			
				dataIndex: 'no_terima',
				header: 'Recv',
				sortable: true,
				width: 70
			},
			{			
				dataIndex: 'tgl_posting_format',
				header: 'Tanggal Update',
				sortable: true,
				width: 80,
				renderer: function(v, params, record)
				{
					// return ShowDate(record.data.tgl_posting);
					return record.data.tgl_posting_format;
					// console.log(record.data.tgl_posting.length);
					
				}
			},

			{
				dataIndex: 'total',
				header: 'Total',
				align:'right',
				xtype:'numbercolumn',
				sortable: true,
				width: 80
			},
			
			{			
				dataIndex: 'jumlah',
				header: 'Dist',
				sortable: true,
				align:'right',
				width: 90
			},
			{			
				dataIndex: 'jumlah_total',
				header: 'Sisa',
				sortable: true,
				align:'right',
				xtype:'numbercolumn',
				width: 80
			},
			
			//-------------- ## --------------
			//-------------- ## --------------
        ],
		viewConfig: 
			{
				forceFit: true
			}

       // plugins:chkSelected_viInvStokBHP,
    });
    return  InvStokBHP.form.Grid.a;
}

function hasilJumlahStokBHP(){
	var aktualstok=0;
	var totalterima=0;
	
	for(var i=0; i < dsDataGrdJab_viInvStokBHP.getCount() ; i++){
		
		var o=dsDataGrdJab_viInvStokBHP.getRange()[i].data;
		if(o.jumlah_total != undefined){
			aktualstok += parseInt(o.jumlah_total);
			totalterima += parseInt(o.total);
		}
	}
	
	Ext.getCmp('txtTotalTerima_InvStokBHPL').setValue(toFormat(totalterima));
	Ext.getCmp('txtAktualSok_InvStokBHPL').setValue(toFormat(aktualstok));
	
	InvStokBHP.form.Grid.a.getView().refresh();
	
	if(aktualstok > Ext.getCmp('txtStokMin_InvStokBHPL').getValue()){
		InvStokBHP.form.Panel.a.update('<img src="'+baseURL+'ui/images/icons/16x16/hijau.png"/>');
	} else{
		InvStokBHP.form.Panel.a.update('<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>');
	}
}


function comboJenisLokasiInvStokBHP(){
	var Field_Vendor = ['kd_jns_lokasi', 'jns_lokasi'];
    ds_JnsLokasiInvStokBHP = new WebApp.DataStore({fields: Field_Vendor});
    ds_JnsLokasiInvStokBHP.load({
        params:{
	        Skip: 0,
	        Take: 1000,
	        Sort: 'kd_jns_lokasi',
	        Sortdir: 'ASC',
	        target: 'ComboJnsLokasiBHP',
	        param: ""
        }
    });
	var cboJnsLokasiInvStokBHP = new Ext.form.ComboBox({
		x:80,
		y:30,
        id:'cbJnsLokasiInvStokBHP',
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:true,
        mode: 'local',
        width: 260,
		readOnly:false,
		fieldLabel:'PBF ',
        store: ds_JnsLokasiInvStokBHP,
        valueField: 'kd_jns_lokasi',
        displayField: 'jns_lokasi',
        listeners:{
			'select': function(a,b,c){
				loaddatastoreLokasiInvDistribusBHP(b.data.kd_jns_lokasi);
				Ext.getCmp('cbLokasiInvStokBHP').enable();
			}
        }
	});
	return cboJnsLokasiInvStokBHP;
};

function loaddatastoreLokasiInvDistribusBHP(kd_jns_lokasi)
{
    dslokasiInvStokBHP.load
	(
		{
		 params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'nama',
					Sortdir: 'ASC',
					target: 'ViewComboBayar',
					param: 'kd_jns_lokasi='+ kd_jns_lokasi+ ' order by lokasi'
				}
	   }
	)      
};

function comboLokasiInvStokBHP()
{
    var Field = ['kd_jns_lokasi','lokasi','kd_lokasi'];

    dslokasiInvStokBHP = new WebApp.DataStore({fields: Field});
	
    var cboLokasiInvStokBHP = new Ext.form.ComboBox
	(
		{
			x : 80,
			y : 60,
		    id: 'cbLokasiInvStokBHP',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
            forceSelection: true,
		    labelWidth:80,
		    align: 'Right',
		    store: dslokasiInvStokBHP,
		    valueField: 'kd_lokasi',
		    displayField: 'lokasi',
			disabled:true,
			width:200,
			listeners:
			{
			    'select': function(a, b, c) 
				{
					
				}
			}
		}
	);

    return cboLokasiInvStokBHP;
};



function dataaddnew_viInvStokBHP(){
	Ext.getCmp('txtPONumber_InvStokBHPL').setValue('');
	Ext.getCmp('dfTglRo_InvStokBHPL').setValue(now_viInvStokBHP);
	Ext.getCmp('cbJnsLokasiInvStokBHP').setValue('');
	
	Ext.getCmp('cbJnsLokasiInvStokBHP').setReadOnly(false);
	
	dsDataGrdJab_viInvStokBHP.removeAll();
}

function datainit_viInvStokBHP(rowdata)
{
	Ext.getCmp('txtKdKelompok_InvStokBHPL').setValue(rowdata.kd_inv);
	Ext.getCmp('txtNoUrut_InvStokBHPL').setValue(rowdata.no_urut_brg);
	Ext.getCmp('txtNamaBarang_InvStokBHPL').setValue(rowdata.nama_brg);
	Ext.getCmp('txtStokMin_InvStokBHPL').setValue(rowdata.min_stok);
	Ext.getCmp('txtKelompok_InvStokBHPL').setValue(rowdata.nama_sub);
	Ext.getCmp('txtSatuan_InvStokBHPL').setValue(rowdata.satuan);
	
	Ext.getCmp('txtNoUrut_InvStokBHPL').setReadOnly(true);
	Ext.getCmp('txtNamaBarang_InvStokBHPL').setReadOnly(true);
	Ext.getCmp('txtStokMin_InvStokBHPL').setReadOnly(true);
	Ext.getCmp('txtKelompok_InvStokBHPL').setReadOnly(true);
	Ext.getCmp('txtSatuan_InvStokBHPL').setReadOnly(true);
	//Ext.getCmp('cbJnsLokasiInvStokBHP').setReadOnly(true);
	
	getGridStokBarang(rowdata.no_urut_brg);
	InvStokBHP.vars.no_urut_brg=rowdata.no_urut_brg;
	
	
};

function datasave_viInvStokBHP(){
	if (ValidasiEntryInvStokBHP(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/gizi/functionPermintaanBahan/save",
				params: getParamInvStokBHP(),
				failure: function(o)
				{
					ShowPesanErrorInvStokBHP('Error, simpan data! Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoInvStokBHP('Data berhasil disimpan','Information');
						Ext.getCmp('txtPONumber_InvStokBHPL').setValue(cst.nominta);
						getDataGridAwal();
						Ext.getCmp('btnSimpan_viInvStokBHP').enable();
						Ext.getCmp('btnSimpanExit_viInvStokBHP').enable();
						Ext.getCmp('btnHapusPermintaanBahan_InvStokBHP').enable();
					}
					else 
					{
						ShowPesanErrorInvStokBHP('Gagal Menyimpan Data ini', 'Error');
					};
				}
			}
			
		)
	}
}

function getDataGridAwal(nama){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/bhp/functionStok/getGridBarang",
			params: {nama:nama},
			failure: function(o)
			{
				ShowPesanErrorInvStokBHP('Error, membaca grid awal! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					dataSource_viInvStokBHP.removeAll();
					
					var recs=[],
						recType=dataSource_viInvStokBHP.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dataSource_viInvStokBHP.add(recs);
					
					GridDataView_viInvStokBHP.getView().refresh();
				}
				else 
				{
					ShowPesanErrorInvStokBHP('Gagal membaca data obat', 'Error');
				};
			}
		}
		
	)
	
}


function getGridStokBarang(no_urut_brg){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/bhp/functionStok/getGridDetailStok",
			params: {no_urut_brg:no_urut_brg},
			failure: function(o)
			{
				ShowPesanErrorInvStokBHP('Error, membaca grid barang! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dsDataGrdJab_viInvStokBHP.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dsDataGrdJab_viInvStokBHP.add(recs);
					
					InvStokBHP.form.Grid.a.getView().refresh();
					hasilJumlahStokBHP();
				}
				else 
				{
					ShowPesanErrorInvStokBHP('Gagal membaca data obat', 'Error');
				};
			}
		}
		
	)
	
}

function getGridStokBarangFilterBulan(no_urut_brg,bulan){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/bhp/functionStok/getGridDetailStokFilter",
			params: {no_urut_brg:no_urut_brg,bulan:bulan},
			failure: function(o)
			{
				ShowPesanErrorInvStokBHP('Error, membaca grid barang! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dsDataGrdJab_viInvStokBHP.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dsDataGrdJab_viInvStokBHP.add(recs);
					
					InvStokBHP.form.Grid.a.getView().refresh();
					hasilJumlahStokBHP();
				}
				else 
				{
					ShowPesanErrorInvStokBHP('Gagal membaca data obat', 'Error');
				};
			}
		}
		
	)
	
}

function cekTerimaBahan(no_minta){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionPermintaanBahan/cekTerimaBahan",
			params: {no_minta:no_minta},
			failure: function(o)
			{
				ShowPesanErrorInvStokBHP('Error, cek! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					Ext.getCmp('btnAddDistribusiBarang').enable();
					Ext.getCmp('btnHapusGridBarang_InvStokBHP').enable();
					Ext.getCmp('btnSimpan_viInvStokBHP').enable();
					Ext.getCmp('btnSimpanExit_viInvStokBHP').enable();
					Ext.getCmp('btnHapusPermintaanBahan_InvStokBHP').enable();
				}
				else 
				{
					ShowPesanInfoInvStokBHP('Bahan-bahan dari permintaan ini sudah diterima', 'Information');
					Ext.getCmp('btnSimpan_viInvStokBHP').disable(true);
					Ext.getCmp('btnSimpanExit_viInvStokBHP').disable(true);
					Ext.getCmp('btnHapusPermintaanBahan_InvStokBHP').disable(true);
					Ext.getCmp('btnAddDistribusiBarang').disable(true);
					Ext.getCmp('btnHapusGridBarang_InvStokBHP').disable(true);
				};
			}
		}
		
	)
}

function getParamInvStokBHP() 
{
	var	params =
	{
		NoMinta:Ext.getCmp('txtPONumber_InvStokBHPL').getValue(),
		TglMinta:Ext.getCmp('dfTglRo_InvStokBHPL').getValue(),
		JenisMinta:Ext.getCmp('cbJnsLokasiInvStokBHP').getValue()
	}
	
	params['jumlah']=dsDataGrdJab_viInvStokBHP.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viInvStokBHP.getCount();i++)
	{
		params['kd_bahan-'+i]=dsDataGrdJab_viInvStokBHP.data.items[i].data.kd_bahan
		params['nama_bahan-'+i]=dsDataGrdJab_viInvStokBHP.data.items[i].data.nama_bahan
		params['qty-'+i]=dsDataGrdJab_viInvStokBHP.data.items[i].data.qty
		params['ket_spek-'+i]=dsDataGrdJab_viInvStokBHP.data.items[i].data.ket_spek
	}
    return params
};

function ValidasiEntryInvStokBHP(modul,mBolHapus)
{
	var x = 1;
	if(Ext.getCmp('cbJnsLokasiInvStokBHP').getValue() === ''){
		ShowPesanWarningInvStokBHP('Jenis minta tidak boleh kosong', 'Warning');
		x = 0;
	}
	if(dsDataGrdJab_viInvStokBHP.getCount() < 0){
		ShowPesanWarningInvStokBHP('Daftar permintaan bahan tidak boleh kosong, minimal bahan 1', 'Warning');
		x = 0;
	}
	
	for(var i=0; i<dsDataGrdJab_viInvStokBHP.getCount() ; i++){
		var o=dsDataGrdJab_viInvStokBHP.getRange()[i].data;
		if(o.qty == undefined || o.qty <= 0){
			ShowPesanWarningInvStokBHP('Qty tidak boleh 0, minimal qty 1', 'Warning');
			x = 0;
		}
		for(var j=i+1; j<dsDataGrdJab_viInvStokBHP.getCount() ; j++){
			var p=dsDataGrdJab_viInvStokBHP.getRange()[j].data;
			if(o.kd_bahan == p.kd_bahan){
				ShowPesanWarningInvStokBHP('Bahan tidak boleh sama, periksa kembali daftar permintaan bahan', 'Warning');
				x = 0;
				break;
			}
		}
	}
	
	return x;
};



function ShowPesanWarningInvStokBHP(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorInvStokBHP(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};


function ShowPesanInfoInvStokBHP(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};