var dataSource_viInvStokOpnameBHP;
var selectCount_viInvStokOpnameBHP=50;
var NamaForm_viInvStokOpnameBHP="Stok Opname";
var mod_name_viInvStokOpnameBHP="viInvStokOpnameBHP";
var now_viInvStokOpnameBHP= new Date();
var rowSelected_viInvStokOpnameBHP;
var setLookUps_viInvStokOpnameBHP;
var tanggal = now_viInvStokOpnameBHP.format("d/M/Y");
var jam = now_viInvStokOpnameBHP.format("H/i/s");
var tmpkriteria;
var GridDataView_viInvStokOpnameBHP;
var ctglawal_viInvStokOpnameBHP;
var ctglakhir_viInvStokOpnameBHP;
var ckdinv_viInvStokOpnameBHP;
var ckdnama_viInvStokOpnameBHP;


var CurrentData_viInvStokOpnameBHP =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viInvStokOpnameBHP(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

var InvStokOpnameBHP={};
InvStokOpnameBHP.form={};
InvStokOpnameBHP.func={};
InvStokOpnameBHP.vars={};
InvStokOpnameBHP.func.parent=InvStokOpnameBHP;
InvStokOpnameBHP.form.ArrayStore={};
InvStokOpnameBHP.form.ComboBox={};
InvStokOpnameBHP.form.DataStore={};
InvStokOpnameBHP.form.Record={};
InvStokOpnameBHP.form.Form={};
InvStokOpnameBHP.form.Grid={};
InvStokOpnameBHP.form.Panel={};
InvStokOpnameBHP.form.TextField={};
InvStokOpnameBHP.form.Button={};

InvStokOpnameBHP.form.ArrayStore.barang= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_inv', 'nama_sub', 'kd_stok','no_urut_brg','nama_brg', 'satuan','tgl_posting', 'min_stok', 'jumlah_total'],
	data: []
});

function dataGrid_viInvStokOpnameBHP(mod_id_viInvStokOpnameBHP){	
    var FieldMaster_viInvStokOpnameBHP = 
	[
		'no_minta','tgl_minta', 'jenis_minta','jenis'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viInvStokOpnameBHP = new WebApp.DataStore
	({
        fields: FieldMaster_viInvStokOpnameBHP
    });
    getDataGridAwalInvStokOpname();
    // Grid Gizi Perencanaan # --------------
	GridDataView_viInvStokOpnameBHP = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: 'Daftar Stok Opname',
			store: dataSource_viInvStokOpnameBHP,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viInvStokOpnameBHP = undefined;
							rowSelected_viInvStokOpnameBHP = dataSource_viInvStokOpnameBHP.getAt(row);
							CurrentData_viInvStokOpnameBHP
							CurrentData_viInvStokOpnameBHP.row = row;
							CurrentData_viInvStokOpnameBHP.data = rowSelected_viInvStokOpnameBHP.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viInvStokOpnameBHP = dataSource_viInvStokOpnameBHP.getAt(ridx);
					if (rowSelected_viInvStokOpnameBHP != undefined)
					{
						setLookUp_viInvStokOpnameBHP(rowSelected_viInvStokOpnameBHP.data);
					}
					else
					{
						setLookUp_viInvStokOpnameBHP();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Gizi perencanaan
	        *	Terdiri dari : Judul, Isi dan Event
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header: 'Kode Kelompok',
						dataIndex: 'kd_inv',
						hideable:false,
						menuDisabled: true,
						width: 25
						
					},
					{
						header: 'No Urut',
						dataIndex: 'no_urut_brg',
						hideable:false,
						menuDisabled: true,
						width: 25
						
					},
					{
						header: 'Nama Barang',
						dataIndex: 'nama_brg',
						hideable:false,
						menuDisabled: true,
						width: 60
						
					},
					{
						header: 'Tanggal Stok Opname',
						dataIndex: 'tgl_adjust',
						hideable:false,
						menuDisabled: true,
						width: 25,
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.tgl_adjust);
						}
					},
					{
						header: 'Stok',
						dataIndex: 'jumlah_total',
						hideable:false,
						align:'right',
						menuDisabled: true,
						width: 30
					},
					{
						header: 'Stok Opname',
						dataIndex: 'adjust_qty',
						hideable:false,
						align:'right',
						menuDisabled: true,
						width: 30
					},
					//-------------- ## --------------
					
					//-------------- ## --------------
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viInvStokOpnameBHP',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add Stok Opname',
						iconCls: 'Edit_Tr',
						tooltip: 'Add Data',
						id: 'btnTambah_viInvStokOpnameBHP',
						handler: function(sm, row, rec)
						{
							
							setLookUp_viInvStokOpnameBHP();
						}
					},
					{
						xtype: 'button',
						text: 'Edit Stok Opname',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viInvStokOpnameBHP',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viInvStokOpnameBHP != undefined)
							{
								setLookUp_viInvStokOpnameBHP(rowSelected_viInvStokOpnameBHP.data)
							}
							
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viInvStokOpnameBHP, selectCount_viInvStokOpnameBHP, dataSource_viInvStokOpnameBHP),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )
	
	var pencarianInvStokOpnameBHP = new Ext.FormPanel({
        labelAlign: 'top',
        frame:true,
        title: '',
        bodyStyle:'padding:5px 5px 0',
        items: [
		{
			layout: 'column',
			border: false,
			items:
			[
				{
					columnWidth:.98,
					layout: 'absolute',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					width: 500,
					height: 120,
					anchor: '100% 100%',
					items:
					[
						{
							x: 10,
							y: 0,
							xtype: 'label',
							text: 'Kode Kelompok'
						},
						{
							x: 120,
							y: 0,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 0,
							xtype: 'textfield',
							id: 'TxtFilterGridKodeKelompokInvStokOpnameBHP',
							name: 'TxtFilterGridKodeKelompokInvStokOpnameBHP',
							width: 350,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										ckdinv_viInvStokOpnameBHP=Ext.getCmp('TxtFilterGridKodeKelompokInvStokOpnameBHP').getValue();
										ckdnama_viInvStokOpnameBHP=Ext.getCmp('TxtFilterGridNamaBarangInvStokOpnameBHP').getValue();
										ctglawal_viInvStokOpnameBHP=Ext.getCmp('dfTglAwalInvStokOpnameBHP').getValue();
										ctglakhir_viInvStokOpnameBHP=Ext.getCmp('dfTglAkhirInvStokOpnameBHP').getValue();
										getDataGridAwalInvStokOpname(ckdinv_viInvStokOpnameBHP,ckdnama_viInvStokOpnameBHP,ctglawal_viInvStokOpnameBHP,ctglakhir_viInvStokOpnameBHP);
									} 						
								}
							}
						},
						{
							x: 10,
							y: 30,
							xtype: 'label',
							text: 'Nama Barang'
						},
						{
							x: 120,
							y: 30,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 30,
							xtype: 'textfield',
							id: 'TxtFilterGridNamaBarangInvStokOpnameBHP',
							name: 'TxtFilterGridNamaBarangInvStokOpnameBHP',
							width: 350,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										ckdinv_viInvStokOpnameBHP=Ext.getCmp('TxtFilterGridKodeKelompokInvStokOpnameBHP').getValue();
										ckdnama_viInvStokOpnameBHP=Ext.getCmp('TxtFilterGridNamaBarangInvStokOpnameBHP').getValue();
										ctglawal_viInvStokOpnameBHP=Ext.getCmp('dfTglAwalInvStokOpnameBHP').getValue();
										ctglakhir_viInvStokOpnameBHP=Ext.getCmp('dfTglAkhirInvStokOpnameBHP').getValue();
										getDataGridAwalInvStokOpname(ckdinv_viInvStokOpnameBHP,ckdnama_viInvStokOpnameBHP,ctglawal_viInvStokOpnameBHP,ctglakhir_viInvStokOpnameBHP);
									} 						
								}
							}
						},
						//----------------------------------------
						{
							x: 10,
							y: 90,
							xtype: 'label',
							text: '*) Enter untuk mencari'
						},
						//----------------------------------------
						{
							x: 580,
							y: 90,
							xtype: 'button',
							text: 'Refresh',
							iconCls: 'refresh',
							tooltip: 'Refresh',
							style:{paddingLeft:'30px'},
							width:150,
							id: 'BtnFilterGridCari_viInvStokOpnameBHP',
							handler: function() 
							{					
								getDataGridAwalInvStokOpname();
							}                        
						},
						{
							x: 10,
							y: 60,
							xtype: 'label',
							text: 'Tanggal Posting'
						},
						{
							x: 120,
							y: 60,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 60,
							xtype: 'datefield',
							id: 'dfTglAwalInvStokOpnameBHP',
							format: 'd/M/Y',
							value:now_viInvStokOpnameBHP,
							width: 150,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										ckdinv_viInvStokOpnameBHP=Ext.getCmp('TxtFilterGridKodeKelompokInvStokOpnameBHP').getValue();
										ckdnama_viInvStokOpnameBHP=Ext.getCmp('TxtFilterGridNamaBarangInvStokOpnameBHP').getValue();
										ctglawal_viInvStokOpnameBHP=Ext.getCmp('dfTglAwalInvStokOpnameBHP').getValue();
										ctglakhir_viInvStokOpnameBHP=Ext.getCmp('dfTglAkhirInvStokOpnameBHP').getValue();
										getDataGridAwalInvStokOpname(ckdinv_viInvStokOpnameBHP,ckdnama_viInvStokOpnameBHP,ctglawal_viInvStokOpnameBHP,ctglakhir_viInvStokOpnameBHP);
									} 						
								}
							}
						},
						{
							x: 290,
							y: 60,
							xtype: 'label',
							text: 's/d'
						},
						{
							x: 320,
							y: 60,
							xtype: 'datefield',
							id: 'dfTglAkhirInvStokOpnameBHP',
							format: 'd/M/Y',
							value:now_viInvStokOpnameBHP,
							width: 150,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										ckdinv_viInvStokOpnameBHP=Ext.getCmp('TxtFilterGridKodeKelompokInvStokOpnameBHP').getValue();
										ckdnama_viInvStokOpnameBHP=Ext.getCmp('TxtFilterGridNamaBarangInvStokOpnameBHP').getValue();
										ctglawal_viInvStokOpnameBHP=Ext.getCmp('dfTglAwalInvStokOpnameBHP').getValue();
										ctglakhir_viInvStokOpnameBHP=Ext.getCmp('dfTglAkhirInvStokOpnameBHP').getValue();
										getDataGridAwalInvStokOpname(ckdinv_viInvStokOpnameBHP,ckdnama_viInvStokOpnameBHP,ctglawal_viInvStokOpnameBHP,ctglakhir_viInvStokOpnameBHP);
									} 						
								}
							}
						}
					]
				}
			]
		}
		]	
		
	})

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viInvStokOpnameBHP = new Ext.Panel
    (
		{
			title: NamaForm_viInvStokOpnameBHP,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viInvStokOpnameBHP,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ pencarianInvStokOpnameBHP,
					GridDataView_viInvStokOpnameBHP],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viInvStokOpnameBHP,
		            columns: 21,
		            defaults: {
					scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viInvStokOpnameBHP;
    //-------------- # End form filter # --------------
}

function setLookUp_viInvStokOpnameBHP(rowdata){
    var lebar = 790;
    setLookUps_viInvStokOpnameBHP = new Ext.Window({
        id: Nci.getId(),
        title: NamaForm_viInvStokOpnameBHP, 
        closeAction: 'destroy',        
        width: 800,
        height: 655,
		constrain:true,
		autoHeight:true,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viInvStokOpnameBHP(lebar,rowdata),
        listeners:{
            activate: function(){
				
            },
            afterShow: function(){
                this.activate();
            },
            deactivate: function(){
                rowSelected_viInvStokOpnameBHP=undefined;
            }
        }
    });

    setLookUps_viInvStokOpnameBHP.show();

    if (rowdata == undefined){
	
    }
    else
    {
        datainit_viInvStokOpnameBHP(rowdata);
    }
}

function getFormItemEntry_viInvStokOpnameBHP(lebar,rowdata){
    var pnlFormDataBasic_viInvStokOpnameBHP = new Ext.FormPanel({
		title: '',
		region: 'north',
		layout: 'form',
		bodyStyle: 'padding:10px 10px 10px 10px',
		anchor: '100%',
		labelWidth: 1,
		autoWidth: true,
		width: lebar,
		border: false,
		items:[
				getItemPanelInputPerencanaan_viInvStokOpnameBHP(lebar),
				getItemGridPerencanaan_viInvStokOpnameBHP(lebar)
			],
			fileUpload: true,
		tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAdd_viInvStokOpnameBHP',
						handler: function(){
							dataaddnew_viInvStokOpnameBHP();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						disabled: true,
						id: 'btnSimpan_viInvStokOpnameBHP',
						handler: function()
						{
							datasave_viInvStokOpnameBHP();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save & Close',
						disabled: true,
						iconCls: 'saveexit',
						id: 'btnSimpanExit_viInvStokOpnameBHP',
						handler: function()
						{
							datasave_viInvStokOpnameBHP();
							refreshInvStokOpnameBHP();
							setLookUps_viInvStokOpnameBHP.close();
						}
					},
					{
						xtype: 'tbseparator'
					},
					
					
				]
			}
		}
    )

    return pnlFormDataBasic_viInvStokOpnameBHP;
}

function getItemPanelInputPerencanaan_viInvStokOpnameBHP(lebar) {
    var items =
	{
		title:'',
		layout:'column',
		items:
		[
			{
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: false,
				width: lebar-22,
				height: 85,
				anchor: '100% 100%',
				items:
				[
					{
						x:10,
						y:5,
						xtype: 'label',
						text:'Kd Kelompok'
					},
					{
						x:80,
						y:5,
						xtype: 'label',
						text:':'
					},
					{
						x:90,
						y:5,
						xtype: 'textfield',
						name: 'txtKdKelompok_InvStokOpnameBHPL',
						id: 'txtKdKelompok_InvStokOpnameBHPL',
						readOnly:true,
						tabIndex:0,
						width: 110
					},
					{
						x:10,
						y:30,
						xtype: 'label',
						text:'Nama Barang'
					},
					{
						x:80,
						y:30,
						xtype: 'label',
						text:':'
					},
					/* {
						x:90,
						y:30,
						xtype: 'textfield',
						name: 'txtNamaBarang_InvStokOpnameBHPL',
						id: 'txtNamaBarang_InvStokOpnameBHPL',
						tabIndex:0,
						width: 220
					}, */
					InvStokOpnameBHP.form.ComboBox.namabarang = new Nci.form.Combobox.autoComplete({
						store	: InvStokOpnameBHP.form.ArrayStore.barang,
						select	: function(a,b,c){
							Ext.getCmp('txtKdKelompok_InvStokOpnameBHPL').setValue(b.data.kd_inv);
							Ext.getCmp('txtSatuan_InvStokOpnameBHPL').setValue(b.data.satuan);
							Ext.getCmp('txtNoUrut_InvStokOpnameBHPL').setValue(b.data.no_urut_brg);
							Ext.getCmp('dfTglPosting_InvStokOpnameBHPL').setValue(ShowDate(b.data.tgl_posting));
							Ext.getCmp('txtStokMin_InvStokOpnameBHPL').setValue(b.data.min_stok);
							Ext.getCmp('txtKelompok_InvStokOpnameBHPL').setValue(b.data.nama_sub);
							
							getGridDetailStokBarang(b.data.nama_brg);
							
							Ext.getCmp('btnSimpan_viInvStokOpnameBHP').enable();
							Ext.getCmp('btnSimpanExit_viInvStokOpnameBHP').enable();
							Ext.getCmp('btnAdd_viInvStokOpnameBHP').enable();
						},
						insert	: function(o){
							return {
								kd_inv				: o.kd_inv,
								nama_sub			: o.nama_sub,
								kd_stok				: o.kd_stok,
								no_urut_brg			: o.no_urut_brg,
								nama_brg			: o.nama_brg,
								satuan				: o.satuan,
								tgl_posting			: o.tgl_posting,
								min_stok			: o.min_stok,
								jumlah_total		: o.jumlah_total,
								text				:  '<table style="font-size: 11px;"><tr><td width="100">'+o.kd_inv+'</td><td width="90">'+o.no_urut_brg+'</td><td width="200">'+o.nama_brg+'</td></tr></table>',
							}
						},
						url		: baseURL + "index.php/bhp/functionStokOpname/getBarang",
						valueField: 'nama_brg',
						displayField: 'text',
						listWidth: 390,
						width	: 220,
						x:90,
						y:30,
					}),
					{
						x:10,
						y:55,
						xtype: 'label',
						text:'Satuan'
					},
					{
						x:80,
						y:55,
						xtype: 'label',
						text:':'
					},
					{
						x:90,
						y:55,
						xtype: 'textfield',
						name: 'txtSatuan_InvStokOpnameBHPL',
						id: 'txtSatuan_InvStokOpnameBHPL',
						readOnly:true,
						tabIndex:0,
						width: 110
					},
					//-------------------------------------
					{
						x:210,
						y:5,
						xtype: 'textfield',
						name: 'txtNoUrut_InvStokOpnameBHPL',
						id: 'txtNoUrut_InvStokOpnameBHPL',
						readOnly:true,
						tabIndex:0,
						width: 100
					},
					//----------------------------------------------
					{
						x:360,
						y:5,
						xtype: 'label',
						text:'Tanggal Posting'
					},
					{
						x:440,
						y:5,
						xtype: 'label',
						text:':'
					},
					{
						x:450,
						y:5,
						xtype: 'datefield',
						name: 'dfTglPosting_InvStokOpnameBHPL',
						id	: 'dfTglPosting_InvStokOpnameBHPL',
						format: 'd/M/Y',
						value:now_viInvStokOpnameBHP,
						readOnly:true,
						width: 130
					},
					{
						x:360,
						y:30,
						xtype: 'label',
						text:'Stok Min'
					},
					{
						x:440,
						y:30,
						xtype: 'label',
						text:':'
					},
					{
						x:450,
						y:30,
						xtype: 'textfield',
						name: 'txtStokMin_InvStokOpnameBHPL',
						id: 'txtStokMin_InvStokOpnameBHPL',
						readOnly:true,
						width: 130
					},
					{
						x:360,
						y:55,
						xtype: 'label',
						text:'Kelompok'
					},
					{
						x:440,
						y:55,
						xtype: 'label',
						text:':'
					},
					{
						x:450,
						y:55,
						xtype: 'textfield',
						name: 'txtKelompok_InvStokOpnameBHPL',
						id: 'txtKelompok_InvStokOpnameBHPL',
						readOnly:true,
						tabIndex:0,
						width: 290
					},
					//-------------------------------------------------
				]
			}
		
		]
	};
    return items;
};

function getItemGridPerencanaan_viInvStokOpnameBHP(lebar) 
{
    var items =
	{
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:1px 1px 1px 1px',
		border:true,
		width: lebar-55,
		height: 350,
		tbar:
		[
		],
	    items:
		[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:
				[
					gridDataViewEdit_viInvStokOpnameBHP(),
				]	
			}
			//-------------- ## --------------
		],
	};
    return items;
};

function gridDataViewEdit_viInvStokOpnameBHP()
{
    var FieldGrdKasir_viInvStokOpnameBHP = [];
	
    dsDataGrdJab_viInvStokOpnameBHP= new WebApp.DataStore
	({
        fields: FieldGrdKasir_viInvStokOpnameBHP
    });
    
    InvStokOpnameBHP.form.Grid.a =new Ext.grid.EditorGridPanel({
        store: dsDataGrdJab_viInvStokOpnameBHP,
        height: 340,//220,
		stripeRows: true,
		columnLines: true,
		selModel: new Ext.grid.CellSelectionModel({
	            singleSelect: true,
	            listeners: {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
        }),
        
        columns: 
		[	
			new Ext.grid.RowNumberer(),
			{			
				dataIndex: 'kd_stok',
				header: 'Kode Stok',
				sortable: true,
				width: 80
			},
			{			
				dataIndex: 'no_urut_brg',
				header: 'Kode',
				sortable: true,
				width: 80
			},
			{			
				dataIndex: 'nama_brg',
				header: 'Nama Barang',
				sortable: true,
				width: 200
			},
			{			
				dataIndex: 'satuan',
				header: 'Satuan',
				sortable: true,
				width: 80
			},
			{			
				dataIndex: 'tgl_posting',
				header: 'Tanggal Posting',
				sortable: true,
				width: 100
			},
			{
				dataIndex: 'jumlah_total',
				header: 'Stok Buku',
				align:'right',
				xtype:'numbercolumn',
				sortable: true,
				width: 80
			},
			{
				dataIndex: 'adjust_qty',
				header: 'Stok Real',
				align:'right',
				xtype:'numbercolumn',
				sortable: true,
				width: 80,
				editor: new Ext.form.NumberField({
					allowBlank: false
				})
			},
			//-------------- HIDDEN --------------
			{			
				dataIndex: 'kd_satuan',
				header: 'kd_satuan',
				sortable: true,
				width: 80,
				hidden:true
			},
			{			
				dataIndex: 'kd_inv',
				header: 'Kode Kelompok',
				sortable: true,
				hidden:true,
				width: 80
			},
			//-------------- ## --------------
        ],
		viewConfig: 
			{
				forceFit: true
			}

       // plugins:chkSelected_viInvStokOpnameBHP,
    });
    return  InvStokOpnameBHP.form.Grid.a;
}



function dataaddnew_viInvStokOpnameBHP(){
	Ext.getCmp('txtKdKelompok_InvStokOpnameBHPL').setValue('');
	Ext.getCmp('txtSatuan_InvStokOpnameBHPL').setValue('');
	Ext.getCmp('txtNoUrut_InvStokOpnameBHPL').setValue('');
	Ext.getCmp('dfTglPosting_InvStokOpnameBHPL').setValue(now_viInvStokOpnameBHP);
	Ext.getCmp('txtStokMin_InvStokOpnameBHPL').setValue('');
	Ext.getCmp('txtKelompok_InvStokOpnameBHPL').setValue('');
	InvStokOpnameBHP.form.ComboBox.namabarang.setValue('');
	
	dsDataGrdJab_viInvStokOpnameBHP.removeAll();
}

function datainit_viInvStokOpnameBHP(rowdata)
{
	Ext.getCmp('txtKdKelompok_InvStokOpnameBHPL').setValue(rowdata.kd_inv);
	Ext.getCmp('txtSatuan_InvStokOpnameBHPL').setValue(rowdata.satuan);
	Ext.getCmp('txtNoUrut_InvStokOpnameBHPL').setValue(rowdata.no_urut_brg);
	Ext.getCmp('dfTglPosting_InvStokOpnameBHPL').setValue(ShowDate(rowdata.tgl_posting));
	Ext.getCmp('txtStokMin_InvStokOpnameBHPL').setValue(rowdata.min_stok);
	Ext.getCmp('txtKelompok_InvStokOpnameBHPL').setValue(rowdata.nama_sub);
	InvStokOpnameBHP.form.ComboBox.namabarang.setValue(rowdata.nama_brg);
	
	getGridDetailStokBarang(rowdata.nama_brg);
	Ext.getCmp('btnSimpan_viInvStokOpnameBHP').enable();
	Ext.getCmp('btnSimpanExit_viInvStokOpnameBHP').enable();
	
};

function datasave_viInvStokOpnameBHP(){
	if (ValidasiEntryInvStokOpnameBHP(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/bhp/functionStokOpname/save",
				params: getParamInvStokOpnameBHP(),
				failure: function(o)
				{
					ShowPesanErrorInvStokOpnameBHP('Error, simpan data! Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoInvStokOpnameBHP('Data berhasil disimpan','Information');
						getDataGridAwalInvStokOpname();
						Ext.getCmp('btnSimpan_viInvStokOpnameBHP').disable();
						Ext.getCmp('btnSimpanExit_viInvStokOpnameBHP').disable();
					}
					else 
					{
						ShowPesanErrorInvStokOpnameBHP('Gagal Menyimpan Data ini', 'Error');
					};
				}
			}
			
		)
	}
}

function getDataGridAwalInvStokOpname(kdinv,nama,tglawal,tglakhir){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/bhp/functionStokOpname/getGridAwal",
			params: {
				kdinv:kdinv,
				nama:nama,
				tglawal:tglawal,
				tglakhir:tglakhir
			},
			failure: function(o)
			{
				ShowPesanErrorInvStokOpnameBHP('Error, membaca grid awal! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					dataSource_viInvStokOpnameBHP.removeAll();
					
					var recs=[],
						recType=dataSource_viInvStokOpnameBHP.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dataSource_viInvStokOpnameBHP.add(recs);
					
					GridDataView_viInvStokOpnameBHP.getView().refresh();
				}
				else 
				{
					ShowPesanErrorInvStokOpnameBHP('Gagal membaca data', 'Error');
				};
			}
		}
		
	)
	
}


function getGridDetailStokBarang(nama){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/bhp/functionStokOpname/getGridDetailBarang",
			params: {nama:nama},
			failure: function(o)
			{
				ShowPesanErrorInvStokOpnameBHP('Error, membaca grid barang! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dsDataGrdJab_viInvStokOpnameBHP.removeAll();
					var recs=[],
						recType=dsDataGrdJab_viInvStokOpnameBHP.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dsDataGrdJab_viInvStokOpnameBHP.add(recs);
					
					 InvStokOpnameBHP.form.Grid.a.getView().refresh();
				}
				else 
				{
					ShowPesanErrorInvStokOpnameBHP('Gagal membaca data', 'Error');
				};
			}
		}
		
	)
	
}

function getParamInvStokOpnameBHP() 
{
	var	params =
	{
		TglPosting:Ext.getCmp('dfTglPosting_InvStokOpnameBHPL').getValue()
	}
	
	params['jumlah']=dsDataGrdJab_viInvStokOpnameBHP.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viInvStokOpnameBHP.getCount();i++)
	{
		params['kd_stok-'+i]=dsDataGrdJab_viInvStokOpnameBHP.data.items[i].data.kd_stok
		params['adjust_qty-'+i]=dsDataGrdJab_viInvStokOpnameBHP.data.items[i].data.adjust_qty
		params['jumlah_total-'+i]=dsDataGrdJab_viInvStokOpnameBHP.data.items[i].data.jumlah_total
	}
    return params
};


function ValidasiEntryInvStokOpnameBHP(modul,mBolHapus)
{
	var x = 1;
	
	if(dsDataGrdJab_viInvStokOpnameBHP.getCount() < 0){
		ShowPesanWarningInvStokOpnameBHP('Daftar stok opname tidak boleh kosong', 'Warning');
		x = 0;
	}
	return x;
};



function ShowPesanWarningInvStokOpnameBHP(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorInvStokOpnameBHP(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};


function ShowPesanInfoInvStokOpnameBHP(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};