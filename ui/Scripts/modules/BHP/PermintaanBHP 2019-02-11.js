var StatusPostingGFReturPBF='0';
var currentRowSelectionPencarianObatGFReturPBF;
var PencarianLookupGFReturPBF; // Variabel pembeda getdata u/ pencarian nama obat dan semua kepemilikan atau bukan. False => pencarian berdasarkan uraian dan kepemilikan, True => pencarian berdasarkan uraian saja
var FocusExitGFReturPBF=false;
var GridListPencarianObat_GFReturPBF;
var GridListPencarianObatColumnModel;
var unit_kerja_aktif;
var kd_unit_kerja;	
var panel_status;
var returPBF={

	coba:function(){
		var $this=this;
	},
	vars:{
		lineDetail:null,
		shortcut:false,
		total:0,
		subTotal:0,
		ppn:0
	},
	ArrayStore:{
		gridDetail:new Ext.data.ArrayStore({fields:[]}),
		comboObat:new Ext.data.ArrayStore({fields:[]}),
		gridMainPermintaan:new Ext.data.ArrayStore({fields:[]})
	},
	Button:{
		deleted:null,
		posting:null,
		save1:null,
		save2:null,
		addDetail:null,
		unposting:null,
		cetak:null
	},
	ComboBox:{
		vendor:null,
		posting:null,
		unit:null
	},
	DataStore:{
		vendor1:new WebApp.DataStore({fields: ['KD_VENDOR', 'VENDOR']})
	},
	DateField:{
		date:null,
		srchStartDate:null,
		srchLastDate:null
	},
	DisplayField:{
		posting:null
	},
	Grid:{
		detail:null,
		main:null
	},
	ComboBox:{
		vendor1:null
	},
	TextField:{
		subtotal:null,
		noRetur:null,
		remark:null,
		ppn:null,
		reduksi:null,
		total:null,
		srchRetNumber:null,
		kd_unit_kerja:null,
		unit_kerja_aktif:null,
		posting:null,
		keterangan:null,
		no_permintaan:null
	},
	delDetail:function(){
		// loadDataComboLokasi_front();
		var $this=this;
		var line = $this.Grid.detail.getSelectionModel().selection.cell[0];
		var o = $this.ArrayStore.gridDetail.getRange()[line].data;
		console.log(o);
		Ext.Msg.confirm('Warning', 'Apakah item BHP ini akan dihapus?', function(button){
			if (button == 'yes'){
				if(o.no_permintaan != undefined  && o.no_urut_brg!= undefined && (o.line!= undefined || o.line!= '') ){
					Ext.Ajax.request ({
						url: baseURL + "index.php/bhp/functionPermintaanBHP/deletedetail_bhp",
						params:{
							no_permintaan: o.no_permintaan,
							no_urut_brg: o.no_urut_brg,
							line: o.line,
						} ,
						failure: function(o) {
							Ext.Msg.alert('Error','Error, delete BHP. Hubungi Admin!');
						},	
						success: function(o) 
						{
							var cst = Ext.decode(o.responseText);
							if (cst.success === true)  {
								$this.ArrayStore.gridDetail.removeAt(line);
								$this.vars.lineDetail=null;
								$this.refreshInput();
								Ext.Msg.alert('Information','Berhasil menghapus data BHP ini');
							} else  {
								Ext.Msg.alert('Error','Gagal menghapus data BHP ini');
							};
						}
					})
				}else{
					$this.ArrayStore.gridDetail.removeAt(line);
					$this.vars.lineDetail=null;
					$this.refreshInput();
				}
			} 
		});
	},
	addDetail:function(){
		var $this=this;
		if(Ext.getCmp('cboPbf_viApotekReturPBF').getValue()!=''){
			$this.TextField.subtotal.focus();
			$this.ArrayStore.gridDetail.add(new $this.ArrayStore.gridDetail.recordType());
			$this.refreshInput();
		}else{
			Ext.Msg.alert('Gagal','Harap Pilih PBF terlebih dahulu.');
		}
	},

	cetak:function(){ //printing
		var $this=this;
		Ext.Msg.confirm('Konfirmasi', 'Siap mencetak bill?', function (id, value) { 
			if (id === 'yes') {
				var params={
					no_permintaan:Ext.getCmp('txt_no_permintaan').getValue()
				};
				var form = document.createElement("form");
				form.setAttribute("method", "post");
				form.setAttribute("target", "_blank");
				form.setAttribute("action", baseURL + "index.php/bhp/cetakan_faktur_bill/cetak_permintaan_bhp");
				var hiddenField = document.createElement("input");
				hiddenField.setAttribute("type", "hidden");
				hiddenField.setAttribute("name", "data");
				hiddenField.setAttribute("value", Ext.encode(params));
				form.appendChild(hiddenField);
				document.body.appendChild(form);
				form.submit();		
			}
		})
	},

	posting:function(){
		var $this=this;
				Ext.Msg.confirm('Konfirmasi', 'Apakah Akan diPosting ?', function (id, value) { 
					if (id === 'yes') { 
						datasaveBHPPosting();
					} 
				}, this); 
	},
	unposting:function(){
		var $this=this;
				Ext.Msg.confirm('Konfirmasi', 'Apakah Akan di UnPosting ?', function (id, value) { 
					if (id === 'yes') { 
						datasaveBHPUnPosting();
					} 
				}, this); 
	},
	
	save:function(callback){
		var $this=this;
		if($this.getParams() != undefined){
			/* if ($this.validasiSaveReturPBF()==1)	
			{ */
				if($this.TextField.No_permintaan.getValue()==''){
					loadMask.show();
					$.ajax({
						url:baseURL + "index.php/gudang_farmasi/functionGFReturPBF/save",
						dataType:'JSON',
						type: 'POST',
						data:$this.getParams(),
						success: function(r){
							loadMask.hide();
							if(r.processResult=='SUCCESS'){
								$this.TextField.No_permintaan.setValue(r.resultObject.code);
								for(var i=0; i<$this.ArrayStore.gridDetail.getCount() ; i++){
									var o=$this.ArrayStore.gridDetail.getRange()[i].data;
									o.ret_number=r.resultObject.code;
								}
								$this.ArrayStore.gridDetail.removeAll();
								getDetailListObat(r.listData,r.totalLisData);
								$this.refreshInput();
								Ext.Msg.alert('Sukses','Data Berhasi Disimpan.');
								Ext.getCmp('btnSaveGFReturPBF').enable();
								Ext.getCmp('btnDeleteGFReturPBF').enable();
								Ext.getCmp('btnPostingGFReturPBF').enable();
								Ext.getCmp('btnAddDetailGFReturPBF').enable();
								Ext.getCmp('btnUnPostingGFReturPBF').disable();
								Ext.getCmp('btnCetakBuktiGFReturPBF').disable();
								if(callback != undefined){
									callback();
								}
							}else{
								Ext.Msg.alert('Gagal',r.processMessage);
							}
						},
						error: function(jqXHR, exception) {
							loadMask.hide();
							Nci.ajax.ErrorMessage(jqXHR, exception);
						}
					});
				}else{
					loadMask.show();
					$.ajax({
						url:baseURL + "index.php/gudang_farmasi/functionGFReturPBF/update",
						dataType:'JSON',
						type: 'POST',
						data:$this.getParams(),
						success: function(r){
							loadMask.hide();
							if(r.processResult=='SUCCESS'){
								Ext.Msg.alert('Sukses','Data Berhasi Diubah.');
								for(var i=0; i<$this.ArrayStore.gridDetail.getCount() ; i++){
									var o=$this.ArrayStore.gridDetail.getRange()[i].data;
									o.ret_number=$this.TextField.No_permintaan.getValue();
								}
								$this.ArrayStore.gridDetail.removeAll();
								getDetailListObat(r.listData,r.totalLisData);
								$this.refreshInput();
								if(callback != undefined){
									callback();
								}
								Ext.getCmp('btnSaveGFReturPBF').enable();
								Ext.getCmp('btnDeleteGFReturPBF').enable();
								Ext.getCmp('btnPostingGFReturPBF').enable();
								Ext.getCmp('btnAddDetailGFReturPBF').enable();
								Ext.getCmp('btnUnPostingGFReturPBF').disable();
								Ext.getCmp('btnCetakBuktiGFReturPBF').disable();
							}else{
								Ext.Msg.alert('Gagal',r.processMessage);
							}
						},
						error: function(jqXHR, exception) {
							loadMask.hide();
							Nci.ajax.ErrorMessage(jqXHR, exception);
						}
					});
				}
			//}
				
		}
	},
	validasiSaveReturPBF:function(){
		var $this=this;
		var x = 1;
		for(var i = 0 ; i < $this.ArrayStore.gridDetail.getCount();i++)
		{
			var o=$this.ArrayStore.gridDetail.getRange()[i].data;
			for(var j = i+1 ; j < $this.ArrayStore.gridDetail.getCount();j++)
			{
				var p=$this.ArrayStore.gridDetail.getRange()[j].data;
				if (p.kd_prd==o.kd_prd)
				{
					Ext.Msg.alert('Warning','Obat ada yang Sama');
					x = 0;
				}
			}
			
		}
		return x;
	},
	getParams:function(){
		var $this=this;
		var a=[];
		if(Ext.getCmp('cboPbf_viApotekReturPBF').getValue()!=''){
			a.push({name: 'kd_vendor',value:Ext.getCmp('cboPbf_viApotekReturPBF').getValue()});
		}else{
			Ext.Msg.alert('Error','Harap Pilih Vendor.');
			$('#'+Ext.getCmp('cboPbf_viApotekReturPBF').id).focus();
			return;
		}
		if($this.TextField.remark.getValue()!=''){
			a.push({name: 'remark',value:$this.TextField.remark.getValue()});
		}else{
			Ext.Msg.alert('Error','Harap Isi Remark.');
			$('#'+$this.TextField.remark.id).focus();
			return;
		}
		a.push({name: 'ret_date',value:$this.DateField.date.value});
		a.push({name: 'ret_number',value:$this.TextField.No_permintaan.getValue()});
		a.push({name: 'ppn',value:toInteger($this.TextField.ppn.getValue())});
		if($this.ArrayStore.gridDetail.getCount()==0){
			Ext.Msg.alert('Error','Obat harus lebih dari 1.');
			return;
		}
		for(var i=0; i<$this.ArrayStore.gridDetail.getCount();i++){
			var o=$this.ArrayStore.gridDetail.getRange()[i].data;
			if(o.uraian != undefined && o.uraian != ''){
				a.push({name: 'kd_prd[]',value:o.kd_prd});
				if(o.qty != 0 && o.qty !=''){
					a.push({name: 'qty[]',value:o.qty});
				}else{
					Ext.Msg.alert('Error','Harap isi qty produk pada baris ke-'+(i+1));
					$this.Grid.detail.startEditing(i,6);
					return;
				}
			}else{
				$this.ArrayStore.gridDetail.removeAt(i);
				$this.Grid.detail.getView().refresh();
				// Ext.Msg.alert('Error','Harap pilih produk pada baris ke-'+(i+1));
				// $this.Grid.detail.startEditing(i,2);
				// return;
			}
			
			if(o.ret_reduksi != undefined && o.ret_reduksi !=''){
				a.push({name: 'ret_reduksi[]',value:o.ret_reduksi});
			}else{
				a.push({name: 'ret_reduksi[]',value:0});
				// return;
			}
			a.push({name: 'tgl_exp[]',value:o.tgl_exp});
			a.push({name: 'batch[]',value:o.batch});
			a.push({name: 'no_obat_in[]',value:o.no_obat_in});
			a.push({name: 'ppn_item[]',value:o.ppn_item});
			a.push({name: 'rcv_line[]',value:o.rcv_line});
			a.push({name: 'ret_line[]',value:o.ret_line});
		}
		return a;
	},
	/* setShortcut:function(){
		var $this=this;
		$(window).unbind().keydown(function(event) {
    		if($this.vars.shortcut==true){
    			if(event.ctrlKey &&  event.which==66){
    				$this.addDetail();
    				event.preventDefault();
					return false;
    			}
			   	if(event.ctrlKey &&  event.which==83){
    				$this.save();
    				event.preventDefault();
					return false;
    			}
    			if(event.ctrlKey &&  event.which==68){
    				if($this.Button.deleted.disabled==false)$this.delDetail();
    				event.preventDefault();
					return false;
    			}
    		}
		});
	}, */
	refreshInput:function(){/*
		var $this=this;
		if($this.ArrayStore.gridDetail.getCount()>0){
			$this.Button.deleted.enable();
		}else{
			$this.Button.deleted.disable();
		}
		if($this.TextField.No_permintaan.getValue()==''){
			$this.Button.posting.disable();	
			$this.Button.unposting.disable();	
			$this.Button.cetak.disable();	
			Ext.getCmp('cboPbf_viApotekReturPBF').enable();
			$this.TextField.remark.enable();
			$this.DateField.date.enable();
		}else{
			$this.Button.posting.enable();	
			$this.Button.unposting.enable();	
			$this.Button.cetak.enable();	
			Ext.getCmp('cboPbf_viApotekReturPBF').disable();
			$this.TextField.remark.disable();
			$this.DateField.date.disable();
		}
		if(StatusPostingGFReturPBF=='0'){
			$this.Button.deleted.enable();
			$this.Button.save1.enable();
			$this.Button.save2.enable();
			$this.Button.addDetail.enable();
			$this.Button.posting.enable();	
			$this.Button.unposting.disable();
			$this.Button.cetak.disable();
			$this.DisplayField.posting.setValue('<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>');
		}else{
			$this.Button.deleted.disable();
			$this.Button.save1.disable();
			$this.Button.save2.disable();
			$this.Button.addDetail.disable();
			$this.Button.posting.disable();	
			$this.Button.unposting.enable();
			$this.Button.cetak.enable();
			$this.DisplayField.posting.setValue('<img src="'+baseURL+'ui/images/icons/16x16/hijau.png"/>');
		}
		var obat={};
		var subTotal=0;
		var reduksi=0;
		for(var i=0;i<$this.ArrayStore.gridDetail.getRange().length ;i++){
			var o=$this.ArrayStore.gridDetail.getRange()[i].data;
			if(o.harga_beli==undefined || o.harga_beli=='')o.harga_beli=0;
			if(o.qty==undefined || o.qty=='')o.qty=0;
			if(o.ret_reduksi==undefined || o.ret_reduksi=='')o.ret_reduksi=0;
			if(o.jml_in_obt==undefined || o.jml_in_obt=='')o.jml_in_obt=0;
			if(o.kd_prd != undefined){
				if(obat[o.kd_prd+'-'+o.no_obat_in+'-'+o.batch] ==undefined){
					obat[o.kd_prd+'-'+o.no_obat_in+'-'+o.batch]={count:parseFloat(o.qty),lastCount:[parseFloat(o.qty)]};
				}else{
					obat[o.kd_prd+'-'+o.no_obat_in+'-'+o.batch].count+=parseFloat(o.qty);
					obat[o.kd_prd+'-'+o.no_obat_in+'-'+o.batch].lastCount.push(parseFloat(o.qty));
				}
				if(o.jml_in_obt<obat[o.kd_prd+'-'+o.no_obat_in+'-'+o.batch].count){
					var jumCount=0
					for(var j=0; j<(obat[o.kd_prd+'-'+o.no_obat_in+'-'+o.batch].lastCount.length-1);j++){
						jumCount+=obat[o.kd_prd+'-'+o.no_obat_in+'-'+o.batch].lastCount[j];
					}
					o.qty=parseFloat(o.jml_in_obt)-jumCount;
					//Ext.Msg.alert('Error','Quantity tidak boleh lebih dari jumlah Stok.');
				}
			}
			o.jumlah=parseFloat(o.qty)*parseFloat(o.harga_beli);
			subTotal+=o.jumlah;
			reduksi+=parseFloat(o.ret_reduksi);
			
		}
		$this.TextField.subtotal.setValue(formatNumberDecimal(toFormat(parseFloat(subTotal)),0));
		var ppn=(parseFloat(subTotal)/100)*10;
		$this.TextField.ppn.setValue(formatNumberDecimalParam(toFormat(ppn),0));
		$this.TextField.reduksi.setValue(toFormat(reduksi));
		var total=(parseFloat(subTotal)+ppn)-reduksi;
		$this.vars.total=total;
		$this.vars.subTotal=subTotal;
		$this.vars.ppn=ppn;
		$this.TextField.total.setValue(formatNumberDecimalParam(toFormat(total),0));
		$this.Grid.detail.getView().refresh();*/
	}
}

var dataSource_viApotekReturPBF;
var selectCount_viApotekReturPBF=50;
var NamaForm_viApotekReturPBF="Permintaan Barang BHP";
var mod_name_viApotekReturPBF="viPerminataanBHP";
var now_viApotekReturPBF= new Date();
var addNew_viApotekReturPBF;
var rowSelected_viApotekReturPBF;
var setLookUps_viBHPPermintaan;
var mNoKunjungan_viApotekReturPBF='';

var CurrentData_viApotekReturPBF ={
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viApotekReturPBF(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
Q(returPBF.Grid.main).refresh();
function ShowPesanWarningPermBHP(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorPermintaanBHP(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:290
		}
	);
};


function ShowPesanInfoReturPBFGF(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:350
		}
	);
};
function dataGrid_viApotekReturPBF(mod_id_viApotekReturPBF){	
	loadDataComboLokasi_front();
    var $this=returPBF;
	$this.Grid.main = Q().table({
		border:false,
		rowdblclick: function (sm, ridx, cidx,store){
			rowSelected_viApotekReturPBF = store.getAt(ridx);
			if (rowSelected_viApotekReturPBF != undefined){
				setLookUp_viApotekReturPBF(rowSelected_viApotekReturPBF.data);
			}else{
				setLookUp_viApotekReturPBF();
			}
		},
		rowselect: function(sm, row, rec,store){
			rowSelected_viApotekReturPBF = undefined;
			rowSelected_viApotekReturPBF = store.getAt(row);
			
			console.log(rowSelected_viApotekReturPBF);
			CurrentData_viApotekReturPBF
			CurrentData_viApotekReturPBF.row = row;
			CurrentData_viApotekReturPBF.data = store;
			console.log(rowSelected_viApotekReturPBF.data.posting);
			if (rowSelected_viApotekReturPBF.data.posting==='0')
							{
								Ext.getCmp('btnHapusTrx_viApotekReturPBFGF').enable();
							}
							else
							{
								Ext.getCmp('btnHapusTrx_viApotekReturPBFGF').disable();
							}
		},
		tbar:{
			xtype: 'toolbar',
			id: 'toolbar_viBHP_Permintaan',
			items: 
			[
				{
					xtype: 'button',
					text: 'Tambah [F1]',
					iconCls: 'add',
					tooltip: 'Add Data',
					id:'btnTambahReturGFReturPBF',
					handler: function(sm, row, rec){
						StatusPostingGFReturPBF='0';
						setLookUp_viApotekReturPBF();
					}
				},{
					xtype: 'button',
					text: 'Ubah',
					iconCls: 'Edit_Tr',
					tooltip: 'Edit Data',
					handler: function(sm, row, rec){
						if (rowSelected_viApotekReturPBF != undefined){
							setLookUp_viApotekReturPBF(rowSelected_viApotekReturPBF.data)
						}
					}
				},
				{
						xtype: 'button',
						text: 'Hapus Permintaan',
						iconCls: 'remove',
						tooltip: 'Hapus Data',
						disabled:false,
						id: 'btnHapusTrx_viApotekReturPBFGF',
						handler: function(sm, row, rec)
						{
							var datanya=rowSelected_viApotekReturPBF.data;
							if (datanya===undefined){
								ShowPesanWarningPermBHP('Belum ada data yang dipilih','BHP');
							}
							else
							{
								 Ext.Msg.show({
									title: 'Hapus Transaksi',
									msg: 'Anda yakin akan menghapus data transaksi ini ?',
									buttons: Ext.MessageBox.YESNO,
									fn: function (btn) {
										if (btn == 'yes'){
											 Ext.Ajax.request({
														url: baseURL + "index.php/bhp/functionPermintaanBHP/hapusTrxBHP",
														 params: {
															no_permintaan: datanya.no_permintaan/*,
															alasan: variablebatalhistori_returpbf*/
														},
														failure: function(o)
														{
															 var cst = Ext.decode(o.responseText);
															ShowPesanWarningPermBHP('Data transaksi tidak dapat dihapus','BHP');
														},	    
														success: function(o) {
															var cst = Ext.decode(o.responseText);
															if (cst.success===true)
															{
																Q(returPBF.Grid.main).refresh();
																ShowPesanInfoReturPBFGF('Data transaksi Berhasil dihapus','BHP');
																Ext.getCmp('btnHapusTrx_viApotekReturPBFGF').disable();
															}
															else
															{
																ShowPesanErrorPermintaanBHP('Data transaksi tidak dapat dihapus','BHP');
															}
															
														}
												
												})
											
										}
									},
									icon: Ext.MessageBox.QUESTION
								});
							} 
						}
					},

			]
		},//x111

		ajax:function(data,callback){
			loadMask.show();
			var a=[];
			a.push({name: 'status_posting',value:$this.ComboBox.posting.getValue()});
			a.push({name: 'no_permintaan',value:$this.TextField.no_permintaan.getValue()});
			a.push({name: 'tanggal_awal',value:$this.DateField.srchStartDate.value});
			a.push({name: 'tanggal_akhir',value:$this.DateField.srchEndDate.value});
			a.push({name: 'lokasi',value:Ext.getCmp('cbJnsLokasiInvDistribusiBHP_permintaan_front').getValue()}
				);
			$.ajax({
				type: 'POST',
				dataType:'JSON',
				url:baseURL + "index.php/bhp/functionPermintaanBHP/get_all_list_permintaan_cari_bhp",
				data:a,
				success: function(r){
					loadMask.hide();

					if(r.processResult=='SUCCESS'){
						$this.ArrayStore.gridMainPermintaan.loadData([],false);
						callback(r.listData,r.totalrecords);
					}else{
						Ext.Msg.alert('Gagal',r.processMessage);
					}
				},
				error: function(jqXHR, exception) {
					loadMask.hide();
					Nci.ajax.ErrorMessage(jqXHR, exception);
				}
			});
		},
		column:new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(),
			{
				header		: 'Status Posting',
				width		: 20,
				sortable	: false,
				hideable	: true,
				hidden		: false,
				menuDisabled: true,
				dataIndex	: 'posting_bhp',
				renderer	: function(value, metaData, record, rowIndex, colIndex, store){
					 switch (value){
						 case '1':
							 metaData.css = 'StatusHijau'; 
							 break;
						 case '0':
							 metaData.css = 'StatusMerah';
							 break;
					 }
					 return '';
				}
			},{
				header		: 'No Permintaan',
				width		: 10,
				sortable	: false,
				hideable	: true,
				hidden		: false,
				menuDisabled: true,
				dataIndex	: 'no_permintaan'
			},{
				header: 'Unit Kerja/Lokasi',
				dataIndex: 'lokasi',
				sortable: false,
				menuDisabled: true,
				width: 10
			},{
				header: 'kd_bagian',
				dataIndex: 'kd_bagian',
				sortable: false,
				menuDisabled: true,
				hidden : true,
				width: 10
			},{
				header:'Tanggal',
				dataIndex: 'tanggal',						
				width: 10,
				sortable: false,
				menuDisabled: true
			},{
				header: 'Keterangan',
				dataIndex: 'keterangan',
				sortable: false,
				menuDisabled: true,
				width: 30
			}
		])
	});

    var FrmFilterGridDataView_viApotekReturPBF = new Ext.Panel({
		title: NamaForm_viApotekReturPBF,
		iconCls: 'Studi_Lanjut',
		id: mod_id_viApotekReturPBF,
		region: 'center',
		layout: 'form', 
		closable: true,        
		border: false,  
		margins: '0 5 5 0',
		items: [
		{
			xtype		: 'panel',
			plain		: false,
			anchor		: '100%',
			bodyStyle	: 'padding: 2px;',
			items	: [
			{
				layout	: 'column',
				border	: false,
				bodyStyle: 'padding: 5px;',
				items	:[
					{
						layout	: 'form',
						border	: false,
						items	: [
							{
								xtype: 'buttongroup',
								columns: 15,
								defaults: {
									scale: 'small'
								},//parameter
								frame: false,
								items: [
									{ 
										xtype: 'tbtext', 
										text: 'No. Permintaan : ', 
										style:{'text-align':'right','font-size':'11px'},
										width: 100,
										height: 25
									},						
									$this.TextField.no_permintaan=new Ext.form.TextField({
										emptyText: 'No. Permintaan',
										width: 150,
										listeners:{ 
											'specialkey' : function(){
												if (Ext.EventObject.getKey() === 13) {
													Q(returPBF.Grid.main).refresh();
												} 						
											}
										}
									}),
									{ 
										xtype: 'tbtext', 
										text: 'Status posting : ', 
										style:{'text-align':'right','font-size':'11px'},
										width: 100,
										height: 25
									},
									$this.ComboBox.posting = new Ext.form.ComboBox({ 
										fieldLabel: '',
										valueField: 'Id',
										displayField: 'displayText',
										id:'cmbStatusPosting',
										store: new Ext.data.ArrayStore
										(
											{
												id: 0,
												fields:
													[
															'Id',
															'displayText'
													],
												data: [[1, 'Posting'],[0,'Belum Posting'],[200, 'Semua']]
											}
										),
										width: 150,
										value:'0',
										mode: 'local',
										typeAhead: true,
										triggerAction: 'all',                        
										name: 'post',
										lazyRender: true,
										id: 'cmbStatusPosting',
										listeners:{ 
											'specialkey' : function(){
												if (Ext.EventObject.getKey() === 13){
													Q(returPBF.Grid.main).refresh();
												} 							
											},
											'select' : function(){
												Q(returPBF.Grid.main).refresh();
											}
										}
									}),
									/*{ 
										xtype: 'tbtext', 
										text: 'Keterangan : ', 
										style:{'text-align':'right','font-size':'11px'},
										width: 100
									},$this.TextField.keterangan=new Ext.form.TextField({
										emptyText: 'Keterangan',
										width: 150,
										listeners:{ 
											'specialkey' : function(){
												if (Ext.EventObject.getKey() === 13) {
													Q(returPBF.Grid.main).refresh();
												} 						
											}
										}
									}),*/		
									{ 
										xtype: 'tbtext', 
										text: 'Lokasi/Unit : ', 
										style:{'text-align':'right','font-size':'11px'},
										width: 100,
										height: 25
									},
									comboJenisLokasiInvDistribusiBHP(),
									
								]
							},
							{
								xtype: 'buttongroup',
								columns: 11,
								defaults: {
									scale: 'small'
								},
								frame: false,
								items: 
								[
									/*{	 
										xtype: 'tbspacer',
										width: 55,
										height: 25
									},*/
									{ 
										xtype: 'tbtext', 
										text: 'Tanggal Awal: ', 
										style:{'text-align':'right','font-size':'11px'},
										width: 100,
										height: 25
									},		
									
									$this.DateField.srchStartDate=new Ext.form.DateField({
										value: now_viApotekReturPBF,
										format: 'd/M/Y',
										width: 150,
										listeners:{ 
											'specialkey' : function(){
												if (Ext.EventObject.getKey() === 13){
													Q(returPBF.Grid.main).refresh();
												} 						
											}
										}
									}),
									
									
									{	
										text: 's/d: ',  
										xtype: 'tbtext',
										style:{'text-align':'right','font-size':'11px'},
										width: 50,
										height: 25
									},
									{	
										text: 'S/D: ',  
										xtype: 'tbspacer',
										width: 50,
										height: 25
									},

									$this.DateField.srchEndDate=new Ext.form.DateField({
										value: now_viApotekReturPBF,
										format: 'd/M/Y',
										width: 150,
										listeners:{ 
											'specialkey' : function(){
												if (Ext.EventObject.getKey() === 13){
													Q(returPBF.Grid.main).refresh();
												} 						
											}
										}
									}),
									{ 
										xtype: 'tbspacer', 
										text: ' : ', 
										style:{'text-align':'right','font-size':'11px'},
										width: 250
									},
									{
										xtype: 'button',
										text: 'Cari',
										iconCls: 'refresh',
										tooltip: 'Cari',
										style:{paddingLeft:'30px'},
										width:150,
										handler: function(){					
											DfltFilterBtn_viApotekReturPBF = 1;
											Q(returPBF.Grid.main).refresh();
										}                        
									}
								]
							}
						]
					}
				]
			}
				
			]
		},
		$this.Grid.main
		]
   });
   return FrmFilterGridDataView_viApotekReturPBF;
}

function setLookUp_viApotekReturPBF(rowdata){
	var $this=returPBF;	
	console.log(rowdata);
    var lebar = 985;
    var $this=returPBF;
    setLookUps_viBHPPermintaan = new Ext.Window({
        title: NamaForm_viApotekReturPBF, 
        closeAction: 'destroy',        
        width: 900,
        height: 500,
        resizable:false,
        layout:{
        	type:'vbox',
        	align:'stretch'
        },
		autoScroll: false,
        border: false,
        constrain : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viPermBHP(lebar,rowdata), //1
        listeners:{
            activate: function(a){
				// $this.vars.shortcut=true;
				// $this.setShortcut();	
				$this.refreshInput();
				shortcuts();
            },
            afterShow: function(){
                this.activate();
            },
            hide:function(){
            	// $this.vars.shortcut=false;
            	$this.ArrayStore.gridDetail.loadData([],false)
            },
            deactivate: function(){
                rowSelected_viApotekReturPBF=undefined;
				mNoKunjungan_viApotekReturPBF = '';
				shortcut.remove('lookup');
            },
			close: function (){
				shortcut.remove('lookup');
			},
        }
    });
    loadMask.show();
  //  get_unit_kerja();
  loadDataComboLokasi();
    if (rowdata == undefined){
		setLookUps_viBHPPermintaan.show();
		$this.refreshInput();
		loadMask.hide();
        /* $.ajax({
			url:baseURL + "index.php/gudang_farmasi/functionGFReturPBF/initTransaksi",
			dataType:'JSON',
			type: 'GET',
			success: function(r){
				
				if(r.processResult=='SUCCESS'){
					
				}else{
					Ext.Msg.alert('Error',r.processMessage);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		}); */
    }else{
    	Ext.getCmp('txt_id_unit_kerja').setValue(rowdata.kd_bagian);
    	Ext.getCmp('cbJnsLokasiInvDistribusiBHP_permintaan_back').setValue(rowdata.lokasi);
    	Ext.getCmp('cbJnsLokasiInvDistribusiBHP_permintaan_back').disable();
    	get_detail_permintaan_bhp(rowdata.no_permintaan);
      /* $.ajax({
 			url:baseURL + "index.php/bhp/functionPermintaanBHP/getAllPermintaanDetail",
 			dataType:'JSON',
 			type: 'POST',
 			data: {'no_permintaan':rowdata.no_permintaan},
 			success: function(r){
 				loadMask.hide();
 				if(r.processResult=='SUCCESS'){	
 					setLookUps_viBHPPermintaan.show();
 					var o=r.resultObject;
 					$this.TextField.No_permintaan.setValue(o.no_permintaan);// set grid
 					$this.TextField.remark.setValue(o.keterangan).disable();
 					var date = Date.parseDate(o.tanggal, "Y-m-d H:i:s");
 					console.log(o.tanggal);
 					$this.DateField.date.setValue(date).disable(0);
 					$this.ArrayStore.gridDetail.loadData([],false);
 					for(var i=0, iLen=r.listData.length ; i<iLen ; i++){
 						$this.ArrayStore.gridDetail.add(new $this.ArrayStore.gridDetail.recordType(r.listData[i]));
 					}
 					$this.refreshInput();
 				}else{
 					Ext.Msg.alert('Gagal',r.processMessage);
 				}
 			},
 			error: function(jqXHR, exception) {
 				loadMask.hide();
 				Nci.ajax.ErrorMessage(jqXHR, exception);
 			}
 		});*/
    }
	// shortcuts();
}

function shortcuts(){
	shortcut.set({
		code:'lookup',
		list:[
			{
				key:'ctrl+s',
				fn:function(){
					Ext.getCmp('btnSaveGFReturPBF').el.dom.click();
				}
			},
			{
				key:'ctrl+d',
				fn:function(){
					Ext.getCmp('btnDeleteGFReturPBF').el.dom.click();
				}
			},{
				key:'f4',
				fn:function(){
					Ext.getCmp('btnPostingGFReturPBF').el.dom.click();
				}
			},{
				key:'f6',
				fn:function(){
					Ext.getCmp('btnUnPostingGFReturPBF').el.dom.click();
				}
			},
			{
				key:'f12',
				fn:function(){
					Ext.getCmp('btnCetakBuktiGFReturPBF').el.dom.click();
				}
			},
			{
				key:'esc',
				fn:function(){
					setLookUps_viBHPPermintaan.close();
				}
			}
		]
	});
}

function getFormItemEntry_viPermBHP(lebar,rowdata){
	var $this=returPBF;
    var pnlFormDataBasic_viApotekReturPBF = new Ext.FormPanel({
		title: '',
		bodyStyle: 'padding:5px 5px 5px 5px',
		width: lebar,
		layout:{
			type:'vbox',
			align:'stretch'
		},
		flex:1,
		items:[
			getItemPanelInputBiodata_viApotekReturPBF(lebar),
			getItemGridTransaksi_viApotekReturPBF(lebar),
			{
					layout	: 'form',
					bodyStyle: 'margin-top: 5px; margin-left: 640px; margin-bottom :-6px;',
					border: false,
					items	: [
						{
							xtype: 'compositefield',
							fieldLabel: ' ',
							labelSeparator: '',
							items: [{
										xtype:'displayfield',
										style:{'text-align':'left','margin-top':'2px'},
										value:'Status Posting :'
									},	
									$this.DisplayField.posting=new Ext.form.DisplayField({
										value		: '<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>',
										style:{'text-align':'left','margin-top':'2px'},
									})										
						    ]
						}
		     	   ]
				}
			
		],
		fileUpload: true,
		tbar: {
			xtype: 'toolbar',
			items: 
			[	//savingnya
				$this.Button.save1=new Ext.Button({
					text: 'Save',
					iconCls: 'save [CTRL+S]',
					id: 'btnSaveGFReturPBF',
					handler: function(){
						datasaveBHP();
					}
				}),{
					xtype: 'tbseparator'
				},$this.Button.save2=new Ext.Button({
					text: 'Save & Close',
					iconCls: 'saveexit',
					hidden:true,
					handler: function(){
						$this.save(function(){
							setLookUps_viBHPPermintaan.close();
						});
					}
				}),{
					xtype: 'tbseparator',
					hidden:true,
				},$this.Button.posting=new Ext.Button({
					text: 'Posting [F4]',
					iconCls: 'gantidok',
					id: 'btnPostingPermintaanBHP',
					handler: function(){
					//	datasaveBHPPosting();
						$this.posting();
					}
				}),{
					xtype: 'tbseparator'
				},
				$this.Button.unposting=new Ext.Button({
					text: 'Unposting',
					iconCls: 'gantidok',
					id: 'btnUnPostingPermintaanBHP',
					handler: function(){
						//datasaveBHPUnPosting();
						$this.unposting();
					}
				}),
				{
					xtype: 'tbseparator'
				},
				$this.Button.cetak=new Ext.Button({
					text: 'Print [F12]',
					iconCls: 'print',
					id: 'btnCetakBuktiGFReturPBF',
					handler: function(){
						$this.cetak();
					}
				}),{
					xtype: 'tbseparator'
				}
			]
		}
	});
    return pnlFormDataBasic_viApotekReturPBF;
}

function getItemPanelInputBiodata_viApotekReturPBF(lebar){
    var $this=returPBF;
    var items ={
	    layout: 'Form',
	    xtype:'fieldset',
	    style:'margin-bottom:-1px;',
	    autoHeight:true,
		items:[	
			{
				layout:'column',
				border:false,
				items:[
					Q().input({
						label:'No. Permintaan',
						width: 250,
						items:[
							$this.TextField.No_permintaan=new Ext.form.TextField({
								width : 120,
								disabled:true,
								emptyText: 'NO. PERMINTAAN',
								id: 'txt_no_permintaan'
							})
						]
					}),
					Q().input({
						label:'Lokasi/Unit',
						width: 500,
						items:[
							/*$this.TextField.unit_kerja_aktif=new Ext.form.TextField({
								xtype: 'textfield',
								width : 360,	
								emptyText: 'LOKASI/UNIT',
								id: 'txt_lokasi',
								disabled:true
								
							}),*/
							comboJenisLokasiInvDistribusiBHP_belakang(),

							$this.TextField.kd_unit_kerja=new Ext.form.TextField({
								xtype: 'textfield',
								width : 360,	
								emptyText: '',
								id: 'txt_id_unit_kerja',
								hidden:true
							})
						]
					})/*,
					{ 
						xtype: 'tbtext', 
						text: '', 
						width: 10,
						height: 25
					}*/
					//,viCombo_Vendor(415, 'cboPbf_viApotekReturPBF')	
				]
			},{
				layout:'column',
				border:false,
				items:[
					Q().input({
						label:'TANGGAL',
						width: 250,
						items:[
							$this.DateField.date=new Ext.form.DateField({
								// disabled:true,
								value: now_viApotekReturPBF,
								format: 'd/M/Y',
								width: 120,
								id: 'cbo_tanggal_permintaan'
							})
						]
					}),
					Q().input({
						label:'KETERANGAN',
						width: 500,
						items:[
							$this.TextField.remark=new Ext.form.TextField({
								xtype: 'textfield',
								width : 360,	
								emptyText: 'Keterangan',
								id: 'txt_keterangan_permintaan',
								hidden:false
								
							})
						]
					}),
					$this.TextField.posting=new Ext.form.TextField({
								xtype: 'textfield',
								width : 360,
								id: 'txt_posting',
								hidden:true
								
							})
				]
			}
		]
	};
    return items;
};

function getItemGridTransaksi_viApotekReturPBF(lebar) {
    var items ={
	    layout: 'fit',
	    anchor: '100%',	    
		border:false,
		style:{'padding-bottom':'10px'},
		flex: 1,
		width: lebar-80,
	    items:gridDataViewEdit_viApotekReturPBF()
	};
    return items;
};

function gridDataViewEdit_viApotekReturPBF(){
	var $this=returPBF;
	$this.Grid.detail=new Ext.grid.EditorGridPanel({
        store: $this.ArrayStore.gridDetail,
        flex:1,
        style:'margin-top:-1px;',
        stripeRows:true,
        columnLines:true,
		selModel: new Ext.grid.CellSelectionModel({
            singleSelect: true,
            listeners:{
                cellselect: function(sm, row, rec){
					// console.log(row)
                	// $this.vars.lineDetail=row;
                }
            }
        }),
        tbar:[
    		$this.Button.addDetail=new Ext.Button({
				text: 'Tambah',
				iconCls: 'add',
				tooltip: 'Edit Data',
				id:'btnAddDetailGFReturPBF',
					handler	: function(){
							var records = new Array();
							setTimeout(
							function(){ 
								records.push(new $this.ArrayStore.gridDetail.recordType());
								$this.ArrayStore.gridDetail.add(records);
								var row =$this.ArrayStore.gridDetail.getCount()-1;
								$this.Grid.detail.startEditing(row, 4);
							}, 200);
				}
			}),{
				xtype: 'tbseparator'
			},$this.Button.deleted=new Ext.Button({
				text: 'Delete Item',
				iconCls: 'remove',
				id:'btnDeleteGFReturPBF',
				handler: function(){
					$this.delDetail();
				}
			}),{
				xtype: 'tbseparator'
			}
        ],
        cm: new Ext.grid.ColumnModel([ //ini grid
			new Ext.grid.RowNumberer(),
			{			
				dataIndex: 'kd_kelompok',
				header: 'KD KELOMPOK',
				sortable: false,
				menuDisabled: true,
				width: 100
			},
			{			
				dataIndex: 'kode',
				header: 'Kode',
				sortable: false,
				menuDisabled: true,
				width: 70
			},
			{			
				dataIndex: 'line',
				header: 'line',
				sortable: false,
				menuDisabled: true,
				width: 70,
				hidden:true
			},
			{			
				dataIndex: 'uraian',
				header: 'Uraian',
				sortable: true,
				width: 250,
				menuDisabled: true,
				fixed: true,
				editor: new Ext.form.TextField({
					allowBlank: false,
					enableKeyEvents:true,
					listeners:{
						keyDown: function(a,b,c){
							if(b.getKey()==13){
								var line	= $this.Grid.detail.getSelectionModel().selection.cell[0];
								var o=$this.ArrayStore.gridDetail.getRange()[line].data;
								console.log(o) ; //get data baris
								/*if(a.getValue().length < 3){
									// if(a.getValue().length != 0){
										Ext.Msg.show({
											title: 'Perhatian',
											msg: 'Kriteria huruf pencarian BHP minimal 3 huruf!',
											buttons: Ext.MessageBox.OK,
											fn: function (btn) {
												if (btn == 'ok')
												{
													$this.Grid.detail.startEditing(line, 3);
												}
											}
										});
									
								} *///else{		
									/*PencarianLookupGFReturPBF_ = true;// Variabel pembeda getdata u/nama obat dan semua kepemilikan atau bukan. False => pencarian berdasarkan uraian dan kepemilikan, True => pencarian berdasarkan uraian saja
									FocusExitGFReturPBF_ = false;*/
									LookUpSearchListBHPGF(a.getValue(),line); // ambil value uraian
								//}
									
							}
						}
					}
				})
			},
			{
				dataIndex: 'satuan',
				header: 'Satuan',
				sortable: false,
				menuDisabled: true,
				width: 60
			},{
				dataIndex: 'qty',
				header: 'qty',
				//xtype:'numbercolumn',
				//align:'right',
				sortable: false,
				menuDisabled: true,
				width: 40,
					editor: new Ext.form.TextField({
					allowBlank: false,
					enableKeyEvents:true
					
				})
			}
			
        ]),
        viewConfig:{
        	forceFit:true
        }
    });   
    return $this.Grid.detail;
}
function viCombo_Vendor(lebar,Nama_ID){
	var $this=returPBF;
	var b=new WebApp.DataStore({fields: ['KD_VENDOR', 'VENDOR']});
	b.load({
        params:{
            Skip: 0,
            Take: 1000,
            Sort: 'KD_VENDOR',
            Sortdir: 'ASC',
            target: 'ComboPBF',
            param: ""
        }
    });
    var a = new Ext.form.ComboBox ({
        fieldLabel: 'Vendor',
		valueField: 'KD_VENDOR',
        displayField: 'VENDOR',
		emptyText:'PBF',
		store: b,
        width: 200,
        mode: 'local',
        typeAhead: true,
        triggerAction: 'all',                        
        name: Nama_ID,
        lazyRender: true,
        id: Nama_ID,
		listeners:{ 
			select:function(a){
				if(a.id=='cboPbf_viApotekReturPBF'){
					$this.ArrayStore.gridDetail.loadData([],false);
					$this.refreshInput();
				}
			},
			'specialkey' : function()
			{
				if (Ext.EventObject.getKey() === 13) 
				{
					if(a.id=='cboPbf_viApotekReturPBF'){
						$this.TextField.remark.focus();
					}
				} 						
			}
			
			
		}
    });    
    return a;
}

function getDetailListObat(listData,totalLisData){
	var $this=returPBF;
	for(var i=0, iLen=totalLisData; i<iLen; i++){
		$this.ArrayStore.gridDetail.add(new $this.ArrayStore.gridDetail.recordType(listData[i]));
	}
}


function LookUpSearchListBHPGF(uraian,line){
	var $this=returPBF;
	var row_select= $this.Grid.detail.getSelectionModel().selection.cell[0];
		WindowLookUpSearchListGetBHP = new Ext.Window
		(
			{
				id: 'pnlLookUpSearchListBHPGF',
				title: 'List Pencarian BHP',
				width:700,
				height: 250,
				border: false,
				resizable:false,
				plain: true,
				iconCls: 'icon_lapor',
				modal: true,
				items: [
					gridListBHPPencarianGF()

				],
				tbar :
			[
				{
					xtype:'button',
					text:'Simpan',
					iconCls	: 'save',
					hideLabel:true,
					id: 'BtnOktrformLookUpBarang',
					handler:function(){
						console.log();
						 if(line==row_select){	
								data_grid = GridListPencarianObat_GFReturPBF;
								data_ceklis= data_grid.selModel.selections;
								var hitung_grid = $this.ArrayStore.gridDetail.getCount()-1;		
							for (var i=0; i < data_ceklis.length; i++){	
								$this.ArrayStore.gridDetail.getRange()[line].data.kd_kelompok= data_ceklis.items[i].data.kd_inv;
								$this.ArrayStore.gridDetail.getRange()[line].data.kode= data_ceklis.items[i].data.no_urut_brg;
								$this.ArrayStore.gridDetail.getRange()[line].data.uraian= data_ceklis.items[i].data.nama_brg;
								$this.ArrayStore.gridDetail.getRange()[line].data.satuan= data_ceklis.items[i].data.satuan;
							}
							$this.Grid.detail.getView().refresh();
							WindowLookUpSearchListGetBHP.close(); 		
						}
						else{
							data_grid = GridListPencarianObat_GFReturPBF;
							data_ceklis= data_grid.selModel.selections;
							var hitung_grid = $this.ArrayStore.gridDetail.getCount()-1;	
							for (var i = 0; i < data_ceklis.length; i++){
								$this.ArrayStore.gridDetail.getRange()[hitung_grid+i].data.kd_kelompok= data_ceklis.items[i].data.kd_inv;
								$this.ArrayStore.gridDetail.getRange()[hitung_grid+i].data.kode= data_ceklis.items[i].data.no_urut_brg;
								$this.ArrayStore.gridDetail.getRange()[hitung_grid+i].data.uraian= data_ceklis.items[i].data.nama_brg;
								$this.ArrayStore.gridDetail.getRange()[hitung_grid+i].data.satuan= data_ceklis.items[i].data.satuan;
								var records = new Array();
								records.push(new $this.ArrayStore.gridDetail.recordType());
								$this.ArrayStore.gridDetail.add(records);
							} 
							$this.Grid.detail.getView().refresh();
							WindowLookUpSearchListGetBHP.close();
					}
						
					}
	},
			],
				listeners:
				{             
					activate: function()
					{
							
					},
					afterShow: function()
					{
						this.activate();

					},
					deactivate: function()
					{
						
					},
					close: function (){
						// if(FocusExitGFReturPBF == false){
						// 	var line	= $this.vars.lineDetail;
						// 	$this.Grid.detail.startEditing(line, 3);	
						// }
					}
				}
			}
		);

		WindowLookUpSearchListGetBHP.show();
		getListBHPSearch(uraian);
	}
	function gridListBHPPencarianGF(uraian){ // lookup form
		var chkgetTindakanKasirRawatInap = new Ext.grid.CheckColumn
	(
		{
			xtype: 'checkcolumn',
			width: 5,
			sortable: false,
			id: 'check1KasirRWI',
			dataIndex: 'checkBHP',
			editor: {
				xtype: 'checkbox',
				cls: 'x-grid-checkheader-editor'
			},
			listeners: {
				checkchange: function (column, recordIndex, checked) {
					// alert(checked);
					// alert("hi");
					console.log(column);
				}
			}
		}
	); 

    var cbxSelModel = new Ext.grid.CheckboxSelectionModel({
        checkOnly: false,
        singleSelect: false,
        sortable: false,
        dataIndex: 'visible',
        width: 20,
        listeners: {
            selectionchange : function(selModel) {
            },
            scope: this
        }
    });
    var selectModel = new Ext.grid.CheckboxSelectionModel();
	var fldDetail = ['kd_inv','no_urut_brg','nama_brg','satuan','tag'];
		dsGridListPencarianBHP = new WebApp.DataStore({ fields: fldDetail });
		GridListPencarianObatColumnModel =  new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(),
			
			{
				dataIndex		: 'kd_inv',
				header			: 'KD Kelompok',
				width			: 100,
				menuDisabled	: true,
			},
			{
				dataIndex		: 'no_urut_brg',
				header			: 'Kode',
				width			: 70,
				menuDisabled	: true,
			},
			{
				dataIndex		: 'nama_brg',
				header			: 'Uraian',
				width			: 180,
				menuDisabled	: true,
			},
			{
				dataIndex		: 'satuan',
				header			: 'Satuan',
				width			: 70,
				menuDisabled	: true,
			},
			/*{
				dataIndex		: 'checkBHP',
				header			: 'Tag',
				width			: 45,
				menuDisabled	: true,
			},*/
			selectModel,
		]);
		
		
		GridListPencarianObat_GFReturPBF= new Ext.grid.EditorGridPanel({
			id			: 'GrdListPencarianObat_GFReturPBF',
			stripeRows	: true,
			width		: 700,
			height		: 220,
			store		: dsGridListPencarianBHP,
			border		: true,
			frame		: false,
			autoScroll	: true,
			sm 			: selectModel,
			cm			: GridListPencarianObatColumnModel,
			selModel: new Ext.grid.RowSelectionModel({
				singleSelect: true,
				listeners:
				{
					rowselect: function(sm, row, rec)
					{
						currentRowSelectionPencarianObatGFReturPBF = undefined;
						currentRowSelectionPencarianObatGFReturPBF = dsGridListPencarianBHP.getAt(row);
					}
				}
			}),
			listeners	: {
				rowclick: function( $this, rowIndex, e )
				{
					// trcellCurrentTindakan_KasirRWJ = rowIndex;
				},
				celldblclick: function(gridView,htmlElement,columnIndex,dataRecord){
					
				},
				keydown : function(e){
					if(e.getKey() == 13){
						var line	= $this.Grid.detail.getSelectionModel().selection.cell[0];
						console.log(line);
						if($this.ArrayStore.gridDetail.getCount()-1===0)
						{
							$this.ArrayStore.gridDetail.getRange()[line].data.uraian=currentRowSelectionPencarianObatGFReturPBF.data.uraian;
							$this.ArrayStore.gridDetail.getRange()[line].data.kd_milik=currentRowSelectionPencarianObatGFReturPBF.data.kd_milik;
							$this.ArrayStore.gridDetail.getRange()[line].data.no_obat_in=currentRowSelectionPencarianObatGFReturPBF.data.no_obat_in;
							$this.ArrayStore.gridDetail.getRange()[line].data.kd_prd=currentRowSelectionPencarianObatGFReturPBF.data.kd_prd;
							$this.ArrayStore.gridDetail.getRange()[line].data.satuan=currentRowSelectionPencarianObatGFReturPBF.data.satuan;
							$this.ArrayStore.gridDetail.getRange()[line].data.harga_beli=currentRowSelectionPencarianObatGFReturPBF.data.harga_beli;
							$this.ArrayStore.gridDetail.getRange()[line].data.jml_in_obt=currentRowSelectionPencarianObatGFReturPBF.data.jml_in_obt;
							$this.ArrayStore.gridDetail.getRange()[line].data.batch=currentRowSelectionPencarianObatGFReturPBF.data.batch;
							$this.ArrayStore.gridDetail.getRange()[line].data.tgl_exp=currentRowSelectionPencarianObatGFReturPBF.data.tgl_exp;
							$this.ArrayStore.gridDetail.getRange()[line].data.min_stok=currentRowSelectionPencarianObatGFReturPBF.data.min_stok;
							$this.ArrayStore.gridDetail.getRange()[line].data.ppn_item=currentRowSelectionPencarianObatGFReturPBF.data.ppn_item;
							$this.ArrayStore.gridDetail.getRange()[line].data.rcv_line=currentRowSelectionPencarianObatGFReturPBF.data.rcv_line;
							$this.refreshInput();
							FocusExitGFReturPBF = true;
							WindowLookUpSearchListGetBHP.close();
							$this.Grid.detail.startEditing(line,7);
						}else{
							var VALUE_x=1;
							for(var i = 0 ; i < $this.ArrayStore.gridDetail.getCount();i++)
							{
								if($this.ArrayStore.gridDetail.getRange()[i].data.kd_prd===currentRowSelectionPencarianObatGFReturPBF.data.kd_prd)
								{
									VALUE_x=0;
								}
							}
							if(VALUE_x===0){
								Ext.Msg.show({
									title: 'Perhatian',
									msg: 'Anda telah memilih Obat yang sama!',
									buttons: Ext.MessageBox.OK,
									fn: function (btn) {
										if (btn == 'ok')
										{
											GridListPencarianObat_GFReturPBF.getView().refresh();
											GridListPencarianObat_GFReturPBF.getSelectionModel().selectRow(0);
											GridListPencarianObat_GFReturPBF.getView().focusRow(0);
										}
									}
								});
								
							}else{
								$this.ArrayStore.gridDetail.getRange()[line].data.uraian=currentRowSelectionPencarianObatGFReturPBF.data.uraian;
								$this.ArrayStore.gridDetail.getRange()[line].data.kd_milik=currentRowSelectionPencarianObatGFReturPBF.data.kd_milik;
								$this.ArrayStore.gridDetail.getRange()[line].data.no_obat_in=currentRowSelectionPencarianObatGFReturPBF.data.no_obat_in;
								$this.ArrayStore.gridDetail.getRange()[line].data.kd_prd=currentRowSelectionPencarianObatGFReturPBF.data.kd_prd;
								$this.ArrayStore.gridDetail.getRange()[line].data.satuan=currentRowSelectionPencarianObatGFReturPBF.data.satuan;
								$this.ArrayStore.gridDetail.getRange()[line].data.harga_beli=currentRowSelectionPencarianObatGFReturPBF.data.harga_beli;
								$this.ArrayStore.gridDetail.getRange()[line].data.jml_in_obt=currentRowSelectionPencarianObatGFReturPBF.data.jml_in_obt;
								$this.ArrayStore.gridDetail.getRange()[line].data.batch=currentRowSelectionPencarianObatGFReturPBF.data.batch;
								$this.ArrayStore.gridDetail.getRange()[line].data.tgl_exp=currentRowSelectionPencarianObatGFReturPBF.data.tgl_exp;
								$this.ArrayStore.gridDetail.getRange()[line].data.min_stok=currentRowSelectionPencarianObatGFReturPBF.data.min_stok;
								$this.ArrayStore.gridDetail.getRange()[line].data.ppn_item=currentRowSelectionPencarianObatGFReturPBF.data.ppn_item;
								$this.ArrayStore.gridDetail.getRange()[line].data.rcv_line=currentRowSelectionPencarianObatGFReturPBF.data.rcv_line;
								$this.refreshInput();
								FocusExitGFReturPBF = true;
								WindowLookUpSearchListGetBHP.close();
								$this.Grid.detail.startEditing(line,7);
							}
						}
					}
				},
			},
			viewConfig	: {forceFit: true}
		});
		return GridListPencarianObat_GFReturPBF;
	}
	
shortcut.set({
	code:'main',
	list:[
		{
			key:'f1',
			fn:function(){
				Ext.getCmp('btnTambahReturGFReturPBF').el.dom.click();
			}
		},
	]
});

function getListBHPSearch(uraian){ // ini ajax
		//console.log(uraian);
	Ext.Ajax.request ({
		url: baseURL + "index.php/bhp/functionPermintaanBHP/getBHP",
		params: {
			uraian:uraian,
		},
		failure: function(o)
		{
			ShowPesanWarningPermBHP('Error menampilkan pencarian btnHapusTrx_viApotekReturPBFGF. Hubungi Admin!', 'Error');
		},	
		success: function(o) 
		{   
			dsGridListPencarianBHP.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				if(cst.totalrecords == 0 ){
				//	if(PencarianLookupResepRWI == true){						
						Ext.Msg.show({
							title: 'Information',
							msg: 'Tidak ada BHP yang sesuai atau kriteria BHP kurang!',
							buttons: Ext.MessageBox.OK,
							fn: function (btn) {
								if (btn == 'ok')
								{
									var line	= ResepRWI.form.Grid.a.getSelectionModel().selection.cell[0];
									ResepRWI.form.Grid.a.startEditing(line, 4);
									WindowLookUpSearchListGetObat_ResepRWI.close();
								}
							}
						});
					//}
				} else{
					var recs=[],
						recType=dsGridListPencarianBHP.recordType;
					for(var i=0; i<cst.ListDataObj.length; i++){
						recs.push(new recType(cst.ListDataObj[i]));						
					}
					dsGridListPencarianBHP.add(recs);
				/*	$this.Grid.detail.refresh();
					$this.Grid.detail.selectRow(0);
					$this.Grid.detail.focusRow(0);*/
				}
				
			} else {
				ShowPesanWarningPermBHP('Gagal membaca data list Permintaan BHP', 'Error');
			};
		}
	});
}

function datasaveBHP(){
	if(ValidasiEntryPErmintaanBHP(nmHeaderSimpanData,false) == 1 ){
	 var $this=returPBF;
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/bhp/functionPermintaanBHP/savePermintaanBHP",
				params: getParamInsertBHP(),
				failure: function(o){
					ShowPesanWarningPermBHP('Hubungi Admin', 'Error');
				},	
				success: function(o){
					var cst = Ext.decode(o.responseText);
					console.log(cst);
						if (cst.success === true){
						Ext.Msg.show({
							title: 'Information',
							msg: 'Data Permintaan Berhasi di Simpan.',
							buttons: Ext.MessageBox.OK,
							fn: function (btn) {
								if (btn == 'ok')
								{
									$this.TextField.No_permintaan.setValue(cst.no_permintaan);
									get_detail_permintaan_bhp(cst.no_permintaan);
									$this.Grid.detail.getView().refresh();
								}
							}
						});	
						}else{
							Ext.Msg.alert('Proses Simpan Gagal');
						}

				}
			}
			
		)

	}
}

function datasaveBHPPosting(){
	 var $this=returPBF;
		Ext.Ajax.request({
				url: baseURL + "index.php/bhp/functionPermintaanBHP/savePermintaanBHPPosting",
				params: {
					no_permintaan:Ext.getCmp('txt_no_permintaan').getValue()
				},
				failure: function(o){
					ShowPesanWarningPermBHP('Hubungi Admin', 'Error');
				},	
				success: function(o){
					var cst = Ext.decode(o.responseText);
					console.log(cst);
						if (cst.success === true){
								Ext.Msg.alert('Sukses','Data Berhasi di Posting.');
								$this.Button.posting.disable();
								$this.Button.unposting.enable();
								$this.Button.save1.disable();
								$this.DisplayField.posting.setValue('<img src="'+baseURL+'ui/images/icons/16x16/hijau.png"/>'); 
								Q(returPBF.Grid.main).refresh();

					}else{
						Ext.Msg.alert('Proses posting Gagal');
					}
				}
			})
	
}

function datasaveBHPUnPosting(){
  var $this=returPBF;
	  var $this=returPBF;
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/bhp/functionPermintaanBHP/savePermintaanBHPUnPosting",
				params: {
					no_permintaan:Ext.getCmp('txt_no_permintaan').getValue()
				},
						

				failure: function(o)
				{
					ShowPesanWarningPermBHP('Hubungi Admin', 'Error');
				},	
				success: function(o){
					var cst = Ext.decode(o.responseText);
					console.log(cst);
						if (cst.success === true){
								Ext.Msg.alert('Sukses','Data Berhasi di UnPosting.');
								Q(returPBF.Grid.main).refresh();	
								Ext.getCmp('btnSaveGFReturPBF').enable();
								$this.Button.posting.enable();
								$this.Button.unposting.disable();
								$this.DisplayField.posting.setValue('<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>'); 
						}else{
							Ext.Msg.alert('Proses Unposting Gagal');
						}
				}
			}
			
		)
}


function get_unit_kerja(){
	 var $this=returPBF;
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/bhp/functionPermintaanBHP/get_unit_kerja_aktif",
				params: {text:''},
				failure: function(o)
				{
					ShowPesanWarningPermBHP('Hubungi Admin', 'Error');
				},	
				success: function(o){
					var cst = Ext.decode(o.responseText);
					console.log(cst);
				/*	if (cst.success === true) {*/
					$this.TextField.kd_unit_kerja.setValue(cst.kd_unit);
//					$this.TextField.unit_kerja_aktif.setValue(cst.unit);
					/*	console.log(cst.nama);
						alert('123');
					}
					else {
						alert('456');
						ShowPesanErrorResepRWI('Gagal Menyimpan Data ini', 'Error');
						refreshRespApotekRWI();
					};*/
				}
			}
			
		)
	
}
function getParamInsertBHP(){
var $this=returPBF;	
	var params = {
		no_permintaan:Ext.getCmp('txt_no_permintaan').getValue(),
		tanggal:Ext.getCmp('cbo_tanggal_permintaan').getValue(),
		kd_bagian:Ext.getCmp('txt_id_unit_kerja').getValue(),
		keterangan: Ext.getCmp('txt_keterangan_permintaan').getValue(),
		posting:Ext.getCmp('txt_posting').getValue()
	}

		params['jumlah']=$this.ArrayStore.gridDetail.getCount();
		for(var i = 0 ; i < $this.ArrayStore.gridDetail.getCount();i++){
			params['kd_kelompok-'+i]=$this.ArrayStore.gridDetail.data.items[i].data.kode
			params['kode-'+i]=$this.ArrayStore.gridDetail.data.items[i].data.kode
			params['qty-'+i]=$this.ArrayStore.gridDetail.data.items[i].data.qty
			params['line-'+i]=$this.ArrayStore.gridDetail.data.items[i].data.line
			params['no_urut_brg-'+i]=$this.ArrayStore.gridDetail.data.items[i].data.kode
		}	
	console.log(params);
    return params

};

function get_detail_permintaan_bhp(no_permintaan){
	var $this=returPBF;
	 $.ajax({
 			url:baseURL + "index.php/bhp/functionPermintaanBHP/getAllPermintaanDetail",
 			dataType:'JSON',
 			type: 'POST',
 			data: {'no_permintaan':no_permintaan},
 			success: function(r){
 				loadMask.hide();
 				if(r.processResult=='SUCCESS'){	
 					setLookUps_viBHPPermintaan.show();
 					var o=r.resultObject;
 					$this.TextField.No_permintaan.setValue(o.no_permintaan);// set grid
 					$this.TextField.remark.setValue(o.keterangan).enable();
 					var date = Date.parseDate(o.tanggal, "Y-m-d H:i:s");
 					$this.DateField.date.setValue(date).disable(0);
 					$this.ArrayStore.gridDetail.loadData([],false);
 					for(var i=0, iLen=r.listData.length ; i<iLen ; i++){
 						$this.ArrayStore.gridDetail.add(new $this.ArrayStore.gridDetail.recordType(r.listData[i]));
 					}

 					console.log(o);
 					if(o.posting==1){	
 						$this.DisplayField.posting.setValue('<img src="'+baseURL+'ui/images/icons/16x16/hijau.png"/>');
 						Ext.getCmp('btnSaveGFReturPBF').disable();
 						Ext.getCmp('btnPostingPermintaanBHP').disable();
 						
 					}else{
 						$this.DisplayField.posting.setValue('<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>');
 					}
 					$this.refreshInput();

 				}else{
 					Ext.Msg.alert('Gagal',r.processMessage);
 				}
 			},
 			error: function(jqXHR, exception) {
 				loadMask.hide();
 				Nci.ajax.ErrorMessage(jqXHR, exception);
 			}
 		});
}


function ValidasiEntryPErmintaanBHP(modul,mBolHapus){
	var $this=returPBF;
	var x = 1;
	for(var i=0; i < $this.ArrayStore.gridDetail.getCount()	; i++){
		var o=$this.ArrayStore.gridDetail.getRange()[i].data;
		//console.log(o);
		if(o.kode == undefined || o.kode == '' ){
			$this.ArrayStore.gridDetail.removeAt(i);
			$this.Grid.detail.getView().refresh(); 
			x=0;
		}else{
			if(o.qty == undefined || o.qty == '' || o.qty == 0 || o.qty == '0' ){
				ShowPesanWarningPermBHP('QTY belum di isi, qty tidak boleh kosong!', 'Konfirmasi');	
						$this.Grid.detail.startEditing(i, 6);
						x = 0;
				
			}


		} 
		
		console.log(o.qty,i);
	}
	
	return x;
};


function comboJenisLokasiInvDistribusiBHP(){ //disini
	var Field_Vendor = ['kd_lokasi', 'lokasi'];
    ds_JnsLokasiInvDistribusiBHP_front = new WebApp.DataStore({fields: Field_Vendor});
   /* ds_JnsLokasiInvDistribusiBHP.load({
        params:{
	        Skip: 0,
	        Take: 1000,
	        Sort: 'kd_jns_lokasi',
	        Sortdir: 'ASC',
	        target: 'ComboJnsLokasiBHP',
	        param: ""
        }
    });*/
	var cboJnsLokasiInvDistribusiBHP = new Ext.form.ComboBox({
		// x:80,
		// y:30,
        id:'cbJnsLokasiInvDistribusiBHP_permintaan_front',
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:true,
        mode: 'local',
      	width : 150,
		readOnly:false,
		fieldLabel:'PBF ',
        store: ds_JnsLokasiInvDistribusiBHP_front,
        valueField: 'kd_lokasi',
        displayField: 'lokasi',
        listeners:{
			'select': function(a,b,c){
				//loaddatastoreLokasiInvDistribusBHP(b.data.kd_lokasi);
				Ext.getCmp('txt_id_unit_kerja').setValue(b.data.kd_lokasi);
			}
        }
	});
	return cboJnsLokasiInvDistribusiBHP;
};

function comboJenisLokasiInvDistribusiBHP_belakang (){ //disini
	var Field_Vendor = ['kd_lokasi', 'lokasi'];
    ds_JnsLokasiInvDistribusiBHP = new WebApp.DataStore({fields: Field_Vendor});
   /* ds_JnsLokasiInvDistribusiBHP.load({
        params:{
	        Skip: 0,
	        Take: 1000,
	        Sort: 'kd_jns_lokasi',
	        Sortdir: 'ASC',
	        target: 'ComboJnsLokasiBHP',
	        param: ""
        }
    });*/
	var cboJnsLokasiInvDistribusiBHP = new Ext.form.ComboBox({
		// x:80,
		// y:30,
        id:'cbJnsLokasiInvDistribusiBHP_permintaan_back',
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:true,
        mode: 'local',
      	width : 150,
		readOnly:false,
		fieldLabel:'PBF ',
        store: ds_JnsLokasiInvDistribusiBHP,
        valueField: 'kd_lokasi',
        displayField: 'lokasi',
        listeners:{
			'select': function(a,b,c){
				//loaddatastoreLokasiInvDistribusBHP(b.data.kd_lokasi);
				Ext.getCmp('txt_id_unit_kerja').setValue(b.data.kd_lokasi);
			}
        }
	});
	return cboJnsLokasiInvDistribusiBHP;
};
function loadDataComboLokasi(){
	Ext.Ajax.request({
	url: baseURL + "index.php/bhp/function_lokasi/get_combo_lokasi_permintaan",
		params: '0',
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType =  ds_JnsLokasiInvDistribusiBHP.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				ds_JnsLokasiInvDistribusiBHP.add(recs);
			}
				console.log(ds_JnsLokasiInvDistribusiBHP);
		}
	});
}

function loadDataComboLokasi_front(){
	Ext.Ajax.request({
	url: baseURL + "index.php/bhp/function_lokasi/get_combo_lokasi_permintaan",
		params: '0',
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType =  ds_JnsLokasiInvDistribusiBHP_front.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				ds_JnsLokasiInvDistribusiBHP_front.add(recs);
			}
				console.log(ds_JnsLokasiInvDistribusiBHP_front);
		}
	});
}