var StatusPostingGFReturBHP='0';
var currentRowSelectionPencarianbarangGFReturBHP;
var PencarianLookupGFReturBHP; // Variabel pembeda getdata u/ pencarian nama barang dan semua kepemilikan atau bukan. False => pencarian berdasarkan nama_barang dan kepemilikan, True => pencarian berdasarkan nama_barang saja
var FocusExitGFReturBHP=false;
var returBHP={
	coba:function(){
		var $this=this;
	},
	vars:{
		lineDetail:null,
		shortcut:false,
		total:0,
		subTotal:0,
		ppn:0
	},
	ArrayStore:{
		gridDetail:new Ext.data.ArrayStore({fields:[]}),
		combobarang:new Ext.data.ArrayStore({fields:[]}),
		gridMain:new Ext.data.ArrayStore({fields:[]})
	},
	Button:{
		deleted:null,
		posting:null,
		save1:null,
		save2:null,
		addDetail:null,
		unposting:null,
		cetak:null
	},
	ComboBox:{
		vendor:null
	},
	DataStore:{
		vendor1:new WebApp.DataStore({fields: ['kd_vendor', 'vendor']})
	},
	DateField:{
		date:null,
		srchStartDate:null,
		srchLastDate:null
	},
	DisplayField:{
		posting:null
	},
	Grid:{
		detail:null,
		main:null
	},
	ComboBox:{
		vendor1:null
	},
	TextField:{
		subtotal:null,
		noRetur:null,
		remark:null,
		ppn:null,
		reduksi:null,
		total:null,
		srchRetNumber:null
	},
	delDetail:function(){
		var $this=this;
		var line = $this.Grid.detail.getSelectionModel().selection.cell[0];
		var o = $this.ArrayStore.gridDetail.getRange()[line].data;
		Ext.Msg.confirm('Warning', 'Apakah barang ini akan dihapus?', function(button){
			if (button == 'yes'){
				if(o.ret_line === undefined || o.ret_line === '' || o.ret_line === 'undefined'){
					$this.ArrayStore.gridDetail.removeAt(line);
					$this.vars.lineDetail=null;
					$this.refreshInput();
					
				}else{
					Ext.Ajax.request ({
						url: baseURL + "index.php/bhp/functionReturBHP/deletedetail",
						params:{
							kd_prd: o.kd_prd,
							ret_number: $this.TextField.noRetur.getValue(),
							ret_line: o.ret_line,
						} ,
						failure: function(o) {
							Ext.Msg.alert('Error','Error, delete barang. Hubungi Admin!');
						},	
						success: function(o) 
						{
							var cst = Ext.decode(o.responseText);
							if (cst.success === true)  {
								$this.ArrayStore.gridDetail.removeAt(line);
								$this.vars.lineDetail=null;
								$this.refreshInput();
								Ext.Msg.alert('Information','Berhasil menghapus data barang ini');
							} else  {
								Ext.Msg.alert('Error','Gagal menghapus data barang ini');
							};
						}
					})
				}
			} 
		});
	},
	addDetail:function(){
		var $this=this;
		if(Ext.getCmp('cboBHP_viReturBHP').getValue()!=''){
			$this.TextField.subtotal.focus();
			$this.ArrayStore.gridDetail.add(new $this.ArrayStore.gridDetail.recordType());
			$this.refreshInput();
		}else{
			Ext.Msg.alert('Gagal','Harap Pilih BHP terlebih dahulu.');
		}
	},
	refresh:function(){
		var $this=this;
		loadMask.show();
		$this.DateField.srchStartDate
		var a=[];
		a.push({name: 'ret_number',value:$this.TextField.srchRetNumber.getValue()});
		a.push({name: 'startDate',value:$this.DateField.srchStartDate.value});
		a.push({name: 'lastDate',value:$this.DateField.srchLastDate.value});
		a.push({name: 'kd_vendor',value:Ext.getCmp('cboBHP_viReturBHPFilter').getValue()});
		a.push({name: 'status_posting',value:$this.ComboBox.posting.value});
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			url:baseURL + "index.php/bhp/functionReturBHP/initList",
			data:a,
			success: function(r){
				loadMask.hide();
				if(r.processResult=='SUCCESS'){
					$this.ArrayStore.gridMain.loadData([],false);
					for(var i=0,iLen=r.listData.length; i<iLen ;i++){
						$this.ArrayStore.gridMain.add(new $this.ArrayStore.gridMain.recordType(r.listData[i]));
					}
					$this.Grid.main.getView().refresh();
				}else{
					Ext.Msg.alert('Gagal',r.processMessage);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
	},
	posting:function(){
		var $this=this;
		if($this.TextField.noRetur.getValue()!=''){
			if($this.getParams() != undefined){
				Ext.Msg.confirm('Konfirmasi', 'Apakah Akan diPosting ?', function (id, value) { 
					if (id === 'yes') { 
						loadMask.show();
						$.ajax({
							url:baseURL + "index.php/bhp/functionReturBHP/posting",
							dataType:'JSON',
							type: 'POST',
							data:$this.getParams(),
							success: function(r){
								loadMask.hide();
								if(r.processResult=='SUCCESS'){
									Ext.Msg.alert('Sukses','Data Berhasi Diubah dan diPosting.');
									Q(returBHP.Grid.main).refresh();
									for(var i=0; i<$this.ArrayStore.gridDetail.getCount() ; i++){
										var o=$this.ArrayStore.gridDetail.getRange()[i].data;
										o.ret_number=$this.TextField.noRetur.getValue();
									}
									StatusPostingGFReturBHP='1';
									$this.refreshInput();
									Ext.getCmp('btnSaveGFReturBHP').disable();
									Ext.getCmp('btnDeleteGFReturBHP').disable();
									Ext.getCmp('btnPostingGFReturBHP').disable();
									Ext.getCmp('btnAddDetailGFReturBHP').disable();
									Ext.getCmp('btnUnPostingGFReturBHP').enable();
									Ext.getCmp('btnCetakBuktiGFReturBHP').enable();
									
								}else{
									Ext.Msg.alert('Gagal',r.processMessage);
								}
							},
							error: function(jqXHR, exception) {
								loadMask.hide();
								Nci.ajax.ErrorMessage(jqXHR, exception);
							}
						});
					} 
				}, this); 
			}
		} else{
			Ext.Msg.alert('Warning','Simpan terlebih dahulu sebelum diPosting!');
		}
	},
	unposting:function(){
		var $this=this;
		if($this.getParams() != undefined){
			Ext.Msg.confirm('Konfirmasi', 'Apakah Akan diUnposting ?', function (id, value) { 
				if (id === 'yes') { 
					loadMask.show();
					$.ajax({
						url:baseURL + "index.php/bhp/functionReturBHP/unposting",
						dataType:'JSON',
						type: 'POST',
						data:{ret_number:$this.TextField.noRetur.getValue()},
						success: function(r){
							loadMask.hide();
							if(r.processResult=='SUCCESS'){
								Ext.Msg.alert('Sukses','Data Berhasi diUnposting.');
								Q(returBHP.Grid.main).refresh();
								StatusPostingGFReturBHP='0';
								$this.refreshInput();
								Ext.getCmp('btnSaveGFReturBHP').enable();
								Ext.getCmp('btnDeleteGFReturBHP').enable();
								Ext.getCmp('btnPostingGFReturBHP').enable();
								Ext.getCmp('btnAddDetailGFReturBHP').enable();
								Ext.getCmp('btnUnPostingGFReturBHP').disable();
								Ext.getCmp('btnCetakBuktiGFReturBHP').disable();
							}else{
								Ext.Msg.alert('Gagal',r.processMessage);
							}
						},
						error: function(jqXHR, exception) {
							loadMask.hide();
							Nci.ajax.ErrorMessage(jqXHR, exception);
						}
					});
				} 
			}, this); 
		}
	},
	cetak:function(){
		var $this=this;
		Ext.Msg.confirm('Konfirmasi', 'Siap mencetak bill?', function (id, value) { 
			if (id === 'yes') {
				var params={
					ret_number:$this.TextField.noRetur.getValue()
				} ;
				var form = document.createElement("form");
				form.setAttribute("method", "post");
				form.setAttribute("target", "_blank");
				form.setAttribute("action", baseURL + "index.php/bhp/cetakan_faktur_bill/cetakreturbhp");
				var hiddenField = document.createElement("input");
				hiddenField.setAttribute("type", "hidden");
				hiddenField.setAttribute("name", "data");
				hiddenField.setAttribute("value", Ext.encode(params));
				form.appendChild(hiddenField);
				document.body.appendChild(form);
				form.submit();		
			}
		})
	},
	save:function(callback){
		var $this=this;
		if($this.getParams() != undefined){
			/* if ($this.validasiSaveReturBHP()==1)	
			{ */
				if($this.TextField.noRetur.getValue()==''){
					loadMask.show();
					$.ajax({
						url:baseURL + "index.php/bhp/functionReturBHP/save",
						dataType:'JSON',
						type: 'POST',
						data:$this.getParams(),
						success: function(r){
							loadMask.hide();
							if(r.processResult=='SUCCESS'){
								$this.TextField.noRetur.setValue(r.resultObject.code);
								for(var i=0; i<$this.ArrayStore.gridDetail.getCount() ; i++){
									var o=$this.ArrayStore.gridDetail.getRange()[i].data;
									o.ret_number=r.resultObject.code;
								}
								$this.ArrayStore.gridDetail.removeAll();
								getDetailListbarang(r.listData,r.totalLisData);
								$this.refreshInput();
								Ext.Msg.alert('Sukses','Data Berhasi Disimpan.');
								Ext.getCmp('btnSaveGFReturBHP').enable();
								Ext.getCmp('btnDeleteGFReturBHP').enable();
								Ext.getCmp('btnPostingGFReturBHP').enable();
								Ext.getCmp('btnAddDetailGFReturBHP').enable();
								Ext.getCmp('btnUnPostingGFReturBHP').disable();
								Ext.getCmp('btnCetakBuktiGFReturBHP').disable();
								if(callback != undefined){
									callback();
								}
							}else{
								Ext.Msg.alert('Gagal',r.processMessage);
							}
						},
						error: function(jqXHR, exception) {
							loadMask.hide();
							Nci.ajax.ErrorMessage(jqXHR, exception);
						}
					});
				}else{
					loadMask.show();
					$.ajax({
						url:baseURL + "index.php/bhp/functionReturBHP/update",
						dataType:'JSON',
						type: 'POST',
						data:$this.getParams(),
						success: function(r){
							loadMask.hide();
							if(r.processResult=='SUCCESS'){
								Ext.Msg.alert('Sukses','Data Berhasi Diubah.');
								for(var i=0; i<$this.ArrayStore.gridDetail.getCount() ; i++){
									var o=$this.ArrayStore.gridDetail.getRange()[i].data;
									o.ret_number=$this.TextField.noRetur.getValue();
								}
								$this.ArrayStore.gridDetail.removeAll();
								getDetailListbarang(r.listData,r.totalLisData);
								$this.refreshInput();
								if(callback != undefined){
									callback();
								}
								Ext.getCmp('btnSaveGFReturBHP').enable();
								Ext.getCmp('btnDeleteGFReturBHP').enable();
								Ext.getCmp('btnPostingGFReturBHP').enable();
								Ext.getCmp('btnAddDetailGFReturBHP').enable();
								Ext.getCmp('btnUnPostingGFReturBHP').disable();
								Ext.getCmp('btnCetakBuktiGFReturBHP').disable();
							}else{
								Ext.Msg.alert('Gagal',r.processMessage);
							}
						},
						error: function(jqXHR, exception) {
							loadMask.hide();
							Nci.ajax.ErrorMessage(jqXHR, exception);
						}
					});
				}
			//}
				
		}
	},
	validasiSaveReturBHP:function(){
		var $this=this;
		var x = 1;
		for(var i = 0 ; i < $this.ArrayStore.gridDetail.getCount();i++)
		{
			var o=$this.ArrayStore.gridDetail.getRange()[i].data;
			for(var j = i+1 ; j < $this.ArrayStore.gridDetail.getCount();j++)
			{
				var p=$this.ArrayStore.gridDetail.getRange()[j].data;
				if (p.kd_prd==o.kd_prd)
				{
					Ext.Msg.alert('Warning','barang ada yang Sama');
					x = 0;
				}
			}
			
		}
		return x;
	},
	getParams:function(){
		var $this=this;
		var a=[];
		if(Ext.getCmp('cboBHP_viReturBHP').getValue()!=''){
			a.push({name: 'kd_vendor',value:Ext.getCmp('cboBHP_viReturBHP').getValue()});
		}else{
			Ext.Msg.alert('Error','Harap Pilih Vendor.');
			$('#'+Ext.getCmp('cboBHP_viReturBHP').id).focus();
			return;
		}
		if($this.TextField.remark.getValue()!=''){
			a.push({name: 'remark',value:$this.TextField.remark.getValue()});
		}else{
			Ext.Msg.alert('Error','Harap Isi Remark.');
			$('#'+$this.TextField.remark.id).focus();
			return;
		}
		a.push({name: 'ret_date',value:$this.DateField.date.value});
		a.push({name: 'ret_number',value:$this.TextField.noRetur.getValue()});
		//a.push({name: 'ppn',value:toInteger($this.TextField.ppn.getValue())});
		if($this.ArrayStore.gridDetail.getCount()==0){
			Ext.Msg.alert('Error','barang harus lebih dari 1.');
			return;
		}
		for(var i=0; i<$this.ArrayStore.gridDetail.getCount();i++){
			var o=$this.ArrayStore.gridDetail.getRange()[i].data;
			if(o.nama_barang != undefined && o.nama_barang != ''){
				a.push({name: 'kd_prd[]',value:o.kd_prd});
				if(o.qty != 0 && o.qty !=''){
					a.push({name: 'qty[]',value:o.qty});
				}else{
					Ext.Msg.alert('Error','Harap isi qty produk pada baris ke-'+(i+1));
					$this.Grid.detail.startEditing(i,6);
					return;
				}
			}else{
				$this.ArrayStore.gridDetail.removeAt(i);
				$this.Grid.detail.getView().refresh();
				// Ext.Msg.alert('Error','Harap pilih produk pada baris ke-'+(i+1));
				// $this.Grid.detail.startEditing(i,2);
				// return;
			}
			
			if(o.ret_reduksi != undefined && o.ret_reduksi !=''){
				a.push({name: 'ret_reduksi[]',value:o.ret_reduksi});
			}else{
				a.push({name: 'ret_reduksi[]',value:0});
				// return;
			}
			//a.push({name: 'tgl_exp[]',value:o.tgl_exp});
			//a.push({name: 'batch[]',value:o.batch});
			a.push({name: 'po_number[]',value:o.po_number});
			a.push({name: 'no_terima[]',value:o.no_terima});
			a.push({name: 'no_baris[]',value:o.no_baris});
			//a.push({name: 'rcv_line[]',value:o.rcv_line});
			a.push({name: 'ret_line[]',value:o.ret_line});
		}
		return a;
	},
	/* setShortcut:function(){
		var $this=this;
		$(window).unbind().keydown(function(event) {
    		if($this.vars.shortcut==true){
    			if(event.ctrlKey &&  event.which==66){
    				$this.addDetail();
    				event.preventDefault();
					return false;
    			}
			   	if(event.ctrlKey &&  event.which==83){
    				$this.save();
    				event.preventDefault();
					return false;
    			}
    			if(event.ctrlKey &&  event.which==68){
    				if($this.Button.deleted.disabled==false)$this.delDetail();
    				event.preventDefault();
					return false;
    			}
    		}
		});
	}, */
	refreshInput:function(){
		var $this=this;
		if($this.ArrayStore.gridDetail.getCount()>0){
			$this.Button.deleted.enable();
		}else{
			$this.Button.deleted.disable();
		}
		if($this.TextField.noRetur.getValue()==''){
			$this.Button.posting.disable();	
			$this.Button.unposting.disable();	
			$this.Button.cetak.disable();	
			Ext.getCmp('cboBHP_viReturBHP').enable();
			$this.TextField.remark.enable();
			$this.DateField.date.enable();
		}else{
			$this.Button.posting.enable();	
			$this.Button.unposting.enable();	
			$this.Button.cetak.enable();	
			Ext.getCmp('cboBHP_viReturBHP').disable();
			$this.TextField.remark.disable();
			$this.DateField.date.disable();
		}
		if(StatusPostingGFReturBHP=='0'){
			$this.Button.deleted.enable();
			$this.Button.save1.enable();
			$this.Button.save2.enable();
			$this.Button.addDetail.enable();
			$this.Button.posting.enable();	
			$this.Button.unposting.disable();
			$this.Button.cetak.disable();
			$this.DisplayField.posting.setValue('<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>');
		}else{
			$this.Button.deleted.disable();
			$this.Button.save1.disable();
			$this.Button.save2.disable();
			$this.Button.addDetail.disable();
			$this.Button.posting.disable();	
			$this.Button.unposting.enable();
			$this.Button.cetak.enable();
			$this.DisplayField.posting.setValue('<img src="'+baseURL+'ui/images/icons/16x16/hijau.png"/>');
		}
		var barang={};
		var subTotal=0;
		var reduksi=0;
		for(var i=0;i<$this.ArrayStore.gridDetail.getRange().length ;i++){
			var o=$this.ArrayStore.gridDetail.getRange()[i].data;
			if(o.harga_beli==undefined || o.harga_beli=='')o.harga_beli=0;
			if(o.qty==undefined || o.qty=='')o.qty=0;
			if(o.ret_reduksi==undefined || o.ret_reduksi=='')o.ret_reduksi=0;
			if(o.jumlah_in==undefined || o.jumlah_in=='')o.jumlah_in=0;
			if(o.kd_prd != undefined){
				if(barang[o.kd_prd+'-'+o.po_number+'-'+o.batch] ==undefined){
					barang[o.kd_prd+'-'+o.po_number+'-'+o.batch]={count:parseFloat(o.qty),lastCount:[parseFloat(o.qty)]};
				}else{
					barang[o.kd_prd+'-'+o.po_number+'-'+o.batch].count+=parseFloat(o.qty);
					barang[o.kd_prd+'-'+o.po_number+'-'+o.batch].lastCount.push(parseFloat(o.qty));
				}
				if(o.jumlah_in<barang[o.kd_prd+'-'+o.po_number+'-'+o.batch].count){
					var jumCount=0
					for(var j=0; j<(barang[o.kd_prd+'-'+o.po_number+'-'+o.batch].lastCount.length-1);j++){
						jumCount+=barang[o.kd_prd+'-'+o.po_number+'-'+o.batch].lastCount[j];
					}
					o.qty=parseFloat(o.jumlah_in)-jumCount;
					//Ext.Msg.alert('Error','Quantity tidak boleh lebih dari jumlah Stok.');
				}
			}
			o.jumlah=parseFloat(o.qty)*parseFloat(o.harga_beli);
			subTotal+=o.jumlah;
			reduksi+=parseFloat(o.ret_reduksi);
			
		}
		$this.TextField.subtotal.setValue(formatNumberDecimal(toFormat(parseFloat(subTotal)),0));
		var ppn=(parseFloat(subTotal)/100)*10;
		$this.TextField.ppn.setValue(formatNumberDecimalParam(toFormat(ppn),0));
		$this.TextField.reduksi.setValue(toFormat(reduksi));
		var total=(parseFloat(subTotal)+ppn)-reduksi;
		$this.vars.total=total;
		$this.vars.subTotal=subTotal;
		$this.vars.ppn=ppn;
		$this.TextField.total.setValue(formatNumberDecimalParam(toFormat(total),0));
		$this.Grid.detail.getView().refresh();
	}
}

var dataSource_viReturBHP;
var selectCount_viReturBHP=50;
var NamaForm_viReturBHP="Retur BHP ";
var mod_name_viReturBHP="viRetur BHP";
var now_viReturBHP= new Date();
var addNew_viReturBHP;
var rowSelected_viReturBHP;
var setLookUps_viReturBHP;
var mNoKunjungan_viReturBHP='';

var CurrentData_viReturBHP ={
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viReturBHP(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
Q(returBHP.Grid.main).refresh();
function ShowPesanWarningReturBHPGF(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorReturBHPGF(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:290
		}
	);
};


function ShowPesanInfoReturBHPGF(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:350
		}
	);
};
function dataGrid_viReturBHP(mod_id_viReturBHP){	
    var $this=returBHP;
	$this.Grid.main = Q().table({
		border:false,
		rowdblclick: function (sm, ridx, cidx,store){
			rowSelected_viReturBHP = store.getAt(ridx);
			if (rowSelected_viReturBHP != undefined){
				setLookUp_viReturBHP(rowSelected_viReturBHP.data);
			}else{
				setLookUp_viReturBHP();
			}
		},
		rowselect: function(sm, row, rec,store){
			rowSelected_viReturBHP = undefined;
			rowSelected_viReturBHP = store.getAt(row);
			
			console.log(rowSelected_viReturBHP);
			CurrentData_viReturBHP
			CurrentData_viReturBHP.row = row;
			CurrentData_viReturBHP.data = store;
			if (rowSelected_viReturBHP.data.ret_post==='0')
							{
								Ext.getCmp('btnHapusTrx_viReturBHPGF').enable();
							}
							else
							{
								Ext.getCmp('btnHapusTrx_viReturBHPGF').disable();
							}
		},
		tbar:{
			xtype: 'toolbar',
			id: 'toolbar_viReturBHP',
			items: 
			[
				{
					xtype: 'button',
					text: 'Tambah [F1]',
					iconCls: 'add',
					tooltip: 'Add Data',
					id:'btnTambahReturGFReturBHP',
					handler: function(sm, row, rec){
						StatusPostingGFReturBHP='0';
						setLookUp_viReturBHP();
					}
				},{
					xtype: 'button',
					text: 'Ubah',
					iconCls: 'Edit_Tr',
					tooltip: 'Edit Data',
					handler: function(sm, row, rec){
						if (rowSelected_viReturBHP != undefined){
							setLookUp_viReturBHP(rowSelected_viReturBHP.data)
						}
					}
				},
				{
						xtype: 'button',
						text: 'Hapus Transaksi',
						iconCls: 'remove',
						tooltip: 'Hapus Data',
						disabled:true,
						id: 'btnHapusTrx_viReturBHPGF',
						handler: function(sm, row, rec)
						{
							var datanya=rowSelected_viReturBHP.data;
							if (datanya===undefined){
								ShowPesanWarningReturBHPGF('Belum ada data yang dipilih','BHP');
							}
							else
							{
								 Ext.Msg.show({
									title: 'Hapus Transaksi',
									msg: 'Anda yakin akan menghapus data transaksi ini ?',
									buttons: Ext.MessageBox.YESNO,
									fn: function (btn) {
										if (btn == 'yes')
										{
											
											console.log(datanya);
											var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan Pembatalan Transaksi:', function (btn, combo) {
														if (btn == 'ok')
														{
															var variablebatalhistori_returBHP = combo;
															if (variablebatalhistori_returBHP != '')
															{
																 Ext.Ajax.request({
										   
																		url: baseURL + "index.php/bhp/functionReturBHP/hapusTrxReturPBF",
																		 params: {
																		
																			ret_date: datanya.ret_date,
																			ret_number: datanya.ret_number,
																			alasan: variablebatalhistori_returBHP
																		
																		},
																		failure: function(o)
																		{
																			 var cst = Ext.decode(o.responseText);
																			ShowPesanWarningReturBHPGF('Data transaksi tidak dapat dihapus','BHP');
																		},	    
																		success: function(o) {
																			var cst = Ext.decode(o.responseText);
																			if (cst.success===true)
																			{
																				Q(returBHP.Grid.main).refresh();
																				ShowPesanInfoReturBHPGF('Data transaksi Berhasil dihapus','BHP');
																				Ext.getCmp('btnHapusTrx_viReturBHPGF').disable();
																			}
																			else
																			{
																				ShowPesanErrorReturBHPGF('Data transaksi tidak dapat dihapus','BHP');
																			}
																			
																		}
																
																})
															} else
															{
																ShowPesanWarningReturBHPGF('Silahkan isi alasan terlebih dahaulu', 'Keterangan');

															}
														}

													}); 
											
										}
									},
									icon: Ext.MessageBox.QUESTION
								});
							} 
						}
					},
			]
		},
		ajax:function(data,callback){
			loadMask.show();
			var a=[];
			a.push({name: 'ret_number',value:$this.TextField.srchRetNumber.getValue()});
			a.push({name: 'startDate',value:$this.DateField.srchStartDate.value});
			a.push({name: 'lastDate',value:$this.DateField.srchLastDate.value});
			a.push({name: 'kd_vendor',value:Ext.getCmp('cboBHP_viReturBHPFilter').getValue()});
			a.push({name: 'status_posting',value:$this.ComboBox.posting.value});
			a.push({name: 'start',value:data.start});
			a.push({name: 'size',value:data.size});
			$.ajax({
				type: 'POST',
				dataType:'JSON',
				url:baseURL + "index.php/bhp/functionReturBHP/initList",
				data:a,
				success: function(r){
					loadMask.hide();
					if(r.processResult=='SUCCESS'){
						callback(r.listData,r.total);
					}else{
						Ext.Msg.alert('Gagal',r.processMessage);
					}
				},
				error: function(jqXHR, exception) {
					loadMask.hide();
					Nci.ajax.ErrorMessage(jqXHR, exception);
				}
			});
		},
		column:new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(),
			{
				header		: 'Status Posting',
				width		: 20,
				sortable	: false,
				hideable	: true,
				hidden		: false,
				menuDisabled: true,
				dataIndex	: 'ret_post',
				renderer	: function(value, metaData, record, rowIndex, colIndex, store){
					 switch (value){
						 case '1':
							 metaData.css = 'StatusHijau'; 
							 break;
						 case '0':
							 metaData.css = 'StatusMerah';
							 break;
					 }
					 return '';
				}
			},{
				header: 'No. Retur',
				dataIndex: 'ret_number',
				sortable: false,
				menuDisabled: true,
				width: 35
			},{
				header:'Tgl Retur',
				dataIndex: 'ret_date',						
				width: 20,
				sortable: false,
				menuDisabled: true,
				format: 'd/M/Y',
				filter: {},
				renderer: function(v, params, record){
					return ShowDate(record.data.ret_date);
				}
			}/* ,{
				header: 'BHP',
				dataIndex: 'vendor',
				sortable: false,
				menuDisabled: true,
				width: 60
			} */
		])
	});

    var FrmFilterGridDataView_viReturBHP = new Ext.Panel({
		title: NamaForm_viReturBHP,
		iconCls: 'Studi_Lanjut',
		id: mod_id_viReturBHP,
		region: 'center',
		layout: 'form', 
		closable: true,        
		border: false,  
		margins: '0 5 5 0',
		items: [
		{
			xtype		: 'panel',
			plain		: false,
			anchor		: '100%',
			bodyStyle	: 'padding: 2px;',
			items	: [
			{
				layout	: 'column',
				border	: false,
				bodyStyle: 'padding: 5px;',
				items	:[
					{
						layout	: 'form',
						border	: false,
						items	: [
							{
								xtype: 'buttongroup',
								columns: 11,
								defaults: {
									scale: 'small'
								},
								frame: false,
								items: [
									{ 
										xtype: 'tbtext', 
										text: 'No. Retur : ', 
										style:{'text-align':'right','font-size':'11px'},
										width: 90,
										height: 25
									},						
									$this.TextField.srchRetNumber=new Ext.form.TextField({
										emptyText: 'No. Retur',
										width: 100,
										listeners:{ 
											'specialkey' : function(){
												if (Ext.EventObject.getKey() === 13) {
													Q(returBHP.Grid.main).refresh();
												} 						
											}
										}
									}),{	 
										xtype: 'tbspacer',
										width: 135,
										height: 25
									},{ 
										xtype: 'tbtext', 
										text: 'Tgl Retur : ', 
										style:{'text-align':'right','font-size':'11px'},
										width: 65,
										height: 25
									},
									$this.DateField.srchStartDate=new Ext.form.DateField({
										value: now_viReturBHP,
										format: 'd/M/Y',
										width: 120,
										listeners:{ 
											'specialkey' : function(){
												if (Ext.EventObject.getKey() === 13){
													Q(returBHP.Grid.main).refresh();
												} 						
											}
										}
									}),{ 
										xtype: 'tbtext', 
										text: ' s.d ', 
										style:{'text-align':'center','font-size':'11px'},
										width: 30,
										height: 25
									},$this.DateField.srchLastDate=new Ext.form.DateField({
										value: now_viReturBHP,
										format: 'd/M/Y',
										width: 120,
										listeners:{ 
											'specialkey' : function(){
												if (Ext.EventObject.getKey() === 13){
													Q(returBHP.Grid.main).refresh();
												} 						
											}
										}
									}),{	 
										xtype: 'tbspacer',
										width: 10,
										height: 25
									},
								]
							},
							{
								xtype: 'buttongroup',
								columns: 11,
								defaults: {
									scale: 'small'
								},
								frame: false,
								items: 
								[
									{	 
										xtype: 'tbspacer',
										width: 55,
										height: 25
									},
									{ 
										xtype: 'tbtext', 
										text: 'Vendor : ', 
										style:{'text-align':'right','font-size':'11px'},
										width: 35,
										height: 25
									},		
									viCombo_Vendor(150, 'cboBHP_viReturBHPFilter'),	
									{ 
										xtype: 'tbtext', 
										text: 'Posting : ', 
										style:{'text-align':'right','font-size':'11px'},
										width: 100
									},		
									$this.ComboBox.posting = new Ext.form.ComboBox({
										fieldLabel: 'Posting',
										valueField: 'displayText',
										displayField: 'displayText',
										id:'cmbStatusPostingGFReturBHP',
										store: new Ext.data.ArrayStore
										(
											{
												id: 0,
												fields:
													[
															'Id',
															'displayText'
													],
												data: [[1, 'Semua'],[2,'Posting'],[3,'Belum Posting']]
											}
										),
										width: 100,
										value:'Belum Posting',
										mode: 'local',
										typeAhead: true,
										triggerAction: 'all',                        
										name: 'post',
										lazyRender: true,
										id: 'cbPostingGFPengeluranUnit',
										listeners:{ 
											'specialkey' : function(){
												if (Ext.EventObject.getKey() === 13){
													Q(returBHP.Grid.main).refresh();
												} 							
											},
											'select' : function(){
												Q(returBHP.Grid.main).refresh();
											}
										}
									}),
									{	 
										xtype: 'tbspacer',
										width: 150,
										height: 25
									},
									{
										xtype: 'button',
										text: 'Cari',
										iconCls: 'refresh',
										tooltip: 'Cari',
										style:{paddingLeft:'30px'},
										width:150,
										handler: function(){					
											DfltFilterBtn_viReturBHP = 1;
											Q(returBHP.Grid.main).refresh();
										}                        
									}
								]
							}
						]
					}
				]
			}
				
			]
		},
		$this.Grid.main
		]
   });
   return FrmFilterGridDataView_viReturBHP;
}

function setLookUp_viReturBHP(rowdata){
    var lebar = 985;
    var $this=returBHP;
    setLookUps_viReturBHP = new Ext.Window({
        title: NamaForm_viReturBHP, 
        closeAction: 'destroy',        
        width: 900,
        height: 500,
        resizable:false,
        layout:{
        	type:'vbox',
        	align:'stretch'
        },
		autoScroll: false,
        border: false,
        constrain : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viReturBHP(lebar,rowdata), //1
        listeners:{
            activate: function(a){
				// $this.vars.shortcut=true;
				// $this.setShortcut();	
				$this.refreshInput();
				shortcuts();
            },
            afterShow: function(){
                this.activate();
            },
            hide:function(){
            	// $this.vars.shortcut=false;
            	$this.ArrayStore.gridDetail.loadData([],false)
            },
            deactivate: function(){
                rowSelected_viReturBHP=undefined;
				mNoKunjungan_viReturBHP = '';
				shortcut.remove('lookup');
            },
			close: function (){
				shortcut.remove('lookup');
			},
        }
    });
    loadMask.show();
    if (rowdata == undefined){
		setLookUps_viReturBHP.show();
		$this.refreshInput();
		loadMask.hide();
        /* $.ajax({
			url:baseURL + "index.php/gudang_farmasi/functionGFReturBHP/initTransaksi",
			dataType:'JSON',
			type: 'GET',
			success: function(r){
				
				if(r.processResult=='SUCCESS'){
					
				}else{
					Ext.Msg.alert('Error',r.processMessage);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		}); */
    }else{
        $.ajax({
 			url:baseURL + "index.php/bhp/functionReturBHP/getForEdit",
 			dataType:'JSON',
 			type: 'POST',
 			data: {'ret_number':rowdata.ret_number},
 			success: function(r){
 				loadMask.hide();
 				if(r.processResult=='SUCCESS'){
 					setLookUps_viReturBHP.show();
 					var o=r.resultObject;
 					$this.TextField.noRetur.setValue(o.ret_number);
 					var date = Date.parseDate(o.ret_date, "Y-m-d H:i:s");
 					$this.DateField.date.setValue(date);
					Ext.getCmp('cboBHP_viReturBHP').setValue(o.kd_vendor);
					//alert(Ext.getCmp('cboBHP_viReturBHP').getValue());
 					Ext.getCmp('cboBHP_viReturBHP').setValue(o.vendor);
 					$this.TextField.remark.setValue(o.remark);
 					
 					if(r.resultObject.ret_post==1){
 						StatusPostingGFReturBHP='1';
						Ext.getCmp('btnSaveGFReturBHP').disable();
						Ext.getCmp('btnDeleteGFReturBHP').disable();
						Ext.getCmp('btnPostingGFReturBHP').disable();
						Ext.getCmp('btnAddDetailGFReturBHP').disable();
						Ext.getCmp('btnUnPostingGFReturBHP').enable();
						Ext.getCmp('btnCetakBuktiGFReturBHP').enable();
					}else{
						StatusPostingGFReturBHP='0';
						Ext.getCmp('btnSaveGFReturBHP').enable();
						Ext.getCmp('btnDeleteGFReturBHP').enable();
						Ext.getCmp('btnPostingGFReturBHP').enable();
						Ext.getCmp('btnAddDetailGFReturBHP').enable();
						Ext.getCmp('btnUnPostingGFReturBHP').disable();
						Ext.getCmp('btnCetakBuktiGFReturBHP').disable();
 					}
 					$this.ArrayStore.gridDetail.loadData([],false);
 					for(var i=0, iLen=r.listData.length ; i<iLen ; i++){
 						$this.ArrayStore.gridDetail.add(new $this.ArrayStore.gridDetail.recordType(r.listData[i]));
 					}
 					$this.refreshInput();
 				}else{
 					Ext.Msg.alert('Gagal',r.processMessage);
 				}
 			},
 			error: function(jqXHR, exception) {
 				loadMask.hide();
 				Nci.ajax.ErrorMessage(jqXHR, exception);
 			}
 		});
    }
	// shortcuts();
}

function shortcuts(){
	shortcut.set({
		code:'lookup',
		list:[
			{
				key:'ctrl+s',
				fn:function(){
					Ext.getCmp('btnSaveGFReturBHP').el.dom.click();
				}
			},
			{
				key:'ctrl+d',
				fn:function(){
					Ext.getCmp('btnDeleteGFReturBHP').el.dom.click();
				}
			},{
				key:'f4',
				fn:function(){
					Ext.getCmp('btnPostingGFReturBHP').el.dom.click();
				}
			},{
				key:'f6',
				fn:function(){
					Ext.getCmp('btnUnPostingGFReturBHP').el.dom.click();
				}
			},
			{
				key:'f12',
				fn:function(){
					Ext.getCmp('btnCetakBuktiGFReturBHP').el.dom.click();
				}
			},
			{
				key:'esc',
				fn:function(){
					setLookUps_viReturBHP.close();
				}
			}
		]
	});
}

function getFormItemEntry_viReturBHP(lebar,rowdata){
	var $this=returBHP;
    var pnlFormDataBasic_viReturBHP = new Ext.FormPanel({
		title: '',
		bodyStyle: 'padding:5px 5px 5px 5px',
		width: lebar,
		layout:{
			type:'vbox',
			align:'stretch'
		},
		flex:1,
		items:[
			getItemPanelInputBiodata_viReturBHP(lebar),
			getItemGridTransaksi_viReturBHP(lebar),
			new Ext.Panel({
				height:50,
				layout:{
					type:'hbox'
				},
				border:false,
				items:[
					new Ext.Panel({
						flex:1,
						layout:'form',
						border:false,
						items:[
							{
								layout:'column',
								border:false,
								items:[
									$this.DisplayField.posting=new Ext.form.DisplayField({
										value		: '<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>'
									}),{
										xtype:'displayfield',
										style:{'text-align':'right','margin-top':'2px'},
										value:'Posted'
									}
								]
							}
						]
					}),
					new Ext.Panel({
						width: 320,
						flex: 1,
						layout:'form',
						border:false,
						items:[
							{
								layout:'column',
								border:false,
								items:[
									{
										xtype: 'displayfield',				
										width: 80,								
										value: 'Reduksi : ',
										style:{'text-align':'right','margin-top':'5px'},
										fieldLabel: 'Label'
									},$this.TextField.reduksi=new Ext.form.TextField({
					                    xtype: 'textfield',
					                    width: 80,
					                    value: 0,
					                    disabled:true,
					                    style:{'padding':'2px','text-align':'right'}
					                }),
									{
										xtype: 'displayfield',				
										width: 80,								
										value: 'Sub Total : ',
										style:{'text-align':'right','margin-top':'5px'},
										fieldLabel: 'Label'
									},$this.TextField.subtotal=new Ext.form.TextField({
					                    style:{'padding':'2px','text-align':'right'},
					                    width: 80,
					                    value: 0,
					                    disabled:true
					                })
								]
							},{
								layout:'column',
								style:{'margin-top': '3px','text-align':'right'},
								border:false,
								items:[
									{
										xtype: 'displayfield',				
										width: 80,								
										value: 'PPN : ',
										style:{'text-align':'right','margin-top':'5px'},
										fieldLabel: 'Label'
									},$this.TextField.ppn=new Ext.form.TextField({
					                    xtype: 'textfield',
					                    width: 80,
					                    value: 0,
					                    disabled:true,
					                    style:{'padding':'2px','text-align':'right'}
					                }),
									{
										xtype: 'displayfield',				
										width: 80,								
										value: '<b>Total</b> : ',
										fieldLabel: 'Label',
										style:{'text-align':'right','margin-top':'5px'}
									},$this.TextField.total=new Ext.form.TextField({
					                    xtype: 'textfield',
										style:{'padding':'2px','text-align':'right'},
					                    width: 80,
					                    value: 0,
					                    disabled:true
					                })
								]
							}
						]
					})
				]
			})
		],
		fileUpload: true,
		tbar: {
			xtype: 'toolbar',
			items: 
			[	
				$this.Button.save1=new Ext.Button({
					text: 'Save',
					iconCls: 'save [CTRL+S]',
					id: 'btnSaveGFReturBHP',
					handler: function(){
						$this.save();
					}
				}),{
					xtype: 'tbseparator'
				},$this.Button.save2=new Ext.Button({
					text: 'Save & Close',
					iconCls: 'saveexit',
					hidden:true,
					handler: function(){
						$this.save(function(){
							setLookUps_viReturBHP.close();
						});
					}
				}),{
					xtype: 'tbseparator',
					hidden:true,
				},$this.Button.posting=new Ext.Button({
					text: 'Posting [F4]',
					iconCls: 'gantidok',
					id: 'btnPostingGFReturBHP',
					handler: function(){
						$this.posting();
					}
				}),{
					xtype: 'tbseparator'
				},
				$this.Button.unposting=new Ext.Button({
					text: 'Unposting [F6]',
					iconCls: 'reuse',
					id: 'btnUnPostingGFReturBHP',
					handler: function(){
						$this.unposting();
					}
				}),{
					xtype: 'tbseparator'
				},
				$this.Button.cetak=new Ext.Button({
					text: 'Print [F12]',
					iconCls: 'print',
					id: 'btnCetakBuktiGFReturBHP',
					handler: function(){
						$this.cetak();
					}
				}),{
					xtype: 'tbseparator'
				}
			]
		}
	});
    return pnlFormDataBasic_viReturBHP;
}

function getItemPanelInputBiodata_viReturBHP(lebar){
    var $this=returBHP;
    var items ={
	    layout: 'Form',
	    xtype:'fieldset',
	    style:'margin-bottom:-1px;',
	    autoHeight:true,
		items:[	
			{
				layout:'column',
				border:false,
				items:[
					Q().input({
						label:'No. Retur',
						width: 250,
						items:[
							$this.TextField.noRetur=new Ext.form.TextField({
								width : 120,
								disabled:true,
								emptyText: 'No Retur'
							})
						]
					}),
					{ 
						xtype: 'tbtext', 
						text: 'Vendor', 
						width: 100,
						height: 25
					},
					{ 
						xtype: 'tbtext', 
						text: ': ', 
						width: 10,
						height: 25
					},
					viCombo_Vendor(415, 'cboBHP_viReturBHP')	
				]
			},{
				layout:'column',
				border:false,
				items:[
					Q().input({
						label:'Tanggal',
						width: 250,
						items:[
							$this.DateField.date=new Ext.form.DateField({
								
								// disabled:true,
								value: now_viReturBHP,
								format: 'd/M/Y',
								width: 120
							})
						]
					}),
					Q().input({
						label:'Remark',
						width: 500,
						items:[
							$this.TextField.remark=new Ext.form.TextField({
								xtype: 'textfield',
								width : 360,	
								emptyText: 'Remark',
								listeners:{
									'specialkey': function(me, e){
										$this.vars.lineDetail=0;
										if (Ext.EventObject.getKey() === 13){
											if($this.ArrayStore.gridDetail.getCount()==0){
												$this.addDetail();
												$this.vars.lineDetail=$this.ArrayStore.gridDetail.getCount()-1;
												$this.Grid.detail.startEditing($this.ArrayStore.gridDetail.getCount()-1,3);
											}else{
												$this.Grid.detail.startEditing(0,3);
												$this.vars.lineDetail=0;
											}
										}
										if (e.getKey() == e.TAB) {
											if($this.ArrayStore.gridDetail.getCount()==0){
												$this.addDetail();
												$this.vars.lineDetail=$this.ArrayStore.gridDetail.getCount()-1;
												$this.Grid.detail.startEditing($this.ArrayStore.gridDetail.getCount()-1,3);
											}else{
												$this.Grid.detail.startEditing(0,3);
												$this.vars.lineDetail=0;
											}
					                    }
									}
								}
							})
						]
					})
				]
			}
		]
	};
    return items;
};

function getItemGridTransaksi_viReturBHP(lebar) {
    var items ={
	    layout: 'fit',
	    anchor: '100%',	    
		border:false,
		style:{'padding-bottom':'10px'},
		flex: 1,
		width: lebar-80,
	    items:gridDataViewEdit_viReturBHP()
	};
    return items;
};

function gridDataViewEdit_viReturBHP(){
	var $this=returBHP;
	$this.Grid.detail=new Ext.grid.EditorGridPanel({
        store: $this.ArrayStore.gridDetail,
        flex:1,
        style:'margin-top:-1px;',
        stripeRows:true,
        columnLines:true,
		selModel: new Ext.grid.CellSelectionModel({
            singleSelect: true,
            listeners:{
                cellselect: function(sm, row, rec){
					// console.log(row)
                	// $this.vars.lineDetail=row;
                }
            }
        }),
        tbar:[
    		$this.Button.addDetail=new Ext.Button({
				text: 'Add',
				iconCls: 'add',
				tooltip: 'Edit Data',
				id:'btnAddDetailGFReturBHP',
				handler: function(sm, row, rec){
					StatusPostingGFReturBHP='0';
					$this.addDetail();
					$this.vars.lineDetail=$this.ArrayStore.gridDetail.getCount()-1;
					$this.Grid.detail.startEditing($this.ArrayStore.gridDetail.getCount()-1,3);
				}
			}),{
				xtype: 'tbseparator'
			},$this.Button.deleted=new Ext.Button({
				text: 'Delete Item',
				iconCls: 'remove',
				id:'btnDeleteGFReturBHP',
				handler: function(){
					$this.delDetail();
				}
			}),{
				xtype: 'tbseparator'
			}
        ],
        cm: new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(),
			{			
				dataIndex: 'kd_milik',
				header: 'M',
				sortable: false,
				menuDisabled: true,
				hidden:true,
				width: 20
			},{			
				dataIndex: 'no_baris',
				header: 'no baris',
				sortable: false,
				menuDisabled: true,
				hidden:true,
				width: 20
			},
			{			
				dataIndex: 'po_number',
				header: 'No PO',
				sortable: false,
				menuDisabled: true,
				width: 60,
				fixed: true,
				editor: new Ext.form.TextField({
					allowBlank: false,
					enableKeyEvents:true,
					listeners:{
						keyDown: function(a,b,c){
							if(b.getKey()==13){
								var line	= $this.Grid.detail.getSelectionModel().selection.cell[0];
								// if(a.getValue().length < 3){
								if(a.getValue().length < 1){
									// if(a.getValue().length != 0){
										Ext.Msg.show({
											title: 'Perhatian',
											msg: 'Kriteria huruf pencarian barang minimal 1 huruf!',
											buttons: Ext.MessageBox.OK,
											fn: function (btn) {
												if (btn == 'ok')
												{
													$this.Grid.detail.startEditing(line, 3);
												}
											}
										});
									
								} else{		
									PencarianLookupGFReturBHP = true;// Variabel pembeda getdata u/nama barang dan semua kepemilikan atau bukan. False => pencarian berdasarkan nama_barang dan kepemilikan, True => pencarian berdasarkan nama_barang saja
									FocusExitGFReturBHP = false;
									LookUpSearchListGetbarang_GFReturBHP(a.getValue());
								}
							}
						}
					}
				})
			},
			{			
				dataIndex: 'no_terima',
				header: 'No Terima',
				sortable: false,
				menuDisabled: true,
				width: 70
			},{			
				dataIndex: 'kd_prd',
				header: 'Kode',
				sortable: false,
				menuDisabled: true,
				width: 20
			},
			{			
				dataIndex: 'nama_barang',
				header: 'Uraian',
				sortable: true,
				// width: 250,
				menuDisabled: true,
				fixed: true,
				/* editor: new Ext.form.TextField({
					allowBlank: false,
					enableKeyEvents:true,
					listeners:{
						keyDown: function(a,b,c){
							if(b.getKey()==13){
								var line	= $this.Grid.detail.getSelectionModel().selection.cell[0];
								// if(a.getValue().length < 3){
								if(a.getValue().length < 1){
									// if(a.getValue().length != 0){
										Ext.Msg.show({
											title: 'Perhatian',
											msg: 'Kriteria huruf pencarian barang minimal 1 huruf!',
											buttons: Ext.MessageBox.OK,
											fn: function (btn) {
												if (btn == 'ok')
												{
													$this.Grid.detail.startEditing(line, 3);
												}
											}
										});
									
								} else{		
									PencarianLookupGFReturBHP = true;// Variabel pembeda getdata u/nama barang dan semua kepemilikan atau bukan. False => pencarian berdasarkan nama_barang dan kepemilikan, True => pencarian berdasarkan nama_barang saja
									FocusExitGFReturBHP = false;
									LookUpSearchListGetbarang_GFReturBHP(a.getValue());
								}
							}
						}
					}
				}) */
			},
			/* {			
				dataIndex: 'nama_barang',
				header: 'Uraian',
				sortable: false,
				menuDisabled: true,
				flex: 1,
				editor:new Nci.form.Combobox.autoComplete({
					store	: returBHP.ArrayStore.combobarang,
					select	: function(a,b,c){
						var line	= $this.vars.lineDetail;
						if($this.ArrayStore.gridDetail.getCount()-1===0)
						{
							$this.ArrayStore.gridDetail.getRange()[line].data.nama_barang=b.data.nama_barang;
							$this.ArrayStore.gridDetail.getRange()[line].data.kd_milik=b.data.kd_milik;
							$this.ArrayStore.gridDetail.getRange()[line].data.po_number=b.data.po_number;
							$this.ArrayStore.gridDetail.getRange()[line].data.kd_prd=b.data.kd_prd;
							$this.ArrayStore.gridDetail.getRange()[line].data.satuan=b.data.satuan;
							$this.ArrayStore.gridDetail.getRange()[line].data.harga_beli=b.data.harga_beli;
							$this.ArrayStore.gridDetail.getRange()[line].data.jumlah_in=b.data.jumlah_in;
							$this.ArrayStore.gridDetail.getRange()[line].data.batch=b.data.batch;
							$this.ArrayStore.gridDetail.getRange()[line].data.tgl_exp=b.data.tgl_exp;
							$this.ArrayStore.gridDetail.getRange()[line].data.min_stok=b.data.min_stok;
							$this.ArrayStore.gridDetail.getRange()[line].data.ppn_item=b.data.ppn_item;
							$this.ArrayStore.gridDetail.getRange()[line].data.rcv_line=b.data.rcv_line;
							$this.refreshInput();
						}else{
							var VALUE_x=1;
							for(var i = 0 ; i < $this.ArrayStore.gridDetail.getCount();i++)
							{
								if($this.ArrayStore.gridDetail.getRange()[i].data.kd_prd===b.data.kd_prd)
								{
									VALUE_x=0;
								}
							}
							if(VALUE_x===0){
								Ext.Msg.alert('Perhatian','Anda telah memilih barang yang sama');
								$this.ArrayStore.gridDetail.removeAt(line);
							}else{
								$this.ArrayStore.gridDetail.getRange()[line].data.nama_barang=b.data.nama_barang;
								$this.ArrayStore.gridDetail.getRange()[line].data.kd_milik=b.data.kd_milik;
								$this.ArrayStore.gridDetail.getRange()[line].data.po_number=b.data.po_number;
								$this.ArrayStore.gridDetail.getRange()[line].data.kd_prd=b.data.kd_prd;
								$this.ArrayStore.gridDetail.getRange()[line].data.satuan=b.data.satuan;
								$this.ArrayStore.gridDetail.getRange()[line].data.harga_beli=b.data.harga_beli;
								$this.ArrayStore.gridDetail.getRange()[line].data.jumlah_in=b.data.jumlah_in;
								$this.ArrayStore.gridDetail.getRange()[line].data.batch=b.data.batch;
								$this.ArrayStore.gridDetail.getRange()[line].data.tgl_exp=b.data.tgl_exp;
								$this.ArrayStore.gridDetail.getRange()[line].data.min_stok=b.data.min_stok;
								$this.ArrayStore.gridDetail.getRange()[line].data.ppn_item=b.data.ppn_item;
								$this.ArrayStore.gridDetail.getRange()[line].data.rcv_line=b.data.rcv_line;
								$this.refreshInput();
							}
						}
						setTimeout(
						function(){ 
							$this.Grid.detail.startEditing($this.vars.lineDetail,7);
						}, 200);
					},
					insert	: function(o){
						return {
							kd_prd        	:o.kd_prd,
							nama_barang 		: o.nama_barang,
							satuan			: o.satuan,
							harga_beli		: o.harga_beli,
							jumlah_in		: o.jumlah_in,
							po_number		: o.po_number,
							batch			: o.batch,
							tgl_exp			: o.tgl_exp,
							milik			: o.milik,
							jml_stok_apt	: o.jml_stok_apt,
							ppn_item		: o.ppn_item,
							rcv_line		: o.rcv_line,
							text			:  '<table style="font-size: 11px;"><tr><td width="60">'+o.kd_prd+'</td><td width="270">'+o.nama_barang+'</td><td width="170">'+o.po_number+'</td><td width="60">'+o.batch+'</td><td width="50">'+o.jml_stok_apt+'</td><td width="50">'+o.milik+'</td></tr></table>'
						}
					},
					param:function(){
						return {BHP:Ext.getCmp('cboBHP_viReturBHP').getValue()}
					},
					url		: baseURL + "index.php/gudang_farmasi/functionGFReturBHP/getbarang",
					valueField: 'nama_barang',
					displayField: 'text',
					listWidth: 450,
					blur: function(){
					}
				})
			}, */{
				dataIndex: 'satuan',
				header: 'Sat K',
				sortable: false,
				hidden : true,
				menuDisabled: true,
				width: 40
			},{
				dataIndex: 'harga_beli',
				header: 'Harga',
				xtype:'numbercolumn',
				align:'right',
				sortable: false,
				menuDisabled: true,
				width: 40
			},{
				dataIndex: 'jumlah_in',
				header: 'Stock',
				xtype:'numbercolumn',
				align:'right',
				sortable: false,
				menuDisabled: true,
				width: 40
			},{
				dataIndex: 'qty',
				xtype:'numbercolumn',
				header: 'Qty',
				sortable: false,
				menuDisabled: true,
				align:'right',
				width: 30,
				editor:new Ext.form.NumberField({
					enableKeyEvents:true,
					listeners:{
						keyDown: function(a,b,c){
							if(b.getKey()==13){
								var line	= $this.Grid.detail.getSelectionModel().selection.cell[0];
								var o = $this.ArrayStore.gridDetail.getRange()[this.indeks].data;
								if (parseFloat(a.getValue()) > parseFloat(o.jumlah_in))
								{
									Ext.Msg.show({
										title: 'Perhatian',
										msg: 'Qty melebihi jumlah penerimaannya!',
										buttons: Ext.MessageBox.OK,
										fn: function (btn) {
											if (btn == 'ok')
											{
												o.qty=o.jumlah_in;
												// 
												$this.Grid.detail.startEditing(line,10);
											}
										}
									});
									
								} else {
									$this.ArrayStore.gridDetail.getRange()[this.indeks].data.qty=a.getValue();
									$this.refreshInput();
									var row=this.indeks;
									$this.Grid.detail.startEditing(row,11);
								}
								GridListPencarianbarang_GFReturBHP.getView().refresh();
							}
						},
						focus:function(){
							this.indeks=$this.vars.lineDetail;
						},
						specialkey: function(){
							if(Ext.EventObject.getKey() == 13){
								// var records = new Array();
								// records.push(new $this.ArrayStore.gridDetail.recordType());
								// $this.ArrayStore.gridDetail.add(records);
								// $this.Grid.detail.startEditing($this.ArrayStore.gridDetail.getCount()-1,2);
								// var line=$this.Grid.detail.getSelectionModel().selection.cell[0];
								/* $this.vars.lineDetail=$this.ArrayStore.gridDetail.getCount()-1;
								$this.Grid.detail.startEditing($this.vars.lineDetail,8); */
							}
						}
					}
				})	
			},
			{
				dataIndex: 'ret_reduksi',
				xtype:'numbercolumn',
				header: 'Reduksi',
				sortable: false,
				menuDisabled: true,
				align:'right',
				width: 30,
				editor:new Ext.form.NumberField({
					enableKeyEvents:true,
					listeners:{
						keyDown: function(a,b,c){
							if(b.getKey()==13){
								var row=this.indeks;
								var o = $this.ArrayStore.gridDetail.getRange()[row].data;
								$this.refreshInput();
								o.jumlah=parseFloat(o.jumlah)-parseFloat(o.ret_reduksi);
								$this.Grid.detail.getView().refresh();
								var row=this.indeks + 1;
								$this.Grid.detail.startEditing(row,3);
							}
						},
						focus:function(){
							this.indeks=$this.vars.lineDetail;
						},
						specialkey: function(){
							if(Ext.EventObject.getKey() == 13){
								var records = new Array();
								records.push(new $this.ArrayStore.gridDetail.recordType());
								$this.ArrayStore.gridDetail.add(records);
								$this.vars.lineDetail=$this.ArrayStore.gridDetail.getCount()-1;
								$this.Grid.detail.startEditing($this.ArrayStore.gridDetail.getCount()-1,3);
								// var line=$this.Grid.detail.getSelectionModel().selection.cell[0];
								$this.Grid.detail.startEditing($this.vars.lineDetail,3);
							}
						}
					}
				})	
			},/* {
				dataIndex: 'jumlah',
				header: 'Jumlah',
				xtype:'numbercolumn',
				sortable: false,
				menuDisabled: true,
				align:'right',
				width: 50
			},{
				dataIndex: 'tgl_exp',
				header: 'Expired',
				format: 'd/m/Y',
				width: 50,
				menuDisabled: true,
				renderer: function(v, params, record){
					return ShowDate(record.data.tgl_exp);
				}
			},{
				dataIndex: 'batch',
				header: 'No. Batch',
				sortable: false,
				menuDisabled: true,
				width: 40
			},{
				dataIndex: 'ppn_item',
				header: 'PPN%',
				align:'right',
				sortable: false,
				menuDisabled: true,
				width: 40
			}, */
			{
				xtype:'numbercolumn',
				dataIndex: 'min_stok',
				header: 'Min Stok',
				align: 'right',
				//hidden :true,
				sortable: true,
				width: 30	
			},
			{
				dataIndex: 'ret_line',
				header: 'ret_line',
				align: 'right',
				hidden :true,
				sortable: true,
				width: 20	
			},
			{
				dataIndex: 'rcv_line',
				header: 'rcv_line',
				align: 'right',
				hidden :true,
				sortable: true,
				width: 100	
			}
        ]),
        viewConfig:{
        	forceFit:true
        }
    });   
    return $this.Grid.detail;
}
function viCombo_Vendor(lebar,Nama_ID){
	var $this=returBHP;
	var b=new WebApp.DataStore({fields: ['kd_vendor', 'vendor']});
	b.load({
        params:{
            Skip: 0,
            Take: 1000,
            Sort: 'kd_vendor',
            Sortdir: 'ASC',
            target: 'ComboVendorBHP',
            param: ""
        }
    });
    var a = new Ext.form.ComboBox ({
        fieldLabel: 'Vendor',
		valueField: 'kd_vendor',
        displayField: 'vendor',
		emptyText:'BHP',
		store: b,
        width: 200,
        mode: 'local',
        typeAhead: true,
        triggerAction: 'all',                        
        name: Nama_ID,
        lazyRender: true,
        id: Nama_ID,
		listeners:{ 
			select:function(a){
				if(a.id=='cboBHP_viReturBHP'){
					$this.ArrayStore.gridDetail.loadData([],false);
					$this.refreshInput();
				}
			},
			'specialkey' : function()
			{
				if (Ext.EventObject.getKey() === 13) 
				{
					if(a.id=='cboBHP_viReturBHP'){
						$this.TextField.remark.focus();
					}
				} 						
			}
			
			
		}
    });    
    return a;
}

function getDetailListbarang(listData,totalLisData){
	var $this=returBHP;
	for(var i=0, iLen=totalLisData; i<iLen; i++){
		$this.ArrayStore.gridDetail.add(new $this.ArrayStore.gridDetail.recordType(listData[i]));
	}
}


function LookUpSearchListGetbarang_GFReturBHP(po_number){
	var $this=returBHP;
		WindowLookUpSearchListGetbarang_GFReturBHP = new Ext.Window
		(
			{
				id: 'pnlLookUpSearchListGetbarang_GFReturBHP',
				title: 'List Pencarian barang',
				width:700,
				height: 250,
				border: false,
				resizable:false,
				plain: true,
				iconCls: 'icon_lapor',
				modal: true,
				items: [
					gridListbarangPencarianGFReturBHP()
				],
				listeners:
				{             
					activate: function()
					{
							
					},
					afterShow: function()
					{
						this.activate();

					},
					deactivate: function()
					{
						
					},
					close: function (){
						if(FocusExitGFReturBHP == false){
							var line	= $this.vars.lineDetail;
							$this.Grid.detail.startEditing(line, 3);	
						}
					}
				}
			}
		);

		WindowLookUpSearchListGetbarang_GFReturBHP.show();
		getListbarangSearch_GFReturBHP(po_number);
	}
	function gridListbarangPencarianGFReturBHP(){
		var $this=returBHP;
		var fldDetail = [];
		dsGridListPencarianbarang_GFReturBHP = new WebApp.DataStore({ fields: fldDetail });
		GridListPencarianbarangColumnModel =  new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(),
			{
				dataIndex		: 'po_number',
				header			: 'No. PO',
				width			: 100,
				menuDisabled	: true,
			},
			{
				dataIndex		: 'no_baris',
				header			: 'No Baris',
				width			: 70,
				hidden			: true,
				menuDisabled	: true,
			},{
				dataIndex		: 'no_terima',
				header			: 'No Terima',
				width			: 70,
				menuDisabled	: true,
			},{
				dataIndex		: 'no_urut_brg',
				header			: 'Kode',
				width			: 70,
				menuDisabled	: true,
			},
			{
				dataIndex		: 'nama_brg',
				header			: 'Nama barang',
				width			: 180,
				menuDisabled	: true,
			},
			{
				dataIndex		: 'jumlah_in',
				header			: 'Jml Terima',
				width			: 70,
				align			: 'right',
				menuDisabled	: true,
			},
			{
				dataIndex		: 'min_stok',
				header			: 'Stok',
				width			: 45,
				align			: 'right',
				menuDisabled	: true,
			},{
				dataIndex		: 'harga_beli',
				header			: 'Harga',
				width			: 45,
				align			: 'right',
				menuDisabled	: true,
			},
			{
				dataIndex		: 'milik',
				header			: 'Kepemilikan',
				width			: 80,
				menuDisabled	: true,
				hidden 			: true
			}
		]);
		
		
		GridListPencarianbarang_GFReturBHP= new Ext.grid.EditorGridPanel({
			id			: 'GrdListPencarianbarang_GFReturBHP',
			stripeRows	: true,
			width		: 700,
			height		: 220,
			store		: dsGridListPencarianbarang_GFReturBHP,
			border		: true,
			frame		: false,
			autoScroll	: true,
			cm			: GridListPencarianbarangColumnModel,
			selModel: new Ext.grid.RowSelectionModel({
				singleSelect: true,
				listeners:
				{
					rowselect: function(sm, row, rec)
					{
						currentRowSelectionPencarianbarangGFReturBHP = undefined;
						currentRowSelectionPencarianbarangGFReturBHP = dsGridListPencarianbarang_GFReturBHP.getAt(row);
					}
				}
			}),
			listeners	: {
				rowclick: function( $this, rowIndex, e )
				{
					// trcellCurrentTindakan_KasirRWJ = rowIndex;
				},
				celldblclick: function(gridView,htmlElement,columnIndex,dataRecord){
					
				},
				keydown : function(e){
					if(e.getKey() == 13){
						var line	= $this.Grid.detail.getSelectionModel().selection.cell[0];
						if($this.ArrayStore.gridDetail.getCount()-1===0)
						{
							$this.ArrayStore.gridDetail.getRange()[line].data.nama_barang=currentRowSelectionPencarianbarangGFReturBHP.data.nama_brg;
							$this.ArrayStore.gridDetail.getRange()[line].data.no_terima=currentRowSelectionPencarianbarangGFReturBHP.data.no_terima;
							$this.ArrayStore.gridDetail.getRange()[line].data.no_baris=currentRowSelectionPencarianbarangGFReturBHP.data.no_baris;
							$this.ArrayStore.gridDetail.getRange()[line].data.po_number=currentRowSelectionPencarianbarangGFReturBHP.data.po_number;
							$this.ArrayStore.gridDetail.getRange()[line].data.kd_prd=currentRowSelectionPencarianbarangGFReturBHP.data.no_urut_brg;
							$this.ArrayStore.gridDetail.getRange()[line].data.satuan=currentRowSelectionPencarianbarangGFReturBHP.data.satuan;
							$this.ArrayStore.gridDetail.getRange()[line].data.harga_beli=currentRowSelectionPencarianbarangGFReturBHP.data.harga_beli;
							$this.ArrayStore.gridDetail.getRange()[line].data.jumlah_in=currentRowSelectionPencarianbarangGFReturBHP.data.jumlah_in;
							//$this.ArrayStore.gridDetail.getRange()[line].data.batch=currentRowSelectionPencarianbarangGFReturBHP.data.batch;
							//$this.ArrayStore.gridDetail.getRange()[line].data.tgl_exp=currentRowSelectionPencarianbarangGFReturBHP.data.tgl_exp;
							$this.ArrayStore.gridDetail.getRange()[line].data.min_stok=currentRowSelectionPencarianbarangGFReturBHP.data.min_stok;
							//$this.ArrayStore.gridDetail.getRange()[line].data.ppn_item=currentRowSelectionPencarianbarangGFReturBHP.data.ppn_item;
							//$this.ArrayStore.gridDetail.getRange()[line].data.rcv_line=currentRowSelectionPencarianbarangGFReturBHP.data.rcv_line;
							$this.refreshInput();
							FocusExitGFReturBHP = true;
							WindowLookUpSearchListGetbarang_GFReturBHP.close();
							$this.Grid.detail.startEditing(line,10);
						}else{
							var VALUE_x=1;
							for(var i = 0 ; i < $this.ArrayStore.gridDetail.getCount();i++)
							{
								if($this.ArrayStore.gridDetail.getRange()[i].data.kd_prd===currentRowSelectionPencarianbarangGFReturBHP.data.no_urut_brg)
								{
									VALUE_x=0;
								}
							}
							if(VALUE_x===0){
								Ext.Msg.show({
									title: 'Perhatian',
									msg: 'Anda telah memilih barang yang sama!',
									buttons: Ext.MessageBox.OK,
									fn: function (btn) {
										if (btn == 'ok')
										{
											GridListPencarianbarang_GFReturBHP.getView().refresh();
											GridListPencarianbarang_GFReturBHP.getSelectionModel().selectRow(0);
											GridListPencarianbarang_GFReturBHP.getView().focusRow(0);
										}
									}
								});
								
							}else{
								$this.ArrayStore.gridDetail.getRange()[line].data.nama_barang=currentRowSelectionPencarianbarangGFReturBHP.data.nama_brg;
								$this.ArrayStore.gridDetail.getRange()[line].data.no_terima=currentRowSelectionPencarianbarangGFReturBHP.data.no_terima;
								$this.ArrayStore.gridDetail.getRange()[line].data.no_baris=currentRowSelectionPencarianbarangGFReturBHP.data.no_baris;
								$this.ArrayStore.gridDetail.getRange()[line].data.po_number=currentRowSelectionPencarianbarangGFReturBHP.data.po_number;
								$this.ArrayStore.gridDetail.getRange()[line].data.kd_prd=currentRowSelectionPencarianbarangGFReturBHP.data.no_urut_brg;
								$this.ArrayStore.gridDetail.getRange()[line].data.satuan=currentRowSelectionPencarianbarangGFReturBHP.data.satuan;
								$this.ArrayStore.gridDetail.getRange()[line].data.harga_beli=currentRowSelectionPencarianbarangGFReturBHP.data.harga_beli;
								$this.ArrayStore.gridDetail.getRange()[line].data.jumlah_in=currentRowSelectionPencarianbarangGFReturBHP.data.jumlah_in;
								$this.ArrayStore.gridDetail.getRange()[line].data.min_stok=currentRowSelectionPencarianbarangGFReturBHP.data.min_stok;
								
								$this.refreshInput();
								FocusExitGFReturBHP = true;
								WindowLookUpSearchListGetbarang_GFReturBHP.close();
								$this.Grid.detail.startEditing(line,10);
							}
						}
					}
				},
			},
			viewConfig	: {forceFit: true}
		});
		return GridListPencarianbarang_GFReturBHP;
	}
	function getListbarangSearch_GFReturBHP(po_number){
		var $this=returBHP;
		Ext.Ajax.request ({
			url: baseURL + "index.php/bhp/functionReturBHP/getListBarang",
			params: {
				po_number:po_number,
				bhp:Ext.getCmp('cboBHP_viReturBHP').getValue()
			},
			failure: function(o)
			{
				Ext.Msg.alert('Error','Error menampilkan pencarian barang. Hubungi Admin!');
			},	
			success: function(o) 
			{   
				dsGridListPencarianbarang_GFReturBHP.removeAll();
				var cst = Ext.decode(o.responseText);

				if (cst.success === true) {
					if(cst.totalrecords == 0 ){
						if(PencarianLookupGFReturBHP == true){						
							Ext.Msg.show({
								title: 'Information',
								msg: 'Tidak ada barang yang sesuai atau kriteria barang kurang!',
								buttons: Ext.MessageBox.OK,
								fn: function (btn) {
									if (btn == 'ok')
									{
										var line	= $this.Grid.detail.getSelectionModel().selection.cell[0];
										$this.Grid.detail.startEditing(line, 3);
										WindowLookUpSearchListGetbarang_GFReturBHP.close();
									}
								}
							});
						}
					} else{
						var recs=[],
							recType=dsGridListPencarianbarang_GFReturBHP.recordType;
						for(var i=0; i<cst.ListDataObj.length; i++){
							recs.push(new recType(cst.ListDataObj[i]));						
						}
						dsGridListPencarianbarang_GFReturBHP.add(recs);
						GridListPencarianbarang_GFReturBHP.getView().refresh();
						GridListPencarianbarang_GFReturBHP.getSelectionModel().selectRow(0);
						GridListPencarianbarang_GFReturBHP.getView().focusRow(0);
					}
					
				} else {
					Ext.Msg.alert('Error','Gagal membaca data list pencarian barang');
				};
			}
		});
	}

shortcut.set({
	code:'main',
	list:[
		{
			key:'f1',
			fn:function(){
				Ext.getCmp('btnTambahReturGFReturBHP').el.dom.click();
			}
		},
	]
});