// Data Source ExtJS # --------------

/**
*	Nama File 		: TRInventoriStokOpname.js
*	Menu 			: 
*	Model id 		: 
*	Keterangan 		: Stok Opname Inventory
*	Di buat tanggal : 21 Agustus 2014
*	Oleh 			: SDY_RI
*/

// Deklarasi Variabel pada Inventori Perencanaan # --------------

var dataSource_viInventoriStokOpname;
var selectCount_viInventoriStokOpname=50;
var NamaForm_viInventoriStokOpname="Stok Opname";
var mod_name_viInventoriStokOpname="viInventoriStokOpname";
var now_viInventoriStokOpname= new Date();
var addNew_viInventoriStokOpname;
var rowSelected_viInventoriStokOpname;
var setLookUps_viInventoriStokOpname;
var mNoKunjungan_viInventoriStokOpname='';

var CurrentData_viInventoriStokOpname =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viInventoriStokOpname(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

/**
*	Function : dataGrid_viInventoriStokOpname
*	
*	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
*/

function dataGrid_viInventoriStokOpname(mod_id_viInventoriStokOpname)
{	
    // Field kiriman dari Project Net.
    var FieldMaster_viInventoriStokOpname = 
	[
		 'NO_KUNJUNGAN', 'KD_KELOMPOK', 'KD_UNIT', 'KD_DOKTER', 'KD_CUSTOMER', 'KD_PASIEN', 
		 'TGL_KUNJUNGAN','JAM_KUNJUNGAN', 'TINGGI_BADAN', 'BERAT_BADAN', 'TEKANAN_DRH', 
		 'NADI','ALERGI', 'KELUHAN', 'RUJUK_RAD', 'RUJUK_LAB', 'TAHAP_PROSES', 'PASIEN_BARU',
		 'NAMA_UNIT','KELOMPOK', 'DOKTER', 'CUSTOMER', 'PS_BARU','KD_PENDIDIKAN','KD_STS_MARITAL', 
		 'KD_AGAMA','KD_PEKERJAAN','NAMA','TEMPAT_LAHIR','TGL_LAHIR','JENIS_KELAMIN','ALAMAT', 
		 'NO_TELP','NO_HP','GOL_DARAH','PENDIDIKAN','STS_MARITAL','AGAMA','PEKERJAAN','JNS_KELAMIN',
		 'TAHUN','BULAN','HARI'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viInventoriStokOpname = new WebApp.DataStore
	({
        fields: FieldMaster_viInventoriStokOpname
    });
    
    // Grid Inventori Perencanaan # --------------
	var GridDataView_viInventoriStokOpname = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viInventoriStokOpname,
			autoScroll: true,
			columnLines: true,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viInventoriStokOpname = undefined;
							rowSelected_viInventoriStokOpname = dataSource_viInventoriStokOpname.getAt(row);
							CurrentData_viInventoriStokOpname
							CurrentData_viInventoriStokOpname.row = row;
							CurrentData_viInventoriStokOpname.data = rowSelected_viInventoriStokOpname.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viInventoriStokOpname = dataSource_viInventoriStokOpname.getAt(ridx);
					if (rowSelected_viInventoriStokOpname != undefined)
					{
						setLookUp_viInventoriStokOpname(rowSelected_viInventoriStokOpname.data);
					}
					else
					{
						setLookUp_viInventoriStokOpname();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Inventori perencanaan
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colKdKelompok_viInventoriStokOpname',
						header: 'Kd. Kelompok',
						dataIndex: '',
						sortable: true,
						width: 35,
						filter:
						{
							type: 'int'
						}
					},
					//-------------- ## --------------
					{
						id: 'colTglRO_viInventoriStokOpname',
						header:'Tanggal',
						dataIndex: '',						
						width: 20,
						sortable: true,
						// format: 'd/M/Y',
						filter: {},
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.TGL_RO);
						}
					},
					//-------------- ## --------------
					{
						id: 'colKeterangan_viInventoriStokOpname',
						header: 'Barang',
						dataIndex: '',
						sortable: true,
						width: 60,
						filter:
						{}
					}
					//-------------- ## --------------
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viInventoriStokOpname',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viInventoriStokOpname',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viInventoriStokOpname != undefined)
							{
								setLookUp_viInventoriStokOpname(rowSelected_viInventoriStokOpname.data)
							}
							else
							{								
								setLookUp_viInventoriStokOpname();
							}
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			bbar : bbar_paging(mod_name_viInventoriStokOpname, selectCount_viInventoriStokOpname, dataSource_viInventoriStokOpname),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viInventoriStokOpname = new Ext.Panel
    (
		{
			title: NamaForm_viInventoriStokOpname,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viInventoriStokOpname,
			region: 'center',
			layout: 'form', 
			closable: true,        
			border: false,  
			margins: '0 5 5 0',
			items: [GridDataView_viInventoriStokOpname],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viInventoriStokOpname,
		            columns: 11,
		            defaults: {
		                scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
						//-------------- ## --------------
			            { 
							xtype: 'tbtext', 
							text: 'Kd. Kelompok : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},						
						{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_KdKelompok_viInventoriStokOpname',
							emptyText: 'Kd Kelompok',
							width: 100,
							height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										
									} 						
								}
							}
						},
						{	 
							xtype: 'tbspacer',
							width: 10,
							height: 25
						},						
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Tanggal : ', 
							style:{'text-align':'right'},
							width: 60,
							height: 25
						},
						{
							xtype: 'datefield',
							id: 'txtfilterTglAwal_viInventoriStokOpname',
							value: now_viInventoriStokOpname,
							format: 'd/M/Y',
							width: 120,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viInventoriStokOpname();								
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: ' s.d ', 
							style:{'text-align':'center'},
							width: 30,
							height: 25
						},																								
						//-------------- ## --------------
						{
							xtype: 'datefield',
							id: 'txtfilterTglKunjAkhir_viInventoriStokOpname',
							value: now_viInventoriStokOpname,
							format: 'd/M/Y',
							width: 120,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viInventoriStokOpname();								
									} 						
								}
							}
						},							
						{	 
							xtype: 'tbspacer',
							width: 10,
							height: 25
						},
						{ 
							xtype: 'tbtext', 
							text: 'Barang : ', 
							style:{'text-align':'right'},
							width: 45,
							height: 25
						},		
						{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_Barang_viInventoriStokOpname',
							emptyText: 'Barang',
							width: 130,
							height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										
									} 						
								}
							}
						},
						//-------------- ## --------------
						{
							xtype: 'button',
							text: 'Cari',
							iconCls: 'refresh',
							tooltip: 'Cari',
							style:{paddingLeft:'30px'},
							//rowspan: 3,
							width:150,
							id: 'BtnFilterGridDataView_viInventoriStokOpname',
							handler: function() 
							{					
								DfltFilterBtn_viInventoriStokOpname = 1;
								DataRefresh_viInventoriStokOpname(getCriteriaFilterGridDataView_viInventoriStokOpname());
							}                        
						},
						//-------------- ## --------------
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viInventoriStokOpname;
    //-------------- # End form filter # --------------
}
// End Function dataGrid_viInventoriStokOpname # --------------

/**
*	Function : setLookUp_viInventoriStokOpname
*	
*	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
*/

function setLookUp_viInventoriStokOpname(rowdata)
{
    var lebar = 985;
    setLookUps_viInventoriStokOpname = new Ext.Window
    (
    {
        id: 'SetLookUps_viInventoriStokOpname',
		name: 'SetLookUps_viInventoriStokOpname',
        title: NamaForm_viInventoriStokOpname, 
        closeAction: 'destroy',        
        width: 1000,
        height: 605,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viInventoriStokOpname(lebar,rowdata), //1
        listeners:
        {
            activate: function()
            {
				
            },
            afterShow: function()
            {
                this.activate();
            },
            deactivate: function()
            {
                rowSelected_viInventoriStokOpname=undefined;
                //datarefresh_viInventoriStokOpname();
				mNoKunjungan_viInventoriStokOpname = '';
            }
        }
    }
    );

    setLookUps_viInventoriStokOpname.show();
    if (rowdata == undefined)
    {
        // dataaddnew_viInventoriStokOpname();
		// Ext.getCmp('btnDelete_viInventoriStokOpname').disable();	
    }
    else
    {
        // datainit_viInventoriStokOpname(rowdata);
    }
}
// End Function setLookUpGridDataView_viInventoriStokOpname # --------------

/**
*	Function : getFormItemEntry_viInventoriStokOpname
*	
*	Sebuah fungsi untuk menampilkan penataan menu dan isian form edit
*/

function getFormItemEntry_viInventoriStokOpname(lebar,rowdata)
{
    var pnlFormDataBasic_viInventoriStokOpname = new Ext.FormPanel
    (
		{
			title: '',
			region: 'north',
			layout: 'form',
			bodyStyle: 'padding:10px 10px 10px 10px',
			anchor: '100%',
			labelWidth: 1,
			autoWidth: true,
			width: lebar,
			border: false,
			//-------------- #items# --------------
			items:
			[
				getItemPanelInput_viInventoriStokOpname(lebar),
				//-------------- ## -------------- 				
				getItemGridTransaksi_viInventoriStokOpname(lebar),
				//-------------- ## --------------
				
			],
			//-------------- #End items# --------------
			fileUpload: true,
			// Tombol pada tollbar Edit Data Pasien
			tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						id: 'btnAdd_viInventoriStokOpname',
						handler: function(){
							dataaddnew_viInventoriStokOpname();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viInventoriStokOpname',
						handler: function()
						{
							datasave_viInventoriStokOpname(addNew_viInventoriStokOpname);
							datarefresh_viInventoriStokOpname();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						id: 'btnSimpanExit_viInventoriStokOpname',
						handler: function()
						{
							var x = datasave_viInventoriStokOpname(addNew_viInventoriStokOpname);
							datarefresh_viInventoriStokOpname();
							if (x===undefined)
							{
								setLookUps_viInventoriStokOpname.close();
							}
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viInventoriStokOpname',
						handler: function()
						{
							datadelete_viInventoriStokOpname();
							datarefresh_viInventoriStokOpname();
							
						}
					},
					//-------------- ## --------------
					{
						xtype:'tbseparator'
					},
					
				]
			}//,items:
		}
    )

    return pnlFormDataBasic_viInventoriStokOpname;
}
// End Function getFormItemEntry_viInventoriStokOpname # --------------

/**
*	Function : getItemPanelInput_viInventoriStokOpname
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemPanelInput_viInventoriStokOpname(lebar) 
{
    
    var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		labelWidth: 100,
		autoWidth: true,
		border:true,
		items:
		[			
			{
                xtype: 'compositefield',
                fieldLabel: 'Kd Kelompok',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					{
						xtype: 'textfield',
						flex: 1,
						width : 120,	
						name: 'txtKdKelompok_viInventoriStokOpname',
						id: 'txtKdKelompok_viInventoriStokOpname',
						emptyText: 'Kd Kelompok',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},                    
					{
						xtype: 'textfield',
						flex: 1,
						width : 90,	
						name: 'txtKdKelompok2_viInventoriStokOpname',
						id: 'txtKdKelompok2_viInventoriStokOpname',						
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},                  
					{
						xtype: 'displayfield',
						flex: 1,
						width: 395,
						name: '',
						value: '',
						fieldLabel: 'Label'
					},						
					{
						xtype: 'displayfield',
						flex: 1,
						width: 80,
						name: '',
						value: 'Tanggal :',
						fieldLabel: 'Label'
					},					
					{
						xtype: 'datefield',
						id: 'txtDateTanggal_viInventoriStokOpname',
						value: now_viInventoriStokOpname,
						format: 'd/M/Y',
						width: 120,
						listeners:
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									datarefresh_viInventoriStokOpname();								
								} 						
							}
						}
					},									                                        					
                ]
            },            			
			{
				xtype: 'compositefield',
				fieldLabel: 'Nama Barang',
				anchor: '100%',
				//labelSeparator: '',
				width: 199,
				items: 
				[
					{
						xtype: 'textfield',
						flex: 1,
						width : 615,	
						name: 'txtNmBarang_viInventoriStokOpname',
						id: 'txtNmBarang_viInventoriStokOpname',
						emptyText: 'Nama Barang',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},
					{
						xtype: 'displayfield',
						flex: 1,
						width: 80,
						name: '',
						value: 'Stok Min:',
						fieldLabel: 'Label'
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 120,	
						name: 'txtStokMin_viInventoriStokOpname',
						id: 'txtStokMin_viInventoriStokOpname',
						emptyText: 'Stok Min',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},	
				]
			},
			{
				xtype: 'compositefield',
				fieldLabel: 'Satuan',
				anchor: '100%',
				//labelSeparator: '',
				width: 199,
				items: 
				[
					{
						xtype: 'textfield',
						flex: 1,
						width : 120,	
						name: 'txtSatuan_viInventoriStokOpname',
						id: 'txtSatuan_viInventoriStokOpname',
						emptyText: 'Satuan',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},						
					{
						xtype: 'displayfield',
						flex: 1,
						width: 80,
						name: '',
						value: 'Kelompok:',
						fieldLabel: 'Label'
					},					
					{
						xtype: 'textfield',
						flex: 1,
						width : 615,	
						name: 'txtKelompok_viInventoriStokOpname',
						id: 'txtKelompok_viInventoriStokOpname',
						emptyText: 'Kelompok',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},
					
				]
			},
		]
	};
    return items;
};
// End Function getItemPanelInput_viInventoriStokOpname # --------------


/**
*	Function : getItemGridTransaksi_viInventoriStokOpname
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemGridTransaksi_viInventoriStokOpname(lebar) 
{
    var items =
	{
		//title: 'Detail Transaksi', 
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:1px 1px 1px 1px',
		border:true,
		width: lebar-80,
		height: 430,//347,//225, 
	    items:
		[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:
				[
					gridDataViewEdit_viInventoriStokOpname()
				]	
			}
			//-------------- ## --------------
		]
	};
    return items;
};
// End Function getItemGridTransaksi_viInventoriStokOpname # --------------

/**
*	Function : gridDataViewEdit_viInventoriStokOpname
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function gridDataViewEdit_viInventoriStokOpname()
{
    
    chkSelected_viInventoriStokOpname = new Ext.grid.CheckColumn
	(
		{
			id: 'chkSelected_viInventoriStokOpname',
			header: '',
			align: 'center',						
			dataIndex: 'SELECTED',			
			width: 20
		}
	);

    var FieldGrdKasir_viInventoriStokOpname = 
	[
	];
	
    dsDataGrdJab_viInventoriStokOpname= new WebApp.DataStore
	({
        fields: FieldGrdKasir_viInventoriStokOpname
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJab_viInventoriStokOpname,
        height: 425,//220,
		selModel: new Ext.grid.RowSelectionModel
        (
	        {
	            singleSelect: true,
	            listeners:
	            {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
	        }
        ),
        columns: 
		[
			chkSelected_viInventoriStokOpname,			
			{			
				dataIndex: '',
				header: 'Kode',
				sortable: true,
				width: 80
			},
			//-------------- ## --------------			
			{
				dataIndex: '',
				header: 'Nama Barang',
				sortable: true,
				width: 400,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Satuan',
				sortable: true,
				width: 150,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Tgl Posting',
				sortable: true,
				width: 100,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Stok Buku',
				sortable: true,
				width: 100,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Stok Real',
				sortable: true,
				width: 100,
				renderer: function(v, params, record) 
				{
					
				}	
			},			
        ],
        plugins:chkSelected_viInventoriStokOpname,
    }    
    return items;
}
// End Function gridDataViewEdit_viInventoriStokOpname # --------------


