var dataSource_viEndsSetupDokter;
var selectCount_viEndsSetupDokter=50;
var NamaForm_viEndsSetupDokter="SetupDokter";
var mod_name_viEndsSetupDokter="SetupDokter";
var now_viEndsSetupDokter= new Date();
var rowSelected_viEndsSetupDokter;
var setLookUps_viEndsSetupDokter;
var tanggal = now_viEndsSetupDokter.format("d/M/Y");
var jam = now_viEndsSetupDokter.format("H/i/s");
var tmpkriteria;
var DataGridJenisObat;
var GridDataView_viEndsSetupDokter;
var kodesetupDokter='';

var CurrentData_viEndsSetupDokter =
{
	data: Object,
	details: Array,
	row: 0
};
var setting_kd_kasir;
var setting_kd_unit;
var tmpkd_unit;
var default_Kd_Unit;
var dsunitEnds_viSetupDokterEnds;
var EndsSetupDokter={};
EndsSetupDokter.form={};
EndsSetupDokter.func={};
EndsSetupDokter.vars={};
EndsSetupDokter.func.parent=EndsSetupDokter;
EndsSetupDokter.form.ArrayStore={};
EndsSetupDokter.form.ComboBox={};
EndsSetupDokter.form.DataStore={};
EndsSetupDokter.form.Record={};
EndsSetupDokter.form.Form={};
EndsSetupDokter.form.Grid={};
EndsSetupDokter.form.Panel={};
EndsSetupDokter.form.TextField={};
EndsSetupDokter.form.Button={};

EndsSetupDokter.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_unit_far', 'nm_unit_far', 'nomor_ro_unit', 'nomorawal','nomor_out_milik'],
	data: []
});

CurrentPage.page = dataGrid_viEndsSetupDokter(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);


function get_kd_kasir(){
    Ext.Ajax.request({
        url: baseURL + "index.php/endoscopy/Endoscopy/getDefaultKasir",
        params: {
           text:''
        },
        failure: function (o)
        {
            var cst = Ext.decode(o.responseText);
        },
        success: function (o) {
            var cst = Ext.decode(o.responseText);
            setting_kd_kasir = cst.kd_kasir;
        }
    });
}

function get_kd_unit(){
    Ext.Ajax.request({
        url: baseURL + "index.php/endoscopy/Endoscopy/getDefaultUnit",
        params: {
            notrans: 0,
        },
        failure: function (o)
        {
            var cst = Ext.decode(o.responseText);
        },
        success: function (o) {
            var cst = Ext.decode(o.responseText);
            // console.log(cst.kd_unit);
            setting_kd_unit = "'"+cst.kd_unit+"'";
        }
    });
}

function dataGrid_viEndsSetupDokter(mod_id_viEndsSetupDokter){	
 	// Field kiriman dari Project Net.
    var FieldMaster_viEndsSetupDokter = 
	[
		'kd_unit_far', 'nm_unit_far', 'nomor_ro_unit', 'nomorawal','nomor_out_milik' 
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viEndsSetupDokter = new WebApp.DataStore
	({
        fields: FieldMaster_viEndsSetupDokter
    });
    dataGriEndsSetupDokter();
	get_kd_kasir();
	get_kd_unit();
    // Grid gizi Perencanaan # --------------
	GridDataView_viEndsSetupDokter = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viEndsSetupDokter,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:true,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							/*kodesetupDokter=rowSelected_viEndsSetupDokter.data.kd_Dokter
							alert(kodesetupDokter);*/
							rowSelected_viEndsSetupDokter = undefined;
							rowSelected_viEndsSetupDokter = dataSource_viEndsSetupDokter.getAt(row);
							CurrentData_viEndsSetupDokter
							CurrentData_viEndsSetupDokter.row = row;
							CurrentData_viEndsSetupDokter.data = rowSelected_viEndsSetupDokter.data;
							kodesetupDokter=rowSelected_viEndsSetupDokter.data.kd_dokter;
							//DataInitEndsSetupDokter(rowSelected_viEndsSetupDokter.data);
							console.log(kodesetupDokter);
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					kodesetupDokter=rowSelected_viEndsSetupDokter.data.kd_Dokter;
					rowSelected_viEndsSetupDokter = dataSource_viEndsSetupDokter.getAt(ridx);
					if (rowSelected_viEndsSetupDokter != undefined)
					{
						DataInitEndsSetupDokter(rowSelected_viEndsSetupDokter.data);
						//setLookUp_viEndsSetupDokter(rowSelected_viEndsSetupDokter.data);
					}
					else
					{
						//setLookUp_viEndsSetupDokter();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Setup Unit
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header: 'Kode Dokter',
						dataIndex: 'kd_dokter',
						hideable:false,
						menuDisabled: true,
						width: 40
						
					},
					//-------------- ## --------------
					{
						header: 'Nama Dokter',
						dataIndex: 'nama',
						hideable:false,
						menuDisabled: true,
						width: 90
					},
					//-------------- ## --------------
					{
						header: 'Jenis Dokter',
						dataIndex: 'jenis_dokter',
						hideable:false,
						menuDisabled: true,
						width: 90
					},
					//-------------- ## --------------
					{
						header: 'spesialisasi',
						dataIndex: 'job',
						hideable:false,
						menuDisabled: true,
						width: 90
					}
				]
			),
			/* tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viEndsSetupDokter',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Dokter Ends',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Dokter Ends',
						id: 'btnEdit_viEndsSetupDokter',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viEndsSetupDokter != undefined)
							{
								DataInitEndsSetupDokter(rowSelected_viEndsSetupDokter.data);
							}
							
						}
					}
					//-------------- ## --------------
				]
			}, */
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viEndsSetupDokter, selectCount_viEndsSetupDokter, dataSource_viEndsSetupDokter),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)	
	var PanelTabEndsSetupDokter = new Ext.FormPanel({
        labelAlign: 'top',
		layout:'form',
		border: false,  
		autoHeight:true,
        title: '',
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [PanelInputUnit()]
		
	})
	// Kriteria filter pada Grid # --------------
    var FrmData_viEndsSetupDokter = new Ext.Panel
    (
		{
			title: NamaForm_viEndsSetupDokter,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viEndsSetupDokter,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ PanelTabEndsSetupDokter,
					GridDataView_viEndsSetupDokter],
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAddSetupDokter_viEndsSetupDokter',
						handler: function(){
							AddNewEndsSetupDokter();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viEndsSetupDokter',
						handler: function()
						{
							loadMask.show();
							dataSave_viEndsSetupDokter();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viEndsSetupDokter',
						handler: function()
						{
							if (kodesetupDokter==='')
							{
								ShowPesanErrorEndsSetupDokter('Tidak ada data yang dipilih', 'Error');
							}
							else
							{
								Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
									if (button == 'yes'){
										loadMask.show();
										dataDelete_viEndsSetupDokter();
									}
								});
							}
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_viEndsSetupDokter',
						handler: function()
						{
							dataSource_viEndsSetupDokter.removeAll();
							dataGriEndsSetupDokter();
							kodesetupDokter='';
						}
					},
					{
						xtype: 'tbseparator'
					}
					
				]
			}
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_viEndsSetupDokter;
    //-------------- # End form filter # --------------
}

function PanelInputUnit(rowdata){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 120,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'Kode Dokter'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 0,
								xtype: 'textfield',
								id: 'txtKdSetupDokter_EndsSetupDokter',
								name: 'txtKdSetupDokter_EndsSetupDokter',
								width: 100,
								allowBlank: false,
								maxLength:3,
								readOnly:true,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									},
									blur: function(a){
										if(a.getValue().length > 3){
											ShowPesanWarningEndsSetupDokter('Kode unit tidak boleh lebih dari 3 huruf', 'Warning');
										}
									}
								}
							},
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'Nama Dokter'
							},
							{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							EndsSetupDokter.vars.nama=new Nci.form.Combobox.autoComplete({
								x: 130,
								y: 30,
								tabIndex:2,
								store	: EndsSetupDokter.form.ArrayStore.a,
								select	: function(a,b,c){
									
									Ext.getCmp('txtKdSetupDokter_EndsSetupDokter').setValue(b.data.kd_dokter);
									
									GridDataView_viEndsSetupDokter.getView().refresh();
									
								},
								/* onShowList:function(a){
									dataSource_viEndsSetupDokter.removeAll();
									
									var recs=[],
									recType=dataSource_viEndsSetupDokter.recordType;
										
									for(var i=0; i<a.length; i++){
										recs.push(new recType(a[i]));
									}
									dataSource_viEndsSetupDokter.add(recs);
									
								}, */
								insert	: function(o){
									return {
										kd_dokter   : o.kd_dokter,
										nama 		: o.nama,
										text			:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_dokter+'</td><td width="200">'+o.nama+'</td></tr></table>'
									}
								},
								url		: baseURL + "index.php/main/functionLAB/getDokter",
								valueField: 'nama',
								displayField: 'text',
								listWidth: 280
							}),
							{
								x: 10,
								y: 60,
								xtype: 'label',
								text: 'Pelaksana'
							},
							{
								x: 120,
								y: 60,
								xtype: 'label',
								text: ':'
							},
								mComboPelaksanaEnds_job(),
							/*{
								x: 130,
								y: 60,
								xtype: 'textfield',
								id: 'txtSpesialisasi_EndsSetupDokter',
								name: 'txtSpesialisasi_EndsSetupDokter',
								width: 150,
								tabIndex:3
							},*/
							{
								x: 10,
								y: 90,
								xtype: 'label',
								text: 'Unit Endoscopy'
							},
							{
								x: 120,
								y: 90,
								xtype: 'label',
								text: ':'
							},mComboUnitEnds()
							
							
							
						]
					}
				]
			}
		]		
	};
        return items;
}

function mComboUnitEnds(){ 
    var Field = ['KD_UNIT','NAMA_UNIT'];
    dsunitlab_viSetupDokterEnds= new WebApp.DataStore({ fields: Field });
    dsunitlab_viSetupDokterEnds.load({
		params:{
			Skip: 0,
			Take: 1000,
			Sort: 'nama_unit',
			Sortdir: 'ASC',
			target: 'ViewSetupUnit',
			param: "parent = "+setting_kd_unit+" OR kd_unit = "+setting_kd_unit
		}
	});
    var cboUnitEnds_viSetupDokterEnds = new Ext.form.ComboBox({
		id: 'cboUnitEnds_viSetupDokterEnds',
		x: 130,
		y: 90,
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		readOnly: false,
		mode: 'local',
		emptyText: '',
		fieldLabel:  ' ',
		align: 'Right',
		width: 230,
		emptyText:'Pilih Unit',
		store: dsunitlab_viSetupDokterEnds,
		valueField: 'KD_UNIT',
		displayField: 'NAMA_UNIT',
		//value:'All',
		editable: false,
			listeners:
			{
				'select': function(a,b,c)
				{
					tmpkd_unit = b.data.KD_UNIT;
				}
			}
	});
    return cboUnitEnds_viSetupDokterEnds;
}
//------------------end---------------------------------------------------------

function dataGriEndsSetupDokter(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/endoscopy/setup_dokter/getDokter",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorEndsSetupDokter('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					tmpkd_unit=cst.kd_unit;
					Ext.getCmp('cboUnitEnds_viSetupDokterEnds').setValue(cst.nama_unit);
					var recs=[],
						recType=dataSource_viEndsSetupDokter.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dataSource_viEndsSetupDokter.add(recs);
					
					
					
					GridDataView_viEndsSetupDokter.getView().refresh();
				}
				else 
				{
					ShowPesanErrorEndsSetupDokter('Gagal membaca data Dokter', 'Error');
				};
			}
		}
		
	)
	
}

function dataSave_viEndsSetupDokter(){
	if (ValidasiSaveEndsSetupDokter(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/endoscopy/setup_dokter/simpan_setup_dokter",
				params: getParamSaveEndsSetupDokter(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorEndsSetupDokter('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoEndsSetupDokter('Berhasil menyimpan data ini','Information');
						dataSource_viEndsSetupDokter.removeAll();
						dataGriEndsSetupDokter();
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorEndsSetupDokter('Gagal menyimpan data ini', 'Error');
						dataSource_viEndsSetupDokter.removeAll();
						dataGriEndsSetupDokter();
					};
				}
			}
			
		)
	}
}

function dataDelete_viEndsSetupDokter(){
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/endoscopy/setup_dokter/hapus_setup_dokter",
				params: getParamDeleteEndsSetupDokter(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorEndsSetupDokter('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoEndsSetupDokter('Berhasil menghapus data ini','Information');
						AddNewEndsSetupDokter()
						dataSource_viEndsSetupDokter.removeAll();
						dataGriEndsSetupDokter();
						kodesetupDokter='';
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorEndsSetupDokter('Gagal menghapus data ini', 'Error');
						dataSource_viEndsSetupDokter.removeAll();
						dataGriEndsSetupDokter();
					};
				}
			}
			
		)
	
}

function AddNewEndsSetupDokter(){
	Ext.getCmp('txtKdSetupDokter_EndsSetupDokter').setValue('');
	EndsSetupDokter.vars.nama.setValue('');
	EndsSetupDokter.vars.nama.focus();
	Ext.getCmp('cboUnitEnds_viSetupDokterEnds').setValue('Endoscopy');
};

function DataInitEndsSetupDokter(rowdata){
	console.log(rowdata);
	Ext.getCmp('txtKdSetupDokter_EndsSetupDokter').setValue(rowdata.kd_dokter);
	EndsSetupDokter.vars.nama.setValue(rowdata.nama);
	Ext.getCmp('cboUnitEnds_viSetupDokterLab_job').setValue(rowdata.kd_job);
};

function getParamSaveEndsSetupDokter(){
	var	params =
	{
		KdSetupDokter:Ext.getCmp('txtKdSetupDokter_EndsSetupDokter').getValue(),
		NamaSetupDokter:EndsSetupDokter.vars.nama.getValue(),
		kd_job:Ext.getCmp('cboUnitEnds_viSetupDokterLab_job').getValue(),
		kd_unit:tmpkd_unit
	}
   
    return params
};

function getParamDeleteEndsSetupDokter(){
	var	params ={
		KdSetupDokter:Ext.getCmp('txtKdSetupDokter_EndsSetupDokter').getValue(),
		kd_unit:tmpkd_unit
	}
   
    return params
};

function ValidasiSaveEndsSetupDokter(modul,mBolHapus){
	var x = 1;
	if(EndsSetupDokter.vars.nama.getValue() === '' /*|| Ext.getCmp('txtKdSetupDokter_EndsSetupDokter').getValue() ===''*/){
		if(EndsSetupDokter.vars.nama.getValue() === ''){
			loadMask.hide();
			ShowPesanWarningEndsSetupDokter('Nama unit masih kosong', 'Warning');
			x = 0;
		} else{
			loadMask.hide();
			ShowPesanWarningEndsSetupDokter('Kode unit masih kosong', 'Warning');
			x = 0;
		}
		
	} 
	if(Ext.getCmp('txtKdSetupDokter_EndsSetupDokter').getValue().length > 3){
		ShowPesanWarningEndsSetupDokter('Kode unit tidak boleh lebih dari 3 huruf', 'Warning');
		x = 0;
	}
	return x;
};



function ShowPesanWarningEndsSetupDokter(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorEndsSetupDokter(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoEndsSetupDokter(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};

function mComboPelaksanaEnds_job(){ 
    var cboUnitEnds_viSetupDokterLab = new Ext.form.ComboBox({
		id: 'cboUnitEnds_viSetupDokterLab_job',
		x: 130,
		y: 60,
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		emptyText: '',
		fieldLabel:  ' ',
		align: 'Right',
		width: 230,
		emptyText:'Pilih Pelaksana',
        store           : new Ext.data.ArrayStore({
            id: 0,
            fields:['Id','displayText'],
            data: [[1, 'Dokter'],[5, 'Perawat'],]
        }),
		valueField: 'Id',
		displayField: 'displayText',
		//value:'All',
			listeners:
			{
				'select': function(a,b,c)
				{
					// tmpkd_unit = b.data.KD_UNIT;
				}
			}
	});
    return cboUnitEnds_viSetupDokterLab;
}