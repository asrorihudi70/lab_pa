var setting_kd_unit;
var setting_kd_kasir;

function get_kd_kasir(){
    Ext.Ajax.request({
        url: baseURL + "index.php/endoscopy/Endoscopy/getDefaultKasir",
        params: {
           text:''
        },
        failure: function (o)
        {
            var cst = Ext.decode(o.responseText);
        },
        success: function (o) {
            var cst = Ext.decode(o.responseText);
            setting_kd_kasir = cst.kd_kasir;
        }
    });
}

function get_kd_unit(){
    Ext.Ajax.request({
        url: baseURL + "index.php/endoscopy/Endoscopy/getDefaultUnit",
        params: {
            notrans: 0,
        },
        failure: function (o)
        {
            var cst = Ext.decode(o.responseText);
        },
        success: function (o) {
            var cst = Ext.decode(o.responseText);
            setting_kd_unit = "'"+cst.kd_unit+"'";
        }
    });
}