var dsRWIDetailPasien;
var selectNamaRWIDetailPasien;
var now = new Date();
var selectCount_viInformasiUnitdokter=50;
var icons_viInformasiUnitdokter="Gaji";
var dataSource_kunjungan;
var dsDataSource_unit = new Ext.data.JsonStore();
var selectSetJenisPasien='Semua Pasien';
var frmDlgRWIDetailPasien;
var secondGridStore;
var varDlgBukaLaporan= ShowFormDlgBukaLaporan();
var selectSetUmum;
var firstGrid;
var secondGrid;
var comboOne;
var type_file=0;
var title_unit='';
var CurrentData_viDaftarRWI =
{
    data: Object,
    details: Array,
    row: 0
};

function ShowFormDlgBukaLaporan()
{
    frmDlgRWIDetailPasien= fnDlgRWIBukaTransaksi();
    frmDlgRWIDetailPasien.show();
};
function fnDlgRWIBukaTransaksi()
{
    // console.log("Cek");
    var winRWIBukatransaksi = new Ext.Window
    (
        {
            id: 'winRWIBukatransaksi',
            title: 'Buka Transaksi',
            closeAction: 'destroy',
            width:500,
            height: '100%',
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [dataGrid_viSummeryPasien()],
            listeners:
			{
				activate: function()
				{
				}
			},
			fbar:[
				{
					xtype:'button',
					text: 'Ok',
					id: 'btnOkShift_TutupShiftRad',
					handler:function()
					{
						TutupShiftSave_TutupShift(false);
					}
				},{
					xtype:'button',
					text: 'Close',
					id: 'btnCloseShift_TutupShiftRad',
					handler:function()
					{
						frmDlgRWIDetailPasien.close();
					}
				},
			]
        }
    );

    return winRWIBukatransaksi;
};

function TutupShiftSave_TutupShift(mBol)
{
	Ext.Msg.show({
		title: 'Konfirmasi Tutup Shift',
		msg: 'Anda yakin akan tutup shift Unit '+Ext.get('cboDataUnit').getValue()+' dari Shift '+ Ext.get('txtNilaiShift_TutupShiftFisiotherapy').getValue()+' ke Shift '+Ext.get('txtNilaiShiftSelanjutnya_TutupShiftFisiotherapy').getValue()+' ?',
		buttons: Ext.MessageBox.YESNO,
		fn: function (btn) {
			if (btn == 'yes')
			{
				Ext.Ajax.request({
					url: baseURL + "index.php/fisiotherapy/shift/tutup_shift",
					params: {
						tanggal 			: Ext.getCmp('txtTglShift_TutupShiftFisiotherapy').getValue(),
						shiftKe 			: Ext.getCmp('txtNilaiShift_TutupShiftFisiotherapy').getValue(),
						shiftSelanjutnya 	: Ext.getCmp('txtNilaiShiftSelanjutnya_TutupShiftFisiotherapy').getValue(),
						kd_unit 			: Ext.getCmp('cboDataUnit').getValue(),
					},
					success: function(o)
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true)
						{
                            document.getElementById('ext-comp-1007').innerHTML='Shift : ' +Ext.getCmp('txtNilaiShiftSelanjutnya_TutupShiftFisiotherapy').getValue();
							ShowPesanInfoShift('Simpan Shift Berhasil','Simpan Shift');
							frmDlgRWIDetailPasien.close();
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanErrorShift('Simpan Shift Gagal','Simpan Shift');
						}
						else
						{
							ShowPesanErrorShift('Simpan Shift Gagal','Simpan Shift');
						};
					}
				})
			}
		},
		icon: Ext.MessageBox.QUESTION
	}); 

};

function ShowPesanWarningRWIDetailPasienReport(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width:300
        }
    );
};

function mComboListUnit()
{
	var Field = ['kd_unit', 'nama_unit'];
	ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});

	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/fisiotherapy/shift/getUnit",
			params: {text:''},
			failure: function(o)
			{
				
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					var recs=[],
					recType=ds_Poli_viDaftar.recordType;	
					for(var i=0; i<cst.listData.length; i++){
						recs.push(new recType(cst.listData[i]));
					}
					ds_Poli_viDaftar.add(recs);
				}
			}
		}
		
	)
    var cboDataUnit = new Ext.form.ComboBox
    (
        {

            id:'cboDataUnit',
            typeAhead       : true,
            triggerAction   : 'all',
            lazyRender      : true,
            mode            : 'local',
            selectOnFocus   : true,
            forceSelection  : true,
            emptyText:'Silahkan Pilih...',
            fieldLabel: 'Unit',
            align: 'Right',
            width: '100%',
            // value: now.format("Y-m-d"),
            store: ds_Poli_viDaftar,
            valueField: 'kd_unit',
            displayField: 'nama_unit',
            listeners:
            {
                select : function(a, b){
                	getShift(b.data.kd_unit);
                }      
            }
        }
    );
    return cboDataUnit;
};

function getShift(kd_unit) 
{
    AddNewKasirShift = true;
   // ajax digunakan untuk meminta hak akses dari server untuk modul yg di request
   // ajax request untuk mengambil data current shift	
	Ext.Ajax.request(
	{
	    url: baseURL + "index.php/fisiotherapy/shift/getCurrentShift",
		params: {
	        kd_unit : kd_unit,
	    },
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
	    success: function(o) {
			var cst = Ext.decode(o.responseText);
			if (cst.status === true) {	
				Ext.getCmp('txtNilaiShift_TutupShiftFisiotherapy').setValue(cst.sekarang);
				Ext.getCmp('txtNilaiShiftSelanjutnya_TutupShiftFisiotherapy').setValue(cst.tujuan);
			}
	    }
	
	});
};

function datarefresh_dataSourceKunjungan()
{
    loadMask.show();
    dsDataSource_unit.removeAll();
    Ext.getCmp('btnOkDlgBukaLaporan').disable(); 
    Ext.Ajax.request({
        url: baseURL + "index.php/rawat_inap/data_kunjungan/kunjungan",
        params: {
            kd_pasien : Ext.getCmp('textfieldNoMedrec').getValue(),
        },
        success: function(response) {
            //var mystore = Ext.data.StoreManager.lookup('CustomerDataStore');
            var cst  = Ext.decode(response.responseText);
            //dsDataPerawatPenindak_KASIR_RWI.load(myData);
            if (cst['DATA'].length > 0) {
                for(var i=0,iLen=cst['DATA'].length; i<iLen; i++){
                    var recs    = [],recType = dsDataSource_unit.recordType;
                    var o       = cst['DATA'][i];
                    recs.push(new recType(o));
                    dsDataSource_unit.add(recs);
                    // console.log(cst['DATA'][i]);
                }
                Ext.getCmp('textfieldNama').setValue(cst.NAMA);
            }else{
                ShowPesanWarningBukaTransaksi("Pasien belum pernah Rawat Inap", "Information");
            }
            loadMask.hide();
        },     
    });
}

function dataGrid_viSummeryPasien(mod_id)
{
    var FrmTabs_viInformasiUnitdokter = new Ext.Panel
        (
            {
                id          : mod_id,
                closable    : true,
                region      : 'center',
                layout      : 'column',
                height      : 130,
                itemCls     : 'blacklabel',
                bodyStyle   : 'padding: 5px 5px 5px 5px',
                border      : false,
                shadhow     : true,
                margins     : '5 5 5 5',
                anchor      : '99%',
                iconCls     : icons_viInformasiUnitdokter,
                items       : 
                [
					{
                        columnWidth: .97,
                        layout: 'form',
                        border: true,
                        autoScroll: true,
                        bodyStyle: 'padding: 5px 5px 5px 5px',
                                            labelAlign: 'right',
                                            labelWidth: 50,
                        items:
                        [

                            {
                                columnWidth: .50,
                                layout: 'form',
                                border: false,
                                labelAlign: 'right',
                                labelWidth: 100,
                                items:
                                [
                                    mComboListUnit(),
                                    {
                                        xtype       : 'textfield',
                                        fieldLabel  : 'Shift aktif',
                                        id          : 'txtNilaiShift_TutupShiftFisiotherapy',
                                        anchor      : '100%',
                                    },
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'Shift selanjutnya ',
                                        id: 'txtNilaiShiftSelanjutnya_TutupShiftFisiotherapy',
                                        anchor: '100%'
                                    },
                                    {
										xtype: 'datefield',
										fieldLabel: 'Tgl Shift ',
										id: 'txtTglShift_TutupShiftFisiotherapy',
										name: 'txtTglShift_TutupShiftFisiotherapy',
										anchor: '100%',
										format: 'd/M/Y',
										readOnly:true,
										value: now,
                                    },
                                ]
                            },
                        ]
                    },
                ]    
        }
    )
    return FrmTabs_viInformasiUnitdokter;
}

function ShowPesanWarningShift(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorShift(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoShift(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};


