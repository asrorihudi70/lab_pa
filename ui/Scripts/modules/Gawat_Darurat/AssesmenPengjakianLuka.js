var PengkajianLukaIGD = {};
var rowSelectedPJRWI_CPPT;
var now = new Date();
PengkajianLukaIGD.selectCount        = 50;
PengkajianLukaIGD.dsComboObat;
PengkajianLukaIGD.gridRiwayatKunjungan;
PengkajianLukaIGD.gridRiwayatDiagnosa;
PengkajianLukaIGD.gridRiwayatTindakan;
PengkajianLukaIGD.selectCountStatus_bayar = 'Belum Pulang';
PengkajianLukaIGD.gridRiwayatObat;
PengkajianLukaIGD.gridRiwayatLab;
PengkajianLukaIGD.gridRiwayatRad;
PengkajianLukaIGD.iComboObat;
PengkajianLukaIGD.iComboVerifiedObat;
PengkajianLukaIGD.gridObat;
PengkajianLukaIGD.gridLastHistoryDiagnosa;
PengkajianLukaIGD.DataStoreDokter=new Ext.data.ArrayStore({id: 0,fields:['kd_dokter','nama','status','jenis_dokter'],data: []});
PengkajianLukaIGD.grid1;
PengkajianLukaIGD.grid2;
PengkajianLukaIGD.grid3;
PengkajianLukaIGD.mRecordRwiCTTP = Ext.data.Record.create([
    {name: 'tgl', mapping: 'tgl'},
    {name: 'jam', mapping: 'jam'},
    {name: 'bagian', mapping: 'gbagian'},
    {name: 'hasil', mapping: 'hasil'},
    {name: 'verifikasi', mapping: 'verifikasi'}
]);
PengkajianLukaIGD.ds1;
PengkajianLukaIGD.ds2;
PengkajianLukaIGD.ds3;
PengkajianLukaIGD.ds4 = new WebApp.DataStore({fields: ['kd_produk', 'kd_klas', 'deskripsi', 'username', 'kd_lab']});
PengkajianLukaIGD.ds5 = new WebApp.DataStore({fields: ['kd_cara_keluar', 'cara_keluar']});
PengkajianLukaIGD.dssebabkematian = new WebApp.DataStore({fields: ['kd_sebab_mati', 'sebab_mati']});
PengkajianLukaIGD.dsstatupulang = new WebApp.DataStore({fields: ['kd_status_pulang', 'status_pulang']});
PengkajianLukaIGD.dsGridObat;
PengkajianLukaIGD.dsGridTindakan;
PengkajianLukaIGD.s1;
PengkajianLukaIGD.btn1;
PengkajianLukaIGD.ds_trbokter_rwi;
PengkajianLukaIGD.form = {};
PengkajianLukaIGD.form.Grid = {};
PengkajianLukaIGD.func = {};
PengkajianLukaIGD.TGL_TRANSAKSI;
PengkajianLukaIGD.TGL_TRANSAKSI_detailtransaksi;
PengkajianLukaIGD.pj_req_rad;
PengkajianLukaIGD.form.Checkbox = {};
PengkajianLukaIGD.form.Class = {};
PengkajianLukaIGD.form.ComboBox = {};
PengkajianLukaIGD.form.DataStore = {};
PengkajianLukaIGD.form.Grid = {};
PengkajianLukaIGD.form.Group = {};
PengkajianLukaIGD.form.Group.print = {};
PengkajianLukaIGD.form.Window = {};
PengkajianLukaIGD.URUT;
PengkajianLukaIGD.KD_PRODUK;
PengkajianLukaIGD.QTY;
PengkajianLukaIGD.TGL_BERLAKU;
PengkajianLukaIGD.HARGA;
PengkajianLukaIGD.varkd_tarif;
PengkajianLukaIGD.KD_TARIF;
PengkajianLukaIGD.DESKRIPSI;
PengkajianLukaIGD.dshasil_cppt_rwi;
PengkajianLukaIGD.gridhasil_lab_PJRWI_CPPT;
PengkajianLukaIGD.gridrad;
PengkajianLukaIGD.gridIcd9;
PengkajianLukaIGD.var_kd_dokter_leb;
PengkajianLukaIGD.var_kd_dokter_rad;
PengkajianLukaIGD.form.DataStore.penyakit = new Ext.data.ArrayStore({id: 0, fields: ['text', 'kd_penyakit', 'penyakit'], data: []});
PengkajianLukaIGD.form.DataStore.kdpenyakit= new Ext.data.ArrayStore({id: 0, fields: ['text', 'kd_penyakit', 'penyakit'], data: []});
PengkajianLukaIGD.form.DataStore.produk = new Ext.data.ArrayStore({id: 0, fields: ['kd_produk', 'deskripsi', 'harga', 'tglberlaku','kd_tarif'], data: []});
PengkajianLukaIGD.form.DataStore.trdokter = new Ext.data.ArrayStore({id: 0, fields: ['kd_dokter', 'nama'], data: []});
PengkajianLukaIGD.form.DataStore.dokter_inap_int = new Ext.data.ArrayStore({id: 0, fields: ['kd_job', 'label'], data: []});
PengkajianLukaIGD.form.DataStore.kdIcd9 = new Ext.data.ArrayStore({id: 0,fields: ['text','kd_icd9','deskripsi'],data: []});
PengkajianLukaIGD.form.DataStore.deskripsi = new Ext.data.ArrayStore({id: 0,fields: ['text','kd_icd9','deskripsi'],data: []});
PengkajianLukaIGD.form.DataStore.obat=new Ext.data.ArrayStore({
    id: 0,
    fields: [
                'kd_prd','nama_obat','jml_stok_apt','kd_unit_far','kd_milik'
            ],
    data: []
});
var fieldsDokterPenindak = [
    {name: 'KD_DOKTER', mapping : 'KD_DOKTER'},
    {name: 'NAMA', mapping : 'NAMA'}
];
dsDataDokterPenindak_PJ_RWI = new WebApp.DataStore({ fields: fieldsDokterPenindak });

var Field = ['KD_DOKTER','NAMA'];
var tgl_masuk="";
var kd_unit="";
var urut_masuk="";
dsDokterRequestEntry = new WebApp.DataStore({fields: Field});

CurrentPage.page = getPanelRWI_CPPT(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
PengkajianLukaIGD.ComboVerifiedObat = function () {
    PengkajianLukaIGD.iComboVerifiedObat = new Ext.form.ComboBox({
        id: Nci.getId(),
        typeAhead: true,
        triggerAction: 'all',
        lazyRender: true,
        mode: 'local',
        anchor: '96.8%',
        emptyText: '',
        store: new Ext.data.ArrayStore({
            id: 0,
            fields: ['Id', 'displayText'],
            data: [[0, 'Verified'], [1, 'Not Verified']]
        }),
        valueField: 'displayText',
        displayField: 'displayText'
    });
    return PengkajianLukaIGD.iComboVerifiedObat;
};
PengkajianLukaIGD.classGridObat = Ext.data.Record.create([
    {name: 'kd_prd', mapping: 'kd_prd'},
    {name: 'nama_obat', mapping: 'nama_obat'},
    {name: 'jumlah', mapping: 'jumlah'},
    {name: 'satuan', mapping: 'satuan'},
    {name: 'cara_pakai', mapping: 'cara_pakai'},
    {name: 'kd_dokter', mapping: 'kd_dokter'},
    {name: 'verified', mapping: 'verified'},
    {name: 'racikan', mapping: 'racikan'},
    {name: 'order_mng',     mapping: 'order_mng'},
    {name: 'jml_stok_apt', mapping: 'jml_stok_apt'}

]);
PengkajianLukaIGD.classGrid3 = Ext.data.Record.create([
    {name: 'kd_produk', mapping: 'kd_produk'},
    {name: 'kd_klas', mapping: 'kd_klas'},
    {name: 'deskripsi', mapping: 'deskripsi'},
    {name: 'username', mapping: 'username'},
    {name: 'kd_lab', mapping: 'kd_lab'}
]);
PengkajianLukaIGD.form.Class.diagnosa = Ext.data.Record.create([
    {name: 'KD_PENYAKIT', mapping: 'KD_PENYAKIT'},
    {name: 'PENYAKIT', mapping: 'PENYAKIT'},
    {name: 'KD_PASIEN', mapping: 'KD_PASIEN'},
    {name: 'URUT', mapping: 'UURUTRUT'},
    {name: 'URUT_MASUK', mapping: 'URUT_MASUK'},
    {name: 'TGL_MASUK', mapping: 'TGL_MASUK'},
    {name: 'KASUS', mapping: 'KASUS'},
    {name: 'STAT_DIAG', mapping: 'STAT_DIAG'},
    {name: 'NOTE', mapping: 'NOTE'},
    {name: 'DETAIL', mapping: 'DETAIL'}
]);
PengkajianLukaIGD.form.Class.produk = Ext.data.Record.create([
    {name: 'KD_PRODUK', mapping: 'KD_PRODUK'},
    {name: 'DESKRIPSI', mapping: 'PDESKRIPSIENYAKIT'},
    {name: 'QTY', mapping: 'QTY'},
    {name: 'DOKTER', mapping: 'DOKTER'},
    {name: 'TGL_TINDAKAN', mapping: 'TGL_TINDAKAN'},
    {name: 'QTY', mapping: 'QTY'},
    {name: 'DESC_REQ', mapping: 'DESC_REQ'},
    {name: 'TGL_BERLAKU', mapping: 'TGL_BERLAKU'},
    {name: 'NO_TRANSAKSI', mapping: 'NO_TRANSAKSI'},
    {name: 'URUT', mapping: 'URUT'},
    {name: 'DESC_STATUS', mapping: 'DESC_STATUS'},
    {name: 'TGL_TRANSAKSI', mapping: 'TGL_TRANSAKSI'},
    {name: 'KD_TARIF', mapping: 'KD_TARIF'},
    {name: 'HARGA', mapping: 'HARGA'}
]);
PengkajianLukaIGD.func.getNullProduk = function () {
    console.log(grListTRRWI_CPPT_RWI.getSelectionModel().getSelections()[0]);
    var o = grListTRRWI_CPPT_RWI.getSelectionModel().getSelections()[0].data;
    // var tmptgl_transaksi = new Date(Ext.getCmp('txtTanggalTransaksi_PJRWI').getValue());
    var tmptgl_transaksi = new Date(now);
    return new PengkajianLukaIGD.form.Class.diagnosa({
        KD_UNIT: tmpkd_unitKamar_PJRWI,
        KD_UNIT_TR: tmpkd_unitKamar_PJRWI,
        KD_PRODUK: '',
        DESKRIPSI: '',
        QTY: 1,
        DOKTER: '',
        TGL_TINDAKAN: '',
        DESC_REQ: '',
        TGL_BERLAKU: '',
        NO_TRANSAKSI: '',
        URUT: '',
        // KD_UNIT_TR: o.KD_UNIT,
        DESC_STATUS: '',
        // TGL_TRANSAKSI: o.TANGGAL_TRANSAKSI,
        TGL_TRANSAKSI: tmptgl_transaksi.format("d-M-Y"),
        KD_TARIF: '',
        HARGA: '',
    });
}
function gettrdokterdarivisitedok(rowdata) {
    //var o=grListTRRWI_CPPT_RWI.getSelectionModel().getSelections()[0].data;
    return new PengkajianLukaIGD.form.Class.trdokter({
        kd_nama: '',
        kd_lab: '',
        jp: 0,
        jpp: 0,
    });
}
PengkajianLukaIGD.func.getNulltrdokter = function () {
    //var o=grListTRRWI_CPPT_RWI.getSelectionModel().getSelections()[0].data;
    return new PengkajianLukaIGD.form.Class.trdokter({
        kd_nama: '',
        kd_lab: '',
        jp: 0,
        jpp: 0,
    });
};
PengkajianLukaIGD.func.getNullDiagnosa = function () {
    return new PengkajianLukaIGD.form.Class.diagnosa({
        KD_PENYAKIT: '',
        PENYAKIT: '',
        KD_PASIEN: '',
        URUT: 0,
        URUT_MASUK: '',
        TGL_MASUK: '',
        KASUS: '',
        STAT_DIAG: '',
        NOTE: '',
        DETAIL: ''
    });
}
PengkajianLukaIGD.nullGridObat = function () {
    return new PengkajianLukaIGD.classGridObat({
        kd_produk   : '',
        nama_obat   : '',
        jumlah      : 0,
        signa       : '',
        satuan      : '',
        cara_pakai  : '',
        kd_dokter   : '',
        verified    : 'Tidak Disetujui',
        racikan     : false,
        racikan_text        : 'Tidak',
        order_mng   : 'Belum Dilayani',
        jml_stok_apt :0
    });
}
function hasilJumlah(qty){
    for(var i=0; i<dsPjTrans2_panatajasarwi.getCount() ; i++){
        var o=dsPjTrans2_panatajasarwi.getRange()[i].data;
        if(qty != undefined){
            if(o.jumlah <= o.jml_stok_apt){
            
            } else{
                PengkajianLukaIGD.gridObat.getView().refresh();
            }
        }
    }
}
function hasilJumlah_rwj(qty) {
    for (var i = 0; i < dsPjTrans2_panatajasarwi.getCount(); i++) {
        var o = dsPjTrans2_panatajasarwi.getRange()[i].data;
        if (qty != undefined) {
            if (o.jumlah <= o.jml_stok_apt) {
            } else {
                o.jumlah = o.jml_stok_apt;
                PengkajianLukaIGD.gridObat.getView().refresh();
                ShowPesanWarning_PengkajianLukaIGD('Jumlah obat melebihi stok yang tersedia', 'Warning');
            }
        }
    }
}
PengkajianLukaIGD.nullGrid3 = function () {
    var tgltranstoday = new Date();
    return new PengkajianLukaIGD.classGrid3({
        kd_produk: '',
        deskripsi: '',
        kd_tarif: '',
        harga: '',
        qty: '',
        tgl_berlaku: '',
        urut: '',
        tgl_transaksi: tgltranstoday.format('Y/m/d'),
        no_transaksi: '',
    });
};
PengkajianLukaIGD.comboObat = function(){
    var $this   = this;
    $this.dsComboObat   = new WebApp.DataStore({ fields: ['kd_prd','nama_obat','jml_stok_apt'] });
/*  $this.dsComboObat.load({ 
        params  : { 
            Skip    : 0, 
            Take    : 50, 
            target  :'ViewComboObatRJPJ',
            kdcustomer: vkode_customer
        } 
    }); */
    $this.iComboObat= new Ext.form.ComboBox({
        id              : Nci.getId(),
        typeAhead       : true,
        triggerAction   : 'all',
        lazyRender      : true,
        mode            : 'local',
        emptyText       : '',
        store           : $this.dsComboObat,
        valueField      : 'nama_obat',
        hideTrigger     : true,
        displayField    : 'nama_obat',
        value           : '',
        listeners       : {
            select  : function(a, b, c){
                var line    = $this.gridObat.getSelectionModel().selection.cell[0];
                $this.dsGridObat.getRange()[line].data.kd_prd       =b.json.kd_prd;
                $this.dsGridObat.getRange()[line].data.satuan       =b.json.satuan;
                $this.dsGridObat.getRange()[line].data.kd_dokter    =b.json.nama;
                $this.dsGridObat.getRange()[line].data.nama_obat    =b.json.nama_obat;
                $this.dsGridObat.getRange()[line].data.jml_stok_apt =b.json.jml_stok_apt;
                $this.gridObat.getView().refresh();
            }
        }
    });
    return $this.iComboObat;
}
function getPanelRWI_CPPT(mod_id) {
    var Field = ['PEKERJAAN','JENIS_KELAMIN','KD_DOKTER', 'NO_TRANSAKSI', 'KD_UNIT', 'KD_PASIEN', 'NAMA', 'NAMA_UNIT','KD_PAY','KD_SPESIAL', 
                'ALAMAT', 'TANGGAL_TRANSAKSI', 'NAMA_DOKTER', 'KD_CUSTOMER', 'CUSTOMER', 'URUT_MASUK', 
                'POSTING_TRANSAKSI', 'ANAMNESE', 'CAT_FISIK','namaunit','KD_UNIT_KAMAR','data_pulang',
                'NAMA_KAMAR','KELAS','KD_KELAS','NO_KAMAR','KD_KASIR','KETERANGAN','JAM_MASUK','TANGGAL_MASUK',
                'CARA_KELUAR','SEBAB_MATI','KEADAAN_PASIEN','BBL','RAWAT_GABUNG','HAK_KELAS'];
    dsTRKasirRWIList_panatajasarwi_cppt = new WebApp.DataStore({fields: Field});
    PengkajianLukaIGD.ds1 = dsTRKasirRWIList_panatajasarwi_cppt;
    RefreshPengkajianLukaIGD_cppt();
    /* getTotKunjunganRWI();
    var i = setInterval(function(){
        getTotKunjunganRWI();
    }, 100000);  */
    grListTRRWI_CPPT_RWI = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dsTRKasirRWIList_panatajasarwi_cppt,
        columnLines: true,
        autoScroll: false,
        border: true,
        flex:1,
        sort: false,
        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
        }),
        listeners: {
            rowdblclick: function (sm, ridx, cidx) {
                rowSelectedPJRWI_CPPT = dsTRKasirRWIList_panatajasarwi_cppt.getAt(ridx);
                PengkajianLukaIGD.s1        = PengkajianLukaIGD.ds1.getAt(ridx);
                if (rowSelectedPJRWI_CPPT != undefined) {
                    console.log(rowSelectedPJRWI_CPPT.data);
                    kd_unit=rowSelectedPJRWI_CPPT.data.KD_UNIT;
                    kd_pasien=rowSelectedPJRWI_CPPT.data.KD_PASIEN;
                    tgl_masuk=rowSelectedPJRWI_CPPT.data.TANGGAL_TRANSAKSI;
                    urut_masuk=rowSelectedPJRWI_CPPT.data.URUT_MASUK;
                    RWILookUpCPPT(rowSelectedPJRWI_CPPT.data);
                } else {
                    RWILookUpCPPT();
                }

                loaddataAssesmenLukaIGD(rowSelectedPJRWI_CPPT);
            }
        },
        cm: new Ext.grid.ColumnModel([
            new Ext.grid.RowNumberer(),
            {
                header: 'St. Posting',
                width: 50,
                sortable: false,
                hideable: true,
                hidden: false,
                menuDisabled: true,
                dataIndex: 'POSTING_TRANSAKSI',
                id: 'txtposting',
                renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                    switch (value) {
                        case 't':
                            metaData.css = 'StatusHijau';
                            break;
                        case 'f':
                            metaData.css = 'StatusMerah';
                            break;
                    }
                    return '';
                }
            }, {
                header: 'St. pulang',
                width: 50,
                sortable: false,
                hideable: true,
                hidden: false,
                menuDisabled: true,
                dataIndex: 'data_pulang',
                id: 'txtposting',
                renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                    switch (value) {
                        case 't':
                            metaData.css = 'StatusHijau';
                            break;
                        case 'f':
                            metaData.css = 'StatusMerah';
                            break;
                    }
                    return '';
                }
            }, {
                id: 'colReqIdViewRWI',
                header: 'No. Transaksi',
                dataIndex: 'NO_TRANSAKSI',
                sortable: false,
                hideable: false,
                menuDisabled: true,
                width: 80
            }, {
                id: 'colTglRWIViewRWI',
                header: 'Tgl Transaksi',
                dataIndex: 'TANGGAL_TRANSAKSI',
                sortable: false,
                hideable: false,
                menuDisabled: true,
                width: 75,
                renderer: function (v, params, record) {
                    return ShowDate(record.data.TANGGAL_TRANSAKSI);
                }
            }, {
                header: 'No. Medrec',
                width: 65,
                sortable: false,
                hideable: false,
                menuDisabled: true,
                dataIndex: 'KD_PASIEN',
                id: 'colRWIerViewRWI'
            }, {
                header: 'Pasien',
                width: 190,
                sortable: false,
                hideable: false,
                menuDisabled: true,
                dataIndex: 'NAMA',
                id: 'colRWIerViewRWI'
            }, {
                id: 'colLocationViewRWI',
                header: 'Alamat',
                dataIndex: 'ALAMAT',
                sortable: false,
                hideable: false,
                menuDisabled: true,
                width: 170
            }, {
                id: 'colDeptViewRWI',
                header: 'Pelaksana',
                dataIndex: 'NAMA_DOKTER',
                sortable: false,
                hideable: false,
                menuDisabled: true,
                width: 150
            },{
                id: 'colDeptViewRWI',
                header: 'Kamar',
                dataIndex: 'KETERANGAN',
                sortable: false,
                hideable: false,
                hidden:true,
                menuDisabled: true,
                width: 150
            },{
                id: 'colKdUnitKamar_PJRWI',
                header: 'Unit Kamar',
                dataIndex: 'KD_UNIT_KAMAR',
                sortable: false,
                hidden:true,
                hideable: true,
                menuDisabled: true,
                width: 150
            },
            
        ]),
        viewConfig: {forceFit: true},
    });
    PengkajianLukaIGD.grid1 = grListTRRWI_CPPT_RWI;
    var FormDepanCttpRWI    = new Ext.Panel({
        id: mod_id,
        closable: true,
        layout: {
            type:'vbox',
            align:'stretch'
        },
        style:'padding: 4px;',
        title: 'ASSESMEN PENGKAJIAN LUKA',
        border: false,
        autoScroll: false,
        items: [
            {
                xtype: 'panel',
                // plain: true,
                // border:false,
                height: 90,
                layout:'column',
                style:'padding-bottom: 4px;',
                defaults: {
                    bodyStyle: 'padding:4px',
                    autoScroll: true
                },
                items: [
                    {
                        layout: 'form',
                        border: false,
                        columnWidth:.3,
                        items: [
                            {
                                xtype: 'textfield',
                                fieldLabel:'No. Medrec ',
                                id: 'txtFilterNomedrec_panatajasarwi_cppt',
                                anchor: '100%',
                                onInit: function () { },
                                listeners: {
                                    'specialkey': function () {
                                        var tmpNoMedrec = Ext.get('txtFilterNomedrec_panatajasarwi_cppt').getValue();
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10) {
                                            if (tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10) {
                                                var tmpgetNoMedrec = formatnomedrec(Ext.get('txtFilterNomedrec_panatajasarwi_cppt').getValue());
                                                Ext.getCmp('txtFilterNomedrec_panatajasarwi_cppt').setValue(tmpgetNoMedrec);
                                                // var tmpkriteria = getCriteriaFilter_viDaftar();
                                                RefreshDataFilter_pengkajianLuka();
                                            } else {
                                                if (tmpNoMedrec.length === 10) {
                                                    RefreshDataFilter_pengkajianLuka();
                                                } else {
                                                    Ext.getCmp('txtFilterNomedrec_panatajasarwi_cppt').setValue('');
                                                }
                                            }
                                        }
                                    }
                                }
                            }, {
                                xtype: 'textfield',
                                fieldLabel: ' Pasien' + ' ',
                                id: 'TxtFilterGridDataView_NAMA_viKasirRwi_cppt',
                                enableKeyEvents: true,
                                listeners: {
                                    'specialkey': function () {
                                        if (Ext.EventObject.getKey() === 13) {
                                            RefreshDataFilter_pengkajianLuka();
                                        }
                                    }
                                }
                            }, {
                                xtype: 'textfield',
                                fieldLabel: ' Dokter' + ' ',
                                id: 'TxtFilterGridDataView_DOKTER_viKasirRwi_cppt',
                                enableKeyEvents: true,
                                listeners: {
                                    'specialkey': function () {
                                        if (Ext.EventObject.getKey() === 13) {
                                            RefreshDataFilter_pengkajianLuka();
                                        }
                                    }
                                }
                            },
                            
                        ]
                    },{
                        layout: 'form',
                        // margins: '0 5 5 0',
                        columnWidth:.7,
                        border: false,
                        items: [
                            getItemcombo_filter_PengkajianLukaIGD(), 
                            getItemPaneltgl_filter_PengkajianLukaIGD(),
                            mComboViewDataPJRWI_cppt(),
                        ]
                    }
                ]
            },
            grListTRRWI_CPPT_RWI,
            ]
    });
    // loaddataAssesmenLukaIGD(rowSelectedPJRWI_CPPT);
    // RefreshDataFilter_pengkajianLuka();
    return FormDepanCttpRWI;
}

function mComboViewDataPJRWI_cppt(){
    var mComboViewDataPJRWI_cppt = new Ext.form.ComboBox({
        width: 50,
        fieldLabel      : 'Jumlah data',
        id              : 'mComboViewDataPJRWI_cppt',
        typeAhead       : true,
        triggerAction   : 'all',
        lazyRender      : true,
        mode            : 'local',
        selectOnFocus   : true,
        forceSelection  : true,
        emptyText       : '',
        store: new Ext.data.ArrayStore({
            fields:['id',],
            data: [[5],[10],[25],[50],[100],]
        }),
        valueField: 'id',
        displayField: 'id',
        value: 10,
        listeners:{
            'select': function (a, b, c) {
                RefreshDataFilter_pengkajianLuka();
            }
        }
    });
    return mComboViewDataPJRWI_cppt;
}

function getItemPaneltgl_filter_PengkajianLukaIGD(){
    var items ={
        layout: 'column',
        border: false,
        items:[
            {
                columnWidth: .35,
                layout: 'form',
                labelWidth: 100,
                border: false,
                items:[
                    {
                        xtype: 'datefield',
                        fieldLabel: 'Tanggal ',
                        id: 'dtpTglAwalFilter_PengkajianLukaIGD_cppt',
                        name: 'dtpTglAwalFilter_PengkajianLukaIGD_cppt',
                        value: new Date().add(Date.DAY, -30),
                        width: 100,
                        format: 'd/M/Y',
                        altFormats: 'dmy',
                        listeners:{
                            'specialkey': function (){
                                var tmpNoMedrec = Ext.get('txtFilterNomedrec_panatajasarwi_cppt').getValue();
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9){
                                    RefreshDataFilter_pengkajianLuka();
                                }
                            }
                        }
                    }
                ]
            },
            {xtype: 'tbtext', text: ' s/d', cls: 'left-label', width: 30},
            {
                columnWidth: .35,
                layout: 'form',
                border: false,
                labelWidth: 1,
                items:[
                    {
                        xtype: 'datefield',
                        id: 'dtpTglAkhirFilter_PengkajianLukaIGD_cppt',
                        name: 'dtpTglAkhirFilter_PengkajianLukaIGD_cppt',
                        format: 'd/M/Y',
                        value: now,
                        width: 100,
                        listeners:{
                            'specialkey': function (){
                                var tmpNoMedrec = Ext.get('txtFilterNomedrec_panatajasarwi_cppt').getValue();
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10){
                                    RefreshDataFilter_pengkajianLuka();
                                }
                            }
                        }
                    }
                ]
            }
        ]
    };
    return items;
}

function getItemcombo_filter_PengkajianLukaIGD(){
    var items ={
        layout: 'column',
        border: false,
        items:[
            {
                columnWidth: .35,
                layout: 'form',
                labelWidth: 100,
                border: false,
                items:[ mComboStatusBayar_panatajasarwi_CPPT()]
            },
            // {xtype: 'tbtext', text: ' s/d', cls: 'left-label', width: 30},
            /*{
                columnWidth: .35,
                layout: 'form',
                border: false,
                labelWidth: 80,
                items:[
                    mCombokelas_kamar(),
                    mComboUnit_panatajasarwi()
                ]
            }*/
        ]
    };
    return items;
}
function RefreshPengkajianLukaIGD_cppt(){
    var tmp_date = new Date().add(Date.DAY, -30);
    dsTRKasirRWIList_panatajasarwi_cppt.load({
        params:{
            Skip: 10,
            Take: PengkajianLukaIGD.selectCount,
            // Sort: 'no_transaksi',
            Sort: '',
            // Sort: 'no_transaksi',
            Sortdir: 'ASC',
            target: 'ViewTrAskepIGD',
            param: " AND (tgl_transaksi between ~" + tmp_date.format('Y-m-d') + "~ and ~" + now.format('Y-m-d') + "~)"
        }
    });
    return dsTRKasirRWIList_panatajasarwi_cppt;
}
function RWILookUpCPPT(rowdata) {
    var lebar = 550;
    FormLookUpsdetailTRrwi_cttp = new Ext.Window({
        id: 'gridRWI',
        title: 'ASSESMEN PENGKAJIAN LUKA',
        closeAction: 'destroy',
        //maximized:true,
        border: false,
        width:850,
        height:580,
        resizable: false,
        plain: true,
        constrain: true,
        layout: 'fit',
        modal: true,
        items: getFormEntryTR_pengkajianLuka(lebar, rowdata),
        listeners: {
            close: function(){  
                RefreshDataFilter_pengkajianLuka();
            },
            'beforeclose': function(){clearInterval(i);}
        }
    });
    FormLookUpsdetailTRrwi_cttp.show();
    if (rowdata == undefined) {
        PengkajianLukaIGD_AddNew();
    } else {
        TRPenata_jasaInit(rowdata);
    }
}

function  TRPenata_jasaInit(rowdata){
    console.log(rowdata);
    Ext.getCmp('txt_kd_pasienPengkajianLuka').setValue(rowdata.KD_PASIEN);
    Ext.getCmp('txt_namaPasienPengkajianLuka').setValue(rowdata.NAMA);

}

function loaddataAssesmenLukaIGD(params, dokter = null){
    Ext.Ajax.request({
        url: baseURL +  "index.php/gawat_darurat/function_assesmen_luka/getDataAssesmet",
        params: {
            kd_unit      : kd_unit,
            kd_pasien    : kd_pasien,
            tgl_masuk    : tgl_masuk,
            urut_masuk   : urut_masuk,
        },
        success: function(response) {
            var cst  = Ext.decode(response.responseText);
            console.log(cst);
            if(cst.success===true){
                    Ext.getCmp('skala_nyeriPengkajianLuka').setValue(cst.listData[0].skala_nyeri);
                    Ext.getCmp('txt_jenisPengkajianLuka').setValue(cst.listData[0].jenis_luka);
                    Ext.getCmp('txt_deskripsiPengkajianLuka').setValue(cst.listData[0].keterangan);
                    Ext.getCmp('txt_panjangLukaPengkajianLuka').setValue(cst.listData[0].panjang);
                    Ext.getCmp('txt_lebarLukaPengkajianLuka').setValue(cst.listData[0].lebar);
                    Ext.getCmp('txt_kedalamanLukaPengkajianLuka').setValue(cst.listData[0].kedalaman);
                    Ext.getCmp('txt_warna_redPengkajianLuka').setValue(cst.listData[0].red);
                    Ext.getCmp('txt_yellow_PengkajianLuka').setValue(cst.listData[0].yellow);
                    Ext.getCmp('txt_black_PengkajianLuka').setValue(cst.listData[0].black);
                    Ext.getCmp('check_redresing').setValue(cst.listData[0].redresing);
                    Ext.getCmp('check_hecting').setValue(cst.listData[0].hecting);
                    Ext.getCmp('pnlPhoto_viDtPgw').setFoto(cst.listData[0].foto_before);
                    Ext.getCmp('pnlPhoto_viDtPgw2').setFoto(cst.listData[0].foto_after);
                    Ext.getCmp('txt_hectingPengkajianLuka').setValue(cst.listData[0].ket_hecting);
            }
         
        }
    });
}


function mComboStatusBayar_panatajasarwi_CPPT() {
    var cboStatus_panatajasarwi_cppt = new Ext.form.ComboBox({
        id: 'cboStatus_panatajasarwi_cppt',
        typeAhead: true,
        triggerAction: 'all',
        lazyRender: true,
        mode: 'local',
        anchor: '96.8%',
        emptyText: '',
        fieldLabel: 'Status Pulang',
        store: new Ext.data.ArrayStore({
            id: 0,
            fields: ['Id', 'displayText'],
            data: [[1, 'Semua'], [2, 'Pulang'], [3, 'Belum Pulang']]
        }),
        valueField: 'Id',
        displayField: 'displayText',
        value: PengkajianLukaIGD.selectCountStatus_bayar,
        listeners: {
            select: function (a, b, c) {
                PengkajianLukaIGD.selectCountStatus_bayar = b.data.displayText;
                RefreshDataFilter_pengkajianLuka();
            }
        }
    });
    return cboStatus_panatajasarwi_cppt;
}

function RefreshDataFilter_pengkajianLuka(){
    var KataKunci = '';
    loadMask.show();
    if (Ext.getCmp('txtFilterNomedrec_panatajasarwi_cppt').getValue() != ''){
        if (KataKunci == ''){
            KataKunci = ' and   LOWER(p.kd_pasien) like  LOWER( ~' + Ext.get('txtFilterNomedrec_panatajasarwi_cppt').getValue() + '%~)';
        } else{
            KataKunci += ' and  LOWER(p.kd_pasien) like  LOWER( ~' + Ext.get('txtFilterNomedrec_panatajasarwi_cppt').getValue() + '%~)';
        }
    }
    if (Ext.getCmp('TxtFilterGridDataView_NAMA_viKasirRwi_cppt').getValue() != ''){
        if (KataKunci == ''){
            KataKunci = ' and   LOWER(p.nama) like  LOWER( ~%' + Ext.get('TxtFilterGridDataView_NAMA_viKasirRwi_cppt').getValue() + '%~)';
        } else{
            KataKunci += ' and  LOWER(p.nama) like  LOWER( ~%' + Ext.get('TxtFilterGridDataView_NAMA_viKasirRwi_cppt').getValue() + '%~)';
        }
    }
    /*if (Ext.getCmp('cboStatus_panatajasarwi_cppt').getValue() == 2 || Ext.getCmp('cboStatus_panatajasarwi_cppt').getValue() == 'Posting'){
        KataKunci += ' and t.posting_transaksi =  ~true~';
    }else if (Ext.getCmp('cboStatus_panatajasarwi_cppt').getValue() == 3 || Ext.getCmp('cboStatus_panatajasarwi_cppt').getValue() == 'Belum Posting') {
        KataKunci += ' and t.posting_transaksi =  ~false~';
    }*/
    if (Ext.getCmp('cboStatus_panatajasarwi_cppt').getValue() == 2 || Ext.getCmp('cboStatus_panatajasarwi_cppt').getValue() == 'Pulang'){
        KataKunci += ' and ( k.tgl_keluar is not null  AND k.jam_keluar is not null) ';
    }else if (Ext.getCmp('cboStatus_panatajasarwi_cppt').getValue() == 3 || Ext.getCmp('cboStatus_panatajasarwi_cppt').getValue() == 'Belum Pulang') {
        KataKunci += ' and ( k.tgl_keluar is null  AND k.jam_keluar is null) ';
    }
    /*if (Ext.get('cboStatuslunas_viKasirrwiKasir').getValue() == 'Semua'){
        if (KataKunci == ''){
            KataKunci = ' and (t.lunas = ~false~ or  t.lunas = ~true~)';
        }else{
            KataKunci += ' and (t.lunas = ~false~ or  t.lunas = ~true~)';
        };
    };*/
    /*if (Ext.getCmp('cboStatus_panatajasarwi_cppt').getValue() == 'Belum Lunas'){
        if (KataKunci == ''){
            KataKunci = ' and  t.lunas = ~false~';
        }else{
            KataKunci += ' and t.lunas =  ~false~';
        };
    };*/
    if (Ext.getCmp('dtpTglAwalFilter_PengkajianLukaIGD_cppt').getValue() != ''){
        if (KataKunci == ''){
            KataKunci = " and (t.tgl_transaksi between ~" + Ext.get('dtpTglAwalFilter_PengkajianLukaIGD_cppt').getValue() + "~ and ~" + Ext.get('dtpTglAkhirFilter_PengkajianLukaIGD_cppt').getValue() + "~)";
        } else{

            KataKunci += " and (t.tgl_transaksi between ~" + Ext.get('dtpTglAwalFilter_PengkajianLukaIGD_cppt').getValue() + "~ and ~" + Ext.get('dtpTglAkhirFilter_PengkajianLukaIGD_cppt').getValue() + "~)";
        }
    }
    if (KataKunci != undefined || KataKunci != ''){
        dsTRKasirRWIList_panatajasarwi_cppt.load({
            params:{
                Skip: Ext.getCmp('mComboViewDataPJRWI_cppt').getValue(),
                Take: selectCountPJRwi,
                //Sort: 'no_transaksi',
                Sort: 'tgl_transaksi',
                //Sort: 'no_transaksi',
                Sortdir: 'DESC',
                // target: 'ViewKasirRWI',
                target: 'ViewPJRWI',
                param: KataKunci
            },
            callback: function(){
                loadMask.hide();
            }
        });
    } else{
        dsTRKasirRWIList_panatajasarwi_cppt.load({
            params:{
                Skip: Ext.getCmp('mComboViewDataPJRWI_cppt').getValue(),
                Take: selectCountPJRwi,
                //Sort: 'no_transaksi',
                Sort: 'tgl_transaksi',
                //Sort: 'no_transaksi',
                Sortdir: 'DESC',
                // target: 'ViewKasirRWI',
                target: 'ViewPJRWI',
                param: KataKunci
            },
            callback: function(){
                loadMask.hide();
            }
        });
    }
    return dsTRKasirRWIList_panatajasarwi_cppt;
}


function AddNewCPPT_RWI(){
  var x=true;
    if (x === true){
        var p = RecordBaru_CTTP_RWI();
        dataSource_viCPPT_RWI.insert(dataSource_viCPPT_RWI.getCount(), p);
        var row =dataSource_viCPPT_RWI.getCount()-1;
        PengkajianLukaIGD.gridhasil_lab_PJRWI_CPPT.startEditing(row, 3);
        lineCttpRWI=row;
    };
    
};

function RecordBaru_CTTP_RWI(){
    var p = new PengkajianLukaIGD.mRecordRwiCTTP({
                        'waktu': now.format("Y-m-d H:i:s"),
                        'bagian': '',
                        'hasil': '',
                        'verifikasi': '',
                        'instruksi': '',
                    });
    return p;
};
function getFormEntryTR_pengkajianLuka(lebar, data) {
    var pnlTR_panatajasarwi_CPPT = new Ext.FormPanel({
        id: 'PanelTR_CPPT_RWI',
        layout: 'fit',
        bodyStyle: 'padding:4px;',
        border: false,
        items: [GetFormPengkajianLuka(lebar)], // langsung gridnya
        tbar: [

            {
                xtype:'button',
                text: 'AddNew',
                iconCls: 'add',
                arrowAlign:'right',
                hidden:true,
                handler: function(){
                  //  AddNewCPPT_RWI()
                }
            },
            {
                xtype:'button',
                text: 'Save',
                iconCls: 'save',
                arrowAlign:'right',
                handler: function(){
                    SimpanAssesmenLuka();
                     // DISINI
                }
                       
            },
            {
                xtype:'button',
                text: 'Detele',
                iconCls: 'RemoveRow',
                arrowAlign:'right',
                handler: function(){
                        Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
                                if (button == 'yes'){
                                    // loadMask.show();
                                    if(Ext.getCmp('skala_nyeriPengkajianLuka').getValue()!='') {
                                            Ext.Ajax.request({
                                                    url: baseURL + "index.php/gawat_darurat/function_assesmen_luka/delete",
                                                    params: {
                                                        kd_pasien     : Ext.getCmp('txt_kd_pasienPengkajianLuka').getValue(),
                                                        kd_unit       : kd_unit,
                                                        tgl_masuk     : tgl_masuk,
                                                        urut_masuk    : urut_masuk
                                                    },
                                                    failure: function(o){
                                                        ShowPesanError_PengkajianLukaIGD('Error Hapus. Hubungi Admin!', 'Gagal');
                                                    },  
                                                    success: function(o) {
                                                        var cst = Ext.decode(o.responseText);
                                                        if (cst.success === true) {
                                                            loadMask.hide();
                                                            ShowPesanInfoAssesmenLukaIGD('Data Berhasil Di hapus', 'Sukses');
                                                            FormLookUpsdetailTRrwi_cttp.close();
                                                        }else {
                                                           ShowPesanError_PengkajianLukaIGD('Data Tidak Bisa Di Hapus Harap Hubungi Admin', 'Gagal');
                                                        };
                                                    }
                                                }
                                            )
                                        }
                                }
                            });
                }
            },
            {
                text: 'Cetak',
                iconCls: 'print',
                handler: function () {
                    var url_laporan    = baseURL + "index.php/gawat_darurat/function_assesmen_luka";
                    var params         = {
                                kd_pasien     : Ext.getCmp('txt_kd_pasienPengkajianLuka').getValue(),
                                kd_unit       : kd_unit,
                                tgl_masuk     : tgl_masuk,
                                urut_masuk    : urut_masuk
                    } ;
                    console.log(params);
                    var form        = document.createElement("form");
                    form.setAttribute("method", "post");
                    form.setAttribute("target", "_blank");
                    form.setAttribute("action", url_laporan+"/cetak");
                    var hiddenField = document.createElement("input");
                    hiddenField.setAttribute("type", "hidden");
                    hiddenField.setAttribute("name", "data");
                    hiddenField.setAttribute("value", Ext.encode(params));
                    form.appendChild(hiddenField);
                    document.body.appendChild(form);
                    form.submit();   
                }
            },
        ]
    });
    var x;
    var GDtabDetail_panatajasarwi_cppt = new Ext.TabPanel({
        id: 'GDtabDetail_panatajasarwi_cppt',
        activeTab: 0,
        flex:1,
        border: true,
        style:'padding: 0px 4px 4px 4px;',
        defaults: {
            autoScroll: false
        },
        items: [

        ],
        listeners: {
            tabchange: function (panel, tab) {
                
            }
        }
    });
    var FormDepanCttpRWI = new Ext.Panel({
        id: 'FormDepanCttpRWI',
        layout: {
            type:'vbox',
            align:'stretch'
        },
        border: true,
        bodyStyle: 'background:#FFFFFF;',
        items: [pnlTR_panatajasarwi_CPPT, GDtabDetail_panatajasarwi_cppt]
    });
    return FormDepanCttpRWI;
}

function GetFormPengkajianLuka(){
    var items = 
    {
        layout:'form',
        border: false,
        height:830,
        width:1000,  
        bodyStyle:'padding: 5px',
        items:
        [
            {
                layout: 'column',
                border: false,
                items:
                [
                    {
                        columnWidth:.98,
                        layout: 'absolute',
                        bodyStyle: 'padding: 10px 10px 10px 10px',
                        border: false,
                        width: 500,
                        height:1000,
                        anchor: '100% 100%',
                        items:
                        [
                            {
                        columnWidth:.98,
                        layout: 'absolute',
                        bodyStyle: 'padding: 10px 10px 10px 10px',
                        border: false,
                        width: 500,
                        height: 500,
                        anchor: '100% 100%',
                        items:
                        [
                            
                            {
                                    xtype: 'fieldset',
                                    title: 'Informasi',
                                    autoHeight: true,
                                    width: '850px',
                                    bodyStyle: 'margin-left: 10px',
                                    items: 
                                    [
                                     {
                                        columnWidth:.98,
                                        layout: 'absolute',
                                        bodyStyle: 'padding: 10px 10px 10px 10px',
                                        border: false,
                                        // width: 500,
                                        height: 55,
                                        anchor: '100% 100%',
                                        items:
                                        [
                                            {
                                                x: 10,
                                                y: 0,
                                                xtype: 'label',
                                                text: 'No Medrec'
                                            },
                                            {
                                                x: 120,
                                                y: 0,
                                                xtype: 'label',
                                                text: ':'
                                            },
                                            {
                                                x: 130,
                                                y: 0,
                                                xtype: 'textfield',
                                                id: 'txt_kd_pasienPengkajianLuka',
                                                name: 'txt_kd_pasienPengkajianLuka',
                                                width: 200,
                                                allowBlank: false,
                                                readOnly: true,
                                                disabled:true,
                                                maxLength:3,
                                                tabIndex:1,
                                                listeners:
                                                { 
                                                    'specialkey' : function()
                                                    {
                                                        if (Ext.EventObject.getKey() === 13) 
                                                        {
                                                            
                                                        }                       
                                                    },
                                                    blur: function(a){
                                                        
                                                    }
                                                }
                                            },
                                            
                                            {
                                                x: 10,
                                                y: 30,
                                                xtype: 'label',
                                                text: 'Nama Pasien'
                                            },
                                            {
                                                x: 120,
                                                y: 30,
                                                xtype: 'label',
                                                text: ':'
                                            },
                                            {
                                                x:130,
                                                y:30,
                                                xtype: 'textfield',
                                                id: 'txt_namaPasienPengkajianLuka',
                                                name: 'txt_namaPasienPengkajianLuka',
                                                width: 200,
                                                allowBlank: false,
                                                readOnly:true,
                                                maxLength:3,
                                                tabIndex:1,
                                                listeners:
                                                { 
                                                    'specialkey' : function()
                                                    {
                                                        if (Ext.EventObject.getKey() === 13) 
                                                        {
                                                            
                                                        }                       
                                                    },
                                                    blur: function(a){
                                                        
                                                    }
                                                }
                                            },

                                            {
                                                x: 350,
                                                y: 0,
                                                xtype: 'label',
                                                text: 'Tanggal'
                                            },
                                            {
                                                x: 410,
                                                y: 0,
                                                xtype: 'label',
                                                text: ':'
                                            },                            
                                            {
                                                xtype: 'datefield',
                                                x: 420,
                                                y: 0,
                                                id: 'dtpTanggalPengkajianLuka',
                                                name: 'dtpTanggalPengkajianLuka',
                                                format: 'd/M/Y',
                                                readOnly: false,
                                                width: 200,
                                                value: now
                                               
                                            },
                                            {
                                                x: 350,
                                                y: 30,
                                                xtype: 'label',
                                                text: 'Jam'
                                            },
                                            {
                                                x: 410,
                                                y: 30,
                                                xtype: 'label',
                                                text: ':'
                                            },    
                                            {
                                                x:420,
                                                y:30,
                                                id: 'txt_jamPengkajianLuka',
                                                xtype: 'timefield',
                                                format: 'H:i:s',
                                                value : now.format('H:i:s'),
                                                width: 200,
                                            }, 
                                        ]
                                    }   
                                    ]
                            },

                                {
                    xtype: 'fieldset',
                    title: 'Keterangan',
                    autoHeight: false,
                    x:0,
                    y:90,
                    height:400,
                    width: 400,
                    bodyStyle: 'margin-left: 10px',
                    items: 
                    [
                     {
                        columnWidth:.10,
                        layout: 'absolute',
                        bodyStyle: 'padding: 10px 10px 10px 10px',
                        border: false,
                        // width: 500,
                        height: 445,
                   //     anchor: '100% 100%',
                        items:
                        [
                           {
                                x: 0,
                                y: 0,
                                xtype: 'label',
                                text: 'Skala Nyeri'
                            },
                            {
                                x: 120,
                                y: 0,
                                xtype: 'label',
                                text: ':'
                            },
                            {
                                xtype: 'combo',
                                x: 130,
                                y: 0,
                                //fieldLabel: 'Kelompok Pasien',
                                id: 'skala_nyeriPengkajianLuka',
                                editable: true,
                                store: new Ext.data.ArrayStore({
                                    id: 0,
                                    fields:[
                                        'Id',
                                        'displayText'
                                    ],
                                    data: [[0,'0'],[1, '1'], [2, '2'], [3, '3'],[4, '4'], [5, '5'], [6, '6'],[7, '7'], [8, '8'], [9, '9'],[10,'10']]
                                }),
                                valueField: 'Id',
                                displayField: 'displayText',
                                mode: 'local',
                                width: 150,
                                triggerAction: 'all',
                                emptyText: 'Pilih Salah Satu...',
                                selectOnFocus: true,
                                tabIndex: 10,
                                //anchor: '99%',
                                disabled : false,
                                enableKeyEvents:true,
                                listeners:{
                                    'select': function (a, b, c){
                                        
                                    },
                                }
                             },
                             {
                                x: 0,
                                y: 30,
                                xtype: 'label',
                                text: 'Luka Sebelum Dibersihkan'
                             },
                             {
                                xtype: 'panel',
                                title: '',
                                width: 150,
                                x: 160,
                                y: 30,
                                height: 200,
                                layout: 'vbox',
                                region: 'center',
                                border: false,
                                items: [
                                    {
                                        title:'Foto',
                                        border:false,
                                        items:[
                                            {
                                                xtype:'ifoto',
                                                folder:'',
                                                id:'pnlPhoto_viDtPgw'
                                            }
                                        ]
                                    },
                               
                                ]
                            },
                           /*  {
                                xtype: 'fileuploadfield',
                                x: 120,
                                y: 20,
                                id: 'fileBefore',
                                emptyText: 'Luka Sebelum Dibersihkan',
                                fieldLabel: 'File Dokumentasi',
                                anchor: '80%',
                                name: 'file_dokumentasi',
                                buttonCfg: {
                                    text: '',
                                    iconCls: 'upload-icon'
                                 }
                             },    */
                             {
                                x: 0,
                                y: 170,
                                xtype: 'label',
                                text: 'Luka Sesudah Dibersihkan'
                             },
                             {
                                xtype: 'panel',
                                title: '',
                                width: 150,
                                x: 160,
                                y: 170,
                                height: 200,
                                layout: 'vbox',
                                region: 'center',
                                border: false,
                                items: [
                                    {
                                        title:'Foto',
                                        border:false,
                                        items:[
                                            {
                                                xtype:'ifoto',
                                                folder:'',
                                                id:'pnlPhoto_viDtPgw2'
                                            }
                                        ]
                                    },
                               
                                ]
                             },
                             {
                                x: 0,
                                y: 300,
                                xtype: 'label',
                                text: 'Jenis Luka'
                             },
                             {
                                x: 120,
                                y: 300,
                                xtype: 'label',
                                text: ':'
                             },
                             {
                                x: 130,
                                y: 300,
                                xtype: 'textfield',
                                id: 'txt_jenisPengkajianLuka',
                                name: 'txt_jenisPengkajianLuka',
                                width: 200,
                                allowBlank: false,
                                readOnly: false,
                                disabled:false,
                                maxLength:3,
                                tabIndex:1,
                             },
                             {
                                x: 0,
                                y: 330,
                                xtype: 'label',
                                text: 'Deskripsi'
                             },
                             {
                                x: 120,
                                y: 330,
                                xtype: 'label',
                                text: ':'
                             },
                             {
                                x: 130,
                                y: 330,
                                xtype: 'textfield',
                                id: 'txt_deskripsiPengkajianLuka',
                                name: 'txt_deskripsiPengkajianLuka',
                                width: 200,
                                allowBlank: false,
                                readOnly: false,
                                disabled:false,
                                maxLength:3,
                                tabIndex:1,
                             },
                        ]
                     }   
                    ]
                },
                            

            
                             {
                                xtype: 'fieldset',
                                x:415,
                                y:250,
                                title: 'Ukuran Luka',
                                autoHeight: false,
                                width: 365,
                                bodyStyle: 'margin-left: 10px',
                                items: 
                                [
                                 {
                                    columnWidth:.10,
                                    layout: 'absolute',
                                    bodyStyle: 'padding: 10px 90px 10px 10px',
                                    border: false,
                                    // width: 500,
                                    height: 85,
                                //    anchor: '100% 100%',
                                    items:
                                    [
                                         {
                                            x: 0,
                                            y: 0,
                                            xtype: 'label',
                                            text: 'Panjang Luka (cm)'
                                         },
                                         {
                                            x: 120,
                                            y: 0,
                                            xtype: 'label',
                                            text: ':'
                                         },
                                         {
                                            x: 130,
                                            y: 0,
                                            xtype: 'textfield',
                                            id: 'txt_panjangLukaPengkajianLuka',
                                            name: 'txt_panjangLukaPengkajianLuka',
                                            width: 200,
                                            allowBlank: false,
                                            readOnly: false,
                                            disabled:false,
                                            maxLength:3,
                                            tabIndex:1,
                                         },
                                         {
                                            x: 0,
                                            y: 30,
                                            xtype: 'label',
                                            text: 'Lebar Luka (cm)'
                                         },
                                         {
                                            x: 120,
                                            y: 30,
                                            xtype: 'label',
                                            text: ':'
                                         },
                                         {
                                            x: 130,
                                            y: 30,
                                            xtype: 'textfield',
                                            id: 'txt_lebarLukaPengkajianLuka',
                                            name: 'txt_lebarLukaPengkajianLuka',
                                            width: 200,
                                            allowBlank: false,
                                            readOnly: false,
                                            disabled:false,
                                            maxLength:3,
                                            tabIndex:1,
                                         },
                                         {
                                            x: 0,
                                            y: 60,
                                            xtype: 'label',
                                            text: 'Kedalaman Luka (cm)'
                                         },
                                         {
                                            x: 120,
                                            y: 60,
                                            xtype: 'label',
                                            text: ':'
                                         },
                                         {
                                            x: 130,
                                            y: 60,
                                            xtype: 'textfield',
                                            id: 'txt_kedalamanLukaPengkajianLuka',
                                            name: 'txt_kedalamanLukaPengkajianLuka',
                                            width: 200,
                                            allowBlank: false,
                                            readOnly: false,
                                            disabled:false,
                                            maxLength:3,
                                            tabIndex:1,
                                         },
                                    ]
                                 }   
                                ]
                             },

                                {
                                    xtype: 'fieldset',
                                    title: 'Warna Luka',
                                    x:415,
                                    y:370,
                                    autoHeight: false,
                                    width: 365,
                                    bodyStyle: 'margin-left: 10px',
                                    items: 
                                    [
                                     {
                                        columnWidth:.10,
                                        layout: 'absolute',
                                        bodyStyle: 'padding: 10px 10px 10px 10px',
                                        border: false,
                                        // width: 500,
                                        height: 85,
                                      //  anchor: '100% 100%',
                                        items:
                                        [
                                            
                                             {
                                                x: 0,
                                                y: 0,
                                                xtype: 'label',
                                                text: 'Red (%)'
                                             },
                                             {
                                                x: 120,
                                                y: 0,
                                                xtype: 'label',
                                                text: ':'
                                             },
                                             {
                                                x: 130,
                                                y: 0,
                                                xtype: 'textfield',
                                                id: 'txt_warna_redPengkajianLuka',
                                                name: 'txt_warna_redPengkajianLuka',
                                                width: 200,
                                                allowBlank: false,
                                                readOnly: false,
                                                disabled:false,
                                                maxLength:3,
                                                tabIndex:1,
                                             },
                                             {
                                                x: 0,
                                                y: 30,
                                                xtype: 'label',
                                                text: 'Yellow (%)'
                                             },
                                             {
                                                x: 120,
                                                y: 30,
                                                xtype: 'label',
                                                text: ':'
                                             },
                                             {
                                                x: 130,
                                                y: 30,
                                                xtype: 'textfield',
                                                id: 'txt_yellow_PengkajianLuka',
                                                name: 'txt_yellow_PengkajianLuka',
                                                width: 200,
                                                allowBlank: false,
                                                readOnly: false,
                                                disabled:false,
                                                maxLength:3,
                                                tabIndex:1,
                                             },
                                             {
                                                x: 0,
                                                y: 60,
                                                xtype: 'label',
                                                text: 'Black (%)'
                                             },
                                             {
                                                x: 120,
                                                y: 60,
                                                xtype: 'label',
                                                text: ':'
                                             },
                                             {
                                                x: 130,
                                                y: 60,
                                                xtype: 'textfield',
                                                id: 'txt_black_PengkajianLuka',
                                                name: 'txt_black_PengkajianLuka',
                                                width: 200,
                                                allowBlank: false,
                                                readOnly: false,
                                                disabled:false,
                                                maxLength:3,
                                                tabIndex:1,
                                             },
                                        ]
                                     }   
                                    ]
                                },
                                {
                                    xtype: 'fieldset',
                                    x:415,
                                    y:90,
                                    title: 'Tindakan Yang Diberikan',
                                    autoHeight: false,
                                    height:160,
                                    width: 365,
                                    bodyStyle: 'margin-left: 10px',
                                    items: 
                                    [
                                     {
                                        columnWidth:.10,
                                        layout: 'absolute',
                                        bodyStyle: 'padding: 10px 10px 10px 10px',
                                        border: false,
                                        // width: 500,
                                        height: 385,
                                     //   anchor: '100% 100%',
                                        items:
                                        [
                                            {
                                                x: 10,
                                                y: 5,
                                                xtype: 'checkboxgroup',
                                                columns: 1,
                                                items: [
                                                    {boxLabel: 'Redresing', name: 'check_redresing',id:'check_redresing'},
                                                    {
	                                                     boxLabel: 'Hecting ',
	                                                     name: 'check_hecting',
	                                                     id: 'check_hecting',
	                                                     listeners: {
															check: function(){
																console.log(Ext.getCmp('check_hecting').getValue()===true);
															    if(Ext.getCmp('check_hecting').getValue()===true){
																	Ext.getCmp('txt_hectingPengkajianLuka').enable();
																}
																else{
																	Ext.getCmp('txt_hectingPengkajianLuka').disable();
																}
															}
													   }
	                                                 }
                                                ],
                                                
                                            },
                                            {
                                                x: 10,
                                                y: 55,
                                                xtype: 'textfield',
                                                id: 'txt_hectingPengkajianLuka',
                                                name: 'txt_hectingPengkajianLuka',
                                                width: 200,
                                                disabled:true,
                                                maxLength:3,
                                                tabIndex:1,
                                             },
                                               {
                                                x: 220,
                                                y: 55,
                                                xtype: 'label',
                                                text: 'Jahitan'
                                             },
                                          
                                        ]
                                     }   
                                    ]
                                },

                        ]
                    }
                        ]
                    }
                ]
            },
        ]       
    };
        return items;
}

function SimpanAssesmenLuka(){
    if (ValidasiSaveAssesmenLukaIGD(nmHeaderSimpanData,false) == 1 ){
        Ext.Ajax.request({
                url: baseURL + "index.php/gawat_darurat/function_assesmen_luka/simpan",
                params: getParamSaveAssesmenLuka(),
                failure: function(o){
                    loadMask.hide();
                    ShowPesanError_PengkajianLukaIGD('Hubungi Admin', 'Error');
                },  
                success: function(o) {
                    var cst = Ext.decode(o.responseText);
                    if (cst.success === true) {
                        loadMask.hide();
                        ShowPesanInfoAssesmenLukaIGD('Berhasil menyimpan data ini','Information');                        
                    }else {
                        loadMask.hide();
                        ShowPesanInfoAssesmenLukaIGD('Gagal menyimpan data ini', 'Error');
                    }
                }
        });

    }
}
function getParamSaveAssesmenLuka(){
    var params =
    {
        kd_pasien       :Ext.getCmp('txt_kd_pasienPengkajianLuka').getValue(),
        kd_unit         :kd_unit,
        tgl_masuk       :tgl_masuk,
        urut_masuk      :urut_masuk,
        skala_nyeri     :Ext.getCmp('skala_nyeriPengkajianLuka').getValue(),
        foto_before     :Ext.getCmp('pnlPhoto_viDtPgw').result,
        foto_after      :Ext.getCmp('pnlPhoto_viDtPgw2').result,
        jenis_luka      :Ext.getCmp('txt_jenisPengkajianLuka').getValue(),
        deskripsi       :Ext.getCmp('txt_deskripsiPengkajianLuka').getValue(),
        panjang_luka    :Ext.getCmp('txt_panjangLukaPengkajianLuka').getValue(),
        lebar_luka      :Ext.getCmp('txt_lebarLukaPengkajianLuka').getValue(),
        kedalaman_luka  :Ext.getCmp('txt_kedalamanLukaPengkajianLuka').getValue(),
        red_luka        :Ext.getCmp('txt_warna_redPengkajianLuka').getValue(),
        yellow_luka     :Ext.getCmp('txt_yellow_PengkajianLuka').getValue(),
        black_luka      :Ext.getCmp('txt_black_PengkajianLuka').getValue(),
        redresing       :Ext.getCmp('check_redresing').getValue(),
        hecting         :Ext.getCmp('check_hecting').getValue(),
        ket_hecting     :Ext.getCmp('txt_hectingPengkajianLuka').getValue(),
        
    }
    return params
};
function ValidasiSaveAssesmenLukaIGD(modul,mBolHapus){
    var x = 1;
    if(Ext.getCmp('skala_nyeriPengkajianLuka').getValue() ==='' ){
        loadMask.hide();
        ShowPesanWarningAssesmenLukaIGD('SKala Nyeri Belum Dipilih!', 'Warning');
        x = 0;
    }
    else if(Ext.getCmp('pnlPhoto_viDtPgw').result==''){
        loadMask.hide();
        ShowPesanWarningAssesmenLukaIGD('Foto Luka Sebelum Diberikan Masih Kosong!', 'Warning');
        x = 0;
    }
    else if(Ext.getCmp('pnlPhoto_viDtPgw2').result===''){
        loadMask.hide();
        ShowPesanWarningAssesmenLukaIGD('Foto Luka Sesudah Diberikan Masih Kosong!', 'Warning');
        x = 0;
    }
    else if(Ext.getCmp('txt_jenisPengkajianLuka').getValue()===''){
        loadMask.hide();
        ShowPesanWarningAssesmenLukaIGD('Jenis Luka Masih Kosong!', 'Warning');
        x = 0;
    }
    else if(Ext.getCmp('txt_panjangLukaPengkajianLuka').getValue()===''){
        loadMask.hide();
        ShowPesanWarningAssesmenLukaIGD('Panjang Luka Belum Diisi!', 'Warning');
        x = 0;
    }
    else if(Ext.getCmp('txt_lebarLukaPengkajianLuka').getValue()===''){
        loadMask.hide();
        ShowPesanWarningAssesmenLukaIGD('Lebar Luka Belum Diisi!', 'Warning');
        x = 0;
    }
    else if(Ext.getCmp('txt_kedalamanLukaPengkajianLuka').getValue()===''){
        loadMask.hide();
        ShowPesanWarningAssesmenLukaIGD('Kedalaman Luka Belum Diisi!', 'Warning');
        x = 0;
    }
    return x;
};
function ShowPesanWarningAssesmenLukaIGD(str, modul) {
    Ext.MessageBox.show
    (
        {
            title: modul,
            msg: str,
            buttons: Ext.MessageBox.OK,
            icon: Ext.MessageBox.WARNING,
            width:250
        }
    );
};

function dataGridItem_CPPT_RWI(){
    Ext.Ajax.request({
            url: baseURL + "index.php/rawat_inap/function_CPPT_RWI/get_data",
            params: {
                kd_pasien   : rowSelectedPJRWI_CPPT.data.KD_PASIEN,
                kd_unit     : rowSelectedPJRWI_CPPT.data.KD_UNIT,
                tgl_masuk   : rowSelectedPJRWI_CPPT.data.TANGGAL_MASUK,
                urut_masuk  : rowSelectedPJRWI_CPPT.data.URUT_MASUK,
            },
            failure: function(o){
                ShowPesanError_PengkajianLukaIGD('Hubungi Admin', 'Error');
            },  
            success: function(o) {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true) {
                    var recs=[],
                        recType=dataSource_viCPPT_RWI.recordType;
                        
                    for(var i=0; i<cst.ListDataObj.length; i++){
                        recs.push(new recType(cst.ListDataObj[i]));
                    }
                    dataSource_viCPPT_RWI.add(recs);
                //    GridDataView_viCPPT_RWITest.getView().refresh();
                }
                else {
                    ShowPesanError_PengkajianLukaIGD('Gagal membaca data CPPT', 'Error');
                }
            }
        }
        
    )   
}

function ShowPesanInfoAssesmenLukaIGD(str, modul) {
  Ext.MessageBox.show({
        title: modul,
        msg: str,
        buttons: Ext.MessageBox.OK,
        icon: Ext.MessageBox.INFO,
        width:250
    });
};
