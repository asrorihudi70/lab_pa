// Data Source ExtJS

/**
*	Nama File 		: TRInformasiTarif.js
*	Menu 			: Pendaftaran
*	Model id 		: 030103
*	Keterangan 		: Untuk View Informasi Tarif
*	Di buat tanggal : 15 April 2014
*	Oleh 			: SDY_RI
*/

// Deklarasi Variabel pada ExtJS
var kode_tarif;
var kode_produk;
var kode_unit;
var tgl_berlaku;
var dsInfotarifList;
var rowSelected_viKasirIgd;
var selectCountInfoTarif=1;
var rowSelectedInfotarif;
var kd_prod;
var dsDataUnsur_viInformasiUnitdokter;
var dataSource_viInformasiUnitdokter;
var selectCount_viInformasiUnitdokter=50;
var NamaForm_viInformasiUnitdokter="Informasi Tarif";
var icons_viInformasiUnitdokter="Gaji";
var addNew_viInformasiUnitdokter;
var rowSelected_viInformasiUnitdokter;
var rowSelected_viInformasiUnitdokterList;
var setLookUps_viInformasiUnitdokter;
var setLookUps_viInformasiUnitdokterList;
var now_viInformasiUnitdokter = new Date();

var BlnIsDetail;
var SELECTDATAPENILAIANPGW; // cek lagi

var CurrentData_viInformasiUnitdokter =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viInformasiUnitdokter(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

// Start Shortcut Key
// Kegunaan : Tombol Cepat untuk Keyboard
	Ext.getDoc().on('keypress', function(event, target) {
    if (event.altKey && !event.shiftKey)
	{
        event.stopEvent();
    /*if (Ext.EventObject.getKey() === 18 && !Ext.EventObject.getKey() === 16) {
        Ext.EventObject.stopEvent();*/

        switch(event.getKey()) {
        //switch(Ext.EventObject.getKey()) {

            case event.F1 :
            		dataaddnew_viInformasiUnitdokter();
            	break;

            case event.F2 :
            		datasave_viInformasiUnitdokter(false);
					datarefresh_viInformasiUnitdokter();
				break;

            case event.F3 : // Alt + F3 untuk Edit
	                if (rowSelected_viInformasiUnitdokter === undefined)
	                {
	                    setLookUp_viInformasiUnitdokter();
	                }
	                else
	                {
	                    setLookUp_viInformasiUnitdokter(rowSelected_viInformasiUnitdokter.data);
	                }
                break;

            case event.F5 :
            		var x = datasave_viInformasiUnitdokter(true);
							datarefresh_viInformasiUnitdokter();
							if (x===undefined)
							{
								setLookUps_viInformasiUnitdokter.close();
							}
				break;

			case event.F10 :
					alert("Delete")
				break;

            // other cases...
        }
        
    }
});
// End Shortcut Key

// Start Project Informasi Tarif

/**
*	Function : dataGrid_viInformasiUnitdokter
*	
*	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
*/
function dataGrid_viInformasiUnitdokter(mod_id)
{

    // Field kiriman dari Project Net.
    var FieldMaster = ['KD_UNIT', 'NAMA_UNIT'];
	// Deklarasi penamaan yang akan digunakan pada Grid
    dataSource_viInformasiUnitdokter = new WebApp.DataStore({
        fields: FieldMaster
    });
    // Pemangilan Function untuk memangil data yang akan ditampilkan
    datarefresh_viInformasiUnitdokter();
    // Grid Informasi Tarif
    var grData_viInformasiUnitdokter = new Ext.grid.EditorGridPanel
    (
    {
        xtype: 'editorgrid',
        title: 'Unit',
        store: dataSource_viInformasiUnitdokter,
        autoScroll: true,
        columnLines: true,
        border:false,
		height:450,
        anchor: '100% 100%',
        plugins: [new Ext.ux.grid.FilterRow()],
        selModel: new Ext.grid.RowSelectionModel
        (
				{
					singleSelect: true,
					listeners:
					{
		
					}
				}
				),
				listeners:
				{
					// Function saat ada event double klik maka akan muncul form view
					rowclick: function (sm, ridx, cidx)
					{
						rowSelected_viKasirIgd = dataSource_viInformasiUnitdokter.getAt(ridx);
                                       
										kode_unit =rowSelected_viKasirIgd.data.KD_UNIT;
									
						RefreshDataRwjTarif();
					}
				},
				/**
				*	Mengatur tampilan pada Grid Informasi Tarif
				*	Terdiri dari : Judul, Isi dan Event
				*	Isi pada Grid di dapat dari pemangilan dari Net.
				*	dimana dataindex adalah data dari Net. yang di dapat dari FieldMaster pada dataSource_viInformasiUnitdokter
				*	didapat dari Function datarefresh_viInformasiUnitdokter()
				*/
				colModel: new Ext.grid.ColumnModel
				(
					[
						
						//-------------- ## --------------  
						{
							id: 'colNama_viInformasiUnitdokter',
							header: 'Kode',
							dataIndex: 'KD_UNIT',
							sortable: false,
							hidden : true,
							filter:{}
						
						
						},
						
						//-------------- ## --------------
						{
							id: 'colPoli_viInformasiUnitdokter',
							header: 'Unit',
							dataIndex: 'NAMA_UNIT',
							sortable: true,
								menuDisabled:true,
							width: 170,

						},
						//-------------- ## --------------     
						
						//-------------- ## --------------
					]
				)
			
			}
    )
	/*
	 public $;
	public $;
	public $KD_UNIT;
	*/

    var Field =['KD_UNIT','KD_DOKTER','NAMA'];
     dsInfotarifList = new WebApp.DataStore({ fields: Field });
     
	
     var grListSetTypeBayar = new Ext.grid.EditorGridPanel
     (
        {
            id: 'grListSetTypeBayar',
            stripeRows: true,
			    title: 'Nama Dokter',
            store: dsInfotarifList,
            autoScroll: true,
            columnLines: true,
            border: false,
			height:420,
            anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
            sm: new Ext.grid.RowSelectionModel
				(
					{
                    singleSelect: true,
                    listeners:
                        {
                           
                        }
					}
				),
            listeners:
			{
			rowdblclick: function (sm, ridx, cidx)
				{
					
				}
			},
           cm: new Ext.grid.ColumnModel
            (
				[
                    new Ext.grid.RowNumberer(),
							
							{
							//'','','','','','TGL_BERLAKU',''
							id: 'colSetDaftarByr',
						
							dataIndex: 'KD_DOKTER',
							width: 100,
							hidden:true,
								menuDisabled:true,
							sortable: true
							},
							{
							id: 'colSetDaftarByr',
							header: 'Nama Dokter',
							dataIndex: 'NAMA',
							width: 800,
								menuDisabled:true,
							sortable: true,
							filter:{}
							},
							
		
				]
			//'','','','','','',''
            )
			
		    ,viewConfig: { forceFit: false}
		}
	); 
	// Kriteria filter pada Grid
	
	var FrmTabs_viInformasiUnitdokter = new Ext.Panel
    (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'column',
		    title:  'Dokter Unit',
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100%',
		    iconCls: icons_viInformasiUnitdokter,
		    items: 
			[
				{
					columnWidth: .15,
					layout: 'form',
					border: false,
					autoScroll: true,
					bodyStyle: 'padding:6px 3px 3px 6px',
					items:
					[grData_viInformasiUnitdokter
						
					]
				},
				{
					columnWidth: .85,
					layout: 'form',
					bodyStyle: 'padding:6px 6px 3px 3px',
					border: false,
					anchor: '100% 100%',
					items:
					[grListSetTypeBayar
						
					]
				}
			]
		
    }
    )
    // datarefresh_viInformasiUnitdokter();
    return FrmTabs_viInformasiUnitdokter;
}







function ShowPesanWarning_viInformasiUnitdokter(str,modul)
{
    Ext.MessageBox.show
    (
		{
			title: modul,
			msg:str,
			buttons: Ext.MessageBox.OK,
			icon: Ext.MessageBox.WARNING,
			width :250
		}
    )
}

function ShowPesanError_viInformasiUnitdokter(str,modul)
{
    Ext.MessageBox.show
    (
		{
			title: modul,
			msg:str,
			buttons: Ext.MessageBox.OK,
			icon: Ext.MessageBox.ERROR,
			width :250
		}
    )
}

function ShowPesanInfo_viInformasiUnitdokter(str,modul)
{
    Ext.MessageBox.show
    (
		{
			title: modul,
			msg:str,
			buttons: Ext.MessageBox.OK,
			icon: Ext.MessageBox.INFO,
			width :250
		}
    )
}










function datainit_viInformasiUnitdokter(rowdata)
{
    
}


function dataaddnew_viInformasiUnitdokter()
{
   
}
///---------------------------------------------------------------------------------------///


/**
*	Function : gridDataForm_viInformasiUnitdokter
*	
*	Sebuah fungsi untuk menampilkan isian grid pada edit Informasi Tarif
*	yang di pangil dari Function getFormItemEntry_viInformasiUnitdokter
*/
//-------------------------------------------- Hapus baris -------------------------------------

//-------------------------------- end hapus kolom -----------------------------
//---------------------- Split row -------------------------------------

//---------------------------- end Split row ------------------------------



function datarefresh_viInformasiUnitdokter()
{
    dataSource_viInformasiUnitdokter.load
    (
		{
			params:
			{
				Skip: 0,
				Take: selectCount_viInformasiUnitdokter,
				Sort: '',
				Sortdir: 'ASC',
				target:'viInfoDokterIGD',
				param: ''
			}
		}
    );
	
    return dataSource_viInformasiUnitdokter;
    //alert("refersh")
}

function datarefresh_viInformasiUnitdokterFilter()
{
	var KataKunci='';


	
	
	
	 if (Ext.get('txtfilternama_viInformasiUnitdokter').getValue() != '')
    {
		if (KataKunci == '')
		{
                        KataKunci = ' and  LOWER(deskripsi) like  LOWER( ~' + Ext.get('txtfilternama_viInformasiUnitdokter').getValue() + '%~)';
			
		}
		else
		{
		
                        KataKunci += ' and  LOWER(deskripsi) like  LOWER( ~' + Ext.get('txtfilternama_viInformasiUnitdokter').getValue() + '%~)';
		};

	};
	
	 if (Ext.get('txtfilpoli_viInformasiUnitdokter').getValue() != '')
    {
		if (KataKunci == '')
		{
                        KataKunci = ' and  LOWER(nama_unit) like  LOWER( ~' + Ext.get('txtfilpoli_viInformasiUnitdokter').getValue() + '%~)';
			
		}
		else
		{
		
                        KataKunci += ' and  LOWER(nama_unit) like  LOWER( ~' + Ext.get('txtfilpoli_viInformasiUnitdokter').getValue() + '%~)';
		};

	};
	
	


    if (KataKunci != undefined )
    {
		dataSource_viInformasiUnitdokter.load
		(
			{
				params:
				{
					Skip: 0,
					Take: selectCount_viInformasiUnitdokter,
                                        Sort: 'KD_DOKTER',
					Sortdir: 'ASC',
					target:'viInfoTarif',
					param : ''
				}
			}
		);
    }
	else
	{
		datarefresh_viInformasiUnitdokter();
	};
};

function RefreshDataRwjTarif()
{
	dsInfotarifList.load
		(
			{
				params:
				{
					Skip: 0,
					Take: selectCountInfoTarif,
					Sort: '',
					Sortdir: 'ASC',
					target:'ViewRWJDokterunit',
					param : 'kd_unit =~'+ kode_unit + '~'
				}
			}
		);

	rowSelectedInfotarif = undefined;
	return dsInfotarifList;
};
