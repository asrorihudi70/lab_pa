// Data Source ExtJS

/**
*	Nama File 		: TRInformasiTarif.js
*	Menu 			: Pendaftaran
*	Model id 		: 030103
*	Keterangan 		: Untuk View Informasi Tarif
*	Di buat tanggal : 15 April 2014
*	Oleh 			: SDY_RI
*/

// Deklarasi Variabel pada ExtJS
var kode_tarif;
var kode_produk;
var kode_unit;
var tgl_berlaku;
var dsInfotarifList;
var rowSelected_viKasirIgd;
var selectCountInfoTarif=1;
var rowSelectedInfotarif;
var kd_prod;
var dsDataUnsur_viInformasiDokterspesialis;
var dataSource_viInformasiDokterspesialis;
var selectCount_viInformasiDokterspesialis=50;
var NamaForm_viInformasiDokterspesialis="Informasi Tarif";
var icons_viInformasiDokterspesialis="Gaji";
var addNew_viInformasiDokterspesialis;
var rowSelected_viInformasiDokterspesialis;
var rowSelected_viInformasiDokterspesialisList;
var setLookUps_viInformasiDokterspesialis;
var setLookUps_viInformasiDokterspesialisList;
var now_viInformasiDokterspesialis = new Date();

var BlnIsDetail;
var SELECTDATAPENILAIANPGW; // cek lagi

var CurrentData_viInformasiDokterspesialis =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viInformasiDokterspesialis(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

// Start Shortcut Key
// Kegunaan : Tombol Cepat untuk Keyboard
	Ext.getDoc().on('keypress', function(event, target) {
    if (event.altKey && !event.shiftKey)
	{
        event.stopEvent();
    /*if (Ext.EventObject.getKey() === 18 && !Ext.EventObject.getKey() === 16) {
        Ext.EventObject.stopEvent();*/

        switch(event.getKey()) {
        //switch(Ext.EventObject.getKey()) {

            case event.F1 :
            		dataaddnew_viInformasiDokterspesialis();
            	break;

            case event.F2 :
            		datasave_viInformasiDokterspesialis(false);
					datarefresh_viInformasiDokterspesialis();
				break;

            case event.F3 : // Alt + F3 untuk Edit
	                if (rowSelected_viInformasiDokterspesialis === undefined)
	                {
	                    setLookUp_viInformasiDokterspesialis();
	                }
	                else
	                {
	                    setLookUp_viInformasiDokterspesialis(rowSelected_viInformasiDokterspesialis.data);
	                }
                break;

            case event.F5 :
            		var x = datasave_viInformasiDokterspesialis(true);
							datarefresh_viInformasiDokterspesialis();
							if (x===undefined)
							{
								setLookUps_viInformasiDokterspesialis.close();
							}
				break;

			case event.F10 :
					alert("Delete")
				break;

            // other cases...
        }
        
    }
});
// End Shortcut Key

// Start Project Informasi Tarif

/**
*	Function : dataGrid_viInformasiDokterspesialis
*	
*	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
*/
function dataGrid_viInformasiDokterspesialis(mod_id)
{

    // Field kiriman dari Project Net.
    var FieldMaster = ['KD_SPESIAL', 'SPESIALISASI'];
	// Deklarasi penamaan yang akan digunakan pada Grid
    dataSource_viInformasiDokterspesialis = new WebApp.DataStore({
        fields: FieldMaster
    });
    // Pemangilan Function untuk memangil data yang akan ditampilkan
    datarefresh_viInformasiDokterspesialis();
    // Grid Informasi Tarif
    var grData_viInformasiDokterspesialis = new Ext.grid.EditorGridPanel
    (
    {
        xtype: 'editorgrid',
        title: 'Poli',
        store: dataSource_viInformasiDokterspesialis,
        autoScroll: true,
        columnLines: true,
        border:false,
		height:550,
        anchor: '100% 100%',
        plugins: [new Ext.ux.grid.FilterRow()],
        selModel: new Ext.grid.RowSelectionModel
        (
				{
					singleSelect: true,
					listeners:
					{
		
					}
				}
				),
				listeners:
				{
					// Function saat ada event double klik maka akan muncul form view
					rowclick: function (sm, ridx, cidx)
					{
						rowSelected_viKasirIgd = dataSource_viInformasiDokterspesialis.getAt(ridx);
                                       
					    kode_spesial =rowSelected_viKasirIgd.data.KD_SPESIAL;
									
						RefreshDataRwjTarif();
					}
				},
				/**
				*	Mengatur tampilan pada Grid Informasi Tarif
				*	Terdiri dari : Judul, Isi dan Event
				*	Isi pada Grid di dapat dari pemangilan dari Net.
				*	dimana dataindex adalah data dari Net. yang di dapat dari FieldMaster pada dataSource_viInformasiDokterspesialis
				*	didapat dari Function datarefresh_viInformasiDokterspesialis()
				*/
				colModel: new Ext.grid.ColumnModel
				(
					[
						
						//-------------- ## --------------  
						{
							id: 'colNama_viInformasiDokterspesialis',
							header: 'Kode',
							dataIndex: 'KD_SPESIAL',
							sortable: false,
								menuDisabled:true,
							hidden : true
						
						
						},
						
						//-------------- ## --------------
						{
							id: 'colPoli_viInformasiDokterspesialis',
							header: 'Poli',
							dataIndex: 'SPESIALISASI',
							sortable: true,
								menuDisabled:true,
							width: 270,
							filter:{}

						},
						//-------------- ## --------------     
						
						//-------------- ## --------------
					]
				)
			
			}
    )
	/*
	 public $;
	public $;
	public $KD_UNIT;
	*/

    var Field =['KD_SPESIAL','KD_DOKTER','NAMA'];
     dsInfotarifList = new WebApp.DataStore({ fields: Field });
     
	
     var grListSetTypeBayar = new Ext.grid.EditorGridPanel
     (
        {
            id: 'grListSetTypeBayar',
            stripeRows: true,
			    title: 'Nama Dokter',
            store: dsInfotarifList,
            autoScroll: true,
            columnLines: true,
            border: false,
			height:450,
            anchor: '100% 100%',
            sm: new Ext.grid.RowSelectionModel
				(
					{
                    singleSelect: true,
                    listeners:
                        {
                           
                        }
					}
				),
            listeners:
			{
			rowdblclick: function (sm, ridx, cidx)
				{
					
				}
			},
           cm: new Ext.grid.ColumnModel
            (
				[
                    new Ext.grid.RowNumberer(),
							
							{
							//'','','','','','TGL_BERLAKU',''
							id: 'colSetDaftarByr',
						
							dataIndex: 'KD_DOKTER',
							width: 100,
							hidden:true,
							sortable: true,
								menuDisabled:true,
							},
							{
							id: 'colSetDaftarByr',
							header: 'Nama Dokter',
							dataIndex: 'NAMA',
							width: 750,
							sortable: true,
								menuDisabled:true,
							},
							
		
				]
			//'','','','','','',''
            )
			
		    ,viewConfig: { forceFit: false}
		}
	); 
	// Kriteria filter pada Grid
	
	var FrmTabs_viInformasiDokterspesialis = new Ext.Panel
    (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'column',
		    title:  'Dokter Spesialis',
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100%',
		    iconCls: icons_viInformasiDokterspesialis,
		    items: 
			[
				{
					columnWidth: .25,
					layout: 'form',
					border: false,
					autoScroll: true,
					bodyStyle: 'padding:6px 3px 3px 6px',
					items:
					[grData_viInformasiDokterspesialis
						
					]
				},
				{
					columnWidth: .75,
					layout: 'form',
					bodyStyle: 'padding:6px 6px 3px 3px',
					border: false,
					anchor: '100% 100%',
					items:
					[grListSetTypeBayar
						
					]
				}
			]
		
    }
    )
    // datarefresh_viInformasiDokterspesialis();
    return FrmTabs_viInformasiDokterspesialis;
}

/*
SELECT     KD_COMPONENT, KD_DOKTER, NAMA_UNIT, KD_UNIT, TGL_BERLAKU, TARIF, TARIF_PERCENT, KD_INDUK
FROM         TARIF_COMPONENT where KD_DOKTER='TU'
*/






function ShowPesanWarning_viInformasiDokterspesialis(str,modul)
{
    Ext.MessageBox.show
    (
		{
			title: modul,
			msg:str,
			buttons: Ext.MessageBox.OK,
			icon: Ext.MessageBox.WARNING,
			width :250
		}
    )
}

function ShowPesanError_viInformasiDokterspesialis(str,modul)
{
    Ext.MessageBox.show
    (
		{
			title: modul,
			msg:str,
			buttons: Ext.MessageBox.OK,
			icon: Ext.MessageBox.ERROR,
			width :250
		}
    )
}

function ShowPesanInfo_viInformasiDokterspesialis(str,modul)
{
    Ext.MessageBox.show
    (
		{
			title: modul,
			msg:str,
			buttons: Ext.MessageBox.OK,
			icon: Ext.MessageBox.INFO,
			width :250
		}
    )
}










function datainit_viInformasiDokterspesialis(rowdata)
{
    
}


function dataaddnew_viInformasiDokterspesialis()
{
   
}
///---------------------------------------------------------------------------------------///


/**
*	Function : gridDataForm_viInformasiDokterspesialis
*	
*	Sebuah fungsi untuk menampilkan isian grid pada edit Informasi Tarif
*	yang di pangil dari Function getFormItemEntry_viInformasiDokterspesialis
*/
//-------------------------------------------- Hapus baris -------------------------------------

//-------------------------------- end hapus kolom -----------------------------
//---------------------- Split row -------------------------------------

//---------------------------- end Split row ------------------------------



function datarefresh_viInformasiDokterspesialis()
{
    dataSource_viInformasiDokterspesialis.load
    (
		{
			params:
			{
				Skip: 0,
				Take: selectCount_viInformasiDokterspesialis,
				Sort: '',
				Sortdir: 'ASC',
				target:'viInfoDokterSpesialis',
				param: ''
			}
		}
    );
	
    return dataSource_viInformasiDokterspesialis;
    //alert("refersh")
}

function datarefresh_viInformasiDokterspesialisFilter()
{
	var KataKunci='';


	
	
	
	 if (Ext.get('txtfilternama_viInformasiDokterspesialis').getValue() != '')
    {
		if (KataKunci == '')
		{
                        KataKunci = ' and  LOWER(deskripsi) like  LOWER( ~' + Ext.get('txtfilternama_viInformasiDokterspesialis').getValue() + '%~)';
			
		}
		else
		{
		
                        KataKunci += ' and  LOWER(deskripsi) like  LOWER( ~' + Ext.get('txtfilternama_viInformasiDokterspesialis').getValue() + '%~)';
		};

	};
	
	 if (Ext.get('txtfilpoli_viInformasiDokterspesialis').getValue() != '')
    {
		if (KataKunci == '')
		{
                        KataKunci = ' and  LOWER(nama_unit) like  LOWER( ~' + Ext.get('txtfilpoli_viInformasiDokterspesialis').getValue() + '%~)';
			
		}
		else
		{
		
                        KataKunci += ' and  LOWER(nama_unit) like  LOWER( ~' + Ext.get('txtfilpoli_viInformasiDokterspesialis').getValue() + '%~)';
		};

	};
	
	


    if (KataKunci != undefined )
    {
		dataSource_viInformasiDokterspesialis.load
		(
			{
				params:
				{
					Skip: 0,
					Take: selectCount_viInformasiDokterspesialis,
                                        Sort: 'KD_DOKTER',
					Sortdir: 'ASC',
					target:'viInfoTarif',
					param : ''
				}
			}
		);
    }
	else
	{
		datarefresh_viInformasiDokterspesialis();
	};
};

function RefreshDataRwjTarif()
{
	dsInfotarifList.load
		(
			{
				params:
				{
					Skip: 0,
					Take: selectCountInfoTarif,
					Sort: '',
					Sortdir: 'ASC',
					target:'ViewRWJDokterspesial',
					param : 'kd_spesial =~'+ kode_spesial + '~'
				}
			}
		);

	rowSelectedInfotarif = undefined;
	return dsInfotarifList;
};
