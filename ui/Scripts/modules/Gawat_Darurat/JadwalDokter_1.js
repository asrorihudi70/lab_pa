// Data Source ExtJS # --------------

/**
*	Nama File 		: TRKasirRwj.js
*	Menu 			: 
*	Model id 		: 
*	Keterangan 		: Jadwal Dokter UGD adalah proses untuk mengatur jadwal dokter praktek
*	Di buat tanggal : 13 Agustus 2014
*	Di EDIT tanggal : 26 November 2015
*	Oleh 			: ADE. S
*	Editing			: Melinda
*/

// Deklarasi Variabel pada Jadwal Dokter UGD # --------------
var mRecordRequest = Ext.data.Record.create
(
    [
       {name: 'ASSET_MAINT', mapping:'ASSET_MAINT'},
       {name: 'ASSET_MAINT_ID', mapping:'ASSET_MAINT_ID'},
       {name: 'ASSET_MAINT_NAME', mapping:'ASSET_MAINT_NAME'},
       {name: 'LOCATION_ID', mapping:'LOCATION_ID'},
       {name: 'LOCATION', mapping:'LOCATION'},
       {name: 'PROBLEM', mapping:'PROBLEM'},
       {name: 'REQ_FINISH_DATE', mapping:'REQ_FINISH_DATE'},
       {name: 'DESC_REQ', mapping:'DESC_REQ'},
       {name: 'IMPACT', mapping:'IMPACT'},
       {name: 'ROW_REQ', mapping:'ROW_REQ'}
    ]
);

var AddNewSetjadwal;
var selectCount_viJadwalDokterRwj=50;

var kddokteredit;
var AddNewRequest;
var selectKlinikPoli_JadwalDokterUGD;
var selectDokter_JadwalDokterUGD;
var selectSetRujukanDari;
var dsSetJadwalDokterList;
var selectCountSetJadwalDokter=5;
var rowSelectedSetJadwalDokter;

var valueUnit_viJadwalDokterUGD='';
var selectCountStatusByr_viJadwalDokterUGD;
var CurrentData_viJadwalDokterUGD =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = getPanelSetJadwalDokter(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

// End Deklarasi Variabel pada Jadwal Dokter UGD # --------------

// Start Project Jadwal Dokter UGD # --------------

// --------------------------------------- # Start Function # ---------------------------------------
// ## Silahkan tempatkan semua fungsi eksekusi disini

// --------------------------------------- # End Function # ---------------------------------------

// --------------------------------------- # Start Form # ---------------------------------------

/**
*	Function : dataGrid_viJadwalDokterUGD
*	
*	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
*/

function RefreshDataSet_viJadwalDokterUGD()
{	
	dsSetJadwalDokterList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: selectCount_viJadwalDokterRwj, 
				//Sort: 'EMP_ID',
                Sort: 'kd_dokter',
				Sortdir: 'ASC', 
				target:'ViewJadwalDokterIGD',
				param: ''
			} 
		}
	);
	rowSelectedSetJadwalDokter = undefined;
	return dsSetJadwalDokterList;
};

function getPanelSetJadwalDokter(mod_id) 
{
	
    var Field = ['KD_DOKTER','KD_UNIT','NAMA_UNIT', 'NAMA', 'HARI','JAM'];
    dsSetJadwalDokterList = new WebApp.DataStore({ fields: Field });
	RefreshDataSet_viJadwalDokterUGD();
	
    var grListSetJadwalDokter_JadwalDokterUGD = new Ext.grid.EditorGridPanel
    (
		{
		    id: 'grListSetJadwalDokter_JadwalDokterUGD',
		    stripeRows: true,
		    store: dsSetJadwalDokterList,
		    columnLines: true,
			border:false,
			plugins: [new Ext.ux.grid.FilterRow()],
		    anchor: '100% 100%',
		    sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedSetJadwalDokter=undefined;
							rowSelectedSetJadwalDokter = dsSetJadwalDokterList.getAt(row);
						}
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, ridx, cidx)
				{
					
					rowSelectedSetJadwalDokter = dsSetJadwalDokterList.getAt(ridx);
					if (rowSelectedSetJadwalDokter != undefined)
					{
						setLookUp_viJadwalDokterUGD(rowSelectedSetJadwalDokter.data);
					}
					else
					{
						dataaddnew_viJadwalDokterUGD();
						setLookUp_viJadwalDokterUGD();
					};
					
				}
			},
		    colModel: new Ext.grid.ColumnModel
            (
				[
					new Ext.grid.RowNumberer(),
                    {
                        id: 'colHari_viJadwalDokterUGD',
                        header: 'HARI',                      
                        dataIndex: 'HARI',
                        sortable: true,
                        width: 200,
						filter :{}
                    },
					{
					    id: 'colUnit_viJadwalDokterUGD',
					    header: 'NAMA_UNIT',					   
					    dataIndex: 'NAMA_UNIT',
					    width: 250,
					    sortable: true,
						filter :{}
					},
					{
					    id: 'colDokter_viJadwalDokterUGD',
					    header: 'DOKTER',					   
					    dataIndex: 'NAMA',
					    width: 250,
					    sortable: true,
						filter :{}
					},
					{
					    id: 'colKdDOKTER_viJadwalDokterUGD',
					    header: 'kd dokter',					   
					    dataIndex: 'KD_DOKTER',
					    width: 50,
						hidden:true,
					    sortable: true
					},
					{
					    id: 'colKdUnit_viJadwalDokterUGD',
					    header: 'kd unit',					   
					    dataIndex: 'KD_UNIT',
					    width: 50,
						hidden:true,
					    sortable: true
					},
					{
					    id: 'colJam_viJadwalDokterUGD',
					    header: 'Jam (PM/AM)',					   
					    dataIndex: 'JAM',
					    width: 100,
					    sortable: true,
						filter :{}
					},
									
                ]
			),
			bbar:new WebApp.PaggingBar({
            displayInfo: true,
            store: dsSetJadwalDokterList,
            pageSize: selectCountSetJadwalDokter,
            displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
            emptyMsg: "Tidak ada record&nbsp;&nbsp;"		
            }),
		    tbar:
			[
				{
				    id: 'btnEditSetJadwalDokter',
				    text: 'Buat Jadwal Baru',
					iconAlign:'left',
				    tooltip: nmEditData,
				    iconCls: 'Edit_Tr',
				    handler: function(sm, row, rec) 
					{
						setLookUp_viJadwalDokterUGD();
				    }
				},' ','-'
			]
		    //,viewConfig: { forceFit: true }
		}
	);


    var FormSetJadwalDokter = new Ext.Panel
    (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: 'Jadwal DOkter UGD',
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:0px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100%',
		    iconCls: 'SetupJadwalDokter',
		    items: [grListSetJadwalDokter_JadwalDokterUGD],
		    tbar:
			[
				
			]
		}
	);
    //END var FormSetJadwalDokter--------------------------------------------------

	RefreshDataSet_viJadwalDokterUGD();
    return FormSetJadwalDokter ;
};

// End Function dataGrid_viJadwalDokterUGD # --------------

/**
*	Function : setLookUp_viJadwalDokterUGD
*	
*	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
*/

//test buat popup jadwal baru
function setLookUp_viJadwalDokterUGD(rowdata)
{
    var lebar = 465;
    setLookUps_JadwalDokterUGD = new Ext.Window
    ( 
		{
			id: 'setLookUps_JadwalDokterUGD',
			name: 'setLookUps_JadwalDokterUGD',
			title: 'Buat Jadwal Baru Dokter', 
			closeAction: 'destroy',        
			width: lebar,
			height:210,
			resizable:false,
			autoScroll: false,
			iconCls: 'Studi_Lanjut',
			modal: true,		
			items: getFormItemEntry_viJadwalDokterUGD(lebar,rowdata), //1
		}
    );

    setLookUps_JadwalDokterUGD.show();
    if (rowdata == undefined)
    {
		console.log(rowdata)
        dataaddnew_viJadwalDokterUGD();
		Ext.getCmp('btnDelete_viDaftar').disable();	
    }
    else
    {
        datainit_viJadwalDokterUGD(rowdata);
    }
}

function dataaddnew_viJadwalDokterUGD()
{
	Ext.get('cboHari_JadwalDokterUGD').dom.value = '';
    Ext.get('cboPoliklinik_JadwalDokterUGD').dom.value = '';
	Ext.get('cbDokterUGD_JadwalDokterUGD').dom.value = '';
	selectKlinikPoli_JadwalDokterUGD = undefined;
	selectDokter_JadwalDokterUGD = undefined;
	
	Ext.get('txtMenit_JadwalDokterUGD').dom.value = '';
	Ext.get('txtJam_JadwalDokterUGD').dom.value = '';
}

function datainit_viJadwalDokterUGD(rowdata) 
{

    AddNewSetjadwal = false;
    Ext.get('cboHari_JadwalDokterUGD').dom.value = rowdata.HARI;
    Ext.get('cboPoliklinik_JadwalDokterUGD').dom.value = rowdata.NAMA_UNIT;
	loaddatastoredokter_JadwalDokterUGD(rowdata.KD_UNIT);
	Ext.get('cbDokterUGD_JadwalDokterUGD').dom.value = rowdata.NAMA;
	Ext.get('txtKdPoli_JadwalDokterUGD').dom.value = rowdata.KD_UNIT;
	Ext.get('txtKdDokter').dom.value = rowdata.KD_DOKTER;
	Ext.get('txtJam_JadwalDokterUGD').dom.value = rowdata.JAM.substr(0,2);
	Ext.get('txtMenit_JadwalDokterUGD').dom.value = rowdata.JAM.substr(3,2);
	kddokteredit = rowdata.KD_DOKTER;
	Kd_Unit: selectKlinikPoli_JadwalDokterUGD;
	Kd_Dokter: selectDokter_JadwalDokterUGD;
	if (rowdata.HARI === null)
	{
		Ext.get('cboHari_JadwalDokterUGD').dom.value = '';
	}
	else
	{
		Ext.get('cboHari_JadwalDokterUGD').dom.value = rowdata.HARI;
	};
	
	if (rowdata.KD_UNIT === null)
	{
			Ext.get('cboPoliklinik_JadwalDokterUGD').dom.value = '';
	}
	else
	{
			Ext.get('cboPoliklinik_JadwalDokterUGD').dom.value = rowdata.NAMA_UNIT;
	};
	
	if (rowdata.KD_DOKTER === null)
	{
		Ext.get('cbDokterUGD_JadwalDokterUGD').dom.value = '';
	}
	else
	{
		Ext.get('cbDokterUGD_JadwalDokterUGD').dom.value = rowdata.NAMA;
	};

};

function SetJadwalAddNew() 
{
    AddNewSetjadwal = true;   
	Ext.get('cboHari_JadwalDokterUGD').dom.value = '';
    Ext.get('cboPoliklinik_JadwalDokterUGD').dom.value = '';
	Ext.get('cbDokterUGD_JadwalDokterUGD').dom.value = '';
};

function getFormItemEntry_viJadwalDokterUGD(lebar,rowdata)
{
    var pnlFormDataBasic_viJadwalDokterUGD = new Ext.FormPanel
    (
		{
			title: '',
			region: 'north',
			layout: 'form',
			bodyStyle: 'padding:10px 10px 10px 10px',
			autoScroll: true,
			width: 450,
			height: 175,
			border: false,
			items:[
				getItemJadwalBarui_JadwalDokterUGD()
			],
			fileUpload: true,
			tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viDaftar',
						handler: function()
						{
							datasave_viJadwalDokterUGD(false);
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						id: 'btnSimpanExit_viDaftar',
						handler: function()
						{
							var x = datasave_viJadwalDokterUGD(true);
							
							if (x===undefined)
							{
								datasave_viJadwalDokterUGD(true);
								setLookUps_JadwalDokterUGD.close();
								RefreshDataSet_viJadwalDokterUGD();
							}
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Hapus',
						iconCls: 'remove',
						id: 'btnDelete_viDaftar',
						handler: function()
						{
							SetJadwalDokterDelete_viJadwalDokterUGD();
							RefreshDataSet_viJadwalDokterUGD();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						tooltip: 'refresh',
						id: 'btnEdit_viJadwalDokterUGDDokterUGD',
						handler: function(sm, row, rec)
						{
							RefreshDataSet_viJadwalDokterUGD();
						}
					},
				]
			}
			
		}
    )

    return pnlFormDataBasic_viJadwalDokterUGD;
}

function getItemJadwalBarui_JadwalDokterUGD()
{
   
	var items =
	{
		layout: 'column',
		border: true,
		bodyStyle: 'padding:10px 10px 10px 10px',
		items:
		[
			{
			    columnWidth: .99,
			    layout: 'form',
			    border: false,
				labelWidth:90,
			    items:
				[
					mComboHari_JadwalDokterUGD(),
					mComboPoliklinik_JadwalDokterUGD(),
					mComboDokter_JadwalDokterUGD()
				]
			},
			{
				columnWidth: .35,
				layout: 'form',
				border: false,
				labelWidth:90,
				items:
				[
					
					{
						xtype: 'textfield',
						emptyText:'Jam',
						fieldLabel: 'Jam Praktek',
						
						id	: 'txtJam_JadwalDokterUGD',
						anchor:'100%',
						
					}
				]
			},
			{
				columnWidth: .60,
				layout: 'form',
				border: false,
				labelWidth:1,
				items:
				[
					
					{
						xtype: 'textfield',
						emptyText:'Menit',
						id	: 'txtMenit_JadwalDokterUGD',
						width: 50,
						
						
					},
				]
			},
			{
				 xtype: 'textfield',
                 fieldLabel: nmRequestId + ' ',
                 id: 'txtKdPoli_JadwalDokterUGD',
				 hidden:true,
                 width: 100,
			},
			{
				 xtype: 'textfield',
                 fieldLabel: nmRequestId + ' ',
                 id: 'txtKdDokter',
				 hidden:true,
                 width: 100,
			},
			]

        };
    return items;
};

function mComboHari_JadwalDokterUGD()
{
    var cbHari_JadwalDokterUGD = new Ext.form.ComboBox
	(
		{
			id:'cboHari_JadwalDokterUGD',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			fieldLabel: 'Hari ',
			width:110,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
					data: [[1, 'Senin'], [2, 'Selasa'],[3, 'Rabu'],[4, 'Kamis'],
					   [5, 'Jumat'], [6, 'Sabtu'],[7, 'Minggu']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetRujukanDari,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetRujukanDari=b.data.displayText ;
				}
			}
		}
	);
	return cbHari_JadwalDokterUGD;
};

function mComboPoliklinik_JadwalDokterUGD()
{
    var Field = ['KD_UNIT','NAMA_UNIT'];
    dsPoli_JadwalDokterUGD = new WebApp.DataStore({fields: Field});

	dsPoli_JadwalDokterUGD.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ViewSetupUnit',
                param: "kd_bagian=3 and type_unit=false"
            }
        }
    )

    var cbPoliklinik_JadwalDokterUGD = new Ext.form.ComboBox
    (
        {
            id: 'cboPoliklinik_JadwalDokterUGD',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            fieldLabel: 'Poliklinik ',
            align: 'Right',
            store: dsPoli_JadwalDokterUGD,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
            width: 200,
            listeners:
			{
				'select': function(a, b, c)
				{
					selectKlinikPoli_JadwalDokterUGD=b.data.KD_UNIT
					loaddatastoredokter_JadwalDokterUGD(b.data.KD_UNIT)
					Ext.get('txtKdPoli_JadwalDokterUGD').dom.value = b.data.KD_UNIT
				},
				'render': function(c)
				{
				}
			}
        }
    )

    return cbPoliklinik_JadwalDokterUGD;
};


function mComboDokter_JadwalDokterUGD()
{ 
	var Field = ['KD_DOKTER','NAMA'];

    dsDokterUGD_JadwalDokterUGD = new WebApp.DataStore({fields: Field});
    var cboDokterUGD_JadwalDokterUGD = new Ext.form.ComboBox
	(
		{
		    id: 'cbDokterUGD_JadwalDokterUGD',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
            forceSelection: true,
		    fieldLabel: 'Dokter ',
		    align: 'Right',
		    store: dsDokterUGD_JadwalDokterUGD,
		    valueField: 'KD_DOKTER',
		    displayField: 'NAMA',
			 width: 200,
		    listeners:
			{
			    'select': function(a,b,c)
				{
					selectDokter_JadwalDokterUGD = b.data.KD_DOKTER;
					Ext.get('txtKdDokter').dom.value = b.data.KD_DOKTER
				},
				'render': function(c)
				{
					/* c.getEl().on('keypress', function(e) {
					if(e.getKey() == 13) //atau Ext.EventObject.ENTER
					Ext.getCmp('kelPasien').focus();
					}, c); */
				}
			}
		}
	);
    return cboDokterUGD_JadwalDokterUGD;

};





function loaddatastoredokter_JadwalDokterUGD(kd_unit)
{
	dsDokterUGD_JadwalDokterUGD.load
	(
		{
			params:
			{
				Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: 'nama',
			    Sortdir: 'ASC',
			    target: 'ViewComboDokter',
			    param: 'where dk.kd_unit=~'+ kd_unit+ '~'
			}
		}
	)
};



//awal simpan detail


function getParamRequestSave_viJadwalDokterUGD() 
{
    var params =
	{
		Table:'ViewJadwalDokterIGD',
		Hari: Ext.get('cboHari_JadwalDokterUGD').getValue(),
		Unit: Ext.get('cboPoliklinik_JadwalDokterUGD').getValue(),
		Dokter: Ext.get('cbDokterUGD_JadwalDokterUGD').getValue(),
		Kd_Unit: selectKlinikPoli_JadwalDokterUGD,
		Kd_Dokter: selectDokter_JadwalDokterUGD,
		jam:Ext.getCmp('txtJam_JadwalDokterUGD').getValue(),
		menit:Ext.getCmp('txtMenit_JadwalDokterUGD').getValue(),
	};
    return params
};

function getParamRequestDelete_viJadwalDokterUGD() 
{
    var params =
	{
		Table:'ViewJadwalDokterIGD',
		Hari: Ext.get('cboHari_JadwalDokterUGD').getValue(),
		Kd_Dokter: Ext.get('txtKdDokter').getValue(),
		Kd_Unit: Ext.get('txtKdPoli_JadwalDokterUGD').getValue(),
		
	};
    return params
};




function datasave_viJadwalDokterUGD(mBol) 
{	
	if (ValidasiEntryCMRequest_viJadwalDokterUGD(nmHeaderSimpanData,false) == 1 )
	{
		if (AddNewRequest == true) 
		{
			Ext.Ajax.request
			(
				{
					url: baseURL + "index.php/main/CreateDataObj",					
					params: getParamRequestSave_viJadwalDokterUGD(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							RefreshDataSet_viJadwalDokterUGD();
							ShowPesanInfoRequest_viJadwalDokterUGD(nmPesanSimpanSukses,nmHeaderSimpanData);
							
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningRequest_viJadwalDokterUGD(nmPesanSimpanGagal,nmHeaderSimpanData);
						}
						else if (cst.success === false && cst.pesan===1)
						{
							ShowPesanWarningRequest_viJadwalDokterUGD(nmPesanSimpanGagal + ' , This request had been approved / rejected',nmHeaderSimpanData);
						}
						else 
						{
							ShowPesanErrorRequest_viJadwalDokterUGD(nmPesanSimpanError,nmHeaderSimpanData);
						};
					}
				}
			)
		}
		else 
		{
			Ext.Ajax.request
			 (
				{
					url: baseURL + "index.php/main/CreateDataObj",
					params: getParamRequestSave_viJadwalDokterUGD(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoRequest_viJadwalDokterUGD(nmPesanSimpanSukses,nmHeaderSimpanData);
							RefreshDataSet_viJadwalDokterUGD();
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningRequest_viJadwalDokterUGD(nmPesanSimpanGagal,nmHeaderSimpanData);
						}
						else if (cst.success === false && cst.pesan===1)
						{
							ShowPesanWarningRequest_viJadwalDokterUGD(nmPesanSimpanGagal + ' , This request had been approved / rejected',nmHeaderSimpanData);
						}
						else 
						{
							ShowPesanErrorRequest_viJadwalDokterUGD(nmPesanSimpanError,nmHeaderSimpanData);
						};
						RefreshDataSet_viJadwalDokterUGD();
					}
				}
			)
		};
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
};


function SetJadwalDokterDelete_viJadwalDokterUGD() 
{
		Ext.Msg.show
		(
			{
			   title:nmHeaderHapusData,
			   msg: "Apakah anda yakin akan menghapus ini?" ,
			   buttons: Ext.MessageBox.YESNO,
			   width:250,
			   fn: function (btn) 
			   {			
					if (btn === 'yes') 
					{
						Ext.Ajax.request
						(
							{
								url: WebAppUrl.UrlDeleteData,
								params: getParamRequestDelete_viJadwalDokterUGD(),
								success: function(o) 
								{
									var cst = Ext.decode(o.responseText);
									if (cst.success === true) 
									{
										RefreshDataSet_viJadwalDokterUGD();
										ShowPesanWarningRequest_viJadwalDokterUGD(nmPesanHapusSukses,nmHeaderHapusData);
										setLookUps_JadwalDokterUGD.close();
										dataaddnew_viJadwalDokterUGD();
										
									}
									else if (cst.success === false && cst.pesan===0)
									{
										ShowPesanWarningRequest_viJadwalDokterUGD(nmPesanHapusGagal,nmHeaderHapusData);
									}
									else 
									{
										ShowPesanWarningRequest_viJadwalDokterUGD(nmPesanHapusError,nmHeaderHapusData);
									};
								}
							}
						)
					};
				}
			}
		)
	};



function ValidasiEntryCMRequest_viJadwalDokterUGD(modul,mBolHapus)
{
	var x = 1;
	
	if((Ext.get('cboHari_JadwalDokterUGD').getValue() == '') || (Ext.get('cboPoliklinik_JadwalDokterUGD').getValue() == '') || (Ext.get('cbDokterUGD_JadwalDokterUGD').getValue() == '') )
	{
		if (Ext.get('cboHari_JadwalDokterUGD').getValue() == '') 
		{
			x = 0;
		}
		else if (Ext.get('cboPoliklinik_JadwalDokterUGD').getValue() == '') 
		{
			ShowPesanWarningRequest_viJadwalDokterUGD(nmGetValidasiKosong(nmRequestId), modul);
			x = 0;
		}
		else if (Ext.get('cbDokterUGD_JadwalDokterUGD').getValue() == '') 
		{
			ShowPesanWarningRequest_viJadwalDokterUGD(nmGetValidasiKosong(nmRequesterRequest), modul);
			x = 0;
		}
	};
	return x;
};


function ShowPesanWarningRequest_viJadwalDokterUGD(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorRequest_viJadwalDokterUGD(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoRequest_viJadwalDokterUGD(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};
