var kdUnit = '3';

var CurrentKasirIGD =
{
    data: Object,
    details: Array,
    row: 0
};
var CurrentHistory =
{
    data: Object,
    details: Array,
    row: 0
};
 var gridDTLTRIGD;
var dsprinter_kasirigd;
var varkd_tarif_igd;
var vKdPasien;
var vKdDokter;
var nilai_kd_tarif;
var syssetting='igd_default_klas_produk';
var tampungtypedata;
var Trkdunit2;
var FormLookUpsdetailTRTransfer_IGD;
var tanggaltransaksitampung;
var winIGDBukaTransaksiPasswordDulu;
var variablebatalhistori_igd;
var variablebukahistori_igd;
var ProdukDataIGD;
var CurrentRowPemeriksaanIGD;
var winIGDPasswordDulu;
var	tmp_kodeunitkamarigd;
var	tmp_kdspesialigd;
var	tmp_nokamarigd;
var KasirIGDComboBoxKdProduk;
var KasirIGDComboBoxKdProdukKP;
var KasirIGDDataStoreProduk=new Ext.data.ArrayStore({id: 0,fields:['kd_produk','deskripsi','harga','tglberlaku'],data: []});
var currentJasaDokterUrutDetailTransaksi_KasirIGD;
var currentJasaDokterKdProduk_KasirIGD;
var currentJasaDokterKdTarif_KasirIGD;
var currentJasaDokterHargaJP_KasirIGD;
var dsGridJasaDokterPenindak_KasirIGD;
var GridDokterTr_KasirIGD;
var CurrentTglTransaksi_KasirIGD;
var CurrentNoTransaksi_KasirIGD;
var CurrentKdKasir_KasirIGD;
var CurrentKdCustomer_KasirIGD;
var CurrentKdPasien_KasirIGD;
var CurrentKdUnit_KasirIGD;
var CurrentUrutMasuk_KasirIGD;
var CurrentTglMasuk_KasirIGD;
var currentTOTAL_KasirIGD;
var SisaPembayaranKasirIGD=0;


dsgridpilihdokterpenindak_KasirIGD	= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_dokter','nama'],
	data: []
});

var mRecordKasirigd = Ext.data.Record.create
(
    [
       {name: 'DESKRIPSI2', mapping:'DESKRIPSI2'},
       {name: 'KD_PRODUK', mapping:'KD_PRODUK'},
       {name: 'DESKRIPSI', mapping:'DESKRIPSI'},
       {name: 'KD_TARIF', mapping:'KD_TARIF'},
       {name: 'HARGA', mapping:'HARGA'},
       {name: 'QTY', mapping:'QTY'},
       {name: 'TGL_TRANSAKSI', mapping:'TGL_TRANSAKSI'},
       {name: 'DESC_REQ', mapping:'DESC_REQ'},
       {name: 'URUT', mapping:'URUT'}
    ]
);
var mRecordIGD = Ext.data.Record.create
(
    [
       {name: 'DESKRIPSI2', mapping:'DESKRIPSI2'},
       {name: 'KD_PRODUK', mapping:'KD_PRODUK'},
       {name: 'DESKRIPSI', mapping:'DESKRIPSI'},
       {name: 'KD_TARIF', mapping:'KD_TARIF'},
       {name: 'HARGA', mapping:'HARGA'},
       {name: 'QTY', mapping:'QTY'},
       {name: 'TGL_TRANSAKSI', mapping:'TGL_TRANSAKSI'},
       {name: 'DESC_REQ', mapping:'DESC_REQ'},
       {name: 'URUT', mapping:'URUT'}
    ]
);
var Kdtransaksi;
var tapungkd_pay;
var dsTRDetailHistoryListIGD;
var AddNewHistory                        = true;
var selectCountHistory                   = 50;
var now                                  = new Date();
var rowSelectedHistory;
var cellSelecteddeskripsi;
var FormLookUpsdetailTRHistory;
var now                                  = new Date();
var cellSelectedtutup_kasirigd;
var vkd_unit;
var kdokter_btl;
var nowTglTransaksiIGD                   = new Date();
var kdcustomeraa;
var labelisi;
var jenispay;
var variablehistori;
var selectCountStatusByr_viKasirigdKasir ='Belum Lunas';
var dsTRKasirigdKasirList;
var dsTRDetailKasirigdKasirList;
var AddNewKasirigdKasir                  = true;
var selectCountKasirigdKasir             = 50;
var now                                  = new Date();
var rowSelectedKasirigdKasir;

var FormLookUpsdetailTRKasirigd;
var valueStatusCMKasirigdView='All';
var nowTglTransaksiIGD2 = nowTglTransaksiIGD.format('d-M-Y');
var dsComboBayar;
var vkode_customer;
var vflag;
 var gridDTLTRKasirigd;
 
var kodekasir;
var notransaksi ;
var tgltrans;
var kodepasien;
var namapasien;
var kodeunit;
var namaunit;
var kodepay;
var uraianpay;
CurrentPage.page = getPanelKasirigd(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelKasirigd(mod_id) 
{

    var Field = ['KD_DOKTER','NO_TRANSAKSI','KD_UNIT','KD_PASIEN','NAMA','NAMA_UNIT','ALAMAT',
	'TANGGAL_TRANSAKSI','NAMA_DOKTER','KD_CUSTOMER','CUSTOMER','URUT_MASUK','FLAG','LUNAS','KET_PAYMENT','CARA_BAYAR',
	'JENIS_PAY','KD_PAY','POSTING','TYPE_DATA','CO_STATUS','KD_KASIR'];
    dsTRKasirigdKasirList = new WebApp.DataStore({ fields: Field });
	
	ProdukDataIGD = new WebApp.DataStore({ fields: ['KD_PRODUK','KD_KLAS','DESKRIPSI','TARIF','TGL_BERLAKU','KD_TARIF'] });
    refeshKasirIgdKasir();
    var grListTRKasirigd = new Ext.grid.EditorGridPanel
    (
        {
			stripeRows: true,
			store: dsTRKasirigdKasirList,
			columnLines: false,
			autoScroll:true,
			anchor: '100% 42%',
			border: false,
			sort :false,
            sm: new Ext.grid.RowSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        rowselect: function(sm, row, rec)
                        {
							
							rowSelectedKasirigdKasir = dsTRKasirigdKasirList.getAt(row);
							CurrentKDUnitIGD         = rowSelectedKasirigdKasir.data.KD_UNIT;
							CurrentKDCustomerIGD     = rowSelectedKasirigdKasir.data.KD_CUSTOMER;
							CurrentKDDokerIGD        = rowSelectedKasirigdKasir.data.KD_DOKTER;
							CurrentURUT_MASUKIGD     = rowSelectedKasirigdKasir.data.URUT_MASUK;
							CurrentKD_PASIENIGD      = rowSelectedKasirigdKasir.data.KD_PASIEN;
							vKdPasien                = rowSelectedKasirigdKasir.data.KD_PASIEN;
							vKdDokter                = rowSelectedKasirigdKasir.data.KD_DOKTER;
							CurrentKdPasien_KasirIGD = rowSelectedKasirigdKasir.data.KD_PASIEN;
							CurrentKdUnit_KasirIGD 	 = rowSelectedKasirigdKasir.data.KD_UNIT;
							CurrentUrutMasuk_KasirIGD = rowSelectedKasirigdKasir.data.URUT_MASUK;
							CurrentTglMasuk_KasirIGD = rowSelectedKasirigdKasir.data.TANGGAL_TRANSAKSI;
							//console.log(rowSelectedKasirigdKasir.data);
						//	getProdukIGD(ProdukDataIGD);
							
						}
                    }
                }
            ),
            listeners:
            {
                rowdblclick: function (sm, ridx, cidx)
                {
			           
                    rowSelectedKasirigdKasir = dsTRKasirigdKasirList.getAt(ridx);
				
                    if (rowSelectedKasirigdKasir != undefined)
                    { 	if ( rowSelectedKasirigdKasir.data.LUNAS=='f' )
							   {
									KasirLookUpigd(rowSelectedKasirigdKasir.data);
									
							   }else
							   {
							   ShowPesanWarningKasirigd('Pembayaran Tidak Bisa Di Lakukan karena Sudah Lunas','Pembayaran');
							   }
                    }else{
					ShowPesanWarningKasirigd('Silahkan Pilih data   ','Pembayaran');
					      }
                   
                },
				rowclick: function (sm, ridx, cidx)
                {
						loadMask.show();
						cellSelectedtutup_kasirigd = rowSelectedKasirigdKasir.data.NO_TRANSAKSI;
						if ( rowSelectedKasirigdKasir.data.LUNAS=='t' && rowSelectedKasirigdKasir.data.CO_STATUS =='t')
						   {
								Ext.getCmp('btnTutupTransaksiKasirigd').disable();
								Ext.getCmp('btnHpsBrsKasirigd').disable();
								Ext.getCmp('btnLookupIGD').disable();
								Ext.getCmp('btnSimpanIGD').disable();
								Ext.getCmp('btnHpsBrsIGD').disable();
								Ext.getCmp('btnTambahBrsKasirIGD').disable();
							 
						   }
						   else if ( rowSelectedKasirigdKasir.data.LUNAS=='f' && rowSelectedKasirigdKasir.data.CO_STATUS =='f')
						   {
								Ext.getCmp('btnTutupTransaksiKasirigd').disable();
								Ext.getCmp('btnHpsBrsKasirigd').enable();
								Ext.getCmp('btnLookupIGD').enable();
								Ext.getCmp('btnSimpanIGD').enable();
								Ext.getCmp('btnHpsBrsIGD').enable();
								Ext.getCmp('btnTambahBrsKasirIGD').enable();
							   
							}
							   else if ( rowSelectedKasirigdKasir.data.LUNAS=='t' && rowSelectedKasirigdKasir.data.CO_STATUS =='f')
						   {
								Ext.getCmp('btnTutupTransaksiKasirigd').enable();
								Ext.getCmp('btnHpsBrsKasirigd').enable();
								Ext.getCmp('btnLookupIGD').enable();
								Ext.getCmp('btnSimpanIGD').enable();
								Ext.getCmp('btnHpsBrsIGD').enable();
								Ext.getCmp('btnTambahBrsKasirIGD').enable();
						       
							}
							
							
							CurrentTglTransaksi_KasirIGD = rowSelectedKasirigdKasir.data.TANGGAL_TRANSAKSI;
							CurrentNoTransaksi_KasirIGD = rowSelectedKasirigdKasir.data.NO_TRANSAKSI;
							CurrentKdKasir_KasirIGD = rowSelectedKasirigdKasir.data.KD_KASIR;
							
							kodekasir    		=rowSelectedKasirigdKasir.data.KD_KASIR;
							notransaksi  		= rowSelectedKasirigdKasir.data.NO_TRANSAKSI ;
							tgltrans     		=rowSelectedKasirigdKasir.data.TANGGAL_TRANSAKSI;
							kodepasien   		=rowSelectedKasirigdKasir.data.KD_PASIEN;
							namapasien   		=rowSelectedKasirigdKasir.data.NAMA;
							kodeunit    		=rowSelectedKasirigdKasir.data.KD_UNIT;
							namaunit     		=rowSelectedKasirigdKasir.data.NAMA_UNIT;
							kodepay      		=rowSelectedKasirigdKasir.data.KD_PAY;
							uraianpay    		=rowSelectedKasirigdKasir.data.CARA_BAYAR;
							kdcustomeraa 		=rowSelectedKasirigdKasir.data.KD_CUSTOMER;
							kdokter_btl 		=rowSelectedKasirigdKasir.data.KD_DOKTER;
							CurrentKDDokerIGD	=rowSelectedKasirigdKasir.data.KD_DOKTER;
							cekTindakan(rowSelectedKasirigdKasir.data);
							RefreshDatahistoribayar(rowSelectedKasirigdKasir.data.NO_TRANSAKSI);
							RefreshDataKasirIGDDetail(rowSelectedKasirigdKasir.data.NO_TRANSAKSI);
								Ext.Ajax.request(
								{
									url:  baseURL + "index.php/main/functionLABPoliklinik/gettarif",
									 params: {
										kd_customer: rowSelectedKasirigdKasir.data.KD_CUSTOMER,
									},
									failure: function(o)
									{},	    
									success: function(o) {

										var cst = Ext.decode(o.responseText);
											varkd_tarif_igd=cst.kd_tarif;
											getProdukIGD(rowSelectedKasirigdKasir.data.KD_UNIT);
										}
								});
							Ext.Ajax.request(
								{
								//url: "./home.mvc/getModule",
								//url: baseURL + "index.php/main/getTrustee",
								url: baseURL + "index.php/main/getcurrentshift",
									params: {
									//UserID: 'Admin',
									command: '0',
									// parameter untuk url yang dituju (fungsi didalam controller)
								},
								failure: function(o)
								{
									 var cst = Ext.decode(o.responseText);
									
								},	    
								success: function(o) {
									//var cst = Ext.decode(o.responseText);
									tampungshiftsekarang=o.responseText
									//Ext.get('txtNilaiShift').dom.value =tampungshiftsekarang ;
								}
										
							});
									Ext.Ajax.request(
									{
										//url: "./home.mvc/getModule",
										//url: baseURL + "index.php/main/getTrustee",
										url: baseURL + "index.php/main/Getkdtarif",
										 params: {
											//UserID: 'Admin',
											customer: rowSelectedKasirigdKasir.data.KD_CUSTOMER,
											// parameter untuk url yang dituju (fungsi didalam controller)
										},
										failure: function(o)
										{
											 var cst = Ext.decode(o.responseText);
											
										},	    
										success: function(o) {										
										
											//var cst = Ext.decode(o.responseText);
											nilai_kd_tarif=o.responseText;
											//Ext.get('txtNilaiShift').dom.value =tampungshiftsekarang ;



										}
									
									});
         
                } 
                   
				
            },
        cm: new Ext.grid.ColumnModel
            (
                [
                    {
                        id: 'colLUNAScoba',
                        header: 'Status Lunas',
                        dataIndex: 'LUNAS',
                        sortable: true,
                        width: 90,
                        align:'center',
                        renderer: function(value, metaData, record, rowIndex, colIndex, store)
                        {
                             switch (value)
                             {
                                 case 't':
                                         metaData.css = 'StatusHijau'; // 
                                         break;
                                 case 'f':
                                        metaData.css = 'StatusMerah'; // rejected

                                         break;
                             }
                             return '';
                        }
                    },
					 {
                        id: 'coltutuptr',
                        header: 'Tutup transaksi',
                        dataIndex: 'CO_STATUS',
                        sortable: true,
                        width: 90,
                        align:'center',
                       renderer: function(value, metaData, record, rowIndex, colIndex, store)
                        {
                             switch (value)
                             {
                                 case 't':
                                         metaData.css = 'StatusHijau'; // 
                                         break;
                                 case 'f':
                                        metaData.css = 'StatusMerah'; // rejected

                                         break;
                             }
                             return '';
                        }
                    },
                    {
                        id: 'colReqIdViewKasirIGD',
                        header: 'No. Transaksi',
                        dataIndex: 'NO_TRANSAKSI',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 80
                    },
                    {
                        id: 'colTglKasirigdViewKasirIGD',
                        header: 'Tgl Transaksi',
                        dataIndex: 'TANGGAL_TRANSAKSI',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 75,
                            renderer: function(v, params, record)
                            {
                                    return ShowDate(record.data.TANGGAL_TRANSAKSI);

                        }
                    },
					{
                        header: 'No. Medrec',
                        width: 100,
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        dataIndex: 'KD_PASIEN',
                        id: 'colKasirigderViewKasirIGD'
                    },
					{
                        header: 'Pasien',
                        width: 190,
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        dataIndex: 'NAMA',
                        id: 'colKasirigderViewKasirIGD'
                    },
                    {
                        id: 'colLocationViewKasirIGD',
                        header: 'Alamat',
                        dataIndex: 'ALAMAT',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 170
                    },
                    
                    {
                        id: 'colDeptViewKasirIGD',
                        header: 'Dokter',
                        dataIndex: 'NAMA_DOKTER',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 150
                    },
                    {
                        id: 'colunitViewKasirIGD',
                        header: 'Unit',
                        dataIndex: 'NAMA_UNIT',
						sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 90
                    },
					     {
                        id: 'colcustomerViewKasirIGD',
                        header: 'Kel. Pasien',
                        dataIndex: 'CUSTOMER',
						sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 90
                    },
					//CUSTOMER
					
                   
                ]
            ),  
			viewConfig:{forceFit: true},
            tbar:
                [
                    {
                        id: 'btnEditKasirigd',
                        text: 'Pembayaran',
                        tooltip: nmEditData,
                        iconCls: 'Edit_Tr',
                        handler: function(sm, row, rec)
                        {
                            if (rowSelectedKasirigdKasir != undefined)
                            {
                                    KasirLookUpigd(rowSelectedKasirigdKasir.data);
                            }
                            else
                            {
							ShowPesanWarningKasirigd('Pilih data tabel  ','Pembayaran');
                                    //alert('');
                            }
                        }, //disabled :true
                    }
					
					, ' ', ' ','' + ' ','' + ' ','' + ' ','' + ' ', ' ','' + '-',
					{
						text: 'Tutup Transaksi',
						id: 'btnTutupTransaksiKasirigd',
						tooltip: nmHapus,
						iconCls: 'remove',
						handler: function()
							{
							if(cellSelectedtutup_kasirigd=='' || cellSelectedtutup_kasirigd=='undefined')
							{
								ShowPesanWarningKasirigd ('Pilih data tabel  ','Pembayaran');
							} else {	
								UpdateTutuptransaksi(false);	
								// Ext.getCmp('btnEditKasirigd').disable();
								Ext.getCmp('btnTutupTransaksiKasirigd').disable();
								Ext.getCmp('btnHpsBrsKasirigd').disable();
								}										
							},
							disabled:true
					},{
						text: 'Buka Transaksi',
						id: 'btnBukaTransaksiKasirigd',
						tooltip: nmHapus,
						iconCls: 'remove',
						handler: function()
						{
							if(cellSelectedtutup_kasirigd=='' || cellSelectedtutup_kasirigd=='undefined')
							{
								ShowPesanWarningKasirigd ('Pilih data tabel  ','Kasir');
							} else {	
								if(rowSelectedKasirigdKasir.data.CO_STATUS == 'f') {
									ShowPesanWarningKasirigd ('Transaksi sudah terbuka','Pembayaran');	
								} else {
									var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan Pembukaan Transaksi:', function(btn, combo){
									if (btn == 'ok')
												{
												variablebukahistori_igd=combo;
																if (combo!='')
																{
																 UpdateBukatransaksi();
																}
																else
																{
																	ShowPesanWarningKasirigd('Silahkan isi alasan terlebih dahulu','Keterangan');

																}
												}

										});
									 //UpdateBukatransaksi();
									//fnDlgIGDBukaTransaksiPasswordDulu();
								}
								
							}										
						},
								//disabled:true
					},{
						text: 'Batal Transaksi',
						id: 'btnBatalTransaksiKasirrwi',
						tooltip: nmHapus,
						iconCls: 'remove',
						handler: function()
						{
							if(cellSelectedtutup_kasirigd=='' || cellSelectedtutup_kasirigd=='undefined')
							{
								ShowPesanWarningKasirigd ('Pilih data tabel  ','Pembayaran');
							} else if(rowSelectedKasirigdKasir.data.CO_STATUS == 'f')
							{
								ShowPesanWarningKasirigd ('Transaksi Belum ditutup tidak bisa dibatalkan','Pembayaran');	
							} else if(rowSelectedKasirigdKasir.data.BATAL == 't')
							{
								ShowPesanWarningKasirigd ('Transaksi Sudah dibatalkan tidak bisa dibatalkan kembali','Pembayaran');	
							} else
								{
								fnDlgIGDPasswordDulu();
							  

							
								}
						},
						disabled:false
					}
                ]
            }
	);
	
	var LegendViewCMRequest = new Ext.Panel
	(
            {
            id: 'LegendViewCMRequest',
            region: 'center',
            border:false,
            bodyStyle: 'padding:0px 7px 0px 7px',
            layout: 'column',
            frame:true,
            //height:32,
            anchor: '100% 8.0001%',
            autoScroll:false,
            items:
            [
                {
                    columnWidth: .033,
                    layout: 'form',
                    style:{'margin-top':'-1px'},
                    //height:32,
                    anchor: '100% 8.0001%',
                    border: false,
                    html: '<img src="'+baseURL+'ui/images/icons/16x16/hijau.png" class="text-desc-legend"/>'
                },
                {
                    columnWidth: .08,
                    layout: 'form',
                    //height:32,
                    anchor: '100% 8.0001%',
                    style:{'margin-top':'1px'},
                    border: false,
                    html: " Lunas"
                },
                {
                    columnWidth: .033,
                    layout: 'form',
                    style:{'margin-top':'-1px'},
                    border: false,
                    //height:35,
                    anchor: '100% 8.0001%',
                    //html: '<img src="./images/icons/16x16/merah.png" class="text-desc-legend"/>'
                    html: '<img src="'+baseURL+'ui/images/icons/16x16/merah.png" class="text-desc-legend"/>'
                },
                {
                    columnWidth: .1,
                    layout: 'form',
                    //height:32,
                    anchor: '100% 8.0001%',
                    style:{'margin-top':'1px'},
                    border: false,
                    html: " Belum Lunas"
                }
            ]

        }
    )
 var GDtabDetailIGD = new Ext.TabPanel   
    (
        {
	        id:'GDtabDetailIGD',
			//region: 'center',
			activeTab: 0,
			anchor: '100% 90%',
			border:false,
			//plain: true,
			width: 300,
			//defaults:
			//{
			//	autoScroll: true
			//},
			items: [
				GetDTLTRHistoryGridIGD(),
				GetDTLTRIGDGrid()//GetDTLTRIGDGrid() 
				//-------------- ## --------------
			],
			listeners:
			{
			
			}
		}
		
    );
    var FormDepanKasirigd = new Ext.Panel
    (
        {
            id: mod_id,
            closable: true,
            region: 'center',
            layout: 'form',
            title: 'Kasir Gawat Darurat',
            border: false,
            shadhow: true,
           // autoScroll:false,
            iconCls: 'Request',
            margins: '0 5 5 0',
            items: [{
						   xtype:'panel',
							   plain:true,
							   activeTab: 0,
								height:135,
							   //deferredRender: false,
							   defaults:
							   {
								bodyStyle:'padding:10px',
								autoScroll: true
							   },
							   items:[
					 {
				    layout: 'form',
					 margins: '0 5 5 0',
					border: true ,
					items:
					[
				     {
                    xtype: 'textfield',
                    fieldLabel: ' No. Medrec' + ' ',
                    id: 'txtFilterNomedrec',
                    anchor : '100%',
                    onInit: function() { },
					listeners:
                        {
                            'specialkey' : function()
                            {
                                var tmpNoMedrec = Ext.get('txtFilterNomedrec').getValue()
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10 )
                                {
                                    if(tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10 )
                                        {
										   
                                             var tmpgetNoMedrec = formatnomedrec(Ext.get('txtFilterNomedrec').getValue())
                                             Ext.getCmp('txtFilterNomedrec').setValue(tmpgetNoMedrec);
                                            var tmpkriteria = getCriteriaFilter_viDaftar();
											 RefreshDataFilterKasirIgdKasir();
											 if(dsTRKasirigdKasirList.getCount() == 0){
												 ShowPesanInfoKasirigd('Pasien tidak ditemukan','Information');
											 }
                                      
                                        }
                                        else
                                            {
                                                if (tmpNoMedrec.length === 10)
                                                    {
                                                       // tmpkriteria = getCriteriaFilter_viDaftar();
                                                     RefreshDataFilterKasirIgdKasir();
													 if(dsTRKasirigdKasirList.getCount() == 0){
														 ShowPesanInfoKasirigd('Pasien tidak ditemukan','Information');
													 }
                                                    }
                                                    else
                                                    Ext.getCmp('txtFilterNomedrec').setValue('')
                                            }
                                }
                            }

                        }
                },
				
				{	 
				xtype: 'tbspacer',
				height: 3
				},	
					{
							xtype: 'textfield',
							fieldLabel: ' Pasien' + ' ',
							id: 'TxtFilterGridDataView_NAMA_viKasirigdKasir',
							anchor :'100%',
							 enableKeyEvents: true,
							listeners:
							{ 
						
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10 )
									{
										RefreshDataFilterKasirIgdKasir();
										if(dsTRKasirigdKasirList.getCount() == 0){
											ShowPesanInfoKasirigd('Pasien tidak ditemukan','Information');
										}
									}
								}
							}
						},
						{	 
						xtype: 'tbspacer',
						height: 3
						},	
						getItemPaneltgl_filter(),
						getItemPanelcombofilter()
						
						]}
						]},grListTRKasirigd,GDtabDetailIGD,LegendViewCMRequest]
           
        }
    );
	
  

   return FormDepanKasirigd

};
function fnDlgIGDBukaTransaksiPasswordDulu()
{
    winIGDBukaTransaksiPasswordDulu = new Ext.Window
    (
        {
            id: 'winIGDBukaTransaksiPasswordDulu',
            title: 'Password',
            closeAction: 'destroy',
            width:320,
            height: 120,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgIGDBukaTransaksiPasswordDulu()]

        }
    );

    winIGDBukaTransaksiPasswordDulu.show();
};
function ItemDlgIGDBukaTransaksiPasswordDulu()
{
    var PnlLapIGDSBukaTransaksiPasswordDulu = new Ext.Panel
    (
        {
            id: 'PnlLapIGDSTransaksiPasswordDulu',
            fileUpload: true,
            layout: 'form',
            width:170,
            height: 120,
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapIGDBukaTransaksiPasswordDulu_Tanggal(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                   // style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '100%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                         {
                            xtype: 'button',
                            text: 'Ok',
                            width: 70,
                            hideLabel: true,
							style:{'margin-left':'5px','margin-top':'0px'},
                            id: 'btnOkLapIGDBukaTransaksiPasswordDulu',
                            handler: function()
                            {
                               Ext.Ajax.request
								 (
									{
										//url: "./Datapool.mvc/CreateDataObj",
										url: baseURL + "index.php/main/functionIGD/cekpasswordbukatrans_igd",
										params: {passDulu:Ext.getCmp('TxBukaTransaksiPasswordDuluIGD').getValue()},
										failure: function(o)
										{
											ShowPesanWarningKasirigd('Proses Gagal segera Hubungi Admin', 'Gagal');
										//RefreshDataKasirRwiDetail(notransaksi);
										},
										success: function(o)
										{
											//RefreshDataKasirRwiDetail(notransaksi);
											var cst = Ext.decode(o.responseText);
											if (cst.success === true)
											{
												winIGDBukaTransaksiPasswordDulu.close();
													var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan Pembukaan Transaksi:', function(btn, combo){
													if (btn == 'ok')
																{
																variablebukahistori_igd=combo;
																				if (combo!='')
																				{
																				 UpdateBukatransaksi();
																				}
																				else
																				{
																					ShowPesanWarningKasirigd('Silahkan isi alasan terlebih dahulu','Keterangan');

																				}
																}

														}); 
											}
											else
											{
													ShowPesanWarningKasirigd('Password salah segera Hubungi Admin', 'Gagal');
											};
										}
									}
								)
							   /* */

								    
                            }
                        },
                        {
                            xtype: 'button',
                            text: 'Cancel' ,
                            width: 70,
                            hideLabel: true,
							style:{'margin-left':'5px','margin-top':'0px'},
                            id: 'btnCancelLapIGDBukaTransaksiPasswordDulu',
                            handler: function()
                            {
                                    winIGDBukaTransaksiPasswordDulu.close();
                            }
                        } 
                    ]
                }
            ]
        }
    );

    return PnlLapIGDSBukaTransaksiPasswordDulu;
};
function getItemLapIGDBukaTransaksiPasswordDulu_Tanggal()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
		
		
		{
			    columnWidth: .90,
			    layout: 'form',
				 labelWidth:120,
				  border: false,
				 
			    items:
				[
					{
						xtype: 'textfield',
						inputType: 'password',
						fieldLabel: 'Masukan Password ',
						id: 'TxBukaTransaksiPasswordDuluIGD',
					    anchor: '99%'
					},
				
					
				]
			},
     
        ]
    }
    return items;
};

function fnDlgIGDPasswordDulu()
{
    winIGDPasswordDulu = new Ext.Window
    (
        {
            id: 'winIGDPasswordDulu',
            title: 'Password',
            closeAction: 'destroy',
            width:320,
            height: 120,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgIGDPasswordDulu()]

        }
    );

    winIGDPasswordDulu.show();
};
function ItemDlgIGDPasswordDulu()
{
    var PnlLapIGDSPasswordDulu = new Ext.Panel
    (
        {
            id: 'PnlLapIGDSPasswordDulu',
            fileUpload: true,
            layout: 'form',
            width:170,
            height: 120,
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapIGDPasswordDulu_Tanggal(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                   // style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '100%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                         {
                            xtype: 'button',
                            text: 'Ok',
                            width: 70,
                            hideLabel: true,
							style:{'margin-left':'5px','margin-top':'0px'},
                            id: 'btnOkLapIGDPasswordDulu',
                            handler: function()
                            {
                               Ext.Ajax.request
								 (
									{
										//url: "./Datapool.mvc/CreateDataObj",
										url: baseURL + "index.php/main/functionIGD/cekpassword_igd",
										params: {passDulu:Ext.getCmp('TxPasswordDuluIGD').getValue()},
										failure: function(o)
										{
											ShowPesanWarningKasirigd('Proses Gagal segera Hubungi Admin', 'Gagal');
										//RefreshDataKasirRwiDetail(notransaksi);
										},
										success: function(o)
										{
											//RefreshDataKasirRwiDetail(notransaksi);
											var cst = Ext.decode(o.responseText);
											if (cst.success === true)
											{
												winIGDPasswordDulu.close();
													var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan Pembatalan Transaksi:', function(btn, combo){
													if (btn == 'ok')
																{
																variablebatalhistori_igd=combo;
																				if (variablehistori!='')
																				{
																				 BatalTransaksi(false);
																				}
																				else
																				{
																					ShowPesanWarningKasirigd('Silahkan isi alasan terlebih dahulu','Keterangan');

																				}
																}

														}); 
											}
											else
											{
													ShowPesanWarningKasirigd('Password salah segera Hubungi Admin', 'Gagal');
											};
										}
									}
								)
							   /* */

								    
                            }
                        },
                        {
                            xtype: 'button',
                            text: 'Cancel' ,
                            width: 70,
                            hideLabel: true,
							style:{'margin-left':'5px','margin-top':'0px'},
                            id: 'btnCancelLapIGDPasswordDulu',
                            handler: function()
                            {
                                    winIGDPasswordDulu.close();
                            }
                        } 
                    ]
                }
            ]
        }
    );

    return PnlLapIGDSPasswordDulu;
};
function getItemLapIGDPasswordDulu_Tanggal()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
		
		
		{
			    columnWidth: .90,
			    layout: 'form',
				 labelWidth:120,
				  border: false,
				 
			    items:
				[
					{
						xtype: 'textfield',
						inputType: 'password',
						fieldLabel: 'Masukan Password ',
						id: 'TxPasswordDuluIGD',
					    anchor: '99%'
					},
				
					
				]
			},
     
        ]
    }
    return items;
};
function TransferLookUp(rowdata) 
{
    var lebar = 440;
    FormLookUpsdetailTRTransfer_IGD = new Ext.Window
    (
        {
            id: 'gridTransfer',
            title: 'Transfer Rawat Inap',
            closeAction: 'destroy',
            width: lebar,
            height: 410,
            border: false,
            resizable: false,
            plain: false,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryTRTransfer(lebar),
			fbar:[
				{
					xtype:'button',
					text:'Simpan',
					width:70,
					style:{'margin-left':'0px','margin-top':'0px'},
					hideLabel:true,
					id: 'btnOkTransfer',
					handler:function()
					{
				
						TransferData_IGD(false);
						
					}
				},
				{
						xtype:'button',
						text:'Tutup',
						width:70,
						hideLabel:true,
						id: 'btnCancelTransfer',
						handler:function() 
						{
							FormLookUpsdetailTRTransfer_IGD.close();
						}
				}
			],
            listeners:
            {
                
            }
        }
    );

    FormLookUpsdetailTRTransfer_IGD.show();
  //  Transferbaru();

};
function getFormEntryTRTransfer(lebar) 
{
    var pnlTRTransfer = new Ext.FormPanel
    (
        {
            id: 'PanelTRTransfer',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:410,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [getItemPanelInputTransfer(lebar),getItemPanelButtonTransfer(lebar)],
			tbar:
            [
               
               
            ]
        }
    );
 
    var FormDepanTransfer = new Ext.Panel
	(
		{
		    id: 'FormDepanTransfer',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [pnlTRTransfer	
				
			]

		}
	);

    return FormDepanTransfer
};

function getItemPanelInputTransfer(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 0px',
		border:false,
		height:320,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
				
			    layout: 'form',
				height:320,
			    border: false,
			    items:
				[
					getTransfertujuan(lebar),
					{	 
						xtype: 'tbspacer',
						height: 5
					},	
					getItemPanelNoTransksiTransfer(lebar),
					{	 
						xtype: 'tbspacer',									
						height:3
					},
					mComboalasan_transfer()
				]
			},
		]
	};
    return items;
};
function getTransfertujuan(lebar) 
{
    var items =
	{
		Width		: lebar-2,
		height		: 150,
		layout		: 'form',
		border		: true,
		labelWidth	: 130,		
	    items:
		[
			{	 
				xtype: 'tbspacer',
				height: 2
			},{
			xtype 		: 'textfield',
			fieldLabel 	: 'No Transaksi',
			name 		: 'txtTranfernoTransaksi',
			id 			: 'txtTranfernoTransaksi',
			labelWidth 	: 130,
			width 		: 100,
			anchor 		: '95%'
			},{
				xtype: 'datefield',
				fieldLabel: 'Tanggal ',
				id: 'dtpTanggalTransfertransaksi',
				name: 'dtpTanggalTransfertransaksi',
				format: 'd/M/Y',
				readOnly : true,
				labelWidth:130,
				width: 100,
				anchor: '95%'
			},{
                                        xtype: 'textfield',
                                        fieldLabel: 'No Medrec',
                                        //maxLength: 200,
                                        name: 'txtTranfernomedrec',
                                        id: 'txtTranfernomedrec',
										labelWidth:130,
                                        width: 100,
                                        anchor: '95%'
                                     },
									 {
                                        xtype: 'textfield',
                                        fieldLabel: 'Nama Pasien',
                                        //maxLength: 200,
                                        name: 'txtTranfernamapasien',
                                        id: 'txtTranfernamapasien',
										labelWidth:130,
                                        width: 100,
                                        anchor: '95%'
                                     },{
                                        xtype: 'textfield',
                                        fieldLabel: 'Unit Perawatan',
                                        //maxLength: 200,
                                        name: 'txtTranferunit',
                                        id: 'txtTranferunit',
										labelWidth:130,
                                        width: 100,
										hidden : true,
                                        anchor: '95%'
                                     },{
                                        xtype: 'textfield',
                                        fieldLabel: 'Unit Perawatan',
                                        //maxLength: 200,
                                        name: 'txtTranferkelaskamarigd',
                                        id: 'txtTranferkelaskamarigd',
										readOnly : true,
										labelWidth:130,
                                        width: 100,
                                        anchor: '95%'
                                     }
		
					
					
				//]
			//}
			
		]
	}
    return items;
};

function getItemPanelNoTransksiTransfer(lebar) 
{
    var items =
	{
		Width:lebar,
		height:110,
	    layout: 'column',
	    border: true,
		
		
	    items:
		[
			{
				columnWidth	: .990,
				layout		: 'form',
				Width		: lebar-10,
				labelWidth	: 130,
				border		: false,
				items		:
				[
					{	 
						xtype	: 'tbspacer',
						height	: 3
					},{
						xtype		: 'textfield',
						fieldLabel	: 'Jumlah Biaya',
						maxLength	: 200,
						style		: {'text-align':'right'},
						name		: 'txtjumlahbiayasal_KasirIGD',
						id			: 'txtjumlahbiayasal_KasirIGD',
						width		: 100,
						readOnly 	: true,
						anchor		: '95%'
					},{
						xtype 		: 'textfield',
						fieldLabel 	: 'Paid',
						maxLength 	: 200,
						style 		: {'text-align':'right'},
						name 		: 'txtpaid_KasirIGD',
						id 			: 'txtpaid_KasirIGD',
						width 		: 100,
						value 		: 0,
						readOnly 	: true,
						anchor 		: '95%'
					},{
						xtype 		: 'textfield',
						fieldLabel 	: 'Jumlah dipindahkan',
						maxLength 	: 200,
						style 		: {'text-align':'right'},
						name 		: 'txtjumlahtranfer_KasirIGD',
						id 			: 'txtjumlahtranfer_KasirIGD',
						readOnly 	: false,
						enableKeyEvents:true,
						width 		: 100,
						anchor 		: '95%',
						listeners:
						{ 
							keyup:function(text,e)
							{
								getsaldotagihan_KasirIGD();
							}
						}
					},{
						xtype 		: 'textfield',
						fieldLabel 	: 'Saldo tagihan',
						maxLength 	: 200,
						style 		: {'text-align':'right'},
						name 		: 'txtsaldotagihan_KasirIGD',
						id 			: 'txtsaldotagihan_KasirIGD',
						value 		: 0,
						readOnly 	: true,
						width 		: 100,
						anchor 		: '95%'
					}
				]
			}
			
		]
	}
    return items;
};

function getItemPanelButtonTransfer(lebar) 
{
    var items =
	{
		layout	: 'column',
		border	: false,
		height	: 30,
		anchor	: '100%',
		style 	: {'margin-top':'-1px'},
		items 	:
		[
			{
				layout		: 'hBox',
				width		: 400,
				border		: false,
				bodyStyle	: 'padding:5px 0px 5px 5px',
				defaults	: { margins: '3 3 3 3' },
				anchor		: '90%',
				layoutConfig: 
				{
					align 	: 'middle',
					pack 	:'end'
				},
				items:
				[
					
				]
			}
		]
	}
    return items;
};
function RefreshDataKasirIGDDetail(no_transaksi) 
{
	loadMask.show();
	var strKriteriaIGD='';
	strKriteriaIGD = "no_transaksi= ~" + no_transaksi + "~ and kd_kasir=~06~";
	dsTRDetailKasirIGDList.load
	(
		{
			params:
			{
				Skip	: 0,
				Take	: 1000,
				Sort	: 'tgl_transaksi',
				Sortdir	: 'ASC',
				target	: 'ViewDetailTRRWJ',
				param	: strKriteriaIGD
			},	
			callback:function(){
				loadMask.hide();
			}	
		}
	);
	return dsTRDetailKasirIGDList;
};

function getParamDataDeleteKasirIGDDetail()
{

    var params =
    {
		Table 			: 'ViewTrKasirIGD',
		TrKodeTranskasi : notransaksi,
		TrTglTransaksi 	: CurrentKasirIGD.data.data.TGL_TRANSAKSI,
		TrKdPasien 		: CurrentKasirIGD.data.data.KD_PASIEN,
		kodePasien 		: CurrentKD_PASIENIGD,
		TrKdNamaPasien 	: namapasien,	
		TrKdUnit 		: kodeunit,
		TrNamaUnit 		: namaunit,
		Uraian 			: CurrentKasirIGD.data.data.DESKRIPSI2,
		AlasanHapus 	: variablehistori,
		TrHarga 		: CurrentKasirIGD.data.data.HARGA,
		TrKdProduk 		: CurrentKasirIGD.data.data.KD_PRODUK,
		RowReq 			: CurrentKasirIGD.data.data.URUT,
		Hapus 			: 2
    };
	
    return params
};

function GetDTLTRIGDGrid() 
{
    var fldDetail = ['KP_PRODUK','KD_PRODUK','DESKRIPSI','DESKRIPSI2','KD_TARIF','HARGA','QTY','DESC_REQ','TGL_BERLAKU','NO_TRANSAKSI','URUT',
					'DESC_STATUS','TGL_TRANSAKSI','STATUS_KONSULTASI','STATUS_PENINDAK','JUMLAH_DOKTER','KD_UNIT'];
	
    dsTRDetailKasirIGDList = new WebApp.DataStore({ fields: fldDetail })
  // RefreshDataKasirIGDDetail() ;
    gridDTLTRIGD = new Ext.grid.EditorGridPanel
    (
        {
			title		: 'Input Produk',
			stripeRows	: true,
			store		: dsTRDetailKasirIGDList,
			border		: false,
			columnLines	: true,
			frame		: false,
			anchor		: '100%',
			autoScroll	: true,
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                        	//console.log(dsTRDetailKasirIGDList.getAt(row));
							cellSelecteddeskripsi                         = dsTRDetailKasirIGDList.getAt(row);
							CurrentKasirIGD.row                           = row;
							CurrentKasirIGD.data                          = cellSelecteddeskripsi;
							CurrentRowPemeriksaanIGD                      = row;
							// FocusCtrlCMIGD                             ='txtAset';
							// getProdukIGD()
							currentJasaDokterUrutDetailTransaksi_KasirIGD = cellSelecteddeskripsi.data.URUT;
							currentJasaDokterKdProduk_KasirIGD            =  cellSelecteddeskripsi.data.KD_PRODUK;
							currentJasaDokterKdTarif_KasirIGD             = cellSelecteddeskripsi.data.KD_TARIF;
							currentJasaDokterHargaJP_KasirIGD             = cellSelecteddeskripsi.data.HARGA;
							currentTOTAL_KasirIGD             			  = parseInt(cellSelecteddeskripsi.data.QTY) * parseInt(cellSelecteddeskripsi.data.HARGA);
							
							
                        }
                    }
                }
            ),
            cm: TRGawatDaruratColumModel(),
			viewConfig: {forceFit: true},
			        tbar:
                [
                       
                       {
                                id:'btnTambahBrsKasirIGD',
                                text: 'Tambah baris',
                                tooltip: nmTambahBaris,
                                iconCls: 'AddRow',
								disabled :true,
                                handler: function()
                                {
                                        TambahBarisIGD();
                                }
                        },
                      
						{
							text: 'Tambah Produk',
							id: 'btnLookupIGD',
							tooltip: nmLookup,
							iconCls: 'find',
							hidden : true,
							disabled :true,
							handler: function()
							{
								var p = RecordBaruRWJ();
									var str='';
									//if (Ext.get('txtKdUnitIGD').dom.value  != undefined && Ext.get('txtKdUnitIGD').dom.value  != '')
									//{
											//str = ' where kd_dokter =~' + Ext.get('txtKdDokter').dom.value  + '~';
											//str = "\"kd_dokter\" = ~" + Ext.get('txtKdDokter').dom.value  + "~";
											str =  kodeunit;
									//};
									FormLookupKasirRWJ(str, dsTRDetailKasirIGDList, p, true, '', true);
							}
						},
                       
							{
								text: 'Simpan',
								id: 'btnSimpanIGD',
								tooltip: nmSimpan,
								iconCls: 'save',
								hidden:true,
								disabled :true,
								handler: function(){	
									Datasave_KasirIGD(false,false);
								}
							},{
                                id:'btnHpsBrsIGD',
                                text: 'Hapus Baris',
                                tooltip: 'Hapus Baris',
								disabled :true,
                                iconCls: 'RemoveRow',
                                handler: function()
                                {

                                        if (dsTRDetailKasirIGDList.getCount() > 0 )
                                        {
                                                if (cellSelecteddeskripsi != undefined)
                                                {
                                                        if(CurrentKasirIGD != undefined)
                                                        {
                                                                HapusBarisIGD();
                                                        }
                                                }
                                                else
                                                {
                                                        ShowPesanWarningKasirigd('Pilih record ','Hapus data');
                                                }
                                        }
                                }
                        },
						'-',
						{
							id		:'btnLookUpEditDokterPenindak_KasirIGD',
							text	: 'Edit Dokter Penindak',
							iconCls	: 'Edit_Tr',
							hidden	: true,
							handler	: function(){
								if(cellSelecteddeskripsi == '' || cellSelecteddeskripsi == undefined){
									ShowPesanWarningKasirigd('Pilih item dokter penindak yang akan diedit!','Error');
								} else{
									if(currentJasaDokterUrutDetailTransaksi_KasirIGD == 0 || currentJasaDokterUrutDetailTransaksi_KasirIGD == undefined){
										ShowPesanErrorKasirigd('Item ini belum ada dokter penindaknya!','Error');
									} else{
										PilihDokterLookUp_KasirIGD(true);
									}
								}
							}
						},
						
                ]
                //, viewConfig:{forceFit: true}
        }
		
		
    );
	
	

    return gridDTLTRIGD;
};

function TambahBarisIGD()
{
    var x=true;

    if (x === true)
    {
        var p = RecordBaruRWJ();
        dsTRDetailKasirIGDList.insert(dsTRDetailKasirIGDList.getCount(), p);
    };
};

function Datasave_KasirIGD(mBol,jasa,alert_simpan,tambah_baris,refresh_filter) {	
	Ext.Ajax.request({
		url: baseURL + "index.php/main/functionIGD/savedetailproduk",
		params: getParamDetailTransaksiIGD(),
		failure: function(o){
			ShowPesanWarningKasirigd('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
			if(mBol === false){
				RefreshDataKasirIGDDetail(notransaksi);
			}
		},	
		success: function(o){
			//RefreshDataKasirIGDDetail(notransaksi);
			var cst = Ext.decode(o.responseText);
			if (cst.success === true){
				if(alert_simpan==undefined || alert_simpan==true){
					ShowPesanInfoKasirigd('Simpan tindakan berhasil','Information');
				}
				//Datasave_KasirIGD_SQL();
				if(tambah_baris==undefined || tambah_baris==true){
					TambahBarisIGD();
				}
				if(jasa == true){
					SimpanJasaDokterPenindak_KasirIGD(currentJasaDokterKdProduk_KasirIGD,currentJasaDokterKdTarif_KasirIGD,currentJasaDokterUrutDetailTransaksi_KasirIGD,currentJasaDokterHargaJP_KasirIGD);
				} 
				if(refresh_filter==undefined || refresh_filter==true){
					RefreshDataFilterKasirIgdKasir();
				}
				if(mBol === false){
					RefreshDataKasirIGDDetail(notransaksi);
				}
			}else{
				ShowPesanWarningKasirigd('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
			}
		}
	})
}



function TransferData_IGD(mBol) 
{	
	Ext.Ajax.request
	 (
		{
			url: baseURL + "index.php/main/functionIGD/saveTransfer",
			params: getParamTransferRwi(),
			failure: function(o)
			{
				ShowPesanWarningKasirigd('Error transfer pembayaran. Hubungi Admin!', 'Gagal');
				RefreshDataKasirIGDDetail(notransaksi);
			},	
			success: function(o) 
			{
				RefreshDataKasirIGDDetail(notransaksi);
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					ShowPesanInfoKasirigd('Transfer berhasil','Information');
					Ext.getCmp('btnTransferKasirigd').disable();
					//Ext.getCmp('btnSimpanKasirigd').disable();
					RefreshDataKasirigdKasirDetail(Ext.get('txtNoTransaksiKasirigdKasir').dom.value);
					RefreshDataFilterKasirIgdKasir();
					FormLookUpsdetailTRTransfer_IGD.close();
					if(mBol === false)
					{
						RefreshDataKasirIGDDetail(notransaksi);
					};
				}
				else 
				{
					ShowPesanWarningKasirigd('Gagal melakukan transfer!', 'Gagal');
				};
			}
		}
	)
};



function RecordBaruRWJ()
{

	var p = new mRecordIGD
	(
		{
			'DESKRIPSI2':'',
			'KD_PRODUK':'',
		    'DESKRIPSI':'', 
		    'KD_TARIF':'', 
		    'HARGA':'',
		    'QTY':'',
		    'TGL_TRANSAKSI':tanggaltransaksitampung, 
		    'DESC_REQ':'',
		    'KD_TARIF':'',
		    'URUT':''
		}
	);
	
	return p;
};

function getParamDetailTransaksiIGD() 
{
    var params =
	{
		Table:'ViewTrKasirIGD',

		TrKodeTranskasi: notransaksi,
		KdUnit: kodeunit,
		//DeptId:Ext.get('txtKdDokter').dom.value ,tampungshiftsekarang
		Tgl: tgltrans,
		kdDokter:CurrentKDDokerIGD,
		urut_masuk:CurrentURUT_MASUKIGD,
		kd_pasien:CurrentKD_PASIENIGD,
		Shift: tampungshiftsekarang,
		List:getArr2DetailTrIGD(),
		//JP:dsTRDetailKasirIGDList.data.items[CurrentRowPemeriksaanIGD].data.HARGA,
		JmlField: mRecordIGD.prototype.fields.length-4,
		JmlList:GetListCoun2tDetailTransaksi(),
		Hapus:1,
		Ubah:0
	};
    return params
};

function getParamTransferRwi() 
{
    var params =
	{
	//,,,,,,,,,shift,status_bayar) 
		//Table:'ViewTrKasirRwi',
        KDkasirIGD:kodekasir,
		TrKodeTranskasi: notransaksi,
		KdUnit: kodeunit,
		Kdpay: 'T1',
		// Jumlahtotal: Ext.get(txtjumlahbiayasal_KasirIGD).dom.value,
		Jumlahtotal: Ext.get(txtjumlahtranfer_KasirIGD).dom.value,
		Tglasal:  ShowDate(tgltrans),
		Shift: tampungshiftsekarang,
		TRKdTransTujuan:Ext.get(txtTranfernoTransaksi).dom.value,
		KdpasienIGDtujuan: Ext.get(txtTranfernomedrec).dom.value,
		TglTranasksitujuan : Ext.get(dtpTanggalTransfertransaksi).dom.value,
		KDunittujuan : Trkdunit2,
		KDalasan :Ext.get(cboalasan_transfer).dom.value,
		KasirRWI:'05',
		Kdcustomer:kdcustomeraa,
		kodeunitkamar:tmp_kodeunitkamarigd,
		kdspesial:tmp_kdspesialigd,
		nokamar:tmp_nokamarigd,
		
	
	};
	params['jumlah'] = dsTRDetailKasirigdKasirList.getCount();
	for(var i = 0 ; i < dsTRDetailKasirigdKasirList.getCount();i++)
	{
		if(dsTRDetailKasirigdKasirList.data.items[i].data.BAYARTR != 0 || dsTRDetailKasirigdKasirList.data.items[i].data.BAYARTR!= undefined){
			params['harga-'+i]=dsTRDetailKasirigdKasirList.data.items[i].data.BAYARTR;
			params['urut-'+i]=dsTRDetailKasirigdKasirList.data.items[i].data.URUT;
			params['tgl_transaksi-'+i]=dsTRDetailKasirigdKasirList.data.items[i].data.TGL_TRANSAKSI;
		} else if(dsTRDetailKasirigdKasirList.data.items[i].data.BAYARTR != 0 || dsTRDetailKasirigdKasirList.data.items[i].data.BAYARTR!= undefined){
			params['harga-'+i]=dsTRDetailKasirigdKasirList.data.items[i].data.PIUTANG;
			params['urut-'+i]=dsTRDetailKasirigdKasirList.data.items[i].data.URUT;
			params['tgl_transaksi-'+i]=dsTRDetailKasirigdKasirList.data.items[i].data.TGL_TRANSAKSI;
		} else{
			params['harga-'+i]=dsTRDetailKasirigdKasirList.data.items[i].data.DISCOUNT;
			params['urut-'+i]=dsTRDetailKasirigdKasirList.data.items[i].data.URUT;
			params['tgl_transaksi-'+i]=dsTRDetailKasirigdKasirList.data.items[i].data.TGL_TRANSAKSI;
		}
	}
    return params
};
function getArr2DetailTrIGD()
{
	var x='';
	console.log(dsTRDetailKasirIGDList.data);
	for(var i = 0 ; i < dsTRDetailKasirIGDList.getCount();i++)
	{
		if (dsTRDetailKasirIGDList.data.items[i].data.KD_PRODUK != '' && dsTRDetailKasirIGDList.data.items[i].data.DESKRIPSI != '')
		{
			var y='';
			var z='@@##$$@@';
			console.log(dsTRDetailKasirIGDList.data.items[i].data.KD_PRODUK);
			y = 'URUT=' + dsTRDetailKasirIGDList.data.items[i].data.URUT
			y += z + dsTRDetailKasirIGDList.data.items[i].data.KD_PRODUK
			y += z + dsTRDetailKasirIGDList.data.items[i].data.QTY
			y += z + ShowDate(dsTRDetailKasirIGDList.data.items[i].data.TGL_BERLAKU)
			y += z +dsTRDetailKasirIGDList.data.items[i].data.HARGA
			y += z +dsTRDetailKasirIGDList.data.items[i].data.KD_TARIF
			y += z +dsTRDetailKasirIGDList.data.items[i].data.URUT
			y += z + ShowDate(dsTRDetailKasirIGDList.data.items[i].data.TGL_TRANSAKSI);
			y += z + dsTRDetailKasirIGDList.data.items[i].data.STATUS_KONSULTASI;
			y += z + dsTRDetailKasirIGDList.data.items[i].data.KD_UNIT;

			
			if (i === (dsTRDetailKasirIGDList.getCount()-1))
			{
				x += y 
			}
			else
			{
				x += y + '##[[]]##'
			};
		};
	}	
	
	return x;
};

function GetListCoun2tDetailTransaksi()
{
	
	var x=0;
	for(var i = 0 ; i < dsTRDetailKasirIGDList.getCount();i++)
	{
		if (dsTRDetailKasirIGDList.data.items[i].data.KD_PRODUK != '' || dsTRDetailKasirIGDList.data.items[i].data.DESKRIPSI  != '')
		{
			x += 1;
		};
	}
	return x;
	
};
function senderProdukIGD(no_transaksi, kd_produk, kd_tarif, kd_kasir, urut, unit, tgl_transaksi, tgl_berlaku, quantity, harga, fungsi){
    Ext.Ajax.request({
        url     : baseURL + "index.php/main/functionIGD/insertDataProduk",
        params  :  {
            no_transaksi    : no_transaksi,
            kd_produk       : kd_produk,
            kd_tarif        : kd_tarif,
            kd_kasir        : kd_kasir,
            urut            : parseInt(urut)+1,
            unit            : unit,
            tgl_transaksi   : tgl_transaksi,
            tgl_berlaku     : tgl_berlaku,
            quantity        : quantity,
            harga           : harga,
            fungsi          : fungsi,
			kd_dokter		: CurrentKDDokerIGD
        },
        failure : function(o)
        {

        },
        success : function(o)
        {
            //tmpRowGrid = PenataJasaKasirRWI.form.Grid.produk.getSelectionModel().selection.cell[0];
            var cst = Ext.decode(o.responseText);
            //console.log(cst.action);
           /* if (cst.action == 'insert') {
                PenataJasaKasirRWI.dsGridTindakan.insert(PenataJasaKasirRWI.dsGridTindakan.getCount(), PenataJasaKasirRWI.func.getNullProduk());
            }else{
                PenataJasaKasirRWI.form.Grid.produk.startEditing(tmpRowGrid+1, 3);
            }*/

            //PenataJasaKasirRWI.dsGridTindakan.insert(PenataJasaKasirRWI.dsGridTindakan.getCount(), PenataJasaKasirRWI.func.getNullProduk());
            //PenataJasaKasirRWI.form.Grid.produk.startEditing(line+1, 3);
        }
    });
};
function TRGawatDaruratColumModel() {
    return new Ext.grid.ColumnModel([
		new Ext.grid.RowNumberer(),
		{
			id			: 'colstatuskonsulrwj',
			header		: 'KONSUL',
			dataIndex	: 'STATUS_KONSULTASI',
			menuDisabled: true,
			hidden 		: true

		},{
			id			: 'coleskripsiIGD',
			header		: 'Uraian',
			dataIndex	: 'DESKRIPSI2',
			width		: 100,
			menuDisabled: true,
			hidden 		: true
			
		},{
			id 			: 'colKdProduk',
			header 		: 'Kode Produk',
			dataIndex 	: 'KD_PRODUK',
			width 		:100,
			menuDisabled:true,
			hidden 		:true
		},{
			id			: 'colKpProduk',
			header		: 'Kode Produk',
			dataIndex	: 'KP_PRODUK',
			width		: 100,
			menuDisabled: true,
			editor 		: new Ext.form.TextField({
				id 					: 'fieldcolKdProduk',
				allowBlank 			: true,
				enableKeyEvents  	: true,
				width 				: 30,
				listeners 			:{ 
					'specialkey' : function(a, b){
						if (b.getKey() === b.ENTER || b.getKey() === b.TAB) {
							a.setValue(Ext.get('fieldcolKdProduk').dom.value.toUpperCase());
							Ext.Ajax.request({
								store	: KasirIGDDataStoreProduk,
								url 	: baseURL + "index.php/main/functionIGD/getProdukKey",
								params 	:  {
									kd_unit 		: kodeunit,
									kd_customer 	: kdcustomeraa,
									text 			: Ext.getCmp('fieldcolKdProduk').getValue(),
								},
								failure : function(o){
									ShowPesanWarningKasirigd("Data produk tidak ada!",'WARNING');
								},
								success : function(o){
									var cst = Ext.decode(o.responseText);
									if (cst.processResult == 'SUCCESS'){
										var lineIGD = gridDTLTRIGD.getSelectionModel().selection.cell[0];
										kd_produk       	= cst.listData.kd_produk;
										kp_produk       	= cst.listData.kp_produk;
										kd_tarif        	= cst.listData.kd_tarif;
										deskripsi 			= cst.listData.deskripsi;
										harga				= cst.listData.tarifx;
										tgl_berlaku			= cst.listData.tgl_berlaku;
										jumlah				= cst.listData.jumlah;
										status_konsultasi 	= cst.listData.status_konsultasi;
										dsTRDetailKasirIGDList.getRange()[lineIGD].set('DESKRIPSI', cst.listData.deskripsi);
										dsTRDetailKasirIGDList.getRange()[lineIGD].set('QTY', 1);
										dsTRDetailKasirIGDList.getRange()[lineIGD].set('HARGA', toInteger(cst.listData.tarifx));

										dsTRDetailKasirIGDList.data.items[lineIGD].data.KD_PRODUK         = cst.listData.kd_produk;
										dsTRDetailKasirIGDList.data.items[lineIGD].data.KP_PRODUK         = cst.listData.kp_produk;
										dsTRDetailKasirIGDList.data.items[lineIGD].data.KD_KLAS           = cst.listData.KD_KLAS;
										dsTRDetailKasirIGDList.data.items[lineIGD].data.KD_UNIT           = cst.listData.kd_unit;
										dsTRDetailKasirIGDList.data.items[lineIGD].data.HARGA             = toInteger(cst.listData.tarifx);
										dsTRDetailKasirIGDList.data.items[lineIGD].data.DESKRIPSI         = cst.listData.deskripsi;
										currentJasaDokterHargaJP_KasirIGD                                 = toInteger(cst.listData.tarifx);
										dsTRDetailKasirIGDList.data.items[lineIGD].data.TGL_BERLAKU       = cst.listData.tgl_berlaku;
										dsTRDetailKasirIGDList.data.items[lineIGD].data.KD_TARIF          = cst.listData.kd_tarif;
										dsTRDetailKasirIGDList.data.items[lineIGD].data.TGL_TRANSAKSI     = nowTglTransaksiIGD2;
										dsTRDetailKasirIGDList.data.items[lineIGD].data.STATUS_KONSULTASI = cst.listData.status_konsultasi;
										dsTRDetailKasirIGDList.data.items[lineIGD].data.QTY               = 1;
										gridDTLTRIGD.startEditing(CurrentKasirIGD.row, 9);
										var igdUrut=0;
										if(lineIGD>=1){
											igdUrut=toInteger(dsTRDetailKasirIGDList.data.items[lineIGD-1].data.URUT); 
										}
										dsTRDetailKasirIGDList.data.items[lineIGD].data.URUT               = igdUrut+1;
										senderProdukIGD(
											notransaksi, 
											kd_produk, 
											kd_tarif, 
											CurrentKdKasir_KasirIGD, 
											igdUrut, 
											cst.listData.kd_unit, 
											nowTglTransaksiIGD2,
											tgl_berlaku,
											1,
											harga,
											true
										);
									}else{
										ShowPesanWarningKasirigd('Data produk tidak ada!','WARNING');
									}
								}
							})
						}
					}
				}
			})
		},{
			id			: 'colUrut',
			header		: 'Urut',
			dataIndex	: 'URUT',
			width		: 100,
			menuDisabled: true,
			hidden		: true
		},{
			id 		 	: 'colDeskripsiIGD',
			header 	 	: 'Nama Produk',
			dataIndex	: 'DESKRIPSI',
			sortable	: false,
			hidden		: false,
			menuDisabled: true,
			width		: 220,
			editor: KasirIGDComboBoxKdProduk = Nci.form.Combobox.autoComplete({
				store	: KasirIGDDataStoreProduk,
				select	: function(a,b,c){
					var lineIGD = gridDTLTRIGD.getSelectionModel().selection.cell[0];
					dsTRDetailKasirIGDList.getRange()[lineIGD].set('DESKRIPSI', cst.listData.deskripsi);
					dsTRDetailKasirIGDList.getRange()[lineIGD].set('QTY', 1);
					dsTRDetailKasirIGDList.getRange()[lineIGD].set('HARGA', toInteger(b.data.harga));
					dsTRDetailKasirIGDList.data.items[lineIGD].data.KD_PRODUK         = b.data.kd_produk;
					dsTRDetailKasirIGDList.data.items[lineIGD].data.KP_PRODUK         = b.data.kp_produk;
					dsTRDetailKasirIGDList.data.items[lineIGD].data.KD_KLAS           = b.data.KD_KLAS;
					dsTRDetailKasirIGDList.data.items[lineIGD].data.HARGA             = b.data.harga;
					dsTRDetailKasirIGDList.data.items[lineIGD].data.KD_UNIT           = b.data.kd_unit;
					currentJasaDokterHargaJP_KasirIGD                                  = b.data.harga;
					dsTRDetailKasirIGDList.data.items[lineIGD].data.TGL_BERLAKU       = b.data.tgl_berlaku;
					dsTRDetailKasirIGDList.data.items[lineIGD].data.KD_TARIF          = b.data.kd_tarif;
					dsTRDetailKasirIGDList.data.items[lineIGD].data.TGL_TRANSAKSI     = nowTglTransaksiIGD2;
					dsTRDetailKasirIGDList.data.items[lineIGD].data.STATUS_KONSULTASI = b.data.status_konsultasi;
					dsTRDetailKasirIGDList.data.items[lineIGD].data.QTY               = 1;
					var igdUrut=0;
					if(lineIGD>=1){
						igdUrut=toInteger(dsTRDetailKasirIGDList.data.items[lineIGD-1].data.URUT); 
					}
					dsTRDetailKasirIGDList.data.items[lineIGD].data.URUT               = igdUrut+1;
					if (b.data.status_konsultasi==true){
						var ayaan='ya';
						cekKomponen(b.data.kd_produk,b.data.kd_tarif,kodeunit,igdUrut,b.data.harga,ayaan);
					}else{
						gridDTLTRIGD.startEditing(lineIGD, 9);
						senderProdukIGD(
							notransaksi, 
							b.data.kd_produk, 
							b.data.kd_tarif, 
							CurrentKdKasir_KasirIGD, 
							igdUrut, 
							b.data.kd_unit, 
							nowTglTransaksiIGD2,
							b.data.tgl_berlaku,
							1,
							b.data.harga,
							true
						);
					}
				},
				param	: function(){
					var params            = {};
					params['kd_unit']     = kodeunit;
					params['kd_customer'] = kdcustomeraa;
					return params;
				},
				insert	: function(o){
					return {
						kd_produk       	: o.kd_produk,
						kp_produk       	: o.kp_produk,
						kd_tarif        	: o.kd_tarif,
						kd_unit        		: o.kd_unit,
						deskripsi 			: o.deskripsi,
						harga				: o.tarifx,
						tgl_berlaku			: o.tgl_berlaku,
						jumlah				: o.jumlah,
						status_konsultasi 	: o.status_konsultasi,
						text				: '<table style="font-size: 11px;"><tr><td width="50">'+o.kp_produk+'</td><td width="160" align="left">'+o.deskripsi+'</td><td width="130">'+o.klasifikasi+'</td></tr></table>'
					}
				},
				url			: baseURL + "index.php/main/functionIGD/getProduk",
				valueField	: 'deskripsi',
				displayField: 'text'
			})
		},{
			header 		: 'Tanggal Transaksi',
			dataIndex 	: 'TGL_TRANSAKSI',
			width 		: 80,
			menuDisabled: true,
			renderer 	: function(v, params, record){
				if(record.data.TGL_TRANSAKSI == undefined || record.data.TGL_TRANSAKSI == null){
					record.data.TGL_TRANSAKSI=nowTglTransaksiIGD2;
					return record.data.TGL_TRANSAKSI;
				} else{
					if(record.data.TGL_TRANSAKSI.substring(5, 4) == '-'){
						return ShowDate(record.data.TGL_TRANSAKSI);
					}else{
						var tglb=record.data.TGL_TRANSAKSI.split("-");
					
						if(tglb[2].length == 4 && isNaN(tglb[1])){
							return record.data.TGL_TRANSAKSI;
						} else{
							return ShowDate(record.data.TGL_TRANSAKSI);
						}
					}
					
				}
			}
		},{
			id 		 	: 'colHARGAIGD',
			header 		: 'Harga Rp.',
			align 		: 'right',
			hidden 		: false,
			menuDisabled: true,
			dataIndex 	: 'HARGA',
			width 		: 80,
			renderer: function(v, params, record){
				return formatCurrency(record.data.HARGA);
			}	
		},{
			id 			: 'colProblemIGD',
			header 		: 'Qty',
			align 		: 'right',
			width 		: 50,
			menuDisabled: true,
			dataIndex 	: 'QTY',
			editor 		: new Ext.form.TextField({
				id:'fieldcolProblemIGD',
				allowBlank: true,
				enableKeyEvents : true,
				width:30,
				listeners:{
					'specialkey' : function(a, b){
						if (b.getKey() === b.ENTER || b.getKey() === b.TAB) {
							var lineIGD = gridDTLTRIGD.getSelectionModel().selection.cell[0];
							var igdUrut=toInteger(dsTRDetailKasirIGDList.data.items[lineIGD].data.URUT)-1;
							senderProdukIGD(
								notransaksi, 
								dsTRDetailKasirIGDList.data.items[lineIGD].data.KD_PRODUK, 
								dsTRDetailKasirIGDList.data.items[lineIGD].data.KD_TARIF, 
								CurrentKdKasir_KasirIGD, 
								igdUrut, 
								dsTRDetailKasirIGDList.data.items[lineIGD].data.KD_UNIT, 
								nowTglTransaksiIGD2,
								dsTRDetailKasirIGDList.data.items[lineIGD].data.TGL_BERLAKU,
								Ext.get('fieldcolProblemIGD').dom.value,
								dsTRDetailKasirIGDList.data.items[lineIGD].data.HARGA,
								false
							);
							TambahBarisIGD();
							
							gridDTLTRIGD.startEditing(lineIGD+1, 4);
						}
					},
				}
			}),
		},{
			id 			: 'colDokterPenindak',
			header 		: 'Dokter',
			align 		: 'center',
			width 		: 50,
			menuDisabled: true,
			dataIndex 	: 'JUMLAH_DOKTER',
			renderer: function(value, metaData, record, rowIndex, colIndex, store){
				 var jumlahDokter;
				 if (value > 0) {
					jumlahDokter = value;
				 }else{
					jumlahDokter = 0;
				 }
				 return jumlahDokter;
			}
		},{
			id 			: 'colImpactIGD',
			header 		: 'CR',
			width 		: 80,
			dataIndex 	: 'IMPACT',
			hidden 		: true,
			editor: new Ext.form.TextField({
				id 				:'fieldcolImpactIGD',
				allowBlank 		: true,
				enableKeyEvents : true,
				width 			: 30
			})
		},{
			id			: 'colkdunittindakanigd',
			header		: 'kd_unit',
			dataIndex	: 'KD_UNIT',
			menuDisabled: true,
			hidden 		: true

		},{
			id 		 	: 'colTotalIGD',
			header 		: 'Total Rp.',
			align 		: 'right',
			hidden 		: false,
			menuDisabled: true,
			dataIndex 	: 'TOTAL',
			width 		: 80,
			renderer: function(v, params, record){
				return formatCurrency(record.data.HARGA*record.data.QTY);
			}	
		}
	]);
}

function getItemTestIGD(){
	var radComboboxIGD = new Ext.form.ComboBox({
	   	id: 'cboTindakanRequestIGD',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		hideTrigger:true,
		forceSelection: true,
		selectOnFocus:true,
		store: ProdukDataIGD,
		valueField: 'DESKRIPSI',
		displayField: 'DESKRIPSI',
		anchor: '95%',
		listeners:{
		
			'select': function(a, b, c){
				dsTRDetailKasirIGDList.data.items[CurrentRowPemeriksaanIGD].data.KD_PRODUK = b.data.KD_PRODUK;
				dsTRDetailKasirIGDList.data.items[CurrentRowPemeriksaanIGD].data.KD_KLAS = b.data.KD_KLAS;
				dsTRDetailKasirIGDList.data.items[CurrentRowPemeriksaanIGD].data.HARGA = b.data.TARIF;
				dsTRDetailKasirIGDList.data.items[CurrentRowPemeriksaanIGD].data.TGL_BERLAKU = b.data.TGL_BERLAKU;
				dsTRDetailKasirIGDList.data.items[CurrentRowPemeriksaanIGD].data.KD_TARIF = b.data.KD_TARIF;
				dsTRDetailKasirIGDList.data.items[CurrentRowPemeriksaanIGD].data.TGL_TRANSAKSI = nowTglTransaksiIGD2;
				dsTRDetailKasirIGDList.data.items[CurrentRowPemeriksaanIGD].data.QTY = 1;
				
				var currenturut= CurrentRowPemeriksaanIGD + 1;
				cekKomponen(b.data.KD_PRODUK,b.data.KD_TARIF,kodeunit,currenturut,b.data.TARIF);
		 	}
		}
	});
	return radComboboxIGD;
}

function getProdukIGD(kd_unit){

	var str='LOWER(tarif.kd_tarif)=LOWER(~'+varkd_tarif_igd+'~) and tarif.kd_unit= ~'+kd_unit+'~ ';
	ProdukDataIGD.load({
		params: {
			Skip: 0,
			Take: 50,
			target:'LookupProduk',
			param: str
		}
	});


	return ProdukDataIGD;
}

function GetLookupAssetCMRIGD(str)
{

		var p = new mRecordIGD
		(
			{
				'DESKRIPSI2':'',
				'KD_PRODUK':'',
				'DESKRIPSI':'', 
				'KD_TARIF':'', 
				'HARGA':'',
				'QTY':dsTRDetailKasirIGDList.data.items[CurrentKasirIGD.row].data.QTY,
				'TGL_TRANSAKSI':dsTRDetailKasirIGDList.data.items[CurrentKasirIGD.row].data.TGL_TRANSAKSI,
				'DESC_REQ':dsTRDetailKasirIGDList.data.items[CurrentKasirIGD.row].data.DESC_REQ,
				'KD_TARIF':dsTRDetailKasirIGDList.data.items[CurrentKasirIGD.row].data.KD_TARIF,
				'URUT':dsTRDetailKasirIGDList.data.items[CurrentKasirIGD.row].data.URUT
			}
		);
	
	FormLookupKasir(str,strb,nilai_kd_tarif,dsTRDetailKasirIGDList,p,false,CurrentKasirIGD.row,false,syssetting);
	
};


function DataDeleteKasirIGDDetail()
{
    Ext.Ajax.request
    (
        {
            url: baseURL + "index.php/main/DeleteDataObj",
            params:  getParamDataDeleteKasirIGDDetail(),
			failure: function(o)
			{
				ShowPesanWarningKasirigd("Error, hapus tindakan. Hubungi Admin!",'WARNING');
				loadMask.hide();
			},
            success: function(o)
            {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true)
                {
                    ShowPesanInfoKasirigd('Hapus baris tindakan berhasil','Information');
					//DataDeleteKasirIGDDetail_SQL();
                    dsTRDetailKasirIGDList.removeAt(CurrentKasirIGD.row);
                    cellSelecteddeskripsi=undefined;
                    RefreshDataKasirIGDDetail(notransaksi);
                    AddNewKasirIGD = false;
                }
                else if (cst.success === false && cst.pesan === 0 )
                {
                    ShowPesanWarningKasirigd('Gagal menghapus baris tindakan', 'WARNING');
                }
                else
                {
                    ShowPesanWarningKasirigd('Error hapus tindakan!','WARNING');
                };
            }
        }
    )
};

function HapusBarisIGD()
{
    // if (cellSelecteddeskripsi != undefined)
    // {
        // if (cellSelecteddeskripsi.data.DESKRIPSI2 != '' && cellSelecteddeskripsi.data.KD_PRODUK != '')
        // {
			var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan penghapusan:', function(btn, combo){
				if (btn == 'ok')
				{
					variablehistori=combo;
					if(variablehistori != ''){
						senderProdukDeleteIGD(CurrentKdKasir_KasirIGD, CurrentKasirIGD.data.data.KD_PRODUK, notransaksi, CurrentKasirIGD.data.data.URUT, CurrentKasirIGD.data.data.TGL_TRANSAKSI,CurrentKasirIGD.data.data.DESKRIPSI,variablehistori); 
						dsTRDetailKasirIGDList.removeAt(CurrentKasirIGD.row);
					} else{
						ShowPesanWarningKasirigd('Alasan penghapusan harus di isi!','WARNING');
					}
					//DataDeleteKasirIGDDetail();
				}
			}); 
        // }
        // else
        // {
            // dsTRDetailKasirIGDList.removeAt(CurrentKasirIGD.row);
        // };
    // }
};
function senderProdukDeleteIGD(kd_kasir, kd_produk, no_transaksi, urut, tgl_transaksi, deskripsi, alasan){
    Ext.Ajax.request({
        url     : baseURL + "index.php/main/functionIGD/hapusDataProduk",
        params  :  {
            kd_produk       : kd_produk,
            no_transaksi    : no_transaksi,
            kd_kasir        : kd_kasir,
            urut            : urut,
            tgl_transaksi   : tgl_transaksi,
			alasan			: alasan,
			kd_pasien		: kodepasien,
			nama			: namapasien,
			kd_unit			: kodeunit,
			nama_unit		: namaunit,
			deskripsi		: deskripsi,
			total			: currentTOTAL_KasirIGD,
		
			
        },
        failure : function(o)
        {
            console.log(o);
        },
        success : function(o)
        {
            var cst = Ext.decode(o.responseText);
            //console.log(cst);
        }
    });
};
function getItemPaneltgl_filter() 
{
  var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .50,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'datefield',
					    fieldLabel: 'Tanggal ',
					    id: 'dtpTglAwalFilterKasirigd',
					    name: 'dtpTglAwalFilterKasirigd',
					    format: 'd/M/Y',
						//readOnly : true,
					    value: now,
					    anchor: '97.3%',
					    listeners:
						{
						
						 'specialkey' : function()
                            {
                                var tmpNoMedrec = Ext.get('txtFilterNomedrec').getValue()
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10 )
                                {
								RefreshDataFilterKasirIgdKasir();
								}
						}
						}
					}
				]
			},
			// {xtype: 'tbtext', text: ' s/d', cls: 'left-label', width: 20},
			{
			    columnWidth: .50,
			    layout: 'form',
			    border: false,
				labelWidth:60,
			    items:
				[ 
		
					{
					    xtype: 'datefield',
					    fieldLabel: 's/d ',
					    id: 'dtpTglAkhirFilterKasirigd',
					    name: 'dtpTglAkhirFilterKasirigd',
					    format: 'd/M/Y',
						//readOnly : true,
					    value: now,
					    anchor: '100%',
						   listeners:
						{
						
						 'specialkey' : function()
                            {
                                var tmpNoMedrec = Ext.get('txtFilterNomedrec').getValue()
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10 )
                                {
								RefreshDataFilterKasirIgdKasir();
								}
						}
						}
					}
				]
			}
		]
	}
    return items;
};
function getItemPanelcombofilter() 
{
  var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .50,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					mComboStatusBayar_viKasirigdKasir()
				]
			},
			 
			{
			    columnWidth: .50,
			    layout: 'form',
			    border: false,
				labelWidth:60,
			    items:
				[ 
		
				mComboUnit_viKasirigdKasir()
				]
			}
		]
	}
    return items;
};



function mComboStatusBayar_viKasirigdKasir()
{
  var cboStatus_viKasirigdKasir = new Ext.form.ComboBox
	(
		{
			id:'cboStatus_viKasirigdKasir',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
		    anchor:'97.4%',
			emptyText:'',
			fieldLabel: 'Status Lunas ',
			//width:60,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Semua'],[2, 'Lunas'], [3, 'Belum Lunas']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCountStatusByr_viKasirigdKasir,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectCountStatusByr_viKasirigdKasir=b.data.displayText ;
					//RefreshDataSetDokter();
					RefreshDataFilterKasirIgdKasir();

				}
			}
		}
	);
	return cboStatus_viKasirigdKasir;
};


function mComboUnit_viKasirigdKasir() 
{
	var Field = ['KD_UNIT','NAMA_UNIT'];

    dsunit_viKasirigdKasir = new WebApp.DataStore({ fields: Field });
    dsunit_viKasirigdKasir.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			  
                            Sort: 'kd_unit',
			    Sortdir: 'ASC',
			    target: 'ComboUnit',
                param: "" //+"~ )"
			}
		}
	);
	
    var cboUNIT_viKasirigdKasir = new Ext.form.ComboBox
	(
		{
		    id: 'cboUNIT_viKasirigdKasir',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel:  ' Poli ',
		    align: 'Right',
			  width: 100,
		    anchor:'100%',
		    store: dsunit_viKasirigdKasir,
		    valueField: 'NAMA_UNIT',
		    displayField: 'NAMA_UNIT',
			value:'All',
		    listeners:
			{
			
			    'select': function(a, b, c) 
				{					       
						   RefreshDataFilterKasirIgdKasir();

			        //selectStatusCMKasirigdView = b.data.DEPT_ID;
					//
			    }

			}
		}
	);
	cboUNIT_viKasirigdKasir.hide();
    return cboUNIT_viKasirigdKasir;
};


function mComboalasan_transfer() 
{
	var Field = ['KD_ALASAN','ALASAN'];

    var dsalasan_transfer = new WebApp.DataStore({ fields: Field });
    dsalasan_transfer.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			  
                            Sort: 'kd_alasan',
			    Sortdir: 'ASC',
			    target: 'ComboAlasanTransfer',
                param: "" //+"~ )"
			}
		}
	);
	
    var cboalasan_transfer = new Ext.form.ComboBox
	(
		{
		    id: 'cboalasan_transfer',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel:  ' Alasan Transfer ',
		    align: 'Right',
			  width: 100,
		    anchor:'100%',
		    store: dsalasan_transfer,
		    valueField: 'ALASAN',
		    displayField: 'ALASAN',
		    listeners:
			{
			
			    'select': function(a, b, c) 
				{			
					//
			    }

			}
		}
	);
	
    return cboalasan_transfer;
};



function KasirLookUpigd(rowdata) 
{
	/*
		PERBARUAN SIZE PANEL
		OLEH 	: HADAD AL GOJALI 
		TANGGAL : 2016 - 01- 17
	 */
    var lebar = 680;
    FormLookUpsdetailTRKasirigd = new Ext.Window
    (
        {
            id: 'gridKasirigd',
            title: 'Kasir Gawat Darurat',
            closeAction: 'destroy',
            width: lebar,
            height: 500,
            border: false,
            resizable: false,
            plain: true,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryKasirigd(lebar),
            listeners:
            {
                
            }
        }
    );

    FormLookUpsdetailTRKasirigd.show();
    getIdGetDataSettingKasirIGD();
    if (rowdata == undefined) 
	{
        KasirigdAddNew();
    }
    else 
	{
        TRKasirigdInit(rowdata)
    }

};
function load_data_printer_kasirigd(param)
{

	Ext.Ajax.request(
	{
		url: baseURL + "index.php/apotek/functionAPOTEK/printer",
		params:{
			command: param
		} ,
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
		success: function(o) {
			//cbopasienorder_mng_apotek.store.removeAll();
				var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsprinter_kasirigd.recordType;
				var o=cst['listData'][i];
				
				recs.push(new recType(o));
				dsprinter_kasirigd.add(recs);
				console.log(o);
			}
		}
	});
}

function mCombo_printer_kasirigd()
{ 
 
	var Field = ['kd_pasien','nama','kd_dokter','kd_unit','kd_customer','no_transaksi','kd_kasir','id_mrresep','urut_masuk','tgl_masuk','tgl_transaksi'];

    dsprinter_kasirigd = new WebApp.DataStore({ fields: Field });
	
	load_data_printer_kasirigd();
	var cbo_printer_kasirigd= new Ext.form.ComboBox
	(
		{
			id: 'cbopasienorder_printer_kasirigd',
			typeAhead		: true,
			triggerAction	: 'all',
			lazyRender		: true,
			mode			: 'local',
			emptyText: 'Pilih Printer',
			fieldLabel:  '',
			align: 'Right',
			hidden:true,
			width: 200,
			store: dsprinter_kasirigd,
			valueField: 'name',
			displayField: 'name',
			//hideTrigger		: true,
			listeners:
			{
								
			}
		}
	);return cbo_printer_kasirigd;
};
function getFormEntryKasirigd(lebar) 
{
    var pnlTRKasirigd = new Ext.FormPanel
    (
        {
            id: 'PanelTRKasirigd',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:130,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [getItemPanelInputKasir(lebar)],
           tbar:
            [
             
            ]
        }
    );
 var x;
 var paneltotal = new Ext.Panel
	(
            {
            id: 'paneltotal',
            region: 'center',
            border:false,
            bodyStyle: 'padding:0px 7px 0px 7px',
            layout: 'column',
            frame:true,
            height:67,
            anchor: '100% 8.0000%',
            autoScroll:false,
            items:
            [
                
				
				{
					xtype: 'compositefield',
					anchor: '100%',
					labelSeparator: '',
					border:true,
					style:{'margin-top':'5px'},
					items: 
					[
						{
                   
							layout: 'form',
							style:{'text-align':'right','margin-left':'10px'},
							border: false,
							html: " *) Tag yang tidak di ceklis tidak bisa di cetak."
						},
		                {
                   
							layout: 'form',
							style:{'text-align':'right','margin-left':'220px'},
							border: false,
							html: " Total :"
						},
						/*{
							xtype: 'textfield',
							id: 'txtJumlahHargaEditData_viKasirigd',
							name: 'txtJumlahHargaEditData_viKasirigd',
							style:{'text-align':'right','margin-left':'390px'},
							width: 82,
							readOnly: true,
                        },*/
						{
							xtype: 'textfield',
							id: 'txtJumlah2EditData_viKasirigd',
							name: 'txtJumlah2EditData_viKasirigd',
							style:{'text-align':'right','margin-left':'240px'},
							width: 82,
							readOnly: true,
                        },
						
		                //-------------- ## --------------
					]
				},
				 {
					xtype: 'compositefield',
					anchor: '100%',
					labelSeparator: '',
					border:true,
					style:{'margin-top':'5px'},
					items: 
					[
						{
                   
                    layout: 'form',
						style:{'text-align':'right','margin-left':'370px'},
						border: false,
						hidden:true,
						html: " Bayar :"
						},
						
		                {
                            xtype: 'numberfield',
                            id: 'txtJumlahEditData_viKasirigd',
                            name: 'txtJumlahEditData_viKasirigd',
							style:{'text-align':'right','margin-left':'390px'},
							hidden:true,
                            width: 82,
							
                            //readOnly: true,
                        }
		                //-------------- ## --------------
					]
				}
            ]

        }
    )
 var GDtabDetailKasirigd = new Ext.Panel   
    (
        {
        id:'GDtabDetailKasirigd',
        region: 'center',
        activeTab: 0,
		height:500,
        anchor: '100% 100%',
        border:true,
        plain: true,
        defaults:
                {
                autoScroll: true
		},
                items: [GetDTLTRKasirigdGrid(),paneltotal],
                tbar:
                [
                    {
                        text: 'Bayar',
                        id: 'btnSimpanKasirigd',
                        tooltip: nmSimpan,
                        iconCls: 'save',
                        handler: function()
                        {
                            Datasave_KasirigdKasir(false);
                        }
                    },{
                        id: 'btnTransferKasirigd',
                        text: 'Transfer',
                        tooltip: nmEditData,
                        iconCls: 'Edit_Tr',
                        handler: function(sm, row, rec)
                        {
							if (Ext.getCmp('txtJumlah2EditData_viKasirigd').getValue()=='0' || Ext.getCmp('txtJumlah2EditData_viKasirigd').getValue()==0){
								ShowPesanWarningKasirigd('Pasien telah melunasi pembayaran','WARNING');
								x = 0;
							}
							else
							{
								TransferLookUp();
								Ext.Ajax.request ({
									url: baseURL + "index.php/main/GetPasienTranfer",
									params: {
										notr: Ext.get(txtNoMedrecDetransaksi).dom.value,
									},
									success: function(o) {	
										var tampungmedrec="";
										o.responseText
										if(o.responseText==""){
											ShowPesanWarningKasirigd('Pasien Belum terdaftar dirawat Inap ','Transfer');
											FormLookUpsdetailTRTransfer_IGD.close();
										} else{ 
											var tmphasil = o.responseText;
											var tmp = tmphasil.split("<>");
											Ext.get(txtTranfernoTransaksi).dom.value=tmp[3];
											Ext.get(dtpTanggalTransfertransaksi).dom.value=ShowDate(tmp[4]);
											Ext.get(txtTranfernomedrec).dom.value=tmp[5];
											tampungmedrec=tmp[5];
											Ext.get(txtTranfernamapasien).dom.value=tmp[2];
											Ext.get(txtTranferunit).dom.value=tmp[1];
											Trkdunit2=tmp[6];
											var kasir=tmp[7];
											Ext.get(txtTranferkelaskamarigd).dom.value=tmp[9];
											tmp_kodeunitkamarigd=tmp[8];
											tmp_kdspesialigd=tmp[10];
											tmp_nokamarigd=tmp[11];
											Trkdunit2=tmp[6];
											
											Ext.Ajax.request ({
												url: baseURL + "index.php/main/functionIGD/getDetailTransfer",
												params: {
													notr: notransaksi,
													kd_kasir:'igd'
												},
												success: function(o)
												{ 
													var cst = Ext.decode(o.responseText);
													Ext.get(txtjumlahbiayasal_KasirIGD).dom.value=formatCurrency(cst.totalpembayaran);
													Ext.get(txtpaid_KasirIGD).dom.value=formatCurrency(cst.totalsudahbayar);
													Ext.get(txtjumlahtranfer_KasirIGD).dom.value=formatCurrency(cst.sisa);
													Ext.get(txtsaldotagihan_KasirIGD).dom.value=formatCurrency(cst.sisa);
													SisaPembayaranKasirIGD=cst.sisa;
													
													if( tampungmedrec===undefined)
													{
														ShowPesanWarningKasirigd('Data Tidak dapat di transfer, hubungi Admin', 'Gagal');
														FormLookUpsdetailTRTransfer_IGD.close();

													}
													//).dom.value=formatCurrency(o.responseText);
												},
												failure: function(o) {
													ShowPesanWarningKasirigd('Data Tidak dapat di transfer,hubungi Admin', 'Gagal');
													FormLookUpsdetailTRTransfer_IGD.close();
												}
											});  
										}
									},
									failure: function(o) {
										/* var datax=tampungmedrec.substring(0, 2); */
										ShowPesanWarningKasirigd('Data Tidak dapat di transfer,hubungi Admin', 'Gagal');
										FormLookUpsdetailTRTransfer_IGD.close();
									}

								});
							}
							
                      
                        } //disabled :true
                    },
					mCombo_printer_kasirigd(),
                    {
                        xtype:'splitbutton',
                        text:'Cetak',
                        iconCls:'print',
                        id:'btnPrint_viDaftar',
                        handler:function()
                        {		
                        },
                        menu: new Ext.menu.Menu({
                        items: [
                            {
                                xtype: 'button',
                                text: 'Print Bill',
                                id: 'btnPrintBillIGD',
                                handler: function()
                                {
									if (Ext.getCmp('cbopasienorder_printer_kasirigd').getValue()==='' || Ext.getCmp('cbopasienorder_printer_kasirigd').getValue()===undefined)
									{
										ShowPesanWarningKasirigd('Pilih printer terlebih dahulu !', 'Cetak Data');
									}else
									{
										printbillIGD();
									}
                                    
                                }
                            },
                            {
                                xtype: 'button',
                                text: 'Print Kwitansi',
                                id: 'btnPrintKwitansiIGD',
                                handler: function(){
									if(CurrentKdCustomer_KasirIGD == "0000000001"){
										if (Ext.getCmp('cbopasienorder_printer_kasirigd').getValue()==='' || Ext.getCmp('cbopasienorder_printer_kasirigd').getValue()===undefined){
											ShowPesanWarningKasirigd('Pilih printer terlebih dahulu !', 'Cetak Data');
										}else{
											printkwitansiIGD();
										} 
									} else{
										ShowPesanWarningKasirigd('Cetak kwitansi hanya untuk pasien UMUM!', 'Cetak Data');
									}
                                }
                            }
                        ]
                        })
                    }
		]
	}
    );
	
   
   
   var pnlTRKasirigd2 = new Ext.FormPanel
    (
        {
            id: 'PanelTRKasirigd2',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:500,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [	GDtabDetailKasirigd,
			
			
			]
        }
    );

	
   
   
    var FormDepanKasirigd = new Ext.Panel
	(
		{
		    id: 'FormDepanKasirigd',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
			height:500,
		    shadhow: true,
		    items: [pnlTRKasirigd,pnlTRKasirigd2,
			
				
			]
		}			
				

			

		
	);

    return FormDepanKasirigd
};






function TambahBarisKasirigd()
{
    var x=true;

    if (x === true)
    {
        var p = RecordBaruKasirrwj();
        dsTRDetailKasirigdKasirList.insert(dsTRDetailKasirigdKasirList.getCount(), p);
    };
};





function GetDTLTRHistoryGridIGD() 
{

    var fldDetail = ['NO_TRANSAKSI','TGL_BAYAR','DESKRIPSI','URUT','BAYAR','USERNAME','SHIFT','KD_USER','URAIAN'];
	
    dsTRDetailHistoryListIGD = new WebApp.DataStore({ fields: fldDetail })
		 
    var gridDTLTRHistoryIGD = new Ext.grid.EditorGridPanel
    (
        {
            title: 'History Bayar',
            stripeRows: true,
            store: dsTRDetailHistoryListIGD,
            border: false,
            columnLines: true,
            frame: false,
			anchor: '100% 25%',
			autoScroll:true,
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                            cellSelecteddeskripsi = dsTRDetailHistoryListIGD.getAt(row);
                            CurrentHistory.row = row;
                            CurrentHistory.data = cellSelecteddeskripsi;
                        }
                    }
                }
            ),
            cm: TRHistoryColumModel(),
			viewConfig:{forceFit: true},
			tbar:
			[                    
				{
					id:'btnHpsBrsKasirigd',
					text: 'Hapus Pembayaran',
					tooltip: 'Hapus Baris',
					iconCls: 'RemoveRow',
					//hidden :true,
					handler: function()
					{
							if (dsTRDetailHistoryListIGD.getCount() > 0 )
							{
								if (cellSelecteddeskripsi != undefined)
								{
										if(CurrentHistory != undefined)
										{
											HapusBarisDetailbayar();
										}
								}
								else
								{
										ShowPesanWarningKasirigd('Pilih record ','Hapus data');
								}
							}
							 //Ext.getCmp('btnEditKasirigd').disable();
							
					},
					disabled :true
				}	
			]
        }
		
		
    );
	
	

    return gridDTLTRHistoryIGD;
};

function TRHistoryColumModel() 
{
    return new Ext.grid.ColumnModel
    (//'','','','','',''
        [
           new Ext.grid.RowNumberer(),
            {
                id: 'colKdTransaksi',
                header: 'No. Transaksi',
                dataIndex: 'NO_TRANSAKSI',
                width:100,
					menuDisabled:true,
                hidden:false
            },
			{
                id: 'colTGlbayar',
                header: 'Tgl Bayar',
                dataIndex: 'TGL_BAYAR',
					menuDisabled:true,
				width:100,
				renderer: function(v, params, record)
                {
                   return ShowDate(record.data.TGL_BAYAR);

                }
                
            },
			{
                id: 'coleurutmasuk',
                header: 'urut Bayar',
                dataIndex: 'URUT',
				//hidden:true
                
            }
            
            ,
			{
                id: 'colePembayaran',
                header: 'Pembayaran',
                dataIndex: 'URAIAN',
				width:150,
				hidden:false
                
            }
			,
			{
                id: 'colJumlah',
                header: 'Jumlah',
				width:150,
				align :'right',
                dataIndex: 'BAYAR',
				hidden:false,
				renderer: function(v, params, record)
                {
                   return formatCurrency(record.data.BAYAR);

                }
                
            }
			,
			
			{
                id: 'coletglmasuk',
                header: 'tgl masuk',
                dataIndex: '',
				hidden:true
                
            }
            ,
            {
                id: 'colStatHistory',
                header: 'History',
                width:130,
				menuDisabled:true,
                dataIndex: '',
				hidden:true
              
				
				
              
            },
            {
                id: 'colPetugasHistory',
                header: 'Petugas',
                width:130,
				menuDisabled:true,
                dataIndex: 'USERNAME',	
				//hidden:true
                
				
				
              
            }
			

        ]
    )
};
function HapusBarisDetailbayar()
{
    if ( cellSelecteddeskripsi != undefined )
    {
        if (cellSelecteddeskripsi.data.NO_TRANSAKSI != '' && cellSelecteddeskripsi.data.URUT != '')
        {
			//'NO_TRANSAKSI','TGL_BAYAR','DESKRIPSI','URUT','BAYAR','USERNAME'
			var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan penghapusan:', function(btn, combo){
				if (btn == 'ok')
				{
					variablehistori=combo;
					if (variablehistori!='')
					{
						DataDeleteKasirigdKasirDetail();
						Ext.getCmp('btnTutupTransaksiKasirigd').disable();
						Ext.getCmp('btnHpsBrsKasirigd').disable();
					}
					else
					{
						ShowPesanWarningKasirigd('Silahkan isi alasan terlebih dahulu','Keterangan');
					}
				}
						
			});
											

												
                                         
                                
               
        }
        else
        {
            dsTRDetailHistoryListIGD.removeAt(CurrentHistory.row);
        };
    }
};

function DataDeleteKasirigdKasirDetail()
{
    Ext.Ajax.request
    (
        {
            url: baseURL + "index.php/main/functionIGD/deletedetail_bayar",
            params:  getParamDataDeleteKasirigdKasirDetail(),
			failure: function(o)
			{
				ShowPesanWarningKasirigd("Error. hapus baris pembayaran, Hubungi Admin!",'WARNING');
				loadMask.hide();
			},
            success: function(o)
            {
			//	RefreshDatahistoribayar(Kdtransaksi);
				RefreshDataFilterKasirIgdKasir();
				 RefreshDatahistoribayar('0');
                var cst = Ext.decode(o.responseText);
                if (cst.success === true)
                {
                    ShowPesanInfoKasirigd('Berhasil menghapus baris pembayaran','Information');
					DataDeleteKasirigdKasirDetail_SQL()
					RefreshDataFilterKasirIgdKasir();
                }
                else if (cst.success === false && cst.pesan === 'LUNAS' )
                {
                    ShowPesanWarningKasirigd('Pembayaran tidak dapat dihapus karena sudah erdapat pembayaran pada transaksi ini', 'WARNING');
                }
                else
                {
                    ShowPesanWarningKasirigd('Gagal menghapus baris pembayaran!','WARNING');
                };
            }
        }
    )
};

function getParamDataDeleteKasirigdKasirDetail()
{
    var params =
    {

        TrKodeTranskasi: CurrentHistory.data.data.NO_TRANSAKSI,
		TrTglbayar:  CurrentHistory.data.data.TGL_BAYAR,
		Urut:  CurrentHistory.data.data.URUT,
		Jumlah:  CurrentHistory.data.data.BAYAR,
		Username :CurrentHistory.data.data.USERNAME,
		Shift: 1,//shift na dipatok hela ke ganti di phpna
		Shift_hapus: 1,//shift na dipatok hela ke ganti di phpna
		Kd_user: CurrentHistory.data.data.KD_USER,
		Tgltransaksi :tgltrans, 
        Kodepasein:kodepasien,
		NamaPasien:namapasien,
        KodeUnit: kodeunit,
        Namaunit:namaunit,
        Kodepay:kodepay,
		Kdcustomer:kdcustomeraa,
        Uraian:CurrentHistory.data.data.DESKRIPSI,
		KDkasir: '06',
		alasan:variablehistori
		
    };
	Kdtransaksi=CurrentHistory.data.data.NO_TRANSAKSI;
    return params
};



function GetDTLTRKasirigdGrid() 
{
    var fldDetail = ['KD_PRODUK','DESKRIPSI','DESKRIPSI2','KD_TARIF','HARGA','QTY','DESC_REQ','TGL_BERLAKU','NO_TRANSAKSI','URUT','DESC_STATUS','TGL_TRANSAKSI','BAYARTR','DISCOUNT','PIUTANG','TAG'];
	
dsTRDetailKasirigdKasirList = new WebApp.DataStore({ fields: fldDetail })
//RefreshDataKasirigdKasirDetail() ;
gridDTLTRKasirigd = new Ext.grid.EditorGridPanel
    (
        {
            title: 'Detail Pembayaran',
            stripeRows: true,
			id: 'gridDTLTRKasirigd',
            store: dsTRDetailKasirigdKasirList,
            border: false,
            columnLines: true,
            frame: false,
            anchor: '100%',
			height:200,
            autoScroll:true,
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        
                    }
                }
            ),
            cm: TRKasirRawatJalanColumModel()
        }
		
		
    );
	
	

    return gridDTLTRKasirigd;
};

function TRKasirRawatJalanColumModel() 
{
    return new Ext.grid.ColumnModel
    (
        [
            new Ext.grid.RowNumberer(),
            {
                id: 'coleskripsiKasirigd',
                header: 'Uraian',
                dataIndex: 'DESKRIPSI2',
                width:250,
				menuDisabled:true,
				hidden :true
                
            },{
                id: 'coltagKasirigd',
                header:'Tag',
                dataIndex: 'TAG',
                width:30,
				xtype:'checkcolumn',
				// listeners:{
					// 'checkchange':function(a,b){
						// console.log(a);
						// alert();
					// },
					// 'change':function(a,b){
						// console.log(a);
						// alert();
					// },'click':function(a,b){
						// console.log(a);
						// alert();
					// }
				// },
				// 'checkchange':function(a,b){
						// console.log(a);
						// alert();
					// },
				// handler:function(){
					// alert();
				// }
				// editor: new Ext.form.Checkbox({
					// listeners:{
						// change:function(a,b){
							// console.log(a);
							// alert();
						// }
					// },
				// })
			},
            {
                id: 'colKdProduk',
                header: 'Kode Produk',
                dataIndex: 'KD_PRODUK',
                width:100,
				menuDisabled:true,
                hidden:true
            },
            {
                id: 'colDeskripsiKasirigd',
                header:'Nama Produk',
                dataIndex: 'DESKRIPSI',
                sortable: false,
                hidden:false,
				menuDisabled:true,
                width:250
                
            },
			   {
                id: 'colURUTKasirigd',
                header:'Urut',
                dataIndex: 'URUT',
                sortable: false,
                hidden:true,
				menuDisabled:true,
                width:250
                
            }
			,
            {
               header: 'Tanggal Transaksi',
               dataIndex: 'TGL_TRANSAKSI',
               width: 130,
			    hidden:true,
			   menuDisabled:true,
               renderer: function(v, params, record)
                    {
                        
                        return ShowDate(record.data.TGL_TRANSAKSI);
                    }
            },
            {
                id: 'colQtyKasirigd',
                header: 'Qty',
                width:91,
				align: 'right',
				menuDisabled:true,
                dataIndex: 'QTY',
             
				
				
              
            },
            {
                id: 'colHARGAKasirigd',
                header: 'Harga',
				align: 'right',
				hidden: false,
				menuDisabled:true,
                dataIndex: 'HARGA',
                width:100,
				
				renderer: function(v, params, record) 
							{
							return formatCurrency(record.data.HARGA);
							
							}	
            },

            {
                id: 'colPiutangKasirigd',
                header: 'Puitang',
                width:80,
                dataIndex: 'PIUTANG',
				align: 'right',
				//hidden: false,
			
				 editor: new Ext.form.TextField
                (
                    {
                        id:'fieldcolPuitangigd',
                        allowBlank: true,
                        enableKeyEvents : true,
                        width:30,
						
                    }
                )
             ,
				renderer: function(v, params, record) 
							{
							
							//getTotalDetailProduk();
						
								return formatCurrency(record.data.PIUTANG);
							
						
							
							
							}
            },
			{
                id: 'colTunaiKasirigd',
                header: 'Tunai',
                width:80,
                dataIndex: 'BAYARTR',
				align: 'right',
				hidden: false,
							 editor: new Ext.form.TextField
								(
									{
										id:'fieldcolTunaiigd',
										allowBlank: true,
										enableKeyEvents : true,
										width:30,
										
									}
								),
				renderer: function(v, params, record) 
							{
							//getTotalHargaDetailProduk();
							getTotalDetailProduk();
					
							return formatCurrency(record.data.BAYARTR);
							
							
							}
				
            },
			{
                id: 'colDiscountKasirigd',
                header: 'Discount',
                width:80,
                dataIndex: 'DISCOUNT',
				align: 'right',
				hidden: false,
				editor: new Ext.form.TextField
								(
									{
										id:'fieldcolDiscountigd',
										allowBlank: true,
										enableKeyEvents : true,
										width:30,
										
									}
								)
				,
					renderer: function(v, params, record) 
							{
							return formatCurrency(record.data.DISCOUNT);
							
							
							}
							 
				
            }

        ]
    )
};




function RecordBaruKasirrwj()
{

	var p = new mRecordKasirrwj
	(
		{
			'DESKRIPSI2':'',
			'KD_PRODUK':'',
		    'DESKRIPSI':'', 
		    'KD_TARIF':'', 
		    'HARGA':'',
		    'QTY':'',
		    'TGL_TRANSAKSI':tanggaltransaksitampung, 
		    'DESC_REQ':'',
		    'KD_TARIF':'',
		    'URUT':''
		}
	);
	
	return p;
};
function TRKasirigdInit(rowdata)
{
    AddNewKasirigdKasir = false;
	vkd_unit = rowdata.KD_UNIT;
	RefreshDataKasirigdKasirDetail(rowdata.NO_TRANSAKSI);
	Ext.get('txtNoTransaksiKasirigdKasir').dom.value = rowdata.NO_TRANSAKSI;
	tanggaltransaksitampung = rowdata.TANGGAL_TRANSAKSI;
    Ext.get('dtpTanggalDetransaksi').dom.value = ShowDate(rowdata.TANGGAL_TRANSAKSI);
	Ext.get('txtNoMedrecDetransaksi').dom.value= rowdata.KD_PASIEN;
	Ext.get('txtNamaPasienDetransaksi').dom.value = rowdata.NAMA;
	Ext.get('cboPembayaran').dom.value= rowdata.KET_PAYMENT;
	Ext.get('cboJenisByr').dom.value= rowdata.CARA_BAYAR; // take the displayField value 
	loaddatastorePembayaran(rowdata.JENIS_PAY);
	vkode_customer = rowdata.KD_CUSTOMER;
	tampungtypedata=0;
	tapungkd_pay=rowdata.KD_PAY;
	console.log(rowdata);
	console.log(rowdata.KD_PAY);
	jenispay=1;
	var vkode_customer = rowdata.LUNAS;
	showCols(Ext.getCmp('gridDTLTRKasirigd'));
	hideCols(Ext.getCmp('gridDTLTRKasirigd'));
	vflag= rowdata.FLAG;
	tgltrans=rowdata.TANGGAL_TRANSAKSI;
	kodepasien=rowdata.KD_PASIEN;
	namapasien=rowdata.NAMA;
	kodeunit=rowdata.KD_UNIT;
	namaunit=rowdata.NAMA_UNIT;
	kodepay=rowdata.KD_PAY;
	kdcustomeraa=rowdata.KD_CUSTOMER;
	notransaksi=rowdata.NO_TRANSAKSI;
	CurrentKdCustomer_KasirIGD = rowdata.KD_CUSTOMER;
	Ext.Ajax.request(
	{
	   
	    url: baseURL + "index.php/main/getcurrentshift",
		 params: {
	        
	        command: '0',
		
	    },
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
	    success: function(o) {
	
			tampungshiftsekarang=o.responseText




	    }
	
	});

	
	
};

function mEnabledKasirigdCM(mBol)
{

	 Ext.get('btnLookupKasirigd').dom.disabled=mBol;
	 Ext.get('btnHpsBrsKasirigd').dom.disabled=mBol;
};


///---------------------------------------------------------------------------------------///
function KasirigdAddNew() 
{
    AddNewKasirigdKasir = true;
	Ext.get('txtNoTransaksiKasirigdKasir').dom.value = '';
    Ext.get('dtpTanggalDetransaksi').dom.value = nowTglTransaksiIGD.format('d/M/Y');
	Ext.get('txtNoMedrecDetransaksi').dom.value='';
	Ext.get('txtNamaPasienDetransaksi').dom.value = '';
	Ext.get('cboStatus_viKasirigdKasir').dom.value= ''
	rowSelectedKasirigdKasir=undefined;
	dsTRDetailKasirigdKasirList.removeAll();
	mEnabledKasirigdCM(false);
	

};

function RefreshDataKasirigdKasirDetail(no_transaksi) 
{
    var strKriteriaKasirigd='';
   
    strKriteriaKasirigd = 'no_transaksi = ~' + no_transaksi + '~ and kd_kasir = ~06~';

    dsTRDetailKasirigdKasirList.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'tgl_transaksi',
			    //Sort: 'tgl_transaksi',
			    Sortdir: 'ASC',
			    target: 'ViewDetailbayarIGD',
			    param: strKriteriaKasirigd
			}
		}
	);
    return dsTRDetailKasirigdKasirList;
};

function RefreshDatahistoribayar(no_transaksi) 
{
    var strKriteriaKasirigd='';
   
    strKriteriaKasirigd = 'no_transaksi= ~' + no_transaksi + '~';

    dsTRDetailHistoryListIGD.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'tgl_transaksi',
			    //Sort: 'tgl_transaksi',
			    Sortdir: 'ASC',
			    target: 'ViewHistoryBayarIGD',
			    param: strKriteriaKasirigd
			}
		}
	);
    return dsTRDetailHistoryListIGD;
};


//---------------------------------------------------------------------------------------///



//---------------------------------------------------------------------------------------///
function getParamDetailTransaksiKasirigd() 
{
	if(tampungtypedata==='')
	{
		tampungtypedata=0;
	};
	var kdpay='';
	if(Ext.getCmp('cboPembayaran').getValue() == ''){
		kdpay=Ext.get('cboPembayaran').getValue();
	} else{
		kdpay=Ext.getCmp('cboPembayaran').getValue();
	}
	
	var params =
	{
		kdUnit : vkd_unit,
		TrKodeTranskasi: Ext.get('txtNoTransaksiKasirigdKasir').getValue(),
		Tgl: Ext.get('dtpTanggalDetransaksi').dom.value,
		Shift: tampungshiftsekarang,
		Flag:vflag,
		bayar: tapungkd_pay,
		Typedata: tampungtypedata,
		Totalbayar:getTotalDetailProduk(),
		List:getArrDetailTrKasirigd(),
		JmlField: mRecordKasirigd.prototype.fields.length-4,
		JmlList:GetListCountDetailTransaksi(),
		UrutMasuk:CurrentURUT_MASUKIGD,
		KdCustomer:CurrentKdCustomer_KasirIGD,
		KdPay:kdpay,
		Hapus:1,
		Ubah:0
	};
    return params
};

function paramUpdateTransaksi()
{
 var params =
	{
            TrKodeTranskasi: cellSelectedtutup_kasirigd,
            KDUnit : kdUnit,
			kd_unit : CurrentKdUnit_KasirIGD,
			urut_masuk:CurrentUrutMasuk_KasirIGD,
			tgl_transaksi:CurrentTglMasuk_KasirIGD,
			kd_pasien:CurrentKdPasien_KasirIGD,
	};
	return params;
};

function GetListCountDetailTransaksi()
{
	
	var x=0;
	for(var i = 0 ; i < dsTRDetailKasirigdKasirList.getCount();i++)
	{
		if (dsTRDetailKasirigdKasirList.data.items[i].data.KD_PRODUK != '' || dsTRDetailKasirigdKasirList.data.items[i].data.DESKRIPSI  != '')
		{
			x += 1;
		};
	}
	return x;
	
};

function getTotalDetailProduk()
{
var TotalProduk=0;
var bayar;
	var tampunggrid ;
	var x='';
	for(var i = 0 ; i < dsTRDetailKasirigdKasirList.getCount();i++)
	{		

			var recordterakhir;
		 
			//alert(TotalProduk);
		if (tampungtypedata==0)
		{
		tampunggrid = parseInt(dsTRDetailKasirigdKasirList.data.items[i].data.BAYARTR);
			
			//recordterakhir= tampunggrid
			//TotalProduk.toString().replace(/./gi, "");
			TotalProduk+= tampunggrid
		  
			recordterakhir=TotalProduk
			Ext.get('txtJumlah2EditData_viKasirigd').dom.value=formatCurrency(recordterakhir);
		}
		if(tampungtypedata==3)
		{
			tampunggrid = parseInt(dsTRDetailKasirigdKasirList.data.items[i].data.PIUTANG);
			//TotalProduk.toString().replace(/./gi, "");
			//recordterakhir=tampunggrid
			TotalProduk+=tampunggrid
		     recordterakhir=TotalProduk
			Ext.get('txtJumlah2EditData_viKasirigd').dom.value=formatCurrency(recordterakhir);
		}
		if(tampungtypedata==1)
		{
		   tampunggrid = parseInt(dsTRDetailKasirigdKasirList.data.items[i].data.DISCOUNT);
		//	TotalProduk.toString().replace(/./gi, "");
			//recordterakhir=dsTRDetailKasirigdKasirList.data.items[i].data.DISCOUNT
			TotalProduk+=tampunggrid
		    recordterakhir=TotalProduk
			Ext.get('txtJumlah2EditData_viKasirigd').dom.value=formatCurrency(recordterakhir);
		}
	
		
		
	}	
	bayar = Ext.get('txtJumlah2EditData_viKasirigd').getValue();
	return bayar;
};

function getTotalHargaDetailProduk()
{
var TotalProduk=0;
var bayar;
	var tampunggrid ;
	var x='';
	for(var i = 0 ; i < dsTRDetailKasirigdKasirList.getCount();i++)
	{		

			var recordterakhir;
		 
			//alert(TotalProduk);
		if (tampungtypedata==0)
		{
			//console.log(dsTRDetailKasirigdKasirList.data.items[i].data);
			tampunggrid = parseInt(dsTRDetailKasirigdKasirList.data.items[i].data.HARGA);
			
			//recordterakhir= tampunggrid
			//TotalProduk.toString().replace(/./gi, "");
			TotalProduk+= tampunggrid
		  
			recordterakhir=TotalProduk
			Ext.get('txtJumlahHargaEditData_viKasirigd').dom.value=formatCurrency(recordterakhir);
		}
		if(tampungtypedata==3)
		{
			tampunggrid = parseInt(dsTRDetailKasirigdKasirList.data.items[i].data.PIUTANG);
			//TotalProduk.toString().replace(/./gi, "");
			//recordterakhir=tampunggrid
			TotalProduk+=tampunggrid
		     recordterakhir=TotalProduk
			Ext.get('txtJumlahHargaEditData_viKasirigd').dom.value=formatCurrency(recordterakhir);
		}
		if(tampungtypedata==1)
		{
		   tampunggrid = parseInt(dsTRDetailKasirigdKasirList.data.items[i].data.DISCOUNT);
		//	TotalProduk.toString().replace(/./gi, "");
			//recordterakhir=dsTRDetailKasirigdKasirList.data.items[i].data.DISCOUNT
			TotalProduk+=tampunggrid
		    recordterakhir=TotalProduk
			Ext.get('txtJumlahHargaEditData_viKasirigd').dom.value=formatCurrency(recordterakhir);
		}
	
		
		
	}	
	bayar = Ext.get('txtJumlahHargaEditData_viKasirigd').getValue();
	return bayar;
};







function getArrDetailTrKasirigd()
{
	var x='';
	for(var i = 0 ; i < dsTRDetailKasirigdKasirList.getCount();i++)
	{
		if (dsTRDetailKasirigdKasirList.data.items[i].data.KD_PRODUK != '' && dsTRDetailKasirigdKasirList.data.items[i].data.DESKRIPSI != '')
		{
			var y='';
			var z='@@##$$@@';
			
			y = 'URUT=' + dsTRDetailKasirigdKasirList.data.items[i].data.URUT
			y += z + dsTRDetailKasirigdKasirList.data.items[i].data.KD_PRODUK
			y += z + dsTRDetailKasirigdKasirList.data.items[i].data.QTY
			y += z +dsTRDetailKasirigdKasirList.data.items[i].data.HARGA
			y += z +dsTRDetailKasirigdKasirList.data.items[i].data.KD_TARIF
			y += z +dsTRDetailKasirigdKasirList.data.items[i].data.URUT
			y += z +dsTRDetailKasirigdKasirList.data.items[i].data.BAYARTR
			y += z +dsTRDetailKasirigdKasirList.data.items[i].data.PIUTANG
			y += z +dsTRDetailKasirigdKasirList.data.items[i].data.DISCOUNT
			y += z +dsTRDetailKasirigdKasirList.data.items[i].data.TGL_TRANSAKSI
			
			
			if (i === (dsTRDetailKasirigdKasirList.getCount()-1))
			{
				x += y 
			}
			else
			{
				x += y + '##[[]]##'
			};
		};
	}	
	
	return x;
};


function getItemPanelInputKasir(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 0px',
		border:true,
		height:110,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
			    layout: 'form',
			    border: false,
			    items:
				[
					getItemPanelNoTransksiigdKasir(lebar),
					getItemPanelmedreckasir(lebar),
					getItemPanelUnitKasir(lebar)	
				]
			}
		]
	};
    return items;
};



function getItemPanelUnitKasir(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
				mComboJenisByrView()
				
				]
			},{
			    columnWidth: .60,
			    layout: 'form',
				labelWidth:0.9,
			    border: false,
			    items:
				[mComboPembayaran()
	
				]
			}
		]
	}
    return items;
};



function loaddatastorePembayaran(jenis_pay)
{
          dsComboBayar.load
                (
                    {
                     params:
							{
								Skip: 0,
								Take: 1000,
								Sort: 'nama',
								Sortdir: 'ASC',
								target: 'ViewComboBayar',
								param: 'jenis_pay=~'+ jenis_pay+ '~ order by uraian asc'
							}
                   }
                )
}

function mComboPembayaran()
{
    var Field = ['KD_PAY','JENIS_PAY','PAYMENT'];

    dsComboBayar = new WebApp.DataStore({fields: Field});


    var cboPembayaran = new Ext.form.ComboBox
	(
		{
		    id: 'cboPembayaran',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
            forceSelection: true,
		    //emptyText:'Pilih Pembayaran...',
		    labelWidth:80,
		    align: 'Right',
		    store: dsComboBayar,
		    valueField: 'KD_PAY',
		    displayField: 'PAYMENT',
            anchor: '100%',
			listeners:
			{
			    'select': function(a, b, c) 
				{
				
						tapungkd_pay=b.data.KD_PAY;
						//getTotalDetailProduk();
					
			   
				},
				  

			}
		}
	);

    return cboPembayaran;
};


function mComboJenisByrView() 
{
	var Field = ['JENIS_PAY','DESKRIPSI','TYPE_DATA'];

    dsJenisbyrView = new WebApp.DataStore({ fields: Field });
    dsJenisbyrView.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
		
                            Sort: 'jenis_pay',
			    Sortdir: 'ASC',
			    target: 'ComboJenis',
			  
			}
		}
	);
	
    var cboJenisByr = new Ext.form.ComboBox
	(
		{
	
		    id: 'cboJenisByr',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
			fieldLabel: 'Pembayaran      ',

		    align: 'Right',
		    anchor:'100%',
		    store: dsJenisbyrView,
		    valueField: 'JENIS_PAY',
		    displayField: 'DESKRIPSI',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
					
						 loaddatastorePembayaran(b.data.JENIS_PAY);
						 tampungtypedata=b.data.TYPE_DATA;
						 jenispay=b.data.JENIS_PAY;
						 showCols(Ext.getCmp('gridDTLTRKasirigd'));
						 hideCols(Ext.getCmp('gridDTLTRKasirigd'));
						getTotalDetailProduk();
						Ext.get('cboPembayaran').dom.value='Pilih Pembayaran...';
				
					
			   
				},
				  

			}
		}
	);
	
    return cboJenisByr;
};
    function hideCols (grid)
	{
		if (tampungtypedata==3)
		{
			grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('BAYARTR'), true);
			grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('DISCOUNT'), true);
		}
		else if(tampungtypedata==0)
		{
			grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('PIUTANG'), true);
			grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('DISCOUNT'), true);
		}
		else if(tampungtypedata==1)
		{
			grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('PIUTANG'), true);
			grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('BAYARTR'), true);
			
		}
	};
      function showCols (grid) {   
	  	if (tampungtypedata==3)
			{
			grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('PIUTANG'), false);
		
			}
			else if(tampungtypedata==0)
			{
				grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('BAYARTR'), false);
			}
			else if(tampungtypedata==1)
			{
				grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('DISCOUNT'), false);
			}
	
	};



function getItemPanelDokter(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel:'Dokter  ',
					    name: 'txtKdDokter',
					    id: 'txtKdDokter',
						//emptyText:nmNomorOtomatis,
						readOnly:true,
					    anchor: '99%'
					},{
					    xtype: 'textfield',
					    fieldLabel: 'Kelompok Pasien',
						//hideLabel:true,
						readOnly:true,
					    name: 'txtCustomer',
					    id: 'txtCustomer',
					    anchor: '99%',
						listeners: 
						{ 
							
						}
					}
				]
			},
			{
			    columnWidth: .600,
			    layout: 'form',
			    border: false,
				labelWidth:2,
			    items:
				[
					{
						xtype: 'textfield',
					   // fieldLabel:'Unit : ',
					    name: 'txtNamaDokter',
					    id: 'txtNamaDokter',
						//emptyText:nmNomorOtomatis,
						readOnly:true,
					    anchor: '100%'
					},
					{
						xtype: 'textfield',
					   // fieldLabel:'Unit : ',
					    name: 'txtKdUrutMasuk',
					    id: 'txtKdUrutMasuk',
						//emptyText:nmNomorOtomatis,
						readOnly:true,
						hidden:true,
					    anchor: '100%'
					}
				]
			}
		]
	}
    return items;
};

function getItemPanelNoTransksiigdKasir(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel:  'No. Transaksi ',
					    name: 'txtNoTransaksiKasirigdKasir',
					    id: 'txtNoTransaksiKasirigdKasir',
						emptyText:nmNomorOtomatis,
						readOnly:true,
					    anchor: '99%'
					}
				]
			},
			{
			    columnWidth: .60,
			    layout: 'form',
			    border: false,
				labelWidth:55,
			    items:
				[
					{
					    xtype: 'datefield',
					    fieldLabel: 'Tanggal ',
					    id: 'dtpTanggalDetransaksi',
					    name: 'dtpTanggalDetransaksi',
					    format: 'd/M/Y',
						readOnly : true,
					    value: now,
					    anchor: '100%'
					}
				]
			}
		]
	}
    return items;
};


function getItemPanelmedreckasir(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel:   'No. Medrec ',
					    name: 'txtNoMedrecDetransaksi',
					    id: 'txtNoMedrecDetransaksi',
						readOnly:true,
					    anchor: '99%',
					    listeners: 
						{ 
							
						}
					}
				]
			},
			{
			    columnWidth: .60,
			    layout: 'form',
			    border: false,
				labelWidth:2,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: '',
						//hideLabel:true,
						readOnly:true,
					    name: 'txtNamaPasienDetransaksi',
					    id: 'txtNamaPasienDetransaksi',
					    anchor: '100%',
						listeners: 
						{ 
							
						}
					}
				]
			}
		]
	}
    return items;
};


function RefreshDataKasirigdKasir() 
{
	loadMask.show();
    dsTRKasirigdKasirList.load
    (
        {
            params:
            {
                Skip: 0,
                Take: selectCountKasirigdKasir,
                Sort: 'tgl_transaksi',
                //Sort: 'tgl_transaksi',
                Sortdir: 'ASC',
                target:'ViewKasirIGD',
                param : ''
            },	
			callback:function(){
				loadMask.hide();
			}
        }
    );
	
    rowSelectedKasirigdKasir = undefined;
    return dsTRKasirigdKasirList;
};

function refeshKasirIgdKasir()
{
	loadMask.show();
	dsTRKasirigdKasirList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountKasirigdKasir, 
					//Sort: 'no_transaksi',
                     Sort: '',
					//Sort: 'no_transaksi',
					Sortdir: 'ASC', 
					target:'ViewKasirIGD',
					param : ''
				},	
				callback:function(){
					loadMask.hide();
				}		
			}
		);   
		return dsTRKasirigdKasirList;
}

function RefreshDataFilterKasirIgdKasir() 
{
	var KataKunci='';
	loadMask.show();
	if (Ext.get('txtFilterNomedrec').getValue() != '')
	{
		if (KataKunci == ''){
			KataKunci = ' and   LOWER(kode_pasien) like  LOWER( ~' + Ext.get('txtFilterNomedrec').getValue() + '%~)';
		}else{
			KataKunci += ' and  LOWER(kode_pasien) like  LOWER( ~' + Ext.get('txtFilterNomedrec').getValue() + '%~)';
		};
	
	};
	
	if (Ext.get('TxtFilterGridDataView_NAMA_viKasirigdKasir').getValue() != '')
	{
	if (KataKunci == '')
	{
	KataKunci = ' and   LOWER(nama) like  LOWER( ~%' + Ext.get('TxtFilterGridDataView_NAMA_viKasirigdKasir').getValue() + '%~)';
	
	}
	else
	{
	
	KataKunci += ' and  LOWER(nama) like  LOWER( ~%' + Ext.get('TxtFilterGridDataView_NAMA_viKasirigdKasir').getValue() + '%~)';
	};
	
	};
	
	
	if (Ext.get('cboUNIT_viKasirigdKasir').getValue() != '' && Ext.get('cboUNIT_viKasirigdKasir').getValue() != 'All'){
		if (KataKunci == ''){
			KataKunci = ' and  LOWER(nama_unit)like  LOWER(~%' + Ext.get('cboUNIT_viKasirigdKasir').getValue() + '%~)';
		}else{
		
			KataKunci += ' and LOWER(nama_unit) like  LOWER(~%' + Ext.get('cboUNIT_viKasirigdKasir').getValue() + '%~)';
		};
	};
	
	
	if (Ext.get('cboStatus_viKasirigdKasir').getValue() == 'Lunas'){
		if (KataKunci == ''){
			KataKunci = ' and  lunas = ~true~';
		}else{
			KataKunci += ' and lunas =  ~true~';
		};
	};
	
	
	if (Ext.get('cboStatus_viKasirigdKasir').getValue() == 'Semua'){
	
	};

	if (Ext.get('cboStatus_viKasirigdKasir').getValue() == 'Belum Lunas'){
		if (KataKunci == ''){
			KataKunci = ' and  lunas = ~false~';
		}else{
			KataKunci += ' and lunas =  ~false~';
		};
	};
	
	
	if (Ext.get('dtpTglAwalFilterKasirigd').getValue() != ''){
		if (KataKunci == ''){                      
			KataKunci = " and (tgl_transaksi between ~" + Ext.get('dtpTglAwalFilterKasirigd').getValue() + "~ and ~" + Ext.get('dtpTglAkhirFilterKasirigd').getValue() + "~)";
		}else{
			KataKunci += " and (tgl_transaksi between ~" + Ext.get('dtpTglAwalFilterKasirigd').getValue() + "~ and ~" + Ext.get('dtpTglAkhirFilterKasirigd').getValue() + "~)";
		};
	};
	
    
    if (KataKunci != undefined ||KataKunci != '' ) 
    {  
		dsTRKasirigdKasirList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountKasirigdKasir, 
					//Sort: 'no_transaksi',
					Sort: 'tgl_transaksi',
					//Sort: 'no_transaksi',
					Sortdir: 'ASC', 
					target:'ViewKasirIGD',
					param : KataKunci
				},	
				callback:function(){
					loadMask.hide();
				}		
			}
		);   
    }else{
		dsTRKasirigdKasirList.load
			(
				{ 
					params:  
					{   
						Skip: 0, 
						Take: selectCountKasirrwjKasir, 
						//Sort: 'no_transaksi',
	                                        Sort: 'tgl_transaksi',
						//Sort: 'no_transaksi',
						Sortdir: 'ASC', 
						target:'ViewKasirIGD',
						param : ''
					},	
					callback:function(){
						loadMask.hide();
					}		
				}
			);   
	};
	return dsTRKasirigdKasirList;
};



/* function Datasave_KasirigdKasir(mBol) 
{	
	if (ValidasiEntryCMKasirigd(nmHeaderSimpanData,false) == 1 )
	{
			Ext.Ajax.request
			 (
				{
					//url: "./Datapool.mvc/CreateDataObj",
					url: baseURL + "index.php/main/functionIGD/savepembayaran",
					params: getParamDetailTransaksiKasirigd(),
					failure: function(o)
					{
					ShowPesanWarningKasirigd('Data Belum Simpan segera Hubungi Admin', 'Gagal');
					 RefreshDataFilterKasirIgdKasir();
					},	
					success: function(o) 
					{
						RefreshDatahistoribayar(0);
						RefreshDataKasirigdKasirDetail(Ext.get('txtNoTransaksiKasirigdKasir').dom.value);
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							
							ShowPesanInfoKasirigd('Data berhasil disimpan', 'Information');
							//FormLookUpsdetailTRKasirigd.close();
							//RefreshDataKasirigdKasir();
							
							if(mBol === false)
							{
									 RefreshDataFilterKasirIgdKasir();
							};
						}
						else 
						{
								ShowPesanWarningKasirigd('Data Belum Simpan segera Hubungi Admin', 'Gagal');
						};
					}
				}
			)
		
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
	
}; */

function BatalTransaksi(mBol)
{

	Ext.Ajax.request
	 (
		{
			//url: "./Datapool.mvc/CreateDataObj",
			url: baseURL + "index.php/main/functionIGD/batal_transaksi",
			params: getParamDetailBatalTransaksiIGD(),
			failure: function(o)
			{
			 ShowPesanWarningKasirigd(Ext.decode(o.responseText), 'Gagal');
			// RefreshDataFilterKasirrwjKasir();
			},
			success: function(o)
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					ShowPesanInfoKasirigd('Batal transaksi berhasil','Information');
					BatalTransaksi_SQL(cst.notrans,cst.notransbaru,cst.urutmasuk,cst.urutmasukbaru);
					RefreshDataFilterKasirIgdKasir();
					if(mBol === false)
					{
					//RefreshDataFilterKasirrwjKasir();
					};
					cellSelectedtutup_kasirigd='';
				}
				else
				{
					ShowPesanWarningKasirigd(Ext.decode(o.responseText), 'Gagal');
					cellSelectedtutup_kasirigd='';
				};
			}
		}
	)
};
function getParamDetailBatalTransaksiIGD()
{
	if(tampungtypedata==='')
		{
		tampungtypedata=0;
		};
	var line = dsTRDetailHistoryListIGD.getCount();
	var totalhehe=0;
	for (var i = 0 ; i < line; i++)
	{
		totalhehe += parseInt(dsTRDetailHistoryListIGD.data.items[i].data.BAYAR);
		
	}
    var params =
	{
		kdPasien 	: kodepasien,
		noTrans 	: notransaksi,
		kdUnit 		: kodeunit,
		Jumlah		: totalhehe,
		kdDokter 	: kdokter_btl,
		tglTrans 	: tgltrans,
		kdCustomer	: kdcustomeraa,
		Keterangan	: variablebatalhistori_igd,
		
	};
    return params
};
function getParamDetailBukaTransaksiIGD()
{
	
    var params =
	{
		kdPasien 	: kodepasien,
		noTrans 	: notransaksi,
		kdUnit 		: kodeunit,
		tglTrans 	: tgltrans,
		urutMasuk 	: CurrentUrutMasuk_KasirIGD,
		Keterangan	: variablebukahistori_igd,
		
	};
    return params
};
function Datasave_KasirigdKasir(mBol) 
{	
	loadMask.show();
	if (ValidasiEntryCMKasirigd(nmHeaderSimpanData,false) == 1 )
	{
			Ext.Ajax.request
			 (
				{
					url: baseURL + "index.php/main/functionIGD/savepembayaran",
					params: getParamDetailTransaksiKasirigd(),
					failure: function(o)
					{
					ShowPesanWarningKasirigd('Data Belum Simpan segera Hubungi Admin', 'Gagal');
					 RefreshDataFilterKasirIgdKasir();
					},	
					success: function(o) 
					{
						RefreshDatahistoribayar(0);
					
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoKasirigd(nmPesanSimpanSukses,nmHeaderSimpanData);
							loadMask.hide();
							RefreshDataKasirigdKasirDetail(Ext.get('txtNoTransaksiKasirigdKasir').dom.value);
							//Datasave_KasirigdKasir_SQL();
							RefreshDataKasirigdKasir();
							/* if(mBol === false)
							{ */
									 //RefreshDataFilterKasirIgdKasir();
							//};
						}
						else 
						{
								ShowPesanWarningKasirigd('Data Belum Simpan segera Hubungi Admin', 'Gagal');
						};
					}
				}
			) 
		
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
	
};


function UpdateBukatransaksi(mBol) 
{	
	Ext.Ajax.request
	({
		//url: "./Datapool.mvc/CreateDataObj",
		url: baseURL + "index.php/main/functionIGD/bukatransaksiigd",
		params: getParamDetailBukaTransaksiIGD(),
		failure: function(o)
		{
			ShowPesanWarningKasirigd('Data gagal tersimpan segera hubungi Admin', 'Gagal');
			RefreshDataFilterKasirIgdKasir();
		},	
		success: function(o) 
		{
			//RefreshDatahistoribayar(Ext.get('cellSelectedtutup_kasirigd);
		
			var cst = Ext.decode(o.responseText);
			if (cst.success === true) 
			{
				ShowPesanInfoKasirigd('Buka transaksi berhasil','Information');
				//UpdateTutuptransaksi_SQL();
				//RefreshDataKasirigdKasir();
				
					RefreshDataFilterKasirIgdKasir();
				
				cellSelectedtutup_kasirigd='';
			}
			else 
			{
				ShowPesanWarningKasirigd('Data gagal tersimpan segera hubungi Admin', 'Gagal');
				cellSelectedtutup_kasirigd='';
			};
		}
	})
};

function UpdateTutuptransaksi(mBol) 
{	
	Ext.Ajax.request
	({
		//url: "./Datapool.mvc/CreateDataObj",
		url: baseURL + "index.php/main/functionIGD/ubah_co_status_transksi",
		params: paramUpdateTransaksi(),
		failure: function(o)
		{
			ShowPesanWarningKasirigd('Data gagal tersimpan segera hubungi Admin', 'Gagal');
			RefreshDataFilterKasirIgdKasir();
		},	
		success: function(o) 
		{
			var cst = Ext.decode(o.responseText);
			if (cst.success === true) 
			{
				ShowPesanInfoKasirigd('Tutup transaksi berhasil','Information');
				//RefreshDataKasirigdKasir();
				if(mBol === false)
				{
					RefreshDataFilterKasirIgdKasir();
				};
				cellSelectedtutup_kasirigd='';
			}
			else 
			{
				ShowPesanWarningKasirigd('Data gagal tersimpan segera hubungi Admin', 'Gagal');
				cellSelectedtutup_kasirigd='';
			};
		}
	})
};

function ValidasiEntryCMKasirigd(modul,mBolHapus)
{
	var x = 1;
	if (Ext.get('txtJumlah2EditData_viKasirigd').getValue()==='0' || Ext.get('txtJumlah2EditData_viKasirigd').getValue()===0){
			loadMask.hide();
			ShowPesanWarningKasirigd('Pasien telah melunasi pembayaran!','WARNING');
			x = 0;
		}
	//console.log(Ext.get('txtJumlah2EditData_viKasirigd').getValue());
	if((Ext.get('txtNoTransaksiKasirigdKasir').getValue() == '') 
	
	|| (Ext.get('txtNoMedrecDetransaksi').getValue() == '') 
	|| (Ext.get('txtNamaPasienDetransaksi').getValue() == '')
	|| (Ext.get('dtpTanggalDetransaksi').getValue() == '') 
	|| dsTRDetailKasirigdKasirList.getCount() === 0 
	|| (Ext.get('cboPembayaran').getValue() == '') 
	||(Ext.get('cboPembayaran').getValue() == 'Pilih Pembayaran...') )
	{
		if (Ext.get('txtNoTransaksiKasirigdKasir').getValue() == '' && mBolHapus === true) 
		{
			loadMask.hide();
			ShowPesanWarningKasirigd(('Data transaksi tidak  boleh kosong!'), 'WARNING');
			x = 0;
		}
		else if (Ext.get('cboPembayaran').getValue() == ''||Ext.get('cboPembayaran').getValue() == 'Pilih Pembayaran...') 
		{
			loadMask.hide();
			ShowPesanWarningKasirigd(('Data pembayaran tidak  boleh kosong!'), 'WARNING');
			x = 0;
		}
		else if (Ext.get('txtNoMedrecDetransaksi').getValue() == '') 
		{
			loadMask.hide();
			ShowPesanWarningKasirigd(('Data no. medrec tidak boleh kosong!'), 'WARNING');
			x = 0;
		}
		else if (Ext.get('txtNamaPasienDetransaksi').getValue() == '') 
		{
			loadMask.hide();
			ShowPesanWarningKasirigd('Nama pasien belum terisi!', 'WARNING');
			x = 0;
		}
		else if (Ext.get('dtpTanggalDetransaksi').getValue() == '') 
		{
			loadMask.hide();
			ShowPesanWarningKasirigd(('Data tanggal kunjungan tidak boleh kosong!'), 'WARNING');
			x = 0;
		}
		//cboPembayaran
	
		else if (dsTRDetailKasirigdKasirList.getCount() === 0) 
		{
			loadMask.hide();
			ShowPesanWarningKasirigd('Data dalam tabel kosong!','WARNING');
			x = 0;
		}
	};
	return x;
};




function ShowPesanWarningKasirigd(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorKasirigd(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoKasirigd(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};

function printbillIGD()
{
    Ext.Ajax.request
    (
        {
                url: baseURL + "index.php/main/CreateDataObj",
                params: dataparamcetakbill_vikasirDaftarIGD(),
                success: function(o)
                {
                        var cst = Ext.decode(o.responseText);
                        if (cst.success === true)
                        {

                        }
                        else if  (cst.success === false && cst.pesan===0)
                        {
                                ShowPesanWarning_viDaftar('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
                        }
                        else
                        {
                                ShowPesanError_viDaftar('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
                        }
                }
        }
    );
};

var tmpkdkasirIGD = '1';
function dataparamcetakbill_vikasirDaftarIGD()
{
    var paramscetakbill_vikasirDaftarIGD =
		{      
                    Table: 'DirectPrintingIgd',
                    No_TRans: Ext.get('txtNoTransaksiKasirigdKasir').getValue(),
					Medrec: Ext.get('txtNoMedrecDetransaksi').getValue(),
					//kdUnit: rowSelectedKasirigdKasir.data.KD_UNIT,
					kdUnit: CurrentKdUnit_KasirIGD,
                    KdKasir : tmpkdkasirIGD,
                    JmlBayar: Ext.get('txtJumlah2EditData_viKasirigd').getValue(),
                    JmlDibayar: Ext.get('txtJumlahEditData_viKasirigd').getValue(),
					printer: Ext.getCmp('cbopasienorder_printer_kasirigd').getValue()
		};
	console.log(Ext.getCmp('gridDTLTRKasirigd').getStore().data.items);
	var urut=[];
	var kd_produk=[];
	var items=Ext.getCmp('gridDTLTRKasirigd').getStore().data.items;
	for(var i=0,iLen=items.length; i<iLen; i++){
		if(items[i].data.TAG==true){
			urut.push(items[i].data.URUT);
			kd_produk.push(items[i].data.KD_PRODUK);
		}
	}
	paramscetakbill_vikasirDaftarIGD['no_urut[]']=urut;
	paramscetakbill_vikasirDaftarIGD['kd_produk[]']=kd_produk;
    return paramscetakbill_vikasirDaftarIGD;
};

function printkwitansiIGD(){
	var dlg = Ext.MessageBox.prompt('Print Kwitansi', 'Nama Pembayar :', function(btn, combo){
		if (btn == 'ok'){
			var IGDParam=dataparamcetakkwitansi_vikasirDaftarIGD();
			IGDParam['namaPembayar']=combo;
			Ext.Ajax.request({
				url: baseURL + "index.php/main/CreateDataObj",
				params: IGDParam,
				success: function(o){
					var cst = Ext.decode(o.responseText);
					if (cst.success === true){
						
					}else if  (cst.success === false && cst.pesan !== ''){
						ShowPesanWarningKasirigd('tidak berhasil melakukan pencetakan '  + cst.pesan,'Cetak Data');
					}else{
						ShowPesanWarningKasirigd('tidak berhasil melakukan pencetakan '  + cst.pesan,'Cetak Data');
					}
				}
			});
		}
	},this,false,Ext.getCmp('txtNamaPasienDetransaksi').getValue());
}

function dataparamcetakkwitansi_vikasirDaftarIGD(){
    var paramscetakbill_vikasirDaftarIGD ={      
		Table: 'DirectKwitansiIgd',
		No_TRans: Ext.get('txtNoTransaksiKasirigdKasir').getValue(),
		//kdUnit: rowSelectedKasirigdKasir.data.KD_UNIT,
		kdUnit: CurrentKdUnit_KasirIGD,
		KdKasir : tmpkdkasirIGD,
		JmlBayar: Ext.get('txtJumlah2EditData_viKasirigd').getValue(),
		JmlDibayar: Ext.get('txtJumlahEditData_viKasirigd').getValue(),
		printer: Ext.getCmp('cbopasienorder_printer_kasirigd').getValue()
	};
	console.log(Ext.getCmp('gridDTLTRKasirigd').getStore().data.items);
	var urut=[];
	var kd_produk=[];
	var items=Ext.getCmp('gridDTLTRKasirigd').getStore().data.items;
	for(var i=0,iLen=items.length; i<iLen; i++){
		if(items[i].data.TAG==true){
			urut.push(items[i].data.URUT);
			kd_produk.push(items[i].data.KD_PRODUK);
		}
	}
	paramscetakbill_vikasirDaftarIGD['no_urut[]']=urut;
	paramscetakbill_vikasirDaftarIGD['kd_produk[]']=kd_produk;
    return paramscetakbill_vikasirDaftarIGD;
}

function cekKomponen(kd_produk,kd_tarif,kd_unit,urut,harga){
	Ext.Ajax.request
	({
		url: baseURL + "index.php/main/functionIGD/cekKomponen",
		params: {
			kd_produk:kd_produk,
			kd_tarif:kd_tarif,
			kd_unit:kd_unit
		},
		failure: function(o)
		{
			ShowPesanErrorKasirrwj('Hubungi Admin', 'Error');
		},	
		success: function(o) 
		{   
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) 
			{
				if(cst.komponen > 0){
					currentJasaDokterKdTarif_KasirIGD=kd_tarif;
					currentJasaDokterKdProduk_KasirIGD=kd_produk;
					currentJasaDokterUrutDetailTransaksi_KasirIGD=urut;
					currentJasaDokterHargaJP_KasirIGD=harga;
					PilihDokterLookUp_KasirIGD()
				}
			} 
			else 
			{
				ShowPesanErrorKasirrwj('Gagal cek komponen pelayanan', 'Error');
			};
		}
	});
};

function PilihDokterLookUp_KasirIGD(edit) 
{
	var fldDetail = [];
    dsGridJasaDokterPenindak_KasirIGD = new WebApp.DataStore({ fields: fldDetail });
    var GridTrDokterColumnModel =  new Ext.grid.ColumnModel([
        new Ext.grid.RowNumberer(),
		{
			header			: 'kd_component',
			dataIndex		: 'kd_component',
			width			: 80,
			menuDisabled	: true,
			hidden 		: true
        },
		{
			header			: 'tgl_berlaku',
			dataIndex		: 'tgl_berlaku',
			width			: 80,
			menuDisabled	: true,
			hidden 		: true
        },
		{
			header			: 'Komponent',
			dataIndex		: 'component',
			width			: 200,
			menuDisabled	: true,
			hidden 		: false
        },
        {
			header			: 'kd_dokter',
			dataIndex		: 'kd_dokter',
			width			: 80,
			menuDisabled	: true,
			hidden 		: true
        },{
            header			:'Nama Dokter',
            dataIndex		: 'nama',
            sortable		: false,
            hidden			: false,
			menuDisabled	: true,
			width			: 250,
            editor			: new Nci.form.Combobox.autoComplete({
				store	: dsgridpilihdokterpenindak_KasirIGD,
				select	: function(a,b,c){
					var line	= GridDokterTr_KasirIGD.getSelectionModel().selection.cell[0];
					dsGridJasaDokterPenindak_KasirIGD.getRange()[line].data.kd_dokter=b.data.kd_dokter;
					dsGridJasaDokterPenindak_KasirIGD.getRange()[line].data.nama=b.data.nama;
					GridDokterTr_KasirIGD.getView().refresh();
				},
				insert	: function(o){
					return {
						kd_dokter       : o.kd_dokter,
						nama       		: o.nama,
						text			:  '<table style="font-size: 11px;"><tr><td width="50">'+o.kd_dokter+'</td><td width="200">'+o.nama+'</td></tr></table>'
					}
				},
				param	: function(){
					var params={};
					params['kd_unit'] = CurrentKDUnitIGD;
					params['penjas'] = 'igd';
					return params;
				},
				url		: baseURL + "index.php/main/functionRWJ/getdokterpenindak",
				valueField: 'nama_obat',
				displayField: 'text',
				listWidth: 380
			})
			//getTrDokter(dsTrDokter)
	    },
        ]
    );
	
	
	GridDokterTr_KasirIGD= new Ext.grid.EditorGridPanel({
		id			: 'GridDokterTr_KasirIGD',
		stripeRows	: true,
		width		: 487,
		height		: 160,
        store		: dsGridJasaDokterPenindak_KasirIGD,
        border		: true,
        frame		: false,
        autoScroll	: true,
        cm			: GridTrDokterColumnModel,
		listeners	: {
			rowclick: function( $this, rowIndex, e )
			{
				trcellCurrentTindakan_KasirIGD = rowIndex;
    		},
			celldblclick: function(gridView,htmlElement,columnIndex,dataRecord){
				
			}
		},
		viewConfig	: {forceFit: true},
    });
 
	
    var lebar = 500;
    var FormLookUDokter_KasirIGD = new Ext.Window
    (
        {
            id: 'winTRDokterPenindak_KasirIGD',
            title: 'Pilih Dokter Penindak',
            closeAction: 'destroy',
            width: 500,
            height: 220,
            border: false,
            resizable: false,
            iconCls: 'Request',
			constrain : true,    
			modal: true,
           	items: [ 
				GridDokterTr_KasirIGD
			],
			tbar :
			[
				{
					xtype:'button',
					text:'Simpan',
					iconCls	: 'save',
					hideLabel:true,
					id: 'BtnOktrDokter',
					handler:function()
					{
						Datasave_KasirIGD(false,true);
					}
				},
				'-',
			],
            listeners:
            { 
            }
        }
    );
	FormLookUDokter_KasirIGD.show();
	if(edit == true){
		GetgridEditDokterPenindakJasa_KasirIGD()
	} else{
		GetgridPilihDokterPenindakJasa_KasirIGD(currentJasaDokterKdProduk_KasirIGD,currentJasaDokterKdTarif_KasirIGD);
	}
	
};

function GetgridEditDokterPenindakJasa_KasirIGD(){
	Ext.Ajax.request
	({
		url: baseURL + "index.php/main/functionIGD/viewgrideditjasadokterpenindak",
		params: {
			kd_unit:CurrentKDUnitIGD,
			urut:currentJasaDokterUrutDetailTransaksi_KasirIGD,
			kd_kasir:CurrentKdKasir_KasirIGD,
			no_transaksi:CurrentNoTransaksi_KasirIGD,
			tgl_transaksi:CurrentTglTransaksi_KasirIGD,
			
		},
		failure: function(o)
		{
			ShowPesanErrorKasirigd('Hubungi Admin', 'Error');
		},	
		success: function(o) 
		{   
			dsGridJasaDokterPenindak_KasirIGD.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) 
			{
				if(cst.totalrecords > 0){
					var recs=[],
						recType=dsGridJasaDokterPenindak_KasirIGD.recordType;
					for(var i=0; i<cst.ListDataObj.length; i++){
						recs.push(new recType(cst.ListDataObj[i]));
					}
					dsGridJasaDokterPenindak_KasirIGD.add(recs);
					GridDokterTr_KasirIGD.getView().refresh();
				} else{
					GetgridPilihDokterPenindakJasa_KasirIGD(currentJasaDokterKdProduk_KasirIGD,currentJasaDokterKdTarif_KasirIGD);
				}
			} 
			else 
			{
				ShowPesanErrorKasirigd('Gagal membaca list dokter penindak', 'Error');
			};
		}
	});
}

function GetgridPilihDokterPenindakJasa_KasirIGD(kd_produk,kd_tarif){
	Ext.Ajax.request
	({
		url: baseURL + "index.php/main/functionRWJ/viewgridjasadokterpenindak",
		params: {
			kd_produk:kd_produk,
			kd_tarif:kd_tarif,
			kd_unit:kodeunit//CurrentKDUnitIGD
		},
		failure: function(o)
		{
			ShowPesanErrorKasirigd('Hubungi Admin', 'Error');
		},	
		success: function(o) 
		{   
			dsGridJasaDokterPenindak_KasirIGD.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) 
			{
				var recs=[],
					recType=dsGridJasaDokterPenindak_KasirIGD.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));
				}
				dsGridJasaDokterPenindak_KasirIGD.add(recs);
				GridDokterTr_KasirIGD.getView().refresh();
			} 
			else 
			{
				ShowPesanErrorKasirigd('Gagal membaca history diagnosa', 'Error');
			};
		}
	});
}


function SimpanJasaDokterPenindak_KasirIGD(kd_produk,kd_tarif,urut,harga){
	Ext.Ajax.request
	({
		url: baseURL + "index.php/main/functionIGD/savejasadokterpenindak",
		params: getParamsJasaDokterPenindak_KasirIGD(urut,harga),
		failure: function(o)
		{
			ShowPesanErrorKasirigd('Hubungi Admin', 'Error');
		},	
		success: function(o) 
		{   
			//dsGridJasaDokterPenindak_KasirRWJ.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) 
			{
				ShowPesanInfoKasirigd('Dokter penindak berhasil disimpan','Information');
				SimpanJasaDokterPenindak_KASIRIGD_SQL(kd_produk,kd_tarif,urut,harga);
				GetgridEditDokterPenindakJasa_KasirIGD();
			} 
			else 
			{
				ShowPesanErrorKasirigd('Gagal membaca history diagnosa', 'Error');
			};
		}
	});
}

function getParamsJasaDokterPenindak_KasirIGD(urut,harga){
	var params = {
		kd_kasir:CurrentKdKasir_KasirIGD,
		no_transaksi:CurrentNoTransaksi_KasirIGD,
		tgl_transaksi:CurrentTglTransaksi_KasirIGD,
		urut:urut,
		harga:harga
	};
	params['jumlah'] = dsGridJasaDokterPenindak_KasirIGD.getCount();
	for(var i = 0 ; i < dsGridJasaDokterPenindak_KasirIGD.getCount();i++)
	{
		params['kd_component-'+i]=dsGridJasaDokterPenindak_KasirIGD.data.items[i].data.kd_component;
		params['kd_dokter-'+i]=dsGridJasaDokterPenindak_KasirIGD.data.items[i].data.kd_dokter;
	}
	return params;
}


// ===========================================================INTEGRASI SQL=====================================================================================================================
function Datasave_KasirigdKasir_SQL() 
{	
	Ext.Ajax.request
	({
		url: baseURL + "index.php/gawat_darurat_sql/functionIGD/savepembayaran",
		params: getParamDetailTransaksiKasirigd(),
		failure: function(o)
		{
			ShowPesanWarningKasirigd('SQL. Error simpan pembayaran, Hubungi Admin!', 'Gagal');
		},	
		success: function(o) 
		{
			var cst = Ext.decode(o.responseText);
			if (cst.success === true) 
			{
				loadMask.hide();
				ShowPesanInfoKasirigd(nmPesanSimpanSukses,nmHeaderSimpanData);
				RefreshDataKasirigdKasirDetail(Ext.get('txtNoTransaksiKasirigdKasir').dom.value);
				RefreshDataFilterKasirIgdKasir();
			}
			else 
			{
				ShowPesanWarningKasirigd('SQL. Data gagal disimpan segera Hubungi Admin!', 'Gagal');
			};
		}
	})
};

function Datasave_KasirIGD_SQL() 
{	
	Ext.Ajax.request
	 (
		{
			url: baseURL + "index.php/gawat_darurat_sql/functionIGD/savedetailproduk",
			params: getParamDetailTransaksiIGD(),
			failure: function(o)
			{
				ShowPesanWarningKasirigd('SQL. Error simpan tindakan, Hubungi Admin!', 'Gagal');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
				
				}
				else 
				{
					ShowPesanWarningKasirigd('SQL. Gagal simpan tindakan!', 'Gagal');
				};
			}
		}
	)
	
};

function DataDeleteKasirIGDDetail_SQL()
{
    Ext.Ajax.request
    (
        {
            url: baseURL + "index.php/gawat_darurat_sql/functionIGD/deletetindakantambahan",
            params:  getParamDataDeleteKasirIGDDetail(),
			failure: function(o)
			{
				ShowPesanWarningKasirigd("SQL. Error hapus baris tindakan, Hubungi Admin!",'WARNING');
				loadMask.hide();
			},
            success: function(o)
            {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true)
                {
					
                }
                else
                {
                    ShowPesanWarningKasirigd('SQL. Gagal mengapus baris tindakan!','WARNING');
                };
            }
        }
    )
};


function DataDeleteKasirigdKasirDetail_SQL()
{
    Ext.Ajax.request
    (
        {
            url: baseURL + "index.php/gawat_darurat_sql/functionIGD/deletedetail_bayar",
            params:  getParamDataDeleteKasirigdKasirDetail(),
			failure: function(o)
			{
				ShowPesanWarningKasirigd("SQL. Error hapus baris pembayaran, Hubungi Admin!",'WARNING');
				loadMask.hide();
			},
            success: function(o)
            {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true)
                {
					
				}
                else
                {
                    ShowPesanWarningKasirigd('SQL. Gagal menghapus baris pembayaran!','WARNING');
					loadMask.hide();
                };
            }
        }
    )
};


function UpdateTutuptransaksi_SQL(mBol) 
{	
	Ext.Ajax.request
	({
		//url: "./Datapool.mvc/CreateDataObj",
		url: baseURL + "index.php/gawat_darurat_sql/functionIGD/ubah_co_status_transksi",
		params: paramUpdateTransaksi(),
		failure: function(o)
		{
			ShowPesanWarningKasirigd('Data gagal tersimpan segera hubungi Admin', 'Gagal');
		},	
		success: function(o) 
		{
			//RefreshDatahistoribayar(Ext.get('cellSelectedtutup_kasirigd);
		
			var cst = Ext.decode(o.responseText);
			if (cst.success === true) 
			{
				cellSelectedtutup_kasirigd='';
			}
			else 
			{
				ShowPesanWarningKasirigd('SQL. Gagal tutup transaksi, segera hubungi Admin', 'Gagal');
				cellSelectedtutup_kasirigd='';
			};
		}
	})
};

function TransferData_IGD_SQL(mBol) 
{	
	if(parseInt(Ext.getCmp('txtjumlahtranfer_KasirIGD').getValue()) > parseInt(SisaPembayaranKasirIGD)){
		ShowPesanErrorKasirigd('Jumlah yang di transfer melebihi saldo tagihan!', 'Gagal');
	} else{
		Ext.Ajax.request
		 (
			{
				url: baseURL + "index.php/gawat_darurat_sql/functionIGD/saveTransfer",
				params: getParamTransferRwi(),
				failure: function(o)
				{
					ShowPesanWarningKasirigd('SQL. Error transfer pembayaran. Hubungi Admin!', 'Gagal');
					RefreshDataKasirIGDDetail(notransaksi);
				},	
				success: function(o) 
				{
					RefreshDataKasirIGDDetail(notransaksi);
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						
					}
					else 
					{
						ShowPesanWarningKasirigd('Error '+cst.pesan+'. Gagal melakukan transfer!', 'Gagal');
					};
				}
			}
		)
	}
};

function BatalTransaksi_SQL(notrans,notransbaru,urutmasuk,urutmasukbaru)
{

	Ext.Ajax.request
	 (
		{
			url: baseURL + "index.php/gawat_darurat_sql/functionIGD/batal_transaksi",
			params: getParamDetailBatalTransaksiIGD_SQL(notrans,notransbaru,urutmasuk,urutmasukbaru),
			failure: function(o)
			{
				ShowPesanWarningKasirigd('SQL. ' + Ext.decode(o.responseText), 'Gagal');
			},
			success: function(o)
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					cellSelectedtutup_kasirigd='';
				}
				else
				{
					ShowPesanWarningKasirigd('SQL. ' + Ext.decode(o.responseText), 'Gagal');
					cellSelectedtutup_kasirigd='';
				};
			}
		}
	)
};

function getParamDetailBatalTransaksiIGD_SQL(notrans,notransbaru,urutmasuk,urutmasukbaru)
{
	if(tampungtypedata==='')
	{
		tampungtypedata=0;
	};
	var line = dsTRDetailHistoryListIGD.getCount();
	var totalhehe=0;
	for (var i = 0 ; i < line; i++)
	{
		totalhehe += parseInt(dsTRDetailHistoryListIGD.data.items[i].data.BAYAR);
		
	}
    var params =
	{
		notransaksi : notrans,
		notransbaru	: notransbaru,
		urutmasuk	: urutmasuk,
		urutmasukbaru: urutmasukbaru,
		kdPasien 	: kodepasien,
		noTrans 	: notransaksi,
		kdUnit 		: kodeunit,
		Jumlah		: totalhehe,
		kdDokter 	: kdokter_btl,
		tglTrans 	: tgltrans,
		kdCustomer	: kdcustomeraa,
		Keterangan	: variablebatalhistori_igd,
		
	};
    return params
};

function SimpanJasaDokterPenindak_KASIRIGD_SQL(kd_produk,kd_tarif,urut,harga){
	Ext.Ajax.request
	({
		url: baseURL + "index.php/rawat_jalan_sql/functionRWJ/savejasadokterpenindak",
		params: getParamsJasaDokterPenindak_KasirIGD(urut,harga),
		failure: function(o)
		{
			//ShowPesanErrorKasirigd('SQL. Hubungi Admin', 'Error');
		},	
		success: function(o) 
		{   
			//dsGridJasaDokterPenindak_KasirRWJ.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) 
			{
				
			} 
			else 
			{
				ShowPesanErrorKasirigd('SQL. Gagal membaca history diagnosa', 'Error');
			};
		}
	});
}

function getIdGetDataSettingKasirIGD()
{
    Ext.Ajax.request(
    {
        url: baseURL + "index.php/setup/manageutillity/getDataSetting", 
        params: {
            UserId:0
        },
        success: function(response, opts) {
            var cst = Ext.decode(response.responseText);
            Ext.getCmp('cbopasienorder_printer_kasirigd').setValue(cst.p_kasir);
            console.log(cst.p_kasir);
        },
        failure: function(response, opts) {
        }
    });
};

function cekTindakan(data){
	Ext.Ajax.request
	({
		url: baseURL + "index.php/main/functionIGD/ceksavetindakan",
		params: getParamsCekTindakan_KasirIGD(data),
		failure: function(o)
		{
			ShowPesanErrorKasirigd('Error. Hubungi Admin!', 'Error');
		},	
		success: function(o) 
		{   
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) 
			{
				
			} 
			else 
			{
				ShowPesanErrorKasirigd('Gagal cek tindakan!', 'Error');
			};
		}
	});
}

function getParamsCekTindakan_KasirIGD(data){
	var params = {
		kd_kasir:data.KD_KASIR,
		no_transaksi:data.NO_TRANSAKSI,
		tgl_transaksi:data.TANGGAL_TRANSAKSI,
		urut_masuk:data.URUT_MASUK,
		kd_pasien:data.KD_PASIEN,
		kd_unit:data.KD_UNIT
	};
	return params;
}

function getsaldotagihan_KasirIGD(){
	var total=0;
	if(Ext.getCmp('txtjumlahtranfer_KasirIGD').getValue() != 0){
		total = parseInt(SisaPembayaranKasirIGD) - parseInt(Ext.getCmp('txtjumlahtranfer_KasirIGD').getValue());
		Ext.getCmp('txtsaldotagihan_KasirIGD').setValue(formatCurrency(total))
	} else{
		Ext.getCmp('txtsaldotagihan_KasirIGD').setValue(formatCurrency(SisaPembayaranKasirIGD))
	}
}