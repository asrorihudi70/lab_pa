var kdUnit = '3';

var CurrentKasirIGD =
{
    data: Object,
    details: Array,
    row: 0
};
var CurrentHistory =
{
    data: Object,
    details: Array,
    row: 0
};

var tmp_group_dokter        = 0;
var DataStoreSecondGridStore_PJIGD = new Ext.data.JsonStore();
var fieldsDokterPenindak = [
    {name: 'KD_DOKTER', mapping : 'KD_DOKTER'},
    {name: 'NAMA', mapping : 'NAMA'}
];
dsDataDokterPenindak_PJ_IGD = new WebApp.DataStore({ fields: fieldsDokterPenindak });
var DataStorefirstGridStore_PJIGD  = new Ext.data.JsonStore();
var rowSelectedPJRWI;
var tmpKdJob                = 1;
var dataRowIndexDetail      = 0;

var gridDTLTRIGD;
var dsprinter_kasirigd;
var varkd_tarif_igd;
var vKdPasien;
var vKdDokter;
var nilai_kd_tarif;
var syssetting='igd_default_klas_produk';
var tampungtypedata;
var Trkdunit2;
var gridEditTarifKasirIGD;
var dataRowIndexDetail   = 0;
var tmp_data_produk;
var jeniscus_IGD;
var FormLookUpGantidokterKasirIGD;
var FormLookUpsdetailTRTransfer_IGD;
var tanggaltransaksitampung;
var DataStoreProduk_KasirIGD;
var winIGDBukaTransaksiPasswordDulu;
var variablebatalhistori_igd;
var variablebukahistori_igd;
var ProdukDataIGD;
var GridDokterTrKasir_IGD;
var FormLookUpsdetailTRKelompokPasien_igd;
var panelActiveDataPasien = 0;
var statusCekDetail;
var CurrentRowPemeriksaanIGD;
var winIGDPasswordDulu;
var	tmp_kodeunitkamarigd;
var	tmp_kdspesialigd;
var	tmp_nokamarigd;
var KasirIGDComboBoxKdProduk;
var KasirIGDComboBoxKdProdukKP;
var KasirIGDDataStoreProduk=new Ext.data.ArrayStore({id: 0,fields:['kd_produk','deskripsi','harga','tglberlaku'],data: []});
var currentJasaDokterUrutDetailTransaksi_KasirIGD;
var currentJasaDokterKdProduk_KasirIGD;
var currentJasaDokterKdTarif_KasirIGD;
var currentJasaDokterHargaJP_KasirIGD;
var dsGridJasaDokterPenindak_KasirIGD;
var GridDokterTr_KasirIGD;
var CurrentTglTransaksi_KasirIGD;
var CurrentNoTransaksi_KasirIGD;
var CurrentKdKasir_KasirIGD;
var CurrentKdCustomer_KasirIGD;
var CurrentKdPasien_KasirIGD;
var CurrentKdUnit_KasirIGD;
var CurrentUrutMasuk_KasirIGD;
var CurrentTglMasuk_KasirIGD;
var currentTOTAL_KasirIGD;
var dsDataStoreGridPoduk=new Ext.data.JsonStore();;
var currentCOSTATUS_KasirIGD;
var SisaPembayaranKasirIGD=0;

/* ====================================================== VARIABLE 2017-04-25*/
var DataStorefirstGridStore_IGD  = new Ext.data.JsonStore();
var DataStoreSecondGridStore_IGD  = new Ext.data.JsonStore();
var dsDataDaftarPenangan_KASIR_IGD;
var tmpKdJob 	= 1;
var tmpkd_unit 	= '301';
var tmp_notransaksi;
var tmp_urut;
var tmp_tgltransaksi;
var tmp_kdproduk;
var tmp_kdunit;
var tmp_folio;
var tmp_tarif;
var dataRow;
var tmp_tglberlaku;

var fieldsDokterPenindak = [
	{name: 'KD_DOKTER', mapping : 'KD_DOKTER'},
	{name: 'NAMA', mapping : 'NAMA'}
];

dsgridpilihdokterpenindak_KasirIGD	= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_dokter','nama'],
	data: []
});

var mRecordKasirigd = Ext.data.Record.create
(
    [
       {name: 'DESKRIPSI2', mapping:'DESKRIPSI2'},
       {name: 'KD_PRODUK', mapping:'KD_PRODUK'},
       {name: 'DESKRIPSI', mapping:'DESKRIPSI'},
       {name: 'KD_TARIF', mapping:'KD_TARIF'},
       {name: 'HARGA', mapping:'HARGA'},
       {name: 'QTY', mapping:'QTY'},
       {name: 'TGL_TRANSAKSI', mapping:'TGL_TRANSAKSI'},
       {name: 'DESC_REQ', mapping:'DESC_REQ'},
       {name: 'URUT', mapping:'URUT'}
    ]
);
var mRecordIGD = Ext.data.Record.create
(
    [
       {name: 'DESKRIPSI2', mapping:'DESKRIPSI2'},
       {name: 'KD_PRODUK', mapping:'KD_PRODUK'},
       {name: 'DESKRIPSI', mapping:'DESKRIPSI'},
       {name: 'KD_TARIF', mapping:'KD_TARIF'},
       {name: 'HARGA', mapping:'HARGA'},
       {name: 'QTY', mapping:'QTY'},
       {name: 'TGL_TRANSAKSI', mapping:'TGL_TRANSAKSI'},
       {name: 'DESC_REQ', mapping:'DESC_REQ'},
       {name: 'URUT', mapping:'URUT'}
    ]
);
var Kdtransaksi;
var tapungkd_pay;
var dsTRDetailHistoryListIGD;
var AddNewHistory                        = true;
var selectCountHistory                   = 50;
var now                                  = new Date();
var rowSelectedHistory;
var cellSelecteddeskripsi;
var FormLookUpsdetailTRHistory;
var now                                  = new Date();
var cellSelectedtutup_kasirigd;
var vkd_unit;
var kdokter_btl;
var nowTglTransaksiIGD                   = new Date();
var kdcustomeraa;
var labelisi;
var jenispay;
var variablehistori;
var selectCountStatusByr_viKasirigdKasir ='Belum Lunas';
var dsTRKasirigdKasirList;
var dsTRDetailKasirigdKasirList;
var AddNewKasirigdKasir                  = true;
var selectCountKasirigdKasir             = 50;
var now                                  = new Date();
var rowSelectedKasirigdKasir;

var FormLookUpsdetailTRKasirigd;
var valueStatusCMKasirigdView='All';
var nowTglTransaksiIGD2 = nowTglTransaksiIGD.format('d-M-Y');
var nowTglTransaksiIGD3 = nowTglTransaksiIGD.format('Y-m-d');
var dsComboBayar;
var vkode_customer;
var vflag;
 var gridDTLTRKasirigd;
 
var kodekasir;
var notransaksi ;
var tgltrans;
var kodepasien;
var namapasien;
var kodeunit;
var namaunit;
var kodepay;
var vNoSEP;
var vCustomer;
var vNoAsuransi;
var vkode_customer_IGD;
var FormLookUpsdetailTRKonsultasi_DataPasien;
var vKdUnitDulu;
var vKdUnit;
var vKdDokter;
var dsDokterRequestEntry;
var DataStoreGridKomponent;
var dsDataStoreGridTarifKomponent = new Ext.data.JsonStore();
var Field = ['KD_DOKTER','NAMA'];
dsDokterRequestEntry = new WebApp.DataStore({fields: Field});
var uraianpay;
var tglubahtindakankasirigd              = new Date();
var nowCurrentDateKasirIGD               = nowTglTransaksiIGD.format('d/M/Y');
CurrentPage.page = getPanelKasirigd(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelKasirigd(mod_id) {
    var Field = ['KD_DOKTER','NO_TRANSAKSI','KD_UNIT','KD_PASIEN','NAMA','NAMA_UNIT','ALAMAT',
		'TANGGAL_TRANSAKSI','NAMA_DOKTER','KD_CUSTOMER','CUSTOMER','URUT_MASUK','FLAG','LUNAS','KET_PAYMENT','CARA_BAYAR',
		'JENIS_PAY','KD_PAY','POSTING','TYPE_DATA','CO_STATUS','KD_KASIR','NO_SEP','NO_ASURANSI'];
    dsTRKasirigdKasirList = new WebApp.DataStore({ fields: Field });
	ProdukDataIGD = new WebApp.DataStore({ fields: ['KD_PRODUK','KD_KLAS','DESKRIPSI','TARIF','TGL_BERLAKU','KD_TARIF'] });
    refeshKasirIgdKasir();
    var grListTRKasirigd = new Ext.grid.EditorGridPanel({
		stripeRows: true,
		store: dsTRKasirigdKasirList,
		columnLines: false,
		autoScroll:true,
		anchor: '100% 35%',
		sort :false,
		sm: new Ext.grid.RowSelectionModel({
			singleSelect: true,
			listeners:{
				rowselect: function(sm, row, rec){
					rowSelectedKasirigdKasir = dsTRKasirigdKasirList.getAt(row);
					rowSelectedPJRWI=rowSelectedKasirigdKasir;
					rowSelectedPJIGD=rowSelectedKasirigdKasir;
					
					CurrentKDUnitIGD         = rowSelectedKasirigdKasir.data.KD_UNIT;
					CurrentKDCustomerIGD     = rowSelectedKasirigdKasir.data.KD_CUSTOMER;
					CurrentKDDokerIGD        = rowSelectedKasirigdKasir.data.KD_DOKTER;
					CurrentURUT_MASUKIGD     = rowSelectedKasirigdKasir.data.URUT_MASUK;
					CurrentKD_PASIENIGD      = rowSelectedKasirigdKasir.data.KD_PASIEN;
					vKdPasien                = rowSelectedKasirigdKasir.data.KD_PASIEN;
					vKdDokter                = rowSelectedKasirigdKasir.data.KD_DOKTER;
					CurrentKdPasien_KasirIGD = rowSelectedKasirigdKasir.data.KD_PASIEN;
					CurrentKdUnit_KasirIGD 	 = rowSelectedKasirigdKasir.data.KD_UNIT;
					CurrentUrutMasuk_KasirIGD = rowSelectedKasirigdKasir.data.URUT_MASUK;
					CurrentTglMasuk_KasirIGD = rowSelectedKasirigdKasir.data.TANGGAL_TRANSAKSI;
					vNoSEP = rowSelectedKasirigdKasir.data.NO_SEP;
					vNoAsuransi = rowSelectedKasirigdKasir.data.NO_ASURANSI;
					vCustomer = rowSelectedKasirigdKasir.data.CUSTOMER;
					cekDetailTransaksi();
					/*UPDATE ISAL*/
					cekTransferApotek(rowSelectedKasirigdKasir.data.NO_TRANSAKSI);
					/**/
					//getProdukIGD(ProdukDataIGD);
				}
			}
		}),
		listeners:{
			rowdblclick: function (sm, ridx, cidx){
				rowSelectedKasirigdKasir = dsTRKasirigdKasirList.getAt(ridx);
				if (rowSelectedKasirigdKasir != undefined){ 	
					if ( rowSelectedKasirigdKasir.data.LUNAS=='f' ){
						KasirLookUpigd(rowSelectedKasirigdKasir.data);
					}else{
						ShowPesanWarningKasirigd('Pembayaran Tidak Bisa Di Lakukan karena Sudah Lunas','Pembayaran');
					}
				}else{
					ShowPesanWarningKasirigd('Silahkan Pilih data   ','Pembayaran');
				}
			},
			rowclick: function (sm, ridx, cidx){
				loadMask.show();
				/*UPDATE ISAL*/
				cekTransferApotek(rowSelectedKasirigdKasir.data.NO_TRANSAKSI);
				/**/
				cekDetailTransaksi();
				cellSelectedtutup_kasirigd = rowSelectedKasirigdKasir.data.NO_TRANSAKSI;
				
				if ( rowSelectedKasirigdKasir.data.LUNAS=='t' && rowSelectedKasirigdKasir.data.CO_STATUS =='t'){
					Ext.getCmp('btnTutupTransaksiKasirigd').disable();
					Ext.getCmp('btnHpsBrsKasirigd').disable();
					Ext.getCmp('btnLookupIGD').disable();
					Ext.getCmp('btnSimpanIGD').disable();
					Ext.getCmp('btnHpsBrsIGD').disable();
					Ext.getCmp('btnTambahBrsKasirIGD').disable();
				}else if ( rowSelectedKasirigdKasir.data.LUNAS=='f' && rowSelectedKasirigdKasir.data.CO_STATUS =='f'){
					Ext.getCmp('btnTutupTransaksiKasirigd').disable();
					Ext.getCmp('btnHpsBrsKasirigd').enable();
					Ext.getCmp('btnLookupIGD').enable();
					Ext.getCmp('btnSimpanIGD').enable();
					Ext.getCmp('btnHpsBrsIGD').enable();
					Ext.getCmp('btnTambahBrsKasirIGD').enable();
				}else if ( rowSelectedKasirigdKasir.data.LUNAS=='t' && rowSelectedKasirigdKasir.data.CO_STATUS =='f'){
					Ext.getCmp('btnTutupTransaksiKasirigd').enable();
					Ext.getCmp('btnHpsBrsKasirigd').enable();
					Ext.getCmp('btnLookupIGD').enable();
					Ext.getCmp('btnSimpanIGD').enable();
					Ext.getCmp('btnHpsBrsIGD').enable();
					Ext.getCmp('btnTambahBrsKasirIGD').enable();
				}
				CurrentTglTransaksi_KasirIGD = rowSelectedKasirigdKasir.data.TANGGAL_TRANSAKSI;
				CurrentNoTransaksi_KasirIGD = rowSelectedKasirigdKasir.data.NO_TRANSAKSI;
				CurrentKdKasir_KasirIGD = rowSelectedKasirigdKasir.data.KD_KASIR;
				currentCOSTATUS_KasirIGD = rowSelectedKasirigdKasir.data.CO_STATUS;
							
				kodekasir    		= rowSelectedKasirigdKasir.data.KD_KASIR;
				notransaksi  		= rowSelectedKasirigdKasir.data.NO_TRANSAKSI;
				tmp_notransaksi 	= rowSelectedKasirigdKasir.data.NO_TRANSAKSI;
				tgltrans     		= rowSelectedKasirigdKasir.data.TANGGAL_TRANSAKSI;
				kodepasien   		= rowSelectedKasirigdKasir.data.KD_PASIEN;
				namapasien   		= rowSelectedKasirigdKasir.data.NAMA;
				kodeunit    		= rowSelectedKasirigdKasir.data.KD_UNIT;
				namaunit     		= rowSelectedKasirigdKasir.data.NAMA_UNIT;
				kodepay      		= rowSelectedKasirigdKasir.data.KD_PAY;
				uraianpay    		= rowSelectedKasirigdKasir.data.CARA_BAYAR;
				kdcustomeraa 		= rowSelectedKasirigdKasir.data.KD_CUSTOMER;
				kdokter_btl 		= rowSelectedKasirigdKasir.data.KD_DOKTER;
				CurrentKDDokerIGD	= rowSelectedKasirigdKasir.data.KD_DOKTER;
				//cekTransferTindakanIGD(true,rowSelectedKasirigdKasir.data);
				RefreshDataKasirIGDDetail(rowSelectedKasirigdKasir.data.NO_TRANSAKSI);
				//cekTindakan(rowSelectedKasirigdKasir.data);
				RefreshDatahistoribayar(rowSelectedKasirigdKasir.data.NO_TRANSAKSI);
				//RefreshDataKasirIGDDetail(rowSelectedKasirigdKasir.data.NO_TRANSAKSI);
				Ext.Ajax.request({
					url: baseURL + "index.php/main/functionRWI/cek_cominal_pembayaran",
					params: {
						kd_kasir 	: '06',
						no_transaksi: rowSelectedKasirigdKasir.data.NO_TRANSAKSI,
					},
					failure: function (o){},
					success: function (o){
						var cst = Ext.decode(o.responseText);
						var sisaPembayaran = cst.harus_membayar - cst.telah_membayar;
						Ext.getCmp('txtJumlahBayar_KASIR_IGD').setValue(cst.harus_membayar);
					}
				});
				Ext.Ajax.request({
					url:  baseURL + "index.php/main/functionLABPoliklinik/gettarif",
					params: {
						kd_customer: rowSelectedKasirigdKasir.data.KD_CUSTOMER,
					},
					failure: function(o){},	    
					success: function(o) {
						var cst = Ext.decode(o.responseText);
						varkd_tarif_igd=cst.kd_tarif;
						getProdukIGD(rowSelectedKasirigdKasir.data.KD_UNIT);
					}
				});
				Ext.Ajax.request({
					//url: "./home.mvc/getModule",
					//url: baseURL + "index.php/main/getTrustee",
					url: baseURL + "index.php/main/getcurrentshift",
						params: {
						//UserID: 'Admin',
						command: '0',
						// parameter untuk url yang dituju (fungsi didalam controller)
					},
					failure: function(o){
						 var cst = Ext.decode(o.responseText);
					},	    
					success: function(o) {
						//var cst = Ext.decode(o.responseText);
						tampungshiftsekarang=o.responseText
						//Ext.get('txtNilaiShift').dom.value =tampungshiftsekarang ;
					}
				});
				Ext.Ajax.request({
					//url: "./home.mvc/getModule",
					//url: baseURL + "index.php/main/getTrustee",
					url: baseURL + "index.php/main/Getkdtarif",
					params: {
						//UserID: 'Admin',
						customer: rowSelectedKasirigdKasir.data.KD_CUSTOMER,
						// parameter untuk url yang dituju (fungsi didalam controller)
					},
					failure: function(o){
						 var cst = Ext.decode(o.responseText);
					},	    
					success: function(o) {										
						//var cst = Ext.decode(o.responseText);
						nilai_kd_tarif=o.responseText;
						//Ext.get('txtNilaiShift').dom.value =tampungshiftsekarang ;
					}
				});
			} 
		},
        cm: new Ext.grid.ColumnModel([
			{
				id: 'colLUNAScoba',
				header: 'Status Lunas',
				dataIndex: 'LUNAS',
				sortable: true,
				width: 90,
				align:'center',
				renderer: function(value, metaData, record, rowIndex, colIndex, store){
					switch (value){
						case 't':
							metaData.css = 'StatusHijau'; // 
							break;
						case 'f':
							metaData.css = 'StatusMerah'; // rejected
							break;
					}
					return '';
				}
			},{
				id: 'coltutuptr',
				header: 'Tutup transaksi',
				dataIndex: 'CO_STATUS',
				sortable: true,
				width: 90,
				align:'center',
				renderer: function(value, metaData, record, rowIndex, colIndex, store){
					switch (value){
						case 't':
							metaData.css = 'StatusHijau'; // 
							break;
						case 'f':
							metaData.css = 'StatusMerah'; // rejected
							break;
					}
					return '';
				}
			},{
				id: 'colReqIdViewKasirIGD',
				header: 'No. Transaksi',
				dataIndex: 'NO_TRANSAKSI',
				sortable: false,
				hideable:false,
				menuDisabled:true,
				width: 80
			},{
				id: 'colTglKasirigdViewKasirIGD',
				header: 'Tgl Transaksi',
				dataIndex: 'TANGGAL_TRANSAKSI',
				sortable: false,
				hideable:false,
				menuDisabled:true,
				width: 75,
				renderer: function(v, params, record){
					return ShowDate(record.data.TANGGAL_TRANSAKSI);
				}
			},{
				header: 'No. Medrec',
				width: 100,
				sortable: false,
				hideable:false,
				menuDisabled:true,
				dataIndex: 'KD_PASIEN',
				id: 'colKasirigderViewKasirIGD'
			},{
				header: 'Pasien',
				width: 190,
				sortable: false,
				hideable:false,
				menuDisabled:true,
				dataIndex: 'NAMA',
				id: 'colKasirigderViewKasirIGD'
			},{
				id: 'colLocationViewKasirIGD',
				header: 'Alamat',
				dataIndex: 'ALAMAT',
				sortable: false,
				hideable:false,
				menuDisabled:true,
				width: 170
			},{
				id: 'colDeptViewKasirIGD',
				header: 'Pelaksana',
				dataIndex: 'NAMA_DOKTER',
				sortable: false,
				hideable:false,
				menuDisabled:true,
				width: 150
			},{
				id: 'colunitViewKasirIGD',
				header: 'Unit',
				dataIndex: 'NAMA_UNIT',
				sortable: false,
				hideable:false,
				menuDisabled:true,
				width: 90
			},{
				id: 'colcustomerViewKasirIGD',
				header: 'Kel. Pasien',
				dataIndex: 'CUSTOMER',
				sortable: false,
				hideable:false,
				menuDisabled:true,
				width: 90
			},
		]),  
		viewConfig:{forceFit: true},
		tbar:[
			{
				id: 'btnEditKasirigd',
				text: 'Pembayaran',
				tooltip: nmEditData,
				iconCls: 'Edit_Tr',
				handler: function(sm, row, rec){
					if (rowSelectedKasirigdKasir != undefined){
						KasirLookUpigd(rowSelectedKasirigdKasir.data);
					}else{
						ShowPesanWarningKasirigd('Pilih data tabel  ','Pembayaran');
					}
				}
			}
			, ' ', ' ','' + ' ','' + ' ','' + ' ','' + ' ', ' ','' + '-',
			{
				text: 'Tutup Transaksi',
				id: 'btnTutupTransaksiKasirigd',
				tooltip: nmHapus,
				iconCls: 'remove',
				handler: function(){
					Ext.Ajax.request({
						url: baseURL + "index.php/main/functionIGD/cek_pelunasan",
						params: {
							kd_kasir 		: rowSelectedKasirigdKasir.data.KD_KASIR,
							no_transaksi 	: rowSelectedKasirigdKasir.data.NO_TRANSAKSI,
						},
						failure: function(o){
							ShowPesanWarningKasirigd('Error transfer pembayaran. Hubungi Admin!', 'Gagal');
						},	
						success: function(o){
							if(o.responseText != '0' || o.responseText != 0){
								ShowPesanWarningKasirigd('Pembayaran belum balance','Pembayaran');
							}else{
								if(cellSelectedtutup_kasirigd=='' || cellSelectedtutup_kasirigd=='undefined'){
									ShowPesanWarningKasirigd ('Pilih data tabel  ','Pembayaran');
								} else {	
									UpdateTutuptransaksi(false);	
									// Ext.getCmp('btnEditKasirigd').disable();
									Ext.getCmp('btnTutupTransaksiKasirigd').disable();
									Ext.getCmp('btnHpsBrsKasirigd').disable();
								}										
							}
						}
					});
				},
				disabled:true
			},{
				text: 'Buka Transaksi',
				id: 'btnBukaTransaksiKasirigd',
				tooltip: nmHapus,
				iconCls: 'remove',
				handler: function(){
					var dlg = Ext.MessageBox.prompt('Password', 'Masukan password untuk menghapus :', function(btn, combo){
						if (btn == 'ok'){
							Ext.Ajax.request({
								url 		: baseURL + "index.php/main/Controller_kunjungan/get_custom_data",
								params 		: {
									select 	: " COALESCE(count(*),0) as count ",
									where 	: " key_data = 'igd_password_buka_transaksi' AND setting = md5('"+combo+"') ",
									table 	: " sys_setting ",
								},
								success: function(o){
									var cst 	= Ext.decode(o.responseText);
									if (cst[0].count > 0) {
										if(cellSelectedtutup_kasirigd=='' || cellSelectedtutup_kasirigd=='undefined'){
											ShowPesanWarningKasirigd ('Pilih data tabel  ','Kasir');
										} else {	
											if(rowSelectedKasirigdKasir.data.CO_STATUS == 'f') {
												ShowPesanWarningKasirigd ('Transaksi sudah terbuka','Pembayaran');	
											} else {
												var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan Pembukaan Transaksi:', function(btn, combo){
													if (btn == 'ok'){
														variablebukahistori_igd=combo;
														if (combo!=''){
														 UpdateBukatransaksi();
														}else{
															ShowPesanWarningKasirigd('Silahkan isi alasan terlebih dahulu','Keterangan');
														}
													}
												});
												 //UpdateBukatransaksi();
												//fnDlgIGDBukaTransaksiPasswordDulu();
											}
										}			
									}else{
										ShowPesanWarningKasirigd ('Password salah','Peringatan');
									}	
								}
							});
						}
					});						
				},
				//disabled:true
			},{
				text: 'Batal Transaksi',
				id: 'btnBatalTransaksiKasirrwi',
				tooltip: nmHapus,
				iconCls: 'remove',
				handler: function(){
					//console.log(rowSelectedKasirigdKasir);
					/* if(cellSelectedtutup_kasirigd=='' || cellSelectedtutup_kasirigd=='undefined')
					{
						ShowPesanWarningKasirigd ('Pilih data tabel  ','Pembayaran');
					} else if(rowSelectedKasirigdKasir.data.CO_STATUS == 't')
					{
						ShowPesanWarningKasirigd ('Transaksi masih ditutup tidak bisa dibatalkan','Pembayaran');	
					} else if(rowSelectedKasirigdKasir.data.LUNAS == 't'){
						ShowPesanWarningKasirigd ('Transaksi sudah dibayar','Pembayaran');	
					} else{
						fnDlgIGDPasswordDulu();
					 } */
					 //if(statusCekDetail == false){
					var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan pengbatalan transaksi:', function(btn, combo){
						if (btn == 'ok'){
							if (combo!='') {
								var params = {
									kd_unit: CurrentKDUnitIGD,
									Tglkunjungan: CurrentTglMasuk_KasirIGD,
									Kodepasein: CurrentKD_PASIENIGD,
									urut: CurrentURUT_MASUKIGD,
									alasan:combo
								};
								loadMask.show();
								Ext.Ajax.request({
									url: baseURL + "index.php/main/functionRWJ/deletekunjungan_Revisi",
									// url: baseURL + "index.php/main/functionRWJ/deletekunjungan",
									params: params,
									failure: function (o){
										loadMask.hide();
										ShowPesanErrorKasirigd('Error proses database', 'Hapus Data');
									},
									success: function (o){
										var cst = Ext.decode(o.responseText);
										/*if (cst.success === true && cst.transaksi === true && cst.bayar === false)
										{
											ShowPesanWarning_viDataPasien('Data transaksi tidak berhasil ditemukan', 'Hapus Data Kunjungan');
										} else if (cst.success === true && cst.transaksi === true && cst.bayar === true)
										{
											ShowPesanWarning_viDataPasien('Anda telah melakukan pembayaran', 'Hapus Data Kunjungan');
										} else */
										if (cst.success === true){
											RefreshDataFilterKasirIgdKasir();
											ShowPesanInfoKasirrwj('Data transaksi pasien berhasil di hapus', 'Hapus Data Kunjungan');
										}/* else if (cst.success === false && cst.pesan === 0)
										{
											ShowPesanWarning_viDataPasien('Data kunjungan pasien tidak berhasil di hapus', 'Hapus Data Kunjungan');
										} */
										else{
											if (cst.kd_kasir) {
												ShowPesanWarningKasirigd('Anda tidak diberikan hak akses untuk menghapus data kunjungan '+cst.kd_kasir, 'Hapus Data Kunjungan');
											}else{
												ShowPesanErrorKasirigd('Data kunjungan pasien tidak berhasil di hapus', 'Hapus Data Kunjungan');
											}
										}
									}
								});
								loadMask.hide();
							} else {
								ShowPesanWarningKasirigd('Silahkan isi alasan terlebih dahulu!','Keterangan');

							}
						}

					});
					/* }else{
						//console.log("kd_kasir = "+vKdKasir+" - no_transaksi = "+vNoTransaksi);
						ShowPesanInfoKasirigd("Tidak dapat batal transaksi karena sudah ada produk atau tindakan", "Failed");
					} */
				},
				disabled:false
			},{
				text: ' Ganti Dokter [F6]',
				id:'btnLookUpGantiDokter_viKasirIGD',
				iconCls: 'gantidok',
				handler: function(){
				   GantiDokterLookUpKasirIGD();
				}
			},{								
				id: 'btnGantiKekompokPasienKasirIGD', text: 'Ganti kelompok pasien', tooltip: 'Ganti kelompok pasien', iconCls: 'gantipasien',// disabled:true,
				handler: function () {
					//Button Ganti Kelompok;
					panelActiveDataPasien = 0;
					KelompokPasienLookUp_igd();
					/* if(statusCekDetail == false){
					}else{
						//console.log("kd_kasir = "+vKdKasir+" - no_transaksi = "+vNoTransaksi);
					} */
					/* Ext.Ajax.request
						(
							{
								url: baseURL +  "index.php/rawat_jalan/functionCekDetailTransaksi/cek",   
								params: {
											NOTRANSAKSI  	: cellSelecteddeskripsi_datapasien.data.NO_TRANSAKSI,
											KDKASIR     	: cellSelecteddeskripsi_datapasien.data.KD_KASIR,
											TGLTRANSAKSI   	: cellSelecteddeskripsi_datapasien.data.TGL_MASUK
								},
								failure: function(o)
								{
									ShowPesanWarning_viDataPasien('Cek Item Tindakan gagal', 'Gagal');
								},  
								success: function(o) 
								{
									var cst = Ext.decode(o.responseText);
									if (cst.success === true) 
									{
										
									}else 
									{
										ShowPesanWarning_viDataPasien('Masih terdapat Item Tindakan', 'Gagal');
									};
								}
							}
						); */
				}
			},{								
				id: 'btnUnitPasienKasirIGD', text: 'Ganti Unit', tooltip: 'Ganti unit pasien', iconCls: 'gantipasien',// disabled:true,
				handler: function () {
					//Button Ganti Unit;
					panelActiveDataPasien = 2;
					//loaddatastoredokter();
					//console.log(statusCekDetail);
					KonsultasiDataPasienLookUp_igd();
					/* if(statusCekDetail == false){
					}else{
						//console.log("kd_kasir = "+vKdKasir+" - no_transaksi = "+vNoTransaksi);
						ShowPesanInfoKasirigd("Tidak dapat ganti unit karena sudah ada produk atau tindakan", "Failed");
					} */
				}
			},'-',{
				id: 'btnReloadList_pasienKasirIGD', 
				text: 'Refresh', 
				tooltip: 'Reload list pasien', 
				iconCls: 'refresh',// disabled:true,
				handler: function () {
					RefreshDataFilterKasirIgdKasir();
				}
			},
		]
	});
	var LegendViewCMRequest = new Ext.Panel({
		id: 'LegendViewCMRequest',
		region: 'center',
		style:'margin-top: -1px',
		bodyStyle: 'padding:4px;',
		layout: 'column',
		anchor: '100%',
		height: 32,
		autoScroll:false,
		items:[
			{
				columnWidth: .033,
				layout: 'form',
				style:{'margin-top':'-1px'},
				anchor: '100% 8.0001%',
				border: false,
				html: '<img src="'+baseURL+'ui/images/icons/16x16/hijau.png" class="text-desc-legend"/>'
			},{
				columnWidth: .08,
				layout: 'form',
				anchor: '100% 8.0001%',
				style:{'margin-top':'1px'},
				border: false,
				html: " Lunas"
			},{
				columnWidth: .033,
				layout: 'form',
				style:{'margin-top':'-1px'},
				border: false,
				anchor: '100% 8.0001%',
				html: '<img src="'+baseURL+'ui/images/icons/16x16/merah.png" class="text-desc-legend"/>'
			},{
				columnWidth: .1,
				layout: 'form',
				anchor: '100% 8.0001%',
				style:{'margin-top':'1px'},
				border: false,
				html: " Belum Lunas"
			},{
				columnWidth: .6,
				layout: 'form',
				style: {'float':'right'},
				border: false,
				items : [
					{
						xtype: 'textfield',
						fieldLabel: 'Total Bayar',
						style: {'padding-left':'10px'},
						id: 'txtJumlahBayar_KASIR_IGD',
						name: 'txtJumlahBayar_KASIR_IGD',
						readOnly: true,
					}
				]
			}
		]
	});
	var GDtabDetailIGD = new Ext.TabPanel({
		id:'GDtabDetailIGD',
		activeTab: 0,
		anchor: '100% 55%',
		style:'padding-top: 4px;',
		width: 300,
		items: [
			GetDTLTRIGDGrid(),
			GetDTLTRHistoryGridIGD()
			
		]
	});
    var FormDepanKasirigd = new Ext.Panel({
		id: mod_id,
		closable: true,
		region: 'center',
		layout: 'form',
		title: 'Kasir Gawat Darurat',
		border: false,
		autoScroll:true,
		bodyStyle:'padding: 4px;',
		items: [
			{
				xtype:'panel',
				border:false,
				activeTab: 0,
				height:66,
				defaults:{
					autoScroll: false
				},
				items:[
					{
						layout: 'form',
						bodyStyle:'padding: 4px;',
						items:[
							{
								layout: 'column',
								border: false ,
								items:[
									{
										layout: 'form',
										columnWidth:.5,
										border: false ,
										items:[
											{
												xtype: 'textfield',
												fieldLabel: ' No. Medrec' + ' ',
												id: 'txtFilterNomedrec',
												anchor : '100%',
												onInit: function() { },
												listeners:{
													'specialkey' : function(){
														var tmpNoMedrec = Ext.get('txtFilterNomedrec').getValue()
														if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10 ){
															if(tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10 ){
																var tmpgetNoMedrec = formatnomedrec(Ext.get('txtFilterNomedrec').getValue())
																Ext.getCmp('txtFilterNomedrec').setValue(tmpgetNoMedrec);
																var tmpkriteria = getCriteriaFilter_viDaftar();
																RefreshDataFilterKasirIgdKasir();
																// if(dsTRKasirigdKasirList.getCount() == 0){
																// 	ShowPesanInfoKasirigd('Pasien tidak ditemukan','Information');
																// }
															}else{
																if (tmpNoMedrec.length === 10){
																	   // tmpkriteria = getCriteriaFilter_viDaftar();
																	RefreshDataFilterKasirIgdKasir();
																	// if(dsTRKasirigdKasirList.getCount() == 0){
																	// 	ShowPesanInfoKasirigd('Pasien tidak ditemukan','Information');
																	// }
																}else
																	Ext.getCmp('txtFilterNomedrec').setValue('')
															}
														}
													}

												}
											},{
												xtype: 'textfield',
												fieldLabel: ' Pasien' + ' ',
												id: 'TxtFilterGridDataView_NAMA_viKasirigdKasir',
												anchor :'100%',
												enableKeyEvents: true,
												listeners:{ 
													'specialkey' : function(){
														if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9){
															RefreshDataFilterKasirIgdKasir();
															// if(dsTRKasirigdKasirList.getCount() == 0){
															// 	ShowPesanInfoKasirigd('Pasien tidak ditemukan','Information');
															// }
														}
													}
												}
											}
										]
									},{
										layout: 'form',
										columnWidth:.5,
										style:'padding-left: 4px;',
										border: false ,
										items:[
											getItemPaneltgl_filter(),
											getItemPanelcombofilter()
										]
									}
								]
							}
						]
					}
				]
			},
			grListTRKasirigd,GDtabDetailIGD,LegendViewCMRequest
		]
	});
	return FormDepanKasirigd
}
function fnDlgIGDBukaTransaksiPasswordDulu()
{
    winIGDBukaTransaksiPasswordDulu = new Ext.Window
    (
        {
            id: 'winIGDBukaTransaksiPasswordDulu',
            title: 'Password',
            closeAction: 'destroy',
            width:320,
            height: 120,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgIGDBukaTransaksiPasswordDulu()]

        }
    );

    winIGDBukaTransaksiPasswordDulu.show();
};
function ItemDlgIGDBukaTransaksiPasswordDulu()
{
    var PnlLapIGDSBukaTransaksiPasswordDulu = new Ext.Panel
    (
        {
            id: 'PnlLapIGDSTransaksiPasswordDulu',
            fileUpload: true,
            layout: 'form',
            width:170,
            height: 120,
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapIGDBukaTransaksiPasswordDulu_Tanggal(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                   // style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '100%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                         {
                            xtype: 'button',
                            text: 'Ok',
                            width: 70,
                            hideLabel: true,
							style:{'margin-left':'5px','margin-top':'0px'},
                            id: 'btnOkLapIGDBukaTransaksiPasswordDulu',
                            handler: function()
                            {
                               Ext.Ajax.request
								 (
									{
										//url: "./Datapool.mvc/CreateDataObj",
										url: baseURL + "index.php/main/functionIGD/cekpasswordbukatrans_igd",
										params: {passDulu:Ext.getCmp('TxBukaTransaksiPasswordDuluIGD').getValue()},
										failure: function(o)
										{
											ShowPesanWarningKasirigd('Proses Gagal segera Hubungi Admin', 'Gagal');
										//RefreshDataKasirRwiDetail(notransaksi);
										},
										success: function(o)
										{
											//RefreshDataKasirRwiDetail(notransaksi);
											var cst = Ext.decode(o.responseText);
											if (cst.success === true)
											{
												winIGDBukaTransaksiPasswordDulu.close();
													var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan Pembukaan Transaksi:', function(btn, combo){
													if (btn == 'ok')
																{
																variablebukahistori_igd=combo;
																				if (combo!='')
																				{
																				 UpdateBukatransaksi();
																				}
																				else
																				{
																					ShowPesanWarningKasirigd('Silahkan isi alasan terlebih dahulu','Keterangan');

																				}
																}

														}); 
											}
											else
											{
													ShowPesanWarningKasirigd('Password salah segera Hubungi Admin', 'Gagal');
											};
										}
									}
								)
							   /* */

								    
                            }
                        },
                        {
                            xtype: 'button',
                            text: 'Cancel' ,
                            width: 70,
                            hideLabel: true,
							style:{'margin-left':'5px','margin-top':'0px'},
                            id: 'btnCancelLapIGDBukaTransaksiPasswordDulu',
                            handler: function()
                            {
                                    winIGDBukaTransaksiPasswordDulu.close();
                            }
                        } 
                    ]
                }
            ]
        }
    );

    return PnlLapIGDSBukaTransaksiPasswordDulu;
};
function getItemLapIGDBukaTransaksiPasswordDulu_Tanggal()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
		
		
		{
			    columnWidth: .90,
			    layout: 'form',
				 labelWidth:120,
				  border: false,
				 
			    items:
				[
					{
						xtype: 'textfield',
						inputType: 'password',
						fieldLabel: 'Masukan Password ',
						id: 'TxBukaTransaksiPasswordDuluIGD',
					    anchor: '99%'
					},
				
					
				]
			},
     
        ]
    }
    return items;
};

function fnDlgIGDPasswordDulu()
{
    winIGDPasswordDulu = new Ext.Window
    (
        {
            id: 'winIGDPasswordDulu',
            title: 'Password',
            closeAction: 'destroy',
            width:320,
            height: 120,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgIGDPasswordDulu()]

        }
    );

    winIGDPasswordDulu.show();
};
function ItemDlgIGDPasswordDulu()
{
    var PnlLapIGDSPasswordDulu = new Ext.Panel
    (
        {
            id: 'PnlLapIGDSPasswordDulu',
            fileUpload: true,
            layout: 'form',
            width:170,
            height: 120,
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapIGDPasswordDulu_Tanggal(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                   // style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '100%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                         {
                            xtype: 'button',
                            text: 'Ok',
                            width: 70,
                            hideLabel: true,
							style:{'margin-left':'5px','margin-top':'0px'},
                            id: 'btnOkLapIGDPasswordDulu',
                            handler: function()
                            {
                               Ext.Ajax.request
								 (
									{
										//url: "./Datapool.mvc/CreateDataObj",
										url: baseURL + "index.php/main/functionIGD/cekpassword_igd",
										params: {passDulu:Ext.getCmp('TxPasswordDuluIGD').getValue()},
										failure: function(o)
										{
											ShowPesanWarningKasirigd('Proses Gagal segera Hubungi Admin', 'Gagal');
										//RefreshDataKasirRwiDetail(notransaksi);
										},
										success: function(o)
										{
											//RefreshDataKasirRwiDetail(notransaksi);
											var cst = Ext.decode(o.responseText);
											if (cst.success === true)
											{
												winIGDPasswordDulu.close();
													var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan Pembatalan Transaksi:', function(btn, combo){
													if (btn == 'ok')
																{
																variablebatalhistori_igd=combo;
																				if (variablehistori!='')
																				{
																				 BatalTransaksi(false);
																				}
																				else
																				{
																					ShowPesanWarningKasirigd('Silahkan isi alasan terlebih dahulu','Keterangan');

																				}
																}

														}); 
											}
											else
											{
													ShowPesanWarningKasirigd('Password salah segera Hubungi Admin', 'Gagal');
											};
										}
									}
								)
							   /* */

								    
                            }
                        },
                        {
                            xtype: 'button',
                            text: 'Cancel' ,
                            width: 70,
                            hideLabel: true,
							style:{'margin-left':'5px','margin-top':'0px'},
                            id: 'btnCancelLapIGDPasswordDulu',
                            handler: function()
                            {
                                    winIGDPasswordDulu.close();
                            }
                        } 
                    ]
                }
            ]
        }
    );

    return PnlLapIGDSPasswordDulu;
};
function getItemLapIGDPasswordDulu_Tanggal()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
		
		
		{
			    columnWidth: .90,
			    layout: 'form',
				 labelWidth:120,
				  border: false,
				 
			    items:
				[
					{
						xtype: 'textfield',
						inputType: 'password',
						fieldLabel: 'Masukan Password ',
						id: 'TxPasswordDuluIGD',
					    anchor: '99%'
					},
				
					
				]
			},
     
        ]
    }
    return items;
};
function TransferLookUp(rowdata) 
{
    var lebar = 440;
    FormLookUpsdetailTRTransfer_IGD = new Ext.Window
    (
        {
            id: 'gridTransfer',
            title: 'Transfer Rawat Inap',
            closeAction: 'destroy',
            width: lebar,
            height: 420,
            border: false,
            resizable: false,
            plain: false,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryTRTransfer(lebar),
			fbar:[
				{
					xtype:'button',
					text:'Simpan',
					width:70,
					style:{'margin-left':'0px','margin-top':'0px'},
					hideLabel:true,
					id: 'btnOkTransfer',
					handler:function()
					{
				
						TransferData_IGD(false);
						
					}
				},
				{
						xtype:'button',
						text:'Tutup',
						width:70,
						hideLabel:true,
						id: 'btnCancelTransfer',
						handler:function() 
						{
							FormLookUpsdetailTRTransfer_IGD.close();
						}
				}
			],
            listeners:
            {
                
            }
        }
    );

    FormLookUpsdetailTRTransfer_IGD.show();
  //  Transferbaru();

};
function getFormEntryTRTransfer(lebar) 
{
    var pnlTRTransfer = new Ext.FormPanel
    (
        {
            id: 'PanelTRTransfer',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:420,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [getItemPanelInputTransfer(lebar)],
			tbar:
            [
               
               
            ]
        }
    );
 
    var FormDepanTransfer = new Ext.Panel
	(
		{
		    id: 'FormDepanTransfer',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [pnlTRTransfer	
				
			]

		}
	);

    return FormDepanTransfer
};

function getItemPanelInputTransfer(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    // bodyStyle: 'padding:10px 10px 10px 0px',
		border:false,
		height:330,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
			    layout: 'form',
				height:330,
			    border: false,
			    items:
				[
					getTransfertujuan(lebar),
					{	 
						xtype: 'tbspacer',
						height: 5
					},	
					getItemPanelNoTransksiTransfer(lebar),
					{	 
						xtype: 'tbspacer',									
						height:3
					},
					mComboalasan_transfer()
				]
			},
		]
	};
    return items;
};
function getTransfertujuan(lebar) 
{
    var items =
	{
		Width		: lebar-2,
		height		: 160,
		layout		: 'form',
		border		: true,
		labelWidth	: 130,		
	    items:
		[
			{	 
				xtype: 'tbspacer',
				height: 2
			},
			{
				xtype 		: 'textfield',
				fieldLabel 	: 'No Transaksi',
				name 		: 'txtTranfernoTransaksiKasirIGD',
				id 			: 'txtTranfernoTransaksiKasirIGD',
				labelWidth 	: 130,
				width 		: 100,
				anchor 		: '95%'
			},
			{
				xtype: 'datefield',
				fieldLabel: 'Tanggal Trans',
				id: 'dtpTanggalTransfertransaksiKasirIGD',
				name: 'dtpTanggalTransfertransaksiKasirIGD',
				format: 'd/M/Y',
				readOnly : true,
				labelWidth:130,
				width: 100,
				anchor: '95%'
			},{
				xtype: 'textfield',
				fieldLabel: 'No Medrec',
				//maxLength: 200,
				name: 'txtTranfernomedrecKasirIGD',
				id: 'txtTranfernomedrecKasirIGD',
				labelWidth:130,
				width: 100,
				anchor: '95%'
			},
			{
				xtype: 'textfield',
				fieldLabel: 'Nama Pasien',
				//maxLength: 200,
				name: 'txtTranfernamapasienKasirIGD',
				id: 'txtTranfernamapasienKasirIGD',
				labelWidth:130,
				width: 100,
				anchor: '95%'
			},{
				xtype: 'textfield',
				fieldLabel: 'Unit Perawatan',
				//maxLength: 200,
				name: 'txtTranferunitKasirIGD',
				id: 'txtTranferunitKasirIGD',
				labelWidth:130,
				width: 100,
				hidden : true,
				anchor: '95%'
			},{
				xtype: 'textfield',
				fieldLabel: 'Unit Perawatan',
				//maxLength: 200,
				name: 'txtTranferkelaskamarigd',
				id: 'txtTranferkelaskamarigd',
				readOnly : true,
				labelWidth:130,
				width: 100,
				anchor: '95%'
			},
			{
				xtype: 'datefield',
				fieldLabel: 'Tanggal Transfer',
				id: 'dtpTanggalTransfertransaksibayarKasirIGD',
				name: 'dtpTanggalTransfertransaksibayarKasirIGD',
				format: 'd/M/Y',
				readOnly : false,
				labelWidth:130,
				width: 100,
				anchor: '95%'
			},
		
					
					
				//]
			//}
			
		]
	}
    return items;
};

function getItemPanelNoTransksiTransfer(lebar) 
{
    var items =
	{
		Width:lebar,
		height:110,
	    layout: 'column',
	    border: true,
		
		
	    items:
		[
			{
				columnWidth	: .990,
				layout		: 'form',
				Width		: lebar-10,
				labelWidth	: 130,
				border		: false,
				items		:
				[
					{	 
						xtype	: 'tbspacer',
						height	: 3
					},{
						xtype		: 'textfield',
						fieldLabel	: 'Jumlah Biaya',
						maxLength	: 200,
						style		: {'text-align':'right'},
						name		: 'txtjumlahbiayasal_KasirIGD',
						id			: 'txtjumlahbiayasal_KasirIGD',
						width		: 100,
						readOnly 	: true,
						anchor		: '95%'
					},{
						xtype 		: 'textfield',
						fieldLabel 	: 'Paid',
						maxLength 	: 200,
						style 		: {'text-align':'right'},
						name 		: 'txtpaid_KasirIGD',
						id 			: 'txtpaid_KasirIGD',
						width 		: 100,
						value 		: 0,
						readOnly 	: true,
						anchor 		: '95%'
					},{
						xtype 		: 'textfield',
						fieldLabel 	: 'Jumlah dipindahkan',
						maxLength 	: 200,
						style 		: {'text-align':'right'},
						name 		: 'txtjumlahtranfer_KasirIGD',
						id 			: 'txtjumlahtranfer_KasirIGD',
						readOnly 	: false,
						enableKeyEvents:true,
						width 		: 100,
						anchor 		: '95%',
						listeners:
						{ 
							keyup:function(text,e)
							{
								getsaldotagihan_KasirIGD();
							}
						}
					},{
						xtype 		: 'textfield',
						fieldLabel 	: 'Saldo tagihan',
						maxLength 	: 200,
						style 		: {'text-align':'right'},
						name 		: 'txtsaldotagihan_KasirIGD',
						id 			: 'txtsaldotagihan_KasirIGD',
						value 		: 0,
						readOnly 	: true,
						width 		: 100,
						anchor 		: '95%'
					}
				]
			}
			
		]
	}
    return items;
};


function RefreshDataKasirIGDDetail(no_transaksi) 
{
	loadMask.show();
	var strKriteriaIGD='';
	strKriteriaIGD = "no_transaksi= ~" + no_transaksi + "~ and kd_kasir=~06~";
	dsTRDetailKasirIGDList.load
	(
		{
			params:
			{
				Skip	: 0,
				Take	: 1000,
				Sort	: 'tgl_transaksi',
				Sortdir	: 'ASC',
				target	: 'ViewDetailTRRWJ',
				param	: strKriteriaIGD
			},	
			callback:function(){
				loadMask.hide();
			}	
		}
	);
	return dsTRDetailKasirIGDList;
};
//UPDATE ISAL TGL 19-07-2019//
function cekTransferApotek(no_transaksi){
	Ext.Ajax.request({
		url: baseURL + "index.php/rawat_jalan/functionRWJ/cek_apotek_transfer",
		params: {
			kd_kasir 	: '06',
			no_transaksi: no_transaksi,
		},
		failure: function (o){

		},
		success: function (o){
			var cst = Ext.decode(o.responseText);
			if(cst.success === true){
				gridDTLTRIGD.getView().refresh();
			}
		}
	});
}
//END OF UPDATE ISAL //
function getParamDataDeleteKasirIGDDetail()
{

    var params =
    {
		Table 			: 'ViewTrKasirIGD',
		TrKodeTranskasi : notransaksi,
		TrTglTransaksi 	: CurrentKasirIGD.data.data.TGL_TRANSAKSI,
		TrKdPasien 		: CurrentKasirIGD.data.data.KD_PASIEN,
		kodePasien 		: CurrentKD_PASIENIGD,
		TrKdNamaPasien 	: namapasien,	
		TrKdUnit 		: kodeunit,
		TrNamaUnit 		: namaunit,
		Uraian 			: CurrentKasirIGD.data.data.DESKRIPSI2,
		AlasanHapus 	: variablehistori,
		TrHarga 		: CurrentKasirIGD.data.data.HARGA,
		TrKdProduk 		: CurrentKasirIGD.data.data.KD_PRODUK,
		RowReq 			: CurrentKasirIGD.data.data.URUT,
		Hapus 			: 2
    };
	
    return params
};

function GetDTLTRIGDGrid() {
    var fldDetail = ['KP_PRODUK','KD_PRODUK','DESKRIPSI','DESKRIPSI2','KD_TARIF','HARGA','QTY','DESC_REQ','TGL_BERLAKU','NO_TRANSAKSI','URUT',
		'DESC_STATUS','TGL_TRANSAKSI','STATUS_KONSULTASI','STATUS_PENINDAK','JUMLAH_DOKTER','KD_UNIT','TMP_TGL_TRANSAKSI','FOLIO','KD_KLAS','MANUAL'];
    dsTRDetailKasirIGDList = new WebApp.DataStore({ fields: fldDetail })
	// RefreshDataKasirIGDDetail() ;
    gridDTLTRIGD = new Ext.grid.EditorGridPanel({
		title		: 'Input Produk',
		stripeRows	: true,
		store		: dsTRDetailKasirIGDList,
		border		: false,
		id:'KasirIGD_gridTindakan',
		columnLines	: true,
		frame		: false,
		height      :'50',
		//anchor		: '100%',
		//anchor: '70%',
		autoScroll	: false,
		sm: new Ext.grid.CellSelectionModel({
			singleSelect: true,
			listeners:{
				cellselect: function(sm, row, rec){
					dataRowIndexDetail = gridDTLTRIGD.getSelectionModel().selection.cell[0];
					// console.log(dsTRDetailKasirIGDList.getAt(row));
					cellSelecteddeskripsi_panatajasaigd=dsTRDetailKasirIGDList.getAt(row);
					cellSelecteddeskripsi                         = dsTRDetailKasirIGDList.getAt(row);
					tmp_folio                                     = cellSelecteddeskripsi.data.FOLIO;
					CurrentKasirIGD.row                           = row;
					CurrentKasirIGD.data                          = cellSelecteddeskripsi;
					CurrentRowPemeriksaanIGD                      = row;
					// FocusCtrlCMIGD                             ='txtAset';
					// getProdukIGD()
					currentJasaDokterUrutDetailTransaksi_KasirIGD = cellSelecteddeskripsi.data.URUT;
					currentJasaDokterKdProduk_KasirIGD            =  cellSelecteddeskripsi.data.KD_PRODUK;
					currentJasaDokterKdTarif_KasirIGD             = cellSelecteddeskripsi.data.KD_TARIF;
					currentJasaDokterHargaJP_KasirIGD             = cellSelecteddeskripsi.data.HARGA;
					currentTOTAL_KasirIGD                         = parseInt(cellSelecteddeskripsi.data.QTY) * parseInt(cellSelecteddeskripsi.data.HARGA);
					
					
				}
			}
		}),
		cm: TRGawatDaruratColumModel(),
		viewConfig: {forceFit: true},
		tbar:[
			{
				id:'btnTambahBrsKasirIGD',
				text: 'Tambah baris',
				tooltip: nmTambahBaris,
				iconCls: 'AddRow',
				disabled :true,
				handler: function(){
					TambahBarisIGD();
				}
			},{
				text: 'Tambah Produk',
				id: 'btnLookupIGD',
				tooltip: nmLookup,
				iconCls: 'find',
				hidden : true,
				disabled :true,
				handler: function(){
					var p = RecordBaruRWJ();
					var str='';
					//if (Ext.get('txtKdUnitIGD').dom.value  != undefined && Ext.get('txtKdUnitIGD').dom.value  != '')
					//{
							//str = ' where kd_dokter =~' + Ext.get('txtKdDokter').dom.value  + '~';
							//str = "\"kd_dokter\" = ~" + Ext.get('txtKdDokter').dom.value  + "~";
							str =  kodeunit;
					//};
					FormLookupKasirRWJ(str, dsTRDetailKasirIGDList, p, true, '', true);
				}
			},{
				text: 'Simpan',
				id: 'btnSimpanIGD',
				tooltip: nmSimpan,
				iconCls: 'save',
				hidden:true,
				disabled :true,
				handler: function(){	
					Datasave_KasirIGD(false,false);
				}
			},{
				id:'btnHpsBrsIGD',
				text: 'Hapus Baris',
				tooltip: 'Hapus Baris',
				disabled :true,
				iconCls: 'RemoveRow',
				handler: function(){
					// console.log(cellSelecteddeskripsi);
					if (dsTRDetailKasirIGDList.getCount() > 0 ){
						if(cellSelecteddeskripsi.data.KD_KLAS == "9" || cellSelecteddeskripsi.data.MANUAL == "t" ){
							ShowPesanWarningKasirigd('Produk transfer tidak dapat dihapus','Hapus data');
						}else if (cellSelecteddeskripsi != undefined){
							if(CurrentKasirIGD != undefined)
							{
								HapusBarisIGD();
							}
						}else{
							ShowPesanWarningKasirigd('Pilih record ','Hapus data');
						}
					}
				}
			},'-',{
				id		:'btnLookUpEditDokterPenindak_KasirIGD',
				text	: 'Edit Pelaksana',
				iconCls	: 'Edit_Tr',
				//hidden	: true,
				handler	: function(){
					if(cellSelecteddeskripsi == '' || cellSelecteddeskripsi == undefined){
						ShowPesanWarningKasirigd('Pilih item dokter penindak yang akan diedit!','Error');
					} else{
						if(currentJasaDokterUrutDetailTransaksi_KasirIGD == 0 || currentJasaDokterUrutDetailTransaksi_KasirIGD == undefined){
							ShowPesanErrorKasirigd('Item ini belum ada dokter penindaknya!','Error');
						} else{
							// PilihDokterLookUp_KasirIGD(true);
							loaddatastoredokterVisite_REVISI();
							PilihDokterLookUpPJ_IGD_REVISI();
						}
					}
				}
			},'-',{
				id		:'btnLookUpProduk_KasirIGD',
				text	: 'Look Up Produk',
				iconCls	: 'Edit_Tr',
				handler	: function(){
					if(rowSelectedKasirigdKasir == '' || rowSelectedKasirigdKasir == undefined){
						ShowPesanWarningKasirigd('Pilih pasien terlebih dahulu !','Error');
					} else{
						formLookUpProduk_KasirIGD(rowSelectedKasirigdKasir.data.KD_UNIT , rowSelectedKasirigdKasir.data.NAMA_UNIT);
					}
				}
			},'-',{
				id		:'btnCetakRekapitulasi_KasirIGD',
				text	: 'Cetak',
				iconCls	: 'print',
				arrowAlign:'right',
				menu: [
					{
						text: 'Rekapitulasi',
						id: 'btnLookUpRekapitulasi_KasirIGD',
						iconCls: 'print',
						handler	: function(){
							var url = baseURL + "index.php/laporan/lap_rekapitulasi/cetak";
							new Ext.Window({
								title: 'Preview',
								width: 1000,
								height: 600,
								constrain: true,
								modal: true,
								html: "<iframe  style ='width: 100%; height: 100%;' src='" + url+"/"+rowSelectedKasirigdKasir.data.NO_TRANSAKSI+"/"+rowSelectedKasirigdKasir.data.KD_KASIR+"/true/3'></iframe>",
								tbar : [
									{
										xtype   : 'button',
										text    : 'Cetak Direct',
										iconCls : 'print',
										handler : function(){
											window.open(url+"/"+rowSelectedKasirigdKasir.data.NO_TRANSAKSI+"/"+rowSelectedKasirigdKasir.data.KD_KASIR+"/false/3", '_blank');
										}
									}
								]
							}).show();
						}
					},{
						text: 'Rekapitulasi Apotek',
						id: 'btnLookUpRekapitulasiApotek_KasirIGD',
						iconCls: 'print',
						handler	: function(){
							var url = baseURL + "index.php/laporan/lap_rekapitulasi/cetak_rekap_apotek";
							new Ext.Window({
								title: 'Preview',
								width: 1000,
								height: 600,
								constrain: true,
								modal: true,
								html: "<iframe  style ='width: 100%; height: 100%;' src='" + url+"/"+rowSelectedKasirigdKasir.data.KD_PASIEN+"/"+rowSelectedKasirigdKasir.data.NO_TRANSAKSI+"/"+rowSelectedKasirigdKasir.data.TANGGAL_TRANSAKSI+"/true/3'></iframe>",
								tbar : [
									{
										xtype   : 'button',
										text    : 'Cetak Direct',
										iconCls : 'print',
										handler : function(){
											window.open(url+"/"+rowSelectedKasirigdKasir.data.KD_PASIEN+"/"+rowSelectedKasirigdKasir.data.NO_TRANSAKSI+"/"+rowSelectedKasirigdKasir.data.TANGGAL_TRANSAKSI+"/false/3", '_blank');
										}
									}
								]
							}).show();
						}
					},
				]
			},
		]
	});
    return gridDTLTRIGD;
}

function TambahBarisIGD()
{
    var x=true;

    if (x === true)
    {
        var p = RecordBaruRWJ();
        dsTRDetailKasirIGDList.insert(dsTRDetailKasirIGDList.getCount(), p);
    };
};

function Datasave_KasirIGD(mBol,jasa,alert_simpan,tambah_baris,refresh_filter) {	
	Ext.Ajax.request({
		url: baseURL + "index.php/main/functionIGD/savedetailproduk",
		params: getParamDetailTransaksiIGD(),
		failure: function(o){
			ShowPesanWarningKasirigd('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
			if(mBol === false){
				RefreshDataKasirIGDDetail(notransaksi);
			}
		},	
		success: function(o){
			//RefreshDataKasirIGDDetail(notransaksi);
			var cst = Ext.decode(o.responseText);
			if (cst.success === true){
				if(alert_simpan==undefined || alert_simpan==true){
					ShowPesanInfoKasirigd('Simpan tindakan berhasil','Information');
				}
				//Datasave_KasirIGD_SQL();
				if(tambah_baris==undefined || tambah_baris==true){
					TambahBarisIGD();
				}
				if(jasa == true){
					SimpanJasaDokterPenindak_KasirIGD(currentJasaDokterKdProduk_KasirIGD,currentJasaDokterKdTarif_KasirIGD,currentJasaDokterUrutDetailTransaksi_KasirIGD,currentJasaDokterHargaJP_KasirIGD);
				} 
				if(refresh_filter==undefined || refresh_filter==true){
					RefreshDataFilterKasirIgdKasir();
				}
				if(mBol === false){
					RefreshDataKasirIGDDetail(notransaksi);
				}
			}else{
				ShowPesanWarningKasirigd('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
			}
		}
	})
}



function TransferData_IGD(mBol) 
{	
	Ext.Ajax.request
	 (
		{
			url: baseURL + "index.php/main/functionIGD/saveTransfer",
			params: getParamTransferRwi(),
			failure: function(o)
			{
				ShowPesanWarningKasirigd('Error transfer pembayaran. Hubungi Admin!', 'Gagal');
				RefreshDataKasirIGDDetail(notransaksi);
			},	
			success: function(o) 
			{
				RefreshDataKasirIGDDetail(notransaksi);
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					ShowPesanInfoKasirigd('Transfer berhasil','Information');
					Ext.getCmp('btnTransferKasirigd').disable();
					//Ext.getCmp('btnSimpanKasirigd').disable();
					RefreshDataKasirigdKasirDetail(Ext.get('txtNoTransaksiKasirigdKasir').dom.value);
					RefreshDataFilterKasirIgdKasir();
					FormLookUpsdetailTRTransfer_IGD.close();
					if(mBol === false)
					{
						RefreshDataKasirIGDDetail(notransaksi);
					};
				}
				else 
				{
					ShowPesanWarningKasirigd('Error '+cst.pesan+'. Gagal melakukan transfer!', 'Gagal');
				};
			}
		}
	)
};



function RecordBaruRWJ()
{

	var p = new mRecordIGD
	(
		{
			'DESKRIPSI2':'',
			'KD_PRODUK':'',
		    'DESKRIPSI':'', 
		    'KD_TARIF':'', 
		    'HARGA':'',
		    'QTY':'',
		    'TGL_TRANSAKSI':nowTglTransaksiIGD3, 
		    'DESC_REQ':'',
		    'KD_TARIF':'',
		    'URUT':''
		}
	);
	
	return p;
};

function getParamDetailTransaksiIGD() 
{
    var params =
	{
		Table:'ViewTrKasirIGD',

		TrKodeTranskasi: notransaksi,
		KdUnit: kodeunit,
		//DeptId:Ext.get('txtKdDokter').dom.value ,tampungshiftsekarang
		Tgl: tgltrans,
		kdDokter:CurrentKDDokerIGD,
		urut_masuk:CurrentURUT_MASUKIGD,
		kd_pasien:CurrentKD_PASIENIGD,
		Shift: tampungshiftsekarang,
		List:getArr2DetailTrIGD(),
		//JP:dsTRDetailKasirIGDList.data.items[CurrentRowPemeriksaanIGD].data.HARGA,
		JmlField: mRecordIGD.prototype.fields.length-4,
		JmlList:GetListCoun2tDetailTransaksi(),
		Hapus:1,
		Ubah:0
	};
    return params
};

function getParamTransferRwi() 
{
    var params =
	{
        KDkasirIGD:kodekasir,
		TrKodeTranskasi: notransaksi,
		KdUnit: kodeunit,
		Kdpay: 'T1',
		// Jumlahtotal: Ext.get(txtjumlahbiayasal_KasirIGD).dom.value,
		Jumlahtotal: Ext.get(txtjumlahtranfer_KasirIGD).dom.value,
		Tglasal:  ShowDate(tgltrans),
		Shift: tampungshiftsekarang,
		TRKdTransTujuan:Ext.get(txtTranfernoTransaksiKasirIGD).dom.value,
		KdpasienIGDtujuan: Ext.get(txtTranfernomedrecKasirIGD).dom.value,
		TglTranasksitujuan : Ext.get(dtpTanggalTransfertransaksiKasirIGD).dom.value,
		KDunittujuan : Trkdunit2,
		KDalasan :Ext.get(cboalasan_transfer).dom.value,
		KasirRWI:'05',
		Kdcustomer:kdcustomeraa,
		kodeunitkamar:tmp_kodeunitkamarigd,
		kdspesial:tmp_kdspesialigd,
		nokamar:tmp_nokamarigd,
		tgltransfer:convertTglTransaksiKasirIGD(Ext.getCmp('dtpTanggalTransfertransaksibayarKasirIGD').getValue()),
		
	
	};
	
	params['jumlah'] = dsTRDetailKasirigdKasirList.getCount();
	for(var i = 0 ; i < dsTRDetailKasirigdKasirList.getCount();i++)
	{
		if(dsTRDetailKasirigdKasirList.data.items[i].data.BAYARTR != 0 || dsTRDetailKasirigdKasirList.data.items[i].data.BAYARTR!= undefined){
			params['harga-'+i]=dsTRDetailKasirigdKasirList.data.items[i].data.BAYARTR;
			params['urut-'+i]=dsTRDetailKasirigdKasirList.data.items[i].data.URUT;
			params['tgl_transaksi-'+i]=dsTRDetailKasirigdKasirList.data.items[i].data.TGL_TRANSAKSI;
		} else if(dsTRDetailKasirigdKasirList.data.items[i].data.BAYARTR != 0 || dsTRDetailKasirigdKasirList.data.items[i].data.BAYARTR!= undefined){
			params['harga-'+i]=dsTRDetailKasirigdKasirList.data.items[i].data.PIUTANG;
			params['urut-'+i]=dsTRDetailKasirigdKasirList.data.items[i].data.URUT;
			params['tgl_transaksi-'+i]=dsTRDetailKasirigdKasirList.data.items[i].data.TGL_TRANSAKSI;
		} else{
			params['harga-'+i]=dsTRDetailKasirigdKasirList.data.items[i].data.DISCOUNT;
			params['urut-'+i]=dsTRDetailKasirigdKasirList.data.items[i].data.URUT;
			params['tgl_transaksi-'+i]=dsTRDetailKasirigdKasirList.data.items[i].data.TGL_TRANSAKSI;
		}
	}
    return params
};
function getArr2DetailTrIGD()
{
	var x='';
	console.log(dsTRDetailKasirIGDList.data);
	for(var i = 0 ; i < dsTRDetailKasirIGDList.getCount();i++)
	{
		if (dsTRDetailKasirIGDList.data.items[i].data.KD_PRODUK != '' && dsTRDetailKasirIGDList.data.items[i].data.DESKRIPSI != '')
		{
			var y='';
			var z='@@##$$@@';
			console.log(dsTRDetailKasirIGDList.data.items[i].data.KD_PRODUK);
			y = 'URUT=' + dsTRDetailKasirIGDList.data.items[i].data.URUT
			y += z + dsTRDetailKasirIGDList.data.items[i].data.KD_PRODUK
			y += z + dsTRDetailKasirIGDList.data.items[i].data.QTY
			y += z + ShowDate(dsTRDetailKasirIGDList.data.items[i].data.TGL_BERLAKU)
			y += z +dsTRDetailKasirIGDList.data.items[i].data.HARGA
			y += z +dsTRDetailKasirIGDList.data.items[i].data.KD_TARIF
			y += z +dsTRDetailKasirIGDList.data.items[i].data.URUT
			y += z + ShowDate(dsTRDetailKasirIGDList.data.items[i].data.TGL_TRANSAKSI);
			y += z + dsTRDetailKasirIGDList.data.items[i].data.STATUS_KONSULTASI;
			y += z + dsTRDetailKasirIGDList.data.items[i].data.KD_UNIT;

			
			if (i === (dsTRDetailKasirIGDList.getCount()-1))
			{
				x += y 
			}
			else
			{
				x += y + '##[[]]##'
			};
		};
	}	
	
	return x;
};

function GetListCoun2tDetailTransaksi()
{
	
	var x=0;
	for(var i = 0 ; i < dsTRDetailKasirIGDList.getCount();i++)
	{
		if (dsTRDetailKasirIGDList.data.items[i].data.KD_PRODUK != '' || dsTRDetailKasirIGDList.data.items[i].data.DESKRIPSI  != '')
		{
			x += 1;
		};
	}
	return x;
	
};
function senderProdukIGD(no_transaksi, kd_produk, kd_tarif, kd_kasir, urut, unit, tgl_transaksi, tgl_berlaku, quantity, harga, fungsi,tmp_tgl_transaksi){
    Ext.Ajax.request({
        url     : baseURL + "index.php/main/functionIGD/insertDataProdukIGD",
        params  :  {
            no_transaksi    	: no_transaksi,
            kd_produk       	: kd_produk,
            kd_tarif        	: kd_tarif,
            kd_kasir        	: kd_kasir,
            urut            	: parseInt(urut)+1,
            unit            	: unit,
            tgl_transaksi   	: tgl_transaksi,
            tgl_berlaku     	: tgl_berlaku,
            quantity        	: quantity,
            harga           	: harga,
            fungsi          	: fungsi,
			kd_dokter			: CurrentKDDokerIGD,
			tmp_tgl_transaksi	: tmp_tgl_transaksi
        },
        failure : function(o)
        {

        },
        success : function(o)
        {
            //tmpRowGrid = PenataJasaKasirRWI.form.Grid.produk.getSelectionModel().selection.cell[0];
            var cst = Ext.decode(o.responseText);
			tmp_notransaksi = no_transaksi;
			tmp_urut        = parseInt(urut)+1;
			tmp_kdunit      = unit;
			tmp_tgltransaksi= tgl_transaksi;
			tmp_tarif 		= kd_tarif;
			tmp_tglberlaku	= tgl_berlaku;
			var lineIGD = gridDTLTRIGD.getSelectionModel().selection.cell[0];
			console.log(dsTRDetailKasirIGDList.getRange()[lineIGD]);
			dsTRDetailKasirIGDList.getRange()[lineIGD].set('URUT', cst.urut);
            Ext.getCmp('txtJumlahBayar_KASIR_IGD').setValue(parseInt(Ext.getCmp('txtJumlahBayar_KASIR_IGD').getValue()) + harga * quantity);
            //console.log(cst.action);
           /* if (cst.action == 'insert') {
                PenataJasaKasirRWI.dsGridTindakan.insert(PenataJasaKasirRWI.dsGridTindakan.getCount(), PenataJasaKasirRWI.func.getNullProduk());
            }else{
                PenataJasaKasirRWI.form.Grid.produk.startEditing(tmpRowGrid+1, 3);
            }*/

            //PenataJasaKasirRWI.dsGridTindakan.insert(PenataJasaKasirRWI.dsGridTindakan.getCount(), PenataJasaKasirRWI.func.getNullProduk());
            //PenataJasaKasirRWI.form.Grid.produk.startEditing(line+1, 3);
        }
    });
};
function TRGawatDaruratColumModel() {
    return new Ext.grid.ColumnModel([
		new Ext.grid.RowNumberer(),
		{
			id			: 'colstatuskonsulrwj',
			header		: 'KONSUL',
			dataIndex	: 'STATUS_KONSULTASI',
			menuDisabled: true,
			hidden 		: true

		},{
			id			: 'coleskripsiIGD',
			header		: 'Uraian',
			dataIndex	: 'DESKRIPSI2',
			width		: 100,
			menuDisabled: true,
			hidden 		: true
			
		},{
			id 			: 'colKdProduk',
			header 		: 'Kode Produk',
			dataIndex 	: 'KD_PRODUK',
			width 		:100,
			menuDisabled:true,
			hidden 		:true
		},{
			id			: 'colKpProduk',
			header		: 'Kode Produk',
			dataIndex	: 'KP_PRODUK',
			width		: 100,
			menuDisabled: true,
			editor 		: new Ext.form.TextField({
				id 					: 'fieldcolKdProduk',
				allowBlank 			: true,
				enableKeyEvents  	: true,
				width 				: 30,
				listeners 			:{ 
					'specialkey' : function(a, b){
						if (b.getKey() === b.ENTER || b.getKey() === b.TAB) {
							a.setValue(Ext.get('fieldcolKdProduk').dom.value.toUpperCase());
							Ext.Ajax.request({
								store	: KasirIGDDataStoreProduk,
								url 	: baseURL + "index.php/main/functionIGD/getProdukKey",
								params 	:  {
									kd_unit 		: kodeunit,
									kd_customer 	: kdcustomeraa,
									text 			: Ext.getCmp('fieldcolKdProduk').getValue(),
								},
								failure : function(o){
									ShowPesanWarningKasirigd("Data produk tidak ada!",'WARNING');
								},
								success : function(o){
									var cst = Ext.decode(o.responseText);
									if (cst.processResult == 'SUCCESS'){
										var lineIGD = gridDTLTRIGD.getSelectionModel().selection.cell[0];
										kd_produk       	= cst.listData.kd_produk;
										kp_produk       	= cst.listData.kp_produk;
										kd_tarif        	= cst.listData.kd_tarif;
										deskripsi 			= cst.listData.deskripsi;
										harga				= cst.listData.tarifx;
										tgl_berlaku			= cst.listData.tgl_berlaku;
										jumlah				= cst.listData.jumlah;
										status_konsultasi 	= cst.listData.status_konsultasi;
										dsTRDetailKasirIGDList.getRange()[lineIGD].set('DESKRIPSI', cst.listData.deskripsi);
										dsTRDetailKasirIGDList.getRange()[lineIGD].set('QTY', 1);
										dsTRDetailKasirIGDList.getRange()[lineIGD].set('HARGA', toInteger(cst.listData.tarifx));

										dsTRDetailKasirIGDList.data.items[lineIGD].data.KD_PRODUK         = cst.listData.kd_produk;
										dsTRDetailKasirIGDList.data.items[lineIGD].data.KP_PRODUK         = cst.listData.kp_produk;
										dsTRDetailKasirIGDList.data.items[lineIGD].data.KD_KLAS           = cst.listData.KD_KLAS;
										dsTRDetailKasirIGDList.data.items[lineIGD].data.KD_UNIT           = cst.listData.kd_unit;
										dsTRDetailKasirIGDList.data.items[lineIGD].data.HARGA             = toInteger(cst.listData.tarifx);
										dsTRDetailKasirIGDList.data.items[lineIGD].data.DESKRIPSI         = cst.listData.deskripsi;
										currentJasaDokterHargaJP_KasirIGD                                 = toInteger(cst.listData.tarifx);
										dsTRDetailKasirIGDList.data.items[lineIGD].data.TGL_BERLAKU       = cst.listData.tgl_berlaku;
										dsTRDetailKasirIGDList.data.items[lineIGD].data.KD_TARIF          = cst.listData.kd_tarif;
										dsTRDetailKasirIGDList.data.items[lineIGD].data.TGL_TRANSAKSI     = now;
										dsTRDetailKasirIGDList.data.items[lineIGD].data.TMP_TGL_TRANSAKSI = nowTglTransaksiIGD2;
										dsTRDetailKasirIGDList.data.items[lineIGD].data.STATUS_KONSULTASI = cst.listData.status_konsultasi;
										dsTRDetailKasirIGDList.data.items[lineIGD].data.QTY               = 1;
										
										var igdUrut=0;
										if(lineIGD>=1){
											igdUrut=toInteger(dsTRDetailKasirIGDList.data.items[lineIGD-1].data.URUT); 
										}
										dsTRDetailKasirIGDList.data.items[lineIGD].data.URUT               = igdUrut+1;
										senderProdukIGD(
											notransaksi, 
											kd_produk, 
											kd_tarif, 
											CurrentKdKasir_KasirIGD, 
											igdUrut, 
											cst.listData.kd_unit, 
											nowTglTransaksiIGD2,
											tgl_berlaku,
											1,
											harga,
											true, // parameter untuk keterangan update atau insert
											dsTRDetailKasirIGDList.data.items[lineIGD].data.TMP_TGL_TRANSAKSI
										);
										gridDTLTRIGD.getView().refresh();
										gridDTLTRIGD.startEditing(CurrentKasirIGD.row, 9);
									}else{
										ShowPesanWarningKasirigd('Data produk tidak ada!','WARNING');
									}
								}
							})
						}
					}
				}
			})
		},{
			id			: 'colUrut',
			header		: 'Urut',
			dataIndex	: 'URUT',
			width		: 100,
			menuDisabled: true,
			hidden		: true
		},{
			id 		 	: 'colDeskripsiIGD',
			header 	 	: 'Nama Produk',
			dataIndex	: 'DESKRIPSI',
			sortable	: false,
			hidden		: false,
			menuDisabled: true,
			width		: 220,
			editor: KasirIGDComboBoxKdProduk = Nci.form.Combobox.autoComplete({
				store	: KasirIGDDataStoreProduk,
				select	: function(a,b,c){
					var lineIGD = gridDTLTRIGD.getSelectionModel().selection.cell[0];
					dsTRDetailKasirIGDList.getRange()[lineIGD].set('DESKRIPSI', b.data.deskripsi);
					dsTRDetailKasirIGDList.getRange()[lineIGD].set('QTY', 1);
					dsTRDetailKasirIGDList.getRange()[lineIGD].set('HARGA', toInteger(b.data.harga));
					dsTRDetailKasirIGDList.data.items[lineIGD].data.KD_PRODUK         = b.data.kd_produk;
					dsTRDetailKasirIGDList.data.items[lineIGD].data.KP_PRODUK         = b.data.kp_produk;
					dsTRDetailKasirIGDList.data.items[lineIGD].data.KD_KLAS           = b.data.KD_KLAS;
					dsTRDetailKasirIGDList.data.items[lineIGD].data.HARGA             = b.data.harga;
					dsTRDetailKasirIGDList.data.items[lineIGD].data.KD_UNIT           = b.data.kd_unit;
					currentJasaDokterHargaJP_KasirIGD                                  = b.data.harga;
					dsTRDetailKasirIGDList.data.items[lineIGD].data.TGL_BERLAKU       = b.data.tgl_berlaku;
					dsTRDetailKasirIGDList.data.items[lineIGD].data.KD_TARIF          = b.data.kd_tarif;
					dsTRDetailKasirIGDList.data.items[lineIGD].data.TGL_TRANSAKSI     = now.toLocaleDateString("en-US");
					dsTRDetailKasirIGDList.data.items[lineIGD].data.TMP_TGL_TRANSAKSI = nowTglTransaksiIGD2;
					dsTRDetailKasirIGDList.data.items[lineIGD].data.STATUS_KONSULTASI = b.data.status_konsultasi;
					dsTRDetailKasirIGDList.data.items[lineIGD].data.QTY               = 1;
					var igdUrut=0;
					if(lineIGD>=1){
						igdUrut=toInteger(dsTRDetailKasirIGDList.data.items[lineIGD-1].data.URUT); 
					}
					dsTRDetailKasirIGDList.data.items[lineIGD].data.URUT               = igdUrut+1;
					currentJasaDokterUrutDetailTransaksi_KasirIGD=igdUrut+1;
						senderProdukIGD(
							notransaksi, 
							b.data.kd_produk, 
							b.data.kd_tarif, 
							CurrentKdKasir_KasirIGD, 
							igdUrut, 
							b.data.kd_unit, 
							nowTglTransaksiIGD2,
							b.data.tgl_berlaku,
							1,
							b.data.harga,
							true, // parameter untuk keterangan update atau insert
							dsTRDetailKasirIGDList.data.items[lineIGD].data.TMP_TGL_TRANSAKSI
						);
						gridDTLTRIGD.getView().refresh();
						gridDTLTRIGD.startEditing(lineIGD, 9);
					
				},
				param	: function(){
					var params            = {};
					params['kd_unit']     = kodeunit;
					params['kd_customer'] = kdcustomeraa;
					return params;
				},
				insert	: function(o){
					return {
						kd_produk       	: o.kd_produk,
						kp_produk       	: o.kp_produk,
						kd_tarif        	: o.kd_tarif,
						kd_unit        		: o.kd_unit,
						deskripsi 			: o.deskripsi,
						harga				: o.tarifx,
						tgl_berlaku			: o.tgl_berlaku,
						jumlah				: o.jumlah,
						status_konsultasi 	: o.status_konsultasi,
						text				: '<table style="font-size: 11px;"><tr><td width="50">'+o.kp_produk+'</td><td width="160" align="left">'+o.deskripsi+'</td><td width="130">'+o.klasifikasi+'</td></tr></table>'
					}
				},
				url			: baseURL + "index.php/main/functionIGD/getProduk",
				valueField	: 'deskripsi',
				displayField: 'text'
			})
		},{
			header 		: 'Tanggal Transaksi',
			dataIndex 	: 'TGL_TRANSAKSI',
			width 		: 80,
			menuDisabled: true,
			editor: new Ext.form.DateField({
					format: 'd/m/Y',
					width: 80,
					editable: true,
					listeners:{
						blur: function(a){
							tglubahtindakankasirigd = a.getValue();
							if((convertTglTransaksiKasirIGD(tglubahtindakankasirigd) < tgltrans) || (convertTglTransaksiKasirIGD(tglubahtindakankasirigd) > convertTglTransaksiKasirIGD(now))){
								if(convertTglTransaksiKasirIGD(tglubahtindakankasirigd) < tgltrans) {
									ShowPesanWarningKasirigd('Tanggal tindakan tidak boleh kurang dari tanggal transaksi!','WARNING');
								} else{
									ShowPesanWarningKasirigd('Tanggal tindakan tidak boleh lebih dari tanggal hari ini!','WARNING');
								}
							}else {
								var lineIGD	= this.indeks;
								var igdUrut=toInteger(dsTRDetailKasirIGDList.data.items[lineIGD].data.URUT)-1;
								
								senderProdukIGD(
									notransaksi, 
									dsTRDetailKasirIGDList.data.items[lineIGD].data.KD_PRODUK, 
									dsTRDetailKasirIGDList.data.items[lineIGD].data.KD_TARIF, 
									CurrentKdKasir_KasirIGD, 
									igdUrut, 
									dsTRDetailKasirIGDList.data.items[lineIGD].data.KD_UNIT, 
									tglubahtindakankasirigd,
									dsTRDetailKasirIGDList.data.items[lineIGD].data.TGL_BERLAKU,
									1,
									dsTRDetailKasirIGDList.data.items[lineIGD].data.HARGA,
									false, // parameter untuk keterangan update atau insert
									dsTRDetailKasirIGDList.data.items[lineIGD].data.TMP_TGL_TRANSAKSI
								); 
								
								dsTRDetailKasirIGDList.data.items[lineIGD].data.TMP_TGL_TRANSAKSI=convertTglTransaksiKasirIGD(tglubahtindakankasirigd);
								gridDTLTRIGD.getView().refresh();
								gridDTLTRIGD.startEditing(lineIGD,9);
							}
								
						},
						focus:function(){
							this.indeks=gridDTLTRIGD.getSelectionModel().selection.cell[0];
						},
						specialkey : function(a, b){
							if(convertTglTransaksiKasirIGD(tglubahtindakankasirigd) < tgltrans){
								ShowPesanWarningKasirigd('Tanggal tindakan tidak boleh kurang dari tanggal transaksi!','WARNING');
							}else {
								if (b.getKey() === b.ENTER || b.getKey() === b.TAB) {
									var lineIGD = gridDTLTRIGD.getSelectionModel().selection.cell[0];
									gridDTLTRIGD.startEditing(lineIGD,9);
								}
							}
						}
					}
			}),
			renderer: Ext.util.Format.dateRenderer('d/M/Y')
		},{
			id 		 	: 'colHARGAIGD',
			header 		: 'Harga Rp.',
			align 		: 'right',
			hidden 		: false,
			menuDisabled: true,
			dataIndex 	: 'HARGA',
			width 		: 80,
			listeners:{
				dblclick: function(dataview, index, item, e) {
					//console.log(dsTRDetailKasirRWJList.data.items[item].data);
					PilihEditTarifLookUp_KasirIGD(dsTRDetailKasirIGDList.data.items[item].data);
				}
			}			
		},{
			id 			: 'colProblemIGD',
			header 		: 'Qty',
			align 		: 'right',
			width 		: 50,
			menuDisabled: true,
			dataIndex 	: 'QTY',
			editor 		: new Ext.form.TextField({
				id:'fieldcolProblemIGD',
				allowBlank: true,
				enableKeyEvents : true,
				width:30,
				listeners:{
					'specialkey' : function(a, b){
						if (b.getKey() === b.ENTER || b.getKey() === b.TAB) {
							if((convertTglTransaksiKasirIGD(tglubahtindakankasirigd) < tgltrans) || (convertTglTransaksiKasirIGD(tglubahtindakankasirigd) > convertTglTransaksiKasirIGD(now))){
								if(convertTglTransaksiKasirIGD(tglubahtindakankasirigd) < tgltrans) {
									ShowPesanWarningKasirigd('Tanggal tindakan tidak boleh kurang dari tanggal transaksi!','WARNING');
								} else{
									ShowPesanWarningKasirigd('Tanggal tindakan tidak boleh lebih dari tanggal hari ini!','WARNING');
								}
							}else {
								var lineIGD = gridDTLTRIGD.getSelectionModel().selection.cell[0];
								var igdUrut=toInteger(dsTRDetailKasirIGDList.data.items[lineIGD].data.URUT)-1;
								senderProdukIGD(
									notransaksi, 
									dsTRDetailKasirIGDList.data.items[lineIGD].data.KD_PRODUK, 
									dsTRDetailKasirIGDList.data.items[lineIGD].data.KD_TARIF, 
									CurrentKdKasir_KasirIGD, 
									igdUrut, 
									dsTRDetailKasirIGDList.data.items[lineIGD].data.KD_UNIT, 
									dsTRDetailKasirIGDList.data.items[lineIGD].data.TGL_TRANSAKSI,
									dsTRDetailKasirIGDList.data.items[lineIGD].data.TGL_BERLAKU,
									Ext.get('fieldcolProblemIGD').dom.value,
									dsTRDetailKasirIGDList.data.items[lineIGD].data.HARGA,
									false, // parameter untuk keterangan update atau insert
									dsTRDetailKasirIGDList.data.items[lineIGD].data.TMP_TGL_TRANSAKSI
								);
								TambahBarisIGD();
								
								gridDTLTRIGD.startEditing(lineIGD+1, 4);
							}
						}
					},
				}
			}),
		},{
			id 			: 'colDokterPenindak',
			header 		: 'Pelaksana',
			align 		: 'center',
			width 		: 50,
			menuDisabled: true,
			dataIndex 	: 'JUMLAH_DOKTER',
			renderer: function(value, metaData, record, rowIndex, colIndex, store){
				 var jumlahDokter;
				 if (value > 0) {
					jumlahDokter = value;
				 }else{
					jumlahDokter = 0;
				 }
				 return jumlahDokter;
			},
			listeners:{
				dblclick: function(dataview, index, item, e) {
					dataRow = item;
					tmpKdJob 	= 1;
					tmpkd_unit 	= '301';
					if (dsTRDetailKasirIGDList.data.items[item].data.KD_PRODUK == '6437' && dsTRDetailKasirIGDList.data.items[item].data.DESKRIPSI=='Konsul Dokter IRD') {
						loaddatastoredokter_IGD_REVISI();
						loaddatastoredokterKlinik_REVISI();
						PilihDokterLookUpKasir_IGD_REVISI(dsTRDetailKasirIGDList.data.items[item].data);
					}
				}
			}
		},{
			id 			: 'colImpactIGD',
			header 		: 'CR',
			width 		: 80,
			dataIndex 	: 'IMPACT',
			hidden 		: true,
			editor: new Ext.form.TextField({
				id 				:'fieldcolImpactIGD',
				allowBlank 		: true,
				enableKeyEvents : true,
				width 			: 30
			})
		},{
			id			: 'colkdunittindakanigd',
			header		: 'kd_unit',
			dataIndex	: 'KD_UNIT',
			menuDisabled: true,
			hidden 		: true

		},{
			id 		 	: 'colTotalIGD',
			header 		: 'Total Rp.',
			align 		: 'right',
			hidden 		: false,
			menuDisabled: true,
			dataIndex 	: 'TOTAL',
			width 		: 80,
			renderer: function(v, params, record){
				return formatCurrency(record.data.HARGA*record.data.QTY);
			}	
		},{
			id			: 'coltmptgltransaksitindakanigd',
			header		: 'tmp_tgl_transaksi',
			dataIndex	: 'TMP_TGL_TRANSAKSI',
			menuDisabled: true,
			hidden 		: true

		},
	]);
}


function PilihDokterLookUpKasir_IGD_REVISI(params) {
	//console.log(params);
	//tmp_notransaksi 	= params.NO_TRANSAKSI;
	tmp_tgltransaksi 	= params.TGL_TRANSAKSI;
	tmp_kdproduk 		= params.KD_PRODUK;
	tmp_urut 			= params.URUT;
	tmp_kdunit 			= params.KD_UNIT;
	tmp_tarif 			= params.KD_TARIF;
	tmp_tglberlaku 		= params.TGL_BERLAKU;
	var FormLookUDokter_IGD = new Ext.Window(
		{
			xtype : 'fieldset',
			layout:'column',
			id: 'winTRDokterPenindak_IGD_REVISI',
			title: 'Pilih Dokter Penindak',
			closeAction: 'destroy',
			width: 720,
			height: 400,
			border: false,
			resizable: false,
			iconCls: 'Request',
			constrain : true,    
			modal: true,
			items: [ 
			{
				border:false,
				columnWidth:.45,
				bodyStyle:'margin-top: 5px',
				items:[
					cboPenangan_KASIRIGD(),
					cboPoli_IRD_KASIRIGD(),
					seconGridPenerimaan(),
				]
			},
			{
				border:false,
				columnWidth:.1,
				bodyStyle:'margin-top: 5px',
			},
			{
				border:false,
				columnWidth:.45,
				bodyStyle:'margin-top: 5px; align:right;',
				items:[
					{
						xtype   : 'textfield',
						anchor  : '100%',
						id      : 'txtSearchDokter_KASIRRWI',
						name    : 'txtSearchDokter_KASIRRWI',
						listeners : {
							specialkey: function (){
								if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9){
									DataStorefirstGridStore_IGD.removeAll();
									/*tmpSearchDokter = Ext.getCmp('txtSearchDokter_KASIRRWI').getValue();
									tmpKdJob        = Ext.getCmp('cboPenangan_KASIRRWI').getValue();
									if (tmpKdJob == 'Dokter') {
										tmpKdJob = 1;
									}else{
										tmpKdJob = 3;
									}
									loaddatastoredokter_IGD_REVISI(Ext.getCmp('txtSearchDokter_KASIRRWI').getValue());*/
									loaddatastoredokter_IGD_REVISI(Ext.getCmp('txtSearchDokter_KASIRRWI').getValue());
								}
							},
						}
					},
					firstGridPenerimaan(),
				]
			}
            ],
            tbar :
            [
                {
					xtype:'button',
					text:'Simpan',
					iconCls : 'save',
					hideLabel:true,
					id: 'BtnOktrDokter',
					handler:function()
					{
						//RefreshDataDetail_kasirrwi(tmpno_transaksi);
						
						//PenataJasaKasirRWI.dsGridTindakan.getRange()[dataRowIndexDetail].set('DESKRIPSI', PenataJasaKasirRWI.dsGridTindakan.getRange()[line].get('DESKRIPSI')+" ("+tmpNamaDokter+")");
						FormLookUDokter_IGD.close();
                    }
				},
				'-',
			],
        }
    );
	FormLookUDokter_IGD.show();
};
function firstGridPenerimaan(){
    	DataStorefirstGridStore_IGD.removeAll();
        var cols = [
            { id : 'KD_DOKTER', header: "Kode Dokter", width: .25, sortable: true, dataIndex: 'KD_DOKTER',hidden : false},
            { id : 'NAMA', header: "Nama", width: .75, sortable: true, dataIndex: 'NAMA'}
        ];
        var firstGrid;
            firstGrid = new Ext.grid.GridPanel({
            ddGroup          : 'secondGridDDGroup',
            store            : DataStorefirstGridStore_IGD,
			columns          : cols,
            autoScroll       : true,
            columnLines      : true,
            border           : true,
            enableDragDrop   : true,
            height           : 250,
            stripeRows       : true,
            trackMouseOver   : true,
            title            : 'Dokter/ Perawat penangan',
            anchor           : '100% 100%',
            plugins          : [new Ext.ux.grid.FilterRow()], 
                    colModel         : new Ext.grid.ColumnModel
                            (
                                    [
                                            new Ext.grid.RowNumberer(),
                                            {
                                                    id: 'colNMKd_Dokter',
                                                    header: 'KD Dokter',
                                                    dataIndex: 'KD_DOKTER',
                                                    sortable: true,
                                                    hidden : false,
                                                    width: .25
                                            },
                                            {
                                                    id: 'colNMDokter',
                                                    header: 'Nama',
                                                    dataIndex: 'NAMA',
                                                    sortable: true,
                                                    width: .75
                                            }
                                    ]
                            ),
            listeners : {
                afterrender : function(comp) {
						var firstGridDropTargetEl =  firstGrid.getView().scroller.dom;
						var firstGridDropTarget = new Ext.dd.DropTarget(firstGridDropTargetEl, {
							ddGroup    : 'firstGridDDGroup',
							notifyDrop : function(ddSource, e, data){
									var records   =  ddSource.dragData.selections;
									var kd_dokter = "";
									for(var i=0,iLen=records.length; i<iLen; i++){
										var o       = records[i];
										kd_dokter = o.data.KD_DOKTER;
									}
									Ext.Ajax.request({
										url: baseURL +  "index.php/gawat_darurat/control_detail_tr_dokter/deleteDokter",
										params: {
											kd_kasir 		: '06',
											no_transaksi 	: tmp_notransaksi,
											urut 			: tmp_urut,
											tgl_transaksi 	: tmp_tgltransaksi,
											kd_dokter 		: kd_dokter,
										},
										success: function(response) {
											var cst  = Ext.decode(response.responseText);
											dsTRDetailKasirIGDList.getRange()[dataRow].set('JUMLAH_DOKTER', cst.count_dokter);
											Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
											firstGrid.store.add(records);
											firstGrid.getView().refresh();
											return true
										},
									});
								}
						});
                },      
            },
			viewConfig: 
			{
				forceFit: true,
			}
        });
        return firstGrid;
    };

function seconGridPenerimaan(){
		DataStoreSecondGridStore_IGD.removeAll();
		var secondGrid;
		var fields = [
			{name: 'KD_UNIT', mapping : 'KD_UNIT'},
			{name: 'NAMA_UNIT', mapping : 'NAMA_UNIT'}
		];
		secondGridStoreLapPenerimaan = new Ext.data.JsonStore({
			fields : fieldsDokterPenindak,
			root   : 'data'
		});
		var cols = [
			{ id : 'KD_DOKTER', header: "Kode Dokter", width: .25, sortable: true, dataIndex: 'KD_DOKTER',hidden : false},
			{ header: "Nama", width: .75, sortable: true, dataIndex: 'NAMA'},
			{ header: "JP", width: .25, dataIndex: 'JP'},
			{ header: "PAJAK", width: .25, dataIndex: 'PAJAK'},
		];
			secondGrid = new Ext.grid.GridPanel({
					ddGroup          : 'firstGridDDGroup',
					store            : DataStoreSecondGridStore_IGD,
					columns          : cols,
					autoScroll       : true,
					columnLines      : true,
					border           : true,
					enableDragDrop   : true,
					enableDragDrop   : true,
					height           : 250,
					stripeRows       : true,
					autoExpandColumn : 'KD_DOKTER',
					title            : 'Dokter/ Perawat yang menangani',
					plugins          : [new Ext.ux.grid.FilterRow()],
					listeners : {
						afterrender : function(comp) {

							var secondGridDropTargetEl = secondGrid.getView().scroller.dom;
							var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
								ddGroup    : 'secondGridDDGroup',
								notifyDrop : function(ddSource, e, data){
									var records   =  ddSource.dragData.selections;
									var kd_dokter = "";
									for(var i=0,iLen=records.length; i<iLen; i++){
										var o       = records[i];
										kd_dokter = o.data.KD_DOKTER;
									}
                                    // console.log(cellSelecteddeskripsi_kasir_rwi.data);
									Ext.Ajax.request({
										url: baseURL +  "index.php/gawat_darurat/control_detail_tr_dokter/insertDokter",
										params: {
											label 			: Ext.getCmp("cboPenangan_KASIRRWI").getValue(),
                                            kd_job          : tmpKdJob,
											no_transaksi    : rowSelectedPJIGD.data.NO_TRANSAKSI,
											tgl_transaksi   : rowSelectedPJIGD.data.TANGGAL_TRANSAKSI,
											kd_produk       : cellSelecteddeskripsi_panatajasaigd.data.KD_PRODUK,
											urut            : cellSelecteddeskripsi_panatajasaigd.data.URUT,
											kd_kasir 		: '06',
											kd_unit 		: tmp_kdunit,
											kd_dokter 		: kd_dokter,
											kd_tarif 		: tmp_tarif,
											tgl_berlaku 	: tmp_tglberlaku,
										},
										success: function(response) {
											DataStoreSecondGridStore_IGD.removeAll();
											loaddatastoredokterKlinik_REVISI();
											var cst  = Ext.decode(response.responseText);
											dsTRDetailKasirIGDList.getRange()[dataRow].set('JUMLAH_DOKTER', cst.count_dokter);
											//console.log(cst);
											Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
											//secondGrid.store.add(records);
											secondGrid.getView().refresh();
											return true
										},
									});
								}
							});
						},/*
					callback : function(){
						Ext.getCmp('txtSearchDokter_KASIRRWI').focus();
					}*/
			},
			viewConfig: 
			{
				forceFit: true,
			}
		});
		return secondGrid;
	};


	function loaddatastoredokterKlinik_REVISI(){
		Ext.Ajax.request({
			url: baseURL +  "index.php/gawat_darurat/control_detail_tr_dokter/getDokterKlinik_Penindak",
			params: {
				kd_kasir 		: '06',
				no_transaksi 	: tmp_notransaksi,
				tgl_transaksi 	: tmp_tgltransaksi,
                urut            : tmp_urut,
			},
			success: function(response) {
				var cst  = Ext.decode(response.responseText);
				for(var i=0,iLen=cst['data'].length; i<iLen; i++){
					var recs    = [],recType = dsDataDaftarPenangan_KASIR_IGD.recordType;
					var o       = cst['data'][i];
					recs.push(new recType(o));
					DataStoreSecondGridStore_IGD.add(recs);
				}
			},
		});
	}

    function cboPenangan_KASIRIGD(){
		var Field = ['KD_JOB','KD_COMPONENT','LABEL'];
		
		dsDataDaftarPenangan_KASIR_IGD = new WebApp.DataStore({ fields: Field });
		//loaddatastorePenangan();
		var cboPenangan_KASIRRWI       = new Ext.form.ComboBox
		({
			id: 'cboPenangan_KASIRRWI',
			typeAhead: true,
			width: 160,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus: true,
			forceSelection: true,
			emptyText: 'Pilih penangan...',
			fieldLabel: 'Penangan ',
			align: 'Right',
			store: new Ext.data.ArrayStore({
				id: 1,
				fields:['KD_JOB','LABEL'],
				data: [[1, 'Dokter'], [0, 'Perawat']]
			}),
			valueField: 'KD_JOB',
			displayField: 'LABEL',
			//anchor           : '25%',
			value           : 1,
			listeners:
			{
                select : function(){
					DataStorefirstGridStore_IGD.removeAll();
					Ext.getCmp('cboPenangan_KASIRRWI').getValue()
					//tmpKdJob = Ext.getCmp('cboPenangan_KASIRRWI').getValue();
                    if (Ext.getCmp('cboPenangan_KASIRRWI').getValue() != 1) {
                    	tmpKdJob = 0;
                    }else{
                    	tmpKdJob = 1;
                    }
					loaddatastoredokter_IGD_REVISI();
                }
			}
		})

        return cboPenangan_KASIRRWI;
    }

    function cboPoli_IRD_KASIRIGD(){
		//loaddatastorePenangan();
		var cboPoli_IRD_KASIRRWI       = new Ext.form.ComboBox
		({
			id: 'cboPoli_IRD_KASIRRWI',
			typeAhead: true,
			width: 160,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus: true,
			forceSelection: true,
			emptyText: 'Pilih penangan...',
			fieldLabel: 'Penangan ',
			align: 'Right',
			store: new Ext.data.ArrayStore({
				id: 1,
				fields:['KD_UNIT','UNIT'],
				data: [[0, 'Semua'], ['301', 'IRD Umum']]
			}),
			valueField: 'KD_UNIT',
			displayField: 'UNIT',
			//anchor           : '25%',
			value           : '301',
			listeners:
			{
                select : function(){
					DataStorefirstGridStore_IGD.removeAll();
					Ext.getCmp('cboPoli_IRD_KASIRRWI').getValue()
					tmpkd_unit = Ext.getCmp('cboPoli_IRD_KASIRRWI').getValue();
					loaddatastoredokter_IGD_REVISI();
                }
			}
		})

        return cboPoli_IRD_KASIRRWI;
    }


/*	function loaddatastorePenangan(){
		Ext.Ajax.request({
			url: baseURL +  "index.php/general/control_cmb_dokter/getCmbDokterInapInt",
			params: {
				null : null,
			},
			success: function(response) {
				var cst  = Ext.decode(response.responseText);
				for(var i=0,iLen=cst['data'].length; i<iLen; i++){
					var recs    = [],recType = dsDataDaftarPenangan_KASIR_IGD.recordType;
					var o       = cst['data'][i];
					recs.push(new recType(o));
					dsDataDaftarPenangan_KASIR_IGD.add(recs);
				}
			},
		});
	}*/

	function loaddatastoredokter_IGD_REVISI(dokter){
		Ext.Ajax.request({
			url: baseURL +  "index.php/gawat_darurat/control_detail_tr_dokter/getCmbDokterKlinik",
			params: {
                kd_unit         : tmpkd_unit,
                jenis_dokter    : tmpKdJob,
				kd_kasir 		: '06',
				no_transaksi 	: tmp_notransaksi,
				tgl_transaksi 	: tmp_tgltransaksi,
				urut 			: tmp_urut,
				txtDokter 		: dokter,
			},
			success: function(response) {
				var cst  = Ext.decode(response.responseText);
				for(var i=0,iLen=cst['data'].length; i<iLen; i++){
					var recs    = [],recType = dsDataDaftarPenangan_KASIR_IGD.recordType;
					var o       = cst['data'][i];
					recs.push(new recType(o));
					DataStorefirstGridStore_IGD.add(recs);
				}
			}
		});
	}
function getItemTestIGD(){
	var radComboboxIGD = new Ext.form.ComboBox({
	   	id: 'cboTindakanRequestIGD',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		hideTrigger:true,
		forceSelection: true,
		selectOnFocus:true,
		store: ProdukDataIGD,
		valueField: 'DESKRIPSI',
		displayField: 'DESKRIPSI',
		anchor: '95%',
		listeners:{
		
			'select': function(a, b, c){
				dsTRDetailKasirIGDList.data.items[CurrentRowPemeriksaanIGD].data.KD_PRODUK     = b.data.KD_PRODUK;
				dsTRDetailKasirIGDList.data.items[CurrentRowPemeriksaanIGD].data.KD_KLAS       = b.data.KD_KLAS;
				dsTRDetailKasirIGDList.data.items[CurrentRowPemeriksaanIGD].data.HARGA         = b.data.TARIF;
				dsTRDetailKasirIGDList.data.items[CurrentRowPemeriksaanIGD].data.TGL_BERLAKU   = b.data.TGL_BERLAKU;
				dsTRDetailKasirIGDList.data.items[CurrentRowPemeriksaanIGD].data.KD_TARIF      = b.data.KD_TARIF;
				dsTRDetailKasirIGDList.data.items[CurrentRowPemeriksaanIGD].data.TGL_TRANSAKSI = nowTglTransaksiIGD2;
				dsTRDetailKasirIGDList.data.items[CurrentRowPemeriksaanIGD].data.QTY           = 1;
				
				var currenturut= CurrentRowPemeriksaanIGD + 1;
				cekKomponen(b.data.KD_PRODUK,b.data.KD_TARIF,kodeunit,currenturut,b.data.TARIF);
		 	}
		}
	});
	return radComboboxIGD;
}

function getProdukIGD(kd_unit){

	var str='LOWER(tarif.kd_tarif)=LOWER(~'+varkd_tarif_igd+'~) and tarif.kd_unit= ~'+kd_unit+'~ ';
	ProdukDataIGD.load({
		params: {
			Skip: 0,
			Take: 50,
			target:'LookupProduk',
			param: str
		}
	});


	return ProdukDataIGD;
}

function GetLookupAssetCMRIGD(str)
{

		var p = new mRecordIGD
		(
			{
				'DESKRIPSI2':'',
				'KD_PRODUK':'',
				'DESKRIPSI':'', 
				'KD_TARIF':'', 
				'HARGA':'',
				'QTY':dsTRDetailKasirIGDList.data.items[CurrentKasirIGD.row].data.QTY,
				'TGL_TRANSAKSI':dsTRDetailKasirIGDList.data.items[CurrentKasirIGD.row].data.TGL_TRANSAKSI,
				'DESC_REQ':dsTRDetailKasirIGDList.data.items[CurrentKasirIGD.row].data.DESC_REQ,
				'KD_TARIF':dsTRDetailKasirIGDList.data.items[CurrentKasirIGD.row].data.KD_TARIF,
				'URUT':dsTRDetailKasirIGDList.data.items[CurrentKasirIGD.row].data.URUT
			}
		);
	
	FormLookupKasir(str,strb,nilai_kd_tarif,dsTRDetailKasirIGDList,p,false,CurrentKasirIGD.row,false,syssetting);
	
};


function DataDeleteKasirIGDDetail()
{
    Ext.Ajax.request
    (
        {
            url: baseURL + "index.php/main/DeleteDataObj",
            params:  getParamDataDeleteKasirIGDDetail(),
			failure: function(o)
			{
				ShowPesanWarningKasirigd("Error, hapus tindakan. Hubungi Admin!",'WARNING');
				loadMask.hide();
			},
            success: function(o)
            {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true)
                {
                    ShowPesanInfoKasirigd('Hapus baris tindakan berhasil','Information');
					//DataDeleteKasirIGDDetail_SQL();
                    dsTRDetailKasirIGDList.removeAt(CurrentKasirIGD.row);
                    cellSelecteddeskripsi=undefined;
                    RefreshDataKasirIGDDetail(notransaksi);
                    AddNewKasirIGD = false;
                }
                else if (cst.success === false && cst.pesan === 0 )
                {
                    ShowPesanWarningKasirigd('Gagal menghapus baris tindakan', 'WARNING');
                }
                else
                {
                    ShowPesanWarningKasirigd('Error hapus tindakan!','WARNING');
                };
            }
        }
    )
};

function HapusBarisIGD()
{
    // if (cellSelecteddeskripsi != undefined)
    // {
        // if (cellSelecteddeskripsi.data.DESKRIPSI2 != '' && cellSelecteddeskripsi.data.KD_PRODUK != '')
        // {
			var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan penghapusan:', function(btn, combo){
				if (btn == 'ok')
				{
					variablehistori=combo;
					if(variablehistori != ''){
						senderProdukDeleteIGD(CurrentKdKasir_KasirIGD, CurrentKasirIGD.data.data.KD_PRODUK, notransaksi, CurrentKasirIGD.data.data.URUT, CurrentKasirIGD.data.data.TGL_TRANSAKSI,CurrentKasirIGD.data.data.DESKRIPSI,variablehistori); 
					} else{
						ShowPesanWarningKasirigd('Alasan penghapusan harus di isi!','WARNING');
					}
					//DataDeleteKasirIGDDetail();
				}
			}); 
        // }
        // else
        // {
            // dsTRDetailKasirIGDList.removeAt(CurrentKasirIGD.row);
        // };
    // }
};
function senderProdukDeleteIGD(kd_kasir, kd_produk, no_transaksi, urut, tgl_transaksi, deskripsi, alasan){
    Ext.Ajax.request({
        url     : baseURL + "index.php/main/functionIGD/hapusDataProduk",
        params  :  {
            kd_produk       : kd_produk,
            no_transaksi    : no_transaksi,
            kd_kasir        : kd_kasir,
            urut            : urut,
            tgl_transaksi   : tgl_transaksi,
			alasan			: alasan,
			kd_pasien		: kodepasien,
			nama			: namapasien,
			kd_unit			: kodeunit,
			nama_unit		: namaunit,
			deskripsi		: deskripsi,
			total			: currentTOTAL_KasirIGD,
		
			
        },
        failure : function(o)
        {
            console.log(o);
        },
        success : function(o)
        {
            var cst = Ext.decode(o.responseText);
            // console.log(CurrentKasirIGD.row);
            // console.log(dsTRDetailKasirIGDList.data);
            // console.log(dsTRDetailKasirIGDList.data.items[CurrentKasirIGD.row].data);
            // console.log(parseInt(dsTRDetailKasirIGDList.data.items[CurrentKasirIGD.row].data.HARGA) * parseInt(dsTRDetailKasirIGDList.data.items[CurrentKasirIGD.row].data.QTY));
            Ext.getCmp('txtJumlahBayar_KASIR_IGD').setValue(parseInt(Ext.getCmp('txtJumlahBayar_KASIR_IGD').getValue()) - (parseInt(dsTRDetailKasirIGDList.data.items[CurrentKasirIGD.row].data.HARGA) * parseInt(dsTRDetailKasirIGDList.data.items[CurrentKasirIGD.row].data.QTY)));
			dsTRDetailKasirIGDList.removeAt(CurrentKasirIGD.row);

        }
    });
};
function getItemPaneltgl_filter() {
	var items ={
	    layout: 'column',
	    border: false,
	    items:[
			{
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:[
					{
					    xtype: 'datefield',
					    fieldLabel: 'Tanggal ',
					    id: 'dtpTglAwalFilterKasirigd',
					    name: 'dtpTglAwalFilterKasirigd',
					    format: 'd/M/Y',
					    value: now,
					    width: 100,
					    listeners:{
							'specialkey' : function(){
                                var tmpNoMedrec = Ext.get('txtFilterNomedrec').getValue()
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10 ){
									RefreshDataFilterKasirIgdKasir();
								}
							}
						}
					}
				]
			},
			{xtype: 'displayfield', value:'&nbsp; s/d &nbsp;'},
			{
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:[ 
					{
					    xtype: 'datefield',
					    id: 'dtpTglAkhirFilterKasirigd',
					    name: 'dtpTglAkhirFilterKasirigd',
					    format: 'd/M/Y',
					    value: now,
					    width: 100,
						listeners:{
							'specialkey' : function(){
                                var tmpNoMedrec = Ext.get('txtFilterNomedrec').getValue()
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10 ){
									RefreshDataFilterKasirIgdKasir();
								}
							}
						}
					}
				]
			}
		]
	}
    return items;
}
function getItemPanelcombofilter() {
	var items ={
	    layout: 'column',
	    border: false,
	    items:[
			{
			    // columnWidth: .50,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:[
					mComboStatusBayar_viKasirigdKasir()
				]
			},{
				xtype:'displayfield',
				value:'&nbsp;'
			},{
			    // columnWidth: .50,
			    layout: 'form',
			    border: false,
				labelWidth:60,
			    items:[ 
					mComboUnit_viKasirigdKasir()
				]
			}
		]
	}
    return items;
}
function mComboStatusBayar_viKasirigdKasir(){
	var cboStatus_viKasirigdKasir = new Ext.form.ComboBox({
		id:'cboStatus_viKasirigdKasir',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender:true,
		mode: 'local',
		emptyText:'',
		fieldLabel: 'Status Lunas ',
		width:120,
		store: new Ext.data.ArrayStore({
			id: 0,
			fields:[
				'Id',
				'displayText'
			],
			data: [[1, 'Semua'],[2, 'Lunas'], [3, 'Belum Lunas']]
		}),
		valueField: 'Id',
		displayField: 'displayText',
		value:selectCountStatusByr_viKasirigdKasir,
		listeners:{
			'select': function(a,b,c){
				selectCountStatusByr_viKasirigdKasir=b.data.displayText ;
				//RefreshDataSetDokter();
				RefreshDataFilterKasirIgdKasir();
			}
		}
	});
	return cboStatus_viKasirigdKasir;
}
function mComboUnit_viKasirigdKasir() {
	var Field = ['KD_UNIT','NAMA_UNIT'];
    dsunit_viKasirigdKasir = new WebApp.DataStore({ fields: Field });
    dsunit_viKasirigdKasir.load({
		params:{
			Skip	: 0,
			Take	: 1000,
			Sort	: 'NAMA_UNIT',
			Sortdir	: 'ASC',
			target	: 'ViewSetupUnitB',
			param	: "kd_bagian=3 and type_unit=false"
		}
	});
    var cboUNIT_viKasirigdKasir = new Ext.form.ComboBox({
		id: 'cboUNIT_viKasirigdKasir',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		fieldLabel:  ' Poli ',
		align: 'Right',
		width: 150,
		store: dsunit_viKasirigdKasir,
		valueField: 'NAMA_UNIT',
		displayField: 'NAMA_UNIT',
		value:'All',
		listeners:{
			'select': function(a, b, c){					       
				RefreshDataFilterKasirIgdKasir();
				//selectStatusCMKasirigdView = b.data.DEPT_ID;
			}
		}
	});
	//cboUNIT_viKasirigdKasir.hide();
    return cboUNIT_viKasirigdKasir;
}


function mComboalasan_transfer() 
{
	var Field = ['KD_ALASAN','ALASAN'];

    var dsalasan_transfer = new WebApp.DataStore({ fields: Field });
    dsalasan_transfer.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			  
                            Sort: 'kd_alasan',
			    Sortdir: 'ASC',
			    target: 'ComboAlasanTransfer',
                param: "" //+"~ )"
			}
		}
	);
	
    var cboalasan_transfer = new Ext.form.ComboBox
	(
		{
		    id: 'cboalasan_transfer',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel:  ' Alasan Transfer ',
		    align: 'Right',
			  width: 100,
		    anchor:'100%',
		    store: dsalasan_transfer,
		    valueField: 'ALASAN',
		    displayField: 'ALASAN',
		    listeners:
			{
			
			    'select': function(a, b, c) 
				{			
					//
			    }

			}
		}
	);
	
    return cboalasan_transfer;
};



function KasirLookUpigd(rowdata) 
{
	/*
		PERBARUAN SIZE PANEL
		OLEH 	: HADAD AL GOJALI 
		TANGGAL : 2016 - 01- 17
	 */
    var lebar = 680;
    FormLookUpsdetailTRKasirigd = new Ext.Window
    (
        {
            id: 'gridKasirigd',
            title: 'Kasir Gawat Darurat',
            closeAction: 'destroy',
            width: lebar,
            height: 500,
            border: false,
            resizable: false,
            plain: true,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryKasirigd(lebar),
            listeners:
            {
                
            }
        }
    );

    FormLookUpsdetailTRKasirigd.show();
    getIdGetDataSettingKasirIGD();
    if (rowdata == undefined) 
	{
        KasirigdAddNew();
    }
    else 
	{
        TRKasirigdInit(rowdata)
    }

};
function load_data_printer_kasirigd(param)
{

	Ext.Ajax.request(
	{
		url: baseURL + "index.php/apotek/functionAPOTEK/printer",
		params:{
			command: param
		} ,
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
		success: function(o) {
			//cbopasienorder_mng_apotek.store.removeAll();
				var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsprinter_kasirigd.recordType;
				var o=cst['listData'][i];
				
				recs.push(new recType(o));
				dsprinter_kasirigd.add(recs);
				console.log(o);
			}
		}
	});
}

function mCombo_printer_kasirigd()
{ 
 
	var Field = ['kd_pasien','nama','kd_dokter','kd_unit','kd_customer','no_transaksi','kd_kasir','id_mrresep','urut_masuk','tgl_masuk','tgl_transaksi'];

    dsprinter_kasirigd = new WebApp.DataStore({ fields: Field });
	
	load_data_printer_kasirigd();
	var cbo_printer_kasirigd= new Ext.form.ComboBox
	(
		{
			id: 'cbopasienorder_printer_kasirigd',
			typeAhead		: true,
			triggerAction	: 'all',
			lazyRender		: true,
			mode			: 'local',
			emptyText: 'Pilih Printer',
			fieldLabel:  '',
			align: 'Right',
			hidden:true,
			width: 200,
			store: dsprinter_kasirigd,
			valueField: 'name',
			displayField: 'name',
			//hideTrigger		: true,
			listeners:
			{
								
			}
		}
	);return cbo_printer_kasirigd;
};
function getFormEntryKasirigd(lebar) 
{
    var pnlTRKasirigd = new Ext.FormPanel
    (
        {
            id: 'PanelTRKasirigd',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:130,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [getItemPanelInputKasir(lebar)],
           tbar:
            [
             
            ]
        }
    );
 var x;
 var paneltotal = new Ext.Panel
	(
            {
            id: 'paneltotal',
            region: 'center',
            border:false,
            bodyStyle: 'padding:0px 7px 0px 7px',
            layout: 'column',
            frame:true,
            height:67,
            anchor: '100% 8.0000%',
            autoScroll:false,
            items:
            [
                
				
				{
					xtype: 'compositefield',
					anchor: '100%',
					labelSeparator: '',
					border:true,
					style:{'margin-top':'5px'},
					items: 
					[
						{
                   
							layout: 'form',
							style:{'text-align':'right','margin-left':'10px'},
							border: false,
							html: " *) Tag yang tidak di ceklis tidak bisa di cetak."
						},
		                {
                   
							layout: 'form',
							style:{'text-align':'right','margin-left':'220px'},
							border: false,
							html: " Total :"
						},
						/*{
							xtype: 'textfield',
							id: 'txtJumlahHargaEditData_viKasirigd',
							name: 'txtJumlahHargaEditData_viKasirigd',
							style:{'text-align':'right','margin-left':'390px'},
							width: 82,
							readOnly: true,
                        },*/
						{
							xtype: 'textfield',
							id: 'txtJumlah2EditData_viKasirigd',
							name: 'txtJumlah2EditData_viKasirigd',
							style:{'text-align':'right','margin-left':'240px'},
							width: 82,
							readOnly: true,
                        },
						
		                //-------------- ## --------------
					]
				},
				 {
					xtype: 'compositefield',
					anchor: '100%',
					labelSeparator: '',
					border:true,
					style:{'margin-top':'5px'},
					items: 
					[
						{xtype: 'tbspacer',height: 10, width:5},
						{
							xtype: 'checkbox',
							id: 'CekLapPilihSemuaKasirIGD',
							hideLabel:false,
							boxLabel: 'Pilih Semua Tag',
							checked: false,
							listeners: 
							{
								check: function()
								{
									if(Ext.getCmp('CekLapPilihSemuaKasirIGD').getValue()===true)
									{
										var items=Ext.getCmp('gridDTLTRKasirigd').getStore().data.items;
										for(var i=0,iLen=items.length; i<iLen; i++){
											dsTRDetailKasirigdKasirList.getRange()[i].set('TAG',true);
											
										}
									}
									else
									{
										var items=Ext.getCmp('gridDTLTRKasirigd').getStore().data.items;
										for(var i=0,iLen=items.length; i<iLen; i++){
											dsTRDetailKasirigdKasirList.getRange()[i].set('TAG',false);
											
										}
									}
								}
							}
						},
						
		                {
                            xtype: 'numberfield',
                            id: 'txtJumlahEditData_viKasirigd',
                            name: 'txtJumlahEditData_viKasirigd',
							style:{'text-align':'right','margin-left':'390px'},
							hidden:true,
                            width: 82,
							
                            //readOnly: true,
                        }
		                //-------------- ## --------------
					]
				}
            ]

        }
    )
 var GDtabDetailKasirigd = new Ext.Panel   
    (
        {
        id:'GDtabDetailKasirigd',
        region: 'center',
        activeTab: 0,
		height:500,
        anchor: '100% 100%',
        border:true,
        plain: true,
        defaults:
                {
                autoScroll: true
		},
                items: [GetDTLTRKasirigdGrid(),paneltotal],
                tbar:
                [
                    {
                        text: 'Bayar',
                        id: 'btnSimpanKasirigd',
                        tooltip: nmSimpan,
                        iconCls: 'save',
                        handler: function()
                        {
                            Datasave_KasirigdKasir(false);
                        }
                    },{
                        id: 'btnTransferKasirigd',
                        text: 'Transfer',
                        tooltip: nmEditData,
                        iconCls: 'Edit_Tr',
                        handler: function(sm, row, rec)
                        {
							if (Ext.getCmp('txtJumlah2EditData_viKasirigd').getValue()=='0' || Ext.getCmp('txtJumlah2EditData_viKasirigd').getValue()==0){
								ShowPesanWarningKasirigd('Pasien telah melunasi pembayaran','WARNING');
								x = 0;
							}
							else
							{
								TransferLookUp();
								Ext.Ajax.request ({
									url: baseURL + "index.php/main/GetPasienTranfer",
									params: {
										notr: Ext.get(txtNoMedrecDetransaksi).dom.value,
									},
									success: function(o) {	
										var tampungmedrec="";
										o.responseText
										if(o.responseText==""){
											ShowPesanWarningKasirigd('Pasien Belum terdaftar dirawat Inap ','Transfer');
											FormLookUpsdetailTRTransfer_IGD.close();
										} else{ 
											var tmphasil = o.responseText;
											var tmp = tmphasil.split("<>");
											Ext.get(txtTranfernoTransaksiKasirIGD).dom.value=tmp[3];
											Ext.get(dtpTanggalTransfertransaksiKasirIGD).dom.value=ShowDate(tmp[4]);
											Ext.get(txtTranfernomedrecKasirIGD).dom.value=tmp[5];
											tampungmedrec=tmp[5];
											Ext.get(txtTranfernamapasienKasirIGD).dom.value=tmp[2];
											Ext.get(txtTranferunitKasirIGD).dom.value=tmp[1];
											Trkdunit2=tmp[6];
											var kasir=tmp[7];
											Ext.get(txtTranferkelaskamarigd).dom.value=tmp[9];
											tmp_kodeunitkamarigd=tmp[8];
											tmp_kdspesialigd=tmp[10];
											tmp_nokamarigd=tmp[11];
											Trkdunit2=tmp[6];
											Ext.getCmp('dtpTanggalTransfertransaksibayarKasirIGD').setValue(Ext.getCmp('dtpTanggalDetransaksi').getValue());
											
											Ext.Ajax.request ({
												url: baseURL + "index.php/main/functionIGD/getDetailTransfer",
												params: {
													notr: notransaksi,
													kd_kasir:'igd'
												},
												success: function(o)
												{ 
													var cst = Ext.decode(o.responseText);
													Ext.get(txtjumlahbiayasal_KasirIGD).dom.value=formatCurrency(cst.totalpembayaran);
													Ext.get(txtpaid_KasirIGD).dom.value=formatCurrency(cst.totalsudahbayar);
													Ext.get(txtjumlahtranfer_KasirIGD).dom.value=formatCurrency(cst.sisa);
													Ext.get(txtsaldotagihan_KasirIGD).dom.value=formatCurrency(cst.sisa);
													SisaPembayaranKasirIGD=cst.sisa;
													
													if( tampungmedrec===undefined)
													{
														ShowPesanWarningKasirigd('Data Tidak dapat di transfer, hubungi Admin', 'Gagal');
														FormLookUpsdetailTRTransfer_IGD.close();

													}
													//).dom.value=formatCurrency(o.responseText);
												},
												failure: function(o) {
													ShowPesanWarningKasirigd('Data Tidak dapat di transfer,hubungi Admin', 'Gagal');
													FormLookUpsdetailTRTransfer_IGD.close();
												}
											});  
										}
									},
									failure: function(o) {
										/* var datax=tampungmedrec.substring(0, 2); */
										ShowPesanWarningKasirigd('Data Tidak dapat di transfer,hubungi Admin', 'Gagal');
										FormLookUpsdetailTRTransfer_IGD.close();
									}

								});
							}
							
                      
                        } //disabled :true
                    },
					mCombo_printer_kasirigd(),
                    {
                        xtype:'splitbutton',
                        text:'Cetak',
                        iconCls:'print',
                        id:'btnPrint_viDaftar',
                        handler:function()
                        {		
                        },
                        menu: new Ext.menu.Menu({
                        items: [
                            {
                                xtype: 'button',
                                text: 'Print Bill',
                                id: 'btnPrintBillIGD',
                                handler: function()
                                {
									if (Ext.getCmp('cbopasienorder_printer_kasirigd').getValue()==='' || Ext.getCmp('cbopasienorder_printer_kasirigd').getValue()===undefined)
									{
										ShowPesanWarningKasirigd('Pilih printer terlebih dahulu !', 'Cetak Data');
									}else{

										var url_laporan = baseURL + "index.php/laporan/lap_billing/";
										var params={
											no_transaksi    : rowSelectedPJIGD.data.NO_TRANSAKSI, 
											kd_kasir        : rowSelectedPJIGD.data.KD_KASIR,
										} ;
										// console.log(params);
										// var form = document.createElement("form");
										// form.setAttribute("method", "post");
										// form.setAttribute("target", "_blank");
										// form.setAttribute("action", url_laporan+"/preview_pdf");
										// var hiddenField = document.createElement("input");
										// hiddenField.setAttribute("type", "hidden");
										// hiddenField.setAttribute("name", "data");
										// hiddenField.setAttribute("value", Ext.encode(params));
										// form.appendChild(hiddenField);
										// document.body.appendChild(form);
										// form.submit();  
										// printbillIGD();
										GetDTLPreviewBilling(url_laporan+"preview_pdf", rowSelectedPJIGD.data.NO_TRANSAKSI, rowSelectedPJIGD.data.KD_KASIR);
									}
                                    
                                }
                            },
                            {
                                xtype: 'button',
                                text: 'Print Kwitansi',
                                id: 'btnPrintKwitansiIGD',
                                handler: function(){
									if(currentCOSTATUS_KasirIGD =='f'){
										// if(CurrentKdCustomer_KasirIGD == "0000000001"){
											if (Ext.getCmp('cbopasienorder_printer_kasirigd').getValue()==='' || Ext.getCmp('cbopasienorder_printer_kasirigd').getValue()===undefined){
												ShowPesanWarningKasirigd('Pilih printer terlebih dahulu !', 'Cetak Data');
											}else{
												// printkwitansiIGD();
												LookUpEditKwitansiPanel();
											}
										// } else{
											// ShowPesanWarningKasirigd('Cetak kwitansi hanya untuk pasien UMUM!', 'Cetak Data');
										// }
									}else{
										ShowPesanWarningKasirigd('Transaksi sudah ditutup, tidak dapat cetak kwitansi!', 'WARNING');
									}
                                }
                            }
                        ]
                        })
                    },
					' ','-',
					/* INI BUAT INFO SHIFT */
					{
						xtype: 'displayfield',				
						width: 60,								
						value: 'Shift Aktif',
						fieldLabel: 'Label',
						style: {'font-weight':'bold'}						
					},
					{
						xtype: 'displayfield',				
						width: 15,								
						value: ':',
						fieldLabel: 'Label',	
						style: {'font-weight':'bold'}							
					},						
					{
						xtype: 'displayfield',	
						id:'txtShiftAktif', 
						width: 60,								
						fieldLabel: 'Label',		
						style: {'font-weight':'bold'}	
					},	
		]
	}
    );
	
   
   
   var pnlTRKasirigd2 = new Ext.FormPanel
    (
        {
            id: 'PanelTRKasirigd2',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:500,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [	GDtabDetailKasirigd,
			
			
			]
        }
    );

	
   
   
    var FormDepanKasirigd = new Ext.Panel
	(
		{
		    id: 'FormDepanKasirigd',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
			height:500,
		    shadhow: true,
		    items: [pnlTRKasirigd,pnlTRKasirigd2,
			
				
			]
		}			
				

			

		
	);

    return FormDepanKasirigd
};






function TambahBarisKasirigd()
{
    var x=true;

    if (x === true)
    {
        var p = RecordBaruKasirrwj();
        dsTRDetailKasirigdKasirList.insert(dsTRDetailKasirigdKasirList.getCount(), p);
    };
};





function GetDTLTRHistoryGridIGD() 
{

    var fldDetail = ['NO_TRANSAKSI','TGL_BAYAR','DESKRIPSI','URUT','BAYAR','USERNAME','SHIFT','KD_USER','URAIAN','TAG'];
	
    dsTRDetailHistoryListIGD = new WebApp.DataStore({ fields: fldDetail })
		 
    var gridDTLTRHistoryIGD = new Ext.grid.EditorGridPanel
    (
        {
            title: 'History Bayar',
            stripeRows: true,
            store: dsTRDetailHistoryListIGD,
            border: false,
            columnLines: true,
            frame: false,
			anchor: '100% 25%',
			autoScroll:true,
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                            cellSelecteddeskripsi = dsTRDetailHistoryListIGD.getAt(row);
                            CurrentHistory.row = row;
                            CurrentHistory.data = cellSelecteddeskripsi;
                        }
                    }
                }
            ),
            cm: TRHistoryColumModel(),
			viewConfig:{forceFit: true},
			tbar:
			[                    
				{
					id:'btnHpsBrsKasirigd',
					text: 'Hapus Pembayaran',
					tooltip: 'Hapus Baris',
					iconCls: 'RemoveRow',
					//hidden :true,
					handler: function()
					{
							if (dsTRDetailHistoryListIGD.getCount() > 0 )
							{
								if (cellSelecteddeskripsi != undefined)
								{
										if(CurrentHistory != undefined)
										{
											HapusBarisDetailbayar();
										}
								}
								else
								{
										ShowPesanWarningKasirigd('Pilih record ','Hapus data');
								}
							}
							 //Ext.getCmp('btnEditKasirigd').disable();
							
					},
					disabled :true
				}	
			]
        }
		
		
    );
	
	

    return gridDTLTRHistoryIGD;
};

function TRHistoryColumModel() 
{
    return new Ext.grid.ColumnModel
    (//'','','','','',''
        [
           new Ext.grid.RowNumberer(),
            {
                id: 'colKdTransaksi',
                header: 'No. Transaksi',
                dataIndex: 'NO_TRANSAKSI',
                width:100,
					menuDisabled:true,
                hidden:false
            },
			{
                id: 'colTGlbayar',
                header: 'Tgl Bayar',
                dataIndex: 'TGL_BAYAR',
					menuDisabled:true,
				width:100,
				renderer: function(v, params, record)
                {
                   return ShowDate(record.data.TGL_BAYAR);

                }
                
            },
			{
                id: 'coleurutmasuk',
                header: 'urut Bayar',
                dataIndex: 'URUT',
				//hidden:true
                
            }
            
            ,
			{
                id: 'colePembayaran',
                header: 'Pembayaran',
                dataIndex: 'URAIAN',
				width:150,
				hidden:false
                
            }
			,
			{
                id: 'colJumlah',
                header: 'Jumlah',
				width:150,
				align :'right',
                dataIndex: 'BAYAR',
				hidden:false,
				renderer: function(v, params, record)
                {
                   return formatCurrency(record.data.BAYAR);

                }
                
            }
			,
			
			{
                id: 'coletglmasuk',
                header: 'tgl masuk',
                dataIndex: '',
				hidden:true
                
            }
            ,
            {
                id: 'colStatHistory',
                header: 'History',
                width:130,
				menuDisabled:true,
                dataIndex: '',
				hidden:true
              
				
				
              
            },
            {
                id: 'colPetugasHistory',
                header: 'Petugas',
                width:130,
				menuDisabled:true,
                dataIndex: 'USERNAME',	
				//hidden:true
                
				
				
              
            },{
				id: 'colStatusPrint',
				header: 'Print Kwitansi',
				dataIndex: 'TAG',
				sortable: true,
				width: 90,
				align:'center',
				renderer: function(value, metaData, record, rowIndex, colIndex, store)
				{
					 switch (value)
					 {
						 case 't':
								 metaData.css = 'StatusHijau'; // 
								 break;
						 case 'f':
								metaData.css = 'StatusMerah'; // rejected

								 break;
					 }
					 return '';
				}
			},
			

        ]
    )
};
function HapusBarisDetailbayar()
{
    if ( cellSelecteddeskripsi != undefined )
    {
        if (cellSelecteddeskripsi.data.NO_TRANSAKSI != '' && cellSelecteddeskripsi.data.URUT != '')
        {
			//'NO_TRANSAKSI','TGL_BAYAR','DESKRIPSI','URUT','BAYAR','USERNAME'
			var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan penghapusan:', function(btn, combo){
				if (btn == 'ok')
				{
					variablehistori=combo;
					if (variablehistori!='')
					{
						DataDeleteKasirigdKasirDetail();
						Ext.getCmp('btnTutupTransaksiKasirigd').disable();
						Ext.getCmp('btnHpsBrsKasirigd').disable();
					}
					else
					{
						ShowPesanWarningKasirigd('Silahkan isi alasan terlebih dahulu','Keterangan');
					}
				}
						
			});
											

												
                                         
                                
               
        }
        else
        {
            dsTRDetailHistoryListIGD.removeAt(CurrentHistory.row);
        };
    }
};

function DataDeleteKasirigdKasirDetail()
{
    Ext.Ajax.request
    (
        {
            url: baseURL + "index.php/main/functionIGD/deletedetail_bayar",
            params:  getParamDataDeleteKasirigdKasirDetail(),
			failure: function(o)
			{
				ShowPesanWarningKasirigd("Error. hapus baris pembayaran, Hubungi Admin!",'WARNING');
				loadMask.hide();
			},
            success: function(o)
            {
			//	RefreshDatahistoribayar(Kdtransaksi);
				RefreshDataFilterKasirIgdKasir();
				 RefreshDatahistoribayar('0');
                var cst = Ext.decode(o.responseText);
                if (cst.success === true)
                {
                    ShowPesanInfoKasirigd('Berhasil menghapus baris pembayaran','Information');
					DataDeleteKasirigdKasirDetail_SQL()
					RefreshDataFilterKasirIgdKasir();
                }
                else if (cst.success === false && cst.pesan === 'LUNAS' )
                {
                    ShowPesanWarningKasirigd('Pembayaran tidak dapat dihapus karena sudah erdapat pembayaran pada transaksi ini', 'WARNING');
                }
                else
                {
                    ShowPesanWarningKasirigd('Gagal menghapus baris pembayaran!','WARNING');
                };
            }
        }
    )
};

function getParamDataDeleteKasirigdKasirDetail()
{
    var params =
    {

        TrKodeTranskasi: CurrentHistory.data.data.NO_TRANSAKSI,
		TrTglbayar:  CurrentHistory.data.data.TGL_BAYAR,
		Urut:  CurrentHistory.data.data.URUT,
		Jumlah:  CurrentHistory.data.data.BAYAR,
		Username :CurrentHistory.data.data.USERNAME,
		Shift: 1,//shift na dipatok hela ke ganti di phpna
		Shift_hapus: 1,//shift na dipatok hela ke ganti di phpna
		Kd_user: CurrentHistory.data.data.KD_USER,
		Tgltransaksi :tgltrans, 
        Kodepasein:kodepasien,
		NamaPasien:namapasien,
        KodeUnit: kodeunit,
        Namaunit:namaunit,
        Kodepay:kodepay,
		Kdcustomer:kdcustomeraa,
        Uraian:CurrentHistory.data.data.DESKRIPSI,
		KDkasir: '06',
		alasan:variablehistori
		
    };
	Kdtransaksi=CurrentHistory.data.data.NO_TRANSAKSI;
    return params
};



function GetDTLTRKasirigdGrid() 
{
    var fldDetail = ['KD_PRODUK','DESKRIPSI','DESKRIPSI2','KD_TARIF','HARGA','QTY','DESC_REQ','TGL_BERLAKU','NO_TRANSAKSI','URUT','DESC_STATUS','TGL_TRANSAKSI','BAYARTR','DISCOUNT','PIUTANG','TAG'];
	
dsTRDetailKasirigdKasirList = new WebApp.DataStore({ fields: fldDetail })
//RefreshDataKasirigdKasirDetail() ;
gridDTLTRKasirigd = new Ext.grid.EditorGridPanel
    (
        {
            title: 'Detail Pembayaran',
            stripeRows: true,
			id: 'gridDTLTRKasirigd',
            store: dsTRDetailKasirigdKasirList,
            border: false,
            columnLines: true,
            frame: false,
            anchor: '100%',
			height:200,
            autoScroll:true,
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        
                    }
                }
            ),
            cm: TRKasirRawatJalanColumModel()
        }
		
		
    );
	
	

    return gridDTLTRKasirigd;
};

function TRKasirRawatJalanColumModel() 
{
    return new Ext.grid.ColumnModel
    (
        [
            new Ext.grid.RowNumberer(),
            {
                id: 'coleskripsiKasirigd',
                header: 'Uraian',
                dataIndex: 'DESKRIPSI2',
                width:250,
				menuDisabled:true,
				hidden :true
                
            },{
                id: 'coltagKasirigd',
                header:'Tag',
                dataIndex: 'TAG',
                width:30,
				xtype:'checkcolumn',
				listeners:{
					'checkchange':function(a,b){
						console.log(a);
						alert();
					},
					'change':function(a,b){
						console.log(a);
						alert();
					},'click':function(a,b){
						console.log(a);
						alert();
					}
				},
				'checkchange':function(a,b){
						console.log(a);
						alert();
					},
				handler:function(){
					alert();
				},
				editor: new Ext.form.Checkbox({
					listeners:{
						change:function(a,b){
							console.log(a);
							alert();
						}
					},
				})
			},
            {
                id: 'colKdProduk',
                header: 'Kode Produk',
                dataIndex: 'KD_PRODUK',
                width:100,
				menuDisabled:true,
                hidden:true
            },
            {
                id: 'colDeskripsiKasirigd',
                header:'Nama Produk',
                dataIndex: 'DESKRIPSI',
                sortable: false,
                hidden:false,
				menuDisabled:true,
                width:250
                
            },
			   {
                id: 'colURUTKasirigd',
                header:'Urut',
                dataIndex: 'URUT',
                sortable: false,
                hidden:true,
				menuDisabled:true,
                width:250
                
            }
			,
            {
               header: 'Tanggal Transaksi',
               dataIndex: 'TGL_TRANSAKSI',
               width: 130,
			    hidden:true,
			   menuDisabled:true,
               renderer: function(v, params, record)
                    {
                        
                        return ShowDate(record.data.TGL_TRANSAKSI);
                    }
            },
            {
                id: 'colQtyKasirigd',
                header: 'Qty',
                width:91,
				align: 'right',
				menuDisabled:true,
                dataIndex: 'QTY',
             
				
				
              
            },
            {
                id: 'colHARGAKasirigd',
                header: 'Harga',
				align: 'right',
				hidden: false,
				menuDisabled:true,
                dataIndex: 'HARGA',
                width:100,
				
				renderer: function(v, params, record) 
							{
							return formatCurrency(record.data.HARGA);
							
							}	
            },

            {
                id: 'colPiutangKasirigd',
                header: 'Puitang',
                width:80,
                dataIndex: 'PIUTANG',
				align: 'right',
				//hidden: false,
			
				 editor: new Ext.form.TextField
                (
                    {
                        id:'fieldcolPuitangigd',
                        allowBlank: true,
                        enableKeyEvents : true,
                        width:30,
						
                    }
                )
             ,
				renderer: function(v, params, record) 
							{
							
							//getTotalDetailProduk();
						
								return formatCurrency(record.data.PIUTANG);
							
						
							
							
							}
            },
			{
                id: 'colTunaiKasirigd',
                header: 'Tunai',
                width:80,
                dataIndex: 'BAYARTR',
				align: 'right',
				hidden: false,
							 editor: new Ext.form.TextField
								(
									{
										id:'fieldcolTunaiigd',
										allowBlank: true,
										enableKeyEvents : true,
										width:30,
										
									}
								),
				renderer: function(v, params, record) 
							{
							//getTotalHargaDetailProduk();
							getTotalDetailProduk();
					
							return formatCurrency(record.data.BAYARTR);
							
							
							}
				
            },
			{
                id: 'colDiscountKasirigd',
                header: 'Discount',
                width:80,
                dataIndex: 'DISCOUNT',
				align: 'right',
				hidden: false,
				editor: new Ext.form.TextField
								(
									{
										id:'fieldcolDiscountigd',
										allowBlank: true,
										enableKeyEvents : true,
										width:30,
										
									}
								)
				,
					renderer: function(v, params, record) 
							{
							return formatCurrency(record.data.DISCOUNT);
							
							
							}
							 
				
            }

        ]
    )
};




function RecordBaruKasirrwj()
{

	var p = new mRecordKasirrwj
	(
		{
			'DESKRIPSI2':'',
			'KD_PRODUK':'',
		    'DESKRIPSI':'', 
		    'KD_TARIF':'', 
		    'HARGA':'',
		    'QTY':'',
		    'TGL_TRANSAKSI':tanggaltransaksitampung, 
		    'DESC_REQ':'',
		    'KD_TARIF':'',
		    'URUT':''
		}
	);
	
	return p;
};
function TRKasirigdInit(rowdata)
{
    AddNewKasirigdKasir = false;
	vkd_unit = rowdata.KD_UNIT;
	RefreshDataKasirigdKasirDetail(rowdata.NO_TRANSAKSI);
	Ext.get('txtNoTransaksiKasirigdKasir').dom.value = rowdata.NO_TRANSAKSI;
	tanggaltransaksitampung = rowdata.TANGGAL_TRANSAKSI;
    Ext.get('dtpTanggalDetransaksi').dom.value = nowCurrentDateKasirIGD;
	Ext.get('txtNoMedrecDetransaksi').dom.value= rowdata.KD_PASIEN;
	Ext.get('txtNamaPasienDetransaksi').dom.value = rowdata.NAMA;
	Ext.get('cboPembayaran').dom.value= rowdata.KET_PAYMENT;
	Ext.get('cboJenisByr').dom.value= rowdata.CARA_BAYAR; // take the displayField value 
	loaddatastorePembayaran(rowdata.JENIS_PAY);
	vkode_customer = rowdata.KD_CUSTOMER;
	tampungtypedata=0;
	tapungkd_pay=rowdata.KD_PAY;
	console.log(rowdata);
	console.log(rowdata.KD_PAY);
	jenispay=1;
	var vkode_customer = rowdata.LUNAS;
	showCols(Ext.getCmp('gridDTLTRKasirigd'));
	hideCols(Ext.getCmp('gridDTLTRKasirigd'));
	vflag= rowdata.FLAG;
	tgltrans=rowdata.TANGGAL_TRANSAKSI;
	kodepasien=rowdata.KD_PASIEN;
	namapasien=rowdata.NAMA;
	kodeunit=rowdata.KD_UNIT;
	namaunit=rowdata.NAMA_UNIT;
	kodepay=rowdata.KD_PAY;
	kdcustomeraa=rowdata.KD_CUSTOMER;
	notransaksi=rowdata.NO_TRANSAKSI;
	CurrentKdCustomer_KasirIGD = rowdata.KD_CUSTOMER;
	Ext.Ajax.request(
	{
	   
	    url: baseURL + "index.php/main/getcurrentshift",
		 params: {
	        
	        command: '0',
		
	    },
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
	    success: function(o) {
	
			tampungshiftsekarang=o.responseText




	    }
	
	});

	/* INI BUAT INFO SHIFT */
	Ext.Ajax.request({
									   
		url: baseURL + "index.php/main/functionShift/getshiftIGD",
		 params: {
			
			command: '0',
		},
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
		success: function(o) {
			var shift_tampung = o.responseText;
			Ext.getCmp('txtShiftAktif').setValue(shift_tampung);
		}
	
	});
	
};

function mEnabledKasirigdCM(mBol)
{

	 Ext.get('btnLookupKasirigd').dom.disabled=mBol;
	 Ext.get('btnHpsBrsKasirigd').dom.disabled=mBol;
};


///---------------------------------------------------------------------------------------///
function KasirigdAddNew() 
{
    AddNewKasirigdKasir = true;
	Ext.get('txtNoTransaksiKasirigdKasir').dom.value = '';
    Ext.get('dtpTanggalDetransaksi').dom.value = nowTglTransaksiIGD.format('d/M/Y');
	Ext.get('txtNoMedrecDetransaksi').dom.value='';
	Ext.get('txtNamaPasienDetransaksi').dom.value = '';
	Ext.get('cboStatus_viKasirigdKasir').dom.value= ''
	rowSelectedKasirigdKasir=undefined;
	dsTRDetailKasirigdKasirList.removeAll();
	mEnabledKasirigdCM(false);
	

};

function RefreshDataKasirigdKasirDetail(no_transaksi) 
{
    var strKriteriaKasirigd='';
   
    strKriteriaKasirigd = 'no_transaksi = ~' + no_transaksi + '~ and kd_kasir = ~06~';

    dsTRDetailKasirigdKasirList.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'tgl_transaksi',
			    //Sort: 'tgl_transaksi',
			    Sortdir: 'ASC',
			    target: 'ViewDetailbayarIGD',
			    param: strKriteriaKasirigd
			}
		}
	);
    return dsTRDetailKasirigdKasirList;
};

function RefreshDataKasirigdKasirDetail_no_tag(no_transaksi) 
{
    var strKriteriaKasirigd='';
   
    strKriteriaKasirigd = 'no_transaksi = ~' + no_transaksi + '~ and kd_kasir = ~06~';

    dsTRDetailKasirigdKasirList.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'tgl_transaksi',
			    //Sort: 'tgl_transaksi',
			    Sortdir: 'ASC',
			    target: 'ViewDetailbayarIGD_no_tag',
			    param: strKriteriaKasirigd
			}
		}
	);
    return dsTRDetailKasirigdKasirList;
};


function RefreshDatahistoribayar(no_transaksi) 
{
    var strKriteriaKasirigd='';
   
    strKriteriaKasirigd = 'no_transaksi= ~' + no_transaksi + '~ and kd_kasir = ~06~';

    dsTRDetailHistoryListIGD.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'tgl_transaksi',
			    //Sort: 'tgl_transaksi',
			    Sortdir: 'ASC',
			    target: 'ViewHistoryBayarIGD',
			    param: strKriteriaKasirigd
			}
		}
	);
    return dsTRDetailHistoryListIGD;
};


//---------------------------------------------------------------------------------------///



//---------------------------------------------------------------------------------------///
function getParamDetailTransaksiKasirigd() 
{
	if(tampungtypedata==='')
	{
		tampungtypedata=0;
	};
	var kdpay='';
	if(Ext.getCmp('cboPembayaran').getValue() == ''){
		kdpay=Ext.get('cboPembayaran').getValue();
	} else{
		kdpay=Ext.getCmp('cboPembayaran').getValue();
	}
	
	var params =
	{
		kdUnit : vkd_unit,
		TrKodeTranskasi: Ext.get('txtNoTransaksiKasirigdKasir').getValue(),
		Tgl: CurrentTglTransaksi_KasirIGD,
		Shift: tampungshiftsekarang,
		Flag:vflag,
		bayar: tapungkd_pay,
		Typedata: tampungtypedata,
		Totalbayar:getTotalDetailProduk(),
		List:getArrDetailTrKasirigd(),
		JmlField: mRecordKasirigd.prototype.fields.length-4,
		JmlList:GetListCountDetailTransaksi(),
		UrutMasuk:CurrentURUT_MASUKIGD,
		KdCustomer:CurrentKdCustomer_KasirIGD,
		KdPay:kdpay,
		TglBayar:Ext.get('dtpTanggalDetransaksi').dom.value,
		Hapus:1,
		Ubah:0
	};
    return params
};

function paramUpdateTransaksi()
{
 var params =
	{
            TrKodeTranskasi: cellSelectedtutup_kasirigd,
            KDUnit : kdUnit,
			kd_unit : CurrentKdUnit_KasirIGD,
			urut_masuk:CurrentUrutMasuk_KasirIGD,
			tgl_transaksi:CurrentTglMasuk_KasirIGD,
			kd_pasien:CurrentKdPasien_KasirIGD,
	};
	return params;
};

function GetListCountDetailTransaksi()
{
	
	var x=0;
	for(var i = 0 ; i < dsTRDetailKasirigdKasirList.getCount();i++)
	{
		if (dsTRDetailKasirigdKasirList.data.items[i].data.KD_PRODUK != '' || dsTRDetailKasirigdKasirList.data.items[i].data.DESKRIPSI  != '')
		{
			x += 1;
		};
	}
	return x;
	
};

function getTotalDetailProduk()
{
var TotalProduk=0;
var bayar;
	var tampunggrid ;
	var x='';
	for(var i = 0 ; i < dsTRDetailKasirigdKasirList.getCount();i++)
	{		

			var recordterakhir;
		 
			//alert(TotalProduk);
		if (tampungtypedata==0)
		{
		tampunggrid = parseInt(dsTRDetailKasirigdKasirList.data.items[i].data.BAYARTR);
			
			//recordterakhir= tampunggrid
			//TotalProduk.toString().replace(/./gi, "");
			TotalProduk+= tampunggrid
		  
			recordterakhir=TotalProduk
			Ext.get('txtJumlah2EditData_viKasirigd').dom.value=formatCurrency(recordterakhir);
		}
		if(tampungtypedata==3)
		{
			tampunggrid = parseInt(dsTRDetailKasirigdKasirList.data.items[i].data.PIUTANG);
			//TotalProduk.toString().replace(/./gi, "");
			//recordterakhir=tampunggrid
			TotalProduk+=tampunggrid
		     recordterakhir=TotalProduk
			Ext.get('txtJumlah2EditData_viKasirigd').dom.value=formatCurrency(recordterakhir);
		}
		if(tampungtypedata==1)
		{
		   tampunggrid = parseInt(dsTRDetailKasirigdKasirList.data.items[i].data.DISCOUNT);
		//	TotalProduk.toString().replace(/./gi, "");
			//recordterakhir=dsTRDetailKasirigdKasirList.data.items[i].data.DISCOUNT
			TotalProduk+=tampunggrid
		    recordterakhir=TotalProduk
			Ext.get('txtJumlah2EditData_viKasirigd').dom.value=formatCurrency(recordterakhir);
		}
	
		
		
	}	
	bayar = Ext.get('txtJumlah2EditData_viKasirigd').getValue();
	return bayar;
};

function getTotalHargaDetailProduk()
{
var TotalProduk=0;
var bayar;
	var tampunggrid ;
	var x='';
	for(var i = 0 ; i < dsTRDetailKasirigdKasirList.getCount();i++)
	{		

			var recordterakhir;
		 
			//alert(TotalProduk);
		if (tampungtypedata==0)
		{
			//console.log(dsTRDetailKasirigdKasirList.data.items[i].data);
			tampunggrid = parseInt(dsTRDetailKasirigdKasirList.data.items[i].data.HARGA);
			
			//recordterakhir= tampunggrid
			//TotalProduk.toString().replace(/./gi, "");
			TotalProduk+= tampunggrid
		  
			recordterakhir=TotalProduk
			Ext.get('txtJumlahHargaEditData_viKasirigd').dom.value=formatCurrency(recordterakhir);
		}
		if(tampungtypedata==3)
		{
			tampunggrid = parseInt(dsTRDetailKasirigdKasirList.data.items[i].data.PIUTANG);
			//TotalProduk.toString().replace(/./gi, "");
			//recordterakhir=tampunggrid
			TotalProduk+=tampunggrid
		     recordterakhir=TotalProduk
			Ext.get('txtJumlahHargaEditData_viKasirigd').dom.value=formatCurrency(recordterakhir);
		}
		if(tampungtypedata==1)
		{
		   tampunggrid = parseInt(dsTRDetailKasirigdKasirList.data.items[i].data.DISCOUNT);
		//	TotalProduk.toString().replace(/./gi, "");
			//recordterakhir=dsTRDetailKasirigdKasirList.data.items[i].data.DISCOUNT
			TotalProduk+=tampunggrid
		    recordterakhir=TotalProduk
			Ext.get('txtJumlahHargaEditData_viKasirigd').dom.value=formatCurrency(recordterakhir);
		}
	
		
		
	}	
	bayar = Ext.get('txtJumlahHargaEditData_viKasirigd').getValue();
	return bayar;
};







function getArrDetailTrKasirigd()
{
	var x='';
	for(var i = 0 ; i < dsTRDetailKasirigdKasirList.getCount();i++)
	{
		if (dsTRDetailKasirigdKasirList.data.items[i].data.KD_PRODUK != '' && dsTRDetailKasirigdKasirList.data.items[i].data.DESKRIPSI != '')
		{
			var y='';
			var z='@@##$$@@';
			
			y = 'URUT=' + dsTRDetailKasirigdKasirList.data.items[i].data.URUT
			y += z + dsTRDetailKasirigdKasirList.data.items[i].data.KD_PRODUK
			y += z + dsTRDetailKasirigdKasirList.data.items[i].data.QTY
			y += z +dsTRDetailKasirigdKasirList.data.items[i].data.HARGA
			y += z +dsTRDetailKasirigdKasirList.data.items[i].data.KD_TARIF
			y += z +dsTRDetailKasirigdKasirList.data.items[i].data.URUT
			y += z +dsTRDetailKasirigdKasirList.data.items[i].data.BAYARTR
			y += z +dsTRDetailKasirigdKasirList.data.items[i].data.PIUTANG
			y += z +dsTRDetailKasirigdKasirList.data.items[i].data.DISCOUNT
			y += z +dsTRDetailKasirigdKasirList.data.items[i].data.TGL_TRANSAKSI
			
			
			if (i === (dsTRDetailKasirigdKasirList.getCount()-1))
			{
				x += y 
			}
			else
			{
				x += y + '##[[]]##'
			};
		};
	}	
	
	return x;
};


function getItemPanelInputKasir(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 0px',
		border:true,
		height:110,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
			    layout: 'form',
			    border: false,
			    items:
				[
					getItemPanelNoTransksiigdKasir(lebar),
					getItemPanelmedreckasir(lebar),
					getItemPanelUnitKasir(lebar)	
				]
			}
		]
	};
    return items;
};



function getItemPanelUnitKasir(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
				mComboJenisByrView()
				
				]
			},{
			    columnWidth: .60,
			    layout: 'form',
				labelWidth:0.9,
			    border: false,
			    items:
				[mComboPembayaran()
	
				]
			}
		]
	}
    return items;
};



function loaddatastorePembayaran(jenis_pay)
{
          dsComboBayar.load
                (
                    {
                     params:
							{
								Skip: 0,
								Take: 1000,
								Sort: 'nama',
								Sortdir: 'ASC',
								target: 'ViewComboBayar',
								param: 'jenis_pay=~'+ jenis_pay+ '~ order by uraian asc'
							}
                   }
                )
}

function mComboPembayaran()
{
    var Field = ['KD_PAY','JENIS_PAY','PAYMENT'];

    dsComboBayar = new WebApp.DataStore({fields: Field});


    var cboPembayaran = new Ext.form.ComboBox
	(
		{
		    id: 'cboPembayaran',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
            forceSelection: true,
		    //emptyText:'Pilih Pembayaran...',
		    labelWidth:80,
		    align: 'Right',
		    store: dsComboBayar,
		    valueField: 'KD_PAY',
		    displayField: 'PAYMENT',
            anchor: '100%',
			listeners:
			{
			    'select': function(a, b, c) 
				{
				
						tapungkd_pay=b.data.KD_PAY;
						//getTotalDetailProduk();
					
			   
				},
				  

			}
		}
	);

    return cboPembayaran;
};


function mComboJenisByrView() 
{
	var Field = ['JENIS_PAY','DESKRIPSI','TYPE_DATA'];

    dsJenisbyrView = new WebApp.DataStore({ fields: Field });
    dsJenisbyrView.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
		
                            Sort: 'jenis_pay',
			    Sortdir: 'ASC',
			    target: 'ComboJenis',
			  
			}
		}
	);
	
    var cboJenisByr = new Ext.form.ComboBox
	(
		{
	
		    id: 'cboJenisByr',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
			fieldLabel: 'Pembayaran      ',

		    align: 'Right',
		    anchor:'100%',
		    store: dsJenisbyrView,
		    valueField: 'JENIS_PAY',
		    displayField: 'DESKRIPSI',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
					
						 loaddatastorePembayaran(b.data.JENIS_PAY);
						 tampungtypedata=b.data.TYPE_DATA;
						 jenispay=b.data.JENIS_PAY;
						 showCols(Ext.getCmp('gridDTLTRKasirigd'));
						 hideCols(Ext.getCmp('gridDTLTRKasirigd'));
						getTotalDetailProduk();
						Ext.get('cboPembayaran').dom.value='Pilih Pembayaran...';
				
					
			   
				},
				  

			}
		}
	);
	
    return cboJenisByr;
};
    function hideCols (grid)
	{
		if (tampungtypedata==3)
		{
			grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('BAYARTR'), true);
			grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('DISCOUNT'), true);
		}
		else if(tampungtypedata==0)
		{
			grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('PIUTANG'), true);
			grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('DISCOUNT'), true);
		}
		else if(tampungtypedata==1)
		{
			grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('PIUTANG'), true);
			grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('BAYARTR'), true);
			
		}
	};
      function showCols (grid) {   
	  	if (tampungtypedata==3)
			{
			grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('PIUTANG'), false);
		
			}
			else if(tampungtypedata==0)
			{
				grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('BAYARTR'), false);
			}
			else if(tampungtypedata==1)
			{
				grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('DISCOUNT'), false);
			}
	
	};



function getItemPanelDokter(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel:'Dokter  ',
					    name: 'txtKdDokter',
					    id: 'txtKdDokter',
						//emptyText:nmNomorOtomatis,
						readOnly:true,
					    anchor: '99%'
					},{
					    xtype: 'textfield',
					    fieldLabel: 'Kelompok Pasien',
						//hideLabel:true,
						readOnly:true,
					    name: 'txtCustomer',
					    id: 'txtCustomer',
					    anchor: '99%',
						listeners: 
						{ 
							
						}
					}
				]
			},
			{
			    columnWidth: .600,
			    layout: 'form',
			    border: false,
				labelWidth:2,
			    items:
				[
					{
						xtype: 'textfield',
					   // fieldLabel:'Unit : ',
					    name: 'txtNamaDokter',
					    id: 'txtNamaDokter',
						//emptyText:nmNomorOtomatis,
						readOnly:true,
					    anchor: '100%'
					},
					{
						xtype: 'textfield',
					   // fieldLabel:'Unit : ',
					    name: 'txtKdUrutMasuk',
					    id: 'txtKdUrutMasuk',
						//emptyText:nmNomorOtomatis,
						readOnly:true,
						hidden:true,
					    anchor: '100%'
					}
				]
			}
		]
	}
    return items;
};

function getItemPanelNoTransksiigdKasir(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel:  'No. Transaksi ',
					    name: 'txtNoTransaksiKasirigdKasir',
					    id: 'txtNoTransaksiKasirigdKasir',
						emptyText:nmNomorOtomatis,
						readOnly:true,
					    anchor: '99%'
					}
				]
			},
			{
			    columnWidth: .60,
			    layout: 'form',
			    border: false,
				labelWidth:120,
			    items:
				[
					{
					    xtype: 'datefield',
					    fieldLabel: 'Tanggal pembayaran',
					    id: 'dtpTanggalDetransaksi',
					    name: 'dtpTanggalDetransaksi',
					    format: 'd/M/Y',
						readOnly : false,
					    value: now,
					    anchor: '100%'
					}
				]
			},
		]
	}
    return items;
};


function getItemPanelmedreckasir(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel:   'No. Medrec ',
					    name: 'txtNoMedrecDetransaksi',
					    id: 'txtNoMedrecDetransaksi',
						readOnly:true,
					    anchor: '99%',
					    listeners: 
						{ 
							
						}
					}
				]
			},
			{
			    columnWidth: .60,
			    layout: 'form',
			    border: false,
				labelWidth:2,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: '',
						//hideLabel:true,
						readOnly:true,
					    name: 'txtNamaPasienDetransaksi',
					    id: 'txtNamaPasienDetransaksi',
					    anchor: '100%',
						listeners: 
						{ 
							
						}
					}
				]
			}
		]
	}
    return items;
};


function RefreshDataKasirigdKasir() 
{
	loadMask.show();
    dsTRKasirigdKasirList.load
    (
        {
            params:
            {
                Skip: 0,
                Take: selectCountKasirigdKasir,
                Sort: 'tgl_transaksi',
                //Sort: 'tgl_transaksi',
                Sortdir: 'ASC',
                target:'ViewKasirIGD',
                param : ''
            },	
			callback:function(){
				loadMask.hide();
			}
        }
    );
	
    rowSelectedKasirigdKasir = undefined;
    return dsTRKasirigdKasirList;
};

function refeshKasirIgdKasir()
{
	loadMask.show();
	dsTRKasirigdKasirList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountKasirigdKasir, 
					//Sort: 'no_transaksi',
                     Sort: '',
					//Sort: 'no_transaksi',
					Sortdir: 'ASC', 
					target:'ViewKasirIGD',
					param : ''
				},	
				callback:function(){
					loadMask.hide();
				}		
			}
		);   
		return dsTRKasirigdKasirList;
}

function RefreshDataFilterKasirIgdKasir() 
{
	var KataKunci='';
	loadMask.show();

	if (Ext.get('dtpTglAwalFilterKasirigd').getValue() != ''){
		if (KataKunci == ''){                      
			KataKunci = " and (tgl_transaksi between ~" + Ext.get('dtpTglAwalFilterKasirigd').getValue() + "~ and ~" + Ext.get('dtpTglAkhirFilterKasirigd').getValue() + "~)";
		}else{
			KataKunci = " and (tgl_transaksi between ~" + Ext.get('dtpTglAwalFilterKasirigd').getValue() + "~ and ~" + Ext.get('dtpTglAkhirFilterKasirigd').getValue() + "~)";
		};
	};

	if (Ext.get('txtFilterNomedrec').getValue() != '')
	{
		if (KataKunci == ''){
			KataKunci = ' and   LOWER(kode_pasien) like  LOWER( ~' + Ext.get('txtFilterNomedrec').getValue() + '%~)';
		}else{
			KataKunci += ' and  LOWER(kode_pasien) like  LOWER( ~' + Ext.get('txtFilterNomedrec').getValue() + '%~)';
		};
	
	};
	
	if (Ext.get('TxtFilterGridDataView_NAMA_viKasirigdKasir').getValue() != '')
	{
		if (KataKunci == '')
		{
			KataKunci = ' and   LOWER(nama) like  LOWER( ~%' + Ext.get('TxtFilterGridDataView_NAMA_viKasirigdKasir').getValue() + '%~)';
		
		}
		else
		{
		
			KataKunci += ' and  LOWER(nama) like  LOWER( ~%' + Ext.get('TxtFilterGridDataView_NAMA_viKasirigdKasir').getValue() + '%~)';
		};
		
	};
	
	
	if (Ext.get('cboUNIT_viKasirigdKasir').getValue() != '' && Ext.get('cboUNIT_viKasirigdKasir').getValue() != 'All'){
		if (KataKunci == ''){
			KataKunci = ' and  LOWER(nama_unit)like  LOWER(~%' + Ext.get('cboUNIT_viKasirigdKasir').getValue() + '%~)';
		}else{
		
			KataKunci += ' and LOWER(nama_unit) like  LOWER(~%' + Ext.get('cboUNIT_viKasirigdKasir').getValue() + '%~)';
		};
	};
	
	
	if (KataKunci == ''){
		if (Ext.get('cboStatus_viKasirigdKasir').getValue() == 'Lunas' || Ext.get('cboStatus_viKasirigdKasir').getValue() == 1){
			KataKunci = '  AND lunas = ~true~';
		}else if(Ext.get('cboStatus_viKasirigdKasir').getValue() == 'Belum Lunas' || Ext.get('cboStatus_viKasirigdKasir').getValue() == 2){
			KataKunci = '  AND lunas = ~false~';
		}
	}else{
		if (Ext.get('cboStatus_viKasirigdKasir').getValue() == 'Lunas' || Ext.get('cboStatus_viKasirigdKasir').getValue() == 1){
			KataKunci += '  AND lunas = ~true~';
		}else if(Ext.get('cboStatus_viKasirigdKasir').getValue() == 'Belum Lunas' || Ext.get('cboStatus_viKasirigdKasir').getValue() == 2){
			KataKunci += '  AND lunas = ~false~';
		}
	}
	
	
    
    if (KataKunci != undefined ||KataKunci != '' ) 
    {  
		dsTRKasirigdKasirList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountKasirigdKasir, 
					//Sort: 'no_transaksi',
					Sort: 'tgl_transaksi',
					//Sort: 'no_transaksi',
					Sortdir: 'ASC', 
					target:'ViewKasirIGD',
					param : KataKunci
				},	
				callback:function(){
					loadMask.hide();
				}		
			}
		);   
    }else{
		dsTRKasirigdKasirList.load
			(
				{ 
					params:  
					{   
						Skip: 0, 
						Take: selectCountKasirrwjKasir, 
						//Sort: 'no_transaksi',
	                                        Sort: 'tgl_transaksi',
						//Sort: 'no_transaksi',
						Sortdir: 'ASC', 
						target:'ViewKasirIGD',
						param : ''
					},	
					callback:function(){
						loadMask.hide();
					}		
				}
			);   
	};
	return dsTRKasirigdKasirList;
};


/* function Datasave_KasirigdKasir(mBol) 
{	
	if (ValidasiEntryCMKasirigd(nmHeaderSimpanData,false) == 1 )
	{
			Ext.Ajax.request
			 (
				{
					//url: "./Datapool.mvc/CreateDataObj",
					url: baseURL + "index.php/main/functionIGD/savepembayaran",
					params: getParamDetailTransaksiKasirigd(),
					failure: function(o)
					{
					ShowPesanWarningKasirigd('Data Belum Simpan segera Hubungi Admin', 'Gagal');
					 RefreshDataFilterKasirIgdKasir();
					},	
					success: function(o) 
					{
						RefreshDatahistoribayar(0);
						RefreshDataKasirigdKasirDetail(Ext.get('txtNoTransaksiKasirigdKasir').dom.value);
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							
							ShowPesanInfoKasirigd('Data berhasil disimpan', 'Information');
							//FormLookUpsdetailTRKasirigd.close();
							//RefreshDataKasirigdKasir();
							
							if(mBol === false)
							{
									 RefreshDataFilterKasirIgdKasir();
							};
						}
						else 
						{
								ShowPesanWarningKasirigd('Data Belum Simpan segera Hubungi Admin', 'Gagal');
						};
					}
				}
			)
		
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
	
}; */

function BatalTransaksi(mBol)
{

	Ext.Ajax.request
	 (
		{
			//url: "./Datapool.mvc/CreateDataObj",
			url: baseURL + "index.php/main/functionIGD/batal_transaksi",
			params: getParamDetailBatalTransaksiIGD(),
			failure: function(o)
			{
			 ShowPesanWarningKasirigd(Ext.decode(o.responseText), 'Gagal');
			// RefreshDataFilterKasirrwjKasir();
			},
			success: function(o)
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					ShowPesanInfoKasirigd('Batal transaksi berhasil','Information');
					//BatalTransaksi_SQL(cst.notrans,cst.notransbaru,cst.urutmasuk,cst.urutmasukbaru);
					RefreshDataFilterKasirIgdKasir();
					if(mBol === false)
					{
					//RefreshDataFilterKasirrwjKasir();
					};
					cellSelectedtutup_kasirigd='';
				}
				else
				{
					ShowPesanWarningKasirigd(Ext.decode(o.responseText), 'Gagal');
					cellSelectedtutup_kasirigd='';
				};
			}
		}
	)
};
function getParamDetailBatalTransaksiIGD()
{
	if(tampungtypedata==='')
		{
		tampungtypedata=0;
		};
	var line = dsTRDetailHistoryListIGD.getCount();
	var totalhehe=0;
	for (var i = 0 ; i < line; i++)
	{
		totalhehe += parseInt(dsTRDetailHistoryListIGD.data.items[i].data.BAYAR);
		
	}
    var params =
	{
		kdPasien 	: kodepasien,
		noTrans 	: notransaksi,
		kdUnit 		: kodeunit,
		Jumlah		: totalhehe,
		kdDokter 	: kdokter_btl,
		tglTrans 	: tgltrans,
		kdCustomer	: kdcustomeraa,
		Keterangan	: variablebatalhistori_igd,
		
	};
    return params
};
function getParamDetailBukaTransaksiIGD()
{
	
    var params =
	{
		kdPasien 	: kodepasien,
		noTrans 	: notransaksi,
		kdUnit 		: kodeunit,
		tglTrans 	: tgltrans,
		urutMasuk 	: CurrentUrutMasuk_KasirIGD,
		Keterangan	: variablebukahistori_igd,
		
	};
    return params
};
function Datasave_KasirigdKasir(mBol) 
{	
	loadMask.show();
	if (ValidasiEntryCMKasirigd(nmHeaderSimpanData,false) == 1 )
	{
			Ext.Ajax.request
			 (
				{
					url: baseURL + "index.php/main/functionIGD/savepembayaran",
					params: getParamDetailTransaksiKasirigd(),
					failure: function(o)
					{
						ShowPesanWarningKasirigd('Data Belum Simpan segera Hubungi Admin', 'Gagal');
						RefreshDataFilterKasirIgdKasir();
					},	
					success: function(o) 
					{
						RefreshDatahistoribayar(0);
					
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoKasirigd(nmPesanSimpanSukses,nmHeaderSimpanData);
							loadMask.hide();

							/*--->>>>DICOMENT AGAR MEMUDAHKAN SAAT CETAK BILLING*/
							RefreshDataKasirigdKasirDetail_no_tag(Ext.get('txtNoTransaksiKasirigdKasir').dom.value);

							//Datasave_KasirigdKasir_SQL();
							// RefreshDataKasirigdKasir();
							RefreshDataFilterKasirIgdKasir();
							/* if(mBol === false)
							{ */
									 //RefreshDataFilterKasirIgdKasir();
							//};
						}
						else 
						{
								ShowPesanWarningKasirigd('Data Belum Simpan segera Hubungi Admin', 'Gagal');
						};
					}
				}
			) 
		
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
	
};


function UpdateBukatransaksi(mBol) 
{	
	Ext.Ajax.request
	({
		//url: "./Datapool.mvc/CreateDataObj",
		url: baseURL + "index.php/main/functionIGD/bukatransaksiigd",
		params: getParamDetailBukaTransaksiIGD(),
		failure: function(o)
		{
			ShowPesanWarningKasirigd('Data gagal tersimpan segera hubungi Admin', 'Gagal');
			RefreshDataFilterKasirIgdKasir();
		},	
		success: function(o) 
		{
			//RefreshDatahistoribayar(Ext.get('cellSelectedtutup_kasirigd);
		
			var cst = Ext.decode(o.responseText);
			if (cst.success === true) 
			{
				ShowPesanInfoKasirigd('Buka transaksi berhasil','Information');
				//UpdateTutuptransaksi_SQL();
				//RefreshDataKasirigdKasir();
				
					RefreshDataFilterKasirIgdKasir();
				
				cellSelectedtutup_kasirigd='';
			}
			else 
			{
				ShowPesanWarningKasirigd('Data gagal tersimpan segera hubungi Admin', 'Gagal');
				cellSelectedtutup_kasirigd='';
			};
		}
	})
};

function UpdateTutuptransaksi(mBol) 
{	
	Ext.Ajax.request
	({
		//url: "./Datapool.mvc/CreateDataObj",
		url: baseURL + "index.php/main/functionIGD/ubah_co_status_transksi",
		params: paramUpdateTransaksi(),
		failure: function(o)
		{
			ShowPesanWarningKasirigd('Data gagal tersimpan segera hubungi Admin', 'Gagal');
			RefreshDataFilterKasirIgdKasir();
		},	
		success: function(o) 
		{
			var cst = Ext.decode(o.responseText);
			if (cst.success === true) 
			{
				ShowPesanInfoKasirigd('Tutup transaksi berhasil','Information');
				//RefreshDataKasirigdKasir();
				if(mBol === false)
				{
					RefreshDataFilterKasirIgdKasir();
				};
				cellSelectedtutup_kasirigd='';
			}
			else 
			{
				ShowPesanWarningKasirigd('Data gagal tersimpan segera hubungi Admin', 'Gagal');
				cellSelectedtutup_kasirigd='';
			};
		}
	})
};

function ValidasiEntryCMKasirigd(modul,mBolHapus)
{
	var x = 1;
	if (Ext.get('txtJumlah2EditData_viKasirigd').getValue()==='0' || Ext.get('txtJumlah2EditData_viKasirigd').getValue()===0){
			loadMask.hide();
			ShowPesanWarningKasirigd('Pasien telah melunasi pembayaran!','WARNING');
			x = 0;
		}
	//console.log(Ext.get('txtJumlah2EditData_viKasirigd').getValue());
	if((Ext.get('txtNoTransaksiKasirigdKasir').getValue() == '') 
	
	|| (Ext.get('txtNoMedrecDetransaksi').getValue() == '') 
	|| (Ext.get('txtNamaPasienDetransaksi').getValue() == '')
	|| (Ext.get('dtpTanggalDetransaksi').getValue() == '') 
	|| dsTRDetailKasirigdKasirList.getCount() === 0 
	|| (Ext.get('cboPembayaran').getValue() == '') 
	||(Ext.get('cboPembayaran').getValue() == 'Pilih Pembayaran...') )
	{
		if (Ext.get('txtNoTransaksiKasirigdKasir').getValue() == '' && mBolHapus === true) 
		{
			loadMask.hide();
			ShowPesanWarningKasirigd(('Data transaksi tidak  boleh kosong!'), 'WARNING');
			x = 0;
		}
		else if (Ext.get('cboPembayaran').getValue() == ''||Ext.get('cboPembayaran').getValue() == 'Pilih Pembayaran...') 
		{
			loadMask.hide();
			ShowPesanWarningKasirigd(('Data pembayaran tidak  boleh kosong!'), 'WARNING');
			x = 0;
		}
		else if (Ext.get('txtNoMedrecDetransaksi').getValue() == '') 
		{
			loadMask.hide();
			ShowPesanWarningKasirigd(('Data no. medrec tidak boleh kosong!'), 'WARNING');
			x = 0;
		}
		else if (Ext.get('txtNamaPasienDetransaksi').getValue() == '') 
		{
			loadMask.hide();
			ShowPesanWarningKasirigd('Nama pasien belum terisi!', 'WARNING');
			x = 0;
		}
		else if (Ext.get('dtpTanggalDetransaksi').getValue() == '') 
		{
			loadMask.hide();
			ShowPesanWarningKasirigd(('Data tanggal kunjungan tidak boleh kosong!'), 'WARNING');
			x = 0;
		}
		//cboPembayaran
	
		else if (dsTRDetailKasirigdKasirList.getCount() === 0) 
		{
			loadMask.hide();
			ShowPesanWarningKasirigd('Data dalam tabel kosong!','WARNING');
			x = 0;
		}
	};
	if((convertTglTransaksiKasirIGD(Ext.getCmp('dtpTanggalDetransaksi').getValue()) < tgltrans) || (convertTglTransaksiKasirIGD(Ext.getCmp('dtpTanggalDetransaksi').getValue()) > convertTglTransaksiKasirIGD(now))){
		if(convertTglTransaksiKasirIGD(Ext.getCmp('dtpTanggalDetransaksi').getValue()) < tgltrans) {
			loadMask.hide();
			ShowPesanWarningKasirigd('Tanggal pembayaran tidak boleh kurang dari tanggal transaksi! Tanggal transaksi adalah '+ ShowDate(tgltrans)+'.','WARNING');
			x = 0;
		} else{
			loadMask.hide();
			ShowPesanWarningKasirigd('Tanggal pembayaran tidak boleh lebih dari tanggal hari ini! Tanggal transaksi adalah '+ ShowDate(tgltrans)+'.','WARNING');
			x = 0;
		}
	};
	return x;
};




function ShowPesanWarningKasirigd(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorKasirigd(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoKasirigd(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};

function printbillIGD()
{
    Ext.Ajax.request
    (
        {
				url: baseURL + "index.php/gawat_darurat/cetak_bill_kwitansi/cetak",
                // url: baseURL + "index.php/main/CreateDataObj",
                params: dataparamcetakbill_vikasirDaftarIGD(),
                success: function(o)
                {
                        var cst = Ext.decode(o.responseText);
                        if (cst.success === true)
                        {
                        	ShowPesanInfoKasirigd("Data sedang di cetak", "Information");
                        }else{
                        	ShowPesanWarningKasirigd("Data gagal di cetak", "Information");
                        }
                }
        }
    );
};

var tmpkdkasirIGD = '1';
function dataparamcetakbill_vikasirDaftarIGD()
{
    var paramscetakbill_vikasirDaftarIGD =
		{      
                    Table: 'DirectPrintingIgd',
                    no_transaksi: Ext.get('txtNoTransaksiKasirigdKasir').getValue(),
                    // No_TRans: Ext.get('txtNoTransaksiKasirigdKasir').getValue(),
					Medrec: Ext.get('txtNoMedrecDetransaksi').getValue(),
					//kdUnit: rowSelectedKasirigdKasir.data.KD_UNIT,
					kdUnit: CurrentKdUnit_KasirIGD,
					billing 	: true,
					kwitansi 	: false,
                    KdKasir : tmpkdkasirIGD,
                    JmlBayar: Ext.get('txtJumlah2EditData_viKasirigd').getValue(),
                    JmlDibayar: Ext.get('txtJumlahEditData_viKasirigd').getValue(),
					printer: Ext.getCmp('cbopasienorder_printer_kasirigd').getValue()
		};
	var urut=[];
	var kd_produk=[];
	var items=Ext.getCmp('gridDTLTRKasirigd').getStore().data.items;

    var data_item = []; 
	for(var i=0,iLen=items.length; i<iLen; i++){
		if(items[i].data.TAG==true){
	        var data = {};
			data.urut      = items[i].data.URUT;
			data.kd_produk = items[i].data.KD_PRODUK;
			data_item.push(data);
	    }
    }

	paramscetakbill_vikasirDaftarIGD['data_produk']=JSON.stringify(data_item);
    return paramscetakbill_vikasirDaftarIGD;
};
function dataparamcetakbill_vikasirDaftarIGD_()
{
    var paramscetakbill_vikasirDaftarIGD =
		{      
                    Table: 'DirectPrintingIgd',
                    No_TRans: Ext.get('txtNoTransaksiKasirigdKasir').getValue(),
					Medrec: Ext.get('txtNoMedrecDetransaksi').getValue(),
					//kdUnit: rowSelectedKasirigdKasir.data.KD_UNIT,
					kdUnit: CurrentKdUnit_KasirIGD,
                    KdKasir : tmpkdkasirIGD,
                    JmlBayar: Ext.get('txtJumlah2EditData_viKasirigd').getValue(),
                    JmlDibayar: Ext.get('txtJumlahEditData_viKasirigd').getValue(),
					printer: Ext.getCmp('cbopasienorder_printer_kasirigd').getValue()
		};
	console.log(Ext.getCmp('gridDTLTRKasirigd').getStore().data.items);
	var urut=[];
	var kd_produk=[];
	var items=Ext.getCmp('gridDTLTRKasirigd').getStore().data.items;
	for(var i=0,iLen=items.length; i<iLen; i++){
		if(items[i].data.TAG==true){
			urut.push(items[i].data.URUT);
			kd_produk.push(items[i].data.KD_PRODUK);
		}
	}
	paramscetakbill_vikasirDaftarIGD['no_urut[]']=urut;
	paramscetakbill_vikasirDaftarIGD['kd_produk[]']=kd_produk;
    return paramscetakbill_vikasirDaftarIGD;
};

function printkwitansiIGD(){
	var dlg = Ext.MessageBox.prompt('Print Kwitansi', 'Nama Pembayar :', function(btn, combo){
		if (btn == 'ok'){
			Ext.Ajax.request({
				url: baseURL + "index.php/gawat_darurat/directkwitansi/cek",
				params: dataparamcetakkwitansi_vikasirDaftarIGD(),
				success: function(o){
					var cst = Ext.decode(o.responseText);
					if (cst.success === true && cst.pesan === 'no_message'){
						var IGDParam=dataparamcetakkwitansi_vikasirDaftarIGD();
						IGDParam['namaPembayar']=combo;
						Ext.Ajax.request({
							url: baseURL + "index.php/main/CreateDataObj",
							params: IGDParam,
							success: function(o){
								var cst = Ext.decode(o.responseText);
								if (cst.success === true && cst.pesan === 'no_message'){
									
								}else if (cst.success === true && cst.pesan !== 'no_message'){
									ShowPesanWarningKasirigd(cst.pesan);
								}else if  (cst.success === false && cst.pesan !== ''){
									ShowPesanWarningKasirigd('tidak berhasil melakukan pencetakan '  + cst.pesan,'Cetak Data');
								}else{
									ShowPesanWarningKasirigd('tidak berhasil melakukan pencetakan '  + cst.pesan,'Cetak Data');
								}
							}
						});
					}else if (cst.success === true && cst.pesan !== 'no_message'){
						Ext.Msg.confirm('Warning', cst.pesan, function(button){
							if (button == 'yes'){
								var IGDParam=dataparamcetakkwitansi_vikasirDaftarIGD();
								IGDParam['namaPembayar']=combo;
								Ext.Ajax.request({
									url: baseURL + "index.php/main/CreateDataObj",
									params: IGDParam,
									success: function(o){
										var cst = Ext.decode(o.responseText);
										if (cst.success === true && cst.pesan === 'no_message'){
											
										}else if (cst.success === true && cst.pesan !== 'no_message'){
											ShowPesanWarningKasirigd(cst.pesan);
										}else if  (cst.success === false && cst.pesan !== ''){
											ShowPesanWarningKasirigd('tidak berhasil melakukan pencetakan '  + cst.pesan,'Cetak Data');
										}else{
											ShowPesanWarningKasirigd('tidak berhasil melakukan pencetakan '  + cst.pesan,'Cetak Data');
										}
									}
								});
							}
						})
					}
				}
			});
			
		}
	},this,false,Ext.getCmp('txtNamaPasienDetransaksi').getValue());
}

function dataparamcetakkwitansi_vikasirDaftarIGD(){
    var paramscetakbill_vikasirDaftarIGD ={      
		Table: 'DirectKwitansiIgd',
		No_TRans: Ext.get('txtNoTransaksiKasirigdKasir').getValue(),
		//kdUnit: rowSelectedKasirigdKasir.data.KD_UNIT,
		kdUnit: CurrentKdUnit_KasirIGD,
		KdKasir : tmpkdkasirIGD,
		JmlBayar: Ext.get('txtJumlah2EditData_viKasirigd').getValue(),
		JmlDibayar: Ext.get('txtJumlahEditData_viKasirigd').getValue(),
		printer: Ext.getCmp('cbopasienorder_printer_kasirigd').getValue()
	};
	console.log(Ext.getCmp('gridDTLTRKasirigd').getStore().data.items);
	var urut=[];
	var kd_produk=[];
	var items=Ext.getCmp('gridDTLTRKasirigd').getStore().data.items;
	for(var i=0,iLen=items.length; i<iLen; i++){
		if(items[i].data.TAG==true){
			urut.push(items[i].data.URUT);
			kd_produk.push(items[i].data.KD_PRODUK);
		}
	}
	paramscetakbill_vikasirDaftarIGD['no_urut[]']=urut;
	paramscetakbill_vikasirDaftarIGD['kd_produk[]']=kd_produk;
    return paramscetakbill_vikasirDaftarIGD;
}

function cekKomponen(kd_produk,kd_tarif,kd_unit,urut,harga){
	Ext.Ajax.request
	({
		url: baseURL + "index.php/main/functionIGD/cekKomponen",
		params: {
			kd_produk:kd_produk,
			kd_tarif:kd_tarif,
			kd_unit:kd_unit
		},
		failure: function(o)
		{
			ShowPesanErrorKasirrwj('Hubungi Admin', 'Error');
		},	
		success: function(o) 
		{   
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) 
			{
				if(cst.komponen > 0){
					currentJasaDokterKdTarif_KasirIGD=kd_tarif;
					currentJasaDokterKdProduk_KasirIGD=kd_produk;
					//currentJasaDokterUrutDetailTransaksi_KasirIGD=urut;
					currentJasaDokterHargaJP_KasirIGD=harga;
					PilihDokterLookUp_KasirIGD()
				}
			} 
			else 
			{
				ShowPesanErrorKasirrwj('Gagal cek komponen pelayanan', 'Error');
			};
		}
	});
};

function PilihDokterLookUp_KasirIGD(edit) 
{
	var fldDetail = [];
    dsGridJasaDokterPenindak_KasirIGD = new WebApp.DataStore({ fields: fldDetail });
    var GridTrDokterColumnModel =  new Ext.grid.ColumnModel([
        new Ext.grid.RowNumberer(),
		{
			header			: 'kd_component',
			dataIndex		: 'kd_component',
			width			: 80,
			menuDisabled	: true,
			hidden 		: true
        },
		{
			header			: 'tgl_berlaku',
			dataIndex		: 'tgl_berlaku',
			width			: 80,
			menuDisabled	: true,
			hidden 		: true
        },
		{
			header			: 'Komponent',
			dataIndex		: 'component',
			width			: 200,
			menuDisabled	: true,
			hidden 		: false
        },
        {
			header			: 'kd_dokter',
			dataIndex		: 'kd_dokter',
			width			: 80,
			menuDisabled	: true,
			hidden 		: true
        },{
            header			:'Nama Dokter',
            dataIndex		: 'nama',
            sortable		: false,
            hidden			: false,
			menuDisabled	: true,
			width			: 250,
            editor			: new Nci.form.Combobox.autoComplete({
				store	: dsgridpilihdokterpenindak_KasirIGD,
				select	: function(a,b,c){
					var line	= GridDokterTr_KasirIGD.getSelectionModel().selection.cell[0];
					dsGridJasaDokterPenindak_KasirIGD.getRange()[line].data.kd_dokter=b.data.kd_dokter;
					dsGridJasaDokterPenindak_KasirIGD.getRange()[line].data.nama=b.data.nama;
					GridDokterTr_KasirIGD.getView().refresh();
				},
				insert	: function(o){
					return {
						kd_dokter       : o.kd_dokter,
						nama       		: o.nama,
						text			:  '<table style="font-size: 11px;"><tr><td width="50">'+o.kd_dokter+'</td><td width="200">'+o.nama+'</td></tr></table>'
					}
				},
				param	: function(){
					var params={};
					params['kd_unit'] = CurrentKDUnitIGD;
					params['penjas'] = 'igd';
					return params;
				},
				url		: baseURL + "index.php/main/functionRWJ/getdokterpenindak",
				valueField: 'nama_obat',
				displayField: 'text',
				listWidth: 380
			})
			//getTrDokter(dsTrDokter)
	    },
        ]
    );
	
	
	GridDokterTr_KasirIGD= new Ext.grid.EditorGridPanel({
		id			: 'GridDokterTr_KasirIGD',
		stripeRows	: true,
		width		: 487,
		height		: 160,
        store		: dsGridJasaDokterPenindak_KasirIGD,
        border		: true,
        frame		: false,
        autoScroll	: true,
        cm			: GridTrDokterColumnModel,
		listeners	: {
			rowclick: function( $this, rowIndex, e )
			{
				trcellCurrentTindakan_KasirIGD = rowIndex;
    		},
			celldblclick: function(gridView,htmlElement,columnIndex,dataRecord){
				
			}
		},
		viewConfig	: {forceFit: true},
    });
 
	
    var lebar = 500;
    var FormLookUDokter_KasirIGD = new Ext.Window
    (
        {
            id: 'winTRDokterPenindak_KasirIGD',
            title: 'Pilih Dokter Penindak',
            closeAction: 'destroy',
            width: 500,
            height: 220,
            border: false,
            resizable: false,
            iconCls: 'Request',
			constrain : true,    
			modal: true,
           	items: [ 
				GridDokterTr_KasirIGD
			],
			tbar :
			[
				{
					xtype:'button',
					text:'Simpan',
					iconCls	: 'save',
					hideLabel:true,
					id: 'BtnOktrDokter',
					handler:function()
					{
						Datasave_KasirIGD(false,true);
					}
				},
				'-',
			],
            listeners:
            { 
            }
        }
    );
	FormLookUDokter_KasirIGD.show();
	if(edit == true){
		GetgridEditDokterPenindakJasa_KasirIGD()
	} else{
		GetgridPilihDokterPenindakJasa_KasirIGD(currentJasaDokterKdProduk_KasirIGD,currentJasaDokterKdTarif_KasirIGD);
	}
	
};

function GetgridEditDokterPenindakJasa_KasirIGD(){
	Ext.Ajax.request
	({
		url: baseURL + "index.php/main/functionRWJ/viewgrideditjasadokterpenindak",
		params: {
			kd_unit:CurrentKDUnitIGD,
			urut:currentJasaDokterUrutDetailTransaksi_KasirIGD,
			kd_kasir:CurrentKdKasir_KasirIGD,
			no_transaksi:CurrentNoTransaksi_KasirIGD,
			tgl_transaksi:CurrentTglTransaksi_KasirIGD,
			kd_produk:currentJasaDokterKdProduk_KasirIGD,
			kd_tarif:currentJasaDokterKdTarif_KasirIGD
			
		},
		failure: function(o)
		{
			ShowPesanErrorKasirigd('Hubungi Admin', 'Error');
		},	
		success: function(o) 
		{   
			dsGridJasaDokterPenindak_KasirIGD.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) 
			{
				if(cst.totalrecords > 0){
					var recs=[],
						recType=dsGridJasaDokterPenindak_KasirIGD.recordType;
					for(var i=0; i<cst.ListDataObj.length; i++){
						recs.push(new recType(cst.ListDataObj[i]));
					}
					dsGridJasaDokterPenindak_KasirIGD.add(recs);
					GridDokterTr_KasirIGD.getView().refresh();
				} else{
					GetgridPilihDokterPenindakJasa_KasirIGD(currentJasaDokterKdProduk_KasirIGD,currentJasaDokterKdTarif_KasirIGD);
				}
			} 
			else 
			{
				ShowPesanErrorKasirigd('Gagal membaca list dokter penindak', 'Error');
			};
		}
	});
}

function GetgridPilihDokterPenindakJasa_KasirIGD(kd_produk,kd_tarif){
	Ext.Ajax.request
	({
		url: baseURL + "index.php/main/functionRWJ/viewgridjasadokterpenindak",
		params: {
			kd_produk:kd_produk,
			kd_tarif:kd_tarif,
			kd_unit:kodeunit//CurrentKDUnitIGD
		},
		failure: function(o)
		{
			ShowPesanErrorKasirigd('Hubungi Admin', 'Error');
		},	
		success: function(o) 
		{   
			dsGridJasaDokterPenindak_KasirIGD.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) 
			{
				var recs=[],
					recType=dsGridJasaDokterPenindak_KasirIGD.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					if(cst.ListDataObj[i].kd_component=='20'){
						cst.ListDataObj[i]['kd_dokter']=rowSelectedKasirigdKasir.data.KD_DOKTER;
						cst.ListDataObj[i]['nama']=rowSelectedKasirigdKasir.data.NAMA_DOKTER;
					}
					recs.push(new recType(cst.ListDataObj[i]));
				}
				dsGridJasaDokterPenindak_KasirIGD.add(recs);
				GridDokterTr_KasirIGD.getView().refresh();
			} 
			else 
			{
				ShowPesanErrorKasirigd('Gagal membaca history diagnosa', 'Error');
			};
		}
	});
}
function loaddatastoredokterVisite_REVISI(){
	console.log(cellSelecteddeskripsi_panatajasaigd.data);
	Ext.Ajax.request({
		url: baseURL +  "index.php/general/control_cmb_dokter/getCmbDokterVisite",
		params: {
			kd_kasir        : '06',
			no_transaksi    : rowSelectedPJIGD.data.NO_TRANSAKSI,
			tgl_transaksi   : rowSelectedPJIGD.data.TANGGAL_TRANSAKSI,
			kd_produk       : cellSelecteddeskripsi_panatajasaigd.data.KD_PRODUK,
			urut            : cellSelecteddeskripsi_panatajasaigd.data.URUT,
		},
		success: function(response) {
			var cst  = Ext.decode(response.responseText);
			for(var i=0,iLen=cst['data'].length; i<iLen; i++){
				var recs    = [],recType = dsDataDokterPenindak_PJ_IGD.recordType;
				var o       = cst['data'][i];
				recs.push(new recType(o));
				DataStoreSecondGridStore_PJIGD.add(recs);
			}
		},
	});
}
function cboPenangan_KASIRIGD(){
	var tmpValue;
	/*if (tmpKdJob == 3) {
		tmpValue = "Perawat";
	}else{
		tmpValue = "Dokter";
	}*/
	var Field = ['KD_JOB','KD_COMPONENT','LABEL'];
	dsDataDaftarPenangan_KASIR_IGD = new WebApp.DataStore({ fields: Field });
	loaddatastorePenangan();
	var cboPenangan_KASIRIGD       = new Ext.form.ComboBox({
		id: 'cboPenangan_KASIRIGD',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Pilih penangan...',
		fieldLabel: 'Penangan ',
		align: 'Right',
		store: dsDataDaftarPenangan_KASIR_IGD,
		valueField: 'KD_JOB',
		displayField: 'LABEL',
		//anchor           : '100%',
		value           : tmpValue,
		listeners:
		{
			select : function(a, b, c){
				DataStorefirstGridStore_PJIGD.removeAll();
				tmpKdJob =  b.data.KD_JOB;
				loaddatastoredokter_REVISI(rowSelectedPJIGD);
			}
		}
	})

	return cboPenangan_KASIRIGD;
}
function loaddatastoredokter_REVISI(params, dokter = null){
	// dsDataDokterPenindak_PJ_IGD.removeAll();
	DataStorefirstGridStore_PJIGD.removeAll();
	var tmp_urut;
	if (typeof params.data.URUT == 'undefined') {
		tmp_urut = params.data.URUT_MASUK;
	}else{
		tmp_urut = params.data.URUT;
	}
	Ext.Ajax.request({
		url: baseURL +  "index.php/general/control_cmb_dokter/getCmbDokterSpesialisasi",
		params: {
			kd_unit         : '31',
			jenis_dokter    : tmpKdJob,
			kd_kasir        : '02',
			no_transaksi    : params.data.NO_TRANSAKSI,
			urut            : tmp_urut,
			tgl_transaksi   : params.data.TGL_TRANSAKSI,
			kd_job          : tmpKdJob,
			txtDokter       : dokter,
		},
		success: function(response) {
			var cst  = Ext.decode(response.responseText);
			console.log(cst);
			for(var i=0,iLen=cst['data'].length; i<iLen; i++){
				var recs    = [],recType = dsDataDokterPenindak_PJ_IGD.recordType;
				var o       = cst['data'][i];
				recs.push(new recType(o));	
				DataStorefirstGridStore_PJIGD.add(recs);
			}
		}
	});
}
function PilihDokterLookUpPJ_IGD_REVISI(edit) {    
    var FormLookUDokter_IGD = new Ext.Window({
		xtype : 'fieldset',
		layout:'fit',
		id: 'winTRDokterPenindak_IGD_REVISI',
		title: 'Pilih Pelaksana',
		closeAction: 'destroy',
		width: 600,
		height: 400,
		border: true,
		resizable: false,
		constrain : true,    
		modal: true,
		items: [ 
			{
				layout:{
					type:'hbox',
					align:'stretch'
				},
				bodyStyle:'padding: 4px;',
				border:false,
				items:[
					{
						border:false,
						flex:1,
						layout:{
							type:'vbox',
							align:'stretch'
						},
						items:[
							cboPenangan_KASIRIGD(),
							seconGridPenerimaan(),
						]
					},{
						border:false,
						flex:1,
						layout:{
							type:'vbox',
							align:'stretch'
						},
						style:'padding-left: 4px;',
						items:[
							{
								xtype   : 'textfield',
								anchor  : '100%',
								id      : 'txtSearchDokter_KASIRIGD',
								name    : 'txtSearchDokter_KASIRIGD',
								listeners : {
									specialkey: function (){
										if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9){
											DataStorefirstGridStore_PJIGD.removeAll();
											tmpSearchDokter = Ext.getCmp('txtSearchDokter_KASIRIGD').getValue();
											/*tmpKdJob      = Ext.getCmp('cboPenangan_KASIRIGD').getValue();
											if (tmpKdJob == 'Dokter') {
												tmpKdJob = 1;
											}else{
												tmpKdJob = 3;
											}*/
											loaddatastoredokter_REVISI(rowSelectedPJIGD, Ext.getCmp('txtSearchDokter_KASIRIGD').getValue());
											//loaddatastoredokter_REVISI();
										}
									},
								}
							},
							firstGridPenerimaan(),
						]
					}
				]
			}
		],
		tbar :[
			{
				xtype:'button',
				text:'Simpan',
				iconCls : 'save',
				hideLabel:true,
				id: 'BtnOktrDokter',
				handler:function(){
					//RefreshDataDetail_kasirrwi(tmpno_transaksi);
					
					//PenataJasaKasirRWI.dsGridTindakan.getRange()[dataRowIndexDetail].set('DESKRIPSI', PenataJasaKasirRWI.dsGridTindakan.getRange()[line].get('DESKRIPSI')+" ("+tmpNamaDokter+")");
					FormLookUDokter_IGD.close();
				}
			},
			'-',
		],
		listeners:{
			activate: function(){
			},
			afterShow: function(){
				this.activate();
			},
			deactivate: function(){
				batal_isi_tindakan='y';
			}
		}
	});
    FormLookUDokter_IGD.show();
}
function seconGridPenerimaan(){
	DataStoreSecondGridStore_PJIGD.removeAll();
	var secondGrid;
	var fields = [
		{name: 'KD_UNIT', mapping : 'KD_UNIT'},
		{name: 'NAMA_UNIT', mapping : 'NAMA_UNIT'}
	];
	secondGridStoreLapPenerimaan = new Ext.data.JsonStore({
		fields : fieldsDokterPenindak,
		root   : 'data'
	});
	var cols = [
		{ id : 'KD_DOKTER', header: "Kode Pelaksana", width: .25, sortable: true, dataIndex: 'KD_DOKTER',hidden : false},
		{ header: "Nama", width: .75, sortable: true, dataIndex: 'NAMA'},
		{ header: "JP", width: .25, dataIndex: 'JP'},
		{ header: "PRC", width: .25, dataIndex: 'PRC'},
	];
	secondGrid = new Ext.grid.GridPanel({
		ddGroup          : 'firstGridDDGroup',
		store            : DataStoreSecondGridStore_PJIGD,
		columns          : cols,
		autoScroll       : true,
		columnLines      : true,
		border           : true,
		enableDragDrop   : true,
		flex:1,
		enableDragDrop   : true,
		stripeRows       : true,
		autoExpandColumn : 'KD_DOKTER',
		title            : 'Dokter/ Perawat yang menangani',
		plugins          : [new Ext.ux.grid.FilterRow()],
		listeners : {
			afterrender : function(comp) {
				var group_dokter = 0;
				console.log(tmp_group_dokter);
				if (tmp_group_dokter!= 0 || tmp_group_dokter != '' || tmp_group_dokter != 'undefined' || tmp_group_dokter != null) {
					console.log(cellSelecteddeskripsi_panatajasaigd.data.GROUP);
					group_dokter = tmp_group_dokter;
					// group_dokter = cellSelecteddeskripsi_panatajasaigd.data.GROUP;
				}
				var secondGridDropTargetEl = secondGrid.getView().scroller.dom;
				var secondGridDropTarget   = new Ext.dd.DropTarget(secondGridDropTargetEl, {
					ddGroup    : 'secondGridDDGroup',
					notifyDrop : function(ddSource, e, data){
						var records   =  ddSource.dragData.selections;
						var kd_dokter = "";
						for(var i=0,iLen=records.length; i<iLen; i++){
							var o       = records[i];
							kd_dokter = o.data.KD_DOKTER;
						}
	
						Ext.Ajax.request({
							url: baseURL +  "index.php/rawat_inap/control_visite_dokter/insertDokter",
							params: {
								label           : Ext.getCmp("cboPenangan_KASIRIGD").getValue(),
								kd_job          : tmpKdJob,
								no_transaksi    : rowSelectedPJIGD.data.NO_TRANSAKSI,
								tgl_transaksi   : rowSelectedPJIGD.data.TANGGAL_TRANSAKSI,
								kd_produk       : cellSelecteddeskripsi_panatajasaigd.data.KD_PRODUK,
								urut            : cellSelecteddeskripsi_panatajasaigd.data.URUT,
								kd_kasir        : '06',
								kd_unit         : '31',
								kd_dokter       : kd_dokter,
								kd_tarif        : cellSelecteddeskripsi_panatajasaigd.data.KD_TARIF,
								tgl_berlaku     : cellSelecteddeskripsi_panatajasaigd.data.TGL_BERLAKU,
								group           : group_dokter,
							},
							success: function(response) {
								DataStoreSecondGridStore_PJIGD.removeAll();
								loaddatastoredokterVisite_REVISI();
								var cst  = Ext.decode(response.responseText);
								dsTRDetailKasirIGDList.getRange()[dataRowIndexDetail].set('JUMLAH_DOKTER', cst.data.JUMLAH_DOKTER);
								Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
								secondGrid.getView().refresh();
								return true
							},
						});
					}
				});
			},
			rowdblclick: function(dataview, index, item, e) {
				//DataStorefirstGridStore.data.items[index].data.KD_DOKTER
				Ext.Ajax.request({
					url: baseURL +  "index.php/rawat_inap/control_visite_dokter/deleteDokter",
					params: {
						kd_kasir        : '06',
						no_transaksi    : rowSelectedKasirigdKasir.data.NO_TRANSAKSI,
						urut            : cellSelecteddeskripsi_panatajasaigd.data.URUT,
						tgl_transaksi   : cellSelecteddeskripsi_panatajasaigd.data.TGL_TRANSAKSI,
						kd_dokter       : DataStoreSecondGridStore_PJIGD.data.items[index].data.KD_DOKTER,
						kd_produk       : cellSelecteddeskripsi_panatajasaigd.data.KD_PRODUK,
						kd_unit         : '31',
						line       		: DataStoreSecondGridStore_PJIGD.data.items[index].data.LINE,
					},
					success: function(response) {
						var cst  = Ext.decode(response.responseText);
						// PenataJasaIGD.dsGridTindakan.getRange()[dataRowIndexDetail].set('DESKRIPSI', cst.DESKRIPSI+" ("+cst.DAFTAR_DOKTER+")");
						dsTRDetailKasirIGDList.getRange()[dataRowIndexDetail].set('JUMLAH_DOKTER', cst.JUMLAH_DOKTER);
						DataStoreSecondGridStore_PJIGD.removeAll();
						loaddatastoredokterVisite_REVISI();

					},
				});
			},
			callback : function(){
				Ext.getCmp('txtSearchDokter_KASIRIGD').focus();
			}
		},
		viewConfig:{
			forceFit: true,
		}
	});
	return secondGrid;
}
function firstGridPenerimaan(){
	DataStorefirstGridStore_PJIGD.removeAll();
	var cols = [
		{ id : 'KD_DOKTER', header: "Kode Pelaksana", width: .25, sortable: true, dataIndex: 'KD_DOKTER',hidden : false},
		{ id : 'NAMA', header: "Nama", width: .75, sortable: true, dataIndex: 'NAMA'}
	];
	var firstGrid;
		firstGrid = new Ext.grid.GridPanel({
		ddGroup          : 'secondGridDDGroup',
		store            : DataStorefirstGridStore_PJIGD,
		columns          : cols,
		autoScroll       : true,
		columnLines      : true,
		border           : true,
		enableDragDrop   : true,
		stripeRows       : true,
		flex:1,
		trackMouseOver   : true,
		title            : 'Dokter/ Perawat penangan',
		plugins          : [new Ext.ux.grid.FilterRow()], 
		colModel         : new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(),
			{
					id: 'colNMKd_Dokter',
					header: 'KD Dokter',
					dataIndex: 'KD_DOKTER',
					sortable: true,
					hidden : false,
					width: .25
			},
			{
					id: 'colNMDokter',
					header: 'Nama',
					dataIndex: 'NAMA',
					sortable: true,
					width: .75
			}
		]),
		listeners : {
			afterrender : function(comp) {
				var firstGridDropTargetEl =  firstGrid.getView().scroller.dom;
				var firstGridDropTarget = new Ext.dd.DropTarget(firstGridDropTargetEl, {
					ddGroup    : 'firstGridDDGroup',
					notifyDrop : function(ddSource, e, data){
						var records   	=  ddSource.dragData.selections;
						var kd_dokter 	= "";
						var line 		= "";
						for(var i=0,iLen=records.length; i<iLen; i++){
							var o       = records[i];
							kd_dokter 	= o.data.KD_DOKTER;
							line 		= o.data.LINE;
						}
						Ext.Ajax.request({
							url: baseURL +  "index.php/rawat_inap/control_visite_dokter/deleteDokter",
							params: {
								kd_kasir        : '06',
								no_transaksi    : rowSelectedKasirigdKasir.data.NO_TRANSAKSI,
								urut            : cellSelecteddeskripsi_panatajasaigd.data.URUT,
								tgl_transaksi   : cellSelecteddeskripsi_panatajasaigd.data.TGL_TRANSAKSI,
								kd_dokter       : kd_dokter,
								kd_produk       : cellSelecteddeskripsi_panatajasaigd.data.KD_PRODUK,
								kd_unit         : '31',
								line         	: line,
							},
							success: function(response) {
								var cst  = Ext.decode(response.responseText);
								// PenataJasaIGD.dsGridTindakan.getRange()[dataRowIndexDetail].set('DESKRIPSI', cst.DESKRIPSI+" ("+cst.DAFTAR_DOKTER+")");
								dsTRDetailKasirIGDList.getRange()[dataRowIndexDetail].set('JUMLAH_DOKTER', cst.JUMLAH_DOKTER);
								Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
								firstGrid.store.add(records);
								firstGrid.getView().refresh();
								return true
							},
						});
					}
				});
			},
			rowdblclick: function(dataview, index, item, e) {
				var group_dokter = 0;
				console.log(tmp_group_dokter);
				if (tmp_group_dokter!= 0 || tmp_group_dokter != '' || tmp_group_dokter != 'undefined' || tmp_group_dokter != null) {
					// group_dokter = cellSelecteddeskripsi_panatajasaigd.data.GROUP;
					group_dokter =tmp_group_dokter;
				}
				Ext.Ajax.request({
					url: baseURL +  "index.php/gawat_darurat/control_visite_dokter/insertDokter",
					params: {
						label           : Ext.getCmp("cboPenangan_KASIRRWI").getValue(),
						kd_job          : tmpKdJob,
						no_transaksi    : rowSelectedPJIGD.data.NO_TRANSAKSI,
						tgl_transaksi   : rowSelectedPJIGD.data.TANGGAL_TRANSAKSI,
						kd_produk       : cellSelecteddeskripsi_panatajasaigd.data.KD_PRODUK,
						urut            : cellSelecteddeskripsi_panatajasaigd.data.URUT,
						kd_kasir        : '02',
						kd_unit         : tmpkd_unit,
						kd_dokter       : DataStorefirstGridStore_PJRWI.data.items[index].data.KD_DOKTER,
						kd_tarif        : cellSelecteddeskripsi_panatajasaigd.data.KD_TARIF,
						tgl_berlaku     : cellSelecteddeskripsi_panatajasaigd.data.TGL_BERLAKU,
						group           : group_dokter,
					},
					success: function(response) {
						DataStoreSecondGridStore_PJIGD.removeAll();
						loaddatastoredokterVisite_REVISI();
						//loaddatastoredokter_REVISI(Ext.getCmp('txtSearchDokter_KASIRRWI'),getValue());
						//
						//var selection = firstGrid.getView().getSelectionModel().getSelection()[0];
						//console.log(selection);
						// RefreshRekapitulasiTindakanKasirRWI(notransaksi, kdkasirnya);
						var cst  = Ext.decode(response.responseText);
						console.log(dataRowIndexDetail);
						console.log(PenataJasaIGD.dsGridTindakan.getRange()[dataRowIndexDetail].get('DESKRIPSI'));
						// PenataJasaIGD.dsGridTindakan.getRange()[dataRowIndexDetail].set('DESKRIPSI', cst.data.DESKRIPSI+" ("+cst.data.DAFTAR_DOKTER+")");
						dsTRDetailKasirIGDList.getRange()[dataRowIndexDetail].set('JUMLAH_DOKTER', cst.data.JUMLAH_DOKTER);
						/*Ext.each(index, firstGrid.store.remove, firstGrid.store);
						firstGrid.store.remove(index);
						firstGrid.getView().refresh();*/
						var selected = firstGrid.getSelectionModel().getSelections();
						
						if(selected.length>0) {
							for(var i    =0;i<selected.length;i++) {
								DataStorefirstGridStore_PJIGD.remove(selected[i]);
							}
						}
					},
				});
			}
		},
		viewConfig:{
			forceFit: true,
		}
	});
	return firstGrid;
}
function loaddatastorePenangan(){
	Ext.Ajax.request({
		url: baseURL +  "index.php/general/control_cmb_dokter/getCmbDokterInapInt",
		params: {
			groups : tmp_group_dokter,
		},
		success: function(response) {
			var cst  = Ext.decode(response.responseText);
			for(var i=0,iLen=cst['data'].length; i<iLen; i++){
				var recs    = [],recType = dsDataDaftarPenangan_KASIR_IGD.recordType;
				var o       = cst['data'][i];
				recs.push(new recType(o));
				dsDataDaftarPenangan_KASIR_IGD.add(recs);
			}
		},
		callback : function(){
			Ext.getCmp('txtSearchDokter_KASIRIGD').focus();
		}
	});
}

function SimpanJasaDokterPenindak_KasirIGD(kd_produk,kd_tarif,urut,harga){
	Ext.Ajax.request
	({
		url: baseURL + "index.php/main/functionIGD/savejasadokterpenindak",
		params: getParamsJasaDokterPenindak_KasirIGD(urut,harga),
		failure: function(o)
		{
			ShowPesanErrorKasirigd('Hubungi Admin', 'Error');
		},	
		success: function(o) 
		{   
			//dsGridJasaDokterPenindak_KasirRWJ.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) 
			{
				ShowPesanInfoKasirigd('Dokter penindak berhasil disimpan','Information');
				SimpanJasaDokterPenindak_KASIRIGD_SQL(kd_produk,kd_tarif,urut,harga);
				GetgridEditDokterPenindakJasa_KasirIGD();
			} 
			else 
			{
				ShowPesanErrorKasirigd('Gagal membaca history diagnosa', 'Error');
			};
		}
	});
}

function getParamsJasaDokterPenindak_KasirIGD(urut,harga){
	var params = {
		kd_kasir:CurrentKdKasir_KasirIGD,
		no_transaksi:CurrentNoTransaksi_KasirIGD,
		tgl_transaksi:CurrentTglTransaksi_KasirIGD,
		urut:urut,
		harga:harga
	};
	params['jumlah'] = dsGridJasaDokterPenindak_KasirIGD.getCount();
	for(var i = 0 ; i < dsGridJasaDokterPenindak_KasirIGD.getCount();i++)
	{
		params['kd_component-'+i]=dsGridJasaDokterPenindak_KasirIGD.data.items[i].data.kd_component;
		params['kd_dokter-'+i]=dsGridJasaDokterPenindak_KasirIGD.data.items[i].data.kd_dokter;
	}
	return params;
}


// ===========================================================INTEGRASI SQL=====================================================================================================================
function Datasave_KasirigdKasir_SQL() 
{	
	Ext.Ajax.request
	({
		url: baseURL + "index.php/gawat_darurat_sql/functionIGD/savepembayaran",
		params: getParamDetailTransaksiKasirigd(),
		failure: function(o)
		{
			ShowPesanWarningKasirigd('SQL. Error simpan pembayaran, Hubungi Admin!', 'Gagal');
		},	
		success: function(o) 
		{
			var cst = Ext.decode(o.responseText);
			if (cst.success === true) 
			{
				loadMask.hide();
				ShowPesanInfoKasirigd(nmPesanSimpanSukses,nmHeaderSimpanData);
				RefreshDataKasirigdKasirDetail(Ext.get('txtNoTransaksiKasirigdKasir').dom.value);
				RefreshDataFilterKasirIgdKasir();
			}
			else 
			{
				ShowPesanWarningKasirigd('SQL. Data gagal disimpan segera Hubungi Admin!', 'Gagal');
			};
		}
	})
};

function Datasave_KasirIGD_SQL() 
{	
	Ext.Ajax.request
	 (
		{
			url: baseURL + "index.php/gawat_darurat_sql/functionIGD/savedetailproduk",
			params: getParamDetailTransaksiIGD(),
			failure: function(o)
			{
				ShowPesanWarningKasirigd('SQL. Error simpan tindakan, Hubungi Admin!', 'Gagal');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
				
				}
				else 
				{
					ShowPesanWarningKasirigd('SQL. Gagal simpan tindakan!', 'Gagal');
				};
			}
		}
	)
	
};

function DataDeleteKasirIGDDetail_SQL()
{
    Ext.Ajax.request
    (
        {
            url: baseURL + "index.php/gawat_darurat_sql/functionIGD/deletetindakantambahan",
            params:  getParamDataDeleteKasirIGDDetail(),
			failure: function(o)
			{
				ShowPesanWarningKasirigd("SQL. Error hapus baris tindakan, Hubungi Admin!",'WARNING');
				loadMask.hide();
			},
            success: function(o)
            {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true)
                {
					
                }
                else
                {
                    ShowPesanWarningKasirigd('SQL. Gagal mengapus baris tindakan!','WARNING');
                };
            }
        }
    )
};


function DataDeleteKasirigdKasirDetail_SQL()
{
    Ext.Ajax.request
    (
        {
            url: baseURL + "index.php/gawat_darurat_sql/functionIGD/deletedetail_bayar",
            params:  getParamDataDeleteKasirigdKasirDetail(),
			failure: function(o)
			{
				ShowPesanWarningKasirigd("SQL. Error hapus baris pembayaran, Hubungi Admin!",'WARNING');
				loadMask.hide();
			},
            success: function(o)
            {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true)
                {
					
				}
                else
                {
                    ShowPesanWarningKasirigd('SQL. Gagal menghapus baris pembayaran!','WARNING');
					loadMask.hide();
                };
            }
        }
    )
};


function UpdateTutuptransaksi_SQL(mBol) 
{	
	Ext.Ajax.request
	({
		//url: "./Datapool.mvc/CreateDataObj",
		url: baseURL + "index.php/gawat_darurat_sql/functionIGD/ubah_co_status_transksi",
		params: paramUpdateTransaksi(),
		failure: function(o)
		{
			ShowPesanWarningKasirigd('Data gagal tersimpan segera hubungi Admin', 'Gagal');
		},	
		success: function(o) 
		{
			//RefreshDatahistoribayar(Ext.get('cellSelectedtutup_kasirigd);
		
			var cst = Ext.decode(o.responseText);
			if (cst.success === true) 
			{
				cellSelectedtutup_kasirigd='';
			}
			else 
			{
				ShowPesanWarningKasirigd('SQL. Gagal tutup transaksi, segera hubungi Admin', 'Gagal');
				cellSelectedtutup_kasirigd='';
			};
		}
	})
};

function TransferData_IGD_SQL(mBol) 
{	
	if(parseInt(Ext.getCmp('txtjumlahtranfer_KasirIGD').getValue()) > parseInt(SisaPembayaranKasirIGD)){
		ShowPesanErrorKasirigd('Jumlah yang di transfer melebihi saldo tagihan!', 'Gagal');
	} else{
		Ext.Ajax.request
		 (
			{
				url: baseURL + "index.php/gawat_darurat_sql/functionIGD/saveTransfer",
				params: getParamTransferRwi(),
				failure: function(o)
				{
					ShowPesanWarningKasirigd('SQL. Error transfer pembayaran. Hubungi Admin!', 'Gagal');
					RefreshDataKasirIGDDetail(notransaksi);
				},	
				success: function(o) 
				{
					RefreshDataKasirIGDDetail(notransaksi);
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						
					}
					else 
					{
						ShowPesanWarningKasirigd('Error '+cst.pesan+'. Gagal melakukan transfer!', 'Gagal');
					};
				}
			}
		)
	}
};

function BatalTransaksi_SQL(notrans,notransbaru,urutmasuk,urutmasukbaru)
{

	Ext.Ajax.request
	 (
		{
			url: baseURL + "index.php/gawat_darurat_sql/functionIGD/batal_transaksi",
			params: getParamDetailBatalTransaksiIGD_SQL(notrans,notransbaru,urutmasuk,urutmasukbaru),
			failure: function(o)
			{
				ShowPesanWarningKasirigd('SQL. ' + Ext.decode(o.responseText), 'Gagal');
			},
			success: function(o)
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					cellSelectedtutup_kasirigd='';
				}
				else
				{
					ShowPesanWarningKasirigd('SQL. ' + Ext.decode(o.responseText), 'Gagal');
					cellSelectedtutup_kasirigd='';
				};
			}
		}
	)
};

function getParamDetailBatalTransaksiIGD_SQL(notrans,notransbaru,urutmasuk,urutmasukbaru)
{
	if(tampungtypedata==='')
	{
		tampungtypedata=0;
	};
	var line = dsTRDetailHistoryListIGD.getCount();
	var totalhehe=0;
	for (var i = 0 ; i < line; i++)
	{
		totalhehe += parseInt(dsTRDetailHistoryListIGD.data.items[i].data.BAYAR);
		
	}
    var params =
	{
		notransaksi : notrans,
		notransbaru	: notransbaru,
		urutmasuk	: urutmasuk,
		urutmasukbaru: urutmasukbaru,
		kdPasien 	: kodepasien,
		noTrans 	: notransaksi,
		kdUnit 		: kodeunit,
		Jumlah		: totalhehe,
		kdDokter 	: kdokter_btl,
		tglTrans 	: tgltrans,
		kdCustomer	: kdcustomeraa,
		Keterangan	: variablebatalhistori_igd,
		
	};
    return params
};

function SimpanJasaDokterPenindak_KASIRIGD_SQL(kd_produk,kd_tarif,urut,harga){
	Ext.Ajax.request
	({
		url: baseURL + "index.php/rawat_jalan_sql/functionRWJ/savejasadokterpenindak",
		params: getParamsJasaDokterPenindak_KasirIGD(urut,harga),
		failure: function(o)
		{
			//ShowPesanErrorKasirigd('SQL. Hubungi Admin', 'Error');
		},	
		success: function(o) 
		{   
			//dsGridJasaDokterPenindak_KasirRWJ.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) 
			{
				
			} 
			else 
			{
				ShowPesanErrorKasirigd('SQL. Gagal membaca history diagnosa', 'Error');
			};
		}
	});
}

function getIdGetDataSettingKasirIGD()
{
    Ext.Ajax.request(
    {
        url: baseURL + "index.php/setup/manageutillity/getDataSetting", 
        params: {
            UserId:0
        },
        success: function(response, opts) {
            var cst = Ext.decode(response.responseText);
            Ext.getCmp('cbopasienorder_printer_kasirigd').setValue(cst.p_kasir);
            console.log(cst.p_kasir);
        },
        failure: function(response, opts) {
        }
    });
};

function cekTindakan(data){
	Ext.Ajax.request
	({
		url: baseURL + "index.php/main/functionIGD/ceksavetindakan",
		params: getParamsCekTindakan_KasirIGD(data),
		failure: function(o)
		{
			ShowPesanErrorKasirigd('Error. Hubungi Admin!', 'Error');
		},	
		success: function(o) 
		{   
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) 
			{
				
			} 
			else 
			{
				ShowPesanErrorKasirigd('Gagal cek tindakan!', 'Error');
			};
		}
	});
}

function getParamsCekTindakan_KasirIGD(data){
	var params = {
		kd_kasir:data.KD_KASIR,
		no_transaksi:data.NO_TRANSAKSI,
		tgl_transaksi:data.TANGGAL_TRANSAKSI,
		urut_masuk:data.URUT_MASUK,
		kd_pasien:data.KD_PASIEN,
		kd_unit:data.KD_UNIT
	};
	return params;
}

function getsaldotagihan_KasirIGD(){
	var total=0;
	if(Ext.getCmp('txtjumlahtranfer_KasirIGD').getValue() != 0){
		total = parseInt(SisaPembayaranKasirIGD) - parseInt(Ext.getCmp('txtjumlahtranfer_KasirIGD').getValue());
		Ext.getCmp('txtsaldotagihan_KasirIGD').setValue(formatCurrency(total))
	} else{
		Ext.getCmp('txtsaldotagihan_KasirIGD').setValue(formatCurrency(SisaPembayaranKasirIGD))
	}
}

function convertTglTransaksiKasirIGD(str){
	var date = new Date(str),
        mnth = ("0" + (date.getMonth()+1)).slice(-2),
        day  = ("0" + date.getDate()).slice(-2);
    return [ date.getFullYear(), mnth, day ].join("-") + ' 00:00:00';
}


function cekTransferTindakanIGD(reload,data){
	Ext.Ajax.request
	({
		url: baseURL + "index.php/main/functionIGD/cek_transfer_tindakan",
		params: getParamsCekTindakan_KasirIGD(data),
		failure: function(o)
		{
			ShowPesanWarningKasirigd("Migrasi data detail transaksi gagal !",'WARNING');
		},	
		success: function(o) 
		{   
			var cst = Ext.decode(o.responseText);

			if (cst.status === true && reload === true) {
				RefreshDataKasirIGDDetail(cst.no_transaksi);
			}else{
				RefreshDataKasirIGDDetail(cst.no_transaksi);
			}
		}
	});
}
//------------------TAMBAH BARU 10 Oktober 2017
function GantiDokterLookUpKasirIGD(mod_id) {
    var FormDepanDokter = new Ext.Panel({
        id: mod_id,
        closable: true,
        region: 'center',
        layout: 'form',
        title: 'Ganti Dokter',
        border: false,
        shadhow: true,
        autoScroll:false,
        iconCls: 'Request',
        margins: '0 5 5 0',
        items: [DokterLookUpKasirIGD()],
        listeners:{
            'afterrender': function()
            {}
        }
    });
   	return FormDepanDokter;
}
function DokterLookUpKasirIGD(rowdata_AG) {
    var lebar = 350;
    FormLookUpGantidokterKasirIGD = new Ext.Window({
        id: 'gridDokter',
        title: 'Ganti Dokter',
        closeAction: 'destroy',
        width: lebar,
        height: 180,
        border: false,
        resizable: false,
        plain: true,
        layout: 'fit',
        constrain: true,
        iconCls: 'Request',
        modal: true,
        items: getFormEntryDokterKasirIGD(lebar),
        listeners:{
            activate: function(){
                 if (varBtnOkLookupEmp === true){
                    Ext.get('txtKdDokter').dom.value   = rowSelectedLookdokter.data.KD_DOKTER;
                    Ext.get('txtNamaDokter').dom.value = rowSelectedLookdokter.data.NAMA_DOKTER;
                    varBtnOkLookupEmp=false;
                }
            },afterShow: function(){
                this.activate();
            },deactivate: function(){
                rowSelectedKasirDokter=undefined;
            },
			close: function (){
				shortcut.remove('ganti_dokter');
			},
			hide:function(){
				shortcut.remove('ganti_dokter');
			}
        }
    });
	shortcut.set({
		code:'ganti_dokter',
		list:[
			{
				key:'ctrl+s',
				fn:function(){
					Ext.getCmp('btnOkGantiDokterKasirIGD').el.dom.click();
				}
			},{
				key:'esc',
				fn:function(){
					FormLookUpGantidokterKasirIGD.close();
				}
			}
		]
	});
    FormLookUpGantidokterKasirIGD.show();
	//Ext.getCmp('cboDokterRequestEntryKasirIGD').focus(true,1000);
}
function getFormEntryDokterKasirIGD(lebar) {
    var pnlTRGantiDokter = new Ext.FormPanel({
        id: 'PanelTRDokter',
        fileUpload: true,
        region: 'north',
        layout: 'column',
        bodyStyle: 'padding:10px 10px 10px 10px',
        height:190,
        anchor: '100%',
        width: lebar,
        border: false,
        items: [getItemPanelInputGantidokterKasirIGD(lebar),getItemPanelButtonGantidokterKasirIGD(lebar)],
       	tbar:[]
    });
    var FormDepanDokter = new Ext.Panel({
	    id: 'FormDepanDokterKasirIGD',
	    region: 'center',
	    width: '100%',
	    anchor: '100%',
	    layout: 'form',
	    title: '',
	    bodyStyle: 'padding:15px',
	    border: true,
	    bodyStyle: 'background:#FFFFFF;',
	    shadhow: true,
	    items: [pnlTRGantiDokter]

	});
    return FormDepanDokter;
};
function getItemPanelInputGantidokterKasirIGD(lebar) {
    var items ={
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 0px',
		border:false,
		height:95,
	    items:[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
			    layout: 'form',
			    border: false,
			    items:[getItemPanelNoTransksiDokterKasirIGD(lebar)]
			}
		]
	};
    return items;
}
function getItemPanelNoTransksiDokterKasirIGD(lebar) {
    var items ={
	    layout: 'column',
	    border: false,
	    items:[
			{
			    columnWidth: 1.0,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:[
					{
					    xtype: 'textfield',
					    fieldLabel:  'Unit Asal ',
					    name: 'cmbUnitAsalKasirIGD',
					    id: 'cmbUnitAsalKasirIGD',
						value:rowSelectedKasirigdKasir.data.NAMA_UNIT,
						readOnly:true,
					    anchor: '100%'
					},{
					    xtype: 'textfield',
					    fieldLabel: 'Dokter Asal ',
					    name: 'cmbDokterAsalKasirIGD',
					    id: 'cmbDokterAsalKasirIGD',
						value:rowSelectedKasirigdKasir.data.NAMA_DOKTER,
						readOnly:true,
					    anchor: '100%'
						
					},
					mComboDokterGantiEntryKasirIGDGantiDok()
				]
			}
		]
	};
    return items;
}
function mComboDokterGantiEntryKasirIGDGantiDok(){ 
	var Field = ['KD_DOKTER','NAMA'];
    dsDokterGantiEntryKasirIGD = new WebApp.DataStore({fields: Field});
	var kDUnit = rowSelectedKasirigdKasir.data.KD_UNIT;
	var kddokter = rowSelectedKasirigdKasir.data.KD_DOKTER;
   	dsDokterGantiEntryKasirIGD.load({
     	params:{
			Skip: 0,
			Take: 1000,
			Sort: 'nama',
			Sortdir: 'ASC',
			target: 'ViewComboDokter',
			param: 'where dk.kd_unit=~'+ kDUnit+ '~ and d.kd_dokter not in (~'+kddokter+'~)'
		}
    });
    var cboDokterGantiEntryKasirIGDGantiDok = new Ext.form.ComboBox({
	    id: 'cboDokterRequestEntryKasirIGDGantiDok',
	    typeAhead: true,
	    triggerAction: 'all',
		name:'txtdokter',
	    lazyRender: true,
	    mode: 'local',
	    selectOnFocus:true,
        forceSelection: true,
	    emptyText:'Pilih Dokter...',
	    fieldLabel: 'Dokter Baru',
	    align: 'Right',
	    store: dsDokterGantiEntryKasirIGD,
	    valueField: 'KD_DOKTER',
	    displayField: 'NAMA',
		anchor:'100%',
	    listeners:{
		    'select': function(a,b,c){
				selectDokter = b.data.KD_DOKTER;
				NamaDokter = b.data.NAMA;
				Ext.get('txtKdDokter').dom.value = b.data.KD_DOKTER;
            },
            'render': function(c){
                c.getEl().on('keypress', function(e) {
                if(e.getKey() == 13)
                	Ext.getCmp('kelPasien').focus();
                }, c);
            }
		}
    });
    return cboDokterGantiEntryKasirIGDGantiDok;
};
function getItemPanelButtonGantidokterKasirIGD(lebar) {
    var items ={
	    layout: 'column',
	    border: false,
		height:39,
		anchor:'100%',
		style:{'margin-top':'-1px'},
	    items:[
			{
				layout: 'hBox',
				width:310,
				border: false,
				bodyStyle: 'padding:5px 0px 5px 5px',
				defaults: { margins: '3 3 3 3' },
				anchor: '90%',
				layoutConfig:{
					align: 'middle',
					pack:'end'
				},
				items:[
					{
						xtype:'button',
						text:'Simpan [Ctrl+s]',
						width:100,
						style:{'margin-left':'0px','margin-top':'0px'},
						hideLabel:true,
						id: 'btnOkGantiDokter',
						handler:function(){
							GantiDokterKasirIGD(false);
							//FormLookUpGantidokter.close();	
						}
					},{
						xtype:'button',
						text:'Tutup' ,
						width:70,
						hideLabel:true,
						id: 'btnCancelGantidokter',
						handler:function(){
							FormLookUpGantidokterKasirIGD.close();
						}
					}
				]
			}
		]
	};
    return items;
}
function GantiDokterKasirIGD_SQL(){
	Ext.Ajax.request({
		url: baseURL + "index.php/rawat_jalan_sql/functionRWJ/gantidokter",		
		params: getParamGantiDokterKasirIGD(),
		failure: function(o){
			 ShowPesanErrorKasirigd('SQL, Error, Hubungi Admin!','Ganti Dokter');
		},	
		success: function(o){
			var cst = Ext.decode(o.responseText);
			if (cst.success === true){
				/* FormDepanDokter.close();
				FormLookUpGantidokter.close(); */
			}else{
				ShowPesanErrorKasirigd('SQL, Gagal ganti Dokter!','Ganti Dokter');
			}
		}
	});
}
function GantiDokterKasirIGD(mBol){
    if (ValidasiGantiDokterKasriIGD(nmHeaderSimpanData,false) == 1 ){
        Ext.Ajax.request({
           	url: WebAppUrl.UrlUpdateData,
        	params: getParamGantiDokterKasirIGD(),
			failure: function(o){
				 ShowPesanWarningKasirigd('Ganti Dokter Gagal','Ganti Dokter');
			},	
            success: function(o){
                var cst = Ext.decode(o.responseText);
                if (cst.success === true){               	
                    RefreshDataFilterKasirIgdKasir();
					//GantiDokterKasirIGD_SQL();
                    ShowPesanInfoKasirigd('Berhasil ganti dokter','Ganti Dokter');
					//refreshDataDepanPenjasRWJ();
					//refeshkasirrwj();
					// Ext.get('txtKdDokter').dom.value = selectDokter;
					// Ext.get('txtNamaDokter').dom.value = NamaDokter;
                    FormLookUpGantidokterKasirIGD.close();
					
                }else if  (cst.success === false && cst.pesan===0){
                        ShowPesanWarningKasirigd('Ganti Dokter Gagal. '+ cst.pesan,'Ganti Dokter');
                }else{
                        ShowPesanWarningKasirigd('Ganti Dokter Gagal. '+ cst.pesan,'Ganti Dokter');
                }
            }
        });
    }else{
        if(mBol === true){
			return false;
        }
    }
}
function ValidasiGantiDokterKasriIGD(modul,mBolHapus){
	var x = 1;
	if ((Ext.get('cboDokterRequestEntryKasirIGDGantiDok').getValue() == '')){
	  	if (Ext.get('cboDokterRequestEntryKasirIGDGantiDok').getValue() === ''){
			x=0;
			if ( mBolHapus === false ){
				ShowPesanWarningKasirigd(nmGetValidasiKosong(nmSatuan),modul);
			}
		}
	}
	return x;
}
function getParamGantiDokterKasirIGD(){
	var tgl=new Date(rowSelectedKasirigdKasir.data.TANGGAL_TRANSAKSI);
    var params ={
        Table: 'ViewGantiDokterIGD',
		TxtMedRec : rowSelectedKasirigdKasir.data.KD_PASIEN,
		TxtTanggal:tgl.format("d/M/Y"),
		KdUnit :  rowSelectedKasirigdKasir.data.KD_UNIT,
		KdDokter : Ext.getCmp('cboDokterRequestEntryKasirIGDGantiDok').getValue(),
		kodebagian : 2,
		urut_masuk:rowSelectedKasirigdKasir.data.URUT_MASUK,
		no_trans:rowSelectedKasirigdKasir.data.NO_TRANSAKSI
	};
    return params;
};

function KelompokPasienLookUp_igd(rowdata) 
{
    var lebar = 440;
    FormLookUpsdetailTRKelompokPasien_igd = new Ext.Window
    (
        {
            id: 'gridKelompokPasienKasirIGD',
            title: 'Ganti Kelompok Pasien',
            closeAction: 'destroy',
            width: lebar,
            height: 260,
            border: false,
            resizable: false,
            plain: false,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryTRKelompokPasien_igd(lebar),
            listeners:
            {
                
            }
        }
    );

    FormLookUpsdetailTRKelompokPasien_igd.show();
    KelompokPasienbaru_igd();

};

function KelompokPasienbaru_igd() 
{
	jeniscus_IGD=0;
    KelompokPasienAddNew_RWJ = true;
    Ext.getCmp('cboKelompokpasien_IGD').show()
	Ext.getCmp('txtCustomer_igdLama').disable();
	Ext.get('txtCustomer_igdLama').dom.value=vCustomer;
	Ext.get('txtIGDNoSEP').dom.value = vNoSEP;
	
	RefreshDatacombo_igd(jeniscus_IGD);
};
function getItemPanelButtonKelompokPasien_igd(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
		height:30,
		anchor:'100%',
		style:{'margin-top':'-1px'},
	    items:
		[
			{
				layout: 'hBox',
				width:400,
				border: false,
				bodyStyle: 'padding:5px 0px 5px 5px',
				defaults: { margins: '3 3 3 3' },
				anchor: '90%',
				layoutConfig: 
				{
					align: 'middle',
					pack:'end'
				},
				items:
				[
					{
						xtype:'button',
						text:'Simpan',
						width:70,
						style:{'margin-left':'0px','margin-top':'0px'},
						hideLabel:true,
						id:Nci.getId(),
						handler:function()
						{
							if(panelActiveDataPasien == 0){
								Datasave_Kelompokpasien_igd();
							}
							if(panelActiveDataPasien == 1){
								Datasave_GantiDokter_igd();
							}
							if(panelActiveDataPasien == 2){
								Datasave_GantiUnit_igd();
							}
						}
					},
					{
						xtype:'button',
						text:'Tutup',
						width:70,
						hideLabel:true,
						id:Nci.getId(),
						handler:function() 
						{
							if(panelActiveDataPasien == 0){
								FormLookUpsdetailTRKelompokPasien_igd.close();
							}
							
							if(panelActiveDataPasien == 1){
								FormLookUpsdetailTRGantiDokter_igd.close();
							}
							
							if(panelActiveDataPasien == 2){
								FormLookUpsdetailTRKonsultasi_DataPasien.close();
							}
						}
					}
				]
			}
		]
	}
    return items;
};
function getFormEntryTRKelompokPasien_igd(lebar) 
{
    var pnlTRKelompokPasien_igd = new Ext.FormPanel
    (
        {
            id: 'PanelTRKelompokPasien_igd',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:250,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [
					getItemPanelInputKelompokPasien_igd(lebar),
					getItemPanelButtonKelompokPasien_igd(lebar)
			],
           tbar:
            [
               
               
            ]
        }
    );
 
    var FormDepanKelompokPasien = new Ext.Panel
	(
		{
		    id: 'FormDepanKelompokPasien',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [pnlTRKelompokPasien_igd	
				
			]

		}
	);

    return FormDepanKelompokPasien
};

function getItemPanelInputKelompokPasien_igd(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 0px',
		border:false,
		height:170,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
			    layout: 'form',
			    border: false,
			    items:
				[
					getKelompokpasienlama_igd(lebar),	
					getItemPanelNoTransksiKelompokPasien_igd(lebar)	,
					
				]
			}
		]
	};
    return items;
};
function getKelompokpasienlama_igd(lebar) 
{
    var items =
	{
		Width:lebar,
		height:40,
	    layout: 'column',
	    border: false,
		
	    items:
		[
			{
			    columnWidth: .990,
			    layout: 'form',
				Width:lebar-10,
				labelWidth:130,
			    border: true,
			    items:
				[{	 
						xtype: 'tbspacer',
						height: 2
						},
						{
							xtype: 'textfield',
							fieldLabel: 'Kelompok Pasien Asal',
							name: 'txtCustomer_igdLama',
							id: 'txtCustomer_igdLama',
							labelWidth:130,
							editable: false,
							width: 100,
							anchor: '95%'
						 }
					]
			}
			
		]
	}
    return items;
};
function getItemPanelNoTransksiKelompokPasien_igd(lebar) 
{
    var items =
	{
		Width:lebar,
		height:120,
	    layout: 'column',
	    border: false,
		
		
	    items:
		[
			{

			    columnWidth: .990,
			    layout: 'form',
				Width:lebar-10,
				labelWidth:130,
			    border: true,
			    items:
				[{	 
						xtype: 'tbspacer',
						height:3
					},{ 
					    xtype: 'combo',
						fieldLabel: 'Kelompok Pasien Baru',
						id: 'kelPasien_IGD',
						editable: false,
						store: new Ext.data.ArrayStore
							(
								{
								id: 0,
								fields:
								[
								'Id',
								'displayText'
								],
								   data: [[1, 'Perseorangan'], [2, 'Perusahaan'], [3, 'Asuransi']]
								}
							),
							  displayField: 'displayText',
							  mode: 'local',
							  width: 100,
							  forceSelection: true,
							  triggerAction: 'all',
							  emptyText: 'Pilih Salah Satu...',
							  selectOnFocus: true,
							  anchor: '95%',
							  listeners:
								 {
										'select': function(a, b, c)
									{
										if(b.data.displayText =='Perseorangan')
										{jeniscus_IGD='0'
											//Ext.getCmp('txtRWJNoSEP').disable();
											//Ext.getCmp('txtRWJNoAskes').disable();
											}
										else if(b.data.displayText =='Perusahaan')
										{jeniscus_IGD='1';
											//Ext.getCmp('txtRWJNoSEP').disable();
											//Ext.getCmp('txtRWJNoAskes').disable();
											}
										else if(b.data.displayText =='Asuransi')
										{jeniscus_IGD='2';
											//Ext.getCmp('txtRWJNoSEP').enable();
											//Ext.getCmp('txtRWJNoAskes').enable();
										}
										
										RefreshDatacombo_igd(jeniscus_IGD);
									}

								}
						},{
							columnWidth: .990,
							layout: 'form',
							border: false,
							labelWidth:130,
							items:
							[
												mComboKelompokpasien_IGD()
							]
						},{
							xtype: 'textfield',
							fieldLabel:'No SEP  ',
							name: 'txtIGDNoSEP',
							id: 'txtIGDNoSEP',
							width: 100,
							anchor: '99%'
						 }, {
                            xtype: 'textfield',
                            fieldLabel:'No Asuransi  ',
                            name: 'txtIGDNoAskes',
                            id: 'txtIGDNoAskes',
                            width: 100,
                            anchor: '99%',
                            value : vNoAsuransi,
						 }
									
				]
			}
			
		]
	}
    return items;
};

function mComboKelompokpasien_IGD()
{

var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viPJ_IGD = new WebApp.DataStore({fields: Field_poli_viDaftar});
	
	if (jeniscus_IGD===undefined || jeniscus_IGD==='')
	{
		jeniscus_IGD=0;
	}
	ds_customer_viPJ_IGD.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: 'jenis_cust=~'+ jeniscus_IGD +'~'
            }
        }
    )
    var cboKelompokpasien_IGD = new Ext.form.ComboBox
	(
		{
			id:'cboKelompokpasien_IGD',
			typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Pilih...',
                        fieldLabel: '',
                        align: 'Right',
                        anchor: '95%',
			store: ds_customer_viPJ_IGD,
			valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetKelompokpasien=b.data.displayText ;
					selectKdCustomer=b.data.KD_CUSTOMER;
					selectNamaCustomer=b.data.CUSTOMER;
				
				}
			}
		}
	);
	return cboKelompokpasien_IGD;
};


function RefreshDatacombo_igd(jeniscus_IGD) 
{

    ds_customer_viPJ_IGD.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: 'jenis_cust=~'+ jeniscus_IGD +'~ and kontraktor.kd_customer not in(~'+ vkode_customer_IGD+'~)'
            }
        }
    )
	
    return ds_customer_viPJ_IGD;
};

/* 
	============================= AJAX 
 */
 function ValidasiEntryUpdateKelompokPasien_IGD(modul,mBolHapus)
{
	var x = 1;
	
	if((Ext.get('kelPasien_IGD').getValue() == '') || (Ext.get('kelPasien_IGD').dom.value  === undefined ))
	{
		if (Ext.get('kelPasien_IGD').getValue() == '' && mBolHapus === true) 
		{
			ShowPesanWarningKasirigd(nmGetValidasiKosong('Kelompok Pasien'), modul);
			x = 0;
		}
	};
	return x;
};
function Datasave_Kelompokpasien_igd(mBol) 
{	
    if (ValidasiEntryUpdateKelompokPasien_IGD(nmHeaderSimpanData,false) == 1 )
    {           

        var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan ganti kelompok:', function(btn, combo){
            if (btn == 'ok')
            {
                if (combo!='') {
                    Ext.Ajax.request
                    (
                        {
                            url: baseURL +  "index.php/main/functionIGD/UpdateGantiKelompok",   
                            params: {
                                        KDCustomer  : selectKdCustomer,
                                        KDNoSJP     : Ext.get('txtIGDNoSEP').getValue(),
                                        KDNoAskes   : Ext.get('txtIGDNoAskes').getValue(),
                                        KdPasien    : CurrentKdPasien_KasirIGD,
                                        TglMasuk    : CurrentTglMasuk_KasirIGD,
                                        KdUnit      : CurrentKDUnitIGD,
                                        UrutMasuk   : CurrentUrutMasuk_KasirIGD,
                                        KdDokter    : CurrentKDDokerIGD,
                                        alasan      : combo,
                            },
                            failure: function(o)
                            {
                                ShowPesanWarningKasirigd('Simpan kelompok pasien gagal', 'Gagal');
                            },  
                            success: function(o) 
                            {
                                var cst = Ext.decode(o.responseText);
                                if (cst.success === true) 
                                {
                                    panelActiveDataPasien = 1;
                                    FormLookUpsdetailTRKelompokPasien_igd.close();
                                    RefreshDatahistoribayar(CurrentKdPasien_KasirIGD);
                                    /* Ext.getCmp('btnGantiKekompokPasien').disable(); 
                                    Ext.getCmp('btnGantiDokter').disable(); 
                                    Ext.getCmp('btnUnitPasien').disable();  
                                    Ext.getCmp('btnHpsdatapasien').disable();    */
                                    ShowPesanInfoKasirigd("Mengganti kelompok pasien berhasil", "Success");
									RefreshDataFilterKasirIgdKasir();

                                }else 
                                {
                                    panelActiveDataPasien = 1;
                                    ShowPesanWarningKasirigd('Simpan kelompok pasien gagal', 'Gagal');
                                };
                            }
                        }
                    );
                }else{
                    ShowPesanWarningKasirigd('Harap memasukkan alasan perpindahan', 'Peringatan');
                }
            }
        });
    }
    else
    {
        if(mBol === true)
        {
            return false;
        };
    };
	
};

function Datasave_GantiUnit_igd(mBol) 
{	
	if((Ext.get('cboDokterRequestEntry').getValue() == '') || (Ext.get('cboDokterRequestEntry').dom.value  === undefined ) || (Ext.get('cboDokterRequestEntry').dom.value  === 'Pilih Dokter...'))
	{
		ShowPesanWarningKasirigd('Dokter baru harap diisi', "Informasi");
	}else{	
			Ext.Ajax.request
			(
				{
					url: baseURL +  "index.php/main/functionIGD/UpdateGantiUnit",	
					params: getParamKelompokpasien_IGD(),
					failure: function(o)
					{
						ShowPesanWarningKasirigd('Simpan unit pasien gagal', 'Gagal');
						loadMask.hide();
					},	
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.totalrecords === 1) 
						{
                            panelActiveDataPasien = 1;
							vKdUnitDulu = "";
							FormLookUpsdetailTRKonsultasi_DataPasien.close();
							RefreshDatahistoribayar(CurrentKdPasien_KasirIGD);
							/* Ext.getCmp('btnGantiKekompokPasien').disable();	
							Ext.getCmp('btnGantiDokter').disable();	
                            Ext.getCmp('btnUnitPasien').disable();  
							Ext.getCmp('btnHpsdatapasien').disable(); */	
							ShowPesanInfoKasirigd("Mengganti unit pasien berhasil", "Success");
							RefreshDataFilterKasirIgdKasir();
						}else 
						{
							ShowPesanWarningKasirigd('Simpan   pasien gagal', 'Gagal');
						};
					}
				}
			 )
	}
	
};
function getParamKelompokpasien_IGD() 
{
	var params;
	
	params = {
		KdPasien  	: CurrentKdPasien_KasirIGD,
		TglMasuk  	: CurrentTglMasuk_KasirIGD,
		KdUnit 		: vKdUnitDulu,
		UrutMasuk  	: CurrentUrutMasuk_KasirIGD,
		KdDokter  	: CurrentKDDokerIGD,
		NoTransaksi	: CurrentNoTransaksi_KasirIGD,
		KdKasir  	: CurrentKdKasir_KasirIGD,
		KdUnitDulu  : CurrentKDUnitIGD,
	}
	
    return params
};
function cekDetailTransaksi() 
{	
	Ext.Ajax.request
	(
		{
			url: baseURL +  "index.php/main/functionRWJ/cekDetailTransaksi",	
			params: getParamKelompokpasien_IGD(),
			failure: function(o)
			{
				ShowPesanWarningKasirigd('Data gagal di periksa', 'Gagal');
				loadMask.hide();
			},	
			success: function(o) 
			{
				//console.log(o.responseText);
				if(o.responseText > 0){
					statusCekDetail = true;
				}else{
					statusCekDetail = false;
				}
			}
		}
	)
};
function KonsultasiDataPasienLookUp_igd(lebar){
	
    var lebar = 440;
    FormLookUpsdetailTRKonsultasi_DataPasien = new Ext.Window
    (
        {
            id: 'idKonsultasiDataPasienKasirIGD',
            title: 'Ganti Unit',
            closeAction: 'destroy',
            width: lebar,
            height: 260,
            border: false,
            resizable: false,
            plain: false,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryTRGantiUnit_igd(lebar),
            listeners:
            {
                
            }
        }
    );
    FormLookUpsdetailTRKonsultasi_DataPasien.show();
    //KonsultasiAddNew();
};



function getFormEntryTRGantiUnit_igd(lebar) 
{
    var pnlTRUnitPasien_igd = new Ext.FormPanel
    (
        {
            id: 'PanelTRGantiUnitKasirIGD',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:250,
            anchor: '100%',
            width: lebar-35,
            border: false,
            items: [
					getItemPanelInputGantiUnit_igd(lebar),
					getItemPanelButtonKelompokPasien_igd(lebar)
			],
           tbar:
            [
               
               
            ]
        }
    );
 
    var FormDepanGantiUnit = new Ext.Panel
	(
		{
		    id: 'FormDepanGantiUnitKasirIGD',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [pnlTRUnitPasien_igd]

		}
	);

    return FormDepanGantiUnit
};


function getItemPanelInputGantiUnit_igd(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 0px',
		border:false,
		height:170,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
			    layout: 'form',
			    border: false,
			    items:
				[	
					{
						xtype: 'textfield',
					    fieldLabel:  'Poli Asal ',
					    name: 'txtUnitAsal_DataPasien',
					    id: 'txtUnitAsal_DataPasien',
						value:namaunit,
						readOnly:true,
						width: 100,
						anchor: '99%'
					},
					mComboPoliklinikKasirIGD(),
					mComboDokterGantiEntryKasirIGD()
				]
			}
		]
	};
    return items;
};
function mComboPoliklinikKasirIGD(){
    var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});
	ds_Poli_viDaftar.load({
        params	:{
            Skip	: 0,
            Take	: 1000,
            Sort	: 'NAMA_UNIT',
            Sortdir	: 'ASC',
            target	: 'ViewSetupUnitB',
            param	: " parent<>'0' and kd_bagian='3'"
        }
    });
    var cboPoliklinikRequestEntryKasirIGD = new Ext.form.ComboBox({
        id				: 'cboPoliklinikRequestEntryKasirIGD',
        typeAhead		: true,
        triggerAction	: 'all',
		width			: 170,
        lazyRender		: true,
        mode			: 'local',
        selectOnFocus	: true,
        forceSelection	: true,
        emptyText		: 'Pilih Poliklinik...',
        fieldLabel		: 'Poliklinik ',
        align			: 'Right',
        store			: ds_Poli_viDaftar,
        valueField		: 'KD_UNIT',
        displayField	: 'NAMA_UNIT',
        anchor			: '100%',
        listeners:{
            select: function(a, b, c){
				vKdUnitDulu 	= b.data.KD_UNIT;
				vKdUnit  		= b.data.KD_UNIT;
				loaddatastoredokterKasirIGD();
            }
		}
    });
    return cboPoliklinikRequestEntryKasirIGD;
};
function mComboDokterGantiEntryKasirIGD(){ 
	/* var Field = ['KD_DOKTER','NAMA'];
    dsDokterRequestEntry = new WebApp.DataStore({fields: Field}); */
    var cboDokterGantiEntryKasirIGD = new Ext.form.ComboBox({
	    id: 'cboDokterRequestEntry',
	    typeAhead: true,
	    triggerAction: 'all',
		name:'txtdokter',
	    lazyRender: true,
	    mode: 'local',
	    selectOnFocus:true,
        forceSelection: true,
	    emptyText:'Pilih Dokter...',
	    fieldLabel: 'Dokter Baru',
	    align: 'Right',
	    store: dsDokterRequestEntry,
	    valueField: 'KD_DOKTER',
	    displayField: 'NAMA',
		anchor:'100%',
	    listeners:{
		    'select': function(a,b,c){
				vKdDokter = b.data.KD_DOKTER;
            },
		}
    });
    return cboDokterGantiEntryKasirIGD;
};
function loaddatastoredokterKasirIGD(){
	dsDokterRequestEntry.load({
         params	:{
            Skip	: 0,
		    Take	: 1000,
            Sort	: 'nama',
		    Sortdir	: 'ASC',
		    target	: 'ViewComboDokter',
		    param	: 'where dk.kd_unit=~'+ vKdUnit+ '~'
		}
    });
}
function mComboDefaultUnitKelasIGD(kdunit,namaunit){
	var Tree = Ext.tree;
	var tree = new Tree.TreePanel({
		id:'pickerDefaultUnitKelasIGD',
		xtype : 'mytreegrid',
		cls : 'x-treegrid',
		title: namaunit+' ('+KetLaboratorium+')', 
		animate:true, 
		useArrows:true,
		
		autoScroll:true,
		//loader: NavTreeLoader, 
		loader: new Ext.tree.TreeLoader({
			url: baseURL + 'index.php/general/daftarbarang/lookup_produk/'+kdunit, 
			requestMethod: 'GET',
			preloadChildren: true,
		}),
		enableDD:true,
		containerScroll: true,
		border: false,
		anchor:'100%',
        rootVisible: false,
		width: 200,
		height: '100%',
		dropConfig: {appendOnly:true},
		listeners: {
            'click': function(n)
            {
				//console.log(n.leaf);
				if (n.leaf == true) {
					var grid = Ext.getCmp('pickerDefaultUnitKelasIGD');
					grid.setTitle(namaunit+' ('+n.text+')');
					LoadDataStoreProdukKasirIGD(n.id);
					CompUnitKelasRWJ = n.id;
                }
            }
        },
	});
	
	//new Tree.TreeSorter(tree, {folderSort:true});
	
	// set the root node
	var root = new Tree.AsyncTreeNode({
		text: 'Ext JS', 
		odeType: 'async',
		draggable:false, 
		expanded: true,
	});
	tree.setRootNode(root);
	
	root.expand(false, false);
	return tree;
};
function LoadDataStoreProdukKasirIGD(kd_klas){
	var Field                = ['kd_produk','deskripsi', 'tarifx', 'kp_produk'];
	DataStoreProduk_KasirIGD = new WebApp.DataStore({ fields: Field });

	Ext.Ajax.request({
		url: baseURL +  "index.php/main/functionRWJ/getLookUpProdukList",
		params: {
			kd_klas 	: kd_klas,
			kd_unit: rowSelectedKasirigdKasir.data.KD_UNIT,
			kd_customer:rowSelectedKasirigdKasir.data.KD_CUSTOMER,
		},

		success: function(response) {
			//var mystore = Ext.data.StoreManager.lookup('CustomerDataStore');
			var cst       = Ext.decode(response.responseText);
			//dsDataPerawatPenindak_KASIR_RWI.load(myData);
			dsDataStoreGridPoduk.removeAll();
			for(var i     =0,iLen=cst['data'].length; i<iLen; i++){
				var recs = [],recType = DataStoreProduk_KasirIGD.recordType;
				var o    = cst['data'][i];
				recs.push(new recType(o));
				dsDataStoreGridPoduk.add(recs);
			}
		},
	});
}

function formLookUpProduk_KasirIGD(kdunit,namaunit) 
{
	
 
	LoadDataStoreProdukKasirIGD('');

	var chkgetTindakanKasirIGD = new Ext.grid.CheckColumn
	(
		{
			xtype: 'checkcolumn',
			width: 5,
			sortable: false,
			id: 'check1KasirIGD',
			dataIndex: 'checkProduk',
			editor: {
				xtype: 'checkbox',
				cls: 'x-grid-checkheader-editor'
			},
			listeners: {
				checkchange: function (column, recordIndex, checked) {
					alert(checked);
					alert("hi");
				}
			}
		}
	); 

    var GridTrDokterColumnModelLookUpProduk =  new Ext.grid.ColumnModel([
        new Ext.grid.RowNumberer(),
		chkgetTindakanKasirIGD,
        /*
			{
				header			: 'KD PRODUK',
				dataIndex		: 'kd_produk',
				width			: 25,
				menuDisabled	: true,
				hidden 			: true
			},*/
			{
				header			: 'KP PRODUK',
				dataIndex		: 'kp_produk',
				id				: 'col_kp_produk',
				width			: 15,
				menuDisabled	: true,
				hidden 			: false,
				filter: {},
			},
			{
				header			: 'DESKRIPSI',
				dataIndex		: 'deskripsi',
				id				: 'col_deskripsi',
				width			: 65,
				menuDisabled	: true,
				hidden 			: false,
				filter: {},
			},
			{
				header			: 'TARIF',
				dataIndex		: 'tarifx',
				width			: 35,
				menuDisabled	: true,
				hidden 			: false,
				renderer: function(v, params, record){
					 return formatCurrency(record.data.tarifx);
				 }
			},
			
        ]
    );
	
    GridDokterTrKasir_IGD= new Ext.grid.EditorGridPanel({
        id          : 'formGridDokterTrKasir_IGD',
        stripeRows  : true,
        width       : '100%',
        height      : '100%',
        store       : dsDataStoreGridPoduk,
        border      : true,
        frame       : false,
        autoScroll  : true,
		plugins 	: [new Ext.ux.grid.FilterRow()],
		selModel: new Ext.grid.RowSelectionModel
		(
			{
				singleSelect 	: true,
				listeners 		: {
				}
			}
		),
		cm          : GridTrDokterColumnModelLookUpProduk,
        listeners   : {
           /*  rowclick: function( $this, rowIndex, e )
            {
                //trcellCurrentTindakan_RWJ = rowIndex;
            }, */
            celldblclick: function(gridView,htmlElement,columnIndex,dataRecord){
                
            }
        },
        viewConfig  : {forceFit: true},
    });
    var lebar = 500;
    var LookUpProduk_KasirIGD = new Ext.Window
    (
        {
            id: 'winTRformLookUpProduk_KasirIGD',
            title: 'Look Up Produk',
            closeAction: 'destroy',
            width: 950,
            height: 400,
            layout: {
				type:'hbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
            resizable: false,
            iconCls: 'Request',
			constrain : true,    
			modal: true,
           	items: [ 
				mComboDefaultUnitKelasIGD(kdunit,namaunit),
				GridDokterTrKasir_IGD
			],
			tbar :
			[
				{
					xtype:'button',
					text:'Simpan',
					iconCls	: 'save',
					hideLabel:true,
					id: 'BtnOktrformLookUpProduk',
					handler:function()
					{
						
						var linePJasaIGD = gridDTLTRIGD.store.data.length;
						//console.log(dsTRDetailKasirRWJList);
						var jumlah_record_ceklis=0;
						for(var i = 0 ; i < dsDataStoreGridPoduk.getCount();i++)
						{	
							if (dsDataStoreGridPoduk.data.items[i].data.checkProduk == true) {
								tmp_data_produk = dsDataStoreGridPoduk.data.items[i].data;
								// console.log(tmp_data_produk);

								// PenataJasaRJ.dsGridTindakan.insert(PenataJasaRJ.dsGridTindakan.getCount(),PenataJasaRJ.func.getNullProduk());
								// console.log(dsTRDetailKasirRWJList.getRange()[linePJasaRWJ].get('KP_PRODUK'));
								var status_produk 	= false;
								var index_produk 	= linePJasaIGD;
								for (var j = 0; j < dsTRDetailKasirIGDList.totalLength; j++) {
									if (dsTRDetailKasirIGDList.data.items[j].data.KD_PRODUK == tmp_data_produk.kd_produk) {
										status_produk 	= true;
										index_produk 	= j;
										break;
									}else{
										status_produk 	= false;
										index_produk 	= linePJasaIGD;
									}
								}

								if (status_produk == false) {

									for(var x = 0 ; x < dsDataStoreGridPoduk.getCount();x++)
									{	
										if (dsDataStoreGridPoduk.data.items[x].data.checkProduk == true) {
											//PenataJasaRJ.dsGridTindakan.insert(PenataJasaRJ.dsGridTindakan.getCount(),PenataJasaRJ.func.getNullProduk());
											jumlah_record_ceklis+=1;
											
										}
									}
									TambahBarisIGD();

									dsTRDetailKasirIGDList.getRange()[index_produk].set('KP_PRODUK', 	tmp_data_produk.kp_produk);
									dsTRDetailKasirIGDList.getRange()[index_produk].set('KD_PRODUK', 	tmp_data_produk.kd_produk);
									dsTRDetailKasirIGDList.getRange()[index_produk].set('DESKRIPSI', 	tmp_data_produk.deskripsi);
									dsTRDetailKasirIGDList.getRange()[index_produk].set('TGL_BERLAKU', 	tmp_data_produk.tgl_berlaku);
									dsTRDetailKasirIGDList.getRange()[index_produk].set('KD_KLAS', 		tmp_data_produk.KD_KLAS);
									dsTRDetailKasirIGDList.getRange()[index_produk].set('HARGA', 		tmp_data_produk.tarifx);
									dsTRDetailKasirIGDList.getRange()[index_produk].set('KD_TARIF', 	tmp_data_produk.kd_tarif);
									dsTRDetailKasirIGDList.getRange()[index_produk].set('STATUS_KONSULTASI', tmp_data_produk.status_konsultasi);
									dsTRDetailKasirIGDList.getRange()[index_produk].set('QTY',1);
									var kasirIGDUrut=0;
									if(linePJasaIGD>=1){
										kasirIGDUrut=toInteger(dsTRDetailKasirIGDList.data.items[linePJasaIGD].data.URUT); 
									}
									dsTRDetailKasirIGDList.data.items[linePJasaIGD].data.URUT               = kasirIGDUrut+1;
									
									senderProdukIGD(
												notransaksi, 
												tmp_data_produk.kd_produk, 
												tmp_data_produk.kd_tarif, 
												CurrentKdKasir_KasirIGD, 
												kasirIGDUrut, 
												rowSelectedKasirigdKasir.data.KD_UNIT, 
												nowTglTransaksiIGD2,
												tmp_data_produk.tgl_berlaku,
												1,
												tmp_data_produk.tarifx,
												true,
												//linePJasaIGD,
												nowTglTransaksiIGD2
											);
									linePJasaIGD++;
									if (jumlah_record_ceklis===1){
										if (tmp_data_produk.status_konsultasi==true){
											var ayaan='ya';
											cekKomponen(tmp_data_produk.kd_produk,tmp_data_produk.kd_tarif,rowSelectedKasirigdKasir.data.KD_UNIT,kasirIGDUrut+1,tmp_data_produk.tarifx,ayaan);
										}else{
											Ext.getCmp('KasirIGD_gridTindakan').startEditing(linePJasaIGD,10);
											
										}
									}
									
								}else{
									var tmp_qty = 0;
									tmp_qty = dsTRDetailKasirIGDList.getRange()[index_produk].get('QTY');
									tmp_qty = parseInt(tmp_qty) + 1;
									dsTRDetailKasirIGDList.getRange()[index_produk].set('QTY', tmp_qty);
								}
							}
						} 
						LookUpProduk_KasirIGD.close();
					}
				},
				'-',
			],
            listeners:{ 
				close:function(){
					shortcut.remove('edit_dokter');
				},
				hide:function(){
					shortcut.remove('edit_dokter');
				}
            }
        }
    );
	LookUpProduk_KasirIGD.show();
	/* shortcut.set({
		code:'edit_dokter',
		list:[
			{
				key:'ctrl+s',
				fn:function(){
					Ext.getCmp('BtnOktrDokter').el.dom.click();
				}
			},{
				key:'esc',
				fn:function(){
					Ext.getCmp('winTRDokterPenindak_KasirRWJ').close();
				}
			}
		]
	}); */
	/* if(edit == true){
		GetgridEditDokterPenindakJasa_KasirRWJ()
	} else{
		GetgridPilihDokterPenindakJasa_KasirRWJ(currentJasaDokterKdProduk_KasirRWJ,currentJasaDokterKdTarif_KasirRWJ);
	} */
	
};
function PilihEditTarifLookUp_KasirIGD(data) 
{
	//console.log(dataRowIndexDetail);
	loaddatastoreTarifComponentKasirIGD(data);
        var formLookupEditTarif_KasirIGD = new Ext.Window
		(
			{
				id: 'formEditTarif_KASIRIGD',
				title: 'Edit data tarif',
				closeAction: 'destroy',
				width: 600,
				height: 400,
				border: false,
				resizable: false,
				plain: true,
				layout: 'fit',
				iconCls: 'Request',
				modal: true,
				bodyStyle:'padding: 3px;',
				items: [
                {
					columnWidth: .40,
					layout: 'form',
					labelWidth: 100,
					border: false,
					bodyStyle:'padding: 10px;',
					items:[
						{
							xtype 		: 'textfield',
							id 			: 'TxtNamaProduk',
							fieldLabel 	: 'Produk',
							anchor		: '100%',
							readOnly    : true,
                            value		: data.DESKRIPSI,
                            bodyStyle	: 'margin:10px;',
						},
						GridEditTarif_KasirIGD(),
                        // paneltotal
					]
				},
				],
				bbar:[  
					{
						text: 'Cito',
						id: 'btnHitungCito_KASIRIGD',
						tooltip: 'Hitung Cito',
						hidden:true,
						//iconCls: 'save',
						handler: function ()
						{
							PilihEditCitoLookUp_KasirIGD(data);
						},
						callback : function(){
							Ext.getCmp('TxtPercentaseTarif').focus();
						}
					},'-',
					{
						text: 'Discount',
						id: 'btnDiscount_KASIRIGD',
						tooltip: 'Hitung Discount',
						hidden:true,
						//iconCls: 'save',
						handler: function ()
						{
							//savePostingManualKasirRWI();
						}
					},'-',
					{
						xtype       : 'label',
						//text        :'Total :'
					},
					{xtype: 'tbspacer', width: 10},
					{
						xtype       : 'numberfield',
						id          : 'txtFieldTotalTarif',
						width       : 100,
						readOnly    : true,
					},
					{
						xtype       : 'label',
						//text        :'Total :'
					},
					{xtype: 'tbspacer', width: 10},
					{
						xtype       : 'numberfield',
						id          : 'txtFieldTotalMarkup',
						width       : 100,
						readOnly    : true,
					},
					{
						xtype       : 'label',
						//text        :'Total :'
					},
					{xtype: 'tbspacer', width: 10},
					{
						xtype       : 'numberfield',
						id          : 'txtFieldTotalDisk',
						width       : 100,
						readOnly    : true,
					},
					{
						xtype       : 'label',
						//text        :'Total :'
					},
					{xtype: 'tbspacer', width: 10},
					{
						xtype       : 'numberfield',
						id          : 'txtFieldTotalTarif_Baru',
						width       : 100,
						readOnly    : true,
					},
				],
                tbar:[
                    {
                        text: 'Simpan',
                        id: 'btnSimpanEditTarif_KASIR_IGD',
                        tooltip: 'Edit Tarif',
                        iconCls: 'save',
                        handler: function ()
                        { 
							Ext.Ajax.request({
								url 	: baseURL + "index.php/rawat_jalan/control_data_tarif_component/update_tarif",
								params 	: listDataParams_EditTarif_Kasir_IGD(0, false, false),
								success : function (o)
								{
									var cst = Ext.decode(o.responseText);
									if (cst.status === true) {
										ShowPesanInfoKasirigd('Tarif berhasil diperbarui', 'Informasi');
										dsTRDetailKasirIGDList.getRange()[dataRowIndexDetail].set('HARGA', cst.total_harga);
										var kuantiti=0;
										kuantiti=dsTRDetailKasirIGDList.getRange()[dataRowIndexDetail].get('QTY');
										dsTRDetailKasirIGDList.getRange()[dataRowIndexDetail].set('TOTAL', cst.total_harga * kuantiti);
									}else{
										ShowPesanWarningKasirigd('Tarif tidak berhasil diperbarui', 'Peringatan');
									}
								}
							});
                        }
					},{
						text: 'Reset',
						id: 'btnEditTarif_KASIR_IGD',
						iconCls: 'cancel',
						handler: function ()
						{
							dsDataStoreGridTarifKomponent.removeAll();
							loaddatastoreTarifComponentKasirIGD(data);
						}
					},        
                ],
				listeners:
				{
				
				}
            }
		);
        formLookupEditTarif_KasirIGD.show();
};

	function listDataParams_EditTarif_Kasir_IGD(persentase, mark, disc){
		//console.log(persentase+" "+mark+" "+disc);
		var data_grid = gridEditTarifKasirIGD.getSelectionModel().grid.store.data;
		var list      = "";
		// console.log(data_grid);
		// console.log(Ext.getCmp('gridEditTarifKasirrwi').getCount());
		for(var x=0; x<data_grid.length; x++){
			var result = data_grid.items[x].data;
			var y      = '';
			var z      = '@@##$$@@';

			var tmpMarkup 	= result.MARKUP;
			var hasilMarkup = result.TARIF;
			var tmpDisc  	= result.DISC;
			var hasilDisc  	= result.TARIF;
			var Totaltarif  = result.TARIF;

			if (persentase > 0 && mark === true) {
				hasilMarkup = (tmpMarkup / 100) * persentase;
				if (result.MARKUP > 0) {
					hasilMarkup = hasilMarkup + tmpMarkup;
				}
			}else{
				hasilMarkup	= result.MARKUP;
			}

			if (persentase > 0 && disc === true) {
				hasilDisc 	= tmpDisc / 100 * persentase;
				if (result.MARKUP > 0) {
					hasilDisc 	= hasilDisc + tmpDisc;
				}
			}else{
				hasilDisc 	= result.DISC;
			}

			y   = result.KD_COMPONENT
			y   += z + hasilMarkup
			y   += z + hasilDisc


			if (x === (data_grid.length - 1))
			{
				list += y
			} else
			{
				list += y + '##[[]]##'
			};
		}
		//return list;
		
		var params =
		{
			no_transaksi: notransaksi,
			list 		: list,
			urut 		: result.URUT,
			kd_kasir	: '06',
			total_tarif	: Ext.getCmp('txtFieldTotalTarif_Baru').getValue(),
		};
		return params
	}



    function GridEditTarif_KasirIGD(){
    	var rowLine = 0;
    	var rowData = "";
		dsDataStoreGridTarifKomponent.removeAll();        
	    var Field = ['TARIF','DISC','MARKUP'];
	    DataStoreGridKomponent = new WebApp.DataStore({ fields: Field });
    	//loaddatastoreProdukComponent();

		var cm = new Ext.grid.ColumnModel({
			// specify any defaults for each column
			defaults: {
				sortable: false // columns are not sortable by default           
			},
			columns: [
			{
				header 		: 'Kd Kasir',
				dataIndex 	: 'KD_KASIR',
				width 		: 200,
				hidden 		: true,
			}, 
			{
				header 		: 'No Transaksi',
				dataIndex 	: 'NO_TRANSAKSI',
				width 		: 200,
				hidden 		: true,
			}, 
			{
				header 		: 'Urut',
				dataIndex 	: 'URUT',
				width 		: 200,
				hidden 		: true,
			}, 
			{
				header 		: 'Tgl Transaksi',
				dataIndex 	: 'TGL_TRANSAKSI',
				width 		: 200,
				hidden 		: true,
			}, 
			{
				header 		: 'Kd Component',
				dataIndex 	: 'KD_COMPONENT',
				width 		: 200,
				hidden 		: true,
			}, 
			{
				header: 'Component',
				dataIndex: 'COMPONENT',
				width: 200
			}, 
			{
				header: 'Tarif Lama',
				dataIndex: 'TARIF',
                align: 'right',
				width: 75,
                renderer: function (v, params, record)
				{
					return formatCurrency(record.data.TARIF);
				},
			}, 
			{
				header: 'Markup',
				dataIndex: 'MARKUP',
				width: 75,
				editor: new Ext.form.NumberField
				(
					{
						id: 'fieldcolMarkKasirRWJ',
						allowBlank: true,
						enableKeyEvents: true,
						width: 30,
						enableKeyEvents : true,
						listeners : {
							specialkey : function(a, b, c){
								if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9){
									var tmpTotalPerhitungan = gridEditTarifKasirIGD.getStore().getRange()[rowLine].get('TARIF');
									tmpTotalPerhitungan = (tmpTotalPerhitungan+Ext.getCmp('fieldcolMarkKasirIGD').getValue())- gridEditTarifKasirIGD.getStore().getRange()[rowLine].get('DISC');
									gridEditTarifKasirIGD.getStore().getRange()[rowLine].set('TARIF_BARU', tmpTotalPerhitungan);
									gridEditTarifKasirIGD.getStore().getRange()[rowLine].set('MARKUP', Ext.getCmp('fieldcolMarkKasirIGD').getValue());

									var totalMarkup = 0;
									var totalDisc 	= 0;
									var totalTarif_Baru= 0;
					                for(var i = 0 ; i < dsDataStoreGridTarifKomponent.getCount();i++)
									{
										totalMarkup 	+= gridEditTarifKasirIGD.getStore().getRange()[i].get('MARKUP');
										totalDisc 		+= gridEditTarifKasirIGD.getStore().getRange()[i].get('DISC');
										totalTarif_Baru += gridEditTarifKasirIGD.getStore().getRange()[i].get('TARIF_BARU');
									}
									Ext.getCmp('txtFieldTotalMarkup').setValue(totalMarkup);
									Ext.getCmp('txtFieldTotalDisk').setValue(totalDisc);
									Ext.getCmp('txtFieldTotalTarif_Baru').setValue(totalTarif_Baru);
								}
							}
						},
					}
				),
			}, 
			{
				header: 'Discount',
				dataIndex: 'DISC',
				width: 75,
				editor: new Ext.form.NumberField
				(
					{
						id: 'fieldcolDiscKasirIGD',
						allowBlank: true,
						enableKeyEvents: true,
						width: 30,
						enableKeyEvents : true,
						listeners : {
							specialkey : function(a, b, c){
								if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9){
									var tmpTotalPerhitungan = gridEditTarifKasirIGD.getStore().getRange()[rowLine].get('TARIF');
									tmpTotalPerhitungan = (tmpTotalPerhitungan+gridEditTarifKasirIGD.getStore().getRange()[rowLine].get('MARKUP'))- Ext.getCmp('fieldcolDiscKasirIGD').getValue();
									gridEditTarifKasirIGD.getStore().getRange()[rowLine].set('TARIF_BARU', tmpTotalPerhitungan);
									gridEditTarifKasirIGD.getStore().getRange()[rowLine].set('DISC', Ext.getCmp('fieldcolDiscKasirIGD').getValue());

									var totalMarkup = 0;
									var totalDisc 	= 0;
									var totalTarif_Baru= 0;
					                for(var i = 0 ; i < dsDataStoreGridTarifKomponent.getCount();i++)
									{
										totalMarkup 	+= gridEditTarifKasirIGD.getStore().getRange()[i].get('MARKUP');
										totalDisc 		+= gridEditTarifKasirIGD.getStore().getRange()[i].get('DISC');
										totalTarif_Baru += gridEditTarifKasirIGD.getStore().getRange()[i].get('TARIF_BARU');
									}
									Ext.getCmp('txtFieldTotalMarkup').setValue(totalMarkup);
									Ext.getCmp('txtFieldTotalDisk').setValue(totalDisc);
									Ext.getCmp('txtFieldTotalTarif_Baru').setValue(totalTarif_Baru);
								}
							}
						},
					}
				),
			}, 
            {
				id: 'fieldcolTarifBaruKasirIGD',
	            header: 'Tarif Baru',
	            dataIndex: 'TARIF_BARU',
                align: 'right',
	            width: 75,
                editor: new Ext.form.TextField
                (
                        {
                            id: 'fieldcolTunairwi',
                            allowBlank: true,
                            enableKeyEvents: true,
                            width: 30,
                        }
                ),
                renderer: function (v, params, record)
				{
					return formatCurrency(record.data.TARIF_BARU);
				},
	        }]
	    });

        gridEditTarifKasirIGD = new Ext.grid.EditorGridPanel
        (
            {
				title: '',
				id: 'gridEditTarifKasirIGD',
				store: dsDataStoreGridTarifKomponent,
				clicksToEdit: 1, 
				editable: true,
				border: true,
				columnLines: true,
				frame: false,
				stripeRows       : true,
				trackMouseOver   : true,
				height: 250,
				width: 560,
				autoScroll: true,
				sm: new Ext.grid.CellSelectionModel
				(
					{
						singleSelect: true,
						listeners:
						{
						}
					}
				),
				sm: new Ext.grid.RowSelectionModel
				(
					{
						singleSelect: true,
						listeners:
						{
							rowselect: function (sm, row, rec)
							{
								rowLine = row;
							}
						}
					}
				),
                cm: cm,
                viewConfig: {
                    forceFit: true
                },  
            }
        );
        return gridEditTarifKasirIGD;
    };

    function loaddatastoreTarifComponentKasirIGD(params){
    	// console.log(params);
    	var rowpasien=rowSelectedKasirigdKasir.data;
        Ext.Ajax.request({
            url: baseURL +  "index.php/rawat_jalan/control_data_tarif_component/get_data_component",
            params: {
                no_transaksi 	: rowpasien.NO_TRANSAKSI,
                // no_transaksi 	: params.NO_TRANSAKSI,
                kd_produk 		: params.KD_PRODUK,
                kd_unit 		: params.KD_UNIT,
                kd_tarif 		: params.KD_TARIF,
                tgl_berlaku 	: params.TGL_BERLAKU,
                tgl_transaksi 	: params.TGL_TRANSAKSI,
                urut 			: params.URUT,
                kd_kasir 		: '06',
            },
            success: function(response) {
                //var mystore = Ext.data.StoreManager.lookup('CustomerDataStore');
                var cst  = Ext.decode(response.responseText);
				var totalTarif 	= 0;
				var totalMarkup = 0;
				var totalDisc 	= 0;
				var totalTarif_Baru= 0;
                //dsDataPerawatPenindak_KASIR_RWI.load(myData);
                for(var i=0,iLen=cst['data'].length; i<iLen; i++){
                    var recs    = [],recType = DataStoreGridKomponent.recordType;
                    var o       = cst['data'][i];
                    recs.push(new recType(o));
                    dsDataStoreGridTarifKomponent.add(recs);
                }
                for(var i = 0 ; i < dsDataStoreGridTarifKomponent.getCount();i++)
				{
					totalTarif 	+= dsDataStoreGridTarifKomponent.data.items[i].data.TARIF;
					totalMarkup += dsDataStoreGridTarifKomponent.data.items[i].data.MARKUP;
					totalDisc 	+= dsDataStoreGridTarifKomponent.data.items[i].data.DISC;
					totalTarif_Baru 	+= dsDataStoreGridTarifKomponent.data.items[i].data.TARIF_BARU;
					//console.log(dsDataStoreGridTarifKomponent.data.items[i].data.TARIF);
				}
				Ext.getCmp('txtFieldTotalTarif').setValue(totalTarif);
				Ext.getCmp('txtFieldTotalMarkup').setValue(totalMarkup);
				Ext.getCmp('txtFieldTotalDisk').setValue(totalDisc);
				Ext.getCmp('txtFieldTotalTarif_Baru').setValue(totalTarif_Baru);
            },
        });
    }
    
function GetDTLPreviewBilling(url, no_transaksi, kd_kasir) {
    new Ext.Window({
        title: 'Preview Billing',
        width: 1000,
        height: 600,
        constrain: true,
        modal: true,
        html: "<iframe  style ='width: 100%; height: 100%;' src='" + url+"/"+no_transaksi+"/"+kd_kasir+"'></iframe>",
        tbar : [
            {
                xtype   : 'button',
                text    : 'Cetak Direct',
                iconCls : 'print',
                handler : function(){
                    window.open(baseURL + "index.php/laporan/lap_billing/print_pdf/"+no_transaksi+"/"+kd_kasir, '_blank');
                }
            }
        ]
    }).show();
};

function LookUpEditKwitansiPanel(rowdata)
{
	// DISINI
	var no_kwitansi;
	// console.log(Ext.getCmp('gridDTLTRKasirigd').getStore().data.items);
	var items=Ext.getCmp('gridDTLTRKasirigd').getStore().data.items;
	var parameter = [];
	var xparameter = "";
	for(var i=0,iLen=items.length; i<iLen; i++){
		if(items[i].data.TAG==true){
			// parameter.push(items[i].data.URUT);
			xparameter += "'"+items[i].data.URUT+"',";
		}
	}

	var lebar          = 440;
	LookUpEditKwitansi = new Ext.Window
    (
        {
			id 			: 'panelEditKwitansi',
			title 		: 'Cetak Kwitansi',
			closeAction : 'destroy',
			width 		: lebar,
			height 		: 430,
			border 		: false,
			resizable 	: false,
			plain 		: false,
			layout 		: 'fit',
			iconCls 	: 'Request',
			modal 		: true,
			items: 	[
						{
							id 			: 'panelInnerEditKwitansi',
							region 		: 'north',
							layout 		: 'column',
							bodyStyle 	: 'padding:10px 10px 10px 10px',
							height 		: 420,
							anchor 		: '100%',
							width 		: lebar,
							border 		: false,
							items 		: [
								{

									layout 		: 'fit',
									anchor 		: '100%',
									width 		: lebar-35,
									labelAlign 	: 'right',
									bodyStyle 	: 'padding:10px 10px 10px 0px',
									border 		:false,
									height 		:340,
									items:
									[
										{
										    columnWidth : .9,
										    width 		: lebar -35,
											labelWidth 	: 100,
										    layout 		: 'form',
											height 		: 340,
										    border 		: false,
										    items:
											[ 
												{
													Width 		: lebar-2,
													height 		: 50,
												    layout 		: 'form',
												    border 		: true,
													bodyStyle 	: 'padding:10px 10px 10px 10px',
													labelWidth 	: 130,
												    items:
													[
														{
															xtype 		: 'textfield',
															fieldLabel 	: 'Kasir',
															maxLength 	: 200,
															name 		: 'txtKodeKasir',
															id 			: 'txtKodeKasir',
															width 		: 100,
															readOnly 	: true,
															anchor 		: '100%',
															value 		:  rowSelectedKasirigdKasir.data.KD_KASIR,
															style 		: {'text-align':'left'},
														},
													]
												},
												{
													xtype: 'tbspacer',
													height: 5
												},
												{
													Width 		: lebar-2,
													height 		: 260,
													layout 		: 'form',
													border 		: true,
													bodyStyle 	: 'padding:10px 10px 10px 10px',
													labelWidth 	: 130,
													items:
													[
														{
															xtype 		: 'textfield',
															fieldLabel 	: 'Medrec',
															maxLength 	: 200,
															name 		: 'txtKodePasien',
															id 			: 'txtKodePasien',
															width 		: 100,
															readOnly 	: true,
															anchor 		: '100%',
															value 		: Ext.getCmp('txtNoMedrecDetransaksi').getValue(),
															style 		: {'text-align':'left'},
														},
														{
															xtype 		: 'textfield',
															fieldLabel 	: 'No Transaksi',
															maxLength 	: 200,
															name 		: 'txtNoTransaksi',
															id 			: 'txtNoTransaksi',
															width 		: 100,
															readOnly 	: true,
															anchor 		: '100%',
															value 		: Ext.getCmp('txtNoTransaksiKasirigdKasir').getValue(),
															style 		: {'text-align':'left'},
														},
														{
															xtype 		: 'textfield',
															fieldLabel 	: 'No Kwitansi',
															maxLength 	: 200,
															name 		: 'txtNoKwitansi',
															id 			: 'txtNoKwitansi',
															width 		: 100,
															readOnly 	: true,
															anchor 		: '100%',
															// value 		: no_kwitansi,
															style 		: {'text-align':'left'},
														},
														{
															xtype 		: 'textfield',
															fieldLabel 	: 'Nama Pembayar',
															maxLength 	: 200,
															name 		: 'txtNamaPembayar',
															id 			: 'txtNamaPembayar',
															width 		: 100,
															readOnly 	: false,
															anchor 		: '100%',
															value 		: Ext.getCmp('txtNamaPasienDetransaksi').getValue(),
															style 		: {'text-align':'left'},
														},
														{
															xtype 		: 'textarea',
															fieldLabel 	: 'Untuk pembayaran',
															maxLength 	: 200,
															name 		: 'txtKeteranganBayar',
															id 			: 'txtKeteranganBayar',
															width 		: 100,
															readOnly 	: false,
															anchor 		: '100%',
															// value 		: "Untuk Pembayaran Biaya Rawat Jalan",
															style 		: {'text-align':'left'},
														},
														{
															xtype 		: 'textfield',
															fieldLabel 	: '',
															maxLength 	: 200,
															name 		: 'txtKeteranganPembayar',
															id 			: 'txtKeteranganPembayar',
															width 		: 100,
															readOnly 	: false,
															anchor 		: '100%',
															value 		: "a/n "+Ext.getCmp('txtNamaPasienDetransaksi').getValue(),
															style 		: {'text-align':'left'},
														},
														{
															xtype 		: 'textfield',
															fieldLabel 	: 'Unit yang dituju',
															maxLength 	: 200,
															name 		: 'txtUnitTujuan',
															id 			: 'txtUnitTujuan',
															width 		: 100,
															readOnly 	: true,
															anchor 		: '100%',
															// value 		: "a/n "+Ext.getCmp('txtNamaPasienDetransaksi').getValue(),
															style 		: {'text-align':'left'},
														},
														{
															xtype 		: 'textfield',
															fieldLabel 	: 'Jumlah bayar',
															maxLength 	: 200,
															name 		: 'txtJumlahBayar',
															id 			: 'txtJumlahBayar',
															width 		: 100,
															readOnly 	: false,
															anchor 		: '100%',
															// value 		: "a/n "+Ext.getCmp('txtNamaPasienDetransaksi').getValue(),
															style 		: {'text-align':'left'},
														},
													]
												},
											]
										},
									]
								}
							],
						},
					],
			fbar:[
				{
					xtype:'button',
					text:'Print',
					width:70,
					style:{'margin-left':'0px','margin-top':'0px'},
					hideLabel:true,
					id: 'btnOkPrint',
					handler:function(){
						var url_laporan = baseURL + "index.php/laporan/lap_kwitansi/";
						var params={
							kd_kasir 		: Ext.getCmp('txtKodeKasir').getValue(),
							kd_pasien 		: Ext.getCmp('txtKodePasien').getValue(),
							no_transaksi 	: Ext.getCmp('txtNoTransaksi').getValue(),
							no_kwitansi		: Ext.getCmp('txtNoKwitansi').getValue(),
							nama_pembayar	: Ext.getCmp('txtNamaPembayar').getValue(),
							ket_bayar		: Ext.getCmp('txtKeteranganBayar').getValue(),
							ket_pembayar	: Ext.getCmp('txtKeteranganPembayar').getValue(),
							unit_tujuan		: Ext.getCmp('txtUnitTujuan').getValue(),
							jumlah_bayar	: Ext.getCmp('txtJumlahBayar').getValue(),
							no_urut			: xparameter,
						};
						console.log(params);
						var form = document.createElement("form");
						form.setAttribute("method", "post");
						form.setAttribute("target", "_blank");
						form.setAttribute("action", url_laporan+"/print_");
						var hiddenField = document.createElement("input");
						hiddenField.setAttribute("type", "hidden");
						hiddenField.setAttribute("name", "data");
						hiddenField.setAttribute("value", Ext.encode(params));
						form.appendChild(hiddenField);
						document.body.appendChild(form);
						form.submit();  
						// printbillIGD();
					}
				},
				{
						xtype:'button',
						text:'Tutup',
						width:70,
						hideLabel:true,
						id: 'btnCancelTransfer',
						handler:function()
						{
							LookUpEditKwitansi.close();
						}
				}
			],
			listeners:
			{
			
			}
		}
	);

	Ext.Ajax.request({
		url 		: baseURL + "index.php/main/Controller_kunjungan/get_custom_data",
		params 		: {
			select 	: " max(no_nota) as kwitansi ",
			where 	: " kd_kasir = '"+ rowSelectedKasirigdKasir.data.KD_KASIR+"' ",
			table 	: " nota_bill ",
		},
		success: function(o){
			var cst 	= Ext.decode(o.responseText);
			no_kwitansi = parseInt(cst[0].kwitansi)+1;
			Ext.getCmp('txtNoKwitansi').setValue(no_kwitansi);
		}
	});

	Ext.Ajax.request({
		url 		: baseURL + "index.php/main/Controller_kunjungan/get_custom_data",
		params 		: {
			select 	: " no_transaksi, kd_unit ",
			where 	: " no_transaksi = '"+Ext.getCmp('txtNoTransaksiKasirigdKasir').getValue()+"' and kd_kasir = '"+ rowSelectedKasirigdKasir.data.KD_KASIR+"' ",
			table 	: " transaksi ",
		},
		success: function(o){
			var cst 	= Ext.decode(o.responseText);

			Ext.Ajax.request({
				url 		: baseURL + "index.php/main/Controller_kunjungan/get_custom_data",
				params 		: {
					select 	: " nama_unit, kd_unit ",
					where 	: " kd_unit = '"+cst[0].kd_unit+"' ",
					table 	: " unit ",
				},
				success: function(o){
					var cst 	= Ext.decode(o.responseText);
					Ext.getCmp('txtUnitTujuan').setValue(cst[0].nama_unit);
					Ext.getCmp('txtKeteranganBayar').setValue("Untuk Pembayaran Biaya "+cst[0].nama_unit);
				}
			});
		}
	});
	Ext.Ajax.request({
		url 		: baseURL + "index.php/main/Controller_kunjungan/get_harga_data",
		params 		: {
			select 		: " sum(qty*harga) as total ",
			field 		: " urut ",
			parameter 	: xparameter,
			where 		: " no_transaksi = '"+Ext.getCmp('txtNoTransaksiKasirigdKasir').getValue()+"' and kd_kasir = '"+rowSelectedKasirigdKasir.data.KD_KASIR+"'",
			table 		: " detail_transaksi ",
		},
		success: function(o){
			var cst 	= Ext.decode(o.responseText);
			Ext.getCmp('txtJumlahBayar').setValue(cst[0].total);
		}
	});
	LookUpEditKwitansi.show();
};