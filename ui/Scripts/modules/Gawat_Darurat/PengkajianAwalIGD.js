var dsDokterJaga;
var Field = ['KD_DOKTER', 'NAMA'];
dsDokterJaga = new WebApp.DataStore({fields: Field});
dsDokterJaga.load({
    params:{
        Skip: 0,
        Take: 100, 
        Sort: 'NAMA',
        Sortdir: 'ASC',
        target: 'ViewComboDokter',
        param: 'where d.jenis_dokter=1 '
    }
});
var dsDokterKonsultasi_1;
var Field = ['KD_DOKTER', 'NAMA'];
dsDokterKonsultasi_1 = new WebApp.DataStore({fields: Field});
dsDokterKonsultasi_1.load({
    params:{
        Skip: 0,
        Take: 100, 
        Sort: 'NAMA',
        Sortdir: 'ASC',
        target: 'ViewComboDokter',
        param: 'where d.jenis_dokter=1 '
    }
});
var dsDokterKonsultasi_2;
var Field = ['KD_DOKTER', 'NAMA'];
dsDokterKonsultasi_2 = new WebApp.DataStore({fields: Field});
dsDokterKonsultasi_2.load({
    params:{
        Skip: 0,
        Take: 100, 
        Sort: 'NAMA',
        Sortdir: 'ASC',
        target: 'ViewComboDokter',
        param: 'where d.jenis_dokter=1 '
    }
});
var dsDokterKonsultasi_3;
var Field = ['KD_DOKTER', 'NAMA'];
dsDokterKonsultasi_3 = new WebApp.DataStore({fields: Field});
dsDokterKonsultasi_3.load({
    params:{
        Skip: 0,
        Take: 100, 
        Sort: 'NAMA',
        Sortdir: 'ASC',
        target: 'ViewComboDokter',
        param: 'where d.jenis_dokter=1 '
    }
});
var dsDokterKonsultasi_4;
var Field = ['KD_DOKTER', 'NAMA'];
dsDokterKonsultasi_4 = new WebApp.DataStore({fields: Field});
dsDokterKonsultasi_4.load({
    params:{
        Skip: 0,
        Take: 100, 
        Sort: 'NAMA',
        Sortdir: 'ASC',
        target: 'ViewComboDokter',
        param: 'where d.jenis_dokter=1 '
    }
});
var dsPerawatTrease;
var Field = ['KD_DOKTER', 'NAMA'];
dsPerawatTrease = new WebApp.DataStore({fields: Field});
dsPerawatTrease.load({
    params:{
        Skip: 0,
        Take: 100, 
        Sort: 'NAMA',
        Sortdir: 'ASC',
        target: 'ViewDataDokter',
        param: 'where jenis_dokter=0 '
    }
});

var now = new Date();
var dsSpesialisasiPengkajian;
var ListHasilPengkajian;
var rowSelectedHasilPengkajian;
var dsKelasPengkajian;
var dsKamarPengkajian;
var dataSource_pengkajianAwal;
var rowSelected_viDaftarAwal;
var tmpkdpasien = '';
var tmpkdunit = '';
var tmpurutmasuk = '';
var tmptglmasuk = '';
var tmpjelas = '';
var tmpefakuasicairan = '';
var tmpS1S2Tunggal = '';
var tmpNyeridada = '';
var tmpCRT = '';
var tmpJVP = '';
var FormLookUpdetailPengkajian;
var tmpediting = '';
//1                                     
var RIWAYAT_UTAMA = '';
var RIWAYAT_PENYAKIT_SEKARANG = '';
var RIWAYAT_PENYAKIT_DAHULU = '';
//2
var NAFAS_PATEN = 'false';
var NAFAS_OBSTRUKTIF = 'false';
var NAFAS_JELAS = 'false';
var NAFAS_POLA_SIMETRI = 'false';
var NAFAS_POLA_ASIMETRI = 'false';
var NAFAS_SUARA_NORMAL = 'false';
var NAFAS_SUARA_HIPERSONOR = 'false';
var NAFAS_MENURUN = 'false';
var NAFAS_JENIS_NORMAL = 'false';
var NAFAS_JENIS_TACHYPNOE = 'false';
var NAFAS_JENIS_CHEYNESTOKES = 'false';
var NAFAS_JENIS_RETRACTIVE = 'false';
var NAFAS_JENIS_KUSMAUL = 'false';
var NAFAS_JENIS_DISPNOE = 'false';
var NAFAS_RR = 0;
var NAFAS_SUARA_WHEEZING = 'false';
var NAFAS_SUARA_RONCHI = 'false';
var NAFAS_SUARA_RALES = 'false';
var NAFAS_EVAKUASI_CAIRAN = 'false';
var NAFAS_JML_CAIRAN = 0;
var NAFAS_WARNA_CAIRAN = '';
var NAFAS_MASALAH = '';
//3
var JANTUNG_REGULER = 'false';
var JANTUNG_IRREGULER = 'false';
var JANTUNG_S1S2_TUNGGAL = 'false';
var JANTUNG_NYERI_DADA = 'false';
var JANTUNG_NYERI_DADA_KET = '';
var JANTUNG_BUNYI_MURMUR = 'false';
var JANTUNG_BUNYI_GALLOP = 'false';
var JANTUNG_BUNYI_BISING = 'false';
var JANTUNG_CRT = 'false';
var JANTUNG_AKRAL_HANGAT = 'false';
var JANTUNG_AKRAL_DINGIN = 'false';
var JANTUNG_PENINGKATAN_JVP = 'false';
var JANTUNG_UKURAN_CVP = 0;
var JANTUNG_WARNA_JAUNDICE = 'false';
var JANTUNG_WARNA_SIANOTIK = 'false';
var JANTUNG_WARNA_KEMERAHAN = 'false';
var JANTUNG_WARNA_PUCAT = 'false';
var JANTUNG_TEKANAN_DARAH = '';
var JANTUNG_NADI = '';
var JANTUNG_TEMP = 0;
var JANTUNG_MASALAH = '';
//4
var OTOT_SENDI_BEBAS = 'false';
var OTOT_SENDI_TERBATAS = 'false';
var OTOT_KEKUATAN = '';
var OTOT_ODEMA_EKSTRIMITAS = 'false';
var OTOT_KELAINAN_BENTUK = 'false';
var OTOT_KREPITASI = 'false';
var OTOT_MASALAH = '';
//5
var SYARAF_GCS_EYE = 'false';
var SYARAF_GCS_VERBAL = 'false';
var SYARAF_GCS_MOTORIK = 'false';
var SYARAF_GCS_TOTAL = 0;
var SYARAF_FISIOLOGIS_BRACHIALIS = 'false';
var SYARAF_FISIOLOGIS_PATELLA = 'false';
var SYARAF_FISIOLOGIS_ACHILLES = 'false';
var SYARAF_PATOLOGIS_CHODDOKS = 'false';
var SYARAF_PATOLOGIS_BABINSKI = 'false';
var SYARAF_PATOLOGIS_BUDZINZKY = 'false';
var SYARAF_PATOLOGIS_KERNIG = 'false';
var SYARAF_MASALAH = '';
var INDRA_PUPIL_ISOKOR = 'false';
var INDRA__PUPIL_ANISOKOR = 'false';
var INDRA__KONJUNGTIVA_ANEMIS = 'false';
var INDRA__KONJUNGTIVA_ICTERUS = 'false';
var INDRA__KONJUNGTIVA_TIDAK = 'false';
var INDRA_GANGGUAN_DENGAR = 'false';
var INDRA_OTORHEA = 'false';
var INDRA_BENTUK_HIDUNG = 'false';
var INDRA_BENTUK_HIDUNG_KET = '';
var INDRA_GANGGUAN_CIUM = 'false';
var INDRA_RHINORHEA = 'false';
var INDRA_MASALAH = '';
var KEMIH_KEBERSIHAN = 'false';
var KEMIH_ALAT_BANTU = 'false';
var KEMIH_JML_URINE = 0;
var KEMIH_WARNA_URINE = '';
var KEMIH_BAU = '';
var KEMIH_KANDUNG_MEMBESAR = 'false';
var KEMIH_NYERI_TEKAN = 'false';
var KEMIH_GANGGUAN_ANURIA = 'false';
var KEMIH_GANGGUAN_ALIGURIA = 'false';
var KEMIH_GANGGUAN_RETENSI = 'false';
var KEMIH_GANGGUAN_INKONTINENSIA = 'false';
var KEMIH_GANGGUAN_DISURIA = 'false';
var KEMIH_GANGGUANHEMATURIA = 'false';
var KEMIH_MASALAH = '';
var CERNA_MAKAN_HABIS = 'false';
var CERNA_MAKAN_KET = '';
var CERNA_MAKAN_FREKUENSI = 0;
var CERNA_JML_MINUM = 0;
var CERNA_JENIS_MINUM = '';
var CERNA_MULUT_BERSIH = 'false';
var CERNA_MULUT_KOTOR = 'false';
var CERNA_MULUT_BERBAU = 'false';
var CERNA_MUKOSA_LEMBAB = 'false';
var CERNA_MUKOSA_KERING = 'false';
var CERNA_MUKOSA_STOMATITIS = 'false';
var CERNA_TENGGOROKAN_SAKIT = 'false';
var CERNA_TENGGOROKAN_NYERI_TEKAN = 'false';
var CERNA_TENGGOROKAN_PEMBESARANTO = 'false';
var CERNA_PERUT_NORMAL = 'false';
var CERNA_PERUT_DISTENDED = 'false';
var CERNA_PERUT_METEORISMUS = 'false';
var CERNA_PERUT_NYERI_TEKAN = 'false';
var CERNA_PERUT_LOKASI_NYERI = '';
var CERNA_PERISTALTIK = 0;
var CERNA_TURGOR_KULIT = 'false';
var CERNA_PEMBESARAN_HEPAR = 'false';
var CERNA_HEMATEMESIS = 'false';
var CERNA_EVAKUASI_CAIRAN_ASCITES = 'false';
var CERNA_JML_CAIRAN_ASCITES = 0;
var CERNA_WARNA_CAIRAN_ASCITES = 'false';
var CERNA_FREK_BAB = 0;
var CERNA_KONSISTENSI = '';
var CERNA_BAU_BAB = '';
var CERNA_WARNA_BAB = '';
var CERNA_MASALAH = '';
var ENDOKRIN_TYROID = 'false';
var ENDOKRIN_HIPOGLIKEMIA = 'false';
var ENDOKRIN_LUKA_GANGREN = 'false';
var ENDOKRIN__PUS = 'false';
var ENDOKRIN_MASALAH = '';
var PERSONAL_MANDI = '';
var PERSONAL_SIKATGIGI = '';
var PERSONAL_KERAMAS = '';
var PERSONAL_GANTIPAKAIAN = '';
var PERSONAL_MASALAH = '';
var PSIKOSOSIAL_ORANGDEKAT = '';
var PSIKOSOSIAL_KEGIATAN_IBADAH = '';
var PSIKOSOSIAL_MASALAH = '';
var TERAPI_PENUNJANG = '';

// gridPemeriksaanPenunjangAwal.data_store.removeAll();
var cbo_tindak_lanjut_pemeriksa;
var DATA_ASKEP_IGD_AWAL               = {};
DATA_ASKEP_IGD_AWAL.airway            = {};
DATA_ASKEP_IGD_AWAL.breathing         = {};
DATA_ASKEP_IGD_AWAL.circulation       = {};
DATA_ASKEP_IGD_AWAL.ekg               = {};
DATA_ASKEP_IGD_AWAL.crt               = {};
DATA_ASKEP_IGD_AWAL.akral             = {};
DATA_ASKEP_IGD_AWAL.turgor_kulit      = {};
DATA_ASKEP_IGD_AWAL.warna_kulit       = {};
DATA_ASKEP_IGD_AWAL.riwayat_cairan    = {};
DATA_ASKEP_IGD_AWAL.disability        = {};
DATA_ASKEP_IGD_AWAL.exposure          = {};
DATA_ASKEP_IGD_AWAL.riwayat_kesehatan = {};
DATA_ASKEP_IGD_AWAL.kehamilan         = {};
DATA_ASKEP_IGD_AWAL.skala_nyeri       = {};
DATA_ASKEP_IGD_AWAL.resiko_jatuh      = {};
DATA_ASKEP_IGD_AWAL.psikologis        = {};
DATA_ASKEP_IGD_AWAL.status_sosial     = {};
DATA_ASKEP_IGD_AWAL.sensorik          = {};
DATA_ASKEP_IGD_AWAL.motorik           = {};
DATA_ASKEP_IGD_AWAL.kognitif          = {};
DATA_ASKEP_IGD_AWAL.tanda_vital       = {};
DATA_ASKEP_IGD_AWAL.lanjutan          = {};
DATA_ASKEP_IGD_AWAL.pemeriksaan       = {};
DATA_ASKEP_IGD_AWAL.kekuatan_otot     = {};
DATA_ASKEP_IGD_AWAL.tindak_lanjut     = {};
DATA_ASKEP_IGD_AWAL.serah_terima      = {};
DATA_ASKEP_IGD_AWAL.trease            = {};
DATA_ASKEP_IGD_AWAL.gizi              = {};
DATA_ASKEP_IGD_AWAL.respontime        = {};
DATA_ASKEP_IGD_AWAL.airway.paten                           = false;
DATA_ASKEP_IGD_AWAL.airway.snoring                         = false;
DATA_ASKEP_IGD_AWAL.airway.gurgling                        = false;
DATA_ASKEP_IGD_AWAL.airway.stridor                         = false;
DATA_ASKEP_IGD_AWAL.airway.wheezing                        = false;
DATA_ASKEP_IGD_AWAL.breathing.rr                           = "";
DATA_ASKEP_IGD_AWAL.breathing.bantu_nafas                  = false;
DATA_ASKEP_IGD_AWAL.breathing.gerakan_dada_simetris        = false;
DATA_ASKEP_IGD_AWAL.breathing.gerakan_dada_asimetris       = false;
DATA_ASKEP_IGD_AWAL.breathing.gerakan_dada_asidosis        = false;
DATA_ASKEP_IGD_AWAL.breathing.gerakan_dada_alkalosis       = false;
DATA_ASKEP_IGD_AWAL.breathing.gerakan_dada_sao2            = false;
DATA_ASKEP_IGD_AWAL.breathing.gerakan_dada_sao2_percentage = "";
DATA_ASKEP_IGD_AWAL.breathing.suhu_badan                   = "";
DATA_ASKEP_IGD_AWAL.breathing.riwayat_demam                = "";
DATA_ASKEP_IGD_AWAL.breathing.riwayat_penyakit_hipertensi  = false;
DATA_ASKEP_IGD_AWAL.breathing.riwayat_penyakit_diabetes    = false;
DATA_ASKEP_IGD_AWAL.breathing.riwayat_alergi               = false;
DATA_ASKEP_IGD_AWAL.breathing.riwayat_alergi_detail        = false;
DATA_ASKEP_IGD_AWAL.circulation.hr                         = "";
DATA_ASKEP_IGD_AWAL.circulation.teratur                    = false;
DATA_ASKEP_IGD_AWAL.circulation.tidak_teratur              = false;
DATA_ASKEP_IGD_AWAL.circulation.kuat                       = false;
DATA_ASKEP_IGD_AWAL.circulation.lemah                      = false;
DATA_ASKEP_IGD_AWAL.circulation.td                         = "";
DATA_ASKEP_IGD_AWAL.ekg.irama_teratur                      = false;
DATA_ASKEP_IGD_AWAL.ekg.irama_tidak_teratur                = false;
DATA_ASKEP_IGD_AWAL.ekg.stemi                              = false;
DATA_ASKEP_IGD_AWAL.ekg.nstemi                             = false;
DATA_ASKEP_IGD_AWAL.crt.kurang                             = false;
DATA_ASKEP_IGD_AWAL.crt.lebih                              = true;
DATA_ASKEP_IGD_AWAL.akral.hangat                           = true;
DATA_ASKEP_IGD_AWAL.akral.dingin                           = false;
DATA_ASKEP_IGD_AWAL.turgor_kulit.normal                    = true;
DATA_ASKEP_IGD_AWAL.turgor_kulit.kurang                    = false;
DATA_ASKEP_IGD_AWAL.warna_kulit.pucat                      = true;
DATA_ASKEP_IGD_AWAL.warna_kulit.sianosis                   = false;
DATA_ASKEP_IGD_AWAL.warna_kulit.pink                       = false;
DATA_ASKEP_IGD_AWAL.riwayat_cairan.diare                   = false;
DATA_ASKEP_IGD_AWAL.riwayat_cairan.muntah                  = false;
DATA_ASKEP_IGD_AWAL.riwayat_cairan.luka_bakar              = false;
DATA_ASKEP_IGD_AWAL.riwayat_cairan.perdarahan              = false;
DATA_ASKEP_IGD_AWAL.disability._3to8                       = false;
DATA_ASKEP_IGD_AWAL.disability._9to13                      = false;
DATA_ASKEP_IGD_AWAL.disability._14to15                     = false;
DATA_ASKEP_IGD_AWAL.disability.isokor                      = true;
DATA_ASKEP_IGD_AWAL.disability.anisokor                    = false;
DATA_ASKEP_IGD_AWAL.disability._1mm                        = true;
DATA_ASKEP_IGD_AWAL.disability._2mm                        = false;
DATA_ASKEP_IGD_AWAL.disability._3mm                        = false;
DATA_ASKEP_IGD_AWAL.disability._4mm                        = false;
DATA_ASKEP_IGD_AWAL.exposure.ekskoriatum             = false;
DATA_ASKEP_IGD_AWAL.exposure.laseratum               = false;
DATA_ASKEP_IGD_AWAL.exposure.morsum                  = false;
DATA_ASKEP_IGD_AWAL.exposure.punctum                 = false;
DATA_ASKEP_IGD_AWAL.exposure.sklopirotum             = false;
DATA_ASKEP_IGD_AWAL.exposure.luka_bakar              = false;
DATA_ASKEP_IGD_AWAL.exposure.luka_bakar_luas         = "";
DATA_ASKEP_IGD_AWAL.exposure.luka_bakar_derajat      = "";
DATA_ASKEP_IGD_AWAL.exposure.luka_bakar_luas_luka    = "";
DATA_ASKEP_IGD_AWAL.exposure.luka_bakar_lokasi_jejas = "";
DATA_ASKEP_IGD_AWAL.riwayat_kesehatan.keluhan_utama     = "";
DATA_ASKEP_IGD_AWAL.riwayat_kesehatan.penyakit_sekarang = "";
DATA_ASKEP_IGD_AWAL.riwayat_kesehatan.penyakit_dahulu   = "";
DATA_ASKEP_IGD_AWAL.kehamilan.hpht   = "";
DATA_ASKEP_IGD_AWAL.kehamilan.g      = "";
DATA_ASKEP_IGD_AWAL.kehamilan.p      = "";
DATA_ASKEP_IGD_AWAL.kehamilan.a      = "";
DATA_ASKEP_IGD_AWAL.kehamilan.h      = "";
DATA_ASKEP_IGD_AWAL.kehamilan.minggu = "";
DATA_ASKEP_IGD_AWAL.skala_nyeri.ringan = false;
DATA_ASKEP_IGD_AWAL.skala_nyeri.sedang = false;
DATA_ASKEP_IGD_AWAL.skala_nyeri.berat  = false;
DATA_ASKEP_IGD_AWAL.skala_nyeri.akut   = false;
DATA_ASKEP_IGD_AWAL.skala_nyeri.kronis = false;
DATA_ASKEP_IGD_AWAL.skala_nyeri.lokasi = "";
DATA_ASKEP_IGD_AWAL.skala_nyeri.durasi = "";
DATA_ASKEP_IGD_AWAL.resiko_jatuh.rendah = false;
DATA_ASKEP_IGD_AWAL.resiko_jatuh.sedang = false;
DATA_ASKEP_IGD_AWAL.resiko_jatuh.tinggi = false;
DATA_ASKEP_IGD_AWAL.psikologis.psikologis_tenang          = false;
DATA_ASKEP_IGD_AWAL.psikologis.psikologis_cemas           = false;
DATA_ASKEP_IGD_AWAL.psikologis.psikologis_takut           = false;
DATA_ASKEP_IGD_AWAL.psikologis.psikologis_marah           = false;
DATA_ASKEP_IGD_AWAL.psikologis.psikologis_sedih           = false;
DATA_ASKEP_IGD_AWAL.psikologis.psikologis_bunuh_diri      = false;
DATA_ASKEP_IGD_AWAL.psikologis.psikologis_lain            = false;
DATA_ASKEP_IGD_AWAL.psikologis.psikologis_lain_keterangan = "";
DATA_ASKEP_IGD_AWAL.status_sosial.status_marita     = 0;
DATA_ASKEP_IGD_AWAL.status_sosial.kd_pekerjaan      = 0;
DATA_ASKEP_IGD_AWAL.sensorik.penglihatan            = "";
DATA_ASKEP_IGD_AWAL.sensorik.penciuman              = 0;
DATA_ASKEP_IGD_AWAL.sensorik.pendengaran_normal     = false;
DATA_ASKEP_IGD_AWAL.sensorik.pendengaran_tuli_kanan = false;
DATA_ASKEP_IGD_AWAL.sensorik.pendengaran_tuli_kiri  = false;
DATA_ASKEP_IGD_AWAL.sensorik.pendengaran_alat_kiri  = false;
DATA_ASKEP_IGD_AWAL.sensorik.pendengaran_alat_kanan = false;
DATA_ASKEP_IGD_AWAL.motorik.aktifitas    = 0;
DATA_ASKEP_IGD_AWAL.motorik.berjalan     = 0;
DATA_ASKEP_IGD_AWAL.kognitif.kognitif    = 0;
DATA_ASKEP_IGD_AWAL.tanda_vital.keadaan_umum_td          = "";
DATA_ASKEP_IGD_AWAL.tanda_vital.keadaan_umum_mmhg        = "";
DATA_ASKEP_IGD_AWAL.tanda_vital.keadaan_umum_suhu        = "";
DATA_ASKEP_IGD_AWAL.tanda_vital.keadaan_umum_nadi        = "";
DATA_ASKEP_IGD_AWAL.tanda_vital.keadaan_umum_nafas       = "";
DATA_ASKEP_IGD_AWAL.tanda_vital.keadaan_umum_sao2        = "";
DATA_ASKEP_IGD_AWAL.tanda_vital.imunisasi                = 0;
DATA_ASKEP_IGD_AWAL.tanda_vital.alergi_obat              = false;
DATA_ASKEP_IGD_AWAL.tanda_vital.alergi_makanan           = false;
DATA_ASKEP_IGD_AWAL.tanda_vital.alergi_lain_lain         = false;
DATA_ASKEP_IGD_AWAL.tanda_vital.diagnosa_kerja           = "";
DATA_ASKEP_IGD_AWAL.lanjutan.resusitasi                  = false;
DATA_ASKEP_IGD_AWAL.lanjutan.anak                        = false;
DATA_ASKEP_IGD_AWAL.lanjutan.obgin                       = false;
DATA_ASKEP_IGD_AWAL.lanjutan.medikal                     = false;
DATA_ASKEP_IGD_AWAL.lanjutan.surgikal                    = false;
DATA_ASKEP_IGD_AWAL.pemeriksaan.ku                       = 0;
DATA_ASKEP_IGD_AWAL.pemeriksaan.gcs                      = "";
DATA_ASKEP_IGD_AWAL.pemeriksaan.e                        = "";
DATA_ASKEP_IGD_AWAL.pemeriksaan.m                        = "";
DATA_ASKEP_IGD_AWAL.pemeriksaan.v                        = "";
DATA_ASKEP_IGD_AWAL.pemeriksaan.catatan                  = "";
DATA_ASKEP_IGD_AWAL.pemeriksaan.kesadaran_cm             = false;
DATA_ASKEP_IGD_AWAL.pemeriksaan.kesadaran_apatis         = false;
DATA_ASKEP_IGD_AWAL.pemeriksaan.kesadaran_somnolen       = false;
DATA_ASKEP_IGD_AWAL.pemeriksaan.kesadaran_sopor          = false;
DATA_ASKEP_IGD_AWAL.pemeriksaan.kesadaran_koma           = false;
DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_1                 = false;
DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_1_text            = "";
DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_2                 = false;
DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_2_text            = "";
DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_3                 = false;
DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_3_text            = "";
DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_4                 = false;
DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_4_text            = "";
DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_5                 = false;
DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_5_text            = "";
DATA_ASKEP_IGD_AWAL.tindak_lanjut.keadaan                = 0;
DATA_ASKEP_IGD_AWAL.tindak_lanjut.td                     = "";
DATA_ASKEP_IGD_AWAL.tindak_lanjut.nadi                   = "";
DATA_ASKEP_IGD_AWAL.tindak_lanjut.suhu                   = "";
DATA_ASKEP_IGD_AWAL.tindak_lanjut.pernapasan             = "";
DATA_ASKEP_IGD_AWAL.tindak_lanjut.tindak_lanjut          = 1;
DATA_ASKEP_IGD_AWAL.tindak_lanjut.konsultasi             = "";
DATA_ASKEP_IGD_AWAL.tindak_lanjut.konsultasi_1             = "";
DATA_ASKEP_IGD_AWAL.tindak_lanjut.konsultasi_2             = "";
DATA_ASKEP_IGD_AWAL.tindak_lanjut.konsultasi_3             = "";
DATA_ASKEP_IGD_AWAL.tindak_lanjut.konsultasi_4             = "";
DATA_ASKEP_IGD_AWAL.tindak_lanjut.telepon                = false;
DATA_ASKEP_IGD_AWAL.tindak_lanjut.on_site                = false;
DATA_ASKEP_IGD_AWAL.tindak_lanjut.atas_persetujuan       = false;
DATA_ASKEP_IGD_AWAL.tindak_lanjut.persetujuan_diri       = false;
DATA_ASKEP_IGD_AWAL.tindak_lanjut.dead					 = false; //2019-04-18
DATA_ASKEP_IGD_AWAL.tindak_lanjut.kontrol                = false;
DATA_ASKEP_IGD_AWAL.tindak_lanjut.tanggal                = now.format('Y-m-d');
DATA_ASKEP_IGD_AWAL.tindak_lanjut.jam_pulang             = now.format('H:i:s');
DATA_ASKEP_IGD_AWAL.tindak_lanjut.tgl_pulang             = now.format('Y-m-d');
DATA_ASKEP_IGD_AWAL.tindak_lanjut.terapi_pulang          = "";
DATA_ASKEP_IGD_AWAL.tindak_lanjut.edukasi_pasien         = false;
DATA_ASKEP_IGD_AWAL.tindak_lanjut.edukasi_keluarga       = false;
DATA_ASKEP_IGD_AWAL.tindak_lanjut.edukasi_tidak          = false;
DATA_ASKEP_IGD_AWAL.tindak_lanjut.karena                 = "";
DATA_ASKEP_IGD_AWAL.tindak_lanjut.dirujuk_ke             = "";
DATA_ASKEP_IGD_AWAL.tindak_lanjut.alasan_dirujuk         = "";
DATA_ASKEP_IGD_AWAL.tindak_lanjut.dokter_pemeriksa       = "";
DATA_ASKEP_IGD_AWAL.tindak_lanjut.dokter_pemeriksa_label = "";
DATA_ASKEP_IGD_AWAL.serah_terima.dokter_jaga             = "";
DATA_ASKEP_IGD_AWAL.serah_terima.dokter_jaga             = "";
DATA_ASKEP_IGD_AWAL.serah_terima.perawat_trease          = "";
DATA_ASKEP_IGD_AWAL.serah_terima.waktu                   = now;
DATA_ASKEP_IGD_AWAL.trease.kd_trease                     = "";
DATA_ASKEP_IGD_AWAL.trease.jalan_nafas_ancaman 				= false;
DATA_ASKEP_IGD_AWAL.trease.jalan_nafas_sumbatan 				= false;
DATA_ASKEP_IGD_AWAL.trease.jalan_nafas_bebas 				= false;
DATA_ASKEP_IGD_AWAL.trease.pernafasan_henti_nafas 			= false;
DATA_ASKEP_IGD_AWAL.trease.pernafasan_bradipnoe              = false;
DATA_ASKEP_IGD_AWAL.trease.pernafasan_sianosis               = false;
DATA_ASKEP_IGD_AWAL.trease.pernafasan_takipnoe               = false;
DATA_ASKEP_IGD_AWAL.trease.pernafasan_mengi                  = false;
DATA_ASKEP_IGD_AWAL.trease.pernafasan_normal                 = false;
DATA_ASKEP_IGD_AWAL.trease.pernafasan_frekuensi_nafas_normal = false;
DATA_ASKEP_IGD_AWAL.trease.sirkulasi_henti_jantung           = false;
DATA_ASKEP_IGD_AWAL.trease.sirkulasi_nadi_tidak_teraba       = false;
DATA_ASKEP_IGD_AWAL.trease.sirkulasi_nadi_teraba_lemah       = false;
DATA_ASKEP_IGD_AWAL.trease.sirkulasi_nadi_kuat               = false;
DATA_ASKEP_IGD_AWAL.trease.sirkulasi_akral_dingin            = false;
DATA_ASKEP_IGD_AWAL.trease.sirkulasi_brakikardi              = false;
DATA_ASKEP_IGD_AWAL.trease.sirkulasi_takikardi               = false;
DATA_ASKEP_IGD_AWAL.trease.sirkulasi_pucat                   = false;
DATA_ASKEP_IGD_AWAL.trease.sirkulasi_tds_lebih_160           = false;
DATA_ASKEP_IGD_AWAL.trease.sirkulasi_tds_lebih_100           = false;
DATA_ASKEP_IGD_AWAL.trease.sirkulasi_frekuensi_nadi_normal   = false;
DATA_ASKEP_IGD_AWAL.trease.sirkulasi_tdd_140_to_180          = false;
DATA_ASKEP_IGD_AWAL.trease.sirkulasi_tdd_90_to_100           = false;
DATA_ASKEP_IGD_AWAL.trease.sirkulasi_td_normal               = false;
DATA_ASKEP_IGD_AWAL.trease.kesadaran_gcs_kurang_sama_8       = false;
DATA_ASKEP_IGD_AWAL.trease.kesadaran_gcs_9_sampai_12         = false;
DATA_ASKEP_IGD_AWAL.trease.kesadaran_gcs_lebih_12            = false;
DATA_ASKEP_IGD_AWAL.trease.kesadaran_gcs_15                  = false;
DATA_ASKEP_IGD_AWAL.trease.kesadaran_kejang                  = false;
DATA_ASKEP_IGD_AWAL.trease.kesadaran_tidak_ada_respon        = false;
DATA_ASKEP_IGD_AWAL.trease.kesadaran_gelisah              	= false;
DATA_ASKEP_IGD_AWAL.trease.kesadaran_hemiparese              = false;
DATA_ASKEP_IGD_AWAL.trease.kesadaran_nyeri_dada              = false;
DATA_ASKEP_IGD_AWAL.trease.kesadaran_apatis                  = false;
DATA_ASKEP_IGD_AWAL.trease.kesadaran_somnolen                = false;
DATA_ASKEP_IGD_AWAL.gizi.gizi_skor_penurunan_bb              = 0;
DATA_ASKEP_IGD_AWAL.gizi.gizi_skor_sulit_makan               = 0;
DATA_ASKEP_IGD_AWAL.gizi.gizi_khusus                         = false;
DATA_ASKEP_IGD_AWAL.pemeriksaan.diagnosa                     = "";
var tanggal = new Date();
DATA_ASKEP_IGD_AWAL.respontime.waktu_datang_tgl  = tanggal.format('Y-m-d');
DATA_ASKEP_IGD_AWAL.respontime.waktu_datang_jam  = tanggal.format('H:i:s');
DATA_ASKEP_IGD_AWAL.respontime.waktu_periksa_tgl = tanggal.format('Y-m-d');
DATA_ASKEP_IGD_AWAL.respontime.waktu_periksa_jam = tanggal.format('H:i:s');
// DISINI

var Field = ['KD_PEKERJAAN', 'PEKERJAAN'];
var ds_pekerjaan = new WebApp.DataStore({fields: Field});
CurrentPage.page = gridListHasilPengkajianAwal(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
function gridListHasilPengkajianAwal(mod_id) {
    var Field = ['KD_DOKTER','NO_TRANSAKSI','KD_UNIT','KD_PASIEN','NAMA','NAMA_UNIT','ALAMAT','TANGGAL_TRANSAKSI','KD_PEKERJAAN','PEKERJAAN','STATUS_MARITA',
        'NAMA_DOKTER','ANAMNESE','CAT_FISIK','KD_CUSTOMER','CUSTOMER','URUT_MASUK','POSTING_TRANSAKSI','KD_KASIR','CARA_PENERIMAAN','KD_KELAS','KD_TRIASE'];
    dataSource_pengkajianAwal = new WebApp.DataStore({
        fields: Field
    });
    load_pengkajian("ng.AKHIR = 't' limit 50");
    var gridListHasilPengkajianAwal = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dataSource_pengkajianAwal,
        anchor: '100% 60%',
        columnLines: false,
        autoScroll: false,
        border: true,
        sort: false,
        autoHeight: false,
        layout: 'fit',
        region: 'center',
        height: 200,
        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
            listeners: {
                rowselect: function (sm, row, rec) {
                    rowSelected_viDaftarAwal = dataSource_pengkajianAwal.getAt(row);
                }
            }
        }),
        listeners: {
            rowdblclick: function (sm, ridx, cidx) {
                rowSelected_viDaftarAwal = dataSource_pengkajianAwal.getAt(ridx);
                // console.log(rowSelected_viDaftarAwal);
                if (rowSelected_viDaftarAwal !== undefined)
                {
                    DATA_ASKEP_IGD_AWAL.status_sosial.status_marita  = rowSelected_viDaftarAwal.data.STATUS_MARITA;
                    DATA_ASKEP_IGD_AWAL.status_sosial.kd_pekerjaan   = rowSelected_viDaftarAwal.data.KD_PEKERJAAN;
                	get_data_askepAwal(rowSelected_viDaftarAwal.data.KD_PASIEN, rowSelected_viDaftarAwal.data.TANGGAL_TRANSAKSI, rowSelected_viDaftarAwal.data.KD_UNIT, rowSelected_viDaftarAwal.data.URUT_MASUK);
                    // HasilPengkajianLookUpAwal(rowSelected_viDaftarAwal.data);
					
                }
                else
                {
                    HasilPengkajianLookUpAwal();
                }
            }
        },
        cm: new Ext.grid.ColumnModel([
            {
                header: 'Status Posting',
                width: 80,
                sortable: false,
                hideable:true,
                hidden:true,
                menuDisabled:true,
                dataIndex: 'POSTING_TRANSAKSI',
                id: 'txtposting',
                renderer: function(value, metaData, record, rowIndex, colIndex, store){
                    switch (value){
                        case 't':
                            metaData.css = 'StatusHijau'; // 
                            break;
                        case 'f':
                            metaData.css = 'StatusMerah'; // rejected
                            break;
                    }
                    return '';
                }
            },{
                id: 'colReqIdViewIGD',
                header: 'No. Transaksi',
                dataIndex: 'NO_TRANSAKSI',
                sortable: false,
                hideable:false,
                menuDisabled:true,
                width: 80
            },{
                id: 'colTglIGDViewIGD',
                header: 'Tgl Transaksi',
                dataIndex: 'TANGGAL_TRANSAKSI',
                sortable: false,
                hideable:false,
                menuDisabled:true,
                width: 75
            },{
                header: 'No. Medrec',
                width: 65,
                sortable: false,
                hideable:false,
                menuDisabled:true,
                dataIndex: 'KD_PASIEN',
                id: 'colIGDerViewIGD'
            },{
                header: 'Pasien',
                width: 190,
                sortable: false,
                hideable:false,
                menuDisabled:true,
                dataIndex: 'NAMA',
                id: 'colIGDerViewIGD'
            },{
                id: 'colLocationViewIGD',
                header: 'Alamat',
                dataIndex: 'ALAMAT',
                sortable: false,
                hideable:false,
                menuDisabled:true,
                width: 170
            },{
                id: 'colDeptViewIGD',
                header: 'Pelaksana',
                dataIndex: 'NAMA_DOKTER',
                sortable: false,
                hideable:false,
                menuDisabled:true,
                width: 150
            },{
                id: 'colImpactViewIGD',
                header: 'Unit',
                dataIndex: 'NAMA_UNIT',
                sortable: false,
                hideable:false,
                hidden:true,
                menuDisabled:true,
                width: 90
            }
        ] ),
        viewConfig: {
            forceFit: true
        },
        tbar: [{
                id: 'btnEditHasilPengkajianAwal',
                text: 'Open List',
                tooltip: 'Open List',
                iconCls: 'Edit_Tr',
                handler: function (sm, row, rec) {
                    if (rowSelected_viDaftarAwal !== undefined) {
                        HasilPengkajianLookUpAwal(rowSelected_viDaftarAwal.data);
                    } else {
                        ShowPesanWarningPengkajian('Pilih Data Terlebih Dahulu  ', 'Pengkajian');
                    }
                }
            }]
    });
    var FormDepanPengkajianAwal = new Ext.Panel({
        id: mod_id,
        closable: true,
        region: 'center',
        layout: 'form',
        title: 'Assesment Awal Pasien',
        border: false,
        shadhow: true,
        autoScroll: false,
        iconCls: 'Request',
        margins: '0 5 5 0',
        items: [
            getPanelPencarianPasien(),
            gridListHasilPengkajianAwal
        ],
        listeners: {
            'afterrender': function () {
                // Ext.getCmp('dtpTanggalawalPengkajianAwal').disable();
                // Ext.getCmp('dtpTanggalakhirPengkajianAwal').disable();
            }
        }
    });
    return FormDepanPengkajianAwal;
};

function getPanelPencarianPasien() {
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 1190,
                height: 200,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'No. Medrec '
                    },
                    {
                        x: 110,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 10,
                        xtype: 'textfield',
                        name: 'TxtCariMedrecPengkajianAwal',
                        id: 'TxtCariMedrecPengkajianAwal',
                        width: 80,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        var tmpNoMedrec = Ext.get('TxtCariMedrecPengkajianAwal').getValue();
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
                                            if (tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10)
                                            {
                                                var tmpgetNoMedrec = formatnomedrec(tmpNoMedrec);
                                                Ext.getCmp('TxtCariMedrecPengkajianAwal').setValue(tmpgetNoMedrec);
                                                load_pengkajian_paramAwal();
                                            }
                                            else
                                            {
                                                if (tmpNoMedrec.length === 10)
                                                {
                                                    load_pengkajian_paramAwal();
                                                }
                                                else{
                                                    load_pengkajian();
                                                    Ext.getCmp('TxtCariMedrecPengkajianAwal').setValue('');
                                                }
                                            }
                                        }
                                    }
                                }
                    },
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Nama '
                    },
                    {
                        x: 110,
                        y: 40,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 40,
                        xtype: 'textfield',
                        name: 'TxtCariNamaPasienPengkajianAwal',
                        id: 'TxtCariNamaPasienPengkajianAwal',
                        width: 200,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
                                            // var tmpkriteriaPengkajian = getCriteriaFilter_viDaftar();
                                            load_pengkajian_paramAwal();
                                        }
                                    }
                                }
                    },
                    {
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Pelaksana '
                    },
                    {
                        x: 110,
                        y: 70,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 70,
                        xtype: 'textfield',
                        name: 'TxtCariDokterPasienPengkajianAwal',
                        id: 'TxtCariDokterPasienPengkajianAwal',
                        width: 200,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
                                            // var tmpkriteriaPengkajian = getCriteriaFilter_viDaftar();
                                            load_pengkajian_paramAwal();
                                        }
                                    }
                                }
                    },
                    {
                        x: 10,
                        y: 100,
                        xtype: 'label',
                        text: 'Status Posting '
                    },
                    {
                        x: 110,
                        y: 100,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboStatusBayar_viPengkajianIGD(),
                    /*{
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Spesialisasi '
                    },
                    {
                        x: 110,
                        y: 70,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboSpesialisasiPengkajian(),
                    {
                        x: 10,
                        y: 100,
                        xtype: 'label',
                        text: 'Kelas '
                    }, {
                        x: 110,
                        y: 100,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboKelasPengkajian(),
                    {
                        x: 260,
                        y: 100,
                        xtype: 'label',
                        text: 'Kamar '
                    },
                    {
                        x: 310,
                        y: 100,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboKamarPengkajian(),*/
                    {
                        x: 10,
                        y: 130,
                        xtype: 'label',
                        text: 'Tgl.Masuk '
                    },
                    {
                        x: 110,
                        y: 130,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 130,
                        xtype: 'datefield',
                        fieldLabel: 'Tgl.Masuk ',
                        name: 'dtpTanggalawalPengkajianAwal',
                        id: 'dtpTanggalawalPengkajianAwal', 
                        format: 'd/M/Y',
                        value: now,
                        width: 122,
                        listeners:{
                            'specialkey': function ()
                            {
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                {
                                    load_pengkajian_paramAwal();
                                }
                            }
                        }
                    },
                    {
                        x: 260,
                        y: 130,
                        xtype: 'label',
                        text: 's/d '
                    },
                    {
                        x: 320,
                        y: 130,
                        xtype: 'datefield',
                        name: 'dtpTanggalakhirPengkajianAwal',
                        id: 'dtpTanggalakhirPengkajianAwal',
                        format: 'd/M/Y',
                        value: now,
                        width: 120,
                        listeners:{
                            'specialkey': function ()
                            {
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                {
                                    load_pengkajian_paramAwal();
                                }
                            }
                        }

                    },/*
                    {
                        x: 450,
                        y: 130,
                        xtype: 'checkbox',
                        id: 'chkTglPengkajian',
                        text: ' ',
                        hideLabel: false,
                        checked: false,
                        handler: function ()
                        {
                            if (this.getValue() === true)
                            {
                                Ext.getCmp('dtpTanggalawalPengkajianAwal').enable();
                                Ext.getCmp('dtpTanggalawalPengkajianAwal').enable();
                            }
                            else
                            {
                                Ext.getCmp('dtpTanggalawalPengkajianAwal').disable();
                                Ext.getCmp('dtpTanggalawalPengkajianAwal').disable();
                            }
                        }
                    },*/
                    {
                        x: 10,
                        y: 160,
                        xtype: 'button',
                        text: 'Cari',
                        iconCls: 'search',
                        id: 'btnpencariandatapengkajianAwal',
                        handler: function () {
                            // var tmpkriteriaPengkajian = getCriteriaFilter_viDaftar();
                            load_pengkajian_paramAwal();
                        }
                    }
                ]
            }
        ]
    };
    return items;
}
;
function HasilPengkajianLookUpAwal(rowdata) {
    FormLookUpdetailPengkajian = new Ext.Window({
        id: 'wHasilPengkajian',
        title: 'Assesment Awal Pasien',
        closeAction: 'destroy',
        closable: false,
        // width: 1190,
		maximized:true,
        border: false,
        // resizable: false,
        resizable : true,
        plain: true,
        layout: 'fit',
        constrain: true,
        iconCls: 'Request',
        modal: true,
        items: [formpopuppengkajianaAwal()],
        listeners: {
            beforedestroy: function (win)
            {

            }
        }
    });
    FormLookUpdetailPengkajian.show();
    if (rowdata === undefined) {

    } else {
        tmpkdpasien = rowdata.KD_PASIEN;
        tmpkdunit = rowdata.KD_UNIT;
        tmpurutmasuk = rowdata.URUT_MASUK;
        tmptglmasuk = rowdata.TGL_MASUK;
        datainit_pengkajian(rowdata);
    }

}
;
function formpopuppengkajianaAwal() {
    var FrmTabs_popupdatahasilrad = new Ext.Panel
            (
                    {
                        id: 'formpopuppengkajianaAwal',
                        closable: true,
                        region: 'center',
                        itemCls: 'blacklabel',
                        bodyStyle: 'padding: 5px 5px 5px 5px',
                        border: false,
                        shadhow: true,
                        margins: '5 5 5 5',
                        layout:{
                            type:'vbox',
                            align:'stretch'
                        },
                        anchor: '100%',
                        iconCls: '',
                        items:
                                [
                                    // PanelPengkajian(),
                                    PanelPengkajian_1(),
                                    // PanelPengkajian_2(),
                                    // TabPanelPengkajian()
                                    {
                                        flex:1,
                                        layout:'fit',
                                        border:false,
                                        items:[
											TabMainPanelPengkajianAwal(),
										]
									}
                                ],
                        tbar:
                                {
                                    xtype: 'toolbar',
                                    items:
                                            [
                                                {
                                                    xtype: 'button',
                                                    text: 'Save',
                                                    iconCls: 'save',
                                                    id: 'btnSimpan_viDaftarAwal',
                                                    disabled: true,
                                                    handler: function ()
                                                    {
														// SavingData();
														CrudDataAwal('ControllerAskepAwalIGD');
														// console.log(gridPemeriksaanPenunjangAwal.data_store_hasil);
                                                    }
                                                },
                                                {
                                                    xtype: 'tbseparator'
                                                },
                                                {
                                                    xtype: 'button',
                                                    text: 'Delete',
                                                    iconCls: 'remove',
                                                    id: 'btnHapus_viDaftar',
                                                    disabled: false,
                                                    handler: function ()
                                                    {
                                                        Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function (button) {
                                                            if (button === 'yes') {
                                                                loadMask.show();
                                                                DeleteData();
                                                            }
                                                        });
                                                    }
                                                },
                                                {
                                                    xtype: 'tbseparator'
                                                },
                                                {
                                                    xtype: 'button',
                                                    text: 'Cetak',
                                                    iconCls: 'print',
                                                    hidden : true,
                                                    id: 'btnCetak_viDaftarPengkajian',
                                                    handler: function ()
                                                    {
                                                        var criteria = GetCriteriaPengkajian();
                                                        loadMask.show();
                                                        loadlaporanAskep('0', 'LapPengkajianKeperawatan', criteria, function () {
                                                            loadMask.hide();
                                                        });
                                                    }
                                                },
                                                '->',
                                                {
                                                    xtype: 'button',
                                                    text: 'Close',
                                                    iconCls: 'closeform',
                                                    id: 'btnclose_viDaftar',
                                                    handler: function ()
                                                    {
                                                        if (tmpediting === 'true')
                                                        {
                                                            Ext.Msg.confirm('Warning', 'Anda Belum Menyimpan Data, tetap keluar ?', function (button) {
                                                                if (button === 'yes') {
                                                                    tmpediting = 'false';
                                                                    // SavingData();
                                                                    FormLookUpdetailPengkajian.close();
                                                                } 
                                                            });
                                                        } else
                                                        {
                                                            FormLookUpdetailPengkajian.close();
                                                        }

                                                    }
                                                }
                                            ]
                                }
                    }
            );

    return FrmTabs_popupdatahasilrad;
};

function PanelPengkajian_2() {
    var items = {
        layout: 'column',
        bodyStyle: 'padding: 10px 10px 10px 10px',
        width : '100%',
        border: true,
        items: [
            {
                columnWidth:.4,
                layout: 'form',
                border:false,
                items: [
                    {
                        xtype:'textfield',
                        fieldLabel: 'Alamat',
                        name: 'TxtPopupAlamatPasien',
                        id: 'TxtPopupAlamatPasien',
                        anchor:'95%'
                    },
                ]
            },{
                columnWidth:.4,
                layout: 'form',
                border:false,
                items: [
                    {
                        xtype:'textfield',
                        fieldLabel: 'Pelaksana',
                        name: 'TxtPopupPelaksana',
                        id: 'TxtPopupPelaksana',
                        anchor:'95%'
                    },
                ]
            }
        ]
    };
    return items;
}

function PanelPengkajian() {
    var items = {
        layout: 'column',
        bodyStyle: 'padding: 10px 10px 10px 10px',
        width : '100%',
        border: true,
        items: [
            /*{
                columnWidth:.2,
                layout: 'form',
                border:false,
                items: [
                    {
                        xtype:'textfield',
                        fieldLabel: 'No. Medrec',
                        name: 'kd_pasien',
                        name: 'TxtPopupMedrec',
                        id: 'TxtPopupMedrec',
                        anchor:'95%'
                    },{
                        xtype:'textfield',
                        fieldLabel: 'Kode Unit',
                        name: 'TxtPopupKdUnit',
                        id: 'TxtPopupKdUnit',
                        anchor:'95%'
                    },
                ]
            },{
                columnWidth:.4,
                layout: 'form',
                border:false,
                items: [
                    {
                        xtype:'textfield',
                        fieldLabel: 'Nama Pasien',
                        name: 'TxtPopupNamaPasien',
                        id: 'TxtPopupNamaPasien',
                        anchor:'95%'
                    },{
                        xtype:'textfield',
                        fieldLabel: 'Nama Unit',
                        name: 'TxtPopupUnit',
                        id: 'TxtPopupUnit',
                        anchor:'95%'
                    },
                ]
            },{
                columnWidth:.2,
                layout: 'form',
                border:false,
                items: [
                    {
                        xtype:'datefield',
                        fieldLabel: 'Waktu Mulai',
                        name: 'textfieldTgl_RespMulaiAwal',
                        id: 'textfieldTgl_RespMulaiAwal',
                        format: 'Y-m-d',
                        value : now.format('Y-m-d'),
                        anchor:'95%',
                    },{
                        xtype:'datefield',
                        fieldLabel: 'Waktu Selesai',
                        name: 'textfieldTgl_RespSelesaiAwal',
                        id: 'textfieldTgl_RespSelesaiAwal',
                        format: 'Y-m-d',
                        value : now.format('Y-m-d'),
                        anchor:'95%',
                    },
                ]
            },{
                columnWidth:.2,
                layout: 'form',
                border:false,
                items: [
                    {
                        xtype:'timefield',
                        name: 'textfieldJam_RespMulaiAwal',
                        id: 'textfieldJam_RespMulaiAwal',
                        format: 'H:i:s',
                        value : now.format('H:i:s'),
                        anchor:'95%',
                    },
                    {
                        xtype:'timefield',
                        name: 'textfieldJam_RespSelesaiAwal',
                        id: 'textfieldJam_RespSelesaiAwal',
                        format: 'H:i:s',
                        value : now.format('H:i:s'),
                        anchor:'95%',
                    },
                ]
            }*/
            {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 1190,
                height: 130,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'No. Medrec '
                    },
                    {
                        x: 110,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 10,
                        xtype: 'textfield',
                        name: 'TxtPopupMedrec',
                        id: 'TxtPopupMedrec',
                        width: 80
                    },
                    {
                        x: 210,
                        y: 10,
                        xtype: 'label',
                        text: 'Nama '
                    },
                    {
                        x: 270,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 280,
                        y: 10,
                        xtype: 'textfield',
                        name: 'TxtPopupNamaPasien',
                        id: 'TxtPopupNamaPasien',
                        width: 200
                    },
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Alamat '
                    },
                    {
                        x: 110,
                        y: 40,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 40,
                        xtype: 'textfield',
                        name: 'TxtPopupAlamatPasien',
                        id: 'TxtPopupAlamatPasien',
                        width: 500
                    },
                    {
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Kode Unit '
                    },
                    {
                        x: 110,
                        y: 70,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 70,
                        xtype: 'textfield',
                        name: 'TxtPopupKdUnit',
                        id: 'TxtPopupKdUnit',
                        width: 80
                    },
                    {
                        x: 210,
                        y: 70,
                        xtype: 'label',
                        text: 'Unit '
                    },
                    {x: 270,
                        y: 70,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 280,
                        y: 70,
                        xtype: 'textfield',
                        name: 'TxtPopupUnit',
                        id: 'TxtPopupUnit',
                        width: 200
                    },
                    {
                        x: 10,
                        y: 100,
                        xtype: 'label',
                        text: 'Pelaksana '
                    },
                    {
                        x: 110,
                        y: 100,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 100, xtype: 'textfield',
                        name: 'TxtPopupPelaksana',
                        id: 'TxtPopupPelaksana',
                        width: 500
                    },{
                        x: 650,
                        y: 10,
                        xtype: 'label',
                        text: 'Waktu Mulai '
                    },{
                        x: 720,
                        y: 10,
                        xtype: 'label',
                        text: ':'
                    },{
                        x: 730,
                        y: 10,
                        xtype: 'datefield',
                        id: 'textfieldTgl_RespMulaiAwal',
                        format: 'Y-m-d',
                        value : now.format('Y-m-d'),
                        width: 100,
                    },{
                        x: 850,
                        y: 10,
                        xtype: 'timefield',
                        id: 'textfieldJam_RespMulaiAwal',
                        format: 'H:i:s',
                        value : now.format('H:i:s'),
                        width: 100,
                    },{
                        x: 650,
                        y: 40,
                        xtype: 'label',
                        text: 'Waktu Selesai '
                    },{
                        x: 720,
                        y: 40,
                        xtype: 'label',
                        text: ':'
                    },{
                        x: 730,
                        y: 40,
                        xtype: 'datefield',
                        id: 'textfieldTgl_RespSelesaiAwal',
                        value : now.format('Y-m-d'),
                        format: 'Y-m-d',
                        width: 100,
                    },{
                        x: 850,
                        y: 40,
                        xtype: 'timefield',
                        id: 'textfieldJam_RespSelesaiAwal',
                        format: 'H:i:s',
                        value : now.format('H:i:s'),
                        width: 100,
                    },{
                        x: 650,
                        y: 70,
                        xtype: 'button',
                        text: 'Simpan'
                    },
                ]
            }
        ]
    };
    return items;
};
function PanelPengkajian_1() {
    var items = {
        layout: 'column',
        bodyStyle: 'padding: 10px 10px 10px 10px',
        width : '100%',
        border: true,
        items: [
            {
                columnWidth:.2,
                layout: 'form',
                border:false,
                items: [
                    {
                        xtype:'textfield',
                        fieldLabel: 'No. Medrec',
                        name: 'kd_pasien',
                        name: 'TxtPopupMedrec',
                        id: 'TxtPopupMedrec',
                        readOnly:true,
                        anchor:'95%'
                    },{
                        xtype:'textfield',
                        fieldLabel: 'Kode Unit',
                        name: 'TxtPopupKdUnit',
                        id: 'TxtPopupKdUnit',
                        readOnly:true,
                        anchor:'95%'
                    },{
                        xtype:'textfield',
                        fieldLabel: 'Pelaksana',
                        name: 'TxtPopupPelaksana',
                        id: 'TxtPopupPelaksana',
                        readOnly:true,
                        anchor:'95%'
                    },
                ]
            },{
                columnWidth:.4,
                layout: 'form',
                border:false,
                items: [
                    {
                        xtype:'textfield',
                        fieldLabel: 'Nama Pasien',
                        name: 'TxtPopupNamaPasien',
                        id: 'TxtPopupNamaPasien',
                        readOnly:true,
                        anchor:'95%'
                    },{
                        xtype:'textfield',
                        fieldLabel: 'Nama Unit',
                        name: 'TxtPopupUnit',
                        id: 'TxtPopupUnit',
                        readOnly:true,
                        anchor:'95%'
                    },{
                        xtype:'textfield',
                        fieldLabel: 'Alamat',
                        name: 'TxtPopupAlamatPasien',
                        id: 'TxtPopupAlamatPasien',
                        readOnly:true,
                        anchor:'95%'
                    },
                ]
            },{
                columnWidth:.2,
                layout: 'form',
                border:false,
                items: [
                    {
                        xtype:'datefield',
                        fieldLabel: 'Waktu Datang',
                        name: 'textfieldTgl_RespMulaiAwal',
                        id: 'textfieldTgl_RespMulaiAwal',
                        format: 'Y-m-d',
                        value : DATA_ASKEP_IGD_AWAL.respontime.waktu_datang_tgl,
                        anchor:'95%',
                        listeners   : {
                            'render': function (c) {
                                c.getEl().on('keypress', function (e) {
                                    DATA_ASKEP_IGD_AWAL.respontime.waktu_datang_tgl = Ext.get('textfieldTgl_RespMulaiAwal').getValue();
                                }, c);
                                c.getEl().on('change', function (e) {
                                    DATA_ASKEP_IGD_AWAL.respontime.waktu_datang_tgl = Ext.get('textfieldTgl_RespMulaiAwal').getValue();
                                }, c);
                            },
                            change: function (t,n,o) {
                                DATA_ASKEP_IGD_AWAL.respontime.waktu_datang_tgl = Ext.get('textfieldTgl_RespMulaiAwal').getValue();
                            },
                        }
                    },{
                        xtype:'datefield',
                        fieldLabel: 'Waktu Ditindak',
                        name: 'textfieldTgl_RespSelesaiAwal',
                        id: 'textfieldTgl_RespSelesaiAwal',
                        format: 'Y-m-d',
                        value : DATA_ASKEP_IGD_AWAL.respontime.waktu_periksa_tgl,
                        anchor:'95%',
                        listeners   : {
                            'render': function (c) {
                                c.getEl().on('keypress', function (e) {
                                    DATA_ASKEP_IGD_AWAL.respontime.waktu_periksa_tgl = Ext.get('textfieldTgl_RespSelesaiAwal').getValue();
                                }, c);
                                c.getEl().on('change', function (e) {
                                    DATA_ASKEP_IGD_AWAL.respontime.waktu_periksa_tgl = Ext.get('textfieldTgl_RespSelesaiAwal').getValue();
                                }, c);
                            },
                            change: function (t,n,o) {
                                DATA_ASKEP_IGD_AWAL.respontime.waktu_periksa_tgl = Ext.get('textfieldTgl_RespSelesaiAwal').getValue();
                            },
                        }
                    },
                ]
            },{
                columnWidth:.2,
                layout: 'form',
                border:false,
                items: [
                    {
                        xtype:'timefield',
                        name: 'textfieldJam_RespMulaiAwal',
                        id: 'textfieldJam_RespMulaiAwal',
                        fieldLabel: 'Jam Datang',
                        format: 'H:i:s',
                        value : DATA_ASKEP_IGD_AWAL.respontime.waktu_datang_jam,
                        anchor:'95%',
                        listeners   : {
                            'render': function (c) {
                                c.getEl().on('keypress', function (e) {
                                    DATA_ASKEP_IGD_AWAL.respontime.waktu_datang_jam = Ext.get('textfieldJam_RespMulaiAwal').getValue();
                                }, c);
                                c.getEl().on('change', function (e) {
                                    DATA_ASKEP_IGD_AWAL.respontime.waktu_datang_jam = Ext.get('textfieldJam_RespMulaiAwal').getValue();
                                }, c);
                            },
                            change: function (t,n,o) {
                                DATA_ASKEP_IGD_AWAL.respontime.waktu_datang_jam = Ext.get('textfieldJam_RespMulaiAwal').getValue();
                            },
                        }
                    },{
                        xtype:'timefield',
                        name: 'textfieldJam_RespSelesaiAwal',
                        id: 'textfieldJam_RespSelesaiAwal',
                        fieldLabel: 'Jam Ditindak',
                        format: 'H:i:s',
                        value : DATA_ASKEP_IGD_AWAL.respontime.waktu_periksa_jam,
                        anchor:'95%',
                        listeners   : {
                            'render': function (c) {
                                c.getEl().on('keypress', function (e) {
                                    DATA_ASKEP_IGD_AWAL.respontime.waktu_periksa_jam = Ext.get('textfieldJam_RespSelesaiAwal').getValue();
                                }, c);
                                c.getEl().on('change', function (e) {
                                    DATA_ASKEP_IGD_AWAL.respontime.waktu_periksa_jam = Ext.get('textfieldJam_RespSelesaiAwal').getValue();
                                }, c);
                            },
                            change: function (t,n,o) {
                                DATA_ASKEP_IGD_AWAL.respontime.waktu_periksa_jam = Ext.get('textfieldJam_RespSelesaiAwal').getValue();
                            },
                        }
                    },{
                        xtype:'button',
                        name: 'btnfieldResponetimeAwal',
                        id: 'btnfieldResponetimeAwal',
                        text : 'Simpan',
                        anchor:'95%',
                        handler : function(){
                            Ext.Ajax.request({
                                url: baseURL + "index.php/gawat_darurat/control_askep_awal_igd/respontime",
                                params: {
                                    KD_PASIEN        : rowSelected_viDaftarAwal.data.KD_PASIEN, 
                                    KD_UNIT          : rowSelected_viDaftarAwal.data.KD_UNIT,
                                    URUT_MASUK       : rowSelected_viDaftarAwal.data.URUT_MASUK,
                                    TGL_MASUK        : rowSelected_viDaftarAwal.data.TANGGAL_TRANSAKSI,
                                    waktu_respontime : JSON.stringify(DATA_ASKEP_IGD_AWAL.respontime),
                                },
                                failure: function (o)
                                {
                                    ShowPesanWarningPengkajian('Proses Saving Tidak berhasil silahkan hubungi admin', 'Gagal');
                                },
                                success: function (o)
                                {
                                    var cst = Ext.decode(o.responseText);
                                    if (cst.status === true) {
                                        ShowPesanInfoPengkajian('Proses Saving Berhasil', 'Save');
                                    }else{
                                        ShowPesanWarningPengkajian('Proses Saving Tidak berhasil silahkan hubungi admin', 'Gagal');
                                    }
                                }
                            });
                        }
                    },
                ]
            }
        ]
    };
    return items;
};

function convert_boolean(type_data){
	if (type_data == 'false' || type_data == 'f' || type_data == '0' || type_data == 0 || type_data == null) {
		return false;
	}else{
		return true;
	}
}

function form_airway() { 
	var items = [
		{
			xtype: 'checkboxgroup',
			columns: 2,
			items : [
				{
					boxLabel: 'Paten',
					name 	: 'checked_airway_paten',
					id 		: 'checked_airway_paten',
					checked : DATA_ASKEP_IGD_AWAL.airway.paten,
				},{
					boxLabel: 'Snoring',
					name 	: 'checked_airway_snoring',
					id 		: 'checked_airway_snoring',
					checked : DATA_ASKEP_IGD_AWAL.airway.snoring,
				},{
					boxLabel: 'Gurgling',
					name 	: 'checked_airway_gurgling',
					id 		: 'checked_airway_gurgling',
					checked : DATA_ASKEP_IGD_AWAL.airway.gurgling,
				},{
					boxLabel: 'Stridor',
					name 	: 'checked_airway_stridor',
					id 		: 'checked_airway_stridor',
					checked : DATA_ASKEP_IGD_AWAL.airway.stridor,
				},{
					boxLabel: 'Wheezing',
					name 	: 'checked_airway_wheezing',
					id 		: 'checked_airway_wheezing',
					checked : DATA_ASKEP_IGD_AWAL.airway.wheezing,
				}, 
			],
			listeners: {
				change: function (checkbox, newValue, oldValue, eOpts) {
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
					DATA_ASKEP_IGD_AWAL.airway.paten 	= false;
					DATA_ASKEP_IGD_AWAL.airway.snoring 	= false;
					DATA_ASKEP_IGD_AWAL.airway.gurgling 	= false;
					DATA_ASKEP_IGD_AWAL.airway.wheezing 	= false;
					DATA_ASKEP_IGD_AWAL.airway.stridor 	= false;
					for (var i = 0; i < newValue.length; i++) {
						if (newValue[i].id == 'checked_airway_paten') {
							DATA_ASKEP_IGD_AWAL.airway.paten = newValue[i].checked;
						}

						if (newValue[i].id == 'checked_airway_snoring') {
							DATA_ASKEP_IGD_AWAL.airway.snoring = newValue[i].checked;
						}

						if (newValue[i].id == 'checked_airway_gurgling') {
							DATA_ASKEP_IGD_AWAL.airway.gurgling = newValue[i].checked;
						}

						if (newValue[i].id == 'checked_airway_wheezing') {
							DATA_ASKEP_IGD_AWAL.airway.wheezing = newValue[i].checked;
						}
						
						if (newValue[i].id == 'checked_airway_stridor') {
							DATA_ASKEP_IGD_AWAL.airway.stridor = newValue[i].checked;
						}
					}
				}
			}
		}
	];
	return items;
};

function form_jenis_luka() { 
	var items = [
		{
			xtype: 'checkboxgroup',
			fieldLabel : 'Jenis luka',
			columns: 1,
			items : [
				{
					boxLabel 	: 'Vulnus ekskoriatum',
					name 		: 'checked_jenis_luka_vulnus_ekskoriatum',
					id 			: 'checked_jenis_luka_vulnus_ekskoriatum',
					checked 	: DATA_ASKEP_IGD_AWAL.exposure.ekskoriatum,
				},{
					boxLabel 	: 'Vulnus laseratum',
					name 		: 'checked_jenis_luka_vulnus_laseratum',
					id 			: 'checked_jenis_luka_vulnus_laseratum',
					checked 	: DATA_ASKEP_IGD_AWAL.exposure.laseratum,
				},{
					boxLabel 	: 'Vulnus morsum',
					name 		: 'checked_jenis_luka_vulnus_morsum',
					id 			: 'checked_jenis_luka_vulnus_morsum',
					checked 	: DATA_ASKEP_IGD_AWAL.exposure.morsum,
				},{
					boxLabel 	: 'Vulnus punctum',
					name 		: 'checked_jenis_luka_vulnus_punctum',
					id 			: 'checked_jenis_luka_vulnus_punctum',
					checked 	: DATA_ASKEP_IGD_AWAL.exposure.punctum,
				},{
					boxLabel 	: 'Vulnus sklopirotum',
					name 		: 'checked_jenis_luka_vulnus_sklopirotum',
					id 			: 'checked_jenis_luka_vulnus_sklopirotum',
					checked 	: DATA_ASKEP_IGD_AWAL.exposure.sklopirotum,
				}, 
			],
			listeners: {
				change: function (checkbox, newValue, oldValue, eOpts) {
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
					DATA_ASKEP_IGD_AWAL.exposure.ekskoriatum = false;
					DATA_ASKEP_IGD_AWAL.exposure.laseratum   = false;
					DATA_ASKEP_IGD_AWAL.exposure.morsum      = false;
					DATA_ASKEP_IGD_AWAL.exposure.punctum     = false;
					DATA_ASKEP_IGD_AWAL.exposure.sklopirotum = false;
					for (var i = 0; i < newValue.length; i++) {
						if (newValue[i].id == 'checked_jenis_luka_vulnus_ekskoriatum') {
							DATA_ASKEP_IGD_AWAL.exposure.ekskoriatum = newValue[i].checked;
						}

						if (newValue[i].id == 'checked_jenis_luka_vulnus_laseratum') {
							DATA_ASKEP_IGD_AWAL.exposure.laseratum = newValue[i].checked;
						}

						if (newValue[i].id == 'checked_jenis_luka_vulnus_morsum') {
							DATA_ASKEP_IGD_AWAL.exposure.morsum = newValue[i].checked;
						}

						if (newValue[i].id == 'checked_jenis_luka_vulnus_punctum') {
							DATA_ASKEP_IGD_AWAL.exposure.punctum = newValue[i].checked;
						}

						if (newValue[i].id == 'checked_jenis_luka_vulnus_sklopirotum') {
							DATA_ASKEP_IGD_AWAL.exposure.sklopirotum = newValue[i].checked;
						}
					}
				}
			}
		}
	];
	return items;
};

function form_luka_bakar() { 
	var items = [
		{
			fieldLabel: 'Luka Bakar',
			name: 'checked_jenis_luka_bakar',
			id: 'checked_jenis_luka_bakar',
			checked 	: DATA_ASKEP_IGD_AWAL.exposure.luka_bakar,
			listeners: {
				check: function (checkbox, isChecked) {
					tmpediting = 'true';
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
					if (isChecked === true) {
						Ext.getCmp('txt_luka_bakar_luas').enable();
						Ext.getCmp('txt_luka_bakar_derajat').enable();
						Ext.getCmp('txt_luka_bakar_luas_luka').enable();
						Ext.getCmp('txt_luka_bakar_lokasi_luka').enable();
					}else{
						Ext.getCmp('txt_luka_bakar_luas').disable();
						Ext.getCmp('txt_luka_bakar_derajat').disable();
						Ext.getCmp('txt_luka_bakar_luas_luka').disable();
						Ext.getCmp('txt_luka_bakar_lokasi_luka').disable();

						Ext.getCmp('txt_luka_bakar_luas').setValue();
						Ext.getCmp('txt_luka_bakar_derajat').setValue();
						Ext.getCmp('txt_luka_bakar_luas_luka').setValue();
						Ext.getCmp('txt_luka_bakar_lokasi_luka').setValue();
					}

					DATA_ASKEP_IGD_AWAL.exposure.luka_bakar = isChecked;
				},
				render: function () {
					if (DATA_ASKEP_IGD_AWAL.exposure.luka_bakar === true) {
						Ext.getCmp('txt_luka_bakar_luas').enable();
						Ext.getCmp('txt_luka_bakar_derajat').enable();
						Ext.getCmp('txt_luka_bakar_luas_luka').enable();
						Ext.getCmp('txt_luka_bakar_lokasi_luka').enable();
					}else{
						Ext.getCmp('txt_luka_bakar_luas').disable();
						Ext.getCmp('txt_luka_bakar_derajat').disable();
						Ext.getCmp('txt_luka_bakar_luas_luka').disable();
						Ext.getCmp('txt_luka_bakar_lokasi_luka').disable();

						Ext.getCmp('txt_luka_bakar_luas').setValue();
						Ext.getCmp('txt_luka_bakar_derajat').setValue();
						Ext.getCmp('txt_luka_bakar_luas_luka').setValue();
						Ext.getCmp('txt_luka_bakar_lokasi_luka').setValue();
					}
				}
			}
		},{
			xtype : 'textfield',
			fieldLabel : 'Luas (%)',
			name : 'txt_luka_bakar_luas',
			id : 'txt_luka_bakar_luas',
			disabled : true,
			value 	: DATA_ASKEP_IGD_AWAL.exposure.luka_bakar_luas,
			listeners 	: {
				'render': function (c) {
					c.getEl().on('keypress', function (e) {
						if (e.getKey() === 13) {
						}
					}, c);
					c.getEl().on('change', function (e) {
						Ext.getCmp('btnSimpan_viDaftarAwal').enable();
						tmpediting 						= 'true';
						DATA_ASKEP_IGD_AWAL.exposure.luka_bakar_luas	= Ext.get('txt_luka_bakar_luas').getValue();
					}, c);
				}
			}
		},{
			xtype : 'textfield',
			fieldLabel : 'Derajat (Cel)',
			name : 'txt_luka_bakar_derajat',
			id : 'txt_luka_bakar_derajat',
			disabled : true,
			value 	: DATA_ASKEP_IGD_AWAL.exposure.luka_bakar_derajat,
			listeners 	: {
				'render': function (c) {
					c.getEl().on('keypress', function (e) {
						if (e.getKey() === 13) {
						}
					}, c);
					c.getEl().on('change', function (e) {
						Ext.getCmp('btnSimpan_viDaftarAwal').enable();
						tmpediting 						= 'true';
						DATA_ASKEP_IGD_AWAL.exposure.luka_bakar_derajat	= Ext.get('txt_luka_bakar_derajat').getValue();
					}, c);
				}
			}
		},{
			xtype : 'textfield',
			fieldLabel : 'Luas Luka (cm)',
			name : 'txt_luka_bakar_luas_luka',
			id : 'txt_luka_bakar_luas_luka',
			value 		: DATA_ASKEP_IGD_AWAL.exposure.luka_bakar_luas_luka,
			disabled 	: true,
			listeners 	: {
				'render': function (c) {
					c.getEl().on('keypress', function (e) {
						if (e.getKey() === 13) {
						}
					}, c);
					c.getEl().on('change', function (e) {
						Ext.getCmp('btnSimpan_viDaftarAwal').enable();
						tmpediting 						= 'true';
						DATA_ASKEP_IGD_AWAL.exposure.luka_bakar_luas_luka	= Ext.get('txt_luka_bakar_luas_luka').getValue();
					}, c);
				}
			}
		},{
			xtype : 'textfield',
			fieldLabel : 'Lokasi Luka',
			name : 'txt_luka_bakar_lokasi_luka',
			id : 'txt_luka_bakar_lokasi_luka',
			disabled : true,
			value 		: DATA_ASKEP_IGD_AWAL.exposure.luka_bakar_lokasi_jejas,
			listeners 	: {
				'render': function (c) {
					c.getEl().on('keypress', function (e) {
						if (e.getKey() === 13) {
						}
					}, c);
					c.getEl().on('change', function (e) {
						Ext.getCmp('btnSimpan_viDaftarAwal').enable();
						tmpediting 						= 'true';
						DATA_ASKEP_IGD_AWAL.exposure.luka_bakar_lokasi_jejas	= Ext.get('txt_luka_bakar_lokasi_luka').getValue();
					}, c);
				}
			}
		},
	];
	return items;
};

function form_ekg() { 
	var items = [
		{
			xtype: 'checkboxgroup',
			columns: 2,
			items : [
				{
					boxLabel	: 'Irama Teratur',
					name		: 'check_ekg_irama_teratur',
					id			: 'check_ekg_irama_teratur',
					checked 	: DATA_ASKEP_IGD_AWAL.ekg.irama_teratur ,
				},{
					boxLabel	: 'STEMI',
					name		: 'check_ekg_stemi',
					id			: 'check_ekg_stemi',
					checked 	: DATA_ASKEP_IGD_AWAL.ekg.stemi ,
				},{
					boxLabel	: 'Irama tidak teratur',
					name		: 'check_ekg_irama_tidak_teratur',
					id			: 'check_ekg_irama_tidak_teratur',
					checked 	: DATA_ASKEP_IGD_AWAL.ekg.irama_tidak_teratur ,
				},{
					boxLabel	: 'NSTEMI',
					name		: 'check_ekg_nstemi',
					id			: 'check_ekg_nstemi',
					checked 	: DATA_ASKEP_IGD_AWAL.ekg.nstemi ,
				},
			],
			listeners: {
				change: function (checkbox, newValue, oldValue, eOpts) {
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
					DATA_ASKEP_IGD_AWAL.ekg.irama_teratur       = false;
					DATA_ASKEP_IGD_AWAL.ekg.stemi               = false;
					DATA_ASKEP_IGD_AWAL.ekg.irama_tidak_teratur = false;
					DATA_ASKEP_IGD_AWAL.ekg.nstemi              = false;
					for (var i = 0; i < newValue.length; i++) {
						if (newValue[i].id == 'check_ekg_irama_teratur') {
							DATA_ASKEP_IGD_AWAL.ekg.irama_teratur = newValue[i].checked;
						}

						if (newValue[i].id == 'check_ekg_stemi') {
							DATA_ASKEP_IGD_AWAL.ekg.stemi = newValue[i].checked;
						}

						if (newValue[i].id == 'check_ekg_irama_tidak_teratur') {
							DATA_ASKEP_IGD_AWAL.ekg.irama_tidak_teratur = newValue[i].checked;
						}

						if (newValue[i].id == 'check_ekg_nstemi') {
							DATA_ASKEP_IGD_AWAL.ekg.nstemi = newValue[i].checked;
						}
					}
				}
			}
		}
	];
	return items;
};

function form_riwayat_cairan() { 
	var items = [
		{
			xtype: 'checkboxgroup',
			columns: 2,
			items : [
				{
					boxLabel	: 'Diare',
					name		: 'check_riwayat_cairan_irama_diare',
					id			: 'check_riwayat_cairan_irama_diare',
					checked 	: DATA_ASKEP_IGD_AWAL.riwayat_cairan.diare,
				},{
					boxLabel	: 'Muntah',
					name		: 'check_riwayat_cairan_muntah',
					id			: 'check_riwayat_cairan_muntah',
					checked 	: DATA_ASKEP_IGD_AWAL.riwayat_cairan.muntah,
				},{
					boxLabel	: 'Luka Bakar',
					name		: 'check_riwayat_cairan_luka_bakar',
					id			: 'check_riwayat_cairan_luka_bakar',
					checked 	: DATA_ASKEP_IGD_AWAL.riwayat_cairan.luka_bakar,
				},{
					boxLabel	: 'Perdarahan',
					name		: 'check_riwayat_cairan_perdarahan',
					id			: 'check_riwayat_cairan_perdarahan',
					checked 	: DATA_ASKEP_IGD_AWAL.riwayat_cairan.perdarahan,
				},
			],
			listeners: {
				change: function (checkbox, newValue, oldValue, eOpts) {
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
					DATA_ASKEP_IGD_AWAL.riwayat_cairan.diare      = false;
					DATA_ASKEP_IGD_AWAL.riwayat_cairan.muntah     = false;
					DATA_ASKEP_IGD_AWAL.riwayat_cairan.luka_bakar = false;
					DATA_ASKEP_IGD_AWAL.riwayat_cairan.perdarahan = false;
					for (var i = 0; i < newValue.length; i++) {
						if (newValue[i].id == 'check_riwayat_cairan_irama_diare') {
							DATA_ASKEP_IGD_AWAL.riwayat_cairan.diare = newValue[i].checked;
						}

						if (newValue[i].id == 'check_riwayat_cairan_muntah') {
							DATA_ASKEP_IGD_AWAL.riwayat_cairan.muntah = newValue[i].checked;
						}

						if (newValue[i].id == 'check_riwayat_cairan_luka_bakar') {
							DATA_ASKEP_IGD_AWAL.riwayat_cairan.luka_bakar = newValue[i].checked;
						}

						if (newValue[i].id == 'check_riwayat_cairan_perdarahan') {
							DATA_ASKEP_IGD_AWAL.riwayat_cairan.perdarahan = newValue[i].checked;
						}
					}
				}
			}
		}
	];
	return items;
};

function form_disability_gcs() { 
	var items = [
		{
			xtype: 'checkboxgroup',
			fieldLabel : 'GCS',
			columns: 2,
			items : [
				{
					boxLabel	: '3-8',
					name		: 'check_disability_gcs_3_8',
					id			: 'check_disability_gcs_3_8',
					checked 	: DATA_ASKEP_IGD_AWAL.disability._3to8,
				},{
					boxLabel	: '9-13',
					name		: 'check_disability_gcs_9_13',
					id			: 'check_disability_gcs_9_13',
					checked 	: DATA_ASKEP_IGD_AWAL.disability._9to13,
				},{
					boxLabel	: '14-15',
					name		: 'check_disability_gcs_14_15',
					id			: 'check_disability_gcs_14_15',
					checked 	: DATA_ASKEP_IGD_AWAL.disability._14to15,
				},
			],
			listeners: {
				change: function (checkbox, newValue, oldValue, eOpts) {
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
					DATA_ASKEP_IGD_AWAL.disability._3to8   = false;
					DATA_ASKEP_IGD_AWAL.disability._9to13  = false;
					DATA_ASKEP_IGD_AWAL.disability._14to15 = false;
					for (var i = 0; i < newValue.length; i++) {
						if (newValue[i].id == 'check_disability_gcs_3_8') {
							DATA_ASKEP_IGD_AWAL.disability._3to8 = newValue[i].checked;
						}

						if (newValue[i].id == 'check_disability_gcs_9_13') {
							DATA_ASKEP_IGD_AWAL.disability._9to13 = newValue[i].checked;
						}

						if (newValue[i].id == 'check_disability_gcs_14_15') {
							DATA_ASKEP_IGD_AWAL.disability._14to15 = newValue[i].checked;
						}
					}
				}
			}
		}
	];
	return items;
};

function form_ukuran_pupil() { 
	var items = [
		{
			xtype: 'radiogroup',
			fieldLabel : 'Ukuran Pupil',
			columns: 2,
			items : [
				{
					boxLabel	: 'Isokar',
					name		: 'radio_ukuran_pupil',
					id			: 'radio_ukuran_pupil_isokar',
					checked 	: DATA_ASKEP_IGD_AWAL.disability.isokor,
				},{
					boxLabel	: 'Anisokor',
					name		: 'radio_ukuran_pupil',
					id			: 'radio_ukuran_pupil_anisokor',
					checked 	: DATA_ASKEP_IGD_AWAL.disability.anisokor,
				},
			],
			listeners: {
				change: function (checkbox, newValue, oldValue, eOpts) {
					tmpediting = 'true';
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
					if(newValue.boxLabel == 'Isokar'){
						DATA_ASKEP_IGD_AWAL.disability.isokor   = true;
						DATA_ASKEP_IGD_AWAL.disability.anisokor = false;
					}else{
						DATA_ASKEP_IGD_AWAL.disability.isokor   = false;
						DATA_ASKEP_IGD_AWAL.disability.anisokor = true;
					}
				}
			}
		}
	];
	return items;
};

function form_crt() { 
	var items = [
		{
			xtype: 'radiogroup', 
			fieldLabel 	: '',
			items : [
				{
					boxLabel	: '< 2 Detik',
					name		: 'radio_crt',
					id			: 'radio_crt_kurang',
					checked 	: DATA_ASKEP_IGD_AWAL.crt.kurang,
				},{
					boxLabel	: '> 2 Detik',
					name		: 'radio_crt',
					id			: 'radio_crt_lebih',
					checked 	: DATA_ASKEP_IGD_AWAL.crt.lebih,
				},
			],
			listeners: {
				change: function (checkbox, newValue, oldValue, eOpts) {
					tmpediting 						= 'true';
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
					if(newValue.boxLabel == '> 2 Detik'){
						DATA_ASKEP_IGD_AWAL.crt.kurang = false;
						DATA_ASKEP_IGD_AWAL.crt.lebih  = true;
					}else{
						DATA_ASKEP_IGD_AWAL.crt.kurang = true;
						DATA_ASKEP_IGD_AWAL.crt.lebih  = false;
					}
				}
			}
		}
	];
	return items;
};
function form_turgor_kulit() { 
	var items = [
		{
			xtype: 'radiogroup', 
			fieldLabel 	: '',
			items : [
				{
					boxLabel	: 'Normal',
					name		: 'radio_turgor_kulit',
					id			: 'radio_turgor_kulit_normal',
					checked 	: DATA_ASKEP_IGD_AWAL.turgor_kulit.normal,
				},{
					boxLabel	: 'Kurang',
					name		: 'radio_turgor_kulit',
					id			: 'radio_turgor_kulit_kurang',
					checked 	: DATA_ASKEP_IGD_AWAL.turgor_kulit.kurang,
				},
			],
			listeners: {
				change: function (checkbox, newValue, oldValue, eOpts) {
					tmpediting = 'true';
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
					if(newValue.boxLabel == 'Normal'){
						DATA_ASKEP_IGD_AWAL.turgor_kulit.normal = true;
						DATA_ASKEP_IGD_AWAL.turgor_kulit.kurang = false;
					}else{
						DATA_ASKEP_IGD_AWAL.turgor_kulit.normal = false;
						DATA_ASKEP_IGD_AWAL.turgor_kulit.kurang = true;
					}
				}
			}
		}
	];
	return items;
};

function form_diameter() { 
	var items = [
		{
			xtype: 'radiogroup', 
			fieldLabel 	: 'Diameter',
			columns : 2,
			items : [
				{
					boxLabel	: '1 mm',
					name		: 'radio_diameter',
					id			: 'radio_diameter_1_mm',
					checked 	: DATA_ASKEP_IGD_AWAL.disability._1mm,
				},{
					boxLabel	: '2 mm',
					name		: 'radio_diameter',
					id			: 'radio_diameter_2_mm',
					checked 	: DATA_ASKEP_IGD_AWAL.disability._2mm,
				},{
					boxLabel	: '3 mm',
					name		: 'radio_diameter',
					id			: 'radio_diameter_3_mm',
					checked 	: DATA_ASKEP_IGD_AWAL.disability._3mm,
				},{
					boxLabel	: '4 mm',
					name		: 'radio_diameter',
					id			: 'radio_diameter_4_mm',
					checked 	: DATA_ASKEP_IGD_AWAL.disability._4mm,
				},
			],
			listeners: {
				change: function (checkbox, newValue, oldValue, eOpts) {
					tmpediting = 'true';
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
					if(newValue.boxLabel == '1 mm'){
						DATA_ASKEP_IGD_AWAL.disability._1mm = true;
						DATA_ASKEP_IGD_AWAL.disability._2mm = false;
						DATA_ASKEP_IGD_AWAL.disability._3mm = false;
						DATA_ASKEP_IGD_AWAL.disability._4mm = false;
					}else if(newValue.boxLabel == '2 mm'){
						DATA_ASKEP_IGD_AWAL.disability._1mm = false;
						DATA_ASKEP_IGD_AWAL.disability._2mm = true;
						DATA_ASKEP_IGD_AWAL.disability._3mm = false;
						DATA_ASKEP_IGD_AWAL.disability._4mm = false;
					}else if(newValue.boxLabel == '3 mm'){
						DATA_ASKEP_IGD_AWAL.disability._1mm = false;
						DATA_ASKEP_IGD_AWAL.disability._3mm = true;
						DATA_ASKEP_IGD_AWAL.disability._2mm = false;
						DATA_ASKEP_IGD_AWAL.disability._4mm = false;
					}else if(newValue.boxLabel == '4 mm'){
						DATA_ASKEP_IGD_AWAL.disability._1mm = false;
						DATA_ASKEP_IGD_AWAL.disability._4mm = true;
						DATA_ASKEP_IGD_AWAL.disability._2mm = false;
						DATA_ASKEP_IGD_AWAL.disability._3mm = false;
					}
				}
			}
		}
	];
	return items;
};

function form_warna_kulit() { 
	var items = [
		{
			xtype: 'radiogroup', 
			fieldLabel 	: '',
			items : [
				{
					boxLabel	: 'Pucat',
					name		: 'radio_warna_kulit',
					id			: 'radio_warna_kulit_pucat',
					checked 	: DATA_ASKEP_IGD_AWAL.warna_kulit.pucat,
				},{
					boxLabel	: 'Sianosis',
					name		: 'radio_warna_kulit',
					id			: 'radio_warna_kulit_sianosis',
					checked 	: DATA_ASKEP_IGD_AWAL.warna_kulit.sianosis,
				},{
					boxLabel	: 'Pink',
					name		: 'radio_warna_kulit',
					id			: 'radio_warna_kulit_pink',
					checked 	: DATA_ASKEP_IGD_AWAL.warna_kulit.pink,
				},
			],
			listeners: {
				change: function (checkbox, newValue, oldValue, eOpts) {
					tmpediting = 'true';
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
					if(newValue.boxLabel == 'Pucat'){
						DATA_ASKEP_IGD_AWAL.warna_kulit.pucat    = true;
						DATA_ASKEP_IGD_AWAL.warna_kulit.sianosis = false;
						DATA_ASKEP_IGD_AWAL.warna_kulit.pink     = false;
					}else if(newValue.boxLabel == 'Sianosis'){
						DATA_ASKEP_IGD_AWAL.warna_kulit.pucat    = false;
						DATA_ASKEP_IGD_AWAL.warna_kulit.sianosis = true;
						DATA_ASKEP_IGD_AWAL.warna_kulit.pink     = false;
					}else{
						DATA_ASKEP_IGD_AWAL.warna_kulit.pucat    = false;
						DATA_ASKEP_IGD_AWAL.warna_kulit.sianosis = false;
						DATA_ASKEP_IGD_AWAL.warna_kulit.pink     = true;
					}
				}
			}
		}
	];
	return items;
};

function form_akral() { 
	var items = [
		{
			xtype: 'radiogroup', 
			fieldLabel 	: '',
			items : [
				{
					boxLabel	: 'Hangat',
					name		: 'radio_breathing_akral',
					id			: 'radio_breathing_akral_hangat',
					checked 	: DATA_ASKEP_IGD_AWAL.akral.hangat,
				},{
					boxLabel	: 'Dingin',
					name		: 'radio_breathing_akral',
					id			: 'radio_breathing_akral_dingin',
					checked 	: DATA_ASKEP_IGD_AWAL.akral.dingin,
				},
			],
			listeners: {
				change: function (checkbox, newValue, oldValue, eOpts) {
					tmpediting = 'true';
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
					if(newValue.boxLabel == 'Hangat'){
						DATA_ASKEP_IGD_AWAL.akral.hangat = true;
						DATA_ASKEP_IGD_AWAL.akral.dingin = false;
					}else{
						DATA_ASKEP_IGD_AWAL.akral.hangat = false;
						DATA_ASKEP_IGD_AWAL.akral.dingin = true;
					}
				}
			}
		}
	];
	return items;
};

function form_breathing() { 
	var items = [
		{
			fieldLabel 	: 'RR (x/menit)',
			xtype 		: 'textfield',
			name 		: 'txt_breathing_rr',
			id 			: 'txt_breathing_rr',
			width 		: '100%',
			value 		: DATA_ASKEP_IGD_AWAL.breathing.rr,
			enableKeyEvents: true,
			listeners 	: {
				'render': function (c) {
					c.getEl().on('keypress', function (e) {
						if (e.getKey() === 13) {
							// Ext.getCmp('txtriwayatpenyakitsekarang').focus();
						}
					}, c);
					c.getEl().on('change', function (e) {
						Ext.getCmp('btnSimpan_viDaftarAwal').enable();
						tmpediting 						= 'true';
						DATA_ASKEP_IGD_AWAL.breathing.rr 	= Ext.get('txt_breathing_rr').getValue();
					}, c);
				}
			}
		},{
			xtype: 'radiogroup', 
			fieldLabel 	: 'Pengguna Otot Bantu Napas',
			items : [
				{
					boxLabel	: 'Tidak',
					name		: 'radio_breathing_bantu_napas',
					id			: 'radio_breathing_bantu_napas_tidak',
					checked 	: true,
				},{
					boxLabel	: 'Ya',
					name		: 'radio_breathing_bantu_napas',
					id			: 'radio_breathing_bantu_napas_ya',
				},
			],
			listeners: {
				change: function (checkbox, newValue, oldValue, eOpts) {
					tmpediting 						= 'true';
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
					if(newValue.boxLabel == 'Ya'){
						DATA_ASKEP_IGD_AWAL.breathing.bantu_nafas = true;
					}else{
						DATA_ASKEP_IGD_AWAL.breathing.bantu_nafas = false;
					}
				},
				render 	: function(){
					if (DATA_ASKEP_IGD_AWAL.breathing.bantu_nafas === true) {
						Ext.getCmp('radio_breathing_bantu_napas_ya').setValue(true);
						Ext.getCmp('radio_breathing_bantu_napas_tidak').setValue(false);
					}else{
						Ext.getCmp('radio_breathing_bantu_napas_ya').setValue(false);
						Ext.getCmp('radio_breathing_bantu_napas_tidak').setValue(true);
					}
				}
			}
		},{
			fieldLabel 	: 'Gerakan Dada',
			boxLabel	: 'Simetris',
			name		: 'checked_breathing_simetris',
			id			: 'checked_breathing_simetris',
			checked		: DATA_ASKEP_IGD_AWAL.breathing.gerakan_dada_simetris,
			listeners: {
				change: function (checkbox, newValue, oldValue, eOpts) {
					tmpediting 						= 'true';
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
					DATA_ASKEP_IGD_AWAL.breathing.gerakan_dada_simetris = newValue;
				}
			}
		},{
			fieldLabel 	: '',
			boxLabel	: 'Asimetris',
			name 		: 'checked_breathing_asimetris',
			id 			: 'checked_breathing_asimetris',
			checked		: DATA_ASKEP_IGD_AWAL.breathing.gerakan_dada_asimetris,
			listeners: {
				change: function (checkbox, newValue, oldValue, eOpts) {
					tmpediting 						= 'true';
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
					DATA_ASKEP_IGD_AWAL.breathing.gerakan_dada_asimetris = newValue;
				}
			}
		},{
			fieldLabel 	: '',
			boxLabel	: 'Asidosis',
			name 		: 'checked_breathing_asidosis',
			id 			: 'checked_breathing_asidosis',
			checked		: DATA_ASKEP_IGD_AWAL.breathing.gerakan_dada_asidosis,
			listeners: {
				change: function (checkbox, newValue, oldValue, eOpts) {
					tmpediting 						= 'true';
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
					DATA_ASKEP_IGD_AWAL.breathing.gerakan_dada_asidosis = newValue;
				}
			}
		},{
			fieldLabel 	: '',
			boxLabel	: 'Alkalosis',
			name 		: 'checked_breathing_alkalosis',
			id 			: 'checked_breathing_alkalosis',
			checked		: DATA_ASKEP_IGD_AWAL.breathing.gerakan_dada_alkalosis,
			listeners: {
				change: function (checkbox, newValue, oldValue, eOpts) {
					tmpediting 						= 'true';
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
					DATA_ASKEP_IGD_AWAL.breathing.gerakan_dada_alkalosis = newValue;
				}
			}
		},{
			fieldLabel 	: '',
			boxLabel	: 'SaO2',
			name 		: 'checked_breathing_sao2',
			id 			: 'checked_breathing_sao2',
			checked		: DATA_ASKEP_IGD_AWAL.breathing.gerakan_dada_sao2,
			listeners: {
				check: function (checkbox, isChecked) {
					tmpediting 						= 'true';
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
					if (isChecked === true) {
						Ext.getCmp('txt_breathing_sao2').enable();
					}else{
						DATA_ASKEP_IGD_AWAL.breathing.gerakan_dada_sao2_percentage = "";
						Ext.getCmp('txt_breathing_sao2').setValue();
						Ext.getCmp('txt_breathing_sao2').disable();
					}

					DATA_ASKEP_IGD_AWAL.breathing.gerakan_dada_sao2 = isChecked;
				},
				render 	: function(){
					if (DATA_ASKEP_IGD_AWAL.breathing.gerakan_dada_sao2 === true) {
						Ext.getCmp('txt_breathing_sao2').enable();
					}else{
						Ext.getCmp('txt_breathing_sao2').disable();
					}
				}
			}
		},{
			fieldLabel 	: '',
			xtype 		: 'textfield',
			name 		: 'txt_breathing_sao2',
			id 			: 'txt_breathing_sao2',
			disabled 	: true,
			width 		: '100%',
			value 		: DATA_ASKEP_IGD_AWAL.breathing.gerakan_dada_sao2_percentage,
			listeners 	: {
				'render': function (c) {
					c.getEl().on('keypress', function (e) {
						if (e.getKey() === 13) {
							// Ext.getCmp('txtriwayatpenyakitsekarang').focus();
						}
					}, c);
					c.getEl().on('change', function (e) {
						Ext.getCmp('btnSimpan_viDaftarAwal').enable();
						tmpediting 						= 'true';
						DATA_ASKEP_IGD_AWAL.breathing.gerakan_dada_sao2_percentage 	= Ext.get('txt_breathing_sao2').getValue();
					}, c);
				}
			}
		},{
			fieldLabel 	: 'Suhu Tubuh (C)',
			xtype 		: 'textfield',
			name 		: 'txt_breathing_suhu',
			id 			: 'txt_breathing_suhu',
			width 		: '100%',
			value 		: DATA_ASKEP_IGD_AWAL.breathing.suhu_badan ,
			listeners 	: {
				'render': function (c) {
					c.getEl().on('keypress', function (e) {
						if (e.getKey() === 13) {
							// Ext.getCmp('txtriwayatpenyakitsekarang').focus();
						}
					}, c);
					c.getEl().on('change', function (e) {
						Ext.getCmp('btnSimpan_viDaftarAwal').enable();
						tmpediting 						= 'true';
						DATA_ASKEP_IGD_AWAL.breathing.suhu_badan 	= Ext.get('txt_breathing_suhu').getValue();
					}, c);
				}
			}
		},{
			fieldLabel 	: 'Riwayat Demam (hari)',
			xtype 		: 'textfield',
			name 		: 'txt_breathing_rwyt_demam',
			id 			: 'txt_breathing_rwyt_demam',
			width 		: '100%',
			value 		: DATA_ASKEP_IGD_AWAL.breathing.riwayat_demam,
			listeners 	: {
				'render': function (c) {
					c.getEl().on('keypress', function (e) {
						if (e.getKey() === 13) {
							// Ext.getCmp('txtriwayatpenyakitsekarang').focus();
						}
					}, c);
					c.getEl().on('change', function (e) {
						Ext.getCmp('btnSimpan_viDaftarAwal').enable();
						tmpediting 						= 'true';
						DATA_ASKEP_IGD_AWAL.breathing.riwayat_demam 	= Ext.get('txt_breathing_rwyt_demam').getValue();
					}, c);
				}
			}
		},{
			fieldLabel 	: 'Riwayat Penyakit',
			boxLabel	: 'Hipertensi',
			name		: 'checked_breathing_hipertensi',
			id			: 'checked_breathing_hipertensi',
			checked 	: DATA_ASKEP_IGD_AWAL.breathing.riwayat_penyakit_hipertensi,
			listeners: {
				change: function (checkbox, newValue, oldValue, eOpts) {
					tmpediting 						= 'true';
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
					DATA_ASKEP_IGD_AWAL.breathing.riwayat_penyakit_hipertensi = newValue;
				}
			}
		},{
			fieldLabel 	: '',
			boxLabel	: 'Diabetes',
			name		: 'checked_breathing_diabetes',
			id			: 'checked_breathing_diabetes',
			checked 	: DATA_ASKEP_IGD_AWAL.breathing.riwayat_penyakit_diabetes,
			listeners: {
				change: function (checkbox, newValue, oldValue, eOpts) {
					tmpediting 						= 'true';
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
					DATA_ASKEP_IGD_AWAL.breathing.riwayat_penyakit_diabetes = newValue;
				}
			}
		},{
			xtype: 'radiogroup', 
			fieldLabel 	: 'Riwayat Alergi',
			items : [
				{
					boxLabel	: 'Tidak',
					name		: 'radio_breathing_alergi',
					id			: 'radio_breathing_alergi_tidak',
				},{
					boxLabel	: 'Ya',
					name		: 'radio_breathing_alergi',
					id			: 'radio_breathing_alergi_ya',
				},
			],
			listeners: {
				change: function (checkbox, newValue, oldValue, eOpts) {
					tmpediting = 'true';
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
					if (newValue.boxLabel == 'Ya') {
						Ext.getCmp('txt_breathing_alergi_ya').enable();
						DATA_ASKEP_IGD_AWAL.breathing.riwayat_alergi = true;
					}else{
						Ext.getCmp('txt_breathing_alergi_ya').disable();
						Ext.getCmp('txt_breathing_alergi_ya').setValue();
						DATA_ASKEP_IGD_AWAL.breathing.riwayat_alergi_detail = "";
						DATA_ASKEP_IGD_AWAL.breathing.riwayat_alergi = false;
					}
				},
				render 	: function(){
					if (DATA_ASKEP_IGD_AWAL.breathing.riwayat_alergi === true) {
						Ext.getCmp('txt_breathing_alergi_ya').enable();
						Ext.getCmp('radio_breathing_alergi_tidak').setValue(false);
						Ext.getCmp('radio_breathing_alergi_ya').setValue(true);
					}else{
						Ext.getCmp('txt_breathing_alergi_ya').disable();
						Ext.getCmp('radio_breathing_alergi_tidak').setValue(true);
						Ext.getCmp('radio_breathing_alergi_ya').setValue(false);
					}
				}
			}
		},{
			fieldLabel 	: '',
			xtype 		: 'textfield',
			name 		: 'txt_breathing_alergi_ya',
			id 			: 'txt_breathing_alergi_ya',
			width 		: '100%',
			disabled 	: true,
			value 		: DATA_ASKEP_IGD_AWAL.breathing.riwayat_alergi_detail,
			listeners 	: {
				'render': function (c) {
					c.getEl().on('keypress', function (e) {
						if (e.getKey() === 13) {
							// Ext.getCmp('txtriwayatpenyakitsekarang').focus();
						}
					}, c);
					c.getEl().on('change', function (e) {
						Ext.getCmp('btnSimpan_viDaftarAwal').enable();
						tmpediting = 'true';
						DATA_ASKEP_IGD_AWAL.breathing.riwayat_alergi_detail 	= Ext.get('txt_breathing_alergi_ya').getValue();
					}, c);
				}
			}
		},
	];
	return items;
};

function form_circulation() { 
	var items = [
		{
			fieldLabel 	: 'HR (x/menit)',
			xtype 		: 'textfield',
			name 		: 'txt_circulation_hr',
			id 			: 'txt_circulation_hr',
			width 		: '100%',
			value 		: DATA_ASKEP_IGD_AWAL.circulation.hr,
			listeners 	: {
				'render': function (c) {
					c.getEl().on('keypress', function (e) {
						if (e.getKey() === 13) {
							// Ext.getCmp('txtriwayatpenyakitsekarang').focus();
						}
					}, c);
					c.getEl().on('change', function (e) {
						Ext.getCmp('btnSimpan_viDaftarAwal').enable();
						tmpediting = 'true';
						DATA_ASKEP_IGD_AWAL.circulation.hr 	= Ext.get('txt_circulation_hr').getValue();
					}, c);
				}
			}
		},{
			xtype: 'checkboxgroup', 
			fieldLabel 	: 'Gerakan Dada',
			columns 	: 2,
			items : [
				{
					boxLabel	: 'Teratur',
					name		: 'checked_circulation_teratur',
					id			: 'checked_circulation_teratur',
					checked 	: DATA_ASKEP_IGD_AWAL.circulation.teratur,
				},{
					boxLabel	: 'Tidak Teratur',
					name 		: 'checked_circulation_tidak_teratur',
					id 			: 'checked_circulation_tidak_teratur',
					checked 	: DATA_ASKEP_IGD_AWAL.circulation.tidak_teratur,
				},{
					boxLabel	: 'Kuat',
					name 		: 'checked_circulation_kuat',
					id 			: 'checked_circulation_kuat',
					checked 	: DATA_ASKEP_IGD_AWAL.circulation.kuat,
				},{
					boxLabel	: 'Lemah',
					name 		: 'checked_circulation_lemah',
					id 			: 'checked_circulation_lemah',
					checked 	: DATA_ASKEP_IGD_AWAL.circulation.lemah,
				}
			],
			listeners: {
				change: function (checkbox, newValue, oldValue, eOpts) {
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
					DATA_ASKEP_IGD_AWAL.circulation.teratur 			= false;
					DATA_ASKEP_IGD_AWAL.circulation.tidak_teratur 	= false;
					DATA_ASKEP_IGD_AWAL.circulation.kuat 			= false;
					DATA_ASKEP_IGD_AWAL.circulation.lemah 			= false;
					for (var i = 0; i < newValue.length; i++) {
						if (newValue[i].id == 'checked_circulation_teratur') {
							DATA_ASKEP_IGD_AWAL.circulation.teratur = newValue[i].checked;
						}

						if (newValue[i].id == 'checked_circulation_tidak_teratur') {
							DATA_ASKEP_IGD_AWAL.circulation.tidak_teratur = newValue[i].checked;
						}

						if (newValue[i].id == 'checked_circulation_kuat') {
							DATA_ASKEP_IGD_AWAL.circulation.kuat = newValue[i].checked;
						}

						if (newValue[i].id == 'checked_circulation_lemah') {
							DATA_ASKEP_IGD_AWAL.circulation.lemah = newValue[i].checked;
						}
					}
				}
			}
		},{
			fieldLabel 	: 'TD (mmHg)',
			xtype 		: 'textfield',
			name 		: 'txt_circulation_td',
			id 			: 'txt_circulation_td',
			width 		: '100%',
			value 		: DATA_ASKEP_IGD_AWAL.circulation.td,
			listeners 	: {
				'render': function (c) {
					c.getEl().on('keypress', function (e) {
						if (e.getKey() === 13) {
							// Ext.getCmp('txtriwayatpenyakitsekarang').focus();
						}
					}, c);
					c.getEl().on('change', function (e) {
						Ext.getCmp('btnSimpan_viDaftarAwal').enable();
						tmpediting = 'true';
						DATA_ASKEP_IGD_AWAL.circulation.td 	= Ext.get('txt_circulation_td').getValue();
					}, c);
				}
			}
		},
	];
	return items;
};

function form_kehamilan() { 
	var items = [
		{
			fieldLabel 	: 'HPHT',
			xtype 		: 'textfield',
			name 		: 'txt_kehamilan_hpht',
			id 			: 'txt_kehamilan_hpht',
			width 		: '100%',
			value 		: DATA_ASKEP_IGD_AWAL.kehamilan.hpht,
			listeners 	: {
				'render': function (c) {
					c.getEl().on('keypress', function (e) {
						if (e.getKey() === 13) {
							// Ext.getCmp('txtriwayatpenyakitsekarang').focus();
						}
					}, c);
					c.getEl().on('change', function (e) {
						Ext.getCmp('btnSimpan_viDaftarAwal').enable();
						tmpediting = 'true';
						DATA_ASKEP_IGD_AWAL.kehamilan.hpht	= Ext.get('txt_kehamilan_hpht').getValue();
					}, c);
				}
			}
		},{
			fieldLabel 	: 'G',
			xtype 		: 'textfield',
			name 		: 'txt_kehamilan_g',
			id 			: 'txt_kehamilan_g',
			width 		: '100%',
			value 		: DATA_ASKEP_IGD_AWAL.kehamilan.g,
			listeners 	: {
				'render': function (c) {
					c.getEl().on('keypress', function (e) {
						if (e.getKey() === 13) {
							// Ext.getCmp('txtriwayatpenyakitsekarang').focus();
						}
					}, c);
					c.getEl().on('change', function (e) {
						Ext.getCmp('btnSimpan_viDaftarAwal').enable();
						tmpediting = 'true';
						DATA_ASKEP_IGD_AWAL.kehamilan.g 	= Ext.get('txt_kehamilan_g').getValue();
					}, c);
				}
			}
		},{
			fieldLabel 	: 'P',
			xtype 		: 'textfield',
			name 		: 'txt_kehamilan_p',
			id 			: 'txt_kehamilan_p',
			value 		: DATA_ASKEP_IGD_AWAL.kehamilan.p,
			width 		: '100%',
			listeners 	: {
				'render': function (c) {
					c.getEl().on('keypress', function (e) {
						if (e.getKey() === 13) {
							// Ext.getCmp('txtriwayatpenyakitsekarang').focus();
						}
					}, c);
					c.getEl().on('change', function (e) {
						Ext.getCmp('btnSimpan_viDaftarAwal').enable();
						tmpediting = 'true';
						DATA_ASKEP_IGD_AWAL.kehamilan.p 	= Ext.get('txt_kehamilan_p').getValue();
					}, c);
				}
			}
		},{
			fieldLabel 	: 'A',
			xtype 		: 'textfield',
			name 		: 'txt_kehamilan_a',
			id 			: 'txt_kehamilan_a',
			value 		: DATA_ASKEP_IGD_AWAL.kehamilan.a,
			width 		: '100%',
			listeners 	: {
				'render': function (c) {
					c.getEl().on('keypress', function (e) {
						if (e.getKey() === 13) {
							// Ext.getCmp('txtriwayatpenyakitsekarang').focus();
						}
					}, c);
					c.getEl().on('change', function (e) {
						Ext.getCmp('btnSimpan_viDaftarAwal').enable();
						tmpediting = 'true';
						DATA_ASKEP_IGD_AWAL.kehamilan.a 	= Ext.get('txt_kehamilan_a').getValue();
					}, c);
				}
			}
		},{
			fieldLabel 	: 'H',
			xtype 		: 'textfield',
			name 		: 'txt_kehamilan_h',
			id 			: 'txt_kehamilan_h',
			value 		: DATA_ASKEP_IGD_AWAL.kehamilan.h,
			width 		: '100%',
			listeners 	: {
				'render': function (c) {
					c.getEl().on('keypress', function (e) {
						if (e.getKey() === 13) {
							// Ext.getCmp('txtriwayatpenyakitsekarang').focus();
						}
					}, c);
					c.getEl().on('change', function (e) {
						Ext.getCmp('btnSimpan_viDaftarAwal').enable();
						tmpediting = 'true';
						DATA_ASKEP_IGD_AWAL.kehamilan.h 	= Ext.get('txt_kehamilan_h').getValue();
					}, c);
				}
			}
		},{
			fieldLabel 	: 'Kehamilan (minggu)',
			xtype 		: 'textfield',
			name 		: 'txt_kehamilan_lama',
			id 			: 'txt_kehamilan_lama',
			value 		: DATA_ASKEP_IGD_AWAL.kehamilan.minggu,
			width 		: '100%',
			listeners 	: {
				'render': function (c) {
					c.getEl().on('keypress', function (e) {
						if (e.getKey() === 13) {
							// Ext.getCmp('txtriwayatpenyakitsekarang').focus();
						}
					}, c);
					c.getEl().on('change', function (e) {
						Ext.getCmp('btnSimpan_viDaftarAwal').enable();
						tmpediting = 'true';
						DATA_ASKEP_IGD_AWAL.kehamilan.minggu	= Ext.get('txt_kehamilan_lama').getValue();
					}, c);
				}
			}
		},
	];
	return items;
};

function form_skala_nyeri() { 
	var items = [
		{
			fieldLabel 	: 'RINGAN',
			boxLabel 	: '0-3',
			name 		: 'radio_skala_nyeri',
			id 			: 'radio_skala_nyeri_ringan',
			checked 	: DATA_ASKEP_IGD_AWAL.skala_nyeri.ringan,
			listeners: {
				change: function (checkbox, newValue, oldValue, eOpts) {
					tmpediting = 'true';
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
					DATA_ASKEP_IGD_AWAL.skala_nyeri.ringan = false;
					DATA_ASKEP_IGD_AWAL.skala_nyeri.sedang = false;
					DATA_ASKEP_IGD_AWAL.skala_nyeri.berat  = false;
					DATA_ASKEP_IGD_AWAL.skala_nyeri.akut   = false;
					DATA_ASKEP_IGD_AWAL.skala_nyeri.kronis = false;
					DATA_ASKEP_IGD_AWAL.skala_nyeri.ringan = newValue;
				},
				check : function(){
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
				}
			}
		},{
			fieldLabel 	: 'SEDANG',
			boxLabel 	: '4-6',
			name 		: 'radio_skala_nyeri',
			id 			: 'radio_skala_nyeri_sedang',
			checked 	: DATA_ASKEP_IGD_AWAL.skala_nyeri.sedang,
			listeners: {
				change: function (checkbox, newValue, oldValue, eOpts) {
					tmpediting = 'true';
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
					DATA_ASKEP_IGD_AWAL.skala_nyeri.ringan = false;
					DATA_ASKEP_IGD_AWAL.skala_nyeri.sedang = false;
					DATA_ASKEP_IGD_AWAL.skala_nyeri.berat  = false;
					DATA_ASKEP_IGD_AWAL.skala_nyeri.akut   = false;
					DATA_ASKEP_IGD_AWAL.skala_nyeri.kronis = false;
					DATA_ASKEP_IGD_AWAL.skala_nyeri.sedang = newValue;
				},
				check : function(){
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
				}
			}
		},{
			fieldLabel 	: 'BERAT',
			boxLabel 	: '7-10',
			name 		: 'radio_skala_nyeri',
			id 			: 'radio_skala_nyeri_berat',
			checked 	: DATA_ASKEP_IGD_AWAL.skala_nyeri.berat,
			listeners: {
				change: function (checkbox, newValue, oldValue, eOpts) {
					tmpediting = 'true';
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
					DATA_ASKEP_IGD_AWAL.skala_nyeri.ringan = false;
					DATA_ASKEP_IGD_AWAL.skala_nyeri.sedang = false;
					DATA_ASKEP_IGD_AWAL.skala_nyeri.berat  = false;
					DATA_ASKEP_IGD_AWAL.skala_nyeri.akut   = false;
					DATA_ASKEP_IGD_AWAL.skala_nyeri.kronis = false;
					DATA_ASKEP_IGD_AWAL.skala_nyeri.berat = newValue;
				},
				check : function(){
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
				}
			}
		},{
			fieldLabel 	: 'AKUT',
			name 		: 'radio_skala_nyeri',
			id 			: 'radio_skala_nyeri_akut',
			checked 	: DATA_ASKEP_IGD_AWAL.skala_nyeri.akut,
			listeners: {
				change: function (checkbox, newValue, oldValue, eOpts) {
					tmpediting = 'true';
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
					DATA_ASKEP_IGD_AWAL.skala_nyeri.ringan = false;
					DATA_ASKEP_IGD_AWAL.skala_nyeri.sedang = false;
					DATA_ASKEP_IGD_AWAL.skala_nyeri.berat  = false;
					DATA_ASKEP_IGD_AWAL.skala_nyeri.akut   = false;
					DATA_ASKEP_IGD_AWAL.skala_nyeri.kronis = false;
					DATA_ASKEP_IGD_AWAL.skala_nyeri.akut = newValue;
				},
				check : function(){
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
				}
			}
		},{
			fieldLabel 	: 'KRONIS',
			name 		: 'radio_skala_nyeri',
			id 			: 'radio_skala_nyeri_kronis',
			checked 	: DATA_ASKEP_IGD_AWAL.skala_nyeri.kronis,
			listeners: {
				change: function (checkbox, newValue, oldValue, eOpts) {
					tmpediting = 'true';
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
					DATA_ASKEP_IGD_AWAL.skala_nyeri.ringan = false;
					DATA_ASKEP_IGD_AWAL.skala_nyeri.sedang = false;
					DATA_ASKEP_IGD_AWAL.skala_nyeri.berat  = false;
					DATA_ASKEP_IGD_AWAL.skala_nyeri.akut   = false;
					DATA_ASKEP_IGD_AWAL.skala_nyeri.kronis = false;
					DATA_ASKEP_IGD_AWAL.skala_nyeri.kronis = newValue;
				},
				check : function(){
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
				}
			}
		},
	];
	return items;
};

function form_resiko_jatuh() { 
	var items = [
		{
			boxLabel 	: 'Rendah',
			name 		: 'radio_resiko_jatuh',
			id 			: 'radio_resiko_jatuh_rendah',
			checked 	: DATA_ASKEP_IGD_AWAL.resiko_jatuh.rendah,
			listeners: {
				change: function (checkbox, newValue, oldValue, eOpts) {
					tmpediting = 'true';
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
					DATA_ASKEP_IGD_AWAL.resiko_jatuh.rendah = false;
					DATA_ASKEP_IGD_AWAL.resiko_jatuh.sedang = false;
					DATA_ASKEP_IGD_AWAL.resiko_jatuh.tinggi  = false;
					DATA_ASKEP_IGD_AWAL.resiko_jatuh.rendah = newValue;
				},
				check : function(){
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
				}
			}
		},{
			boxLabel 	: 'Sedang',
			name 		: 'radio_resiko_jatuh',
			id 			: 'radio_resiko_jatuh_sedang',
			checked 	: DATA_ASKEP_IGD_AWAL.resiko_jatuh.sedang,
			listeners: {
				change: function (checkbox, newValue, oldValue, eOpts) {
					tmpediting = 'true';
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
					DATA_ASKEP_IGD_AWAL.resiko_jatuh.rendah = false;
					DATA_ASKEP_IGD_AWAL.resiko_jatuh.sedang = false;
					DATA_ASKEP_IGD_AWAL.resiko_jatuh.tinggi = false;
					DATA_ASKEP_IGD_AWAL.resiko_jatuh.sedang = newValue;
				},
				check : function(){
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
				}
			}
		},{
			boxLabel 	: 'Tinggi',
			name 		: 'radio_resiko_jatuh',
			id 			: 'radio_resiko_jatuh_tinggi',
			checked 	: DATA_ASKEP_IGD_AWAL.resiko_jatuh.tinggi,
			listeners: {
				change: function (checkbox, newValue, oldValue, eOpts) {
					tmpediting = 'true';
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
					DATA_ASKEP_IGD_AWAL.resiko_jatuh.rendah = false;
					DATA_ASKEP_IGD_AWAL.resiko_jatuh.sedang = false;
					DATA_ASKEP_IGD_AWAL.resiko_jatuh.tinggi  = false;
					DATA_ASKEP_IGD_AWAL.resiko_jatuh.tinggi = newValue;
				},
				check : function(){
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
				}
			}
		},
	];
	return items;
};

function form_triase_jalan_nafas() { 
	var items = [
		{
			boxLabel 	: 'Sumbatan',
			name 		: 'radio_triase_jalan_nafas',
			id 			: 'radio_triase_jalan_nafas_sumbatan',
			hidden 		: true,
		},{
			boxLabel 	: 'Ancaman',
			name 		: 'radio_triase_jalan_nafas',
			id 			: 'radio_triase_jalan_nafas_ancaman',
			hidden 		: true,
		},{
			xtype 		: 'textfield',
			name 		: 'txt_triase_jalan_nafas',
			id 			: 'txt_triase_jalan_nafas_bebas',
			hidden 		: true,
			width 		: '100%',
		},
	];
	return items;
};

function form_triase_pernafasan() { 
	var items = [
		{
			boxLabel 	: 'Henti nafas',
			name 		: 'check_triase_pernafasan',
			id 			: 'check_triase_pernafasan_henti_nafas',
			hidden 		: true,
		},{
			boxLabel 	: 'Bradipnoe',
			name 		: 'check_triase_pernafasan',
			id 			: 'check_triase_pernafasan_bradipnoe',
			hidden 		: true,
		},{
			boxLabel 	: 'Sianosis',
			name 		: 'check_triase_pernafasan',
			id 			: 'check_triase_pernafasan_sianosis',
			hidden 		: true,
		},{
			boxLabel 	: 'Takipnoe',
			name 		: 'check_triase_pernafasan',
			id 			: 'check_triase_pernafasan_takipnoe',
			hidden 		: true,
		},{
			boxLabel 	: 'Mengi',
			name 		: 'check_triase_pernafasan',
			id 			: 'check_triase_pernafasan_mengi',
			hidden 		: true,
		},{
			boxLabel 	: 'Normal',
			name 		: 'check_triase_pernafasan',
			id 			: 'check_triase_pernafasan_normal',
			hidden 		: true,
		},{
			boxLabel 	: 'Frekuensi nafas normal',
			name 		: 'check_triase_pernafasan',
			id 			: 'check_triase_pernafasan_frekuensi',
			hidden 		: true,
		},
	];
	return items;
};

function form_triase_sirkulasi() { 
	var items = [
		{
			xtype 		: 'checkboxgroup',
			columns 	: 2,
			items 		: [
				{
					boxLabel 	: 'Henti Jantung',
					name 		: 'check_triase_sirkulasi',
					id 			: 'check_triase_sirkulasi_henti_jantung',
					hidden 		: true,
				},{
					boxLabel 	: 'Nadi Tidak Teraba',
					name 		: 'check_triase_sirkulasi',
					id 			: 'check_triase_sirkulasi_nadi_tidak_teraba',
					hidden 		: true,
				},{
					boxLabel 	: 'Akral Dingin',
					name 		: 'check_triase_sirkulasi',
					id 			: 'check_triase_sirkulasi_akral_dingin',
					hidden 		: true,
				},{
					boxLabel 	: 'Nadi Teraba Lemah',
					name 		: 'check_triase_sirkulasi',
					id 			: 'check_triase_sirkulasi_nadi_teraba_lemah',
					hidden 		: true,
				},{
					boxLabel 	: 'Bradikardi',
					name 		: 'check_triase_sirkulasi',
					id 			: 'check_triase_sirkulasi_bradikardi',
					hidden 		: true,
				},{
					boxLabel 	: 'Takikardi',
					name 		: 'check_triase_sirkulasi',
					id 			: 'check_triase_sirkulasi_takikardi',
					hidden 		: true,
				},{
					boxLabel 	: 'Pucat',
					name 		: 'check_triase_sirkulasi',
					id 			: 'check_triase_sirkulasi_pucat',
					hidden 		: true,
				},{
					boxLabel 	: 'CRT > 2 detik',
					name 		: 'check_triase_sirkulasi',
					id 			: 'check_triase_sirkulasi_crt_2_detik',
					hidden 		: true,
				},{
					boxLabel 	: 'Nadi Kuat',
					name 		: 'check_triase_sirkulasi',
					id 			: 'check_triase_sirkulasi_nadi_kuat',
					hidden 		: true,
				},{
					boxLabel 	: 'TDS > 160 mmHg',
					name 		: 'check_triase_sirkulasi',
					id 			: 'check_triase_sirkulasi_tds_160_mmhg',
					hidden 		: true,
				},{
					boxLabel 	: 'TDD > 100 mmHg',
					name 		: 'check_triase_sirkulasi',
					id 			: 'check_triase_sirkulasi_tdd_100_mmhg',
					hidden 		: true,
				},{
					boxLabel 	: 'Frekuensi nadi normal',
					name 		: 'check_triase_sirkulasi',
					id 			: 'check_triase_sirkulasi_frekuensi_nadi_normal',
					hidden 		: true,
				},{
					boxLabel 	: 'TDS 140 - 160 mmHg',
					name 		: 'check_triase_sirkulasi',
					id 			: 'check_triase_sirkulasi_tds_140_160_mmhg',
					hidden 		: true,
				},{
					boxLabel 	: 'TDD 90 - 100 mmHg',
					name 		: 'check_triase_sirkulasi',
					id 			: 'check_triase_sirkulasi_tdd_90_100_mmhg',
					hidden 		: true,
				},{
					boxLabel 	: 'TD Normal',
					name 		: 'check_triase_sirkulasi',
					id 			: 'check_triase_sirkulasi_td_normal',
					hidden 		: true,
				},
			]	
		}
	];
	return items;
};

function form_triase_kesadaran() { 
	var items = [
		{
			xtype 		: 'checkboxgroup',
			columns 	: 2, 
			items 		: [
				{
					boxLabel 	: 'GCS <= 8',
					name 		: 'check_triase_kesadaran',
					id 			: 'check_triase_kesadaran_gcs_8',
					hidden 		: true,
				},{
					boxLabel 	: 'Kejang',
					name 		: 'check_triase_kesadaran',
					id 			: 'check_triase_kesadaran_kejang',
					hidden 		: true,
				},{
					boxLabel 	: 'Tidak ada respon',
					name 		: 'check_triase_kesadaran',
					id 			: 'check_triase_kesadaran_tidak_ada_respon',
					hidden 		: true,
				},{
					boxLabel 	: 'GCS 9 - 12',
					name 		: 'check_triase_kesadaran',
					id 			: 'check_triase_kesadaran_gcs_9_12',
					hidden 		: true,
				},{
					boxLabel 	: 'Gelisah',
					name 		: 'check_triase_kesadaran',
					id 			: 'check_triase_kesadaran_gelisah',
					hidden 		: true,
				},{
					boxLabel 	: 'Hemiparese',
					name 		: 'check_triase_kesadaran',
					id 			: 'check_triase_kesadaran_hemiparese',
					hidden 		: true,
				},{
					boxLabel 	: 'Nyeri Dada',
					name 		: 'check_triase_kesadaran',
					id 			: 'check_triase_kesadaran_nyeri_dada',
					hidden 		: true,
				},{
					boxLabel 	: 'GCS > 12',
					name 		: 'check_triase_kesadaran',
					id 			: 'check_triase_kesadaran_gcs_12',
					hidden 		: true,
				},{
					boxLabel 	: 'Apatis',
					name 		: 'check_triase_kesadaran',
					id 			: 'check_triase_kesadaran_apatis',
					hidden 		: true,
				},{
					boxLabel 	: 'Somnolen',
					name 		: 'check_triase_kesadaran',
					id 			: 'check_triase_kesadaran_somnolen',
					hidden 		: true,
				},{
					boxLabel 	: 'GCS 15',
					name 		: 'check_triase_kesadaran',
					id 			: 'check_triase_kesadaran_gcs_15',
					hidden 		: true,
				},
			]
		}
	];
	return items;
};

function form_triase_lanjutan() { 
	var items = [
		{
			xtype 		: 'checkboxgroup',
			columns 	: 2, 
			items 		: [
				{
					boxLabel 	: 'Resusitasi',
					name 		: 'check_triase_lanjutan',
					id 			: 'check_triase_lanjutan_resusitasi',
                    checked     : DATA_ASKEP_IGD_AWAL.lanjutan.resusitasi,
				},{
					boxLabel 	: 'Medikal',
					name 		: 'check_triase_lanjutan',
					id 			: 'check_triase_lanjutan_medikal',
                    checked     : DATA_ASKEP_IGD_AWAL.lanjutan.medikal,
				},{
					boxLabel 	: 'Anak',
					name 		: 'check_triase_lanjutan',
					id 			: 'check_triase_lanjutan_anak',
                    checked     : DATA_ASKEP_IGD_AWAL.lanjutan.anak,
				},{
					boxLabel 	: 'Surgikal',
					name 		: 'check_triase_lanjutan',
					id 			: 'check_triase_lanjutan_surgikal',
                    checked     : DATA_ASKEP_IGD_AWAL.lanjutan.surgikal,
				},{
					boxLabel 	: 'Obgin',
					name 		: 'check_triase_lanjutan',
					id 			: 'check_triase_lanjutan_obgin',
                    checked     : DATA_ASKEP_IGD_AWAL.lanjutan.obgin,
				},
			],
            listeners   : {
                change: function (checkbox, newValue, oldValue, eOpts) {
                    DATA_ASKEP_IGD_AWAL.lanjutan.resusitasi = false;
                    DATA_ASKEP_IGD_AWAL.lanjutan.anak       = false;
                    DATA_ASKEP_IGD_AWAL.lanjutan.obgin      = false;
                    DATA_ASKEP_IGD_AWAL.lanjutan.medikal    = false;
                    DATA_ASKEP_IGD_AWAL.lanjutan.surgikal   = false;
                    tmpediting = 'true';
                    Ext.getCmp('btnSimpan_viDaftarAwal').enable();

                    for (var i = 0; i < newValue.length; i++) {
                        if (newValue[i].id == 'check_triase_lanjutan_resusitasi') {
                            DATA_ASKEP_IGD_AWAL.lanjutan.resusitasi = newValue[i].checked;
                        }

                        if (newValue[i].id == 'check_triase_lanjutan_anak') {
                            DATA_ASKEP_IGD_AWAL.lanjutan.anak = newValue[i].checked;
                        }

                        if (newValue[i].id == 'check_triase_lanjutan_obgin') {
                            DATA_ASKEP_IGD_AWAL.lanjutan.obgin = newValue[i].checked;
                        }

                        if (newValue[i].id == 'check_triase_lanjutan_medikal') {
                            DATA_ASKEP_IGD_AWAL.lanjutan.medikal = newValue[i].checked;
                        }

                        if (newValue[i].id == 'check_triase_lanjutan_surgikal') {
                            DATA_ASKEP_IGD_AWAL.lanjutan.surgikal = newValue[i].checked;
                        }
                    }
                }
            }
		}
	];
	return items;
};

function form_triase_keadaan_umum() { 
    var items = [
        {
            xtype       : 'fieldset',
            layout      : 'column',
            bodyStyle   : 'margin : 0px;padding:0px;',
            border      : false,
            labelWidth  : 120,
            items       : [
                {
                    columnWidth : .5,
                    layout      : 'form',
                    border      : false,
                    items: [
                        {
                            xtype       : 'textfield',
                            fieldLabel  : 'Tekanan Darah',
                            name        : 'txt_triase_keadaan_umum_tekanan_darah',
                            id          : 'txt_triase_keadaan_umum_tekanan_darah',
                            anchor      : '100%',
                            value       : DATA_ASKEP_IGD_AWAL.tanda_vital.keadaan_umum_td,
                            listeners   : {
                                render  : function(c){
                                    c.getEl().on('keypress', function (e) {
                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                        tmpediting = 'true';
                                    }, c);
                                    c.getEl().on('change', function (e) {
                                        DATA_ASKEP_IGD_AWAL.tanda_vital.keadaan_umum_td = Ext.getCmp('txt_triase_keadaan_umum_tekanan_darah').getValue();
                                    }, c);
                                }
                            },
                        }, 
                    ]
                },{
                    columnWidth : .5,
                    layout      : 'form',
                    border      : false,
                    items: [
                        {
                            xtype       : 'textfield',
                            fieldLabel  : '/ (mmHg)',
                            name        : 'txt_triase_keadaan_umum_mmhg',
                            id          : 'txt_triase_keadaan_umum_mmhg',
                            anchor      : '100%',
                            value       : DATA_ASKEP_IGD_AWAL.tanda_vital.keadaan_umum_mmhg,
                            listeners   : {
                                render  : function(c){
                                    c.getEl().on('keypress', function (e) {
                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                        tmpediting = 'true';
                                    }, c);
                                    c.getEl().on('change', function (e) {
                                        DATA_ASKEP_IGD_AWAL.tanda_vital.keadaan_umum_mmhg = Ext.getCmp('txt_triase_keadaan_umum_mmhg').getValue();
                                    }, c);
                                }
                            },
                        },
                    ]
                },
            ]
        },{
            fieldLabel  : 'Suhu (Cel)',
            id          : 'txt_triase_keadaan_umum_suhu',
            name        : 'txt_triase_keadaan_umum_suhu',
            value       : DATA_ASKEP_IGD_AWAL.tanda_vital.keadaan_umum_suhu,
            listeners   : {
                render  : function(c){
                    c.getEl().on('keypress', function (e) {
                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                        tmpediting = 'true';
                    }, c);
                    c.getEl().on('change', function (e) {
                        DATA_ASKEP_IGD_AWAL.tanda_vital.keadaan_umum_suhu = Ext.getCmp('txt_triase_keadaan_umum_suhu').getValue();
                    }, c);
                }
            }
        },{
            fieldLabel  : 'Nadi (X/M)',
            id          : 'txt_triase_keadaan_umum_nadi',
            name        : 'txt_triase_keadaan_umum_nadi',
            value       : DATA_ASKEP_IGD_AWAL.tanda_vital.keadaan_umum_nadi,
            listeners   : {
                render  : function(c){
                    c.getEl().on('keypress', function (e) {
                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                        tmpediting = 'true';
                    }, c);
                    c.getEl().on('change', function (e) {
                        DATA_ASKEP_IGD_AWAL.tanda_vital.keadaan_umum_nadi = Ext.getCmp('txt_triase_keadaan_umum_nadi').getValue();
                    }, c);
                }
            }
        },{
            fieldLabel  : 'Nafas (X/menit)',
            id          : 'txt_triase_keadaan_umum_nafas',
            name        : 'txt_triase_keadaan_umum_nafas',
            value       : DATA_ASKEP_IGD_AWAL.tanda_vital.keadaan_umum_nafas,
            listeners   : {
                render  : function(c){
                    c.getEl().on('keypress', function (e) {
                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                        tmpediting = 'true';
                    }, c);
                    c.getEl().on('change', function (e) {
                        DATA_ASKEP_IGD_AWAL.tanda_vital.keadaan_umum_nafas = Ext.getCmp('txt_triase_keadaan_umum_nafas').getValue();
                    }, c);
                }
            }
        },{
            fieldLabel  : 'SaO2 (%)',
            id          : 'txt_triase_keadaan_umum_sao2',
            name        : 'txt_triase_keadaan_umum_sao2',
            value       : DATA_ASKEP_IGD_AWAL.tanda_vital.keadaan_umum_sao2,
            listeners   : {
                render  : function(c){
                    c.getEl().on('keypress', function (e) {
                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                        tmpediting = 'true';
                    }, c);
                    c.getEl().on('change', function (e) {
                        DATA_ASKEP_IGD_AWAL.tanda_vital.keadaan_umum_sao2 = Ext.getCmp('txt_triase_keadaan_umum_sao2').getValue();
                    }, c);
                }
            }
        },
    ];
    return items;
};

function form_pemeriksaan_fisik_ku() { 
    var items = [
        {
            xtype       : 'radiogroup',
            columns     : 1,
            fieldLabel  : '',
            items       : [
                {
                    boxLabel    : 'Baik',
                    id          : 'check_pemeriksaan_fisik_ku_baik',
                    name        : 'check_pemeriksaan_fisik_ku',
                    value       : 0,
                },{
                    boxLabel    : 'Sakit Ringan',
                    id          : 'check_pemeriksaan_fisik_ku_sakit_ringan',
                    name        : 'check_pemeriksaan_fisik_ku',
                    value       : 1,
                },{
                    boxLabel    : 'Sakit Sedang',
                    id          : 'check_pemeriksaan_fisik_ku_sakit_sedang',
                    name        : 'check_pemeriksaan_fisik_ku',
                    value       : 2,
                },{
                    boxLabel    : 'Sakit Berat',
                    id          : 'check_pemeriksaan_fisik_ku_sakit_berat',
                    name        : 'check_pemeriksaan_fisik_ku',
                    value       : 3,
                },
            ],
            listeners   : {
                change  : function(a, b){
                    Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                    tmpediting = 'true';
                    DATA_ASKEP_IGD_AWAL.pemeriksaan.ku = b.value;
                },
                render  : function(){
                    if(DATA_ASKEP_IGD_AWAL.pemeriksaan.ku == 0 || DATA_ASKEP_IGD_AWAL.pemeriksaan.ku == '0'){
                        Ext.getCmp('check_pemeriksaan_fisik_ku_baik').setValue(true);
                    }else if(DATA_ASKEP_IGD_AWAL.pemeriksaan.ku == 1 || DATA_ASKEP_IGD_AWAL.pemeriksaan.ku == '1'){
                        Ext.getCmp('check_pemeriksaan_fisik_ku_sakit_ringan').setValue(true);
                    }else if(DATA_ASKEP_IGD_AWAL.pemeriksaan.ku == 2 || DATA_ASKEP_IGD_AWAL.pemeriksaan.ku == '2'){
                        Ext.getCmp('check_pemeriksaan_fisik_ku_sakit_sedang').setValue(true);
                    }else if(DATA_ASKEP_IGD_AWAL.pemeriksaan.ku == 3 || DATA_ASKEP_IGD_AWAL.pemeriksaan.ku == '3'){
                        Ext.getCmp('check_pemeriksaan_fisik_ku_sakit_berat').setValue(true);
                    }
                }
            }
        }
    ];
    return items;
};


function form_pemeriksaan_fisik_kesadaran() { 
    var items = [
        {
            xtype       : 'checkboxgroup',
            columns     : 1, 
            fieldLabel  : '',
            items       : [
                {
                    boxLabel    : 'CM',
                    id          : 'check_pemeriksaan_fisik_kesadaran_cm',
                    name        : 'check_pemeriksaan_fisik_kesadaran_cm',
                    checked     : DATA_ASKEP_IGD_AWAL.pemeriksaan.kesadaran_cm,
                },{
                    boxLabel    : 'Apatis',
                    id          : 'check_pemeriksaan_fisik_kesadaran_apatis',
                    name        : 'check_pemeriksaan_fisik_kesadaran_apatis',
                    checked     : DATA_ASKEP_IGD_AWAL.pemeriksaan.kesadaran_apatis,
                },{
                    boxLabel    : 'Somnolen',
                    id          : 'check_pemeriksaan_fisik_kesadaran_somnolen',
                    name        : 'check_pemeriksaan_fisik_kesadaran_somnolen',
                    checked     : DATA_ASKEP_IGD_AWAL.pemeriksaan.kesadaran_somnolen,
                },{
                    boxLabel    : 'Sopor',
                    id          : 'check_pemeriksaan_fisik_kesadaran_sopor',
                    name        : 'check_pemeriksaan_fisik_kesadaran_sopor',
                    checked     : DATA_ASKEP_IGD_AWAL.pemeriksaan.kesadaran_sopor,
                },{
                    boxLabel    : 'Koma',
                    id          : 'check_pemeriksaan_fisik_kesadaran_koma',
                    name        : 'check_pemeriksaan_fisik_kesadaran_koma',
                    checked     : DATA_ASKEP_IGD_AWAL.pemeriksaan.kesadaran_koma,
                },
            ],
            listeners   : {
                change: function (checkbox, newValue, oldValue, eOpts) {
                    Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                    tmpediting = 'true';
                    DATA_ASKEP_IGD_AWAL.pemeriksaan.kesadaran_cm       = false;
                    DATA_ASKEP_IGD_AWAL.pemeriksaan.kesadaran_apatis   = false;
                    DATA_ASKEP_IGD_AWAL.pemeriksaan.kesadaran_somnolen = false;
                    DATA_ASKEP_IGD_AWAL.pemeriksaan.kesadaran_sopor    = false;
                    DATA_ASKEP_IGD_AWAL.pemeriksaan.kesadaran_koma     = false;

                    for (var i = 0; i < newValue.length; i++) {
                        if (newValue[i].id == 'check_pemeriksaan_fisik_kesadaran_cm') {
                            DATA_ASKEP_IGD_AWAL.pemeriksaan.kesadaran_cm = newValue[i].checked;
                        }

                        if (newValue[i].id == 'check_pemeriksaan_fisik_kesadaran_apatis') {
                            DATA_ASKEP_IGD_AWAL.pemeriksaan.kesadaran_apatis  = newValue[i].checked;
                        }

                        if (newValue[i].id == 'check_pemeriksaan_fisik_kesadaran_somnolen') {
                            DATA_ASKEP_IGD_AWAL.pemeriksaan.kesadaran_somnolen  = newValue[i].checked;
                        }

                        if (newValue[i].id == 'check_pemeriksaan_fisik_kesadaran_sopor') {
                            DATA_ASKEP_IGD_AWAL.pemeriksaan.kesadaran_sopor   = newValue[i].checked;
                        }

                        if (newValue[i].id == 'check_pemeriksaan_fisik_kesadaran_koma') {
                            DATA_ASKEP_IGD_AWAL.pemeriksaan.kesadaran_koma  = newValue[i].checked;
                        }
                    }
                },
            }
        }
    ];
    return items;
};

function form_pemeriksaan_fisik_() { 
	var items = [
        {
            fieldLabel  : 'GCS',
            id          : 'check_pemeriksaan_fisik_gcs',
            name        : 'check_pemeriksaan_fisik_gcs',
            width       : '100%',
            value       : DATA_ASKEP_IGD_AWAL.pemeriksaan.gcs,
            listeners   : {
                render  : function(c){
                    c.getEl().on('keypress', function (e) {
                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                        tmpediting = 'true';
                    }, c);
                    c.getEl().on('change', function (e) {
                        DATA_ASKEP_IGD_AWAL.pemeriksaan.gcs = Ext.getCmp('check_pemeriksaan_fisik_gcs').getValue();
                    }, c);
                }
            }
        },{
            fieldLabel  : 'E',
            id          : 'check_pemeriksaan_fisik_e',
            name        : 'check_pemeriksaan_fisik_e',
            width       : '100%',
            value       : DATA_ASKEP_IGD_AWAL.pemeriksaan.e,
            listeners   : {
                render  : function(c){
                    c.getEl().on('keypress', function (e) {
                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                        tmpediting = 'true';
                    }, c);
                    c.getEl().on('change', function (e) {
                        DATA_ASKEP_IGD_AWAL.pemeriksaan.e = Ext.getCmp('check_pemeriksaan_fisik_e').getValue();
                    }, c);
                }
            }
        },{
            fieldLabel  : 'M',
            id          : 'check_pemeriksaan_fisik_m',
            name        : 'check_pemeriksaan_fisik_m',
            width       : '100%',
            value       : DATA_ASKEP_IGD_AWAL.pemeriksaan.m,
            listeners   : {
                render  : function(c){
                    c.getEl().on('keypress', function (e) {
                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                        tmpediting = 'true';
                    }, c);
                    c.getEl().on('change', function (e) {
                        DATA_ASKEP_IGD_AWAL.pemeriksaan.m = Ext.getCmp('check_pemeriksaan_fisik_m').getValue();
                    }, c);
                }
            }
        },{
            fieldLabel  : 'V',
            id          : 'check_pemeriksaan_fisik_v',
            name        : 'check_pemeriksaan_fisik_v',
            width       : '100%',
            value       : DATA_ASKEP_IGD_AWAL.pemeriksaan.v,
            listeners   : {
                render  : function(c){
                    c.getEl().on('keypress', function (e) {
                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                        tmpediting = 'true';
                    }, c);
                    c.getEl().on('change', function (e) {
                        DATA_ASKEP_IGD_AWAL.pemeriksaan.v = Ext.getCmp('check_pemeriksaan_fisik_v').getValue();
                    }, c);
                }
            }
        },
	];
	return items;
};

function form_fungsional_sensorik() { 
	var items = [
        {
            fieldLabel  : 'Penglihatan',
            xtype       : 'radiogroup',
            id          : 'radio_assesment_sensorik_penglihatan_induk',
            columns     : 1,
            items       : [
                {
                    fieldLabel  : '',
                    boxLabel    : 'Normal',
                    name        : 'radio_assesment_sensorik_penglihatan',
                    id          : 'radio_assesment_sensorik_penglihatan_normal',
                    value       : 0, 
                },{
                    fieldLabel  : '',
                    boxLabel    : 'Kabur',
                    name        : 'radio_assesment_sensorik_penglihatan',
                    id          : 'radio_assesment_sensorik_penglihatan_kabur',
                    value       : 1, 
                },{
                    fieldLabel  : '',
                    boxLabel    : 'Kaca Mata',
                    name        : 'radio_assesment_sensorik_penglihatan',
                    id          : 'radio_assesment_sensorik_penglihatan_kaca_mata',
                    value       : 2, 
                },{
                    fieldLabel  : '',
                    boxLabel    : 'Lensa Kontak',
                    name        : 'radio_assesment_sensorik_penglihatan',
                    id          : 'radio_assesment_sensorik_penglihatan_lensa_kontak',
                    value       : 3, 
                },
            ],
            listeners   : {
                change  : function(a, b){
                    Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                    Ext.getCmp('txt_assesment_sensorik_penglihatan_lain_lain').setValue();
                    DATA_ASKEP_IGD_AWAL.sensorik.penglihatan = b.value;
                },
                render  : function(){
                    if(DATA_ASKEP_IGD_AWAL.sensorik.penglihatan === '0' || DATA_ASKEP_IGD_AWAL.sensorik.penglihatan === 0){
                        Ext.getCmp('radio_assesment_sensorik_penglihatan_normal').setValue(true);
                    }else if(DATA_ASKEP_IGD_AWAL.sensorik.penglihatan === '1' || DATA_ASKEP_IGD_AWAL.sensorik.penglihatan === 1){
                        Ext.getCmp('radio_assesment_sensorik_penglihatan_kabur').setValue(true);
                    }else if(DATA_ASKEP_IGD_AWAL.sensorik.penglihatan === '2' || DATA_ASKEP_IGD_AWAL.sensorik.penglihatan === 2){
                        Ext.getCmp('radio_assesment_sensorik_penglihatan_kaca_mata').setValue(true);
                    }else if(DATA_ASKEP_IGD_AWAL.sensorik.penglihatan === '3' || DATA_ASKEP_IGD_AWAL.sensorik.penglihatan === 3){
                        Ext.getCmp('radio_assesment_sensorik_penglihatan_lensa_kontak').setValue(true);
                    }
                }
            }
        },{
            xtype       : 'textfield',
            fieldLabel  : 'Lain-lain',
            name        : 'txt_assesment_sensorik_penglihatan',
            id          : 'txt_assesment_sensorik_penglihatan_lain_lain',
            width       : '100%',
            listeners   : {
                'render': function (c) {

                    if(DATA_ASKEP_IGD_AWAL.sensorik.penglihatan === '0' || DATA_ASKEP_IGD_AWAL.sensorik.penglihatan === 0){
                    }else if(DATA_ASKEP_IGD_AWAL.sensorik.penglihatan === '1' || DATA_ASKEP_IGD_AWAL.sensorik.penglihatan === 1){
                    }else if(DATA_ASKEP_IGD_AWAL.sensorik.penglihatan === '2' || DATA_ASKEP_IGD_AWAL.sensorik.penglihatan === 2){
                    }else if(DATA_ASKEP_IGD_AWAL.sensorik.penglihatan === '3' || DATA_ASKEP_IGD_AWAL.sensorik.penglihatan === 3){
                    }else{
                        Ext.getCmp('txt_assesment_sensorik_penglihatan_lain_lain').setValue(DATA_ASKEP_IGD_AWAL.sensorik.penglihatan);
                    }

                    c.getEl().on('keypress', function (e) {
                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                        Ext.getCmp('radio_assesment_sensorik_penglihatan_normal').setValue(false);
                        Ext.getCmp('radio_assesment_sensorik_penglihatan_kabur').setValue(false);
                        Ext.getCmp('radio_assesment_sensorik_penglihatan_kaca_mata').setValue(false);
                        Ext.getCmp('radio_assesment_sensorik_penglihatan_lensa_kontak').setValue(false);
                        DATA_ASKEP_IGD_AWAL.sensorik.penglihatan = Ext.getCmp('txt_assesment_sensorik_penglihatan_lain_lain').getValue();
                        if (e.getKey() === 13) {
                        }
                    }, c);
                    c.getEl().on('change', function (e) {
                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                        tmpediting = 'true';
                    }, c);
                }
            }
        },
		{
			xtype 		: 'label',
			html 		: '<hr>',
		},
        {
            xtype       : 'radiogroup',
            columns     : 1,
            fieldLabel  : 'Penciuman',
            items       : [
                {
                    fieldLabel  : '',
                    boxLabel    : 'Normal',
                    name        : 'radio_assesment_sensorik_penciuman',
                    id          : 'radio_assesment_sensorik_penciuman_normal',
                    value       : 0,
                },{
                    fieldLabel  : '',
                    boxLabel    : 'Tidak',
                    name        : 'radio_assesment_sensorik_penciuman',
                    id          : 'radio_assesment_sensorik_penciuman_tidak',
                    value       : 1,
                },
            ],
            listeners   : {
                change   : function(a, b){
                    Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                    DATA_ASKEP_IGD_AWAL.sensorik.penciuman = b.value;
                },
                render  : function(){
                    if(DATA_ASKEP_IGD_AWAL.sensorik.penciuman == 0){
                       Ext.getCmp('radio_assesment_sensorik_penciuman_normal').setValue(true); 
                    }else{
                       Ext.getCmp('radio_assesment_sensorik_penciuman_tidak').setValue(true); 
                    }
                }
            }
        },
		{
			xtype 		: 'label',
			html 		: '<hr>',
		},
        {
            xtype       : 'checkboxgroup',
            columns     : 1,
            fieldLabel  : 'Pendengaran',
            items       : [
                {
                    fieldLabel  : '',
                    boxLabel    : 'Normal',
                    name        : 'check_assesment_sensorik_pendengaran',
                    id          : 'check_assesment_sensorik_pendengaran_normal',
                    checked     : DATA_ASKEP_IGD_AWAL.sensorik.pendengaran_normal,
                },{
                    fieldLabel  : '',
                    boxLabel    : 'Tuli Kanan',
                    name        : 'check_assesment_sensorik_pendengaran',
                    id          : 'check_assesment_sensorik_pendengaran_tuli_kanan',
                    checked     : DATA_ASKEP_IGD_AWAL.sensorik.pendengaran_tuli_kanan,
                },{
                    fieldLabel  : '',
                    boxLabel    : 'Tuli Kiri',
                    name        : 'check_assesment_sensorik_pendengaran',
                    id          : 'check_assesment_sensorik_pendengaran_tuli_kiri',
                    checked     : DATA_ASKEP_IGD_AWAL.sensorik.pendengaran_tuli_kiri,
                },{
                    fieldLabel  : '',
                    boxLabel    : 'Alat bantu Kanan',
                    name        : 'check_assesment_sensorik_pendengaran',
                    id          : 'check_assesment_sensorik_pendengaran_alat_bantu_kanan',
                    checked     : DATA_ASKEP_IGD_AWAL.sensorik.pendengaran_alat_kanan,
                },{
                    fieldLabel  : '',
                    boxLabel    : 'Alat bantu Kiri',
                    name        : 'check_assesment_sensorik_pendengaran',
                    id          : 'check_assesment_sensorik_pendengaran_alat_bantu_kiri',
                    checked     : DATA_ASKEP_IGD_AWAL.sensorik.pendengaran_alat_kiri,
                },
            ],
            listeners   : {
                change: function (checkbox, newValue, oldValue, eOpts) {
                    Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                    DATA_ASKEP_IGD_AWAL.sensorik.pendengaran_normal      = false;
                    DATA_ASKEP_IGD_AWAL.sensorik.pendengaran_tuli_kanan  = false;
                    DATA_ASKEP_IGD_AWAL.sensorik.pendengaran_tuli_kiri   = false;
                    DATA_ASKEP_IGD_AWAL.sensorik.pendengaran_alat_kanan  = false;
                    DATA_ASKEP_IGD_AWAL.sensorik.pendengaran_alat_kiri   = false;
                    for (var i = 0; i < newValue.length; i++) {
                        if (newValue[i].id == 'check_assesment_sensorik_pendengaran_normal') {
                            DATA_ASKEP_IGD_AWAL.sensorik.pendengaran_normal  = newValue[i].checked;
                        }

                        if (newValue[i].id == 'check_assesment_sensorik_pendengaran_tuli_kanan') {
                            DATA_ASKEP_IGD_AWAL.sensorik.pendengaran_tuli_kanan  = newValue[i].checked;
                        }
                        if (newValue[i].id == 'check_assesment_sensorik_pendengaran_tuli_kiri') {
                            DATA_ASKEP_IGD_AWAL.sensorik.pendengaran_tuli_kiri  = newValue[i].checked;
                        }
                        if (newValue[i].id == 'check_assesment_sensorik_pendengaran_alat_bantu_kanan') {
                            DATA_ASKEP_IGD_AWAL.sensorik.pendengaran_alat_kanan  = newValue[i].checked;
                        }
                        if (newValue[i].id == 'check_assesment_sensorik_pendengaran_alat_bantu_kiri') {
                            DATA_ASKEP_IGD_AWAL.sensorik.pendengaran_alat_kiri  = newValue[i].checked;
                        }
                    }
                },
            }
        },
		
	];
	return items;
};

function form_fungsional_motorik() { 
	var items = [
        {
            xtype       : 'radiogroup',
            fieldLabel  : 'Aktifitas',
            columns     : 1,
            items       : [
                {
                    fieldLabel  : '',
                    boxLabel    : 'Mandiri',
                    name        : 'radio_assesment_motorik_aktifitas',
                    id          : 'radio_assesment_motorik_aktifitas_mandiri',
                    value       : 0,
                },{
                    fieldLabel  : '',
                    boxLabel    : 'Bantuan minimal',
                    name        : 'radio_assesment_motorik_aktifitas',
                    id          : 'radio_assesment_motorik_aktifitas_bantuan_minimal',
                    value       : 1,
                },{
                    fieldLabel  : '',
                    boxLabel    : 'Bantuan sebagian',
                    name        : 'radio_assesment_motorik_aktifitas',
                    id          : 'radio_assesment_motorik_aktifitas_bantuan_sebagian',
                    value       : 2,
                },{
                    fieldLabel  : '',
                    boxLabel    : 'Ketergantungan total',
                    name        : 'radio_assesment_motorik_aktifitas',
                    id          : 'radio_assesment_motorik_aktifitas_bantuan_total',
                    value       : 3,
                },
            ],
            listeners   : {
                change  : function(a,b){
                    Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                    DATA_ASKEP_IGD_AWAL.motorik.aktifitas = b.value;
                },
                render : function(){
                    if(DATA_ASKEP_IGD_AWAL.motorik.aktifitas == '0' || DATA_ASKEP_IGD_AWAL.motorik.aktifitas == 0) {
                        Ext.getCmp('radio_assesment_motorik_aktifitas_mandiri').setValue(true);
                    }else if(DATA_ASKEP_IGD_AWAL.motorik.aktifitas == '1' || DATA_ASKEP_IGD_AWAL.motorik.aktifitas == 1) {
                        Ext.getCmp('radio_assesment_motorik_aktifitas_bantuan_minimal').setValue(true);
                    }else if(DATA_ASKEP_IGD_AWAL.motorik.aktifitas == '2' || DATA_ASKEP_IGD_AWAL.motorik.aktifitas == 2) {
                        Ext.getCmp('radio_assesment_motorik_aktifitas_bantuan_sebagian').setValue(true);
                    }else if(DATA_ASKEP_IGD_AWAL.motorik.aktifitas == '3' || DATA_ASKEP_IGD_AWAL.motorik.aktifitas == 3) {
                        Ext.getCmp('radio_assesment_motorik_aktifitas_bantuan_total').setValue(true);
                    }
                }
            }
        },
		{
			xtype 		: 'label',
			html 		: '<hr>',
		},
        {
            xtype       : 'radiogroup',
			fieldLabel 	: 'Berjalan',
            columns     : 1, 
            items       : [
                {
                    fieldLabel  : '',
                    boxLabel    : 'Tidak ada kesulitan',
                    name        : 'radio_assesment_motorik_berjalan',
                    id          : 'radio_assesment_motorik_berjalan_tidak_ada_kesulitan',
                    value       : 0,
                },{
                    fieldLabel  : '',
                    boxLabel    : 'Perlu bantuan',
                    name        : 'radio_assesment_motorik_berjalan',
                    id          : 'radio_assesment_motorik_berjalan_perlu_bantuan',
                    value       : 1,
                },{
                    fieldLabel  : '',
                    boxLabel    : 'Sering jatuh',
                    name        : 'radio_assesment_motorik_berjalan',
                    id          : 'radio_assesment_motorik_berjalan_sering_jatuh',
                    value       : 2,
                },{
                    fieldLabel  : '',
                    boxLabel    : 'Kelumpuhan',
                    name        : 'radio_assesment_motorik_berjalan',
                    id          : 'radio_assesment_motorik_berjalan_kelumpuhan',
                    value       : 3,
                },
            ],
            listeners   : {
                change  : function(a,b){
                    Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                    DATA_ASKEP_IGD_AWAL.motorik.berjalan = b.value;
                },
                render : function(){
                    if(DATA_ASKEP_IGD_AWAL.motorik.berjalan == '0' || DATA_ASKEP_IGD_AWAL.motorik.berjalan == 0) {
                        Ext.getCmp('radio_assesment_motorik_berjalan_tidak_ada_kesulitan').setValue(true);
                    }else if(DATA_ASKEP_IGD_AWAL.motorik.berjalan == '1' || DATA_ASKEP_IGD_AWAL.motorik.berjalan == 1) {
                        Ext.getCmp('radio_assesment_motorik_berjalan_perlu_bantuan').setValue(true);
                    }else if(DATA_ASKEP_IGD_AWAL.motorik.berjalan == '2' || DATA_ASKEP_IGD_AWAL.motorik.berjalan == 2) {
                        Ext.getCmp('radio_assesment_motorik_berjalan_sering_jatuh').setValue(true);
                    }else if(DATA_ASKEP_IGD_AWAL.motorik.berjalan == '3' || DATA_ASKEP_IGD_AWAL.motorik.berjalan == 3) {
                        Ext.getCmp('radio_assesment_motorik_berjalan_kelumpuhan').setValue(true);
                    }
                }
            }
        }
	];
	return items;
};

function form_fungsional_kognitif() { 
	var items = [
        {
            xtype       : 'radiogroup',
			fieldLabel 	: '',
            columns     : 1,
            items       : [
                {
                    fieldLabel  : '',
                    boxLabel    : 'Orientasi penuh',
                    name        : 'radio_assesment_kognitif',
                    id          : 'radio_assesment_kognitif_orientasi_penuh',
                    value       : 0,
                },{
                    fieldLabel  : '',
                    boxLabel    : 'Pelupa',
                    name        : 'radio_assesment_kognitif',
                    id          : 'radio_assesment_kognitif_pelupa',
                    value       : 1,
                },{
                    fieldLabel  : '',
                    boxLabel    : 'Bingung',
                    name        : 'radio_assesment_kognitif',
                    id          : 'radio_assesment_kognitif_bingung',
                    value       : 2,
                },{
                    fieldLabel  : '',
                    boxLabel    : 'Tidak dapat dimengerti',
                    name        : 'radio_assesment_kognitif',
                    id          : 'radio_assesment_kognitif_tidak_dapat_dimengerti',
                    value       : 3,
                },
            ],
            listeners   : {
                change  : function(a, b){
                    Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                    DATA_ASKEP_IGD_AWAL.kognitif.kognitif = b.value;
                },
                render : function(){
                    if(DATA_ASKEP_IGD_AWAL.kognitif.kognitif == '0' || DATA_ASKEP_IGD_AWAL.kognitif.kognitif == 0) {
                        Ext.getCmp('radio_assesment_kognitif_orientasi_penuh').setValue(true);
                    }else if(DATA_ASKEP_IGD_AWAL.kognitif.kognitif == '1' || DATA_ASKEP_IGD_AWAL.kognitif.kognitif == 1) {
                        Ext.getCmp('radio_assesment_kognitif_pelupa').setValue(true);
                    }else if(DATA_ASKEP_IGD_AWAL.kognitif.kognitif == '2' || DATA_ASKEP_IGD_AWAL.kognitif.kognitif == 2) {
                        Ext.getCmp('radio_assesment_kognitif_bingung').setValue(true);
                    }else if(DATA_ASKEP_IGD_AWAL.kognitif.kognitif == '3' || DATA_ASKEP_IGD_AWAL.kognitif.kognitif == 3) {
                        Ext.getCmp('radio_assesment_kognitif_tidak_dapat_dimengerti').setValue(true);
                    }
                }
            }
        }
	];
	return items;
};

function form_psikologis() { 
	var items = [
		{
			xtype 	 	: 'checkboxgroup',
			columns 	: 2,
			items 		: [
				{
					fieldLabel 	: 'Tenang',
					name 		: 'check_psikologis',
					id 			: 'check_psikologis_tenang',
					checked 	: DATA_ASKEP_IGD_AWAL.psikologis.psikologis_tenang,
				},{
					fieldLabel 	: 'Cemas',
					name 		: 'check_psikologis',
					id 			: 'check_psikologis_cemas',
					checked 	: DATA_ASKEP_IGD_AWAL.psikologis.psikologis_cemas,
				},{
					fieldLabel 	: 'Takut',
					name 		: 'check_psikologis',
					id 			: 'check_psikologis_takut',
					checked 	: DATA_ASKEP_IGD_AWAL.psikologis.psikologis_takut,
				},{
					fieldLabel 	: 'Marah',
					name 		: 'check_psikologis',
					id 			: 'check_psikologis_marah',
					checked 	: DATA_ASKEP_IGD_AWAL.psikologis.psikologis_marah,
				},{
					fieldLabel 	: 'Sedih',
					name 		: 'check_psikologis',
					id 			: 'check_psikologis_sedih',
					checked 	: DATA_ASKEP_IGD_AWAL.psikologis.psikologis_sedih,
				},{
					fieldLabel 	: 'Kecenderungan bunuh diri',
					name 		: 'check_psikologis',
					id 			: 'check_psikologis_bunuh_diri',
					checked 	: DATA_ASKEP_IGD_AWAL.psikologis.psikologis_bunuh_diri,
				},{
					fieldLabel 	: 'Lain-lain (Sebutkan)',
					name 		: 'check_psikologis',
					id 			: 'check_psikologis_lain_lain',
					checked 	: DATA_ASKEP_IGD_AWAL.psikologis.psikologis_lain,
					listeners 	: {
						check 	: function(a, isChecked){
                            Ext.getCmp('btnSimpan_viDaftarAwal').enable();
							if (isChecked === true) {
								Ext.getCmp('txt_psikologis_lain_lain').enable();
							}else{
								Ext.getCmp('txt_psikologis_lain_lain').disable();
								Ext.getCmp('txt_psikologis_lain_lain').setValue();
							}
						},
						render : function(){
							if (DATA_ASKEP_IGD_AWAL.psikologis.psikologis_lain === true) {
								Ext.getCmp('txt_psikologis_lain_lain').enable();
							}else{
								Ext.getCmp('txt_psikologis_lain_lain').disable();
								Ext.getCmp('txt_psikologis_lain_lain').setValue();
							}
						}
					}
				},{
					xtype 		: 'textfield',
					name 		: 'txt_psikologis',
					id 			: 'txt_psikologis_lain_lain',
					width 		: '100%',
					disabled 	: true,
					value 		: DATA_ASKEP_IGD_AWAL.psikologis.psikologis_lain_keterangan,
					listeners 	: {
						'render': function (c) {
							c.getEl().on('keypress', function (e) {
								if (e.getKey() === 13) {
								}
							}, c);
							c.getEl().on('change', function (e) {
								Ext.getCmp('btnSimpan_viDaftarAwal').enable();
								tmpediting 						= 'true';
								DATA_ASKEP_IGD_AWAL.psikologis.psikologis_lain_keterangan	= Ext.get('txt_psikologis_lain_lain').getValue();
							}, c);
						}
					}
				},
			],
			listeners: {
				change: function (checkbox, newValue, oldValue, eOpts) {
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
					DATA_ASKEP_IGD_AWAL.psikologis.psikologis_tenang 	= false;
					DATA_ASKEP_IGD_AWAL.psikologis.psikologis_cemas 		= false;
					DATA_ASKEP_IGD_AWAL.psikologis.psikologis_takut 		= false;
					DATA_ASKEP_IGD_AWAL.psikologis.psikologis_marah 		= false;
					DATA_ASKEP_IGD_AWAL.psikologis.psikologis_sedih 		= false;
					DATA_ASKEP_IGD_AWAL.psikologis.psikologis_bunuh_diri = false;
					DATA_ASKEP_IGD_AWAL.psikologis.psikologis_lain 		= false;

					for (var i = 0; i < newValue.length; i++) {
						if (newValue[i].id == 'check_psikologis_tenang') {
							DATA_ASKEP_IGD_AWAL.psikologis.psikologis_tenang = newValue[i].checked;
						}

						if (newValue[i].id == 'check_psikologis_cemas') {
							DATA_ASKEP_IGD_AWAL.psikologis.psikologis_cemas  = newValue[i].checked;
						}

						if (newValue[i].id == 'check_psikologis_takut') {
							DATA_ASKEP_IGD_AWAL.psikologis.psikologis_takut  = newValue[i].checked;
						}

						if (newValue[i].id == 'check_psikologis_marah') {
							DATA_ASKEP_IGD_AWAL.psikologis.psikologis_marah	 = newValue[i].checked;
						}

						if (newValue[i].id == 'check_psikologis_sedih') {
							DATA_ASKEP_IGD_AWAL.psikologis.psikologis_sedih  = newValue[i].checked;
						}

						if (newValue[i].id == 'check_psikologis_bunuh_diri') {
							DATA_ASKEP_IGD_AWAL.psikologis.psikologis_bunuh_diri = newValue[i].checked;
						}
						if (newValue[i].id == 'check_psikologis_lain_lain') {
							DATA_ASKEP_IGD_AWAL.psikologis.psikologis_lain  = newValue[i].checked;
						}
					}
				}
			}
		}
		
	];
	return items;
};

function form_status_sosial_ekonomiAwal() { 
	var items = [
		{
			xtype 		: 'label',
			text 		: 'Status pernikahan',
		},
		{
			xtype 		: 'label',
			html 		: '<hr>',
		},
		{
			xtype 	 	: 'radiogroup',
			columns 	: 1,
            id          : 'radio_status_pernikahan',
			items 		: [
				{
					boxLabel 	: 'Belum Menikah',
					name 		: 'radio_status_sosial',
					id 			: 'radio_status_sosial_belum_menikah',
                    value       : 0,
				},{
					boxLabel 	: 'Menikah',
					name 		: 'radio_status_sosial',
					id 			: 'radio_status_sosial_menikah',
                    value       : 1,
				},{
					boxLabel 	: 'Janda',
					name 		: 'radio_status_sosial',
					id 			: 'radio_status_sosial_cerai_janda',
                    value       : 2,
				},{
					boxLabel 	: 'Duda',
					name 		: 'radio_status_sosial',
					id 			: 'radio_status_sosial_cerai_duda',
                    value       : 3,
				},
			],
            listeners  : {
                render : function(){
                    if (DATA_ASKEP_IGD_AWAL.status_sosial.status_marita == 0) {
                        Ext.getCmp('radio_status_sosial_belum_menikah').setValue(true);
                    }else if(DATA_ASKEP_IGD_AWAL.status_sosial.status_marita == 1){
                        Ext.getCmp('radio_status_sosial_menikah').setValue(true);
                    }else if(DATA_ASKEP_IGD_AWAL.status_sosial.status_marita == 2){
                        Ext.getCmp('radio_status_sosial_cerai_janda').setValue(true);
                    }else if(DATA_ASKEP_IGD_AWAL.status_sosial.status_marita == 3){
                        Ext.getCmp('radio_status_sosial_cerai_duda').setValue(true);
                    }
                },
                change : function(a, b){
                    Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                    DATA_ASKEP_IGD_AWAL.status_sosial.status_marita  = b.value;
                    rowSelected_viDaftarAwal.data.STATUS_MARITA     = b.value;
                    // console.log(DATA_ASKEP_IGD_AWAL.status_sosial.status_marita);
                }
            }
		},
		{
			xtype 		: 'label',
			text 		: 'Status pekerjaan',
		},	
		{
			xtype 		: 'label',
			html 		: '<hr>',
		},{
            xtype           : 'combo',
            id              : 'cbo_pekerjaan_pengkajianIGD',
            typeAhead       : true,
            triggerAction   : 'all',
            lazyRender      : true,
            mode            : 'local',
            selectOnFocus   : true,
            forceSelection  : true,
            emptyText       : 'Pilih Pekerjaan...',
            fieldLabel      : '',
            align           : 'Right',
            tabIndex        : 16,
            value           : rowSelected_viDaftarAwal.data.PEKERJAAN,
            store           : ds_pekerjaan,
            valueField      : 'KD_PEKERJAAN',
            displayField    : 'PEKERJAAN',
            anchor          : '100%',
            listeners       : {
                render : function(){
                    Ext.getCmp('cbo_pekerjaan_pengkajianIGD').setValue(rowSelected_viDaftarAwal.data.PEKERJAAN);
                },
                select : function(a, b){
                    Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                    DATA_ASKEP_IGD_AWAL.status_sosial.kd_pekerjaan = b.data.KD_PEKERJAAN;
                    rowSelected_viDaftarAwal.data.KD_PEKERJAAN    = b.data.KD_PEKERJAAN;
                    rowSelected_viDaftarAwal.data.PEKERJAAN       = b.data.PEKERJAAN;
                }
            }
		},		
	];
	return items;
};

function get_data_store_riwayat_lab(kd_pasien, tgl_masuk,kd_unit,urut_masuk){
	Ext.Ajax.request
	({
		url: baseURL + "index.php/main/functionIGD/viewgridriwayatlab",
		params: {
			kd_pasien 	: kd_pasien,
			kd_unit 	: kd_unit, 
			tgl_masuk 	: tgl_masuk,
			urut_masuk 	: urut_masuk,
		},
		failure: function(o)
		{
			ShowPesanErrorIGD('Hubungi Admin', 'Error');
		},	
		success: function(o) 
		{   
			gridlaboratorium.data_store_hasil_lab.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) 
			{
				var recs=[],
					recType=gridlaboratorium.data_store_hasil_lab.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));
				}
				gridlaboratorium.data_store_hasil_lab.add(recs);
				gridlaboratorium.grid_store_hasil_lab.getView().refresh();
			} 
			else 
			{
				ShowPesanErrorIGD('Gagal membaca riwayat laboratorium', 'Error');
			};
		}
	});
}

function get_data_askepAwal(kd_pasien, tgl_masuk,kd_unit,urut_masuk){

    Ext.Ajax.request({
        url: baseURL + "index.php/gawat_darurat/control_askep_awal_igd/insert_askep_keperawatan",
        params: {
            kd_pasien   : kd_pasien,
            kd_unit     : kd_unit, 
            tgl_masuk   : tgl_masuk,
            urut_masuk  : urut_masuk,
        },
        failure: function(o){
            ShowPesanErrorIGD('Hubungi Admin', 'Error');
        },  
        success: function(o) {   
            var cst = Ext.decode(o.responseText);
        }
    });

    Ext.Ajax.request({
        url: baseURL + "index.php/gawat_darurat/control_askep_awal_igd/insert_tindak_invasif",
        params: {
            kd_pasien   : rowSelected_viDaftarAwal.data.KD_PASIEN,
            kd_unit     : rowSelected_viDaftarAwal.data.KD_UNIT,
            tgl_masuk   : rowSelected_viDaftarAwal.data.TANGGAL_TRANSAKSI,
            urut_masuk  : rowSelected_viDaftarAwal.data.URUT_MASUK,
            data        : JSON.stringify(gridTindakanInvasifAwal.template_data_store)
        },
        success     : function(o){
        },
        failure     : function(o){
        }
    });

	Ext.Ajax.request({
		url: baseURL + "index.php/gawat_darurat/control_askep_awal_igd/get_data_askep",
		params: {
			kd_pasien 	: kd_pasien,
			kd_unit 	: kd_unit, 
			tgl_masuk 	: tgl_masuk,
			urut_masuk 	: urut_masuk,
		},
		failure: function(o){
			ShowPesanErrorIGD('Hubungi Admin', 'Error');
		},	
		success: function(o) {   
			var cst = Ext.decode(o.responseText);
			// console.log(cst.data[0]);
			if (cst.count > 0) {
				DATA_ASKEP_IGD_AWAL.airway.paten    = convert_boolean(cst.data[0].airways_paten);
				DATA_ASKEP_IGD_AWAL.airway.snoring  = convert_boolean(cst.data[0].airways_snoring);
				DATA_ASKEP_IGD_AWAL.airway.gurgling = convert_boolean(cst.data[0].airways_gurgling);
				DATA_ASKEP_IGD_AWAL.airway.stridor  = convert_boolean(cst.data[0].airways_stridor);
				DATA_ASKEP_IGD_AWAL.airway.wheezing = convert_boolean(cst.data[0].airways_wheezing);

				DATA_ASKEP_IGD_AWAL.breathing.rr                           = cst.data[0].breathing_rr;
				DATA_ASKEP_IGD_AWAL.breathing.bantu_nafas                  = convert_boolean(cst.data[0].breathing_otot_bantu_nafas);
				DATA_ASKEP_IGD_AWAL.breathing.gerakan_dada_alkalosis       = convert_boolean(cst.data[0].breathing_gerakan_dada_alkalosis);
				DATA_ASKEP_IGD_AWAL.breathing.gerakan_dada_asidosis        = convert_boolean(cst.data[0].breathing_gerakan_dada_asidosis);
				DATA_ASKEP_IGD_AWAL.breathing.gerakan_dada_asimetris       = convert_boolean(cst.data[0].breathing_gerakan_dada_asimetris);
				DATA_ASKEP_IGD_AWAL.breathing.gerakan_dada_simetris        = convert_boolean(cst.data[0].breathing_gerakan_dada_simetris);
				DATA_ASKEP_IGD_AWAL.breathing.gerakan_dada_sao2            = convert_boolean(cst.data[0].breathing_gerakan_dada_sa02);
				DATA_ASKEP_IGD_AWAL.breathing.gerakan_dada_sao2_percentage = cst.data[0].breathing_gerakan_dada_sa02_text;


				DATA_ASKEP_IGD_AWAL.breathing.suhu_badan                   = cst.data[0].breathing_suhu_tubuh;
				DATA_ASKEP_IGD_AWAL.breathing.riwayat_demam                = cst.data[0].breathing_riwayat_demam;
				DATA_ASKEP_IGD_AWAL.breathing.riwayat_penyakit_hipertensi  = convert_boolean(cst.data[0].breathing_riwayat_penyakit_hipertensi);
				DATA_ASKEP_IGD_AWAL.breathing.riwayat_penyakit_diabetes    = convert_boolean(cst.data[0].breathing_riwayat_penyakit_diabetes);
				DATA_ASKEP_IGD_AWAL.breathing.riwayat_alergi               = convert_boolean(cst.data[0].breathing_riwayat_alergi);
				DATA_ASKEP_IGD_AWAL.breathing.riwayat_alergi_detail        = cst.data[0].breathing_riwayat_alergi_detail;

				DATA_ASKEP_IGD_AWAL.circulation.hr            = cst.data[0].circulation_hr;
				DATA_ASKEP_IGD_AWAL.circulation.teratur       = convert_boolean(cst.data[0].circulation_teratur);
				DATA_ASKEP_IGD_AWAL.circulation.tidak_teratur = convert_boolean(cst.data[0].circulation_tidak_teratur);
				DATA_ASKEP_IGD_AWAL.circulation.kuat          = convert_boolean(cst.data[0].circulation_kuat);
				DATA_ASKEP_IGD_AWAL.circulation.lemah         = convert_boolean(cst.data[0].circulation_lemah);
				DATA_ASKEP_IGD_AWAL.circulation.td            = cst.data[0].circulation_td;

				DATA_ASKEP_IGD_AWAL.ekg.irama_teratur       = convert_boolean(cst.data[0].ekg_irama_teratur);
				DATA_ASKEP_IGD_AWAL.ekg.stemi               = convert_boolean(cst.data[0].ekg_stemi);
				DATA_ASKEP_IGD_AWAL.ekg.irama_tidak_teratur = convert_boolean(cst.data[0].ekg_irama_tidak_teratur);
				DATA_ASKEP_IGD_AWAL.ekg.nstemi              = convert_boolean(cst.data[0].ekg_nstemi);

				DATA_ASKEP_IGD_AWAL.crt.lebih             	= convert_boolean(cst.data[0].crt_upper2_detik);
				DATA_ASKEP_IGD_AWAL.crt.kurang              	= convert_boolean(cst.data[0].crt_under2_detik);

				DATA_ASKEP_IGD_AWAL.akral.hangat = convert_boolean(cst.data[0].akral_hangat);
				DATA_ASKEP_IGD_AWAL.akral.dingin = convert_boolean(cst.data[0].akral_dingin);

				DATA_ASKEP_IGD_AWAL.turgor_kulit.normal = convert_boolean(cst.data[0].turgor_kulit_normal);
				DATA_ASKEP_IGD_AWAL.turgor_kulit.kurang = convert_boolean(cst.data[0].turgor_kulit_kurang);

				DATA_ASKEP_IGD_AWAL.warna_kulit.pucat    = convert_boolean(cst.data[0].warna_kulit_pucat);
				DATA_ASKEP_IGD_AWAL.warna_kulit.sianosis = convert_boolean(cst.data[0].warna_kulit_sianosis);
				DATA_ASKEP_IGD_AWAL.warna_kulit.pink     = convert_boolean(cst.data[0].warna_kulit_pink);

				DATA_ASKEP_IGD_AWAL.riwayat_cairan.diare 		= convert_boolean(cst.data[0].riwayat_cairan_diare);
				DATA_ASKEP_IGD_AWAL.riwayat_cairan.muntah 		= convert_boolean(cst.data[0].riwayat_cairan_muntah);
				DATA_ASKEP_IGD_AWAL.riwayat_cairan.luka_bakar 	= convert_boolean(cst.data[0].riwayat_cairan_lukabakar);
				DATA_ASKEP_IGD_AWAL.riwayat_cairan.perdarahan 	= convert_boolean(cst.data[0].riwayat_cairan_perdarahan);

				DATA_ASKEP_IGD_AWAL.disability._3to8   = convert_boolean(cst.data[0].gcs_3to8);
				DATA_ASKEP_IGD_AWAL.disability._9to13  = convert_boolean(cst.data[0].gcs_9to13);
				DATA_ASKEP_IGD_AWAL.disability._14to15 = convert_boolean(cst.data[0].gcs_14to15);

				DATA_ASKEP_IGD_AWAL.disability.isokor   = convert_boolean(cst.data[0].pupil_isokor);
				DATA_ASKEP_IGD_AWAL.disability.anisokor = convert_boolean(cst.data[0].pupil_anisokor);

				DATA_ASKEP_IGD_AWAL.disability._1mm 	= convert_boolean(cst.data[0].diameter_1mm);
				DATA_ASKEP_IGD_AWAL.disability._2mm 	= convert_boolean(cst.data[0].diameter_2mm);
				DATA_ASKEP_IGD_AWAL.disability._3mm 	= convert_boolean(cst.data[0].diameter_3mm);
				DATA_ASKEP_IGD_AWAL.disability._4mm 	= convert_boolean(cst.data[0].diameter_4mm);

				DATA_ASKEP_IGD_AWAL.exposure.ekskoriatum             = convert_boolean(cst.data[0].exposure_jenisluka_vulnus_ekskoriatum);
				DATA_ASKEP_IGD_AWAL.exposure.laseratum               = convert_boolean(cst.data[0].exposure_jenisluka_vulnus_laseratum);
				DATA_ASKEP_IGD_AWAL.exposure.morsum                  = convert_boolean(cst.data[0].exposure_jenisluka_vulnus_morsum);
				DATA_ASKEP_IGD_AWAL.exposure.punctum                 = convert_boolean(cst.data[0].exposure_jenisluka_vulnus_punctum);
				DATA_ASKEP_IGD_AWAL.exposure.sklopirotum             = convert_boolean(cst.data[0].exposure_jenisluka_vulnus_sklopirotum);
				DATA_ASKEP_IGD_AWAL.exposure.luka_bakar              = convert_boolean(cst.data[0].exposure_luka_bakar);
				DATA_ASKEP_IGD_AWAL.exposure.luka_bakar_luas         = cst.data[0].exposure_luka_bakar_luas;
				DATA_ASKEP_IGD_AWAL.exposure.luka_bakar_derajat      = cst.data[0].exposure_luka_bakar_derajat;
				DATA_ASKEP_IGD_AWAL.exposure.luka_bakar_luas_luka    = cst.data[0].exposure_luka_bakar_luasluka;
				DATA_ASKEP_IGD_AWAL.exposure.luka_bakar_lokasi_jejas = cst.data[0].exposure_luka_bakar_lokasi;

				DATA_ASKEP_IGD_AWAL.riwayat_kesehatan.keluhan_utama     = cst.data[0].riwayat_kesehatan_keluhan_utama;
				DATA_ASKEP_IGD_AWAL.riwayat_kesehatan.penyakit_sekarang = cst.data[0].riwayat_kesehatan_penyakit_sekarang;
				DATA_ASKEP_IGD_AWAL.riwayat_kesehatan.penyakit_dahulu   = cst.data[0].riwayat_kesehatan_penyakit_dahulu;
				
				DATA_ASKEP_IGD_AWAL.kehamilan.hpht   = cst.data[0].kehamilan_hpht;
				DATA_ASKEP_IGD_AWAL.kehamilan.g      = cst.data[0].kehamilan_g;
				DATA_ASKEP_IGD_AWAL.kehamilan.p      = cst.data[0].kehamilan_p;
				DATA_ASKEP_IGD_AWAL.kehamilan.a      = cst.data[0].kehamilan_a;
				DATA_ASKEP_IGD_AWAL.kehamilan.h      = cst.data[0].kehamilan_h;
				DATA_ASKEP_IGD_AWAL.kehamilan.minggu = cst.data[0].kehamilan_minggu;

				DATA_ASKEP_IGD_AWAL.skala_nyeri.ringan = convert_boolean(cst.data[0].nyeri_ringan);
				DATA_ASKEP_IGD_AWAL.skala_nyeri.sedang = convert_boolean(cst.data[0].nyeri_sedang);
				DATA_ASKEP_IGD_AWAL.skala_nyeri.berat  = convert_boolean(cst.data[0].nyeri_berat);
				DATA_ASKEP_IGD_AWAL.skala_nyeri.akut   = convert_boolean(cst.data[0].nyeri_akut);
				DATA_ASKEP_IGD_AWAL.skala_nyeri.kronis = convert_boolean(cst.data[0].nyeri_kronis);
				DATA_ASKEP_IGD_AWAL.skala_nyeri.lokasi = cst.data[0].nyeri_lokasi;
				DATA_ASKEP_IGD_AWAL.skala_nyeri.durasi = cst.data[0].nyeri_durasi;

				DATA_ASKEP_IGD_AWAL.resiko_jatuh.rendah = convert_boolean(cst.data[0].resiko_jatuh_rendah);
				DATA_ASKEP_IGD_AWAL.resiko_jatuh.sedang = convert_boolean(cst.data[0].resiko_jatuh_sedang);
				DATA_ASKEP_IGD_AWAL.resiko_jatuh.tinggi = convert_boolean(cst.data[0].resiko_jatuh_tinggi);

                DATA_ASKEP_IGD_AWAL.psikologis.psikologis_tenang          = convert_boolean(cst.data[0].psikologis_tenang);
                DATA_ASKEP_IGD_AWAL.psikologis.psikologis_cemas           = convert_boolean(cst.data[0].psikologis_cemas);
                DATA_ASKEP_IGD_AWAL.psikologis.psikologis_takut           = convert_boolean(cst.data[0].psikologis_takut);
                DATA_ASKEP_IGD_AWAL.psikologis.psikologis_marah           = convert_boolean(cst.data[0].psikologis_marah);
                DATA_ASKEP_IGD_AWAL.psikologis.psikologis_sedih           = convert_boolean(cst.data[0].psikologis_sedih);
                DATA_ASKEP_IGD_AWAL.psikologis.psikologis_bunuh_diri      = convert_boolean(cst.data[0].psikologis_bunuh_diri);
                DATA_ASKEP_IGD_AWAL.psikologis.psikologis_lain            = convert_boolean(cst.data[0].psikologis_lain);
                DATA_ASKEP_IGD_AWAL.psikologis.psikologis_lain_keterangan = cst.data[0].psikologis_lain_keterangan;
                DATA_ASKEP_IGD_AWAL.sensorik.penglihatan                  = cst.data[0].fungsional_sensorik_penglihatan;
                DATA_ASKEP_IGD_AWAL.sensorik.penciuman                    = convert_boolean(cst.data[0].fungsional_sensorik_penciuman);

                DATA_ASKEP_IGD_AWAL.sensorik.pendengaran_normal     = convert_boolean(cst.data[0].fungsional_sensorik_pendengaran_normal);
                DATA_ASKEP_IGD_AWAL.sensorik.pendengaran_tuli_kanan = convert_boolean(cst.data[0].fungsional_sensorik_pendengaran_tuli_kanan);
                DATA_ASKEP_IGD_AWAL.sensorik.pendengaran_tuli_kiri  = convert_boolean(cst.data[0].fungsional_sensorik_pendengaran_tuli_kiri);
                DATA_ASKEP_IGD_AWAL.sensorik.pendengaran_alat_kiri  = convert_boolean(cst.data[0].fungsional_sensorik_pendengaran_alat_kiri);
                DATA_ASKEP_IGD_AWAL.sensorik.pendengaran_alat_kanan = convert_boolean(cst.data[0].fungsional_sensorik_pendengaran_alat_kanan);

                DATA_ASKEP_IGD_AWAL.motorik.aktifitas = cst.data[0].fungsional_motorik_aktfitas;
                DATA_ASKEP_IGD_AWAL.motorik.berjalan  = cst.data[0].fungsional_motorik_berjalan;
                
                DATA_ASKEP_IGD_AWAL.kognitif.kognitif = cst.data[0].fungsional_kognitif;

                DATA_ASKEP_IGD_AWAL.tanda_vital.keadaan_umum_td    = cst.data[0].tanda_vital_keadaan_umum_td;
                DATA_ASKEP_IGD_AWAL.tanda_vital.keadaan_umum_mmhg  = cst.data[0].tanda_vital_keadaan_umum_mmhg;
                DATA_ASKEP_IGD_AWAL.tanda_vital.keadaan_umum_suhu  = cst.data[0].tanda_vital_keadaan_umum_suhu;
                DATA_ASKEP_IGD_AWAL.tanda_vital.keadaan_umum_nadi  = cst.data[0].tanda_vital_keadaan_umum_nadi;
                DATA_ASKEP_IGD_AWAL.tanda_vital.keadaan_umum_nafas = cst.data[0].tanda_vital_keadaan_umum_nafas;
                DATA_ASKEP_IGD_AWAL.tanda_vital.keadaan_umum_sao2  = cst.data[0].tanda_vital_keadaan_umum_sao2;
                DATA_ASKEP_IGD_AWAL.tanda_vital.imunisasi = cst.data[0].tanda_vital_imunisasi;

                DATA_ASKEP_IGD_AWAL.tanda_vital.alergi_obat        = convert_boolean(cst.data[0].tanda_vital_alergi_obat);
                DATA_ASKEP_IGD_AWAL.tanda_vital.alergi_makanan     = convert_boolean(cst.data[0].tanda_vital_alergi_makanan);
                DATA_ASKEP_IGD_AWAL.tanda_vital.alergi_lain_lain   = convert_boolean(cst.data[0].tanda_vital_alergi_lain_lain);
                DATA_ASKEP_IGD_AWAL.tanda_vital.diagnosa_kerja     = cst.data[0].tanda_vital_diagnosa_kerja;

                DATA_ASKEP_IGD_AWAL.lanjutan.resusitasi = convert_boolean(cst.data[0].lanjutan_resusitasi);
                DATA_ASKEP_IGD_AWAL.lanjutan.anak       = convert_boolean(cst.data[0].lanjutan_anak);
                DATA_ASKEP_IGD_AWAL.lanjutan.obgin      = convert_boolean(cst.data[0].lanjutan_obgin);
                DATA_ASKEP_IGD_AWAL.lanjutan.medikal    = convert_boolean(cst.data[0].lanjutan_medikal);
                DATA_ASKEP_IGD_AWAL.lanjutan.surgikal   = convert_boolean(cst.data[0].lanjutan_surgikal);

                DATA_ASKEP_IGD_AWAL.pemeriksaan.ku      = cst.data[0].pemeriksaan_ku;
                DATA_ASKEP_IGD_AWAL.pemeriksaan.gcs     = cst.data[0].pemeriksaan_gcs;
                DATA_ASKEP_IGD_AWAL.pemeriksaan.e       = cst.data[0].pemeriksaan_e;
                DATA_ASKEP_IGD_AWAL.pemeriksaan.m       = cst.data[0].pemeriksaan_m;
                DATA_ASKEP_IGD_AWAL.pemeriksaan.v       = cst.data[0].pemeriksaan_v;
                DATA_ASKEP_IGD_AWAL.pemeriksaan.catatan = cst.data[0].pemeriksaan_catatan;

                DATA_ASKEP_IGD_AWAL.pemeriksaan.kesadaran_cm       = convert_boolean(cst.data[0].pemeriksaan_kesadaran_cm);
                DATA_ASKEP_IGD_AWAL.pemeriksaan.kesadaran_apatis   = convert_boolean(cst.data[0].pemeriksaan_kesadaran_apatis);
                DATA_ASKEP_IGD_AWAL.pemeriksaan.kesadaran_somnolen = convert_boolean(cst.data[0].pemeriksaan_kesadaran_somnolen);
                DATA_ASKEP_IGD_AWAL.pemeriksaan.kesadaran_sopor    = convert_boolean(cst.data[0].pemeriksaan_kesadaran_sopor);
                DATA_ASKEP_IGD_AWAL.pemeriksaan.kesadaran_koma     = convert_boolean(cst.data[0].pemeriksaan_kesadaran_koma);

                DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_1           = convert_boolean(cst.data[0].kekuatan_1);
                DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_1_text      = cst.data[0].kekuatan_1_text;
                DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_2           = convert_boolean(cst.data[0].kekuatan_2);
                DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_2_text      = cst.data[0].kekuatan_2_text;
                DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_3           = convert_boolean(cst.data[0].kekuatan_3);
                DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_3_text      = cst.data[0].kekuatan_3_text;
                DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_4           = convert_boolean(cst.data[0].kekuatan_4);
                DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_4_text      = cst.data[0].kekuatan_4_text;
                DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_5           = convert_boolean(cst.data[0].kekuatan_5);
                DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_5_text      = cst.data[0].kekuatan_5_text;

                DATA_ASKEP_IGD_AWAL.tindak_lanjut.keadaan          = cst.data[0].tindak_lanjut_keadaan;
                DATA_ASKEP_IGD_AWAL.tindak_lanjut.dokter_pemeriksa = rowSelected_viDaftarAwal.data.NAMA_DOKTER;
			
                Ext.Ajax.request({
                    url: baseURL + "index.php/gawat_darurat/control_askep_awal_igd/get_data_dokter",
                    params: {
                        text: DATA_ASKEP_IGD_AWAL.tindak_lanjut.dokter_pemeriksa,
                    },
                    success: function (o){
                        var cst = Ext.decode(o.responseText);
                        if (cst.processResult == 'SUCCESS') {
                            DATA_ASKEP_IGD_AWAL.tindak_lanjut.dokter_pemeriksa_label = cst.data[0].nama;
                        }
                    },
                    failure: function (o){

                    }
                });

                DATA_ASKEP_IGD_AWAL.tindak_lanjut.td               = cst.data[0].tindak_lanjut_td;
                DATA_ASKEP_IGD_AWAL.tindak_lanjut.nadi             = cst.data[0].tindak_lanjut_nadi;
                DATA_ASKEP_IGD_AWAL.tindak_lanjut.suhu             = cst.data[0].tindak_lanjut_suhu;
                DATA_ASKEP_IGD_AWAL.tindak_lanjut.pernapasan       = cst.data[0].tindak_lanjut_pernapasan;

                DATA_ASKEP_IGD_AWAL.tindak_lanjut.tindak_lanjut 	= cst.data[0].tindak_lanjut;
                DATA_ASKEP_IGD_AWAL.tindak_lanjut.konsultasi    	= cst.data[0].tindak_lanjut_konsultasi;
                DATA_ASKEP_IGD_AWAL.tindak_lanjut.konsultasi_1    	= cst.data[0].tindak_lanjut_konsultasi_1;
                DATA_ASKEP_IGD_AWAL.tindak_lanjut.konsultasi_2    	= cst.data[0].tindak_lanjut_konsultasi_2;
                DATA_ASKEP_IGD_AWAL.tindak_lanjut.konsultasi_3   	= cst.data[0].tindak_lanjut_konsultasi_3;
                DATA_ASKEP_IGD_AWAL.tindak_lanjut.konsultasi_4    	= cst.data[0].tindak_lanjut_konsultasi_4;
                DATA_ASKEP_IGD_AWAL.tindak_lanjut.telepon       	= convert_boolean(cst.data[0].tindak_lanjut_telepon);
                DATA_ASKEP_IGD_AWAL.tindak_lanjut.on_site       	= convert_boolean(cst.data[0].tindak_lanjut_on_site);
				if (cst.data[0].tindak_lanjut_waktu_plg != null) {
                    var tmp_tgl_plg = new Date(cst.data[0].tindak_lanjut_waktu_plg);
					DATA_ASKEP_IGD_AWAL.tindak_lanjut.jam_pulang    	= tmp_tgl_plg.format('H:i:s');
					DATA_ASKEP_IGD_AWAL.tindak_lanjut.tgl_pulang    	= tmp_tgl_plg.format('Y-m-d');
                }

                DATA_ASKEP_IGD_AWAL.tindak_lanjut.atas_persetujuan   = convert_boolean(cst.data[0].tindak_lanjut_atas_persetujuan);
                DATA_ASKEP_IGD_AWAL.tindak_lanjut.persetujuan_diri   = convert_boolean(cst.data[0].tindak_lanjut_persetujuan_diri);
                DATA_ASKEP_IGD_AWAL.tindak_lanjut.dead   			 = convert_boolean(cst.data[0].tindak_lanjut_dead); //2019-04-18
                DATA_ASKEP_IGD_AWAL.tindak_lanjut.kontrol            = convert_boolean(cst.data[0].tindak_lanjut_kontrol);
                DATA_ASKEP_IGD_AWAL.tindak_lanjut.tanggal            = cst.data[0].tindak_lanjut_tanggal;
                DATA_ASKEP_IGD_AWAL.tindak_lanjut.terapi_pulang      = cst.data[0].tindak_lanjut_terapi_pulang;
                DATA_ASKEP_IGD_AWAL.tindak_lanjut.edukasi_pasien     = convert_boolean(cst.data[0].tindak_lanjut_edukasi_pasien);
                DATA_ASKEP_IGD_AWAL.tindak_lanjut.edukasi_keluarga   = convert_boolean(cst.data[0].tindak_lanjut_edukasi_keluarga);
                DATA_ASKEP_IGD_AWAL.tindak_lanjut.edukasi_tidak      = convert_boolean(cst.data[0].tindak_lanjut_edukasi_tidak);
                DATA_ASKEP_IGD_AWAL.tindak_lanjut.karena             = cst.data[0].tindak_lanjut_karena;

                DATA_ASKEP_IGD_AWAL.tindak_lanjut.dirujuk_ke       = cst.data[0].tindak_lanjut_dirujuk_ke;
                DATA_ASKEP_IGD_AWAL.tindak_lanjut.alasan_dirujuk   = cst.data[0].tindak_lanjut_alasan_dirujuk;

                DATA_ASKEP_IGD_AWAL.trease.kd_trease               = cst.data[0].kd_triase;
				/* if(DATA_ASKEP_IGD_AWAL.trease.kd_trease == 6 || DATA_ASKEP_IGD_AWAL.trease.kd_trease =='6'){
					DATA_ASKEP_IGD_AWAL.trease.kd_trease ='DOA';
				} */
	
				DATA_ASKEP_IGD_AWAL.trease.jalan_nafas_ancaman 				= convert_boolean(cst.data[0].trease_jalan_nafas_ancaman);
				DATA_ASKEP_IGD_AWAL.trease.jalan_nafas_sumbatan 			= convert_boolean(cst.data[0].trease_jalan_nafas_sumbatan);
				DATA_ASKEP_IGD_AWAL.trease.jalan_nafas_bebas 				= convert_boolean(cst.data[0].trease_jalan_nafas_bebas);

				DATA_ASKEP_IGD_AWAL.trease.pernafasan_henti_nafas 			 = convert_boolean(cst.data[0].trease_pernafasan_henti_nafas);
				DATA_ASKEP_IGD_AWAL.trease.pernafasan_bradipnoe              = convert_boolean(cst.data[0].trease_pernafasan_bradipnoe);
				DATA_ASKEP_IGD_AWAL.trease.pernafasan_sianosis               = convert_boolean(cst.data[0].trease_pernafasan_sianosis);
				DATA_ASKEP_IGD_AWAL.trease.pernafasan_takipnoe               = convert_boolean(cst.data[0].trease_pernafasan_takipnoe);
				DATA_ASKEP_IGD_AWAL.trease.pernafasan_mengi                  = convert_boolean(cst.data[0].trease_pernafasan_mengi);
				DATA_ASKEP_IGD_AWAL.trease.pernafasan_normal                 = convert_boolean(cst.data[0].trease_pernafasan_normal);
				DATA_ASKEP_IGD_AWAL.trease.pernafasan_frekuensi_nafas_normal = convert_boolean(cst.data[0].trease_pernafasan_frekuensi_nafas_normal);

				DATA_ASKEP_IGD_AWAL.trease.sirkulasi_henti_jantung           = convert_boolean(cst.data[0].trease_sirkulasi_henti_jantung);
				DATA_ASKEP_IGD_AWAL.trease.sirkulasi_nadi_tidak_teraba       = convert_boolean(cst.data[0].trease_sirkulasi_nadi_tidak_teraba);
				DATA_ASKEP_IGD_AWAL.trease.sirkulasi_nadi_teraba_lemah       = convert_boolean(cst.data[0].trease_sirkulasi_nadi_teraba_lemah);
				DATA_ASKEP_IGD_AWAL.trease.sirkulasi_nadi_kuat               = convert_boolean(cst.data[0].trease_sirkulasi_nadi_kuat);
				DATA_ASKEP_IGD_AWAL.trease.sirkulasi_akral_dingin            = convert_boolean(cst.data[0].trease_sirkulasi_akral_dingin);
				DATA_ASKEP_IGD_AWAL.trease.sirkulasi_brakikardi              = convert_boolean(cst.data[0].trease_sirkulasi_brakikardi);
				DATA_ASKEP_IGD_AWAL.trease.sirkulasi_takikardi               = convert_boolean(cst.data[0].trease_sirkulasi_takikardi);
				DATA_ASKEP_IGD_AWAL.trease.sirkulasi_pucat                   = convert_boolean(cst.data[0].trease_sirkulasi_pucat);
				DATA_ASKEP_IGD_AWAL.trease.sirkulasi_tds_lebih_160           = convert_boolean(cst.data[0].trease_sirkulasi_tds_lebih_160);
				DATA_ASKEP_IGD_AWAL.trease.sirkulasi_tds_lebih_100           = convert_boolean(cst.data[0].trease_sirkulasi_tdd_lebih_100);
				DATA_ASKEP_IGD_AWAL.trease.sirkulasi_frekuensi_nadi_normal   = convert_boolean(cst.data[0].trease_sirkulasi_frekuensi_nadi_normal);
				DATA_ASKEP_IGD_AWAL.trease.sirkulasi_tdd_140_to_180          = convert_boolean(cst.data[0].trease_sirkulasi_tds_140_to_180);
				DATA_ASKEP_IGD_AWAL.trease.sirkulasi_tdd_90_to_100           = convert_boolean(cst.data[0].trease_sirkulasi_tdd_90_to_100);
				DATA_ASKEP_IGD_AWAL.trease.sirkulasi_td_normal               = convert_boolean(cst.data[0].trease_sirkulasi_td_normal);

				DATA_ASKEP_IGD_AWAL.trease.kesadaran_gcs_kurang_sama_8       = convert_boolean(cst.data[0].trease_kesadaran_gcs_kurang_sama_8);
				DATA_ASKEP_IGD_AWAL.trease.kesadaran_gcs_9_sampai_12         = convert_boolean(cst.data[0].trease_kesadaran_gcs_9_sampai_12);
				DATA_ASKEP_IGD_AWAL.trease.kesadaran_gcs_lebih_12            = convert_boolean(cst.data[0].trease_kesadaran_gcs_lebih_12);
				DATA_ASKEP_IGD_AWAL.trease.kesadaran_gcs_15                  = convert_boolean(cst.data[0].trease_kesadaran_gcs_15);
				DATA_ASKEP_IGD_AWAL.trease.kesadaran_kejang                  = convert_boolean(cst.data[0].trease_kesadaran_kejang);
				DATA_ASKEP_IGD_AWAL.trease.kesadaran_tidak_ada_respon        = convert_boolean(cst.data[0].trease_kesadaran_tidak_ada_respon);
				DATA_ASKEP_IGD_AWAL.trease.kesadaran_gelisah               	= convert_boolean(cst.data[0].trease_kesadaran_gelisah);
				DATA_ASKEP_IGD_AWAL.trease.kesadaran_hemiparese              = convert_boolean(cst.data[0].trease_kesadaran_hemiparese);
				DATA_ASKEP_IGD_AWAL.trease.kesadaran_nyeri_dada              = convert_boolean(cst.data[0].trease_kesadaran_nyeri_dada);
				DATA_ASKEP_IGD_AWAL.trease.kesadaran_apatis                  = convert_boolean(cst.data[0].trease_kesadaran_apatis);
				DATA_ASKEP_IGD_AWAL.trease.kesadaran_somnolen                = convert_boolean(cst.data[0].trease_kesadaran_somnolen);

				DATA_ASKEP_IGD_AWAL.serah_terima.dokter_jaga       = cst.data[0].serah_terima_dokter_jaga;
				DATA_ASKEP_IGD_AWAL.serah_terima.perawat_trease    = cst.data[0].serah_terima_perawat_trease;

                DATA_ASKEP_IGD_AWAL.gizi.gizi_skor_penurunan_bb  = cst.data[0].gizi_skor_penurunan_bb;
                DATA_ASKEP_IGD_AWAL.gizi.gizi_skor_sulit_makan   = cst.data[0].gizi_skor_sulit_makan;
                DATA_ASKEP_IGD_AWAL.gizi.gizi_khusus             = cst.data[0].gizi_skor_penyakit_khusus;

                DATA_ASKEP_IGD_AWAL.pemeriksaan.diagnosa         = cst.data[0].pemeriksaan_diagnosa;

                if (cst.data[0].waktu_datang != null) {
                    var tgl_datang = new Date(cst.data[0].waktu_datang);
                    DATA_ASKEP_IGD_AWAL.respontime.waktu_datang_tgl = tgl_datang.format('Y-m-d');
                    DATA_ASKEP_IGD_AWAL.respontime.waktu_datang_jam = tgl_datang.format('H:i:s');
                }

                if (cst.data[0].waktu_ditindak != null) {
                    var tgl_datang = new Date(cst.data[0].waktu_ditindak);
                    DATA_ASKEP_IGD_AWAL.respontime.waktu_periksa_tgl = tgl_datang.format('Y-m-d');
                    DATA_ASKEP_IGD_AWAL.respontime.waktu_periksa_jam = tgl_datang.format('H:i:s');
                }
				// DISINI
			}
			HasilPengkajianLookUpAwal(rowSelected_viDaftarAwal.data);


            
            gridTataLaksanaAwal.data_store.load({
                params:
                {
                    Skip: 0,
                    Take: 1000,
                    Sort: 'tindakan',
                    Sortdir: 'ASC',
                    target: 'TataLaksanaAskepIGD',
                    param: 'kd_pasien = ~'+rowSelected_viDaftarAwal.data.KD_PASIEN+'~ AND kd_unit = ~'+rowSelected_viDaftarAwal.data.KD_UNIT+'~ AND tgl_masuk = ~'+rowSelected_viDaftarAwal.data.TANGGAL_TRANSAKSI+'~ AND urut_masuk = ~'+rowSelected_viDaftarAwal.data.URUT_MASUK+'~'
                }
            });
            Ext.Ajax.request({
                url: baseURL + "index.php/gawat_darurat/control_askep_awal_igd/get_grid_tata_laksana",
                params: {
                    kd_pasien   : rowSelected_viDaftarAwal.data.KD_PASIEN,
                    kd_unit     : rowSelected_viDaftarAwal.data.KD_UNIT,
                    tgl_masuk   : rowSelected_viDaftarAwal.data.TANGGAL_TRANSAKSI,
                    urut_masuk  : rowSelected_viDaftarAwal.data.URUT_MASUK,
                },
                success     : function(o){
                    var cst = Ext.decode(o.responseText);
                    if (cst.status === true) {
                        var same;
                        for (var i = 0; i < cst.data.length; i++) {
                            if (gridTataLaksanaAwal.data_store.data.length > 0) {
                                for (var z = 0; z < gridTataLaksanaAwal.data_store.data.length; z++) {
                                    if(gridTataLaksanaAwal.data_store.data.items[z].data.TINDAKAN == cst.data[i].nama_obat){
                                        same = true;
                                        break;
                                    }else{
                                        same = false;
                                    }
                                }
                            }else{
                                same = false;
                            }

                            if (same === false) {
                                var p = new gridTataLaksanaAwal.map_record({
                                    'WAKTU' : cst.data[i].waktu,
                                    'TINDAKAN' : cst.data[i].nama_obat,
                                    'DOSIS' : cst.data[i].dosis,
                                    'CARA_PEMBERIAN' : cst.data[i].cara_pemberian,
                                    'WAKTU_PEMBERIAN' : cst.data[i].waktu_pemberian,
                                    'TTD_DOKTER' : '',
                                    'TTD_PERAWAT' : '',
                                });
                                gridTataLaksanaAwal.data_store.insert(gridTataLaksanaAwal.data_store.getCount(), p);
                                console.log(gridTataLaksanaAwal.grid_store_hasil);
                            }
                        }
                    }
                },
                failure     : function(o){
                }
            });

            gridPemeriksaanPenunjangAwal.data_store.load({
                params:
                {
                    Skip: 0,
                    Take: 1000,
                    Sort: 'pemeriksaan',
                    Sortdir: 'ASC',
                    target: 'PemeriksaanPenunjangAskepIGD',
                    param: 'kd_pasien = ~'+rowSelected_viDaftarAwal.data.KD_PASIEN+'~ AND kd_unit = ~'+rowSelected_viDaftarAwal.data.KD_UNIT+'~ AND tgl_masuk = ~'+rowSelected_viDaftarAwal.data.TANGGAL_TRANSAKSI+'~ AND urut_masuk = ~'+rowSelected_viDaftarAwal.data.URUT_MASUK+'~'
                }
            });

            gridTandaVitalAwal.data_store.load({
                params:
                {
                    Skip: 0,
                    Take: 1000,
                    Sort: 'waktu',
                    Sortdir: 'ASC',
                    target: 'TandaVitalAskepIGD',
                    param: 'kd_pasien = ~'+rowSelected_viDaftarAwal.data.KD_PASIEN+'~ AND kd_unit = ~'+rowSelected_viDaftarAwal.data.KD_UNIT+'~ AND tgl_masuk = ~'+rowSelected_viDaftarAwal.data.TANGGAL_TRANSAKSI+'~ AND urut_masuk = ~'+rowSelected_viDaftarAwal.data.URUT_MASUK+'~'
                }
            });


            gridTindakanInvasifAwal.data_store.load({
                params:
                {
                    Skip: 0,
                    Take: 0,
                    Sort: 'urut',
                    Sortdir: '',
                    target: 'TindakInvasifAskepIGD',
                    param: 'kd_pasien = ~'+rowSelected_viDaftarAwal.data.KD_PASIEN+'~ AND kd_unit = ~'+rowSelected_viDaftarAwal.data.KD_UNIT+'~ AND tgl_masuk = ~'+rowSelected_viDaftarAwal.data.TANGGAL_TRANSAKSI+'~ AND urut_masuk = ~'+rowSelected_viDaftarAwal.data.URUT_MASUK+'~'
                }
            });
		}
	});
}

function get_data_store_riwayat_rad(kd_pasien, tgl_masuk,kd_unit,urut_masuk){
	Ext.Ajax.request
	({
		url: baseURL + "index.php/main/functionIGD/viewgridriwayatrad",
		params: {
			kd_pasien 	: kd_pasien,
			kd_unit 	: kd_unit, 
			tgl_masuk 	: tgl_masuk,
			urut_masuk 	: urut_masuk,
		},
		failure: function(o)
		{
			ShowPesanErrorIGD('Hubungi Admin', 'Error');
		},	
		success: function(o) 
		{   
			gridRadiologi.data_store_hasil.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) 
			{
				var recs=[],
					recType=gridRadiologi.data_store_hasil.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));
				}
				gridRadiologi.data_store_hasil.add(recs);
				gridRadiologi.grid_store_hasil.getView().refresh();
			} 
			else 
			{
				ShowPesanErrorIGD('Gagal membaca riwayat laboratorium', 'Error');
			};
		}
	});
}

function form_kekuatan_otot() { 
	var items = [
				{
					boxLabel	: '1',
					name		: 'checked_kekuatan_otot_1',
					id			: 'checked_kekuatan_otot_1',
                    checked     : DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_1,
                    listeners   : {
                        check: function (checkbox, isChecked) {
                            Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                            tmpediting = 'true';
                            DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_1 = isChecked;
                            if (isChecked === true) {
                                Ext.getCmp('txt_kekuatan_otot_1').enable();
                            }else{
                                Ext.getCmp('txt_kekuatan_otot_1').disable();
                                Ext.getCmp('txt_kekuatan_otot_1').setValue();
                                DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_1_text = "";
                            }
                        },
                        render  : function(){
                            if (DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_1 === true) {
                                Ext.getCmp('txt_kekuatan_otot_1').enable();
                            }else{
                                Ext.getCmp('txt_kekuatan_otot_1').disable();
                            }
                        }
                    }
                },{
                    xtype       : 'textfield',
                    name        : 'txt_kekuatan_otot_1',
                    id          : 'txt_kekuatan_otot_1',
                    width       : '100%',
                    disabled    : true,
                    value       : DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_1_text,
                    listeners   : {
                        'render': function (c) {
                            c.getEl().on('keypress', function (e) {
                                Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                tmpediting                      = 'true';
                            }, c);
                            c.getEl().on('change', function (e) {
                                DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_1_text  = Ext.get('txt_kekuatan_otot_1').getValue();
                            }, c);
                        }
                    }
                },
                {
                    boxLabel    : '2',
                    name        : 'checked_kekuatan_otot_2',
                    id          : 'checked_kekuatan_otot_2',
                    checked     : DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_2,
                    listeners   : {
                        check: function (checkbox, isChecked) {
                            Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                            tmpediting = 'true';
                            DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_2 = isChecked;
                            if (isChecked === true) {
                                Ext.getCmp('txt_kekuatan_otot_2').enable();
                            }else{
                                Ext.getCmp('txt_kekuatan_otot_2').disable();
                                Ext.getCmp('txt_kekuatan_otot_2').setValue();
                                DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_2_text = "";
                            }
                        },
                        render  : function(){
                            if (DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_2 === true) {
                                Ext.getCmp('txt_kekuatan_otot_2').enable();
                            }else{
                                Ext.getCmp('txt_kekuatan_otot_2').disable();
                            }
                        }
                    }
                },{
                    xtype       : 'textfield',
                    name        : 'txt_kekuatan_otot_2',
                    id          : 'txt_kekuatan_otot_2',
                    width       : '100%',
                    disabled    : true,
                    value       : DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_2_text,
                    listeners   : {
                        'render': function (c) {
                            c.getEl().on('keypress', function (e) {
                                Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                tmpediting                      = 'true';
                            }, c);
                            c.getEl().on('change', function (e) {
                                DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_2_text  = Ext.get('txt_kekuatan_otot_2').getValue();
                            }, c);
                        }
                    }
                },
                {
                    boxLabel    : '3',
                    name        : 'checked_kekuatan_otot_3',
                    id          : 'checked_kekuatan_otot_3',
                    checked     : DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_3,
                    listeners   : {
                        check: function (checkbox, isChecked) {
                            Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                            tmpediting = 'true';
                            DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_3 = isChecked;
                            if (isChecked === true) {
                                Ext.getCmp('txt_kekuatan_otot_3').enable();
                            }else{
                                Ext.getCmp('txt_kekuatan_otot_3').disable();
                                Ext.getCmp('txt_kekuatan_otot_3').setValue();
                                DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_3_text = "";
                            }
                        },
                        render  : function(){
                            if (DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_3 === true) {
                                Ext.getCmp('txt_kekuatan_otot_3').enable();
                            }else{
                                Ext.getCmp('txt_kekuatan_otot_3').disable();
                            }
                        }
                    }
                },{
                    xtype       : 'textfield',
                    name        : 'txt_kekuatan_otot_3',
                    id          : 'txt_kekuatan_otot_3',
                    width       : '100%',
                    disabled    : true,
                    value       : DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_3_text,
                    listeners   : {
                        'render': function (c) {
                            c.getEl().on('keypress', function (e) {
                                Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                tmpediting                      = 'true';
                            }, c);
                            c.getEl().on('change', function (e) {
                                DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_3_text  = Ext.get('txt_kekuatan_otot_3').getValue();
                            }, c);
                        }
                    }
                },
                {
                    boxLabel    : '4',
                    name        : 'checked_kekuatan_otot_4',
                    id          : 'checked_kekuatan_otot_4',
                    checked     : DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_4,
                    listeners   : {
                        check: function (checkbox, isChecked) {
                            Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                            tmpediting = 'true';
                            DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_4 = isChecked;
                            if (isChecked === true) {
                                Ext.getCmp('txt_kekuatan_otot_4').enable();
                            }else{
                                Ext.getCmp('txt_kekuatan_otot_4').disable();
                                Ext.getCmp('txt_kekuatan_otot_4').setValue();
                                DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_4_text = "";
                            }
                        },
                        render  : function(){
                            if (DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_4 === true) {
                                Ext.getCmp('txt_kekuatan_otot_4').enable();
                            }else{
                                Ext.getCmp('txt_kekuatan_otot_4').disable();
                            }
                        }
                    }
                },{
                    xtype       : 'textfield',
                    name        : 'txt_kekuatan_otot_4',
                    id          : 'txt_kekuatan_otot_4',
                    width       : '100%',
                    disabled    : true,
                    value       : DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_4_text,
                    listeners   : {
                        'render': function (c) {
                            c.getEl().on('keypress', function (e) {
                                Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                tmpediting                      = 'true';
                            }, c);
                            c.getEl().on('change', function (e) {
                                DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_4_text  = Ext.get('txt_kekuatan_otot_4').getValue();
                            }, c);
                        }
                    }
                },
                {
                    boxLabel    : '5',
                    name        : 'checked_kekuatan_otot_5',
                    id          : 'checked_kekuatan_otot_5',
                    checked     : DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_5,
                    listeners   : {
                        check: function (checkbox, isChecked) {
                            Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                            tmpediting = 'true';
                            DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_5 = isChecked;
                            if (isChecked === true) {
                                Ext.getCmp('txt_kekuatan_otot_5').enable();
                            }else{
                                Ext.getCmp('txt_kekuatan_otot_5').disable();
                                Ext.getCmp('txt_kekuatan_otot_5').setValue();
                                DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_5_text = "";
                            }
                        },
                        render  : function(){
                            if (DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_5 === true) {
                                Ext.getCmp('txt_kekuatan_otot_5').enable();
                            }else{
                                Ext.getCmp('txt_kekuatan_otot_5').disable();
                            }
                        }
                    }
                },{
                    xtype       : 'textfield',
                    name        : 'txt_kekuatan_otot_5',
                    id          : 'txt_kekuatan_otot_5',
                    width       : '100%',
                    disabled    : true,
                    value       : DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_5_text,
                    listeners   : {
                        'render': function (c) {
                            c.getEl().on('keypress', function (e) {
                                Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                tmpediting                      = 'true';
                            }, c);
                            c.getEl().on('change', function (e) {
                                DATA_ASKEP_IGD_AWAL.kekuatan_otot.otot_5_text  = Ext.get('txt_kekuatan_otot_5').getValue();
                            }, c);
                        }
                    }
                },
            // ]
        // }
    ];
    return items;
};

function TabMainPanelPengkajianAwal() {
    var items = {
        xtype: 'tabpanel',
		id:'idMasterTab',
        bodyStyle : 'padding: 10px 10px 10px 10px;',
        activeTab: 0,
        flex:1,
		listeners: {
			tabchange: function (tab) {
                // DISINI
                // gridPemeriksaanPenunjangAwal.data_store.removeAll();
                gridGizi.data_store_hasil.loadData(gridGizi.data_store);
                if (tab.activeTab.title == "Riwayat Kesehatan") {
                    ds_pekerjaan.load({
                        params:
                        {
                            Skip: 0,
                            Take: 1000,
                            Sort: 'pekerjaan',
                            Sortdir: 'ASC',
                            target: 'ViewComboPekerjaan',
                            param: ''
                        }
                    });
                }
                if(tab.activeTab.title == "Trease"){
                    gridTreaseAwal.all_hidden();
                    gridTreaseAwal.validasi_form(DATA_ASKEP_IGD_AWAL.trease.kd_trease);
                }
                if (tab.activeTab.title == 'Pemeriksaan Fisik dan Penunjang') {
                    if (tmpediting != 'true') {
                        gridPemeriksaanPenunjangAwal.data_store.load({
                            params:
                            {
                                Skip: 0,
                                Take: 1000,
                                Sort: 'pemeriksaan',
                                Sortdir: 'ASC',
                                target: 'PemeriksaanPenunjangAskepIGD',
                                param: 'kd_pasien = ~'+rowSelected_viDaftarAwal.data.KD_PASIEN+'~ AND kd_unit = ~'+rowSelected_viDaftarAwal.data.KD_UNIT+'~ AND tgl_masuk = ~'+rowSelected_viDaftarAwal.data.TANGGAL_TRANSAKSI+'~ AND urut_masuk = ~'+rowSelected_viDaftarAwal.data.URUT_MASUK+'~'
                            }
                        });
                    }
                }
                if (tab.activeTab.title == 'Tata Laksana') {
                    if (tmpediting != 'true') {
                        gridTataLaksanaAwal.data_store.load({
                            params:
                            {
                                Skip: 0,
                                Take: 1000,
                                Sort: 'tindakan',
                                Sortdir: 'ASC',
                                target: 'TataLaksanaAskepIGD',
                                param: 'kd_pasien = ~'+rowSelected_viDaftarAwal.data.KD_PASIEN+'~ AND kd_unit = ~'+rowSelected_viDaftarAwal.data.KD_UNIT+'~ AND tgl_masuk = ~'+rowSelected_viDaftarAwal.data.TANGGAL_TRANSAKSI+'~ AND urut_masuk = ~'+rowSelected_viDaftarAwal.data.URUT_MASUK+'~'
                            }
                        });
                        Ext.Ajax.request({
                            url: baseURL + "index.php/gawat_darurat/control_askep_awal_igd/get_grid_tata_laksana",
                            params: {
                                kd_pasien   : rowSelected_viDaftarAwal.data.KD_PASIEN,
                                kd_unit     : rowSelected_viDaftarAwal.data.KD_UNIT,
                                tgl_masuk   : rowSelected_viDaftarAwal.data.TANGGAL_TRANSAKSI,
                                urut_masuk  : rowSelected_viDaftarAwal.data.URUT_MASUK,
                            },
                            success     : function(o){
                                var cst = Ext.decode(o.responseText);
                                if (cst.status === true) {
                                    var same;
                                    for (var i = 0; i < cst.data.length; i++) {
                                        if (gridTataLaksanaAwal.data_store.data.length > 0) {
                                            for (var z = 0; z < gridTataLaksanaAwal.data_store.data.length; z++) {
                                                if(gridTataLaksanaAwal.data_store.data.items[z].data.TINDAKAN == cst.data[i].nama_obat){
                                                    same = true;
                                                    break;
                                                }else{
                                                    same = false;
                                                }
                                            }
                                        }else{
                                            same = false;
                                        }

                                        if (same === false) {
                                            var p = new gridTataLaksanaAwal.map_record({
                                                'WAKTU' : cst.data[i].waktu,
                                                'TINDAKAN' : cst.data[i].nama_obat,
                                                'DOSIS' : cst.data[i].dosis,
                                                'CARA_PEMBERIAN' : cst.data[i].cara_pemberian,
                                                'WAKTU_PEMBERIAN' : cst.data[i].waktu_pemberian,
                                                'TTD_DOKTER' : '',
                                                'TTD_PERAWAT' : '',
                                            });
                                            gridTataLaksanaAwal.data_store.insert(gridTataLaksanaAwal.data_store.getCount(), p);
                                            console.log(gridTataLaksanaAwal.grid_store_hasil);
                                        }
                                    }
                                }
                            },
                            failure     : function(o){
                            }
                        });
                    }

                }
                if (tab.activeTab.title == 'Tanda Vital') {
                    if (tmpediting != 'true') {
                        gridTandaVitalAwal.data_store.load({
                            params:
                            {
                                Skip: 0,
                                Take: 1000,
                                Sort: 'waktu',
                                Sortdir: 'ASC',
                                target: 'TandaVitalAskepIGD',
                                param: 'kd_pasien = ~'+rowSelected_viDaftarAwal.data.KD_PASIEN+'~ AND kd_unit = ~'+rowSelected_viDaftarAwal.data.KD_UNIT+'~ AND tgl_masuk = ~'+rowSelected_viDaftarAwal.data.TANGGAL_TRANSAKSI+'~ AND urut_masuk = ~'+rowSelected_viDaftarAwal.data.URUT_MASUK+'~'
                            }
                        });
                    }
                    // gridTindakanInvasifAwal.data_store
                    // KeperawatanAskepIGD;gawat_darurat/view_keperawatan
                }
                if (tab.activeTab.title == 'Keperawatan') {
                    if (tmpediting != 'true') {
                        gridPerawat.data_store.load({
                            params:
                            {
                                Skip: 0,
                                Take: 0,
                                Sort: 'urut',
                                Sortdir: '',
                                target: 'KeperawatanAskepIGD',
                                param: 'kd_pasien = ~'+rowSelected_viDaftarAwal.data.KD_PASIEN+'~ AND kd_unit = ~'+rowSelected_viDaftarAwal.data.KD_UNIT+'~ AND tgl_masuk = ~'+rowSelected_viDaftarAwal.data.TANGGAL_TRANSAKSI+'~ AND urut_masuk = ~'+rowSelected_viDaftarAwal.data.URUT_MASUK+'~'
                            },
                            callback : function(){
                                gridPerawat.data_store_hasil.removeAll();
                                var tmp_store = [];
                                for (var i = 0; i < gridPerawat.data_store.data.items.length; i++) {
                                    var data = [];
                                    data[0]     =  gridPerawat.data_store.data.items[i].data.CHECK_DIAGNOSA;
                                    data[1]     =  gridPerawat.data_store.data.items[i].data.DIAGNOSA;
                                    data[2]     =  gridPerawat.data_store.data.items[i].data.CHECK_INTERVENSI;
                                    data[3]     =  gridPerawat.data_store.data.items[i].data.INTERVENSI;
                                    data[4]     =  gridPerawat.data_store.data.items[i].data.EVALUASI;
                                    data[5]     =  gridPerawat.data_store.data.items[i].data.PARAF;
                                    data[6]     =  gridPerawat.data_store.data.items[i].data.ID_DIAGNOSA;
                                    data[7]     =  gridPerawat.data_store.data.items[i].data.ID_INTERVENSI;
                                    data[8]     =  gridPerawat.data_store.data.items[i].data.ID_GROUP;
                                    data[9]     =  gridPerawat.data_store.data.items[i].data.GROUP;
                                    tmp_store.push(data);
                                }
                                gridPerawat.data_store_hasil.loadData(tmp_store);
                            }
                        });
                    }

                    tmpediting  = 'true';
                    Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                }
                if (tab.activeTab.title == 'Tindakan Invasif') {
                    if (tmpediting != 'true') {
                        gridTindakanInvasifAwal.data_store.load({
                            params:
                            {
                                Skip: 0,
                                Take: 0,
                                Sort: 'urut',
                                Sortdir: '',
                                target: 'TindakInvasifAskepIGD',
                                param: 'kd_pasien = ~'+rowSelected_viDaftarAwal.data.KD_PASIEN+'~ AND kd_unit = ~'+rowSelected_viDaftarAwal.data.KD_UNIT+'~ AND tgl_masuk = ~'+rowSelected_viDaftarAwal.data.TANGGAL_TRANSAKSI+'~ AND urut_masuk = ~'+rowSelected_viDaftarAwal.data.URUT_MASUK+'~'
                            }
                        });
                    }
                    tmpediting  = 'true';
                    Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                    console.log(gridTindakanInvasifAwal.data_store);
                }
			}
		},
		items: [
			{
				title 		: 'Riwayat Kesehatan',
				layout 		: 'column',
				defaults 	: {width: 400},
				border 		: false,
                autoScroll  : true,
				items 		: [
					{
						border 	 	: false,
						layout 	 	: 'form',
						defaults 	: {width: 380},
						defaultType : 'textfield',
						items : [
							{
								xtype: 'fieldset',
								title: 'Keluhan Utama',
								autoHeight: true,
								labelStyle: 'display:none;',
								labelWidth: 1,
								defaultType: 'checkbox', // each item will be a checkbox
								items: [
									{
										xtype 		: 'textarea', 
										id 			: 'text_keluhan_utama',
										name		: 'text_keluhan_utama',
										width 		: '100%',
										rows 		: 4,
										value 		: DATA_ASKEP_IGD_AWAL.riwayat_kesehatan.keluhan_utama,
										listeners 	: {
											'render': function (c) {
												c.getEl().on('keypress', function (e) {
													if (e.getKey() === 13) {
													}
												}, c);
												c.getEl().on('change', function (e) {
													Ext.getCmp('btnSimpan_viDaftarAwal').enable();
													tmpediting 						= 'true';
													DATA_ASKEP_IGD_AWAL.riwayat_kesehatan.keluhan_utama	= Ext.get('text_keluhan_utama').getValue();
												}, c);
											}
										}
									}
								]
							},{
                                xtype: 'fieldset',
                                title: 'Riwayat Penyakit Sekarang',
                                autoHeight: true,
                                labelStyle: 'display:none;',
                                labelWidth: 1,
                                defaultType: 'checkbox', // each item will be a checkbox
                                items: [
                                    {
                                        xtype       : 'textarea', 
                                        id          : 'text_riwayat_penyakit_sekarang',
                                        name        : 'text_riwayat_penyakit_sekarang',
                                        width       : '100%',
                                        rows        : 4,
										value 		: DATA_ASKEP_IGD_AWAL.riwayat_kesehatan.penyakit_sekarang,
										listeners 	: {
											'render': function (c) {
												c.getEl().on('keypress', function (e) {
													if (e.getKey() === 13) {
													}
												}, c);
												c.getEl().on('change', function (e) {
													Ext.getCmp('btnSimpan_viDaftarAwal').enable();
													tmpediting 						= 'true';
													DATA_ASKEP_IGD_AWAL.riwayat_kesehatan.penyakit_sekarang	= Ext.get('text_riwayat_penyakit_sekarang').getValue();
												}, c);
											}
										}
                                    }
                                ]
                            },{
								xtype: 'fieldset',
								title: 'Riwayat Penyakit Dahulu',
								autoHeight: true,
								labelStyle: 'display:none;',
								labelWidth: 1,
								defaultType: 'checkbox', // each item will be a checkbox
								items: [
									{
										xtype 		: 'textarea', 
										id 			: 'text_riwayat_penyakit_dahulu',
										name		: 'text_riwayat_penyakit_dahulu',
										width 		: '100%',
										rows 		: 4,
										value 		: DATA_ASKEP_IGD_AWAL.riwayat_kesehatan.penyakit_dahulu,
										listeners 	: {
											'render': function (c) {
												c.getEl().on('keypress', function (e) {
													if (e.getKey() === 13) {
													}
												}, c);
												c.getEl().on('change', function (e) {
													Ext.getCmp('btnSimpan_viDaftarAwal').enable();
													tmpediting 						= 'true';
													DATA_ASKEP_IGD_AWAL.riwayat_kesehatan.penyakit_dahulu	= Ext.get('text_riwayat_penyakit_dahulu').getValue();
												}, c);
											}
										}
									}
								]
							},{
								xtype: 'fieldset',
								title: 'Kehamilan',
								autoHeight: true,
								defaultType: 'checkbox', // each item will be a checkbox
								items: [
									form_kehamilan(),
								]
							},
						]
					},{
						border: false,
						layout: 'form',
						defaults 	: {width: 380},
						defaultType: 'textfield',
						items : [
							{
								xtype: 'fieldset',
								title: 'SKALA NYERI',
								autoHeight: true,
								defaultType: 'radio', // each item will be a checkbox
								items: [
									form_skala_nyeri(),
									{
										fieldLabel 	: 'Lokasi',
										xtype 		: 'textfield',
										name 		: 'txt_skala_nyeri_lokasi',
										id 			: 'txt_skala_nyeri_lokasi',
										width 		: '100%',
										value 		: DATA_ASKEP_IGD_AWAL.skala_nyeri.lokasi,
										listeners 	: {
											'render': function (c) {
												c.getEl().on('keypress', function (e) {
													if (e.getKey() === 13) {
														// Ext.getCmp('txtriwayatpenyakitsekarang').focus();
													}
												}, c);
												c.getEl().on('change', function (e) {
													Ext.getCmp('btnSimpan_viDaftarAwal').enable();
													tmpediting = 'true';
													DATA_ASKEP_IGD_AWAL.skala_nyeri.lokasi	= Ext.get('txt_skala_nyeri_lokasi').getValue();
												}, c);
											}
										}
									},{
										fieldLabel 	: 'Durasi',
										xtype 		: 'textfield',
										name 		: 'txt_skala_nyeri_durasi',
										id 			: 'txt_skala_nyeri_durasi',
										width 		: '100%',
										value 		: DATA_ASKEP_IGD_AWAL.skala_nyeri.durasi,
										listeners 	: {
											'render': function (c) {
												c.getEl().on('keypress', function (e) {
													if (e.getKey() === 13) {
														// Ext.getCmp('txtriwayatpenyakitsekarang').focus();
													}
												}, c);
												c.getEl().on('change', function (e) {
													Ext.getCmp('btnSimpan_viDaftarAwal').enable();
													tmpediting = 'true';
													DATA_ASKEP_IGD_AWAL.skala_nyeri.durasi	= Ext.get('txt_skala_nyeri_durasi').getValue();
												}, c);
											}
										}
									},
								]
							},{
								xtype: 'fieldset',
								title: 'RESIKO JATUH',
								autoHeight: true,
								labelStyle: 'display:none;',
								labelWidth: 1,
								defaultType: 'radio', // each item will be a checkbox
								items: [
									form_resiko_jatuh(),
								]
							},
						]
					},{
						border: false,
						layout: 'form',
						defaults 	: {width: 380},
						defaultType: 'textfield',
						items : [
							{
								xtype: 'fieldset',
								title: 'Psikologis',
								autoHeight: true,
								labelStyle: 'display:none;',
								labelWidth: 1,
								defaultType: 'checkbox', // each item will be a checkbox
								items: [
									form_psikologis(),
								]
							},
						]
					},{
						border: false,
						layout: 'form',
						defaults 	: {width: 380},
						defaultType: 'textfield',
						items : [
							{
								xtype: 'fieldset',
								title: 'Status Sosial Ekonomi',
								autoHeight: true,
								labelStyle: 'display:none;',
								labelWidth: 1,
								defaultType: 'radio', // each item will be a checkbox
								items: [
									form_status_sosial_ekonomiAwal(),
								]
							},
						]
					},
				]
            },
			{
				title 		: 'Trease',
				layout 		: 'column',
				border 		: false,
                autoScroll  : true,
				items  		: [
					{
						border: false,
						layout: 'form',
						defaults 	: {width: 600},
						defaultType: 'textfield',
						bodyStyle 	: 'margin-right : 10px;',
						items : [
							{
								xtype: 'fieldset',
								title: 'Trease',
								autoHeight: true,
								defaultType: 'checkbox', // each item will be a checkbox
								items: [
                                    {
                                        xtype           : 'combo',
                                        width           : '100%',
                                        fieldLabel      : 'Trease',
                                        mode            : 'local',
                                        typeAhead       : true,
                                        triggerAction   : 'all',
                                        lazyRender      : true,
                                        selectOnFocus   : true,
                                        forceSelection  : true,
                                        valueField      : 'id',
                                        displayField    : 'displayText',
                                        value           : DATA_ASKEP_IGD_AWAL.trease.kd_trease,
                                        store           : new Ext.data.ArrayStore
                                        (
                                            {
                                                id: 0,
                                                fields:
                                                [
                                                    'id','displayText'
                                                ],
                                            data: [[1,'1'],[2,'2'],[3,'3'],[4,'4'],[5,'5'],[6,'DOA']]
                                            }
                                        ),
                                        listeners   : {
                                            select  : function(a, b){
                                                Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                tmpediting = 'true';
                                                DATA_ASKEP_IGD_AWAL.trease.kd_trease = b.data.id;
                                                gridTreaseAwal.all_hidden();
                                                gridTreaseAwal.all_uncheck();
                                                gridTreaseAwal.validasi_form(DATA_ASKEP_IGD_AWAL.trease.kd_trease);
                                            }
                                        }
                                    },
                                    {
                                        xtype   : 'label',
                                        html    : '<hr>',
                                    },
                                    gridTreaseAwal.form_jalan_nafas(),
                                    {
                                        xtype   : 'label',
                                        html    : '<hr>',
                                    },
                                    gridTreaseAwal.form_pernafasan(),
                                    {
                                        xtype   : 'label',
                                        html    : '<hr>',
                                    },
                                    gridTreaseAwal.form_sirkulasi(),
                                    {
                                        xtype   : 'label',
                                        html    : '<hr>',
                                    },
                                    gridTreaseAwal.form_kesadaran(),
								],
							}
						]
					},{
						border: false,
						layout: 'form',
						defaults 	: {width: 320},
						defaultType: 'textfield',
						bodyStyle 	: 'margin-right : 10px;',
						items : [
                            {
								xtype: 'fieldset',
								title: 'Tanda Vital',
								autoHeight: true,
								bodyStyle 	: 'margin : 0px;padding:0px;',
								labelWidth 	: 120,
								defaultType: 'textfield', 
								items: [
                                    {
                                        xtype   : 'label',
                                        text    : 'Keadaan Umum',
                                    },
                                    {
                                        xtype   : 'label',
                                        html    : '<hr>',
                                    },
									form_triase_keadaan_umum(),
                                    {
                                        xtype   : 'label',
                                        html    : '<hr>',
                                    },{
                                        xtype       : 'radiogroup',
                                        columns     : 2,
                                        fieldLabel  : 'Imunisasi',
                                        items       : [
                                            {
                                                boxLabel    : 'Ya',
                                                name        : 'radio_triase_imunisasi',
                                                id          : 'radio_triase_imunisasi_ya',
                                                value       : 0,
                                            },{
                                                boxLabel    : 'Tidak',
                                                name        : 'radio_triase_imunisasi',
                                                id          : 'radio_triase_imunisasi_tidak',
                                                value       : 1,
                                            }
                                        ],
                                        listeners   : {
                                            render  : function(c){
                                                if(DATA_ASKEP_IGD_AWAL.tanda_vital.imunisasi == '0' || DATA_ASKEP_IGD_AWAL.tanda_vital.imunisasi == 0){
                                                    Ext.getCmp('radio_triase_imunisasi_ya').setValue(true);
                                                }else{
                                                    Ext.getCmp('radio_triase_imunisasi_tidak').setValue(true);
                                                }
                                            },
                                            change   : function(checked, newValue){
                                                Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                tmpediting = 'true';
                                                DATA_ASKEP_IGD_AWAL.tanda_vital.imunisasi = newValue.value;
                                            }
                                        }
                                    },{
                                        xtype   : 'label',
                                        html    : '<hr>',
                                    },{
                                        xtype       : 'checkboxgroup',
                                        columns     : 2,
                                        fieldLabel  : 'Alergi',
                                        items       : [
                                            {
                                                boxLabel    : 'Obat',
                                                id          : 'check_triase_riwayat_alergi_obat',
                                                name        : 'check_triase_riwayat_alergi_obat',
                                                checked     : DATA_ASKEP_IGD_AWAL.tanda_vital.alergi_obat,
                                            },{
                                                boxLabel    : 'Makanan',
                                                id          : 'check_triase_riwayat_alergi_makanan',
                                                name        : 'check_triase_riwayat_alergi_makanan',
                                                checked     : DATA_ASKEP_IGD_AWAL.tanda_vital.alergi_makanan,
                                            },{
                                                boxLabel    : 'Lain-lain',
                                                id          : 'check_triase_riwayat_alergi_lain_lain',
                                                name        : 'check_triase_riwayat_alergi_lain_lain',
                                                checked     : DATA_ASKEP_IGD_AWAL.tanda_vital.alergi_lain_lain,
                                            }
                                        ],
                                        listeners   : {
                                            change: function (checkbox, newValue, oldValue, eOpts) {
                                                tmpediting = 'true';
                                                Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                DATA_ASKEP_IGD_AWAL.tanda_vital.alergi_obat        = false;
                                                DATA_ASKEP_IGD_AWAL.tanda_vital.alergi_makanan     = false;
                                                DATA_ASKEP_IGD_AWAL.tanda_vital.alergi_lain_lain   = false;
                                                for (var i = 0; i < newValue.length; i++) {
                                                    if (newValue[i].id == 'check_triase_riwayat_alergi_obat') {
                                                        DATA_ASKEP_IGD_AWAL.tanda_vital.alergi_obat = newValue[i].checked;
                                                    }

                                                    if (newValue[i].id == 'check_triase_riwayat_alergi_makanan') {
                                                        DATA_ASKEP_IGD_AWAL.tanda_vital.alergi_makanan = newValue[i].checked;
                                                    }

                                                    if (newValue[i].id == 'check_triase_riwayat_alergi_lain_lain') {
                                                        DATA_ASKEP_IGD_AWAL.tanda_vital.alergi_lain_lain = newValue[i].checked;
                                                    }
                                                }
                                            }
                                        }
                                    },{
                                        xtype   : 'label',
                                        html    : '<hr>',
                                    },/*{
                                        fieldLabel  : 'Diagnosa Kerja',
                                        xtype       : 'textfield',
                                        name        : 'textfield_diagnosa_kerja',
                                        id          : 'textfield_diagnosa_kerja',
                                        width       : '100%',
                                        value       : DATA_ASKEP_IGD_AWAL.tanda_vital.diagnosa_kerja,
                                        listeners   : {

                                        }
                                    }*/
                                    Nci.form.Combobox.autoComplete({
                                        fieldLabel  : 'Diagnosa Kerja',
                                        name        : 'textfield_diagnosa_kerja',
                                        id          : 'textfield_diagnosa_kerja',
                                        store       : new Ext.data.ArrayStore({id: 0,fields: ['kd_penyakit','penyakit'],data: []}),
                                        value       : DATA_ASKEP_IGD_AWAL.tanda_vital.diagnosa_kerja,
                                        select      : function(a,b,c){
                                            DATA_ASKEP_IGD_AWAL.tanda_vital.diagnosa_kerja = b.data.penyakit;
                                            tmpediting = 'true';
                                            Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                        },
                                        param   : function(){
                                            var params={};
                                            return params;
                                        },
                                        insert  : function(o){
                                            return {
                                                kd_penyakit     : o.kd_penyakit,
                                                penyakit        : o.penyakit,
                                                text            : '<table style="font-size: 11px;"><tr><td width="50">'+o.kd_penyakit+'</td><td width="160" align="left">'+o.penyakit+'</td></tr></table>'
                                            }
                                        },
                                        url         : baseURL + "index.php/gawat_darurat/control_askep_awal_igd/get_icd_10",
                                        valueField  : 'penyakit',
                                        displayField: 'penyakit',
                                        keyField    :'kd_penyakit',
                                        listWidth: 250
                                    })
								]
                            },
						]
					},{
						border: false,
						layout: 'form',
						defaults 	: {width: 320},
						defaultType: 'textfield',
						bodyStyle 	: 'margin-right : 10px;',
						items : [
                            {
                                xtype: 'fieldset',
                                title: 'Lanjutan',
                                autoHeight: true,
                                labelStyle: 'display:none;',
                                labelWidth: 1,
                                defaultType: 'checkbox', // each item will be a checkbox
                                items: [
                                    form_triase_lanjutan(),
                                ]
                            },{
                                xtype: 'fieldset',
                                title: 'Serah terima Jaga Triase',
                                autoHeight: true,
                                defaultType: 'textfield', // each item will be a checkbox
                                items: [
                                    {
                                        xtype           : 'combo',
                                        fieldLabel      : 'Dokter Jaga',
                                        id              : 'cbo_dokter_jaga_trease',
                                        store           : dsDokterJaga,
                                        valueField      : 'KD_DOKTER',
                                        displayField    : 'NAMA',
                                        typeAhead       : true,
                                        width           : '100%',
                                        triggerAction   : 'all',
                                        lazyRender      : true,
                                        mode            : 'local', 
                                        forceSelection  : true,
                                        emptyText       : 'Pilih Dokter...',
                                        selectOnFocus   : true,
                                        align           : 'Right',
                                        value           : DATA_ASKEP_IGD_AWAL.serah_terima.dokter_jaga,
                                        listeners   : {
                                        	select 	: function(a, b){
												Ext.getCmp('btnSimpan_viDaftarAwal').enable();
												tmpediting = 'true';
												DATA_ASKEP_IGD_AWAL.serah_terima.dokter_jaga = b.data.KD_DOKTER;
                                        	}
                                        }
                                    },
                                	/*{
                                		xtype 		: 'textfield',
                                		id 			: 'txt_serah_terima_trease_',
                                		name 		: 'txt_serah_terima_trease_',
                                		fieldLabel 	: 'Dokter jaga',
                                		width 		: '100%',
                                	},*/{
                                		xtype 		    : 'textfield',
                                		id 			    : 'txt_serah_terima_trease_jam',
                                		name 		    : 'txt_serah_terima_trease_jam',
                                		fieldLabel 	    : 'Jam',
                                		width 		    : '100%',
                                        value           : DATA_ASKEP_IGD_AWAL.serah_terima.waktu.format('H:i:s'),
                                		// value 		    : now.format('H:i:s'),
									},{
                                        xtype           : 'combo',
                                        fieldLabel      : 'Perawat Trease',
                                        id              : 'cbo_perawat_trease',
                                        store           : dsPerawatTrease,
                                        valueField      : 'KD_DOKTER',
                                        displayField    : 'NAMA',
                                        typeAhead       : true,
                                        width           : '100%',
                                        triggerAction   : 'all',
                                        lazyRender      : true,
                                        mode            : 'local', 
                                        forceSelection  : true,
                                        emptyText       : 'Pilih Dokter...',
                                        value           : DATA_ASKEP_IGD_AWAL.serah_terima.perawat_trease,
                                        selectOnFocus   : true,
                                        align           : 'Right',
                                        listeners   : {
                                        	select 	: function(a, b){
												Ext.getCmp('btnSimpan_viDaftarAwal').enable();
												tmpediting = 'true';
                                        		DATA_ASKEP_IGD_AWAL.serah_terima.perawat_trease = b.data.KD_DOKTER;
                                        	}
                                        }
                                    },/*{
                                        // dsPerawatTrease
										xtype 		: 'textfield',
										id 			: 'txt_serah_terima_trease_perawat_trease',
										name 		: 'txt_serah_terima_trease_perawat_trease',
										fieldLabel 	: 'Perawat Trease',
										width 		: '100%',
									},{
										xtype 		: 'textfield',
										id 			: 'txt_serah_terima_trease_medikal',
										name 		: 'txt_serah_terima_trease_medikal',
										fieldLabel 	: 'Medikal',
										width 		: '100%',
									},{
										xtype 		: 'textfield',
										id 			: 'txt_serah_terima_trease_anak',
										name 		: 'txt_serah_terima_trease_anak',
										fieldLabel 	: 'Anak',
										width 		: '100%',
									},{
										xtype 		: 'textfield',
										id 			: 'txt_serah_terima_trease_surgikal',
										name 		: 'txt_serah_terima_trease_surgikal',
										fieldLabel 	: 'Surgikal',
										width 		: '100%',
									},{
										xtype 		: 'textfield',
										id 			: 'txt_serah_terima_trease_kebidanan',
										name 		: 'txt_serah_terima_trease_kebidanan',
										fieldLabel 	: 'Kebidanan',
										width 		: '100%',
									},{
										xtype 		: 'textfield',
										id 			: 'txt_serah_terima_trease_resusitasi',
										name 		: 'txt_serah_terima_trease_resusitasi',
										fieldLabel 	: 'Resusitasi',
										width 		: '100%',
									},*/
                                ]
                            },
						]
					}
				]
			},{
                title       : 'Pemeriksaan Fisik dan Penunjang',
                layout      : 'column',
                border      : false,
                autoScroll  : true,
                items       : [
                    {
                        border: false,
                        layout: 'form',
                        defaults    : {width: 360},
                        defaultType: 'textfield',
                        bodyStyle   : 'margin-right : 10px;',
                        items : [
                            {
                                xtype: 'fieldset',
                                title: 'KU',
                                autoHeight: true,
                                bodyStyle   : 'margin : 0px;padding:0px;',
                                labelStyle: 'display:none;',
                                labelWidth  : 1,
                                defaultType: 'radio', 
                                items: [
                                    form_pemeriksaan_fisik_ku(),
                                ]
                            },{
                                xtype: 'fieldset',
                                title: 'Kesadaran',
                                autoHeight: true,
                                bodyStyle   : 'margin : 0px;padding:0px;',
                                labelStyle: 'display:none;',
                                labelWidth  : 1,
                                defaultType: 'checkbox', 
                                items: [
                                    form_pemeriksaan_fisik_kesadaran(),
                                ]
                            },{
                                xtype: 'fieldset',
                                title: '',
                                autoHeight: true,
                                bodyStyle   : 'margin : 0px;padding:0px;',
                                labelWidth  : 100,
                                defaultType: 'textfield', 
                                items: [
                                    form_pemeriksaan_fisik_(),
                                    {
                                        xtype       : 'textarea',
                                        id          : 'textarea_catatan_pemeriksaan',
                                        rows        : 10,
                                        width       : '100%',
                                        value       : DATA_ASKEP_IGD_AWAL.pemeriksaan.catatan,
                                        listeners   : {
                                            render  : function(c){
                                                c.getEl().on('keypress', function (e) {
                                                    Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                    tmpediting = 'true';
                                                }, c);
                                                c.getEl().on('change', function (e) {
                                                    DATA_ASKEP_IGD_AWAL.pemeriksaan.catatan = Ext.getCmp('textarea_catatan_pemeriksaan').getValue();
                                                }, c);
                                            }
                                        }
                                    }
                                ]
                            },
                        ]
                    },{
                        border: false,
                        layout: 'form',
                        defaults    : {width: 800},
                        defaultType: 'textfield',
                        bodyStyle   : 'margin-right : 10px;',
                        items : [
                           gridPemeriksaanPenunjangAwal.get_grid(),
                            Nci.form.Combobox.autoComplete({
                                store       : new Ext.data.ArrayStore({id: 0,fields: ['kd_penyakit','penyakit'],data: []}),
                                fieldLabel  : 'Diagnosis',
                                value       : DATA_ASKEP_IGD_AWAL.pemeriksaan.diagnosa,
                                select  : function(a,b,c){
                                    DATA_ASKEP_IGD_AWAL.pemeriksaan.diagnosa       = b.data.penyakit;
                                    tmpediting = 'true';
                                    Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                },
                                param   : function(){
                                    var params={};
                                    return params;
                                },
                                insert  : function(o){
                                    return {
                                        kd_penyakit     : o.kd_penyakit,
                                        penyakit        : o.penyakit,
                                        text            : '<table style="font-size: 11px;"><tr><td width="50">'+o.kd_penyakit+'</td><td width="160" align="left">'+o.penyakit+'</td></tr></table>'
                                    }
                                },
                                url         : baseURL + "index.php/gawat_darurat/control_askep_awal_igd/get_icd_10",
                                valueField  : 'penyakit',
                                displayField: 'penyakit',
                            })
                        ]
                    }
                ]
            },{
                title       : 'Tata Laksana',
                layout      : 'column',
                border      : false,
                bodyStyle   : 'padding:0px;',
                items       : [    
                    gridTataLaksanaAwal.get_grid(),
                ]
            },{
                title       : 'Tanda Vital',
                layout      : 'column',
                border      : false,
                bodyStyle   : 'padding:0px;',
                items       : [
                    gridTandaVitalAwal.get_grid(),
                ]
            },{
                title       : 'Tindakan Invasif',
                layout      : 'column',
                border      : false,
                bodyStyle   : 'padding:0px;',
                items       : [
                    gridTindakanInvasifAwal.get_grid(),
                ]
            },{
                title       : 'Tindak Lanjut',
                layout      : 'column',
                border      : false,
                bodyStyle   : 'padding:0px;',
                items       : [
                    {
                        border: false,
                        layout: 'form',
                        defaults    : {width: 500},
                        defaultType: 'textfield',
                        bodyStyle   : 'margin-right : 10px;',
                        items : [
                            {
                                xtype: 'fieldset',
                                title: 'Keadaan saat keluar',
                                autoHeight: true,
                                bodyStyle   : 'margin : 0px;padding:0px;', 
                                defaultType: 'radio',
                                items: [
                                    {
                                        xtype       : 'radiogroup',
                                        columns     : 1,
                                        fieldLabel  : '',
                                        items       : [
                                            {
                                                fieldLabel  : 'Membaik',
                                                id          : 'radio_keadaan_keluar_membaik',
                                                name        : 'radio_keadaan_keluar',
                                                value       : 0,
                                            },{
                                                fieldLabel  : 'Memburuk',
                                                id          : 'radio_keadaan_keluar_memburuk',
                                                name        : 'radio_keadaan_keluar',
                                                value       : 1,
                                            },{
                                                fieldLabel  : 'Menetap',
                                                id          : 'radio_keadaan_keluar_menetap',
                                                name        : 'radio_keadaan_keluar',
                                                value       : 2,
                                            },{
                                                fieldLabel  : 'Meninggal',
                                                id          : 'radio_keadaan_keluar_meninggal',
                                                name        : 'radio_keadaan_keluar',
                                                value       : 3,
                                            },
                                        ],
                                        listeners   : {
                                            change  : function(a, b){   
                                                DATA_ASKEP_IGD_AWAL.tindak_lanjut.keadaan = b.value;
                                                Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                tmpediting = 'true';
                                            },
                                            render  : function(c){
                                                if (DATA_ASKEP_IGD_AWAL.tindak_lanjut.keadaan == 0 || DATA_ASKEP_IGD_AWAL.tindak_lanjut.keadaan == '0') { 
                                                    Ext.getCmp('radio_keadaan_keluar_membaik').setValue(true);
                                                }else if (DATA_ASKEP_IGD_AWAL.tindak_lanjut.keadaan == 1 || DATA_ASKEP_IGD_AWAL.tindak_lanjut.keadaan == '1') { 
                                                    Ext.getCmp('radio_keadaan_keluar_memburuk').setValue(true);
                                                }else if (DATA_ASKEP_IGD_AWAL.tindak_lanjut.keadaan == 2 || DATA_ASKEP_IGD_AWAL.tindak_lanjut.keadaan == '2') { 
                                                    Ext.getCmp('radio_keadaan_keluar_menetap').setValue(true);
                                                }else if (DATA_ASKEP_IGD_AWAL.tindak_lanjut.keadaan == 3 || DATA_ASKEP_IGD_AWAL.tindak_lanjut.keadaan == '3') { 
                                                    Ext.getCmp('radio_keadaan_keluar_meninggal').setValue(true);
                                                }
                                            }
                                        }
                                    },{
                                        xtype       : 'textfield',
                                        fieldLabel  : 'TD (mmHg)',
                                        id          : 'txt_keadaan_keluar_td',
                                        name        : 'txt_keadaan_keluar',
                                        width       : '100%',
                                        value       : DATA_ASKEP_IGD_AWAL.tindak_lanjut.td,
                                        listeners   : {
                                            'render': function (c) {
                                                c.getEl().on('keypress', function (e) {
                                                    Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                    tmpediting = 'true';
                                                }, c);
                                                c.getEl().on('change', function (e) {
                                                    DATA_ASKEP_IGD_AWAL.tindak_lanjut.td = Ext.get('txt_keadaan_keluar_td').getValue();
                                                }, c);
                                            }
                                        }
                                    },{
                                        xtype       : 'textfield',
                                        fieldLabel  : 'Nadi (x/i)',
                                        id          : 'txt_keadaan_keluar_nadi',
                                        name        : 'txt_keadaan_keluar',
                                        value       : DATA_ASKEP_IGD_AWAL.tindak_lanjut.nadi,
                                        width       : '100%',
                                        listeners   : {
                                            'render': function (c) {
                                                c.getEl().on('keypress', function (e) {
                                                    Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                    tmpediting = 'true';
                                                }, c);
                                                c.getEl().on('change', function (e) {
                                                    DATA_ASKEP_IGD_AWAL.tindak_lanjut.nadi = Ext.get('txt_keadaan_keluar_nadi').getValue();
                                                }, c);
                                            }
                                        }
                                    },{
                                        xtype       : 'textfield',
                                        fieldLabel  : 'Suhu (Cel)',
                                        id          : 'txt_keadaan_keluar_suhu',
                                        name        : 'txt_keadaan_keluar',
                                        value       : DATA_ASKEP_IGD_AWAL.tindak_lanjut.suhu,
                                        width       : '100%',
                                        listeners   : {
                                            'render': function (c) {
                                                c.getEl().on('keypress', function (e) {
                                                    Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                    tmpediting = 'true';
                                                }, c);
                                                c.getEl().on('change', function (e) {
                                                    DATA_ASKEP_IGD_AWAL.tindak_lanjut.suhu = Ext.get('txt_keadaan_keluar_suhu').getValue();
                                                }, c);
                                            }
                                        }
                                    },{
                                        xtype       : 'textfield',
                                        fieldLabel  : 'Pernapasan (x/i)',
                                        id          : 'txt_keadaan_keluar_pernapasan',
                                        name        : 'txt_keadaan_keluar',
                                        value       : DATA_ASKEP_IGD_AWAL.tindak_lanjut.pernapasan,
                                        width       : '100%',
                                        listeners   : {
                                            'render': function (c) {
                                                c.getEl().on('keypress', function (e) {
                                                    Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                    tmpediting = 'true';
                                                }, c);
                                                c.getEl().on('change', function (e) {
                                                    DATA_ASKEP_IGD_AWAL.tindak_lanjut.pernapasan = Ext.get('txt_keadaan_keluar_pernapasan').getValue();
                                                }, c);
                                            }
                                        }
                                    },
                                ]
                            },
                        ]
                    },{
                        border: false,
                        layout: 'form',
                        defaults    : {width: 700},
                        defaultType: 'textfield',
                        bodyStyle   : 'margin-right : 10px;',
                        items : [
                            {
                                xtype: 'fieldset',
                                title: 'Tindak Lanjut',
                                autoHeight: true,
                                bodyStyle   : 'margin : 0px;padding:0px;', 
                                defaultType: 'checkbox',
                                items: [
                                    {
                                        xtype       :'combo',
                                        fieldLabel  : 'Tindak Lanjut',
                                        name        :'combo_tindak_lanjut_',
                                        id          :'combo_tindak_lanjut',
                                        mode        : 'local',
                                        typeAhead: true,
                                        triggerAction: 'all',
                                        lazyRender: true,
                                        selectOnFocus:true,
                                        forceSelection: true,
                                        store: new Ext.data.ArrayStore
                                        (
                                            {
                                                id: 0,
                                                fields:
                                                [
                                                    'Id',
                                                    'displayText'
                                                ],
                                            data: [[1, 'Rawat'],[2, 'Pulang'],[3, 'Rujuk']]
                                            }
                                        ),
										valueField  : 'Id',
										displayField: 'displayText',
										width       : '100%',
                                        value       : DATA_ASKEP_IGD_AWAL.tindak_lanjut.tindak_lanjut,
										anchor      : '100%',
										listeners   : {
											select  : function(a, b){
                                                Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                tmpediting = 'true';
                                                DATA_ASKEP_IGD_AWAL.tindak_lanjut.tindak_lanjut = b.data.Id;
                                                cek_form_tindak_lanjutAwal(b.data.Id);
											}
										}
									}, {
										xtype   : 'label',
										html    : '<hr>',
									},
									form_tindak_lanjutAwal(DATA_ASKEP_IGD_AWAL.tindak_lanjut.tindak_lanjut),
                                ]
							},{
								xtype: 'fieldset',
								title: 'Dokter Pemeriksa',
								autoHeight: true,
								bodyStyle   : 'margin : 0px;padding:0px;', 
								defaultType: 'textfield',
								items: [
									/*{
										fieldLabel 	: 'Dokter pemeriksa',
										id 			: 'txt_tindak_lanjut_dokter_pemeriksa',
										name		: 'txt_tindak_lanjut_dokter_pemeriksa',
										width		: '100%',
									}*/
                                    cbo_tindak_lanjut_pemeriksa = new Nci.form.Combobox.autoComplete({
                                        fieldLabel  : 'Dokter Pemeriksaan',
                                        name        : 'txt_tindak_lanjut_dokter_pemeriksa',
                                        id          : 'txt_tindak_lanjut_dokter_pemeriksa',
                                        width       : '100%',
                                        anchor      : '100%',
                                        store       : new Ext.data.ArrayStore({id: 0,fields: ['kd_dokter','nama'],data: []}),
                                        value       : DATA_ASKEP_IGD_AWAL.tindak_lanjut.dokter_pemeriksa,
                                        select      : function(a,b,c){
                                            DATA_ASKEP_IGD_AWAL.tindak_lanjut.dokter_pemeriksa = b.data.kd_dokter;
                                            tmpediting = 'true';
                                            Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                        },
                                        param   : function(){
                                            var params={};
                                            params.kd_job = '1';
                                            return params;
                                        },
                                        insert  : function(o){
                                            return {
                                                kd_dokter       : o.kd_dokter,
                                                nama            : o.nama,
                                                text            : '<table style="font-size: 11px;"><tr><td width="50">'+o.kd_dokter+'</td><td width="160" align="left">'+o.nama+'</td></tr></table>'
                                            }
                                        },
                                        url         : baseURL + "index.php/gawat_darurat/control_askep_awal_igd/get_dokter",
                                        valueField  : 'nama',
                                        displayField: 'nama',
                                        keyField    :'kd_dokter',
                                        listWidth   : 250,
                                    })
								],
							},
						]
					}
				]
            },
		]
	};
	return items;
}

function cek_form_tindak_lanjutAwal(combo){
    if (combo == 1) {
        Ext.getCmp('tindak_lanjut_rawat_konsultasi_dokter_1').show();
        Ext.getCmp('tindak_lanjut_rawat_konsultasi_dokter_2').show();
        Ext.getCmp('tindak_lanjut_rawat_konsultasi_dokter_3').show();
        Ext.getCmp('tindak_lanjut_rawat_konsultasi_dokter_4').show();
        Ext.getCmp('tindak_lanjut_pulang_waktu_rawat').show();//2019-04-18
        Ext.getCmp('tindak_lanjut_rawat_waktu').show();
        Ext.getCmp('tindak_lanjut_pulang_persetujuan').hide();
        Ext.getCmp('tindak_lanjut_pulang_waktu').hide();
        Ext.getCmp('tindak_lanjut_pulang_kontrol').hide();
        Ext.getCmp('tindak_lanjut_pulang_terapi_pulang').hide();
        Ext.getCmp('tindak_lanjut_pulang_edukasi').hide();
        Ext.getCmp('tindak_lanjut_pulang_karena').hide();
        Ext.getCmp('tindak_lanjut_rujuk_kepada').hide();
        Ext.getCmp('tindak_lanjut_rujuk_alasan').hide();
        Ext.getCmp('tindak_lanjut_pulang_waktu_rujuk').hide(); //2019-04-18
    }else if(combo == 2){
        Ext.getCmp('tindak_lanjut_rawat_konsultasi_dokter_1').hide();
        Ext.getCmp('tindak_lanjut_rawat_konsultasi_dokter_2').hide();
        Ext.getCmp('tindak_lanjut_rawat_konsultasi_dokter_3').hide();
        Ext.getCmp('tindak_lanjut_rawat_konsultasi_dokter_4').hide();
        Ext.getCmp('tindak_lanjut_rawat_waktu').hide();
        Ext.getCmp('tindak_lanjut_pulang_waktu_rawat').hide();//2019-04-18
        Ext.getCmp('tindak_lanjut_pulang_persetujuan').show();
        Ext.getCmp('tindak_lanjut_pulang_waktu').show();
        Ext.getCmp('tindak_lanjut_pulang_kontrol').show();
        Ext.getCmp('tindak_lanjut_pulang_terapi_pulang').show();
        Ext.getCmp('tindak_lanjut_pulang_edukasi').show();
        Ext.getCmp('tindak_lanjut_pulang_karena').show();
        Ext.getCmp('tindak_lanjut_rujuk_kepada').hide();
        Ext.getCmp('tindak_lanjut_rujuk_alasan').hide();
        Ext.getCmp('tindak_lanjut_pulang_waktu_rujuk').hide(); //2019-04-18
    }else if(combo == 3){
        Ext.getCmp('tindak_lanjut_rawat_konsultasi_dokter_1').hide();
        Ext.getCmp('tindak_lanjut_rawat_konsultasi_dokter_2').hide();
        Ext.getCmp('tindak_lanjut_rawat_konsultasi_dokter_3').hide();
        Ext.getCmp('tindak_lanjut_rawat_konsultasi_dokter_4').hide();
        Ext.getCmp('tindak_lanjut_pulang_waktu_rawat').hide();//2019-04-18
        Ext.getCmp('tindak_lanjut_rawat_waktu').hide();
        Ext.getCmp('tindak_lanjut_pulang_persetujuan').hide();
        Ext.getCmp('tindak_lanjut_pulang_waktu').hide();
        Ext.getCmp('tindak_lanjut_pulang_kontrol').hide();
        Ext.getCmp('tindak_lanjut_pulang_terapi_pulang').hide();
        Ext.getCmp('tindak_lanjut_pulang_edukasi').hide();
        Ext.getCmp('tindak_lanjut_pulang_karena').hide();
        Ext.getCmp('tindak_lanjut_rujuk_kepada').show();
        Ext.getCmp('tindak_lanjut_rujuk_alasan').show();
        Ext.getCmp('tindak_lanjut_pulang_waktu_rujuk').show();//2019-04-18
    }else{
		Ext.getCmp('tindak_lanjut_rawat_konsultasi_dokter_1').hide();
        Ext.getCmp('tindak_lanjut_rawat_konsultasi_dokter_2').hide();
        Ext.getCmp('tindak_lanjut_rawat_konsultasi_dokter_3').hide();
        Ext.getCmp('tindak_lanjut_rawat_konsultasi_dokter_4').hide();
        Ext.getCmp('tindak_lanjut_pulang_waktu_rawat').hide();//2019-04-18
        Ext.getCmp('tindak_lanjut_rawat_waktu').hide();
        Ext.getCmp('tindak_lanjut_pulang_persetujuan').hide();
        Ext.getCmp('tindak_lanjut_pulang_waktu').hide();
        Ext.getCmp('tindak_lanjut_pulang_kontrol').hide();
        Ext.getCmp('tindak_lanjut_pulang_terapi_pulang').hide();
        Ext.getCmp('tindak_lanjut_pulang_edukasi').hide();
        Ext.getCmp('tindak_lanjut_pulang_karena').hide();
        Ext.getCmp('tindak_lanjut_rujuk_kepada').hide();
        Ext.getCmp('tindak_lanjut_rujuk_alasan').hide();
        Ext.getCmp('tindak_lanjut_pulang_waktu_rujuk').hide(); //2019-04-18
	}
}
function form_tindak_lanjutAwal(combo) { 
    var hide_rawat  = true;
    var hide_pulang = true;
    var hide_rujuk = true;
    if (combo == 1 || combo == '1') {
        hide_rawat  = false;
    }
    if (combo == 2 || combo == '2') {
        hide_pulang  = false;
    }
    if (combo == 3 || combo == '3') {
        hide_rujuk  = false;
    }
    var items = [
        /* {
            xtype       : 'textfield',
            width       : '100%',
            fieldLabel  : 'Konsultasi',
            id          : 'tindak_lanjut_rawat_konsultasi_dokter_1_tmp',
            value       : DATA_ASKEP_IGD_AWAL.tindak_lanjut.konsultasi,
            // hidden      : hide_rawat,
            hidden      : true,
             listeners   : {
                'render': function (c) {
                    c.getEl().on('keypress', function (e) {
                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                        tmpediting = 'true';
                    }, c);
                        c.getEl().on('change', function (e) {
                        DATA_ASKEP_IGD_AWAL.tindak_lanjut.konsultasi = Ext.get('tindak_lanjut_rawat_konsultasi_dokter_1_tmp').getValue();
                    }, c);
                }
             }
        }, */
		// 2019-04-18
		{
            xtype       : 'checkboxgroup',
            columns     : 3,
            fieldLabel  : 'Waktu Pulang',
            id          : 'tindak_lanjut_pulang_waktu_rawat',
            hidden      : hide_rawat,
            items       : [
               {
                    xtype       : 'datefield',
                    id          : 'txt_tanggal_pulang_rawat',
                    fieldLabel  : '',
                    format      : 'Y-m-d',
                    value       : DATA_ASKEP_IGD_AWAL.tindak_lanjut.tgl_pulang,
                    listeners   : {
                       'render': function (c) {
                           c.getEl().on('keypress', function (e) {
                               Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                               tmpediting = 'true';
                           }, c);
                               c.getEl().on('change', function (e) {
							    Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                               DATA_ASKEP_IGD_AWAL.tindak_lanjut.tgl_pulang = Ext.get('txt_tanggal_pulang_rawat').getValue();
							  
                           }, c);
                       },
					   change: function (t,n,o) {
							DATA_ASKEP_IGD_AWAL.tindak_lanjut.tgl_pulang = Ext.get('txt_tanggal_pulang_rawat').getValue();
							  
						},
                    }
                },
				{
					xtype		:'timefield',
					name		: 'textfieldJamPulangRawat',
					id			: 'textfieldJamPulangRawat',
					fieldLabel	: '&nbsp; Jam Plg',
					format		: 'H:i:s',
					value       : DATA_ASKEP_IGD_AWAL.tindak_lanjut.jam_pulang,
					listeners   : {
						'render': function (c) {
							c.getEl().on('keypress', function (e) {
								Ext.getCmp('btnSimpan_viDaftarAwal').enable();
								DATA_ASKEP_IGD_AWAL.tindak_lanjut.jam_pulang = Ext.get('textfieldJamPulangRawat').getValue();
							}, c);
							c.getEl().on('change', function (e) {
								 Ext.getCmp('btnSimpan_viDaftarAwal').enable();
								DATA_ASKEP_IGD_AWAL.tindak_lanjut.jam_pulang = Ext.get('textfieldJamPulangRawat').getValue();
							}, c);
						},
						change: function (t,n,o) {
							 Ext.getCmp('btnSimpan_viDaftarAwal').enable();
							DATA_ASKEP_IGD_AWAL.tindak_lanjut.jam_pulang = Ext.get('textfieldJamPulangRawat').getValue();
						},
					}
				} 
				
            ],
            listeners   : {
                change: function (checkbox, newValue, oldValue, eOpts) {
                    Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                    tmpediting = 'true';
                },
            }
        },
		{
			xtype           : 'combo',
			fieldLabel      : 'Konsultasi',
			id              : 'tindak_lanjut_rawat_konsultasi_dokter_1',
			store           : dsDokterKonsultasi_1,
			valueField      : 'KD_DOKTER',
			displayField    : 'NAMA',
			typeAhead       : true,
			width           : '100%',
			triggerAction   : 'all',
			lazyRender      : true,
			mode            : 'local', 
			forceSelection  : true,
			emptyText       : 'Pilih Dokter...',
			selectOnFocus   : true,
			align           : 'Right',
			hidden      	: hide_rawat,
			value           : DATA_ASKEP_IGD_AWAL.tindak_lanjut.konsultasi_1,
			listeners   : {
				select 	: function(a, b){
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
					tmpediting = 'true';
					DATA_ASKEP_IGD_AWAL.tindak_lanjut.konsultasi_1  = b.data.KD_DOKTER;
				}
			}
		},
		{
			xtype           : 'combo',
			fieldLabel      : '',
			id              : 'tindak_lanjut_rawat_konsultasi_dokter_2',
			store           : dsDokterKonsultasi_2,
			valueField      : 'KD_DOKTER',
			displayField    : 'NAMA',
			typeAhead       : true,
			width           : '100%',
			triggerAction   : 'all',
			lazyRender      : true,
			mode            : 'local', 
			forceSelection  : true,
			emptyText       : 'Pilih Dokter...',
			selectOnFocus   : true,
			align           : 'Right',
			hidden      	: hide_rawat,
			value           : DATA_ASKEP_IGD_AWAL.tindak_lanjut.konsultasi_2,
			listeners   : {
				select 	: function(a, b){
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
					tmpediting = 'true';
					DATA_ASKEP_IGD_AWAL.tindak_lanjut.konsultasi_2  = b.data.KD_DOKTER;
				}
			}
		},
		{
			xtype           : 'combo',
			fieldLabel      : '',
			id              : 'tindak_lanjut_rawat_konsultasi_dokter_3',
			store           : dsDokterKonsultasi_3,
			valueField      : 'KD_DOKTER',
			displayField    : 'NAMA',
			typeAhead       : true,
			width           : '100%',
			triggerAction   : 'all',
			lazyRender      : true,
			mode            : 'local', 
			forceSelection  : true,
			emptyText       : 'Pilih Dokter...',
			selectOnFocus   : true,
			align           : 'Right',
			hidden      	: hide_rawat,
			value           : DATA_ASKEP_IGD_AWAL.tindak_lanjut.konsultasi_3,
			listeners   : {
				select 	: function(a, b){
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
					tmpediting = 'true';
					DATA_ASKEP_IGD_AWAL.tindak_lanjut.konsultasi_3  = b.data.KD_DOKTER;
				}
			}
		},
		{
			xtype           : 'combo',
			fieldLabel      : '',
			id              : 'tindak_lanjut_rawat_konsultasi_dokter_4',
			store           : dsDokterKonsultasi_4,
			valueField      : 'KD_DOKTER',
			displayField    : 'NAMA',
			typeAhead       : true,
			width           : '100%',
			triggerAction   : 'all',
			lazyRender      : true,
			mode            : 'local', 
			forceSelection  : true,
			emptyText       : 'Pilih Dokter...',
			selectOnFocus   : true,
			align           : 'Right',
			hidden      	: hide_rawat,
			value           : DATA_ASKEP_IGD_AWAL.tindak_lanjut.konsultasi_4,
			listeners   : {
				select 	: function(a, b){
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
					tmpediting = 'true';
					DATA_ASKEP_IGD_AWAL.tindak_lanjut.konsultasi_4  = b.data.KD_DOKTER;
				}
			}
		},
		//
		{
            xtype       : 'checkboxgroup',
            columns     : 2,
            fieldLabel  : 'Pukul',
            id          : 'tindak_lanjut_rawat_waktu',
            hidden      : hide_rawat,
            items       : [
                {
                    boxLabel    : 'Via Telepon',
                    id          : 'check_tindak_lanjut_telepon',
                    name        : 'check_tindak_lanjut',
                    checked     : DATA_ASKEP_IGD_AWAL.tindak_lanjut.telepon,
                },{
                    boxLabel    : 'On Site',
                    id          : 'check_tindak_lanjut_on_site',
                    name        : 'check_tindak_lanjut',
                    checked     : DATA_ASKEP_IGD_AWAL.tindak_lanjut.on_site,
                },
            ],
            listeners   : {
                change: function (checkbox, newValue, oldValue, eOpts) {
                    Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                    tmpediting = 'true';
                    DATA_ASKEP_IGD_AWAL.tindak_lanjut.telepon       = false;
                    DATA_ASKEP_IGD_AWAL.tindak_lanjut.on_site       = false;

                    for (var i = 0; i < newValue.length; i++) {
                        if (newValue[i].id == 'check_tindak_lanjut_telepon') {
                            DATA_ASKEP_IGD_AWAL.tindak_lanjut.telepon = newValue[i].checked;
                        }

                        if (newValue[i].id == 'check_tindak_lanjut_on_site') {
                            DATA_ASKEP_IGD_AWAL.tindak_lanjut.on_site  = newValue[i].checked;
                        }
                    }
                },
            }
        },
		{
            xtype       : 'checkboxgroup',
            columns     : 3,
            fieldLabel  : 'Waktu Pulang',
            id          : 'tindak_lanjut_pulang_waktu',
            hidden      : hide_pulang,
            items       : [
               {
                    xtype       : 'datefield',
                    id          : 'txt_tanggal_pulang',
                    fieldLabel  : '',
                    format      : 'Y-m-d',
                    value       : DATA_ASKEP_IGD_AWAL.tindak_lanjut.tgl_pulang,
                    listeners   : {
                       'render': function (c) {
                           c.getEl().on('keypress', function (e) {
                               Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                               tmpediting = 'true';
                           }, c);
                               c.getEl().on('change', function (e) {
							    Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                               DATA_ASKEP_IGD_AWAL.tindak_lanjut.tgl_pulang = Ext.get('txt_tanggal_pulang').getValue();
							  
                           }, c);
                       },
					   change: function (t,n,o) {
							DATA_ASKEP_IGD_AWAL.tindak_lanjut.tgl_pulang = Ext.get('txt_tanggal_pulang').getValue();
							  
						},
                    }
                },
				{
					xtype		:'timefield',
					name		: 'textfieldJamPulang',
					id			: 'textfieldJamPulang',
					fieldLabel	: '&nbsp; Jam Plg',
					format		: 'H:i:s',
					value       : DATA_ASKEP_IGD_AWAL.tindak_lanjut.jam_pulang,
					listeners   : {
						'render': function (c) {
							c.getEl().on('keypress', function (e) {
								Ext.getCmp('btnSimpan_viDaftarAwal').enable();
								DATA_ASKEP_IGD_AWAL.tindak_lanjut.jam_pulang = Ext.get('textfieldJamPulang').getValue();
							}, c);
							c.getEl().on('change', function (e) {
								 Ext.getCmp('btnSimpan_viDaftarAwal').enable();
								DATA_ASKEP_IGD_AWAL.tindak_lanjut.jam_pulang = Ext.get('textfieldJamPulang').getValue();
							}, c);
						},
						change: function (t,n,o) {
							 Ext.getCmp('btnSimpan_viDaftarAwal').enable();
							DATA_ASKEP_IGD_AWAL.tindak_lanjut.jam_pulang = Ext.get('textfieldJamPulang').getValue();
						},
					}
				} 
				
            ],
            listeners   : {
                change: function (checkbox, newValue, oldValue, eOpts) {
                    Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                    tmpediting = 'true';
                },
            }
        },
		{
            xtype       : 'checkboxgroup',
            columns     : 3,
            fieldLabel  : 'Persetujuan',
            id          : 'tindak_lanjut_pulang_persetujuan',
            hidden      : hide_pulang,
            items       : [
                {
                    boxLabel    : 'Atas Persetujuan',
                    id          : 'check_tindak_lanjut_persetujuan',
                    name        : 'check_tindak_lanjut',
                    checked     : DATA_ASKEP_IGD_AWAL.tindak_lanjut.atas_persetujuan,
                },
				{
                    boxLabel    : 'Permintaan Sendiri',
                    id          : 'check_tindak_lanjut_permintaan_sendiri',
                    name        : 'check_tindak_lanjut',
                    checked     : DATA_ASKEP_IGD_AWAL.tindak_lanjut.persetujuan_diri,
                },
				//2019-04-18
				{
                    boxLabel    : 'Dead',
                    id          : 'check_tindak_lanjut_dead',
                    name        : 'check_tindak_lanjut',
                    checked     : DATA_ASKEP_IGD_AWAL.tindak_lanjut.dead,
                },
            ],
            listeners   : {
                change: function (checkbox, newValue, oldValue, eOpts) {
                    Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                    tmpediting = 'true';
                    DATA_ASKEP_IGD_AWAL.tindak_lanjut.atas_persetujuan = false;
                    DATA_ASKEP_IGD_AWAL.tindak_lanjut.persetujuan_diri = false;
                    DATA_ASKEP_IGD_AWAL.tindak_lanjut.dead = false; //2019-04-18

                    for (var i = 0; i < newValue.length; i++) {
                        if (newValue[i].id == 'check_tindak_lanjut_persetujuan') {
                            DATA_ASKEP_IGD_AWAL.tindak_lanjut.atas_persetujuan = newValue[i].checked;
                        }

                        if (newValue[i].id == 'check_tindak_lanjut_permintaan_sendiri') {
                            DATA_ASKEP_IGD_AWAL.tindak_lanjut.persetujuan_diri  = newValue[i].checked;
                        }
						
						//2019-04-18
						if (newValue[i].id == 'check_tindak_lanjut_dead') {
                            DATA_ASKEP_IGD_AWAL.tindak_lanjut.dead  = newValue[i].checked;
                        }
                    }
                },
            }
        },{
            xtype       : 'checkboxgroup',
            columns     : 3,
            fieldLabel  : 'Kontrol',
            id          : 'tindak_lanjut_pulang_kontrol',
            hidden      : hide_pulang,
            items       : [
                {
                    boxLabel    : 'Poli',
                    id          : 'check_tindak_lanjut_kontrol_poli',
                    name        : 'check_tindak_lanjut',
                    checked     : DATA_ASKEP_IGD_AWAL.tindak_lanjut.kontrol
                },{
                    xtype       : 'datefield',
                    id          : 'txt_tanggal_rujuk_poli_awal',
                    fieldLabel  : 'Tanggal',
                    format      : 'Y-m-d',
                    value       : DATA_ASKEP_IGD_AWAL.tindak_lanjut.tanggal,
                    listeners   : {
                       'render': function (c) {
                           c.getEl().on('keypress', function (e) {
                               Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                               tmpediting = 'true';
                           }, c);
                               c.getEl().on('change', function (e) {
                               DATA_ASKEP_IGD_AWAL.tindak_lanjut.tanggal = Ext.get('txt_tanggal_rujuk_poli_awal').getValue();
                           }, c);
                       },
					   //2019-04-30
					   change: function (t,n,o) {
							Ext.getCmp('btnSimpan_viDaftarAwal').enable();
							DATA_ASKEP_IGD_AWAL.tindak_lanjut.tanggal = Ext.get('txt_tanggal_rujuk_poli_awal').getValue();
						},
                    }
                },
				/* {
					xtype		:'timefield',
					name		: 'textfieldJamPulang',
					id			: 'textfieldJamPulang',
					fieldLabel	: '&nbsp; Jam Plg',
					format		: 'H:i:s',
					value       : DATA_ASKEP_IGD_AWAL.tindak_lanjut.jam_pulang,
					anchor		:'95%',
					listeners   : {
						'render': function (c) {
							c.getEl().on('keypress', function (e) {
								DATA_ASKEP_IGD_AWAL.tindak_lanjut.jam_pulang = Ext.get('textfieldJamPulang').getValue();
							}, c);
							c.getEl().on('change', function (e) {
								DATA_ASKEP_IGD_AWAL.tindak_lanjut.jam_pulang = Ext.get('textfieldJamPulang').getValue();
							}, c);
						},
						change: function (t,n,o) {
							DATA_ASKEP_IGD_AWAL.tindak_lanjut.jam_pulang = Ext.get('textfieldJamPulang').getValue();
						},
					}
				} */
				
            ],
            listeners   : {
                change: function (checkbox, newValue, oldValue, eOpts) {
                    Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                    tmpediting = 'true';
                    DATA_ASKEP_IGD_AWAL.tindak_lanjut.kontrol = false;

                    for (var i = 0; i < newValue.length; i++) {
                        if (newValue[i].id == 'check_tindak_lanjut_kontrol_poli') {
                            DATA_ASKEP_IGD_AWAL.tindak_lanjut.kontrol = newValue[i].checked;
                        }
                    }
                },
            }
        },{
            xtype       : 'textarea',
            fieldLabel  : 'Terapi Pulang',
            width       : '100%',
            id          : 'tindak_lanjut_pulang_terapi_pulang',
            hidden      : hide_pulang,
            value       : DATA_ASKEP_IGD_AWAL.tindak_lanjut.terapi_pulang,
            rows        : 4,
            listeners   : {
               'render': function (c) {
                   c.getEl().on('keypress', function (e) {
                       Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                       tmpediting = 'true';
                   }, c);
                       c.getEl().on('change', function (e) {
                       DATA_ASKEP_IGD_AWAL.tindak_lanjut.terapi_pulang = Ext.get('tindak_lanjut_pulang_terapi_pulang').getValue();
                   }, c);
               }
            }
        },{
            xtype       : 'checkboxgroup',
            columns     : 2,
            fieldLabel  : 'Edukasi kepada',
            id          : 'tindak_lanjut_pulang_edukasi',
            hidden      : hide_pulang,
            items       : [
                {
                    boxLabel    : 'Pasien',
                    id          : 'check_tindak_lanjut_edukasi_pasien',
                    name        : 'check_tindak_lanjut',
                    checked     : DATA_ASKEP_IGD_AWAL.tindak_lanjut.edukasi_pasien,
                },{
                    boxLabel    : 'Keluarga',
                    id          : 'check_tindak_lanjut_edukasi_keluarga',
                    name        : 'check_tindak_lanjut',
                    checked     : DATA_ASKEP_IGD_AWAL.tindak_lanjut.edukasi_keluarga,
                },{
                    boxLabel    : 'Tidak keduanya',
                    id          : 'check_tindak_lanjut_edukasi_tidak_keduanya',
                    name        : 'check_tindak_lanjut',
                    checked     : DATA_ASKEP_IGD_AWAL.tindak_lanjut.edukasi_tidak,
                },
            ],
            listeners   : {
                change: function (checkbox, newValue, oldValue, eOpts) {
                    Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                    tmpediting = 'true';
                    DATA_ASKEP_IGD_AWAL.tindak_lanjut.edukasi_pasien   = false;
                    DATA_ASKEP_IGD_AWAL.tindak_lanjut.edukasi_keluarga = false;
                    DATA_ASKEP_IGD_AWAL.tindak_lanjut.edukasi_tidak    = false;                    

                    for (var i = 0; i < newValue.length; i++) {
                        if (newValue[i].id == 'check_tindak_lanjut_edukasi_pasien') {
                            DATA_ASKEP_IGD_AWAL.tindak_lanjut.edukasi_pasien = newValue[i].checked;
                        }
                        if (newValue[i].id == 'check_tindak_lanjut_edukasi_keluarga') {
                            DATA_ASKEP_IGD_AWAL.tindak_lanjut.edukasi_keluarga  = newValue[i].checked;
                        }
                        if (newValue[i].id == 'check_tindak_lanjut_edukasi_tidak_keduanya') {
                            DATA_ASKEP_IGD_AWAL.tindak_lanjut.edukasi_tidak  = newValue[i].checked;
                        }
                    }
                },
            }
        },{
            xtype       : 'textfield',
            fieldLabel  : 'karena',
            width       : '100%',
            id          : 'tindak_lanjut_pulang_karena',
            hidden      : hide_pulang,
            value       : DATA_ASKEP_IGD_AWAL.tindak_lanjut.karena,
            listeners   : {
               'render': function (c) {
                   c.getEl().on('keypress', function (e) {
                       Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                       tmpediting = 'true';
                   }, c);
                       c.getEl().on('change', function (e) {
                       DATA_ASKEP_IGD_AWAL.tindak_lanjut.karena = Ext.get('tindak_lanjut_pulang_karena').getValue();
                   }, c);
               }
            }
        },
		//2019-04-18
		{
            xtype       : 'checkboxgroup',
            columns     : 3,
            fieldLabel  : 'Waktu Pulang',
            id          : 'tindak_lanjut_pulang_waktu_rujuk',
            hidden      : hide_rujuk,
            items       : [
               {
                    xtype       : 'datefield',
                    id          : 'txt_tanggal_pulang_rujuk',
                    fieldLabel  : '',
                    format      : 'Y-m-d',
                    value       : DATA_ASKEP_IGD_AWAL.tindak_lanjut.tgl_pulang,
                    listeners   : {
                       'render': function (c) {
                           c.getEl().on('keypress', function (e) {
                               Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                               tmpediting = 'true';
                           }, c);
                               c.getEl().on('change', function (e) {
							    Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                               DATA_ASKEP_IGD_AWAL.tindak_lanjut.tgl_pulang = Ext.get('txt_tanggal_pulang_rujuk').getValue();
							  
                           }, c);
                       },
					   change: function (t,n,o) {
							DATA_ASKEP_IGD_AWAL.tindak_lanjut.tgl_pulang = Ext.get('txt_tanggal_pulang_rujuk').getValue();
							  
						},
                    }
                },
				{
					xtype		:'timefield',
					name		: 'textfieldJamPulangRujuk',
					id			: 'textfieldJamPulangRujuk',
					fieldLabel	: '&nbsp; Jam Plg',
					format		: 'H:i:s',
					value       : DATA_ASKEP_IGD_AWAL.tindak_lanjut.jam_pulang,
					listeners   : {
						'render': function (c) {
							c.getEl().on('keypress', function (e) {
								Ext.getCmp('btnSimpan_viDaftarAwal').enable();
								DATA_ASKEP_IGD_AWAL.tindak_lanjut.jam_pulang = Ext.get('textfieldJamPulangRujuk').getValue();
							}, c);
							c.getEl().on('change', function (e) {
								 Ext.getCmp('btnSimpan_viDaftarAwal').enable();
								DATA_ASKEP_IGD_AWAL.tindak_lanjut.jam_pulang = Ext.get('textfieldJamPulangRujuk').getValue();
							}, c);
						},
						change: function (t,n,o) {
							 Ext.getCmp('btnSimpan_viDaftarAwal').enable();
							DATA_ASKEP_IGD_AWAL.tindak_lanjut.jam_pulang = Ext.get('textfieldJamPulangRujuk').getValue();
						},
					}
				} 
				
            ],
            listeners   : {
                change: function (checkbox, newValue, oldValue, eOpts) {
                    Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                    tmpediting = 'true';
                },
            }
        },
		//end 2019-04-18//
		{
            xtype       : 'textfield',
            xtype       : 'textfield',
            fieldLabel  : 'Dirujuk ke',
            width       : '100%',
            id          : 'tindak_lanjut_rujuk_kepada',
            hidden      : hide_rujuk,
            value       : DATA_ASKEP_IGD_AWAL.tindak_lanjut.dirujuk_ke,
            listeners   : {
               'render': function (c) {
                   c.getEl().on('keypress', function (e) {
                       Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                       tmpediting = 'true';
                   }, c);
                       c.getEl().on('change', function (e) {
                       DATA_ASKEP_IGD_AWAL.tindak_lanjut.dirujuk_ke = Ext.get('tindak_lanjut_rujuk_kepada').getValue();
                   }, c);
               }
            }
        },
		{
            xtype       : 'textarea',
            fieldLabel  : 'Alasan dirujuk',
            width       : '100%',
            id          : 'tindak_lanjut_rujuk_alasan',
            hidden      : hide_rujuk,
            value       : DATA_ASKEP_IGD_AWAL.tindak_lanjut.alasan_dirujuk,
            listeners   : {
               'render': function (c) {
                   c.getEl().on('keypress', function (e) {
                       Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                       tmpediting = 'true';
                   }, c);
                       c.getEl().on('change', function (e) {
                       DATA_ASKEP_IGD_AWAL.tindak_lanjut.alasan_dirujuk = Ext.get('tindak_lanjut_rujuk_alasan').getValue();
                   }, c);
               }
            }
        },
    ];
    return items;
};

function TabPanelPengkajian() {
    var items = {
        xtype: 'tabpanel',
        plain: true,
        activeTab: 0,
        height: 500,
        width: 1190,
        defaults:
                {
                    bodyStyle: 'padding:2px',
                    autoScroll: true
                },
        listeners: {
            tabchange: function (tab) {

            }
        },
        items: [
            {
                title: 'Rwy. Penyakit',
                layout: 'form',
                items:
                        [
                            riwayatpenyakit()
                        ]
            },
            {
                title: "Airway'n B.",
                layout: 'form',
                items:
                        [
                            Airway()
                        ]
            },
            {
                title: 'Circulation',
                layout: 'form',
                items:
                        [
                            Circulation()
                        ]
            },
            {
                title: 'Musculo',
                layout: 'form',
                items:
                        [
                            Musculo()
                        ]
            },
            {
                title: 'Saraf',
                layout: 'form',
                items:
                        [
                            Saraf()
                        ]
            },
            {
                title: 'Pengindraan',
                layout: 'form',
                items:
                        [
                            Pengindraan()
                        ]
            },
            {
                title: 'Perkemihan',
                layout: 'form',
                items:
                        [
                            Perkemihan()
                        ]
            },
            {
                title: 'Pencernaan',
                layout: 'form',
                items:
                        [
                            Pencernaan()
                        ]
            },
            {
                title: 'Endokrin',
                layout: 'form',
                items:
                        [
                            Endokrin()
                        ]
            },
            {
                title: 'P.Hygiene',
                layout: 'form',
                items:
                        [
                            PHygiene()
                        ]
            },
            {
                title: 'Psy.Sos.',
                layout: 'form',
                items:
                        [
                            PsySos()
                        ]
            },
            {
                title: 'Penunjang',
                layout: 'form',
                items:
                        [
                            Penunjang()
                        ]
            }
        ]
    };
    return items;
}
;
function riwayatpenyakit()
{
    var tabDetailriwayatpenyakit = new Ext.Panel
            (
                    {
                        title: 'Riwayat Penyakit',
                        id: 'tabDetailriwayatpenyakit',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        bodyStyle: 'padding:5px 5px 5px 5px',
                        border: true,
                        autoscroll: true,
                        items: [
                            {
                                xtype: 'textarea',
                                fieldLabel: 'Keluhan Utama',
                                name: 'txtkeluhanutama',
                                id: 'txtkeluhanutama',
                                height: 50,
                                width: 855,
                                enableKeyEvents: true,
                                listeners:
                                        {
                                            'render': function (c) {
                                                c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                                        Ext.getCmp('txtriwayatpenyakitsekarang').focus();
                                                }, c);
                                                c.getEl().on('change', function (e) {
                                                    Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                    tmpediting = 'true';
                                                    RIWAYAT_UTAMA = Ext.get('txtkeluhanutama').getValue();
                                                }, c);
                                            }
                                        }
                            },
                            {
                                xtype: 'textarea',
                                fieldLabel: 'Riwayat Penyakit Sekarang',
                                name: 'txtriwayatpenyakitsekarang',
                                id: 'txtriwayatpenyakitsekarang',
                                height: 50,
                                width: 855,
                                enableKeyEvents: true,
                                listeners:
                                        {
                                            'render': function (c) {
                                                c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                                        Ext.getCmp('txtriwayatpenyakitTerdahulu').focus();
                                                }, c);
                                                c.getEl().on('change', function (e) {
                                                    Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                    tmpediting = 'true';
                                                    RIWAYAT_PENYAKIT_SEKARANG = Ext.get('txtriwayatpenyakitsekarang').getValue();
                                                }, c);
                                            }
                                        }

                            },
                            {
                                xtype: 'textarea',
                                fieldLabel: 'Riwayat Penyakit Terdahulu',
                                name: 'txtriwayatpenyakitTerdahulu',
                                id: 'txtriwayatpenyakitTerdahulu',
                                height: 224,
                                width: 855,
                                listeners:
                                        {
                                            'render': function (c) {
                                                c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                                        console.log(MainTabs);
                                                }, c);
                                                c.getEl().on('change', function (e) {
                                                    Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                    tmpediting = 'true';
                                                    RIWAYAT_PENYAKIT_DAHULU = Ext.get('txtriwayatpenyakitTerdahulu').getValue();
                                                }, c);
                                            }
                                        }
                            }
                        ]
                    }
            );
    return tabDetailriwayatpenyakit;
}
;
function Airway()
{
    var tabAirway = new Ext.Panel
            (
                    {
                        title: "Airways'n Breathing",
                        id: 'tabAirway',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        bodyStyle: 'padding:5px 5px 5px 5px',
                        border: true,
                        autoscroll: true,
                        items: [
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Jalan Napas '
                                            },
                                            {
                                                x: 125,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 135,
                                                y: 10,
                                                xtype: 'checkboxgroup',
                                                id: 'cgjelannapas',
                                                itemCls: 'x-check-group-alt',
                                                columns: 2,
                                                boxMaxWidth: 150,
                                                items: [
                                                    {xtype: 'checkbox', boxLabel: 'Paten', name: 'cbjlpaten', id: 'cbjlpaten'},
                                                    {xtype: 'checkbox', boxLabel: 'Obtruksif', name: 'cbjlobtruksif', id: 'cbjlobtruksif'}
                                                ],
                                                listeners: {
                                                    change: function (checkbox, newValue, oldValue, eOpts) {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('cbjlpaten').checked === true)
                                                        {
                                                            NAFAS_PATEN = 'true'
                                                        } else
                                                        {
                                                            NAFAS_PATEN = 'false'
                                                        }
                                                        if (Ext.getCmp('cbjlobtruksif').checked === true)
                                                        {
                                                            NAFAS_OBSTRUKTIF = 'true'
                                                        } else
                                                        {
                                                            NAFAS_OBSTRUKTIF = 'false'
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Jelas '
                                            },
                                            {
                                                x: 125,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 135,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgjelas',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: 'Ya',
                                                        name: 'rbjelasya',
                                                        inputValue: 1,
                                                        id: 'rbjelasya'
                                                    },
                                                    {
                                                        boxLabel: 'Tidak',
                                                        name: 'rbjelasya',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rbjelastidak'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rbjelasya').checked === true)
                                                        {
                                                            NAFAS_JELAS = 'true';
                                                        } else {
                                                            NAFAS_JELAS = 'false';
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                x: 300,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Pola Napas '
                                            },
                                            {
                                                x: 360,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 370,
                                                y: 10,
                                                xtype: 'checkboxgroup',
                                                id: 'cgpolanapas',
                                                columns: 2,
                                                boxMaxWidth: 150,
                                                vertical: true,
                                                items: [
                                                    {xtype: 'checkbox', boxLabel: 'Simetri', name: 'cbjlSimetri', id: 'cbjlSimetri'},
                                                    {xtype: 'checkbox', boxLabel: 'Asimetri', name: 'cbjlAsimetri', id: 'cbjlAsimetri'}
                                                ],
                                                listeners: {
                                                    change: function (checkbox, newValue, oldValue, eOpts) {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('cbjlSimetri').checked === true)
                                                        {
                                                            NAFAS_POLA_SIMETRI = 'true'
                                                        } else
                                                        {
                                                            NAFAS_POLA_SIMETRI = 'false'
                                                        }
                                                        if (Ext.getCmp('cbjlAsimetri').checked === true)
                                                        {
                                                            NAFAS_POLA_ASIMETRI = 'true'
                                                        } else
                                                        {
                                                            NAFAS_POLA_ASIMETRI = 'false'
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Suara Lapang Paru '
                                            },
                                            {
                                                x: 125,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 135,
                                                y: 10,
                                                xtype: 'checkboxgroup',
                                                id: 'cgsuaralapangparu',
                                                columns: 3,
                                                boxMaxWidth: 300,
                                                vertical: true,
                                                items: [
                                                    {xtype: 'checkbox', boxLabel: 'Normal/Sonor', name: 'cbjlNormal', id: 'cbjlNormal'},
                                                    {xtype: 'checkbox', boxLabel: 'HyperSonor', name: 'cbjlHyper', id: 'cbjlHyper'},
                                                    {xtype: 'checkbox', boxLabel: 'Menurun', name: 'cbjlMenurun', id: 'cbjlMenurun'}
                                                ],
                                                listeners: {
                                                    change: function (checkbox, newValue, oldValue, eOpts) {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('cbjlNormal').checked === true)
                                                        {
                                                            NAFAS_SUARA_NORMAL = 'true'
                                                        } else
                                                        {
                                                            NAFAS_SUARA_NORMAL = 'false'
                                                        }
                                                        if (Ext.getCmp('cbjlHyper').checked === true)
                                                        {
                                                            NAFAS_SUARA_HIPERSONOR = 'true'
                                                        } else
                                                        {
                                                            NAFAS_SUARA_HIPERSONOR = 'false'
                                                        }
                                                        if (Ext.getCmp('cbjlMenurun').checked === true)
                                                        {
                                                            NAFAS_MENURUN = 'true'
                                                        } else
                                                        {
                                                            NAFAS_MENURUN = 'false'
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 60,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Jenis '
                                            },
                                            {
                                                x: 125,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 135,
                                                y: 10,
                                                xtype: 'checkboxgroup',
                                                id: 'cgjenis',
                                                columns: 4,
                                                boxMaxWidth: 600,
                                                vertical: true,
                                                items: [
                                                    {xtype: 'checkbox', boxLabel: 'Normal', name: 'cbjlJenisNormal', id: 'cbjlJenisNormal'},
                                                    {xtype: 'checkbox', boxLabel: 'Tachypnoe', name: 'cbjlJenisTachypnoe', id: 'cbjlJenisTachypnoe'},
                                                    {xtype: 'checkbox', boxLabel: 'Cheyne Stokes', name: 'cbjlJenisCheyneStokes', id: 'cbjlJenisCheyneStokes'},
                                                    {xtype: 'checkbox', boxLabel: 'Retractive', name: 'cbjlJenisRetractive', id: 'cbjlJenisRetractive'},
                                                    {xtype: 'checkbox', boxLabel: 'Kusmaul', name: 'cbjlJenisKusmaul', id: 'cbjlJenisKusmaul'},
                                                    {xtype: 'checkbox', boxLabel: 'Dispone', name: 'cbjlJenisDispone', id: 'cbjlJenisDispone'}
                                                ],
                                                listeners: {
                                                    change: function (checkbox, newValue, oldValue, eOpts) {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('cbjlJenisNormal').checked === true)
                                                        {
                                                            NAFAS_JENIS_NORMAL = 'true'
                                                        } else
                                                        {
                                                            NAFAS_JENIS_NORMAL = 'false'
                                                        }
                                                        if (Ext.getCmp('cbjlJenisTachypnoe').checked === true)
                                                        {
                                                            NAFAS_JENIS_TACHYPNOE = 'true'
                                                        } else
                                                        {
                                                            NAFAS_JENIS_TACHYPNOE = 'false'
                                                        }
                                                        if (Ext.getCmp('cbjlJenisCheyneStokes').checked === true)
                                                        {
                                                            NAFAS_JENIS_CHEYNESTOKES = 'true'
                                                        } else
                                                        {
                                                            NAFAS_JENIS_CHEYNESTOKES = 'false'
                                                        }
                                                        if (Ext.getCmp('cbjlJenisRetractive').checked === true)
                                                        {
                                                            NAFAS_JENIS_RETRACTIVE = 'true'
                                                        } else
                                                        {
                                                            NAFAS_JENIS_RETRACTIVE = 'false'
                                                        }
                                                        if (Ext.getCmp('cbjlJenisKusmaul').checked === true)
                                                        {
                                                            NAFAS_JENIS_KUSMAUL = 'true'
                                                        } else
                                                        {
                                                            NAFAS_JENIS_KUSMAUL = 'false'
                                                        }
                                                        if (Ext.getCmp('cbjlJenisDispone').checked === true)
                                                        {
                                                            NAFAS_JENIS_DISPNOE = 'true'
                                                        } else
                                                        {
                                                            NAFAS_JENIS_DISPNOE = 'false'
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                x: 520,
                                                y: 35,
                                                xtype: 'label',
                                                text: 'RR(x/mnt) '
                                            },
                                            {
                                                x: 580,
                                                y: 30,
                                                xtype: 'textfield',
                                                id: 'txtJenisRR',
                                                width: 100,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        NAFAS_RR = Ext.get('txtJenisRR').getValue();
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Suara Nafas Tambahan '
                                            },
                                            {
                                                x: 125,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 135,
                                                y: 10,
                                                xtype: 'checkboxgroup',
                                                id: 'cgsuaranafastambahan',
                                                columns: 3,
                                                boxMaxWidth: 300,
                                                vertical: true,
                                                items: [
                                                    {xtype: 'checkbox', boxLabel: 'Wheezing', name: 'cbjlsntWheezing', id: 'cbjlsntWheezing'},
                                                    {xtype: 'checkbox', boxLabel: 'Ronchi', name: 'cbjlsntRonchi', id: 'cbjlsntRonchi'},
                                                    {xtype: 'checkbox', boxLabel: 'Rales', name: 'cbjlsntRales', id: 'cbjlsntRales'}
                                                ],
                                                listeners: {
                                                    change: function (checkbox, newValue, oldValue, eOpts) {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('cbjlsntWheezing').checked === true)
                                                        {
                                                            NAFAS_SUARA_WHEEZING = 'true'
                                                        } else
                                                        {
                                                            NAFAS_SUARA_WHEEZING = 'false'
                                                        }
                                                        if (Ext.getCmp('cbjlsntRonchi').checked === true)
                                                        {
                                                            NAFAS_SUARA_RONCHI = 'true'
                                                        } else
                                                        {
                                                            NAFAS_SUARA_RONCHI = 'false'
                                                        }
                                                        if (Ext.getCmp('cbjlsntRales').checked === true)
                                                        {
                                                            NAFAS_SUARA_RALES = 'true'
                                                        } else
                                                        {
                                                            NAFAS_SUARA_RALES = 'false'
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Efakuasi Cairan '
                                            },
                                            {
                                                x: 125,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 135,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgefakuasi',
                                                boxMaxWidth: 200,
                                                items: [
                                                    {boxLabel: 'Ya', name: 'rbefakuasi', inputValue: 1, id: 'rbefakuasiya'},
                                                    {boxLabel: 'Tidak', name: 'rbefakuasi', inputValue: 2, checked: true, id: 'rbefakuasitidak'}
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        if (Ext.getCmp('rbefakuasiya').checked === true)
                                                        {
                                                            NAFAS_EVAKUASI_CAIRAN = 'true';
                                                        } else {
                                                            NAFAS_EVAKUASI_CAIRAN = 'false';
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                x: 335,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Jumlah(cc) '
                                            },
                                            {
                                                x: 390,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 400,
                                                y: 5,
                                                xtype: 'textfield',
                                                id: 'txtjumlahcairan',
                                                width: 100,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        NAFAS_JML_CAIRAN = Ext.get('txtjumlahcairan').getValue();
                                                    }
                                                }
                                            },
                                            {
                                                x: 520,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Warna '
                                            },
                                            {
                                                x: 570,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 580,
                                                y: 5,
                                                xtype: 'textfield',
                                                id: 'txtwarnacairan',
                                                width: 100,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        NAFAS_WARNA_CAIRAN = Ext.get('txtwarnacairan').getValue();
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 124,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Masalah '
                                            },
                                            {
                                                x: 125,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 135,
                                                y: 10,
                                                xtype: 'textarea',
                                                name: 'txtmasalahairway',
                                                id: 'txtmasalahairway',
                                                height: 111,
                                                width: 822,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        NAFAS_MASALAH = Ext.get('txtmasalahairway').getValue();
                                                    }
                                                }
                                            }
                                        ]
                            }
                        ]
                    }
            );
    return tabAirway;
}
;
function Circulation()
{
    var tabCirculation = new Ext.Panel
            (
                    {
                        title: "Circulation",
                        id: 'tabCirculation',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        bodyStyle: 'padding:5px 5px 5px 5px',
                        border: true,
                        autoscroll: true,
                        items: [
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Irama Jantung '
                                            },
                                            {
                                                x: 125,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 135,
                                                y: 10,
                                                xtype: 'checkboxgroup',
                                                id: 'cgIramaJantung',
                                                columns: 2,
                                                boxMaxWidth: 150,
                                                vertical: true,
                                                items: [
                                                    {boxLabel: 'Reguler', name: 'cgIRReguler', id: 'cgIRReguler'},
                                                    {boxLabel: 'Irreguler', name: 'cgIRIrreguler', id: 'cgIRIrreguler'}
                                                ],
                                                listeners: {
                                                    change: function (checkbox, newValue, oldValue, eOpts) {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('cgIRReguler').checked === true)
                                                        {
                                                            JANTUNG_REGULER = 'true';
                                                        } else
                                                        {
                                                            JANTUNG_REGULER = 'false';
                                                        }
                                                        if (Ext.getCmp('cgIRIrreguler').checked === true)
                                                        {
                                                            JANTUNG_IRREGULER = 'true';
                                                        } else
                                                        {
                                                            JANTUNG_IRREGULER = 'false';
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                x: 300,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'S1/S2 Tunggal '
                                            },
                                            {
                                                x: 380,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 400,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgS1S2Tunggal',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {boxLabel: 'Ya', name: 'rgS1S2Tunggal', inputValue: 1, id: 'rbS1S2Tunggalya'},
                                                    {boxLabel: 'Tidak', name: 'rgS1S2Tunggal', inputValue: 2, checked: true, id: 'rbS1S2Tunggaltidak'}
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rbS1S2Tunggalya').checked === true)
                                                        {
                                                            JANTUNG_S1S2_TUNGGAL = 'true';
                                                        } else {
                                                            JANTUNG_S1S2_TUNGGAL = 'false';
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Nyeri Dada '
                                            },
                                            {
                                                x: 125,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 135,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgNyeridada',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: 'Ya',
                                                        name: 'rgNyeridada',
                                                        inputValue: 1,
                                                        id: 'rgNyeridadaya'
                                                    },
                                                    {
                                                        boxLabel: 'Tidak',
                                                        name: 'rgNyeridada',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rgNyeridadatidak'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rgNyeridadaya').checked === true)
                                                        {
                                                            JANTUNG_NYERI_DADA = 'true';
                                                        } else {
                                                            JANTUNG_NYERI_DADA = 'false';
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                x: 300,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Jelaskan '
                                            },
                                            {
                                                x: 380,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 400,
                                                y: 5,
                                                xtype: 'textfield',
                                                name: 'txtpenjelasannyeridada',
                                                id: 'txtpenjelasannyeridada',
                                                width: 560,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        JANTUNG_NYERI_DADA_KET = Ext.get('txtpenjelasannyeridada').getValue();
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Bunyi Jantung Tambahan '
                                            },
                                            {
                                                x: 125,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 135,
                                                y: 10,
                                                xtype: 'checkboxgroup',
                                                id: 'cgBunyiJantungTambahan',
                                                columns: 3,
                                                boxMaxWidth: 225,
                                                vertical: true,
                                                items: [
                                                    {boxLabel: 'Murmur', name: 'cgBunyiJantungTambahanMurmur', id: 'cgBunyiJantungTambahanMurmur'},
                                                    {boxLabel: 'Gallop', name: 'cgBunyiJantungTambahanGallop', id: 'cgBunyiJantungTambahanGallop'},
                                                    {boxLabel: 'Bising Sitolik', name: 'cgBunyiJantungTambahanBising', id: 'cgBunyiJantungTambahanBising'}
                                                ],
                                                listeners: {
                                                    change: function (checkbox, newValue, oldValue, eOpts) {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('cgBunyiJantungTambahanMurmur').checked === true)
                                                        {
                                                            JANTUNG_BUNYI_MURMUR = 'true'
                                                        } else
                                                        {
                                                            JANTUNG_BUNYI_MURMUR = 'false'
                                                        }
                                                        if (Ext.getCmp('cgBunyiJantungTambahanGallop').checked === true)
                                                        {
                                                            JANTUNG_BUNYI_GALLOP = 'true'
                                                        } else
                                                        {
                                                            JANTUNG_BUNYI_GALLOP = 'false'
                                                        }
                                                        if (Ext.getCmp('cgBunyiJantungTambahanBising').checked === true)
                                                        {
                                                            JANTUNG_BUNYI_BISING = 'true'
                                                        } else
                                                        {
                                                            JANTUNG_BUNYI_BISING = 'false'
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'CRT '
                                            },
                                            {
                                                x: 125,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 135,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgCRT',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: '<3 Dtk',
                                                        name: 'rgCRT',
                                                        inputValue: 1,
                                                        id: 'rgCRTkurang'
                                                    },
                                                    {
                                                        boxLabel: '>3 Dtk',
                                                        name: 'rgCRT',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rgCRTlebih'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rgCRTkurang').checked === true)
                                                        {
                                                            JANTUNG_CRT = 'true';
                                                        } else {
                                                            JANTUNG_CRT = 'false';
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                x: 300,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Akral '
                                            },
                                            {
                                                x: 380,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 400,
                                                y: 10,
                                                xtype: 'checkboxgroup',
                                                id: 'cgAkral',
                                                columns: 3,
                                                boxMaxWidth: 225,
                                                vertical: true,
                                                items: [
                                                    {boxLabel: 'Hangat', name: 'cgAkralHangat', id: 'cgAkralHangat'},
                                                    {boxLabel: 'Dingin', name: 'cgAkralDingin', id: 'cgAkralDingin'}
                                                ],
                                                listeners: {
                                                    change: function (checkbox, newValue, oldValue, eOpts) {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('cgAkralHangat').checked === true)
                                                        {
                                                            JANTUNG_AKRAL_HANGAT = 'true'
                                                        } else
                                                        {
                                                            JANTUNG_AKRAL_HANGAT = 'false'
                                                        }
                                                        if (Ext.getCmp('cgAkralDingin').checked === true)
                                                        {
                                                            JANTUNG_AKRAL_DINGIN = 'true'
                                                        } else
                                                        {
                                                            JANTUNG_AKRAL_DINGIN = 'false'
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Peningkatan JVP '
                                            },
                                            {
                                                x: 125,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 135,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgJVP',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: 'Ya',
                                                        name: 'rgJVP',
                                                        inputValue: 1,
                                                        id: 'rgJVPya'
                                                    },
                                                    {
                                                        boxLabel: 'Tidak',
                                                        name: 'rgJVP',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rgJVPtidak'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rgJVPya').checked === true)
                                                        {
                                                            JANTUNG_PENINGKATAN_JVP = 'true';
                                                        } else {
                                                            JANTUNG_PENINGKATAN_JVP = 'false';
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                x: 300,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Ukuran '
                                            },
                                            {
                                                x: 380,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 400,
                                                y: 5,
                                                xtype: 'textfield',
                                                id: 'txtukuranJVP',
                                                Width: 150,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        JANTUNG_UKURAN_CVP = Ext.get('txtukuranJVP').getValue();
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Warna Kulit '
                                            },
                                            {
                                                x: 125,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 135,
                                                y: 10,
                                                xtype: 'checkboxgroup',
                                                id: 'cgWarnaKulit',
                                                columns: 4,
                                                boxMaxWidth: 325,
                                                vertical: true,
                                                items: [
                                                    {boxLabel: 'Jaudice', name: 'cgWarnaKulitJaudice', id: 'cgWarnaKulitJaudice'},
                                                    {boxLabel: 'Sianotik', name: 'cgWarnaKulitSianotik', id: 'cgWarnaKulitSianotik'},
                                                    {boxLabel: 'Kemerahan', name: 'cgWarnaKulitKemerahan', id: 'cgWarnaKulitKemerahan'},
                                                    {boxLabel: 'Pucat', name: 'cgWarnaKulitPucat', id: 'cgWarnaKulitPucat'}
                                                ],
                                                listeners: {
                                                    change: function (checkbox, newValue, oldValue, eOpts) {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('cgWarnaKulitJaudice').checked === true)
                                                        {
                                                            JANTUNG_WARNA_JAUNDICE = 'true';
                                                        } else
                                                        {
                                                            JANTUNG_WARNA_JAUNDICE = 'false';
                                                        }
                                                        if (Ext.getCmp('cgWarnaKulitSianotik').checked === true)
                                                        {
                                                            JANTUNG_WARNA_SIANOTIK = 'true';
                                                        } else
                                                        {
                                                            JANTUNG_WARNA_SIANOTIK = 'false';
                                                        }
                                                        if (Ext.getCmp('cgWarnaKulitKemerahan').checked === true)
                                                        {
                                                            JANTUNG_WARNA_KEMERAHAN = 'true';
                                                        } else
                                                        {
                                                            JANTUNG_WARNA_KEMERAHAN = 'false';
                                                        }
                                                        if (Ext.getCmp('cgWarnaKulitPucat').checked === true)
                                                        {
                                                            JANTUNG_WARNA_PUCAT = 'true';
                                                        } else
                                                        {
                                                            JANTUNG_WARNA_PUCAT = 'false';
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Tekanan Darah(mmHg) '
                                            },
                                            {
                                                x: 125,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 135,
                                                y: 5,
                                                xtype: 'textfield',
                                                name: 'txttekanandarah',
                                                id: 'txttekanandarah',
                                                width: 72,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        JANTUNG_TEKANAN_DARAH = Ext.get('txttekanandarah').getValue();
                                                    }
                                                }
                                            },
                                            {
                                                x: 215,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Nadi '
                                            },
                                            {
                                                x: 250,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 270,
                                                y: 5,
                                                xtype: 'textfield',
                                                id: 'txtNadi',
                                                name: 'txtNadi',
                                                width: 72,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        JANTUNG_NADI = Ext.get('txtNadi').getValue();
                                                    }
                                                }
                                            },
                                            {
                                                x: 350,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Temp(CelSius) '
                                            },
                                            {
                                                x: 430,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 450,
                                                y: 5,
                                                xtype: 'textfield',
                                                name: 'txtTemp',
                                                id: 'txtTemp',
                                                width: 72,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        JANTUNG_TEMP = Ext.get('txtTemp').getValue();
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 124,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Masalah '
                                            },
                                            {
                                                x: 125,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 135,
                                                y: 10,
                                                xtype: 'textarea',
                                                name: 'txtmasalahCirculation',
                                                id: 'txtmasalahCirculation',
                                                height: 111,
                                                width: 822,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        JANTUNG_MASALAH = Ext.get('txtmasalahCirculation').getValue();
                                                    }
                                                }
                                            }
                                        ]
                            }
                        ]
                    }
            );
    return tabCirculation;
}
;
function Musculo()
{
    var tabMusculo = new Ext.Panel
            (
                    {
                        title: "Musculo",
                        id: 'tabMusculo',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        bodyStyle: 'padding:5px 5px 5px 5px',
                        border: true,
                        autoscroll: true,
                        items: [
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Kemampuan Pergerakan Sendi '
                                            },
                                            {
                                                x: 160,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 180,
                                                y: 10,
                                                xtype: 'checkboxgroup',
                                                id: 'cgPergerakanSendi',
                                                columns: 2,
                                                boxMaxWidth: 150,
                                                vertical: true,
                                                items: [
                                                    {boxLabel: 'Bebas', name: 'cgPergerakanSendiBebas', id: 'cgPergerakanSendiBebas'},
                                                    {boxLabel: 'Terbatas', name: 'cgPergerakanSendiTerbatas', id: 'cgPergerakanSendiTerbatas'}
                                                ],
                                                listeners: {
                                                    change: function (checkbox, newValue, oldValue, eOpts) {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('cgPergerakanSendiBebas').checked === true)
                                                        {
                                                            OTOT_SENDI_BEBAS = 'true';
                                                        } else
                                                        {
                                                            OTOT_SENDI_BEBAS = 'false';
                                                        }
                                                        if (Ext.getCmp('cgPergerakanSendiTerbatas').checked === true)
                                                        {
                                                            OTOT_SENDI_TERBATAS = 'true';
                                                        } else
                                                        {
                                                            OTOT_SENDI_TERBATAS = 'false';
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Kekuatan Otot '
                                            },
                                            {
                                                x: 160,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 180,
                                                y: 5,
                                                xtype: 'textfield',
                                                id: 'txtkekuatanotot',
                                                width: 780,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        OTOT_KEKUATAN = Ext.get('txtkekuatanotot').getValue();
                                                    }
                                                }
                                            }]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Odema ekstrimitas '
                                            },
                                            {
                                                x: 160,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 180,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgekstrimitas',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: 'Ya',
                                                        name: 'rgekstrimitas',
                                                        inputValue: 1,
                                                        id: 'rgekstrimitasya'
                                                    },
                                                    {
                                                        boxLabel: 'Tidak',
                                                        name: 'rgekstrimitas',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rgekstrimitastidak'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rgekstrimitasya').checked === true)
                                                        {
                                                            OTOT_ODEMA_EKSTRIMITAS = 'true';
                                                        } else {
                                                            OTOT_ODEMA_EKSTRIMITAS = 'false';
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Kelainan Bentuk '
                                            },
                                            {
                                                x: 160,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 180,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgKelainan',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: 'Ya',
                                                        name: 'rgKelainan',
                                                        inputValue: 1,
                                                        id: 'rgKelainanya'
                                                    },
                                                    {
                                                        boxLabel: 'Tidak',
                                                        name: 'rgKelainan',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rgKelainantidak'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rgKelainanya').checked === true)
                                                        {
                                                            OTOT_KELAINAN_BENTUK = 'true';
                                                        } else {
                                                            OTOT_KELAINAN_BENTUK = 'false';
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                x: 320,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Krepitas '
                                            },
                                            {
                                                x: 380,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 400,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgKrepitas',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: 'Ya',
                                                        name: 'rgKrepitas',
                                                        inputValue: 1,
                                                        id: 'rgKrepitasya'
                                                    },
                                                    {
                                                        boxLabel: 'Tidak',
                                                        name: 'rgKrepitas',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rgKrepitastidak'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rgKrepitasya').checked === true)
                                                        {
                                                            OTOT_KREPITASI = 'true';
                                                        } else {
                                                            OTOT_KREPITASI = 'false';
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 216,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Masalah '
                                            },
                                            {
                                                x: 160,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 180,
                                                y: 10,
                                                xtype: 'textarea',
                                                name: 'txtmasalahMusculo',
                                                id: 'txtmasalahMusculo',
                                                height: 206,
                                                width: 780,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        OTOT_MASALAH = Ext.get('txtmasalahMusculo').getValue();
                                                    }
                                                }
                                            }]
                            },
                        ]
                    }
            );
    return tabMusculo;
}
;
function Saraf()
{
    var tabSaraf = new Ext.Panel
            ({title: "Saraf",
                id: 'tabSaraf',
                fileUpload: true,
                region: 'north',
                layout: 'form',
                bodyStyle: 'padding:5px 5px 5px 5px',
                border: true,
                autoscroll: true,
                items: [
                    {
                        columnWidth: .20,
                        layout: 'absolute',
                        labelWidth: 100,
                        border: false,
                        height: 30,
                        items:
                                [
                                    {
                                        x: 10,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'GCS '
                                    },
                                    {
                                        x: 125,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 135,
                                        y: 10,
                                        xtype: 'checkboxgroup',
                                        id: 'cgGCS',
                                        columns: 3,
                                        boxMaxWidth: 225,
                                        vertical: true,
                                        items: [
                                            {boxLabel: 'Eye', name: 'cgGCSEye', id: 'cgGCSEye'},
                                            {boxLabel: 'Verbal', name: 'cgGCSVerbal', id: 'cgGCSVerbal'},
                                            {boxLabel: 'Motorik', name: 'cgGCSMotorik', id: 'cgGCSMotorik'}
                                        ],
                                        listeners: {
                                            change: function (checkbox, newValue, oldValue, eOpts) {
                                                Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                tmpediting = 'true';
                                                if (Ext.getCmp('cgGCSEye').checked === true)
                                                {
                                                    SYARAF_GCS_EYE = 'true';
                                                } else
                                                {
                                                    SYARAF_GCS_EYE = 'false';
                                                }
                                                if (Ext.getCmp('cgGCSVerbal').checked === true)
                                                {
                                                    SYARAF_GCS_VERBAL = 'true';
                                                } else
                                                {
                                                    SYARAF_GCS_VERBAL = 'false';
                                                }
                                                if (Ext.getCmp('cgGCSMotorik').checked === true)
                                                {
                                                    SYARAF_GCS_MOTORIK = 'true';
                                                } else
                                                {
                                                    SYARAF_GCS_MOTORIK = 'false';
                                                }
                                            }
                                        }
                                    },
                                    {
                                        x: 400,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'Total '
                                    },
                                    {
                                        x: 430,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 450,
                                        y: 5,
                                        xtype: 'textfield',
                                        id: 'txttotalGCS',
                                        width: 100,
                                        listeners: {
                                            change: function () {
                                                Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                tmpediting = 'true';
                                                SYARAF_GCS_TOTAL = Ext.get('txttotalGCS').getValue();
                                            }
                                        }
                                    }]
                    },
                    {
                        columnWidth: .20,
                        layout: 'absolute',
                        labelWidth: 100,
                        border: false,
                        height: 30,
                        items:
                                [
                                    {
                                        x: 10,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'Refleksi Fisiologis '
                                    },
                                    {
                                        x: 125,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 135,
                                        y: 10,
                                        xtype: 'checkboxgroup',
                                        id: 'cgRefleksi',
                                        columns: 3,
                                        boxMaxWidth: 225,
                                        vertical: true,
                                        items: [
                                            {boxLabel: 'Brachialis', name: 'cgcgRefleksiBrachialis', id: 'cgcgRefleksiBrachialis'},
                                            {boxLabel: 'Patella', name: 'cgcgRefleksiPatella', id: 'cgcgRefleksiPatella'},
                                            {boxLabel: 'Achilla', name: 'cgcgRefleksiAchilla', id: 'cgcgRefleksiAchilla'}
                                        ],
                                        listeners: {
                                            change: function (checkbox, newValue, oldValue, eOpts) {
                                                Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                tmpediting = 'true';
                                                if (Ext.getCmp('cgcgRefleksiBrachialis').checked === true)
                                                {
                                                    SYARAF_FISIOLOGIS_BRACHIALIS = 'true';
                                                } else
                                                {
                                                    SYARAF_FISIOLOGIS_BRACHIALIS = 'false';
                                                }
                                                if (Ext.getCmp('cgcgRefleksiPatella').checked === true)
                                                {
                                                    SYARAF_FISIOLOGIS_PATELLA = 'true';
                                                } else
                                                {
                                                    SYARAF_FISIOLOGIS_PATELLA = 'false';
                                                }
                                                if (Ext.getCmp('cgcgRefleksiAchilla').checked === true)
                                                {
                                                    SYARAF_FISIOLOGIS_ACHILLES = 'true';
                                                } else
                                                {
                                                    SYARAF_FISIOLOGIS_ACHILLES = 'false';
                                                }
                                            }
                                        }
                                    }
                                ]
                    },
                    {
                        columnWidth: .20,
                        layout: 'absolute',
                        labelWidth: 100,
                        border: false,
                        height: 30,
                        items:
                                [
                                    {
                                        x: 10,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'Refleksi Patologis '
                                    },
                                    {
                                        x: 125,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 135,
                                        y: 10,
                                        xtype: 'checkboxgroup',
                                        id: 'cgPatologis',
                                        columns: 4,
                                        boxMaxWidth: 300,
                                        vertical: true,
                                        items: [
                                            {boxLabel: 'Choddoks', name: 'cgPatologisChoddoks', id: 'cgPatologisChoddoks'},
                                            {boxLabel: 'Babinski', name: 'cgPatologisBabinski', id: 'cgPatologisBabinski'},
                                            {boxLabel: 'Budzinzky', name: 'cgPatologisBudzinzky', id: 'cgPatologisBudzinzky'},
                                            {boxLabel: 'Kerning', name: 'cgPatologisKerning', id: 'cgPatologisKerning'}
                                        ],
                                        listeners: {
                                            change: function (checkbox, newValue, oldValue, eOpts) {
                                                Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                tmpediting = 'true';
                                                if (Ext.getCmp('cgPatologisChoddoks').checked === true)
                                                {
                                                    SYARAF_PATOLOGIS_CHODDOKS = 'true';
                                                } else
                                                {
                                                    SYARAF_PATOLOGIS_CHODDOKS = 'false';
                                                }
                                                if (Ext.getCmp('cgPatologisBabinski').checked === true)
                                                {
                                                    SYARAF_PATOLOGIS_BABINSKI = 'true';
                                                } else
                                                {
                                                    SYARAF_PATOLOGIS_BABINSKI = 'false';
                                                }
                                                if (Ext.getCmp('cgPatologisBudzinzky').checked === true)
                                                {
                                                    SYARAF_PATOLOGIS_BUDZINZKY = 'true';
                                                } else
                                                {
                                                    SYARAF_PATOLOGIS_BUDZINZKY = 'false';
                                                }
                                                if (Ext.getCmp('cgPatologisKerning').checked === true)
                                                {
                                                    SYARAF_PATOLOGIS_KERNIG = 'true';
                                                } else
                                                {
                                                    SYARAF_PATOLOGIS_KERNIG = 'false';
                                                }
                                            }
                                        }
                                    }
                                ]
                    },
                    {
                        columnWidth: .20,
                        layout: 'absolute',
                        labelWidth: 100,
                        border: false,
                        height: 246,
                        items:
                                [
                                    {
                                        x: 10,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'Masalah '
                                    },
                                    {
                                        x: 125,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 135,
                                        y: 10,
                                        xtype: 'textarea',
                                        name: 'txtmasalahSaraf',
                                        id: 'txtmasalahSaraf',
                                        height: 236,
                                        width: 825,
                                        listeners: {
                                            change: function () {
                                                Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                tmpediting = 'true';
                                                SYARAF_MASALAH = Ext.get('txtmasalahSaraf').getValue();
                                            }
                                        }
                                    }]
                    }
                ]
            }
            );
    return tabSaraf;
}
;
function Pengindraan()
{
    var tabPengindraan = new Ext.Panel
            (
                    {title: "Pengindraan",
                        id: 'tabPengindraan',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        bodyStyle: 'padding:5px 5px 5px 5px',
                        border: true,
                        autoscroll: true,
                        items: [
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Pupil '
                                            },
                                            {
                                                x: 130,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 140,
                                                y: 10,
                                                xtype: 'checkboxgroup',
                                                id: 'cgPupil',
                                                columns: 2,
                                                boxMaxWidth: 150,
                                                vertical: true,
                                                items: [
                                                    {boxLabel: 'Isokor', name: 'cgPupilIsokor', id: 'cgPupilIsokor'},
                                                    {boxLabel: 'Anisokor', name: 'cgPupilAnisokor', id: 'cgPupilAnisokor'}
                                                ],
                                                listeners: {
                                                    change: function (checkbox, newValue, oldValue, eOpts) {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('cgPupilIsokor').checked === true)
                                                        {
                                                            INDRA_PUPIL_ISOKOR = 'true';
                                                        } else
                                                        {
                                                            INDRA_PUPIL_ISOKOR = 'false';
                                                        }
                                                        if (Ext.getCmp('cgPupilAnisokor').checked === true)
                                                        {
                                                            INDRA__PUPIL_ANISOKOR = 'true';
                                                        } else
                                                        {
                                                            INDRA__PUPIL_ANISOKOR = 'false';
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                x: 300,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Konjungtiva/Skalar '
                                            },
                                            {
                                                x: 400,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 420,
                                                y: 10,
                                                xtype: 'checkboxgroup',
                                                id: 'cgKonjungtiva',
                                                columns: 3,
                                                boxMaxWidth: 225,
                                                vertical: true,
                                                items: [
                                                    {boxLabel: 'Anemis', name: 'cgKonjungtivaAnemis', id: 'cgKonjungtivaAnemis'},
                                                    {boxLabel: 'Icterus', name: 'cgKonjungtivaIcterus', id: 'cgKonjungtivaIcterus'},
                                                    {boxLabel: 'Tidak', name: 'cgKonjungtivaTidak', id: 'cgKonjungtivaTidak'},
                                                ],
                                                listeners: {
                                                    change: function (checkbox, newValue, oldValue, eOpts) {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('cgKonjungtivaAnemis').checked === true)
                                                        {
                                                            INDRA__KONJUNGTIVA_ANEMIS = 'true';
                                                        } else
                                                        {
                                                            INDRA__KONJUNGTIVA_ANEMIS = 'false';
                                                        }
                                                        if (Ext.getCmp('cgKonjungtivaIcterus').checked === true)
                                                        {
                                                            INDRA__KONJUNGTIVA_ICTERUS = 'true';
                                                        } else
                                                        {
                                                            INDRA__KONJUNGTIVA_ICTERUS = 'false';
                                                        }
                                                        if (Ext.getCmp('cgKonjungtivaTidak').checked === true)
                                                        {
                                                            INDRA__KONJUNGTIVA_TIDAK = 'true';
                                                        } else
                                                        {
                                                            INDRA__KONJUNGTIVA_TIDAK = 'false';
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Gangguan Pendengaran '
                                            },
                                            {
                                                x: 130,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 140,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgGangguanPendengaran',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: 'Ya',
                                                        name: 'rgGangguanPendengaran',
                                                        inputValue: 1,
                                                        id: 'rgGangguanPendengaranya'
                                                    },
                                                    {
                                                        boxLabel: 'Tidak',
                                                        name: 'rgGangguanPendengaran',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rgGangguanPendengarantidak'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rgGangguanPendengaranya').checked === true)
                                                        {
                                                            INDRA_GANGGUAN_DENGAR = 'true';
                                                        } else {
                                                            INDRA_GANGGUAN_DENGAR = 'false';
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                x: 300,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Otorhea '
                                            },
                                            {
                                                x: 400,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 420,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgOtorhea',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: 'Ya',
                                                        name: 'rgOtorhea',
                                                        inputValue: 1,
                                                        id: 'rgOtorheaya'
                                                    },
                                                    {
                                                        boxLabel: 'Tidak',
                                                        name: 'rgOtorhea',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rgOtorheatidak'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rgGangguanPendengaranya').checked === true)
                                                        {
                                                            INDRA_OTORHEA = 'true';
                                                        } else {
                                                            INDRA_OTORHEA = 'false';
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Bentuk Hidung '
                                            },
                                            {
                                                x: 130,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 140,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgBentukHidung',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: 'Ya',
                                                        name: 'rgBentukHidung',
                                                        inputValue: 1,
                                                        id: 'rgBentukHidungya'
                                                    },
                                                    {
                                                        boxLabel: 'Tidak',
                                                        name: 'rgBentukHidung',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rgBentukHidungtidak'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rgBentukHidungya').checked === true)
                                                        {
                                                            INDRA_BENTUK_HIDUNG = 'true';
                                                        } else {
                                                            INDRA_BENTUK_HIDUNG = 'false';
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                x: 300,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Jelaskan '
                                            },
                                            {
                                                x: 400,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 420,
                                                y: 5,
                                                xtype: 'textfield',
                                                id: 'txtjelaskanpengindraan',
                                                width: 540,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        INDRA_BENTUK_HIDUNG_KET = Ext.get('txtjelaskanpengindraan').getValue();
                                                    }
                                                }
                                            }]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Gangguan Penciuman '
                                            },
                                            {
                                                x: 130,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 140,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgGangguanPenciuman',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: 'Ya',
                                                        name: 'rgGangguanPenciuman',
                                                        inputValue: 1,
                                                        id: 'rgGangguanPenciumanya'
                                                    },
                                                    {
                                                        boxLabel: 'Tidak',
                                                        name: 'rgGangguanPenciuman',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rgGangguanPenciumantidak'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rgGangguanPenciumanya').checked === true)
                                                        {
                                                            INDRA_GANGGUAN_CIUM = 'true';
                                                        } else {
                                                            INDRA_GANGGUAN_CIUM = 'false';
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                x: 300,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Rhinorhea '
                                            },
                                            {
                                                x: 400,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 420,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgRhinorhea',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: 'Ya',
                                                        name: 'rgRhinorhea',
                                                        inputValue: 1,
                                                        id: 'rgRhinorheaya'
                                                    },
                                                    {
                                                        boxLabel: 'Tidak',
                                                        name: 'rgRhinorhea',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rgRhinorheatidak'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rgRhinorheaya').checked === true)
                                                        {
                                                            INDRA_RHINORHEA = 'true';
                                                        } else {
                                                            INDRA_RHINORHEA = 'false';
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 216,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'masalah '
                                            },
                                            {
                                                x: 130,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 140,
                                                y: 5,
                                                xtype: 'textarea',
                                                id: 'trmasalahpengindraan',
                                                width: 820,
                                                height: 211,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        INDRA_MASALAH = Ext.get('trmasalahpengindraan').getValue();
                                                    }
                                                }
                                            }]
                            }
                        ]
                    }
            );
    return tabPengindraan;
}
;
function Perkemihan()
{
    var tabPerkemihan = new Ext.Panel
            (
                    {title: "Perkemihan",
                        id: 'tabPerkemihan',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        bodyStyle: 'padding:5px 5px 5px 5px',
                        border: true,
                        autoscroll: true,
                        items: [
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Kebersihan '
                                            },
                                            {
                                                x: 100,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 120,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgKebersihan',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: 'Bersih',
                                                        name: 'rgKebersihan',
                                                        inputValue: 1,
                                                        id: 'rgKebersihanBersih'
                                                    },
                                                    {
                                                        boxLabel: 'Kotor',
                                                        name: 'rgKebersihan',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rgKebersihanKotor'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rgKebersihanBersih').checked === true)
                                                        {
                                                            KEMIH_KEBERSIHAN = 'true';
                                                        } else {
                                                            KEMIH_KEBERSIHAN = 'false';
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                x: 310,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Alat Bantu Kateter '
                                            },
                                            {
                                                x: 400,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 420,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgAlatBantu',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: 'Ya',
                                                        name: 'rgAlatBantu',
                                                        inputValue: 1,
                                                        id: 'rgAlatBantuYa'
                                                    },
                                                    {
                                                        boxLabel: 'Tidak',
                                                        name: 'rgAlatBantu',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rgAlatBantuTidak'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rgAlatBantuYa').checked === true)
                                                        {
                                                            KEMIH_ALAT_BANTU = 'true';
                                                        } else {
                                                            KEMIH_ALAT_BANTU = 'false';
                                                        }
                                                    }
                                                }
                                            },
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Urin '
                                            },
                                            {
                                                x: 100,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 120,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Jumlah(cc) '
                                            },
                                            {
                                                x: 175,
                                                y: 5,
                                                xtype: 'textfield',
                                                id: 'txtjumlahurin',
                                                width: 100,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        KEMIH_JML_URINE = Ext.get('txtjumlahurin').getValue();
                                                    }
                                                }
                                            },
                                            {
                                                x: 310,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Warna '
                                            },
                                            {
                                                x: 400,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 420,
                                                y: 5,
                                                xtype: 'textfield',
                                                id: 'txtwarnaurin',
                                                width: 100,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        KEMIH_WARNA_URINE = Ext.get('txtwarnaurin').getValue();
                                                    }
                                                }
                                            },
                                            {
                                                x: 550,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Bau '
                                            },
                                            {
                                                x: 580,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 600,
                                                y: 5,
                                                xtype: 'textfield',
                                                id: 'txtbauurin',
                                                width: 100,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        KEMIH_BAU = Ext.get('txtbauurin').getValue();
                                                    }
                                                }
                                            }]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Kand. Kencing '
                                            },
                                            {
                                                x: 100,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 120,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Membesar '
                                            },
                                            {
                                                x: 175,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgMembesar',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: 'Ya',
                                                        name: 'rgMembesar',
                                                        inputValue: 1,
                                                        id: 'rgMembesarYa'
                                                    },
                                                    {
                                                        boxLabel: 'Tidak',
                                                        name: 'rgMembesar',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rgMembesarTidak'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rgMembesarYa').checked === true)
                                                        {
                                                            KEMIH_KANDUNG_MEMBESAR = 'true';
                                                        } else {
                                                            KEMIH_KANDUNG_MEMBESAR = 'false';
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                x: 310,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Nyeri Tekanan '
                                            },
                                            {
                                                x: 400,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 420,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgNyeri',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: 'Ya',
                                                        name: 'rgNyeri',
                                                        inputValue: 1,
                                                        id: 'rgNyeriYa'
                                                    },
                                                    {
                                                        boxLabel: 'Tidak',
                                                        name: 'rgNyeri',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rgNyeriTidak'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rgNyeriYa').checked === true)
                                                        {
                                                            KEMIH_NYERI_TEKAN = 'true';
                                                        } else {
                                                            KEMIH_NYERI_TEKAN = 'false';
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 60,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Ganguan '
                                            },
                                            {
                                                x: 100,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 120,
                                                y: 10,
                                                xtype: 'checkboxgroup',
                                                id: 'cgGanguankemih',
                                                columns: 3,
                                                boxMaxWidth: 300,
                                                vertical: true,
                                                items: [
                                                    {boxLabel: 'Anuir', name: 'cgGanguankemihAnuir', id: 'cgGanguankemihAnuir'},
                                                    {boxLabel: 'Oligura', name: 'cgGanguankemihOligura', id: 'cgGanguankemihOligura'},
                                                    {boxLabel: 'Retensi', name: 'cgGanguankemihRetensi', id: 'cgGanguankemihRetensi'},
                                                    {boxLabel: 'Inkontinesia', name: 'cgGanguankemihInkontinesia', id: 'cgGanguankemihInkontinesia'},
                                                    {boxLabel: 'Disuria', name: 'cgGanguankemihDisuria', id: 'cgGanguankemihDisuria'},
                                                    {boxLabel: 'Hematuri', name: 'cgGanguankemihHematuri', id: 'cgGanguankemihHematuri'}
                                                ],
                                                listeners: {
                                                    change: function (checkbox, newValue, oldValue, eOpts) {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('cgGanguankemihAnuir').checked === true)
                                                        {
                                                            KEMIH_GANGGUAN_ANURIA = 'true';
                                                        } else
                                                        {
                                                            KEMIH_GANGGUAN_ANURIA = 'false';
                                                        }
                                                        if (Ext.getCmp('cgGanguankemihOligura').checked === true)
                                                        {
                                                            KEMIH_GANGGUAN_ALIGURIA = 'true';
                                                        } else
                                                        {
                                                            KEMIH_GANGGUAN_ALIGURIA = 'false';
                                                        }
                                                        if (Ext.getCmp('cgGanguankemihRetensi').checked === true)
                                                        {
                                                            KEMIH_GANGGUAN_RETENSI = 'true';
                                                        } else
                                                        {
                                                            KEMIH_GANGGUAN_RETENSI = 'false';
                                                        }
                                                        if (Ext.getCmp('cgGanguankemihInkontinesia').checked === true)
                                                        {
                                                            KEMIH_GANGGUAN_INKONTINENSIA = 'true';
                                                        } else
                                                        {
                                                            KEMIH_GANGGUAN_INKONTINENSIA = 'false';
                                                        }

                                                        if (Ext.getCmp('cgGanguankemihDisuria').checked === true)
                                                        {
                                                            KEMIH_GANGGUAN_DISURIA = 'true';
                                                        } else
                                                        {
                                                            KEMIH_GANGGUAN_DISURIA = 'false';
                                                        }
                                                        if (Ext.getCmp('cgGanguankemihHematuri').checked === true)
                                                        {
                                                            KEMIH_GANGGUANHEMATURIA = 'true';
                                                        } else
                                                        {
                                                            KEMIH_GANGGUANHEMATURIA = 'false';
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 186,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Masalah '
                                            },
                                            {
                                                x: 100,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 120,
                                                y: 10,
                                                xtype: 'textarea',
                                                id: 'trmasalahperkemihan',
                                                width: 840,
                                                height: 176,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        KEMIH_MASALAH = Ext.get('trmasalahperkemihan').getValue();
                                                    }
                                                }
                                            }]
                            },
                        ]
                    }
            );
    return tabPerkemihan;
}
;
function Pencernaan()
{
    var tabPencernaan = new Ext.Panel
            (
                    {title: "Pencernaan",
                        id: 'tabPencernaan',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        bodyStyle: 'padding:5px 5px 5px 5px',
                        border: true,
                        autoscroll: true,
                        items: [
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Porsi Makan '
                                            },
                                            {
                                                x: 100,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 120,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgPorsiMakan',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: 'Habis',
                                                        name: 'rgPorsiMakan',
                                                        inputValue: 1,
                                                        id: 'rgPorsiMakanHabis'
                                                    },
                                                    {
                                                        boxLabel: 'Tidak',
                                                        name: 'rgPorsiMakan',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rgPorsiMakanTidak'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rgPorsiMakanHabis').checked === true)
                                                        {
                                                            CERNA_MAKAN_HABIS = 'true';
                                                        } else {
                                                            CERNA_MAKAN_HABIS = 'false';
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                x: 265,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Keterangan (Porsi) '
                                            },
                                            {
                                                x: 355,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 360,
                                                y: 5,
                                                xtype: 'textfield',
                                                id: 'txtporsi',
                                                width: 80,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        CERNA_MAKAN_KET = Ext.get('txtporsi').getValue();
                                                    }
                                                }
                                            }, {
                                                x: 460,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Frekuensi(x/hr) '
                                            },
                                            {
                                                x: 545,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 550,
                                                y: 5,
                                                xtype: 'textfield',
                                                id: 'txtFrekuensimakan',
                                                width: 60,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        CERNA_MAKAN_FREKUENSI = Ext.get('txtFrekuensimakan').getValue();
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Minum(cc/Hari) '
                                            },
                                            {
                                                x: 100,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 120,
                                                y: 5,
                                                xtype: 'textfield',
                                                id: 'txtMinum',
                                                width: 60,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        CERNA_JML_MINUM = Ext.get('txtMinum').getValue();
                                                    }
                                                }
                                            }, {
                                                x: 265,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Jenis '
                                            },
                                            {
                                                x: 355,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 360,
                                                y: 5,
                                                xtype: 'textfield',
                                                id: 'txtjenisMinuman',
                                                width: 80,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        CERNA_JENIS_MINUM = Ext.get('txtjenisMinuman').getValue();
                                                    }
                                                }
                                            }, {
                                                x: 460,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Paristaltik(x/mnt) '
                                            },
                                            {
                                                x: 545,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 550,
                                                y: 5,
                                                xtype: 'textfield',
                                                id: 'txtParistaltik',
                                                width: 80,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        CERNA_PERISTALTIK = Ext.get('txtParistaltik').getValue();
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Mulut '
                                            },
                                            {
                                                x: 100,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 120,
                                                y: 10,
                                                xtype: 'checkboxgroup',
                                                id: 'cgKondisiMulut',
                                                columns: 3,
                                                boxMaxWidth: 225,
                                                vertical: true,
                                                items: [
                                                    {boxLabel: 'Bersih', name: 'cgKondisiMulutBersih', id: 'cgKondisiMulutBersih'},
                                                    {boxLabel: 'Kotor', name: 'cgKondisiMulutKotor', id: 'cgKondisiMulutKotor'},
                                                    {boxLabel: 'Berbau', name: 'cgKondisiMulutBerbau', id: 'cgKondisiMulutBerbau'}
                                                ],
                                                listeners: {
                                                    change: function (checkbox, newValue, oldValue, eOpts) {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('cgKondisiMulutBersih').checked === true)
                                                        {
                                                            CERNA_MULUT_BERSIH = 'true';
                                                        } else
                                                        {
                                                            CERNA_MULUT_BERSIH = 'false';
                                                        }
                                                        if (Ext.getCmp('cgKondisiMulutKotor').checked === true)
                                                        {
                                                            CERNA_MULUT_KOTOR = 'true';
                                                        } else
                                                        {
                                                            CERNA_MULUT_KOTOR = 'false';
                                                        }
                                                        if (Ext.getCmp('cgKondisiMulutBerbau').checked === true)
                                                        {
                                                            CERNA_MULUT_BERBAU = 'true';
                                                        } else
                                                        {
                                                            CERNA_MULUT_BERBAU = 'false';
                                                        }
                                                    }
                                                }
                                            }

                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Mukosa '
                                            },
                                            {
                                                x: 100,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 120,
                                                y: 10,
                                                xtype: 'checkboxgroup',
                                                id: 'cgMukosa',
                                                columns: 3,
                                                boxMaxWidth: 225,
                                                vertical: true,
                                                items: [
                                                    {boxLabel: 'Lembab', name: 'cgMukosaLembab', id: 'cgMukosaLembab'},
                                                    {boxLabel: 'Kering', name: 'cgMukosaKering', id: 'cgMukosaKering'},
                                                    {boxLabel: 'Stomatitis', name: 'cgMukosaStomatitis', id: 'cgMukosaStomatitis'}
                                                ],
                                                listeners: {
                                                    change: function (checkbox, newValue, oldValue, eOpts) {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('cgMukosaLembab').checked === true)
                                                        {
                                                            CERNA_MUKOSA_LEMBAB = 'true';
                                                        } else
                                                        {
                                                            CERNA_MUKOSA_LEMBAB = 'false';
                                                        }
                                                        if (Ext.getCmp('cgMukosaKering').checked === true)
                                                        {
                                                            CERNA_MUKOSA_KERING = 'true';
                                                        } else
                                                        {
                                                            CERNA_MUKOSA_KERING = 'false';
                                                        }
                                                        if (Ext.getCmp('cgMukosaKering').checked === true)
                                                        {
                                                            CERNA_MUKOSA_STOMATITIS = 'true';
                                                        } else
                                                        {
                                                            CERNA_MUKOSA_STOMATITIS = 'false';
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                x: 355,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Turgor Kulit '
                                            },
                                            {
                                                x: 420,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 440,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgTurgor',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: 'Baik',
                                                        name: 'rgTurgor',
                                                        inputValue: 1,
                                                        id: 'rgTurgorBaik'
                                                    },
                                                    {
                                                        boxLabel: 'Jelek',
                                                        name: 'rgTurgor',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rgTurgorJelek'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rgTurgorBaik').checked === true)
                                                        {
                                                            CERNA_TURGOR_KULIT = 'true';
                                                        } else {
                                                            CERNA_TURGOR_KULIT = 'false';
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Tenggorokan '
                                            },
                                            {
                                                x: 100,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 120,
                                                y: 10,
                                                xtype: 'checkboxgroup',
                                                id: 'cgTenggorokan',
                                                columns: 3,
                                                boxMaxWidth: 300,
                                                vertical: true,
                                                items: [
                                                    {boxLabel: 'Sakit/Sulit Nelan', name: 'cgTenggorokanSakit', id: 'cgTenggorokanSakit'},
                                                    {boxLabel: 'Nyeri tekan', name: 'cgTenggorokanNyeri', id: 'cgTenggorokanNyeri'},
                                                    {boxLabel: 'Pembesaran Tonsil', name: 'cgTenggorokanTonsil', id: 'cgTenggorokanTonsil'}]
                                            }
                                        ],
                                listeners: {
                                    change: function (checkbox, newValue, oldValue, eOpts) {
                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                        tmpediting = 'true';
                                        if (Ext.getCmp('cgTenggorokanSakit').checked === true)
                                        {
                                            CERNA_TENGGOROKAN_SAKIT = 'true';
                                        } else
                                        {
                                            CERNA_TENGGOROKAN_SAKIT = 'false';
                                        }
                                        if (Ext.getCmp('cgTenggorokanNyeri').checked === true)
                                        {
                                            CERNA_TENGGOROKAN_NYERI_TEKAN = 'true';
                                        } else
                                        {
                                            CERNA_TENGGOROKAN_NYERI_TEKAN = 'false';
                                        }
                                        if (Ext.getCmp('cgTenggorokanTonsil').checked === true)
                                        {
                                            CERNA_TENGGOROKAN_PEMBESARANTO = 'true';
                                        } else
                                        {
                                            CERNA_TENGGOROKAN_PEMBESARANTO = 'false';
                                        }
                                    }
                                }
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Perut '
                                            },
                                            {
                                                x: 100,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 120,
                                                y: 10,
                                                xtype: 'checkboxgroup',
                                                id: 'cgPerut',
                                                columns: 3,
                                                boxMaxWidth: 300,
                                                vertical: true,
                                                items: [
                                                    {boxLabel: 'Normal', name: 'cgPerutNormal', id: 'cgPerutNormal'},
                                                    {boxLabel: 'Distended', name: 'cgPerutDistended', id: 'cgPerutDistended'},
                                                    {boxLabel: 'Meteorismus', name: 'cgPerutMeteorismus', id: 'cgPerutMeteorismus'}
                                                ],
                                                listeners: {
                                                    change: function (checkbox, newValue, oldValue, eOpts) {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('cgPerutNormal').checked === true)
                                                        {
                                                            CERNA_PERUT_NORMAL = 'true';
                                                        } else
                                                        {
                                                            CERNA_PERUT_NORMAL = 'false';
                                                        }
                                                        if (Ext.getCmp('cgPerutDistended').checked === true)
                                                        {
                                                            CERNA_PERUT_DISTENDED = 'true';
                                                        } else
                                                        {
                                                            CERNA_PERUT_DISTENDED = 'false';
                                                        }
                                                        if (Ext.getCmp('cgPerutMeteorismus').checked === true)
                                                        {
                                                            CERNA_PERUT_METEORISMUS = 'true';
                                                        } else
                                                        {
                                                            CERNA_PERUT_METEORISMUS = 'false';
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                x: 460,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Pembesaran Tonsil '
                                            },
                                            {
                                                x: 560,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 570,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgPembesaranTonsil',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: 'Ya',
                                                        name: 'rgPembesaranTonsil',
                                                        inputValue: 1,
                                                        id: 'rgPembesaranTonsilYa'
                                                    },
                                                    {
                                                        boxLabel: 'Tidak',
                                                        name: 'rgPembesaranTonsil',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rgPembesaranTonsilTidak'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rgPembesaranTonsilYa').checked === true)
                                                        {
                                                            CERNA_PEMBESARAN_HEPAR = 'true';
                                                        } else {
                                                            CERNA_PEMBESARAN_HEPAR = 'false';
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 120,
                                                y: 10,
                                                xtype: 'checkboxgroup',
                                                id: 'cgNyeriTekan',
                                                columns: 1,
                                                boxMaxWidth: 300,
                                                vertical: true,
                                                items: [
                                                    {boxLabel: 'Nyeri Tekan', name: 'cgNyerisaatTekan', id: 'cgNyerisaatTekan'}
                                                ],
                                                listeners: {
                                                    change: function (checkbox, newValue, oldValue, eOpts) {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('cgNyerisaatTekan').checked === true)
                                                        {
                                                            CERNA_PERUT_NYERI_TEKAN = 'true';
                                                        } else
                                                        {
                                                            CERNA_PERUT_NYERI_TEKAN = 'false';
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                x: 202,
                                                y: 10,
                                                xtype: 'label',
                                                text: ','
                                            },
                                            {
                                                x: 211,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Lokasi'
                                            },
                                            {
                                                x: 246,
                                                y: 5,
                                                xtype: 'textfield',
                                                width: 190,
                                                id: 'txtlokasisakit',
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        CERNA_PERUT_LOKASI_NYERI = Ext.get('txtlokasisakit').getValue();
                                                    }
                                                }
                                            },
                                            {
                                                x: 460,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Hematemesis '
                                            },
                                            {
                                                x: 560,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 570,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgHematemesis',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: 'Ya',
                                                        name: 'rgHematemesis',
                                                        inputValue: 1,
                                                        id: 'rgHematemesisYa'
                                                    },
                                                    {
                                                        boxLabel: 'Tidak',
                                                        name: 'rgHematemesis',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rgHematemesisTidak'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rgHematemesisYa').checked === true)
                                                        {
                                                            CERNA_HEMATEMESIS = 'true';
                                                        } else {
                                                            CERNA_HEMATEMESIS = 'false';
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Buang Air B.(x/Hr) '
                                            },
                                            {
                                                x: 100,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 120,
                                                y: 5,
                                                xtype: 'textfield',
                                                width: 100,
                                                id: 'txtjumlahBAB',
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        CERNA_FREK_BAB = Ext.get('txtjumlahBAB').getValue();
                                                    }
                                                }
                                            },
                                            {
                                                x: 230,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Konsintensi '
                                            },
                                            {
                                                x: 290,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 310,
                                                y: 5,
                                                xtype: 'textfield',
                                                width: 100,
                                                id: 'txtKonsintensiBAB',
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        CERNA_KONSISTENSI = Ext.get('txtKonsintensiBAB').getValue();
                                                    }
                                                }
                                            },
                                            {
                                                x: 430,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Warna '
                                            },
                                            {
                                                x: 470,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 490,
                                                y: 5,
                                                xtype: 'textfield',
                                                width: 100,
                                                id: 'txtWarnaBAB',
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        CERNA_WARNA_BAB = Ext.get('txtWarnaBAB').getValue();
                                                    }
                                                }
                                            },
                                            {
                                                x: 610,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Bau '
                                            },
                                            {
                                                x: 640,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 660,
                                                y: 5,
                                                xtype: 'textfield',
                                                width: 100,
                                                id: 'txtBauBAB',
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        CERNA_BAU_BAB = Ext.get('txtBauBAB').getValue();
                                                    }
                                                }
                                            },
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Evakuasi Cairan A '
                                            },
                                            {
                                                x: 100,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 120,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgEvakuasiCairan',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: 'Ya',
                                                        name: 'rgEvakuasiCairan',
                                                        inputValue: 1,
                                                        id: 'rgEvakuasiCairanYa'
                                                    },
                                                    {
                                                        boxLabel: 'Tidak',
                                                        name: 'rgEvakuasiCairan',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rgEvakuasiCairanTidak'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rgEvakuasiCairanYa').checked === true)
                                                        {
                                                            CERNA_EVAKUASI_CAIRAN_ASCITES = 'true';
                                                        } else {
                                                            CERNA_EVAKUASI_CAIRAN_ASCITES = 'false';
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                x: 260,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Jumlah(cc) '
                                            },
                                            {
                                                x: 320,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 330,
                                                y: 5,
                                                xtype: 'textfield',
                                                width: 100,
                                                id: 'txtJumlahcairan',
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        CERNA_JML_CAIRAN_ASCITES = Ext.get('txtJumlahcairan').getValue();
                                                    }
                                                }
                                            },
                                            {
                                                x: 450,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Warna '
                                            },
                                            {
                                                x: 490,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 500,
                                                y: 5,
                                                xtype: 'textfield',
                                                width: 100,
                                                id: 'txtWarnaCairan',
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        CERNA_WARNA_CAIRAN_ASCITES = Ext.get('txtWarnaCairan').getValue();
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 65,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Masalah '
                                            },
                                            {
                                                x: 100,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 120,
                                                y: 10,
                                                xtype: 'textarea',
                                                width: 840,
                                                height: 55,
                                                id: 'trmasalahpencernaan',
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        CERNA_MASALAH = Ext.get('trmasalahpencernaan').getValue();
                                                    }
                                                }
                                            }
                                        ]
                            },
                        ]
                    }
            );
    return tabPencernaan;
}
;
function Endokrin()
{
    var tabEndokrin = new Ext.Panel
            (
                    {title: "Endokrin",
                        id: 'tabEndokrin',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        bodyStyle: 'padding:5px 5px 5px 5px',
                        border: true,
                        autoscroll: true,
                        items: [
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Tyroid '
                                            },
                                            {
                                                x: 100,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 120,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgTyroid',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: 'Membesar',
                                                        name: 'rgTyroid',
                                                        inputValue: 1,
                                                        id: 'rgTyroidMembesar'
                                                    },
                                                    {
                                                        boxLabel: 'Tidak',
                                                        name: 'rgTyroid',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rgTyroidTidak'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rgTyroidMembesar').checked === true)
                                                        {
                                                            ENDOKRIN_TYROID = 'true';
                                                        } else {
                                                            ENDOKRIN_TYROID = 'false';
                                                        }
                                                        //console.log(ENDOKRIN_TYROID);
                                                    }
                                                }
                                            },
                                            {
                                                x: 260,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Hyper/Hypoglikernia '
                                            },
                                            {
                                                x: 360,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 370,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgHyper',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: 'Ya',
                                                        name: 'rgHyper',
                                                        inputValue: 1,
                                                        id: 'rgHyperYa'
                                                    },
                                                    {
                                                        boxLabel: 'Tidak',
                                                        name: 'rgHyper',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rgHyperTidak'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rgHyperYa').checked === true)
                                                        {
                                                            ENDOKRIN_HIPOGLIKEMIA = 'true';
                                                        } else {
                                                            ENDOKRIN_HIPOGLIKEMIA = 'false';
                                                        }
                                                        //console.log(ENDOKRIN_HIPOGLIKEMIA);
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Luka Gangren '
                                            },
                                            {
                                                x: 100,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 120,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgGangren',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: 'Ya',
                                                        name: 'rgGangren',
                                                        inputValue: 1,
                                                        id: 'rgGangrenYa'
                                                    },
                                                    {
                                                        boxLabel: 'Tidak',
                                                        name: 'rgGangren',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rgGangrenTidak'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rgGangrenYa').checked === true)
                                                        {
                                                            ENDOKRIN_LUKA_GANGREN = 'true';
                                                        } else {
                                                            ENDOKRIN_LUKA_GANGREN = 'false';
                                                        }
                                                        //console.log(ENDOKRIN_LUKA_GANGREN);
                                                    }
                                                }
                                            },
                                            {
                                                x: 260,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Pus '
                                            },
                                            {
                                                x: 360,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 370,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgPus',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: 'Ya',
                                                        name: 'rgPus',
                                                        inputValue: 1,
                                                        id: 'rgPusYa'
                                                    },
                                                    {
                                                        boxLabel: 'Tidak',
                                                        name: 'rgPus',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rgPusTidak'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rgPusYa').checked === true)
                                                        {
                                                            ENDOKRIN__PUS = 'true';
                                                        } else {
                                                            ENDOKRIN__PUS = 'false';
                                                        }
                                                        //console.log(ENDOKRIN__PUS);
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 276,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Masalah '
                                            },
                                            {
                                                x: 100,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 120,
                                                y: 10,
                                                xtype: 'textarea',
                                                id: 'trmasalahendokrin',
                                                width: 840,
                                                height: 266,
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        ENDOKRIN_MASALAH = Ext.get('trmasalahendokrin').getValue();
                                                        //console.log(ENDOKRIN_MASALAH);
                                                    }
                                                }
                                            }]
                            }
                        ]
                    }
            );
    return tabEndokrin;
}
;
function PHygiene()
{
    var tabPHygiene = new Ext.Panel
            (
                    {title: "Personal Hygiene",
                        id: 'tabPHygiene',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        bodyStyle: 'padding:5px 5px 5px 5px',
                        border: true,
                        autoscroll: true,
                        items: [
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Mandi '
                                            },
                                            {
                                                x: 100,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 120,
                                                y: 5,
                                                xtype: 'textfield',
                                                id: 'txtmandi',
                                                width: 400,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        PERSONAL_MANDI = Ext.get('txtmandi').getValue();
                                                    }
                                                }
                                            }]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Keramas '
                                            },
                                            {
                                                x: 100,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 120,
                                                y: 5,
                                                xtype: 'textfield',
                                                id: 'txtKeramas',
                                                width: 400,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        PERSONAL_KERAMAS = Ext.get('txtKeramas').getValue();
                                                    }
                                                }
                                            }]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Sikat Gigi '
                                            },
                                            {
                                                x: 100,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 120,
                                                y: 5,
                                                xtype: 'textfield',
                                                id: 'txtSikatGigi',
                                                width: 400,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        PERSONAL_SIKATGIGI = Ext.get('txtSikatGigi').getValue();
                                                    }
                                                }
                                            }]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Ganti Pakaian '
                                            },
                                            {
                                                x: 100,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 120,
                                                y: 5,
                                                xtype: 'textfield',
                                                id: 'txtGantiPakaian',
                                                width: 400,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        PERSONAL_GANTIPAKAIAN = Ext.get('txtGantiPakaian').getValue();
                                                    }
                                                }
                                            }]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 216,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Masalah '
                                            },
                                            {
                                                x: 100,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 120,
                                                y: 5,
                                                xtype: 'textarea',
                                                id: 'trMasalahKebersihan',
                                                width: 840,
                                                height: 211,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        PERSONAL_MASALAH = Ext.get('trMasalahKebersihan').getValue();
                                                    }
                                                }
                                            }]
                            },
                        ]
                    }
            );
    return tabPHygiene;
}
;
function PsySos()
{
    var tabPsySos = new Ext.Panel
            ({title: "Psiko Sosial Spiritual",
                id: 'tabPsySos',
                fileUpload: true,
                region: 'north',
                layout: 'form',
                bodyStyle: 'padding:5px 5px 5px 5px',
                border: true,
                autoscroll: true,
                items: [
                    {
                        columnWidth: .20,
                        layout: 'absolute',
                        labelWidth: 100,
                        border: false,
                        height: 80,
                        items:
                                [
                                    {
                                        x: 10,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'Orang Paling Dekat '
                                    },
                                    {
                                        x: 110,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 10,
                                        xtype: 'textarea',
                                        id: 'trorangyangdekat',
                                        width: 840,
                                        height: 70,
                                        listeners: {
                                            change: function () {
                                                Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                tmpediting = 'true';
                                                PSIKOSOSIAL_ORANGDEKAT = Ext.get('trorangyangdekat').getValue();
                                            }
                                        }
                                    }]
                    },
                    {
                        columnWidth: .20,
                        layout: 'absolute',
                        labelWidth: 100,
                        border: false,
                        height: 80,
                        items:
                                [
                                    {
                                        x: 10,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'Kegiatan Ibadah '
                                    },
                                    {
                                        x: 110,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 10,
                                        xtype: 'textarea',
                                        id: 'trIbadah',
                                        width: 840,
                                        height: 70,
                                        listeners: {
                                            change: function () {
                                                Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                tmpediting = 'true';
                                                PSIKOSOSIAL_KEGIATAN_IBADAH = Ext.get('trIbadah').getValue();
                                            }
                                        }
                                    }]
                    },
                    {
                        columnWidth: .20,
                        layout: 'absolute',
                        labelWidth: 100,
                        border: false,
                        height: 175,
                        items:
                                [
                                    {
                                        x: 10,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'Masalah '
                                    },
                                    {
                                        x: 110,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 10,
                                        xtype: 'textarea',
                                        id: 'trMasalahSosial',
                                        width: 840,
                                        height: 165,
                                        listeners: {
                                            change: function () {
                                                Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                tmpediting = 'true';
                                                PSIKOSOSIAL_MASALAH = Ext.get('trMasalahSosial').getValue();
                                            }
                                        }
                                    }
                                ]
                    }
                ]
            }
            );
    return tabPsySos;
}
;
function Penunjang()
{
    var tabPenunjang = new Ext.Panel
            (
                    {title: "Data Penunjang(Lab,Foto,USG, Dll)",
                        id: 'tabPenunjang',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        bodyStyle: 'padding:5px 5px 5px 5px',
                        border: true,
                        autoscroll: true,
                        items: [
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 336,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Terapi '
                                            },
                                            {
                                                x: 50,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 58,
                                                y: 10,
                                                xtype: 'textarea',
                                                id: 'trterapipenunjang',
                                                width: 902,
                                                height: 326,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                                                        tmpediting = 'true';
                                                        TERAPI_PENUNJANG = Ext.get('trterapipenunjang').getValue();
                                                    }
                                                }
                                            }
                                        ]
                            }
                        ]
                    }
            );
    return tabPenunjang;
}
;
//combo dan load database

function datainit_pengkajian(rowdata)
{
    Ext.getCmp('TxtPopupMedrec').setValue(rowdata.KD_PASIEN);
    Ext.getCmp('TxtPopupNamaPasien').setValue(rowdata.NAMA);
    Ext.getCmp('TxtPopupAlamatPasien').setValue(rowdata.ALAMAT);
    Ext.getCmp('TxtPopupKdUnit').setValue(rowdata.KD_UNIT);
    Ext.getCmp('TxtPopupUnit').setValue(rowdata.NAMA_UNIT);
    Ext.getCmp('TxtPopupPelaksana').setValue(rowdata.NAMA_DOKTER);
    // caridatapasien();
}

function caridatapasien()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/CreateDataObj",
                        params: {
                            Table: 'ViewAskepDetPengkajian',
                            query: "kd_pasien_kunj = '" + tmpkdpasien + "' and kd_unit_kunj = '" + tmpkdunit + "' and urut_masuk_kunj = " + tmpurutmasuk + " and tgl_masuk_kunj = '" + tmptglmasuk + "'"
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {

                                if (cst.totalrecords !== 0)
                                {
                                    var tmphasil = cst.ListDataObj[0];
                                    RIWAYAT_UTAMA = tmphasil.RIWAYAT_UTAMA;
                                    RIWAYAT_PENYAKIT_SEKARANG = tmphasil.RIWAYAT_PENYAKIT_SEKARANG;
                                    RIWAYAT_PENYAKIT_DAHULU = tmphasil.RIWAYAT_PENYAKIT_DAHULU;
                                    NAFAS_JELAS = tmphasil.NAFAS_JELAS;
                                    NAFAS_EVAKUASI_CAIRAN = tmphasil.NAFAS_EVAKUASI_CAIRAN;
                                    NAFAS_PATEN = tmphasil.NAFAS_PATEN;
                                    NAFAS_OBSTRUKTIF = tmphasil.NAFAS_OBSTRUKTIF;
                                    NAFAS_POLA_SIMETRI = tmphasil.NAFAS_POLA_SIMETRI;
                                    NAFAS_POLA_ASIMETRI = tmphasil.NAFAS_POLA_ASIMETRI;
                                    NAFAS_SUARA_NORMAL = tmphasil.NAFAS_SUARA_NORMAL;
                                    NAFAS_SUARA_HIPERSONOR = tmphasil.NAFAS_SUARA_HIPERSONOR;
                                    NAFAS_MENURUN = tmphasil.NAFAS_MENURUN;
                                    NAFAS_JENIS_NORMAL = tmphasil.NAFAS_JENIS_NORMAL;
                                    NAFAS_JENIS_TACHYPNOE = tmphasil.NAFAS_JENIS_TACHYPNOE;
                                    NAFAS_JENIS_CHEYNESTOKES = tmphasil.NAFAS_JENIS_CHEYNESTOKES;
                                    NAFAS_JENIS_RETRACTIVE = tmphasil.NAFAS_JENIS_RETRACTIVE;
                                    NAFAS_JENIS_KUSMAUL = tmphasil.NAFAS_JENIS_KUSMAUL;
                                    NAFAS_JENIS_DISPNOE = tmphasil.NAFAS_JENIS_DISPNOE;
                                    NAFAS_RR = tmphasil.NAFAS_RR;
                                    NAFAS_SUARA_WHEEZING = tmphasil.NAFAS_SUARA_WHEEZING;
                                    NAFAS_SUARA_RONCHI = tmphasil.NAFAS_SUARA_RONCHI;
                                    NAFAS_SUARA_RALES = tmphasil.NAFAS_SUARA_RALES;
                                    NAFAS_JML_CAIRAN = tmphasil.NAFAS_JML_CAIRAN;
                                    NAFAS_WARNA_CAIRAN = tmphasil.NAFAS_WARNA_CAIRAN;
                                    NAFAS_MASALAH = tmphasil.NAFAS_MASALAH;
                                    JANTUNG_REGULER = tmphasil.JANTUNG_REGULER;
                                    JANTUNG_IRREGULER = tmphasil.JANTUNG_IRREGULER;
                                    JANTUNG_S1S2_TUNGGAL = tmphasil.JANTUNG_S1S2_TUNGGAL;
                                    JANTUNG_NYERI_DADA = tmphasil.JANTUNG_NYERI_DADA;
                                    JANTUNG_NYERI_DADA_KET = tmphasil.JANTUNG_NYERI_DADA_KET;
                                    JANTUNG_BUNYI_MURMUR = tmphasil.JANTUNG_BUNYI_MURMUR;
                                    JANTUNG_BUNYI_GALLOP = tmphasil.JANTUNG_BUNYI_GALLOP;
                                    JANTUNG_BUNYI_BISING = tmphasil.JANTUNG_BUNYI_BISING;
                                    JANTUNG_CRT = tmphasil.JANTUNG_CRT;
                                    JANTUNG_AKRAL_HANGAT = tmphasil.JANTUNG_AKRAL_HANGAT;
                                    JANTUNG_AKRAL_DINGIN = tmphasil.JANTUNG_AKRAL_DINGIN;
                                    JANTUNG_PENINGKATAN_JVP = tmphasil.JANTUNG_PENINGKATAN_JVP;
                                    JANTUNG_UKURAN_CVP = tmphasil.JANTUNG_UKURAN_CVP;
                                    JANTUNG_WARNA_JAUNDICE = tmphasil.JANTUNG_WARNA_JAUNDICE;
                                    JANTUNG_WARNA_SIANOTIK = tmphasil.JANTUNG_WARNA_SIANOTIK;
                                    JANTUNG_WARNA_KEMERAHAN = tmphasil.JANTUNG_WARNA_KEMERAHAN;
                                    JANTUNG_WARNA_PUCAT = tmphasil.JANTUNG_WARNA_PUCAT;
                                    JANTUNG_TEKANAN_DARAH = tmphasil.JANTUNG_TEKANAN_DARAH;
                                    JANTUNG_NADI = tmphasil.JANTUNG_NADI;
                                    JANTUNG_TEMP = tmphasil.JANTUNG_TEMP;
                                    JANTUNG_MASALAH = tmphasil.JANTUNG_MASALAH;
                                    OTOT_SENDI_BEBAS = tmphasil.OTOT_SENDI_BEBAS;
                                    OTOT_SENDI_TERBATAS = tmphasil.OTOT_SENDI_TERBATAS;
                                    OTOT_KEKUATAN = tmphasil.OTOT_KEKUATAN;
                                    OTOT_ODEMA_EKSTRIMITAS = tmphasil.OTOT_ODEMA_EKSTRIMITAS;
                                    OTOT_KELAINAN_BENTUK = tmphasil.OTOT_KELAINAN_BENTUK;
                                    OTOT_KREPITASI = tmphasil.OTOT_KREPITASI;
                                    OTOT_MASALAH = tmphasil.OTOT_MASALAH;
                                    SYARAF_GCS_EYE = tmphasil.SYARAF_GCS_EYE;
                                    SYARAF_GCS_VERBAL = tmphasil.SYARAF_GCS_VERBAL;
                                    SYARAF_GCS_MOTORIK = tmphasil.SYARAF_GCS_MOTORIK;
                                    SYARAF_GCS_TOTAL = tmphasil.SYARAF_GCS_TOTAL;
                                    SYARAF_FISIOLOGIS_BRACHIALIS = tmphasil.SYARAF_FISIOLOGIS_BRACHIALIS;
                                    SYARAF_FISIOLOGIS_PATELLA = tmphasil.SYARAF_FISIOLOGIS_PATELLA;
                                    SYARAF_FISIOLOGIS_ACHILLES = tmphasil.SYARAF_FISIOLOGIS_ACHILLES;
                                    SYARAF_PATOLOGIS_CHODDOKS = tmphasil.SYARAF_PATOLOGIS_CHODDOKS;
                                    SYARAF_PATOLOGIS_BABINSKI = tmphasil.SYARAF_PATOLOGIS_BABINSKI;
                                    SYARAF_PATOLOGIS_BUDZINZKY = tmphasil.SYARAF_PATOLOGIS_BUDZINZKY;
                                    SYARAF_PATOLOGIS_KERNIG = tmphasil.SYARAF_PATOLOGIS_KERNIG;
                                    SYARAF_MASALAH = tmphasil.SYARAF_MASALAH;
                                    INDRA_PUPIL_ISOKOR = tmphasil.INDRA_PUPIL_ISOKOR;
                                    INDRA__PUPIL_ANISOKOR = tmphasil.INDRA__PUPIL_ANISOKOR;
                                    INDRA__KONJUNGTIVA_ANEMIS = tmphasil.INDRA__KONJUNGTIVA_ANEMIS;
                                    INDRA__KONJUNGTIVA_ICTERUS = tmphasil.INDRA__KONJUNGTIVA_ICTERUS;
                                    INDRA__KONJUNGTIVA_TIDAK = tmphasil.INDRA__KONJUNGTIVA_TIDAK;
                                    INDRA_GANGGUAN_DENGAR = tmphasil.INDRA_GANGGUAN_DENGAR;
                                    INDRA_OTORHEA = tmphasil.INDRA_OTORHEA;
                                    INDRA_BENTUK_HIDUNG = tmphasil.INDRA_BENTUK_HIDUNG;
                                    INDRA_BENTUK_HIDUNG_KET = tmphasil.INDRA_BENTUK_HIDUNG_KET;
                                    INDRA_GANGGUAN_CIUM = tmphasil.INDRA_GANGGUAN_CIUM;
                                    INDRA_RHINORHEA = tmphasil.INDRA_RHINORHEA;
                                    INDRA_MASALAH = tmphasil.INDRA_MASALAH;
                                    CERNA_MAKAN_HABIS = tmphasil.CERNA_MAKAN_HABIS;
                                    CERNA_MAKAN_KET = tmphasil.CERNA_MAKAN_KET;
                                    CERNA_MAKAN_FREKUENSI = tmphasil.CERNA_MAKAN_FREKUENSI;
                                    CERNA_JML_MINUM = tmphasil.CERNA_JML_MINUM;
                                    CERNA_JENIS_MINUM = tmphasil.CERNA_JENIS_MINUM;
                                    CERNA_MULUT_BERSIH = tmphasil.CERNA_MULUT_BERSIH;
                                    CERNA_MULUT_KOTOR = tmphasil.CERNA_MULUT_KOTOR;
                                    CERNA_MULUT_BERBAU = tmphasil.CERNA_MULUT_BERBAU;
                                    CERNA_MUKOSA_LEMBAB = tmphasil.CERNA_MUKOSA_LEMBAB;
                                    CERNA_MUKOSA_KERING = tmphasil.CERNA_MUKOSA_KERING;
                                    CERNA_MUKOSA_STOMATITIS = tmphasil.CERNA_MUKOSA_STOMATITIS;
                                    CERNA_TENGGOROKAN_SAKIT = tmphasil.CERNA_TENGGOROKAN_SAKIT;
                                    CERNA_TENGGOROKAN_NYERI_TEKAN = tmphasil.CERNA_TENGGOROKAN_NYERI_TEKAN;
                                    CERNA_TENGGOROKAN_PEMBESARANTO = tmphasil.CERNA_TENGGOROKAN_PEMBESARANTO;
                                    CERNA_PERUT_NORMAL = tmphasil.CERNA_PERUT_NORMAL;
                                    CERNA_PERUT_DISTENDED = tmphasil.CERNA_PERUT_DISTENDED;
                                    CERNA_PERUT_METEORISMUS = tmphasil.CERNA_PERUT_METEORISMUS;
                                    CERNA_PERUT_NYERI_TEKAN = tmphasil.CERNA_PERUT_NYERI_TEKAN;
                                    CERNA_PERUT_LOKASI_NYERI = tmphasil.CERNA_PERUT_LOKASI_NYERI;
                                    CERNA_PERISTALTIK = tmphasil.CERNA_PERISTALTIK;
                                    CERNA_TURGOR_KULIT = tmphasil.CERNA_TURGOR_KULIT;
                                    CERNA_PEMBESARAN_HEPAR = tmphasil.CERNA_PEMBESARAN_HEPAR;
                                    CERNA_HEMATEMESIS = tmphasil.CERNA_HEMATEMESIS;
                                    CERNA_EVAKUASI_CAIRAN_ASCITES = tmphasil.CERNA_EVAKUASI_CAIRAN_ASCITES;
                                    CERNA_JML_CAIRAN_ASCITES = tmphasil.CERNA_JML_CAIRAN_ASCITES;
                                    CERNA_WARNA_CAIRAN_ASCITES = tmphasil.CERNA_WARNA_CAIRAN_ASCITES;
                                    CERNA_FREK_BAB = tmphasil.CERNA_FREK_BAB;
                                    CERNA_KONSISTENSI = tmphasil.CERNA_KONSISTENSI;
                                    CERNA_BAU_BAB = tmphasil.CERNA_BAU_BAB;
                                    CERNA_WARNA_BAB = tmphasil.CERNA_WARNA_BAB;
                                    CERNA_MASALAH = tmphasil.CERNA_MASALAH;
                                    KEMIH_KEBERSIHAN = tmphasil.KEMIH_KEBERSIHAN;
                                    KEMIH_ALAT_BANTU = tmphasil.KEMIH_ALAT_BANTU;
                                    KEMIH_JML_URINE = tmphasil.KEMIH_JML_URINE;
                                    KEMIH_WARNA_URINE = tmphasil.KEMIH_WARNA_URINE;
                                    KEMIH_BAU = tmphasil.KEMIH_BAU;
                                    KEMIH_KANDUNG_MEMBESAR = tmphasil.KEMIH_KANDUNG_MEMBESAR;
                                    KEMIH_NYERI_TEKAN = tmphasil.KEMIH_NYERI_TEKAN;
                                    KEMIH_GANGGUAN_ANURIA = tmphasil.KEMIH_GANGGUAN_ANURIA;
                                    KEMIH_GANGGUAN_ALIGURIA = tmphasil.KEMIH_GANGGUAN_ALIGURIA;
                                    KEMIH_GANGGUAN_RETENSI = tmphasil.KEMIH_GANGGUAN_RETENSI;
                                    KEMIH_GANGGUAN_INKONTINENSIA = tmphasil.KEMIH_GANGGUAN_INKONTINENSIA;
                                    KEMIH_GANGGUAN_DISURIA = tmphasil.KEMIH_GANGGUAN_DISURIA;
                                    KEMIH_GANGGUANHEMATURIA = tmphasil.KEMIH_GANGGUANHEMATURIA;
                                    KEMIH_MASALAH = tmphasil.KEMIH_MASALAH;
                                    //console.log(ENDOKRIN_TYROID + ' x ' + ENDOKRIN_HIPOGLIKEMIA + ' x ' + ENDOKRIN_LUKA_GANGREN + ' x ' + ENDOKRIN__PUS + ' x ' + ENDOKRIN_MASALAH);

                                    ENDOKRIN_TYROID = tmphasil.ENDOKRIN_TYROID;
                                    ENDOKRIN_HIPOGLIKEMIA = tmphasil.ENDOKRIN_HIPOGLIKEMIA;
                                    ENDOKRIN_LUKA_GANGREN = tmphasil.ENDOKRIN_LUKA_GANGREN;
                                    ENDOKRIN__PUS = tmphasil.ENDOKRIN__PUS;
                                    ENDOKRIN_MASALAH = tmphasil.ENDOKRIN_MASALAH;
                                    PERSONAL_MANDI = tmphasil.PERSONAL_MANDI;
                                    PERSONAL_SIKATGIGI = tmphasil.PERSONAL_SIKATGIGI;
                                    PERSONAL_KERAMAS = tmphasil.PERSONAL_KERAMAS;
                                    PERSONAL_GANTIPAKAIAN = tmphasil.PERSONAL_GANTIPAKAIAN;
                                    PERSONAL_MASALAH = tmphasil.PERSONAL_MASALAH;
                                    PSIKOSOSIAL_ORANGDEKAT = tmphasil.PSIKOSOSIAL_ORANGDEKAT;
                                    PSIKOSOSIAL_KEGIATAN_IBADAH = tmphasil.PSIKOSOSIAL_KEGIATAN_IBADAH;
                                    PSIKOSOSIAL_MASALAH = tmphasil.PSIKOSOSIAL_MASALAH;
                                    TERAPI_PENUNJANG = tmphasil.TERAPI_PENUNJANG;
                                    //asu -_-

//radiogroup & Chekboxgroup
//tab2 ----------------------------------------------------------------------------------------------------------------------------------
                                    if (tmphasil.NAFAS_PATEN === "t" || tmphasil.NAFAS_OBSTRUKTIF === "t")
                                    {
                                        if (tmphasil.NAFAS_PATEN === "t" && tmphasil.NAFAS_OBSTRUKTIF === "t") {
                                            Ext.getCmp('cgjelannapas').setValue([true, true]);
                                        } else if (tmphasil.NAFAS_PATEN === "t" && tmphasil.NAFAS_OBSTRUKTIF === "f") {
                                            Ext.getCmp('cgjelannapas').setValue([true, false]);
                                        } else if (tmphasil.NAFAS_PATEN === "f" && tmphasil.NAFAS_OBSTRUKTIF === "t")
                                        {
                                            Ext.getCmp('cgjelannapas').setValue([false, true]);
                                        }
                                    }
                                    else {
                                        Ext.getCmp('cgjelannapas').setValue([false, false]);
                                    }

                                    if (tmphasil.NAFAS_JELAS === "t")
                                    {
                                        Ext.getCmp('rgjelas').setValue("rbjelasya", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgjelas').setValue("rbjelastidak", true);
                                    }

                                    if (tmphasil.NAFAS_POLA_SIMETRI === "t" || tmphasil.NAFAS_POLA_ASIMETRI === "t")
                                    {
                                        if (tmphasil.NAFAS_POLA_SIMETRI === "t" && tmphasil.NAFAS_POLA_ASIMETRI === "t") {
                                            Ext.getCmp('cgpolanapas').setValue([true, true]);
                                        } else if (tmphasil.NAFAS_POLA_SIMETRI === "t" && tmphasil.NAFAS_POLA_ASIMETRI === "f") {
                                            Ext.getCmp('cgpolanapas').setValue([true, false]);
                                        } else if (tmphasil.NAFAS_POLA_SIMETRI === "f" && tmphasil.NAFAS_POLA_ASIMETRI === "t")
                                        {
                                            Ext.getCmp('cgpolanapas').setValue([false, true]);
                                        }
                                    }
                                    else {
                                        Ext.getCmp('cgpolanapas').setValue([false, false]);
                                    }

                                    if (tmphasil.NAFAS_SUARA_NORMAL === "t" || tmphasil.NAFAS_SUARA_HIPERSONOR === "t" || tmphasil.NAFAS_MENURUN === "t")
                                    {
                                        if (tmphasil.NAFAS_SUARA_NORMAL === "t" && tmphasil.NAFAS_SUARA_HIPERSONOR === "t" && tmphasil.NAFAS_MENURUN === "t") {
                                            Ext.getCmp('cgsuaralapangparu').setValue([true, true, true]);
                                        } else if (tmphasil.NAFAS_SUARA_NORMAL === "t" && tmphasil.NAFAS_SUARA_HIPERSONOR === "f" && tmphasil.NAFAS_MENURUN === "f") {
                                            Ext.getCmp('cgsuaralapangparu').setValue([true, false, false]);
                                        }
                                        else if (tmphasil.NAFAS_SUARA_NORMAL === "t" && tmphasil.NAFAS_SUARA_HIPERSONOR === "t" && tmphasil.NAFAS_MENURUN === "f") {
                                            Ext.getCmp('cgsuaralapangparu').setValue([true, true, false]);
                                        }
                                        else if (tmphasil.NAFAS_SUARA_NORMAL === "t" && tmphasil.NAFAS_SUARA_HIPERSONOR === "f" && tmphasil.NAFAS_MENURUN === "t") {
                                            Ext.getCmp('cgsuaralapangparu').setValue([true, false, true]);
                                        } else if (tmphasil.NAFAS_SUARA_NORMAL === "f" && tmphasil.NAFAS_SUARA_HIPERSONOR === "t" && tmphasil.NAFAS_MENURUN === "f")
                                        {
                                            Ext.getCmp('cgsuaralapangparu').setValue([false, true, false]);
                                        }
                                        else if (tmphasil.NAFAS_SUARA_NORMAL === "f" && tmphasil.NAFAS_SUARA_HIPERSONOR === "t" && tmphasil.NAFAS_MENURUN === "t")
                                        {
                                            Ext.getCmp('cgsuaralapangparu').setValue([false, true, true]);
                                        } else if (tmphasil.NAFAS_SUARA_NORMAL === "f" && tmphasil.NAFAS_SUARA_HIPERSONOR === "f" && tmphasil.NAFAS_MENURUN === "t")
                                        {
                                            Ext.getCmp('cgsuaralapangparu').setValue([false, false, true]);
                                        }
                                    }
                                    else {
                                        Ext.getCmp('cgsuaralapangparu').setValue([false, false, false]);
                                    }

                                    if (tmphasil.NAFAS_JENIS_NORMAL === "t" || tmphasil.NAFAS_JENIS_TACHYPNOE === "t" || tmphasil.NAFAS_JENIS_CHEYNESTOKES === "t" || tmphasil.NAFAS_JENIS_RETRACTIVE === "t" || tmphasil.NAFAS_JENIS_KUSMAUL === "t" || tmphasil.NAFAS_JENIS_DISPNOE === "t")
                                    {
                                        if (tmphasil.NAFAS_JENIS_NORMAL === "t" && tmphasil.NAFAS_JENIS_TACHYPNOE === "t" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "t" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "t" && tmphasil.NAFAS_JENIS_KUSMAUL === "t" && tmphasil.NAFAS_JENIS_DISPNOE === "t") {
                                            Ext.getCmp('cgjenis').setValue([true, true, true, true, true, true]);
                                        }                                         //1
                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "t" && tmphasil.NAFAS_JENIS_TACHYPNOE === "f" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "f" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "f" && tmphasil.NAFAS_JENIS_KUSMAUL === "f" && tmphasil.NAFAS_JENIS_DISPNOE === "f") {
                                            Ext.getCmp('cgjenis').setValue([true, false, false, false, false, false]);
                                        }
                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "f" && tmphasil.NAFAS_JENIS_TACHYPNOE === "t" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "f" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "f" && tmphasil.NAFAS_JENIS_KUSMAUL === "f" && tmphasil.NAFAS_JENIS_DISPNOE === "f") {
                                            Ext.getCmp('cgjenis').setValue([false, true, false, false, false, false]);
                                        }

                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "f" && tmphasil.NAFAS_JENIS_TACHYPNOE === "f" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "t" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "f" && tmphasil.NAFAS_JENIS_KUSMAUL === "f" && tmphasil.NAFAS_JENIS_DISPNOE === "f") {
                                            Ext.getCmp('cgjenis').setValue([false, false, true, false, false, false]);
                                        }

                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "f" && tmphasil.NAFAS_JENIS_TACHYPNOE === "f" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "f" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "t" && tmphasil.NAFAS_JENIS_KUSMAUL === "f" && tmphasil.NAFAS_JENIS_DISPNOE === "f") {
                                            Ext.getCmp('cgjenis').setValue([false, false, false, true, false, false]);
                                        }

                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "f" && tmphasil.NAFAS_JENIS_TACHYPNOE === "f" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "f" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "f" && tmphasil.NAFAS_JENIS_KUSMAUL === "t" && tmphasil.NAFAS_JENIS_DISPNOE === "f") {
                                            Ext.getCmp('cgjenis').setValue([false, false, false, false, true, false]);
                                        }

                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "f" && tmphasil.NAFAS_JENIS_TACHYPNOE === "f" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "f" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "f" && tmphasil.NAFAS_JENIS_KUSMAUL === "f" && tmphasil.NAFAS_JENIS_DISPNOE === "t") {
                                            Ext.getCmp('cgjenis').setValue([false, false, false, false, false, true]);
                                        }
                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "t" && tmphasil.NAFAS_JENIS_TACHYPNOE === "t" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "f" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "f" && tmphasil.NAFAS_JENIS_KUSMAUL === "f" && tmphasil.NAFAS_JENIS_DISPNOE === "f") {
                                            Ext.getCmp('cgjenis').setValue([true, true, false, false, false, false]);
                                        }

                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "t" && tmphasil.NAFAS_JENIS_TACHYPNOE === "t" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "t" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "f" && tmphasil.NAFAS_JENIS_KUSMAUL === "f" && tmphasil.NAFAS_JENIS_DISPNOE === "f") {
                                            Ext.getCmp('cgjenis').setValue([true, true, true, false, false, false]);
                                        }

                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "t" && tmphasil.NAFAS_JENIS_TACHYPNOE === "t" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "t" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "t" && tmphasil.NAFAS_JENIS_KUSMAUL === "f" && tmphasil.NAFAS_JENIS_DISPNOE === "f") {
                                            Ext.getCmp('cgjenis').setValue([true, true, true, true, false, false]);
                                        }

                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "t" && tmphasil.NAFAS_JENIS_TACHYPNOE === "t" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "t" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "t" && tmphasil.NAFAS_JENIS_KUSMAUL === "t" && tmphasil.NAFAS_JENIS_DISPNOE === "f") {
                                            Ext.getCmp('cgjenis').setValue([true, true, true, true, true, false]);
                                        }
                                        //2
                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "f" && tmphasil.NAFAS_JENIS_TACHYPNOE === "t" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "f" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "f" && tmphasi.lNAFAS_JENIS_KUSMAUL === "f" && tmphasil.NAFAS_JENIS_DISPNOE === "f") {
                                            Ext.getCmp('cgjenis').setValue([false, true, false, false, false, false]);
                                        }

                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "f" && tmphasil.NAFAS_JENIS_TACHYPNOE === "t" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "t" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "f" && tmphasil.NAFAS_JENIS_KUSMAUL === "f" && tmphasil.NAFAS_JENIS_DISPNOE === "f") {
                                            Ext.getCmp('cgjenis').setValue([false, true, true, false, false, false]);
                                        }

                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "f" && tmphasil.NAFAS_JENIS_TACHYPNOE === "t" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "t" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "t" && tmphasil.NAFAS_JENIS_KUSMAUL === "f" && tmphasil.NAFAS_JENIS_DISPNOE === "f") {
                                            Ext.getCmp('cgjenis').setValue([false, true, true, true, false, false]);
                                        }

                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "f" && tmphasil.NAFAS_JENIS_TACHYPNOE === "t" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "t" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "t" && tmphasil.NAFAS_JENIS_KUSMAUL === "t" && tmphasil.NAFAS_JENIS_DISPNOE === "f") {
                                            Ext.getCmp('cgjenis').setValue([false, true, true, true, true, false]);
                                        }

                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "f" && tmphasil.NAFAS_JENIS_TACHYPNOE === "t" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "t" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "t" && tmphasil.NAFAS_JENIS_KUSMAUL === "t" && tmphasil.NAFAS_JENIS_DISPNOE === "t") {
                                            Ext.getCmp('cgjenis').setValue([false, true, true, true, true, true]);
                                        }                                         //3
                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "f" && tmphasil.NAFAS_JENIS_TACHYPNOE === "f" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "t" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "f" && tmphasil.NAFAS_JENIS_KUSMAUL === "f" && tmphasil.NAFAS_JENIS_DISPNOE === "f") {
                                            Ext.getCmp('cgjenis').setValue([false, false, false, true, false, false]);
                                        }

                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "f" && tmphasil.NAFAS_JENIS_TACHYPNOE === "f" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "t" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "t" && tmphasil.NAFAS_JENIS_KUSMAUL === "f" && tmphasil.NAFAS_JENIS_DISPNOE === "f") {
                                            Ext.getCmp('cgjenis').setValue([false, false, true, true, false, false]);
                                        }

                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "f" && tmphasil.NAFAS_JENIS_TACHYPNOE === "f" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "t" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "t" && tmphasil.NAFAS_JENIS_KUSMAUL === "t" && tmphasil.NAFAS_JENIS_DISPNOE === "f") {
                                            Ext.getCmp('cgjenis').setValue([false, false, true, true, true, false]);
                                        }

                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "f" && tmphasil.NAFAS_JENIS_TACHYPNOE === "f" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "t" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "t" && tmphasil.NAFAS_JENIS_KUSMAUL === "t" && tmphasil.NAFAS_JENIS_DISPNOE === "t") {
                                            Ext.getCmp('cgjenis').setValue([false, false, true, true, true, true]);
                                        }
                                        //4
                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "f" && tmphasil.NAFAS_JENIS_TACHYPNOE === "f" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "f" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "t" && tmphasil.NAFAS_JENIS_KUSMAUL === "f" && tmphasil.NAFAS_JENIS_DISPNOE === "f") {
                                            Ext.getCmp('cgjenis').setValue([false, false, false, true, false, false]);
                                        }

                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "f" && tmphasil.NAFAS_JENIS_TACHYPNOE === "f" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "f" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "t" && tmphasil.NAFAS_JENIS_KUSMAUL === "t" && tmphasil.NAFAS_JENIS_DISPNOE === "f") {
                                            Ext.getCmp('cgjenis').setValue([false, false, false, true, true, false]);
                                        }

                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "f" && tmphasil.NAFAS_JENIS_TACHYPNOE === "f" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "f" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "t" && tmphasil.NAFAS_JENIS_KUSMAUL === "t" && tmphasil.NAFAS_JENIS_DISPNOE === "t") {
                                            Ext.getCmp('cgjenis').setValue([false, false, false, true, true, true]);
                                        }                                         //5
                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "f" && tmphasil.NAFAS_JENIS_TACHYPNOE === "f" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "f" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "f" && tmphasil.NAFAS_JENIS_KUSMAUL === "t" && tmphasil.NAFAS_JENIS_DISPNOE === "f") {
                                            Ext.getCmp('cgjenis').setValue([false, false, false, false, true, false]);
                                        }

                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "f" && tmphasil.NAFAS_JENIS_TACHYPNOE === "f" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "f" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "f" && tmphasil.NAFAS_JENIS_KUSMAUL === "t" && tmphasil.NAFAS_JENIS_DISPNOE === "t") {
                                            Ext.getCmp('cgjenis').setValue([false, false, false, false, true, true]);
                                        }
                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "t" && tmphasil.NAFAS_JENIS_TACHYPNOE === "f" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "f" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "f" && tmphasil.NAFAS_JENIS_KUSMAUL === "t" && tmphasil.NAFAS_JENIS_DISPNOE === "t") {
                                            Ext.getCmp('cgjenis').setValue([true, false, false, false, true, true]);
                                        }
                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "t" && tmphasil.NAFAS_JENIS_TACHYPNOE === "t" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "f" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "f" && tmphasil.NAFAS_JENIS_KUSMAUL === "t" && tmphasil.NAFAS_JENIS_DISPNOE === "t") {
                                            Ext.getCmp('cgjenis').setValue([true, true, false, false, true, true]);
                                        }
                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "t" && tmphasil.NAFAS_JENIS_TACHYPNOE === "t" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "t" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "f" && tmphasil.NAFAS_JENIS_KUSMAUL === "t" && tmphasil.NAFAS_JENIS_DISPNOE === "t") {
                                            Ext.getCmp('cgjenis').setValue([true, true, true, false, true, true]);
                                        }                                         //6
                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "f" && tmphasil.NAFAS_JENIS_TACHYPNOE === "f" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "f" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "f" && tmphasil.NAFAS_JENIS_KUSMAUL === "f" && tmphasil.NAFAS_JENIS_DISPNOE === "t") {
                                            Ext.getCmp('cgjenis').setValue([false, false, false, false, false, true]);
                                        }
                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "t" && tmphasil.NAFAS_JENIS_TACHYPNOE === "f" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "f" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "f" && tmphasil.NAFAS_JENIS_KUSMAUL === "f" && tmphasil.NAFAS_JENIS_DISPNOE === "t") {
                                            Ext.getCmp('cgjenis').setValue([true, false, false, false, false, true]);
                                        }
                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "t" && tmphasil.NAFAS_JENIS_TACHYPNOE === "t" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "f" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "f" && tmphasil.NAFAS_JENIS_KUSMAUL === "f" && tmphasil.NAFAS_JENIS_DISPNOE === "t") {
                                            Ext.getCmp('cgjenis').setValue([true, true, false, false, false, true]);
                                        }
                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "t" && tmphasil.NAFAS_JENIS_TACHYPNOE === "t" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "t" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "f" && tmphasil.NAFAS_JENIS_KUSMAUL === "f" && tmphasil.NAFAS_JENIS_DISPNOE === "t") {
                                            Ext.getCmp('cgjenis').setValue([true, true, true, false, false, true]);
                                        }

                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "t" && tmphasil.NAFAS_JENIS_TACHYPNOE === "t" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "t" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "t" && tmphasil.NAFAS_JENIS_KUSMAUL === "f" && tmphasil.NAFAS_JENIS_DISPNOE === "t") {
                                            Ext.getCmp('cgjenis').setValue([true, true, true, true, false, true]);
                                        }
                                    }
                                    else {
                                        Ext.getCmp('cgjenis').setValue([false, false, false, false, false, false]);
                                    }


                                    if (tmphasil.NAFAS_SUARA_WHEEZING === "t" || tmphasil.NAFAS_SUARA_RONCHI === "t" || tmphasil.NAFAS_SUARA_RALES === "t")
                                    {
                                        if (tmphasil.NAFAS_SUARA_WHEEZING === "t" && tmphasil.NAFAS_SUARA_RONCHI === "t" && tmphasil.NAFAS_SUARA_RALES === "t") {
                                            Ext.getCmp('cgsuaranafastambahan').setValue([true, true, true]);
                                        } else if (tmphasil.NAFAS_SUARA_WHEEZING === "t" && tmphasil.NAFAS_SUARA_RONCHI === "f" && tmphasil.NAFAS_SUARA_RALES === "f") {
                                            Ext.getCmp('cgsuaranafastambahan').setValue([true, false, false]);
                                        }
                                        else if (tmphasil.NAFAS_SUARA_WHEEZING === "t" && tmphasil.NAFAS_SUARA_RONCHI === "t" && tmphasil.NAFAS_SUARA_RALES === "f") {
                                            Ext.getCmp('cgsuaranafastambahan').setValue([true, true, false]);
                                        }
                                        else if (tmphasil.NAFAS_SUARA_WHEEZING === "t" && tmphasil.NAFAS_SUARA_RONCHI === "f" && tmphasil.NAFAS_SUARA_RALES === "t") {
                                            Ext.getCmp('cgsuaranafastambahan').setValue([true, false, true]);
                                        } else if (tmphasil.NAFAS_SUARA_WHEEZING === "f" && tmphasil.NAFAS_SUARA_RONCHI === "t" && tmphasil.NAFAS_SUARA_RALES === "f")
                                        {
                                            Ext.getCmp('cgsuaranafastambahan').setValue([false, true, false]);
                                        }
                                        else if (tmphasil.NAFAS_SUARA_WHEEZING === "f" && tmphasil.NAFAS_SUARA_RONCHI === "t" && tmphasil.NAFAS_SUARA_RALES === "t")
                                        {
                                            Ext.getCmp('cgsuaranafastambahan').setValue([false, true, true]);
                                        } else if (tmphasil.NAFAS_SUARA_WHEEZING === "f" && tmphasil.NAFAS_SUARA_RONCHI === "f" && tmphasil.NAFAS_SUARA_RALES === "t")
                                        {
                                            Ext.getCmp('cgsuaranafastambahan').setValue([false, false, true]);
                                        }
                                    }
                                    else {
                                        Ext.getCmp('cgsuaranafastambahan').setValue([false, false, false]);
                                    }

                                    if (tmphasil.NAFAS_EVAKUASI_CAIRAN === "t")
                                    {
                                        Ext.getCmp('rgefakuasi').setValue("rbefakuasiya", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgefakuasi').setValue("rbefakuasitidak", true);
                                    }
                                    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//tab3 ----------------------------------------------------------------------------------------------------------------------------------
                                    if (tmphasil.JANTUNG_REGULER === "t" || tmphasil.JANTUNG_IRREGULER === "t")
                                    {
                                        if (tmphasil.JANTUNG_REGULER === "t" && tmphasil.JANTUNG_IRREGULER === "t") {
                                            Ext.getCmp('cgIramaJantung').setValue([true, true]);
                                        } else if (tmphasil.JANTUNG_REGULER === "t" && tmphasil.JANTUNG_IRREGULER === "f") {
                                            Ext.getCmp('cgIramaJantung').setValue([true, false]);
                                        } else if (tmphasil.JANTUNG_REGULER === "f" && tmphasil.JANTUNG_IRREGULER === "t")
                                        {
                                            Ext.getCmp('cgIramaJantung').setValue([false, true]);
                                        }
                                    }
                                    else {
                                        Ext.getCmp('cgIramaJantung').setValue([false, false]);
                                    }

                                    if (tmphasil.JANTUNG_S1S2_TUNGGAL === "t")
                                    {
                                        Ext.getCmp('rgS1S2Tunggal').setValue("rbS1S2Tunggalya", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgS1S2Tunggal').setValue("rbS1S2Tunggaltidak", true);
                                    }

                                    if (tmphasil.JANTUNG_NYERI_DADA === "t")
                                    {
                                        Ext.getCmp('rgNyeridada').setValue("rgNyeridadaya", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgNyeridada').setValue("rgNyeridadatidak", true);
                                    }

                                    if (tmphasil.JANTUNG_BUNYI_MURMUR === "t" || tmphasil.JANTUNG_BUNYI_GALLOP === "t" || tmphasil.JANTUNG_BUNYI_BISING === "t") {
                                        if (tmphasil.JANTUNG_BUNYI_MURMUR === "t" && tmphasil.JANTUNG_BUNYI_GALLOP === "t" && tmphasil.JANTUNG_BUNYI_BISING === "t") {
                                            Ext.getCmp('cgBunyiJantungTambahan').setValue([true, true, true]);
                                        } else if (tmphasil.JANTUNG_BUNYI_MURMUR === "t" && tmphasil.JANTUNG_BUNYI_GALLOP === "f" && tmphasil.JANTUNG_BUNYI_BISING === "f") {
                                            Ext.getCmp('cgBunyiJantungTambahan').setValue([true, false, false]);
                                        }
                                        else if (tmphasil.JANTUNG_BUNYI_MURMUR === "t" && tmphasil.JANTUNG_BUNYI_GALLOP === "t" && tmphasil.JANTUNG_BUNYI_BISING === "f") {
                                            Ext.getCmp('cgBunyiJantungTambahan').setValue([true, true, false]);
                                        }
                                        else if (tmphasil.JANTUNG_BUNYI_MURMUR === "t" && tmphasil.JANTUNG_BUNYI_GALLOP === "f" && tmphasil.JANTUNG_BUNYI_BISING === "t") {
                                            Ext.getCmp('cgBunyiJantungTambahan').setValue([true, false, true]);
                                        } else if (tmphasil.JANTUNG_BUNYI_MURMUR === "f" && tmphasil.JANTUNG_BUNYI_GALLOP === "t" && tmphasil.JANTUNG_BUNYI_BISING === "f") {
                                            Ext.getCmp('cgBunyiJantungTambahan').setValue([false, true, false]);
                                        }
                                        else if (tmphasil.JANTUNG_BUNYI_MURMUR === "f" && tmphasil.JANTUNG_BUNYI_GALLOP === "t" && tmphasil.JANTUNG_BUNYI_BISING === "t") {
                                            Ext.getCmp('cgBunyiJantungTambahan').setValue([false, true, true]);
                                        } else if (tmphasil.JANTUNG_BUNYI_MURMUR === "f" && tmphasil.JANTUNG_BUNYI_GALLOP === "f" && tmphasil.JANTUNG_BUNYI_BISING === "t") {
                                            Ext.getCmp('cgBunyiJantungTambahan').setValue([false, false, true]);
                                        }
                                    }
                                    else {
                                        Ext.getCmp('cgBunyiJantungTambahan').setValue([false, false, false]);
                                    }

                                    if (tmphasil.JANTUNG_CRT === "t")
                                    {
                                        Ext.getCmp('rgCRT').setValue("rgCRTkurang", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgCRT').setValue("rgCRTlebih", true);
                                    }

                                    if (tmphasil.JANTUNG_AKRAL_HANGAT === "t" || tmphasil.JANTUNG_AKRAL_DINGIN === "t") {
                                        if (tmphasil.JANTUNG_AKRAL_HANGAT === "t" && tmphasil.JANTUNG_AKRAL_DINGIN === "t") {
                                            Ext.getCmp('cgAkral').setValue([true, true]);
                                        } else if (tmphasil.JANTUNG_AKRAL_HANGAT === "t" && tmphasil.JANTUNG_AKRAL_DINGIN === "f") {
                                            Ext.getCmp('cgAkral').setValue([true, false]);
                                        } else if (tmphasil.JANTUNG_AKRAL_HANGAT === "f" && tmphasil.JANTUNG_AKRAL_DINGIN === "t") {
                                            Ext.getCmp('cgAkral').setValue([false, true]);
                                        }
                                    }
                                    else {
                                        Ext.getCmp('cgAkral').setValue([false, false]);
                                    }

                                    if (tmphasil.JANTUNG_PENINGKATAN_JVP === "t")
                                    {
                                        Ext.getCmp('rgJVP').setValue("rgJVPya", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgJVP').setValue("rgJVPtidak", true);
                                    }

                                    if (tmphasil.JANTUNG_WARNA_JAUNDICE === "t" || tmphasil.JANTUNG_WARNA_SIANOTIK === "t" || tmphasil.JANTUNG_WARNA_KEMERAHAN === "t" || tmphasil.JANTUNG_WARNA_PUCAT === "t")
                                    {
                                        if (tmphasil.JANTUNG_WARNA_JAUNDICE === "t" && tmphasil.JANTUNG_WARNA_SIANOTIK === "t" && tmphasil.JANTUNG_WARNA_KEMERAHAN === "t" && tmphasil.JANTUNG_WARNA_PUCAT === "t") {
                                            Ext.getCmp('cgWarnaKulit').setValue([true, true, true, true]);
                                        }                                         //1
                                        else if (tmphasil.JANTUNG_WARNA_JAUNDICE === "t" && tmphasil.JANTUNG_WARNA_SIANOTIK === "f" && tmphasil.JANTUNG_WARNA_KEMERAHAN === "f" && tmphasil.JANTUNG_WARNA_PUCAT === "f") {
                                            Ext.getCmp('cgWarnaKulit').setValue([true, false, false, false]);
                                        }

                                        else if (tmphasil.JANTUNG_WARNA_JAUNDICE === "t" && tmphasil.JANTUNG_WARNA_SIANOTIK === "t" && tmphasil.JANTUNG_WARNA_KEMERAHAN === "f" && tmphasil.JANTUNG_WARNA_PUCAT === "f") {
                                            Ext.getCmp('cgWarnaKulit').setValue([true, true, false, false]);
                                        }

                                        else if (tmphasil.JANTUNG_WARNA_JAUNDICE === "t" && tmphasil.JANTUNG_WARNA_SIANOTIK === "t" && tmphasil.JANTUNG_WARNA_KEMERAHAN === "t" && tmphasil.JANTUNG_WARNA_PUCAT === "f") {
                                            Ext.getCmp('cgWarnaKulit').setValue([true, true, true, false]);
                                        }
                                        //2
                                        else if (tmphasil.JANTUNG_WARNA_JAUNDICE === "f" && tmphasil.JANTUNG_WARNA_SIANOTIK === "t" && tmphasil.JANTUNG_WARNA_KEMERAHAN === "f" && tmphasil.JANTUNG_WARNA_PUCAT === "f") {
                                            Ext.getCmp('cgWarnaKulit').setValue([false, true, false, false]);
                                        }

                                        else if (tmphasil.JANTUNG_WARNA_JAUNDICE === "f" && tmphasil.JANTUNG_WARNA_SIANOTIK === "t" && tmphasil.JANTUNG_WARNA_KEMERAHAN === "t" && tmphasil.JANTUNG_WARNA_PUCAT === "f") {
                                            Ext.getCmp('cgWarnaKulit').setValue([false, true, true, false]);
                                        }

                                        else if (tmphasil.JANTUNG_WARNA_JAUNDICE === "f" && tmphasil.JANTUNG_WARNA_SIANOTIK === "t" && tmphasil.JANTUNG_WARNA_KEMERAHAN === "t" && tmphasil.JANTUNG_WARNA_PUCAT === "t") {
                                            Ext.getCmp('cgWarnaKulit').setValue([false, true, true, true]);
                                        }                                         //3
                                        else if (tmphasil.JANTUNG_WARNA_JAUNDICE === "f" && tmphasil.JANTUNG_WARNA_SIANOTIK === "f" && tmphasil.JANTUNG_WARNA_KEMERAHAN === "t" && tmphasil.JANTUNG_WARNA_PUCAT === "f") {
                                            Ext.getCmp('cgWarnaKulit').setValue([false, false, true, false]);
                                        }

                                        else if (tmphasil.JANTUNG_WARNA_JAUNDICE === "f" && tmphasil.JANTUNG_WARNA_SIANOTIK === "f" && tmphasil.JANTUNG_WARNA_KEMERAHAN === "t" && tmphasil.JANTUNG_WARNA_PUCAT === "t") {
                                            Ext.getCmp('cgWarnaKulit').setValue([false, false, true, true]);
                                        }
                                        else if (tmphasil.JANTUNG_WARNA_JAUNDICE === "t" && tmphasil.JANTUNG_WARNA_SIANOTIK === "f" && tmphasil.JANTUNG_WARNA_KEMERAHAN === "t" && tmphasil.JANTUNG_WARNA_PUCAT === "t") {
                                            Ext.getCmp('cgWarnaKulit').setValue([true, false, true, true]);
                                        }                                         //4
                                        else if (tmphasil.JANTUNG_WARNA_JAUNDICE === "f" && tmphasil.JANTUNG_WARNA_SIANOTIK === "f" && tmphasil.JANTUNG_WARNA_KEMERAHAN === "f" && tmphasil.JANTUNG_WARNA_PUCAT === "t") {
                                            Ext.getCmp('cgWarnaKulit').setValue([false, false, false, true]);
                                        }
                                        else if (tmphasil.JANTUNG_WARNA_JAUNDICE === "t" && tmphasil.JANTUNG_WARNA_SIANOTIK === "f" && tmphasil.JANTUNG_WARNA_KEMERAHAN === "f" && tmphasil.JANTUNG_WARNA_PUCAT === "t") {
                                            Ext.getCmp('cgWarnaKulit').setValue([true, false, false, true]);
                                        }
                                        else if (tmphasil.JANTUNG_WARNA_JAUNDICE === "t" && tmphasil.JANTUNG_WARNA_SIANOTIK === "t" && tmphasil.JANTUNG_WARNA_KEMERAHAN === "f" && tmphasil.JANTUNG_WARNA_PUCAT === "t") {
                                            Ext.getCmp('cgWarnaKulit').setValue([true, true, false, true]);
                                        }
                                    }
                                    else {
                                        Ext.getCmp('cgWarnaKulit').setValue([false, false, false, false]);
                                    }
                                    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//tab4 -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                                    if (tmphasil.OTOT_SENDI_BEBAS === "t" || tmphasil.OTOT_SENDI_TERBATAS === "t")
                                    {
                                        if (tmphasil.OTOT_SENDI_BEBAS === "t" && tmphasil.OTOT_SENDI_TERBATAS === "t") {
                                            Ext.getCmp('cgPergerakanSendi').setValue([true, true]);
                                        } else if (tmphasil.OTOT_SENDI_BEBAS === "t" && tmphasil.OTOT_SENDI_TERBATAS === "f") {
                                            Ext.getCmp('cgPergerakanSendi').setValue([true, false]);
                                        } else if (tmphasil.OTOT_SENDI_BEBAS === "f" && tmphasil.OTOT_SENDI_TERBATAS === "t")
                                        {
                                            Ext.getCmp('cgPergerakanSendi').setValue([false, true]);
                                        }
                                    }
                                    else {
                                        Ext.getCmp('cgPergerakanSendi').setValue([false, false]);
                                    }

                                    if (tmphasil.OTOT_ODEMA_EKSTRIMITAS === "t")
                                    {
                                        Ext.getCmp('rgekstrimitas').setValue("rgekstrimitasya", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgekstrimitas').setValue("rgekstrimitastidak", true);
                                    }

                                    if (tmphasil.OTOT_KELAINAN_BENTUK === "t")
                                    {
                                        Ext.getCmp('rgKelainan').setValue("rgKelainanya", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgKelainan').setValue("rgKelainantidak", true);
                                    }

                                    if (tmphasil.OTOT_KREPITASI === "t")
                                    {
                                        Ext.getCmp('rgKrepitas').setValue("rgKrepitasya", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgKrepitas').setValue("rgKrepitastidak", true);
                                    }
                                    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//tab5 -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                                    if (tmphasil.SYARAF_GCS_EYE === "t" || tmphasil.SYARAF_GCS_VERBAL === "t" || tmphasil.SYARAF_GCS_MOTORIK === "t")
                                    {
                                        if (tmphasil.SYARAF_GCS_EYE === "t" && tmphasil.SYARAF_GCS_VERBAL === "t" && tmphasil.SYARAF_GCS_MOTORIK === "t") {
                                            Ext.getCmp('cgGCS').setValue([true, true, true]);
                                        } else if (tmphasil.SYARAF_GCS_EYE === "t" && tmphasil.SYARAF_GCS_VERBAL === "f" && tmphasil.SYARAF_GCS_MOTORIK === "f") {
                                            Ext.getCmp('cgGCS').setValue([true, false, false]);
                                        }
                                        else if (tmphasil.SYARAF_GCS_EYE === "t" && tmphasil.SYARAF_GCS_VERBAL === "t" && tmphasil.SYARAF_GCS_MOTORIK === "f") {
                                            Ext.getCmp('cgGCS').setValue([true, true, false]);
                                        }
                                        else if (tmphasil.SYARAF_GCS_EYE === "t" && tmphasil.SYARAF_GCS_VERBAL === "f" && tmphasil.SYARAF_GCS_MOTORIK === "t") {
                                            Ext.getCmp('cgGCS').setValue([true, false, true]);
                                        } else if (tmphasil.SYARAF_GCS_EYE === "f" && tmphasil.SYARAF_GCS_VERBAL === "t" && tmphasil.SYARAF_GCS_MOTORIK === "f")
                                        {
                                            Ext.getCmp('cgGCS').setValue([false, true, false]);
                                        }
                                        else if (tmphasil.SYARAF_GCS_EYE === "f" && tmphasil.SYARAF_GCS_VERBAL === "t" && tmphasil.SYARAF_GCS_MOTORIK === "t")
                                        {
                                            Ext.getCmp('cgGCS').setValue([false, true, true]);
                                        } else if (tmphasil.SYARAF_GCS_EYE === "f" && tmphasil.SYARAF_GCS_VERBAL === "f" && tmphasil.SYARAF_GCS_MOTORIK === "t")
                                        {
                                            Ext.getCmp('cgGCS').setValue([false, false, true]);
                                        }
                                    }
                                    else {
                                        Ext.getCmp('cgGCS').setValue([false, false, false]);
                                    }

                                    if (tmphasil.SYARAF_FISIOLOGIS_BRACHIALIS === "t" || tmphasil.SYARAF_FISIOLOGIS_PATELLA === "t" || tmphasil.SYARAF_FISIOLOGIS_ACHILLES === "t")
                                    {
                                        if (tmphasil.SYARAF_FISIOLOGIS_BRACHIALIS === "t" && tmphasil.SYARAF_FISIOLOGIS_PATELLA === "t" && tmphasil.SYARAF_FISIOLOGIS_ACHILLES === "t") {
                                            Ext.getCmp('cgRefleksi').setValue([true, true, true]);
                                        } else if (tmphasil.SYARAF_FISIOLOGIS_BRACHIALIS === "t" && tmphasil.SYARAF_FISIOLOGIS_PATELLA === "f" && tmphasil.SYARAF_FISIOLOGIS_ACHILLES === "f") {
                                            Ext.getCmp('cgRefleksi').setValue([true, false, false]);
                                        }
                                        else if (tmphasil.SYARAF_FISIOLOGIS_BRACHIALIS === "t" && tmphasil.SYARAF_FISIOLOGIS_PATELLA === "t" && tmphasil.SYARAF_FISIOLOGIS_ACHILLES === "f") {
                                            Ext.getCmp('cgRefleksi').setValue([true, true, false]);
                                        }
                                        else if (tmphasil.SYARAF_FISIOLOGIS_BRACHIALIS === "t" && tmphasil.SYARAF_FISIOLOGIS_PATELLA === "f" && tmphasil.SYARAF_FISIOLOGIS_ACHILLES === "t") {
                                            Ext.getCmp('cgRefleksi').setValue([true, false, true]);
                                        } else if (tmphasil.SYARAF_FISIOLOGIS_BRACHIALIS === "f" && tmphasil.SYARAF_FISIOLOGIS_PATELLA === "t" && tmphasil.SYARAF_FISIOLOGIS_ACHILLES === "f")
                                        {
                                            Ext.getCmp('cgRefleksi').setValue([false, true, false]);
                                        }
                                        else if (tmphasil.SYARAF_FISIOLOGIS_BRACHIALIS === "f" && tmphasil.SYARAF_FISIOLOGIS_PATELLA === "t" && tmphasil.SYARAF_FISIOLOGIS_ACHILLES === "t")
                                        {
                                            Ext.getCmp('cgRefleksi').setValue([false, true, true]);
                                        } else if (tmphasil.SYARAF_FISIOLOGIS_BRACHIALIS === "f" && tmphasil.SYARAF_FISIOLOGIS_PATELLA === "f" && tmphasil.SYARAF_FISIOLOGIS_ACHILLES === "t")
                                        {
                                            Ext.getCmp('cgRefleksi').setValue([false, false, true]);
                                        }
                                    }
                                    else {
                                        Ext.getCmp('cgRefleksi').setValue([false, false, false]);
                                    }

                                    if (tmphasil.SYARAF_PATOLOGIS_CHODDOKS === "t" || tmphasil.SYARAF_PATOLOGIS_BABINSKI === "t" ||
                                            tmphasil.SYARAF_PATOLOGIS_BUDZINZKY === "t" || tmphasil.SYARAF_PATOLOGIS_KERNIG === "t")
                                    {
                                        if (tmphasil.SYARAF_PATOLOGIS_CHODDOKS === "t" && tmphasil.SYARAF_PATOLOGIS_BABINSKI === "t" &&
                                                tmphasil.SYARAF_PATOLOGIS_BUDZINZKY === "t" && tmphasil.SYARAF_PATOLOGIS_KERNIG === "t") {
                                            Ext.getCmp('cgPatologis').setValue([true, true, true, true]);
                                        }                                         //1
                                        else if (tmphasil.SYARAF_PATOLOGIS_CHODDOKS === "t" && tmphasil.SYARAF_PATOLOGIS_BABINSKI === "f" &&
                                                tmphasil.SYARAF_PATOLOGIS_BUDZINZKY === "f" && tmphasil.SYARAF_PATOLOGIS_KERNIG === "f") {
                                            Ext.getCmp('cgPatologis').setValue([true, false, false, false]);
                                        }

                                        else if (tmphasil.SYARAF_PATOLOGIS_CHODDOKS === "t" && tmphasil.SYARAF_PATOLOGIS_BABINSKI === "t" &&
                                                tmphasil.SYARAF_PATOLOGIS_BUDZINZKY === "f" && tmphasil.SYARAF_PATOLOGIS_KERNIG === "f") {
                                            Ext.getCmp('cgPatologis').setValue([true, true, false, false]);
                                        }

                                        else if (tmphasil.SYARAF_PATOLOGIS_CHODDOKS === "t" && tmphasil.SYARAF_PATOLOGIS_BABINSKI === "t" &&
                                                tmphasil.SYARAF_PATOLOGIS_BUDZINZKY === "t" && tmphasil.SYARAF_PATOLOGIS_KERNIG === "f") {
                                            Ext.getCmp('cgPatologis').setValue([true, true, true, false]);
                                        }
                                        //2
                                        else if (tmphasil.SYARAF_PATOLOGIS_CHODDOKS === "f" && tmphasil.SYARAF_PATOLOGIS_BABINSKI === "t" &&
                                                tmphasil.SYARAF_PATOLOGIS_BUDZINZKY === "f" && tmphasil.SYARAF_PATOLOGIS_KERNIG === "f") {
                                            Ext.getCmp('cgPatologis').setValue([false, true, false, false]);
                                        }

                                        else if (tmphasil.SYARAF_PATOLOGIS_CHODDOKS === "f" && tmphasil.SYARAF_PATOLOGIS_BABINSKI === "t" &&
                                                tmphasil.SYARAF_PATOLOGIS_BUDZINZKY === "t" && tmphasil.SYARAF_PATOLOGIS_KERNIG === "f") {
                                            Ext.getCmp('cgPatologis').setValue([false, true, true, false]);
                                        }

                                        else if (tmphasil.SYARAF_PATOLOGIS_CHODDOKS === "f" && tmphasil.SYARAF_PATOLOGIS_BABINSKI === "t" &&
                                                tmphasil.SYARAF_PATOLOGIS_BUDZINZKY === "t" && tmphasil.SYARAF_PATOLOGIS_KERNIG === "t") {
                                            Ext.getCmp('cgPatologis').setValue([false, true, true, true]);
                                        }                                         //3
                                        else if (tmphasil.SYARAF_PATOLOGIS_CHODDOKS === "f" && tmphasil.SYARAF_PATOLOGIS_BABINSKI === "f" &&
                                                tmphasil.SYARAF_PATOLOGIS_BUDZINZKY === "t" && tmphasil.SYARAF_PATOLOGIS_KERNIG === "f") {
                                            Ext.getCmp('cgPatologis').setValue([false, false, true, false]);
                                        }

                                        else if (tmphasil.SYARAF_PATOLOGIS_CHODDOKS === "f" && tmphasil.SYARAF_PATOLOGIS_BABINSKI === "f" &&
                                                tmphasil.SYARAF_PATOLOGIS_BUDZINZKY === "t" && tmphasil.SYARAF_PATOLOGIS_KERNIG === "t") {
                                            Ext.getCmp('cgPatologis').setValue([false, false, true, true]);
                                        }
                                        else if (tmphasil.SYARAF_PATOLOGIS_CHODDOKS === "t" && tmphasil.SYARAF_PATOLOGIS_BABINSKI === "f" &&
                                                tmphasil.SYARAF_PATOLOGIS_BUDZINZKY === "t" && tmphasil.SYARAF_PATOLOGIS_KERNIG === "t") {
                                            Ext.getCmp('cgPatologis').setValue([true, false, true, true]);
                                        }                                         //4
                                        else if (tmphasil.SYARAF_PATOLOGIS_CHODDOKS === "f" && tmphasil.SYARAF_PATOLOGIS_BABINSKI === "f" &&
                                                tmphasil.SYARAF_PATOLOGIS_BUDZINZKY === "f" && tmphasil.SYARAF_PATOLOGIS_KERNIG === "t") {
                                            Ext.getCmp('cgPatologis').setValue([false, false, false, true]);
                                        }
                                        else if (tmphasil.SYARAF_PATOLOGIS_CHODDOKS === "t" && tmphasil.SYARAF_PATOLOGIS_BABINSKI === "f" &&
                                                tmphasil.SYARAF_PATOLOGIS_BUDZINZKY === "f" && tmphasil.SYARAF_PATOLOGIS_KERNIG === "t") {
                                            Ext.getCmp('cgPatologis').setValue([true, false, false, true]);
                                        }
                                        else if (tmphasil.SYARAF_PATOLOGIS_CHODDOKS === "t" && tmphasil.SYARAF_PATOLOGIS_BABINSKI === "t" &&
                                                tmphasil.SYARAF_PATOLOGIS_BUDZINZKY === "f" && tmphasil.SYARAF_PATOLOGIS_KERNIG === "t") {
                                            Ext.getCmp('cgPatologis').setValue([true, true, false, true]);
                                        }
                                    }
                                    else {
                                        Ext.getCmp('cgPatologis').setValue([false, false, false, false]);
                                    }
                                    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//tab6 -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

                                    if (tmphasil.INDRA_PUPIL_ISOKOR === "t" || tmphasil.INDRA__PUPIL_ANISOKOR === "t")
                                    {
                                        if (tmphasil.INDRA_PUPIL_ISOKOR === "t" && tmphasil.INDRA__PUPIL_ANISOKOR === "t") {
                                            Ext.getCmp('cgPupil').setValue([true, true]);
                                        } else if (tmphasil.INDRA_PUPIL_ISOKOR === "t" && tmphasil.INDRA__PUPIL_ANISOKOR === "f") {
                                            Ext.getCmp('cgPupil').setValue([true, false]);
                                        } else if (tmphasil.INDRA_PUPIL_ISOKOR === "f" && tmphasil.INDRA__PUPIL_ANISOKOR === "t")
                                        {
                                            Ext.getCmp('cgPupil').setValue([false, true]);
                                        }
                                    }
                                    else {
                                        Ext.getCmp('cgPupil').setValue([false, false]);
                                    }

                                    if (tmphasil.INDRA__KONJUNGTIVA_ANEMIS === "t" || tmphasil.INDRA__KONJUNGTIVA_ICTERUS === "t" || tmphasil.INDRA__KONJUNGTIVA_TIDAK === "t") {
                                        if (tmphasil.INDRA__KONJUNGTIVA_ANEMIS === "t" && tmphasil.INDRA__KONJUNGTIVA_ICTERUS === "t" && tmphasil.INDRA__KONJUNGTIVA_TIDAK === "t") {
                                            Ext.getCmp('cgKonjungtiva').setValue([true, true, true]);
                                        } else if (tmphasil.INDRA__KONJUNGTIVA_ANEMIS === "t" && tmphasil.INDRA__KONJUNGTIVA_ICTERUS === "f" && tmphasil.INDRA__KONJUNGTIVA_TIDAK === "f") {
                                            Ext.getCmp('cgKonjungtiva').setValue([true, false, false]);
                                        }
                                        else if (tmphasil.INDRA__KONJUNGTIVA_ANEMIS === "t" && tmphasil.INDRA__KONJUNGTIVA_ICTERUS === "t" && tmphasil.INDRA__KONJUNGTIVA_TIDAK === "f") {
                                            Ext.getCmp('cgKonjungtiva').setValue([true, true, false]);
                                        }
                                        else if (tmphasil.INDRA__KONJUNGTIVA_ANEMIS === "t" && tmphasil.INDRA__KONJUNGTIVA_ICTERUS === "f" && tmphasil.INDRA__KONJUNGTIVA_TIDAK === "t") {
                                            Ext.getCmp('cgKonjungtiva').setValue([true, false, true]);
                                        } else if (tmphasil.INDRA__KONJUNGTIVA_ANEMIS === "f" && tmphasil.INDRA__KONJUNGTIVA_ICTERUS === "t" && tmphasil.INDRA__KONJUNGTIVA_TIDAK === "f") {
                                            Ext.getCmp('cgKonjungtiva').setValue([false, true, false]);
                                        }
                                        else if (tmphasil.INDRA__KONJUNGTIVA_ANEMIS === "f" && tmphasil.INDRA__KONJUNGTIVA_ICTERUS === "t" && tmphasil.INDRA__KONJUNGTIVA_TIDAK === "t") {
                                            Ext.getCmp('cgKonjungtiva').setValue([false, true, true]);
                                        } else if (tmphasil.INDRA__KONJUNGTIVA_ANEMIS === "f" && tmphasil.INDRA__KONJUNGTIVA_ICTERUS === "f" && tmphasil.INDRA__KONJUNGTIVA_TIDAK === "t") {
                                            Ext.getCmp('cgKonjungtiva').setValue([false, false, true]);
                                        }
                                    }
                                    else {
                                        Ext.getCmp('cgKonjungtiva').setValue([false, false, false]);
                                    }

                                    if (tmphasil.INDRA_GANGGUAN_DENGAR === "t")
                                    {
                                        Ext.getCmp('rgGangguanPendengaran').setValue("rgGangguanPendengaranya", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgGangguanPendengaran').setValue("rgGangguanPendengarantidak", true);
                                    }

                                    if (tmphasil.INDRA_OTORHEA === "t")
                                    {
                                        Ext.getCmp('rgOtorhea').setValue("rgOtorheaya", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgOtorhea').setValue("rgOtorheatidak", true);
                                    }

                                    if (tmphasil.INDRA_BENTUK_HIDUNG === "t")
                                    {
                                        Ext.getCmp('rgBentukHidung').setValue("rgBentukHidungya", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgBentukHidung').setValue("rgBentukHidungtidak", true);
                                    }

                                    if (tmphasil.INDRA_GANGGUAN_CIUM === "t")
                                    {
                                        Ext.getCmp('rgGangguanPenciuman').setValue("rgGangguanPenciumanya", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgGangguanPenciuman').setValue("rgGangguanPenciumantidak", true);
                                    }

                                    if (tmphasil.INDRA_RHINORHEA === "t")
                                    {
                                        Ext.getCmp('rgRhinorhea').setValue("rgRhinorheaya", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgRhinorhea').setValue("rgRhinorheatidak", true);
                                    }

                                    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//tab7 -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                                    if (tmphasil.KEMIH_KEBERSIHAN === "t")
                                    {
                                        Ext.getCmp('rgKebersihan').setValue("rgKebersihanBersih", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgKebersihan').setValue("rgKebersihanKotor", true);
                                    }

                                    if (tmphasil.KEMIH_ALAT_BANTU === "t")
                                    {
                                        Ext.getCmp('rgAlatBantu').setValue("rgAlatBantuYa", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgAlatBantu').setValue("rgAlatBantuTidak", true);
                                    }

                                    if (tmphasil.KEMIH_KANDUNG_MEMBESAR === "t")
                                    {
                                        Ext.getCmp('rgMembesar').setValue("rgMembesarYa", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgMembesar').setValue("rgMembesarTidak", true);
                                    }

                                    if (tmphasil.KEMIH_NYERI_TEKAN === "t")
                                    {
                                        Ext.getCmp('rgNyeri').setValue("rgNyeriYa", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgNyeri').setValue("rgNyeriTidak", true);
                                    }

                                    if (tmphasil.KEMIH_GANGGUAN_ANURIA === "t" || tmphasil.KEMIH_GANGGUAN_ALIGURIA === "t" || tmphasil.KEMIH_GANGGUAN_RETENSI === "t" || tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "t" || tmphasil.KEMIH_GANGGUAN_DISURIA === "t" || tmphasil.KEMIH_GANGGUANHEMATURIA === "t")
                                    {
                                        if (tmphasil.KEMIH_GANGGUAN_ANURIA === "t" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "t" && tmphasil.KEMIH_GANGGUAN_RETENSI === "t" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "t" && tmphasil.KEMIH_GANGGUAN_DISURIA === "t" && tmphasil.KEMIH_GANGGUANHEMATURIA === "t") {
                                            Ext.getCmp('cgGanguankemih').setValue([true, true, true, true, true, true]);
                                        }                                         //1
                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "t" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "f" && tmphasil.KEMIH_GANGGUAN_RETENSI === "f" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "f" && tmphasil.KEMIH_GANGGUAN_DISURIA === "f" && tmphasil.KEMIH_GANGGUANHEMATURIA === "f") {
                                            Ext.getCmp('cgGanguankemih').setValue([true, false, false, false, false, false]);
                                        }
                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "f" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "t" && tmphasil.KEMIH_GANGGUAN_RETENSI === "f" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "f" && tmphasil.KEMIH_GANGGUAN_DISURIA === "f" && tmphasil.KEMIH_GANGGUANHEMATURIA === "f") {
                                            Ext.getCmp('cgGanguankemih').setValue([false, true, false, false, false, false]);
                                        }

                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "f" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "f" && tmphasil.KEMIH_GANGGUAN_RETENSI === "t" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "f" && tmphasil.KEMIH_GANGGUAN_DISURIA === "f" && tmphasil.KEMIH_GANGGUANHEMATURIA === "f") {
                                            Ext.getCmp('cgGanguankemih').setValue([false, false, true, false, false, false]);
                                        }

                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "f" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "f" && tmphasil.KEMIH_GANGGUAN_RETENSI === "f" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "t" && tmphasil.KEMIH_GANGGUAN_DISURIA === "f" && tmphasil.KEMIH_GANGGUANHEMATURIA === "f") {
                                            Ext.getCmp('cgGanguankemih').setValue([false, false, false, true, false, false]);
                                        }

                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "f" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "f" && tmphasil.KEMIH_GANGGUAN_RETENSI === "f" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "f" && tmphasil.KEMIH_GANGGUAN_DISURIA === "t" && tmphasil.KEMIH_GANGGUANHEMATURIA === "f") {
                                            Ext.getCmp('cgGanguankemih').setValue([false, false, false, false, true, false]);
                                        }

                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "f" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "f" && tmphasil.KEMIH_GANGGUAN_RETENSI === "f" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "f" && tmphasil.KEMIH_GANGGUAN_DISURIA === "f" && tmphasil.KEMIH_GANGGUANHEMATURIA === "t") {
                                            Ext.getCmp('cgGanguankemih').setValue([false, false, false, false, false, true]);
                                        }
                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "t" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "t" && tmphasil.KEMIH_GANGGUAN_RETENSI === "f" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "f" && tmphasil.KEMIH_GANGGUAN_DISURIA === "f" && tmphasil.KEMIH_GANGGUANHEMATURIA === "f") {
                                            Ext.getCmp('cgGanguankemih').setValue([true, true, false, false, false, false]);
                                        }

                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "t" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "t" && tmphasil.KEMIH_GANGGUAN_RETENSI === "t" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "f" && tmphasil.KEMIH_GANGGUAN_DISURIA === "f" && tmphasil.KEMIH_GANGGUANHEMATURIA === "f") {
                                            Ext.getCmp('cgGanguankemih').setValue([true, true, true, false, false, false]);
                                        }

                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "t" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "t" && tmphasil.KEMIH_GANGGUAN_RETENSI === "t" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "t" && tmphasil.KEMIH_GANGGUAN_DISURIA === "f" && tmphasil.KEMIH_GANGGUANHEMATURIA === "f") {
                                            Ext.getCmp('cgGanguankemih').setValue([true, true, true, true, false, false]);
                                        }

                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "t" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "t" && tmphasil.KEMIH_GANGGUAN_RETENSI === "t" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "t" && tmphasil.KEMIH_GANGGUAN_DISURIA === "t" && tmphasil.KEMIH_GANGGUANHEMATURIA === "f") {
                                            Ext.getCmp('cgGanguankemih').setValue([true, true, true, true, true, false]);
                                        }
                                        //2
                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "f" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "t" && tmphasil.KEMIH_GANGGUAN_RETENSI === "f" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "f" && tmphasi.lKEMIH_GANGGUAN_DISURIA === "f" && tmphasil.KEMIH_GANGGUANHEMATURIA === "f") {
                                            Ext.getCmp('cgGanguankemih').setValue([false, true, false, false, false, false]);
                                        }

                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "f" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "t" && tmphasil.KEMIH_GANGGUAN_RETENSI === "t" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "f" && tmphasil.KEMIH_GANGGUAN_DISURIA === "f" && tmphasil.KEMIH_GANGGUANHEMATURIA === "f") {
                                            Ext.getCmp('cgGanguankemih').setValue([false, true, true, false, false, false]);
                                        }

                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "f" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "t" && tmphasil.KEMIH_GANGGUAN_RETENSI === "t" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "t" && tmphasil.KEMIH_GANGGUAN_DISURIA === "f" && tmphasil.KEMIH_GANGGUANHEMATURIA === "f") {
                                            Ext.getCmp('cgGanguankemih').setValue([false, true, true, true, false, false]);
                                        }

                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "f" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "t" && tmphasil.KEMIH_GANGGUAN_RETENSI === "t" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "t" && tmphasil.KEMIH_GANGGUAN_DISURIA === "t" && tmphasil.KEMIH_GANGGUANHEMATURIA === "f") {
                                            Ext.getCmp('cgGanguankemih').setValue([false, true, true, true, true, false]);
                                        }

                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "f" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "t" && tmphasil.KEMIH_GANGGUAN_RETENSI === "t" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "t" && tmphasil.KEMIH_GANGGUAN_DISURIA === "t" && tmphasil.KEMIH_GANGGUANHEMATURIA === "t") {
                                            Ext.getCmp('cgGanguankemih').setValue([false, true, true, true, true, true]);
                                        }                                         //3
                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "f" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "f" && tmphasil.KEMIH_GANGGUAN_RETENSI === "t" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "f" && tmphasil.KEMIH_GANGGUAN_DISURIA === "f" && tmphasil.KEMIH_GANGGUANHEMATURIA === "f") {
                                            Ext.getCmp('cgGanguankemih').setValue([false, false, false, true, false, false]);
                                        }

                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "f" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "f" && tmphasil.KEMIH_GANGGUAN_RETENSI === "t" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "t" && tmphasil.KEMIH_GANGGUAN_DISURIA === "f" && tmphasil.KEMIH_GANGGUANHEMATURIA === "f") {
                                            Ext.getCmp('cgGanguankemih').setValue([false, false, true, true, false, false]);
                                        }

                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "f" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "f" && tmphasil.KEMIH_GANGGUAN_RETENSI === "t" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "t" && tmphasil.KEMIH_GANGGUAN_DISURIA === "t" && tmphasil.KEMIH_GANGGUANHEMATURIA === "f") {
                                            Ext.getCmp('cgGanguankemih').setValue([false, false, true, true, true, false]);
                                        }

                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "f" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "f" && tmphasil.KEMIH_GANGGUAN_RETENSI === "t" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "t" && tmphasil.KEMIH_GANGGUAN_DISURIA === "t" && tmphasil.KEMIH_GANGGUANHEMATURIA === "t") {
                                            Ext.getCmp('cgGanguankemih').setValue([false, false, true, true, true, true]);
                                        }
                                        //4
                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "f" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "f" && tmphasil.KEMIH_GANGGUAN_RETENSI === "f" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "t" && tmphasil.KEMIH_GANGGUAN_DISURIA === "f" && tmphasil.KEMIH_GANGGUANHEMATURIA === "f") {
                                            Ext.getCmp('cgGanguankemih').setValue([false, false, false, true, false, false]);
                                        }

                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "f" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "f" && tmphasil.KEMIH_GANGGUAN_RETENSI === "f" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "t" && tmphasil.KEMIH_GANGGUAN_DISURIA === "t" && tmphasil.KEMIH_GANGGUANHEMATURIA === "f") {
                                            Ext.getCmp('cgGanguankemih').setValue([false, false, false, true, true, false]);
                                        }

                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "f" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "f" && tmphasil.KEMIH_GANGGUAN_RETENSI === "f" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "t" && tmphasil.KEMIH_GANGGUAN_DISURIA === "t" && tmphasil.KEMIH_GANGGUANHEMATURIA === "t") {
                                            Ext.getCmp('cgGanguankemih').setValue([false, false, false, true, true, true]);
                                        }                                         //5
                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "f" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "f" && tmphasil.KEMIH_GANGGUAN_RETENSI === "f" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "f" && tmphasil.KEMIH_GANGGUAN_DISURIA === "t" && tmphasil.KEMIH_GANGGUANHEMATURIA === "f") {
                                            Ext.getCmp('cgGanguankemih').setValue([false, false, false, false, true, false]);
                                        }

                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "f" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "f" && tmphasil.KEMIH_GANGGUAN_RETENSI === "f" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "f" && tmphasil.KEMIH_GANGGUAN_DISURIA === "t" && tmphasil.KEMIH_GANGGUANHEMATURIA === "t") {
                                            Ext.getCmp('cgGanguankemih').setValue([false, false, false, false, true, true]);
                                        }
                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "t" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "f" && tmphasil.KEMIH_GANGGUAN_RETENSI === "f" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "f" && tmphasil.KEMIH_GANGGUAN_DISURIA === "t" && tmphasil.KEMIH_GANGGUANHEMATURIA === "t") {
                                            Ext.getCmp('cgGanguankemih').setValue([true, false, false, false, true, true]);
                                        }
                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "t" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "t" && tmphasil.KEMIH_GANGGUAN_RETENSI === "f" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "f" && tmphasil.KEMIH_GANGGUAN_DISURIA === "t" && tmphasil.KEMIH_GANGGUANHEMATURIA === "t") {
                                            Ext.getCmp('cgGanguankemih').setValue([true, true, false, false, true, true]);
                                        }
                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "t" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "t" && tmphasil.KEMIH_GANGGUAN_RETENSI === "t" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "f" && tmphasil.KEMIH_GANGGUAN_DISURIA === "t" && tmphasil.KEMIH_GANGGUANHEMATURIA === "t") {
                                            Ext.getCmp('cgGanguankemih').setValue([true, true, true, false, true, true]);
                                        }                                         //6
                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "f" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "f" && tmphasil.KEMIH_GANGGUAN_RETENSI === "f" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "f" && tmphasil.KEMIH_GANGGUAN_DISURIA === "f" && tmphasil.KEMIH_GANGGUANHEMATURIA === "t") {
                                            Ext.getCmp('cgGanguankemih').setValue([false, false, false, false, false, true]);
                                        }
                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "t" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "f" && tmphasil.KEMIH_GANGGUAN_RETENSI === "f" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "f" && tmphasil.KEMIH_GANGGUAN_DISURIA === "f" && tmphasil.KEMIH_GANGGUANHEMATURIA === "t") {
                                            Ext.getCmp('cgGanguankemih').setValue([true, false, false, false, false, true]);
                                        }
                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "t" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "t" && tmphasil.KEMIH_GANGGUAN_RETENSI === "f" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "f" && tmphasil.KEMIH_GANGGUAN_DISURIA === "f" && tmphasil.KEMIH_GANGGUANHEMATURIA === "t") {
                                            Ext.getCmp('cgGanguankemih').setValue([true, true, false, false, false, true]);
                                        }
                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "t" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "t" && tmphasil.KEMIH_GANGGUAN_RETENSI === "t" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "f" && tmphasil.KEMIH_GANGGUAN_DISURIA === "f" && tmphasil.KEMIH_GANGGUANHEMATURIA === "t") {
                                            Ext.getCmp('cgGanguankemih').setValue([true, true, true, false, false, true]);
                                        }

                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "t" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "t" && tmphasil.KEMIH_GANGGUAN_RETENSI === "t" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "t" && tmphasil.KEMIH_GANGGUAN_DISURIA === "f" && tmphasil.KEMIH_GANGGUANHEMATURIA === "t") {
                                            Ext.getCmp('cgGanguankemih').setValue([true, true, true, true, false, true]);
                                        }
                                    }
                                    else {
                                        Ext.getCmp('cgGanguankemih').setValue([false, false, false, false, false, false]);
                                    }
                                    //------------------------------------------------------------------------------------------------

//tab8
                                    if (tmphasil.CERNA_MAKAN_HABIS === "t")
                                    {
                                        Ext.getCmp('rgPorsiMakan').setValue("rgPorsiMakanHabis", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgPorsiMakan').setValue("rgPorsiMakanTidak", true);
                                    }

                                    if (tmphasil.CERNA_MULUT_BERSIH === "t" || tmphasil.CERNA_MULUT_KOTOR === "t" || tmphasil.CERNA_MULUT_BERBAU === "t")
                                    {
                                        if (tmphasil.CERNA_MULUT_BERSIH === "t" && tmphasil.CERNA_MULUT_KOTOR === "t" && tmphasil.CERNA_MULUT_BERBAU === "t") {
                                            Ext.getCmp('cgKondisiMulut').setValue([true, true, true]);
                                        } else if (tmphasil.CERNA_MULUT_BERSIH === "t" && tmphasil.CERNA_MULUT_KOTOR === "f" && tmphasil.CERNA_MULUT_BERBAU === "f") {
                                            Ext.getCmp('cgKondisiMulut').setValue([true, false, false]);
                                        }
                                        else if (tmphasil.CERNA_MULUT_BERSIH === "t" && tmphasil.CERNA_MULUT_KOTOR === "t" && tmphasil.CERNA_MULUT_BERBAU === "f") {
                                            Ext.getCmp('cgKondisiMulut').setValue([true, true, false]);
                                        }
                                        else if (tmphasil.CERNA_MULUT_BERSIH === "t" && tmphasil.CERNA_MULUT_KOTOR === "f" && tmphasil.CERNA_MULUT_BERBAU === "t") {
                                            Ext.getCmp('cgKondisiMulut').setValue([true, false, true]);
                                        } else if (tmphasil.CERNA_MULUT_BERSIH === "f" && tmphasil.CERNA_MULUT_KOTOR === "t" && tmphasil.CERNA_MULUT_BERBAU === "f")
                                        {
                                            Ext.getCmp('cgKondisiMulut').setValue([false, true, false]);
                                        }
                                        else if (tmphasil.CERNA_MULUT_BERSIH === "f" && tmphasil.CERNA_MULUT_KOTOR === "t" && tmphasil.CERNA_MULUT_BERBAU === "t")
                                        {
                                            Ext.getCmp('cgKondisiMulut').setValue([false, true, true]);
                                        } else if (tmphasil.CERNA_MULUT_BERSIH === "f" && tmphasil.CERNA_MULUT_KOTOR === "f" && tmphasil.CERNA_MULUT_BERBAU === "t")
                                        {
                                            Ext.getCmp('cgKondisiMulut').setValue([false, false, true]);
                                        }
                                    }
                                    else {
                                        Ext.getCmp('cgKondisiMulut').setValue([false, false, false]);
                                    }

                                    if (tmphasil.CERNA_MUKOSA_LEMBAB === "t" || tmphasil.CERNA_MUKOSA_KERING === "t" || tmphasil.CERNA_MUKOSA_STOMATITIS === "t")
                                    {
                                        if (tmphasil.CERNA_MUKOSA_LEMBAB === "t" && tmphasil.CERNA_MUKOSA_KERING === "t" && tmphasil.CERNA_MUKOSA_STOMATITIS === "t") {
                                            Ext.getCmp('cgMukosa').setValue([true, true, true]);
                                        } else if (tmphasil.CERNA_MUKOSA_LEMBAB === "t" && tmphasil.CERNA_MUKOSA_KERING === "f" && tmphasil.CERNA_MUKOSA_STOMATITIS === "f") {
                                            Ext.getCmp('cgMukosa').setValue([true, false, false]);
                                        }
                                        else if (tmphasil.CERNA_MUKOSA_LEMBAB === "t" && tmphasil.CERNA_MUKOSA_KERING === "t" && tmphasil.CERNA_MUKOSA_STOMATITIS === "f") {
                                            Ext.getCmp('cgMukosa').setValue([true, true, false]);
                                        }
                                        else if (tmphasil.CERNA_MUKOSA_LEMBAB === "t" && tmphasil.CERNA_MUKOSA_KERING === "f" && tmphasil.CERNA_MUKOSA_STOMATITIS === "t") {
                                            Ext.getCmp('cgMukosa').setValue([true, false, true]);
                                        } else if (tmphasil.CERNA_MUKOSA_LEMBAB === "f" && tmphasil.CERNA_MUKOSA_KERING === "t" && tmphasil.CERNA_MUKOSA_STOMATITIS === "f")
                                        {
                                            Ext.getCmp('cgMukosa').setValue([false, true, false]);
                                        }
                                        else if (tmphasil.CERNA_MUKOSA_LEMBAB === "f" && tmphasil.CERNA_MUKOSA_KERING === "t" && tmphasil.CERNA_MUKOSA_STOMATITIS === "t")
                                        {
                                            Ext.getCmp('cgMukosa').setValue([false, true, true]);
                                        } else if (tmphasil.CERNA_MUKOSA_LEMBAB === "f" && tmphasil.CERNA_MUKOSA_KERING === "f" && tmphasil.CERNA_MUKOSA_STOMATITIS === "t")
                                        {
                                            Ext.getCmp('cgMukosa').setValue([false, false, true]);
                                        }
                                    }
                                    else {
                                        Ext.getCmp('cgMukosa').setValue([false, false, false]);
                                    }

                                    if (tmphasil.CERNA_TURGOR_KULIT === "t")
                                    {
                                        Ext.getCmp('rgTurgor').setValue("rgTurgorBaik", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgPorsiMakan').setValue("rgTurgorJelek", true);
                                    }

                                    if (tmphasil.CERNA_TENGGOROKAN_SAKIT === "t" || tmphasil.CERNA_TENGGOROKAN_NYERI_TEKAN === "t" || tmphasil.CERNA_TENGGOROKAN_PEMBESARANTO === "t")
                                    {
                                        if (tmphasil.CERNA_TENGGOROKAN_SAKIT === "t" && tmphasil.CERNA_TENGGOROKAN_NYERI_TEKAN === "t" && tmphasil.CERNA_TENGGOROKAN_PEMBESARANTO === "t") {
                                            Ext.getCmp('cgTenggorokan').setValue([true, true, true]);
                                        } else if (tmphasil.CERNA_TENGGOROKAN_SAKIT === "t" && tmphasil.CERNA_TENGGOROKAN_NYERI_TEKAN === "f" && tmphasil.CERNA_TENGGOROKAN_PEMBESARANTO === "f") {
                                            Ext.getCmp('cgTenggorokan').setValue([true, false, false]);
                                        }
                                        else if (tmphasil.CERNA_TENGGOROKAN_SAKIT === "t" && tmphasil.CERNA_TENGGOROKAN_NYERI_TEKAN === "t" && tmphasil.CERNA_TENGGOROKAN_PEMBESARANTO === "f") {
                                            Ext.getCmp('cgTenggorokan').setValue([true, true, false]);
                                        }
                                        else if (tmphasil.CERNA_TENGGOROKAN_SAKIT === "t" && tmphasil.CERNA_TENGGOROKAN_NYERI_TEKAN === "f" && tmphasil.CERNA_TENGGOROKAN_PEMBESARANTO === "t") {
                                            Ext.getCmp('cgTenggorokan').setValue([true, false, true]);
                                        } else if (tmphasil.CERNA_TENGGOROKAN_SAKIT === "f" && tmphasil.CERNA_TENGGOROKAN_NYERI_TEKAN === "t" && tmphasil.CERNA_TENGGOROKAN_PEMBESARANTO === "f")
                                        {
                                            Ext.getCmp('cgTenggorokan').setValue([false, true, false]);
                                        }
                                        else if (tmphasil.CERNA_TENGGOROKAN_SAKIT === "f" && tmphasil.CERNA_TENGGOROKAN_NYERI_TEKAN === "t" && tmphasil.CERNA_TENGGOROKAN_PEMBESARANTO === "t")
                                        {
                                            Ext.getCmp('cgTenggorokan').setValue([false, true, true]);
                                        } else if (tmphasil.CERNA_TENGGOROKAN_SAKIT === "f" && tmphasil.CERNA_TENGGOROKAN_NYERI_TEKAN === "f" && tmphasil.CERNA_TENGGOROKAN_PEMBESARANTO === "t")
                                        {
                                            Ext.getCmp('cgTenggorokan').setValue([false, false, true]);
                                        }
                                    }
                                    else {
                                        Ext.getCmp('cgTenggorokan').setValue([false, false, false]);
                                    }

                                    if (tmphasil.CERNA_PERUT_NORMAL === "t" || tmphasil.CERNA_PERUT_DISTENDED === "t" || tmphasil.CERNA_PERUT_METEORISMUS === "t")
                                    {
                                        if (tmphasil.CERNA_PERUT_NORMAL === "t" && tmphasil.CERNA_PERUT_DISTENDED === "t" && tmphasil.CERNA_PERUT_METEORISMUS === "t") {
                                            Ext.getCmp('cgPerut').setValue([true, true, true]);
                                        } else if (tmphasil.CERNA_PERUT_NORMAL === "t" && tmphasil.CERNA_PERUT_DISTENDED === "f" && tmphasil.CERNA_PERUT_METEORISMUS === "f") {
                                            Ext.getCmp('cgPerut').setValue([true, false, false]);
                                        }
                                        else if (tmphasil.CERNA_PERUT_NORMAL === "t" && tmphasil.CERNA_PERUT_DISTENDED === "t" && tmphasil.CERNA_PERUT_METEORISMUS === "f") {
                                            Ext.getCmp('cgPerut').setValue([true, true, false]);
                                        }
                                        else if (tmphasil.CERNA_PERUT_NORMAL === "t" && tmphasil.CERNA_PERUT_DISTENDED === "f" && tmphasil.CERNA_PERUT_METEORISMUS === "t") {
                                            Ext.getCmp('cgPerut').setValue([true, false, true]);
                                        } else if (tmphasil.CERNA_PERUT_NORMAL === "f" && tmphasil.CERNA_PERUT_DISTENDED === "t" && tmphasil.CERNA_PERUT_METEORISMUS === "f")
                                        {
                                            Ext.getCmp('cgPerut').setValue([false, true, false]);
                                        }
                                        else if (tmphasil.CERNA_PERUT_NORMAL === "f" && tmphasil.CERNA_PERUT_DISTENDED === "t" && tmphasil.CERNA_PERUT_METEORISMUS === "t")
                                        {
                                            Ext.getCmp('cgPerut').setValue([false, true, true]);
                                        } else if (tmphasil.CERNA_PERUT_NORMAL === "f" && tmphasil.CERNA_PERUT_DISTENDED === "f" && tmphasil.CERNA_PERUT_METEORISMUS === "t")
                                        {
                                            Ext.getCmp('cgPerut').setValue([false, false, true]);
                                        }
                                    }
                                    else {
                                        Ext.getCmp('cgPerut').setValue([false, false, false]);
                                    }

                                    if (tmphasil.CERNA_PEMBESARAN_HEPAR === "t")
                                    {
                                        Ext.getCmp('rgPembesaranTonsil').setValue("rgPembesaranTonsilYa", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgPembesaranTonsil').setValue("rgPembesaranTonsilTidak", true);
                                    }

                                    if (tmphasil.CERNA_PERUT_NYERI_TEKAN === "t")
                                    {
                                        Ext.getCmp('cgNyeriTekan').setValue([true]);
                                    } else
                                    {
                                        Ext.getCmp('cgNyeriTekan').setValue([true]);
                                    }

                                    if (tmphasil.CERNA_HEMATEMESIS === "t")
                                    {
                                        Ext.getCmp('rgHematemesis').setValue("rgHematemesisYa", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgHematemesis').setValue("rgHematemesisTidak", true);
                                    }

                                    if (tmphasil.CERNA_EVAKUASI_CAIRAN_ASCITES === "t")
                                    {
                                        Ext.getCmp('rgEvakuasiCairan').setValue("rgEvakuasiCairanYa", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgEvakuasiCairan').setValue("rgEvakuasiCairanTidak", true);
                                    }
                                    //------------------------------------------------------------------------------------------------

//tab9
                                    if (tmphasil.ENDOKRIN_TYROID === "t")
                                    {
                                        Ext.getCmp('rgTyroid').setValue("rgTyroidMembesar", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgTyroid').setValue("rgTyroidTidak", true);
                                    }

                                    if (tmphasil.ENDOKRIN_HIPOGLIKEMIA === "t")
                                    {
                                        Ext.getCmp('rgHyper').setValue("rgHyperYa", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgHyper').setValue("rgHyperTidak", true);
                                    }

                                    if (tmphasil.ENDOKRIN_LUKA_GANGREN === "t")
                                    {
                                        Ext.getCmp('rgGangren').setValue("rgGangrenYa", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgGangren').setValue("rgGangrenTidak", true);
                                    }

                                    if (tmphasil.ENDOKRIN__PUS === "t")
                                    {
                                        Ext.getCmp('rgPus').setValue("rgPusYa", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgPus').setValue("rgPusTidak", true);
                                    }
                                    //------------------------------------------------------------------------------------------------
//end

                                    //textfield

                                    //tab1
                                    Ext.getCmp('txtkeluhanutama').setValue(tmphasil.RIWAYAT_UTAMA);
                                    Ext.getCmp('txtriwayatpenyakitsekarang').setValue(tmphasil.RIWAYAT_PENYAKIT_SEKARANG);
                                    Ext.getCmp('txtriwayatpenyakitTerdahulu').setValue(tmphasil.RIWAYAT_PENYAKIT_DAHULU);
                                    //-----------------------------------------------------------------------------------------------

                                    //tab2
                                    Ext.getCmp('txtJenisRR').setValue(tmphasil.NAFAS_RR);
                                    Ext.getCmp('txtjumlahcairan').setValue(tmphasil.NAFAS_JML_CAIRAN);
                                    Ext.getCmp('txtwarnacairan').setValue(tmphasil.NAFAS_WARNA_CAIRAN);
                                    Ext.getCmp('txtmasalahairway').setValue(tmphasil.NAFAS_MASALAH);
                                    //-----------------------------------------------------------------------------------------------

                                    //tab3
                                    Ext.getCmp('txtpenjelasannyeridada').setValue(tmphasil.JANTUNG_NYERI_DADA_KET);
                                    Ext.getCmp('txtukuranJVP').setValue(tmphasil.JANTUNG_UKURAN_CVP);
                                    Ext.getCmp('txttekanandarah').setValue(tmphasil.JANTUNG_TEKANAN_DARAH);
                                    Ext.getCmp('txtNadi').setValue(tmphasil.JANTUNG_NADI);
                                    Ext.getCmp('txtTemp').setValue(tmphasil.JANTUNG_TEMP);
                                    Ext.getCmp('txtmasalahCirculation').setValue(tmphasil.JANTUNG_MASALAH);
                                    //-----------------------------------------------------------------------------------------------

                                    //tab4
                                    Ext.getCmp('txtkekuatanotot').setValue(tmphasil.OTOT_KEKUATAN);
                                    Ext.getCmp('txtmasalahMusculo').setValue(tmphasil.OTOT_MASALAH);
                                    //-----------------------------------------------------------------------------------------------

                                    //tab5
                                    Ext.getCmp('txttotalGCS').setValue(tmphasil.SYARAF_GCS_TOTAL);
                                    Ext.getCmp('txtmasalahSaraf').setValue(tmphasil.SYARAF_MASALAH);
                                    //-----------------------------------------------------------------------------------------------

                                    //tab6
                                    Ext.getCmp('txtjelaskanpengindraan').setValue(tmphasil.INDRA_BENTUK_HIDUNG_KET);
                                    Ext.getCmp('trmasalahpengindraan').setValue(tmphasil.INDRA_MASALAH);
                                    //------------------------------------------------------------------------------------------------
                                    //tab7
                                    Ext.getCmp('txtjumlahurin').setValue(tmphasil.KEMIH_JML_URINE);
                                    Ext.getCmp('txtwarnaurin').setValue(tmphasil.KEMIH_WARNA_URINE);
                                    Ext.getCmp('txtbauurin').setValue(tmphasil.KEMIH_BAU);
                                    Ext.getCmp('trmasalahperkemihan').setValue(tmphasil.KEMIH_MASALAH);
                                    //------------------------------------------------------------------------------------------------

                                    //tab8
                                    Ext.getCmp('txtporsi').setValue(tmphasil.CERNA_MAKAN_KET);
                                    Ext.getCmp('txtFrekuensimakan').setValue(tmphasil.CERNA_MAKAN_FREKUENSI);
                                    Ext.getCmp('txtMinum').setValue(tmphasil.CERNA_JML_MINUM);
                                    Ext.getCmp('txtjenisMinuman').setValue(tmphasil.CERNA_JENIS_MINUM);
                                    Ext.getCmp('txtParistaltik').setValue(tmphasil.CERNA_PERISTALTIK);
                                    Ext.getCmp('txtlokasisakit').setValue(tmphasil.CERNA_PERUT_LOKASI_NYERI);
                                    Ext.getCmp('txtJumlahcairan').setValue(tmphasil.CERNA_JML_CAIRAN_ASCITES);
                                    Ext.getCmp('txtjumlahBAB').setValue(tmphasil.CERNA_FREK_BAB);
                                    Ext.getCmp('txtKonsintensiBAB').setValue(tmphasil.CERNA_KONSISTENSI);
                                    Ext.getCmp('txtWarnaBAB').setValue(tmphasil.CERNA_WARNA_BAB);
                                    Ext.getCmp('txtBauBAB').setValue(tmphasil.CERNA_BAU_BAB);
                                    Ext.getCmp('txtWarnaCairan').setValue(tmphasil.CERNA_WARNA_CAIRAN_ASCITES);
                                    Ext.getCmp('trmasalahpencernaan').setValue(tmphasil.CERNA_MASALAH);
                                    //------------------------------------------------------------------------------------------------
                                    //tab9
                                    Ext.getCmp('trmasalahendokrin').setValue(tmphasil.ENDOKRIN_MASALAH);
                                    //------------------------------------------------------------------------------------------------
                                    //tab10
                                    Ext.getCmp('txtmandi').setValue(tmphasil.PERSONAL_MANDI);
                                    Ext.getCmp('txtKeramas').setValue(tmphasil.PERSONAL_KERAMAS);
                                    Ext.getCmp('txtSikatGigi').setValue(tmphasil.PERSONAL_SIKATGIGI);
                                    Ext.getCmp('txtGantiPakaian').setValue(tmphasil.PERSONAL_GANTIPAKAIAN);
                                    Ext.getCmp('trMasalahKebersihan').setValue(tmphasil.PERSONAL_MASALAH);
                                    //------------------------------------------------------------------------------------------------
                                    //tab11
                                    Ext.getCmp('trorangyangdekat').setValue(tmphasil.PSIKOSOSIAL_ORANGDEKAT);
                                    Ext.getCmp('trIbadah').setValue(tmphasil.PSIKOSOSIAL_KEGIATAN_IBADAH);
                                    Ext.getCmp('trMasalahSosial').setValue(tmphasil.PSIKOSOSIAL_MASALAH);
                                    //------------------------------------------------------------------------------------------------
                                    //tab12
                                    Ext.getCmp('trterapipenunjang').setValue(tmphasil.TERAPI_PENUNJANG);
                                    //------------------------------------------------------------------------------------------------
                                    //end

                                    //panjang cuy
                                }
                                else {
                                    //tab 1
                                    Ext.getCmp('txtkeluhanutama').setValue('');
                                    Ext.getCmp('txtriwayatpenyakitsekarang').setValue('');
                                    Ext.getCmp('txtriwayatpenyakitTerdahulu').setValue('');
                                    //-----------------------------------------------------------------------------------------------

                                    //tab2
                                    Ext.getCmp('txtJenisRR').setValue(0);
                                    Ext.getCmp('txtjumlahcairan').setValue(0);
                                    Ext.getCmp('txtwarnacairan').setValue('');
                                    Ext.getCmp('txtmasalahairway').setValue('');
                                    Ext.getCmp('rgjelas').setValue("rbjelasya", false);
                                    Ext.getCmp('rgjelas').setValue("rbjelastidak", false);
                                    Ext.getCmp('rgefakuasi').setValue("rbefakuasiya", false);
                                    Ext.getCmp('rgefakuasi').setValue("rbefakuasitidak", false);
                                    Ext.getCmp('cgjelannapas').setValue([false, false]);
                                    Ext.getCmp('cgpolanapas').setValue([false, false]);
                                    Ext.getCmp('cgsuaralapangparu').setValue([false, false, false]);
                                    Ext.getCmp('cgjenis').setValue([false, false, false, false, false, false]);
                                    Ext.getCmp('cgsuaranafastambahan').setValue([false, false, false]);
                                    //--------------------------------------------------------------------

                                    //tab3
                                    Ext.getCmp('txtpenjelasannyeridada').setValue('');
                                    Ext.getCmp('txttekanandarah').setValue('');
                                    Ext.getCmp('txtNadi').setValue('');
                                    Ext.getCmp('txtTemp').setValue(0);
                                    Ext.getCmp('txtukuranJVP').setValue(0);
                                    Ext.getCmp('txtmasalahCirculation').setValue('');
                                    Ext.getCmp('rgS1S2Tunggal').setValue("rbS1S2Tunggalya", false);
                                    Ext.getCmp('rgS1S2Tunggal').setValue("rbS1S2Tunggaltidak", false);
                                    Ext.getCmp('rgNyeridada').setValue("rgNyeridadaya", false);
                                    Ext.getCmp('rgNyeridada').setValue("rgNyeridadatidak", false);
                                    Ext.getCmp('rgCRT').setValue("rgCRTkurang", false);
                                    Ext.getCmp('rgCRT').setValue("rgCRTlebih", false);
                                    Ext.getCmp('rgJVP').setValue("rgJVPya", false);
                                    Ext.getCmp('rgJVP').setValue("rgJVPtidak", false);
                                    Ext.getCmp('cgIramaJantung').setValue([false, false]);
                                    Ext.getCmp('cgBunyiJantungTambahan').setValue([false, false, false]);
                                    Ext.getCmp('cgAkral').setValue([false, false]);
                                    Ext.getCmp('cgWarnaKulit').setValue([false, false, false, false]);
                                    //---------------------------------------------------------------------------

                                    //tab3
                                    Ext.getCmp('txtkekuatanotot').setValue('');
                                    Ext.getCmp('txtmasalahMusculo').setValue('');
                                    Ext.getCmp('cgPergerakanSendi').setValue([false, false]);
                                    Ext.getCmp('rgekstrimitas').setValue("rgekstrimitasya", false);
                                    Ext.getCmp('rgekstrimitas').setValue("rgekstrimitastidak", false);
                                    Ext.getCmp('rgKelainan').setValue("rgKelainanya", false);
                                    Ext.getCmp('rgKelainan').setValue("rgKelainantidak", false);
                                    Ext.getCmp('rgKrepitas').setValue("rgKrepitasya", false);
                                    Ext.getCmp('rgKrepitas').setValue("rgKrepitastidak", false);
//                                    ------------------------------------------------------------------------------

                                    //tab4
                                    Ext.getCmp('txttotalGCS').setValue(0);
                                    Ext.getCmp('txtmasalahSaraf').setValue('');
                                    Ext.getCmp('cgGCS').setValue([false, false, false]);
                                    Ext.getCmp('cgRefleksi').setValue([false, false, false]);
                                    Ext.getCmp('cgPatologis').setValue([false, false, false, false]);
//                                    -----------------------------------------------------------------------------------
                                    Ext.getCmp('cgPupil').setValue([false, false]);
                                    Ext.getCmp('cgKonjungtiva').setValue([false, false, false]);
                                    Ext.getCmp('rgGangguanPendengaran').setValue("rgGangguanPendengaranya", false);
                                    Ext.getCmp('rgGangguanPendengaran').setValue("rgGangguanPendengarantidak", false);
                                    Ext.getCmp('rgOtorhea').setValue("rgOtorheaya", false);
                                    Ext.getCmp('rgOtorhea').setValue("rgOtorheatidak", false);
                                    Ext.getCmp('rgBentukHidung').setValue("rgBentukHidungya", false);
                                    Ext.getCmp('rgBentukHidung').setValue("rgBentukHidungtidak", false);
                                    Ext.getCmp('rgGangguanPenciuman').setValue("rgGangguanPenciumanya", false);
                                    Ext.getCmp('rgGangguanPenciuman').setValue("rgGangguanPenciumantidak", false);
                                    Ext.getCmp('txtjelaskanpengindraan').setValue('');
                                    Ext.getCmp('rgRhinorhea').setValue("rgRhinorheaya", false);
                                    Ext.getCmp('rgRhinorhea').setValue("rgRhinorheatidak", false);
                                    Ext.getCmp('trmasalahpengindraan').setValue('');
                                    Ext.getCmp('rgKebersihan').setValue("rgKebersihanBersih", false);
                                    Ext.getCmp('rgKebersihan').setValue("rgKebersihanKotor", false);
                                    Ext.getCmp('rgAlatBantu').setValue("rgAlatBantuYa", false);
                                    Ext.getCmp('rgAlatBantu').setValue("rgAlatBantuTidak", false);
                                    Ext.getCmp('txtjumlahurin').setValue(0);
                                    Ext.getCmp('txtwarnaurin').setValue('');
                                    Ext.getCmp('txtbauurin').setValue('');
                                    Ext.getCmp('rgMembesar').setValue("rgMembesarYa", false);
                                    Ext.getCmp('rgMembesar').setValue("rgMembesarTidak", false);
                                    Ext.getCmp('rgNyeri').setValue("rgNyeriYa", false);
                                    Ext.getCmp('rgNyeri').setValue("rgNyeriYa", false);
                                    Ext.getCmp('cgGanguankemih').setValue([false, false, false, false, false, false]);
                                    Ext.getCmp('trmasalahperkemihan').setValue('');
                                    Ext.getCmp('rgPorsiMakan').setValue("rgPorsiMakanHabis", false);
                                    Ext.getCmp('rgPorsiMakan').setValue("rgPorsiMakanTidak", false);
                                    Ext.getCmp('txtporsi').setValue(0);
                                    Ext.getCmp('txtFrekuensimakan').setValue('');
                                    Ext.getCmp('txtMinum').setValue(0);
                                    Ext.getCmp('txtjenisMinuman').setValue('');
                                    Ext.getCmp('txtParistaltik').setValue(0);
                                    Ext.getCmp('cgKondisiMulut').setValue([false, false, false]);
                                    Ext.getCmp('cgMukosa').setValue([false, false, false]);
                                    Ext.getCmp('rgTurgor').setValue("rgTurgorBaik", false);
                                    Ext.getCmp('rgTurgor').setValue("rgTurgorJelek", false);
                                    Ext.getCmp('cgTenggorokan').setValue([false, false, false]);
                                    Ext.getCmp('cgPerut').setValue([false, false, false]);
                                    Ext.getCmp('rgPembesaranTonsil').setValue("rgPembesaranTonsilYa", false);
                                    Ext.getCmp('rgPembesaranTonsil').setValue("rgPembesaranTonsilTidak", false);
                                    Ext.getCmp('cgNyeriTekan').setValue([false]);
                                    Ext.getCmp('txtlokasisakit').setValue('');
                                    Ext.getCmp('rgHematemesis').setValue("rgHematemesisYa", false);
                                    Ext.getCmp('rgHematemesis').setValue("rgHematemesisTidak", false);
                                    Ext.getCmp('txtKonsintensiBAB').setValue('');
                                    Ext.getCmp('txtjumlahBAB').setValue(0);
                                    Ext.getCmp('txtWarnaBAB').setValue('');
                                    Ext.getCmp('txtBauBAB').setValue('');
                                    Ext.getCmp('rgEvakuasiCairan').setValue("rgEvakuasiCairanYa", false);
                                    Ext.getCmp('rgEvakuasiCairan').setValue("rgEvakuasiCairanTidak", false);
                                    Ext.getCmp('txtJumlahcairan').setValue(0);
                                    Ext.getCmp('txtWarnaCairan').setValue('');
                                    Ext.getCmp('trmasalahpencernaan').setValue('');
                                    Ext.getCmp('rgTyroid').setValue("rgTyroidMembesar", false);
                                    Ext.getCmp('rgTyroid').setValue("rgTyroidTidak", false);
                                    Ext.getCmp('rgHyper').setValue("rgHyperYa", false);
                                    Ext.getCmp('rgHyper').setValue("rgHyperTidak", false);
                                    Ext.getCmp('rgGangren').setValue("rgGangrenYa", false);
                                    Ext.getCmp('rgGangren').setValue("rgGangrenTidak", false);
                                    Ext.getCmp('rgPus').setValue("rgPusYa", false);
                                    Ext.getCmp('rgPus').setValue("rgPusTidak", false);
                                    Ext.getCmp('txtmasalahMusculo').setValue('');
                                    Ext.getCmp('txtmandi').setValue('');
                                    Ext.getCmp('txtSikatGigi').setValue('');
                                    Ext.getCmp('txtKeramas').setValue('');
                                    Ext.getCmp('txtGantiPakaian').setValue('');
                                    Ext.getCmp('trMasalahKebersihan').setValue('');
                                    Ext.getCmp('trorangyangdekat').setValue('');
                                    Ext.getCmp('trIbadah').setValue('');
                                    Ext.getCmp('trMasalahSosial').setValue('');
                                    Ext.getCmp('trterapipenunjang').setValue('');
                                    //1                                     
                                    RIWAYAT_UTAMA = '';
                                    RIWAYAT_PENYAKIT_SEKARANG = '';
                                    RIWAYAT_PENYAKIT_DAHULU = '';
                                    //2
                                    NAFAS_PATEN = 'false';
                                    NAFAS_OBSTRUKTIF = 'false';
                                    NAFAS_JELAS = 'false';
                                    NAFAS_POLA_SIMETRI = 'false';
                                    NAFAS_POLA_ASIMETRI = 'false';
                                    NAFAS_SUARA_NORMAL = 'false';
                                    NAFAS_SUARA_HIPERSONOR = 'false';
                                    NAFAS_MENURUN = 'false';
                                    NAFAS_JENIS_NORMAL = 'false';
                                    NAFAS_JENIS_TACHYPNOE = 'false';
                                    NAFAS_JENIS_CHEYNESTOKES = 'false';
                                    NAFAS_JENIS_RETRACTIVE = 'false';
                                    NAFAS_JENIS_KUSMAUL = 'false';
                                    NAFAS_JENIS_DISPNOE = 'false';
                                    NAFAS_RR = 0;
                                    NAFAS_SUARA_WHEEZING = 'false';
                                    NAFAS_SUARA_RONCHI = 'false';
                                    NAFAS_SUARA_RALES = 'false';
                                    NAFAS_EVAKUASI_CAIRAN = 'false';
                                    NAFAS_JML_CAIRAN = 0;
                                    NAFAS_WARNA_CAIRAN = '';
                                    NAFAS_MASALAH = '';
                                    //3
                                    JANTUNG_REGULER = 'false';
                                    JANTUNG_IRREGULER = 'false';
                                    JANTUNG_S1S2_TUNGGAL = 'false';
                                    JANTUNG_NYERI_DADA = 'false';
                                    JANTUNG_NYERI_DADA_KET = '';
                                    JANTUNG_BUNYI_MURMUR = 'false';
                                    JANTUNG_BUNYI_GALLOP = 'false';
                                    JANTUNG_BUNYI_BISING = 'false';
                                    JANTUNG_CRT = 'false';
                                    JANTUNG_AKRAL_HANGAT = 'false';
                                    JANTUNG_AKRAL_DINGIN = 'false';
                                    JANTUNG_PENINGKATAN_JVP = 'false';
                                    JANTUNG_UKURAN_CVP = 0;
                                    JANTUNG_WARNA_JAUNDICE = 'false';
                                    JANTUNG_WARNA_SIANOTIK = 'false';
                                    JANTUNG_WARNA_KEMERAHAN = 'false';
                                    JANTUNG_WARNA_PUCAT = 'false';
                                    JANTUNG_TEKANAN_DARAH = '';
                                    JANTUNG_NADI = '';
                                    JANTUNG_TEMP = 0;
                                    JANTUNG_MASALAH = '';
                                    //4
                                    OTOT_SENDI_BEBAS = 'false';
                                    OTOT_SENDI_TERBATAS = 'false';
                                    OTOT_KEKUATAN = '';
                                    OTOT_ODEMA_EKSTRIMITAS = 'false';
                                    OTOT_KELAINAN_BENTUK = 'false';
                                    OTOT_KREPITASI = 'false';
                                    OTOT_MASALAH = '';
                                    //5
                                    SYARAF_GCS_EYE = 'false';
                                    SYARAF_GCS_VERBAL = 'false';
                                    SYARAF_GCS_MOTORIK = 'false';
                                    SYARAF_GCS_TOTAL = 0;
                                    SYARAF_FISIOLOGIS_BRACHIALIS = 'false';
                                    SYARAF_FISIOLOGIS_PATELLA = 'false';
                                    SYARAF_FISIOLOGIS_ACHILLES = 'false';
                                    SYARAF_PATOLOGIS_CHODDOKS = 'false';
                                    SYARAF_PATOLOGIS_BABINSKI = 'false';
                                    SYARAF_PATOLOGIS_BUDZINZKY = 'false';
                                    SYARAF_PATOLOGIS_KERNIG = 'false';
                                    SYARAF_MASALAH = '';
                                    INDRA_PUPIL_ISOKOR = 'false';
                                    INDRA__PUPIL_ANISOKOR = 'false';
                                    INDRA__KONJUNGTIVA_ANEMIS = 'false';
                                    INDRA__KONJUNGTIVA_ICTERUS = 'false';
                                    INDRA__KONJUNGTIVA_TIDAK = 'false';
                                    INDRA_GANGGUAN_DENGAR = 'false';
                                    INDRA_OTORHEA = 'false';
                                    INDRA_BENTUK_HIDUNG = 'false';
                                    INDRA_BENTUK_HIDUNG_KET = '';
                                    INDRA_GANGGUAN_CIUM = 'false';
                                    INDRA_RHINORHEA = 'false';
                                    INDRA_MASALAH = '';
                                    KEMIH_KEBERSIHAN = 'false';
                                    KEMIH_ALAT_BANTU = 'false';
                                    KEMIH_JML_URINE = 0;
                                    KEMIH_WARNA_URINE = '';
                                    KEMIH_BAU = '';
                                    KEMIH_KANDUNG_MEMBESAR = 'false';
                                    KEMIH_NYERI_TEKAN = 'false';
                                    KEMIH_GANGGUAN_ANURIA = 'false';
                                    KEMIH_GANGGUAN_ALIGURIA = 'false';
                                    KEMIH_GANGGUAN_RETENSI = 'false';
                                    KEMIH_GANGGUAN_INKONTINENSIA = 'false';
                                    KEMIH_GANGGUAN_DISURIA = 'false';
                                    KEMIH_GANGGUANHEMATURIA = 'false';
                                    KEMIH_MASALAH = '';
                                    CERNA_MAKAN_HABIS = 'false';
                                    CERNA_MAKAN_KET = '';
                                    CERNA_MAKAN_FREKUENSI = 0;
                                    CERNA_JML_MINUM = 0;
                                    CERNA_JENIS_MINUM = '';
                                    CERNA_MULUT_BERSIH = 'false';
                                    CERNA_MULUT_KOTOR = 'false';
                                    CERNA_MULUT_BERBAU = 'false';
                                    CERNA_MUKOSA_LEMBAB = 'false';
                                    CERNA_MUKOSA_KERING = 'false';
                                    CERNA_MUKOSA_STOMATITIS = 'false';
                                    CERNA_TENGGOROKAN_SAKIT = 'false';
                                    CERNA_TENGGOROKAN_NYERI_TEKAN = 'false';
                                    CERNA_TENGGOROKAN_PEMBESARANTO = 'false';
                                    CERNA_PERUT_NORMAL = 'false';
                                    CERNA_PERUT_DISTENDED = 'false';
                                    CERNA_PERUT_METEORISMUS = 'false';
                                    CERNA_PERUT_NYERI_TEKAN = 'false';
                                    CERNA_PERUT_LOKASI_NYERI = '';
                                    CERNA_PERISTALTIK = 0;
                                    CERNA_TURGOR_KULIT = 'false';
                                    CERNA_PEMBESARAN_HEPAR = 'false';
                                    CERNA_HEMATEMESIS = 'false';
                                    CERNA_EVAKUASI_CAIRAN_ASCITES = 'false';
                                    CERNA_JML_CAIRAN_ASCITES = 0;
                                    CERNA_WARNA_CAIRAN_ASCITES = 'false';
                                    CERNA_FREK_BAB = 0;
                                    CERNA_KONSISTENSI = '';
                                    CERNA_BAU_BAB = '';
                                    CERNA_WARNA_BAB = '';
                                    CERNA_MASALAH = '';
                                    ENDOKRIN_TYROID = 'false';
                                    ENDOKRIN_HIPOGLIKEMIA = 'false';
                                    ENDOKRIN_LUKA_GANGREN = 'false';
                                    ENDOKRIN__PUS = 'false';
                                    ENDOKRIN_MASALAH = '';
                                    PERSONAL_MANDI = '';
                                    PERSONAL_SIKATGIGI = '';
                                    PERSONAL_KERAMAS = '';
                                    PERSONAL_GANTIPAKAIAN = '';
                                    PERSONAL_MASALAH = '';
                                    PSIKOSOSIAL_ORANGDEKAT = '';
                                    PSIKOSOSIAL_KEGIATAN_IBADAH = '';
                                    PSIKOSOSIAL_MASALAH = '';
                                    TERAPI_PENUNJANG = '';
                                }
                            }
                        }
                    }
            );
}

function CrudDataAwal(controller){
	console.log(DATA_ASKEP_IGD_AWAL.tindak_lanjut);
    if (DATA_ASKEP_IGD_AWAL.tindak_lanjut.tindak_lanjut != 1) {
        DATA_ASKEP_IGD_AWAL.tindak_lanjut.konsultasi = "";
        DATA_ASKEP_IGD_AWAL.tindak_lanjut.konsultasi_1 = "";
        DATA_ASKEP_IGD_AWAL.tindak_lanjut.konsultasi_2 = "";
        DATA_ASKEP_IGD_AWAL.tindak_lanjut.konsultasi_3 = "";
        DATA_ASKEP_IGD_AWAL.tindak_lanjut.konsultasi_4 = "";
        DATA_ASKEP_IGD_AWAL.tindak_lanjut.telepon    = false;
        DATA_ASKEP_IGD_AWAL.tindak_lanjut.on_site    = false;
    }
    if (DATA_ASKEP_IGD_AWAL.tindak_lanjut.tindak_lanjut != 2) {
        DATA_ASKEP_IGD_AWAL.tindak_lanjut.atas_persetujuan = false;
        DATA_ASKEP_IGD_AWAL.tindak_lanjut.persetujuan_diri = false;
        DATA_ASKEP_IGD_AWAL.tindak_lanjut.dead 			   = false; //2019-04-18
        DATA_ASKEP_IGD_AWAL.tindak_lanjut.kontrol          = false;
        DATA_ASKEP_IGD_AWAL.tindak_lanjut.tanggal          = now.format('Y-m-d');
        DATA_ASKEP_IGD_AWAL.tindak_lanjut.terapi_pulang    = "";
        DATA_ASKEP_IGD_AWAL.tindak_lanjut.edukasi_pasien   = false;
        DATA_ASKEP_IGD_AWAL.tindak_lanjut.edukasi_keluarga = false;
        DATA_ASKEP_IGD_AWAL.tindak_lanjut.edukasi_tidak    = false;
        DATA_ASKEP_IGD_AWAL.tindak_lanjut.karena           = "";
    }
    if (DATA_ASKEP_IGD_AWAL.tindak_lanjut.tindak_lanjut != 3) {
        DATA_ASKEP_IGD_AWAL.tindak_lanjut.dirujuk_ke       = "";
        DATA_ASKEP_IGD_AWAL.tindak_lanjut.alasan_dirujuk   = "";
    }

    Ext.Ajax.request({
		url: baseURL + "index.php/main/CreateDataObj",
		params: parameterAwal(controller),
		failure: function (o)
		{
			ShowPesanWarningPengkajian('Proses Saving Tidak berhasil silahkan hubungi admin', 'Gagal');
		},
		success: function (o)
		{
			var cst = Ext.decode(o.responseText);
			if (cst.success === true){
				ShowPesanInfoPengkajian('Proses Saving Berhasil', 'Save');
				tmpediting = "false";
                Ext.getCmp('btnSimpan_viDaftarAwal').disable();
                gridPemeriksaanPenunjangAwal.status = false;
                gridTataLaksanaAwal.status = false;
			}else{
				ShowPesanWarningPengkajian('Proses Saving Tidak berhasil silahkan hubungi admin', 'Gagal');
			};
		}
	});
};

function parameterAwal(controller){
    var tata_laksanaAwal           = []; 
	var pemeriksaan_penunjangAwal           = []; 
	var variable_pemeriksaan_penunjangAwal  = gridPemeriksaanPenunjangAwal.grid_store_hasil.store;
    for (var i = 0; i < variable_pemeriksaan_penunjangAwal.data.length; i++) {
        var data = {};
        data.catatan         = variable_pemeriksaan_penunjangAwal.data.items[i].data.CATATAN;
        data.pemeriksaan     = variable_pemeriksaan_penunjangAwal.data.items[i].data.PEMERIKSAAN;
        data.waktu_diminta   = variable_pemeriksaan_penunjangAwal.data.items[i].data.WAKTU_DIMINTA;
        data.waktu_diperiksa = variable_pemeriksaan_penunjangAwal.data.items[i].data.WAKTU_DIPERIKSA;
        pemeriksaan_penunjangAwal.push(data);
    }
    var variable_tata_laksanaAwal  = gridTataLaksanaAwal.grid_store_hasil.store;
    for (var i = 0; i < variable_tata_laksanaAwal.data.length; i++) {
        var data = {};
        data.waktu           = variable_tata_laksanaAwal.data.items[i].data.WAKTU;
        data.tindakan        = variable_tata_laksanaAwal.data.items[i].data.TINDAKAN;
        data.dosis           = variable_tata_laksanaAwal.data.items[i].data.DOSIS;
        data.cara_pemberian  = variable_tata_laksanaAwal.data.items[i].data.CARA_PEMBERIAN;
        data.waktu_pemberian = variable_tata_laksanaAwal.data.items[i].data.WAKTU_PEMBERIAN;
        data.ttd_dokter      = variable_tata_laksanaAwal.data.items[i].data.TTD_DOKTER;
        data.ttd_perawat     = variable_tata_laksanaAwal.data.items[i].data.TTD_PERAWAT;
        tata_laksanaAwal.push(data);
    }
    var tanda_vitalAwal           = []; 
    var variable_tanda_vitalAwal  = gridTandaVitalAwal.grid_store_hasil.store;
    for (var i = 0; i < variable_tanda_vitalAwal.data.length; i++) {
        var data = {};
        data.waktu         = variable_tanda_vitalAwal.data.items[i].data.WAKTU;
        data.td            = variable_tanda_vitalAwal.data.items[i].data.TD;
        data.n             = variable_tanda_vitalAwal.data.items[i].data.N;
        data.p             = variable_tanda_vitalAwal.data.items[i].data.P;
        data.sao2          = variable_tanda_vitalAwal.data.items[i].data.SAO2;
        data.irama_jantung = variable_tanda_vitalAwal.data.items[i].data.IRAMA_JANTUNG;
        tanda_vitalAwal.push(data);
    }

    var tindakan_invasifAwal           = []; 
    var variable_tindakan_invasifAwal  = gridTindakanInvasifAwal.grid_store_hasil.store;
    for (var i = 0; i < variable_tindakan_invasifAwal.data.length; i++) {
        var data        = {};
        data.urut       = variable_tindakan_invasifAwal.data.items[i].data.URUT;
        data.checked    = variable_tindakan_invasifAwal.data.items[i].data.CHECKED;
        data.checklist  = variable_tindakan_invasifAwal.data.items[i].data.CHECKLIST;
        data.keterangan = variable_tindakan_invasifAwal.data.items[i].data.KETERANGAN;
        data.pelaksana  = variable_tindakan_invasifAwal.data.items[i].data.PELAKSANA;

        tindakan_invasifAwal.push(data);
    }

   
    var params = {
		Table 					 : controller,
		KD_PASIEN 				 : rowSelected_viDaftarAwal.data.KD_PASIEN, 
		KD_UNIT 				 : rowSelected_viDaftarAwal.data.KD_UNIT,
		URUT_MASUK 				 : rowSelected_viDaftarAwal.data.URUT_MASUK,
		TGL_MASUK 				 : rowSelected_viDaftarAwal.data.TANGGAL_TRANSAKSI,
		airway 					 : JSON.stringify(DATA_ASKEP_IGD_AWAL.airway),
		breathing				 : JSON.stringify(DATA_ASKEP_IGD_AWAL.breathing),
		circulation				 : JSON.stringify(DATA_ASKEP_IGD_AWAL.circulation),
		ekg						 : JSON.stringify(DATA_ASKEP_IGD_AWAL.ekg),
		crt						 : JSON.stringify(DATA_ASKEP_IGD_AWAL.crt),
		akral					 : JSON.stringify(DATA_ASKEP_IGD_AWAL.akral),
		turgor_kulit 			 : JSON.stringify(DATA_ASKEP_IGD_AWAL.turgor_kulit),
		warna_kulit 			 : JSON.stringify(DATA_ASKEP_IGD_AWAL.warna_kulit),
		riwayat_cairan 			 : JSON.stringify(DATA_ASKEP_IGD_AWAL.riwayat_cairan),
		disability 				 : JSON.stringify(DATA_ASKEP_IGD_AWAL.disability),
		exposure 				 : JSON.stringify(DATA_ASKEP_IGD_AWAL.exposure),
		riwayat_kesehatan 		 : JSON.stringify(DATA_ASKEP_IGD_AWAL.riwayat_kesehatan),
		kehamilan 				 : JSON.stringify(DATA_ASKEP_IGD_AWAL.kehamilan),
		skala_nyeri 			 : JSON.stringify(DATA_ASKEP_IGD_AWAL.skala_nyeri),
		resiko_jatuh 			 : JSON.stringify(DATA_ASKEP_IGD_AWAL.resiko_jatuh),
        psikologis               : JSON.stringify(DATA_ASKEP_IGD_AWAL.psikologis),
        status_sosial            : JSON.stringify(DATA_ASKEP_IGD_AWAL.status_sosial),
        sensorik                 : JSON.stringify(DATA_ASKEP_IGD_AWAL.sensorik),
        motorik                  : JSON.stringify(DATA_ASKEP_IGD_AWAL.motorik),
        kognitif                 : JSON.stringify(DATA_ASKEP_IGD_AWAL.kognitif),
        tanda_vital              : JSON.stringify(DATA_ASKEP_IGD_AWAL.tanda_vital),
        lanjutan                 : JSON.stringify(DATA_ASKEP_IGD_AWAL.lanjutan),
        pemeriksaan              : JSON.stringify(DATA_ASKEP_IGD_AWAL.pemeriksaan),
        kekuatan_otot            : JSON.stringify(DATA_ASKEP_IGD_AWAL.kekuatan_otot),
        tindak_lanjut            : JSON.stringify(DATA_ASKEP_IGD_AWAL.tindak_lanjut),
		trease 	                 : JSON.stringify(DATA_ASKEP_IGD_AWAL.trease),
        serah_terima             : JSON.stringify(DATA_ASKEP_IGD_AWAL.serah_terima),
		pemeriksaan_penunjang    : JSON.stringify(pemeriksaan_penunjangAwal),
		edit_pemeriksaan_penunjang : gridPemeriksaanPenunjangAwal.status,
		tata_laksana            : JSON.stringify(tata_laksanaAwal),
        edit_tata_laksana       : gridTataLaksanaAwal.status,
        _tanda_vital            : JSON.stringify(tanda_vitalAwal),
        tindakan_invasif        : JSON.stringify(tindakan_invasifAwal),
		edit_tanda_vital        : gridTandaVitalAwal.status,
        gizi                    : JSON.stringify(gridGizi.checked_radio),
        _gizi                   : JSON.stringify(DATA_ASKEP_IGD_AWAL.gizi),
       
	};
	return params;
}

function SavingData(){
    Ext.Ajax.request({
		url: baseURL + "index.php/main/CreateDataObj",
		params: paramsaving(),
		failure: function (o)
		{
			ShowPesanWarningPengkajian('Proses Saving Tidak berhasil silahkan hubungi admin', 'Gagal');
			caridatapasien();
		},
		success: function (o)
		{
			var cst = Ext.decode(o.responseText);
			if (cst.success === true)
			{
				ShowPesanInfoPengkajian('Proses Saving Berhasil', 'Save');
				caridatapasien();
				Ext.getCmp('btnSimpan_viDaftarAwal').disable();
				tmpediting = 'false';
			}
			else
			{
				ShowPesanWarningPengkajian('Proses Saving Tidak berhasil silahkan hubungi admin', 'Gagal');
				caridatapasien();
				tmpediting = 'false';
			}
			;
		}
	}
);
};

function paramsaving()
{
    var params =
            {
                Table: 'ViewTrAskepIGD',
                query: "kd_pasien_kunj = '" + tmpkdpasien + "' and kd_unit_kunj = '" + tmpkdunit + "' and urut_masuk_kunj = " + tmpurutmasuk + " and tgl_masuk_kunj = '" + tmptglmasuk + "'",
                KD_PASIEN_KUNJ: tmpkdpasien, KD_UNIT_KUNJ: tmpkdunit,
                URUT_MASUK_KUNJ: tmpurutmasuk,
                TGL_MASUK_KUNJ: tmptglmasuk,
                RIWAYAT_UTAMA: Ext.get('txtkeluhanutama').getValue(),
                RIWAYAT_PENYAKIT_SEKARANG: Ext.get('txtriwayatpenyakitsekarang').getValue(),
                RIWAYAT_PENYAKIT_DAHULU: Ext.get('txtriwayatpenyakitTerdahulu').getValue(),
                NAFAS_PATEN: NAFAS_PATEN,
                NAFAS_OBSTRUKTIF: NAFAS_OBSTRUKTIF,
                NAFAS_JELAS: NAFAS_JELAS,
                NAFAS_POLA_SIMETRI: NAFAS_POLA_SIMETRI,
                NAFAS_POLA_ASIMETRI: NAFAS_POLA_ASIMETRI,
                NAFAS_SUARA_NORMAL: NAFAS_SUARA_NORMAL,
                NAFAS_SUARA_HIPERSONOR: NAFAS_SUARA_HIPERSONOR,
                NAFAS_MENURUN: NAFAS_MENURUN,
                NAFAS_JENIS_NORMAL: NAFAS_JENIS_NORMAL,
                NAFAS_JENIS_TACHYPNOE: NAFAS_JENIS_TACHYPNOE,
                NAFAS_JENIS_CHEYNESTOKES: NAFAS_JENIS_CHEYNESTOKES,
                NAFAS_JENIS_RETRACTIVE: NAFAS_JENIS_RETRACTIVE,
                NAFAS_JENIS_KUSMAUL: NAFAS_JENIS_KUSMAUL,
                NAFAS_JENIS_DISPNOE: NAFAS_JENIS_DISPNOE,
                NAFAS_RR: NAFAS_RR,
                NAFAS_SUARA_WHEEZING: NAFAS_SUARA_WHEEZING,
                NAFAS_SUARA_RONCHI: NAFAS_SUARA_RONCHI,
                NAFAS_SUARA_RALES: NAFAS_SUARA_RALES,
                NAFAS_EVAKUASI_CAIRAN: NAFAS_EVAKUASI_CAIRAN,
                NAFAS_JML_CAIRAN: NAFAS_JML_CAIRAN,
                NAFAS_WARNA_CAIRAN: NAFAS_WARNA_CAIRAN,
                NAFAS_MASALAH: NAFAS_MASALAH,
                JANTUNG_REGULER: JANTUNG_REGULER,
                JANTUNG_IRREGULER: JANTUNG_IRREGULER,
                JANTUNG_S1S2_TUNGGAL: JANTUNG_S1S2_TUNGGAL,
                JANTUNG_NYERI_DADA: JANTUNG_NYERI_DADA,
                JANTUNG_NYERI_DADA_KET: JANTUNG_NYERI_DADA_KET,
                JANTUNG_BUNYI_MURMUR: JANTUNG_BUNYI_MURMUR,
                JANTUNG_BUNYI_GALLOP: JANTUNG_BUNYI_GALLOP,
                JANTUNG_BUNYI_BISING: JANTUNG_BUNYI_BISING,
                JANTUNG_CRT: JANTUNG_CRT,
                JANTUNG_AKRAL_HANGAT: JANTUNG_AKRAL_HANGAT,
                JANTUNG_AKRAL_DINGIN: JANTUNG_AKRAL_DINGIN,
                JANTUNG_PENINGKATAN_JVP: JANTUNG_PENINGKATAN_JVP,
                JANTUNG_UKURAN_CVP: JANTUNG_UKURAN_CVP,
                JANTUNG_WARNA_JAUNDICE: JANTUNG_WARNA_JAUNDICE,
                JANTUNG_WARNA_SIANOTIK: JANTUNG_WARNA_SIANOTIK,
                JANTUNG_WARNA_KEMERAHAN: JANTUNG_WARNA_KEMERAHAN,
                JANTUNG_WARNA_PUCAT: JANTUNG_WARNA_PUCAT,
                JANTUNG_TEKANAN_DARAH: JANTUNG_TEKANAN_DARAH,
                JANTUNG_NADI: JANTUNG_NADI,
                JANTUNG_TEMP: JANTUNG_TEMP,
                JANTUNG_MASALAH: JANTUNG_MASALAH,
                OTOT_SENDI_BEBAS: OTOT_SENDI_BEBAS,
                OTOT_SENDI_TERBATAS: OTOT_SENDI_TERBATAS,
                OTOT_KEKUATAN: OTOT_KEKUATAN,
                OTOT_ODEMA_EKSTRIMITAS: OTOT_ODEMA_EKSTRIMITAS,
                OTOT_KELAINAN_BENTUK: OTOT_KELAINAN_BENTUK,
                OTOT_KREPITASI: OTOT_KREPITASI,
                OTOT_MASALAH: OTOT_MASALAH,
                SYARAF_GCS_EYE: SYARAF_GCS_EYE,
                SYARAF_GCS_VERBAL: SYARAF_GCS_VERBAL,
                SYARAF_GCS_MOTORIK: SYARAF_GCS_MOTORIK,
                SYARAF_GCS_TOTAL: SYARAF_GCS_TOTAL,
                SYARAF_FISIOLOGIS_BRACHIALIS: SYARAF_FISIOLOGIS_BRACHIALIS,
                SYARAF_FISIOLOGIS_PATELLA: SYARAF_FISIOLOGIS_PATELLA,
                SYARAF_FISIOLOGIS_ACHILLES: SYARAF_FISIOLOGIS_ACHILLES,
                SYARAF_PATOLOGIS_CHODDOKS: SYARAF_PATOLOGIS_CHODDOKS,
                SYARAF_PATOLOGIS_BABINSKI: SYARAF_PATOLOGIS_BABINSKI,
                SYARAF_PATOLOGIS_BUDZINZKY: SYARAF_PATOLOGIS_BUDZINZKY,
                SYARAF_PATOLOGIS_KERNIG: SYARAF_PATOLOGIS_KERNIG,
                SYARAF_MASALAH: SYARAF_MASALAH,
                INDRA_PUPIL_ISOKOR: INDRA_PUPIL_ISOKOR,
                INDRA__PUPIL_ANISOKOR: INDRA__PUPIL_ANISOKOR,
                INDRA__KONJUNGTIVA_ANEMIS: INDRA__KONJUNGTIVA_ANEMIS,
                INDRA__KONJUNGTIVA_ICTERUS: INDRA__KONJUNGTIVA_ICTERUS,
                INDRA__KONJUNGTIVA_TIDAK: INDRA__KONJUNGTIVA_TIDAK,
                INDRA_GANGGUAN_DENGAR: INDRA_GANGGUAN_DENGAR,
                INDRA_OTORHEA: INDRA_OTORHEA,
                INDRA_BENTUK_HIDUNG: INDRA_BENTUK_HIDUNG,
                INDRA_BENTUK_HIDUNG_KET: INDRA_BENTUK_HIDUNG_KET,
                INDRA_GANGGUAN_CIUM: INDRA_GANGGUAN_CIUM,
                INDRA_RHINORHEA: INDRA_RHINORHEA,
                INDRA_MASALAH: INDRA_MASALAH,
                KEMIH_KEBERSIHAN: KEMIH_KEBERSIHAN,
                KEMIH_ALAT_BANTU: KEMIH_ALAT_BANTU,
                KEMIH_JML_URINE: KEMIH_JML_URINE,
                KEMIH_WARNA_URINE: KEMIH_WARNA_URINE,
                KEMIH_BAU: KEMIH_BAU,
                KEMIH_KANDUNG_MEMBESAR: KEMIH_KANDUNG_MEMBESAR,
                KEMIH_NYERI_TEKAN: KEMIH_NYERI_TEKAN,
                KEMIH_GANGGUAN_ANURIA: KEMIH_GANGGUAN_ANURIA,
                KEMIH_GANGGUAN_ALIGURIA: KEMIH_GANGGUAN_ALIGURIA,
                KEMIH_GANGGUAN_RETENSI: KEMIH_GANGGUAN_RETENSI,
                KEMIH_GANGGUAN_INKONTINENSIA: KEMIH_GANGGUAN_INKONTINENSIA,
                KEMIH_GANGGUAN_DISURIA: KEMIH_GANGGUAN_DISURIA,
                KEMIH_GANGGUANHEMATURIA: KEMIH_GANGGUANHEMATURIA,
                KEMIH_MASALAH: KEMIH_MASALAH,
                CERNA_MAKAN_HABIS: CERNA_MAKAN_HABIS,
                CERNA_MAKAN_KET: CERNA_MAKAN_KET,
                CERNA_MAKAN_FREKUENSI: CERNA_MAKAN_FREKUENSI,
                CERNA_JML_MINUM: CERNA_JML_MINUM,
                CERNA_JENIS_MINUM: CERNA_JENIS_MINUM,
                CERNA_MULUT_BERSIH: CERNA_MULUT_BERSIH,
                CERNA_MULUT_KOTOR: CERNA_MULUT_KOTOR,
                CERNA_MULUT_BERBAU: CERNA_MULUT_BERBAU,
                CERNA_MUKOSA_LEMBAB: CERNA_MUKOSA_LEMBAB,
                CERNA_MUKOSA_KERING: CERNA_MUKOSA_KERING,
                CERNA_MUKOSA_STOMATITIS: CERNA_MUKOSA_STOMATITIS,
                CERNA_TENGGOROKAN_SAKIT: CERNA_TENGGOROKAN_SAKIT,
                CERNA_TENGGOROKAN_NYERI_TEKAN: CERNA_TENGGOROKAN_NYERI_TEKAN,
                CERNA_TENGGOROKAN_PEMBESARANTO: CERNA_TENGGOROKAN_PEMBESARANTO,
                CERNA_PERUT_NORMAL: CERNA_PERUT_NORMAL,
                CERNA_PERUT_DISTENDED: CERNA_PERUT_DISTENDED,
                CERNA_PERUT_METEORISMUS: CERNA_PERUT_METEORISMUS,
                CERNA_PERUT_NYERI_TEKAN: CERNA_PERUT_NYERI_TEKAN,
                CERNA_PERUT_LOKASI_NYERI: CERNA_PERUT_LOKASI_NYERI,
                CERNA_PERISTALTIK: CERNA_PERISTALTIK,
                CERNA_TURGOR_KULIT: CERNA_TURGOR_KULIT,
                CERNA_PEMBESARAN_HEPAR: CERNA_PEMBESARAN_HEPAR,
                CERNA_HEMATEMESIS: CERNA_HEMATEMESIS,
                CERNA_EVAKUASI_CAIRAN_ASCITES: CERNA_EVAKUASI_CAIRAN_ASCITES,
                CERNA_JML_CAIRAN_ASCITES: CERNA_JML_CAIRAN_ASCITES,
                CERNA_FREK_BAB: CERNA_FREK_BAB,
                CERNA_KONSISTENSI: CERNA_KONSISTENSI,
                CERNA_BAU_BAB: CERNA_BAU_BAB,
                CERNA_WARNA_BAB: CERNA_WARNA_BAB,
                CERNA_WARNA_CAIRAN_ASCITES: CERNA_WARNA_CAIRAN_ASCITES,
                CERNA_MASALAH: CERNA_MASALAH,
                ENDOKRIN_TYROID: ENDOKRIN_TYROID,
                ENDOKRIN_HIPOGLIKEMIA: ENDOKRIN_HIPOGLIKEMIA,
                ENDOKRIN_LUKA_GANGREN: ENDOKRIN_LUKA_GANGREN,
                ENDOKRIN__PUS: ENDOKRIN__PUS,
                ENDOKRIN_MASALAH: ENDOKRIN_MASALAH,
                PERSONAL_MANDI: PERSONAL_MANDI,
                PERSONAL_SIKATGIGI: PERSONAL_SIKATGIGI,
                PERSONAL_KERAMAS: PERSONAL_KERAMAS,
                PERSONAL_GANTIPAKAIAN: PERSONAL_GANTIPAKAIAN,
                PERSONAL_MASALAH: PERSONAL_MASALAH,
                PSIKOSOSIAL_ORANGDEKAT: PSIKOSOSIAL_ORANGDEKAT,
                PSIKOSOSIAL_KEGIATAN_IBADAH: PSIKOSOSIAL_KEGIATAN_IBADAH,
                PSIKOSOSIAL_MASALAH: PSIKOSOSIAL_MASALAH,
                TERAPI_PENUNJANG: TERAPI_PENUNJANG
            };
    return params;
}

function mComboSpesialisasiPengkajian()
{
    var Field = ['KD_SPESIAL', 'SPESIALISASI'];
    dsSpesialisasiPengkajian = new WebApp.DataStore({fields: Field});
    dsSpesialisasiPengkajian.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100, Sort: 'kd_spesial',
                                    Sortdir: 'ASC',
                                    target: 'ViewSpesialisasi',
                                    param: ''
                                }
                    }
            );
    var cboSpesialisasiPengkajian = new Ext.form.ComboBox(
            {
                x: 120,
                y: 70,
                id: 'cboSpesialisasiPengkajian',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local', 
                forceSelection: true,
                emptyText: 'Select a Spesialisasi...',
                selectOnFocus: true,
                fieldLabel: 'Propinsi',
                align: 'Right',
                store: dsSpesialisasiPengkajian,
                valueField: 'KD_SPESIAL',
                displayField: 'SPESIALISASI',
                anchor: '20%',
                listeners:
                        {
                            'select': function (a, b, c)
                            {
                                Ext.getCmp('cboKelasPengkajian').setValue('');
                                Ext.getCmp('cboKamarPengkajian').setValue('');
                                loaddatastoreKelasPengkajian(b.data.KD_SPESIAL);
                                var tmpkriteriaPengkajian = getCriteriaFilter_viDaftar();
                                load_pengkajian(tmpkriteriaPengkajian);
                            },
                            'render': function (c) {
                                c.getEl().on('keypress', function (e) {
                                    if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                        Ext.getCmp('cboKelasPengkajian').focus();
                                }, c);
                            }

                        }
            }
    );
    return cboSpesialisasiPengkajian;
}
;
function loaddatastoreKelasPengkajian(kd_spesial) {
    dsKelasPengkajian.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100, Sort: 'kd_spesial',
                                    Sortdir: 'ASC',
                                    target: 'ViewKelasAskep',
                                    param: kd_spesial
                                }
                    }
            );
}

function mComboKelasPengkajian()
{
    var Field = ['kd_unit', 'fieldjoin', 'kd_kelas'];
    dsKelasPengkajian = new WebApp.DataStore({fields: Field});
    var cboKelasPengkajian = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 100,
                        id: 'cboKelasPengkajian',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true, mode: 'local', forceSelection: true,
                        emptyText: 'Select a kelas...',
                        selectOnFocus: true,
                        fieldLabel: '',
                        align: 'Right',
                        store: dsKelasPengkajian,
                        valueField: 'kd_unit',
                        displayField: 'fieldjoin',
                        anchor: '20%',
                        listeners:
                                {'select': function (a, b, c)
                                    {
                                        Ext.getCmp('cboKamarPengkajian').setValue('');
                                        loaddatastoreKamarPengkajian(b.data.kd_unit);
                                        var tmpkriteriaPengkajian = getCriteriaFilter_viDaftar();
                                        load_pengkajian(tmpkriteriaPengkajian);
                                    },
                                    'render': function (c) {
                                        c.getEl().on('keypress', function (e) {
                                            if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                                Ext.getCmp('cboKamarPengkajian').focus();
                                        }, c);
                                    }

                                }
                    }
            );
    return cboKelasPengkajian;
}
;
function loaddatastoreKamarPengkajian(kd_unit)
{
    dsKamarPengkajian.load
            (
                    {
                        params:
                                {
                                    Skip: 0, Take: 100,
                                    Sort: 'kd_spesial',
                                    Sortdir: 'ASC',
                                    target: 'ViewKamarAskep',
                                    param: kd_unit + '<>' + Ext.getCmp('cboSpesialisasiPengkajian').getValue()
                                }
                    });
}

function mComboKamarPengkajian()
{
    var Field = ['roomname', 'no_kamar', 'jumlah_bed', 'kd_unit', 'digunakan', 'fieldjoin'];
    dsKamarPengkajian = new WebApp.DataStore({fields: Field});
    var cboKamarPengkajian = new Ext.form.ComboBox
            (
                    {
                        x: 320,
                        y: 100,
                        id: 'cboKamarPengkajian',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true, mode: 'local', forceSelection: true,
                        emptyText: 'Select a kamar...',
                        selectOnFocus: true,
                        fieldLabel: '',
                        align: 'Right',
                        store: dsKamarPengkajian,
                        valueField: 'no_kamar',
                        displayField: 'roomname',
                        width: 120,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        var tmpkriteriaPengkajian = getCriteriaFilter_viDaftar();
                                        load_pengkajian(tmpkriteriaPengkajian);
                                    }
                                }
                    }
            );
    return cboKamarPengkajian;
};

function load_pengkajian(criteria){
    dataSource_pengkajianAwal.load
            (
                    {params:
                                {Skip: 0,
                                    Take: 50,
                                    Sort: '', Sortdir: 'ASC',
                                    // target: 'ViewAskepPengkajian',
                                    target: 'ViewTrAskepIGD',
                                    // param: criteria
                                    param: ''
                                }
                    }
            );
    return dataSource_pengkajianAwal;
}

function load_pengkajian_paramAwal(criteria){
    var KataKunci = '';
    if(Ext.getCmp('TxtCariMedrecPengkajianAwal').getValue() != '')
    {
        if (KataKunci == ''){
            KataKunci = ' and LOWER(kd_pasien) like  LOWER( ~' + Ext.get('TxtCariMedrecPengkajianAwal').getValue() + '%~) ';
        }else{
            KataKunci += ' and LOWER(kd_pasien) like  LOWER( ~' + Ext.get('TxtCariMedrecPengkajianAwal').getValue() + '%~) ';
        };
    };

    if(Ext.getCmp('TxtCariNamaPasienPengkajianAwal').getValue() != '')
    {
        if (KataKunci == ''){
            KataKunci = ' and LOWER(nama) like  LOWER( ~' + Ext.get('TxtCariNamaPasienPengkajianAwal').getValue() + '%~) ';
        }else{
            KataKunci += ' and LOWER(nama) like  LOWER( ~' + Ext.get('TxtCariNamaPasienPengkajianAwal').getValue() + '%~) ';
        };
    };

    if(Ext.getCmp('TxtCariDokterPasienPengkajianAwal').getValue() != '')
    {
        if (KataKunci == ''){
            KataKunci = ' and LOWER(nama_dokter) like  LOWER( ~%' + Ext.get('TxtCariDokterPasienPengkajianAwal').getValue() + '%~) ';
        }else{
            KataKunci += ' and LOWER(nama_dokter) like  LOWER( ~%' + Ext.get('TxtCariDokterPasienPengkajianAwal').getValue() + '%~) ';
        };
    };

    if(Ext.getCmp('dtpTanggalawalPengkajianAwal').getValue() != '' || Ext.getCmp('dtpTanggalakhirPengkajianAwal').getValue() != '')
    {
        if (KataKunci == ''){
            KataKunci = ' and (tgl_transaksi between ~'+Ext.get('dtpTanggalawalPengkajianAwal').getValue() + '~ and ~'+Ext.get('dtpTanggalakhirPengkajianAwal').getValue() + '~ ) ';
        }else{
            KataKunci += ' and (tgl_transaksi between ~'+Ext.get('dtpTanggalawalPengkajianAwal').getValue() + '~ and ~'+Ext.get('dtpTanggalakhirPengkajianAwal').getValue() + '~ ) ';
        };
    };

   /*  if(Ext.getCmp('cboStatus_viPengkajianIGDAwal').getValue() != '')
    {
        var status  = Ext.getCmp('cboStatus_viPengkajianIGDAwal').getValue();
        var posting = '';
        if ((status == '3' || status == 3)){
            posting = 'FALSE';

            if (KataKunci == ''){
                KataKunci = 'and (posting_transaksi = '+posting+')';
            }else{
                KataKunci += 'and (posting_transaksi = '+posting+')';
            };
        }else if ((status == '2' || status == 2)){
            posting = 'TRUE';

            if (KataKunci == ''){
                KataKunci = 'and (posting_transaksi = '+posting+')';
            }else{
                KataKunci += 'and (posting_transaksi = '+posting+')';
            };
        }

    }; */

    dataSource_pengkajianAwal.load
            (
                    {params:
                                {Skip: 0,
                                    Take: 50,
                                    Sort: '', Sortdir: 'ASC',
                                    // target: 'ViewAskepPengkajian',
                                    target: 'ViewTrAskepIGD',
                                    // param: criteria
                                    param: KataKunci
                                }
                    }
            );
    return dataSource_pengkajianAwal;
}
function loadfilter_pengkajian() {
    var criteria = getCriteriaFilter_viDaftar();
    dataSource_pengkajianAwal.load(
            {
                params:
                        {Skip: 0,
                            Take: 50,
                            Sort: '',
                            Sortdir: 'ASC',
                            // target: 'ViewAskepPengkajian',
                            target: 'ViewTrAskepIGD',
                            // param: criteria
                            param: ''
                        }
            });
    return dataSource_pengkajianAwal;
}
function getCriteriaFilter_viDaftar()
{
    var strKriteria = " ng.AKHIR = 't' ";
    var tmpmedrec = Ext.getCmp('TxtCariMedrecPengkajianAwal').getValue();
    var tmpnama = Ext.getCmp('TxtCariNamaPasienPengkajianAwal').getValue();
    var tmpspesialisasi = Ext.getCmp('cboSpesialisasiPengkajian').getValue();
    var tmpkelas = Ext.getCmp('cboKelasPengkajian').getValue();
    var tmpkamar = Ext.getCmp('cboKamarPengkajian').getValue();
    var tmptglawal = Ext.get('dtpTanggalawalPengkajianAwal').getValue();
    var tmptglakhir = Ext.get('dtpTanggalakhirPengkajianAwal').getValue();
    var tmptambahan = Ext.getCmp('chkTglPengkajian').getValue()

    if (tmpmedrec !== "")
    {
        strKriteria += " AND P.KD_PASIEN " + "ilike '%" + tmpmedrec + "%' ";
    }
    if (tmpnama !== "")
    {
        strKriteria += " AND P.NAMA " + "ilike '%" + tmpnama + "%' ";
    }
    if (tmpspesialisasi !== "")
    {
        if (tmpspesialisasi === '0')
        {
            strKriteria += "";
        } else
        {
            strKriteria += " AND NG.KD_SPESIAL = '" + tmpspesialisasi + "' ";
        }

    }
    if (tmpkelas !== "") {
        strKriteria += " AND ng.KD_UNIT_KAMAR='" + tmpkelas + "' ";
    }
    if (tmpkamar !== "") {
        strKriteria += " AND NG.NO_KAMAR= '" + tmpkamar + "' ";
    }
    if (tmptambahan === true)
    {
        strKriteria += " AND ng.Tgl_masuk Between '" + tmptglawal + "'  and '" + tmptglakhir + "' ";
    }
    strKriteria += ' Limit 50 ';
    return strKriteria;
}

function radiojelas(tmp)
{
    if (tmp === 'rbjelasya')
    {
        Ext.getDom('rbjelastidak').checked = false;
    }
    if (Ext.getCmp('rbjelastidak').getValue() === true)
    {
        Ext.getDom('rbjelasya').checked = false;
    }
}

function ShowPesanWarningPengkajian(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING,
                        width: 250
                    }
            );
}
;
function ShowPesanInfoPengkajian(str, modul) {
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO,
                        width: 250
                    }
            );
}
;
function DeleteData()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/DeleteDataObj",
                        params: ParamDelete(),
                        failure: function (o)
                        {
                            loadMask.hide();
                            ShowPesanWarningPengkajian('Hubungi Admin', 'Error');
                            loadData_RencanaAsuhan();
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                loadMask.hide();
                                ShowPesanInfoPengkajian('Data berhasil di hapus', 'Information');
                                caridatapasien();
                            }
                            else
                            {
                                loadMask.hide();
                                ShowPesanWarningPengkajian('Gagal menghapus data', 'Error');
                                caridatapasien();
                            }
                            ;
                        }
                    }

            );
}
;
function ParamDelete()
{
    var params =
            {
                // Table: 'ViewAskepPengkajian',
                Table: 'ViewTrAskepIGD',
                query: "kd_pasien_kunj = '" + tmpkdpasien + "' and kd_unit_kunj = '" + tmpkdunit + "' and urut_masuk_kunj = " + tmpurutmasuk + " and tgl_masuk_kunj = '" + tmptglmasuk + "'"
            };
    return params;
}

function GetCriteriaPengkajian()
{
    var strKriteria = '';
    strKriteria = tmpkdpasien;
    strKriteria += '##@@##' + tmpkdunit;
    strKriteria += '##@@##' + tmpurutmasuk;
    strKriteria += '##@@##' + tmptglmasuk;
    strKriteria += '##@@##' + Ext.get('TxtPopupMedrec').getValue();
    strKriteria += '##@@##' + Ext.get('TxtPopupNamaPasien').getValue();
    strKriteria += '##@@##' + Ext.get('TxtPopupAlamatPasien').getValue();
    strKriteria += '##@@##' + Ext.get('TxtPopupSpesialisasi').getValue();
    strKriteria += '##@@##' + Ext.get('TxtPopupKelas').getValue();
    strKriteria += '##@@##' + Ext.get('TxtPopupkamarPasien').getValue();
    return strKriteria;
}


function RefreshDataFilterKasirIGD() 
{

    var KataKunci='';
    
     if (Ext.get('txtFilterIGDNomedrec').getValue() != '')
    {
        if (KataKunci == '')
        {
                        KataKunci = ' and   LOWER(kd_pasien) like  LOWER( ~' + Ext.get('txtFilterIGDNomedrec').getValue() + '%~)';
            
        }
        else
        {
        
                        KataKunci += ' and  LOWER(kd_pasien) like  LOWER( ~' + Ext.get('txtFilterIGDNomedrec').getValue() + '%~)';
        };

    };
    
     if (Ext.get('TxtIGDFilternama').getValue() != '')
    {
        if (KataKunci == '')
        {
                        KataKunci = ' and   LOWER(nama) like  LOWER( ~%' + Ext.get('TxtIGDFilternama').getValue() + '%~)';
            
        }
        else
        {
        
                        KataKunci += ' and  LOWER(nama) like  LOWER( ~%' + Ext.get('TxtIGDFilternama').getValue() + '%~)';
        };

    };
    
    
     if (Ext.get('cboUNIT_viKasirIGD').getValue() != '' && Ext.get('cboUNIT_viKasirIGD').getValue() != 'All')
    {
        if (KataKunci == '')
        {
    
                        KataKunci = ' and  LOWER(nama_unit)like  LOWER(~%' + Ext.get('cboUNIT_viKasirIGD').getValue() + '%~)';
        }
        else
        {
    
                        KataKunci += ' and LOWER(nama_unit) like  LOWER(~%' + Ext.get('cboUNIT_viKasirIGD').getValue() + '%~)';
        };
    };
    if (Ext.get('TxtIGDFilterDokter').getValue() != '')
        {
        if (KataKunci == '')
        {
            
                        KataKunci = ' and  LOWER(nama_dokter) like  LOWER(~' + Ext.get('TxtIGDFilterDokter').getValue() + '%~)';
        }
        else
        {
        
                        KataKunci += ' and LOWER(nama_dokter) like  LOWER(~' + Ext.get('TxtIGDFilterDokter').getValue() + '%~)';
        };
    };
        
        
    if (Ext.get('cboStatus_viKasirIGD').getValue() == 'Posting')
    {
        if (KataKunci == '')
        {

                        KataKunci = ' and  posting_transaksi = TRUE';
        }
        else
        {
        
                        KataKunci += ' and posting_transaksi =  TRUE';
        };
    
    };
        
        
        
    if (Ext.get('cboStatus_viKasirIGD').getValue() == 'Belum Posting')
    {
        if (KataKunci == '')
        {
        
                        KataKunci = ' and  posting_transaksi = FALSE';
        }
        else
        {
    
                        KataKunci += ' and posting_transaksi =  FALSE';
        };
        
        
    };
    
    if (Ext.get('cboStatus_viKasirIGD').getValue() == 'Semua')
    {
        if (KataKunci == '')
        {
        
                        KataKunci = ' and  (posting_transaksi = FALSE OR posting_transaksi = TRUE )';
        }
        else
        {
    
                        KataKunci += ' and (posting_transaksi = FALSE OR posting_transaksi = TRUE )';
        };
        
        
    };
    
        
    if (Ext.get('dtpTglAwalFilterIGD').getValue() != '')
    {
        if (KataKunci == '')
        {                      
                        KataKunci = " and (tgl_transaksi between ~" + Ext.get('dtpTglAwalFilterIGD').getValue() + "~ and ~" + Ext.get('dtpTglAkhirFilterIGD').getValue() + "~)";
        }
        else
        {
            
                        KataKunci += " and (tgl_transaksi between ~" + Ext.get('dtpTglAwalFilterIGD').getValue() + "~ and ~" + Ext.get('dtpTglAkhirFilterIGD').getValue() + "~)";
        };
    
    };
    
     
    if (KataKunci != undefined ||KataKunci != '' ) 
    {  
        dsTRKasirIGDList.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: selectCountKasirIGD, 
                    //Sort: 'no_transaksi',
                                        Sort: 'tgl_transaksi',
                    //Sort: 'no_transaksi',
                    Sortdir: 'ASC', 
                    target:'ViewTrAskepIGD',
                    param : KataKunci
                }           
            }
        );  
        getTotKunjunganIGD();
    }
    else
    {
    getTotKunjunganIGD();
    dsTRKasirIGDList.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: selectCountKasirIGD, 
                    //Sort: 'no_transaksi',
                                        Sort: 'tgl_transaksi',
                    //Sort: 'no_transaksi',
                    Sortdir: 'ASC', 
                    target:'ViewTrAskepIGD',
                    param : ''
                }           
            }
        );   
    };
    
    return dsTRKasirIGDList;
    
};


function mComboStatusBayar_viPengkajianIGD(){
    var cboStatus_viPengkajianIGDAwal = new Ext.form.ComboBox({
        x: 120,
        y: 100,
        id              : 'cboStatus_viPengkajianIGDAwal',
        typeAhead       : true,
        triggerAction   : 'all',
        lazyRender      : true,
        mode            : 'local',
        width: 200,
        emptyText       : '',
        fieldLabel      : 'Status Posting',
        store           : new Ext.data.ArrayStore({
            id: 0,
            fields:['Id','displayText'],
            data: [[1, 'Semua'],[2, 'Posting'], [3, 'Belum Posting']]
        }),
        valueField      : 'Id',
        displayField    : 'displayText',
        value           : 1,
        listeners       :{
            select: function(a,b,c){
                // selectCountStatusByr_viKasirIGD=b.data.displayText ;
                load_pengkajian_paramAwal();
            }
        }
    });
    return cboStatus_viPengkajianIGDAwal;
};

