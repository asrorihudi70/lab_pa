
var now = new Date();
var dsSpesialisasiPengkajian;
var ListHasilPengkajian;
var rowSelectedHasilPengkajian;
var dsKelasPengkajian;
var dsKamarPengkajian;
var dataSource_pengkajian;
var rowSelected_viDaftar;
var tmpkdpasien = '';
var tmpkdunit = '';
var tmpurutmasuk = '';
var tmptglmasuk = '';
var tmpjelas = '';
var tmpefakuasicairan = '';
var tmpS1S2Tunggal = '';
var tmpNyeridada = '';
var tmpCRT = '';
var tmpJVP = '';
var FormLookUpdetailPengkajian;
var tmpediting = '';
//1                                     
var RIWAYAT_UTAMA = '';
var RIWAYAT_PENYAKIT_SEKARANG = '';
var RIWAYAT_PENYAKIT_DAHULU = '';
//2
var NAFAS_PATEN = 'false';
var NAFAS_OBSTRUKTIF = 'false';
var NAFAS_JELAS = 'false';
var NAFAS_POLA_SIMETRI = 'false';
var NAFAS_POLA_ASIMETRI = 'false';
var NAFAS_SUARA_NORMAL = 'false';
var NAFAS_SUARA_HIPERSONOR = 'false';
var NAFAS_MENURUN = 'false';
var NAFAS_JENIS_NORMAL = 'false';
var NAFAS_JENIS_TACHYPNOE = 'false';
var NAFAS_JENIS_CHEYNESTOKES = 'false';
var NAFAS_JENIS_RETRACTIVE = 'false';
var NAFAS_JENIS_KUSMAUL = 'false';
var NAFAS_JENIS_DISPNOE = 'false';
var NAFAS_RR = 0;
var NAFAS_SUARA_WHEEZING = 'false';
var NAFAS_SUARA_RONCHI = 'false';
var NAFAS_SUARA_RALES = 'false';
var NAFAS_EVAKUASI_CAIRAN = 'false';
var NAFAS_JML_CAIRAN = 0;
var NAFAS_WARNA_CAIRAN = '';
var NAFAS_MASALAH = '';
//3
var JANTUNG_REGULER = 'false';
var JANTUNG_IRREGULER = 'false';
var JANTUNG_S1S2_TUNGGAL = 'false';
var JANTUNG_NYERI_DADA = 'false';
var JANTUNG_NYERI_DADA_KET = '';
var JANTUNG_BUNYI_MURMUR = 'false';
var JANTUNG_BUNYI_GALLOP = 'false';
var JANTUNG_BUNYI_BISING = 'false';
var JANTUNG_CRT = 'false';
var JANTUNG_AKRAL_HANGAT = 'false';
var JANTUNG_AKRAL_DINGIN = 'false';
var JANTUNG_PENINGKATAN_JVP = 'false';
var JANTUNG_UKURAN_CVP = 0;
var JANTUNG_WARNA_JAUNDICE = 'false';
var JANTUNG_WARNA_SIANOTIK = 'false';
var JANTUNG_WARNA_KEMERAHAN = 'false';
var JANTUNG_WARNA_PUCAT = 'false';
var JANTUNG_TEKANAN_DARAH = '';
var JANTUNG_NADI = '';
var JANTUNG_TEMP = 0;
var JANTUNG_MASALAH = '';
//4
var OTOT_SENDI_BEBAS = 'false';
var OTOT_SENDI_TERBATAS = 'false';
var OTOT_KEKUATAN = '';
var OTOT_ODEMA_EKSTRIMITAS = 'false';
var OTOT_KELAINAN_BENTUK = 'false';
var OTOT_KREPITASI = 'false';
var OTOT_MASALAH = '';
//5
var SYARAF_GCS_EYE = 'false';
var SYARAF_GCS_VERBAL = 'false';
var SYARAF_GCS_MOTORIK = 'false';
var SYARAF_GCS_TOTAL = 0;
var SYARAF_FISIOLOGIS_BRACHIALIS = 'false';
var SYARAF_FISIOLOGIS_PATELLA = 'false';
var SYARAF_FISIOLOGIS_ACHILLES = 'false';
var SYARAF_PATOLOGIS_CHODDOKS = 'false';
var SYARAF_PATOLOGIS_BABINSKI = 'false';
var SYARAF_PATOLOGIS_BUDZINZKY = 'false';
var SYARAF_PATOLOGIS_KERNIG = 'false';
var SYARAF_MASALAH = '';
var INDRA_PUPIL_ISOKOR = 'false';
var INDRA__PUPIL_ANISOKOR = 'false';
var INDRA__KONJUNGTIVA_ANEMIS = 'false';
var INDRA__KONJUNGTIVA_ICTERUS = 'false';
var INDRA__KONJUNGTIVA_TIDAK = 'false';
var INDRA_GANGGUAN_DENGAR = 'false';
var INDRA_OTORHEA = 'false';
var INDRA_BENTUK_HIDUNG = 'false';
var INDRA_BENTUK_HIDUNG_KET = '';
var INDRA_GANGGUAN_CIUM = 'false';
var INDRA_RHINORHEA = 'false';
var INDRA_MASALAH = '';
var KEMIH_KEBERSIHAN = 'false';
var KEMIH_ALAT_BANTU = 'false';
var KEMIH_JML_URINE = 0;
var KEMIH_WARNA_URINE = '';
var KEMIH_BAU = '';
var KEMIH_KANDUNG_MEMBESAR = 'false';
var KEMIH_NYERI_TEKAN = 'false';
var KEMIH_GANGGUAN_ANURIA = 'false';
var KEMIH_GANGGUAN_ALIGURIA = 'false';
var KEMIH_GANGGUAN_RETENSI = 'false';
var KEMIH_GANGGUAN_INKONTINENSIA = 'false';
var KEMIH_GANGGUAN_DISURIA = 'false';
var KEMIH_GANGGUANHEMATURIA = 'false';
var KEMIH_MASALAH = '';
var CERNA_MAKAN_HABIS = 'false';
var CERNA_MAKAN_KET = '';
var CERNA_MAKAN_FREKUENSI = 0;
var CERNA_JML_MINUM = 0;
var CERNA_JENIS_MINUM = '';
var CERNA_MULUT_BERSIH = 'false';
var CERNA_MULUT_KOTOR = 'false';
var CERNA_MULUT_BERBAU = 'false';
var CERNA_MUKOSA_LEMBAB = 'false';
var CERNA_MUKOSA_KERING = 'false';
var CERNA_MUKOSA_STOMATITIS = 'false';
var CERNA_TENGGOROKAN_SAKIT = 'false';
var CERNA_TENGGOROKAN_NYERI_TEKAN = 'false';
var CERNA_TENGGOROKAN_PEMBESARANTO = 'false';
var CERNA_PERUT_NORMAL = 'false';
var CERNA_PERUT_DISTENDED = 'false';
var CERNA_PERUT_METEORISMUS = 'false';
var CERNA_PERUT_NYERI_TEKAN = 'false';
var CERNA_PERUT_LOKASI_NYERI = '';
var CERNA_PERISTALTIK = 0;
var CERNA_TURGOR_KULIT = 'false';
var CERNA_PEMBESARAN_HEPAR = 'false';
var CERNA_HEMATEMESIS = 'false';
var CERNA_EVAKUASI_CAIRAN_ASCITES = 'false';
var CERNA_JML_CAIRAN_ASCITES = 0;
var CERNA_WARNA_CAIRAN_ASCITES = 'false';
var CERNA_FREK_BAB = 0;
var CERNA_KONSISTENSI = '';
var CERNA_BAU_BAB = '';
var CERNA_WARNA_BAB = '';
var CERNA_MASALAH = '';
var ENDOKRIN_TYROID = 'false';
var ENDOKRIN_HIPOGLIKEMIA = 'false';
var ENDOKRIN_LUKA_GANGREN = 'false';
var ENDOKRIN__PUS = 'false';
var ENDOKRIN_MASALAH = '';
var PERSONAL_MANDI = '';
var PERSONAL_SIKATGIGI = '';
var PERSONAL_KERAMAS = '';
var PERSONAL_GANTIPAKAIAN = '';
var PERSONAL_MASALAH = '';
var PSIKOSOSIAL_ORANGDEKAT = '';
var PSIKOSOSIAL_KEGIATAN_IBADAH = '';
var PSIKOSOSIAL_MASALAH = '';
var TERAPI_PENUNJANG = '';
CurrentPage.page = getPanelPengkajian(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
function getPanelPengkajian(mod_id) {
    var Field = ['KD_DOKTER','NO_TRANSAKSI','KD_UNIT','KD_PASIEN','NAMA','NAMA_UNIT','ALAMAT','TANGGAL_TRANSAKSI',
        'NAMA_DOKTER','ANAMNESE','CAT_FISIK','KD_CUSTOMER','CUSTOMER','URUT_MASUK','POSTING_TRANSAKSI','KD_KASIR','CARA_PENERIMAAN','KD_KELAS'];
    dataSource_pengkajian = new WebApp.DataStore({
        fields: Field
    });
    load_pengkajian("ng.AKHIR = 't' limit 50");
    var gridListHasilPengkajian = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dataSource_pengkajian,
        anchor: '100% 60%',
        columnLines: false,
        autoScroll: false,
        border: true,
        sort: false,
        autoHeight: false,
        layout: 'fit',
        region: 'center',
        height: 200,
        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
            listeners: {
                rowselect: function (sm, row, rec) {
                    rowSelected_viDaftar = dataSource_pengkajian.getAt(row);
                }
            }
        }),
        listeners: {
            rowdblclick: function (sm, ridx, cidx) {
                rowSelected_viDaftar = dataSource_pengkajian.getAt(ridx);
                console.log(rowSelected_viDaftar);
                if (rowSelected_viDaftar !== undefined)
                {
                    HasilPengkajianLookUp(rowSelected_viDaftar.data);
                }
                else
                {
                    HasilPengkajianLookUp();
                }
            }
        },
        cm: new Ext.grid.ColumnModel([
            {
                header: 'Status Posting',
                width: 80,
                sortable: false,
                hideable:true,
                hidden:false,
                menuDisabled:true,
                dataIndex: 'POSTING_TRANSAKSI',
                id: 'txtposting',
                renderer: function(value, metaData, record, rowIndex, colIndex, store){
                    switch (value){
                        case 't':
                            metaData.css = 'StatusHijau'; // 
                            break;
                        case 'f':
                            metaData.css = 'StatusMerah'; // rejected
                            break;
                    }
                    return '';
                }
            },{
                id: 'colReqIdViewIGD',
                header: 'No. Transaksi',
                dataIndex: 'NO_TRANSAKSI',
                sortable: false,
                hideable:false,
                menuDisabled:true,
                width: 80
            },{
                id: 'colTglIGDViewIGD',
                header: 'Tgl Transaksi',
                dataIndex: 'TANGGAL_TRANSAKSI',
                sortable: false,
                hideable:false,
                menuDisabled:true,
                width: 75
            },{
                header: 'No. Medrec',
                width: 65,
                sortable: false,
                hideable:false,
                menuDisabled:true,
                dataIndex: 'KD_PASIEN',
                id: 'colIGDerViewIGD'
            },{
                header: 'Pasien',
                width: 190,
                sortable: false,
                hideable:false,
                menuDisabled:true,
                dataIndex: 'NAMA',
                id: 'colIGDerViewIGD'
            },{
                id: 'colLocationViewIGD',
                header: 'Alamat',
                dataIndex: 'ALAMAT',
                sortable: false,
                hideable:false,
                menuDisabled:true,
                width: 170
            },{
                id: 'colDeptViewIGD',
                header: 'Pelaksana',
                dataIndex: 'NAMA_DOKTER',
                sortable: false,
                hideable:false,
                menuDisabled:true,
                width: 150
            },{
                id: 'colImpactViewIGD',
                header: 'Unit',
                dataIndex: 'NAMA_UNIT',
                sortable: false,
                hideable:false,
                hidden:true,
                menuDisabled:true,
                width: 90
            }
        ] ),
        viewConfig: {
            forceFit: true
        },
        tbar: [{
                id: 'btnEditHasilPengkajian',
                text: 'Open List',
                tooltip: 'Open List',
                iconCls: 'Edit_Tr',
                handler: function (sm, row, rec) {
                    if (rowSelected_viDaftar !== undefined) {
                        HasilPengkajianLookUp(rowSelected_viDaftar.data);
                    } else {
                        ShowPesanWarningPengkajian('Pilih Data Terlebih Dahulu  ', 'Pengkajian');
                    }
                }
            }]
    });
    var FormDepanPengkajian = new Ext.Panel({
        id: mod_id,
        closable: true,
        region: 'center',
        layout: 'form',
        title: 'Pengkajian',
        border: false,
        shadhow: true,
        autoScroll: false,
        iconCls: 'Request',
        margins: '0 5 5 0',
        items: [
            getPanelPencarianPasien(),
            gridListHasilPengkajian
        ],
        listeners: {
            'afterrender': function () {
                // Ext.getCmp('dtpTanggalawalPengkajian').disable();
                // Ext.getCmp('dtpTanggalakhirPengkajian').disable();
            }
        }
    });
    return FormDepanPengkajian;
};

function getPanelPencarianPasien() {
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 1190,
                height: 200,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'No. Medrec '
                    },
                    {
                        x: 110,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 10,
                        xtype: 'textfield',
                        name: 'TxtCariMedrecPengkajian',
                        id: 'TxtCariMedrecPengkajian',
                        width: 80,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        var tmpNoMedrec = Ext.get('TxtCariMedrecPengkajian').getValue();
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
                                            if (tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10)
                                            {
                                                var tmpgetNoMedrec = formatnomedrec(tmpNoMedrec);
                                                Ext.getCmp('TxtCariMedrecPengkajian').setValue(tmpgetNoMedrec);
                                                load_pengkajian_param();
                                            }
                                            else
                                            {
                                                if (tmpNoMedrec.length === 10)
                                                {
                                                    load_pengkajian_param();
                                                }
                                                else{
                                                    load_pengkajian();
                                                    Ext.getCmp('TxtCariMedrecPengkajian').setValue('');
                                                }
                                            }
                                        }
                                    }
                                }
                    },
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Nama '
                    },
                    {
                        x: 110,
                        y: 40,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 40,
                        xtype: 'textfield',
                        name: 'TxtCariNamaPasienPengkajian',
                        id: 'TxtCariNamaPasienPengkajian',
                        width: 200,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
                                            // var tmpkriteriaPengkajian = getCriteriaFilter_viDaftar();
                                            load_pengkajian_param();
                                        }
                                    }
                                }
                    },
                    {
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Pelaksana '
                    },
                    {
                        x: 110,
                        y: 70,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 70,
                        xtype: 'textfield',
                        name: 'TxtCariDokterPasienPengkajian',
                        id: 'TxtCariDokterPasienPengkajian',
                        width: 200,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
                                            // var tmpkriteriaPengkajian = getCriteriaFilter_viDaftar();
                                            load_pengkajian_param();
                                        }
                                    }
                                }
                    },
                    {
                        x: 10,
                        y: 100,
                        xtype: 'label',
                        text: 'Status Posting '
                    },
                    {
                        x: 110,
                        y: 100,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboStatusBayar_viPengkajianIGD(),
                    /*{
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Spesialisasi '
                    },
                    {
                        x: 110,
                        y: 70,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboSpesialisasiPengkajian(),
                    {
                        x: 10,
                        y: 100,
                        xtype: 'label',
                        text: 'Kelas '
                    }, {
                        x: 110,
                        y: 100,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboKelasPengkajian(),
                    {
                        x: 260,
                        y: 100,
                        xtype: 'label',
                        text: 'Kamar '
                    },
                    {
                        x: 310,
                        y: 100,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboKamarPengkajian(),*/
                    {
                        x: 10,
                        y: 130,
                        xtype: 'label',
                        text: 'Tgl.Masuk '
                    },
                    {
                        x: 110,
                        y: 130,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 130,
                        xtype: 'datefield',
                        fieldLabel: 'Tgl.Masuk ',
                        name: 'dtpTanggalawalPengkajian',
                        id: 'dtpTanggalawalPengkajian', 
                        format: 'd/M/Y',
                        value: now,
                        width: 122,
                        listeners:{
                            'specialkey': function ()
                            {
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                {
                                    load_pengkajian_param();
                                }
                            }
                        }
                    },
                    {
                        x: 260,
                        y: 130,
                        xtype: 'label',
                        text: 's/d '
                    },
                    {
                        x: 320,
                        y: 130,
                        xtype: 'datefield',
                        name: 'dtpTanggalakhirPengkajian',
                        id: 'dtpTanggalakhirPengkajian',
                        format: 'd/M/Y',
                        value: now,
                        width: 120,
                        listeners:{
                            'specialkey': function ()
                            {
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                {
                                    load_pengkajian_param();
                                }
                            }
                        }

                    },/*
                    {
                        x: 450,
                        y: 130,
                        xtype: 'checkbox',
                        id: 'chkTglPengkajian',
                        text: ' ',
                        hideLabel: false,
                        checked: false,
                        handler: function ()
                        {
                            if (this.getValue() === true)
                            {
                                Ext.getCmp('dtpTanggalawalPengkajian').enable();
                                Ext.getCmp('dtpTanggalawalPengkajian').enable();
                            }
                            else
                            {
                                Ext.getCmp('dtpTanggalawalPengkajian').disable();
                                Ext.getCmp('dtpTanggalawalPengkajian').disable();
                            }
                        }
                    },*/
                    {
                        x: 10,
                        y: 160,
                        xtype: 'button',
                        text: 'Cari',
                        iconCls: 'search',
                        id: 'btnpencariandatapengkajian',
                        handler: function () {
                            // var tmpkriteriaPengkajian = getCriteriaFilter_viDaftar();
                            load_pengkajian_param();
                        }
                    }
                ]
            }
        ]
    };
    return items;
}
;
function HasilPengkajianLookUp(rowdata) {
    FormLookUpdetailPengkajian = new Ext.Window({
        id: 'wHasilPengkajian',
        title: 'Pengkajian',
        closeAction: 'destroy',
        closable: false,
        // width: 1190,
		maximized:true,
        border: false,
        // resizable: false,
        resizable : true,
        plain: true,
        layout: 'fit',
        constrain: true,
        iconCls: 'Request',
        modal: true,
        items: [formpopuppengkajian()],
        listeners: {
            beforedestroy: function (win)
            {

            }
        }
    });
    FormLookUpdetailPengkajian.show();
    if (rowdata === undefined) {

    } else {
        tmpkdpasien = rowdata.KD_PASIEN;
        tmpkdunit = rowdata.KD_UNIT;
        tmpurutmasuk = rowdata.URUT_MASUK;
        tmptglmasuk = rowdata.TGL_MASUK;
        datainit_pengkajian(rowdata);
    }

}
;
function formpopuppengkajian() {
    var FrmTabs_popupdatahasilrad = new Ext.Panel
            (
                    {
                        id: 'formpopuppengkajian',
                        closable: true,
                        region: 'center',
                        itemCls: 'blacklabel',
                        bodyStyle: 'padding: 5px 5px 5px 5px',
                        border: false,
                        shadhow: true,
                        margins: '5 5 5 5',
                        anchor: '100%',
                        iconCls: '',
                        items:
                                [
                                    // PanelPengkajian(),
                                    PanelPengkajian_1(),
                                    // PanelPengkajian_2(),
                                    // TabPanelPengkajian()
                                    
									{
										xtype: 'panel',
										layout: 'fit',
										items : [
											TabMainPanelPengkajian(),
										]
									}
                                ],
                        tbar:
                                {
                                    xtype: 'toolbar',
                                    items:
                                            [
                                                {
                                                    xtype: 'button',
                                                    text: 'Save',
                                                    iconCls: 'save',
                                                    id: 'btnSimpan_viDaftar',
                                                    disabled: true,
                                                    handler: function ()
                                                    {
                                                        SavingData();
                                                    }
                                                },
                                                {
                                                    xtype: 'tbseparator'
                                                },
                                                {
                                                    xtype: 'button',
                                                    text: 'Delete',
                                                    iconCls: 'remove',
                                                    id: 'btnHapus_viDaftar',
                                                    disabled: false,
                                                    handler: function ()
                                                    {
                                                        Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function (button) {
                                                            if (button === 'yes') {
                                                                loadMask.show();
                                                                DeleteData();
                                                            }
                                                        });
                                                    }
                                                },
                                                {
                                                    xtype: 'tbseparator'
                                                },
                                                {
                                                    xtype: 'button',
                                                    text: 'Cetak',
                                                    iconCls: 'print',
                                                    id: 'btnCetak_viDaftarPengkajian',
                                                    handler: function ()
                                                    {
                                                        var criteria = GetCriteriaPengkajian();
                                                        loadMask.show();
                                                        loadlaporanAskep('0', 'LapPengkajianKeperawatan', criteria, function () {
                                                            loadMask.hide();
                                                        });
                                                    }
                                                },
                                                '->',
                                                {
                                                    xtype: 'button',
                                                    text: 'Close',
                                                    iconCls: 'closeform',
                                                    id: 'btnclose_viDaftar',
                                                    handler: function ()
                                                    {
                                                        if (tmpediting === 'true')
                                                        {
                                                            Ext.Msg.confirm('Warning', 'Anda Belum Menyimpan Data', function (button) {
                                                                if (button === 'yes') {
                                                                    tmpediting = 'false';
                                                                    SavingData();
                                                                    FormLookUpdetailPengkajian.close();
                                                                } else
                                                                {
                                                                    tmpediting = 'false';
                                                                    FormLookUpdetailPengkajian.close();
                                                                }
                                                            });
                                                        } else
                                                        {
                                                            FormLookUpdetailPengkajian.close();
                                                        }

                                                    }
                                                }
                                            ]
                                }
                    }
            );
    return FrmTabs_popupdatahasilrad;
};

function PanelPengkajian_2() {
    var items = {
        layout: 'column',
        bodyStyle: 'padding: 10px 10px 10px 10px',
        width : '100%',
        border: true,
        items: [
            {
                columnWidth:.4,
                layout: 'form',
                border:false,
                items: [
                    {
                        xtype:'textfield',
                        fieldLabel: 'Alamat',
                        name: 'TxtPopupAlamatPasien',
                        id: 'TxtPopupAlamatPasien',
                        anchor:'95%'
                    },
                ]
            },{
                columnWidth:.4,
                layout: 'form',
                border:false,
                items: [
                    {
                        xtype:'textfield',
                        fieldLabel: 'Pelaksana',
                        name: 'TxtPopupPelaksana',
                        id: 'TxtPopupPelaksana',
                        anchor:'95%'
                    },
                ]
            }
        ]
    };
    return items;
}

function PanelPengkajian() {
    var items = {
        layout: 'column',
        bodyStyle: 'padding: 10px 10px 10px 10px',
        width : '100%',
        border: true,
        items: [
            /*{
                columnWidth:.2,
                layout: 'form',
                border:false,
                items: [
                    {
                        xtype:'textfield',
                        fieldLabel: 'No. Medrec',
                        name: 'kd_pasien',
                        name: 'TxtPopupMedrec',
                        id: 'TxtPopupMedrec',
                        anchor:'95%'
                    },{
                        xtype:'textfield',
                        fieldLabel: 'Kode Unit',
                        name: 'TxtPopupKdUnit',
                        id: 'TxtPopupKdUnit',
                        anchor:'95%'
                    },
                ]
            },{
                columnWidth:.4,
                layout: 'form',
                border:false,
                items: [
                    {
                        xtype:'textfield',
                        fieldLabel: 'Nama Pasien',
                        name: 'TxtPopupNamaPasien',
                        id: 'TxtPopupNamaPasien',
                        anchor:'95%'
                    },{
                        xtype:'textfield',
                        fieldLabel: 'Nama Unit',
                        name: 'TxtPopupUnit',
                        id: 'TxtPopupUnit',
                        anchor:'95%'
                    },
                ]
            },{
                columnWidth:.2,
                layout: 'form',
                border:false,
                items: [
                    {
                        xtype:'datefield',
                        fieldLabel: 'Waktu Mulai',
                        name: 'textfieldTgl_RespMulai',
                        id: 'textfieldTgl_RespMulai',
                        format: 'Y-m-d',
                        value : now.format('Y-m-d'),
                        anchor:'95%',
                    },{
                        xtype:'datefield',
                        fieldLabel: 'Waktu Selesai',
                        name: 'textfieldTgl_RespSelesai',
                        id: 'textfieldTgl_RespSelesai',
                        format: 'Y-m-d',
                        value : now.format('Y-m-d'),
                        anchor:'95%',
                    },
                ]
            },{
                columnWidth:.2,
                layout: 'form',
                border:false,
                items: [
                    {
                        xtype:'timefield',
                        name: 'textfieldJam_RespMulai',
                        id: 'textfieldJam_RespMulai',
                        format: 'H:i:s',
                        value : now.format('H:i:s'),
                        anchor:'95%',
                    },
                    {
                        xtype:'timefield',
                        name: 'textfieldJam_RespSelesai',
                        id: 'textfieldJam_RespSelesai',
                        format: 'H:i:s',
                        value : now.format('H:i:s'),
                        anchor:'95%',
                    },
                ]
            }*/
            {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 1190,
                height: 130,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'No. Medrec '
                    },
                    {
                        x: 110,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 10,
                        xtype: 'textfield',
                        name: 'TxtPopupMedrec',
                        id: 'TxtPopupMedrec',
                        width: 80
                    },
                    {
                        x: 210,
                        y: 10,
                        xtype: 'label',
                        text: 'Nama '
                    },
                    {
                        x: 270,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 280,
                        y: 10,
                        xtype: 'textfield',
                        name: 'TxtPopupNamaPasien',
                        id: 'TxtPopupNamaPasien',
                        width: 200
                    },
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Alamat '
                    },
                    {
                        x: 110,
                        y: 40,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 40,
                        xtype: 'textfield',
                        name: 'TxtPopupAlamatPasien',
                        id: 'TxtPopupAlamatPasien',
                        width: 500
                    },
                    {
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Kode Unit '
                    },
                    {
                        x: 110,
                        y: 70,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 70,
                        xtype: 'textfield',
                        name: 'TxtPopupKdUnit',
                        id: 'TxtPopupKdUnit',
                        width: 80
                    },
                    {
                        x: 210,
                        y: 70,
                        xtype: 'label',
                        text: 'Unit '
                    },
                    {x: 270,
                        y: 70,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 280,
                        y: 70,
                        xtype: 'textfield',
                        name: 'TxtPopupUnit',
                        id: 'TxtPopupUnit',
                        width: 200
                    },
                    {
                        x: 10,
                        y: 100,
                        xtype: 'label',
                        text: 'Pelaksana '
                    },
                    {
                        x: 110,
                        y: 100,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 100, xtype: 'textfield',
                        name: 'TxtPopupPelaksana',
                        id: 'TxtPopupPelaksana',
                        width: 500
                    },{
                        x: 650,
                        y: 10,
                        xtype: 'label',
                        text: 'Waktu Mulai '
                    },{
                        x: 720,
                        y: 10,
                        xtype: 'label',
                        text: ':'
                    },{
                        x: 730,
                        y: 10,
                        xtype: 'datefield',
                        id: 'textfieldTgl_RespMulai',
                        format: 'Y-m-d',
                        value : now.format('Y-m-d'),
                        width: 100,
                    },{
                        x: 850,
                        y: 10,
                        xtype: 'timefield',
                        id: 'textfieldJam_RespMulai',
                        format: 'H:i:s',
                        value : now.format('H:i:s'),
                        width: 100,
                    },{
                        x: 650,
                        y: 40,
                        xtype: 'label',
                        text: 'Waktu Selesai '
                    },{
                        x: 720,
                        y: 40,
                        xtype: 'label',
                        text: ':'
                    },{
                        x: 730,
                        y: 40,
                        xtype: 'datefield',
                        id: 'textfieldTgl_RespSelesai',
                        value : now.format('Y-m-d'),
                        format: 'Y-m-d',
                        width: 100,
                    },{
                        x: 850,
                        y: 40,
                        xtype: 'timefield',
                        id: 'textfieldJam_RespSelesai',
                        format: 'H:i:s',
                        value : now.format('H:i:s'),
                        width: 100,
                    },{
                        x: 650,
                        y: 70,
                        xtype: 'button',
                        text: 'Simpan'
                    },
                ]
            }
        ]
    };
    return items;
};
function PanelPengkajian_1() {
    var items = {
        layout: 'column',
        bodyStyle: 'padding: 10px 10px 10px 10px',
        width : '100%',
        border: true,
        items: [
            {
                columnWidth:.2,
                layout: 'form',
                border:false,
                items: [
                    {
                        xtype:'textfield',
                        fieldLabel: 'No. Medrec',
                        name: 'kd_pasien',
                        name: 'TxtPopupMedrec',
                        id: 'TxtPopupMedrec',
                        readOnly:true,
                        anchor:'95%'
                    },{
                        xtype:'textfield',
                        fieldLabel: 'Kode Unit',
                        name: 'TxtPopupKdUnit',
                        id: 'TxtPopupKdUnit',
                        readOnly:true,
                        anchor:'95%'
                    },{
                        xtype:'textfield',
                        fieldLabel: 'Pelaksana',
                        name: 'TxtPopupPelaksana',
                        id: 'TxtPopupPelaksana',
                        readOnly:true,
                        anchor:'95%'
                    },
                ]
            },{
                columnWidth:.4,
                layout: 'form',
                border:false,
                items: [
                    {
                        xtype:'textfield',
                        fieldLabel: 'Nama Pasien',
                        name: 'TxtPopupNamaPasien',
                        id: 'TxtPopupNamaPasien',
                        readOnly:true,
                        anchor:'95%'
                    },{
                        xtype:'textfield',
                        fieldLabel: 'Nama Unit',
                        name: 'TxtPopupUnit',
                        id: 'TxtPopupUnit',
                        readOnly:true,
                        anchor:'95%'
                    },{
                        xtype:'textfield',
                        fieldLabel: 'Alamat',
                        name: 'TxtPopupAlamatPasien',
                        id: 'TxtPopupAlamatPasien',
                        readOnly:true,
                        anchor:'95%'
                    },
                ]
            },{
                columnWidth:.2,
                layout: 'form',
                border:false,
                items: [
                    {
                        xtype:'datefield',
                        fieldLabel: 'Waktu Datang',
                        name: 'textfieldTgl_RespMulai',
                        id: 'textfieldTgl_RespMulai',
                        format: 'Y-m-d',
                        value : now.format('Y-m-d'),
                        anchor:'95%',
                    },{
                        xtype:'datefield',
                        fieldLabel: 'Waktu Ditindak',
                        name: 'textfieldTgl_RespSelesai',
                        id: 'textfieldTgl_RespSelesai',
                        format: 'Y-m-d',
                        value : now.format('Y-m-d'),
                        anchor:'95%',
                    },
                ]
            },{
                columnWidth:.2,
                layout: 'form',
                border:false,
                items: [
                    {
                        xtype:'timefield',
                        name: 'textfieldJam_RespMulai',
                        id: 'textfieldJam_RespMulai',
                        fieldLabel: 'Jam Datang',
                        format: 'H:i:s',
                        value : now.format('H:i:s'),
                        anchor:'95%',
                    },{
                        xtype:'timefield',
                        name: 'textfieldJam_RespSelesai',
                        id: 'textfieldJam_RespSelesai',
                        fieldLabel: 'Jam Ditindak',
                        format: 'H:i:s',
                        value : now.format('H:i:s'),
                        anchor:'95%',
                    },{
                        xtype:'button',
                        name: 'btnfieldResponetime',
                        id: 'btnfieldResponetime',
                        text : 'Simpan',
                        anchor:'95%',
                    },
                ]
            }
        ]
    };
    return items;
};


function form_airway() { 
	var items = [
		{
			xtype: 'checkboxgroup',
			columns: 2,
			items : [
				{
					boxLabel: 'Paten',
					name: 'checked_airway_paten'
				},{
					boxLabel: 'Snoring',
					name: 'checked_airway_snoring'
				},{
					boxLabel: 'Gurgling',
					name: 'checked_airway_gurgling'
				},{
					boxLabel: 'Stridor',
					name: 'checked_airway_stridor'
				},{
					boxLabel: 'Wheezing',
					name: 'checked_airway_wheezing'
				}, 
			]
		}
	];
	return items;
};

function form_jenis_luka() { 
	var items = [
		{
			xtype: 'checkboxgroup',
			fieldLabel : 'Jenis luka',
			columns: 1,
			items : [
				{
					boxLabel: 'Vulnus ekskoriatum',
					name: 'checked_jenis_luka_vulnus_ekskoriatum'
				},{
					boxLabel: 'Vulnus laseratum',
					name: 'checked_jenis_luka_vulnus_laseratum'
				},{
					boxLabel: 'Vulnus morsum',
					name: 'checked_jenis_luka_vulnus_morsum'
				},{
					boxLabel: 'Vulnus punctum',
					name: 'checked_jenis_luka_vulnus_punctum'
				},{
					boxLabel: 'Vulnus sklopirotum',
					name: 'checked_jenis_luka_vulnus_sklopirotum'
				}, 
			]
		}
	];
	return items;
};

function form_luka_bakar() { 
	var items = [
		{
			fieldLabel: 'Luka Bakar',
			name: 'checked_jenis_luka_bakar'
		},{
			xtype : 'textfield',
			fieldLabel : 'Luas (%)',
			name : 'txt_luka_bakar_luas',
			id : 'txt_luka_bakar_luas',
			disabled : true,
		},{
			xtype : 'textfield',
			fieldLabel : 'Derajat (Cel)',
			name : 'txt_luka_bakar_derajat',
			id : 'txt_luka_bakar_derajat',
			disabled : true,
		},{
			xtype : 'textfield',
			fieldLabel : 'Luas Luka (cm)',
			name : 'txt_luka_bakar_luas_luka',
			id : 'txt_luka_bakar_luas_luka',
			disabled : true,
		},{
			xtype : 'textfield',
			fieldLabel : 'Lokasi Luka',
			name : 'txt_luka_bakar_lokasi_luka',
			id : 'txt_luka_bakar_lokasi_luka',
			disabled : true,
		},
	];
	return items;
};

function form_ekg() { 
	var items = [
		{
			xtype: 'checkboxgroup',
			columns: 2,
			items : [
				{
					boxLabel	: 'Irama Teratur',
					name		: 'check_ekg_irama_teratur',
					id			: 'check_ekg_irama_teratur',
				},{
					boxLabel	: 'STEMI',
					name		: 'check_ekg_stemi',
					id			: 'check_ekg_stemi',
				},{
					boxLabel	: 'Irama tidak teratur',
					name		: 'check_ekg_irama_tidak_teratur',
					id			: 'check_ekg_irama_tidak_teratur',
				},{
					boxLabel	: 'NSTEMI',
					name		: 'check_ekg_nstemi',
					id			: 'check_ekg_nstemi',
				},
			]
		}
	];
	return items;
};

function form_riwayat_cairan() { 
	var items = [
		{
			xtype: 'checkboxgroup',
			columns: 2,
			items : [
				{
					boxLabel	: 'Diare',
					name		: 'check_riwayat_cairan_irama_diare',
					id			: 'check_riwayat_cairan_irama_diare',
				},{
					boxLabel	: 'Muntah',
					name		: 'check_riwayat_cairan_muntah',
					id			: 'check_riwayat_cairan_muntah',
				},{
					boxLabel	: 'Luka Bakar',
					name		: 'check_riwayat_cairan_luka_bakar',
					id			: 'check_riwayat_cairan_luka_bakar',
				},{
					boxLabel	: 'Perdarahan',
					name		: 'check_riwayat_cairan_perdarahan',
					id			: 'check_riwayat_cairan_perdarahan',
				},
			]
		}
	];
	return items;
};

function form_disability_gcs() { 
	var items = [
		{
			xtype: 'checkboxgroup',
			fieldLabel : 'GCS',
			columns: 2,
			items : [
				{
					boxLabel	: '3-8',
					name		: 'check_disability_gcs_3_8',
					id			: 'check_disability_gcs_3_8',
				},{
					boxLabel	: '9-13',
					name		: 'check_disability_gcs_9_13',
					id			: 'check_disability_gcs_9_13',
				},{
					boxLabel	: '14-15',
					name		: 'check_disability_gcs_14_15',
					id			: 'check_disability_gcs_14_15',
				},
			]
		}
	];
	return items;
};

function form_ukuran_pupil() { 
	var items = [
		{
			xtype: 'radiogroup',
			fieldLabel : 'Ukuran Pupil',
			columns: 2,
			items : [
				{
					boxLabel	: 'Isokar',
					name		: 'radio_ukuran_pupil',
					id			: 'radio_ukuran_pupil_isokar',
				},{
					boxLabel	: 'Anisokor',
					name		: 'radio_ukuran_pupil',
					id			: 'radio_ukuran_pupil_anisokor',
				},
			]
		}
	];
	return items;
};

function form_crt() { 
	var items = [
		{
			xtype: 'radiogroup', 
			fieldLabel 	: '',
			items : [
				{
					boxLabel	: '< 2 Detik',
					name		: 'radio_crt',
					id			: 'radio_crt_kurang',
				},{
					boxLabel	: '> 2 Detik',
					name		: 'radio_crt',
					id			: 'radio_crt_lebih',
				},
			]
		}
	];
	return items;
};
function form_turgor_kulit() { 
	var items = [
		{
			xtype: 'radiogroup', 
			fieldLabel 	: '',
			items : [
				{
					boxLabel	: 'Normal',
					name		: 'radio_turgor_kulit',
					id			: 'radio_turgor_kulit_normal',
				},{
					boxLabel	: 'Kurang',
					name		: 'radio_turgor_kulit',
					id			: 'radio_turgor_kulit_kurang',
				},
			]
		}
	];
	return items;
};

function form_diameter() { 
	var items = [
		{
			xtype: 'radiogroup', 
			fieldLabel 	: 'Diameter',
			columns : 2,
			items : [
				{
					boxLabel	: '1 mm',
					name		: 'radio_diameter',
					id			: 'radio_diameter_1_mm',
				},{
					boxLabel	: '2 mm',
					name		: 'radio_diameter',
					id			: 'radio_diameter_2_mm',
				},{
					boxLabel	: '3 mm',
					name		: 'radio_diameter',
					id			: 'radio_diameter_3_mm',
				},{
					boxLabel	: '4 mm',
					name		: 'radio_diameter',
					id			: 'radio_diameter_4_mm',
				},
			]
		}
	];
	return items;
};

function form_warna_kulit() { 
	var items = [
		{
			xtype: 'radiogroup', 
			fieldLabel 	: '',
			items : [
				{
					boxLabel	: 'Pucat',
					name		: 'radio_warna_kulit',
					id			: 'radio_warna_kulit_pucat',
				},{
					boxLabel	: 'Sianosis',
					name		: 'radio_warna_kulit',
					id			: 'radio_warna_kulit_sianosis',
				},{
					boxLabel	: 'Pink',
					name		: 'radio_warna_kulit',
					id			: 'radio_warna_kulit_pink',
				},
			]
		}
	];
	return items;
};

function form_akral() { 
	var items = [
		{
			xtype: 'radiogroup', 
			fieldLabel 	: '',
			items : [
				{
					boxLabel	: 'Hangat',
					name		: 'radio_breathing_akral',
					id			: 'radio_breathing_akral_hangat',
				},{
					boxLabel	: 'Dingin',
					name		: 'radio_breathing_akral',
					id			: 'radio_breathing_akral_dingin',
				},
			]
		}
	];
	return items;
};

function form_breathing() { 
	var items = [
		{
			fieldLabel 	: 'RR (x/menit)',
			xtype 		: 'textfield',
			name 		: 'txt_breathing_rr',
			id 			: 'txt_breathing_rr',
			width 		: '100%',
		},{
			xtype: 'radiogroup', 
			fieldLabel 	: 'Pengguna Otot Bantu Napas',
			items : [
				{
					boxLabel	: 'Tidak',
					name		: 'radio_breathing_bantu_napas',
					id			: 'radio_breathing_bantu_napas_tidak',
				},{
					boxLabel	: 'Ya',
					name		: 'radio_breathing_bantu_napas',
					id			: 'radio_breathing_bantu_napas_ya',
				},
			]
		},{
			fieldLabel 	: 'Gerakan Dada',
			boxLabel	: 'Simetris',
			name		: 'checked_breathing_simetris',
			id			: 'checked_breathing_simetris',
		},{
			fieldLabel 	: '',
			boxLabel	: 'Asimetris',
			name 		: 'checked_breathing_asimetris',
			id 			: 'checked_breathing_asimetris',
		},{
			fieldLabel 	: '',
			boxLabel	: 'Asidosis',
			name 		: 'checked_breathing_asidosis',
			id 			: 'checked_breathing_asidosis',
		},{
			fieldLabel 	: '',
			boxLabel	: 'Alkalosis',
			name 		: 'checked_breathing_alkalosis',
			id 			: 'checked_breathing_alkalosis',
		},{
			fieldLabel 	: '',
			boxLabel	: 'SaO2',
			name 		: 'checked_breathing_sao2',
			id 			: 'checked_breathing_sao2',
		},{
			fieldLabel 	: '',
			xtype 		: 'textfield',
			name 		: 'txt_breathing_sao2',
			id 			: 'txt_breathing_sao2',
			disabled 	: true,
			width 		: '100%',
		},{
			fieldLabel 	: 'Suhu Tubuh (C)',
			xtype 		: 'textfield',
			name 		: 'txt_breathing_suhu',
			id 			: 'txt_breathing_suhu',
			width 		: '100%',
		},{
			fieldLabel 	: 'Riwayat Demam (hari)',
			xtype 		: 'textfield',
			name 		: 'txt_breathing_rwyt_demam',
			id 			: 'txt_breathing_rwyt_demam',
			width 		: '100%',
		},{
			fieldLabel 	: 'Riwayat Penyakit',
			boxLabel	: 'Hipertensi',
			name		: 'checked_breathing_hipertensi',
			id			: 'checked_breathing_hipertensi',
		},{
			fieldLabel 	: '',
			boxLabel	: 'Diabetes',
			name		: 'checked_breathing_diabetes',
			id			: 'checked_breathing_diabetes',
		},{
			xtype: 'radiogroup', 
			fieldLabel 	: 'Riwayat Alergi',
			items : [
				{
					boxLabel	: 'Tidak',
					name		: 'radio_breathing_alergi',
					id			: 'radio_breathing_alergi_tidak',
				},{
					boxLabel	: 'Ya',
					name		: 'radio_breathing_alergi',
					id			: 'radio_breathing_alergi_ya',
				},
			]
		},{
			fieldLabel 	: '',
			xtype 		: 'textfield',
			name 		: 'txt_breathing_alergi_ya',
			id 			: 'txt_breathing_alergi_ya',
			width 		: '100%',
		},
	];
	return items;
};

function form_circulation() { 
	var items = [
		{
			fieldLabel 	: 'HR (x/menit)',
			xtype 		: 'textfield',
			name 		: 'txt_circulation_hr',
			id 			: 'txt_circulation_hr',
			width 		: '100%',
		},{
			xtype: 'checkboxgroup', 
			fieldLabel 	: 'Gerakan Dada',
			columns 	: 2,
			items : [
				{
					boxLabel	: 'Teratur',
					name		: 'checked_circulation_teratur',
					id			: 'checked_circulation_teratur',
				},{
					boxLabel	: 'Tidak Teratur',
					name 		: 'checked_circulation_tidak_teratur',
					id 			: 'checked_circulation_tidak_teratur',
				},{
					boxLabel	: 'Kuat',
					name 		: 'checked_circulation_kuat',
					id 			: 'checked_circulation_kuat',
				},{
					boxLabel	: 'Lemah',
					name 		: 'checked_circulation_lemah',
					id 			: 'checked_circulation_lemah',
				}
			]
		},{
			fieldLabel 	: 'TD (mmHg)',
			xtype 		: 'textfield',
			name 		: 'txt_circulation_td',
			id 			: 'txt_circulation_td',
			width 		: '100%',
		},
	];
	return items;
};

function form_kekuatan_otot() { 
	var items = [
		{
			xtype 		: 'checkboxgroup',
			columns 	: 2,
			items 		: [
				{
					boxLabel	: '1',
					name		: 'checked_kekuatan_otot_1',
					id			: 'checked_kekuatan_otot_1',
				},{
					xtype 		: 'textfield',
					name		: 'txt_kekuatan_otot_1',
					id			: 'txt_kekuatan_otot_1',
					width 		: '100%',
					disabled 	: true,
				},
				{
					boxLabel	: '2',
					name		: 'checked_kekuatan_otot_2',
					id			: 'checked_kekuatan_otot_2',
				},{
					xtype 		: 'textfield',
					name		: 'txt_kekuatan_otot_2',
					id			: 'txt_kekuatan_otot_2',
					width 		: '100%',
					disabled 	: true,
				},
				{
					boxLabel	: '3',
					name		: 'checked_kekuatan_otot_3',
					id			: 'checked_kekuatan_otot_3',
				},{
					xtype 		: 'textfield',
					name		: 'txt_kekuatan_otot_3',
					id			: 'txt_kekuatan_otot_3',
					width 		: '100%',
					disabled 	: true,
				},
				{
					boxLabel	: '4',
					name		: 'checked_kekuatan_otot_4',
					id			: 'checked_kekuatan_otot_4',
				},{
					xtype 		: 'textfield',
					name		: 'txt_kekuatan_otot_4',
					id			: 'txt_kekuatan_otot_4',
					width 		: '100%',
					disabled 	: true,
				},
				{
					boxLabel	: '5',
					name		: 'checked_kekuatan_otot_5',
					id			: 'checked_kekuatan_otot_5',
				},{
					xtype 		: 'textfield',
					name		: 'txt_kekuatan_otot_5',
					id			: 'txt_kekuatan_otot_5',
					width 		: '100%',
					disabled 	: true,
				},
			]
		}
	];
	return items;
};

function TabMainPanelPengkajian() {
    var items = {
		xtype: 'tabpanel',
		plain: true,
		activeTab: 0,
        maximized : true,
        height : 500,
        defaults:{
       		bodyStyle: 'padding:5px 5px 5px 5px;',
       		autoScroll: true
		},
		listeners: {
			tabchange: function (tab) {
				console.log(tab);
			}
		},
		items: [
			{
				title: 'Awal Keperawatan',
				layout: 'column',
				defaults: {width: 340},
				border: false,
				items: [
					{
						border: false,
						layout: 'form',
						defaults: {width: 320},
						defaultType: 'textfield',
						items : [
							{
								xtype: 'fieldset',
								title: 'Airway',
								autoHeight: true,
								labelStyle: 'display:none;',
								labelWidth: 1,
								defaultType: 'checkbox', // each item will be a checkbox
								items: [
									form_airway(),
								]
							},{
								xtype: 'fieldset',
								title: 'Breathing',
								autoHeight: true,
								defaultType: 'checkbox', // each item will be a checkbox
								items: [
									form_breathing(),
								]
							}
						]
					},{
						border: false,
						layout: 'form',
						defaults: {width: 320},
						defaultType: 'textfield',
						items : [
							{
								xtype: 'fieldset',
								title: 'Circulation',
								autoHeight: true,
								defaultType: 'checkbox', // each item will be a checkbox
								items: [
									form_circulation(),
								]
							},{
								xtype: 'fieldset',
								title: 'EKG',
								autoHeight: true,
								labelStyle: 'display:none;',
								labelWidth: 1,
								defaultType: 'checkbox', // each item will be a checkbox
								items: [
									form_ekg(),
								]
							},{
								xtype: 'fieldset',
								title: 'CRT',
								autoHeight: true,
								labelStyle: 'display:none;',
								labelWidth: 1,
								defaultType: 'checkbox', // each item will be a checkbox
								items: [
									form_crt(),
								]
							},{
								xtype: 'fieldset',
								title: 'Akral',
								autoHeight: true,
								labelStyle: 'display:none;',
								labelWidth: 1,
								defaultType: 'checkbox', // each item will be a checkbox
								items: [
									form_akral(),
								]
							},{
								xtype: 'fieldset',
								title: 'Turgor Kulit',
								autoHeight: true,
								labelStyle: 'display:none;',
								labelWidth: 1,
								defaultType: 'checkbox', // each item will be a checkbox
								items: [
									form_turgor_kulit(),
								]
							},{
								xtype: 'fieldset',
								title: 'Warna Kulit',
								autoHeight: true,
								labelStyle: 'display:none;',
								labelWidth: 1,
								defaultType: 'checkbox', // each item will be a checkbox
								items: [
									form_warna_kulit(),
								]
							},
						]
					},{
						border: false,
						layout: 'form',
						defaults: {width: 320},
						defaultType: 'textfield',
						items : [
							{
								xtype: 'fieldset',
								title: 'Riwayat kehilangan cairan',
								autoHeight: true,
								labelStyle: 'display:none;',
								labelWidth: 1,
								defaultType: 'checkbox', // each item will be a checkbox
								items: [
									form_riwayat_cairan(),
								]
							},{
								xtype: 'fieldset',
								title: 'DISABILITY',
								autoHeight: true,
								defaultType: 'checkbox', // each item will be a checkbox
								items: [
									form_disability_gcs(),
									form_ukuran_pupil(),
									form_diameter(),
								]
							},{
								xtype: 'fieldset',
								title: 'Kekuatan Otot',
								autoHeight: true,
								labelStyle: 'display:none;',
								labelWidth: 1,
								defaultType: 'checkbox', // each item will be a checkbox
								items: [
									form_kekuatan_otot(),
								]
							},
						]
					},{
						border: false,
						layout: 'form',
						defaults: {width: 320},
						defaultType: 'textfield',
						items : [
							{
								xtype: 'fieldset',
								title: 'EXPOSURE',
								autoHeight: true,
								defaultType: 'checkbox', // each item will be a checkbox
								items: [
									form_jenis_luka(),
									form_luka_bakar(),
								]
							},
						]
					},
				]
            },{
                title: 'Penunjang',
                layout: 'form',
            },{
                title: 'Riwayat Kesehatan',
                layout: 'form',
            },
        ]
	};
	return items;
}
function TabPanelPengkajian() {
    var items = {
        xtype: 'tabpanel',
        plain: true,
        activeTab: 0,
        height: 500,
        width: 1190,
        defaults:
                {
                    bodyStyle: 'padding:2px',
                    autoScroll: true
                },
        listeners: {
            tabchange: function (tab) {

            }
        },
        items: [
            {
                title: 'Rwy. Penyakit',
                layout: 'form',
                items:
                        [
                            riwayatpenyakit()
                        ]
            },
            {
                title: "Airway'n B.",
                layout: 'form',
                items:
                        [
                            Airway()
                        ]
            },
            {
                title: 'Circulation',
                layout: 'form',
                items:
                        [
                            Circulation()
                        ]
            },
            {
                title: 'Musculo',
                layout: 'form',
                items:
                        [
                            Musculo()
                        ]
            },
            {
                title: 'Saraf',
                layout: 'form',
                items:
                        [
                            Saraf()
                        ]
            },
            {
                title: 'Pengindraan',
                layout: 'form',
                items:
                        [
                            Pengindraan()
                        ]
            },
            {
                title: 'Perkemihan',
                layout: 'form',
                items:
                        [
                            Perkemihan()
                        ]
            },
            {
                title: 'Pencernaan',
                layout: 'form',
                items:
                        [
                            Pencernaan()
                        ]
            },
            {
                title: 'Endokrin',
                layout: 'form',
                items:
                        [
                            Endokrin()
                        ]
            },
            {
                title: 'P.Hygiene',
                layout: 'form',
                items:
                        [
                            PHygiene()
                        ]
            },
            {
                title: 'Psy.Sos.',
                layout: 'form',
                items:
                        [
                            PsySos()
                        ]
            },
            {
                title: 'Penunjang',
                layout: 'form',
                items:
                        [
                            Penunjang()
                        ]
            }
        ]
    };
    return items;
}
;
function riwayatpenyakit()
{
    var tabDetailriwayatpenyakit = new Ext.Panel
            (
                    {
                        title: 'Riwayat Penyakit',
                        id: 'tabDetailriwayatpenyakit',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        bodyStyle: 'padding:5px 5px 5px 5px',
                        border: true,
                        autoscroll: true,
                        items: [
                            {
                                xtype: 'textarea',
                                fieldLabel: 'Keluhan Utama',
                                name: 'txtkeluhanutama',
                                id: 'txtkeluhanutama',
                                height: 50,
                                width: 855,
                                enableKeyEvents: true,
                                listeners:
                                        {
                                            'render': function (c) {
                                                c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                                        Ext.getCmp('txtriwayatpenyakitsekarang').focus();
                                                }, c);
                                                c.getEl().on('change', function (e) {
                                                    Ext.getCmp('btnSimpan_viDaftar').enable();
                                                    tmpediting = 'true';
                                                    RIWAYAT_UTAMA = Ext.get('txtkeluhanutama').getValue();
                                                }, c);
                                            }
                                        }
                            },
                            {
                                xtype: 'textarea',
                                fieldLabel: 'Riwayat Penyakit Sekarang',
                                name: 'txtriwayatpenyakitsekarang',
                                id: 'txtriwayatpenyakitsekarang',
                                height: 50,
                                width: 855,
                                enableKeyEvents: true,
                                listeners:
                                        {
                                            'render': function (c) {
                                                c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                                        Ext.getCmp('txtriwayatpenyakitTerdahulu').focus();
                                                }, c);
                                                c.getEl().on('change', function (e) {
                                                    Ext.getCmp('btnSimpan_viDaftar').enable();
                                                    tmpediting = 'true';
                                                    RIWAYAT_PENYAKIT_SEKARANG = Ext.get('txtriwayatpenyakitsekarang').getValue();
                                                }, c);
                                            }
                                        }

                            },
                            {
                                xtype: 'textarea',
                                fieldLabel: 'Riwayat Penyakit Terdahulu',
                                name: 'txtriwayatpenyakitTerdahulu',
                                id: 'txtriwayatpenyakitTerdahulu',
                                height: 224,
                                width: 855,
                                listeners:
                                        {
                                            'render': function (c) {
                                                c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                                        console.log(MainTabs);
                                                }, c);
                                                c.getEl().on('change', function (e) {
                                                    Ext.getCmp('btnSimpan_viDaftar').enable();
                                                    tmpediting = 'true';
                                                    RIWAYAT_PENYAKIT_DAHULU = Ext.get('txtriwayatpenyakitTerdahulu').getValue();
                                                }, c);
                                            }
                                        }
                            }
                        ]
                    }
            );
    return tabDetailriwayatpenyakit;
}
;
function Airway()
{
    var tabAirway = new Ext.Panel
            (
                    {
                        title: "Airways'n Breathing",
                        id: 'tabAirway',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        bodyStyle: 'padding:5px 5px 5px 5px',
                        border: true,
                        autoscroll: true,
                        items: [
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Jalan Napas '
                                            },
                                            {
                                                x: 125,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 135,
                                                y: 10,
                                                xtype: 'checkboxgroup',
                                                id: 'cgjelannapas',
                                                itemCls: 'x-check-group-alt',
                                                columns: 2,
                                                boxMaxWidth: 150,
                                                items: [
                                                    {xtype: 'checkbox', boxLabel: 'Paten', name: 'cbjlpaten', id: 'cbjlpaten'},
                                                    {xtype: 'checkbox', boxLabel: 'Obtruksif', name: 'cbjlobtruksif', id: 'cbjlobtruksif'}
                                                ],
                                                listeners: {
                                                    change: function (checkbox, newValue, oldValue, eOpts) {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('cbjlpaten').checked === true)
                                                        {
                                                            NAFAS_PATEN = 'true'
                                                        } else
                                                        {
                                                            NAFAS_PATEN = 'false'
                                                        }
                                                        if (Ext.getCmp('cbjlobtruksif').checked === true)
                                                        {
                                                            NAFAS_OBSTRUKTIF = 'true'
                                                        } else
                                                        {
                                                            NAFAS_OBSTRUKTIF = 'false'
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Jelas '
                                            },
                                            {
                                                x: 125,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 135,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgjelas',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: 'Ya',
                                                        name: 'rbjelasya',
                                                        inputValue: 1,
                                                        id: 'rbjelasya'
                                                    },
                                                    {
                                                        boxLabel: 'Tidak',
                                                        name: 'rbjelasya',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rbjelastidak'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rbjelasya').checked === true)
                                                        {
                                                            NAFAS_JELAS = 'true';
                                                        } else {
                                                            NAFAS_JELAS = 'false';
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                x: 300,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Pola Napas '
                                            },
                                            {
                                                x: 360,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 370,
                                                y: 10,
                                                xtype: 'checkboxgroup',
                                                id: 'cgpolanapas',
                                                columns: 2,
                                                boxMaxWidth: 150,
                                                vertical: true,
                                                items: [
                                                    {xtype: 'checkbox', boxLabel: 'Simetri', name: 'cbjlSimetri', id: 'cbjlSimetri'},
                                                    {xtype: 'checkbox', boxLabel: 'Asimetri', name: 'cbjlAsimetri', id: 'cbjlAsimetri'}
                                                ],
                                                listeners: {
                                                    change: function (checkbox, newValue, oldValue, eOpts) {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('cbjlSimetri').checked === true)
                                                        {
                                                            NAFAS_POLA_SIMETRI = 'true'
                                                        } else
                                                        {
                                                            NAFAS_POLA_SIMETRI = 'false'
                                                        }
                                                        if (Ext.getCmp('cbjlAsimetri').checked === true)
                                                        {
                                                            NAFAS_POLA_ASIMETRI = 'true'
                                                        } else
                                                        {
                                                            NAFAS_POLA_ASIMETRI = 'false'
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Suara Lapang Paru '
                                            },
                                            {
                                                x: 125,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 135,
                                                y: 10,
                                                xtype: 'checkboxgroup',
                                                id: 'cgsuaralapangparu',
                                                columns: 3,
                                                boxMaxWidth: 300,
                                                vertical: true,
                                                items: [
                                                    {xtype: 'checkbox', boxLabel: 'Normal/Sonor', name: 'cbjlNormal', id: 'cbjlNormal'},
                                                    {xtype: 'checkbox', boxLabel: 'HyperSonor', name: 'cbjlHyper', id: 'cbjlHyper'},
                                                    {xtype: 'checkbox', boxLabel: 'Menurun', name: 'cbjlMenurun', id: 'cbjlMenurun'}
                                                ],
                                                listeners: {
                                                    change: function (checkbox, newValue, oldValue, eOpts) {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('cbjlNormal').checked === true)
                                                        {
                                                            NAFAS_SUARA_NORMAL = 'true'
                                                        } else
                                                        {
                                                            NAFAS_SUARA_NORMAL = 'false'
                                                        }
                                                        if (Ext.getCmp('cbjlHyper').checked === true)
                                                        {
                                                            NAFAS_SUARA_HIPERSONOR = 'true'
                                                        } else
                                                        {
                                                            NAFAS_SUARA_HIPERSONOR = 'false'
                                                        }
                                                        if (Ext.getCmp('cbjlMenurun').checked === true)
                                                        {
                                                            NAFAS_MENURUN = 'true'
                                                        } else
                                                        {
                                                            NAFAS_MENURUN = 'false'
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 60,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Jenis '
                                            },
                                            {
                                                x: 125,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 135,
                                                y: 10,
                                                xtype: 'checkboxgroup',
                                                id: 'cgjenis',
                                                columns: 4,
                                                boxMaxWidth: 600,
                                                vertical: true,
                                                items: [
                                                    {xtype: 'checkbox', boxLabel: 'Normal', name: 'cbjlJenisNormal', id: 'cbjlJenisNormal'},
                                                    {xtype: 'checkbox', boxLabel: 'Tachypnoe', name: 'cbjlJenisTachypnoe', id: 'cbjlJenisTachypnoe'},
                                                    {xtype: 'checkbox', boxLabel: 'Cheyne Stokes', name: 'cbjlJenisCheyneStokes', id: 'cbjlJenisCheyneStokes'},
                                                    {xtype: 'checkbox', boxLabel: 'Retractive', name: 'cbjlJenisRetractive', id: 'cbjlJenisRetractive'},
                                                    {xtype: 'checkbox', boxLabel: 'Kusmaul', name: 'cbjlJenisKusmaul', id: 'cbjlJenisKusmaul'},
                                                    {xtype: 'checkbox', boxLabel: 'Dispone', name: 'cbjlJenisDispone', id: 'cbjlJenisDispone'}
                                                ],
                                                listeners: {
                                                    change: function (checkbox, newValue, oldValue, eOpts) {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('cbjlJenisNormal').checked === true)
                                                        {
                                                            NAFAS_JENIS_NORMAL = 'true'
                                                        } else
                                                        {
                                                            NAFAS_JENIS_NORMAL = 'false'
                                                        }
                                                        if (Ext.getCmp('cbjlJenisTachypnoe').checked === true)
                                                        {
                                                            NAFAS_JENIS_TACHYPNOE = 'true'
                                                        } else
                                                        {
                                                            NAFAS_JENIS_TACHYPNOE = 'false'
                                                        }
                                                        if (Ext.getCmp('cbjlJenisCheyneStokes').checked === true)
                                                        {
                                                            NAFAS_JENIS_CHEYNESTOKES = 'true'
                                                        } else
                                                        {
                                                            NAFAS_JENIS_CHEYNESTOKES = 'false'
                                                        }
                                                        if (Ext.getCmp('cbjlJenisRetractive').checked === true)
                                                        {
                                                            NAFAS_JENIS_RETRACTIVE = 'true'
                                                        } else
                                                        {
                                                            NAFAS_JENIS_RETRACTIVE = 'false'
                                                        }
                                                        if (Ext.getCmp('cbjlJenisKusmaul').checked === true)
                                                        {
                                                            NAFAS_JENIS_KUSMAUL = 'true'
                                                        } else
                                                        {
                                                            NAFAS_JENIS_KUSMAUL = 'false'
                                                        }
                                                        if (Ext.getCmp('cbjlJenisDispone').checked === true)
                                                        {
                                                            NAFAS_JENIS_DISPNOE = 'true'
                                                        } else
                                                        {
                                                            NAFAS_JENIS_DISPNOE = 'false'
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                x: 520,
                                                y: 35,
                                                xtype: 'label',
                                                text: 'RR(x/mnt) '
                                            },
                                            {
                                                x: 580,
                                                y: 30,
                                                xtype: 'textfield',
                                                id: 'txtJenisRR',
                                                width: 100,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        NAFAS_RR = Ext.get('txtJenisRR').getValue();
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Suara Nafas Tambahan '
                                            },
                                            {
                                                x: 125,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 135,
                                                y: 10,
                                                xtype: 'checkboxgroup',
                                                id: 'cgsuaranafastambahan',
                                                columns: 3,
                                                boxMaxWidth: 300,
                                                vertical: true,
                                                items: [
                                                    {xtype: 'checkbox', boxLabel: 'Wheezing', name: 'cbjlsntWheezing', id: 'cbjlsntWheezing'},
                                                    {xtype: 'checkbox', boxLabel: 'Ronchi', name: 'cbjlsntRonchi', id: 'cbjlsntRonchi'},
                                                    {xtype: 'checkbox', boxLabel: 'Rales', name: 'cbjlsntRales', id: 'cbjlsntRales'}
                                                ],
                                                listeners: {
                                                    change: function (checkbox, newValue, oldValue, eOpts) {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('cbjlsntWheezing').checked === true)
                                                        {
                                                            NAFAS_SUARA_WHEEZING = 'true'
                                                        } else
                                                        {
                                                            NAFAS_SUARA_WHEEZING = 'false'
                                                        }
                                                        if (Ext.getCmp('cbjlsntRonchi').checked === true)
                                                        {
                                                            NAFAS_SUARA_RONCHI = 'true'
                                                        } else
                                                        {
                                                            NAFAS_SUARA_RONCHI = 'false'
                                                        }
                                                        if (Ext.getCmp('cbjlsntRales').checked === true)
                                                        {
                                                            NAFAS_SUARA_RALES = 'true'
                                                        } else
                                                        {
                                                            NAFAS_SUARA_RALES = 'false'
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Efakuasi Cairan '
                                            },
                                            {
                                                x: 125,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 135,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgefakuasi',
                                                boxMaxWidth: 200,
                                                items: [
                                                    {boxLabel: 'Ya', name: 'rbefakuasi', inputValue: 1, id: 'rbefakuasiya'},
                                                    {boxLabel: 'Tidak', name: 'rbefakuasi', inputValue: 2, checked: true, id: 'rbefakuasitidak'}
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        if (Ext.getCmp('rbefakuasiya').checked === true)
                                                        {
                                                            NAFAS_EVAKUASI_CAIRAN = 'true';
                                                        } else {
                                                            NAFAS_EVAKUASI_CAIRAN = 'false';
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                x: 335,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Jumlah(cc) '
                                            },
                                            {
                                                x: 390,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 400,
                                                y: 5,
                                                xtype: 'textfield',
                                                id: 'txtjumlahcairan',
                                                width: 100,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        NAFAS_JML_CAIRAN = Ext.get('txtjumlahcairan').getValue();
                                                    }
                                                }
                                            },
                                            {
                                                x: 520,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Warna '
                                            },
                                            {
                                                x: 570,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 580,
                                                y: 5,
                                                xtype: 'textfield',
                                                id: 'txtwarnacairan',
                                                width: 100,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        NAFAS_WARNA_CAIRAN = Ext.get('txtwarnacairan').getValue();
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 124,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Masalah '
                                            },
                                            {
                                                x: 125,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 135,
                                                y: 10,
                                                xtype: 'textarea',
                                                name: 'txtmasalahairway',
                                                id: 'txtmasalahairway',
                                                height: 111,
                                                width: 822,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        NAFAS_MASALAH = Ext.get('txtmasalahairway').getValue();
                                                    }
                                                }
                                            }
                                        ]
                            }
                        ]
                    }
            );
    return tabAirway;
}
;
function Circulation()
{
    var tabCirculation = new Ext.Panel
            (
                    {
                        title: "Circulation",
                        id: 'tabCirculation',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        bodyStyle: 'padding:5px 5px 5px 5px',
                        border: true,
                        autoscroll: true,
                        items: [
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Irama Jantung '
                                            },
                                            {
                                                x: 125,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 135,
                                                y: 10,
                                                xtype: 'checkboxgroup',
                                                id: 'cgIramaJantung',
                                                columns: 2,
                                                boxMaxWidth: 150,
                                                vertical: true,
                                                items: [
                                                    {boxLabel: 'Reguler', name: 'cgIRReguler', id: 'cgIRReguler'},
                                                    {boxLabel: 'Irreguler', name: 'cgIRIrreguler', id: 'cgIRIrreguler'}
                                                ],
                                                listeners: {
                                                    change: function (checkbox, newValue, oldValue, eOpts) {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('cgIRReguler').checked === true)
                                                        {
                                                            JANTUNG_REGULER = 'true';
                                                        } else
                                                        {
                                                            JANTUNG_REGULER = 'false';
                                                        }
                                                        if (Ext.getCmp('cgIRIrreguler').checked === true)
                                                        {
                                                            JANTUNG_IRREGULER = 'true';
                                                        } else
                                                        {
                                                            JANTUNG_IRREGULER = 'false';
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                x: 300,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'S1/S2 Tunggal '
                                            },
                                            {
                                                x: 380,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 400,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgS1S2Tunggal',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {boxLabel: 'Ya', name: 'rgS1S2Tunggal', inputValue: 1, id: 'rbS1S2Tunggalya'},
                                                    {boxLabel: 'Tidak', name: 'rgS1S2Tunggal', inputValue: 2, checked: true, id: 'rbS1S2Tunggaltidak'}
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rbS1S2Tunggalya').checked === true)
                                                        {
                                                            JANTUNG_S1S2_TUNGGAL = 'true';
                                                        } else {
                                                            JANTUNG_S1S2_TUNGGAL = 'false';
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Nyeri Dada '
                                            },
                                            {
                                                x: 125,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 135,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgNyeridada',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: 'Ya',
                                                        name: 'rgNyeridada',
                                                        inputValue: 1,
                                                        id: 'rgNyeridadaya'
                                                    },
                                                    {
                                                        boxLabel: 'Tidak',
                                                        name: 'rgNyeridada',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rgNyeridadatidak'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rgNyeridadaya').checked === true)
                                                        {
                                                            JANTUNG_NYERI_DADA = 'true';
                                                        } else {
                                                            JANTUNG_NYERI_DADA = 'false';
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                x: 300,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Jelaskan '
                                            },
                                            {
                                                x: 380,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 400,
                                                y: 5,
                                                xtype: 'textfield',
                                                name: 'txtpenjelasannyeridada',
                                                id: 'txtpenjelasannyeridada',
                                                width: 560,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        JANTUNG_NYERI_DADA_KET = Ext.get('txtpenjelasannyeridada').getValue();
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Bunyi Jantung Tambahan '
                                            },
                                            {
                                                x: 125,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 135,
                                                y: 10,
                                                xtype: 'checkboxgroup',
                                                id: 'cgBunyiJantungTambahan',
                                                columns: 3,
                                                boxMaxWidth: 225,
                                                vertical: true,
                                                items: [
                                                    {boxLabel: 'Murmur', name: 'cgBunyiJantungTambahanMurmur', id: 'cgBunyiJantungTambahanMurmur'},
                                                    {boxLabel: 'Gallop', name: 'cgBunyiJantungTambahanGallop', id: 'cgBunyiJantungTambahanGallop'},
                                                    {boxLabel: 'Bising Sitolik', name: 'cgBunyiJantungTambahanBising', id: 'cgBunyiJantungTambahanBising'}
                                                ],
                                                listeners: {
                                                    change: function (checkbox, newValue, oldValue, eOpts) {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('cgBunyiJantungTambahanMurmur').checked === true)
                                                        {
                                                            JANTUNG_BUNYI_MURMUR = 'true'
                                                        } else
                                                        {
                                                            JANTUNG_BUNYI_MURMUR = 'false'
                                                        }
                                                        if (Ext.getCmp('cgBunyiJantungTambahanGallop').checked === true)
                                                        {
                                                            JANTUNG_BUNYI_GALLOP = 'true'
                                                        } else
                                                        {
                                                            JANTUNG_BUNYI_GALLOP = 'false'
                                                        }
                                                        if (Ext.getCmp('cgBunyiJantungTambahanBising').checked === true)
                                                        {
                                                            JANTUNG_BUNYI_BISING = 'true'
                                                        } else
                                                        {
                                                            JANTUNG_BUNYI_BISING = 'false'
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'CRT '
                                            },
                                            {
                                                x: 125,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 135,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgCRT',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: '<3 Dtk',
                                                        name: 'rgCRT',
                                                        inputValue: 1,
                                                        id: 'rgCRTkurang'
                                                    },
                                                    {
                                                        boxLabel: '>3 Dtk',
                                                        name: 'rgCRT',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rgCRTlebih'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rgCRTkurang').checked === true)
                                                        {
                                                            JANTUNG_CRT = 'true';
                                                        } else {
                                                            JANTUNG_CRT = 'false';
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                x: 300,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Akral '
                                            },
                                            {
                                                x: 380,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 400,
                                                y: 10,
                                                xtype: 'checkboxgroup',
                                                id: 'cgAkral',
                                                columns: 3,
                                                boxMaxWidth: 225,
                                                vertical: true,
                                                items: [
                                                    {boxLabel: 'Hangat', name: 'cgAkralHangat', id: 'cgAkralHangat'},
                                                    {boxLabel: 'Dingin', name: 'cgAkralDingin', id: 'cgAkralDingin'}
                                                ],
                                                listeners: {
                                                    change: function (checkbox, newValue, oldValue, eOpts) {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('cgAkralHangat').checked === true)
                                                        {
                                                            JANTUNG_AKRAL_HANGAT = 'true'
                                                        } else
                                                        {
                                                            JANTUNG_AKRAL_HANGAT = 'false'
                                                        }
                                                        if (Ext.getCmp('cgAkralDingin').checked === true)
                                                        {
                                                            JANTUNG_AKRAL_DINGIN = 'true'
                                                        } else
                                                        {
                                                            JANTUNG_AKRAL_DINGIN = 'false'
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Peningkatan JVP '
                                            },
                                            {
                                                x: 125,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 135,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgJVP',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: 'Ya',
                                                        name: 'rgJVP',
                                                        inputValue: 1,
                                                        id: 'rgJVPya'
                                                    },
                                                    {
                                                        boxLabel: 'Tidak',
                                                        name: 'rgJVP',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rgJVPtidak'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rgJVPya').checked === true)
                                                        {
                                                            JANTUNG_PENINGKATAN_JVP = 'true';
                                                        } else {
                                                            JANTUNG_PENINGKATAN_JVP = 'false';
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                x: 300,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Ukuran '
                                            },
                                            {
                                                x: 380,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 400,
                                                y: 5,
                                                xtype: 'textfield',
                                                id: 'txtukuranJVP',
                                                Width: 150,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        JANTUNG_UKURAN_CVP = Ext.get('txtukuranJVP').getValue();
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Warna Kulit '
                                            },
                                            {
                                                x: 125,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 135,
                                                y: 10,
                                                xtype: 'checkboxgroup',
                                                id: 'cgWarnaKulit',
                                                columns: 4,
                                                boxMaxWidth: 325,
                                                vertical: true,
                                                items: [
                                                    {boxLabel: 'Jaudice', name: 'cgWarnaKulitJaudice', id: 'cgWarnaKulitJaudice'},
                                                    {boxLabel: 'Sianotik', name: 'cgWarnaKulitSianotik', id: 'cgWarnaKulitSianotik'},
                                                    {boxLabel: 'Kemerahan', name: 'cgWarnaKulitKemerahan', id: 'cgWarnaKulitKemerahan'},
                                                    {boxLabel: 'Pucat', name: 'cgWarnaKulitPucat', id: 'cgWarnaKulitPucat'}
                                                ],
                                                listeners: {
                                                    change: function (checkbox, newValue, oldValue, eOpts) {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('cgWarnaKulitJaudice').checked === true)
                                                        {
                                                            JANTUNG_WARNA_JAUNDICE = 'true';
                                                        } else
                                                        {
                                                            JANTUNG_WARNA_JAUNDICE = 'false';
                                                        }
                                                        if (Ext.getCmp('cgWarnaKulitSianotik').checked === true)
                                                        {
                                                            JANTUNG_WARNA_SIANOTIK = 'true';
                                                        } else
                                                        {
                                                            JANTUNG_WARNA_SIANOTIK = 'false';
                                                        }
                                                        if (Ext.getCmp('cgWarnaKulitKemerahan').checked === true)
                                                        {
                                                            JANTUNG_WARNA_KEMERAHAN = 'true';
                                                        } else
                                                        {
                                                            JANTUNG_WARNA_KEMERAHAN = 'false';
                                                        }
                                                        if (Ext.getCmp('cgWarnaKulitPucat').checked === true)
                                                        {
                                                            JANTUNG_WARNA_PUCAT = 'true';
                                                        } else
                                                        {
                                                            JANTUNG_WARNA_PUCAT = 'false';
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Tekanan Darah(mmHg) '
                                            },
                                            {
                                                x: 125,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 135,
                                                y: 5,
                                                xtype: 'textfield',
                                                name: 'txttekanandarah',
                                                id: 'txttekanandarah',
                                                width: 72,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        JANTUNG_TEKANAN_DARAH = Ext.get('txttekanandarah').getValue();
                                                    }
                                                }
                                            },
                                            {
                                                x: 215,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Nadi '
                                            },
                                            {
                                                x: 250,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 270,
                                                y: 5,
                                                xtype: 'textfield',
                                                id: 'txtNadi',
                                                name: 'txtNadi',
                                                width: 72,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        JANTUNG_NADI = Ext.get('txtNadi').getValue();
                                                    }
                                                }
                                            },
                                            {
                                                x: 350,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Temp(CelSius) '
                                            },
                                            {
                                                x: 430,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 450,
                                                y: 5,
                                                xtype: 'textfield',
                                                name: 'txtTemp',
                                                id: 'txtTemp',
                                                width: 72,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        JANTUNG_TEMP = Ext.get('txtTemp').getValue();
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 124,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Masalah '
                                            },
                                            {
                                                x: 125,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 135,
                                                y: 10,
                                                xtype: 'textarea',
                                                name: 'txtmasalahCirculation',
                                                id: 'txtmasalahCirculation',
                                                height: 111,
                                                width: 822,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        JANTUNG_MASALAH = Ext.get('txtmasalahCirculation').getValue();
                                                    }
                                                }
                                            }
                                        ]
                            }
                        ]
                    }
            );
    return tabCirculation;
}
;
function Musculo()
{
    var tabMusculo = new Ext.Panel
            (
                    {
                        title: "Musculo",
                        id: 'tabMusculo',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        bodyStyle: 'padding:5px 5px 5px 5px',
                        border: true,
                        autoscroll: true,
                        items: [
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Kemampuan Pergerakan Sendi '
                                            },
                                            {
                                                x: 160,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 180,
                                                y: 10,
                                                xtype: 'checkboxgroup',
                                                id: 'cgPergerakanSendi',
                                                columns: 2,
                                                boxMaxWidth: 150,
                                                vertical: true,
                                                items: [
                                                    {boxLabel: 'Bebas', name: 'cgPergerakanSendiBebas', id: 'cgPergerakanSendiBebas'},
                                                    {boxLabel: 'Terbatas', name: 'cgPergerakanSendiTerbatas', id: 'cgPergerakanSendiTerbatas'}
                                                ],
                                                listeners: {
                                                    change: function (checkbox, newValue, oldValue, eOpts) {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('cgPergerakanSendiBebas').checked === true)
                                                        {
                                                            OTOT_SENDI_BEBAS = 'true';
                                                        } else
                                                        {
                                                            OTOT_SENDI_BEBAS = 'false';
                                                        }
                                                        if (Ext.getCmp('cgPergerakanSendiTerbatas').checked === true)
                                                        {
                                                            OTOT_SENDI_TERBATAS = 'true';
                                                        } else
                                                        {
                                                            OTOT_SENDI_TERBATAS = 'false';
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Kekuatan Otot '
                                            },
                                            {
                                                x: 160,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 180,
                                                y: 5,
                                                xtype: 'textfield',
                                                id: 'txtkekuatanotot',
                                                width: 780,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        OTOT_KEKUATAN = Ext.get('txtkekuatanotot').getValue();
                                                    }
                                                }
                                            }]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Odema ekstrimitas '
                                            },
                                            {
                                                x: 160,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 180,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgekstrimitas',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: 'Ya',
                                                        name: 'rgekstrimitas',
                                                        inputValue: 1,
                                                        id: 'rgekstrimitasya'
                                                    },
                                                    {
                                                        boxLabel: 'Tidak',
                                                        name: 'rgekstrimitas',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rgekstrimitastidak'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rgekstrimitasya').checked === true)
                                                        {
                                                            OTOT_ODEMA_EKSTRIMITAS = 'true';
                                                        } else {
                                                            OTOT_ODEMA_EKSTRIMITAS = 'false';
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Kelainan Bentuk '
                                            },
                                            {
                                                x: 160,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 180,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgKelainan',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: 'Ya',
                                                        name: 'rgKelainan',
                                                        inputValue: 1,
                                                        id: 'rgKelainanya'
                                                    },
                                                    {
                                                        boxLabel: 'Tidak',
                                                        name: 'rgKelainan',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rgKelainantidak'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rgKelainanya').checked === true)
                                                        {
                                                            OTOT_KELAINAN_BENTUK = 'true';
                                                        } else {
                                                            OTOT_KELAINAN_BENTUK = 'false';
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                x: 320,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Krepitas '
                                            },
                                            {
                                                x: 380,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 400,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgKrepitas',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: 'Ya',
                                                        name: 'rgKrepitas',
                                                        inputValue: 1,
                                                        id: 'rgKrepitasya'
                                                    },
                                                    {
                                                        boxLabel: 'Tidak',
                                                        name: 'rgKrepitas',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rgKrepitastidak'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rgKrepitasya').checked === true)
                                                        {
                                                            OTOT_KREPITASI = 'true';
                                                        } else {
                                                            OTOT_KREPITASI = 'false';
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 216,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Masalah '
                                            },
                                            {
                                                x: 160,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 180,
                                                y: 10,
                                                xtype: 'textarea',
                                                name: 'txtmasalahMusculo',
                                                id: 'txtmasalahMusculo',
                                                height: 206,
                                                width: 780,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        OTOT_MASALAH = Ext.get('txtmasalahMusculo').getValue();
                                                    }
                                                }
                                            }]
                            },
                        ]
                    }
            );
    return tabMusculo;
}
;
function Saraf()
{
    var tabSaraf = new Ext.Panel
            ({title: "Saraf",
                id: 'tabSaraf',
                fileUpload: true,
                region: 'north',
                layout: 'form',
                bodyStyle: 'padding:5px 5px 5px 5px',
                border: true,
                autoscroll: true,
                items: [
                    {
                        columnWidth: .20,
                        layout: 'absolute',
                        labelWidth: 100,
                        border: false,
                        height: 30,
                        items:
                                [
                                    {
                                        x: 10,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'GCS '
                                    },
                                    {
                                        x: 125,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 135,
                                        y: 10,
                                        xtype: 'checkboxgroup',
                                        id: 'cgGCS',
                                        columns: 3,
                                        boxMaxWidth: 225,
                                        vertical: true,
                                        items: [
                                            {boxLabel: 'Eye', name: 'cgGCSEye', id: 'cgGCSEye'},
                                            {boxLabel: 'Verbal', name: 'cgGCSVerbal', id: 'cgGCSVerbal'},
                                            {boxLabel: 'Motorik', name: 'cgGCSMotorik', id: 'cgGCSMotorik'}
                                        ],
                                        listeners: {
                                            change: function (checkbox, newValue, oldValue, eOpts) {
                                                Ext.getCmp('btnSimpan_viDaftar').enable();
                                                tmpediting = 'true';
                                                if (Ext.getCmp('cgGCSEye').checked === true)
                                                {
                                                    SYARAF_GCS_EYE = 'true';
                                                } else
                                                {
                                                    SYARAF_GCS_EYE = 'false';
                                                }
                                                if (Ext.getCmp('cgGCSVerbal').checked === true)
                                                {
                                                    SYARAF_GCS_VERBAL = 'true';
                                                } else
                                                {
                                                    SYARAF_GCS_VERBAL = 'false';
                                                }
                                                if (Ext.getCmp('cgGCSMotorik').checked === true)
                                                {
                                                    SYARAF_GCS_MOTORIK = 'true';
                                                } else
                                                {
                                                    SYARAF_GCS_MOTORIK = 'false';
                                                }
                                            }
                                        }
                                    },
                                    {
                                        x: 400,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'Total '
                                    },
                                    {
                                        x: 430,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 450,
                                        y: 5,
                                        xtype: 'textfield',
                                        id: 'txttotalGCS',
                                        width: 100,
                                        listeners: {
                                            change: function () {
                                                Ext.getCmp('btnSimpan_viDaftar').enable();
                                                tmpediting = 'true';
                                                SYARAF_GCS_TOTAL = Ext.get('txttotalGCS').getValue();
                                            }
                                        }
                                    }]
                    },
                    {
                        columnWidth: .20,
                        layout: 'absolute',
                        labelWidth: 100,
                        border: false,
                        height: 30,
                        items:
                                [
                                    {
                                        x: 10,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'Refleksi Fisiologis '
                                    },
                                    {
                                        x: 125,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 135,
                                        y: 10,
                                        xtype: 'checkboxgroup',
                                        id: 'cgRefleksi',
                                        columns: 3,
                                        boxMaxWidth: 225,
                                        vertical: true,
                                        items: [
                                            {boxLabel: 'Brachialis', name: 'cgcgRefleksiBrachialis', id: 'cgcgRefleksiBrachialis'},
                                            {boxLabel: 'Patella', name: 'cgcgRefleksiPatella', id: 'cgcgRefleksiPatella'},
                                            {boxLabel: 'Achilla', name: 'cgcgRefleksiAchilla', id: 'cgcgRefleksiAchilla'}
                                        ],
                                        listeners: {
                                            change: function (checkbox, newValue, oldValue, eOpts) {
                                                Ext.getCmp('btnSimpan_viDaftar').enable();
                                                tmpediting = 'true';
                                                if (Ext.getCmp('cgcgRefleksiBrachialis').checked === true)
                                                {
                                                    SYARAF_FISIOLOGIS_BRACHIALIS = 'true';
                                                } else
                                                {
                                                    SYARAF_FISIOLOGIS_BRACHIALIS = 'false';
                                                }
                                                if (Ext.getCmp('cgcgRefleksiPatella').checked === true)
                                                {
                                                    SYARAF_FISIOLOGIS_PATELLA = 'true';
                                                } else
                                                {
                                                    SYARAF_FISIOLOGIS_PATELLA = 'false';
                                                }
                                                if (Ext.getCmp('cgcgRefleksiAchilla').checked === true)
                                                {
                                                    SYARAF_FISIOLOGIS_ACHILLES = 'true';
                                                } else
                                                {
                                                    SYARAF_FISIOLOGIS_ACHILLES = 'false';
                                                }
                                            }
                                        }
                                    }
                                ]
                    },
                    {
                        columnWidth: .20,
                        layout: 'absolute',
                        labelWidth: 100,
                        border: false,
                        height: 30,
                        items:
                                [
                                    {
                                        x: 10,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'Refleksi Patologis '
                                    },
                                    {
                                        x: 125,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 135,
                                        y: 10,
                                        xtype: 'checkboxgroup',
                                        id: 'cgPatologis',
                                        columns: 4,
                                        boxMaxWidth: 300,
                                        vertical: true,
                                        items: [
                                            {boxLabel: 'Choddoks', name: 'cgPatologisChoddoks', id: 'cgPatologisChoddoks'},
                                            {boxLabel: 'Babinski', name: 'cgPatologisBabinski', id: 'cgPatologisBabinski'},
                                            {boxLabel: 'Budzinzky', name: 'cgPatologisBudzinzky', id: 'cgPatologisBudzinzky'},
                                            {boxLabel: 'Kerning', name: 'cgPatologisKerning', id: 'cgPatologisKerning'}
                                        ],
                                        listeners: {
                                            change: function (checkbox, newValue, oldValue, eOpts) {
                                                Ext.getCmp('btnSimpan_viDaftar').enable();
                                                tmpediting = 'true';
                                                if (Ext.getCmp('cgPatologisChoddoks').checked === true)
                                                {
                                                    SYARAF_PATOLOGIS_CHODDOKS = 'true';
                                                } else
                                                {
                                                    SYARAF_PATOLOGIS_CHODDOKS = 'false';
                                                }
                                                if (Ext.getCmp('cgPatologisBabinski').checked === true)
                                                {
                                                    SYARAF_PATOLOGIS_BABINSKI = 'true';
                                                } else
                                                {
                                                    SYARAF_PATOLOGIS_BABINSKI = 'false';
                                                }
                                                if (Ext.getCmp('cgPatologisBudzinzky').checked === true)
                                                {
                                                    SYARAF_PATOLOGIS_BUDZINZKY = 'true';
                                                } else
                                                {
                                                    SYARAF_PATOLOGIS_BUDZINZKY = 'false';
                                                }
                                                if (Ext.getCmp('cgPatologisKerning').checked === true)
                                                {
                                                    SYARAF_PATOLOGIS_KERNIG = 'true';
                                                } else
                                                {
                                                    SYARAF_PATOLOGIS_KERNIG = 'false';
                                                }
                                            }
                                        }
                                    }
                                ]
                    },
                    {
                        columnWidth: .20,
                        layout: 'absolute',
                        labelWidth: 100,
                        border: false,
                        height: 246,
                        items:
                                [
                                    {
                                        x: 10,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'Masalah '
                                    },
                                    {
                                        x: 125,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 135,
                                        y: 10,
                                        xtype: 'textarea',
                                        name: 'txtmasalahSaraf',
                                        id: 'txtmasalahSaraf',
                                        height: 236,
                                        width: 825,
                                        listeners: {
                                            change: function () {
                                                Ext.getCmp('btnSimpan_viDaftar').enable();
                                                tmpediting = 'true';
                                                SYARAF_MASALAH = Ext.get('txtmasalahSaraf').getValue();
                                            }
                                        }
                                    }]
                    }
                ]
            }
            );
    return tabSaraf;
}
;
function Pengindraan()
{
    var tabPengindraan = new Ext.Panel
            (
                    {title: "Pengindraan",
                        id: 'tabPengindraan',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        bodyStyle: 'padding:5px 5px 5px 5px',
                        border: true,
                        autoscroll: true,
                        items: [
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Pupil '
                                            },
                                            {
                                                x: 130,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 140,
                                                y: 10,
                                                xtype: 'checkboxgroup',
                                                id: 'cgPupil',
                                                columns: 2,
                                                boxMaxWidth: 150,
                                                vertical: true,
                                                items: [
                                                    {boxLabel: 'Isokor', name: 'cgPupilIsokor', id: 'cgPupilIsokor'},
                                                    {boxLabel: 'Anisokor', name: 'cgPupilAnisokor', id: 'cgPupilAnisokor'}
                                                ],
                                                listeners: {
                                                    change: function (checkbox, newValue, oldValue, eOpts) {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('cgPupilIsokor').checked === true)
                                                        {
                                                            INDRA_PUPIL_ISOKOR = 'true';
                                                        } else
                                                        {
                                                            INDRA_PUPIL_ISOKOR = 'false';
                                                        }
                                                        if (Ext.getCmp('cgPupilAnisokor').checked === true)
                                                        {
                                                            INDRA__PUPIL_ANISOKOR = 'true';
                                                        } else
                                                        {
                                                            INDRA__PUPIL_ANISOKOR = 'false';
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                x: 300,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Konjungtiva/Skalar '
                                            },
                                            {
                                                x: 400,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 420,
                                                y: 10,
                                                xtype: 'checkboxgroup',
                                                id: 'cgKonjungtiva',
                                                columns: 3,
                                                boxMaxWidth: 225,
                                                vertical: true,
                                                items: [
                                                    {boxLabel: 'Anemis', name: 'cgKonjungtivaAnemis', id: 'cgKonjungtivaAnemis'},
                                                    {boxLabel: 'Icterus', name: 'cgKonjungtivaIcterus', id: 'cgKonjungtivaIcterus'},
                                                    {boxLabel: 'Tidak', name: 'cgKonjungtivaTidak', id: 'cgKonjungtivaTidak'},
                                                ],
                                                listeners: {
                                                    change: function (checkbox, newValue, oldValue, eOpts) {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('cgKonjungtivaAnemis').checked === true)
                                                        {
                                                            INDRA__KONJUNGTIVA_ANEMIS = 'true';
                                                        } else
                                                        {
                                                            INDRA__KONJUNGTIVA_ANEMIS = 'false';
                                                        }
                                                        if (Ext.getCmp('cgKonjungtivaIcterus').checked === true)
                                                        {
                                                            INDRA__KONJUNGTIVA_ICTERUS = 'true';
                                                        } else
                                                        {
                                                            INDRA__KONJUNGTIVA_ICTERUS = 'false';
                                                        }
                                                        if (Ext.getCmp('cgKonjungtivaTidak').checked === true)
                                                        {
                                                            INDRA__KONJUNGTIVA_TIDAK = 'true';
                                                        } else
                                                        {
                                                            INDRA__KONJUNGTIVA_TIDAK = 'false';
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Gangguan Pendengaran '
                                            },
                                            {
                                                x: 130,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 140,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgGangguanPendengaran',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: 'Ya',
                                                        name: 'rgGangguanPendengaran',
                                                        inputValue: 1,
                                                        id: 'rgGangguanPendengaranya'
                                                    },
                                                    {
                                                        boxLabel: 'Tidak',
                                                        name: 'rgGangguanPendengaran',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rgGangguanPendengarantidak'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rgGangguanPendengaranya').checked === true)
                                                        {
                                                            INDRA_GANGGUAN_DENGAR = 'true';
                                                        } else {
                                                            INDRA_GANGGUAN_DENGAR = 'false';
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                x: 300,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Otorhea '
                                            },
                                            {
                                                x: 400,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 420,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgOtorhea',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: 'Ya',
                                                        name: 'rgOtorhea',
                                                        inputValue: 1,
                                                        id: 'rgOtorheaya'
                                                    },
                                                    {
                                                        boxLabel: 'Tidak',
                                                        name: 'rgOtorhea',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rgOtorheatidak'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rgGangguanPendengaranya').checked === true)
                                                        {
                                                            INDRA_OTORHEA = 'true';
                                                        } else {
                                                            INDRA_OTORHEA = 'false';
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Bentuk Hidung '
                                            },
                                            {
                                                x: 130,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 140,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgBentukHidung',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: 'Ya',
                                                        name: 'rgBentukHidung',
                                                        inputValue: 1,
                                                        id: 'rgBentukHidungya'
                                                    },
                                                    {
                                                        boxLabel: 'Tidak',
                                                        name: 'rgBentukHidung',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rgBentukHidungtidak'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rgBentukHidungya').checked === true)
                                                        {
                                                            INDRA_BENTUK_HIDUNG = 'true';
                                                        } else {
                                                            INDRA_BENTUK_HIDUNG = 'false';
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                x: 300,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Jelaskan '
                                            },
                                            {
                                                x: 400,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 420,
                                                y: 5,
                                                xtype: 'textfield',
                                                id: 'txtjelaskanpengindraan',
                                                width: 540,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        INDRA_BENTUK_HIDUNG_KET = Ext.get('txtjelaskanpengindraan').getValue();
                                                    }
                                                }
                                            }]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Gangguan Penciuman '
                                            },
                                            {
                                                x: 130,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 140,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgGangguanPenciuman',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: 'Ya',
                                                        name: 'rgGangguanPenciuman',
                                                        inputValue: 1,
                                                        id: 'rgGangguanPenciumanya'
                                                    },
                                                    {
                                                        boxLabel: 'Tidak',
                                                        name: 'rgGangguanPenciuman',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rgGangguanPenciumantidak'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rgGangguanPenciumanya').checked === true)
                                                        {
                                                            INDRA_GANGGUAN_CIUM = 'true';
                                                        } else {
                                                            INDRA_GANGGUAN_CIUM = 'false';
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                x: 300,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Rhinorhea '
                                            },
                                            {
                                                x: 400,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 420,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgRhinorhea',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: 'Ya',
                                                        name: 'rgRhinorhea',
                                                        inputValue: 1,
                                                        id: 'rgRhinorheaya'
                                                    },
                                                    {
                                                        boxLabel: 'Tidak',
                                                        name: 'rgRhinorhea',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rgRhinorheatidak'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rgRhinorheaya').checked === true)
                                                        {
                                                            INDRA_RHINORHEA = 'true';
                                                        } else {
                                                            INDRA_RHINORHEA = 'false';
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 216,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'masalah '
                                            },
                                            {
                                                x: 130,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 140,
                                                y: 5,
                                                xtype: 'textarea',
                                                id: 'trmasalahpengindraan',
                                                width: 820,
                                                height: 211,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        INDRA_MASALAH = Ext.get('trmasalahpengindraan').getValue();
                                                    }
                                                }
                                            }]
                            }
                        ]
                    }
            );
    return tabPengindraan;
}
;
function Perkemihan()
{
    var tabPerkemihan = new Ext.Panel
            (
                    {title: "Perkemihan",
                        id: 'tabPerkemihan',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        bodyStyle: 'padding:5px 5px 5px 5px',
                        border: true,
                        autoscroll: true,
                        items: [
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Kebersihan '
                                            },
                                            {
                                                x: 100,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 120,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgKebersihan',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: 'Bersih',
                                                        name: 'rgKebersihan',
                                                        inputValue: 1,
                                                        id: 'rgKebersihanBersih'
                                                    },
                                                    {
                                                        boxLabel: 'Kotor',
                                                        name: 'rgKebersihan',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rgKebersihanKotor'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rgKebersihanBersih').checked === true)
                                                        {
                                                            KEMIH_KEBERSIHAN = 'true';
                                                        } else {
                                                            KEMIH_KEBERSIHAN = 'false';
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                x: 310,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Alat Bantu Kateter '
                                            },
                                            {
                                                x: 400,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 420,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgAlatBantu',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: 'Ya',
                                                        name: 'rgAlatBantu',
                                                        inputValue: 1,
                                                        id: 'rgAlatBantuYa'
                                                    },
                                                    {
                                                        boxLabel: 'Tidak',
                                                        name: 'rgAlatBantu',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rgAlatBantuTidak'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rgAlatBantuYa').checked === true)
                                                        {
                                                            KEMIH_ALAT_BANTU = 'true';
                                                        } else {
                                                            KEMIH_ALAT_BANTU = 'false';
                                                        }
                                                    }
                                                }
                                            },
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Urin '
                                            },
                                            {
                                                x: 100,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 120,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Jumlah(cc) '
                                            },
                                            {
                                                x: 175,
                                                y: 5,
                                                xtype: 'textfield',
                                                id: 'txtjumlahurin',
                                                width: 100,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        KEMIH_JML_URINE = Ext.get('txtjumlahurin').getValue();
                                                    }
                                                }
                                            },
                                            {
                                                x: 310,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Warna '
                                            },
                                            {
                                                x: 400,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 420,
                                                y: 5,
                                                xtype: 'textfield',
                                                id: 'txtwarnaurin',
                                                width: 100,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        KEMIH_WARNA_URINE = Ext.get('txtwarnaurin').getValue();
                                                    }
                                                }
                                            },
                                            {
                                                x: 550,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Bau '
                                            },
                                            {
                                                x: 580,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 600,
                                                y: 5,
                                                xtype: 'textfield',
                                                id: 'txtbauurin',
                                                width: 100,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        KEMIH_BAU = Ext.get('txtbauurin').getValue();
                                                    }
                                                }
                                            }]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Kand. Kencing '
                                            },
                                            {
                                                x: 100,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 120,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Membesar '
                                            },
                                            {
                                                x: 175,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgMembesar',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: 'Ya',
                                                        name: 'rgMembesar',
                                                        inputValue: 1,
                                                        id: 'rgMembesarYa'
                                                    },
                                                    {
                                                        boxLabel: 'Tidak',
                                                        name: 'rgMembesar',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rgMembesarTidak'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rgMembesarYa').checked === true)
                                                        {
                                                            KEMIH_KANDUNG_MEMBESAR = 'true';
                                                        } else {
                                                            KEMIH_KANDUNG_MEMBESAR = 'false';
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                x: 310,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Nyeri Tekanan '
                                            },
                                            {
                                                x: 400,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 420,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgNyeri',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: 'Ya',
                                                        name: 'rgNyeri',
                                                        inputValue: 1,
                                                        id: 'rgNyeriYa'
                                                    },
                                                    {
                                                        boxLabel: 'Tidak',
                                                        name: 'rgNyeri',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rgNyeriTidak'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rgNyeriYa').checked === true)
                                                        {
                                                            KEMIH_NYERI_TEKAN = 'true';
                                                        } else {
                                                            KEMIH_NYERI_TEKAN = 'false';
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 60,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Ganguan '
                                            },
                                            {
                                                x: 100,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 120,
                                                y: 10,
                                                xtype: 'checkboxgroup',
                                                id: 'cgGanguankemih',
                                                columns: 3,
                                                boxMaxWidth: 300,
                                                vertical: true,
                                                items: [
                                                    {boxLabel: 'Anuir', name: 'cgGanguankemihAnuir', id: 'cgGanguankemihAnuir'},
                                                    {boxLabel: 'Oligura', name: 'cgGanguankemihOligura', id: 'cgGanguankemihOligura'},
                                                    {boxLabel: 'Retensi', name: 'cgGanguankemihRetensi', id: 'cgGanguankemihRetensi'},
                                                    {boxLabel: 'Inkontinesia', name: 'cgGanguankemihInkontinesia', id: 'cgGanguankemihInkontinesia'},
                                                    {boxLabel: 'Disuria', name: 'cgGanguankemihDisuria', id: 'cgGanguankemihDisuria'},
                                                    {boxLabel: 'Hematuri', name: 'cgGanguankemihHematuri', id: 'cgGanguankemihHematuri'}
                                                ],
                                                listeners: {
                                                    change: function (checkbox, newValue, oldValue, eOpts) {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('cgGanguankemihAnuir').checked === true)
                                                        {
                                                            KEMIH_GANGGUAN_ANURIA = 'true';
                                                        } else
                                                        {
                                                            KEMIH_GANGGUAN_ANURIA = 'false';
                                                        }
                                                        if (Ext.getCmp('cgGanguankemihOligura').checked === true)
                                                        {
                                                            KEMIH_GANGGUAN_ALIGURIA = 'true';
                                                        } else
                                                        {
                                                            KEMIH_GANGGUAN_ALIGURIA = 'false';
                                                        }
                                                        if (Ext.getCmp('cgGanguankemihRetensi').checked === true)
                                                        {
                                                            KEMIH_GANGGUAN_RETENSI = 'true';
                                                        } else
                                                        {
                                                            KEMIH_GANGGUAN_RETENSI = 'false';
                                                        }
                                                        if (Ext.getCmp('cgGanguankemihInkontinesia').checked === true)
                                                        {
                                                            KEMIH_GANGGUAN_INKONTINENSIA = 'true';
                                                        } else
                                                        {
                                                            KEMIH_GANGGUAN_INKONTINENSIA = 'false';
                                                        }

                                                        if (Ext.getCmp('cgGanguankemihDisuria').checked === true)
                                                        {
                                                            KEMIH_GANGGUAN_DISURIA = 'true';
                                                        } else
                                                        {
                                                            KEMIH_GANGGUAN_DISURIA = 'false';
                                                        }
                                                        if (Ext.getCmp('cgGanguankemihHematuri').checked === true)
                                                        {
                                                            KEMIH_GANGGUANHEMATURIA = 'true';
                                                        } else
                                                        {
                                                            KEMIH_GANGGUANHEMATURIA = 'false';
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 186,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Masalah '
                                            },
                                            {
                                                x: 100,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 120,
                                                y: 10,
                                                xtype: 'textarea',
                                                id: 'trmasalahperkemihan',
                                                width: 840,
                                                height: 176,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        KEMIH_MASALAH = Ext.get('trmasalahperkemihan').getValue();
                                                    }
                                                }
                                            }]
                            },
                        ]
                    }
            );
    return tabPerkemihan;
}
;
function Pencernaan()
{
    var tabPencernaan = new Ext.Panel
            (
                    {title: "Pencernaan",
                        id: 'tabPencernaan',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        bodyStyle: 'padding:5px 5px 5px 5px',
                        border: true,
                        autoscroll: true,
                        items: [
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Porsi Makan '
                                            },
                                            {
                                                x: 100,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 120,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgPorsiMakan',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: 'Habis',
                                                        name: 'rgPorsiMakan',
                                                        inputValue: 1,
                                                        id: 'rgPorsiMakanHabis'
                                                    },
                                                    {
                                                        boxLabel: 'Tidak',
                                                        name: 'rgPorsiMakan',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rgPorsiMakanTidak'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rgPorsiMakanHabis').checked === true)
                                                        {
                                                            CERNA_MAKAN_HABIS = 'true';
                                                        } else {
                                                            CERNA_MAKAN_HABIS = 'false';
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                x: 265,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Keterangan (Porsi) '
                                            },
                                            {
                                                x: 355,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 360,
                                                y: 5,
                                                xtype: 'textfield',
                                                id: 'txtporsi',
                                                width: 80,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        CERNA_MAKAN_KET = Ext.get('txtporsi').getValue();
                                                    }
                                                }
                                            }, {
                                                x: 460,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Frekuensi(x/hr) '
                                            },
                                            {
                                                x: 545,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 550,
                                                y: 5,
                                                xtype: 'textfield',
                                                id: 'txtFrekuensimakan',
                                                width: 60,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        CERNA_MAKAN_FREKUENSI = Ext.get('txtFrekuensimakan').getValue();
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Minum(cc/Hari) '
                                            },
                                            {
                                                x: 100,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 120,
                                                y: 5,
                                                xtype: 'textfield',
                                                id: 'txtMinum',
                                                width: 60,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        CERNA_JML_MINUM = Ext.get('txtMinum').getValue();
                                                    }
                                                }
                                            }, {
                                                x: 265,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Jenis '
                                            },
                                            {
                                                x: 355,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 360,
                                                y: 5,
                                                xtype: 'textfield',
                                                id: 'txtjenisMinuman',
                                                width: 80,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        CERNA_JENIS_MINUM = Ext.get('txtjenisMinuman').getValue();
                                                    }
                                                }
                                            }, {
                                                x: 460,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Paristaltik(x/mnt) '
                                            },
                                            {
                                                x: 545,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 550,
                                                y: 5,
                                                xtype: 'textfield',
                                                id: 'txtParistaltik',
                                                width: 80,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        CERNA_PERISTALTIK = Ext.get('txtParistaltik').getValue();
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Mulut '
                                            },
                                            {
                                                x: 100,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 120,
                                                y: 10,
                                                xtype: 'checkboxgroup',
                                                id: 'cgKondisiMulut',
                                                columns: 3,
                                                boxMaxWidth: 225,
                                                vertical: true,
                                                items: [
                                                    {boxLabel: 'Bersih', name: 'cgKondisiMulutBersih', id: 'cgKondisiMulutBersih'},
                                                    {boxLabel: 'Kotor', name: 'cgKondisiMulutKotor', id: 'cgKondisiMulutKotor'},
                                                    {boxLabel: 'Berbau', name: 'cgKondisiMulutBerbau', id: 'cgKondisiMulutBerbau'}
                                                ],
                                                listeners: {
                                                    change: function (checkbox, newValue, oldValue, eOpts) {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('cgKondisiMulutBersih').checked === true)
                                                        {
                                                            CERNA_MULUT_BERSIH = 'true';
                                                        } else
                                                        {
                                                            CERNA_MULUT_BERSIH = 'false';
                                                        }
                                                        if (Ext.getCmp('cgKondisiMulutKotor').checked === true)
                                                        {
                                                            CERNA_MULUT_KOTOR = 'true';
                                                        } else
                                                        {
                                                            CERNA_MULUT_KOTOR = 'false';
                                                        }
                                                        if (Ext.getCmp('cgKondisiMulutBerbau').checked === true)
                                                        {
                                                            CERNA_MULUT_BERBAU = 'true';
                                                        } else
                                                        {
                                                            CERNA_MULUT_BERBAU = 'false';
                                                        }
                                                    }
                                                }
                                            }

                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Mukosa '
                                            },
                                            {
                                                x: 100,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 120,
                                                y: 10,
                                                xtype: 'checkboxgroup',
                                                id: 'cgMukosa',
                                                columns: 3,
                                                boxMaxWidth: 225,
                                                vertical: true,
                                                items: [
                                                    {boxLabel: 'Lembab', name: 'cgMukosaLembab', id: 'cgMukosaLembab'},
                                                    {boxLabel: 'Kering', name: 'cgMukosaKering', id: 'cgMukosaKering'},
                                                    {boxLabel: 'Stomatitis', name: 'cgMukosaStomatitis', id: 'cgMukosaStomatitis'}
                                                ],
                                                listeners: {
                                                    change: function (checkbox, newValue, oldValue, eOpts) {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('cgMukosaLembab').checked === true)
                                                        {
                                                            CERNA_MUKOSA_LEMBAB = 'true';
                                                        } else
                                                        {
                                                            CERNA_MUKOSA_LEMBAB = 'false';
                                                        }
                                                        if (Ext.getCmp('cgMukosaKering').checked === true)
                                                        {
                                                            CERNA_MUKOSA_KERING = 'true';
                                                        } else
                                                        {
                                                            CERNA_MUKOSA_KERING = 'false';
                                                        }
                                                        if (Ext.getCmp('cgMukosaKering').checked === true)
                                                        {
                                                            CERNA_MUKOSA_STOMATITIS = 'true';
                                                        } else
                                                        {
                                                            CERNA_MUKOSA_STOMATITIS = 'false';
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                x: 355,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Turgor Kulit '
                                            },
                                            {
                                                x: 420,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 440,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgTurgor',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: 'Baik',
                                                        name: 'rgTurgor',
                                                        inputValue: 1,
                                                        id: 'rgTurgorBaik'
                                                    },
                                                    {
                                                        boxLabel: 'Jelek',
                                                        name: 'rgTurgor',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rgTurgorJelek'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rgTurgorBaik').checked === true)
                                                        {
                                                            CERNA_TURGOR_KULIT = 'true';
                                                        } else {
                                                            CERNA_TURGOR_KULIT = 'false';
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Tenggorokan '
                                            },
                                            {
                                                x: 100,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 120,
                                                y: 10,
                                                xtype: 'checkboxgroup',
                                                id: 'cgTenggorokan',
                                                columns: 3,
                                                boxMaxWidth: 300,
                                                vertical: true,
                                                items: [
                                                    {boxLabel: 'Sakit/Sulit Nelan', name: 'cgTenggorokanSakit', id: 'cgTenggorokanSakit'},
                                                    {boxLabel: 'Nyeri tekan', name: 'cgTenggorokanNyeri', id: 'cgTenggorokanNyeri'},
                                                    {boxLabel: 'Pembesaran Tonsil', name: 'cgTenggorokanTonsil', id: 'cgTenggorokanTonsil'}]
                                            }
                                        ],
                                listeners: {
                                    change: function (checkbox, newValue, oldValue, eOpts) {
                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                        tmpediting = 'true';
                                        if (Ext.getCmp('cgTenggorokanSakit').checked === true)
                                        {
                                            CERNA_TENGGOROKAN_SAKIT = 'true';
                                        } else
                                        {
                                            CERNA_TENGGOROKAN_SAKIT = 'false';
                                        }
                                        if (Ext.getCmp('cgTenggorokanNyeri').checked === true)
                                        {
                                            CERNA_TENGGOROKAN_NYERI_TEKAN = 'true';
                                        } else
                                        {
                                            CERNA_TENGGOROKAN_NYERI_TEKAN = 'false';
                                        }
                                        if (Ext.getCmp('cgTenggorokanTonsil').checked === true)
                                        {
                                            CERNA_TENGGOROKAN_PEMBESARANTO = 'true';
                                        } else
                                        {
                                            CERNA_TENGGOROKAN_PEMBESARANTO = 'false';
                                        }
                                    }
                                }
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Perut '
                                            },
                                            {
                                                x: 100,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 120,
                                                y: 10,
                                                xtype: 'checkboxgroup',
                                                id: 'cgPerut',
                                                columns: 3,
                                                boxMaxWidth: 300,
                                                vertical: true,
                                                items: [
                                                    {boxLabel: 'Normal', name: 'cgPerutNormal', id: 'cgPerutNormal'},
                                                    {boxLabel: 'Distended', name: 'cgPerutDistended', id: 'cgPerutDistended'},
                                                    {boxLabel: 'Meteorismus', name: 'cgPerutMeteorismus', id: 'cgPerutMeteorismus'}
                                                ],
                                                listeners: {
                                                    change: function (checkbox, newValue, oldValue, eOpts) {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('cgPerutNormal').checked === true)
                                                        {
                                                            CERNA_PERUT_NORMAL = 'true';
                                                        } else
                                                        {
                                                            CERNA_PERUT_NORMAL = 'false';
                                                        }
                                                        if (Ext.getCmp('cgPerutDistended').checked === true)
                                                        {
                                                            CERNA_PERUT_DISTENDED = 'true';
                                                        } else
                                                        {
                                                            CERNA_PERUT_DISTENDED = 'false';
                                                        }
                                                        if (Ext.getCmp('cgPerutMeteorismus').checked === true)
                                                        {
                                                            CERNA_PERUT_METEORISMUS = 'true';
                                                        } else
                                                        {
                                                            CERNA_PERUT_METEORISMUS = 'false';
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                x: 460,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Pembesaran Tonsil '
                                            },
                                            {
                                                x: 560,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 570,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgPembesaranTonsil',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: 'Ya',
                                                        name: 'rgPembesaranTonsil',
                                                        inputValue: 1,
                                                        id: 'rgPembesaranTonsilYa'
                                                    },
                                                    {
                                                        boxLabel: 'Tidak',
                                                        name: 'rgPembesaranTonsil',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rgPembesaranTonsilTidak'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rgPembesaranTonsilYa').checked === true)
                                                        {
                                                            CERNA_PEMBESARAN_HEPAR = 'true';
                                                        } else {
                                                            CERNA_PEMBESARAN_HEPAR = 'false';
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 120,
                                                y: 10,
                                                xtype: 'checkboxgroup',
                                                id: 'cgNyeriTekan',
                                                columns: 1,
                                                boxMaxWidth: 300,
                                                vertical: true,
                                                items: [
                                                    {boxLabel: 'Nyeri Tekan', name: 'cgNyerisaatTekan', id: 'cgNyerisaatTekan'}
                                                ],
                                                listeners: {
                                                    change: function (checkbox, newValue, oldValue, eOpts) {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('cgNyerisaatTekan').checked === true)
                                                        {
                                                            CERNA_PERUT_NYERI_TEKAN = 'true';
                                                        } else
                                                        {
                                                            CERNA_PERUT_NYERI_TEKAN = 'false';
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                x: 202,
                                                y: 10,
                                                xtype: 'label',
                                                text: ','
                                            },
                                            {
                                                x: 211,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Lokasi'
                                            },
                                            {
                                                x: 246,
                                                y: 5,
                                                xtype: 'textfield',
                                                width: 190,
                                                id: 'txtlokasisakit',
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        CERNA_PERUT_LOKASI_NYERI = Ext.get('txtlokasisakit').getValue();
                                                    }
                                                }
                                            },
                                            {
                                                x: 460,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Hematemesis '
                                            },
                                            {
                                                x: 560,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 570,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgHematemesis',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: 'Ya',
                                                        name: 'rgHematemesis',
                                                        inputValue: 1,
                                                        id: 'rgHematemesisYa'
                                                    },
                                                    {
                                                        boxLabel: 'Tidak',
                                                        name: 'rgHematemesis',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rgHematemesisTidak'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rgHematemesisYa').checked === true)
                                                        {
                                                            CERNA_HEMATEMESIS = 'true';
                                                        } else {
                                                            CERNA_HEMATEMESIS = 'false';
                                                        }
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Buang Air B.(x/Hr) '
                                            },
                                            {
                                                x: 100,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 120,
                                                y: 5,
                                                xtype: 'textfield',
                                                width: 100,
                                                id: 'txtjumlahBAB',
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        CERNA_FREK_BAB = Ext.get('txtjumlahBAB').getValue();
                                                    }
                                                }
                                            },
                                            {
                                                x: 230,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Konsintensi '
                                            },
                                            {
                                                x: 290,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 310,
                                                y: 5,
                                                xtype: 'textfield',
                                                width: 100,
                                                id: 'txtKonsintensiBAB',
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        CERNA_KONSISTENSI = Ext.get('txtKonsintensiBAB').getValue();
                                                    }
                                                }
                                            },
                                            {
                                                x: 430,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Warna '
                                            },
                                            {
                                                x: 470,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 490,
                                                y: 5,
                                                xtype: 'textfield',
                                                width: 100,
                                                id: 'txtWarnaBAB',
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        CERNA_WARNA_BAB = Ext.get('txtWarnaBAB').getValue();
                                                    }
                                                }
                                            },
                                            {
                                                x: 610,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Bau '
                                            },
                                            {
                                                x: 640,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 660,
                                                y: 5,
                                                xtype: 'textfield',
                                                width: 100,
                                                id: 'txtBauBAB',
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        CERNA_BAU_BAB = Ext.get('txtBauBAB').getValue();
                                                    }
                                                }
                                            },
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Evakuasi Cairan A '
                                            },
                                            {
                                                x: 100,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 120,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgEvakuasiCairan',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: 'Ya',
                                                        name: 'rgEvakuasiCairan',
                                                        inputValue: 1,
                                                        id: 'rgEvakuasiCairanYa'
                                                    },
                                                    {
                                                        boxLabel: 'Tidak',
                                                        name: 'rgEvakuasiCairan',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rgEvakuasiCairanTidak'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rgEvakuasiCairanYa').checked === true)
                                                        {
                                                            CERNA_EVAKUASI_CAIRAN_ASCITES = 'true';
                                                        } else {
                                                            CERNA_EVAKUASI_CAIRAN_ASCITES = 'false';
                                                        }
                                                    }
                                                }
                                            },
                                            {
                                                x: 260,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Jumlah(cc) '
                                            },
                                            {
                                                x: 320,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 330,
                                                y: 5,
                                                xtype: 'textfield',
                                                width: 100,
                                                id: 'txtJumlahcairan',
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        CERNA_JML_CAIRAN_ASCITES = Ext.get('txtJumlahcairan').getValue();
                                                    }
                                                }
                                            },
                                            {
                                                x: 450,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Warna '
                                            },
                                            {
                                                x: 490,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 500,
                                                y: 5,
                                                xtype: 'textfield',
                                                width: 100,
                                                id: 'txtWarnaCairan',
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        CERNA_WARNA_CAIRAN_ASCITES = Ext.get('txtWarnaCairan').getValue();
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 65,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Masalah '
                                            },
                                            {
                                                x: 100,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 120,
                                                y: 10,
                                                xtype: 'textarea',
                                                width: 840,
                                                height: 55,
                                                id: 'trmasalahpencernaan',
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        CERNA_MASALAH = Ext.get('trmasalahpencernaan').getValue();
                                                    }
                                                }
                                            }
                                        ]
                            },
                        ]
                    }
            );
    return tabPencernaan;
}
;
function Endokrin()
{
    var tabEndokrin = new Ext.Panel
            (
                    {title: "Endokrin",
                        id: 'tabEndokrin',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        bodyStyle: 'padding:5px 5px 5px 5px',
                        border: true,
                        autoscroll: true,
                        items: [
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Tyroid '
                                            },
                                            {
                                                x: 100,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 120,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgTyroid',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: 'Membesar',
                                                        name: 'rgTyroid',
                                                        inputValue: 1,
                                                        id: 'rgTyroidMembesar'
                                                    },
                                                    {
                                                        boxLabel: 'Tidak',
                                                        name: 'rgTyroid',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rgTyroidTidak'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rgTyroidMembesar').checked === true)
                                                        {
                                                            ENDOKRIN_TYROID = 'true';
                                                        } else {
                                                            ENDOKRIN_TYROID = 'false';
                                                        }
                                                        //console.log(ENDOKRIN_TYROID);
                                                    }
                                                }
                                            },
                                            {
                                                x: 260,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Hyper/Hypoglikernia '
                                            },
                                            {
                                                x: 360,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 370,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgHyper',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: 'Ya',
                                                        name: 'rgHyper',
                                                        inputValue: 1,
                                                        id: 'rgHyperYa'
                                                    },
                                                    {
                                                        boxLabel: 'Tidak',
                                                        name: 'rgHyper',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rgHyperTidak'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rgHyperYa').checked === true)
                                                        {
                                                            ENDOKRIN_HIPOGLIKEMIA = 'true';
                                                        } else {
                                                            ENDOKRIN_HIPOGLIKEMIA = 'false';
                                                        }
                                                        //console.log(ENDOKRIN_HIPOGLIKEMIA);
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Luka Gangren '
                                            },
                                            {
                                                x: 100,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 120,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgGangren',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: 'Ya',
                                                        name: 'rgGangren',
                                                        inputValue: 1,
                                                        id: 'rgGangrenYa'
                                                    },
                                                    {
                                                        boxLabel: 'Tidak',
                                                        name: 'rgGangren',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rgGangrenTidak'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rgGangrenYa').checked === true)
                                                        {
                                                            ENDOKRIN_LUKA_GANGREN = 'true';
                                                        } else {
                                                            ENDOKRIN_LUKA_GANGREN = 'false';
                                                        }
                                                        //console.log(ENDOKRIN_LUKA_GANGREN);
                                                    }
                                                }
                                            },
                                            {
                                                x: 260,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Pus '
                                            },
                                            {
                                                x: 360,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 370,
                                                y: 10,
                                                xtype: 'radiogroup',
                                                id: 'rgPus',
                                                boxMaxWidth: 150,
                                                items: [
                                                    {
                                                        boxLabel: 'Ya',
                                                        name: 'rgPus',
                                                        inputValue: 1,
                                                        id: 'rgPusYa'
                                                    },
                                                    {
                                                        boxLabel: 'Tidak',
                                                        name: 'rgPus',
                                                        inputValue: 0,
                                                        checked: true,
                                                        id: 'rgPusTidak'
                                                    }
                                                ],
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        if (Ext.getCmp('rgPusYa').checked === true)
                                                        {
                                                            ENDOKRIN__PUS = 'true';
                                                        } else {
                                                            ENDOKRIN__PUS = 'false';
                                                        }
                                                        //console.log(ENDOKRIN__PUS);
                                                    }
                                                }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 276,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Masalah '
                                            },
                                            {
                                                x: 100,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 120,
                                                y: 10,
                                                xtype: 'textarea',
                                                id: 'trmasalahendokrin',
                                                width: 840,
                                                height: 266,
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        ENDOKRIN_MASALAH = Ext.get('trmasalahendokrin').getValue();
                                                        //console.log(ENDOKRIN_MASALAH);
                                                    }
                                                }
                                            }]
                            }
                        ]
                    }
            );
    return tabEndokrin;
}
;
function PHygiene()
{
    var tabPHygiene = new Ext.Panel
            (
                    {title: "Personal Hygiene",
                        id: 'tabPHygiene',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        bodyStyle: 'padding:5px 5px 5px 5px',
                        border: true,
                        autoscroll: true,
                        items: [
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Mandi '
                                            },
                                            {
                                                x: 100,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 120,
                                                y: 5,
                                                xtype: 'textfield',
                                                id: 'txtmandi',
                                                width: 400,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        PERSONAL_MANDI = Ext.get('txtmandi').getValue();
                                                    }
                                                }
                                            }]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Keramas '
                                            },
                                            {
                                                x: 100,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 120,
                                                y: 5,
                                                xtype: 'textfield',
                                                id: 'txtKeramas',
                                                width: 400,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        PERSONAL_KERAMAS = Ext.get('txtKeramas').getValue();
                                                    }
                                                }
                                            }]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Sikat Gigi '
                                            },
                                            {
                                                x: 100,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 120,
                                                y: 5,
                                                xtype: 'textfield',
                                                id: 'txtSikatGigi',
                                                width: 400,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        PERSONAL_SIKATGIGI = Ext.get('txtSikatGigi').getValue();
                                                    }
                                                }
                                            }]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 30,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Ganti Pakaian '
                                            },
                                            {
                                                x: 100,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 120,
                                                y: 5,
                                                xtype: 'textfield',
                                                id: 'txtGantiPakaian',
                                                width: 400,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        PERSONAL_GANTIPAKAIAN = Ext.get('txtGantiPakaian').getValue();
                                                    }
                                                }
                                            }]
                            },
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 216,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Masalah '
                                            },
                                            {
                                                x: 100,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 120,
                                                y: 5,
                                                xtype: 'textarea',
                                                id: 'trMasalahKebersihan',
                                                width: 840,
                                                height: 211,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        PERSONAL_MASALAH = Ext.get('trMasalahKebersihan').getValue();
                                                    }
                                                }
                                            }]
                            },
                        ]
                    }
            );
    return tabPHygiene;
}
;
function PsySos()
{
    var tabPsySos = new Ext.Panel
            ({title: "Psiko Sosial Spiritual",
                id: 'tabPsySos',
                fileUpload: true,
                region: 'north',
                layout: 'form',
                bodyStyle: 'padding:5px 5px 5px 5px',
                border: true,
                autoscroll: true,
                items: [
                    {
                        columnWidth: .20,
                        layout: 'absolute',
                        labelWidth: 100,
                        border: false,
                        height: 80,
                        items:
                                [
                                    {
                                        x: 10,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'Orang Paling Dekat '
                                    },
                                    {
                                        x: 110,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 10,
                                        xtype: 'textarea',
                                        id: 'trorangyangdekat',
                                        width: 840,
                                        height: 70,
                                        listeners: {
                                            change: function () {
                                                Ext.getCmp('btnSimpan_viDaftar').enable();
                                                tmpediting = 'true';
                                                PSIKOSOSIAL_ORANGDEKAT = Ext.get('trorangyangdekat').getValue();
                                            }
                                        }
                                    }]
                    },
                    {
                        columnWidth: .20,
                        layout: 'absolute',
                        labelWidth: 100,
                        border: false,
                        height: 80,
                        items:
                                [
                                    {
                                        x: 10,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'Kegiatan Ibadah '
                                    },
                                    {
                                        x: 110,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 10,
                                        xtype: 'textarea',
                                        id: 'trIbadah',
                                        width: 840,
                                        height: 70,
                                        listeners: {
                                            change: function () {
                                                Ext.getCmp('btnSimpan_viDaftar').enable();
                                                tmpediting = 'true';
                                                PSIKOSOSIAL_KEGIATAN_IBADAH = Ext.get('trIbadah').getValue();
                                            }
                                        }
                                    }]
                    },
                    {
                        columnWidth: .20,
                        layout: 'absolute',
                        labelWidth: 100,
                        border: false,
                        height: 175,
                        items:
                                [
                                    {
                                        x: 10,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'Masalah '
                                    },
                                    {
                                        x: 110,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 10,
                                        xtype: 'textarea',
                                        id: 'trMasalahSosial',
                                        width: 840,
                                        height: 165,
                                        listeners: {
                                            change: function () {
                                                Ext.getCmp('btnSimpan_viDaftar').enable();
                                                tmpediting = 'true';
                                                PSIKOSOSIAL_MASALAH = Ext.get('trMasalahSosial').getValue();
                                            }
                                        }
                                    }
                                ]
                    }
                ]
            }
            );
    return tabPsySos;
}
;
function Penunjang()
{
    var tabPenunjang = new Ext.Panel
            (
                    {title: "Data Penunjang(Lab,Foto,USG, Dll)",
                        id: 'tabPenunjang',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        bodyStyle: 'padding:5px 5px 5px 5px',
                        border: true,
                        autoscroll: true,
                        items: [
                            {
                                columnWidth: .20,
                                layout: 'absolute',
                                labelWidth: 100,
                                border: false,
                                height: 336,
                                items:
                                        [
                                            {
                                                x: 10,
                                                y: 10,
                                                xtype: 'label',
                                                text: 'Terapi '
                                            },
                                            {
                                                x: 50,
                                                y: 10,
                                                xtype: 'label',
                                                text: ' : '
                                            },
                                            {
                                                x: 58,
                                                y: 10,
                                                xtype: 'textarea',
                                                id: 'trterapipenunjang',
                                                width: 902,
                                                height: 326,
                                                listeners: {
                                                    change: function () {
                                                        Ext.getCmp('btnSimpan_viDaftar').enable();
                                                        tmpediting = 'true';
                                                        TERAPI_PENUNJANG = Ext.get('trterapipenunjang').getValue();
                                                    }
                                                }
                                            }
                                        ]
                            }
                        ]
                    }
            );
    return tabPenunjang;
}
;
//combo dan load database

function datainit_pengkajian(rowdata)
{
    Ext.getCmp('TxtPopupMedrec').setValue(rowdata.KD_PASIEN);
    Ext.getCmp('TxtPopupNamaPasien').setValue(rowdata.NAMA);
    Ext.getCmp('TxtPopupAlamatPasien').setValue(rowdata.ALAMAT);
    Ext.getCmp('TxtPopupKdUnit').setValue(rowdata.KD_UNIT);
    Ext.getCmp('TxtPopupUnit').setValue(rowdata.NAMA_UNIT);
    Ext.getCmp('TxtPopupPelaksana').setValue(rowdata.NAMA_DOKTER);
    // caridatapasien();
}

function caridatapasien()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/CreateDataObj",
                        params: {
                            Table: 'ViewAskepDetPengkajian',
                            query: "kd_pasien_kunj = '" + tmpkdpasien + "' and kd_unit_kunj = '" + tmpkdunit + "' and urut_masuk_kunj = " + tmpurutmasuk + " and tgl_masuk_kunj = '" + tmptglmasuk + "'"
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {

                                if (cst.totalrecords !== 0)
                                {
                                    var tmphasil = cst.ListDataObj[0];
                                    RIWAYAT_UTAMA = tmphasil.RIWAYAT_UTAMA;
                                    RIWAYAT_PENYAKIT_SEKARANG = tmphasil.RIWAYAT_PENYAKIT_SEKARANG;
                                    RIWAYAT_PENYAKIT_DAHULU = tmphasil.RIWAYAT_PENYAKIT_DAHULU;
                                    NAFAS_JELAS = tmphasil.NAFAS_JELAS;
                                    NAFAS_EVAKUASI_CAIRAN = tmphasil.NAFAS_EVAKUASI_CAIRAN;
                                    NAFAS_PATEN = tmphasil.NAFAS_PATEN;
                                    NAFAS_OBSTRUKTIF = tmphasil.NAFAS_OBSTRUKTIF;
                                    NAFAS_POLA_SIMETRI = tmphasil.NAFAS_POLA_SIMETRI;
                                    NAFAS_POLA_ASIMETRI = tmphasil.NAFAS_POLA_ASIMETRI;
                                    NAFAS_SUARA_NORMAL = tmphasil.NAFAS_SUARA_NORMAL;
                                    NAFAS_SUARA_HIPERSONOR = tmphasil.NAFAS_SUARA_HIPERSONOR;
                                    NAFAS_MENURUN = tmphasil.NAFAS_MENURUN;
                                    NAFAS_JENIS_NORMAL = tmphasil.NAFAS_JENIS_NORMAL;
                                    NAFAS_JENIS_TACHYPNOE = tmphasil.NAFAS_JENIS_TACHYPNOE;
                                    NAFAS_JENIS_CHEYNESTOKES = tmphasil.NAFAS_JENIS_CHEYNESTOKES;
                                    NAFAS_JENIS_RETRACTIVE = tmphasil.NAFAS_JENIS_RETRACTIVE;
                                    NAFAS_JENIS_KUSMAUL = tmphasil.NAFAS_JENIS_KUSMAUL;
                                    NAFAS_JENIS_DISPNOE = tmphasil.NAFAS_JENIS_DISPNOE;
                                    NAFAS_RR = tmphasil.NAFAS_RR;
                                    NAFAS_SUARA_WHEEZING = tmphasil.NAFAS_SUARA_WHEEZING;
                                    NAFAS_SUARA_RONCHI = tmphasil.NAFAS_SUARA_RONCHI;
                                    NAFAS_SUARA_RALES = tmphasil.NAFAS_SUARA_RALES;
                                    NAFAS_JML_CAIRAN = tmphasil.NAFAS_JML_CAIRAN;
                                    NAFAS_WARNA_CAIRAN = tmphasil.NAFAS_WARNA_CAIRAN;
                                    NAFAS_MASALAH = tmphasil.NAFAS_MASALAH;
                                    JANTUNG_REGULER = tmphasil.JANTUNG_REGULER;
                                    JANTUNG_IRREGULER = tmphasil.JANTUNG_IRREGULER;
                                    JANTUNG_S1S2_TUNGGAL = tmphasil.JANTUNG_S1S2_TUNGGAL;
                                    JANTUNG_NYERI_DADA = tmphasil.JANTUNG_NYERI_DADA;
                                    JANTUNG_NYERI_DADA_KET = tmphasil.JANTUNG_NYERI_DADA_KET;
                                    JANTUNG_BUNYI_MURMUR = tmphasil.JANTUNG_BUNYI_MURMUR;
                                    JANTUNG_BUNYI_GALLOP = tmphasil.JANTUNG_BUNYI_GALLOP;
                                    JANTUNG_BUNYI_BISING = tmphasil.JANTUNG_BUNYI_BISING;
                                    JANTUNG_CRT = tmphasil.JANTUNG_CRT;
                                    JANTUNG_AKRAL_HANGAT = tmphasil.JANTUNG_AKRAL_HANGAT;
                                    JANTUNG_AKRAL_DINGIN = tmphasil.JANTUNG_AKRAL_DINGIN;
                                    JANTUNG_PENINGKATAN_JVP = tmphasil.JANTUNG_PENINGKATAN_JVP;
                                    JANTUNG_UKURAN_CVP = tmphasil.JANTUNG_UKURAN_CVP;
                                    JANTUNG_WARNA_JAUNDICE = tmphasil.JANTUNG_WARNA_JAUNDICE;
                                    JANTUNG_WARNA_SIANOTIK = tmphasil.JANTUNG_WARNA_SIANOTIK;
                                    JANTUNG_WARNA_KEMERAHAN = tmphasil.JANTUNG_WARNA_KEMERAHAN;
                                    JANTUNG_WARNA_PUCAT = tmphasil.JANTUNG_WARNA_PUCAT;
                                    JANTUNG_TEKANAN_DARAH = tmphasil.JANTUNG_TEKANAN_DARAH;
                                    JANTUNG_NADI = tmphasil.JANTUNG_NADI;
                                    JANTUNG_TEMP = tmphasil.JANTUNG_TEMP;
                                    JANTUNG_MASALAH = tmphasil.JANTUNG_MASALAH;
                                    OTOT_SENDI_BEBAS = tmphasil.OTOT_SENDI_BEBAS;
                                    OTOT_SENDI_TERBATAS = tmphasil.OTOT_SENDI_TERBATAS;
                                    OTOT_KEKUATAN = tmphasil.OTOT_KEKUATAN;
                                    OTOT_ODEMA_EKSTRIMITAS = tmphasil.OTOT_ODEMA_EKSTRIMITAS;
                                    OTOT_KELAINAN_BENTUK = tmphasil.OTOT_KELAINAN_BENTUK;
                                    OTOT_KREPITASI = tmphasil.OTOT_KREPITASI;
                                    OTOT_MASALAH = tmphasil.OTOT_MASALAH;
                                    SYARAF_GCS_EYE = tmphasil.SYARAF_GCS_EYE;
                                    SYARAF_GCS_VERBAL = tmphasil.SYARAF_GCS_VERBAL;
                                    SYARAF_GCS_MOTORIK = tmphasil.SYARAF_GCS_MOTORIK;
                                    SYARAF_GCS_TOTAL = tmphasil.SYARAF_GCS_TOTAL;
                                    SYARAF_FISIOLOGIS_BRACHIALIS = tmphasil.SYARAF_FISIOLOGIS_BRACHIALIS;
                                    SYARAF_FISIOLOGIS_PATELLA = tmphasil.SYARAF_FISIOLOGIS_PATELLA;
                                    SYARAF_FISIOLOGIS_ACHILLES = tmphasil.SYARAF_FISIOLOGIS_ACHILLES;
                                    SYARAF_PATOLOGIS_CHODDOKS = tmphasil.SYARAF_PATOLOGIS_CHODDOKS;
                                    SYARAF_PATOLOGIS_BABINSKI = tmphasil.SYARAF_PATOLOGIS_BABINSKI;
                                    SYARAF_PATOLOGIS_BUDZINZKY = tmphasil.SYARAF_PATOLOGIS_BUDZINZKY;
                                    SYARAF_PATOLOGIS_KERNIG = tmphasil.SYARAF_PATOLOGIS_KERNIG;
                                    SYARAF_MASALAH = tmphasil.SYARAF_MASALAH;
                                    INDRA_PUPIL_ISOKOR = tmphasil.INDRA_PUPIL_ISOKOR;
                                    INDRA__PUPIL_ANISOKOR = tmphasil.INDRA__PUPIL_ANISOKOR;
                                    INDRA__KONJUNGTIVA_ANEMIS = tmphasil.INDRA__KONJUNGTIVA_ANEMIS;
                                    INDRA__KONJUNGTIVA_ICTERUS = tmphasil.INDRA__KONJUNGTIVA_ICTERUS;
                                    INDRA__KONJUNGTIVA_TIDAK = tmphasil.INDRA__KONJUNGTIVA_TIDAK;
                                    INDRA_GANGGUAN_DENGAR = tmphasil.INDRA_GANGGUAN_DENGAR;
                                    INDRA_OTORHEA = tmphasil.INDRA_OTORHEA;
                                    INDRA_BENTUK_HIDUNG = tmphasil.INDRA_BENTUK_HIDUNG;
                                    INDRA_BENTUK_HIDUNG_KET = tmphasil.INDRA_BENTUK_HIDUNG_KET;
                                    INDRA_GANGGUAN_CIUM = tmphasil.INDRA_GANGGUAN_CIUM;
                                    INDRA_RHINORHEA = tmphasil.INDRA_RHINORHEA;
                                    INDRA_MASALAH = tmphasil.INDRA_MASALAH;
                                    CERNA_MAKAN_HABIS = tmphasil.CERNA_MAKAN_HABIS;
                                    CERNA_MAKAN_KET = tmphasil.CERNA_MAKAN_KET;
                                    CERNA_MAKAN_FREKUENSI = tmphasil.CERNA_MAKAN_FREKUENSI;
                                    CERNA_JML_MINUM = tmphasil.CERNA_JML_MINUM;
                                    CERNA_JENIS_MINUM = tmphasil.CERNA_JENIS_MINUM;
                                    CERNA_MULUT_BERSIH = tmphasil.CERNA_MULUT_BERSIH;
                                    CERNA_MULUT_KOTOR = tmphasil.CERNA_MULUT_KOTOR;
                                    CERNA_MULUT_BERBAU = tmphasil.CERNA_MULUT_BERBAU;
                                    CERNA_MUKOSA_LEMBAB = tmphasil.CERNA_MUKOSA_LEMBAB;
                                    CERNA_MUKOSA_KERING = tmphasil.CERNA_MUKOSA_KERING;
                                    CERNA_MUKOSA_STOMATITIS = tmphasil.CERNA_MUKOSA_STOMATITIS;
                                    CERNA_TENGGOROKAN_SAKIT = tmphasil.CERNA_TENGGOROKAN_SAKIT;
                                    CERNA_TENGGOROKAN_NYERI_TEKAN = tmphasil.CERNA_TENGGOROKAN_NYERI_TEKAN;
                                    CERNA_TENGGOROKAN_PEMBESARANTO = tmphasil.CERNA_TENGGOROKAN_PEMBESARANTO;
                                    CERNA_PERUT_NORMAL = tmphasil.CERNA_PERUT_NORMAL;
                                    CERNA_PERUT_DISTENDED = tmphasil.CERNA_PERUT_DISTENDED;
                                    CERNA_PERUT_METEORISMUS = tmphasil.CERNA_PERUT_METEORISMUS;
                                    CERNA_PERUT_NYERI_TEKAN = tmphasil.CERNA_PERUT_NYERI_TEKAN;
                                    CERNA_PERUT_LOKASI_NYERI = tmphasil.CERNA_PERUT_LOKASI_NYERI;
                                    CERNA_PERISTALTIK = tmphasil.CERNA_PERISTALTIK;
                                    CERNA_TURGOR_KULIT = tmphasil.CERNA_TURGOR_KULIT;
                                    CERNA_PEMBESARAN_HEPAR = tmphasil.CERNA_PEMBESARAN_HEPAR;
                                    CERNA_HEMATEMESIS = tmphasil.CERNA_HEMATEMESIS;
                                    CERNA_EVAKUASI_CAIRAN_ASCITES = tmphasil.CERNA_EVAKUASI_CAIRAN_ASCITES;
                                    CERNA_JML_CAIRAN_ASCITES = tmphasil.CERNA_JML_CAIRAN_ASCITES;
                                    CERNA_WARNA_CAIRAN_ASCITES = tmphasil.CERNA_WARNA_CAIRAN_ASCITES;
                                    CERNA_FREK_BAB = tmphasil.CERNA_FREK_BAB;
                                    CERNA_KONSISTENSI = tmphasil.CERNA_KONSISTENSI;
                                    CERNA_BAU_BAB = tmphasil.CERNA_BAU_BAB;
                                    CERNA_WARNA_BAB = tmphasil.CERNA_WARNA_BAB;
                                    CERNA_MASALAH = tmphasil.CERNA_MASALAH;
                                    KEMIH_KEBERSIHAN = tmphasil.KEMIH_KEBERSIHAN;
                                    KEMIH_ALAT_BANTU = tmphasil.KEMIH_ALAT_BANTU;
                                    KEMIH_JML_URINE = tmphasil.KEMIH_JML_URINE;
                                    KEMIH_WARNA_URINE = tmphasil.KEMIH_WARNA_URINE;
                                    KEMIH_BAU = tmphasil.KEMIH_BAU;
                                    KEMIH_KANDUNG_MEMBESAR = tmphasil.KEMIH_KANDUNG_MEMBESAR;
                                    KEMIH_NYERI_TEKAN = tmphasil.KEMIH_NYERI_TEKAN;
                                    KEMIH_GANGGUAN_ANURIA = tmphasil.KEMIH_GANGGUAN_ANURIA;
                                    KEMIH_GANGGUAN_ALIGURIA = tmphasil.KEMIH_GANGGUAN_ALIGURIA;
                                    KEMIH_GANGGUAN_RETENSI = tmphasil.KEMIH_GANGGUAN_RETENSI;
                                    KEMIH_GANGGUAN_INKONTINENSIA = tmphasil.KEMIH_GANGGUAN_INKONTINENSIA;
                                    KEMIH_GANGGUAN_DISURIA = tmphasil.KEMIH_GANGGUAN_DISURIA;
                                    KEMIH_GANGGUANHEMATURIA = tmphasil.KEMIH_GANGGUANHEMATURIA;
                                    KEMIH_MASALAH = tmphasil.KEMIH_MASALAH;
                                    //console.log(ENDOKRIN_TYROID + ' x ' + ENDOKRIN_HIPOGLIKEMIA + ' x ' + ENDOKRIN_LUKA_GANGREN + ' x ' + ENDOKRIN__PUS + ' x ' + ENDOKRIN_MASALAH);

                                    ENDOKRIN_TYROID = tmphasil.ENDOKRIN_TYROID;
                                    ENDOKRIN_HIPOGLIKEMIA = tmphasil.ENDOKRIN_HIPOGLIKEMIA;
                                    ENDOKRIN_LUKA_GANGREN = tmphasil.ENDOKRIN_LUKA_GANGREN;
                                    ENDOKRIN__PUS = tmphasil.ENDOKRIN__PUS;
                                    ENDOKRIN_MASALAH = tmphasil.ENDOKRIN_MASALAH;
                                    PERSONAL_MANDI = tmphasil.PERSONAL_MANDI;
                                    PERSONAL_SIKATGIGI = tmphasil.PERSONAL_SIKATGIGI;
                                    PERSONAL_KERAMAS = tmphasil.PERSONAL_KERAMAS;
                                    PERSONAL_GANTIPAKAIAN = tmphasil.PERSONAL_GANTIPAKAIAN;
                                    PERSONAL_MASALAH = tmphasil.PERSONAL_MASALAH;
                                    PSIKOSOSIAL_ORANGDEKAT = tmphasil.PSIKOSOSIAL_ORANGDEKAT;
                                    PSIKOSOSIAL_KEGIATAN_IBADAH = tmphasil.PSIKOSOSIAL_KEGIATAN_IBADAH;
                                    PSIKOSOSIAL_MASALAH = tmphasil.PSIKOSOSIAL_MASALAH;
                                    TERAPI_PENUNJANG = tmphasil.TERAPI_PENUNJANG;
                                    //asu -_-

//radiogroup & Chekboxgroup
//tab2 ----------------------------------------------------------------------------------------------------------------------------------
                                    if (tmphasil.NAFAS_PATEN === "t" || tmphasil.NAFAS_OBSTRUKTIF === "t")
                                    {
                                        if (tmphasil.NAFAS_PATEN === "t" && tmphasil.NAFAS_OBSTRUKTIF === "t") {
                                            Ext.getCmp('cgjelannapas').setValue([true, true]);
                                        } else if (tmphasil.NAFAS_PATEN === "t" && tmphasil.NAFAS_OBSTRUKTIF === "f") {
                                            Ext.getCmp('cgjelannapas').setValue([true, false]);
                                        } else if (tmphasil.NAFAS_PATEN === "f" && tmphasil.NAFAS_OBSTRUKTIF === "t")
                                        {
                                            Ext.getCmp('cgjelannapas').setValue([false, true]);
                                        }
                                    }
                                    else {
                                        Ext.getCmp('cgjelannapas').setValue([false, false]);
                                    }

                                    if (tmphasil.NAFAS_JELAS === "t")
                                    {
                                        Ext.getCmp('rgjelas').setValue("rbjelasya", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgjelas').setValue("rbjelastidak", true);
                                    }

                                    if (tmphasil.NAFAS_POLA_SIMETRI === "t" || tmphasil.NAFAS_POLA_ASIMETRI === "t")
                                    {
                                        if (tmphasil.NAFAS_POLA_SIMETRI === "t" && tmphasil.NAFAS_POLA_ASIMETRI === "t") {
                                            Ext.getCmp('cgpolanapas').setValue([true, true]);
                                        } else if (tmphasil.NAFAS_POLA_SIMETRI === "t" && tmphasil.NAFAS_POLA_ASIMETRI === "f") {
                                            Ext.getCmp('cgpolanapas').setValue([true, false]);
                                        } else if (tmphasil.NAFAS_POLA_SIMETRI === "f" && tmphasil.NAFAS_POLA_ASIMETRI === "t")
                                        {
                                            Ext.getCmp('cgpolanapas').setValue([false, true]);
                                        }
                                    }
                                    else {
                                        Ext.getCmp('cgpolanapas').setValue([false, false]);
                                    }

                                    if (tmphasil.NAFAS_SUARA_NORMAL === "t" || tmphasil.NAFAS_SUARA_HIPERSONOR === "t" || tmphasil.NAFAS_MENURUN === "t")
                                    {
                                        if (tmphasil.NAFAS_SUARA_NORMAL === "t" && tmphasil.NAFAS_SUARA_HIPERSONOR === "t" && tmphasil.NAFAS_MENURUN === "t") {
                                            Ext.getCmp('cgsuaralapangparu').setValue([true, true, true]);
                                        } else if (tmphasil.NAFAS_SUARA_NORMAL === "t" && tmphasil.NAFAS_SUARA_HIPERSONOR === "f" && tmphasil.NAFAS_MENURUN === "f") {
                                            Ext.getCmp('cgsuaralapangparu').setValue([true, false, false]);
                                        }
                                        else if (tmphasil.NAFAS_SUARA_NORMAL === "t" && tmphasil.NAFAS_SUARA_HIPERSONOR === "t" && tmphasil.NAFAS_MENURUN === "f") {
                                            Ext.getCmp('cgsuaralapangparu').setValue([true, true, false]);
                                        }
                                        else if (tmphasil.NAFAS_SUARA_NORMAL === "t" && tmphasil.NAFAS_SUARA_HIPERSONOR === "f" && tmphasil.NAFAS_MENURUN === "t") {
                                            Ext.getCmp('cgsuaralapangparu').setValue([true, false, true]);
                                        } else if (tmphasil.NAFAS_SUARA_NORMAL === "f" && tmphasil.NAFAS_SUARA_HIPERSONOR === "t" && tmphasil.NAFAS_MENURUN === "f")
                                        {
                                            Ext.getCmp('cgsuaralapangparu').setValue([false, true, false]);
                                        }
                                        else if (tmphasil.NAFAS_SUARA_NORMAL === "f" && tmphasil.NAFAS_SUARA_HIPERSONOR === "t" && tmphasil.NAFAS_MENURUN === "t")
                                        {
                                            Ext.getCmp('cgsuaralapangparu').setValue([false, true, true]);
                                        } else if (tmphasil.NAFAS_SUARA_NORMAL === "f" && tmphasil.NAFAS_SUARA_HIPERSONOR === "f" && tmphasil.NAFAS_MENURUN === "t")
                                        {
                                            Ext.getCmp('cgsuaralapangparu').setValue([false, false, true]);
                                        }
                                    }
                                    else {
                                        Ext.getCmp('cgsuaralapangparu').setValue([false, false, false]);
                                    }

                                    if (tmphasil.NAFAS_JENIS_NORMAL === "t" || tmphasil.NAFAS_JENIS_TACHYPNOE === "t" || tmphasil.NAFAS_JENIS_CHEYNESTOKES === "t" || tmphasil.NAFAS_JENIS_RETRACTIVE === "t" || tmphasil.NAFAS_JENIS_KUSMAUL === "t" || tmphasil.NAFAS_JENIS_DISPNOE === "t")
                                    {
                                        if (tmphasil.NAFAS_JENIS_NORMAL === "t" && tmphasil.NAFAS_JENIS_TACHYPNOE === "t" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "t" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "t" && tmphasil.NAFAS_JENIS_KUSMAUL === "t" && tmphasil.NAFAS_JENIS_DISPNOE === "t") {
                                            Ext.getCmp('cgjenis').setValue([true, true, true, true, true, true]);
                                        }                                         //1
                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "t" && tmphasil.NAFAS_JENIS_TACHYPNOE === "f" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "f" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "f" && tmphasil.NAFAS_JENIS_KUSMAUL === "f" && tmphasil.NAFAS_JENIS_DISPNOE === "f") {
                                            Ext.getCmp('cgjenis').setValue([true, false, false, false, false, false]);
                                        }
                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "f" && tmphasil.NAFAS_JENIS_TACHYPNOE === "t" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "f" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "f" && tmphasil.NAFAS_JENIS_KUSMAUL === "f" && tmphasil.NAFAS_JENIS_DISPNOE === "f") {
                                            Ext.getCmp('cgjenis').setValue([false, true, false, false, false, false]);
                                        }

                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "f" && tmphasil.NAFAS_JENIS_TACHYPNOE === "f" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "t" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "f" && tmphasil.NAFAS_JENIS_KUSMAUL === "f" && tmphasil.NAFAS_JENIS_DISPNOE === "f") {
                                            Ext.getCmp('cgjenis').setValue([false, false, true, false, false, false]);
                                        }

                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "f" && tmphasil.NAFAS_JENIS_TACHYPNOE === "f" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "f" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "t" && tmphasil.NAFAS_JENIS_KUSMAUL === "f" && tmphasil.NAFAS_JENIS_DISPNOE === "f") {
                                            Ext.getCmp('cgjenis').setValue([false, false, false, true, false, false]);
                                        }

                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "f" && tmphasil.NAFAS_JENIS_TACHYPNOE === "f" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "f" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "f" && tmphasil.NAFAS_JENIS_KUSMAUL === "t" && tmphasil.NAFAS_JENIS_DISPNOE === "f") {
                                            Ext.getCmp('cgjenis').setValue([false, false, false, false, true, false]);
                                        }

                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "f" && tmphasil.NAFAS_JENIS_TACHYPNOE === "f" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "f" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "f" && tmphasil.NAFAS_JENIS_KUSMAUL === "f" && tmphasil.NAFAS_JENIS_DISPNOE === "t") {
                                            Ext.getCmp('cgjenis').setValue([false, false, false, false, false, true]);
                                        }
                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "t" && tmphasil.NAFAS_JENIS_TACHYPNOE === "t" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "f" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "f" && tmphasil.NAFAS_JENIS_KUSMAUL === "f" && tmphasil.NAFAS_JENIS_DISPNOE === "f") {
                                            Ext.getCmp('cgjenis').setValue([true, true, false, false, false, false]);
                                        }

                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "t" && tmphasil.NAFAS_JENIS_TACHYPNOE === "t" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "t" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "f" && tmphasil.NAFAS_JENIS_KUSMAUL === "f" && tmphasil.NAFAS_JENIS_DISPNOE === "f") {
                                            Ext.getCmp('cgjenis').setValue([true, true, true, false, false, false]);
                                        }

                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "t" && tmphasil.NAFAS_JENIS_TACHYPNOE === "t" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "t" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "t" && tmphasil.NAFAS_JENIS_KUSMAUL === "f" && tmphasil.NAFAS_JENIS_DISPNOE === "f") {
                                            Ext.getCmp('cgjenis').setValue([true, true, true, true, false, false]);
                                        }

                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "t" && tmphasil.NAFAS_JENIS_TACHYPNOE === "t" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "t" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "t" && tmphasil.NAFAS_JENIS_KUSMAUL === "t" && tmphasil.NAFAS_JENIS_DISPNOE === "f") {
                                            Ext.getCmp('cgjenis').setValue([true, true, true, true, true, false]);
                                        }
                                        //2
                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "f" && tmphasil.NAFAS_JENIS_TACHYPNOE === "t" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "f" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "f" && tmphasi.lNAFAS_JENIS_KUSMAUL === "f" && tmphasil.NAFAS_JENIS_DISPNOE === "f") {
                                            Ext.getCmp('cgjenis').setValue([false, true, false, false, false, false]);
                                        }

                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "f" && tmphasil.NAFAS_JENIS_TACHYPNOE === "t" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "t" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "f" && tmphasil.NAFAS_JENIS_KUSMAUL === "f" && tmphasil.NAFAS_JENIS_DISPNOE === "f") {
                                            Ext.getCmp('cgjenis').setValue([false, true, true, false, false, false]);
                                        }

                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "f" && tmphasil.NAFAS_JENIS_TACHYPNOE === "t" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "t" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "t" && tmphasil.NAFAS_JENIS_KUSMAUL === "f" && tmphasil.NAFAS_JENIS_DISPNOE === "f") {
                                            Ext.getCmp('cgjenis').setValue([false, true, true, true, false, false]);
                                        }

                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "f" && tmphasil.NAFAS_JENIS_TACHYPNOE === "t" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "t" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "t" && tmphasil.NAFAS_JENIS_KUSMAUL === "t" && tmphasil.NAFAS_JENIS_DISPNOE === "f") {
                                            Ext.getCmp('cgjenis').setValue([false, true, true, true, true, false]);
                                        }

                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "f" && tmphasil.NAFAS_JENIS_TACHYPNOE === "t" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "t" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "t" && tmphasil.NAFAS_JENIS_KUSMAUL === "t" && tmphasil.NAFAS_JENIS_DISPNOE === "t") {
                                            Ext.getCmp('cgjenis').setValue([false, true, true, true, true, true]);
                                        }                                         //3
                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "f" && tmphasil.NAFAS_JENIS_TACHYPNOE === "f" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "t" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "f" && tmphasil.NAFAS_JENIS_KUSMAUL === "f" && tmphasil.NAFAS_JENIS_DISPNOE === "f") {
                                            Ext.getCmp('cgjenis').setValue([false, false, false, true, false, false]);
                                        }

                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "f" && tmphasil.NAFAS_JENIS_TACHYPNOE === "f" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "t" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "t" && tmphasil.NAFAS_JENIS_KUSMAUL === "f" && tmphasil.NAFAS_JENIS_DISPNOE === "f") {
                                            Ext.getCmp('cgjenis').setValue([false, false, true, true, false, false]);
                                        }

                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "f" && tmphasil.NAFAS_JENIS_TACHYPNOE === "f" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "t" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "t" && tmphasil.NAFAS_JENIS_KUSMAUL === "t" && tmphasil.NAFAS_JENIS_DISPNOE === "f") {
                                            Ext.getCmp('cgjenis').setValue([false, false, true, true, true, false]);
                                        }

                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "f" && tmphasil.NAFAS_JENIS_TACHYPNOE === "f" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "t" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "t" && tmphasil.NAFAS_JENIS_KUSMAUL === "t" && tmphasil.NAFAS_JENIS_DISPNOE === "t") {
                                            Ext.getCmp('cgjenis').setValue([false, false, true, true, true, true]);
                                        }
                                        //4
                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "f" && tmphasil.NAFAS_JENIS_TACHYPNOE === "f" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "f" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "t" && tmphasil.NAFAS_JENIS_KUSMAUL === "f" && tmphasil.NAFAS_JENIS_DISPNOE === "f") {
                                            Ext.getCmp('cgjenis').setValue([false, false, false, true, false, false]);
                                        }

                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "f" && tmphasil.NAFAS_JENIS_TACHYPNOE === "f" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "f" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "t" && tmphasil.NAFAS_JENIS_KUSMAUL === "t" && tmphasil.NAFAS_JENIS_DISPNOE === "f") {
                                            Ext.getCmp('cgjenis').setValue([false, false, false, true, true, false]);
                                        }

                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "f" && tmphasil.NAFAS_JENIS_TACHYPNOE === "f" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "f" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "t" && tmphasil.NAFAS_JENIS_KUSMAUL === "t" && tmphasil.NAFAS_JENIS_DISPNOE === "t") {
                                            Ext.getCmp('cgjenis').setValue([false, false, false, true, true, true]);
                                        }                                         //5
                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "f" && tmphasil.NAFAS_JENIS_TACHYPNOE === "f" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "f" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "f" && tmphasil.NAFAS_JENIS_KUSMAUL === "t" && tmphasil.NAFAS_JENIS_DISPNOE === "f") {
                                            Ext.getCmp('cgjenis').setValue([false, false, false, false, true, false]);
                                        }

                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "f" && tmphasil.NAFAS_JENIS_TACHYPNOE === "f" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "f" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "f" && tmphasil.NAFAS_JENIS_KUSMAUL === "t" && tmphasil.NAFAS_JENIS_DISPNOE === "t") {
                                            Ext.getCmp('cgjenis').setValue([false, false, false, false, true, true]);
                                        }
                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "t" && tmphasil.NAFAS_JENIS_TACHYPNOE === "f" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "f" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "f" && tmphasil.NAFAS_JENIS_KUSMAUL === "t" && tmphasil.NAFAS_JENIS_DISPNOE === "t") {
                                            Ext.getCmp('cgjenis').setValue([true, false, false, false, true, true]);
                                        }
                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "t" && tmphasil.NAFAS_JENIS_TACHYPNOE === "t" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "f" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "f" && tmphasil.NAFAS_JENIS_KUSMAUL === "t" && tmphasil.NAFAS_JENIS_DISPNOE === "t") {
                                            Ext.getCmp('cgjenis').setValue([true, true, false, false, true, true]);
                                        }
                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "t" && tmphasil.NAFAS_JENIS_TACHYPNOE === "t" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "t" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "f" && tmphasil.NAFAS_JENIS_KUSMAUL === "t" && tmphasil.NAFAS_JENIS_DISPNOE === "t") {
                                            Ext.getCmp('cgjenis').setValue([true, true, true, false, true, true]);
                                        }                                         //6
                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "f" && tmphasil.NAFAS_JENIS_TACHYPNOE === "f" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "f" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "f" && tmphasil.NAFAS_JENIS_KUSMAUL === "f" && tmphasil.NAFAS_JENIS_DISPNOE === "t") {
                                            Ext.getCmp('cgjenis').setValue([false, false, false, false, false, true]);
                                        }
                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "t" && tmphasil.NAFAS_JENIS_TACHYPNOE === "f" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "f" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "f" && tmphasil.NAFAS_JENIS_KUSMAUL === "f" && tmphasil.NAFAS_JENIS_DISPNOE === "t") {
                                            Ext.getCmp('cgjenis').setValue([true, false, false, false, false, true]);
                                        }
                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "t" && tmphasil.NAFAS_JENIS_TACHYPNOE === "t" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "f" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "f" && tmphasil.NAFAS_JENIS_KUSMAUL === "f" && tmphasil.NAFAS_JENIS_DISPNOE === "t") {
                                            Ext.getCmp('cgjenis').setValue([true, true, false, false, false, true]);
                                        }
                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "t" && tmphasil.NAFAS_JENIS_TACHYPNOE === "t" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "t" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "f" && tmphasil.NAFAS_JENIS_KUSMAUL === "f" && tmphasil.NAFAS_JENIS_DISPNOE === "t") {
                                            Ext.getCmp('cgjenis').setValue([true, true, true, false, false, true]);
                                        }

                                        else if (tmphasil.NAFAS_JENIS_NORMAL === "t" && tmphasil.NAFAS_JENIS_TACHYPNOE === "t" && tmphasil.NAFAS_JENIS_CHEYNESTOKES === "t" &&
                                                tmphasil.NAFAS_JENIS_RETRACTIVE === "t" && tmphasil.NAFAS_JENIS_KUSMAUL === "f" && tmphasil.NAFAS_JENIS_DISPNOE === "t") {
                                            Ext.getCmp('cgjenis').setValue([true, true, true, true, false, true]);
                                        }
                                    }
                                    else {
                                        Ext.getCmp('cgjenis').setValue([false, false, false, false, false, false]);
                                    }


                                    if (tmphasil.NAFAS_SUARA_WHEEZING === "t" || tmphasil.NAFAS_SUARA_RONCHI === "t" || tmphasil.NAFAS_SUARA_RALES === "t")
                                    {
                                        if (tmphasil.NAFAS_SUARA_WHEEZING === "t" && tmphasil.NAFAS_SUARA_RONCHI === "t" && tmphasil.NAFAS_SUARA_RALES === "t") {
                                            Ext.getCmp('cgsuaranafastambahan').setValue([true, true, true]);
                                        } else if (tmphasil.NAFAS_SUARA_WHEEZING === "t" && tmphasil.NAFAS_SUARA_RONCHI === "f" && tmphasil.NAFAS_SUARA_RALES === "f") {
                                            Ext.getCmp('cgsuaranafastambahan').setValue([true, false, false]);
                                        }
                                        else if (tmphasil.NAFAS_SUARA_WHEEZING === "t" && tmphasil.NAFAS_SUARA_RONCHI === "t" && tmphasil.NAFAS_SUARA_RALES === "f") {
                                            Ext.getCmp('cgsuaranafastambahan').setValue([true, true, false]);
                                        }
                                        else if (tmphasil.NAFAS_SUARA_WHEEZING === "t" && tmphasil.NAFAS_SUARA_RONCHI === "f" && tmphasil.NAFAS_SUARA_RALES === "t") {
                                            Ext.getCmp('cgsuaranafastambahan').setValue([true, false, true]);
                                        } else if (tmphasil.NAFAS_SUARA_WHEEZING === "f" && tmphasil.NAFAS_SUARA_RONCHI === "t" && tmphasil.NAFAS_SUARA_RALES === "f")
                                        {
                                            Ext.getCmp('cgsuaranafastambahan').setValue([false, true, false]);
                                        }
                                        else if (tmphasil.NAFAS_SUARA_WHEEZING === "f" && tmphasil.NAFAS_SUARA_RONCHI === "t" && tmphasil.NAFAS_SUARA_RALES === "t")
                                        {
                                            Ext.getCmp('cgsuaranafastambahan').setValue([false, true, true]);
                                        } else if (tmphasil.NAFAS_SUARA_WHEEZING === "f" && tmphasil.NAFAS_SUARA_RONCHI === "f" && tmphasil.NAFAS_SUARA_RALES === "t")
                                        {
                                            Ext.getCmp('cgsuaranafastambahan').setValue([false, false, true]);
                                        }
                                    }
                                    else {
                                        Ext.getCmp('cgsuaranafastambahan').setValue([false, false, false]);
                                    }

                                    if (tmphasil.NAFAS_EVAKUASI_CAIRAN === "t")
                                    {
                                        Ext.getCmp('rgefakuasi').setValue("rbefakuasiya", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgefakuasi').setValue("rbefakuasitidak", true);
                                    }
                                    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//tab3 ----------------------------------------------------------------------------------------------------------------------------------
                                    if (tmphasil.JANTUNG_REGULER === "t" || tmphasil.JANTUNG_IRREGULER === "t")
                                    {
                                        if (tmphasil.JANTUNG_REGULER === "t" && tmphasil.JANTUNG_IRREGULER === "t") {
                                            Ext.getCmp('cgIramaJantung').setValue([true, true]);
                                        } else if (tmphasil.JANTUNG_REGULER === "t" && tmphasil.JANTUNG_IRREGULER === "f") {
                                            Ext.getCmp('cgIramaJantung').setValue([true, false]);
                                        } else if (tmphasil.JANTUNG_REGULER === "f" && tmphasil.JANTUNG_IRREGULER === "t")
                                        {
                                            Ext.getCmp('cgIramaJantung').setValue([false, true]);
                                        }
                                    }
                                    else {
                                        Ext.getCmp('cgIramaJantung').setValue([false, false]);
                                    }

                                    if (tmphasil.JANTUNG_S1S2_TUNGGAL === "t")
                                    {
                                        Ext.getCmp('rgS1S2Tunggal').setValue("rbS1S2Tunggalya", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgS1S2Tunggal').setValue("rbS1S2Tunggaltidak", true);
                                    }

                                    if (tmphasil.JANTUNG_NYERI_DADA === "t")
                                    {
                                        Ext.getCmp('rgNyeridada').setValue("rgNyeridadaya", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgNyeridada').setValue("rgNyeridadatidak", true);
                                    }

                                    if (tmphasil.JANTUNG_BUNYI_MURMUR === "t" || tmphasil.JANTUNG_BUNYI_GALLOP === "t" || tmphasil.JANTUNG_BUNYI_BISING === "t") {
                                        if (tmphasil.JANTUNG_BUNYI_MURMUR === "t" && tmphasil.JANTUNG_BUNYI_GALLOP === "t" && tmphasil.JANTUNG_BUNYI_BISING === "t") {
                                            Ext.getCmp('cgBunyiJantungTambahan').setValue([true, true, true]);
                                        } else if (tmphasil.JANTUNG_BUNYI_MURMUR === "t" && tmphasil.JANTUNG_BUNYI_GALLOP === "f" && tmphasil.JANTUNG_BUNYI_BISING === "f") {
                                            Ext.getCmp('cgBunyiJantungTambahan').setValue([true, false, false]);
                                        }
                                        else if (tmphasil.JANTUNG_BUNYI_MURMUR === "t" && tmphasil.JANTUNG_BUNYI_GALLOP === "t" && tmphasil.JANTUNG_BUNYI_BISING === "f") {
                                            Ext.getCmp('cgBunyiJantungTambahan').setValue([true, true, false]);
                                        }
                                        else if (tmphasil.JANTUNG_BUNYI_MURMUR === "t" && tmphasil.JANTUNG_BUNYI_GALLOP === "f" && tmphasil.JANTUNG_BUNYI_BISING === "t") {
                                            Ext.getCmp('cgBunyiJantungTambahan').setValue([true, false, true]);
                                        } else if (tmphasil.JANTUNG_BUNYI_MURMUR === "f" && tmphasil.JANTUNG_BUNYI_GALLOP === "t" && tmphasil.JANTUNG_BUNYI_BISING === "f") {
                                            Ext.getCmp('cgBunyiJantungTambahan').setValue([false, true, false]);
                                        }
                                        else if (tmphasil.JANTUNG_BUNYI_MURMUR === "f" && tmphasil.JANTUNG_BUNYI_GALLOP === "t" && tmphasil.JANTUNG_BUNYI_BISING === "t") {
                                            Ext.getCmp('cgBunyiJantungTambahan').setValue([false, true, true]);
                                        } else if (tmphasil.JANTUNG_BUNYI_MURMUR === "f" && tmphasil.JANTUNG_BUNYI_GALLOP === "f" && tmphasil.JANTUNG_BUNYI_BISING === "t") {
                                            Ext.getCmp('cgBunyiJantungTambahan').setValue([false, false, true]);
                                        }
                                    }
                                    else {
                                        Ext.getCmp('cgBunyiJantungTambahan').setValue([false, false, false]);
                                    }

                                    if (tmphasil.JANTUNG_CRT === "t")
                                    {
                                        Ext.getCmp('rgCRT').setValue("rgCRTkurang", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgCRT').setValue("rgCRTlebih", true);
                                    }

                                    if (tmphasil.JANTUNG_AKRAL_HANGAT === "t" || tmphasil.JANTUNG_AKRAL_DINGIN === "t") {
                                        if (tmphasil.JANTUNG_AKRAL_HANGAT === "t" && tmphasil.JANTUNG_AKRAL_DINGIN === "t") {
                                            Ext.getCmp('cgAkral').setValue([true, true]);
                                        } else if (tmphasil.JANTUNG_AKRAL_HANGAT === "t" && tmphasil.JANTUNG_AKRAL_DINGIN === "f") {
                                            Ext.getCmp('cgAkral').setValue([true, false]);
                                        } else if (tmphasil.JANTUNG_AKRAL_HANGAT === "f" && tmphasil.JANTUNG_AKRAL_DINGIN === "t") {
                                            Ext.getCmp('cgAkral').setValue([false, true]);
                                        }
                                    }
                                    else {
                                        Ext.getCmp('cgAkral').setValue([false, false]);
                                    }

                                    if (tmphasil.JANTUNG_PENINGKATAN_JVP === "t")
                                    {
                                        Ext.getCmp('rgJVP').setValue("rgJVPya", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgJVP').setValue("rgJVPtidak", true);
                                    }

                                    if (tmphasil.JANTUNG_WARNA_JAUNDICE === "t" || tmphasil.JANTUNG_WARNA_SIANOTIK === "t" || tmphasil.JANTUNG_WARNA_KEMERAHAN === "t" || tmphasil.JANTUNG_WARNA_PUCAT === "t")
                                    {
                                        if (tmphasil.JANTUNG_WARNA_JAUNDICE === "t" && tmphasil.JANTUNG_WARNA_SIANOTIK === "t" && tmphasil.JANTUNG_WARNA_KEMERAHAN === "t" && tmphasil.JANTUNG_WARNA_PUCAT === "t") {
                                            Ext.getCmp('cgWarnaKulit').setValue([true, true, true, true]);
                                        }                                         //1
                                        else if (tmphasil.JANTUNG_WARNA_JAUNDICE === "t" && tmphasil.JANTUNG_WARNA_SIANOTIK === "f" && tmphasil.JANTUNG_WARNA_KEMERAHAN === "f" && tmphasil.JANTUNG_WARNA_PUCAT === "f") {
                                            Ext.getCmp('cgWarnaKulit').setValue([true, false, false, false]);
                                        }

                                        else if (tmphasil.JANTUNG_WARNA_JAUNDICE === "t" && tmphasil.JANTUNG_WARNA_SIANOTIK === "t" && tmphasil.JANTUNG_WARNA_KEMERAHAN === "f" && tmphasil.JANTUNG_WARNA_PUCAT === "f") {
                                            Ext.getCmp('cgWarnaKulit').setValue([true, true, false, false]);
                                        }

                                        else if (tmphasil.JANTUNG_WARNA_JAUNDICE === "t" && tmphasil.JANTUNG_WARNA_SIANOTIK === "t" && tmphasil.JANTUNG_WARNA_KEMERAHAN === "t" && tmphasil.JANTUNG_WARNA_PUCAT === "f") {
                                            Ext.getCmp('cgWarnaKulit').setValue([true, true, true, false]);
                                        }
                                        //2
                                        else if (tmphasil.JANTUNG_WARNA_JAUNDICE === "f" && tmphasil.JANTUNG_WARNA_SIANOTIK === "t" && tmphasil.JANTUNG_WARNA_KEMERAHAN === "f" && tmphasil.JANTUNG_WARNA_PUCAT === "f") {
                                            Ext.getCmp('cgWarnaKulit').setValue([false, true, false, false]);
                                        }

                                        else if (tmphasil.JANTUNG_WARNA_JAUNDICE === "f" && tmphasil.JANTUNG_WARNA_SIANOTIK === "t" && tmphasil.JANTUNG_WARNA_KEMERAHAN === "t" && tmphasil.JANTUNG_WARNA_PUCAT === "f") {
                                            Ext.getCmp('cgWarnaKulit').setValue([false, true, true, false]);
                                        }

                                        else if (tmphasil.JANTUNG_WARNA_JAUNDICE === "f" && tmphasil.JANTUNG_WARNA_SIANOTIK === "t" && tmphasil.JANTUNG_WARNA_KEMERAHAN === "t" && tmphasil.JANTUNG_WARNA_PUCAT === "t") {
                                            Ext.getCmp('cgWarnaKulit').setValue([false, true, true, true]);
                                        }                                         //3
                                        else if (tmphasil.JANTUNG_WARNA_JAUNDICE === "f" && tmphasil.JANTUNG_WARNA_SIANOTIK === "f" && tmphasil.JANTUNG_WARNA_KEMERAHAN === "t" && tmphasil.JANTUNG_WARNA_PUCAT === "f") {
                                            Ext.getCmp('cgWarnaKulit').setValue([false, false, true, false]);
                                        }

                                        else if (tmphasil.JANTUNG_WARNA_JAUNDICE === "f" && tmphasil.JANTUNG_WARNA_SIANOTIK === "f" && tmphasil.JANTUNG_WARNA_KEMERAHAN === "t" && tmphasil.JANTUNG_WARNA_PUCAT === "t") {
                                            Ext.getCmp('cgWarnaKulit').setValue([false, false, true, true]);
                                        }
                                        else if (tmphasil.JANTUNG_WARNA_JAUNDICE === "t" && tmphasil.JANTUNG_WARNA_SIANOTIK === "f" && tmphasil.JANTUNG_WARNA_KEMERAHAN === "t" && tmphasil.JANTUNG_WARNA_PUCAT === "t") {
                                            Ext.getCmp('cgWarnaKulit').setValue([true, false, true, true]);
                                        }                                         //4
                                        else if (tmphasil.JANTUNG_WARNA_JAUNDICE === "f" && tmphasil.JANTUNG_WARNA_SIANOTIK === "f" && tmphasil.JANTUNG_WARNA_KEMERAHAN === "f" && tmphasil.JANTUNG_WARNA_PUCAT === "t") {
                                            Ext.getCmp('cgWarnaKulit').setValue([false, false, false, true]);
                                        }
                                        else if (tmphasil.JANTUNG_WARNA_JAUNDICE === "t" && tmphasil.JANTUNG_WARNA_SIANOTIK === "f" && tmphasil.JANTUNG_WARNA_KEMERAHAN === "f" && tmphasil.JANTUNG_WARNA_PUCAT === "t") {
                                            Ext.getCmp('cgWarnaKulit').setValue([true, false, false, true]);
                                        }
                                        else if (tmphasil.JANTUNG_WARNA_JAUNDICE === "t" && tmphasil.JANTUNG_WARNA_SIANOTIK === "t" && tmphasil.JANTUNG_WARNA_KEMERAHAN === "f" && tmphasil.JANTUNG_WARNA_PUCAT === "t") {
                                            Ext.getCmp('cgWarnaKulit').setValue([true, true, false, true]);
                                        }
                                    }
                                    else {
                                        Ext.getCmp('cgWarnaKulit').setValue([false, false, false, false]);
                                    }
                                    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//tab4 -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                                    if (tmphasil.OTOT_SENDI_BEBAS === "t" || tmphasil.OTOT_SENDI_TERBATAS === "t")
                                    {
                                        if (tmphasil.OTOT_SENDI_BEBAS === "t" && tmphasil.OTOT_SENDI_TERBATAS === "t") {
                                            Ext.getCmp('cgPergerakanSendi').setValue([true, true]);
                                        } else if (tmphasil.OTOT_SENDI_BEBAS === "t" && tmphasil.OTOT_SENDI_TERBATAS === "f") {
                                            Ext.getCmp('cgPergerakanSendi').setValue([true, false]);
                                        } else if (tmphasil.OTOT_SENDI_BEBAS === "f" && tmphasil.OTOT_SENDI_TERBATAS === "t")
                                        {
                                            Ext.getCmp('cgPergerakanSendi').setValue([false, true]);
                                        }
                                    }
                                    else {
                                        Ext.getCmp('cgPergerakanSendi').setValue([false, false]);
                                    }

                                    if (tmphasil.OTOT_ODEMA_EKSTRIMITAS === "t")
                                    {
                                        Ext.getCmp('rgekstrimitas').setValue("rgekstrimitasya", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgekstrimitas').setValue("rgekstrimitastidak", true);
                                    }

                                    if (tmphasil.OTOT_KELAINAN_BENTUK === "t")
                                    {
                                        Ext.getCmp('rgKelainan').setValue("rgKelainanya", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgKelainan').setValue("rgKelainantidak", true);
                                    }

                                    if (tmphasil.OTOT_KREPITASI === "t")
                                    {
                                        Ext.getCmp('rgKrepitas').setValue("rgKrepitasya", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgKrepitas').setValue("rgKrepitastidak", true);
                                    }
                                    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//tab5 -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                                    if (tmphasil.SYARAF_GCS_EYE === "t" || tmphasil.SYARAF_GCS_VERBAL === "t" || tmphasil.SYARAF_GCS_MOTORIK === "t")
                                    {
                                        if (tmphasil.SYARAF_GCS_EYE === "t" && tmphasil.SYARAF_GCS_VERBAL === "t" && tmphasil.SYARAF_GCS_MOTORIK === "t") {
                                            Ext.getCmp('cgGCS').setValue([true, true, true]);
                                        } else if (tmphasil.SYARAF_GCS_EYE === "t" && tmphasil.SYARAF_GCS_VERBAL === "f" && tmphasil.SYARAF_GCS_MOTORIK === "f") {
                                            Ext.getCmp('cgGCS').setValue([true, false, false]);
                                        }
                                        else if (tmphasil.SYARAF_GCS_EYE === "t" && tmphasil.SYARAF_GCS_VERBAL === "t" && tmphasil.SYARAF_GCS_MOTORIK === "f") {
                                            Ext.getCmp('cgGCS').setValue([true, true, false]);
                                        }
                                        else if (tmphasil.SYARAF_GCS_EYE === "t" && tmphasil.SYARAF_GCS_VERBAL === "f" && tmphasil.SYARAF_GCS_MOTORIK === "t") {
                                            Ext.getCmp('cgGCS').setValue([true, false, true]);
                                        } else if (tmphasil.SYARAF_GCS_EYE === "f" && tmphasil.SYARAF_GCS_VERBAL === "t" && tmphasil.SYARAF_GCS_MOTORIK === "f")
                                        {
                                            Ext.getCmp('cgGCS').setValue([false, true, false]);
                                        }
                                        else if (tmphasil.SYARAF_GCS_EYE === "f" && tmphasil.SYARAF_GCS_VERBAL === "t" && tmphasil.SYARAF_GCS_MOTORIK === "t")
                                        {
                                            Ext.getCmp('cgGCS').setValue([false, true, true]);
                                        } else if (tmphasil.SYARAF_GCS_EYE === "f" && tmphasil.SYARAF_GCS_VERBAL === "f" && tmphasil.SYARAF_GCS_MOTORIK === "t")
                                        {
                                            Ext.getCmp('cgGCS').setValue([false, false, true]);
                                        }
                                    }
                                    else {
                                        Ext.getCmp('cgGCS').setValue([false, false, false]);
                                    }

                                    if (tmphasil.SYARAF_FISIOLOGIS_BRACHIALIS === "t" || tmphasil.SYARAF_FISIOLOGIS_PATELLA === "t" || tmphasil.SYARAF_FISIOLOGIS_ACHILLES === "t")
                                    {
                                        if (tmphasil.SYARAF_FISIOLOGIS_BRACHIALIS === "t" && tmphasil.SYARAF_FISIOLOGIS_PATELLA === "t" && tmphasil.SYARAF_FISIOLOGIS_ACHILLES === "t") {
                                            Ext.getCmp('cgRefleksi').setValue([true, true, true]);
                                        } else if (tmphasil.SYARAF_FISIOLOGIS_BRACHIALIS === "t" && tmphasil.SYARAF_FISIOLOGIS_PATELLA === "f" && tmphasil.SYARAF_FISIOLOGIS_ACHILLES === "f") {
                                            Ext.getCmp('cgRefleksi').setValue([true, false, false]);
                                        }
                                        else if (tmphasil.SYARAF_FISIOLOGIS_BRACHIALIS === "t" && tmphasil.SYARAF_FISIOLOGIS_PATELLA === "t" && tmphasil.SYARAF_FISIOLOGIS_ACHILLES === "f") {
                                            Ext.getCmp('cgRefleksi').setValue([true, true, false]);
                                        }
                                        else if (tmphasil.SYARAF_FISIOLOGIS_BRACHIALIS === "t" && tmphasil.SYARAF_FISIOLOGIS_PATELLA === "f" && tmphasil.SYARAF_FISIOLOGIS_ACHILLES === "t") {
                                            Ext.getCmp('cgRefleksi').setValue([true, false, true]);
                                        } else if (tmphasil.SYARAF_FISIOLOGIS_BRACHIALIS === "f" && tmphasil.SYARAF_FISIOLOGIS_PATELLA === "t" && tmphasil.SYARAF_FISIOLOGIS_ACHILLES === "f")
                                        {
                                            Ext.getCmp('cgRefleksi').setValue([false, true, false]);
                                        }
                                        else if (tmphasil.SYARAF_FISIOLOGIS_BRACHIALIS === "f" && tmphasil.SYARAF_FISIOLOGIS_PATELLA === "t" && tmphasil.SYARAF_FISIOLOGIS_ACHILLES === "t")
                                        {
                                            Ext.getCmp('cgRefleksi').setValue([false, true, true]);
                                        } else if (tmphasil.SYARAF_FISIOLOGIS_BRACHIALIS === "f" && tmphasil.SYARAF_FISIOLOGIS_PATELLA === "f" && tmphasil.SYARAF_FISIOLOGIS_ACHILLES === "t")
                                        {
                                            Ext.getCmp('cgRefleksi').setValue([false, false, true]);
                                        }
                                    }
                                    else {
                                        Ext.getCmp('cgRefleksi').setValue([false, false, false]);
                                    }

                                    if (tmphasil.SYARAF_PATOLOGIS_CHODDOKS === "t" || tmphasil.SYARAF_PATOLOGIS_BABINSKI === "t" ||
                                            tmphasil.SYARAF_PATOLOGIS_BUDZINZKY === "t" || tmphasil.SYARAF_PATOLOGIS_KERNIG === "t")
                                    {
                                        if (tmphasil.SYARAF_PATOLOGIS_CHODDOKS === "t" && tmphasil.SYARAF_PATOLOGIS_BABINSKI === "t" &&
                                                tmphasil.SYARAF_PATOLOGIS_BUDZINZKY === "t" && tmphasil.SYARAF_PATOLOGIS_KERNIG === "t") {
                                            Ext.getCmp('cgPatologis').setValue([true, true, true, true]);
                                        }                                         //1
                                        else if (tmphasil.SYARAF_PATOLOGIS_CHODDOKS === "t" && tmphasil.SYARAF_PATOLOGIS_BABINSKI === "f" &&
                                                tmphasil.SYARAF_PATOLOGIS_BUDZINZKY === "f" && tmphasil.SYARAF_PATOLOGIS_KERNIG === "f") {
                                            Ext.getCmp('cgPatologis').setValue([true, false, false, false]);
                                        }

                                        else if (tmphasil.SYARAF_PATOLOGIS_CHODDOKS === "t" && tmphasil.SYARAF_PATOLOGIS_BABINSKI === "t" &&
                                                tmphasil.SYARAF_PATOLOGIS_BUDZINZKY === "f" && tmphasil.SYARAF_PATOLOGIS_KERNIG === "f") {
                                            Ext.getCmp('cgPatologis').setValue([true, true, false, false]);
                                        }

                                        else if (tmphasil.SYARAF_PATOLOGIS_CHODDOKS === "t" && tmphasil.SYARAF_PATOLOGIS_BABINSKI === "t" &&
                                                tmphasil.SYARAF_PATOLOGIS_BUDZINZKY === "t" && tmphasil.SYARAF_PATOLOGIS_KERNIG === "f") {
                                            Ext.getCmp('cgPatologis').setValue([true, true, true, false]);
                                        }
                                        //2
                                        else if (tmphasil.SYARAF_PATOLOGIS_CHODDOKS === "f" && tmphasil.SYARAF_PATOLOGIS_BABINSKI === "t" &&
                                                tmphasil.SYARAF_PATOLOGIS_BUDZINZKY === "f" && tmphasil.SYARAF_PATOLOGIS_KERNIG === "f") {
                                            Ext.getCmp('cgPatologis').setValue([false, true, false, false]);
                                        }

                                        else if (tmphasil.SYARAF_PATOLOGIS_CHODDOKS === "f" && tmphasil.SYARAF_PATOLOGIS_BABINSKI === "t" &&
                                                tmphasil.SYARAF_PATOLOGIS_BUDZINZKY === "t" && tmphasil.SYARAF_PATOLOGIS_KERNIG === "f") {
                                            Ext.getCmp('cgPatologis').setValue([false, true, true, false]);
                                        }

                                        else if (tmphasil.SYARAF_PATOLOGIS_CHODDOKS === "f" && tmphasil.SYARAF_PATOLOGIS_BABINSKI === "t" &&
                                                tmphasil.SYARAF_PATOLOGIS_BUDZINZKY === "t" && tmphasil.SYARAF_PATOLOGIS_KERNIG === "t") {
                                            Ext.getCmp('cgPatologis').setValue([false, true, true, true]);
                                        }                                         //3
                                        else if (tmphasil.SYARAF_PATOLOGIS_CHODDOKS === "f" && tmphasil.SYARAF_PATOLOGIS_BABINSKI === "f" &&
                                                tmphasil.SYARAF_PATOLOGIS_BUDZINZKY === "t" && tmphasil.SYARAF_PATOLOGIS_KERNIG === "f") {
                                            Ext.getCmp('cgPatologis').setValue([false, false, true, false]);
                                        }

                                        else if (tmphasil.SYARAF_PATOLOGIS_CHODDOKS === "f" && tmphasil.SYARAF_PATOLOGIS_BABINSKI === "f" &&
                                                tmphasil.SYARAF_PATOLOGIS_BUDZINZKY === "t" && tmphasil.SYARAF_PATOLOGIS_KERNIG === "t") {
                                            Ext.getCmp('cgPatologis').setValue([false, false, true, true]);
                                        }
                                        else if (tmphasil.SYARAF_PATOLOGIS_CHODDOKS === "t" && tmphasil.SYARAF_PATOLOGIS_BABINSKI === "f" &&
                                                tmphasil.SYARAF_PATOLOGIS_BUDZINZKY === "t" && tmphasil.SYARAF_PATOLOGIS_KERNIG === "t") {
                                            Ext.getCmp('cgPatologis').setValue([true, false, true, true]);
                                        }                                         //4
                                        else if (tmphasil.SYARAF_PATOLOGIS_CHODDOKS === "f" && tmphasil.SYARAF_PATOLOGIS_BABINSKI === "f" &&
                                                tmphasil.SYARAF_PATOLOGIS_BUDZINZKY === "f" && tmphasil.SYARAF_PATOLOGIS_KERNIG === "t") {
                                            Ext.getCmp('cgPatologis').setValue([false, false, false, true]);
                                        }
                                        else if (tmphasil.SYARAF_PATOLOGIS_CHODDOKS === "t" && tmphasil.SYARAF_PATOLOGIS_BABINSKI === "f" &&
                                                tmphasil.SYARAF_PATOLOGIS_BUDZINZKY === "f" && tmphasil.SYARAF_PATOLOGIS_KERNIG === "t") {
                                            Ext.getCmp('cgPatologis').setValue([true, false, false, true]);
                                        }
                                        else if (tmphasil.SYARAF_PATOLOGIS_CHODDOKS === "t" && tmphasil.SYARAF_PATOLOGIS_BABINSKI === "t" &&
                                                tmphasil.SYARAF_PATOLOGIS_BUDZINZKY === "f" && tmphasil.SYARAF_PATOLOGIS_KERNIG === "t") {
                                            Ext.getCmp('cgPatologis').setValue([true, true, false, true]);
                                        }
                                    }
                                    else {
                                        Ext.getCmp('cgPatologis').setValue([false, false, false, false]);
                                    }
                                    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//tab6 -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

                                    if (tmphasil.INDRA_PUPIL_ISOKOR === "t" || tmphasil.INDRA__PUPIL_ANISOKOR === "t")
                                    {
                                        if (tmphasil.INDRA_PUPIL_ISOKOR === "t" && tmphasil.INDRA__PUPIL_ANISOKOR === "t") {
                                            Ext.getCmp('cgPupil').setValue([true, true]);
                                        } else if (tmphasil.INDRA_PUPIL_ISOKOR === "t" && tmphasil.INDRA__PUPIL_ANISOKOR === "f") {
                                            Ext.getCmp('cgPupil').setValue([true, false]);
                                        } else if (tmphasil.INDRA_PUPIL_ISOKOR === "f" && tmphasil.INDRA__PUPIL_ANISOKOR === "t")
                                        {
                                            Ext.getCmp('cgPupil').setValue([false, true]);
                                        }
                                    }
                                    else {
                                        Ext.getCmp('cgPupil').setValue([false, false]);
                                    }

                                    if (tmphasil.INDRA__KONJUNGTIVA_ANEMIS === "t" || tmphasil.INDRA__KONJUNGTIVA_ICTERUS === "t" || tmphasil.INDRA__KONJUNGTIVA_TIDAK === "t") {
                                        if (tmphasil.INDRA__KONJUNGTIVA_ANEMIS === "t" && tmphasil.INDRA__KONJUNGTIVA_ICTERUS === "t" && tmphasil.INDRA__KONJUNGTIVA_TIDAK === "t") {
                                            Ext.getCmp('cgKonjungtiva').setValue([true, true, true]);
                                        } else if (tmphasil.INDRA__KONJUNGTIVA_ANEMIS === "t" && tmphasil.INDRA__KONJUNGTIVA_ICTERUS === "f" && tmphasil.INDRA__KONJUNGTIVA_TIDAK === "f") {
                                            Ext.getCmp('cgKonjungtiva').setValue([true, false, false]);
                                        }
                                        else if (tmphasil.INDRA__KONJUNGTIVA_ANEMIS === "t" && tmphasil.INDRA__KONJUNGTIVA_ICTERUS === "t" && tmphasil.INDRA__KONJUNGTIVA_TIDAK === "f") {
                                            Ext.getCmp('cgKonjungtiva').setValue([true, true, false]);
                                        }
                                        else if (tmphasil.INDRA__KONJUNGTIVA_ANEMIS === "t" && tmphasil.INDRA__KONJUNGTIVA_ICTERUS === "f" && tmphasil.INDRA__KONJUNGTIVA_TIDAK === "t") {
                                            Ext.getCmp('cgKonjungtiva').setValue([true, false, true]);
                                        } else if (tmphasil.INDRA__KONJUNGTIVA_ANEMIS === "f" && tmphasil.INDRA__KONJUNGTIVA_ICTERUS === "t" && tmphasil.INDRA__KONJUNGTIVA_TIDAK === "f") {
                                            Ext.getCmp('cgKonjungtiva').setValue([false, true, false]);
                                        }
                                        else if (tmphasil.INDRA__KONJUNGTIVA_ANEMIS === "f" && tmphasil.INDRA__KONJUNGTIVA_ICTERUS === "t" && tmphasil.INDRA__KONJUNGTIVA_TIDAK === "t") {
                                            Ext.getCmp('cgKonjungtiva').setValue([false, true, true]);
                                        } else if (tmphasil.INDRA__KONJUNGTIVA_ANEMIS === "f" && tmphasil.INDRA__KONJUNGTIVA_ICTERUS === "f" && tmphasil.INDRA__KONJUNGTIVA_TIDAK === "t") {
                                            Ext.getCmp('cgKonjungtiva').setValue([false, false, true]);
                                        }
                                    }
                                    else {
                                        Ext.getCmp('cgKonjungtiva').setValue([false, false, false]);
                                    }

                                    if (tmphasil.INDRA_GANGGUAN_DENGAR === "t")
                                    {
                                        Ext.getCmp('rgGangguanPendengaran').setValue("rgGangguanPendengaranya", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgGangguanPendengaran').setValue("rgGangguanPendengarantidak", true);
                                    }

                                    if (tmphasil.INDRA_OTORHEA === "t")
                                    {
                                        Ext.getCmp('rgOtorhea').setValue("rgOtorheaya", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgOtorhea').setValue("rgOtorheatidak", true);
                                    }

                                    if (tmphasil.INDRA_BENTUK_HIDUNG === "t")
                                    {
                                        Ext.getCmp('rgBentukHidung').setValue("rgBentukHidungya", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgBentukHidung').setValue("rgBentukHidungtidak", true);
                                    }

                                    if (tmphasil.INDRA_GANGGUAN_CIUM === "t")
                                    {
                                        Ext.getCmp('rgGangguanPenciuman').setValue("rgGangguanPenciumanya", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgGangguanPenciuman').setValue("rgGangguanPenciumantidak", true);
                                    }

                                    if (tmphasil.INDRA_RHINORHEA === "t")
                                    {
                                        Ext.getCmp('rgRhinorhea').setValue("rgRhinorheaya", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgRhinorhea').setValue("rgRhinorheatidak", true);
                                    }

                                    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//tab7 -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                                    if (tmphasil.KEMIH_KEBERSIHAN === "t")
                                    {
                                        Ext.getCmp('rgKebersihan').setValue("rgKebersihanBersih", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgKebersihan').setValue("rgKebersihanKotor", true);
                                    }

                                    if (tmphasil.KEMIH_ALAT_BANTU === "t")
                                    {
                                        Ext.getCmp('rgAlatBantu').setValue("rgAlatBantuYa", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgAlatBantu').setValue("rgAlatBantuTidak", true);
                                    }

                                    if (tmphasil.KEMIH_KANDUNG_MEMBESAR === "t")
                                    {
                                        Ext.getCmp('rgMembesar').setValue("rgMembesarYa", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgMembesar').setValue("rgMembesarTidak", true);
                                    }

                                    if (tmphasil.KEMIH_NYERI_TEKAN === "t")
                                    {
                                        Ext.getCmp('rgNyeri').setValue("rgNyeriYa", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgNyeri').setValue("rgNyeriTidak", true);
                                    }

                                    if (tmphasil.KEMIH_GANGGUAN_ANURIA === "t" || tmphasil.KEMIH_GANGGUAN_ALIGURIA === "t" || tmphasil.KEMIH_GANGGUAN_RETENSI === "t" || tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "t" || tmphasil.KEMIH_GANGGUAN_DISURIA === "t" || tmphasil.KEMIH_GANGGUANHEMATURIA === "t")
                                    {
                                        if (tmphasil.KEMIH_GANGGUAN_ANURIA === "t" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "t" && tmphasil.KEMIH_GANGGUAN_RETENSI === "t" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "t" && tmphasil.KEMIH_GANGGUAN_DISURIA === "t" && tmphasil.KEMIH_GANGGUANHEMATURIA === "t") {
                                            Ext.getCmp('cgGanguankemih').setValue([true, true, true, true, true, true]);
                                        }                                         //1
                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "t" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "f" && tmphasil.KEMIH_GANGGUAN_RETENSI === "f" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "f" && tmphasil.KEMIH_GANGGUAN_DISURIA === "f" && tmphasil.KEMIH_GANGGUANHEMATURIA === "f") {
                                            Ext.getCmp('cgGanguankemih').setValue([true, false, false, false, false, false]);
                                        }
                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "f" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "t" && tmphasil.KEMIH_GANGGUAN_RETENSI === "f" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "f" && tmphasil.KEMIH_GANGGUAN_DISURIA === "f" && tmphasil.KEMIH_GANGGUANHEMATURIA === "f") {
                                            Ext.getCmp('cgGanguankemih').setValue([false, true, false, false, false, false]);
                                        }

                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "f" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "f" && tmphasil.KEMIH_GANGGUAN_RETENSI === "t" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "f" && tmphasil.KEMIH_GANGGUAN_DISURIA === "f" && tmphasil.KEMIH_GANGGUANHEMATURIA === "f") {
                                            Ext.getCmp('cgGanguankemih').setValue([false, false, true, false, false, false]);
                                        }

                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "f" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "f" && tmphasil.KEMIH_GANGGUAN_RETENSI === "f" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "t" && tmphasil.KEMIH_GANGGUAN_DISURIA === "f" && tmphasil.KEMIH_GANGGUANHEMATURIA === "f") {
                                            Ext.getCmp('cgGanguankemih').setValue([false, false, false, true, false, false]);
                                        }

                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "f" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "f" && tmphasil.KEMIH_GANGGUAN_RETENSI === "f" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "f" && tmphasil.KEMIH_GANGGUAN_DISURIA === "t" && tmphasil.KEMIH_GANGGUANHEMATURIA === "f") {
                                            Ext.getCmp('cgGanguankemih').setValue([false, false, false, false, true, false]);
                                        }

                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "f" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "f" && tmphasil.KEMIH_GANGGUAN_RETENSI === "f" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "f" && tmphasil.KEMIH_GANGGUAN_DISURIA === "f" && tmphasil.KEMIH_GANGGUANHEMATURIA === "t") {
                                            Ext.getCmp('cgGanguankemih').setValue([false, false, false, false, false, true]);
                                        }
                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "t" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "t" && tmphasil.KEMIH_GANGGUAN_RETENSI === "f" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "f" && tmphasil.KEMIH_GANGGUAN_DISURIA === "f" && tmphasil.KEMIH_GANGGUANHEMATURIA === "f") {
                                            Ext.getCmp('cgGanguankemih').setValue([true, true, false, false, false, false]);
                                        }

                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "t" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "t" && tmphasil.KEMIH_GANGGUAN_RETENSI === "t" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "f" && tmphasil.KEMIH_GANGGUAN_DISURIA === "f" && tmphasil.KEMIH_GANGGUANHEMATURIA === "f") {
                                            Ext.getCmp('cgGanguankemih').setValue([true, true, true, false, false, false]);
                                        }

                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "t" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "t" && tmphasil.KEMIH_GANGGUAN_RETENSI === "t" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "t" && tmphasil.KEMIH_GANGGUAN_DISURIA === "f" && tmphasil.KEMIH_GANGGUANHEMATURIA === "f") {
                                            Ext.getCmp('cgGanguankemih').setValue([true, true, true, true, false, false]);
                                        }

                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "t" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "t" && tmphasil.KEMIH_GANGGUAN_RETENSI === "t" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "t" && tmphasil.KEMIH_GANGGUAN_DISURIA === "t" && tmphasil.KEMIH_GANGGUANHEMATURIA === "f") {
                                            Ext.getCmp('cgGanguankemih').setValue([true, true, true, true, true, false]);
                                        }
                                        //2
                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "f" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "t" && tmphasil.KEMIH_GANGGUAN_RETENSI === "f" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "f" && tmphasi.lKEMIH_GANGGUAN_DISURIA === "f" && tmphasil.KEMIH_GANGGUANHEMATURIA === "f") {
                                            Ext.getCmp('cgGanguankemih').setValue([false, true, false, false, false, false]);
                                        }

                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "f" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "t" && tmphasil.KEMIH_GANGGUAN_RETENSI === "t" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "f" && tmphasil.KEMIH_GANGGUAN_DISURIA === "f" && tmphasil.KEMIH_GANGGUANHEMATURIA === "f") {
                                            Ext.getCmp('cgGanguankemih').setValue([false, true, true, false, false, false]);
                                        }

                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "f" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "t" && tmphasil.KEMIH_GANGGUAN_RETENSI === "t" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "t" && tmphasil.KEMIH_GANGGUAN_DISURIA === "f" && tmphasil.KEMIH_GANGGUANHEMATURIA === "f") {
                                            Ext.getCmp('cgGanguankemih').setValue([false, true, true, true, false, false]);
                                        }

                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "f" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "t" && tmphasil.KEMIH_GANGGUAN_RETENSI === "t" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "t" && tmphasil.KEMIH_GANGGUAN_DISURIA === "t" && tmphasil.KEMIH_GANGGUANHEMATURIA === "f") {
                                            Ext.getCmp('cgGanguankemih').setValue([false, true, true, true, true, false]);
                                        }

                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "f" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "t" && tmphasil.KEMIH_GANGGUAN_RETENSI === "t" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "t" && tmphasil.KEMIH_GANGGUAN_DISURIA === "t" && tmphasil.KEMIH_GANGGUANHEMATURIA === "t") {
                                            Ext.getCmp('cgGanguankemih').setValue([false, true, true, true, true, true]);
                                        }                                         //3
                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "f" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "f" && tmphasil.KEMIH_GANGGUAN_RETENSI === "t" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "f" && tmphasil.KEMIH_GANGGUAN_DISURIA === "f" && tmphasil.KEMIH_GANGGUANHEMATURIA === "f") {
                                            Ext.getCmp('cgGanguankemih').setValue([false, false, false, true, false, false]);
                                        }

                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "f" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "f" && tmphasil.KEMIH_GANGGUAN_RETENSI === "t" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "t" && tmphasil.KEMIH_GANGGUAN_DISURIA === "f" && tmphasil.KEMIH_GANGGUANHEMATURIA === "f") {
                                            Ext.getCmp('cgGanguankemih').setValue([false, false, true, true, false, false]);
                                        }

                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "f" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "f" && tmphasil.KEMIH_GANGGUAN_RETENSI === "t" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "t" && tmphasil.KEMIH_GANGGUAN_DISURIA === "t" && tmphasil.KEMIH_GANGGUANHEMATURIA === "f") {
                                            Ext.getCmp('cgGanguankemih').setValue([false, false, true, true, true, false]);
                                        }

                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "f" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "f" && tmphasil.KEMIH_GANGGUAN_RETENSI === "t" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "t" && tmphasil.KEMIH_GANGGUAN_DISURIA === "t" && tmphasil.KEMIH_GANGGUANHEMATURIA === "t") {
                                            Ext.getCmp('cgGanguankemih').setValue([false, false, true, true, true, true]);
                                        }
                                        //4
                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "f" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "f" && tmphasil.KEMIH_GANGGUAN_RETENSI === "f" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "t" && tmphasil.KEMIH_GANGGUAN_DISURIA === "f" && tmphasil.KEMIH_GANGGUANHEMATURIA === "f") {
                                            Ext.getCmp('cgGanguankemih').setValue([false, false, false, true, false, false]);
                                        }

                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "f" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "f" && tmphasil.KEMIH_GANGGUAN_RETENSI === "f" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "t" && tmphasil.KEMIH_GANGGUAN_DISURIA === "t" && tmphasil.KEMIH_GANGGUANHEMATURIA === "f") {
                                            Ext.getCmp('cgGanguankemih').setValue([false, false, false, true, true, false]);
                                        }

                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "f" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "f" && tmphasil.KEMIH_GANGGUAN_RETENSI === "f" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "t" && tmphasil.KEMIH_GANGGUAN_DISURIA === "t" && tmphasil.KEMIH_GANGGUANHEMATURIA === "t") {
                                            Ext.getCmp('cgGanguankemih').setValue([false, false, false, true, true, true]);
                                        }                                         //5
                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "f" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "f" && tmphasil.KEMIH_GANGGUAN_RETENSI === "f" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "f" && tmphasil.KEMIH_GANGGUAN_DISURIA === "t" && tmphasil.KEMIH_GANGGUANHEMATURIA === "f") {
                                            Ext.getCmp('cgGanguankemih').setValue([false, false, false, false, true, false]);
                                        }

                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "f" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "f" && tmphasil.KEMIH_GANGGUAN_RETENSI === "f" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "f" && tmphasil.KEMIH_GANGGUAN_DISURIA === "t" && tmphasil.KEMIH_GANGGUANHEMATURIA === "t") {
                                            Ext.getCmp('cgGanguankemih').setValue([false, false, false, false, true, true]);
                                        }
                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "t" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "f" && tmphasil.KEMIH_GANGGUAN_RETENSI === "f" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "f" && tmphasil.KEMIH_GANGGUAN_DISURIA === "t" && tmphasil.KEMIH_GANGGUANHEMATURIA === "t") {
                                            Ext.getCmp('cgGanguankemih').setValue([true, false, false, false, true, true]);
                                        }
                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "t" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "t" && tmphasil.KEMIH_GANGGUAN_RETENSI === "f" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "f" && tmphasil.KEMIH_GANGGUAN_DISURIA === "t" && tmphasil.KEMIH_GANGGUANHEMATURIA === "t") {
                                            Ext.getCmp('cgGanguankemih').setValue([true, true, false, false, true, true]);
                                        }
                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "t" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "t" && tmphasil.KEMIH_GANGGUAN_RETENSI === "t" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "f" && tmphasil.KEMIH_GANGGUAN_DISURIA === "t" && tmphasil.KEMIH_GANGGUANHEMATURIA === "t") {
                                            Ext.getCmp('cgGanguankemih').setValue([true, true, true, false, true, true]);
                                        }                                         //6
                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "f" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "f" && tmphasil.KEMIH_GANGGUAN_RETENSI === "f" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "f" && tmphasil.KEMIH_GANGGUAN_DISURIA === "f" && tmphasil.KEMIH_GANGGUANHEMATURIA === "t") {
                                            Ext.getCmp('cgGanguankemih').setValue([false, false, false, false, false, true]);
                                        }
                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "t" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "f" && tmphasil.KEMIH_GANGGUAN_RETENSI === "f" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "f" && tmphasil.KEMIH_GANGGUAN_DISURIA === "f" && tmphasil.KEMIH_GANGGUANHEMATURIA === "t") {
                                            Ext.getCmp('cgGanguankemih').setValue([true, false, false, false, false, true]);
                                        }
                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "t" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "t" && tmphasil.KEMIH_GANGGUAN_RETENSI === "f" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "f" && tmphasil.KEMIH_GANGGUAN_DISURIA === "f" && tmphasil.KEMIH_GANGGUANHEMATURIA === "t") {
                                            Ext.getCmp('cgGanguankemih').setValue([true, true, false, false, false, true]);
                                        }
                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "t" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "t" && tmphasil.KEMIH_GANGGUAN_RETENSI === "t" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "f" && tmphasil.KEMIH_GANGGUAN_DISURIA === "f" && tmphasil.KEMIH_GANGGUANHEMATURIA === "t") {
                                            Ext.getCmp('cgGanguankemih').setValue([true, true, true, false, false, true]);
                                        }

                                        else if (tmphasil.KEMIH_GANGGUAN_ANURIA === "t" && tmphasil.KEMIH_GANGGUAN_ALIGURIA === "t" && tmphasil.KEMIH_GANGGUAN_RETENSI === "t" && tmphasil.KEMIH_GANGGUAN_INKONTINENSIA === "t" && tmphasil.KEMIH_GANGGUAN_DISURIA === "f" && tmphasil.KEMIH_GANGGUANHEMATURIA === "t") {
                                            Ext.getCmp('cgGanguankemih').setValue([true, true, true, true, false, true]);
                                        }
                                    }
                                    else {
                                        Ext.getCmp('cgGanguankemih').setValue([false, false, false, false, false, false]);
                                    }
                                    //------------------------------------------------------------------------------------------------

//tab8
                                    if (tmphasil.CERNA_MAKAN_HABIS === "t")
                                    {
                                        Ext.getCmp('rgPorsiMakan').setValue("rgPorsiMakanHabis", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgPorsiMakan').setValue("rgPorsiMakanTidak", true);
                                    }

                                    if (tmphasil.CERNA_MULUT_BERSIH === "t" || tmphasil.CERNA_MULUT_KOTOR === "t" || tmphasil.CERNA_MULUT_BERBAU === "t")
                                    {
                                        if (tmphasil.CERNA_MULUT_BERSIH === "t" && tmphasil.CERNA_MULUT_KOTOR === "t" && tmphasil.CERNA_MULUT_BERBAU === "t") {
                                            Ext.getCmp('cgKondisiMulut').setValue([true, true, true]);
                                        } else if (tmphasil.CERNA_MULUT_BERSIH === "t" && tmphasil.CERNA_MULUT_KOTOR === "f" && tmphasil.CERNA_MULUT_BERBAU === "f") {
                                            Ext.getCmp('cgKondisiMulut').setValue([true, false, false]);
                                        }
                                        else if (tmphasil.CERNA_MULUT_BERSIH === "t" && tmphasil.CERNA_MULUT_KOTOR === "t" && tmphasil.CERNA_MULUT_BERBAU === "f") {
                                            Ext.getCmp('cgKondisiMulut').setValue([true, true, false]);
                                        }
                                        else if (tmphasil.CERNA_MULUT_BERSIH === "t" && tmphasil.CERNA_MULUT_KOTOR === "f" && tmphasil.CERNA_MULUT_BERBAU === "t") {
                                            Ext.getCmp('cgKondisiMulut').setValue([true, false, true]);
                                        } else if (tmphasil.CERNA_MULUT_BERSIH === "f" && tmphasil.CERNA_MULUT_KOTOR === "t" && tmphasil.CERNA_MULUT_BERBAU === "f")
                                        {
                                            Ext.getCmp('cgKondisiMulut').setValue([false, true, false]);
                                        }
                                        else if (tmphasil.CERNA_MULUT_BERSIH === "f" && tmphasil.CERNA_MULUT_KOTOR === "t" && tmphasil.CERNA_MULUT_BERBAU === "t")
                                        {
                                            Ext.getCmp('cgKondisiMulut').setValue([false, true, true]);
                                        } else if (tmphasil.CERNA_MULUT_BERSIH === "f" && tmphasil.CERNA_MULUT_KOTOR === "f" && tmphasil.CERNA_MULUT_BERBAU === "t")
                                        {
                                            Ext.getCmp('cgKondisiMulut').setValue([false, false, true]);
                                        }
                                    }
                                    else {
                                        Ext.getCmp('cgKondisiMulut').setValue([false, false, false]);
                                    }

                                    if (tmphasil.CERNA_MUKOSA_LEMBAB === "t" || tmphasil.CERNA_MUKOSA_KERING === "t" || tmphasil.CERNA_MUKOSA_STOMATITIS === "t")
                                    {
                                        if (tmphasil.CERNA_MUKOSA_LEMBAB === "t" && tmphasil.CERNA_MUKOSA_KERING === "t" && tmphasil.CERNA_MUKOSA_STOMATITIS === "t") {
                                            Ext.getCmp('cgMukosa').setValue([true, true, true]);
                                        } else if (tmphasil.CERNA_MUKOSA_LEMBAB === "t" && tmphasil.CERNA_MUKOSA_KERING === "f" && tmphasil.CERNA_MUKOSA_STOMATITIS === "f") {
                                            Ext.getCmp('cgMukosa').setValue([true, false, false]);
                                        }
                                        else if (tmphasil.CERNA_MUKOSA_LEMBAB === "t" && tmphasil.CERNA_MUKOSA_KERING === "t" && tmphasil.CERNA_MUKOSA_STOMATITIS === "f") {
                                            Ext.getCmp('cgMukosa').setValue([true, true, false]);
                                        }
                                        else if (tmphasil.CERNA_MUKOSA_LEMBAB === "t" && tmphasil.CERNA_MUKOSA_KERING === "f" && tmphasil.CERNA_MUKOSA_STOMATITIS === "t") {
                                            Ext.getCmp('cgMukosa').setValue([true, false, true]);
                                        } else if (tmphasil.CERNA_MUKOSA_LEMBAB === "f" && tmphasil.CERNA_MUKOSA_KERING === "t" && tmphasil.CERNA_MUKOSA_STOMATITIS === "f")
                                        {
                                            Ext.getCmp('cgMukosa').setValue([false, true, false]);
                                        }
                                        else if (tmphasil.CERNA_MUKOSA_LEMBAB === "f" && tmphasil.CERNA_MUKOSA_KERING === "t" && tmphasil.CERNA_MUKOSA_STOMATITIS === "t")
                                        {
                                            Ext.getCmp('cgMukosa').setValue([false, true, true]);
                                        } else if (tmphasil.CERNA_MUKOSA_LEMBAB === "f" && tmphasil.CERNA_MUKOSA_KERING === "f" && tmphasil.CERNA_MUKOSA_STOMATITIS === "t")
                                        {
                                            Ext.getCmp('cgMukosa').setValue([false, false, true]);
                                        }
                                    }
                                    else {
                                        Ext.getCmp('cgMukosa').setValue([false, false, false]);
                                    }

                                    if (tmphasil.CERNA_TURGOR_KULIT === "t")
                                    {
                                        Ext.getCmp('rgTurgor').setValue("rgTurgorBaik", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgPorsiMakan').setValue("rgTurgorJelek", true);
                                    }

                                    if (tmphasil.CERNA_TENGGOROKAN_SAKIT === "t" || tmphasil.CERNA_TENGGOROKAN_NYERI_TEKAN === "t" || tmphasil.CERNA_TENGGOROKAN_PEMBESARANTO === "t")
                                    {
                                        if (tmphasil.CERNA_TENGGOROKAN_SAKIT === "t" && tmphasil.CERNA_TENGGOROKAN_NYERI_TEKAN === "t" && tmphasil.CERNA_TENGGOROKAN_PEMBESARANTO === "t") {
                                            Ext.getCmp('cgTenggorokan').setValue([true, true, true]);
                                        } else if (tmphasil.CERNA_TENGGOROKAN_SAKIT === "t" && tmphasil.CERNA_TENGGOROKAN_NYERI_TEKAN === "f" && tmphasil.CERNA_TENGGOROKAN_PEMBESARANTO === "f") {
                                            Ext.getCmp('cgTenggorokan').setValue([true, false, false]);
                                        }
                                        else if (tmphasil.CERNA_TENGGOROKAN_SAKIT === "t" && tmphasil.CERNA_TENGGOROKAN_NYERI_TEKAN === "t" && tmphasil.CERNA_TENGGOROKAN_PEMBESARANTO === "f") {
                                            Ext.getCmp('cgTenggorokan').setValue([true, true, false]);
                                        }
                                        else if (tmphasil.CERNA_TENGGOROKAN_SAKIT === "t" && tmphasil.CERNA_TENGGOROKAN_NYERI_TEKAN === "f" && tmphasil.CERNA_TENGGOROKAN_PEMBESARANTO === "t") {
                                            Ext.getCmp('cgTenggorokan').setValue([true, false, true]);
                                        } else if (tmphasil.CERNA_TENGGOROKAN_SAKIT === "f" && tmphasil.CERNA_TENGGOROKAN_NYERI_TEKAN === "t" && tmphasil.CERNA_TENGGOROKAN_PEMBESARANTO === "f")
                                        {
                                            Ext.getCmp('cgTenggorokan').setValue([false, true, false]);
                                        }
                                        else if (tmphasil.CERNA_TENGGOROKAN_SAKIT === "f" && tmphasil.CERNA_TENGGOROKAN_NYERI_TEKAN === "t" && tmphasil.CERNA_TENGGOROKAN_PEMBESARANTO === "t")
                                        {
                                            Ext.getCmp('cgTenggorokan').setValue([false, true, true]);
                                        } else if (tmphasil.CERNA_TENGGOROKAN_SAKIT === "f" && tmphasil.CERNA_TENGGOROKAN_NYERI_TEKAN === "f" && tmphasil.CERNA_TENGGOROKAN_PEMBESARANTO === "t")
                                        {
                                            Ext.getCmp('cgTenggorokan').setValue([false, false, true]);
                                        }
                                    }
                                    else {
                                        Ext.getCmp('cgTenggorokan').setValue([false, false, false]);
                                    }

                                    if (tmphasil.CERNA_PERUT_NORMAL === "t" || tmphasil.CERNA_PERUT_DISTENDED === "t" || tmphasil.CERNA_PERUT_METEORISMUS === "t")
                                    {
                                        if (tmphasil.CERNA_PERUT_NORMAL === "t" && tmphasil.CERNA_PERUT_DISTENDED === "t" && tmphasil.CERNA_PERUT_METEORISMUS === "t") {
                                            Ext.getCmp('cgPerut').setValue([true, true, true]);
                                        } else if (tmphasil.CERNA_PERUT_NORMAL === "t" && tmphasil.CERNA_PERUT_DISTENDED === "f" && tmphasil.CERNA_PERUT_METEORISMUS === "f") {
                                            Ext.getCmp('cgPerut').setValue([true, false, false]);
                                        }
                                        else if (tmphasil.CERNA_PERUT_NORMAL === "t" && tmphasil.CERNA_PERUT_DISTENDED === "t" && tmphasil.CERNA_PERUT_METEORISMUS === "f") {
                                            Ext.getCmp('cgPerut').setValue([true, true, false]);
                                        }
                                        else if (tmphasil.CERNA_PERUT_NORMAL === "t" && tmphasil.CERNA_PERUT_DISTENDED === "f" && tmphasil.CERNA_PERUT_METEORISMUS === "t") {
                                            Ext.getCmp('cgPerut').setValue([true, false, true]);
                                        } else if (tmphasil.CERNA_PERUT_NORMAL === "f" && tmphasil.CERNA_PERUT_DISTENDED === "t" && tmphasil.CERNA_PERUT_METEORISMUS === "f")
                                        {
                                            Ext.getCmp('cgPerut').setValue([false, true, false]);
                                        }
                                        else if (tmphasil.CERNA_PERUT_NORMAL === "f" && tmphasil.CERNA_PERUT_DISTENDED === "t" && tmphasil.CERNA_PERUT_METEORISMUS === "t")
                                        {
                                            Ext.getCmp('cgPerut').setValue([false, true, true]);
                                        } else if (tmphasil.CERNA_PERUT_NORMAL === "f" && tmphasil.CERNA_PERUT_DISTENDED === "f" && tmphasil.CERNA_PERUT_METEORISMUS === "t")
                                        {
                                            Ext.getCmp('cgPerut').setValue([false, false, true]);
                                        }
                                    }
                                    else {
                                        Ext.getCmp('cgPerut').setValue([false, false, false]);
                                    }

                                    if (tmphasil.CERNA_PEMBESARAN_HEPAR === "t")
                                    {
                                        Ext.getCmp('rgPembesaranTonsil').setValue("rgPembesaranTonsilYa", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgPembesaranTonsil').setValue("rgPembesaranTonsilTidak", true);
                                    }

                                    if (tmphasil.CERNA_PERUT_NYERI_TEKAN === "t")
                                    {
                                        Ext.getCmp('cgNyeriTekan').setValue([true]);
                                    } else
                                    {
                                        Ext.getCmp('cgNyeriTekan').setValue([true]);
                                    }

                                    if (tmphasil.CERNA_HEMATEMESIS === "t")
                                    {
                                        Ext.getCmp('rgHematemesis').setValue("rgHematemesisYa", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgHematemesis').setValue("rgHematemesisTidak", true);
                                    }

                                    if (tmphasil.CERNA_EVAKUASI_CAIRAN_ASCITES === "t")
                                    {
                                        Ext.getCmp('rgEvakuasiCairan').setValue("rgEvakuasiCairanYa", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgEvakuasiCairan').setValue("rgEvakuasiCairanTidak", true);
                                    }
                                    //------------------------------------------------------------------------------------------------

//tab9
                                    if (tmphasil.ENDOKRIN_TYROID === "t")
                                    {
                                        Ext.getCmp('rgTyroid').setValue("rgTyroidMembesar", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgTyroid').setValue("rgTyroidTidak", true);
                                    }

                                    if (tmphasil.ENDOKRIN_HIPOGLIKEMIA === "t")
                                    {
                                        Ext.getCmp('rgHyper').setValue("rgHyperYa", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgHyper').setValue("rgHyperTidak", true);
                                    }

                                    if (tmphasil.ENDOKRIN_LUKA_GANGREN === "t")
                                    {
                                        Ext.getCmp('rgGangren').setValue("rgGangrenYa", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgGangren').setValue("rgGangrenTidak", true);
                                    }

                                    if (tmphasil.ENDOKRIN__PUS === "t")
                                    {
                                        Ext.getCmp('rgPus').setValue("rgPusYa", true);
                                    }
                                    else
                                    {
                                        Ext.getCmp('rgPus').setValue("rgPusTidak", true);
                                    }
                                    //------------------------------------------------------------------------------------------------
//end

                                    //textfield

                                    //tab1
                                    Ext.getCmp('txtkeluhanutama').setValue(tmphasil.RIWAYAT_UTAMA);
                                    Ext.getCmp('txtriwayatpenyakitsekarang').setValue(tmphasil.RIWAYAT_PENYAKIT_SEKARANG);
                                    Ext.getCmp('txtriwayatpenyakitTerdahulu').setValue(tmphasil.RIWAYAT_PENYAKIT_DAHULU);
                                    //-----------------------------------------------------------------------------------------------

                                    //tab2
                                    Ext.getCmp('txtJenisRR').setValue(tmphasil.NAFAS_RR);
                                    Ext.getCmp('txtjumlahcairan').setValue(tmphasil.NAFAS_JML_CAIRAN);
                                    Ext.getCmp('txtwarnacairan').setValue(tmphasil.NAFAS_WARNA_CAIRAN);
                                    Ext.getCmp('txtmasalahairway').setValue(tmphasil.NAFAS_MASALAH);
                                    //-----------------------------------------------------------------------------------------------

                                    //tab3
                                    Ext.getCmp('txtpenjelasannyeridada').setValue(tmphasil.JANTUNG_NYERI_DADA_KET);
                                    Ext.getCmp('txtukuranJVP').setValue(tmphasil.JANTUNG_UKURAN_CVP);
                                    Ext.getCmp('txttekanandarah').setValue(tmphasil.JANTUNG_TEKANAN_DARAH);
                                    Ext.getCmp('txtNadi').setValue(tmphasil.JANTUNG_NADI);
                                    Ext.getCmp('txtTemp').setValue(tmphasil.JANTUNG_TEMP);
                                    Ext.getCmp('txtmasalahCirculation').setValue(tmphasil.JANTUNG_MASALAH);
                                    //-----------------------------------------------------------------------------------------------

                                    //tab4
                                    Ext.getCmp('txtkekuatanotot').setValue(tmphasil.OTOT_KEKUATAN);
                                    Ext.getCmp('txtmasalahMusculo').setValue(tmphasil.OTOT_MASALAH);
                                    //-----------------------------------------------------------------------------------------------

                                    //tab5
                                    Ext.getCmp('txttotalGCS').setValue(tmphasil.SYARAF_GCS_TOTAL);
                                    Ext.getCmp('txtmasalahSaraf').setValue(tmphasil.SYARAF_MASALAH);
                                    //-----------------------------------------------------------------------------------------------

                                    //tab6
                                    Ext.getCmp('txtjelaskanpengindraan').setValue(tmphasil.INDRA_BENTUK_HIDUNG_KET);
                                    Ext.getCmp('trmasalahpengindraan').setValue(tmphasil.INDRA_MASALAH);
                                    //------------------------------------------------------------------------------------------------
                                    //tab7
                                    Ext.getCmp('txtjumlahurin').setValue(tmphasil.KEMIH_JML_URINE);
                                    Ext.getCmp('txtwarnaurin').setValue(tmphasil.KEMIH_WARNA_URINE);
                                    Ext.getCmp('txtbauurin').setValue(tmphasil.KEMIH_BAU);
                                    Ext.getCmp('trmasalahperkemihan').setValue(tmphasil.KEMIH_MASALAH);
                                    //------------------------------------------------------------------------------------------------

                                    //tab8
                                    Ext.getCmp('txtporsi').setValue(tmphasil.CERNA_MAKAN_KET);
                                    Ext.getCmp('txtFrekuensimakan').setValue(tmphasil.CERNA_MAKAN_FREKUENSI);
                                    Ext.getCmp('txtMinum').setValue(tmphasil.CERNA_JML_MINUM);
                                    Ext.getCmp('txtjenisMinuman').setValue(tmphasil.CERNA_JENIS_MINUM);
                                    Ext.getCmp('txtParistaltik').setValue(tmphasil.CERNA_PERISTALTIK);
                                    Ext.getCmp('txtlokasisakit').setValue(tmphasil.CERNA_PERUT_LOKASI_NYERI);
                                    Ext.getCmp('txtJumlahcairan').setValue(tmphasil.CERNA_JML_CAIRAN_ASCITES);
                                    Ext.getCmp('txtjumlahBAB').setValue(tmphasil.CERNA_FREK_BAB);
                                    Ext.getCmp('txtKonsintensiBAB').setValue(tmphasil.CERNA_KONSISTENSI);
                                    Ext.getCmp('txtWarnaBAB').setValue(tmphasil.CERNA_WARNA_BAB);
                                    Ext.getCmp('txtBauBAB').setValue(tmphasil.CERNA_BAU_BAB);
                                    Ext.getCmp('txtWarnaCairan').setValue(tmphasil.CERNA_WARNA_CAIRAN_ASCITES);
                                    Ext.getCmp('trmasalahpencernaan').setValue(tmphasil.CERNA_MASALAH);
                                    //------------------------------------------------------------------------------------------------
                                    //tab9
                                    Ext.getCmp('trmasalahendokrin').setValue(tmphasil.ENDOKRIN_MASALAH);
                                    //------------------------------------------------------------------------------------------------
                                    //tab10
                                    Ext.getCmp('txtmandi').setValue(tmphasil.PERSONAL_MANDI);
                                    Ext.getCmp('txtKeramas').setValue(tmphasil.PERSONAL_KERAMAS);
                                    Ext.getCmp('txtSikatGigi').setValue(tmphasil.PERSONAL_SIKATGIGI);
                                    Ext.getCmp('txtGantiPakaian').setValue(tmphasil.PERSONAL_GANTIPAKAIAN);
                                    Ext.getCmp('trMasalahKebersihan').setValue(tmphasil.PERSONAL_MASALAH);
                                    //------------------------------------------------------------------------------------------------
                                    //tab11
                                    Ext.getCmp('trorangyangdekat').setValue(tmphasil.PSIKOSOSIAL_ORANGDEKAT);
                                    Ext.getCmp('trIbadah').setValue(tmphasil.PSIKOSOSIAL_KEGIATAN_IBADAH);
                                    Ext.getCmp('trMasalahSosial').setValue(tmphasil.PSIKOSOSIAL_MASALAH);
                                    //------------------------------------------------------------------------------------------------
                                    //tab12
                                    Ext.getCmp('trterapipenunjang').setValue(tmphasil.TERAPI_PENUNJANG);
                                    //------------------------------------------------------------------------------------------------
                                    //end

                                    //panjang cuy
                                }
                                else {
                                    //tab 1
                                    Ext.getCmp('txtkeluhanutama').setValue('');
                                    Ext.getCmp('txtriwayatpenyakitsekarang').setValue('');
                                    Ext.getCmp('txtriwayatpenyakitTerdahulu').setValue('');
                                    //-----------------------------------------------------------------------------------------------

                                    //tab2
                                    Ext.getCmp('txtJenisRR').setValue(0);
                                    Ext.getCmp('txtjumlahcairan').setValue(0);
                                    Ext.getCmp('txtwarnacairan').setValue('');
                                    Ext.getCmp('txtmasalahairway').setValue('');
                                    Ext.getCmp('rgjelas').setValue("rbjelasya", false);
                                    Ext.getCmp('rgjelas').setValue("rbjelastidak", false);
                                    Ext.getCmp('rgefakuasi').setValue("rbefakuasiya", false);
                                    Ext.getCmp('rgefakuasi').setValue("rbefakuasitidak", false);
                                    Ext.getCmp('cgjelannapas').setValue([false, false]);
                                    Ext.getCmp('cgpolanapas').setValue([false, false]);
                                    Ext.getCmp('cgsuaralapangparu').setValue([false, false, false]);
                                    Ext.getCmp('cgjenis').setValue([false, false, false, false, false, false]);
                                    Ext.getCmp('cgsuaranafastambahan').setValue([false, false, false]);
                                    //--------------------------------------------------------------------

                                    //tab3
                                    Ext.getCmp('txtpenjelasannyeridada').setValue('');
                                    Ext.getCmp('txttekanandarah').setValue('');
                                    Ext.getCmp('txtNadi').setValue('');
                                    Ext.getCmp('txtTemp').setValue(0);
                                    Ext.getCmp('txtukuranJVP').setValue(0);
                                    Ext.getCmp('txtmasalahCirculation').setValue('');
                                    Ext.getCmp('rgS1S2Tunggal').setValue("rbS1S2Tunggalya", false);
                                    Ext.getCmp('rgS1S2Tunggal').setValue("rbS1S2Tunggaltidak", false);
                                    Ext.getCmp('rgNyeridada').setValue("rgNyeridadaya", false);
                                    Ext.getCmp('rgNyeridada').setValue("rgNyeridadatidak", false);
                                    Ext.getCmp('rgCRT').setValue("rgCRTkurang", false);
                                    Ext.getCmp('rgCRT').setValue("rgCRTlebih", false);
                                    Ext.getCmp('rgJVP').setValue("rgJVPya", false);
                                    Ext.getCmp('rgJVP').setValue("rgJVPtidak", false);
                                    Ext.getCmp('cgIramaJantung').setValue([false, false]);
                                    Ext.getCmp('cgBunyiJantungTambahan').setValue([false, false, false]);
                                    Ext.getCmp('cgAkral').setValue([false, false]);
                                    Ext.getCmp('cgWarnaKulit').setValue([false, false, false, false]);
                                    //---------------------------------------------------------------------------

                                    //tab3
                                    Ext.getCmp('txtkekuatanotot').setValue('');
                                    Ext.getCmp('txtmasalahMusculo').setValue('');
                                    Ext.getCmp('cgPergerakanSendi').setValue([false, false]);
                                    Ext.getCmp('rgekstrimitas').setValue("rgekstrimitasya", false);
                                    Ext.getCmp('rgekstrimitas').setValue("rgekstrimitastidak", false);
                                    Ext.getCmp('rgKelainan').setValue("rgKelainanya", false);
                                    Ext.getCmp('rgKelainan').setValue("rgKelainantidak", false);
                                    Ext.getCmp('rgKrepitas').setValue("rgKrepitasya", false);
                                    Ext.getCmp('rgKrepitas').setValue("rgKrepitastidak", false);
//                                    ------------------------------------------------------------------------------

                                    //tab4
                                    Ext.getCmp('txttotalGCS').setValue(0);
                                    Ext.getCmp('txtmasalahSaraf').setValue('');
                                    Ext.getCmp('cgGCS').setValue([false, false, false]);
                                    Ext.getCmp('cgRefleksi').setValue([false, false, false]);
                                    Ext.getCmp('cgPatologis').setValue([false, false, false, false]);
//                                    -----------------------------------------------------------------------------------
                                    Ext.getCmp('cgPupil').setValue([false, false]);
                                    Ext.getCmp('cgKonjungtiva').setValue([false, false, false]);
                                    Ext.getCmp('rgGangguanPendengaran').setValue("rgGangguanPendengaranya", false);
                                    Ext.getCmp('rgGangguanPendengaran').setValue("rgGangguanPendengarantidak", false);
                                    Ext.getCmp('rgOtorhea').setValue("rgOtorheaya", false);
                                    Ext.getCmp('rgOtorhea').setValue("rgOtorheatidak", false);
                                    Ext.getCmp('rgBentukHidung').setValue("rgBentukHidungya", false);
                                    Ext.getCmp('rgBentukHidung').setValue("rgBentukHidungtidak", false);
                                    Ext.getCmp('rgGangguanPenciuman').setValue("rgGangguanPenciumanya", false);
                                    Ext.getCmp('rgGangguanPenciuman').setValue("rgGangguanPenciumantidak", false);
                                    Ext.getCmp('txtjelaskanpengindraan').setValue('');
                                    Ext.getCmp('rgRhinorhea').setValue("rgRhinorheaya", false);
                                    Ext.getCmp('rgRhinorhea').setValue("rgRhinorheatidak", false);
                                    Ext.getCmp('trmasalahpengindraan').setValue('');
                                    Ext.getCmp('rgKebersihan').setValue("rgKebersihanBersih", false);
                                    Ext.getCmp('rgKebersihan').setValue("rgKebersihanKotor", false);
                                    Ext.getCmp('rgAlatBantu').setValue("rgAlatBantuYa", false);
                                    Ext.getCmp('rgAlatBantu').setValue("rgAlatBantuTidak", false);
                                    Ext.getCmp('txtjumlahurin').setValue(0);
                                    Ext.getCmp('txtwarnaurin').setValue('');
                                    Ext.getCmp('txtbauurin').setValue('');
                                    Ext.getCmp('rgMembesar').setValue("rgMembesarYa", false);
                                    Ext.getCmp('rgMembesar').setValue("rgMembesarTidak", false);
                                    Ext.getCmp('rgNyeri').setValue("rgNyeriYa", false);
                                    Ext.getCmp('rgNyeri').setValue("rgNyeriYa", false);
                                    Ext.getCmp('cgGanguankemih').setValue([false, false, false, false, false, false]);
                                    Ext.getCmp('trmasalahperkemihan').setValue('');
                                    Ext.getCmp('rgPorsiMakan').setValue("rgPorsiMakanHabis", false);
                                    Ext.getCmp('rgPorsiMakan').setValue("rgPorsiMakanTidak", false);
                                    Ext.getCmp('txtporsi').setValue(0);
                                    Ext.getCmp('txtFrekuensimakan').setValue('');
                                    Ext.getCmp('txtMinum').setValue(0);
                                    Ext.getCmp('txtjenisMinuman').setValue('');
                                    Ext.getCmp('txtParistaltik').setValue(0);
                                    Ext.getCmp('cgKondisiMulut').setValue([false, false, false]);
                                    Ext.getCmp('cgMukosa').setValue([false, false, false]);
                                    Ext.getCmp('rgTurgor').setValue("rgTurgorBaik", false);
                                    Ext.getCmp('rgTurgor').setValue("rgTurgorJelek", false);
                                    Ext.getCmp('cgTenggorokan').setValue([false, false, false]);
                                    Ext.getCmp('cgPerut').setValue([false, false, false]);
                                    Ext.getCmp('rgPembesaranTonsil').setValue("rgPembesaranTonsilYa", false);
                                    Ext.getCmp('rgPembesaranTonsil').setValue("rgPembesaranTonsilTidak", false);
                                    Ext.getCmp('cgNyeriTekan').setValue([false]);
                                    Ext.getCmp('txtlokasisakit').setValue('');
                                    Ext.getCmp('rgHematemesis').setValue("rgHematemesisYa", false);
                                    Ext.getCmp('rgHematemesis').setValue("rgHematemesisTidak", false);
                                    Ext.getCmp('txtKonsintensiBAB').setValue('');
                                    Ext.getCmp('txtjumlahBAB').setValue(0);
                                    Ext.getCmp('txtWarnaBAB').setValue('');
                                    Ext.getCmp('txtBauBAB').setValue('');
                                    Ext.getCmp('rgEvakuasiCairan').setValue("rgEvakuasiCairanYa", false);
                                    Ext.getCmp('rgEvakuasiCairan').setValue("rgEvakuasiCairanTidak", false);
                                    Ext.getCmp('txtJumlahcairan').setValue(0);
                                    Ext.getCmp('txtWarnaCairan').setValue('');
                                    Ext.getCmp('trmasalahpencernaan').setValue('');
                                    Ext.getCmp('rgTyroid').setValue("rgTyroidMembesar", false);
                                    Ext.getCmp('rgTyroid').setValue("rgTyroidTidak", false);
                                    Ext.getCmp('rgHyper').setValue("rgHyperYa", false);
                                    Ext.getCmp('rgHyper').setValue("rgHyperTidak", false);
                                    Ext.getCmp('rgGangren').setValue("rgGangrenYa", false);
                                    Ext.getCmp('rgGangren').setValue("rgGangrenTidak", false);
                                    Ext.getCmp('rgPus').setValue("rgPusYa", false);
                                    Ext.getCmp('rgPus').setValue("rgPusTidak", false);
                                    Ext.getCmp('txtmasalahMusculo').setValue('');
                                    Ext.getCmp('txtmandi').setValue('');
                                    Ext.getCmp('txtSikatGigi').setValue('');
                                    Ext.getCmp('txtKeramas').setValue('');
                                    Ext.getCmp('txtGantiPakaian').setValue('');
                                    Ext.getCmp('trMasalahKebersihan').setValue('');
                                    Ext.getCmp('trorangyangdekat').setValue('');
                                    Ext.getCmp('trIbadah').setValue('');
                                    Ext.getCmp('trMasalahSosial').setValue('');
                                    Ext.getCmp('trterapipenunjang').setValue('');
                                    //1                                     
                                    RIWAYAT_UTAMA = '';
                                    RIWAYAT_PENYAKIT_SEKARANG = '';
                                    RIWAYAT_PENYAKIT_DAHULU = '';
                                    //2
                                    NAFAS_PATEN = 'false';
                                    NAFAS_OBSTRUKTIF = 'false';
                                    NAFAS_JELAS = 'false';
                                    NAFAS_POLA_SIMETRI = 'false';
                                    NAFAS_POLA_ASIMETRI = 'false';
                                    NAFAS_SUARA_NORMAL = 'false';
                                    NAFAS_SUARA_HIPERSONOR = 'false';
                                    NAFAS_MENURUN = 'false';
                                    NAFAS_JENIS_NORMAL = 'false';
                                    NAFAS_JENIS_TACHYPNOE = 'false';
                                    NAFAS_JENIS_CHEYNESTOKES = 'false';
                                    NAFAS_JENIS_RETRACTIVE = 'false';
                                    NAFAS_JENIS_KUSMAUL = 'false';
                                    NAFAS_JENIS_DISPNOE = 'false';
                                    NAFAS_RR = 0;
                                    NAFAS_SUARA_WHEEZING = 'false';
                                    NAFAS_SUARA_RONCHI = 'false';
                                    NAFAS_SUARA_RALES = 'false';
                                    NAFAS_EVAKUASI_CAIRAN = 'false';
                                    NAFAS_JML_CAIRAN = 0;
                                    NAFAS_WARNA_CAIRAN = '';
                                    NAFAS_MASALAH = '';
                                    //3
                                    JANTUNG_REGULER = 'false';
                                    JANTUNG_IRREGULER = 'false';
                                    JANTUNG_S1S2_TUNGGAL = 'false';
                                    JANTUNG_NYERI_DADA = 'false';
                                    JANTUNG_NYERI_DADA_KET = '';
                                    JANTUNG_BUNYI_MURMUR = 'false';
                                    JANTUNG_BUNYI_GALLOP = 'false';
                                    JANTUNG_BUNYI_BISING = 'false';
                                    JANTUNG_CRT = 'false';
                                    JANTUNG_AKRAL_HANGAT = 'false';
                                    JANTUNG_AKRAL_DINGIN = 'false';
                                    JANTUNG_PENINGKATAN_JVP = 'false';
                                    JANTUNG_UKURAN_CVP = 0;
                                    JANTUNG_WARNA_JAUNDICE = 'false';
                                    JANTUNG_WARNA_SIANOTIK = 'false';
                                    JANTUNG_WARNA_KEMERAHAN = 'false';
                                    JANTUNG_WARNA_PUCAT = 'false';
                                    JANTUNG_TEKANAN_DARAH = '';
                                    JANTUNG_NADI = '';
                                    JANTUNG_TEMP = 0;
                                    JANTUNG_MASALAH = '';
                                    //4
                                    OTOT_SENDI_BEBAS = 'false';
                                    OTOT_SENDI_TERBATAS = 'false';
                                    OTOT_KEKUATAN = '';
                                    OTOT_ODEMA_EKSTRIMITAS = 'false';
                                    OTOT_KELAINAN_BENTUK = 'false';
                                    OTOT_KREPITASI = 'false';
                                    OTOT_MASALAH = '';
                                    //5
                                    SYARAF_GCS_EYE = 'false';
                                    SYARAF_GCS_VERBAL = 'false';
                                    SYARAF_GCS_MOTORIK = 'false';
                                    SYARAF_GCS_TOTAL = 0;
                                    SYARAF_FISIOLOGIS_BRACHIALIS = 'false';
                                    SYARAF_FISIOLOGIS_PATELLA = 'false';
                                    SYARAF_FISIOLOGIS_ACHILLES = 'false';
                                    SYARAF_PATOLOGIS_CHODDOKS = 'false';
                                    SYARAF_PATOLOGIS_BABINSKI = 'false';
                                    SYARAF_PATOLOGIS_BUDZINZKY = 'false';
                                    SYARAF_PATOLOGIS_KERNIG = 'false';
                                    SYARAF_MASALAH = '';
                                    INDRA_PUPIL_ISOKOR = 'false';
                                    INDRA__PUPIL_ANISOKOR = 'false';
                                    INDRA__KONJUNGTIVA_ANEMIS = 'false';
                                    INDRA__KONJUNGTIVA_ICTERUS = 'false';
                                    INDRA__KONJUNGTIVA_TIDAK = 'false';
                                    INDRA_GANGGUAN_DENGAR = 'false';
                                    INDRA_OTORHEA = 'false';
                                    INDRA_BENTUK_HIDUNG = 'false';
                                    INDRA_BENTUK_HIDUNG_KET = '';
                                    INDRA_GANGGUAN_CIUM = 'false';
                                    INDRA_RHINORHEA = 'false';
                                    INDRA_MASALAH = '';
                                    KEMIH_KEBERSIHAN = 'false';
                                    KEMIH_ALAT_BANTU = 'false';
                                    KEMIH_JML_URINE = 0;
                                    KEMIH_WARNA_URINE = '';
                                    KEMIH_BAU = '';
                                    KEMIH_KANDUNG_MEMBESAR = 'false';
                                    KEMIH_NYERI_TEKAN = 'false';
                                    KEMIH_GANGGUAN_ANURIA = 'false';
                                    KEMIH_GANGGUAN_ALIGURIA = 'false';
                                    KEMIH_GANGGUAN_RETENSI = 'false';
                                    KEMIH_GANGGUAN_INKONTINENSIA = 'false';
                                    KEMIH_GANGGUAN_DISURIA = 'false';
                                    KEMIH_GANGGUANHEMATURIA = 'false';
                                    KEMIH_MASALAH = '';
                                    CERNA_MAKAN_HABIS = 'false';
                                    CERNA_MAKAN_KET = '';
                                    CERNA_MAKAN_FREKUENSI = 0;
                                    CERNA_JML_MINUM = 0;
                                    CERNA_JENIS_MINUM = '';
                                    CERNA_MULUT_BERSIH = 'false';
                                    CERNA_MULUT_KOTOR = 'false';
                                    CERNA_MULUT_BERBAU = 'false';
                                    CERNA_MUKOSA_LEMBAB = 'false';
                                    CERNA_MUKOSA_KERING = 'false';
                                    CERNA_MUKOSA_STOMATITIS = 'false';
                                    CERNA_TENGGOROKAN_SAKIT = 'false';
                                    CERNA_TENGGOROKAN_NYERI_TEKAN = 'false';
                                    CERNA_TENGGOROKAN_PEMBESARANTO = 'false';
                                    CERNA_PERUT_NORMAL = 'false';
                                    CERNA_PERUT_DISTENDED = 'false';
                                    CERNA_PERUT_METEORISMUS = 'false';
                                    CERNA_PERUT_NYERI_TEKAN = 'false';
                                    CERNA_PERUT_LOKASI_NYERI = '';
                                    CERNA_PERISTALTIK = 0;
                                    CERNA_TURGOR_KULIT = 'false';
                                    CERNA_PEMBESARAN_HEPAR = 'false';
                                    CERNA_HEMATEMESIS = 'false';
                                    CERNA_EVAKUASI_CAIRAN_ASCITES = 'false';
                                    CERNA_JML_CAIRAN_ASCITES = 0;
                                    CERNA_WARNA_CAIRAN_ASCITES = 'false';
                                    CERNA_FREK_BAB = 0;
                                    CERNA_KONSISTENSI = '';
                                    CERNA_BAU_BAB = '';
                                    CERNA_WARNA_BAB = '';
                                    CERNA_MASALAH = '';
                                    ENDOKRIN_TYROID = 'false';
                                    ENDOKRIN_HIPOGLIKEMIA = 'false';
                                    ENDOKRIN_LUKA_GANGREN = 'false';
                                    ENDOKRIN__PUS = 'false';
                                    ENDOKRIN_MASALAH = '';
                                    PERSONAL_MANDI = '';
                                    PERSONAL_SIKATGIGI = '';
                                    PERSONAL_KERAMAS = '';
                                    PERSONAL_GANTIPAKAIAN = '';
                                    PERSONAL_MASALAH = '';
                                    PSIKOSOSIAL_ORANGDEKAT = '';
                                    PSIKOSOSIAL_KEGIATAN_IBADAH = '';
                                    PSIKOSOSIAL_MASALAH = '';
                                    TERAPI_PENUNJANG = '';
                                }
                            }
                        }
                    }
            );
}

function SavingData()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/CreateDataObj",
                        params: paramsaving(),
                        failure: function (o)
                        {
                            ShowPesanWarningPengkajian('Proses Saving Tidak berhasil silahkan hubungi admin', 'Gagal');
                            caridatapasien();
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                ShowPesanInfoPengkajian('Proses Saving Berhasil', 'Save');
                                caridatapasien();
                                Ext.getCmp('btnSimpan_viDaftar').disable();
                                tmpediting = 'false';
                            }
                            else
                            {
                                ShowPesanWarningPengkajian('Proses Saving Tidak berhasil silahkan hubungi admin', 'Gagal');
                                caridatapasien();
                                tmpediting = 'false';
                            }
                            ;
                        }
                    }
            );
}
;
function paramsaving()
{

    var params =
            {
                Table: 'ViewTrKasirIGD',
                // Table: 'ViewAskepPengkajian',
                query: "kd_pasien_kunj = '" + tmpkdpasien + "' and kd_unit_kunj = '" + tmpkdunit + "' and urut_masuk_kunj = " + tmpurutmasuk + " and tgl_masuk_kunj = '" + tmptglmasuk + "'",
                KD_PASIEN_KUNJ: tmpkdpasien, KD_UNIT_KUNJ: tmpkdunit,
                URUT_MASUK_KUNJ: tmpurutmasuk,
                TGL_MASUK_KUNJ: tmptglmasuk,
                RIWAYAT_UTAMA: Ext.get('txtkeluhanutama').getValue(),
                RIWAYAT_PENYAKIT_SEKARANG: Ext.get('txtriwayatpenyakitsekarang').getValue(),
                RIWAYAT_PENYAKIT_DAHULU: Ext.get('txtriwayatpenyakitTerdahulu').getValue(),
                NAFAS_PATEN: NAFAS_PATEN,
                NAFAS_OBSTRUKTIF: NAFAS_OBSTRUKTIF,
                NAFAS_JELAS: NAFAS_JELAS,
                NAFAS_POLA_SIMETRI: NAFAS_POLA_SIMETRI,
                NAFAS_POLA_ASIMETRI: NAFAS_POLA_ASIMETRI,
                NAFAS_SUARA_NORMAL: NAFAS_SUARA_NORMAL,
                NAFAS_SUARA_HIPERSONOR: NAFAS_SUARA_HIPERSONOR,
                NAFAS_MENURUN: NAFAS_MENURUN,
                NAFAS_JENIS_NORMAL: NAFAS_JENIS_NORMAL,
                NAFAS_JENIS_TACHYPNOE: NAFAS_JENIS_TACHYPNOE,
                NAFAS_JENIS_CHEYNESTOKES: NAFAS_JENIS_CHEYNESTOKES,
                NAFAS_JENIS_RETRACTIVE: NAFAS_JENIS_RETRACTIVE,
                NAFAS_JENIS_KUSMAUL: NAFAS_JENIS_KUSMAUL,
                NAFAS_JENIS_DISPNOE: NAFAS_JENIS_DISPNOE,
                NAFAS_RR: NAFAS_RR,
                NAFAS_SUARA_WHEEZING: NAFAS_SUARA_WHEEZING,
                NAFAS_SUARA_RONCHI: NAFAS_SUARA_RONCHI,
                NAFAS_SUARA_RALES: NAFAS_SUARA_RALES,
                NAFAS_EVAKUASI_CAIRAN: NAFAS_EVAKUASI_CAIRAN,
                NAFAS_JML_CAIRAN: NAFAS_JML_CAIRAN,
                NAFAS_WARNA_CAIRAN: NAFAS_WARNA_CAIRAN,
                NAFAS_MASALAH: NAFAS_MASALAH,
                JANTUNG_REGULER: JANTUNG_REGULER,
                JANTUNG_IRREGULER: JANTUNG_IRREGULER,
                JANTUNG_S1S2_TUNGGAL: JANTUNG_S1S2_TUNGGAL,
                JANTUNG_NYERI_DADA: JANTUNG_NYERI_DADA,
                JANTUNG_NYERI_DADA_KET: JANTUNG_NYERI_DADA_KET,
                JANTUNG_BUNYI_MURMUR: JANTUNG_BUNYI_MURMUR,
                JANTUNG_BUNYI_GALLOP: JANTUNG_BUNYI_GALLOP,
                JANTUNG_BUNYI_BISING: JANTUNG_BUNYI_BISING,
                JANTUNG_CRT: JANTUNG_CRT,
                JANTUNG_AKRAL_HANGAT: JANTUNG_AKRAL_HANGAT,
                JANTUNG_AKRAL_DINGIN: JANTUNG_AKRAL_DINGIN,
                JANTUNG_PENINGKATAN_JVP: JANTUNG_PENINGKATAN_JVP,
                JANTUNG_UKURAN_CVP: JANTUNG_UKURAN_CVP,
                JANTUNG_WARNA_JAUNDICE: JANTUNG_WARNA_JAUNDICE,
                JANTUNG_WARNA_SIANOTIK: JANTUNG_WARNA_SIANOTIK,
                JANTUNG_WARNA_KEMERAHAN: JANTUNG_WARNA_KEMERAHAN,
                JANTUNG_WARNA_PUCAT: JANTUNG_WARNA_PUCAT,
                JANTUNG_TEKANAN_DARAH: JANTUNG_TEKANAN_DARAH,
                JANTUNG_NADI: JANTUNG_NADI,
                JANTUNG_TEMP: JANTUNG_TEMP,
                JANTUNG_MASALAH: JANTUNG_MASALAH,
                OTOT_SENDI_BEBAS: OTOT_SENDI_BEBAS,
                OTOT_SENDI_TERBATAS: OTOT_SENDI_TERBATAS,
                OTOT_KEKUATAN: OTOT_KEKUATAN,
                OTOT_ODEMA_EKSTRIMITAS: OTOT_ODEMA_EKSTRIMITAS,
                OTOT_KELAINAN_BENTUK: OTOT_KELAINAN_BENTUK,
                OTOT_KREPITASI: OTOT_KREPITASI,
                OTOT_MASALAH: OTOT_MASALAH,
                SYARAF_GCS_EYE: SYARAF_GCS_EYE,
                SYARAF_GCS_VERBAL: SYARAF_GCS_VERBAL,
                SYARAF_GCS_MOTORIK: SYARAF_GCS_MOTORIK,
                SYARAF_GCS_TOTAL: SYARAF_GCS_TOTAL,
                SYARAF_FISIOLOGIS_BRACHIALIS: SYARAF_FISIOLOGIS_BRACHIALIS,
                SYARAF_FISIOLOGIS_PATELLA: SYARAF_FISIOLOGIS_PATELLA,
                SYARAF_FISIOLOGIS_ACHILLES: SYARAF_FISIOLOGIS_ACHILLES,
                SYARAF_PATOLOGIS_CHODDOKS: SYARAF_PATOLOGIS_CHODDOKS,
                SYARAF_PATOLOGIS_BABINSKI: SYARAF_PATOLOGIS_BABINSKI,
                SYARAF_PATOLOGIS_BUDZINZKY: SYARAF_PATOLOGIS_BUDZINZKY,
                SYARAF_PATOLOGIS_KERNIG: SYARAF_PATOLOGIS_KERNIG,
                SYARAF_MASALAH: SYARAF_MASALAH,
                INDRA_PUPIL_ISOKOR: INDRA_PUPIL_ISOKOR,
                INDRA__PUPIL_ANISOKOR: INDRA__PUPIL_ANISOKOR,
                INDRA__KONJUNGTIVA_ANEMIS: INDRA__KONJUNGTIVA_ANEMIS,
                INDRA__KONJUNGTIVA_ICTERUS: INDRA__KONJUNGTIVA_ICTERUS,
                INDRA__KONJUNGTIVA_TIDAK: INDRA__KONJUNGTIVA_TIDAK,
                INDRA_GANGGUAN_DENGAR: INDRA_GANGGUAN_DENGAR,
                INDRA_OTORHEA: INDRA_OTORHEA,
                INDRA_BENTUK_HIDUNG: INDRA_BENTUK_HIDUNG,
                INDRA_BENTUK_HIDUNG_KET: INDRA_BENTUK_HIDUNG_KET,
                INDRA_GANGGUAN_CIUM: INDRA_GANGGUAN_CIUM,
                INDRA_RHINORHEA: INDRA_RHINORHEA,
                INDRA_MASALAH: INDRA_MASALAH,
                KEMIH_KEBERSIHAN: KEMIH_KEBERSIHAN,
                KEMIH_ALAT_BANTU: KEMIH_ALAT_BANTU,
                KEMIH_JML_URINE: KEMIH_JML_URINE,
                KEMIH_WARNA_URINE: KEMIH_WARNA_URINE,
                KEMIH_BAU: KEMIH_BAU,
                KEMIH_KANDUNG_MEMBESAR: KEMIH_KANDUNG_MEMBESAR,
                KEMIH_NYERI_TEKAN: KEMIH_NYERI_TEKAN,
                KEMIH_GANGGUAN_ANURIA: KEMIH_GANGGUAN_ANURIA,
                KEMIH_GANGGUAN_ALIGURIA: KEMIH_GANGGUAN_ALIGURIA,
                KEMIH_GANGGUAN_RETENSI: KEMIH_GANGGUAN_RETENSI,
                KEMIH_GANGGUAN_INKONTINENSIA: KEMIH_GANGGUAN_INKONTINENSIA,
                KEMIH_GANGGUAN_DISURIA: KEMIH_GANGGUAN_DISURIA,
                KEMIH_GANGGUANHEMATURIA: KEMIH_GANGGUANHEMATURIA,
                KEMIH_MASALAH: KEMIH_MASALAH,
                CERNA_MAKAN_HABIS: CERNA_MAKAN_HABIS,
                CERNA_MAKAN_KET: CERNA_MAKAN_KET,
                CERNA_MAKAN_FREKUENSI: CERNA_MAKAN_FREKUENSI,
                CERNA_JML_MINUM: CERNA_JML_MINUM,
                CERNA_JENIS_MINUM: CERNA_JENIS_MINUM,
                CERNA_MULUT_BERSIH: CERNA_MULUT_BERSIH,
                CERNA_MULUT_KOTOR: CERNA_MULUT_KOTOR,
                CERNA_MULUT_BERBAU: CERNA_MULUT_BERBAU,
                CERNA_MUKOSA_LEMBAB: CERNA_MUKOSA_LEMBAB,
                CERNA_MUKOSA_KERING: CERNA_MUKOSA_KERING,
                CERNA_MUKOSA_STOMATITIS: CERNA_MUKOSA_STOMATITIS,
                CERNA_TENGGOROKAN_SAKIT: CERNA_TENGGOROKAN_SAKIT,
                CERNA_TENGGOROKAN_NYERI_TEKAN: CERNA_TENGGOROKAN_NYERI_TEKAN,
                CERNA_TENGGOROKAN_PEMBESARANTO: CERNA_TENGGOROKAN_PEMBESARANTO,
                CERNA_PERUT_NORMAL: CERNA_PERUT_NORMAL,
                CERNA_PERUT_DISTENDED: CERNA_PERUT_DISTENDED,
                CERNA_PERUT_METEORISMUS: CERNA_PERUT_METEORISMUS,
                CERNA_PERUT_NYERI_TEKAN: CERNA_PERUT_NYERI_TEKAN,
                CERNA_PERUT_LOKASI_NYERI: CERNA_PERUT_LOKASI_NYERI,
                CERNA_PERISTALTIK: CERNA_PERISTALTIK,
                CERNA_TURGOR_KULIT: CERNA_TURGOR_KULIT,
                CERNA_PEMBESARAN_HEPAR: CERNA_PEMBESARAN_HEPAR,
                CERNA_HEMATEMESIS: CERNA_HEMATEMESIS,
                CERNA_EVAKUASI_CAIRAN_ASCITES: CERNA_EVAKUASI_CAIRAN_ASCITES,
                CERNA_JML_CAIRAN_ASCITES: CERNA_JML_CAIRAN_ASCITES,
                CERNA_FREK_BAB: CERNA_FREK_BAB,
                CERNA_KONSISTENSI: CERNA_KONSISTENSI,
                CERNA_BAU_BAB: CERNA_BAU_BAB,
                CERNA_WARNA_BAB: CERNA_WARNA_BAB,
                CERNA_WARNA_CAIRAN_ASCITES: CERNA_WARNA_CAIRAN_ASCITES,
                CERNA_MASALAH: CERNA_MASALAH,
                ENDOKRIN_TYROID: ENDOKRIN_TYROID,
                ENDOKRIN_HIPOGLIKEMIA: ENDOKRIN_HIPOGLIKEMIA,
                ENDOKRIN_LUKA_GANGREN: ENDOKRIN_LUKA_GANGREN,
                ENDOKRIN__PUS: ENDOKRIN__PUS,
                ENDOKRIN_MASALAH: ENDOKRIN_MASALAH,
                PERSONAL_MANDI: PERSONAL_MANDI,
                PERSONAL_SIKATGIGI: PERSONAL_SIKATGIGI,
                PERSONAL_KERAMAS: PERSONAL_KERAMAS,
                PERSONAL_GANTIPAKAIAN: PERSONAL_GANTIPAKAIAN,
                PERSONAL_MASALAH: PERSONAL_MASALAH,
                PSIKOSOSIAL_ORANGDEKAT: PSIKOSOSIAL_ORANGDEKAT,
                PSIKOSOSIAL_KEGIATAN_IBADAH: PSIKOSOSIAL_KEGIATAN_IBADAH,
                PSIKOSOSIAL_MASALAH: PSIKOSOSIAL_MASALAH,
                TERAPI_PENUNJANG: TERAPI_PENUNJANG
            };
    return params;
}

function mComboSpesialisasiPengkajian()
{
    var Field = ['KD_SPESIAL', 'SPESIALISASI'];
    dsSpesialisasiPengkajian = new WebApp.DataStore({fields: Field});
    dsSpesialisasiPengkajian.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100, Sort: 'kd_spesial',
                                    Sortdir: 'ASC',
                                    target: 'ViewSpesialisasi',
                                    param: ''
                                }
                    }
            );
    var cboSpesialisasiPengkajian = new Ext.form.ComboBox(
            {
                x: 120,
                y: 70,
                id: 'cboSpesialisasiPengkajian',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local', forceSelection: true,
                emptyText: 'Select a Spesialisasi...',
                selectOnFocus: true,
                fieldLabel: 'Propinsi',
                align: 'Right',
                store: dsSpesialisasiPengkajian,
                valueField: 'KD_SPESIAL',
                displayField: 'SPESIALISASI',
                anchor: '20%',
                listeners:
                        {
                            'select': function (a, b, c)
                            {
                                Ext.getCmp('cboKelasPengkajian').setValue('');
                                Ext.getCmp('cboKamarPengkajian').setValue('');
                                loaddatastoreKelasPengkajian(b.data.KD_SPESIAL);
                                var tmpkriteriaPengkajian = getCriteriaFilter_viDaftar();
                                load_pengkajian(tmpkriteriaPengkajian);
                            },
                            'render': function (c) {
                                c.getEl().on('keypress', function (e) {
                                    if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                        Ext.getCmp('cboKelasPengkajian').focus();
                                }, c);
                            }

                        }
            }
    );
    return cboSpesialisasiPengkajian;
}
;
function loaddatastoreKelasPengkajian(kd_spesial) {
    dsKelasPengkajian.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 100, Sort: 'kd_spesial',
                                    Sortdir: 'ASC',
                                    target: 'ViewKelasAskep',
                                    param: kd_spesial
                                }
                    }
            );
}

function mComboKelasPengkajian()
{
    var Field = ['kd_unit', 'fieldjoin', 'kd_kelas'];
    dsKelasPengkajian = new WebApp.DataStore({fields: Field});
    var cboKelasPengkajian = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 100,
                        id: 'cboKelasPengkajian',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true, mode: 'local', forceSelection: true,
                        emptyText: 'Select a kelas...',
                        selectOnFocus: true,
                        fieldLabel: '',
                        align: 'Right',
                        store: dsKelasPengkajian,
                        valueField: 'kd_unit',
                        displayField: 'fieldjoin',
                        anchor: '20%',
                        listeners:
                                {'select': function (a, b, c)
                                    {
                                        Ext.getCmp('cboKamarPengkajian').setValue('');
                                        loaddatastoreKamarPengkajian(b.data.kd_unit);
                                        var tmpkriteriaPengkajian = getCriteriaFilter_viDaftar();
                                        load_pengkajian(tmpkriteriaPengkajian);
                                    },
                                    'render': function (c) {
                                        c.getEl().on('keypress', function (e) {
                                            if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                                Ext.getCmp('cboKamarPengkajian').focus();
                                        }, c);
                                    }

                                }
                    }
            );
    return cboKelasPengkajian;
}
;
function loaddatastoreKamarPengkajian(kd_unit)
{
    dsKamarPengkajian.load
            (
                    {
                        params:
                                {
                                    Skip: 0, Take: 100,
                                    Sort: 'kd_spesial',
                                    Sortdir: 'ASC',
                                    target: 'ViewKamarAskep',
                                    param: kd_unit + '<>' + Ext.getCmp('cboSpesialisasiPengkajian').getValue()
                                }
                    });
}

function mComboKamarPengkajian()
{
    var Field = ['roomname', 'no_kamar', 'jumlah_bed', 'kd_unit', 'digunakan', 'fieldjoin'];
    dsKamarPengkajian = new WebApp.DataStore({fields: Field});
    var cboKamarPengkajian = new Ext.form.ComboBox
            (
                    {
                        x: 320,
                        y: 100,
                        id: 'cboKamarPengkajian',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true, mode: 'local', forceSelection: true,
                        emptyText: 'Select a kamar...',
                        selectOnFocus: true,
                        fieldLabel: '',
                        align: 'Right',
                        store: dsKamarPengkajian,
                        valueField: 'no_kamar',
                        displayField: 'roomname',
                        width: 120,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        var tmpkriteriaPengkajian = getCriteriaFilter_viDaftar();
                                        load_pengkajian(tmpkriteriaPengkajian);
                                    }
                                }
                    }
            );
    return cboKamarPengkajian;
};

function load_pengkajian(criteria){
    dataSource_pengkajian.load
            (
                    {params:
                                {Skip: 0,
                                    Take: 50,
                                    Sort: '', Sortdir: 'ASC',
                                    // target: 'ViewAskepPengkajian',
                                    target: 'ViewTrKasirIGD',
                                    // param: criteria
                                    param: ''
                                }
                    }
            );
    return dataSource_pengkajian;
}

function load_pengkajian_param(criteria){
    var KataKunci = '';
    if(Ext.getCmp('TxtCariMedrecPengkajian').getValue() != '')
    {
        if (KataKunci == ''){
            KataKunci = ' and LOWER(kd_pasien) like  LOWER( ~' + Ext.get('TxtCariMedrecPengkajian').getValue() + '%~) ';
        }else{
            KataKunci += ' and LOWER(kd_pasien) like  LOWER( ~' + Ext.get('TxtCariMedrecPengkajian').getValue() + '%~) ';
        };
    };

    if(Ext.getCmp('TxtCariNamaPasienPengkajian').getValue() != '')
    {
        if (KataKunci == ''){
            KataKunci = ' and LOWER(nama) like  LOWER( ~' + Ext.get('TxtCariNamaPasienPengkajian').getValue() + '%~) ';
        }else{
            KataKunci += ' and LOWER(nama) like  LOWER( ~' + Ext.get('TxtCariNamaPasienPengkajian').getValue() + '%~) ';
        };
    };

    if(Ext.getCmp('TxtCariDokterPasienPengkajian').getValue() != '')
    {
        if (KataKunci == ''){
            KataKunci = ' and LOWER(nama_dokter) like  LOWER( ~%' + Ext.get('TxtCariDokterPasienPengkajian').getValue() + '%~) ';
        }else{
            KataKunci += ' and LOWER(nama_dokter) like  LOWER( ~%' + Ext.get('TxtCariDokterPasienPengkajian').getValue() + '%~) ';
        };
    };

    if(Ext.getCmp('dtpTanggalawalPengkajian').getValue() != '' || Ext.getCmp('dtpTanggalakhirPengkajian').getValue() != '')
    {
        if (KataKunci == ''){
            KataKunci = ' and (tgl_transaksi between ~'+Ext.get('dtpTanggalawalPengkajian').getValue() + '~ and ~'+Ext.get('dtpTanggalakhirPengkajian').getValue() + '~ ) ';
        }else{
            KataKunci += ' and (tgl_transaksi between ~'+Ext.get('dtpTanggalawalPengkajian').getValue() + '~ and ~'+Ext.get('dtpTanggalakhirPengkajian').getValue() + '~ ) ';
        };
    };

    if(Ext.getCmp('cboStatus_viPengkajianIGD').getValue() != '')
    {
        var status  = Ext.getCmp('cboStatus_viPengkajianIGD').getValue();
        var posting = '';
        if ((status == '3' || status == 3)){
            posting = 'FALSE';

            if (KataKunci == ''){
                KataKunci = 'and (posting_transaksi = '+posting+')';
            }else{
                KataKunci += 'and (posting_transaksi = '+posting+')';
            };
        }else if ((status == '2' || status == 2)){
            posting = 'TRUE';

            if (KataKunci == ''){
                KataKunci = 'and (posting_transaksi = '+posting+')';
            }else{
                KataKunci += 'and (posting_transaksi = '+posting+')';
            };
        }

    };

    dataSource_pengkajian.load
            (
                    {params:
                                {Skip: 0,
                                    Take: 50,
                                    Sort: '', Sortdir: 'ASC',
                                    // target: 'ViewAskepPengkajian',
                                    target: 'ViewTrKasirIGD',
                                    // param: criteria
                                    param: KataKunci
                                }
                    }
            );
    return dataSource_pengkajian;
}
function loadfilter_pengkajian() {
    var criteria = getCriteriaFilter_viDaftar();
    dataSource_pengkajian.load(
            {
                params:
                        {Skip: 0,
                            Take: 50,
                            Sort: '',
                            Sortdir: 'ASC',
                            // target: 'ViewAskepPengkajian',
                            target: 'ViewTrKasirIGD',
                            // param: criteria
                            param: ''
                        }
            });
    return dataSource_pengkajian;
}
function getCriteriaFilter_viDaftar()
{
    var strKriteria = " ng.AKHIR = 't' ";
    var tmpmedrec = Ext.getCmp('TxtCariMedrecPengkajian').getValue();
    var tmpnama = Ext.getCmp('TxtCariNamaPasienPengkajian').getValue();
    var tmpspesialisasi = Ext.getCmp('cboSpesialisasiPengkajian').getValue();
    var tmpkelas = Ext.getCmp('cboKelasPengkajian').getValue();
    var tmpkamar = Ext.getCmp('cboKamarPengkajian').getValue();
    var tmptglawal = Ext.get('dtpTanggalawalPengkajian').getValue();
    var tmptglakhir = Ext.get('dtpTanggalakhirPengkajian').getValue();
    var tmptambahan = Ext.getCmp('chkTglPengkajian').getValue()

    if (tmpmedrec !== "")
    {
        strKriteria += " AND P.KD_PASIEN " + "ilike '%" + tmpmedrec + "%' ";
    }
    if (tmpnama !== "")
    {
        strKriteria += " AND P.NAMA " + "ilike '%" + tmpnama + "%' ";
    }
    if (tmpspesialisasi !== "")
    {
        if (tmpspesialisasi === '0')
        {
            strKriteria += "";
        } else
        {
            strKriteria += " AND NG.KD_SPESIAL = '" + tmpspesialisasi + "' ";
        }

    }
    if (tmpkelas !== "") {
        strKriteria += " AND ng.KD_UNIT_KAMAR='" + tmpkelas + "' ";
    }
    if (tmpkamar !== "") {
        strKriteria += " AND NG.NO_KAMAR= '" + tmpkamar + "' ";
    }
    if (tmptambahan === true)
    {
        strKriteria += " AND ng.Tgl_masuk Between '" + tmptglawal + "'  and '" + tmptglakhir + "' ";
    }
    strKriteria += ' Limit 50 ';
    return strKriteria;
}

function radiojelas(tmp)
{
    if (tmp === 'rbjelasya')
    {
        Ext.getDom('rbjelastidak').checked = false;
    }
    if (Ext.getCmp('rbjelastidak').getValue() === true)
    {
        Ext.getDom('rbjelasya').checked = false;
    }
}

function ShowPesanWarningPengkajian(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING,
                        width: 250
                    }
            );
}
;
function ShowPesanInfoPengkajian(str, modul) {
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO,
                        width: 250
                    }
            );
}
;
function DeleteData()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/DeleteDataObj",
                        params: ParamDelete(),
                        failure: function (o)
                        {
                            loadMask.hide();
                            ShowPesanWarningPengkajian('Hubungi Admin', 'Error');
                            loadData_RencanaAsuhan();
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                loadMask.hide();
                                ShowPesanInfoPengkajian('Data berhasil di hapus', 'Information');
                                caridatapasien();
                            }
                            else
                            {
                                loadMask.hide();
                                ShowPesanWarningPengkajian('Gagal menghapus data', 'Error');
                                caridatapasien();
                            }
                            ;
                        }
                    }

            );
}
;
function ParamDelete()
{
    var params =
            {
                // Table: 'ViewAskepPengkajian',
                Table: 'ViewTrKasirIGD',
                query: "kd_pasien_kunj = '" + tmpkdpasien + "' and kd_unit_kunj = '" + tmpkdunit + "' and urut_masuk_kunj = " + tmpurutmasuk + " and tgl_masuk_kunj = '" + tmptglmasuk + "'"
            };
    return params;
}

function GetCriteriaPengkajian()
{
    var strKriteria = '';
    strKriteria = tmpkdpasien;
    strKriteria += '##@@##' + tmpkdunit;
    strKriteria += '##@@##' + tmpurutmasuk;
    strKriteria += '##@@##' + tmptglmasuk;
    strKriteria += '##@@##' + Ext.get('TxtPopupMedrec').getValue();
    strKriteria += '##@@##' + Ext.get('TxtPopupNamaPasien').getValue();
    strKriteria += '##@@##' + Ext.get('TxtPopupAlamatPasien').getValue();
    strKriteria += '##@@##' + Ext.get('TxtPopupSpesialisasi').getValue();
    strKriteria += '##@@##' + Ext.get('TxtPopupKelas').getValue();
    strKriteria += '##@@##' + Ext.get('TxtPopupkamarPasien').getValue();
    return strKriteria;
}


function RefreshDataFilterKasirIGD() 
{

    var KataKunci='';
    
     if (Ext.get('txtFilterIGDNomedrec').getValue() != '')
    {
        if (KataKunci == '')
        {
                        KataKunci = ' and   LOWER(kd_pasien) like  LOWER( ~' + Ext.get('txtFilterIGDNomedrec').getValue() + '%~)';
            
        }
        else
        {
        
                        KataKunci += ' and  LOWER(kd_pasien) like  LOWER( ~' + Ext.get('txtFilterIGDNomedrec').getValue() + '%~)';
        };

    };
    
     if (Ext.get('TxtIGDFilternama').getValue() != '')
    {
        if (KataKunci == '')
        {
                        KataKunci = ' and   LOWER(nama) like  LOWER( ~%' + Ext.get('TxtIGDFilternama').getValue() + '%~)';
            
        }
        else
        {
        
                        KataKunci += ' and  LOWER(nama) like  LOWER( ~%' + Ext.get('TxtIGDFilternama').getValue() + '%~)';
        };

    };
    
    
     if (Ext.get('cboUNIT_viKasirIGD').getValue() != '' && Ext.get('cboUNIT_viKasirIGD').getValue() != 'All')
    {
        if (KataKunci == '')
        {
    
                        KataKunci = ' and  LOWER(nama_unit)like  LOWER(~%' + Ext.get('cboUNIT_viKasirIGD').getValue() + '%~)';
        }
        else
        {
    
                        KataKunci += ' and LOWER(nama_unit) like  LOWER(~%' + Ext.get('cboUNIT_viKasirIGD').getValue() + '%~)';
        };
    };
    if (Ext.get('TxtIGDFilterDokter').getValue() != '')
        {
        if (KataKunci == '')
        {
            
                        KataKunci = ' and  LOWER(nama_dokter) like  LOWER(~' + Ext.get('TxtIGDFilterDokter').getValue() + '%~)';
        }
        else
        {
        
                        KataKunci += ' and LOWER(nama_dokter) like  LOWER(~' + Ext.get('TxtIGDFilterDokter').getValue() + '%~)';
        };
    };
        
        
    if (Ext.get('cboStatus_viKasirIGD').getValue() == 'Posting')
    {
        if (KataKunci == '')
        {

                        KataKunci = ' and  posting_transaksi = TRUE';
        }
        else
        {
        
                        KataKunci += ' and posting_transaksi =  TRUE';
        };
    
    };
        
        
        
    if (Ext.get('cboStatus_viKasirIGD').getValue() == 'Belum Posting')
    {
        if (KataKunci == '')
        {
        
                        KataKunci = ' and  posting_transaksi = FALSE';
        }
        else
        {
    
                        KataKunci += ' and posting_transaksi =  FALSE';
        };
        
        
    };
    
    if (Ext.get('cboStatus_viKasirIGD').getValue() == 'Semua')
    {
        if (KataKunci == '')
        {
        
                        KataKunci = ' and  (posting_transaksi = FALSE OR posting_transaksi = TRUE )';
        }
        else
        {
    
                        KataKunci += ' and (posting_transaksi = FALSE OR posting_transaksi = TRUE )';
        };
        
        
    };
    
        
    if (Ext.get('dtpTglAwalFilterIGD').getValue() != '')
    {
        if (KataKunci == '')
        {                      
                        KataKunci = " and (tgl_transaksi between ~" + Ext.get('dtpTglAwalFilterIGD').getValue() + "~ and ~" + Ext.get('dtpTglAkhirFilterIGD').getValue() + "~)";
        }
        else
        {
            
                        KataKunci += " and (tgl_transaksi between ~" + Ext.get('dtpTglAwalFilterIGD').getValue() + "~ and ~" + Ext.get('dtpTglAkhirFilterIGD').getValue() + "~)";
        };
    
    };
    
     
    if (KataKunci != undefined ||KataKunci != '' ) 
    {  
        dsTRKasirIGDList.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: selectCountKasirIGD, 
                    //Sort: 'no_transaksi',
                                        Sort: 'tgl_transaksi',
                    //Sort: 'no_transaksi',
                    Sortdir: 'ASC', 
                    target:'ViewTrKasirIGD',
                    param : KataKunci
                }           
            }
        );  
        getTotKunjunganIGD();
    }
    else
    {
    getTotKunjunganIGD();
    dsTRKasirIGDList.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: selectCountKasirIGD, 
                    //Sort: 'no_transaksi',
                                        Sort: 'tgl_transaksi',
                    //Sort: 'no_transaksi',
                    Sortdir: 'ASC', 
                    target:'ViewTrKasirIGD',
                    param : ''
                }           
            }
        );   
    };
    
    return dsTRKasirIGDList;
    
};


function mComboStatusBayar_viPengkajianIGD(){
    var cboStatus_viPengkajianIGD = new Ext.form.ComboBox({
        x: 120,
        y: 100,
        id              : 'cboStatus_viPengkajianIGD',
        typeAhead       : true,
        triggerAction   : 'all',
        lazyRender      : true,
        mode            : 'local',
        width: 200,
        emptyText       : '',
        fieldLabel      : 'Status Posting',
        store           : new Ext.data.ArrayStore({
            id: 0,
            fields:['Id','displayText'],
            data: [[1, 'Semua'],[2, 'Posting'], [3, 'Belum Posting']]
        }),
        valueField      : 'Id',
        displayField    : 'displayText',
        value           : 1,
        listeners       :{
            select: function(a,b,c){
                // selectCountStatusByr_viKasirIGD=b.data.displayText ;
                load_pengkajian_param();
            }
        }
    });
    return cboStatus_viPengkajianIGD;
};