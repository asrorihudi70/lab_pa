// Data Source ExtJS # --------------

/**
 *	Nama File 		: TRDataPasien.js
 *	Menu 			: Pendaftaran
 *	Model id 		: 030101
 *	Keterangan 		: Data Pasien adalah proses untuk melihat list/ daftar pasien, selain itu data pasien ini digunakan untuk___
 *					  ___melengkapi atau memperbaiki biodata dari pasien
 *	Di buat tanggal : 15 April 2014
 *	Oleh 			: SDY_RI
 *	Edit                    : HDHT
 */

// Deklarasi Variabel pada Data Pasien # --------------
var CurrentHistorydatapasien =
        {
            data: Object,
            details: Array,
            row: 0
        };
var datapasienvariable = {};
datapasienvariable.kd_pasien;
datapasienvariable.urut;
datapasienvariable.tglkunjungan;
datapasienvariable.kd_unit;
var mod_name_viDataPasien = "viDataPasien";
var NamaForm_viDataPasien = "Data Pasien";
var selectSetJK;
var selectSetGolDarah;
var selectSetSatusMarital;
var icons_viDataPasien = "Data_pegawai";
var DfltFilterBtn_viDataPasien = 0;
var slctCount_viDataPasien = 10;
var now_viDataPasien = new Date();
var rowSelectedGridDataView_viDataPasien;
var setLookUpsGridDataView_viDataPasien;
var addNew_viDataPasien;
var tmphasil;

CurrentPage.page = dataGrid_viDataPasien(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);


// Start Project Data Pasien # --------------

// --------------------------------------- # Start Function # ---------------------------------------

/**
 *	Function : ShowPesanWarning_viDataPasien
 *	
 *	Sebuah fungsi untuk menampilkan pesan saat terjadi kesalahan
 */

function ShowPesanWarning_viDataPasien(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING,
                        width: 250
                    }
            )
}
// End Function ShowPesanWarning_viDataPasien # --------------

/**
 *	Function : ShowPesanError_viDataPasien
 *	
 *	Sebuah fungsi untuk menampilkan pesan saat terjadi kesalahan
 */

function ShowPesanError_viDataPasien(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.ERROR,
                        width: 250
                    }
            )
}
// End Function ShowPesanError_viDataPasien # --------------

/**
 *	Function : ShowPesanInfo_viDataPasien
 *	
 *	Sebuah fungsi untuk menampilkan pesan saat data berhasil
 */

function ShowPesanInfo_viDataPasien(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO,
                        width: 250
                    }
            )
}
// End Function ShowPesanInfo_viDataPasien # --------------

/**
 *	Function : getCriteriaFilterGridDataView_viDataPasien
 *	
 *	Sebuah fungsi untuk memfilter data yang diambil dari Net.
 */
function getCriteriaFilterGridDataView_viDataPasien()
{
    var strKriteriaGridDataView_viDataPasienGridDataView_viDataPasien = "";

    //strKriteriaGridDataView_viDataPasien = " WHERE PS.KD_CUSTOMER = '" + DfltFilter_KD_CUSTOMER_viDataPasien + "' ";

    if (DfltFilterBtn_viDataPasien == 1)
    {
        if (Ext.getCmp('TxtFilterGridDataView_NO_RM_viDataPasien').getValue() != "")
        {
            strKriteriaGridDataView_viDataPasien += " AND PS.KD_PASIEN LIKE '" + DfltFilter_KD_CUSTOMER_viDataPasien + ".%" + Ext.getCmp('TxtFilterGridDataView_NO_RM_viDataPasien').getValue() + "%' ";
        }

        if (Ext.getCmp('TxtFilterGridDataView_NAMA_viDataPasien').getValue() != "")
        {
            strKriteriaGridDataView_viDataPasien += " AND PS.NAMA LIKE '%" + Ext.getCmp('TxtFilterGridDataView_NAMA_viDataPasien').getValue() + "%' ";
        }

        if (Ext.getCmp('TxtFilterGridDataView_NO_TELP_viDataPasien').getValue() != "")
        {
            strKriteriaGridDataView_viDataPasien += " AND PS.NO_TELP LIKE '%" + Ext.getCmp('TxtFilterGridDataView_NO_TELP_viDataPasien').getValue() + "%' ";
        }

        if (Ext.getCmp('TxtFilterGridDataView_NO_HP_viDataPasien').getValue() != "")
        {
            strKriteriaGridDataView_viDataPasien += " AND PS.NO_HP LIKE '%" + Ext.getCmp('TxtFilterGridDataView_NO_HP_viDataPasien').getValue() + "%' ";
        }

        if (Ext.getCmp('TxtFilterGridDataView_ALAMAT_viDataPasien').getValue() != "")
        {
            strKriteriaGridDataView_viDataPasien += " AND PS.ALAMAT LIKE '%" + Ext.getCmp('TxtFilterGridDataView_ALAMAT_viDataPasien').getValue() + "%' ";
        }
    }

    //strKriteriaGridDataView_viDataPasien += " ORDER BY PS.KD_PASIEN ASC ";

    // DfltFilterBtn_viDataPasien = 0; // Tidak diaktifkan agar saat tutup windows popup tetap di filter
    //return strKriteriaGridDataView_viDataPasien;
}
// End Function getCriteriaFilterGridDataView_viDataPasien # --------------

/**
 *	Function : ValidasiEntry_viDataPasien
 *	
 *	Sebuah fungsi untuk mengecek isian
 */
function ValidasiEntry_viDataPasien(modul, mBolHapus)
{
    var x = 1;
    if (Ext.get('TxtWindowPopup_NAMA_viDataPasien').getValue() === 'Nama' || (Ext.get('TxtWindowPopup_TEMPAT_LAHIR_viDataPasien').getValue() === 'Tempat Lahir' ||
            (Ext.get('TxtWindowPopup_ALAMAT_viDataPasien').getValue() === 'Alamat' || (Ext.get('TxtWindowPopup_NAMA_viDataPasien').getValue() === '' ||
                    (Ext.get('TxtWindowPopup_TEMPAT_LAHIR_viDataPasien').getValue() === '' || (Ext.get('TxtWindowPopup_ALAMAT_viDataPasien').getValue() === ''))))))
    {
        if (Ext.get('TxtWindowPopup_NAMA_viDataPasien').getValue() === 'Nama' || (Ext.get('TxtWindowPopup_NAMA_viDataPasien').getValue() === ''))
        {
            ShowPesanWarning_viDataPasien('Nama belum terisi', modul);
            x = 0;
        } else if (Ext.get('TxtWindowPopup_TEMPAT_LAHIR_viDataPasien').getValue() === 'Tempat Lahir' || (Ext.get('TxtWindowPopup_TEMPAT_LAHIR_viDataPasien').getValue() === ''))
        {
            ShowPesanWarning_viDaftar("Tempat Lahir belum terisi", modul);
            x = 0;
        } else if (Ext.get('TxtWindowPopup_ALAMAT_viDataPasien').getValue() === 'Alamat' || (Ext.get('TxtWindowPopup_ALAMAT_viDataPasien').getValue() === ''))
        {
            ShowPesanWarning_viDaftar("Alamat belum terisi", modul);
            x = 0;
        }
    }

    return x;
}


function ValidasiEntryHistori_viDataPasien(modul)
{
    var x = 1;
    if (Ext.get('TxtWindowPopup_NAMA_viDataPasien').getValue() === 'Nama' || (Ext.get('TxtWindowPopup_NAMA_viDataPasien').getValue() === ''))
    {
        ShowPesanWarning_viDataPasien('Nama belum terisi', modul);
        x = 0;
    }

    return x;
}
// End Function ValidasiEntry_viDataPasien # --------------

/**
 *	Function : DataRefresh_viDataPasien
 *	
 *	Sebuah fungsi untuk mengambil data dari Net.
 *	Digunakan pada View Grid Pertama, Filter Grid 
 */

function DataRefresh_viDataPasien(criteria)
{

    dataSourceGrid_viDataPasien.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: slctCount_viDataPasien,
                                    Sort: 'kunjungan.tgl_masuk',
                                    Sortdir: 'ASC',
                                    target: 'Vi_ViewDataPasienIgd',
                                    param: criteria

                                }
                    }
            );
    return dataSourceGrid_viDataPasien;
}
// End Function DataRefresh_viDataPasien # --------------

/**
 *	Function : datainit_viDataPasien
 *	
 *	Sebuah fungsi untuk menampilkan data dari yang dipilih pada grid 
 */

function datainit_viDataPasien(rowdata)
{
    addNew_viDataPasien = false;
    var tmpjk;
    //-------------- # textfield # --------------
    Ext.get('TxtWindowPopup_KD_CUSTOMER_viDataPasien').dom.value = rowdata.TGL_MASUK;
    Ext.get('TxtWindowPopup_KD_PASIEN_viDataPasien').dom.value = rowdata.KD_UNIT;
    Ext.get('TxtWindowPopup_NO_RM_viDataPasien').dom.value = rowdata.KD_PASIEN;
    Ext.get('TxtWindowPopup_Urut_Masuk_viDataPasien').dom.value = rowdata.URUT_MASUK;
    Ext.get('TxtWindowPopup_NAMA_viDataPasien').dom.value = rowdata.NAMA;
    Ext.get('TxtWindowPopup_TEMPAT_LAHIR_viDataPasien').dom.value = rowdata.TEMPAT_LAHIR;
    Ext.get('TxtWindowPopup_TAHUN_viDataPasien').dom.value = rowdata.TAHUN;
    Ext.get('TxtWindowPopup_BULAN_viDataPasien').dom.value = rowdata.BULAN;
    Ext.get('TxtWindowPopup_HARI_viDataPasien').dom.value = rowdata.HARI;
    Ext.get('TxtWindowPopup_ALAMAT_viDataPasien').dom.value = rowdata.ALAMAT;
    Ext.get('TxtTmpAgama_viDataPasien').dom.value = rowdata.KD_AGAMA;
    Ext.get('TxtTmpPekerjaan_viDataPasien').dom.value = rowdata.KD_PEKERJAAN;
    Ext.get('TxtTmpPendidikan_viDataPasien').dom.value = rowdata.KD_PENDIDIKAN;

    //Ext.get('TxtWindowPopup_NO_HP_viDataPasien').dom.value=TmpNoHP;
    Ext.get('TxtWindowPopup_TAHUN_viDataPasien').dom.value = '';
    Ext.get('TxtWindowPopup_BULAN_viDataPasien').dom.value = '';
    Ext.get('TxtWindowPopup_HARI_viDataPasien').dom.value = '';

    setUsia(ShowDate(rowdata.TGL_LAHIR));
    //-------------- # datefield # --------------
    Ext.get('DateWindowPopup_TGL_LAHIR_viDataPasien').dom.value = ShowDate(rowdata.TGL_LAHIR);

    //-------------- # combobox # --------------
    if (rowdata.JENIS_KELAMIN === "t")
    {
        tmpjk = "1";
    } else
    {
        tmpjk = "2";
    }
    Ext.getCmp('cboJK').setValue(tmpjk);
    Ext.getCmp('cboGolDarah').setValue(rowdata.GOL_DARAH);
    Ext.getCmp('cboStatusMarital').setValue(rowdata.STATUS_MARITA);
    Ext.getCmp('cboPekerjaanRequestEntry').setValue(rowdata.PEKERJAAN);
    Ext.getCmp('cboAgamaRequestEntry').setValue(rowdata.AGAMA);
    Ext.getCmp('cboPendidikanRequestEntry').setValue(rowdata.PENDIDIKAN);
    Ext.getCmp('TxtWindowPopup_NO_TELP_viDataPasien').setValue(rowdata.TELEPON);
    Ext.getCmp('cbokelurahan_editdatapasien').setValue(rowdata.KELURAHAN);
    Ext.getCmp('cboKecamatan_editdatapasien').setValue(rowdata.KECAMATAN);
    Ext.getCmp('cboKabupaten_editdatapasien').setValue(rowdata.KABUPATEN);
    Ext.getCmp('cboPropinsi_EditdataPasien').setValue(rowdata.PROPINSI);
    loaddatastorekelurahan(rowdata.KD_KECAMATAN);
    loaddatastorekecamatan(rowdata.KD_KABUPATEN);
    loaddatastorekabupaten(rowdata.KD_PROPINSI);
    selectPropinsiRequestEntry = rowdata.KD_PROPINSI;
    selectKabupatenRequestEntry = rowdata.KD_KABUPATEN;
    selectKecamatanpasien = rowdata.KD_KECAMATAN;
    kelurahanpasien = rowdata.KD_KELURAHAN;

    Ext.getCmp('TxtWindowPopup_Email_viDataPasien').setValue(rowdata.EMAIL);
    Ext.getCmp('TxtWindowPopup_HP_viDataPasien').setValue(rowdata.HP);
    Ext.getCmp('TxtWindowPopup_Nama_ayah_viDataPasien').setValue(rowdata.AYAH);
    Ext.getCmp('TxtWindowPopup_NamaIbu_viDataPasien').setValue(rowdata.IBU);


}
// End Function datainit_viDataPasien # --------------

/**
 *	Function : dataparam_datapasviDataPasien
 *	
 *	Sebuah fungsi untuk mengirim balik isian ke Net.
 *	Digunakan saat save, edit, delete
 */

function setUsia(Tanggal)
{

    Ext.Ajax.request
            ({
                url: baseURL + "index.php/main/GetUmur",
                params: {
                    TanggalLahir: ShowDateReal(Tanggal)
                },
                success: function (o)
                {
                    //alert('test');  

                    var tmphasil = o.responseText;
                    // alert(tmphasil);
                    var tmp = tmphasil.split(' ');
                    if (tmp.length == 6)
                    {
                        Ext.getCmp('TxtWindowPopup_TAHUN_viDataPasien').setValue(tmp[0]);
                        Ext.getCmp('TxtWindowPopup_BULAN_viDataPasien').setValue(tmp[2]);
                        Ext.getCmp('TxtWindowPopup_HARI_viDataPasien').setValue(tmp[4]);
                    } else if (tmp.length == 4)
                    {
                        if (tmp[1] === 'years' && tmp[3] === 'day')
                        {
                            Ext.getCmp('TxtWindowPopup_TAHUN_viDataPasien').setValue(tmp[0]);
                            Ext.getCmp('TxtWindowPopup_BULAN_viDataPasien').setValue('0');
                            Ext.getCmp('TxtWindowPopup_HARI_viDataPasien').setValue(tmp[2]);
                        } else if (tmp[1] === 'year' && tmp[3] === 'days')
                        {
                            Ext.getCmp('TxtWindowPopup_TAHUN_viDataPasien').setValue(tmp[0]);
                            Ext.getCmp('TxtWindowPopup_BULAN_viDataPasien').setValue('0');
                            Ext.getCmp('TxtWindowPopup_HARI_viDataPasien').setValue(tmp[2]);
                        } else
                        {
                            Ext.getCmp('TxtWindowPopup_TAHUN_viDataPasien').setValue('0');
                            Ext.getCmp('TxtWindowPopup_BULAN_viDataPasien').setValue(tmp[0]);
                            Ext.getCmp('TxtWindowPopup_HARI_viDataPasien').setValue(tmp[2]);
                        }
                    } else if (tmp.length == 2)
                    {

                        if (tmp[1] == 'year')
                        {
                            Ext.getCmp('TxtWindowPopup_TAHUN_viDataPasien').setValue(tmp[0]);
                            Ext.getCmp('TxtWindowPopup_BULAN_viDataPasien').setValue('0');
                            Ext.getCmp('TxtWindowPopup_HARI_viDataPasien').setValue('0');
                        } else if (tmp[1] == 'years')
                        {
                            Ext.getCmp('TxtWindowPopup_TAHUN_viDataPasien').setValue(tmp[0]);
                            Ext.getCmp('TxtWindowPopup_BULAN_viDataPasien').setValue('0');
                            Ext.getCmp('TxtWindowPopup_HARI_viDataPasien').setValue('0');
                        } else if (tmp[1] == 'mon')
                        {
                            Ext.getCmp('TxtWindowPopup_TAHUN_viDataPasien').setValue('0');
                            Ext.getCmp('TxtWindowPopup_BULAN_viDataPasien').setValue(tmp[0]);
                            Ext.getCmp('TxtWindowPopup_HARI_viDataPasien').setValue('0');
                        } else if (tmp[1] == 'mons')
                        {
                            Ext.getCmp('TxtWindowPopup_TAHUN_viDataPasien').setValue('0');
                            Ext.getCmp('TxtWindowPopup_BULAN_viDataPasien').setValue(tmp[0]);
                            Ext.getCmp('TxtWindowPopup_HARI_viDataPasien').setValue('0');
                        } else {
                            Ext.getCmp('TxtWindowPopup_TAHUN_viDataPasien').setValue('0');
                            Ext.getCmp('TxtWindowPopup_BULAN_viDataPasien').setValue('0');
                            Ext.getCmp('TxtWindowPopup_HARI_viDataPasien').setValue(tmp[0]);
                        }
                    } else if (tmp.length == 1)
                    {
                        Ext.getCmp('TxtWindowPopup_TAHUN_viDataPasien').setValue('0');
                        Ext.getCmp('TxtWindowPopup_BULAN_viDataPasien').setValue('0');
                        Ext.getCmp('TxtWindowPopup_HARI_viDataPasien').setValue('1');
                    } else
                    {
                        alert("Tanggal yang anda masukan lebih besar dari tanggal sekarang");
                    }
                }


            });



}


function dataparam_datapasviDataPasien()
{
    var params_viDataPasien =
            {
                //-------------- # modelist Net. # --------------
                Table: 'Vi_ViewDataPasienIgd',
                //-------------- # textfield # --------------
                NO_MEDREC: Ext.getCmp('TxtWindowPopup_NO_RM_viDataPasien').getValue(),
                NAMA: Ext.getCmp('TxtWindowPopup_NAMA_viDataPasien').getValue(),
                TEMPAT_LAHIR: Ext.getCmp('TxtWindowPopup_TEMPAT_LAHIR_viDataPasien').getValue(),
                ALAMAT: Ext.getCmp('TxtWindowPopup_ALAMAT_viDataPasien').getValue(),
                NO_TELP: Ext.getCmp('TxtWindowPopup_NO_TELP_viDataPasien').getValue(),
                //NO_HP: Ext.get('TxtWindowPopup_NO_HP_viDataPasien').getValue(),

                //-------------- # datefield # --------------
                TGL_LAHIR: Ext.get('DateWindowPopup_TGL_LAHIR_viDataPasien').getValue(),
                //-------------- # combobox # --------------
                ID_JENIS_KELAMIN: Ext.getCmp('cboJK').getValue(),
                ID_GOL_DARAH: Ext.getCmp('cboGolDarah').getValue(),
                KD_STS_MARITAL: Ext.getCmp('cboStatusMarital').getValue(),
                NAMA_PEKERJAAN: Ext.getCmp('cboPekerjaanRequestEntry').getValue(),
                NAMA_AGAMA: Ext.getCmp('cboAgamaRequestEntry').getValue(),
                NAMA_PENDIDIKAN: Ext.getCmp('cboPendidikanRequestEntry').getValue(),
                //KD_PEKERJAAN:Ext.getCmp('cboPekerjaanRequestEntry').getValue(),
                //KD_AGAMA:Ext.getCmp('cboAgamaRequestEntry').getValue(),

                KD_AGAMA: Ext.getCmp('TxtTmpAgama_viDataPasien').getValue(),
                KD_PEKERJAAN: Ext.getCmp('TxtTmpPekerjaan_viDataPasien').getValue(),
                KD_PENDIDIKAN: Ext.getCmp('TxtTmpPendidikan_viDataPasien').getValue(),
                TMPPARAM: '0',
                AYAHPASIEN: Ext.getCmp('TxtWindowPopup_Nama_ayah_viDataPasien').getValue(),
                IBUPASIEN: Ext.getCmp('TxtWindowPopup_NamaIbu_viDataPasien').getValue(),
                Emailpasien: Ext.getCmp('TxtWindowPopup_Email_viDataPasien').getValue(),
                HPpasien: Ext.getCmp('TxtWindowPopup_HP_viDataPasien').getValue()
            }

    return params_viDataPasien
}
/**
 *	Function : datasave_dataPasiDataPasien
 *	
 *	Sebuah fungsi untuk menyimpan dan mengedit data entry 
 */

function getParamRequest()
{
    var params =
            {
                Table: 'Vi_ViewDataPasienIgd',
                NOMEDREC: Ext.get('TxtWindowPopup_NO_RM_viDataPasien').getValue(),
                TGLMASUK: Ext.get('TxtWindowPopup_KD_CUSTOMER_viDataPasien').getValue(),
                URUTMASUK: Ext.get('TxtWindowPopup_Urut_Masuk_viDataPasien').getValue(),
                KDUNIT: Ext.get('TxtWindowPopup_KD_PASIEN_viDataPasien').getValue(),
                KET: Ext.get('TxtHistoriDeleteDataPasien').getValue(),
                TMPPARAM: '1'
            };
    return params
}
;

function datasavehistori_viDataPasien()
{
    if (ValidasiEntryHistori_viDataPasien('Simpan Data', true) === 1)
    {
        Ext.Ajax.request
                (
                        {
                            url: baseURL + "index.php/main/CreateDataObj",
                            params: getParamRequest(),
                            success: function (o)
                            {
                                var cst = Ext.decode(o.responseText);
                                if (cst.success === true)
                                {
                                    ShowPesanInfo_viDataPasien('Data transaksi pasien berhasil di hapus', 'Edit Data');
                                    DataRefresh_viDataPasien(getCriteriaFilterGridDataView_viDataPasien());
                                    //tmphasil = true;
                                } else
                                {
                                    ShowPesanError_viDataPasien('Data transaksi pasien tidak berhasil di hapus' + cst.pesan, 'Edit Data');
                                    //tmphasil = false;
                                }
                            }
                        }
                )
    } else
    {
        if (mBol === true)
        {
            return false;
        }
    }
}

function datasave_dataPasiDataPasien(mBol)
{
    if (ValidasiEntry_viDataPasien('Simpan Data', true) == 1)
    {
        Ext.Ajax.request
                (
                        {
                            url: baseURL + "index.php/main/CreateDataObj",
                            params: dataparam_datapasviDataPasien(),
                            failure: function (o)
                            {
                                ShowPesanError_viDataPasien('Data tidak berhasil diupdate ', 'Edit Data');
                            },
                            success: function (o)
                            {
                                var cst = Ext.decode(o.responseText);
                                if (cst.success === true)
                                {
                                    ShowPesanInfo_viDataPasien('Data berhasil disimpan', 'Edit Data');
                                    DataRefresh_viDataPasien(getCriteriaFilterGridDataView_viDataPasien());
                                } else if (cst.success === false && cst.pesan === 0)
                                {
                                    ShowPesanError_viDataPasien('Data tidak berhasil diupdate ', 'Edit Data');
                                } else
                                {
                                    ShowPesanError_viDataPasien('Data tidak berhasil diupdate ', 'Edit Data');
                                }
                            }
                        }
                )
    } else
    {
        if (mBol === true)
        {
            return false;
        }
    }
}
// End Function datasave_dataPasiDataPasien # --------------

/**
 *	Function : datadelete_viDataPasien
 *	
 *	Sebuah fungsi untuk menghapus data entry 
 */

function datadelete_viDataPasien()
{
    //}
}
// End Function datadelete_viDataPasien # --------------

// --------------------------------------- # End Function # ---------------------------------------

// --------------------------------------- # Start Form # ---------------------------------------

/**
 *	Function : dataGrid_viDataPasien
 *	
 *	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
 */

function dataGrid_viDataPasien(mod_id_viDataPasien)
{/*
 
 
 Public $;
 public $;
 public $;
 public $; */

    var FieldGrid_viDataPasien =
            [
                'KD_PASIEN', 'NAMA', 'NAMA_KELUARGA', 'JENIS_KELAMIN', 'TEMPAT_LAHIR', 'TGL_LAHIR', 'AGAMA',
                'GOL_DARAH', 'WNI', 'STATUS_MARITA', 'ALAMAT', 'KD_KELURAHAN', 'PENDIDIKAN', 'PEKERJAAN',
                'KD_UNIT', 'TGL_MASUK', 'URUT_MASUK', 'KD_KABUPATEN', 'KABUPATEN', 'KECAMATAN', 'KD_KECAMATAN', 'PROPINSI',
                'KD_PROPINSI', 'KELURAHAN', 'EMAIL', 'HP', 'AYAH', 'IBU',
                'KD_AGAMA', 'KD_PEKERJAAN', 'KD_PENDIDIKAN', 'TELEPON', 'NAMA_UNIT'
            ];
    // Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSourceGrid_viDataPasien = new WebApp.DataStore({
        fields: FieldGrid_viDataPasien

    });



    // Pemangilan Function untuk memangil data yang akan ditampilkan # --------------

    // Grid Data Pasien # --------------
    var GridDataView_viDataPasien = new Ext.grid.EditorGridPanel
            (
                    {
                        xtype: 'editorgrid',
                        store: dataSourceGrid_viDataPasien,
                        title: '',
                        anchor: '100% 40%',
                        columnLines: true,
                        border: true,
                        plugins: [new Ext.ux.grid.FilterRow()],
                        selModel: new Ext.grid.RowSelectionModel
                                // Tanda aktif saat salah satu baris dipilih # --------------
                                        ({
                                            singleSelect: true,
                                            listeners:
                                                    {
                                                        rowselect: function (sm, row, rec)
                                                        {
                                                            rowSelectedGridDataView_viDataPasien = undefined;
                                                            rowSelectedGridDataView_viDataPasien = dataSourceGrid_viDataPasien.getAt(row);
                                                        }
                                                    }
                                        }),
                                // Proses eksekusi baris yang dipilih # --------------
                                listeners:
                                        {
                                            // Function saat ada event double klik maka akan muncul form view # --------------
                                            rowdblclick: function (sm, ridx, cidx)
                                            {
                                                rowSelectedGridDataView_viDataPasien = dataSourceGrid_viDataPasien.getAt(ridx);
                                                if (rowSelectedGridDataView_viDataPasien != undefined)
                                                {
                                                    setLookUpGridDataView_viDataPasien(rowSelectedGridDataView_viDataPasien.data);
                                                } else
                                                {
                                                    ShowPesanWarning_viDataPasien('Silahkan pilih Data Pasien ', 'Edit Data');
                                                }
                                            },
                                            rowclick: function (sm, ridx, cidx)
                                            {
                                                rowSelectedGridDataView_viDataPasien.data.KD_PASIEN
                                                RefreshDatahistoribayar(rowSelectedGridDataView_viDataPasien.data.KD_PASIEN);

                                                //rowSelectedGridDataView_viDataPasien.data.KD_PASIEN;

                                            }
                                        },
                                /**
                                 *	Mengatur tampilan pada Grid Data Pasien
                                 *	Terdiri dari : Judul, Isi dan Event
                                 *	Isi pada Grid di dapat dari pemangilan dari Net.
                                 *	dimana dataindex adalah data dari Net. yang di dapat dari FieldMaster pada dataSourceGrid_viDataPasien
                                 *	didapat dari Function DataRefresh_viDataPasien()
                                 */
                                colModel: new Ext.grid.ColumnModel
                                        (
                                                [
                                                    new Ext.grid.RowNumberer(),
                                                    {
                                                        id: 'colNRM_viDaftar',
                                                        header: 'No.Medrec',
                                                        dataIndex: 'KD_PASIEN',
                                                        sortable: true,
                                                        width: 20
//						filter:
//						{
//							type: 'int'
//						}
                                                    },
                                                    {
                                                        id: 'colNMPASIEN_viDaftar',
                                                        header: 'Nama',
                                                        dataIndex: 'NAMA',
                                                        sortable: true,
                                                        width: 40
//						filter:
//						{}
                                                    },
                                                    {
                                                        id: 'colALAMAT_viDaftar',
                                                        header: 'Alamat',
                                                        dataIndex: 'ALAMAT',
                                                        width: 60,
                                                        sortable: true
//						filter: {}
                                                    },
                                                    {
                                                        id: 'colTglKunj_viDaftar',
                                                        header: 'Tgl Lahir',
                                                        dataIndex: 'TGL_LAHIR',
                                                        width: 20,
                                                        sortable: true,
                                                        format: 'd/M/Y',
//						filter: {},
                                                        renderer: function (v, params, record)
                                                        {

                                                            return ShowDate(record.data.TGL_LAHIR);
                                                        }
                                                    }

                                                    //-------------- ## --------------
                                                ]
                                                ),
                                // Tolbar ke Dua # --------------
                                tbar:
                                        {
                                            xtype: 'toolbar',
                                            id: 'ToolbarGridDataView_viDataPasien',
                                            items:
                                                    [
                                                        {
                                                            xtype: 'button',
                                                            text: 'Edit Data',
                                                            iconCls: 'Edit_Tr',
                                                            tooltip: 'Edit Data',
                                                            width: 100,
                                                            id: 'BtnEditGridDataView_viDataPasien',
                                                            handler: function (sm, row, rec)
                                                            {
                                                                if (rowSelectedGridDataView_viDataPasien != undefined)
                                                                {
                                                                    setLookUpGridDataView_viDataPasien(rowSelectedGridDataView_viDataPasien.data);
                                                                } else
                                                                {
                                                                    ShowPesanWarning_viDataPasien('Silahkan pilih Data Pasien ', 'Edit Data');
                                                                }
                                                            }
                                                        }
                                                    ]
                                        },
                                bbar: bbar_paging(mod_name_viDataPasien, slctCount_viDataPasien, dataSourceGrid_viDataPasien),
                                // End Button Bar Pagging # --------------
                                viewConfig:
                                        {
                                            forceFit: true
                                        }
                            }
                            )
                    DataRefresh_viDataPasien();

                    var top = new Ext.FormPanel(
                            {
                                labelAlign: 'top',
                                frame: true,
                                title: '',
                                bodyStyle: 'padding:5px 5px 0',
                                //width: 600,
                                items: [{
                                        layout: 'column',
                                        items:
                                                [
                                                    {
                                                        columnWidth: .3,
                                                        layout: 'form',
                                                        items:
                                                                [
                                                                    {
                                                                        columnWidth: .5,
                                                                        layout: 'form',
                                                                        items: [
                                                                            {
                                                                                xtype: 'textfield',
                                                                                fieldLabel: 'Alamat Pasien ',
                                                                                name: 'txtAlamatPasien_DataPasien',
                                                                                enableKeyEvents: true,
                                                                                id: 'txtAlamatPasien_DataPasien',
                                                                                anchor: '95%',
                                                                                listeners:
                                                                                        {
                                                                                            'keyup': function ()
                                                                                            {
                                                                                                var tmpkriteria = getCriteriaFilter_viDaftar();
                                                                                                DataRefresh_viDataPasien(tmpkriteria);
																								
                                                                                            }
                                                                                        }
                                                                            }
                                                                        ]
                                                                    }
                                                                ]
                                                    }
                                                ]
                                    }]
                            });

                    // Kriteria filter pada Grid # --------------
                    var FrmFilterGridDataView_viDataPasien = new Ext.Panel
                            (
                                    {
                                        title: NamaForm_viDataPasien,
                                        iconCls: icons_viDataPasien,
                                        id: mod_id_viDataPasien,
                                        region: 'center',
                                        layout: 'form',
                                        closable: true,
                                        border: false,
                                        margins: '0 5 5 0',
                                        items: [
                                            {
                                                layout: 'form',
                                                margins: '0 5 5 0',
                                                border: true,
                                                items:
                                                        [
                                                            {
                                                                xtype: 'tbspacer',
                                                                height: 3
                                                            },
                                                            {
                                                                xtype: 'textfield',
                                                                fieldLabel: '.    No. Medrec / NIK' + ' ',
                                                                id: 'txtNoMedrec_DataPasien',
                                                                anchor: '40%',
                                                                onInit: function () { },
                                                                listeners:
                                                                        {
                                                                            'specialkey': function ()
                                                                            {
                                                                                var tmpNoMedrec = Ext.get('txtNoMedrec_DataPasien').getValue()
                                                                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10)
                                                                                {
                                                                                    if (tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10)
                                                                                    {

                                                                                        var tmpgetNoMedrec = formatnomedrec(Ext.get('txtNoMedrec_DataPasien').getValue())
                                                                                        Ext.getCmp('txtNoMedrec_DataPasien').setValue(tmpgetNoMedrec);
                                                                                        var tmpkriteria = getCriteriaFilter_viDaftar();
                                                                                        DataRefresh_viDataPasien(tmpkriteria);
																						RefreshDatahistoribayar(tmpgetNoMedrec);


                                                                                    } else
                                                                                    {
                                                                                        if (tmpNoMedrec.length === 10)
                                                                                        {
                                                                                            tmpkriteria = getCriteriaFilter_viDaftar();
                                                                                            DataRefresh_viDataPasien(tmpkriteria);
																							RefreshDatahistoribayar(Ext.get('txtNoMedrec_DataPasien').getValue());
																							Ext.Ajax.request
																							(
																									{
																										url: baseURL + "index.php/rawat_jalan/viewkunjungan/ceknikpasien2",
																										params: {part_number_nik: Ext.get('txtNoMedrec_DataPasien').getValue()},
																										failure: function (o)
																										{
																											ShowPesanError_viDataPasien('Pencarian error ! ', 'Data Pasien');
																										},
																										success: function (o)
																										{
																											var cst = Ext.decode(o.responseText);
																											if (cst.success === true)
																											{
																												Ext.getCmp('txtNoMedrec_DataPasien').setValue(cst.nomedrec);
																											}
																											else
																											{
																												//Ext.getCmp('txtNoMedrec_DataPasien').setValue('');
																											}
																										}
																									}
																							) 
                                                                                        } else
                                                                                        {
                                                                                            Ext.getCmp('txtNoMedrec_DataPasien').setValue('')
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }

                                                                        }
                                                            },
                                                            {
                                                                xtype: 'tbspacer',
                                                                height: 3
                                                            },
                                                            {
                                                                xtype: 'textfield',
                                                                fieldLabel: '.   Pasien' + ' ',
                                                                name: 'txtNamaPasien_DataPasien',
                                                                id: 'txtNamaPasien_DataPasien',
                                                                enableKeyEvents: true,
                                                                anchor: '40%',
                                                                listeners:
                                                                        {
                                                                            'keyup': function ()
                                                                            {
                                                                                if (Ext.EventObject.getKey() === 13)
                                                                                {
                                                                                    var tmpkriteria = getCriteriaFilter_viDaftar();
                                                                                    DataRefresh_viDataPasien(tmpkriteria);
																					console.log(dataSourceGrid_viDataPasien.getCount());
																					var x=dataSourceGrid_viDataPasien.getCount();
																					if (x===1)
																					{
																						RefreshDatahistoribayar(Ext.get('txtNoMedrec_DataPasien').getValue());
																					}
																					else
																					{
																						RefreshDatahistoribayar();
																					}
                                                                                }
                                                                            }
                                                                        }
                                                            },
                                                            {
                                                                xtype: 'tbspacer',
                                                                height: 3
                                                            },
                                                            //getItemPanelcombofilter(),
                                                            //	getItemPaneltgl_filter()
                                                        ]},
                                            GridDataView_viDataPasien,
                                            {
                                                xtype: 'tbspacer',
                                                height: 7
                                            }, gridKunjunganpasien_editdata()]

                                    }
                            )
                    return FrmFilterGridDataView_viDataPasien;

                    //-------------- # End form pencarian # --------------
                }
// End Function dataGrid_viDataPasien # --------------

        function gridKunjunganpasien_editdata()
        {
            var fldDetail = ['KD_PASIEN', 'TGL_MASUK', 'NAMA_UNIT', 'URUT', 'DOKTER', 'KD_UNIT', 'JAM', 'TGL_KELUAR', 'CUST'];
            dskunjunganpasien = new WebApp.DataStore({fields: fldDetail})
            var griddatapasienIGD = new Ext.grid.EditorGridPanel
                    ({
                        title: 'Kunjungan',
                        stripeRows: true,
                        store: dskunjunganpasien,
                        border: true,
                        autoScroll: true,
                        columnLines: true,
                        frame: false,
                        anchor: '100% 35%',
                        sm: new Ext.grid.CellSelectionModel
                                ({
                                    singleSelect: true,
                                    listeners:
                                            {cellselect: function (sm, row, rec) {
                                                    var cellSelecteddeskripsi_datapasien = dskunjunganpasien.getAt(row);
                                                    CurrentHistorydatapasien.row = row;
                                                    CurrentHistorydatapasien.data = cellSelecteddeskripsi_datapasien;
                                                    datapasienvariable.kd_pasien = CurrentHistorydatapasien.data.data.KD_PASIEN;
                                                    datapasienvariable.urut = CurrentHistorydatapasien.data.data.URUT;
                                                    datapasienvariable.tglkunjungan = CurrentHistorydatapasien.data.data.TGL_MASUK;
                                                    datapasienvariable.kd_unit = CurrentHistorydatapasien.data.data.KD_UNIT;
                                                }}}),
                        tbar: [
                            {id: 'btnHpsdatapasien', text: 'Hapus data pasien', tooltip: 'Hapus data pasien', iconCls: 'RemoveRow',
                                handler: function () {
                                    var params = {
                                        kd_unit: datapasienvariable.kd_unit,
                                        Tglkunjungan: datapasienvariable.tglkunjungan,
                                        Kodepasein: datapasienvariable.kd_pasien,
                                        urut: datapasienvariable.urut
                                    };
                                    Ext.Ajax.request
                                            ({
                                                url: baseURL + "index.php/main/functionIGD/deletekunjungan",
                                                params: params,
                                                failure: function (o)
                                                {
                                                    loadMask.hide();
                                                    ShowPesanError_viDataPasien('data tidak bisa di hapus karena telah dilakukan pembayaran sebelumnya atau tranfer ke unit lain ', 'Hapus Data');
                                                },
                                                success: function (o)
                                                {
                                                    var cst = Ext.decode(o.responseText);
                                                    if (cst.success === true && cst.cari_trans === true && cst.cari_bayar === false)
                                                    {
                                                        ShowPesanWarning_viDataPasien('Data transaksi tidak berhasil ditemukan', 'Hapus Data Kunjungan');
                                                    } else if (cst.success === true && cst.cari_trans === true && cst.cari_bayar === true)
                                                    {
                                                        ShowPesanWarning_viDataPasien('Anda telah melakukan pembayaran', 'Hapus Data Kunjungan');
                                                    } else if (cst.success === true)
                                                    {
                                                        RefreshDatahistoribayar(datapasienvariable.kd_pasien);
                                                        ShowPesanInfo_viDataPasien('Data transaksi pasien berhasil di hapus', 'Hapus Data Kunjungan');
                                                    } else if (cst.success === false && cst.pesan === 0)
                                                    {
                                                        ShowPesanWarning_viDataPasien('Data kunjungan pasien tidak berhasil di hapus', 'Hapus Data Kunjungan');
                                                    } else
                                                    {
                                                        ShowPesanError_viDataPasien('Data kunjungan pasien tidak berhasil di hapus', 'Hapus Data Kunjungan');
                                                    }
                                                    ;
                                                }})
                                }
                            }
                        ],
                        cm: kunjunganpasiencolummodel(),
                        viewConfig: {forceFit: true}
                    });
            return griddatapasienIGD;
        }
        ;


        function RefreshDatahistoribayar(kd_pasien) {
            var strKriteriaKasirIGD = '';
            strKriteriaKasirIGD = 'kd_pasien= ~' + kd_pasien + '~';
            dskunjunganpasien.load
                    ({params: {
                            Skip: 0,
                            Take: 1000,
                            Sort: 'tgl_transaksi',
//Sort: 'tgl_transaksi',
                            Sortdir: 'ASC',
                            target: 'ViewkunjunganPasien',
                            param: strKriteriaKasirIGD
                        }});
            return dskunjunganpasien;
        }
        ;


        function kunjunganpasiencolummodel()
        {
            return new Ext.grid.ColumnModel(
                    [new Ext.grid.RowNumberer(),
                        {id: 'colKdTransaksi', header: 'No. Transaksi', dataIndex: 'KD_PASIEN', width: 100, menuDisabled: true, hidden: true},
                        {id: 'colenamunit', header: 'Poli', dataIndex: 'NAMA_UNIT', width: 80, hidden: false},
                        {id: 'coldok', header: 'Dokter', dataIndex: 'DOKTER', width: 150, menuDisabled: true, hidden: false},
                        {id: 'colkelpas', header: 'Kelompok Pasien', dataIndex: 'CUST', width: 150, menuDisabled: true, hidden: false},
                        {id: 'colTGlMasuk', header: 'Tgl Masuk', dataIndex: 'TGL_MASUK', menuDisabled: true, width: 100, renderer:
                                    function (v, params, record) {
                                        return ShowDate(record.data.TGL_MASUK);
                                    }},
                        {
                            id: 'coletglkeluar',
                            header: 'tgl Keluar',
                            menuDisabled: true,
                            dataIndex: 'TGL_KELUAR'

                        }
                        ,
                        {
                            id: 'colStatHistory',
                            header: 'Jam Masuk',
                            menuDisabled: true,
                        dataIndex: 'JAM',
                        }
                        ,
                        {
                            id: 'colStatHistory',
                            header: 'NO SEP',
                            menuDisabled: true,
                        dataIndex: 'NO_SJP',
                        }
                        ,
                        {
                            id: 'coleurutmasuk',
                            header: 'urut Masuk',
                            menuDisabled: true,
                            dataIndex: 'URUT',
                            //hidden:true

                        }



                    ]
                    )
        }
        ;
        /**
         *	Function : setLookUpGridDataView_viDataPasien
         *	
         *	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
         */
        function setLookUpGridDataView_viDataPasien(rowdata)
        {
            var lebar = 819; // Lebar Form saat PopUp
            setLookUpsGridDataView_viDataPasien = new Ext.Window
                    (
                            {
                                id: 'setLookUpsGridDataView_viDataPasien',
                                title: NamaForm_viDataPasien,
                                closeAction: 'destroy',
                                autoScroll: true,
                                width: 640,
                                height: 425,
                                resizable: false,
                                border: false,
                                plain: true,
                                layout: 'fit',
                                iconCls: icons_viDataPasien,
                                modal: true,
                                items: getFormItemEntryGridDataView_viDataPasien(lebar, rowdata),
                                listeners:
                                        {
                                            activate: function ()
                                            {
                                            },
                                            afterShow: function ()
                                            {
                                                this.activate();
                                            },
                                            deactivate: function ()
                                            {
                                                rowSelectedGridDataView_viDataPasien = undefined;
                                                DataRefresh_viDataPasien(getCriteriaFilterGridDataView_viDataPasien());
                                            }
                                        }
                            }
                    );

            setLookUpsGridDataView_viDataPasien.show();
            if (rowdata == undefined)
            {
                dataaddnew_viDataPasien();
            } else
            {
                datainit_viDataPasien(rowdata)
            }
        }
// End Function setLookUpGridDataView_viDataPasien # --------------

        /**
         *	Function : getFormItemEntryGridDataView_viDataPasien
         *	
         *	Sebuah fungsi untuk menampilkan penataan menu dan isian form edit
         *	Di pangil dari Function setLookUpGridDataView_viDataPasien
         */
        function getFormItemEntryGridDataView_viDataPasien(lebar, rowdata)
        {
            var pnlFormDataWindowPopup_viDataPasien = new Ext.FormPanel
                    (
                            {
                                title: '',
                                region: 'center',
                                fileUpload: true,
                                layout: 'anchor',
                                padding: '8px',
                                bodyStyle: 'padding:10px 0px 10px 10px;',
                                // Tombol pada tollbar Edit Data Pasien
                                tbar: {
                                    xtype: 'toolbar',
                                    items:
                                            [
                                                {
                                                    xtype: 'button',
                                                    text: 'Save',
                                                    id: 'btnSimpanWindowPopup_viDataPasien',
                                                    iconCls: 'save',
                                                    handler: function ()
                                                    {
                                                        datasave_dataPasiDataPasien(false); //1.1
                                                        DataRefresh_viDataPasien(getCriteriaFilterGridDataView_viDataPasien());
                                                    }
                                                },
                                                //-------------- ## --------------
                                                {
                                                    xtype: 'tbseparator'
                                                },
                                                //-------------- ## --------------
                                                {
                                                    xtype: 'button',
                                                    text: 'Save & Close',
                                                    id: 'btnSimpanExitWindowPopup_viDataPasien',
                                                    iconCls: 'saveexit',
                                                    handler: function ()
                                                    {
                                                        var x = datasave_dataPasiDataPasien(false); //1.2
                                                        DataRefresh_viDataPasien(getCriteriaFilterGridDataView_viDataPasien());
                                                        if (x === undefined)
                                                        {
                                                            setLookUpsGridDataView_viDataPasien.close();
                                                        }
                                                    }
                                                },
                                                //-------------- ## --------------
                                                {
                                                    xtype: 'tbseparator'
                                                },
                                                //-------------- ## --------------
                                            ]
                                },
                                //-------------- #items# --------------
                                items:
                                        [
                                            // Isian Pada Edit Data Pasien
                                            {
                                                xtype: 'fieldset',
                                                title: '',
                                                labelAlign: 'right',
                                                width: 610,
                                                height: 170,
                                                items:
                                                        [//---------------# Penampung data untuk fungsi update # ---------------
                                                            {
                                                                xtype: 'textfield',
                                                                fieldLabel: '',
                                                                id: 'TxtTmpAgama_viDataPasien',
                                                                name: 'TxtTmpAgama_viDataPasien',
                                                                readOnly: true,
                                                                hidden: true
                                                            },
                                                            {
                                                                xtype: 'textfield',
                                                                fieldLabel: '',
                                                                id: 'TxtTmpPekerjaan_viDataPasien',
                                                                name: 'TxtTmpPekerjaan_viDataPasien',
                                                                readOnly: true,
                                                                hidden: true
                                                            },
                                                            {
                                                                xtype: 'textfield',
                                                                fieldLabel: '',
                                                                id: 'TxtTmpPendidikan_viDataPasien',
                                                                name: 'TxtTmpPendidikan_viDataPasien',
                                                                readOnly: true,
                                                                hidden: true
                                                            },
                                                            //------------------------ end ---------------------------------------------								
                                                            //-------------- # Pelengkap untuk kriteria ke Net. # --------------
                                                            {
                                                                xtype: 'textfield',
                                                                fieldLabel: 'KD_CUSTOMER',
                                                                id: 'TxtWindowPopup_KD_CUSTOMER_viDataPasien',
                                                                name: 'TxtWindowPopup_KD_CUSTOMER_viDataPasien',
                                                                readOnly: true,
                                                                hidden: true
                                                            },
                                                            //-------------- ## --------------
                                                            {
                                                                xtype: 'textfield',
                                                                fieldLabel: 'URUT_MASUK',
                                                                id: 'TxtWindowPopup_Urut_Masuk_viDataPasien',
                                                                name: 'TxtWindowPopup_Urut_Masuk_viDataPasien',
                                                                readOnly: true,
                                                                hidden: true
                                                            },
                                                            //-------------- ## --------------
                                                            {
                                                                xtype: 'textfield',
                                                                fieldLabel: 'KD_PASIEN',
                                                                id: 'TxtWindowPopup_KD_PASIEN_viDataPasien',
                                                                name: 'TxtWindowPopup_KD_PASIEN_viDataPasien',
                                                                readOnly: true,
                                                                hidden: true
                                                            },
                                                            //-------------- # End Pelengkap untuk kriteria ke Net. # --------------
                                                            {
                                                                xtype: 'textfield',
                                                                fieldLabel: 'No. MedRec',
                                                                id: 'TxtWindowPopup_NO_RM_viDataPasien',
                                                                name: 'TxtWindowPopup_NO_RM_viDataPasien',
                                                                emptyText: 'No. MedRec',
                                                                labelSeparator: '',
                                                                readOnly: true,
                                                                flex: 1,
                                                                width: 195
                                                            },
                                                            //-------------- ## --------------
                                                            {
                                                                xtype: 'textfield',
                                                                fieldLabel: 'Nama',
                                                                id: 'TxtWindowPopup_NAMA_viDataPasien',
                                                                name: 'TxtWindowPopup_NAMA_viDataPasien',
                                                                emptyText: 'Nama',
                                                                labelSeparator: '',
                                                                flex: 1,
                                                                anchor: '100%'
                                                            },
                                                            //-------------- ## --------------
                                                            {
                                                                xtype: 'compositefield',
                                                                fieldLabel: 'Tempat Lahir',
                                                                labelSeparator: '',
                                                                anchor: '100%',
                                                                width: 250,
                                                                items:
                                                                        [
                                                                            {
                                                                                xtype: 'textfield',
                                                                                id: 'TxtWindowPopup_TEMPAT_LAHIR_viDataPasien',
                                                                                name: 'TxtWindowPopup_TEMPAT_LAHIR_viDataPasien',
                                                                                emptyText: 'Tempat Lahir',
                                                                                flex: 1,
                                                                                width: 195
                                                                            },
                                                                            //-------------- ## --------------
                                                                            {
                                                                                xtype: 'tbspacer',
                                                                                width: 17,
                                                                                height: 23
                                                                            },
                                                                            //-------------- ## --------------
                                                                            {
                                                                                xtype: 'displayfield',
                                                                                fieldLabel: 'Label',
                                                                                value: 'Tgl. Lahir',
                                                                                style: {'text-align': 'right'},
                                                                                id: '',
                                                                                name: '',
                                                                                flex: 1,
                                                                                width: 58
                                                                            },
                                                                            //-------------- ## --------------
                                                                            {
                                                                                xtype: 'datefield',
                                                                                fieldLabel: 'Tanggal',
                                                                                id: 'DateWindowPopup_TGL_LAHIR_viDataPasien',
                                                                                name: 'DateWindowPopup_TGL_LAHIR_viDataPasien',
                                                                                width: 198,
                                                                                format: 'd/M/Y'
                                                                            }
                                                                            //-------------- ## --------------				
                                                                        ]
                                                            },
                                                            //-------------- ## --------------
                                                            {
                                                                xtype: 'compositefield',
                                                                fieldLabel: 'Umur',
                                                                labelSeparator: '',
                                                                anchor: '100%',
                                                                width: 50,
                                                                items:
                                                                        [
                                                                            {
                                                                                xtype: 'textfield',
                                                                                id: 'TxtWindowPopup_TAHUN_viDataPasien',
                                                                                name: 'TxtWindowPopup_TAHUN_viDataPasien',
                                                                                emptyText: 'Thn',
                                                                                style: {'text-align': 'right'},
                                                                                readOnly: true,
                                                                                flex: 1,
                                                                                width: 35
                                                                            },
                                                                            //-------------- ## --------------
                                                                            {
                                                                                xtype: 'displayfield',
                                                                                fieldLabel: 'Label',
                                                                                value: 'Thn',
                                                                                id: '',
                                                                                name: '',
                                                                                flex: 1,
                                                                                width: 20
                                                                            },
                                                                            //-------------- ## --------------
                                                                            {
                                                                                xtype: 'textfield',
                                                                                id: 'TxtWindowPopup_BULAN_viDataPasien',
                                                                                name: 'TxtWindowPopup_BULAN_viDataPasien',
                                                                                emptyText: 'Bln',
                                                                                style: {'text-align': 'right'},
                                                                                readOnly: true,
                                                                                flex: 1,
                                                                                width: 35
                                                                            },
                                                                            //-------------- ## --------------
                                                                            {
                                                                                xtype: 'displayfield',
                                                                                fieldLabel: 'Label',
                                                                                value: 'Bln',
                                                                                id: '',
                                                                                name: '',
                                                                                flex: 1,
                                                                                width: 20
                                                                            },
                                                                            //-------------- ## --------------
                                                                            {
                                                                                xtype: 'textfield',
                                                                                id: 'TxtWindowPopup_HARI_viDataPasien',
                                                                                name: 'TxtWindowPopup_HARI_viDataPasien',
                                                                                emptyText: 'Hari',
                                                                                style: {'text-align': 'right'},
                                                                                readOnly: true,
                                                                                flex: 1,
                                                                                width: 35
                                                                            },
                                                                            //-------------- ## --------------
                                                                            {
                                                                                xtype: 'displayfield',
                                                                                fieldLabel: 'Label',
                                                                                value: 'Hari',
                                                                                id: '',
                                                                                name: '',
                                                                                flex: 1,
                                                                                width: 20
                                                                            }
                                                                            //-------------- ## --------------			
                                                                        ]
                                                            },
                                                            //-------------- ## --------------
                                                            {
                                                                xtype: 'compositefield',
                                                                fieldLabel: 'Jenis Kelamin',
                                                                labelSeparator: '',
                                                                anchor: '100%',
                                                                width: 250,
                                                                items:
                                                                        [
                                                                            mComboJK(),
                                                                            {
                                                                                xtype: 'displayfield',
                                                                                fieldLabel: 'Label',
                                                                                value: 'Golongan Darah',
                                                                                id: '',
                                                                                name: '',
                                                                                flex: 1,
                                                                                width: 90
                                                                            },
                                                                            mComboGolDarah()
                                                                                    //-------------- ## --------------				
                                                                        ]
                                                            },
                                                            //-------------- ## --------------						

                                                            //-------------- ## --------------
                                                        ]
                                            },
                                            //-------------- ## --------------
                                            /*{
                                             xtype: 'spacer',
                                             width: 10,
                                             height: 1
                                             },*/
                                            //-------------- ## --------------
                                            tabsWindowPopupDataLainnya_viDataPasien(rowdata)
                                                    //-------------- ## --------------
                                        ]
                                        //-------------- #items# --------------
                            }
                    )
            return pnlFormDataWindowPopup_viDataPasien;
        }
// End Function getFormItemEntryGridDataView_viDataPasien # --------------

        /**
         *	Function : tabsWindowPopupDataLainnya_viDataPasien
         *	
         *	Sebuah fungsi untuk menampilkan penataan menu dan isian form edit pada panel
         *	Di pangil dari Function getFormItemEntryGridDataView_viDataPasien
         */
        function tabsWindowPopupDataLainnya_viDataPasien(rowdata)
        {
            var TabsDataLainnya_viDataPasien = new Ext.TabPanel
                    (
                            {
                                id: 'GDtabDetailIGD',
                                region: 'center',
                                activeTab: 2,
                                anchor: '100% 400%',
                                border: false,
                                plain: true,
                                defaults:
                                        {
                                            autoScroll: true
                                        },
                                items: [
                                    tabsDetailWindowPopupDataLainnyaAktif_viDataPasien(rowdata),
                                    tabsDetailWindowPopupkontak_viDataPasien(rowdata),
                                    tabsDetailWindowPopupalamatpasien_viDataPasien(rowdata),
                                    tabsDetailWindowPopupkeluarga_viDataPasien(rowdata)


                                ]
                            }
                    );

            return TabsDataLainnya_viDataPasien;
        }
// End Function tabsWindowPopupDataLainnya_viDataPasien # --------------

        /**
         *	Function : tabsDetailWindowPopupDataLainnyaAktif_viDataPasien
         *	
         *	Sebuah fungsi untuk menampilkan penataan menu dan isian form edit pada panel
         *	Di pangil dari Function tabsWindowPopupDataLainnya_viDataPasien
         */
        function tabsDetailWindowPopupDataLainnyaAktif_viDataPasien(rowdata)
        {
            var itemstabsDetailWindowPopupDataLainnyaAktif_viDataPasien =
                    {
                        xtype: 'panel',
                        title: 'Data Pasien',
                        id: 'tabsDetailWindowPopupDataLainnyaAktifItem_viDataPasien',
                        layout: 'form',
                        bodyStyle: 'padding: 5px;',
                        labelAlign: 'right',
                        height: 110,
                        border: true,
                        items:
                                [
                                    {
                                        layout: 'column',
                                        items: [{
                                                columnWidth: .5,
                                                layout: 'form',
                                                items: [
                                                    mComboAgamaRequestEntry(),
                                                    mComboSatusMarital()
                                                ]
                                            },
                                            {
                                                columnWidth: .5,
                                                layout: 'form',
                                                items: [
                                                    mComboPekerjaanRequestEntry(),
                                                    mComboPendidikanRequestEntry()
                                                ]
                                            }]
                                    }
                                ]
                    }
            return itemstabsDetailWindowPopupDataLainnyaAktif_viDataPasien;
        }
// End Function tabsDetailWindowPopupDataLainnyaAktif_viDataPasien # --------------



        function tabsDetailWindowPopupalamatpasien_viDataPasien(rowdata)
        {
            var itemstabsDetailWindowPopupalamatpasien_viDataPasien =
                    {
                        xtype: 'panel',
                        title: 'Tempat Tinggal',
                        id: 'tabsDetailWindowPopupAlamatpasienItem_viDataPasien',
                        layout: 'form',
                        bodyStyle: 'padding: 5px;',
                        labelAlign: 'right',
                        height: 90,
                        border: true,
                        items:
                                [
                                    //-------------- ## --------------
                                    {
                                        xtype: 'textarea',
                                        fieldLabel: 'Alamat',
                                        id: 'TxtWindowPopup_ALAMAT_viDataPasien',
                                        name: 'TxtWindowPopup_ALAMAT_viDataPasien',
                                        emptyText: 'Alamat',
                                        labelSeparator: '',
                                        anchor: '100%',
                                        flex: 1
                                    },
                                    {
                                        layout: 'column',
                                        items: [{
                                                columnWidth: .5,
                                                layout: 'form',
                                                items: [
                                                    mComboPropinsi_EditdataPasien(),
                                                    mComboKabupaten_datapasien(),
                                                ]
                                            },
                                            {
                                                columnWidth: .5,
                                                layout: 'form',
                                                items: [
                                                    mComboKecamatan_editdatapasien(),
                                                    mCombokelurahan_editdatapasien()
                                                ]
                                            }]
                                    }
                                ]
                    }
            return itemstabsDetailWindowPopupalamatpasien_viDataPasien;
        }


        function loaddatastorekelurahan(kd_kecamatan)
        {
            dsKelurahan.load
                    (
                            {
                                params:
                                        {
                                            Skip: 0,
                                            Take: 1000,
                                            Sort: 'kelurahan',
                                            Sortdir: 'ASC',
                                            target: 'ViewComboKelurahan',
                                            param: 'kd_kecamatan=' + kd_kecamatan
                                        }
                            }
                    )
        }


        function mCombokelurahan_editdatapasien()
        {
            var Field = ['KD_KELURAHAN', 'KD_KECAMATAN', 'KELURAHAN'];
            dsKelurahan = new WebApp.DataStore({fields: Field});

            var cbokelurahan_editdatapasien = new Ext.form.ComboBox
                    (
                            {
                                id: 'cbokelurahan_editdatapasien',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                selectOnFocus: true,
                                forceSelection: true,
                                emptyText: 'Pilih Kelurahan...',
                                fieldLabel: 'Kelurahan',
                                align: 'Right',
                                tabIndex: 21,
//		    anchor:'60%',
                                store: dsKelurahan,
                                valueField: 'KD_KELURAHAN',
                                displayField: 'KELURAHAN',
                                anchor: '99%',
                                listeners:
                                        {
                                            'select': function (a, b, c)
                                            {
                                                kelurahanpasien = b.data.KD_KELURAHAN;
                                            },
                                            'render': function (c) {
                                                c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                    {
                                                        Ext.getCmp('txtpos').focus();
                                                    }
                                                    //atau Ext.EventObject.ENTER
                                                    else if (e.getKey() == 9) //atau Ext.EventObject.ENTER
                                                    {

                                                        kelurahanpasien = c.value;
                                                    }
                                                }, c);
                                            }
                                        }
                            }
                    );

            return cbokelurahan_editdatapasien;
        }
        ;


        function mComboPropinsi_EditdataPasien()
        {
            var Field = ['KD_PROPINSI', 'PROPINSI'];

            dsPropinsiRequestEntry = new WebApp.DataStore({fields: Field});
            dsPropinsiRequestEntry.load
                    (
                            {
                                params:
                                        {
                                            Skip: 0,
                                            Take: 1000,
                                            Sort: 'propinsi',
                                            Sortdir: 'ASC',
                                            target: 'ViewComboPropinsi',
                                            param: ''
                                        }
                            }
                    )

            var cboPropinsi_EditdataPasien = new Ext.form.ComboBox
                    (
                            {
                                id: 'cboPropinsi_EditdataPasien',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                forceSelection: true,
                                emptyText: 'Pilih a Propinsi...',
                                selectOnFocus: true,
                                fieldLabel: 'Propinsi',
                                align: 'Right',
                                store: dsPropinsiRequestEntry,
                                valueField: 'KD_PROPINSI',
                                displayField: 'PROPINSI',
                                anchor: '99%',
                                tabIndex: 18,
                                listeners:
                                        {
                                            'select': function (a, b, c)
                                            {
                                                selectPropinsiRequestEntry = b.data.KD_PROPINSI;
                                                loaddatastorekabupaten(b.data.KD_PROPINSI);
                                            },
                                            'render': function (c) {
                                                c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                    {
                                                        Ext.getCmp('cboKabupaten_editdatapasien').focus();
                                                        selectPropinsiRequestEntry = c.value;
                                                        loaddatastorekabupaten(selectPropinsiRequestEntry);
                                                    } else if (e.getKey() == 9) //atau Ext.EventObject.ENTER
                                                    {
                                                        selectPropinsiRequestEntry = c.value;
                                                        loaddatastorekabupaten(selectPropinsiRequestEntry);
                                                    }
                                                }, c);
                                            }

                                        }
                            }
                    );
            return cboPropinsi_EditdataPasien;


        }
        ;


        function mComboKabupaten_datapasien()
        {
            var Field = ['KD_KABUPATEN', 'KD_PROPINSI', 'KABUPATEN'];
            dsKabupatenRequestEntry = new WebApp.DataStore({fields: Field});

            var cboKabupaten_editdatapasien = new Ext.form.ComboBox
                    (
                            {
                                id: 'cboKabupaten_editdatapasien',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                selectOnFocus: true,
                                forceSelection: true,
                                emptyText: 'Pilih Kabupaten...',
                                fieldLabel: 'Kab/Kod',
                                align: 'Right',
                                store: dsKabupatenRequestEntry,
                                valueField: 'KD_KABUPATEN',
                                displayField: 'KABUPATEN',
                                tabIndex: 19,
                                anchor: '99%',
                                listeners:
                                        {
                                            'select': function (a, b, c)
                                            {

                                                selectKabupatenRequestEntry = b.data.KD_KABUPATEN;

                                                loaddatastorekecamatan(b.data.KD_KABUPATEN);
                                            },
                                            'render': function (c)
                                            {
                                                c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                    {
                                                        Ext.getCmp('cboKecamatan_editdatapasien').focus();
                                                        selectKabupatenRequestEntry = c.value;
                                                        loaddatastorekecamatan(selectKabupatenRequestEntry);
                                                    } else if (e.getKey() == 9) //atau Ext.EventObject.ENTER
                                                    {
                                                        selectKabupatenRequestEntry = c.value;
                                                        loaddatastorekecamatan(selectKabupatenRequestEntry);
                                                    }
                                                }, c);
                                            }

                                        }
                            }
                    );
            return cboKabupaten_editdatapasien;
        }
        ;


        function loaddatastorekabupaten(kd_propinsi)
        {
            dsKabupatenRequestEntry.load
                    (
                            {
                                params:
                                        {
                                            Skip: 0,
                                            Take: 1000,
                                            //Sort: 'DEPT_ID',
                                            Sort: 'kabupaten',
                                            Sortdir: 'ASC',
                                            target: 'ViewComboKabupaten',
                                            param: 'kd_propinsi=' + kd_propinsi
                                        }
                            }
                    )
        }

        function loaddatastorekecamatan(kd_kabupaten)
        {
            dsKecamatanRequestEntry.load
                    (
                            {
                                params:
                                        {
                                            Skip: 0,
                                            Take: 1000,
                                            Sort: 'kecamatan',
                                            Sortdir: 'ASC',
                                            target: 'ViewComboKecamatan',
                                            param: 'kd_kabupaten=' + kd_kabupaten
                                        }
                            }
                    )
        }


        function mComboKecamatan_editdatapasien()
        {
            var Field = ['KD_KECAMATAN', 'KD_KABUPATEN', 'KECAMATAN'];
            dsKecamatanRequestEntry = new WebApp.DataStore({fields: Field});

            var cboKecamatan_editdatapasien = new Ext.form.ComboBox
                    (
                            {
                                id: 'cboKecamatan_editdatapasien',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                selectOnFocus: true,
                                forceSelection: true,
                                emptyText: 'pilih Kecamatan...',
                                fieldLabel: 'Kecamatan',
                                align: 'Right',
                                tabIndex: 20,
                                store: dsKecamatanRequestEntry,
                                valueField: 'KD_KECAMATAN',
                                displayField: 'KECAMATAN',
                                anchor: '99%',
                                listeners:
                                        {
                                            'select': function (a, b, c)
                                            {
                                                selectKecamatanpasien = b.data.KD_KECAMATAN;
                                                loaddatastorekelurahan(b.data.KD_KECAMATAN)
                                            },
                                            'render': function (c) {
                                                c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() == 13)
                                                    {
                                                        Ext.getCmp('cbokelurahan_editdatapasien').focus();
                                                        selectKecamatanpasien = c.value;
                                                    }																//atau Ext.EventObject.ENTER
                                                    else if (e.getKey() == 9) //atau Ext.EventObject.ENTER
                                                    {
                                                        loaddatastorekelurahan(c.value);
                                                        selectKecamatanpasien = c.value;
                                                    }
                                                }, c);
                                            }
                                        }
                            }
                    );

            return cboKecamatan_editdatapasien;
        }
        ;


        function tabsDetailWindowPopupkontak_viDataPasien(rowdata)
        {
            var itemstabsDetailWindowPopupkontak_viDataPasien =
                    {
                        xtype: 'panel',
                        title: 'Kontak',
                        id: 'tabsDetailWindowPopupDatakontakItem_viDataPasien',
                        layout: 'form',
                        bodyStyle: 'padding: 5px;',
                        labelAlign: 'right',
                        height: 90,
                        border: true,
                        items:
                                [
                                    {
                                        layout: 'column',
                                        items: [{
                                                columnWidth: .5,
                                                layout: 'form',
                                                items: [
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: ' No. Tlpn' + ' ',
                                                        id: 'TxtWindowPopup_NO_TELP_viDataPasien',
                                                        name: 'TxtWindowPopup_NO_TELP_viDataPasien',
                                                        //emptyText: '',
                                                        anchor: '95%'
                                                    },
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: ' Email' + ' ',
                                                        id: 'TxtWindowPopup_Email_viDataPasien',
                                                        name: 'TxtWindowPopup_Email_viDataPasien',
                                                        anchor: '95%'
                                                    }
                                                ]
                                            },
                                            {
                                                columnWidth: .5,
                                                layout: 'form',
                                                items: [
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: 'No. Hp',
                                                        id: 'TxtWindowPopup_HP_viDataPasien',
                                                        name: 'TxtWindowPopup_HP_viDataPasien',
                                                        anchor: '95%'
                                                    }


                                                ]
                                            }]
                                    }
                                ]
                    }
            return itemstabsDetailWindowPopupkontak_viDataPasien;
        }




        function tabsDetailWindowPopupkeluarga_viDataPasien(rowdata)
        {
            var itemstabsDetailWindowPopupkeluarga_viDataPasien =
                    {
                        xtype: 'panel',
                        title: 'Keluarga',
                        id: 'tabsDetailWindowPopupDataKeluargaItem_viDataPasien',
                        layout: 'form',
                        bodyStyle: 'padding: 5px;',
                        labelAlign: 'right',
                        height: 90,
                        border: true,
                        items:
                                [
                                    {
                                        layout: 'column',
                                        items: [{
                                                columnWidth: .5,
                                                layout: 'form',
                                                items: [
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: 'Nama Ayah' + ' ',
                                                        id: 'TxtWindowPopup_Nama_ayah_viDataPasien',
                                                        name: 'TxtWindowPopup_Nama_ayah_viDataPasien',
                                                        //emptyText: '',
                                                        anchor: '95%'
                                                    }
                                                ]
                                            },
                                            {
                                                columnWidth: .5,
                                                layout: 'form',
                                                items: [
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: 'Nama Ibu',
                                                        id: 'TxtWindowPopup_NamaIbu_viDataPasien',
                                                        name: 'TxtWindowPopup_NamaIbu_viDataPasien',
                                                        anchor: '95%'
                                                    }


                                                ]
                                            }]
                                    }
                                ]
                    }
            return itemstabsDetailWindowPopupkeluarga_viDataPasien;
        }


        function getCriteriaFilter_viDaftar()
        {
            var strKriteria = "";

            if (Ext.get('txtNoMedrec_DataPasien').getValue() != "")
            {
                strKriteria = " pasien.kd_pasien = " + "'" + Ext.get('txtNoMedrec_DataPasien').getValue() + "'"+ " or pasien.part_number_nik= "+ "'" + Ext.get('txtNoMedrec_DataPasien').dom.value + "'" ;
				
            }

            if (Ext.get('txtNamaPasien_DataPasien').getValue() != "")
            {
                if (strKriteria == "")
                {
                    strKriteria = " lower(pasien.nama) " + "LIKE lower('%" + Ext.get('txtNamaPasien_DataPasien').getValue() + "%')";
                } else
                {
                    strKriteria += " and lower(pasien.nama) " + "LIKE lower('%" + Ext.get('txtNamaPasien_DataPasien').getValue() + "%')";
                }

            }
            /*if (Ext.get('txtTglLahir_DataPasien').getValue() != "")
             {
             if (strKriteria == "")
             {
             strKriteria = " pasien.tgl_lahir = " + "'" + Ext.get('txtTglLahir_DataPasien').getValue() +"'";
             }
             else
             {
             strKriteria += " and pasien.tgl_lahir = " + "'" + Ext.get('txtTglLahir_DataPasien').getValue() +"'";
             }
             
             }
             if (Ext.get('txtAlamatPasien_DataPasien').getValue() != "")
             {
             if (strKriteria == "")
             {
             strKriteria = " lower(pasien.alamat) " + "LIKE lower('" + Ext.get('txtAlamatPasien_DataPasien').getValue() +"%')";
             }
             else
             {
             strKriteria += " and lower(pasien.alamat) " + "LIKE lower('" + Ext.get('txtAlamatPasien_DataPasien').getValue() +"%')";
             }
             
             }
             
             //	 strKriteria += " and (K.TGL_KUNJUNGAN between '" + ShowDate(Ext.getCmp('txtfilterTglKunjAwal_viDaftar').getValue()) + "' ";
             //	 strKriteria += " and '" + ShowDate(Ext.getCmp('txtfilterTglKunjAkhir_viDaftar').getValue()) + "') ";
             //	
             
             //	 if(Ext.getCmp('txtfilterNama_viDaftar').getValue() != "")
             //	 {
             //	 	strKriteria += " and U.NAMA_UNIT LIKE '%" + Ext.get('ComboPoli_viDaftar').dom.value + "%' ";
             //	 }
             //
             //	 if(Ext.getCmp('cboPoliviDaftar').getValue() != "")
             //	 {
             //	 	strKriteria += " and RIGHT(K.KD_PASIEN,10) LIKE '%" +  Ext.getCmp('txtfilterNoRM_viDaftar').getValue() + "%' ";
             //	 }
             //
             //	 if(Ext.getCmp('txtfilterNama_viDaftar').getValue() != "")
             //	 {
             //	 	strKriteria += " and PS.NAMA LIKE '%" +  Ext.getCmp('txtfilterNama_viDaftar').getValue() + "%' ";
             //	 }
             */
            return strKriteria;
        }

// --------------------------------------- # End Form # ---------------------------------------


// End Project Data Pasien # --------------

        function mComboPekerjaanRequestEntry()
        {
            var Field = ['KD_PEKERJAAN', 'PEKERJAAN'];

            dsPekerjaanRequestEntry = new WebApp.DataStore({fields: Field});
            dsPekerjaanRequestEntry.load
                    (
                            {
                                params:
                                        {
                                            Skip: 0,
                                            Take: 1000,
                                            //Sort: 'DEPT_ID',
                                            Sort: 'pekerjaan',
                                            Sortdir: 'ASC',
                                            target: 'ViewComboPekerjaan',
                                            param: ''
                                        }
                            }
                    )

            var cboPekerjaanRequestEntry = new Ext.form.ComboBox
                    (
                            {
                                id: 'cboPekerjaanRequestEntry',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                selectOnFocus: true,
                                forceSelection: true,
                                emptyText: 'Pilih Pekerjaan...',
                                fieldLabel: 'Pekerjaan',
                                align: 'Right',
//		    anchor:'60%',
                                store: dsPekerjaanRequestEntry,
                                valueField: 'KD_PEKERJAAN',
                                displayField: 'PEKERJAAN',
                                anchor: '95%',
                                listeners:
                                        {
                                            'select': function (a, b, c)
                                            {
                                                var selectPekerjaanRequestEntry = b.data.KD_PROPINSI;
                                                Ext.getCmp('cboPekerjaanRequestEntry').setValue(b.data.KD_PEKERJAAN);
                                                Ext.getCmp('TxtTmpPekerjaan_viDataPasien').setValue(b.data.KD_PEKERJAAN);
                                            },
                                            'render': function (c) {
                                                c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                        Ext.getCmp('cboSukuRequestEntry').focus();
                                                }, c);
                                            }
                                        }
                            }
                    );

            return cboPekerjaanRequestEntry;
        }
        ;

        function mComboPendidikanRequestEntry()
        {
            var Field = ['KD_PENDIDIKAN', 'PENDIDIKAN'];

            dsPendidikanRequestEntry = new WebApp.DataStore({fields: Field});
            dsPendidikanRequestEntry.load
                    (
                            {
                                params:
                                        {
                                            Skip: 0,
                                            Take: 1000,
                                            //Sort: 'DEPT_ID',
                                            Sort: 'pendidikan',
                                            Sortdir: 'ASC',
                                            target: 'ViewComboPendidikan',
                                            param: ''
                                        }
                            }
                    )

            var cboPendidikanRequestEntry = new Ext.form.ComboBox
                    (
                            {
                                id: 'cboPendidikanRequestEntry',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                selectOnFocus: true,
                                forceSelection: true,
                                emptyText: 'Pilih Pendidikan...',
                                fieldLabel: 'Pendidikan',
                                align: 'Right',
//		    anchor:'60%',
                                store: dsPendidikanRequestEntry,
                                valueField: 'KD_PENDIDIKAN',
                                displayField: 'PENDIDIKAN',
                                anchor: '95%',
                                listeners:
                                        {
                                            'select': function (a, b, c)
                                            {
                                                var selectPendidikanRequestEntry = b.data.KD_PENDIDIKAN;
                                                Ext.getCmp('cboPendidikanRequestEntry').setValue(b.data.KD_PENDIDIKAN);
                                                Ext.getCmp('TxtTmpPendidikan_viDataPasien').setValue(b.data.KD_PENDIDIKAN);
                                            },
                                            'render': function (c) {
                                                c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                        Ext.getCmp('cboPekerjaanRequestEntry').focus();
                                                }, c);
                                            }
                                        }
                            }
                    );

            return cboPendidikanRequestEntry;
        }
        ;

        function mComboAgamaRequestEntry()
        {
            var Field = ['KD_AGAMA', 'AGAMA'];

            dsAgamaRequestEntry = new WebApp.DataStore({fields: Field});
            dsAgamaRequestEntry.load
                    (
                            {
                                params:
                                        {
                                            Skip: 0,
                                            Take: 1000,
                                            //Sort: 'DEPT_ID',
                                            Sort: 'agama',
                                            Sortdir: 'ASC',
                                            target: 'ViewComboAgama',
                                            param: ''
                                        }
                            }
                    )

            var cboAgamaRequestEntry = new Ext.form.ComboBox
                    (
                            {
                                id: 'cboAgamaRequestEntry',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                selectOnFocus: true,
                                forceSelection: true,
                                emptyText: 'Pilih Agama...',
                                fieldLabel: 'Agama',
                                align: 'Right',
//		    anchor:'60%',
                                store: dsAgamaRequestEntry,
                                valueField: 'KD_AGAMA',
                                displayField: 'AGAMA',
                                anchor: '95%',
                                listeners:
                                        {
                                            'select': function (a, b, c)
                                            {
                                                var selectAgamaRequestEntry = b.data.KD_AGAMA;
                                                Ext.getCmp('cboAgamaRequestEntry').setValue(b.data.KD_AGAMA);
                                                Ext.getCmp('TxtTmpAgama_viDataPasien').setValue(b.data.KD_AGAMA);

                                            },
                                            'render': function (c)
                                            {
                                                c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                                        Ext.getCmp('cboGolDarah').focus();
                                                }, c);
                                            }
                                        }
                            }
                    );

            return cboAgamaRequestEntry;
        }
        ;

        function mComboSatusMarital()
        {
            var cboStatusMarital = new Ext.form.ComboBox
                    (
                            {
                                id: 'cboStatusMarital',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                selectOnFocus: true,
                                forceSelection: true,
                                emptyText: 'Pilih Status...',
                                fieldLabel: 'Status Marital ',
                                width: 110,
                                anchor: '95%',
                                store: new Ext.data.ArrayStore
                                        (
                                                {
                                                    id: 0,
                                                    fields:
                                                            [
                                                                'Id',
                                                                'displayText'
                                                            ],
                                                    data: [[1, 'Blm Kawin'], [2, 'Kawin'], [3, 'Janda'], [4, 'Duda']]
                                                }
                                        ),
                                valueField: 'Id',
                                displayField: 'displayText',
                                value: selectSetSatusMarital,
                                listeners:
                                        {
                                            'select': function (a, b, c)
                                            {
                                                selectSetSatusMarital = b.data.displayText;
                                            },
                                            'render': function (c)
                                            {
                                                c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                        Ext.getCmp('txtAlamat').focus();
                                                }, c);
                                            }
                                        }
                            }
                    );
            return cboStatusMarital;
        }
        ;

        function mComboGolDarah()
        {
            var cboGolDarah = new Ext.form.ComboBox
                    (
                            {
                                id: 'cboGolDarah',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                //allowBlank: false,
                                selectOnFocus: true,
                                forceSelection: true,
                                emptyText: 'Pilih Gol. Darah...',
                                fieldLabel: 'Gol. Darah ',
                                width: 50,
                                anchor: '95%',
                                store: new Ext.data.ArrayStore
                                        (
                                                {
                                                    id: 0,
                                                    fields:
                                                            [
                                                                'Id',
                                                                'displayText'
                                                            ],
                                                    data: [[0, '-'], [1, 'A+'], [2, 'B+'], [3, 'AB+'], [4, 'O+'], [5, 'A-'], [6, 'B-'], [7, 'AB-'], [8, 'O-']]
                                                }
                                        ),
                                valueField: 'Id',
                                displayField: 'displayText',
                                value: selectSetGolDarah,
                                listeners:
                                        {
                                            'select': function (a, b, c)
                                            {
                                                selectSetGolDarah = b.data.displayText;
                                            },
                                            'render': function (c)
                                            {
                                                c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                                        Ext.getCmp('cboWarga').focus();
                                                }, c);
                                            }
                                        }
                            }
                    );
            return cboGolDarah;
        }
        ;

        function mComboJK()
        {
            var cboJK = new Ext.form.ComboBox
                    (
                            {
                                id: 'cboJK',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                selectOnFocus: true,
                                forceSelection: true,
                                emptyText: 'Pilih Jenis Kelamin...',
                                fieldLabel: 'Jenis Kelamin ',
                                width: 100,
                                store: new Ext.data.ArrayStore
                                        (
                                                {
                                                    id: 0,
                                                    fields:
                                                            [
                                                                'Id',
                                                                'displayText'
                                                            ],
                                                    data: [[1, 'Laki - Laki'], [2, 'Perempuan']]
                                                }
                                        ),
                                valueField: 'Id',
                                displayField: 'displayText',
                                value: selectSetJK,
                                listeners:
                                        {
                                            'select': function (a, b, c)
                                            {
                                                selectSetJK = b.data.displayText;
//                                        getdatajeniskelamin(b.data.displayText)
                                                //alert(jenis_kelamin)
                                            },
                                            'render': function (c) {
                                                c.getEl().on('keypress', function (e) {
                                                    if (e.getKey() === 13) //atau Ext.EventObject.ENTER
                                                        Ext.getCmp('txtTempatLahir').focus();
                                                }, c);
                                            }
                                        }
                            }
                    );
            return cboJK;
        }
        ;


        function form_histori() {
            var form = new Ext.form.FormPanel({
                baseCls: 'x-plain',
                labelWidth: 55,
                url: 'save-form.php',
                defaultType: 'textfield',
                items:
                        [
                            {
                                x: 0,
                                y: 60,
                                xtype: 'textarea',
                                id: 'TxtHistoriDeleteDataPasien',
                                hideLabel: true,
                                name: 'msg',
                                anchor: '100% 100%'  // anchor width by percentage and height by raw adjustment
                            }

                        ]
            });

            var window = new Ext.Window({
                title: 'Pesan',
                width: 400,
                height: 300,
                minWidth: 300,
                minHeight: 200,
                layout: 'fit',
                plain: true,
                bodyStyle: 'padding:5px;',
                buttonAlign: 'center',
                items: form,
                buttons: [{
                        xtype: 'button',
                        text: 'Save',
                        //height: 20,
                        id: 'btnSimpanHistori_viDataPasien',
                        iconCls: 'save',
                        handler: function ()
                        {
                            datasavehistori_viDataPasien(false); //1.1
                            window.close();
                            setLookUpsGridDataView_viDataPasien.close();
                            //DataRefresh_viDataPasien(getCriteriaFilterGridDataView_viDataPasien());
                        }
                    }]
            });

            window.show();
        }
        ;

        function item()
        {
            var pnlFormDataHistori_viDataPasien = new Ext.FormPanel
                    (
                            {
                                title: '',
                                region: 'center',
                                fileUpload: true,
                                layout: 'anchor',
                                padding: '8px',
                                bodyStyle: 'padding:10px 0px 10px 10px;',
                                items:
                                        [
                                        ]
                            }
                    );
            return pnlFormDataHistori_viDataPasien;
        }