var gridGizi={};
gridGizi.grid_store_hasil;
gridGizi.data_store_hasil;
gridGizi.data_store;
gridGizi.store_reader;
gridGizi.get_grid;
gridGizi.column;
Ext.QuickTips.init();
gridGizi.column=function(){
    return new Ext.grid.ColumnModel([
		new Ext.grid.RowNumberer(),
		{
			header 		: 'Parameter',
			dataIndex 	: 'parameter',
			flex 		: 1,
			menuDisabled: true
		},
		{
			header 		: 'Skor',
			dataIndex 	: 'skor',
			width 		: 150,
			menuDisabled: true,
			editor		: new Ext.form.TextField({
				id				: 'txt_skor',
			})
		},
		{
			header 		: 'Group',
			dataIndex 	: 'group',
			id 	: 'group_gizi',
			hidden 		: true,
		},
    ]);
}
	// shared reader
	gridGizi.store_reader = new Ext.data.ArrayReader({}, [
		{name: 'id'},
		{name: 'parameter',},
		{name: 'skor'},
		{name: 'group'},
	]);

	gridGizi.data_store = [
		[0, 'Tidak ada penurunan berat badan', 0, 'Apakah pasien mengalami penurunan berat badan yang tidak diingkan dalam 6 bulan terakhir ?'],
		[1, 'Tidak yakin (ada tanda : baju menjadi lebih longgar)', 0, 'Apakah pasien mengalami penurunan berat badan yang tidak diingkan dalam 6 bulan terakhir ?'],
		[2, 'Ya, ada penurunan BB banyak : 1-5 Kg', 0, 'Apakah pasien mengalami penurunan berat badan yang tidak diingkan dalam 6 bulan terakhir ?'],
		[3, 'Ya, ada penurunan BB banyak : 6-10 Kg', 0, 'Apakah pasien mengalami penurunan berat badan yang tidak diingkan dalam 6 bulan terakhir ?'],
		[4, 'Ya, ada penurunan BB banyak : 11-15 Kg', 0, 'Apakah pasien mengalami penurunan berat badan yang tidak diingkan dalam 6 bulan terakhir ?'],
		[5, 'Ya, ada penurunan BB banyak : > 15 Kg', 0, 'Apakah pasien mengalami penurunan berat badan yang tidak diingkan dalam 6 bulan terakhir ?'],
		[6, 'Ya, ada penurunan BB banyak : Tidak tahu berapa Kg penurunannya', 0, 'Apakah pasien mengalami penurunan berat badan yang tidak diingkan dalam 6 bulan terakhir ?'],
		
		[7, 'Ya', 0, 'Apakah asupan makanan pasien berkurang karena berkurangnya nafsu makan/ kesulitan menerima makanan ?'],
		[8, 'Tidak', 0, 'Apakah asupan makanan pasien berkurang karena berkurangnya nafsu makan/ kesulitan menerima makanan ?']
		// [8, 'Penyakit', 0, 'Pasien dengan diagnosa khusus']
	];

	gridGizi.data_store_hasil = new Ext.data.GroupingStore({
		reader 	: gridGizi.store_reader,
		data 	: gridGizi.data_store,
		sortInfo:{field: 'id', direction: "ASC"},
		groupField:'group'
	});

	/*gridGizi.data_store_hasil = new Ext.data.ArrayStore({
		fields: [
			{name: 'id'},
			{name: 'parameter',},
			{name: 'skor',},
		]
	});*/
/*gridGizi.data_store_hasil = new Ext.data.ArrayStore({
	id		: 0,
	fields	: ['id','parameter'],
	data	: [
		[0, 'Disetujui'],
		[1, 'Tdk Disetujui']
	]
});*/



gridGizi.get_grid=function(){
	console.log(gridGizi.data_store_hasil);
	gridGizi.data_store_hasil.removeAll();
    gridGizi.grid_store_hasil = new Ext.grid.EditorGridPanel({
		stripeRows: true,
		id:'grid_store_hasil_rad',
		store: gridGizi.data_store_hasil,
		border: false,
		/*view: new Ext.grid.GroupingView({
			forceFit:true,
			groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
		}),*/
		viewConfig : {
			forceFit:true,
		},
        animCollapse: false,
		columnLines: true,
		mode			: 'local',
		typeAhead		: true,
		triggerAction	: 'all',
		lazyRender		: true,
		autoScroll:true,
        height 		: 280,
        anchor 		: '100% 100%',
        sm: new Ext.grid.CellSelectionModel({
                singleSelect: true,
                listeners:{
                    cellselect: function(sm, row, rec){
                        
                    }
                }
            }
        ),
        cm: gridGizi.column(),
	    view: new Ext.grid.GroupingView({
			forceFit:true,
	        groupTextTpl: '{text}'
	    }),
    });
    return gridGizi.grid_store_hasil;
}