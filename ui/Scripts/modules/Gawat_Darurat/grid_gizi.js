var gridGizi={};
gridGizi.grid_store_hasil;
gridGizi.data_store_hasil;
gridGizi.data_store;
gridGizi.store_reader;
gridGizi.get_grid;
gridGizi.column;
gridGizi.check_radio;
gridGizi.checked_radio = {};
Ext.QuickTips.init();

gridGizi.check_radio = function(group, value){
	gridGizi.checked_radio["_"+group] = value;
	Ext.getCmp('btnSimpan_viDaftar').enable();
	tmpediting = 'true';
	console.log(gridGizi.checked_radio);
}

gridGizi.column=function(){
    return new Ext.grid.ColumnModel([
		new Ext.grid.RowNumberer(),
		{
			header 		: 'Radio',
			dataIndex 	: 'id',
			width 	 	: 50,
			renderer 	: function(value) {
				var group;
				var checked;
				if (value == 7 || value == 8) {
					group = 1;
				}else{
					group = 0;
				}

				if (DATA_ASKEP_IGD.gizi.gizi_skor_penurunan_bb == value || DATA_ASKEP_IGD.gizi.gizi_skor_sulit_makan == value ) {
					checked = "checked='checked'";
				}else{
					checked = "";
					// " + (value ? "checked='checked'" : "") + "
				}
				return "<input type='radio' name = 'primaryRadio_"+group+"' id = 'primaryRadio_"+group+"' onclick='gridGizi.check_radio("+group+", "+value+")' "+checked+">";
 			}
 		},
		{
			header 		: 'Parameter',
			dataIndex 	: 'parameter',
			flex 		: 1,
			menuDisabled: true
		},
		{
			header 		: 'Skor',
			dataIndex 	: 'skor',
			width 		: 150,
			menuDisabled: true
		},
		{
			header 		: 'Group',
			dataIndex 	: 'group',
			hidden 		: true,
			width 		: 1,
		},
    ]);
}
	// shared reader
	gridGizi.store_reader = new Ext.data.ArrayReader({}, [
		{name: 'id'},
		{name: 'parameter',},
		{name: 'skor'},
		{name: 'group'},
	]);

	gridGizi.data_store = [
		[0, 'Tidak ada penurunan berat badan', 0, 'Apakah pasien mengalami penurunan berat badan yang tidak diingkan dalam 6 bulan terakhir ?'],
		[1, 'Tidak yakin (ada tanda : baju menjadi lebih longgar)', 2, 'Apakah pasien mengalami penurunan berat badan yang tidak diingkan dalam 6 bulan terakhir ?'],
		[2, 'Ya, ada penurunan BB banyak : 1-5 Kg', 1, 'Apakah pasien mengalami penurunan berat badan yang tidak diingkan dalam 6 bulan terakhir ?'],
		[3, 'Ya, ada penurunan BB banyak : 6-10 Kg', 2, 'Apakah pasien mengalami penurunan berat badan yang tidak diingkan dalam 6 bulan terakhir ?'],
		[4, 'Ya, ada penurunan BB banyak : 11-15 Kg', 3, 'Apakah pasien mengalami penurunan berat badan yang tidak diingkan dalam 6 bulan terakhir ?'],
		[5, 'Ya, ada penurunan BB banyak : > 15 Kg', 4, 'Apakah pasien mengalami penurunan berat badan yang tidak diingkan dalam 6 bulan terakhir ?'],
		[6, 'Ya, ada penurunan BB banyak : Tidak tahu berapa Kg penurunannya', 2, 'Apakah pasien mengalami penurunan berat badan yang tidak diingkan dalam 6 bulan terakhir ?'],
		
		[7, 'Ya', 1, 'Apakah asupan makanan pasien berkurang karena berkurangnya nafsu makan/ kesulitan menerima makanan ?'],
		[8, 'Tidak', 0, 'Apakah asupan makanan pasien berkurang karena berkurangnya nafsu makan/ kesulitan menerima makanan ?'],
		// [8, 'Penyakit', 0, 'Pasien dengan diagnosa khusus']
	];

	gridGizi.data_store_hasil = new Ext.data.GroupingStore({
		reader 	: gridGizi.store_reader,
		data 	: gridGizi.data_store,
		sortInfo:{field: 'id', direction: "ASC"},
		groupField:'group'
	});

	/*gridGizi.data_store_hasil = new Ext.data.ArrayStore({
		fields: [
			{name: 'id'},
			{name: 'parameter',},
			{name: 'skor',},
		]
	});*/
/*gridGizi.data_store_hasil = new Ext.data.ArrayStore({
	id		: 0,
	fields	: ['id','parameter'],
	data	: [
		[0, 'Disetujui'],
		[1, 'Tdk Disetujui']
	]
});*/



gridGizi.get_grid=function(){
	console.log(gridGizi.data_store_hasil);
	gridGizi.data_store_hasil.removeAll();
    gridGizi.grid_store_hasil = new Ext.grid.EditorGridPanel({
		stripeRows: true,
		id:'grid_store_hasil_rad',
		store: gridGizi.data_store_hasil,
		border: false,
		/*view: new Ext.grid.GroupingView({
			forceFit:true,
			groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
		}),*/
		viewConfig : {
			forceFit:true,
		},
        animCollapse: false,
		columnLines: true,
		mode			: 'local',
		typeAhead		: true,
		triggerAction	: 'all',
		lazyRender		: true,
		autoScroll:true,
        height 		: 320,
       // anchor 		: '100% 100%',
        sm: new Ext.grid.CellSelectionModel({
                singleSelect: true,
                listeners:{
                    cellselect: function(sm, row, rec){
                        
                    }
                }
            }
        ),
        cm: gridGizi.column(),
	    view: new Ext.grid.GroupingView({
			forceFit:true,
	        groupTextTpl: '{text}'
	    }),
    });
    return gridGizi.grid_store_hasil;
}