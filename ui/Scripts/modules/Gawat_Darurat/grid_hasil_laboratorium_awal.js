var gridlaboratorium={};
gridlaboratorium.data_store_hasil_lab;
gridlaboratorium.grid_store_hasil_lab;
gridlaboratorium.get_grid;
gridlaboratorium.column;

gridlaboratorium.column=function(){
    return new Ext.grid.ColumnModel([
		new Ext.grid.RowNumberer(),
        {
			header: 'Kode Tes',
			dataIndex: 'kd_test',
			width:80,
			menuDisabled:true,
			hidden:true
		},
		{
			header: 'Item Pemeriksaan',
			dataIndex: 'judul_item',
			width:150,
			menuDisabled:true,
			renderer:function(value, metaData, record, rowIndex, colIndex, store)
			{
				metaData.style  ='font-color:#ffb3b3;';
				return value;
			}
		},
		{
			header:'Pemeriksaan',
			dataIndex: 'item_test',
			sortable: false,
			hidden:false,
			menuDisabled:true,
			width:200
			
		},
		{
			header:'Metode',
			dataIndex: 'metode',
			sortable: false,
			align: 'center',
			hidden:false,
			menuDisabled:true,
			width:90
			
		},
		{
			header:'Hasil',
			dataIndex: 'hasil',
			sortable: false,
			hidden:false,
			menuDisabled:true,
			width:90,
			align: 'right'
			
		},
		{
			header:'Normal',
			dataIndex: 'normal',
			sortable: false,
			hidden:false,
			align: 'center',
			menuDisabled:true,
			width:90
			
		},
		{
			header:'Ket Hasil',
			dataIndex: 'ket_hasil',
			sortable: false,
			hidden:false,
			align: 'center',
			menuDisabled:true,
			width:70
			
		},
		{
			header:'Satuan',
			dataIndex: 'satuan',
			sortable: false,
			hidden:false,
			menuDisabled:true,
			width:70
			
		},
		{
			header:'Keterangan',
			dataIndex: 'KET',
			width:90
			
		},
		{
			header:'Kode Lab',
			dataIndex: 'kd_lab',
			width:250,
			hidden:true
			
		}
    ]);
}

gridlaboratorium.get_grid=function(){
	var fldDetail = ['klasifikasi', 'deskripsi', 'kd_lab', 'kd_test', 'item_test', 'satuan', 'normal', 'normal_w',  
					'normal_a', 'normal_b', 'countable', 'max_m', 'min_m', 'max_f', 'min_f', 'max_a', 'min_a', 'max_b',
					'min_b', 'kd_metode', 'hasil', 'ket','kd_unit_asal','nama_unit_asal','urut','metode','judul_item','ket_hasil'];
    gridlaboratorium.data_store_hasil_lab = new WebApp.DataStore({ fields: fldDetail });
	gridlaboratorium.data_store_hasil_lab.removeAll();
    gridlaboratorium.grid_store_hasil_lab = new Ext.grid.EditorGridPanel({
        stripeRows: true,
		id:'grid_store_hasil_lab',
        store: gridlaboratorium.data_store_hasil_lab,
        border: false,
        columnLines: true,
        autoScroll:true,
        height 		: 500,
        anchor 		: '100% 100%',
        sm: new Ext.grid.CellSelectionModel({
                singleSelect: true,
                listeners:{
                    cellselect: function(sm, row, rec){
                        
                    }
                }
            }
        ),
        cm: gridlaboratorium.column(),
		viewConfig:{forceFit: true}
    });
    return gridlaboratorium.grid_store_hasil_lab;
}