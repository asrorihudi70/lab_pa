var gridRadiologi={};
gridRadiologi.grid_store_hasil;
gridRadiologi.data_store_hasil;
gridRadiologi.get_grid;
gridRadiologi.column;

gridRadiologi.column=function(){
    return new Ext.grid.ColumnModel([
		new Ext.grid.RowNumberer(),
        {
			header: 'kd_produk',
			dataIndex: 'kd_produk',
			width:80,
			menuDisabled:true,
			hidden:true
		},
		{
			header: 'Deskripsi pemeriksaan radiologi',
			dataIndex: 'deskripsi',
			width:100,
			menuDisabled:true
		},
		{
			header: 'Hasil',
			dataIndex: 'hasil',
			width:150,
			menuDisabled:true
		},
    ]);
}

gridRadiologi.get_grid=function(){
	var fldDetail = ['kd_produk','deskripsi','kd_tarif','harga','qty','desc_req','cito',
					'tgl_berlaku','no_transaksi','urut','desc_status','tgl_transaksi','jumlah',
					'kd_dokter','namadok','lunas','kd_pasien','urutkun','tglkun','kdunitkun'
					,'hasil'];
    gridRadiologi.data_store_hasil = new WebApp.DataStore({ fields: fldDetail });
	gridRadiologi.data_store_hasil.removeAll();
    gridRadiologi.grid_store_hasil = new Ext.grid.EditorGridPanel({
        stripeRows: true,
		id:'grid_store_hasil_rad',
        store: gridRadiologi.data_store_hasil,
        border: false,
        columnLines: true,
        autoScroll:true,
        height 		: 500,
        anchor 		: '100% 100%',
        sm: new Ext.grid.CellSelectionModel({
                singleSelect: true,
                listeners:{
                    cellselect: function(sm, row, rec){
                        
                    }
                }
            }
        ),
        cm: gridRadiologi.column(),
		viewConfig:{forceFit: true}
    });
    return gridRadiologi.grid_store_hasil;
}