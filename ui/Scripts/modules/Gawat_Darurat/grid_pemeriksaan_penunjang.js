var gridPemeriksaanPenunjang={};
gridPemeriksaanPenunjang.grid_store_hasil;
gridPemeriksaanPenunjang.data_store_hasil;
gridPemeriksaanPenunjang.data_store;
gridPemeriksaanPenunjang.store_reader;
gridPemeriksaanPenunjang.get_grid;
gridPemeriksaanPenunjang.column;
gridPemeriksaanPenunjang.new_record;
gridPemeriksaanPenunjang.map_record;
gridPemeriksaanPenunjang.status = false;
Ext.QuickTips.init();

gridPemeriksaanPenunjang.map_record = Ext.data.Record.create([
	{name: 'WAKTU_DIMINTA', mapping:'WAKTU_DIMINTA'},
	{name: 'PEMERIKSAAN', mapping:'PEMERIKSAAN'},
	{name: 'WAKTU_DIPERIKSA', mapping:'WAKTU_DIPERIKSA'},
	{name: 'CATATAN', mapping:'CATATAN'},
]);

gridPemeriksaanPenunjang.column=function(){
    return new Ext.grid.ColumnModel([
		new Ext.grid.RowNumberer(),
		{
			header 		: 'Waktu Diminta',
			dataIndex 	: 'WAKTU_DIMINTA',
			width 		: 120,
			menuDisabled: true,
			enableKeyEvents: true,
			editor		: new Ext.form.TextField({
				id				: 'txt_waktu_diminta',
				listeners:{
					'render': function (c) {
						c.getEl().on('keypress', function (e) {
							gridPemeriksaanPenunjang.status = true;
							tmpediting = 'true';
							Ext.getCmp('btnSimpan_viDaftar').enable();
						}, c);

						c.getEl().on('change', function (e) {
							gridPemeriksaanPenunjang.status = true;
						}, c);
					}
				}
			})
		},{
			header 		: 'Pemeriksaan',
			dataIndex 	: 'PEMERIKSAAN',
			width 		: 120,
			menuDisabled: true,
			enableKeyEvents: true,
			editor 		:  Nci.form.Combobox.autoComplete({
				store   : new Ext.data.ArrayStore({id: 0,fields: ['kd_icd9','deskripsi'],data: []}),
				select	: function(a,b,c){
					gridPemeriksaanPenunjang.status = true;
					tmpediting = 'true';
					Ext.getCmp('btnSimpan_viDaftar').enable();
				},
				param	: function(){
					var params={};
					return params;
				},
				insert	: function(o){
					return {
						kd_icd9 	: o.kd_icd9,
						deskripsi 	: o.deskripsi,
						text		: '<table style="font-size: 11px;"><tr><td width="50">'+o.kd_icd9+'</td><td width="160" align="left">'+o.deskripsi+'</td></tr></table>'
					}
				},
				url			: baseURL + "index.php/gawat_darurat/control_askep_igd/get_icd_9",
				valueField 	: 'deskripsi',
				displayField: 'deskripsi',
			})
		},{
			header 		: 'Waktu Diperiksa',
			dataIndex 	: 'WAKTU_DIPERIKSA',
			width 		: 120,
			menuDisabled: true,
			enableKeyEvents: true,
			editor		: new Ext.form.TextField({
				id				: 'txt_waktu_diperiksa',
				listeners:{
					'render': function (c) {
						c.getEl().on('keypress', function (e) {
							gridPemeriksaanPenunjang.status = true;
							tmpediting = 'true';
							Ext.getCmp('btnSimpan_viDaftar').enable();
						}, c);

						c.getEl().on('change', function (e) {
							gridPemeriksaanPenunjang.status = true;
						}, c);
					}
				}
			})
		},{
			header 		: 'Catatan',
			dataIndex 	: 'CATATAN',
			width 		: 120,
			menuDisabled: true,
			enableKeyEvents: true,
			editor		: new Ext.form.TextField({
				id				: 'txt_waktu_catatan',
				listeners:{
					'render': function (c) {
						c.getEl().on('keypress', function (e) {
							gridPemeriksaanPenunjang.status = true;
							tmpediting = 'true';
							Ext.getCmp('btnSimpan_viDaftar').enable();
						}, c);

						c.getEl().on('change', function (e) {
							gridPemeriksaanPenunjang.status = true;
						}, c);
					}
				}
			})
		},
    ]);
}
	// shared reader
	/*gridPemeriksaanPenunjang.store_reader = new Ext.data.ArrayReader({}, [
		{name: 'waktu'},
		{name: 'tindakan'},
		{name: 'dosis'},
		{name: 'cara_pemberian'},
		{name: 'waktu_pemberian'},
		{name: 'ttd_dokter'},
		{name: 'ttd_perawat'},
	]);*/

	var Field = ['WAKTU_DIMINTA','PEMERIKSAAN','WAKTU_DIPERIKSA','CATATAN'];
	gridPemeriksaanPenunjang.data_store = new WebApp.DataStore({fields: Field});
	// gridPemeriksaanPenunjang.data_store;

	/*gridPemeriksaanPenunjang.data_store_hasil = new Ext.data.GroupingStore({
		reader 	: gridPemeriksaanPenunjang.store_reader,
		data 	: gridPemeriksaanPenunjang.data_store,
		sortInfo:{field: 'id', direction: "ASC"},
		groupField:'group'
	});*/

	gridPemeriksaanPenunjang.data_store_hasil = new Ext.data.ArrayStore({
		fields: [
			{name : 'WAKTU_DIMINTA'},
			{name : 'PEMERIKSAAN'},
			{name : 'WAKTU_DIPERIKSA'},
			{name : 'CATATAN'},
		]
	});
/*gridPemeriksaanPenunjang.data_store_hasil = new Ext.data.ArrayStore({
	id		: 0,
	fields	: ['id','parameter'],
	data	: [
		[0, 'Disetujui'],
		[1, 'Tdk Disetujui']
	]
});*/

gridPemeriksaanPenunjang.get_grid=function(){
	gridPemeriksaanPenunjang.data_store.removeAll();
	gridPemeriksaanPenunjang.grid_store_hasil = new Ext.grid.EditorGridPanel({
		stripeRows: true,
		id:'grid_store_hasil_rad_gridPemeriksaanPenunjang',
		store: gridPemeriksaanPenunjang.data_store,
		border: false,
		viewConfig : {
			forceFit:true,
		},
		animCollapse: false,
		columnLines: true,
		mode			: 'local',
		border 			: true,
		bodyStyle		: 'margin-bottom:10px;',
		typeAhead		: true,
		triggerAction	: 'all',
		lazyRender		: true,
		autoScroll:true,
		title 		: 'Pemeriksaan Penunjang',
		height 		: 380,
		anchor 		: '100% 100%',
		sm: new Ext.grid.CellSelectionModel({
				singleSelect: true,
				listeners:{
					cellselect: function(sm, row, rec){
					}
				}
			}
		),
		cm: gridPemeriksaanPenunjang.column(),
		tbar : [
			{
				xtype   : 'button',
				text    : 'Tambah data',
				handler : function(){
					tmpediting = 'true';
					gridPemeriksaanPenunjang.status = true;
					Ext.getCmp('btnSimpan_viDaftar').enable();
					gridPemeriksaanPenunjang.new_record();
				}
			},{
				xtype   : 'button',
				text    : 'Hapus data',
				handler : function(){
					tmpediting = 'true';
					gridPemeriksaanPenunjang.status = true;
					Ext.getCmp('btnSimpan_viDaftar').enable();
					var line=gridPemeriksaanPenunjang.grid_store_hasil.getSelectionModel().selection.cell[0];
					gridPemeriksaanPenunjang.data_store.removeAt(line);
				}
			}
		],
	});
	return gridPemeriksaanPenunjang.grid_store_hasil;
}


gridPemeriksaanPenunjang.new_record = function(){
	var format_waktu = new Date(); 
	var p = new gridPemeriksaanPenunjang.map_record
	(
		{
			'WAKTU_DIMINTA':format_waktu.format('Y-m-d H:i:s'),
			'PEMERIKSAAN':'',
			'WAKTU_DIPERIKSA':format_waktu.format('Y-m-d H:i:s'),
			'CATATAN':'',
		}
	);
	gridPemeriksaanPenunjang.data_store.insert(gridPemeriksaanPenunjang.data_store.getCount(), p);
    // return p;
};

