var gridPemeriksaanPenunjangAwal={};
gridPemeriksaanPenunjangAwal.grid_store_hasil;
gridPemeriksaanPenunjangAwal.data_store_hasil;
gridPemeriksaanPenunjangAwal.data_store;
gridPemeriksaanPenunjangAwal.store_reader;
gridPemeriksaanPenunjangAwal.get_grid;
gridPemeriksaanPenunjangAwal.column;
gridPemeriksaanPenunjangAwal.new_record;
gridPemeriksaanPenunjangAwal.map_record;
gridPemeriksaanPenunjangAwal.status = false;
Ext.QuickTips.init();

gridPemeriksaanPenunjangAwal.map_record = Ext.data.Record.create([
	{name: 'WAKTU_DIMINTA', mapping:'WAKTU_DIMINTA'},
	{name: 'PEMERIKSAAN', mapping:'PEMERIKSAAN'},
	{name: 'WAKTU_DIPERIKSA', mapping:'WAKTU_DIPERIKSA'},
	{name: 'CATATAN', mapping:'CATATAN'},
]);

gridPemeriksaanPenunjangAwal.column=function(){
    return new Ext.grid.ColumnModel([
		new Ext.grid.RowNumberer(),
		{
			header 		: 'Waktu Diminta',
			dataIndex 	: 'WAKTU_DIMINTA',
			width 		: 80,
			menuDisabled: true,
			enableKeyEvents: true,
			editor		: new Ext.form.TextField({
				id				: 'txt_waktu_diminta',
				listeners:{
					'render': function (c) {
						c.getEl().on('keypress', function (e) {
							gridPemeriksaanPenunjangAwal.status = true;
							tmpediting = 'true';
							Ext.getCmp('btnSimpan_viDaftarAwal').enable();
						}, c);

						c.getEl().on('change', function (e) {
							gridPemeriksaanPenunjangAwal.status = true;
						}, c);
					}
				}
			})
		},{
			header 		: 'Pemeriksaan',
			dataIndex 	: 'PEMERIKSAAN',
			width 		: 120,
			menuDisabled: true,
			enableKeyEvents: true,
			editor 		:  Nci.form.Combobox.autoComplete({
				store   : new Ext.data.ArrayStore({id: 0,fields: ['kd_icd9','deskripsi'],data: []}),
				select	: function(a,b,c){
					gridPemeriksaanPenunjangAwal.status = true;
					tmpediting = 'true';
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
				},
				param	: function(){
					var params={};
					return params;
				},
				insert	: function(o){
					return {
						kd_icd9 	: o.kd_icd9,
						deskripsi 	: o.deskripsi,
						text		: '<table style="font-size: 11px;"><tr><td width="50">'+o.kd_icd9+'</td><td width="160" align="left">'+o.deskripsi+'</td></tr></table>'
					}
				},
				url			: baseURL + "index.php/gawat_darurat/control_askep_igd/get_icd_9",
				valueField 	: 'deskripsi',
				displayField: 'deskripsi',
			})
		},{
			header 		: 'Waktu Diperiksa',
			dataIndex 	: 'WAKTU_DIPERIKSA',
			width 		: 80,
			menuDisabled: true,
			enableKeyEvents: true,
			editor		: new Ext.form.TextField({
				id				: 'txt_waktu_diperiksa',
				listeners:{
					'render': function (c) {
						c.getEl().on('keypress', function (e) {
							gridPemeriksaanPenunjangAwal.status = true;
							tmpediting = 'true';
							Ext.getCmp('btnSimpan_viDaftarAwal').enable();
						}, c);

						c.getEl().on('change', function (e) {
							gridPemeriksaanPenunjangAwal.status = true;
						}, c);
					}
				}
			})
		},{
			header 		: 'Catatan',
			dataIndex 	: 'CATATAN',
			width 		: 120,
			menuDisabled: true,
			enableKeyEvents: true,
			editor		: new Ext.form.TextField({
				id				: 'txt_waktu_catatan',
				listeners:{
					'render': function (c) {
						c.getEl().on('keypress', function (e) {
							gridPemeriksaanPenunjangAwal.status = true;
							tmpediting = 'true';
							Ext.getCmp('btnSimpan_viDaftarAwal').enable();
						}, c);

						c.getEl().on('change', function (e) {
							gridPemeriksaanPenunjangAwal.status = true;
						}, c);
					}
				}
			})
		},
    ]);
}
	// shared reader
	/*gridPemeriksaanPenunjangAwal.store_reader = new Ext.data.ArrayReader({}, [
		{name: 'waktu'},
		{name: 'tindakan'},
		{name: 'dosis'},
		{name: 'cara_pemberian'},
		{name: 'waktu_pemberian'},
		{name: 'ttd_dokter'},
		{name: 'ttd_perawat'},
	]);*/

	var Field = ['WAKTU_DIMINTA','PEMERIKSAAN','WAKTU_DIPERIKSA','CATATAN'];
	gridPemeriksaanPenunjangAwal.data_store = new WebApp.DataStore({fields: Field});
	// gridPemeriksaanPenunjangAwal.data_store;

	/*gridPemeriksaanPenunjangAwal.data_store_hasil = new Ext.data.GroupingStore({
		reader 	: gridPemeriksaanPenunjangAwal.store_reader,
		data 	: gridPemeriksaanPenunjangAwal.data_store,
		sortInfo:{field: 'id', direction: "ASC"},
		groupField:'group'
	});*/

	gridPemeriksaanPenunjangAwal.data_store_hasil = new Ext.data.ArrayStore({
		fields: [
			{name : 'WAKTU_DIMINTA'},
			{name : 'PEMERIKSAAN'},
			{name : 'WAKTU_DIPERIKSA'},
			{name : 'CATATAN'},
		]
	});
/*gridPemeriksaanPenunjangAwal.data_store_hasil = new Ext.data.ArrayStore({
	id		: 0,
	fields	: ['id','parameter'],
	data	: [
		[0, 'Disetujui'],
		[1, 'Tdk Disetujui']
	]
});*/

gridPemeriksaanPenunjangAwal.get_grid=function(){
	gridPemeriksaanPenunjangAwal.data_store.removeAll();
	gridPemeriksaanPenunjangAwal.grid_store_hasil = new Ext.grid.EditorGridPanel({
		stripeRows: true,
		id:'grid_store_hasil_rad_gridPemeriksaanPenunjangAwal',
		store: gridPemeriksaanPenunjangAwal.data_store,
		border: false,
		viewConfig : {
			forceFit:true,
		},
		animCollapse: false,
		columnLines: true,
		mode			: 'local',
		border 			: true,
		bodyStyle		: 'margin-bottom:10px;',
		typeAhead		: true,
		triggerAction	: 'all',
		lazyRender		: true,
		autoScroll:true,
		title 		: 'Pemeriksaan Penunjang',
		height 		: 380,
		anchor 		: '100% 100%',
		sm: new Ext.grid.CellSelectionModel({
				singleSelect: true,
				listeners:{
					cellselect: function(sm, row, rec){
					}
				}
			}
		),
		cm: gridPemeriksaanPenunjangAwal.column(),
		tbar : [
			{
				xtype   : 'button',
				text    : 'Tambah data',
				handler : function(){
					tmpediting = 'true';
					gridPemeriksaanPenunjangAwal.status = true;
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
					gridPemeriksaanPenunjangAwal.new_record();
				}
			},{
				xtype   : 'button',
				text    : 'Hapus data',
				handler : function(){
					tmpediting = 'true';
					gridPemeriksaanPenunjangAwal.status = true;
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
					var line=gridPemeriksaanPenunjangAwal.grid_store_hasil.getSelectionModel().selection.cell[0];
					gridPemeriksaanPenunjangAwal.data_store.removeAt(line);
				}
			}
		],
	});
	return gridPemeriksaanPenunjangAwal.grid_store_hasil;
}


gridPemeriksaanPenunjangAwal.new_record = function(){
	var format_waktu = new Date(); 
	var p = new gridPemeriksaanPenunjangAwal.map_record
	(
		{
			'WAKTU_DIMINTA':format_waktu.format('Y-m-d H:i:s'),
			'PEMERIKSAAN':'',
			'WAKTU_DIPERIKSA':format_waktu.format('Y-m-d H:i:s'),
			'CATATAN':'',
		}
	);
	gridPemeriksaanPenunjangAwal.data_store.insert(gridPemeriksaanPenunjangAwal.data_store.getCount(), p);
    // return p;
};

