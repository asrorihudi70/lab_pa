var gridPerawat={};
gridPerawat.grid_store_hasil;
gridPerawat.data_store_hasil;
gridPerawat.data_store;
gridPerawat.data_store_;
gridPerawat.store_reader;
gridPerawat.get_grid;
gridPerawat.column;
gridPerawat.store_dokter = new Ext.data.ArrayStore({id: 0,fields:['kd_dokter','nama'],data: []});
Ext.QuickTips.init();
gridPerawat.column=function(){
	chkgetDiagnosa = new Ext.grid.CheckColumn({
		id: 'chkgetDiagnosa',
		header: 'Pilih',
		align: 'center',
		//disabled:false,
		dataIndex: 'CHECK_DIAGNOSA',
		anchor: '10% 100%',
		width: 30,
	});
	chkgetIntervensi = new Ext.grid.CheckColumn({
		id: 'chkgetIntervensi',
		header: 'Pilih',
		align: 'center',
		//disabled:false,
		dataIndex: 'CHECK_INTERVENSI',
		anchor: '10% 100%',
		width: 30,
	});
    return new Ext.grid.ColumnModel([
		new Ext.grid.RowNumberer(),
		chkgetDiagnosa,
		{
			header 		: 'Diagnosa Perawat',
			dataIndex 	: 'DIAGNOSA',
			width 		: 240,
			/*renderer: function (value, meta, record, rowIndex, colIndex, store) {
				var first = !rowIndex || value !== store.getAt(rowIndex - 1).get('diagnosa'),
				last = rowIndex >= store.getCount() - 1 || value !== store.getAt(rowIndex + 1).get('diagnosa');
				meta.css += 'row-span' + (first ? ' row-span-first' : '') +  (last ? ' row-span-last' : '');
				if (first) {
					var i = rowIndex + 1;
					while (i < store.getCount() && value === store.getAt(i).get('diagnosa')) {
						i++;
					}
					var rowHeight = 20, padding = 6,
					height = (rowHeight * (i - rowIndex) - padding) + 'px';
					meta.attr = 'style="height:' + height + ';line-height:' + height + ';"';
				}
				if (value != '') {
					return '<input type ="checkbox" id="'+record.data.diagnosa+'_'+rowIndex+'" name="'+record.data.diagnosa+'_'+rowIndex+'">'+value;
				}else{
					return first ? value : '';
				}
			},*/
		},
		chkgetIntervensi,
		{
			header 		: 'Intervensi',
			dataIndex 	: 'INTERVENSI',
			width 		: 250,
			/*renderer: function (value, meta, record, rowIndex, colIndex, store) {
				var first = !rowIndex || value !== store.getAt(rowIndex - 1).get('evaluasi'),
				last = rowIndex >= store.getCount() - 1 || value !== store.getAt(rowIndex + 1).get('evaluasi');
				meta.css += 'row-span' + (first ? ' row-span-first' : '') +  (last ? ' row-span-last' : '');
				if (first) {
					var i = rowIndex + 1;
					while (i < store.getCount() && value === store.getAt(i).get('evaluasi')) {
						i++;
					}
					var rowHeight = 20, padding = 6,
					height = (rowHeight * (i - rowIndex) - padding) + 'px';
					meta.attr = 'style="height:' + height + ';line-height:' + height + ';"';
				}
				if (value != '') {
					return '<input type ="checkbox" id="'+record.data.diagnosa+'_'+rowIndex+'" name="'+record.data.diagnosa+'_'+rowIndex+'">'+value;
				}else{
					return first ? value : '';
				}
			},*/
		},{
			header 		: 'Evaluasi',
			dataIndex 	: 'EVALUASI',
			width 		: 240,
			menuDisabled: true,
			editor		: new Ext.form.TextField({
				id				: 'txt_evaluasi',
			}),
		},{
			header 		: 'Paraf',
			dataIndex 	: 'PARAF',
			width 		: 150,
			menuDisabled: true,
			editor 		:  Nci.form.Combobox.autoComplete({
				store	: gridPerawat.store_dokter,
				select	: function(a,b,c){
					var line = gridPerawat.grid_store_hasil.getSelectionModel().selection.cell[0];
					gridPerawat.data_store_hasil.getRange()[line].set('PARAF',b.data.nama);
					// Ext.getCmp('col_ttd_dokter').setValue(b.data.nama);
					// gridPerawat.grid_store_hasil.store.data.items[line].data.paraf = b.data.nama;
					// gridPerawat.grid_store_hasil.getView().refresh();
					console.log(gridPerawat.data_store_hasil.getRange()[line]);
					// console.log(gridPerawat.grid_store_hasil);
					gridPerawat.status = true;
					tmpediting = 'true';
					Ext.getCmp('btnSimpan_viDaftar').enable();
				},
				param	: function(){
					var params={};
					params['kd_job'] 	= '1';
					return params;
				},
				insert	: function(o){
					return {
						kd_dokter       : o.kd_dokter,
						nama       		: o.nama,
						text			:  '<table style="font-size: 11px;"><tr><td width="50">'+o.kd_dokter+'</td><td width="160" align="left">'+o.nama+'</td></tr></table>'
					}
				},
				url		: baseURL + "index.php/gawat_darurat/control_askep_igd/get_dokter",
				valueField: 'deskripsi',
				displayField: 'text',
			})
		},{
			header 		: 'ID Diagnosa',
			dataIndex 	: 'ID_DIAGNOSA',
			hidden 		: true,
		},{
			header 		: 'ID Intervensi',
			dataIndex 	: 'ID_INTERVENSI',
			hidden 		: true,
		},{
			header 		: 'ID Group',
			dataIndex 	: 'ID_GROUP',
			hidden 		: true,
		},{
			header 		: 'Group',
			dataIndex 	: 'group',
			hidden 		: true,
		},
    ]);
}
	gridPerawat.store_reader = new Ext.data.ArrayReader({}, [
		{name : 'CHECK_DIAGNOSA'},
		{name : 'DIAGNOSA'},
		{name : 'CHECK_INTERVENSI'},
		{name : 'INTERVENSI'},
		{name : 'EVALUASI'},
		{name : 'PARAF'},
		{name : 'ID_DIAGNOSA'},
		{name : 'ID_INTERVENSI'},
		{name : 'ID_GROUP'},
		{name : 'group'},
	]);

	var Field = ['ID_DIAGNOSA','ID_INTERVENSI','ID_GROUP','CHECK_DIAGNOSA','CHECK_INTERVENSI','KD_PASIEN','TGL_MASUK','KD_UNIT','URUT_MASUK','DIAGNOSA','EVALUASI','INTERVENSI','GROUP','PARAF'];
	gridPerawat.data_store = new WebApp.DataStore({fields: Field});
	
	gridPerawat.data_store_hasil = new Ext.data.GroupingStore({
		reader 		: gridPerawat.store_reader,
		// data 		: gridPerawat.data_store,
		sortInfo	: {field: 'group', direction: "ASC"},
		groupField 	: 'group'
	});

	// shared reader
	// gridPerawat.data_store = [
	// 	[0, '', '', '', '', ''],
	// ];
	/*gridPerawat.data_store_ = [
		[0, 'Bersih jalan nafas tidak efektif', '<b>Implementasi : </b> Observasi produk sputum, jumlah, warna, dan kekentalan', '', '', 'Bersih jalan nafas tidak efektif'],
		[1, '', '<b>Implementasi : </b> lakukan suction jika perlu', '', '', 'Bersih jalan nafas tidak efektif'],
		[2, '', '<b>Implementasi : </b> Ajarkan pasien untuk nafas dalam dan batuk efeisien', '', '', 'Bersih jalan nafas tidak efektif'],
		[3, '', '<b>Implementasi : </b> Berikan posisi semi fowler atau posisi miring yang aman', '', '', 'Bersih jalan nafas tidak efektif'],
		[4, '', '<b>Implementasi : </b> Berikan fisioterapi dada jika perlu', '', '', 'Bersih jalan nafas tidak efektif'],
		[5, '', '<b>Kolaborasi : </b> Kollar Neck', '', '', 'Bersih jalan nafas tidak efektif'],
		[6, '', '<b>Kolaborasi : </b> Suction', '', '', 'Bersih jalan nafas tidak efektif'],
		[7, '', '<b>Kolaborasi : </b> OPA', '', '', 'Bersih jalan nafas tidak efektif'],
		[8, '', '<b>Kolaborasi : </b> Head tilt - chin lift', '', '', 'Bersih jalan nafas tidak efektif'],
		[9, '', '<b>Kolaborasi : </b> Jaw thrust', '', '', 'Bersih jalan nafas tidak efektif'],
		[10, '', '<b>Kolaborasi : </b> Nebulize', '', '', 'Bersih jalan nafas tidak efektif'],

		[11, 'Pola Nafas tidak efektif', '<b>Implementasi : </b> Monitor pernapasan : irama, pengembangan dinding dada, penggunaan otot tambahan pernapasan', '', '', 'Pola Nafas tidak efektif'],
		[12, 'Gangguan pertukaran gas', '<b>Implementasi : </b> Berikan oksigen', 'liter/i', '', 'Pola Nafas tidak efektif'],
		[13, '', '<b>Implementasi : </b> Berikan posisi semi fowler jika tidak ada kontraindikasi', '', '', 'Pola Nafas tidak efektif'],
		[14, '', '<b>Implementasi : </b> Ajarkan teknik rileksasi nafas dalam', '', '', 'Pola Nafas tidak efektif'],
		[15, '', '<b>Kolaborasi : </b> AGD', '', '', 'Pola Nafas tidak efektif'],
		[16, '', '<b>Kolaborasi : </b> Nasal Kanul', '', '', 'Pola Nafas tidak efektif'],
		[17, '', '<b>Kolaborasi : </b> Masker', '', '', 'Pola Nafas tidak efektif'],
		[18, '', '<b>Kolaborasi : </b> NRM', '', '', 'Pola Nafas tidak efektif'],

		[19, 'Penurunan curah jantung', '<b>Implementasi : </b> Monitor tanda - tanda shock : TD menurun, nadi cepat dan lemah', '', '', 'Penurunan curah jantung'],
		[20, 'Hipovolemia', '<b>Implementasi : </b> Observasi tanda-tanda perdarahan atau pengeluaran cairan (Muntah,diare,dehidrasi dll)', '', '', 'Penurunan curah jantung'],
		[21, 'Diare', '<b>Implementasi : </b> Observasi tanda-tanda perubahan mental (Gelisah, bingung, penurunan kesadaran)', '', '', 'Penurunan curah jantung'],
		[22, '', '<b>Implementasi : </b> Observasi perfusi perifer (turgor, membrane, mukosa, dan CRT)', '', '', 'Penurunan curah jantung'],
		[23, '', '<b>Implementasi : </b> Berikan cairan peroral jika tidak ada kontraindikasi', '', '', 'Penurunan curah jantung'],
		[24, '', '<b>Implementasi : </b> Lakukan monitoring balance cairan', '', '', 'Penurunan curah jantung'],
		[25, '', '<b>Kolaborasi : </b> Resusitasi cairan', '', '', 'Penurunan curah jantung'],
		[26, '', '<b>Kolaborasi : </b> IVFD', '', '', 'Penurunan curah jantung'],
		[27, '', '<b>Kolaborasi : </b> Pemeriksaan hitung darah lengkap', '', '', 'Penurunan curah jantung'],
		[28, '', '<b>Kolaborasi : </b> kateter urin', '', '', 'Penurunan curah jantung'],

		[29, 'Penurunan kapasitas adaptif', '<b>Implementasi : </b> Observasi tanda-tanda peningkatan TIK (Sakit kepala mual muntah, penglihatan ganda dll)', '', '', 'Penurunan kapasitas adaptif'],
		[30, 'Intoleransi aktifitas', '<b>Implementasi : </b> Elevasi kepala 15-30 (HOB) jika tidak ada kontraindikasi', '', '', 'Penurunan kapasitas adaptif'],
		[31, '', '<b>Implementasi : </b> Berikan oksigen', 'liter/i', '', 'Penurunan kapasitas adaptif'],
		[32, '', '<b>Implementasi : </b> Lakukan pencegahan resiko jatuh tinggi', '', '', 'Penurunan kapasitas adaptif'],
		[33, '', '<b>Implementasi : </b> Batas aktivitas untuk mengurangi cedera', '', '', 'Penurunan kapasitas adaptif'],
		[34, '', '<b>Implementasi : </b> Edukasi keluarga untuk mendampingi pasien', '', '', 'Penurunan kapasitas adaptif'],
		[35, '', '<b>Kolaborasi : </b> O2', '', '', 'Penurunan kapasitas adaptif'],
		[36, '', '<b>Kolaborasi : </b> Pasang OPA', '', '', 'Penurunan kapasitas adaptif'],
		[37, '', '<b>Kolaborasi : </b> CT - Scan', '', '', 'Penurunan kapasitas adaptif'],
		[38, '', '<b>Kolaborasi : </b> MRI', '', '', 'Penurunan kapasitas adaptif'],
		[39, '', '<b>Kolaborasi : </b> Obat-obatan (Manitol, dll)', '', '', 'Penurunan kapasitas adaptif'],
		[40, '', '<b>Kolaborasi : </b> Pembedahan', '', '', 'Penurunan kapasitas adaptif'],

		[41, 'Gangguan integritas kulit/jaringan', '<b>Implementasi : </b> Kaji tanda-tanda perdarahan dan atau karakteristik luka', '', '', 'Gangguan integritas kulit/jaringan'],
		[42, '<b>Nyeri</b> Akut', '<b>Implementasi : </b> Lakukan balut tekan untuk mengurangi perdarahan', '', '', 'Gangguan integritas kulit/jaringan'],
		[43, '<b>Nyeri</b> Kronis', '<b>Implementasi : </b> Pasang bidai dan batasi aktivitas pada daerah yang dicurigai fraktur', '', '', 'Gangguan integritas kulit/jaringan'],
		[44, 'Hipertemia', '<b>Implementasi : </b> Lakukan perawatan luka (redressing) dengan teknik aseptic', '', '', 'Gangguan integritas kulit/jaringan'],
		[45, 'Hipotermi', '<b>Implementasi : </b> Berikan kompres panas/dingin pada area cidera', '', '', 'Gangguan integritas kulit/jaringan'],
		[46, 'Resiko infeksi', '<b>Implementasi : </b> Kaji karakteristik nyeri', '', '', 'Gangguan integritas kulit/jaringan'],
		[47, '', '<b>Implementasi : </b> Ajarkan teknik rileksasi nafas dalam untuk skala nyeri ringan-sedang', '', '', 'Gangguan integritas kulit/jaringan'],
		[48, '', '<b>Kolaborasi : </b> Hecting', '', '', 'Gangguan integritas kulit/jaringan'],
		[49, '', '<b>Kolaborasi : </b> Rontgen', '', '', 'Gangguan integritas kulit/jaringan'],
		[50, '', '<b>Kolaborasi : </b> Analgetik', '', '', 'Gangguan integritas kulit/jaringan'],
		[51, '', '<b>Kolaborasi : </b> Antibiotik', '', '', 'Gangguan integritas kulit/jaringan'],
		[52, '', '<b>Kolaborasi : </b> ATS', '', '', 'Gangguan integritas kulit/jaringan'],
		[53, '', '<b>Kolaborasi : </b> Antipiretik', '', '', 'Gangguan integritas kulit/jaringan'],
		
		[54, 'Ketidakstabilan gula darah', '<b>Implementasi : </b> Observasi tanda-tanda hipoglikemia/ hiperglikemia', '', '', 'Ketidakstabilan gula darah'],
		[55, 'Reterensi urin', '<b>Implementasi : </b> Observasi tanda-tanda penurunan kesadaran', '', '', 'Ketidakstabilan gula darah'],
		[56, 'Konstipasi', '<b>Kolaborasi : </b> Managemen hipoglikemia/ hiperglikemia', '', '', 'Ketidakstabilan gula darah'],
		[57, '', '<b>Kolaborasi : </b> Kateter urin', '', '', 'Ketidakstabilan gula darah'],
		[58, '', '<b>Kolaborasi : </b> Huknah', '', '', 'Ketidakstabilan gula darah'],
	];*/

	/*gridPerawat.data_store_hasil = new Ext.data.ArrayStore({
		fields: [
			{name: 'id'},
			{name: 'parameter',},
			{name: 'skor',},
		]
	});*/
/*gridPerawat.data_store_hasil = new Ext.data.ArrayStore({
	id		: 0,
	fields	: ['id','parameter'],
	data	: [
		[0, 'Disetujui'],
		[1, 'Tdk Disetujui']
	]
});*/



gridPerawat.get_grid=function(){
	// console.log(gridPerawat.data_store_hasil);
	// gridPerawat.data_store_hasil.removeAll();
    // gridPerawat.data_store_hasil.loadData(gridPerawat.data_store);
    gridPerawat.grid_store_hasil = new Ext.grid.EditorGridPanel({
		stripeRows: true,
		id:'grid_store_hasil_rad',
		store: gridPerawat.data_store_hasil,
		border: false,
		viewConfig : {
			forceFit:true,
		},
        animCollapse: false,
		columnLines: true,
		mode			: 'local',
		typeAhead		: true,
		triggerAction	: 'all',
		lazyRender		: true,
		autoScroll:true,
        height 		: 460,
        anchor 		: '100% 100%',
        sm: new Ext.grid.CellSelectionModel({
                singleSelect: true,
                listeners:{
                    cellselect: function(sm, row, rec){
                        
                    }
                }
            }
        ),
        cm: gridPerawat.column(),
	    view: new Ext.grid.GroupingView({
			forceFit:true,
	        groupTextTpl: '{text}'
	    }),
    });
    return gridPerawat.grid_store_hasil;
}