var gridTandaVitalAwal={};
gridTandaVitalAwal.grid_store_hasil;
gridTandaVitalAwal.data_store_hasil;
gridTandaVitalAwal.data_store;
gridTandaVitalAwal.store_reader;
gridTandaVitalAwal.get_grid;
gridTandaVitalAwal.column;
gridTandaVitalAwal.map_record;
gridTandaVitalAwal.new_record;
gridTandaVitalAwal.status = false;
Ext.QuickTips.init();

gridTandaVitalAwal.map_record = Ext.data.Record.create([
	{name: 'WAKTU', mapping: 'WAKTU'},
	{name: 'TD', mapping: 'TD'},
	{name: 'N', mapping: 'N'},
	{name: 'P', mapping: 'P'},
	{name: 'SAO2', mapping: 'SAO2'},
	{name: 'IRAMA_JANTUNG', mapping: 'IRAMA_JANTUNG'},
]);

gridTandaVitalAwal.column=function(){
    return new Ext.grid.ColumnModel([
		new Ext.grid.RowNumberer(),
		{
			header 		: 'Waktu',
			dataIndex 	: 'WAKTU',
			enableKeyEvents: true,
			width 		: 120,
			menuDisabled: true,
			editor		: new Ext.form.TextField({
				id				: 'txt_td',
				enableKeyEvents : true,
				listeners:{
					'render': function (c) {
						c.getEl().on('keypress', function (e) {
							gridTandaVitalAwal.status = true;
							tmpediting = 'true';
							Ext.getCmp('btnSimpan_viDaftarAwal').enable();
						}, c);

						c.getEl().on('change', function (e) {
							gridTandaVitalAwal.status = true;
						}, c);
					}
				}
			})
		},{
			header 		: 'TD',
			dataIndex 	: 'TD',
			width 		: 120,
			menuDisabled: true,
			enableKeyEvents: true,
			editor		: new Ext.form.TextField({
				id				: 'txt_td',
				enableKeyEvents : true,
				listeners:{
					'render': function (c) {
						c.getEl().on('keypress', function (e) {
							gridTandaVitalAwal.status = true;
							tmpediting = 'true';
							Ext.getCmp('btnSimpan_viDaftarAwal').enable();
						}, c);

						c.getEl().on('change', function (e) {
							gridTandaVitalAwal.status = true;
						}, c);
					}
				}
			})
		},{
			header 		: 'N',
			dataIndex 	: 'N',
			width 		: 120,
			menuDisabled: true,
			enableKeyEvents: true,
			editor		: new Ext.form.TextField({
				id				: 'txt_n',
				enableKeyEvents : true,
				listeners:{
					'render': function (c) {
						c.getEl().on('keypress', function (e) {
							gridTandaVitalAwal.status = true;
							tmpediting = 'true';
							Ext.getCmp('btnSimpan_viDaftarAwal').enable();
						}, c);

						c.getEl().on('change', function (e) {
							gridTandaVitalAwal.status = true;
						}, c);
					}
				}
			})
		},{
			header 		: 'P',
			dataIndex 	: 'P',
			width 		: 120,
			menuDisabled: true,
			enableKeyEvents: true,
			editor		: new Ext.form.TextField({
				id				: 'txt_p',
				enableKeyEvents : true,
				listeners:{
					'render': function (c) {
						c.getEl().on('keypress', function (e) {
							gridTandaVitalAwal.status = true;
							tmpediting = 'true';
							Ext.getCmp('btnSimpan_viDaftarAwal').enable();
						}, c);

						c.getEl().on('change', function (e) {
							gridTandaVitalAwal.status = true;
						}, c);
					}
				}
			})
		},{
			header 		: 'SaO2',
			dataIndex 	: 'SAO2',
			width 		: 120,
			menuDisabled: true,
			enableKeyEvents: true,
			editor		: new Ext.form.TextField({
				id				: 'txt_sao2',
				enableKeyEvents : true,
				listeners:{
					'render': function (c) {
						c.getEl().on('keypress', function (e) {
							gridTandaVitalAwal.status = true;
							tmpediting = 'true';
							Ext.getCmp('btnSimpan_viDaftarAwal').enable();
						}, c);

						c.getEl().on('change', function (e) {
							gridTandaVitalAwal.status = true;
						}, c);
					}
				}
			})
		},{
			header 		: 'Irama Jantung',
			dataIndex 	: 'IRAMA_JANTUNG',
			width 		: 120,
			menuDisabled: true,
			enableKeyEvents: true,
			editor		: new Ext.form.TextField({
				id				: 'txt_irama_jantung',
				enableKeyEvents : true,
				listeners:{
					'render': function (c) {
						c.getEl().on('keypress', function (e) {
							gridTandaVitalAwal.status = true;
							tmpediting = 'true';
							Ext.getCmp('btnSimpan_viDaftarAwal').enable();
						}, c);

						c.getEl().on('change', function (e) {
							gridTandaVitalAwal.status = true;
						}, c);
					}
				}
			})
		},
    ]);
}
	// shared reader
	/*gridTandaVitalAwal.store_reader = new Ext.data.ArrayReader({}, [
		{name: 'waktu'},
		{name: 'tindakan'},
		{name: 'dosis'},
		{name: 'cara_pemberian'},
		{name: 'waktu_pemberian'},
		{name: 'ttd_dokter'},
		{name: 'ttd_perawat'},
	]);*/

	var Field = ['WAKTU','TD','N','P','SAO2','IRAMA_JANTUNG'];
	gridTandaVitalAwal.data_store = new WebApp.DataStore({fields: Field});

	gridTandaVitalAwal.data_store_hasil = new Ext.data.ArrayStore({
		fields: [
			{name : 'WAKTU'},
			{name : 'TD'},
			{name : 'N'},
			{name : 'P'},
			{name : 'SAO2'},
			{name : 'IRAMA_JANTUNG'},
		]
	});
/*gridTandaVitalAwal.data_store_hasil = new Ext.data.ArrayStore({
	id		: 0,
	fields	: ['id','parameter'],
	data	: [
		[0, 'Disetujui'],
		[1, 'Tdk Disetujui']
	]
});*/



gridTandaVitalAwal.get_grid=function(){
	gridTandaVitalAwal.data_store.removeAll();
    gridTandaVitalAwal.grid_store_hasil = new Ext.grid.EditorGridPanel({
		stripeRows: true,
		id:'grid_store_hasil_rad_gridTandaVitalAwal',
		store: gridTandaVitalAwal.data_store,
		border: false,
		viewConfig : {
			forceFit:true,
		},
        animCollapse: false,
		columnLines: true,
		mode			: 'local',
		typeAhead		: true,
		triggerAction	: 'all',
		lazyRender		: true,
		autoScroll:true,
		title 		: 'Data Tanda Vital',
        height 		: 280,
        anchor 		: '100% 100%',
        sm: new Ext.grid.CellSelectionModel({
                singleSelect: true,
                listeners:{
                    cellselect: function(sm, row, rec){
                        
                    }
                }
            }
        ),
        cm: gridTandaVitalAwal.column(),
       	tbar : [
       	    {
       	        xtype   : 'button',
       	        text    : 'Tambah data',
       	        handler : function(){
       	        	gridTandaVitalAwal.status = true;
					tmpediting = 'true';
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
       	        	gridTandaVitalAwal.new_record();
       	        }
       	    },{
				xtype   : 'button',
				text    : 'Hapus data',
				handler : function(){
					gridTandaVitalAwal.status = true;
					tmpediting = 'true';
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
					var line=gridTandaVitalAwal.grid_store_hasil.getSelectionModel().selection.cell[0];
					gridTandaVitalAwal.data_store.removeAt(line);
				}
			}
       	],
    });
    return gridTandaVitalAwal.grid_store_hasil;
}

gridTandaVitalAwal.new_record = function(){
	var format_waktu = new Date(); 
	var p = new gridTataLaksana.map_record
	(
		{
			'WAKTU' : format_waktu.format('Y-m-d H:i:s'),
			'TD' : '',
			'N' : '',
			'P' : '',
			'SAO2' : '',
			'IRAMA_JANTUNG' : '',
		}
	);
	gridTandaVitalAwal.data_store.insert(gridTandaVitalAwal.data_store.getCount(), p);
    // return p;
};

