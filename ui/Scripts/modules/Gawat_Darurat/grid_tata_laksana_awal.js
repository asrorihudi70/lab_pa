var gridTataLaksanaAwal={};
gridTataLaksanaAwal.grid_store_hasil;
gridTataLaksanaAwal.data_store_hasil;
gridTataLaksanaAwal.data_store;
gridTataLaksanaAwal.store_reader;
gridTataLaksanaAwal.get_grid;
gridTataLaksanaAwal.column;
gridTataLaksanaAwal.map_record;
gridTataLaksanaAwal.new_record;
gridTataLaksanaAwal.status = false;
gridTataLaksanaAwal.store_dokter = new Ext.data.ArrayStore({id: 0,fields:['kd_dokter','nama'],data: []});
Ext.QuickTips.init();

gridTataLaksanaAwal.map_record = Ext.data.Record.create([
	{name: 'WAKTU', mapping: 'WAKTU'},
	{name: 'TINDAKAN', mapping: 'TINDAKAN'},
	{name: 'DOSIS', mapping: 'DOSIS'},
	{name: 'CARA_PEMBERIAN', mapping: 'CARA_PEMBERIAN'},
	{name: 'WAKTU_PEMBERIAN', mapping: 'WAKTU_PEMBERIAN'},
	{name: 'TTD_DOKTER', mapping: 'TTD_DOKTER'},
	{name: 'TTD_PERAWAT', mapping: 'TTD_PERAWAT'},
]);

gridTataLaksanaAwal.column=function(){
    return new Ext.grid.ColumnModel([
		new Ext.grid.RowNumberer(),
		{
			header 		: 'Waktu',
			dataIndex 	: 'WAKTU',
			width 		: 120,
			menuDisabled: true,
			enableKeyEvents: true,
			editor		: new Ext.form.TextField({
				id				: 'txt_waktu',
				listeners:{
					'render': function (c) {
						c.getEl().on('keypress', function (e) {
							gridTataLaksanaAwal.status = true;
							tmpediting = 'true';
							Ext.getCmp('btnSimpan_viDaftarAwal').enable();
						}, c);

						c.getEl().on('change', function (e) {
							gridTataLaksanaAwal.status = true;
						}, c);
					}
				}
			})
		},{
			header 		: 'Terapi atau tindakan',
			dataIndex 	: 'TINDAKAN',
			width 		: 240,
			menuDisabled: true,
			enableKeyEvents: true,
			editor 		:  Nci.form.Combobox.autoComplete({
				store       : new Ext.data.ArrayStore({id: 0,fields: ['kd_icd9','deskripsi'],data: []}),
				select	: function(a,b,c){
					tmpediting = 'true';
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
				},
				param	: function(){
					var params={};
					params['kd_job'] 	= '1';
					return params;
				},
				insert	: function(o){
					return {
						kd_icd9       	: o.kd_icd9,
						deskripsi       : o.deskripsi,
						text			: '<table style="font-size: 11px;"><tr><td width="50">'+o.kd_icd9+'</td><td width="160" align="left">'+o.deskripsi+'</td></tr></table>'
					}
				},
				id 		: 'col_ttd_dokter',
				url		: baseURL + "index.php/gawat_darurat/control_askep_awal_igd/get_icd_9",
				valueField: 'deskripsi',
				displayField: 'deskripsi',
			})
		},{
			header 		: 'Dosis',
			dataIndex 	: 'DOSIS',
			width 		: 120,
			menuDisabled: true,
			enableKeyEvents: true,
			editor		: new Ext.form.TextField({
				id				: 'txt_dosis',
				listeners:{
					'render': function (c) {
						c.getEl().on('keypress', function (e) {
							gridTataLaksanaAwal.status = true;
							tmpediting = 'true';
							Ext.getCmp('btnSimpan_viDaftarAwal').enable();
						}, c);

						c.getEl().on('change', function (e) {
							gridTataLaksanaAwal.status = true;
						}, c);
					}
				}
			})
		},{
			header 		: 'Cara Pemberian',
			dataIndex 	: 'CARA_PEMBERIAN',
			width 		: 120,
			menuDisabled: true,
			enableKeyEvents: true,
			editor		: new Ext.form.TextField({
				id				: 'txt_cara_pemberian',
				listeners:{
					'render': function (c) {
						c.getEl().on('keypress', function (e) {
							gridTataLaksanaAwal.status = true;
							tmpediting = 'true';
							Ext.getCmp('btnSimpan_viDaftarAwal').enable();
						}, c);

						c.getEl().on('change', function (e) {
							gridTataLaksanaAwal.status = true;
						}, c);
					}
				}
			})
		},{
			header 		: 'TTD Dokter',
			dataIndex 	: 'TTD_DOKTER',
			width 		: 120,
			menuDisabled: true,
			editor 		:  Nci.form.Combobox.autoComplete({
				store	: gridTataLaksanaAwal.store_dokter,
				select	: function(a,b,c){
					var line = gridTataLaksanaAwal.grid_store_hasil.getSelectionModel().selection.cell[0];
					gridTataLaksanaAwal.data_store.getRange()[line].set('TTD_DOKTER',b.data.nama);
					gridTataLaksanaAwal.status = true;
					tmpediting = 'true';
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
				},
				param	: function(){
					var params={};
					params['kd_job'] 	= '1';
					return params;
				},
				insert	: function(o){
					return {
						kd_dokter       : o.kd_dokter,
						nama       		: o.nama,
						text			:  '<table style="font-size: 11px;"><tr><td width="50">'+o.kd_dokter+'</td><td width="160" align="left">'+o.nama+'</td></tr></table>'
					}
				},
				id 		: 'col_ttd_dokter',
				url		: baseURL + "index.php/gawat_darurat/control_askep_awal_igd/get_dokter",
				valueField: 'deskripsi',
				displayField: 'text',
			})
		},{
			header 		: 'Waktu Pemberian',
			dataIndex 	: 'WAKTU_PEMBERIAN',
			width 		: 120,
			menuDisabled: true,
			enableKeyEvents: true,
			editor		: new Ext.form.TextField({
				id				: 'txt_cara_waktu_pemberian',
				listeners:{
					'render': function (c) {
						c.getEl().on('keypress', function (e) {
							gridTataLaksanaAwal.status = true;
							tmpediting = 'true';
							Ext.getCmp('btnSimpan_viDaftarAwal').enable();
						}, c);

						c.getEl().on('change', function (e) {
							gridTataLaksanaAwal.status = true;
						}, c);
					}
				}
			})
		},{
			header 		: 'TTD Perawat',
			dataIndex 	: 'TTD_PERAWAT',
			width 		: 120,
			menuDisabled: true,
			editor 		:  Nci.form.Combobox.autoComplete({
				store	: gridTataLaksanaAwal.store_dokter,
				select	: function(a,b,c){
					var line = gridTataLaksanaAwal.grid_store_hasil.getSelectionModel().selection.cell[0];
					gridTataLaksanaAwal.data_store.getRange()[line].set('TTD_PERAWAT',b.data.nama);
					gridTataLaksanaAwal.status = true;
					tmpediting = 'true';
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
				},
				param	: function(){
					var params={};
					params['kd_job'] 	= '0';
					return params;
				},
				insert	: function(o){
					return {
						kd_dokter       : o.kd_dokter,
						nama       		: o.nama,
						text			:  '<table style="font-size: 11px;"><tr><td width="50">'+o.kd_dokter+'</td><td width="160" align="left">'+o.nama+'</td></tr></table>'
					}
				},
				id 		: 'col_ttd_perawat',
				url		: baseURL + "index.php/gawat_darurat/control_askep_awal_igd/get_dokter",
				valueField: 'deskripsi',
				displayField: 'text',
			})
		},
    ]);
}
	// shared reader
	/*gridTataLaksanaAwal.store_reader = new Ext.data.ArrayReader({}, [
		{name: 'waktu'},
		{name: 'tindakan'},
		{name: 'dosis'},
		{name: 'cara_pemberian'},
		{name: 'waktu_pemberian'},
		{name: 'ttd_dokter'},
		{name: 'ttd_perawat'},
	]);*/


	var Field = ['WAKTU','TINDAKAN','DOSIS','CARA_PEMBERIAN','WAKTU_PEMBERIAN','TTD_DOKTER','TTD_PERAWAT'];
	gridTataLaksanaAwal.data_store = new WebApp.DataStore({fields: Field});

	/*gridTataLaksanaAwal.data_store_hasil = new Ext.data.GroupingStore({
		reader 	: gridTataLaksanaAwal.store_reader,
		data 	: gridTataLaksanaAwal.data_store,
		sortInfo:{field: 'id', direction: "ASC"},
		groupField:'group'
	});*/

	gridTataLaksanaAwal.data_store_hasil = new Ext.data.ArrayStore({
		fields: [
			{name: 'WAKTU'},
			{name: 'TINDAKAN'},
			{name: 'DOSIS'},
			{name: 'CARA_PEMBERIAN'},
			{name: 'WAKTU_PEMBERIAN'},
			{name: 'TTD_DOKTER'},
			{name: 'TTD_PERAWAT'},
		]
	});
/*gridTataLaksanaAwal.data_store_hasil = new Ext.data.ArrayStore({
	id		: 0,
	fields	: ['id','parameter'],
	data	: [
		[0, 'Disetujui'],
		[1, 'Tdk Disetujui']
	]
});*/



gridTataLaksanaAwal.get_grid=function(){
	gridTataLaksanaAwal.data_store.removeAll();
    gridTataLaksanaAwal.grid_store_hasil = new Ext.grid.EditorGridPanel({
		stripeRows: true,
		id:'grid_store_hasil_rad_gridTataLaksanaAwal',
		store: gridTataLaksanaAwal.data_store,
		border: false,
		viewConfig : {
			forceFit:true,
			getRowClass: function(record, index, rowParams, ds) {
				// rowParams.tstyle = 'width:' + this.getTotalWidth() + ';';
				// if (set_background) {
				// rowParams.tstyle += "background-color:green;";
				// }
				// if (set_foreground) {
				// 	rowParams.style += "color:red;";
				// }
				console.log(rowParams);
			}
		},
        animCollapse: false,
		columnLines: true,
		mode			: 'local',
		typeAhead		: true,
		triggerAction	: 'all',
		lazyRender		: true,
		autoScroll:true,
		title 		: 'Data Tata Laksana',
        height 		: 280,
        anchor 		: '100% 100%',
        sm: new Ext.grid.CellSelectionModel({
                singleSelect: true,
                listeners:{
                    cellselect: function(sm, row, rec){
                        
                    }
                }
            }
        ),
        cm: gridTataLaksanaAwal.column(),
       	tbar : [
       	    {
       	        xtype   : 'button',
       	        text    : 'Tambah data',
       	        handler : function(){
       	        	gridTataLaksanaAwal.status = true;
					tmpediting = 'true';
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
       	        	gridTataLaksanaAwal.new_record();
       	        }
       	    },{
				xtype   : 'button',
				text    : 'Hapus data',
				handler : function(){
					gridTataLaksanaAwal.status = true;
					tmpediting = 'true';
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
					var line=gridTataLaksanaAwal.grid_store_hasil.getSelectionModel().selection.cell[0];
					gridTataLaksanaAwal.data_store.removeAt(line);
				}
			}
       	],
    });
    return gridTataLaksanaAwal.grid_store_hasil;
}


gridTataLaksanaAwal.new_record = function(){
	var format_waktu = new Date(); 
	var p = new gridTataLaksanaAwal.map_record
	(
		{
			'WAKTU' : format_waktu.format('Y-m-d H:i:s'),
			'TINDAKAN' : '',
			'DOSIS' : '',
			'CARA_PEMBERIAN' : '',
			'WAKTU_PEMBERIAN' : format_waktu.format('Y-m-d H:i:s'),
			'TTD_DOKTER' : '',
			'TTD_PERAWAT' : '',
		}
	);
	gridTataLaksanaAwal.data_store.insert(gridTataLaksanaAwal.data_store.getCount(), p);
    // return p;
};

