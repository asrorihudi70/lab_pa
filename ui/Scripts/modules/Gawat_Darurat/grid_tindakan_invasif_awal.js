var gridTindakanInvasifAwal={};
gridTindakanInvasifAwal.grid_store_hasil;
gridTindakanInvasifAwal.data_store_hasil;
gridTindakanInvasifAwal.data_store;
gridTindakanInvasifAwal.data_invasif = [];
gridTindakanInvasifAwal.store_reader;
gridTindakanInvasifAwal.get_grid;
gridTindakanInvasifAwal.column;
gridTindakanInvasifAwal.status;
gridTindakanInvasifAwal.data_checklist = [];
gridTindakanInvasifAwal.store_dokter = new Ext.data.ArrayStore({id: 0,fields:['kd_dokter','nama'],data: []});
Ext.QuickTips.init();

gridTindakanInvasifAwal.check_radio = function(id){
	// gridGizi.checked_radio["_"+group] = value;
	Ext.getCmp('btnSimpan_viDaftarAwal').enable();
	tmpediting = 'true';
	gridTindakanInvasifAwal.data_checklist.push(id);
	var x=$("#primaryCheck_"+id).is(":checked");
	// gridTindakanInvasifAwal.data_invasif = [];
	// alert(value);
	// console.log(gridTindakanInvasifAwal.data_store);
	// var length      = haystack.length;
	/*if (gridTindakanInvasifAwal.data_checklist.length == 0) {
		gridTindakanInvasifAwal.data_checklist.push(value);	
	}else{
		for (var i = 0; i < gridTindakanInvasifAwal.data_checklist.length; i++) {
			if(gridTindakanInvasifAwal.data_checklist[i] == value){
				console.log("sama");
				console.log(gridTindakanInvasifAwal.data_checklist[i]);
			}else{
				console.log("beda");
				gridTindakanInvasifAwal.data_checklist.push(value);
			}
		}
	}
	console.log(gridTindakanInvasifAwal.data_checklist);*/

	for (var i = 0; i < gridTindakanInvasifAwal.grid_store_hasil.store.data.length; i++) {
		if(id == gridTindakanInvasifAwal.grid_store_hasil.store.data.items[i].data.id && x === true) {
			var data_store = {};
			data_store.id         = gridTindakanInvasifAwal.grid_store_hasil.store.data.items[i].data.id;
			data_store.pelaksana  = gridTindakanInvasifAwal.grid_store_hasil.store.data.items[i].data.pelaksana;
			data_store.keterangan = gridTindakanInvasifAwal.grid_store_hasil.store.data.items[i].data.keterangan;
			// console.log("ID "+gridTindakanInvasifAwal.grid_store_hasil.store.data.items[i].data.id);
			// console.log("Dokter "+gridTindakanInvasifAwal.grid_store_hasil.store.data.items[i].data.pelaksana);
			// console.log("keterangan "+gridTindakanInvasifAwal.grid_store_hasil.store.data.items[i].data.keterangan);
			gridTindakanInvasifAwal.data_invasif.push(data_store);
		}
	}

	for (var i = 0; i < gridTindakanInvasifAwal.data_invasif.length; i++) {
		if(id == gridTindakanInvasifAwal.data_invasif[i].id && x === false){
			console.log(i);
       		// arr.splice(index, 1);
			gridTindakanInvasifAwal.data_invasif.splice(i, 1);
		}
	}
	console.log(gridTindakanInvasifAwal.data_invasif);
}

gridTindakanInvasifAwal.column=function(){
	chkgetTindakanPenjasRad = new Ext.grid.CheckColumn({
		id: 'chkgetTindakanPenjasRad',
		header: 'Pilih',
		align: 'center',
		//disabled:false,
		dataIndex: 'CHECKED',
		anchor: '10% 100%',
		width: 30,
		listeners : {
			render : function(a){
				console.log(a);
			}
		}
	}); 
    return new Ext.grid.ColumnModel([
		new Ext.grid.RowNumberer(),
		chkgetTindakanPenjasRad,
		{
			header 		: 'Ceklist Tindakan',
			dataIndex 	: 'CHECKLIST',
			width 		: 400,
			menuDisabled: true,
		},{
			header 		: 'No/ keterangan',
			dataIndex 	: 'KETERANGAN',
			width 		: 150,
			menuDisabled: true,
			editor		: new Ext.form.TextField({
				id		: 'txt_no',
			}),
		},{
			header 		: 'Pelaksana',
			dataIndex 	: 'PELAKSANA',
			width 		: 400,
			menuDisabled: true,
			editor 		:  Nci.form.Combobox.autoComplete({
				store	: gridTindakanInvasifAwal.store_dokter,
				select	: function(a,b,c){
					// var line = gridTindakanInvasifAwal.grid_store_hasil.getSelectionModel().selection.cell[0];
					gridTindakanInvasifAwal.status = true;
					tmpediting = 'true';
					Ext.getCmp('btnSimpan_viDaftarAwal').enable();
				},
				param	: function(){
					var params={};
					params['kd_job'] 	= '1';
					return params;
				},
				insert	: function(o){
					return {
						kd_dokter       : o.kd_dokter,
						nama       		: o.nama,
						text			:  '<table style="font-size: 11px;"><tr><td width="50">'+o.kd_dokter+'</td><td width="160" align="left">'+o.nama+'</td></tr></table>'
					}
				},
				id 		: 'col_ttd_dokter',
				url		: baseURL + "index.php/gawat_darurat/control_askep_igd/get_dokter",
				valueField: 'nama',
				displayField: 'nama',
			})
		},{
			header 		: 'Group',
			dataIndex 	: 'group',
			hidden 		: true,
		},
    ]);
}
	var Field = ['URUT','KETERANGAN','PELAKSANA','CHECKLIST','CHECKED'];
	gridTindakanInvasifAwal.data_store = new WebApp.DataStore({fields: Field});

	gridTindakanInvasifAwal.template_data_store = [
		[0, 'Pemasangan pipa lambung', '', '', '-'],
		[1, 'Pemasangan kateter urin', '', '', '-'],
		[2, 'Airway', '', '', '-'],
		[3, 'OPA', '', '', '-'],
		[4, 'NPA', '', '', '-'],
		[5, 'Oksigen', '', '', '-'],
		[6, 'NC', '', '', '-'],
		[7, 'SM', '', '', '-'],
		[8, 'NRM', '', '', '-'],
		[9, 'RM', '', '', '-'],
		[10, 'CPAP', '', '', '-'],
		[11, 'FLOW', 'L/menit', '', '-'],
		[12, 'FiO2', '', '', '-'],
		[13, 'Chest Tube', '', '', '-'],
		[14, 'Intubasi', '', '', '-'],
		[15, 'Oral', '', '', '-'],
		[16, 'Nasal', '', '', '-'],
		[17, 'Ett', '', '', '-'],


		// [8, 'Penyakit', 0, 'Pasien dengan diagnosa khusus']
	];

	// gridTindakanInvasifAwal.data_store_hasil = new Ext.data.GroupingStore({
	// 	reader 		: gridTindakanInvasifAwal.store_reader,
	// 	data 		: gridTindakanInvasifAwal.data_store,
	// 	sortInfo	: {field: 'id', direction: "ASC"},
	// 	groupField 	: 'group'
	// });

	/*gridTindakanInvasifAwal.data_store_hasil = new Ext.data.ArrayStore({
		fields: [
			{name: 'id'},
			{name: 'parameter',},
			{name: 'skor',},
		]
	});*/
/*gridTindakanInvasifAwal.data_store_hasil = new Ext.data.ArrayStore({
	id		: 0,
	fields	: ['id','parameter'],
	data	: [
		[0, 'Disetujui'],
		[1, 'Tdk Disetujui']
	]
});*/



gridTindakanInvasifAwal.get_grid=function(){
	gridTindakanInvasifAwal.data_store.removeAll();
    gridTindakanInvasifAwal.grid_store_hasil = new Ext.grid.EditorGridPanel({
		stripeRows: true,
		id:'grid_tindakan_invasif',
		store: gridTindakanInvasifAwal.data_store,
		border: false,
		viewConfig : {
			forceFit:true,
		},
		columnPlugins 	: [1, 2],
        animCollapse 	: false,
		columnLines 	: true,
		mode			: 'local',
		typeAhead		: true,
		triggerAction	: 'all',
		lazyRender		: true,
		cls: 'grid-row-span',
		autoScroll:true,
        height 		: 460,
        anchor 		: '100% 100%',
        sm: new Ext.grid.CellSelectionModel({
                singleSelect: true,
                listeners:{
                    cellselect: function(sm, row, rec){
                        
                    }
                }
            }
        ),
        cm: gridTindakanInvasifAwal.column(),
    });
    return gridTindakanInvasifAwal.grid_store_hasil;
}