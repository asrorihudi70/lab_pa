var gridTrease={};
gridTrease.grid_store_hasil;
gridTrease.data_store_hasil;
gridTrease.data_store;
gridTrease.store_reader;
gridTrease.get_grid;
gridTrease.column;
gridTrease.updateRowSpan;
Ext.QuickTips.init();
gridTrease.column=function(){
    return new Ext.grid.ColumnModel([
		new Ext.grid.RowNumberer(),
		{
			header 		: 'Pemeriksaan',
			dataIndex 	: 'pemeriksaan',
			width 		: 300,
			height 		: 50,
			sortable : false,
            columnLines: true,
			menuDisabled: true,
			renderer: function (value, meta, record, rowIndex, colIndex, store) {
				var first = !rowIndex || value !== store.getAt(rowIndex - 1).get('pemeriksaan'),
				last = rowIndex >= store.getCount() - 1 || value !== store.getAt(rowIndex + 1).get('pemeriksaan');
				meta.css += 'row-span' + (first ? ' row-span-first' : '') +  (last ? ' row-span-last' : '');
				if (first) {
					var i = rowIndex + 1;
					while (i < store.getCount() && value === store.getAt(i).get('pemeriksaan')) {
						i++;
					}
					var rowHeight = 20, padding = 6,
					height = (rowHeight * (i - rowIndex) - padding) + 'px';
					meta.attr = 'style="height:' + height + ';line-height:' + height + ';"';
				}
				return first ? value : '';
			},
		},{
			header 		: 'Skala Trease 1',
			dataIndex 	: 'trease_1',
			width 		: 300,
			height 		: 50,
			sortable : false,
            columnLines: true,
			menuDisabled: true,
			renderer  : function (value, meta, record, rowIndex, colIndex, store) {
				console.log(value);
				console.log(meta);
				console.log(record);
				console.log(rowIndex);
				console.log(colIndex);
				console.log(store);
				var first = !rowIndex || value !== store.getAt(rowIndex - 1).get('trease_1'),
				last = rowIndex >= store.getCount() - 1 || value !== store.getAt(rowIndex + 1).get('trease_1');
				meta.css += 'row-span' + (first ? ' row-span-first' : '') +  (last ? ' row-span-last' : '');
				if (first) {
					var i = rowIndex + 1;
					while (i < store.getCount() && value === store.getAt(i).get('trease_1')) {
						i++;
					}
					var rowHeight = 20, padding = 6,
					height = (rowHeight * (i - rowIndex) - padding) + 'px';
					meta.attr = 'style="height:' + height + ';line-height:' + height + ';"';
				}
				if (value != '') {
                	return '<input type="checkbox" name="checkbox" id="'+record.data.pemeriksaan+'_'+rowIndex+'" name="'+record.data.pemeriksaan+'_'+rowIndex+'">'+value;
				}else{
					return first ? value : '';
				}
			} 
		},{
			header 		: 'Skala Trease 2',
			dataIndex 	: 'trease_2',
			width 		: 300,
			height 		: 50,
			sortable : false,
            columnLines: true,
			menuDisabled: true,
			renderer  : function (value, meta, record, rowIndex, colIndex, store) {
				var first = !rowIndex || value !== store.getAt(rowIndex - 1).get('trease_2'),
				last = rowIndex >= store.getCount() - 1 || value !== store.getAt(rowIndex + 1).get('trease_2');
				meta.css += 'row-span' + (first ? ' row-span-first' : '') +  (last ? ' row-span-last' : '');
				if (first) {
					var i = rowIndex + 1;
					while (i < store.getCount() && value === store.getAt(i).get('trease_2')) {
						i++;
					}
					var rowHeight = 20, padding = 6,
					height = (rowHeight * (i - rowIndex) - padding) + 'px';
					meta.attr = 'style="height:' + height + ';line-height:' + height + ';"';
				}
				if (value != '') {
                	return '<input type="checkbox" name="checkbox" id="'+record.data.pemeriksaan+'_'+rowIndex+'" name="'+record.data.pemeriksaan+'_'+rowIndex+'">'+value;
				}else{
					return first ? value : '';
				}
			} 
		},{
			header 		: 'Skala Trease 3',
			dataIndex 	: 'trease_3',
			width 		: 300,
			height 		: 50,
			sortable : false,
            columnLines: true,
			menuDisabled: true,
			renderer  : function (value, meta, record, rowIndex, colIndex, store) {
				var first = !rowIndex || value !== store.getAt(rowIndex - 1).get('trease_3'),
				last = rowIndex >= store.getCount() - 1 || value !== store.getAt(rowIndex + 1).get('trease_3');
				meta.css += 'row-span' + (first ? ' row-span-first' : '') +  (last ? ' row-span-last' : '');
				if (first) {
					var i = rowIndex + 1;
					while (i < store.getCount() && value === store.getAt(i).get('trease_3')) {
						i++;
					}
					var rowHeight = 20, padding = 6,
					height = (rowHeight * (i - rowIndex) - padding) + 'px';
					meta.attr = 'style="height:' + height + ';line-height:' + height + ';"';
				}
				if (value != '') {
                	return '<input type="checkbox" name="checkbox" id="'+record.data.pemeriksaan+'_'+rowIndex+'" name="'+record.data.pemeriksaan+'_'+rowIndex+'">'+value;
				}else{
					return first ? value : '';
				}
			} 
		},{
			header 		: 'Skala Trease 4',
			dataIndex 	: 'trease_4',
			width 		: 300,
			height 		: 50,
			sortable : false,
            columnLines: true,
			menuDisabled: true,
			renderer  : function (value, meta, record, rowIndex, colIndex, store) {
				var first = !rowIndex || value !== store.getAt(rowIndex - 1).get('trease_4'),
				last = rowIndex >= store.getCount() - 1 || value !== store.getAt(rowIndex + 1).get('trease_4');
				meta.css += 'row-span' + (first ? ' row-span-first' : '') +  (last ? ' row-span-last' : '');
				if (first) {
					var i = rowIndex + 1;
					while (i < store.getCount() && value === store.getAt(i).get('trease_4')) {
						i++;
					}
					var rowHeight = 20, padding = 6,
					height = (rowHeight * (i - rowIndex) - padding) + 'px';
					meta.attr = 'style="height:' + height + ';line-height:' + height + ';"';
				}
				if (value != '') {
                	return '<input type="checkbox" name="checkbox" id="'+record.data.pemeriksaan+'_'+rowIndex+'" name="'+record.data.pemeriksaan+'_'+rowIndex+'">'+value;
				}else{
					return first ? value : '';
				}
			} 
		},{
			header 		: 'Skala Trease 5',
			dataIndex 	: 'trease_5',
			width 		: 300,
			height 		: 50,
			sortable : false,
            columnLines: true,
			menuDisabled: true,
			renderer  : function (value, meta, record, rowIndex, colIndex, store) {
				var first = !rowIndex || value !== store.getAt(rowIndex - 1).get('trease_5'),
				last = rowIndex >= store.getCount() - 1 || value !== store.getAt(rowIndex + 1).get('trease_5');
				meta.css += 'row-span' + (first ? ' row-span-first' : '') +  (last ? ' row-span-last' : '');
				if (first) {
					var i = rowIndex + 1;
					while (i < store.getCount() && value === store.getAt(i).get('trease_5')) {
						i++;
					}
					var rowHeight = 20, padding = 6,
					height = (rowHeight * (i - rowIndex) - padding) + 'px';
					meta.attr = 'style="height:' + height + ';line-height:' + height + ';"';
				}
				if (value != '') {
                	return '<input type="checkbox" name="checkbox" id="'+record.data.pemeriksaan+'_'+rowIndex+'" name="'+record.data.pemeriksaan+'_'+rowIndex+'">'+value;
				}else{
					return first ? value : '';
				}
			} 
		},
    ]);
}
	// shared reader
	// gridTrease.store_reader = new Ext.data.ArrayReader({}, [
	// 	{name: 'id'},
	// 	{name: 'pemeriksaan'},
	// 	{name: 'trease_1'},
	// 	{name: 'trease_2'},
	// 	{name: 'trease_3'},
	// 	{name: 'trease_4'},
	// 	{name: 'trease_5'},
	// 	{name: 'group'},
	// ]);

    // create the data store
    gridTrease.store_reader = new Ext.data.ArrayStore({
        fields: [
			{name: 'pemeriksaan'},
			{name: 'trease_1'},
			{name: 'trease_2'},
			{name: 'trease_3'},
			{name: 'trease_4'},
			{name: 'trease_5'},
        ]
    });
	gridTrease.data_store = [
		['Jalan Nafas' 	,'Sumbatan' 			,'Bebas' 					,'Bebas' 			,'Bebas'				 	,'Bebas'],
		['' 	,'' 					,'Ancaman' 					,'' 			,''				 	,''],

		['Pernafasan' 	,'Henti Nafas' 			,'Takipnoe' 				,'Normal' 			,'Frekuensi Nafas Normal' 	,'Frekuensi Nafas Normal'],
		['' 	,'Bradipnoe' 			,'Mengi' 					,'Mengi' 			,'' 						,''],
		['' 	,'Sianosis' 			,'' 						,'' 				,'' 						,''],


		['Sirkulasi' 	,'Henti Jantung' 		,'Nadi Teraba Lemah' 		,'Nadi Kuat' 		,'Nadi Kuat' 				,'Nadi Kuat'],
		['' 	,'Nadi Tidak Teraba'	,'Bradikardi' 				,'Takikardi' 		,'Frekuensi Nadi Normal'	,'Frekuensi Nadi Normal'],
		['' 	,'Akral Dingin'			,'Takikardi' 				,'TDS > 160 mmHg' 	,'TDS 140 - 180 mmHg'		,'TD Normal'],
		['' 	,''						,'Pucat' 					,'TDD > 100 mmHg' 	,'TDD 90 - 100 mmHg'		,''],
		['' 	,''						,'Akral Dingin'				,'' 				,''							,''],

		['Kesadaran' 	,'GCS <= 8' 			,'GCS 9 - 12' 				,'GCS > 12'		,'GCS 15' 				,'GCS 15'],
		['' 	,'Kejang'				,'Gelisah' 					,'Apatis' 		,''						,''],
		['' 	,'Tidak Ada Respon'		,'Hemiparese' 				,'Somnolen' 	,''						,''],
		['' 	,''						,'Nyeri Dada' 				,'' 			,''						,''],
	];

	/*gridTrease.data_store_hasil = new Ext.data.GroupingStore({
		reader 	: gridTrease.store_reader,
		data 	: gridTrease.data_store,
		sortInfo:{field: 'id', direction: "ASC"},
		groupField:'group'
	});*/

	/*gridTrease.data_store_hasil = new Ext.data.ArrayStore({
		fields: [
			{name: 'id'},
			{name: 'parameter',},
			{name: 'skor',},
		]
	});*/
/*gridTrease.data_store_hasil = new Ext.data.ArrayStore({
	id		: 0,
	fields	: ['id','parameter'],
	data	: [
		[0, 'Disetujui'],
		[1, 'Tdk Disetujui']
	]
});*/

gridTrease.get_grid=function(){
	gridTrease.store_reader.removeAll();
	gridTrease.store_reader.loadData(gridTrease.data_store);
	console.log(gridTrease.store_reader);
    gridTrease.grid_store_hasil = new Ext.grid.EditorGridPanel({
		stripeRows: true,
		id:'grid_store_hasil_rad',
		store: gridTrease.store_reader,
		border: false,
		/*view: new Ext.grid.GroupingView({
			forceFit:true,
			groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
		}),*/
		stateful: true,
		multiSelect: true,
		stateId: 'stateGrid',
		// title: 'Array Grid',
		cls: 'grid-row-span',
		viewConfig: {
	        stripeRows: false,
	        enableTextSelection: true
	    },
        height 		: 400,
        anchor 		: '100% 100%',
        sm: new Ext.grid.CellSelectionModel({
                singleSelect: true,
                listeners:{
                    cellselect: function(sm, row, rec){
                        
                    }
                }
            }
        ),
        cm: gridTrease.column(),
	 //    view: new Ext.grid.GroupingView({
		// 	forceFit:true,
		// 	groupTextTpl: '{text}'
		// }),
    });
    return gridTrease.grid_store_hasil;
}