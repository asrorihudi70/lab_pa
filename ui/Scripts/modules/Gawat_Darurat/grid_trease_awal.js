var gridTreaseAwal={};
gridTreaseAwal.grid_store_hasil;
gridTreaseAwal.data_store_hasil;
gridTreaseAwal.data_store;
gridTreaseAwal.store_reader;
gridTreaseAwal.get_grid;
gridTreaseAwal.form_jalan_nafas;
gridTreaseAwal.form_sirkulasi;
gridTreaseAwal.form_kesadaran;
gridTreaseAwal.validasi_form;
gridTreaseAwal.form_pernafasan;
gridTreaseAwal.all_hidden;
gridTreaseAwal.all_uncheck;
gridTreaseAwal.column;
gridTreaseAwal.obj_checkbox;
gridTreaseAwal.updateRowSpan;
Ext.QuickTips.init();

gridTreaseAwal.obj_checkbox = function(checkbox){
	// console.log('checkbox');
	console.log(checkbox);
}
gridTreaseAwal.column=function(){
    return new Ext.grid.ColumnModel([
		new Ext.grid.RowNumberer(),
		{
			header 		: 'Pemeriksaan',
			dataIndex 	: 'pemeriksaan',
			width 		: 300,
			height 		: 50,
			sortable : false,
            columnLines: true,
			menuDisabled: true,
			renderer: function (value, meta, record, rowIndex, colIndex, store) {
				var first = !rowIndex || value !== store.getAt(rowIndex - 1).get('pemeriksaan'),
				last = rowIndex >= store.getCount() - 1 || value !== store.getAt(rowIndex + 1).get('pemeriksaan');
				meta.css += 'row-span' + (first ? ' row-span-first' : '') +  (last ? ' row-span-last' : '');
				if (first) {
					var i = rowIndex + 1;
					while (i < store.getCount() && value === store.getAt(i).get('pemeriksaan')) {
						i++;
					}
					var rowHeight = 20, padding = 6,
					height = (rowHeight * (i - rowIndex) - padding) + 'px';
					meta.attr = 'style="height:' + height + ';line-height:' + height + ';"';
				}
				return first ? value : '';
			},
		},{
			header 		: 'Skala Trease 1',
			dataIndex 	: 'trease_1',
			width 		: 300,
			height 		: 50,
			sortable : false,
            columnLines: true,
			menuDisabled: true,
			renderer  : function (value, meta, record, rowIndex, colIndex, store) {
				// console.log(value);
				// console.log(meta);
				// console.log(record);
				// console.log(rowIndex);
				// console.log(colIndex);
				// console.log(store);
				var first = !rowIndex || value !== store.getAt(rowIndex - 1).get('trease_1'),
				last = rowIndex >= store.getCount() - 1 || value !== store.getAt(rowIndex + 1).get('trease_1');
				meta.css += 'row-span' + (first ? ' row-span-first' : '') +  (last ? ' row-span-last' : '');
				if (first) {
					var i = rowIndex + 1;
					while (i < store.getCount() && value === store.getAt(i).get('trease_1')) {
						i++;
					}
					var rowHeight = 20, padding = 6,
					height = (rowHeight * (i - rowIndex) - padding) + 'px';
					meta.attr = 'style="height:' + height + ';line-height:' + height + ';"';
				}
				if (value != '') {
                	return '<input type="checkbox" name="checkbox" id="'+record.data.pemeriksaan+'_'+rowIndex+'" name="'+record.data.pemeriksaan+'_'+rowIndex+'" onchange = "gridTreaseAwal.obj_checkbox('+record.data.pemeriksaan+'_'+rowIndex+');">'+value;
				}else{
					return first ? value : '';
				}
			} 
		},{
			header 		: 'Skala Trease 2',
			dataIndex 	: 'trease_2',
			width 		: 300,
			height 		: 50,
			sortable : false,
            columnLines: true,
			menuDisabled: true,
			renderer  : function (value, meta, record, rowIndex, colIndex, store) {
				var first = !rowIndex || value !== store.getAt(rowIndex - 1).get('trease_2'),
				last = rowIndex >= store.getCount() - 1 || value !== store.getAt(rowIndex + 1).get('trease_2');
				meta.css += 'row-span' + (first ? ' row-span-first' : '') +  (last ? ' row-span-last' : '');
				if (first) {
					var i = rowIndex + 1;
					while (i < store.getCount() && value === store.getAt(i).get('trease_2')) {
						i++;
					}
					var rowHeight = 20, padding = 6,
					height = (rowHeight * (i - rowIndex) - padding) + 'px';
					meta.attr = 'style="height:' + height + ';line-height:' + height + ';"';
				}
				if (value != '') {
                	return '<input type="checkbox" name="checkbox" id="'+record.data.pemeriksaan+'_'+rowIndex+'" name="'+record.data.pemeriksaan+'_'+rowIndex+'">'+value;
				}else{
					return first ? value : '';
				}
			} 
		},{
			header 		: 'Skala Trease 3',
			dataIndex 	: 'trease_3',
			width 		: 300,
			height 		: 50,
			sortable : false,
            columnLines: true,
			menuDisabled: true,
			renderer  : function (value, meta, record, rowIndex, colIndex, store) {
				var first = !rowIndex || value !== store.getAt(rowIndex - 1).get('trease_3'),
				last = rowIndex >= store.getCount() - 1 || value !== store.getAt(rowIndex + 1).get('trease_3');
				meta.css += 'row-span' + (first ? ' row-span-first' : '') +  (last ? ' row-span-last' : '');
				if (first) {
					var i = rowIndex + 1;
					while (i < store.getCount() && value === store.getAt(i).get('trease_3')) {
						i++;
					}
					var rowHeight = 20, padding = 6,
					height = (rowHeight * (i - rowIndex) - padding) + 'px';
					meta.attr = 'style="height:' + height + ';line-height:' + height + ';"';
				}
				if (value != '') {
                	return '<input type="checkbox" name="checkbox" id="'+record.data.pemeriksaan+'_'+rowIndex+'" name="'+record.data.pemeriksaan+'_'+rowIndex+'">'+value;
				}else{
					return first ? value : '';
				}
			} 
		},{
			header 		: 'Skala Trease 4',
			dataIndex 	: 'trease_4',
			width 		: 300,
			height 		: 50,
			sortable : false,
            columnLines: true,
			menuDisabled: true,
			renderer  : function (value, meta, record, rowIndex, colIndex, store) {
				var first = !rowIndex || value !== store.getAt(rowIndex - 1).get('trease_4'),
				last = rowIndex >= store.getCount() - 1 || value !== store.getAt(rowIndex + 1).get('trease_4');
				meta.css += 'row-span' + (first ? ' row-span-first' : '') +  (last ? ' row-span-last' : '');
				if (first) {
					var i = rowIndex + 1;
					while (i < store.getCount() && value === store.getAt(i).get('trease_4')) {
						i++;
					}
					var rowHeight = 20, padding = 6,
					height = (rowHeight * (i - rowIndex) - padding) + 'px';
					meta.attr = 'style="height:' + height + ';line-height:' + height + ';"';
				}
				if (value != '') {
                	return '<input type="checkbox" name="checkbox" id="'+record.data.pemeriksaan+'_'+rowIndex+'" name="'+record.data.pemeriksaan+'_'+rowIndex+'">'+value;
				}else{
					return first ? value : '';
				}
			} 
		},{
			header 		: 'Skala Trease 5',
			dataIndex 	: 'trease_5',
			width 		: 300,
			height 		: 50,
			sortable : false,
            columnLines: true,
			menuDisabled: true,
			renderer  : function (value, meta, record, rowIndex, colIndex, store) {
				var first = !rowIndex || value !== store.getAt(rowIndex - 1).get('trease_5'),
				last = rowIndex >= store.getCount() - 1 || value !== store.getAt(rowIndex + 1).get('trease_5');
				meta.css += 'row-span' + (first ? ' row-span-first' : '') +  (last ? ' row-span-last' : '');
				if (first) {
					var i = rowIndex + 1;
					while (i < store.getCount() && value === store.getAt(i).get('trease_5')) {
						i++;
					}
					var rowHeight = 20, padding = 6,
					height = (rowHeight * (i - rowIndex) - padding) + 'px';
					meta.attr = 'style="height:' + height + ';line-height:' + height + ';"';
				}
				if (value != '') {
                	return '<input type="checkbox" name="checkbox" id="'+record.data.pemeriksaan+'_'+rowIndex+'" name="'+record.data.pemeriksaan+'_'+rowIndex+'">'+value;
				}else{
					return first ? value : '';
				}
			} 
		},
    ]);
}
	// shared reader
	// gridTreaseAwal.store_reader = new Ext.data.ArrayReader({}, [
	// 	{name: 'id'},
	// 	{name: 'pemeriksaan'},
	// 	{name: 'trease_1'},
	// 	{name: 'trease_2'},
	// 	{name: 'trease_3'},
	// 	{name: 'trease_4'},
	// 	{name: 'trease_5'},
	// 	{name: 'group'},
	// ]);

    // create the data store
    gridTreaseAwal.store_reader = new Ext.data.ArrayStore({
        fields: [
			{name: 'pemeriksaan'},
			{name: 'trease_1'},
			{name: 'trease_2'},
			{name: 'trease_3'},
			{name: 'trease_4'},
			{name: 'trease_5'},
        ]
    });
	gridTreaseAwal.data_store = [
		['Jalan Nafas' 	,'Sumbatan' 			,'Bebas' 					,'Bebas' 			,'Bebas'				 	,'Bebas'],
		['' 	,'' 					,'Ancaman' 					,'' 			,''				 	,''],

		['Pernafasan' 	,'Henti Nafas' 			,'Takipnoe' 				,'Normal' 			,'Frekuensi Nafas Normal' 	,'Frekuensi Nafas Normal'],
		['' 	,'Bradipnoe' 			,'Mengi' 					,'Mengi' 			,'' 						,''],
		['' 	,'Sianosis' 			,'' 						,'' 				,'' 						,''],


		['Sirkulasi' 	,'Henti Jantung' 		,'Nadi Teraba Lemah' 		,'Nadi Kuat' 		,'Nadi Kuat' 				,'Nadi Kuat'],
		['' 	,'Nadi Tidak Teraba'	,'Bradikardi' 				,'Takikardi' 		,'Frekuensi Nadi Normal'	,'Frekuensi Nadi Normal'],
		['' 	,'Akral Dingin'			,'Takikardi' 				,'TDS > 160 mmHg' 	,'TDS 140 - 180 mmHg'		,'TD Normal'],
		['' 	,''						,'Pucat' 					,'TDD > 100 mmHg' 	,'TDD 90 - 100 mmHg'		,''],
		['' 	,''						,'Akral Dingin'				,'' 				,''							,''],

		['Kesadaran' 	,'GCS <= 8' 			,'GCS 9 - 12' 				,'GCS > 12'		,'GCS 15' 				,'GCS 15'],
		['' 	,'Kejang'				,'Gelisah' 					,'Apatis' 		,''						,''],
		['' 	,'Tidak Ada Respon'		,'Hemiparese' 				,'Somnolen' 	,''						,''],
		['' 	,''						,'Nyeri Dada' 				,'' 			,''						,''],
	];

	/*gridTreaseAwal.data_store_hasil = new Ext.data.GroupingStore({
		reader 	: gridTreaseAwal.store_reader,
		data 	: gridTreaseAwal.data_store,
		sortInfo:{field: 'id', direction: "ASC"},
		groupField:'group'
	});*/

	/*gridTreaseAwal.data_store_hasil = new Ext.data.ArrayStore({
		fields: [
			{name: 'id'},
			{name: 'parameter',},
			{name: 'skor',},
		]
	});*/
/*gridTreaseAwal.data_store_hasil = new Ext.data.ArrayStore({
	id		: 0,
	fields	: ['id','parameter'],
	data	: [
		[0, 'Disetujui'],
		[1, 'Tdk Disetujui']
	]
});*/

gridTreaseAwal.get_grid=function(){
	gridTreaseAwal.store_reader.removeAll();
	gridTreaseAwal.store_reader.loadData(gridTreaseAwal.data_store);
	console.log(gridTreaseAwal.store_reader);
    gridTreaseAwal.grid_store_hasil = new Ext.grid.EditorGridPanel({
		stripeRows: true,
		id:'grid_store_hasil_rad',
		store: gridTreaseAwal.store_reader,
		border: false,
		/*view: new Ext.grid.GroupingView({
			forceFit:true,
			groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
		}),*/
		stateful: true,
		multiSelect: true,
		stateId: 'stateGrid',
		// title: 'Array Grid',
		cls: 'grid-row-span',
		viewConfig: {
	        stripeRows: false,
	        enableTextSelection: true
	    },
        height 		: 400,
        anchor 		: '100% 100%',
        sm: new Ext.grid.CellSelectionModel({
                singleSelect: true,
                listeners:{
                    cellselect: function(sm, row, rec){
                        
                    }
                }
            }
        ),
        cm: gridTreaseAwal.column(),
	 //    view: new Ext.grid.GroupingView({
		// 	forceFit:true,
		// 	groupTextTpl: '{text}'
		// }),
    });
    return gridTreaseAwal.grid_store_hasil;
}

gridTreaseAwal.form_jalan_nafas = function(){
	/*
	DATA_ASKEP_IGD_AWAL.trease.jalan_nafas_ancaman 		
DATA_ASKEP_IGD_AWAL.trease.jalan_nafas_sumbatan 		
DATA_ASKEP_IGD_AWAL.trease.jalan_nafas_bebas 		
	 */
	var form = {
		xtype 	: 'checkboxgroup',
		fieldLabel 	: 'Jalan Nafas',
		columns : 3,
		items 	: [
			{
				fieldLabel 	: '',
				xtype 		: 'checkbox',
				boxLabel  	: 'Ancaman',
				id  		: 'check_trease_jalan_nafas_ancaman',
				checked 	: DATA_ASKEP_IGD_AWAL.trease.jalan_nafas_ancaman,
			},{
				fieldLabel 	: '',
				xtype 		: 'checkbox',
				boxLabel  	: 'Sumbatan',
				id  		: 'check_trease_jalan_nafas_sumbatan',
				checked 	: DATA_ASKEP_IGD_AWAL.trease.jalan_nafas_sumbatan,
			},{
				fieldLabel 	: '',
				xtype 		: 'checkbox',
				boxLabel  	: 'Bebas',
				id  		: 'check_trease_jalan_nafas_bebas',
				checked 	: DATA_ASKEP_IGD_AWAL.trease.jalan_nafas_bebas,
			},
		],
		listeners : {
            change: function (checkbox, newValue, oldValue, eOpts) {
                tmpediting = 'true';
                Ext.getCmp('btnSimpan_viDaftarAwal').enable();

				DATA_ASKEP_IGD_AWAL.trease.jalan_nafas_ancaman 	= false;
				DATA_ASKEP_IGD_AWAL.trease.jalan_nafas_sumbatan 	= false;
				DATA_ASKEP_IGD_AWAL.trease.jalan_nafas_bebas 	= false;

                for (var i = 0; i < newValue.length; i++) {
                    if (newValue[i].id == 'check_trease_jalan_nafas_ancaman') {
                        DATA_ASKEP_IGD_AWAL.trease.jalan_nafas_ancaman = newValue[i].checked;
                    }

                    if (newValue[i].id == 'check_trease_jalan_nafas_sumbatan') {
                        DATA_ASKEP_IGD_AWAL.trease.jalan_nafas_sumbatan = newValue[i].checked;
                    }

                    if (newValue[i].id == 'check_trease_jalan_nafas_bebas') {
                        DATA_ASKEP_IGD_AWAL.trease.jalan_nafas_bebas = newValue[i].checked;
                    }
                }
            }
		}
	}
	return form;
}

gridTreaseAwal.form_pernafasan = function(){
	var form = {
		xtype 	: 'checkboxgroup',
		fieldLabel 	: 'Pernafasan',
		columns : 3,
		items 	: [
			{
				fieldLabel 	: '',
				xtype 		: 'checkbox',
				boxLabel  	: 'Henti Nafas',
				id  		: 'check_trease_pernafasan_henti_nafas',
				checked 	: DATA_ASKEP_IGD_AWAL.trease.pernafasan_henti_nafas,
			},{
				fieldLabel 	: '',
				xtype 		: 'checkbox',
				boxLabel  	: 'Bradipnoe',
				id  		: 'check_trease_pernafasan_henti_bradipnoe',
				checked 	: DATA_ASKEP_IGD_AWAL.trease.pernafasan_bradipnoe,
			},{
				fieldLabel 	: '',
				xtype 		: 'checkbox',
				boxLabel  	: 'Sianosis',
				id  		: 'check_trease_pernafasan_henti_sianosis',
				checked 	: DATA_ASKEP_IGD_AWAL.trease.pernafasan_sianosis,
			},{
				fieldLabel 	: '',
				xtype 		: 'checkbox',
				boxLabel  	: 'Takipnoe',
				id  		: 'check_trease_pernafasan_henti_takipnoe',
				checked 	: DATA_ASKEP_IGD_AWAL.trease.pernafasan_takipnoe,
			},{
				fieldLabel 	: '',
				xtype 		: 'checkbox',
				boxLabel  	: 'Mengi',
				id  		: 'check_trease_pernafasan_henti_mengi',
				checked 	: DATA_ASKEP_IGD_AWAL.trease.pernafasan_mengi,
			},{
				fieldLabel 	: '',
				xtype 		: 'checkbox',
				boxLabel  	: 'Normal',
				id  		: 'check_trease_pernafasan_henti_normal',
				checked 	: DATA_ASKEP_IGD_AWAL.trease.pernafasan_normal,
			},{
				fieldLabel 	: '',
				xtype 		: 'checkbox',
				boxLabel  	: 'Frekuensi Nafas Normal',
				id  		: 'check_trease_pernafasan_henti_frekuensi_nafas_normal',
				checked 	: DATA_ASKEP_IGD_AWAL.trease.pernafasan_frekuensi_nafas_normal,
			},
		],
		listeners 	: {
			change: function (checkbox, newValue, oldValue, eOpts) {
                tmpediting = 'true';
                Ext.getCmp('btnSimpan_viDaftarAwal').enable();
                for (var i = 0; i < newValue.length; i++) {
                    if (newValue[i].id == 'check_trease_pernafasan_henti_nafas') {
                        DATA_ASKEP_IGD_AWAL.trease.pernafasan_henti_nafas = newValue[i].checked;
                    }
                    
                    if (newValue[i].id == 'check_trease_pernafasan_henti_bradipnoe') {
                        DATA_ASKEP_IGD_AWAL.trease.pernafasan_bradipnoe = newValue[i].checked;
                    }
                    
                    if (newValue[i].id == 'check_trease_pernafasan_henti_sianosis') {
                        DATA_ASKEP_IGD_AWAL.trease.pernafasan_sianosis = newValue[i].checked;
                    }
                    
                    if (newValue[i].id == 'check_trease_pernafasan_henti_takipnoe') {
                        DATA_ASKEP_IGD_AWAL.trease.pernafasan_takipnoe = newValue[i].checked;
                    }
                    
                    if (newValue[i].id == 'check_trease_pernafasan_henti_mengi') {
                        DATA_ASKEP_IGD_AWAL.trease.pernafasan_mengi = newValue[i].checked;
                    }
                    
                    if (newValue[i].id == 'check_trease_pernafasan_henti_normal') {
                        DATA_ASKEP_IGD_AWAL.trease.pernafasan_normal = newValue[i].checked;
                    }

                    if (newValue[i].id == 'check_trease_pernafasan_henti_frekuensi_nafas_normal') {
                        DATA_ASKEP_IGD_AWAL.trease.pernafasan_frekuensi_nafas_normal = newValue[i].checked;
                    }
                }
				
			}
		}
	}
	return form;
}

gridTreaseAwal.form_sirkulasi = function(){
	var form = {
		xtype 	: 'checkboxgroup',
		fieldLabel 	: 'Sirkulasi',
		columns : 3,
		items 	: [
			{
				fieldLabel 	: '',
				xtype 		: 'checkbox',
				boxLabel  	: 'Henti Jantung',
				id  		: 'check_trease_sirkulasi_henti_jantung',
				checked 	: DATA_ASKEP_IGD_AWAL.trease.sirkulasi_henti_jantung,
			},{
				fieldLabel 	: '',
				xtype 		: 'checkbox',
				boxLabel  	: 'Nadi tidak teraba',
				id  		: 'check_trease_sirkulasi_nadi_tidak_teraba',
				checked 	: DATA_ASKEP_IGD_AWAL.trease.sirkulasi_nadi_tidak_teraba,
			},{
				fieldLabel 	: '',
				xtype 		: 'checkbox',
				boxLabel  	: 'Nadi teraba lemah',
				id  		: 'check_trease_sirkulasi_nadi_teraba_lemah',
				checked 	: DATA_ASKEP_IGD_AWAL.trease.sirkulasi_nadi_teraba_lemah,
			},{
				fieldLabel 	: '',
				xtype 		: 'checkbox',
				boxLabel  	: 'Nadi kuat',
				id  		: 'check_trease_sirkulasi_nadi_kuat',
				checked 	: DATA_ASKEP_IGD_AWAL.trease.sirkulasi_nadi_kuat,
			},{
				fieldLabel 	: '',
				xtype 		: 'checkbox',
				boxLabel  	: 'Akral Dingin',
				id  		: 'check_trease_sirkulasi_akral_dingin',
				checked 	: DATA_ASKEP_IGD_AWAL.trease.sirkulasi_akral_dingin,
			},{
				fieldLabel 	: '',
				xtype 		: 'checkbox',
				boxLabel  	: 'Bradikardi',
				id  		: 'check_trease_sirkulasi_bradikardi',
				checked 	: DATA_ASKEP_IGD_AWAL.trease.sirkulasi_brakikardi,
			},{
				fieldLabel 	: '',
				xtype 		: 'checkbox',
				boxLabel  	: 'Takikardi',
				id  		: 'check_trease_sirkulasi_takikardi',
				checked 	: DATA_ASKEP_IGD_AWAL.trease.sirkulasi_takikardi,
			},{
				fieldLabel 	: '',
				xtype 		: 'checkbox',
				boxLabel  	: 'Pucat',
				id  		: 'check_trease_sirkulasi_pucat',
				checked 	: DATA_ASKEP_IGD_AWAL.trease.sirkulasi_pucat,
			},{
				fieldLabel 	: '',
				xtype 		: 'checkbox',
				boxLabel  	: 'TDS > 160 mmHg',
				id  		: 'check_trease_sirkulasi_tds_lebih_160_mmhg',
				checked 	: DATA_ASKEP_IGD_AWAL.trease.sirkulasi_tds_lebih_160,
			},{
				fieldLabel 	: '',
				xtype 		: 'checkbox',
				boxLabel  	: 'TDD > 100 mmHg',
				id  		: 'check_trease_sirkulasi_tdd_lebih_100_mmhg',
				checked 	: DATA_ASKEP_IGD_AWAL.trease.sirkulasi_tds_lebih_100,
			},{
				fieldLabel 	: '',
				xtype 		: 'checkbox',
				boxLabel  	: 'Frekuensi nadi normal',
				id  		: 'check_trease_sirkulasi_frekuensi_nadi_normal',
				checked 	: DATA_ASKEP_IGD_AWAL.trease.sirkulasi_frekuensi_nadi_normal,
			},{
				fieldLabel 	: '',
				xtype 		: 'checkbox',
				boxLabel  	: 'TDS 140 - 180 mmHg',
				id  		: 'check_trease_sirkulasi_tds_140_sd_180_mmhg',
				checked 	: DATA_ASKEP_IGD_AWAL.trease.sirkulasi_tdd_140_to_180,
			},{
				fieldLabel 	: '',
				xtype 		: 'checkbox',
				boxLabel  	: 'TDD 90 - 100 mmHg',
				id  		: 'check_trease_sirkulasi_tdd_90_sd_100_mmhg',
				checked 	: DATA_ASKEP_IGD_AWAL.trease.sirkulasi_tdd_90_to_100,
			},{
				fieldLabel 	: '',
				xtype 		: 'checkbox',
				boxLabel  	: 'TD Normal',
				id  		: 'check_trease_sirkulasi_td_normal',
				checked 	: DATA_ASKEP_IGD_AWAL.trease.sirkulasi_td_normal,
			},
		],
		listeners 	: {
			change: function (checkbox, newValue, oldValue, eOpts) {
				tmpediting = 'true';
				Ext.getCmp('btnSimpan_viDaftarAwal').enable();
				DATA_ASKEP_IGD_AWAL.trease.sirkulasi_henti_jantung           = false;
				DATA_ASKEP_IGD_AWAL.trease.sirkulasi_nadi_tidak_teraba       = false;
				DATA_ASKEP_IGD_AWAL.trease.sirkulasi_nadi_teraba_lemah       = false;
				DATA_ASKEP_IGD_AWAL.trease.sirkulasi_nadi_kuat               = false;
				DATA_ASKEP_IGD_AWAL.trease.sirkulasi_akral_dingin            = false;
				DATA_ASKEP_IGD_AWAL.trease.sirkulasi_brakikardi              = false;
				DATA_ASKEP_IGD_AWAL.trease.sirkulasi_takikardi               = false;
				DATA_ASKEP_IGD_AWAL.trease.sirkulasi_pucat                   = false;
				DATA_ASKEP_IGD_AWAL.trease.sirkulasi_tds_lebih_160           = false;
				DATA_ASKEP_IGD_AWAL.trease.sirkulasi_tds_lebih_100           = false;
				DATA_ASKEP_IGD_AWAL.trease.sirkulasi_frekuensi_nadi_normal   = false;
				DATA_ASKEP_IGD_AWAL.trease.sirkulasi_tdd_140_to_180          = false;
				DATA_ASKEP_IGD_AWAL.trease.sirkulasi_tdd_90_to_100           = false;
				DATA_ASKEP_IGD_AWAL.trease.sirkulasi_td_normal               = false;
				for (var i = 0; i < newValue.length; i++) {
					console.log(newValue[i].id);
					if(newValue[i].id == 'check_trease_sirkulasi_henti_jantung'){ DATA_ASKEP_IGD_AWAL.trease.sirkulasi_henti_jantung = newValue[i].checked; }
					if(newValue[i].id == 'check_trease_sirkulasi_nadi_tidak_teraba'){ DATA_ASKEP_IGD_AWAL.trease.sirkulasi_nadi_tidak_teraba 	= newValue[i].checked; }
					if(newValue[i].id == 'check_trease_sirkulasi_nadi_teraba_lemah'){ DATA_ASKEP_IGD_AWAL.trease.sirkulasi_nadi_teraba_lemah 	= newValue[i].checked; }
					if(newValue[i].id == 'check_trease_sirkulasi_nadi_kuat'){ DATA_ASKEP_IGD_AWAL.trease.sirkulasi_nadi_kuat 	= newValue[i].checked; }
					if(newValue[i].id == 'check_trease_sirkulasi_akral_dingin'){ DATA_ASKEP_IGD_AWAL.trease.sirkulasi_akral_dingin	= newValue[i].checked; }
					if(newValue[i].id == 'check_trease_sirkulasi_bradikardi'){ DATA_ASKEP_IGD_AWAL.trease.sirkulasi_brakikardi	= newValue[i].checked; }
					if(newValue[i].id == 'check_trease_sirkulasi_takikardi'){ DATA_ASKEP_IGD_AWAL.trease.sirkulasi_takikardi	= newValue[i].checked; }
					if(newValue[i].id == 'check_trease_sirkulasi_pucat'){ DATA_ASKEP_IGD_AWAL.trease.sirkulasi_pucat	= newValue[i].checked; }
					if(newValue[i].id == 'check_trease_sirkulasi_tds_lebih_160_mmhg'){ DATA_ASKEP_IGD_AWAL.trease.sirkulasi_tds_lebih_160	= newValue[i].checked; }
					if(newValue[i].id == 'check_trease_sirkulasi_tdd_lebih_100_mmhg'){ DATA_ASKEP_IGD_AWAL.trease.sirkulasi_tds_lebih_100	= newValue[i].checked; }
					if(newValue[i].id == 'check_trease_sirkulasi_frekuensi_nadi_normal'){ DATA_ASKEP_IGD_AWAL.trease.sirkulasi_frekuensi_nadi_normal	= newValue[i].checked; }
					if(newValue[i].id == 'check_trease_sirkulasi_tds_140_sd_180_mmhg'){ DATA_ASKEP_IGD_AWAL.trease.sirkulasi_tdd_140_to_180	= newValue[i].checked; }
					if(newValue[i].id == 'check_trease_sirkulasi_tdd_90_sd_100_mmhg'){ DATA_ASKEP_IGD_AWAL.trease.sirkulasi_tdd_90_to_100	= newValue[i].checked; }
					if(newValue[i].id == 'check_trease_sirkulasi_td_normal'){ DATA_ASKEP_IGD_AWAL.trease.sirkulasi_td_normal	= newValue[i].checked; }

				}
			}
		}
	}
	return form;
}

gridTreaseAwal.form_kesadaran = function(){
	var form = {
		xtype 	: 'checkboxgroup',
		fieldLabel 	: 'Kesadaran',
		columns : 3,
		items 	: [
			{
				fieldLabel 	: '',
				xtype 		: 'checkbox',
				boxLabel  	: 'GCS <= 8',
				id  		: 'check_trease_kesadaran_gcs_kurang_sama_8',
				checked 	: DATA_ASKEP_IGD_AWAL.trease.kesadaran_gcs_kurang_sama_8,
			},{
				fieldLabel 	: '',
				xtype 		: 'checkbox',
				boxLabel  	: 'GCS 9 - 12',
				id  		: 'check_trease_kesadaran_gcs_9_sampai_12',
				checked 	: DATA_ASKEP_IGD_AWAL.trease.kesadaran_gcs_9_sampai_12,
			},{
				fieldLabel 	: '',
				xtype 		: 'checkbox',
				boxLabel  	: 'GCS > 12',
				id  		: 'check_trease_kesadaran_gcs_lebih_12',
				checked 	: DATA_ASKEP_IGD_AWAL.trease.kesadaran_gcs_lebih_12,
			},{
				fieldLabel 	: '',
				xtype 		: 'checkbox',
				boxLabel  	: 'GCS 15',
				id  		: 'check_trease_kesadaran_gcs_15',
				checked 	: DATA_ASKEP_IGD_AWAL.trease.kesadaran_gcs_15,
			},{
				fieldLabel 	: '',
				xtype 		: 'checkbox',
				boxLabel  	: 'Kejang',
				id  		: 'check_trease_kesadaran_kejang',
				checked 	: DATA_ASKEP_IGD_AWAL.trease.kesadaran_kejang,
			},{
				fieldLabel 	: '',
				xtype 		: 'checkbox',
				boxLabel  	: 'Tidak ada respon',
				id  		: 'check_trease_kesadaran_tidak_ada_respon',
				checked 	: DATA_ASKEP_IGD_AWAL.trease.kesadaran_tidak_ada_respon,
			},{
				fieldLabel 	: '',
				xtype 		: 'checkbox',
				boxLabel  	: 'Gelisah',
				id  		: 'check_trease_kesadaran_gelisah',
				checked 	: DATA_ASKEP_IGD_AWAL.trease.kesadaran_gelisah,
			},{
				fieldLabel 	: '',
				xtype 		: 'checkbox',
				boxLabel  	: 'Hemiparese',
				id  		: 'check_trease_kesadaran_hemiparese',
				checked 	: DATA_ASKEP_IGD_AWAL.trease.kesadaran_hemiparese,
			},{
				fieldLabel 	: '',
				xtype 		: 'checkbox',
				boxLabel  	: 'Nyeri Dada',
				id  		: 'check_trease_kesadaran_nyeri_dada',
				checked 	: DATA_ASKEP_IGD_AWAL.trease.kesadaran_nyeri_dada,
			},{
				fieldLabel 	: '',
				xtype 		: 'checkbox',
				boxLabel  	: 'Apatis',
				id  		: 'check_trease_kesadaran_apatis',
				checked 	: DATA_ASKEP_IGD_AWAL.trease.kesadaran_apatis,
			},{
				fieldLabel 	: '',
				xtype 		: 'checkbox',
				boxLabel  	: 'Somnolen',
				id  		: 'check_trease_kesadaran_somnolen',
				checked 	: DATA_ASKEP_IGD_AWAL.trease.kesadaran_somnolen,
			},
		],
		listeners 	: {
			change: function (checkbox, newValue, oldValue, eOpts) {
				tmpediting = 'true';
				Ext.getCmp('btnSimpan_viDaftarAwal').enable();
				DATA_ASKEP_IGD_AWAL.trease.kesadaran_gcs_kurang_sama_8       = false;
				DATA_ASKEP_IGD_AWAL.trease.kesadaran_gcs_9_sampai_12         = false;
				DATA_ASKEP_IGD_AWAL.trease.kesadaran_gcs_lebih_12            = false;
				DATA_ASKEP_IGD_AWAL.trease.kesadaran_gcs_15                  = false;
				DATA_ASKEP_IGD_AWAL.trease.kesadaran_kejang                  = false;
				DATA_ASKEP_IGD_AWAL.trease.kesadaran_tidak_ada_respon        = false;
				DATA_ASKEP_IGD_AWAL.trease.kesadaran_gelisah               	= false;
				DATA_ASKEP_IGD_AWAL.trease.kesadaran_hemiparese              = false;
				DATA_ASKEP_IGD_AWAL.trease.kesadaran_nyeri_dada              = false;
				DATA_ASKEP_IGD_AWAL.trease.kesadaran_apatis                  = false;
				DATA_ASKEP_IGD_AWAL.trease.kesadaran_somnolen                = false;
				for (var i = 0; i < newValue.length; i++) {
					if(newValue[i].id == 'check_trease_kesadaran_gcs_kurang_sama_8'){ DATA_ASKEP_IGD_AWAL.trease.kesadaran_gcs_kurang_sama_8 = newValue[i].checked; }
					if(newValue[i].id == 'check_trease_kesadaran_gcs_9_sampai_12'){ DATA_ASKEP_IGD_AWAL.trease.kesadaran_gcs_9_sampai_12 	= newValue[i].checked; }
					if(newValue[i].id == 'check_trease_kesadaran_gcs_lebih_12'){ DATA_ASKEP_IGD_AWAL.trease.kesadaran_gcs_lebih_12 	= newValue[i].checked; }
					if(newValue[i].id == 'check_trease_kesadaran_gcs_15'){ DATA_ASKEP_IGD_AWAL.trease.kesadaran_gcs_15 	= newValue[i].checked; }
					if(newValue[i].id == 'check_trease_kesadaran_kejang'){ DATA_ASKEP_IGD_AWAL.trease.kesadaran_kejang	= newValue[i].checked; }
					if(newValue[i].id == 'check_trease_kesadaran_tidak_ada_respon'){ DATA_ASKEP_IGD_AWAL.trease.kesadaran_tidak_ada_respon	= newValue[i].checked; }
					if(newValue[i].id == 'check_trease_kesadaran_gelisah'){ DATA_ASKEP_IGD_AWAL.trease.kesadaran_gelisah	= newValue[i].checked; }
					if(newValue[i].id == 'check_trease_kesadaran_hemiparese'){ DATA_ASKEP_IGD_AWAL.trease.kesadaran_hemiparese	= newValue[i].checked; }
					if(newValue[i].id == 'check_trease_kesadaran_nyeri_dada'){ DATA_ASKEP_IGD_AWAL.trease.kesadaran_nyeri_dada	= newValue[i].checked; }
					if(newValue[i].id == 'check_trease_kesadaran_apatis'){ DATA_ASKEP_IGD_AWAL.trease.kesadaran_apatis	= newValue[i].checked; }
					if(newValue[i].id == 'check_trease_kesadaran_somnolen'){ DATA_ASKEP_IGD_AWAL.trease.kesadaran_somnolen	= newValue[i].checked; }

				}
			}
		}
	}
	return form;
}

gridTreaseAwal.validasi_form = function(trease){
	if (trease == 1 || trease == '1') {
		Ext.getCmp('check_trease_jalan_nafas_sumbatan').show();

		Ext.getCmp('check_trease_pernafasan_henti_nafas').show();
		Ext.getCmp('check_trease_pernafasan_henti_bradipnoe').show();
		Ext.getCmp('check_trease_pernafasan_henti_sianosis').show();

		Ext.getCmp('check_trease_sirkulasi_henti_jantung').show();
		Ext.getCmp('check_trease_sirkulasi_nadi_tidak_teraba').show();
		Ext.getCmp('check_trease_sirkulasi_akral_dingin').show();

		Ext.getCmp('check_trease_kesadaran_gcs_kurang_sama_8').show();
		Ext.getCmp('check_trease_kesadaran_kejang').show();
		Ext.getCmp('check_trease_kesadaran_tidak_ada_respon').show();
	}else if(trease == 2 || trease == '2'){
		Ext.getCmp('check_trease_jalan_nafas_ancaman').show();
		Ext.getCmp('check_trease_jalan_nafas_bebas').show();

		Ext.getCmp('check_trease_pernafasan_henti_takipnoe').show();
		Ext.getCmp('check_trease_pernafasan_henti_mengi').show();

		Ext.getCmp('check_trease_sirkulasi_nadi_teraba_lemah').show();
		Ext.getCmp('check_trease_sirkulasi_bradikardi').show();
		Ext.getCmp('check_trease_sirkulasi_takikardi').show();
		Ext.getCmp('check_trease_sirkulasi_pucat').show();
		Ext.getCmp('check_trease_sirkulasi_akral_dingin').show();

		Ext.getCmp('check_trease_kesadaran_gcs_9_sampai_12').show();
		Ext.getCmp('check_trease_kesadaran_gelisah').show();
		Ext.getCmp('check_trease_kesadaran_hemiparese').show();
		Ext.getCmp('check_trease_kesadaran_nyeri_dada').show();
	}else if(trease == 3 || trease == '3'){
		Ext.getCmp('check_trease_jalan_nafas_bebas').show();

		Ext.getCmp('check_trease_pernafasan_henti_normal').show();
		Ext.getCmp('check_trease_pernafasan_henti_mengi').show();

		Ext.getCmp('check_trease_sirkulasi_nadi_kuat').show();
		Ext.getCmp('check_trease_sirkulasi_takikardi').show();
		Ext.getCmp('check_trease_sirkulasi_tds_lebih_160_mmhg').show();
		Ext.getCmp('check_trease_sirkulasi_tdd_lebih_100_mmhg').show();

		Ext.getCmp('check_trease_kesadaran_gcs_lebih_12').show();
		Ext.getCmp('check_trease_kesadaran_apatis').show();
		Ext.getCmp('check_trease_kesadaran_somnolen').show();
	}else if(trease == 4 || trease == '4'){
		Ext.getCmp('check_trease_jalan_nafas_bebas').show();

		Ext.getCmp('check_trease_pernafasan_henti_frekuensi_nafas_normal').show();

		Ext.getCmp('check_trease_sirkulasi_nadi_kuat').show();
		Ext.getCmp('check_trease_sirkulasi_frekuensi_nadi_normal').show();
		Ext.getCmp('check_trease_sirkulasi_tds_140_sd_180_mmhg').show();
		Ext.getCmp('check_trease_sirkulasi_tdd_90_sd_100_mmhg').show();

		Ext.getCmp('check_trease_kesadaran_gcs_15').show();
	}else if(trease == 5 || trease == '5'){
		Ext.getCmp('check_trease_jalan_nafas_bebas').show();

		Ext.getCmp('check_trease_pernafasan_henti_frekuensi_nafas_normal').show();
		
		Ext.getCmp('check_trease_sirkulasi_nadi_kuat').show();
		Ext.getCmp('check_trease_sirkulasi_frekuensi_nadi_normal').show();
		Ext.getCmp('check_trease_sirkulasi_td_normal').show();

		Ext.getCmp('check_trease_kesadaran_gcs_15').show();
	}
}

gridTreaseAwal.all_hidden = function(){
	Ext.getCmp('check_trease_jalan_nafas_ancaman').hide();
	Ext.getCmp('check_trease_jalan_nafas_sumbatan').hide();
	Ext.getCmp('check_trease_jalan_nafas_bebas').hide();

	Ext.getCmp('check_trease_pernafasan_henti_nafas').hide();
	Ext.getCmp('check_trease_pernafasan_henti_bradipnoe').hide();
	Ext.getCmp('check_trease_pernafasan_henti_sianosis').hide();
	Ext.getCmp('check_trease_pernafasan_henti_takipnoe').hide();
	Ext.getCmp('check_trease_pernafasan_henti_mengi').hide();
	Ext.getCmp('check_trease_pernafasan_henti_normal').hide();
	Ext.getCmp('check_trease_pernafasan_henti_frekuensi_nafas_normal').hide();

	Ext.getCmp('check_trease_sirkulasi_henti_jantung').hide();
	Ext.getCmp('check_trease_sirkulasi_nadi_tidak_teraba').hide();
	Ext.getCmp('check_trease_sirkulasi_nadi_teraba_lemah').hide();
	Ext.getCmp('check_trease_sirkulasi_nadi_kuat').hide();
	Ext.getCmp('check_trease_sirkulasi_akral_dingin').hide();
	Ext.getCmp('check_trease_sirkulasi_bradikardi').hide();
	Ext.getCmp('check_trease_sirkulasi_takikardi').hide();
	Ext.getCmp('check_trease_sirkulasi_pucat').hide();
	Ext.getCmp('check_trease_sirkulasi_tds_lebih_160_mmhg').hide();
	Ext.getCmp('check_trease_sirkulasi_tdd_lebih_100_mmhg').hide();
	Ext.getCmp('check_trease_sirkulasi_frekuensi_nadi_normal').hide();
	Ext.getCmp('check_trease_sirkulasi_tds_140_sd_180_mmhg').hide();
	Ext.getCmp('check_trease_sirkulasi_tdd_90_sd_100_mmhg').hide();
	Ext.getCmp('check_trease_sirkulasi_td_normal').hide();

	Ext.getCmp('check_trease_kesadaran_gcs_kurang_sama_8').hide();
	Ext.getCmp('check_trease_kesadaran_gcs_9_sampai_12').hide();
	Ext.getCmp('check_trease_kesadaran_gcs_lebih_12').hide();
	Ext.getCmp('check_trease_kesadaran_gcs_15').hide();
	Ext.getCmp('check_trease_kesadaran_kejang').hide();
	Ext.getCmp('check_trease_kesadaran_tidak_ada_respon').hide();
	Ext.getCmp('check_trease_kesadaran_gelisah').hide();
	Ext.getCmp('check_trease_kesadaran_hemiparese').hide();
	Ext.getCmp('check_trease_kesadaran_nyeri_dada').hide();
	Ext.getCmp('check_trease_kesadaran_apatis').hide();
	Ext.getCmp('check_trease_kesadaran_somnolen').hide();
}

gridTreaseAwal.all_uncheck = function(){
	Ext.getCmp('check_trease_jalan_nafas_ancaman').setValue(false);
	Ext.getCmp('check_trease_jalan_nafas_sumbatan').setValue(false);
	Ext.getCmp('check_trease_jalan_nafas_bebas').setValue(false);

	Ext.getCmp('check_trease_pernafasan_henti_nafas').setValue(false);
	Ext.getCmp('check_trease_pernafasan_henti_bradipnoe').setValue(false);
	Ext.getCmp('check_trease_pernafasan_henti_sianosis').setValue(false);
	Ext.getCmp('check_trease_pernafasan_henti_takipnoe').setValue(false);
	Ext.getCmp('check_trease_pernafasan_henti_mengi').setValue(false);
	Ext.getCmp('check_trease_pernafasan_henti_normal').setValue(false);
	Ext.getCmp('check_trease_pernafasan_henti_frekuensi_nafas_normal').setValue(false);

	Ext.getCmp('check_trease_sirkulasi_henti_jantung').setValue(false);
	Ext.getCmp('check_trease_sirkulasi_nadi_tidak_teraba').setValue(false);
	Ext.getCmp('check_trease_sirkulasi_nadi_teraba_lemah').setValue(false);
	Ext.getCmp('check_trease_sirkulasi_nadi_kuat').setValue(false);
	Ext.getCmp('check_trease_sirkulasi_akral_dingin').setValue(false);
	Ext.getCmp('check_trease_sirkulasi_bradikardi').setValue(false);
	Ext.getCmp('check_trease_sirkulasi_takikardi').setValue(false);
	Ext.getCmp('check_trease_sirkulasi_pucat').setValue(false);
	Ext.getCmp('check_trease_sirkulasi_tds_lebih_160_mmhg').setValue(false);
	Ext.getCmp('check_trease_sirkulasi_tdd_lebih_100_mmhg').setValue(false);
	Ext.getCmp('check_trease_sirkulasi_frekuensi_nadi_normal').setValue(false);
	Ext.getCmp('check_trease_sirkulasi_tds_140_sd_180_mmhg').setValue(false);
	Ext.getCmp('check_trease_sirkulasi_tdd_90_sd_100_mmhg').setValue(false);
	Ext.getCmp('check_trease_sirkulasi_td_normal').setValue(false);

	Ext.getCmp('check_trease_kesadaran_gcs_kurang_sama_8').setValue(false);
	Ext.getCmp('check_trease_kesadaran_gcs_9_sampai_12').setValue(false);
	Ext.getCmp('check_trease_kesadaran_gcs_lebih_12').setValue(false);
	Ext.getCmp('check_trease_kesadaran_gcs_15').setValue(false);
	Ext.getCmp('check_trease_kesadaran_kejang').setValue(false);
	Ext.getCmp('check_trease_kesadaran_tidak_ada_respon').setValue(false);
	Ext.getCmp('check_trease_kesadaran_gelisah').setValue(false);
	Ext.getCmp('check_trease_kesadaran_hemiparese').setValue(false);
	Ext.getCmp('check_trease_kesadaran_nyeri_dada').setValue(false);
	Ext.getCmp('check_trease_kesadaran_apatis').setValue(false);
	Ext.getCmp('check_trease_kesadaran_somnolen').setValue(false);


	DATA_ASKEP_IGD_AWAL.trease.jalan_nafas_ancaman 				= false;
	DATA_ASKEP_IGD_AWAL.trease.jalan_nafas_sumbatan 				= false;
	DATA_ASKEP_IGD_AWAL.trease.jalan_nafas_bebas 				= false;

	DATA_ASKEP_IGD_AWAL.trease.pernafasan_henti_nafas 			= false;
	DATA_ASKEP_IGD_AWAL.trease.pernafasan_bradipnoe              = false;
	DATA_ASKEP_IGD_AWAL.trease.pernafasan_sianosis               = false;
	DATA_ASKEP_IGD_AWAL.trease.pernafasan_takipnoe               = false;
	DATA_ASKEP_IGD_AWAL.trease.pernafasan_mengi                  = false;
	DATA_ASKEP_IGD_AWAL.trease.pernafasan_normal                 = false;
	DATA_ASKEP_IGD_AWAL.trease.pernafasan_frekuensi_nafas_normal = false;

	DATA_ASKEP_IGD_AWAL.trease.sirkulasi_henti_jantung           = false;
	DATA_ASKEP_IGD_AWAL.trease.sirkulasi_nadi_tidak_teraba       = false;
	DATA_ASKEP_IGD_AWAL.trease.sirkulasi_nadi_teraba_lemah       = false;
	DATA_ASKEP_IGD_AWAL.trease.sirkulasi_nadi_kuat               = false;
	DATA_ASKEP_IGD_AWAL.trease.sirkulasi_akral_dingin            = false;
	DATA_ASKEP_IGD_AWAL.trease.sirkulasi_brakikardi              = false;
	DATA_ASKEP_IGD_AWAL.trease.sirkulasi_takikardi               = false;
	DATA_ASKEP_IGD_AWAL.trease.sirkulasi_pucat                   = false;
	DATA_ASKEP_IGD_AWAL.trease.sirkulasi_tds_lebih_160           = false;
	DATA_ASKEP_IGD_AWAL.trease.sirkulasi_tds_lebih_100           = false;
	DATA_ASKEP_IGD_AWAL.trease.sirkulasi_frekuensi_nadi_normal   = false;
	DATA_ASKEP_IGD_AWAL.trease.sirkulasi_tdd_140_to_180          = false;
	DATA_ASKEP_IGD_AWAL.trease.sirkulasi_tdd_90_to_100           = false;
	DATA_ASKEP_IGD_AWAL.trease.sirkulasi_td_normal               = false;

	DATA_ASKEP_IGD_AWAL.trease.kesadaran_gcs_kurang_sama_8       = false;
	DATA_ASKEP_IGD_AWAL.trease.kesadaran_gcs_9_sampai_12         = false;
	DATA_ASKEP_IGD_AWAL.trease.kesadaran_gcs_lebih_12            = false;
	DATA_ASKEP_IGD_AWAL.trease.kesadaran_gcs_15                  = false;
	DATA_ASKEP_IGD_AWAL.trease.kesadaran_kejang                  = false;
	DATA_ASKEP_IGD_AWAL.trease.kesadaran_tidak_ada_respon        = false;
	DATA_ASKEP_IGD_AWAL.trease.kesadaran_gelisah               	= false;
	DATA_ASKEP_IGD_AWAL.trease.kesadaran_hemiparese              = false;
	DATA_ASKEP_IGD_AWAL.trease.kesadaran_nyeri_dada              = false;
	DATA_ASKEP_IGD_AWAL.trease.kesadaran_apatis                  = false;
	DATA_ASKEP_IGD_AWAL.trease.kesadaran_somnolen                = false;
}