var panelTrease={};
panelTrease.grid_store_hasil;
panelTrease.data_store_hasil;
panelTrease.data_store;
panelTrease.store_reader;
panelTrease.get_grid;
panelTrease.column;
panelTrease.panel;

panelTrease.panel = function(){
	var subpanel_accordion = new Ext.Panel({
		layout: {
			type: 'accordion',
			titleCollapse: true,
			multi: true,
			fill: false,
			animate: false, 
			activeOnTop: false
			
		},
		flex: 1,
		autoScroll:true,
		id: 'accordian_panel_trease',
		layoutConfig: {
			titleCollapse: false,
			animate: true,
			activeOnTop: false
		},
		items: [
			{
				title 		: 'Jalan Nafas',
				collapsed 	: true,
				bodyStyle 	: 'padding : 5px 5px 5px 5px;',
				height 		: 260,
				items 		: [{
						layout 		: 'column',
						border 		: false,
						items 		: [
							{
								border: true,
								title: 'Skala Trease Tahap 1 (0 Menit)',
								layout: 'form',
								defaultType: 'checkbox',
								bodyStyle 	: 'margin-right : 10px;',
								height 		: 300,
								items : [
									{
										boxLabel 	: 'Sumbatan',
										id 			: 'check_trease_tahap_1_sumbatan',
									},
								]
							},{
								border: true,
								title: 'Skala Trease Tahap 2 (10 Menit)',
								layout: 'form',
								defaultType: 'checkbox',
								bodyStyle 	: 'margin-right : 10px;',
								height 		: 300,
								items : [
									{
										boxLabel 	: 'Ancaman',
										id 			: 'check_trease_tahap_2_ancaman',
									},
								]
							},
						]
					}
					
				],
			    listeners: {
					expand: function() {
					}
			    }
			},{
				title 		: 'Pernafasan',
				collapsed 	: true,
				height 		: 260,
				bodyStyle 	: 'padding : 5px 5px 5px 5px;',
				items:[
				],
				listeners 	: {
					expand 	: function(){
					}
				}
			},{
				title 		: 'Sirkulasi',
				collapsed 	: true,
				height 		: 260,
				bodyStyle 	: 'padding : 5px 5px 5px 5px;',
				items:[
				],
				listeners 	: {
					expand 	: function(){
					}
				}
			},{
				title 		: 'Kesadaran',
				collapsed 	: true,
				height 		: 260,
				bodyStyle 	: 'padding : 5px 5px 5px 5px;',
				items:[
				],
				listeners 	: {
					expand 	: function(){
					}
				}
			},
		]
	});
    return subpanel_accordion;
}