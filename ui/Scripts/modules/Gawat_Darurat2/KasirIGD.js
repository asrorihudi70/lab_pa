var kdUnit = '3';

var CurrentKasirIGD =
{
    data: Object,
    details: Array,
    row: 0
};
var CurrentHistory =
{
    data: Object,
    details: Array,
    row: 0
};
var dsprinter_kasirigd;
var varkd_tarif_igd;
var vKdPasien;
var vKdDokter;
var nilai_kd_tarif;
var syssetting='igd_default_klas_produk';
var tampungtypedata;
var Trkdunit2;
var FormLookUpsdetailTRTransfer_IGD;
var tanggaltransaksitampung;
var variablebatalhistori_igd;
var ProdukDataIGD;
var CurrentRowPemeriksaanIGD;
var winIGDPasswordDulu;
var	tmp_kodeunitkamarigd;
var	tmp_kdspesialigd;
var	tmp_nokamarigd;
var mRecordKasirigd = Ext.data.Record.create
(
    [
       {name: 'DESKRIPSI2', mapping:'DESKRIPSI2'},
       {name: 'KD_PRODUK', mapping:'KD_PRODUK'},
       {name: 'DESKRIPSI', mapping:'DESKRIPSI'},
       {name: 'KD_TARIF', mapping:'KD_TARIF'},
       {name: 'HARGA', mapping:'HARGA'},
       {name: 'QTY', mapping:'QTY'},
       {name: 'TGL_TRANSAKSI', mapping:'TGL_TRANSAKSI'},
       {name: 'DESC_REQ', mapping:'DESC_REQ'},
       {name: 'URUT', mapping:'URUT'}
    ]
);
var mRecordIGD = Ext.data.Record.create
(
    [
       {name: 'DESKRIPSI2', mapping:'DESKRIPSI2'},
       {name: 'KD_PRODUK', mapping:'KD_PRODUK'},
       {name: 'DESKRIPSI', mapping:'DESKRIPSI'},
       {name: 'KD_TARIF', mapping:'KD_TARIF'},
       {name: 'HARGA', mapping:'HARGA'},
       {name: 'QTY', mapping:'QTY'},
       {name: 'TGL_TRANSAKSI', mapping:'TGL_TRANSAKSI'},
       {name: 'DESC_REQ', mapping:'DESC_REQ'},
       {name: 'URUT', mapping:'URUT'}
    ]
);
var Kdtransaksi;
var tapungkd_pay;
var dsTRDetailHistoryListIGD;
var AddNewHistory = true;
var selectCountHistory = 50;
var now = new Date();
var rowSelectedHistory;
var cellSelecteddeskripsi;
var FormLookUpsdetailTRHistory;
var now = new Date();
var cellSelectedtutup_kasirigd;
var vkd_unit;
var kdokter_btl;
var nowTglTransaksiIGD = new Date();
var kdcustomeraa;
var labelisi;
var jenispay;
var variablehistori;
var selectCountStatusByr_viKasirigdKasir='Belum Lunas';
var dsTRKasirigdKasirList;
var dsTRDetailKasirigdKasirList;
var AddNewKasirigdKasir = true;
var selectCountKasirigdKasir = 50;
var now = new Date();
var rowSelectedKasirigdKasir;

var FormLookUpsdetailTRKasirigd;
var valueStatusCMKasirigdView='All';
var nowTglTransaksiIGD2 = nowTglTransaksiIGD.format('d-M-Y');
var dsComboBayar;
var vkode_customer;
var vflag;
 var gridDTLTRKasirigd;
 
var kodekasir;
var notransaksi ;
var tgltrans;
var kodepasien;
var namapasien;
var kodeunit;
var namaunit;
var kodepay;
var uraianpay;
CurrentPage.page = getPanelKasirigd(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelKasirigd(mod_id) 
{

    var Field = ['KD_DOKTER','NO_TRANSAKSI','KD_UNIT','KD_PASIEN','NAMA','NAMA_UNIT','ALAMAT',
	'TANGGAL_TRANSAKSI','NAMA_DOKTER','KD_CUSTOMER','CUSTOMER','URUT_MASUK','FLAG','LUNAS','KET_PAYMENT','CARA_BAYAR','JENIS_PAY','KD_PAY','POSTING','TYPE_DATA','CO_STATUS'];
    dsTRKasirigdKasirList = new WebApp.DataStore({ fields: Field });
	
	ProdukDataIGD = new WebApp.DataStore({ fields: ['KD_PRODUK','KD_KLAS','DESKRIPSI','TARIF','TGL_BERLAKU','KD_TARIF'] });
    refeshKasirIgdKasir();
    var grListTRKasirigd = new Ext.grid.EditorGridPanel
    (
        {
            stripeRows: true,
            store: dsTRKasirigdKasirList,
            columnLines: false,
            autoScroll:true,
			anchor: '100% 42%',
            border: false,
			sort :false,
            sm: new Ext.grid.RowSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        rowselect: function(sm, row, rec)
                        {
							rowSelectedKasirigdKasir = dsTRKasirigdKasirList.getAt(row);
							CurrentKDUnitIGD = rowSelectedKasirigdKasir.data.KD_UNIT;
							CurrentKDCustomerIGD = rowSelectedKasirigdKasir.data.KD_CUSTOMER;
							CurrentKDDokerIGD = rowSelectedKasirigdKasir.data.KD_DOKTER;
							CurrentURUT_MASUKIGD = rowSelectedKasirigdKasir.data.URUT_MASUK;
							CurrentKD_PASIENIGD = rowSelectedKasirigdKasir.data.KD_PASIEN;
							vKdPasien=rowSelectedKasirrwiKasir.data.KD_PASIEN;
							vKdDokter=rowSelectedKasirrwiKasir.data.KD_DOKTER;
						//	getProdukIGD(ProdukDataIGD);
							
						}
                    }
                }
            ),
            listeners:
            {
                rowdblclick: function (sm, ridx, cidx)
                {
			           
                    rowSelectedKasirigdKasir = dsTRKasirigdKasirList.getAt(ridx);
				
                    if (rowSelectedKasirigdKasir != undefined)
                    { 	if ( rowSelectedKasirigdKasir.data.LUNAS=='f' )
							   {
									KasirLookUpigd(rowSelectedKasirigdKasir.data);
									
							   }else
							   {
							   ShowPesanWarningKasirigd('Pembayaran Tidak Bisa Di Lakukan karena Sudah Lunas','Pembayaran');
							   }
                    }else{
					ShowPesanWarningKasirigd('Silahkan Pilih data   ','Pembayaran');
					      }
                   
                },
				rowclick: function (sm, ridx, cidx)
                {
						cellSelectedtutup_kasirigd=rowSelectedKasirigdKasir.data.NO_TRANSAKSI;
						//alert(rowSelectedKasirigdKasir.data.LUNAS);
						if ( rowSelectedKasirigdKasir.data.LUNAS=='t' && rowSelectedKasirigdKasir.data.CO_STATUS =='t')
						   {
						   
						     Ext.getCmp('btnEditKasirigd').disable();
						     Ext.getCmp('btnTutupTransaksiKasirigd').disable();
						   	 Ext.getCmp('btnHpsBrsKasirigd').disable();
							 Ext.getCmp('btnLookupIGD').disable();
							 Ext.getCmp('btnSimpanIGD').disable();
							 Ext.getCmp('btnHpsBrsIGD').disable();
							// Ext.getCmp('btnTransferKasirigd').disable();
							 Ext.getCmp('btnTambahBrsKasirIGD').disable();
							 
						   }
						   else if ( rowSelectedKasirigdKasir.data.LUNAS=='f' && rowSelectedKasirigdKasir.data.CO_STATUS =='f')
						   {
						        Ext.getCmp('btnTutupTransaksiKasirigd').disable();
						   	    Ext.getCmp('btnHpsBrsKasirigd').enable();
								Ext.getCmp('btnEditKasirigd').enable();
							   Ext.getCmp('btnLookupIGD').enable();
							   Ext.getCmp('btnSimpanIGD').enable();
							   Ext.getCmp('btnHpsBrsIGD').enable();
							  // Ext.getCmp('btnTransferKasirigd').enable();
							   	 Ext.getCmp('btnTambahBrsKasirIGD').enable();
							   
							}
							   else if ( rowSelectedKasirigdKasir.data.LUNAS=='t' && rowSelectedKasirigdKasir.data.CO_STATUS =='f')
						   {
						       Ext.getCmp('btnEditKasirigd').disable();
						       Ext.getCmp('btnTutupTransaksiKasirigd').enable();
						   	   Ext.getCmp('btnHpsBrsKasirigd').enable();
							    Ext.getCmp('btnLookupIGD').disable();
								Ext.getCmp('btnSimpanIGD').disable();
								Ext.getCmp('btnHpsBrsIGD').disable();
								// Ext.getCmp('btnTransferKasirigd').disable();
								 	 Ext.getCmp('btnTambahBrsKasirIGD').disable();
						       
							}
				            kodekasir=rowSelectedKasirigdKasir.data.KD_KASIR;
							notransaksi= rowSelectedKasirigdKasir.data.NO_TRANSAKSI ;
						     tgltrans =rowSelectedKasirigdKasir.data.TANGGAL_TRANSAKSI;
							kodepasien=rowSelectedKasirigdKasir.data.KD_PASIEN;
							 namapasien=rowSelectedKasirigdKasir.data.NAMA;
							kodeunit =rowSelectedKasirigdKasir.data.KD_UNIT;
							namaunit =rowSelectedKasirigdKasir.data.NAMA_UNIT;
							kodepay=rowSelectedKasirigdKasir.data.KD_PAY;
							uraianpay=rowSelectedKasirigdKasir.data.CARA_BAYAR;
							kdcustomeraa=rowSelectedKasirigdKasir.data.KD_CUSTOMER;
							kdokter_btl=rowSelectedKasirigdKasir.data.KD_DOKTER;
							RefreshDatahistoribayar(rowSelectedKasirigdKasir.data.NO_TRANSAKSI);
							RefreshDataKasirIGDDetail(rowSelectedKasirigdKasir.data.NO_TRANSAKSI);
								Ext.Ajax.request(
								{
									url:  baseURL + "index.php/main/functionLABPoliklinik/gettarif",
									 params: {
										kd_customer: rowSelectedKasirigdKasir.data.KD_CUSTOMER,
									},
									failure: function(o)
									{},	    
									success: function(o) {
										 var cst = Ext.decode(o.responseText);
											varkd_tarif_igd=cst.kd_tarif;
										
										getProdukIGD(rowSelectedKasirigdKasir.data.KD_UNIT);
										
										 }
								});
							Ext.Ajax.request(
										{
											//url: "./home.mvc/getModule",
											//url: baseURL + "index.php/main/getTrustee",
											url: baseURL + "index.php/main/getcurrentshift",
											 params: {
												//UserID: 'Admin',
												command: '0',
												// parameter untuk url yang dituju (fungsi didalam controller)
											},
											failure: function(o)
											{
												 var cst = Ext.decode(o.responseText);
												
											},	    
											success: function(o) {
												
											
												//var cst = Ext.decode(o.responseText);
												tampungshiftsekarang=o.responseText
												//Ext.get('txtNilaiShift').dom.value =tampungshiftsekarang ;



											}
										
										});
									Ext.Ajax.request(
									{
										//url: "./home.mvc/getModule",
										//url: baseURL + "index.php/main/getTrustee",
										url: baseURL + "index.php/main/Getkdtarif",
										 params: {
											//UserID: 'Admin',
											customer: rowSelectedKasirigdKasir.data.KD_CUSTOMER,
											// parameter untuk url yang dituju (fungsi didalam controller)
										},
										failure: function(o)
										{
											 var cst = Ext.decode(o.responseText);
											
										},	    
										success: function(o) {
											
										
											//var cst = Ext.decode(o.responseText);
											nilai_kd_tarif=o.responseText;
											//Ext.get('txtNilaiShift').dom.value =tampungshiftsekarang ;



										}
									
									});
         
                } 
                   
				
            },
        cm: new Ext.grid.ColumnModel
            (
                [
                    {
                        id: 'colLUNAScoba',
                        header: 'Status Lunas',
                        dataIndex: 'LUNAS',
                        sortable: true,
                        width: 90,
                        align:'center',
                        renderer: function(value, metaData, record, rowIndex, colIndex, store)
                        {
                             switch (value)
                             {
                                 case 't':
                                         metaData.css = 'StatusHijau'; // 
                                         break;
                                 case 'f':
                                        metaData.css = 'StatusMerah'; // rejected

                                         break;
                             }
                             return '';
                        }
                    },
					 {
                        id: 'coltutuptr',
                        header: 'Tutup transaksi',
                        dataIndex: 'CO_STATUS',
                        sortable: true,
                        width: 90,
                        align:'center',
                       renderer: function(value, metaData, record, rowIndex, colIndex, store)
                        {
                             switch (value)
                             {
                                 case 't':
                                         metaData.css = 'StatusHijau'; // 
                                         break;
                                 case 'f':
                                        metaData.css = 'StatusMerah'; // rejected

                                         break;
                             }
                             return '';
                        }
                    },
                    {
                        id: 'colReqIdViewKasirIGD',
                        header: 'No. Transaksi',
                        dataIndex: 'NO_TRANSAKSI',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 80
                    },
                    {
                        id: 'colTglKasirigdViewKasirIGD',
                        header: 'Tgl Transaksi',
                        dataIndex: 'TANGGAL_TRANSAKSI',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 75,
                            renderer: function(v, params, record)
                            {
                                    return ShowDate(record.data.TANGGAL_TRANSAKSI);

                        }
                    },
					{
                        header: 'No. Medrec',
                        width: 65,
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        dataIndex: 'KD_PASIEN',
                        id: 'colKasirigderViewKasirIGD'
                    },
					{
                        header: 'Pasien',
                        width: 190,
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        dataIndex: 'NAMA',
                        id: 'colKasirigderViewKasirIGD'
                    },
                    {
                        id: 'colLocationViewKasirIGD',
                        header: 'Alamat',
                        dataIndex: 'ALAMAT',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 170
                    },
                    
                    {
                        id: 'colDeptViewKasirIGD',
                        header: 'Dokter',
                        dataIndex: 'NAMA_DOKTER',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 150
                    },
                    {
                        id: 'colunitViewKasirIGD',
                        header: 'Unit',
                        dataIndex: 'NAMA_UNIT',
						sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 90
                    },
					     {
                        id: 'colcustomerViewKasirIGD',
                        header: 'Kel. Pasien',
                        dataIndex: 'CUSTOMER',
						sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 90
                    },
					//CUSTOMER
					
                   
                ]
            ),  
			viewConfig:{forceFit: true},
            tbar:
                [
                    {
                        id: 'btnEditKasirigd',
                        text: 'Pembayaran',
                        tooltip: nmEditData,
                        iconCls: 'Edit_Tr',
                        handler: function(sm, row, rec)
                        {
                            if (rowSelectedKasirigdKasir != undefined)
                            {
                                    KasirLookUpigd(rowSelectedKasirigdKasir.data);
                            }
                            else
                            {
							ShowPesanWarningKasirigd('Pilih data tabel  ','Pembayaran');
                                    //alert('');
                            }
                        }, disabled :true
                    }
					
					, ' ', ' ','' + ' ','' + ' ','' + ' ','' + ' ', ' ','' + '-',
					{
								text: 'Tutup Transaksi',
								id: 'btnTutupTransaksiKasirigd',
								tooltip: nmHapus,
								iconCls: 'remove',
								handler: function()
									{
									if(cellSelectedtutup_kasirigd=='' || cellSelectedtutup_kasirigd=='undefined')
									{
									ShowPesanWarningKasirigd ('Pilih data tabel  ','Pembayaran');
									}
									 else
										{	
									
									   UpdateTutuptransaksi(false);	
								        Ext.getCmp('btnEditKasirigd').disable();
										Ext.getCmp('btnTutupTransaksiKasirigd').disable();
						   	            Ext.getCmp('btnHpsBrsKasirigd').disable();
										}										
								    },
									disabled:true
					},{
								text: 'Batal Transaksi',
								id: 'btnBatalTransaksiKasirrwi',
								tooltip: nmHapus,
								iconCls: 'remove',
								handler: function()
									{
									if(cellSelectedtutup_kasirigd=='' || cellSelectedtutup_kasirigd=='undefined')
									{
									ShowPesanWarningKasirigd ('Pilih data tabel  ','Pembayaran');
									}
									else if(rowSelectedKasirigdKasir.data.CO_STATUS == 'f')
									{
									ShowPesanWarningKasirigd ('Transaksi Belum ditutup tidak bisa dibatalkan','Pembayaran');	
									}
									else if(rowSelectedKasirigdKasir.data.BATAL == 't')
									{
									ShowPesanWarningKasirigd ('Transaksi Sudah dibatalkan tidak bisa dibatalkan kembali','Pembayaran');	
									}
									 else
										{
										fnDlgIGDPasswordDulu();
									  

								    
										}
								    },
									disabled:false
					}
                ]
            }
	);
	
	var LegendViewCMRequest = new Ext.Panel
	(
            {
            id: 'LegendViewCMRequest',
            region: 'center',
            border:false,
            bodyStyle: 'padding:0px 7px 0px 7px',
            layout: 'column',
            frame:true,
            //height:32,
            anchor: '100% 8.0001%',
            autoScroll:false,
            items:
            [
                {
                    columnWidth: .033,
                    layout: 'form',
                    style:{'margin-top':'-1px'},
                    //height:32,
                    anchor: '100% 8.0001%',
                    border: false,
                    html: '<img src="'+baseURL+'ui/images/icons/16x16/hijau.png" class="text-desc-legend"/>'
                },
                {
                    columnWidth: .08,
                    layout: 'form',
                    //height:32,
                    anchor: '100% 8.0001%',
                    style:{'margin-top':'1px'},
                    border: false,
                    html: " Lunas"
                },
                {
                    columnWidth: .033,
                    layout: 'form',
                    style:{'margin-top':'-1px'},
                    border: false,
                    //height:35,
                    anchor: '100% 8.0001%',
                    //html: '<img src="./images/icons/16x16/merah.png" class="text-desc-legend"/>'
                    html: '<img src="'+baseURL+'ui/images/icons/16x16/merah.png" class="text-desc-legend"/>'
                },
                {
                    columnWidth: .1,
                    layout: 'form',
                    //height:32,
                    anchor: '100% 8.0001%',
                    style:{'margin-top':'1px'},
                    border: false,
                    html: " Belum Lunas"
                }
            ]

        }
    )
 var GDtabDetailIGD = new Ext.TabPanel   
    (
        {
        id:'GDtabDetailIGD',
        region: 'center',
        activeTab: 0,
	   anchor: '100% 30%',
        border:false,
        plain: true,
        defaults:
                {
                autoScroll: true
				},
                items: [
					GetDTLTRHistoryGridIGD(),GetDTLTRIGDGrid()//GetDTLTRIGDGrid() 
		                //-------------- ## --------------
					],
				listeners:
					{
				  
					}
		}
		
    );
    var FormDepanKasirigd = new Ext.Panel
    (
        {
            id: mod_id,
            closable: true,
            region: 'center',
            layout: 'form',
            title: 'Kasir Gawat Darurat',
            border: false,
            shadhow: true,
           // autoScroll:false,
            iconCls: 'Request',
            margins: '0 5 5 0',
            items: [{
						   xtype:'panel',
							   plain:true,
							   activeTab: 0,
								height:135,
							   //deferredRender: false,
							   defaults:
							   {
								bodyStyle:'padding:10px',
								autoScroll: true
							   },
							   items:[
					 {
				    layout: 'form',
					 margins: '0 5 5 0',
					border: true ,
					items:
					[
				     {
                    xtype: 'textfield',
                    fieldLabel: ' No. Medrec' + ' ',
                    id: 'txtFilterNomedrec',
                    anchor : '100%',
                    onInit: function() { },
					listeners:
                        {
                            'specialkey' : function()
                            {
                                var tmpNoMedrec = Ext.get('txtFilterNomedrec').getValue()
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10 )
                                {
                                    if(tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10 )
                                        {
										   
                                             var tmpgetNoMedrec = formatnomedrec(Ext.get('txtFilterNomedrec').getValue())
                                             Ext.getCmp('txtFilterNomedrec').setValue(tmpgetNoMedrec);
                                            var tmpkriteria = getCriteriaFilter_viDaftar();
											 RefreshDataFilterKasirIgdKasir();
                                      
                                        }
                                        else
                                            {
                                                if (tmpNoMedrec.length === 10)
                                                    {
                                                       // tmpkriteria = getCriteriaFilter_viDaftar();
                                                     RefreshDataFilterKasirIgdKasir();
                                                    }
                                                    else
                                                    Ext.getCmp('txtFilterNomedrec').setValue('')
                                            }
                                }
                            }

                        }
                },
				
				{	 
				xtype: 'tbspacer',
				height: 3
				},	
					{
							xtype: 'textfield',
							fieldLabel: ' Pasien' + ' ',
							id: 'TxtFilterGridDataView_NAMA_viKasirigdKasir',
							anchor :'100%',
							 enableKeyEvents: true,
							listeners:
							{ 
						
								'keyup' : function()
								{

			                      RefreshDataFilterKasirIgdKasir();
			
								}
							}
						},
						{	 
						xtype: 'tbspacer',
						height: 3
						},	
						getItemPaneltgl_filter(),
						getItemPanelcombofilter()
						
						]}
						]},grListTRKasirigd,GDtabDetailIGD,LegendViewCMRequest]
           
        }
    );
	
  

   return FormDepanKasirigd

};
function fnDlgIGDPasswordDulu()
{
    winIGDPasswordDulu = new Ext.Window
    (
        {
            id: 'winIGDPasswordDulu',
            title: 'Password',
            closeAction: 'destroy',
            width:320,
            height: 120,
            border: false,
            resizable:false,
            plain: true,
            layout: 'fit',
            iconCls: 'icon_lapor',
            modal: true,
            items: [ItemDlgIGDPasswordDulu()]

        }
    );

    winIGDPasswordDulu.show();
};
function ItemDlgIGDPasswordDulu()
{
    var PnlLapIGDSPasswordDulu = new Ext.Panel
    (
        {
            id: 'PnlLapIGDSPasswordDulu',
            fileUpload: true,
            layout: 'form',
            width:170,
            height: 120,
            bodyStyle: 'padding:15px',
            border: true,
            items:
            [
                getItemLapIGDPasswordDulu_Tanggal(),
                {
                    layout: 'hBox',
                    border: false,
                    defaults: { margins: '0 5 0 0' },
                   // style:{'margin-left':'30px','margin-top':'5px'},
                    anchor: '100%',
                    layoutConfig:
                    {
                        padding: '3',
                        pack: 'end',
                        align: 'middle'
                    },
                    items:
                    [
                         {
                            xtype: 'button',
                            text: nmBtnOK,
                            width: 70,
                            hideLabel: true,
							style:{'margin-left':'5px','margin-top':'0px'},
                            id: 'btnOkLapIGDPasswordDulu',
                            handler: function()
                            {
                               Ext.Ajax.request
								 (
									{
										//url: "./Datapool.mvc/CreateDataObj",
										url: baseURL + "index.php/main/functionIGD/cekpassword_igd",
										params: {passDulu:Ext.getCmp('TxPasswordDuluIGD').getValue()},
										failure: function(o)
										{
											ShowPesanWarningKasirrwj('Proses Gagal segera Hubungi Admin', 'Gagal');
										//RefreshDataKasirRwiDetail(notransaksi);
										},
										success: function(o)
										{
											//RefreshDataKasirRwiDetail(notransaksi);
											var cst = Ext.decode(o.responseText);
											if (cst.success === true)
											{
												winIGDPasswordDulu.close();
													var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan Pembatalan Transaksi:', function(btn, combo){
													if (btn == 'ok')
																{
																variablebatalhistori_igd=combo;
																				if (variablehistori!='')
																				{
																				 BatalTransaksi(false);
																				}
																				else
																				{
																					ShowPesanWarningKasirigd('Silahkan isi alasan terlebih dahaulu','Keterangan');

																				}
																}

														}); 
											}
											else
											{
													ShowPesanWarningKasirigd('Password salah segera Hubungi Admin', 'Gagal');
											};
										}
									}
								)
							   /* */

								    
                            }
                        },
                        {
                            xtype: 'button',
                            text: nmBtnCancel ,
                            width: 70,
                            hideLabel: true,
							style:{'margin-left':'5px','margin-top':'0px'},
                            id: 'btnCancelLapIGDPasswordDulu',
                            handler: function()
                            {
                                    winIGDPasswordDulu.close();
                            }
                        } 
                    ]
                }
            ]
        }
    );

    return PnlLapIGDSPasswordDulu;
};
function getItemLapIGDPasswordDulu_Tanggal()
{
   var items =
    {
        layout: 'column',
        border: false,
        items:
        [
		
		
		{
			    columnWidth: .90,
			    layout: 'form',
				 labelWidth:120,
				  border: false,
				 
			    items:
				[
					{
						xtype: 'textfield',
						inputType: 'password',
						fieldLabel: 'Masukan Password ',
						id: 'TxPasswordDuluIGD',
					    anchor: '99%'
					},
				
					
				]
			},
     
        ]
    }
    return items;
};
function TransferLookUp(rowdata) 
{
    var lebar = 440;
    FormLookUpsdetailTRTransfer_IGD = new Ext.Window
    (
        {
            id: 'gridTransfer',
            title: 'Transfer Rawat Inap',
            closeAction: 'destroy',
            width: lebar,
            height: 410,
            border: false,
            resizable: false,
            plain: false,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryTRTransfer(lebar),
            listeners:
            {
                
            }
        }
    );

    FormLookUpsdetailTRTransfer_IGD.show();
  //  Transferbaru();

};
function getFormEntryTRTransfer(lebar) 
{
    var pnlTRTransfer = new Ext.FormPanel
    (
        {
            id: 'PanelTRTransfer',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:410,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [getItemPanelInputTransfer(lebar),getItemPanelButtonTransfer(lebar)],
           tbar:
            [
               
               
            ]
        }
    );
 
    var FormDepanTransfer = new Ext.Panel
	(
		{
		    id: 'FormDepanTransfer',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [pnlTRTransfer	
				
			]

		}
	);

    return FormDepanTransfer
};

function getItemPanelInputTransfer(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 0px',
		border:false,
		height:320,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
				
			    layout: 'form',
				height:320,
			    border: false,
			    items:
				[
					getTransfertujuan(lebar),
					    {	 
															xtype: 'tbspacer',
															height: 5
						},	
						getItemPanelNoTransksiTransfer(lebar),
						{	 
															xtype: 'tbspacer',									
															height:3
						},
						mComboalasan_transfer()
						
						
					
				]
			},
		]
	};
    return items;
};
function getTransfertujuan(lebar) 
{
    var items =
	{
		Width:lebar-2,
		height:150,
	    layout: 'form',
	    border: true,
		labelWidth:130,
	//	title: 'Kelompok Pasien tujuan',
		
	    items:
		[
			/*{
			    columnWidth: .990,
			    layout: 'form',
				Width:lebar-10,
				labelWidth:130,
			    border: true,
				//title: 'Kelompok Pasien tujuan',
			    items:
				[*/
					
					
						{	 
															xtype: 'tbspacer',
															height: 2
						},
									{
                                        xtype: 'textfield',
                                        fieldLabel: 'No Transaksi',
                                        //maxLength: 200,
                                        name: 'txtTranfernoTransaksi',
                                        id: 'txtTranfernoTransaksi',
										labelWidth:130,
                                        width: 100,
                                        anchor: '95%'
                                     },	
					{
					    xtype: 'datefield',
					    fieldLabel: 'Tanggal ',
					    id: 'dtpTanggalTransfertransaksi',
					    name: 'dtpTanggalTransfertransaksi',
					    format: 'd/M/Y',
						readOnly : true,
					  //  value: now,
					    labelWidth:130,
                        width: 100,
                        anchor: '95%'
					},
					{
                                        xtype: 'textfield',
                                        fieldLabel: 'No Medrec',
                                        //maxLength: 200,
                                        name: 'txtTranfernomedrec',
                                        id: 'txtTranfernomedrec',
										labelWidth:130,
                                        width: 100,
                                        anchor: '95%'
                                     },
									 {
                                        xtype: 'textfield',
                                        fieldLabel: 'Nama Pasien',
                                        //maxLength: 200,
                                        name: 'txtTranfernamapasien',
                                        id: 'txtTranfernamapasien',
										labelWidth:130,
                                        width: 100,
                                        anchor: '95%'
                                     },{
                                        xtype: 'textfield',
                                        fieldLabel: 'Unit Perawatan',
                                        //maxLength: 200,
                                        name: 'txtTranferunit',
                                        id: 'txtTranferunit',
										labelWidth:130,
                                        width: 100,
										hidden : true,
                                        anchor: '95%'
                                     },{
                                        xtype: 'textfield',
                                        fieldLabel: 'Unit Perawatan',
                                        //maxLength: 200,
                                        name: 'txtTranferkelaskamarigd',
                                        id: 'txtTranferkelaskamarigd',
										readOnly : true,
										labelWidth:130,
                                        width: 100,
                                        anchor: '95%'
                                     }
		
					
					
				//]
			//}
			
		]
	}
    return items;
};

function getItemPanelNoTransksiTransfer(lebar) 
{
    var items =
	{
		Width:lebar,
		height:110,
	    layout: 'column',
	    border: true,
		
		
	    items:
		[
			{

			    columnWidth: .990,
			    layout: 'form',
				Width:lebar-10,
				labelWidth:130,
			    border: false,
			    items:
				[
					
					
														{	 
															xtype: 'tbspacer',
															
															height:3
														}, 
								
									{
                                        xtype: 'textfield',
                                        fieldLabel: 'Jumlah Biaya',
                                        maxLength: 200,
											style:{'text-align':'right'},
                                        name: 'txtjumlahbiayasal',
                                        id: 'txtjumlahbiayasal',
                                        width: 100,
										readOnly :true,
                                        anchor: '95%'
                                     },
									{
                                        xtype: 'textfield',
                                        fieldLabel: 'Paid',
                                        maxLength: 200,
											style:{'text-align':'right'},
                                        name: 'txtpaid',
                                        id: 'txtpaid',
                                        width: 100,
										value:0,
                                        anchor: '95%'
                                     }
									 ,
									  {
                                        xtype: 'textfield',
                                        fieldLabel: 'Jumlah dipindahkan',
                                        maxLength: 200,
											style:{'text-align':'right'},
                                        name: 'txtjumlahtranfer',
                                        id: 'txtjumlahtranfer',
										readOnly:true,
                                        width: 100,
                                        anchor: '95%'
                                     },
									   {
                                        xtype: 'textfield',
                                        fieldLabel: 'Saldo tagihan',
                                        maxLength: 200,
											style:{'text-align':'right'},
                                        name: 'txtsaldotagihan',
                                        id: 'txtsaldotagihan',
											value:0,
                                        width: 100,
                                        anchor: '95%'
                                     }
									
									//mComboTransfer
					
					
				]
			}
			
		]
	}
    return items;
};

function getItemPanelButtonTransfer(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
		height:30,
		anchor:'100%',
		style:{'margin-top':'-1px'},
	    items:
		[
			{
				layout: 'hBox',
				width:400,
				border: false,
				bodyStyle: 'padding:5px 0px 5px 5px',
				defaults: { margins: '3 3 3 3' },
				anchor: '90%',
				layoutConfig: 
				{
					align: 'middle',
					pack:'end'
				},
				items:
				[
					{
						xtype:'button',
						text:'Simpan',
						width:70,
						style:{'margin-left':'0px','margin-top':'0px'},
						hideLabel:true,
						id: 'btnOkTransfer',
						handler:function()
						{
					
							TransferData_IGD(false);
							
						}
					},
					{
							xtype:'button',
							text:'Tutup',
							width:70,
							hideLabel:true,
							id: 'btnCancelTransfer',
							handler:function() 
							{
								FormLookUpsdetailTRTransfer_IGD.close();
							}
					}
				]
			}
		]
	}
    return items;
};
function RefreshDataKasirIGDDetail(no_transaksi) 
{
    var strKriteriaIGD='';
    strKriteriaIGD = "no_transaksi= ~" + no_transaksi + "~ and kd_kasir=~06~";
   dsTRDetailKasirIGDList.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'tgl_transaksi',
			    Sortdir: 'ASC',
			    target: 'ViewDetailTRRWJ',
			    param: strKriteriaIGD
			}
		}
	);
    return dsTRDetailKasirIGDList;
};

function getParamDataDeleteKasirIGDDetail()
{

    var params =
    {
		Table: 'ViewTrKasirIGD',
                TrKodeTranskasi: notransaksi,
		TrTglTransaksi:  CurrentKasirIGD.data.data.TGL_TRANSAKSI,
		TrKdPasien :	 CurrentKasirIGD.data.data.KD_PASIEN,
		kodePasien: CurrentKD_PASIENIGD,
		TrKdNamaPasien : namapasien,	
		TrKdUnit :		kodeunit,
		TrNamaUnit :	 namaunit,
		Uraian :		 CurrentKasirIGD.data.data.DESKRIPSI2,
		AlasanHapus : variablehistori,
		TrHarga :		 CurrentKasirIGD.data.data.HARGA,
		TrKdProduk :	 CurrentKasirIGD.data.data.KD_PRODUK,
        RowReq: CurrentKasirIGD.data.data.URUT,
        Hapus:2
    };
	
    return params
};

function GetDTLTRIGDGrid() 
{
    var fldDetail = ['KD_PRODUK','DESKRIPSI','DESKRIPSI2','KD_TARIF','HARGA','QTY','DESC_REQ','TGL_BERLAKU','NO_TRANSAKSI','URUT','DESC_STATUS','TGL_TRANSAKSI'];
	
    dsTRDetailKasirIGDList = new WebApp.DataStore({ fields: fldDetail })
  // RefreshDataKasirIGDDetail() ;
    var gridDTLTRIGD = new Ext.grid.EditorGridPanel
    (
        {
            title: 'Input Produk',
            stripeRows: true,
            store: dsTRDetailKasirIGDList,
            border: false,
            columnLines: true,
            frame: false,
            anchor: '100%',
             autoScroll:true,
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                            cellSelecteddeskripsi = dsTRDetailKasirIGDList.getAt(row);
                            CurrentKasirIGD.row = row;
                            CurrentKasirIGD.data = cellSelecteddeskripsi;
							CurrentRowPemeriksaanIGD = row;
                           // FocusCtrlCMIGD='txtAset';
						  // getProdukIGD()
                        }
                    }
                }
            ),
            cm: TRGawatDaruratColumModel(),
			viewConfig: {forceFit: true},
			        tbar:
                [
                       
                       {
                                id:'btnTambahBrsKasirIGD',
                                text: 'Tambah baris',
                                tooltip: nmTambahBaris,
                                iconCls: 'AddRow',
								disabled :true,
                                handler: function()
                                {
                                        TambahBarisIGD();
                                }
                        },
                      
						{
							text: 'Tambah Produk',
							id: 'btnLookupIGD',
							tooltip: nmLookup,
							iconCls: 'find',
							hidden : true,
							disabled :true,
							handler: function()
							{
								var p = RecordBaruRWJ();
									var str='';
									//if (Ext.get('txtKdUnitIGD').dom.value  != undefined && Ext.get('txtKdUnitIGD').dom.value  != '')
									//{
											//str = ' where kd_dokter =~' + Ext.get('txtKdDokter').dom.value  + '~';
											//str = "\"kd_dokter\" = ~" + Ext.get('txtKdDokter').dom.value  + "~";
											str =  kodeunit;
									//};
									FormLookupKasirRWJ(str, dsTRDetailKasirIGDList, p, true, '', true);
							}
						},
                       
							 {
								text: 'Simpan',
								id: 'btnSimpanIGD',
								tooltip: nmSimpan,
								iconCls: 'save',
								disabled :true,
								handler: function()
							{	
							 Datasave_KasirIGD(false);
							}
							},  
							{
                                id:'btnHpsBrsIGD',
                                text: 'Hapus Baris',
                                tooltip: 'Hapus Baris',
								disabled :true,
                                iconCls: 'RemoveRow',
                                handler: function()
                                {
                                        if (dsTRDetailKasirIGDList.getCount() > 0 )
                                        {
                                                if (cellSelecteddeskripsi != undefined)
                                                {
                                                        if(CurrentKasirIGD != undefined)
                                                        {
                                                                HapusBarisIGD();
                                                        }
                                                }
                                                else
                                                {
                                                        ShowPesanWarningIGD('Pilih record ','Hapus data');
                                                }
                                        }
                                }
                        }
						
                ]
                //, viewConfig:{forceFit: true}
        }
		
		
    );
	
	

    return gridDTLTRIGD;
};

function TambahBarisIGD()
{
    var x=true;

    if (x === true)
    {
        var p = RecordBaruRWJ();
        dsTRDetailKasirIGDList.insert(dsTRDetailKasirIGDList.getCount(), p);
    };
};
function Datasave_KasirIGD(mBol) 
{	
	//if (ValidasiEntryCMIGD(nmHeaderSimpanData,false) == 1 )
	//{//ShowPesanWarningKasirIGD,ShowPesanInfoKasirIGD,notransaksi
			Ext.Ajax.request
			 (
				{
					//url: "./Datapool.mvc/CreateDataObj",
					//url: baseURL + "index.php/main/functionIGD/savedetailpenyakit",savedetailproduk
					url: baseURL + "index.php/main/functionIGD/savedetailproduk",
					params: getParamDetailTransaksiIGD(),
					failure: function(o)
					{
					ShowPesanWarningKasirigd('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
					RefreshDataKasirIGDDetail(notransaksi);
					},	
					success: function(o) 
					{
						RefreshDataKasirIGDDetail(notransaksi);
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoKasirigd(nmPesanSimpanSukses,nmHeaderSimpanData);
							RefreshDataFilterKasirIgdKasir();
							if(mBol === false)
							{
								RefreshDataKasirIGDDetail(notransaksi);
							};
						}
						else 
						{
								ShowPesanWarningKasirigd('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
						};
					}
				}
			)
		
	//}
	//else
	//{
	//	if(mBol === true)
	//	{
		//	return false;
		//};
	//};//ShowPesanInfoKasirigd,ShowPesanWarningKasirigd
	
};



function TransferData_IGD(mBol) 
{	

			Ext.Ajax.request
			 (
				{
					url: baseURL + "index.php/main/functionIGD/saveTransfer",
					params: getParamTransferRwi(),
					failure: function(o)
					{
					ShowPesanWarningKasirigd('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
					RefreshDataKasirIGDDetail(notransaksi);
					},	
					success: function(o) 
					{
						RefreshDataKasirIGDDetail(notransaksi);
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoKasirigd(nmPesanSimpanSukses,nmHeaderSimpanData);
							Ext.getCmp('btnTransferKasirigd').disable();
							Ext.getCmp('btnSimpanKasirigd').disable();
							RefreshDataKasirigdKasirDetail(Ext.get('txtNoTransaksiKasirigdKasir').dom.value);
							RefreshDataFilterKasirIgdKasir();
							FormLookUpsdetailTRTransfer_IGD.close();
							if(mBol === false)
							{
								RefreshDataKasirIGDDetail(notransaksi);
							};
						}
						else 
						{
								ShowPesanWarningKasirigd('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
						};
					}
				}
			)
		

	
};



function RecordBaruRWJ()
{

	var p = new mRecordIGD
	(
		{
			'DESKRIPSI2':'',
			'KD_PRODUK':'',
		    'DESKRIPSI':'', 
		    'KD_TARIF':'', 
		    'HARGA':'',
		    'QTY':'',
		    'TGL_TRANSAKSI':tanggaltransaksitampung, 
		    'DESC_REQ':'',
		    'KD_TARIF':'',
		    'URUT':''
		}
	);
	
	return p;
};

function getParamDetailTransaksiIGD() 
{
    var params =
	{
		Table:'ViewTrKasirIGD',

		TrKodeTranskasi: notransaksi,
		KdUnit: kodeunit,
		//DeptId:Ext.get('txtKdDokter').dom.value ,tampungshiftsekarang
		Tgl: tgltrans,
		kdDokter:CurrentKDDokerIGD,
		urut_masuk:CurrentURUT_MASUKIGD,
		kd_pasien:CurrentKD_PASIENIGD,
		Shift: tampungshiftsekarang,
		List:getArr2DetailTrIGD(),
		JmlField: mRecordIGD.prototype.fields.length-4,
		JmlList:GetListCoun2tDetailTransaksi(),
		Hapus:1,
		Ubah:0
	};
    return params
};

function getParamTransferRwi() 
{
    var params =
	{
	//,,,,,,,,,shift,status_bayar) 
		//Table:'ViewTrKasirRwi',
        KDkasirIGD:kodekasir,
		TrKodeTranskasi: notransaksi,
		KdUnit: kodeunit,
		Kdpay: 'T1',
		Jumlahtotal: Ext.get(txtjumlahbiayasal).dom.value,
		Tglasal:  ShowDate(tgltrans),
		Shift: tampungshiftsekarang,
		TRKdTransTujuan:Ext.get(txtTranfernoTransaksi).dom.value,
		KdpasienIGDtujuan: Ext.get(txtTranfernomedrec).dom.value,
		TglTranasksitujuan : Ext.get(dtpTanggalTransfertransaksi).dom.value,
		KDunittujuan : Trkdunit2,
		KDalasan :Ext.get(cboalasan_transfer).dom.value,
		KasirRWI:'05',
		Kdcustomer:kdcustomeraa,
		kodeunitkamar:tmp_kodeunitkamarigd,
		kdspesial:tmp_kdspesialigd,
		nokamar:tmp_nokamarigd,
		
	
	};
    return params
};
function getArr2DetailTrIGD()
{
	var x='';
	for(var i = 0 ; i < dsTRDetailKasirIGDList.getCount();i++)
	{
		if (dsTRDetailKasirIGDList.data.items[i].data.KD_PRODUK != '' && dsTRDetailKasirIGDList.data.items[i].data.DESKRIPSI != '')
		{
			var y='';
			var z='@@##$$@@';
			
			y = 'URUT=' + dsTRDetailKasirIGDList.data.items[i].data.URUT
			y += z + dsTRDetailKasirIGDList.data.items[i].data.KD_PRODUK
			y += z + dsTRDetailKasirIGDList.data.items[i].data.QTY
			y += z + ShowDate(dsTRDetailKasirIGDList.data.items[i].data.TGL_BERLAKU)
			y += z +dsTRDetailKasirIGDList.data.items[i].data.HARGA
			y += z +dsTRDetailKasirIGDList.data.items[i].data.KD_TARIF
			y += z +dsTRDetailKasirIGDList.data.items[i].data.URUT
			
			
			if (i === (dsTRDetailKasirIGDList.getCount()-1))
			{
				x += y 
			}
			else
			{
				x += y + '##[[]]##'
			};
		};
	}	
	
	return x;
};

function GetListCoun2tDetailTransaksi()
{
	
	var x=0;
	for(var i = 0 ; i < dsTRDetailKasirIGDList.getCount();i++)
	{
		if (dsTRDetailKasirIGDList.data.items[i].data.KD_PRODUK != '' || dsTRDetailKasirIGDList.data.items[i].data.DESKRIPSI  != '')
		{
			x += 1;
		};
	}
	return x;
	
};

function TRGawatDaruratColumModel() 
{
    return new Ext.grid.ColumnModel
    (
        [
            new Ext.grid.RowNumberer(),
            {
                id: 'coleskripsiIGD',
                header: 'Uraian',
                dataIndex: 'DESKRIPSI2',
                width:100,
				menuDisabled:true,
				hidden :true
                
            },
            {
                id: 'colKdProduk',
                header: 'Kode Produk',
                dataIndex: 'KD_PRODUK',
                width:100,
				menuDisabled:true,
                hidden:true
            },
            {
                id: 'colDeskripsiIGD',
                header:'Nama Produk',
                dataIndex: 'DESKRIPSI',
                sortable: false,
                hidden:false,
				menuDisabled:true,
                width:220,
				editor: getItemTestIGD(),
				/* new Ext.form.TextField
                (
                    {
                        id:'fieldcolKasirIGD',
                        allowBlank: false,
                        enableKeyEvents : true,
                        listeners:
                        {
                            'specialkey' : function()
                            {
                                if (Ext.EventObject.getKey() === 13)
                                {
                                    var str='';

                              
									str =  kodeunit;
									strb='';
									
									//alert(strb);
									if(Ext.get('fieldcolKasirIGD').dom.value != undefined || Ext.get('fieldcolKasirIGD').dom.value  != '')
									{
									strb= "and lower(deskripsi) like lower(~"+ Ext.get('fieldcolKasirIGD').dom.value+"%~)";
									}
									else
									{
									strb='';
									}
									GetLookupAssetCMRIGD(str,strb);
									Ext.get('fieldcolKasirIGD').dom.value='';
                                };
                            }
                        }
                    }
                ) */
                
            }
			,
            {
				header: 'Tanggal Transaksi',
				dataIndex: 'TGL_TRANSAKSI',
				width: 130,
				menuDisabled:true,
				renderer: function(v, params, record)
				{
					if(record.data.TGL_TRANSAKSI == undefined || record.data.TGL_TRANSAKSI == null){
						record.data.TGL_TRANSAKSI=nowTglTransaksiIGD2;
						return record.data.TGL_TRANSAKSI;
					} else{
						if(record.data.TGL_TRANSAKSI.substring(5, 4) == '-'){
							return ShowDate(record.data.TGL_TRANSAKSI);
						} else{
							var tglb=record.data.TGL_TRANSAKSI.split("-");
						
							if(tglb[2].length == 4 && isNaN(tglb[1])){
								return record.data.TGL_TRANSAKSI;
							} else{
								return ShowDate(record.data.TGL_TRANSAKSI);
							}
						}
						
					}
					//return ShowDate(record.data.TGL_TRANSAKSI);
				}
            },
            {
                id: 'colHARGAIGD',
                header: 'Harga',
				align: 'right',
				hidden: true,
				menuDisabled:true,
                dataIndex: 'HARGA',
                width:100,
				renderer: function(v, params, record) 
							{
							return formatCurrency(record.data.HARGA);
							
							}	
            },
            {
                id: 'colProblemIGD',
                header: 'Qty',
                width:91,
				align: 'right',
				menuDisabled:true,
                dataIndex: 'QTY',
                editor: new Ext.form.TextField
                (
                    {
                        id:'fieldcolProblemIGD',
                        allowBlank: true,
                        enableKeyEvents : true,
                        width:30,
						listeners:
							{ 
								'specialkey' : function()
								{
									
											Dataupdate_KasirIGD(false);
											//RefreshDataFilterKasirIGD();
									        //RefreshDataFilterKasirIGD();

								}
							}
                    }
                ),
				
				
              
            },

            {
                id: 'colImpactIGD',
                header: 'CR',
                width:80,
                dataIndex: 'IMPACT',
				hidden: true,
                editor: new Ext.form.TextField
                (
                        {
                                id:'fieldcolImpactIGD',
                                allowBlank: true,
                                enableKeyEvents : true,
                                width:30
                        }
                )
				
            }

        ]
    )
};


function getItemTestIGD(){
	var radComboboxIGD = new Ext.form.ComboBox({
	   	id: 'cboTindakanRequestIGD',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		hideTrigger:true,
		forceSelection: true,
		selectOnFocus:true,
		store: ProdukDataIGD,
		valueField: 'DESKRIPSI',
		displayField: 'DESKRIPSI',
		anchor: '95%',
		listeners:{
		
			'select': function(a, b, c){
				dsTRDetailKasirIGDList.data.items[CurrentRowPemeriksaanIGD].data.KD_PRODUK = b.data.KD_PRODUK;
				dsTRDetailKasirIGDList.data.items[CurrentRowPemeriksaanIGD].data.KD_KLAS = b.data.KD_KLAS;
				dsTRDetailKasirIGDList.data.items[CurrentRowPemeriksaanIGD].data.HARGA = b.data.TARIF;
				dsTRDetailKasirIGDList.data.items[CurrentRowPemeriksaanIGD].data.TGL_BERLAKU = b.data.TGL_BERLAKU;
				dsTRDetailKasirIGDList.data.items[CurrentRowPemeriksaanIGD].data.KD_TARIF = b.data.KD_TARIF;
				dsTRDetailKasirIGDList.data.items[CurrentRowPemeriksaanIGD].data.TGL_TRANSAKSI = nowTglTransaksiIGD2;
				dsTRDetailKasirIGDList.data.items[CurrentRowPemeriksaanIGD].data.QTY = 1;
		 	}
		}
	});
	return radComboboxIGD;
}

function getProdukIGD(kd_unit){

	var str='LOWER(tarif.kd_tarif)=LOWER(~'+varkd_tarif_igd+'~) and tarif.kd_unit= ~'+kd_unit+'~ ';
	ProdukDataIGD.load({
		params: {
			Skip: 0,
			Take: 50,
			target:'LookupProduk',
			param: str
		}
	});


	return ProdukDataIGD;
}

function GetLookupAssetCMRIGD(str)
{

		var p = new mRecordIGD
		(
			{
				'DESKRIPSI2':'',
				'KD_PRODUK':'',
				'DESKRIPSI':'', 
				'KD_TARIF':'', 
				'HARGA':'',
				'QTY':dsTRDetailKasirIGDList.data.items[CurrentKasirIGD.row].data.QTY,
				'TGL_TRANSAKSI':dsTRDetailKasirIGDList.data.items[CurrentKasirIGD.row].data.TGL_TRANSAKSI,
				'DESC_REQ':dsTRDetailKasirIGDList.data.items[CurrentKasirIGD.row].data.DESC_REQ,
				'KD_TARIF':dsTRDetailKasirIGDList.data.items[CurrentKasirIGD.row].data.KD_TARIF,
				'URUT':dsTRDetailKasirIGDList.data.items[CurrentKasirIGD.row].data.URUT
			}
		);
	
	FormLookupKasir(str,strb,nilai_kd_tarif,dsTRDetailKasirIGDList,p,false,CurrentKasirIGD.row,false,syssetting);
	
};


function DataDeleteKasirIGDDetail()
{
    Ext.Ajax.request
    (
        {
                //url: "./Datapool.mvc/DeleteDataObj",
            url: baseURL + "index.php/main/DeleteDataObj",
            params:  getParamDataDeleteKasirIGDDetail(),
			failure: function(o)
			{
				ShowPesanWarningKasirigd("Gagal dihapus karena pasien telah melakukan pembayaran",nmHeaderHapusData);
				loadMask.hide();
			},
            success: function(o)
            {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true)
                {
                    ShowPesanInfoKasirigd(nmPesanHapusSukses,nmHeaderHapusData);
                    dsTRDetailKasirIGDList.removeAt(CurrentKasirIGD.row);
                    cellSelecteddeskripsi=undefined;
                    RefreshDataKasirIGDDetail(notransaksi);
                    AddNewKasirIGD = false;
                }
                else if (cst.success === false && cst.pesan === 0 )
                {
                    ShowPesanWarningKasirigd(nmPesanHapusGagal, nmHeaderHapusData);
                }
                else
                {
                    ShowPesanWarningKasirigd(nmPesanHapusError,nmHeaderHapusData);
                };
            }
        }
    )
};

function HapusBarisIGD()
{
    if ( cellSelecteddeskripsi != undefined )
    {
        if (cellSelecteddeskripsi.data.DESKRIPSI2 != '' && cellSelecteddeskripsi.data.KD_PRODUK != '')
        {
		
           
                                          
											var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan penghapusan:', function(btn, combo){
											if (btn == 'ok')
														{
														variablehistori=combo;
														//alert(variablehistori=text);
														// process text value and close...
														DataDeleteKasirIGDDetail();
													     
														}
												});

												
                                         
                                
               
        }
        else
        {
            dsTRDetailKasirIGDList.removeAt(CurrentKasirIGD.row);
        };
    }
};
function getItemPaneltgl_filter() 
{
  var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .50,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'datefield',
					    fieldLabel: 'Tanggal ',
					    id: 'dtpTglAwalFilterKasirigd',
					    name: 'dtpTglAwalFilterKasirigd',
					    format: 'd/M/Y',
						//readOnly : true,
					    value: now,
					    anchor: '97.3%',
					    listeners:
						{
						
						 'specialkey' : function()
                            {
                                var tmpNoMedrec = Ext.get('txtFilterNomedrec').getValue()
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10 )
                                {
								RefreshDataFilterKasirIgdKasir();
								}
						}
						}
					}
				]
			},
			// {xtype: 'tbtext', text: ' s/d', cls: 'left-label', width: 20},
			{
			    columnWidth: .50,
			    layout: 'form',
			    border: false,
				labelWidth:60,
			    items:
				[ 
		
					{
					    xtype: 'datefield',
					    fieldLabel: 's/d ',
					    id: 'dtpTglAkhirFilterKasirigd',
					    name: 'dtpTglAkhirFilterKasirigd',
					    format: 'd/M/Y',
						//readOnly : true,
					    value: now,
					    anchor: '100%',
						   listeners:
						{
						
						 'specialkey' : function()
                            {
                                var tmpNoMedrec = Ext.get('txtFilterNomedrec').getValue()
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10 )
                                {
								RefreshDataFilterKasirIgdKasir();
								}
						}
						}
					}
				]
			}
		]
	}
    return items;
};
function getItemPanelcombofilter() 
{
  var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .50,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					mComboStatusBayar_viKasirigdKasir()
				]
			},
			 
			{
			    columnWidth: .50,
			    layout: 'form',
			    border: false,
				labelWidth:60,
			    items:
				[ 
		
				mComboUnit_viKasirigdKasir()
				]
			}
		]
	}
    return items;
};



function mComboStatusBayar_viKasirigdKasir()
{
  var cboStatus_viKasirigdKasir = new Ext.form.ComboBox
	(
		{
			id:'cboStatus_viKasirigdKasir',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
		    anchor:'97.4%',
			emptyText:'',
			fieldLabel: 'Status Lunas ',
			//width:60,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Semua'],[2, 'Lunas'], [3, 'Belum Lunas']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCountStatusByr_viKasirigdKasir,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectCountStatusByr_viKasirigdKasir=b.data.displayText ;
					//RefreshDataSetDokter();
					RefreshDataFilterKasirIgdKasir();

				}
			}
		}
	);
	return cboStatus_viKasirigdKasir;
};


function mComboUnit_viKasirigdKasir() 
{
	var Field = ['KD_UNIT','NAMA_UNIT'];

    dsunit_viKasirigdKasir = new WebApp.DataStore({ fields: Field });
    dsunit_viKasirigdKasir.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			  
                            Sort: 'kd_unit',
			    Sortdir: 'ASC',
			    target: 'ComboUnit',
                param: "" //+"~ )"
			}
		}
	);
	
    var cboUNIT_viKasirigdKasir = new Ext.form.ComboBox
	(
		{
		    id: 'cboUNIT_viKasirigdKasir',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel:  ' Poli ',
		    align: 'Right',
			  width: 100,
		    anchor:'100%',
		    store: dsunit_viKasirigdKasir,
		    valueField: 'NAMA_UNIT',
		    displayField: 'NAMA_UNIT',
			value:'All',
		    listeners:
			{
			
			    'select': function(a, b, c) 
				{					       
						   RefreshDataFilterKasirIgdKasir();

			        //selectStatusCMKasirigdView = b.data.DEPT_ID;
					//
			    }

			}
		}
	);
	cboUNIT_viKasirigdKasir.hide();
    return cboUNIT_viKasirigdKasir;
};


function mComboalasan_transfer() 
{
	var Field = ['KD_ALASAN','ALASAN'];

    var dsalasan_transfer = new WebApp.DataStore({ fields: Field });
    dsalasan_transfer.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			  
                            Sort: 'kd_alasan',
			    Sortdir: 'ASC',
			    target: 'ComboAlasanTransfer',
                param: "" //+"~ )"
			}
		}
	);
	
    var cboalasan_transfer = new Ext.form.ComboBox
	(
		{
		    id: 'cboalasan_transfer',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel:  ' Alasan Transfer ',
		    align: 'Right',
			  width: 100,
		    anchor:'100%',
		    store: dsalasan_transfer,
		    valueField: 'ALASAN',
		    displayField: 'ALASAN',
		    listeners:
			{
			
			    'select': function(a, b, c) 
				{			
					//
			    }

			}
		}
	);
	
    return cboalasan_transfer;
};



function KasirLookUpigd(rowdata) 
{
    var lebar = 580;
    FormLookUpsdetailTRKasirigd = new Ext.Window
    (
        {
            id: 'gridKasirigd',
            title: 'Kasir Gawat Darurat',
            closeAction: 'destroy',
            width: lebar,
            height: 500,
            border: false,
            resizable: false,
            plain: true,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryKasirigd(lebar),
            listeners:
            {
                
            }
        }
    );

    FormLookUpsdetailTRKasirigd.show();
    if (rowdata == undefined) 
	{
        KasirigdAddNew();
    }
    else 
	{
        TRKasirigdInit(rowdata)
    }

};
function load_data_printer_kasirigd(param)
{

	Ext.Ajax.request(
	{
		url: baseURL + "index.php/apotek/functionAPOTEK/printer",
		params:{
			command: param
		} ,
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
		success: function(o) {
			//cbopasienorder_mng_apotek.store.removeAll();
				var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsprinter_kasirigd.recordType;
				var o=cst['listData'][i];
				
				recs.push(new recType(o));
				dsprinter_kasirigd.add(recs);
				console.log(o);
			}
		}
	});
}

function mCombo_printer_kasirigd()
{ 
 
	var Field = ['kd_pasien','nama','kd_dokter','kd_unit','kd_customer','no_transaksi','kd_kasir','id_mrresep','urut_masuk','tgl_masuk','tgl_transaksi'];

    dsprinter_kasirigd = new WebApp.DataStore({ fields: Field });
	
	load_data_printer_kasirigd();
	var cbo_printer_kasirigd= new Ext.form.ComboBox
	(
		{
			id: 'cbopasienorder_printer_kasirigd',
			typeAhead		: true,
			triggerAction	: 'all',
			lazyRender		: true,
			mode			: 'local',
			emptyText: 'Pilih Printer',
			fieldLabel:  '',
			align: 'Right',
			width: 100,
			store: dsprinter_kasirigd,
			valueField: 'name',
			displayField: 'name',
			//hideTrigger		: true,
			listeners:
			{
								
			}
		}
	);return cbo_printer_kasirigd;
};
function getFormEntryKasirigd(lebar) 
{
    var pnlTRKasirigd = new Ext.FormPanel
    (
        {
            id: 'PanelTRKasirigd',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
             height:130,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [getItemPanelInputKasir(lebar)],
           tbar:
            [
             
            ]
        }
    );
 var x;
 var paneltotal = new Ext.Panel
	(
            {
            id: 'paneltotal',
            region: 'center',
            border:false,
            bodyStyle: 'padding:0px 7px 0px 7px',
            layout: 'column',
            frame:true,
            height:67,
            anchor: '100% 8.0000%',
            autoScroll:false,
            items:
            [
                
				
				{
					xtype: 'compositefield',
					anchor: '100%',
					labelSeparator: '',
					border:true,
					style:{'margin-top':'5px'},
					items: 
					[
		                {
                   
                    layout: 'form',
                    style:{'text-align':'right','margin-left':'370px'},
                    border: false,
                    html: " Total :"
						},
						{
                            xtype: 'textfield',
                            id: 'txtJumlah2EditData_viKasirigd',
                            name: 'txtJumlah2EditData_viKasirigd',
							style:{'text-align':'right','margin-left':'390px'},
                            width: 82,
                            readOnly: true,
                        },
						
		                //-------------- ## --------------
					]
				},
				 {
					xtype: 'compositefield',
					anchor: '100%',
					labelSeparator: '',
					border:true,
					style:{'margin-top':'5px'},
					items: 
					[
						{
                   
                    layout: 'form',
						style:{'text-align':'right','margin-left':'370px'},
						border: false,
						hidden:true,
						html: " Bayar :"
						},
						
		                {
                            xtype: 'numberfield',
                            id: 'txtJumlahEditData_viKasirigd',
                            name: 'txtJumlahEditData_viKasirigd',
							style:{'text-align':'right','margin-left':'390px'},
							hidden:true,
                            width: 82,
							
                            //readOnly: true,
                        }
		                //-------------- ## --------------
					]
				}
            ]

        }
    )
 var GDtabDetailKasirigd = new Ext.Panel   
    (
        {
        id:'GDtabDetailKasirigd',
        region: 'center',
        activeTab: 0,
		height:350,
        anchor: '100% 100%',
        border:false,
        plain: true,
        defaults:
                {
                autoScroll: true
		},
                items: [GetDTLTRKasirigdGrid(),paneltotal],
                tbar:
                [
                    {
                        text: 'Bayar',
                        id: 'btnSimpanKasirigd',
                        tooltip: nmSimpan,
                        iconCls: 'save',
                        handler: function()
                        {
                            Datasave_KasirigdKasir(false);
                        }
                    },{
                        id: 'btnTransferKasirigd',
                        text: 'Transfer',
                        tooltip: nmEditData,
                        iconCls: 'Edit_Tr',
						
                        handler: function(sm, row, rec)
                        {
			
						TransferLookUp();
						Ext.Ajax.request
							( {
									   url: baseURL + "index.php/main/GetPasienTranfer",
									   params: {
									   notr: Ext.get(txtNoMedrecDetransaksi).dom.value,
									   },
									   success: function(o)
									   {	var tampungmedrec="";
											o.responseText
											if   (o.responseText==""){
											ShowPesanWarningKasirigd('Pasien Belum terdaftar dirawat Inap ','Transfer');
											FormLookUpsdetailTRTransfer_IGD.close();
											}
											else{ var tmphasil = o.responseText;
													   var tmp = tmphasil.split("<>");
													   Ext.get(txtTranfernoTransaksi).dom.value=tmp[3];
													   Ext.get(dtpTanggalTransfertransaksi).dom.value=ShowDate(tmp[4]);
													   Ext.get(txtTranfernomedrec).dom.value=tmp[5];
													   tampungmedrec=tmp[5];
													   Ext.get(txtTranfernamapasien).dom.value=tmp[2];
													   Ext.get(txtTranferunit).dom.value=tmp[1];
													   Trkdunit2=tmp[6];
													   var kasir=tmp[7];
														Ext.get(txtTranferkelaskamarigd).dom.value=tmp[9];
														tmp_kodeunitkamarigd=tmp[8];
														tmp_kdspesialigd=tmp[10];
														tmp_nokamarigd=tmp[11];
														Trkdunit2=tmp[6];
															  Ext.Ajax.request
																				( {
																			   url: baseURL + "index.php/main/GettotalTranfer",
																			   params: {
																			   notr: notransaksi,
																			   kd_kasir:'igd'
																			   },
																			   success: function(o)
																			   { 
																				Ext.get(txtjumlahbiayasal).dom.value=formatCurrency(o.responseText);
																				Ext.get(txtjumlahtranfer).dom.value=formatCurrency(o.responseText);
																				
																				if( tampungmedrec===undefined)
																				{
																				ShowPesanWarningKasirigd('Data Tidak dapat di transfer, hubungi Admin', 'Gagal');
																				FormLookUpsdetailTRTransfer_IGD.close();
																				
																				}
																				//).dom.value=formatCurrency(o.responseText);
																				
																			   },failure: function(o)
																					{
																						
																						ShowPesanWarningKasirigd('Data Tidak dapat di transfer,hubungi Admin', 'Gagal');
																						FormLookUpsdetailTRTransfer_IGD.close();
																					
																						
																					}
																			   });
															   
										   }
														
									   },failure: function(o)
											{
											/* var datax=tampungmedrec.substring(0, 2); */
												
												ShowPesanWarningKasirigd('Data Tidak dapat di transfer,hubungi Admin', 'Gagal');
												FormLookUpsdetailTRTransfer_IGD.close();
											
												
											}
									   
									   });
                      
                        } //disabled :true
                    },
					mCombo_printer_kasirigd(),
                    {
                        xtype:'splitbutton',
                        text:'Cetak',
                        iconCls:'print',
                        id:'btnPrint_viDaftar',
                        handler:function()
                        {		
                        },
                        menu: new Ext.menu.Menu({
                        items: [
                            {
                                xtype: 'button',
                                text: 'Print Bill',
                                id: 'btnPrintBillIGD',
                                handler: function()
                                {
									if (Ext.getCmp('cbopasienorder_printer_kasirigd').getValue()==='' || Ext.getCmp('cbopasienorder_printer_kasirigd').getValue()===undefined)
									{
										ShowPesanWarningKasirigd('Pilih printer terlebih dahulu !', 'Cetak Data');
									}else
									{
										printbillIGD();
									}
                                    
                                }
                            },
                            {
                                xtype: 'button',
                                text: 'Print Kwitansi',
                                id: 'btnPrintKwitansiIGD',
                                handler: function()
                                {
									if (Ext.getCmp('cbopasienorder_printer_kasirigd').getValue()==='' || Ext.getCmp('cbopasienorder_printer_kasirigd').getValue()===undefined)
									{
										ShowPesanWarningKasirigd('Pilih printer terlebih dahulu !', 'Cetak Data');
									}else
									{
										 printkwitansiIGD();
									}
                                    
                                }
                            }
                        ]
                        })
                    }
		]
	}
    );
	
   
   
   var pnlTRKasirigd2 = new Ext.FormPanel
    (
        {
            id: 'PanelTRKasirigd2',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
             height:380,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [	GDtabDetailKasirigd,
			
			
			]
        }
    );

	
   
   
    var FormDepanKasirigd = new Ext.Panel
	(
		{
		    id: 'FormDepanKasirigd',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
			height:380,
		    shadhow: true,
		    items: [pnlTRKasirigd,pnlTRKasirigd2,
			
				
			]
		}			
				

			

		
	);

    return FormDepanKasirigd
};






function TambahBarisKasirigd()
{
    var x=true;

    if (x === true)
    {
        var p = RecordBaruKasirrwj();
        dsTRDetailKasirigdKasirList.insert(dsTRDetailKasirigdKasirList.getCount(), p);
    };
};





function GetDTLTRHistoryGridIGD() 
{

    var fldDetail = ['NO_TRANSAKSI','TGL_BAYAR','DESKRIPSI','URUT','BAYAR','USERNAME','SHIFT','KD_USER'];
	
    dsTRDetailHistoryListIGD = new WebApp.DataStore({ fields: fldDetail })
		 
    var gridDTLTRHistoryIGD = new Ext.grid.EditorGridPanel
    (
        {
            title: 'History Bayar',
            stripeRows: true,
            store: dsTRDetailHistoryListIGD,
            border: false,
            columnLines: true,
            frame: false,
				anchor: '100% 25%',
          
                autoScroll:true,
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                            cellSelecteddeskripsi = dsTRDetailHistoryListIGD.getAt(row);
                            CurrentHistory.row = row;
                            CurrentHistory.data = cellSelecteddeskripsi;
                        }
                    }
                }
            ),
            cm: TRHistoryColumModel()
			,
viewConfig:{forceFit: true},
			      tbar:
                [
                       
                                             
							    {
                                id:'btnHpsBrsKasirigd',
                                text: 'Hapus Pembayaran',
                                tooltip: 'Hapus Baris',
                                iconCls: 'RemoveRow',
								//hidden :true,
                                handler: function()
                                {
                                        if (dsTRDetailHistoryListIGD.getCount() > 0 )
                                        {
                                                if (cellSelecteddeskripsi != undefined)
                                                {
                                                        if(CurrentHistory != undefined)
                                                        {
                                                                HapusBarisDetailbayar();
                                                        }
                                                }
                                                else
                                                {
                                                        ShowPesanWarningKasirigd('Pilih record ','Hapus data');
                                                }
                                        }
										 Ext.getCmp('btnEditKasirigd').disable();
											Ext.getCmp('btnTutupTransaksiKasirigd').disable();
										Ext.getCmp('btnHpsBrsKasirigd').disable();
                                },
								disabled :true
                        }
                        
							 
							
                ]
             
        }
		
		
    );
	
	

    return gridDTLTRHistoryIGD;
};

function TRHistoryColumModel() 
{
    return new Ext.grid.ColumnModel
    (//'','','','','',''
        [
           new Ext.grid.RowNumberer(),
            {
                id: 'colKdTransaksi',
                header: 'No. Transaksi',
                dataIndex: 'NO_TRANSAKSI',
                width:100,
					menuDisabled:true,
                hidden:false
            },
			{
                id: 'colTGlbayar',
                header: 'Tgl Bayar',
                dataIndex: 'TGL_BAYAR',
					menuDisabled:true,
				width:100,
				renderer: function(v, params, record)
                {
                   return ShowDate(record.data.TGL_BAYAR);

                }
                
            },
			{
                id: 'coleurutmasuk',
                header: 'urut Bayar',
                dataIndex: 'URUT',
				//hidden:true
                
            }
            
            ,
			{
                id: 'colePembayaran',
                header: 'Pembayaran',
                dataIndex: 'DESKRIPSI',
				width:150,
				hidden:false
                
            }
			,
			{
                id: 'colJumlah',
                header: 'Jumlah',
				width:150,
				align :'right',
                dataIndex: 'BAYAR',
				hidden:false,
				renderer: function(v, params, record)
                {
                   return formatCurrency(record.data.BAYAR);

                }
                
            }
			,
			
			{
                id: 'coletglmasuk',
                header: 'tgl masuk',
                dataIndex: '',
				hidden:true
                
            }
            ,
            {
                id: 'colStatHistory',
                header: 'History',
                width:130,
				menuDisabled:true,
                dataIndex: '',
				hidden:true
              
				
				
              
            },
            {
                id: 'colPetugasHistory',
                header: 'Petugas',
                width:130,
				menuDisabled:true,
                dataIndex: 'USERNAME',	
				//hidden:true
                
				
				
              
            }
			

        ]
    )
};
function HapusBarisDetailbayar()
{
    if ( cellSelecteddeskripsi != undefined )
    {
        if (cellSelecteddeskripsi.data.NO_TRANSAKSI != '' && cellSelecteddeskripsi.data.URUT != '')
        {
		
           
                                          //'NO_TRANSAKSI','TGL_BAYAR','DESKRIPSI','URUT','BAYAR','USERNAME'
											var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan penghapusan:', function(btn, combo){
											if (btn == 'ok')
														{
														variablehistori=combo;
																		if (variablehistori!='')
																		{
																		DataDeleteKasirigdKasirDetail();
																		}
																		else
																		{
																			ShowPesanWarningKasirigd('Silahkan isi alasan terlebih dahaulu','Keterangan');
																	
																		}
														}
														
												});
											

												
                                         
                                
               
        }
        else
        {
            dsTRDetailHistoryListIGD.removeAt(CurrentHistory.row);
        };
    }
};

function DataDeleteKasirigdKasirDetail()
{
    Ext.Ajax.request
    (
        {
            url: baseURL + "index.php/main/functionIGD/deletedetail_bayar",
            params:  getParamDataDeleteKasirigdKasirDetail(),
            success: function(o)
            {
			//	RefreshDatahistoribayar(Kdtransaksi);
				RefreshDataFilterKasirIgdKasir();
				 RefreshDatahistoribayar('0');
                var cst = Ext.decode(o.responseText);
                if (cst.success === true)
                {
                    ShowPesanInfoKasirigd(nmPesanHapusSukses,nmHeaderHapusData);
					RefreshDataFilterKasirIgdKasir();
								//alert(Kdtransaksi);					 
						
					//refeshKasirIgdKasir();
                }
                else if (cst.success === false && cst.pesan === 'LUNAS' )
                {
                    ShowPesanWarningKasirigd('Pembayaran tidak dapat dihapus karena sudah erdapat pembayaran pada transaksi ini', nmHeaderHapusData);
                }
                else
                {
                    ShowPesanWarningKasirigd(nmPesanHapusError,nmHeaderHapusData);
                };
            }
        }
    )
};

function getParamDataDeleteKasirigdKasirDetail()
{
    var params =
    {

        TrKodeTranskasi: CurrentHistory.data.data.NO_TRANSAKSI,
		TrTglbayar:  CurrentHistory.data.data.TGL_BAYAR,
		Urut:  CurrentHistory.data.data.URUT,
		Jumlah:  CurrentHistory.data.data.BAYAR,
		Username :CurrentHistory.data.data.USERNAME,
		Shift: 1,//shift na dipatok hela ke ganti di phpna
		Shift_hapus: 1,//shift na dipatok hela ke ganti di phpna
		Kd_user: CurrentHistory.data.data.KD_USER,
		Tgltransaksi :tgltrans, 
        Kodepasein:kodepasien,
		NamaPasien:namapasien,
        KodeUnit: kodeunit,
        Namaunit:namaunit,
        Kodepay:kodepay,
		Kdcustomer:kdcustomeraa,
        Uraian:CurrentHistory.data.data.DESKRIPSI,
		KDkasir: '06'
		
    };
	Kdtransaksi=CurrentHistory.data.data.NO_TRANSAKSI;
    return params
};



function GetDTLTRKasirigdGrid() 
{
    var fldDetail = ['KD_PRODUK','DESKRIPSI','DESKRIPSI2','KD_TARIF','HARGA','QTY','DESC_REQ','TGL_BERLAKU','NO_TRANSAKSI','URUT','DESC_STATUS','TGL_TRANSAKSI','BAYARTR','DISCOUNT','PIUTANG'];
	
dsTRDetailKasirigdKasirList = new WebApp.DataStore({ fields: fldDetail })
RefreshDataKasirigdKasirDetail() ;
gridDTLTRKasirigd = new Ext.grid.EditorGridPanel
    (
        {
            title: 'Detail Pembayaran',
            stripeRows: true,
			id: 'gridDTLTRKasirigd',
            store: dsTRDetailKasirigdKasirList,
            border: false,
            columnLines: true,
            frame: false,
            anchor: '100%',
			height:230,
            autoScroll:true,
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        
                    }
                }
            ),
            cm: TRKasirRawatJalanColumModel()
        }
		
		
    );
	
	

    return gridDTLTRKasirigd;
};

function TRKasirRawatJalanColumModel() 
{
    return new Ext.grid.ColumnModel
    (
        [
            new Ext.grid.RowNumberer(),
            {
                id: 'coleskripsiKasirigd',
                header: 'Uraian',
                dataIndex: 'DESKRIPSI2',
                width:250,
				menuDisabled:true,
				hidden :true
                
            },
            {
                id: 'colKdProduk',
                header: 'Kode Produk',
                dataIndex: 'KD_PRODUK',
                width:100,
				menuDisabled:true,
                hidden:true
            },
            {
                id: 'colDeskripsiKasirigd',
                header:'Nama Produk',
                dataIndex: 'DESKRIPSI',
                sortable: false,
                hidden:false,
				menuDisabled:true,
                width:250
                
            },
			   {
                id: 'colURUTKasirigd',
                header:'Urut',
                dataIndex: 'URUT',
                sortable: false,
                hidden:true,
				menuDisabled:true,
                width:250
                
            }
			,
            {
               header: 'Tanggal Transaksi',
               dataIndex: 'TGL_TRANSAKSI',
               width: 130,
			    hidden:true,
			   menuDisabled:true,
               renderer: function(v, params, record)
                    {
                        
                        return ShowDate(record.data.TGL_TRANSAKSI);
                    }
            },
            {
                id: 'colQtyKasirigd',
                header: 'Qty',
                width:91,
				align: 'right',
				menuDisabled:true,
                dataIndex: 'QTY',
             
				
				
              
            },
            {
                id: 'colHARGAKasirigd',
                header: 'Harga',
				align: 'right',
				hidden: false,
				menuDisabled:true,
                dataIndex: 'HARGA',
                width:100,
				
				renderer: function(v, params, record) 
							{
							return formatCurrency(record.data.HARGA);
							
							}	
            },

            {
                id: 'colPiutangKasirigd',
                header: 'Puitang',
                width:80,
                dataIndex: 'PIUTANG',
				align: 'right',
				//hidden: false,
			
				 editor: new Ext.form.TextField
                (
                    {
                        id:'fieldcolPuitangigd',
                        allowBlank: true,
                        enableKeyEvents : true,
                        width:30,
						
                    }
                )
             ,
				renderer: function(v, params, record) 
							{
							
							//getTotalDetailProduk();
						
								return formatCurrency(record.data.PIUTANG);
							
						
							
							
							}
            },
			{
                id: 'colTunaiKasirigd',
                header: 'Tunai',
                width:80,
                dataIndex: 'BAYARTR',
				align: 'right',
				hidden: false,
							 editor: new Ext.form.TextField
								(
									{
										id:'fieldcolTunaiigd',
										allowBlank: true,
										enableKeyEvents : true,
										width:30,
										
									}
								),
				renderer: function(v, params, record) 
							{
							
							getTotalDetailProduk();
					
							return formatCurrency(record.data.BAYARTR);
							
							
							}
				
            },
			{
                id: 'colDiscountKasirigd',
                header: 'Discount',
                width:80,
                dataIndex: 'DISCOUNT',
				align: 'right',
				hidden: false,
				editor: new Ext.form.TextField
								(
									{
										id:'fieldcolDiscountigd',
										allowBlank: true,
										enableKeyEvents : true,
										width:30,
										
									}
								)
				,
					renderer: function(v, params, record) 
							{
							return formatCurrency(record.data.DISCOUNT);
							
							
							}
							 
				
            }

        ]
    )
};




function RecordBaruKasirrwj()
{

	var p = new mRecordKasirrwj
	(
		{
			'DESKRIPSI2':'',
			'KD_PRODUK':'',
		    'DESKRIPSI':'', 
		    'KD_TARIF':'', 
		    'HARGA':'',
		    'QTY':'',
		    'TGL_TRANSAKSI':tanggaltransaksitampung, 
		    'DESC_REQ':'',
		    'KD_TARIF':'',
		    'URUT':''
		}
	);
	
	return p;
};
function TRKasirigdInit(rowdata)
{
    AddNewKasirigdKasir = false;
	
	vkd_unit = rowdata.KD_UNIT;
	RefreshDataKasirigdKasirDetail(rowdata.NO_TRANSAKSI);
	Ext.get('txtNoTransaksiKasirigdKasir').dom.value = rowdata.NO_TRANSAKSI;
	tanggaltransaksitampung = rowdata.TANGGAL_TRANSAKSI;
    Ext.get('dtpTanggalDetransaksi').dom.value = ShowDate(rowdata.TANGGAL_TRANSAKSI);
	Ext.get('txtNoMedrecDetransaksi').dom.value= rowdata.KD_PASIEN;
	Ext.get('txtNamaPasienDetransaksi').dom.value = rowdata.NAMA;
	Ext.get('cboPembayaran').dom.value= rowdata.KET_PAYMENT;
	Ext.get('cboJenisByr').dom.value= rowdata.CARA_BAYAR; // take the displayField value 
	loaddatastorePembayaran(rowdata.JENIS_PAY);
	vkode_customer = rowdata.KD_CUSTOMER;
	tampungtypedata=0;
	tapungkd_pay=rowdata.KD_PAY;
	jenispay=1;
	var vkode_customer = rowdata.LUNAS;
	showCols(Ext.getCmp('gridDTLTRKasirigd'));
	hideCols(Ext.getCmp('gridDTLTRKasirigd'));
	vflag= rowdata.FLAG;
	tgltrans=rowdata.TANGGAL_TRANSAKSI;
	kodepasien=rowdata.KD_PASIEN;
	namapasien=rowdata.NAMA;
	kodeunit=rowdata.KD_UNIT;
	namaunit=rowdata.NAMA_UNIT;
	kodepay=rowdata.KD_PAY;
	kdcustomeraa=rowdata.KD_CUSTOMER;
	notransaksi=rowdata.NO_TRANSAKSI;
	
	Ext.Ajax.request(
	{
	   
	    url: baseURL + "index.php/main/getcurrentshift",
		 params: {
	        
	        command: '0',
		
	    },
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
	    success: function(o) {
	
			tampungshiftsekarang=o.responseText




	    }
	
	});

	
	
};

function mEnabledKasirigdCM(mBol)
{

	 Ext.get('btnLookupKasirigd').dom.disabled=mBol;
	 Ext.get('btnHpsBrsKasirigd').dom.disabled=mBol;
};


///---------------------------------------------------------------------------------------///
function KasirigdAddNew() 
{
    AddNewKasirigdKasir = true;
	Ext.get('txtNoTransaksiKasirigdKasir').dom.value = '';
    Ext.get('dtpTanggalDetransaksi').dom.value = nowTglTransaksiIGD.format('d/M/Y');
	Ext.get('txtNoMedrecDetransaksi').dom.value='';
	Ext.get('txtNamaPasienDetransaksi').dom.value = '';
	Ext.get('cboStatus_viKasirigdKasir').dom.value= ''
	rowSelectedKasirigdKasir=undefined;
	dsTRDetailKasirigdKasirList.removeAll();
	mEnabledKasirigdCM(false);
	

};

function RefreshDataKasirigdKasirDetail(no_transaksi) 
{
    var strKriteriaKasirigd='';
   
    strKriteriaKasirigd = 'no_transaksi = ~' + no_transaksi + '~ and kd_kasir = ~06~';

    dsTRDetailKasirigdKasirList.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'tgl_transaksi',
			    //Sort: 'tgl_transaksi',
			    Sortdir: 'ASC',
			    target: 'ViewDetailbayarIGD',
			    param: strKriteriaKasirigd
			}
		}
	);
    return dsTRDetailKasirigdKasirList;
};

function RefreshDatahistoribayar(no_transaksi) 
{
    var strKriteriaKasirigd='';
   
    strKriteriaKasirigd = 'no_transaksi= ~' + no_transaksi + '~';

    dsTRDetailHistoryListIGD.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'tgl_transaksi',
			    //Sort: 'tgl_transaksi',
			    Sortdir: 'ASC',
			    target: 'ViewHistoryBayarIGD',
			    param: strKriteriaKasirigd
			}
		}
	);
    return dsTRDetailHistoryListIGD;
};


///---------------------------------------------------------------------------------------///



///---------------------------------------------------------------------------------------///
function getParamDetailTransaksiKasirigd() 
{
	if(tampungtypedata==='')
		{
		tampungtypedata=0;
		};
		
		var params =
		{
	
	//	Table:'',
	
		kdUnit : vkd_unit,
		TrKodeTranskasi: Ext.get('txtNoTransaksiKasirigdKasir').getValue(),
		Tgl: Ext.get('dtpTanggalDetransaksi').dom.value,
		Shift: tampungshiftsekarang,
		Flag:vflag,
		bayar: tapungkd_pay,
		
		Typedata: tampungtypedata,
		Totalbayar:getTotalDetailProduk(),
		List:getArrDetailTrKasirigd(),
		JmlField: mRecordKasirigd.prototype.fields.length-4,
		JmlList:GetListCountDetailTransaksi(),
		
		Hapus:1,
		Ubah:0
		};
    return params
};

function paramUpdateTransaksi()
{
 var params =
	{
            TrKodeTranskasi: cellSelectedtutup_kasirigd,
            KDUnit : kdUnit
	};
	return params;
};

function GetListCountDetailTransaksi()
{
	
	var x=0;
	for(var i = 0 ; i < dsTRDetailKasirigdKasirList.getCount();i++)
	{
		if (dsTRDetailKasirigdKasirList.data.items[i].data.KD_PRODUK != '' || dsTRDetailKasirigdKasirList.data.items[i].data.DESKRIPSI  != '')
		{
			x += 1;
		};
	}
	return x;
	
};

function getTotalDetailProduk()
{
var TotalProduk=0;
var bayar;
	var tampunggrid ;
	var x='';
	for(var i = 0 ; i < dsTRDetailKasirigdKasirList.getCount();i++)
	{		

			var recordterakhir;
		 
			//alert(TotalProduk);
		if (tampungtypedata==0)
		{
		tampunggrid = parseInt(dsTRDetailKasirigdKasirList.data.items[i].data.BAYARTR);
			
			//recordterakhir= tampunggrid
			//TotalProduk.toString().replace(/./gi, "");
			TotalProduk+= tampunggrid
		  
			recordterakhir=TotalProduk
			Ext.get('txtJumlah2EditData_viKasirigd').dom.value=formatCurrency(recordterakhir);
		}
		if(tampungtypedata==3)
		{
			tampunggrid = parseInt(dsTRDetailKasirigdKasirList.data.items[i].data.PIUTANG);
			//TotalProduk.toString().replace(/./gi, "");
			//recordterakhir=tampunggrid
			TotalProduk+=tampunggrid
		     recordterakhir=TotalProduk
			Ext.get('txtJumlah2EditData_viKasirigd').dom.value=formatCurrency(recordterakhir);
		}
		if(tampungtypedata==1)
		{
		   tampunggrid = parseInt(dsTRDetailKasirigdKasirList.data.items[i].data.DISCOUNT);
		//	TotalProduk.toString().replace(/./gi, "");
			//recordterakhir=dsTRDetailKasirigdKasirList.data.items[i].data.DISCOUNT
			TotalProduk+=tampunggrid
		    recordterakhir=TotalProduk
			Ext.get('txtJumlah2EditData_viKasirigd').dom.value=formatCurrency(recordterakhir);
		}
	
		
		
	}	
	bayar = Ext.get('txtJumlah2EditData_viKasirigd').getValue();
	return bayar;
};







function getArrDetailTrKasirigd()
{
	var x='';
	for(var i = 0 ; i < dsTRDetailKasirigdKasirList.getCount();i++)
	{
		if (dsTRDetailKasirigdKasirList.data.items[i].data.KD_PRODUK != '' && dsTRDetailKasirigdKasirList.data.items[i].data.DESKRIPSI != '')
		{
			var y='';
			var z='@@##$$@@';
			
			y = 'URUT=' + dsTRDetailKasirigdKasirList.data.items[i].data.URUT
			y += z + dsTRDetailKasirigdKasirList.data.items[i].data.KD_PRODUK
			y += z + dsTRDetailKasirigdKasirList.data.items[i].data.QTY
			y += z +dsTRDetailKasirigdKasirList.data.items[i].data.HARGA
			y += z +dsTRDetailKasirigdKasirList.data.items[i].data.KD_TARIF
			y += z +dsTRDetailKasirigdKasirList.data.items[i].data.URUT
			y += z +dsTRDetailKasirigdKasirList.data.items[i].data.BAYARTR
			y += z +dsTRDetailKasirigdKasirList.data.items[i].data.PIUTANG
			y += z +dsTRDetailKasirigdKasirList.data.items[i].data.DISCOUNT
			y += z +dsTRDetailKasirigdKasirList.data.items[i].data.TGL_TRANSAKSI
			
			
			if (i === (dsTRDetailKasirigdKasirList.getCount()-1))
			{
				x += y 
			}
			else
			{
				x += y + '##[[]]##'
			};
		};
	}	
	
	return x;
};


function getItemPanelInputKasir(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 0px',
		border:true,
		height:110,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
			    layout: 'form',
			    border: false,
			    items:
				[
					getItemPanelNoTransksiigdKasir(lebar),getItemPanelmedreckasir(lebar),getItemPanelUnitKasir(lebar)	
				]
			}
		]
	};
    return items;
};



function getItemPanelUnitKasir(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
				mComboJenisByrView()
				
				]
			},{
			    columnWidth: .60,
			    layout: 'form',
				labelWidth:0.9,
			    border: false,
			    items:
				[mComboPembayaran()
	
				]
			}
		]
	}
    return items;
};



function loaddatastorePembayaran(jenis_pay)
{
          dsComboBayar.load
                (
                    {
                     params:
							{
								Skip: 0,
								Take: 1000,
								Sort: 'nama',
								Sortdir: 'ASC',
								target: 'ViewComboBayar',
								param: 'jenis_pay=~'+ jenis_pay+ '~ order by uraian asc'
							}
                   }
                )
}

function mComboPembayaran()
{
    var Field = ['KD_PAY','JENIS_PAY','PAYMENT'];

    dsComboBayar = new WebApp.DataStore({fields: Field});


    var cboPembayaran = new Ext.form.ComboBox
	(
		{
		    id: 'cboPembayaran',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
            forceSelection: true,
		    emptyText:'Pilih Pembayaran...',
		    labelWidth:80,
		    align: 'Right',
		    store: dsComboBayar,
		    valueField: 'KD_PAY',
		    displayField: 'PAYMENT',
            anchor: '100%',
			listeners:
			{
			    'select': function(a, b, c) 
				{
				
						tapungkd_pay=b.data.KD_PAY;
						//getTotalDetailProduk();
					
			   
				},
				  

			}
		}
	);

    return cboPembayaran;
};


function mComboJenisByrView() 
{
	var Field = ['JENIS_PAY','DESKRIPSI','TYPE_DATA'];

    dsJenisbyrView = new WebApp.DataStore({ fields: Field });
    dsJenisbyrView.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
		
                            Sort: 'jenis_pay',
			    Sortdir: 'ASC',
			    target: 'ComboJenis',
			  
			}
		}
	);
	
    var cboJenisByr = new Ext.form.ComboBox
	(
		{
	
		    id: 'cboJenisByr',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
			fieldLabel: 'Pembayaran      ',

		    align: 'Right',
		    anchor:'100%',
		    store: dsJenisbyrView,
		    valueField: 'JENIS_PAY',
		    displayField: 'DESKRIPSI',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
					
						 loaddatastorePembayaran(b.data.JENIS_PAY);
						 tampungtypedata=b.data.TYPE_DATA;
						 jenispay=b.data.JENIS_PAY;
						 showCols(Ext.getCmp('gridDTLTRKasirigd'));
						 hideCols(Ext.getCmp('gridDTLTRKasirigd'));
						getTotalDetailProduk();
						Ext.get('cboPembayaran').dom.value='Pilih Pembayaran...';
				
					
			   
				},
				  

			}
		}
	);
	
    return cboJenisByr;
};
    function hideCols (grid)
	{
		if (tampungtypedata==3)
		{
			grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('BAYARTR'), true);
			grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('DISCOUNT'), true);
		}
		else if(tampungtypedata==0)
		{
			grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('PIUTANG'), true);
			grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('DISCOUNT'), true);
		}
		else if(tampungtypedata==1)
		{
			grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('PIUTANG'), true);
			grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('BAYARTR'), true);
			
		}
	};
      function showCols (grid) {   
	  	if (tampungtypedata==3)
			{
			grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('PIUTANG'), false);
		
			}
			else if(tampungtypedata==0)
			{
				grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('BAYARTR'), false);
			}
			else if(tampungtypedata==1)
			{
				grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('DISCOUNT'), false);
			}
	
	};



function getItemPanelDokter(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel:'Dokter  ',
					    name: 'txtKdDokter',
					    id: 'txtKdDokter',
						//emptyText:nmNomorOtomatis,
						readOnly:true,
					    anchor: '99%'
					},{
					    xtype: 'textfield',
					    fieldLabel: 'Kelompok Pasien',
						//hideLabel:true,
						readOnly:true,
					    name: 'txtCustomer',
					    id: 'txtCustomer',
					    anchor: '99%',
						listeners: 
						{ 
							
						}
					}
				]
			},
			{
			    columnWidth: .600,
			    layout: 'form',
			    border: false,
				labelWidth:2,
			    items:
				[
					{
						xtype: 'textfield',
					   // fieldLabel:'Unit : ',
					    name: 'txtNamaDokter',
					    id: 'txtNamaDokter',
						//emptyText:nmNomorOtomatis,
						readOnly:true,
					    anchor: '100%'
					},
					{
						xtype: 'textfield',
					   // fieldLabel:'Unit : ',
					    name: 'txtKdUrutMasuk',
					    id: 'txtKdUrutMasuk',
						//emptyText:nmNomorOtomatis,
						readOnly:true,
						hidden:true,
					    anchor: '100%'
					}
				]
			}
		]
	}
    return items;
};

function getItemPanelNoTransksiigdKasir(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel:  'No. Transaksi ',
					    name: 'txtNoTransaksiKasirigdKasir',
					    id: 'txtNoTransaksiKasirigdKasir',
						emptyText:nmNomorOtomatis,
						readOnly:true,
					    anchor: '99%'
					}
				]
			},
			{
			    columnWidth: .60,
			    layout: 'form',
			    border: false,
				labelWidth:55,
			    items:
				[
					{
					    xtype: 'datefield',
					    fieldLabel: 'Tanggal ',
					    id: 'dtpTanggalDetransaksi',
					    name: 'dtpTanggalDetransaksi',
					    format: 'd/M/Y',
						readOnly : true,
					    value: now,
					    anchor: '100%'
					}
				]
			}
		]
	}
    return items;
};


function getItemPanelmedreckasir(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel:   'No. Medrec ',
					    name: 'txtNoMedrecDetransaksi',
					    id: 'txtNoMedrecDetransaksi',
						readOnly:true,
					    anchor: '99%',
					    listeners: 
						{ 
							
						}
					}
				]
			},
			{
			    columnWidth: .60,
			    layout: 'form',
			    border: false,
				labelWidth:2,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: '',
						//hideLabel:true,
						readOnly:true,
					    name: 'txtNamaPasienDetransaksi',
					    id: 'txtNamaPasienDetransaksi',
					    anchor: '100%',
						listeners: 
						{ 
							
						}
					}
				]
			}
		]
	}
    return items;
};


function RefreshDataKasirigdKasir() 
{
    dsTRKasirigdKasirList.load
    (
        {
            params:
            {
                Skip: 0,
                Take: selectCountKasirigdKasir,
                Sort: 'tgl_transaksi',
                //Sort: 'tgl_transaksi',
                Sortdir: 'ASC',
                target:'ViewKasirIGD',
                param : ''
            }		
        }
    );
	
    rowSelectedKasirigdKasir = undefined;
    return dsTRKasirigdKasirList;
};

function refeshKasirIgdKasir()
{
dsTRKasirigdKasirList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountKasirigdKasir, 
					//Sort: 'no_transaksi',
                     Sort: '',
					//Sort: 'no_transaksi',
					Sortdir: 'ASC', 
					target:'ViewKasirIGD',
					param : ''
				}			
			}
		);   
		return dsTRKasirigdKasirList;
}

function RefreshDataFilterKasirIgdKasir() 
{

	var KataKunci='';
	
	 if (Ext.get('txtFilterNomedrec').getValue() != '')
    {
		if (KataKunci == '')
		{
                        KataKunci = ' and   LOWER(kode_pasien) like  LOWER( ~' + Ext.get('txtFilterNomedrec').getValue() + '%~)';
			
		}
		else
		{
		
                        KataKunci += ' and  LOWER(kode_pasien) like  LOWER( ~' + Ext.get('txtFilterNomedrec').getValue() + '%~)';
		};

	};
	
	 if (Ext.get('TxtFilterGridDataView_NAMA_viKasirigdKasir').getValue() != '')
    {
		if (KataKunci == '')
		{
                        KataKunci = ' and   LOWER(nama) like  LOWER( ~%' + Ext.get('TxtFilterGridDataView_NAMA_viKasirigdKasir').getValue() + '%~)';
			
		}
		else
		{
		
                        KataKunci += ' and  LOWER(nama) like  LOWER( ~%' + Ext.get('TxtFilterGridDataView_NAMA_viKasirigdKasir').getValue() + '%~)';
		};

	};
	
	
	 if (Ext.get('cboUNIT_viKasirigdKasir').getValue() != '' && Ext.get('cboUNIT_viKasirigdKasir').getValue() != 'All')
    {
		if (KataKunci == '')
		{
	
                        KataKunci = ' and  LOWER(nama_unit)like  LOWER(~%' + Ext.get('cboUNIT_viKasirigdKasir').getValue() + '%~)';
		}
		else
		{
	
                        KataKunci += ' and LOWER(nama_unit) like  LOWER(~%' + Ext.get('cboUNIT_viKasirigdKasir').getValue() + '%~)';
		};
	};

		
	if (Ext.get('cboStatus_viKasirigdKasir').getValue() == 'Lunas')
	{
		if (KataKunci == '')
		{

                        KataKunci = ' and  lunas = ~true~';
		}
		else
		{
		
                        KataKunci += ' and lunas =  ~true~';
		};
	
	};
		

		if (Ext.get('cboStatus_viKasirigdKasir').getValue() == 'Semua')
	{
		
		
		
	};
	if (Ext.get('cboStatus_viKasirigdKasir').getValue() == 'Belum Lunas')
	{
		if (KataKunci == '')
		{
		
                        KataKunci = ' and  lunas = ~false~';
		}
		else
		{
	
                        KataKunci += ' and lunas =  ~false~';
		};
		
		
	};
	
		
	if (Ext.get('dtpTglAwalFilterKasirigd').getValue() != '')
	{
		if (KataKunci == '')
		{                      
						KataKunci = " and (tgl_transaksi between ~" + Ext.get('dtpTglAwalFilterKasirigd').getValue() + "~ and ~" + Ext.get('dtpTglAkhirFilterKasirigd').getValue() + "~)";
		}
		else
		{
			
                        KataKunci += " and (tgl_transaksi between ~" + Ext.get('dtpTglAwalFilterKasirigd').getValue() + "~ and ~" + Ext.get('dtpTglAkhirFilterKasirigd').getValue() + "~)";
		};
	
	};
	
     
    if (KataKunci != undefined ||KataKunci != '' ) 
    {  
		dsTRKasirigdKasirList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountKasirigdKasir, 
					//Sort: 'no_transaksi',
                                        Sort: 'tgl_transaksi',
					//Sort: 'no_transaksi',
					Sortdir: 'ASC', 
					target:'ViewKasirIGD',
					param : KataKunci
				}			
			}
		);   
    }
	else
	{
	
	dsTRKasirigdKasirList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountKasirrwjKasir, 
					//Sort: 'no_transaksi',
                                        Sort: 'tgl_transaksi',
					//Sort: 'no_transaksi',
					Sortdir: 'ASC', 
					target:'ViewKasirIGD',
					param : ''
				}			
			}
		);   
	};
    
	return dsTRKasirigdKasirList;
};



function Datasave_KasirigdKasir(mBol) 
{	
	if (ValidasiEntryCMKasirigd(nmHeaderSimpanData,false) == 1 )
	{
			Ext.Ajax.request
			 (
				{
					//url: "./Datapool.mvc/CreateDataObj",
					url: baseURL + "index.php/main/functionIGD/savepembayaran",
					params: getParamDetailTransaksiKasirigd(),
					failure: function(o)
					{
					ShowPesanWarningKasirigd('Data Belum Simpan segera Hubungi Admin', 'Gagal');
					 RefreshDataFilterKasirIgdKasir();
					},	
					success: function(o) 
					{
						RefreshDatahistoribayar(0);
						RefreshDataKasirigdKasirDetail(Ext.get('txtNoTransaksiKasirigdKasir').dom.value);
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoKasirigd(nmPesanSimpanSukses,nmHeaderSimpanData);
							//FormLookUpsdetailTRKasirigd.close();
							//RefreshDataKasirigdKasir();
							
							if(mBol === false)
							{
									 RefreshDataFilterKasirIgdKasir();
							};
						}
						else 
						{
								ShowPesanWarningKasirigd('Data Belum Simpan segera Hubungi Admin', 'Gagal');
						};
					}
				}
			)
		
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
	
};

function BatalTransaksi(mBol)
{

			Ext.Ajax.request
			 (
				{
					//url: "./Datapool.mvc/CreateDataObj",
					url: baseURL + "index.php/main/functionIGD/batal_transaksi",
					params: getParamDetailBatalTransaksiIGD(),
					failure: function(o)
					{
					 ShowPesanWarningKasirigd(Ext.decode(o.responseText), 'Gagal');
					// RefreshDataFilterKasirrwjKasir();
					},
					success: function(o)
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true)
						{
							ShowPesanInfoKasirigd(nmPesanSimpanSukses,nmHeaderSimpanData);
							RefreshDataFilterKasirIgdKasir();
							if(mBol === false)
							{
							//RefreshDataFilterKasirrwjKasir();
							};
							cellSelectedtutup_kasirigd='';
						}
						else
						{
								ShowPesanWarningKasirigd(Ext.decode(o.responseText), 'Gagal');
								cellSelectedtutup_kasirigd='';
						};
					}
				}
			)
};
function getParamDetailBatalTransaksiIGD()
{
	if(tampungtypedata==='')
		{
		tampungtypedata=0;
		};

    var params =
	{
		kdPasien 	: kodepasien,
		noTrans 	: notransaksi,
		kdUnit 		: kodeunit,
		kdDokter 	: kdokter_btl,
		tglTrans 	: tgltrans,
		kdCustomer	: kdcustomeraa,
		Keterangan	: variablebatalhistori_igd,
		
	};
    return params
};
function Datasave_KasirigdKasir(mBol) 
{	
	if (ValidasiEntryCMKasirigd(nmHeaderSimpanData,false) == 1 )
	{
			Ext.Ajax.request
			 (
				{
					url: baseURL + "index.php/main/functionIGD/savepembayaran",
					params: getParamDetailTransaksiKasirigd(),
					failure: function(o)
					{
					ShowPesanWarningKasirigd('Data Belum Simpan segera Hubungi Admin', 'Gagal');
					 RefreshDataFilterKasirIgdKasir();
					},	
					success: function(o) 
					{
						RefreshDatahistoribayar(0);
					
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoKasirigd(nmPesanSimpanSukses,nmHeaderSimpanData);
							RefreshDataKasirigdKasirDetail(Ext.get('txtNoTransaksiKasirigdKasir').dom.value);
							//RefreshDataKasirigdKasir();
							/* if(mBol === false)
							{ */
									 RefreshDataFilterKasirIgdKasir();
							//};
						}
						else 
						{
								ShowPesanWarningKasirigd('Data Belum Simpan segera Hubungi Admin', 'Gagal');
						};
					}
				}
			)
		
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
	
};




function UpdateTutuptransaksi(mBol) 
{	
	
			Ext.Ajax.request
			 (
				{
					//url: "./Datapool.mvc/CreateDataObj",
					url: baseURL + "index.php/main/functionIGD/ubah_co_status_transksi",
					params: paramUpdateTransaksi(),
					failure: function(o)
					{
						ShowPesanWarningKasirigd('Data gagal tersimpan segera hubungi Admin', 'Gagal');
					 RefreshDataFilterKasirIgdKasir();
					},	
					success: function(o) 
					{
						//RefreshDatahistoribayar(Ext.get('cellSelectedtutup_kasirigd);
					
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoKasirigd(nmPesanSimpanSukses,nmHeaderSimpanData);
							
							//RefreshDataKasirigdKasir();
							if(mBol === false)
							{
									 RefreshDataFilterKasirIgdKasir();
							};
							cellSelectedtutup_kasirigd='';
						}
						else 
						{
								ShowPesanWarningKasirigd('Data gagal tersimpan segera hubungi Admin', 'Gagal');
								cellSelectedtutup_kasirigd='';
						};
					}
				}
			)
		
	
	
};

function ValidasiEntryCMKasirigd(modul,mBolHapus)
{
	var x = 1;
	
	if((Ext.get('txtNoTransaksiKasirigdKasir').getValue() == '') 
	
	|| (Ext.get('txtNoMedrecDetransaksi').getValue() == '') 
	|| (Ext.get('txtNamaPasienDetransaksi').getValue() == '')
	|| (Ext.get('dtpTanggalDetransaksi').getValue() == '') 
	|| dsTRDetailKasirigdKasirList.getCount() === 0 
	|| (Ext.get('cboPembayaran').getValue() == '') 
	||(Ext.get('cboPembayaran').getValue() == 'Pilih Pembayaran...') )
	{
		if (Ext.get('txtNoTransaksiKasirigdKasir').getValue() == '' && mBolHapus === true) 
		{
			x = 0;
		}
		else if (Ext.get('cboPembayaran').getValue() == ''||Ext.get('cboPembayaran').getValue() == 'Pilih Pembayaran...') 
		{
			ShowPesanWarningKasirigd(('Data pembayaran tidak  boleh kosong'), modul);
			x = 0;
		}
		else if (Ext.get('txtNoMedrecDetransaksi').getValue() == '') 
		{
			ShowPesanWarningKasirigd(('Data no. medrec tidak boleh kosong'), modul);
			x = 0;
		}
		else if (Ext.get('txtNamaPasienDetransaksi').getValue() == '') 
		{
			ShowPesanWarningKasirigd('Nama pasien belum terisi', modul);
			x = 0;
		}
		else if (Ext.get('dtpTanggalDetransaksi').getValue() == '') 
		{
			ShowPesanWarningKasirigd(('Data tanggal kunjungan tidak boleh kosong'), modul);
			x = 0;
		}
		//cboPembayaran
	
		else if (dsTRDetailKasirigdKasirList.getCount() === 0) 
		{
			ShowPesanWarningKasirigd('Data dalam tabel kosong',modul);
			x = 0;
		};
	};
	return x;
};




function ShowPesanWarningKasirigd(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorKasirigd(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoKasirigd(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};

function printbillIGD()
{
    Ext.Ajax.request
    (
        {
                url: baseURL + "index.php/main/CreateDataObj",
                params: dataparamcetakbill_vikasirDaftarIGD(),
                success: function(o)
                {
                        var cst = Ext.decode(o.responseText);
                        if (cst.success === true)
                        {

                        }
                        else if  (cst.success === false && cst.pesan===0)
                        {
                                ShowPesanWarning_viDaftar('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
                        }
                        else
                        {
                                ShowPesanError_viDaftar('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
                        }
                }
        }
    );
};

var tmpkdkasirIGD = '1';
function dataparamcetakbill_vikasirDaftarIGD()
{
    var paramscetakbill_vikasirDaftarIGD =
		{      
                    Table: 'DirectPrintingIgd',
                    No_TRans: Ext.get('txtNoTransaksiKasirigdKasir').getValue(),
					Medrec: Ext.get('txtNoMedrecDetransaksi').getValue(),
					kdUnit: rowSelectedKasirigdKasir.data.KD_UNIT,
                    KdKasir : tmpkdkasirIGD,
                    JmlBayar: Ext.get('txtJumlah2EditData_viKasirigd').getValue(),
                    JmlDibayar: Ext.get('txtJumlahEditData_viKasirigd').getValue(),
					printer: Ext.getCmp('cbopasienorder_printer_kasirigd').getValue()
		};
    return paramscetakbill_vikasirDaftarIGD;
};

function printkwitansiIGD()
{
    Ext.Ajax.request
    (
        {
                url: baseURL + "index.php/main/CreateDataObj",
                params: dataparamcetakkwitansi_vikasirDaftarIGD(),
                success: function(o)
                {
                        var cst = Ext.decode(o.responseText);
                        if (cst.success === true)
                        {
                            
                        }
                        else if  (cst.success === false && cst.pesan !== '')
                        {
                                ShowPesanWarningKasirigd('tidak berhasil melakukan pencetakan '  + cst.pesan,'Cetak Data');
                        }
                        else
                        {
                                ShowPesanWarningKasirigd('tidak berhasil melakukan pencetakan '  + cst.pesan,'Cetak Data');
                        }
                }
        }
    );
};

function dataparamcetakkwitansi_vikasirDaftarIGD()
{
    var paramscetakbill_vikasirDaftarIGD =
		{      
                    Table: 'DirectKwitansiIgd',
                    No_TRans: Ext.get('txtNoTransaksiKasirigdKasir').getValue(),
                    JmlBayar: Ext.get('txtJumlah2EditData_viKasirigd').getValue(),
                    JmlDibayar: Ext.get('txtJumlahEditData_viKasirigd').getValue(),
					printer: Ext.getCmp('cbopasienorder_printer_kasirigd').getValue()
		};
    return paramscetakbill_vikasirDaftarIGD;
};
