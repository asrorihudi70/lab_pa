// Data Source
var dataSource_viDaftarIGD;
var datasource_gettempat;
var datagetappto;
var selectCount_viDaftarIGD=50;
var NamaForm_viDaftarIGD="Pendaftaran IGD";
var mod_name_viDaftarIGD="viDaftarIGD";
var cboAsuransiIGD;
var now_viDaftarIGD= new Date;
var Today_PendaftaranIGD = new Date;
var h=now_viDaftarIGD.getHours();
var m=now_viDaftarIGD.getMinutes();
var s=now_viDaftarIGD.getSeconds();
var year=now_viDaftarIGD.getFullYear();
var nowSerahIjazah_viDaftarIGD= new Date();
var urutJenjang_viDaftarIGD;
var ds_PROPINSI_viKasirRwj;
var addNew_viDaftarIGD;
var rowSelected_viDaftarIGD;
var tmp_tempat;
var setLookUps_viDaftarIGD;
var ds_KABUPATEN_viKasirRwj;
var ds_KECAMATAN_viKasirRwj;
var tampung;
var BlnIsDetail;
var SELECTDATASTUDILANJUT;

var tmpnotransaksi;
var tmpkdkasir;

var selectPoliPendaftaranIGD;
var selectKelompokPoli;
var autocomdiagnosa;
var selectPendidikanPendaftaranIGD;
var selectPekerjaanPendaftaranIGD;
var selectPendaftaranIGDStatusMarital;
var selectAgamaPendaftaranIGD;
var mNoKunjungan_viKasir='1';
var selectSetJK;
var selectSetJKPJ;
var selectSetPerseorangan;
var selectSetAsuransi;
var selectSetGolDarah;
var selectSetSatusMarital;
var selectSetKelompokPasien;
var selectSetRujukanDari;
var selectSetNamaRujukan;
var dsAgamaRequestEntry;
var selectSetHubunganKeluarga;
var jeniscus='0';
//var selectAgamaRequestEntry;
var dsPropinsiRequestEntry;
var selectPropinsiRequestEntry;
var dsKecamatanRequestEntry;
//var selectKecamtanRequestEntry;
var dsPendidikanRequestEntry;
var dsPekerjaanRequestEntry;
var dsDokterRequestEntry;
var dsKabupatenRequestEntry;
var selectSetWarga;
var txtedit = "Tambah Data";
var selectAgamaRequestEntry;
var selectPropinsiRequestEntry;
var selectKabupatenRequestEntry;
var selectKecamatanRequestEntry;
var tmpcriteriaIGD = "transaksi.kd_kasir in( '06') and unit.kd_bagian=3";
//var tmpapp = FALSE;
//var tmpkontrol = FALSE;
//var tmpcetakkartu = FALSE;

function daftarRWJgetdatabpjs(){
	if(Ext.getCmp('txtNoAskes').getValue().trim() != '' &&
		Ext.getCmp('cboAsuransiIGD').getValue() == '0000000309'
	){ 
		var e=false;
		if(Ext.getCmp('cboPoliklinikRequestEntry').getValue() == ''){
			e=true;
			Ext.MessageBox.alert('Gagal', 'Poliklinik Harus dipilih.');
		}
		if(autocomdiagnosa.getValue() == ''){
			e=true;
			Ext.MessageBox.alert('Gagal', 'Diagnosa Harus dipilih.');
		}
		if(e==false){
			loadMask.show();
			Ext.Ajax.request({
				method:'POST',
				url: baseURL+"index.php/rawat_jalan/functionRWJ/getDataBpjs",
				params:{
					klinik:Ext.getCmp('cboPoliklinikRequestEntry').getValue()
				},
				success: function(o){
					var cst = Ext.decode(o.responseText);
					if(cst.poli != null){
						$.ajax({
							type: 'GET',
							dataType:'JSON',
							headers: {
								'X-cons-id':cst.id,
								'X-timestamp':cst.timestamp,
								'X-signature':cst.signature
							},
							url: cst.url_briging+Ext.getCmp('txtNoAskes').getValue().trim(),
							success: function(r){
								var p=r.response.peserta;
								Ext.getCmp('txtNama_viDaftarIGDPeserta').setValue(r.response.peserta.nama);
								loadMask.hide();
								Ext.MessageBox.confirm('Delete', 'Nama : '+p.nama+', Kelas : '+p.kelasTanggungan.nmKelas+', ' +
										'Faskes : '+p.provUmum.nmProvider+'. \n Akan Buat SEP Sekarang ?', function(btn){
								   if(btn === 'yes'){
								   	loadMask.show();
								       $.ajax({
											type: 'POST',
											dataType:'JSON',
											headers: {
												'Content-type': 'application/xml',
												'X-cons-id':cst.id,
												'X-timestamp':cst.timestamp,
												'X-signature':cst.signature
											},
											url: cst.url_getSep,
											data:'<request>'+
												'<data>'+
												'<t_sep>'+
												'<noKartu>'+p.noKartu+'</noKartu>'+
												'<tglSep>'+new Date().format('Y-m-d h:i:s')+'</tglSep>'+
												'<tglRujukan>'+new Date().format('Y-m-d h:i:s')+'</tglRujukan>'+
												'<noRujukan>1234590000300003</noRujukan>'+
												'<ppkRujukan>'+p.provUmum.kdProvider+'</ppkRujukan>'+
												'<ppkPelayanan>'+cst.kd_rs+'</ppkPelayanan>'+
												'<jnsPelayanan>2</jnsPelayanan>'+
												'<catatan>dari WS</catatan>'+
												'<diagAwal>'+autocomdiagnosa.getValue()+'</diagAwal>'+
												'<poliTujuan>PAR</poliTujuan>'+
												'<klsRawat>'+p.kelasTanggungan.kdKelas+'</klsRawat>'+
												'<user>1</user>'+
												'<noMr></noMr>'+
												'</t_sep>'+
												'</data>'+
												'</request>',
											success: function(resp1){
												Ext.getCmp('txtNoSJP').setValue(resp1.response);
												loadMask.hide();
											},
											error: function(jqXHR, exception) {
												Ext.MessageBox.alert('Gagal', 'Nomor SEP tidak ditemukan.');
												Ext.getCmp('txtNoSJP').setValue('Tidak Ditemukan');
												Ext.getCmp('txtNama_viDaftarIGDPeserta').setValue('Tidak Ditemukan');
												loadMask.hide();
											}
										});
									}
								});
							},
							error: function(jqXHR, exception) {
								Ext.MessageBox.alert('Gagal', 'Nomor Asuransi tidak ditemukan.');
								Ext.getCmp('txtNoSJP').setValue('');
								Ext.getCmp('txtNama_viDaftarIGDPeserta').setValue('');
								loadMask.hide();
							}
						});
					}else{
						loadMask.hide();
						Ext.MessageBox.alert('Gagal', 'Poliklinik '+Ext.getCmp('cboPoliklinikRequestEntry').getRawValue()+' tidak terdaftar didata BPJS, Harap Hubungi Admin.');
					}
					
				}
			});
		}
	}
}
var CurrentData_viDaftarIGD =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viDaftarIGD(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

//datarefresh_viDaftarIGD();

function dataGrid_viDaftarIGD(mod_id)
{
       var FieldMaster_viDaftarIGD = 
	[
                    'KD_PASIEN', 'NAMA', 'NAMA_KELUARGA', 'JENIS_KELAMIN', 'TEMPAT_LAHIR', 'TGL_LAHIR',
					'TELEPON','HANDPHONE','EMAIL','NAMA_AYAH','NAMA_IBU','AGAMA',
                    'GOL_DARAH', 'WNI', 'STATUS_MARITA', 'ALAMAT', 'KD_KELURAHAN','KELURAHAN','KD_POS','ALAMAT_KTP','KD_POS_KTP','KD_KELURAHAN_KTP', 
					'PENDIDIKAN', 'PEKERJAAN', 'NAMA_UNIT','TGL_MASUK', 'URUT_MASUK','KABUPATEN','KECAMATAN','PROPINSI',
                    'KD_KABUPATEN','KD_KECAMATAN','KD_PROPINSI','KD_PENDIDIKAN','KD_PEKERJAAN','KD_AGAMA',
					'PROPINSIKTP','KABUPATENKTP','KECAMATANKTP','KELURAHANKTP',	'KEL_KTP','KEC_KTP',
					'KAB_KTP','PRO_KTP'
					
	];

    dataSource_viDaftarIGD = new WebApp.DataStore({fields: FieldMaster_viDaftarIGD});
    
	datarefresh_viDaftarIGD(tmpcriteriaIGD);
            
	var grData_viDaftarIGD = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viDaftarIGD,
			autoScroll: false,
			columnLines: true,
			border:false,
			trackMouseOver: true,
			
			anchor: '100% 79%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viDaftarIGD = undefined;
							rowSelected_viDaftarIGD = dataSource_viDaftarIGD.getAt(row);
							CurrentData_viDaftarIGD;
							CurrentData_viDaftarIGD.row = row;
							CurrentData_viDaftarIGD.data = rowSelected_viDaftarIGD.data;
                                                        Ext.getCmp('btntambah_viDaftarIGD').hide()
                                                        Ext.getCmp('btnEdit_viDaftarIGD').show()
                                                }
                                        }
				}
			),
			listeners:
			{
				rowdblclick: function (sm, ridx, cidx)
				{
                                        
                                        rowSelected_viDaftarIGD = dataSource_viDaftarIGD.getAt(ridx);
					if (rowSelected_viDaftarIGD !== undefined)
					{
						setLookUp_viDaftarIGD(rowSelected_viDaftarIGD.data);
                                                
					}
					else
					{
						setLookUp_viDaftarIGD();
					}
				},
                                containerclick : function(){
                                                Ext.getCmp('btntambah_viDaftarIGD').show()
                                                Ext.getCmp('btnEdit_viDaftarIGD').hide()
                                                }
                                },
                                

			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colNRM_viDaftarIGD',
						header: 'No.Medrec',
						dataIndex: 'KD_PASIEN',
						sortable: true,
						width: 35
//						filter:
//						{
//							type: 'int'
//						}
					},
					{
						id: 'colNMPASIEN_viDaftarIGD',
						header: 'Nama',
						dataIndex: 'NAMA',
						sortable: true,
						width: 35
//						filter:
//						{}
					},
					{
						id: 'colALAMAT_viDaftarIGD',
						header:'Alamat',
						dataIndex: 'ALAMAT',
						width: 60,
						sortable: true
//						filter: {}
					},
					{
						id: 'colTglKunj_viDaftarIGD',
						header:'Tgl Lahir',
						dataIndex: 'TGL_LAHIR',
						width: 50,
						sortable: true,
						format: 'd/M/Y',
//						filter: {},
						renderer: function(v, params, record)
						{

                                                       return ShowDate(record.data.TGL_LAHIR);
						}
					},
					{
						id: 'colPoliTujuan_viDaftarIGD',
						header:'Poli Tujuan',
						dataIndex: 'NAMA_UNIT',
						width: 50,
						sortable: true
//						filter: {}
					}
				]
				),
			//tbar1:
                        tbar:
                            {
				xtype: 'toolbar',
				id: 'toolbar_viDaftarIGD',
				items:
                                    [
                                            {
						xtype: 'button',
						text: 'Kunjungan',
                                                hidden : true,
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viDaftarIGD',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viDaftarIGD != undefined)
							{
                                                            setLookUp_viDaftarIGD(rowSelected_viDaftarIGD.data);
                                                            
							}
							else
							{
								setLookUp_viDaftarIGD();
                                                                if (Ext.get('txtTglLahir_vidaftarIGD').getValue() != "")
                                                                {
                                                                    Ext.get('txtTglLahir_vidaftarIGD').getValue(Ext.get('txtTglLahir_vidaftarIGD').getValue());
                                                                }
                                                                else
                                                                    {
                                                                        Ext.get('txtTglLahir_vidaftarIGD').getValue('');
                                                                    }
                                                            if (Ext.get('txtAlamat_viDaftarIGDPasien_vidaftarIGD').getValue() != "")
                                                                {
                                                                    Ext.getCmp('txtAlamat_viDaftarIGD').setValue(Ext.get('txtAlamat_viDaftarIGDPasien_vidaftarIGD').getValue());
                                                                }
                                                                else
                                                                    {
                                                                        Ext.getCmp('txtAlamat_viDaftarIGD').setValue('');
                                                                    }
                                                            if (Ext.get('txtNama_viDaftarIGDPasien_vidaftarIGD').getValue() != "")
                                                                {
                                                                    Ext.getCmp('txtNama_viDaftarIGD').setValue(Ext.get('txtNama_viDaftarIGDPasien_vidaftarIGD').getValue());
                                                                }
                                                                else
                                                                    {
                                                                        Ext.getCmp('txtNama_viDaftarIGD').setValue('');
                                                                    }
							}
						}
					},
                                        {
						xtype: 'button',
						text: 'Pasien Baru',
						iconCls: 'Edit_Tr',
                                                hidden : false,
						tooltip: 'Edit Data',
						id: 'btntambah_viDaftarIGD',
						handler: function(sm, row, rec)
						{
                                                        setLookUp_viDaftarIGD();
                                                            if (Ext.get('txtTglLahir_vidaftarIGD').getValue() != "")
                                                                {
                                                                    var tmpTanggalLahir = ShowDate(Ext.get('txtTglLahir_vidaftarIGD').getValue())
                                                                    Ext.get('txtTglLahir_vidaftarIGD').getValue(tmpTanggalLahir);
                                                                }
                                                                else
                                                                    {
                                                                        Ext.get('txtTglLahir_vidaftarIGD').getValue('');
                                                                    }
                                                            if (Ext.get('txtAlamat_viDaftarIGDPasien_vidaftarIGD').getValue() != "")
                                                                {
                                                                    Ext.getCmp('txtAlamat_viDaftarIGD').setValue(Ext.get('txtAlamat_viDaftarIGDPasien_vidaftarIGD').getValue());
                                                                }
                                                                else
                                                                    {
                                                                        Ext.getCmp('txtAlamat_viDaftarIGD').setValue('');
                                                                    }
                                                            if (Ext.get('txtNama_viDaftarIGDPasien_vidaftarIGD').getValue() != "")
                                                                {
                                                                    Ext.getCmp('txtNama_viDaftarIGD').setValue(Ext.get('txtNama_viDaftarIGDPasien_vidaftarIGD').getValue());
                                                                }
                                                                else
                                                                    {
                                                                        Ext.getCmp('txtNama_viDaftarIGD').setValue('');
                                                                    }
						}
					},' ', '-',
                                        {
                                                xtype: 'checkbox',
                                                id: 'chkWithTglRequest',
                                                 text: 'Select Bacon',
                                                hideLabel:false,
                                                checked: false,

                                                handler: function()
                                                  {
                                                    if (this.getValue()===true)
                                                    {
                                                        criteria = "tgl_masuk = " + "'" + now_viDaftarIGD.format('Y/M/d') + "'" ;
                                                        //alert(criteria);
														datarefresh_viDaftarIGD(criteria);
                                                    }
                                                    else
                                                    {
                                                        criteria = tmpcriteriaIGD;
                                                        datarefresh_viDaftarIGD(criteria);
                                                    }
                                                  }
                                            },
                                            {xtype: 'tbtext', text: 'Data Pasien Hari Ini ', cls: 'left-label', width: 90},
                                    ]},

			bbar : bbar_paging(mod_name_viDaftarIGD, selectCount_viDaftarIGD, dataSource_viDaftarIGD),
			viewConfig: 
			{
				forceFit: true
			}
		}
    )
     var top = new Ext.FormPanel({
        labelAlign: 'top',
        frame:true,
        title: '',
        bodyStyle:'padding:5px 5px 0',
        //width: 600,
        items: [{
            layout:'column',
            items:[{
                columnWidth: .3,
                layout: 'form',
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'No. Medrec (Enter Untuk Mencari)',
                        name: 'txtNoMedrecD_vidaftarIGD',
                        id: 'txtNoMedrecD_vidaftarIGD',
                        //width: 50,
                        //emptyText:'Tekan Enter Untuk Mencari...',
                        enableKeyEvents: true,
                        anchor: '95%',
                        listeners:
                        {
                            'specialkey' : function()
                            {
                                var tmpNoMedrec = Ext.get('txtNoMedrecD_vidaftarIGD').getValue()
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10 )
                                {
                                    if(tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10 )
                                        {
                                             var tmpgetNoMedrec = formatnomedrec(Ext.get('txtNoMedrecD_vidaftarIGD').getValue())
                                             Ext.getCmp('txtNoMedrecD_vidaftarIGD').setValue(tmpgetNoMedrec);
                                             var tmpkriteria = getCriteriaFilter_viDaftarIGD();
                                             datarefresh_viDaftarIGD(tmpkriteria);
                                        }
                                        else
                                            {
                                                if (tmpNoMedrec.length === 10)
                                                    {
                                                        tmpkriteria = getCriteriaFilter_viDaftarIGD();
                                                        datarefresh_viDaftarIGD(tmpkriteria);
                                                    }
                                                    else
                                                    Ext.getCmp('txtNoMedrec_vidaftarIGD').setValue('')
                                            }
                                }
                            }

                        }

                    },
					{
                        xtype: 'textfield',
                        fieldLabel: 'Tanggal Lahir (ddMMYYYY) ',
                        name: 'txtTglLahir_vidaftarIGD',
                        id: 'txtTglLahir_vidaftarIGD',
                        enableKeyEvents: true,
                        anchor: '95%',
                        listeners:
                        {
                            'specialkey' : function()
                            {
                                var tmptanggal = Ext.get('txtTglLahir_vidaftarIGD').getValue()
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                {
                                       if (tmptanggal.length !== 0)
                                        {
                                            if(tmptanggal.length === 8)
                                                {
                                                    var tmptgl = ShowDateUbah(Ext.get('txtTglLahir_vidaftarIGD').getValue())
                                                    Ext.getCmp('txtTglLahir_vidaftarIGD').setValue(tmptgl);
                                                }
                                                else
                                                    {
                                                         if(tmptanggal.length === 10)
                                                            {
                                                                 var tmpkriteria = getCriteriaFilter_viDaftarIGD();
                                                                 datarefresh_viDaftarIGD(tmpkriteria);
                                                            }
                                                            else
                                                                {
                                                                    alert("Format yang Anda masukan salah...");
                                                                }
                                                     }
                                        }
                                            tmpkriteria = getCriteriaFilter_viDaftarIGD();
                                            datarefresh_viDaftarIGD(tmpkriteria);
                                }
                            }

                        }

                    }
             ]
            },{
                columnWidth:.5,
                layout: 'form',
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Nama Pasien ',
                        name: 'txtNama_viDaftarIGDPasien_vidaftarIGD',
                        id: 'txtNama_viDaftarIGDPasien_vidaftarIGD',
                        enableKeyEvents: true,
                        anchor: '95%',
                        listeners:
                        {     
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13)
									{
											   var tmpkriteria = getCriteriaFilter_viDaftarIGD();
											   datarefresh_viDaftarIGD(tmpkriteria);
									}
								}
						}
						},
                    {
                    xtype: 'textfield',
                    fieldLabel: 'Alamat Pasien ',
                    name: 'txtAlamat_viDaftarIGDPasien_vidaftarIGD',
                    enableKeyEvents: true,
                    id: 'txtAlamat_viDaftarIGDPasien_vidaftarIGD',
                    anchor: '95%',
                    listeners:
                        {
						
						     
							'specialkey' : function()
							{
							if (Ext.EventObject.getKey() === 13)
							{
										var tmpkriteria = getCriteriaFilter_viDaftarIGD();
                                        datarefresh_viDaftarIGD(tmpkriteria);
							}
							}
                           
                        }
                }
             ]
            }]
        }]       
    });

    //top.render(document.body);

    var FrmTabs_viDaftarIGD = new Ext.Panel
    (
		{
			id: mod_id,
			region: 'center',
			layout: 'form',
			title: NamaForm_viDaftarIGD,          
			border: false,  
			closable: true,
			autoScroll: false,
			iconCls: 'Studi_Lanjut',
			margins: '5 5 5 5',
                        items: [ top , grData_viDaftarIGD],

			listeners: 
			{ 
				'afterrender': function() 
				{           
					
				}
			}
		}
    )
    return FrmTabs_viDaftarIGD;
}

function setLookUp_viDaftarIGD(rowdata)
{
    var lebar = 1000;
    setLookUps_viDaftarIGD = new Ext.Window
    (
    {
        id: 'SetLookUps_viDaftarIGD',
        name: 'SetLookUps_viDaftarIGD',
        title: NamaForm_viDaftarIGD, 
        closeAction: 'destroy',        
        width: lebar,
        height: 575,//575,
        resizable:false,
	autoScroll: false,

        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viDaftarIGD(lebar,rowdata), //1
        listeners:
        {
            activate: function()
            {
                Ext.getCmp('cboSukuRequestEntry').hide(); //suku di hide
                Ext.getCmp('txtJamKunjung').hide();
                Ext.getCmp('dptTanggal').hide();
                Ext.getCmp('txtNama_viDaftarIGDPeserta').hide();
                Ext.getCmp('txtNoAskes').hide();
                Ext.getCmp('txtNoSJP').hide();
                Ext.getCmp('cboPerseorangan').hide();
               // Ext.getCmp('cboAsuransiIGD').hide();
				  Ext.getCmp('cboAsuransiIGD').show(); // show combo
                Ext.getCmp('cboPerusahaanRequestEntry').hide();
                if(rowdata !== undefined)
                {/* 
                    Ext.getCmp('txtNama_viDaftarIGD').disable();
                    Ext.getCmp('txtNoRequest_viDaftarIGD').disable();
                    Ext.getCmp('txtNama_viDaftarIGD').disable();
                    Ext.getCmp('txtNama_viDaftarIGDKeluarga').disable();
                    //Ext.getCmp('txtTempatLahir_viDaftarIGD').disable();
                    Ext.getCmp('cboPendidikanRequestEntry').disable();
                    Ext.getCmp('cboPekerjaanRequestEntry').disable();
                    Ext.getCmp('cbWna').disable();
                    Ext.getCmp('txtAlamat_viDaftarIGD').disable();
                    Ext.getCmp('cboAgamaRequestEntry').disable();
                    Ext.getCmp('cboGolDarah').disable();
                    Ext.getCmp('dtpTanggalLahir_viDaftarIGD').disable();
                    Ext.getCmp('cboStatusMarital').disable();
                    Ext.getCmp('cboJK').disable();
                    Ext.getCmp('cbWna').disable();
                    Ext.getCmp('cboPropinsiRequestEntry').disable();
                    Ext.getCmp('cboKabupatenRequestEntry').disable();
                    Ext.getCmp('cboKecamatanRequestEntry').disable();
					Ext.getCmp('txtThnLahir_viDaftarIGD').disable();
					Ext.getCmp('txtBlnLahir_viDaftarIGD').disable();
					Ext.getCmp('txtHariLahir_viDaftarIGD').disable();
					Ext.getCmp('txtpos').disable();
					Ext.getCmp('txtAlamatktp').disable();
					Ext.getCmp('cboPropinsiKtp').disable();
					Ext.getCmp('cboKtpKabupaten').disable();
					Ext.getCmp('cboKecamatanKtp').disable();
					Ext.getCmp('cbokelurahanKtp').disable();
					Ext.getCmp('txtposktp').disable(); */
                }
            },
            afterShow: function()
            {
                this.activate();
            },
            deactivate: function()
            {
                rowSelected_viDaftarIGD=undefined;
            // datarefresh_viDaftarIGD();
		        mNoKunjungan_viKasir = '';
                Ext.getCmp('txtNoMedrecD_vidaftarIGD').setValue('');
                Ext.getCmp('txtTglLahir_vidaftarIGD').setValue('');
                Ext.getCmp('txtAlamat_viDaftarIGDPasien_vidaftarIGD').setValue('');
                Ext.getCmp('txtNama_viDaftarIGDPasien_vidaftarIGD').setValue('');
            }
        }
    }
    );

    setLookUps_viDaftarIGD.show();
    if (rowdata == undefined)
    {
        dataaddnew_viDaftarIGD();
    }
    else
    {
        datainit_viDaftarIGD(rowdata);
		
    }
}

// From view popup data daftar pasien / kunjungan
function getFormItemEntry_viDaftarIGD(lebar,rowdata)
{
    var pnlFormDataBasic_viDaftarIGD = new Ext.FormPanel
    (
		{
			title: '',
			region: 'north',//'center',
			layout: 'form',//'anchor',
			bodyStyle: 'padding:10px 10px 10px 10px',
			anchor: '100%',
                        autoScroll: true,
			width: lebar,//lebar-55,
                        height: 900,
			border: false,

                        items:[
                            getItemPanelInputBiodata_viDaftarIGD(),
                            getPenelItemDataKunjungan_viDaftarIGD(lebar)
                                ],
			fileUpload: true,
			tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						id: 'btnAdd_viDaftarIGD',
						handler: function(){
							dataaddnew_viDaftarIGD();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viDaftarIGD',
						handler: function()
						{
							datasave_viDaftarIGD(addNew_viDaftarIGD);
							datarefresh_viDaftarIGD(tmpcriteriaIGD);
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						id: 'btnSimpanExit_viDaftarIGD',
						handler: function()
						{
							var x = datasave_viDaftarIGD(addNew_viDaftarIGD);
							datarefresh_viDaftarIGD(tmpcriteriaIGD);
							if (x===undefined)
							{
								setLookUps_viDaftarIGD.close();
							}
						}
					},
                                        {
						xtype: 'tbseparator'
					},
					{
						xtype:'button',
						text:'Lookup Pasien',
						iconCls:'find',
						id:'btnLookUp_viDaftarIGD',
						handler:function(){
							FormLookupPasien("","","","","","");
						}
					},					
					{
						xtype:'tbseparator'
					},
					{
						xtype:'splitbutton',
						text:'Print Kartu Pasien',
						iconCls:'print',
						id:'btnPrint_viDaftarIGD',
						handler:function()
						{							
                                                    GetPrintKartu();								
						},
                                                //handler: optionsHandler, // handle a click on the button itself
                                                menu: new Ext.menu.Menu({
                                                    items: [
                                                        // these items will render as dropdown menu items when the arrow is clicked:
                                                        {
                                                            xtype: 'button',
                                                            text: 'Print Bill',
//                                                            iconCls: 'remove',
                                                            id: 'btnPrintBill',
                                                            handler: function()
                                                            {
                                                                printbill()
//                                                                    datadelete_viDaftarIGD();
//                                                                    datarefresh_viDaftarIGD();

                                                            }
                                                        },
                                                        {
                                                            xtype: 'button',
                                                            text: 'Print ',
//                                                            iconCls: 'remove',
                                                            id: 'btnPrint',
                                                            handler: function()
                                                            {
                                                                
                                                                 var criteria = 'kd_pasien = '+ Ext.get('txtNoRequest_viDaftarIGD').getValue() +' And kd_unit = '+ Ext.getCmp('cboPoliklinikRequestEntry').getValue();
                                                                 ShowReport('0', 1010204, criteria);

                                                            }
                                                        },
                                                    ]
                                                })
					},
					{
						xtype:'tbseparator'
					},
					{
						xtype:'button',
						text:'Print Formulir',
						iconCls:'print',
						id:'btnPrintFormulirIGD',
						handler:function(){
							
						}
					},	
                                        
				]
			}
//                        ,items:
			
		}
    )
    

    return pnlFormDataBasic_viDaftarIGD;
}
//-----------------------------end----------------------------------------------

//form view data biodata pasien
function getItemPanelInputBiodata_viDaftarIGD() 
{
    var items =
	{
	    layout: 'column',
	    border: false,
            labelAlign: 'top',
	    items:
		[
                        {
			    columnWidth: .20,
			    layout: 'form',
                            labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: '',
					    name: 'txtTmpKecamatan_viDaftarIGD',
					    id: 'txtTmpKecamatan_viDaftarIGD',
						emptyText:'',
						hidden : true,
					    anchor: '95%'
					},
                                        {
					    xtype: 'textfield',
					    fieldLabel: '',
					    name: 'txtTmpKabupaten_viDaftarIGD',
					    id: 'txtTmpKabupaten_viDaftarIGD',
						emptyText:'',
						hidden : true,
					    anchor: '95%'
					},
					{
					    xtype: 'textfield',
					    fieldLabel: '',
					    name: 'txtTmpPropinsi_viDaftarIGD',
					    id: 'txtTmpPropinsi_viDaftarIGD',
						emptyText:'',
						hidden : true,
					    anchor: '95%'
					},
					{
					    xtype: 'textfield',
					    fieldLabel: '',
					    name: 'txtTmpKelurahan_viDaftarIGD',
					    id: 'txtTmpKelurahan_viDaftarIGD',
						emptyText:'',
						hidden : true,
					    anchor: '95%'
					},
					{
					    xtype: 'textfield',
					    fieldLabel: '',
					    name: 'txtTmpPendidikan_viDaftarIGD',
					    id: 'txtTmpPendidikan_viDaftarIGD',
						emptyText:'',
						hidden : true,
					    anchor: '95%'
					},
                                        {
					    xtype: 'textfield',
					    fieldLabel: '',
					    name: 'txtTmpPekerjaan_viDaftarIGD',
					    id: 'txtTmpPekerjaan_viDaftarIGD',
						emptyText:'',
						hidden : true,
					    anchor: '95%'
					},
                                        {
					    xtype: 'textfield',
					    fieldLabel: '',
					    name: 'txtTmpAgama_viDaftarIGD',
					    id: 'txtTmpAgama_viDaftarIGD',
						emptyText:'',
						hidden : true,
					    anchor: '95%'
					},
					{
					    xtype: 'textfield',
					    fieldLabel: '',
					    name: 'txtTmpKelurahanKtp_viDaftarIGD',
					    id: 'txtTmpKelurahanKtp_viDaftarIGD',
						emptyText:'',
						hidden : true,
					    anchor: '95%'
					}
				]
			},
			{
			    columnWidth: .10,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: 'No. Medrec ',
					    name: 'txtNoRequest_viDaftarIGD',
					    id: 'txtNoRequest_viDaftarIGD',
						emptyText:'Automatic from the system ...',
						readOnly:true,
					    anchor: '99%'
					}
				]
			},
			{
			    columnWidth: .25,
			    layout: 'form',
			    border: false,
				labelWidth:90,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: 'Nama ',
					    name: 'txtNama_viDaftarIGD',
					    id: 'txtNama_viDaftarIGD',
						tabIndex:1,
						emptyText:'',
						disabled:false,
					    anchor: '99%',
                        //regex:/^((([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z\s?]{2,5}){1,25})*(\s*?;\s*?)*)*$/,
						enableKeyEvents: true,
						listeners: {
							'render': function(c) {
								c.getEl().on('keypress', function(e) {
									if(e.getKey() == 13) //atau Ext.EventObject.ENTER
									Ext.getCmp('txtNama_viDaftarIGDKeluarga').focus();
								}, c);
							}
						}
					}
				]
			},
                        {
			    columnWidth: .17,
			    layout: 'form',
			    border: false,
				labelWidth:90,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: 'Nama Keluarga ',
					    name: 'txtNama_viDaftarIGDKeluarga',
					    id: 'txtNama_viDaftarIGDKeluarga',
						tabIndex:2,
                        emptyText:'',
					    anchor: '99%',
						listeners: {
							'render': function(c) {
								c.getEl().on('keypress', function(e) {
									if(e.getKey() == 13) //atau Ext.EventObject.ENTER
									Ext.getCmp('cboAgamaRequestEntry').focus();
								}, c);
							}
						}
					}
				]
			},
			{
			    columnWidth: .12,
			    layout: 'form',
			    border: false,
				labelWidth:90,
				anchor: '95%',
			    items:
				[
					mComboAgamaRequestEntry(),
				]
			},
			{
			    columnWidth: .060,
			    layout: 'form',
			    border: false,
				labelWidth:0,           
			    items:
				[
					mComboGolDarah()
				]
			},
			{
			    columnWidth: .090,
			    layout: 'form',
			    border: false,
				labelWidth:90,
			    items:
				[
					mComboJK()
				]
			},
			{
			    columnWidth: .10,
			    layout: 'form',
			    border: false,
				labelWidth:90,
				anchor: '95%',
			    items:
				[
					mComboSatusMarital()
				]
			},
			{
			    columnWidth: .050,
			    layout: 'form',
			    border: false,
				labelWidth:90,
				anchor: '95%',
			    items:
				[
					{
						xtype     :'checkbox',
						boxLabel  : 'WNA',
						name      : 'cbWna',
						id        : 'cbWna',
						tabIndex  : 7,
					}
				]
			},

			{
			    columnWidth: .15,
			    layout: 'form',
			    border: false,
				labelWidth:90,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: 'Tempat Lahir ',
					    name: 'txtTempatLahir_viDaftarIGD',
					    id: 'txtTempatLahir_viDaftarIGD',
						emptyText:'',
						tabIndex:8,
					    anchor: '99%',
						listeners: {
							'render': function(c) {
								c.getEl().on('keypress', function(e) {
									if(e.getKey() == 13) //atau Ext.EventObject.ENTER
									Ext.getCmp('dtpTanggalLahir_viDaftarIGD').focus();
								}, c);
							}
						}
					}
				]
			},
			{
			    columnWidth: .10,
			    layout: 'form',
			    border: false,
				labelWidth:90,
			    items:
				[
					{
					    xtype: 'datefield',
					    fieldLabel: 'Tanggal Lahir ',
					    name: 'dtpTanggalLahir_viDaftarIGD',
					    id: 'dtpTanggalLahir_viDaftarIGD',
						//format: 'M/d/Y',
						format : "d/M/Y",
						value: Today_PendaftaranIGD,
					    anchor: '95%',
						tabIndex:9,
						listeners:
						{
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 9 || Ext.EventObject.getKey() === 13) // 9 Kode Kode Char Untuk Tab
								{
									var year1=Ext.get('dtpTanggalLahir_viDaftarIGD').getValue();
									var fullyearnow= now_viDaftarIGD.format('d/M/Y');
									var tmp = fullyearnow.substring(7, 11);
									var tmp2 = year1.substring(7, 11);
									var Umur = tmp - tmp2
									// alert(year1)
									// alert(fullyearnow)
									if(tmp2 < tmp)
									{
										Ext.getCmp('txtThnLahir_viDaftarIGD').setValue(Umur);
										Ext.getCmp('txtBlnLahir_viDaftarIGD').setValue('0');
										Ext.getCmp('txtHariLahir_viDaftarIGD').setValue('0');
									}
									else if(tmp2 == tmp)
										{
											var tmpblnlahir = ShowNewDate(year1)
											var tmpblnsekarang = ShowNewDate(fullyearnow)
											//var bulan =  tmpblnsekarang - tmpblnlahir
											Ext.getCmp('txtThnLahir_viDaftarIGD').setValue('0');
											Ext.getCmp('txtBlnLahir_viDaftarIGD').setValue('0');
											Ext.getCmp('txtHariLahir_viDaftarIGD').setValue('0');
											//alert(tmpblnlahir)
											//alert(tmpblnsekarang)
											//var umurku = datediff(year1,fullyearnow,'Y')
											// alert(umurku)
										}
									else
										{
											Ext.getCmp('txtThnLahir_viDaftarIGD').setValue('0');
											Ext.getCmp('txtBlnLahir_viDaftarIGD').setValue('0');
											Ext.getCmp('txtHariLahir_viDaftarIGD').setValue('0');
										}
																	
									
								}
							},
						   'render': function(c) {
								c.getEl().on('keypress', function(e) {
								if(e.getKey() == 13) //atau Ext.EventObject.ENTER
								Ext.getCmp('txtNamaAyah').focus();
								}, c);

							}
						}	

					}
				]
					
                                
			},
			{
			    columnWidth: .039,
			    layout: 'form',
			    border: false,
				labelWidth:10,
				emptyText:' ',
				anchor: '99%',

			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: 'Thn ',
					    name: 'txtThnLahir_viDaftarIGD',
					    id: 'txtThnLahir_viDaftarIGD',
						emptyText:'',
						readOnly:true,
					    anchor: '95%'
					}
				]
			},
			{
			    columnWidth: .037,
			    layout: 'form',
			    border: false,
				labelWidth:10,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: 'Bln ',
					    name: 'txtBlnLahir_viDaftarIGD',
					    id: 'txtBlnLahir_viDaftarIGD',
						readOnly:true,
						emptyText:'',
					    anchor: '95%'
					}
				]
			},
			{
			    columnWidth: .039,
			    layout: 'form',
			    border: false,
				labelWidth:10,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: 'Hari ',
					    name: 'txtHariLahir_viDaftarIGD',
					    id: 'txtHariLahir_viDaftarIGD',
						emptyText:'',
						readOnly:true,
					    anchor: '95%'
					}
				]
			},
                        
                        
			/* {
			    columnWidth: .15,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
                            anchor: '95%',
			    items:
				[
				  mComboWargaNegara()
				]
			}, */
			        {
			    columnWidth: .10,
			    layout: 'form',
			    border: false,
				labelWidth:90,
			    items:
				[
					{
						xtype: 'textfield',
						fieldLabel: 'Nama Ayah ',
						name: 'txtNamaAyah',
						id: 'txtNamaAyah',
						emptyText:'',
						tabIndex:10,
						anchor: '99%',
						listeners: {
							'render': function(c) {
								c.getEl().on('keypress', function(e) {
									if(e.getKey() == 13) //atau Ext.EventObject.ENTER
									Ext.getCmp('txtNamaIbu').focus();
								}, c);
							}
						}
						
					}
				]
			},
			{
			    columnWidth: .10,
			    layout: 'form',
			    border: false,
				labelWidth:90,
			    items:
				[
					{
						xtype: 'textfield',
						fieldLabel: 'Nama Ibu ',
						name: 'txtNamaIbu',
						id: 'txtNamaIbu',
						emptyText:'',
						tabIndex:11,
						anchor: '99%',
						listeners: {
							'render': function(c) {
								c.getEl().on('keypress', function(e) {
									if(e.getKey() == 13) //atau Ext.EventObject.ENTER
									Ext.getCmp('cboPendidikanRequestEntry').focus();
								}, c);
							}
						}
					}
				]
			},
			{
			    columnWidth: .15,
			    layout: 'form',
			    border: false,
				labelWidth:90,
			    items:
				[
					mComboPendidikanRequestEntry()
				]
			},
			{
			    columnWidth: .17,
			    layout: 'form',
			    border: false,
				labelWidth:90,
			    items:
				[
					mComboPekerjaanRequestEntry()
				]
			},       
                        
			{
			    columnWidth: .25,
			    layout: 'form',
			    border: false,
				labelWidth:90,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: 'Alamat ',
					    name: 'txtAlamat_viDaftarIGD',
					    id: 'txtAlamat_viDaftarIGD',
						emptyText:'',
						tabIndex:14,
					    anchor: '99%',
						listeners: {
								'render': function(c) {
									c.getEl().on('keypress', function(e) {
										if(e.getKey() == 13) //atau Ext.EventObject.ENTER
										Ext.getCmp('cboPropinsiRequestEntry').focus();
									}, c);
							}
						}
					}
				]
			},
			{
			    columnWidth: .15,
			    layout: 'form',
			    border: false,
				labelWidth:90,
			    items:
				[
					mComboPropinsiRequestEntry()
				]
			},
			{
			    columnWidth: .15,
			    layout: 'form',
			    border: false,
				labelWidth:90,
			    items:
				[
					mComboKabupatenRequestEntry()
				]
			},
                        {
			    columnWidth: .15,
			    layout: 'form',
			    border: false,
				labelWidth:90,
			    items:
				[
					mComboKecamatanRequestEntry()
				]
			},
            {
			    columnWidth: .13,
			    layout: 'form',
			    border: false,
				labelWidth:90,
			    items:
				[
					mComboSukuRequestEntry()
				]
			},
			{
			    columnWidth: .13,
			    layout: 'form',
			    border: false,
				labelWidth:90,
			    items:
				[
					mCombokelurahan()
				]
			},
			{
				columnWidth: .060,
				layout: 'form',
				border: false,
				labelWidth:90,
				items:
				[
					{
						xtype: 'textfield',
						fieldLabel: 'Kd. Pos ',
						name: 'txtpos',
						id: 'txtpos',
						tabIndex:20,
						emptyText:' ',
						anchor: '99%',
						listeners: {
								'render': function(c) {
									c.getEl().on('keypress', function(e) {
										if(e.getKey() == 13) //atau Ext.EventObject.ENTER
										Ext.getCmp('txtAlamatktp').focus();
									}, c);
								}
						}
					}
				] 
			}, 
			{
				columnWidth: .1,
				layout: 'form',
				border: false,
				labelWidth:90,
				anchor: '95%',
				items:
				[
					{
						xtype     :'checkbox',
						boxLabel  : 'Copy Alamat ',
						name      : 'cbalamatsama',
						id        : 'cbalamatsama',
						tabIndex  :28,
						handler: function()
						{
							if (this.getValue()===true)
							{
								Ext.getCmp('cboPropinsiKtp').setValue(Ext.get('cboPropinsiRequestEntry').getValue());
								Ext.getCmp('cboKtpKabupaten').setValue(Ext.get('cboKabupatenRequestEntry').getValue());
								Ext.getCmp('txtAlamatktp').setValue(Ext.get('txtAlamat_viDaftarIGD').getValue());
								Ext.getCmp('cboKecamatanKtp').setValue(Ext.get('cboKecamatanRequestEntry').getValue());
								Ext.getCmp('txtposktp').setValue(Ext.get('txtpos').getValue());
								Ext.getCmp('cbokelurahanKtp').setValue(Ext.get('cbokelurahan').getValue());
								Ext.getCmp('txtTmpKelurahanKtp_viDaftarIGD').setValue(Ext.getCmp('txtTmpKelurahan_viDaftarIGD').getValue());
								//alert(Ext.getCmp('txtTmpKelurahan_viDaftarIGD').getValue());
								selectkelurahanktp=kelurahanpasien;
								selectPropinsiKtp= Ext.getCmp('txtTmpPropinsi').getValue();
								selectKecamatanktp=Ext.getCmp('txtTmpKecamatan').getValue();
							}
							else
							{
								Ext.getCmp('cboPropinsiKtp').setValue();
								Ext.getCmp('cboKtpKabupaten').setValue();
								Ext.getCmp('txtAlamatktp').setValue();
								Ext.getCmp('cboKecamatanKtp').setValue();
								selectkelurahanktp="";
								selectPropinsiKtp="";
								selectKecamatanktp="";
							}
						}

					}
				]
			},			
		    {
			    columnWidth: .25,
			    layout: 'form',
			    border: false,
				labelWidth:90,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: 'Alamat Ktp ',
					    name: 'txtAlamatktp',
					    id: 'txtAlamatktp',
						tabIndex:21,
						emptyText:' ',
					    anchor: '99%',
						listeners: {
								'render': function(c) {
									c.getEl().on('keypress', function(e) {
										if(e.getKey() == 13) //atau Ext.EventObject.ENTER
										Ext.getCmp('cboPropinsiKtp').focus();
									}, c);
								}
						}
					}
				]
			},    
			{
			    columnWidth: .15,
			    layout: 'form',
			    border: false,
				labelWidth:90,
			    items:
				[
					mComboPropinsiKtp()
				]
			},
		    {
			    columnWidth: .15,
			    layout: 'form',
			    border: false,
				labelWidth:90,
			    items:
				[
					mComboKtpKabupaten()
				]
			},
			{
			    columnWidth: .15,
			    layout: 'form',
			    border: false,
				labelWidth:90,
			    items:
				[
					mComboKecamatanktp()
				]
			},
			{
			    columnWidth: .13,
			    layout: 'form',
			    border: false,
				labelWidth:90,
			    items:
				[
					mComboKtpkelurahan()
				]
			},
			{
				columnWidth: .060,
				layout: 'form',
				border: false,
				labelWidth:90,
				items:
				[
					{
						xtype: 'textfield',
						fieldLabel: 'Kd. Pos ',
						name: 'txtpos',
						id: 'txtposktp',
						tabIndex:26,
						emptyText:' ',
						anchor: '99%',
						listeners: {
							'render': function(c) {
								c.getEl().on('keypress', function(e) {
									if(e.getKey() == 13) //atau Ext.EventObject.ENTER
									Ext.getCmp('cboPoliklinikRequestEntry').focus();
								}, c);
							}
						}
					}
				]
			}


		]
        };
    return items;
};
//-------------------------end--------------------------------------------------
var autohideandshowunitperawat;

//sub form view data kunjungan pasien 1
function getItemDataKunjungan_viDaftarIGD1()
{
   
        var items =
            {

                layout: 'column',
                border: false,
                labelAlign: 'top',
                items:
		[
			{
			    columnWidth: .15,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'datefield',
					    fieldLabel: 'Tanggal ',
					    name: 'dtpTanggal',
					    id: 'dptTanggal',
						format: 'd/M/Y',
						value: now_viDaftarIGD,
					    anchor: '95%'
					}
				]
			},
			{
			    columnWidth: .10,
			    layout: 'form',
			    border: false,
				labelWidth:90,
			    items:
				[
					{
						xtype: 'textfield',
						id: 'txtJamKunjung',
						value: h+':'+m+':'+s,
						width: 120,
						anchor: '95%',
						listeners:
						{
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13)
								{
									 
								}
							}
						}
					}
				]
			},
			{
			    columnWidth: .20,
			    layout: 'form',
			    border: false,
				labelWidth:90,
				anchor: '95%',
			    items:
				[
					{
						xtype     :'checkbox',
						boxLabel  : 'Non Kunjungan ',
						name      : 'cbnonkunjunganIGD',
						id        : 'cbnonkunjunganIGD',
					},
					mComboPoliklinik(),
					mComboDokterRequestEntry()
				]
			},
			{
			    columnWidth: .60,
				height:50,
			    layout: 'form',
			    border: false,
                labelWidth:90,
			    items:
				[]
			},
			{
			    columnWidth: .30,
			    layout: 'form',
			    border: false,
				labelWidth:90,
			    items:
				[
				  {  

					xtype: 'combo',
					fieldLabel: 'Kelompok Pasien',
					id: 'kelPasien',
					editable: false,
					tabIndex:29,
					//value: 'Perseorangan',
					store: new Ext.data.ArrayStore
						(
							{
							id: 0,
							fields:
							[
								'Id',
								'displayText'
							],
							   data: [[1, 'Perseorangan'], [2, 'Perusahaan'], [3, 'Asuransi']]
							}
						),
				  displayField: 'displayText',
				  mode: 'local',
				  width: 100,
				  forceSelection: true,
				  triggerAction: 'all',
				  emptyText: 'Pilih Salah Satu...',
				  selectOnFocus: true,
				  anchor: '95%',
				  listeners:
					{
							'select': function(a, b, c) // show combo
						{
						   Combo_Select(b.data.displayText);
						   if(b.data.displayText == 'Perseorangan')
						   {
							   Ext.getCmp('cboRujukanDariRequestEntry').hide();
							   Ext.getCmp('cboRujukanRequestEntry').hide();
							   jeniscus='0';
						   }
						   else if(b.data.displayText == 'Perusahaan')
						   {
							   Ext.getCmp('cboRujukanDariRequestEntry').show();
							   Ext.getCmp('cboRujukanRequestEntry').show();
							   jeniscus='1';
						   }
						   else if(b.data.displayText == 'Asuransi')
						   {
							  Ext.getCmp('cboRujukanDariRequestEntry').show();
							   Ext.getCmp('cboRujukanRequestEntry').show();  
							   jeniscus='2';
						   }
						   RefreshDatacombo(jeniscus);
						   
						   //alert(b.data.displayText);
						},
						  'render': function(c)
							{
								c.getEl().on('keypress', function(e) {
								if(e.getKey() == 13) //atau Ext.EventObject.ENTER
								Ext.getCmp('cboPerseorangan').focus();
								}, c);
							}

					}
				  }
				]
			},
                        
                        {
			    columnWidth: .20,
			    layout: 'form',
			    border: false,
				labelWidth:90,
			    items:
				[
					mComboPerseorangan()
				]
			},
			{
			    columnWidth: .20,
			    layout: 'form',
			    border: false,
				labelWidth:90,
			    items:
				[
					mComboPerusahaan()
				]
			},
                        {
			    columnWidth: .20,
			    layout: 'form',
			    border: false,
				labelWidth:90,
			    items:
				[
					mComboAsuransi()
				]
			},{
			    columnWidth: .20,
			    layout: 'form',
			    border: false,
                labelWidth:100,
			    items:
				[
                	autocomdiagnosa=Q().autocomplete({
						hidden:true,
                		fieldLabel: 'Diagnosa',
                	
						store	: new Ext.data.ArrayStore({id: 0,fields: ['text','kd_penyakit','penyakit'],data: []}),
						select	: function(a,b,c){
						},
						success:function(o){
							return o.listData;
						},
						insert	: function(o){
							return {
								kd_penyakit        	:o.kd_penyakit,
								penyakit 			: o.penyakit,
								text				:'<table style="font-size: 11px;"><tr><td width="50">'+o.kd_penyakit+'</td><td width="200">'+o.penyakit+'</td></tr></table>'
							}
						},
						url		: baseURL + "index.php/main/functionRWJ/getPenyakit",
						valueField: 'penyakit',
						displayField: 'text',
						keyField:'kd_penyakit',
						
						listWidth: 250
					})
				]
			},
			{
				columnWidth: .20,
				layout: 'form',
				border: false,
				labelWidth:90,
				items:
					[
						{
							xtype: 'textfield',
							fieldLabel: 'Nama Peserta',
							maxLength: 200,
							name: 'txtNama_viDaftarIGDPeserta',
							id: 'txtNama_viDaftarIGDPeserta',
							width: 100,
							tabIndex:31,
							anchor: '95%'
						 }
					]
			},
			{
			    columnWidth: .20,
			    layout: 'form',
			    border: false,
				labelWidth:90,
			    items:
				[
					{
						xtype: 'textfield',
						fieldLabel: 'No. Askes',
						maxLength: 200,
						name: 'txtNoAskes',
						id: 'txtNoAskes',
						width: 100,
						tabIndex:32,
						anchor: '95%',
							listeners :
										{
										'render': function(c)
											{
												c.getEl().on('keypress', function(e) {
												if(e.getKey() == 13) {
													daftarRWJgetdatabpjs();
												Ext.getCmp('txtNoSJP').focus();
												}//atau Ext.EventObject.ENTER
													
												}, c);
											},
											blur:function(){
												
											
											}

										}
					}
				]
			},
			{
			    columnWidth: .20,
			    layout: 'form',
			    border: false,
				labelWidth:90,
			    items:
				[
					{
						xtype: 'textfield',
						fieldLabel: 'No. SJP',
						maxLength: 200,
						name: 'txtNoSJP',
						id: 'txtNoSJP',
						width: 100,
						tabIndex:33,
						anchor: '95%'
					}
				]
			}
                        
                        
		]

	};
    return items;
};
//-------------------------end--------------------------------------------------

//form data pasien tab
function getPenelItemDataKunjungan_viDaftarIGD(lebar)
{
    var items =
	{
	   xtype:'tabpanel',
           plain:true,
           activeTab: 0,
           height:300,
           //deferredRender: false,
           defaults:
           {
            bodyStyle:'padding:10px',
            autoScroll: true
           },
           items:[
                    DataPanel1(),
                    DataPanel2(),
                    DataPanel3(),
                    DataPanel4()
                            
                 ]
        }
    return items;
};
//------------------end---------------------------------------------------------

function subdatapanel1()
{
    var items=
    {
           xtype: 'fieldset',
            title: 'Rujukan',
            //Height: 50,
            items: [
                    {
						columnWidth: .80,
						layout: 'form',
						labelWidth:90,
						border: false,
						items:
						[
							{
								xtype: 'textfield',
								fieldLabel: 'Anamnese ',
								name: 'txtAnamnese',
								id: 'txtAnamnese',
								emptyText:' ',
								tabIndex:34,
								anchor: '95%',
								listeners: {
										'render': function(c) {
											c.getEl().on('keypress', function(e) {
												if(e.getKey() == 13) //atau Ext.EventObject.ENTER
												Ext.getCmp('txtAlergi').focus();
											}, c);
										}
								}
							}
						]
					},
            {
                columnWidth: .80,
                layout: 'form',
                border: false,
                labelWidth:90,
                items:
                    [
                            {
                                xtype: 'textfield',
                                fieldLabel: 'Alergi ',
                                name: 'txtAlergi',
                                id: 'txtAlergi',
                                emptyText:' ',
								tabIndex:35,
                                anchor: '95%'
                            }
                    ]
            }  

            ]   
    };
    return items;
}

//sub form data pasien tab1
function DataPanel1()
{
        var items =
        {
            title:'Kunjungan',
            layout:'form',
            items:
                [
                    getItemDataKunjungan_viDaftarIGD1(),
                    subdatapanel1()
                ]
        };
        return items;
}
//------------------end---------------------------------------------------------

//sub form data pasien tab4 kontak pasien
function DataPanel2()
{
    var items =
        {
            title:'Kontak',
            layout:'form',
            items:
                [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'No Telepon ',
                        name: 'txtNotlppasien_viDaftarIGD',
                        id: 'txtNotlppasien_viDaftarIGD',
						tabIndex:36,
                        width: 250,
						listeners: {
								'render': function(c) {
									c.getEl().on('keypress', function(e) {
										if(e.getKey() == 13) //atau Ext.EventObject.ENTER
											Ext.getCmp('txtNohppasien_viDaftarIGD').focus();
									}, c);
								}
						}
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Handphone ',
                        name: 'txtNohppasien_viDaftarIGD',
                        id: 'txtNohppasien_viDaftarIGD',
						tabIndex:37,
                        width: 500,
						listeners: {
								'render': function(c) {
									c.getEl().on('keypress', function(e) {
										if(e.getKey() == 13) //atau Ext.EventObject.ENTER
											Ext.getCmp('txtEmailPasien').focus();
									}, c);
								}
						}
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Email ',
                        name: 'txtEmailPasien',
						tabIndex:38,
                        id: 'txtEmailPasien'
                    }
                ]
                                
                               
                                 
        };
        return items;
};

//sub form data pasein tab3
function DataPanel3()
{
    var items =
        {
            title:'Penerimaan',
            layout:'form',
            items:
                [
                    {
                        xtype: 'fieldset',
                        title: 'Rujukan',
                        //Height: 50,
                        items: [{
                                    xtype: 'radiogroup',
                                    id: 'rbrujukan',
                                    //fieldLabel: 'Auto Layout',
                                    items: [
                                    {boxLabel: 'Perseorangan', name: 'rb-auto', tabIndex:39, inputValue: 1},
                                    {boxLabel: 'Rujukan', name: 'rb-auto', tabIndex:40, inputValue: 2}
                                            ]
                                }
                        ]
                    },
//                       mComboRujukanDari(),
                        mcomborujukandari(),
                        mComboRujukan(),
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Nama ',
                        name: 'txtNama_viDaftarIGDPerujuk',
                        id: 'txtNama_viDaftarIGDPerujuk',
						tabIndex:43,
                        width: 250
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Alamat ',
                        name: 'txtAlamat_viDaftarIGDPerujuk',
                        id: 'txtAlamat_viDaftarIGDPerujuk',
						tabIndex:44,
                        width: 500
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Kota ',
                        name: 'txtKotaPerujuk',
						tabIndex:45,
                        id: 'txtKotaPerujuk'
                    }
                ]
                                
                               
                                 
        };
        return items;
};
//------------------end---------------------------------------------------------

//sub foem data pasien tab4
function DataPanel4()
{
    var items =
    {
		title:'Penanggung Jawab',
		layout:'column',
		items:
		[
			{
				columnWidth: .50,
				layout: 'form',
				labelWidth:100,
				border: false,
				items:
				[
						{
							xtype: 'textfield',
							fieldLabel: 'Nama ',
							name: 'txtNamaPenanggungjawab',
							id: 'txtNamaPenanggungjawab',
							tabIndex:46,
							width: 200
						},
						{
							xtype: 'textfield',
							fieldLabel: 'Tempat Lahir ',
							name: 'txtTempatPenanggungjawab',
							id	: 'txtTempatPenanggungjawab',
							tabIndex:47,
							width: 200
						},
						
							
						{
						xtype: 'datefield',
						fieldLabel: 'Tanggal Lahir ',
						name: 'dtpPenanggungJawabTanggalLahir',
						id: 'dtpPenanggungJawabTanggalLahir',
						tabIndex:48,
						//format: 'M/d/Y',
						format : "d/M/Y",
						value: Today_PendaftaranIGD,
						anchor: '95%',
						listeners:
							{
								'render': function(c) {
														c.getEl().on('keypress', function(e) {
															if(e.getKey() == 13) //atau Ext.EventObject.ENTER
															Ext.getCmp('cboJKPJ').focus();
														}, c);
                                                }	
							}
						},
						mComboJKPJ(),
						{
							columnWidth: .050,
							layout: 'form',
							border: false,
							labelWidth:100,
							anchor: '95%',
							items:
							[
								{
									xtype     :'checkbox',
									boxLabel  : 'WNA',
									tabIndex  :49,
									name      : 'cbWnaPenanggungJawab',
									id        : 'cbWnaPenanggungJawab'
								}
							]
						},
						mComboStatusMaritalPenanggungjawab(),
						{
							xtype: 'textfield',
							fieldLabel: 'Alamat ',
							name: 'txtAlamatPenanggungjawab',
							id: 'txtAlamatPenanggungjawab',
							tabIndex:51,
							width: 300,
						},
						mComboPropinsiPenanggungJawab(),
						mComboKabupatenpenanggungjawab(),
						mComboKecamatanPenanggungJawab(),
						mCombokelurahanPenanggungJawab(),
						{
							xtype: 'textfield',
							fieldLabel: 'Kode Pos ',
							name: 'txtKdPosPenanggungjawab',
							id: 'txtKdPosPenanggungjawab',
							tabIndex:56,
							width: 50
						},
						{

							xtype: 'textfield',
							fieldLabel: 'Tlp ',
							tabIndex:57,
							name: 'txtTlpPenanggungjawab',
							id: 'txtTlpPenanggungjawab',
							width: 100
						},
						{
							xtype: 'textfield',
							fieldLabel: 'No Handphone ',
							name: 'txtHpPenanggungjawab',
							tabIndex:58,
							id: 'txtHpPenanggungjawab',
							width: 100
						}
						
				]
			},
			
			{
				columnWidth: .50,
				layout: 'form',
				labelWidth:100,
				border: false,
				items:
				[
						{
							xtype: 'textfield',
							fieldLabel: 'No KTP ',
							name: 'txtNoKtpPenanggungjawab',
							id: 'txtNoKtpPenanggungjawab',
							tabIndex:59,
							width: 200
						},
						{
							xtype: 'textfield',
							fieldLabel: 'Alamat KTP',
							name: 'txtalamatktpPenanggungjawab',
							id: 'txtalamatktpPenanggungjawab',
							tabIndex:60,
							width: 300
						},
							mComboPropinsiKTPPenanggungJawab(),
							mComboKabupatenKtppenanggungjawab(),
							mComboKecamatanKtpPenanggungJawab(),
							mCombokelurahanKtpPenanggungJawab(),
						{
							xtype: 'textfield',
							fieldLabel: 'Kode Pos KTP ',
							name: 'txtKdPosKtpPenanggungjawab',
							id: 'txtKdPosKtpPenanggungjawab',
							tabIndex:65,
							width: 50
						},
						{
							xtype: 'textfield',
							fieldLabel: 'Alamat Prsh ',
							name: 'txtAlamatPrshPenanggungjawab',
							id: 'txtAlamatPrshPenanggungjawab',
							tabIndex:66,
							width: 300
						},
						mComboHubunganKeluarga(),
						mComboPendidikanPenanggungJawab(),
						mComboPekerjaanPenanggungJawabRequestEntry(),
						mComboPerusahaanPenanggungJawab(),
						{
							xtype: 'textfield',
							fieldLabel: 'Email ',
							name: 'txtEmailPenanggungjawab',
							id: 'txtEmailPenanggungjawab',
							tabIndex:71,
							width: 300
						},
				]
			}
		]
            
    };
    return items;
}


//------------------end---------------------------------------------------------

function datasave_viDaftarIGD(mBol)
{	
    if (ValidasiEntry_viDaftarIGD('Simpan Data',false) == 1 )
    {
        //alert('a')
        if (addNew_viDaftarIGD == true)
        {
            Ext.Ajax.request
            (
				{
					url: baseURL + "index.php/main/CreateDataObj",
					params: dataparam_viDaftarIGD(),
					success: function(o)
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true)
						{
							ShowPesanInfo_viDaftarIGD('Data berhasil di simpan','Simpan Data');
							datarefresh_viDaftarIGD(tmpcriteriaIGD);
							// if(mBol === false)
							// {
								// Ext.get('txtID_viDaftarIGD').dom.value=cst.ID_SETUP;
							// };
							addNew_viDaftarIGD = false;
							//Ext.get('txtNoKunjungan_viDaftarIGD').dom.value = cst.NO_KUNJUNGAN
							Ext.get('txtNoRequest_viDaftarIGD').dom.value = cst.KD_PASIEN;
                                                        printbill();
							Ext.getCmp('btnSimpan_viDaftarIGD').disable();
							Ext.getCmp('btnSimpanExit_viDaftarIGD').disable();
							//Ext.getCmp('btnDelete_viDaftarIGD').enable();
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarning_viDaftarIGD('Data tidak berhasil di simpan ','Simpan Data');
						}
					    else if (cst.success == false && cst.cari == true) 
							{
								ShowPesanError_viDaftarIGD('Anda telah bekunjung sebelumnya', 'Simpan Data');
							}
						
						else
						{
							ShowPesanError_viDaftarIGD('Data tidak berhasil di simpan ','Simpan Data');
						}
					}
				}
            )
        }
        else
        {
            Ext.Ajax.request
            (
            {
                url: WebAppUrl.UrlUpdateData,
                params: dataparam_viDaftarIGD(),
                success: function(o)
                {
                    //alert(o);
                    var cst = Ext.decode(o.responseText);
                    if (cst.success === true)
                    {
                        ShowPesanInfo_viDaftarIGD('Data berhasil disimpan','Edit Data');
                        datarefresh_viDaftarIGD(tmpcriteriaIGD);
                    }
                    else if  (cst.success === false && cst.pesan===0)
                    {
                        ShowPesanWarning_viDaftarIGD('Data tidak berhasil disimpan '  + cst.pesan,'Edit Data');
                    }
                    else
                    {
                        ShowPesanError_viDaftarIGD('Data tidak berhasil disimpan '  + cst.pesan,'Edit Data');
                    }
                }
            }
            )
        }
    }
    else
    {
        if(mBol === true)
        {
            return false;
        }
    }
}

function datadelete_viDaftarIGD()
{
    // var DataHapus = Ext.get('txtNPP_viDaftarIGD').getValue();
    if (ValidasiEntry_viDaftarIGD('Hapus Data',true) == 1 )
    {
        Ext.Msg.show
        (
        {
            title:'Hapus Data',
            msg: "Akan menghapus data?" ,
            buttons: Ext.MessageBox.YESNO,
            width:300,
            fn: function (btn)
            {
                if (btn =='yes')
                {
                    Ext.Ajax.request
                    (
                    {
                        url: WebAppUrl.UrlDeleteData,
                        params: dataparamDelete_viDaftarIGD(), //dataparam_viDaftarIGD(),
                        success: function(o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                ShowPesanInfo_viDaftarIGD('Data berhasil dihapus','Hapus Data');
                                datarefresh_viDaftarIGD(tmpcriteriaIGD);
                                dataaddnew_viDaftarIGD();
								Ext.getCmp('btnSimpan_viDaftarIGD').disable();
								Ext.getCmp('btnSimpanExit_viDaftarIGD').disable();	
								Ext.getCmp('btnDelete_viDaftarIGD').disable();	
								
                            }
                            else if (cst.success === false && cst.pesan===0)
                            {
                                ShowPesanWarning_viDaftarIGD('Data tidak berhasil dihapus ' ,'Hapus Data');
                            }
                            else if (cst.success === false && cst.pesan===1)
                            {
                                ShowPesanError_viDaftarIGD('Data tidak berhasil dihapus '  + cst.msg ,'Hapus Data');
                            }
                        }
                    }
                    )//end Ajax.request
                } // end if btn yes
            }// end fn
        }
        )//end msg.show
    }
}
//-----------------------------------------------------------------------------------------------------------------///

function ValidasiEntry_viDaftarIGD(modul,mBolHapus)
{
    var x = 1;
	
	if (Ext.getCmp('txtNama_viDaftarIGD').getValue() === "")
	{
		ShowPesanWarning_viDaftarIGD("Nama belum terisi...",modul);
		x=0;
	}/* 
	if (Ext.get('txtNama_viDaftarIGDKeluarga').getValue() === " ")
	{
		ShowPesanWarning_viDaftarIGD("Nama keluarga belum terisi...",modul);
		x=0;
	} */
	if (Ext.getCmp('cboJK').getValue() === "")
	{
		ShowPesanWarning_viDaftarIGD("Jenis Kelamin belum dipilih...",modul);
		x=0;
	}
	if (Ext.getCmp('txtTempatLahir_viDaftarIGD').getValue() === "")
	{
		ShowPesanWarning_viDaftarIGD("Tempat Lahir belum terisi...",modul);
		x=0;
	}
	if (Ext.getCmp('cboAgamaRequestEntry').getValue() === "" || Ext.getCmp('cboAgamaRequestEntry').getValue() === "Pilih Agama..."){
		ShowPesanWarning_viDaftarIGD("Agama belum terisi...",modul);
		x=0;
	}
	if (Ext.getCmp('cboGolDarah').getValue() === "" || Ext.getCmp('cboGolDarah').getValue() === "Pilih Gol. Darah..."){
		ShowPesanWarning_viDaftarIGD("Golongan darah belum terisi...",modul);
		x=0;
	}
	if (Ext.getCmp('cboJK').getValue() === "" || Ext.getCmp('cboJK').getValue() === "Pilih Gol. Darah..."){
		ShowPesanWarning_viDaftarIGD("Jenis kelamin belum terisi...",modul);
		x=0;
	}
	if (Ext.getCmp('cboStatusMarital').getValue() === "" || Ext.getCmp('cboStatusMarital').getValue() === "Pilih Status..."){
		ShowPesanWarning_viDaftarIGD("Status marita belum terisi...",modul);
		x=0;
	}
	if (Ext.getCmp('cboPendidikanRequestEntry').getValue() === "" || Ext.getCmp('cboPendidikanRequestEntry').getValue() === "Pilih Pendidikan..."){
		ShowPesanWarning_viDaftarIGD("Pendidikan belum terisi...",modul);
		x=0;
	}
	if (Ext.getCmp('cboPekerjaanRequestEntry').getValue() === "" || Ext.getCmp('cboPekerjaanRequestEntry').getValue() === "Pilih Pekerjaan..."){
		ShowPesanWarning_viDaftarIGD("Pekerjaan belum terisi...",modul);
		x=0;
	}
	if (Ext.getCmp('txtAlamat_viDaftarIGD').getValue() === ""){
		ShowPesanWarning_viDaftarIGD("Alamat belum terisi...",modul);
		x=0;
	}
	if (Ext.getCmp('cboPoliklinikRequestEntry').getValue() === ""  &&  Ext.getCmp('cbnonkunjunganIGD').getValue() === false){
		ShowPesanWarning_viDaftarIGD("Unit belum terisi...",modul);
		x=0;
	}


    return x;
}

function ShowPesanWarning_viDaftarIGD(str,modul)
{
    Ext.MessageBox.show
    (
		{
			title: modul,
			msg:str,
			buttons: Ext.MessageBox.OK,
			icon: Ext.MessageBox.WARNING,
			width :250
		}
    )
}

function ShowPesanError_viDaftarIGD(str,modul)
{
    Ext.MessageBox.show
    (
		{
			title: modul,
			msg:str,
			buttons: Ext.MessageBox.OK,
			icon: Ext.MessageBox.ERROR,
			width :250
		}
    )
}

function ShowPesanInfo_viDaftarIGD(str,modul)
{
    Ext.MessageBox.show
    (
		{
			title: modul,
			msg:str,
			buttons: Ext.MessageBox.OK,
			icon: Ext.MessageBox.INFO,
			width :250
		}
    )
}

function datarefresh_viDaftarIGD(criteria)
{
	
    //criteria = getCriteriaFilter_viDaftarIGD()

    dataSource_viDaftarIGD.load
    (
		{
			params:
			{
				Skip: 0,
				Take: selectCount_viDaftarIGD,
				Sort: '',
				Sortdir: 'ASC',
				target:'View_DaftarIGD',
				param: criteria
			}
		}
    );
    return dataSource_viDaftarIGD;
}

function getCriteriaFilter_viDaftarIGD()
{
      	 var strKriteria = "";

           if (Ext.get('txtNoMedrecD_vidaftarIGD').getValue() != "")
            {
                strKriteria = " kd_pasien = " + "'" + Ext.get('txtNoMedrecD_vidaftarIGD').getValue() +"'";
            }
            if (Ext.get('txtTglLahir_vidaftarIGD').getValue() != "")
            {
                if (strKriteria == "")
                    {
                         strKriteria = " tgl_lahir = " + "'" + Ext.get('txtTglLahir_vidaftarIGD').getValue() +"'";
                    }
                    else
                        {
                            strKriteria += " tgl_lahir = " + "'" + Ext.get('txtTglLahir_vidaftarIGD').getValue() +"'";
                        }
                
            }
            if (Ext.get('txtAlamat_viDaftarIGDPasien_vidaftarIGD').getValue() != "")
            {
                if (strKriteria == "")
                    {
                         strKriteria = " lower(alamat) " + "LIKE lower('" + Ext.get('txtAlamat_viDaftarIGDPasien_vidaftarIGD').getValue() +"%')";
                    }
                    else
                        {
                            strKriteria += " and lower(alamat) " + "LIKE lower('" + Ext.get('txtAlamat_viDaftarIGDPasien_vidaftarIGD').getValue() +"%')";
                        }
                
            }
            if (Ext.get('txtNama_viDaftarIGDPasien_vidaftarIGD').getValue() != "")
            {
                if (strKriteria == "")
                    {
                        strKriteria = " lower(nama) " + "LIKE lower('" + Ext.get('txtNama_viDaftarIGDPasien_vidaftarIGD').getValue() +"%')";
                    }
                    else
                        {
                            strKriteria += " and lower(nama) " + "LIKE lower('" + Ext.get('txtNama_viDaftarIGDPasien_vidaftarIGD').getValue() +"%')";
                        }
                
            }

	 return strKriteria;
}

function setUsia(Tanggal)
{

Ext.Ajax.request
( {
	   url: baseURL + "index.php/main/GetUmur",
	   params: {
	   TanggalLahir: ShowDateReal(Tanggal)
	   },
	   success: function(o)
	   {
		//alert('test');  
		
	   var tmphasil = o.responseText;
	 // alert(tmphasil);
	   var tmp = tmphasil.split(' ');
		//alert(tmp.length);
	  if (tmp.length == 6)
							{
								Ext.getCmp('txtThnLahir_viDaftarIGD').setValue(tmp[0]);
								Ext.getCmp('txtBlnLahir_viDaftarIGD').setValue(tmp[2]);
								Ext.getCmp('txtHariLahir_viDaftarIGD').setValue(tmp[4]);
							}
							else if(tmp.length == 4)
							{
								if(tmp[1]== 'years' && tmp[3] == 'day')
								{
									Ext.getCmp('txtThnLahir_viDaftarIGD').setValue(tmp[0]);
									Ext.getCmp('txtBlnLahir_viDaftarIGD').setValue('0');
									Ext.getCmp('txtHariLahir_viDaftarIGD').setValue(tmp[2]);  
								}else{
								Ext.getCmp('txtThnLahir_viDaftarIGD').setValue('0');
								Ext.getCmp('txtBlnLahir_viDaftarIGD').setValue(tmp[0]);
								Ext.getCmp('txtHariLahir_viDaftarIGD').setValue(tmp[2]);
									  }
							}
							else if(tmp.length == 2 )
							{
								
								if (tmp[1] == 'year' )
								{
									Ext.getCmp('txtThnLahir_viDaftarIGD').setValue(tmp[0]);
									Ext.getCmp('txtBlnLahir_viDaftarIGD').setValue('0');
									Ext.getCmp('txtHariLahir_viDaftarIGD').setValue('0');
								}
								else if (tmp[1] == 'years' )
								{
									Ext.getCmp('txtThnLahir_viDaftarIGD').setValue(tmp[0]);
									Ext.getCmp('txtBlnLahir_viDaftarIGD').setValue('0');
									Ext.getCmp('txtHariLahir_viDaftarIGD').setValue('0');
								}
								else if (tmp[1] == 'mon'  )
								{
									Ext.getCmp('txtThnLahir_viDaftarIGD').setValue('0');
									Ext.getCmp('txtBlnLahir_viDaftarIGD').setValue(tmp[0]);
									Ext.getCmp('txtHariLahir_viDaftarIGD').setValue('0');
								}
								else if (tmp[1] == 'mons'  )
								{
									Ext.getCmp('txtThnLahir_viDaftarIGD').setValue('0');
									Ext.getCmp('txtBlnLahir_viDaftarIGD').setValue(tmp[0]);
									Ext.getCmp('txtHariLahir_viDaftarIGD').setValue('0');
								}
								else{
								Ext.getCmp('txtThnLahir_viDaftarIGD').setValue('0');
								Ext.getCmp('txtBlnLahir_viDaftarIGD').setValue('0');
								Ext.getCmp('txtHariLahir_viDaftarIGD').setValue(tmp[0]);
									}
							}
							
							else if(tmp.length == 1)
							{
								Ext.getCmp('txtThnLahir_viDaftarIGD').setValue('0');
								Ext.getCmp('txtBlnLahir_viDaftarIGD').setValue('0');
								Ext.getCmp('txtHariLahir_viDaftarIGD').setValue('1');
							}else
							{
								alert("Tanggal yang anda masukan lebih besar dari tanggal sekarang");
							}
	   }
		   
		   
});


	
}

function datainit_viDaftarIGD(rowdata)
{
	addNew_viDaftarIGD = false;
	addNew_viDaftarIGD = true;
	dsPropinsiRequestEntry = undefined;
	Ext.getCmp('txtNoRequest_viDaftarIGD').setValue(rowdata.KD_PASIEN);
	Ext.getCmp('txtNama_viDaftarIGD').setValue(rowdata.NAMA);
	Ext.getCmp('txtNama_viDaftarIGDKeluarga').setValue(rowdata.NAMA_KELUARGA);
	Ext.getCmp('txtTempatLahir_viDaftarIGD').setValue(rowdata.TEMPAT_LAHIR);
	Ext.getCmp('cboPendidikanRequestEntry').setValue(rowdata.PENDIDIKAN);
	Ext.getCmp('cboPekerjaanRequestEntry').setValue(rowdata.PEKERJAAN);
	Ext.getCmp('cbWna').setValue(rowdata.WNI);
	Ext.getCmp('txtAlamat_viDaftarIGD').setValue(rowdata.ALAMAT);
	
	var tmpagama="";
	if (rowdata.AGAMA===0){
		tmpagama = 'ISLAM';
	} else if(rowdata.AGAMA===1){
		tmpagama = 'KRISTEN';
	} else if(rowdata.AGAMA===2){
		tmpagama = 'PROTESTAN';
	} else if(rowdata.AGAMA===3){
		tmpagama = 'KATHOLIK';
	} else if(rowdata.AGAMA===4){
		tmpagama = 'HINDU';
	} else if(rowdata.AGAMA===5){
		tmpagama = 'BUDHA';
	} else if(rowdata.AGAMA===6){
		tmpagama = 'KONG HU CU';
	} else if(rowdata.AGAMA===7){
		tmpagama = 'KEPERCAYAAN';
	} else {
		tmpagama = 'TIDAK TAHU';
	}
	
	var goldarah="";
	if (rowdata.GOL_DARAH === 1){
		goldarah = '-';
	} else if (rowdata.GOL_DARAH === 2){
		goldarah = 'A+';
	} else if (rowdata.GOL_DARAH === 3){
		goldarah = 'B+';
	} else if (rowdata.GOL_DARAH === 4){
		goldarah = 'AB+';
	} else if (rowdata.GOL_DARAH === 5){
		goldarah = 'O+';
	} else if (rowdata.GOL_DARAH === 6){
		goldarah = 'A-';
	} else {
		goldarah = 'B-';
	} 
	
	Ext.getCmp('cboAgamaRequestEntry').setValue(tmpagama);
	Ext.getCmp('cboGolDarah').setValue(goldarah);
	Ext.get('dtpTanggalLahir_viDaftarIGD').dom.value = ShowDate(rowdata.TGL_LAHIR);
	Ext.getCmp('cboStatusMarital').setValue(rowdata.STATUS_MARITA);
	var tmpjk = "";
	var tmpwni = "";
	if (rowdata.JENIS_KELAMIN === "t")
		{
			tmpjk = "Laki - Laki";
		}
		else
			{
				tmpjk = "Perempuan";
			}
	Ext.getCmp('cbWna').setValue(tmpwni);
	Ext.getCmp('cboJK').setValue(tmpjk);
	
	Ext.getCmp('txtpos').setValue(rowdata.KD_POS);
	Ext.getCmp('cboPropinsiRequestEntry').setValue(rowdata.PROPINSI);
	Ext.getCmp('cboKabupatenRequestEntry').setValue(rowdata.KABUPATEN);
	Ext.getCmp('cboKecamatanRequestEntry').setValue(rowdata.KECAMATAN);
	Ext.getCmp('cbokelurahan').setValue(rowdata.KELURAHAN);
	Ext.getCmp('txtTmpKelurahan_viDaftarIGD').setValue(rowdata.KD_KELURAHAN);
	Ext.getCmp('txtTmpPropinsi_viDaftarIGD').setValue(rowdata.KD_PROPINSI);
	Ext.getCmp('txtTmpKabupaten_viDaftarIGD').setValue(rowdata.KD_KABUPATEN);
	Ext.getCmp('txtTmpKecamatan_viDaftarIGD').setValue(rowdata.KD_KECAMATAN);
	Ext.getCmp('txtTmpPendidikan_viDaftarIGD').setValue(rowdata.KD_PENDIDIKAN);
	Ext.getCmp('txtTmpPekerjaan_viDaftarIGD').setValue(rowdata.KD_PEKERJAAN);
	Ext.getCmp('txtTmpAgama_viDaftarIGD').setValue(rowdata.KD_AGAMA);
	Ext.getCmp('txtNotlppasien_viDaftarIGD').setValue(rowdata.TELEPON); 
	Ext.getCmp('txtNohppasien_viDaftarIGD').setValue(rowdata.HANDPHONE); 
	Ext.getCmp('txtEmailPasien').setValue(rowdata.EMAIL); 
	Ext.getCmp('txtNamaAyah').setValue(rowdata.NAMA_AYAH);
	Ext.getCmp('txtNamaIbu').setValue(rowdata.NAMA_IBU);
	Ext.getCmp('txtAlamatktp').setValue(rowdata.ALAMAT_KTP);
	
	Ext.getCmp('cboPropinsiKtp').setValue(rowdata.PRO_KTP);
	Ext.getCmp('cboKtpKabupaten').setValue(rowdata.KAB_KTP);
	Ext.getCmp('cboKecamatanKtp').setValue(rowdata.KEC_KTP);
	Ext.getCmp('cbokelurahanKtp').setValue(rowdata.KEL_KTP);
	Ext.getCmp('txtposktp').setValue(rowdata.KD_POS_KTP);
	Ext.getCmp('txtTmpKelurahanKtp_viDaftarIGD').setValue(rowdata.KELURAHANKTP);
	setUsia(ShowDate(rowdata.TGL_LAHIR));
	loaddatastoreKtpkabupaten(rowdata.PROPINSIKTP);
	loaddatastoreKtpkecamatan(rowdata.KABUPATENKTP);
	loaddatastoreKtpkelurahan(rowdata.KECAMATANKTP);
	loaddatastorekabupaten(rowdata.KD_PROPINSI);
	loaddatastorekecamatan(rowdata.KD_KABUPATEN);
	loaddatastorekelurahan(rowdata.KD_KECAMATAN);
       	
	
	
		
		
	//Ext.getCmp('btnSimpan_viDaftarIGD').disable();
	//Ext.getCmp('btnSimpanExit_viDaftarIGD').disable();	
	//Ext.getCmp('btnDelete_viDaftarIGD').enable();
	//Ext.getCmp('btnUlang_viDaftarIGD').enable();
	mNoKunjungan_viKasir = rowdata.NO_KUNJUNGAN;
	
}

function dataaddnew_viDaftarIGD()
{
    addNew_viDaftarIGD = true;
    dsPropinsiRequestEntry = undefined;
	Ext.getCmp('txtNoRequest_viDaftarIGD').setValue('');
	Ext.getCmp('txtNama_viDaftarIGD').setValue('');
	Ext.getCmp('txtNama_viDaftarIGDKeluarga').setValue('');
	Ext.getCmp('txtTempatLahir_viDaftarIGD').setValue('');
	Ext.getCmp('txtThnLahir_viDaftarIGD').setValue('');
	Ext.getCmp('txtBlnLahir_viDaftarIGD').setValue('');
	Ext.getCmp('txtHariLahir_viDaftarIGD').setValue('');
	//Ext.getCmp('cboWarga').setValue('');
	Ext.getCmp('txtAlamat_viDaftarIGD').setValue('');
	Ext.getCmp('txtNotlppasien_viDaftarIGD').setValue('');
	Ext.getCmp('txtNohppasien_viDaftarIGD').setValue('');
	Ext.getCmp('txtEmailPasien').setValue('');
	Ext.getCmp('txtTempatPenanggungjawab').setValue('');
	Ext.getCmp('txtEmailPenanggungjawab').setValue('');
	
	//kunjungan
	Ext.getCmp('kelPasien').setValue('');
	Ext.getCmp('txtJamKunjung').setValue(h+':'+m+':'+s);
	Ext.getCmp('txtNama_viDaftarIGDPeserta').setValue('');
	Ext.getCmp('txtNoAskes').setValue('');
	Ext.getCmp('txtNoSJP').setValue('');
	Ext.getCmp('txtAnamnese').setValue('');
	Ext.getCmp('txtAlergi').setValue('');
	
	//Perujuk
	Ext.getCmp('cboRujukanDariRequestEntry').setValue('');
	Ext.getCmp('cboRujukanRequestEntry').setValue('');
	Ext.getCmp('txtNama_viDaftarIGDPerujuk').setValue('');
	Ext.getCmp('txtAlamat_viDaftarIGDPerujuk').setValue('');
	Ext.getCmp('txtKotaPerujuk').setValue('');
	//Ext.getCmp('rbrujukan').setValue('');
	
	//Penanggung jawab
	Ext.getCmp('txtNamaPenanggungjawab').setValue('');
	Ext.getCmp('txtAlamatPenanggungjawab').setValue('');
	Ext.getCmp('cboKabupatenpenanggungjawab').setValue('');
	Ext.getCmp('txtKdPosPenanggungjawab').setValue('');
	Ext.getCmp('txtTlpPenanggungjawab').setValue('');
	Ext.getCmp('cboPekerjaanPenanggungJawabRequestEntry').setValue('');
	Ext.getCmp('cboPropinsiPenanggungJawab').setValue('');
	Ext.getCmp('cboKecamatanPenanggungJawab').setValue('');
	Ext.getCmp('cbokelurahanPenanggungJawab').setValue('');
	Ext.getCmp('txtNoKtpPenanggungjawab').setValue('');
	Ext.getCmp('cboPropinsiKtpPenanggungJawab').setValue('');
	Ext.getCmp('cboKabupatenKtppenanggungjawab').setValue('');
	Ext.getCmp('cboKecamatanKtpPenanggungJawab').setValue('');
	Ext.getCmp('cbokelurahanKtpPenanggungJawab').setValue('');
	Ext.getCmp('txtalamatktpPenanggungjawab').setValue('');
	Ext.getCmp('txtKdPosKtpPenanggungjawab').setValue('');
	Ext.getCmp('txtHpPenanggungjawab').setValue('');
	Ext.getCmp('txtAlamatPrshPenanggungjawab').setValue('');
	Ext.getCmp('cboHubunganKeluarga').setValue('');
	
	Ext.getCmp('cboJK').setValue('');
	Ext.getCmp('cboAgamaRequestEntry').setValue('');
	Ext.getCmp('cboGolDarah').setValue('');
	Ext.getCmp('cboPropinsiRequestEntry').setValue('');
	Ext.getCmp('cboKabupatenRequestEntry').setValue('');
	Ext.getCmp('cboKecamatanRequestEntry').setValue('');
	Ext.getCmp('cboPendidikanRequestEntry').setValue('');
	Ext.getCmp('cboSukuRequestEntry').setValue('');
	Ext.getCmp('cboStatusMarital').setValue('');
//  Ext.getCmp('cboRujukanDari').setValue('');
	Ext.getCmp('cboDokterRequestEntry').setValue('');
	Ext.getCmp('cboPerseorangan').setValue('');
	Ext.getCmp('cboPerusahaanRequestEntry').setValue('');
	Ext.getCmp('cboAsuransiIGD').setValue('');
	Ext.getCmp('cboPoliklinikRequestEntry').setValue('');
	Ext.getCmp('dtpTanggalLahir_viDaftarIGD').setValue(now_viDaftarIGD);
	Ext.getCmp('cboPekerjaanRequestEntry').setValue
	Ext.getCmp('txtTmpPropinsi_viDaftarIGD').setValue('');
	Ext.getCmp('txtTmpKabupaten_viDaftarIGD').setValue('');
	Ext.getCmp('txtTmpKecamatan_viDaftarIGD').setValue('');
	Ext.getCmp('txtTmpPendidikan_viDaftarIGD').setValue('');
	Ext.getCmp('txtTmpPekerjaan_viDaftarIGD').setValue('');
	Ext.getCmp('txtTmpAgama_viDaftarIGD').setValue('');
	Ext.getCmp('txtNamaAyah').setValue('');
	Ext.getCmp('txtNamaIbu').setValue('');
	Ext.getCmp('cbokelurahan').setValue('');
	Ext.getCmp('cboStatusMarital').setValue();
	Ext.getCmp('txtpos').setValue('');
	Ext.getCmp('txtposktp').setValue('');
	Ext.getCmp('cbWna').setValue(false);
	Ext.getCmp('cbWnaPenanggungJawab').setValue(false);
	Ext.getCmp('cboPekerjaanRequestEntry').setValue('');
	Ext.getCmp('txtAlamatktp').setValue('');
	Ext.getCmp('cboPropinsiKtp').setValue('');
	Ext.getCmp('cboKtpKabupaten').setValue('');
	Ext.getCmp('cboKecamatanKtp').setValue('');
	Ext.getCmp('cbokelurahanKtp').setValue('');
	Ext.getCmp('cboStatusMaritalPenanggungjawab').setValue();


	Ext.getCmp('btnSimpan_viDaftarIGD').enable();
	Ext.getCmp('btnSimpanExit_viDaftarIGD').enable();
//	Ext.getCmp('btnDelete_viDaftarIGD').disable();
//        Ext.getCmp('btnUlang_viDaftarIGD').disable();

//	mNoKunjungan_viKasir = '';
	
    rowSelected_viDaftarIGD   = undefined;
}
///---------------------------------------------------------------------------------------///

function dataparam_viDaftarIGD()
{
    var jenis_kelamin;
    if (Ext.getCmp('cboJK').getValue() === 1)
    {
        jenis_kelamin = true;
    }
    else
	{
		jenis_kelamin = false;
	}
		
    if(Ext.getCmp('cboJKPJ').getValue() !== '' || Ext.getCmp('cboJKPJ').getValue() !== 'Pilih Jenis Kelamin...'){
		var jeniskelaminpj;
		if (Ext.getCmp('cboJKPJ').getValue() === 1)
		{
			jeniskelaminpj = true;
		}
		else
		{
			jeniskelaminpj = false;
		}
	} else {
		jeniskelaminpj = true;
	}
	
	
	
	var tmpstastusmaritapj;
	if(Ext.getCmp('cboStatusMaritalPenanggungjawab').getValue() === '' || Ext.getCmp('cboStatusMaritalPenanggungjawab').getValue() === 'Pilih Status...'){	
		tmpstastusmaritapj = 0;
	} else {
		
		tmpstastusmaritapj = Ext.getCmp('cboStatusMaritalPenanggungjawab').getValue();
	}
	
	var tmpkelurahanpj;
	if(Ext.getCmp('cbokelurahanPenanggungJawab').getValue() === '' || Ext.getCmp('cbokelurahanPenanggungJawab').getValue() === 'Select a Kelurahan...'){	
		tmpkelurahanpj = 0;
	} else {
		tmpkelurahanpj = Ext.getCmp('cbokelurahanPenanggungJawab').getValue();
	}
	
	var tmpkelurahanktppj;
	if(Ext.getCmp('cbokelurahanKtpPenanggungJawab').getValue() === '' || Ext.getCmp('cbokelurahanKtpPenanggungJawab').getValue() === 'Select a Kelurahan...'){	
		tmpkelurahanktppj = 0;
	} else {
		tmpkelurahanktppj = Ext.getCmp('cbokelurahanKtpPenanggungJawab').getValue();
	}
	
	var tmphubungankel;
	if(Ext.getCmp('cboHubunganKeluarga').getValue() === '' || Ext.getCmp('cboHubunganKeluarga').getValue() === 'Pilih...'){	
		tmphubungankel = 14;
	} else {
		tmphubungankel = Ext.getCmp('cboHubunganKeluarga').getValue();
	}
	
	var tmppendidikanpj;
	if(Ext.getCmp('cboPendidikanPenanggungJawab').getValue() === '' || Ext.getCmp('cboPendidikanPenanggungJawab').getValue() === 'Pilih Pendidikan...'){	
		tmppendidikanpj = 9;
	} else {
		tmppendidikanpj = Ext.getCmp('cboPendidikanPenanggungJawab').getValue();
	}
	
	var tmppekerjaaanpj;
	if(Ext.getCmp('cboPekerjaanPenanggungJawabRequestEntry').getValue() === '' || Ext.getCmp('cboPekerjaanPenanggungJawabRequestEntry').getValue() === 'Pilih Pekerjaan...'){	
		tmppekerjaaanpj = 0;
	} else {
		tmppekerjaaanpj = Ext.getCmp('cboPekerjaanPenanggungJawabRequestEntry').getValue();
	}
	
	var tmppekerjaaanpj;
	if(Ext.getCmp('cboPerusahaanPenanggungJawabRequestEntry').getValue() === '' || Ext.getCmp('cboPerusahaanPenanggungJawabRequestEntry').getValue() === 'Pilih Perusahaan...'){	
		tmppekerjaaanpj = 0;
	} else {
		tmppekerjaaanpj = Ext.getCmp('cboPerusahaanPenanggungJawabRequestEntry').getValue();
	}
	
	var tmpkelurahanktp;
	if(Ext.getCmp('cbokelurahanKtp').getValue() === '' || Ext.get('cbokelurahanKtp').getValue() === 'Select Kelurahan...'){	
		tmpkelurahanktp = 0;
	} else {
		tmpkelurahanktp = Ext.getCmp('txtTmpKelurahanKtp_viDaftarIGD').getValue();
	}
	
	var tmpkelurahan;
	if(Ext.getCmp('cbokelurahan').getValue() === '' || Ext.getCmp('cbokelurahan').getValue() === 'Select Kelurahan...'){	
		tmpkelurahan = 0;
	} else {
		tmpkelurahan = Ext.getCmp('txtTmpKelurahan_viDaftarIGD').getValue();
	}	

		var params_ViPendaftaranIGD =
		{
      
                    Table: 'View_DaftarIGD',
                    NoMedrec:  Ext.get('txtNoRequest_viDaftarIGD').getValue(),
                    NamaPasien: Ext.get('txtNama_viDaftarIGD').getValue(),
                    NamaKeluarga: Ext.get('txtNama_viDaftarIGDKeluarga').getValue(),
                    JenisKelamin: jenis_kelamin,
                    Tempatlahir: Ext.get('txtTempatLahir_viDaftarIGD').getValue(),
                    TglLahir : Ext.get('dtpTanggalLahir_viDaftarIGD').getValue(),
                    Agama: Ext.getCmp('cboAgamaRequestEntry').getValue(),
                    GolDarah: Ext.getCmp('cboGolDarah').getValue(),
                    StatusMarita: Ext.getCmp('cboStatusMarital').getValue(),
                    StatusWarga: Ext.getCmp('cbWna').getValue(),
                    Alamat : Ext.get('txtAlamat_viDaftarIGD').getValue(),
                    No_Tlp: Ext.get('txtNotlppasien_viDaftarIGD').getValue(),
                    Handphone: Ext.get('txtNohppasien_viDaftarIGD').getValue(),
                    Email: Ext.get('txtEmailPasien').getValue(),
					
                    
                    Pendidikan: Ext.getCmp('cboPendidikanRequestEntry').getValue(),
                    Pekerjaan: Ext.getCmp('cboPekerjaanRequestEntry').getValue(),
                    NamaPeserta : Ext.get('txtNama_viDaftarIGDPeserta').getValue(),
                    KD_Asuransi : 1,
                    NoAskes : Ext.get('txtNoAskes').getValue(),
                    NoSjp : Ext.get('txtNoSJP').getValue(),
                    Kd_Suku: 0,
                    Jabatan : '',
                    Perusahaan: Ext.getCmp('cboPerusahaanRequestEntry').getValue(),
                    Perusahaan1 : 0,
                    Cek: Ext.get('kelPasien').getValue(),

                    Poli: Ext.getCmp('cboPoliklinikRequestEntry').getValue(),
                    TanggalMasuk : Ext.get('dptTanggal').getValue(),
                    UrutMasuk:0,
                    JamKunjungan : now_viDaftarIGD, //Ext.get('txtJamKunjung').getValue(),
                    CaraPenerimaan: 99,
                    KdRujukan: 0,
                    
                    KdDokter: Ext.getCmp('cboDokterRequestEntry').getValue(),
                    Baru: true,
                    KdCustomer: Ext.getCmp('cboAsuransiIGD').getValue(),
                    Shift: 1,
                    Karyawan: 0,
                    Kontrol: false,
                    Antrian:0,
                    NoSurat:'',
                    Alergi: Ext.get('txtAlergi').getValue(),
                    Anamnese: Ext.get('txtAnamnese').getValue(),

                    TahunLahir : Ext.get('txtThnLahir_viDaftarIGD').getValue(),
                    BulanLahir : Ext.get('txtBlnLahir_viDaftarIGD').getValue(),
                    HariLahir : Ext.get('txtHariLahir_viDaftarIGD').getValue(),
                    //NamaPerujuk : Ext.get('txtNama_viDaftarIGDPerujuk').getValue(),
                    //AlamatPerujuk : Ext.get('txtAlamat_viDaftarIGDPerujuk').getValue(),

                    Kota: Ext.getCmp('cboKabupatenRequestEntry').getValue(),
                    
                    Kd_Kecamatan: Ext.getCmp('cboKecamatanRequestEntry').getValue(),
					//tmpkelurahan cboPropinsiRequestEntry
                    Kelurahan: tmpkelurahan,
                    AsalPasien: Ext.getCmp('cboKabupatenRequestEntry').getValue(),
                    
                    KDPROPINSI : Ext.getCmp('txtTmpPropinsi_viDaftarIGD').getValue(),
                    KDKABUPATEN: Ext.getCmp('txtTmpKabupaten_viDaftarIGD').getValue(),
                    KDKECAMATAN: Ext.getCmp('txtTmpKecamatan_viDaftarIGD').getValue(),
                    
                    KDPENDIDIKAN : Ext.getCmp('txtTmpPendidikan_viDaftarIGD').getValue(),
                    KDPEKERJAAN: Ext.getCmp('txtTmpPekerjaan_viDaftarIGD').getValue(),
                    KDAGAMA: Ext.getCmp('txtTmpAgama_viDaftarIGD').getValue(),  
					
                    KDPOS : Ext.getCmp('txtpos').getValue(),
                    //HANDPHONE : Ext.getCmp('').getValue(),
                    //EMAIL : Ext.getCmp('').getValue(),
                    NAMAAYAH : Ext.getCmp('txtNamaAyah').getValue(),
                    NAMAIBU : Ext.getCmp('txtNamaIbu').getValue(),
                    ALAMATKTP : Ext.getCmp('txtAlamatktp').getValue(),
                    KDPOSKTP : Ext.getCmp('txtposktp').getValue(),
                    KDKELURAHANKTP: tmpkelurahanktp,



                    //penanggung jawab 
                    KDPERUSAHAANPJ : tmppekerjaaanpj,
                    PENDIDIKANPJ : tmppendidikanpj,
                    KDKELURAHANPJ : tmpkelurahanpj,
                    KDPEKERJAANPJ : tmppekerjaaanpj,
                    NAMAPJ : Ext.getCmp('txtNamaPenanggungjawab').getValue(),
                    ALAMATPJ : Ext.getCmp('txtAlamatPenanggungjawab').getValue(),
                    TELEPONPJ : Ext.getCmp('txtTlpPenanggungjawab').getValue(),
                    KDPOSPJ : Ext.getCmp('txtKdPosPenanggungjawab').getValue(),
                    HUBUNGANPJ : tmphubungankel,

                    ALAMATKTPPJ : Ext.getCmp('txtalamatktpPenanggungjawab').getValue(),
                    NOHPPJ : Ext.getCmp('txtHpPenanggungjawab').getValue(),
                    KDPOSKTPPJ : Ext.getCmp('txtKdPosKtpPenanggungjawab').getValue(),
                    NOKTPPJ : Ext.getCmp('txtNoKtpPenanggungjawab').getValue(),
                    KDKELURAHANKTPPJ : tmpkelurahanktppj,

                    EMAILPJ : Ext.getCmp('txtEmailPenanggungjawab').getValue(),
                    STATUSMARITALPJ : tmpstastusmaritapj,
                    WNIPJ : Ext.getCmp('cbWnaPenanggungJawab').getValue(),
                    TEMPATLAHIRPJ :Ext.getCmp('txtTempatPenanggungjawab').getValue(),
                    TGLLAHIRPJ :Ext.getCmp('dtpPenanggungJawabTanggalLahir').getValue(),
                    JKPJ : jeniskelaminpj,
                    NonKunjungan:Ext.getCmp('cbnonkunjunganIGD').getValue(),

					//EMAILPJ : Ext.getCmp('txtTmpPendidikan_viDaftarIGD').getValue(),
		};
    return params_ViPendaftaranIGD
}

function dataparamDelete_viDaftarIGD()
{
		var paramsDelete_ViPendaftaranIGD =
		{
			Table: 'viKunjungan',			
			// KD_KELOMPOK: Ext.getCmp('ComboKelompokDaftarIGD_viDaftarIGD').getValue(),
			// KD_UNIT: Ext.getCmp('ComboPoliDaftarIGD_viDaftarIGD').getValue(),
			// KD_DOKTER: Ext.getCmp('ComboDokterDaftarIGD_viDaftarIGD').getValue(),
			NO_KUNJUNGAN : Ext.get('txtNoKunjungan_viDaftarIGD').getValue(), //Ext.getCmp('txtNoKunjungan_viDaftarIGD').setValue(rowdata.NO_KUNJUNGAN);		
			KD_CUSTOMER: strKD_CUST
			// KD_PASIEN: Ext.get('txtKDPasien_viDaftarIGD').getValue(),
			// TGL_KUNJUNGAN: ShowDate(Ext.get('dtpKunjungan_viDaftarIGD').getValue()),
			// TINGGI_BADAN : Ext.get('txtTinggiBadan_viDaftarIGD').getValue(),
			// BERAT_BADAN : Ext.get('txtBeratBadan_viDaftarIGD').getValue(),
			// TEKANAN_DRH : Ext.get('txtTekananDarah_viDaftarIGD').getValue(),
			// NADI : Ext.get('txtNadi_viDaftarIGD').getValue(),
			// ALERGI :  Ext.get('txtchkAlergi_viDaftarIGD').dom.checked,
			// KELUHAN : Ext.get('txtkeluhan_viDaftarIGD').getValue(),  
			// PASIEN_BARU : Ext.get('txtchkPasienBaru_viDaftarIGD').dom.checked,						
			// KD_PENDIDIKAN : Ext.getCmp('ComboPendidikan_viDaftarIGD').getValue(), //PASIEN
			// KD_STS_MARITAL : Ext.getCmp('ComboSTS_MARITAL_viDaftarIGD').getValue(),
			// KD_AGAMA :  Ext.getCmp('ComboAgama_viDaftarIGD').getValue(),
			// KD_PEKERJAAN : Ext.getCmp('ComboPekerjaan_viDaftarIGD').getValue(),
			// NAMA : Ext.get('txtNama_viDaftarIGDPs_viDaftarIGD').getValue(),
			// TEMPAT_LAHIR : Ext.get('txtTmpLahir_viDaftarIGD').getValue(),
			// TGL_LAHIR : ShowDate(Ext.get('DtpTglLahir_viDaftarIGD').getValue()),
			// JENIS_KELAMIN: Ext.getCmp('ComboJK_viDaftarIGD').getValue(),
			// ALAMAT : Ext.get('txtAlamat_viDaftarIGD_viDaftarIGD').getValue(),
			// NO_TELP : Ext.get('txtNoTlp_viDaftarIGD').getValue(),
			// NO_HP : Ext.get('txtHP_viDaftarIGD').getValue(),
			// GOL_DARAH : Ext.getCmp('ComboGolDRH_viDaftarIGD').getValue(),
		
		}
	
    return paramsDelete_ViPendaftaranIGD
}
//============================================ Grid Data ======================================

//-------------------------------------------- Hapus baris -------------------------------------
function HapusBarisNgajar(nBaris)
{
	if (CurrentData_viDaftarIGD.row >= 0
	/*SELECTDATASTUDILANJUT.data.NO_SURAT_STLNJ != '' ||  SELECTDATASTUDILANJUT.data.TGL_MULAI_SURAT != '' ||
		SELECTDATASTUDILANJUT.data.TGL_AKHIR_SURAT != '' || SELECTDATASTUDILANJUT.data.ID_DANA != '' ||
		SELECTDATASTUDILANJUT.data.SURAT_DARI != '' || SELECTDATASTUDILANJUT.data.TGL_SURAT != '' || SELECTDATASTUDILANJUT.data.THN_PERKIRAAN_LULUS != ''*/) 
		{
			Ext.Msg.show
			(
				{
					title: 'Hapus Baris',
					msg: 'Apakah baris ini akan dihapus ?',
					buttons: Ext.MessageBox.YESNO,
					fn: function(btn) 
					{
						if (btn == 'yes') 
						{
							DataDeletebaris_viDaftarIGD()
							dsDetailSL_viDaftarIGD.removeAt(CurrentData_viDaftarIGD.row);
							SELECTDATASTUDILANJUT = undefined;
						}
					},
					icon: Ext.MessageBox.QUESTION
				}
			);
		}
	else 
		{
			dsDetailSL_viDaftarIGD.removeAt(CurrentData_viDaftarIGD.row);
			SELECTDATASTUDILANJUT = undefined;
		}
}

function DataDeletebaris_viDaftarIGD() 
{
    Ext.Ajax.request
	({url: "./Datapool.mvc/DeleteDataObj",
		params: dataparam_viDaftarIGD(),
		success: function(o) 
		{
			var cst = o.responseText;
			if (cst == '{"success":true}') 
			{
				ShowPesanInfo_viDaftarIGD('Data berhasil dihapus','Hapus Data');                
			}
			else
			{ 
				ShowPesanInfo_viDaftarIGD('Data gagal dihapus','Hapus Data'); 
			}
		}
	})       
};

var mRecord = Ext.data.Record.create
(
	[
		'NIP', 
		'ID_STUDI_LANJUT', 
		'NO_SURAT_STLNJ', 
		'TGL_MULAI_SURAT', 
		'TGL_AKHIR_SURAT', 
		'ID_DANA', 
		'SURAT_DARI', 
		'TGL_SURAT', 
		'THN_PERKIRAAN_LULUS'  
	]
);
//-------------------------------- end hapus kolom -----------------------------
//---------------------- Split row -------------------------------------
function getArrDetail_viDaftarIGD() 
{
    var x = '';
    for (var i = 0; i < dsDetailSL_viDaftarIGD.getCount(); i++) 
	{
        var y = '';
        var z = '@@##$$@@';
        
        y += 'NIP=' + Ext.get('txtNPP_viDaftarIGD').getValue()
        y += z + 'ID_STUDI_LANJUT=' + Ext.get('txtID_viDaftarIGD').getValue()
        y += z + 'NO_SURAT_STLNJ=' + dsDetailSL_viDaftarIGD.data.items[i].data.NO_SURAT_STLNJ
		
		/*if (dsDetailSL_viDaftarIGD.data.items[i].data.TGL_MULAI_SURAT.length == 8) 
		{	
			
			y += z + 'TGL_MULAI_SURAT=' + ShowDate(dsDetailSL_viDaftarIGD.data.items[i].data.TGL_MULAI_SURAT)
		}
		else
		{
		
			y += z + 'TGL_MULAI_SURAT=' + ShowDate(dsDetailSL_viDaftarIGD.data.items[i].data.TGL_MULAI_SURAT)
		}
		
		if (dsDetailSL_viDaftarIGD.data.items[i].data.TGL_AKHIR_SURAT.length == 8) 
		{		
			y += z + 'TGL_AKHIR_SURAT=' + ShowDate(dsDetailSL_viDaftarIGD.data.items[i].data.TGL_AKHIR_SURAT)
		}
		else
		{
			y += z + 'TGL_AKHIR_SURAT=' + ShowDate(dsDetailSL_viDaftarIGD.data.items[i].data.TGL_AKHIR_SURAT)
		}*/
		y += z + 'TGL_MULAI_SURAT=' + ShowDate(dsDetailSL_viDaftarIGD.data.items[i].data.TGL_MULAI_SURAT)
		y += z + 'TGL_AKHIR_SURAT=' + ShowDate(dsDetailSL_viDaftarIGD.data.items[i].data.TGL_AKHIR_SURAT)
        y += z + 'ID_DANA=' + dsDetailSL_viDaftarIGD.data.items[i].data.ID_DANA
        y += z + 'SURAT_DARI=' + dsDetailSL_viDaftarIGD.data.items[i].data.SURAT_DARI
        //alert(dsDetailSL_viDaftarIGD.data.items[i].data.ID_DANA)
		
		/*if (dsDetailSL_viDaftarIGD.data.items[i].data.TGL_SURAT.length == 8) 
		{		
			y += z + 'TGL_SURAT=' + dsDetailSL_viDaftarIGD.data.items[i].data.TGL_SURAT
		}
		else
		{
			y += z + 'TGL_SURAT=' + ShowDate(dsDetailSL_viDaftarIGD.data.items[i].data.TGL_SURAT)
		}*/
		
		y += z + 'TGL_SURAT=' + ShowDate(dsDetailSL_viDaftarIGD.data.items[i].data.TGL_SURAT)
		
		y += z + 'THN_PERKIRAAN_LULUS=' + dsDetailSL_viDaftarIGD.data.items[i].data.THN_PERKIRAAN_LULUS
		
        if (i === (dsDetailSL_viDaftarIGD.getCount() - 1)) 
		{
            x += y
        }
        else {
            x += y + '##[[]]##'
        }
    }
    return x;
};
//---------------------------- end Split row ------------------------------
function DatarefreshDetailSL_viDaftarIGD(rowdataaparam)
{
    dsDetailSL_viDaftarIGD.load
    (
		{
			params:
			{
				Skip: 0,
				Take: 50,
				Sort: '',
				Sortdir: 'ASC',
				target:'viviewDetailStudiLanjut',
				param: rowdataaparam
			}
		}
    );
    return dsDetailSL_viDaftarIGD;
}

function GetPasienDaftarIGD(strCari)
{
	Ext.Ajax.request
	 (
		{
            url: "./Module.mvc/ExecProc",
            params: 
			{
                UserID: 'Admin',
                ModuleID: 'getKunjunganDataPasien',
				Params:	getParamKunjunganKdPasien(strCari)
            },
            success: function(o) 
			{
				
                var cst = Ext.decode(o.responseText);

				if (cst.success === true) 
				{					
//					Ext.getCmp('txtKDPasien_viDaftarIGD').setValue(cst.KD_PASIEN);
//					Ext.getCmp('txtNama_viDaftarIGDPs_viDaftarIGD').setValue(cst.NAMA);
//					Ext.get('txtchkPasienBaru_viDaftarIGD').dom.checked = 0;
//					Ext.getCmp('txtTmpLahir_viDaftarIGD').setValue(cst.TEMPAT_LAHIR);
//					Ext.getCmp('DtpTglLahir_viDaftarIGD').setValue(ShowDate(cst.TGL_LAHIR));
//					Ext.getCmp('txtUmurThn_viDaftarIGD').setValue(cst.TAHUN);
//					Ext.getCmp('txtUmurBln_viDaftarIGD').setValue(cst.BULAN);
//					Ext.getCmp('txtUmurHari_viDaftarIGD').setValue(cst.HARI);
//					Ext.getCmp('ComboJK_viDaftarIGD').setValue(cst.JENIS_KELAMIN);
//					Ext.getCmp('ComboGolDRH_viDaftarIGD').setValue(cst.GOL_DARAH);
//					Ext.getCmp('txtNoTlp_viDaftarIGD').setValue(cst.NO_TELP);
//					Ext.getCmp('txtHP_viDaftarIGD').setValue(cst.NO_HP);
//					Ext.getCmp('txtAlamat_viDaftarIGD_viDaftarIGD').setValue(cst.ALAMAT);
//					Ext.getCmp('ComboPoliDaftarIGD_viDaftarIGD').setValue(cst.KD_UNIT);
//					Ext.getCmp('ComboDokterDaftarIGD_viDaftarIGD').setValue(cst.KD_DOKTER);
//					Ext.getCmp('ComboKelompokDaftarIGD_viDaftarIGD').setValue(cst.KD_KELOMPOK);
//					// Ext.getCmp('dtpKunjungan_viDaftarIGD').setValue(ShowDate(cst.TGL_KUNJUNGAN));
//					Ext.getCmp('txtNadi_viDaftarIGD').setValue(cst.NADI);
//					Ext.getCmp('txtchkAlergi_viDaftarIGD').setValue(cst.ALERGI);
//					Ext.getCmp('txtTinggiBadan_viDaftarIGD').setValue(cst.TINGGI_BADAN);
//					Ext.getCmp('txtBeratBadan_viDaftarIGD').setValue(cst.BERAT_BADAN);
//					Ext.getCmp('txtTekananDarah_viDaftarIGD').setValue(cst.TEKANAN_DARAH);
//					Ext.getCmp('txtkeluhan_viDaftarIGD').setValue(cst.KELUHAN);
//					Ext.getCmp('ComboSTS_MARITAL_viDaftarIGD').setValue(cst.KD_STS_MARITAL);
//					Ext.getCmp('ComboPekerjaan_viDaftarIGD').setValue(cst.KD_PEKERJAAN);
//					Ext.getCmp('ComboAgama_viDaftarIGD').setValue(cst.KD_AGAMA);
//					Ext.getCmp('ComboPendidikan_viDaftarIGD').setValue(cst.KD_PENDIDIKAN);
					
					// Ext.getCmp('ComboPoliDaftarIGD_viDaftarIGD').setValue(rowdata.KD_UNIT);
					// Ext.get('ComboJK_viDaftarIGD').dom.value = rowdata.JNS_KELAMIN;	
					// Ext.get('ComboPoliDaftarIGD_viDaftarIGD').dom.value = rowdata.NAMA_UNIT;
					// Ext.get('ComboDokterDaftarIGD_viDaftarIGD').dom.value = rowdata.DOKTER;
					// Ext.get('ComboKelompokDaftarIGD_viDaftarIGD').dom.value = rowdata.KELOMPOK;
					// Ext.get('ComboSTS_MARITAL_viDaftarIGD').dom.value = rowdata.STS_MARITAL;
					// Ext.get('ComboPekerjaan_viDaftarIGD').dom.value = rowdata.PEKERJAAN;
					// Ext.get('ComboAgama_viDaftarIGD').dom.value = rowdata.AGAMA;
					// Ext.get('ComboPendidikan_viDaftarIGD').dom.value = rowdata.PENDIDIKAN;
				}
				else
				{
					ShowPesanWarning_viDaftarIGD('No.Medrec tidak di temukan '  + cst.pesan,'Informasi');
					// Ext.get('txtNama_viDaftarIGDKRSMahasiswa').dom.value = '';								
					// Ext.get('txtFakJurKRSMahasiswa').dom.value = '';					
					// Ext.get('txtKdJurKRSMahasiswa').dom.value = '';					
					// Ext.get('txtBatasStudyKRSMahasiswa').dom.value = '';					
					// Ext.get('txtBarcode').dom.focus();

					// var criteria = ' WHERE NIM IS NOT NULL AND J.KD_JURUSAN IN ' + strKD_JURUSAN + ' ';
						// if (Ext.get('txtNIMKRSMahasiswa').dom.value != '')
						// {
							// criteria += ' AND NIM like ~%' + Ext.get('txtNIMKRSMahasiswa').dom.value + '%~';
							
						// };
						// FormLookupMahasiswa('txtNIMKRSMahasiswa','txtNama_viDaftarIGDKRSMahasiswa','txtFakJurKRSMahasiswa','txtBatasStudyKRSMahasiswa','txtIPSKRSMahasiswa','','','','txtKdJurKRSMahasiswa',criteria);
				};
            }

        }
	);
}

function getParamKunjunganKdPasien(kdCari)
{					
	var strKriteria = "";	
	var x ="x";
		
	
	if (kdCari == 1)
	{	
		if(Ext.get('txtKDPasien_viDaftarIGD').getValue() != '')
		{
			strKriteria += Ext.get('txtKDPasien_viDaftarIGD').getValue() + "##";							
		}	
	}
	else if(kdCari == 2) 
	{
		if(Ext.get('txtNoTlp_viDaftarIGD').getValue() != '')
		{
			strKriteria += Ext.get('txtNoTlp_viDaftarIGD').getValue() + "##";							
		}	
	}else
	{
		if(Ext.get('txtHP_viDaftarIGD').getValue() != '')
		{
			strKriteria += Ext.get('txtHP_viDaftarIGD').getValue() + "##";							
		}	
	};
	strKriteria += strKD_CUST + "##"
	strKriteria += kdCari + "##"
	
	return strKriteria;
}

function getKriteriaCariLookup()
{					
	var strKriteria = "";	
		
	//strKriteria=" WHERE RIGHT(PS.KD_PASIEN,10) like '%" + Ext.get('txtKDPasien_viDaftarIGD').dom.value + "%' and ps.kd_customer='" + strKD_CUST +"' "	
	
	if(Ext.get('txtKDPasien_viDaftarIGD').getValue() != '')
	{
		strKriteria += " WHERE RIGHT(x.KD_PASIEN,10) like '%" + Ext.get('txtKDPasien_viDaftarIGD').dom.value + "%'"
	}	
	
	if(Ext.get('txtNama_viDaftarIGDPs_viDaftarIGD').getValue() != '')	
	{	
		if (strKriteria != "")	
		{
			strKriteria += " AND x.NAMA like '%" + Ext.get('txtNama_viDaftarIGDPs_viDaftarIGD').dom.value + "%'"
		}
		else
		{
			strKriteria += " WHERE x.NAMA like '%" + Ext.get('txtNama_viDaftarIGDPs_viDaftarIGD').dom.value + "%'"
		}
	}
	
	// if(Ext.get('txtNoTlp_viDaftarIGD').getValue() != '')	
	// {	
		// if (strKriteria != "")	
		// {
			// strKriteria += " OR PS.NO_TELP like '%" + Ext.get('txtNoTlp_viDaftarIGD').dom.value + "%'"
		// }
		// else
		// {
			// strKriteria += " WHERE PS.NO_TELP like '%" + Ext.get('txtNoTlp_viDaftarIGD').dom.value + "%'"
		// }
	// }
	
	// if(Ext.get('txtHP_viDaftarIGD').getValue() != '')	
	// {	
		// if (strKriteria != "")	
		// {
			// strKriteria += " OR PS.NO_HP like '%" + Ext.get('txtHP_viDaftarIGD').dom.value + "%'"
		// }
		// else
		// {
			// strKriteria += " WHERE PS.NO_HP like '%" + Ext.get('txtHP_viDaftarIGD').dom.value + "%'"
		// }
	// }
	
	if(Ext.get('txtAlamat_viDaftarIGD_viDaftarIGD').getValue() != '')	
	{	
		if (strKriteria != "")	
		{
			strKriteria += " AND x.ALAMAT like '%" + Ext.get('txtAlamat_viDaftarIGD_viDaftarIGD').dom.value + "%'"
		}
		else
		{
			strKriteria += " WHERE x.ALAMAT like '%" + Ext.get('txtAlamat_viDaftarIGD_viDaftarIGD').dom.value + "%'"
		}
	}

	if (strKriteria != "")	
	{
		strKriteria += " AND x.KD_CUSTOMER = '" + strKD_CUST + "'"
	}
	else
	{
		strKriteria += " WHERE x.KD_CUSTOMER = '" + strKD_CUST + "'"
	}		
	
	return strKriteria;
}

function dataprint_viDaftarIGD()
{
    var params_ViPendaftaranIGD =
    {

        Table: 'ViewPrintBill',

        No_TRans: tmpnotransaksi,
        KdKasir : tmpkdkasir
//        NoMedrec:  Ext.get('txtNoRequest_viDaftarIGD').getValue(),
//        NamaPasien: Ext.get('txtNama_viDaftarIGD').getValue(),
//        TglLahir : Ext.get('dtpTanggalLahir_viDaftarIGD').getValue(),
//        Alamat : Ext.get('txtAlamat_viDaftarIGD').getValue(),
//
//        Poli: Ext.getCmp('cboPoliklinikRequestEntry').getValue(),
//        TanggalMasuk : Ext.get('dptTanggal').getValue(),
//        KdDokter: Ext.getCmp('cboDokterRequestEntry').getValue(),
//        KdCustomer: Ext.getCmp('cboAsuransiIGD').getValue()

    };
    return params_ViPendaftaranIGD
}

function GetPrintKartu()
{
    Ext.Ajax.request
    (
        {
            url: baseURL + "index.php/main/LaporanDataObj",
            params: dataprint_viDaftarIGD(),
            success: function(o) 
                        {

                var cst = Ext.decode(o.responseText);

                                if (cst.success === true) 
                                {
                                }
                                else
                                {				

                                };
                }

        }
    );
}

function mComboJK()
{
    var cboJK = new Ext.form.ComboBox
	(
		{
			id:'cboJK',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Pilih Jenis Kelamin...',
			fieldLabel: 'Jen. Kelamin ',
			anchor :'99%',
			tabIndex:5,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Laki - Laki'], [2, 'Perempuan']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetJK,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetJK=b.data.displayText ;
				},
				'render': function(c) {
					c.getEl().on('keypress', function(e) {
						if(e.getKey() == 13) //atau Ext.EventObject.ENTER
						Ext.getCmp('cboStatusMarital').focus();
					}, c);
				}
			}
		}
	);
	return cboJK;
};

function mComboWargaNegara()
{
    var cboWarga = new Ext.form.ComboBox
	(
		{
			id:'cboWarga',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih...',
                        selectOnFocus:true,
                        forceSelection: true,
			fieldLabel: 'Warga Negara ',
			width:100,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'WNI'], [2, 'WNA']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetWarga,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetWarga=b.data.displayText ;
                                        GetDataWargaNegara(b.data.displayText)
                                        //alert(StatusWargaNegara)
				},
                                'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('cboStatusMarital').focus();
                                    }, c);
                                }
			}
		}
	);
	return cboWarga;
};

var StatusWargaNegara
function GetDataWargaNegara(warga)
{
    var tampung = warga
    if (tampung === 'WNI')
    {
        StatusWargaNegara = 'true'
    }
    else
        {
            StatusWargaNegara = 'false'
        }
}

function mComboGolDarah()
{
    var cboGolDarah = new Ext.form.ComboBox
	(
		{
			id:'cboGolDarah',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			//allowBlank: false,
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Pilih Gol. Darah...',
			fieldLabel: 'G.Darah ',
			width:50,
			tabIndex:4,
			anchor: '99%',
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, '-'], [2, 'A+'],[3, 'B+'], [4, 'AB+'],[5, 'O+'], [6, 'A-'], [7, 'B-']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetGolDarah,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetGolDarah=b.data.displayText ;
				},
				'render': function(c)
				{
					c.getEl().on('keypress', function(e) {
						if(e.getKey() == 13) //atau Ext.EventObject.ENTER
						Ext.getCmp('cboJK').focus();
					}, c);
				}
			}
		}
	);
	return cboGolDarah;
};

function mComboPerseorangan()
{
    var cboPerseorangan = new Ext.form.ComboBox
	(
		{
			id:'cboPerseorangan',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: 'Jenis',
			width:50,
			anchor: '95%',
			value:1,
			tabIndex:30,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Umum']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetPerseorangan,
			listeners:
			{
				'select': function(a,b,c)
				{
                                  selectSetPerseorangan=b.data.displayText ;
				}
			}
		}
	);
	return cboPerseorangan;
};

function mComboSatusMarital()
{
    var cboStatusMarital = new Ext.form.ComboBox
	(
		{
			id:'cboStatusMarital',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Pilih Status...',
			fieldLabel: 'Status Marital ',
			width:110,
			tabIndex:6,
			anchor: '95%',
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[0, 'Blm Kawin'], [1, 'Kawin'],[2, 'Janda'], [3, 'Duda']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetSatusMarital,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetSatusMarital=b.data.displayText ;
				},
                                'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('txtTempatLahir_viDaftarIGD').focus();
                                    }, c);
                                }
			}
		}
	);
	return cboStatusMarital;
};

function mComboNamaRujukan()
{
    var cboNamaRujukan = new Ext.form.ComboBox
	(
		{
			id:'cboNamaRujukan',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Pilih Rujukan...',
			fieldLabel: 'Nama ',
			width:110,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
                                       data: [[1, ''], [2, ''],[3, ''],[4, '']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetNamaRujukan,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetNamaRujukan=b.data.displayText ;
				}
			}
		}
	);
	return cboNamaRujukan;
};

function mComboPoli_viDaftarIGD(lebar,Nama_ID)
{
    var Field_poli_viDaftarIGD = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftarIGD = new WebApp.DataStore({fields: Field_poli_viDaftarIGD});

	ds_Poli_viDaftarIGD.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ViewSetupUnit',
                param: "kd_bagian=2 and type_unit=false"
            }
        }
    )

    var cbo_Poli_viDaftarIGD = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'Poli',
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
            store: ds_Poli_viDaftarIGD,
            width: lebar,
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Poli...',
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            name: Nama_ID,
            lazyRender: true,
            id: 'cboPoliviDaftarIGD',
            listeners:
			{
				'select': function (a,b,c)
				{
				}
			}
        }
    )

    return cbo_Poli_viDaftarIGD;
}

function mComboAgamaRequestEntry()
{
    var Field = ['KD_AGAMA','AGAMA'];

    dsAgamaRequestEntry = new WebApp.DataStore({fields: Field});
    dsAgamaRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: 'kd_agama',
			    Sortdir: 'ASC',
			    target: 'ViewComboAgama',
			    param: ''
			}
		}
	)

    var cboAgamaRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboAgamaRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Agama...',
		    fieldLabel: 'Agama',
		    align: 'Right',
			tabIndex:3,
//		    anchor:'60%',
		    store: dsAgamaRequestEntry,
		    valueField: 'KD_AGAMA',
		    displayField: 'AGAMA',
                    anchor: '99%',
		    listeners:
			{
			    'select': function(a, b, c)
				{
                                    selectAgamaRequestEntry = b.data.KD_AGAMA;
                                },
                            'render': function(c)
							{
								c.getEl().on('keypress', function(e) {
									if(e.getKey() == 13) //atau Ext.EventObject.ENTER
									Ext.getCmp('cboGolDarah').focus();
								}, c);
							}
			}
                }
	);

    return cboAgamaRequestEntry;
};

function mComboPropinsiRequestEntry()
{
     var Field = ['KD_PROPINSI','PROPINSI'];

    dsPropinsiRequestEntry = new WebApp.DataStore({fields: Field});
    dsPropinsiRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: 'kd_propinsi',
			    Sortdir: 'ASC',
			    target: 'ViewComboPropinsi',
			    param: ''
			}
		}
	)

    var cboPropinsiRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboPropinsiRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
			forceSelection: true,
			emptyText:'Select a Propinsi...',
			selectOnFocus:true,
			 //editable: false,
		    //emptyText: ' ',
		    fieldLabel: 'Propinsi',
		    align: 'Right',
//		    anchor:'60%',
		    store: dsPropinsiRequestEntry,
		    valueField: 'KD_PROPINSI',
		    displayField: 'PROPINSI',
			anchor: '99%',
			tabIndex:15,
		    listeners:
			{
			    'select': function(a, b, c)
				{
                                   //alert(b.data.KD_PROPINSI)
                                   selectPropinsiRequestEntry = b.data.KD_PROPINSI;

                                   loaddatastorekabupaten(b.data.KD_PROPINSI);
                                },
								'render': function(c) {
                                                c.getEl().on('keypress', function(e) {
                                                if(e.getKey() == 13 ) //atau Ext.EventObject.ENTER
												{
                                                Ext.getCmp('cboKabupatenRequestEntry').focus();
												}
												else if (e.getKey() == 9 ) //atau Ext.EventObject.ENTER
												{
												dataKabupaten = c.value;
                                                loaddatastorekabupaten(dataKabupaten);
                                                }
												}, c);
                                            }
                           /* 'render': function(c) {
                                                c.getEl().on('keypress', function(e) {
                                                if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                Ext.getCmp('cboKabupatenRequestEntry').focus();
                                                }, c);
                                            }*/

			}
                }
	);
        return cboPropinsiRequestEntry;
        
    
};

function loaddatastorekabupaten(kd_propinsi)
{
          dsKabupatenRequestEntry.load
                (
                    {
                     params:
			{
                            Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
                            Sort: 'kd_propinsi',
			    Sortdir: 'ASC',
			    target: 'ViewComboKabupaten',
			    param: 'kd_propinsi='+ kd_propinsi
			}
                    }
                )
}

function mComboKabupatenRequestEntry()
{
      var Field = ['KD_KABUPATEN','KD_PROPINSI','KABUPATEN'];
      dsKabupatenRequestEntry = new WebApp.DataStore({fields: Field});

      var cboKabupatenRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboKabupatenRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Select a Kabupaten...',
		    fieldLabel: 'Kab/Kod',
		    align: 'Right',
		    store: dsKabupatenRequestEntry,
		    valueField: 'KD_KABUPATEN',
		    displayField: 'KABUPATEN',
			anchor: '99%',
			tabIndex:16,
		    listeners:
			{
			    'select': function(a, b, c)
				{
					selectKabupatenRequestEntry = b.data.KD_KABUPATEN;

					loaddatastorekecamatan(b.data.KD_KABUPATEN);
				},
				'render': function(c) {
								c.getEl().on('keypress', function(e) {
									if(e.getKey() == 13){ //atau Ext.EventObject.ENTER
										Ext.getCmp('cboKecamatanRequestEntry').focus();
									} else if (e.getKey() == 9 ) {
										var dataKecamatan = c.value;
										loaddatastorekecamatan(dataKecamatan);
									}
								}, c);
							}
				/* 'render':function(c)
				{
				   c.getEl().on('keypress', function(e) {
							   if (e.getKey() == 9 ) //atau Ext.EventObject.ENTER
								{
								var dataKecamatan = c.value;
								loaddatastorekecamatan(dataKecamatan);
								}
								}, c);
				} */
                                
			}
		}
	);
    return cboKabupatenRequestEntry;
};

function loaddatastorekecamatan(kd_kabupaten)
{
    dsKecamatanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'kd_kabupaten',
			    Sortdir: 'ASC',
			    target: 'ViewComboKecamatan',
			    param: 'kd_kabupaten='+ kd_kabupaten
			}
		}
	)
}

function mComboKecamatanRequestEntry()
{
    var Field = ['KD_KECAMATAN','KD_KABUPATEN','KECAMATAN'];
    dsKecamatanRequestEntry = new WebApp.DataStore({fields: Field});
 
    var cboKecamatanRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboKecamatanRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Select a Kecamatan...',
			fieldLabel: 'Kecamatan',
		    align: 'Right',
//		    anchor:'60%',
		    store: dsKecamatanRequestEntry,
		    valueField: 'KD_KECAMATAN',
		    displayField: 'KECAMATAN',
			tabIndex:17,
			anchor: '99%',
		    listeners:
			{
			    'select': function(a, b, c)
				{
			        selectKecamatanRequestEntry = b.data.KD_KECAMATAN;
					loaddatastorekelurahan(b.data.KD_KECAMATAN);
				},
				'render': function(c) {
								c.getEl().on('keypress', function(e) {
									if(e.getKey() == 13) //atau Ext.EventObject.ENTER
									Ext.getCmp('cbokelurahan').focus();
								}, c);
							}
			}
                }
	);

    return cboKecamatanRequestEntry;
};

function mComboPendidikanRequestEntry()
{
    var Field = ['KD_PENDIDIKAN','PENDIDIKAN'];

    dsPendidikanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPendidikanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: 'kd_pendidikan',
			    Sortdir: 'ASC',
			    target: 'ViewComboPendidikan',
			    param: ''
			}
		}
	)

    var cboPendidikanRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboPendidikanRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Pendidikan...',
		    fieldLabel: 'Pendidikan',
		    align: 'Right',
//		    anchor:'60%',
		    store: dsPendidikanRequestEntry,
		    valueField: 'KD_PENDIDIKAN',
		    displayField: 'PENDIDIKAN',
			anchor: '95%',
			tabIndex:12,
		    listeners:
			{
			    'select': function(a, b, c)
				{
			        var selectPendidikanRequestEntry = b.data.KD_PROPINSI;
                                },
                                'render': function(c) {
                                                c.getEl().on('keypress', function(e) {
													if(e.getKey() == 13) //atau Ext.EventObject.ENTER
													Ext.getCmp('cboPekerjaanRequestEntry').focus();
                                                }, c);
                                            }
			}
		}
	);

    return cboPendidikanRequestEntry;
};

function mComboAsuransi()
{
var Field_poli_viDaftarIGD = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftarIGD = new WebApp.DataStore({fields: Field_poli_viDaftarIGD});

	ds_customer_viDaftarIGD.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboCustomer',
                param: "status=true"
            }
        }
    )
     cboAsuransiIGD = new Ext.form.ComboBox
	(
		{
			id:'cboAsuransiIGD',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Pilih Asuransi...',
			fieldLabel: 'Asuransi',
			align: 'Right',
			anchor: '95%',
			tabIndex:30,
			store: ds_customer_viDaftarIGD,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetAsuransi=b.data.displayText ;
				}
			}
		}
	);
	return cboAsuransiIGD;
};

function mComboPekerjaanRequestEntry()
{
    var Field = ['KD_PEKERJAAN','PEKERJAAN'];

    dsPekerjaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPekerjaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: 'kd_pekerjaan',
			    Sortdir: 'ASC',
			    target: 'ViewComboPekerjaan',
			    param: ''
			}
		}
	)

    var cboPekerjaanRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboPekerjaanRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Pekerjaan...',
		    fieldLabel: 'Pekerjaan',
		    align: 'Right',
//		    anchor:'60%',
		    store: dsPekerjaanRequestEntry,
		    valueField: 'KD_PEKERJAAN',
		    displayField: 'PEKERJAAN',
			anchor: '95%',
			tabIndex:13,
		    listeners:
			{
			    'select': function(a, b, c)
				{
			        var selectPekerjaanRequestEntry = b.data.KD_PROPINSI;
				},
				'render': function(c) {
								c.getEl().on('keypress', function(e) {
									if(e.getKey() == 13) //atau Ext.EventObject.ENTER
									Ext.getCmp('txtAlamat_viDaftarIGD').focus();
								}, c);
							}
			}
		}
	);

    return cboPekerjaanRequestEntry;
};
function mCombokelurahan()
{
    var Field = ['KD_KELURAHAN','KD_KECAMATAN','KELURAHAN'];
    dsKelurahan = new WebApp.DataStore({fields: Field});
 
    var cbokelurahan = new Ext.form.ComboBox
	(
		{
		    id: 'cbokelurahan',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
            forceSelection: true,
		    emptyText:'Pilih Kelurahan...',
            fieldLabel: 'Kelurahan',
		    align: 'Right',
			tabIndex:19,
//		    anchor:'60%',
		    store: dsKelurahan,
		    valueField: 'KD_KELURAHAN',
		    displayField: 'KELURAHAN',
			anchor: '99%',
		    listeners:
						{
							'select': function(a, b, c)
							{
								selectKecamatanRequestEntry = b.data.KD_KECAMATAN;
								Ext.getCmp('txtTmpKelurahan_viDaftarIGD').setValue(b.data.KD_KECAMATAN);
							},
								'render': function(c) {
									c.getEl().on('keypress', function(e) {
										if(e.getKey() == 13) //atau Ext.EventObject.ENTER
										Ext.getCmp('txtpos').focus();
									}, c);
								}
						}
        }
	);

    return cbokelurahan;
};

function loaddatastoreKtpkabupaten(kd_propinsi)
{
        dsKabupatenKtp.load
		(
			{
				params:
					{
						Skip: 0,
						Take: 1000,
						//Sort: 'DEPT_ID',
						Sort: 'kd_propinsi',
						Sortdir: 'ASC',
						target: 'ViewComboKabupaten',
						param: 'kd_propinsi='+ kd_propinsi
					}
			}
		)
}

function mComboPropinsiKtp()
{
     var Field = ['KD_PROPINSI','PROPINSI'];

    dsPropinsiRequestEntry = new WebApp.DataStore({fields: Field});
    dsPropinsiRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: 'kd_propinsi',
			    Sortdir: 'ASC',
			    target: 'ViewComboPropinsi',
			    param: ''
			}
		}
	)

    var cboPropinsiKtp = new Ext.form.ComboBox
	(
		{
		    id: 'cboPropinsiKtp',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
			forceSelection: true,
			emptyText:'Select a Propinsi...',
			selectOnFocus:true,
			 //editable: false,
		    //emptyText: ' ',
		    fieldLabel: 'Propinsi Ktp',
		    align: 'Right',
//		    anchor:'60%',
		    store: dsPropinsiRequestEntry,
		    valueField: 'KD_PROPINSI',
		    displayField: 'PROPINSI',
            anchor: '99%',
			tabIndex:22,
		    listeners:
			{
			    'select': function(a, b, c)
				{							  
					   selectPropinsiRequestEntry = b.data.KD_PROPINSI;

					   loaddatastoreKtpkabupaten(b.data.KD_PROPINSI);
				},
				'render': function(c) {
									c.getEl().on('keypress', function(e) {
									if(e.getKey() == 13 ) //atau Ext.EventObject.ENTER
									{
									 Ext.getCmp('cboKtpKabupaten').focus();
									}
									else if (e.getKey() == 9 ) //atau Ext.EventObject.ENTER
									{
									dataKabupaten = c.value;
									loaddatastoreKtpkabupaten(dataKabupaten);
									}
									}, c);
								}

			}
                }
	);
        return cboPropinsiKtp;
        
    
};

function loaddatastoreKtpkecamatan(kd_kabupaten)
{
    dsKecamatanKtp.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'kd_kabupaten',
			    Sortdir: 'ASC',
			    target: 'ViewComboKecamatan',
			    param: 'kd_kabupaten='+ kd_kabupaten
			}
		}
	)
}

function mComboKtpKabupaten()
{
	var Field = ['KD_KABUPATEN','KD_PROPINSI','KABUPATEN'];
	dsKabupatenKtp = new WebApp.DataStore({fields: Field});

    var cboKtpKabupaten = new Ext.form.ComboBox
	(
		{
		    id: 'cboKtpKabupaten',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Select a Kabupaten...',
		    fieldLabel: 'Kab/Kod Ktp',
		    align: 'Right',
            tabIndex:23,        
		    store: dsKabupatenKtp,
		    valueField: 'KD_KABUPATEN',
		    displayField: 'KABUPATEN',
			anchor: '99%',
		    listeners:
			{
			    'select': function(a, b, c)
				{
					selectKabupatenRequestEntry = b.data.KD_KABUPATEN;

					loaddatastoreKtpkecamatan(b.data.KD_KABUPATEN);
				},
				
				'render':function(c)
				{
				   c.getEl().on('keypress', function(e) {
								if(e.getKey() == 13 ) //atau Ext.EventObject.ENTER
									{
										Ext.getCmp('cboKecamatanKtp').focus();
									}
									else if (e.getKey() == 9 ) //atau Ext.EventObject.ENTER
									{
										var dataKecamatan = c.value;
										loaddatastoreKtpkecamatan(dataKecamatan);
									}
								}, c);
				}
                                
			}
		}
	);
    return cboKtpKabupaten;
};

function loaddatastoreKtpkelurahan(kd_kecamatan)
{
    dsKtpKelurahan.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'kd_kecamatan',
			    Sortdir: 'ASC',
			    target: 'ViewComboKelurahan',
			    param: 'kd_kecamatan='+ kd_kecamatan
			}
		}
	)
}

function mComboKecamatanktp()
{
    var Field = ['KD_KECAMATAN','KD_KABUPATEN','KECAMATAN'];
    dsKecamatanKtp = new WebApp.DataStore({fields: Field});
 
    var cboKecamatanKtp = new Ext.form.ComboBox
	(
		{
		    id: 'cboKecamatanKtp',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
            forceSelection: true,
		    emptyText:'Select a Kecamatan...',
			fieldLabel: 'Kecamatan',
		    align: 'Right',
			tabIndex:24,
//		    anchor:'60%',
		    store: dsKecamatanKtp,
		    valueField: 'KD_KECAMATAN',
		    displayField: 'KECAMATAN',
			anchor: '99%',
		    listeners:
						{
							'select': function(a, b, c)
							{
								selectKecamatanRequestEntry = b.data.KD_KECAMATAN;
								loaddatastoreKtpkelurahan(b.data.KD_KECAMATAN)
											},
											'render': function(c) {
															c.getEl().on('keypress', function(e) {
															if(e.getKey() == 13) //atau Ext.EventObject.ENTER
															Ext.getCmp('cbokelurahanKtp').focus();
															}, c);
														}
						}
        }
	);

    return cboKecamatanKtp;
};

function mComboKtpkelurahan()
{
    var Field = ['KD_KELURAHAN','KD_KECAMATAN','KELURAHAN'];
    dsKtpKelurahan = new WebApp.DataStore({fields: Field});
 
    var cbokelurahanKtp = new Ext.form.ComboBox
	(
		{
		    id: 'cbokelurahanKtp',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
            forceSelection: true,
		    emptyText:'Select Kelurahan...',
            fieldLabel: 'Kelurahan',
		    align: 'Right',
			tabIndex:25,
//		    anchor:'60%',
		    store: dsKtpKelurahan,
		    valueField: 'KD_KELURAHAN',
		    displayField: 'KELURAHAN',
			anchor: '99%',
		    listeners:
						{
							'select': function(a, b, c)
							{
								selectKecamatanRequestEntry = b.data.valueField;
							},
											'render': function(c) {
															c.getEl().on('keypress', function(e) {
															if(e.getKey() == 13) //atau Ext.EventObject.ENTER
															Ext.getCmp('txtposktp').focus();
															}, c);
														}
						}
        }
	);

    return cbokelurahanKtp;
};

function mComboSukuRequestEntry()
{
    var Field = ['KD_SUKU','SUKU'];

    dsSukuRequestEntry = new WebApp.DataStore({fields: Field});
    dsSukuRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: 'kd_suku',
			    Sortdir: 'ASC',
			    target: 'ViewComboSuku',
			    param: ''
			}
		}
	)

    var cboSukuRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboSukuRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: ' ',
			editable: false,
		    fieldLabel: 'Suku ',
		    align: 'Right',
//		    anchor:'60%',
		    store: dsSukuRequestEntry,
		    valueField: 'KD_SUKU',
		    displayField: 'SUKU',
			anchor: '95%',
			tabIndex:18,
		    listeners:
			{
			    'select': function(a, b, c)
				{
			        var selectSukuRequestEntry = b.data.KD_PROPINSI;
                                },
                            'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13 || e.getKey() == 9 ) //atau Ext.EventObject.ENTER
                                     Ext.getCmp('setunit').collapsed = true;
                                    }, c);
                                }
			}
                }
	);

    return cboSukuRequestEntry;
};

function mComboPoliklinik()
{
    var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftarIGD = new WebApp.DataStore({fields: Field});

	ds_Poli_viDaftarIGD.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ViewSetupUnit',
                param: "kd_bagian=3 and type_unit=false"
            }
        }
    )

    var cboPoliklinikRequestEntry = new Ext.form.ComboBox
    (
        {
            id: 'cboPoliklinikRequestEntry',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'',
            fieldLabel: 'Unit ',
            align: 'Right',
            store: ds_Poli_viDaftarIGD,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
			tabIndex:27,
            anchor: '95%',
            listeners:
                {
                    'select': function(a, b, c)
				{
                                   loaddatastoredokter(b.data.KD_UNIT)
								
                                },
                    'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('cboDokterRequestEntry').focus();
                                    }, c);
                                }
        	}
        }
    )

    return cboPoliklinikRequestEntry;
};

function loaddatastoredokter(kd_unit)
{
          dsDokterRequestEntry.load
                (
                    {
                     params:
			{
                            Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
                            Sort: 'nama',
			    Sortdir: 'ASC',
			    target: 'ViewComboDokter',
			    param: 'where dk.kd_unit=~'+ kd_unit+ '~'
			}
                    }
                )
};

function mComboDokterRequestEntry()
{
    var Field = ['KD_DOKTER','NAMA'];

    dsDokterRequestEntry = new WebApp.DataStore({fields: Field});


    var cboDokterRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboDokterRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Dokter...',
		    fieldLabel: 'Dokter ',
		    align: 'Right',
            tabIndex:28,         
//		    anchor:'60%',
		    store: dsDokterRequestEntry,
		    valueField: 'KD_DOKTER',
		    displayField: 'NAMA',
			anchor: '95%',
		    listeners:
			{
			    'select': function(val)
				{
//                                  var selectDokterRequestEntry = b.data.KD_PROPINSI;
                                    //alert("is");
                                },
                            'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('kelPasien').focus();
                                    }, c);
                                }
			}
                }
	);

    return cboDokterRequestEntry;
};

function mcomborujukandari()
{
    var Field = ['CARA_PENERIMAAN','PENERIMAAN'];
    ds_Poli_viDaftarIGD = new WebApp.DataStore({fields: Field});

	ds_Poli_viDaftarIGD.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'penerimaan',
                Sortdir: 'ASC',
                target:'ViewComboRujukanDari',
                param: ""
            }
        }
    )

    var cboRujukanDariRequestEntry = new Ext.form.ComboBox
    (
        {
            id: 'cboRujukanDariRequestEntry',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Rujukan...',
            fieldLabel: 'Rujukan Dari ',
            align: 'Right',
            store: ds_Poli_viDaftarIGD,
            valueField: 'CARA_PENERIMAAN',
            displayField: 'PENERIMAAN',
            anchor: '95%',
			tabIndex:41,
            listeners:
                {
                    'select': function(a, b, c)
				{
                                   //alert(b.data.KD_PROPINSI)
                                   loaddatastorerujukan(b.data.CARA_PENERIMAAN)
                                },
                    'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('cboDokterRequestEntry').focus();
                                    }, c);
                                }


		}
        }
    )

    return cboRujukanDariRequestEntry;
}

function loaddatastorerujukan(cara_penerimaan)
{
          dsRujukanRequestEntry.load
                (
                    {
                     params:
			{
                            Skip: 0,
			    Take: 1000,
			    Sort: 'rujukan',
			    Sortdir: 'ASC',
			    target: 'ViewComboRujukan',
			    param: 'cara_penerimaan=~'+ cara_penerimaan+ '~'
			}
                    }
                )
}

function mComboRujukan()
{
    var Field = ['KD_RUJUKAN','RUJUKAN'];

    dsRujukanRequestEntry = new WebApp.DataStore({fields: Field});


    var cboRujukanRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboRujukanRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Rujukan...',
		    fieldLabel: 'Rujukan ',
		    align: 'Right',
            tabIndex:42,         
//		    anchor:'60%',
		    store: dsRujukanRequestEntry,
		    valueField: 'KD_RUJUKAN',
		    displayField: 'RUJUKAN',
			anchor: '95%',
		    listeners:
			{
			    'select': function(val)
				{
//                                  var selectDokterRequestEntry = b.data.KD_PROPINSI;
                                    //alert("is");
                                },
                            'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('kelPasien').focus();
                                    }, c);
                                }
			}
                }
	);

    return cboRujukanRequestEntry;
};


function RefreshDatacombo(jeniscus)  // show combo
{

    ds_customer_viDaftarIGD.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: 'jenis_cust=~'+ jeniscus +'~'
            }
        }
    )
	
   // rowSelectedKasirRWJ = undefined;
    return ds_customer_viDaftarIGD;
};


function mComboPerusahaan()
{
    var Field = ['KD_PERUSAHAAN','PERUSAHAAN'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboPerusahaan',
			    param: ''
			}
		}
	)
    var cboPerusahaanRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboPerusahaanRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: 'Perusahaan',
		    align: 'Right',
//		    anchor:'60%',
		    store: dsPerusahaanRequestEntry,
		    valueField: 'KD_PERUSAHAAN',
		    displayField: 'PERUSAHAAN',
			anchor: '95%',
			tabIndex:30,
		    listeners:
			{
			    'select': function(val)
				{
//			        var selectDokterRequestEntry = b.data.KD_PROPINSI;
                                    //alert("is");
                                }
			}
                }
	);

    return cboPerusahaanRequestEntry;
};




function loaddatastorekabupatenpenanggungjawab(kd_propinsi)
{
          dsKabupatenpenanggungjawab.load
                (
                    {
                     params:
							{
								Skip: 0,
								Take: 1000,
								//Sort: 'DEPT_ID',
								Sort: 'kd_propinsi',
								Sortdir: 'ASC',
								target: 'ViewComboKabupaten',
								param: 'kd_propinsi='+ kd_propinsi
							}
                    }
                )
};




function mComboPropinsiPenanggungJawab()
{
     var Field = ['KD_PROPINSI','PROPINSI'];

    dsPropinsiRequestEntry = new WebApp.DataStore({fields: Field});
    dsPropinsiRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: 'kd_propinsi',
			    Sortdir: 'ASC',
			    target: 'ViewComboPropinsi',
			    param: ''
			}
		}
	)

    var cboPropinsiPenanggungJawab = new Ext.form.ComboBox
	(
		{
		    id: 'cboPropinsiPenanggungJawab',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
			forceSelection: true,
			emptyText:'Select a Propinsi...',
			selectOnFocus:true,
            tabIndex:52,       
		    fieldLabel: 'Propinsi',
		    align: 'Right',
//		    anchor:'60%',
		    store: dsPropinsiRequestEntry,
		    valueField: 'KD_PROPINSI',
		    displayField: 'PROPINSI',
			anchor: '95%',
		    listeners:
			{
			    'select': function(a, b, c)
				{
				
								  
                                   selectPropinsiRequestEntry = b.data.KD_PROPINSI;

                                   loaddatastorekabupatenpenanggungjawab(b.data.KD_PROPINSI);
                                },
                            'render': function(c) {
                                                c.getEl().on('keypress', function(e) {
                                                if(e.getKey() == 13 ) //atau Ext.EventObject.ENTER
												{
                                                 Ext.getCmp('cboKabupatenRequestEntry').focus();
												}
												else if (e.getKey() == 9 ) //atau Ext.EventObject.ENTER
												{
												dataKabupaten = c.value;
                                                loaddatastorekabupatenpenanggungjawab(dataKabupaten);
                                                }
												}, c);
                                            }

			}
                }
	);
        return cboPropinsiPenanggungJawab;
        
    
};




function loaddatastorekecamatanpenanggungjawab(kd_kabupaten)
{
    dsKecamatanPenanggungJawab.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'kd_kabupaten',
			    Sortdir: 'ASC',
			    target: 'ViewComboKecamatan',
			    param: 'kd_kabupaten='+ kd_kabupaten
			}
		}
	)
};

function mComboKabupatenpenanggungjawab()
{
      var Field = ['KD_KABUPATEN','KD_PROPINSI','KABUPATEN'];
      dsKabupatenpenanggungjawab = new WebApp.DataStore({fields: Field});

    var cboKabupatenpenanggungjawab = new Ext.form.ComboBox
	(
		{
		    id: 'cboKabupatenpenanggungjawab',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Select a Kabupaten...',
		    fieldLabel: 'Kab/Kod',
		    align: 'Right',
                    
		    store: dsKabupatenpenanggungjawab,
		    valueField: 'KD_KABUPATEN',
		    displayField: 'KABUPATEN',
			tabIndex:53,
                    anchor: '95%',
		    listeners:
			{
			    'select': function(a, b, c)
				{
                                   selectKabupatenRequestEntry = b.data.KD_KABUPATEN;

                                    loaddatastorekecamatanpenanggungjawab(b.data.KD_KABUPATEN);
                                },
                                
								'render':function(c)
								{
								   c.getEl().on('keypress', function(e) {
                                                if(e.getKey() == 13 ) //atau Ext.EventObject.ENTER
												{
                                                Ext.getCmp('cboKecamatanRequestEntry').focus();
												}
												else if (e.getKey() == 9 ) //atau Ext.EventObject.tab
												{
												var dataKecamatan = c.value;
                                                loaddatastorekecamatanpenanggungjawab(dataKecamatan);
                                                }
												}, 
												c);
								}
                                
			}
                }
	);
    return cboKabupatenpenanggungjawab;
};








function loaddatastorekelurahanpenanggungjawab(kd_kecamatan)
{
    dsKelurahanPenanggungJawab.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'kd_kecamatan',
			    Sortdir: 'ASC',
			    target: 'ViewComboKelurahan',
			    param: 'kd_kecamatan='+ kd_kecamatan
			}
		}
	)
}

function loaddatastorekelurahan(kd_kecamatan)
{
    dsKelurahan.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'kd_kecamatan',
			    Sortdir: 'ASC',
			    target: 'ViewComboKelurahan',
			    param: 'kd_kecamatan='+ kd_kecamatan
			}
		}
	)
}


function mComboKecamatanPenanggungJawab()
{
    var Field = ['KD_KECAMATAN','KD_KABUPATEN','KECAMATAN'];
    dsKecamatanPenanggungJawab = new WebApp.DataStore({fields: Field});
 
    var cboKecamatanPenanggungJawab = new Ext.form.ComboBox
	(
		{
		    id: 'cboKecamatanPenanggungJawab',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
            forceSelection: true,
		    emptyText:'Select a Kecamatan...',
            fieldLabel: 'Kecamatan',
		    align: 'Right',
			tabIndex:54,
//		    anchor:'60%',
		    store: dsKecamatanPenanggungJawab,
		    valueField: 'KD_KECAMATAN',
		    displayField: 'KECAMATAN',
                    anchor: '95%',
		    listeners:
						{
							'select': function(a, b, c)
							{
								loaddatastorekelurahanpenanggungjawab(b.data.KD_KECAMATAN);
								selectKecamatanRequestEntry = b.data.KD_KECAMATAN;
							},
											'render': function(c) {
															c.getEl().on('keypress', function(e) {
															if(e.getKey() == 13) //atau Ext.EventObject.ENTER
															Ext.getCmp('cboPendidikanRequestEntry').focus();
															}, c);
														}
						}
        }
	);

    return cboKecamatanPenanggungJawab;
};



function mCombokelurahanPenanggungJawab()
{
    var Field = ['KD_KELURAHAN','KD_KECAMATAN','KELURAHAN'];
    dsKelurahanPenanggungJawab = new WebApp.DataStore({fields: Field});
 
    var cbokelurahanPenanggungJawab = new Ext.form.ComboBox
	(
		{
		    id: 'cbokelurahanPenanggungJawab',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
            forceSelection: true,
		    emptyText:'Select a Kelurahan...',
            fieldLabel: 'Kelurahan',
		    align: 'Right',
			tabIndex:55,
//		    anchor:'60%',
		    store: dsKelurahanPenanggungJawab,
		    valueField: 'KD_KELURAHAN',
		    displayField: 'KELURAHAN',
			anchor: '95%',
		    listeners:
						{
							'select': function(a, b, c)
							{
								selectKecamatanRequestEntry = b.data.KD_KECAMATAN;
							},
											'render': function(c) {
															c.getEl().on('keypress', function(e) {
															if(e.getKey() == 13) //atau Ext.EventObject.ENTER
															Ext.getCmp('cboPendidikanRequestEntry').focus();
															}, c);
														}
						}
        }
	);

    return cbokelurahanPenanggungJawab;
};




function mComboPekerjaanPenanggungJawabRequestEntry()
{
    var Field = ['KD_PEKERJAAN','PEKERJAAN'];

    dsPekerjaanPenanggungJawabRequestEntry = new WebApp.DataStore({fields: Field});
    dsPekerjaanPenanggungJawabRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: 'kd_pekerjaan',
			    Sortdir: 'ASC',
			    target: 'ViewComboPekerjaan',
			    param: ''
			}
		}
	)

    var cboPekerjaanPenanggungJawabRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboPekerjaanPenanggungJawabRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Pekerjaan...',
		    fieldLabel: 'Pekerjaan',
		    align: 'Right',
			tabIndex:69,
//		    anchor:'60%',
		    store: dsPekerjaanPenanggungJawabRequestEntry,
		    valueField: 'KD_PEKERJAAN',
		    displayField: 'PEKERJAAN',
                    anchor: '95%',
		    listeners:
			{
			    'select': function(a, b, c)
				{
			        var selectPekerjaanPenanggungJawabRequestEntry = b.data.KD_PROPINSI;
                                },
                                'render': function(c) {
                                                c.getEl().on('keypress', function(e) {
                                                if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                Ext.getCmp('cboSukuRequestEntry').focus();
                                                }, c);
                                            }
			}
                }
	);

    return cboPekerjaanPenanggungJawabRequestEntry;
};


function loaddatastorekabupatenKtppenanggungjawab(kd_propinsi)
{
          dsKabupatenKtppenanggungjawab.load
                (
                    {
                     params:
							{
								Skip: 0,
								Take: 1000,
								//Sort: 'DEPT_ID',
								Sort: 'kd_propinsi',
								Sortdir: 'ASC',
								target: 'ViewComboKabupaten',
								param: 'kd_propinsi='+ kd_propinsi
							}
                    }
                )
};

function mComboPropinsiKTPPenanggungJawab()
{
     var Field = ['KD_PROPINSI','PROPINSI'];

    dsPropinsiRequestEntry = new WebApp.DataStore({fields: Field});
    dsPropinsiRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: 'kd_propinsi',
			    Sortdir: 'ASC',
			    target: 'ViewComboPropinsi',
			    param: ''
			}
		}
	)

    var cboPropinsiKtpPenanggungJawab = new Ext.form.ComboBox
	(
		{
		    id: 'cboPropinsiKtpPenanggungJawab',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
			forceSelection: true,
			emptyText:'Select a Propinsi...',
			selectOnFocus:true,   
		    fieldLabel: 'Propinsi',
		    align: 'Right',
//		    anchor:'60%',
		    store: dsPropinsiRequestEntry,
		    valueField: 'KD_PROPINSI',
		    displayField: 'PROPINSI',
			anchor: '95%',
			tabIndex:61,
		    listeners:
			{
			    'select': function(a, b, c)
				{
				
								  
                                   selectPropinsiRequestEntry = b.data.KD_PROPINSI;

                                   loaddatastorekabupatenKtppenanggungjawab(b.data.KD_PROPINSI);
                                },
                            'render': function(c) {
                                                c.getEl().on('keypress', function(e) {
                                                if(e.getKey() == 13 ) //atau Ext.EventObject.ENTER
												{
                                                 Ext.getCmp('cboKabupatenRequestEntry').focus();
												}
												else if (e.getKey() == 9 ) //atau Ext.EventObject.ENTER
												{
												dataKabupaten = c.value;
                                                loaddatastorekabupatenKtppenanggungjawab(dataKabupaten);
                                                }
												}, c);
                                            }

			}
                }
	);
        return cboPropinsiKtpPenanggungJawab;
        
    
};

function loaddatastoreKtpkecamatanpenanggungjawab(kd_kabupaten)
{
    dsKecamatanKtpPenanggungJawab.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'kd_kabupaten',
			    Sortdir: 'ASC',
			    target: 'ViewComboKecamatan',
			    param: 'kd_kabupaten='+ kd_kabupaten
			}
		}
	)
};

function mComboKabupatenKtppenanggungjawab()
{
      var Field = ['KD_KABUPATEN','KD_PROPINSI','KABUPATEN'];
      dsKabupatenKtppenanggungjawab = new WebApp.DataStore({fields: Field});

    var cboKabupatenKtppenanggungjawab = new Ext.form.ComboBox
	(
		{
		    id: 'cboKabupatenKtppenanggungjawab',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Select a Kabupaten...',
		    fieldLabel: 'Kab/Kod',
		    align: 'Right',
		    store: dsKabupatenKtppenanggungjawab,
		    valueField: 'KD_KABUPATEN',
		    displayField: 'KABUPATEN',
			tabIndex:62,
			anchor: '95%',
		    listeners:
			{
			    'select': function(a, b, c)
				{
									selectKabupatenRequestEntry = b.data.KD_KABUPATEN;

                                    loaddatastoreKtpkecamatanpenanggungjawab(b.data.KD_KABUPATEN);
                                },
                                
								'render':function(c)
								{
								   c.getEl().on('keypress', function(e) {
                                                if(e.getKey() == 13 ) //atau Ext.EventObject.ENTER
												{
													Ext.getCmp('cboKecamatanRequestEntry').focus();
												}
												else if (e.getKey() == 9 ) //atau Ext.EventObject.ENTER
												{
													var dataKecamatan = c.value;
													loaddatastoreKtpkecamatanpenanggungjawab(dataKecamatan);
                                                }
												}, c);
								}
                                
			}
                }
	);
    return cboKabupatenKtppenanggungjawab;
};

function mCombokelurahanKtpPenanggungJawab()
{
    var Field = ['KD_KELURAHAN','KD_KECAMATAN','KELURAHAN'];
    dsKelurahanKtpPenanggungJawab = new WebApp.DataStore({fields: Field});
 
    var cbokelurahanKtpPenanggungJawab = new Ext.form.ComboBox
	(
		{
		    id: 'cbokelurahanKtpPenanggungJawab',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
            forceSelection: true,
		    emptyText:'Select a Kelurahan...',
            fieldLabel: 'Kelurahan',
		    align: 'Right',
			tabIndex:64,
//		    anchor:'60%',
		    store: dsKelurahanKtpPenanggungJawab,
		    valueField: 'KD_KELURAHAN',
		    displayField: 'KELURAHAN',
			anchor: '95%',
		    listeners:
						{
							'select': function(a, b, c)
							{
								selectKecamatanRequestEntry = b.data.KD_KECAMATAN;
							},
											'render': function(c) {
															c.getEl().on('keypress', function(e) {
															if(e.getKey() == 13) //atau Ext.EventObject.ENTER
															Ext.getCmp('cboPendidikanRequestEntry').focus();
															}, c);
														}
						}
        }
	);

    return cbokelurahanKtpPenanggungJawab;
};

function loaddatastorekelurahanKtppenanggungjawab(kd_kecamatan)
{
    dsKelurahanKtpPenanggungJawab.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'kd_kecamatan',
			    Sortdir: 'ASC',
			    target: 'ViewComboKelurahan',
			    param: 'kd_kecamatan='+ kd_kecamatan
			}
		}
	)
}

function mComboKecamatanKtpPenanggungJawab()
{
    var Field = ['KD_KECAMATAN','KD_KABUPATEN','KECAMATAN'];
    dsKecamatanKtpPenanggungJawab = new WebApp.DataStore({fields: Field});
 
    var cboKecamatanKtpPenanggungJawab = new Ext.form.ComboBox
	(
		{
		    id: 'cboKecamatanKtpPenanggungJawab',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
            forceSelection: true,
		    emptyText:'Select a Kecamatan...',
            fieldLabel: 'Kecamatan',
		    align: 'Right',
			tabIndex:63,
//		    anchor:'60%',
		    store: dsKecamatanKtpPenanggungJawab,
		    valueField: 'KD_KECAMATAN',
		    displayField: 'KECAMATAN',
                    anchor: '95%',
		    listeners:
						{
							'select': function(a, b, c)
							{
								selectKecamatanRequestEntry = b.data.KD_KECAMATAN;
								loaddatastorekelurahanKtppenanggungjawab(b.data.KD_KECAMATAN);
							},
											'render': function(c) {
															c.getEl().on('keypress', function(e) {
															if(e.getKey() == 13) //atau Ext.EventObject.ENTER
															Ext.getCmp('cboPendidikanRequestEntry').focus();
															}, c);
														}
						}
        }
	);

    return cboKecamatanKtpPenanggungJawab;
};

function mComboHubunganKeluarga()
{
    var cboHubunganKeluarga = new Ext.form.ComboBox
	(
		{
			id:'cboHubunganKeluarga',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih...',
			selectOnFocus:true,
			forceSelection: true,
			tabIndex:67,
			fieldLabel: 'Hub.Keluarga',
			width:120,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Kep. Keluarga'], [2, 'Suami'],[3, 'Istri'], [4, 'Anak Kandung']
                                        ,[5, 'Anak Tiri'], [6, 'Anak Angkat'],[7, 'Ayah'], [8, 'Ibu'], [9, 'Mertua']
                                        ,[10, 'Sdr Kandung'], [11, 'Cucu'],[12, 'Famili'], [13, 'Pembantu']
                                        ,[14, 'Lain-Lain']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetHubunganKeluarga,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetHubunganKeluarga=b.data.valueField ;
					//GetDataHubunganKeluarga(b.data.valueField)
					//alert(StatusHubunganKeluarga)
				},
				'render': function(c)
				{
					c.getEl().on('keypress', function(e) {
					if(e.getKey() == 13) //atau Ext.EventObject.ENTER
					Ext.getCmp('cboStatusMarital').focus();
					}, c);
				}
			}
		}
	);
	return cboHubunganKeluarga;
};

function mComboPendidikanPenanggungJawab()
{
    var Field = ['KD_PENDIDIKAN','PENDIDIKAN'];

    dsPendidikanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPendidikanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: 'kd_pendidikan',
			    Sortdir: 'ASC',
			    target: 'ViewComboPendidikan',
			    param: ''
			}
		}
	)

    var cboPendidikanPenanggungJawab = new Ext.form.ComboBox
	(
		{
		    id: 'cboPendidikanPenanggungJawab',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Pendidikan...',
		    fieldLabel: 'Pendidikan',
		    align: 'Right',
//		    anchor:'60%',
		    store: dsPendidikanRequestEntry,
		    valueField: 'KD_PENDIDIKAN',
		    displayField: 'PENDIDIKAN',
			tabIndex:68,
			anchor: '95%',
		    listeners:
			{
			    'select': function(a, b, c)
				{
			        var selectPendidikanRequestEntry = b.data.valueField;
                                },
                                'render': function(c) {
                                                c.getEl().on('keypress', function(e) {
                                                if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                Ext.getCmp('cboPekerjaanRequestEntry').focus();
                                                }, c);
                                            }
			}
                }
	);

    return cboPendidikanPenanggungJawab;
};

function mComboStatusMaritalPenanggungjawab()
{
    var cboStatusMaritalPenanggungjawab = new Ext.form.ComboBox
	(
		{
			id:'cboStatusMaritalPenanggungjawab',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			tabIndex:50,
			forceSelection: true,
			emptyText:'Pilih Status...',
			fieldLabel: 'Status Marital ',
			//width:110,
                        anchor: '95%',
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[0, 'Blm Kawin'], [1, 'Kawin'],[2, 'Janda'], [3, 'Duda']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetSatusMarital,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetSatusMarital=b.data.displayText ;
				},
                                'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('txtAlamat').focus();
                                    }, c);
                                }
			}
		}
	);
	return cboStatusMaritalPenanggungjawab;
};


function mComboPerusahaanPenanggungJawab()
{
    var Field = ['KD_PERUSAHAAN','PERUSAHAAN','ALAMAT'];
    dsPerusahaanPenanggungJawabRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanPenanggungJawabRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboPerusahaan',
			    param: ''
			}
		}
	)
    var cboPerusahaanPenanggungJawabRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboPerusahaanPenanggungJawabRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: 'Perusahaan',
		    align: 'Right',
			width: 120,
//		    anchor:'60%',
		    store: dsPerusahaanPenanggungJawabRequestEntry,
		    valueField: 'KD_PERUSAHAAN',
		    displayField: 'PERUSAHAAN',
			tabIndex:70,
			anchor: '95%',
		    listeners:
			{
			   'select': function(a, b, c)
				{
					Ext.getCmp('txtAlamatPrshPenanggungjawab').setValue(b.data.ALAMAT);
				}
			}
                }
	);

    return cboPerusahaanPenanggungJawabRequestEntry;
};

function mComboJKPJ()
{
    var cboJKPJ = new Ext.form.ComboBox
	(
		{
			id:'cboJKPJ',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Pilih Jenis Kelamin...',
			fieldLabel: 'Jen. Kelamin ',
			//anchor :'99%',
			width:150,
			tabIndex:49,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Laki - Laki'], [2, 'Perempuan']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetJK,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetJKPJ=b.data.displayField ;
//                                        getdatajeniskelamin(b.data.displayText)
                                        //alert(jenis_kelamin)
				},
                                'render': function(c) {
                                                    c.getEl().on('keypress', function(e) {
                                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                    Ext.getCmp('cbWniPenanggungJawab').focus();
                                                    }, c);
                                                }
			}
		}
	);
	return cboJKPJ;
};





var rowSelectedLookPasien;
var rowSelectedLookSL;
var mWindowLookup;
var nFormPendaftaranIGD=1;
var dsLookupPasienList;

var criteria ='';

function FormLookupPasien(criteria,nFormAsal,nName_ID,strRM,strNama,strAlamat)
{
    var vWinFormEntry = new Ext.Window
	(
		{
		    id: 'FormPasienLookup',
		    title: 'Lookup Pasien',
		    closable: true,
		    width: 600,//450,//
		    height: 400,
		    border: true,
		    plain: true,
		    resizable: false,
		    layout: 'form',
		    iconCls: 'find',
		    modal: true,
		    items:
			[
			    // fnGetDTLGridLookUp(criteria, nFormAsal),
				fnGetDTLGridLookUpPas(criteria, nFormAsal,nName_ID),
				{
				    xtype: 'button',
				    text: 'Ok',
				    width: 70,
				    style: {'margin-left': '510px', 'margin-top': '7px'},
				    hideLabel: true,
				    id: 'btnOkpgw',
				    handler: function()
					{
						GetPasien(nFormAsal,nName_ID);
				    }
				}
			],
			tbar:
			{
				xtype: 'toolbar',
				items:
				[
					{
						xtype: 'tbtext',
						text: 'No. RM : '
					},
					{
						xtype: 'textfield',
						id: 'txtNoRMLookupPasien',
						listeners:
						{
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13)
								{
									criteria = getQueryCariPasien();
									RefreshDataLookupPasien(criteria);
								}
							}
						}

					},
					{
						xtype: 'tbtext',
						text: 'Nama : '
					},
					{
						xtype: 'textfield',
						id: 'txtNama_viDaftarIGDMLookupPasien',
						listeners:
						{
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13)
								{
									criteria = getQueryCariPasien();
									RefreshDataLookupPasien(criteria);
								}
							}
						}
					},
					{
					xtype: 'tbtext',
					text: 'Alamat : '
					},
					{
						xtype: 'textfield',
						id: 'txtAlamat_viDaftarIGDMLookupPasien',
						listeners:
						{
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13)
								{
									criteria = getQueryCariPasien();
									RefreshDataLookupPasien(criteria);
								}
							}
						}
					},
					{
						xtype: 'tbfill'
					},
					{
						xtype: 'button',
						id: 'btnRefreshGLLookupCalonMHS',
						iconCls: 'refresh',
						handler: function()
						{
							// var criteria ='';
							criteria = getQueryCariPasien();
							RefreshDataLookupPasien(criteria);
						}
					}
				]
			}
			,
		    listeners:
				{
				    activate: function()
				    {
						Ext.get('txtNoRMLookupPasien').dom.value = strRM;
						Ext.get('txtNama_viDaftarIGDMLookupPasien').dom.value = strNama;
						Ext.get('txtAlamat_viDaftarIGDMLookupPasien').dom.value = strAlamat;

					}
				}
		}
	);
    vWinFormEntry.show();
	mWindowLookup = vWinFormEntry;
};

///---------------------------------------------------------------------------------------///


function fnGetDTLGridLookUpPas(criteria,nFormAsal,nName_ID)
{
    var fldDetail =
	['KD_PASIEN', 'NAMA', 'NAMA_KELUARGA', 'JENIS_KELAMIN', 'TEMPAT_LAHIR', 'TGL_LAHIR', 'AGAMA',
        'GOL_DARAH', 'WNI', 'STATUS_MARITA', 'ALAMAT', 'KD_KELURAHAN', 'PENDIDIKAN', 'PEKERJAAN',
        'NAMA_UNIT','TGL_MASUK', 'URUT_MASUK','KD_KELURAHAN','KABUPATEN','KECAMATAN','PROPINSI',
	];

	// var dsLookupPasienList = new WebApp.DataStore({ fields: fldDetail });
	dsLookupPasienList = new WebApp.DataStore({fields: fldDetail});

	RefreshDataLookupPasien(criteria);
	var vGridLookupPasienFormEntry = new Ext.grid.EditorGridPanel
	(
		{
			id:'vGridLookupPasienFormEntry',
			title: '',
			stripeRows: true,
			store: dsLookupPasienList,
			height: 310, //330,
			columnLines:true,
			bodyStyle: 'padding:0px',
			enableKeyEvents: true,
			border: false,
			sm: new Ext.grid.RowSelectionModel
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelectedLookPasien = dsLookupPasienList.getAt(row);
						}
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, ridx, cidx)
				{
                                    var nFormAsal = 1;
					GetPasien(nFormAsal,nName_ID);
				},

				'specialkey': function()
				{
					if (Ext.EventObject.getKey() == 13)
					{
						GetPasien(nFormAsal,nName_ID);
					}
				}
			},
			cm: fnGridLookPasienColumnModel(),
			viewConfig: {forceFit: true}
		});

	return vGridLookupPasienFormEntry;
};

function fnGridLookPasienColumnModel()
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{
				id: 'colLookupPasien',
				header: "No. RM",
				dataIndex: 'KD_PASIEN',
				width: 200
			},
			{
				id: 'colLookupNamaPasien',
				header: "Nama",
				dataIndex: 'NAMA',
				width: 300
			},
			{
				id: 'colLookupAlamatPasien',
				header: "Alamat",
				dataIndex: 'ALAMAT',
				width: 300
			}

		]
	)
};

function GetPasien(nFormAsal,nName_ID)
{
	if (rowSelectedLookPasien != undefined || nName_ID != undefined)
	{
		if (nFormAsal === nFormPendaftaranIGD)
		{

                        Ext.getCmp('txtNoRequest_viDaftarIGD').setValue(rowSelectedLookPasien.data.KD_PASIEN);
                        Ext.getCmp('txtNama_viDaftarIGD').setValue(rowSelectedLookPasien.data.NAMA);
                        Ext.getCmp('txtNama_viDaftarIGDKeluarga').setValue(rowSelectedLookPasien.data.NAMA_KELUARGA);
                        Ext.getCmp('txtTempatLahir_viDaftarIGD').setValue(rowSelectedLookPasien.data.TEMPAT_LAHIR);
                        Ext.getCmp('cboPendidikanRequestEntry').setValue(rowSelectedLookPasien.data.PENDIDIKAN);
                        Ext.getCmp('cboPekerjaanRequestEntry').setValue(rowSelectedLookPasien.data.PEKERJAAN);
                        Ext.getCmp('cboWarga').setValue(rowSelectedLookPasien.data.WNI);
                        Ext.getCmp('txtAlamat_viDaftarIGD').setValue(rowSelectedLookPasien.data.ALAMAT);
                        Ext.getCmp('cboAgamaRequestEntry').setValue(rowSelectedLookPasien.data.AGAMA);
                        Ext.getCmp('cboGolDarah').setValue(rowSelectedLookPasien.data.GOL_DARAH);
                        Ext.get('dtpTanggalLahir_viDaftarIGD').dom.value = ShowDate(rowSelectedLookPasien.data.TGL_LAHIR);
                        Ext.getCmp('cboStatusMarital').setValue(rowSelectedLookPasien.data.STATUS_MARITA);
                        var tmpjk = "";
                        var tmpwni = "";
                        if (rowSelectedLookPasien.data.JENIS_KELAMIN === "t")
                            {
                                tmpjk = "Laki - Laki";
                            }
                            else
                                {
                                    tmpjk = "Perempuan";
                                }
                        Ext.getCmp('cboJK').setValue(tmpjk);
                        if (rowSelectedLookPasien.data.WNI === "t")
                            {
                                tmpwni = "WNI";
                            }
                            else
                                {
                                    tmpwni = "WNA";
                                }
                        Ext.getCmp('cboWarga').setValue(tmpwni);
                        //getdatatempat_viDaftarIGD(rowdata.KD_PASIEN);

                        Ext.getCmp('cboPropinsiRequestEntry').setValue(rowSelectedLookPasien.data.PROPINSI);
                        Ext.getCmp('cboKabupatenRequestEntry').setValue(rowSelectedLookPasien.data.KABUPATEN);
                        Ext.getCmp('cboKecamatanRequestEntry').setValue(rowSelectedLookPasien.data.KECAMATAN);

                        Ext.getCmp('btnSimpan_viDaftarIGD').disable();
                        Ext.getCmp('btnSimpanExit_viDaftarIGD').disable();
                       // Ext.getCmp('btnDelete_viDaftarIGD').enable();
                        //Ext.getCmp('btnUlang_viDaftarIGD').enable();
//                        mNoKunjungan_viKasir = rowdata.NO_KUNJUNGAN;
		}

	}
	rowSelectedLookPasien=undefined;
	mWindowLookup.close();
}

function getQueryCariPasien()
{
	var strKriteria = "";

	if(Ext.get('txtNoRMLookupPasien').getValue() != '')
	{
		strKriteria += " RIGHT(pasien.KD_PASIEN,10) like '%" + Ext.get('txtNoRMLookupPasien').dom.value + "%'"
	}

	if(Ext.get('txtNama_viDaftarIGDMLookupPasien').getValue() != '')
	{
		if (strKriteria != "")
		{
			strKriteria += " AND pasien.NAMA like '%" + Ext.get('txtNama_viDaftarIGDMLookupPasien').dom.value + "%'"
		}
		else
		{
			strKriteria += " pasien.NAMA like '%" + Ext.get('txtNama_viDaftarIGDMLookupPasien').dom.value + "%'"
		}
	}

	if(Ext.get('txtAlamat_viDaftarIGDMLookupPasien').getValue() != '')
	{
		if (strKriteria != "")
		{
			strKriteria += " AND pasien.ALAMAT like '%" + Ext.get('txtAlamat_viDaftarIGDMLookupPasien').dom.value + "%'"
		}
		else
		{
			strKriteria += " pasien.ALAMAT like '%" + Ext.get('txtAlamat_viDaftarIGDMLookupPasien').dom.value + "%'"
		}
	}

	return strKriteria;
}

function RefreshDataLookupPasien(criteria)
{
	dsLookupPasienList.load
		(
			{
				params:
				{
					Skip: 0,
					Take: 100,
					Sortdir: 'ASC',
					target: 'View_DaftarIGD',//'Viview_viDataPasien',
					param: criteria
				}
			}
		);

	return dsLookupPasienList;
}

function Combo_Select(combo)
{
   var value = combo
   //var txtgetnamaPeserta = Ext.getCmp('txtNama_viDaftarIGDPeserta') ;

   if(value == "Perseorangan")
   {    
        Ext.getCmp('txtNama_viDaftarIGDPeserta').hide()
        Ext.getCmp('txtNoAskes').hide()
        Ext.getCmp('txtNoSJP').hide()
        Ext.getCmp('cboPerseorangan').hide()
        Ext.getCmp('cboAsuransiIGD').show()
		cboAsuransiIGD.label.update('Perseorangan');
        Ext.getCmp('cboPerusahaanRequestEntry').hide()
        

   }
   else if(value == "Perusahaan")
   {    
        Ext.getCmp('txtNama_viDaftarIGDPeserta').hide()
        Ext.getCmp('txtNoAskes').hide()
        Ext.getCmp('txtNoSJP').hide()
        Ext.getCmp('cboPerseorangan').hide()
        Ext.getCmp('cboAsuransiIGD').show()
		cboAsuransiIGD.label.update('Perusahaan');
        Ext.getCmp('cboPerusahaanRequestEntry').hide()
        
   }
   else
       {
         Ext.getCmp('txtNama_viDaftarIGDPeserta').show()
         Ext.getCmp('txtNoAskes').show()
         Ext.getCmp('txtNoSJP').show()
         Ext.getCmp('cboPerseorangan').hide()
         Ext.getCmp('cboAsuransiIGD').show()
         Ext.getCmp('cboPerusahaanRequestEntry').hide()
		 cboAsuransiIGD.label.update('Asuransi');
         
       }
}

function printbill()
{
    Ext.Ajax.request
            (
				{
					url: baseURL + "index.php/main/CreateDataObj",
					params: dataparamreport_viDaftarIGD(),
					success: function(o)
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true)
						{
							ShowPesanInfo_viDaftarIGD('Data berhasil di simpan','Simpan Data');
							datarefresh_viDaftarIGD(tmpcriteriaIGD);
							// if(mBol === false)
							// {
								// Ext.get('txtID_viDaftarIGD').dom.value=cst.ID_SETUP;
							// };
							addNew_viDaftarIGD = false;
							//Ext.get('txtNoKunjungan_viDaftarIGD').dom.value = cst.NO_KUNJUNGAN
							Ext.get('txtNoRequest_viDaftarIGD').dom.value = cst.KD_PASIEN
							Ext.getCmp('btnSimpan_viDaftarIGD').disable();
							Ext.getCmp('btnSimpanExit_viDaftarIGD').disable();
//							Ext.getCmp('btnDelete_viDaftarIGD').enable();
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarning_viDaftarIGD('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
						}
						else
						{
							ShowPesanError_viDaftarIGD('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
						}
					}
				}
            )
}

function dataparamreport_viDaftarIGD()
{
    var paramsreport_ViPendaftaranIGD =
		{
      
                    Table: 'DirectPrinting',
                    No_TRans: tmpnotransaksi,
                    KdKasir : tmpkdkasir
//                    NoMedrec:  Ext.get('txtNoRequest_viDaftarIGD').getValue(),
//                    KelPasien: Ext.getCmp('kelPasien').getValue(),
//                    Dokter: Ext.getCmp('cboDokterRequestEntry').getValue(),
//                    NamaPasien: Ext.get('txtNama_viDaftarIGD').getValue(),
//                    Alamat : Ext.get('txtAlamat_viDaftarIGD').getValue(),
//                    Poli: Ext.getCmp('cboPoliklinikRequestEntry').getValue()
		};
    return paramsreport_ViPendaftaranIGD
}

