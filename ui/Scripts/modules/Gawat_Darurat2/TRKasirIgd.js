// Data Source ExtJS # --------------

/**
*	Nama File 		: TRKasirIgd.js
*	Menu 			: 
*	Model id 		: 
*	Keterangan 		: Kasir IGD adalah proses untuk pembayaran pasien pada IGD
*	Di buat tanggal : 18 Agustus 2014
*	Oleh 			: SDY_RI
*/

// Deklarasi Variabel pada Kasir IGD # --------------

var dataSource_viKasirIgd;
var selectCount_viKasirIgd=50;
var NamaForm_viKasirIgd="Kasir IGD ";
var mod_name_viKasirIgd="viKasirIgd";
var now_viKasirIgd= new Date();
var addNew_viKasirIgd;
var rowSelected_viKasirIgd;
var setLookUps_viKasirIgd;
var mNoKunjungan_viKasirIgd='';

var CurrentData_viKasirIgd =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viKasirIgd(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

// End Deklarasi Variabel pada Kasir IGD # --------------

// Start Project Kasir IGD # --------------

// --------------------------------------- # Start Function # ---------------------------------------
// ## Silahkan tempatkan semua fungsi eksekusi disini
function GetPasienData()
{
	// Ext.Ajax.request
	 // (
		// {
            // url: "./Module.mvc/ExecProc",
            // params: 
			// {
                // UserID: 'Admin',
                // ModuleID: 'getKunjunganDataPasien',
				// Params:	getParamKunjunganKdPasien(strCari)
            // },
            // success: function(o) 
			// {
				
                // var cst = Ext.decode(o.responseText);

				// if (cst.success === true) 
				// {
					//test;
					Ext.getCmp('txtNoMedrec_viKasirIgd').setValue('0-00-00-22');
					Ext.getCmp('txtNama_viKasirIgd').setValue('Wahyudin, ST');
					Ext.getCmp('txtNoreg_viKasirIgd').setValue('0000001');
					Ext.getCmp('txtNotrans_viKasirIgd').setValue('0000002');
					Ext.getCmp('DtpTglLahir_viKasirIgd').setValue(ShowDate('2014-10-22'));					
					Ext.getCmp('txtUnit_viKasirIgd').setValue('3');
					Ext.getCmp('txtNmUnit_viKasirIgd').setValue('Unit Gawat Darurat');
					Ext.getCmp('txtKelompok_viKasirIgd').setValue('Jaminan Kesehatan Nasional');									
					Ext.getCmp('txtDokter_viKasirIgd').setValue('001');
					// Ext.getCmp('dtpKunjungan_viDaftar').setValue(ShowDate(cst.TGL_KUNJUNGAN));
					Ext.getCmp('txtNmDokter_viKasirIgd').setValue('Dr. Handi Nurfatah');
					Ext.getCmp('rdDatangSendiri_viKasirIgd').setValue(1);
				// }
				// else
				// {
					// ShowPesanWarning_viDaftar('No. RM tidak di temukan '  + cst.pesan,'Informasi');
					// // Ext.get('txtNamaKRSMahasiswa').dom.value = '';								
					// // Ext.get('txtFakJurKRSMahasiswa').dom.value = '';					
					// // Ext.get('txtKdJurKRSMahasiswa').dom.value = '';					
					// // Ext.get('txtBatasStudyKRSMahasiswa').dom.value = '';					
					// // Ext.get('txtBarcode').dom.focus();

					// // var criteria = ' WHERE NIM IS NOT NULL AND J.KD_JURUSAN IN ' + strKD_JURUSAN + ' ';
						// // if (Ext.get('txtNIMKRSMahasiswa').dom.value != '')
						// // {
							// // criteria += ' AND NIM like ~%' + Ext.get('txtNIMKRSMahasiswa').dom.value + '%~';
							
						// // };
						// // FormLookupMahasiswa('txtNIMKRSMahasiswa','txtNamaKRSMahasiswa','txtFakJurKRSMahasiswa','txtBatasStudyKRSMahasiswa','txtIPSKRSMahasiswa','','','','txtKdJurKRSMahasiswa',criteria);
				// };
            // }
        // }
	// );
}


// --------------------------------------- # End Function # ---------------------------------------

// --------------------------------------- # Start Form # ---------------------------------------

/**
*	Function : dataGrid_viKasirIgd
*	
*	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
*/

function dataGrid_viKasirIgd(mod_id_viKasirIgd)
{	
    // Field kiriman dari Project Net.
    var FieldMaster_viKasirIgd = 
	[
		 'NO_KUNJUNGAN', 'KD_KELOMPOK', 'KD_UNIT', 'KD_DOKTER', 'KD_CUSTOMER', 'KD_PASIEN', 
		 'TGL_KUNJUNGAN','JAM_KUNJUNGAN', 'TINGGI_BADAN', 'BERAT_BADAN', 'TEKANAN_DRH', 
		 'NADI','ALERGI', 'KELUHAN', 'RUJUK_RAD', 'RUJUK_LAB', 'TAHAP_PROSES', 'PASIEN_BARU',
		 'NAMA_UNIT','KELOMPOK', 'DOKTER', 'CUSTOMER', 'PS_BARU','KD_PENDIDIKAN','KD_STS_MARITAL', 
		 'KD_AGAMA','KD_PEKERJAAN','NAMA','TEMPAT_LAHIR','TGL_LAHIR','JENIS_KELAMIN','ALAMAT', 
		 'NO_TELP','NO_HP','GOL_DARAH','PENDIDIKAN','STS_MARITAL','AGAMA','PEKERJAAN','JNS_KELAMIN',
		 'TAHUN','BULAN','HARI'
	];
	
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    // dataSource_viKasirIgd = new WebApp.DataStore
	// ({
        // fields: FieldMaster_viKasirIgd
    // });
    
	var dataSource_viKasirIgd = new Ext.data.Store({
	reader: new Ext.data.JsonReader
		({
			//fields: ['code',  'NoFaktur',  'Uraian',  {name: 'Tanggal',  type: 'date',  dateFormat: 'Y-m-d'},  'Dok', 'CdTR','Harga','Qty','DR','CR','TAG']
			fields: ['KD_PASIEN',  'NAMA',  'ALAMAT',  'TGL_KUNJUNGAN',  'NAMA_UNIT']
		}),
	data: 
		[
			{KD_PASIEN: '0-00-00-22', NAMA: '-', ALAMAT: 'Jl. Pelajar Pejuang 45 No. 43', TGL_KUNJUNGAN: ShowDate('21-10-2014'), NAMA_UNIT: 'UGD'}
			// {field1: 'C', field2: 'D'},
			// {field1: 'E', field2: 'E'}
		]
	});
	
    // Grid Kasir IGD # --------------
	var GridDataView_viKasirIgd = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viKasirIgd,
			autoScroll: true,
			columnLines: true,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viKasirIgd = undefined;
							rowSelected_viKasirIgd = dataSource_viKasirIgd.getAt(row);
							CurrentData_viKasirIgd
							CurrentData_viKasirIgd.row = row;
							CurrentData_viKasirIgd.data = rowSelected_viKasirIgd.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viKasirIgd = dataSource_viKasirIgd.getAt(ridx);
					if (rowSelected_viKasirIgd != undefined)
					{
						setLookUp_viKasirIgd(rowSelected_viKasirIgd.data);
					}
					else
					{
						setLookUp_viKasirIgd();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Kasir IGD
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colNoMedrec_viKasirIgd',
						header: 'No. Medrec',
						dataIndex: 'KD_PASIEN',
						sortable: true,
						width: 35,
						filter:
						{
							type: 'int'
						}
					},
					//-------------- ## --------------
					{
						id: 'colNMPASIEN_viKasirIgd',
						header: 'Nama',
						dataIndex: 'NAMA',
						sortable: true,
						width: 35,
						filter:
						{}
					},
					//-------------- ## --------------
					{
						id: 'colALAMAT_viKasirIgd',
						header:'Alamat',
						dataIndex: 'ALAMAT',
						width: 60,
						sortable: true,
						filter: {}
					},
					//-------------- ## --------------
					{
						id: 'colTglKunj_viKasirIgd',
						header:'Tgl Kunjungan',
						dataIndex: 'TGL_KUNJUNGAN',						
						width: 50,
						sortable: true,
						// format: 'd/M/Y',
						filter: {},
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.TGL_KUNJUNGAN);
						}
					},
					//-------------- ## --------------
					{
						id: 'colPoliTujuan_viKasirIgd',
						header:'Poli Tujuan',
						dataIndex: 'NAMA_UNIT',
						width: 50,
						sortable: true,
						filter: {}
					}
					//-------------- ## --------------
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viKasirIgd',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viKasirIgd',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viKasirIgd != undefined)
							{
								setLookUp_viKasirIgd(rowSelected_viKasirIgd.data)
								GetPasienData()
							}
							else
							{								
								setLookUp_viKasirIgd();
							}
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			bbar : bbar_paging(mod_name_viKasirIgd, selectCount_viKasirIgd, dataSource_viKasirIgd),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viKasirIgd = new Ext.Panel
    (
		{
			title: NamaForm_viKasirIgd,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viKasirIgd,
			region: 'center',
			layout: 'form', 
			closable: true,        
			border: false,  
			margins: '0 5 5 0',
			items: [GridDataView_viKasirIgd],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viKasirIgd,
		            columns: 9,
		            defaults: {
		                scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
			            { 
							xtype: 'tbtext', 
							text: 'Jenis Transaksi : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},
						viComboJenisTransaksi_MediSmart(125, "ComboJenisTransaksi_viKasirIgd"),	
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'No. : ', 
							style:{'text-align':'right'},
							width: 45,
							height: 25
						},
						{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_NoMedrec_viKasirIgd',
							emptyText: 'No. Medrec',
							width: 300,
							height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										DfltFilterBtn_viKasirIgd = 1;
										DataRefresh_viKasirIgd(getCriteriaFilterGridDataView_viKasirIgd());
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Tgl Kunjungan : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},
						{
							xtype: 'datefield',
							id: 'txtfilterTglKunjAwal_viKasirIgd',
							value: now_viKasirIgd,
							format: 'd/M/Y',
							width: 120,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viKasirIgd();								
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Kelompok : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},
						Vicbo_Kelompok(125, "ComboKelompok_viKasirIgd"),	
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Nama : ', 
							style:{'text-align':'right'},
							width: 45,
							height: 25
						},
						{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_NAMA_viKasirIgd',
							emptyText: 'Nama',
							width: 300,
							height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										DfltFilterBtn_viKasirIgd = 1;
										DataRefresh_viKasirIgd(getCriteriaFilterGridDataView_viKasirIgd());								
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 's/d Tgl : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},
						{
							xtype: 'datefield',
							id: 'txtfilterTglKunjAkhir_viKasirIgd',
							value: now_viKasirIgd,
							format: 'd/M/Y',
							width: 120,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viKasirIgd();								
									} 						
								}
							}
						},
						//-------------- ## --------------
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Poli Tujuan : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},
						viCombo_POLI(125, "ComboPoli_viKasirIgd"),
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Alamat : ', 
							style:{'text-align':'right'},
							width: 45,
							height: 25
						},
						{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_ALAMAT_viKasirIgd',
							emptyText: 'Alamat',
							width: 300,
							height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										DfltFilterBtn_viKasirIgd = 1;
										DataRefresh_viKasirIgd(getCriteriaFilterGridDataView_viKasirIgd());								
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{
							xtype: 'button',
							text: 'Cari',
							iconCls: 'refresh',
							tooltip: 'Cari',
							style:{paddingLeft:'30px'},
							//rowspan: 3,
							width:150,
							id: 'BtnFilterGridDataView_viKasirIgd',
							handler: function() 
							{					
								DfltFilterBtn_viKasirIgd = 1;
								DataRefresh_viKasirIgd(getCriteriaFilterGridDataView_viKasirIgd());
							}                        
						},
						//-------------- ## --------------
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viKasirIgd;
    //-------------- # End form filter # --------------
}
// End Function dataGrid_viKasirIgd # --------------

/**
*	Function : setLookUp_viKasirIgd
*	
*	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
*/

function setLookUp_viKasirIgd(rowdata)
{
    var lebar = 760;
    setLookUps_viKasirIgd = new Ext.Window
    (
    {
        id: 'SetLookUps_viKasirIgd',
		name: 'SetLookUps_viKasirIgd',
        title: NamaForm_viKasirIgd, 
        closeAction: 'destroy',        
        width: 775,
        height: 610,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viKasirIgd(lebar,rowdata), //1
        listeners:
        {
            activate: function()
            {
				
            },
            afterShow: function()
            {
                this.activate();
            },
            deactivate: function()
            {
                rowSelected_viKasirIgd=undefined;
                //datarefresh_viKasirIgd();
				mNoKunjungan_viKasirIgd = '';
            }
        }
    }
    );

    setLookUps_viKasirIgd.show();
    if (rowdata == undefined)
    {
        // dataaddnew_viKasirIgd();
		// Ext.getCmp('btnDelete_viKasirIgd').disable();	
    }
    else
    {
        // datainit_viKasirIgd(rowdata);
    }
}
// End Function setLookUpGridDataView_viKasirIgd # --------------

/**
*	Function : getFormItemEntry_viKasirIgd
*	
*	Sebuah fungsi untuk menampilkan penataan menu dan isian form edit
*/

function getFormItemEntry_viKasirIgd(lebar,rowdata)
{
    var pnlFormDataBasic_viKasirIgd = new Ext.FormPanel
    (
		{
			title: '',
			region: 'north',
			layout: 'form',
			bodyStyle: 'padding:10px 10px 10px 10px',
			anchor: '100%',
			width: lebar,
			border: false,
			//-------------- #items# --------------
			items:
			[
				getItemPanelInputBiodata_viKasirIgd(lebar), 
				//-------------- ## --------------
				getItemDataKunjungan_viKasirIgd(lebar), 
				//-------------- ## --------------
				getItemGridTransaksi_viKasirIgd(lebar),
				//-------------- ## --------------
				{
					xtype: 'compositefield',
					fieldLabel: ' ',
					anchor: '100%',
					labelSeparator: '',
					//width: 199,
					style:{'margin-top':'7px'},
					items: 
					[
		                {
                            xtype: 'textfield',
                            id: 'txtJumlah1EditData_viKasirIgd',
                            name: 'txtJumlah1EditData_viKasirIgd',
							style:{'text-align':'right','margin-left':'420px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
		                //-------------- ## --------------
		                {
                            xtype: 'textfield',
                            id: 'txtJumlah2EditData_viKasirIgd',
                            name: 'txtJumlah2EditData_viKasirIgd',
							style:{'text-align':'right','margin-left':'416px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
		                //-------------- ## --------------
					]
				},
				//-------------- ## --------------
			],
			//-------------- #End items# --------------
			fileUpload: true,
			// Tombol pada tollbar Edit Data Pasien
			tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						id: 'btnAdd_viKasirIgd',
						handler: function(){
							dataaddnew_viKasirIgd();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viKasirIgd',
						handler: function()
						{
							datasave_viKasirIgd(addNew_viKasirIgd);
							datarefresh_viKasirIgd();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						id: 'btnSimpanExit_viKasirIgd',
						handler: function()
						{
							var x = datasave_viKasirIgd(addNew_viKasirIgd);
							datarefresh_viKasirIgd();
							if (x===undefined)
							{
								setLookUps_viKasirIgd.close();
							}
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viKasirIgd',
						handler: function()
						{
							datadelete_viKasirIgd();
							datarefresh_viKasirIgd();
							
						}
					},
					//-------------- ## --------------
					{
						xtype:'tbseparator'
					},
					//-------------- ## --------------
					{
		                text: 'Lookup ',
		                iconCls:'find',
		                menu: 
		                [
		                	{
						        text: 'Lookup Produk/Tindakan',
						        id:'btnLookUpProduk_viKasirIgd',
						        iconCls: 'find',
						        handler: function(){
						            FormSetLookupProduk_viKasirIgd('1','2');
						        },
					    	},
					    	//-------------- ## --------------
        					{
						        text: 'Lookup Konsultasi',
						        id:'btnLookUpKonsultasi_viKasirIgd',
						        iconCls: 'find',
						        handler: function(){
						            FormSetLookupKonsultasi_viKasirIgd('1','2');
						        },
					    	},
					    	//-------------- ## --------------
					    	{
						        text: 'Lookup Ganti Dokter',
						        id:'btnLookUpGantiDokter_viKasirIgd',
						        iconCls: 'find',
						        handler: function(){
						            FormSetLookupGantiDokter_viKasirIgd('1','2');
						        },
					    	},
					    	//-------------- ## --------------
		                ],
		            },
					//-------------- ## --------------
					{
						xtype:'tbseparator'
					},
					//-------------- ## --------------
					{
		                text: 'Pembayaran ',
		                iconCls:'print',
		                menu: 
		                [
		                	{
						        text: 'Pembayaran',
						        id:'btnPembayaran_viKasirIgd',
						        iconCls: 'print',
						        handler: function(){
						            FormSetPembayaran_viKasirIgd('1','2');
						        },
					    	},
					    	//-------------- ## --------------
        					{
						        text: 'Transfer ke Rawat Inap',
						        id:'btnTransferPembayaran_viKasirIgd',
						        iconCls: 'print',
						        handler: function(){
						            FormSetTransferPembayaran_viKasirIgd('1','2');
						        },
					    	},
					    	//-------------- ## --------------
					    	{
						        text: 'Edit Tarif',
						        id:'btnEditTarif_viKasirIgd',
						        iconCls: 'print',
						        handler: function(){
						            FormSetEditTarif_viKasirIgd('1','2');
						        },
					    	},
					    	//-------------- ## --------------
					    	{
						        text: 'Posting Manual',
						        id:'btnPostingManual_viKasirIgd',
						        iconCls: 'print',
						        handler: function(){
						            FormSetPostingManual_viKasirIgd('1','2');
						        },
					    	},
					    	//-------------- ## --------------
		                ],
		            },
					//-------------- ## --------------
				]
			}//,items:
		}
    )

    return pnlFormDataBasic_viKasirIgd;
}
// End Function getFormItemEntry_viKasirIgd # --------------

/**
*	Function : getItemPanelInputBiodata_viKasirIgd
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemPanelInputBiodata_viKasirIgd(lebar) 
{
    var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		border:true,
		items:
		[			
			{
				xtype: 'compositefield',
				fieldLabel: 'No. Medrec',
				anchor: '100%',
				//labelSeparator: '',
				width: 199,
				items: 
				[
					{
						xtype: 'textfield',
						flex: 1,
						fieldLabel: 'Label',
						width: 100,
						name: 'txtNoMedrec_viKasirIgd',
						id: 'txtNoMedrec_viKasirIgd',
						style:{'text-align':'right'},
						emptyText: 'No. Medrec',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									GetPasienData()									
									// var strKriteria='';
									// strKriteria=" WHERE RIGHT(PS.KD_PASIEN,10) like '%" + Ext.get('txtKDPasien_viDaftar').dom.value + "%' and ps.kd_customer='" + strKD_CUST +"' "	
									// FormLookupPasien(strKriteria,nFormPendaftaran,Ext.get('txtKDPasien_viDaftar').getValue());
									
								};
							}
						}
					},
					//-------------- ## --------------
					{
						xtype: 'displayfield',
						flex: 1,
						width: 35,
						name: '',
						value: 'Nama:',
						fieldLabel: 'Label'
					},
					{
						xtype: 'textfield',
						flex: 1,
						fieldLabel: 'Label',														
						width : 465,	
						name: 'txtNama_viKasirIgd',
						id: 'txtNama_viKasirIgd',
						emptyText: 'Nama',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					}
					//-------------- ## --------------
				]
			},	
			//-------------- ## --------------				
			{
				xtype: 'compositefield',
				fieldLabel: 'No. Register',
				name: 'compNoreg_viKasirIgd',
				id: 'compNoreg_viKasirIgd',
				items: 
				[
					{
						xtype: 'textfield',					
						fieldLabel: '',								
						anchor: '100%',
						width: 100,
						name: 'txtNoreg_viKasirIgd',
						id: 'txtNoreg_viKasirIgd',
						emptyText: 'No. Register',
					},
					//-------------- ## --------------
					{
						xtype: 'displayfield',				
						width: 80,								
						value: 'No. Transaksi:',
						fieldLabel: 'Label',
					},
					{
						xtype: 'textfield',					
						fieldLabel: '',								
						width: 246,
						name: 'txtNotrans_viKasirIgd',
						id: 'txtNotrans_viKasirIgd',
						emptyText: 'No. Transaksi',
					},
					//-------------- ## --------------
					{
						xtype: 'displayfield',				
						width: 54,								
						value: 'Tanggal:',
						fieldLabel: 'Label',
					},
					{
						xtype: 'datefield',					
						fieldLabel: 'Tanggal',
						name: 'DtpTglLahir_viKasirIgd',
						id: 'DtpTglLahir_viKasirIgd',
						format: 'd/M/Y',
						width: 110,
						value: now_viKasirIgd,
					}
					//-------------- ## --------------
				]
			},
			//-------------- ## --------------	
			{
				xtype: 'compositefield',
				fieldLabel: 'Unit',
				name: 'compUnit_viKasirIgd',
				id: 'compUnit_viKasirIgd',
				items: 
				[
					{
						xtype: 'textfield',					
						fieldLabel: '',								
						width: 74,
						name: 'txtUnit_viKasirIgd',
						id: 'txtUnit_viKasirIgd',
						emptyText: 'Kode Unit',
					},
					{
						xtype: 'textfield',					
						fieldLabel: '',								
						width: 357,
						name: 'txtNmUnit_viKasirIgd',
						id: 'txtNmUnit_viKasirIgd',
						emptyText: 'Nama Unit',
					},
					//-------------- ## --------------
					{
						xtype: 'displayfield',				
						width: 55,								
						value: 'Kelompok:',
						fieldLabel: 'Label',
						id: 'lblKelompok_viKasirIgd',
						name: 'lblKelompok_viKasirIgd'
					},
					{
						xtype: 'textfield',					
						fieldLabel: '',								
						width: 110,
						name: 'txtKelompok_viKasirIgd',
						id: 'txtKelompok_viKasirIgd',
						emptyText: 'Kelompok',
					}
					//-------------- ## --------------
				]
			},
			//-------------- ## --------------	
			{
				xtype: 'compositefield',
				fieldLabel: 'Dokter',
				name: 'compDokter_viKasirIgd',
				id: 'compDokter_viKasirIgd',
				items: 
				[
					{
						xtype: 'textfield',					
						fieldLabel: '',								
						width: 74,
						name: 'txtDokter_viKasirIgd',
						id: 'txtDokter_viKasirIgd',
						emptyText: 'Kode Dokter',
					},
					{
						xtype: 'textfield',					
						fieldLabel: '',								
						width: 533,
						name: 'txtNmDokter_viKasirIgd',
						id: 'txtNmDokter_viKasirIgd',
						emptyText: 'Nama Dokter',
					}
				]
			}
			//-------------- ## --------------
		]
	};
    return items;
};
// End Function getItemPanelInputBiodata_viKasirIgd # --------------

/**
*	Function : getItemDataKunjungan_viKasirIgd
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemDataKunjungan_viKasirIgd(lebar) 
{
	var rdJenisKunjungan_viKasirIgd = new Ext.form.RadioGroup
	({  
        fieldLabel: '',  
        columns: 2, 
        width: 400,
		border:false,
        items: 
		[  
			{
				boxLabel: 'Datang Senidri', 
				width:70,
				id: 'rdDatangSendiri_viKasirIgd', 
				name: 'rdDatangSendiri_viKasirIgd', 
				inputValue: '1'
			},  
			//-------------- ## --------------
			{
				boxLabel: 'Rujukan', 
				width:70,
				id: 'rdDatangRujukan_viKasirIgd', 
				name: 'rdDatangRujukan_viKasirIgd', 
				inputValue: '2'
			} 
			//-------------- ## --------------
        ]  
    });
	//-------------- ## --------------

    var items =
	{
		layout: 'form',
		anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',		
		width: lebar-80,
		height:110,
	    items:
		[	
			rdJenisKunjungan_viKasirIgd,
			{
				xtype: 'compositefield',
				fieldLabel: 'Dari',	
				labelAlign: 'Left',
				//labelSeparator: '',
				labelWidth:70,
				defaults: 
				{
					flex: 1
				},
				items: 
				[
					{
						xtype: 'textfield',
						flex: 1,
						fieldLabel: '',
						name: 'txtdari_viKasirIgd',
						id: 'txtdari_viKasirIgd',								
						width: 100,
						emptyText: 'Dari',
					},
					//-------------- ## --------------
					{
					   xtype: 'displayfield',
					   value: 'Nama:',
					   //style:{'margin-top':'2px', 'text-align':'right'},
					   width:40
					},
					Vicbo_Dokter(463, "ComboDariNama_viKasirIgd"),					
					//-------------- ## --------------
					{ 
						xtype: 'tbspacer',
						width: 15,
						height: 23
					}					
					//-------------- ## --------------
				]
			},
			//-------------- ## --------------
			{
				xtype: 'compositefield',
				fieldLabel: 'Dokter',		
				labelAlign: 'Left',
				labelWidth:105,
				defaults: 
				{
					flex: 1
				},
				items: 
				[
					Vicbo_Kelompok(403, "ComboKelompokDaftar_viKasirIgd"),
					//-------------- ## --------------
					{
					   xtype: 'displayfield',
					   value: 'Tgl Kunjungan:',
					   //style:{'margin-top':'2px', 'text-align':'right'},
					   width:90
					},
					{
						xtype: 'datefield',								
						id: 'dtpKunjungan_viKasirIgd',
						format: 'd/M/Y',
						value: now_viKasirIgd,
						autoSchrol:false,
						width:110,
						onInit: function() { }				
					},
					//-------------- ## --------------
					{ 
						xtype: 'tbspacer',
						width: 15,
						height: 120
					}							
					//-------------- ## --------------
				]
			}
			//-------------- ## --------------
		]
	};
    return items;
};
// End Function getItemDataKunjungan_viKasirIgd # --------------

/**
*	Function : getItemGridTransaksi_viKasirIgd
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemGridTransaksi_viKasirIgd(lebar) 
{
    var items =
	{
		title: 'Detail Transaksi', 
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		border:true,
		width: lebar-80,
		height:265, 
	    items:
		[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:
				[
					gridDataViewEdit_viKasirIgd()
				]	
			}
			//-------------- ## --------------
		]
	};
    return items;
};
// End Function getItemGridTransaksi_viKasirIgd # --------------

/**
*	Function : gridDataViewEdit_viKasirIgd
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function gridDataViewEdit_viKasirIgd()
{  
    var FieldGrdKasir_viKasirIgd = 
	[
	];
	
    //dsDataGrdJab_viKasirIgd= new WebApp.DataStore
	// ({
		// fields: fieldgrdkasir_vikasirigd
    // });
	
	// metode 1 gagal !!!
	
	// dsDataGrdJab_viKasirIgd= new Ext.data.ArrayStore
	// ({
	// data:[
			// [1,"-","BBSR","Mindfire Solutions","21-10-2014","1","20.000","1","20.000","0"]
			// // [2,"Sangita Das","BBSR","Mindfire Solutions","8888888888","2009-09-16" ],
			// // [3,"Arya Acharya","BBSR","Mindfire Solutions","7777777777","2007-06-26"],
			// // [4,"Bhumi","BBSR","Mindfire Solutions","6666666666","2008-03-06"],
			// // [5,"Smruti Das","BBSR","Mindfire Solutions","5555555555","2009-02-16"]
			// ],
			 
			// reader: new Ext.data.ArrayReader( { id: 'code' },
			// ['code',  'NoFaktur',  'Uraian',  {name: 'Tanggal',  type: 'date',  dateFormat: 'Y-m-d'},  'Dok', 'CdTR','Harga','Qty','DR','CR','TAG']
			// // ['id',  'User_Name',  'Location',  'Office',  'Mobile_No', {name: 'DOJ',  type: 'date',  dateFormat: 'Y-m-d'}]
			// ) 
			// });
	
	//metode 2
	var datastoreGrid = new Ext.data.Store({
	reader: new Ext.data.JsonReader
		({
			//fields: ['code',  'NoFaktur',  'Uraian',  {name: 'Tanggal',  type: 'date',  dateFormat: 'Y-m-d'},  'Dok', 'CdTR','Harga','Qty','DR','CR','TAG']
			fields: ['code',  'NoFaktur',  'Uraian',  'Tanggal',  'Dok', 'CdTR','Harga','Qty','DR','CR','TAG']
		}),
	data: 
		[
			{code: '1', NoFaktur: '-', Uraian: 'Angkat Jahitan < 5cm', Tanggal: '2014-10-01', Dok: '1', CdTR: '', Harga: '20.000', Qty: 1, DR: '20.000', CR: '0', TAG: ''},
			{code: '2', NoFaktur: '-', Uraian: 'Pasang Infus', Tanggal: '2014-10-01', Dok: '', CdTR: '', Harga: '10.000', Qty: 1, DR: '10.000', CR: '0', TAG: ''},
			{code: '3', NoFaktur: '-', Uraian: 'Pemeriksaan Dokter', Tanggal: '2014-10-01', Dok: '1', CdTR: '', Harga: '50.000', Qty: 1, DR: '50.000', CR: '0', TAG: ''},
			{code: '', NoFaktur: '-', Uraian: 'BPJS', Tanggal: '2014-10-01', Dok: '1', CdTR: '', Harga: '', Qty: '', DR: '', CR: '80.000', TAG: ''}
			// {field1: 'C', field2: 'D'},
			// {field1: 'E', field2: 'E'}
		]
	});
    
    var items = 
    {
        xtype: 'editorgrid',
        // store: dsDataGrdJab_viKasirIgd,
		store: datastoreGrid,
        height: 220,
		selModel: new Ext.grid.RowSelectionModel
        (
	        {
	            singleSelect: true,
	            listeners:	
	            {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
	        }
        ),
        columns: 
		[
			{
				dataIndex: 'code',
				header: 'Code',
				sortable: true,
				width: 40
			},
			//-------------- ## --------------
			{
				dataIndex: 'NoFaktur',
				header: 'No. Faktur',
				sortable: true,
				width: 70
			},
			//-------------- ## --------------			
			{			
				dataIndex: 'Uraian',
				header: 'Uraian',
				sortable: true,
				width: 180
			},
			//-------------- ## --------------
			{
				dataIndex: 'Tanggal',
				header: 'Tanggal',
				sortable: true,
				width: 70
				//renderer: function(v, params, record) 
				// {
					
				// }			
			},
			//-------------- ## --------------
			{
				dataIndex: 'Dok',
				header: 'Dok',
				sortable: true,
				width: 30
				// renderer: function(v, params, record) 
				// {
					
				// }	
			},
			//-------------- ## --------------
			{
				dataIndex: 'CdTR',
				header: 'Cd. TR',
				sortable: true,
				width: 0
				// renderer: function(v, params, record) 
				// {
					
				// }	
			},
			//-------------- ## --------------
			{
				dataIndex: 'Harga',
				header: 'Harga/Unit',
				sortable: true,
				width: 100
				// renderer: function(v, params, record) 
				// {
					
				// }	
			},
			//-------------- ## --------------
			{
				dataIndex: 'Qty',
				header: 'Qty',
				sortable: true,
				width: 30
				// renderer: function(v, params, record) 
				// {
					
				// }	
			},
			//-------------- ## --------------
			{
				dataIndex: 'DR',
				header: 'DR',
				sortable: true,
				width: 75
				// renderer: function(v, params, record) 
				// {
					
				// }	
			},
			//-------------- ## --------------
			{
				dataIndex: 'CR',
				header: 'CR',
				sortable: true,
				width: 75
				// renderer: function(v, params, record) 
				// {
					
				// }	
			},
			//-------------- ## --------------
			{
				dataIndex: 'TAG',
				header: 'Tag',
				sortable: true,
				width: 30
				// renderer: function(v, params, record) 
				// {
					
				// }	
			}
			//-------------- ## --------------
        ]
    }    
    return items;
}
// End Function gridDataViewEdit_viKasirIgd # --------------

// ## MENU LOOKUP ## ------------------------------------------------------------------

// ## MENU LOOKUP - LOOKUP PRODUK/ TINDAKAN ## ------------------------------------------------------------------

/**
*   Function : FormSetLookupProduk_viKasirIgd
*   
*   Sebuah fungsi untuk menampilkan windows popup List Produk
*/

function FormSetLookupProduk_viKasirIgd(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryLookupProduk_viKasirIgd = new Ext.Window
    (
        {
            id: 'FormGrdLookupProduk_viKasirIgd',
            title: 'List Produk',
            closeAction: 'destroy',
            closable:true,
            width: 760,
            height: 473,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
			//padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryProdukGridDataView_viKasirIgd(subtotal,NO_KUNJUNGAN),
                //-------------- ## --------------
            ],
            tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						id: 'btnAddTreeProduk_viKasirIgd',
						handler: function()
						{
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDeleteTreeProduk_viKasirIgd',
						handler: function()
						{
						}
					},
					//-------------- ## --------------
				]
			}
        }
    );
    vWinFormEntryLookupProduk_viKasirIgd.show();
    mWindowGridLookupLookupProduk  = vWinFormEntryLookupProduk_viKasirIgd; 
}
// End Function FormSetLookupProduk_viKasirIgd # --------------

/**
*   Function : getFormItemEntryProdukGridDataView_viKasirIgd
*   
*   Sebuah fungsi untuk menampilkan isian form produk
*/
function getFormItemEntryProdukGridDataView_viKasirIgd(subtotal,NO_KUNJUNGAN)
{
	var lebar = 515;
	var itemsListProdukGridDataView_viKasirIgd = 
    {
        xtype: 'panel',
        title: '',
        name: 'PanelListProdukGridDataView_viKasirIgd',
        id: 'PanelListProdukGridDataView_viKasirIgd',        
        bodyStyle: 'padding: 5px;',
        layout: 'hbox',
        height: 1170,
        border:false,
        items: 
        [           
            itemsTreeListProdukGridDataView_viKasirIgd(),
            //-------------- ## -------------- 
            { 
                xtype: 'tbspacer',
                width: 5,
            },
            //-------------- ## -------------- 
            getItemTreeGridTransaksiGridDataView_viKasirIgd(lebar),
            //-------------- ## -------------- 
        ]
    }
    return itemsListProdukGridDataView_viKasirIgd;
}
// End Function getFormItemEntryProdukGridDataView_viKasirIgd # --------------

/**
*   Function : itemsTreeListProdukGridDataView_viKasirIgd
*   
*   Sebuah fungsi untuk menampilkan tree prosuk
*/
function itemsTreeListProdukGridDataView_viKasirIgd()
{
	var children = 
	[
		{
		     text:'First Level Child 1',
		     children:
		     [
		     	{
		        	text:'Second Level Child 1',
		        	leaf:true
		        },
		        //-------------- ## -------------- 
		        {
		        	text:'Second Level Child 2',
		        	leaf:true
		        },
		        //-------------- ## -------------- 
		    ]
		},
		{
		     text:'First Level Child 2',
		     children:
		     [
		     	{
		        	text:'Second Level Child 1',
		        	leaf:true
		        },
		        //-------------- ## -------------- 
		        {
		        	text:'Second Level Child 2',
		        	leaf:true
		        },
		        //-------------- ## -------------- 
		    ]
		},
	];

    var tree = new Ext.tree.TreePanel
    (
    	{
        	loader:new Ext.tree.TreeLoader(),
        	width:200,
        	height:340,
        	renderTo:Ext.getBody(),
        	root:new Ext.tree.AsyncTreeNode
        	(
        		{
             		expanded:true,
             		leaf:false,
             		text:'Tree Root',
             		children:children
             	}
            )
    	}
    );

    var pnlTreeFormDataWindowPopup_viKasirIgd = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                {
		            xtype: 'radiogroup',
		            //fieldLabel: 'Single Column',
		            itemCls: 'x-check-group-alt',
		            columns: 1,
		            items: 
		            [
		                {
		                	boxLabel: 'Semua Unit', 
		                	name: 'ckboxTreeSemuaUnit_viKasirIgd',
		                	id: 'ckboxTreeSemuaUnit_viKasirIgd',  
		                	inputValue: 1,
		                },
		                //-------------- ## -------------- 
		                {
		                	boxLabel: 'Unit Penyakit Dalam', 
		                	name: 'ckboxTreePerUnit_viKasirIgd',
		                	id: 'ckboxTreePerUnit_viKasirIgd',  
		                	inputValue: 2, 
		                	checked: true,
		                },
		                //-------------- ## -------------- 
		            ]
		        },
				//-------------- ## -------------- 
                tree,
                //-------------- ## -------------- 
            ],
            //-------------- #End items# --------------
        }
    )

    return pnlTreeFormDataWindowPopup_viKasirIgd;
}
// End Function itemsTreeListProdukGridDataView_viKasirIgd # --------------

/**
*   Function : getItemTreeGridTransaksiGridDataView_viKasirIgd
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/

function getItemTreeGridTransaksiGridDataView_viKasirIgd(lebar) 
{
    var items =
    {
        //title: 'Detail Transaksi', 
        layout: 'form',
        anchor: '100%',     
        labelAlign: 'Left',
        bodyStyle: 'padding:2px 2px 2px 2px',
        border:true,
        width: lebar,
        height:402, 
        items:
        [
            {
                labelWidth:105,
                layout: 'form',
                labelAlign: 'Left',
                border: false,
                items:
                [
                    gridTreeDataViewEditGridDataView_viKasirIgd()
                ]   
            }
            //-------------- ## --------------
        ]
    };
    return items;
};
// End Function getItemTreeGridTransaksiGridDataView_viKasirIgd # --------------

/**
*   Function : gridTreeDataViewEditGridDataView_viKasirIgd
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/

function gridTreeDataViewEditGridDataView_viKasirIgd()
{
    
    var FieldTreeGrdKasirGridDataView_viKasirIgd = 
    [
    ];
    
    dsDataTreeGrdJabGridDataView_viKasirIgd= new WebApp.DataStore
    ({
        fields: FieldTreeGrdKasirGridDataView_viKasirIgd
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataTreeGrdJabGridDataView_viKasirIgd,
        height: 395,
        selModel: new Ext.grid.RowSelectionModel
        (
            {
                singleSelect: true,
                listeners:
                {
                    rowselect: function(sm, row, rec)
                    {
                    }
                }
            }
        ),
        columns: 
        [
            {
                dataIndex: '',
                header: 'Cek',
                sortable: true,
                width: 50
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Deskripsi Produk',
                sortable: true,
                width: 330,
                renderer: function(v, params, record) 
                {
                    
                }           
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Tarif',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }   
            },
            //-------------- ## --------------
        ]
    }    
    return items;
}
// End Function gridTreeDataViewEditGridDataView_viKasirIgd # --------------

// ## END MENU LOOKUP - LOOKUP PRODUK/ TINDAKAN ## ------------------------------------------------------------------

// ## MENU LOOKUP - LOOKUP KONSULTASI ## ------------------------------------------------------------------

/**
*   Function : FormSetLookupKonsultasi_viKasirIgd
*   
*   Sebuah fungsi untuk menampilkan windows popup Konsultasi
*/

function FormSetLookupKonsultasi_viKasirIgd(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryKonsultasi_viKasirIgd = new Ext.Window
    (
        {
            id: 'FormGrdLookupKonsultasi_viKasirIgd',
            title: 'Konsultasi',
            closeAction: 'destroy',
            closable:true,
            width: 390,
            height: 125,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
            //padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryLookupKonsultasi_viKasirIgd(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryKonsultasi_viKasirIgd.show();
    mWindowGridLookupKonsultasi  = vWinFormEntryKonsultasi_viKasirIgd; 
};

// End Function FormSetLookupKonsultasi_viKasirIgd # --------------

/**
*   Function : getFormItemEntryLookupKonsultasi_viKasirIgd
*   
*   Sebuah fungsi untuk menampilkan isian form konsultasi
*/
function getFormItemEntryLookupKonsultasi_viKasirIgd(subtotal,NO_KUNJUNGAN)
{
    var lebar = 500;
    var pnlFormDataKonsultasiWindowPopup_viKasirIgd = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                getItemPanelInputKonsultasiDataView_viKasirIgd(lebar),
                //-------------- ## --------------
            ],
            //-------------- #End items# --------------
        }
    )
    return pnlFormDataKonsultasiWindowPopup_viKasirIgd;
}
// End Function getFormItemEntryLookupKonsultasi_viKasirIgd # --------------

/**
*   Function : getItemPanelInputKonsultasiDataView_viKasirIgd
*   
*   Sebuah fungsi untuk menampilkan isian form konsultasi
*/

function getItemPanelInputKonsultasiDataView_viKasirIgd(lebar) 
{
    var items =
    {
        title: '',
        layout: 'Form',
        anchor: '100%',
        width: lebar,
        labelAlign: 'Left',
        bodyStyle: 'padding:1px 1px 1px 1px',
        border:false,
        items:
        [           
            {
                xtype: 'compositefield',
                fieldLabel: 'Asal Unit',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_MediSmart(250,'CmboAsalUnit_viKasirIgd'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
            {
                xtype: 'compositefield',
                fieldLabel: 'Konsultasi ke',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_MediSmart(250,'CmboKonsultasike_viKasirIgd'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
            {
                xtype: 'compositefield',
                fieldLabel: 'Dokter',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_MediSmart(250,'CmboDokter_viKasirIgd'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
        ]
    };
    return items;
}
// End Function getItemPanelInputKonsultasiDataView_viKasirIgd # --------------

// ## END MENU LOOKUP - LOOKUP KONSULTASI ## ------------------------------------------------------------------

// ## MENU LOOKUP - LOOKUP GANTI DOKTER ## ------------------------------------------------------------------

/**
*   Function : FormSetLookupGantiDokter_viKasirIgd
*   
*   Sebuah fungsi untuk menampilkan windows popup Ganti Dokter
*/

function FormSetLookupGantiDokter_viKasirIgd(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryGantiDokter_viKasirIgd = new Ext.Window
    (
        {
            id: 'FormGrdLookupGantiDokter_viKasirIgd',
            title: 'Ganti Dokter',
            closeAction: 'destroy',
            closable:true,
            width: 390,
            height: 125,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
            //padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryLookupGantiDokter_viKasirIgd(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryGantiDokter_viKasirIgd.show();
    mWindowGridLookupGantiDokter  = vWinFormEntryGantiDokter_viKasirIgd; 
};

// End Function FormSetLookupGantiDokter_viKasirIgd # --------------

/**
*   Function : getFormItemEntryLookupGantiDokter_viKasirIgd
*   
*   Sebuah fungsi untuk menampilkan isian form Ganti Dokter
*/
function getFormItemEntryLookupGantiDokter_viKasirIgd(subtotal,NO_KUNJUNGAN)
{
    var lebar = 500;
    var pnlFormDataGantiDokterWindowPopup_viKasirIgd = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                getItemPanelInputGantiDokterDataView_viKasirIgd(lebar),
                //-------------- ## --------------
            ],
            //-------------- #End items# --------------
        }
    )
    return pnlFormDataGantiDokterWindowPopup_viKasirIgd;
}
// End Function getFormItemEntryLookupGantiDokter_viKasirIgd # --------------

/**
*   Function : getItemPanelInputGantiDokterDataView_viKasirIgd
*   
*   Sebuah fungsi untuk menampilkan isian form Ganti Dokter
*/

function getItemPanelInputGantiDokterDataView_viKasirIgd(lebar) 
{
    var items =
    {
        title: '',
        layout: 'Form',
        anchor: '100%',
        width: lebar,
        labelAlign: 'Left',
        bodyStyle: 'padding:1px 1px 1px 1px',
        border:false,
        items:
        [           
            {
                xtype: 'compositefield',
                fieldLabel: 'Tanggal',
                anchor: '100%',
                width: 250,
                items: 
                [
                    {
                        xtype: 'textfield',
                        id: 'TxtTglGantiDokter_viKasirIgd',
                        emptyText: '01',
                        width: 30,
                    },
                    //-------------- ## --------------  
                    {
                        xtype: 'textfield',
                        id: 'TxtBlnGantiDokter_viKasirIgd',
                        emptyText: '01',
                        width: 30,
                    },
                    //-------------- ## --------------  
                    {
                        xtype: 'textfield',
                        id: 'TxtThnGantiDokter_viKasirIgd',
                        emptyText: '2014',
                        width: 50,
                    },
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
            {
                xtype: 'compositefield',
                fieldLabel: 'Dokter Asal',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_MediSmart(250,'CmboDokterAsal_viKasirIgd'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
            {
                xtype: 'compositefield',
                fieldLabel: 'Dokter Ganti',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_MediSmart(250,'CmboDokterGanti_viKasirIgd'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
        ]
    };
    return items;
}
// End Function getItemPanelInputGantiDokterDataView_viKasirIgd # --------------

// ## END MENU LOOKUP - LOOKUP GANTI DOKTER ## ------------------------------------------------------------------

// ## END MENU LOOKUP ## ------------------------------------------------------------------

// ## MENU PEMBAYARAN ## ------------------------------------------------------------------

// ## MENU PEMBAYARAN - PEMBAYARAN ## ------------------------------------------------------------------

/**
*   Function : FormSetPembayaran_viKasirIgd
*   
*   Sebuah fungsi untuk menampilkan windows popup Pembayaran
*/

function FormSetPembayaran_viKasirIgd(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryPembayaran_viKasirIgd = new Ext.Window
    (
        {
            id: 'FormGrdPembayaran_viKasirIgd',
            title: 'Pembayaran',
            closeAction: 'destroy',
            closable:true,
            width: 760,
            height: 473,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
			//padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryGridDataView_viKasirIgd(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                {
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryPembayaran_viKasirIgd.show();
    mWindowGridLookupPembayaran  = vWinFormEntryPembayaran_viKasirIgd; 
};

// End Function FormSetPembayaran_viKasirIgd # --------------

/**
*   Function : getFormItemEntryGridDataView_viKasirIgd
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/
function getFormItemEntryGridDataView_viKasirIgd(subtotal,NO_KUNJUNGAN)
{
    var lebar = 760;
    var pnlFormDataWindowPopup_viKasirIgd = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
			items:
			[
				getItemPanelInputBiodataGridDataView_viKasirIgd(lebar),
				//-------------- ## -------------- 
				getItemGridTransaksiGridDataView_viKasirIgd(lebar),
				//-------------- ## --------------
				{
					xtype: 'compositefield',
					fieldLabel: ' ',
					anchor: '100%',
					//labelSeparator: '',
					width: 199,
					style:{'margin-top':'7px'},
					items: 
					[
						{
		                    xtype:'button',
		                    text:'Ok',
		                    width:70,
		                    //style:{'margin-left':'190px','margin-top':'7px'},
		                    hideLabel:true,
		                    id: 'btnOkGridTransaksiPembayaranGridDataView_viKasirIgd',
		                    handler:function()
		                    {
		                    }   
		                },
		                //-------------- ## --------------
		                {
		                    xtype:'button',
		                    text:'Cancel',
		                    width:70,
		                    //style:{'margin-left':'190px','margin-top':'7px'},
		                    hideLabel:true,
		                    id: 'btnCancelGridTransaksiPembayaranGridDataView_viKasirIgd',
		                    handler:function()
		                    {
		                    }   
		                },
		                //-------------- ## --------------
		                {
                            xtype: 'textfield',
                            id: 'txtBiayaGridTransaksiPembayaranGridDataView_viKasirIgd',
                            name: 'txtBiayaGridTransaksiPembayaranGridDataView_viKasirIgd',
							style:{'text-align':'right','margin-left':'150px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
		                //-------------- ## --------------
		                {
                            xtype: 'textfield',
                            id: 'txtPiutangGridTransaksiPembayaranGridDataView_viKasirIgd',
                            name: 'txtPiutangGridTransaksiPembayaranGridDataView_viKasirIgd',
							style:{'text-align':'right','margin-left':'146px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
		                //-------------- ## --------------
		                {
                            xtype: 'textfield',
                            id: 'txtTunaiGridTransaksiPembayaranGridDataView_viKasirIgd',
                            name: 'txtTunaiGridTransaksiPembayaranGridDataView_viKasirIgd',
							style:{'text-align':'right','margin-left':'142px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
		                //-------------- ## --------------
		                {
                            xtype: 'textfield',
                            id: 'txtDiscGridTransaksiPembayaranGridDataView_viKasirIgd',
                            name: 'txtDiscGridTransaksiPembayaranGridDataView_viKasirIgd',
							style:{'text-align':'right','margin-left':'138px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
		                //-------------- ## --------------
					]
				},	
			],
			//-------------- #End items# --------------
        }
    )
    return pnlFormDataWindowPopup_viKasirIgd;
}
// End Function getFormItemEntryGridDataView_viKasirIgd # --------------

/**
*   Function : getItemPanelInputBiodataGridDataView_viKasirIgd
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/

function getItemPanelInputBiodataGridDataView_viKasirIgd(lebar) 
{
    var items =
    {
        title: 'Infromasi Pasien',
        layout: 'Form',
        anchor: '100%',
        width: lebar- 80,
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        items:
        [           
            {
				xtype: 'textfield',
				fieldLabel: 'No. Transaksi',
				id: 'txtNoTransaksiPembayaranGridDataView_viKasirIgd',
				name: 'txtNoTransaksiPembayaranGridDataView_viKasirIgd',
				emptyText: 'No. Transaksi',
				readOnly: true,
				flex: 1,
				width: 195,				
			},
			//-------------- ## --------------
			{
				xtype: 'textfield',
				fieldLabel: 'Nama Pasien',
				id: 'txtNamaPasienPembayaranGridDataView_viKasirIgd',
				name: 'txtNamaPasienPembayaranGridDataView_viKasirIgd',
				emptyText: 'Nama Pasien',
				flex: 1,
				anchor: '100%',					
			},
			//-------------- ## --------------
			{
				xtype: 'compositefield',
				fieldLabel: 'Pembayaran',
				anchor: '100%',
				width: 250,
				items: 
				[
					viComboJenisTransaksi_MediSmart(195,'CmboPembayaranGridDataView_viKasirIgd'),
					//-------------- ## --------------	
				]
			},
			//-------------- ## --------------
			{
				xtype: 'compositefield',
				fieldLabel: ' ',
				anchor: '100%',
				labelSeparator: '',
				width: 250,
				items: 
				[
					viComboJenisTransaksi_MediSmart(195,'CmboPembayaranDuaGridDataView_viKasirIgd'),
					//-------------- ## --------------	
				]
			},
			//-------------- ## --------------
        ]
    };
    return items;
}
// End Function getItemPanelInputBiodataGridDataView_viKasirIgd # --------------

/**
*   Function : getItemGridTransaksiGridDataView_viKasirIgd
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/

function getItemGridTransaksiGridDataView_viKasirIgd(lebar) 
{
    var items =
    {
        //title: 'Detail Transaksi', 
        layout: 'form',
        anchor: '100%',     
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        width: lebar-80,
        height:245, 
        items:
        [
            {
                labelWidth:105,
                layout: 'form',
                labelAlign: 'Left',
                border: false,
                items:
                [
                    gridDataViewEditGridDataView_viKasirIgd()
                ]   
            }
            //-------------- ## --------------
        ]
    };
    return items;
};
// End Function getItemGridTransaksiGridDataView_viKasirIgd # --------------

/**
*   Function : gridDataViewEditGridDataView_viKasirIgd
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/

function gridDataViewEditGridDataView_viKasirIgd()
{
    
    var FieldGrdKasirGridDataView_viKasirIgd = 
    [
    ];
    
    dsDataGrdJabGridDataView_viKasirIgd= new WebApp.DataStore
    ({
        fields: FieldGrdKasirGridDataView_viKasirIgd
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJabGridDataView_viKasirIgd,
        height: 220,
        selModel: new Ext.grid.RowSelectionModel
        (
            {
                singleSelect: true,
                listeners:
                {
                    rowselect: function(sm, row, rec)
                    {
                    }
                }
            }
        ),
        columns: 
        [
            {
                dataIndex: '',
                header: 'Kode',
                sortable: true,
                width: 50
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Deskripsi',
                sortable: true,
                width: 200
            },
            //-------------- ## --------------          
            {           
                dataIndex: '',
                header: 'Qty',
                sortable: true,
                width: 40
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Biaya',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }           
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Piutang',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }   
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Tunai',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }   
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Disc',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }   
            },
            //-------------- ## --------------
        ]
    }    
    return items;
}
// End Function gridDataViewEditGridDataView_viKasirIgd # --------------

// ## END MENU PEMBAYARAN - PEMBAYARAN ## ------------------------------------------------------------------

// ## MENU PEMBAYARAN - TRANSFER KE RAWAT INAP ## ------------------------------------------------------------------

/**
*   Function : FormSetTransferPembayaran_viKasirIgd
*   
*   Sebuah fungsi untuk menampilkan windows popup Transfer ke Rawat Inap
*/

function FormSetTransferPembayaran_viKasirIgd(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryTransferPembayaran_viKasirIgd = new Ext.Window
    (
        {
            id: 'FormGrdTrnsferPembayaran_viKasirIgd',
            title: 'Pemindahan Tagihan Ke Unit Rawat Inap',
            closeAction: 'destroy',
            closable:true,
            width: 380,
            height: 450,
            border: false,
            plain: true,
            resizable:false,
            layout: 'form',
            iconCls: 'Edit_Tr',
            constrainHeader : true,
            //padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormTransferPembayaranItemEntryGridDataView_viKasirIgd(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryTransferPembayaran_viKasirIgd.show();
    mWindowGridLookupTransferPembayaran  = vWinFormEntryTransferPembayaran_viKasirIgd; 
};

// End Function FormSetTransferPembayaran_viKasirIgd # --------------

/**
*   Function : getFormTransferPembayaranItemEntryGridDataView_viKasirIgd
*   
*   Sebuah fungsi untuk menampilkan isian form transfer ke rawat inap
*/
function getFormTransferPembayaranItemEntryGridDataView_viKasirIgd(subtotal,NO_KUNJUNGAN)
{
    var pnlFormTransferPembayaranDataWindowPopup_viKasirIgd = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            items:
            //-------------- #items# --------------
            [
                {
                    xtype: 'fieldset',
                    title: 'Tagihan dipindahkan ke',
                    frame: false,
                    width: 350,
                    height: 165,
                    items: 
                    [
                        {
                            xtype: 'textfield',
                            fieldLabel: 'No. Transaksi',
                            id: 'txtNoTransaksiTransaksiTransferPembayaran_viKasirIgd',
                            name: 'txtNoTransaksiTransaksiTransferPembayaran_viKasirIgd',
                            width: 130,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
							xtype: 'compositefield',
							fieldLabel: 'Tgl. Lahir',
							anchor: '100%',
							width: 130,
							items: 
							[
								{
									xtype: 'datefield',
									fieldLabel: 'Tanggal',
									id: 'DateTransaksiTransferPembayaran_viKasirIgd',
									name: 'DateTransaksiTransferPembayaran_viKasirIgd',
									width: 130,
									format: 'd/M/Y',
								}
								//-------------- ## --------------				
							]
						},
						//-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'No. Rekam Medis',
                            id: 'txtNoRekamMedisTransaksiTransferPembayaran_viKasirIgd',
                            name: 'txtNoRekamMedisTransaksiTransferPembayaran_viKasirIgd',
                            width: 130,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Nama Pasien',
                            id: 'txtNamaPasienTransaksiTransferPembayaran_viKasirIgd',
                            name: 'txtNamaPasienTransaksiTransferPembayaran_viKasirIgd',
                            width: 222,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Unit Perawatan',
                            id: 'txtUnitPerawatanTransaksiTransferPembayaran_viKasirIgd',
                            name: 'txtUnitPerawatanTransaksiTransferPembayaran_viKasirIgd',
                            width: 222,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                    ]
                },
                //-------------- ## --------------
                {
                    xtype: 'fieldset',
                    title: '',
                    frame: false,
                    width: 350,
                    height: 125,
                    items: 
                    [
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Jumlah biaya',
                            id: 'txtJumlhBiayaTransaksiTransferPembayaran_viKasirIgd',
                            name: 'txtJumlhBiayaTransaksiTransferPembayaran_viKasirIgd',
                            width: 180,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'PAID',
                            id: 'txtPAIDTransaksiTransferPembayaran_viKasirIgd',
                            name: 'txtPAIDTransaksiTransferPembayaran_viKasirIgd',
                            width: 180,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Jmlh dipindahkan',
                            id: 'txtJumlahDipindahkanTransaksiTransferPembayaran_viKasirIgd',
                            name: 'txtJumlahDipindahkanTransaksiTransferPembayaran_viKasirIgd',
                            width: 180,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Saldo Tagihan',
                            id: 'txtSaldoTagihanTransaksiTransferPembayaran_viKasirIgd',
                            name: 'txtSaldoTagihanTransaksiTransferPembayaran_viKasirIgd',
                            width: 180,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                    ]
                },
                //-------------- ## --------------
                {
                    xtype: 'fieldset',
                    title: 'Alasan Transfer',
                    frame: false,
                    width: 350,
                    height: 62,
                    items: 
                    [
						{
					        xtype: 'panel',
					        title: '',
					        border:false,
					        items: 
					        [           
					            viComboJenisTransaksi_MediSmart(320,'CmboAlasanTransfer_viKasirIgd'),
					            //-------------- ## -------------- 
					        ]
					    }
                    ]
                },
                //-------------- ## --------------
                {
					xtype: 'compositefield',
					fieldLabel: ' ',
					anchor: '100%',
					width: 160,
					items: 
					[
						{
		                    xtype:'button',
		                    text:'Ok',
		                    width:70,
		                    style:{'margin-left':'205px'},
		                    hideLabel:true,
		                    id: 'btnOkTransferPembayaran_viKasirIgd',
		                    handler:function()
		                    {
		                        
		                    }   
		                },
						//-------------- ## --------------
						{
		                    xtype:'button',
		                    text:'Cancel',
		                    width:70,
		                    style:{'margin-left':'205px'},
		                    hideLabel:true,
		                    id: 'btnCancelTransferPembayaran_viKasirIgd',
		                    handler:function()
		                    {
		                        
		                    }   
		                },
						//-------------- ## --------------
					]
				},
                //-------------- ## --------------
            ]
            //-------------- #items# --------------
        }
    )
    return pnlFormTransferPembayaranDataWindowPopup_viKasirIgd;
}
// End Function getFormTransferPembayaranItemEntryGridDataView_viKasirIgd # --------------

// ## END MENU PEMBAYARAN - TRANSFER KE RAWAT INAP ## ------------------------------------------------------------------

// ## MENU PEMBAYARAN - EDIT TARIF ## ------------------------------------------------------------------

/**
*   Function : FormSetEditTarif_viKasirIgd
*   
*   Sebuah fungsi untuk menampilkan windows popup Edit Tarif
*/

function FormSetEditTarif_viKasirIgd(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryEditTarif_viKasirIgd = new Ext.Window
    (
        {
            id: 'FormGrdEditTarif_viKasirIgd',
            title: 'Edit Tarif',
            closeAction: 'destroy',
            closable:true,
            width: 475,
            height: 395,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
            //padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryEditTarifGridDataView_viKasirIgd(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryEditTarif_viKasirIgd.show();
    mWindowGridLookupEditTarif  = vWinFormEntryEditTarif_viKasirIgd; 
};

// End Function FormSetEditTarif_viKasirIgd # --------------

/**
*   Function : getFormItemEntryEditTarifGridDataView_viKasirIgd
*   
*   Sebuah fungsi untuk menampilkan isian form edit tarif
*/
function getFormItemEntryEditTarifGridDataView_viKasirIgd(subtotal,NO_KUNJUNGAN)
{
    var lebar = 760;
    var pnlFormDataEditTarifWindowPopup_viKasirIgd = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                getItemPanelInputProdukEditTarifGridDataView_viKasirIgd(lebar),
                //-------------- ## -------------- 
                getItemGridTransaksiEditTarifGridDataView_viKasirIgd(lebar),
                //-------------- ## --------------
                {
                    xtype: 'compositefield',
                    fieldLabel: ' ',
                    anchor: '100%',
                    //labelSeparator: '',
                    width: 199,
                    style:{'margin-top':'7px'},
                    items: 
                    [
                        {
                            xtype:'button',
                            text:'Ok',
                            width:70,
                            //style:{'margin-left':'190px','margin-top':'7px'},
                            hideLabel:true,
                            id: 'btnOkGridTransaksiEditTarifGridDataView_viKasirIgd',
                            handler:function()
                            {
                            }   
                        },
                        //-------------- ## --------------
                        {
                            xtype:'button',
                            text:'Cancel',
                            width:70,
                            //style:{'margin-left':'190px','margin-top':'7px'},
                            hideLabel:true,
                            id: 'btnCancelGridTransaksiEditTarifGridDataView_viKasirIgd',
                            handler:function()
                            {
                            }   
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            id: 'txtTarifLamaGridTransaksiEditTarifGridDataView_viKasirIgd',
                            name: 'txtTarifLamaGridTransaksiEditTarifGridDataView_viKasirIgd',
                            style:{'text-align':'right','margin-left':'60px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            id: 'txtTarifBaruGridTransaksiEditTarifGridDataView_viKasirIgd',
                            name: 'txtTarifBaruGridTransaksiEditTarifGridDataView_viKasirIgd',
                            style:{'text-align':'right','margin-left':'56px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                    ]
                },  
            ],
            //-------------- #End items# --------------
        }
    )
    return pnlFormDataEditTarifWindowPopup_viKasirIgd;
}
// End Function getFormItemEntryEditTarifGridDataView_viKasirIgd # --------------

/**
*   Function : getItemPanelInputProdukEditTarifGridDataView_viKasirIgd
*   
*   Sebuah fungsi untuk menampilkan isian form edit tarif
*/

function getItemPanelInputProdukEditTarifGridDataView_viKasirIgd(lebar) 
{
    var items =
    {
        title: 'Tarif Produk',
        layout: 'Form',
        anchor: '100%',
        width: lebar- 80,
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        items:
        [           
            {
                xtype: 'textfield',
                fieldLabel: 'Produk',
                id: 'txtProdukEditTarifGridDataView_viKasirIgd',
                name: 'txtProdukEditTarifGridDataView_viKasirIgd',
                emptyText: 'Produk',
                readOnly: true,
                flex: 1,
                width: 195,             
            },
            //-------------- ## --------------
        ]
    };
    return items;
}
// End Function getItemPanelInputProdukEditTarifGridDataView_viKasirIgd # --------------

/**
*   Function : getItemGridTransaksiEditTarifGridDataView_viKasirIgd
*   
*   Sebuah fungsi untuk menampilkan isian form edit tarif
*/

function getItemGridTransaksiEditTarifGridDataView_viKasirIgd(lebar) 
{
    var items =
    {
        title: 'Perubahan Tarif Menjadi', 
        layout: 'form',
        anchor: '100%',     
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        width: lebar,
        height:245, 
        items:
        [
            {
                labelWidth:105,
                layout: 'form',
                labelAlign: 'Left',
                border: false,
                items:
                [
                    gridDataViewEditGridEditTarifDataView_viKasirIgd()
                ]   
            }
            //-------------- ## --------------
        ]
    };
    return items;
};
// End Function getItemGridTransaksiEditTarifGridDataView_viKasirIgd # --------------

/**
*   Function : gridDataViewEditGridEditTarifDataView_viKasirIgd
*   
*   Sebuah fungsi untuk menampilkan isian form edit tarif
*/

function gridDataViewEditGridEditTarifDataView_viKasirIgd()
{
    
    var FieldGrdKasirGridDataView_viKasirIgd = 
    [
    ];
    
    dsDataGrdJabGridDataView_viKasirIgd= new WebApp.DataStore
    ({
        fields: FieldGrdKasirGridDataView_viKasirIgd
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJabGridDataView_viKasirIgd,
        height: 200,
        selModel: new Ext.grid.RowSelectionModel
        (
            {
                singleSelect: true,
                listeners:
                {
                    rowselect: function(sm, row, rec)
                    {
                    }
                }
            }
        ),
        columns: 
        [
            {
                dataIndex: '',
                header: 'Komponen',
                sortable: true,
                width: 200
            },
            //-------------- ## --------------          
            {           
                dataIndex: '',
                header: 'Tarif Lama',
                sortable: true,
                width: 100
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Tarif Baru',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }           
            },
            //-------------- ## --------------
        ]
    }    
    return items;
}
// End Function gridDataViewEditGridEditTarifDataView_viKasirIgd # --------------

// ## END MENU PEMBAYARAN - EDIT TARIF ## ------------------------------------------------------------------

// ## MENU PEMBAYARAN - POSTING MANUAL ## ------------------------------------------------------------------

/**
*   Function : FormSetPostingManual_viKasirIgd
*   
*   Sebuah fungsi untuk menampilkan windows popup Posting Manual
*/

function FormSetPostingManual_viKasirIgd(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryPostingManual_viKasirIgd = new Ext.Window
    (
        {
            id: 'FormGrdPostingManual_viKasirIgd',
            title: 'Posting Manual',
            closeAction: 'destroy',
            closable:true,
            width: 475,
            height: 395,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
            //padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryPostingManualGridDataView_viKasirIgd(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryPostingManual_viKasirIgd.show();
    mWindowGridLookupPostingManual  = vWinFormEntryPostingManual_viKasirIgd; 
};

// End Function FormSetPostingManual_viKasirIgd # --------------

/**
*   Function : getFormItemEntryPostingManualGridDataView_viKasirIgd
*   
*   Sebuah fungsi untuk menampilkan isian form Posting Manual
*/
function getFormItemEntryPostingManualGridDataView_viKasirIgd(subtotal,NO_KUNJUNGAN)
{
    var lebar = 760;
    var pnlFormDataPostingManualWindowPopup_viKasirIgd = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                getItemPanelInputProdukPostingManualGridDataView_viKasirIgd(lebar),
                //-------------- ## -------------- 
                getItemGridTransaksiPostingManualGridDataView_viKasirIgd(lebar),
                //-------------- ## --------------
                {
                    xtype: 'compositefield',
                    fieldLabel: ' ',
                    anchor: '100%',
                    //labelSeparator: '',
                    width: 199,
                    style:{'margin-top':'7px'},
                    items: 
                    [
                        {
                            xtype:'button',
                            text:'Ok',
                            width:70,
                            //style:{'margin-left':'190px','margin-top':'7px'},
                            hideLabel:true,
                            id: 'btnOkGridTransaksiPostingManualGridDataView_viKasirIgd',
                            handler:function()
                            {
                            }   
                        },
                        //-------------- ## --------------
                        {
                            xtype:'button',
                            text:'Cancel',
                            width:70,
                            //style:{'margin-left':'190px','margin-top':'7px'},
                            hideLabel:true,
                            id: 'btnCancelGridTransaksiPostingManualGridDataView_viKasirIgd',
                            handler:function()
                            {
                            }   
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            id: 'txtTarifLamaGridTransaksiPostingManualGridDataView_viKasirIgd',
                            name: 'txtTarifLamaGridTransaksiPostingManualGridDataView_viKasirIgd',
                            style:{'text-align':'right','margin-left':'60px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            id: 'txtTarifBaruGridTransaksiPostingManualGridDataView_viKasirIgd',
                            name: 'txtTarifBaruGridTransaksiPostingManualGridDataView_viKasirIgd',
                            style:{'text-align':'right','margin-left':'56px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                    ]
                },  
            ],
            //-------------- #End items# --------------
        }
    )
    return pnlFormDataPostingManualWindowPopup_viKasirIgd;
}
// End Function getFormItemEntryPostingManualGridDataView_viKasirIgd # --------------

/**
*   Function : getItemPanelInputProdukPostingManualGridDataView_viKasirIgd
*   
*   Sebuah fungsi untuk menampilkan isian form Posting Manual
*/

function getItemPanelInputProdukPostingManualGridDataView_viKasirIgd(lebar) 
{
    var items =
    {
        title: 'Tarif Produk',
        layout: 'Form',
        anchor: '100%',
        width: lebar- 80,
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        items:
        [           
            {
                xtype: 'textfield',
                fieldLabel: 'Produk',
                id: 'txtProdukPostingManualGridDataView_viKasirIgd',
                name: 'txtProdukPostingManualGridDataView_viKasirIgd',
                emptyText: 'Produk',
                readOnly: true,
                flex: 1,
                width: 195,             
            },
            //-------------- ## --------------
        ]
    };
    return items;
}
// End Function getItemPanelInputProdukPostingManualGridDataView_viKasirIgd # --------------

/**
*   Function : getItemGridTransaksiPostingManualGridDataView_viKasirIgd
*   
*   Sebuah fungsi untuk menampilkan isian form Posting Manual
*/

function getItemGridTransaksiPostingManualGridDataView_viKasirIgd(lebar) 
{
    var items =
    {
        title: 'Perubahan Tarif Menjadi', 
        layout: 'form',
        anchor: '100%',     
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        width: lebar,
        height:245, 
        items:
        [
            {
                labelWidth:105,
                layout: 'form',
                labelAlign: 'Left',
                border: false,
                items:
                [
                    gridDataViewEditGridPostingManualDataView_viKasirIgd()
                ]   
            }
            //-------------- ## --------------
        ]
    };
    return items;
};
// End Function getItemGridTransaksiPostingManualGridDataView_viKasirIgd # --------------

/**
*   Function : gridDataViewEditGridPostingManualDataView_viKasirIgd
*   
*   Sebuah fungsi untuk menampilkan isian form Posting Manual
*/

function gridDataViewEditGridPostingManualDataView_viKasirIgd()
{
    
    var FieldGrdKasirGridDataView_viKasirIgd = 
    [
    ];
    
    dsDataGrdJabGridDataView_viKasirIgd= new WebApp.DataStoreDataStore
    ({
        fields: FieldGrdKasirGridDataView_viKasirIgd
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJabGridDataView_viKasirIgd,
        height: 200,
        selModel: new Ext.grid.RowSelectionModel
        (
            {
                singleSelect: true,
                listeners:
                {
                    rowselect: function(sm, row, rec)
                    {
                    }
                }
            }
        ),
        columns: 
        [
            {
                dataIndex: '',
                header: 'Komponen',
                sortable: true,
                width: 200
            },
            //-------------- ## --------------          
            {           
                dataIndex: '',
                header: 'Tarif Lama',
                sortable: true,
                width: 100
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Tarif Baru',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }           
            },
            //-------------- ## --------------
        ]
    }    
    return items;
}
// End Function gridDataViewEditGridPostingManualDataView_viKasirIgd # --------------

// ## END MENU PEMBAYARAN - POSTING MANUAL ## ------------------------------------------------------------------

// ## END MENU PEMBAYARAN ## ------------------------------------------------------------------

// --------------------------------------- # End Form # ---------------------------------------

// End Project Kasir IGD # --------------