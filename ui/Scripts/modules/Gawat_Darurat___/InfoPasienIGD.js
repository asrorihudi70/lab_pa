var now = new Date();
var anow=now.format('Y-m-d');
var InfoPasienIGD={};
var dsunit_viInfoPasienIGD;
InfoPasienIGD.form={};
InfoPasienIGD.func={};
InfoPasienIGD.vars={};
InfoPasienIGD.func.parent=InfoPasienIGD;
InfoPasienIGD.form.DataStore={};
InfoPasienIGD.form.Grid={};
InfoPasienIGD.form.Panel={};
InfoPasienIGD.form.TextField={};
InfoPasienIGD.form.ComboBox={};
InfoPasienIGD.form.DateField={};
InfoPasienIGD.form.Button={};
InfoPasienIGD.form.DataStore.InfoPasien=new WebApp.DataStore({fields:['TANGGAL','JAM','KD_PASIEN','NAMA_PASIEN','TGL_MASUK','NAMA_UNIT','DOKTER','ALAMAT']});

InfoPasienIGD.func.getId=function(){
	$this=this.parent;
	if($this.vars.genNum == undefined)$this.vars.genNum=0;
	$this.vars.genNum+=1;
	return 'nci-InfoPasien-rj-'+$this.vars.genNum;
};
function mComboUnit_viInfoPasienIGD()
{
	var Field = ['KD_UNIT','NAMA_UNIT'];

    dsunit_viInfoPasienIGD = new WebApp.DataStore({ fields: Field });
    dsunit_viInfoPasienIGD.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
				ort: 'kd_unit',
			    Sortdir: 'ASC',
			    target: 'ComboUnit',
                param: "" //+"~ )"
			}
		}
	);

    $this.form.ComboBox.txtSearch = new Ext.form.ComboBox
	(
		{
		    id: 'cboUNIT_viKasirIGDKasir',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel:  ' Poli ',
		    align: 'Right',
			width: 60,
		    anchor:'100%',
		    store: dsunit_viInfoPasienIGD,
		    valueField: 'KD_UNIT',
		    displayField: 'NAMA_UNIT',
			value:'All',
		    listeners:
			{

			    'select': function(a, b, c)
				{
						  // RefreshDataFilterKasirIGDKasir();

			        //selectStatusCMKasirIGDView = b.data.DEPT_ID;
					//
			    }

			}
		}
	);

    return $this.form.ComboBox.txtSearch;
};
InfoPasienIGD.func.getInit=function(mod_id){
	$this=this.parent;
	$this.func.getInfoPasien();
	setInterval(function(){
		$this.func.getInfoPasien();
	},15000);
	
	
	var grid= new Ext.Panel({
        id			: $this.func.getId(),
        bodyStyle	: 'margin: 5px 5px;padding: 5px 0 0 5px;',
        layout		: 'column',
        border		: true,
        items		:[
			{
			    layout		: 'form',
			    labelWidth	: 150,
			    border		: false,
			    items		: [
         		   
				   /* {
						xtype: 'textfield',
						fieldLabel: 'Tanggal ',
						name: 'tglInfoPasienIGD',
						id	: 'tglInfoPasienIGD',
						format: 'd/M/Y',
						value: now,
						anchor: '95%',
						listeners	: {
         				   keyup	: function(a, b,c){
         					   if(b.getKey()==13){
								   //console.log(typeof $this.form.DateField.caritanggal.getValue());
         						  $this.func.getInfoPasien();
         					   }
         				   }
         			   }
                   },  */
				   $this.form.DateField.caritanggal=new Ext.form.DateField({
         			   id			: 'tglInfoPasienIGD',
         			   fieldLabel	: 'Tanggal',
         			   width		: 150,
					   format		: 'd/M/Y',
					   value: anow,
         			   enableKeyEvents: true,
         			   listeners	: {
         				   keyup	: function(a, b,c){
         					   if(b.getKey()==13){
         						  $this.func.getInfoPasien();
         					   }
         				   }
         			   }
         		   }),
					/* $this.form.ComboBox.txtSearch=new Ext.form.ComboBox({
         			   id			: $this.func.getId(),
         			   fieldLabel	: 'Poli',
         			   width		: 150,
         			   enableKeyEvents: true,
					   store: dsunit_viInfoPasienIGD,
					   valueField: 'KD_UNIT',
					   displayField: 'NAMA_UNIT',
					   value:'All',
         			   listeners	: {
         				   keyup	: function(a, b,c){
         					   if(b.getKey()==13){
								   console.log(typeof $this.form.ComboBox.txtSearch.getValue());
         						  $this.func.getInfoPasien();
         					   }
         				   }
         			   }
         		   }) *///mComboUnit_viInfoPasienIGD(),				   
				   $this.form.Button.btnSearch= new Ext.Button({
					text	: 'Cari',
					id		: $this.func.getId(),
					tooltip	: nmLookup,
					iconCls	: 'find',
					handler	: function(){
						$this.func.getInfoPasien();
					}
				}),
			    ]
			},
			
			/* {
                layout		: 'form',
                border		: false,
                html		: ' &nbsp; *Table Auto Refresh.'
            } */
        ]
    });
	this.parent.form.Panel.InfoPasien= new Ext.Panel({
	    id			: mod_id,
	    closable	: true,
        layout		: 'form',
        title		: 'Info Pasien ',
        border		: false,
        shadhow		: true,
        iconCls		: 'Request',
        margins		: '0 5 5 0',
        autoScroll	: false,
	    items		:[
 		  	grid,
 		  	$this.func.getGridMain()
	    ]
	});
	return this.parent.form.Panel.InfoPasien;
};

InfoPasienIGD.func.getGridMain=function(){
	$this=this.parent;
    $this.form.Grid.InfoPasien	= new Ext.grid.EditorGridPanel({
        title		: 'Info Pasien',
		id			: $this.func.getId(),
		stripeRows	: true,
//		height		: 130,
        store		: $this.form.DataStore.InfoPasien,
        border		: false,
        frame		: false,
        anchor		: '100% 100%',
        autoScroll	: true,
        cm			: new Ext.grid.ColumnModel([
                  new Ext.grid.RowNumberer(),
            {
    			id			: $this.func.getId(),
            	header		: 'Tanggal',
                dataIndex	: 'TGL_MASUK',
                width		: 80,
    			menuDisabled: true,
                hidden		: false
            },/* {
    			id			: $this.func.getId(),
            	header		: 'Jam',
                dataIndex	: 'JAM',
                width		: 150,
    			menuDisabled: true,
                hidden		: false,
            }, */{
            	id			: $this.func.getId(),
            	header		: 'Kode Pasien',
                dataIndex	: 'KD_PASIEN',
                width		: 150,
    			menuDisabled: true,
                hidden		: false,
            },{
            	id			: $this.func.getId(),
            	header		: 'Nama Pasien',
                dataIndex	: 'NAMA_PASIEN',
                width		: 150,
    			menuDisabled: true,
                hidden		: false,
            },{
            	id			: $this.func.getId(),
            	header		: 'Unit',
                dataIndex	: 'NAMA_UNIT',
                width		: 150,
    			menuDisabled: true,
                hidden		: false,
            },{
            	id			: $this.func.getId(),
            	header		: 'Dokter',
                dataIndex	: 'DOKTER',
                width		: 150,
    			menuDisabled: true,
                hidden		: false,
            },{
            	id			: $this.func.getId(),
            	header		: 'Alamat',
                dataIndex	: 'ALAMAT',
                width		: 150,
    			menuDisabled: true,
                hidden		: false
            }
       ]),
        viewConfig	: {forceFit: true}
    });
    
    return $this.form.Grid.InfoPasien;
};

InfoPasienIGD.func.getInfoPasien=function(tglmasuk){
	$this=this.parent;
	var txtSearch='';
	var cariTanggal=anow;
	/* if($this.form.ComboBox.txtSearch != undefined){
		txtSearch=$this.form.ComboBox.txtSearch.getValue();
	} */
	if($this.form.DateField.caritanggal != undefined){
		cariTanggal=$this.form.DateField.caritanggal.getValue().format('Y-m-d');
		console.log(cariTanggal);
	}
	
	/* if (txtSearch === 'All' || txtSearch === '')
	{
		var criteria='tgl_masuk=~'+cariTanggal+'~ ';
	}
	else
	{ */
	var criteria='tgl_masuk=~'+cariTanggal+'~ and kd_unit=~3~';
	//}	
	//console.log(Ext.get('tglInfoPasienIGD'));
	//console.log(anow);
	InfoPasienIGD.form.DataStore.InfoPasien.load({ 
		params: { 
			Skip: 0, 
			Take: 0, 
            Sort: 'kd_penyakit',
			Sortdir: 'ASC', 
			target:'ViewGridInfoPasienIGD',
			param: criteria
		} 
	});
	//
};

CurrentPage.page = InfoPasienIGD.func.getInit(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
console.log(InfoPasienIGD);