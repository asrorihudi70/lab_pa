// Data Source ExtJS # --------------

/**
*	Nama File 		: TRKasirRwj.js
*	Menu 			: 
*	Model id 		: 
*	Keterangan 		: Kasir Rawat Jalan adalah proses untuk pembayaran pasien pada rawat jalan
*	Di buat tanggal : 13 Agustus 2014
*	Di EDIT tanggal : 14 Agustus 2014
*	Oleh 			: ADE. S
*	Editing			: SDY_RI
*/

// Deklarasi Variabel pada Kasir Rawat Jalan # --------------
var mRecordRequest = Ext.data.Record.create
(
    [
       {name: 'ASSET_MAINT', mapping:'ASSET_MAINT'},
       {name: 'ASSET_MAINT_ID', mapping:'ASSET_MAINT_ID'},
       {name: 'ASSET_MAINT_NAME', mapping:'ASSET_MAINT_NAME'},
       {name: 'LOCATION_ID', mapping:'LOCATION_ID'},
       {name: 'LOCATION', mapping:'LOCATION'},
       {name: 'PROBLEM', mapping:'PROBLEM'},
       {name: 'REQ_FINISH_DATE', mapping:'REQ_FINISH_DATE'},
       {name: 'DESC_REQ', mapping:'DESC_REQ'},
       {name: 'IMPACT', mapping:'IMPACT'},
       {name: 'ROW_REQ', mapping:'ROW_REQ'}
    ]
);
var tampungjumlah=0;
var AddNewSetjadwal;
var cellSelectedRequestDet;
var rowSelectedTreeSetEquipmentKlas_produk;
var selectCountCatEqp=50;
var selectCountKlas_produkEqp=50;
var IdKlas_produkegorySetEquipmentKlas_produkView='0';
var dataSource_viJadwalDokterRwj;
var selectCount_viJadwalDokterRwj=50;
var selectCount_viJadwalDokterRwj=50;
var NamaForm_viKasirRwj="Jadwal";
var mod_name_viKasirRwj="viKasirRwj";
var now_viKasirRwj= new Date();
var addNew_viKasirRwj;

var setLookUps_viKasirRwj;
var mNoKunjungan_viKasirRwj='';
var dsunit_viKasirRwj;
var StrTreeSetEquipment;
var dsDTLTRRequestList;
var kddokteredit;
var AddNewRequest;
var selectKlinikPoli;
var selectDokter;
var selectSetRujukanDari;

var selectDeptSetJadwalDokter;
var dsDeptSetupJadwalDokter;

var dsSetJadwalDokterList;
var AddNewSetJadwalDokter;
var selectCountSetJadwalDokter=5;
var rowSelectedSetJadwalDokter;
var SetJadwalDokterLookUps;

var valueUnit_viKasirRwj='';
var selectCountStatusByr_viKasirRwj;
var CurrentData_viKasirRwj =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = getPanelSetJadwalDokter(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

// End Deklarasi Variabel pada Kasir Rawat Jalan # --------------

// Start Project Kasir Rawat Jalan # --------------

// --------------------------------------- # Start Function # ---------------------------------------
// ## Silahkan tempatkan semua fungsi eksekusi disini

// --------------------------------------- # End Function # ---------------------------------------

// --------------------------------------- # Start Form # ---------------------------------------

/**
*	Function : dataGrid_viKasirRwj
*	
*	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
*/


/*function DataRefresh_viJadwalDokterRwj()
{	
	dataSource_viJadwalDokterRwj.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: selectCount_viJadwalDokterRwj, 
				//Sort: 'EMP_ID',
                Sort: 'NAMA_UNIT',
				Sortdir: 'ASC', 
				target:'ViewJadwalDokter',
				param: ''
			} 
		}
	);
	rowSelected_viJadwalDokterRwj = undefined;
	return dataSource_viJadwalDokterRwj;
};*/

function RefreshDataSetJadwalDokter()
{	
	dsSetJadwalDokterList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: selectCount_viJadwalDokterRwj, 
				//Sort: 'EMP_ID',
                Sort: 'kd_dokter',
				Sortdir: 'ASC', 
				target:'ViewJadwalDokterIGD',
				param: ''
			} 
		}
	);
	rowSelectedSetJadwalDokter = undefined;
	return dsSetJadwalDokterList;
};





function getPanelSetJadwalDokter(mod_id) 
{
	

    var Field = ['KD_DOKTER','KD_UNIT','NAMA_UNIT', 'NAMA', 'HARI'];
    dsSetJadwalDokterList = new WebApp.DataStore({ fields: Field });
	RefreshDataSetJadwalDokter();
	




    var grListSetJadwalDokter = new Ext.grid.EditorGridPanel
    (
		{
		    id: 'grListSetJadwalDokter',
		    stripeRows: true,
		    store: dsSetJadwalDokterList,
			autoScroll: true,
		    columnLines: true,
			border:false,
			plugins: [new Ext.ux.grid.FilterRow()],
		    anchor: '100% 100%',
		    sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedSetJadwalDokter=undefined;
							rowSelectedSetJadwalDokter = dsSetJadwalDokterList.getAt(row);
						}
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, ridx, cidx)
				{
					
					rowSelectedSetJadwalDokter = dsSetJadwalDokterList.getAt(ridx);
					if (rowSelectedSetJadwalDokter != undefined)
					{
						//setLookUp_viKasirRwj();
						setLookUp_viKasirRwj(rowSelectedSetJadwalDokter.data);
					}
					else
					{
						setLookUp_viKasirRwj();
					};
					
				}
			},
		    colModel: new Ext.grid.ColumnModel
            (
				[
					new Ext.grid.RowNumberer(),
                    {
                        id: 'colHari',
                        header: 'HARI',                      
                        dataIndex: 'HARI',
                        sortable: true,
                        width: 200,
						filter :{}
                    },
					{
					    id: 'colUnit',
					    header: 'NAMA_UNIT',					   
					    dataIndex: 'NAMA_UNIT',
					    width: 250,
					    sortable: true,
						filter :{}
					},
					{
					    id: 'colDokter',
					    header: 'DOKTER',					   
					    dataIndex: 'NAMA',
					    width: 250,
					    sortable: true,
						filter :{}
					},
					{
					    id: 'colKdDOKTER',
					    header: 'kd dokter',					   
					    dataIndex: 'KD_DOKTER',
					    width: 50,
						hidden:true,
					    sortable: true
					},
					{
					    id: 'colKdUnit',
					    header: 'kd unit',					   
					    dataIndex: 'KD_UNIT',
					    width: 50,
						hidden:true,
					    sortable: true
					},
									
                ]
			),
			bbar:new WebApp.PaggingBar({
            displayInfo: true,
            store: dsSetJadwalDokterList,
            pageSize: selectCountSetJadwalDokter,
            displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
            emptyMsg: "Tidak ada record&nbsp;&nbsp;"		
            }),
		    tbar:
			[
				{
				    id: 'btnEditSetJadwalDokter',
				    text: 'Buat Jadwal Baru',
					iconAlign:'left',
				    tooltip: nmEditData,
				    iconCls: 'Edit_Tr',
				    handler: function(sm, row, rec) 
					{
						if (rowSelectedSetJadwalDokter != undefined)
						{
						    setLookUp_viKasirRwj(rowSelectedSetJadwalDokter.data);
						}
						else
						{
						    setLookUp_viKasirRwj();
						}
				    }
				},' ','-'
			]
		    //,viewConfig: { forceFit: true }
		}
	);


    var FormSetJadwalDokter = new Ext.Panel
    (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: 'Jadwal',
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:0px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100%',
		    iconCls: 'SetupJadwalDokter',
		    items: [grListSetJadwalDokter],
		    tbar:
			[
				
			]
		}
	);
    //END var FormSetJadwalDokter--------------------------------------------------

	RefreshDataSetJadwalDokter();
    return FormSetJadwalDokter ;
};








// End Function dataGrid_viKasirRwj # --------------


//awal simpan detail


function getParamRequest() 
{
    var params =
	{
		Table:'ViewJadwalDokterIGD',
		Hari: Ext.get('cboRujukanDari').getValue(),
		Unit: Ext.get('cboPoliklinikRequestEntry').getValue(),
		Dokter: Ext.get('cboDokterRequestEntry').getValue(),
		Kd_Unit: selectKlinikPoli,
		Kd_Dokter: selectDokter
		//Hapus:1
	};
    return params
};

function getParamRequest2() 
{
    var params =
	{
		Table:'ViewJadwalDokterIGD',
		Hari: Ext.get('cboRujukanDari').getValue(),
		Kd_Dokter: Ext.get('txtKdDokter').getValue(),
		Kd_Unit: Ext.get('txtKdPoli').getValue(),
		
		//Hapus:1
	};
    return params
};




function datasave_viJadwal(mBol) 
{	
	if (ValidasiEntryCMRequest(nmHeaderSimpanData,false) == 1 )
	{
		if (AddNewRequest == true) 
		{
			Ext.Ajax.request
			(
				{
					//url: "./Datapool.mvc/CreateDataObj",
					url: baseURL + "index.php/main/CreateDataObj",					
					params: getParamRequest(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							RefreshDataSetJadwalDokter();
							ShowPesanInfoRequest(nmPesanSimpanSukses,nmHeaderSimpanData);
							
							/*RefreshDataRequest();
							if(mBol === false)
							{
								Ext.get('txtNoRequest').dom.value=cst.ReqId;
								RefreshDataRequestDetail(Ext.get('txtNoRequest').dom.value);
							};
							AddNewRequest = false;*/
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningRequest(nmPesanSimpanGagal,nmHeaderSimpanData);
						}
						else if (cst.success === false && cst.pesan===1)
						{
							ShowPesanWarningRequest(nmPesanSimpanGagal + ' , This request had been approved / rejected',nmHeaderSimpanData);
						}
						else 
						{
							ShowPesanErrorRequest(nmPesanSimpanError,nmHeaderSimpanData);
						};
						//DataRefresh_viJadwalDokterRwj();
					}
				}
			)
		}
		else 
		{
			Ext.Ajax.request
			 (
				{
					//url: "./Datapool.mvc/CreateDataObj",
					url: baseURL + "index.php/main/CreateDataObj",
					params: getParamRequest(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoRequest(nmPesanSimpanSukses,nmHeaderSimpanData);
							RefreshDataSetJadwalDokter();
							//setLookUps_viDaftar.close();
							//DataRefresh_viJadwalDokterRwj();
							/*RefreshDataRequest();
							if(mBol === false)
							{
								//RefreshDataRequestDetail(Ext.get('txtNoRequest').dom.value);
							};*/
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningRequest(nmPesanSimpanGagal,nmHeaderSimpanData);
						}
						else if (cst.success === false && cst.pesan===1)
						{
							ShowPesanWarningRequest(nmPesanSimpanGagal + ' , This request had been approved / rejected',nmHeaderSimpanData);
						}
						else 
						{
							ShowPesanErrorRequest(nmPesanSimpanError,nmHeaderSimpanData);
						};
						RefreshDataSetJadwalDokter();
					}
				}
			)
		};
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
};


function ValidasiEntryCMRequest(modul,mBolHapus)
{
	var x = 1;
	
	if((Ext.get('cboRujukanDari').getValue() == '') || (Ext.get('cboPoliklinikRequestEntry').getValue() == '') || (Ext.get('cboDokterRequestEntry').getValue() == '') )
	{
		if (Ext.get('cboRujukanDari').getValue() == '') 
		{
			x = 0;
		}
		else if (Ext.get('cboPoliklinikRequestEntry').getValue() == '') 
		{
			ShowPesanWarningRequest(nmGetValidasiKosong(nmRequestId), modul);
			x = 0;
		}
		else if (Ext.get('cboDokterRequestEntry').getValue() == '') 
		{
			ShowPesanWarningRequest(nmGetValidasiKosong(nmRequesterRequest), modul);
			x = 0;
		}
	};
	return x;
};


function ShowPesanWarningRequest(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorRequest(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoRequest(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};





/**
*	Function : setLookUp_viKasirRwj
*	
*	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
*/

//test buat popup jadwal baru
function setLookUp_viKasirRwj(rowdata)
{
    var lebar = 900;
    setLookUps_viDaftar = new Ext.Window
    ( 
    {
        id: 'SetLookUps_viDaftar',
        name: 'SetLookUps_viDaftar',
        title: 'Buat Jadwal Baru Dokter', 
        closeAction: 'destroy',        
        width: lebar,
        height:650,//575,
        resizable:false,
	autoScroll: false,

        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viJadwal(lebar,rowdata), //1
       /* listeners:
        {
            activate: function()
            {
                Ext.getCmp('cboSukuRequestEntry').hide() //suku di hide
                Ext.getCmp('txtJamKunjung').hide()
                Ext.getCmp('dptTanggal').hide()
                Ext.getCmp('txtNamaPeserta').hide()
                Ext.getCmp('txtNoAskes').hide()
                Ext.getCmp('txtNoSJP').hide()
                Ext.getCmp('cboPerseorangan').show()
                Ext.getCmp('cboAsuransi').hide()
                Ext.getCmp('cboPerusahaanRequestEntry').hide()
            },
            afterShow: function()
            {
                this.activate();
            },
			
            deactivate: function()
            {
                rowSelected_viDaftar=undefined;
                datarefresh_viDaftar();
		mNoKunjungan_viKasir = '';
                Ext.getCmp('txtNoMedrec').setValue('');
                Ext.getCmp('txtTglLahir').setValue('');
                Ext.getCmp('txtAlamatPasien').setValue('');
                Ext.getCmp('txtNamaPasien').setValue('');
            }
        }*/
    }
    );

    setLookUps_viDaftar.show();
    if (rowdata == undefined)
    {
        dataaddnew_viJadwal();
		// Ext.getCmp('btnSimpan_viDaftar').disable();
		// Ext.getCmp('btnSimpanExit_viDaftar').disable();	
		Ext.getCmp('btnDelete_viDaftar').disable();	
    }
    else
    {
        datainit_viJadwal(rowdata);
    }
}

function dataaddnew_viJadwal()
{
	Ext.get('cboRujukanDari').dom.value = '';
    Ext.get('cboPoliklinikRequestEntry').dom.value = '';
	Ext.get('cboDokterRequestEntry').dom.value = '';
	selectKlinikPoli = undefined;
	selectDokter = undefined;
	//rowSelectedSetJadwalDokter   = undefined;
	//Ext.get('txtKode_SetJadwalDokter').dom.readOnly=false;
}

function datainit_viJadwal(rowdata) 
{

    AddNewSetjadwal = false;
    Ext.get('cboRujukanDari').dom.value = rowdata.HARI;
    Ext.get('cboPoliklinikRequestEntry').dom.value = rowdata.NAMA_UNIT;
	Ext.get('cboDokterRequestEntry').dom.value = rowdata.NAMA;
	Ext.get('txtKdPoli').dom.value = rowdata.KD_UNIT;
	Ext.get('txtKdDokter').dom.value = rowdata.KD_DOKTER;
	kddokteredit = rowdata.KD_DOKTER;
	Kd_Unit: selectKlinikPoli;
	Kd_Dokter: selectDokter;
	if (rowdata.HARI === null)
	{
		Ext.get('cboRujukanDari').dom.value = '';
	}
	else
	{
		Ext.get('cboRujukanDari').dom.value = rowdata.HARI;
	};
	
	if (rowdata.KD_UNIT === null)
	{
			Ext.get('cboPoliklinikRequestEntry').dom.value = '';
	}
	else
	{
			Ext.get('cboPoliklinikRequestEntry').dom.value = rowdata.NAMA_UNIT;
	};
	
	if (rowdata.KD_DOKTER === null)
	{
		Ext.get('cboDokterRequestEntry').dom.value = '';
	}
	else
	{
		Ext.get('cboDokterRequestEntry').dom.value = rowdata.NAMA;
	};

};

function SetJadwalAddNew() 
{
    AddNewSetjadwal = true;   
	Ext.get('cboRujukanDari').dom.value = '';
    Ext.get('cboPoliklinikRequestEntry').dom.value = '';
	Ext.get('cboDokterRequestEntry').dom.value = '';
};

function getFormItemEntry_viJadwal(lebar,rowdata)
{
    var pnlFormDataBasic_viJadwal = new Ext.FormPanel
    (
		{
			title: '',
			region: 'north',//'center',
			layout: 'form',//'anchor',
			bodyStyle: 'padding:10px 10px 10px 10px',
			anchor: '100%',
                        autoScroll: true,
			width: lebar,//lebar-55,
                        height: 900,
			border: false,

                        items:[
                           getItemPanelInputJadwalBaru_viJadwal(),
                           getItemDataKunjungan_viBaru()
						   
                           
                                ],
			fileUpload: true,
			tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viDaftar',
						handler: function()
						{
							datasave_viJadwal(false);
							//setLookUps_viDaftar.close();
							//DataRefresh_viJadwalDokterRwj();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						id: 'btnSimpanExit_viDaftar',
						handler: function()
						{
							var x = datasave_viJadwal(true);
							
							if (x===undefined)
							{
								datasave_viJadwal(true);
								setLookUps_viDaftar.close();
								RefreshDataSetJadwalDokter();
							}
						}
					},
                                        {
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Hapus',
						iconCls: 'remove',
						id: 'btnDelete_viDaftar',
						handler: function()
						{
							SetJadwalDokterDelete();
							RefreshDataSetJadwalDokter();
							/*var x = datasave_viJadwal(addNew_viDaftar);
							datarefresh_viDaftar();
							if (x===undefined)
							{
								datasave_viJadwal(addNew_viDaftar);
								SetJadwalAddNew.close();
							}*/
						}
					},
					  {
						xtype: 'tbseparator'
					},
					
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						tooltip: 'refresh',
						id: 'btnEdit_viKasirRwj',
						handler: function(sm, row, rec)
						{
							RefreshDataSetJadwalDokter();
						}
					},
					
					
					
					
				]
			}//,items:
			
		}
    )

    return pnlFormDataBasic_viJadwal;
}

function getItemPanelInputJadwalBaru_viJadwal()
{
    var items =
	{
	    layout: 'column',
	    border: false,
        labelAlign: 'top',
	    items:
		[
		]
    };
    return items;
};

function getItemDataKunjungan_viBaru()
{
     var items =
	{
	    xtype:'fieldset',
            //checkboxToggle:true,
            id:'setunit',
            title: 'Buat Jadwal Baru Dokter',
            autoHeight:true,
            collapsible: true,
            //defaults: {width: 210},
            //defaultType: 'textfield',
            collapsed: false,
            items :
                [
                    getItemDataKunjungan_viBaru1(),
                ]
       }
        
    return items;
};

function getItemDataKunjungan_viBaru1()
{
   
        var items =
            {

                layout: 'column',
                border: false,
                labelAlign: 'top',
                items:
		[
			{
			    columnWidth: .30,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
                            anchor: '95%',
			    items:
				[
                                    mComboHari()
				]
			},
			{
			    columnWidth: .30,
			    layout: 'form',
			    border: false,
                            labelWidth:90,
                            anchor: '95%',
			    items:
				[
                                    mComboPoliklinik()
				]
			},
			{
			    columnWidth: .30,
			    layout: 'form',
			    border: false,
                labelWidth:10,
                anchor: '100%',
			    items:
				[
                                    mComboDokterRequestEntry()
				]
			},
			{
				 xtype: 'textfield',
                 fieldLabel: nmRequestId + ' ',
                 id: 'txtKdPoli',
				 hidden:true,
                 width: 100,
			},
			{
				 xtype: 'textfield',
                 fieldLabel: nmRequestId + ' ',
                 id: 'txtKdDokter',
				 hidden:true,
                 width: 100,
			},
			]

        };
    return items;
};

function mComboHari()
{
    var cboRujukanDari = new Ext.form.ComboBox
	(
		{
			id:'cboRujukanDari',
			typeAhead: true,
			name:'txthari',
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Pilih Hari...',
			fieldLabel: 'Hari ',
			width:110,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Senin'], [2, 'Selasa'],[3, 'Rabu'],[4, 'Kamis'],
                                       [5, 'Jumat'], [6, 'Sabtu'],[7, 'Minggu']
                                      ]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetRujukanDari,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetRujukanDari=b.data.displayText ;
				}
			}
		}
	);
	return cboRujukanDari;
};

function mComboPoliklinik()
{
    var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});

	ds_Poli_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ViewSetupUnit',
                param: "kd_bagian=3 and type_unit=false"
            }
        }
    )

    var cboPoliklinikRequestEntry = new Ext.form.ComboBox
    (
        {
            id: 'cboPoliklinikRequestEntry',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
			name:'txtpoliklinik',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Poliklinik...',
            fieldLabel: 'Poliklinik ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
            anchor: '95%',
            listeners:
                {
                    'select': function(a, b, c)
				{
                                   //alert(b.data.KD_PROPINSI)
								   selectKlinikPoli=b.data.KD_UNIT
                                   loaddatastoredokter(b.data.KD_UNIT)
								   Ext.get('txtKdPoli').dom.value = b.data.KD_UNIT
								   
								   //Ext.get('txtKdDokter').getvalue(b.data.KD_UNIT) 
								   //alert(b.data.KD_UNIT);
								   
								   
                                },
                    'render': function(c)
                                {
								  /* selectKlinikPoli=b.data.KD_UNIT
                                   loaddatastoredokter(b.data.KD_UNIT)
                                    alert(b.data.KD_UNIT);*/
                                }


		}
        }
    )

    return cboPoliklinikRequestEntry;
};


function mComboDokterRequestEntry()
{ 
	
	
    // Grid Kasir Rawat Jalan # --------------
 

	// Kriteria filter pada Grid # --------------
   
    //-------------- # End form filter # --------------

 var Field = ['KD_DOKTER','NAMA'];

    dsDokterRequestEntry = new WebApp.DataStore({fields: Field});


    var cboDokterRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboDokterRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
			name:'txtdokter',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
                    forceSelection: true,
		    emptyText:'Pilih Dokter...',
		    fieldLabel: 'Dokter ',
		    align: 'Right',
                     
//		    anchor:'60%',
		    store: dsDokterRequestEntry,
		    valueField: 'KD_DOKTER',
		    displayField: 'NAMA',
                    anchor: '95%',
		    listeners:
			{
			    'select': function(a,b,c)
				{
//                                  var selectDokterRequestEntry = b.data.KD_PROPINSI;
                                    //alert("is");
									selectDokter = b.data.KD_DOKTER;
									 Ext.get('txtKdDokter').dom.value = b.data.KD_DOKTER
                                },
                            'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('kelPasien').focus();
                                    }, c);
                                }
			}
                }
	);
	
	/*var cboDokterRequestEntry = new Ext.form.CheckboxGroup({
    id:'myGroup',
    xtype: 'checkboxgroup',
    fieldLabel: 'Pilih Dokter',
    itemCls: 'x-check-group-alt',
	store: dsDokterRequestEntry,
    // Put all controls in a single column with width 100%
    columns: 1,
    items: [
        {
                boxLabel: 'test',
                name: 'fav-test1',
                inputValue: '1'
            }, {
                boxLabel: 'test2',
                name: 'fav-test2',
                inputValue: '2'
            }, {
                checked: true,
                boxLabel: 'test',
                name: 'fav-test3',
                inputValue: '3'
            }
    ]
});*/

    return cboDokterRequestEntry;

};




function SetJadwalDokterDelete() 
{
	//if (ValidasiEntrySetJadwalDokter(nmHeaderHapusData,true) == 1 )
	//{
		Ext.Msg.show
		(
			{
			   title:nmHeaderHapusData,
			   msg: nmGetValidasiHapus(nmEmp2) ,
			   buttons: Ext.MessageBox.YESNO,
			   width:250,
			   fn: function (btn) 
			   {			
					if (btn === 'yes') 
					{
						Ext.Ajax.request
						(
							{
								url: WebAppUrl.UrlDeleteData,
								params: getParamRequest2(),
								success: function(o) 
								{
									var cst = Ext.decode(o.responseText);
									if (cst.success === true) 
									{
										RefreshDataSetJadwalDokter();
										ShowPesanWarningRequest(nmPesanHapusSukses,nmHeaderHapusData);
										setLookUps_viDaftar.close();
										dataaddnew_viJadwal();
										
									}
									else if (cst.success === false && cst.pesan===0)
									{
										ShowPesanWarningRequest(nmPesanHapusGagal,nmHeaderHapusData);
									}
									else 
									{
										ShowPesanWarningRequest(nmPesanHapusError,nmHeaderHapusData);
									};
								}
							}
						)
					};
				}
			}
		)
	};
//};

function loaddatastoredokter(kd_unit)
{
          dsDokterRequestEntry.load
                (
                    {
                     params:
			{
                            Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
                            Sort: 'nama',
			    Sortdir: 'ASC',
			    target: 'ViewComboDokter',
			    param: 'where dk.kd_unit=~'+ kd_unit+ '~'
			}
                    }
                )
};
