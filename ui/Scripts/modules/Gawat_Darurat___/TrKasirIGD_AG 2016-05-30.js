var dataTrDokter = [];
var CurrentCellTrDokter='';
var dsTrDokter	= new WebApp.DataStore({ fields: ['KD_DOKTER','NAMA','KD_JOB','KD_PRODUK'] });

var CurrentKasirIGD =
{
    data: Object,
    details: Array,
    row: 0
};
var dsLookProdukList_rad_IGD;
var FormLookUpsdetailTRKelompokPasien_igd;
var dsDokterGanti_igd;
var syssetting='IGD_default_klas_produk';
var strb;
var nilai_kd_tarif;
var dsLook_dokter_rad_igd;
var tanggaltransaksitampung_IGD;
var dsTRDetailDiagnosaList_IGD
var nowTglTransaksi_IGDGrid_poli_IGD = new Date();
var tglGridBawah_poli_IGD = nowTglTransaksi_IGDGrid_poli_IGD.format("d/M/Y");
var ds_customer_viPJ_IGD;
var mRecordIGD = Ext.data.Record.create
(
    [
       {name: 'DESKRIPSI2', mapping:'DESKRIPSI2'},
       {name: 'KD_PRODUK', mapping:'KD_PRODUK'},
       {name: 'DESKRIPSI', mapping:'DESKRIPSI'},
       {name: 'KD_TARIF', mapping:'KD_TARIF'},
       {name: 'HARGA', mapping:'HARGA'},
       {name: 'QTY', mapping:'QTY'},
       {name: 'TGL_TRANSAKSI', mapping:'TGL_TRANSAKSI'},
       {name: 'DESC_REQ', mapping:'DESC_REQ'},
      // {name: 'KD_TARIF', mapping:'KD_TARIF'},
       {name: 'URUT', mapping:'URUT'}
    ]
);
var CurrentDiagnosa_IGD =
{
    data: Object,
    details: Array,
    row: 0
};
var dsLookProdukList_igd_dokter_leb;
var FormLookUpGantidokter_IGD;
var mRecordDiagnosa_IGD = Ext.data.Record.create
(
    [
       {name: 'KASUS', mapping:'KASUS'},
       {name: 'KD_PENYAKIT', mapping:'KD_PENYAKIT'},
       {name: 'PENYAKIT', mapping:'PENYAKIT'},
       //{name: 'KD_TARIF', mapping:'KD_TARIF'},
      // {name: 'HARGA', mapping:'HARGA'},
       {name: 'STAT_DIAG', mapping:'STAT_DIAG'},
       {name: 'TGL_TRANSAKSI', mapping:'TGL_TRANSAKSI'},
      // {name: 'DESC_REQ', mapping:'DESC_REQ'},
      // {name: 'KD_TARIF', mapping:'KD_TARIF'},
       {name: 'URUT_MASUK', mapping:'URUT_MASUK'}
    ]
);

var dsrad;
var cellCurrentTindakan_IGD;
var cellselectedrad_IGD;
var CurrentRad_igd ={
    data: Object,
    details: Array,
    row: 0
};
var PenataJasaIGD={};
var CurrentKasirIGD ={
    data: Object,
    details: Array,
    row: 0
};

var mRecordIGD = Ext.data.Record.create([
       {name: 'DESKRIPSI2', mapping:'DESKRIPSI2'},
       {name: 'KD_PRODUK', mapping:'KD_PRODUK'},
       {name: 'DESKRIPSI', mapping:'DESKRIPSI'},
       {name: 'KD_TARIF', mapping:'KD_TARIF'},
       {name: 'HARGA', mapping:'HARGA'},
       {name: 'QTY', mapping:'QTY'},
       {name: 'TGL_TRANSAKSI', mapping:'TGL_TRANSAKSI'},
       {name: 'DESC_REQ', mapping:'DESC_REQ'},
       {name: 'URUT', mapping:'URUT'}
]);



var combo;
var FormLookUpGantidokter_IGD;
var mRecordDiagnosa_IGD = Ext.data.Record.create([
   {name: 'KASUS', mapping:'KASUS'},
   {name: 'KD_PENYAKIT', mapping:'KD_PENYAKIT'},
   {name: 'PENYAKIT', mapping:'PENYAKIT'},
   {name: 'STAT_DIAG', mapping:'STAT_DIAG'},
   {name: 'TGL_TRANSAKSI', mapping:'TGL_TRANSAKSI'},
   {name: 'URUT_MASUK', mapping:'URUT_MASUK'}
]);


var AddNewDiagnosa_IGD = true;
var selectCountDiagnosa_IGD = 50;
var now = new Date();
var rowSelectedDiagnosa_IGD;
var cellSelecteddeskripsi_IGD;
var FormLookUpsdetailTRDiagnosa_IGD;

var nowTglTransaksi_IGD = new Date();

var labelisi_IGD;
var jeniscus_IGD;
var variablehistori_IGD;
var selectCountStatusByr_viKasirIGD='Belum Posting';
var dsTRKasirIGDList;
var dsTRDetailKasirIGDList;
var AddNewKasirIGD = true;
var selectCountKasirIGD = 50;




var rowSelectedKasirIGD;
var FormLookUpsdetailTRIGD;
var nowTglTransaksi_IGD = new Date();
var KelompokPasienAddNew_IGD=true;
//var FocusCtrlCMIGD;





var dsPjTrans2_IGD;
var dsIGDPJLab_IGD;
var dsCmbIGDPJDiag_IGD;


var anamnese;
var vkode_customer_IGD;

var trcellCurrentTindakan_IGD;

// Asep
PenataJasaIGD.dsComboObat;
PenataJasaIGD.iComboObat;
PenataJasaIGD.iComboVerifiedObat;
PenataJasaIGD.gridObat;
PenataJasaIGD.grid1;
PenataJasaIGD.grid2;
PenataJasaIGD.grid3;//untuk data grid di tab labolatorium
PenataJasaIGD.ds1;
PenataJasaIGD.ds2;
PenataJasaIGD.ds3;//untuk data store grid di tab labolatorium
PenataJasaIGD.ds4=new WebApp.DataStore({ fields: ['kd_produk','kd_klas','deskripsi','username','kd_lab']});
PenataJasaIGD.ds5 = new WebApp.DataStore({fields: ['kd_jenis_pelayanan_igd', 'jenis_pelayanan_igd']});
PenataJasaIGD.dssebabkematian = new WebApp.DataStore({fields: ['kd_sebab_mati', 'sebab_mati']});
PenataJasaIGD.dsstatupulang = new WebApp.DataStore({fields: ['kd_status_pulang', 'status_pulang']});
PenataJasaIGD.dsGridObat;
PenataJasaIGD.dsGridTindakan;
PenataJasaIGD.s1;
PenataJasaIGD.btn1;
PenataJasaIGD.pj_req_rad;
PenataJasaIGD.form={};
PenataJasaIGD.func={};
PenataJasaIGD.gridrad;
PenataJasaIGD.dshasilLabIGD;
PenataJasaIGD.gridhasil_lab_PJIGD;
PenataJasaIGD.form.Checkbox={};
PenataJasaIGD.form.Class={};
PenataJasaIGD.form.ComboBox={};
PenataJasaIGD.form.DataStore={};
PenataJasaIGD.form.Grid={};
PenataJasaIGD.form.Group={};
PenataJasaIGD.form.Group.print={};
PenataJasaIGD.form.Window={};
PenataJasaIGD.var_kd_dokter_leb;
PenataJasaIGD.varkd_tarif;
PenataJasaIGD.var_kd_dokter_rad;
CurrentPage.page = getPanelIGD(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

//------------------------------------------- BARU --------------------------------------//

PenataJasaIGD.form.DataStore.penyakit=new Ext.data.ArrayStore({id: 0,fields: ['text','kd_penyakit','penyakit'],data: []});
PenataJasaIGD.form.DataStore.produk=new Ext.data.ArrayStore({id: 0,fields:['kd_produk','deskripsi','harga','tglberlaku'],data: []});

PenataJasaIGD.ComboVerifiedObat=function(){
	PenataJasaIGD.iComboVerifiedObat	= new Ext.form.ComboBox({
		id				: Nci.getId(),
		typeAhead		: true,
		triggerAction	: 'all',
		lazyRender		: true,
		mode			: 'local',
		anchor 			: '96.8%',
		emptyText		: '',
		store			: new Ext.data.ArrayStore({
			id		: 0,
			fields	: ['Id','displayText'],
			data	: [[0, 'Disetujui'],[1, 'Tdk Disetujui']]
		}),
		valueField		: 'displayText',
		displayField	: 'displayText'
	});
	return PenataJasaIGD.iComboVerifiedObat;
};

PenataJasaIGD.classGridObat	= Ext.data.Record.create([
   {name: 'kd_prd', 	mapping: 'kd_prd'},
   {name: 'nama_obat', 	mapping: 'nama_obat'},
   {name: 'jumlah', 	mapping: 'jumlah'},
   {name: 'satuan', 	mapping: 'satuan'},
   {name: 'cara_pakai', mapping: 'cara_pakai'},
   {name: 'kd_dokter', 	mapping: 'kd_dokter'},
   {name: 'verified', 	mapping: 'verified'},
   {name: 'racikan', 	mapping: 'racikan'},
   {name: 'order_mng', 	mapping: 'order_mng'},
   {name: 'jml_stok_apt', 	mapping: 'jml_stok_apt'},
   
]);

PenataJasaIGD.classGrid3	= Ext.data.Record.create([
    {name: 'kd_produk', 	mapping: 'kd_produk'},
    {name: 'kd_klas', 	mapping: 'kd_klas'},
    {name: 'deskripsi', 	mapping: 'deskripsi'},
    {name: 'username', 	mapping: 'username'},
    {name: 'kd_lab', 	mapping: 'kd_lab'}
 ]);

PenataJasaIGD.form.Class.diagnosa= Ext.data.Record.create([
  {name: 'KD_PENYAKIT', 	mapping: 'KD_PENYAKIT'},
  {name: 'PENYAKIT', 	mapping: 'PENYAKIT'},
  {name: 'KD_PASIEN', 	mapping: 'KD_PASIEN'},
  {name: 'URUT', 	mapping: 'UURUTRUT'},
  {name: 'URUT_MASUK', 	mapping: 'URUT_MASUK'},
  {name: 'TGL_MASUK', 	mapping: 'TGL_MASUK'},
  {name: 'KASUS', 	mapping: 'KASUS'},
  {name: 'STAT_DIAG', 	mapping: 'STAT_DIAG'},
  {name: 'NOTE', 	mapping: 'NOTE'},
  {name: 'DETAIL', 	mapping: 'DETAIL'}
]);
PenataJasaIGD.form.Class.produk= Ext.data.Record.create([
  {name: 'KD_PRODUK', 	mapping: 'KD_PRODUK'},
  {name: 'DESKRIPSI', 	mapping: 'PDESKRIPSIENYAKIT'},
  {name: 'QTY', 	mapping: 'QTY'},
  {name: 'DOKTER', 	mapping: 'DOKTER'},
  {name: 'TGL_TINDAKAN', 	mapping: 'TGL_TINDAKAN'},
  {name: 'QTY', 	mapping: 'QTY'},
  {name: 'DESC_REQ', 	mapping: 'DESC_REQ'},
  {name: 'TGL_BERLAKU', 	mapping: 'TGL_BERLAKU'},
  {name: 'NO_TRANSAKSI', 	mapping: 'NO_TRANSAKSI'},
  {name: 'URUT', 	mapping: 'URUT'},
  {name: 'DESC_STATUS', 	mapping: 'DESC_STATUS'},
  {name: 'TGL_TRANSAKSI', 	mapping: 'TGL_TRANSAKSI'},
  {name: 'KD_TARIF', 	mapping: 'KD_TARIF'},
  {name: 'HARGA', 	mapping: 'HARGA'}
]);

PenataJasaIGD.func.getNullProduk= function(){
	var o=PenataJasaIGD.grid1.getSelectionModel().getSelections()[0].data;
	return new PenataJasaIGD.form.Class.diagnosa({
		KD_PRODUK	: '',
		DESKRIPSI	: '',
		QTY			: 1,
		DOKTER		: Ext.getCmp('txtNamaDokter_igd').getValue(),
		TGL_TINDAKAN: '',
		QTY			: 1,
		DESC_REQ	: '',
		TGL_BERLAKU	: '',
		NO_TRANSAKSI: '',
		URUT		: 0,
		DESC_STATUS	: '',
		TGL_TRANSAKSI: o.TANGGAL_TRANSAKSI,
		KD_TARIF	: '',
		HARGA		: '',
		JUMLAH		: '',
	});
};

PenataJasaIGD.func.getNullDiagnosa= function(){
	return new PenataJasaIGD.form.Class.diagnosa({
		KD_PENYAKIT	: '',
		PENYAKIT	: '',
		KD_PASIEN	: '',
		URUT		: 0,
		URUT_MASUK	: '',
		TGL_MASUK	: '',
		KASUS		: '',
		STAT_DIAG	: '',
		NOTE		: '',
		DETAIL		: 0
	});
};

PenataJasaIGD.nullGridObat	= function(){
	return new PenataJasaIGD.classGridObat({
		kd_produk	: '',
		nama_obat	: '',
		jumlah		: 0,
		satuan		: '',
		cara_pakai	: '',
		kd_dokter	: '',
		verified	: 'Tidak Disetujui',
		racikan		: 0,
		order_mng	: 'Belum Dilayani',
		jml_stok_apt :0
	});
};

PenataJasaIGD.nullGrid3	= function(){
	var tgltranstoday =new Date();
	return new PenataJasaIGD.classGrid3({
		kd_produk	 	: '',
		deskripsi		: '',
		kd_tarif		: '',
		harga			: '',
		qty				: '',
		tgl_berlaku		: '',
		urut			: '',
		tgl_transaksi	: tgltranstoday.format('Y/m/d'),
		no_transaksi	: '',
	});
};
PenataJasaIGD.comboObat	= function(){
	var $this	= this;
 	$this.dsComboObat	= new WebApp.DataStore({ fields: ['kd_prd','nama_obat','jml_stok_apt'] });
/*	$this.dsComboObat.load({ 
		params	: { 
			Skip	: 0, 
			Take	: 50, 
			target	:'ViewComboObatRJPJ'
		} 
	}); */
	$this.iComboObat= new Ext.form.ComboBox({
		id				: Nci.getId(),
		typeAhead		: true,
	    triggerAction	: 'all',
	    lazyRender		: true,
	    mode			: 'local',
	    emptyText		: '',
		store			: $this.dsComboObat,
		valueField		: 'nama_obat',
		hideTrigger		: true,
		displayField	: 'nama_obat',
		value			: '',
		listeners		: {
			select	: function(a, b, c){	
				var line	= $this.gridObat.getSelectionModel().selection.cell[0];
				$this.dsGridObat.getRange()[line].data.kd_prd=b.json.kd_prd;
				$this.dsGridObat.getRange()[line].data.satuan=b.json.satuan;
				$this.dsGridObat.getRange()[line].data.kd_dokter=b.json.nama;
				$this.dsGridObat.getRange()[line].data.nama_obat=b.json.nama_obat;
				$this.dsGridObat.getRange()[line].data.jml_stok_apt=b.json.jml_stok_apt;
				$this.gridObat.getView().refresh();
		    }
		}
	});
	return $this.iComboObat;
};

function getPanelIGD(mod_id) 
{
	 var Field = ['KD_DOKTER','NO_TRANSAKSI','KD_UNIT','KD_PASIEN','NAMA','NAMA_UNIT','ALAMAT','TANGGAL_TRANSAKSI','NAMA_DOKTER','KD_CUSTOMER','CUSTOMER','URUT_MASUK','POSTING_TRANSAKSI'];
    dsTRKasirIGDList = new WebApp.DataStore({ fields: Field });
	 PenataJasaIGD.ds1=dsTRKasirIGDList;
    refeshkasirIGD();
    var grListTRIGD = new Ext.grid.EditorGridPanel
    (
        {
            stripeRows: true,
            store: dsTRKasirIGDList,
            anchor: '100% 65%',
            columnLines: false,
            autoScroll:true,
            border: true,
			sort :false,
            sm: new Ext.grid.RowSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        rowselect: function(sm, row, rec)
                        {
                            rowSelectedKasirIGD = dsTRKasirIGDList.getAt(row);
                        }
                    }
                }
            ),
            listeners:
            {
                rowdblclick: function (sm, ridx, cidx)
                {
                	PenataJasaIGD.s1=PenataJasaIGD.ds1.getAt(ridx);
                    rowSelectedKasirIGD = dsTRKasirIGDList.getAt(ridx);
                    if (rowSelectedKasirIGD != undefined)
                    {
                        IGDLookUp(rowSelectedKasirIGD.data);
                    }
                    else
                    {
                        IGDLookUp();
                    }
                }
            },
        cm: new Ext.grid.ColumnModel
            (
                [
                   {
                        header: 'Status Posting',
                        width: 150,
                        sortable: false,
						hideable:true,
						hidden:false,
						menuDisabled:true,
                        dataIndex: 'POSTING_TRANSAKSI',
                        id: 'txtposting',
						renderer: function(value, metaData, record, rowIndex, colIndex, store)
                        {
                             switch (value)
                             {
                                 case 't':
                                         metaData.css = 'StatusHijau'; // 
                                         break;
                                 case 'f':
                                        metaData.css = 'StatusMerah'; // rejected

                                         break;
                             }
                             return '';
                        }
						
                    },
                    {
                        id: 'colReqIdViewIGD',
                        header: 'No. Transaksi',
                        dataIndex: 'NO_TRANSAKSI',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 80
                    },
                    {
                        id: 'colTglIGDViewIGD',
                        header: 'Tgl Transaksi',
                        dataIndex: 'TANGGAL_TRANSAKSI',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 75,
                            renderer: function(v, params, record)
                            {
                                    return ShowDate(record.data.TANGGAL_TRANSAKSI);

                        }
                    },
					{
                        header: 'No. Medrec',
                        width: 65,
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        dataIndex: 'KD_PASIEN',
                        id: 'colIGDerViewIGD'
                    },
					{
                        header: 'Pasien',
                        width: 190,
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        dataIndex: 'NAMA',
                        id: 'colIGDerViewIGD'
                    },
                    {
                        id: 'colLocationViewIGD',
                        header: 'Alamat',
                        dataIndex: 'ALAMAT',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 170
                    },
                    
                    {
                        id: 'colDeptViewIGD',
                        header: 'Dokter',
                        dataIndex: 'NAMA_DOKTER',
                        sortable: false,
							hideable:false,
							menuDisabled:true,
                        width: 150
                    },
                    {
                        id: 'colImpactViewIGD',
                        header: 'Unit',
                        dataIndex: 'NAMA_UNIT',
						sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 90
                    },
					
                   
                ]
            ),viewConfig: {forceFit: true},
            tbar:
                [/*
                    {
                        id: 'btnEditIGD',
                        text: nmEditData,
                        tooltip: nmEditData,
                        iconCls: 'Edit_Tr',
                        handler: function(sm, row, rec)
                        {
                            if (rowSelectedKasirIGD != undefined)
                            {
                                    IGDLookUp(rowSelectedKasirIGD.data);
                            }
                            else
                            {
							ShowPesanWarningIGD('Pilih data data tabel  ','Edit data');
                                    //alert('');
                            }
                        }
                    },'' ,' ' , '' , '' ,'' ,' ' , '' , '' , ' ', '' , '' , ' ','', '' ,' ', ' ','' , ' ','' , ' ','' , ' ','' ,
					'', '', '' + ' ', ' ','' + ' ','' + ' ','' + ' ','' + ' ', ' ','' + ''
					,'' ,' ' , '' , '' ,'' ,' ' , '' , '' , ' ', '' , '' , ' ','', '' ,' ', ' ','' , ' ','' , ' ','' , ' '
					,' ', '-', 'Status Posting' + ' : ', ' ',
						mComboStatusBayar_viKasirIGD()
                    ,' ','' + ' ','' + ' ', ' ','' + '',
					' ','' + ' ','' + ' ', ' ','' + '', ' ', '-', 'Tanggal Kunjungan' + ' : ', ' ',
                    {
                        xtype: 'datefield',
                        fieldLabel: 'Tanggal Kunjungan' + ' ',
                        id: 'dtpTglAwalFilterIGD',
                        format: 'd/M/Y',
                        value: now,
                        width: 100,
                        onInit: function() { }
                    }, ' ', '  ' + 's/d' + ' ', ' ', {
                        xtype: 'datefield',
                        fieldLabel: nmSd + ' ',
                        id: 'dtpTglAkhirFilterIGD',
                        format: 'd/M/Y',
                        value: now,
                        width: 100
                    }*/
                ]
            }
	);
	
	 PenataJasaIGD.grid1=grListTRIGD;
var LegendpenatajasaIGD = new Ext.Panel
	(
            {
            id: 'LegendpenatajasaIGD',
            region: 'center',
            border:false,
            bodyStyle: 'padding:0px 7px 0px 7px',
            layout: 'column',
            frame:true,
            //height:32,
            anchor: '100% 8.0001%',
            autoScroll:false,
            items:
            [
                {
                    columnWidth: .033,
                    layout: 'form',
                    style:{'margin-top':'-1px'},
                    //height:32,
                    anchor: '100% 8.0001%',
                    border: false,
                    html: '<img src="'+baseURL+'ui/images/icons/16x16/hijau.png" class="text-desc-legend"/>'
                },
                {
                    columnWidth: .08,
                    layout: 'form',
                    //height:32,
                    anchor: '100% 8.0001%',
                    style:{'margin-top':'1px'},
                    border: false,
                    html: " Posting"
                },
                {
                    columnWidth: .033,
                    layout: 'form',
                    style:{'margin-top':'-1px'},
                    border: false,
                    //height:35,
                    anchor: '100% 8.0001%',
                    //html: '<img src="./images/icons/16x16/merah.png" class="text-desc-legend"/>'
                    html: '<img src="'+baseURL+'ui/images/icons/16x16/merah.png" class="text-desc-legend"/>'
                },
                {
                    columnWidth: .1,
                    layout: 'form',
                    //height:32,
                    anchor: '100% 8.0001%',
                    style:{'margin-top':'1px'},
                    border: false,
                    html: " Belum Posting"
                }
            ]

        }
    )
    var FormDepanIGD = new Ext.Panel
    (
        {
            id: mod_id,
            closable: true,
            region: 'center',
            layout: 'form',
            title: 'Penata Jasa ',
            border: false,
            shadhow: true,
            autoScroll:false,
            iconCls: 'Request',
            margins: '0 5 5 0',
            items: [{
						   xtype:'panel',
							   plain:true,
							   activeTab: 0,
								height:180,
							   //deferredRender: false,
							   defaults:
							   {
								bodyStyle:'padding:10px',
								autoScroll: true
							   },
							   items:[
								{
									layout: 'form',
									margins: '0 5 5 0',
									border: true ,
									items:
									[
									 {
										xtype: 'textfield',
										fieldLabel: ' No. Medrec' + ' ',
										id: 'txtFilterIGDNomedrec',
										anchor : '70%',
										onInit: function() { },
										listeners:
										{
											'specialkey' : function()
											{
												var tmpNoMedrec = Ext.get('txtFilterIGDNomedrec').getValue()
												if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10 )
												{
													if(tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10 )
														{
														   
															 var tmpgetNoMedrec = formatnomedrec(Ext.get('txtFilterIGDNomedrec').getValue())
															 Ext.getCmp('txtFilterIGDNomedrec').setValue(tmpgetNoMedrec);
															var tmpkriteria = getCriteriaFilter_viDaftar();
															 RefreshDataFilterKasirIGD();
													  
														}
														else
															{
																if (tmpNoMedrec.length === 10)
																	{
																	   // tmpkriteria = getCriteriaFilter_viDaftar();
																	 RefreshDataFilterKasirIGD();
																	}
																	else
																	Ext.getCmp('txtFilterIGDNomedrec').setValue('')
															}
												}
											}

										}
								},
								
								{	 
								xtype: 'tbspacer',
								height: 3
								},	
									{
											xtype: 'textfield',
											fieldLabel: ' Pasien' + ' ',
											id: 'TxtIGDFilternama',
											anchor :'70%',
											 enableKeyEvents: true,
											listeners:
											{ 
										
												'specialkey' : function()
													{
														if (Ext.EventObject.getKey() === 13)
														{
																  RefreshDataFilterKasirIGD();
														}
													}
											}
										},
										{	 
										xtype: 'tbspacer',
										height: 3
										},	
										
										mComboUnit_viKasirIGD(),
										
											{
											xtype: 'textfield',
											fieldLabel: ' Dokter' + ' ',
											id: 'TxtIGDFilterDokter',
											anchor:'70%',
											enableKeyEvents: true,
											listeners:
											{ 
													 
												'specialkey' : function()
												{
													if (Ext.EventObject.getKey() === 13)
													{
															  RefreshDataFilterKasirIGD();
													}
												}
											}
										},mComboStatusBayar_viKasirIGD()
										,getItemPaneltgl_filter_IGD()
										]
								}
						]
						},
						grListTRIGD,LegendpenatajasaIGD],
         
            listeners:
            {
                'afterrender': function()
                {}
            }
        }
    );
	
   return FormDepanIGD


};




function getItemPaneltgl_filter_IGD() 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .35,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'datefield',
					    fieldLabel: 'Tanggal ',
					    id: 'dtpTglAwalFilterIGD',
					    name: 'dtpTglAwalFilterIGD',
					    format: 'd/M/Y',
						//readOnly : true,
					    value: now,
					    anchor: '99%',
					    listeners:
						{
						
						 'specialkey' : function()
                            {
                                var tmpNoMedrec = Ext.get('txtFilterIGDNomedrec').getValue()
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10 )
                                {
								RefreshDataFilterKasirIGD();
								}
						}
						}
					}
				]
			},
			 {xtype: 'tbtext', text: ' s/d', cls: 'left-label', width: 30},
			{
			    columnWidth: .35,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[ 
		
					{
					    xtype: 'datefield',
					   // fieldLabel: 'Tanggal ',
					    id: 'dtpTglAkhirFilterIGD',
					    name: 'dtpTglAkhirFilterIGD',
					    format: 'd/M/Y',
						//readOnly : true,
					    value: now,
					    anchor: '97.8%',
						   listeners:
						{
						
						 'specialkey' : function()
                            {
                                var tmpNoMedrec = Ext.get('txtFilterIGDNomedrec').getValue()
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10 )
                                {
								RefreshDataFilterKasirIGD();
								}
						}
						}
					}
				]
			}
		]
	}
    return items;
};



function mComboUnit_viKasirIGD(){
	var Field = ['KD_UNIT','NAMA_UNIT'];
    dsunit_viKasirIGD = new WebApp.DataStore({ fields: Field });
    dsunit_viKasirIGD.load({
	    params:{
		    Skip	: 0,
		    Take	: 1000,
            Sort	: 'kd_unit',
		    Sortdir	: 'ASC',
		    target	: 'ComboUnit',
            param	: ""
		}
	});
    var cboUNIT_viKasirIGD = new Ext.form.ComboBox({
	    id				: 'cboUNIT_viKasirIGD',
	    typeAhead		: true,
	    triggerAction	: 'all',
	    lazyRender		: true,
	    mode			: 'local',
	    emptyText		: '',
	    fieldLabel		: ' Poli Tujuan',
	    align			: 'Right',
	    width			: 100,
	    anchor			: '70%',
	    store			: dsunit_viKasirIGD,
	    valueField		: 'NAMA_UNIT',
	    displayField	: 'NAMA_UNIT',
		value			:'All',
	    listeners		:{
		    select: function(a, b, c){					       
		    	RefreshDataFilterKasirIGD();
		    }
		}
	});
	cboUNIT_viKasirIGD.hide();
    return cboUNIT_viKasirIGD;
	
};


function IGDLookUp(rowdata) 
{
	var i = setInterval(function(){
		RefreshDataKasirIGDDetail(rowdata);
	}, 400000);
     var lebar = 850;
    FormLookUpsdetailTRIGD = new Ext.Window({
            id			: 'gridIGD',
            title		: 'Penata Instalasi Gawat Darurat',
            closeAction	: 'destroy',
            width		: lebar,
            height		: 600,
            border		: false,
            resizable	: false,
            plain		: true,
            constrain	: true,
            layout		: 'fit',
            iconCls		: 'Request',
            modal		: true,
            items		: getFormEntryTRIGD(lebar,rowdata),
            listeners	: {
            	close: function(){	
            		RefreshDataFilterKasirIGD();
        		}
            }
    });
    FormLookUpsdetailTRIGD.show();
    dsCmbIGDPJDiag_IGD.loadData([],false);
	for(var i=0,iLen=PenataJasaIGD.ds2.getCount(); i<iLen; i++){
		var recs    = [],
		recType = dsCmbIGDPJDiag_IGD.recordType;
		var o=PenataJasaIGD.ds2.getRange()[i].data;
		recs.push(new recType({
			Id        	:o.KD_PENYAKIT,
			displayText : o.KD_PENYAKIT
	    }));
		dsCmbIGDPJDiag_IGD.add(recs);
	}
	PenataJasaIGD.ds4.load({ 
		params	: { 
			Skip	: 0, 
			Take	: 50, 
			target	:'ViewComboLabRJPJ'
		} 
	});
	var o=PenataJasaIGD.grid1.getSelectionModel().getSelections()[0].data;
	var par	= ' A.kd_pasien=~'+o.KD_PASIEN+'~ AND A.kd_unit=~'+o.KD_UNIT+'~ AND tgl_masuk=~'+o.TANGGAL_TRANSAKSI+'~ AND urut_masuk='+o.URUT_MASUK;
/* 	PenataJasaIGD.ds3.load({
		params	: { 
			Skip	: 0, 
			Take	: 50, 
			target	:'ViewGridLabRJPJ',
			param	:par
		}
	}); */
PenataJasaIGD.ds5.load({
        params: {
            Skip: 0,
            Take: 50,
            target: 'ViewComboJenisPelayanan'
        }
    });
  PenataJasaIGD.dssebabkematian.load({
      params: {
          Skip: 0,
          Take: 50,
          target: 'ViewComboSebabKematian'
      }
    });
  PenataJasaIGD.dsstatupulang.load({
      params: {
          Skip: 0,
          Take: 50,
          target: 'ViewComboStatusPulang',
          param:3
      }
    });
	var o=PenataJasaIGD.grid1.getSelectionModel().getSelections()[0].data;
	var params={
		kd_pasien 	: o.KD_PASIEN,
		kd_unit 	: o.KD_UNIT,
		tgl_masuk	: o.TANGGAL_TRANSAKSI,
		urut_masuk	: o.URUT_MASUK
	};
	Ext.Ajax.request({
		url			: baseURL + "index.php/main/functionRWJ/getTindakan",
		params		: params,
		failure		: function(o){
			ShowPesanWarningIGD('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
		},
		success		: function(o){
			var cst = Ext.decode(o.responseText);
			if (cst.success === true){
				if(cst.echo.id_status >=0){
					PenataJasaIGD.iCombo1.selectedIndex=cst.echo.id_status-1;
					PenataJasaIGD.iCombo1.setValue(PenataJasaIGD.ds5.getRange()[(cst.echo.id_status-1)].data.status);
					PenataJasaIGD.iTArea1.setValue(cst.echo.catatan);
				}
			}else{
				ShowPesanWarningIGD('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
			}
		}
	});
    if (rowdata == undefined){
        IGDAddNew();
    }else{
        TRIGDInit(rowdata);
    }
};







function KonsultasiAddNew() 
{
    AddNewKasirKonsultasi = true;
	Ext.get('txtKdunitKonsultasi').dom.value =Ext.get('txtKdUnitIGD').dom.value   ;
	Ext.get('txtNamaUnit_igdKonsultasi').dom.value=Ext.get('txtNamaUnit_igd').dom.value;
	

};

function getFormEntryTRIGD(lebar,data) 
{
    var pnlTRIGD = new Ext.FormPanel({
		id			: 'PanelTRIGD',
        fileUpload	: true,
        region		: 'north',
        layout		: 'column',
        bodyStyle	: 'padding:10px 10px 10px 10px',
        height		: 189,
        anchor		: '100%',
        width		: lebar,
        border		: false,
        items		: [getItemPanelInputIGD(lebar)],
        tbar		: [
			{
		        text: ' Ganti Dokter',
		        id:'btnLookUpGantiDokter_viKasirIGD',
		        iconCls: 'gantidok',
		        handler: function(){
				   GantiDokterLookUp_igd_igd();
		        }
	    	},{
		        text: 'Ganti Kelompok Pasien',
		        id:'btngantipasien_igd',
		        iconCls: 'gantipasien',
		        handler: function(){
		        	KelompokPasienLookUp_igd();
		        }
	    	},{
		        text: 'Posting Ke Kasir',
		        id:'btnposting_pj_Igd',
		        iconCls: 'gantidok',
		        handler: function(){
		        	setpostingtransaksi_igd(data.NO_TRANSAKSI);
		        }
			},
        ]
    });
    var x;
	var GDtabDetailIGD = new Ext.TabPanel({
        id			:'GDtabDetailIGD',
        region		: 'center',
        activeTab	: 0,
		height		: 480,
		width 		: 815,
        anchor		: '100% 100%',
        border		: false,
        plain		: true,
        defaults	: {
            autoScroll	: true
		},
        items		: [GetDTLTRDiagnosaGrid_igd(),GetDTLTRIGDGrid(data),PenataJasaIGD.getLabolatorium(data),GetDTLTRRadiologiGrid(data),PenataJasaIGD.getTindakan(data),PenataJasaIGD.ok_ok(data)],
        tbar		:[
			{
				text	: 'Jadwalkan Operasi',
		        id		: 'btnsimpanop_PJ_igd',
		        iconCls	: 'Konsultasi',
		        handler	: function(){
					if( Ext.getCmp('txtJam_viJdwlOperasi_Igd').getValue()>24 || Ext.getCmp('txtMenit_viJdwlOperasi_igd').getValue()>59 || Ext.getCmp('txtJam_viJdwlOperasi_Igd').getValue()<0 || Ext.getCmp('txtMenit_viJdwlOperasi_igd').getValue()<0){
						ShowPesanWarningIGD('Ketentuan Jam salah ', 'Validasi');		
					}else if( Ext.getCmp('txtJam_viJdwlOperasi_Igd').getValue()==''|| Ext.getCmp('txtJam_viJdwlOperasi_Igd').getValue()=='Jam'|| Ext.getCmp('txtMenit_viJdwlOperasi_igd').getValue()=='' || Ext.getCmp('txtMenit_viJdwlOperasi_igd').getValue()=='Menit' ){
						ShowPesanWarningIGD('Harap Isi Jam dan Menit ', 'Validasi');		
					}else if( Ext.getCmp('cbo_viComboJenisTindakan_viJdwlOperasi_igd').getValue()==''||Ext.getCmp('cbo_viComboJenisTindakan_viJdwlOperasi_igd').getValue()=='Pilih Jenis Tindakan..' ){
						ShowPesanWarningIGD('Harap Isi Tindakan ', 'Validasi');
					}else if( Ext.getCmp('cbo_viComboKamar_viJdwlOperasi_igd').getValue()==''||Ext.getCmp('cbo_viComboKamar_viJdwlOperasi_igd').getValue()=='Pilih Jenis Tindakan..' ){
						ShowPesanWarningIGD('Harap Isi Kamar Operasi ', 'Validasi');
					}else{
						Datasave_ok_Igd();
					}	
				}
			},{
				text	: 'Tambah item',
				id		: 'btnLookupIGD_igd',
				tooltip	: nmLookup,
				iconCls	: 'find',
				handler	: function(){
					PenataJasaIGD.dsGridTindakan.insert(PenataJasaIGD.dsGridTindakan.getCount(),PenataJasaIGD.func.getNullProduk());
				}
			},{
				text	: 'Simpan',
				id		: 'btnSimpanIGD_igd',
				tooltip	: nmSimpan,
				iconCls	: 'save',
				handler	: function(){
					var e=false;
					if(PenataJasaIGD.dsGridTindakan.getRange().length > 0){
						for(var i=0,iLen=PenataJasaIGD.dsGridTindakan.getRange().length; i<iLen ; i++){
							var o=PenataJasaIGD.dsGridTindakan.getRange()[i].data;
							if(o.QTY == '' || o.QTY==0 || o.QTY == null){
								PenataJasaIGD.alertError('Tindakan Yang Diberikan : "Qty" Pada Baris Ke-'+(i+1)+', Wajib Diisi.','Peringatan');
								e=true;
								break;
							}
							
						}
					}else{
						PenataJasaIGD.alertError('Isi Tindakan Yang Diberikan','Peringatan');
						e=true;
					}
					for(var i=0,iLen=PenataJasaIGD.dsGridObat.getRange().length; i<iLen ; i++){
						var o=PenataJasaIGD.dsGridObat.getRange()[i].data;
						if(o.nama_obat == '' || o.nama_obat == null){
							PenataJasaIGD.alertError('Terapi Obat : "Nama Obat" Pada Baris Ke-'+(i+1)+', Wajib Diisi.','Peringatan');
							e=true;
							break;
						}
						if(o.jumlah == '' || o.jumlah==0 || o.jumlah == null){
							PenataJasaIGD.alertError('Terapi Obat : "Qty" Pada Baris Ke-'+(i+1)+', Wajib Diisi.','Peringatan');
							e=true;
							break;
						}
						if(o.cara_pakai == ''||  o.cara_pakai == null){
							PenataJasaIGD.alertError('Terapi Obat : "Cara Pakai" Baris Ke-'+(i+1)+', Wajib Diisi.','Peringatan');
							e=true;
							break;
						}
						if(o.verified == '' || o.verified==null){
							PenataJasaIGD.alertError('Terapi Obat : "Verified" Baris Ke-'+(i+1)+', Wajib Diisi.','Peringatan');
							e=true;
							break;
						}
					}
					if(e==false){
						Datasave_KasirIGD(false);
					}
				}
			},{
                id		:'btnHpsBrsIGD_igd',
                text	: 'Hapus item',
                tooltip	: 'Hapus Baris',
                iconCls	: 'RemoveRow',
                handler	: function(){
                    if (dsTRDetailKasirIGDList .getCount() > 0 ){
                        if (cellSelecteddeskripsi_IGD != undefined){
                            if(CurrentKasirIGD != undefined){
                                    HapusBarisIGD();
                            }
                        }else{
                            ShowPesanWarningIGD('Pilih record ','Hapus data');
                        }
                    }
                }
            },{
				text	: 'Tambah Diagnosa',
				id		: 'btnLookupDiagnosa_igd',
				tooltip	: nmLookup,
				iconCls	: 'find',
				handler	: function(){
					PenataJasaIGD.ds2.insert(PenataJasaIGD.ds2.getCount(),PenataJasaIGD.func.getNullDiagnosa());
				}
			},{
				text	: 'Simpan',
				id		: 'btnSimpanDiagnosa_igd',
				tooltip	: nmSimpan,
				iconCls	: 'save',
				handler	: function(){
					if (dsTRDetailDiagnosaList_IGD.getCount() > 0 ){
						var e=false;
						for(var i=0,iLen=dsTRDetailDiagnosaList_IGD.getCount(); i<iLen; i++){
							var o=dsTRDetailDiagnosaList_IGD.getRange()[i].data;
							if(o.STAT_DIAG=='' || o.STAT_DIAG==null){
								PenataJasaIGD.alertError('Diagnosa : Diagnosa Pada Baris Ke-'+(i+1)+' Harus Diisi.','Peringatan');
								e=true;
								break;
							}
							if(o.KASUS=='' || o.KASUS==null){
								PenataJasaIGD.alertError('Diagnosa : Kasus Pada Baris Ke-'+(i+1)+' Harus Diisi.','Peringatan');
								e=true;
								break;
							}
						}
						if(e==false){
							Datasave_Diagnosa_IGD(false);
						}
					}
				}
			},{
	            id		:'btnHpsBrsDiagnosa_igd',
	            text	: 'Hapus item',
	            tooltip	: 'Hapus Baris',
	            iconCls	: 'RemoveRow',
                handler	: function(){
                    if (dsTRDetailDiagnosaList_IGD.getCount() > 0 ){
                        if (cellSelecteddeskripsi_IGD != undefined){
                        	if(CurrentDiagnosa_IGD != undefined){
                                HapusBarisDiagnosa_IGD();
                            }
                        }else{
                            ShowPesanWarningIGD('Pilih record ','Hapus data');
                        }
                    }
                }
			},{
				text	: 'Tambah Item Pemeriksaan',
				id		: 'btnbarisRad_igd',
				tooltip	: nmLookup,
				iconCls	: 'find',
				handler	: function(){
					var records = new Array();
					records.push(new dsIGDPJLab_IGD.recordType());
					dsIGDPJLab_IGD.add(records);
				}
			},{
				text	: 'Simpan',
				id		: 'btnSimpanRad_igd',
				tooltip	: nmSimpan,
				iconCls	: 'save',
				handler	: function(){
					if(dsIGDPJLab_IGD.getCount()==0){
								PenataJasaIGD.alertError('Laboratorium: Harap isi data Labolatorium.','Peringatan');
							}else{
								var e=false;
								for(var i=0,iLen=dsIGDPJLab_IGD.getCount();i<iLen ; i++){
									if(dsIGDPJLab_IGD.getRange()[i].data.kd_produk=='' || dsIGDPJLab_IGD.getRange()[i].data.kd_produk==null){
										PenataJasaIGD.alertError('Laboratorium: Data Laboratorium baris-'+(i+1)+' tidak Lengkap.','Peringatan');
										e=true;
										break;
									}
								}if(e==false){
								if (PenataJasaIGD.var_kd_dokter_rad==="" || PenataJasaIGD.var_kd_dokter_rad===undefined)
								{
									ShowPesanWarningIGD('harap Isi salah satu baris dokter', 'Gagal');
								 }else{
									Ext.Ajax.request({
										url			: baseURL + "index.php/main/functionRADPoliklinik/savedetailrad",
										params		: getParamDetailTransaksiRAD_IGD(),
										failure		: function(o){
											PenataJasaIGD.var_kd_dokter_rad="";
											ShowPesanWarningIGD('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
										},
										success		: function(o){
											var cst = Ext.decode(o.responseText);
											if (cst.success === true) {
												ShowPesanInfoDiagnosa_IGD('Data Berhasil Disimpan', 'Info');
												var o=PenataJasaIGD.grid1.getSelectionModel().getSelections()[0].data;
												var par	= ' A.kd_pasien=~'+o.KD_PASIEN+'~ AND A.kd_unit=~'+o.KD_UNIT+'~ AND tgl_masuk=~'+o.TANGGAL_TRANSAKSI+'~ AND urut_masuk='+o.URUT_MASUK;
													//	ViewGridBawahpoliLab_IGD(o.KD_PASIEN);
													ViewGridBawahpoliRad_igd(o.KD_PASIEN);
											}else if(cst.success === false && cst.cari=== false)
											{
												PenataJasaIGD.var_kd_dokter_rad="";
												ShowPesanWarningIGD('Harap lakukan pembayaran terlebih dahulu pada transaksi sebelumnya', 'Gagal');
											}else{
												PenataJasaIGD.var_kd_dokter_rad="";
												ShowPesanWarningIGD('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
											}
										}
									});
									}
							    }
							}
				}
			}, {
                id:'btnHpsBrsRad_igd',
                text: 'Hapus item',
                tooltip: 'Hapus Baris',
                iconCls: 'RemoveRow',
                handler: function(){
                  	var line=PenataJasaIGD.pj_req_rad.getSelectionModel().selection.cell[0];
							if(PenataJasaIGD.pj_req_rad.getSelectionModel().selection==null){
								ShowPesanWarningIGD('Harap Pilih terlebih dahulu data labolatorium.', 'Gagal');
							}else{
								Ext.Msg.show({
									title:nmHapusBaris,
				                   	msg: 'Anda yakin akan menghapus data kode produk' + ' : ' + dsIGDPJLab_IGD.getRange()[line].data.kd_produk ,
				                   	buttons: Ext.MessageBox.YESNO,
				                   	fn: function (btn){
				                   		if (btn =='yes'){
				                   			var o=PenataJasaIGD.grid1.getSelectionModel().getSelections()[0].data;
									
											if (dsIGDPJLab_IGD.getRange()[line].data.no_transaksi===""||dsIGDPJLab_IGD.getRange()[line].data.no_transaksi===undefined)
											{
											dsIGDPJLab_IGD.removeAt(line);
											PenataJasaIGD.pj_req_rad.getView().refresh();
											}else{
											     ShowPesanWarningIGD('data Tidak dapat dihapus karena sudah tersimpan didatabase', 'Gagal');
												 }
				                        }
				                   	},
				                   	icon: Ext.MessageBox.QUESTION
				                });
							}
                }
            },
				PenataJasaIGD.btn1= new Ext.Button({
					text	: 'Tambah Item Pemeriksaan',
					id		: 'RJPJBtnAddLab_PJ_IGD',
					tooltip	: nmLookup,
					iconCls	: 'find',
					handler	: function(){
						PenataJasaIGD.ds3.insert(PenataJasaIGD.ds3.getCount(),PenataJasaIGD.nullGrid3());
					}
				}),
				PenataJasaIGD.btn2= new Ext.Button({
					text	: 'Simpan',
					id		: 'RJPJBtnSaveLab_PJ_IGD',
					tooltip	: nmLookup,
					iconCls	: 'save',
						handler	: function(){
						if(PenataJasaIGD.ds3.getCount()==0){
								PenataJasaIGD.alertError('Laboratorium: Harap isi data Labolatorium.','Peringatan');
							}else{
								var e=false;
								for(var i=0,iLen=PenataJasaIGD.ds3.getCount();i<iLen ; i++){
									if(PenataJasaIGD.ds3.getRange()[i].data.kd_produk=='' || PenataJasaIGD.ds3.getRange()[i].data.kd_produk==null){
										PenataJasaIGD.alertError('Laboratorium: Data Laboratorium baris-'+(i+1)+' tidak Lengkap.','Peringatan');
										e=true;
										break;
									}
								}
								if(e==false){
								if (PenataJasaIGD.var_kd_dokter_leb==="" || PenataJasaIGD.var_kd_dokter_leb===undefined)
									{
									ShowPesanWarningIGD('harap Isi salah satu baris dokter', 'Gagal');
									}else{
									Ext.Ajax.request({
										url			: baseURL + "index.php/main/functionLABPoliklinik/savedetaillab",
										params		: getParamDetailTransaksiLAB_IGD(),
										failure		: function(o){
											ShowPesanWarningIGD('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
											PenataJasaIGD.var_kd_dokter_leb="";
										},
										success		: function(o){
											var cst = Ext.decode(o.responseText);
											if (cst.success === true) {
												ShowPesanInfoDiagnosa_IGD('Data Berhasil Disimpan', 'Info');
												var o=PenataJasaIGD.grid1.getSelectionModel().getSelections()[0].data;
												var par	= ' A.kd_pasien=~'+o.KD_PASIEN+'~ AND A.kd_unit=~'+o.KD_UNIT+'~ AND tgl_masuk=~'+o.TANGGAL_TRANSAKSI+'~ AND urut_masuk='+o.URUT_MASUK;
														ViewGridBawahpoliLab_IGD(o.KD_PASIEN);
											}else if(cst.success === false && cst.cari=== false)
												{
													PenataJasaIGD.var_kd_dokter_leb="";
													ShowPesanWarningIGD('Harap lakukan pembayaran terlebih dahulu pada transaksi sebelumnya', 'Gagal');
												}else{
													PenataJasaIGD.var_kd_dokter_leb="";
													ShowPesanWarningIGD('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
													}
										}
									});
								 }
							    }
							}
						}
					}),
					PenataJasaIGD.btn3= new Ext.Button({
						text	: 'Hapus',
						id		: 'RJPJBtnDelLab_PJ_IGD',
						tooltip	: nmLookup,
						iconCls	: 'RemoveRow',
						handler	: function(){
							var line=PenataJasaIGD.grid3.getSelectionModel().selection.cell[0];
							if(PenataJasaIGD.grid3.getSelectionModel().selection==null){
								ShowPesanWarningIGD('Harap Pilih terlebih dahulu data labolatorium.', 'Gagal');
							}else{
								Ext.Msg.show({
									title:nmHapusBaris,
				                   	msg: 'Anda yakin akan menghapus data kode produk' + ' : ' + PenataJasaIGD.ds3.getRange()[line].data.kd_produk ,
				                   	buttons: Ext.MessageBox.YESNO,
				                   	fn: function (btn){
				                   		if (btn =='yes'){
				                   			var o=PenataJasaIGD.grid1.getSelectionModel().getSelections()[0].data;
											var params={
												kd_pasien 	: o.KD_PASIEN,
												kd_unit 	: o.KD_UNIT,
												tgl_masuk	: o.TANGGAL_TRANSAKSI,
												urut_masuk	: o.URUT_MASUK,
												kd_produk	: PenataJasaIGD.ds3.getRange()[line].data.kd_produk
											};
											if (PenataJasaIGD.ds3.getRange()[line].data.no_transaksi===null|| PenataJasaIGD.ds3.getRange()[line].data.no_transaksi==='undefined'
											||PenataJasaIGD.ds3.getRange()[line].data.no_transaksi===''||PenataJasaIGD.ds3.getRange()[line].data.no_transaksi===undefined)
											{}else{
											/* Ext.Ajax.request({
												url			: baseURL + "index.php/main/functionRWJ/deletelaboratorium",
												params		: params,
												failure		: function(o){
													ShowPesanWarningIGD('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
												},
												success		: function(o){
													var cst = Ext.decode(o.responseText);
													if (cst.success === true) {
														var par	= ' A.kd_pasien=~'+o.KD_PASIEN+'~ AND A.kd_unit=~'+o.KD_UNIT+'~ AND tgl_masuk=~'+o.TANGGAL_TRANSAKSI+'~ AND urut_masuk='+o.URUT_MASUK;
														ShowPesanInfoDiagnosa_IGD('Data Berhasil Dihapus', 'Info');
													/* 	PenataJasaIGD.ds3.load({
															target	:'ViewGridLabRJPJ',
															param	:par
														});
													}else{
														ShowPesanWarningIGD('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
													}
												}
											}); */}
											if (PenataJasaIGD.ds3.getRange()[line].data.no_transaksi===""||PenataJasaIGD.ds3.getRange()[line].data.no_transaksi===undefined)
											{
											PenataJasaIGD.ds3.removeAt(line);
											PenataJasaIGD.grid3.getView().refresh();
											}else{
											 ShowPesanWarningIGD('data Tidak dapat dihapus karena sudah tersimpan didatabase', 'Gagal');
											}
				                        }
				                   	},
				                   	icon: Ext.MessageBox.QUESTION
				                });
							}
						}
					}),
					PenataJasaIGD.btn4= new Ext.Button({
						text	: 'Simpan',
						id		: 'RJPJBtnSaveTin',
						tooltip	: nmLookup,
						iconCls	: 'save',
						handler	: function(){
							var o=PenataJasaIGD.grid1.getSelectionModel().getSelections()[0].data;
							if(PenataJasaIGD.iCombo1.selectedIndex>-1){
								var params={
									kd_pasien  : o.KD_PASIEN,
                  kd_unit   : o.KD_UNIT,
                  tgl_masuk : o.TANGGAL_TRANSAKSI,
                  urut_masuk  : o.URUT_MASUK,
                  id_status : '',
                  catatan   : '',
                  tglkeluar: nowTglTransaksi_IGDGrid_poli_IGD,
                  jenisPelayanan: Ext.getCmp('iComboJenisPelayananRJPJ').getValue(),
                  keadaanakhir: Ext.getCmp('iComboStatusKeadaanAkhir').getValue()
								};
								Ext.Ajax.request({
									url			: baseURL + "index.php/main/functionIGD/saveTindakan",
									params		: params,
									failure		: function(o){
										ShowPesanWarningIGD('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
									},
									success		: function(o){
										var cst = Ext.decode(o.responseText);
										if (cst.success === true) {
											ShowPesanInfoDiagnosa_IGD('Data Berhasil Disimpan', 'Info');
										}else{
											ShowPesanWarningIGD('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
										}
									}
								});
							}else{
								ShowPesanWarningIGD('Status/Catatan tidak boleh kosong.', 'Peringatan');
							}
						}
					})
        ],
		listeners:{
               tabchange : function (panel, tab) {
				if (tab.id == 'tabDiagnosa'){
					dsCmbIGDPJDiag_IGD.loadData([],false);
					for(var i=0,iLen=PenataJasaIGD.ds2.getCount(); i<iLen; i++){
						var recs    = [],
						recType = dsCmbIGDPJDiag_IGD.recordType;
						var o=PenataJasaIGD.ds2.getRange()[i].data;
						recs.push(new recType({
							Id        :o.KD_PENYAKIT,
							displayText : o.KD_PENYAKIT
					    }));
						dsCmbIGDPJDiag_IGD.add(recs);
					}
					Ext.getCmp('catLainGroup_igd').hide();
					Ext.getCmp('txtneoplasma_igd').hide();
					Ext.getCmp('txtkecelakaan_igd').hide();
				
	                 Ext.getCmp('btnHpsBrsDiagnosa_igd').show();
					 Ext.getCmp('btnSimpanDiagnosa_igd').show();
					 Ext.getCmp('btnLookupDiagnosa_igd').show();
				 
	                 Ext.getCmp('btnHpsBrsIGD_igd').hide();
					 Ext.getCmp('btnSimpanIGD_igd').hide();
					 Ext.getCmp('btnLookupIGD_igd').hide();
				 
					 PenataJasaIGD.btn1.hide();
					 PenataJasaIGD.btn2.hide();
					 PenataJasaIGD.btn3.hide();
					 Ext.getCmp('btnbarisRad_igd').hide();
					 Ext.getCmp('btnSimpanRad_igd').hide();
					 Ext.getCmp('btnHpsBrsRad_igd').hide();
				 	 Ext.getCmp('btnsimpanop_PJ_igd').hide();
				     PenataJasaIGD.btn4.hide();
				}else if(tab.id == 'tabTransaksi'){
					 
					 
					 Ext.getCmp('btnHpsBrsDiagnosa_igd').hide();
					 Ext.getCmp('btnSimpanDiagnosa_igd').hide();
					 Ext.getCmp('btnLookupDiagnosa_igd').hide();
					 
					 Ext.getCmp('btnHpsBrsIGD_igd').show();
					 Ext.getCmp('btnSimpanIGD_igd').show();
					 Ext.getCmp('btnLookupIGD_igd').show();
					 
					 PenataJasaIGD.btn1.hide();
					 PenataJasaIGD.btn2.hide();
					 PenataJasaIGD.btn3.hide();
					 
					 PenataJasaIGD.btn4.hide();
					 
					 Ext.getCmp('btnbarisRad_igd').hide();
					 Ext.getCmp('btnSimpanRad_igd').hide();
					 Ext.getCmp('btnHpsBrsRad_igd').hide();
					 Ext.getCmp('btnsimpanop_PJ_igd').hide();
				 
				}else if (tab.id == 'tabradiologi'){
					 
					 Ext.getCmp('btnHpsBrsDiagnosa_igd').hide();
					 Ext.getCmp('btnSimpanDiagnosa_igd').hide();
					 Ext.getCmp('btnLookupDiagnosa_igd').hide();
					 Ext.getCmp('btnHpsBrsIGD_igd').hide();
					 Ext.getCmp('btnSimpanIGD_igd').hide();
					 Ext.getCmp('btnLookupIGD_igd').hide();
					 PenataJasaIGD.btn1.hide();
					 PenataJasaIGD.btn2.hide();
					 PenataJasaIGD.btn3.hide();
					 PenataJasaIGD.btn4.hide();
					 Ext.getCmp('btnsimpanop_PJ_igd').hide();
					 Ext.getCmp('btnbarisRad_igd').show();
         			 Ext.getCmp('btnSimpanRad_igd').show();
					 Ext.getCmp('btnHpsBrsRad_igd').show();

				}else if(tab.id == 'tabAnamnses'){				 
					 Ext.getCmp('btnHpsBrsDiagnosa_igd').hide();
					 Ext.getCmp('btnSimpanDiagnosa_igd').hide();
					 Ext.getCmp('btnLookupDiagnosa_igd').hide();
					 Ext.getCmp('btnHpsBrsIGD_igd').hide();
					 Ext.getCmp('btnSimpanIGD_igd').hide();
					 Ext.getCmp('btnLookupIGD_igd').hide();
					 PenataJasaIGD.btn1.hide();
					 PenataJasaIGD.btn2.hide();
					 PenataJasaIGD.btn3.hide();
					 Ext.getCmp('btnbarisRad_igd').hide();
					 Ext.getCmp('btnSimpanRad_igd').hide();
					 Ext.getCmp('btnHpsBrsRad_igd').hide();
					 Ext.getCmp('btnsimpanop_PJ_igd').hide();
					 PenataJasaIGD.btn4.hide();
				}else if(tab.id=='tabLaboratorium'){
					PenataJasaIGD.btn1.show();
					PenataJasaIGD.btn2.show();
					PenataJasaIGD.btn3.show();
					Ext.getCmp('btnHpsBrsDiagnosa_igd').hide();
					Ext.getCmp('btnSimpanDiagnosa_igd').hide();
					Ext.getCmp('btnLookupDiagnosa_igd').hide();
					Ext.getCmp('btnHpsBrsIGD_igd').hide();
					Ext.getCmp('btnSimpanIGD_igd').hide();
					Ext.getCmp('btnLookupIGD_igd').hide();
					Ext.getCmp('btnbarisRad_igd').hide();
					Ext.getCmp('btnSimpanRad_igd').hide();
					Ext.getCmp('btnHpsBrsRad_igd').hide();
				 	Ext.getCmp('btnsimpanop_PJ_igd').hide();
					PenataJasaIGD.btn4.hide();
				}else if(tab.id=='tabTindakan'){
					 Ext.getCmp('btnHpsBrsDiagnosa_igd').hide();
					 Ext.getCmp('btnSimpanDiagnosa_igd').hide();
					 Ext.getCmp('btnLookupDiagnosa_igd').hide();
					 Ext.getCmp('btnHpsBrsIGD_igd').hide();
					 Ext.getCmp('btnSimpanIGD_igd').hide();
					 Ext.getCmp('btnLookupIGD_igd').hide();
					 PenataJasaIGD.btn1.hide();
					 PenataJasaIGD.btn2.hide();
					 PenataJasaIGD.btn3.hide();
					 PenataJasaIGD.btn4.show();
					 Ext.getCmp('btnbarisRad_igd').hide();
					 Ext.getCmp('btnSimpanRad_igd').hide();
					 Ext.getCmp('btnHpsBrsRad_igd').hide();
					Ext.getCmp('btnsimpanop_PJ_igd').hide();
				}else if(tab.id=='tabjadwalop_rwi'){
					 Ext.getCmp('btnHpsBrsDiagnosa_igd').hide();
					 Ext.getCmp('btnSimpanDiagnosa_igd').hide();
					 Ext.getCmp('btnLookupDiagnosa_igd').hide();
					 Ext.getCmp('btnHpsBrsIGD_igd').hide();
					 Ext.getCmp('btnSimpanIGD_igd').hide();
					 Ext.getCmp('btnLookupIGD_igd').hide();
					 PenataJasaIGD.btn1.hide();
					 PenataJasaIGD.btn2.hide();
					 PenataJasaIGD.btn3.hide();
					 PenataJasaIGD.btn4.hide();
					 Ext.getCmp('btnbarisRad_igd').hide();
					 Ext.getCmp('btnSimpanRad_igd').hide();
					 Ext.getCmp('btnHpsBrsRad_igd').hide();
					 Ext.getCmp('btnsimpanop_PJ_igd').show();
					 
				 
				}
			}
        }
	});
   var pnlTRIGD2 = new Ext.FormPanel({
            id: 'PanelTRIGD2',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
             height:360,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [	GDtabDetailIGD]
    });
    var FormDepanIGD = new Ext.Panel({
		    id: 'FormDepanIGD',
		    region: 'center',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
			resizable:false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [pnlTRIGD,pnlTRIGD2	]
		});
	
	if( data.POSTING_TRANSAKSI == 't'){
		setdisablebutton_PJ_igd();
	}else{
		setenablebutton_PJ_igd();	
	}
    return FormDepanIGD;
};


function ViewGridBawahpoliRad_igd(no_transaksi,data) 
{	
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/main/functionRADPoliklinik/getItemPemeriksaan",
			params: {no_transaksi:no_transaksi},
			failure: function(o)
			{
				ShowPesanErrorIGD('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dsIGDPJLab_IGD.removeAll();
					var recs=[],
						recType=dsIGDPJLab_IGD.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
					dsIGDPJLab_IGD.add(recs);
					
					PenataJasaIGD.pj_req_rad.getView().refresh();
					
				
				} 
			}
		}
		
	)
};



function getParamDetailTransaksiRAD_IGD() 
{
	Ext.Ajax.request(
	{
	    url: baseURL + "index.php/main/getcurrentshift",
		 params: {
	        command: '0',
			},
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
		},	    
	    success: function(o) {
			tampungshiftsekarang=o.responseText}
	});
  var params =
	{	KdPasien:Ext.getCmp('txtNoMedrecDetransaksii_igd').getValue(),
		KdUnit: Ext.getCmp('txtKdUnitIGD').getValue(),
		KdDokter:PenataJasaIGD.var_kd_dokter_rad,
		KdDokter_mr_tindakan:Ext.get('txtKdDokterIGD').dom.value ,
		kd_kasir:'default_kd_kasir_igd',
		Modul:'igd',
		Tgl:'',
		TglTransaksiAsal:Ext.getCmp('dtpTanggalDetransaksi_igd').getValue(),
		KdCusto:vkode_customer_IGD,
		TmpCustoLama:'', 
		Shift: tampungshiftsekarang,
		List:getArrPoliRad_IGD(),
		JmlField: mRecordIGD.prototype.fields.length-4,
		JmlList:PenataJasaIGD.ds3.getCount(),
		unit:5,
		TmpNotransaksi:Ext.getCmp('txtNoTransaksiKasirIGD').getValue(),
		KdKasirAsal:'06',
		KdSpesial:'',
		Kamar:'',
		NoSJP:'',
		pasienBaru:0,
		listTrDokter	: []
	};
    return params
};
function getArrPoliRad_IGD()
{var x='';
console.log (dsIGDPJLab_IGD.getCount());
	var arr=[];
	for(var i = 0 ; i < dsIGDPJLab_IGD.getCount();i++)
	{
	
			var o={};
			var y='';
			var z='@@##$$@@';
		
			o['URUT']= dsIGDPJLab_IGD.data.items[i].data.urut;
			o['KD_PRODUK']= dsIGDPJLab_IGD.data.items[i].data.kd_produk;
			o['QTY']= dsIGDPJLab_IGD.data.items[i].data.qty;
			o['TGL_TRANSAKSI']= dsIGDPJLab_IGD.data.items[i].data.tgl_transaksi;
			o['TGL_BERLAKU']= dsIGDPJLab_IGD.data.items[i].data.tgl_berlaku;
			o['HARGA']= dsIGDPJLab_IGD.data.items[i].data.harga;
			o['KD_TARIF']= dsIGDPJLab_IGD.data.items[i].data.kd_tarif;
			o['NO_TRANSAKSI_BAWAH']= dsIGDPJLab_IGD.data.items[i].data.no_transaksi;
			o['cito']= dsIGDPJLab_IGD.data.items[i].data.cito;
			arr.push(o);
		
	}	
	
	return Ext.encode(arr);
};

function ViewGridBawahpoliLab_IGD(no_transaksi,data) 
{	
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/main/functionLABPoliklinik/getItemPemeriksaan",
			params: {no_transaksi:no_transaksi},
			failure: function(o)
			{
				ShowPesanWarningIGD('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					PenataJasaIGD.ds3.removeAll();
					var recs=[],
						recType=PenataJasaIGD.ds3.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
					PenataJasaIGD.ds3.add(recs);
					
					PenataJasaIGD.grid3.getView().refresh();
					
				
				} 
			}
		}
		
	)
};
function getParamDetailTransaksiLAB_IGD() 
{
	Ext.Ajax.request(
	{
	    url: baseURL + "index.php/main/getcurrentshift",
		 params: {
	        command: '0',
			},
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
		},	    
	    success: function(o) {
			tampungshiftsekarang=o.responseText}
	});
  var params =
	{	KdPasien:Ext.getCmp('txtNoMedrecDetransaksii_igd').getValue(),
		KdUnit: Ext.getCmp('txtKdUnitIGD').getValue(),
		KdDokter:PenataJasaIGD.var_kd_dokter_leb,
		KdDokter_mr_tindakan:Ext.get('txtKdDokterIGD').dom.value ,
		kd_kasir:'default_kd_kasir_igd',
		Modul:'igd',
		Tgl:'',
		TglTransaksiAsal:Ext.getCmp('dtpTanggalDetransaksi_igd').getValue(),
		KdCusto:vkode_customer_IGD,
		TmpCustoLama:'', 
		Shift: tampungshiftsekarang,
		List:getArrPoliLab_IGD(),
		JmlList:PenataJasaIGD.ds3.getCount(),
		unit:41,
		TmpNotransaksi:Ext.getCmp('txtNoTransaksiKasirIGD').getValue(),
		KdKasirAsal:'06',
		KdSpesial:'',
		Kamar:'',
		NoSJP:'',
		pasienBaru:0,
		listTrDokter	: []
	};
    return params
};


function getArrPoliLab_IGD()
{
var x='';
console.log (PenataJasaIGD.ds3.getCount());
	var arr=[];
	for(var i = 0 ; i < PenataJasaIGD.ds3.getCount();i++)
	{
	
			var o={};
			var y='';
			var z='@@##$$@@';
		
			o['URUT']= PenataJasaIGD.ds3.data.items[i].data.urut;
			o['KD_PRODUK']= PenataJasaIGD.ds3.data.items[i].data.kd_produk;
			o['QTY']= PenataJasaIGD.ds3.data.items[i].data.qty;
			o['TGL_TRANSAKSI']= PenataJasaIGD.ds3.data.items[i].data.tgl_transaksi;
			o['TGL_BERLAKU']= PenataJasaIGD.ds3.data.items[i].data.tgl_berlaku;
			o['HARGA']= PenataJasaIGD.ds3.data.items[i].data.harga;
			o['KD_TARIF']= PenataJasaIGD.ds3.data.items[i].data.kd_tarif;
			o['NO_TRANSAKSI_BAWAH']= PenataJasaIGD.ds3.data.items[i].data.no_transaksi;
			o['cito']= PenataJasaIGD.ds3.data.items[i].data.cito;
			arr.push(o);
		
	}	
	
	return Ext.encode(arr);
};
function HapusBarisDiagnosa_IGD()
{
    if ( cellSelecteddeskripsi_IGD != undefined )
    {
        if (cellSelecteddeskripsi_IGD.data.PENYAKIT != '' && cellSelecteddeskripsi_IGD.data.KD_PENYAKIT != '')
        {
            Ext.Msg.show
            (
                {
                   title:nmHapusBaris,
                   msg: 'Anda yakin akan menghapus produk' + ' : ' + cellSelecteddeskripsi_IGD.data.PENYAKIT ,
                   buttons: Ext.MessageBox.YESNO,
                   fn: function (btn)
                   {
                       if (btn =='yes')
                        {
                            if(dsTRDetailDiagnosaList_IGD.data.items[CurrentDiagnosa_IGD.row].data.URUT_MASUK === '')
                            {
                                dsTRDetailDiagnosaList_IGD.removeAt(CurrentDiagnosa_IGD.row);
                            }
                            else
                            {
                                
                                            if (btn =='yes')
                                            {
                                               DataDeleteDiagnosaDetail_igd();
                                            };
                                
                            };
                        };
                   },
                   icon: Ext.MessageBox.QUESTION
                }
            );
        }
        else
        {
            dsTRDetailDiagnosaList_IGD.removeAt(CurrentDiagnosa_IGD.row);
        };
    }
};

function DataDeleteDiagnosaDetail_igd()
{
    Ext.Ajax.request
    (
        {
            
            url: baseURL + "index.php/main/DeleteDataObj",
            params:  getParamDataDeleteDiagnosaDetail_igd(),
            success: function(o)
            {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true)
                {
                    ShowPesanInfoDiagnosa_IGD(nmPesanHapusSukses,nmHeaderHapusData);
                    dsTRDetailDiagnosaList_IGD.removeAt(CurrentDiagnosa_IGD.row);
                    cellSelecteddeskripsi_IGD=undefined;
                  RefreshDataSetDiagnosa_igd(Ext.get('txtNoMedrecDetransaksii_igd').dom.value,Ext.get('txtKdUnitIGD').dom.value,Ext.get('dtpTanggalDetransaksi_igd').dom.value);
                    AddNewDiagnosa_IGD = false;
                }
           
                else
                {
                    ShowPesanWarningDiagnosa_igd(nmPesanHapusError,nmHeaderHapusData);
					RefreshDataSetDiagnosa_igd(Ext.get('txtNoMedrecDetransaksii_igd').dom.value,Ext.get('txtKdUnitIGD').dom.value,Ext.get('dtpTanggalDetransaksi_igd').dom.value);
                };
            }
        }
    )
};

function getParamDataDeleteDiagnosaDetail_igd()
{
    var params =
    {
        Table: 'ViewDiagnosa',
        KdPasien: Ext.get('txtNoMedrecDetransaksii_igd').getValue(),
		KdUnit: Ext.get('txtKdUnitIGD').getValue(),
		TglMasuk:CurrentDiagnosa_IGD.data.data.TGL_MASUK,
		KdPenyakit : CurrentDiagnosa_IGD.data.data.KD_PENYAKIT,
		UrutMasuk:CurrentDiagnosa_IGD.data.data.URUT_MASUK,
		Urut:CurrentDiagnosa_IGD.data.data.URUT,
    };
	
    return params
};



function getParamDetailTransaksiDiagnosa2_igd() 
{
    var params =
	{
		Table:'ViewTrDiagnosa',
		KdPasien: Ext.get('txtNoMedrecDetransaksii_igd').getValue(),
		KdUnit: Ext.get('txtKdUnitIGD').getValue(),
		UrutMasuk:Ext.get('txtKdUrutMasuk_igd').getValue(),
		Tgl: Ext.get('dtpTanggalDetransaksi_igd').dom.value,
		List:getArrDetailTrDiagnosa_igd(),
		JmlField: mRecordDiagnosa_IGD.prototype.fields.length-4,
		JmlList:GetListCountDetailDiagnosa_igd(),
		Hapus:1,
		Ubah:0
	};
    return params
};


function GetListCountDetailDiagnosa_igd()
{var x=0;
	for(var i = 0 ; i < dsTRDetailDiagnosaList_IGD.getCount();i++)
	{
		if (dsTRDetailDiagnosaList_IGD.data.items[i].data.KD_PENYAKIT != '' || dsTRDetailDiagnosaList_IGD.data.items[i].data.PENYAKIT  != '')
		{
			x += 1;
		};
	}
	return x;
	
};




function getArrDetailTrDiagnosa_igd()
{
	var x='';
	for(var i = 0 ; i < dsTRDetailDiagnosaList_IGD.getCount();i++)
	{
		if (dsTRDetailDiagnosaList_IGD.data.items[i].data.KD_PENYAKIT != '' && dsTRDetailDiagnosaList_IGD.data.items[i].data.PENYAKIT != '')
		{
			var y='';
			var z='@@##$$@@';
			
			y = dsTRDetailDiagnosaList_IGD.data.items[i].data.URUT_MASUK
			y += z + dsTRDetailDiagnosaList_IGD.data.items[i].data.KD_PENYAKIT
			y += z + dsTRDetailDiagnosaList_IGD.data.items[i].data.STAT_DIAG
			y += z + dsTRDetailDiagnosaList_IGD.data.items[i].data.KASUS
			
			
			if (i === (dsTRDetailDiagnosaList_IGD.getCount()-1))
			{
				x += y 
			}
			else
			{
				x += y + '##[[]]##'
			};
		};
	}	
	
	return x;
};




function Datasave_Diagnosa_IGD(mBol) 
{	
			Ext.Ajax.request
			 (
				{
					//url: "./Datapool.mvc/CreateDataObj",
					url: baseURL + "index.php/main/functionIGD/saveDiagnosa",
					params: getParamDetailTransaksiDiagnosa2_igd(),
					success: function(o) 
					{
	
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoDiagnosa_IGD(nmPesanSimpanSukses,nmHeaderSimpanData);
							//RefreshDataDiagnosa();
							if(mBol === false)
							{
						
							RefreshDataSetDiagnosa_igd(Ext.get('txtNoMedrecDetransaksii_igd').dom.value,Ext.get('txtKdUnitIGD').dom.value,Ext.get('dtpTanggalDetransaksi_igd').dom.value);
								
							};
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							RefreshDataSetDiagnosa_igd(Ext.get('txtNoMedrecDetransaksii_igd').dom.value,Ext.get('txtKdUnitIGD').dom.value,Ext.get('dtpTanggalDetransaksi_igd').dom.value);
							ShowPesanWarningDiagnosa_igd(nmPesanSimpanGagal,nmHeaderSimpanData);
						}
						
						else 
						{
							RefreshDataSetDiagnosa_igd(Ext.get('txtNoMedrecDetransaksii_igd').dom.value,Ext.get('txtKdUnitIGD').dom.value,Ext.get('dtpTanggalDetransaksi_igd').dom.value);
							ShowPesanErrorDiagnosa_igd(nmPesanSimpanError,nmHeaderSimpanData);
						};
					}
				}
			)
		
	
};

function ShowPesanWarningDiagnosa_igd(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorDiagnosa_igd(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoDiagnosa_IGD(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};







/* function GetDTLTRDiagnosaGrid_igd() 
{
    var fldDetail = ['KD_PENYAKIT','PENYAKIT','KD_PASIEN','URUT','URUT_MASUK','TGL_MASUK','KASUS','STAT_DIAG'];
	
    dsTRDetailDiagnosaList_IGD = new WebApp.DataStore({ fields: fldDetail })

    var gridDTLTRDiagnosa = new Ext.grid.EditorGridPanel
    (
        {
            title: 'Diagnosa',
            stripeRows: true,
            store: dsTRDetailDiagnosaList_IGD,
            border: true,
            columnLines: true,
            frame: false,
            anchor: '100%',
             autoScroll:true,
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                            cellSelecteddeskripsi_IGD = dsTRDetailDiagnosaList_IGD.getAt(row);
                            CurrentDiagnosa_IGD.row = row;
                            CurrentDiagnosa_IGD.data = cellSelecteddeskripsi_IGD;
                           // FocusCtrlCMDiagnosa='txtAset';
                        }
                    }
                }
            ),
            cm: TRDiagnosaColumModel_igd()
             , viewConfig:{forceFit: true}
        }
		
		
    );
	
	

    return gridDTLTRDiagnosa;
};

function TRDiagnosaColumModel_igd() 
{
    return new Ext.grid.ColumnModel
    (
        [
           new Ext.grid.RowNumberer(),
            {
                id: Nci.getId(),
                header: 'No.ICD',
                dataIndex: 'KD_PENYAKIT',
                width:70,
					menuDisabled:true,
                hidden:false
            },
			{
                id: Nci.getId(),
                header: 'Penyakit',
                dataIndex: 'PENYAKIT',
					menuDisabled:true,
				width:200
                
            }
            ,
			{
                id	: Nci.getId(),
                header: 'kd_pasien',
                dataIndex: 'KD_PASIEN',
				hidden:true
                
            }
			,
			{
               id: Nci.getId(),
                header: 'urut',
                dataIndex: 'URUT',
				hidden:true
                
            }
			,
			{
				id	: Nci.getId(),
                header: 'urut masuk',
                dataIndex: 'URUT_MASUK',
				hidden:true
                
            }
            ,
			{
               id: Nci.getId(),
                header: 'tgl masuk',
                dataIndex: 'TGL_MASUK',
				hidden:true
                
            }
            ,
            {
                id	: Nci.getId(),
                header: 'Diagnosa',
                width:130,
				menuDisabled:true,
				//align: 'right',
				//hidden :true,
                dataIndex: 'STAT_DIAG',
                editor: new Ext.form.ComboBox
                (
                    {
							id:'cboDiagnosa',
							typeAhead: true,
							triggerAction: 'all',
							lazyRender:true,
							mode: 'local',
							selectOnFocus:true,
							forceSelection: true,
							emptyText:'Silahkan Pilih...',
							//fieldLabel: 'Jenis',
							width:50,
							anchor: '95%',
							value:1,
							store: new Ext.data.ArrayStore
							(
								{
									id: 0,
									fields:
									[
										'Id',
										'displayText'
									],
								data: [[1, 'Diagnosa Awal'],[2, 'Diagnosa Utama'],[3, 'Komplikasi'],[4, 'Diagnosa Sekunder']]
								}
							),
						valueField: 'displayText',
						displayField: 'displayText',
						value:'',
						listeners:
						{
							
						}
					}
                ),
				
				
              
            },
            {
               id: Nci.getId(),
                header: 'Kasus',
                width:130,
				//align: 'right',
				//hidden :true,
				menuDisabled:true,
                dataIndex: 'KASUS',
                editor: new Ext.form.ComboBox
                (
                    {
							id:'cboKasus',
							typeAhead: true,
							triggerAction: 'all',
							lazyRender:true,
							mode: 'local',
							selectOnFocus:true,
							forceSelection: true,
							emptyText:'Silahkan Pilih...',
							//fieldLabel: 'Jenis',
							width:50,
							anchor: '95%',
							value:1,
							store: new Ext.data.ArrayStore
							(
								{
									id: 0,
									fields:
									[
										'Id',
										'displayText'
									],
								data: [[1, 'Baru'],[2, 'Lama']]
								}
							),
						valueField: 'displayText',
						displayField: 'displayText',
						value:'',
						listeners:
						{
							
						}
					}
                ),
				
				
              
            }
			

        ]
    )
}; */

///---------------------------------------------------------------------------------------///


function HapusBarisIGD()
{
    if ( cellSelecteddeskripsi_IGD != undefined )
    {
        if (cellSelecteddeskripsi_IGD.data.DESKRIPSI2 != '' && cellSelecteddeskripsi_IGD.data.KD_PRODUK != '')
        {
		
           
                                          
											var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan penghapusan:', function(btn, combo){
											if (btn == 'ok')
														{
														variablehistori_IGD=combo;
														
														DataDeleteKasirIGDDetail();
													    dsTRDetailKasirIGDList.removeAt(CurrentKasirIGD.row);
														}
												});

												
                                         
                                
               
        }
        else
        {
            dsTRDetailKasirIGDList.removeAt(CurrentKasirIGD.row);
        };
    }
};

function DataDeleteKasirIGDDetail()
{
    Ext.Ajax.request
    (
        {
                //url: "./Datapool.mvc/DeleteDataObj",
            url: baseURL + "index.php/main/DeleteDataObj",
            params:  getParamDataDeleteKasirIGDDetail(),
            success: function(o)
            {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true)
                {
                    ShowPesanInfoIGD(nmPesanHapusSukses,nmHeaderHapusData);
                    dsTRDetailKasirIGDList.removeAt(CurrentKasirIGD.row);
                    cellSelecteddeskripsi_IGD=undefined;
                    RefreshDataKasirIGDDetail(Ext.get('txtNoTransaksiKasirIGD').dom.value);
                    AddNewKasirIGD = false;
                }
                else if (cst.success === false && cst.produktr === true )
                {
                    ShowPesanWarningIGD('Produk Transfer Tidak dapat dihapus', nmHeaderHapusData);
					  RefreshDataKasirIGDDetail(Ext.get('txtNoTransaksiKasirIGD').dom.value);
                }
                else
                {
                    ShowPesanWarningIGD(nmPesanHapusError,nmHeaderHapusData);
                };
            }
        }
    )
};

function getParamDataDeleteKasirIGDDetail()
{
	console.log(CurrentKasirIGD.data);
    var params =
    {
		Table: 'ViewTrKasirIGD',
        TrKodeTranskasi: CurrentKasirIGD.data.data.NO_TRANSAKSI,
		TrTglTransaksi:  CurrentKasirIGD.data.data.TGL_TRANSAKSI,
		TrKdPasien :	 CurrentKasirIGD.data.data.KD_PASIEN,
		kodePasien: Ext.getCmp('txtNoMedrecDetransaksii_igd').getValue(),
		TrKdNamaPasien : Ext.get('txtNamaPasienDetransaksi_igd').getValue(),	
		TrKdUnit :		 Ext.get('txtKdUnitIGD').getValue(),
		TrNamaUnit :	 Ext.get('txtNamaUnit_igd').getValue(),
		Uraian :		 CurrentKasirIGD.data.data.DESKRIPSI2,
		AlasanHapus : variablehistori_IGD,
		TrHarga :		 CurrentKasirIGD.data.data.HARGA,
		
		TrKdProduk :	 CurrentKasirIGD.data.data.KD_PRODUK,
        RowReq: CurrentKasirIGD.data.data.URUT,
        Hapus:2
    };
	
    return params
};



/* 
function GetDTLTRIGDGrid() 
{
    var fldDetail = ['KD_PRODUK','DESKRIPSI','DESKRIPSI2','KD_TARIF','HARGA','QTY','DESC_REQ','TGL_BERLAKU','NO_TRANSAKSI','URUT','DESC_STATUS','TGL_TRANSAKSI'];
	
    dsTRDetailKasirIGDList = new WebApp.DataStore({ fields: fldDetail })
   RefreshDataKasirIGDDetail() ;
    var gridDTLTRIGD = new Ext.grid.EditorGridPanel
    (
        {
            title: 'Input Tindakan',
            stripeRows: true,
            store: dsTRDetailKasirIGDList,
            border: true,
            columnLines: true,
            frame: false,
            anchor: '100%',
            autoScroll:true,
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                            cellSelecteddeskripsi_IGD = dsTRDetailKasirIGDList.getAt(row);
                            CurrentKasirIGD.row = row;
                            CurrentKasirIGD.data = cellSelecteddeskripsi_IGD;
                           // FocusCtrlCMIGD='txtAset';
                        }
                    }
                }
            ),
            cm: TRGawatdaruratColumModel()
              , viewConfig:{forceFit: true}
        }
		
		
    );
	
	

    return gridDTLTRIGD;
};

function TRGawatdaruratColumModel() 
{
    return new Ext.grid.ColumnModel
    (
        [
            new Ext.grid.RowNumberer(),
            {
               id: Nci.getId(),
                header: 'Uraian',
                dataIndex: 'DESKRIPSI2',
                width:250,
				menuDisabled:true,
				hidden :true
                
            },
            {
                id: Nci.getId(),
                header: 'Kode Produk',
                dataIndex: 'KD_PRODUK',
                width:100,
				menuDisabled:true,
                hidden:true
            },
            {
                id: Nci.getId(),
                header:'Nama Produk',
                dataIndex: 'DESKRIPSI',
                sortable: false,
                hidden:false,
				menuDisabled:true,
                width:320,
				editor: new Ext.form.TextField
                (
                    {
                        id:'fieldAsetNameRequest',
                        allowBlank: false,
                        enableKeyEvents : true,
                        listeners:
                        {
                            'specialkey' : function()
                            {
                                if (Ext.EventObject.getKey() === 13)
                                {
                                    var str='';

                                   if (Ext.get('txtKdUnitIGD').dom.value  != undefined && Ext.get('txtKdUnitIGD').dom.value  != '')
									{
											//str = ' where kd_dokter =~' + Ext.get('txtKdDokterIGD').dom.value  + '~';
											//str = "\"kd_dokter\" = ~" + Ext.get('txtKdDokterIGD').dom.value  + "~";
											str =  Ext.get('txtKdUnitIGD').dom.value;
									};
									strb='';
									
									//alert(strb);
									if(Ext.get('fieldAsetNameRequest').dom.value != undefined || Ext.get('fieldAsetNameRequest').dom.value  != '')
									{
									strb= "and lower(deskripsi) like lower(~"+ Ext.get('fieldAsetNameRequest').dom.value+"%~)";
									}
									else
									{
									strb='';
									}
									//alert(strb);
									GetLookupAssetCMIGD(str,strb);
								  Ext.get('fieldAsetNameRequest').dom.value='';
                                };
                            }
                        }
                    }
                )
                
            }
			,
            {  id: Nci.getId(),
               header: 'Tanggal Transaksi',
               dataIndex: 'TGL_TRANSAKSI',
               width: 130,
			   	menuDisabled:true,
               renderer: function(v, params, record)
                    {
                        
                        return ShowDate(record.data.TGL_TRANSAKSI);
                    }
            },
            {
                id: Nci.getId(),
                header: 'Harga',
				align: 'right',
				hidden: true,
				menuDisabled:true,
                dataIndex: 'HARGA',
                width:100,
				renderer: function(v, params, record) 
							{
							return formatCurrency(record.data.HARGA);
							
							}	
            },
            {
                id: Nci.getId(),
                header: 'Qty',
                width:91,
				align: 'right',
				menuDisabled:true,
                dataIndex: 'QTY',
                editor: new Ext.form.TextField
                (
                    {
                        id:'fieldcolProblemIGD',
                        allowBlank: true,
                        enableKeyEvents : true,
                        width:30,
						listeners:
							{ 
								'specialkey' : function()
								{
									
											Dataupdate_KasirIGD(false);
											//RefreshDataFilterKasirIGD();
									        //RefreshDataFilterKasirIGD();

								}
							}
                    }
                ),
				
				
              
            },

            {
               id: Nci.getId(),
                header: 'CR',
                width:80,
                dataIndex: 'IMPACT',
				hidden: true,
                editor: new Ext.form.TextField
                (
                        {
                                id:'fieldcolImpactIGD',
                                allowBlank: true,
                                enableKeyEvents : true,
                                width:30
                        }
                )
				
            }

        ]
    )
};

function GetLookupAssetCMIGD(str,strb)
{
	if (AddNewKasirIGD === true)
	{
		var p = new mRecordIGD
		(
			{
				'DESKRIPSI2':'',
				'KD_PRODUK':'',
				'DESKRIPSI':'', 
				'KD_TARIF':'', 
				'HARGA':'',
				'QTY':dsTRDetailKasirIGDList.data.items[CurrentKasirIGD.row].data.QTY,
				'TGL_TRANSAKSI':dsTRDetailKasirIGDList.data.items[CurrentKasirIGD.row].data.TGL_TRANSAKSI,
				'DESC_REQ':dsTRDetailKasirIGDList.data.items[CurrentKasirIGD.row].data.DESC_REQ,
				'KD_TARIF':dsTRDetailKasirIGDList.data.items[CurrentKasirIGD.row].data.KD_TARIF,
				'URUT':''
			}
		);
		
		FormLookupKasir(str,strb,nilai_kd_tarif,dsTRDetailKasirIGDList,p,true,'',false,syssetting);
	}
	else
	{	
		var p = new mRecordIGD
		(
			{
				'DESKRIPSI2':'',
				'KD_PRODUK':'',
				'DESKRIPSI':'', 
				'KD_TARIF':'', 
				'HARGA':'',
				'QTY':dsTRDetailKasirIGDList.data.items[CurrentKasirIGD.row].data.QTY,
				'TGL_TRANSAKSI':dsTRDetailKasirIGDList.data.items[CurrentKasirIGD.row].data.TGL_TRANSAKSI,
				'DESC_REQ':dsTRDetailKasirIGDList.data.items[CurrentKasirIGD.row].data.DESC_REQ,
				'KD_TARIF':dsTRDetailKasirIGDList.data.items[CurrentKasirIGD.row].data.KD_TARIF,
				'URUT':dsTRDetailKasirIGDList.data.items[CurrentKasirIGD.row].data.URUT
			}
		);
	
		FormLookupKasir(str,strb,nilai_kd_tarif,dsTRDetailKasirIGDList,p,false,CurrentKasirIGD.row,false,syssetting);
	};
}; */

/* function RecordBaruIGD()
{

	var p = new mRecordIGD
	(
		{
			'DESKRIPSI2':'',
			'KD_PRODUK':'',
		    'DESKRIPSI':'', 
		    'KD_TARIF':'', 
		    'HARGA':'',
		    'QTY':'',
		    'TGL_TRANSAKSI':tanggaltransaksitampung_IGD, 
		    'DESC_REQ':'',
		    'KD_TARIF':'',
		    'URUT':''
		}
	);
	
	return p;
}; */
function RefreshDataSetDiagnosa_igd(medrec,unit,tgl)
{	
 var strKriteriaDiagnosa='';
    //strKriteriaDiagnosa = 'where no_transaksi = ~' + no_transaksi + '~'
    strKriteriaDiagnosa = 'kd_pasien = ~' + medrec + '~ and kd_unit=~'+unit+'~ and tgl_masuk in(~'+tgl+'~)';
    //strKriteriaDiagnosa = 'no_transaksi = ~0000004~';
	
	dsTRDetailDiagnosaList_IGD.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: selectCountDiagnosa_IGD, 
				//Sort: 'EMP_ID',
                Sort: 'kd_penyakit',
				Sortdir: 'ASC', 
				target:'ViewDiagnosa',
				param: strKriteriaDiagnosa
			} 
		}
	);
	rowSelectedDiagnosa_IGD = undefined;
	return dsTRDetailDiagnosaList_IGD;
};

function TRIGDInit(rowdata)
{
	Ext.get('txtNoTransaksiKasirIGD').dom.value = rowdata.NO_TRANSAKSI;
	tanggaltransaksitampung_IGD = rowdata.TANGGAL_TRANSAKSI;
    Ext.get('dtpTanggalDetransaksi_igd').dom.value = ShowDate(rowdata.TANGGAL_TRANSAKSI);
	Ext.get('txtNoMedrecDetransaksii_igd').dom.value= rowdata.KD_PASIEN;
	Ext.get('txtNamaPasienDetransaksi_igd').dom.value = rowdata.NAMA;
	Ext.get('txtKdDokterIGD').dom.value   = rowdata.KD_DOKTER;
	Ext.get('txtNamaDokter_igd').dom.value = rowdata.NAMA_DOKTER;
	Ext.get('txtKdUnitIGD').dom.value   = rowdata.KD_UNIT;
	Ext.get('txtNamaUnit_igd').dom.value = rowdata.NAMA_UNIT;
	Ext.get('txtCustomer_igd').dom.value = rowdata.CUSTOMER;
	Ext.get('txtKdUrutMasuk_igd').dom.value = rowdata.URUT_MASUK;
	/* var $this	= this; */
	//$this.dsComboObat	= new WebApp.DataStore({ fields: ['kd_prd','nama_obat','jml_stok_apt'] });
	PenataJasaIGD.dsComboObat.load({ 
		params	: { 
			Skip	: 0, 
			Take	: 50, 
			target	:'ViewComboObatRJPJ',
			kdcustomer: vkode_customer_IGD
		} 
	});
	vkode_customer_IGD = rowdata.KD_CUSTOMER;
	Ext.Ajax.request(
	{
	    url:  baseURL + "index.php/main/functionLABPoliklinik/gettarif",
		 params: {
	        kd_customer: vkode_customer_IGD,
	    },
		failure: function(o)
		{},	    
	    success: function(o) {
			 var cst = Ext.decode(o.responseText);
			PenataJasaIGD.varkd_tarif=cst.kd_tarif;
			
			getproduk_PJIGD();
			dokter_leb_IGD();
			dokter_rad_IGD();
			 }
	});
	
	RefreshDataKasirIGDDetail(rowdata.NO_TRANSAKSI);
	ViewGridBawahpoliRad_igd(rowdata.KD_PASIEN);
	//RefreshDataSetRadiologi_igd();
	ViewGridBawahpoliLab_IGD(rowdata.KD_PASIEN);
	RefreshDataSetDiagnosa_igd(rowdata.KD_PASIEN,rowdata.KD_UNIT,rowdata.TANGGAL_TRANSAKSI);
	Ext.Ajax.request({
	    url: baseURL + "index.php/main/getcurrentshift",
		 params: {
	        command: '0'
	    },
		failure: function(o){
			 var cst = Ext.decode(o.responseText);
		},	    
	    success: function(o) {
			tampungshiftsekarang=o.responseText;
	    }
	});
	
};

function mEnabledIGDCM(mBol)
{
//	 Ext.get('btnSimpanIGD_igd').dom.disabled=mBol;
//	 Ext.get('btnSimpanKeluarIGD').dom.disabled=mBol;
//	 Ext.get('btnHapusIGD').dom.disabled=mBol;
	 Ext.get('btnLookupIGD_igd').dom.disabled=mBol;
//	 Ext.get('btnTambahBrsIGD').dom.disabled=mBol;
	 Ext.get('btnHpsBrsIGD_igd').dom.disabled=mBol;
};


///---------------------------------------------------------------------------------------///
function IGDAddNew() 
{
    AddNewKasirIGD = true;
	Ext.get('txtNoTransaksiKasirIGD').dom.value = '';
    Ext.get('dtpTanggalDetransaksi_igd').dom.value = nowTglTransaksi_IGD.format('d/M/Y');
	Ext.get('txtNoMedrecDetransaksii_igd').dom.value='';
	Ext.get('txtNamaPasienDetransaksi_igd').dom.value = '';
	Ext.get('txtKdDokterIGD').dom.value   = undefined;
	Ext.get('txtNamaDokter_igd').dom.value = '';
	Ext.get('txtKdUrutMasuk_igd').dom.value = '';
	Ext.get('cboStatus_viKasirIGD').dom.value= ''
	rowSelectedKasirIGD=undefined;
	dsTRDetailKasirIGDList.removeAll();
	mEnabledIGDCM(false);
	

};

function RefreshDataKasirIGDDetail(no_transaksi) 
{
    var strKriteriaIGD='';
    //strKriteriaIGD = 'where no_transaksi = ~' + no_transaksi + '~'
    strKriteriaIGD = "\"no_transaksi\" = ~" + no_transaksi + "~ and kd_kasir=~06~";
    //strKriteriaIGD = 'no_transaksi = ~0000004~';
   
    dsTRDetailKasirIGDList.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'tgl_transaksi',
			    //Sort: 'tgl_transaksi',
			    Sortdir: 'ASC',
			    target: 'ViewDetailTRRWJ',
			    param: strKriteriaIGD
			}
		}
	);
    return dsTRDetailKasirIGDList;
};

///---------------------------------------------------------------------------------------///



/* function getArrDetailTrIGD(){
	var x='';
	for(var i = 0 ; i < PenataJasaIGD.dsGridTindakan.getCount();i++){
		if (PenataJasaIGD.dsGridTindakan.data.items[i].data.KD_PRODUK != '' && PenataJasaIGD.dsGridTindakan.data.items[i].data.DESKRIPSI != ''){
			var y='';
			var z='@@##$$@@';
			y = 'URUT=' + PenataJasaIGD.dsGridTindakan.data.items[i].data.URUT;
			y += z + PenataJasaIGD.dsGridTindakan.data.items[i].data.KD_PRODUK;
			y += z + PenataJasaIGD.dsGridTindakan.data.items[i].data.QTY;
			y += z + ShowDate(PenataJasaIGD.dsGridTindakan.data.items[i].data.TGL_BERLAKU);
			y += z +PenataJasaIGD.dsGridTindakan.data.items[i].data.HARGA;
			y += z +PenataJasaIGD.dsGridTindakan.data.items[i].data.KD_TARIF;
			y += z +PenataJasaIGD.dsGridTindakan.data.items[i].data.URUT;
			
			if (i === (PenataJasaIGD.dsGridTindakan.getCount()-1)){
				x += y ;
			}else{
				x += y + '##[[]]##';
			}
		}
	}	
	return x;
} */

///---------------------------------------------------------------------------------------///
function getParamDetailTransaksiIGD() 
{
     var params={
		Table			: 'ViewTrKasirRWJ',
		TrKodeTranskasi	: Ext.get('txtNoTransaksiKasirIGD').getValue(),
		KdUnit			: Ext.get('txtKdUnitIGD').getValue(),
		kdDokter		: Ext.get('txtKdDokterIGD').getValue(),
		Tgl				: PenataJasaIGD.s1.data.TANGGAL_TRANSAKSI,
		Shift			: tampungshiftsekarang,
		List			: getArrDetailTrIGD(),
		JmlField		: mRecordIGD.prototype.fields.length-4,
		JmlList			: GetListCountDetailTransaksi_igd(),
		listTrDokter	: Ext.encode(dataTrDokter),
		Hapus			: 1,
		Ubah			: 0
	};
    params.jmlObat	= PenataJasaIGD.dsGridObat.getRange().length;
    params.urut_masuk	= PenataJasaIGD.s1.data.URUT_MASUK;
    params.kd_pasien	= PenataJasaIGD.s1.data.KD_PASIEN;
    for(var i=0, iLen= params.jmlObat ;i<iLen;i++){
    	var o=PenataJasaIGD.dsGridObat.getRange()[i].data;
    	params['kd_prd'+i]	= o.kd_prd;
    	params['jumlah'+i]	= o.jumlah;
    	params['cara_pakai'+i]	= o.cara_pakai;
    	params['verified'+i]	= o.verified;
    	params['racikan'+i]	= o.racikan;
    	params['kd_dokter'+i]	= o.kd_dokter;
		params['urut' + i]  = o.urut;
    }
    return params;
};

function getParamKonsultasi_igd() 
{

    var params =
	{
		
		Table:'ViewTrKasirIGD', //data access listnya belum dibuat
		
		TrKodeTranskasi: Ext.get('txtNoTransaksiKasirIGD').getValue(),
		KdUnitAsal : Ext.get('txtKdUnitIGD').getValue(),
		KdDokterAsal : Ext.get('txtKdDokterIGD').getValue(),
		KdUnit: selectKlinikPoli,
		KdDokter:selectDokter,
		KdPasien:Ext.get('txtNoMedrecDetransaksii_igd').getValue(),
		TglTransaksi : Ext.get('dtpTanggalDetransaksi_igd').dom.value,
		KDCustomer :vkode_customer_IGD,
	};
    return params
};

function GetListCountDetailTransaksi_igd()
{
	
	var x=0;
	for(var i = 0 ; i < dsTRDetailKasirIGDList.getCount();i++)
	{
		if (dsTRDetailKasirIGDList.data.items[i].data.KD_PRODUK != '' || dsTRDetailKasirIGDList.data.items[i].data.DESKRIPSI  != '')
		{
			x += 1;
		};
	}
	return x;
	
};


function getArrDetailTrIGD()
{
	var x='';
	for(var i = 0 ; i < dsTRDetailKasirIGDList.getCount();i++)
	{
		console.log(dsTRDetailKasirIGDList.data.items[i].data);
		if (dsTRDetailKasirIGDList.data.items[i].data.KD_PRODUK != '' && dsTRDetailKasirIGDList.data.items[i].data.DESKRIPSI != '')
		{
			var y='';
			var z='@@##$$@@';
			
			y = 'URUT=' + dsTRDetailKasirIGDList.data.items[i].data.URUT
			y += z + dsTRDetailKasirIGDList.data.items[i].data.KD_PRODUK
			y += z + dsTRDetailKasirIGDList.data.items[i].data.QTY
			y += z + ShowDate(dsTRDetailKasirIGDList.data.items[i].data.TGL_BERLAKU)
			y += z +dsTRDetailKasirIGDList.data.items[i].data.HARGA
			y += z +dsTRDetailKasirIGDList.data.items[i].data.KD_TARIF
			y += z +dsTRDetailKasirIGDList.data.items[i].data.URUT
			y += z + ShowDate(PenataJasaIGD.dsGridTindakan.data.items[i].data.TGL_TRANSAKSI);
			
			if (i === (dsTRDetailKasirIGDList.getCount()-1))
			{
				x += y 
			}
			else
			{
				x += y + '##[[]]##'
			};
		};
	}	
	
	return x;
};


function getItemPanelInputIGD(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 0px',
		border:true,
		height:149,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
			    layout: 'form',
			    border: false,
			    items:
				[
					getItemPanelNoTransksiIGD(lebar),getItemPanelmedrec_igd(lebar),getItemPanelUnit_igd(lebar) ,getItemPanelDokter_igd(lebar)			
				]
			}
		]
	};
    return items;
};



function getItemPanelUnit_igd(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel:'Unit  ',
					    name: 'txtKdUnitIGD',
					    id: 'txtKdUnitIGD',
						//emptyText:nmNomorOtomatis,
						readOnly:true,
					    anchor: '99%'
					}
				]
			},
			{
			    columnWidth: .60,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[
					{
						xtype: 'textfield',
					   // fieldLabel:'Unit : ',
					    name: 'txtNamaUnit_igd',
					    id: 'txtNamaUnit_igd',
						//emptyText:nmNomorOtomatis,
						readOnly:true,
					    anchor: '100%'
					}
				]
			}
		]
	}
    return items;
};

/* function getItemPanelDokter_igd(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel:'Dokter  ',
					    name: 'txtKdDokterIGD',
					    id: 'txtKdDokterIGD',
						//emptyText:nmNomorOtomatis,
						readOnly:true,
					    anchor: '99%'
					},{
					    xtype: 'textfield',
					    fieldLabel: 'Kelompok Pasien',
						//hideLabel:true,
						readOnly:true,
					    name: 'txtCustomer_igd',
					    id: 'txtCustomer_igd',
					    anchor: '99%',
						listeners: 
						{ 
							
						}
					}
				]
			},
			{
			    columnWidth: .600,
			    layout: 'form',
			    border: false,
				labelWidth:2,
			    items:
				[
					{
						xtype: 'textfield',
					   // fieldLabel:'Unit : ',
					    name: 'txtNamaDokter_igd',
					    id: 'txtNamaDokter_igd',
						//emptyText:nmNomorOtomatis,
						readOnly:true,
					    anchor: '100%'
					},
					{
						xtype: 'textfield',
					   // fieldLabel:'Unit : ',
					    name: 'txtKdUrutMasuk_igd',
					    id: 'txtKdUrutMasuk_igd',
						//emptyText:nmNomorOtomatis,
						readOnly:true,
						hidden:true,
					    anchor: '100%'
					}
				]
			}
		]
	}
    return items;
}; */

function getItemPanelNoTransksiIGD(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel:  'No. Transaksi ',
					    name: 'txtNoTransaksiKasirIGD',
					    id: 'txtNoTransaksiKasirIGD',
						emptyText:nmNomorOtomatis,
						readOnly:true,
					    anchor: '99%'
					}
				]
			},
			{
			    columnWidth: .60,
			    layout: 'form',
			    border: false,
				labelWidth:55,
			    items:
				[
					{
					    xtype: 'datefield',
					    fieldLabel: 'Tanggal ',
					    id: 'dtpTanggalDetransaksi_igd',
					    name: 'dtpTanggalDetransaksi_igd',
					    format: 'd/M/Y',
						readOnly : true,
					    value: now,
					    anchor: '100%'
					}
				]
			}
		]
	}
    return items;
};


/* function getItemPanelmedrec_igd(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel:   'No. Medrec',
					    name: 'txtNoMedrecDetransaksii_igd',
					    id: 'txtNoMedrecDetransaksii_igd',
						readOnly:true,
					    anchor: '99%',
					    listeners: 
						{ 
							
						}
					}
				]
			},
			{
			    columnWidth: .60,
			    layout: 'form',
			    border: false,
				labelWidth:2,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: '',
						//hideLabel:true,
						readOnly:true,
					    name: 'txtNamaPasienDetransaksi_igd',
					    id: 'txtNamaPasienDetransaksi_igd',
					    anchor: '100%',
						listeners: 
						{ 
							
						}
					}
				]
			}
		]
	}
    return items;
}; */


function RefreshDataKasirIGD() 
{
    dsTRKasirIGDList.load
    (
        {
            params:
            {
                Skip: 0,
                Take: selectCountKasirIGD,
                Sort: 'tgl_transaksi',
                //Sort: 'tgl_transaksi',
                Sortdir: 'ASC',
                target:'ViewTrKasirIGD',
                param : ''
            }		
        }
    );
	
    rowSelectedKasirIGD = undefined;
    return dsTRKasirIGDList;
};
function refeshkasirIGD()
{
dsTRKasirIGDList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountKasirIGD, 
					//Sort: 'no_transaksi',
                     Sort: '',
					//Sort: 'no_transaksi',
					Sortdir: 'ASC', 
					target:'ViewTrKasirIGD',
					param : ''
				}			
			}
		);   
		return dsTRKasirIGDList;
}

function RefreshDataFilterKasirIGD() 
{

	var KataKunci='';
	
	 if (Ext.get('txtFilterIGDNomedrec').getValue() != '')
    {
		if (KataKunci == '')
		{
                        KataKunci = ' and   LOWER(kd_pasien) like  LOWER( ~' + Ext.get('txtFilterIGDNomedrec').getValue() + '%~)';
			
		}
		else
		{
		
                        KataKunci += ' and  LOWER(kd_pasien) like  LOWER( ~' + Ext.get('txtFilterIGDNomedrec').getValue() + '%~)';
		};

	};
	
	 if (Ext.get('TxtIGDFilternama').getValue() != '')
    {
		if (KataKunci == '')
		{
                        KataKunci = ' and   LOWER(nama) like  LOWER( ~' + Ext.get('TxtIGDFilternama').getValue() + '%~)';
			
		}
		else
		{
		
                        KataKunci += ' and  LOWER(nama) like  LOWER( ~' + Ext.get('TxtIGDFilternama').getValue() + '%~)';
		};

	};
	
	
	 if (Ext.get('cboUNIT_viKasirIGD').getValue() != '' && Ext.get('cboUNIT_viKasirIGD').getValue() != 'All')
    {
		if (KataKunci == '')
		{
	
                        KataKunci = ' and  LOWER(nama_unit)like  LOWER(~%' + Ext.get('cboUNIT_viKasirIGD').getValue() + '%~)';
		}
		else
		{
	
                        KataKunci += ' and LOWER(nama_unit) like  LOWER(~%' + Ext.get('cboUNIT_viKasirIGD').getValue() + '%~)';
		};
	};
	if (Ext.get('TxtIGDFilterDokter').getValue() != '')
		{
		if (KataKunci == '')
		{
			
                        KataKunci = ' and  LOWER(nama_dokter) like  LOWER(~' + Ext.get('TxtIGDFilterDokter').getValue() + '%~)';
		}
		else
		{
		
                        KataKunci += ' and LOWER(nama_dokter) like  LOWER(~' + Ext.get('TxtIGDFilterDokter').getValue() + '%~)';
		};
	};
		
		
	if (Ext.get('cboStatus_viKasirIGD').getValue() == 'Posting')
	{
		if (KataKunci == '')
		{

                        KataKunci = ' and  posting_transaksi = TRUE';
		}
		else
		{
		
                        KataKunci += ' and posting_transaksi =  TRUE';
		};
	
	};
		
		
		
	if (Ext.get('cboStatus_viKasirIGD').getValue() == 'Belum Posting')
	{
		if (KataKunci == '')
		{
		
                        KataKunci = ' and  posting_transaksi = FALSE';
		}
		else
		{
	
                        KataKunci += ' and posting_transaksi =  FALSE';
		};
		
		
	};
	
	if (Ext.get('cboStatus_viKasirIGD').getValue() == 'Semua')
	{
		if (KataKunci == '')
		{
		
                        KataKunci = ' and  (posting_transaksi = FALSE OR posting_transaksi = TRUE )';
		}
		else
		{
	
                        KataKunci += ' and (posting_transaksi = FALSE OR posting_transaksi = TRUE )';
		};
		
		
	};
	
		
	if (Ext.get('dtpTglAwalFilterIGD').getValue() != '')
	{
		if (KataKunci == '')
		{                      
						KataKunci = " and (tgl_transaksi between ~" + Ext.get('dtpTglAwalFilterIGD').getValue() + "~ and ~" + Ext.get('dtpTglAkhirFilterIGD').getValue() + "~)";
		}
		else
		{
			
                        KataKunci += " and (tgl_transaksi between ~" + Ext.get('dtpTglAwalFilterIGD').getValue() + "~ and ~" + Ext.get('dtpTglAkhirFilterIGD').getValue() + "~)";
		};
	
	};
	
     
    if (KataKunci != undefined ||KataKunci != '' ) 
    {  
		dsTRKasirIGDList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountKasirIGD, 
					//Sort: 'no_transaksi',
                                        Sort: 'tgl_transaksi',
					//Sort: 'no_transaksi',
					Sortdir: 'ASC', 
					target:'ViewTrKasirIGD',
					param : KataKunci
				}			
			}
		);   
    }
	else
	{
	
	dsTRKasirIGDList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountKasirIGD, 
					//Sort: 'no_transaksi',
                                        Sort: 'tgl_transaksi',
					//Sort: 'no_transaksi',
					Sortdir: 'ASC', 
					target:'ViewTrKasirIGD',
					param : ''
				}			
			}
		);   
	};
    
	return dsTRKasirIGDList;
};


function Datasave_Konsultasi_igd(mBol) 
{	
	if (ValidasiEntryKonsultasi_igd(nmHeaderSimpanData,false) == 1 )
	{
		
			Ext.Ajax.request
			 (
				{
					//url: "./Datapool.mvc/CreateDataObj",
					url: baseURL + "index.php/main/functionIGD/KonsultasiPenataJasa",					
					params: getParamKonsultasi_igd(),
					failure: function(o)
					{
					ShowPesanWarningIGD('Konsultasi ulang gagal', 'Gagal');
					RefreshDataKasirIGDDetail(Ext.get('txtNoTransaksiKasirIGD').dom.value);
					},	
					success: function(o) 
					{
						RefreshDataKasirIGDDetail(Ext.get('txtNoTransaksiKasirIGD').dom.value);
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoIGD(nmPesanSimpanSukses,nmHeaderSimpanData);
							RefreshDataKasirIGD();
							if(mBol === false)
							{
								RefreshDataKasirIGDDetail(Ext.get('txtNoTransaksiKasirIGD').dom.value);
							};
						}
						else 
						{
								ShowPesanWarningIGD('Konsultasi ulang gagal', 'Gagal');
						
						};
					}
				}
			)
		
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
	
};



function Datasave_KasirIGD(mBol) 
{	
	if (ValidasiEntryCMIGD(nmHeaderSimpanData,false) == 1 ){
		Ext.Ajax.request({
			url			: baseURL + "index.php/main/functionIGD/savedetailpenyakit",
			params		: getParamDetailTransaksiIGD(),
			failure		: function(o){
				ShowPesanInfoIGD('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
				RefreshDataKasirIGDDetail(Ext.get('txtNoTransaksiKasirIGD').dom.value);
			},
			success		: function(o){
				RefreshDataKasirIGDDetail(Ext.get('txtNoTransaksiKasirIGD').dom.value);
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) {
					ShowPesanInfoIGD(nmPesanSimpanSukses,nmHeaderSimpanData);
					//RefreshDataFilterKasirIGD();
					if(mBol === false){
						RefreshDataKasirIGDDetail(Ext.get('txtNoTransaksiKasirIGD').dom.value);
					}
				}else{
					ShowPesanInfoIGD('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
				}
			}
		});
	}else{
		if(mBol === true){
			return false;
		}
	}
	
};

function ValidasiEntryCMIGD(modul,mBolHapus)
{
	var x = 1;
	
	if((Ext.get('txtNoTransaksiKasirIGD').getValue() == '') || (Ext.get('txtNoMedrecDetransaksii_igd').getValue() == '') || (Ext.get('txtNamaPasienDetransaksi_igd').getValue() == '') || (Ext.get('txtNamaDokter_igd').getValue() == '') || (Ext.get('dtpTanggalDetransaksi_igd').getValue() == '') || dsTRDetailKasirIGDList.getCount() === 0 || (Ext.get('txtKdDokterIGD').dom.value  === undefined ))
	{
		if (Ext.get('txtNoTransaksiKasirIGD').getValue() == '' && mBolHapus === true) 
		{
			x = 0;
		}
		else if (Ext.get('txtNoMedrecDetransaksii_igd').getValue() == '') 
		{
			ShowPesanWarningIGD(nmGetValidasiKosong('No. Medrec'), modul);
			x = 0;
		}
		else if (Ext.get('txtNamaPasienDetransaksi_igd').getValue() == '') 
		{
			ShowPesanWarningIGD(nmGetValidasiKosong(nmRequesterRequest), modul);
			x = 0;
		}
		else if (Ext.get('dtpTanggalDetransaksi_igd').getValue() == '') 
		{
			ShowPesanWarningIGD(nmGetValidasiKosong('Tanggal Kunjungan'), modul);
			x = 0;
		}
		else if (Ext.get('txtNamaDokter_igd').getValue() == '' || Ext.get('txtKdDokterIGD').dom.value  === undefined) 
		{
			ShowPesanWarningIGD(nmGetValidasiKosong(nmDeptRequest), modul);
			x = 0;
		}
		else if (dsTRDetailKasirIGDList.getCount() === 0) 
		{
			ShowPesanWarningIGD(nmGetValidasiKosong(nmTitleDetailFormRequest),modul);
			x = 0;
		};
	};
	return x;
};

function ValidasiEntryKonsultasi_igd(modul,mBolHapus)
{
	var x = 1;
	
	if((Ext.get('cboPoliklinikRequestEntry').getValue() == '') || (Ext.get('cboDokterRequestEntry_igd').getValue() == '') )
	{
		if (Ext.get('cboPoliklinikRequestEntry').getValue() == '' && mBolHapus === true) 
		{
			x = 0;
		}
		else if (Ext.get('cboDokterRequestEntry_igd').getValue() == '') 
		{
			ShowPesanWarningIGD(nmGetValidasiKosong('No. Medrec'), modul);
			x = 0;
		}
	};
	return x;
};


function ShowPesanWarningIGD(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorIGD(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoIGD(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};


function DataDeleteKasirIGD() 
{
   if (ValidasiEntryCMIGD(nmHeaderHapusData,true) == 1 )
    {
        Ext.Msg.show
        (
            {
               title:nmHeaderHapusData,
               msg: nmGetValidasiHapus(nmTitleFormRequest) ,
               buttons: Ext.MessageBox.YESNO,
               width:275,
               fn: function (btn)
               {
                    if (btn =='yes')
                    {
                        Ext.Ajax.request
                        (
                            {
                              
                                url: baseURL + "index.php/main/DeleteDataObj",
                                params: getParamDetailTransaksiIGD(),
                                success: function(o)
                                {
                                    var cst = Ext.decode(o.responseText);
                                    if (cst.success === true)
                                    {
                                        ShowPesanInfoIGD(nmPesanHapusSukses,nmHeaderHapusData);
                                        RefreshDataKasirIGD();
                                        IGDAddNew();
                                    }
                                    else if (cst.success === false && cst.pesan===0)
                                    {
                                        ShowPesanWarningIGD(nmPesanHapusGagal,nmHeaderHapusData);
                                    }
                                    else if (cst.success === false && cst.pesan===1)
                                    {
                                        ShowPesanWarningIGD(nmPesanHapusGagal + ' , ',nmHeaderHapusData);
                                    }
                                    else
                                    {
                                        ShowPesanErrorIGD(nmPesanHapusError,nmHeaderHapusData);
                                    };
                                }
                            }
                        )
                    };
                }
            }
        )
    };
};


function GantiDokterLookUp_igd_igd(mod_id) 
{
   
   

    var FormDepanDokter_igd = new Ext.Panel
    (
        {
            id: mod_id,
            closable: true,
            region: 'center',
            layout: 'form',
            title: 'Ganti Dokter',
            border: false,
            shadhow: true,
            autoScroll:false,
            iconCls: 'Request',
            margins: '0 5 5 0',
            items: [DokterLookUp_igd()],
            listeners:
            {
                'afterrender': function()
                {}
            }
        }
    );
	

	
   return FormDepanDokter_igd

};









function DokterLookUp_igd(rowdata) 
{
    var lebar = 350;
    FormLookUpGantidokter_IGD = new Ext.Window
    (
        {
            id: 'gridDokter',
            title: 'Ganti Dokter',
            closeAction: 'destroy',
            width: lebar,
            height: 180,
            border: false,
            resizable: false,
            plain: true,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryDokter_igd(lebar),
            listeners:
            {
                activate: function()
                {
                     if (varBtnOkLookupEmp === true)
                    {
                        Ext.get('txtKdDokterIGD').dom.value   = rowSelectedLookdokter.data.KD_DOKTER;
                        Ext.get('txtNamaDokter_igd').dom.value = rowSelectedLookdokter.data.NAMA_DOKTER;
                        varBtnOkLookupEmp=false;
                    };
                },
                afterShow: function()
                {
                    this.activate();
                },
                deactivate: function()
                {
                    rowSelectedKasirDokter=undefined;
               
                }
            }
        }
    );

    FormLookUpGantidokter_IGD.show();
 

};
function getItemPanelButtonGantidokter_igd(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
		height:39,
		anchor:'100%',
		style:{'margin-top':'-1px'},
	    items:
		[
			{
				layout: 'hBox',
				width:310,
				border: false,
				bodyStyle: 'padding:5px 0px 5px 5px',
				defaults: { margins: '3 3 3 3' },
				anchor: '90%',
				layoutConfig: 
				{
					align: 'middle',
					pack:'end'
				},
				items:
				[
					{
						xtype:'button',
						text:'Simpan',
						width:100,
						style:{'margin-left':'0px','margin-top':'0px'},
						hideLabel:true,
						id: 'btnOkGantiDokter',
						handler:function()
						{
						saveGantiDokter_igd(false);
						FormLookUpGantidokter_IGD.close();	
						}
					},
					{
							xtype:'button',
							text:'Tutup' ,
							width:70,
							hideLabel:true,
							id: 'btnCancelGantidokter',
							handler:function() 
							{
								FormLookUpGantidokter_IGD.close();
							}
					}
				]
			}
		]
	}
    return items;
};
function getFormEntryDokter_igd(lebar) 
{
    var pnlTRGantiDokter_igd = new Ext.FormPanel
    (
        {
            id: 'PanelTRDokter',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:190,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [getItemPanelInputGantidokter_igd(lebar),getItemPanelButtonGantidokter_igd(lebar)],
           tbar:
            [
               
               
            ]
        }
    );


   
    var FormDepanDokter_igd = new Ext.Panel
	(
		{
		    id: 'FormDepanDokter_igd',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [pnlTRGantiDokter_igd
				

			]

		}
	);

    return FormDepanDokter_igd
};


function getItemPanelInputGantidokter_igd(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 0px',
		border:false,
		height:95,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
			    layout: 'form',
			    border: false,
			    items:
				[
					getItemPanelNoTransksiDokter_igd(lebar)		
				]
			}
		]
	};
    return items;
};



function getItemPanelNoTransksiDokter_igd(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: 1.0,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel:  'Unit Asal ',
					    name: 'cmbUnitAsal_igd',
					    id: 'cmbUnitAsal_igd',
						value:Ext.get('txtNamaUnit_igd').getValue(),
						readOnly:true,
					    anchor: '100%'
					},
					
					{
					    xtype: 'textfield',
					    fieldLabel: 'Dokter Asal ',
					    name: 'cmbDokterAsal_igd',
					    id: 'cmbDokterAsal_igd',
						value:Ext.get('txtNamaDokter_igd').getValue(),
						readOnly:true,
					    anchor: '100%'
						
					},
			
					mComboDokterGantiEntry_igd()
				
					
				]
				
			
			}
			
			
		]
	}
    return items;
};


function mComboDokterGantiEntry_igd()
{ 
	
	
    
 var Field = ['KD_DOKTER','NAMA'];

    dsDokterGanti_igd = new WebApp.DataStore({fields: Field});
	var kDUnit = Ext.get('txtKdUnitIGD').getValue();
	var kddokter = Ext.get('txtKdDokterIGD').getValue();
    dsDokterGanti_igd.load
                (
                    {
                     params:
							{
								Skip: 0,
								Take: 1000,
								Sort: 'nama',
								Sortdir: 'ASC',
								target: 'ViewComboDokter',
								param: 'where dk.kd_unit=~'+ kDUnit+ '~ and d.kd_dokter not in (~'+kddokter+'~)'
							}
                    }
                )

    var cboDokterGantiEntry_igd = new Ext.form.ComboBox
	(
		{
		    id: 'cboDokterRequestEntry_igd',
		    typeAhead: true,
		    triggerAction: 'all',
			name:'txtdokter',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
            forceSelection: true,
		    emptyText:'Pilih Dokter...',
		    fieldLabel: 'Dokter Baru',
		    align: 'Right',
			store: dsDokterGanti_igd,
		    valueField: 'KD_DOKTER',
		    displayField: 'NAMA',
			anchor:'100%',
		    listeners:
			{
			    'select': function(a,b,c)
				{

									selectDokter = b.data.KD_DOKTER;
									NamaDokter = b.data.NAMA;
									 Ext.get('txtKdDokterIGD').dom.value = b.data.KD_DOKTER
                                },
                            'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) 
                                    Ext.getCmp('kelPasien_IGD').focus();
                                    }, c);
                                }
			}
          }
	);
	
    return cboDokterGantiEntry_igd;

};

function saveGantiDokter_igd(mBol)
{
    if (ValidasiGantiDokter_igd(nmHeaderSimpanData,false) == 1 )
    {
            
                    Ext.Ajax.request
                     (
                            {
                                   url: WebAppUrl.UrlUpdateData,
                                    params: getParamGantiDokter_igd(),
									failure: function(o)
										{
										 ShowPesanErrorIGD('Ganti Dokter Gagal','Ganti Dokter');
										},	
                                    success: function(o)
                                    {
                                            var cst = Ext.decode(o.responseText);
                                            if (cst.success === true)
                                            {
                                                    ShowPesanInfoIGD(nmPesanSimpanSukses,'Ganti Dokter');
													Ext.get('txtKdDokterIGD').dom.value = selectDokter;
													Ext.get('txtNamaDokter_igd').dom.value = NamaDokter;
													//FormDepanDokter_igd.close();
                                                    FormLookUpGantidokter_IGD.close();
                                            }
                                            else if  (cst.success === false && cst.pesan===0)
                                            {
                                                    ShowPesanErrorIGD('Ganti Dokter Gagal','Ganti Dokter');
                                            }
                                            else
                                            {
                                                    ShowPesanErrorIGD('Ganti Dokter Gagal','Ganti Dokter');
                                            };
                                    }
                            }
                    )
            
    }
    else
    {
            if(mBol === true)
            {
                    return false;
            };
    };

};

function ValidasiGantiDokter_igd(modul,mBolHapus)
{
	var x = 1;
	if ((Ext.get('cboDokterRequestEntry_igd').getValue() == ''))
	{
	  if (Ext.get('cboDokterRequestEntry_igd').getValue() === '')
		{
			x=0;
			if ( mBolHapus === false )
			{
				ShowPesanWarningIGD(nmGetValidasiKosong(nmSatuan),modul);
			};

		};
	};


	return x;
};


function getParamGantiDokter_igd()
{
    var params =
	{
        Table: 'ViewGantiDokter',
		TxtMedRec : Ext.get('txtNoMedrecDetransaksii_igd').getValue(),
		TxtTanggal:Ext.get('dtpTanggalDetransaksi_igd').getValue(),
		KdUnit :  Ext.get('txtKdUnitIGD').getValue(),
		KdDokter : selectDokter,
		kodebagian : 2
		
	};
    return params
};



function KelompokPasienLookUp_igd(rowdata) 
{
    var lebar = 440;
    FormLookUpsdetailTRKelompokPasien_igd = new Ext.Window
    (
        {
            id: 'gridKelompokPasien',
            title: 'Ganti Kelompok Pasien',
            closeAction: 'destroy',
            width: lebar,
            height: 260,
            border: false,
            resizable: false,
            plain: false,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryTRKelompokPasien_igd(lebar),
            listeners:
            {
                
            }
        }
    );

    FormLookUpsdetailTRKelompokPasien_igd.show();
    KelompokPasienbaru_igd();

};


function getFormEntryTRKelompokPasien_igd(lebar) 
{
    var pnlTRKelompokPasien_igd = new Ext.FormPanel
    (
        {
            id: 'PanelTRKelompokPasien_igd',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:250,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [getItemPanelInputKelompokPasien_igd(lebar),getItemPanelButtonKelompokPasien_igd(lebar)],
           tbar:
            [
               
               
            ]
        }
    );
 
    var FormDepanKelompokPasien = new Ext.Panel
	(
		{
		    id: 'FormDepanKelompokPasien',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [pnlTRKelompokPasien_igd	
				
			]

		}
	);

    return FormDepanKelompokPasien
};

function getItemPanelInputKelompokPasien_igd(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 0px',
		border:false,
		height:170,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
			    layout: 'form',
			    border: false,
			    items:
				[
					getKelompokpasienlama_igd(lebar),	getItemPanelNoTransksiKelompokPasien_igd(lebar)	,
					
				]
			}
		]
	};
    return items;
};




function getItemPanelButtonKelompokPasien_igd(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
		height:30,
		anchor:'100%',
		style:{'margin-top':'-1px'},
	    items:
		[
			{
				layout: 'hBox',
				width:400,
				border: false,
				bodyStyle: 'padding:5px 0px 5px 5px',
				defaults: { margins: '3 3 3 3' },
				anchor: '90%',
				layoutConfig: 
				{
					align: 'middle',
					pack:'end'
				},
				items:
				[
					{
						xtype:'button',
						text:'Simpan',
						width:70,
						style:{'margin-left':'0px','margin-top':'0px'},
						hideLabel:true,
						id:Nci.getid(),
						handler:function()
						{
					
					Datasave_Kelompokpasien_igd();
					FormLookUpsdetailTRKelompokPasien_igd.close();
							
						}
					},
					{
							xtype:'button',
							text:'Tutup',
							width:70,
							hideLabel:true,
							id:Nci.getid(),
							handler:function() 
							{
								FormLookUpsdetailTRKelompokPasien_igd.close();
							}
					}
				]
			}
		]
	}
    return items;
};

function KelompokPasienbaru_igd() 
{
	jeniscus_IGD=0;
    KelompokPasienAddNew_IGD = true;
    Ext.getCmp('cboKelompokpasien_IGD').show()
	Ext.getCmp('txtNoSJP').disable();
	Ext.getCmp('txtNoAskes').disable();
	RefreshDatacombo_igd(jeniscus_IGD);
	Ext.get('txtCustomer_igdLama').dom.value=	Ext.get('txtCustomer_igd').dom.value

	

};

function RefreshDatacombo_igd(jeniscus_IGD) 
{

    ds_customer_viPJ_IGD.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: 'jenis_cust=~'+ jeniscus_IGD +'~ and kontraktor.kd_customer not in(~'+ vkode_customer_IGD+'~)'
            }
        }
    )
	
    return ds_customer_viPJ_IGD;
};
function mComboKelompokpasien_igd()
{

var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viPJ_IGD = new WebApp.DataStore({fields: Field_poli_viDaftar});
	
	ds_customer_viPJ_IGD.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: 'jenis_cust=~'+ jeniscus_IGD +'~'
            }
        }
    )
    var cboKelompokpasien_IGD = new Ext.form.ComboBox
	(
		{
			id:'cboKelompokpasien_IGD',
			typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Pilih...',
                        fieldLabel: labelisi_IGD,
                        align: 'Right',
                        anchor: '95%',
			store: ds_customer_viPJ_IGD,
			valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetKelompokpasien=b.data.displayText ;
					selectKdCustomer=b.data.KD_CUSTOMER;
					selectNamaCustomer=b.data.CUSTOMER;
				
				}
			}
		}
	);
	return cboKelompokpasien_IGD;
};

function getKelompokpasienlama_igd(lebar) 
{
    var items =
	{
		Width:lebar,
		height:40,
	    layout: 'column',
	    border: false,
		
	    items:
		[
			{
			    columnWidth: .990,
			    layout: 'form',
				Width:lebar-10,
				labelWidth:130,
			    border: true,
			    items:
				[{	 
						xtype: 'tbspacer',
						height: 2
						},
						{
							xtype: 'textfield',
							fieldLabel: 'Kelompok Pasien Asal',
							name: 'txtCustomer_igdLama',
							id: 'txtCustomer_igdLama',
							labelWidth:130,
							width: 100,
							anchor: '95%'
						 }
					]
			}
			
		]
	}
    return items;
};


function getItemPanelNoTransksiKelompokPasien_igd(lebar) 
{
    var items =
	{
		Width:lebar,
		height:120,
	    layout: 'column',
	    border: false,
		
		
	    items:
		[
			{

			    columnWidth: .990,
			    layout: 'form',
				Width:lebar-10,
				labelWidth:130,
			    border: true,
			    items:
				[{	 
						xtype: 'tbspacer',
						height:3
					},{ 
					    xtype: 'combo',
						fieldLabel: 'Kelompok Pasien Baru',
						id: 'kelPasien_IGD',
						editable: false,
						store: new Ext.data.ArrayStore
							(
								{
								id: 0,
								fields:
								[
								'Id',
								'displayText'
								],
								   data: [[1, 'Perseorangan'], [2, 'Perusahaan'], [3, 'Asuransi']]
								}
							),
							  displayField: 'displayText',
							  mode: 'local',
							  width: 100,
							  forceSelection: true,
							  triggerAction: 'all',
							  emptyText: 'Pilih Salah Satu...',
							  selectOnFocus: true,
							  anchor: '95%',
							  listeners:
								 {
										'select': function(a, b, c)
									{
									if(b.data.displayText =='Perseorangan')
									{jeniscus_IGD='0'
									Ext.getCmp('txtNoSJP').disable();
									Ext.getCmp('txtNoAskes').disable();}
									else if(b.data.displayText =='Perusahaan')
									{jeniscus_IGD='1';
									Ext.getCmp('txtNoSJP').disable();
									Ext.getCmp('txtNoAskes').disable();}
									else if(b.data.displayText =='Asuransi')
									{jeniscus_IGD='2';
									Ext.getCmp('txtNoSJP').enable();
									Ext.getCmp('txtNoAskes').enable();
								}
									
									RefreshDatacombo_igd(jeniscus_IGD);
									}

								}
						},{
							columnWidth: .990,
							layout: 'form',
							border: false,
							labelWidth:130,
							items:
							[
												mComboKelompokpasien_igd()
							]
						},{
							xtype: 'textfield',
							fieldLabel: 'No. SJP',
							maxLength: 200,
							name: 'txtNoSJP',
							id: 'txtNoSJP',
							width: 100,
							anchor: '95%'
						 }, {
							xtype: 'textfield',
							fieldLabel: 'No. Askes',
							maxLength: 200,
							name: 'txtNoAskes',
							id: 'txtNoAskes',
							width: 100,
							anchor: '95%'
						 }
									
				]
			}
			
		]
	}
    return items;
};

function Datasave_Kelompokpasien_igd(mBol) 
{	
	if (ValidasiEntryUpdateKelompokPasien_IGD(nmHeaderSimpanData,false) == 1 )
	{
		
		
			Ext.Ajax.request
			 (
				{
					
					url: baseURL +  "index.php/main/functionIGD/UpdateKdCustomer",	
					params: getParamKelompokpasien_IGD(),
					failure: function(o)
					{
					ShowPesanWarningIGD('Simpan kelompok pasien gagal', 'Gagal');
					RefreshDataKasirIGDDetail(Ext.get('txtNoTransaksiKasirIGD').dom.value);
					},	
					success: function(o) 
					{
						RefreshDataKasirIGDDetail(Ext.get('txtNoTransaksiKasirIGD').dom.value);
						Ext.get('txtCustomer_igd').dom.value = selectNamaCustomer;
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoIGD(nmPesanSimpanSukses,nmHeaderSimpanData);
							RefreshDataKasirIGD();
							if(mBol === false)
							{
								RefreshDataKasirIGDDetail(Ext.get('txtNoTransaksiKasirIGD').dom.value);
							};
						}

						else 
						{
								ShowPesanWarningIGD('Simpan kelompok pasien gagal', 'Gagal');
						};
					}
				}
			)
		
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
	
};


function getParamKelompokpasien_IGD() 
{
	
    var params =
	{
		
		Table:'ViewTrKasirIGD', 
		TrKodePasien : Ext.get('txtNoMedrecDetransaksii_igd').getValue(),
		TrKodeTranskasi: Ext.get('txtNoTransaksiKasirIGD').getValue(),
		KdUnit: Ext.get('txtKdUnitIGD').getValue(),
		KdDokter:Ext.get('txtKdDokterIGD').dom.value ,
		TglTransaksi : Ext.get('dtpTanggalDetransaksi_igd').dom.value,
		KDCustomer:selectKdCustomer,
		TglTransaksi : Ext.get('dtpTanggalDetransaksi_igd').dom.value,
		KDNoSJP :Ext.get('txtNoSJP').dom.value,
		KDNoAskes :Ext.get('txtNoAskes').dom.value
		
		
	};
    return params
};

function ValidasiEntryUpdateKelompokPasien_IGD(modul,mBolHapus)
{
	var x = 1;
	
	if((Ext.get('kelPasien_IGD').getValue() == '') || (Ext.get('kelPasien_IGD').dom.value  === undefined ))
	{
		if (Ext.get('kelPasien_IGD').getValue() == '' && mBolHapus === true) 
		{
			ShowPesanWarningIGD(nmGetValidasiKosong('Kelompok Pasien'), modul);
			x = 0;
		}
	};
	return x;
};



function setpostingtransaksi_igd(notransaksi) 
{

		Ext.Msg.show
		(
			{
			   title:'Posting',
			   msg: 'Kirim Data Transaksi ini Ke Kasir ? ' ,
			   buttons: Ext.MessageBox.YESNO,
			   width:250,
			   fn: function (btn) 
			   {			
					if (btn === 'yes') 
					{
						Ext.Ajax.request
						(
							{
								
								url : baseURL + "index.php/main/posting",
								params: 
								{
								_notransaksi : 	notransaksi,
								},
								success: function(o) 
								{
									var cst = Ext.decode(o.responseText);
									if (cst.success === true) 
									{
									
										RefreshDataFilterKasirIGD();
										ShowPesanInfoDiagnosa_IGD('Posting Berhasil Dilakukan','Posting');
										
										FormLookUpsdetailTRIGD.close();
										
										
									}
									else if (cst.success === false && cst.pesan===0)
									{
										ShowPesanWarningDiagnosa_igd(nmPesanHapusGagal,'Posting');
									}
									else 
									{
										ShowPesanWarningDiagnosa_igd(nmPesanHapusError,'Posting');
									};
								}
							}
						)
					};
				}
			}
		)
	};
	



function setdisablebutton_PJ_igd()
{
	//Ext.getCmp('btnLookUpKonsultasi_viKasirIGD').disable();
	Ext.getCmp('btnLookUpGantiDokter_viKasirIGD').disable();
	Ext.getCmp('btngantipasien_igd').disable();
	Ext.getCmp('btnposting_pj_Igd').disable();	
	Ext.getCmp('btnLookupIGD_igd').disable();
	Ext.getCmp('btnSimpanIGD_igd').disable();
	Ext.getCmp('btnHpsBrsIGD_igd').disable();
	Ext.getCmp('btnHpsBrsDiagnosa_igd').disable();
	Ext.getCmp('btnSimpanDiagnosa_igd').disable();
	Ext.getCmp('btnLookupDiagnosa_igd').disable();
}

function setenablebutton_PJ_igd()
{
	//Ext.getCmp('btnLookUpKonsultasi_viKasirIGD').enable();
	Ext.getCmp('btnLookUpGantiDokter_viKasirIGD').enable();
	Ext.getCmp('btngantipasien_igd').enable();
	Ext.getCmp('btnposting_pj_Igd').enable();	
	
	Ext.getCmp('btnLookupIGD_igd').enable();
	Ext.getCmp('btnSimpanIGD_igd').enable();
	Ext.getCmp('btnHpsBrsIGD_igd').enable();
	Ext.getCmp('btnHpsBrsDiagnosa_igd').enable();
	Ext.getCmp('btnSimpanDiagnosa_igd').enable();
	Ext.getCmp('btnLookupDiagnosa_igd').enable();	
}


function getItemPanelUnit_igd(lebar) {
    var items ={
	    layout: 'column',
	    border: false,
	    items:[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:[
					{
					    xtype: 'textfield',
					    fieldLabel:'Unit  ',
					    name: 'txtKdUnitIGD',
					    id: 'txtKdUnitIGD',
						readOnly:true,
					    anchor: '99%'
					}
				]
			},{
			    columnWidth: .60,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:[
					{
						xtype: 'textfield',
					    name: 'txtNamaUnit_igd',
					    id: 'txtNamaUnit_igd',
						readOnly:true,
					    anchor: '100%'
					}
				]
			}
		]
	};
    return items;
}

function getItemPanelDokter_igd(lebar) {
    var items ={
	    layout: 'column',
	    border: false,
	    items:[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:[
					{
					    xtype: 'textfield',
					    fieldLabel:'Dokter  ',
					    name: 'txtKdDokterIGD',
					    id: 'txtKdDokterIGD',
						readOnly:true,
					    anchor: '99%'
					},{
					    xtype: 'textfield',
					    fieldLabel: 'Kelompok Pasien',
						readOnly:true,
					    name: 'txtCustomer_igd',
					    id: 'txtCustomer_igd',
					    anchor: '99%',
						listeners:{ 
							
						}
					}
				]
			},{
			    columnWidth: .600,
			    layout: 'form',
			    border: false,
				labelWidth:2,
			    items:[
					{
						xtype: 'textfield',
					    name: 'txtNamaDokter_igd',
					    id: 'txtNamaDokter_igd',
						readOnly:true,
					    anchor: '100%'
					},{
						xtype: 'textfield',
					    name: 'txtKdUrutMasuk_igd',
					    id: 'txtKdUrutMasuk_igd',
						readOnly:true,
						hidden:true,
					    anchor: '100%'
					}
				]
			}
		]
	};
    return items;
}

function getItemPanelNoTransksiIGD(lebar) {
    var items ={
	    layout: 'column',
	    border: false,
	    items:[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:[
					{
					    xtype: 'textfield',
					    fieldLabel:  'No. Transaksi ',
					    name: 'txtNoTransaksiKasirIGD',
					    id: 'txtNoTransaksiKasirIGD',
						emptyText:nmNomorOtomatis,
						readOnly:true,
					    anchor: '99%'
					}
				]
			},{
			    columnWidth: .60,
			    layout: 'form',
			    border: false,
				labelWidth:55,
			    items:[
					{
					    xtype: 'datefield',
					    fieldLabel: 'Tanggal ',
					    id: 'dtpTanggalDetransaksi_igd',
					    name: 'dtpTanggalDetransaksi_igd',
					    format: 'd/M/Y',
						readOnly : true,
					    value: now,
					    anchor: '100%'
					}
				]
			}
		]
	};
    return items;
}




function getItemPanelmedrec_igd(lebar) {
    var items ={
	    layout: 'column',
	    border: false,
	    items:[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:[
					{
					    xtype: 'textfield',
					    fieldLabel:   'No. Medrec',
					    name: 'txtNoMedrecDetransaksii_igd',
					    id: 'txtNoMedrecDetransaksii_igd',
						readOnly:true,
					    anchor: '99%',
					    listeners: 
						{ 
							
						}
					}
				]
			},{
			    columnWidth: .60,
			    layout: 'form',
			    border: false,
				labelWidth:2,
			    items:[
					{
					    xtype: 'textfield',
					    fieldLabel: '',
						readOnly:true,
					    name: 'txtNamaPasienDetransaksi_igd',
					    id: 'txtNamaPasienDetransaksi_igd',
					    anchor: '100%',
						listeners: 
						{ 
							
						}
					}
				]
			}
		]
	};
    return items;
};

function GetDTLTRIGDGrid(data){
	var tabTransaksi = new Ext.Panel({
		title: 'Transaksi',
		id:'tabTransaksi',
        fileUpload: true,
        region: 'north',
        layout: 'column',
        height:100,
        anchor: '100%',
        width: 815,
        border: false,
        items: [GetDTLTRIGDGridSecond(data),GetDTLTRIGDGridFirst(data)]
    });
	return tabTransaksi;
}

function GetDTLTRIGDGridFirst(data){
	var fldDetail = ['kd_prd','nama_obat','jumlah','satuan','cara_pakai','kd_dokter','nama','verified','racikan','jml_stok_apt','urut','order_mng'];
	dsPjTrans2_IGD = new WebApp.DataStore({ fields: fldDetail });
    RefreshDataKasirIGDDetail2(data) ;
    var gridDTLTRIGD = new Ext.grid.EditorGridPanel({
        title: 'Terapi Obat',
		id:'PjTransGrid1',
        stripeRows: true,
        height: 170,
        store: dsPjTrans2_IGD,
        border: false,
        frame: false,
        width:815,
        anchor: '100%',
        autoScroll:true,
        sm: new Ext.grid.CellSelectionModel({
            singleSelect: true,
            listeners:{
                cellselect: function(sm, row, rec){
                    cellSelecteddeskripsi_IGD = dsTRDetailKasirIGDList .getAt(row);
                    CurrentKasirIGD.row = row;
                    CurrentKasirIGD.data = cellSelecteddeskripsi_IGD;
                }
            }
        }),
        cm: TRGawatdaruratColumModel_obt(),
        viewConfig:{forceFit: true},
        tbar:[
			{
				text: 'Tambah Obat',
				id: 'BtnTambahObatTrKasirIGD',
				iconCls: 'find',
				handler: function(){
					PenataJasaIGD.dsGridObat.insert(PenataJasaIGD.dsGridObat.getCount(),PenataJasaIGD.nullGridObat());
				}
			},{
				text: 'Hapus',
				id: 'BtnHapusObatTrKasirIGD',
				iconCls: 'RemoveRow',
				handler: function(){
					Ext.Msg.show({
	                   title:nmHapusBaris,
	                   msg: 'Anda yakin akan menghapus data Obat ini?',
	                   buttons: Ext.MessageBox.YESNO,
	                   fn: function (btn)
	                   {
	                       if (btn =='yes')
	                        {
	                    	   PenataJasaIGD.dsGridObat.removeAt(PenataJasaIGD.gridObat.getSelectionModel().selection.cell[0]);
								PenataJasaIGD.gridObat.getView().refresh();
	                        }
	                   },
	                   icon: Ext.MessageBox.QUESTION
	                });
				}
			}
        ]}
    );
    PenataJasaIGD.gridObat=gridDTLTRIGD;
    PenataJasaIGD.dsGridObat=dsPjTrans2_IGD;
    return gridDTLTRIGD;
}

function GetDTLTRIGDGridSecond(){
	var fldDetail	= ['KD_PRODUK','DESKRIPSI','QTY','DOKTER','TGL_TINDAKAN','QTY','DESC_REQ','TGL_BERLAKU','NO_TRANSAKSI','URUT','DESC_STATUS','TGL_TRANSAKSI','KD_TARIF','HARGA','JUMLAH_DOKTER','JUMLAH'];
	dsTRDetailKasirIGDList	= new WebApp.DataStore({ fields: fldDetail });
    RefreshDataKasirIGDDetail () ;
    PenataJasaIGD.dsGridTindakan	= dsTRDetailKasirIGDList;
    PenataJasaIGD.form.Grid.produk	= new Ext.grid.EditorGridPanel({
        title		: 'Transaksi Yang Diberikan',
		id			: 'PjTransGrid2',
		stripeRows	: true,
		height		: 130,
        store		: PenataJasaIGD.dsGridTindakan,
        border		: false,
        frame		: false,
        anchor		: '100% 100%',
        autoScroll	: true,
        sm			: new Ext.grid.CellSelectionModel({
	        singleSelect: true,
	        listeners	: {
	            cellselect	: function(sm, row, rec){
	                cellSelecteddeskripsi_IGD	= dsTRDetailKasirIGDList .getAt(row);
	                CurrentKasirIGD.row	= row;
	                CurrentKasirIGD.data	= cellSelecteddeskripsi_IGD;
	            }
	        },
			
        }),
        cm			: TRGawatdaruratColumModel2(),
        viewConfig	: {forceFit: true},
		listeners	: {
			rowclick: function( $this, rowIndex, e )
			{
        	cellCurrentTindakan_IGD = rowIndex;
    		},
			celldblclick: function(gridView,htmlElement,columnIndex,dataRecord){
			console.log(PenataJasaIGD.dsGridTindakan.data.items[cellCurrentTindakan_IGD].data);	
			var NoTrans =  Ext.getCmp('txtNoTransaksiKasirIGD').getValue();
			var Urt =  PenataJasaIGD.dsGridTindakan.data.items[cellCurrentTindakan_IGD].data.URUT;
			var TglTrans =  Ext.get('dtpTanggalDetransaksi_igd').dom.value;
			var kdPrdk = PenataJasaIGD.dsGridTindakan.data.items[cellCurrentTindakan_IGD].data.KD_PRODUK;
			var kdTrf = PenataJasaIGD.dsGridTindakan.data.items[cellCurrentTindakan_IGD].data.KD_TARIF;
			var tglBerlaku = PenataJasaIGD.dsGridTindakan.data.items[cellCurrentTindakan_IGD].data.TGL_BERLAKU;
			var trf = PenataJasaIGD.dsGridTindakan.data.items[cellCurrentTindakan_IGD].data.HARGA;
			if(columnIndex == 8 && PenataJasaIGD.dsGridTindakan.data.items[cellCurrentTindakan_IGD].data.JUMLAH == 'Ada'){
				PilihDokterLookUp_igd(NoTrans,Urt,TglTrans,kdPrdk,kdTrf,tglBerlaku,trf);
        	}
			else if(columnIndex == 8 && PenataJasaIGD.dsGridTindakan.data.items[cellCurrentTindakan_IGD].data.JUMLAH == null)
			{
				ShowPesanInfoDiagnosa_IGD('Maaf Produk ini tidak mempunyai jasa Dokter', 'Informasi');
			}
   		  }
		}
    });
    
    return PenataJasaIGD.form.Grid.produk;
};

function TRGawatdaruratColumModel_obt(){
    return new Ext.grid.ColumnModel([
             new Ext.grid.RowNumberer(),
        {
			id			: Nci.getId(),
        	header		: 'KD.Obat',
            dataIndex	: 'kd_prd',
            width		: 80,
			menuDisabled: true,
            hidden		: false
        },{
			id			: Nci.getId(),
        	header		: 'Nama Obat',
            dataIndex	: 'nama_obat',
            width		: 150,
			menuDisabled: true,
            hidden		: false,
            editor		: PenataJasaIGD.comboObat()
        },{
            id			: Nci.getId(),
            header		: 'Qty',
            dataIndex	: 'jumlah',
            sortable	: false,
            hidden		: false,
			menuDisabled: true,
            width		: 50,
            editor		: new Ext.form.NumberField({
				id				: 'txtQty',
				selectOnFocus	: true,
				width			: 50,
				anchor			: '100%',
				listeners       :{
								blur: function(a){
									var line	= this.index;
							       	if(a.getValue()==0){
								ShowPesanWarningIGD('Qty obat belum di isi', 'Warning');
								}else{
								hasilJumlah(a.getValue());
								}
									
								},
								focus: function(a){
								
									this.index=PenataJasaIGD.dsGridObat.getSelectionModel().selection.cell[0]
								}
						
					}
			})
        },{
            id			: Nci.getId(),
            header		: 'Satuan',
			hidden		: false,
			menuDisabled: true,
            dataIndex	: 'satuan'	
        },{
            id			: Nci.getId(),
            header		: 'Cara Pakai',
			hidden		: false,
			menuDisabled: true,
            dataIndex	: 'cara_pakai'	,
        	editor		: new Ext.form.TextField({
				id				: 'txtcarapakai',
				selectOnFocus	: true,
				width			: 50,
				anchor			: '100%'
			})
        },{
            id			: Nci.getId(),
            header		: 'stok',
            dataIndex	: 'jml_stok_apt',
            sortable	: false,
            hidden		: true,
			menuDisabled: true,
            width		: 50
           
        },{
            id			: Nci.getId(),
            header		: 'Dokter',
			hidden		: false,
			menuDisabled: true,
            dataIndex	: 'nama',
			hidden		: true
        },{
            id			: Nci.getId(),
            header		: 'Verified',
			hidden		: true,
			menuDisabled: true,
            dataIndex	: 'verified',
            editor		: PenataJasaIGD.ComboVerifiedObat()
        },{
            id			: Nci.getId(),
            header		: 'Racikan',
			hidden		: true,
			menuDisabled: true,
            dataIndex	: 'racikan',
            editor		: new Ext.form.NumberField({
				id				: 'txtRacikan',
				selectOnFocus	: true,
				width			: 50,
				anchor			: '100%'
			})
        },{
            id			: Nci.getId(),
            header		: 'Order',
			hidden		: false,
			menuDisabled: true,
            dataIndex	: 'order_mng',
			renderer	: function(v, params, record)
							{
								if  (record.data.order_mng==='Dilayani')
								{
									Ext.getCmp('BtnTambahObatTrKasirIGD').disable();
									Ext.getCmp('BtnHapusObatTrKasirIGD').disable();
								}
								else
								{
									Ext.getCmp('BtnTambahObatTrKasirIGD').enable();
									Ext.getCmp('BtnHapusObatTrKasirIGD').enable();
								}
								
								return record.data.order_mng;
							}
        }
    ]);
}


function hasilJumlah(qty){
	
	
	for(var i=0; i<dsPjTrans2_IGD.getCount() ; i++){


		var o=dsPjTrans2_IGD.getRange()[i].data;
		console.log(o);
		if(qty != undefined){
			if(o.jumlah <= o.jml_stok_apt){
			
			} else{
				o.jumlah=o.jml_stok_apt;
				PenataJasaIGD.gridObat.getView().refresh();
				ShowPesanWarningIGD('Jumlah obat melebihi stok yang tersedia','Warning');
				
			}
			
		}
		

	}
	
}

function TRGawatdaruratColumModel2(){
    return new Ext.grid.ColumnModel([
             new Ext.grid.RowNumberer(),
        {
        	 id				: 'coleskripsiIGD2',
        	 header			: 'Uraian',
        	 dataIndex		: 'DESKRIPSI2',
        	 menuDisabled	: true,
        	 hidden 		: true
        },{
        	id				: 'colKdProduk2',
            header			: 'Kode Produk',
            dataIndex		: 'KD_PRODUK',
            width			: 100,
			menuDisabled	: true,
            hidden			: true
        },{
        	id			: 'colDeskripsiIGD2',
            header		:'Item Transaksi',
            dataIndex	: 'DESKRIPSI',
            sortable	: false,
            hidden		:false,
			menuDisabled:true,
            width		:200,
            editor		: PenataJasaIGD.form.ComboBox.produk= Nci.form.Combobox.autoComplete({
				store	: PenataJasaIGD.form.DataStore.produk,
				select	: function(a,b,c){
					console.log(b);
					var line	= PenataJasaIGD.form.Grid.produk.getSelectionModel().selection.cell[0];
    				PenataJasaIGD.dsGridTindakan.getRange()[line].data.KD_PRODUK=b.data.kd_produk;
    				PenataJasaIGD.dsGridTindakan.getRange()[line].data.DESKRIPSI=b.data.deskripsi;
    				PenataJasaIGD.dsGridTindakan.getRange()[line].data.KD_TARIF='TU';
    				PenataJasaIGD.dsGridTindakan.getRange()[line].data.HARGA=b.data.harga;
    				PenataJasaIGD.dsGridTindakan.getRange()[line].data.TGL_BERLAKU=b.data.tglberlaku;
					PenataJasaIGD.dsGridTindakan.getRange()[line].data.JUMLAH=b.data.jumlah;
					PenataJasaIGD.dsGridTindakan.getRange()[line].data.TGL_TINDAKAN=b.data.tgl_tindakan;
    				PenataJasaIGD.form.Grid.produk.getView().refresh();
				},
				param	: function(){
					var o=PenataJasaIGD.grid1.getSelectionModel().getSelections()[0].data;
					var params={};
					params['kd_unit']=o.KD_UNIT;
					params['kd_customer']=o.KD_CUSTOMER;
					return params;
				},
				insert	: function(o){
					return {
						kd_produk        :o.kd_produk,
						deskripsi 		: o.deskripsi,
						harga			: o.tarifx,
						tglberlaku		: o.tglberlaku,
						jumlah			: o.jumlah,
						text				:  '<table style="font-size: 11px;"><tr><td width="50">'+o.kd_produk+'</td><td width="160" align="left">'+o.deskripsi+'</td><td width="130">'+o.klasifikasi+'</td></tr></table>'
				    }
				},
				url		: baseURL + "index.php/main/functionRWJ/getProduk",
				valueField: 'deskripsi',
				displayField: 'text'
			})
	        },{
               header: 'Tanggal Transaksi',
               dataIndex: 'TGL_TRANSAKSI',
               width:100,
		   		menuDisabled:true,
               renderer: function(v, params, record){
                        
                        return ShowDate(record.data.TGL_TRANSAKSI);
                    }
            },{
                id: 'colHARGAIGD2',
                header: 'Harga',
				align: 'right',
				hidden: true,
				menuDisabled:true,
                dataIndex: 'HARGA',
				renderer: function(v, params, record){
					return formatCurrency(record.data.HARGA);
				}	
            },{
                id: 'colProblemIGD2',
                header: 'Qty',
               width:'100%',
				align: 'right',
				menuDisabled:true,
                dataIndex: 'QTY',
				width:50,
                editor: new Ext.form.TextField({
                    id:'fieldcolProblemIGD2',
                    allowBlank: true,
                    enableKeyEvents : true,
                    width:50,
					listeners:{ 
						'specialkey' : function(){
							Dataupdate_KasirIGD(false);
						}
					}
                })
            },
			{
                id: 'colDokterPoli',
                header: 'Nama Dokter',
				align: 'left',
				hidden: true,
				menuDisabled:true,
                dataIndex: 'DOKTER',
            },
			{
                id: 'coljumlahDOkter',
                header: 'Dokter',
				align: 'left',
				hidden: true,
				width:50,
                dataIndex: 'JUMLAH_DOKTER',
            },
			{
                id: 'colImpactIGD2',
                header: 'CR',
                dataIndex: 'IMPACT',
				hidden: true,
                editor: new Ext.form.TextField({
                    id:'fieldcolImpactIGD2',
                    allowBlank: true,
                    enableKeyEvents : true,
                    width:30
                })
            }
        ]
    );
};


function GetDTLTRDiagnosaGrid_igd(){
	var pnlTRDiagnosa = new Ext.Panel({
		title		: 'Diagnosa',
		id			: 'tabDiagnosa',
        fileUpload	: true,
        region		: 'north',
        layout		: 'column',
        height		: 100,
        anchor		: '100%',
        width		: 815,
        border		: false,
        items		: [GetDTLTRDiagnosaGrid_igdFirst(),FieldKeteranganDiagnosa_igd()]
    });
	return pnlTRDiagnosa;
}

function FieldKeteranganDiagnosa_igd(){
	dsCmbIGDPJDiag_IGD = new Ext.data.ArrayStore({
		id: 0,
		fields:[
			'Id',
			'displayText'
		],
		data: []
	});
    var items ={
	    layout: 'column',
	    border: true,
	    width: 815,
	    bodyStyle:'margin-top:5px;padding: 5px;',
	    items:[
			{
			    layout: 'form',
			    border: true,
				labelWidth:150,
				labelAlign:'right',
			    border: false,
				items:[
					combo = new Ext.form.ComboBox({
						id:'cmbIGDPJDiag',
						typeAhead: true,
						triggerAction: 'all',
						lazyRender:true,
						editable: false,
						mode: 'local',
						width: 150,
						emptyText:'',
						fieldLabel: 'Kode Penyakit &nbsp;',
						store: dsCmbIGDPJDiag_IGD,
						valueField: 'Id',
						displayField: 'displayText',
						listeners:{
							select: function(){
								if(this.getValue() != ''){
									Ext.getCmp('catLainGroup_igd').show();
									for(var j=0,jLen=dsTRDetailDiagnosaList_IGD.getRange().length; j< jLen; j++){
										if(dsTRDetailDiagnosaList_IGD.getRange()[j].data.KD_PENYAKIT==Ext.getCmp('cmbIGDPJDiag').getValue()){
											if(dsTRDetailDiagnosaList_IGD.getRange()[j].data.NOTE==2){
												Ext.getCmp('txtkecelakaan_igd').setValue(dsTRDetailDiagnosaList_IGD.getRange()[j].data.DETAIL);
												Ext.getCmp('txtkecelakaan_igd').show();
												Ext.getCmp('txtneoplasma_igd').hide();
												Ext.get('cbxkecelakaan').dom.checked=true;
											}else if(dsTRDetailDiagnosaList_IGD.getRange()[j].data.NOTE==1){
												Ext.getCmp('txtneoplasma_igd').setValue(dsTRDetailDiagnosaList_IGD.getRange()[j].data.DETAIL);
												Ext.getCmp('txtneoplasma_igd').show();
												Ext.getCmp('txtkecelakaan_igd').hide();
												Ext.get('cbxneoplasma').dom.checked=true;
											}else{
												Ext.get('cbxlain').dom.checked=true;
												Ext.getCmp('txtkecelakaan_igd').hide();
												Ext.getCmp('txtneoplasma_igd').hide();
											}
										}
									}
								}
									
							}
						}
					}),
					{
						xtype: 'radiogroup',
						width:300,
						fieldLabel: 'Catatan Lain &nbsp;',
						id:'catLainGroup_igd',
						name: 'mycbxgrp',
						columns: 3,
						items: [
							{ 
								id: 'cbxlain', 
								boxLabel: 'Lain-lain', 
								name: 'mycbxgrp', 
								width:70, 
								inputValue: 1
							},{ 
								id: 'cbxneoplasma', 
								boxLabel: 'Neoplasma', 
								name: 'mycbxgrp',  
								width:100, 
								inputValue: 2 
							},{ 
								id: 'cbxkecelakaan', 
								boxLabel: 'Kecelakaan', 
								name: 'mycbxgrp', 
								width:100, 
								inputValue: 3 
							}
					   ],
					     listeners: {
		             		change: function(radiogroup, radio){
		             			if(Ext.getDom('cbxlain').checked == true){
									Ext.getCmp('txtneoplasma_igd').hide();
									Ext.getCmp('txtkecelakaan_igd').hide();
									for(var j=0,jLen=dsTRDetailDiagnosaList_IGD.getRange().length; j< jLen; j++){
										
										if(dsTRDetailDiagnosaList_IGD.getRange()[j].data.KD_PENYAKIT==Ext.getCmp('cmbIGDPJDiag').getValue()){
											dsTRDetailDiagnosaList_IGD.getRange()[j].data.DETAIL='';
											dsTRDetailDiagnosaList_IGD.getRange()[j].data.NOTE=0;
											Ext.getCmp('tabDiagnosaGrid').getView().refresh();
											break;
										}
									}
								}else if(Ext.getDom('cbxneoplasma').checked == true){
									Ext.getCmp('txtneoplasma_igd').show();
									Ext.getCmp('txtneoplasma_igd').setValue('');
									Ext.getCmp('txtkecelakaan_igd').hide();
									for(var j=0,jLen=dsTRDetailDiagnosaList_IGD.getRange().length; j< jLen; j++){
										if(dsTRDetailDiagnosaList_IGD.getRange()[j].data.KD_PENYAKIT==Ext.getCmp('cmbIGDPJDiag').getValue()){
											Ext.getCmp('txtneoplasma_igd').setValue(dsTRDetailDiagnosaList_IGD.getRange()[j].data.DETAIL);
											dsTRDetailDiagnosaList_IGD.getRange()[j].data.NOTE=1;
											Ext.getCmp('tabDiagnosaGrid').getView().refresh();
											break;
										}
									}
								}else if(Ext.getDom('cbxkecelakaan').checked == true){
									Ext.getCmp('txtneoplasma_igd').hide();
									Ext.getCmp('txtkecelakaan_igd').show();
									Ext.getCmp('txtkecelakaan_igd').setValue('');
									for(var j=0,jLen=dsTRDetailDiagnosaList_IGD.getRange().length; j< jLen; j++){
										if(dsTRDetailDiagnosaList_IGD.getRange()[j].data.KD_PENYAKIT==Ext.getCmp('cmbIGDPJDiag').getValue()){
											Ext.getCmp('txtkecelakaan_igd').setValue(dsTRDetailDiagnosaList_IGD.getRange()[j].data.DETAIL);
											dsTRDetailDiagnosaList_IGD.getRange()[j].data.NOTE=2;
											Ext.getCmp('tabDiagnosaGrid').getView().refresh();
											break;
										}
									}
								}
							}
		                }
						},{
							  xtype: 'textfield',
							  fieldLabel:'Neoplasma &nbsp;',
							  name: 'txtneoplasma_igd',
							  id: 'txtneoplasma_igd',
							  hidden:true,
							  width:600,
							  listeners:{
								  blur: function(){
									  if(Ext.getCmp('cmbIGDPJDiag').getValue() != ''){
										  	for(var j=0,jLen=dsTRDetailDiagnosaList_IGD.getRange().length; j< jLen; j++){
												if(dsTRDetailDiagnosaList_IGD.getRange()[j].data.KD_PENYAKIT==Ext.getCmp('cmbIGDPJDiag').getValue()){
													dsTRDetailDiagnosaList_IGD.getRange()[j].data.DETAIL=this.getValue();
													dsTRDetailDiagnosaList_IGD.getRange()[j].data.NOTE=1;
													Ext.getCmp('tabDiagnosaGrid').getView().refresh();
													Ext.getCmp('tabDiagnosaGrid').getView().refresh();
												}
											}
									  }
									  	
								  }
							  }
					       },{
					        	xtype: 'textfield',
					        	fieldLabel:'Kecelakaan / Keracunan &nbsp;',
					        	name: 'txtkecelakaan_igd',
					        	id: 'txtkecelakaan_igd',
					        	hidden:true,
					        	width:600,
						    	listeners:{
									  blur: function(){
										  if(Ext.getCmp('cmbIGDPJDiag').getValue() != ''){
											  	for(var j=0,jLen=dsTRDetailDiagnosaList_IGD.getRange().length; j< jLen; j++){
													if(dsTRDetailDiagnosaList_IGD.getRange()[j].data.KD_PENYAKIT==Ext.getCmp('cmbIGDPJDiag').getValue()){
														dsTRDetailDiagnosaList_IGD.getRange()[j].data.DETAIL=this.getValue();
														dsTRDetailDiagnosaList_IGD.getRange()[j].data.NOTE=2;
														Ext.getCmp('tabDiagnosaGrid').getView().refresh();
													}
												}
										  }
										  	
									  }
								  }
					        }
				]
			}
		]
	};
	return items;
}

PenataJasaIGD.form.Class.diagnosa	= Ext.data.Record.create([
   {name: 'KD_PENYAKIT', 	mapping: 'KD_PENYAKIT'},
   {name: 'PENYAKIT', 	mapping: 'PENYAKIT'},
   {name: 'KD_PASIEN', 	mapping: 'KD_PASIEN'},
   {name: 'URUT', 	mapping: 'URUT'},
   {name: 'URUT_MASUK', 	mapping: 'URUT_MASUK'},
   {name: 'TGL_MASUK', 	mapping: 'TGL_MASUK'},
   {name: 'KASUS', 	mapping: 'KASUS'},
   {name: 'STAT_DIAG', 	mapping: 'STAT_DIAG'},
   {name: 'NOTE', 	mapping: 'NOTE'}
]);

function GetDTLTRDiagnosaGrid_igdFirst(){
    var fldDetail = ['KD_PENYAKIT','PENYAKIT','KD_PASIEN','URUT','URUT_MASUK','TGL_MASUK','KASUS','STAT_DIAG','NOTE','DETAIL'];
    dsTRDetailDiagnosaList_IGD = new WebApp.DataStore({ fields: fldDetail });
    PenataJasaIGD.ds2=dsTRDetailDiagnosaList_IGD;
    RefreshDataSetDiagnosa_igd(PenataJasaIGD.s1.data.KD_PASIEN,PenataJasaIGD.s1.data.KD_UNIT,PenataJasaIGD.s1.data.TANGGAL_TRANSAKSI);
    PenataJasaIGD.grid2 = new Ext.grid.EditorGridPanel({
        stripeRows: true,
		id:'tabDiagnosaGrid',
        store: PenataJasaIGD.ds2,
        border: true,
        columnLines: true,
        frame: false,
        anchor: '100% 100%',
        autoScroll:true,
		height:150,
        sm: new Ext.grid.CellSelectionModel({
                singleSelect: true,
                listeners:{
                    cellselect: function(sm, row, rec){
                        cellSelecteddeskripsi_IGD = dsTRDetailDiagnosaList_IGD.getAt(row);
                        CurrentDiagnosa_IGD.row = row;
                        CurrentDiagnosa_IGD.data = cellSelecteddeskripsi_IGD;
                    }
                }
            }
        ),
        cm: TRDiagnosaColumModel_igd(),
		viewConfig:{forceFit: true}
    });
    return PenataJasaIGD.grid2;
}

function TRDiagnosaColumModel_igd(){
    return new Ext.grid.ColumnModel([
             new Ext.grid.RowNumberer(),
        {
            id			: Nci.getId(),
            header		: 'No.ICD',
            dataIndex	: 'KD_PENYAKIT',
            width		: 70,
			menuDisabled: true,
            hidden		: false
        },{
            id			: Nci.getId(),
            header		: 'Penyakit',
            dataIndex	: 'PENYAKIT',
			menuDisabled: true,
			width		: 200,
			editor		: PenataJasaIGD.form.ComboBox.penyakit= Nci.form.Combobox.autoComplete({
				store	: PenataJasaIGD.form.DataStore.penyakit,
				select	: function(a,b,c){
					var line	= PenataJasaIGD.grid2.getSelectionModel().selection.cell[0];
    				PenataJasaIGD.ds2.getRange()[line].data.KD_PENYAKIT=b.data.kd_penyakit;
    				PenataJasaIGD.ds2.getRange()[line].data.PENYAKIT=b.data.penyakit;
    				PenataJasaIGD.grid2.getView().refresh();
    				dsCmbIGDPJDiag_IGD.loadData([],false);
					for(var i=0,iLen=PenataJasaIGD.ds2.getCount(); i<iLen; i++){
						var recs    = [],
						recType = dsCmbIGDPJDiag_IGD.recordType;
						var o=PenataJasaIGD.ds2.getRange()[i].data;
						recs.push(new recType({
							Id        :o.KD_PENYAKIT,
							displayText : o.KD_PENYAKIT
					    }));
						dsCmbIGDPJDiag_IGD.add(recs);
					}
				},
				insert	: function(o){
					return {
						kd_penyakit        	:o.kd_penyakit,
						penyakit 			: o.penyakit,
						text				:  '<table style="font-size: 11px;"><tr><td width="50">'+o.kd_penyakit+'</td><td width="200">'+o.penyakit+'</td></tr></table>'
					}
				},
				url		: baseURL + "index.php/main/functionRWJ/getPenyakit",
				valueField: 'penyakit',
				displayField: 'text',
				listWidth: 250
			})
        },{
            id: Nci.getId(),
            header: 'kd_pasien',
            dataIndex: 'KD_PASIEN',
			hidden:true
        },{
            id: Nci.getId(),
            header: 'urut',
            dataIndex: 'URUT',
			hidden:true
        },{
            id: Nci.getId(),
            header: 'urut masuk',
            dataIndex: 'URUT_MASUK',
			hidden:true
            
        },{
            id: Nci.getId(),
            header: 'tgl masuk',
            dataIndex: 'TGL_MASUK',
			hidden:true
        },{
            id			: Nci.getId(),
            header		: 'Diagnosa',
            width		: 130,
			menuDisabled: true,
            dataIndex	: 'STAT_DIAG',
            editor		: new Ext.form.ComboBox ( {
				id				: Nci.getId(),
				typeAhead		: true,
				triggerAction	: 'all',
				lazyRender		: true,
				mode			: 'local',
				selectOnFocus	: true,
				forceSelection	: true,
				emptyText		: 'Silahkan Pilih...',
				width			: 50,
				anchor			: '95%',
				value			: 1,
				store			: new Ext.data.ArrayStore({
					id		: 0,
					fields	:['Id','displayText'],
					data	: [[1, 'Diagnosa Awal'],[2, 'Diagnosa Utama'],[3, 'Komplikasi'],[4, 'Diagnosa Sekunder']]
				}),
				valueField	: 'displayText',
				displayField: 'displayText',
				value		: '',
				listeners	: {}
			})
        },{
            id: 'colKasusDiagnosa',
            header: 'Kasus',
            width:130,
			menuDisabled:true,
            dataIndex: 'KASUS',
            editor: new Ext.form.ComboBox({
				id				: 'cboKasus',
				typeAhead		: true,
				triggerAction	: 'all',
				lazyRender		: true,
				mode			: 'local',
				selectOnFocus	: true,
				forceSelection	: true,
				emptyText		: 'Silahkan Pilih...',
				width			: 50,
				anchor			: '95%',
				value			: 1,
				store			: new Ext.data.ArrayStore({
					id		: 0,
					fields	: ['Id','displayText'],
					data	: [[1, 'Baru'],[2, 'Lama']]
				}),
				valueField	: 'displayText',
				displayField: 'displayText',
				value		: '',
				listeners	: {}
			})
        },{
            id			: 'colNote',
            header		: 'Note',
            dataIndex	: 'NOTE',
            width		: 70,
			menuDisabled: true,
            hidden		: true
        },{
            id			: 'colKdProduk',
            header		: 'Detail',
            dataIndex	: 'DETAIL',
            width		: 70,
			menuDisabled: true,
            hidden		: true
        }
    ]);
}

PenataJasaIGD.getLabolatorium=function(data){
	var $this=this;
	var tabTransaksi = new Ext.Panel({
		title: 'Konsultasi / Rujukan Laboratorium',
		id:'tabLaboratorium',
        fileUpload: true,
        region: 'north',
        layout: 'column',
        height:100,
        anchor: '100%',
        width: 815,
        border: false,
        items: [$this.getGrid3(data),GetDTGridHasilLab_PJIGD()]
    });
	return tabTransaksi;
};

function GetDTLTRRadiologiGrid(data){
	var tabTransaksi = new Ext.Panel({
		title: 'Konsultasi / Rujukan Radiologi',
		id:'tabradiologi',
        fileUpload: true,
        region: 'north',
        layout: 'column',
        height:100,
        anchor: '100%',
        width: 815,
        border: false,
       items: [GetGridIGDPJRad(data),PenataJasaIGD.gridrad(data)]
    });

	return tabTransaksi;
};

function RefreshDataKasirIGDDetail2(data){
    dsPjTrans2_IGD.load({
	    params:{
		    Skip: 0,
		    Take: 1000,
		    Sort: 'kd_obat',
		    Sortdir: 'ASC',
		    target: 'ViewResepRWJ',
		    param: "KD_PASIEN='"+data.KD_PASIEN+"' AND KD_UNIT = '"+data.KD_UNIT+"' AND TGL_MASUK = '"+data.TANGGAL_TRANSAKSI+"'"
		}
	});
    return dsPjTrans2_IGD;
}

function mComboStatusBayar_viKasirIGD(){
	var cboStatus_viKasirIGD = new Ext.form.ComboBox({
		id				: 'cboStatus_viKasirIGD',
		typeAhead		: true,
		triggerAction	: 'all',
		lazyRender		: true,
		mode			: 'local',
		anchor 			: '70%',
		emptyText		: '',
		fieldLabel		: 'Status Posting',
		store			: new Ext.data.ArrayStore({
			id: 0,
			fields:['Id','displayText'],
			data: [[1, 'Semua'],[2, 'Posting'], [3, 'Belum Posting']]
		}),
		valueField		: 'Id',
		displayField	: 'displayText',
		value			: selectCountStatusByr_viKasirIGD,
		listeners		:{
			select: function(a,b,c){
				selectCountStatusByr_viKasirIGD=b.data.displayText ;
				RefreshDataFilterKasirIGD();
			}
		}
	});
	return cboStatus_viKasirIGD;
};



PenataJasaIGD.getGrid3=function(data){
	var $this=this;
	PenataJasaIGD.ds3 = new WebApp.DataStore({ fields: ['kd_produk','deskripsi','kd_tarif','harga','qty','desc_req','tgl_berlaku','no_transaksi','urut','desc_status','tgl_transaksi','jumlah','namadok'] });
	PenataJasaIGD.grid3 = new Ext.grid.EditorGridPanel({
        title: 'Laboratorium',
		id:'grid3',
        stripeRows: true,
        height: 130,
		width:815,
        store: PenataJasaIGD.ds3,
        border: true,
        frame: true,
        anchor: '100%',
        autoScroll:true,
		   sm: new Ext.grid.CellSelectionModel
			(
				{
					singleSelect: true,
					listeners:
					{
						cellselect: function(sm, row, rec)
						{
							var rowSelectedPJKasir = PenataJasaIGD.ds3.getAt(row);
							
							if(rowSelectedPJKasir.data.kd_pasien===undefined||rowSelectedPJKasir.data.kd_pasien==="")
							{}else{
							ViewGridDetailHasilLab_igd(rowSelectedPJKasir.data.kd_pasien,rowSelectedPJKasir.data.tglkun,rowSelectedPJKasir.data.urutkun);
							}
						}
					}
				}
			),
        cm: $this.getModel1(),
        viewConfig:{forceFit: true}
    });
    return PenataJasaIGD.grid3;
};

function RefreshDataSetRadiologi_igd(){	
	var strKriteriaRadiologi='';
	strKriteriaRadiologi = 'kd_pasien = ~' + Ext.get('txtNoMedrecDetransaksii_igd').getValue() + '~ and kd_unit=~'+Ext.get('txtKdUnitIGD').getValue()+'~ and tgl_masuk = ~'+Ext.get('dtpTanggalDetransaksi_igd').dom.value+'~';
	dsIGDPJLab_IGD.load({ 
		params	: { 
			Skip	: 0, 
			Take	: 50, 
            Sort	: 'id_konsul',
			Sortdir	: 'ASC', 
			target	: 'CrudpoliRad',
			param	: strKriteriaRadiologi
		} 
	});
	return dsIGDPJLab_IGD;
}


function ViewGridDetailHasilLab_igd(kd_pasien,tgl_masuk,urut_masuk) 
{
    var strKriteriaHasilLab='';
    strKriteriaHasilLab = "LAB_hasil.Kd_Pasien = '" + kd_pasien + "' And LAB_hasil.Tgl_Masuk = '" + tgl_masuk + "'  and LAB_hasil.Urut_Masuk ="+ urut_masuk +"  and LAB_hasil.kd_unit= '41'";
   
    PenataJasaIGD.dshasilLabIGD.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'tgl_transaksi',
			    //Sort: 'tgl_transaksi',
			    Sortdir: 'ASC',
			    target: 'ViewGridHasilLab',
			    param: strKriteriaHasilLab
			}
		}
	);
    return PenataJasaIGD.dshasilLabIGD;
};


function GetDTGridHasilLab_PJIGD() 
{
	var fm = Ext.form;
    var fldDetailHasilLab = ['KLASIFIKASI', 'DESKRIPSI', 'KD_LAB', 'KD_TEST', 'ITEM_TEST', 'SATUAN', 'NORMAL', 'NORMAL_W',  'NORMAL_A', 'NORMAL_B', 'COUNTABLE', 'MAX_M', 'MIN_M', 'MAX_F', 'MIN_F', 'MAX_A', 'MIN_A', 'MAX_B', 'MIN_B', 'KD_METODE', 'HASIL', 'KET','KD_UNIT_ASAL','NAMA_UNIT_ASAL','URUT','METODE'];
	
    PenataJasaIGD.dshasilLabIGD = new WebApp.DataStore({ fields: fldDetailHasilLab })
    PenataJasaIGD.gridhasil_lab_PJIGD = new Ext.grid.EditorGridPanel
    (
        {
            title: 'Detail Hasil Lab',
			height: 160,
			width:815,
            stripeRows: true,
            store: PenataJasaIGD.dshasilLabIGD,
			frame: false,
            border: true,
            columnLines: true,
            autoScroll:true,
            cm: new Ext.grid.ColumnModel
            (
			[
				{	id: Nci.getId(),
					header: 'Kode Tes',
					dataIndex: 'KD_TEST',
					width:80,
					menuDisabled:true,
					hidden:true
				},
				{	id: Nci.getId(),
					header:'Pemeriksaan',
					dataIndex: 'ITEM_TEST',
					sortable: false,
					hidden:false,
					menuDisabled:true,
					width:200
					
				},
				{	id: Nci.getId(),
					header:'Metode',
					dataIndex: 'METODE',
					sortable: false,
					align: 'center',
					hidden:false,
					menuDisabled:true,
					width:100
					
				},
				{	id: Nci.getId(),
					header:'Hasil',
					dataIndex: 'HASIL',
					sortable: false,
					hidden:false,
					menuDisabled:true,
					width:100,
					align: 'right',
					
					
				},
				{	id: Nci.getId(),
					header:'Normal',
					dataIndex: 'NORMAL',
					sortable: false,
					hidden:false,
					align: 'center',
					menuDisabled:true,
					width:100
					
				},
				{
					header:'Satuan',
					dataIndex: 'SATUAN',
					sortable: false,
					hidden:false,
					menuDisabled:true,
					width:100
					
				},
				{
					header:'Keterangan',
					dataIndex: 'KET',
					width:250,
					
					
				},
				{
					header:'Kode Lab',
					dataIndex: 'KD_LAB',
					width:250,
					hidden:true
					
				}

			]
			),
			viewConfig:{forceFit: true}
        }
		
		
    );
	
	

    return PenataJasaIGD.gridhasil_lab_PJIGD;
};

function GetGridIGDPJRad(data){
	
	var fldDetail = ['ID_RADKONSUL','KD_PRODUK','KLASIFIKASI','KD_KLAS','DESKRIPSI','KD_DOKTER','cito'];
	dsIGDPJLab_IGD = new WebApp.DataStore({ fields: fldDetail });
    PenataJasaIGD.pj_req_rad = new Ext.grid.EditorGridPanel({
        title: 'Radiologi',
		id:'gridIGDPJRad',
        stripeRows: true,
		height: 130,
        store: dsIGDPJLab_IGD,
        border: true,
        frame: true,
        width:815,
        anchor: '100%',
        autoScroll:true,
		 sm: new Ext.grid.CellSelectionModel({
            singleSelect: true,
            listeners:{
                cellselect: function(sm, row, rec){
                        var rowSelectedPJKasir_rad = dsIGDPJLab_IGD.getAt(row);
                    	if(rowSelectedPJKasir_rad.data.kd_pasien===undefined||rowSelectedPJKasir_rad.data.kd_pasien==="")
							{}else{
								pj_req_radhasil_IGD(rowSelectedPJKasir_rad.data.kd_pasien,
								rowSelectedPJKasir_rad.data.kdunitkun,
								rowSelectedPJKasir_rad.data.tglkun,
								rowSelectedPJKasir_rad.data.urutkun,
								rowSelectedPJKasir_rad.data.kd_produk,
								rowSelectedPJKasir_rad.data.urut);
							}
				
                }
          }
        }),
        cm: getModelIGDPJRad(),
        viewConfig:{forceFit: true}
    });
    
    return PenataJasaIGD.pj_req_rad;
}
function getproduk_PJIGD()
{

var str='LOWER(tarif.kd_tarif)=LOWER(~'+PenataJasaIGD.varkd_tarif+'~) and tarif.kd_unit= ~5~ ' 

dsLookProdukList_rad_IGD.load
	(
            {
                params:
                    {
                        Skip: 0,
                        Take: 1000,
                        Sort: 'tgl_transaksi',
                        Sortdir: 'ASC',
                        target: 'LookupProduk',
                        param: str
                    }
            }
	);
 return dsLookProdukList_rad_IGD;
}

function dokter_rad_IGD()
{
    dsLook_dokter_rad_igd.load
	(
            {
                params:
                    {
                        Skip: 0,
                        Take: 1000,

                        Sort: 'kd_dokter',
                        Sortdir: 'ASC',
                        target: 'ViewDokterPenunjang',
                        param: "kd_unit = '5'"
                    }
            }
	);
 return dsLook_dokter_rad_igd;
}

function getModelIGDPJRad(){
var flddokterradio= ['KD_DOKTER','NAMA'];
dsLook_dokter_rad_igd = new WebApp.DataStore({ fields: flddokterradio })
PenataJasaIGD.form.ComboBox.dok_rad= new Ext.form.ComboBox({
        		id				: Nci.getId(),
        		typeAhead		: true,
        	    triggerAction	: 'all',
        	    lazyRender		: true,
        	    mode			: 'local',
        	    emptyText		: '',
        		store			: dsLook_dokter_rad_igd,
        		valueField		: 'NAMA',
        		hideTrigger		: true,
        		displayField	: 'NAMA',
        		value			: '',
        		listeners		: {
        			select	: function(a, b, c){
					
						var line = PenataJasaIGD.pj_req_rad.getSelectionModel().selection.cell[0];
						
						if(dsIGDPJLab_IGD.data.items[line].data.no_transaksi==="" || dsIGDPJLab_IGD.data.items[line].data.no_transaksi===undefined)
						{
						dsIGDPJLab_IGD.data.items[line].data.namadok=b.data.NAMA;
						PenataJasaIGD.var_kd_dokter_rad=b.data.KD_DOKTER;
						PenataJasaIGD.pj_req_rad.getView().refresh();
						}else{
						ViewGridBawahpoliRad(Ext.getCmp('txtNoMedrecDetransaksii_igd').getValue());
						ShowPesanWarningIGD('dokter tidak bisa di ganti karena data sudah tersimpan ', 'Warning');
						}
        		    }
        		}
        	});
			
 var fldDetail = ['TARIF','KLASIFIKASI','PERENT','TGL_BERAKHIR','KD_KAT','KD_TARIF','KD_KLAS','DESKRIPSI','YEARS','NAMA_UNIT','KD_PRODUK','TGL_BERLAKU','CHEK','JUMLAH'];
//var str='LOWER(tarif.kd_tarif)=LOWER(~TU~) and tarif.kd_unit= ~5~ ' 
dsLookProdukList_rad_IGD = new WebApp.DataStore({ fields: fldDetail })

PenataJasaIGD.form.ComboBox.produk_rab= new Ext.form.ComboBox({
        		id				: Nci.getId(),
        		typeAhead		: true,
        	    triggerAction	: 'all',
        	    lazyRender		: true,
        	    mode			: 'local',
        	    emptyText		: '',
        		store			: dsLookProdukList_rad_IGD,
        		valueField		: 'DESKRIPSI',
        		hideTrigger		: true,
        		displayField	: 'DESKRIPSI',
        		value			: '',
        		listeners		: {
        			select	: function(a, b, c){
					
        				var line= PenataJasaIGD.pj_req_rad .getSelectionModel().selection.cell[0];
						
						dsIGDPJLab_IGD.data.items[line].data.deskripsi2=b.data.DESKRIPSI;
        				dsIGDPJLab_IGD.data.items[line].data.kd_tarif=b.data.KD_TARIF;
        				dsIGDPJLab_IGD.data.items[line].data.deskripsi=b.data.DESKRIPSI;
        				dsIGDPJLab_IGD.data.items[line].data.tgl_berlaku=b.data.TGL_BERLAKU;
						dsIGDPJLab_IGD.data.items[line].data.qty=1;
						dsIGDPJLab_IGD.data.items[line].data.kd_produk=b.data.KD_PRODUK;
						dsIGDPJLab_IGD.data.items[line].data.harga=b.data.TARIF;
						dsIGDPJLab_IGD.data.items[line].data.jumlah=b.data.JUMLAH;
						dsIGDPJLab_IGD.data.items[line].data.no_transaksi="";
						PenataJasaIGD.pj_req_rad .getView().refresh();
						//Ext.getCmp('btnSimpanPenJasRad').enable();
        				
        		    }
        		}
        	});
	return new Ext.grid.ColumnModel([
             new Ext.grid.RowNumberer(),
			 {
				header: 'Cito',
                dataIndex: 'cito',
                width:65,
				menuDisabled:true,
					renderer:function (v, metaData, record)
					{
						if ( record.data.cito=='0')
						{
						record.data.cito='Tidak'
						}else if (record.data.cito=='1')
						{
						metaData.style  ='background:#FF0000;  "font-weight":"bold";';
						record.data.cito='Ya'
						}else if (record.data.cito=='Ya')
						{
						metaData.style  ='background:#FF0000;  "font-weight":"bold";';
						}
						
						return record.data.cito; 
					},
				editor:new Ext.form.ComboBox
								({
								id: 'cboKasusRadIGD',
								typeAhead: true,
								triggerAction: 'all',
								lazyRender: true,
								mode: 'local',
								selectOnFocus: true,
								forceSelection: true,
								emptyText: 'Silahkan Pilih...',
								width: 50,
								anchor: '95%',
								value: 1,
								store: new Ext.data.ArrayStore({
									id: 0,
									fields: ['Id', 'displayText'],
									data: [[1, 'Ya'], [2, 'Tidak']]
								}),
								valueField: 'displayText',
								displayField: 'displayText',
								value		: '',
									   
							})
					
				
		},
			 {
            id			: Nci.getId(),
            header		: 'No Transaksi',
			width		: 60,
			hidden		: false,
			menuDisabled: true,
            dataIndex	: 'no_transaksi'	
        },{
			id	 : Nci.getId(),
			header: 'Pembayaran',
			dataIndex: 'lunas',
			sortable: true,
			width: 60,
			align:'center',
			renderer: function(value, metaData, record, rowIndex, colIndex, store)
			{//console.log(metaData);
				 switch (value)
				 { 
					
					 case 't':
					 
							value = 'lunas'; //
							 break;
					 case 'f':
							value = 'belum '; // rejected

							 break;
				 }
				 return value;
			}
        },{
			id			: Nci.getId(),
        	header		: 'Kode',
            dataIndex	: 'kd_produk',
            width:30,
			menuDisabled:true,
            hidden:false,
			//editor:getRadtest_igd()
        },
		
		{
            id			: Nci.getId(),
            header		: 'DESKRIPSI',
			dataIndex	: 'deskripsi',
			hidden		: false,
			menuDisabled:true,
			width		:150,
			editor		:PenataJasaIGD.form.ComboBox.produk_rab
        },{		id			: Nci.getId(),
				header		: 'Tanggal Berkunjung',
				dataIndex	: 'tgl_transaksi',
				width		: 130,
				menuDisabled:true,
				renderer	: function(v, params, record)
				{
					if(record.data.tgl_transaksi == undefined){
						record.data.tgl_transaksi=tglGridBawah_poli_IGD;
						return record.data.tgl_transaksi;
					} else{
						if(record.data.tgl_transaksi.substring(5, 4) == '-'){
							return ShowDate(record.data.tgl_transaksi);
						} else{
							var tgl=record.data.tgl_transaksi.split("/");
						
							if(tgl[2].length == 4 && isNaN(tgl[1])){
								return record.data.tgl_transaksi;
							} else{
								return ShowDate(record.data.tgl_transaksi);
							}
						}
						
					}
				}
            },{
            id: 'colDokterRwJPJRad',
            header: 'DOKTER',
			dataIndex: 'namadok',
			menuDisabled:true,
			hidden: false,
			width:100,
			editor:PenataJasaIGD.form.ComboBox.dok_rad
        },{
			id			: Nci.getId(),
        	header		: 'QTY',
            dataIndex	: 'qty',
            width		: 100,
			menuDisabled: true,
            hidden		: true,
			editor: new Ext.form.NumberField (
					{allowBlank: false}
   
                ) }
    ]);
}


function getRadtest_igd(){
	var radCombobox = new Ext.form.ComboBox({
	   	id: 'cboRadRequest',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		hideTrigger:true,
		forceSelection: true,
		selectOnFocus:true,
		fieldLabel: 'Kode Test',
		align: 'Right',
		store: radData(),
		valueField: 'KD_PRODUK',
		displayField: 'KD_PRODUK',
		anchor: '95%',
		listeners:{
			'select': function(a, b, c){
				dsIGDPJLab_IGD.data.items[CurrentRad_igd.row].data.KD_KLAS = b.data.KD_KLAS;
				dsIGDPJLab_IGD.data.items[CurrentRad_igd.row].data.DESKRIPSI = b.data.DESKRIPSI;
				dsIGDPJLab_IGD.data.items[CurrentRad_igd.row].data.KD_DOKTER = b.data.KD_DOKTER;
				dsIGDPJLab_IGD.data.items[CurrentRad_igd.row].data.KLASIFIKASI = b.data.KLASIFIKASI;
		 	}		
		}
	});
	return radCombobox;
}

function getRadDesk(){
	var radComboboxDesk = new Ext.form.ComboBox({
	   id: 'cboRadRequestDesk',
	   typeAhead: true,
	   triggerAction: 'all',
	   lazyRender: true,
	   mode: 'local',
	   hideTrigger:true,
	   forceSelection: true,
	   selectOnFocus:true,
	   fieldLabel: 'Kode Test',
	   align: 'Right',
	   store: radData(),
	   valueField: 'DESKRIPSI',
	   displayField: 'DESKRIPSI',
	   anchor: '95%',
	   listeners:{
		   select: function(a, b, c){
			   dsIGDPJLab_IGD.data.items[CurrentRad_igd.row].data.KD_PRODUK = b.data.KD_PRODUK;
			   dsIGDPJLab_IGD.data.items[CurrentRad_igd.row].data.KD_KLAS = b.data.KD_KLAS;
			   dsIGDPJLab_IGD.data.items[CurrentRad_igd.row].data.KD_DOKTER = b.data.KD_DOKTER;
			   dsIGDPJLab_IGD.data.items[CurrentRad_igd.row].data.KLASIFIKASI = b.data.KLASIFIKASI;
		   }		
	   }
	});
	return radComboboxDesk;
}

function getRadDokter(){
	var radText = new Ext.form.TextField({
	   id			: 'RadRequestDok',
	   readonly		: true,
	   disable		: true,
	   store		: radData(),
	   value		: 'USERNAME',
	   valueField	: 'USERNAME',
	   displayField	: 'USERNAME',
	   anchor		: '95%',
	   listeners	: {}
	});
	return radText;
}

function getRadKelas(){
	var radText = new Ext.form.ComboBox({
	   id				: 'RadRequestDok',
	   typeAhead		: true,
	   triggerAction	: 'all',
	   lazyRender		: true,
	   mode				: 'local',
	   hideTrigger		: true,
	   forceSelection	: true,
	   selectOnFocus	: true,
	   fieldLabel		: 'Kode Test',
	   align			: 'Right',
	   store			: radData(),
	   valueField		: 'KD_KLASS',
	   displayField		: 'KD_KLASS',
	   anchor			: '95%',
	   listeners		: {}
	});
	return radText;
}

function radData(){
	dsrad = new WebApp.DataStore({ fields: ['KD_PRODUK','KD_KLAS','KLASIFIKASI','DESKRIPSI','KD_DOKTER'] });
	dsrad.load({ 
		params: { 
			Skip: 0, 
			Take: 50, 
			target:'ViewProdukRad',
			param: ''
		} 
	});
	return dsrad;
}


function dokter_leb_IGD()
{
    dsLookProdukList_igd_dokter_leb.load
	(
            {
                params:
                    {
                        Skip: 0,
                        Take: 1000,

                        Sort: 'kd_dokter',
                        Sortdir: 'ASC',
                        target: 'ViewDokterPenunjang',
                        param: "kd_unit = '41'"
                    }
            }
	);
 return dsLookProdukList_igd_dokter_leb;
}

PenataJasaIGD.getModel1=function(){

var fldDetail = ['KD_DOKTER','NAMA'];
dsLookProdukList_igd_dokter_leb = new WebApp.DataStore({ fields: fldDetail })
PenataJasaIGD.form.ComboBox.dok_lab= new Ext.form.ComboBox({
        		id				: Nci.getId(),
        		typeAhead		: true,
        	    triggerAction	: 'all',
        	    lazyRender		: true,
        	    mode			: 'local',
        	    emptyText		: '',
        		store			: dsLookProdukList_igd_dokter_leb,
        		valueField		: 'NAMA',
        		hideTrigger		: true,
        		displayField	: 'NAMA',
        		value			: '',
        		listeners		: {
        			select	: function(a, b, c){
					
						var line = PenataJasaIGD.grid3.getSelectionModel().selection.cell[0];
						
						if(PenataJasaIGD.ds3.data.items[line].data.no_transaksi==="" || PenataJasaIGD.ds3.data.items[line].data.no_transaksi===undefined)
						{
						PenataJasaIGD.ds3.data.items[line].data.namadok=b.data.NAMA;
						PenataJasaIGD.var_kd_dokter_leb=b.data.KD_DOKTER;
						PenataJasaIGD.grid3.getView().refresh();
						}else{
						ViewGridBawahpoliLab_IGD(Ext.getCmp('txtNoMedrecDetransaksii_igd').getValue());
						ShowPesanWarningIGD('dokter tidak bisa di ganti karena data sudah tersimpan ', 'Warning');
						}
        		    }
        		}
        	});
	var $this=this;
	return new Ext.grid.ColumnModel([
       new Ext.grid.RowNumberer(),
	    {
				header: 'Cito',
                dataIndex: 'cito',
                width:65,
				menuDisabled:true,
				renderer:function (v, metaData, record)
					{
						if ( record.data.cito=='0')
						{
						record.data.cito='Tidak'
						}else if (record.data.cito=='1')
						{
						metaData.style  ='background:#FF0000;  "font-weight":"bold";';
						record.data.cito='Ya'
						}else if (record.data.cito=='Ya')
						{
						metaData.style  ='background:#FF0000;  "font-weight":"bold";';
						}
						
						return record.data.cito; 
					},
				editor:new Ext.form.ComboBox
								({
								id: 'cboKasus',
								typeAhead: true,
								triggerAction: 'all',
								lazyRender: true,
								mode: 'local',
								selectOnFocus: true,
								forceSelection: true,
								emptyText: 'Silahkan Pilih...',
								width: 50,
								anchor: '95%',
								value: 1,
								store: new Ext.data.ArrayStore({
									id: 0,
									fields: ['Id', 'displayText'],
									data: [[1, 'Ya'], [2, 'Tidak']]
								}),
								valueField: 'displayText',
								displayField: 'displayText',
								value		: '',
									   
							})
					
				
		},{
            id			: Nci.getId(),
            header		: 'No Transaksi',
			width		: 60,
			hidden		: false,
			menuDisabled: true,
            dataIndex	: 'no_transaksi'	
        },{
			id	 : Nci.getId(),
			header: 'Pembayaran',
			dataIndex: 'lunas',
			sortable: true,
			width: 60,
			align:'center',
			renderer: function(value, metaData, record, rowIndex, colIndex, store)
			{//console.log(metaData);
				 switch (value)
				 { 
					
					 case 't':
					 
							value = 'lunas'; //
							 break;
					 case 'f':
							value = 'belum '; // rejected

							 break;
				 }
				 return value;
			}
        },
        {
			id			: Nci.getId(),
        	header		: 'Kode',
            dataIndex	: 'kd_produk',
            width		: 25,
			menuDisabled: true,
            hidden		: false,
            editor		: $this.iCombo1= new Ext.form.ComboBox({
        		id				: Nci.getId(),
        		typeAhead		: true,
        	    triggerAction	: 'all',
        	    lazyRender		: true,
        	    mode			: 'local',
        	    emptyText		: '',
        		store			: $this.ds4,
        		valueField		: 'kd_produk',
        		hideTrigger		: true,
        		displayField	: 'kd_produk',
        		value			: '',
        		listeners		: {
        			select	: function(a, b, c){	
        				var line	= $this.grid3.getSelectionModel().selection.cell[0];
        				$this.ds3.getRange()[line].data.kd_produk=b.json.kd_produk;
        				$this.ds3.getRange()[line].data.kd_klas=b.json.kd_klas;
        				$this.ds3.getRange()[line].data.deskripsi=b.json.deskripsi;
        				$this.ds3.getRange()[line].data.username=b.json.username;
        				$this.ds3.getRange()[line].data.kd_lab=b.json.kd_lab;
        				$this.grid3.getView().refresh();
        		    }
        		}
        	})
        },{
            id			: Nci.getId(),
            header		: 'DESKRIPSI',
            dataIndex	: 'deskripsi',
			 width		: 150,
            sortable	: false,
            hidden		: false,
			menuDisabled: true,
				editor:PenataJasaIGD.form.ComboBox.produk_labdesk=new Nci.form.Combobox.autoComplete({
							store	: PenataJasaIGD.form.DataStore.produk,
							select	: function(a,b,c){
								console.log(b);
								Ext.Ajax.request
								(
									{
										url: baseURL + "index.php/main/functionLAB/cekProduk",
										params:{kd_lab:b.data.kd_produk} ,
										failure: function(o)
										{
											ShowPesanErrorIGD('Hubungi Admin', 'Error');
										},
										success: function(o)
										{
											var cst = Ext.decode(o.responseText);
											if (cst.success === true)
											{
												var line = PenataJasaIGD.grid3.getSelectionModel().selection.cell[0];
												PenataJasaIGD.ds3.data.items[line].data.deskripsi=b.data.deskripsi;
												PenataJasaIGD.ds3.data.items[line].data.uraian=b.data.uraian;
												PenataJasaIGD.ds3.data.items[line].data.kd_tarif=b.data.kd_tarif;
												PenataJasaIGD.ds3.data.items[line].data.kd_produk=b.data.kd_produk;
												PenataJasaIGD.ds3.data.items[line].data.tgl_transaksi=b.data.tgl_transaksi;
												PenataJasaIGD.ds3.data.items[line].data.tgl_berlaku=b.data.tgl_berlaku;
												PenataJasaIGD.ds3.data.items[line].data.harga=b.data.harga;
												PenataJasaIGD.ds3.data.items[line].data.qty=b.data.qty;
												PenataJasaIGD.ds3.data.items[line].data.jumlah=b.data.jumlah;
								
								 
								
												PenataJasaIGD.grid3.getView().refresh();
											}
											else
											{
												ShowPesanInfoIGD('Nilai normal item '+b.data.deskripsi+' belum tersedia', 'Information');
											};
										}
									}

								)
							},
							insert	: function(o){
								return {
									uraian        	: o.uraian,
									kd_tarif 		: o.kd_tarif,
									kd_produk		: o.kd_produk,
									tgl_transaksi	: o.tgl_transaksi,
									tgl_berlaku		: o.tgl_berlaku,
									harga			: o.harga,
									qty				: o.qty,
									deskripsi		: o.deskripsi,
									jumlah			: o.jumlah,
									text			:  '<table style="font-size: 11px;"><tr><td width="60">'+o.kd_produk+'</td><td width="150">'+o.deskripsi+'</td></tr></table>'
								}
							},param	: function(){
					var o=PenataJasaIGD.grid1.getSelectionModel().getSelections()[0].data;
					var params={};
					params['kd_unit']=o.KD_UNIT;
					params['kd_customer']=o.KD_CUSTOMER;
					return params;
						},
							url		: baseURL + "index.php/main/functionLAB/getProduk",
							valueField: 'deskripsi',
							displayField: 'text',
							listWidth: 210
						})
        },{		id			: Nci.getId(),
				header: 'Tanggal Berkunjung',
				dataIndex: 'tgl_transaksi',
				width: 130,
				menuDisabled:true,
				renderer: function(v, params, record)
				{
					if(record.data.tgl_transaksi == undefined){
						record.data.tgl_transaksi=tglGridBawah_poli_IGD;
						return record.data.tgl_transaksi;
					} else{
						if(record.data.tgl_transaksi.substring(5, 4) == '-'){
							return ShowDate(record.data.tgl_transaksi);
						} else{
							var tgl=record.data.tgl_transaksi.split("/");
						
							if(tgl[2].length == 4 && isNaN(tgl[1])){
								return record.data.tgl_transaksi;
							} else{
								return ShowDate(record.data.tgl_transaksi);
							}
						}
						
					}
				}
            },{
			id			: Nci.getId(),
        	header		: 'QTY',
            dataIndex	: 'qty',
            width		: 100,
			menuDisabled: true,
            hidden		: true,
			editor: new Ext.form.NumberField (
					{allowBlank: false}),
        },{
            id			: Nci.getId(),
            header		: 'DOKTER',
			hidden		: false,
			menuDisabled: true,
            dataIndex	: 'namadok',
			editor		: PenataJasaIGD.form.ComboBox.dok_lab
        }
    ]);
};

function TambahBarisRad(){
    var x=true;
    if (x === true) {
        var p = RecordBaruRad();
        dsIGDPJLab_IGD.insert(dsIGDPJLab_IGD.getCount(), p);
    }
}

function RecordBaruRad(){
	var p = new mRecordRad({
		'ID_RADKONSUL':'',
		'KD_PRODUK':'',
	    'KD_KLAS':'', 
	    'KD_TARIF':'', 
	    'DESKRIPSI':'',
	    'KD_DOKTER':''
	});
	return p;
};

var mRecordRad = Ext.data.Record.create([
   {name: 'ID_RADKONSUL', mapping:'ID_RADKONSUL'},
   {name: 'KD_PRODUK', mapping:'KD_PRODUK'},
   {name: 'KD_KLAS', mapping:'KD_KLAS'},
   {name: 'DESKRIPSI', mapping:'DESKRIPSI'},
   {name: 'KD_DOKTER', mapping:'KD_DOKTER'}
]);


PenataJasaIGD.getTindakan=function(data){
	var $this=this;
	var tabTransaksi = new Ext.Panel({
		title: 'Tindak Lanjut',
		id:'tabTindakan',
        fileUpload: true,
        region: 'north',
        layout: 'column',
        height:100,
        anchor: '100%',
        width: 815,
        border: false,
        items: [
			{
				layout	: 'column',
			    bodyStyle: 'margin-top: 10px;',
			    border	: false,
			   
				items:[{
					layout: 'form',
					labelWidth:100,
					labelAlign:'right',
				    border: false,
				    items	: [
            $this.iCombo1 = new Ext.form.ComboBox({
                      id: 'iComboJenisPelayananRJPJ',
                      typeAhead: true,
                      triggerAction: 'all',
                      lazyRender: true,
                      mode: 'local',
                      emptyText: '',
                      width: 300,
                      store: $this.ds5,
                      valueField: 'kd_jenis_pelayanan_igd',
                      displayField: 'jenis_pelayanan_igd',
                      value: '',
                      fieldLabel: 'Jenis Pelayanan &nbsp;',
                      listeners: {
                          select: function (a, b, c) {
                              }
                      }
                  }),
              $this.iCombo2 = new Ext.form.ComboBox({
                                id: 'iComboStatusKeadaanAkhir',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                emptyText: '',
                                width: 300,
                                store: $this.dsstatupulang,
                                valueField: 'kd_status_pulang',
                                displayField: 'status_pulang',
                                value: '',
                                fieldLabel: 'Keadaan Akhir &nbsp;',
                                listeners: {
                                    select: function (a, b, c) {                                        
                                        
                                }
                  }}),
						]}
				]
			}
        ]
    });
	return tabTransaksi;
};

function datasavepoliklinikrad(mBol){	
	Ext.Ajax.request({
		url		: baseURL + "index.php/main/CreateDataObj",
		params	: getParamDetailPoliklinikRad(),
		success	: function(o){
			var cst = Ext.decode(o.responseText);
			if (cst.success === true){
				ShowPesanInfoDiagnosa_IGD(nmPesanSimpanSukses,nmHeaderSimpanData);
				if(mBol === false){
					RefreshDataSetDiagnosa_igd(Ext.get('txtNoMedrecDetransaksii_igd').dom.value,Ext.get('txtKdUnitIGD').dom.value,Ext.get('dtpTanggalDetransaksi_igd').dom.value);
				}
			}else if  (cst.success === false && cst.pesan===0){
				RefreshDataSetDiagnosa_igd(Ext.get('txtNoMedrecDetransaksii_igd').dom.value,Ext.get('txtKdUnitIGD').dom.value,Ext.get('dtpTanggalDetransaksi_igd').dom.value);
				ShowPesanWarningDiagnosa_igd(nmPesanSimpanGagal,nmHeaderSimpanData);
			}else{
				RefreshDataSetDiagnosa_igd(Ext.get('txtNoMedrecDetransaksii_igd').dom.value,Ext.get('txtKdUnitIGD').dom.value,Ext.get('dtpTanggalDetransaksi_igd').dom.value);
				ShowPesanErrorDiagnosa_igd(nmPesanSimpanError,nmHeaderSimpanData);
			}
		}
	});
}

function getParamDetailPoliklinikRad(){
    var params ={
		Table		: 'CrudpoliRad',
		KdPasien	: Ext.get('txtNoMedrecDetransaksii_igd').getValue(),
		KdUnit		: Ext.get('txtKdUnitIGD').getValue(),
		UrutMasuk	: Ext.get('txtKdUrutMasuk_igd').getValue(),
		Tgl			: Ext.get('dtpTanggalDetransaksi_igd').dom.value,
		List		: getArrdetailRadiologi()
	};
	return params;
}

function getArrdetailRadiologi(){
	var x = '',y='',z='::',hasil;
	for(var k = 0; k < dsIGDPJLab_IGD.getCount(); k++){
		if (dsIGDPJLab_IGD.data.items[k].data.KD_PRODUK == null){
			x += '';
		}else{
			hasil = dsIGDPJLab_IGD.data.items[k].data.KD_PRODUK;
			y = dsIGDPJLab_IGD.data.items[k].data.KD_DOKTER;
			y += z + hasil;
		}
		x += y + '<>';
	}
	return x;
}


function HapusBarisRad_igd(){
    if( cellselectedrad_IGD != undefined ){
    	Ext.Msg.show({
           title	: nmHapusBaris,
           msg		: 'Anda yakin akan menghapus' ,
           buttons	: Ext.MessageBox.YESNO,
           fn		: function (btn){
               if (btn =='yes'){
   					if (cellselectedrad_IGD.data.KD_PRODUK != '' && cellselectedrad_IGD.data.DESKRIPSI != ''){
   						dsIGDPJLab_IGD.removeAt(CurrentRad_igd.row);
					   	DataDeleteRad();
                    }else{
                    	ShowPesanWarningIGD('Pilih record ','Hapus data');
                    }
               }
           },
           icon: Ext.MessageBox.QUESTION
        });
    }
}



function DataDeleteRad(){
    Ext.Ajax.request({
	    url		: baseURL + "index.php/main/DeleteDataObj",
	    params	: getParamDataDeleteRad(),
	    success	: function(o){
            var cst = Ext.decode(o.responseText);
            if (cst.success === true){
                ShowPesanInfoDiagnosa_IGD(nmPesanHapusSukses,nmHeaderHapusData);
                dsTRDetailKasirIGDList.removeAt(CurrentKasirIGD.row);
                cellSelecteddeskripsi_IGD=undefined;
                RefreshDataKasirIGDDetail(Ext.get('txtNoTransaksiKasirIGD').dom.value);
                AddNewKasirIGD = false;
            }else if (cst.success === false && cst.pesan === 0 ){
                ShowPesanWarningIGD(nmPesanHapusGagal, nmHeaderHapusData);
            }else{
                ShowPesanWarningIGD(nmPesanHapusError,nmHeaderHapusData);
            }
        }
    });
}

function getParamDataDeleteRad(){
    var params ={
		Table		: 'CrudpoliRad',
		Kdproduk 	: CurrentRad_igd.data.data.KD_PRODUK,
		idradkonsul	: CurrentRad_igd.data.data.ID_RADKONSUL
	};
	return params;
}

function PilihDokterLookUp_igd(NoTrans,Urt,TglTrans,kdPrdk,kdTrf,tglBerlaku,trf) 
{
	
	
	RefreshDataTrDokter(NoTrans,Urt,TglTrans,kdPrdk);
	
	 var FormPilihDokter = 
	  {
			layout: 'column',
						border: false,
						anchor: '90%',
						items:
						[
						
							{
								columnWidth: 0.18,
								layout: 'form',
								labelWidth:70,
								border: false,
								items:
								[
								{
								xtype:'button',
								text:'Simpan',
							 	width:70,
								hideLabel:true,
								id: 'BtnOktrDokter',
								handler:function()
								{
								if(dataTrDokter.length == '' || dataTrDokter.length== 'undefined')
								{
									var jumlah =0;
								}
								else
								{
									var jumlah = dataTrDokter.length;
								}
								for(var i = 0 ;i< dsTrDokter.getCount()  ;i++)
								{	
								dataTrDokter.push({
								index		: i,
								no_tran 	: NoTrans,
								urut		: Urt,
								tgl_trans 	: TglTrans,
								kd_produk 	: kdPrdk,
								kd_tarif	: kdTrf,
								tgl_berlaku	: tglBerlaku,
								tarif		: trf,
								kd_dokter 	: dsTrDokter.data.items[i].data.KD_DOKTER ,
								kd_job 		: dsTrDokter.data.items[i].data.KD_JOB ,
								});
								}
								FormLookUDokter.close();	
								}
								},
								],
								},			
								{
								columnWidth: 0.2,
								layout: 'form',
								labelWidth:70,
								border: false,
								items:
								[	
								{
								xtype:'button',
								text:'Batal' ,
								width:70,
								hideLabel:true,
								id: 'BtnCancelGantiDokter',
								handler:function() 
									{
								dataTrDokter = [];	
								FormLookUDokter.close()
									}
								}
								]
								}
								
									
								]
  }

    var GridTrDokterColumnModel =  new Ext.grid.ColumnModel([
         new Ext.grid.RowNumberer(),
        {
        	 header			: 'kd_dokter',
        	 dataIndex		: 'KD_DOKTER',
			 width			: 80,
        	 menuDisabled	: true,
        	 hidden 		: false
        },{
            header			:'Nama Dokter',
            dataIndex		: 'NAMA',
            sortable		: false,
            hidden			: false,
			menuDisabled	: true,
			width			: 200,
            editor			: getTrDokter(dsTrDokter)
	    },{
            header			: 'Job',
            dataIndex		: 'KD_JOB',
            width			:150,
		   	menuDisabled	:true,
			editor			: JobDokter(),

        },
        ]
    );
		
  var GridPilihDokter= new Ext.grid.EditorGridPanel({
        title		: '',
		id			: 'GridDokterTr',
		stripeRows	: true,
		width		: 490,
		height		: 245,
        store		: RefreshDataTrDokter(NoTrans,Urt,TglTrans,kdPrdk),
        border		: false,
        frame		: false,
        anchor		: '100% 60%',
        autoScroll	: true,
        cm			: GridTrDokterColumnModel,
		
        //viewConfig	: {forceFit: true},
		listeners	: {
			rowclick: function( $this, rowIndex, e )
			{
        	trcellCurrentTindakan_IGD = rowIndex;
    		},
			celldblclick: function(gridView,htmlElement,columnIndex,dataRecord){
   		  }
		}
    });

 
	
	var PanelTrDokter = new Ext.Panel
	({
            id: 'PanelPilitTrDokter',
            region: 'center',
			width: 490,
			height: 260,
            border:false,
            layout: 'form',
            frame:true,
            anchor: '100% 8.0001%',
            autoScroll:false,
			
            items:
            [
			GridPilihDokter,
			{xtype: 'tbspacer',height: 3, width:100},	
			FormPilihDokter
			]
	})
 
	
    var lebar = 500;
    var FormLookUDokter = new Ext.Window
    (
        {
            id: 'gridTrDokter',
            title: 'Pilih Dokter Tindakan',
            closeAction: 'destroy',
            width: lebar,
            height: 270,
            border: false,
            resizable: false,
            plain: true,
            layout: 'column',
            iconCls: 'Request',
            modal: true,
			tbar		:[
						{
						xtype	: 'button',
						id		: 'btnaddtrdokter',
						iconCls	: 'add',
						text	: 'Tambah Data Dokter',
						handler	:  function()
						{
						dataTrDokter = [];		
						TambahBaristrDokter(dsTrDokter);	
						}
						},
						'-',
						{
						xtype	: 'button',
						id		: 'btndeltrdokter',
						iconCls	: 'remove',
						text	: 'Delete Data Dokter',
						handler	: function()  
						{
						    if (dsTrDokter.getCount() > 0 )
							{
                            if(trcellCurrentTindakan_IGD != undefined)
							{
                             HapusDataTrDokter(dsTrDokter);
                            }
                        	}else{
                            ShowPesanWarningDiagnosa_igd('Pilih record ','Hapus data');
                    		}	
							}
						},
						'-',
			],
           	items: [ PanelTrDokter
			],
            listeners:
            { 
            }
        }
    );FormLookUDokter.show();
};

var mtrDataDokter = Ext.data.Record.create([
	   {name: 'KD_DOKTER', mapping:'KD_DOKTER'},
	   {name: 'NAMA', mapping:'NAMA'},
	   {name: 'KD_JOB', mapping:'KD_JOB'},
	]);

function TambahBaristrDokter(store){
    var x=true;
    if (x === true) {
        var p = BaristrDokter();
        store.insert(store.getCount(), p);
    }
}

function BaristrDokter(){
	var p = new mtrDataDokter({
		'KD_DOKTER':'',
		'NAMA':'',
	    'KD_JOB':'',
	});
	return p;
};

function getTrDokter(store){
	var trDokterData = new Ext.form.ComboBox({
	   typeAhead: true,
	   triggerAction: 'all',
	   lazyRender: true,
	   mode: 'local',
	   hideTrigger:true,
	   forceSelection: true,
	   selectOnFocus:true,
	   store: GetDokter(),
	   valueField: 'NAMA',
	   displayField: 'NAMA',
	   anchor: '95%',
	   listeners:{
		   select: function(a, b, c){
			   console.log(store);
			   console.log(b);
			  store.data.items[trcellCurrentTindakan_IGD].data.KD_DOKTER = b.data.KD_DOKTER;
		   }		
	   }
	});
	return trDokterData;
}

function GetDokter(){
	var dataDokter  = new WebApp.DataStore({ fields: ['KD_DOKTER','NAMA'] });
	dataDokter.load({ 
		params: { 
			Skip: 0, 
			Take: 50, 
			target:'ViewComboDokter',
			param: ''
		} 
	});
	return dataDokter;
}

function JobDokter(){
	var combojobdokter = new Ext.form.ComboBox({
		typeAhead		: true,
		triggerAction	: 'all',
		lazyRender		: true,
		mode			: 'local',
		anchor 			: '96.8%',
		emptyText		: '',
		store			: new Ext.data.ArrayStore({
			fields	: ['Id','displayText'],
			data	: [[1, 'Dokter'],[2, 'Dokter Anastesi']]
		}),
		valueField		: 'displayText',
		displayField	: 'displayText'
	});
	return combojobdokter;
};


function RefreshDataTrDokter(NoTrans,Urt,TglTrans,kdPrdk)
{
	dsTrDokter.load({ 
		params	: { 
			Skip	: 0, 
			Take	: 50, 
			target	:'ViewTrDokter',
			param	:"b.kd_kasir='06' and b.no_transaksi=~"+NoTrans+"~ and b.urut=~"+Urt+"~ and b.tgl_transaksi=~"+TglTrans+"~ and a.kd_produk = ~"+kdPrdk+"~"
		} 
	});
	
	return dsTrDokter;
}

function HapusDataTrDokter(store)
{

    if ( trcellCurrentTindakan_IGD != undefined ){
        if (store.data.items[trcellCurrentTindakan_IGD].data.KD_DOKTER != '' && store.data.items[trcellCurrentTindakan_IGD].data.KD_PRODUK != ''){
					DataDeleteTrDokter(store);
					dsTrDokter.removeAt(trcellCurrentTindakan_IGD);
				}
               
        }/*else{
            dsTrDokter.removeAt(trcellCurrentTindakan_IGD);
        }*/
   

}

function DataDeleteTrDokter(store){
    Ext.Ajax.request({
        url: baseURL + "index.php/main/DeleteDataObj",
        params:  
		{
			Table			:'ViewTrDokter',
			no_transaksi	:Ext.getCmp('txtNoTransaksiKasirIGD').getValue(),
			kd_unit			:Ext.getCmp('txtKdUnitIGD').getValue(),
			kd_produk		:store.data.items[trcellCurrentTindakan_IGD].data.KD_PRODUK,
			kd_kasir		:'06',
			kd_dokter		:store.data.items[trcellCurrentTindakan_IGD].data.KD_DOKTER,
			urut			:PenataJasaIGD.dsGridTindakan.data.items[cellCurrentTindakan_IGD].data.URUT,
			tgl_transaksi	:PenataJasaIGD.dsGridTindakan.data.items[cellCurrentTindakan_IGD].data.TGL_TRANSAKSI,
		},
		//getParamDataDeleteKasirIGDDetail(),
        success: function(o){
            var cst = Ext.decode(o.responseText);
            if (cst.success === true){
                ShowPesanInfoDiagnosa_IGD(nmPesanHapusSukses,nmHeaderHapusData);
                store.removeAt(CurrentKasirIGD.row);
				RefreshDataKasirIGDDetail(Ext.getCmp('txtNoTransaksiKasirIGD').getValue());
                RefreshDataTrDokter(Ext.getCmp('txtNoTransaksiKasirIGD').getValue(),PenataJasaIGD.dsGridTindakan.data.items[cellCurrentTindakan_IGD].data.URUT,PenataJasaIGD.dsGridTindakan.data.items[cellCurrentTindakan_IGD].data.TGL_TRANSAKSI,PenataJasaIGD.dsGridTindakan.data.items[cellCurrentTindakan_IGD].data.KD_PRODUK);
				trcellCurrentTindakan_IGD=undefined;
            }else{
                ShowPesanInfoDiagnosa_IGD(nmPesanHapusError,nmHeaderHapusData);
				RefreshDataKasirIGDDetail(Ext.getCmp('txtNoTransaksiKasirIGD').getValue());
				RefreshDataTrDokter(Ext.getCmp('txtNoTransaksiKasirIGD').getValue(),PenataJasaIGD.dsGridTindakan.data.items[cellCurrentTindakan_IGD].data.URUT,PenataJasaIGD.dsGridTindakan.data.items[cellCurrentTindakan_IGD].data.TGL_TRANSAKSI,PenataJasaIGD.dsGridTindakan.data.items[cellCurrentTindakan_IGD].data.KD_PRODUK);
            }
        }
    });
}

PenataJasaIGD.alertError	= function(str, modul){
	Ext.MessageBox.show({
	    title	: modul,
	    msg		: str,
	    buttons	: Ext.MessageBox.OK,
	    icon	: Ext.MessageBox.ERROR,
		width	: 250
	});
};

function pj_req_radhasil_IGD(kd_pasien,kd_unit,tgl_masuk,urut_masuk,kd_produk,urut){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/main/functionRADPoliklinik/gethasiltest",
			params: {
			kd_pasien:kd_pasien,
			kd_unit:kd_unit,
			tgl_masuk:tgl_masuk,
			urut_masuk:urut_masuk,
			kd_produk:kd_produk,
			urut:urut},
			failure: function(o)
			{
				ShowPesanErrorIGD('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				
				if (cst.hasil==="null"|| cst.hasil===null || cst.hasil==="")
				{
				ShowPesanWarningIGD('Hasil Radiologi tidak ditemukan cek input hasil atau hubungi admin', 'cari hasil');
				
				}else{
				
				Ext.getCmp('textareapopuphasil_pjIGD').setValue(cst.hasil);
				}
			}
		}
		
	)

};
PenataJasaIGD.gridrad=function(data){
    PenataJasaIGD.panelradiodetail_hasil = new Ext.Panel
        ({
		    id: Nci.getId(),
			title: 'Hasil Pemeriksaaan',
		    closable  : true,
		    layout    : 'column',
			width     : 815,
			height    : 162,
		    itemCls   : 'blacklabel',
		    bodyStyle : 'padding: 5px 5px 5px 5px',
		    border    : true,
		    shadhow   : true,
		    margins   : '5 5 5 5',
		    anchor    : '99%',
		    iconCls   : '',
		    items     : 
			[
				{
					columnWidth: .99,
					layout: 'form',
					border: false,
					height: 162,
					labelWidth: 2,
					items:
					[
						{  
							
							xtype: 'textarea',
							name: 'textarea',
							id: 'textareapopuphasil_pjIGD',
							anchor:'99% 75%',
							readOnly:true,
							
						}
					]
				}
			]
    }
    );
    
    return PenataJasaIGD.panelradiodetail_hasil;

}
PenataJasaIGD.ok_ok=function(data){
	var $this=this;
	var tab_OK_igd = new Ext.Panel({
		id:'tabjadwalop_rwi',
        fileUpload: true,
		layout: 'Form',
		anchor: '100%',
		title: 'Informasi Jadwal',
	    width: 815,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
        items: [
				{
				xtype: 'compositefield',
				fieldLabel: 'Tgl. Operasi',
				anchor: '100%',
				width: 200,
				items: 
				[
					{
						xtype: 'datefield',
						id: 'TglOperasi_viJdwlOperasi',						
						format: 'd/M/Y',
						width: 120,
						value:now,
						listeners:
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									datarefresh_viJdwlOperasi();								
								} 						
							}
						}
					},
					
				]
			},{
				xtype: 'compositefield',
				fieldLabel: 'Jam Operasi',
				anchor: '100%',
				width: 200,
				items: 
				[
				{
					xtype: 'numberfield',
					fieldLabel: 'Jam',
					id: 'txtJam_viJdwlOperasi_Igd',
					name: 'txtJam_viJdwlOperasi_Igd',
					width: 50,
					emptyText:'Jam',
					maxLength:2,
				},
				{
					xtype: 'numberfield',
					fieldLabel: 'Menit',
					id: 'txtMenit_viJdwlOperasi_igd',
					name: 'txtMenit_viJdwlOperasi_igd',
					width: 50,
					emptyText:'Menit',
					maxLength:2,
				}
				]
			},viComboJenisTindakan_viJdwlOperasi(),
			viComboKamar_viJdwlOperasi()
			
        ]
    });
	return tab_OK_igd;
};
function viComboJenisTindakan_viJdwlOperasi()
{
	var Field =['kd_tindakan','tindakan'];
    dsvComboTindakanOperasiJadwalOperasiOK = new WebApp.DataStore({fields: Field});
	dsComboTindakanOperasiJadwalOperasiOK()
	  var cbo_viComboJenisTindakan_viJdwlOperasi_igd = new Ext.form.ComboBox
		(
		
			{
				id:"cbo_viComboJenisTindakan_viJdwlOperasi_igd",
				typeAhead: true,
				triggerAction: 'all',
				lazyRender:true,
				mode: 'local',
				emptyText:'Pilih Jenis Tindakan..',
				fieldLabel: "Jenis Tindakan",           
				width:230,
				store:dsvComboTindakanOperasiJadwalOperasiOK,
				valueField: 'kd_tindakan',
				displayField: 'tindakan',
				listeners:  
				{
				}
			}
		);
	return cbo_viComboJenisTindakan_viJdwlOperasi_igd;
};
function dsComboKamarOperasiJadwalOperasiOK()
{
	dsvComboKamarOperasiJadwalOperasiOK.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
					Sortdir: 'ASC', 
                    target: 'vComboKamarOperasiJadwalOperasiOK',
					//param:'kd_spesial='+kriteria
                }			
            }
        );   
    return dsvComboKamarOperasiJadwalOperasiOK;
}


function viComboKamar_viJdwlOperasi()
{
	var Field =['kd_unit','no_kamar','nama_kamar','jumlah_bed','digunakan'];
    dsvComboKamarOperasiJadwalOperasiOK = new WebApp.DataStore({fields: Field});
	dsComboKamarOperasiJadwalOperasiOK();
  var cbo_viComboKamar_viJdwlOperasi_igd = new Ext.form.ComboBox
    (
    
        {
            id:"cbo_viComboKamar_viJdwlOperasi_igd",
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'Pilih Kamar..',
            fieldLabel: "Kamar",     
            width:230,
            store:dsvComboKamarOperasiJadwalOperasiOK,
            valueField: 'no_kamar',
            displayField: 'nama_kamar',
            listeners:  
            {
				'select': function(a,b,c)
				{
					
				},
            }
        }
    );
    return cbo_viComboKamar_viJdwlOperasi_igd;
};

function getParamok_Igd(){
	var jamOp=Ext.getCmp('txtJam_viJdwlOperasi_Igd').getValue()+":"+Ext.getCmp('txtMenit_viJdwlOperasi_igd').getValue()+":00";
    var params={
		TrKodeTranskasi	: Ext.getCmp('txtNoTransaksiKasirIGD').getValue(),
		KdUnit			: Ext.getCmp('txtKdUnitIGD').getValue(),
		kdDokter		: Ext.getCmp('txtKdDokterIGD').getValue(),
		jam_op			: jamOp,
		tindakan		: Ext.getCmp('cbo_viComboJenisTindakan_viJdwlOperasi_igd').getValue(),
		kamar			: Ext.getCmp('cbo_viComboKamar_viJdwlOperasi_igd').getValue(),
		tgl_operasi		:Ext.getCmp('TglOperasi_viJdwlOperasi').getValue(),
		kasir			:'igd'
		
		
	};
   return params;
}

function Datasave_ok_Igd(mBol){	
		Ext.Ajax.request({
			url			: baseURL + "index.php/kamar_operasi/functionKamarOperasi/save_order_mng",
			params		: getParamok_Igd(),
			failure		: function(o){
				ShowPesanWarningIGD('Data Tidak berhasil disimpan Hubungi admin', 'Gagal');
			},
			success		: function(o){
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) {
					ShowPesanInfoIGD(nmPesanSimpanSukses,nmHeaderSimpanData);
					
				}else if(cst.success === false && cst.cari === true ){
				ShowPesanWarningIGD('Pasien telah terjadwal di Kamar operasi ', 'Gagal');
				}
				else{
				ShowPesanWarningIGD('Data Tidak berhasil disimpan hubungi admin', 'Gagal');
				}
			}
		});

}

function dsComboTindakanOperasiJadwalOperasiOK(kriteria)
{
	dsvComboTindakanOperasiJadwalOperasiOK.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
					Sortdir: 'ASC', 
                    target: 'vComboTindakanOperasiJadwalOperasiOK',
					param:kriteria
                }			
            }
        );   
    return dsvComboTindakanOperasiJadwalOperasiOK;
}