var CurrentKasirIGD =
{
    data: Object,
    details: Array,
    row: 0
};
var syssetting='igd_default_klas_produk';
var strb;
var nilai_kd_tarif;
var tanggaltransaksitampung;
var mRecordRwj = Ext.data.Record.create
(
    [
       {name: 'DESKRIPSI2', mapping:'DESKRIPSI2'},
       {name: 'KD_PRODUK', mapping:'KD_PRODUK'},
       {name: 'DESKRIPSI', mapping:'DESKRIPSI'},
       {name: 'KD_TARIF', mapping:'KD_TARIF'},
       {name: 'HARGA', mapping:'HARGA'},
       {name: 'QTY', mapping:'QTY'},
       {name: 'TGL_TRANSAKSI', mapping:'TGL_TRANSAKSI'},
       {name: 'DESC_REQ', mapping:'DESC_REQ'},
      // {name: 'KD_TARIF', mapping:'KD_TARIF'},
       {name: 'URUT', mapping:'URUT'}
    ]
);
var CurrentDiagnosa =
{
    data: Object,
    details: Array,
    row: 0
};

var FormLookUpGantidokter;
var mRecordDiagnosa = Ext.data.Record.create
(
    [
       {name: 'KASUS', mapping:'KASUS'},
       {name: 'KD_PENYAKIT', mapping:'KD_PENYAKIT'},
       {name: 'PENYAKIT', mapping:'PENYAKIT'},
       //{name: 'KD_TARIF', mapping:'KD_TARIF'},
      // {name: 'HARGA', mapping:'HARGA'},
       {name: 'STAT_DIAG', mapping:'STAT_DIAG'},
       {name: 'TGL_TRANSAKSI', mapping:'TGL_TRANSAKSI'},
      // {name: 'DESC_REQ', mapping:'DESC_REQ'},
      // {name: 'KD_TARIF', mapping:'KD_TARIF'},
       {name: 'URUT_MASUK', mapping:'URUT_MASUK'}
    ]
);


var dsTRDetailDiagnosaList;
var AddNewDiagnosa = true;
var selectCountDiagnosa = 50;
var now = new Date();
var rowSelectedDiagnosa;
var cellSelecteddeskripsi;
var FormLookUpsdetailTRDiagnosa;
var valueStatusCMDiagnosaView='All';
var nowTglTransaksi = new Date();

var labelisi;
var jeniscus;
var variablehistori;
var selectCountStatusByr_viKasirIGD='Belum Posting';
var dsTRKasirIGDList;
var dsTRDetailKasirIGDList;
var AddNewKasirIGD = true;
var selectCountKasirIGD = 50;
var now = new Date();
var rowSelectedKasirIGD;
var cellSelecteddeskripsi;
var FormLookUpsdetailTRIGD;
var valueStatusCMIGDView='All';
var nowTglTransaksi = new Date();
var KelompokPasienAddNew=true;
//var FocusCtrlCMIGD;
var vkode_customer
CurrentPage.page = getPanelIGD(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelIGD(mod_id) 
{
    var Field = ['KD_DOKTER','NO_TRANSAKSI','KD_UNIT','KD_PASIEN','NAMA','NAMA_UNIT','ALAMAT','TANGGAL_TRANSAKSI','NAMA_DOKTER','KD_CUSTOMER','CUSTOMER','URUT_MASUK','POSTING_TRANSAKSI'];
    dsTRKasirIGDList = new WebApp.DataStore({ fields: Field });
    refeshkasirIGD();
    var grListTRIGD = new Ext.grid.EditorGridPanel
    (
        {
            stripeRows: true,
            store: dsTRKasirIGDList,
            anchor: '100% 65%',
            columnLines: false,
            autoScroll:true,
            border: true,
			sort :false,
            sm: new Ext.grid.RowSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        rowselect: function(sm, row, rec)
                        {
                            rowSelectedKasirIGD = dsTRKasirIGDList.getAt(row);
                        }
                    }
                }
            ),
            listeners:
            {
                rowdblclick: function (sm, ridx, cidx)
                {
                    rowSelectedKasirIGD = dsTRKasirIGDList.getAt(ridx);
                    if (rowSelectedKasirIGD != undefined)
                    {
                        IGDLookUp(rowSelectedKasirIGD.data);
                    }
                    else
                    {
                        IGDLookUp();
                    }
                }
            },
        cm: new Ext.grid.ColumnModel
            (
                [
                   {
                        header: 'Status Posting',
                        width: 150,
                        sortable: false,
						hideable:true,
						hidden:false,
						menuDisabled:true,
                        dataIndex: 'POSTING_TRANSAKSI',
                        id: 'txtposting',
						renderer: function(value, metaData, record, rowIndex, colIndex, store)
                        {
                             switch (value)
                             {
                                 case 't':
                                         metaData.css = 'StatusHijau'; // 
                                         break;
                                 case 'f':
                                        metaData.css = 'StatusMerah'; // rejected

                                         break;
                             }
                             return '';
                        }
						
                    },
                    {
                        id: 'colReqIdViewIGD',
                        header: 'No. Transaksi',
                        dataIndex: 'NO_TRANSAKSI',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 80
                    },
                    {
                        id: 'colTglIGDViewIGD',
                        header: 'Tgl Transaksi',
                        dataIndex: 'TANGGAL_TRANSAKSI',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 75,
                            renderer: function(v, params, record)
                            {
                                    return ShowDate(record.data.TANGGAL_TRANSAKSI);

                        }
                    },
					{
                        header: 'No. Medrec',
                        width: 65,
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        dataIndex: 'KD_PASIEN',
                        id: 'colIGDerViewIGD'
                    },
					{
                        header: 'Pasien',
                        width: 190,
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        dataIndex: 'NAMA',
                        id: 'colIGDerViewIGD'
                    },
                    {
                        id: 'colLocationViewIGD',
                        header: 'Alamat',
                        dataIndex: 'ALAMAT',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 170
                    },
                    
                    {
                        id: 'colDeptViewIGD',
                        header: 'Dokter',
                        dataIndex: 'NAMA_DOKTER',
                        sortable: false,
							hideable:false,
							menuDisabled:true,
                        width: 150
                    },
                    {
                        id: 'colImpactViewIGD',
                        header: 'Unit',
                        dataIndex: 'NAMA_UNIT',
						sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 90
                    },
					
                   
                ]
            ),viewConfig: {forceFit: true},
            tbar:
                [/*
                    {
                        id: 'btnEditIGD',
                        text: nmEditData,
                        tooltip: nmEditData,
                        iconCls: 'Edit_Tr',
                        handler: function(sm, row, rec)
                        {
                            if (rowSelectedKasirIGD != undefined)
                            {
                                    IGDLookUp(rowSelectedKasirIGD.data);
                            }
                            else
                            {
							ShowPesanWarningIGD('Pilih data data tabel  ','Edit data');
                                    //alert('');
                            }
                        }
                    },'' ,' ' , '' , '' ,'' ,' ' , '' , '' , ' ', '' , '' , ' ','', '' ,' ', ' ','' , ' ','' , ' ','' , ' ','' ,
					'', '', '' + ' ', ' ','' + ' ','' + ' ','' + ' ','' + ' ', ' ','' + ''
					,'' ,' ' , '' , '' ,'' ,' ' , '' , '' , ' ', '' , '' , ' ','', '' ,' ', ' ','' , ' ','' , ' ','' , ' '
					,' ', '-', 'Status Posting' + ' : ', ' ',
						mComboStatusBayar_viKasirIGD()
                    ,' ','' + ' ','' + ' ', ' ','' + '',
					' ','' + ' ','' + ' ', ' ','' + '', ' ', '-', 'Tanggal Kunjungan' + ' : ', ' ',
                    {
                        xtype: 'datefield',
                        fieldLabel: 'Tanggal Kunjungan' + ' ',
                        id: 'dtpTglAwalFilterIGD',
                        format: 'd/M/Y',
                        value: now,
                        width: 100,
                        onInit: function() { }
                    }, ' ', '  ' + 's/d' + ' ', ' ', {
                        xtype: 'datefield',
                        fieldLabel: nmSd + ' ',
                        id: 'dtpTglAkhirFilterIGD',
                        format: 'd/M/Y',
                        value: now,
                        width: 100
                    }*/
                ]
            }
	);
	
	
var Legendpenatajasaigd = new Ext.Panel
	(
            {
            id: 'Legendpenatajasaigd',
            region: 'center',
            border:false,
            bodyStyle: 'padding:0px 7px 0px 7px',
            layout: 'column',
            frame:true,
            //height:32,
            anchor: '100% 8.0001%',
            autoScroll:false,
            items:
            [
                {
                    columnWidth: .033,
                    layout: 'form',
                    style:{'margin-top':'-1px'},
                    //height:32,
                    anchor: '100% 8.0001%',
                    border: false,
                    html: '<img src="'+baseURL+'ui/images/icons/16x16/hijau.png" class="text-desc-legend"/>'
                },
                {
                    columnWidth: .08,
                    layout: 'form',
                    //height:32,
                    anchor: '100% 8.0001%',
                    style:{'margin-top':'1px'},
                    border: false,
                    html: " Posting"
                },
                {
                    columnWidth: .033,
                    layout: 'form',
                    style:{'margin-top':'-1px'},
                    border: false,
                    //height:35,
                    anchor: '100% 8.0001%',
                    //html: '<img src="./images/icons/16x16/merah.png" class="text-desc-legend"/>'
                    html: '<img src="'+baseURL+'ui/images/icons/16x16/merah.png" class="text-desc-legend"/>'
                },
                {
                    columnWidth: .1,
                    layout: 'form',
                    //height:32,
                    anchor: '100% 8.0001%',
                    style:{'margin-top':'1px'},
                    border: false,
                    html: " Belum Posting"
                }
            ]

        }
    )
    var FormDepanIGD = new Ext.Panel
    (
        {
            id: mod_id,
            closable: true,
            region: 'center',
            layout: 'form',
            title: 'Penata Jasa ',
            border: false,
            shadhow: true,
            autoScroll:false,
            iconCls: 'Request',
            margins: '0 5 5 0',
            items: [{
						   xtype:'panel',
							   plain:true,
							   activeTab: 0,
								height:180,
							   //deferredRender: false,
							   defaults:
							   {
								bodyStyle:'padding:10px',
								autoScroll: true
							   },
							   items:[
					 {
				    layout: 'form',
					 margins: '0 5 5 0',
					border: true ,
					items:
					[
				     {
                    xtype: 'textfield',
                    fieldLabel: ' No. Medrec' + ' ',
                    id: 'txtFilterIGDNomedrec',
                    anchor : '100%',
                    onInit: function() { },
					listeners:
                        {
                            'specialkey' : function()
                            {
                                var tmpNoMedrec = Ext.get('txtFilterIGDNomedrec').getValue()
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10 )
                                {
                                    if(tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10 )
                                        {
										   
                                             var tmpgetNoMedrec = formatnomedrec(Ext.get('txtFilterIGDNomedrec').getValue())
                                             Ext.getCmp('txtFilterIGDNomedrec').setValue(tmpgetNoMedrec);
                                            var tmpkriteria = getCriteriaFilter_viDaftar();
											 RefreshDataFilterKasirIGD();
                                      
                                        }
                                        else
                                            {
                                                if (tmpNoMedrec.length === 10)
                                                    {
                                                       // tmpkriteria = getCriteriaFilter_viDaftar();
                                                     RefreshDataFilterKasirIGD();
                                                    }
                                                    else
                                                    Ext.getCmp('txtFilterIGDNomedrec').setValue('')
                                            }
                                }
                            }

                        }
                },
				
				{	 
				xtype: 'tbspacer',
				height: 3
				},	
					{
							xtype: 'textfield',
							fieldLabel: ' Pasien' + ' ',
							id: 'TxtIgdFilternama',
							anchor :'100%',
							 enableKeyEvents: true,
							listeners:
							{ 
						
								'keyup' : function()
								{

			                      RefreshDataFilterKasirIGD();
			
								}
							}
						},
						{	 
						xtype: 'tbspacer',
						height: 3
						},	
						
						mComboUnit_viKasirIgd(),
						
					{
							xtype: 'textfield',
							fieldLabel: ' Dokter' + ' ',
							id: 'TxtIgdFilterDokter',
							anchor:'100%',
							enableKeyEvents: true,
							listeners:
							{ 
								'keyup' : function()
								{
									
									RefreshDataFilterKasirIGD();

						
														
								}
							}
						},mComboStatusBayar_viKasirIGD(),getItemPaneltgl_filter()
						]}
						]},grListTRIGD,Legendpenatajasaigd],
            tbar:
            [
             
            ],
            listeners:
            {
                'afterrender': function()
                {}
            }
        }
    );
	
   return FormDepanIGD

};


function getItemPaneltgl_filter() 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .50,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'datefield',
					    fieldLabel: 'Tanggal ',
					    id: 'dtpTglAwalFilterIGD',
					    name: 'dtpTglAwalFilterIGD',
					    format: 'd/M/Y',
						//readOnly : true,
					    value: now,
					    anchor: '99%',
					    listeners:
						{
						
						 'specialkey' : function()
                            {
                                var tmpNoMedrec = Ext.get('txtFilterIGDNomedrec').getValue()
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10 )
                                {
								RefreshDataFilterKasirIGD();
								}
						}
						}
					}
				]
			},
			 {xtype: 'tbtext', text: ' s/d', cls: 'left-label', width: 30},
			{
			    columnWidth: .50,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[ 
		
					{
					    xtype: 'datefield',
					   // fieldLabel: 'Tanggal ',
					    id: 'dtpTglAkhirFilterIGD',
					    name: 'dtpTglAkhirFilterIGD',
					    format: 'd/M/Y',
						//readOnly : true,
					    value: now,
					    anchor: '100%',
						   listeners:
						{
						
						 'specialkey' : function()
                            {
                                var tmpNoMedrec = Ext.get('txtFilterIGDNomedrec').getValue()
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10 )
                                {
								RefreshDataFilterKasirIGD();
								}
						}
						}
					}
				]
			}
		]
	}
    return items;
};

function mComboStatusBayar_viKasirIGD()
{
  var cboStatus_viKasirIgd = new Ext.form.ComboBox
	(
		{
			id:'cboStatus_viKasirIgd',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
		    anchor :'100%',
			emptyText:'',
			fieldLabel: 'Status Posting',
			//width:60,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Semua'],[2, 'Posting'], [3, 'Belum Posting']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCountStatusByr_viKasirIGD,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectCountStatusByr_viKasirIGD=b.data.displayText ;
					//RefreshDataSetDokter();
					RefreshDataFilterKasirIGD();

				}
			}
		}
	);
	return cboStatus_viKasirIgd;
};


function mComboUnit_viKasirIgd() 
{
	var Field = ['KD_UNIT','NAMA_UNIT'];

    dsunit_viKasirIGD = new WebApp.DataStore({ fields: Field });
    dsunit_viKasirIGD.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			  
                            Sort: 'kd_unit',
			    Sortdir: 'ASC',
			    target: 'ComboUnit',
                param: "" //+"~ )"
			}
		}
	);
	
    var cboUNIT_viKasirIGD = new Ext.form.ComboBox
	(
		{
		    id: 'cboUNIT_viKasirIGD',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel:  'Poli ',
		    align: 'Right',
			  width: 100,
		    anchor:'100%',
		    store: dsunit_viKasirIGD,
		    valueField: 'NAMA_UNIT',
		    displayField: 'NAMA_UNIT',
			value:'all',
		    listeners:
			{
			
			    'select': function(a, b, c) 
				{					       
						   RefreshDataFilterKasirIGD();

			        //selectStatusCMIGDView = b.data.DEPT_ID;
					//
			    }

			}
		}
	);
	
    return cboUNIT_viKasirIGD;
};



function IGDLookUp(rowdata) 
{
    var lebar = 600;
    FormLookUpsdetailTRIGD = new Ext.Window
    (
        {
            id: 'gridIGD',
            title: 'Penata Jasa IGD',
            closeAction: 'destroy',
            width: lebar,
            height: 500,
            border: false,
            resizable: false,
            plain: true,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryTRIGD(lebar,rowdata),
            listeners:
            {
                
            }
        }
    );

    FormLookUpsdetailTRIGD.show();
    if (rowdata == undefined) 
	{
        IGDAddNew();
    }
    else 
	{
        TRIGDInit(rowdata)
    }

};


function mComboPoliklinik(lebar)
{

    var Field = ['KD_UNIT','NAMA_UNIT'];
    ds_Poli_viDaftar = new WebApp.DataStore({fields: Field});

	ds_Poli_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'NAMA_UNIT',
                Sortdir: 'ASC',
                target:'ViewSetupUnit',
                param: 'kd_bagian=2 and type_unit=false and kd_unit not in (~'+Ext.get('txtKdUnitIGD').dom.value +'~)'
            }
        }
    )

    var cboPoliklinikRequestEntry = new Ext.form.ComboBox
    (
        {
            id: 'cboPoliklinikRequestEntry',
            typeAhead: true,
            triggerAction: 'all',
			width: 170,
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Poliklinik...',
            fieldLabel: 'Poliklinik ',
            align: 'Right',
            store: ds_Poli_viDaftar,
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
            anchor: '95%',
            listeners:
                {
                    'select': function(a, b, c)
				{
                                   //alert(b.data.KD_PROPINSI)
                                   loaddatastoredokter(b.data.KD_UNIT)
								   selectKlinikPoli=b.data.KD_UNIT
                                }
                   


		}
        }
    )

    return cboPoliklinikRequestEntry;
}

function loaddatastoredokter(kd_unit)
{
          dsDokterRequestEntry.load
                (
                    {
                     params:
			{
                            Skip: 0,
			    Take: 1000,
			    //Sort: 'DEPT_ID',
                            Sort: 'nama',
			    Sortdir: 'ASC',
			    target: 'ViewComboDokter',
			    param: 'where dk.kd_unit=~'+ kd_unit+ '~'
			}
                    }
                )
}

function mComboDokterRequestEntry()
{
    var Field = ['KD_DOKTER','NAMA'];

    dsDokterRequestEntry = new WebApp.DataStore({fields: Field});


    var cboDokterRequestEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboDokterRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
            forceSelection: true,
		    emptyText:'Pilih Dokter...',
		    labelWidth:80,
			fieldLabel: 'Dokter      ',
		    align: 'Right',
                     
//		    anchor:'60%',
		    store: dsDokterRequestEntry,
		    valueField: 'KD_DOKTER',
		    displayField: 'NAMA',
            anchor: '95%',
		    listeners:
			{
			    'select': function(a,b,c)
				{
//                                  var selectDokterRequestEntry = b.data.KD_PROPINSI;
                                    //alert("is");
									selectDokter = b.data.KD_DOKTER;
                                },
                            'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) //atau Ext.EventObject.ENTER
                                    Ext.getCmp('kelPasien').focus();
                                    }, c);
                                }
			}
                }
	);

    return cboDokterRequestEntry;
};

function KonsultasiLookUp(rowdata) 
{
    var lebar = 350;
    FormLookUpsdetailTRKonsultasi = new Ext.Window
    (
        {
            id: 'gridKonsultasi',
            title: 'Konsultasi',
            closeAction: 'destroy',
            width: lebar,
            height: 130,
            border: false,
            resizable: false,
            plain: true,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
           items: {
						layout: 'hBox',
					//	width:222,
						border: false,
						bodyStyle: 'padding:5px 0px 5px 5px',
						defaults: { margins: '3 3 3 3' },
						anchor: '90%',
					
								items:
								[
								
										
											{
												columnWidth: 250,
												layout: 'form',
												labelWidth:80,
												border: false,
												items:
												[
													{
														xtype: 'textfield',
														//fieldLabel:'Dokter  ',
														name: 'txtKdunitKonsultasi',
														id: 'txtKdunitKonsultasi',
														//emptyText:nmNomorOtomatis,
														hidden :true,
														readOnly:true,
														anchor: '80%'
													},
													{
														xtype: 'textfield',
														fieldLabel: 'Poli Asal ',
														//hideLabel:true,
														name: 'txtnamaunitKonsultasi',
														id: 'txtnamaunitKonsultasi',
														readOnly:true,
														anchor: '95%',
														listeners: 
														{ 
															
														}
													}, 
														mComboPoliklinik(lebar),
													
														mComboDokterRequestEntry()
														
												]
											},
											{
												columnWidth: 250,
												layout: 'form',
												labelWidth:70,
												border: false,
												items:
												[
														{
																	xtype:'button',
																	text:'Kosultasi',
																	width:70,
																	style:{'margin-left':'0px','margin-top':'0px'},
																	hideLabel:true,
																	id: 'btnOkkonsultasi',
																	handler:function()
																	{
																	Datasave_Konsultasi(false);
																	FormLookUpsdetailTRKonsultasi.close()	
																	}
														},
														{	 
															xtype: 'tbspacer',
															
															height: 3
														},				
														{
															xtype:'button',
															text:'Batal' ,
															width:70,
															hideLabel:true,
															id: 'btnCancelkonsultasi',
															handler:function() 
															{
																FormLookUpsdetailTRKonsultasi.close()
															}
														}
											 ]
											
											
											}
											
											
										
									,
									
								]
			},
            listeners:
            {
              
            }
        }
    );

    FormLookUpsdetailTRKonsultasi.show();
  
      KonsultasiAddNew();
	

};
function KonsultasiAddNew() 
{
    AddNewKasirKonsultasi = true;
	Ext.get('txtKdunitKonsultasi').dom.value =Ext.get('txtKdUnitIGD').dom.value   ;
	Ext.get('txtnamaunitKonsultasi').dom.value=Ext.get('txtNamaUnit').dom.value;
	

};

function getFormEntryTRIGD(lebar,data) 
{
    var pnlTRIGD = new Ext.FormPanel
    (
        {
            id: 'PanelTRIGD',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
             height:189,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [getItemPanelInputIGD(lebar)],
           tbar:
            [
              
		              
		                
					 
					    	{
						        text: ' Ganti Dokter',
						        id:'btnLookUpGantiDokter_viKasirIgd',
						        iconCls: 'gantidok',
						        handler: function(){
								   GantiDokterLookUp();
						        },
					    	},
							{
						        text: 'Ganti Kelompok Pasien',
						        id:'btngantipasien',
						        iconCls: 'gantipasien',
						        handler: function(){
								   KelompokPasienLookUp();
						        },
					    	},
							{
						        text: 'Posting Ke Kasir',
						        id:'btnposting',
						        iconCls: 'gantidok',
						        handler: function(){
								  setpostingtransaksi(data.NO_TRANSAKSI);
						        },
							}
					    	
		               
		          
               
            ]
        }
    );
var x;
 var GDtabDetailIGD = new Ext.TabPanel   
    (
        {
        id:'GDtabDetailIGD',
        region: 'center',
        activeTab: 0,
		height:250,
		width :565,
        anchor: '100% 100%',
        border:false,
        plain: true,
        defaults:
                {
                autoScroll: true
				},
                items: [GetDTLTRIGDGrid(),GetDTLTRDiagnosaGrid()
				
		                //-------------- ## --------------
					],
        tbar:
                [
                       
                           {
                                id:'btnTambahBrsRequest',
                                text: 'Tambah baris',
                                tooltip: nmTambahBaris,
                                iconCls: 'AddRow',
                                handler: function()
                                {
                                        TambahBarisIGD();
                                }
                        },
						{
							text: 'Tambah Produk',
							id: 'btnLookupIGD',
							tooltip: nmLookup,
							hidden :true,
							iconCls: 'find',
							handler: function()
							{
						  
									var p = RecordBaruRWJ();
									var str='';
									if (Ext.get('txtKdUnitIGD').dom.value  != undefined && Ext.get('txtKdUnitIGD').dom.value  != '')
									{
											
											str =  Ext.get('txtKdUnitIGD').dom.value;
									};
									
									FormLookupKasir(str,strb,nilai_kd_tarif, dsTRDetailKasirIGDList, p, true, '', true,syssetting);
							}
						},
                       
							 {
								text: 'Simpan',
								id: 'btnSimpanIGD',
								tooltip: nmSimpan,
								iconCls: 'save',
								handler: function()
								{
								
								Datasave_KasirIGD(false);
								   
								}
							},   {
                                id:'btnHpsBrsIGD',
                                text: 'Hapus Baris',
                                tooltip: 'Hapus Baris',
                                iconCls: 'RemoveRow',
                                handler: function()
                                {
                                        if (dsTRDetailKasirIGDList.getCount() > 0 )
                                        {
                                                if (cellSelecteddeskripsi != undefined)
                                                {
                                                        if(CurrentKasirIGD != undefined)
                                                        {
                                                                HapusBarisIGD();
                                                        }
                                                }
                                                else
                                                {
                                                        ShowPesanWarningIGD('Pilih record ','Hapus data');
                                                }
                                        }
                                }
                        },
						{
							text: 'Tambah Diagnosa',
							id: 'btnLookupDiagnosa',
							tooltip: nmLookup,
							iconCls: 'find',
							handler: function()
							{
								
									var p = RecordBaruDiagnosa();
									var str='';
									FormLookupDiagnosa(str, dsTRDetailDiagnosaList, p, true, '', true);
						}
						},
                        
							 {
							text: 'Simpan',
							id: 'btnSimpanDiagnosa',
							tooltip: nmSimpan,
							iconCls: 'save',
							handler: function()
								{
								
								 if (dsTRDetailDiagnosaList.getCount() > 0 )
												{
														if (cellSelecteddeskripsi != undefined)
														{
																if(CurrentDiagnosa != undefined)
																{
																		 
																		Datasave_Diagnosa(false);
																		
																		
																}
														}
														else
														{
																ShowPesanWarningDiagnosa('Pilih record lalu ubah STAT_DIAG  ','Ubah data');
														}
												}
                       
								}
							},
							{
                                id:'btnHpsBrsDiagnosa',
                                text: 'Hapus Baris',
                                tooltip: 'Hapus Baris',
                                iconCls: 'RemoveRow',
                                handler: function()
                                {
                                        if (dsTRDetailDiagnosaList.getCount() > 0 )
                                        {
                                                if (cellSelecteddeskripsi != undefined)
                                                {
                                                        if(CurrentDiagnosa != undefined)
                                                        {
                                                                HapusBarisDiagnosa();
                                                        }
                                                }
                                                else
                                                {
                                                        ShowPesanWarningDiagnosa('Pilih record ','Hapus data');
                                                }
                                        }
                                }
							}
                ],
		listeners:
            {
               'tabchange' : function (panel, tab) {
				//GDtabDetailIGD.setActiveTab(1);
				if (x ==1)
				{
				 Ext.getCmp('btnLookupIGD').hide()
                 Ext.getCmp('btnSimpanIGD').hide()
				 Ext.getCmp('btnHpsBrsIGD').hide()
                 Ext.getCmp('btnHpsBrsDiagnosa').show()
				  Ext.getCmp('btnSimpanDiagnosa').show()
				  Ext.getCmp('btnLookupDiagnosa').show()
				//alert('a');
					x=2;
					return x;
				}else 
				{ 	
				 Ext.getCmp('btnLookupIGD').hide()
                 Ext.getCmp('btnSimpanIGD').show()
				 Ext.getCmp('btnHpsBrsIGD').show()
                 Ext.getCmp('btnHpsBrsDiagnosa').hide()
				  Ext.getCmp('btnSimpanDiagnosa').hide()
				  Ext.getCmp('btnLookupDiagnosa').hide()
				 ,
				x=1;	
				return x;
				}
				
			}
		//bbar :			/**/
        }
		}
		
    );
	
   
   
   var pnlTRIGD2 = new Ext.FormPanel
    (
        {
            id: 'PanelTRIGD2',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
             height:260,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [	GDtabDetailIGD
			
			]
        }
    );

	
   
   
    var FormDepanIGD = new Ext.Panel
	(
		{
		    id: 'FormDepanIGD',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [pnlTRIGD,pnlTRIGD2	
				

			]

		}
	);
	
	if( data.POSTING_TRANSAKSI == 't')
	{
	setdisablebutton();
	}
	else
	{
	setenablebutton();	
	//Ext.getCmp('PanelTRIGD').enable();	
	}


    return FormDepanIGD
};

function RecordBaruDiagnosa()
{
	var p = new mRecordDiagnosa
	(
		{
			'KASUS':'',
			'KD_PENYAKIT':'',
		    'PENYAKIT':'', 
		  //  'KD_TARIF':'', 
		   // 'HARGA':'',
		    'STAT_DIAG':'',
		    'TGL_TRANSAKSI':Ext.get('dtpTanggalDetransaksi').dom.value, 
		    //'DESC_REQ':'',
		   // 'KD_TARIF':'',
		    'URUT_MASUK':''
		}
	);
	
	return p;
};

function HapusBarisDiagnosa()
{
    if ( cellSelecteddeskripsi != undefined )
    {
        if (cellSelecteddeskripsi.data.PENYAKIT != '' && cellSelecteddeskripsi.data.KD_PENYAKIT != '')
        {
            Ext.Msg.show
            (
                {
                   title:nmHapusBaris,
                   msg: 'Anda yakin akan menghapus produk' + ' : ' + cellSelecteddeskripsi.data.PENYAKIT ,
                   buttons: Ext.MessageBox.YESNO,
                   fn: function (btn)
                   {
                       if (btn =='yes')
                        {
                            if(dsTRDetailDiagnosaList.data.items[CurrentDiagnosa.row].data.URUT_MASUK === '')
                            {
                                dsTRDetailDiagnosaList.removeAt(CurrentDiagnosa.row);
                            }
                            else
                            {
                                
                                            if (btn =='yes')
                                            {
                                               DataDeleteDiagnosaDetail();
                                            };
                                
                            };
                        };
                   },
                   icon: Ext.MessageBox.QUESTION
                }
            );
        }
        else
        {
            dsTRDetailDiagnosaList.removeAt(CurrentDiagnosa.row);
        };
    }
};

function DataDeleteDiagnosaDetail()
{
    Ext.Ajax.request
    (
        {
            
            url: baseURL + "index.php/main/DeleteDataObj",
            params:  getParamDataDeleteDiagnosaDetail(),
            success: function(o)
            {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true)
                {
                    ShowPesanInfoDiagnosa(nmPesanHapusSukses,nmHeaderHapusData);
                    dsTRDetailDiagnosaList.removeAt(CurrentDiagnosa.row);
                    cellSelecteddeskripsi=undefined;
                  RefreshDataSetDiagnosa(Ext.get('txtNoMedrecDetransaksi').dom.value,Ext.get('txtKdUnitIGD').dom.value,Ext.get('dtpTanggalDetransaksi').dom.value);
                    AddNewDiagnosa = false;
                }
           
                else
                {
                    ShowPesanWarningDiagnosa(nmPesanHapusError,nmHeaderHapusData);
					RefreshDataSetDiagnosa(Ext.get('txtNoMedrecDetransaksi').dom.value,Ext.get('txtKdUnitIGD').dom.value,Ext.get('dtpTanggalDetransaksi').dom.value);
                };
            }
        }
    )
};

function getParamDataDeleteDiagnosaDetail()
{
    var params =
    {
        Table: 'ViewDiagnosa',
        KdPasien: Ext.get('txtNoMedrecDetransaksi').getValue(),
		KdUnit: Ext.get('txtKdUnitIGD').getValue(),
		TglMasuk:CurrentDiagnosa.data.data.TGL_MASUK,
		KdPenyakit : CurrentDiagnosa.data.data.KD_PENYAKIT,
		UrutMasuk:CurrentDiagnosa.data.data.URUT_MASUK,
		Urut:CurrentDiagnosa.data.data.URUT,
    };
	
    return params
};



function getParamDetailTransaksiDiagnosa2() 
{
    var params =
	{
		Table:'ViewTrDiagnosa',
		
		
		KdPasien: Ext.get('txtNoMedrecDetransaksi').getValue(),
		KdUnit: Ext.get('txtKdUnitIGD').getValue(),
		UrutMasuk:Ext.get('txtKdUrutMasuk').getValue(),
		Tgl: Ext.get('dtpTanggalDetransaksi').dom.value,
		List:getArrDetailTrDiagnosa(),
		JmlField: mRecordDiagnosa.prototype.fields.length-4,
		JmlList:GetListCountDetailDiagnosa(),
		Hapus:1,
		Ubah:0
	};
    return params
};


function GetListCountDetailDiagnosa()
{
	
	var x=0;
	for(var i = 0 ; i < dsTRDetailDiagnosaList.getCount();i++)
	{
		if (dsTRDetailDiagnosaList.data.items[i].data.KD_PENYAKIT != '' || dsTRDetailDiagnosaList.data.items[i].data.PENYAKIT  != '')
		{
			x += 1;
		};
	}
	return x;
	
};




function getArrDetailTrDiagnosa()
{
	var x='';
	for(var i = 0 ; i < dsTRDetailDiagnosaList.getCount();i++)
	{
		if (dsTRDetailDiagnosaList.data.items[i].data.KD_PENYAKIT != '' && dsTRDetailDiagnosaList.data.items[i].data.PENYAKIT != '')
		{
			var y='';
			var z='@@##$$@@';
			
			y = dsTRDetailDiagnosaList.data.items[i].data.URUT_MASUK
			y += z + dsTRDetailDiagnosaList.data.items[i].data.KD_PENYAKIT
			y += z + dsTRDetailDiagnosaList.data.items[i].data.STAT_DIAG
			y += z + dsTRDetailDiagnosaList.data.items[i].data.KASUS
			
			
			if (i === (dsTRDetailDiagnosaList.getCount()-1))
			{
				x += y 
			}
			else
			{
				x += y + '##[[]]##'
			};
		};
	}	
	
	return x;
};




function Datasave_Diagnosa(mBol) 
{	
			Ext.Ajax.request
			 (
				{
					//url: "./Datapool.mvc/CreateDataObj",
					url: baseURL + "index.php/main/functionIGD/saveDiagnosa",
					params: getParamDetailTransaksiDiagnosa2(),
					success: function(o) 
					{
	
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoDiagnosa(nmPesanSimpanSukses,nmHeaderSimpanData);
							//RefreshDataDiagnosa();
							if(mBol === false)
							{
						
							RefreshDataSetDiagnosa(Ext.get('txtNoMedrecDetransaksi').dom.value,Ext.get('txtKdUnitIGD').dom.value,Ext.get('dtpTanggalDetransaksi').dom.value);
								
							};
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							RefreshDataSetDiagnosa(Ext.get('txtNoMedrecDetransaksi').dom.value,Ext.get('txtKdUnitIGD').dom.value,Ext.get('dtpTanggalDetransaksi').dom.value);
							ShowPesanWarningDiagnosa(nmPesanSimpanGagal,nmHeaderSimpanData);
						}
						
						else 
						{
							RefreshDataSetDiagnosa(Ext.get('txtNoMedrecDetransaksi').dom.value,Ext.get('txtKdUnitIGD').dom.value,Ext.get('dtpTanggalDetransaksi').dom.value);
							ShowPesanErrorDiagnosa(nmPesanSimpanError,nmHeaderSimpanData);
						};
					}
				}
			)
		
	
};

function ShowPesanWarningDiagnosa(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorDiagnosa(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoDiagnosa(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};







function GetDTLTRDiagnosaGrid() 
{
    var fldDetail = ['KD_PENYAKIT','PENYAKIT','KD_PASIEN','URUT','URUT_MASUK','TGL_MASUK','KASUS','STAT_DIAG'];
	
    dsTRDetailDiagnosaList = new WebApp.DataStore({ fields: fldDetail })

    var gridDTLTRDiagnosa = new Ext.grid.EditorGridPanel
    (
        {
            title: 'Diagnosa',
            stripeRows: true,
            store: dsTRDetailDiagnosaList,
            border: true,
            columnLines: true,
            frame: false,
            anchor: '100%',
             autoScroll:true,
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                            cellSelecteddeskripsi = dsTRDetailDiagnosaList.getAt(row);
                            CurrentDiagnosa.row = row;
                            CurrentDiagnosa.data = cellSelecteddeskripsi;
                           // FocusCtrlCMDiagnosa='txtAset';
                        }
                    }
                }
            ),
            cm: TRDiagnosaColumModel()
             , viewConfig:{forceFit: true}
        }
		
		
    );
	
	

    return gridDTLTRDiagnosa;
};

function TRDiagnosaColumModel() 
{
    return new Ext.grid.ColumnModel
    (
        [
           new Ext.grid.RowNumberer(),
            {
                id: 'colKdProduk',
                header: 'No.ICD',
                dataIndex: 'KD_PENYAKIT',
                width:70,
					menuDisabled:true,
                hidden:false
            },
			{
                id: 'colePenyakitDiagnosa',
                header: 'Penyakit',
                dataIndex: 'PENYAKIT',
					menuDisabled:true,
				width:200
                
            }
            ,
			{
                id: 'colePasien',
                header: 'kd_pasien',
                dataIndex: 'KD_PASIEN',
				hidden:true
                
            }
			,
			{
                id: 'coleurut',
                header: 'urut',
                dataIndex: 'URUT',
				hidden:true
                
            }
			,
			{
                id: 'coleurutmasuk',
                header: 'urut masuk',
                dataIndex: 'URUT_MASUK',
				hidden:true
                
            }
            ,
			{
                id: 'coletglmasuk',
                header: 'tgl masuk',
                dataIndex: 'TGL_MASUK',
				hidden:true
                
            }
            ,
            {
                id: 'colProblemDiagnosa',
                header: 'Diagnosa',
                width:130,
				menuDisabled:true,
				//align: 'right',
				//hidden :true,
                dataIndex: 'STAT_DIAG',
                editor: new Ext.form.ComboBox
                (
                    {
							id:'cboDiagnosa',
							typeAhead: true,
							triggerAction: 'all',
							lazyRender:true,
							mode: 'local',
							selectOnFocus:true,
							forceSelection: true,
							emptyText:'Silahkan Pilih...',
							//fieldLabel: 'Jenis',
							width:50,
							anchor: '95%',
							value:1,
							store: new Ext.data.ArrayStore
							(
								{
									id: 0,
									fields:
									[
										'Id',
										'displayText'
									],
								data: [[1, 'Diagnosa Awal'],[2, 'Diagnosa Utama'],[3, 'Komplikasi'],[4, 'Diagnosa Sekunder']]
								}
							),
						valueField: 'displayText',
						displayField: 'displayText',
						value:'',
						listeners:
						{
							
						}
					}
                ),
				
				
              
            },
            {
                id: 'colKasusDiagnosa',
                header: 'Kasus',
                width:130,
				//align: 'right',
				//hidden :true,
				menuDisabled:true,
                dataIndex: 'KASUS',
                editor: new Ext.form.ComboBox
                (
                    {
							id:'cboKasus',
							typeAhead: true,
							triggerAction: 'all',
							lazyRender:true,
							mode: 'local',
							selectOnFocus:true,
							forceSelection: true,
							emptyText:'Silahkan Pilih...',
							//fieldLabel: 'Jenis',
							width:50,
							anchor: '95%',
							value:1,
							store: new Ext.data.ArrayStore
							(
								{
									id: 0,
									fields:
									[
										'Id',
										'displayText'
									],
								data: [[1, 'Baru'],[2, 'Lama']]
								}
							),
						valueField: 'displayText',
						displayField: 'displayText',
						value:'',
						listeners:
						{
							
						}
					}
                ),
				
				
              
            }
			

        ]
    )
};

///---------------------------------------------------------------------------------------///
function form_histori(){
    var form = new Ext.form.FormPanel({
        baseCls: 'x-plain',
        labelWidth: 55,
        url:'save-form.php',
        defaultType: 'textfield',

        items: 
            [
                {
                    x:0,
                    y:60,
                    xtype: 'textarea',
                    id:'TxtHistoriDeleteDataPasien',
                    hideLabel: true,
                    name: 'msg',
                    anchor: '100% 100%'  // anchor width by percentage and height by raw adjustment
                }

            ]
    })};
function TambahBarisIGD()
{
    var x=true;

    if (x === true)
    {
        var p = RecordBaruRWJ();
        dsTRDetailKasirIGDList.insert(dsTRDetailKasirIGDList.getCount(), p);
    };
};

function HapusBarisIGD()
{
    if ( cellSelecteddeskripsi != undefined )
    {
        if (cellSelecteddeskripsi.data.DESKRIPSI2 != '' && cellSelecteddeskripsi.data.KD_PRODUK != '')
        {
		
           
                                          
											var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan penghapusan:', function(btn, combo){
											if (btn == 'ok')
														{
														variablehistori=combo;
														
														DataDeleteKasirIGDDetail();
													    dsTRDetailKasirIGDList.removeAt(CurrentKasirIGD.row);
														}
												});

												
                                         
                                
               
        }
        else
        {
            dsTRDetailKasirIGDList.removeAt(CurrentKasirIGD.row);
        };
    }
};

function DataDeleteKasirIGDDetail()
{
    Ext.Ajax.request
    (
        {
                //url: "./Datapool.mvc/DeleteDataObj",
            url: baseURL + "index.php/main/DeleteDataObj",
            params:  getParamDataDeleteKasirIGDDetail(),
            success: function(o)
            {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true)
                {
                    ShowPesanInfoIGD(nmPesanHapusSukses,nmHeaderHapusData);
                    dsTRDetailKasirIGDList.removeAt(CurrentKasirIGD.row);
                    cellSelecteddeskripsi=undefined;
                    RefreshDataKasirIGDDetail(Ext.get('txtNoTransaksiKasirIGD').dom.value);
                    AddNewKasirIGD = false;
                }
                else if (cst.success === false && cst.pesan === 0 )
                {
                    ShowPesanWarningIGD(nmPesanHapusGagal, nmHeaderHapusData);
                }
                else
                {
                    ShowPesanWarningIGD(nmPesanHapusError,nmHeaderHapusData);
                };
            }
        }
    )
};

function getParamDataDeleteKasirIGDDetail()
{
    var params =
    {
		Table: 'ViewTrKasirIGD',
        TrKodeTranskasi: CurrentKasirIGD.data.data.NO_TRANSAKSI,
		TrTglTransaksi:  CurrentKasirIGD.data.data.TGL_TRANSAKSI,
		TrKdPasien :	 CurrentKasirIGD.data.data.KD_PASIEN,
		TrKdNamaPasien : Ext.get('txtNamaPasienDetransaksi').getValue(),	
		TrKdUnit :		 Ext.get('txtKdUnitIGD').getValue(),
		TrNamaUnit :	 Ext.get('txtNamaUnit').getValue(),
		Uraian :		 CurrentKasirIGD.data.data.DESKRIPSI2,
		AlasanHapus : variablehistori,
		TrHarga :		 CurrentKasirIGD.data.data.HARGA,
		
		TrKdProduk :	 CurrentKasirIGD.data.data.KD_PRODUK,
        RowReq: CurrentKasirIGD.data.data.URUT,
        Hapus:2
    };
	
    return params
};




function GetDTLTRIGDGrid() 
{
    var fldDetail = ['KD_PRODUK','DESKRIPSI','DESKRIPSI2','KD_TARIF','HARGA','QTY','DESC_REQ','TGL_BERLAKU','NO_TRANSAKSI','URUT','DESC_STATUS','TGL_TRANSAKSI'];
	
    dsTRDetailKasirIGDList = new WebApp.DataStore({ fields: fldDetail })
   RefreshDataKasirIGDDetail() ;
    var gridDTLTRIGD = new Ext.grid.EditorGridPanel
    (
        {
            title: 'Detail Transaksi',
            stripeRows: true,
            store: dsTRDetailKasirIGDList,
            border: true,
            columnLines: true,
            frame: false,
            anchor: '100%',
             autoScroll:true,
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                            cellSelecteddeskripsi = dsTRDetailKasirIGDList.getAt(row);
                            CurrentKasirIGD.row = row;
                            CurrentKasirIGD.data = cellSelecteddeskripsi;
                           // FocusCtrlCMIGD='txtAset';
                        }
                    }
                }
            ),
            cm: TRRawatJalanColumModel()
              , viewConfig:{forceFit: true}
        }
		
		
    );
	
	

    return gridDTLTRIGD;
};

function TRRawatJalanColumModel() 
{
    return new Ext.grid.ColumnModel
    (
        [
            new Ext.grid.RowNumberer(),
            {
                id: 'coleskripsiIGD',
                header: 'Uraian',
                dataIndex: 'DESKRIPSI2',
                width:250,
				menuDisabled:true,
				hidden :true
                
            },
            {
                id: 'colKdProduk',
                header: 'Kode Produk',
                dataIndex: 'KD_PRODUK',
                width:100,
				menuDisabled:true,
                hidden:true
            },
            {
                id: 'colDeskripsiIGD',
                header:'Nama Produk',
                dataIndex: 'DESKRIPSI',
                sortable: false,
                hidden:false,
				menuDisabled:true,
                width:320,
				editor: new Ext.form.TextField
                (
                    {
                        id:'fieldAsetNameRequest',
                        allowBlank: false,
                        enableKeyEvents : true,
                        listeners:
                        {
                            'specialkey' : function()
                            {
                                if (Ext.EventObject.getKey() === 13)
                                {
                                    var str='';

                                   if (Ext.get('txtKdUnitIGD').dom.value  != undefined && Ext.get('txtKdUnitIGD').dom.value  != '')
									{
											//str = ' where kd_dokter =~' + Ext.get('txtKdDokterIGD').dom.value  + '~';
											//str = "\"kd_dokter\" = ~" + Ext.get('txtKdDokterIGD').dom.value  + "~";
											str =  Ext.get('txtKdUnitIGD').dom.value;
									};
									strb='';
									
									//alert(strb);
									if(Ext.get('fieldAsetNameRequest').dom.value != undefined || Ext.get('fieldAsetNameRequest').dom.value  != '')
									{
									strb= "and lower(deskripsi) like lower(~"+ Ext.get('fieldAsetNameRequest').dom.value+"%~)";
									}
									else
									{
									strb='';
									}
									//alert(strb);
									GetLookupAssetCMIGD(str,strb);
								  Ext.get('fieldAsetNameRequest').dom.value='';
                                };
                            }
                        }
                    }
                )
                
            }
			,
            {
               header: 'Tanggal Transaksi',
               dataIndex: 'TGL_TRANSAKSI',
               width: 130,
			   	menuDisabled:true,
               renderer: function(v, params, record)
                    {
                        
                        return ShowDate(record.data.TGL_TRANSAKSI);
                    }
            },
            {
                id: 'colHARGAIGD',
                header: 'Harga',
				align: 'right',
				hidden: true,
				menuDisabled:true,
                dataIndex: 'HARGA',
                width:100,
				renderer: function(v, params, record) 
							{
							return formatCurrency(record.data.HARGA);
							
							}	
            },
            {
                id: 'colProblemIGD',
                header: 'Qty',
                width:91,
				align: 'right',
				menuDisabled:true,
                dataIndex: 'QTY',
                editor: new Ext.form.TextField
                (
                    {
                        id:'fieldcolProblemIGD',
                        allowBlank: true,
                        enableKeyEvents : true,
                        width:30,
						listeners:
							{ 
								'specialkey' : function()
								{
									
											Dataupdate_KasirIGD(false);
											//RefreshDataFilterKasirIGD();
									        //RefreshDataFilterKasirIGD();

								}
							}
                    }
                ),
				
				
              
            },

            {
                id: 'colImpactIGD',
                header: 'CR',
                width:80,
                dataIndex: 'IMPACT',
				hidden: true,
                editor: new Ext.form.TextField
                (
                        {
                                id:'fieldcolImpactIGD',
                                allowBlank: true,
                                enableKeyEvents : true,
                                width:30
                        }
                )
				
            }

        ]
    )
};

function GetLookupAssetCMIGD(str,strb)
{
	if (AddNewKasirIGD === true)
	{
		var p = new mRecordRwj
		(
			{
				'DESKRIPSI2':'',
				'KD_PRODUK':'',
				'DESKRIPSI':'', 
				'KD_TARIF':'', 
				'HARGA':'',
				'QTY':dsTRDetailKasirIGDList.data.items[CurrentKasirIGD.row].data.QTY,
				'TGL_TRANSAKSI':dsTRDetailKasirIGDList.data.items[CurrentKasirIGD.row].data.TGL_TRANSAKSI,
				'DESC_REQ':dsTRDetailKasirIGDList.data.items[CurrentKasirIGD.row].data.DESC_REQ,
				'KD_TARIF':dsTRDetailKasirIGDList.data.items[CurrentKasirIGD.row].data.KD_TARIF,
				'URUT':''
			}
		);
		
		FormLookupKasir(str,strb,nilai_kd_tarif,dsTRDetailKasirIGDList,p,true,'',false,syssetting);
	}
	else
	{	
		var p = new mRecordRwj
		(
			{
				'DESKRIPSI2':'',
				'KD_PRODUK':'',
				'DESKRIPSI':'', 
				'KD_TARIF':'', 
				'HARGA':'',
				'QTY':dsTRDetailKasirIGDList.data.items[CurrentKasirIGD.row].data.QTY,
				'TGL_TRANSAKSI':dsTRDetailKasirIGDList.data.items[CurrentKasirIGD.row].data.TGL_TRANSAKSI,
				'DESC_REQ':dsTRDetailKasirIGDList.data.items[CurrentKasirIGD.row].data.DESC_REQ,
				'KD_TARIF':dsTRDetailKasirIGDList.data.items[CurrentKasirIGD.row].data.KD_TARIF,
				'URUT':dsTRDetailKasirIGDList.data.items[CurrentKasirIGD.row].data.URUT
			}
		);
	
		FormLookupKasir(str,strb,nilai_kd_tarif,dsTRDetailKasirIGDList,p,false,CurrentKasirIGD.row,false,syssetting);
	};
};

function RecordBaruRWJ()
{

	var p = new mRecordRwj
	(
		{
			'DESKRIPSI2':'',
			'KD_PRODUK':'',
		    'DESKRIPSI':'', 
		    'KD_TARIF':'', 
		    'HARGA':'',
		    'QTY':'',
		    'TGL_TRANSAKSI':tanggaltransaksitampung, 
		    'DESC_REQ':'',
		    'KD_TARIF':'',
		    'URUT':''
		}
	);
	
	return p;
};
function RefreshDataSetDiagnosa(medrec,unit,tgl)
{	
 var strKriteriaDiagnosa='';
    //strKriteriaDiagnosa = 'where no_transaksi = ~' + no_transaksi + '~'
    strKriteriaDiagnosa = 'kd_pasien = ~' + medrec + '~ and kd_unit=~'+unit+'~ and tgl_masuk in(~'+tgl+'~)';
    //strKriteriaDiagnosa = 'no_transaksi = ~0000004~';
	
	dsTRDetailDiagnosaList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: selectCountDiagnosa, 
				//Sort: 'EMP_ID',
                Sort: 'kd_penyakit',
				Sortdir: 'ASC', 
				target:'ViewDiagnosa',
				param: strKriteriaDiagnosa
			} 
		}
	);
	rowSelectedDiagnosa = undefined;
	return dsTRDetailDiagnosaList;
};

function TRIGDInit(rowdata)
{
    AddNewKasirIGD = false;
	
	Ext.get('txtNoTransaksiKasirIGD').dom.value = rowdata.NO_TRANSAKSI;
	tanggaltransaksitampung = rowdata.TANGGAL_TRANSAKSI;
    Ext.get('dtpTanggalDetransaksi').dom.value = ShowDate(rowdata.TANGGAL_TRANSAKSI);
	Ext.get('txtNoMedrecDetransaksi').dom.value= rowdata.KD_PASIEN;
	Ext.get('txtNamaPasienDetransaksi').dom.value = rowdata.NAMA;
	Ext.get('txtKdDokterIGD').dom.value   = rowdata.KD_DOKTER;
	Ext.get('txtNamaDokter').dom.value = rowdata.NAMA_DOKTER;
	Ext.get('txtKdUnitIGD').dom.value   = rowdata.KD_UNIT;
	Ext.get('txtNamaUnit').dom.value = rowdata.NAMA_UNIT;
	Ext.get('txtCustomer').dom.value = rowdata.CUSTOMER;
//	Ext.get('txtCustomerLama').dom.value=rowdata.CUSTOMER;
	Ext.get('txtKdUrutMasuk').dom.value = rowdata.URUT_MASUK;
	vkode_customer = rowdata.KD_CUSTOMER;
	RefreshDataKasirIGDDetail(rowdata.NO_TRANSAKSI);
	RefreshDataSetDiagnosa(rowdata.KD_PASIEN,rowdata.KD_UNIT,rowdata.TANGGAL_TRANSAKSI);
	Ext.Ajax.request(
	{
	    //url: "./home.mvc/getModule",
	    //url: baseURL + "index.php/main/getTrustee",
	    url: baseURL + "index.php/main/getcurrentshift",
		 params: {
	        //UserID: 'Admin',
	        command: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
	    },
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
	    success: function(o) {
			
		
	        //var cst = Ext.decode(o.responseText);
			tampungshiftsekarang=o.responseText
			//Ext.get('txtNilaiShift').dom.value =tampungshiftsekarang ;



	    }
	
	});
	Ext.Ajax.request(
	{
	    //url: "./home.mvc/getModule",
	    //url: baseURL + "index.php/main/getTrustee",
	    url: baseURL + "index.php/main/Getkdtarif",
		 params: {
	        //UserID: 'Admin',
	        customer: rowdata.KD_CUSTOMER,
			// parameter untuk url yang dituju (fungsi didalam controller)
	    },
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
	    success: function(o) {
			
		
	        //var cst = Ext.decode(o.responseText);
			nilai_kd_tarif=o.responseText;
			//Ext.get('txtNilaiShift').dom.value =tampungshiftsekarang ;



	    }
	
	});
	//,,
	
	
};

function mEnabledIGDCM(mBol)
{
//	 Ext.get('btnSimpanIGD').dom.disabled=mBol;
//	 Ext.get('btnSimpanKeluarIGD').dom.disabled=mBol;
//	 Ext.get('btnHapusIGD').dom.disabled=mBol;
	 Ext.get('btnLookupIGD').dom.disabled=mBol;
//	 Ext.get('btnTambahBrsIGD').dom.disabled=mBol;
	 Ext.get('btnHpsBrsIGD').dom.disabled=mBol;
};


///---------------------------------------------------------------------------------------///
function IGDAddNew() 
{
    AddNewKasirIGD = true;
	Ext.get('txtNoTransaksiKasirIGD').dom.value = '';
    Ext.get('dtpTanggalDetransaksi').dom.value = nowTglTransaksi.format('d/M/Y');
	Ext.get('txtNoMedrecDetransaksi').dom.value='';
	Ext.get('txtNamaPasienDetransaksi').dom.value = '';
	Ext.get('txtKdDokterIGD').dom.value   = undefined;
	Ext.get('txtNamaDokter').dom.value = '';
	Ext.get('txtKdUrutMasuk').dom.value = '';
	Ext.get('cboStatus_viKasirIgd').dom.value= ''
	rowSelectedKasirIGD=undefined;
	dsTRDetailKasirIGDList.removeAll();
	mEnabledIGDCM(false);
	

};

function RefreshDataKasirIGDDetail(no_transaksi) 
{
    var strKriteriaIGD='';
    //strKriteriaIGD = 'where no_transaksi = ~' + no_transaksi + '~'
    strKriteriaIGD = "\"no_transaksi\" = ~" + no_transaksi + "~ and kd_kasir=~06~";
    //strKriteriaIGD = 'no_transaksi = ~0000004~';
   
    dsTRDetailKasirIGDList.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'tgl_transaksi',
			    //Sort: 'tgl_transaksi',
			    Sortdir: 'ASC',
			    target: 'ViewDetailTRRWJ',
			    param: strKriteriaIGD
			}
		}
	);
    return dsTRDetailKasirIGDList;
};

///---------------------------------------------------------------------------------------///



///---------------------------------------------------------------------------------------///
function getParamDetailTransaksiIGD() 
{
    var params =
	{
		Table:'ViewTrKasirIGD',
		
		TrKodeTranskasi: Ext.get('txtNoTransaksiKasirIGD').getValue(),
		KdUnit: Ext.get('txtKdUnitIGD').getValue(),
		Tgl: Ext.get('dtpTanggalDetransaksi').dom.value,
		Shift: tampungshiftsekarang,
		List:getArrDetailTrIGD(),
		JmlField: mRecordRwj.prototype.fields.length-4,
		JmlList:GetListCountDetailTransaksi(),
		Hapus:1,
		Ubah:0
	};
    return params
};

function getParamKonsultasi() 
{

    var params =
	{
		
		Table:'ViewTrKasirIGD', //data access listnya belum dibuat
		
		TrKodeTranskasi: Ext.get('txtNoTransaksiKasirIGD').getValue(),
		KdUnitAsal : Ext.get('txtKdUnitIGD').getValue(),
		KdDokterAsal : Ext.get('txtKdDokterIGD').getValue(),
		KdUnit: selectKlinikPoli,
		KdDokter:selectDokter,
		KdPasien:Ext.get('txtNoMedrecDetransaksi').getValue(),
		TglTransaksi : Ext.get('dtpTanggalDetransaksi').dom.value,
		KDCustomer :vkode_customer,
	};
    return params
};

function GetListCountDetailTransaksi()
{
	
	var x=0;
	for(var i = 0 ; i < dsTRDetailKasirIGDList.getCount();i++)
	{
		if (dsTRDetailKasirIGDList.data.items[i].data.KD_PRODUK != '' || dsTRDetailKasirIGDList.data.items[i].data.DESKRIPSI  != '')
		{
			x += 1;
		};
	}
	return x;
	
};

function getTotalDetailProduk()
{
var TotalProduk=0;
	var x='';
	for(var i = 0 ; i < dsTRDetailKasirIGDList.getCount();i++)
	{		
			var recordterakhir;
			var y='';
			var z='@@##$$@@';
			
			
			recordterakhir=dsTRDetailKasirIGDList.data.items[i].data.DESC_REQ
			TotalProduk=TotalProduk+recordterakhir
			
			Ext.get('txtJumlah1EditData_viKasirIGD').dom.value=formatCurrency(TotalProduk);
			if (i === (dsTRDetailKasirIGDList.getCount()-1))
			{
				x += y 
			}
			else
			{
				x += y + '##[[]]##'
			};
		
	}	
	
	return x;
};





function getArrDetailTrIGD()
{
	var x='';
	for(var i = 0 ; i < dsTRDetailKasirIGDList.getCount();i++)
	{
		if (dsTRDetailKasirIGDList.data.items[i].data.KD_PRODUK != '' && dsTRDetailKasirIGDList.data.items[i].data.DESKRIPSI != '')
		{
			var y='';
			var z='@@##$$@@';
			
			y = 'URUT=' + dsTRDetailKasirIGDList.data.items[i].data.URUT
			y += z + dsTRDetailKasirIGDList.data.items[i].data.KD_PRODUK
			y += z + dsTRDetailKasirIGDList.data.items[i].data.QTY
			y += z + ShowDate(dsTRDetailKasirIGDList.data.items[i].data.TGL_BERLAKU)
			y += z +dsTRDetailKasirIGDList.data.items[i].data.HARGA
			y += z +dsTRDetailKasirIGDList.data.items[i].data.KD_TARIF
			y += z +dsTRDetailKasirIGDList.data.items[i].data.URUT
			
			
			if (i === (dsTRDetailKasirIGDList.getCount()-1))
			{
				x += y 
			}
			else
			{
				x += y + '##[[]]##'
			};
		};
	}	
	
	return x;
};


function getItemPanelInputIGD(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 0px',
		border:true,
		height:149,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
			    layout: 'form',
			    border: false,
			    items:
				[
					getItemPanelNoTransksiIGD(lebar),getItemPanelmedrec(lebar),getItemPanelUnit(lebar) ,getItemPanelDokter(lebar)			
				]
			}
		]
	};
    return items;
};



function getItemPanelUnit(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel:'Unit  ',
					    name: 'txtKdUnitIGD',
					    id: 'txtKdUnitIGD',
						//emptyText:nmNomorOtomatis,
						readOnly:true,
					    anchor: '99%'
					}
				]
			},
			{
			    columnWidth: .60,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[
					{
						xtype: 'textfield',
					   // fieldLabel:'Unit : ',
					    name: 'txtNamaUnit',
					    id: 'txtNamaUnit',
						//emptyText:nmNomorOtomatis,
						readOnly:true,
					    anchor: '100%'
					}
				]
			}
		]
	}
    return items;
};

function getItemPanelDokter(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel:'Dokter  ',
					    name: 'txtKdDokterIGD',
					    id: 'txtKdDokterIGD',
						//emptyText:nmNomorOtomatis,
						readOnly:true,
					    anchor: '99%'
					},{
					    xtype: 'textfield',
					    fieldLabel: 'Kelompok Pasien',
						//hideLabel:true,
						readOnly:true,
					    name: 'txtCustomer',
					    id: 'txtCustomer',
					    anchor: '99%',
						listeners: 
						{ 
							
						}
					}
				]
			},
			{
			    columnWidth: .600,
			    layout: 'form',
			    border: false,
				labelWidth:2,
			    items:
				[
					{
						xtype: 'textfield',
					   // fieldLabel:'Unit : ',
					    name: 'txtNamaDokter',
					    id: 'txtNamaDokter',
						//emptyText:nmNomorOtomatis,
						readOnly:true,
					    anchor: '100%'
					},
					{
						xtype: 'textfield',
					   // fieldLabel:'Unit : ',
					    name: 'txtKdUrutMasuk',
					    id: 'txtKdUrutMasuk',
						//emptyText:nmNomorOtomatis,
						readOnly:true,
						hidden:true,
					    anchor: '100%'
					}
				]
			}
		]
	}
    return items;
};

function getItemPanelNoTransksiIGD(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel:  'No. Transaksi ',
					    name: 'txtNoTransaksiKasirIGD',
					    id: 'txtNoTransaksiKasirIGD',
						emptyText:nmNomorOtomatis,
						readOnly:true,
					    anchor: '99%'
					}
				]
			},
			{
			    columnWidth: .60,
			    layout: 'form',
			    border: false,
				labelWidth:55,
			    items:
				[
					{
					    xtype: 'datefield',
					    fieldLabel: 'Tanggal ',
					    id: 'dtpTanggalDetransaksi',
					    name: 'dtpTanggalDetransaksi',
					    format: 'd/M/Y',
						readOnly : true,
					    value: now,
					    anchor: '100%'
					}
				]
			}
		]
	}
    return items;
};


function getItemPanelmedrec(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel:   'No. Medrec',
					    name: 'txtNoMedrecDetransaksi',
					    id: 'txtNoMedrecDetransaksi',
						readOnly:true,
					    anchor: '99%',
					    listeners: 
						{ 
							
						}
					}
				]
			},
			{
			    columnWidth: .60,
			    layout: 'form',
			    border: false,
				labelWidth:2,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: '',
						//hideLabel:true,
						readOnly:true,
					    name: 'txtNamaPasienDetransaksi',
					    id: 'txtNamaPasienDetransaksi',
					    anchor: '100%',
						listeners: 
						{ 
							
						}
					}
				]
			}
		]
	}
    return items;
};


function RefreshDataKasirIGD() 
{
    dsTRKasirIGDList.load
    (
        {
            params:
            {
                Skip: 0,
                Take: selectCountKasirIGD,
                Sort: 'tgl_transaksi',
                //Sort: 'tgl_transaksi',
                Sortdir: 'ASC',
                target:'ViewTrKasirIGD',
                param : ''
            }		
        }
    );
	
    rowSelectedKasirIGD = undefined;
    return dsTRKasirIGDList;
};
function refeshkasirIGD()
{
dsTRKasirIGDList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountKasirIGD, 
					//Sort: 'no_transaksi',
                     Sort: '',
					//Sort: 'no_transaksi',
					Sortdir: 'ASC', 
					target:'ViewTrKasirIGD',
					param : ''
				}			
			}
		);   
		return dsTRKasirIGDList;
}

function RefreshDataFilterKasirIGD() 
{

	var KataKunci='';
	
	 if (Ext.get('txtFilterIGDNomedrec').getValue() != '')
    {
		if (KataKunci == '')
		{
                        KataKunci = ' and   LOWER(kd_pasien) like  LOWER( ~' + Ext.get('txtFilterIGDNomedrec').getValue() + '%~)';
			
		}
		else
		{
		
                        KataKunci += ' and  LOWER(kd_pasien) like  LOWER( ~' + Ext.get('txtFilterIGDNomedrec').getValue() + '%~)';
		};

	};
	
	 if (Ext.get('TxtIgdFilternama').getValue() != '')
    {
		if (KataKunci == '')
		{
                        KataKunci = ' and   LOWER(nama) like  LOWER( ~' + Ext.get('TxtIgdFilternama').getValue() + '%~)';
			
		}
		else
		{
		
                        KataKunci += ' and  LOWER(nama) like  LOWER( ~' + Ext.get('TxtIgdFilternama').getValue() + '%~)';
		};

	};
	
	
	 if (Ext.get('cboUNIT_viKasirIGD').getValue() != '' && Ext.get('cboUNIT_viKasirIGD').getValue() != 'all')
    {
		if (KataKunci == '')
		{
	
                        KataKunci = ' and  LOWER(nama_unit)like  LOWER(~%' + Ext.get('cboUNIT_viKasirIGD').getValue() + '%~)';
		}
		else
		{
	
                        KataKunci += ' and LOWER(nama_unit) like  LOWER(~%' + Ext.get('cboUNIT_viKasirIGD').getValue() + '%~)';
		};
	};
	if (Ext.get('TxtIgdFilterDokter').getValue() != '')
		{
		if (KataKunci == '')
		{
			
                        KataKunci = ' and  LOWER(nama_dokter) like  LOWER(~' + Ext.get('TxtIgdFilterDokter').getValue() + '%~)';
		}
		else
		{
		
                        KataKunci += ' and LOWER(nama_dokter) like  LOWER(~' + Ext.get('TxtIgdFilterDokter').getValue() + '%~)';
		};
	};
		
		
	if (Ext.get('cboStatus_viKasirIgd').getValue() == 'Posting')
	{
		if (KataKunci == '')
		{

                        KataKunci = ' and  posting_transaksi = TRUE';
		}
		else
		{
		
                        KataKunci += ' and posting_transaksi =  TRUE';
		};
	
	};
		
		
		
	if (Ext.get('cboStatus_viKasirIgd').getValue() == 'Belum Posting')
	{
		if (KataKunci == '')
		{
		
                        KataKunci = ' and  posting_transaksi = FALSE';
		}
		else
		{
	
                        KataKunci += ' and posting_transaksi =  FALSE';
		};
		
		
	};
	
	if (Ext.get('cboStatus_viKasirIgd').getValue() == 'Semua')
	{
		if (KataKunci == '')
		{
		
                        KataKunci = ' and  (posting_transaksi = FALSE OR posting_transaksi = TRUE )';
		}
		else
		{
	
                        KataKunci += ' and (posting_transaksi = FALSE OR posting_transaksi = TRUE )';
		};
		
		
	};
	
		
	if (Ext.get('dtpTglAwalFilterIGD').getValue() != '')
	{
		if (KataKunci == '')
		{                      
						KataKunci = " and (tgl_transaksi between ~" + Ext.get('dtpTglAwalFilterIGD').getValue() + "~ and ~" + Ext.get('dtpTglAkhirFilterIGD').getValue() + "~)";
		}
		else
		{
			
                        KataKunci += " and (tgl_transaksi between ~" + Ext.get('dtpTglAwalFilterIGD').getValue() + "~ and ~" + Ext.get('dtpTglAkhirFilterIGD').getValue() + "~)";
		};
	
	};
	
     
    if (KataKunci != undefined ||KataKunci != '' ) 
    {  
		dsTRKasirIGDList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountKasirIGD, 
					//Sort: 'no_transaksi',
                                        Sort: 'tgl_transaksi',
					//Sort: 'no_transaksi',
					Sortdir: 'ASC', 
					target:'ViewTrKasirIGD',
					param : KataKunci
				}			
			}
		);   
    }
	else
	{
	
	dsTRKasirIGDList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountKasirIGD, 
					//Sort: 'no_transaksi',
                                        Sort: 'tgl_transaksi',
					//Sort: 'no_transaksi',
					Sortdir: 'ASC', 
					target:'ViewTrKasirIGD',
					param : ''
				}			
			}
		);   
	};
    
	return dsTRKasirIGDList;
};


function Datasave_Konsultasi(mBol) 
{	
	if (ValidasiEntryKonsultasi(nmHeaderSimpanData,false) == 1 )
	{
		
			Ext.Ajax.request
			 (
				{
					//url: "./Datapool.mvc/CreateDataObj",
					url: baseURL + "index.php/main/functionIGD/KonsultasiPenataJasa",					
					params: getParamKonsultasi(),
					failure: function(o)
					{
					ShowPesanWarningIGD('Konsultasi ulang gagal', 'Gagal');
					RefreshDataKasirIGDDetail(Ext.get('txtNoTransaksiKasirIGD').dom.value);
					},	
					success: function(o) 
					{
						RefreshDataKasirIGDDetail(Ext.get('txtNoTransaksiKasirIGD').dom.value);
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoIGD(nmPesanSimpanSukses,nmHeaderSimpanData);
							RefreshDataKasirIGD();
							if(mBol === false)
							{
								RefreshDataKasirIGDDetail(Ext.get('txtNoTransaksiKasirIGD').dom.value);
							};
						}
						else 
						{
								ShowPesanWarningIGD('Konsultasi ulang gagal', 'Gagal');
						
						};
					}
				}
			)
		
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
	
};



function Datasave_KasirIGD(mBol) 
{	
	if (ValidasiEntryCMIGD(nmHeaderSimpanData,false) == 1 )
	{
			Ext.Ajax.request
			 (
				{
			
					url: baseURL + "index.php/main/functionIGD/savedetailpenyakit",
					params: getParamDetailTransaksiIGD(),
					failure: function(o)
					{
					ShowPesanWarningIGD('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
					RefreshDataKasirIGDDetail(Ext.get('txtNoTransaksiKasirIGD').dom.value);
					},	
					success: function(o) 
					{
						RefreshDataKasirIGDDetail(Ext.get('txtNoTransaksiKasirIGD').dom.value);
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoIGD(nmPesanSimpanSukses,nmHeaderSimpanData);
							RefreshDataKasirIGD();
							if(mBol === false)
							{
								RefreshDataKasirIGDDetail(Ext.get('txtNoTransaksiKasirIGD').dom.value);
							};
						}
						else 
						{
								ShowPesanWarningIGD('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
						};
					}
				}
			)
		
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
	
};

function ValidasiEntryCMIGD(modul,mBolHapus)
{
	var x = 1;
	
	if((Ext.get('txtNoTransaksiKasirIGD').getValue() == '') || (Ext.get('txtNoMedrecDetransaksi').getValue() == '') || (Ext.get('txtNamaPasienDetransaksi').getValue() == '') || (Ext.get('txtNamaDokter').getValue() == '') || (Ext.get('dtpTanggalDetransaksi').getValue() == '') || dsTRDetailKasirIGDList.getCount() === 0 || (Ext.get('txtKdDokterIGD').dom.value  === undefined ))
	{
		if (Ext.get('txtNoTransaksiKasirIGD').getValue() == '' && mBolHapus === true) 
		{
			x = 0;
		}
		else if (Ext.get('txtNoMedrecDetransaksi').getValue() == '') 
		{
			ShowPesanWarningIGD(nmGetValidasiKosong('No. Medrec'), modul);
			x = 0;
		}
		else if (Ext.get('txtNamaPasienDetransaksi').getValue() == '') 
		{
			ShowPesanWarningIGD(nmGetValidasiKosong(nmRequesterRequest), modul);
			x = 0;
		}
		else if (Ext.get('dtpTanggalDetransaksi').getValue() == '') 
		{
			ShowPesanWarningIGD(nmGetValidasiKosong('Tanggal Kunjungan'), modul);
			x = 0;
		}
		else if (Ext.get('txtNamaDokter').getValue() == '' || Ext.get('txtKdDokterIGD').dom.value  === undefined) 
		{
			ShowPesanWarningIGD(nmGetValidasiKosong(nmDeptRequest), modul);
			x = 0;
		}
		else if (dsTRDetailKasirIGDList.getCount() === 0) 
		{
			ShowPesanWarningIGD(nmGetValidasiKosong(nmTitleDetailFormRequest),modul);
			x = 0;
		};
	};
	return x;
};

function ValidasiEntryKonsultasi(modul,mBolHapus)
{
	var x = 1;
	
	if((Ext.get('cboPoliklinikRequestEntry').getValue() == '') || (Ext.get('cboDokterRequestEntry').getValue() == '') )
	{
		if (Ext.get('cboPoliklinikRequestEntry').getValue() == '' && mBolHapus === true) 
		{
			x = 0;
		}
		else if (Ext.get('cboDokterRequestEntry').getValue() == '') 
		{
			ShowPesanWarningIGD(nmGetValidasiKosong('No. Medrec'), modul);
			x = 0;
		}
	};
	return x;
};


function ShowPesanWarningIGD(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorIGD(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoIGD(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};


function DataDeleteKasirIGD() 
{
   if (ValidasiEntryCMIGD(nmHeaderHapusData,true) == 1 )
    {
        Ext.Msg.show
        (
            {
               title:nmHeaderHapusData,
               msg: nmGetValidasiHapus(nmTitleFormRequest) ,
               buttons: Ext.MessageBox.YESNO,
               width:275,
               fn: function (btn)
               {
                    if (btn =='yes')
                    {
                        Ext.Ajax.request
                        (
                            {
                              
                                url: baseURL + "index.php/main/DeleteDataObj",
                                params: getParamDetailTransaksiIGD(),
                                success: function(o)
                                {
                                    var cst = Ext.decode(o.responseText);
                                    if (cst.success === true)
                                    {
                                        ShowPesanInfoIGD(nmPesanHapusSukses,nmHeaderHapusData);
                                        RefreshDataKasirIGD();
                                        IGDAddNew();
                                    }
                                    else if (cst.success === false && cst.pesan===0)
                                    {
                                        ShowPesanWarningIGD(nmPesanHapusGagal,nmHeaderHapusData);
                                    }
                                    else if (cst.success === false && cst.pesan===1)
                                    {
                                        ShowPesanWarningIGD(nmPesanHapusGagal + ' , ',nmHeaderHapusData);
                                    }
                                    else
                                    {
                                        ShowPesanErrorIGD(nmPesanHapusError,nmHeaderHapusData);
                                    };
                                }
                            }
                        )
                    };
                }
            }
        )
    };
};


function GantiDokterLookUp(mod_id) 
{
   
   

    var FormDepanDokter = new Ext.Panel
    (
        {
            id: mod_id,
            closable: true,
            region: 'center',
            layout: 'form',
            title: 'Ganti Dokter',
            border: false,
            shadhow: true,
            autoScroll:false,
            iconCls: 'Request',
            margins: '0 5 5 0',
            items: [DokterLookUp()],
            listeners:
            {
                'afterrender': function()
                {}
            }
        }
    );
	

	
   return FormDepanDokter

};









function DokterLookUp(rowdata) 
{
    var lebar = 350;
    FormLookUpGantidokter = new Ext.Window
    (
        {
            id: 'gridDokter',
            title: 'Ganti Dokter',
            closeAction: 'destroy',
            width: lebar,
            height: 180,
            border: false,
            resizable: false,
            plain: true,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryDokter(lebar),
            listeners:
            {
                activate: function()
                {
                     if (varBtnOkLookupEmp === true)
                    {
                        Ext.get('txtKdDokterIGD').dom.value   = rowSelectedLookdokter.data.KD_DOKTER;
                        Ext.get('txtNamaDokter').dom.value = rowSelectedLookdokter.data.NAMA_DOKTER;
                        varBtnOkLookupEmp=false;
                    };
                },
                afterShow: function()
                {
                    this.activate();
                },
                deactivate: function()
                {
                    rowSelectedKasirDokter=undefined;
               
                }
            }
        }
    );

    FormLookUpGantidokter.show();
 

};
function getItemPanelButtonGantidokter(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
		height:39,
		anchor:'100%',
		style:{'margin-top':'-1px'},
	    items:
		[
			{
				layout: 'hBox',
				width:310,
				border: false,
				bodyStyle: 'padding:5px 0px 5px 5px',
				defaults: { margins: '3 3 3 3' },
				anchor: '90%',
				layoutConfig: 
				{
					align: 'middle',
					pack:'end'
				},
				items:
				[
					{
						xtype:'button',
						text:'Simpan',
						width:100,
						style:{'margin-left':'0px','margin-top':'0px'},
						hideLabel:true,
						id: 'btnOkGantiDokter',
						handler:function()
						{
						GantiDokter(false);
						FormLookUpGantidokter.close();	
						}
					},
					{
							xtype:'button',
							text:'Tutup' ,
							width:70,
							hideLabel:true,
							id: 'btnCancelGantidokter',
							handler:function() 
							{
								FormLookUpGantidokter.close();
							}
					}
				]
			}
		]
	}
    return items;
};
function getFormEntryDokter(lebar) 
{
    var pnlTRGantiDokter = new Ext.FormPanel
    (
        {
            id: 'PanelTRDokter',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:190,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [getItemPanelInputGantidokter(lebar),getItemPanelButtonGantidokter(lebar)],
           tbar:
            [
               
               
            ]
        }
    );


   
    var FormDepanDokter = new Ext.Panel
	(
		{
		    id: 'FormDepanDokter',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [pnlTRGantiDokter
				

			]

		}
	);

    return FormDepanDokter
};


function getItemPanelInputGantidokter(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 0px',
		border:false,
		height:95,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
			    layout: 'form',
			    border: false,
			    items:
				[
					getItemPanelNoTransksiDokter(lebar)		
				]
			}
		]
	};
    return items;
};

function ValidasiEntryTutupDokter(modul,mBolHapus)
{
	var x = 1;
	if (Ext.get('txtNilaiDokter').getValue() == '' || (Ext.get('txtNilaiDokterSelanjutnya').getValue() == ''))
	{
		if (Ext.get('txtNilaiDokter').getValue() == '' && mBolHapus === true)
		{
			x=0;
		}
		else if (Ext.get('txtNilaiDokterSelanjutnya').getValue() === '')
		{
			x=0;
			if ( mBolHapus === false )
			{
				ShowPesanWarningTutupDokter(nmGetValidasiKosong(nmSatuan),modul);
			};

		};
	};


	return x;
};

function getItemPanelNoTransksiDokter(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: 1.0,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel:  'Unit Asal ',
					    name: 'cmbUnitAsal',
					    id: 'cmbUnitAsal',
						value:Ext.get('txtNamaUnit').getValue(),
						readOnly:true,
					    anchor: '100%'
					},
					
					{
					    xtype: 'textfield',
					    fieldLabel: 'Dokter Asal ',
					    name: 'cmbDokterAsal',
					    id: 'cmbDokterAsal',
						value:Ext.get('txtNamaDokter').getValue(),
						readOnly:true,
					    anchor: '100%'
						
					},
			
					mComboDokterGantiEntry()
				
					
				]
				
			
			}
			
			
		]
	}
    return items;
};


function mComboDokterGantiEntry()
{ 
	
	
    
 var Field = ['KD_DOKTER','NAMA'];

    dsDokterGantiEntry = new WebApp.DataStore({fields: Field});
	var kDUnit = Ext.get('txtKdUnitIGD').getValue();
	var kddokter = Ext.get('txtKdDokterIGD').getValue();
    dsDokterGantiEntry.load
                (
                    {
                     params:
							{
								Skip: 0,
								Take: 1000,
								Sort: 'nama',
								Sortdir: 'ASC',
								target: 'ViewComboDokter',
								param: 'where dk.kd_unit=~'+ kDUnit+ '~ and d.kd_dokter not in (~'+kddokter+'~)'
							}
                    }
                )

    var cboDokterGantiEntry = new Ext.form.ComboBox
	(
		{
		    id: 'cboDokterRequestEntry',
		    typeAhead: true,
		    triggerAction: 'all',
			name:'txtdokter',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
            forceSelection: true,
		    emptyText:'Pilih Dokter...',
		    fieldLabel: 'Dokter Baru',
		    align: 'Right',
             
		    store: dsDokterGantiEntry,
		    valueField: 'KD_DOKTER',
		    displayField: 'NAMA',
			anchor:'100%',
		    listeners:
			{
			    'select': function(a,b,c)
				{

									selectDokter = b.data.KD_DOKTER;
									NamaDokter = b.data.NAMA;
									 Ext.get('txtKdDokterIGD').dom.value = b.data.KD_DOKTER
                                },
                            'render': function(c)
                                {
                                    c.getEl().on('keypress', function(e) {
                                    if(e.getKey() == 13) 
                                    Ext.getCmp('kelPasien').focus();
                                    }, c);
                                }
			}
                }
	);
	
    return cboDokterGantiEntry;

};

function GantiDokter(mBol)
{
    if (ValidasiGantiDokter(nmHeaderSimpanData,false) == 1 )
    {
            
                    Ext.Ajax.request
                     (
                            {
                                   url: WebAppUrl.UrlUpdateData,
                                    params: getParamGantiDokter(),
									failure: function(o)
										{
										 ShowPesanErrorIGD('Ganti Dokter Gagal','Ganti Dokter');
										},	
                                    success: function(o)
                                    {
                                            var cst = Ext.decode(o.responseText);
                                            if (cst.success === true)
                                            {
                                                    ShowPesanInfoIGD(nmPesanSimpanSukses,'Ganti Dokter');
													Ext.get('txtKdDokterIGD').dom.value = selectDokter;
													Ext.get('txtNamaDokter').dom.value = NamaDokter;
													FormDepanDokter.close();
                                                    FormLookUpGantidokter.close();
                                            }
                                            else if  (cst.success === false && cst.pesan===0)
                                            {
                                                    ShowPesanErrorIGD('Ganti Dokter Gagal','Ganti Dokter');
                                            }
                                            else
                                            {
                                                    ShowPesanErrorIGD('Ganti Dokter Gagal','Ganti Dokter');
                                            };
                                    }
                            }
                    )
            
    }
    else
    {
            if(mBol === true)
            {
                    return false;
            };
    };

};

function ValidasiGantiDokter(modul,mBolHapus)
{
	var x = 1;
	if ((Ext.get('cboDokterRequestEntry').getValue() == ''))
	{
	  if (Ext.get('cboDokterRequestEntry').getValue() === '')
		{
			x=0;
			if ( mBolHapus === false )
			{
				ShowPesanWarningTutupDokter(nmGetValidasiKosong(nmSatuan),modul);
			};

		};
	};


	return x;
};


function getParamGantiDokter()
{
    var params =
	{
        Table: 'ViewGantiDokter',
		TxtMedRec : Ext.get('txtNoMedrecDetransaksi').getValue(),
		TxtTanggal:Ext.get('dtpTanggalDetransaksi').getValue(),
		KdUnit :  Ext.get('txtKdUnitIGD').getValue(),
		KdDokter : selectDokter,
		kodebagian : 2
		
	};
    return params
};



function KelompokPasienLookUp(rowdata) 
{
    var lebar = 440;
    FormLookUpsdetailTRKelompokPasien = new Ext.Window
    (
        {
            id: 'gridKelompokPasien',
            title: 'Ganti Kelompok Pasien',
            closeAction: 'destroy',
            width: lebar,
            height: 260,
            border: false,
            resizable: false,
            plain: false,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryTRKelompokPasien(lebar),
            listeners:
            {
                
            }
        }
    );

    FormLookUpsdetailTRKelompokPasien.show();
    KelompokPasienbaru();

};


function getFormEntryTRKelompokPasien(lebar) 
{
    var pnlTRKelompokPasien = new Ext.FormPanel
    (
        {
            id: 'PanelTRKelompokPasien',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:250,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [getItemPanelInputKelompokPasien(lebar),getItemPanelButtonKelompokPasien(lebar)],
           tbar:
            [
               
               
            ]
        }
    );
 
    var FormDepanKelompokPasien = new Ext.Panel
	(
		{
		    id: 'FormDepanKelompokPasien',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [pnlTRKelompokPasien	
				
			]

		}
	);

    return FormDepanKelompokPasien
};

function getItemPanelInputKelompokPasien(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 0px',
		border:false,
		height:170,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
			    layout: 'form',
			    border: false,
			    items:
				[
					getKelompokpasienlama(lebar),	getItemPanelNoTransksiKelompokPasien(lebar)	,
					
				]
			}
		]
	};
    return items;
};




function getItemPanelButtonKelompokPasien(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
		height:30,
		anchor:'100%',
		style:{'margin-top':'-1px'},
	    items:
		[
			{
				layout: 'hBox',
				width:400,
				border: false,
				bodyStyle: 'padding:5px 0px 5px 5px',
				defaults: { margins: '3 3 3 3' },
				anchor: '90%',
				layoutConfig: 
				{
					align: 'middle',
					pack:'end'
				},
				items:
				[
					{
						xtype:'button',
						text:'Simpan',
						width:70,
						style:{'margin-left':'0px','margin-top':'0px'},
						hideLabel:true,
						id: 'btnOkKelompokPasien',
						handler:function()
						{
					
					Datasave_Kelompokpasien();
					FormLookUpsdetailTRKelompokPasien.close();
							
						}
					},
					{
							xtype:'button',
							text:'Tutup',
							width:70,
							hideLabel:true,
							id: 'btnCancelKelompokPasien',
							handler:function() 
							{
								FormLookUpsdetailTRKelompokPasien.close();
							}
					}
				]
			}
		]
	}
    return items;
};

function KelompokPasienbaru() 
{
	jeniscus=0;
    KelompokPasienAddNew = true;
    Ext.getCmp('cboKelompokpasien').show()
	Ext.getCmp('txtNoSJP').disable();
	Ext.getCmp('txtNoAskes').disable();
	RefreshDatacombo(jeniscus);
	Ext.get('txtCustomerLama').dom.value=	Ext.get('txtCustomer').dom.value

	

};

function RefreshDatacombo(jeniscus) 
{

    ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: 'jenis_cust=~'+ jeniscus +'~ and kontraktor.kd_customer not in(~'+ vkode_customer+'~)'
            }
        }
    )
	
    return ds_customer_viDaftar;
};
function mComboKelompokpasien()
{

var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});
	
	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: 'jenis_cust=~'+ jeniscus +'~'
            }
        }
    )
    var cboKelompokpasien = new Ext.form.ComboBox
	(
		{
			id:'cboKelompokpasien',
			typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Pilih...',
                        fieldLabel: labelisi,
                        align: 'Right',
                        anchor: '95%',
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetKelompokpasien=b.data.displayText ;
					selectKdCustomer=b.data.KD_CUSTOMER;
					selectNamaCustomer=b.data.CUSTOMER;
				
				}
			}
		}
	);
	return cboKelompokpasien;
};

function getKelompokpasienlama(lebar) 
{
    var items =
	{
		Width:lebar,
		height:40,
	    layout: 'column',
	    border: false,
		
	    items:
		[
			{
			    columnWidth: .990,
			    layout: 'form',
				Width:lebar-10,
				labelWidth:130,
			    border: true,
			    items:
				[
					
					
						{	 
															xtype: 'tbspacer',
														
															height: 2
						},
									{
                                        xtype: 'textfield',
                                        fieldLabel: 'Kelompok Pasien Asal',
                                        name: 'txtCustomerLama',
                                        id: 'txtCustomerLama',
										labelWidth:130,
                                        width: 100,
                                        anchor: '95%'
                                     },
					
					
				]
			}
			
		]
	}
    return items;
};


function getItemPanelNoTransksiKelompokPasien(lebar) 
{
    var items =
	{
		Width:lebar,
		height:120,
	    layout: 'column',
	    border: false,
		
		
	    items:
		[
			{

			    columnWidth: .990,
			    layout: 'form',
				Width:lebar-10,
				labelWidth:130,
			    border: true,
			    items:
				[
					
					
														{	 
															xtype: 'tbspacer',
															
															height:3
														},
									{  

                                    xtype: 'combo',
                                    fieldLabel: 'Kelompok Pasien Baru',
                                    id: 'kelPasien',
                                    editable: false,
                                    store: new Ext.data.ArrayStore
                                        (
                                            {
                                            id: 0,
                                            fields:
                                            [
											'Id',
											'displayText'
                                            ],
                                               data: [[1, 'Perseorangan'], [2, 'Perusahaan'], [3, 'Asuransi']]
                                            }
                                        ),
										  displayField: 'displayText',
										  mode: 'local',
										  width: 100,
										  forceSelection: true,
										  triggerAction: 'all',
										  emptyText: 'Pilih Salah Satu...',
										  selectOnFocus: true,
										  anchor: '95%',
										  listeners:
											 {
													'select': function(a, b, c)
												{
												if(b.data.displayText =='Perseorangan')
												{jeniscus='0'
												Ext.getCmp('txtNoSJP').disable();
												Ext.getCmp('txtNoAskes').disable();}
												else if(b.data.displayText =='Perusahaan')
												{jeniscus='1';
												Ext.getCmp('txtNoSJP').disable();
												Ext.getCmp('txtNoAskes').disable();}
												else if(b.data.displayText =='Asuransi')
												{jeniscus='2';
												Ext.getCmp('txtNoSJP').enable();
												Ext.getCmp('txtNoAskes').enable();
											}
												
												RefreshDatacombo(jeniscus);
												}

											}
                                  }, 
								  {
										columnWidth: .990,
										layout: 'form',
										border: false,
										labelWidth:130,
										items:
										[
															mComboKelompokpasien()
										]
									},
									{
                                        xtype: 'textfield',
                                        fieldLabel: 'No. SJP',
                                        maxLength: 200,
                                        name: 'txtNoSJP',
                                        id: 'txtNoSJP',
                                        width: 100,
                                        anchor: '95%'
                                     },
									  {
                                        xtype: 'textfield',
                                        fieldLabel: 'No. Askes',
                                        maxLength: 200,
                                        name: 'txtNoAskes',
                                        id: 'txtNoAskes',
                                        width: 100,
                                        anchor: '95%'
                                     }
									
				]
			}
			
		]
	}
    return items;
};

function Datasave_Kelompokpasien(mBol) 
{	
	if (ValidasiEntryUpdateKelompokPasien(nmHeaderSimpanData,false) == 1 )
	{
		
		
			Ext.Ajax.request
			 (
				{
					
					url: baseURL +  "index.php/main/functionIGD/UpdateKdCustomer",	
					params: getParamKelompokpasien(),
					failure: function(o)
					{
					ShowPesanWarningIGD('Simpan kelompok pasien gagal', 'Gagal');
					RefreshDataKasirIGDDetail(Ext.get('txtNoTransaksiKasirIGD').dom.value);
					},	
					success: function(o) 
					{
						RefreshDataKasirIGDDetail(Ext.get('txtNoTransaksiKasirIGD').dom.value);
						Ext.get('txtCustomer').dom.value = selectNamaCustomer;
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoIGD(nmPesanSimpanSukses,nmHeaderSimpanData);
							RefreshDataKasirIGD();
							if(mBol === false)
							{
								RefreshDataKasirIGDDetail(Ext.get('txtNoTransaksiKasirIGD').dom.value);
							};
						}

						else 
						{
								ShowPesanWarningIGD('Simpan kelompok pasien gagal', 'Gagal');
						};
					}
				}
			)
		
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
	
};


function getParamKelompokpasien() 
{
	
    var params =
	{
		
		Table:'ViewTrKasirIGD', 
		TrKodePasien : Ext.get('txtNoMedrecDetransaksi').getValue(),
		TrKodeTranskasi: Ext.get('txtNoTransaksiKasirIGD').getValue(),
		KdUnit: Ext.get('txtKdUnitIGD').getValue(),
		KdDokter:Ext.get('txtKdDokterIGD').dom.value ,
		TglTransaksi : Ext.get('dtpTanggalDetransaksi').dom.value,
		KDCustomer:selectKdCustomer,
		TglTransaksi : Ext.get('dtpTanggalDetransaksi').dom.value,
		KDNoSJP :Ext.get('txtNoSJP').dom.value,
		KDNoAskes :Ext.get('txtNoAskes').dom.value
		
		
	};
    return params
};

function ValidasiEntryUpdateKelompokPasien(modul,mBolHapus)
{
	var x = 1;
	
	if((Ext.get('kelPasien').getValue() == '') || (Ext.get('kelPasien').dom.value  === undefined ))
	{
		if (Ext.get('kelPasien').getValue() == '' && mBolHapus === true) 
		{
			ShowPesanWarningIGD(nmGetValidasiKosong('Kelompok Pasien'), modul);
			x = 0;
		}
	};
	return x;
};



function setpostingtransaksi(notransaksi) 
{

		Ext.Msg.show
		(
			{
			   title:'Posting',
			   msg: 'Kirim Data Transaksi ini Ke Kasir ? ' ,
			   buttons: Ext.MessageBox.YESNO,
			   width:250,
			   fn: function (btn) 
			   {			
					if (btn === 'yes') 
					{
						Ext.Ajax.request
						(
							{
								
								url : baseURL + "index.php/main/posting",
								params: 
								{
								_notransaksi : 	notransaksi,
								},
								success: function(o) 
								{
									var cst = Ext.decode(o.responseText);
									if (cst.success === true) 
									{
									
										RefreshDataFilterKasirIGD();
										ShowPesanInfoDiagnosa('Posting Berhasil Dilakukan','Posting');
										
										FormLookUpsdetailTRIGD.close();
										
										
									}
									else if (cst.success === false && cst.pesan===0)
									{
										ShowPesanWarningDiagnosa(nmPesanHapusGagal,'Posting');
									}
									else 
									{
										ShowPesanWarningDiagnosa(nmPesanHapusError,'Posting');
									};
								}
							}
						)
					};
				}
			}
		)
	};
	



function setdisablebutton()
{

Ext.getCmp('btnposting').disable();	

Ext.getCmp('btnLookupIGD').disable()
Ext.getCmp('btnSimpanIGD').disable()
Ext.getCmp('btnHpsBrsIGD').disable()
Ext.getCmp('btnHpsBrsDiagnosa').disable()
Ext.getCmp('btnSimpanDiagnosa').disable()
Ext.getCmp('btnLookupDiagnosa').disable()
}

function setenablebutton()
{

Ext.getCmp('btnposting').enable();	

Ext.getCmp('btnLookupIGD').enable()
Ext.getCmp('btnSimpanIGD').enable()
Ext.getCmp('btnHpsBrsIGD').enable()
Ext.getCmp('btnHpsBrsDiagnosa').enable()
Ext.getCmp('btnSimpanDiagnosa').enable()
Ext.getCmp('btnLookupDiagnosa').enable()	
}
