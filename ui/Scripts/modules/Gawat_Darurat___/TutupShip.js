var CurrentKasirShift =
{
    data: Object,
    details: Array,
    row: 0
};

var tampungshiftsekarang;
var tampungshiftnanti;
var dsTRKasirShiftList;
var dsTRDetailKasirShiftList;
var AddNewKasirShift = true;
var selectCountKasirShift = 50;
var now = new Date();
var rowSelectedKasirShift;
var cellSelecteddeskripsi;
var FormLookUpsdetailTRShift;
var valueStatusCMShiftView='All';
var nowTglTransaksi = new Date();
//var FocusCtrlCMShift;

CurrentPage.page = getPanelShift(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelShift(mod_id) 
{
   
   

    var FormDepanShift = new Ext.Panel
    (
        {
            id: mod_id,
            closable: true,
            region: 'center',
            layout: 'form',
            title: 'Kasir Shift',
            border: false,
            shadhow: true,
            autoScroll:false,
            iconCls: 'Request',
            margins: '0 5 5 0',
            items: [ShiftLookUp()],
            listeners:
            {
                'afterrender': function()
                {}
            }
        }
    );
	

	
   return FormDepanShift

};









function ShiftLookUp(rowdata) 
{
    var lebar = 350;
    FormLookUpsdetailTRShift = new Ext.Window
    (
        {
            id: 'gridShift',
            title: 'Tutup Shift',
            closeAction: 'destroy',
            width: lebar,
            height: 260,
            border: false,
            resizable: false,
            plain: true,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryTRShift(lebar),
            listeners:
            {
                activate: function()
                {
                     if (varBtnOkLookupEmp === true)
                    {
                        Ext.get('txtKdDokter').dom.value   = rowSelectedLookdokter.data.KD_DOKTER;
                        Ext.get('txtNamaDokter').dom.value = rowSelectedLookdokter.data.NAMA_DOKTER;
                        varBtnOkLookupEmp=false;
                    };
                },
                afterShow: function()
                {
                    this.activate();
                },
                deactivate: function()
                {
                    rowSelectedKasirShift=undefined;
                    //RefreshDataFilterKasirShift();
                }
            }
        }
    );

    FormLookUpsdetailTRShift.show();
    if (rowdata == undefined) 
	{
        ShiftAddNew();
    }
    else 
	{
        
    }

};
function getItemPanelButtonshift(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
		height:39,
		anchor:'100%',
		style:{'margin-top':'-1px'},
	    items:
		[
			{
				layout: 'hBox',
				width:222,
				border: false,
				bodyStyle: 'padding:5px 0px 5px 5px',
				defaults: { margins: '3 3 3 3' },
				anchor: '90%',
				layoutConfig: 
				{
					align: 'middle',
					pack:'end'
				},
				items:
				[
					{
						xtype:'button',
						text:nmBtnOK,
						width:70,
						style:{'margin-left':'0px','margin-top':'0px'},
						hideLabel:true,
						id: 'btnOkShift',
						handler:function()
						{
					TutupShiftSave(false);
							
						}
					},
					{
							xtype:'button',
							text:nmBtnCancel ,
							width:70,
							hideLabel:true,
							id: 'btnCancelShift',
							handler:function() 
							{
								FormLookUpsdetailTRShift.close();
							}
					}
				]
			}
		]
	}
    return items;
};
function getFormEntryTRShift(lebar) 
{
    var pnlTRShift = new Ext.FormPanel
    (
        {
            id: 'PanelTRShift',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
             height:300,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [getItemPanelInputShift(lebar),getItemPanelButtonshift(lebar)],
           tbar:
            [
               
               
            ]
        }
    );


	
   
   
  
   
   
   
   
    var FormDepanShift = new Ext.Panel
	(
		{
		    id: 'FormDepanShift',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [pnlTRShift	
				
			]

		}
	);

    return FormDepanShift
};
///---------------------------------------------------------------------------------------///





///---------------------------------------------------------------------------------------///
function ShiftAddNew() 
{
    AddNewKasirShift = true;
	
	
	
	       // ajax digunakan untuk meminta hak akses dari server untuk modul yg di request
		   // ajax request untuk mengambil data current shift	
        Ext.Ajax.request(
	{
	    //url: "./home.mvc/getModule",
	    //url: baseURL + "index.php/main/getTrustee",
	    url: baseURL + "index.php/main/getcurrentshiftIGD",
		 params: {
	        //UserID: 'Admin',
	        command: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
	    },
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
	    success: function(o) {
			
			//alert('ada');    	
	        //var cst = Ext.decode(o.responseText);
			tampungshiftsekarang=o.responseText
			Ext.get('txtNilaiShift').dom.value =tampungshiftsekarang ;



	    }
	
	});
	
	
	/*var tamp1=2;
    Ext.get('dtpTanggalShift').dom.value = nowTglTransaksi.format('d/M/Y');
	if (Ext.get('txtNilaiShift').dom.value <2)
	{

	Ext.get('txtNilaiShiftSelanjutnya').dom.value = 	(tamp1+ 1);
	}
	else
	{
	Ext.get('txtNilaiShiftSelanjutnya').dom.value = 1
	}*/
	
	 // ajax request untuk mengambil data max shift	
	 Ext.Ajax.request(
	{ 

	    
	    url: baseURL + "index.php/main/getMaxkdbagianIGD",
		 params: {
	       
	        command: '0',
		
	    },
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
	    success: function(o) {
		tampungshiftnanti= o.responseText;
		if (tampungshiftsekarang <tampungshiftnanti)
		{
			Ext.get('txtNilaiShiftSelanjutnya').dom.value =parseFloat(tampungshiftsekarang)+1;
		}
		else{
		Ext.get('txtNilaiShiftSelanjutnya').dom.value =1;
		}

	    }
		});

};



///---------------------------------------------------------------------------------------///


function getItemPanelInputShift(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 0px',
		border:false,
		height:120,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
			    layout: 'form',
			    border: false,
			    items:
				[
					getItemPanelNoTransksiShift(lebar)		
				]
			}
		]
	};
    return items;
};

function ValidasiEntryTutupShift(modul,mBolHapus)
{
	var x = 1;
	if (Ext.get('txtNilaiShift').getValue() == '' || (Ext.get('txtNilaiShiftSelanjutnya').getValue() == ''))
	{
		if (Ext.get('txtNilaiShift').getValue() == '' && mBolHapus === true)
		{
			x=0;
		}
		else if (Ext.get('txtNilaiShiftSelanjutnya').getValue() === '')
		{
			x=0;
			if ( mBolHapus === false )
			{
				ShowPesanWarningTutupShift(nmGetValidasiKosong(nmSatuan),modul);
			};

		};
	};


	return x;
};

function getItemPanelNoTransksiShift(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .70,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'numberfield',
					    fieldLabel:  'Shift Ke',
					    name: 'txtNilaiShift',
					    id: 'txtNilaiShift',
					    anchor: '100%'
					},
					
					{
					    xtype: 'textfield',
					    fieldLabel: 'Shift Selanjutnya',
					    name: 'txtNilaiShiftSelanjutnya',
					    id: 'txtNilaiShiftSelanjutnya',
					    anchor: '100%'
						
					},
					{
					    xtype: 'datefield',
					    fieldLabel: 'Tgl Shift ',
					    id: 'dtpTanggalShift',
					    name: 'dtpTanggalShift',
					    format: 'd/M/Y',
					    value: now,
					    anchor: '100%'
					
				
					}
					
				]
			},
			
		]
	}
    return items;
};


function getItemPanelmedrec(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel:   ' No. Medrec',
					    name: 'txtNoMedrecDetransaksi',
					    id: 'txtNoMedrecDetransaksi',
						readOnly:true,
					    anchor: '99%',
					    listeners: 
						{ 
							
						}
					}
				]
			},
			{
			    columnWidth: .40,
			    layout: 'form',
			    border: false,
				labelWidth:2,
			    items:
				[
					
				]
			}
		]
	}
    return items;
};






function ValidasiEntryTutupShift(modul,mBolHapus)
{
	var x = 1;
	
	if (Ext.get('txtNilaiShift').getValue() == '' || (Ext.get('txtNilaiShiftSelanjutnya').getValue() == ''))
	{
		if (Ext.get('txtNilaiShift').getValue() == '' && mBolHapus === true)
		{
			x=0;
		}
		else if (Ext.get('txtNilaiShiftSelanjutnya').getValue() === '')
		{
			x=0;
			if ( mBolHapus === false )
			{
				ShowPesanWarningTutupShift(nmGetValidasiKosong(nmSatuan),modul);
			};

		};
	};


	return x;
};

function TutupShiftSave(mBol)
{
    if (ValidasiEntryTutupShift(nmHeaderSimpanData,false) == 1 )
    {
            
                    Ext.Ajax.request
                     (
                            {
                                    url: WebAppUrl.UrlUpdateData,
                                    params: getParamTutupShift(),
                                    success: function(o)
                                    {
                                            var cst = Ext.decode(o.responseText);
                                            if (cst.success === true)
                                            {
                                                    ShowPesanInfoShift('Simpan Shift Berhasil','Simpan Shift');
                                                    FormLookUpsdetailTRShift.close();
                                            }
                                            else if  (cst.success === false && cst.pesan===0)
                                            {
                                                    ShowPesanErrorShift('Simpan Shift Gagal','Simpan Shift');
                                            }
                                            else
                                            {
                                                    ShowPesanErrorShift('Simpan Shift Gagal','Simpan Shift');
                                            };
                                    }
                            }
                    )
            
    }
    else
    {
            if(mBol === true)
            {
                    return false;
            };
    };

};//END FUNCTION TutupShiftSave
///---------------------------------------------------------------------------------------///
function getParamTutupShift()
{
    var params =
	{
        Table: 'ViewTutupShipIGD',
	    Shfit: Ext.get('txtNilaiShift').getValue(),
		ShiftBerikutnya :Ext.get('txtNilaiShiftSelanjutnya').getValue(),
		tglshift :Ext.get('dtpTanggalShift').dom.value,
		kodebagian : 3
		
	};
    return params
};

function ShowPesanWarningShift(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorShift(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoShift(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};










