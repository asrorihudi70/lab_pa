var Field = ['kode','nama'];
var dialog_update_sep = {};
dialog_update_sep.combobox  = {};
dialog_update_sep.datastore = {};
dialog_update_sep.params = {};
dialog_update_sep.form = {};
dialog_update_sep.form.dialog;

dialog_update_sep.combobox.provinsi;
dialog_update_sep.combobox.kabupaten;
dialog_update_sep.combobox.kecamatan;

dialog_update_sep.datastore.kabupaten = new WebApp.DataStore({fields: Field});
dialog_update_sep.datastore.provinsi  = new WebApp.DataStore({fields: Field});
dialog_update_sep.datastore.kecamatan = new WebApp.DataStore({fields: Field});
dialog_update_sep.datastore.diagnosa  = new WebApp.DataStore({fields: Field});

dialog_update_sep.params.kd_pasien;
dialog_update_sep.params.kd_unit;
dialog_update_sep.params.tgl_masuk;
dialog_update_sep.params.urut_masuk;
dialog_update_sep.params.nomor_rujukan;
dialog_update_sep.params.kd_dokter_dpjp;
dialog_update_sep.params.kd_rujukan;
dialog_update_sep.params.no_kartu;
dialog_update_sep.params.kelas_rawat;
dialog_update_sep.params.tgl_rujukan;
dialog_update_sep.params.no_rujukan;
dialog_update_sep.params.ppk_rujukan;
dialog_update_sep.params.catatan;
dialog_update_sep.params.diag_awal;
dialog_update_sep.params.poli_tujuan;
dialog_update_sep.params.eksekutif;
dialog_update_sep.params.tgl_kejadian;
dialog_update_sep.params.keterangan_kejadian;
dialog_update_sep.params.kd_prov;
dialog_update_sep.params.kd_kab;
dialog_update_sep.params.kd_kec;
dialog_update_sep.params.no_surat_dpjp;
dialog_update_sep.params.no_tlp;
dialog_update_sep.params.cob;
dialog_update_sep.params.katarak;
dialog_update_sep.params.lakalantas;
dialog_update_sep.params.suplesi;
dialog_update_sep.params.kd_dpjp;
dialog_update_sep.params.no_suplesi;
dialog_update_sep.params.penjamin_1;
dialog_update_sep.params.penjamin_2;
dialog_update_sep.params.penjamin_3;
dialog_update_sep.params.penjamin_4;
dialog_update_sep.params.no_sep;
dialog_update_sep.params.kd_pasien;
dialog_update_sep.params.kd_unit;
dialog_update_sep.params.tgl_masuk;
dialog_update_sep.params.urut_masuk;
dialog_update_sep.params.asal_rujukan;

function dialog_update_sep_modal(MODULE){
	dialog_update_sep.form.dialog = new Ext.Window({
		id: 'formLookup_rujukan_bpjs_RWJrujukan_bpjs'+MODULE,
		name: 'formLookup_rujukan_bpjs_RWJrujukan_bpjs'+MODULE,
		title: 'Form Update SEP',
		closeAction: 'destroy',
		width: 650,
		height: 430, //575,
		resizable: false,
		constrain: true,
		autoScroll: false,
		iconCls: 'Studi_Lanjut',
		modal: true,
		items: panel_form_lookup_rujukan_ALL_update(MODULE), //1
		listeners:{
			afterShow: function ()
			{
				this.activate();

			}
		}
	});
	dialog_update_sep.form.dialog.show();
}
function init_data(no_kartu, data_sep, data_pasien, MODULE){
	Ext.Ajax.request({
	url: baseURL + "index.php/rawat_jalan/functionRWJ/cari_history_sep",
		params: {no_kartu   :no_kartu,
				 no_sep 	:data_sep.response.noSep },
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);
			console.log(data_pasien);
			console.log(data_sep);
			Ext.getCmp('txt_no_kartu_peserta_update'+MODULE).setValue(cst.listData[0].no_kartu);
			Ext.getCmp('txt_no_tlp_update'+MODULE).setValue(cst.listData[0].no_tlp);
			Ext.getCmp('txt_nama_peserta_update'+MODULE).setValue(data_pasien.data.NAMA);
			Ext.getCmp('txt_no_medrec_update'+MODULE).setValue(data_pasien.data.KD_PASIEN);
			Ext.getCmp('txt_poli_update'+MODULE).setValue(data_sep.response.poli);
			Ext.getCmp('txt_nama_diagnosa_update'+MODULE).setValue(cst.listData[0].penyakit);
			Ext.getCmp('txt_no_catatan_update'+MODULE).setValue(cst.listData[0].catatan);
			Ext.getCmp('txt_ket_kejadian_update'+MODULE).setValue(cst.listData[0].keterangan_kejadian);
			Ext.getCmp('txt_no_sep_update'+MODULE).setValue(data_sep.response.noSep);
			Ext.getCmp('txt_hak_kelas_peserta_update'+MODULE).setValue(data_sep.response.peserta.hakKelas);
			Ext.getCmp('tgl_kejadian_update'+MODULE).setValue(cst.listData[0].tgl_kejadian);
			Ext.getCmp('tgl_SEP_update'+MODULE).setValue(cst.listData[0].tgl_rujukan);
			// CHECK KATARAK
			if (cst.listData[0].katarak=="1" || cst.listData[0].katarak==1) {
				Ext.getCmp('cbo_katarak_update'+MODULE).setValue(true);
			}

			// CHECK COB
			if (cst.listData[0].cob=="1" || cst.listData[0].cob==1) {
				Ext.getCmp('cbo_peserta_cob_update'+MODULE).setValue(true);
			}

			// CHECK LAKALANTAS
			if (cst.listData[0].lakalantas=="1" || cst.listData[0].lakalantas==1) {
				Ext.getCmp('penjamin_kkl_update'+MODULE).setValue(true);
			}

			// CHECK SUPLESI
			if (cst.listData[0].suplesi=="1" || cst.listData[0].suplesi==1) {
				Ext.getCmp('cbo_suplesi_update'+MODULE).setValue(true);
				Ext.getCmp('txt_no_sep_suplesi_update'+MODULE).setValue(cst.listData[0].no_suplesi);
			}else{
				Ext.getCmp('txt_no_sep_suplesi_update'+MODULE).setValue("");
			}

			// CHECK LAKALANTAS RAHARJA
			if (cst.listData[0].penjamin_1=="1" || cst.listData[0].penjamin_1==1) {
				Ext.getCmp('jasa_raharja_update'+MODULE).setValue(true);
			}

			// CHECK LAKALANTAS BPJS
			if (cst.listData[0].penjamin_2=="2" || cst.listData[0].penjamin_2==2) {
				Ext.getCmp('bpjs_update'+MODULE).setValue(true);
			}

			// CHECK LAKALANTAS TASPEN
			if (cst.listData[0].penjamin_3=="3" || cst.listData[0].penjamin_3==3) {
				Ext.getCmp('taspen_update'+MODULE).setValue(true);
			}

			// CHECK LAKALANTAS ASABRI
			if (cst.listData[0].penjamin_4=="4" || cst.listData[0].penjamin_4==4) {
				Ext.getCmp('asabri_update'+MODULE).setValue(true);
			}

			// CHECK PROPINSI MELALUI REST CLIENT
			Ext.Ajax.request({
			url: baseURL + "index.php/gawat_darurat/Gawat_darurat/cari_propinsi",
				params: {kd_prov   :cst.listData[0].kd_prov },
				failure: function(o){
					var cst = Ext.decode(o.responseText);
				},	    
				success: function(o) {
					var cst = Ext.decode(o.responseText);
					Ext.getCmp("cboPropinsiLokasiLaka_update"+MODULE).setValue(cst.listData.nama);								
				}
			});

			// CHECK KABUPATEN MELALUI REST CLIENT
			Ext.Ajax.request({
			url: baseURL + "index.php/gawat_darurat/Gawat_darurat/cari_kabupaten",
				params: {kd_kab   :cst.listData[0].kd_kab,
						 kd_prov  :cst.listData[0].kd_prov},
				failure: function(o){
					var cst = Ext.decode(o.responseText);
				},	    
				success: function(o) {
					var cst = Ext.decode(o.responseText);
					Ext.getCmp("cboKabupatenLokasiLaka_update"+MODULE).setValue(cst.listData.nama);
					// selectKabupatenLokasiLaka_update=cst.listData.kode;										
				}
			});
			
			// CHECK KECAMATAN MELALUI REST CLIENT
			Ext.Ajax.request({
			url: baseURL + "index.php/gawat_darurat/Gawat_darurat/cari_kecamatan",
				params: {kd_kec   :cst.listData[0].kd_kec,
						 kd_kab   :cst.listData[0].kd_kab },
				failure: function(o){
					var cst = Ext.decode(o.responseText);
				},	    
				success: function(o) {
					var cst = Ext.decode(o.responseText);
					Ext.getCmp("cboKecamatanSLokasiLaka_update"+MODULE).setValue(cst.listData.nama);					
				}
			});

			dialog_update_sep.params.kd_pasien 				= data_pasien.data.KD_PASIEN;
			dialog_update_sep.params.kd_unit 				= cst.listData[0].kd_unit;
			dialog_update_sep.params.tgl_masuk 				= cst.listData[0].tgl_masuk;
			dialog_update_sep.params.urut_masuk 			= cst.listData[0].urut_masuk;
			dialog_update_sep.params.nomor_rujukan 			= cst.listData[0].no_rujukan;
			dialog_update_sep.params.kd_dokter_dpjp 		= cst.listData[0].kd_dokter_dpjp;
			dialog_update_sep.params.kd_rujukan 			= cst.listData[0].ppk_rujukan;
			dialog_update_sep.params.no_kartu 				= cst.listData[0].no_kartu;
			dialog_update_sep.params.kelas_rawat 			= cst.listData[0].kelas_rawat;
			dialog_update_sep.params.tgl_rujukan 			= cst.listData[0].tgl_rujukan;
			dialog_update_sep.params.no_rujukan 			= cst.listData[0].no_rujukan;
			dialog_update_sep.params.ppk_rujukan 			= cst.listData[0].ppk_rujukan;
			dialog_update_sep.params.catatan 				= cst.listData[0].catatan;
			dialog_update_sep.params.diag_awal				= cst.listData[0].diag_awal;
			dialog_update_sep.params.poli_tujuan			= cst.listData[0].poli_tujuan;
			dialog_update_sep.params.eksekutif				= cst.listData[0].eksekutif;
			dialog_update_sep.params.tgl_kejadian			= cst.listData[0].tgl_kejadian;
			dialog_update_sep.params.keterangan_kejadian	= cst.listData[0].keterangan_kejadian;
			dialog_update_sep.params.kd_prov				= cst.listData[0].kd_prov;
			dialog_update_sep.params.kd_kab					= cst.listData[0].kd_kab;
			dialog_update_sep.params.kd_kec					= cst.listData[0].kd_kec;
			dialog_update_sep.params.no_surat_dpjp			= cst.listData[0].no_surat_dpjp;
			dialog_update_sep.params.no_tlp					= cst.listData[0].no_tlp;
			dialog_update_sep.params.cob					= cst.listData[0].cob;
			dialog_update_sep.params.katarak				= cst.listData[0].katarak;
			dialog_update_sep.params.lakalantas				= parseInt(cst.listData[0].lakalantas);
			dialog_update_sep.params.suplesi				= cst.listData[0].suplesi;
			dialog_update_sep.params.kd_dpjp				= cst.listData[0].kd_dpjp;
			dialog_update_sep.params.no_suplesi				= cst.listData[0].no_suplesi;
			dialog_update_sep.params.penjamin_1				= parseInt(cst.listData[0].penjamin_1);
			dialog_update_sep.params.penjamin_2				= parseInt(cst.listData[0].penjamin_2);
			dialog_update_sep.params.penjamin_3				= parseInt(cst.listData[0].penjamin_3);
			dialog_update_sep.params.penjamin_4				= parseInt(cst.listData[0].penjamin_4);
			dialog_update_sep.params.no_sep					= cst.listData[0].no_sep;
			dialog_update_sep.params.kd_pasien				= cst.listData[0].kd_pasien;
			dialog_update_sep.params.kd_unit				= cst.listData[0].kd_unit;
			dialog_update_sep.params.tgl_masuk				= cst.listData[0].tgl_masuk;
			dialog_update_sep.params.urut_masuk				= cst.listData[0].urut_masuk;
			dialog_update_sep.params.asal_rujukan			= cst.listData[0].asal_rujukan;
		}
	});
}

function panel_form_lookup_rujukan_ALL_update(MODULE){
 // loadDataComboProvinsiLaka_All();
	var formPanel_update= new Ext.form.FormPanel({
		id: "myformpanel_rujukan_bpjs_update"+MODULE,
		bodyStyle: "padding:5px",
		labelAlign: "top",
		columnWidth:.10,
		layout: 'absolute',
		border: false,
		height: 405,
		items:[
		{
			x: 10,
			y: 5,
			xtype: 'label',
			text: 'Nama Peserta'
		},
		{
			x: 120,
			y: 5,
			xtype: 'label',
			text: ':'
		},
		{
			x: 130,
			y: 5,
			xtype: 'textfield',
			id: 'txt_nama_peserta_update'+MODULE,
			name: 'txt_nama_peserta_update'+MODULE,
			width: 200,
			disabled:true,
			allowBlank: false,
			readOnly: true,
			maxLength:3,
			tabIndex:1
		},

		{
			x: 340,
			y: 5,
			xtype: 'label',
			text: 'No Kartu'
		},
		{
			x: 410,
			y: 5,
			xtype: 'label',
			text: ':'
		},
		{
			x: 420,
			y: 5,
			xtype: 'textfield',
			id: 'txt_no_kartu_peserta_update'+MODULE,
			name: 'txt_no_kartu_peserta_update'+MODULE,
			width: 200,
			disabled:true,
			allowBlank: false,
			readOnly: true,
			maxLength:3,
			tabIndex:1
		},

		{
			x: 10,
			y: 105,
			xtype: 'label',
			text: 'Hak Kelas'
		},
		{
			x: 120,
			y: 105,
			xtype: 'label',
			text: ':'
		},
		{
			x: 130,
			y: 105,
			xtype: 'textfield',
			id: 'txt_hak_kelas_peserta_update'+MODULE,
			name: 'txt_hak_kelas_peserta_update'+MODULE,
			width: 200,
			disabled:true,
			allowBlank: false,
			readOnly: true,
			maxLength:3,
			tabIndex:1
		},
		{
			x: 10,
			y: 55,
			xtype: 'label',
			text: 'Spesialis/SubSpesialis'
		},
		{
			x: 120,
			y: 55,
			xtype: 'label',
			text: ':'
		},
		
		{
			x: 130,
			y: 55,
			xtype: 'textfield',
			id: 'txt_poli_update'+MODULE,
			name: 'txt_poli_update'+MODULE,
			width: 200,
			disabled:true,
			allowBlank: false,
			readOnly: false,
			maxLength:3,
			tabIndex:1,
			listeners:{
						specialkey:function(text,e){
							if(e.getKey()==13 ){
								formLookup_poli_faskesTLSEP();
								if(Ext.getCmp('txt_poli').getValue()!=''){
									Ext.Ajax.request({
										method:'POST',
										url: baseURL + "index.php/rawat_jalan/functionRWJ/get_poli_rujukan",
										params: {
											nama_poli_rujuk: Ext.getCmp('txt_poli_update').getValue() 
										},
										success:function(o){
											loadMask.hide();
						                    var cst = Ext.decode(o.responseText);
											if (cst.success === true) {
												var recs=[],
													recType=dataSourcePoliFaskesTL.recordType;
													
												for(var i=0; i<cst.listData.length; i++){
													recs.push(new recType(cst.listData[i]));
												}
												dataSourcePoliFaskesTL.add(recs);
												gridListPoliFaskesTLSEP.getView().refresh();
											}
											else {
												ShowPesanError_viDaftar('Gagal membaca data di poli rujuk', 'Error');
											}
											
										},error: function (jqXHR, exception) {
										   Ext.getCmp('textAreaKepesertaan').setValue('Terjadi kesalahan dari server');
										}
									});	

								}
								else{
								ShowPesanWarning_viIgdDaftar('No Rujukan Belum Diisi' , 'No Rujukan');
								}	
							}
							
						}
					}
		},
		{
			
			xtype: 'textfield',
			id: 'txt_kd_spesialis_update'+MODULE,
			name: 'txt_kd_spesialis_update'+MODULE,
			width: 150,
			disabled:true,
			allowBlank: false,
			readOnly: true,
			maxLength:3,
			tabIndex:1,
			hidden:true
		},{
			x: 340,
			y: 30,
			xtype: 'label',
			text: 'No. Telepon'
		},{
			x: 410,
			y: 30,
			xtype: 'label',
			text: ':'
		},{
			x: 420,
			y: 30,
			xtype: 'textfield',
			id: 'txt_no_tlp_update'+MODULE,
			name: 'txt_no_tlp_update'+MODULE,
			width: 200,
			disabled:false,
			allowBlank: false,
			maxLength:3,
			tabIndex:1,
			listeners : {

			}
		},{
			x: 10,
			y: 80,
			xtype: 'label',
			text: 'Tgl.Rujukan'
		},{
			x: 120,
			y: 80,
			xtype: 'label',
			text: ':'
		},{
			xtype: 'datefield',
			x: 130,
			y: 80,
			id: 'tgl_SEP_update'+MODULE,
			name: 'tgl_SEP_update'+MODULE,
			format: 'Y-m-d',
			readOnly: true,
			disabled:true,
			value: now,
			width: 200,            
        },{
			x: 340,
			y: 55,
			xtype: 'label',
			text: 'Katarak'
		},{
			x: 410,
			y: 55,
			xtype: 'label',
			text: ':'
		},{
			x: 420,
			y: 55,
			xtype: 'checkbox',
			boxLabel: '',
			name: 'cbo_katarak_update',
			id: 'cbo_katarak_update'+MODULE,
			listeners : {
				check: function (checkbox, isChecked) {
					if (isChecked === true) {
						dialog_update_sep.params.katarak=1;
					}else{
						dialog_update_sep.params.katarak=0;
					}
				}
			}
		},{
			x: 10,
			y: 30,
			xtype: 'label',
			text: 'No Medrec'
		},{
			x: 120,
			y: 30,
			xtype: 'label',
			text: ':'
		},{
			x: 130,
			y: 30, 
			xtype: 'textfield',
			id: 'txt_no_medrec_update'+MODULE,
			name: 'txt_no_medrec_update'+MODULE,
			width: 200,
			disabled:true,
			allowBlank: false,
			readOnly: false,
			maxLength:3,
			tabIndex:1
		},{
			x: 340,
			y: 70,
			xtype: 'label',
			text: 'Peserta COB'
		},{
			x: 410,
			y: 70,
			xtype: 'label',
			text: ':'
		},{
			x: 420,
			y: 70,
			xtype: 'checkbox',
			boxLabel: '',
			name: 'cbo_peserta_cob_update'+MODULE,
			id: 'cbo_peserta_cob_update'+MODULE,
			disabled:false,
			listeners : {
				check: function (checkbox, isChecked) {
					if (isChecked === true) {
						dialog_update_sep.params.cob=1;
					}else{
						dialog_update_sep.params.cob=0;
					}
				}
			}
		},{
			x: 10,
			y: 130,
			xtype: 'label',
			text: 'Diagnosa'
		},
		{
			x: 120,
			y: 130,
			xtype: 'label',
			text: ':'
		},
		{
			xtype: 'textfield',
			id: 'txt_kd_diagnosa_update'+MODULE,
			name: 'txt_kd_diagnosa_update'+MODULE,
			width: 50,
			disabled:true,
			hidden:true,
			allowBlank: false,
			readOnly: false,
			maxLength:3,
			tabIndex:1
		},{
				x: 130,
				y: 130,
				xtype: 'textfield',
				id: 'txt_nama_diagnosa_update'+MODULE,
				name: 'txt_nama_diagnosa_update'+MODULE,
				width: 300,
				disabled:false,
				allowBlank: false,
				readOnly: false,
				maxLength:3,
				tabIndex:1,
				listeners:{
						specialkey:function(text,e){
								if(e.getKey()==13){
									if(Ext.getCmp('txt_nama_diagnosa_update'+MODULE).getValue().trim()!=''){
										
										loadMask.show();
										Ext.Ajax.request({
											method:'POST',
											url: baseURL + "index.php/rawat_jalan/functionRWJ/cari_data_diagnosa",
											params: {
												nama_diagnosa: Ext.getCmp('txt_nama_diagnosa_update'+MODULE).getValue() 
											},
											success:function(o){
												loadMask.hide();
												var cst = Ext.decode(o.responseText);
												console.log(cst);
												if(cst.success==true){
													dialog_update_sep.datastore.diagnosa.removeAll();
													for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
														var recs    = [],recType =  dialog_update_sep.datastore.diagnosa.recordType;
														var o=cst['listData'][i];
														recs.push(new recType(o));	
														dialog_update_sep.datastore.diagnosa.add(recs);
													}
													form_daftar_diagnosa_All(MODULE);
												}else{
													ShowPesanWarning_viIgdDaftar(cst.metaData.message , 'Generate SEP IGD');

												}
												
							                   
											},error: function (jqXHR, exception) {
											  // Ext.getCmp('textAreaKepesertaan').setValue('Terjadi kesalahan dari server');
											}
										});
									}else{
										Ext.MessageBox.show({
											title: 'Informasi',
											msg: 'Harap Nama Diagnosa.',
											buttons: Ext.MessageBox.OK,
											icon: Ext.MessageBox.INFO,
											fn: function(btn){
												if(btn == 'ok'){
													Ext.getCmp('txtIgdNoAskes').focus();
												}
											}
										});
									}
								}
							}
				}
			},
			{
				x: 10,
				y: 155,
				xtype: 'label',
				text: 'Catatan'
			},
			{
				x: 120,
				y: 155,
				xtype: 'label',
				text: ':'
			},
			{
				x: 130,
				y: 155,
				xtype: 'textfield',
				id: 'txt_no_catatan_update'+MODULE,
				name: 'txt_no_catatan_update'+MODULE,
				width: 200,
				disabled:false,
				allowBlank: false,
				readOnly: false,
				maxLength:3,
				tabIndex:1
			},

			{
				x: 340,
				y: 155,
				xtype: 'label',
				id:'lbl_no_sep_update'+MODULE,
				text: 'No SEP'
			},
			{
				id:'lbl_sep_separator_update'+MODULE,
				x: 410,
				y: 155,
				xtype: 'label',
				text: ':'
			},
			{	
				x: 420,
				y: 155,
				xtype: 'textfield',
				id: 'txt_no_sep_update'+MODULE,
				name: 'txt_no_sep_update'+MODULE,
				width: 200,
				disabled:false,
				allowBlank: false,
				readOnly: true,
				maxLength:3,
				tabIndex:1
			},

			{
				x: 10,
				y: 175,
				xtype: 'checkbox',
				boxLabel: 'Penjamin KLL',
				name: 'penjamin_kkl_update'+MODULE,
				id: 'penjamin_kkl_update'+MODULE,
				listeners : {
					check: function (checkbox, isChecked) {
							if (isChecked === true) {
								Ext.getCmp('lbl_penjamin_update'+MODULE).show();
								Ext.getCmp('lbl_penjamin_separator_update'+MODULE).show();
								Ext.getCmp('jasa_raharja_update'+MODULE).show();
								Ext.getCmp('bpjs_update'+MODULE).show();
								Ext.getCmp('taspen_update'+MODULE).show();
								Ext.getCmp('asabri_update'+MODULE).show();
								Ext.getCmp('lbl_tgl_kejadian_update'+MODULE).show();
								Ext.getCmp('lbl_tgl_separator_update'+MODULE).show();
								Ext.getCmp('tgl_kejadian_update'+MODULE).show();
								Ext.getCmp('lbl_lokasi_kejadian_update'+MODULE).show();
								Ext.getCmp('lbl_lokasi_kejadian_separator_update'+MODULE).show();
								dialog_update_sep.combobox.provinsi.show();
								dialog_update_sep.combobox.kabupaten.show();
								dialog_update_sep.combobox.kecamatan.show();
								Ext.getCmp('lbl_ket_kejadian_update'+MODULE).show();
								Ext.getCmp('lbl_ket_kejadian_separator_update'+MODULE).show();
								Ext.getCmp('txt_ket_kejadian_update'+MODULE).show();
								Ext.getCmp('cbo_suplesi_update'+MODULE).show();
								dialog_update_sep.params.lakalantas = 1;
							}if(isChecked === false){
								dialog_update_sep.params.lakalantas = 0;
								Ext.getCmp('lbl_penjamin_update'+MODULE).hide();
								Ext.getCmp('lbl_penjamin_separator_update'+MODULE).hide();
								Ext.getCmp('jasa_raharja_update'+MODULE).hide();
								Ext.getCmp('bpjs_update'+MODULE).hide();
								Ext.getCmp('taspen_update'+MODULE).hide();
								Ext.getCmp('asabri_update'+MODULE).hide();
								Ext.getCmp('lbl_tgl_kejadian_update'+MODULE).hide();
								Ext.getCmp('lbl_tgl_separator_update'+MODULE).hide();
								Ext.getCmp('tgl_kejadian_update'+MODULE).hide();
								Ext.getCmp('lbl_lokasi_kejadian_update'+MODULE).hide();
								Ext.getCmp('lbl_lokasi_kejadian_separator_update'+MODULE).hide();
								dialog_update_sep.combobox.provinsi.hide();
								dialog_update_sep.combobox.kabupaten.hide();
								dialog_update_sep.combobox.kecamatan.hide();
								Ext.getCmp('lbl_ket_kejadian_update'+MODULE).hide();
								Ext.getCmp('lbl_ket_kejadian_separator_update'+MODULE).hide();
								Ext.getCmp('txt_ket_kejadian_update'+MODULE).hide();
								Ext.getCmp('cbo_suplesi_update'+MODULE).hide();
								
							}
					}
				}
			},

			{
				x: 10,
				y: 185,
				xtype: 'checkbox',
				boxLabel: 'Suplesi',
				name: 'cbo_suplesi_update'+MODULE,
				id: 'cbo_suplesi_update'+MODULE,
				hidden:true,
				listeners : {
					check: function (checkbox, isChecked) {
							if (isChecked === true) {
								dialog_update_sep.params.suplesi = 1;
								Ext.getCmp('lbl_no_sep_suplesi_update'+MODULE).show();
								Ext.getCmp('sprt_no_sep_suplesi_update'+MODULE).show();
								Ext.getCmp('txt_no_sep_suplesi_update'+MODULE).show();	
								Ext.getCmp('txt_no_sep_suplesi_update'+MODULE).setValue(dialog_update_sep.params.no_suplesi);	
							}if(isChecked === false){
								dialog_update_sep.params.suplesi = 0;
								Ext.getCmp('lbl_no_sep_suplesi_update'+MODULE).hide();
								Ext.getCmp('sprt_no_sep_suplesi_update'+MODULE).hide();
								Ext.getCmp('txt_no_sep_suplesi_update'+MODULE).hide();
								Ext.getCmp('txt_no_sep_suplesi_update'+MODULE).setValue('');	
							}
					}
				}
			},

			{
				x: 120,
				y: 185,
				hidden:true,
				xtype: 'label',
				id:'lbl_no_sep_suplesi_update'+MODULE,
				text: 'No SEP Suplesi'
			},
			{
				x: 210,
				y: 185,
				hidden:true,
				xtype: 'label',
				id:'sprt_no_sep_suplesi_update'+MODULE,
				text: ':'
			},
			{
				x: 220,
				y: 185,
				hidden:true,
				xtype: 'textfield',
				id: 'txt_no_sep_suplesi_update'+MODULE,
				name: 'txt_no_sep_suplesi_update'+MODULE,
				width: 150,
				disabled:false,
				allowBlank: false,
				readOnly: false,
				maxLength:3,
				tabIndex:1,
				listeners   : {
				'render': function (c) {
					c.getEl().on('keypress', function (e) {
						dialog_update_sep.params.no_suplesi = Ext.get('txt_no_sep_suplesi_update'+MODULE).getValue();
					}, c);
					c.getEl().on('change', function (e) {
						dialog_update_sep.params.no_suplesi = Ext.get('txt_no_sep_suplesi_update'+MODULE).getValue();
					}, c);
					},
					change: function (t,n,o) {
						dialog_update_sep.params.no_suplesi = Ext.get('txt_no_sep_suplesi_update'+MODULE).getValue();
					},
				}
			},

			{
				x: 10,
				y: 210,
				xtype: 'label',
				id:'lbl_penjamin_update'+MODULE,
				text: 'Penjamin',
				hidden:true
			},
			{
				x: 120,
				y: 210,
				xtype: 'label',
				id:'lbl_penjamin_separator_update'+MODULE,
				text: ':',
				hidden:true
			},
			{
				x: 130,
				y: 210,
				xtype: 'checkbox',
				boxLabel: 'Jasa Raharja',
				name: 'jasa_raharja_update'+MODULE,
				id: 'jasa_raharja_update'+MODULE,
				hidden:true,
				listeners : {
					check: function (checkbox, isChecked) {
						if (isChecked === true) {
							dialog_update_sep.params.penjamin_1 = 1;
						}else{
							dialog_update_sep.params.penjamin_1 = 0;
						}
					}
				}
			},
			{
				x: 240,
				y: 210,
				xtype: 'checkbox',
				boxLabel: 'BPJS',
				name: 'bpjs_update'+MODULE,
				id: 'bpjs_update'+MODULE,
				hidden:true,
				listeners : {
					check: function (checkbox, isChecked) {
							if (isChecked === true) {
								dialog_update_sep.params.penjamin_2=2;
							}else{
								dialog_update_sep.params.penjamin_2=0;
							}
					}
				}
			},
			{
				x: 360,
				y: 210,
				xtype: 'checkbox',
				boxLabel: 'Taspen',
				name: 'taspen_update'+MODULE,
				id: 'taspen_update'+MODULE,
				hidden:true,
				listeners : {
					check: function (checkbox, isChecked) {
							if (isChecked === true) {
								dialog_update_sep.params.penjamin_3=3;
							}else{
								dialog_update_sep.params.penjamin_3=0;
							}
					}
				}
			},
			{
				x: 450,
				y: 210,
				xtype: 'checkbox',
				boxLabel: 'Asabri',
				name: 'asabri_update'+MODULE,
				id: 'asabri_update'+MODULE,
				hidden:true,
				listeners : {
					check: function (checkbox, isChecked) {
							if (isChecked === true) {
								dialog_update_sep.params.penjamin_4=4;
							}else{
								dialog_update_sep.params.penjamin_4=0;
							}
					}
				}
			},

			{
				x: 10,
				y: 230,
				xtype: 'label',
				id:'lbl_tgl_kejadian_update'+MODULE,
				text: 'Tanggal Kejadian',
				hidden:true
			},
			{
				x: 120,
				y: 230,
				xtype: 'label',
				id:'lbl_tgl_separator_update'+MODULE,
				text: ':',
				hidden:true
			},
			{
				x: 130,
				y: 230,
				xtype: 'datefield',
                id: 'tgl_kejadian_update'+MODULE,
                name: 'tgl_kejadian_update'+MODULE,
                format: 'Y-m-d',
                readOnly: false,
                hidden:true,
                value: now,
                width:200
			},

			{
				x: 10,
				y: 260,
				xtype: 'label',
				id:'lbl_lokasi_kejadian_update'+MODULE,
				hidden:true,
				text: 'Lokasi Kejadian'
			},
			{
				x: 120,
				y: 260,
				xtype: 'label',
				id:'lbl_lokasi_kejadian_separator_update'+MODULE,
				text: ':',
				hidden:true
			},
			mComboPropinsiLokasiLaka_ALL(MODULE),
			mComboKabupatenLokasiLaka_ALL(MODULE),
			mComboKecamatanLokasiLaka_All(MODULE),
			{
				x: 10,
				y: 335,
				xtype: 'label',
				id: 'lbl_ket_kejadian_update'+MODULE,
				text: 'Keteragan Kejadian',
				hidden:true
			},
			{
				x: 120,
				y: 335,
				xtype: 'label',
				id: 'lbl_ket_kejadian_separator_update'+MODULE,
				text: ':',
				hidden:true
			},
			{
				x: 130,
				y: 335,
				xtype: 'textfield',
				id: 'txt_ket_kejadian_update'+MODULE,
				name: 'txt_ket_kejadian_update'+MODULE,
				width: 200,
				disabled:false,
				allowBlank: false,
				readOnly: false,
				maxLength:3,
				hidden:true,
				tabIndex:1
			},
		],
		buttons:
		[
			{
				text: "Update SEP",
				id:'update_sep_'+MODULE,
				handler: function(){
					ajax_update_sep_All(MODULE);
				}
			}
		]
	});
	return formPanel_update;
}


function ajax_update_sep_All(MODULE){
	dialog_update_sep.params.no_tlp 				= Ext.getCmp('txt_no_tlp_update'+MODULE).getValue();
	dialog_update_sep.params.catatan 				= Ext.getCmp('txt_no_catatan_update'+MODULE).getValue();
	dialog_update_sep.params.keterangan_kejadian 	= Ext.getCmp('txt_ket_kejadian_update'+MODULE).getValue();
	dialog_update_sep.params.katarak 				= Ext.getCmp('cbo_katarak_update'+MODULE).getValue();
	dialog_update_sep.params.cob 					= Ext.getCmp('cbo_peserta_cob_update'+MODULE).getValue();
	dialog_update_sep.params.tgl_rujukan			= Ext.getCmp('tgl_SEP_update'+MODULE).getValue();
	dialog_update_sep.params.tgl_kejadian			= Ext.getCmp('tgl_kejadian_update'+MODULE).getValue();

	// loadMask.show();
	$.ajax({
		type: 'POST',
		dataType: 'JSON',
		crossDomain: true,
		url: baseURL + "index.php/rawat_jalan/functionRWJ/UpdateSEP_Rev",
		data: {
			parameter : JSON.stringify(dialog_update_sep.params),
		}, 
		success: function (resp1) {
			// loadMask.hide();
			
			if (resp1.metaData.code == '200') {
				dialog_update_sep.form.dialog.close();
				Ext.MessageBox.alert('Update SEP', 'SEP Berhasil Di Update');
			}else{
				Ext.MessageBox.alert('Gagal', resp1.metaData.message );
			}
		},
		error: function (jqXHR, exception) {
			// loadMask.hide();
			Ext.MessageBox.alert('Gagal', 'Kesalahan Pada Jaringan.');
			Ext.Ajax.request({
				url: baseURL + "index.php/rawat_jalan/functionRWJ/save_log_bpjs",
				params: {
					respon: jqXHR.responseText,
					keterangan: 'Error create SEP'
				}
			});
		}
	}); 
}

function getParamSepBpjsUpdate(MODULE){
	var tgl_kejadian_update_clean=Ext.getCmp('tgl_kejadian_update'+MODULE).getValue();
	// penjamin_kkl_update
	var tmp_penjamin = 0;
	if (Ext.getCmp('penjamin_kkl_update'+MODULE).getValue() === true) {
		tmp_penjamin = 1;
	}else{
		tmp_penjamin_1_update = 0;
		tmp_penjamin_2_update = 0;
		tmp_penjamin_3_update = 0;
		tmp_penjamin_4_update = 0;
	}
	var	params =
	{
		flaging          :MODULE,
		noKartu			 :Ext.getCmp('txt_no_kartu_peserta_update'+MODULE).getValue(), //1
		klsRawat	  	 :kelas_rawat, //tmp_kelas_rawat, //2
		noMR 	    	 :Ext.getCmp('txt_no_medrec_update'+MODULE).getValue(), //3
		tglRujukan 	 	 :tmp_tgl_rujukan_update,//4
		noRujukan 		 :'', //5
		ppkRujukan 		 :tmp_ppk_rujukan_update, //6
		catatan 		 :Ext.getCmp('txt_no_catatan_update'+MODULE).getValue(),//7
		diagAwal 		 :tmp_diagnosa_update, 
		tujuan 			 :MODULE,
		katarak          :tmp_katarak,
		cob   			 :tmp_cob_update,
		eksekutif 		 :tmp_eksekutif_update,
		no_sep 			 :Ext.getCmp('txt_no_sep_update'+MODULE).getValue(),

		lakalantas 		 :tmp_penjamin,
		penjamin_1 		 :tmp_penjamin_1_update,
		penjamin_2 		 :tmp_penjamin_2_update,
		penjamin_3 		 :tmp_penjamin_3_update,
		penjamin_4 		 :tmp_penjamin_4_update,
		tglKejadian 	 :tgl_kejadian_update_clean.format("Y-m-d"),
		keterangan 		 :Ext.getCmp('txt_ket_kejadian_update'+MODULE).getValue(),
		suplesi 		 :tmp_suplesi_update,
		noSepSuplesi 	 :Ext.getCmp('txt_no_sep_suplesi_update'+MODULE).getValue(),
		kdPropinsi 		 :selectProvinsiLokasiLaka_update,
		kdKabupaten 	 :selectKabupatenLokasiLaka_update,
		kdKecamatan 	 :selectKecamatannLokasiLaka_update,
		noSurat 		 :'',
		kodeDPJP 		 :tmp_dokter_dpjp_update,
		noTelp 			 :Ext.getCmp('txt_no_tlp_update'+MODULE).getValue(),
		user 			 :'Test WS',
		no_suplesi 		 :Ext.getCmp('txt_no_sep_suplesi_update'+MODULE).getValue(),
	}
    return params
};


function loadDataComboProvinsiLaka_All(){ 
	Ext.Ajax.request({
	url: baseURL + "index.php/rawat_jalan/functionRWJ/get_provisi_from_bpjs",
		params: '0',
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);
			dialog_update_sep.datastore.provinsi.removeAll();
			dialog_update_sep.datastore.kabupaten.removeAll();
			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType =  dialog_update_sep.datastore.provinsi.recordType;
				var o=cst['listData'][i];
				recs.push(new recType(o));
				dialog_update_sep.datastore.provinsi.add(recs);
			}
				console.log(dialog_update_sep.datastore.provinsi);
		}
	});
}

function mComboPropinsiLokasiLaka_ALL(MODULE){      
	loadDataComboProvinsiLaka_All();
	dialog_update_sep.combobox.provinsi = new Ext.form.ComboBox
	(
		{
			x: 130,
			y: 260,
			readOnly: false,
			hidden 	: true,
		    id: 'cboPropinsiLokasiLaka_update'+MODULE,
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
		    align: 'Right',
		    emptyText: 'Pilih Propinsi...',
		    store: dialog_update_sep.datastore.provinsi,
		    valueField: 'kode',
		    displayField: 'nama',
		    width: 200,
			tabIndex:3,
		    listeners:
			{
			    'select': function(a, b, c){
			    	dialog_update_sep.datastore.provinsi = b.data.kode;
					loaddatastoreKabupatenLakalantas_All(b.data.kode);
                },				                       
			}
                }
	);
    return dialog_update_sep.combobox.provinsi;
}

function mComboKabupatenLokasiLaka_ALL(MODULE){
    dialog_update_sep.combobox.kabupaten = new Ext.form.ComboBox({
		id: 'cboKabupatenLokasiLaka_update'+MODULE,
		typeAhead: true,
		x: 130,
		y: 285,
		readOnly: false,
		hidden:true,
		triggerAction: 'all',
		selectOnFocus: true,
		lazyRender: true,
		mode: 'local',
		width:200,
		forceSelection: true,
		emptyText: 'Pilih Kabupaten...',
		fieldLabel: 'Kab/Kod',
		align: 'Right',
		store: dialog_update_sep.datastore.kabupaten,
		valueField: 'kode',
		displayField: 'nama',
		tabIndex: 6,
		enableKeyEvents:true,
		listeners:{
			'select': function (a, b, c){
				dialog_update_sep.datastore.kabupaten = b.data.kode;
				loaddatastorekecamatanLakalantas_All(b.data.kode);
			}
		}
	});
    return dialog_update_sep.combobox.kabupaten;
}


function loaddatastoreKabupatenLakalantas_All(kd_provinsi){ 
	Ext.Ajax.request({
	url: baseURL + "index.php/rawat_jalan/functionRWJ/get_kabupaten_from_bpjs",
		params: {
			kode_propinsi:kd_provinsi,
		},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);
			dialog_update_sep.datastore.kabupaten.removeAll();
			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType =  dialog_update_sep.datastore.kabupaten.recordType;
				var o=cst['listData'][i];
				recs.push(new recType(o));	
				dialog_update_sep.datastore.kabupaten.add(recs);
			}
		}
	});
}

function loaddatastorekecamatanLakalantas_All(kd_kab){ 
	Ext.Ajax.request({
	url: baseURL + "index.php/rawat_jalan/functionRWJ/get_kecamatan_from_bpjs",
		params: {
			kode_kab:kd_kab,
		},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);
			dialog_update_sep.datastore.kecamatan.remove();
			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType =  dialog_update_sep.datastore.kecamatan.recordType;
				var o=cst['listData'][i];
				recs.push(new recType(o));
				dialog_update_sep.datastore.kecamatan.add(recs);
			}
		}
	});
}


function mComboKecamatanLokasiLaka_All(MODULE){
    dialog_update_sep.combobox.kecamatan = new Ext.form.ComboBox({
		id: 'cboKecamatanSLokasiLaka_update'+MODULE,
		x: 130,
		y: 310,
		width:200,
		readOnly: false,
		hidden:true,
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		forceSelection: true,
		emptyText: 'Pilih Kecamatan...',
		fieldLabel: 'Kecamatan',
		align: 'Right',
		tabIndex: 6,
		store: dialog_update_sep.datastore.kecamatan,
		valueField: 'kode',
		displayField: 'nama',
		enableKeyEvents:true,
		listeners:{
			'select': function (a, b, c){
				dialog_update_sep.datastore.kecamatan = b.data.kode;
			}
		}
	});
    return dialog_update_sep.combobox.kecamatan;
}


function form_daftar_diagnosa_All(MODULE){
    var lebar = 550;
    form_list_rujukan_bpjs = new Ext.Window({
		id: 'formLookup_diagnosa_bpjs'+MODULE,
		name: 'formLookup_diagnosa_bpjs'+MODULE,
		title: 'Form List Diagnosa',
		closeAction: 'destroy',
		width: lebar,
		height: 200, //575,
		resizable: false,
		constrain: true,
		autoScroll: false,
		iconCls: 'Studi_Lanjut',
		modal: true,
		items: panel_form_lookup_daftar_diagnosa_All(MODULE), //1
		listeners:{
			afterShow: function ()
			{
				this.activate();

			}
		}
	});
    form_list_rujukan_bpjs.show();
}

function panel_form_lookup_daftar_diagnosa_All(MODULE){
	var gridListDiagnosa= new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dialog_update_sep.datastore.diagnosa,
        columnLines: false,
        autoScroll: false,
		width : 720,
		height: 250, 
		x: 5,
		y:10,
        border: true,
        sort: false,
        autoHeight: false,
        layout: 'absolute',
        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
            listeners: {
                rowselect: function (sm, row, rec) {
					Ext.getCmp('txt_nama_diagnosa_update'+MODULE).setValue(rec.data.nama);
					dialog_update_sep.params.diag_awal = rec.data.kode;
					form_list_rujukan_bpjs.close();
                }
            }
        }),
        cm: new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer({
				header:'No.'
			}),{
				id: 'colKodeDiagnosaBPJS'+MODULE,
				header: 'KODE',
				dataIndex: 'kode',
				sortable: true,
				hideable:false,
				menuDisabled:true,
				width: 10
			},
			{
				id: 'colNamaDiagnosaBPJS'+MODULE,
				header: 'NAMA',
				dataIndex: 'nama',
				menuDisabled: false,
				sortable: true,
				hideable:false,
				menuDisabled:true,
				width: 40
			}
		]),
        viewConfig: {
            forceFit: true
        }
    }); 
    var items_diagnosa ={
		layout: 'column',
		border: false,
		autoScroll: false,
		items:[
			{
				layout: 'absolute',
				bodyStyle: 'padding: 0px 0px 0px 0px',
				border: false,
				autoScroll: false,
				width: '100%',
				height: 350,
				anchor: '100% 100%',
				items:[
					gridListDiagnosa
				]
			}
		]
	};
    return items_diagnosa;
}
