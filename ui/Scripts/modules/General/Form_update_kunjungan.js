﻿/*
	HADAD AL GOJALI
	2019
	FUNCTION untuk formulir update kunjungan
 */
var UpdateKunjungan_RWJIGD = {};
UpdateKunjungan_RWJIGD.response = {};
UpdateKunjungan_RWJIGD.variable = {};
UpdateKunjungan_RWJIGD.variable.data_store = {};
UpdateKunjungan_RWJIGD.variable.data_store.unit 	= new WebApp.DataStore({fields: ['KD_UNIT', 'NAMA_UNIT'] });
UpdateKunjungan_RWJIGD.variable.data_store.dokter 	= new WebApp.DataStore({fields: ['KD_DOKTER', 'NAMA_DOKTER', 'NAMA'] });
UpdateKunjungan_RWJIGD.variable.data_store.customer	= new WebApp.DataStore({fields: ['KD_CUSTOMER', 'CUSTOMER'] });;
UpdateKunjungan_RWJIGD.variable.data_store.rujukan	= new WebApp.DataStore({fields: ['KD_RUJUKAN', 'RUJUKAN', "CARA_PENERIMAAN"] });
UpdateKunjungan_RWJIGD.variable.data_store.diagnosa	= new WebApp.DataStore({fields: ['kd_penyakit', 'penyakit'] });
UpdateKunjungan_RWJIGD.function = {};
UpdateKunjungan_RWJIGD.function.dialog;
UpdateKunjungan_RWJIGD.function.get_data;
UpdateKunjungan_RWJIGD.function.get_unit;
UpdateKunjungan_RWJIGD.function.get_dokter;
UpdateKunjungan_RWJIGD.function.get_diagnosa;
UpdateKunjungan_RWJIGD.function.form_diagnosa;
UpdateKunjungan_RWJIGD.variable.parameter 	= {};
UpdateKunjungan_RWJIGD.variable.parameter.no_asuransi;
UpdateKunjungan_RWJIGD.variable.parameter.nama_peserta;
UpdateKunjungan_RWJIGD.variable.parameter.no_sep;
UpdateKunjungan_RWJIGD.variable.parameter.kd_unit;
UpdateKunjungan_RWJIGD.variable.parameter.kd_dokter;
UpdateKunjungan_RWJIGD.variable.parameter.kd_customer;
UpdateKunjungan_RWJIGD.variable.parameter.kd_rujukan;
UpdateKunjungan_RWJIGD.variable.parameter.diagnosa;

UpdateKunjungan_RWJIGD.object 	= {};
UpdateKunjungan_RWJIGD.object.dialog;
UpdateKunjungan_RWJIGD.object.penyakit;
UpdateKunjungan_RWJIGD.object.grid = {};
UpdateKunjungan_RWJIGD.object.grid.penyakit;
UpdateKunjungan_RWJIGD.object.form = {};
UpdateKunjungan_RWJIGD.object.form.kd_pasien 	= new Ext.form.TextField( { fieldLabel : 'Medrec', width : '100%', readOnly : true, } );
UpdateKunjungan_RWJIGD.object.form.nama 		= new Ext.form.TextField( { fieldLabel : 'Nama Pasien', width : '100%', readOnly : true, } );
UpdateKunjungan_RWJIGD.object.form.unit 		= new Ext.form.TextField( { fieldLabel : 'Unit', width : '100%', readOnly : true, } );
UpdateKunjungan_RWJIGD.object.form.tgl_masuk 	= new Ext.form.TextField( { fieldLabel : 'Tgl. Masuk', width : '100%', readOnly : true, format : 'd/M/Y' } );
UpdateKunjungan_RWJIGD.object.form.no_asuransi 	= new Ext.form.TextField( { fieldLabel : 'No Asuransi', width : '100%', readOnly : false, } );
UpdateKunjungan_RWJIGD.object.form.nama_peserta	= new Ext.form.TextField( { fieldLabel : 'Nama Peserta', width : '100%', readOnly : false, } );
UpdateKunjungan_RWJIGD.object.form.no_sep		= new Ext.form.TextField( { fieldLabel : 'No SEP', width : '100%', readOnly : false, } );
UpdateKunjungan_RWJIGD.object.form.diagnosa		= new Ext.form.TextField( 
	{ 
		fieldLabel : 'Diagnosa', 
		width : '100%', 
		readOnly : false, 
		enableKeyEvents : true,
		listeners:{
			keydown:function(c,e){
				if (e.keyCode == 13){
					params = {
						text : UpdateKunjungan_RWJIGD.object.form.diagnosa.getValue(),
					};
					UpdateKunjungan_RWJIGD.function.get_diagnosa(params);
					UpdateKunjungan_RWJIGD.function.form_diagnosa(params);
				}   
			}
		}
	} 
);
UpdateKunjungan_RWJIGD.object.form.combo_unit 	= new Ext.form.ComboBox( { 
	fieldLabel 		: 'Unit', 
	width 			: '100%', 
	typeAhead 		: true,
	triggerAction 	: 'all',
	lazyRender 		: true,
	mode 			: 'local',
	selectOnFocus 	: true,
	forceSelection 	: true,
	emptyText 		: 'Pilih Unit...',
	align 			: 'Right',
	enableKeyEvents : true,
	store 			: UpdateKunjungan_RWJIGD.variable.data_store.unit,
	valueField 		: 'KD_UNIT',
	displayField 	: 'NAMA_UNIT',
	anchor 			: '100%',
	listeners 		: {
		select 		: function(a,b){
			UpdateKunjungan_RWJIGD.object.data.kd_unit = b.data.KD_UNIT;
			UpdateKunjungan_RWJIGD.function.get_dokter(b.data.KD_UNIT);
			UpdateKunjungan_RWJIGD.object.form.combo_dokter.setValue();
		}
	}
} );
UpdateKunjungan_RWJIGD.object.form.combo_dokter 	= new Ext.form.ComboBox( { 
	fieldLabel 		: 'Dokter', 
	width 			: '100%', 
	typeAhead 		: true,
	triggerAction 	: 'all',
	lazyRender 		: true,
	mode 			: 'local',
	selectOnFocus 	: true,
	forceSelection 	: true,
	emptyText 		: 'Pilih Dokter...',
	align 			: 'Right',
	enableKeyEvents : true,
	store 			: UpdateKunjungan_RWJIGD.variable.data_store.dokter,
	valueField 		: 'KD_DOKTER',
	displayField 	: 'NAMA',
	anchor 			: '100%',
	listeners 		: {
		select 		: function(a,b){
			UpdateKunjungan_RWJIGD.object.data.kd_dokter 		= b.data.KD_DOKTER;
		}
	}
} );

UpdateKunjungan_RWJIGD.object.form.combo_customer 	= new Ext.form.ComboBox( { 
	fieldLabel 		: 'Customer', 
	width 			: '100%', 
	typeAhead 		: true,
	triggerAction 	: 'all',
	lazyRender 		: true,
	mode 			: 'local',
	selectOnFocus 	: true,
	forceSelection 	: true,
	emptyText 		: 'Pilih Customer...',
	align 			: 'Right',
	enableKeyEvents : true,
	store 			: UpdateKunjungan_RWJIGD.variable.data_store.customer,
	valueField 		: 'KD_CUSTOMER',
	displayField 	: 'CUSTOMER',
	anchor 			: '100%',
	listeners 		: {
		select 		: function(a,b){
			UpdateKunjungan_RWJIGD.object.data.kd_customer = b.data.KD_CUSTOMER;
			if (b.data.KD_CUSTOMER == '0000000001') {
				UpdateKunjungan_RWJIGD.object.form.combo_rujukan.disable();
				UpdateKunjungan_RWJIGD.object.form.no_asuransi.disable();
				UpdateKunjungan_RWJIGD.object.form.nama_peserta.disable();
				UpdateKunjungan_RWJIGD.object.form.no_sep.disable();
				UpdateKunjungan_RWJIGD.object.form.combo_rujukan.setValue(0);
				UpdateKunjungan_RWJIGD.object.form.no_asuransi.setValue("");
				UpdateKunjungan_RWJIGD.object.form.nama_peserta.setValue("");
				UpdateKunjungan_RWJIGD.object.form.no_sep.setValue("");
				// UpdateKunjungan_RWJIGD.variable.parameter.kd_rujukan = 0;
			}else{
				UpdateKunjungan_RWJIGD.object.form.combo_rujukan.enable();
				UpdateKunjungan_RWJIGD.object.form.no_asuransi.enable();
				UpdateKunjungan_RWJIGD.object.form.nama_peserta.enable();
				UpdateKunjungan_RWJIGD.object.form.no_sep.enable();
				UpdateKunjungan_RWJIGD.object.form.combo_rujukan.setValue(UpdateKunjungan_RWJIGD.object.data.kd_rujukan);
				UpdateKunjungan_RWJIGD.object.form.no_asuransi.setValue(UpdateKunjungan_RWJIGD.object.data.no_asuransi);
				UpdateKunjungan_RWJIGD.object.form.nama_peserta.setValue(UpdateKunjungan_RWJIGD.object.data.nama_peserta);
				UpdateKunjungan_RWJIGD.object.form.no_sep.setValue(UpdateKunjungan_RWJIGD.object.data.no_sep);
				// UpdateKunjungan_RWJIGD.variable.parameter.kd_rujukan = UpdateKunjungan_RWJIGD.object.data.kd_rujukan;
			}
		}
	}
} );

UpdateKunjungan_RWJIGD.object.form.combo_rujukan 	= new Ext.form.ComboBox( { 
	fieldLabel 		: 'Rujukan', 
	width 			: '100%', 
	typeAhead 		: true,
	triggerAction 	: 'all',
	lazyRender 		: true,
	mode 			: 'local',
	selectOnFocus 	: true,
	forceSelection 	: true,
	emptyText 		: 'Pilih Rujukan...',
	align 			: 'Right',
	enableKeyEvents : true,
	store 			: UpdateKunjungan_RWJIGD.variable.data_store.rujukan,
	valueField 		: 'KD_RUJUKAN',
	displayField 	: 'RUJUKAN',
	anchor 			: '100%',
	listeners 		: {
		select 		: function(a,b){
			UpdateKunjungan_RWJIGD.object.data.cara_penerimaan 	= b.data.CARA_PENERIMAAN;
			UpdateKunjungan_RWJIGD.object.data.kd_rujukan 		= b.data.KD_RUJUKAN;
			// UpdateKunjungan_RWJIGD.variable.parameter.kd_rujukan = b.data.KD_RUJUKAN;
		}
	}
} );

UpdateKunjungan_RWJIGD.object.data = {};
UpdateKunjungan_RWJIGD.object.data.kd_pasien;
UpdateKunjungan_RWJIGD.object.data.nama_pasien;
UpdateKunjungan_RWJIGD.object.data.nama_dokter;
UpdateKunjungan_RWJIGD.object.data.customer;
UpdateKunjungan_RWJIGD.object.data.unit;
UpdateKunjungan_RWJIGD.object.data.urut_masuk;
UpdateKunjungan_RWJIGD.object.data.tgl_masuk;

UpdateKunjungan_RWJIGD.object.data.kd_unit;
UpdateKunjungan_RWJIGD.object.data.kd_dokter;
UpdateKunjungan_RWJIGD.object.data.kd_rujukan;
UpdateKunjungan_RWJIGD.object.data.no_asuransi;
UpdateKunjungan_RWJIGD.object.data.nama_peserta;
UpdateKunjungan_RWJIGD.object.data.no_sep;
UpdateKunjungan_RWJIGD.variable.selected_penyakit;
UpdateKunjungan_RWJIGD.object.data.diagnosa;
UpdateKunjungan_RWJIGD.object.data.kd_customer;
UpdateKunjungan_RWJIGD.object.data.cara_penerimaan;

UpdateKunjungan_RWJIGD.function.get_unit = function(kd_unit){
    UpdateKunjungan_RWJIGD.variable.data_store.unit.load({
		params:{
			Skip 	: 0,
			Take 	: 1000,
			Sort 	: 'NAMA_UNIT',
			Sortdir : 'ASC',
			target 	: 'ViewSetupUnitB',
			param 	: " kd_bagian ="+kd_unit.substring(0,1)+" and type_unit=false"
		}
	});
}
UpdateKunjungan_RWJIGD.function.get_customer = function(kd_unit){
    UpdateKunjungan_RWJIGD.variable.data_store.customer.load({
		params:{
			Skip: 0,
			Take: 1000,
			Sort: 'customer',
			Sortdir: 'ASC',
			target: 'ViewComboCustomer',
			param: ''
		}
	});
}
UpdateKunjungan_RWJIGD.function.get_dokter = function(kd_unit){
    UpdateKunjungan_RWJIGD.variable.data_store.dokter.load({
		params:{
			Skip: 0,
			Take: 1000,
			Sort: 'nama',
			Sortdir: 'ASC',
			target: 'ViewComboDokter',
			param: 'where dk.kd_unit=~' + kd_unit + '~'  + 'and d.jenis_dokter=1 '
		}
	});
}
UpdateKunjungan_RWJIGD.function.get_rujukan = function(){
    UpdateKunjungan_RWJIGD.variable.data_store.rujukan.load({
		params:{
			Skip: 0,
			Take: 1000,
			Sort: 'rujukan',
			Sortdir: 'ASC',
			target: 'ViewComboRujukan',
		},
	});
}

UpdateKunjungan_RWJIGD.function.get_data = function (kd_pasien, kd_unit){
	Ext.Ajax.request({
		url 		: baseURL + "index.php/main/Controller_kunjungan/get_custom_data",
		params 		: {
			select 	: " * ",
			where 	: " kd_pasien = '"+kd_pasien+"' AND left(kd_unit, 1) = '"+kd_unit.substring(0,1)+"' order by tgl_masuk desc limit 1",
			table 	: " kunjungan ",
		},
		success: function(o){
			var cst 	= Ext.decode(o.responseText);
			UpdateKunjungan_RWJIGD.function.get_dokter(cst[0].kd_unit);
			UpdateKunjungan_RWJIGD.function.get_customer();
			UpdateKunjungan_RWJIGD.function.get_rujukan();
			UpdateKunjungan_RWJIGD.object.form.kd_pasien.setValue(cst[0].kd_pasien);
			UpdateKunjungan_RWJIGD.object.form.tgl_masuk.setValue(cst[0].tgl_masuk);
			UpdateKunjungan_RWJIGD.object.data.kd_pasien_asal 	= cst[0].kd_pasien;
			UpdateKunjungan_RWJIGD.object.data.kd_customer 		= cst[0].kd_customer;
			UpdateKunjungan_RWJIGD.object.data.kd_dokter 		= cst[0].kd_dokter;
			UpdateKunjungan_RWJIGD.object.data.kd_unit_asal 	= cst[0].kd_unit;
			UpdateKunjungan_RWJIGD.object.data.kd_unit 			= cst[0].kd_unit;
			UpdateKunjungan_RWJIGD.object.data.urut_masuk_asal 	= cst[0].urut_masuk;
			UpdateKunjungan_RWJIGD.object.data.tgl_masuk_asal 	= cst[0].tgl_masuk;
			UpdateKunjungan_RWJIGD.object.data.no_sep 			= cst[0].no_sjp;
			UpdateKunjungan_RWJIGD.object.form.no_sep.setValue(cst[0].no_sjp);

			Ext.Ajax.request({
				url 		: baseURL + "index.php/main/Controller_kunjungan/get_custom_data",
				params 		: {
					select 	: " kd_unit, nama_unit ",
					where 	: " kd_unit = '"+cst[0].kd_unit+"' ",
					table 	: " unit ",
				},
				success: function(res){
					var rest 	= Ext.decode(res.responseText);
					UpdateKunjungan_RWJIGD.object.data.unit = rest[0].nama_unit;
					UpdateKunjungan_RWJIGD.object.form.unit.setValue(rest[0].nama_unit);
					UpdateKunjungan_RWJIGD.object.form.combo_unit.setValue(cst[0].kd_unit);
				}
			});

			Ext.Ajax.request({
				url 		: baseURL + "index.php/main/Controller_kunjungan/get_custom_data",
				params 		: {
					select 	: " kd_pasien, nama, no_asuransi ",
					where 	: " kd_pasien = '"+cst[0].kd_pasien+"' ",
					table 	: " pasien ",
				},
				success: function(res){
					var rest 	= Ext.decode(res.responseText);
					UpdateKunjungan_RWJIGD.object.form.nama.setValue(rest[0].nama);
					UpdateKunjungan_RWJIGD.object.data.nama_pasien 		= rest[0].nama;
					UpdateKunjungan_RWJIGD.object.data.no_asuransi 		= rest[0].no_asuransi;
					UpdateKunjungan_RWJIGD.object.data.nama_peserta 	= rest[0].nama;
					UpdateKunjungan_RWJIGD.object.form.no_asuransi.setValue(rest[0].no_asuransi);
					UpdateKunjungan_RWJIGD.object.form.nama_peserta.setValue(rest[0].nama);
				}
			});

			Ext.Ajax.request({
				url 		: baseURL + "index.php/main/Controller_kunjungan/get_custom_data",
				params 		: {
					select 	: " kd_dokter, nama ",
					where 	: " kd_dokter = '"+cst[0].kd_dokter+"' ",
					table 	: " dokter ",
				},
				success: function(res){
					var rest 	= Ext.decode(res.responseText);
					UpdateKunjungan_RWJIGD.object.form.combo_dokter.setValue(rest[0].kd_dokter);
					UpdateKunjungan_RWJIGD.object.data.nama_dokter = rest[0].nama;
				}
			});

			Ext.Ajax.request({
				url 		: baseURL + "index.php/main/Controller_kunjungan/get_custom_data",
				params 		: {
					select 	: " kd_customer, customer ",
					where 	: " kd_customer = '"+cst[0].kd_customer+"' ",
					table 	: " customer ",
				},
				success: function(res){
					var rest 	= Ext.decode(res.responseText);
					UpdateKunjungan_RWJIGD.object.data.kd_customer = rest[0].kd_customer;
					UpdateKunjungan_RWJIGD.object.form.combo_customer.setValue(rest[0].kd_customer);
				}
			});

			Ext.Ajax.request({
				url 		: baseURL + "index.php/main/Controller_kunjungan/get_custom_data",
				params 		: {
					select 	: " kd_rujukan, rujukan, cara_penerimaan ",
					where 	: " kd_rujukan = '"+cst[0].kd_rujukan+"' ",
					table 	: " rujukan ",
				},
				success: function(res){
					var rest 	= Ext.decode(res.responseText);
					UpdateKunjungan_RWJIGD.object.form.combo_rujukan.setValue(rest[0].kd_rujukan);
					UpdateKunjungan_RWJIGD.object.data.kd_rujukan 		= rest[0].kd_rujukan;
					UpdateKunjungan_RWJIGD.object.data.cara_penerimaan 	= rest[0].cara_penerimaan;
				}
			});
			Ext.Ajax.request({
				url 		: baseURL + "index.php/main/Controller_kunjungan/get_custom_data",
				params 		: {
					select 	: " * ",
					where 	: " kd_pasien = '"+cst[0].kd_pasien+"' and kd_unit = '"+cst[0].kd_unit+"' and tgl_masuk = '"+cst[0].tgl_masuk+"' and urut_masuk = '"+cst[0].urut_masuk+"' ",
					table 	: " mr_penyakit ",
				},
				success: function(res){
					var rest 	= Ext.decode(res.responseText);
					if (rest.length > 0) {
						Ext.Ajax.request({
							url 		: baseURL + "index.php/main/Controller_kunjungan/get_custom_data",
							params 		: {
								select 	: " * ",
								where 	: " kd_penyakit = '"+rest[0].kd_penyakit+"' ",
								table 	: " penyakit ",
							},
							success: function(res){
								var rest 	= Ext.decode(res.responseText);
								UpdateKunjungan_RWJIGD.object.form.diagnosa.setValue(rest[0].kd_penyakit+" - "+rest[0].penyakit);
								UpdateKunjungan_RWJIGD.object.data.diagnosa = rest[0].kd_penyakit;
							}
						});
					}
				}
			});
		}
	});
}
UpdateKunjungan_RWJIGD.function.dialog = function (kd_pasien, kd_unit){
	UpdateKunjungan_RWJIGD.function.get_unit(kd_unit);
	UpdateKunjungan_RWJIGD.object.dialog = new Ext.Window({
		title: 'Formulir',
		closeAction: 'destroy',
		width: 500,
		height: 430, //575,
		resizable: false,
		constrain: true,
		autoScroll: false,
		iconCls: 'Studi_Lanjut',
		modal: true,
		items:[
			{
				layout 		: 'form',
				defaults 	: 'textfield',
				bodyStyle 	: 'padding:5px',
				items 		: [
					UpdateKunjungan_RWJIGD.object.form.kd_pasien,
					UpdateKunjungan_RWJIGD.object.form.nama,
					UpdateKunjungan_RWJIGD.object.form.unit,
					UpdateKunjungan_RWJIGD.object.form.tgl_masuk,
				]
			},{
				layout 		: 'form',
				defaults 	: 'textfield',
				bodyStyle 	: 'padding:5px',
				items 		: [
					UpdateKunjungan_RWJIGD.object.form.combo_unit,
					UpdateKunjungan_RWJIGD.object.form.combo_dokter,
					UpdateKunjungan_RWJIGD.object.form.combo_customer,
					UpdateKunjungan_RWJIGD.object.form.combo_rujukan,
					UpdateKunjungan_RWJIGD.object.form.no_asuransi,
					UpdateKunjungan_RWJIGD.object.form.nama_peserta,
					UpdateKunjungan_RWJIGD.object.form.no_sep,
					UpdateKunjungan_RWJIGD.object.form.diagnosa,
				]
			}
		],
		fbar 	: [
			{
				xtype 	: 'button',
				text 	: 'Update',
				handler : function(){
					UpdateKunjungan_RWJIGD.object.data.no_asuransi 		= UpdateKunjungan_RWJIGD.object.form.no_asuransi.getValue();
					UpdateKunjungan_RWJIGD.object.data.nama_peserta 	= UpdateKunjungan_RWJIGD.object.form.nama_peserta.getValue();
					UpdateKunjungan_RWJIGD.object.data.no_sep 			= UpdateKunjungan_RWJIGD.object.form.no_sep.getValue();
					Ext.Ajax.request({
						method:'POST',
						url: baseURL + "index.php/main/Controller_kunjungan/update",
						params:{
							parameter : JSON.stringify(UpdateKunjungan_RWJIGD.object.data),
						},	
						success:function(o){
							var cst  = Ext.decode(o.responseText);
							Ext.MessageBox.alert('Informasi', cst.message);
							UpdateKunjungan_RWJIGD.object.dialog.hide();
							polipilihanpasien = cst.data.kd_unit;
						}
					});
				}
			},
			{
				xtype 	: 'button',
				text 	: 'Keluar',
				handler : function(){
					UpdateKunjungan_RWJIGD.object.dialog.hide();				
				}
			}
		],
		listeners:{
			afterShow: function (){
				this.activate();
			}
		}
	});
	// return UpdateKunjungan_RWJIGD.object.dialog;
	UpdateKunjungan_RWJIGD.object.dialog.show();
	UpdateKunjungan_RWJIGD.function.get_data(kd_pasien, kd_unit);
}

UpdateKunjungan_RWJIGD.function.get_diagnosa = function(params){
	Ext.Ajax.request({
		url: baseURL +  "index.php/main/functionRWJ/getPenyakit",
		params: params,
		success: function(response) {
			var cst  = Ext.decode(response.responseText); 
			// PilihDiagnosaLookUp_PendaftaranRWI();
			if (cst['listData'].length > 0) {
				for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
					var recs    = [],recType = UpdateKunjungan_RWJIGD.variable.data_store.diagnosa.recordType;
					var o       = cst['listData'][i];
					recs.push(new recType(o));
					UpdateKunjungan_RWJIGD.variable.data_store.diagnosa.add(recs);
				}

				UpdateKunjungan_RWJIGD.object.grid.penyakit.getView().refresh();
				UpdateKunjungan_RWJIGD.object.grid.penyakit.getSelectionModel().selectRow(0);
				UpdateKunjungan_RWJIGD.object.grid.penyakit.getView().focusRow(0);
			}else{
				UpdateKunjungan_RWJIGD.object.penyakit.hide();
				Ext.MessageBox.alert('Informasi', 'Tindakan '+params.text+' tidak ada', function(){
					UpdateKunjungan_RWJIGD.object.form.diagnosa.focus(true,10);
				});
			}
		},
	});
}

UpdateKunjungan_RWJIGD.function.form_diagnosa = function(params) {
	UpdateKunjungan_RWJIGD.object.penyakit = new Ext.Window({
		title: 'Daftar diagnosa',
		closeAction: 'destroy',
		width: 600,
		height: 400,
		border: false,
		resizable: false,
		plain: true,
		layout: 'fit',
		iconCls: 'Request',
		modal: true,
		bodyStyle:'padding: 3px;',
		items: [
			UpdateKunjungan_RWJIGD.object.grid.penyakit = new Ext.grid.EditorGridPanel({
				store 				: UpdateKunjungan_RWJIGD.variable.data_store.diagnosa,
				clicksToEdit 		: 1, 
				editable 			: true,
				border 				: true,
				columnLines 		: true,
				frame 				: false,
				stripeRows 			: true,
				trackMouseOver 		: true,
				anchor 				: '100%',
				autoScroll 			: true,
				sm: new Ext.grid.RowSelectionModel
				(
					{
						singleSelect: true,
						listeners:
						{
							rowselect: function (sm, row, rec)
							{
								rowLine = row;
								UpdateKunjungan_RWJIGD.variable.selected_penyakit = undefined;
								UpdateKunjungan_RWJIGD.variable.selected_penyakit = UpdateKunjungan_RWJIGD.variable.data_store.diagnosa.getAt(row);
							}
						}
					}
				),
				cm: new Ext.grid.ColumnModel({
				// specify any defaults for each column
				defaults: {
					sortable: false // columns are not sortable by default           
				},
				columns: [
					{
						header: 'Kode Penyakit',
						dataIndex: 'kd_penyakit',
						width: 25,
					}, 
					{
						header: 'Penyakit',
						dataIndex: 'penyakit',
						width: 75,
					}, 
				]
				}),
				viewConfig: {
					forceFit: true
				},
				listeners: {
					'keydown' : function(e, d){
						if(e.getKey() == 13){
							UpdateKunjungan_RWJIGD.object.data.diagnosa 		= UpdateKunjungan_RWJIGD.variable.selected_penyakit.data.kd_penyakit;
							UpdateKunjungan_RWJIGD.object.form.diagnosa.setValue(UpdateKunjungan_RWJIGD.variable.selected_penyakit.data.kd_penyakit+" - "+UpdateKunjungan_RWJIGD.variable.selected_penyakit.data.penyakit);
							UpdateKunjungan_RWJIGD.object.penyakit.hide();
						}
					}
		        }  
	        }
	    )
		],
		}
	);
	UpdateKunjungan_RWJIGD.object.penyakit.show();
};