/// <reference path="../../ext-base.js" />
/// <reference path="../../ext-all.js" />

	
var rowSelectedLookHistoryAsset;
var vWinFormEntryLookupHistoryAsset;
var selectFilterMaintenanceHistory=0;
var selectFilterRepairHistory=0;
var dsLookHistoryAssetList;
var mCriteria;
var selectServiceHistory='xxx';
var ServiceHistory=' All';
var dsServiceHistory;
var now = new Date();

function FormLookupHistoryAsset(criteria,NamaAset) 
{

	var strNamaAsetLookupHistoryAsset = nmTitleFormHistoryAset;
	if(NamaAset != '')
	{
		strNamaAsetLookupHistoryAsset = nmTitleFormHistoryAset + ' ( ' + NamaAset + ' )';
	};
	
	rowSelectedLookHistoryAsset = undefined;
	
	var pnlItemAsetHistory = new Ext.Panel  
	(
		{
			id: 'pnlItemAsetHistory',
			region: 'center',
			layout: 'form',
			border:true,
			height: 384,
			title: '',
			items: 
			[
				getBarHistoryAset(criteria),
				GetPanelLookUpHistoryAsset(criteria)
			]
		}
	)
	
    vWinFormEntryLookupHistoryAsset = new Ext.Window
	(
		{
			id: 'vWinFormEntryLookupHistoryAsset',
			title: strNamaAsetLookupHistoryAsset,
			closeAction: 'hide',
			closable:false,
			width: 700,
			height: 450,
			border: false,
			plain: true,
			resizable:false,
			layout: 'form',
			iconCls: 'Informasi',
			modal: true,                                   
			items: 
			[
				pnlItemAsetHistory,getItemPanelLookupHistoryAsset()
			]
		}
	);
	
	

    vWinFormEntryLookupHistoryAsset.show();
};

function GetPanelLookUpHistoryAsset(criteria)
{
	
	var FormLookUpHistoryAsset = new Ext.Panel  
	(
		{
			id: 'FormLookUpHistoryAsset',
			region: 'center',
			layout: 'form',
			title: '',
			bodyStyle: 'padding:15px',
			anchor:'100%',
			border: false,
			bodyStyle: 'background:#FFFFFF;',
			height: 442,
			shadhow: true,
			items: 
			[
				fnGetDTLGridLookUpHistoryAsset(criteria)
			],
			tbar: 
			[	
				' ', nmMaintHistoryAset + ' : ', ' ',										
				mComboFilterMaintenanceHistory(),
				' ', '-', nmRepairHistoryAset + ' : ', ' ',
				mComboFilterRepairHistory()				
			]
        }
	);
	
	return FormLookUpHistoryAsset;
};

function getBarHistoryAset(criteria)
{
	var PnlgetBarHistoryAset = new Ext.Panel  
	(
		{
			id: 'PnlgetBarHistoryAset',
			region: 'center',
			layout: 'form',
			title: '',
			height:0,
			border: false,
			items:[],
			tbar: 
			[							
				' ', nmDateHistoryAset + ' : ', ' ',
				{
				    xtype: 'datefield',
				    fieldLabel: nmDateHistoryAset + ' ',
				    id: 'dtpTglAwalFilterHistory',
				    format: 'd/M/Y',
				    value: now,
				    width: 100,
					listeners:  
					{
						'change': function(a,b,c)
						{   							
							getDataHistory();
						} 
					},
				    onInit: function() { }
				}, ' ', ' ' + nmSd + ' ', ' ', {
				    xtype: 'datefield',
				    fieldLabel: nmSd + ' ',
				    id: 'dtpTglAkhirFilterHistory',
				    format: 'd/M/Y',
				    value: now,
					listeners:  
					{
						'change': function(a,b,c)
						{   							
							getDataHistory();
						} 
					},
				    width: 100
				},' ', '-', nmServiceHistoryAset + ' : ', ' ',
				mComboServiceHistory(criteria)//,
				// ' ', '-', 'Maintenance : ', ' ',										
				// mComboFilterMaintenanceHistory(),
				// ' ', '-', 'Repair : ', ' ',
				// mComboFilterRepairHistory()				
			]
		}
	)
	
	return PnlgetBarHistoryAset;
}

function getItemPanelLookupHistoryAsset() 
{
    var items =
	{
	    layout: 'form',
	    border: true,
		height:39,
		anchor:'100%',
		style:{'margin-top':'-1px'},
	    items:
		[
			{
				layout: 'hBox',
				width:670,
				border: false,
				bodyStyle: 'padding:5px 0px 5px 5px',
				defaults: { margins: '3 3 3 3' },
				anchor: '98%',
				layoutConfig: 
				{
					align: 'middle',
					pack:'end'
				},
				items:
				[
					{
						xtype :'label',
						text: nmTotalHistoryAset + ' : '
					},
					{
					    xtype: 'textfield',
					    fieldLabel: ' ',
						style: 
							{
								'font-weight': 'bold',
								'text-align':'right'
							},
					    name: 'txtTotalHistory',
					    id: 'txtTotalHistory',
						readOnly:true
					},
					{
						xtype:'spacer',
						flex:1
					},
					{
						xtype:'button',
						text:nmBtnOK,
						width:70,
						hideLabel:true,
						id: 'btnCancelLookupHistoryAsset',
						handler:function() 
						{
							vWinFormEntryLookupHistoryAsset.close();
						}
					}
				]
			}
		]
	}
    return items;
};

function ShowPesanWarningLookupHistoryAsset(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:300
		}
	);
};

function fnGetDTLGridLookUpHistoryAsset(criteria) 
{  
	
	var fldDetail = ['SERVICE_ID','SERVICE_NAME','DUE_DATE','LAST_COST','DESCRIPTION','FINISH_DATE','ASSET_MAINT_ID','ASSET_MAINT_NAME','Jenis','Repair'];
	dsLookHistoryAssetList = new WebApp.DataStore({ fields: fldDetail });

	var vGridLookHistoryAssetFormEntry = new Ext.grid.EditorGridPanel
	(
		{ 
			id:'vGridLookHistoryAssetFormEntry',
			title: '',
			stripeRows: true,
			store: dsLookHistoryAssetList,
			height:333,
			columnLines:true,
			bodyStyle: 'padding:0px',
			border: false,	
			frame:false,
			sm: new Ext.grid.RowSelectionModel
			(
				{ 
					singleSelect: true,
					listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedLookHistoryAsset = dsLookHistoryAssetList.getAt(row);
						}
					}
				}
			),
			cm: fnGridLookHistoryAssetColumnModel(),
			viewConfig: { forceFit: true }
			//,			
			// bbar: new Ext.PagingToolbar({
            // pageSize: 1,
            // store: dsLookHistoryAssetList,
            // displayInfo: true,
            // displayMsg: 'Displaying History {0} - {1} of {2}',
            // emptyMsg: "No History to display"            
        // })
		}
	);


	// dsLookHistoryAssetList.load
	// (
		// { 
			// params: 
			// { 
				// Skip: 0, 
				// Take: 10000, 
				// Sort: 'SERVICE_ID', 
				// Sortdir: 'ASC', 
				// target:'viHistoryAsset',				
				// param: criteria
			// }
		// }
	// );
	

	// GetTotalHistory(criteria);
	mCriteria=criteria;
	RefreshDataHistory(criteria);
			
	return vGridLookHistoryAssetFormEntry;
};

function RefreshDataHistory(criteria)
{
	dsLookHistoryAssetList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 10000, 
				//Sort: 'SERVICE_ID',
                                Sort: 'finish_date',
				Sortdir: 'ASC', 
				target:'viHistoryAsset',				
				param: criteria
			}
		}
	);
	
	GetTotalHistory(criteria);
	return dsLookHistoryAssetList;
	
};

function fnGridLookHistoryAssetColumnModel() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{ 
				id: 'colKdHistoryAssetLook',
				header: nmServIDHistoryAset,
				dataIndex: 'SERVICE_ID',
				width: 70,
				hidden:true
			},
			{ 
				id: 'colHistoryAssetNameLook',
				header: nmServNameHistoryAset,
				dataIndex: 'SERVICE_NAME',
				width: 200
            },
			{ 
				id: 'colHistoryAssetNameLook',
				header: nmDateHistoryAset,
				dataIndex: 'FINISH_DATE',
				width: 90,
				renderer: function(v, params, record) 
						{
					        return ShowDate(record.data.FINISH_DATE);
					    }
            },
			{ 
				id: 'colBiayaAssetNameLook',
				header: nmCostHistoryAset,
				dataIndex: 'LAST_COST',
				align:'right',
				width: 90,
				 renderer: function(v, params, record) 
						{
					        return formatCurrency(record.data.LAST_COST);
					    }
            },
			{ 
				id: 'colJenisLook',
				header: nmMaintHistoryAset,
				dataIndex: 'Jenis',
				width: 90
            },
			{ 
				id: 'colRepairNameLook',
				header: nmRepairHistoryAset,
				dataIndex: 'Repair',
				width: 70
            }
			,
			{ 
				id: 'colDescAssetNameLook',
				header: nmDescHistoryAset,
				dataIndex: 'DESCRIPTION',
				width: 130
            }
		]
	)
};

function GetTotalHistory(criteria)
{
	 Ext.Ajax.request
	 (
		{
            //url: "./Module.mvc/ExecProc",
            url: baseURL + "index.php/main/ExecProc",
            params: 
			{
                UserID: 'Admin',
                ModuleID: 'ProsesGetTotalHistory',
				Params:	criteria		
            },
            success: function(o) 
			{
                var cst = Ext.decode(o.responseText);

				if (cst.success === true) 
				{
					Ext.get('txtTotalHistory').dom.value=formatCurrency(cst.Total);					
				}
				else
				{
					Ext.get('txtTotalHistory').dom.value=0;
					
				};
            }

        }
	);
};

function mComboFilterMaintenanceHistory()
{
  var cboFilterMaintenanceHistory = new Ext.form.ComboBox
	(
		{
			id:'cboFilterMaintenanceHistory',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: nmMaintHistoryAset + ' ',			
			width:150,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[0, 'All'], [1, 'Corrective Maintenance'],[2, 'Preventive Maintenance']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectFilterMaintenanceHistory,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectFilterMaintenanceHistory=b.data.Id ;
					getDataHistory();
				} 
			}
		}
	);
	return cboFilterMaintenanceHistory;
};

function mComboFilterRepairHistory()
{
  var cboFilterRepairHistory = new Ext.form.ComboBox
	(
		{
			id:'cboFilterRepairHistory',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: nmRepairHistoryAset + ' ',			
			width:90,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[0, 'All'], [1, 'Internal'],[2, 'External']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectFilterRepairHistory,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectFilterRepairHistory=b.data.Id ;
					getDataHistory();
				} 
			}
		}
	);
	return cboFilterRepairHistory;
};

function mComboServiceHistory(criteria)
{
	var Field = ['SERVICE_ID', 'SERVICE_NAME'];
	dsServiceHistory = new WebApp.DataStore({ fields: Field });

	dsServiceHistory.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'SERVICE_NAME',
                            Sort: 'service_name',
			    Sortdir: 'ASC',
			    target: 'viComboAssetService',
			    param: criteria
			}
		}
	);
	
  var cboServiceHistory = new Ext.form.ComboBox
	(
		{
			id:'cboServiceHistory',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',			
			fieldLabel: nmServiceHistoryAset + ' ',			
			anchor:'80%',
			store: dsServiceHistory,
			valueField: 'SERVICE_ID',
			displayField: 'SERVICE_NAME',
			value:ServiceHistory,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectServiceHistory=b.data.SERVICE_ID ;
					getDataHistory();
				} 
			}
		}
	);
	
	dsServiceHistory.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    //Sort: 'SERVICE_NAME',
                            Sort: 'service_name',
			    Sortdir: 'ASC',
			    target: 'viComboAssetService',
			     param: criteria
			}
		}
	);
	
	return cboServiceHistory;
};


function getDataHistory()
{
	var cKtiteria;
	cKtiteria=mCriteria;
	//cKtiteria += ' AND Finish_Date BETWEEN ~' + Ext.get('dtpTglAwalFilterHistory').getValue() + '~';
        cKtiteria += ' AND finish_date BETWEEN ~' + Ext.get('dtpTglAwalFilterHistory').getValue() + '~';
	cKtiteria += ' AND ~' + Ext.get('dtpTglAkhirFilterHistory').getValue() + '~';
	if (selectServiceHistory !='xxx')
	{
		//cKtiteria +=' AND Service_Id=~' + selectServiceHistory + '~'
                cKtiteria +=' AND service_id = ~' + selectServiceHistory + '~'
	};
	if (selectFilterMaintenanceHistory === 1)
	{
		//cKtiteria +=' AND Jenis=~CM~'
                cKtiteria +=' AND jenis = ~CM~'
	};
	if (selectFilterMaintenanceHistory === 2)
	{
		//cKtiteria +=' AND Jenis=~PM~'
                cKtiteria +=' AND jenis = ~PM~'
	};
	if (selectFilterRepairHistory === 1)
	{
		//cKtiteria +=' AND Repair=~Internal~'
                cKtiteria +=' AND repair = ~Internal~'
	};
	if (selectFilterRepairHistory === 2)
	{
		//cKtiteria +=' AND Repair=~External~'
                cKtiteria +=' AND repair = ~External~'
	};
	RefreshDataHistory(cKtiteria);
};

///---------------------------------------------------------------------------------------///