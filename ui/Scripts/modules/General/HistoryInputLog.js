/// <reference path="../../ext-base.js" />
/// <reference path="../../ext-all.js" />


	
var rowSelectedLookHistoryInputLog;
var vWinFormEntryLookupHistoryInputLog;
var dsLookHistoryInputLogList;
var mCriteriaHistoryInputLog;
var now = new Date();

function FormLookupHistoryInputLog(criteria,NamaAset) 
{
	var strNamaAsetHistoryInputLog = nmTitleFormHistoryInputLog;
	if(NamaAset != '')
	{
		strNamaAsetHistoryInputLog = nmTitleFormHistoryInputLog + ' ( ' + NamaAset + ' )';
	};
	
	rowSelectedLookHistoryInputLog = undefined;
	
	var pnlItemAsetHistoryInputLog = new Ext.Panel  
	(
		{
			id: 'pnlItemAsetHistoryInputLog',
			region: 'center',
			layout: 'form',
			border:true,
			height: 384,
			title: '',
			items: 
			[
				getBarHistoryInputLog(criteria),
				GetPanelLookUpHistoryInputLog(criteria)
			]
		}
	)
	
    vWinFormEntryLookupHistoryInputLog = new Ext.Window
	(
		{
			id: 'vWinFormEntryLookupHistoryInputLog',
			title: strNamaAsetHistoryInputLog,
			closeAction: 'hide',
			closable:false,
			width: 500,
			height: 450,
			border: false,
			plain: true,
			resizable:false,
			layout: 'form',
			iconCls: 'Informasi',
			modal: true,                                   
			items: 
			[
				pnlItemAsetHistoryInputLog,getItemPanelLookupHistoryInputLog()
			]
		}
	);
	
	

    vWinFormEntryLookupHistoryInputLog.show();
};



function getBarHistoryInputLog(criteria)
{
	var PnlgetBarHistoryInputLog = new Ext.Panel  
	(
		{
			id: 'PnlgetBarHistoryInputLog',
			region: 'center',
			layout: 'form',
			title: '',
			height:0,
			border: false,
			items:[],
			tbar: 
			[							
				' ', nmDateHistoryAset + ' : ', ' ',
				{
				    xtype: 'datefield',
				    fieldLabel: nmDateHistoryAset + ' ',
				    id: 'dtpTglAwalFilterHistory',
				    format: 'd/M/Y',
				    value: now,
				    width: 100,
					listeners:  
					{
						'change': function(a,b,c)
						{   							
							getDataHistoryInputLog();
						} 
					},
				    onInit: function() { }
				}, ' ', ' ' + nmSd + ' ', ' ', {
				    xtype: 'datefield',
				    fieldLabel: nmSd + ' ',
				    id: 'dtpTglAkhirFilterHistory',
				    format: 'd/M/Y',
				    value: now,
					listeners:  
					{
						'change': function(a,b,c)
						{   							
							getDataHistoryInputLog();
						} 
					},
				    width: 100
				}			
			]
		}
	)
	
	return PnlgetBarHistoryInputLog;
}

function getItemPanelLookupHistoryInputLog() 
{
    var items =
	{
	    layout: 'form',
	    border: true,
		height:39,
		anchor:'100%',
		style:{'margin-top':'-1px'},
	    items:
		[
			{
				layout: 'hBox',
				width:470,
				border: false,
				bodyStyle: 'padding:5px 0px 5px 5px',
				defaults: { margins: '3 3 3 3' },
				anchor: '98%',
				layoutConfig: 
				{
					align: 'middle',
					pack:'end'
				},
				items:
				[
					
					{
						xtype:'button',
						text:nmBtnOK,
						width:70,
						hideLabel:true,
						id: 'btnCancelLookupHistoryInputLog',
						handler:function() 
						{
							vWinFormEntryLookupHistoryInputLog.close();
						}
					}
				]
			}
		]
	}
    return items;
};

function ShowPesanWarningLookupHistoryInputLog(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:300
		}
	);
};

function GetPanelLookUpHistoryInputLog(criteria)
{
	
	var FormLookUpHistoryInputLog = new Ext.Panel  
	(
		{
			id: 'FormLookUpHistoryInputLog',
			region: 'center',
			layout: 'form',
			title: '',
			bodyStyle: 'padding:15px',
			anchor:'100%',
			border: false,
			bodyStyle: 'background:#FFFFFF;',
			height: 442,
			shadhow: true,
			items: 
			[
				fnGetDTLGridLookUpHistoryInputLog(criteria)
			]
        }
	);
	
	return FormLookUpHistoryInputLog;
};

function fnGetDTLGridLookUpHistoryInputLog(criteria) 
{  
	
	var fldDetail = ['HIST_LOG_ID','ASSET_MAINT_ID','ASSET_MAINT_NAME','EMP_ID','EMP_NAME','INPUT_DATE','METER','DESC_LOG'];
	dsLookHistoryInputLogList = new WebApp.DataStore({ fields: fldDetail });

	var vGridLookHistoryInputLogFormEntry = new Ext.grid.EditorGridPanel
	(
		{ 
			id:'vGridLookHistoryInputLogFormEntry',
			title: '',
			stripeRows: true,
			store: dsLookHistoryInputLogList,
			height:333,
			columnLines:true,
			bodyStyle: 'padding:0px',
			border: false,	
			frame:false,
			sm: new Ext.grid.RowSelectionModel
			(
				{ 
					singleSelect: true,
					listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedLookHistoryInputLog = dsLookHistoryInputLogList.getAt(row);
						}
					}
				}
			),
			cm: fnGridLookHistoryInputLogColumnModel(),
			viewConfig: { forceFit: true }
			
		}
	);

	mCriteriaHistoryInputLog=criteria;
	RefreshDataHistoryInputLog(criteria);
			
	return vGridLookHistoryInputLogFormEntry;
};

function RefreshDataHistoryInputLog(criteria)
{
	dsLookHistoryInputLogList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 10000, 
				//Sort: 'INPUT_DATE',
                                Sort: 'input_date',
				Sortdir: 'DESC', 
				target:'viHistoryInputLog',				
				param: criteria
			}
		}
	);
	
	
	return dsLookHistoryInputLogList;
	
};

function fnGridLookHistoryInputLogColumnModel() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{ 
				id: 'colInputDate',
				header: nmDateHistoryInputLog,
				dataIndex: 'INPUT_DATE',
				width: 30,
				renderer: function(v, params, record) 
						{
					        return ShowDate(record.data.INPUT_DATE);
					    }
			},
			{ 
				id: 'colMeterInputLog',
				header: nmMeterHistoryInputLog,
				dataIndex: 'METER',
				width: 20
            },
			{ 
				id: 'colEmpNameInputLog',
				header: nmEmpHistoryInputLog,
				dataIndex: 'EMP_NAME',
				width: 50
            }
		]
	)
};


function getDataHistoryInputLog()
{
	var cKtiteria;
	cKtiteria=mCriteriaHistoryInputLog;
	//cKtiteria += ' AND INPUT_DATE BETWEEN ~' + Ext.get('dtpTglAwalFilterHistory').getValue() + '~';
        cKtiteria += ' and input_date BETWEEN ~' + Ext.get('dtpTglAwalFilterHistory').getValue() + '~';
	cKtiteria += ' and ~' + Ext.get('dtpTglAkhirFilterHistory').getValue() + '~';
	
	RefreshDataHistoryInputLog(cKtiteria);
};

///---------------------------------------------------------------------------------------///