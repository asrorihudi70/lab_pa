var SetInfoVendorLookUps;
	
function InfoVendorLookUp(rowdata) 
{
	var lebar=600;
    SetInfoVendorLookUps = new Ext.Window   	
    (
		{
		    id: 'SetInfoVendorLookUps',
		    title: nmTitleFormInfoVendor,
		    closeAction: 'destroy',
			y:90,
		    width: lebar,
		    height: 277,
			resizable:false,
		    border: false,
		    plain: true,
		    layout: 'fit',
		    iconCls: 'Informasi',
		    modal: true,
		    items: getFormEntrySetInfoVendor(lebar),
		    listeners:
            {
                activate: function() 
                {
                },
				afterShow: function() 
				{ 
					this.activate(); 
				},
				deactivate: function()
				{
					rowSelectedLookVendor=undefined;
				}
            }
		}
	);


    SetInfoVendorLookUps.show();
	if (rowdata != undefined)
	{
		SetInfoVendorInit(rowdata)
	};	
};


function getFormEntrySetInfoVendor(lebar) 
{
    var pnlSetInfoVendor = new Ext.FormPanel
    (
		{
		    id: 'PanelSetInfoVendor',
		    fileUpload: true,
		    region: 'north',
		    layout: 'column',
		    height: 350,
		    anchor: '100%',
		    bodyStyle: 'padding:10px 0px 10px 10px',
		    iconCls: 'SetupInfoVendor',
		    border: true,
		    items:
			[
				{
				    layout: 'column',
				    width:lebar-34,
					bodyStyle: 'padding:10px 0px 10px 10px',
				    height: 226,
				    labelAlign: 'right',
				    anchor: '100%',
				    items:
					[
						{
						    columnWidth:.989,
						    layout: 'form',
						    id: 'PnlKiriSetInfoVendor',
							labelWidth:65,
						    border: false,
						    items:
							[
								{
								    xtype: 'textfield',
								    fieldLabel: nmKdVendor2 + ' ',
								    name: 'txtKode_SetInfoVendor',
								    id: 'txtKode_SetInfoVendor',
								    anchor: '40%',
									readOnly:true
								},
								{
								    xtype: 'textfield',
								    fieldLabel: nmVendor + ' ',
								    name: 'txtNameSetInfoVendor',
								    id: 'txtNameSetInfoVendor',
								    anchor: '100%',
									readOnly:true
								},
								{
								    xtype: 'textfield',
								    fieldLabel: nmVendorContact1 + ' ',
								    name: 'txtContact1SetInfoVendor',
								    id: 'txtContact1SetInfoVendor',
								    anchor: '100%',
									readOnly:true
								},
								{
								    xtype: 'textfield',
								    fieldLabel: nmVendorContact2 + ' ',
								    name: 'txtContact2SetInfoVendor',
								    id: 'txtContact2SetInfoVendor',
								    anchor: '100%',
									readOnly:true
								},
								{
								    xtype: 'textfield',
								    fieldLabel: nmVendorAddress + ' ',
								    name: 'txtAddressSetInfoVendor',
								    id: 'txtAddressSetInfoVendor',
								    anchor: '100%',
									readOnly:true
								},getItemPanelCitySetInfoVendor(),
								{
								    xtype: 'textfield',
								    fieldLabel: nmVendorCountry + ' ',
								    name: 'txtCountrySetInfoVendor',
								    id: 'txtCountrySetInfoVendor',
								    anchor: '69.3%',
									readOnly:true
								},getItemPanelPhoneSetInfoVendor()
							]
						}
					]
				}
			]
		}
	); 

    return pnlSetInfoVendor
};

function getItemPanelCitySetInfoVendor()
{
	var items= 			
	{
		layout:'column',
		border:false,
		items:
		[
			{
				columnWidth:.672,
				layout: 'form',
				border:false,
				items: 
				[ 	
					{
						xtype: 'textfield',
						fieldLabel: nmVendorCity + ' ',
						name: 'txtCitySetInfoVendor',
						id: 'txtCitySetInfoVendor',						
						anchor: '100%',
						readOnly:true
					}	
				]
			},
			{
				columnWidth:.3,
				layout: 'form',
				labelWidth:75,
				border:false,
				items: 
				[ 	
					{
						xtype: 'textfield',
						fieldLabel: nmVendorPosCode + ' ',
						name: 'txtPosCodeSetInfoVendor',
						id: 'txtPosCodeSetInfoVendor',						
						anchor: '100%',
						readOnly:true
					}	
				]
			}
		]
	}
	
	return items;
};



function getItemPanelPhoneSetInfoVendor()
{
	var items= 			
	{
		layout:'column',
		border:false,
		items:
		[
			{
				columnWidth:.486,
				layout: 'form',
				border:false,
				items: 
				[ 	
					{
						xtype: 'textfield',
						fieldLabel: nmVendorPhone1 + ' ',
						name: 'txtPhone1SetInfoVendor',
						id: 'txtPhone1SetInfoVendor',						
						anchor: '100%',
						readOnly:true
					}	
				]
			},
			{
				columnWidth:.486,
				layout: 'form',
				border:false,
				labelWidth:75,
				items: 
				[ 	
					{
						xtype: 'textfield',
						fieldLabel: nmVendorPhone2 + ' ',
						name: 'txtPhone2SetInfoVendor',
						id: 'txtPhone2SetInfoVendor',						
						anchor: '100%',
						readOnly:true
					}	
				]
			}
		]
	}
	
	return items;
};


//------------------------------------------------------------------------------------
function SetInfoVendorInit(rowdata) 
{
    Ext.get('txtKode_SetInfoVendor').dom.value = rowdata.data.VENDOR_ID;
    Ext.get('txtNameSetInfoVendor').dom.value = rowdata.data.VENDOR;
	
	if (rowdata.data.CONTACT1 === null)
	{
		Ext.get('txtContact1SetInfoVendor').dom.value = '';
	}
	else
	{
		Ext.get('txtContact1SetInfoVendor').dom.value = rowdata.data.CONTACT1
	};
	
	if (rowdata.data.CONTACT2 === null)
	{
		Ext.get('txtContact2SetInfoVendor').dom.value = '';
	}
	else
	{
		Ext.get('txtContact2SetInfoVendor').dom.value = rowdata.data.CONTACT2
	};
	
	if (rowdata.data.VEND_ADDRESS === null)
	{
		Ext.get('txtAddressSetInfoVendor').dom.value = '';
	}
	else
	{
		Ext.get('txtAddressSetInfoVendor').dom.value = rowdata.data.VEND_ADDRESS;
	};
	
	if (rowdata.data.VEND_CITY === null)
	{
		Ext.get('txtCitySetInfoVendor').dom.value = '';
	}
	else
	{
		Ext.get('txtCitySetInfoVendor').dom.value = rowdata.data.VEND_CITY;
	};
	
	if (rowdata.data.VEND_POS_CODE === null)
	{
		Ext.get('txtPosCodeSetInfoVendor').dom.value = '';
	}
	else
	{
		Ext.get('txtPosCodeSetInfoVendor').dom.value = rowdata.data.VEND_POS_CODE;
	};
	
	if (rowdata.data.COUNTRY === null)
	{
		Ext.get('txtCountrySetInfoVendor').dom.value = '';
	}
	else
	{
		Ext.get('txtCountrySetInfoVendor').dom.value = rowdata.data.COUNTRY;
	};
	
	if (rowdata.data.VEND_PHONE1 === null)
	{
		Ext.get('txtPhone1SetInfoVendor').dom.value = '';
	}
	else
	{
		Ext.get('txtPhone1SetInfoVendor').dom.value = rowdata.data.VEND_PHONE1;
	};
	
	if (rowdata.data.VEND_PHONE2 === null)
	{
		Ext.get('txtPhone2SetInfoVendor').dom.value = '';
	}
	else
	{
		Ext.get('txtPhone2SetInfoVendor').dom.value = rowdata.data.VEND_PHONE2;
	};
};




 



