var dsInputLogMeteringList;
var AddNewInputLogMetering;
var InputLogMeteringLookUps;
var selectLogTypeInputLogMetering;

var FocusCtrlLogMetering;

var strAssetID;

function InputLogMeteringLookUp(Asset_ID,Asset_Name) 
{	

	var strNamaAsetInputLogMetering = nmTitleFormInfoLogMet
	if(Asset_Name != '')
	{
		strNamaAsetInputLogMetering = nmTitleFormInfoLogMet + ' ( ' + Asset_Name + ' )';
	};
	
	var lebar=400;
    InputLogMeteringLookUps = new Ext.Window   	
    (
		{
		    id: 'InputLogMeteringLookUps',
		    title: strNamaAsetInputLogMetering,
		    closeAction: 'destroy',
		    width: lebar,
		    height: 250,//220,
			resizable:false,
		    border: false,
		    plain: true,
		    layout: 'fit',
		    iconCls: 'InputLogMetering',
		    modal: true,
		    items: getFormEntryInputLogMetering(lebar)
			//,
		    // listeners:
            // {
                // activate: function() 
                // {
                // },
				// afterShow: function() 
				// { 
					// this.activate(); 
				// },
				// deactivate: function()
				// {
					// InputLogMeteringLookUps=undefined;
				// }
            // }
		}
	);
	
	strAssetID=Asset_ID;

    InputLogMeteringLookUps.show();
	if (Asset_ID == undefined)
	{
		InputLogMeteringAddNew();
	}
	else
	{
		InputLogMeteringInit(Asset_ID)
	}	
};


function getFormEntryInputLogMetering(lebar) 
{
    var pnlInputLogMetering = new Ext.FormPanel
    (
		{
		    id: 'PanelInputLogMetering',
		    fileUpload: true,
		    region: 'north',
		    layout: 'column',
		    height: 205,//175,//350,
		    anchor: '100%',
		    bodyStyle: 'padding:10px 0px 10px 10px',
		    iconCls: 'InputLogMetering',
		    border: true,
		    items:
			[
				{
				    layout: 'form',
				    width:lebar-34,
					bodyStyle: 'padding:10px 0px 10px 10px',
				    height: 200,//170,//226,
				    labelAlign: 'right',
				    anchor: '100%',
				    items:
					[
						{
						    columnWidth:.989,
						    layout: 'form',
						    id: 'PnlKiriInputLogMetering',
							labelWidth:75,
						    border: false,
						    items:
							[	
								getItemPanelLogInputLogMetering(lebar),
								getItemPanelEmployee(),
								{
									xtype: 'checkbox',
									id: 'chkUpdateLastServiceInfoLogMet',
									name:'chkUpdateLastServiceInfoLogMet',
									hidden:true,
									style: 
									{
										'margin-top': '3.5px'
									},
									boxLabel: nmUpdateLastServInfoLogMet,
									//fieldLabel: nmUpdateLastServInfoLogMet + ' ',
									//anchor: '100%'
									width:200
								}
								//getItemPanelOkCancel()
							]
						},getItemPanelOkCancel()
					]
				}
			]//,
		    // tbar:
			// [
				// {
				    // id: 'btnAddInputLogMetering',
				    // text: nmTambah,
				    // tooltip: nmTambah,
				    // iconCls: 'add',				   
				    // handler: function() { InputLogMeteringAddNew() }
				// }, '-',
				// {
				    // id: 'btnSimpanInputLogMetering',
				    // text: nmSimpan,
				    // tooltip: nmSimpan,
				    // iconCls: 'save',				   
				    // handler: function() 
					// { 
						// InputLogMeteringSave(false);
						// //RefreshDataInputLogMetering();
					// }
				// }, '-',
				// {
				    // id: 'btnSimpanCloseInputLogMetering',
				    // text: nmSimpanKeluar,
				    // tooltip: nmSimpanKeluar,
				    // iconCls: 'saveexit',
				    // handler: function() 
					// {					
						// var x = InputLogMeteringSave(true);
						// //RefreshDataInputLogMetering();
						// if (x===undefined)
						// {
							// InputLogMeteringLookUps.close();
						// };
					// }
				// },'-',
				// {
				    // id: 'btnHapusInputLogMetering',
				    // text: nmHapus,
				    // tooltip: nmHapus,
				    // iconCls: 'remove',
				    // handler: function() 
					// {
							// InputLogMeteringDelete() ;
							// RefreshDataInputLogMetering();					
					// }
				// }
				// // ,'-','->','-',
				// // {
					// // id:'btnPrintInputLogMetering',
					// // text: nmCetak,
					// // tooltip: nmCetak,
					// // iconCls: 'print',					
					// // handler: function() {LoadReport(950012);}
				// // }
			// ]
		}
	); 
	
	return pnlInputLogMetering;	
	
};

function getItemPanelLogInputLogMetering(lebar)
{
	var items= 			
	{
		layout:'form',
		border:false,
		width:lebar-10,
		items:
		[
			{
				columnWidth:1,
				layout: 'form',
				labelWidth:75,
				border:false,
				items: 
				[ 	
					{
						xtype: 'textfield',
						fieldLabel: nmLastInfoLogMet + ' ',
						name: 'txtLast',
						id: 'txtLast'//,
						//anchor: '99%'
					},
					{
						xtype: 'numberfield',
						fieldLabel: nmCurrInfoLogMet + ' ',
						name: 'txtCurrent',
						id: 'txtCurrent'//,
						//anchor: '99%'
					},
					{
						xtype: 'textfield',
						fieldLabel: nmTargetInfoLogMet + ' ',
						name: 'txtTarget',
						id: 'txtTarget'//,
						//anchor: '99%'
					}
				]
			}
		]
	}
	
	return items;
};




function InputLogMeteringSave(mBol) 
{
	if (ValidasiEntryInputLogMetering(nmHeaderSimpanData) == 1 )
	{
		if (AddNewInputLogMetering == true) 
		{
			Ext.Ajax.request
			(
				{
					//url: "./Datapool.mvc/CreateDataObj",
                                        url: WebAppUrl.UrlSaveData,
					params: getParamInputLogMetering(),
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{	
							ShowPesanInfoInputLogMetering(nmPesanSimpanSukses,nmHeaderSimpanData);
							AddNewInputLogMetering = false;
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningInputLogMetering(nmPesanSimpanGagal,nmHeaderSimpanData);
						}
						else 
						{
							ShowPesanErrorInputLogMetering(nmPesanSimpanError,nmHeaderSimpanData);
						};
					}
				}
			)
		}
		else 
		{
			Ext.Ajax.request
			 (
				{
					//url: "./Datapool.mvc/UpdateDataObj",
                                        url: WebAppUrl.UrlUpdateData,
					params: getParamInputLogMetering(),
					success: function(o) 
					{
						// var cst = Ext.decode(o.responseText);
						// if (cst.success === true) 
						// {
							// ShowPesanInfoInputLogMetering(nmPesanEditSukses,nmHeaderEditData);
							// RefreshDataAddFieldMetering(Ext.get('txtKode_SetEquipmentCat').dom.value);
						// }
						// else if  (cst.success === false && cst.pesan===0)
						// {
							// ShowPesanWarningInputLogMetering(nmPesanEditGagal,nmHeaderEditData);
						// }
						// else 
						// {
							// ShowPesanErrorInputLogMetering(nmPesanEditError,nmHeaderEditData);
						// };
					}
				}
			)
		};
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
};

function InputLogMeteringDelete() 
{
	if (ValidasiEntryInputLogMetering(nmHeaderHapusData) == 1 )
	{
		Ext.Msg.show
		(
			{
			   title:nmHeaderHapusData,
			   msg: nmGetValidasiHapus(nmMetering2) ,
			   buttons: Ext.MessageBox.YESNO,
			   width:300,
			   fn: function (btn) 
			   {			
					if (btn === 'yes') 
					{
						Ext.Ajax.request
						(
							{
								//url: "./Datapool.mvc/DeleteDataObj",
                                                                url: WebAppUrl.UrlDeleteData,
								params: getParamInputLogMetering(),
								success: function(o) 
								{
									var cst = Ext.decode(o.responseText);
									if (cst.success === true) 
									{
										ShowPesanInfoInputLogMetering(nmPesanHapusSukses,nmHeaderHapusData);
										//RefreshDataInputLogMetering();
										InputLogMeteringAddNew();
									}
									else if (cst.success === false && cst.pesan===0)
									{
										ShowPesanWarningInputLogMetering(nmPesanHapusGagal,nmHeaderHapusData);
									}
									else 
									{
										ShowPesanErrorInputLogMetering(nmPesanHapusError,nmHeaderHapusData);
									};
								}
							}
						)
					};
				}
			}
		)
	};
};


function ValidasiEntryInputLogMetering(modul)
{
	var x = 1;
	if (Ext.get('txtKdEmployeeLog').getValue() == '' || Ext.get('txtCurrent').getValue() == '')
	{
		if (Ext.get('txtKdEmployeeLog').getValue() == '')
		{			
			ShowPesanWarningInputLogMetering(nmGetValidasiKosong(nmEmpIDNameInfoLogMet),modul);
			x=0;
		}
		else if (Ext.get('txtCurrent').getValue() == '')
		{
			ShowPesanWarningInputLogMetering(nmGetValidasiKosong(nmCurrInfoLogMet),modul);
			x=0;			
		};
	};
	return x;
};

function ShowPesanWarningInputLogMetering(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING,
		    width :250
		}
	);
};

function ShowPesanErrorInputLogMetering(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR,
		    width :250
		}
	);
};

function ShowPesanInfoInputLogMetering(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO,
		    width :250
		}
	);
};

//------------------------------------------------------------------------------------
function InputLogMeteringInit(Asset_ID) 
{
    AddNewInputLogMetering = true;	
	GetLastIntervalMetering(Asset_ID);
   	
};
///---------------------------------------------------------------------------------------///



function InputLogMeteringAddNew() 
{
    AddNewInputLogMetering = true;   
	// Ext.get('txtKdServiceInputLogMetering').dom.value = '';
    // Ext.get('txtIntervalInputLogMetering').dom.value = '';
	// Ext.get('txtAsumsi1InputLogMetering').dom.value = '';
	// Ext.get('txtAsumsi2InputLogMetering').dom.value = '';
	// Ext.get('txtServiceInputLogMetering').dom.value = '';
	// Ext.get('cboSatuanInputLogMetering').dom.value = '';
	// Ext.get('txtBeforeInputLogMetering').dom.value= '';
	// Ext.get('txtTargetInputLogMetering').dom.value= '';
	// Ext.get('txtAfterInputLogMetering').dom.value= '';
	
	rowSelectedInputLogMetering   = undefined;
};
///---------------------------------------------------------------------------------------///


function getParamInputLogMetering() 
{
    var params =
	{	
		Table: 'InputLogMetering',   
	    AssetId: strAssetID,
		EmpId:Ext.get('txtKdEmployeeLog').dom.value,
		Meter:Ext.get('txtCurrent').dom.value,
	  	Desc:''	,
		Update:Ext.get('chkUpdateLastServiceInfoLogMet').dom.checked
	};
    return params
};


function RefreshDataInputLogMetering()
{	
	dsInputLogMeteringList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: selectCountInputLogMetering, 
				//Sort: 'Metering_ID',
                                Sort: 'metering_id',
				Sortdir: 'ASC', 
				target:'ViewSetupMetering',
				param: ''
			} 
		}
	);
	rowSelectedInputLogMetering = undefined;
	return dsInputLogMeteringList;
};

function RefreshDataInputLogMeteringFilter() 
{   
	var KataKunci='';
    if (Ext.get('txtInputLogMeteringFilter').getValue() != '')
    { 
		//KataKunci = ' where Metering like ~%' + Ext.get('txtInputLogMeteringFilter').getValue() + '%~';
                KataKunci = ' metering like ~%' + Ext.get('txtInputLogMeteringFilter').getValue() + '%~';
	};
	
    if (Ext.get('txtInputLogMeteringAddressFilter').getValue() != '')
    { 
		if (KataKunci === '')
		{
			//KataKunci = ' where vend_address like  ~%' + Ext.get('txtInputLogMeteringAddressFilter').getValue() + '%~';
                        KataKunci = ' vend_address like ~%' + Ext.get('txtInputLogMeteringAddressFilter').getValue() + '%~';
		}
		else
		{
			//KataKunci += ' and  vend_address like  ~%' + Ext.get('txtInputLogMeteringAddressFilter').getValue() + '%~';
                        KataKunci += ' and vend_address like  ~%' + Ext.get('txtInputLogMeteringAddressFilter').getValue() + '%~';
		};  
	};
	    
    if (KataKunci != undefined) 
    {  
		dsInputLogMeteringList.load
		(
			{ 
				params: 
				{ 
					Skip: 0, 
					Take: selectCountInputLogMetering, 
					//Sort: 'Metering_ID',
                                        Sort: 'metering_od',
					Sortdir: 'ASC', 
					target:'ViewSetupMetering',
					param: KataKunci
				} 
			}
		);     
    }
	else
	{
		RefreshDataInputLogMetering();
	};
};

function getItemPanelOkCancel()  
{
    var items =
	{
	    layout: 'column',
	    border: false,
		height:35,
		anchor:'100%',
		//style:{'margin-top':'-1px'},
		style:{'margin-top':'7px'},
	    items:
		[
			{
				layout: 'hBox',
				width:222,
				border: false,
				bodyStyle: 'padding:5px 0px 5px 5px',
				defaults: { margins: '3 3 3 3' },
				anchor: '90%',
				layoutConfig: 
				{
					align: 'middle',
					pack:'end'
				},
				items:
				[
					{
						xtype:'button',
						text:nmBtnOK,
						width:70,
						style:{'margin-left':'0px','margin-top':'0px'},
						hideLabel:true,
						id: 'btnOkLog',
						handler:function()
						{
							var x = InputLogMeteringSave(true);
						
							if (x===undefined)
							{
								InputLogMeteringLookUps.close();
							};					
						}
					},
					{
							xtype:'button',
							text:nmBtnCancel,
							width:70,
							hideLabel:true,
							id: 'btnCancelLog',
							handler:function() 
							{
								InputLogMeteringLookUps.close();
							}
					}
				]
			}
		]
	}
    return items;
};

function getItemPanelEmployee() 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .5,
			    layout: 'form',
			    border: false,
				labelWidth:75,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: nmEmpIDNameInfoLogMet + ' ',
					    name: 'txtKdEmployeeLog',
					    id: 'txtKdEmployeeLog',
						readOnly:true,
					    anchor: '98%',
					    listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									var criteria='';
									if (Ext.get('txtKdEmployeeLog').dom.value != '')
									{
										//criteria = ' where EMP_ID like ~%' + Ext.get('txtKdEmployeeLog').dom.value + '%~';
                                                                                criteria = ' emp_id like ~%' + Ext.get('txtKdEmployeeLog').dom.value + '%~';
									};
									FormLookupEmployee('txtKdEmployeeLog','txtEmployeeLog',criteria);
								};
							}
						}
					}
				]
			},
			{
			    columnWidth: .5,
			    layout: 'form',
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: '',
						hideLabel:true,
					    name: 'txtEmployeeLog',
					    id: 'txtEmployeeLog',
					    anchor: '95%',
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									var criteria='';
									if (Ext.get('txtEmployeeLog').dom.value != '')
									{
										//criteria = ' where EMP_NAME like ~%' + Ext.get('txtEmployeeLog').dom.value + '%~';
                                                                                criteria = ' emp_name like ~%' + Ext.get('txtEmployeeLog').dom.value + '%~';
									};
									FormLookupEmployee('txtKdEmployeeLog','txtEmployeeLog',criteria);
								};
							},
							'focus' : function()
							{
								FocusCtrlLogMetering='txtemp';
							}
						}
					}
				]
			}
		]
	}
    return items;
};

function GetLastIntervalMetering(criteria)
{
	 Ext.Ajax.request
	 (
		{
            //url: "./Module.mvc/ExecProc",
            url: WebAppUrl.UrlExecProcess,
            params: 
			{
                UserID: 'Admin',
                ModuleID: 'ProsesGetLastIntervalMetering',
				Params:	criteria		
            },
            success: function(o) 
			{
                var cst = Ext.decode(o.responseText);

				if (cst.success === true) 
				{
					Ext.get('txtTarget').dom.value=formatCurrency(cst.Target);	
					Ext.get('txtLast').dom.value=formatCurrency(cst.Last);						
				}
				else
				{
					Ext.get('txtTarget').dom.value=0;
					Ext.get('txtLast').dom.value=0;
				};
            }

        }
	);
};
