﻿// <reference path="../../ext-base.js" />
// <reference path="../../ext-all.js" />

var rowSelectedLookAcc;
var vWinFormEntry;
var CurrentSelected_LookAcc = 
{
    data: Object,
    details:Array, 
    row: 0
};


function FormLookupAccount(criteria,dsStore,p,mBolAddNew,idx,mBolLookup) 
{
    vWinFormEntry = new Ext.Window
	(
		{
			id: 'FormROLookup',
			title: 'Lookup Akun',
			closeAction: 'hide',
			closable:false,
			width: 550,//450,
			height: 420,
			border: false,
			plain: true,
			resizable:false,
			layout: 'form',
			iconCls: 'find',
			modal: true,                                   
			items: 
			[
				GetPanelLookUp(criteria,dsStore,p,mBolAddNew,idx,mBolLookup)
			],
			listeners:
				{ 
					activate: function()
					{ } 
				}
		}
	);	

    vWinFormEntry.show();
};

function GetPanelLookUp(criteria,dsStore,p,mBolAddNew,idx,mBolLookup)
{
	
	var FormLookUp = new Ext.Panel    // from transaksi jurnal
	(
		{
			id: 'FormLookUp',
			region: 'center',
			layout: 'form',
			title: '',
			bodyStyle: 'padding:15px',
			anchor:'100%',
			border: true,
			bodyStyle: 'background:#FFFFFF;',
			height: 392,
			shadhow: true,
			items: 
			[
				fnGetDTLGridLookUpFE(criteria,dsStore,p,mBolAddNew,idx,mBolLookup),
				{
					layout: 'hBox',
					border: false,
					defaults: { margins: '0 5 0 0' },
					style:{'margin-left':'11px','margin-top':'2px'},
					anchor: '94%',
					layoutConfig: 
					{
						padding: '3',
						pack: 'end',
						align: 'middle'
					},
					items:
					[
						{
							xtype:'button',
							text:'Ok',
							width:70,
							style:{'margin-left':'3px','margin-top':'2px'},
							hideLabel:true,
							id: 'btnOkAcc',
							handler:function()
							{
								if (p != undefined && rowSelectedLookAcc != undefined)
								{
									if (mBolAddNew === true || mBolAddNew === undefined)
									{
										p.data.account=rowSelectedLookAcc.data.account;
										p.data.name=rowSelectedLookAcc.data.name;
										// p.data.NAMAACC=rowSelectedLookAcc.data.Name;										
										if (mBolLookup === true)
										{
										   dsStore.insert(dsStore.getCount(), p);										   
										}
										else
										{
										   dsStore.removeAt(dsStore.getCount()-1);
										   dsStore.insert(dsStore.getCount(), p);										   
										}										
									}
									else
									{
										if (dsStore != undefined)
										{											
											p.data.account=rowSelectedLookAcc.data.account;
											p.data.name=rowSelectedLookAcc.data.name;
											// p.data.NAMAACC=rowSelectedLookAcc.data.Name;
											
											dsStore.removeAt(idx);
											dsStore.insert(idx, p);
										}										
									}
									console.log(dsStore);
									vWinFormEntry.close();
									//alert(getArrDetail_PenerimaanNonMhs())
								}
								else
								{
									ShowPesanWarningLookupAccountGeneral('Belum ada account yang di pilih','Lookup Account');
								};
								
							}
						},
						{
								xtype:'button',
								text:'Cancel',
								width:70,
								style:{'margin-left':'5px','margin-top':'2px'},
								hideLabel:true,
								id: 'btnCancelAcc',
								handler:function() 
								{
									vWinFormEntry.close();
								}
						}
					]
				}
			]
        }
	);
	
	return FormLookUp;
};

function ShowPesanWarningLookupAccountGeneral(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING
		}
	);
};

function fnGetDTLGridLookUpFE(criteria,dsStore,p,mBolAddNew,idx,mBolLookup) 
{  	
	var fldDetail = ['account','name','parent','saldo_awal'];
	var dsLookAccList = new WebApp.DataStore({ fields: fldDetail });

	// if(criteria != "")
	// {
		// criteria += " AND ";
	// }
	// else
	// {
		// criteria += " WHERE ";
	// }
	// criteria += " Type = 'D'";
	
	var vGridLookAccFormEntry = new Ext.grid.EditorGridPanel
	(
		{ 
			id:'vGridLookAccFormEntry',
			title: '',
			stripeRows: true,
			store: dsLookAccList,
			height:353,
			columnLines:true,
			bodyStyle: 'padding:0px',
			border: false,		
			sm: new Ext.grid.RowSelectionModel
			(
				{ 
					singleSelect: true,
					listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedLookAcc = dsLookAccList.getAt(row);
						}
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, row, rec)
				{
					rowSelectedLookAcc = dsLookAccList.getAt(row);
					
					CurrentSelected_LookAcc.row = rec;
					CurrentSelected_LookAcc.data = rowSelectedLookAcc;

					if (rowSelectedLookAcc != undefined)
						{
							
							if (mBolAddNew === true || mBolAddNew === undefined)
							{
								p.data.account=rowSelectedLookAcc.data.account;
								p.data.name=rowSelectedLookAcc.data.name;
								// p.data.NAMAACC=rowSelectedLookAcc.data.Name;										
								if (mBolLookup === true)
								{
								   dsStore.insert(dsStore.getCount(), p);										   
								}
								else
								{
								   dsStore.removeAt(dsStore.getCount()-1);
								   dsStore.insert(dsStore.getCount(), p);										   
								}										
							}
							else
							{
								if (dsStore != undefined)
								{											
									p.data.account=rowSelectedLookAcc.data.account;
									p.data.name=rowSelectedLookAcc.data.name;
									// p.data.NAMAACC=rowSelectedLookAcc.data.Name;
									
									dsStore.removeAt(idx);
									dsStore.insert(idx, p);
								}										
							}
							console.log(p)
							vWinFormEntry.close();
					}
					else
					{
							//DataAddNew_OpenArForm=true;
							//LookUpForm_OpenArForm();
					}
				}
			},
			cm: fnGridLookAccColumnModel(),
			viewConfig: { forceFit: true }
		}
	);
	
	
	dsLookAccList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				Sort: 'Groups,Account', 
				Sortdir: 'ASC', 
				target:'viewAkunNew',
				param: criteria
			}
		}
	);
	
	return vGridLookAccFormEntry;
};



function fnGridLookAccColumnModel() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{ 
				id: 'colAccLook',
				header: "No Akun",
				dataIndex: 'account',
				width: 70
			},
			{ 
				id: 'colAccNameLook',
				header: "Nama Akun",
				dataIndex: 'name',
				width: 250,
				renderer: function(value, cell) 
				{
				var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
				return str;
				}
            },
            { 
				id: 'colAccNamaParent',
				header: "Parent",
				dataIndex: 'saldo_awal',
				width: 250,
				renderer: function(value, cell) 
				{
				var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
				return str;
				}

            }

		]
	)
};
///---------------------------------------------------------------------------------------///