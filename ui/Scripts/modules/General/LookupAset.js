/// <reference path="../../ext-base.js" />
/// <reference path="../../ext-all.js" />

var rowSelectedLookAset;
var vWinFormEntryLookupAset;
var dsCategoryLookupAset;
var dsDeptLookupAset;
var valueCategoryLookupAset=' All';
var valueDeptLookupAset=' All';
var selectCategoryLookupAset=' 9999';
var selectDeptLookupAset=' 9999';
var DefaultSelectDeptLookupAset=' 9999';
var DefaultSelectCategoryLookupAset=' 9999';
var dsLookAsetList;
var IdKlas_produkegorySetEquipmentKlas_produkView='0';
var str;


function FormLookupKasirRWJ(criteria,dsStore,p,mBolAddNew,idx,mBolLookup) 
{
	
	rowSelectedLookAset=undefined;
    vWinFormEntryLookupAset = new Ext.Window
	(
		{
			id: 'vWinFormEntryLookupAset',
			title: 'Lihat Produk',
			closeAction: 'hide',
			closable:false,
			width: 550,
			height: 550,//420,
			border: false,
			plain: true,
			resizable:false,
			layout: 'form',
			iconCls: 'find',
			modal: true,                                   
			items: 
			[	itemsTreeListProdukGridDataView_viKasirRwj(),
				GetPanelLookUpAset(criteria,dsStore,p,mBolAddNew,idx,mBolLookup) 
				
			],
			listeners:
				{ 
					activate: function()
					{ } 
				}
		}
	);
	
    vWinFormEntryLookupAset.show();
};


function itemsTreeListProdukGridDataView_viKasirRwj()
{	
		
	var treeKlas_produk_viKasirRwj= new Ext.tree.TreePanel
	(
		{
			autoScroll: true,
			split: true,
			height:150,
			loader: new Ext.tree.TreeLoader(),
			listeners: 
			{
				click: function(n) 
				{
					strTreeCriteriaSetEquipment_viKasirRwj=n.attributes
					rowSelectedTreeSetEquipmentKlas_produk=n.attributes;
					if (strTreeCriteriaSetEquipment_viKasirRwj.id != ' 0')
					{
						if (strTreeCriteriaSetEquipment_viKasirRwj.leaf == false)
						{
							var str='';
							//str=' and left(parent,' + n.attributes.id.length + ')=' + n.attributes.id
                             //                           str='substring(parent,1, ' + n.attributes.id.length + ') = ~' + n.attributes.id + '~';
						//	RefreshDataSetEquipmentCat(str);
						}
						else
						{
							//RefreshDataSetEquipmentCat('parent = ~' + strTreeCriteriaSetEquipment_viKasirRwj.id + '~');
						};
					}
					else
					{
						//RefreshDataSetEquipmentCat('');
					};
				}
			}
		}
	);
	
	rootTreeSetEquipmentKlas_produkView = new Ext.tree.AsyncTreeNode
	(
		{
			expanded: true,
			text:'Master Produk',
			id:IdKlas_produkegorySetEquipmentKlas_produkView,
			children: StrTreeSetEquipment,
			autoScroll: true
		}
	)  
  
  treeKlas_produk_viKasirRwj.setRootNode(rootTreeSetEquipmentKlas_produkView);  
	
	
	
	
	
	

    var pnlTreeFormDataWindowPopup_viKasirRwj = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                
				//-------------- ## -------------- 
                treeKlas_produk_viKasirRwj,
                //-------------- ## -------------- 
            ],
            //-------------- #End items# --------------
        }
    )

    return pnlTreeFormDataWindowPopup_viKasirRwj;
}


function GetStrTreeSetEquipment()
{
	 Ext.Ajax.request
	 (
		{
			url: WebAppUrl.UrlExecProcess,
			params: 
			{
				UserID: strUser, // 'Admin',
				ModuleID: 'ProsesGetDataTreeSetCat',
				Params:	''
			},
			success : function(resp) 
			{
				var cst = Ext.decode(resp.responseText);
				StrTreeSetEquipment= cst.arr;
			},
			failure:function()
			{
			    
			}
		}
	);
};

function GetPanelLookUpAset(criteria,dsStore,p,mBolAddNew,idx,mBolLookup) 
{
	
	var FormLookupKasirRWJ = new Ext.Panel  
	(
		{
			id: 'FormLookupKasirRWJ',
			region: 'center',
			layout: 'form',
			title: '',
			bodyStyle: 'padding:15px',
			anchor:'100%',
			border: false,
			bodyStyle: 'background:#FFFFFF;',
			height: 510,//392,
			shadhow: true,
			items: 
			[
				//getItemPanelCboCatLookupAset(),
				fnGetDTLGridLookUpAset(criteria),
				getItemPanelLookupAset(dsStore,p,mBolAddNew,idx,mBolLookup)
			]
        }
	);
	
	return FormLookupKasirRWJ;
};


function getItemPanelLookupAset(dsStore,p,mBolAddNew,idx,mBolLookup) 
{
    var items =
	{
	    layout: 'column',
	    border: true,
		height:39,
		anchor:'100%',
		style:{'margin-top':'-1px'},
	    items:
		[
			{
				layout: 'hBox',
				width:522,
				border: false,
				bodyStyle: 'padding:5px 0px 5px 5px',
				defaults: { margins: '3 3 3 3' },
				anchor: '90%',
				layoutConfig: 
				{
					align: 'middle',
					pack:'end'
				},
				items:
				[
					{
						xtype:'button',
						text:nmBtnOK,
						width:70,
						style:{'margin-left':'0px','margin-top':'0px'},
						hideLabel:true,
						id: 'btnOkLookupAset',
						handler:function()
						{
							if (p != undefined && rowSelectedLookAset != undefined)
							{
								if (mBolAddNew === true || mBolAddNew === undefined)
									{
										p.data.DESKRIPSI2 =rowSelectedLookAset.data.DESKRIPSI;
										p.data.KD_PRODUK=rowSelectedLookAset.data.KD_PRODUK;
										p.data.DESKRIPSI=rowSelectedLookAset.data.DESKRIPSI;
										p.data.KD_TARIF=rowSelectedLookAset.data.KD_TARIF;
										p.data.HARGA=rowSelectedLookAset.data.TARIF;
										p.data.TGL_BERLAKU=rowSelectedLookAset.data.TGL_BERLAKU;
										p.data.QTY=1;
										
										if (mBolLookup === true)
										{
											if(dsStore.getCount() === 0)
											{
												dsStore.insert(dsStore.getCount(), p);
												RequestSave(false);
											}
											else
											{
												if (dsStore.data.items[dsStore.getCount()-1].data.KD_PRODUK === '' || dsStore.data.items[dsStore.getCount()-1].data.KD_PRODUK === undefined)
												{
													dsStore.removeAt(dsStore.getCount()-1);
													dsStore.insert(dsStore.getCount(), p);
													RequestSave(false);
												}
												else
												{
													dsStore.insert(dsStore.getCount(), p);
													RequestSave(false);
												};
											};
										}
										else
										{
										   dsStore.removeAt(dsStore.getCount()-1);
										   dsStore.insert(dsStore.getCount(), p);
										}
										
									}
									else
									{
										if (dsStore != undefined)
										{
										//


										p.data.ASSET_MAINT =rowSelectedLookAset.data.DESKRIPSI;
											p.data.KD_PRODUK=rowSelectedLookAset.data.KD_PRODUK;
											p.data.DESKRIPSI=rowSelectedLookAset.data.DESKRIPSI;
											p.data.KD_TARIF=rowSelectedLookAset.data.KD_TARIF;
											p.data.HARGA=rowSelectedLookAset.data.TARIF;
											p.data.TGL_BERLAKU=rowSelectedLookAset.data.TGL_BERLAKU;
											p.data.PROBLEM=1;
											dsStore.removeAt(idx);
											dsStore.insert(idx, p);
										}
									}
								vWinFormEntryLookupAset.close();
							}
							else
							{
								ShowPesanWarningLookupAset(nmGetValidasiSelect(nmAsetLookupAset),nmTitleFormLookupAset);
							};
							
						}
					},
					{
							xtype:'button',
							text:nmBtnCancel ,
							width:70,
							hideLabel:true,
							id: 'btnCancelLookupAset',
							handler:function() 
							{
								vWinFormEntryLookupAset.close();
							}
					}
				]
			}
		]
	}
    return items;
};

function ShowPesanWarningLookupAset(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};
/*

$row->KD_PRODUK=$rec->kd_produk;
        $row->KD_PRODUK=$rec->parent;
        $row->DESKRIPSI=$rec->deskripsi;
        $row->TGL_BERAKHIR=$rec->tgl_berakhir;
        $row->KLASIFIKASI=$rec->klasifikasi;
        $row->KD_KAT=$rec->kd_kat;
        $row->KD_KLAS=$rec->kd_klas;
        $row->NAMA_UNIT=$rec->nama_unit;
        $row->LOCATION=$rec->tarif;
        $row->LOCATION_ID=$rec->kd_tarif;
		$row->REQ_FINISH_DATE=$rec->tgl_berlaku;
		
  $row->KD_PRODUK=$rec->kd_produk;
        $row->KD_PRODUK=$rec->parent;
        $row->DESKRIPSI=$rec->deskripsi;
        $row->TGL_BERAKHIR=$rec->tgl_berakhir;
        $row->KLASIFIKASI=$rec->klasifikasi;
        $row->KD_KAT=$rec->kd_kat;
        $row->KD_KLAS=$rec->kd_klas;
        $row->NAMA_UNIT=$rec->nama_unit;
        $row->=$rec->tarif;
        $row->=$rec->kd_tarif;
		$row->=$rec->tgl_berlaku;
        return $row;

*/
function fnGetDTLGridLookUpAset(criteria) 
{

    var fldDetail = ['TARIF','KLASIFIKASI','PERENT','TGL_BERAKHIR','KD_KAT','KD_TARIF','KD_KLAS','DESKRIPSI','YEARS','NAMA_UNIT','KD_PRODUK','TGL_BERLAKU'];
	dsLookAsetList = new WebApp.DataStore({ fields: fldDetail });

	var vGridLookAsetFormEntry = new Ext.grid.EditorGridPanel
	(
		{ 
			id:'vGridLookAsetFormEntry',
			title: '',
			stripeRows: true,
			store: dsLookAsetList,
			style:{'margin-top':'-1px'},
			height:300,
			columnLines:true,
			bodyStyle: 'padding:0px',
			border: true,	
			frame:false,
			sm: new Ext.grid.RowSelectionModel
			(
				{ 
					singleSelect: true,
					listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedLookAset = dsLookAsetList.getAt(row);
						}
					}
				}
			),
			cm: fnGridLookAsetColumnModel(),
			viewConfig: { forceFit: true }
		}
	);
	

	RefreshDataSetEquipmentCat(str);
	
	return vGridLookAsetFormEntry;
};



function RefreshDataSetEquipmentCat(str)
{
		
 if (str == undefined || str == 'parent = ~0~')
  {
	str='LOWER(kd_tarif)=LOWER(~TU~) and left(kd_unit,3)=~202~ and tgl_berakhir is null' ;
  }else
  {
	str='LOWER(kd_tarif)=LOWER(~TU~) and left(kd_unit,3)=~202~ and tgl_berakhir is null and '+ str;
  };
	dsLookAsetList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				//Sort: 'PERENT',
                                Sort: 'KD_PRODUK',
				Sortdir: 'ASC',
				target: 'LookupAsset',
				param: str
			}
		}
	);
	return dsLookAsetList;
};



function fnGridLookAsetColumnModel() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			
			{ 
				id: 'colAsetNameLook',
				header: 'Produk',
				dataIndex: 'DESKRIPSI',
				width: 300
            },
			{ 
				id: 'colAsetTARIFLook',
				header: 'Harga',
				align: 'right',
				dataIndex: 'TARIF',
				width: 300,
				renderer: function(v, params, record) 
							{
							return formatCurrency(record.data.TARIF);
							
							}	
            }
		]
	)
};




///---------------------------------------------------------------------------------------///