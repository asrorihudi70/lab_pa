/// <reference path="../../ext-base.js" />
/// <reference path="../../ext-all.js" />

var rowSelectedLookDiagnosa;
var vWinFormEntryLookupDiagnosa;



var dsLookDiagnosaList;
var IdKlas_DiagnosaegorySetEquipmentKlas_DiagnosaView='0';
var str;
var chkAktif;


function FormLookupDiagnosa(criteria,dsStore,p,mBolAddNew,idx,mBolLookup) 
{

	rowSelectedLookDiagnosa=undefined;
    vWinFormEntryLookupDiagnosa = new Ext.Window
	(
		{
			id: 'vWinFormEntryLookupDiagnosa',
			title: 'Tambah Diagnosa',
			closeAction: 'hide',
			closable:false,
			width: 550,
			height: 450,//420,
			border: false,
			plain: true,
			resizable:false,
			layout: 'form',
			iconCls: 'find',
			modal: true,                                   
			items: 
			[	
				GetPanelLookUpDiagnosa(criteria,dsStore,p,mBolAddNew,idx,mBolLookup) 
				
			],
			listeners:
				{ 
					activate: function()
					{ } 
				},
				 tbar:
            [
			'-','Kode Penyakit ', '-',
			{
							xtype: 'textfield',
							id: 'TxtFilterkodepenyakit',
							fieldLabel: 'Kode Penyakit',
							
							width: 80,
							
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
							
									    if (Ext.get('TxtFilterkodepenyakit').dom.value==''&&Ext.get('TxtFilterpenyakit').dom.value=='')
								{
								
									ShowPesanWarningLookupDiagnosa('Silahkan isi kolom Penyakit atau kode penyakit','Lihat Penyakit');
								
								}else
								{
										if (Ext.get('chkdiagnosa').dom.checked===false){
										RefreshDatDiagnosa(str);}
										else{
										RefreshDatDiagnosaDTD(str);}
								}
										  

									} 						
								}
							}
						},
						'-','Penyakit ', '-',
						{
							xtype: 'textfield',
							id: 'TxtFilterpenyakit',
							fieldLabel: 'Penyakit',
							
							width: 120,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
									
									
									      if (Ext.get('TxtFilterkodepenyakit').dom.value==''&&Ext.get('TxtFilterpenyakit').dom.value=='')
								{
								
									ShowPesanWarningLookupDiagnosa('Silahkan isi kolom Penyakit atau kode penyakit','Lihat Penyakit');
								
								}else
								{
										if (Ext.get('chkdiagnosa').dom.checked===false){
										RefreshDatDiagnosa(str);}
										else{
										RefreshDatDiagnosaDTD(str);}
								}
										 

									} 						
								}
							}
						},'-','DTD ', '-',
						{
						xtype: 'checkbox',
						id: 'chkdiagnosa',
						name:'chkdiagnosa',
						style: 
						{
							'margin-top': '3.5px'
						},
						fieldLabel: 'DTD' + ' ',
						anchor: '100%'
						}
						,
						{
							text: nmRefresh,
							tooltip: nmRefresh,
							iconCls: 'refresh',
							anchor: '25%',
							handler: function(sm, row, rec)
							{
							  
								if (Ext.get('TxtFilterkodepenyakit').dom.value==''&&Ext.get('TxtFilterpenyakit').dom.value=='')
								{
								
									ShowPesanWarningLookupDiagnosa('Isi kriteria pencarian','Lihat Penyakit');
								
								}else
								{
										if (Ext.get('chkdiagnosa').dom.checked===false){
										RefreshDatDiagnosa(str);}
										else{
										RefreshDatDiagnosaDTD(str);}
								}
							}
						}
						],
		}
	);
	
    vWinFormEntryLookupDiagnosa.show();
};



function GetPanelLookUpDiagnosa(criteria,dsStore,p,mBolAddNew,idx,mBolLookup) 
{
	
	var FormLookupDiagnosa = new Ext.Panel  
	(
		{
			id: 'FormLookupDiagnosa',
			region: 'center',
			layout: 'form',
			title: '',
			bodyStyle: 'padding:15px',
			anchor:'100%',
			border: false,
			bodyStyle: 'background:#FFFFFF;',
			height: 510,//392,
			shadhow: true,
			items: 
			[
				//getItemPanelCboCatLookupDiagnosa(),
				fnGetDTLGridLookUpDiagnosa(criteria),
				getItemPanelLookupDiagnosa(dsStore,p,mBolAddNew,idx,mBolLookup)
			]
        }
	);
	
	return FormLookupDiagnosa;
};



function getItemPanelLookupDiagnosa(dsStore,p,mBolAddNew,idx,mBolLookup) 
{
    var items =
	{
	    layout: 'column',
	    border: true,
		height:39,
		anchor:'100%',
		style:{'margin-top':'-1px'},
	    items:
		[
			{
				layout: 'hBox',
				width:522,
				border: false,
				bodyStyle: 'padding:5px 0px 5px 5px',
				defaults: { margins: '3 3 3 3' },
				anchor: '90%',
				layoutConfig: 
				{
					align: 'middle',
					pack:'end'
				},
				items:
				[
					{
						xtype:'button',
						text:nmBtnOK,
						width:70,
						style:{'margin-left':'0px','margin-top':'0px'},
						hideLabel:true,
						id: 'btnOkLookupDiagnosa',
						handler:function()
						{
							//if (p != undefined && rowSelectedLookDiagnosa != undefined)
							//{
							var mBol= false;
							for(var i = 0 ; i < dsLookDiagnosaList.getCount();i++)
								{
								if (dsLookDiagnosaList.data.items[i].data.CHECKBOX === true)
								{
										p =RecordBaruDiagnosa()
										p.data.KD_PENYAKIT=dsLookDiagnosaList.data.items[i].data.KD_PENYAKIT;
										p.data.PENYAKIT=dsLookDiagnosaList.data.items[i].data.PENYAKIT;												
										dsStore.insert( dsStore.getCount(), p);
										mBol=true;
											
								}		
			
						}
						vWinFormEntryLookupDiagnosa.close();
					}
					},
					{
							xtype:'button',
							text:nmBtnCancel ,
							width:70,
							hideLabel:true,
							id: 'btnCancelLookupDiagnosa',
							handler:function() 
							{
								vWinFormEntryLookupDiagnosa.close();
							}
					}
				]
			}
		]
	}
    return items;
};

function ShowPesanWarningLookupDiagnosa(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function fnGetDTLGridLookUpDiagnosa(criteria) 
{

    var fldDetail = ['KD_PENYAKIT','PENYAKIT','KETERANGAN','CHECKBOX'];
	dsLookDiagnosaList = new WebApp.DataStore({ fields: fldDetail });
	chkAktif = new Ext.grid.CheckColumn
	(
		{
			id: 'chkAktif',
			header: 'Pilih',
			align: 'center',
			disabled:false,
			dataIndex: 'CHECKBOX',
			width: 50
	
		}
	);
	var vGridLookDiagnosaFormEntry = new Ext.grid.EditorGridPanel
	(
		{ 
			id:'vGridLookDiagnosaFormEntry',
			title: '',
			stripeRows: true,
			store: dsLookDiagnosaList,
			style:{'margin-top':'-1px'},
			height:300,
			columnLines:true,
			bodyStyle: 'padding:0px',
			border: true,	
			
			plugins :[chkAktif],
			frame:false,
			sm: new Ext.grid.RowSelectionModel
			(
				{ 
					singleSelect: true,
					listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedLookDiagnosa = dsLookDiagnosaList.getAt(row);
						}
					}
				}
			),
			cm: fnGridLookDiagnosaColumnModel(),
			viewConfig: { forceFit: true }
		}
	);
	

	
	return vGridLookDiagnosaFormEntry;
};



function RefreshDatDiagnosa(str)
{
	if(Ext.get('TxtFilterkodepenyakit').dom.value!='' && Ext.get('TxtFilterpenyakit').dom.value=='')
	{
	str='kd_penyakit = ~'+Ext.get('TxtFilterkodepenyakit').dom.value+'~';
	}
	if(Ext.get('TxtFilterkodepenyakit').dom.value=='' && Ext.get('TxtFilterpenyakit').dom.value!='')
	{
	str='LOWER(penyakit) like LOWER(~'+Ext.get('TxtFilterpenyakit').dom.value+'%~)';
	}
	dsLookDiagnosaList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				//Sort: 'PERENT',
                                Sort: 'KD_PENYAKIT',
				Sortdir: 'ASC',
				target: 'LookupDiagnosa',
				param: str
			},
			
		}
	);
	return dsLookDiagnosaList;
};



function fnGridLookDiagnosaColumnModel() 
{
 
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			
			chkAktif,
			{ 
				id: 'colDiagnosaKdPenyakitLook',
				header: 'Kode Penyakit',
				dataIndex: 'KD_PENYAKIT',
				width: 200
            },
			{ 
				id: 'colDiagnosaPenyakitLook',
				header: 'Penyakit',
				dataIndex: 'PENYAKIT',
				width: 200
            },
			{ 
				id: 'colDiagnosaKeteranganLook',
				header: 'Keterangan',
				dataIndex: 'KETERANGAN',
				width: 200
				
            }
		]
	)
};


function RefreshDatDiagnosaDTD(str)
{
	if(Ext.get('TxtFilterkodepenyakit').dom.value!='' && Ext.get('TxtFilterpenyakit').dom.value=='')
	{
	str='kd_penyakit = ~'+Ext.get('TxtFilterkodepenyakit').dom.value+'~';
	}
	if(Ext.get('TxtFilterkodepenyakit').dom.value=='' && Ext.get('TxtFilterpenyakit').dom.value!='')
	{
	str='LOWER(keterangan) like LOWER(~'+Ext.get('TxtFilterpenyakit').dom.value+'%~)';
	}
	dsLookDiagnosaList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				//Sort: 'PERENT',
                                Sort: 'KD_PENYAKIT',
				Sortdir: 'ASC',
				target: 'LookupDiagnosadtd',
				param: str
			},
			success: function(o) 
					{
						alert('test');
						
					}

		}
	);
	return dsLookDiagnosaList;
};



///---------------------------------------------------------------------------------------///