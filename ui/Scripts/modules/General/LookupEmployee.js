/// <reference path="../../ext-base.js" />
/// <reference path="../../ext-all.js" />

var rowSelectedLookEmployee;
var vWinFormEntryLookupEmployee;
var varBtnOkLookupEmp=false;


function FormLookupEmployee(x,y,criteria,mBolGrid,p,dsStore,mBolLookup) 
{
	rowSelectedLookEmployee = undefined;
    vWinFormEntryLookupEmployee = new Ext.Window
	(
		{
			id: 'vWinFormEntryLookupEmployee',
			title: nmTitleFormLookupEmp,
			closeAction: 'hide',
			closable:false,
			width: 450,
			height: 420,
			border: false,
			plain: true,
			resizable:false,
			layout: 'form',
			iconCls: 'find',
			modal: true,                                   
			items: 
			[
				GetPanelLookUpEmployee(x,y,criteria,mBolGrid,p,dsStore,mBolLookup) 
			],
			listeners:
				{ 
					activate: function()
					{ } 
				}
		}
	);

    vWinFormEntryLookupEmployee.show();
};

function GetPanelLookUpEmployee(x,y,criteria,mBolGrid,p,dsStore,mBolLookup) 
{
	
	var FormLookUpEmployee = new Ext.Panel  
	(
		{
			id: 'FormLookUpEmployee',
			region: 'center',
			layout: 'form',
			title: '',
			bodyStyle: 'padding:15px',
			anchor:'100%',
			border: false,
			bodyStyle: 'background:#FFFFFF;',
			height: 392,
			shadhow: true,
			items: 
			[
				fnGetDTLGridLookUpEmployee(criteria),
				getItemPanelLookupEmployee(x,y,mBolGrid,p,dsStore,mBolLookup) 
			]
        }
	);
	
	return FormLookUpEmployee;
};

function getItemPanelLookupEmployee(x,y,mBolGrid,p,dsStore,mBolLookup) 
{
    var items =
	{
	    layout: 'column',
	    border: true,
		height:40,
		anchor:'100%',
		style:{'margin-top':'-1px'},
	    items:
		[
			{
				layout: 'hBox',
				width:422,
				border: false,
				bodyStyle: 'padding:5px 0px 5px 5px',
				defaults: { margins: '3 3 3 3' },
				anchor: '90%',
				layoutConfig: 
				{
					align: 'middle',
					pack:'end'
				},
				items:
				[
					{
						xtype:'button',
						text:nmBtnOK,
						width:70,
						style:{'margin-left':'0px','margin-top':'0px'},
						hideLabel:true,
						id: 'btnOkLookupEmployee',
						handler:function()
						{
							if (rowSelectedLookEmployee != undefined)
							{
								if (mBolGrid === true)
								{
									p.data.EMP_ID=rowSelectedLookEmployee.data.EMP_ID;
									p.data.EMP_NAME=rowSelectedLookEmployee.data.EMP_NAME;
								
									if (mBolLookup === true)
										{
										   dsStore.insert(dsStore.getCount(), p);
										}
										else
										{
										   dsStore.removeAt(dsStore.getCount()-1);
										   dsStore.insert(dsStore.getCount(), p);
										};
									varBtnOkLookupEmp=true;
								}
								else
								{
									Ext.get(x).dom.value=rowSelectedLookEmployee.data.EMP_ID;
									Ext.get(y).dom.value=rowSelectedLookEmployee.data.EMP_NAME;
								};
								vWinFormEntryLookupEmployee.close();
							}
							else
							{
								ShowPesanWarningLookupEmployee(nmGetValidasiSelect(nmEmpLookupEmp),nmTitleFormLookupEmp);
							};	
						}
					},
					{
							xtype:'button',
							text:nmBtnCancel,
							width:70,
							hideLabel:true,
							id: 'btnCancelLookupEmployee',
							handler:function() 
							{
								vWinFormEntryLookupEmployee.close();
							}
					}
				]
			}
		]
	}
    return items;
};

function ShowPesanWarningLookupEmployee(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING
		}
	);
};

function fnGetDTLGridLookUpEmployee(criteria) 
{  
	
	var fldDetail = ['EMP_ID','DEPT_ID','EMP_NAME','EMP_ADDRESS','EMP_CITY','EMP_STATE','EMP_POS_CODE','EMP_PHONE1','EMP_PHONE2','EMP_EMAIL','IS_AKTIF','DEPT_NAME'];
	var dsLookEmployeeList = new WebApp.DataStore({ fields: fldDetail });

	var vGridLookEmployeeFormEntry = new Ext.grid.EditorGridPanel
	(
		{ 
			id:'vGridLookEmployeeFormEntry',
			title: '',
			stripeRows: true,
			store: dsLookEmployeeList,
			height:353,
			columnLines:true,
			bodyStyle: 'padding:0px',
			border: true,	
			frame:false,
			sm: new Ext.grid.RowSelectionModel
			(
				{ 
					singleSelect: true,
					listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedLookEmployee = dsLookEmployeeList.getAt(row);
						}
					}
				}
			),
			cm: fnGridLookEmployeeColumnModel(),
			viewConfig: { forceFit: true }
		}
	);
	
	if ( criteria === '')
	{
		//criteria = ' where IS_AKTIF = 1';
		//criteria = "\"IS_AKTIF\" = true";
                criteria = "is_aktif = true";
	}
	else
	{
		//criteria += ' and IS_AKTIF = 1';
		//criteria += " AND \"IS_AKTIF\" = true";
                criteria += " AND is_aktif = true";
	};
	
	dsLookEmployeeList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				//Sort: 'EMP_ID',
                                Sort: 'emp_id',
				Sortdir: 'ASC', 
				target:'LookupEmployee',
				param: criteria
			}
		}
	);
	
	return vGridLookEmployeeFormEntry;
};



function fnGridLookEmployeeColumnModel() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{ 
				id: 'colKdEmployeeLook',
				header: nmEmpIDLookupEmp,
				dataIndex: 'EMP_ID',
				width: 70
			},
			{ 
				id: 'colEmployeeNameLook',
				header: nmEmpNameLookupEmp,
				dataIndex: 'EMP_NAME',
				width: 300
            }
		]
	)
};
///---------------------------------------------------------------------------------------///