﻿// Lookup Tindakan # --------------
var rowSelectedLook_vLookupPemeriksaanGrid;
var mWindowGridLookup;
var CurrentData_vLookupPemeriksaanGrid =
	{
	    data: Object,
	    details: Array,
	    row: 0
	};
	
var dsLookupList_vLookupPemeriksaanGrid;
// End Lookup Tindakan # --------------

// Lookup Obat # --------------
var rowSelectedLookObat_vLookupPemeriksaanGrid;
var mWindowGridLookupObat;
var CurrentDataObat_vLookupPemeriksaanGrid =
	{
	    data: Object,
	    details: Array,
	    row: 0
	};
	
var dsLookupListObat_vLookupPemeriksaanGrid;
// End Lookup Obat # --------------

// Lookup Diagnosa # --------------
var rowSelectedLookDiagnosa_vLookupPemeriksaanGrid;
var mWindowGridLookupDiagnosa;
var CurrentDataDiagnosa_vLookupPemeriksaanGrid =
	{
	    data: Object,
	    details: Array,
	    row: 0
	};
	
var dsLookupListDiagnosa_vLookupPemeriksaanGrid;
// End Lookup Diagnosa # --------------

// Lookup Obat Resep # --------------
var rowSelectedLookObatResep_vLookupResepGrid;
var mWindowGridLookupObatResep;
var CurrentDataObatResep_vLookupResepGrid =
	{
	    data: Object,
	    details: Array,
	    row: 0
	};
	
var dsLookupListObatResep_vLookupResepGrid;
// End Lookup Obat Resep # --------------

/**
*	Function : FormLookupPemeriksaanGrid
*	
*	Sebuah fungsi untuk menampilkan windows popup tindakan / produk
*/

function FormLookupPemeriksaanGrid(criteria,dsStore,p,mBolAddNew,idx,mBolLookup) 
{
	
    var vWinFormEntry = new Ext.Window
	(
		{
			id: 'FormGrdLookup',
			title: 'Lookup Tindakan',
			closable:true,
			width: 450,
			height: 400,
			border: true,
			plain: true,
			resizable:false,
			layout: 'form',
			iconCls: 'find',
			modal: true,                                   
			items: 
			[
				fnGetDTLGridLookUpFE(criteria,dsStore,p,mBolAddNew,idx,mBolLookup,vWinFormEntry),
				{
					xtype:'button',
					text:'Ok',
					width:70,
					style:{'margin-left':'360px','margin-top':'7px'},
					hideLabel:true,
					id: 'btnOkAcc',
					handler:function()
					{
						if (p != undefined && rowSelectedLook_vLookupPemeriksaanGrid != undefined)
						{
							
							p.data.KD_PRODUK=rowSelectedLook_vLookupPemeriksaanGrid.data.KD_PRODUK;
							p.data.KD_PRODUK_VIEW=rowSelectedLook_vLookupPemeriksaanGrid.data.KD_PRODUK_VIEW;
							p.data.DESKRIPSI=rowSelectedLook_vLookupPemeriksaanGrid.data.DESKRIPSI;
							p.data.HARGA=rowSelectedLook_vLookupPemeriksaanGrid.data.TARIF;
							p.data.QTY="1";
							p.data.SUBTOTAL="";
							
							dsStore.removeAt(idx);
							dsStore.insert(idx, p);
						}
						rowSelectedLook_vLookupPemeriksaanGrid='';
						vWinFormEntry.close();
					}	
				}
			],
			listeners:
				{ 
					activate: function()
					{ } 
				}
		}
	);
    vWinFormEntry.show();
	mWindowGridLookup  = vWinFormEntry; 
};

//-------------- ## --------------

function fnGetDTLGridLookUpFE(criteria,dsStore,p,mBolAddNew,idx,mBolLookup,vWinFormEntry) 
{  
	var fldDetail = 
	[
		'KD_PRODUK', 'KD_PRODUK_VIEW', 'DESKRIPSI', 'TARIF'
	];
	
	dsLookupList_vLookupPemeriksaanGrid = new WebApp.DataStore({ fields: fldDetail });
	var vGridLookAccFormEntry_vLookupPemeriksaanGrid = new Ext.grid.EditorGridPanel
	(
		{ 
			id:'vGridLookAccFormEntry_vLookupPemeriksaanGrid',
			title: '',
			stripeRows: true,
			store: dsLookupList_vLookupPemeriksaanGrid,
			height:330,
			columnLines:true,
			bodyStyle: 'padding:0px',
			border: false,		
			sm: new Ext.grid.RowSelectionModel
			(
				{ 
					singleSelect: true,
					listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedLook_vLookupPemeriksaanGrid = undefined;
							rowSelectedLook_vLookupPemeriksaanGrid = dsLookupList_vLookupPemeriksaanGrid.getAt(row);
							CurrentData_vLookupPemeriksaanGrid;
							CurrentData_vLookupPemeriksaanGrid.row = row;
					        CurrentData_vLookupPemeriksaanGrid.data = rowSelectedLook_vLookupPemeriksaanGrid;
						}
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, row, rec)
				{
					/*rowSelectedLook_vLookupPemeriksaanGrid = dsLookupList_vLookupPemeriksaanGrid.getAt(row);
					CurrentData_vLookupPemeriksaanGrid.row = rec;
					CurrentData_vLookupPemeriksaanGrid.data = rowSelectedLook_vLookupPemeriksaanGrid;*/
					
					if (p != undefined && rowSelectedLook_vLookupPemeriksaanGrid != undefined)
						{
							p.data.KD_PRODUK=rowSelectedLook_vLookupPemeriksaanGrid.data.KD_PRODUK;
							p.data.KD_PRODUK_VIEW=rowSelectedLook_vLookupPemeriksaanGrid.data.KD_PRODUK_VIEW;
							p.data.DESKRIPSI=rowSelectedLook_vLookupPemeriksaanGrid.data.DESKRIPSI;
							p.data.HARGA=rowSelectedLook_vLookupPemeriksaanGrid.data.TARIF;
							p.data.QTY="1";
							p.data.SUBTOTAL="";

							dsStore.removeAt(idx);
							dsStore.insert(idx, p);
						}
						rowSelectedLook_vLookupPemeriksaanGrid='';
						mWindowGridLookup.close();
				}
			},
			cm: fnGridLookAccColumnModel(),
			viewConfig: { forceFit: true }
		});
		
	dsLookupList_vLookupPemeriksaanGrid.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 100, 
				Sort: 'KD_PRODUK', 
				Sortdir: 'ASC', 
				target:'viProduk',
				param: criteria
			} 
		}
	);
	return vGridLookAccFormEntry_vLookupPemeriksaanGrid;
};

//-------------- ## --------------	

function fnGridLookAccColumnModel() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{ 
				id: 'colKD_PRODUK',
				header: "KD PRODUK",
				dataIndex: 'KD_PRODUK_VIEW',
				width: 100
			},
			{ 
				id: 'colDESKRIPSI',
				header: "DESKRIPSI",
				dataIndex: 'DESKRIPSI',
				width: 170
			},
			{ 
				id: 'colTARIF',
				header: "TARIF",
				dataIndex: 'TARIF',
				width: 70,
				align:'right',
				readOnly: true,
				renderer: function(v, params, record) 
				{
					return formatCurrency(record.data.TARIF);
				},
			}		
		]
	)
};
// End Function FormLookupPemeriksaanGrid # --------------

/**
*	Function : FormLookupObatPemeriksaanGrid
*	
*	Sebuah fungsi untuk menampilkan windows popup obat
*/

function FormLookupObatPemeriksaanGrid(criteria,dsStore,p,mBolAddNew,idx,mBolLookup) 
{
	
    var vWinFormEntryObat_vLookupPemeriksaanGrid = new Ext.Window
	(
		{
			id: 'FormGrdLookup',
			title: 'Lookup Obat',
			closable:true,
			width: 450,
			height: 400,
			border: true,
			plain: true,
			resizable:false,
			layout: 'form',
			iconCls: 'find',
			modal: true,                                   
			items: 
			[
				fnGetDTLGridLookUpFEObat_vLookupPemeriksaanGrid(criteria,dsStore,p,mBolAddNew,idx,mBolLookup,vWinFormEntryObat_vLookupPemeriksaanGrid),
				{
					xtype:'button',
					text:'Ok',
					width:70,
					style:{'margin-left':'360px','margin-top':'7px'},
					hideLabel:true,
					id: 'btnOkAcc',
					handler:function()
					{
						if (p != undefined && rowSelectedLookObat_vLookupPemeriksaanGrid != undefined)
						{
							p.data.KD_OBAT=rowSelectedLookObat_vLookupPemeriksaanGrid.data.KD_OBAT;
							p.data.KD_OBAT_VIEW=rowSelectedLookObat_vLookupPemeriksaanGrid.data.KD_OBAT_VIEW;
							p.data.NAMA_OBAT=rowSelectedLookObat_vLookupPemeriksaanGrid.data.NAMA_OBAT;
							p.data.HARGA=rowSelectedLookObat_vLookupPemeriksaanGrid.data.HARGA_JUAL;
							p.data.STOK=rowSelectedLookObat_vLookupPemeriksaanGrid.data.STOK;
							p.data.QTY="";
							p.data.CARA_PAKAI="";
							
							dsStore.removeAt(idx);
							dsStore.insert(idx, p);
						}
						rowSelectedLookObat_vLookupPemeriksaanGrid='';
						vWinFormEntryObat_vLookupPemeriksaanGrid.close();
					}	
				}
			],
			listeners:
				{ 
					activate: function()
					{ } 
				}
		}
	);
    vWinFormEntryObat_vLookupPemeriksaanGrid.show();
	mWindowGridLookupObat  = vWinFormEntryObat_vLookupPemeriksaanGrid; 
};

//-------------- ## --------------

function fnGetDTLGridLookUpFEObat_vLookupPemeriksaanGrid(criteria,dsStore,p,mBolAddNew,idx,mBolLookup,vWinFormEntryObat_vLookupPemeriksaanGrid) 
{  
	var fldDetailObat_vLookupPemeriksaanGrid = 
	[
		'KD_OBAT', 'KD_OBAT_VIEW', 'KD_CUSTOMER', 'NAMA_OBAT', 'MIN_STOK', 'STOK', 'HARGA_BELI', 'HARGA_JUAL', 'SATUAN'
	];
	
	dsLookupListObat_vLookupPemeriksaanGrid = new WebApp.DataStore({ fields: fldDetailObat_vLookupPemeriksaanGrid });
	var vGridLookAccFormEntryObat_vLookupPemeriksaanGrid = new Ext.grid.EditorGridPanel
	(
		{ 
			id:'vGridLookAccFormEntryObat_vLookupPemeriksaanGrid',
			title: '',
			stripeRows: true,
			store: dsLookupListObat_vLookupPemeriksaanGrid,
			height:330,
			columnLines:true,
			bodyStyle: 'padding:0px',
			border: false,		
			sm: new Ext.grid.RowSelectionModel
			(
				{ 
					singleSelect: true,
					listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedLookObat_vLookupPemeriksaanGrid = undefined;
							rowSelectedLookObat_vLookupPemeriksaanGrid = dsLookupListObat_vLookupPemeriksaanGrid.getAt(row);
							CurrentDataObat_vLookupPemeriksaanGrid;
							CurrentDataObat_vLookupPemeriksaanGrid.row = row;
					        CurrentDataObat_vLookupPemeriksaanGrid.data = rowSelectedLookObat_vLookupPemeriksaanGrid.data;
						}
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, row, rec)
				{
					/*rowSelectedLookObat_vLookupPemeriksaanGrid = dsLookupListObat_vLookupPemeriksaanGrid.getAt(row);
					CurrentDataObat_vLookupPemeriksaanGrid.row = rec;
					CurrentDataObat_vLookupPemeriksaanGrid.data = rowSelectedLookObat_vLookupPemeriksaanGrid;*/
					if (p != undefined && rowSelectedLookObat_vLookupPemeriksaanGrid != undefined)
						{
							p.data.KD_OBAT=rowSelectedLookObat_vLookupPemeriksaanGrid.data.KD_OBAT;
							p.data.KD_OBAT_VIEW=rowSelectedLookObat_vLookupPemeriksaanGrid.data.KD_OBAT_VIEW;
							p.data.NAMA_OBAT=rowSelectedLookObat_vLookupPemeriksaanGrid.data.NAMA_OBAT;
							p.data.HARGA=rowSelectedLookObat_vLookupPemeriksaanGrid.data.HARGA_JUAL;
							p.data.STOK=rowSelectedLookObat_vLookupPemeriksaanGrid.data.STOK;
							p.data.QTY="";
							p.data.CARA_PAKAI="";

							dsStore.removeAt(idx);
							dsStore.insert(idx, p);
						}
						rowSelectedLookObat_vLookupPemeriksaanGrid='';
						mWindowGridLookupObat.close();
				}
			},
			cm: fnGridLookAccColumnModelObat_vLookupPemeriksaanGrid(),
			viewConfig: { forceFit: true }
		});
		
	dsLookupListObat_vLookupPemeriksaanGrid.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 100, 
				Sort: 'KD_OBAT', 
				Sortdir: 'ASC', 
				target:'viObat',
				param: criteria
			} 
		}
	);
	return vGridLookAccFormEntryObat_vLookupPemeriksaanGrid;
};

//-------------- ## --------------

function fnGridLookAccColumnModelObat_vLookupPemeriksaanGrid() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{ 
				id: 'colKD_OBAT',
				header: "KD OBAT",
				dataIndex: 'KD_OBAT_VIEW',
				width: 100
			},
			{ 
				id: 'colNAMA_OBAT',
				header: "NAMA OBAT",
				dataIndex: 'NAMA_OBAT',
				width: 170
			},
			{ 
				id: 'colSATUAN',
				header: "SATUAN",
				dataIndex: 'SATUAN',
				width: 70
			}		
		]
	)
};
// End Function FormLookupObatPemeriksaanGrid # --------------

/**
*	Function : FormLookupDiagnosaPemeriksaanGrid
*	
*	Sebuah fungsi untuk menampilkan windows popup diagnosa
*/

function FormLookupDiagnosaPemeriksaanGrid(criteria,dsStore,p,mBolAddNew,idx,mBolLookup) 
{
	
    var vWinFormEntryDiagnosa_vLookupPemeriksaanGrid = new Ext.Window
	(
		{
			id: 'FormGrdLookup',
			title: 'Lookup Diagnosa',
			closable:true,
			width: 450,
			height: 400,
			border: true,
			plain: true,
			resizable:false,
			layout: 'form',
			iconCls: 'find',
			modal: true,                                   
			items: 
			[
				fnGetDTLGridLookUpFEDiagnosa_vLookupPemeriksaanGrid(criteria,dsStore,p,mBolAddNew,idx,mBolLookup,vWinFormEntryDiagnosa_vLookupPemeriksaanGrid),
				{
					xtype:'button',
					text:'Ok',
					width:70,
					style:{'margin-left':'360px','margin-top':'7px'},
					hideLabel:true,
					id: 'btnOkAcc',
					handler:function()
					{
						if (p != undefined && rowSelectedLookDiagnosa_vLookupPemeriksaanGrid != undefined)
						{
							p.data.KD_PENYAKIT=rowSelectedLookDiagnosa_vLookupPemeriksaanGrid.data.KD_PENYAKIT;
							p.data.KD_PENYAKIT_VIEW=rowSelectedLookDiagnosa_vLookupPemeriksaanGrid.data.KD_PENYAKIT_VIEW;
							p.data.PENYAKIT=rowSelectedLookDiagnosa_vLookupPemeriksaanGrid.data.PENYAKIT;
							p.data.STATUS_BAHAYA=0;
							
							dsStore.removeAt(idx);
							dsStore.insert(idx, p);
						}
						rowSelectedLookDiagnosa_vLookupPemeriksaanGrid='';
						vWinFormEntryDiagnosa_vLookupPemeriksaanGrid.close();
					}	
				}
			],
			listeners:
				{ 
					activate: function()
					{ } 
				}
		}
	);
    vWinFormEntryDiagnosa_vLookupPemeriksaanGrid.show();
	mWindowGridLookupDiagnosa  = vWinFormEntryDiagnosa_vLookupPemeriksaanGrid; 
};

//-------------- ## --------------

function fnGetDTLGridLookUpFEDiagnosa_vLookupPemeriksaanGrid(criteria,dsStore,p,mBolAddNew,idx,mBolLookup,vWinFormEntryDiagnosa_vLookupPemeriksaanGrid) 
{  
	var fldDetailDiagnosa_vLookupPemeriksaanGrid = 
	[
		'KD_PENYAKIT', 'KD_PENYAKIT_VIEW', 'PEN_KD_PENYAKIT', 'PENYAKIT'
	];
	
	dsLookupListDiagnosa_vLookupPemeriksaanGrid = new WebApp.DataStore({ fields: fldDetailDiagnosa_vLookupPemeriksaanGrid });
	var vGridLookAccFormEntryDiagnosa_vLookupPemeriksaanGrid = new Ext.grid.EditorGridPanel
	(
		{ 
			id:'vGridLookAccFormEntryDiagnosa_vLookupPemeriksaanGrid',
			title: '',
			stripeRows: true,
			store: dsLookupListDiagnosa_vLookupPemeriksaanGrid,
			height:330,
			columnLines:true,
			bodyStyle: 'padding:0px',
			border: false,		
			sm: new Ext.grid.RowSelectionModel
			(
				{ 
					singleSelect: true,
					listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedLookDiagnosa_vLookupPemeriksaanGrid = undefined;
							rowSelectedLookDiagnosa_vLookupPemeriksaanGrid = dsLookupListDiagnosa_vLookupPemeriksaanGrid.getAt(row);
							CurrentDataDiagnosa_vLookupPemeriksaanGrid;
							CurrentDataDiagnosa_vLookupPemeriksaanGrid.row = row;
					        CurrentDataDiagnosa_vLookupPemeriksaanGrid.data = rowSelectedLookDiagnosa_vLookupPemeriksaanGrid.data;
						}
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, row, rec)
				{
					/*rowSelectedLookDiagnosa_vLookupPemeriksaanGrid = dsLookupListDiagnosa_vLookupPemeriksaanGrid.getAt(row);
					CurrentDataDiagnosa_vLookupPemeriksaanGrid.row = rec;
					CurrentDataDiagnosa_vLookupPemeriksaanGrid.data = rowSelectedLookDiagnosa_vLookupPemeriksaanGrid;*/
					if (p != undefined && rowSelectedLookDiagnosa_vLookupPemeriksaanGrid != undefined)
						{
							p.data.KD_PENYAKIT=rowSelectedLookDiagnosa_vLookupPemeriksaanGrid.data.KD_PENYAKIT;
							p.data.KD_PENYAKIT_VIEW=rowSelectedLookDiagnosa_vLookupPemeriksaanGrid.data.KD_PENYAKIT_VIEW;
							p.data.PENYAKIT=rowSelectedLookDiagnosa_vLookupPemeriksaanGrid.data.PENYAKIT;
							p.data.STATUS_BAHAYA=0;

							dsStore.removeAt(idx);
							dsStore.insert(idx, p);
						}
						rowSelectedLookDiagnosa_vLookupPemeriksaanGrid='';
						mWindowGridLookupDiagnosa.close();
				}
			},
			cm: fnGridLookAccColumnModelDiagnosa_vLookupPemeriksaanGrid(),
			viewConfig: { forceFit: true }
		});
		
	dsLookupListDiagnosa_vLookupPemeriksaanGrid.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 100, 
				Sort: 'KD_PENYAKIT', 
				Sortdir: 'ASC', 
				target:'viPenyakit',
				param: criteria
			} 
		}
	);
	return vGridLookAccFormEntryDiagnosa_vLookupPemeriksaanGrid;
};

//-------------- ## --------------

function fnGridLookAccColumnModelDiagnosa_vLookupPemeriksaanGrid() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{ 
				id: 'colKD_PENYAKIT',
				header: "KD ICD",
				dataIndex: 'KD_PENYAKIT_VIEW',
				width: 140
			},
			{ 
				id: 'colPENYAKIT',
				header: "NAMA ICD",
				dataIndex: 'PENYAKIT',
				width: 200
			}		
		]
	)
};
// End Function FormLookupDiagnosaPemeriksaanGrid # --------------

/**
*	Function : FormLookupObatResepGrid
*	
*	Sebuah fungsi untuk menampilkan windows popup obat
*/

function FormLookupObatResepGrid(criteria,dsStore,p,mBolAddNew,idx,mBolLookup) 
{
	
    var vWinFormEntryObatResep_vLookupResepGrid = new Ext.Window
	(
		{
			id: 'FormGrdLookup',
			title: 'Lookup Obat',
			closable:true,
			width: 450,
			height: 400,
			border: true,
			plain: true,
			resizable:false,
			layout: 'form',
			iconCls: 'find',
			modal: true,                                   
			items: 
			[
				fnGetDTLGridLookUpFEObatResep_vLookupResepGrid(criteria,dsStore,p,mBolAddNew,idx,mBolLookup,vWinFormEntryObatResep_vLookupResepGrid),
				{
					xtype:'button',
					text:'Ok',
					width:70,
					style:{'margin-left':'360px','margin-top':'7px'},
					hideLabel:true,
					id: 'btnOkAcc',
					handler:function()
					{
						if (p != undefined && rowSelectedLookObatResep_vLookupResepGrid != undefined)
						{
							p.data.KD_OBAT=rowSelectedLookObatResep_vLookupResepGrid.data.KD_OBAT;
							p.data.KD_OBAT_VIEW=rowSelectedLookObatResep_vLookupResepGrid.data.KD_OBAT_VIEW;
							p.data.NAMA_OBAT=rowSelectedLookObatResep_vLookupResepGrid.data.NAMA_OBAT;
							p.data.HARGA=rowSelectedLookObatResep_vLookupResepGrid.data.HARGA_JUAL;
							p.data.STOK=rowSelectedLookObatResep_vLookupResepGrid.data.STOK;
							p.data.QTY="";
							p.data.CARA_PAKAI="";
							
							dsStore.removeAt(idx);
							dsStore.insert(idx, p);
						}
						rowSelectedLookObatResep_vLookupResepGrid='';
						vWinFormEntryObatResep_vLookupResepGrid.close();
					}	
				}
			],
			listeners:
				{ 
					activate: function()
					{ } 
				}
		}
	);
    vWinFormEntryObatResep_vLookupResepGrid.show();
	mWindowGridLookupObatResep  = vWinFormEntryObatResep_vLookupResepGrid; 
};

//-------------- ## --------------

function fnGetDTLGridLookUpFEObatResep_vLookupResepGrid(criteria,dsStore,p,mBolAddNew,idx,mBolLookup,vWinFormEntryObatResep_vLookupResepGrid) 
{  
	var fldDetailObatResep_vLookupResepGrid = 
	[
		'KD_OBAT', 'KD_OBAT_VIEW', 'KD_CUSTOMER', 'NAMA_OBAT', 'MIN_STOK', 'STOK', 'HARGA_BELI', 'HARGA_JUAL', 'SATUAN'
	];
	
	dsLookupListObatResep_vLookupResepGrid = new WebApp.DataStore({ fields: fldDetailObatResep_vLookupResepGrid });
	var vGridLookAccFormEntryObatResep_vLookupResepGrid = new Ext.grid.EditorGridPanel
	(
		{ 
			id:'vGridLookAccFormEntryObatResep_vLookupResepGrid',
			title: '',
			stripeRows: true,
			store: dsLookupListObatResep_vLookupResepGrid,
			height:330,
			columnLines:true,
			bodyStyle: 'padding:0px',
			border: false,		
			sm: new Ext.grid.RowSelectionModel
			(
				{ 
					singleSelect: true,
					listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedLookObatResep_vLookupResepGrid = undefined;
							rowSelectedLookObatResep_vLookupResepGrid = dsLookupListObatResep_vLookupResepGrid.getAt(row);
							CurrentDataObatResep_vLookupResepGrid;
							CurrentDataObatResep_vLookupResepGrid.row = row;
					        CurrentDataObatResep_vLookupResepGrid.data = rowSelectedLookObatResep_vLookupResepGrid.data;
						}
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, row, rec)
				{
					/*rowSelectedLookObatResep_vLookupResepGrid = dsLookupListObatResep_vLookupResepGrid.getAt(row);
					CurrentDataObatResep_vLookupResepGrid.row = rec;
					CurrentDataObatResep_vLookupResepGrid.data = rowSelectedLookObatResep_vLookupResepGrid;*/
					if (p != undefined && rowSelectedLookObatResep_vLookupResepGrid != undefined)
						{
							p.data.KD_OBAT=rowSelectedLookObatResep_vLookupResepGrid.data.KD_OBAT;
							p.data.KD_OBAT_VIEW=rowSelectedLookObatResep_vLookupResepGrid.data.KD_OBAT_VIEW;
							p.data.NAMA_OBAT=rowSelectedLookObatResep_vLookupResepGrid.data.NAMA_OBAT;
							p.data.HARGA=rowSelectedLookObatResep_vLookupResepGrid.data.HARGA_JUAL;
							p.data.STOK=rowSelectedLookObatResep_vLookupResepGrid.data.STOK;
							p.data.QTY="";
							p.data.CARA_PAKAI="";

							dsStore.removeAt(idx);
							dsStore.insert(idx, p);
						}
						rowSelectedLookObatResep_vLookupResepGrid='';
						mWindowGridLookupObatResep.close();
				}
			},
			cm: fnGridLookAccColumnModelObatResep_vLookupResepGrid(),
			viewConfig: { forceFit: true }
		});
		
	dsLookupListObatResep_vLookupResepGrid.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 100, 
				Sort: 'KD_OBAT', 
				Sortdir: 'ASC', 
				target:'viObat',
				param: criteria
			} 
		}
	);
	return vGridLookAccFormEntryObatResep_vLookupResepGrid;
};

//-------------- ## --------------

function fnGridLookAccColumnModelObatResep_vLookupResepGrid() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{ 
				id: 'colKD_OBAT',
				header: "KD OBAT",
				dataIndex: 'KD_OBAT_VIEW',
				width: 100
			},
			{ 
				id: 'colNAMA_OBAT',
				header: "NAMA OBAT",
				dataIndex: 'NAMA_OBAT',
				width: 170
			},
			{ 
				id: 'colSATUAN',
				header: "SATUAN",
				dataIndex: 'SATUAN',
				width: 70
			}		
		]
	)
};
// End Function FormLookupObatResepGrid # --------------