/// <reference path="../../ext-base.js" />
/// <reference path="../../ext-all.js" />

var rowSelectedLookProduk;
var vWinFormEntryLookupProduk;
var dsCategoryLookupProduk;
var dsDeptLookupProduk;

var dsLookProdukList;
var IdKlas_produkegorySetEquipmentKlas_produkView='0';
var str;


function FormLookupKonsultasi(criteria,dsStore,p,mBolAddNew,idx,mBolLookup) 
{
	
	rowSelectedLookProduk=undefined;
    vWinFormEntryLookupProduk = new Ext.Window
	(
		{
			id: 'vWinFormEntryLookupProduk',
			title: 'Lihat Produk',
			closeAction: 'hide',
			closable:false,
			width: 550,
			height: 600,//420,
			border: false,
			plain: true,
			resizable:false,
			layout: 'form',
			iconCls: 'find',
			modal: true,                                   
			items: 
			[	itemsTreeListProdukGridDataView_viKasirRwj(),
				GetPanelLookUpProduk(criteria,dsStore,p,mBolAddNew,idx,mBolLookup) 
				
			],
			listeners:
				{ 
					activate: function()
					{ } 
				}
		}
	);
	
    vWinFormEntryLookupProduk.show();
};


function itemsTreeListProdukGridDataView_viKasirRwj()
{	
		
	var treeKlas_produk_viKasirRwj= new Ext.tree.TreePanel
	(
		{
			autoScroll: true,
			split: true,
			height:200,
			loader: new Ext.tree.TreeLoader(),
			listeners: 
			{
				click: function(n) 
				{
					strTreeCriteriProdukEquipment_viKasirRwj=n.attributes
					rowSelectedTreeSetEquipmentKlas_produk=n.attributes;
					if (strTreeCriteriProdukEquipment_viKasirRwj.id != ' 0')
					{
						if (strTreeCriteriProdukEquipment_viKasirRwj.leaf == false)
						{
							var str='';
							str=' and left(parent,' + n.attributes.id.length + ')=' + n.attributes.id
                                                        str='substring(parent,1, ' + n.attributes.id.length + ') = ~' + n.attributes.id + '~';
							RefreshDatProdukEquipmentCat(str);
						}
						else
						{
							RefreshDatProdukEquipmentCat('parent = ~' + strTreeCriteriProdukEquipment_viKasirRwj.id + '~');
						};
					}
					else
					{
						RefreshDatProdukEquipmentCat('');
					};
				}
			}
		}
	);
	
	rootTreeSetEquipmentKlas_produkView = new Ext.tree.AsyncTreeNode
	(
		{
			expanded: true,
			text:'Master Produk',
			id:IdKlas_produkegorySetEquipmentKlas_produkView,
			children: StrTreeSetEquipment,
			autoScroll: true
		}
	)  
  
  treeKlas_produk_viKasirRwj.setRootNode(rootTreeSetEquipmentKlas_produkView);  
	
	
	
	
	
	

    var pnlTreeFormDataWindowPopup_viKasirRwj = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                
				//-------------- ## -------------- 
                treeKlas_produk_viKasirRwj,
                //-------------- ## -------------- 
            ],
            //-------------- #End items# --------------
        }
    )

    return pnlTreeFormDataWindowPopup_viKasirRwj;
}


function GetStrTreeSetEquipment()
{
	 Ext.Ajax.request
	 (
		{
			url: WebAppUrl.UrlExecProcess,
			params: 
			{
				UserID: strUser, // 'Admin',
				ModuleID: 'ProsesGetDataTreeSetCat',
				Params:	''
			},
			success : function(resp) 
			{
				var cst = Ext.decode(resp.responseText);
				StrTreeSetEquipment= cst.arr;
			},
			failure:function()
			{
			    
			}
		}
	);
};

function GetPanelLookUpProduk(criteria,dsStore,p,mBolAddNew,idx,mBolLookup) 
{
	
	var FormLookupKonsultasi = new Ext.Panel  
	(
		{
			id: 'FormLookupKonsultasi',
			region: 'center',
			layout: 'form',
			title: '',
			bodyStyle: 'padding:15px',
			anchor:'100%',
			border: false,
			bodyStyle: 'background:#FFFFFF;',
			height: 510,//392,
			shadhow: true,
			items: 
			[
				//getItemPanelCboCatLookupProduk(),
				fnGetDTLGridLookUpProduk(criteria),
				getItemPanelLookupProduk(dsStore,p,mBolAddNew,idx,mBolLookup)
			]
        }
	);
	
	return FormLookupKonsultasi;
};


function getItemPanelLookupProduk(dsStore,p,mBolAddNew,idx,mBolLookup) 
{
    var items =
	{
	    layout: 'column',
	    border: true,
		height:39,
		anchor:'100%',
		style:{'margin-top':'-1px'},
	    items:
		[
			{
				layout: 'hBox',
				width:522,
				border: false,
				bodyStyle: 'padding:5px 0px 5px 5px',
				defaults: { margins: '3 3 3 3' },
				anchor: '90%',
				layoutConfig: 
				{
					align: 'middle',
					pack:'end'
				},
				items:
				[
					{
						xtype:'button',
						text:nmBtnOK,
						width:70,
						style:{'margin-left':'0px','margin-top':'0px'},
						hideLabel:true,
						id: 'btnOkLookupProduk',
						handler:function()
						{
							if (p != undefined && rowSelectedLookProduk != undefined)
							{
								if (mBolAddNew === true || mBolAddNew === undefined)
									{
										p.data.DESKRIPSI2 =rowSelectedLookProduk.data.DESKRIPSI;
										p.data.KD_PRODUK=rowSelectedLookProduk.data.KD_PRODUK;
										p.data.DESKRIPSI=rowSelectedLookProduk.data.DESKRIPSI;
										p.data.KD_TARIF=rowSelectedLookProduk.data.KD_TARIF;
										p.data.HARGA=rowSelectedLookProduk.data.TARIF;
										p.data.TGL_BERLAKU=rowSelectedLookProduk.data.TGL_BERLAKU;
										p.data.QTY=1;
										
										if (mBolLookup === true)
										{
											if(dsStore.getCount() === 0)
											{
												dsStore.insert(dsStore.getCount(), p);
												Datasave_KasirRWJ(false);
											}
											else
											{
												if (dsStore.data.items[dsStore.getCount()-1].data.KD_PRODUK === '' || dsStore.data.items[dsStore.getCount()-1].data.KD_PRODUK === undefined)
												{
													dsStore.removeAt(dsStore.getCount()-1);
													dsStore.insert(dsStore.getCount(), p);
													Datasave_KasirRWJ(false);
												}
												else
												{
													dsStore.insert(dsStore.getCount(), p);
													Datasave_KasirRWJ(false);
												};
											};
										}
										else
										{
										   dsStore.removeAt(dsStore.getCount()-1);
										   dsStore.insert(dsStore.getCount(), p);
										}
										
									}
									else
									{
										if (dsStore != undefined)
										{
										//


										p.data.ASSET_MAINT =rowSelectedLookProduk.data.DESKRIPSI;
											p.data.KD_PRODUK=rowSelectedLookProduk.data.KD_PRODUK;
											p.data.DESKRIPSI=rowSelectedLookProduk.data.DESKRIPSI;
											p.data.KD_TARIF=rowSelectedLookProduk.data.KD_TARIF;
											p.data.HARGA=rowSelectedLookProduk.data.TARIF;
											p.data.TGL_BERLAKU=rowSelectedLookProduk.data.TGL_BERLAKU;
											p.data.PROBLEM=1;
											dsStore.removeAt(idx);
											dsStore.insert(idx, p);
										}
									}
								vWinFormEntryLookupProduk.close();
							}
							else
							{
								ShowPesanWarningLookupProduk('Silahkan pilih Baris','Lihat Produk');
							};
							
						}
					},
					{
							xtype:'button',
							text:nmBtnCancel ,
							width:70,
							hideLabel:true,
							id: 'btnCancelLookupProduk',
							handler:function() 
							{
								vWinFormEntryLookupProduk.close();
							}
					}
				]
			}
		]
	}
    return items;
};

function ShowPesanWarningLookupProduk(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function fnGetDTLGridLookUpProduk(criteria) 
{

    var fldDetail = ['TARIF','KLASIFIKASI','PERENT','TGL_BERAKHIR','KD_KAT','KD_TARIF','KD_KLAS','DESKRIPSI','YEARS','NAMA_UNIT','KD_PRODUK','TGL_BERLAKU'];
	dsLookProdukList = new WebApp.DataStore({ fields: fldDetail });

	var vGridLookProdukFormEntry = new Ext.grid.EditorGridPanel
	(
		{ 
			id:'vGridLookProdukFormEntry',
			title: '',
			stripeRows: true,
			store: dsLookProdukList,
			style:{'margin-top':'-1px'},
			height:300,
			columnLines:true,
			bodyStyle: 'padding:0px',
			border: true,	
			frame:false,
			sm: new Ext.grid.RowSelectionModel
			(
				{ 
					singleSelect: true,
					listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedLookProduk = dsLookProdukList.getAt(row);
						}
					}
				}
			),
			cm: fnGridLookProdukColumnModel(),
			viewConfig: { forceFit: true }
		}
	);
	

	RefreshDatProdukEquipmentCat(str);
	
	return vGridLookProdukFormEntry;
};



function RefreshDatProdukEquipmentCat(str)
{
		
 if (str == undefined || str == 'parent = ~0~')
  {
	str='LOWER(kd_tarif)=LOWER(~TU~) and left(kd_unit,3)=~202~ and tgl_berakhir is null' ;
  }else
  {
	str='LOWER(kd_tarif)=LOWER(~TU~) and left(kd_unit,3)=~202~ and tgl_berakhir is null and '+ str;
  };
	dsLookProdukList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				//Sort: 'PERENT',
                                Sort: 'KD_PRODUK',
				Sortdir: 'ASC',
				target: 'LookupAsset',
				param: str
			}
		}
	);
	return dsLookProdukList;
};



function fnGridLookProdukColumnModel() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			
			{ 
				id: 'colProdukNameLook',
				header: 'Produk',
				dataIndex: 'DESKRIPSI',
				width: 300
            },
			{ 
				id: 'colProdukTARIFLook',
				header: 'Harga',
				align: 'right',
				dataIndex: 'TARIF',
				width: 300,
				renderer: function(v, params, record) 
							{
							return formatCurrency(record.data.TARIF);
							
							}	
            }
		]
	)
};




///---------------------------------------------------------------------------------------///