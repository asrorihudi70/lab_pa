/// <reference path="../../ext-base.js" />
/// <reference path="../../ext-all.js" />

var rowSelectedLookPGW_vLookupObatGrid;
var mWindowGridLookup;
var CurrentData_vLookupObat =
	{
	    data: Object,
	    details: Array,
	    row: 0
	};
	
//var	kdTemp;
//var urutTemp;
var dsLookupPgwList_vLookupObatGrid;

// -- Stok Opname Obat --
var rowSelectedLookObat_viStokOpname;
var mWindowGridLookupObat_viStokOpname;
var CurrentDataObat_viStokOpname =
    {
        data: Object,
        details: Array,
        row: 0
    };
    
//var   kdTemp;
//var urutTemp;
var dsLookupListObat_viStokOpname;
// -- End Stok Opname Obat --

function FormLookupObatGrid(criteria,dsStore,p,mBolAddNew,idx,mBolLookup,kdTemp,urutTemp) //(criteria,dsStore,p,mBolAddNew,idx,mBolLookup,kdTemp,urutTemp) 
{
	
    var vWinFormEntry = new Ext.Window
	(
		{
			id: 'FormGrdPgwLookup',
			title: 'Lookup Obat',
			closable:true,
			width: 450,
			height: 400,
			border: true,
			plain: true,
			resizable:false,
			layout: 'form',
			iconCls: 'find',
			modal: true,                                   
			items: 
			[
				//(criteria,dsStore,p,mBolAddNew,idx,mBolLookup,vWinFormEntry,kdTemp,urutTemp),
				fnGetDTLGridLookUpFE_viPenerimaanApotek(criteria,dsStore,p,mBolAddNew,idx,mBolLookup,vWinFormEntry,kdTemp,urutTemp),
				{
					xtype:'button',
					text:'Ok',
					width:70,
					style:{'margin-left':'360px','margin-top':'7px'},
					hideLabel:true,
					id: 'btnOkAcc',
					handler:function()
					{
						if (p != undefined && rowSelectedLookPGW_vLookupObatGrid != undefined)
						{										
							if (mBolAddNew === true || mBolAddNew === undefined)
							{
								p.data.KD_OBAT = rowSelectedLookPGW_vLookupObatGrid.data.KD_OBAT_VIEW;
								p.data.NAMA_OBAT = rowSelectedLookPGW_vLookupObatGrid.data.NAMA_OBAT;
								p.data.HARGA_BELI = rowSelectedLookPGW_vLookupObatGrid.data.HARGA_BELI;
								p.data.KD_OBAT_TEMP = kdTemp;
								p.data.URUT_IN_TEMP = urutTemp;

								if (mBolLookup === true)
								{								
								   dsStore.insert(dsStore.getCount(), p);
								}
								else
								{
								   dsStore.removeAt(dsStore.getCount()-1);
								   dsStore.insert(dsStore.getCount(), p);
								}
								
							}
							else
							{
								if (dsStore != undefined)
								{
									p.data.KD_OBAT = rowSelectedLookPGW_vLookupObatGrid.data.KD_OBAT_VIEW;
									p.data.NAMA_OBAT = rowSelectedLookPGW_vLookupObatGrid.data.NAMA_OBAT;
									p.data.HARGA_BELI = rowSelectedLookPGW_vLookupObatGrid.data.HARGA_BELI;
									p.data.KD_OBAT_TEMP = kdTemp;
									p.data.URUT_IN_TEMP = urutTemp;
								
									dsStore.removeAt(idx);
									dsStore.insert(idx, p);
								}
							}
						}
						rowSelectedLookPGW_vLookupObatGrid='';
						vWinFormEntry.close();
					}	
				}
			],
			listeners:
				{ 
					activate: function()
					{ } 
				}
		}
	);
    vWinFormEntry.show();
	mWindowGridLookup  = vWinFormEntry; 
};

///---------------------------------------------------------------------------------------///

function fnGetDTLGridLookUpFE_viPenerimaanApotek(criteria,dsStore,p,mBolAddNew,idx,mBolLookup,vWinFormEntry,kdTemp,urutTemp) //(criteria,dsStore,p,mBolAddNew,idx,mBolLookup,vWinFormEntry,kdTemp,urutTemp) 
{  

	var fldDetail = 
	[
		'KD_OBAT',
		'KD_OBAT_VIEW',
		'NAMA_OBAT', 
		'HARGA_BELI'
	];
	
	dsLookupPgwList_vLookupObatGrid = new WebApp.DataStore({ fields: fldDetail });
	var vGridLookAccFormEntry_vLookupObatGrid = new Ext.grid.EditorGridPanel
	(
		{ 
			id:'vGridLookAccFormEntry_vLookupObatGrid',
			title: '',
			stripeRows: true,
			store: dsLookupPgwList_vLookupObatGrid,
			height:330,
			columnLines:true,
			bodyStyle: 'padding:0px',
			border: false,		
			sm: new Ext.grid.RowSelectionModel
			(
				{ 
					singleSelect: true,
					listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedLookPGW_vLookupObatGrid = dsLookupPgwList_vLookupObatGrid.getAt(row);
							CurrentData_vLookupObat.row = row;
					        CurrentData_vLookupObat.data = rowSelectedLookPGW_vLookupObatGrid;
						}
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, row, rec)
				{
					rowSelectedLookPGW_vLookupObatGrid = dsLookupPgwList_vLookupObatGrid.getAt(row);
					CurrentData_vLookupObat.row = rec;
					CurrentData_vLookupObat.data = rowSelectedLookPGW_vLookupObatGrid;
					
					if (p != undefined && rowSelectedLookPGW_vLookupObatGrid != undefined)
						{											
							if (mBolAddNew === true || mBolAddNew === undefined)
							{
								p.data.KD_OBAT = rowSelectedLookPGW_vLookupObatGrid.data.KD_OBAT_VIEW;
								p.data.NAMA_OBAT = rowSelectedLookPGW_vLookupObatGrid.data.NAMA_OBAT;
								p.data.HARGA_BELI = rowSelectedLookPGW_vLookupObatGrid.data.HARGA_BELI;
								//p.data.KD_OBAT_TEMP = kdTemp;
								//p.data.URUT_IN_TEMP = urutTemp;

								if (mBolLookup === true)
								{								
								   dsStore.insert(dsStore.getCount(), p);
								}
								else
								{
								   dsStore.removeAt(dsStore.getCount()-1);
								   dsStore.insert(dsStore.getCount(), p);
								}
								
							}
							else
							{
								if (dsStore != undefined)
								{
								
									p.data.KD_OBAT = rowSelectedLookPGW_vLookupObatGrid.data.KD_OBAT_VIEW;
									p.data.NAMA_OBAT = rowSelectedLookPGW_vLookupObatGrid.data.NAMA_OBAT;
									p.data.HARGA_BELI = rowSelectedLookPGW_vLookupObatGrid.data.HARGA_BELI;
									p.data.KD_OBAT_TEMP = kdTemp;
									p.data.URUT_IN_TEMP = urutTemp;
									
									dsStore.removeAt(idx);
									dsStore.insert(idx, p);
								}
							}
						}
						rowSelectedLookPGW_vLookupObatGrid='';
						mWindowGridLookup.close();
				}
			},
			cm: fnGridLookAccColumnModel_viPenerimaanApotek(),
			viewConfig: { forceFit: true }
		});
		
	dsLookupPgwList_vLookupObatGrid.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 100, 
				Sort: 'KD_OBAT', 
				Sortdir: 'ASC', 
				target:'viObat',
				param: criteria
			} 
		}
	);
	return vGridLookAccFormEntry_vLookupObatGrid;
};



function fnGridLookAccColumnModel_viPenerimaanApotek() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{ 
				id: 'colKDObat',
				header: "Kd. Obat",
				dataIndex: 'KD_OBAT_VIEW',
				width: 70
			},
			{ 
				id: 'colNamaObat',
				header: "Nama Obat",
				dataIndex: 'NAMA_OBAT',
				width: 200
			},
			{ 
				id: 'colHarga',
				header: "Harga",
				dataIndex: 'HARGA_BELI',
				width: 70,
				align:'right',
				readOnly: true,
				renderer: function(v, params, record) 
				{
					return formatCurrency(record.data.HARGA_BELI);
				},
			}		
		]
	)
};
///---------------------------------------------------------------------------------------///
// -- Start function look up Stok Opname Obat --
function FormLookupObatGrid_viStokOpname(criteria,dsStore,p,mBolAddNew,idx,mBolLookup,kdTemp,urutTemp) //(criteria,dsStore,p,mBolAddNew,idx,mBolLookup,kdTemp,urutTemp) 
{
    
    var vWinFormEntryObat_viStokOpname = new Ext.Window
    (
        {
            id: 'FormGrdPgwLookup_viStokOpname',
            title: 'Lookup Obat Stok Opname',
            closable:true,
            width: 450,
            height: 400,
            border: true,
            plain: true,
            resizable:false,
            layout: 'form',
            iconCls: 'find',
            modal: true,                                   
            items: 
            [
                //(criteria,dsStore,p,mBolAddNew,idx,mBolLookup,vWinFormEntryObat_viStokOpname,kdTemp,urutTemp),
                fnGetDTLGridLookUpFEObat_viStokOpname(criteria,dsStore,p,mBolAddNew,idx,mBolLookup,vWinFormEntryObat_viStokOpname,kdTemp,urutTemp),
                {
                    xtype:'button',
                    text:'Ok',
                    width:70,
                    style:{'margin-left':'360px','margin-top':'7px'},
                    hideLabel:true,
                    id: 'btnOkAcc_viStokOpname',
                    handler:function()
                    {
                        if (p != undefined && rowSelectedLookObat_viStokOpname != undefined)
                        {                                       
                            if (mBolAddNew === true || mBolAddNew === undefined)
                            {
                                p.data.KD_OBAT = rowSelectedLookObat_viStokOpname.data.KD_OBAT;
                                p.data.KD_OBAT_TMP = rowSelectedLookObat_viStokOpname.data.KD_OBAT_VIEW;
                                p.data.NAMA_OBAT = rowSelectedLookObat_viStokOpname.data.NAMA_OBAT;
                                p.data.QTY_AWAL = rowSelectedLookObat_viStokOpname.data.STOK;
                                p.data.SATUAN = rowSelectedLookObat_viStokOpname.data.SATUAN;
                                p.data.KD_OBAT_TEMP = kdTemp;
                                p.data.URUT_IN_TEMP = urutTemp;

                                if (mBolLookup === true)
                                {                               
                                   dsStore.insert(dsStore.getCount(), p);
                                }
                                else
                                {
                                   dsStore.removeAt(dsStore.getCount()-1);
                                   dsStore.insert(dsStore.getCount(), p);
                                }
                                
                            }
                            else
                            {
                                if (dsStore != undefined)
                                {
                                    p.data.KD_OBAT = rowSelectedLookObat_viStokOpname.data.KD_OBAT;
                                	p.data.KD_OBAT_TMP = rowSelectedLookObat_viStokOpname.data.KD_OBAT_VIEW;
                                    p.data.NAMA_OBAT = rowSelectedLookObat_viStokOpname.data.NAMA_OBAT;
                                    p.data.QTY_AWAL = rowSelectedLookObat_viStokOpname.data.STOK;
                                    p.data.SATUAN = rowSelectedLookObat_viStokOpname.data.SATUAN;
                                    p.data.KD_OBAT_TEMP = kdTemp;
                                    p.data.URUT_IN_TEMP = urutTemp;
                                
                                    dsStore.removeAt(idx);
                                    dsStore.insert(idx, p);
                                }
                            }
                        }
                        rowSelectedLookObat_viStokOpname='';
                        vWinFormEntryObat_viStokOpname.close();
                    }   
                }
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryObat_viStokOpname.show();
    mWindowGridLookupObat_viStokOpname  = vWinFormEntryObat_viStokOpname; 
};

///---------------------------------------------------------------------------------------///

function fnGetDTLGridLookUpFEObat_viStokOpname(criteria,dsStore,p,mBolAddNew,idx,mBolLookup,vWinFormEntryObat_viStokOpname,kdTemp,urutTemp) //(criteria,dsStore,p,mBolAddNew,idx,mBolLookup,vWinFormEntryObat_viStokOpname,kdTemp,urutTemp) 
{  

    var fldDetailObat_viStokOpname = 
    [
        'KD_OBAT',
        'KD_OBAT_VIEW',
        'NAMA_OBAT', 
        'STOK',
        'SATUAN'
    ];
    
    dsLookupListObat_viStokOpname = new WebApp.DataStore({ fields: fldDetailObat_viStokOpname });
    var vGridLookAccFormEntryObat_viStokOpname = new Ext.grid.EditorGridPanel
    (
        { 
            id:'vGridLookAccFormEntryObat_viStokOpname',
            title: '',
            stripeRows: true,
            store: dsLookupListObat_viStokOpname,
            height:330,
            columnLines:true,
            bodyStyle: 'padding:0px',
            border: false,      
            sm: new Ext.grid.RowSelectionModel
            (
                { 
                    singleSelect: true,
                    listeners:
                    { 
                        rowselect: function(sm, row, rec) 
                        {
                            rowSelectedLookObat_viStokOpname = dsLookupListObat_viStokOpname.getAt(row);
                            CurrentDataObat_viStokOpname.row = row;
                            CurrentDataObat_viStokOpname.data = rowSelectedLookObat_viStokOpname;
                        }
                    }
                }
            ),
            listeners:
            {
                rowdblclick: function (sm, row, rec)
                {
                    rowSelectedLookObat_viStokOpname = dsLookupListObat_viStokOpname.getAt(row);
                    CurrentDataObat_viStokOpname.row = rec;
                    CurrentDataObat_viStokOpname.data = rowSelectedLookObat_viStokOpname;
                    
                    if (p != undefined && rowSelectedLookObat_viStokOpname != undefined)
                        {                                           
                            if (mBolAddNew === true || mBolAddNew === undefined)
                            {
                                p.data.KD_OBAT = rowSelectedLookObat_viStokOpname.data.KD_OBAT;
                                p.data.KD_OBAT_TMP = rowSelectedLookObat_viStokOpname.data.KD_OBAT_VIEW;
                                p.data.NAMA_OBAT = rowSelectedLookObat_viStokOpname.data.NAMA_OBAT;
                                p.data.QTY_AWAL = rowSelectedLookObat_viStokOpname.data.STOK;
                                p.data.SATUAN = rowSelectedLookObat_viStokOpname.data.SATUAN;
                                //p.data.KD_OBAT_TEMP = kdTemp;
                                //p.data.URUT_IN_TEMP = urutTemp;

                                if (mBolLookup === true)
                                {                               
                                   dsStore.insert(dsStore.getCount(), p);
                                }
                                else
                                {
                                   dsStore.removeAt(dsStore.getCount()-1);
                                   dsStore.insert(dsStore.getCount(), p);
                                }
                                
                            }
                            else
                            {
                                if (dsStore != undefined)
                                {
                                
                                    p.data.KD_OBAT = rowSelectedLookObat_viStokOpname.data.KD_OBAT;
                                	p.data.KD_OBAT_TMP = rowSelectedLookObat_viStokOpname.data.KD_OBAT_VIEW;
                                    p.data.NAMA_OBAT = rowSelectedLookObat_viStokOpname.data.NAMA_OBAT;
                                    p.data.QTY_AWAL = rowSelectedLookObat_viStokOpname.data.STOK;
                                    p.data.SATUAN = rowSelectedLookObat_viStokOpname.data.SATUAN;
                                    p.data.KD_OBAT_TEMP = kdTemp;
                                    p.data.URUT_IN_TEMP = urutTemp;
                                    
                                    dsStore.removeAt(idx);
                                    dsStore.insert(idx, p);
                                }
                            }
                        }
                        rowSelectedLookObat_viStokOpname='';
                        mWindowGridLookupObat_viStokOpname.close();
                }
            },
            cm: fnGridLookAccColumnModelObat_viStokOpname(),
            viewConfig: { forceFit: true }
        });
        
    dsLookupListObat_viStokOpname.load
    (
        { 
            params: 
            { 
                Skip: 0, 
                Take: 100, 
                Sort: 'KD_OBAT', 
                Sortdir: 'ASC', 
                target:'viObat',
                param: criteria
            } 
        }
    );
    return vGridLookAccFormEntryObat_viStokOpname;
};



function fnGridLookAccColumnModelObat_viStokOpname() 
{
    return new Ext.grid.ColumnModel
    (
        [
            new Ext.grid.RowNumberer(),
            { 
                id: 'colKDObat',
                header: "Kd. Obat",
                dataIndex: 'KD_OBAT_VIEW',
                width: 70
            },
            { 
                id: 'colNamaObat',
                header: "Nama Obat",
                dataIndex: 'NAMA_OBAT',
                width: 200
            },
            { 
                id: 'colHarga',
                header: "Qty",
                dataIndex: 'STOK',
                width: 70,
                align:'right',
				readOnly: true,
				renderer: function(v, params, record) 
				{
					return formatCurrency(record.data.STOK);
				},
            }       
        ]
    )
};
// -- End function look up Stok Opname Obat --

// -- Function Look Up Rpt Apotek --

function FormLookupObatGrid_Rpt(criteria, ex_kd_obat, ex_nama_obat) {

    var vWinFormEntryObat_Rpt = new Ext.Window
    (
        {
            id: 'FormGrdPgwLookup_Rpt',
            title: 'Lookup Obat',
            closable: true,
            width: 400,
            height: 470,
            border: true,
            plain: true,
            resizable: false,
            layout: 'form',
            iconCls: 'find',
            modal: true,
            items:
            [
                fnGetDTLGridLookUpFEObat_Rpt(criteria, ex_kd_obat, ex_nama_obat, vWinFormEntryObat_Rpt),
                {
                    xtype: 'button',
                    text: 'Ok',
                    width: 70,
                    style: { 'margin-left': '5px', 'margin-top': '7px' },
                    hideLabel: true,
                    id: 'btnOkAcc',
                    handler: function() {
                    Ext.get(ex_kd_obat).dom.value = rowSelectedLookObat_Rpt.data.KD_OBAT_VIEW;
                        Ext.get(ex_nama_obat).dom.value = rowSelectedLookObat_Rpt.data.NAMA_OBAT;

                        rowSelectedLookObat_Rpt = '';
                        vWinFormEntryObat_Rpt.close();
                    }
                }
            ],
            listeners:
                {
                    activate: function()
                    { }
                }
        }
    );
    vWinFormEntryObat_Rpt.show();
    mWindowGridLookupObat_Rpt = vWinFormEntryObat_Rpt;
};

///---------------------------------------------------------------------------------------///

function fnGetDTLGridLookUpFEObat_Rpt(criteria, ex_kd_obat, ex_nama_obat, vWinFormEntryObat_Rpt) {

    var fldDetailObat_Rpt =
    [
        'KD_OBAT',
        'KD_OBAT_VIEW',
        'NAMA_OBAT',
    ];

    dsLookupListObat_Rpt = new WebApp.DataStore({ fields: fldDetailObat_Rpt });
    var vGridLookAccFormEntryObat_Rpt = new Ext.grid.EditorGridPanel
    (
        {
            id: 'vGridLookAccFormEntryObat_Rpt',
            title: '',
            stripeRows: true,
            store: dsLookupListObat_Rpt,
            height: 400,
            columnLines: true,
            bodyStyle: 'padding:0px',
            border: false,
            sm: new Ext.grid.RowSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        rowselect: function(sm, row, rec) {
                            rowSelectedLookObat_Rpt = dsLookupListObat_Rpt.getAt(row);
                            CurrentDataObat_Rpt.row = row;
                            CurrentDataObat_Rpt.data = rowSelectedLookObat_Rpt;
                        }
                    }
                }
            ),
            listeners:
            {
                rowdblclick: function(sm, row, rec) {
                    rowSelectedLookObat_Rpt = dsLookupListObat_Rpt.getAt(row);
                    CurrentDataObat_Rpt.row = rec;
                    CurrentDataObat_Rpt.data = rowSelectedLookObat_Rpt;

                    Ext.get(ex_kd_obat).dom.value = rowSelectedLookObat_Rpt.data.KD_OBAT_VIEW;
                    Ext.get(ex_nama_obat).dom.value = rowSelectedLookObat_Rpt.data.NAMA_OBAT;

                    rowSelectedLookObat_Rpt = '';
                    mWindowGridLookupObat_Rpt.close();
                }
            },
            cm: fnGridLookAccColumnModelObat_Rpt(),
            viewConfig: { forceFit: true }
        });

    dsLookupListObat_Rpt.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 100,
                Sort: 'KD_OBAT',
                Sortdir: 'ASC',
                target: 'viObat',
                param: criteria
            }
        }
    );
    return vGridLookAccFormEntryObat_Rpt;
};



function fnGridLookAccColumnModelObat_Rpt() {
    return new Ext.grid.ColumnModel
    (
        [
            new Ext.grid.RowNumberer(),
            {
                id: 'colKDObat',
                header: "Kd. Obat",
                dataIndex: 'KD_OBAT_VIEW',
                width: 70
            },
            {
                id: 'colNamaObat',
                header: "Nama Obat",
                dataIndex: 'NAMA_OBAT',
                width: 70
            }
        ]
    )
};

// End Function Look Up Rpt Apotek --