/// <reference path="../../ext-base.js" />
/// <reference path="../../ext-all.js" />

var rowSelectedLookPart;
var vWinFormEntryLookupPart;
var dsLookPartList;
var varBtnOkLookupPart=false;



function FormLookupPart(criteria,dsStore,p,mBolAddNew,idx,mBolLookup) 
{
	rowSelectedLookPart=undefined;
    vWinFormEntryLookupPart = new Ext.Window
	(
		{
			id: 'vWinFormEntryLookupPart',
			title: nmTitleFormLookupPart,
			closeAction: 'hide',
			closable:false,
			width: 550,
			height: 420,
			border: false,
			plain: true,
			resizable:false,
			layout: 'form',
			iconCls: 'find',
			modal: true,                                   
			items: 
			[
				GetPanelLookUpPart(criteria,dsStore,p,mBolAddNew,idx,mBolLookup) 
			],
			listeners:
				{ 
					activate: function()
					{ } 
				}
		}
	);

    vWinFormEntryLookupPart.show();
};

function GetPanelLookUpPart(criteria,dsStore,p,mBolAddNew,idx,mBolLookup) 
{
	
	var FormLookUpPart = new Ext.Panel  
	(
		{
			id: 'FormLookUpPart',
			region: 'center',
			layout: 'form',
			title: '',
			bodyStyle: 'padding:15px',
			anchor:'100%',
			border: false,
			bodyStyle: 'background:#FFFFFF;',
			height: 392,
			shadhow: true,
			items: 
			[
				fnGetDTLGridLookUpPart(criteria),
				getItemPanelLookupPart(dsStore,p,mBolAddNew,idx,mBolLookup)
			]
        }
	);
	
	return FormLookUpPart;
};


function getItemPanelLookupPart(dsStore,p,mBolAddNew,idx,mBolLookup) 
{
    var items =
	{
	    layout: 'column',
	    border: true,
		height:40,
		anchor:'100%',
		style:{'margin-top':'-1px'},
	    items:
		[
			{
				layout: 'hBox',
				width:522,
				border: false,
				bodyStyle: 'padding:5px 0px 5px 5px',
				defaults: { margins: '3 3 3 3' },
				anchor: '90%',
				layoutConfig: 
				{
					align: 'middle',
					pack:'end'
				},
				items:
				[
					{
						xtype:'button',
						text:nmBtnOK,
						width:70,
						style:{'margin-left':'0px','margin-top':'0px'},
						hideLabel:true,
						id: 'btnOkLookupPart',
						handler:function()
						{
							if (p != undefined && rowSelectedLookPart != undefined)
							{
								if (mBolAddNew === true || mBolAddNew === undefined)
									{
										p.data.PART_ID=rowSelectedLookPart.data.PART_ID;
										p.data.PART_NAME=rowSelectedLookPart.data.PART_NAME;
										
										if (mBolLookup === true)
										{
										   dsStore.insert(dsStore.getCount(), p);
										}
										else
										{
										   dsStore.removeAt(dsStore.getCount()-1);
										   dsStore.insert(dsStore.getCount(), p);
										};
										
									}
									else
									{
										if (dsStore != undefined)
										{
											p.data.PART_ID=rowSelectedLookPart.data.PART_ID;
											p.data.PART_NAME=rowSelectedLookPart.data.PART_NAME;
											
											dsStore.removeAt(idx);
											dsStore.insert(idx, p);
										};
									};
									varBtnOkLookupPart=true;
								vWinFormEntryLookupPart.close();
							}
							else
							{
								ShowPesanWarningLookupPart(nmGetValidasiSelect(nmPartLookupPart),nmTitleFormLookupPart);
							};
							
						}
					},
					{
							xtype:'button',
							text:nmBtnCancel ,
							width:70,
							hideLabel:true,
							id: 'btnCancelLookupPart',
							handler:function() 
							{
								vWinFormEntryLookupPart.close();
							}
					}
				]
			}
		]
	}
    return items;
};

function ShowPesanWarningLookupPart(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function fnGetDTLGridLookUpPart(criteria) 
{

    var fldDetail = ['PART_ID','PART_NAME','PRICE','DESC_PART','CATEGORY_ID'];
	dsLookPartList = new WebApp.DataStore({ fields: fldDetail });

	var vGridLookPartFormEntry = new Ext.grid.EditorGridPanel
	(
		{ 
			id:'vGridLookPartFormEntry',
			title: '',
			stripeRows: true,
			store: dsLookPartList,
			height:353,
			columnLines:true,
			bodyStyle: 'padding:0px',
			border: true,	
			frame:false,
			sm: new Ext.grid.RowSelectionModel
			(
				{ 
					singleSelect: true,
					listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedLookPart = dsLookPartList.getAt(row);
						}
					}
				}
			),
			cm: fnGridLookPartColumnModel(),
			viewConfig: { forceFit: true }
		}
	);
	

	dsLookPartList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				//Sort: 'PART_ID',
                                Sort: 'part_id',
				Sortdir: 'ASC',
				target: 'LookupPartCategory',
				param: criteria
			}
		}
	);
	
	return vGridLookPartFormEntry;
};



function fnGridLookPartColumnModel() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{ 
				id: 'colKdPartLook',
				header: nmNumberLookupPart,
				dataIndex: 'PART_ID',
				width: 70
			},
			{ 
				id: 'colPartNameLook',
				header: nmNameLookupPart,
				dataIndex: 'PART_NAME',
				width: 200
            },
			{ 
				id: 'colPartPriceLook',
				header: nmPriceLookupPart,
				dataIndex: 'PRICE',
				width: 100,
				renderer: function(v, params, record) 
				{
					return formatCurrency(record.data.PRICE);
				}	
            },
			{ 
				id: 'colPartDescLook',
				header: nmDescLookupPart,
				dataIndex: 'DESC_PART',
				width: 100
            }
		]
	)
};

