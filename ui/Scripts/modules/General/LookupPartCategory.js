/// <reference path="../../ext-base.js" />
/// <reference path="../../ext-all.js" />

var rowSelectedLookPartCategory;
var vWinFormEntryLookupPartCategory;
var chkSparepartsLookupPartCategory;
var dsLookPartCategoryList;
var mCriteria;

function FormLookupPartCategory(criteria,p,dsStore) 
{
	rowSelectedLookPartCategory = undefined;
    vWinFormEntryLookupPartCategory = new Ext.Window
	(
		{
			id: 'vWinFormEntryLookupPartCategory',
			title: nmTitleFormLookupPart,
			closeAction: 'hide',
			closable:false,
			width: 450,
			height: 447,
			border: false,
			plain: true,
			resizable:false,
			layout: 'form',
			iconCls: 'find',
			modal: true,                                   
			items: 
			[
				GetPanelLookUpPartCategory(criteria,p,dsStore),
				getItemPanelLookupPartCategory(p,dsStore)
			]
			
		}
	);
		mCriteria = criteria;		
    vWinFormEntryLookupPartCategory.show();
};

function GetPanelLookUpPartCategory(criteria,p,dsStore)
{
	
	var FormLookUpPartCategory = new Ext.Panel  
	(
		{
			id: 'FormLookUpPartCategory',
			region: 'center',
			layout: 'form',
			title: '',
			bodyStyle: 'padding:15px',
			anchor:'100%',
			border: true,
			bodyStyle: 'background:#FFFFFF;',
			height: 360,
			shadhow: true,
			items: 
			[
				fnGetDTLGridLookUpPartCategory(criteria)
			],
			tbar: 
			[							
				' ', nmPartNameLookupPart + ' : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: nmPartNameLookupPart + ' ',
					id: 'txtPartNameFilter',                   
					width:150,
					listeners:
					{ 
						'specialkey': function()
						{ 
							if (Ext.EventObject.getKey() === 13) 
							{
								getDataPartCat();
							};
						} 
					}
				}		
			]
        }
	);
	
	return FormLookUpPartCategory;
};

function getItemPanelLookupPartCategory(p,dsStore) 
{
    var items =
	{
	    layout: 'column',
	    border: true,
		height:40,
		anchor:'100%',
		style:{'margin-top':'-1px'},
	    items:
		[
			{
				layout: 'hBox',
				width:422,
				border: false,
				bodyStyle: 'padding:5px 0px 5px 5px',
				defaults: { margins: '3 3 3 3' },
				anchor: '90%',
				layoutConfig: 
				{
					align: 'middle',
					pack:'end'
				},
				items:
				[
					{
						xtype:'button',
						text:nmBtnAdd,
						width:70,
						style:{'margin-left':'0px','margin-top':'0px'},
						hideLabel:true,
						id: 'btnAddLookupPartCategory',
						handler:function()
						{
						    var mBol = false;
							
							for(var i=0;i < dsLookPartCategoryList.getCount();i++)
							{
								if (dsLookPartCategoryList.data.items[i].data.SELECT === true)
								{
									p=new mRecordPartCategory;
									p.data.PART_ID=dsLookPartCategoryList.data.items[i].data.PART_ID;
									p.data.PART_NAME=dsLookPartCategoryList.data.items[i].data.PART_NAME;									
									p.data.TAG='1';
									dsStore.insert(dsStore.getCount(), p);
									mBol=true;
									var cKri;
									if (mCriteria==='')
									{
										//cKri=' WHERE PART_ID not In (\''+ p.data.PART_ID +'\')'
                                                                                cKri=' part_id not In (\''+ p.data.PART_ID +'\')'
									}
									else
									{
										//cKri=' AND PART_ID not In (\''+ p.data.PART_ID +'\')'
                                                                                cKri=' AND part_id not In (\''+ p.data.PART_ID +'\')'
									};
									mCriteria += cKri;
									RefreshDataPartCatSetCat(mCriteria);
								};
							}
							
							if (mBol === false)
							{
								ShowPesanWarningLookupPartCategory(nmGetValidasiSelect(nmPartLookupPart),nmTitleFormLookupPart);
							};
							
						}
					},
					{
						xtype:'button',
						text:nmBtnOK,
						width:70,
						style:{'margin-left':'0px','margin-top':'0px'},
						hideLabel:true,
						id: 'btnOkLookupPartCategory',
						handler:function()
						{
						    var mBol = false;
							
							for(var i=0;i < dsLookPartCategoryList.getCount();i++)
							{
								if (dsLookPartCategoryList.data.items[i].data.SELECT === true)
								{
									p=new mRecordPartCategory;
									p.data.PART_ID=dsLookPartCategoryList.data.items[i].data.PART_ID;
									p.data.PART_NAME=dsLookPartCategoryList.data.items[i].data.PART_NAME;		
									p.data.TAG='1';									
									dsStore.insert(dsStore.getCount(), p);
									mBol=true;
								};
							}
							
							if (mBol === false)
							{
								ShowPesanWarningLookupPartCategory(nmGetValidasiSelect(nmPartLookupPart),nmTitleFormLookupPart);
							}
							else
							{
								vWinFormEntryLookupPartCategory.close();
							};
						}
					},
					{
						xtype:'button',
						text:nmBtnCancel ,
						width:70,
						hideLabel:true,
						id: 'btnCancelLookupPartCategory',
						handler:function() 
						{
							vWinFormEntryLookupPartCategory.close();
						}
					}
				]
			}
		]
	}
    return items;
};

function ShowPesanWarningLookupPartCategory(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING
		}
	);
};

function fnGetDTLGridLookUpPartCategory(criteria) 
{  
	
	var fldDetail = ['PART_ID','PART_NAME','SELECT'];
	dsLookPartCategoryList = new WebApp.DataStore({ fields: fldDetail });
	
	chkSparepartsLookupPartCategory = new Ext.grid.CheckColumn
	(
		{
			id: 'chkSparepartsLookupPartCategory',
			header: nmSelectLookupPart,
			align: 'center',
			disabled:true,
			dataIndex: 'SELECT',
			width: 40
		}
	);

	var vGridLookPartCategoryFormEntry = new Ext.grid.EditorGridPanel
	(
		{ 
			id:'vGridLookPartCategoryFormEntry',
			title: '',
			stripeRows: true,
			store: dsLookPartCategoryList,
			height:353,
			columnLines:true,
			bodyStyle: 'padding:0px',
			border: false,	
			frame:false,
			sm: new Ext.grid.CellSelectionModel
			(
				{ 
					singleSelect: true,
					listeners:
					{ 
						cellselect: function(sm, row, rec) 
						{
							rowSelectedLookPartCategory = dsLookPartCategoryList.getAt(row);
						}
					}
				}
			),
			cm: fnGridLookPartCategoryColumnModel(),
			plugins:chkSparepartsLookupPartCategory,
			viewConfig: { forceFit: true }
		}
	);


	RefreshDataPartCatSetCat(criteria);
	
	
	return vGridLookPartCategoryFormEntry;
};

function RefreshDataPartCatSetCat(criteria)
{
	dsLookPartCategoryList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				//Sort: 'PART_NAME',
                                Sort: 'part_name',
				Sortdir: 'ASC', 
				target:'LookupPart',
				param: criteria
			}
		}
	);
	return dsLookPartCategoryList;
};

function fnGridLookPartCategoryColumnModel() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),chkSparepartsLookupPartCategory,
			{ 
				id: 'colKdPartCategoryLook',
				header: nmNumberLookupPart,
				dataIndex: 'PART_ID',
				width: 60
			},
			{ 
				id: 'colPartCategoryNameLook',
				header: nmNameLookupPart,
				dataIndex: 'PART_NAME',
				width: 180
            }
		]
	)
};

function getDataPartCat()
{
	var cKtiteria;
	cKtiteria=mCriteria;
	//cKtiteria += ' AND PART_NAME LIKE ~' + Ext.get('txtPartNameFilter').getValue() + '%~';
        cKtiteria += ' AND part_name LIKE ~' + Ext.get('txtPartNameFilter').getValue() + '%~';
	
	RefreshDataPartCatSetCat(cKtiteria);
};
///---------------------------------------------------------------------------------------///