/// <reference path="../../ext-base.js" />
/// <reference path="../../ext-all.js" />

var rowSelectedLookPartService;
var vWinFormEntryLookupPartService;
var dsLookPartServiceList;
var varBtnOkLookupPartService=false;


function FormLookupPartService(criteria,dsStore,p,mBolAddNew,idx,mBolLookup) 
{
	rowSelectedLookPartService=undefined;
    vWinFormEntryLookupPartService = new Ext.Window
	(
		{
			id: 'vWinFormEntryLookupPartService',
			title: nmTitleFormLookupPart,
			closeAction: 'hide',
			closable:false,
			width: 550,
			height: 420,
			border: false,
			plain: true,
			resizable:false,
			layout: 'form',
			iconCls: 'find',
			modal: true,                                   
			items: 
			[
				GetPanelLookUpPartService(criteria,dsStore,p,mBolAddNew,idx,mBolLookup) 
			],
			listeners:
				{ 
					activate: function()
					{ } 
				}
		}
	);

    vWinFormEntryLookupPartService.show();
};

function GetPanelLookUpPartService(criteria,dsStore,p,mBolAddNew,idx,mBolLookup) 
{
	
	var FormLookUpPartService = new Ext.Panel  
	(
		{
			id: 'FormLookUpPartService',
			region: 'center',
			layout: 'form',
			title: '',
			bodyStyle: 'padding:15px',
			anchor:'100%',
			border: false,
			bodyStyle: 'background:#FFFFFF;',
			height: 392,
			shadhow: true,
			items: 
			[
				fnGetDTLGridLookUpPartService(criteria),
				getItemPanelLookupPartService(dsStore,p,mBolAddNew,idx,mBolLookup)
			]
        }
	);
	
	return FormLookUpPartService;
};


function getItemPanelLookupPartService(dsStore,p,mBolAddNew,idx,mBolLookup) 
{
    var items =
	{
	    layout: 'column',
	    border: true,
		height:40,
		anchor:'100%',
		style:{'margin-top':'-1px'},
	    items:
		[
			{
				layout: 'hBox',
				width:522,
				border: false,
				bodyStyle: 'padding:5px 0px 5px 5px',
				defaults: { margins: '3 3 3 3' },
				anchor: '90%',
				layoutConfig: 
				{
					align: 'middle',
					pack:'end'
				},
				items:
				[
					{
						xtype:'button',
						text:nmBtnOK,
						width:70,
						style:{'margin-left':'0px','margin-top':'0px'},
						hideLabel:true,
						id: 'btnOkLookupPartService',
						handler:function()
						{
							if (p != undefined && rowSelectedLookPartService != undefined)
							{
								if (mBolAddNew === true || mBolAddNew === undefined)
									{
										p.data.PART_ID=rowSelectedLookPartService.data.PART_ID;
										p.data.PART_NAME=rowSelectedLookPartService.data.PART_NAME;
										p.data.QTY=rowSelectedLookPartService.data.QTY;
										p.data.UNIT_COST=rowSelectedLookPartService.data.UNIT_COST;
										p.data.TOT_COST=rowSelectedLookPartService.data.TOT_COST;
										
										if (mBolLookup === true)
										{
										   dsStore.insert(dsStore.getCount(), p);
										}
										else
										{
										   dsStore.removeAt(dsStore.getCount()-1);
										   dsStore.insert(dsStore.getCount(), p);
										}
										
									}
									else
									{
										if (dsStore != undefined)
										{
											p.data.PART_ID=rowSelectedLookPartService.data.PART_ID;
											p.data.PART_NAME=rowSelectedLookPartService.data.PART_NAME;
											p.data.QTY=rowSelectedLookPartService.data.QTY;
											p.data.UNIT_COST=rowSelectedLookPartService.data.UNIT_COST;
											p.data.TOT_COST=rowSelectedLookPartService.data.TOT_COST;
											
											dsStore.removeAt(idx);
											dsStore.insert(idx, p);
										}
									}
									varBtnOkLookupPartService=true;
									vWinFormEntryLookupPartService.close();
							}
							else
							{
								ShowPesanWarningLookupPartService(nmGetValidasiSelect(nmPartLookupPart),nmTitleFormLookupPart);
							};
							
						}
					},
					{
							xtype:'button',
							text:nmBtnCancel ,
							width:70,
							hideLabel:true,
							id: 'btnCancelLookupPartService',
							handler:function() 
							{
								vWinFormEntryLookupPartService.close();
							}
					}
				]
			}
		]
	}
    return items;
};

function ShowPesanWarningLookupPartService(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function fnGetDTLGridLookUpPartService(criteria) 
{

    var fldDetail = ['SERVICE_ID','CATEGORY_ID','PART_ID','QTY','UNIT_COST','TOT_COST','INSTRUCTION','PART_NAME','PRICE','DESC_PART'];
	dsLookPartServiceList = new WebApp.DataStore({ fields: fldDetail });

	var vGridLookPartServiceFormEntry = new Ext.grid.EditorGridPanel
	(
		{ 
			id:'vGridLookPartServiceFormEntry',
			title: '',
			stripeRows: true,
			store: dsLookPartServiceList,
			height:353,
			columnLines:true,
			bodyStyle: 'padding:0px',
			border: true,	
			frame:false,
			sm: new Ext.grid.RowSelectionModel
			(
				{ 
					singleSelect: true,
					listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedLookPartService = dsLookPartServiceList.getAt(row);
						}
					}
				}
			),
			cm: fnGridLookPartServiceColumnModel(),
			viewConfig: { forceFit: true }
		}
	);
	

	dsLookPartServiceList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				//Sort: 'PART_ID',
                                Sort: 'part_id',
				Sortdir: 'ASC',
				target: 'LookupPartService',
				param: criteria
			}
		}
	);
	
	return vGridLookPartServiceFormEntry;
};



function fnGridLookPartServiceColumnModel() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{ 
				id: 'colKdPartServiceLook',
				header: nmNumberLookupPart,
				dataIndex: 'PART_ID',
				width: 70
			},
			{ 
				id: 'colPartServiceNameLook',
				header: nmNameLookupPart,
				dataIndex: 'PART_NAME',
				width: 200
            },
			{ 
				id: 'colPartServicePriceLook',
				header: nmPriceLookupPart,
				dataIndex: 'UNIT_COST',
				width: 100,
				renderer: function(v, params, record) 
				{
					return formatCurrency(record.data.UNIT_COST);
				}	
            },
			{ 
				id: 'colPartServiceDescLook',
				header: nmDescLookupPart,
				dataIndex: 'INSTRUCTION',
				width: 100
            }
		]
	)
};

