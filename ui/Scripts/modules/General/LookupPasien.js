/// <reference path="../../ext-base.js" />
/// <referece path="../../ext-all.js" />

var rowSelectedLookPasien;
var rowSelectedLookSL;
var mWindowLookup;
var nFormPendaftaran=1; 
var dsLookupPasienList;

var criteria ='';

function FormLookupPasien(criteria,nFormAsal,nName_ID,strRM,strNama,strAlamat) 
{
    var vWinFormEntry = new Ext.Window
	(
		{
		    id: 'FormPasienLookup',
		    title: 'Lookup Pasien',		    
		    closable: true,
		    width: 600,//450,//
		    height: 400,
		    border: true,
		    plain: true,
		    resizable: false,
		    layout: 'form',
		    iconCls: 'find',
		    modal: true,
		    items:
			[
			    // fnGetDTLGridLookUp(criteria, nFormAsal),
				fnGetDTLGridLookUpPas(criteria, nFormAsal,nName_ID),
				{
				    xtype: 'button',
				    text: 'Ok',
				    width: 70,
				    style: { 'margin-left': '510px', 'margin-top': '7px' },
				    hideLabel: true,
				    id: 'btnOkpgw',
				    handler: function() 
					{
						GetPasien(nFormAsal,nName_ID);
				    }
				}
			],
			tbar:
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'tbtext',
						text: 'No. RM : '
					},
					{
						xtype: 'textfield',
						id: 'txtNoRMLookupPasien',
						listeners:
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									criteria = getQueryCariPasien();
									RefreshDataLookupPasien(criteria);						
								} 						
							}
						}
						
					},				
					{
						xtype: 'tbtext',
						text: 'Nama : '
					},
					{
						xtype: 'textfield',
						id: 'txtNamaMLookupPasien',
						listeners:
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									criteria = getQueryCariPasien();
									RefreshDataLookupPasien(criteria);						
								} 						
							}
						}						
					},
					{
					xtype: 'tbtext',
					text: 'Alamat : '
					},
					{
						xtype: 'textfield',
						id: 'txtAlamatMLookupPasien',
						listeners:
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									criteria = getQueryCariPasien();
									RefreshDataLookupPasien(criteria);						
								} 						
							}
						}						
					},
					{
						xtype: 'tbfill'
					},
					{
						xtype: 'button',
						id: 'btnRefreshGLLookupCalonMHS',
						iconCls: 'refresh',
						handler: function()
						{	
							// var criteria ='';
							criteria = getQueryCariPasien();
							RefreshDataLookupPasien(criteria);
						}
					}
				]
			}
			,
		    listeners:
				{
				    activate: function()
				    { 
						Ext.get('txtNoRMLookupPasien').dom.value = strRM;
						Ext.get('txtNamaMLookupPasien').dom.value = strNama;
						Ext.get('txtAlamatMLookupPasien').dom.value = strAlamat;
						
					}
				}
		}
	);
    vWinFormEntry.show();		
	mWindowLookup = vWinFormEntry;
};

///---------------------------------------------------------------------------------------///


function fnGetDTLGridLookUpPas(criteria,nFormAsal,nName_ID) 
{
    var fldDetail = 
	['KD_CUSTOMER','KD_PASIEN', 'NO_RM', 'KD_PENDIDIKAN', 'KD_STS_MARITAL', 'KD_AGAMA', 'KD_PEKERJAAN', 'NAMA', 'TEMPAT_LAHIR', 'TGL_LAHIR', 'NO_TELP', 'NO_HP','ALAMAT', 'PEKERJAAN', 
	'STS_MARITAL', 'AGAMA', 'PENDIDIKAN', 'ID_JENIS_KELAMIN', 'JENIS_KELAMIN', 'GOL_DARAH', 'TAHUN', 'BULAN', 'HARI','KD_UNIT','NAMA_UNIT','KD_DOKTER','DOKTER','KD_KELOMPOK','KELOMPOK',
	'NADI', 'ALERGI','TINGGI_BADAN','BERAT_BADAN','TEKANAN_DARAH','KELUHAN','ID_GOL_DARAH'
	]; 
                      
	// var dsLookupPasienList = new WebApp.DataStore({ fields: fldDetail });
	dsLookupPasienList = new WebApp.DataStore({ fields: fldDetail });
	
	RefreshDataLookupPasien(criteria);
	var vGridLookupPasienFormEntry = new Ext.grid.EditorGridPanel
	(
		{ 
			id:'vGridLookupPasienFormEntry',
			title: '',
			stripeRows: true,
			store: dsLookupPasienList,
			height: 310, //330,
			columnLines:true,
			bodyStyle: 'padding:0px',
			enableKeyEvents: true,
			border: false,		
			sm: new Ext.grid.RowSelectionModel
			(
				{ 
					singleSelect: true,
					listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedLookPasien = dsLookupPasienList.getAt(row);
						}										
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, ridx, cidx)
				{
					GetPasien(nFormAsal,nName_ID);
				},
				
				'specialkey': function() 
				{					
					if (Ext.EventObject.getKey() == 13) 
					{
						GetPasien(nFormAsal,nName_ID);
					}	
				}											
			},			
			cm: fnGridLookPasienColumnModel(),
			viewConfig: { forceFit: true }
		});
		
		// dsLookupPasienList.load
		// (
			// { 
				// params: 
				// { 
					// Skip: 0, 
					// Take: 100, 					
					// Sortdir: 'ASC',					
					// target: 'Viview_viDataPasien',
					// param: criteria				
				// } 
			// }
		// );	
	return vGridLookupPasienFormEntry;
};

function fnGridLookPasienColumnModel() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{ 
				id: 'colLookupPasien',
				header: "No. RM",
				dataIndex: 'NO_RM',
				width: 200
			},			
			{ 
				id: 'colLookupNamaPasien',
				header: "Nama",
				dataIndex: 'NAMA',
				width: 300
			},
			{ 
				id: 'colLookupNoTelpPasien',
				header: "Telp 1",
				dataIndex: 'NO_TELP',
				width: 170
			},
			{ 
				id: 'colLookupNoHPPasien',
				header: "Telp 2",
				dataIndex: 'NO_HP',
				width: 150
			},
			{ 
				id: 'colLookupAlamatPasien',
				header: "Alamat",
				dataIndex: 'ALAMAT',
				width: 300
			}
			
		]
	)
};
function GetPasien(nFormAsal,nName_ID)
{
	if (rowSelectedLookPasien != undefined || nName_ID != undefined) 
	{			          
		if (nFormAsal === nFormPendaftaran) 
		{				              

			Ext.getCmp('ComboPoliDaftar_viDaftar').setValue(rowSelectedLookPasien.data.KD_UNIT);
			Ext.getCmp('ComboDokterDaftar_viDaftar').setValue(rowSelectedLookPasien.data.KD_DOKTER);
			Ext.getCmp('ComboKelompokDaftar_viDaftar').setValue(rowSelectedLookPasien.data.KD_KELOMPOK);
			Ext.getCmp('ComboGolDRH_viDaftar').setValue(rowSelectedLookPasien.data.ID_GOL_DARAH);
			Ext.getCmp('ComboJK_viDaftar').setValue(rowSelectedLookPasien.data.ID_JENIS_KELAMIN);
			
			Ext.getCmp('ComboSTS_MARITAL_viDaftar').setValue(rowSelectedLookPasien.data.KD_STS_MARITAL);
			Ext.getCmp('ComboPekerjaan_viDaftar').setValue(rowSelectedLookPasien.data.KD_PEKERJAAN);
			Ext.getCmp('ComboAgama_viDaftar').setValue(rowSelectedLookPasien.data.KD_AGAMA);
			Ext.getCmp('ComboPendidikan_viDaftar').setValue(rowSelectedLookPasien.data.KD_PENDIDIKAN);
			
			Ext.get('txtKDPasien_viDaftar').dom.value = rowSelectedLookPasien.data.NO_RM;
			Ext.get('txtNamaPs_viDaftar').dom.value = rowSelectedLookPasien.data.NAMA;
			Ext.get('txtchkPasienBaru_viDaftar').dom.checked = 0;
			Ext.get('txtTmpLahir_viDaftar').dom.value = rowSelectedLookPasien.data.TEMPAT_LAHIR;
			Ext.get('DtpTglLahir_viDaftar').dom.value = ShowDate(rowSelectedLookPasien.data.TGL_LAHIR);
			Ext.get('txtUmurThn_viDaftar').dom.value = rowSelectedLookPasien.data.TAHUN;
			Ext.get('txtUmurBln_viDaftar').dom.value = rowSelectedLookPasien.data.BULAN;
			Ext.get('txtUmurHari_viDaftar').dom.value = rowSelectedLookPasien.data.HARI;
			Ext.get('txtAlamat_viDaftar').dom.value = rowSelectedLookPasien.data.ALAMAT;						
			
			Ext.get('ComboPoliDaftar_viDaftar').dom.value = rowSelectedLookPasien.data.NAMA_UNIT;
			Ext.get('ComboDokterDaftar_viDaftar').dom.value = rowSelectedLookPasien.data.DOKTER;
			
			
			Ext.get('txtNadi_viDaftar').dom.value = rowSelectedLookPasien.data.NADI;
			Ext.get('txtchkAlergi_viDaftar').dom.value = rowSelectedLookPasien.data.ALERGI;
			Ext.get('txtTinggiBadan_viDaftar').dom.value = rowSelectedLookPasien.data.TINGGI_BADAN;
			Ext.get('txtBeratBadan_viDaftar').dom.value = rowSelectedLookPasien.data.BERAT_BADAN;
			Ext.get('txtTekananDarah_viDaftar').dom.value = rowSelectedLookPasien.data.TEKANAN_DARAH;
			Ext.get('txtkeluhan_viDaftar').dom.value = rowSelectedLookPasien.data.KELUHAN;
			
			Ext.get('ComboJK_viDaftar').dom.value = rowSelectedLookPasien.data.JENIS_KELAMIN;
			Ext.get('ComboGolDRH_viDaftar').dom.value = rowSelectedLookPasien.data.GOL_DARAH;
			Ext.get('txtNoTlp_viDaftar').dom.value = rowSelectedLookPasien.data.NO_TELP;
			Ext.get('txtHP_viDaftar').dom.value = rowSelectedLookPasien.data.NO_HP;
			Ext.get('ComboSTS_MARITAL_viDaftar').dom.value = rowSelectedLookPasien.data.STS_MARITAL;
			Ext.get('ComboPekerjaan_viDaftar').dom.value = rowSelectedLookPasien.data.PEKERJAAN;
			Ext.get('ComboAgama_viDaftar').dom.value = rowSelectedLookPasien.data.AGAMA;
			Ext.get('ComboPendidikan_viDaftar').dom.value = rowSelectedLookPasien.data.PENDIDIKAN;
			Ext.getCmp('btnSimpan_viDaftar').enable();			
			
			Ext.get('ComboKelompokDaftar_viDaftar').dom.value = rowSelectedLookPasien.data.KELOMPOK;
			
		}					
		
	}
	rowSelectedLookPasien=undefined;
	mWindowLookup.close();
}

// function getQueryCariPasien()
// {
	// var strKriteria = "";		
	
	// if(Ext.get('txtNoRMLookupPasien').getValue() != '')
	// {
		// strKriteria += " WHERE RIGHT(PS.KD_PASIEN,10) like '%" + Ext.get('txtNoRMLookupPasien').dom.value + "%'"
	// }	
	
	// if(Ext.get('txtNamaMLookupPasien').getValue() != '')	
	// {	
		// if (strKriteria != "")	
		// {
			// strKriteria += " AND PS.NAMA like '%" + Ext.get('txtNamaMLookupPasien').dom.value + "%'"
		// }
		// else
		// {
			// strKriteria += " WHERE PS.NAMA like '%" + Ext.get('txtNamaMLookupPasien').dom.value + "%'"
		// }
	// }
	
	// if(Ext.get('txtAlamatMLookupPasien').getValue() != '')	
	// {	
		// if (strKriteria != "")	
		// {
			// strKriteria += " AND PS.ALAMAT like '%" + Ext.get('txtAlamatMLookupPasien').dom.value + "%'"
		// }
		// else
		// {
			// strKriteria += " WHERE PS.ALAMAT like '%" + Ext.get('txtAlamatMLookupPasien').dom.value + "%'"
		// }
	// }

	// if (strKriteria != "")	
	// {
		// strKriteria += " AND PS.KD_CUSTOMER = '" + strKD_CUST + "'"
	// }
	// else
	// {
		// strKriteria += " WHERE PS.KD_CUSTOMER = '" + strKD_CUST + "'"
	// }
		
	// // dsLookupPasienList.load
	// // (
		// // { 
			// // params: 
			// // { 
				// // Skip: 0, 
				// // Take: 100, 					
				// // Sortdir: 'ASC',					
				// // target: 'Viview_viDataPasien',
				// // param: strKriteria				
			// // } 
		// // }
	// // );	
	// // return vGridLookupPasienFormEntry;
	
	// return strKriteria;
// }
///---------------------------------------------------------------------------------------///

function getQueryCariPasien()
{
	var strKriteria = "";		
	
	if(Ext.get('txtNoRMLookupPasien').getValue() != '')
	{
		strKriteria += " WHERE RIGHT(x.KD_PASIEN,10) like '%" + Ext.get('txtNoRMLookupPasien').dom.value + "%'"
	}	
	
	if(Ext.get('txtNamaMLookupPasien').getValue() != '')	
	{	
		if (strKriteria != "")	
		{
			strKriteria += " AND x.NAMA like '%" + Ext.get('txtNamaMLookupPasien').dom.value + "%'"
		}
		else
		{
			strKriteria += " WHERE x.NAMA like '%" + Ext.get('txtNamaMLookupPasien').dom.value + "%'"
		}
	}
	
	if(Ext.get('txtAlamatMLookupPasien').getValue() != '')	
	{	
		if (strKriteria != "")	
		{
			strKriteria += " AND x.ALAMAT like '%" + Ext.get('txtAlamatMLookupPasien').dom.value + "%'"
		}
		else
		{
			strKriteria += " WHERE x.ALAMAT like '%" + Ext.get('txtAlamatMLookupPasien').dom.value + "%'"
		}
	}

	if (strKriteria != "")	
	{
		strKriteria += " AND x.KD_CUSTOMER = '" + strKD_CUST + "'"
	}
	else
	{
		strKriteria += " WHERE x.KD_CUSTOMER = '" + strKD_CUST + "'"
	}				
	
	return strKriteria;
}

function RefreshDataLookupPasien(criteria)
{	
	dsLookupPasienList.load
		(
			{ 
				params: 
				{ 
					Skip: 0, 
					Take: 100, 					
					Sortdir: 'ASC',					
					target: 'viKunjungan',//'Viview_viDataPasien',
					param: criteria				
				} 
			}
		);	
	
	return dsLookupPasienList;
}