/// <reference path="../../ext-base.js" />
/// <reference path="../../ext-all.js" />

//var rowSelectedLookProduk;
var vWinFormEntryLookupProduk;
var kdsetting;

var dsLookProdukList;
var IdKlas_produkegorySetEquipmentKlas_produkView='0';
var str;
var kdperent;
var kodeunit;
var panjangklas;
var nows = new Date('d/m/y');
var chkAktifProduk;
var kriteriadepan;
var kdtarifuser;
var syseting;
function FormLookupKasir(criteria,criteriab,nilai_kd_tarif,dsStore,p,mBolAddNew,idx,mBolLookup,syssett) 
{
syseting=syssett;
if (criteriab===undefined)
			{
			
			kriteriadepan='';
			}
			else
			{
		
			kriteriadepan=criteriab;
			
			};
	
	kodeunit=criteria;
	kdtarifuser=nilai_kd_tarif;
    vWinFormEntryLookupProduk = new Ext.Window
	(
		{
			id: 'vWinFormEntryLookupProduk',
			title: 'Lihat Produk',
			closeAction: 'hide',
			closable:false,
			width: 550,
			height: 600,//420,
			border: false,
			plain: true,
			resizable:false,
			layout: 'form',
			iconCls: 'find',
			modal: true,                                   
			items: 
			[	
				GetPanelLookUpProduk(criteria,criteriab,nilai_kd_tarif,dsStore,p,mBolAddNew,idx,mBolLookup) 
			],
			listeners:
				{ 
					activate: function()
					{ } 
				}
		}
	);
	
    vWinFormEntryLookupProduk.show();
};


function itemsTreeListProdukGridDataView_viKasirRwj()
{	
		
	var treeKlas_produk_viKasirRwj= new Ext.tree.TreePanel
	(
		{
			autoScroll: true,
			split: true,
			height:200,
			loader: new Ext.tree.TreeLoader(),
			listeners: 
			{
				click: function(n) 
				{
					strTreeCriteriProdukEquipment_viKasirRwj=n.attributes
					rowSelectedTreeSetEquipmentKlas_produk=n.attributes;
					if (strTreeCriteriProdukEquipment_viKasirRwj.id != ' 0')
					{
						if (strTreeCriteriProdukEquipment_viKasirRwj.leaf == false)
						{
							var str='';
							//alert( n.attributes.id.length);
						//	alert(n.attributes.id);
							//str=' and left(parent,' + n.attributes.id.length + ')=' + n.attributes.id
                                                        str='substring(kd_klas,1, ' + n.attributes.id.length + ') = ~' + n
														
														.attributes.id + '~';
														kdperent=n.attributes.id;
														panjangklas=n.attributes.id.length;
							RefreshDatProdukEquipmentCat(str);
						}
						else
						{
							RefreshDatProdukEquipmentCat('kd_klas = ~' + strTreeCriteriProdukEquipment_viKasirRwj.id + '~');
						};
					}
					else
					{
						RefreshDatProdukEquipmentCat('');
					};
				}
			}
		}
	);
	
	rootTreeSetEquipmentKlas_produkView = new Ext.tree.AsyncTreeNode
	(
		{
			expanded: true,
			text:'Master Produk',
			id:IdKlas_produkegorySetEquipmentKlas_produkView,
			children: StrTreeSetEquipment,
			autoScroll: true
		}
	)  
  
  treeKlas_produk_viKasirRwj.setRootNode(rootTreeSetEquipmentKlas_produkView);  
	
	
	
	
	
	

    var pnlTreeFormDataWindowPopup_viKasirRwj = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                
				//-------------- ## -------------- 
                treeKlas_produk_viKasirRwj,
                //-------------- ## -------------- 
            ],
            //-------------- #End items# --------------
        }
    )

    return pnlTreeFormDataWindowPopup_viKasirRwj;
}


function GetStrTreeSetEquipment()
{
	 Ext.Ajax.request
	 (
		{
			url: WebAppUrl.UrlExecProcess,
			params: 
			{
				UserID: strUser, // 'Admin',
				ModuleID: 'ProsesGetDataTreeSetCat',
				Params:	''
			},
			success : function(resp) 
			{
				var cst = Ext.decode(resp.responseText);
				StrTreeSetEquipment= cst.arr;
			},
			failure:function()
			{
			    
			}
		}
	);
};

function GetPanelLookUpProduk(criteria,criteriab,nilai_kd_tarif,dsStore,p,mBolAddNew,idx,mBolLookup) 
{
	
	var FormLookupKasir = new Ext.Panel  
	(
		{
			id: 'FormLookupKasir',
			region: 'center',
			layout: 'form',
			title: '',
			bodyStyle: 'padding:15px',
			anchor:'100%',
			border: false,
			bodyStyle: 'background:#FFFFFF;',
			height: 600,//392,
			shadhow: true,
			items: 
			[
				fnGetDTLGridLookUpProduk(criteria,criteriab,nilai_kd_tarif),
				getItemPanelLookupProduk(dsStore,p,mBolAddNew,idx,mBolLookup)
			],
			
        }
	);
	
	return FormLookupKasir;
};


function getItemPanelLookupProduk(dsStore,p,mBolAddNew,idx,mBolLookup) 
{
    var items =
	{
	    layout: 'column',
	    border: true,
		height:39,
		anchor:'100%',
		style:{'margin-top':'-1px'},
	    items:
		[
			{
				layout: 'hBox',
				width:522,
				border: false,
				bodyStyle: 'padding:5px 0px 5px 5px',
				defaults: { margins: '3 3 3 3' },
				anchor: '90%',
				layoutConfig: 
				{
					align: 'middle',
					pack:'end'
				},
				items:
				[
					{
						xtype:'button',
						text:nmBtnOK,
						width:70,
						style:{'margin-left':'0px','margin-top':'0px'},
						hideLabel:true,
						id: 'btnOkLookupProduk',
						handler:function()
						{
						
									for(var i = 0 ; i < dsLookProdukList.getCount();i++)
									{
										if (dsLookProdukList.data.items[i].data.CHEK=== true)
										{
										p=RecordBaruRWJ()
										p.data.DESKRIPSI2 =dsLookProdukList.data.items[i].data.DESKRIPSI;
										p.data.KD_PRODUK=dsLookProdukList.data.items[i].data.KD_PRODUK;
										p.data.DESKRIPSI=dsLookProdukList.data.items[i].data.DESKRIPSI;
										p.data.KD_TARIF=dsLookProdukList.data.items[i].data.KD_TARIF;
										p.data.HARGA=dsLookProdukList.data.items[i].data.TARIF;
										p.data.TGL_BERLAKU=dsLookProdukList.data.items[i].data.TGL_BERLAKU;
										p.data.QTY=1;
										
										if (dsStore.data.items[dsStore.getCount()-1].data.KD_PRODUK === '' || dsStore.data.items[dsStore.getCount()-1].data.DESKRIPSI2 === undefined)
												{
												 
											    	dsStore.removeAt(dsStore.getCount()-1);
													dsStore.insert(dsStore.getCount(), p);
													
												}
												else
												{
												dsStore.insert(dsStore.getCount(), p);
												};
											//	dsStore.insert( dsStore.getCount(), p);
											mBol=true;
							
										}	
										if (dsLookProdukList.data.items[i].data.CHEK=== false)
										{
										
										ShowPesanWarningLookupProduk('Pilih produk dengan ceklist kolom Pilih','Tambah Produk');
										}											
									}
									
									vWinFormEntryLookupProduk.close();	
												//Datasave_KasirRWJ(false);
											
										
									
								//vWinFormEntryLookupProduk.close();
							
							
						}
					},
					{
							xtype:'button',
							text:nmBtnCancel ,
							width:70,
							hideLabel:true,
							id: 'btnCancelLookupProduk',
							handler:function() 
							{
								vWinFormEntryLookupProduk.close();
							}
					}
				]
			}
		],
		
	}
    return items;
};
function ShowPesanInfoprduk(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};
function ShowPesanWarningLookupProduk(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function fnGetDTLGridLookUpProduk(criteria,criteriab,nilai_kd_tarif) 
{

    var fldDetail = ['TARIF','KLASIFIKASI','PERENT','TGL_BERAKHIR','KD_KAT','KD_TARIF','KD_KLAS','DESKRIPSI','YEARS','NAMA_UNIT','KD_PRODUK','TGL_BERLAKU','CHEK'];
	dsLookProdukList = new WebApp.DataStore({ fields: fldDetail });
	chkAktifProduk = new Ext.grid.CheckColumn
	(
		{
			id: 'chkAktifProduku',
			header: 'Pilih',
			align: 'center',
			disabled:false,
			dataIndex: 'CHEK',
			width: 50,
			editor:  new Ext.form.Checkbox({
						id: 'chkAktif22',
						align: 'center',
						width: 50,
						//name: 'active'
					})
	
		}
	);
	var vGridLookProdukFormEntry = new Ext.grid.EditorGridPanel
	(
		{ 
			id:'vGridLookProdukFormEntry',
			title: '',
			stripeRows: true,
			store: dsLookProdukList,
			style:{'margin-top':'-1px'},
			height:530,
			columnLines:true,
			bodyStyle: 'padding:0px',
			border: true,	
			frame:false,
			plugins: [chkAktifProduk],
			sm: new Ext.grid.RowSelectionModel
			(
				{ 
					singleSelect: true,
					listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
						
						}
					}
				}
			),
			cm: fnGridLookProdukColumnModel(),
			
			viewConfig: { forceFit: true },
			tbar:
                [
					'-','Produk ', '-',
					
					{
							xtype: 'textfield',
							id: 'TxtFilterGridDataProduk',
							//emptyText: 'Nama',
							width: 150,
							//height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										
										
												if(Ext.get('TxtFilterGridDataProduk').dom.value!='')
												{
													str=' and LOWER(deskripsi) like LOWER(~'+Ext.get('TxtFilterGridDataProduk').dom.value+'%~)';
																			
													RefreshDatProdukEquipmentCat(str);
													
												}
											else{
												ShowPesanWarningLookupProduk('Isi textbox produk','Lihat Produk');
												
												}
										
									} 						
								}
							}
						},
                    {
                        id: 'btnEditRWJ',
                        text: 'Cari',
                        tooltip: 'Cari',
                        iconCls: 'refresh',
                        handler: function(sm, row, rec)
                        {
                           
							
							
					   }
                    }
					
				]
		}
	);
	

	RefreshDatProdukEquipmentCat(str);
	
	return vGridLookProdukFormEntry;
};



function RefreshDatProdukEquipmentCat(str)
{

			

	Ext.Ajax.request(
	{
	    
	    url: baseURL + "index.php/main/getsistemsettingdefault",
		 params: {
	       
	        command: 'igd_default_klas_produk',
			
	    },
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
	    success: function(o) {
			
		
	       kdsetting=o.responseText;
			
			
			if (str == undefined || str == '')
			{
			str='LOWER(kd_tarif)=LOWER(~'+kdtarifuser+'~) and kd_unit= ~'+ kodeunit +'~ and tgl_berakhir is null and tglberlaku<= gettanggalberlakufromklas(~TU~,now()::date, now()::date,~~) and kd_klas in ('+kdsetting+')'+kriteriadepan+'' ;
			
			}
			else
			{
			str='LOWER(kd_tarif)=LOWER(~'+kdtarifuser+'~) and kd_unit= ~'+ kodeunit +'~ and tgl_berakhir is null and tglberlaku<= gettanggalberlakufromklas(~TU~,now()::date, now()::date,~'+71+'~) and kd_klas in( '+kdsetting+') '+str+'' ;
			
			}
				dsLookProdukList.load
				(
					{ 
						params: 
						{ 
							Skip: 0, 
							Take: 1000, 
							Sort: 'KD_PRODUK',
							Sortdir: 'ASC',
							target: 'LookupProduk',
							param: str

						}
					}
				);
	
	//kdperent='';
	return dsLookProdukList;
	str = '';
	kriteriadepan='';
	    }
	
	}
	);
		



	
};



function fnGridLookProdukColumnModel() 
{




	
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			chkAktifProduk,
			{ 
				id: 'colProdukNameLook',
				header: 'Produk',
				dataIndex: 'DESKRIPSI',
				width: 300
            },
			{ 
				id: 'colProdukTARIFLook',
				header: 'Harga',
				align: 'right',
				dataIndex: 'TARIF',
				width: 300,
				renderer: function(v, params, record) 
							{
							return formatCurrency(record.data.TARIF);
							
							}	
            }
		]
	)
};

