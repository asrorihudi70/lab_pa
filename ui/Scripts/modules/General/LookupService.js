/// <reference path="../../ext-base.js" />
/// <reference path="../../ext-all.js" />

var rowSelectedLookService;
var vWinFormEntryLookupService;


function FormLookupService(x,y,criteria,mBolGrid,p,dsStore,mBolLookup) 
{
	rowSelectedLookService = undefined;
    vWinFormEntryLookupService = new Ext.Window
	(
		{
			id: 'vWinFormEntryLookupService',
			title: nmTitleFormLookupServ,
			closeAction: 'hide',
			closable:false,
			width: 450,
			height: 420,
			border: false,
			plain: true,
			resizable:false,
			layout: 'form',
			iconCls: 'find',
			modal: true,                                   
			items: 
			[
				GetPanelLookUpService(x,y,criteria,mBolGrid,p,dsStore,mBolLookup)
			],
			listeners:
				{ 
					activate: function()
					{ } 
				}
		}
	);

    vWinFormEntryLookupService.show();
};

function GetPanelLookUpService(x,y,criteria,mBolGrid,p,dsStore,mBolLookup)
{
	
	var FormLookUpService = new Ext.Panel  
	(
		{
			id: 'FormLookUpService',
			region: 'center',
			layout: 'form',
			title: '',
			bodyStyle: 'padding:15px',
			anchor:'100%',
			border: false,
			bodyStyle: 'background:#FFFFFF;',
			height: 392,
			shadhow: true,
			items: 
			[
				fnGetDTLGridLookUpService(criteria),
				getItemPanelLookupService(x,y,mBolGrid,p,dsStore,mBolLookup)
			]
        }
	);
	
	return FormLookUpService;
};

function getItemPanelLookupService(x,y,mBolGrid,p,dsStore,mBolLookup) 
{
    var items =
	{
	    layout: 'column',
	    border: true,
		height:40,
		anchor:'100%',
		style:{'margin-top':'-1px'},
	    items:
		[
			{
				layout: 'hBox',
				width:422,
				border: false,
				bodyStyle: 'padding:5px 0px 5px 5px',
				defaults: { margins: '3 3 3 3' },
				anchor: '90%',
				layoutConfig: 
				{
					align: 'middle',
					pack:'end'
				},
				items:
				[
					{
						xtype:'button',
						text:nmBtnOK,
						width:70,
						style:{'margin-left':'0px','margin-top':'0px'},
						hideLabel:true,
						id: 'btnOkLookupService',
						handler:function()
						{
							if (rowSelectedLookService != undefined)
							{
								if (mBolGrid === true)
								{
									p.data.SERVICE_ID=rowSelectedLookService.data.SERVICE_ID;
									p.data.SERVICE_NAME=rowSelectedLookService.data.SERVICE_NAME;
									
									if (mBolLookup === true)
										{
										   dsStore.insert(dsStore.getCount(), p);
										}
										else
										{
										   dsStore.removeAt(dsStore.getCount()-1);
										   dsStore.insert(dsStore.getCount(), p);
										}
								}
								else
								{
									Ext.get(x).dom.value=rowSelectedLookService.data.SERVICE_ID;
									Ext.get(y).dom.value=rowSelectedLookService.data.SERVICE_NAME;
								};
								
								vWinFormEntryLookupService.close();
							}
							else
							{
								ShowPesanWarningLookupService(nmGetValidasiSelect(nmServLookupServ),nmTitleFormLookupServ);
							};
							
						}
					},
					{
							xtype:'button',
							text:nmBtnCancel ,
							width:70,
							hideLabel:true,
							id: 'btnCancelLookupService',
							handler:function() 
							{
								vWinFormEntryLookupService.close();
							}
					}
				]
			}
		]
	}
    return items;
};

function ShowPesanWarningLookupService(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING
		}
	);
};

function fnGetDTLGridLookUpService(criteria) 
{  
	
	var fldDetail = ['SERVICE_ID','SERVICE_NAME'];
	var dsLookServiceList = new WebApp.DataStore({ fields: fldDetail });

	var vGridLookServiceFormEntry = new Ext.grid.EditorGridPanel
	(
		{ 
			id:'vGridLookServiceFormEntry',
			title: '',
			stripeRows: true,
			store: dsLookServiceList,
			height:353,
			columnLines:true,
			bodyStyle: 'padding:0px',
			border: true,	
			frame:false,
			sm: new Ext.grid.RowSelectionModel
			(
				{ 
					singleSelect: true,
					listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedLookService = dsLookServiceList.getAt(row);
						}
					}
				}
			),
			cm: fnGridLookServiceColumnModel(),
			viewConfig: { forceFit: true }
		}
	);
	

	dsLookServiceList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				//Sort: 'SERVICE_ID',
                                Sort: 'service_id',
				Sortdir: 'ASC', 
				target:'LookupService',
				param: criteria
			}
		}
	);
	
	return vGridLookServiceFormEntry;
};



function fnGridLookServiceColumnModel() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{ 
				id: 'colKdServiceLook',
				header: nmServIDLookupServ,
				dataIndex: 'SERVICE_ID',
				width: 70
			},
			{ 
				id: 'colServiceNameLook',
				header:nmServNameLookupServ,
				dataIndex: 'SERVICE_NAME',
				width: 300
            }
		]
	)
};
///---------------------------------------------------------------------------------------///