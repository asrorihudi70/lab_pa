/// <reference path="../../ext-base.js" />
/// <reference path="../../ext-all.js" />

var rowSelectedLookServiceCategory;
var vWinFormEntryLookupServiceCategory;


function FormLookupServiceCategory(x,y,criteria,mBolGrid,p,dsStore,mBolLookup) 
{
	rowSelectedLookServiceCategory = undefined;
    vWinFormEntryLookupServiceCategory = new Ext.Window
	(
		{
			id: 'vWinFormEntryLookupServiceCategory',
			title: nmTitleFormLookupServ,
			closeAction: 'hide',
			closable:false,
			width: 450,
			height: 420,
			border: false,
			plain: true,
			resizable:false,
			layout: 'form',
			iconCls: 'find',
			modal: true,                                   
			items: 
			[
				GetPanelLookUpServiceCategory(x,y,criteria,mBolGrid,p,dsStore,mBolLookup)
			],
			listeners:
				{ 
					activate: function()
					{ } 
				}
		}
	);

    vWinFormEntryLookupServiceCategory.show();
};

function GetPanelLookUpServiceCategory(x,y,criteria,mBolGrid,p,dsStore,mBolLookup)
{
	
	var FormLookUpServiceCategory = new Ext.Panel  
	(
		{
			id: 'FormLookUpServiceCategory',
			region: 'center',
			layout: 'form',
			title: '',
			bodyStyle: 'padding:15px',
			anchor:'100%',
			border: false,
			bodyStyle: 'background:#FFFFFF;',
			height: 392,
			shadhow: true,
			items: 
			[
				fnGetDTLGridLookUpServiceCategory(criteria),
				getItemPanelLookupServiceCategory(x,y,mBolGrid,p,dsStore,mBolLookup)
			]
        }
	);
	
	return FormLookUpServiceCategory;
};

function getItemPanelLookupServiceCategory(x,y,mBolGrid,p,dsStore,mBolLookup) 
{
    var items =
	{
	    layout: 'column',
	    border: true,
		height:40,
		anchor:'100%',
		style:{'margin-top':'-1px'},
	    items:
		[
			{
				layout: 'hBox',
				width:422,
				border: false,
				bodyStyle: 'padding:5px 0px 5px 5px',
				defaults: { margins: '3 3 3 3' },
				anchor: '90%',
				layoutConfig: 
				{
					align: 'middle',
					pack:'end'
				},
				items:
				[
					{
						xtype:'button',
						text:nmBtnOK,
						width:70,
						style:{'margin-left':'0px','margin-top':'0px'},
						hideLabel:true,
						id: 'btnOkLookupServiceCategory',
						handler:function()
						{
							if (rowSelectedLookServiceCategory != undefined)
							{
								if (mBolGrid === true)
								{
									p.data.SERVICE_ID=rowSelectedLookServiceCategory.data.SERVICE_ID;
									p.data.SERVICE_NAME=rowSelectedLookServiceCategory.data.SERVICE_NAME;
									
									if (mBolLookup === true)
										{
										   dsStore.insert(dsStore.getCount(), p);
										}
										else
										{
										   dsStore.removeAt(dsStore.getCount()-1);
										   dsStore.insert(dsStore.getCount(), p);
										}
								}
								else
								{
									Ext.get(x).dom.value=rowSelectedLookServiceCategory.data.SERVICE_ID;
									Ext.get(y).dom.value=rowSelectedLookServiceCategory.data.SERVICE_NAME;
								};
								
								vWinFormEntryLookupServiceCategory.close();
							}
							else
							{
								ShowPesanWarningLookupServiceCategory(nmGetValidasiSelect(nmServLookupServ),nmTitleFormLookupServ);
							};
							
						}
					},
					{
							xtype:'button',
							text:nmBtnCancel ,
							width:70,
							hideLabel:true,
							id: 'btnCancelLookupServiceCategory',
							handler:function() 
							{
								vWinFormEntryLookupServiceCategory.close();
							}
					}
				]
			}
		]
	}
    return items;
};

function ShowPesanWarningLookupServiceCategory(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING
		}
	);
};

function fnGetDTLGridLookUpServiceCategory(criteria) 
{  
	
	var fldDetail = ['SERVICE_ID','SERVICE_NAME'];
	var dsLookServiceCategoryList = new WebApp.DataStore({ fields: fldDetail });

	var vGridLookServiceCategoryFormEntry = new Ext.grid.EditorGridPanel
	(
		{ 
			id:'vGridLookServiceCategoryFormEntry',
			title: '',
			stripeRows: true,
			store: dsLookServiceCategoryList,
			height:353,
			columnLines:true,
			bodyStyle: 'padding:0px',
			border: true,	
			frame:false,
			sm: new Ext.grid.RowSelectionModel
			(
				{ 
					singleSelect: true,
					listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedLookServiceCategory = dsLookServiceCategoryList.getAt(row);
						}
					}
				}
			),
			cm: fnGridLookServiceCategoryColumnModel(),
			viewConfig: { forceFit: true }
		}
	);
	

	dsLookServiceCategoryList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				//Sort: 'SERVICE_ID',
                                Sort: 'service_id',
				Sortdir: 'ASC', 
				target:'LookupServiceCategory',
				param: criteria
			}
		}
	);
	
	return vGridLookServiceCategoryFormEntry;
};



function fnGridLookServiceCategoryColumnModel() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{ 
				id: 'colKdServiceCategoryLook',
				header: nmServIDLookupServ,
				dataIndex: 'SERVICE_ID',
				width: 70
			},
			{ 
				id: 'colServiceCategoryNameLook',
				header: nmServNameLookupServ,
				dataIndex: 'SERVICE_NAME',
				width: 300
            }
		]
	)
};
///---------------------------------------------------------------------------------------///