/// <reference path="../../ext-base.js" />
/// <reference path="../../ext-all.js" />

var rowSelectedLookServiceWOPM;
var vWinFormEntryLookupServiceWOPM;
var chkServiceLookupServiceWOPM;
var dsLookServiceWOPMList;


function FormLookupServiceWOPM(criteria,p,dsStore) 
{
	rowSelectedLookServiceWOPM = undefined;
    vWinFormEntryLookupServiceWOPM = new Ext.Window
	(
		{
			id: 'vWinFormEntryLookupServiceWOPM',
			title: nmTitleFormLookupServ,
			closeAction: 'hide',
			closable:false,
			width: 450,
			height: 420,
			border: false,
			plain: true,
			resizable:false,
			layout: 'form',
			iconCls: 'find',
			modal: true,                                   
			items: 
			[
				GetPanelLookUpServiceWOPM(criteria,p,dsStore)
			],
			listeners:
				{ 
					activate: function()
					{ } 
				}
		}
	);

    vWinFormEntryLookupServiceWOPM.show();
};

function GetPanelLookUpServiceWOPM(criteria,p,dsStore)
{
	
	var FormLookUpServiceWOPM = new Ext.Panel  
	(
		{
			id: 'FormLookUpServiceWOPM',
			region: 'center',
			layout: 'form',
			title: '',
			bodyStyle: 'padding:15px',
			anchor:'100%',
			border: false,
			bodyStyle: 'background:#FFFFFF;',
			height: 392,
			shadhow: true,
			items: 
			[
				fnGetDTLGridLookUpServiceWOPM(criteria),
				getItemPanelLookupServiceWOPM(p,dsStore)
			]
        }
	);
	
	return FormLookUpServiceWOPM;
};

function getItemPanelLookupServiceWOPM(p,dsStore) 
{
    var items =
	{
	    layout: 'column',
	    border: true,
		height:40,
		anchor:'100%',
		style:{'margin-top':'-1px'},
	    items:
		[
			{
				layout: 'hBox',
				width:422,
				border: false,
				bodyStyle: 'padding:5px 0px 5px 5px',
				defaults: { margins: '3 3 3 3' },
				anchor: '90%',
				layoutConfig: 
				{
					align: 'middle',
					pack:'end'
				},
				items:
				[
					{
						xtype:'button',
						text:nmBtnOK,
						width:70,
						style:{'margin-left':'0px','margin-top':'0px'},
						hideLabel:true,
						id: 'btnOkLookupServiceWOPM',
						handler:function()
						{
							var mBol= false;
							for(var i=0;i < dsLookServiceWOPMList.getCount();i++)
							{
								if (dsLookServiceWOPMList.data.items[i].data.SELECT === true)
								{
									p = GetRecordBaruServicePMWorkOrder()
									p.data.SERVICE_ID=dsLookServiceWOPMList.data.items[i].data.SERVICE_ID;
									p.data.SERVICE_NAME=dsLookServiceWOPMList.data.items[i].data.SERVICE_NAME;
									p.data.ROW_SCH=dsLookServiceWOPMList.data.items[i].data.ROW_SCH;
									p.data.DUE_DATE=dsLookServiceWOPMList.data.items[i].data.DUE_DATE;
									dsStore.insert(dsStore.getCount(), p);
									mBol=true;
								};
							}
							
							if (mBol === false)
							{
								ShowPesanWarningLookupServiceWOPM(nmGetValidasiSelect(nmServLookupServ),nmTitleFormLookupServ);
							}
							else
							{
								vWinFormEntryLookupServiceWOPM.close();
							};
						}
					},
					{
						xtype:'button',
						text:nmBtnCancel ,
						width:70,
						hideLabel:true,
						id: 'btnCancelLookupServiceWOPM',
						handler:function() 
						{
							vWinFormEntryLookupServiceWOPM.close();
						}
					}
				]
			}
		]
	}
    return items;
};

function ShowPesanWarningLookupServiceWOPM(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING
		}
	);
};

function fnGetDTLGridLookUpServiceWOPM(criteria) 
{  
	
	var fldDetail = ['SCH_CM_ID','SERVICE_ID','SERVICE_NAME','CATEGORY_ID','DUE_DATE','ROW_SCH','CATEGORY_ID','SELECT'];
	dsLookServiceWOPMList = new WebApp.DataStore({ fields: fldDetail });
	
	chkServiceLookupServiceWOPM = new Ext.grid.CheckColumn
	(
		{
			id: 'chkServiceLookupServiceWOPM',
			header: nmSelectLookupServ,
			align: 'center',
			disabled:true,
			dataIndex: 'SELECT',
			width: 70
		}
	);

	var vGridLookServiceWOPMFormEntry = new Ext.grid.EditorGridPanel
	(
		{ 
			id:'vGridLookServiceWOPMFormEntry',
			title: '',
			stripeRows: true,
			store: dsLookServiceWOPMList,
			height:353,
			columnLines:true,
			bodyStyle: 'padding:0px',
			border: true,	
			frame:false,
			sm: new Ext.grid.CellSelectionModel
			(
				{ 
					singleSelect: true,
					listeners:
					{ 
						cellselect: function(sm, row, rec) 
						{
							rowSelectedLookServiceWOPM = dsLookServiceWOPMList.getAt(row);
						}
					}
				}
			),
			cm: fnGridLookServiceWOPMColumnModel(),
			plugins:chkServiceLookupServiceWOPM,
			viewConfig: { forceFit: true }
		}
	);
	

	dsLookServiceWOPMList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				//Sort: 'SERVICE_ID',
                                Sort: 'service_id',
				Sortdir: 'ASC', 
				target:'ViewSchServicePM',
				param: criteria
			}
		}
	);
	
	return vGridLookServiceWOPMFormEntry;
};



function fnGridLookServiceWOPMColumnModel() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),chkServiceLookupServiceWOPM,
			{ 
				id: 'colKdServiceWOPMLook',
				header: nmServIDLookupServ,
				dataIndex: 'SERVICE_ID',
				width: 90
			},
			{ 
				id: 'colServiceWOPMNameLook',
				header: nmServNameLookupServ,
				dataIndex: 'SERVICE_NAME',
				width: 300
            },
			{ 
				id: 'colDueDateWOPMNameLook',
				header: nmDueDateLookupServ,
				dataIndex: 'DUE_DATE',
				width: 100,
				renderer: function(v, params, record) 
				{
					return ShowDate(record.data.DUE_DATE);
				}
            }
		]
	)
};
///---------------------------------------------------------------------------------------///