/// <reference path="../../ext-base.js" />
/// <reference path="../../ext-all.js" />

var rowSelectedLook_vupload;
var strpathfile;
var FormWindow;
var nAsal;
var strDirektori;
function Lookup_vupload(Asal,Direktori) 
{	
    var FormUpldAsset = new Ext.Window
	(			
		{
			id: 'FormUpldAsset',
			name:'FormUpldAsset',
			title: nmTitleFormUpload,
			closeAction: 'destroy',
			//closable:false,
			width: 450,
			height: 120,//400,
			//bodyStyle: 'padding:5px 5px 5px 5px',
			border: false,			
			frame: true,
			plain: true,
			resizable:false,
			layout: 'form',
			iconCls: 'find',			
			modal: true,                                   
			items: 
			[
				GetpanelUpld_Asset(Asal)
			],		
			listeners:
			{ 
				activate: function()
				{ 
					nAsal=Asal;
					strDirektori=Direktori;
				} 
			}
		}
	);

		
			
    FormUpldAsset.show();
};

function GetpanelUpld_Asset(Asal)
{
    var FormUpldAsset2 = new Ext.FormPanel
	(
		{
		    id: 'FormUpldAsset2',
			name: 'FormUpldAsset2',
		    layout: 'form',
		    width: 400,
		    height: 135,
		    frame: false,
		    border: true,
		    fileUpload: true,
		    bodyStyle: 'background:#FFFFFF;',		
			 bodyStyle: 'padding: 10px 10px 10px 10px;',
			 autoHeight: true,
		    labelAlign: 'right',
			 labelWidth:60,
		    anchor: '100%',
		    items:
			[
				{
				    xtype: 'fileuploadfield',
				    emptyText: nmEmptyPathUpload,
				    fieldLabel: nmLabelPathUpload + ' ',									
				    frame: false,
				    border: false,
				    name: 'file',
					id:'file',
				    anchor: '100%'//,
				    //buttonText: 'browse'
				},
				{
				    xtype: 'hidden',
				    name: 'direktori',
					id: 'direktori',
				    value: 'Foto Asset'
				},
				{
					layout: 'hBox',
					width:470,
					border: false,
					bodyStyle: 'padding:5px 0px 5px 5px',
					defaults: { margins: '3 3 3 3' },
					anchor: '87%',
					layoutConfig: 
					{
						align: 'middle',
						pack:'end'
					},
					items:
					[
						{
							xtype:'button',
							text:nmBtnUpload,
							width:70,
							style:{'margin-left':'0px','margin-top':'0px'},
							hideLabel:true,
							id: 'btnUploadAset',
							handler:function()
							{
								 Ext.get('direktori').dom.value =strDirektori;					  
								  if (FormUpldAsset2.getForm().isValid()) 
								  {
									  FormUpldAsset2.getForm().submit
									({
										//url: 'Home/Upload',
										//url: "./home.mvc/Upload",
                                                                                url: baseURL + "index.php/main/Upload",
										waitMsg: nmWaitMsgUploadFile,
										success: function(form, o) //(3)
										{
											strpathfile = o.result.namafile;					        
											Ext.Msg.show
											({
												title: nmTitleFormUpload,
												msg: o.result.result,
												buttons: Ext.Msg.OK,
												//strpathfile: o.result.namafile,
												icon: Ext.Msg.INFO
											});
										},
										failure: function(form, o) //(4)
										{
											Ext.Msg.show
											({
												title: nmTitleFormUpload,
												msg: o.result.error,
												buttons: Ext.Msg.OK,
												icon: Ext.Msg.ERROR
											});
										}
									});
								}
							}
						},
						{
							xtype:'button',
							text:nmBtnOK,
							width:70,
							hideLabel:true,
							id: 'btnOKUploadAset',
							handler:function() 
							{
								 //strpathfile = Ext.get('file').getValue();
								  var pathupload;					  
								  pathupload = './' + strDirektori + '/' + strpathfile;
								 if (Asal === 1)
								 {
									strPathFileImageSetEquipment = pathupload;
									Dataview_fotoAsset(pathupload);
								 }else
								  if (Asal === 5)
								 {
									Ext.get('txtAttach_SetReff').dom.value = pathupload;
									Dataview_fotoReff(pathupload);
								 }
								 else
								 {
									Dataview_fotoAssetSetMetering(pathupload,Asal)
								 }
								  FormWindow = Ext.getCmp('FormUpldAsset');
								  FormWindow.close();
							}
						}
					]
				}
			]
		});   

			
		return FormUpldAsset2;
};
///---------------------------------------------------------------------------------------///