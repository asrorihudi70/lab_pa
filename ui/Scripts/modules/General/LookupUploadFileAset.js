/// <reference path="../../ext-base.js" />
/// <reference path="../../ext-all.js" />

var rowSelectedLook_vuploadAset;
var strpathfileAset;
var strExtAsetRef;
var strPathImgAsetRef;
var strIdAsetRef;
var FormWindowAset;
var nAsalAset;
var strDirektoriAset;
var mBolUploadSetAsetRef=false;

function Lookup_vuploadAset(Asal,Direktori,ds,p,idx) 
{	
	strPathImgAsetRef =  '';
	strExtAsetRef = '';
	strIdAsetRef = '';	
	pathupload = '';
	strpathfileAset='';
	
    var FormUpldAssetRef = new Ext.Window
	(			
		{
			id: 'FormUpldAssetRef',
			name:'FormUpldAssetRef',
			title: nmTitleFormUpload,
			closeAction: 'destroy',
			closable:false,
			width: 500,
			height: 190,//400,
			//bodyStyle: 'padding:5px 5px 5px 5px',
			border: false,			
			frame: true,
			plain: true,
			resizable:false,
			layout: 'form',
			iconCls: 'find',			
			modal: true,                                   
			items: 
			[
				GetpanelUpld_AssetRef(Asal,ds,p,idx)
			],		
			listeners:
			{ 
				activate: function()
				{ 
					nAsalAset=Asal;
					strDirektoriAset=Direktori;
				} 
			}
		}
	);

		
    FormUpldAssetRef.show();
	if (p != undefined)
	{
		if (p.data.PATH != '' && p.data.PATH != undefined && p.data.PATH != null)
		{
			initSetupAssetRef(p);
		};
	};
};

function initSetupAssetRef(p)
{
	Ext.get('txtTitleSetEquipmentRef').dom.value=p.data.TITLE;
	if(p.data.DESCRIPTION === '' || p.data.DESCRIPTION === null)
	{
		Ext.get('txtDescSetEquipmentRef').dom.value='';
	}
	else
	{
		Ext.get('txtDescSetEquipmentRef').dom.value=p.data.DESCRIPTION;
	};
	
	if(p.data.PATH === '' || p.data.PATH === null || p.data.PATH === undefined || p.data.PATH === 'null')
	{
		Ext.get('fileAsetRef').dom.value='';
	}
	else
	{
		Ext.get('fileAsetRef').dom.value=p.data.PATH;
	};
	
	strPathImgAsetRef =  p.data.PATH_IMAGE
	strExtAsetRef = p.data.EXT
	strIdAsetRef = p.data.REFF_ID		
	pathupload = p.data.PATH
	strpathfileAset=p.data.PATH
};

function GetpanelUpld_AssetRef(Asal,ds,p,idx)
{
    var FormUpldAssetRef2 = new Ext.FormPanel
		({
		    id: 'FormUpldAssetRef2',
			name: 'FormUpldAssetRef2',
		    layout: 'form',
		    width: 500,
		    height: 200,
		    frame: false,
		    border: true,
		    fileUpload: true,
		    bodyStyle: 'background:#FFFFFF;',		
			 bodyStyle: 'padding: 10px 10px 10px 10px;',
			 autoHeight: true,
			 labelAlign: 'right',
			 labelWidth:70,
		    anchor: '100%',
		    items:
			[
				{
					xtype: 'textfield',
					fieldLabel: nmTitleUpload + ' ',
					name: 'txtTitleSetEquipmentRef',
					id: 'txtTitleSetEquipmentRef',
					anchor: '100%'
				},
				{
					xtype: 'textarea',
					fieldLabel: nmDescUpload + ' ',
					name: 'txtDescSetEquipmentRef',
					id: 'txtDescSetEquipmentRef',
					scroll:true,
					anchor: '100% 30%'
				},
				{
				    xtype: 'fileuploadfield',
				    emptyText: nmEmptyPathUpload,
				    fieldLabel: nmLabelPathUpload + ' ',									
				    frame: false,
				    border: false,
				    name: 'fileAsetRef',
					id:'fileAsetRef',
					labelAlign: 'right',
				    anchor: '100%'
				},
				{
				    xtype: 'hidden',
				    name: 'direktoriAsetRef',
					id: 'direktoriAsetRef',
				    value: 'Foto Asset'
				},
				{
					layout: 'hBox',
					width:470,
					border: false,
					bodyStyle: 'padding:5px 0px 5px 5px',
					defaults: { margins: '3 3 3 3' },
					anchor: '87%',
					layoutConfig: 
					{
						align: 'middle',
						pack:'end'
					},
					items:
					[
						{
							xtype:'button',
							text:nmBtnUpload,
							width:70,
							style:{'margin-left':'0px','margin-top':'0px'},
							hideLabel:true,
							id: 'btnUploadRefAset',
							handler:function()
							{
								Ext.get('direktoriAsetRef').dom.value =strDirektoriAset;					  
								  if (FormUpldAssetRef2.getForm().isValid()) 
								  {
									  FormUpldAssetRef2.getForm().submit
									({
										//url: 'Home/Upload',
										//url: "./home.mvc/UploadRef",
                                                                                url: baseURL + "index.php/main/UploadRef",
										waitMsg: nmWaitMsgUploadFile,
										success: function(form, o) //(3)
										{
											mBolUploadSetAsetRef = true;
											strpathfileAset = o.result.namafile;
											strExtAsetRef=o.result.namaext;
											GetPathFileSetEquipmentRef(strExtAsetRef);
											Ext.Msg.show
											({
												title: nmTitleFormUpload,
												msg: o.result.result,
												buttons: Ext.Msg.OK,
												width:250,
												//strpathfileAset: o.result.namafile,
												icon: Ext.Msg.INFO
											});
										},
										failure: function(form, o) //(4)
										{
											mBolUploadSetAsetRef=false;
											Ext.Msg.show
											({
												title: nmTitleFormUpload,
												msg: o.result.error,
												buttons: Ext.Msg.OK,
												icon: Ext.Msg.ERROR
											});
										}
									});
								  }
							}
						},
						{
							xtype:'button',
							text:nmBtnOK,
							width:70,
							hideLabel:true,
							id: 'btnOKRefAset',
							handler:function() 
							{
								var mBol=true;
								var mBol2=true;
								var mBolAddNewRef=true;
								if (strpathfileAset != '' && strpathfileAset != undefined && strpathfileAset != null && strpathfileAset != 'null')
								{
									if(validasiSetEquipmentRef()=== 1)
									{
										var pathupload;	
										
										if(p.data.PATH != '' && p.data.PATH != 'null' && p.data.PATH != null && p.data.PATH != undefined )
										{
											mBolAddNewRef = false;
											if (mBolUploadSetAsetRef === false)
											{
												pathupload=strpathfileAset
											}
											else
											{
												pathupload = './' + strDirektoriAset + '/' + strpathfileAset;
											};
											
										}
										else
										{
											pathupload = './' + strDirektoriAset + '/' + strpathfileAset;
										};
																				
										p.data.TITLE=Ext.get('txtTitleSetEquipmentRef').dom.value;
										p.data.DESCRIPTION=Ext.get('txtDescSetEquipmentRef').dom.value;
										p.data.EXT=strExtAsetRef;
										p.data.PATH_IMAGE=strPathImgAsetRef;
										p.data.PATH=pathupload;
										  
										if(mBolAddNewRef === true)
										{
											p.data.REFF_ID=strIdAsetRef;
											ds.insert(ds.getCount(), p);
										}
										else
										{
											ds.removeAt(idx);
											ds.insert(idx, p);
										};
									}
									else
									{
										mBol=false;
									};
								}
								else if ((strpathfileAset === '' || strpathfileAset === undefined) && Ext.get('fileAsetRef').dom.value != '')
								{
									mBol2=false;
									Ext.Msg.show
									(
										{
										   title:nmTitleFormUpload,
										   msg: nmKonfirmasiUploadFile, 
										   buttons: Ext.MessageBox.YESNO,
										   fn: function (btn) 
										   {			
											   if (btn =='yes') 
												{
													mBol = true
													FormWindowAset = Ext.getCmp('FormUpldAssetRef');
													FormWindowAset.close();
												}
												else
												{
													mBol = false
												};
										   },
										   icon: Ext.MessageBox.QUESTION
										}
									);
								};
								
								if (mBol === true && mBol2 === true)
								{
									FormWindowAset = Ext.getCmp('FormUpldAssetRef');
									FormWindowAset.close();
								};
							}
						}
					]
				}
			]
		});   
		return FormUpldAssetRef2;
};

function validasiSetEquipmentRef()
{
	var x=1;
	
	if(Ext.get('txtTitleSetEquipmentRef').dom.value === '')
	{
		x=0;
		ShowPesanWarningSetEquipment(nmGetValidasiKosong(nmTitleUpload),nmTitleFormUpload);
	};
	
	return x;
};

function GetPathFileSetEquipmentRef(str)
{
	 Ext.Ajax.request
	 (
		{
			//url: "./Module.mvc/ExecProc",
                        url: baseURL + "index.php/main/ExecProc",
			params: 
			{
				UserID: 'Admin',
				ModuleID: 'ProsesGetPathFileSetEquipmentRef',
				//Params:	'where description=~' + str + '~'
                                Params:	'description = ~' + str + '~'
			},
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					strPathImgAsetRef=cst.Path;
					strIdAsetRef=cst.RefId;
				}
				else
				{
					strPathImgAsetRef='';
					strIdAsetRef='';
				};
			}

		}
	);
};
///---------------------------------------------------------------------------------------///