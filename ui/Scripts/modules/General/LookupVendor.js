/// <reference path="../../ext-base.js" />
/// <reference path="../../ext-all.js" />

var rowSelectedLookVendor;
var vWinFormEntryLookupVendor;
var varBtnOkLookupVendor=false;


function FormLookupVendor(p,criteria,mBolDetail,mBolGrid,dsStore,mBolLookup)  
{
    rowSelectedLookVendor=undefined;
    vWinFormEntryLookupVendor = new Ext.Window
	(
		{
			id: 'vWinFormEntryLookupVendor',
			title: nmTitleFormLookupVend,
			closeAction: 'hide',
			closable:false,
			width: 450,
			height: 420,
			border: false,
			plain: true,
			resizable:false,
			layout: 'form',
			iconCls: 'find',
			modal: true,                                   
			items: 
			[
				GetPanelLookUpVendor(p,criteria,mBolDetail,mBolGrid,dsStore,mBolLookup)  
			],
			listeners:
				{ 
					activate: function()
					{ } 
				}
		}
	);

    vWinFormEntryLookupVendor.show();
};

function GetPanelLookUpVendor(p,criteria,mBolDetail,mBolGrid,dsStore,mBolLookup)  
{
	
	var FormLookUpVendor = new Ext.Panel  
	(
		{
			id: 'FormLookUpVendor',
			region: 'center',
			layout: 'form',
			title: '',
			bodyStyle: 'padding:15px',
			anchor:'100%',
			border: false,
			bodyStyle: 'background:#FFFFFF;',
			height: 392,
			shadhow: true,
			items: 
			[
				fnGetDTLGridLookUpVendor(criteria),
				getItemPanelLookupVendor(p,mBolDetail,mBolGrid,dsStore,mBolLookup)  
			]
        }
	);
	
	return FormLookUpVendor;
};

function getItemPanelLookupVendor(p,mBolDetail,mBolGrid,dsStore,mBolLookup)  
{
    var items =
	{
	    layout: 'column',
	    border: true,
		height:40,
		anchor:'100%',
		style:{'margin-top':'-1px'},
	    items:
		[
			{
				layout: 'hBox',
				width:422,
				border: false,
				bodyStyle: 'padding:5px 0px 5px 5px',
				defaults: { margins: '3 3 3 3' },
				anchor: '90%',
				layoutConfig: 
				{
					align: 'middle',
					pack:'end'
				},
				items:
				[
					{
						xtype:'button',
						text:nmBtnOK,
						width:70,
						style:{'margin-left':'0px','margin-top':'0px'},
						hideLabel:true,
						id: 'btnOkLookupVendor',
						handler:function()
						{
							if (p != undefined && rowSelectedLookVendor != undefined)
							{
								if( mBolGrid === true )
								{
									p.data.VENDOR_ID=rowSelectedLookVendor.data.VENDOR_ID;
									p.data.VENDOR=rowSelectedLookVendor.data.VENDOR;
									p.data.PERSON_NAME=rowSelectedLookVendor.data.CONTACT1;
									
									if (mBolLookup === true)
									{
									   dsStore.insert(dsStore.getCount(), p);
									}
									else
									{
									   dsStore.removeAt(dsStore.getCount()-1);
									   dsStore.insert(dsStore.getCount(), p);
									};
									varBtnOkLookupVendor=true;
								}
								else
								{
									var x;
									Ext.get(p.data.KD).dom.value=rowSelectedLookVendor.data.VENDOR_ID;
									x=rowSelectedLookVendor.data.VENDOR;
									if(x != null){Ext.get(p.data.NAME).dom.value=x}else{Ext.get(p.data.NAME).dom.value=''};
								
									if(mBolDetail === true)
									{
										x=rowSelectedLookVendor.data.VEND_ADDRESS;
										if(x != null){Ext.get(p.data.ADDRESS).dom.value=x}else{Ext.get(p.data.ADDRESS).dom.value=''};
										x=rowSelectedLookVendor.data.VEND_CITY;
										if(x != null){Ext.get(p.data.CITY).dom.value=x}else{Ext.get(p.data.CITY).dom.value=''};
										x=rowSelectedLookVendor.data.VEND_PHONE1;
										if(x != null){Ext.get(p.data.PHONE).dom.value=x}else{Ext.get(p.data.PHONE).dom.value=''};
										x=rowSelectedLookVendor.data.CONTACT1;
										if(x != null){Ext.get(p.data.CONTACT1).dom.value=x}else{Ext.get(p.data.CONTACT1).dom.value=''};
										x=rowSelectedLookVendor.data.CONTACT2;
										if(x != null){Ext.get(p.data.CONTACT2).dom.value=x}else{Ext.get(p.data.CONTACT2).dom.value=''};
									};
									//varBtnOkLookupVendor=true;
								};
								
								
								vWinFormEntryLookupVendor.close();
							}
							else
							{
								ShowPesanWarningLookupVendor(nmGetValidasiHapus(nmVendLookupVend),nmTitleFormLookupVend);
							};
							
						}
					},
					{
							xtype:'button',
							text:nmBtnCancel ,
							width:70,
							hideLabel:true,
							id: 'btnCancelLookupVendor',
							handler:function() 
							{
								vWinFormEntryLookupVendor.close();
							}
					}
				]
			}
		]
	}
    return items;
};

function ShowPesanWarningLookupVendor(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function fnGetDTLGridLookUpVendor(criteria) 
{

    var fldDetail = ['VENDOR_ID','VENDOR','CONTACT1','CONTACT2','VEND_ADDRESS','VEND_CITY','VEND_PHONE1','VEND_PHONE2','VEND_POS_CODE','COUNTRY'];
	var dsLookVendorList = new WebApp.DataStore({ fields: fldDetail });

	var vGridLookVendorFormEntry = new Ext.grid.EditorGridPanel
	(
		{ 
			id:'vGridLookVendorFormEntry',
			title: '',
			stripeRows: true,
			store: dsLookVendorList,
			height:353,
			columnLines:true,
			bodyStyle: 'padding:0px',
			border: true,	
			frame:false,
			sm: new Ext.grid.RowSelectionModel
			(
				{ 
					singleSelect: true,
					listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedLookVendor = dsLookVendorList.getAt(row);
						}
					}
				}
			),
			cm: fnGridLookVendorColumnModel(),
			viewConfig: { forceFit: true }
		}
	);
	

	dsLookVendorList.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				//Sort: 'VENDOR_ID',
                                Sort: 'vendor_id',
				Sortdir: 'ASC',
				target: 'LookupVendor',
				param: criteria
			}
		}
	);
	
	return vGridLookVendorFormEntry;
};



function fnGridLookVendorColumnModel() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{ 
				id: 'colKdVendorLook',
				header: nmVendIDLookupVend,
				dataIndex: 'VENDOR_ID',
				width: 70
			},
			{ 
				id: 'colVendorNameLook',
				header: nmVendNameLookupVend,
				dataIndex: 'VENDOR',
				width: 300,
				renderer: function(value, cell) 
				{
					var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
					return str;
                }
            },
			{ 
				id: 'colVendorAddressLook',
				header: nmAddressLookupVend,
				dataIndex: 'VEND_ADDRESS',
				width: 300,
				renderer: function(value, cell) 
				{
				    var str='';
					if(value != null)
					{
						var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
					}
					else
					{
						var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + '' + "</div>";
					};
					return str;
                }
				
            }
		]
	)
};
///---------------------------------------------------------------------------------------///