var winFormLookup_AkadPiutang;
var dsGLLookup_AkadPiutang;
var selectedrowGLLookup_AkadPiutang;
var mCancel=true;
var nASALFORM_Piutang =1;
var nASALFORM_Hutang =2;
var CountGrid=0;
var now_AkadPiutang = new Date();
var NUMBER_AkadPiutang;
var DATE_AkadPiutang;
var CUST_AkadPiutang;
var AccHead_AkadPiutang;
var AccCust_AkadPiutang;
var Amount_AkadPiutang;
var Notes_AkadPiutang;
var Reff_AkadPiutang;
var unitKerja_AkadPiutang;
 
function FormLookup_AkadPiutang(criteriaCust, criteriaNo, criteriaDate,JdlForm,nASALFORM,dblNilai,AccHead,AccCust,StrNote,strReff,strUK)
{
	NUMBER_AkadPiutang=criteriaNo;
	DATE_AkadPiutang=criteriaDate;
	CUST_AkadPiutang=criteriaCust;
	AccCust_AkadPiutang=AccHead;
	AccHead_AkadPiutang=AccCust;
	Amount_AkadPiutang=dblNilai;
	Notes_AkadPiutang=StrNote;
	Reff_AkadPiutang=strReff;
	unitKerja_AkadPiutang=strUK;
	winFormLookup_AkadPiutang = new Ext.Window
	(
		{
		    id: 'winFormLookup_AkadPiutang',
		    title: 'Form Approve '+JdlForm,
		    closeAction: 'destroy',
		    closable: true,
		    width: 650,
		    height: 480,
		    border: false,
		    plain: true,
		    resizable: false,
		    layout: 'border',
		    iconCls: 'find',
		    modal: true,
			autoScroll: true,
		    items: [getItemFormLookup_AkadPiutang(criteriaNo,criteriaDate,nASALFORM,JdlForm,dblNilai)],
			fbar:
			[
				{
					xtype: 'button',
					width: 70,
					text: 'Hitung Tagihan',
					hidden:true,
					handler: function()
					{
						CalcTotal_AkadPiutang();
					}
				},
				{ xtype: 'label', cls: 'left-label', width: 110, text: 'Jumlah Tagihan:' },
				{
					xtype:'textfield',
					name: 'txtTotal_AkadPiutang',
					id: 'txtTotal_AkadPiutang',
					width:100,
					readOnly:true,
					style: 'font-weight: bold;text-align: right'
					// hidden:true
				},
				{
					xtype: 'button',
					width: 70,
					text: 'Ok',
					handler: function()
					{
						var total=0;
						if(ValidasiEntry_AkadPiutang()==1)
						{
							Approve_AkadPiutang(nASALFORM);
							winFormLookup_AkadPiutang.close();

						}
					}
				},
				{
					xtype: 'button',
					width: 70,
					text: 'Cancel',
					handler: function()
					{
						selectedrowGLLookup_AkadPiutang = undefined;
						winFormLookup_AkadPiutang.close();
					}
				}
			],
		}
	);
	
	winFormLookup_AkadPiutang.show();
}

function getItemFormLookup_AkadPiutang(criteriaNo,criteriaDate,nASALFORM,JdlForm,dblNilai)
{
	var pnlButtonLookup_AkadPiutang = new Ext.Panel
	(
		{
			id: 'pnlButtonLookup_AkadPiutang',
			layout: 'hbox',
			border: false,
			region: 'south',
			defaults: { margins: '0 5 0 0' },
			style: { 'margin-left': '4px', 'margin-top': '3px' },
			anchor: '96.5%',
			layoutConfig:
			{
				padding: '3',
				pack: 'end',
				align: 'middle'
			},
			      
			listeners: 
			{
				activate: function()
				{		
					CalcTotal_AkadPiutang();
					CheckboxLineSetTrue(false);
				}												
			}	
		}
	);
	
	var frmListLookup_AkadPiutang = new Ext.Panel
	(
		{
			id: 'frmListLookup_AkadPiutang',
			layout: 'form',
			region: 'center',
			autoScroll: true,
			items: 
			[
				getGridListLookup_AkadPiutang(criteriaNo,nASALFORM,JdlForm),
				pnlButtonLookup_AkadPiutang,
			],
			
		}
	);
	
	return frmListLookup_AkadPiutang;
}

function getGridListLookup_AkadPiutang(criteriaNo,nASALFORM,JdlForm)
{
	var fields_AkadPiutang = 
	[
		'pilih','nomor','tanggal','cust_code','amount','paid','remain','bayar','due_date','posted','jenis_piu','type'
	];
	dsGLLookup_AkadPiutang = new WebApp.DataStore({ fields: fields_AkadPiutang });
	RefreshDataLookup_AkadPiutang(nASALFORM);
	var chkSelectedLookup_AkadPiutang = new Ext.grid.CheckColumn(
		{
			id: 'chkSelectedLookup_AkadPiutang',
			header: '',
			align: 'center',			
			dataIndex: 'pilih',
			width: 20,
			onMouseDown: function(e, t)
			{
				var index = this.grid.getView().findRowIndex(t);			  
				
				   if(t.className && t.className.indexOf('x-grid3-cc-'+this.id) != -1)
				    {
						e.stopEvent();
						var index = this.grid.getView().findRowIndex(t);
						var record = this.grid.store.getAt(index);
						record.set(this.dataIndex, !record.data[this.dataIndex]);
						rowData = record.data;//save row records. will be used in the ajax request
						CalcTotal_AkadPiutang();
					}						
				
			}
		}
	);
	
	gridListLookup_AkadPiutang = new Ext.grid.EditorGridPanel
	(
		{
			stripeRows: true,
			id: 'gridListLookup_AkadPiutang',
			store: dsGLLookup_AkadPiutang,
			height:353,
			anchor: '100% 98%',
			columnLines:true,
			bodyStyle: 'padding:0px',
			border: true,
			frame: true,
			plugins: [new Ext.ux.grid.FilterRow(),chkSelectedLookup_AkadPiutang],
			viewConfig : 
			{
				forceFit: true
			},				
			sm: new Ext.grid.CellSelectionModel
			(
				{ 
					singleSelect: false,
					listeners:
					{ 
						cellselect: function(sm, row, rec) 
						{
							selectedrowGLLookup_AkadPiutang = dsGLLookup_AkadPiutang.getAt(row);
						}						
					}
				}
			),
			// plugins: ,
			cm: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					chkSelectedLookup_AkadPiutang,
					{ 
						id: 'colNoGLLookup_AkadPiutang',
						header: 'No. Piutang',
						dataIndex: 'nomor',
						width:130,	
						renderer: function(value, cell) 
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
							return str;
						},
						filter: {}						
					},
					{ 
						id: 'colTGLoGLLookup_AkadPiutang',
						header: 'Tanggal',
						dataIndex: 'tanggal',
						width:100,	
						renderer: function(v, params, record) 
						{
							return ShowDate(record.data.tanggal);
						},
						filter: {}					
					},
					{ 
						id: 'colJMLGLLookupAkadPiutang',
						header: 'Amount',
						dataIndex: 'amount',
						width:100,	
						align:'right',						
						renderer: function(v, params, record) 
						{
							var str = "<div style='white-space:normal;padding:2px 20px 2px 2px'>" + formatCurrencyDec(record.data.amount) + "</div>";
							return str;
							 
						}	
					}
					,{ 
						id: 'colPaidGLLookupAkadPiutang',
						name: 'colPaidGLLookupAkadPiutang',
						header: 'Paid',
						dataIndex: 'paid',
						width:100,
						xtype:'numbercolumn',
						align:'right',
						editor: new Ext.form.NumberField({
							allowBlank: false,
							enableKeyEvents : true,
							listeners: 
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										CalcTotal_AkadPiutang();
									}
								},
								'blur' : function()
								{									
									CalcTotal_AkadPiutang();				
								},
								'onchange' : function()
								{									
									CalcTotal_AkadPiutang();				
								}
							}
						})	
					},
					{ 
						id: 'colRemainGLLookupAkadPiutang',
						header: 'Remain',
						dataIndex: 'remain',
						width:100,	
						align:'right',							
						renderer: function(v, params, record) 
						{
							// var str = "<div style='white-space:normal;padding:2px 15px 2px 2px'>" + formatCurrencyDec(record.data.remain) + "</div>";
							if (record.data.remain === undefined || record.data.remain === '' || record.data.remain === null) {
					            var str = "<div style='white-space:normal;padding:2px 15px 2px 2px'>" + formatCurrencyDec(record.data.remain) + "</div>";
								return str;
					        }
					        else {
					            if (record.data.paid === undefined || record.data.paid === '' || record.data.paid === null) {
					            	var str = "<div style='white-space:normal;padding:2px 15px 2px 2px'>" + formatCurrencyDec(record.data.remain) + "</div>";
									return str;
					            }
					            else {
					                var str = "<div style='white-space:normal;padding:2px 15px 2px 2px'>" + formatCurrencyDec(record.data.amount - record.data.paid) + "</div>";
									return str;
					            };
					        };							 
						}	
					}
				]
			)
			,listeners:
			{
				'afterrender': function()
				{
					this.store.on("load", function()
						{
							CheckboxLineSetTrue(false);
						} 
					);
					this.store.on("datachanged", function()
						{
							CalcTotal_AkadPiutang();
						}
					);
				},
				activate: function()
				{	
					if(chkSelectedLookup_AkadPiutang == undefined){
						ShowPesanWarning("Tagihan belum dipilih.", "Lookup " + JdlForm);
					}
				}
			}
		}
	);
	
	return gridListLookup_AkadPiutang;
}

function RefreshDataLookup_AkadPiutang(nASALFORM)
{
	var StrTarget='';
	if (nASALFORM == nASALFORM_Piutang ){
		StrTarget="index.php/keuangan/functionApproveAkad/getLookupFakturPiutang";
		var criteriaGrd ={
			kd_customer : CUST_AkadPiutang,
			pembayaran : Amount_AkadPiutang
		};
	 }else{
		StrTarget="index.php/keuangan/functionApproveAkad/getLookupFakturHutang";
		var criteriaGrd ={
			kd_vendor : CUST_AkadPiutang,
			pembayaran : Amount_AkadPiutang
		};
	};

	// dsGLLookup_AkadPiutang.load
	// (
		// {
		    // params:
			// {
			    // Skip: 0,
			    // Take: 1000,
			    // Sort: 'NOMER',
			    // Sortdir: 'ASC',
			    // target: StrTarget,
			    // param: criteriaGrd 
			// }
		// }
	// );
	
	// return dsGLLookup_AkadPiutang;
	Ext.Ajax.request({
		url: baseURL + StrTarget,
		params: criteriaGrd,
		success: function(o) {
			var cst = Ext.decode(o.responseText);
			if (cst.success === true) 
			{
				dsGLLookup_AkadPiutang.removeAll();
				var recs=[],
				recType=dsGLLookup_AkadPiutang.recordType;
				for(var i=0; i<cst.total_record; i++){
					recs.push(new recType(cst.listData[i]));						
				}
				dsGLLookup_AkadPiutang.add(recs);
				gridListLookup_AkadPiutang.getView().refresh();
			}
			else {
				// ShowPesanError_CSARForm('Gagal membaca list detail penerimaan piutang!' , 'Simpan Data');
			}
		}
	})
}
function CalcTotal_AkadPiutang(idx)
{
    var total=0;
	var nilai=0;
	for(var i=0;i < dsGLLookup_AkadPiutang.getCount();i++)
	{
		if (dsGLLookup_AkadPiutang.data.items[i].data.pilih === true)
		{
			nilai=dsGLLookup_AkadPiutang.data.items[i].data.paid;
			total += parseInt(nilai);	
		}else
		{
			total += 0;
		}		
	}
	Ext.get('txtTotal_AkadPiutang').dom.value=formatCurrencyDec(total);
};

function CheckboxLineSetTrue(idx)
{
	for(var i=0;i < dsGLLookup_AkadPiutang.getCount();i++)
	{
		dsGLLookup_AkadPiutang.data.items[i].data.PILIH=idx;
		// total += getNumber(nilai);
	}
};

function ValidasiEntry_AkadPiutang(dblNilai)
{
	var x = 1;
	/* if (parseFloat(getAmount_AkadPiutang(Ext.get('txtTotal_AkadPiutang').getValue())) > parseFloat(getAmount_AkadPiutang(Amount_AkadPiutang)))
	{
		ShowPesanWarning_AkadPiutang('Tagihan melebihi pembayaran','Approve');
		x=0;			
	};
	if (parseFloat(getAmount_AkadPiutang(Ext.get('txtTotal_AkadPiutang').getValue())) < parseFloat(getAmount_AkadPiutang(Amount_AkadPiutang)))
	{
		ShowPesanWarning_AkadPiutang('Pembayaran melebihi tagihan','Approve');
		x=0;			
	}; */
	if (getAmount_AkadPiutang(Ext.get('txtTotal_AkadPiutang').getValue()) <= 0 )
	{
		ShowPesanWarning_AkadPiutang('Tagihan belum dipilih','Approve');
		x=0;			
	};
	return x;
};

function CalcTotalGrid_AkadPiutang()
{
	var CountGrid=0;
  	for(var i=0;i < dsGLLookup_AkadPiutang.getCount();i++)
	{
		if (dsGLLookup_AkadPiutang.data.items[i].data.PILIH === true)
		{
			CountGrid +=1
		}else
		{
		}		
	}
	return CountGrid;
};

function getAmount_AkadPiutang(dblNilai)
{
    // var dblAmount;
    // dblAmount = dblNilai.replace('Rp.', '')
    for (var i = 0; i < dblNilai.length; i++) {
        var x = dblNilai.substr(i, 1)
        if (x === '.') {
            dblNilai = dblNilai.replace('.', '');
        }
    }    
    return dblNilai;
};

function ShowPesanInfo_AkadPiutang(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO
		}
	);
};
function ShowPesanWarning_AkadPiutang(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};

function ShowPesanError_AkadPiutang(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR
		}
	);
};

function Approve_AkadPiutang(nASALFORM) 
{
	var StrTarget='';
	if (nASALFORM == nASALFORM_Piutang ){
		StrTarget="index.php/keuangan/functionApproveAkad/savePiutang";
	 }else{
		StrTarget="index.php/keuangan/functionApproveAkad/saveHutang";
	};
	loadMask.show();
	Ext.Ajax.request
	(
		{
			url: baseURL + StrTarget,
			params:  getParamApprove_AkadPiutang(nASALFORM), 
			success: function(o) 
			{				
				var cst = Ext.decode(o.responseText);
				loadMask.hide();
				if (cst.success === true)
				{
					ShowPesanInfo_AkadPiutang('Data berhasil di Approve','Approve');
					if (nASALFORM==1){
						Ext.getCmp('ChkApproveEntry_CSARForm').setValue(true);
						Ext.getCmp('btnSimpan').disable();
						Ext.getCmp('btnSimpanKeluar').disable();
						Ext.getCmp('btnHapus').disable();
					} else{
						Ext.getCmp('ChkApproveEntry_CSAPForm').setValue(true);
						Ext.getCmp('btnSimpan').disable();
						Ext.getCmp('btnSimpanKeluar').disable();
						Ext.getCmp('btnHapus').disable();
					}
					// RefreshDataFilter_AkadPiutang(false);								
				}
				else if (cst.success === false && cst.pesan === 0 )
				{
					ShowPesanWarning_AkadPiutang('Data tidak berhasil di Approve','Edit Data');
				}
				else 
				{
					ShowPesanError_AkadPiutang('Data tidak berhasil di Approve ' + cst.pesan,'Approve');
				}										
			}
		}
	)
}

function getParamApprove_AkadPiutang(nASALFORM) 
{

	var TglApprove=ShowDateAkuntansi(now_AkadPiutang)
	if(nASALFORM_Piutang==nASALFORM)
	{
		var params = 
		{
			csar_number:NUMBER_AkadPiutang,
			csar_date:DATE_AkadPiutang,
			cust_code:CUST_AkadPiutang,
		};
		params['jumlah']=dsGLLookup_AkadPiutang.getCount();
		for(var i = 0 ; i < dsGLLookup_AkadPiutang.getCount();i++)
		{
			params['pilih-'+i]=dsGLLookup_AkadPiutang.data.items[i].data.pilih;
			params['nomor-'+i]=dsGLLookup_AkadPiutang.data.items[i].data.nomor;
			params['tanggal-'+i]=dsGLLookup_AkadPiutang.data.items[i].data.tanggal;
			params['amount-'+i]=dsGLLookup_AkadPiutang.data.items[i].data.amount;
			params['paid-'+i]=dsGLLookup_AkadPiutang.data.items[i].data.paid;
			params['remain-'+i]=dsGLLookup_AkadPiutang.data.items[i].data.remain;
		}
	}else if (nASALFORM_Hutang==nASALFORM){
		var params = 
		{
			csap_number:NUMBER_AkadPiutang,
			csap_date:DATE_AkadPiutang,
			vend_code:CUST_AkadPiutang,
		};
		params['jumlah']=dsGLLookup_AkadPiutang.getCount();
		for(var i = 0 ; i < dsGLLookup_AkadPiutang.getCount();i++)
		{
			params['pilih-'+i]=dsGLLookup_AkadPiutang.data.items[i].data.pilih;
			params['nomor-'+i]=dsGLLookup_AkadPiutang.data.items[i].data.nomor;
			params['tanggal-'+i]=dsGLLookup_AkadPiutang.data.items[i].data.tanggal;
			params['amount-'+i]=dsGLLookup_AkadPiutang.data.items[i].data.amount;
			params['paid-'+i]=dsGLLookup_AkadPiutang.data.items[i].data.paid;
			params['remain-'+i]=dsGLLookup_AkadPiutang.data.items[i].data.remain;
		}
	}

	
	return params
};

function getArrDetailAR_AkadPiutang(TglApprove)
{
	var x='';
	for(var i = 0 ; i < dsGLLookup_AkadPiutang.getCount();i++)
	{
		// alert(dsGLLookup_AkadPiutang.data.items[i].data.ARO_DATE);
		var y='';
		var z='@@##$$@@';
		y += 'CSAR_NUMBER='+NUMBER_AkadPiutang
		y += z + 'CSAR_DATE='+DATE_AkadPiutang
		y += z + 'ARO_NUMBER='+dsGLLookup_AkadPiutang.data.items[i].data.NOMER	
		y += z + 'ARO_DATE='+ShowDate(dsGLLookup_AkadPiutang.data.items[i].data.TGL)
		y += z + 'AROH_NUMBER='+dsGLLookup_AkadPiutang.data.items[i].data.AROH_NUMBER
		y += z + 'AROH_DATE='+ShowDate(TglApprove)
		y += z + 'AMOUNT='+dsGLLookup_AkadPiutang.data.items[i].data.AMOUNT
		y += z + 'PAID='+ dsGLLookup_AkadPiutang.data.items[i].data.PAID
		y += z + 'REMAIN='+dsGLLookup_AkadPiutang.data.items[i].data.REMAIN
		y += z + 'JENIS_PIU='+dsGLLookup_AkadPiutang.data.items[i].data.JENIS_PIU
		y += z + 'PILIH='+dsGLLookup_AkadPiutang.data.items[i].data.PILIH
		y += z + 'TYPE='+dsGLLookup_AkadPiutang.data.items[i].data.TYPE
		if (i === (dsGLLookup_AkadPiutang.getCount()-1))
		{
			x += y 
		}
		else
		{
			x += y + '##[[]]##'
		}
	}		
	return x;
};	

function getArrDetailAP_AkadPiutang(TglApprove)
{
	var x='';
	for(var i = 0 ; i < dsGLLookup_AkadPiutang.getCount();i++)
	{
		var y='';
		var z='@@##$$@@';
		y += 'CSAP_NUMBER='+NUMBER_AkadPiutang
		y += z + 'CSAP_DATE='+DATE_AkadPiutang
		y += z + 'APO_NUMBER='+dsGLLookup_AkadPiutang.data.items[i].data.NOMER
		y += z + 'APO_DATE='+ShowDate(dsGLLookup_AkadPiutang.data.items[i].data.TGL)
		y += z + 'APOH_NUMBER='+dsGLLookup_AkadPiutang.data.items[i].data.AROH_NUMBER
		y += z + 'APOH_DATE='+ShowDate(TglApprove)
		y += z + 'AMOUNT='+dsGLLookup_AkadPiutang.data.items[i].data.AMOUNT
		y += z + 'PAID='+ dsGLLookup_AkadPiutang.data.items[i].data.PAID
		y += z + 'REMAIN='+dsGLLookup_AkadPiutang.data.items[i].data.REMAIN
		y += z + 'JENIS_PIU='+dsGLLookup_AkadPiutang.data.items[i].data.JENIS_PIU
		y += z + 'PILIH='+dsGLLookup_AkadPiutang.data.items[i].data.PILIH
		y += z + 'TYPE='+dsGLLookup_AkadPiutang.data.items[i].data.TYPE
		if (i === (dsGLLookup_AkadPiutang.getCount()-1))
		{
			x += y 
		}
		else
		{
			x += y + '##[[]]##'
		}
	}		
	return x;
};