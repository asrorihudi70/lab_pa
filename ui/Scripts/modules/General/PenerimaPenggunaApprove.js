﻿var rowSelectedLookApprove;
var vWinFormEntryApprove;
var now = new Date();
var vPesan;
var vCekPeriode;

function FormApprove(jumlah,type,nomor,Keterangan,tgl) {

    vWinFormEntryApprove = new Ext.Window
	(
		{
		    id: 'FormApprove',
		    title: 'Approve',
		    closeAction: 'hide',
		    closable: false,
		    width: 400,
		    height: 200,
		    border: false,
		    plain: true,
		    resizable: false,
		    layout: 'form',
		    iconCls: 'approve',
		    modal: true,
		    items:
			[
				ItemDlgApprove(jumlah,type,nomor,Keterangan,tgl)
			],
		    listeners:
				{
				    activate: function()
				    
				    {  }
				}
		}
	);
    vWinFormEntryApprove.show();
};


function ItemDlgApprove(jumlah,type,nomor,Keterangan,tgl) 
{	
	if (tgl==undefined)
	{		
		tgl=now;
	}
	
	var PnlLapApprove = new Ext.Panel
	(
		{ 
			id: 'PnlLapApprove',
			fileUpload: true,
			layout: 'form',
			height: '100',
			anchor: '100%',
			bodyStyle: 'padding:15px',
			border: true,
			items: 
			[
				getItemApprove_Nomor(nomor),
				getItemApprove_Tgl(tgl),
				getItemApprove_Jml(jumlah),
				getItemApprove_Notes(Keterangan),
				{
					layout: 'hBox',
					border: false,
					defaults: { margins: '0 5 0 0' },
					style:{'margin-left':'30px','margin-top':'5px'},
					anchor: '94%',
					layoutConfig: 
					{
						padding: '3',
						pack: 'end',
						align: 'middle'
					},
					items: 
					[
						{
							xtype: 'button',
							text: 'Ok',
							width: 70,
							hideLabel: true,
							id: 'btnOkLapApprove',
							handler: function() 
							{
								if (ValidasiEntryNomorApp('Approve') == 1 )
								{										
									if (type==='0')
									{
										PenerimaanApprove(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtNomorApp').getValue(),Ext.get('txtCatatanApp').getValue());
										RefreshDataPenerimaanFilter(false);
									};
									if (type==='2')
									{
										Approve_PenerimaanMhs(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtCatatanApp').getValue())										
									};
									if (type==='3')
									{
										Approve_PenerimaanNonMhs(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtCatatanApp').getValue())
										
									};
									if (type==='4')
									{
										Approve_KasKeluar(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};
									if (type==='5')
									{
										Approve_KasKeluarkecil(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};
									if (type==='6')
									{
										Approve_kbs(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};
									if (type==='7')
									{										
										Approve_RekapSP3D(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};		
									if (type==='8')
									{
										Approve_LPJ(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};									
									if (type==='9')
									{
										Approve_PaguGNRL(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};	
									if (type==='10')
									{
										Approve_viKembaliBDATM(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtCatatanApp').getValue())										
									};
									if (type==='11')
									{
										// Approve_OpenArForm(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtNomorApp').getValue())
										Approve_OpenArForm(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtNomorApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};
									if (type==='12')
									{
										Approve_OpenApForm(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtNomorApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};
									if (type==='13')
									{
										Approve_AdjustArForm(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtNomorApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};
									if (type==='14')
									{
										Approve_AdjustApForm(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtNomorApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};
									vWinFormEntryApprove.close();	
								}
							}
						},
						{
							xtype: 'button',
							text: 'Cancel',
							width: 70,
							hideLabel: true,
							id: 'btnCancelLapApprove',
							handler: function() 
							{
								vWinFormEntryApprove.close();
							}
						}
					]
				}
			]
		}
	);
 
    return PnlLapApprove
};

function getItemApprove_Nomor(nomor)
{
	var items = 			
	{
	    layout:'column',
	    border:false,	
	    items:
		[
			{
				columnWidth:1,
				layout: 'form',
				labelWidth: 65,
				labelAlign:'right',
				readOnly:true,
				border:false,				
				items: 
				[ 
					{
						xtype:'textfield',
						fieldLabel: 'No.Reff. ',
						name: 'txtNomorApp',
						id: 'txtNomorApp',						
						value:nomor,
						readOnly:true,
						anchor:'95%'
					}
				]
			}
		]
	}
	return items;
};

function getItemApprove_Notes(Keterangan)
{
	var items = 			
	{
	    layout:'form',
	    border:false,	
	    items:
		[
			{
				columnWidth:1,
				layout: 'form',
				labelWidth: 65,
				labelAlign:'right',
				border:false,
				items: 
				[ 
					{
						xtype:'textfield',
						fieldLabel: 'Catatan ',
						name: 'txtCatatanApp',
						id: 'txtCatatanApp',
						value:Keterangan,
						anchor:'95%'
					}
				]
			}
		]
	}
	return items;
};

function getItemApprove_Tgl(tgl)
{
	var items = 			
	{
	    layout:'form',
	    border:false,	
	    items:
		[
			{
				columnWidth:1,
				layout: 'form',
				labelWidth: 65,
				labelAlign:'right',
				border:false,
				items: 
				[
					{
						xtype: 'datefield',					
                        fieldLabel: 'Tanggal ',
                        id: 'dtpTanggalApp',
                        name: 'dtpTanggalApp',
                        format: 'd/M/Y',						
						value:tgl,
                        anchor: '50%'
					}
				]
			}
		]
	}
	return items;
};

function getItemApprove_Jml(jumlah)
{
	var items = 			
	{
	    layout:'column',
	    border:false,	
	    items:
		[			
			{
				columnWidth:1,
				layout: 'form',
				labelWidth: 65,
				labelAlign:'right',
				border:false,
				items: 
				[ 
					{
						xtype:'textfield',
						fieldLabel: 'Jumlah ',
						name: 'txtJumlah',
						id: 'txtJumlah',
						anchor:'95%',
						value:jumlah,
						readOnly:true
					}
				]
			}
		]
	}
	return items;
};

function ValidasiEntryNomorApp(modul)
{
	var x = 1;
	if (Ext.get('txtNomorApp').getValue() == '' )
	{
		if (Ext.get('txtNomorApp').getValue() == '')
		{
			ShowPesanWarningApprove('Nomor belum di isi',modul);
			x=0;
		}		
	}
	return x;
};

function ShowPesanWarningApprove(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};