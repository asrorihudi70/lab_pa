var rowSelectedLookApproveAdjust;
var vWinFormEntryApproveAdjust;
var now_AppAdjust = new Date();
var vPesan;
var vCekPeriode;
var tempJumlah=0;
var tempType;

var CurrentFakturTR_ApproveAdjust =
{
    data: Object,
    details: Array,
    row: 0
};



function FormApproveAdjust(jumlah,type,nomor,Keterangan,tgl,custcode) {
	tempJumlah=jumlah;
	tempType=type;

    vWinFormEntryApproveAdjust = new Ext.Window
	(
		{
		    id: 'FormApproveAdjust',
		    title: 'Approve',
		    closeAction: 'hide',
		    closable: false,
		    width: 400,
		    height: 440,
		    border: false,
		    plain: true,
		    resizable: false,
		    layout: 'form',
		    iconCls: 'ApproveAdjust',
		    modal: true,
		    items:
			[
				ItemDlgApproveAdjust(jumlah,type,nomor,Keterangan,tgl,custcode)
			],
		    listeners:
				{
				    activate: function()
				    
				    {  }
				}
		}
	);
    vWinFormEntryApproveAdjust.show();
};


function ItemDlgApproveAdjust(jumlah,type,nomor,Keterangan,tgl,custcode) 
{	
	if (tgl==undefined)
	{		
		tgl=now;
	}
	
	var PnlLapApproveAdjust = new Ext.Panel
	(
		{ 
			id: 'PnlLapApproveAdjust',
			fileUpload: true,
			layout: 'form',
			height: '100',
			anchor: '100%',
			bodyStyle: 'padding:15px',
			border: true,
			items: 
			[
				getItemApproveAdjust_Nomor(nomor),
				getItemApproveAdjust_Tgl(tgl),
				getItemApproveAdjust_Jml(jumlah),
				getItemApproveAdjust_Notes(Keterangan),
				GetGridListFaktur_ApproveAdjust(custcode),
				{
					layout: 'hBox',
					border: false,
					defaults: { margins: '0 5 0 0' },
					style:{'margin-left':'0px','margin-top':'5px'},
					anchor: '94%',
					layoutConfig: 
					{
						padding: '3',
						pack: 'end',
						align: 'middle'
					},
					items: 
					[
						{
							xtype:'displayfield',
							fieldLabel: '',
							name: 'txtjmldataApproveAdjust',
							id: 'txtjmldataApproveAdjust',
							value:'0 Reff',
							width:170
							//anchor:'95%'
						},	
						
						{
							xtype: 'button',
							text: 'Ok',
							width: 70,
							hideLabel: true,
							id: 'btnOkLapApproveAdjust',
							handler: function() 
							{
								var Noreff='';
								var Amount=0;
								var jml=0;
								var selected=0;
								var jmlselected=0;
								if (Ext.getCmp('ChkSelectFaktur_AppAdjustArForm').getValue()==true || Ext.getCmp('ChkSelectFaktur_AppAdjustArForm').getValue()=='true'){

									if (CekArrKosong_ApproveAdjust()==1){

										for(var i = 0 ; i < dsTRListFaktur_ApproveAdjust.getCount();i++)
											{
												
												if (dsTRListFaktur_ApproveAdjust.data.items[i].data.ID==true || dsTRListFaktur_ApproveAdjust.data.items[i].data.ID==1)
												{
													//alert(dsTRListImport_OpenArForm.data.items[i].data.NO_TRANSAKSI);
													jmlselected+=1;
													
													if (jmlselected==1){

														Noreff=dsTRListFaktur_ApproveAdjust.data.items[i].data.reff;
														Amount=dsTRListFaktur_ApproveAdjust.data.items[i].data.jumlah;
														//alert(notrans);
													} else if (jmlselected>1){
														
														Noreff=Noreff+'#'+dsTRListFaktur_ApproveAdjust.data.items[i].data.reff;
														Amount=Amount+'#'+dsTRListFaktur_ApproveAdjust.data.items[i].data.jumlah;
														
													}

													selected=i;
													jml=jml+1;
		
												}
													
											}
											
											// if (jmlselected==1){
												// RefreshDataImportFakturAmount_ApproveAdjust(kdkasirtrans,selectCustomer_OpenArForm,ShowDate(Ext.getCmp('DtpAwalTransaksi_OpenArForm').getValue()),ShowDate(Ext.getCmp('DtpAkhirTransaksi_OpenArForm').getValue()),notrans,tgltrans,shiftrans,chkAll,chk1,chk2,chk3);	
											// } else{
												setFrmLookUpListFakturAmount_AdjustArForm(Noreff,jml,type);	
											// }


									} else{
										ShowPesanWarningApproveAdjust("Pilih Data"," List Faktur")
									}

								} else{
									
									//approve
									if (ValidasiEntryNomorApp_Adjust('ApproveAdjust') == 1 )
									{										
										
										if (type==='13')
										{
											Approve_AdjustArForm(Ext.get('dtpTanggalAppAdjust').getValue(),Ext.get('txtNomorAppAdjust').getValue(),Ext.get('txtCatatanAppAdjust').getValue(),Ext.getCmp('txtJumlahAppAdjust').getValue())
										};
										if (type==='14')
										{
											Approve_AdjustApForm(Ext.get('dtpTanggalAppAdjust').getValue(),Ext.get('txtNomorAppAdjust').getValue(),Ext.get('txtCatatanAppAdjust').getValue(),Ext.getCmp('txtJumlahAppAdjust').getValue())
										};
										vWinFormEntryApproveAdjust.close();	
									}

								}
									
								/*if (ValidasiEntryNomorApp_Adjust('ApproveAdjust') == 1 )
								{										
									
									if (type==='13')
									{
										ApproveAdjust_AdjustArForm(Ext.get('dtpTanggalAppAdjust').getValue(),Ext.get('txtNomorAppAdjust').getValue(),Ext.get('txtCatatanAppAdjust').getValue())
									};
									if (type==='14')
									{
										ApproveAdjust_AdjustApForm(Ext.get('dtpTanggalAppAdjust').getValue(),Ext.get('txtNomorAppAdjust').getValue(),Ext.get('txtCatatanAppAdjust').getValue())
									};
									vWinFormEntryApproveAdjust.close();	
								}*/
							}
						},
						{
							xtype: 'button',
							text: 'Cancel',
							width: 70,
							hideLabel: true,
							id: 'btnCancelLapApproveAdjust',
							handler: function() 
							{
								vWinFormEntryApproveAdjust.close();
							}
						}
					]
				}
			]
		}
	);
 
    return PnlLapApproveAdjust
};

function getItemApproveAdjust_Nomor(nomor)
{
	var items = 			
	{
	    layout:'column',
	    border:false,	
	    items:
		[
			{
				columnWidth:1,
				layout: 'form',
				labelWidth: 65,
				labelAlign:'right',
				readOnly:true,
				border:false,				
				items: 
				[ 
					{
						xtype:'textfield',
						fieldLabel: 'No.Reff. ',
						name: 'txtNomorAppAdjust',
						id: 'txtNomorAppAdjust',						
						value:nomor,
						readOnly:true,
						anchor:'95%'
					}
				]
			}
		]
	}
	return items;
};

function getItemApproveAdjust_Notes(Keterangan)
{
	var items = 			
	{
	    layout:'form',
	    border:false,	
	    items:
		[
			{
				columnWidth:1,
				layout: 'form',
				labelWidth: 65,
				labelAlign:'right',
				border:false,
				items: 
				[ 
					{
						xtype:'textfield',
						fieldLabel: 'Catatan ',
						name: 'txtCatatanAppAdjust',
						id: 'txtCatatanAppAdjust',
						value:Keterangan,
						anchor:'95%'
					},
					{
						xtype:'checkbox',
						boxLabel: 'Pilih Faktur',
						name: 'ChkSelectFaktur_AppAdjustArForm',
						id: 'ChkSelectFaktur_AppAdjustArForm',
						disabled:false,
						checked: true,
						anchor:'95%',
						handler: function() 
						{
							if (this.getValue()===true)
							{
								Ext.getCmp('GListFaktur_ApproveAdjust').enable();
								
							}
							else
							{
								Ext.getCmp('GListFaktur_ApproveAdjust').disable();					
							};
						}
					}
				]
			}
		]
	}
	return items;
};

function getItemApproveAdjust_Tgl(tgl)
{
	var items = 			
	{
	    layout:'form',
	    border:false,	
	    items:
		[
			{
				columnWidth:1,
				layout: 'form',
				labelWidth: 65,
				labelAlign:'right',
				border:false,
				items: 
				[
					{
						xtype: 'datefield',					
                        fieldLabel: 'Tanggal ',
                        id: 'dtpTanggalAppAdjust',
                        name: 'dtpTanggalAppAdjust',
                        format: 'd/M/Y',						
						value:tgl,
                        anchor: '50%'
					}
				]
			}
		]
	}
	return items;
};

function getItemApproveAdjust_Jml(jumlah)
{
	var items = 			
	{
	    layout:'column',
	    border:false,	
	    items:
		[			
			{
				columnWidth:1,
				layout: 'form',
				labelWidth: 65,
				labelAlign:'right',
				border:false,
				items: 
				[ 
					{
						xtype:'textfield',
						fieldLabel: 'Jumlah ',
						name: 'txtJumlahAppAdjust',
						id: 'txtJumlahAppAdjust',
						anchor:'95%',
						value:jumlah,
						readOnly:true
					}
				]
			}
		]
	}
	return items;
};

function ValidasiEntryNomorApp_Adjust(modul)
{
	var x = 1;
	if (Ext.get('txtNomorAppAdjust').getValue() == '' )
	{
		if (Ext.get('txtNomorAppAdjust').getValue() == '')
		{
			ShowPesanWarningApproveAdjust('Nomor belum di isi',modul);
			x=0;
		}		
	}
	return x;
};

function ShowPesanWarningApproveAdjust(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};

var rowSelectedFaktur_ApproveAdjust;
var dsTRListFaktur_ApproveAdjust;
var fldDetail_Faktur =['ID','REFF','JUMLAH','STATUS']  
	
dsTRListFaktur_ApproveAdjust = new WebApp.DataStore({ fields: fldDetail_Faktur })

function RefreshDataImportFaktur_ApproveAdjustAR(cust)
{			
	Ext.Ajax.request({
		url: baseURL + "index.php/keuangan/functionGeneral/getListFakturAR",
		params: {code:cust},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);
			loadMask.hide();
			if (cst.success === true) 
			{
				dsTRListFaktur_ApproveAdjust.removeAll();
				var recs=[],
				recType=dsTRListFaktur_ApproveAdjust.recordType;
				for(var i=0; i<cst.totalrecords; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsTRListFaktur_ApproveAdjust.add(recs);
				GridListFaktur_ApproveAdjust.getView().refresh();
			} else{
				Ext.Msg('Gagal membaca list detail adjustment!' , 'Simpan Data');
			}
		}
	});
};

function RefreshDataImportFaktur_ApproveAdjustAP(vend)
{			
	Ext.Ajax.request({
		url: baseURL + "index.php/keuangan/functionGeneral/getListFakturAP",
		params: {code:vend},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) 
		{
			var cst = Ext.decode(o.responseText);
			loadMask.hide();
			if (cst.success === true) 
			{
				dsTRListFaktur_ApproveAdjust.removeAll();
				var recs=[],
				recType=dsTRListFaktur_ApproveAdjust.recordType;
				for(var i=0; i<cst.totalrecords; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsTRListFaktur_ApproveAdjust.add(recs);
				GridListFaktur_ApproveAdjust.getView().refresh();
			} else{
				Ext.Msg('Gagal membaca list detail adjustment!' , 'Simpan Data');
			}
		}
	});
};

function RefreshDataImportFakturAmount_ApproveAdjust(REFF,JML,id,prm)
{			
	// dsTRListFakturAmount_ApproveAdjust.load
	// (
		// { 
			// params: 
			// { 	
				// Skip: 0,
				// Take: 1000,
				// Sort: 'REFF',
				// Sortdir: 'ASC',
				// target:'viviewListFakturAmountApproveAdjust',
				// param: REFF+'#@#'+id					
			// }
		// }
	// );
	// return dsTRListFakturAmount_ApproveAdjust;
	Ext.Ajax.request({
		url: baseURL + "index.php/keuangan/functionGeneral/getListFakturAmountApproveAR",
		params: {
			reff:REFF,
			type:id,
			jml:JML
		},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);
			loadMask.hide();
			if (cst.success === true) 
			{
				dsTRListFakturAmount_ApproveAdjust.removeAll();
				var recs=[],
				recType=dsTRListFakturAmount_ApproveAdjust.recordType;
				for(var i=0; i<cst.totalrecords; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsTRListFakturAmount_ApproveAdjust.add(recs);
				GridListFakturAmount_ApproveAdjust.getView().refresh();
			} else{
				Ext.Msg('Gagal membaca list detail adjustment!' , 'Simpan Data');
			}
		}
	});
};

function RefreshDataImportFakturAmount2_ApproveAdjust(REFF,JML,id,prm)
{			
	dsTRListFakturAmount_ApproveAdjust.load
	(
		{ 
			params: 
			{ 	
				Skip: 0,
				Take: 1000,
				Sort: 'REFF',
				Sortdir: 'ASC',
				target:'viviewListFakturAmountApproveAdjust',
				param: REFF+'#@#'+id			
			}
		}
	);
return dsTRListFakturAmount_ApproveAdjust;
};

var fm = Ext.form;
function GetGridListFaktur_ApproveAdjust(custcode){

	var chkListFaktur_ApproveAdjust = new Ext.grid.CheckColumn({
        header: "",
        align:'center',
        dataIndex: 'ID',
        //value:true,
        width: 30,
        hidden:false,
    });
	
    GridListFaktur_ApproveAdjust = new Ext.grid.EditorGridPanel
	(
		{	
			id:'GListFaktur_ApproveAdjust',
			stripeRows: true,
			xtype: 'editorgrid',
			store: dsTRListFaktur_ApproveAdjust,
			//anchor: '100% 100%',
			width:360,
			columnLines:true,
			border:true,
			autoScroll:true,
			height:235,
			//plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			//sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedFaktur_ApproveAdjust = dsTRListFaktur_ApproveAdjust.getAt(row);
						}
					}
				}
			),
			listeners:
			{
				'afterrender': function()
				{	
					if (tempType=='13'){
						RefreshDataImportFaktur_ApproveAdjustAR(custcode)

					} else if (tempType=='14'){
						RefreshDataImportFaktur_ApproveAdjustAP(custcode)

					}
					
					// loadMask.show();

					// this.store.on("load", function()
						// {
							// Ext.getCmp('txtjmldataApproveAdjust').setValue(dsTRListFaktur_ApproveAdjust.getCount() + ' Reff');
						// } 
					// );
					// this.store.on("datachanged", function()
						// {
							// if (dsTRListFaktur_ApproveAdjust.getCount()==0){
								// Ext.getCmp('txtjmldataApproveAdjust').setValue('0 Reff');
								// loadMask.hide();
								// ShowPesanWarningApproveAdjust('Tidak ada data','Adjust AR')

							// } else if (dsTRListFaktur_ApproveAdjust.getCount()>0){
								// Ext.getCmp('txtjmldataApproveAdjust').setValue(dsTRListFaktur_ApproveAdjust.getCount() + 'Reff');
								// loadMask.hide();

							// }		
						// }
					// );

				}
			},
			colModel: new Ext.grid.ColumnModel			
			//cm: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					chkListFaktur_ApproveAdjust,
					{
						id: 'ColNoref_ApproveAdjust',
						header: 'Referensi',
						dataIndex: 'reff',
						sortable: true,	
						align:'left',		
						width :125,
						//filter: {}
					}, 
					{
						id: 'Coljml_ApproveAdjust',
						header: "Jumlah (Rp)",
						align:'right',
						dataIndex: 'jumlah',
						renderer: function(v, params, record) 
						{
							
							var str = "<div style='white-space:normal;padding:2px 15px 2px 2px;'>" + formatCurrency(record.data.jumlah) + "</div>";
							return str;
						},
						width :125,
						//filter: {}
					},
				]
			),
			plugins: chkListFaktur_ApproveAdjust,
			viewConfig: {forceFit: true} 			
		}
	);

	return GridListFaktur_ApproveAdjust;
};

function CekArrKosong_ApproveAdjust()
{
	var x=0;
	for(var i = 0 ; i < dsTRListFaktur_ApproveAdjust.getCount();i++)
	{
		if (dsTRListFaktur_ApproveAdjust.data.items[i].data.ID!=0 || dsTRListFaktur_ApproveAdjust.data.items[i].data.ID!=false){
			x=1;
		}

	}		
	return x;
};




////////////////////////////////
var setLookUpsListFakturAmount_ApproveAdjust;
var nowListFakturAmount_AdjustArForm = new Date();
var NMformListFakturAmount_AdjustArForm='';
function setFrmLookUpListFakturAmount_AdjustArForm(Noreff,jml,type)
{
	if (type === '13'){
		NMformListFakturAmount_AdjustArForm='Adjustment AR Posting';
	}else{
		NMformListFakturAmount_AdjustArForm='Adjustment AP Posting';
	}
    var lebar = 800;
    setLookUpsListFakturAmount_ApproveAdjust = new Ext.Window
    (
        {
            id: 'setLookUpsListFakturAmount_ApproveAdjust',
            title: NMformListFakturAmount_AdjustArForm, 
            closeAction: 'destroy',
            // y:90,
            width: lebar,
            height: 300,
            resizable:false,
            border: false,
            plain: true,
            layout: 'fit',
            iconCls: 'Penerimaan',
            modal: true,
            items: getFormEntryListFakturAmount_AdjustArForm(lebar,Noreff,jml,type),
            listeners:
            {
                activate: function() 
                {
                },
                afterShow: function() 
                { 
                    this.activate(); 
					setLookUpsListFakturAmount_ApproveAdjust.close();					
                },
                deactivate: function()
                {
                    					
                },
                'afterrender': function() 
				{           
					RefreshDataImportFakturAmount_ApproveAdjust(Noreff,jml,type,'');		 
				}
          
            }
        }
    );        
    setLookUpsListFakturAmount_ApproveAdjust.show();
}

function getFormEntryListFakturAmount_AdjustArForm(lebar,Noreff,jml,type)
{
    var pnlFormListFakturAmount_AdjustArForm = new Ext.FormPanel
    (
        {
            region: 'center',
            padding: '5px',
            layout: 'fit',
            border:false,
            items: 
			[
                {
                    xtype: 'panel',
                    layout: 'form',
                    height: 440,//195,
                    padding: '5px',
                    border:false,
                    labelWidth: 70,
                    items: 
					[
						{
							xtype: 'compositefield',
							fieldLabel: 'Nomor :',
							anchor: '100%',
							labelSeparator: '',
							name: 'compsxx_ApproveAdjust',
							id: 'compsxx_ApproveAdjust',
							items: 
							[
								{
									xtype:'textfield',
									name: 'txtfilterNo_ApproveAdjust',
									id: 'txtfilterNo_ApproveAdjust',
									readOnly:false,
									width:150,
								},
								{
									xtype:'displayfield',
									id:'dsLblTglfilter_ApproveAdjust',
									width:70,
									value:'Tgl Posting :'
								},
								{
									xtype: 'datefield',
			                        id: 'dtpfilterTanggal_ApproveAdjust',
			                        name: 'dtpfilterTanggal_ApproveAdjust',
			                        format: 'd/M/Y',
									value:now_AppAdjust,
			                        width:125
								}
							]
						},	
								
						GetGridListFakturAmount_ApproveAdjust(Noreff,jml),	
						{
							layout: 'hBox',
							border: false,
							defaults: { margins: '0 5 0 0' },
							style:{'margin-left':'0px','margin-top':'5px'},
							anchor: '100%',
							items: 
							[
								{
									xtype: 'button',
									text: 'Ok',
									width: 70,
									hideLabel: true,
									id: 'btnOkListFakturAmount_AdjustArForm',
									handler: function() 
									{
										//alert(Ext.getCmp('txtJumlahAppAdjust').getValue())
										var totalx=GetAngkaAdjustArForm(Ext.getCmp('txtJumlahAppAdjust').getValue()).replace(",00","");
										var paidx=CalcTotalPaidOnly_ApproveAdjust();//GetAngkaAdjustArForm(Ext.getCmp('txttotalPaid_ApproveAdjust').getValue()).replace(",00","");

										// if (Ext.getCmp('txtfilterNo_ApproveAdjust').getValue()!=''){

											if (Ext.get('dtpfilterTanggal_ApproveAdjust').getValue()!=''){

												if (CekPaidKosong_ApproveAdjust()==1){
													//alert( totalx+ ' - '+paidx)

													if (cekTotalPaid_ApprovedAdjust(totalx,paidx)==1){

														//alert('good')
														loadMask.show();
				 										if (type==='13')
														{
															Approve_AdjustArForm(Ext.get('dtpTanggalAppAdjust').getValue(),Ext.get('txtNomorAppAdjust').getValue(),Ext.get('txtCatatanAppAdjust').getValue(),Ext.getCmp('txtJumlahAppAdjust').getValue(),Ext.getCmp('txtfilterNo_ApproveAdjust').getValue(),Ext.get('dtpfilterTanggal_ApproveAdjust').getValue())
														};
														if (type==='14')
														{
															Approve_AdjustApForm(Ext.get('dtpTanggalAppAdjust').getValue(),Ext.get('txtNomorAppAdjust').getValue(),Ext.get('txtCatatanAppAdjust').getValue(),Ext.getCmp('txtJumlahAppAdjust').getValue(),Ext.getCmp('txtfilterNo_ApproveAdjust').getValue(),Ext.get('dtpfilterTanggal_ApproveAdjust').getValue())
														};
														vWinFormEntryApproveAdjust.close();	
			 											setLookUpsListFakturAmount_ApproveAdjust.close();

													} else{
														ShowPesanWarningApproveAdjust(" Paid tidak sesuai"," Adjust AR");
													}

													

												} else{
													ShowPesanWarningApproveAdjust(" Paid tidak boleh kosong"," Adjust AR");
												}

												

											} else{

												ShowPesanWarningApproveAdjust(" Tgl Posting","Simpan Data")
											}

										// } else{
											// ShowPesanWarningApproveAdjust(" Nomor Belum diisi","Simpan Data")
										// }
 										
									}
								},
								{
									xtype: 'button',
									text: 'Cancel',
									width: 70,
									hideLabel: true,
									id: 'btnCanceldataListFakturAmount_AdjustArForm',
									handler: function() 
									{
										dsTRListFakturAmount_ApproveAdjust.removeAll();

										setLookUpsListFakturAmount_ApproveAdjust.close();
									}
								},
								{
									xtype:'displayfield',
									id:'dsLblTotal_ApproveAdjust',
									width:330,
									value:''
								},
								{
									xtype:'textfield',
									name: 'txttotalAmount_ApproveAdjust',
									id: 'txttotalAmount_ApproveAdjust',
									readOnly:true,
									value:'0',
									style:
									{	
										'text-align':'right',
										'font-weight':'bold'
									},
									width:90
								},
								{
									xtype:'textfield',
									name: 'txttotalRemain_ApproveAdjust',
									id: 'txttotalRemain_ApproveAdjust',
									readOnly:true,
									value:'0',
									style:
									{	
										'text-align':'right',
										'font-weight':'bold'
									},
									width:90
								},
								{
									xtype:'textfield',
									name: 'txttotalPaid_ApproveAdjust',
									id: 'txttotalPaid_ApproveAdjust',
									readOnly:true,
									value:'0',
									style:
									{	
										'text-align':'right',
										'font-weight':'bold'
									},
									width:90
								}
								
							]
						}
                    ]
                }
            ]            
        }
    );
        
    return pnlFormListFakturAmount_AdjustArForm;     
}

var fm = Ext.form;
function GetGridListFakturAmount_ApproveAdjust(Noreff,jml){
	var Field = 
	[
		'REFF','REFF_DATE','NOTES','AMOUNT_ADD','AMOUNT','PAID'
	]
    dsTRListFakturAmount_ApproveAdjust = new WebApp.DataStore({ fields: Field });
	
	var chkListFakturAmount_ApproveAdjust = new Ext.grid.CheckColumn({
        header: "",
        align:'center',
        dataIndex: 'ID',
        //value:true,
        id:'chkSelect_ApproveAdjust',
        width: 30,
        hidden:false,
        onMouseDown: function(e, t)
		{
			var index = this.grid.getView().findRowIndex(t);			  
			
			   if(t.className && t.className.indexOf('x-grid3-cc-'+this.id) != -1)
			    {
					e.stopEvent();
					var index = this.grid.getView().findRowIndex(t);
					var record = this.grid.store.getAt(index);
					record.set(this.dataIndex, !record.data[this.dataIndex]);
					rowData = record.data;//save row records. will be used in the ajax request
					CalcTotalItemChecked_ApproveAdjust();	
				}						
			
		}
    });
	
    GridListFakturAmount_ApproveAdjust = new Ext.grid.EditorGridPanel
	(
		{	
			id:'GListFakturAmount_ApproveAdjust',
			stripeRows: true,
			xtype: 'editorgrid',
			store: dsTRListFakturAmount_ApproveAdjust,
			width:770,
			columnLines:true,
			autoScroll:true,
			height:200,
			sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedFakturAmount_ApproveAdjust = dsTRListFakturAmount_ApproveAdjust.getAt(row);
							//tmpRowSelectListFakturAmount_OpenArForm=rowSelectedImport_OpenArForm.data.NO_TRANSAKSI;
							//alert(tmpRowSelectListFakturAmount_OpenArForm)
							CurrentFakturTR_ApproveAdjust.row = rec;
							CurrentFakturTR_ApproveAdjust.data = rowSelectedFakturAmount_ApproveAdjust;
						}
					}
				}
			),
			listeners:
			{
				'afterrender': function()
				{
					// if (tempType=='13'){
						// RefreshDataImportFakturAmount_ApproveAdjust(Noreff,jml,13,'');

					// } else{
						// RefreshDataImportFakturAmount2_ApproveAdjust(Noreff,jml,14,'');

					// }
					RefreshDataImportFakturAmount_ApproveAdjust(Noreff,jml,tempType,'');
					
					loadMask.show();

					this.store.on("load", function()
						{
							CalcTotalItemChecked_ApproveAdjust();
						} 
					);
					this.store.on("datachanged", function()
						{
							if (dsTRListFakturAmount_ApproveAdjust.getCount()==0){
								loadMask.hide();
								ShowPesanWarningApproveAdjust('Tidak ada data','List Open AR')

							} else if (dsTRListFakturAmount_ApproveAdjust.getCount()>0){
								CalcTotalItemChecked_ApproveAdjust();
								loadMask.hide();

							}		
						}
					);

				}
			},
			colModel: new Ext.grid.ColumnModel			
			//cm: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					chkListFakturAmount_ApproveAdjust,
					
					{
						id: 'ColNoReffAmount_ApproveAdjust',
						header: 'Referensi',
						dataIndex: 'REFF',
						sortable: true,	
						align:'left',		
						width :100,
						//filter: {}
					}, 
					{
						id: 'ColNoReffNotes_ApproveAdjust',
						header: 'Deskripsi',
						dataIndex: 'NOTES',
						sortable: true,	
						align:'left',		
						width :150,
						renderer: function(v, params, record) 
						{
							
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px;'>" + record.data.NOTES + "</div>";
							return str;
						},
						//filter: {}
					},
					{
						id: 'ColNoRefftgl_ApproveAdjust',
						header: 'Tanggal',
						dataIndex: 'REFF_DATE',
						sortable: true,	
						align:'left',		
						width :100,
						//filter: {}
					},
					{
						id: 'ColjmlAmount_ApproveAdjust',
						header: "Adjustment",
						align:'right',
						dataIndex: 'AMOUNT_ADD',
						renderer: function(v, params, record) 
						{
							
							var str = "<div style='white-space:normal;padding:2px 15px 2px 2px;'>" + formatCurrency(record.data.AMOUNT_ADD) + "</div>";
							return str;
						},
						width :100,
						//filter: {}
					},
					{
						id: 'ColAmount2_ApproveAdjust',
						header: "Amount",
						align:'right',
						dataIndex: 'AMOUNT',
						renderer: function(v, params, record) 
						{
							
							var str = "<div style='white-space:normal;padding:2px 15px 2px 2px;'>" + formatCurrency(record.data.AMOUNT) + "</div>";
							return str;
						},
						width :100,
						//filter: {}
					},
					{
						id: 'ColRemain_ApproveAdjust',
						header: "Remain",
						align:'right',
						dataIndex: 'AMOUNT',
						renderer: function(v, params, record) 
						{
							
							var str = "<div style='white-space:normal;padding:2px 15px 2px 2px;'>" + formatCurrency(record.data.REMAIN) + "</div>";
							return str;
						},
						width :100,
						//filter: {}
					},
					{
						id: 'ColPaid_ApproveAdjust',
						header: "Paid",
						align:'right',
						dataIndex: 'PAID',
						align: 'right',
						editor: new Ext.form.NumberField
						(
							{
								id:'fnumPaid_ApproveAdjust',
								allowBlank: false,
								enableKeyEvents : true,
								listeners: 
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() == 13) 
										{
											CalcTotalItemChecked_ApproveAdjust();	
										}
									},
									'blur' : function()
									{
										CalcTotalItemChecked_ApproveAdjust();	
									},
									'change': function(){	
									        CalcTotalItemChecked_ApproveAdjust();				
									},
									'keyup': {
									       fn: function(obj) {	
									       CalcTotalItemChecked_ApproveAdjust();			
									       		
									      },
									      delay: 1000
									}
								}
							}
						),
						renderer: function(v, params, record) 
						{
							
							var str = "<div style='white-space:normal;padding:2px 15px 2px 2px;'>" + formatCurrency(record.data.PAID) + "</div>";
							return str;
						},
						width :100,
						//filter: {}
					},
					{
						id: 'ColPaid_ApproveAdjust',
						header: "LINE",
						align:'right',
						dataIndex: 'LINE',
						hidden: true,
					}
				]
			),
			plugins: chkListFakturAmount_ApproveAdjust,
			viewConfig: {forceFit: true} 			
		}
	);

	return GridListFakturAmount_ApproveAdjust;
};

function CalcTotalItem_ApproveAdjust()
{
    var total=Ext.num(0);
	var nilai=Ext.num(0);
	var total2=Ext.num(0);
	var nilai2=Ext.num(0);
	var total3=Ext.num(0);
	var nilai3=Ext.num(0);

	for(var i=0;i < dsTRListFakturAmount_ApproveAdjust.getCount();i++)
	{
		nilai=Ext.num(parseFloat(dsTRListFakturAmount_ApproveAdjust.data.items[i].data.AMOUNT));
		nilai2=Ext.num(parseFloat(dsTRListFakturAmount_ApproveAdjust.data.items[i].data.REMAIN));
		nilai3=Ext.num(parseFloat(dsTRListFakturAmount_ApproveAdjust.data.items[i].data.PAID));
		// total += getNumber(nilai);
		total +=  nilai
		total2 +=  nilai2
		total3 +=  nilai3
	}	
	Ext.getCmp('txttotalAmount_ApproveAdjust').setValue(formatCurrency(total));
	Ext.getCmp('txttotalRemain_ApproveAdjust').setValue(formatCurrency(total2));
	Ext.getCmp('txttotalPaid_ApproveAdjust').setValue(formatCurrency(total3));
};

function CalcTotalItemChecked_ApproveAdjust()
{
    var total=Ext.num(0);
	var nilai=Ext.num(0);
	var total2=Ext.num(0);
	var nilai2=Ext.num(0);
	var total3=Ext.num(0);
	var nilai3=Ext.num(0);

	for(var i=0;i < dsTRListFakturAmount_ApproveAdjust.getCount();i++)
	{
		if (dsTRListFakturAmount_ApproveAdjust.data.items[i].data.ID==true || dsTRListFakturAmount_ApproveAdjust.data.items[i].data.ID=="true"){

			nilai=Ext.num(parseFloat(dsTRListFakturAmount_ApproveAdjust.data.items[i].data.AMOUNT));
			nilai2=Ext.num(parseFloat(dsTRListFakturAmount_ApproveAdjust.data.items[i].data.REMAIN));
			nilai3=Ext.num(parseFloat(dsTRListFakturAmount_ApproveAdjust.data.items[i].data.PAID));
			// total += getNumber(nilai);
			total +=  nilai
			total2 +=  nilai2
			total3 +=  nilai3

		}
		
	}	
	Ext.getCmp('txttotalAmount_ApproveAdjust').setValue(formatCurrency(total));
	Ext.getCmp('txttotalRemain_ApproveAdjust').setValue(formatCurrency(total2));
	Ext.getCmp('txttotalPaid_ApproveAdjust').setValue(formatCurrency(total3));
};

function CalcTotalPaidOnly_ApproveAdjust()
{
    var total=Ext.num(0);

	for(var i=0;i < dsTRListFakturAmount_ApproveAdjust.getCount();i++)
	{
		if (dsTRListFakturAmount_ApproveAdjust.data.items[i].data.ID==true || dsTRListFakturAmount_ApproveAdjust.data.items[i].data.ID=="true"){

			nilai=Ext.num(parseFloat(dsTRListFakturAmount_ApproveAdjust.data.items[i].data.PAID));
			total +=  nilai

		}
		
	}	
	return total
};

function CekPaidKosong_ApproveAdjust()
{
	var x=1;
	for(var i=0;i < dsTRListFakturAmount_ApproveAdjust.getCount();i++)
	{
		if (dsTRListFakturAmount_ApproveAdjust.data.items[i].data.ID==true || dsTRListFakturAmount_ApproveAdjust.data.items[i].data.ID=="true"){

			if(Ext.num(parseFloat(dsTRListFakturAmount_ApproveAdjust.data.items[i].data.PAID)) <= 0) {
				x=0;
			}

			
		}
		
	}
	return x	
};

function cekTotalPaid_ApprovedAdjust(total,paid){
	var x=1;
	if (total != paid){
		x=0;
	}
	return x
}

function hitung_ApproveAdjust(i)
{
    //var total=Ext.num(0);
	var sisa=Ext.num(0);
	if (tempJumlah > 0){
		//dsDTLTRListItem_OpenArForm.data.items[i].data.VALUE
		//alert(dsTRListFakturAmount_ApproveAdjust.data.items[i].data.AMOUNT+' - '+tempJumlah);
		//alert(tempJumlah);
		if (Ext.num(parseFloat(dsTRListFakturAmount_ApproveAdjust.data.items[i].data.AMOUNT)) >= tempJumlah){
			Ext.num(parseFloat(dsTRListFakturAmount_ApproveAdjust.data.items[i].data.PAID))+=tempJumlah;
			tempJumlah=0;
			sisa=0;
			
		} else if (Ext.num(parseFloat(dsTRListFakturAmount_ApproveAdjust.data.items[i].AMOUNT)) < tempJumlah){

			sisa=(tempJumlah - Ext.num(parseFloat(dsTRListFakturAmount_ApproveAdjust.data.items[i].data.AMOUNT)));
			Ext.num(parseFloat(dsTRListFakturAmount_ApproveAdjust.data.items[i].data.PAID))+=sisa;
			tempJumlah=(tempJumlah - sisa);
		
		}
	}
	CalcTotalItemChecked_ApproveAdjust();

};