
var varViewReportGenRequest='viRptRequestCM_RG';
var varViewReportGenApproval='viRptApproval_RG';
var varViewReportGenSchCM='viRptScheduleCM_RG';
var varViewReportGenWOCM='viRptWorkOrderCM_RG';
var varViewReportGenResultCM='viRptJobCloseCM_RG';

var varViewReportGenSchPM='viRptSchedulePM_RG';
var varViewReportGenWOPM='viRptWorkOrderPM_RG';
var varViewReportGenResultPM='viRptJobClosePM_RG';

var varTempViewReportGen;
var varTempTitleReportGen='';

var dsSetReportGen;
var dsSetPilihReportGen;
var dsTempHapusReportGen;

var varSelectAddReportGen;
var varSelectDelReportGen;


var mRecReportGen = Ext.data.Record.create
(
    [
       {name: 'COLUMN_NAME', mapping:'COLUMN_NAME'}
    ]
);
	
CurrentPage.page = getPanelSetReportGen(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelSetReportGen(mod_id) 
{
    var Field = ['COLUMN_NAME']
    dsSetReportGen = new WebApp.DataStore({ fields: Field });
	dsSetPilihReportGen = new WebApp.DataStore({ fields: Field });
	dsTempHapusReportGen= new WebApp.DataStore({ fields: Field });
	
	  var listViewReportGen = new Ext.ListView
	  ({
        store: dsSetReportGen,
        multiSelect: true,
		border:true,
		anchor: '100% 100%',
		height:410,
        emptyText: '',
        reserveScrollOffset: true,
		listeners: 
		{
			selectionchange: 
			{
				fn: function(dv,nodes)
				{
					varSelectAddReportGen=dv;
				}
			}
        },
        columns: 
		[
			{
				header: nmTitleNamaKolomReportGen,
				width: 225,
				dataIndex: 'COLUMN_NAME'
			}
		]
    });
	
	 var panellistViewReportGen = new Ext.Panel
	 (
	 {
        id:'panellistViewReportGen',
       // width:185,
	    anchor: '100% 100%',
        height:410,
        collapsible:true,
        title:'',
        items: listViewReportGen
    });
	
	 var listViewReportGen2 = new Ext.ListView
	  ({
        store: dsSetPilihReportGen,
        multiSelect: true,
		border:true,
		anchor: '100% 92.3%',
		height:410,
        emptyText: '',
        reserveScrollOffset: true,
		listeners: 
		{
			selectionchange: 
			{
				fn: function(dv,nodes)
				{
					varSelectAddReportGen=dv;
				}
			}
        },
        columns: 
		[
			{
				header: nmTitleNamaKolomReportGen,
				width: 225,
				dataIndex: 'COLUMN_NAME'
			}
		]
    });
	
	var panellistViewReportGen2 = new Ext.Panel
	 (
	 {
        id:'panellistViewReportGen2',
		anchor: '100% 92.3%',
        //width:225,
        height:410,
        collapsible:true,
        title:'',
        items: listViewReportGen2
    });
	
	var treeReportGen= new Ext.tree.TreePanel
	(
		{
			autoScroll: true,
			split: true,
			height:410,
			loader: new Ext.tree.TreeLoader(),
			listeners: 
			{
				click: function(n) 
				{
					dsSetPilihReportGen.removeAll();
					if (n.attributes.id === 'CM1')
					{
						dsSetReportGen.load
						(
							{
								params:
								{
									Skip: 0,
									Take: 1000,
									//Sort: 'COLUMN_NAME',
                                                                        Sort: 'column_name',
									Sortdir: 'ASC',
									target: 'ViewGetFieldRepGenCMRequest',
									param: varViewReportGenRequest
								}
							}
						);
						varTempViewReportGen=varViewReportGenRequest;
						varTempTitleReportGen=nmTreeReqCMReportGenView
					}
					else if (n.attributes.id === 'CM2')
					{
						dsSetReportGen.load
						(
							{
								params:
								{
									Skip: 0,
									Take: 1000,
									//Sort: 'COLUMN_NAME',
                                                                        Sort: 'column_name',
									Sortdir: 'ASC',
									target: 'ViewGetFieldRepGenCMRequest',
									param: varViewReportGenApproval
								}
							}
						);
						varTempViewReportGen=varViewReportGenApproval;
						varTempTitleReportGen=nmTreeAppCMReportGenView;
					}
					else if (n.attributes.id === 'CM3')
					{
						dsSetReportGen.load
						(
							{
								params:
								{
									Skip: 0,
									Take: 1000,
									//Sort: 'COLUMN_NAME',
                                                                        Sort: 'column_name',
									Sortdir: 'ASC',
									target: 'ViewGetFieldRepGenCMRequest',
									param: varViewReportGenSchCM
								}
							}
						);
						varTempViewReportGen=varViewReportGenSchCM;
						varTempTitleReportGen=nmTreeSchCMReportGenView
					}
					else if (n.attributes.id === 'CM4')
					{
						dsSetReportGen.load
						(
							{
								params:
								{
									Skip: 0,
									Take: 1000,
									//Sort: 'COLUMN_NAME',
                                                                        Sort: 'column_name',
									Sortdir: 'ASC',
									target: 'ViewGetFieldRepGenCMRequest',
									param:  varViewReportGenWOCM
								}
							}
						);
						varTempViewReportGen=varViewReportGenWOCM;
						varTempTitleReportGen=nmTreeWOCMReportGenView
					}
					else if (n.attributes.id === 'CM5')
					{
						dsSetReportGen.load
						(
							{
								params:
								{
									Skip: 0,
									Take: 1000,
									//Sort: 'COLUMN_NAME',
                                                                        Sort: 'column_name',
									Sortdir: 'ASC',
									target: 'ViewGetFieldRepGenCMRequest',
									param: varViewReportGenResultCM
								}
							}
						);
						varTempViewReportGen=varViewReportGenResultCM;
						varTempTitleReportGen=nmTreeResultCMReportGenView
					}
					else if (n.attributes.id === 'PM1')
					{
						dsSetReportGen.load
						(
							{
								params:
								{
									Skip: 0,
									Take: 1000,
									//Sort: 'COLUMN_NAME',
                                                                        Sort: 'column_name',
									Sortdir: 'ASC',
									target: 'ViewGetFieldRepGenCMRequest',
									param: varViewReportGenSchPM
								}
							}
						);
						varTempViewReportGen=varViewReportGenSchPM;
						varTempTitleReportGen=nmTreeSchPMReportGenView
					}
					else if (n.attributes.id === 'PM2')
					{
						dsSetReportGen.load
						(
							{
								params:
								{
									Skip: 0,
									Take: 1000,
									//Sort: 'COLUMN_NAME',
                                                                        Sort: 'column_name',
									Sortdir: 'ASC',
									target: 'ViewGetFieldRepGenCMRequest',
									param: varViewReportGenWOPM
								}
							}
						);
						varTempViewReportGen=varViewReportGenWOPM;
						varTempTitleReportGen=nmTreeWOPMReportGenView
					}
					else if (n.attributes.id === 'PM3')
					{
						dsSetReportGen.load
						(
							{
								params:
								{
									Skip: 0,
									Take: 1000,
									//Sort: 'COLUMN_NAME',
                                                                        Sort: 'column_name',
									Sortdir: 'ASC',
									target: 'ViewGetFieldRepGenCMRequest',
									param: varViewReportGenResultPM
								}
							}
						);
						varTempViewReportGen=varViewReportGenResultPM;
						varTempTitleReportGen=nmTreeResultPMReportGenView
					}
					;
				}
			}
		}
	);
	
	var rootTreeReportGen = new Ext.tree.AsyncTreeNode
	(
		{
			expanded: true,
			text:nmTitleTreeHeaderReportGenView,
			id:'rootTreeReportGen',
			children: getStrTreeReportGen(),
			autoScroll: true
		}
	)

	
  
  treeReportGen.setRootNode(rootTreeReportGen);  
  treeReportGen.getRootNode().expand();

    var FormSetReportGen  = new Ext.Panel
    (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'column',
		    title: nmTitleFormReportGenView,
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100% 100%',
		    iconCls: 'SetupSetEquipmentCat',
		    items: 
			[
				{
					columnWidth: .35,
					layout: 'form',
					border: false,
					autoScroll: true,
					bodyStyle: 'padding:6px 3px 3px 6px',
					items:
					[
						treeReportGen
					]
				},
				{
					columnWidth: .275,
					layout: 'form',
					bodyStyle: 'padding:6px 3px 3px 3px',
					border: false,
					items:
					[
						panellistViewReportGen
					]
				},
				{
					columnWidth: .05,
					layout: 'form',
					border: false,
					autoScroll: false,
					bodyStyle: 'padding:6px 0px 3px 3px',
					items:
					[
						{
							xtype:'button',
							style:{'margin-top':'150px'},
							text:' > ',
							width:25,
							hideLabel:true,
							id: 'btnAddRepGen',
							handler:function()
							{
								if (varSelectAddReportGen != undefined)
								{
									if (varSelectAddReportGen.getSelectedIndexes().length > 0)
									{
										for(var i=0;i < varSelectAddReportGen.getSelectedIndexes().length ;i++)
										{
											var p = new mRecReportGen
											(
												{
													COLUMN_NAME:dsSetReportGen.getAt(varSelectAddReportGen.getSelectedIndexes()[i]).data.COLUMN_NAME
												}
											);
											
											dsSetPilihReportGen.insert(dsSetPilihReportGen.getCount(),p);
											dsTempHapusReportGen.insert(dsSetPilihReportGen.getCount(),p);
											
										}
										
										if (dsTempHapusReportGen != undefined)
										{
											if(dsTempHapusReportGen.getCount() > 0)
											{
												for(var i=0;i < dsTempHapusReportGen.getCount() ;i++)
												{
													for(var j=0;j < dsSetReportGen.getCount() ;j++)
													{
														if (dsSetReportGen.data.items[j].data.COLUMN_NAME === dsTempHapusReportGen.data.items[i].data.COLUMN_NAME)
														{
															dsSetReportGen.removeAt(j);
															break;
														};
													}
												}
												
												dsTempHapusReportGen.removeAll();
											};
										};
										
										if (dsSetReportGen.getCount() > 0 )
										{
											varSelectAddReportGen.select(varSelectAddReportGen.getNodes(0, 0), true);
										};
										//varSelectAddReportGen=undefined;
									};
								}
								else
								{
									ShowPesanWarningReportGen(nmGetValidasiSelect(nmTitleNamaKolomReportGen),nmTitleFormReportGenView);
								};
							}
						},
						{
							xtype:'button',
							text:' >> ',
							style:{'margin-top':'3px'},
							width:25,
							hideLabel:true,
							id: 'btnAddAllRepGen',
							handler:function()
							{
								if(dsSetReportGen.getCount() > 0)
								{
									for(var i=0;i < dsSetReportGen.getCount() ;i++)
									{
										dsSetPilihReportGen.insert(dsSetPilihReportGen.getCount(),dsSetReportGen.data.items[i]);
									}
								};
								dsSetReportGen.removeAll();
							}
						},
						{
							xtype:'button',
							text:' < ',
							style:{'margin-top':'3px'},
							width:25,
							hideLabel:true,
							id: 'btnRemoveRepGen',
							handler:function()
							{
								if (varSelectAddReportGen != undefined)
								{
									if (varSelectAddReportGen.getSelectedIndexes().length > 0)
									{
										for(var i=0;i < varSelectAddReportGen.getSelectedIndexes().length ;i++)
										{
											var p = new mRecReportGen
											(
												{
													COLUMN_NAME:dsSetPilihReportGen.getAt(varSelectAddReportGen.getSelectedIndexes()[i]).data.COLUMN_NAME
												}
											);
											
											dsSetReportGen.insert(dsSetReportGen.getCount(),p);
											dsTempHapusReportGen.insert(dsSetReportGen.getCount(),p);
											
										}
										
										if (dsTempHapusReportGen != undefined)
										{
											if(dsTempHapusReportGen.getCount() > 0)
											{
												for(var i=0;i < dsTempHapusReportGen.getCount() ;i++)
												{
													for(var j=0;j < dsSetPilihReportGen.getCount() ;j++)
													{
														if (dsSetPilihReportGen.data.items[j].data.COLUMN_NAME === dsTempHapusReportGen.data.items[i].data.COLUMN_NAME)
														{
															dsSetPilihReportGen.removeAt(j);
															break;
														};
													}
												}
												
												dsTempHapusReportGen.removeAll();
											};
										};
										if (dsSetPilihReportGen.getCount() > 0 )
										{
											varSelectAddReportGen.select(varSelectAddReportGen.getNodes(0, 0), true);
										};
										//varSelectAddReportGen=undefined;
									};
								}
								else
								{
									ShowPesanWarningReportGen(nmGetValidasiSelect(nmTitleNamaKolomReportGen),nmTitleFormReportGenView);
								};
							}
						},
						{
							xtype:'button',
							text:' << ',
							width:25,
							style:{'margin-top':'3px'},
							hideLabel:true,
							id: 'btnRemoveAllRepGen',
							handler:function()
							{
								if(dsSetPilihReportGen.getCount() > 0)
								{
									for(var i=0;i < dsSetPilihReportGen.getCount() ;i++)
									{
										dsSetReportGen.insert(dsSetReportGen.getCount(),dsSetPilihReportGen.data.items[i]);
									}
								};
								dsSetPilihReportGen.removeAll();
							}
						}
					]
				},
				{
					columnWidth: .275,
					layout: 'form',
					bodyStyle: 'padding:6px 3px 3px 3px',
					border: false,
					anchor: '100% 100%',
					items:
					[
						panellistViewReportGen2,
						{
							xtype:'button',
							text:nmBtnOK,
							style:{'margin-top':'5px'},
							width:70,
							hideLabel:true,
							id: 'btnOKSetReportGen',
							handler:function() 
							{
								var x = getStrCriteriaFieldReportGen()
								var cStrSelect = varTempViewReportGen + '@^@' + x + '@^@' + varTempTitleReportGen;

								if (x != '')
								{
									ShowReport('', '960001', cStrSelect);
								};
							}
						}
					]
				},
				{
					columnWidth: .05,
					layout: 'form',
					border: false,
					autoScroll: false,
					bodyStyle: 'padding:6px 0px 3px 3px',
					items:
					[
						{
							xtype:'button',
							style:{'margin-top':'150px'},
							text:' ^ ',
							width:25,
							hideLabel:true,
							id: 'btnUpRepGen',
							handler:function()
							{
								if (varSelectAddReportGen != undefined)
								{
									if (varSelectAddReportGen.getSelectedIndexes().length > 0)
									{
										var arr = new Array();
										for(var i=0 ; i < varSelectAddReportGen.getSelectedIndexes().length ;i++)
										{
											arr[i]= varSelectAddReportGen.getSelectedIndexes()[i];
										};
										
										arr.sort();
										
										for(var i=0 ; i < arr.length ;i++)
										{
											var j = arr[i];
											
											if (j > 0)
											{
												var p = new mRecReportGen
												(
													{
														COLUMN_NAME:dsSetPilihReportGen.getAt(arr[i]).data.COLUMN_NAME
													}
												);
												
												
												dsTempHapusReportGen.insert(0,dsSetPilihReportGen.getAt(j-1));
												dsSetPilihReportGen.removeAt(j-1);
												dsSetPilihReportGen.insert(j-1,p);
												dsSetPilihReportGen.removeAt(j);
												dsSetPilihReportGen.insert(j,dsTempHapusReportGen.getAt(0))
												dsTempHapusReportGen.removeAll();
												varSelectAddReportGen.select(varSelectAddReportGen.getNodes(j-1, j-1), true);
											};
										};
									};
								}
								else
								{
									ShowPesanWarningReportGen(nmGetValidasiSelect(nmTitleNamaKolomReportGen),nmTitleFormReportGenView);
								};
							}
						},
						{
							xtype:'button',
							text:' v ',
							width:25,
							style:{'margin-top':'3px'},
							hideLabel:true,
							id: 'btnDownRepGen',
							handler:function()
							{
								if (varSelectAddReportGen != undefined)
								{
									if (varSelectAddReportGen.getSelectedIndexes().length > 0)
									{
										var arr = new Array();
										for(var i=0 ; i < varSelectAddReportGen.getSelectedIndexes().length ;i++)
										{
											arr[i]= varSelectAddReportGen.getSelectedIndexes()[i];
										};
										
										arr.sort();
										
										for(var i=arr.length-1 ; i >= 0 ;i--)
										{
											var j = arr[i];
											if (j != (dsSetPilihReportGen.getCount()-1))
											{
												var p = new mRecReportGen
												(
													{
														COLUMN_NAME:dsSetPilihReportGen.getAt(arr[i]).data.COLUMN_NAME
													}
												);
												
												dsTempHapusReportGen.insert(0,dsSetPilihReportGen.getAt(j+1));
												dsSetPilihReportGen.removeAt(j+1);
												dsSetPilihReportGen.insert(j+1,p);
												dsSetPilihReportGen.removeAt(j);
												dsSetPilihReportGen.insert(j,dsTempHapusReportGen.getAt(0))
												dsTempHapusReportGen.removeAll();
												varSelectAddReportGen.select(varSelectAddReportGen.getNodes(j+1, j+1), true);
											};
										}
									};
								}
								else
								{
									ShowPesanWarningReportGen(nmGetValidasiSelect(nmTitleNamaKolomReportGen),nmTitleFormReportGenView);
								};
							}
						}
					]
				}
			]
		}
	);
  
    return FormSetReportGen
};

function ShowPesanWarningReportGen(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function getStrCriteriaFieldReportGen()
{
	var x='';
	var j=0;
	for(var i=0;i < dsSetPilihReportGen.getCount();i++)
	{
			j=j+1;
			x += dsSetPilihReportGen.data.items[i].data.COLUMN_NAME + '#[]#'
	}
	
	if (x != '')
	{
		x = j + '@^@' + x;
	};

	return x;
};

function getStrTreeReportGen()
{
	var str='';
	
	str=[
			{
				"text": nmTreeCMReportGenView,
				"id": "CM",
				"cls": "folder",
				"leaf":false,
				"children":[
							{
								"text": nmTreeReqCMReportGenView,
								"id": "CM1",
								"leaf": true,
								"cls": "file"
							},
							{
								"text": nmTreeAppCMReportGenView,
								"id": "CM2",
								"leaf": true,
								"cls": "file"
							},
							{
								"text": nmTreeSchCMReportGenView,
								"id": "CM3",
								"leaf": true,
								"cls": "file"
							},
							{
								"text": nmTreeWOCMReportGenView,
								"id": "CM4",
								"leaf": true,
								"cls": "file"
							},
							{
								"text": nmTreeResultCMReportGenView,
								"id": "CM5",
								"leaf": true,
								"cls": "file"
							}
						]
						
			}, 
			{
				"text": nmTreePMReportGenView,
				"id": "PM",
				"leaf": false,
				"cls": "folder",
				"children":[
							{
								"text": nmTreeSchPMReportGenView,
								"id": "PM1",
								"leaf": true,
								"cls": "file"
							},
							{
								"text": nmTreeWOPMReportGenView,
								"id": "PM2",
								"leaf": true,
								"cls": "file"
							},
							{
								"text": nmTreeResultPMReportGenView,
								"id": "PM3",
								"leaf": true,
								"cls": "file"
							}
						]
				
			}
		]
	
	return str;
};





