
var dsSetReportGenQuery;
var dsSetPilihReportGenQuery;
var dsTempHapusReportGenQuery;

var varSelectAddReportGenQuery;
var varSelectDelReportGenQuery;


var mRecReportGenQuery = Ext.data.Record.create
(
    [
       {name: 'COLUMN_NAME', mapping:'COLUMN_NAME'}
    ]
);
	
CurrentPage.page = getPanelSetReportGenQuery(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelSetReportGenQuery(mod_id) 
{
    var Field = ['COLUMN_NAME']
    dsSetReportGenQuery = new WebApp.DataStore({ fields: Field });
	dsSetPilihReportGenQuery = new WebApp.DataStore({ fields: Field });
	dsTempHapusReportGenQuery= new WebApp.DataStore({ fields: Field });
	
	  var listViewReportGenQuery = new Ext.ListView
	  ({
        store: dsSetReportGenQuery,
        multiSelect: true,
		border:true,
		anchor: '100% 100%',
		height:220,
        emptyText: '',
        reserveScrollOffset: true,
		listeners: 
		{
			selectionchange: 
			{
				fn: function(dv,nodes)
				{
					varSelectAddReportGenQuery=dv;
				}
			}
        },
        columns: 
		[
			{
				header: nmTitleNamaKolomReportGen,
				width: 225,
				dataIndex: 'COLUMN_NAME'
			}
		]
    });
	
	
	 var panellistViewReportGenQuery = new Ext.Panel
	 (
	 {
        id:'panellistViewReportGenQuery',
	    anchor: '100% 100%',
		collapsible:true,
        height:220,
        title:'',
        items: listViewReportGenQuery
    });
	
	 var listViewReportGenQuery2 = new Ext.ListView
	  ({
        store: dsSetPilihReportGenQuery,
        multiSelect: true,
		border:true,
		anchor: '100% 100%',
		height:220,
        emptyText: '',
        reserveScrollOffset: true,
		listeners: 
		{
			selectionchange: 
			{
				fn: function(dv,nodes)
				{
					varSelectAddReportGenQuery=dv;
				}
			}
        },
        columns: 
		[
			{
				header: nmTitleNamaKolomReportGen,
				width: 225,
				dataIndex: 'COLUMN_NAME'
			}
		]
    });
	
	var panellistViewReportGenQuery2 = new Ext.Panel
	 (
	 {
        id:'panellistViewReportGenQuery2',
		anchor: '100% 100%',
        //width:225,
        height:220,
        collapsible:true,
        title:'',
        items: listViewReportGenQuery2
    });
	
	
	var panellistQueryReportGenQuery = new Ext.Panel
	 (
	 {
        id:'panellistQueryReportGenQuery',
		anchor: '100% 45%',
        height:240,
		border:false,
		layout:'form',
        autoScroll:false,
		labelWidth:75,
		bodyStyle: 'padding:10px 10px 10px 10px',
        title:'',
        items: 
		[
			{
				xtype: 'textarea',
				fieldLabel: 'Query ',
				//hideLabel:true,
				height:123,
				name: 'txtQueryReportGenQuery',
				id: 'txtQueryReportGenQuery',
				scroll:true,
				width:537//618//,
				//anchor: '100% 100%'
			},
			{
				layout: 'hBox',
				width:618,
				border: false,
				bodyStyle: 'padding:0px 0px 0px 0px',
				defaults: { margins: '3 3 3 3' },
				anchor: '100%',
				layoutConfig: 
				{
					align: 'middle',
					pack:'end'
				},
				items:
				[
					{
						xtype:'button',
						text:'Execute' ,
						width:70,
						hideLabel:true,
						id: 'btnExecuteReportGenQuery',
						handler:function() 
						{
							dsSetPilihReportGenQuery.removeAll();
							dsTempHapusReportGenQuery.removeAll();
							varSelectAddReportGenQuery=undefined;
							var str = '';
							
							str=Ext.get('txtQueryReportGenQuery').dom.value;
							
							if (str != '')
							{
								dsSetReportGenQuery.load
								(
									{
										params:
										{
											Skip: 0,
											Take: 1000,
											//Sort: 'COLUMN_NAME',
                                                                                        Sort: 'column_name',
											Sortdir: 'ASC',
											target: 'ViewGetFieldQueryRepGen',
											param: str
										}
									}
								);
							};
						}
					}
				]
			},
			{
				xtype: 'textfield',
				fieldLabel: nmTitleReportGen + ' ',
				name: 'txtJudulReportGenQuery',
				id: 'txtJudulReportGenQuery',
				anchor: '80%'
			}
		]
    });
	
	var panellistViewFieldReportGenQuery = new Ext.Panel
	 (
	 {
        id:'panellistViewFieldReportGenQuery',
		anchor: '100% 55%',
        height:100,
		layout: 'column',
		border:false,
        title:'',
        items: 
		[
			{
				columnWidth: .375,
				layout: 'form',
				bodyStyle: 'padding:6px 3px 3px 10px',
				border: false,
				items:
				[
					panellistViewReportGenQuery
				]
			},
			{
				columnWidth: .05,
				layout: 'form',
				border: false,
				autoScroll: false,
				bodyStyle: 'padding:6px 0px 3px 3px',
				items:
				[
					{
						xtype:'button',
						style:{'margin-top':'65px'},
						text:' > ',
						width:25,
						hideLabel:true,
						id: 'btnAddRepGen',
						handler:function()
						{
							if (varSelectAddReportGenQuery != undefined)
							{
								if (varSelectAddReportGenQuery.getSelectedIndexes().length > 0)
								{
									for(var i=0;i < varSelectAddReportGenQuery.getSelectedIndexes().length ;i++)
									{
										var p = new mRecReportGenQuery
										(
											{
												COLUMN_NAME:dsSetReportGenQuery.getAt(varSelectAddReportGenQuery.getSelectedIndexes()[i]).data.COLUMN_NAME
											}
										);
										
										dsSetPilihReportGenQuery.insert(dsSetPilihReportGenQuery.getCount(),p);
										dsTempHapusReportGenQuery.insert(dsSetPilihReportGenQuery.getCount(),p);
									}
									
									if (dsTempHapusReportGenQuery != undefined)
									{
										if(dsTempHapusReportGenQuery.getCount() > 0)
										{
											for(var i=0;i < dsTempHapusReportGenQuery.getCount() ;i++)
											{
												for(var j=0;j < dsSetReportGenQuery.getCount() ;j++)
												{
													if (dsSetReportGenQuery.data.items[j].data.COLUMN_NAME === dsTempHapusReportGenQuery.data.items[i].data.COLUMN_NAME)
													{
														dsSetReportGenQuery.removeAt(j);
														break;
													};
												}
											}
											
											dsTempHapusReportGenQuery.removeAll();
										};
									};
									
									if (dsSetReportGenQuery.getCount() > 0 )
									{
										varSelectAddReportGenQuery.select(varSelectAddReportGenQuery.getNodes(0, 0), true);
									};
									//varSelectAddReportGenQuery=undefined;
								};
							}
							else
							{
								ShowPesanWarningReportGenQuery(nmGetValidasiSelect(nmTitleNamaKolomReportGen),nmTitleFormReportGenQuery);
							};
						}
					},
					{
						xtype:'button',
						text:' >> ',
						style:{'margin-top':'3px'},
						width:25,
						hideLabel:true,
						id: 'btnAddAllRepGen',
						handler:function()
						{
							if(dsSetReportGenQuery.getCount() > 0)
							{
								for(var i=0;i < dsSetReportGenQuery.getCount() ;i++)
								{
									dsSetPilihReportGenQuery.insert(dsSetPilihReportGenQuery.getCount(),dsSetReportGenQuery.data.items[i]);
								}
							};
							dsSetReportGenQuery.removeAll();
							
						}
					},
					{
						xtype:'button',
						text:' < ',
						style:{'margin-top':'3px'},
						width:25,
						hideLabel:true,
						id: 'btnRemoveRepGen',
						handler:function()
						{
							if (varSelectAddReportGenQuery != undefined)
							{
								if (varSelectAddReportGenQuery.getSelectedIndexes().length > 0)
								{
									for(var i=0;i < varSelectAddReportGenQuery.getSelectedIndexes().length ;i++)
									{
										var p = new mRecReportGenQuery
										(
											{
												COLUMN_NAME:dsSetPilihReportGenQuery.getAt(varSelectAddReportGenQuery.getSelectedIndexes()[i]).data.COLUMN_NAME
											}
										);
										
										dsSetReportGenQuery.insert(dsSetReportGenQuery.getCount(),p);
										dsTempHapusReportGenQuery.insert(dsSetReportGenQuery.getCount(),p);
										
									}
									
									if (dsTempHapusReportGenQuery != undefined)
									{
										if(dsTempHapusReportGenQuery.getCount() > 0)
										{
											for(var i=0;i < dsTempHapusReportGenQuery.getCount() ;i++)
											{
												for(var j=0;j < dsSetPilihReportGenQuery.getCount() ;j++)
												{
													if (dsSetPilihReportGenQuery.data.items[j].data.COLUMN_NAME === dsTempHapusReportGenQuery.data.items[i].data.COLUMN_NAME)
													{
														dsSetPilihReportGenQuery.removeAt(j);
														break;
													};
												}
											}
											
											dsTempHapusReportGenQuery.removeAll();
										};
									};
									if (dsSetPilihReportGenQuery.getCount() > 0 )
									{
										varSelectAddReportGenQuery.select(varSelectAddReportGenQuery.getNodes(0, 0), true);
									};
									//varSelectAddReportGenQuery=undefined;
								};
							}
							else
							{
								ShowPesanWarningReportGenQuery(nmGetValidasiSelect(nmTitleNamaKolomReportGen),nmTitleFormReportGenQuery);
							};
						}
					},
					{
						xtype:'button',
						text:' << ',
						width:25,
						style:{'margin-top':'3px'},
						hideLabel:true,
						id: 'btnRemoveAllRepGen',
						handler:function()
						{
							if(dsSetPilihReportGenQuery.getCount() > 0)
							{
								for(var i=0;i < dsSetPilihReportGenQuery.getCount() ;i++)
								{
									dsSetReportGenQuery.insert(dsSetReportGenQuery.getCount(),dsSetPilihReportGenQuery.data.items[i]);
								}
							};
							
							dsSetPilihReportGenQuery.removeAll();
						}
					}
				]
			},
			{
				columnWidth: .375,
				layout: 'form',
				bodyStyle: 'padding:6px 3px 3px 3px',
				border: false,
				anchor: '100% 100%',
				items:
				[
					panellistViewReportGenQuery2
				]
			},
			{
				columnWidth: .05,
				layout: 'form',
				border: false,
				autoScroll: false,
				bodyStyle: 'padding:6px 0px 3px 3px',
				items:
				[
					{
						xtype:'button',
						style:{'margin-top':'65px'},
						text:' ^ ',
						width:25,
						hideLabel:true,
						id: 'btnUpRepGen',
						handler:function()
						{
							if (varSelectAddReportGenQuery != undefined)
							{
								if (varSelectAddReportGenQuery.getSelectedIndexes().length > 0)
								{
									var arr = new Array();
									for(var i=0 ; i < varSelectAddReportGenQuery.getSelectedIndexes().length ;i++)
									{
										arr[i]= varSelectAddReportGenQuery.getSelectedIndexes()[i];
									};
									
									arr.sort();
									
									for(var i=0 ; i < arr.length ;i++)
									{
										var j = arr[i];
										
										if (j > 0)
										{
											var p = new mRecReportGenQuery
											(
												{
													COLUMN_NAME:dsSetPilihReportGenQuery.getAt(arr[i]).data.COLUMN_NAME
												}
											);
											
											
											dsTempHapusReportGenQuery.insert(0,dsSetPilihReportGenQuery.getAt(j-1));
											dsSetPilihReportGenQuery.removeAt(j-1);
											dsSetPilihReportGenQuery.insert(j-1,p);
											dsSetPilihReportGenQuery.removeAt(j);
											dsSetPilihReportGenQuery.insert(j,dsTempHapusReportGenQuery.getAt(0))
											dsTempHapusReportGenQuery.removeAll();
											varSelectAddReportGenQuery.select(varSelectAddReportGenQuery.getNodes(j-1, j-1), true);
										};
									};
								};
							}
							else
							{
								ShowPesanWarningReportGenQuery(nmGetValidasiSelect(nmTitleNamaKolomReportGen),nmTitleFormReportGenQuery);
							};
						}
					},
					{
						xtype:'button',
						text:' v ',
						width:25,
						style:{'margin-top':'3px'},
						hideLabel:true,
						id: 'btnDownRepGen',
						handler:function()
						{
							if (varSelectAddReportGenQuery != undefined)
							{
								if (varSelectAddReportGenQuery.getSelectedIndexes().length > 0)
								{
									var arr = new Array();
									for(var i=0 ; i < varSelectAddReportGenQuery.getSelectedIndexes().length ;i++)
									{
										arr[i]= varSelectAddReportGenQuery.getSelectedIndexes()[i];
									};
									
									arr.sort();
									
									for(var i=arr.length-1 ; i >= 0 ;i--)
									{
										var j = arr[i];
										if (j != (dsSetPilihReportGenQuery.getCount()-1))
										{
											var p = new mRecReportGenQuery
											(
												{
													COLUMN_NAME:dsSetPilihReportGenQuery.getAt(arr[i]).data.COLUMN_NAME
												}
											);
											
											dsTempHapusReportGenQuery.insert(0,dsSetPilihReportGenQuery.getAt(j+1));
											dsSetPilihReportGenQuery.removeAt(j+1);
											dsSetPilihReportGenQuery.insert(j+1,p);
											dsSetPilihReportGenQuery.removeAt(j);
											dsSetPilihReportGenQuery.insert(j,dsTempHapusReportGenQuery.getAt(0))
											dsTempHapusReportGenQuery.removeAll();
											varSelectAddReportGenQuery.select(varSelectAddReportGenQuery.getNodes(j+1, j+1), true);
										};
									}
								};
							}
							else
							{
								ShowPesanWarningReportGenQuery(nmGetValidasiSelect(nmTitleNamaKolomReportGen),nmTitleFormReportGenQuery);
							};
						}
					}
				]
			},
			{
				columnWidth: .15,
				layout: 'form',
				border: false,
				autoScroll: false,
				bodyStyle: 'padding:6px 0px 3px 3px',
				items:
				[
					{
						xtype:'button',
						text:nmBtnOK,
						style:{'margin-top':'198px','margin-left':'13px'},
						width:70,
						hideLabel:true,
						id: 'btnOKSetReportGenQuery',
						handler:function() 
						{
							var str = '';
							str = Ext.get('txtQueryReportGenQuery').dom.value;
							
							if (str != '')
							{
								var x = getStrCriteriaFieldReportGenQuery();
								var cStrSelect = str + '@^@' + x + '@^@' + Ext.get('txtJudulReportGenQuery').dom.value;
								if (x != '')
								{
									ShowReport('', '960002', cStrSelect);
								};
							}
						}
					}
				]
			}
		]
    });
	
	

    var FormSetReportGenQuery  = new Ext.Panel
    (
		{
		    id: mod_id,
		    closable: true,
		    region: 'center',
		    layout: 'form',
		    title: nmTitleFormReportGenQuery,
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '100% 100%',
		    iconCls: 'SetupSetEquipmentCat',
		    items: 
			[
				panellistQueryReportGenQuery,
				panellistViewFieldReportGenQuery
			]
		}
	);
  
    return FormSetReportGenQuery
};

function ShowPesanWarningReportGenQuery(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function getStrCriteriaFieldReportGenQuery()
{
	var x='';
	var j=0;
	for(var i=0;i < dsSetPilihReportGenQuery.getCount();i++)
	{
			j=j+1;
			x += dsSetPilihReportGenQuery.data.items[i].data.COLUMN_NAME + '#[]#'
	}
	
	if (x != '')
	{
		x = j + '@^@' + x;
	};

	return x;
};







