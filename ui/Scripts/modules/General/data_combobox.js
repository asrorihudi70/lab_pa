function viRefresh_Vendor()
{
    ds_Vendor.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'KD_VENDOR',
                Sortdir: 'ASC',
                target:'viVENDOR',
                //param: " Where KD_CUSTOMER='" + strKD_CUST + "' "
            }            
        }
    )
}

function viCombo_Vendor(lebar,Nama_ID)
{
    var Field_Vendor = ['KD_VENDOR', 'KD_CUSTOMER', 'VENDOR'];
    ds_Vendor = new WebApp.DataStore({fields: Field_Vendor});
    
	viRefresh_Vendor();
	
    var cbo_Vendor = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'Vendor',
			valueField: 'KD_VENDOR',
            displayField: 'VENDOR',
			store: ds_Vendor,
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',                        
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
			listeners:
			{ 
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_Vendor;
}

function viRefresh_STS_MARITAL()
{
    ds_STS_MARITAL.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'KD_STS_MARITAL',
                Sortdir: 'ASC',
                target:'ViStsMarital',
				param: ""
            }            
        }
    )
}
	
function Vicbo_STS_MARITAL(lebar,Nama_ID)
{
    var Field_STS_MARITAL = ['KD_STS_MARITAL', 'STS_MARITAL'];
    ds_STS_MARITAL = new WebApp.DataStore({fields: Field_STS_MARITAL});
    
	viRefresh_STS_MARITAL();
    var cbo_STS_MARITAL = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'STS_MARITAL',
			valueField: 'KD_STS_MARITAL',
            displayField: 'STS_MARITAL',
			store: ds_STS_MARITAL,
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',                        
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
            listeners:
			{
				'select': function (a,b,c)
				{
				}
			}
        }
    )    
    return cbo_STS_MARITAL;
}

function viRefresh_Pendidikan()
{
    ds_Pendidikan.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'KD_Pendidikan',
                Sortdir: 'ASC',
                target:'viPendidikan',
				param: ""
            }            
        }
    )
}
	
function Vicbo_Pendidikan(lebar,Nama_ID)
{
    var Field_Pendidikan = ['KD_PENDIDIKAN', 'PENDIDIKAN'];
    ds_Pendidikan = new WebApp.DataStore({fields: Field_Pendidikan});
    
	viRefresh_Pendidikan();
    var cbo_Pendidikan = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'Pendidikan',
			valueField: 'KD_PENDIDIKAN',
            displayField: 'PENDIDIKAN',
			store: ds_Pendidikan,
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',                        
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
            listeners:
			{
				'select': function (a,b,c)
				{
				}
			}
        }
    )    
    return cbo_Pendidikan;
}

function viRefresh_Pekerjaan()
{
    ds_Pekerjaan.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'KD_PEKERJAAN',
                Sortdir: 'ASC',
                target:'viPekerjaan',
				//param: " Where KD_CUSTOMER='" + strKD_CUST + "' "
            }            
        }
    )
}
	
function Vicbo_Pekerjaan(lebar,Nama_ID)
{
    var Field_Pekerjaan = ['KD_PEKERJAAN', 'KD_CUSTOMER', 'PEKERJAAN'];
    ds_Pekerjaan = new WebApp.DataStore({fields: Field_Pekerjaan});
    
	viRefresh_Pekerjaan();
    var cbo_Pekerjaan = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'Pekerjaan',
			valueField: 'KD_PEKERJAAN',
            displayField: 'PEKERJAAN',
			store: ds_Pekerjaan,
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',                        
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
            listeners:
			{
				'select': function (a,b,c)
				{
				}
			}
        }
    )    
    return cbo_Pekerjaan;
}

function viRefresh_Agama()
{
    ds_Agama.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'KD_AGAMA',
                Sortdir: 'ASC',
                target:'viAgama',
				param: ""
            }            
        }
    )
}
	
function Vicbo_Agama(lebar,Nama_ID)
{
    var Field_Agama = ['KD_AGAMA', 'AGAMA'];
    ds_Agama = new WebApp.DataStore({fields: Field_Agama});
    
	viRefresh_Agama();
    var cbo_Agama = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'Agama',
			valueField: 'KD_AGAMA',
            displayField: 'AGAMA',
			store: ds_Agama,
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',                        
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
            listeners:
			{
				'select': function (a,b,c)
				{
				}
			}
        }
    )    
    return cbo_Agama;
}

function viRefresh_Kelompok()
{
    ds_Kelompok.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'KD_KELOMPOK',
                Sortdir: 'ASC',
                target:'viKelompok',
                //param: " Where KD_CUSTOMER='" + strKD_CUST + "' "
            }            
        }
    )
}
	
function Vicbo_Kelompok(lebar,Nama_ID)
{
    // var Field_Kelompok = ['KD_KELOMPOK', 'KD_CUSTOMER', 'KD_TYPE', 'KELOMPOK'];
    // ds_Kelompok = new WebApp.DataStore({fields: Field_Kelompok});
    
	// viRefresh_Kelompok();
    var cbo_Kelompok = new Ext.form.ComboBox
    (
        {
			id: Nama_ID,
            flex: 1,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			emptyText:'',
            fieldLabel: 'Kelompok',
            width: lebar,
            name: Nama_ID,
            store: new Ext.data.ArrayStore
            (
				{
					id: 0,
					fields: 
					[
						'KD_KELOMPOK',
						'KELOMPOK'
					],
					data: [[0, "Umum"],[1, "JKN"]]
				}
			),
			valueField: 'KD_KELOMPOK',
            displayField: 'KELOMPOK',	
            listeners:
			{
			}
        }
    )    
    return cbo_Kelompok;
}


function viCombo_POLI(lebar,Nama_ID)
{
    // var Field_poli = ['KD_UNIT','NAMA_UNIT'];
    // ds_Poli = new WebApp.DataStore({fields: Field_poli});
    
	// viRefresh_Poli();
    var cbo_Poli = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'Poli',
			// store: ds_Poli,
			store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields:
                    [
                            'KD_UNIT',
                            'NAMA_UNIT'
                    ],
                    data: [[0, "UGD"]]
                }
            ),
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',                        
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
            listeners:
            {
            }
        }
    )
    
    return cbo_Poli;
}

function viCombo_UnitRWI(lebar,Nama_ID)
{
    // var Field_poli = ['KD_UNIT','NAMA_UNIT'];
    // ds_Poli = new WebApp.DataStore({fields: Field_poli});

	// viRefresh_Poli();
    var cbo_UnitRWI = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'Poli',
			// store: ds_Poli,
			store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields:
                    [
                            'KD_UNIT',
                            'NAMA_UNIT'
                    ],
                    data: [[0, "Kelas 1"], [1, "Kelas 2"]]
                }
            ),
            valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
            listeners:
            {
            }
        }
    )

    return cbo_UnitRWI;
}

// function Vicbo_Kelompok(lebar,Nama_ID)
// {
    // var Field_Kelompok = ['KD_KELOMPOK', 'KD_CUSTOMER', 'KD_TYPE', 'KELOMPOK'];
    // ds_Kelompok = new WebApp.DataStore({fields: Field_Kelompok});
    
	// viRefresh_Kelompok();
    // var cbo_Kelompok = new Ext.form.ComboBox
    // (
        // {
            // flex: 1,
            // fieldLabel: 'Kelompok',
			// valueField: 'KD_KELOMPOK',
            // displayField: 'KELOMPOK',
			// store: ds_Kelompok,
            // width: lebar,
            // mode: 'local',
            // typeAhead: true,
            // triggerAction: 'all',                        
            // name: Nama_ID,
            // lazyRender: true,
            // id: Nama_ID,
            // listeners:
			// {
				// 'select': function (a,b,c)
				// {
				// }
			// }
        // }
    // )    
    // return cbo_Kelompok;
// }

function viRefresh_Dokter()
{
    ds_Dokter.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'KD_DOKTER',
                Sortdir: 'ASC',
                target:'viDokter',
                //param: " Where KD_CUSTOMER='" + strKD_CUST + "' "
            }            
        }
    )
}
	
function Vicbo_Dokter(lebar,Nama_ID)
{
    var Field_Dokter = ['KD_DOKTER',  'KD_CUSTOMER', 'NAMA'];
    ds_Dokter = new WebApp.DataStore({fields: Field_Dokter});
    
	viRefresh_Dokter();
    var cbo_Dokter = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'Dokter',
			valueField: 'KD_DOKTER',
            displayField: 'NAMA',
			store: ds_Dokter,
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',                        
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
            listeners:
			{
				'select': function (a,b,c)
				{
				}
			}
        }
    )    
    return cbo_Dokter;
}

function viRefresh_Poli()
{
    ds_Poli.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'KD_UNIT',
                Sortdir: 'ASC',
                target:'viUNIT',
                //param: " Where KD_CUSTOMER='" + strKD_CUST + "' "
            }            
        }
    )
}
	
// function viCombo_POLI(lebar,Nama_ID)
// {
    // var Field_poli = ['KD_UNIT','NAMA_UNIT'];
    // ds_Poli = new WebApp.DataStore({fields: Field_poli});
    
	// viRefresh_Poli();
    // var cbo_Poli = new Ext.form.ComboBox
    // (
        // {
            // flex: 1,
            // fieldLabel: 'Poli',
			// valueField: 'KD_UNIT',
            // displayField: 'NAMA_UNIT',
			// store: ds_Poli,
            // width: lebar,
            // mode: 'local',
            // typeAhead: true,
            // triggerAction: 'all',                        
            // name: Nama_ID,
            // lazyRender: true,
            // id: Nama_ID,
            // listeners:
			// {
				// 'select': function (a,b,c)
				// {
				// }
			// }
        // }
    // )
    
    // return cbo_Poli;
// }

function viComboJK(lebar,Nama_ID)
{
  var cbo_viComboJK = new Ext.form.ComboBox
	(
	
		{
			id:Nama_ID,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: "Kategori",			
			width:lebar,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'IdPendaftaran',
						'displayTextPendaftaran'
					],
					data: [[1, "Laki-Laki"],[0, "Perempuan"]]
				}
			),
			valueField: 'IdPendaftaran',
			displayField: 'displayTextPendaftaran',
			listeners:  
			{
			}
		}
	);
	return cbo_viComboJK;
};

function viComboGolDRH(lebar,Nama_ID)
{
  var cbo_viComboGolD = new Ext.form.ComboBox
	(
	
		{
			id:Nama_ID,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: "Gol. Darah",			
			width:lebar,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'IdGD',
						'displayTextGD'
					],
					data: [[0, "A"],[1, "B"],[2, "AB"],[3, "O"]]
				}
			),
			valueField: 'IdGD',
			displayField: 'displayTextGD',
			listeners:  
			{
			}
		}
	);
	return cbo_viComboGolD;
};


//Status Nikah
function viRefresh_StatusNikah()
{
    ds_StatusNikah.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'ID_MARITAL',
                Sortdir: 'ASC',
                target:'viSDM_STS_MARITAL',
                param: ' order by STATUS_MARITAL  '
            }            
        }
    )
}
	
function viCombo_StatusNikah(lebar,Nama_ID)
{
    var Field = ['ID_MARITAL','STATUS_MARITAL'];
    ds_StatusNikah = new WebApp.DataStore({fields: Field});
    
	viRefresh_StatusNikah();
    var cbo_StatusNikah = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'Status Nikah',
			valueField: 'ID_MARITAL',
            displayField: 'STATUS_MARITAL',
			store: ds_StatusNikah,
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',                        
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
            listeners:
			{
				'select': function (a,b,c)
				{
				}
			}
        }
    )
    
    return cbo_StatusNikah;
}

function viComboJK_trpegawai(lebar,Nama_ID)
{
  var cbo_viComboJK_trpegawai = new Ext.form.ComboBox
	(
	
		{
			id:Nama_ID,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: "Kategori",			
			width:lebar,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'IdJK',
						'displayTextJK'
					],
					data: [[1, "Laki-Laki"],[0, "Perempuan"]]
				}
			),
			valueField: 'IdJK',
			displayField: 'displayTextJK',
			listeners:  
			{
			}
		}
	);
	return cbo_viComboJK_trpegawai;
};


function viComboMarital_trpegawai(lebar,Nama_ID)
{
  var cbo_viComboMarital_trpegawai = new Ext.form.ComboBox
	(
	
		{
			id:Nama_ID,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: "Kategori",			
			width:lebar,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'IdMarital',
						'displayTextMarital'
					],
					data: [[0, "Menikah"],[1, "Belum"]]
				}
			),
			valueField: 'IdMarital',
			displayField: 'displayTextMarital',
			listeners:  
			{
			}
		}
	);
	return cbo_viComboMarital_trpegawai;
};



function viComboGolD_trpegawai(lebar,Nama_ID)
{
  var cbo_viComboGolD_trpegawai = new Ext.form.ComboBox
	(
	
		{
			id:Nama_ID,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: "Kategori",			
			width:lebar,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'IdGD',
						'displayTextGD'
					],
					data: [[0, "A"],[1, "B"],[2, "AB"],[3, "O"]]
				}
			),
			valueField: 'IdGD',
			displayField: 'displayTextGD',
			listeners:  
			{
			}
		}
	);
	return cbo_viComboGolD_trpegawai;
};
//OutPut
function viComboOutput_output(lebar,Nama_ID)
{
  var cboComboOutput_output = new Ext.form.ComboBox
	(
		{
			id:Nama_ID,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			fieldLabel: 'Output',
			mode: 'local',
			emptyText:'',
			labelSeparator: '',
			width: lebar,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: [[1, "PDF"], [0, "EXCEL"]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			listeners:  
			{
				'select': function (a,b,c)
				{					
				}
			}
		}
	);
	return cboComboOutput_output;
};

//TYPE REPORT SUM_DET
function viComboDet_Sum(lebar,Nama_ID)
{
  var cboComboDet_Sum = new Ext.form.ComboBox
	(
		{
			id:Nama_ID,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			fieldLabel: 'Type Report ',
			mode: 'local',
			labelSeparator: '',
			emptyText:'',
			width: lebar,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'IdReport',
						'displayTextReport'
					],
					data: [[1, "Summary"], [0, "Detail"]]
				}
			),
			valueField: 'IdReport',
			displayField: 'displayTextReport',
			listeners:  
			{
				'select': function (a,b,c)
				{					
				}
			}
		}
	);
	return cboComboDet_Sum;
};

function viRefresh_UnitRpt(ID_Cust)
{
	var x = ' where KD_CUSTOMER=~'+ ID_Cust + '~  order by KD_UNIT desc '
	     
    ds_UnitRpt.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'KD_UNIT',
                Sortdir: 'ASC',
                target: 'viCbo_UNITRpt',
                param: x //' order by NAMA_UNIT '
            }            
        }
    )
}

function viCombo_UnitRpt(lebar,Nama_ID,ID_Cust)
{
    var Field = ['KD_UNIT', 'KD_CUSTOMER','NAMA_UNIT'];
    ds_UnitRpt = new WebApp.DataStore({fields: Field});
    
	viRefresh_UnitRpt(ID_Cust);
    var cbo_UnitRpt = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'Poli',
			valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
			store: ds_UnitRpt,
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',                        
			labelSeparator: '',
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
            listeners:
			{
				'select': function (a,b,c)
				{
				}
			}
        }
    )    
    return cbo_UnitRpt;
} 


function viRefresh_KelompokRpt(ID_Cust)
{
	var x = ' where KD_CUSTOMER= ~'+ ID_Cust + '~ '
	
    ds_KelompokRpt.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'KD_KELOMPOK',
                Sortdir: 'ASC',
                target: 'viCbo_KelompokRpt',
                param: x //' order by KELOMPOK '
            }            
        }
    )
}

function viCombo_KelompokRpt(lebar,Nama_ID,ID_Cust)
{
    var Field = ['KD_KELOMPOK', 'KD_CUSTOMER','KD_TYPE','KELOMPOK'];
    ds_KelompokRpt = new WebApp.DataStore({fields: Field});
    
	viRefresh_KelompokRpt(ID_Cust);
    var cbo_KelompokRpt = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'Jenis Pasien ',
			valueField: 'KD_KELOMPOK',
            displayField: 'KELOMPOK',
			store: ds_KelompokRpt,  
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',                        
			labelSeparator: '',
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
            listeners:
			{
				'select': function (a,b,c)
				{
				}
			}
        }
    )    
    return cbo_KelompokRpt;
} 

/**
*   Function : viComboPeriksa_pasien
*   
*   Sebuah fungsi untuk menampilkan pilihan jenis pemeriksaan pasien
*/
function viComboPeriksa_pasien(lebar,Nama_ID)
{
  var cbo_viComboPeriksa_pasien = new Ext.form.ComboBox
    (
    
        {
            id:Nama_ID,
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'Pilih Pemeriksaan..',
            fieldLabel: "Pemeriksaan",
			value:1,		
            width:lebar,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields: 
                    [
                        'IdPrks',
                        'displayTextPrks'
                    ],
                    data: [['all', "All"],[1, "Belum Diperiksa"],[2, "Sudah Diperiksa"]]
                }
            ),
            valueField: 'IdPrks',
            displayField: 'displayTextPrks',
            listeners:  
            {
            }
        }
    );
    return cbo_viComboPeriksa_pasien;
};
// End Function viComboPeriksa_pasien # --------------
function viRefresh_Vendor() {
    ds_Vendor.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'KD_VENDOR',
                Sortdir: 'ASC',
                // target: 'viVendor',
                target: 'ComboPBF',
                //param: " Where KD_CUSTOMER='" + strKD_CUST + "' "
            }
        }
    )
}

function Vicbo_Vendor(lebar, Nama_ID) {
    var Field_Vendor = ['KD_VENDOR', 'KD_CUSTOMER', 'VENDOR'];
    ds_Vendor = new WebApp.DataStore({ fields: Field_Vendor });

    viRefresh_Vendor();
    var cbo_Vendor = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'Vendor',
            valueField: 'KD_VENDOR',
            displayField: 'VENDOR',
            store: ds_Vendor,
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
            listeners:
			{
			    'select': function(a, b, c) {
			    }
			}
        }
    )
    return cbo_Vendor;
}

// MediSmart

/**
*   Function : viComboJenisTransaksi_MediSmart
*   
*   Sebuah fungsi untuk menampilkan pilihan jenis transaksi pasien
*/
function viComboJenisTransaksi_MediSmart(lebar,Nama_ID)
{
  var cbo_viComboJenisTransaksi_MediSmart = new Ext.form.ComboBox
    (
    
        {
            id:Nama_ID,
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'Pilih Transaksi..',
            fieldLabel: "Transaksi",
            value:1,        
            width:lebar,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields: 
                    [
                        'IdTrnsksi',
                        'displayTextTrnsksi'
                    ],
                    data: [['all', "Semua"],[1, "Sudah Bayar"],[2, "Belum Bayar"]]
                }
            ),
            valueField: 'IdTrnsksi',
            displayField: 'displayTextTrnsksi',
            listeners:  
            {
            }
        }
    );
    return cbo_viComboJenisTransaksi_MediSmart;
};
// End Function viComboJenisTransaksi_MediSmart # --------------