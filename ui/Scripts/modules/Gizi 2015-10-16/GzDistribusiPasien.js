var dataSource_viGzDistribusiPasien;
var selectCount_viGzDistribusiPasien=50;
var NamaForm_viGzDistribusiPasien="Distribusi Pasien";
var selectCountStatusPostingGzDistribusiPasien='Semua';
var mod_name_viGzDistribusiPasien="viGzDistribusiPasien";
var now_viGzDistribusiPasien= new Date();
var addNew_viGzDistribusiPasien;
var rowSelected_viGzDistribusiPasien;
var setLookUps_viGzDistribusiPasien;
var mNoKunjungan_viGzDistribusiPasien='';
var selectSetUnit;
var cellSelecteddeskripsiRWI;
var tanggal = now_viGzDistribusiPasien.format("d/M/Y");
var jam = now_viGzDistribusiPasien.format("H/i/s");
var tampungshiftsekarang;
var tmpkriteria;
var gridDTLTRHistoryApotekRWI;
var GridDataView_viGzDistribusiPasien;

var CurrentGzDistribusiPasien =
{
    data: Object,
    details: Array,
    row: 0
};

var CurrentData_viGzDistribusiPasien =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viGzDistribusiPasien(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

var GzDistribusiPasien={};
GzDistribusiPasien.form={};
GzDistribusiPasien.func={};
GzDistribusiPasien.vars={};
GzDistribusiPasien.func.parent=GzDistribusiPasien;
GzDistribusiPasien.form.ArrayStore={};
GzDistribusiPasien.form.ComboBox={};
GzDistribusiPasien.form.DataStore={};
GzDistribusiPasien.form.Record={};
GzDistribusiPasien.form.Form={};
GzDistribusiPasien.form.Grid={};
GzDistribusiPasien.form.Panel={};
GzDistribusiPasien.form.TextField={};
GzDistribusiPasien.form.Button={};

GzDistribusiPasien.form.ArrayStore.a	= new Ext.data.ArrayStore({
	id: 0,
	fields:['no_minta','kd_pasien', 'nama', 'kd_unit', 'no_kamar', 'nama_kamar', 'kd_waktu','waktu',
			'kd_jenis','jenis_diet','realisasi'],
	data: []
});

function dataGrid_viGzDistribusiPasien(mod_id_viGzDistribusiPasien){	
    var FieldMaster_viGzDistribusiPasien = 
	[
		'no_distribusi', 'kd_petugas', 'petugas', 'kd_waktu', 'waktu', 'tgl_distribusi','kd_unit', 'nama_unit', 'no_minta'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viGzDistribusiPasien = new WebApp.DataStore
	({
        fields: FieldMaster_viGzDistribusiPasien
    });
    dataGriAwal();
    // Grid Apotek Perencanaan # --------------
	GridDataView_viGzDistribusiPasien = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viGzDistribusiPasien,
			autoScroll: true,
			columnLines: true,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viGzDistribusiPasien = undefined;
							rowSelected_viGzDistribusiPasien = dataSource_viGzDistribusiPasien.getAt(row);
							CurrentData_viGzDistribusiPasien
							CurrentData_viGzDistribusiPasien.row = row;
							CurrentData_viGzDistribusiPasien.data = rowSelected_viGzDistribusiPasien.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viGzDistribusiPasien = dataSource_viGzDistribusiPasien.getAt(ridx);
					if (rowSelected_viGzDistribusiPasien != undefined)
					{
						setLookUp_viGzDistribusiPasien(rowSelected_viGzDistribusiPasien.data);
					}
					else
					{
						setLookUp_viGzDistribusiPasien();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Apotek perencanaan
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header: 'No. Distribusi',
						dataIndex: 'no_distribusi',
						sortable: true,
						width: 35
						
					},
					//-------------- ## --------------
					{
						header:'Tanggal',
						dataIndex: 'tgl_distribusi',						
						width: 30,
						sortable: true,
						hideable:false,
                        menuDisabled:true,
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.tgl_distribusi);
						}
					},
					//-------------- ## --------------
					{
						header: 'Ruangan',
						dataIndex: 'nama_unit',
						sortable: true,
						width: 50
					},
					//-------------- ## --------------
					{
						header: 'Waktu',
						dataIndex: 'waktu',
						sortable: true,
						width: 40
					},
					//-------------- ## --------------
					{
						header: 'Petugas Gizi',
						dataIndex: 'petugas',
						sortable: true,
						width: 40
					},
					//-------------- ## --------------
					{
						header: 'kd_petugas',
						dataIndex: 'kd_petugas',
						sortable: true,
						hidden:true,
						width: 40
					},
					{
						header: 'kd_waktu',
						dataIndex: 'kd_waktu',
						sortable: true,
						hidden:true,
						width: 40
					},
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viGzDistribusiPasien',
				items: 
				[
					{
						xtype: 'button',
						text: 'Tambah Distribusi ',
						iconCls: 'Edit_Tr',
						tooltip: 'Add Data',
						id: 'btnTambah_viGzDistribusiPasien',
						handler: function(sm, row, rec)
						{
							setLookUp_viGzDistribusiPasien();
						}
					},
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viGzDistribusiPasien',
						handler: function(sm, row, rec)
						{
							
							if (rowSelected_viGzDistribusiPasien != undefined)
							{
								setLookUp_viGzDistribusiPasien(rowSelected_viGzDistribusiPasien.data)
							}
							//setLookUp_viGzDistribusiPasien();
							
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viGzDistribusiPasien, selectCount_viGzDistribusiPasien, dataSource_viGzDistribusiPasien),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )
	
	var pencarianGzDistribusiPasien = new Ext.FormPanel({
        labelAlign: 'top',
        frame:true,
        title: '',
        bodyStyle:'padding:5px 5px 0',
        //width: 600,
        items: [
		{
			layout: 'column',
			border: false,
			items:
			[
				{
					columnWidth:.98,
					layout: 'absolute',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					width: 500,
					height: 90,
					anchor: '100% 100%',
					items:
					[
						{
							x: 10,
							y: 0,
							xtype: 'label',
							text: 'No. Distribusi'
						},
						{
							x: 120,
							y: 0,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 0,
							xtype: 'textfield',
							id: 'TxtNoDistribusiFilterGridDataView_viGzDistribusiPasien',
							name: 'TxtNoDistribusiFilterGridDataView_viGzDistribusiPasien',
							emptyText: '',
							width: 130,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariGzDistribusiPasien();
										refreshDistribusiPasien(tmpkriteria);
									} 						
								}
							}
						},
						{
							x: 10,
							y: 30,
							xtype: 'label',
							text: 'Petugas Gizi'
						},
						{
							x: 120,
							y: 30,
							xtype: 'label',
							text: ':'
						},
						ComboPetugasGiziGzDistribusiPasien(),
						
						//-------------- ## --------------
						{
							x: 310,
							y: 0,
							xtype: 'label',
							text: 'Ruangan'
						},
						{
							x: 400,
							y: 0,
							xtype: 'label',
							text: ':'
						},
						ComboUnitGzDistribusiPasien(),	
						
						{
							x: 310,
							y: 30,
							xtype: 'label',
							text: 'Tanggal'
						},
						{
							x: 400,
							y: 30,
							xtype: 'label',
							text: ':'
						},
						{
							x: 410,
							y: 30,
							xtype: 'datefield',
							id: 'dfTglAwalGzDistribusiPasien',
							format: 'd/M/Y',
							width: 120,
							tabIndex:3,
							value:now_viGzDistribusiPasien,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariGzDistribusiPasien();
										refreshDistribusiPasien(tmpkriteria);
									} 						
								}
							}
						},
						{
							x: 540,
							y: 30,
							xtype: 'label',
							text: 's/d'
						},
						{
							x: 568,
							y: 30,
							xtype: 'datefield',
							id: 'dfTglAkhirGzDistribusiPasien',
							format: 'd/M/Y',
							width: 120,
							tabIndex:4,
							value:now_viGzDistribusiPasien,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariGzDistribusiPasien();
										refreshDistribusiPasien(tmpkriteria);
									} 						
								}
							}
						},
						//----------------------------------------
						{
							x: 568,
							y: 60,
							xtype: 'button',
							text: 'Cari',
							iconCls: 'refresh',
							tooltip: 'Cari',
							style:{paddingLeft:'30px'},
							//rowspan: 3,
							width:150,
							id: 'BtnFilterGridDataView_viGzDistribusiPasien',
							handler: function() 
							{					
								tmpkriteria = getCriteriaCariGzDistribusiPasien();
								refreshDistribusiPasien(tmpkriteria);
							}                        
						}
					]
				}
			]
		}
		]	
						/*				
						//-------------- ## --------------
						
						//-------------- ## --------------
				]
			} */
		
		
	})

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viGzDistribusiPasien = new Ext.Panel
    (
		{
			title: NamaForm_viGzDistribusiPasien,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viGzDistribusiPasien,
			region: 'center',
			layout: 'form', 
			closable: true,        
			border: false,  
			margins: '0 5 5 0',
			items: [ pencarianGzDistribusiPasien,
					GridDataView_viGzDistribusiPasien],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viGzDistribusiPasien,
		            columns: 21,
		            defaults: {
					scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viGzDistribusiPasien;
    //-------------- # End form filter # --------------
}

function refreshDistribusiPasien(kriteria)
{
    dataSource_viGzDistribusiPasien.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'ViewDistribusiPasien',
                    param : kriteria
                }			
            }
        );   
    return dataSource_viGzDistribusiPasien;
}

function setLookUp_viGzDistribusiPasien(rowdata){
    var lebar = 985;
    setLookUps_viGzDistribusiPasien = new Ext.Window({
        id: Nci.getId(),
        title: NamaForm_viGzDistribusiPasien, 
        closeAction: 'destroy',        
        width: 900,
        height: 550,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viGzDistribusiPasien(lebar,rowdata),
        listeners:{
            activate: function(){
				
            },
            afterShow: function(){
                this.activate();
            },
            deactivate: function(){
                rowSelected_viGzDistribusiPasien=undefined;
            }
        }
    });

    setLookUps_viGzDistribusiPasien.show();
	
    if (rowdata == undefined){	
    }
    else
    {
        datainit_viGzDistribusiPasien(rowdata);
    }
}

function getFormItemEntry_viGzDistribusiPasien(lebar,rowdata){
    var pnlFormDataBasic_viGzDistribusiPasien = new Ext.FormPanel({
		title: '',
		region: 'north',
		layout: 'form',
		bodyStyle: 'padding:10px 10px 10px 10px',
		anchor: '100%',
		labelWidth: 1,
		autoWidth: true,
		width: lebar,
		border: false,
		items:[
				getItemPanelInputBiodata_viGzDistribusiPasien(lebar),
				getItemGridTransaksi_viGzDistribusiPasien(lebar)
			],
			fileUpload: true,
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						hidden:true,
						id: 'btnAdd_viGzDistribusiPasien',
						handler: function(){
							dataaddnew_viGzDistribusiPasien();
						}
					},{
						xtype: 'tbseparator'
					},{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						disabled:true,
						id: 'btnSimpan_viGzDistribusiPasien',
						handler: function()
						{
							datasave_viGzDistribusiPasien();
						}
					},{
						xtype: 'tbseparator'
					},{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						disabled:true,
						id: 'btnSimpanExit_viGzDistribusiPasien',
						handler: function()
						{
							datasave_viGzDistribusiPasien();
							refreshDistribusiPasien();
							setLookUps_viGzDistribusiPasien.close();
						}
					},{
						xtype: 'tbseparator'
					},
					{
						xtype:'splitbutton',
						text:'Cetak',
						iconCls:'print',
						id:'btnPrint_viGzDistribusiPasien',
						disabled:true,
						handler:function()
						{							
								
						},
						menu: new Ext.menu.Menu({
						items: [
							// these items will render as dropdown menu items when the arrow is clicked:
							{
								xtype: 'button',
								text: 'Print Bill',
								id: 'btnPrintBillGzDistribusiPasien',
								handler: function()
								{
									printbill();
								}
							},
						]
						})
					},
					{
						xtype:'tbseparator'
					}
					
				]
			}//,items:
		}
    )

    return pnlFormDataBasic_viGzDistribusiPasien;
}

function getItemPanelInputBiodata_viGzDistribusiPasien(lebar) {
    var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 90,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'No. Distribusi'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 0,
								xtype: 'textfield',
								id: 'txtNoDistribusiGzDistribusiPasienL',
								name: 'txtNoDistribusiGzDistribusiPasienL',
								width: 130,
								readOnly:true,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									}
								}
							},
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'Tanggal'
							},
							{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 30,
								xtype: 'datefield',
								id: 'dfTglDistribusiGzDistribusiPasienL',
								format: 'd/M/Y',
								width: 130,
								tabIndex:2,
								readOnly:true,
								value:now_viGzDistribusiPasien,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									}
								}
							},
							{
								x: 10,
								y: 60,
								xtype: 'checkbox',
								id: 'chkPasienGzDistribusiPasienL',
								name: 'chkPasienGzDistribusiPasienL',
								disabled: false,
								autoWidth: false,		
								style: { 'margin-top': '2px' },							
								boxLabel: 'Tampilkan pasien yang sudah dilayani ',
								width: 220,
								handler: function (field, value) {
									if (value === true){
										dsDataGrdPasien_viGzDistribusiPasien.removeAll();
										getGridPasienDilayani();
									} else{
										dsDataGrdPasien_viGzDistribusiPasien.removeAll();
										getGridDetailLoad(Ext.getCmp('txtNoDistribusiGzDistribusiPasienL').getValue());
									}
								}
							}, 
							
							//-------------- ## --------------
							{
								x: 320,
								y: 0,
								xtype: 'label',
								text: 'Ke Ruangan'
							},
							{
								x: 410,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							ComboUnitGzDistribusiPasienLookup(),
							{
								x: 320,
								y: 30,
								xtype: 'label',
								text: 'Waktu'
							},
							{
								x: 410,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							ComboWaktuGzDistribusiPasienLookup(),
							{
								x: 320,
								y: 60,
								xtype: 'label',
								text: 'Ahli Gizi'
							},
							{
								x: 410,
								y: 60,
								xtype: 'label',
								text: ':'
							},
							ComboPetugasGiziGzDistribusiPasienLookup()
							
						]
					}
				]
			}
		]		
	};
    return items;
};

function getItemGridTransaksi_viGzDistribusiPasien(lebar){
    var items ={
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'margin-top: -1px;',
		border:false,
		width: lebar-80,
		height: 375,//300, 
	    tbar:
		[
			{
				text	: 'Add Pasien',
				id		: 'btnAddPasienGzDistribusiPasienL',
				disabled:true,
				tooltip	: nmLookup,
				//disabled: true,
				iconCls	: 'find',
				handler	: function(){
					var records = new Array();
					records.push(new dsDataGrdPasien_viGzDistribusiPasien.recordType());
					dsDataGrdPasien_viGzDistribusiPasien.add(records);
				}
			},{
				xtype: 'tbseparator'
			},{
				xtype: 'button',
				text: 'Delete',
				iconCls: 'remove',
				disabled:true,
				id: 'btnDelete_viGzDistribusiPasien',
				handler: function()
				{
					var line = GzDistribusiPasien.form.Grid.a.getSelectionModel().selection.cell[0];
					var o = dsDataGrdPasien_viGzDistribusiPasien.getRange()[line].data;
					if(dsDataGrdPasien_viGzDistribusiPasien.getCount()>1){
						Ext.Msg.confirm('Warning', 'Apakah data resep obat ini akan dihapus?', function(button){
							if (button == 'yes'){
								if(dsDataGrdPasien_viGzDistribusiPasien.getRange()[line].data.no_out != undefined){
									Ext.Ajax.request
									(
										{
											url: baseURL + "index.php/apotek/functionAPOTEKrwi/hapusBarisGridGzDistribusiPasien",
											params:{no_out:o.no_out, tgl_out:o.tgl_out, kd_prd:o.kd_prd, kd_milik:o.kd_milik, no_urut:o.no_urut} ,
											failure: function(o)
											{
												ShowPesanErrorGzDistribusiPasien('Hubungi Admin', 'Error');
											},	
											success: function(o) 
											{
												var cst = Ext.decode(o.responseText);
												if (cst.success === true) 
												{
													dsDataGrdPasien_viGzDistribusiPasien.removeAt(line);
													GzDistribusiPasien.form.Grid.a.getView().refresh();
													Ext.getCmp('btnSimpan_viGzDistribusiPasien').enable();
													Ext.getCmp('btnSimpanExit_viGzDistribusiPasien').enable();
													Ext.getCmp('btnBayar_viGzDistribusiPasien').disable();
													
												}
												else 
												{
													ShowPesanErrorGzDistribusiPasien('Gagal melakukan penghapusan', 'Error');
												};
											}
										}
										
									)
								}else{
									dsDataGrdPasien_viGzDistribusiPasien.removeAt(line);
									GzDistribusiPasien.form.Grid.a.getView().refresh();
									Ext.getCmp('btnSimpan_viGzDistribusiPasien').enable();
									Ext.getCmp('btnSimpanExit_viGzDistribusiPasien').enable();
								}
							} 
							
						});
					} else{
						ShowPesanErrorGzDistribusiPasien('Data tidak bisa dihapus karena minimal resep 1 obat','Error');
					}
					
				}
			}	
		],
		items:[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:[
					gridDataViewEdit_viGzDistribusiPasien()
				]	
			}
		]
	};
    return items;
};


var a={};
function gridDataViewEdit_viGzDistribusiPasien(){
	chkSelected_viGzDistribusiPasien = new Ext.grid.CheckColumn({
		id: 'chkSelected_viGzDistribusiPasien',
		header: 'Cek',
		align: 'center',						
		dataIndex: 'realisasi',			
		width: 20
	});
	
    var FieldGrdKasir_viGzDistribusiPasien = [];
    dsDataGrdPasien_viGzDistribusiPasien= new WebApp.DataStore({
        fields: FieldGrdKasir_viGzDistribusiPasien
    });
    
    GzDistribusiPasien.form.Grid.a =new Ext.grid.EditorGridPanel({
        store: dsDataGrdPasien_viGzDistribusiPasien,
        height: 340,
        columnLines: true,
		selModel: new Ext.grid.CellSelectionModel ({
            singleSelect: true,
            listeners:{
                rowselect: function(sm, row, rec){
                }
            }
        }),
        stripeRows: true,
        cm:new Ext.grid.ColumnModel([
        	new Ext.grid.RowNumberer(),	
			{
				dataIndex: 'no_minta',
				header: 'No minta',
				sortable: true,
				width: 60,
				editor:new Nci.form.Combobox.autoComplete({
					store	: GzDistribusiPasien.form.ArrayStore.a,
					select	: function(a,b,c){
						var line	= GzDistribusiPasien.form.Grid.a.getSelectionModel().selection.cell[0];
						dsDataGrdPasien_viGzDistribusiPasien.getRange()[line].data.no_minta=b.data.no_minta;
						dsDataGrdPasien_viGzDistribusiPasien.getRange()[line].data.kd_pasien=b.data.kd_pasien;
						dsDataGrdPasien_viGzDistribusiPasien.getRange()[line].data.nama=b.data.nama;
						dsDataGrdPasien_viGzDistribusiPasien.getRange()[line].data.no_kamar=b.data.no_kamar;
						dsDataGrdPasien_viGzDistribusiPasien.getRange()[line].data.nama_kamar=b.data.nama_kamar;
						dsDataGrdPasien_viGzDistribusiPasien.getRange()[line].data.kd_jenis=b.data.kd_jenis;
						dsDataGrdPasien_viGzDistribusiPasien.getRange()[line].data.jenis_diet=b.data.jenis_diet;
						dsDataGrdPasien_viGzDistribusiPasien.getRange()[line].data.kd_waktu=b.data.kd_waktu;
						
						if(b.data.realisasi == false || b.data.realisasi == 'f'){
							dsDataGrdPasien_viGzDistribusiPasien.getRange()[line].data.realisasi=false;
						} else{
							dsDataGrdPasien_viGzDistribusiPasien.getRange()[line].data.realisasi=b.data.realisasi;
						}
						
						GzDistribusiPasien.form.Grid.a.getView().refresh();
						
						Ext.getCmp('btnDelete_viGzDistribusiPasien').enable();
						Ext.getCmp('btnSimpan_viGzDistribusiPasien').enable();
						Ext.getCmp('btnSimpanExit_viGzDistribusiPasien').enable();
						Ext.getCmp('cbo_UnitGzDistribusiPasienLookup').setReadOnly(true);
						Ext.getCmp('cbo_WaktuGzDistribusiPasien').setReadOnly(true);
						Ext.getCmp('cbo_PetugasiGzDistribusiPasienLookup').setReadOnly(true);
						
					},
					insert	: function(o){
						return {
							//md.no_minta,md.kd_pasien, p.nama, m.kd_unit, m.no_kamar, k.nama_kamar, md.realisasi, md.kd_waktu,w.waktu,md.kd_jenis,j.jenis_diet
							no_minta        : o.no_minta,
							kd_pasien 		: o.kd_pasien,
							nama			: o.nama,
							no_kamar		: o.no_kamar,
							nama_kamar		: o.nama_kamar,
							realisasi		: o.realisasi,
							kd_jenis		: o.kd_jenis,
							jenis_diet		: o.jenis_diet,
							kd_waktu		: o.kd_waktu,
							text			:  '<table style="font-size: 11px;"><tr><td width="100">'+o.no_minta+'</td><td width="100">'+o.kd_pasien+'</td><td width="200">'+o.nama+'</td><td width="150">'+o.jenis_diet+'</td></tr></table>'
						}
					},
					param:function(){
							return {
								kd_waktu:Ext.getCmp('cbo_WaktuGzDistribusiPasien').getValue(),
								kd_unit:Ext.getCmp('cbo_UnitGzDistribusiPasienLookup').getValue()
							}
					},
					url		: baseURL + "index.php/gizi/functionDistribusiPasien/getAutoComGridDetail",
					valueField: 'no_minta',
					displayField: 'text',
					listWidth: 450
				})
			},
			{
				dataIndex: 'kd_pasien',
				header: 'Kode Pasien',
				sortable: true,
				width: 60
			},
			{			
				dataIndex: 'nama',
				header: 'Nama Pasien',
				sortable: true,
				width: 150
			},
			{
				dataIndex: 'no_kamar',
				header: 'No Kamar',
				width: 45,
			},{
				dataIndex: 'nama_kamar',
				header: 'Kamar',
				sortable: true,
				width: 60
			},{
				dataIndex: 'jenis_diet',
				header: 'Jenis Diet',
				sortable: true,
				width: 70
			},
			chkSelected_viGzDistribusiPasien,
			//-------------- HIDDEN --------------
			{
				dataIndex: 'kd_jenis',
				header: 'kd_jenis',
				hidden: true,
				width: 80
			},
			{
				dataIndex: 'kd_waktu',
				header: 'kd_waktu',
				hidden: true,
				width: 80
			}
			//-------------- ## --------------
        ]),
		 plugins:chkSelected_viGzDistribusiPasien,
		viewConfig:{
			forceFit: true
		}
    });
    return GzDistribusiPasien.form.Grid.a;
}

function ComboPetugasGiziGzDistribusiPasienLookup()
{
    var Field_ahliGiziLookup = ['kd_petugas', 'petugas'];
    ds_PetugasGiziGZPermintaanLookup = new WebApp.DataStore({fields: Field_ahliGiziLookup});
    ds_PetugasGiziGZPermintaanLookup.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'kd_petugas',
					Sortdir: 'ASC',
					target: 'ComboPetugasGizi',
					param: ''
				}
		}
	);
	
    var cbo_PetugasiGzDistribusiPasienLookup = new Ext.form.ComboBox
    (
        {
			x: 420,
			y: 60,
            flex: 1,
			id: 'cbo_PetugasiGzDistribusiPasienLookup',
			valueField: 'kd_petugas',
            displayField: 'petugas',
			store: ds_PetugasGiziGZPermintaanLookup,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:150,
			tabIndex:2,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_PetugasiGzDistribusiPasienLookup;
};

function ComboPetugasGiziGzDistribusiPasien()
{
    var Field_ahliGizi = ['kd_petugas', 'petugas'];
    ds_PetugasGiziGZPermintaan = new WebApp.DataStore({fields: Field_ahliGizi});
    ds_PetugasGiziGZPermintaan.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'kd_petugas',
					Sortdir: 'ASC',
					target: 'ComboPetugasGizi',
					param: ''
				}
		}
	);
	
    var cbo_PetugasGiziGzDistribusiPasien = new Ext.form.ComboBox
    (
        {
			x: 130,
			y: 30,
            flex: 1,
			id: 'cbo_PetugasGiziGzDistribusiPasien',
			valueField: 'kd_petugas',
            displayField: 'petugas',
			store: ds_PetugasGiziGZPermintaan,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:150,
			tabIndex:2,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						tmpkriteria = getCriteriaCariGzDistribusiPasien();
						refreshDistribusiPasien(tmpkriteria);
					} 						
				}
			}
        }
    )    
    return cbo_PetugasGiziGzDistribusiPasien;
};

function ComboUnitGzDistribusiPasien(){
    var Field_Vendor = ['KD_UNIT', 'NAMA_UNIT'];
    ds_unit = new WebApp.DataStore({fields: Field_Vendor});
    ds_unit.load({
        params:{
	        Skip: 0,
	        Take: 1000,
	        Sort: 'kd_unit',
	        Sortdir: 'ASC',
	        target: 'ComboUnitApotek',
	        param: "parent='100' ORDER BY nama_unit"
        }
    });
    var cbo_UnitGzDistribusiPasien = new Ext.form.ComboBox({
			x:410,
			y:0,
            flex: 1,
			id: 'cbo_UnitGzDistribusiPasien',
			valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
			emptyText:'Unit/Ruang',
			store: ds_unit,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:150,
			tabIndex:2,
			listeners:{ 
				'select': function(a,b,c){
					selectSetUnit=b.data.displayText ;
				},
				'specialkey' : function(){
					if (Ext.EventObject.getKey() === 13) {
						tmpkriteria = getCriteriaCariGzDistribusiPasien();
						refreshDistribusiPasien(tmpkriteria);
					} 						
				}
			}
        }
    )    
    return cbo_UnitGzDistribusiPasien;
}

function ComboUnitGzDistribusiPasienLookup()
{
    var Field_Vendor = ['KD_UNIT', 'NAMA_UNIT'];
    ds_unitL = new WebApp.DataStore({fields: Field_Vendor});
    ds_unitL.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'kd_unit',
					Sortdir: 'ASC',
					target: 'ComboUnitApotek',
					param: "parent='100' ORDER BY nama_unit"
				}
		}
	);
	
    var cbo_UnitGzDistribusiPasienLookup = new Ext.form.ComboBox
    (
        {
			x: 420,
			y: 0,
			id: 'cbo_UnitGzDistribusiPasienLookup',
            fieldLabel: 'Unit',
			valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
			store: ds_unitL,
			valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:150,
			tabIndex:3,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					//selectSetUnit=b.data.valueField;
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						/* tmpkriteria = getCriteriaCariApotekResepRWJ();
						refeshRespApotekRWJ(tmpkriteria); */
					} 						
				}
			}
        }
    )    
    return cbo_UnitGzDistribusiPasienLookup;
}

function ComboWaktuGzDistribusiPasienLookup()
{
    var Field_WaktuGzDistribusiPasien = ['kd_waktu', 'waktu'];
    ds_WaktuGzDistribusiPasien = new WebApp.DataStore({fields: Field_WaktuGzDistribusiPasien});
    ds_WaktuGzDistribusiPasien.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sortdir: 'ASC',
					target: 'ComboWaktuGizi',
					param: ""
				}
		}
	);
	
    var cbo_WaktuGzDistribusiPasien = new Ext.form.ComboBox
    (
        {
			x: 420,
			y: 30,
			id: 'cbo_WaktuGzDistribusiPasien',
			valueField: 'kd_waktu',
            displayField: 'waktu',
			store: ds_WaktuGzDistribusiPasien,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:150,
			tabIndex:3,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					Ext.getCmp('btnAddPasienGzDistribusiPasienL').enable();
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_WaktuGzDistribusiPasien;
}

function datainit_viGzDistribusiPasien(rowdata)
{
    var tgl_distribusi2 = rowdata.tgl_distribusi.split(" ");
	
	Ext.getCmp('txtNoDistribusiGzDistribusiPasienL').setValue(rowdata.no_distribusi);
	Ext.getCmp('cbo_UnitGzDistribusiPasienLookup').setValue(rowdata.nama_unit);
	Ext.getCmp('cbo_WaktuGzDistribusiPasien').setValue(rowdata.waktu);
	Ext.getCmp('cbo_PetugasiGzDistribusiPasienLookup').setValue(rowdata.petugas);
	Ext.getCmp('dfTglDistribusiGzDistribusiPasienL').setValue(tgl_distribusi2[0]);
	
	GzDistribusiPasien.vars.kd_unit=rowdata.kd_unit;
	GzDistribusiPasien.vars.kd_waktu=rowdata.kd_waktu;
	getGridDetailLoad(rowdata.no_distribusi);
	//Ext.getCmp('btnSimpan_viGzDistribusiPasien').enable();
	//Ext.getCmp('btnSimpanExit_viGzDistribusiPasien').enable();
	Ext.getCmp('cbo_UnitGzDistribusiPasienLookup').setReadOnly(true);
	Ext.getCmp('cbo_WaktuGzDistribusiPasien').setReadOnly(true);
	Ext.getCmp('cbo_PetugasiGzDistribusiPasienLookup').setReadOnly(true);
};


function dataGriAwal(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionDistribusiPasien/getDataGridAwal",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorGzDistribusiPasien('Error, membaca data grid awal! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dataSource_viGzDistribusiPasien.removeAll();
					var recs=[],
						recType=dataSource_viGzDistribusiPasien.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
						dataSource_viGzDistribusiPasien.add(recs);
					
					
					
					GridDataView_viGzDistribusiPasien.getView().refresh();
				}
				else 
				{
					ShowPesanErrorGzDistribusiPasien('Gagal membaca data grid awal', 'Error');
				};
			}
		}
		
	)
	
}

function getGridDetailLoad(no_distribusi){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionDistribusiPasien/getGridDetailLoad",
			params: {no_distribusi:no_distribusi},
			failure: function(o)
			{
				ShowPesanErrorGzDistribusiPasien('Error, membaca data pasien dilayani! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				
				if (cst.success === true) 
				{
					var recs=[],
						recType=dsDataGrdPasien_viGzDistribusiPasien.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
					dsDataGrdPasien_viGzDistribusiPasien.add(recs);
					
					GzDistribusiPasien.form.Grid.a.getView().refresh();
				}
				else 
				{
					ShowPesanWarningGzDistribusiPasien('Gagal membaca data ini', 'Warning');
				};
			}
		}
		
	)
	
}

function getGridPasienDilayani(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionDistribusiPasien/getGridPasienDilayani",
			params: {
				kd_waktu:GzDistribusiPasien.vars.kd_waktu,
				kd_unit:GzDistribusiPasien.vars.kd_unit,
				no_distribusi:Ext.getCmp('txtNoDistribusiGzDistribusiPasienL').getValue()
			},
			failure: function(o)
			{
				ShowPesanErrorGzDistribusiPasien('Error, membaca data pasien dilayani! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				
				if (cst.success === true) 
				{
					var recs=[],
						recType=dsDataGrdPasien_viGzDistribusiPasien.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
					dsDataGrdPasien_viGzDistribusiPasien.add(recs);
					
					GzDistribusiPasien.form.Grid.a.getView().refresh();
				}
				else 
				{
					ShowPesanWarningGzDistribusiPasien('Gagal membaca data ini', 'Warning');
				};
			}
		}
		
	)
	
}



function datasave_viGzDistribusiPasien(){
	if (ValidasiEntryGzDistribusiPasien(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/gizi/functionDistribusiPasien/save",
				params: getParamGzDistribusiPasien(),
				failure: function(o)
				{
					ShowPesanErrorGzDistribusiPasien('Error, simpan data! Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						dataGriAwal();
						ShowPesanInfoGzDistribusiPasien('Data berhasil disimpan','Information');
						Ext.get('txtNoDistribusiGzDistribusiPasienL').setValue(cst.nodistribusi);
					}
					else 
					{
						if(cst.error =='distribusi'){
							ShowPesanErrorgzPenerimaanOrder('Gagal Menyimpan Data ini. Error simpan distribusi', 'Error');
						} else if(cst.error =='detail'){
							ShowPesanErrorgzPenerimaanOrder('Gagal Menyimpan Data ini. Error simpan, distribusi detail', 'Error');
						} else{
							ShowPesanErrorgzPenerimaanOrder('Gagal Menyimpan Data ini. Error simpan, update', 'Error');
						}
					};
				}
			}
			
		)
	}
}

function printbill()
{
    Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/main/CreateDataObj",
			params: datacetakbill(),
			failure: function(o)
			{	
				ShowPesanErrorGzDistribusiPasien('Error hubungi admin' ,'Error');
			},
			success: function(o)
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					
				}
				else if  (cst.success === false && cst.pesan===0)
				{
					ShowPesanWarningGzDistribusiPasien('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
				}
				else
				{
					ShowPesanErrorGzDistribusiPasien('Gagal print '  + 'Error');
				}
			}
		}
	)
}


function getParamGzDistribusiPasien() 
{
    var params =
	{
		NoDistribusi:Ext.getCmp('txtNoDistribusiGzDistribusiPasienL').getValue(),
		Tanggal:Ext.getCmp('dfTglDistribusiGzDistribusiPasienL').getValue(),		
		KdUnit: Ext.getCmp('cbo_UnitGzDistribusiPasienLookup').getValue(),
		KdWaktu:Ext.getCmp('cbo_WaktuGzDistribusiPasien').getValue(),
		KdPetugas:Ext.getCmp('cbo_PetugasiGzDistribusiPasienLookup').getValue(),
		Qty:dsDataGrdPasien_viGzDistribusiPasien.getCount()
	};
	
	params['jumlah']=dsDataGrdPasien_viGzDistribusiPasien.getCount();
	for(var i = 0 ; i < dsDataGrdPasien_viGzDistribusiPasien.getCount();i++)
	{
		params['no_minta-'+i]=dsDataGrdPasien_viGzDistribusiPasien.data.items[i].data.no_minta;
		params['kd_pasien-'+i]=dsDataGrdPasien_viGzDistribusiPasien.data.items[i].data.kd_pasien
		params['kd_waktu-'+i]=dsDataGrdPasien_viGzDistribusiPasien.data.items[i].data.kd_waktu
		params['kd_jenis-'+i]=dsDataGrdPasien_viGzDistribusiPasien.data.items[i].data.kd_jenis
		params['realisasi-'+i]=dsDataGrdPasien_viGzDistribusiPasien.data.items[i].data.realisasi
	}
	
    return params
};

function datacetakbill(){
	var params =
	{
		Table: 'billprintingGzDistribusiPasien',
		NoResep:Ext.getCmp('txtNoResepGzDistribusiPasienL').getValue(),
		NoOut:Ext.getCmp('txtTmpNooutGzDistribusiPasienL').getValue(),
		TglOut:Ext.getCmp('txtTmpTgloutGzDistribusiPasienL').getValue(),
		KdPasien:Ext.getCmp('txtKdPasienGzDistribusiPasienL').getValue(),
		NamaPasien:GzDistribusiPasien.form.ComboBox.namaPasien.getValue(),
		JenisPasien:Ext.get('cboPilihankelompokPasienAptGzDistribusiPasien').getValue(),
		Kelas:Ext.get('cbo_UnitGzDistribusiPasienL').getValue(),
		Dokter:Ext.get('cbo_DokterGzDistribusiPasien').getValue(),
		Total:Ext.get('txtTotalBayarGzDistribusiPasienL').getValue(),
		Tot:toInteger(Ext.get('txtTotalBayarGzDistribusiPasienL').getValue())
		
	}
	params['jumlah']=dsDataGrdPasien_viGzDistribusiPasien.getCount();
	for(var i = 0 ; i < dsDataGrdPasien_viGzDistribusiPasien.getCount();i++)
	{
		params['nama_obat-'+i]=dsDataGrdPasien_viGzDistribusiPasien.data.items[i].data.nama_obat;
		params['jml-'+i]=dsDataGrdPasien_viGzDistribusiPasien.data.items[i].data.jml;
	}
	
    return params
}

function getCriteriaCariGzDistribusiPasien()//^^^
{
      	 var strKriteria = "";

			if (Ext.get('TxtNoDistribusiFilterGridDataView_viGzDistribusiPasien').getValue() != "")
            {
                strKriteria = " upper(d.no_distribusi) " + "LIKE upper('" + Ext.get('TxtNoDistribusiFilterGridDataView_viGzDistribusiPasien').getValue() +"%')";
            }
            
            if (Ext.get('cbo_PetugasGiziGzDistribusiPasien').getValue() != "")//^^^
            {
				if (strKriteria == "")
				{
					strKriteria = " d.kd_petugas= " + "'" + Ext.getCmp('cbo_PetugasGiziGzDistribusiPasien').getValue() +"'" ;
				}
				else {
					strKriteria += " and d.kd_petugas =" + "'" + Ext.getCmp('cbo_PetugasGiziGzDistribusiPasien').getValue() +"'";
				}
            }
			
			if (Ext.get('dfTglAwalGzDistribusiPasien').getValue() != "" && Ext.get('dfTglAkhirGzDistribusiPasien').getValue() != "")
            {
                if (strKriteria == "")
				{
					strKriteria = " d.tgl_distribusi between '" + Ext.get('dfTglAwalGzDistribusiPasien').getValue() + "' and '" + Ext.get('dfTglAkhirGzDistribusiPasien').getValue() + "'" ;
				}
				else {
					strKriteria += " and d.tgl_distribusi between '" + Ext.get('dfTglAwalGzDistribusiPasien').getValue() + "' and '" + Ext.get('dfTglAkhirGzDistribusiPasien').getValue() + "'" ;
				}
                
            }
			
			if (Ext.get('cbo_UnitGzDistribusiPasien').getValue() != "" && Ext.get('cbo_UnitGzDistribusiPasien').getValue() != 'Unit/Ruang')
            {
				if (strKriteria == "")
				{
					strKriteria = " d.kd_unit ='" + Ext.getCmp('cbo_UnitGzDistribusiPasien').getValue() + "'"  ;
				}
				else {
					strKriteria += " and d.kd_unit ='" + Ext.getCmp('cbo_UnitGzDistribusiPasien').getValue() + "'";
			    }
                
            }
	
		strKriteria= strKriteria + " ORDER BY d.no_distribusi LIMIT 50"
	 return strKriteria;
}


function ValidasiEntryGzDistribusiPasien(modul,mBolHapus)
{
	var x = 1;
	if(dsDataGrdPasien_viGzDistribusiPasien.getCount() === 0){
		ShowPesanWarningGzDistribusiPasien('Daftar distribusi diet tidak boleh kosong, minimal distribusi 1 pasien', 'Warning');
		x = 0;
	}

	for(var i=0; i<dsDataGrdPasien_viGzDistribusiPasien.getCount() ; i++){
		var o=dsDataGrdPasien_viGzDistribusiPasien.getRange()[i].data;
		if(o.realisasi == false){
			ShowPesanWarningGzDistribusiPasien('Kolom cek belum di ceklis', 'Warning');
			x = 0;
		}
		for(var j=i+1; j<dsDataGrdPasien_viGzDistribusiPasien.getCount() ; j++){
			var p=dsDataGrdPasien_viGzDistribusiPasien.getRange()[j].data;
			if(o.kd_pasien == p.kd_pasien && o.kd_jenis == p.kd_jenis && o.kd_waktu == p.kd_waktu && o.no_minta == p.no_minta){
				ShowPesanWarningGzDistribusiPasien('Pasien dengan jenis diet dan waktu yang sama tidak boleh berulang. Hapus salah satu untuk melanjutkan', 'Warning');
				x = 0;
			}
		}
	}
	
	return x;
};

function ValidasiBayarGzDistribusiPasien(modul,mBolHapus)
{
	var x = 1;
	if(Ext.getCmp('cboJenisByrGzDistribusiPasien').getValue() === '' || Ext.getCmp('cboJenisByrGzDistribusiPasien').getValue() === 'TUNAI'){
		ShowPesanWarningGzDistribusiPasien('Pembayaran belum dipilih', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('cboPembayaranRWI').getValue() === '' || Ext.getCmp('cboPembayaranRWI').getValue() === 'Pilih Pembayaran...'){
		ShowPesanWarningGzDistribusiPasien('Jenis pembayaran belum dipilih', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('txtBayarGzDistribusiPasien_Pembayaran').getValue() === ''){
		ShowPesanWarningGzDistribusiPasien('Jumlah pembayaran belum di isi', 'Warning');
		x = 0;
	}
	
	return x;
};


function ShowPesanWarningGzDistribusiPasien(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorGzDistribusiPasien(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:290
		}
	);
};


function ShowPesanInfoGzDistribusiPasien(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};