var dataSource_viGzVendor;
var selectCount_viGzVendor=50;
var NamaForm_viGzVendor="Vendor";
var mod_name_viGzVendor="Vendor";
var now_viGzVendor= new Date();
var rowSelected_viGzVendor;
var setLookUps_viGzVendor;
var tanggal = now_viGzVendor.format("d/M/Y");
var jam = now_viGzVendor.format("H/i/s");
var tmpkriteria;
var DataGridJenisObat;
var GridDataView_viGzVendor;
var kodesetupvendor='';

var CurrentData_viGzVendor =
{
	data: Object,
	details: Array,
	row: 0
};

var GzVendor={};
GzVendor.form={};
GzVendor.func={};
GzVendor.vars={};
GzVendor.func.parent=GzVendor;
GzVendor.form.ArrayStore={};
GzVendor.form.ComboBox={};
GzVendor.form.DataStore={};
GzVendor.form.Record={};
GzVendor.form.Form={};
GzVendor.form.Grid={};
GzVendor.form.Panel={};
GzVendor.form.TextField={};
GzVendor.form.Button={};

GzVendor.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_unit_far', 'nm_unit_far', 'nomor_ro_unit', 'nomorawal','nomor_out_milik'],
	data: []
});

CurrentPage.page = dataGrid_viGzVendor(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dataGrid_viGzVendor(mod_id_viGzVendor){	
 	// Field kiriman dari Project Net.
    var FieldMaster_viGzVendor = 
	[
		'kd_unit_far', 'nm_unit_far', 'nomor_ro_unit', 'nomorawal','nomor_out_milik' 
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viGzVendor = new WebApp.DataStore
	({
        fields: FieldMaster_viGzVendor
    });
    dataGriGzVendor();
    // Grid gizi Perencanaan # --------------
	GridDataView_viGzVendor = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viGzVendor,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:true,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							/*kodesetupvendor=rowSelected_viGzVendor.data.kd_vendor
							alert(kodesetupvendor);*/
							rowSelected_viGzVendor = undefined;
							rowSelected_viGzVendor = dataSource_viGzVendor.getAt(row);
							CurrentData_viGzVendor
							CurrentData_viGzVendor.row = row;
							CurrentData_viGzVendor.data = rowSelected_viGzVendor.data;
							kodesetupvendor=rowSelected_viGzVendor.data.kd_vendor;
							//DataInitGzVendor(rowSelected_viGzVendor.data);
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					kodesetupvendor=rowSelected_viGzVendor.data.kd_vendor;
					rowSelected_viGzVendor = dataSource_viGzVendor.getAt(ridx);
					if (rowSelected_viGzVendor != undefined)
					{
						DataInitGzVendor(rowSelected_viGzVendor.data);
						//setLookUp_viGzVendor(rowSelected_viGzVendor.data);
					}
					else
					{
						//setLookUp_viGzVendor();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Setup Unit
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header: 'Kode Vendor',
						dataIndex: 'kd_vendor',
						hideable:false,
						menuDisabled: true,
						width: 40
						
					},
					//-------------- ## --------------
					{
						header: 'Nama Vendor',
						dataIndex: 'vendor',
						hideable:false,
						menuDisabled: true,
						width: 90
					},
					//-------------- ## --------------
					{
						header: 'Alamat',
						dataIndex: 'alamat',
						hideable:false,
						menuDisabled: true,
						width: 90
					},
					//-------------- ## --------------
					{
						header: 'Telepon',
						dataIndex: 'phone',
						hideable:false,
						menuDisabled: true,
						width: 90
					}
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viGzVendor',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Vendor',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Vendor',
						id: 'btnEdit_viGzVendor',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viGzVendor != undefined)
							{
								DataInitGzVendor(rowSelected_viGzVendor.data);
							}
							
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viGzVendor, selectCount_viGzVendor, dataSource_viGzVendor),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)	
	var PanelTabGzVendor = new Ext.FormPanel({
        labelAlign: 'top',
		layout:'form',
		border: false,  
		autoHeight:true,
        title: '',
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [PanelInputUnit()]
		
	})
	// Kriteria filter pada Grid # --------------
    var FrmData_viGzVendor = new Ext.Panel
    (
		{
			title: NamaForm_viGzVendor,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viGzVendor,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ PanelTabGzVendor,
					GridDataView_viGzVendor],
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAddVendor_viGzVendor',
						handler: function(){
							AddNewGzVendor();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viGzVendor',
						handler: function()
						{
							loadMask.show();
							dataSave_viGzVendor();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viGzVendor',
						handler: function()
						{
							if (kodesetupvendor==='')
							{
								ShowPesanErrorGzVendor('Tidak ada data yang dipilih', 'Error');
							}
							else
							{
								Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
									if (button == 'yes'){
										loadMask.show();
										dataDelete_viGzVendor();
									}
								});
							}
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_viGzVendor',
						handler: function()
						{
							dataSource_viGzVendor.removeAll();
							dataGriGzVendor();
							kodesetupvendor='';
						}
					},
					{
						xtype: 'tbseparator'
					}
					
				]
			}
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_viGzVendor;
    //-------------- # End form filter # --------------
}

function PanelInputUnit(rowdata){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 120,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'Kode vendor'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 0,
								xtype: 'textfield',
								id: 'txtKdVendor_GzVendor',
								name: 'txtKdVendor_GzVendor',
								width: 100,
								allowBlank: false,
								maxLength:3,
								readOnly:true,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									},
									blur: function(a){
										if(a.getValue().length > 3){
											ShowPesanWarningGzVendor('Kode unit tidak boleh lebih dari 3 huruf', 'Warning');
										}
									}
								}
							},
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'Nama vendor'
							},
							{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							GzVendor.vars.nama=new Nci.form.Combobox.autoComplete({
								x: 130,
								y: 30,
								tabIndex:2,
								store	: GzVendor.form.ArrayStore.a,
								select	: function(a,b,c){
									
									Ext.getCmp('txtKdVendor_GzVendor').setValue(b.data.kd_vendor);
									
									GridDataView_viGzVendor.getView().refresh();
									
								},
								onShowList:function(a){
									dataSource_viGzVendor.removeAll();
									
									var recs=[],
									recType=dataSource_viGzVendor.recordType;
										
									for(var i=0; i<a.length; i++){
										recs.push(new recType(a[i]));
									}
									dataSource_viGzVendor.add(recs);
									
								},
								insert	: function(o){
									return {
										kd_vendor   : o.kd_vendor,
										vendor 		: o.vendor,
										alamat 		: o.alamat,
										phone 		: o.phone,
										text			:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_vendor+'</td><td width="200">'+o.vendor+'</td></tr></table>'
									}
								},
								url		: baseURL + "index.php/gizi/functionMasterVendor/getVendorGrid",
								valueField: 'vendor',
								displayField: 'text',
								listWidth: 280
							}),
							{
								x: 10,
								y: 60,
								xtype: 'label',
								text: 'Alamat'
							},
							{
								x: 120,
								y: 60,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 60,
								xtype: 'textfield',
								id: 'txtAlamat_GzVendor',
								name: 'txtAlamat_GzVendor',
								width: 150,
								tabIndex:3
							},
							{
								x: 10,
								y: 90,
								xtype: 'label',
								text: 'No. Telepon'
							},
							{
								x: 120,
								y: 90,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 90,
								xtype: 'textfield',
								id: 'txtTelepon_GzVendor',
								name: 'txtTelepon_GzVendor',
								width: 150,
								tabIndex:4
							},
							
							
						]
					}
				]
			}
		]		
	};
        return items;
}
//------------------end---------------------------------------------------------

function dataGriGzVendor(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionMasterVendor/getVendorGrid",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorGzVendor('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dataSource_viGzVendor.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dataSource_viGzVendor.add(recs);
					
					
					
					GridDataView_viGzVendor.getView().refresh();
				}
				else 
				{
					ShowPesanErrorGzVendor('Gagal membaca data vendor', 'Error');
				};
			}
		}
		
	)
	
}

function dataSave_viGzVendor(){
	if (ValidasiSaveGzVendor(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/gizi/functionMasterVendor/save",
				params: getParamSaveGzVendor(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorGzVendor('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoGzVendor('Berhasil menyimpan data ini','Information');
						dataSource_viGzVendor.removeAll();
						dataGriGzVendor();
						Ext.getCmp('txtKdVendor_GzVendor').setValue(cst.kodevendor);
						kodesetupvendor='';
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorGzVendor('Gagal menyimpan data ini', 'Error');
						dataSource_viGzVendor.removeAll();
						dataGriGzVendor();
					};
				}
			}
			
		)
	}
}

function dataDelete_viGzVendor(){
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/gizi/functionMasterVendor/delete",
				params: getParamDeleteGzVendor(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorGzVendor('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoGzVendor('Berhasil menghapus data ini','Information');
						AddNewGzVendor()
						dataSource_viGzVendor.removeAll();
						dataGriGzVendor();
						kodesetupvendor='';
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorGzVendor('Gagal menghapus data ini', 'Error');
						dataSource_viGzVendor.removeAll();
						dataGriGzVendor();
					};
				}
			}
			
		)
	
}

function AddNewGzVendor(){
	Ext.getCmp('txtKdVendor_GzVendor').setValue('');
	GzVendor.vars.nama.setValue('');
	Ext.getCmp('txtAlamat_GzVendor').setValue('');
	Ext.getCmp('txtTelepon_GzVendor').setValue('');
};

function DataInitGzVendor(rowdata){
	Ext.getCmp('txtKdVendor_GzVendor').setValue(rowdata.kd_vendor);
	GzVendor.vars.nama.setValue(rowdata.vendor);
	Ext.getCmp('txtAlamat_GzVendor').setValue(rowdata.alamat);
	Ext.getCmp('txtTelepon_GzVendor').setValue(rowdata.phone);
};

function getParamSaveGzVendor(){
	var	params =
	{
		KdVendor:Ext.getCmp('txtKdVendor_GzVendor').getValue(),
		NamaVendor:GzVendor.vars.nama.getValue(),
		Alamat:Ext.getCmp('txtAlamat_GzVendor').getValue(),
		Telepon:Ext.getCmp('txtTelepon_GzVendor').getValue()
	}
   
    return params
};

function getParamDeleteGzVendor(){
	var	params =
	{
		KdVendor:kodesetupvendor
	}
   
    return params
};

function ValidasiSaveGzVendor(modul,mBolHapus){
	var x = 1;
	if(GzVendor.vars.nama.getValue() === '' /*|| Ext.getCmp('txtKdVendor_GzVendor').getValue() ===''*/){
		if(GzVendor.vars.nama.getValue() === ''){
			loadMask.hide();
			ShowPesanWarningGzVendor('Nama unit masih kosong', 'Warning');
			x = 0;
		} else{
			loadMask.hide();
			ShowPesanWarningGzVendor('Kode unit masih kosong', 'Warning');
			x = 0;
		}
		
	} 
	if(Ext.getCmp('txtKdVendor_GzVendor').getValue().length > 3){
		ShowPesanWarningGzVendor('Kode unit tidak boleh lebih dari 3 huruf', 'Warning');
		x = 0;
	}
	return x;
};



function ShowPesanWarningGzVendor(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorGzVendor(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoGzVendor(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};