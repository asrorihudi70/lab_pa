var dataSource_viGzOrderDiet;
var selectCount_viGzOrderDiet=50;
var NamaForm_viGzOrderDiet="Order Diet Ke Vendor";
var selectCountStatusPostingGzOrderDiet='Semua';
var mod_name_viGzOrderDiet="viGzOrderDiet";
var now_viGzOrderDiet= new Date();
var addNew_viGzOrderDiet;
var rowSelected_viGzOrderDiet;
var setLookUps_viGzOrderDiet;
var selectSetUnit;
var tanggal = now_viGzOrderDiet.format("d/M/Y");
var jam = now_viGzOrderDiet.format("H/i/s");
var tampungshiftsekarang;
var tmpkriteria;
var GridDataView_viGzOrderDiet;

var CurrentGzOrderDiet =
{
    data: Object,
    details: Array,
    row: 0
};

var CurrentData_viGzOrderDiet =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viGzOrderDiet(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

var GzOrderDiet={};
GzOrderDiet.form={};
GzOrderDiet.func={};
GzOrderDiet.vars={};
GzOrderDiet.func.parent=GzOrderDiet;
GzOrderDiet.form.ArrayStore={};
GzOrderDiet.form.ComboBox={};
GzOrderDiet.form.DataStore={};
GzOrderDiet.form.Record={};
GzOrderDiet.form.Form={};
GzOrderDiet.form.Grid={};
GzOrderDiet.form.Panel={};
GzOrderDiet.form.TextField={};
GzOrderDiet.form.Button={};


GzOrderDiet.form.ArrayStore.nominta=new Ext.data.ArrayStore({
		id: 0,
		fields: [	
					'no_minta'
				],
		data: []
	});


function dataGrid_viGzOrderDiet(mod_idOrder_viGzOrderDiet){	
    var FieldMaster_viGzOrderDiet = 
	[
		'no_order', 'no_minta','kd_vendor', 'vendor', 'tgl_order', 'tgl_makan', 
		'keterangan', 'kd_petugas','petugas'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viGzOrderDiet = new WebApp.DataStore
	({
        fields: FieldMaster_viGzOrderDiet
    });
    dataGriAwal();
    // Grid Apotek Perencanaan # --------------
	GridDataView_viGzOrderDiet = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viGzOrderDiet,
			autoScroll: true,
			columnLines: true,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viGzOrderDiet = undefined;
							rowSelected_viGzOrderDiet = dataSource_viGzOrderDiet.getAt(row);
							CurrentData_viGzOrderDiet
							CurrentData_viGzOrderDiet.row = row;
							CurrentData_viGzOrderDiet.data = rowSelected_viGzOrderDiet.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viGzOrderDiet = dataSource_viGzOrderDiet.getAt(ridx);
					if (rowSelected_viGzOrderDiet != undefined)
					{
						setLookUp_viGzOrderDiet(rowSelected_viGzOrderDiet.data);
					}
					else
					{
						setLookUp_viGzOrderDiet();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Apotek perencanaan
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header: 'No. Order',
						dataIndex: 'no_order',
						sortable: true,
						width: 35
						
					},
					//-------------- ## --------------
					{
						header: 'No. Permintaan',
						dataIndex: 'no_minta',
						sortable: true,
						width: 35
						
					},
					//-------------- ## --------------
					{
						header:'Tgl Minta',
						dataIndex: 'tgl_order',						
						width: 30,
						sortable: true,
						hideable:false,
                        menuDisabled:true,
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.tgl_order);
						}
					},
					//-------------- ## --------------
					{
						header: 'Untuk Tanggal',
						dataIndex: 'tgl_makan',
						sortable: true,
						hideable:false,
                        menuDisabled:true,
						width: 30,
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.tgl_makan);
						}
					},
					//-------------- ## --------------
					{
						header: 'Vendor',
						dataIndex: 'vendor',
						sortable: true,
						width: 50
					},
					//-------------- ## --------------
					{
						header: 'Keterangan',
						dataIndex: 'keterangan',
						sortable: true,
						width: 50
					},
					//-------------- HIDDEN --------------
					{
						header: 'kd_vendor',
						dataIndex: 'kd_vendor',
						sortable: true,
						hidden:true,
						width: 40
					},
					{
						header: 'kd_petugas',
						dataIndex: 'kd_petugas',
						sortable: true,
						hidden:true,
						width: 40
					}
					//-------------- ## --------------
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbarOrder_viGzOrderDiet',
				items: 
				[
					{
						xtype: 'button',
						text: 'Tambah Order ',
						iconCls: 'Edit_Tr',
						tooltip: 'Add Data',
						id: 'btnTambahOrder_viGzOrderDiet',
						handler: function(sm, row, rec)
						{
							setLookUp_viGzOrderDiet();
						}
					},
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEditOrder_viGzOrderDiet',
						handler: function(sm, row, rec)
						{
							/* if (rowSelected_viGzOrderDiet != undefined)
							{
								setLookUp_viGzOrderDiet(rowSelected_viGzOrderDiet.data)
							} */
							setLookUp_viGzOrderDiet(rowSelected_viGzOrderDiet.data);
							
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viGzOrderDiet, selectCount_viGzOrderDiet, dataSource_viGzOrderDiet),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )
	
	var pencarianGzOrderDiet = new Ext.FormPanel({
        labelAlign: 'top',
        frame:true,
        title: '',
        bodyStyle:'padding:5px 5px 0',
        items: [
		{
			layout: 'column',
			border: false,
			items:
			[
				{
					columnWidth:.98,
					layout: 'absolute',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					width: 500,
					height: 70,
					anchor: '100% 100%',
					items:
					[
						{
							x: 10,
							y: 0,
							xtype: 'label',
							text: 'No. Order'
						},
						{
							x: 120,
							y: 0,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 0,
							xtype: 'textfield',
							id: 'TxtNoOrderFilterGridDataView_viGzOrderDiet',
							name: 'TxtNoOrderFilterGridDataView_viGzOrderDiet',
							emptyText: '',
							width: 130,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariGzOrderDiet();
										refreshOrderDiet(tmpkriteria);
									} 						
								}
							}
						},
						{
							x: 10,
							y: 30,
							xtype: 'label',
							text: 'No.Permintaan'
						},
						{
							x: 120,
							y: 30,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 30,
							xtype: 'textfield',
							id: 'txtPermintaanGzOrderDiet',
							name: 'txtPermintaanGzOrderDiet',
							width: 130,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariGzOrderDiet();
										refreshOrderDiet(tmpkriteria);
									} 						
								}
							}
						},
						
						//-------------- ## --------------
						{
							x: 310,
							y: 0,
							xtype: 'label',
							text: 'Tanggal Minta'
						},
						{
							x: 400,
							y: 0,
							xtype: 'label',
							text: ':'
						},
						{
							x: 410,
							y: 0,
							xtype: 'datefield',
							id: 'dfTglAwalOrderGzOrderDiet',
							format: 'd/M/Y',
							width: 120,
							tabIndex:3,
							value:now_viGzOrderDiet,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariGzOrderDiet();
										refreshOrderDiet(tmpkriteria);
									} 						
								}
							}
						},
						{
							x: 540,
							y: 0,
							xtype: 'label',
							text: 's/d'
						},
						{
							x: 568,
							y: 0,
							xtype: 'datefield',
							id: 'dfTglAkhirOrderGzOrderDiet',
							format: 'd/M/Y',
							width: 120,
							tabIndex:4,
							value:now_viGzOrderDiet,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariGzOrderDiet();
										refreshOrderDiet(tmpkriteria);
									} 						
								}
							}
						},
						//----------------------------------------
						{
							x: 568,
							y: 30,
							xtype: 'button',
							text: 'Cari',
							iconCls: 'refresh',
							tooltip: 'Cari',
							style:{paddingLeft:'30px'},
							//rowspan: 3,
							width:150,
							id: 'BtnFilterGridDataView_viGzOrderDiet',
							handler: function() 
							{					
								tmpkriteria = getCriteriaCariGzOrderDiet();
								refreshOrderDiet(tmpkriteria);
							}                        
						}
					]
				}
			]
		}
		]	
	})

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viGzOrderDiet = new Ext.Panel
    (
		{
			title: NamaForm_viGzOrderDiet,
			iconCls: 'Studi_Lanjut',
			id: mod_idOrder_viGzOrderDiet,
			region: 'center',
			layout: 'form', 
			closable: true,        
			border: false,  
			margins: '0 5 5 0',
			items: [ pencarianGzOrderDiet,
					GridDataView_viGzOrderDiet],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viGzOrderDiet,
		            columns: 21,
		            defaults: {
					scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viGzOrderDiet;
    //-------------- # End form filter # --------------
}

function refreshOrderDiet(kriteria)
{
    dataSource_viGzOrderDiet.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'ViewOrderDiet',
                    param : kriteria
                }			
            }
        );   
    return dataSource_viGzOrderDiet;
}

function setLookUp_viGzOrderDiet(rowdata){
    var lebar = 985;
    setLookUps_viGzOrderDiet = new Ext.Window({
        id: Nci.getId(),
        title: NamaForm_viGzOrderDiet, 
        closeAction: 'destroy',        
        width: 900,
        height: 580,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viGzOrderDiet(lebar,rowdata),
        listeners:{
            activate: function(){
				
            },
            afterShow: function(){
                this.activate();
            },
            deactivate: function(){
                rowSelected_viGzOrderDiet=undefined;
            }
        }
    });

    setLookUps_viGzOrderDiet.show();

    if (rowdata == undefined){	
    }
    else
    {
        datainit_viGzOrderDiet(rowdata);
    }
}

function getFormItemEntry_viGzOrderDiet(lebar,rowdata){
    var pnlFormDataBasic_viGzOrderDiet = new Ext.FormPanel({
		title: '',
		region: 'north',
		layout: 'form',
		bodyStyle: 'padding:10px 10px 10px 10px',
		anchor: '100%',
		labelWidth: 1,
		autoWidth: true,
		width: lebar,
		border: false,
		items:[
				getItemPanelInputBiodata_viGzOrderDiet(lebar),
				getItemDetailDiet_viGzOrderDiet(lebar)
			],
			fileUpload: true,
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						id: 'btnAdd_viGzOrderDiet',
						handler: function(){
							dataaddnew_viGzOrderDiet();
						}
					},{
						xtype: 'tbseparator'
					},{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						disabled:true,
						id: 'btnSimpan_viGzOrderDiet',
						handler: function()
						{
							datasave_viGzOrderDiet();
						}
					},{
						xtype: 'tbseparator'
					},{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						disabled:true,
						id: 'btnSimpanExit_viGzOrderDiet',
						handler: function()
						{
							datasave_viGzOrderDiet();
							refreshOrderDiet();
							setLookUps_viGzOrderDiet.close();
						}
					},{
						xtype: 'tbseparator'
					},
					{
						xtype:'splitbutton',
						text:'Cetak',
						iconCls:'print',
						id:'btnPrint_viGzOrderDiet',
						disabled:true,
						handler:function()
						{							
								
						},
						menu: new Ext.menu.Menu({
						items: [
							// these items will render as dropdown menu items when the arrow is clicked:
							{
								xtype: 'button',
								text: 'Print Bill',
								id: 'btnPrintBillGzOrderDiet',
								handler: function()
								{
									printbill();
								}
							},
						]
						})
					},
					{
						xtype:'tbseparator'
					}
					
				]
			}
		}
    )

    return pnlFormDataBasic_viGzOrderDiet;
}

function getItemPanelInputBiodata_viGzOrderDiet(lebar) {
    var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 115,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'No. Order'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 0,
								xtype: 'textfield',
								id: 'txtNoOrderGzOrderDietL',
								name: 'txtNoOrderGzOrderDietL',
								width: 130,
								readOnly:true,
								tabIndex:1
							},
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'No. Permintaan'
							},
							{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							GzOrderDiet.form.ComboBox.noPermintaan= new Nci.form.Combobox.autoComplete({
								store	: GzOrderDiet.form.ArrayStore.nominta,
								select	: function(a,b,c){									
									cekDataPermintaan(b.data.no_minta);
									
									Ext.getCmp('btnSimpan_viGzOrderDiet').enable();
									Ext.getCmp('btnSimpanExit_viGzOrderDiet').enable();
									
									
								},
								width	: 150,
								x: 130,
								y: 30,
								insert	: function(o){
									return {
										text				:  '<table style="font-size: 11px;"><tr><td width="100">'+o.no_minta+'</td></tr></table>',
										no_minta	 		:o.no_minta
									}
								},
								param:function(){
										return {
											untuktanggal:Ext.getCmp('dfTglUntukOrderGzOrderDietL').getValue()
										}
								},
								url		: baseURL + "index.php/gizi/functionOrderDiet/getNoPermintaan",
								valueField: 'no_minta',
								displayField: 'text',
								listWidth: 100
							}),
							{
								x: 10,
								y: 60,
								xtype: 'label',
								text: 'Tanggal Order'
							},
							{
								x: 120,
								y: 60,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 60,
								xtype: 'datefield',
								id: 'dfTglOrderGzOrderDietL',
								format: 'd/M/Y',
								width: 150,
								tabIndex:2,
								value:now_viGzOrderDiet,
								readOnly:true,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									}
								}
							},
							{
								x: 10,
								y: 90,
								xtype: 'label',
								text: 'Untuk Tanggal'
							},
							{
								x: 120,
								y: 90,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 90,
								xtype: 'datefield',
								id: 'dfTglUntukOrderGzOrderDietL',
								format: 'd/M/Y',
								width: 150,
								tabIndex:4,
								value:now_viGzOrderDiet,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									}
								}
							},
							
							//-------------- ## --------------
							{
								x: 320,
								y: 0,
								xtype: 'label',
								text: 'Petugas Gizi'
							},
							{
								x: 410,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							ComboAhliGiziGzOrderDiet(),
							{
								x: 320,
								y: 30,
								xtype: 'label',
								text: 'Vendor'
							},
							{
								x: 410,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							ComboVendorGzOrderDiet(),
							{
								x: 320,
								y: 60,
								xtype: 'label',
								text: 'Keterangan'
							},
							{
								x: 410,
								y: 60,
								xtype: 'label',
								text: ':'
							},
							{
								x: 420,
								y: 60,	
								xtype: 'textarea',
								name: 'txtKetGZPenerimaanL',
								id: 'txtKetGZPenerimaanL',
								width : 280,
								height : 50,
								tabIndex:6
							}
							
						]
					}
				]
			}
		]		
	};
    return items;
};

function getItemDetailDiet_viGzOrderDiet(lebar){
    var items ={
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'margin-top: -1px;',
		border:false,
		width: lebar-80,
		height: 375,//300, 
	    tbar:
		[
			/* {
				text	: 'Add Pasien',
				id		: 'btnAddPasienGzOrderDietL',
				tooltip	: nmLookup,
				disabled: true,
				iconCls	: 'find',
				handler	: function(){
					var records = new Array();
					records.push(new dsDataGrdPermintaan_viGzOrderDiet.recordType());
					dsDataGrdPermintaan_viGzOrderDiet.add(records);
				}
			},{
				xtype: 'tbseparator'
			},{
				xtype: 'button',
				text: 'Delete',
				iconCls: 'remove',
				disabled:true,
				id: 'btnDelete_viGzOrderDiet',
				handler: function()
				{
					var line = GzOrderDiet.form.Grid.a.getSelectionModel().selection.cell[0];
					var o = dsDataGrdPermintaan_viGzOrderDiet.getRange()[line].data;
					if(dsDataGrdPermintaan_viGzOrderDiet.getCount()>1){
						Ext.Msg.confirm('Warning', 'Apakah data resep obat ini akan dihapus?', function(button){
							if (button == 'yes'){
								if(dsDataGrdPermintaan_viGzOrderDiet.getRange()[line].data.no_out != undefined){
									Ext.Ajax.request
									(
										{
											url: baseURL + "index.php/apotek/functionAPOTEKrwi/hapusBarisGridGzOrderDiet",
											params:{no_out:o.no_out, tgl_out:o.tgl_out, kd_prd:o.kd_prd, kd_milik:o.kd_milik, no_urut:o.no_urut} ,
											failure: function(o)
											{
												ShowPesanErrorGzOrderDiet('Hubungi Admin', 'Error');
											},	
											success: function(o) 
											{
												var cst = Ext.decode(o.responseText);
												if (cst.success === true) 
												{
													dsDataGrdPermintaan_viGzOrderDiet.removeAt(line);
													GzOrderDiet.form.Grid.a.getView().refresh();
													hasilJumlahGzOrderDiet();
													Ext.getCmp('btnSimpan_viGzOrderDiet').enable();
													Ext.getCmp('btnSimpanExit_viGzOrderDiet').enable();
													Ext.getCmp('btnBayar_viGzOrderDiet').disable();
													hasilJumlahGzOrderDiet();
													
												}
												else 
												{
													ShowPesanErrorGzOrderDiet('Gagal melakukan penghapusan', 'Error');
												};
											}
										}
										
									)
								}else{
									dsDataGrdPermintaan_viGzOrderDiet.removeAt(line);
									GzOrderDiet.form.Grid.a.getView().refresh();
									Ext.getCmp('btnSimpan_viGzOrderDiet').enable();
									Ext.getCmp('btnSimpanExit_viGzOrderDiet').enable();
								}
							} 
							
						});
					} else{
						ShowPesanErrorGzOrderDiet('Data tidak bisa dihapus karena minimal resep 1 obat','Error');
					}
					
				}
			}	 */
		],
		items:[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:[
					gridDataViewEdit_viGzOrderDiet()
				]	
			}
		]
	};
    return items;
};


var a={};
function gridDataViewEdit_viGzOrderDiet(){
    var FieldGrdKasir_viGzOrderDiet = [];
    dsDataGrdPermintaan_viGzOrderDiet= new WebApp.DataStore({
        fields: FieldGrdKasir_viGzOrderDiet
    });
    
    GzOrderDiet.form.Grid.a =new Ext.grid.EditorGridPanel({
        store: dsDataGrdPermintaan_viGzOrderDiet,
        height: 340,
        columnLines: true,
		selModel: new Ext.grid.CellSelectionModel ({
            singleSelect: true,
            listeners:{
                rowselect: function(sm, row, rec){
                }
            }
        }),
        stripeRows: true,
        cm:new Ext.grid.ColumnModel([
        	new Ext.grid.RowNumberer(),	
			{
				dataIndex: 'no_minta',
				header: 'No Permintaan',
				sortable: true,
				width: 70
			},
			{
				dataIndex: 'jenis_diet',
				header: 'Jenis Diet',
				sortable: true,
				width: 150
			},
			{
				dataIndex: 'waktu',
				header: 'Waktu',
				width: 70
			},{
				dataIndex: 'qty_order',
				header: 'Qty Order',
				sortable: true,
				xtype:'numbercolumn',
				align:'right',
				width: 45,
				editor: new Ext.form.NumberField({
					allowBlank: false,
					listeners:{
						blur: function(a){
							
						},
						focus: function(a){
							
						}
					}
				})	
			},{
				dataIndex: 'qty_minta',
				header: 'Qty Minta',
				xtype:'numbercolumn',
				sortable: true,
				width: 45,
				align:'right'
			},
			{
				dataIndex: 'realisasi',
				header: 'Qty Terima',
				xtype:'numbercolumn',
				sortable: true,
				width: 45,
				align:'right'
			},
			//-------------- HIDDEN --------------
			{
				dataIndex: 'kd_jenis',
				header: 'kode jenis',
				hidden: true,
				width: 80
			},
			{
				dataIndex: 'kd_waktu',
				header: 'Kode waktu',
				hidden: true,
				width: 80
			}
        ]),
		viewConfig:{
			forceFit: true
		}
    });
    return GzOrderDiet.form.Grid.a;
}

function ComboAhliGiziGzOrderDiet()
{
    var Field_ahliGiziGzOrderDiet = ['kd_petugas', 'petugas'];
    ds_ahliGiziGzOrderDiet = new WebApp.DataStore({fields: Field_ahliGiziGzOrderDiet});
    ds_ahliGiziGzOrderDiet.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'kd_petugas',
					Sortdir: 'ASC',
					target: 'ComboPetugasGizi',
					param: ''
				}
		}
	);
	
    var cbo_PetugasGiziGzOrderDiet = new Ext.form.ComboBox
    (
        {
			x: 420,
			y: 0,
            flex: 1,
			id: 'cbo_PetugasGiziGzOrderDiet',
			valueField: 'kd_petugas',
            displayField: 'petugas',
			store: ds_ahliGiziGzOrderDiet,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:150,
			tabIndex:2,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_PetugasGiziGzOrderDiet;
};

function ComboVendorGzOrderDiet()
{
    var Field_VendorGzOrderDiet = ['kd_vendor', 'vendor'];
    ds_VendorGzOrderDiet = new WebApp.DataStore({fields: Field_VendorGzOrderDiet});
    ds_VendorGzOrderDiet.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'kd_vendor',
					Sortdir: 'ASC',
					target: 'ComboVendorGizi',
					param: ''
				}
		}
	);
	
    var cbo_vendorGzOrderDiet = new Ext.form.ComboBox
    (
        {
			x: 420,
			y: 30,
            flex: 1,
			id: 'cbo_vendorGzOrderDiet',
			valueField: 'kd_vendor',
            displayField: 'vendor',
			store: ds_VendorGzOrderDiet,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:150,
			tabIndex:2,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_vendorGzOrderDiet;
};

function datainit_viGzOrderDiet(rowdata)
{
	var tgl_order2 = rowdata.tgl_order.split(" ");
	var tgl_makan2 = rowdata.tgl_makan.split(" ");
	Ext.getCmp('txtNoOrderGzOrderDietL').setValue(rowdata.no_order);
	GzOrderDiet.form.ComboBox.noPermintaan.setValue(rowdata.no_minta);
	Ext.getCmp('dfTglOrderGzOrderDietL').setValue(tgl_order2[0]);
	Ext.getCmp('dfTglUntukOrderGzOrderDietL').setValue(tgl_makan2[0]);
	Ext.getCmp('cbo_vendorGzOrderDiet').setValue(rowdata.vendor);		
	Ext.getCmp('txtKetGZPenerimaanL').setValue(rowdata.keterangan);
	Ext.getCmp('cbo_PetugasGiziGzOrderDiet').setValue(rowdata.petugas);
	
	
	Ext.getCmp('txtNoOrderGzOrderDietL').disable();
	GzOrderDiet.form.ComboBox.noPermintaan.disable();
	Ext.getCmp('dfTglOrderGzOrderDietL').disable();
	Ext.getCmp('dfTglUntukOrderGzOrderDietL').disable();
	Ext.getCmp('cbo_vendorGzOrderDiet').disable();	
	Ext.getCmp('txtKetGZPenerimaanL').disable();
	Ext.getCmp('cbo_PetugasGiziGzOrderDiet').disable();
	
	getGridLoadDataOrderPermintaan(rowdata.no_order);
};

function dataaddnew_viGzOrderDiet(){
	Ext.getCmp('txtNoOrderGzOrderDietL').setValue('');
	GzOrderDiet.form.ComboBox.noPermintaan.setValue('');
	Ext.getCmp('dfTglOrderGzOrderDietL').setValue(now_viGzOrderDiet);
	Ext.getCmp('dfTglUntukOrderGzOrderDietL').setValue(now_viGzOrderDiet);
	Ext.getCmp('cbo_vendorGzOrderDiet').setValue('');		
	Ext.getCmp('txtKetGZPenerimaanL').setValue('');
	Ext.getCmp('cbo_PetugasGiziGzOrderDiet').setValue('');
	
	dsDataGrdPermintaan_viGzOrderDiet.removeAll();
	
	Ext.getCmp('txtNoOrderGzOrderDietL').enable();
	GzOrderDiet.form.ComboBox.noPermintaan.enable();
	Ext.getCmp('dfTglOrderGzOrderDietL').enable();
	Ext.getCmp('dfTglUntukOrderGzOrderDietL').enable();
	Ext.getCmp('cbo_vendorGzOrderDiet').enable();	
	Ext.getCmp('txtKetGZPenerimaanL').enable();
	Ext.getCmp('cbo_PetugasGiziGzOrderDiet').enable();
	
}


function dataGriAwal(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionOrderDiet/getDataGridAwal",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorGzPermintaanDiet('Error, membaca data grid awal! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dataSource_viGzOrderDiet.removeAll();
					var recs=[],
						recType=dataSource_viGzOrderDiet.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
						dataSource_viGzOrderDiet.add(recs);
					
					
					
					GridDataView_viGzOrderDiet.getView().refresh();
				}
				else 
				{
					ShowPesanErrorGzPermintaanDiet('Gagal membaca data grid awal', 'Error');
				};
			}
		}
		
	)
	
}

function cekDataPermintaan(no_minta){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionOrderDiet/cekPermintaan",
			params: {no_minta:no_minta},
			failure: function(o)
			{
				ShowPesanErrorGzOrderDiet('Error, cek data order! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				
				if (cst.success === true) 
				{
					getGridDataPermintaan(no_minta);
				}
				else 
				{
					Ext.getCmp('btnSimpan_viGzOrderDiet').disable();
					Ext.getCmp('btnSimpanExit_viGzOrderDiet').disable();
					ShowPesanErrorGzOrderDiet('No minta ini sudah pernah diOrder, harap periksa kembali no permintaan', 'Error');
					
				};
			}
		}
	)
}


function getGridDataPermintaan(no_minta){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionOrderDiet/getGridDataPemintaan",
			params: {no_minta:no_minta},
			failure: function(o)
			{
				ShowPesanErrorGzOrderDiet('Error, menbaca data permintaan diet! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					var recs=[],
						recType=dsDataGrdPermintaan_viGzOrderDiet.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
					dsDataGrdPermintaan_viGzOrderDiet.add(recs);
					
					GzOrderDiet.form.Grid.a.getView().refresh();
				}
				else 
				{
					ShowPesanErrorGzOrderDiet('Gagal mengambil data permintaan diet', 'Error');
				};
			}
		}
		
	)
	
}
function getGridLoadDataOrderPermintaan(no_order){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionOrderDiet/getGridLoadDataOrderPermintaan",
			params: {no_order:no_order},
			failure: function(o)
			{
				ShowPesanErrorGzOrderDiet('Error, membaca data order permintaan! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					var recs=[],
						recType=dsDataGrdPermintaan_viGzOrderDiet.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
					dsDataGrdPermintaan_viGzOrderDiet.add(recs);
					
					GzOrderDiet.form.Grid.a.getView().refresh();
				}
				else 
				{
					ShowPesanErrorGzOrderDiet('Gagal membaca data order permintaan diet', 'Error');
				};
			}
		}
		
	)
	
}

function datasave_viGzOrderDiet(){
	if (ValidasiEntryGzOrderDiet(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/gizi/functionOrderDiet/save",
				params: getParamGzOrderDiet(),
				failure: function(o)
				{
					ShowPesanErrorGzOrderDiet('Error, simpan data! Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoGzOrderDiet('Data berhasil disimpan','Information');
						Ext.get('txtNoOrderGzOrderDietL').dom.value=cst.noorder;
						
						Ext.getCmp('btnSimpan_viGzOrderDiet').disable();
						Ext.getCmp('btnSimpanExit_viGzOrderDiet').disable();
						
						Ext.getCmp('txtNoOrderGzOrderDietL').disable();
						GzOrderDiet.form.ComboBox.noPermintaan.disable();
						Ext.getCmp('dfTglOrderGzOrderDietL').disable();
						Ext.getCmp('dfTglUntukOrderGzOrderDietL').disable();
						Ext.getCmp('cbo_vendorGzOrderDiet').disable();
						Ext.getCmp('txtKetGZPenerimaanL').disable();
						Ext.getCmp('cbo_PetugasGiziGzOrderDiet').disable();
						dataGriAwal();
					}
					else 
					{
						if(cst.error =='order'){
							ShowPesanErrorGzOrderDiet('Gagal Menyimpan Data ini. Error simpan order', 'Error');
						} else if(cst.error =='order_detail'){
							ShowPesanErrorGzOrderDiet('Gagal Menyimpan Data ini. Error simpan order detail', 'Error');
						} else if(cst.error =='ada'){
							ShowPesanErrorGzOrderDiet('Gagal menyimpan data ini. Permintaan ini sudah pernah diorder', 'Error');
						}
					};
				}
			}
		)
	}
}

function printbill()
{
    Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/main/CreateDataObj",
			params: datacetakbill(),
			failure: function(o)
			{	
				ShowPesanErrorGzOrderDiet('Error hubungi admin' ,'Error');
			},
			success: function(o)
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					
				}
				else if  (cst.success === false && cst.pesan===0)
				{
					ShowPesanWarningGzOrderDiet('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
				}
				else
				{
					ShowPesanErrorGzOrderDiet('Gagal print '  + 'Error');
				}
			}
		}
	)
}


function getParamGzOrderDiet() 
{
    var params =
	{
		NoOrder:Ext.getCmp('txtNoOrderGzOrderDietL').getValue(),
		KdVendor:Ext.getCmp('cbo_vendorGzOrderDiet').getValue(),	
		KdPetugas:Ext.getCmp('cbo_PetugasGiziGzOrderDiet').getValue(),		
		TglOrder: Ext.getCmp('dfTglOrderGzOrderDietL').getValue(),
		TglMakan:Ext.getCmp('dfTglUntukOrderGzOrderDietL').getValue(),
		Ket:Ext.getCmp('txtKetGZPenerimaanL').getValue(),
		
	};
	
	params['jumlah']=dsDataGrdPermintaan_viGzOrderDiet.getCount();
	for(var i = 0 ; i < dsDataGrdPermintaan_viGzOrderDiet.getCount();i++)
	{
		params['no_minta-'+i]=dsDataGrdPermintaan_viGzOrderDiet.data.items[i].data.no_minta;
		params['kd_jenis-'+i]=dsDataGrdPermintaan_viGzOrderDiet.data.items[i].data.kd_jenis
		params['kd_waktu-'+i]=dsDataGrdPermintaan_viGzOrderDiet.data.items[i].data.kd_waktu
		params['qty_minta-'+i]=dsDataGrdPermintaan_viGzOrderDiet.data.items[i].data.qty_minta
		params['qty_order-'+i]=dsDataGrdPermintaan_viGzOrderDiet.data.items[i].data.qty_order
		
	}
	
    return params
};

function datacetakbill(){
	var params =
	{
		Table: 'billprintingGzOrderDiet',
		
		
	}
	params['jumlah']=dsDataGrdPermintaan_viGzOrderDiet.getCount();
	for(var i = 0 ; i < dsDataGrdPermintaan_viGzOrderDiet.getCount();i++)
	{
		params['nama_obat-'+i]=dsDataGrdPermintaan_viGzOrderDiet.data.items[i].data.nama_obat;
		params['jml-'+i]=dsDataGrdPermintaan_viGzOrderDiet.data.items[i].data.jml;
	}
	
    return params
}

function getCriteriaCariGzOrderDiet()//^^^
{
      	 var strKriteria = "";

			if (Ext.get('TxtNoOrderFilterGridDataView_viGzOrderDiet').getValue() != "")
            {
                strKriteria = " upper(o.no_order) " + "LIKE upper('" + Ext.get('TxtNoOrderFilterGridDataView_viGzOrderDiet').getValue() +"%')";
            }
            
            if (Ext.get('txtPermintaanGzOrderDiet').getValue() != "")//^^^
            {
				if (strKriteria == "")
				{
					strKriteria = " upper(od.no_minta) " + "LIKE upper('" + Ext.get('txtPermintaanGzOrderDiet').getValue() +"%')" ;
				}
				else {
					strKriteria += " and upper(od.no_minta) " + "LIKE upper('" + Ext.get('txtPermintaanGzOrderDiet').getValue() +"%')";
				}
            }
			if (Ext.get('dfTglAwalOrderGzOrderDiet').getValue() != "" && Ext.get('dfTglAkhirOrderGzOrderDiet').getValue() != "")
            {
                if (strKriteria == "")
				{
					strKriteria = " o.tgl_order between '" + Ext.get('dfTglAwalOrderGzOrderDiet').getValue() + "' and '" + Ext.get('dfTglAkhirOrderGzOrderDiet').getValue() + "'" ;
				}
				else {
					strKriteria += " and o.tgl_order between '" + Ext.get('dfTglAwalOrderGzOrderDiet').getValue() + "' and '" + Ext.get('dfTglAkhirOrderGzOrderDiet').getValue() + "'" ;
				}
                
            }
			
	
		strKriteria= strKriteria + " ORDER BY od.no_minta limit 50"
	 return strKriteria;
}


function ValidasiEntryGzOrderDiet(modul,mBolHapus)
{
	var x = 1;
	if(Ext.getCmp('cbo_vendorGzOrderDiet').getValue() === '' || dsDataGrdPermintaan_viGzOrderDiet.getCount() === 0 || GzOrderDiet.form.ComboBox.noPermintaan.getValue() === ''){
		if(Ext.getCmp('cbo_vendorGzOrderDiet').getValue() === ''){
			ShowPesanWarningGzOrderDiet('Vendor tidak boleh kosong', 'Warning');
			x = 0;
		} else if(dsDataGrdPermintaan_viGzOrderDiet.getCount() == 0){
			ShowPesanWarningGzOrderDiet('Daftar permintaan diet tidak boleh kosong', 'Warning');
			x = 0;
		} else {
			ShowPesanWarningGzOrderDiet('No Permintaan tidak boleh kosong', 'Warning');
			x = 0;
		} 
	}
	
	for(var i=0; i<dsDataGrdPermintaan_viGzOrderDiet.getCount() ; i++){
		var o=dsDataGrdPermintaan_viGzOrderDiet.getRange()[i].data;
		if(o.qty_order == undefined || o.qty_minta == undefined){
			if(o.qty_order == undefined){
				ShowPesanWarningGzOrderDiet('Qty order belum di isi, qty order tidak boleh kosong', 'Warning');
				x = 0;
			}else if(o.qty_minta == undefined){
				ShowPesanWarningGzOrderDiet('Total obat kosong, harap periksa qty dan kelengkapan pengisian obat', 'Warning');
				x = 0;
			}
		}
	}
	
	return x;
};

function ShowPesanWarningGzOrderDiet(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorGzOrderDiet(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:290
		}
	);
};


function ShowPesanInfoGzOrderDiet(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};