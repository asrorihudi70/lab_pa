var dataSource_viGzPemakaianBahanMakanan;
var selectCount_viGzPemakaianBahanMakanan=50;
var NamaForm_viGzPemakaianBahanMakanan="Pemakaian Bahan Makanan";
var selectCountStatusPostingGzPemakaianBahanMakanan='Semua';
var mod_name_viGzPemakaianBahanMakanan="viGzPemakaianBahanMakanan";
var now_viGzPemakaianBahanMakanan= new Date();
var addNew_viGzPemakaianBahanMakanan;
var rowSelected_viGzPemakaianBahanMakanan;
var setLookUps_viGzPemakaianBahanMakanan;
var tanggal = now_viGzPemakaianBahanMakanan.format("d/M/Y");
var jam = now_viGzPemakaianBahanMakanan.format("H/i/s");
var tampungshiftsekarang;
var tmpkriteria;
var GridDataView_viGzPemakaianBahanMakanan;
var cNoPakai;
var cTglAwal;
var cTglAkhir;
var cJenis;


var CurrentData_viGzPemakaianBahanMakanan =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viGzPemakaianBahanMakanan(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

var GzPemakaianBahanMakanan={};
GzPemakaianBahanMakanan.form={};
GzPemakaianBahanMakanan.func={};
GzPemakaianBahanMakanan.vars={};
GzPemakaianBahanMakanan.func.parent=GzPemakaianBahanMakanan;
GzPemakaianBahanMakanan.form.ArrayStore={};
GzPemakaianBahanMakanan.form.ComboBox={};
GzPemakaianBahanMakanan.form.DataStore={};
GzPemakaianBahanMakanan.form.Record={};
GzPemakaianBahanMakanan.form.Form={};
GzPemakaianBahanMakanan.form.Grid={};
GzPemakaianBahanMakanan.form.Panel={};
GzPemakaianBahanMakanan.form.TextField={};
GzPemakaianBahanMakanan.form.Button={};

GzPemakaianBahanMakanan.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_bahan', 'nama_bahan', 'kd_satuan', 'satuan', 'qty', 'jml_stok'],
	data: []
});

function dataGrid_viGzPemakaianBahanMakanan(mod_id_viGzPemakaianBahanMakanan){	
    var FieldMaster_viGzPemakaianBahanMakanan = 
	[
		'no_minta','tgl_minta','jenis'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viGzPemakaianBahanMakanan = new WebApp.DataStore
	({
        fields: FieldMaster_viGzPemakaianBahanMakanan
    });
    getDataGridAwal()
    // Grid Apotek Perencanaan # --------------
	GridDataView_viGzPemakaianBahanMakanan = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viGzPemakaianBahanMakanan,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viGzPemakaianBahanMakanan = undefined;
							rowSelected_viGzPemakaianBahanMakanan = dataSource_viGzPemakaianBahanMakanan.getAt(row);
							CurrentData_viGzPemakaianBahanMakanan
							CurrentData_viGzPemakaianBahanMakanan.row = row;
							CurrentData_viGzPemakaianBahanMakanan.data = rowSelected_viGzPemakaianBahanMakanan.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viGzPemakaianBahanMakanan = dataSource_viGzPemakaianBahanMakanan.getAt(ridx);
					if (rowSelected_viGzPemakaianBahanMakanan != undefined)
					{
						setLookUp_viGzPemakaianBahanMakanan(rowSelected_viGzPemakaianBahanMakanan.data);
					}
					else
					{
						setLookUp_viGzPemakaianBahanMakanan();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Apotek perencanaan
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header		: 'Status Posting',
						width		: 20,
						sortable	: false,
						hideable	: false,
						menuDisabled: true,
						align		: 'center',
						dataIndex	: 'posted',
						id			: 'colStatusPosting_viGzPenerimaanBahanMakanan',
						renderer	: function(value, metaData, record, rowIndex, colIndex, store){
							 switch (value){
								 case '1':
									 metaData.css = 'StatusHijau'; 
									 break;
								 case '0':
									 metaData.css = 'StatusMerah';
									 break;
							 }
							 return '';
						}
					},
					{
						header: 'No. Pemakaian',
						dataIndex: 'no_pakai',
						hideable:false,
						menuDisabled: true,
						width: 25
						
					},
					//-------------- ## --------------
					{
						header: 'Tanggal Pemakaian',
						dataIndex: 'tgl_pakai',
						hideable:false,
						menuDisabled: true,
						width: 30,
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.tgl_pakai);
						}
					},
					{
						header: 'Jenis Permintaan',
						dataIndex: 'jenis',
						hideable:false,
						menuDisabled: true,
						width: 30
						
					},
					{
						header: 'Keterangan',
						dataIndex: 'keterangan',
						hideable:false,
						menuDisabled: true,
						width: 30
						
					},
					{
						header: 'jenis_pakai',
						dataIndex: 'jenis_pakai',
						hideable:false,
						hidden:true,
						menuDisabled: true,
						width: 30
						
					},
					//-------------- ## --------------
					
					//-------------- ## --------------
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viGzPemakaianBahanMakanan',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add Pemakaian',
						iconCls: 'Edit_Tr',
						tooltip: 'Add Data',
						id: 'btnTambah_viGzPemakaianBahanMakanan',
						handler: function(sm, row, rec)
						{
							setLookUp_viGzPemakaianBahanMakanan();
						}
					},
					{
						xtype: 'button',
						text: 'Edit Pemakaian',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viGzPemakaianBahanMakanan',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viGzPemakaianBahanMakanan != undefined)
							{
								setLookUp_viGzPemakaianBahanMakanan(rowSelected_viGzPemakaianBahanMakanan.data)
							}
							
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viGzPemakaianBahanMakanan, selectCount_viGzPemakaianBahanMakanan, dataSource_viGzPemakaianBahanMakanan),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )
	
	var pencarianGzPemakaianBahanMakanan = new Ext.FormPanel({
        labelAlign: 'top',
        frame:true,
        title: '',
        bodyStyle:'padding:5px 5px 0',
        //width: 600,
        items: [
		{
			layout: 'column',
			border: false,
			items:
			[
				{
					columnWidth:.98,
					layout: 'absolute',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					width: 500,
					height: 90,
					anchor: '100% 100%',
					items:
					[
						{
							x: 10,
							y: 0,
							xtype: 'label',
							text: 'No. Pemakaian'
						},
						{
							x: 120,
							y: 0,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 0,
							xtype: 'textfield',
							id: 'TxtFilterGridNoPemakaianGzPemakaianBahanMakanan',
							name: 'TxtFilterGridNoPemakaianGzPemakaianBahanMakanan',
							width: 350,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										/* tmpkriteria = getCriteriaCariGzPemakaianBahanMakanan();
										refreshGzPemakaianBahanMakanan(tmpkriteria); */
										cNoPakai=Ext.getCmp('TxtFilterGridNoPemakaianGzPemakaianBahanMakanan').getValue();
										cTglAwal=Ext.getCmp('dfTglAwalMintaGzPemakaianBahanMakanan').getValue();
										cTglAkhir=Ext.getCmp('dfTglAkhirMintaGzPemakaianBahanMakanan').getValue();
										cJenis=Ext.getCmp('cbJenisMintaGzPemakaianBahanMakanan').getValue();
										getDataGridAwal(cNoPakai,cTglAwal,cTglAkhir,cJenis);
									} 						
								}
							}
						},
						{
							x: 10,
							y: 30,
							xtype: 'label',
							text: 'Tanggal'
						},
						{
							x: 120,
							y: 30,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 30,
							xtype: 'datefield',
							id: 'dfTglAwalMintaGzPemakaianBahanMakanan',
							format: 'd/M/Y',
							value:now_viGzPemakaianBahanMakanan,
							width: 150,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										/* tmpkriteria = getCriteriaCariGzPemakaianBahanMakanan();
										refreshGzPemakaianBahanMakanan(tmpkriteria); */
										cNoPakai=Ext.getCmp('TxtFilterGridNoPemakaianGzPemakaianBahanMakanan').getValue();
										cTglAwal=Ext.getCmp('dfTglAwalMintaGzPemakaianBahanMakanan').getValue();
										cTglAkhir=Ext.getCmp('dfTglAkhirMintaGzPemakaianBahanMakanan').getValue();
										cJenis=Ext.getCmp('cbJenisMintaGzPemakaianBahanMakanan').getValue();
										getDataGridAwal(cNoPakai,cTglAwal,cTglAkhir,cJenis);
									} 						
								}
							}
						},
						{
							x: 290,
							y: 30,
							xtype: 'label',
							text: 's/d'
						},
						{
							x: 320,
							y: 30,
							xtype: 'datefield',
							id: 'dfTglAkhirMintaGzPemakaianBahanMakanan',
							format: 'd/M/Y',
							value:now_viGzPemakaianBahanMakanan,
							width: 150,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										/* tmpkriteria = getCriteriaCariGzPemakaianBahanMakanan();
										refreshGzPemakaianBahanMakanan(tmpkriteria); */
										cNoPakai=Ext.getCmp('TxtFilterGridNoPemakaianGzPemakaianBahanMakanan').getValue();
										cTglAwal=Ext.getCmp('dfTglAwalMintaGzPemakaianBahanMakanan').getValue();
										cTglAkhir=Ext.getCmp('dfTglAkhirMintaGzPemakaianBahanMakanan').getValue();
										cJenis=Ext.getCmp('cbJenisMintaGzPemakaianBahanMakanan').getValue();
										getDataGridAwal(cNoPakai,cTglAwal,cTglAkhir,cJenis);
									} 						
								}
							}
						},
						{
							x: 10,
							y: 60,
							xtype: 'label',
							text: 'Jenis Minta'
						},
						{
							x: 120,
							y: 60,
							xtype: 'label',
							text: ':'
						},
						comboJenisMintaGzPemakaianBahanMakanan(),
						//----------------------------------------
						{
							x: 483,
							y: 0,
							xtype: 'label',
							text: '* Enter untuk mencari'
						},
						//----------------------------------------
						{
							x: 580,
							y: 30,
							xtype: 'button',
							text: 'Refresh',
							iconCls: 'refresh',
							tooltip: 'Refresh',
							style:{paddingLeft:'30px'},
							width:150,
							id: 'BtnFilterGridCari_viGzPemakaianBahanMakanan',
							handler: function() 
							{					
								getDataGridAwal();
							}                        
						}
					]
				}
			]
		}
		]	
		
	})

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viGzPemakaianBahanMakanan = new Ext.Panel
    (
		{
			title: NamaForm_viGzPemakaianBahanMakanan,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viGzPemakaianBahanMakanan,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ pencarianGzPemakaianBahanMakanan,
					GridDataView_viGzPemakaianBahanMakanan],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viGzPemakaianBahanMakanan,
		            columns: 21,
		            defaults: {
					scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viGzPemakaianBahanMakanan;
    //-------------- # End form filter # --------------
}

function refreshGzPemakaianBahanMakanan(kriteria)
{
    dataSource_viGzPemakaianBahanMakanan.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'ViewGzPemakaianBahanMakanan',
                    param : kriteria
                }			
            }
        );   
    return dataSource_viGzPemakaianBahanMakanan;
}

function setLookUp_viGzPemakaianBahanMakanan(rowdata){
    var lebar = 785;
    setLookUps_viGzPemakaianBahanMakanan = new Ext.Window({
        id: Nci.getId(),
        title: NamaForm_viGzPemakaianBahanMakanan, 
        closeAction: 'destroy',        
        width: 700,
        height: 555,
		constrain:true,
		autoHeight:true,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viGzPemakaianBahanMakanan(lebar,rowdata),
        listeners:{
            activate: function(){
				
            },
            afterShow: function(){
                this.activate();
            },
            deactivate: function(){
                rowSelected_viGzPemakaianBahanMakanan=undefined;
            }
        }
    });

    setLookUps_viGzPemakaianBahanMakanan.show();

    if (rowdata == undefined){
	
    }
    else
    {
        datainit_viGzPemakaianBahanMakanan(rowdata);
    }
}

function getFormItemEntry_viGzPemakaianBahanMakanan(lebar,rowdata){
    var pnlFormDataBasic_viGzPemakaianBahanMakanan = new Ext.FormPanel({
		title: '',
		region: 'north',
		layout: 'form',
		bodyStyle: 'padding:10px 10px 10px 10px',
		anchor: '100%',
		labelWidth: 1,
		autoWidth: true,
		width: lebar,
		border: false,
		items:[
				getItemPanelInputData_viGzPemakaianBahanMakanan(lebar),
				getItemGridDetail_viGzPemakaianBahanMakanan(lebar)
			],
			fileUpload: true,
		tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						disabled: false,
						iconCls: 'add',
						id: 'btnAdd_viGzPemakaianBahanMakanan',
						handler: function(){
							dataaddnew_viGzPemakaianBahanMakanan();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						disabled: true,
						id: 'btnSimpan_viGzPemakaianBahanMakanan',
						handler: function()
						{
							datasave_viGzPemakaianBahanMakanan();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save & Close',
						disabled: true,
						iconCls: 'saveexit',
						id: 'btnSimpanExit_viGzPemakaianBahanMakanan',
						handler: function()
						{
							datasave_viGzPemakaianBahanMakanan();
							refreshGzPemakaianBahanMakanan();
							setLookUps_viGzPemakaianBahanMakanan.close();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						text	: 'Delete',
						id		: 'btnHapusPemakaianBahan_GzPemakaianBahanMakanan',
						tooltip	: nmLookup,
						iconCls	: 'remove',
						disabled: true,
						hidden	: true,
						handler	: function(){
							Ext.Msg.confirm('Hapus pemakaian', 'Apakah data pemakaian ini akan dihapus?', function(button){
								if (button == 'yes'){
									var line = GzPemakaianBahanMakanan.form.Grid.a.getSelectionModel().selection.cell[0];
									var o = dsDataGrdJab_viGzPemakaianBahanMakanan.getRange()[line].data;
									if(dsDataGrdJab_viGzPemakaianBahanMakanan.getCount() > 1){
										dsDataGrdJab_viGzPemakaianBahanMakanan.removeAt(line);
										GzPemakaianBahanMakanan.form.Grid.a.getView().refresh();
									} else{
										ShowPesanErrorGzPemakaianBahanMakanan('Data tidak bisa dihapus karena minimal 1 bahan','Error');
									}
								}
							});
						}
					},	
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Posting',
						disabled: true,
						iconCls: 'gantidok',
						id: 'btnPosting_viGzPemakaianBahanMakanan',
						handler: function()
						{
							dataposting_viGzPemakaianBahanMakanan();
						}
					},
					{
						xtype: 'tbseparator'
					}
					
				]
			}
		}
    )

    return pnlFormDataBasic_viGzPemakaianBahanMakanan;
}

function getItemPanelInputData_viGzPemakaianBahanMakanan(lebar) {
    var items =
	{
		title:'',
		layout:'column',
		items:
		[
			{
				columnWidth: .50,
				layout: 'form',
				labelWidth:110,
				bodyStyle:'padding: 10px',
				border: false,
				items:
				[
					{
						xtype: 'textfield',
						fieldLabel: 'No. Pemakaian ',
						name: 'txtNoPemakaian_GzPemakaianBahanMakananL',
						id: 'txtNoPemakaian_GzPemakaianBahanMakananL',
						readOnly:true,
						tabIndex:0,
						width: 150
					},
					{
						xtype: 'datefield',
						fieldLabel: 'Tanggal Pemakaian',
						name: 'dfTglTerima_GzPemakaianBahanMakananL',
						id	: 'dfTglTerima_GzPemakaianBahanMakananL',
						format: 'd/M/Y',
						value:now_viGzPemakaianBahanMakanan,
						tabIndex:1,
						readOnly:true,
						width: 150
					},
					comboJenisMintaGzPemakaianBahanMakananLookup()
				]
			},
			{
				columnWidth: .50,
				layout: 'form',
				labelWidth:80,
				bodyStyle:'padding: 10px',
				border: false,
				items:
				[
					{
						xtype: 'textarea',
						fieldLabel: 'Keterangan ',
						name: 'txtKeteranan_GzPemakaianBahanMakananL',
						id: 'txtKeteranan_GzPemakaianBahanMakananL',
						width : 210,
						height : 70,
						tabIndex:3
					}
				]
			}
		
		]
	};
    return items;
};

function getItemGridDetail_viGzPemakaianBahanMakanan(lebar) 
{
    var items =
	{
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:1px 1px 1px 1px',
		border:true,
		width: lebar-80,
		height: 250,//225, 
		tbar:
		[
			{
				text	: 'Add Bahan',
				id		: 'btnAddPemakaianBahan',
				tooltip	: nmLookup,
				disabled: true,
				iconCls	: 'find',
				handler	: function(){
					var records = new Array();
					records.push(new dsDataGrdJab_viGzPemakaianBahanMakanan.recordType());
					dsDataGrdJab_viGzPemakaianBahanMakanan.add(records);
					
					
				}
			},	
			{
				text	: 'Delete',
				id		: 'btnHapusGridBahan_GzPemakaianBahanMakanan',
				tooltip	: nmLookup,
				iconCls	: 'remove',
				disabled: true,
				handler	: function(){
					Ext.Msg.confirm('Hapus bahan', 'Apakah bahan ini akan dihapus?', function(button){
						if (button == 'yes'){
							var line = GzPemakaianBahanMakanan.form.Grid.a.getSelectionModel().selection.cell[0];
							var o = dsDataGrdJab_viGzPemakaianBahanMakanan.getRange()[line].data;
							if(dsDataGrdJab_viGzPemakaianBahanMakanan.getCount() > 1){
								dsDataGrdJab_viGzPemakaianBahanMakanan.removeAt(line);
								GzPemakaianBahanMakanan.form.Grid.a.getView().refresh();
							} else{
								ShowPesanErrorGzPemakaianBahanMakanan('Data tidak bisa dihapus karena minimal 1 bahan','Error');
							}
						}
					});
				}
			}	
		],
	    items:
		[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:
				[
					gridDataViewEdit_viGzPemakaianBahanMakanan()
				]	
			}
			//-------------- ## --------------
		]
	};
    return items;
};

function gridDataViewEdit_viGzPemakaianBahanMakanan()
{
    var FieldGrdKasir_viGzPemakaianBahanMakanan = [];
	
    dsDataGrdJab_viGzPemakaianBahanMakanan= new WebApp.DataStore
	({
        fields: FieldGrdKasir_viGzPemakaianBahanMakanan
    });
    
    GzPemakaianBahanMakanan.form.Grid.a =new Ext.grid.EditorGridPanel({
        store: dsDataGrdJab_viGzPemakaianBahanMakanan,
        height: 220,//220,
		stripeRows: true,
		columnLines: true,
		selModel: new Ext.grid.CellSelectionModel({
	            singleSelect: true,
	            listeners: {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
        }),
        
        columns: 
		[	
			new Ext.grid.RowNumberer(),
			{			
				dataIndex: 'kd_bahan',
				header: 'Kode Bahan',
				sortable: true,
				width: 100
			},
			//-------------- ## --------------
			{			
				dataIndex: 'nama_bahan',
				header: 'Nama Bahan',
				sortable: true,
				width: 275,
				editor:new Nci.form.Combobox.autoComplete({
					store	: GzPemakaianBahanMakanan.form.ArrayStore.a,
					select	: function(a,b,c){
						var line	= GzPemakaianBahanMakanan.form.Grid.a.getSelectionModel().selection.cell[0];
						dsDataGrdJab_viGzPemakaianBahanMakanan.getRange()[line].data.kd_bahan=b.data.kd_bahan;
						dsDataGrdJab_viGzPemakaianBahanMakanan.getRange()[line].data.nama_bahan=b.data.nama_bahan;
						dsDataGrdJab_viGzPemakaianBahanMakanan.getRange()[line].data.kd_satuan=b.data.kd_satuan;
						dsDataGrdJab_viGzPemakaianBahanMakanan.getRange()[line].data.satuan=b.data.satuan;
						dsDataGrdJab_viGzPemakaianBahanMakanan.getRange()[line].data.qty=b.data.qty;
						dsDataGrdJab_viGzPemakaianBahanMakanan.getRange()[line].data.jml_stok=b.data.jml_stok;
						
						GzPemakaianBahanMakanan.form.Grid.a.getView().refresh();
						Ext.getCmp('btnSimpan_viGzPemakaianBahanMakanan').enable();
						Ext.getCmp('btnSimpanExit_viGzPemakaianBahanMakanan').enable();
						Ext.getCmp('btnHapusGridBahan_GzPemakaianBahanMakanan').enable();
					},
					insert	: function(o){
						return {
							kd_bahan        : o.kd_bahan,
							nama_bahan 		: o.nama_bahan,
							kd_milik		: o.kd_milik,
							satuan			: o.satuan,
							no_pakai		: o.no_pakai,
							qty				: o.qty,
							jml_stok		: o.jml_stok,
							text			:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_bahan+'</td><td width="200">'+o.nama_bahan+'</td></tr></table>'
						}
					},
					url		: baseURL + "index.php/gizi/functionPemakaianBahan/getBahan",
					valueField: 'nama_bahan',
					displayField: 'text',
					listWidth: 180
				})
			},
			//-------------- ## --------------
			{
				dataIndex: 'satuan',
				header: 'Satuan',
				sortable: true,
				width: 110
			},
			//-------------- ## --------------
			{
				dataIndex: 'qty',
				header: 'Qty Pakai',
				sortable: true,
				align:'right',
				width: 100,
				editor: new Ext.form.NumberField({
					allowBlank: false,
					listeners:{
						blur: function(a){
							/* var line	= this.index;
							dsDataGrdJab_viGzPemakaianBahanMakanan.getRange()[line].data.stok_opname=a.getValue(); */
							var line	= this.index;
							if(a.getValue() == 0 || a.getValue() < 0){
								ShowPesanWarningGzPemakaianBahanMakanan('Qty tidak boleh 0', 'Warning');
							}
							cekQty();
						},
						focus: function(a){
							this.index=GzPemakaianBahanMakanan.form.Grid.a.getSelectionModel().selection.cell[0]
						}
					}
				})	
			},
			//-------------- HIDDEN --------------
			{
				dataIndex: 'kd_satuan',
				header: 'kd_satuan',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'no_pakai',
				header: 'no_pakai',
				hidden: true,
				width: 80
			},
			{
				dataIndex: 'jml_stok',
				header: 'jml_stok',
				hidden: true,
				width: 80
			}
			//-------------- ## --------------
        ],
		viewConfig: 
			{
				forceFit: true
			}

       // plugins:chkSelected_viGzPemakaianBahanMakanan,
    });
    return  GzPemakaianBahanMakanan.form.Grid.a;
}


function comboJenisMintaGzPemakaianBahanMakanan(){
  var cboJenisMintaGzPemakaianBahanMakanan = new Ext.form.ComboBox({
        x: 130,
		y: 60,
		id:'cbJenisMintaGzPemakaianBahanMakanan',
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:true,
		editable: false,
        mode: 'local',
        width: 150,
        fieldLabel: 'Jenis Minta',
		tabIndex:2,
        store: new Ext.data.ArrayStore({
            id: 0,
            fields:[
                'Id',
                'displayText'
            ],
            data: [[1, 'Pasien'],[2, 'Karyawan RS'], [3, 'Keluarga Pasien'], [4, 'Umum']]
        }),
        valueField: 'Id',
        displayField: 'displayText',
        listeners:{
			'select': function(a,b,c){
				cNoPakai=Ext.getCmp('TxtFilterGridNoPemakaianGzPemakaianBahanMakanan').getValue();
				cTglAwal=Ext.getCmp('dfTglAwalMintaGzPemakaianBahanMakanan').getValue();
				cTglAkhir=Ext.getCmp('dfTglAkhirMintaGzPemakaianBahanMakanan').getValue();
				cJenis=Ext.getCmp('cbJenisMintaGzPemakaianBahanMakanan').getValue();
				getDataGridAwal(cNoPakai,cTglAwal,cTglAkhir,cJenis);
			}
        }
	});
	return cboJenisMintaGzPemakaianBahanMakanan;
};


function comboJenisMintaGzPemakaianBahanMakananLookup(){
  var cboJenisMintaGzPemakaianBahanMakananLookup = new Ext.form.ComboBox({
		id:'cbJenisMintaGzPemakaianBahanMakananLookup',
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:true,
		editable: false,
        mode: 'local',
        width: 150,
        fieldLabel: 'Untuk Jenis Minta',
		tabIndex:2,
        store: new Ext.data.ArrayStore({
            id: 0,
            fields:[
                'Id',
                'displayText'
            ],
            data: [[1, 'Pasien'],[2, 'Karyawan RS'], [3, 'Keluarga Pasien'], [4, 'Umum']]
        }),
        valueField: 'Id',
        displayField: 'displayText',
        listeners:{
			'select': function(a,b,c){
				Ext.getCmp('btnAddPemakaianBahan').enable();
			}
        }
	});
	return cboJenisMintaGzPemakaianBahanMakananLookup;
};


function dataaddnew_viGzPemakaianBahanMakanan(){
	Ext.getCmp('txtNoPemakaian_GzPemakaianBahanMakananL').setValue('');
	Ext.getCmp('cbJenisMintaGzPemakaianBahanMakananLookup').setValue('');
	Ext.getCmp('txtKeteranan_GzPemakaianBahanMakananL').setValue('');
	Ext.getCmp('dfTglTerima_GzPemakaianBahanMakananL').setValue(now_viGzPemakaianBahanMakanan);
	
	Ext.getCmp('btnAddPemakaianBahan').disable();
	dsDataGrdJab_viGzPemakaianBahanMakanan.removeAll();
}

function datainit_viGzPemakaianBahanMakanan(rowdata)
{
	if(rowdata.posted == '1'){
		Ext.getCmp('btnSimpan_viGzPemakaianBahanMakanan').disable(true);
		Ext.getCmp('btnSimpanExit_viGzPemakaianBahanMakanan').disable(true);
		Ext.getCmp('btnHapusPemakaianBahan_GzPemakaianBahanMakanan').disable(true);
		Ext.getCmp('btnPosting_viGzPemakaianBahanMakanan').disable(true);
		Ext.getCmp('btnAddPemakaianBahan').disable(true);
		Ext.getCmp('txtKeteranan_GzPemakaianBahanMakananL').setReadOnly(true);
		ShowPesanInfoGzPemakaianBahanMakanan('Data ini sudah diPosting','Information');
	} else{
		Ext.getCmp('btnSimpan_viGzPemakaianBahanMakanan').enable(true);
		Ext.getCmp('btnSimpanExit_viGzPemakaianBahanMakanan').enable(true);
		Ext.getCmp('btnHapusPemakaianBahan_GzPemakaianBahanMakanan').enable(true);
		Ext.getCmp('btnPosting_viGzPemakaianBahanMakanan').enable(true);
		Ext.getCmp('btnAddPemakaianBahan').enable(true);
	}

	Ext.getCmp('txtNoPemakaian_GzPemakaianBahanMakananL').setValue(rowdata.no_pakai);
	Ext.getCmp('dfTglTerima_GzPemakaianBahanMakananL').setValue(ShowDate(rowdata.tgl_pakai));
	Ext.getCmp('cbJenisMintaGzPemakaianBahanMakananLookup').setValue(rowdata.jenis_pakai);
	Ext.getCmp('txtKeteranan_GzPemakaianBahanMakananL').setValue(rowdata.keterangan);
	getGridBahanPakai(rowdata.no_pakai);
	
	Ext.getCmp('cbJenisMintaGzPemakaianBahanMakananLookup').setReadOnly(true);
	
};

function datasave_viGzPemakaianBahanMakanan(){
	if (ValidasiEntryGzPemakaianBahanMakanan(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/gizi/functionPemakaianBahan/save",
				params: getParamGzPemakaianBahanMakanan(),
				failure: function(o)
				{
					ShowPesanErrorGzPemakaianBahanMakanan('Error, simpan data! Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoGzPemakaianBahanMakanan(nmPesanSimpanSukses,nmHeaderSimpanData);
						Ext.getCmp('txtNoPemakaian_GzPemakaianBahanMakananL').setValue(cst.nopakai);
						getDataGridAwal();
						Ext.getCmp('btnSimpan_viGzPemakaianBahanMakanan').disable(true);
						Ext.getCmp('btnSimpanExit_viGzPemakaianBahanMakanan').disable(true);
						Ext.getCmp('btnHapusPemakaianBahan_GzPemakaianBahanMakanan').enable();
						Ext.getCmp('btnPosting_viGzPemakaianBahanMakanan').enable(true);
					}
					else 
					{
						ShowPesanErrorGzPemakaianBahanMakanan('Gagal Menyimpan Data ini', 'Error');
					};
				}
			}
			
		)
	}
}

function dataposting_viGzPemakaianBahanMakanan(){
	Ext.Msg.confirm('Warning', 'Apakah data ini akan diPosting? Pastikan semua data sudah benar sebelum diPosting', function(button){
		if (button == 'yes'){
			Ext.Ajax.request
			(
				{
					url: baseURL + "index.php/gizi/functionPemakaianBahan/posting",
					params: getParamPostingGzPemakaianBahanMakanan(),
					failure: function(o)
					{
						ShowPesanErrorGzPemakaianBahanMakanan('Error, posting! Hubungi Admin', 'Error');
					},	
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoGzPemakaianBahanMakanan('Posting Berhasil','Information');
							Ext.getCmp('btnSimpan_viGzPemakaianBahanMakanan').disable(true);
							Ext.getCmp('btnSimpanExit_viGzPemakaianBahanMakanan').disable(true);
							Ext.getCmp('btnHapusPemakaianBahan_GzPemakaianBahanMakanan').disable(true);
							Ext.getCmp('btnPosting_viGzPemakaianBahanMakanan').disable(true);
							getDataGridAwal();
						}
						else 
						{
							ShowPesanErrorGzPemakaianBahanMakanan('Gagal melakukan Posting', 'Error');
						};
					}
				}
				
			)
		}
	});
}

function getDataGridAwal(cNoPakai,cTglAwal,cTglAkhir,cJenis){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionPemakaianBahan/getGridAwal",
			params: {
				no_pakai:cNoPakai,
				tgl_awal:cTglAwal,
				tgl_akhir:cTglAkhir,
				jenis_pakai:cJenis
			},
			failure: function(o)
			{
				ShowPesanErrorGzPemakaianBahanMakanan('Error, membaca grid awal! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					dataSource_viGzPemakaianBahanMakanan.removeAll();
					
					var recs=[],
						recType=dataSource_viGzPemakaianBahanMakanan.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
						dataSource_viGzPemakaianBahanMakanan.add(recs);
					
					GridDataView_viGzPemakaianBahanMakanan.getView().refresh();
				}
				else 
				{
					ShowPesanErrorGzPemakaianBahanMakanan('Gagal membaca data obat', 'Error');
				};
			}
		}
		
	)
	
}

function getGridBahanPakai(no_pakai){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionPemakaianBahan/getGridBahanLoad",
			params: {no_pakai:no_pakai},
			failure: function(o)
			{
				ShowPesanErrorGzPemakaianBahanMakanan('Error, get bahan! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dsDataGrdJab_viGzPemakaianBahanMakanan.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
						dsDataGrdJab_viGzPemakaianBahanMakanan.add(recs);
					
					
					
					GzPemakaianBahanMakanan.form.Grid.a.getView().refresh();
				}
				else 
				{
					ShowPesanErrorGzPemakaianBahanMakanan('Gagal membaca data bahan', 'Error');
				};
			}
		}
	)
}

function getParamPostingGzPemakaianBahanMakanan(){
	
	var	params =
	{
		NoPakai:Ext.getCmp('txtNoPemakaian_GzPemakaianBahanMakananL').getValue(),
	}
	
	params['jumlah']=dsDataGrdJab_viGzPemakaianBahanMakanan.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viGzPemakaianBahanMakanan.getCount();i++)
	{
		params['kd_bahan-'+i]=dsDataGrdJab_viGzPemakaianBahanMakanan.data.items[i].data.kd_bahan
		params['qty-'+i]=dsDataGrdJab_viGzPemakaianBahanMakanan.data.items[i].data.qty
		params['jml_stok-'+i]=dsDataGrdJab_viGzPemakaianBahanMakanan.data.items[i].data.jml_stok
		
	}
    return params
}

function getParamGzPemakaianBahanMakanan() 
{
	var	params =
	{
		NoPakai:Ext.getCmp('txtNoPemakaian_GzPemakaianBahanMakananL').getValue(),
		TglPakai:Ext.getCmp('dfTglTerima_GzPemakaianBahanMakananL').getValue(),
		JenisPakai:Ext.getCmp('cbJenisMintaGzPemakaianBahanMakananLookup').getValue(),
		Ket:Ext.getCmp('txtKeteranan_GzPemakaianBahanMakananL').getValue()
	}
	
	params['jumlah']=dsDataGrdJab_viGzPemakaianBahanMakanan.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viGzPemakaianBahanMakanan.getCount();i++)
	{
		params['kd_bahan-'+i]=dsDataGrdJab_viGzPemakaianBahanMakanan.data.items[i].data.kd_bahan
		params['qty-'+i]=dsDataGrdJab_viGzPemakaianBahanMakanan.data.items[i].data.qty
		params['jml_stok-'+i]=dsDataGrdJab_viGzPemakaianBahanMakanan.data.items[i].data.jml_stok
		
	}
    return params
};


function getCriteriaCariGzPemakaianBahanMakanan()//^^^
{
	var strKriteria = "";

	if (Ext.get('TxtFilterGridNoPemakaianGzPemakaianBahanMakanan').getValue() != "" && Ext.get('TxtFilterGridNoPemakaianGzPemakaianBahanMakanan').getValue()!=='Nama/Kode obat')
	{
		strKriteria = " so.no_so " + "LIKE '%" + Ext.get('TxtFilterGridNoPemakaianGzPemakaianBahanMakanan').getValue() +"%' ";
	}
		strKriteria= strKriteria + "order by so.no_so desc limit 30"
	return strKriteria;
}


function ValidasiEntryGzPemakaianBahanMakanan(modul,mBolHapus)
{
	var x = 1;
	if(Ext.getCmp('cbJenisMintaGzPemakaianBahanMakananLookup').getValue() === ''){
		ShowPesanWarningGzPemakaianBahanMakanan('Jenis pakai masih kosong', 'Warning');
		x = 0;
	} 
	
	for(var i=0; i<dsDataGrdJab_viGzPemakaianBahanMakanan.getCount() ; i++){
		var o=dsDataGrdJab_viGzPemakaianBahanMakanan.getRange()[i].data;
		
		if(o.qty == undefined || o.qty == 0 || o.qty < 0){
			ShowPesanWarningGzPemakaianBahanMakanan('Qty tidak boleh 0', 'Warning');
			x = 0;
		}
		
		for(var j=i+1; j<dsDataGrdJab_viGzPemakaianBahanMakanan.getCount() ; j++){
			var p=dsDataGrdJab_viGzPemakaianBahanMakanan.getRange()[j].data;
			if(o.kd_bahan == p.kd_bahan){
				ShowPesanWarningGzPemakaianBahanMakanan('Bahan tidak boleh sama, periksa kembali daftar bahan', 'Warning');
				x = 0;
				break;
			}
		}
		
	}
	
	return x;
};

function cekQty(){
	
	for(var i=0; i < dsDataGrdJab_viGzPemakaianBahanMakanan.getCount() ; i++){
		var o=dsDataGrdJab_viGzPemakaianBahanMakanan.getRange()[i].data;
		if(o.qty != undefined){
			if(o.qty >= o.jml_stok){
				ShowPesanWarningGzPemakaianBahanMakanan('Qty melebihi stok bahan, stok '+ o.nama_bahan +' adalah '+ o.jml_stok +' ','Warning');
				o.qty=o.jml_stok;
			} else{
				o.qty=o.qty;
			}
		}
	}
	GzPemakaianBahanMakanan.form.Grid.a.getView().refresh();
}

function ShowPesanWarningGzPemakaianBahanMakanan(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorGzPemakaianBahanMakanan(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};


function ShowPesanInfoGzPemakaianBahanMakanan(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};