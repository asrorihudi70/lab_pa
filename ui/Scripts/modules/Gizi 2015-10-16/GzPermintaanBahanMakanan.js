var dataSource_viGzPermintaanBahanMakanan;
var selectCount_viGzPermintaanBahanMakanan=50;
var NamaForm_viGzPermintaanBahanMakanan="Permintaan Bahan Makanan";
var mod_name_viGzPermintaanBahanMakanan="viGzPermintaanBahanMakanan";
var now_viGzPermintaanBahanMakanan= new Date();
var rowSelected_viGzPermintaanBahanMakanan;
var setLookUps_viGzPermintaanBahanMakanan;
var tanggal = now_viGzPermintaanBahanMakanan.format("d/M/Y");
var jam = now_viGzPermintaanBahanMakanan.format("H/i/s");
var tmpkriteria;
var GridDataView_viGzPermintaanBahanMakanan;


var CurrentData_viGzPermintaanBahanMakanan =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viGzPermintaanBahanMakanan(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

var GzPermintaanBahanMakanan={};
GzPermintaanBahanMakanan.form={};
GzPermintaanBahanMakanan.func={};
GzPermintaanBahanMakanan.vars={};
GzPermintaanBahanMakanan.func.parent=GzPermintaanBahanMakanan;
GzPermintaanBahanMakanan.form.ArrayStore={};
GzPermintaanBahanMakanan.form.ComboBox={};
GzPermintaanBahanMakanan.form.DataStore={};
GzPermintaanBahanMakanan.form.Record={};
GzPermintaanBahanMakanan.form.Form={};
GzPermintaanBahanMakanan.form.Grid={};
GzPermintaanBahanMakanan.form.Panel={};
GzPermintaanBahanMakanan.form.TextField={};
GzPermintaanBahanMakanan.form.Button={};

GzPermintaanBahanMakanan.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_bahan', 'nama_bahan', 'kd_satuan', 'satuan', 'qty'],
	data: []
});

function dataGrid_viGzPermintaanBahanMakanan(mod_id_viGzPermintaanBahanMakanan){	
    var FieldMaster_viGzPermintaanBahanMakanan = 
	[
		'no_minta','tgl_minta', 'jenis_minta','jenis'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viGzPermintaanBahanMakanan = new WebApp.DataStore
	({
        fields: FieldMaster_viGzPermintaanBahanMakanan
    });
    getDataGridAwal();
    // Grid Gizi Perencanaan # --------------
	GridDataView_viGzPermintaanBahanMakanan = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viGzPermintaanBahanMakanan,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viGzPermintaanBahanMakanan = undefined;
							rowSelected_viGzPermintaanBahanMakanan = dataSource_viGzPermintaanBahanMakanan.getAt(row);
							CurrentData_viGzPermintaanBahanMakanan
							CurrentData_viGzPermintaanBahanMakanan.row = row;
							CurrentData_viGzPermintaanBahanMakanan.data = rowSelected_viGzPermintaanBahanMakanan.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viGzPermintaanBahanMakanan = dataSource_viGzPermintaanBahanMakanan.getAt(ridx);
					if (rowSelected_viGzPermintaanBahanMakanan != undefined)
					{
						setLookUp_viGzPermintaanBahanMakanan(rowSelected_viGzPermintaanBahanMakanan.data);
					}
					else
					{
						setLookUp_viGzPermintaanBahanMakanan();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Gizi perencanaan
	        *	Terdiri dari : Judul, Isi dan Event
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header: 'No. Permintaan',
						dataIndex: 'no_minta',
						hideable:false,
						menuDisabled: true,
						width: 25
						
					},
					//-------------- ## --------------
					{
						header: 'Tanggal Minta',
						dataIndex: 'tgl_minta',
						hideable:false,
						menuDisabled: true,
						width: 30,
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.tgl_minta);
						}
					},
					{
						header: 'Jenis Minta',
						dataIndex: 'jenis',
						hideable:false,
						menuDisabled: true,
						width: 30
					},
					{
						header: 'Jenis Minta',
						dataIndex: 'jenis_minta',
						hideable:false,
						menuDisabled: true,
						hidden:true,
						width: 30
					},
					//-------------- ## --------------
					
					//-------------- ## --------------
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viGzPermintaanBahanMakanan',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add Permintaan',
						iconCls: 'Edit_Tr',
						tooltip: 'Add Data',
						id: 'btnTambah_viGzPermintaanBahanMakanan',
						handler: function(sm, row, rec)
						{
							
							setLookUp_viGzPermintaanBahanMakanan();
							Ext.getCmp('btnAdd_viGzPermintaanBahanMakanan').disable();
						}
					},
					{
						xtype: 'button',
						text: 'Edit Permintaan',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viGzPermintaanBahanMakanan',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viGzPermintaanBahanMakanan != undefined)
							{
								setLookUp_viGzPermintaanBahanMakanan(rowSelected_viGzPermintaanBahanMakanan.data)
							}
							
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viGzPermintaanBahanMakanan, selectCount_viGzPermintaanBahanMakanan, dataSource_viGzPermintaanBahanMakanan),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )
	
	var pencarianGzPermintaanBahanMakanan = new Ext.FormPanel({
        labelAlign: 'top',
        frame:true,
        title: '',
        bodyStyle:'padding:5px 5px 0',
        //width: 600,
        items: [
		{
			layout: 'column',
			border: false,
			items:
			[
				{
					columnWidth:.98,
					layout: 'absolute',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					width: 500,
					height: 70,
					anchor: '100% 100%',
					items:
					[
						{
							x: 10,
							y: 0,
							xtype: 'label',
							text: 'No. Permintaan'
						},
						{
							x: 120,
							y: 0,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 0,
							xtype: 'textfield',
							id: 'TxtFilterGridNoPermintaanGzPermintaanBahanMakanan',
							name: 'TxtFilterGridNoPermintaanGzPermintaanBahanMakanan',
							width: 350,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariGzPermintaanBahanMakanan();
										refreshGzPermintaanBahanMakanan(tmpkriteria);
									} 						
								}
							}
						},
						//----------------------------------------
						{
							x: 483,
							y: 0,
							xtype: 'label',
							text: '* Enter untuk mencari'
						},
						//----------------------------------------
						{
							x: 580,
							y: 30,
							xtype: 'button',
							text: 'Refresh',
							iconCls: 'refresh',
							tooltip: 'Refresh',
							style:{paddingLeft:'30px'},
							width:150,
							id: 'BtnFilterGridCari_viGzPermintaanBahanMakanan',
							handler: function() 
							{					
								getDataGridAwal();
							}                        
						},
						{
							x: 10,
							y: 30,
							xtype: 'label',
							text: 'Tanggal'
						},
						{
							x: 120,
							y: 30,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 30,
							xtype: 'datefield',
							id: 'dfTglAwalMintaGzPermintaanBahanMakanan',
							format: 'd/M/Y',
							value:now_viGzPermintaanBahanMakanan,
							width: 150,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariGzPermintaanBahanMakanan();
										refreshGzPermintaanBahanMakanan(tmpkriteria);
									} 						
								}
							}
						},
						{
							x: 290,
							y: 30,
							xtype: 'label',
							text: 's/d'
						},
						{
							x: 320,
							y: 30,
							xtype: 'datefield',
							id: 'dfTglAkhirMintaGzPermintaanBahanMakanan',
							format: 'd/M/Y',
							value:now_viGzPermintaanBahanMakanan,
							width: 150,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariGzPermintaanBahanMakanan();
										refreshGzPermintaanBahanMakanan(tmpkriteria);
									} 						
								}
							}
						}
					]
				}
			]
		}
		]	
		
	})

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viGzPermintaanBahanMakanan = new Ext.Panel
    (
		{
			title: NamaForm_viGzPermintaanBahanMakanan,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viGzPermintaanBahanMakanan,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ pencarianGzPermintaanBahanMakanan,
					GridDataView_viGzPermintaanBahanMakanan],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viGzPermintaanBahanMakanan,
		            columns: 21,
		            defaults: {
					scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viGzPermintaanBahanMakanan;
    //-------------- # End form filter # --------------
}

function refreshGzPermintaanBahanMakanan(kriteria)
{
    dataSource_viGzPermintaanBahanMakanan.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'ViewPermintaanBahan',
                    param : kriteria
                }			
            }
        );   
    return dataSource_viGzPermintaanBahanMakanan;
}

function setLookUp_viGzPermintaanBahanMakanan(rowdata){
    var lebar = 785;
    setLookUps_viGzPermintaanBahanMakanan = new Ext.Window({
        id: Nci.getId(),
        title: NamaForm_viGzPermintaanBahanMakanan, 
        closeAction: 'destroy',        
        width: 700,
        height: 555,
		constrain:true,
		autoHeight:true,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viGzPermintaanBahanMakanan(lebar,rowdata),
        listeners:{
            activate: function(){
				
            },
            afterShow: function(){
                this.activate();
            },
            deactivate: function(){
                rowSelected_viGzPermintaanBahanMakanan=undefined;
            }
        }
    });

    setLookUps_viGzPermintaanBahanMakanan.show();

    if (rowdata == undefined){
	
    }
    else
    {
        datainit_viGzPermintaanBahanMakanan(rowdata);
    }
}

function getFormItemEntry_viGzPermintaanBahanMakanan(lebar,rowdata){
    var pnlFormDataBasic_viGzPermintaanBahanMakanan = new Ext.FormPanel({
		title: '',
		region: 'north',
		layout: 'form',
		bodyStyle: 'padding:10px 10px 10px 10px',
		anchor: '100%',
		labelWidth: 1,
		autoWidth: true,
		width: lebar,
		border: false,
		items:[
				getItemPanelInputData_viGzPermintaanBahanMakanan(lebar),
				getItemGridDetail_viGzPermintaanBahanMakanan(lebar)
			],
			fileUpload: true,
		tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAdd_viGzPermintaanBahanMakanan',
						handler: function(){
							dataaddnew_viGzPermintaanBahanMakanan();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						disabled: true,
						id: 'btnSimpan_viGzPermintaanBahanMakanan',
						handler: function()
						{
							datasave_viGzPermintaanBahanMakanan();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save & Close',
						disabled: true,
						iconCls: 'saveexit',
						id: 'btnSimpanExit_viGzPermintaanBahanMakanan',
						handler: function()
						{
							datasave_viGzPermintaanBahanMakanan();
							refreshGzPermintaanBahanMakanan();
							setLookUps_viGzPermintaanBahanMakanan.close();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						text	: 'Delete',
						id		: 'btnHapusPermintaanBahan_GzPermintaanBahanMakanan',
						tooltip	: nmLookup,
						iconCls	: 'remove',
						disabled: true,
						handler	: function(){
							Ext.Msg.confirm('Hapus permintaan', 'Apakah permintaan ini akan dihapus?', function(button){
								if (button == 'yes'){
									Ext.Ajax.request
									(
										{
											url: baseURL + "index.php/gizi/functionPermintaanBahan/hapusPermintaan",
											params:{no_minta:Ext.getCmp('txtMintaBahan_GzPermintaanBahanMakananL').getValue()} ,
											failure: function(o)
											{
												ShowPesanErrorGzPermintaanBahanMakanan('Error, hapus permintaan! Hubungi Admin', 'Error');
											},	
											success: function(o) 
											{
												var cst = Ext.decode(o.responseText);
												if (cst.success === true) 
												{
													getDataGridAwal();
													setLookUps_viGzPermintaanBahanMakanan.close();
												}
												else 
												{
													ShowPesanErrorGzPermintaanBahanMakanan('Tidak dapat menghapus permintaan ini. Bahan dari permintaan ini sudah diterima', 'Error');
												};
											}
										}
										
									)
								}
							});		
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Posting',
						disabled: true,
						iconCls: 'gantidok',
						id: 'btnPosting_viGzPermintaanBahanMakanan',
						handler: function()
						{
							dataposting_viGzPermintaanBahanMakanan();
						}
					},
					{
						xtype: 'tbseparator'
					}
					
				]
			}
		}
    )

    return pnlFormDataBasic_viGzPermintaanBahanMakanan;
}

function getItemPanelInputData_viGzPermintaanBahanMakanan(lebar) {
    var items =
	{
		title:'',
		layout:'column',
		items:
		[
			{
				columnWidth: .50,
				layout: 'form',
				labelWidth:110,
				bodyStyle:'padding: 10px',
				border: false,
				items:
				[
					{
						xtype: 'textfield',
						fieldLabel: 'No. Permintaan ',
						name: 'txtMintaBahan_GzPermintaanBahanMakananL',
						id: 'txtMintaBahan_GzPermintaanBahanMakananL',
						readOnly:true,
						tabIndex:0,
						width: 150
					},
					{
						xtype: 'datefield',
						fieldLabel: 'Tanggal Minta',
						name: 'dfTglMintaBahan_GzPermintaanBahanMakananL',
						id	: 'dfTglMintaBahan_GzPermintaanBahanMakananL',
						format: 'd/M/Y',
						value:now_viGzPermintaanBahanMakanan,
						tabIndex:1,
						readOnly:true,
						width: 150
					},
					comboJenisMintaGzPermintaanBahanMakanan(),
					/* {
						xtype: 'checkbox',
						boxLabel: 'Menambahkan Stok Sebelumnya',
						id: 'cbAktifMaterObat',
						name: 'cbAktifMaterObat',
						width: 200,
						checked: false,
						hidden:true,
						handler:function(a,b) 
						{
							if(a.checked==true){
								
							}else{
								
							}
						}
					} */
				]
			}
		
		]
	};
    return items;
};

function getItemGridDetail_viGzPermintaanBahanMakanan(lebar) 
{
    var items =
	{
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:1px 1px 1px 1px',
		border:true,
		width: lebar-80,
		height: 250,
		tbar:
		[
			{
				text	: 'Add Bahan',
				id		: 'btnAddPermintaanBahan',
				tooltip	: nmLookup,
				disabled: true,
				iconCls	: 'find',
				handler	: function(){
					var records = new Array();
					records.push(new dsDataGrdJab_viGzPermintaanBahanMakanan.recordType());
					dsDataGrdJab_viGzPermintaanBahanMakanan.add(records);
				}
			},	
			{
				text	: 'Delete',
				id		: 'btnHapusGridBahan_GzPermintaanBahanMakanan',
				tooltip	: nmLookup,
				iconCls	: 'remove',
				disabled: true,
				handler: function()
				{
					var line =  GzPermintaanBahanMakanan.form.Grid.a.getSelectionModel().selection.cell[0];
					var o = dsDataGrdJab_viGzPermintaanBahanMakanan.getRange()[line].data;
					if(dsDataGrdJab_viGzPermintaanBahanMakanan.getCount()>1){
						Ext.Msg.confirm('Warning', 'Apakah bahan ini akan dihapus?', function(button){
							if (button == 'yes'){
								if(dsDataGrdJab_viGzPermintaanBahanMakanan.getRange()[line].data.no_minta != undefined){
									Ext.Ajax.request
									(
										{
											url: baseURL + "index.php/gizi/functionPermintaanBahan/hapusBarisGridBahan",
											params: {
												no_minta:Ext.getCmp('txtMintaBahan_GzPermintaanBahanMakananL').getValue(),
												kd_bahan:o.kd_bahan
											},
											failure: function(o)
											{
												ShowPesanErrorGzPermintaanBahanMakanan('Error, hapus bahan! Hubungi Admin', 'Error');
											},	
											success: function(o) 
											{
												var cst = Ext.decode(o.responseText);
												if (cst.success === true) 
												{
													dsDataGrdJab_viGzPermintaanBahanMakanan.removeAt(line);
													GzPermintaanBahanMakanan.form.Grid.a.getView().refresh();
													Ext.getCmp('btnSimpan_viGzPermintaanBahanMakanan').enable();
													Ext.getCmp('btnSimpanExit_viGzPermintaanBahanMakanan').enable();
												}
												else 
												{
													ShowPesanErrorGzPermintaanBahanMakanan('Gagal menghapus data ini', 'Error');
												};
											}
										}
										
									)
								}else{
									dsDataGrdJab_viGzPermintaanBahanMakanan.removeAt(line);
									GzPermintaanBahanMakanan.form.Grid.a.getView().refresh();
									Ext.getCmp('btnSimpan_viGzPermintaanBahanMakanan').enable();
									Ext.getCmp('btnSimpanExit_viGzPermintaanBahanMakanan').enable();
								}
							} 
							
						});
					} else{
						ShowPesanErrorGzPermintaanBahanMakanan('Data tidak bisa dihapus karena minimal bahan 1','Error');
					}
					
				}
			}	
		],
	    items:
		[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:
				[
					gridDataViewEdit_viGzPermintaanBahanMakanan()
				]	
			}
			//-------------- ## --------------
		]
	};
    return items;
};

function gridDataViewEdit_viGzPermintaanBahanMakanan()
{
    var FieldGrdKasir_viGzPermintaanBahanMakanan = [];
	
    dsDataGrdJab_viGzPermintaanBahanMakanan= new WebApp.DataStore
	({
        fields: FieldGrdKasir_viGzPermintaanBahanMakanan
    });
    
    GzPermintaanBahanMakanan.form.Grid.a =new Ext.grid.EditorGridPanel({
        store: dsDataGrdJab_viGzPermintaanBahanMakanan,
        height: 220,//220,
		stripeRows: true,
		columnLines: true,
		selModel: new Ext.grid.CellSelectionModel({
	            singleSelect: true,
	            listeners: {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
        }),
        
        columns: 
		[	
			new Ext.grid.RowNumberer(),
			{			
				dataIndex: 'kd_bahan',
				header: 'Kode Bahan',
				sortable: true,
				width: 100
			},
			//-------------- ## --------------
			{			
				dataIndex: 'nama_bahan',
				header: 'Nama Bahan',
				sortable: true,
				width: 200,
				editor:new Nci.form.Combobox.autoComplete({
					store	: GzPermintaanBahanMakanan.form.ArrayStore.a,
					select	: function(a,b,c){
						var line	= GzPermintaanBahanMakanan.form.Grid.a.getSelectionModel().selection.cell[0];
						dsDataGrdJab_viGzPermintaanBahanMakanan.getRange()[line].data.kd_bahan=b.data.kd_bahan;
						dsDataGrdJab_viGzPermintaanBahanMakanan.getRange()[line].data.nama_bahan=b.data.nama_bahan;
						dsDataGrdJab_viGzPermintaanBahanMakanan.getRange()[line].data.kd_satuan=b.data.kd_satuan;
						dsDataGrdJab_viGzPermintaanBahanMakanan.getRange()[line].data.satuan=b.data.satuan;
						dsDataGrdJab_viGzPermintaanBahanMakanan.getRange()[line].data.qty=b.data.qty;
						dsDataGrdJab_viGzPermintaanBahanMakanan.getRange()[line].data.no_minta=b.data.no_minta;
						
						Ext.getCmp('btnAdd_viGzPermintaanBahanMakanan').enable();
						Ext.getCmp('btnHapusGridBahan_GzPermintaanBahanMakanan').enable();
						Ext.getCmp('btnSimpan_viGzPermintaanBahanMakanan').enable();
						Ext.getCmp('btnSimpanExit_viGzPermintaanBahanMakanan').enable();
						
						GzPermintaanBahanMakanan.form.Grid.a.getView().refresh();
					},
					insert	: function(o){
						return {
							kd_bahan        : o.kd_bahan,
							nama_bahan 		: o.nama_bahan,
							kd_satuan		: o.kd_satuan,
							satuan			: o.satuan,
							qty				: o.qty,
							no_minta		: o.no_minta,
							text			:  '<table style="font-size: 11px;"><tr><td width="60">'+o.kd_bahan+'</td><td width="150">'+o.nama_bahan+'</td></tr></table>'
						}
					},
					url		: baseURL + "index.php/gizi/functionPermintaanBahan/getBahan",
					valueField: 'nama_bahan',
					displayField: 'text',
					listWidth: 210
				})
			},
			//-------------- ## --------------
			{
				dataIndex: 'satuan',
				header: 'Satuan',
				sortable: true,
				width: 90
			},
			//-------------- ## --------------
			{
				dataIndex: 'qty',
				header: 'Qty',
				sortable: true,
				align:'right',
				width: 70,
				editor: new Ext.form.NumberField({
					allowBlank: false,
					listeners:{
						blur: function(a){
							/* var line	= this.index;
							dsDataGrdJab_viGzPermintaanBahanMakanan.getRange()[line].data.stok_opname=a.getValue(); */
							var line	= this.index;
							if(a.getValue() == 0){
								ShowPesanWarningGzPermintaanBahanMakanan('qty tidak boleh 0', 'Warning');
							}
						},
						focus: function(a){
							this.index=GzPermintaanBahanMakanan.form.Grid.a.getSelectionModel().selection.cell[0]
						}
					}
				})	
			},
			//-------------- ## --------------
			{
				dataIndex: 'ket_spek',
				header: 'Spesifikasi Khusus',
				sortable: true,
				width: 130,
				editor: new Ext.form.TextField({
					allowBlank: false,
					listeners:{
						blur: function(a){
							/* var line	= this.index;
							dsDataGrdJab_viGzPermintaanBahanMakanan.getRange()[line].data.stok_opname=a.getValue(); */
							/* var line	= this.index;
							if(a.getValue() == 0){
								ShowPesanWarningGzPermintaanBahanMakanan('qty tidak boleh 0', 'Warning');
							} */
						},
						focus: function(a){
							this.index=GzPermintaanBahanMakanan.form.Grid.a.getSelectionModel().selection.cell[0]
						}
					}
				})	
			},
			//-------------- ## --------------
			{
				dataIndex: 'kd_satuan',
				header: 'kd_satuan',
				hidden: true,
				width: 80
			},
			{
				dataIndex: 'no_minta',
				header: 'no_minta',
				hidden: true,
				width: 80
			}
			//-------------- ## --------------
        ],
		viewConfig: 
			{
				forceFit: true
			}

       // plugins:chkSelected_viGzPermintaanBahanMakanan,
    });
    return  GzPermintaanBahanMakanan.form.Grid.a;
}


function comboJenisMintaGzPermintaanBahanMakanan(){
  var cboJenisMintaGzPermintaanBahanMakanan = new Ext.form.ComboBox({
        id:'cbJenisMintaGzPermintaanBahanMakanan',
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:true,
		editable: false,
        mode: 'local',
        width: 150,
        fieldLabel: 'Jenis Minta',
		tabIndex:2,
        store: new Ext.data.ArrayStore({
            id: 0,
            fields:[
                'Id',
                'displayText'
            ],
            data: [[1, 'Pasien'],[2, 'Karyawan RS'], [3, 'Keluarga Pasien'], [4, 'Umum']]
        }),
        valueField: 'Id',
        displayField: 'displayText',
        listeners:{
			'select': function(a,b,c){
				Ext.getCmp('btnAddPermintaanBahan').enable();
			}
        }
	});
	return cboJenisMintaGzPermintaanBahanMakanan;
};


function dataaddnew_viGzPermintaanBahanMakanan(){
	Ext.getCmp('txtMintaBahan_GzPermintaanBahanMakananL').setValue('');
	Ext.getCmp('dfTglMintaBahan_GzPermintaanBahanMakananL').setValue(now_viGzPermintaanBahanMakanan);
	Ext.getCmp('cbJenisMintaGzPermintaanBahanMakanan').setValue('');
	
	Ext.getCmp('cbJenisMintaGzPermintaanBahanMakanan').setReadOnly(false);
	
	dsDataGrdJab_viGzPermintaanBahanMakanan.removeAll();
}

function datainit_viGzPermintaanBahanMakanan(rowdata)
{
	Ext.getCmp('txtMintaBahan_GzPermintaanBahanMakananL').setValue(rowdata.no_minta);
	Ext.getCmp('dfTglMintaBahan_GzPermintaanBahanMakananL').setValue(ShowDate(rowdata.tgl_minta));
	Ext.getCmp('cbJenisMintaGzPermintaanBahanMakanan').setValue(rowdata.jenis_minta);
	
	Ext.getCmp('txtMintaBahan_GzPermintaanBahanMakananL').setReadOnly(true);
	Ext.getCmp('dfTglMintaBahan_GzPermintaanBahanMakananL').setReadOnly(true);
	Ext.getCmp('cbJenisMintaGzPermintaanBahanMakanan').setReadOnly(true);
	
	getGridBahan(rowdata.no_minta);
	cekTerimaBahan(rowdata.no_minta);
	
};

function datasave_viGzPermintaanBahanMakanan(){
	if (ValidasiEntryGzPermintaanBahanMakanan(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/gizi/functionPermintaanBahan/save",
				params: getParamGzPermintaanBahanMakanan(),
				failure: function(o)
				{
					ShowPesanErrorGzPermintaanBahanMakanan('Error, simpan data! Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoGzPermintaanBahanMakanan('Data berhasil disimpan','Information');
						Ext.getCmp('txtMintaBahan_GzPermintaanBahanMakananL').setValue(cst.nominta);
						getDataGridAwal();
						Ext.getCmp('btnSimpan_viGzPermintaanBahanMakanan').enable();
						Ext.getCmp('btnSimpanExit_viGzPermintaanBahanMakanan').enable();
						Ext.getCmp('btnHapusPermintaanBahan_GzPermintaanBahanMakanan').enable();
					}
					else 
					{
						ShowPesanErrorGzPermintaanBahanMakanan('Gagal Menyimpan Data ini', 'Error');
					};
				}
			}
			
		)
	}
}

function getDataGridAwal(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionPermintaanBahan/getGridAwal",
			params: {query:''},
			failure: function(o)
			{
				ShowPesanErrorGzPermintaanBahanMakanan('Error, membaca grid awal! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					dataSource_viGzPermintaanBahanMakanan.removeAll();
					
					var recs=[],
						recType=dataSource_viGzPermintaanBahanMakanan.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
						dataSource_viGzPermintaanBahanMakanan.add(recs);
					
					GridDataView_viGzPermintaanBahanMakanan.getView().refresh();
				}
				else 
				{
					ShowPesanErrorGzPermintaanBahanMakanan('Gagal membaca data obat', 'Error');
				};
			}
		}
		
	)
	
}


function getGridBahan(no_minta){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionPermintaanBahan/getGridBahanLoad",
			params: {no_minta:no_minta},
			failure: function(o)
			{
				ShowPesanErrorGzPermintaanBahanMakanan('Error, membaca grid bahan! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dsDataGrdJab_viGzPermintaanBahanMakanan.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
						dsDataGrdJab_viGzPermintaanBahanMakanan.add(recs);
					
					 GzPermintaanBahanMakanan.form.Grid.a.getView().refresh();
				}
				else 
				{
					ShowPesanErrorGzPermintaanBahanMakanan('Gagal membaca data obat', 'Error');
				};
			}
		}
		
	)
	
}

function cekTerimaBahan(no_minta){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionPermintaanBahan/cekTerimaBahan",
			params: {no_minta:no_minta},
			failure: function(o)
			{
				ShowPesanErrorGzPermintaanBahanMakanan('Error, cek! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					Ext.getCmp('btnAddPermintaanBahan').enable();
					Ext.getCmp('btnHapusGridBahan_GzPermintaanBahanMakanan').enable();
					Ext.getCmp('btnSimpan_viGzPermintaanBahanMakanan').enable();
					Ext.getCmp('btnSimpanExit_viGzPermintaanBahanMakanan').enable();
					Ext.getCmp('btnHapusPermintaanBahan_GzPermintaanBahanMakanan').enable();
				}
				else 
				{
					ShowPesanInfoGzPermintaanBahanMakanan('Bahan-bahan dari permintaan ini sudah diterima', 'Information');
					Ext.getCmp('btnSimpan_viGzPermintaanBahanMakanan').disable(true);
					Ext.getCmp('btnSimpanExit_viGzPermintaanBahanMakanan').disable(true);
					Ext.getCmp('btnHapusPermintaanBahan_GzPermintaanBahanMakanan').disable(true);
					Ext.getCmp('btnAddPermintaanBahan').disable(true);
					Ext.getCmp('btnHapusGridBahan_GzPermintaanBahanMakanan').disable(true);
				};
			}
		}
		
	)
}

function getParamGzPermintaanBahanMakanan() 
{
	var	params =
	{
		NoMinta:Ext.getCmp('txtMintaBahan_GzPermintaanBahanMakananL').getValue(),
		TglMinta:Ext.getCmp('dfTglMintaBahan_GzPermintaanBahanMakananL').getValue(),
		JenisMinta:Ext.getCmp('cbJenisMintaGzPermintaanBahanMakanan').getValue()
	}
	
	params['jumlah']=dsDataGrdJab_viGzPermintaanBahanMakanan.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viGzPermintaanBahanMakanan.getCount();i++)
	{
		params['kd_bahan-'+i]=dsDataGrdJab_viGzPermintaanBahanMakanan.data.items[i].data.kd_bahan
		params['nama_bahan-'+i]=dsDataGrdJab_viGzPermintaanBahanMakanan.data.items[i].data.nama_bahan
		params['qty-'+i]=dsDataGrdJab_viGzPermintaanBahanMakanan.data.items[i].data.qty
		params['ket_spek-'+i]=dsDataGrdJab_viGzPermintaanBahanMakanan.data.items[i].data.ket_spek
	}
    return params
};


function getCriteriaCariGzPermintaanBahanMakanan()//^^^
{
	var strKriteria = "";

	if (Ext.get('TxtFilterGridNoPermintaanGzPermintaanBahanMakanan').getValue() != "" && Ext.get('TxtFilterGridNoPermintaanGzPermintaanBahanMakanan').getValue()!=='Nama/Kode obat')
	{
		strKriteria = " upper(no_minta) " + "LIKE upper('%" + Ext.get('TxtFilterGridNoPermintaanGzPermintaanBahanMakanan').getValue() +"%') ";
	}
	if (Ext.get('dfTglAwalMintaGzPermintaanBahanMakanan').getValue() != "" && Ext.get('dfTglAkhirMintaGzPermintaanBahanMakanan').getValue() != "")
	{
		if (strKriteria == "")
		{
			strKriteria = " tgl_minta between '" + Ext.get('dfTglAwalMintaGzPermintaanBahanMakanan').getValue() + "' and '" + Ext.get('dfTglAkhirMintaGzPermintaanBahanMakanan').getValue() + "'" ;
		}
		else {
			strKriteria += " and tgl_minta between '" + Ext.get('dfTglAwalMintaGzPermintaanBahanMakanan').getValue() + "' and '" + Ext.get('dfTglAkhirMintaGzPermintaanBahanMakanan').getValue() + "'" ;
		}
	}
		strKriteria= strKriteria + "order by no_minta asc limit 50"
	return strKriteria;
}


function ValidasiEntryGzPermintaanBahanMakanan(modul,mBolHapus)
{
	var x = 1;
	if(Ext.getCmp('cbJenisMintaGzPermintaanBahanMakanan').getValue() === ''){
		ShowPesanWarningGzPermintaanBahanMakanan('Jenis minta tidak boleh kosong', 'Warning');
		x = 0;
	}
	if(dsDataGrdJab_viGzPermintaanBahanMakanan.getCount() < 0){
		ShowPesanWarningGzPermintaanBahanMakanan('Daftar permintaan bahan tidak boleh kosong, minimal bahan 1', 'Warning');
		x = 0;
	}
	
	for(var i=0; i<dsDataGrdJab_viGzPermintaanBahanMakanan.getCount() ; i++){
		var o=dsDataGrdJab_viGzPermintaanBahanMakanan.getRange()[i].data;
		if(o.qty == undefined || o.qty <= 0){
			ShowPesanWarningGzPermintaanBahanMakanan('Qty tidak boleh 0, minimal qty 1', 'Warning');
			x = 0;
		}
		for(var j=i+1; j<dsDataGrdJab_viGzPermintaanBahanMakanan.getCount() ; j++){
			var p=dsDataGrdJab_viGzPermintaanBahanMakanan.getRange()[j].data;
			if(o.kd_bahan == p.kd_bahan){
				ShowPesanWarningGzPermintaanBahanMakanan('Bahan tidak boleh sama, periksa kembali daftar permintaan bahan', 'Warning');
				x = 0;
				break;
			}
		}
	}
	
	return x;
};



function ShowPesanWarningGzPermintaanBahanMakanan(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorGzPermintaanBahanMakanan(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};


function ShowPesanInfoGzPermintaanBahanMakanan(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};