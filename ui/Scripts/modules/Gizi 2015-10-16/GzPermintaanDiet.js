
var selectCount_viGzPermintaanDiet=50;
var NamaForm_viGzPermintaanDiet="Permintaan Diet Pasien";
var selectCountStatusPostingGzPermintaanDiet='Semua';//
var mod_name_viGzPermintaanDiet="viGzPermintaanDiet";
var now_viGzPermintaanDiet= new Date();
var rowSelected_viGzPermintaanDiet;
var rowSelectedGridPasien_viGzPermintaanDiet;
var setLookUps_viGzPermintaanDiet;
var selectSetUnit;
var cellSelecteddeskripsiRWI;
var tanggal = now_viGzPermintaanDiet.format("d/M/Y");
var jam = now_viGzPermintaanDiet.format("H/i/s");
var tmpkriteria;
var gridDTLDiet_GzPermintaanDiet;
var GridDataView_viGzPermintaanDiet;

var CurrentGzPermintaanDiet =
{
    data: Object,
    details: Array,
    row: 0
};

var CurrentData_viGzPermintaanDiet =
{
	data: Object,
	details: Array,
	row: 0
};

var CurrentDataPasien_viGzPermintaanDiet =
{
	data: Object,
	details: Array,
	row: 0
};


CurrentPage.page = dataGrid_viGzPermintaanDiet(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

var GzPermintaanDiet={};
GzPermintaanDiet.form={};
GzPermintaanDiet.func={};
GzPermintaanDiet.vars={};
GzPermintaanDiet.func.parent=GzPermintaanDiet;
GzPermintaanDiet.form.ArrayStore={};
GzPermintaanDiet.form.ComboBox={};
GzPermintaanDiet.form.DataStore={};
GzPermintaanDiet.form.Record={};
GzPermintaanDiet.form.Form={};
GzPermintaanDiet.form.Grid={};
GzPermintaanDiet.form.Panel={};
GzPermintaanDiet.form.TextField={};
GzPermintaanDiet.form.Button={};

GzPermintaanDiet.form.ArrayStore.pasien=new Ext.data.ArrayStore({
		id: 0,
		fields: [	
					'kd_pasien','nama','tgl_masuk','jam_masuk','no_kamar','nama_kamar'
				],
		data: []
	});
	
GzPermintaanDiet.form.ArrayStore.waktu	= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_waktu','waktu'],
	data: []
});

GzPermintaanDiet.form.ArrayStore.jenisdiet	= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_jenis','jenis_diet','harga_pokok'],
	data: []
});

function dataGrid_viGzPermintaanDiet(mod_id_viGzPermintaanDiet){	
    var FieldMaster_viGzPermintaanDiet = 
	[
		'no_minta', 'kd_unit', 'nama_unit', 'tgl_minta', 'tgl_makan', 'kd_ahli_gizi', 'nama_ahli_gizi'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viGzPermintaanDiet = new WebApp.DataStore
	({
        fields: FieldMaster_viGzPermintaanDiet
    });
    dataGriAwal();
    // Grid Apotek Perencanaan # --------------
	GridDataView_viGzPermintaanDiet = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viGzPermintaanDiet,
			autoScroll: true,
			columnLines: true,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viGzPermintaanDiet = undefined;
							rowSelected_viGzPermintaanDiet = dataSource_viGzPermintaanDiet.getAt(row);
							CurrentData_viGzPermintaanDiet
							CurrentData_viGzPermintaanDiet.row = row;
							CurrentData_viGzPermintaanDiet.data = rowSelected_viGzPermintaanDiet.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viGzPermintaanDiet = dataSource_viGzPermintaanDiet.getAt(ridx);
					if (rowSelected_viGzPermintaanDiet != undefined)
					{
						setLookUp_viGzPermintaanDiet(rowSelected_viGzPermintaanDiet.data);
					}
					else
					{
						setLookUp_viGzPermintaanDiet();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Apotek perencanaan
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header: 'No. Permintaan',
						dataIndex: 'no_minta',
						sortable: true,
						width: 35
						
					},
					//-------------- ## --------------
					{
						header: 'Ruangan',
						dataIndex: 'nama_unit',
						sortable: true,
						width: 35
					},
					//-------------- ## --------------
					{
						header:'Tgl Minta',
						dataIndex: 'tgl_minta',						
						width: 30,
						sortable: true,
						hideable:false,
                        menuDisabled:true,
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.tgl_minta);
						}
					},
					//-------------- ## --------------
					{
						header: 'Untuk Tanggal',
						dataIndex: 'tgl_makan',
						sortable: true,
						hideable:false,
                        menuDisabled:true,
						width: 30,
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.tgl_makan);
						}
					},
					//-------------- ## --------------
					{
						header: 'Ahli Gizi',
						dataIndex: 'nama_ahli_gizi',
						sortable: true,
						width: 50
					},
					//-------------- ## --------------
					{
						header: 'kd_ahli_gizi',
						dataIndex: 'kd_ahli_gizi',
						sortable: true,
						width: 40,
						hidden:true
					},
					//-------------- ## --------------
					{
						header: 'kd_unit',
						dataIndex: 'kd_unit',
						sortable: true,
						width: 40,
						hidden:true
					}
					//-------------- ## --------------
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viGzPermintaanDiet',
				items: 
				[
					{
						xtype: 'button',
						text: 'Tambah Penerimaan ',
						iconCls: 'Edit_Tr',
						tooltip: 'Add Data',
						id: 'btnTambah_viGzPermintaanDiet',
						handler: function(sm, row, rec)
						{
							setLookUp_viGzPermintaanDiet();								
						}
					},
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viGzPermintaanDiet',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viGzPermintaanDiet != undefined)
							{
								setLookUp_viGzPermintaanDiet(rowSelected_viGzPermintaanDiet.data)
							} else{
								ShowPesanWarningGzPermintaanDiet('Pilih data yang akan di edit','Warning');
							}
							
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viGzPermintaanDiet, selectCount_viGzPermintaanDiet, dataSource_viGzPermintaanDiet),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )
	
	var pencarianGzPermintaanDiet = new Ext.FormPanel({
        labelAlign: 'top',
        frame:true,
        title: '',
        bodyStyle:'padding:5px 5px 0',
        //width: 600,
        items: [
		{
			layout: 'column',
			border: false,
			items:
			[
				{
					columnWidth:.98,
					layout: 'absolute',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					width: 500,
					height: 70,
					anchor: '100% 100%',
					items:
					[
						{
							x: 10,
							y: 0,
							xtype: 'label',
							text: 'No. Permintaan'
						},
						{
							x: 120,
							y: 0,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 0,
							xtype: 'textfield',
							id: 'TxtNoPermintaanFilterGridDataView_viGzPermintaanDiet',
							name: 'TxtNoPermintaanFilterGridDataView_viGzPermintaanDiet',
							emptyText: '',
							width: 130,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariGzPermintaanDiet();
										refreshPermintaanDiet(tmpkriteria);
									} 						
								}
							}
						},
						{
							x: 10,
							y: 30,
							xtype: 'label',
							text: 'Ahli Gizi'
						},
						{
							x: 120,
							y: 30,
							xtype: 'label',
							text: ':'
						},
						ComboAhliGiziGzPermintaanDiet(),
						//-------------- ## --------------
						{
							x: 310,
							y: 0,
							xtype: 'label',
							text: 'Ruangan'
						},
						{
							x: 400,
							y: 0,
							xtype: 'label',
							text: ':'
						},
						ComboUnitGzPermintaanDiet(),	
						{
							x: 310,
							y: 30,
							xtype: 'label',
							text: 'Tanggal Minta'
						},
						{
							x: 400,
							y: 30,
							xtype: 'label',
							text: ':'
						},
						{
							x: 410,
							y: 30,
							xtype: 'datefield',
							id: 'dfTglAwalGzPermintaanDiet',
							format: 'd/M/Y',
							width: 120,
							tabIndex:3,
							value:now_viGzPermintaanDiet,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariGzPermintaanDiet();
										refreshPermintaanDiet(tmpkriteria);
									} 						
								}
							}
						},
						{
							x: 540,
							y: 30,
							xtype: 'label',
							text: 's/d'
						},
						{
							x: 568,
							y: 30,
							xtype: 'datefield',
							id: 'dfTglAkhirGzPermintaanDiet',
							format: 'd/M/Y',
							width: 120,
							tabIndex:4,
							value:now_viGzPermintaanDiet,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariGzPermintaanDiet();
										refreshPermintaanDiet(tmpkriteria);
									} 						
								}
							}
						},
						//----------------------------------------
						{
							x: 678,
							y: 30,
							xtype: 'button',
							text: 'Cari',
							iconCls: 'refresh',
							tooltip: 'Cari',
							style:{paddingLeft:'30px'},
							width:150,
							id: 'BtnFilterGridDataView_viGzPermintaanDiet',
							handler: function() 
							{				
								tmpkriteria = getCriteriaCariGzPermintaanDiet();
								refreshPermintaanDiet(tmpkriteria);
							}                        
						}
					]
				}
			]
		}
		]	
	})

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viGzPermintaanDiet = new Ext.Panel
    (
		{
			title: NamaForm_viGzPermintaanDiet,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viGzPermintaanDiet,
			region: 'center',
			layout: 'form', 
			closable: true,        
			border: false,  
			margins: '0 5 5 0',
			items: [ pencarianGzPermintaanDiet,
					GridDataView_viGzPermintaanDiet],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viGzPermintaanDiet,
		            columns: 21,
		            defaults: {
					scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viGzPermintaanDiet;
    //-------------- # End form filter # --------------
}

function refreshPermintaanDiet(kriteria)
{
    dataSource_viGzPermintaanDiet.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'ViewPermintaanDiet',
                    param : kriteria
                }			
            }
        );   
    return dataSource_viGzPermintaanDiet;
}

function setLookUp_viGzPermintaanDiet(rowdata){
    var lebar = 985;
    setLookUps_viGzPermintaanDiet = new Ext.Window({
        id: Nci.getId(),
        title: NamaForm_viGzPermintaanDiet, 
        closeAction: 'destroy',        
        width: 900,
        height: 580,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viGzPermintaanDiet(lebar,rowdata),
        listeners:{
            activate: function(){
				
            },
            afterShow: function(){
                this.activate();
            },
            deactivate: function(){
                rowSelected_viGzPermintaanDiet=undefined;
            }
        }
    });

    setLookUps_viGzPermintaanDiet.show();
    if (rowdata == undefined){	
    }
    else
    {
        datainit_viGzPermintaanDiet(rowdata);
    }
}

function getFormItemEntry_viGzPermintaanDiet(lebar,rowdata){
    var pnlFormDataBasic_viGzPermintaanDiet = new Ext.FormPanel({
		title: '',
		region: 'north',
		layout: 'form',
		bodyStyle: 'padding:10px 10px 10px 10px',
		anchor: '100%',
		labelWidth: 1,
		autoWidth: true,
		width: lebar,
		border: false,
		items:[
				getItemPanelInputPermintaan_viGzPermintaanDiet(lebar),
				getItemGridPasien_viGzPermintaanDiet(lebar),
				getItemGridDetail_viGzPermintaanDiet(lebar,rowdata)
			],
			fileUpload: true,
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAdd_viGzPermintaanDiet',
						handler: function(){
							dataAddNew_viGzPermintaanDiet();
						}
					},{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						id:'btnDeletePermintaan_viGzPermintaanDiet',
						iconCls: 'remove',
						disabled:true,
						handler:function()
						{
							Ext.Msg.confirm('Warning', 'Apakah data permintaan ini akan di hapus?', function(button){
								if (button == 'yes'){
									loadMask.show();
									Ext.Ajax.request
									(
										{
											url: baseURL + "index.php/gizi/functionPermintaanDiet/deletePermintaan",
											params: {no_minta:Ext.getCmp('txtNoPermintaanGzPermintaanDietL').getValue()},
											failure: function(o)
											{
												loadMask.hide();
												ShowPesanErrorGzPermintaanDiet('Error, delete permintaan! Hubungi Admin', 'Error');
											},	
											success: function(o) 
											{
												var cst = Ext.decode(o.responseText);
												if (cst.success === true) 
												{
													loadMask.hide();
													getDataGridAwal();
													ShowPesanInfoGzPermintaanDiet('Penghapusan berhasil','Information');
													setLookUps_viGzPermintaanDiet.close();
												}
												else 
												{
													if(cst.order =='true'){
														loadMask.hide();
														ShowPesanWarningGzPermintaanDiet('Gagal menghapus data ini. Permintaan diet ini sudah diorder', 'Warning');
													} else{
														loadMask.hide();
														ShowPesanWarningGzPermintaanDiet('Gagal menghapus data ini', 'Warning');
													}
													
												};
											}
										}
										
									)
								}
							});
						}
						  
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype:'splitbutton',
						text:'Print',
						iconCls:'print',
						id:'btnPrint_viGzPermintaanDiet',
						disabled:true,
						handler:function()
						{							
								
						},
						menu: new Ext.menu.Menu({
						items: [
							// these items will render as dropdown menu items when the arrow is clicked:
							{
								xtype: 'button',
								text: 'Print Bill',
								id: 'btnPrintBillGzPermintaanDiet',
								handler: function()
								{
									printbill();
								}
							},
						]
						})
					},
					{
						xtype:'tbseparator'
					}
					
				]
			}//,items:
		}
    )

    return pnlFormDataBasic_viGzPermintaanDiet;
}

function getItemPanelInputPermintaan_viGzPermintaanDiet(lebar) {
    var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 115,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'No. Penerimaan'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 0,
								xtype: 'textfield',
								id: 'txtNoPermintaanGzPermintaanDietL',
								name: 'txtNoPermintaanGzPermintaanDietL',
								width: 130,
								readOnly:true,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									}
								}
							},
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'Tanggal Minta'
							},
							{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 30,
								xtype: 'datefield',
								id: 'dfTglMintaGzPermintaanDietL',
								format: 'd/M/Y',
								width: 150,
								tabIndex:2,
								readOnly:true,
								value:now_viGzPermintaanDiet,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									}
								}
							},
							{
								x: 10,
								y: 60,
								xtype: 'label',
								text: 'Dari Ruangan'
							},
							{
								x: 120,
								y: 60,
								xtype: 'label',
								text: ':'
							},
							ComboUnitGzPermintaanDietLookup(),
							{
								x: 10,
								y: 90,
								xtype: 'label',
								text: 'Untuk Tanggal'
							},
							{
								x: 120,
								y: 90,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 90,
								xtype: 'datefield',
								id: 'dfTglUntukGzPermintaanDietL',
								format: 'd/M/Y',
								width: 150,
								tabIndex:4,
								value:now_viGzPermintaanDiet
							},
							//-------------- ## --------------
							{
								x: 320,
								y: 0,
								xtype: 'label',
								text: 'Ahli Gizi'
							},
							{
								x: 410,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							ComboAhliGiziGzPermintaanDietLookup(),
							{
								x: 320,
								y: 30,
								xtype: 'label',
								text: 'Keterangan'
							},
							{
								x: 410,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							{
								x: 420,
								y: 30,	
								xtype: 'textarea',
								name: 'txtKetGZPenerimaanL',
								id: 'txtKetGZPenerimaanL',
								width : 280,
								height : 80,
								tabIndex:6
							},
							//-----------------HIDDEN-----------------------
							{
								x: 130,
								y: 0,
								xtype: 'textfield',
								id: 'txtTmpNoPermintaanGzPermintaanDietL',
								name: 'txtTmpNoPermintaanGzPermintaanDietL',
								width: 130,
								readOnly:true,
								hidden:true
							},
							{
								x: 0,
								y: 0,
								xtype: 'textfield',
								id: 'txtTmpKdPasienGzPermintaanDietL',
								name: 'txtTmpKdPasienGzPermintaanDietL',
								width: 130,
								readOnly:true,
								hidden:true
							},
							
						]
					}
				]
			}
		]		
	};
    return items;
};

function getItemGridPasien_viGzPermintaanDiet(lebar){
    var items ={
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'margin-top: -1px;',
		border:false,
		width: lebar-80,
		height: 235,//300, 
	    tbar:
		[
			{
				text	: 'Add Pasien',
				id		: 'btnAddPasienGzPermintaanDietL',
				tooltip	: nmLookup,
				disabled: true,
				iconCls	: 'find',
				handler	: function(){
					var records = new Array();
					
					if(Ext.getCmp('txtTmpNoPermintaanGzPermintaanDietL').getValue() == ''){
						//jika permintaan baru
						loadMask.show();
						records.push(new dsDataGrdPasien_viGzPermintaanDiet.recordType());
						dsDataGrdPasien_viGzPermintaanDiet.add(records);
						saveMinta_viGzPermintaanDiet();
						
						dsTRDetailDietGzPermintaanDiet.removeAll();
						GzPermintaanDiet.vars.status_order = 'false';
						
						Ext.getCmp('txtNoPermintaanGzPermintaanDietL').disable();
						Ext.getCmp('dfTglMintaGzPermintaanDietL').disable();
						Ext.getCmp('cbo_UnitGzPermintaanDietLookup').disable();
						Ext.getCmp('dfTglUntukGzPermintaanDietL').disable();
						Ext.getCmp('cbo_AhliGiziGzPermintaanDietLookup').disable();
						Ext.getCmp('txtKetGZPenerimaanL').disable();
					} else{
						records.push(new dsDataGrdPasien_viGzPermintaanDiet.recordType());
						dsDataGrdPasien_viGzPermintaanDiet.add(records);
						
						dsTRDetailDietGzPermintaanDiet.removeAll();
						GzPermintaanDiet.vars.status_order = 'false';
					}
					
					
				}
			},{
				xtype: 'tbseparator'
			},{
				xtype: 'button',
				text: 'Delete',
				iconCls: 'remove',
				disabled:true,
				id: 'btnDelete_viGzPermintaanDiet',
				handler: function()
				{
					var line = GzPermintaanDiet.form.Grid.a.getSelectionModel().selection.cell[0];
					var o = dsDataGrdPasien_viGzPermintaanDiet.getRange()[line].data;
					if(dsDataGrdPasien_viGzPermintaanDiet.getCount()>1){
						Ext.Msg.confirm('Warning', 'Apakah pasien ini akan dihapus?', function(button){
							if (button == 'yes'){
								if(dsDataGrdPasien_viGzPermintaanDiet.getRange()[line].data.kd_pasien != undefined){
									Ext.Ajax.request
									(
										{
											url: baseURL + "index.php/gizi/functionPermintaanDiet/hapusBarisPasien",
											params:{kd_pasien:o.kd_pasien,no_minta:Ext.getCmp('txtTmpNoPermintaanGzPermintaanDietL').getValue()} ,
											failure: function(o)
											{
												ShowPesanErrorGzPermintaanDiet('Error, delete pasien! Hubungi Admin', 'Error');
											},	
											success: function(o) 
											{
												var cst = Ext.decode(o.responseText);
												if (cst.success === true) 
												{
													dsDataGrdPasien_viGzPermintaanDiet.removeAt(line);
													GzPermintaanDiet.form.Grid.a.getView().refresh();
													Ext.getCmp('btnAddDetDietGzPermintaanDietL').enable();
													ShowPesanInfoGzPermintaanDiet('Berhasil menghapus data pasien ini', 'Information');
													
												}
												else 
												{
													ShowPesanErrorGzPermintaanDiet('Gagal menghapus data pasien ini', 'Error');
												};
											}
										}
										
									)
								}else{
									dsDataGrdPasien_viGzPermintaanDiet.removeAt(line);
									GzPermintaanDiet.form.Grid.a.getView().refresh();
									
								}
							} 
							
						});
					} else{
						ShowPesanErrorGzPermintaanDiet('Data tidak bisa dihapus karena minimal pasien 1','Error');
					}
					
				}
			}	
		],
		items:[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:[
					gridDataViewEdit_viGzPermintaanDiet()
				]	
			}
		]
	};
    return items;
};


//var a={};
function gridDataViewEdit_viGzPermintaanDiet(){
    var FieldGrdKasir_viGzPermintaanDiet = [];
    dsDataGrdPasien_viGzPermintaanDiet= new WebApp.DataStore({
        fields: FieldGrdKasir_viGzPermintaanDiet
    });
    
    GzPermintaanDiet.form.Grid.a =new Ext.grid.EditorGridPanel({
        store: dsDataGrdPasien_viGzPermintaanDiet,
        height: 210,
        columnLines: true,
		selModel: new Ext.grid.CellSelectionModel ({
            singleSelect: true,
            listeners:{
                cellselect: function(sm, row, rec){
					rowSelectedGridPasien_viGzPermintaanDiet = undefined;
					rowSelectedGridPasien_viGzPermintaanDiet = dsDataGrdPasien_viGzPermintaanDiet.getAt(row);
					CurrentDataPasien_viGzPermintaanDiet
					CurrentDataPasien_viGzPermintaanDiet.row = row;
					CurrentDataPasien_viGzPermintaanDiet.data = rowSelectedGridPasien_viGzPermintaanDiet.data;
					
					dsTRDetailDietGzPermintaanDiet.removeAll();
					getGridWaktu(Ext.getCmp('txtTmpNoPermintaanGzPermintaanDietL').getValue(),CurrentDataPasien_viGzPermintaanDiet.data.kd_pasien);
					
					if(GzPermintaanDiet.vars.status_order == 'false'){
						Ext.getCmp('btnAddDetDietGzPermintaanDietL').enable();
						Ext.getCmp('btnDeleteDet_viGzPermintaanDiet').enable();
					} else{
						Ext.getCmp('btnAddPasienGzPermintaanDietL').disable();
						Ext.getCmp('btnAddDetDietGzPermintaanDietL').disable();
					}
					Ext.getCmp('txtTmpKdPasienGzPermintaanDietL').setValue(CurrentDataPasien_viGzPermintaanDiet.data.kd_pasien);
					
                },
            }
        }),
        stripeRows: true,
        cm:new Ext.grid.ColumnModel([
        	new Ext.grid.RowNumberer(),	
			{
				dataIndex: 'kd_pasien',
				header: 'Kode Pasien',
				sortable: true,
				width: 70
			},
			{			
				dataIndex: 'nama',
				header: 'Nama Pasien',
				sortable: true,
				width: 250,
				editor:new Nci.form.Combobox.autoComplete({
					store	: GzPermintaanDiet.form.ArrayStore.pasien,
					select	: function(a,b,c){
						var line	= GzPermintaanDiet.form.Grid.a.getSelectionModel().selection.cell[0];
						dsDataGrdPasien_viGzPermintaanDiet.getRange()[line].data.kd_pasien=b.data.kd_pasien;
						dsDataGrdPasien_viGzPermintaanDiet.getRange()[line].data.tgl_masuk=b.data.tgl_masuk;
						dsDataGrdPasien_viGzPermintaanDiet.getRange()[line].data.jam_masuk=b.data.jam_masuk;
						dsDataGrdPasien_viGzPermintaanDiet.getRange()[line].data.no_kamar=b.data.no_kamar;
						dsDataGrdPasien_viGzPermintaanDiet.getRange()[line].data.nama_kamar=b.data.nama_kamar;
						dsDataGrdPasien_viGzPermintaanDiet.getRange()[line].data.nama=b.data.nama;
						
						GzPermintaanDiet.vars.kd_pasien=b.data.kd_pasien;
						GzPermintaanDiet.vars.tgl_masuk=b.data.tgl_masuk;
						GzPermintaanDiet.vars.no_kamar=b.data.no_kamar;
						GzPermintaanDiet.vars.kd_unit=b.data.kd_unit;
						
						loadMask.show();
						
						savePasien_viGzPermintaanDiet();
						
						GzPermintaanDiet.form.Grid.a.getView().refresh();
					},
					insert	: function(o){
						return {
							kd_pasien       : o.kd_pasien,
							tgl_masuk 		: o.tgl_masuk,
							jam_masuk		: o.jam_masuk,
							no_kamar		: o.no_kamar,
							nama_kamar		: o.nama_kamar,
							nama			: o.nama,
							text			:  '<table style="font-size: 11px;"><tr><td width="50">'+o.kd_pasien+'</td><td width="200">'+o.nama+'</td></tr></table>',
							kd_unit			: o.kd_unit
						}
					},
					param:function(){
							return {
								kd_unitLama:GzPermintaanDiet.vars.kd_unit,
								kd_unitNew:Ext.getCmp('cbo_UnitGzPermintaanDietLookup').getValue()
							}
					},
					url		: baseURL + "index.php/gizi/functionPermintaanDiet/getPasienRWI",
					valueField: 'nama',
					displayField: 'text',
					listWidth: 380
				})
			},
			{
				dataIndex: 'tgl_masuk',
				header: 'Tanggal Masuk',
				width: 70,
				align:'center'
			},{
				dataIndex: 'jam_masuk',
				header: 'Jam Masuk',
				sortable: true,
				align:'center',
				width: 85
			},{
				dataIndex: 'no_kamar',
				header: 'No Kamar',
				sortable: true,
				width: 45,
				align:'center'
			},
			{
				dataIndex: 'nama_kamar',
				header: 'Kamar',
				sortable: true,
				width: 65,
				align:'center'
			},
			{
				dataIndex: 'no_minta',
				header: 'no_minta',
				sortable: true,
				width: 65,
				align:'center',
				hidden:true
			}
        ]),
		viewConfig:{
			forceFit: true
		}
    });
    return GzPermintaanDiet.form.Grid.a;
}

function getItemGridDetail_viGzPermintaanDiet(lebar,rowdata) 
{
    var items =
	{
		title: 'Detail Diet', 
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
		bodyStyle: 'margin-top: -1px;',
		border:true,
		width: lebar-80,
		height: 150,//300, 
	    items:
		[
		
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
				items:
				[
					gridDataViewDetaiDiet_viGzPermintaanDiet()
				]	
			}
			//-------------- ## --------------
		],
		tbar:
		[
			{
				text	: 'Add Diet',
				id		: 'btnAddDetDietGzPermintaanDietL',
				tooltip	: nmLookup,
				iconCls	: 'find',
				disabled:true,
				handler	: function(){
					var records = new Array();
					records.push(new dsTRDetailDietGzPermintaanDiet.recordType());
					dsTRDetailDietGzPermintaanDiet.add(records);
				}
			},{
				xtype: 'tbseparator'
			},{
				xtype: 'button',
				text: 'Delete',
				iconCls: 'remove',
				disabled:true,
				id: 'btnDeleteDet_viGzPermintaanDiet',
				handler: function()
				{
					var line = gridDTLDiet_GzPermintaanDiet.getSelectionModel().selection.cell[0];
					var o = dsTRDetailDietGzPermintaanDiet.getRange()[line].data;
					if(dsTRDetailDietGzPermintaanDiet.getCount()>1){
						Ext.Msg.confirm('Warning', 'Apakah jenis diet ini akan dihapus?', function(button){
							if (button == 'yes'){
								if(dsTRDetailDietGzPermintaanDiet.getRange()[line].data.kd_jenis != undefined){
									Ext.Ajax.request
									(
										{
											url: baseURL + "index.php/gizi/functionPermintaanDiet/hapusBarisDetailDiet",
											params: {		
												kd_jenis:o.kd_jenis, 
												kd_waktu:o.kd_waktu, 
												no_minta:Ext.getCmp('txtTmpNoPermintaanGzPermintaanDietL').getValue(),
												kd_pasien:Ext.getCmp('txtTmpKdPasienGzPermintaanDietL').getValue()
											},
											failure: function(o) {
												ShowPesanErrorGzPermintaanDiet('Error, delete detail diet! Hubungi Admin', 'Error');
											},	
											success: function(o) {
												var cst = Ext.decode(o.responseText);
												if (cst.success === true) 
												{
													ShowPesanInfoGzPermintaanDiet('Berhasil menghapus jenis diet ini', 'Information');
													dsTRDetailDietGzPermintaanDiet.removeAt(line);
													gridDTLDiet_GzPermintaanDiet.getView().refresh();
												}
												else 
												{
													ShowPesanErrorGzPermintaanDiet('Gagal menghapus data jenis diet ini', 'Error');
												};
											}
										}
										
									)
								}else{
									dsTRDetailDietGzPermintaanDiet.removeAt(line);
									gridDTLDiet_GzPermintaanDiet.getView().refresh();
								}
							} 
							
						});
					} else{
						ShowPesanErrorGzPermintaanDiet('Data tidak bisa dihapus karena minimal jenis diet 1','Error');
					}
					
				}
			}	
		]
	};
    return items;
};

function gridDataViewDetaiDiet_viGzPermintaanDiet() 
{

    var fldDetail = [];
	
    dsTRDetailDietGzPermintaanDiet = new WebApp.DataStore({ fields: fldDetail })
		 
    gridDTLDiet_GzPermintaanDiet = new Ext.grid.EditorGridPanel
    (
        {
            store: dsTRDetailDietGzPermintaanDiet,
            border: false,
            columnLines: true,
            height: 95,
			autoScroll:true,
            selModel: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                            //cellSelecteddeskripsiRWI = dsTRDetailDietGzPermintaanDietList.getAt(row);
                            //CurrentGzPermintaanDiet.row = row;
                            //CurrentGzPermintaanDiet.data = cellSelecteddeskripsiRWI;
                        }
                    }
                }
            ),
            stripeRows: true,
            columns:
			[
				{
					header: 'kd waktu',
					dataIndex: 'kd_waktu',
					width:100,
					hidden:true
				},
				{			
					dataIndex: 'waktu',
					header: 'Waktu',
					sortable: false,
					width: 250,	
					editor:new Nci.form.Combobox.autoComplete({
						store	: GzPermintaanDiet.form.ArrayStore.waktu,
						select	: function(a,b,c){
							var line	= gridDTLDiet_GzPermintaanDiet.getSelectionModel().selection.cell[0];
							dsTRDetailDietGzPermintaanDiet.getRange()[line].data.kd_waktu=b.data.kd_waktu;
							dsTRDetailDietGzPermintaanDiet.getRange()[line].data.waktu=b.data.waktu;
							
							GzPermintaanDiet.vars.kd_waktu=b.data.kd_waktu;
							
							gridDTLDiet_GzPermintaanDiet.getView().refresh();
						},
						insert	: function(o){
							return {
								kd_waktu	: o.kd_waktu,
								waktu		: o.waktu,
								text		:  '<table style="font-size: 11px;"><tr><td width="100">'+o.waktu+'</td></tr></table>'
							}
						},
						url		: baseURL + "index.php/gizi/functionPermintaanDiet/getWaktu",
						valueField: 'waktu',
						displayField: 'text',
						listWidth: 150
					})
				},
				{			
					dataIndex: 'jenis_diet',
					header: 'Jenis diet',
					sortable: false,
					width: 250,	
					editor:new Nci.form.Combobox.autoComplete({
						store	: GzPermintaanDiet.form.ArrayStore.jenisdiet,
						select	: function(a,b,c){
							var line	= gridDTLDiet_GzPermintaanDiet.getSelectionModel().selection.cell[0];
							dsTRDetailDietGzPermintaanDiet.getRange()[line].data.kd_jenis=b.data.kd_jenis;
							dsTRDetailDietGzPermintaanDiet.getRange()[line].data.jenis_diet=b.data.jenis_diet;
							
							GzPermintaanDiet.vars.kd_jenis=b.data.kd_jenis;
							
							loadMask.show();
							
							saveDetailDiet_viGzPermintaanDiet();
							
							gridDTLDiet_GzPermintaanDiet.getView().refresh();
						},
						insert	: function(o){
							return {
								kd_jenis	: o.kd_jenis,
								jenis_diet	: o.jenis_diet,
								text		:  '<table style="font-size: 11px;"><tr><td width="100">'+o.jenis_diet+'</td></tr></table>'
							}
						},
						url		: baseURL + "index.php/gizi/functionPermintaanDiet/getJenisDiet",
						valueField: 'jenis_diet',
						displayField: 'text',
						listWidth: 150
					})
				},
				{
					header: 'kd jenis',
					dataIndex: 'kd_jenis',
					width:100,
					hidden:true
				},
				{
					header: 'kd pasien',
					dataIndex: 'kd_pasien',
					width:100,
					hidden:true
				}
            ],
			viewConfig: {forceFit: true}
			
	 
		}
    );
    return gridDTLDiet_GzPermintaanDiet;
};


function ComboAhliGiziGzPermintaanDietLookup()
{
    var Field_ahliGiziLookup = ['KD_AHLI_GIZI', 'NAMA_AHLI_GIZI'];
    ds_ahliGiziGZPermintaanLookup = new WebApp.DataStore({fields: Field_ahliGiziLookup});
    ds_ahliGiziGZPermintaanLookup.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'KD_AHLI_GIZI',
					Sortdir: 'ASC',
					target: 'ComboAhliGizi',
					param: ''
				}
		}
	);
	
    var cbo_AhliGiziGzPermintaanDietLookup = new Ext.form.ComboBox
    (
        {
			x: 420,
			y: 0,
            flex: 1,
			id: 'cbo_AhliGiziGzPermintaanDietLookup',
			valueField: 'KD_AHLI_GIZI',
            displayField: 'NAMA_AHLI_GIZI',
			store: ds_ahliGiziGZPermintaanLookup,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:150,
			tabIndex:2,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					Ext.getCmp('btnAddPasienGzPermintaanDietL').enable();
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_AhliGiziGzPermintaanDietLookup;
};

function ComboAhliGiziGzPermintaanDiet()
{
    var Field_ahliGizi = ['KD_AHLI_GIZI', 'NAMA_AHLI_GIZI'];
    ds_ahliGiziGZPermintaan = new WebApp.DataStore({fields: Field_ahliGizi});
    ds_ahliGiziGZPermintaan.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'KD_AHLI_GIZI',
					Sortdir: 'ASC',
					target: 'ComboAhliGizi',
					param: ''
				}
		}
	);
	
    var cbo_AhliGiziGzPermintaanDiet = new Ext.form.ComboBox
    (
        {
			x: 130,
			y: 30,
            flex: 1,
			id: 'cbo_AhliGiziGzPermintaanDiet',
			valueField: 'KD_AHLI_GIZI',
            displayField: 'NAMA_AHLI_GIZI',
			store: ds_ahliGiziGZPermintaan,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:150,
			tabIndex:2,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						tmpkriteria = getCriteriaCariGzPermintaanDiet();
						refreshPermintaanDiet(tmpkriteria);
					} 						
				}
			}
        }
    )    
    return cbo_AhliGiziGzPermintaanDiet;
};


function ComboUnitGzPermintaanDiet(){
    var Field_Vendor = ['KD_UNIT', 'NAMA_UNIT'];
    ds_unit = new WebApp.DataStore({fields: Field_Vendor});
    ds_unit.load({
        params:{
	        Skip: 0,
	        Take: 1000,
	        Sort: 'kd_unit',
	        Sortdir: 'ASC',
	        target: 'ComboUnitApotek',
	        param: "parent='100' ORDER BY nama_unit"
        }
    });
    var cbo_UnitGzPermintaanDiet = new Ext.form.ComboBox({
			x:410,
			y:0,
            flex: 1,
			id: 'cbo_UnitGzPermintaanDiet',
			valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
			emptyText:'Unit/Ruang',
			store: ds_unit,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:150,
			tabIndex:2,
			listeners:{ 
				'select': function(a,b,c){
					selectSetUnit=b.data.displayText ;
				},
				'specialkey' : function(){
					if (Ext.EventObject.getKey() === 13) {
						tmpkriteria = getCriteriaCariGzPermintaanDiet();
						refreshPermintaanDiet(tmpkriteria);
					} 						
				}
			}
        }
    )    
    return cbo_UnitGzPermintaanDiet;
}

function ComboUnitGzPermintaanDietLookup()
{
    var Field_Vendor = ['KD_UNIT', 'NAMA_UNIT'];
    ds_unitL = new WebApp.DataStore({fields: Field_Vendor});
    ds_unitL.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'kd_unit',
					Sortdir: 'ASC',
					target: 'ComboUnitApotek',
					param: "parent='100' ORDER BY nama_unit"
				}
		}
	);
	
    var cbo_UnitGzPermintaanDietLookup = new Ext.form.ComboBox
    (
        {
			x:130,
			y:60,
			id: 'cbo_UnitGzPermintaanDietLookup',
			store: ds_unitL,
			valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:150,
			tabIndex:3,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_UnitGzPermintaanDietLookup;
}

function datainit_viGzPermintaanDiet(rowdata)
{
	var tgl_minta2 = rowdata.tgl_minta.split(" ");
	var tgl_makan2 = rowdata.tgl_makan.split(" ");
	Ext.getCmp('txtTmpNoPermintaanGzPermintaanDietL').setValue(rowdata.no_minta);
	Ext.getCmp('txtNoPermintaanGzPermintaanDietL').setValue(rowdata.no_minta);
	Ext.getCmp('dfTglMintaGzPermintaanDietL').setValue(tgl_minta2[0]);
	Ext.getCmp('cbo_UnitGzPermintaanDietLookup').setValue(rowdata.nama_unit);
	Ext.getCmp('dfTglUntukGzPermintaanDietL').setValue(tgl_makan2[0]);
	Ext.getCmp('cbo_AhliGiziGzPermintaanDietLookup').setValue(rowdata.nama_ahli_gizi);
	Ext.getCmp('txtKetGZPenerimaanL').setValue(rowdata.keterangan);
	GzPermintaanDiet.vars.kd_unit=rowdata.kd_unit
	
	Ext.getCmp('txtNoPermintaanGzPermintaanDietL').disable();
	Ext.getCmp('dfTglMintaGzPermintaanDietL').disable();
	Ext.getCmp('cbo_UnitGzPermintaanDietLookup').disable();
	Ext.getCmp('dfTglUntukGzPermintaanDietL').disable();
	Ext.getCmp('cbo_AhliGiziGzPermintaanDietLookup').disable();
	Ext.getCmp('txtKetGZPenerimaanL').disable();	
	
	getGridPasien(rowdata.no_minta);
	cekOrder(rowdata.no_minta);
	
};

function dataGriAwal(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionPermintaanDiet/getDataGridAwal",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorGzPermintaanDiet('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dataSource_viGzPermintaanDiet.removeAll();
					var recs=[],
						recType=dataSource_viGzPermintaanDiet.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
						dataSource_viGzPermintaanDiet.add(recs);
					
					
					
					GridDataView_viGzPermintaanDiet.getView().refresh();
				}
				else 
				{
					ShowPesanErrorGzPermintaanDiet('Gagal membaca data obat', 'Error');
				};
			}
		}
		
	)
	
}


function getGridPasien(no_minta){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionPermintaanDiet/getGridPasien",
			params: {nominta:no_minta},
			failure: function(o)
			{
				ShowPesanErrorGzPermintaanDiet('Error, pasien! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dsDataGrdPasien_viGzPermintaanDiet.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
					dsDataGrdPasien_viGzPermintaanDiet.add(recs);
					
					GzPermintaanDiet.form.Grid.a.getView().refresh();
				}
				else 
				{
					ShowPesanErrorGzPermintaanDiet('Gagal membaca data pasien ini', 'Error');
				};
			}
		}
		
	)
	
}

function getGridWaktu(no_minta,kd_pasien){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionPermintaanDiet/getGridWaktu",
			params: {nominta:no_minta, kdpasien:kd_pasien},
			failure: function(o)
			{
				ShowPesanErrorGzPermintaanDiet('Error, detail diet! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				
				if (cst.success === true) 
				{
					var recs=[],
						recType=dsTRDetailDietGzPermintaanDiet.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
					dsTRDetailDietGzPermintaanDiet.add(recs);
					
					gridDTLDiet_GzPermintaanDiet.getView().refresh();
				}
				else 
				{
					ShowPesanErrorGzPermintaanDiet('Gagal membaca data detail diet pasien ini', 'Error');
				};
			}
		}
		
	)
	
}


function cekOrder(no_minta){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionPermintaanDiet/cekOrderPermintaanDiet",
			params: {nominta:no_minta},
			failure: function(o)
			{
				ShowPesanErrorGzPermintaanDiet('Error, cek order! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					Ext.getCmp('btnAddPasienGzPermintaanDietL').enable();
					Ext.getCmp('btnDelete_viGzPermintaanDiet').enable();
					Ext.getCmp('btnDeletePermintaan_viGzPermintaanDiet').enable();
					GzPermintaanDiet.vars.status_order='false';
				}
				else 
				{
					ShowPesanInfoGzPermintaanDiet('Permintaan diet ini sudah di order', 'Information');
					Ext.getCmp('btnDeletePermintaan_viGzPermintaanDiet').disable();
					GzPermintaanDiet.vars.status_order='true';
				};
			}
		}
		
	)
}

function saveMinta_viGzPermintaanDiet(){
	if (ValidasiEntryPermintaanGzPermintaanDiet(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/gizi/functionPermintaanDiet/saveMinta",
				params: getParamMintaGzPermintaanDiet(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorGzPermintaanDiet('Error, permintaan! Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						Ext.getCmp('txtTmpNoPermintaanGzPermintaanDietL').setValue(cst.nominta);
						Ext.getCmp('txtNoPermintaanGzPermintaanDietL').setValue(cst.nominta);
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorGzPermintaanDiet('Gagal menyimpan data permintaan', 'Error');
					};
				}
			}
		)
	}
}


function savePasien_viGzPermintaanDiet(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionPermintaanDiet/saveMintaPasien",
			params: getParamMintaPasienGzPermintaanDiet(),
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorGzPermintaanDiet('Error, pasien! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					loadMask.hide();
					Ext.getCmp('txtTmpKdPasienGzPermintaanDietL').setValue(cst.kdpasien);
					if(cst.error=='ada'){
						ShowPesanWarningGzPermintaanDiet('Pasien ini susah ada dalam transaksi pemintaan diet hari ini, hapus pasien ini untuk melanjutkan', 'Warning');
						Ext.getCmp('btnDelete_viGzPermintaanDiet').enable();
						Ext.getCmp('btnAddPasienGzPermintaanDietL').disable();
						Ext.getCmp('btnAddDetDietGzPermintaanDietL').disable();
					} else{
						if(GzPermintaanDiet.vars.status_order == 'false'){
							Ext.getCmp('btnAddPasienGzPermintaanDietL').disable();
							Ext.getCmp('btnDelete_viGzPermintaanDiet').disable();
							Ext.getCmp('btnAddDetDietGzPermintaanDietL').enable();
						} else{
							Ext.getCmp('btnAddPasienGzPermintaanDietL').disable();
							Ext.getCmp('btnDelete_viGzPermintaanDiet').disable();
							Ext.getCmp('btnAddDetDietGzPermintaanDietL').disable();
						}
					}
				}
				else 
				{
					loadMask.hide();
					ShowPesanErrorGzPermintaanDiet('Gagal menyimpan data pasien', 'Error');
				};
			}
		}
	)
}

function saveDetailDiet_viGzPermintaanDiet(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionPermintaanDiet/saveMintaPasienDetail",
			params: getParamDetailDietGzPermintaanDiet(),
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorGzPermintaanDiet('Error, detail diet! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					if(cst.ada=='0'){
						loadMask.hide();
						dataGriAwal()
						Ext.getCmp('btnAddPasienGzPermintaanDietL').enable();
						Ext.getCmp('btnDeleteDet_viGzPermintaanDiet').enable();
						Ext.getCmp('txtNoPermintaanGzPermintaanDietL').setValue(Ext.getCmp('txtTmpNoPermintaanGzPermintaanDietL').getValue());
					} else{
						loadMask.hide();
						ShowPesanWarningGzPermintaanDiet('Jenis diet ini sudah ada dengan waktu yang sama, hapus salah satu untuk melanjutkan', 'Error');
						Ext.getCmp('btnAddPasienGzPermintaanDietL').disable();
						Ext.getCmp('btnAddDetDietGzPermintaanDietL').disable();
						Ext.getCmp('btnDeleteDet_viGzPermintaanDiet').enable();
					}
				}
				else 
				{
					loadMask.hide();
					ShowPesanErrorGzPermintaanDiet('Gagal menyimpan data detail diet', 'Error');
				};
			}
		}
	)
}


function printbill()
{
    Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/main/CreateDataObj",
			params: datacetakbill(),
			failure: function(o)
			{	
				ShowPesanErrorGzPermintaanDiet('Error hubungi admin' ,'Error');
			},
			success: function(o)
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					
				}
				else if  (cst.success === false && cst.pesan===0)
				{
					ShowPesanWarningGzPermintaanDiet('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
				}
				else
				{
					ShowPesanErrorGzPermintaanDiet('Gagal print '  + 'Error');
				}
			}
		}
	)
}

function getParamMintaGzPermintaanDiet() 
{
    var params =
	{	
		NoMinta: Ext.getCmp('txtNoPermintaanGzPermintaanDietL').getValue(),
		KdUnit: Ext.getCmp('cbo_UnitGzPermintaanDietLookup').getValue(),
		TglMinta:Ext.getCmp('dfTglMintaGzPermintaanDietL').getValue(),
		TglMakan:Ext.getCmp('dfTglUntukGzPermintaanDietL').getValue(),
		AhliGizi:Ext.getCmp('cbo_AhliGiziGzPermintaanDietLookup').getValue(),
		Ket:Ext.getCmp('txtKetGZPenerimaanL').getValue()
	};
    return params
};

function getParamMintaPasienGzPermintaanDiet() 
{
    var params =
	{
		NoMinta:Ext.getCmp('txtTmpNoPermintaanGzPermintaanDietL').getValue(),
		KdUnit: Ext.getCmp('cbo_UnitGzPermintaanDietLookup').getValue(),
		KdPasien:GzPermintaanDiet.vars.kd_pasien,
		TglMasuk:GzPermintaanDiet.vars.tgl_masuk,
		NoKamar:GzPermintaanDiet.vars.no_kamar
	};
	
    return params
};

function getParamDetailDietGzPermintaanDiet() 
{
    var params =
	{
		NoMinta:Ext.getCmp('txtTmpNoPermintaanGzPermintaanDietL').getValue(),
		KdPasien: Ext.getCmp('txtTmpKdPasienGzPermintaanDietL').getValue(),
		kd_jenis:GzPermintaanDiet.vars.kd_jenis,
		kd_waktu:GzPermintaanDiet.vars.kd_waktu
	};
	
    return params
};

function datacetakbill(){
	var params =
	{
		Table: 'billprintingGzPermintaanDiet',
		NoResep:Ext.getCmp('txtNoResepGzPermintaanDietL').getValue(),
		NoOut:Ext.getCmp('txtTmpNooutGzPermintaanDietL').getValue(),
		TglOut:Ext.getCmp('txtTmpTgloutGzPermintaanDietL').getValue(),
		KdPasien:Ext.getCmp('txtKdPasienGzPermintaanDietL').getValue(),
		NamaPasien:GzPermintaanDiet.form.ComboBox.namaPasien.getValue(),
		JenisPasien:Ext.get('cboPilihankelompokPasienAptGzPermintaanDiet').getValue(),
		Kelas:Ext.get('cbo_UnitGzPermintaanDietL').getValue(),
		Dokter:Ext.get('cbo_DokterGzPermintaanDiet').getValue(),
		Total:Ext.get('txtTotalBayarGzPermintaanDietL').getValue(),
		Tot:toInteger(Ext.get('txtTotalBayarGzPermintaanDietL').getValue())
		
	}
	params['jumlah']=dsDataGrdPasien_viGzPermintaanDiet.getCount();
	for(var i = 0 ; i < dsDataGrdPasien_viGzPermintaanDiet.getCount();i++)
	{
		params['nama_obat-'+i]=dsDataGrdPasien_viGzPermintaanDiet.data.items[i].data.nama_obat;
		params['jml-'+i]=dsDataGrdPasien_viGzPermintaanDiet.data.items[i].data.jml;
	}
	
    return params
}

function getCriteriaCariGzPermintaanDiet()//^^^
{
      	 var strKriteria = "";

			if (Ext.getCmp('TxtNoPermintaanFilterGridDataView_viGzPermintaanDiet').getValue() != "")
            {
                strKriteria = " m.no_minta " + "LIKE upper('" + Ext.getCmp('TxtNoPermintaanFilterGridDataView_viGzPermintaanDiet').getValue() +"%')";
            }
            
            if (Ext.get('cbo_AhliGiziGzPermintaanDiet').getValue() != "")//^^^
            {
				if (strKriteria == "")
				{
					strKriteria = " lower(a.nama_ahli_gizi) " + "LIKE lower('" + Ext.get('cbo_AhliGiziGzPermintaanDiet').getValue() +"%')" ;
				}
				else {
					strKriteria += " AND lower(a.nama_ahli_gizi) " + "LIKE lower('" + Ext.get('cbo_AhliGiziGzPermintaanDiet').getValue() +"%')')";
				}
            }
			
			if (Ext.getCmp('cbo_UnitGzPermintaanDiet').getValue() != "" && Ext.get('cbo_UnitGzPermintaanDiet').getValue() != 'Unit/Ruang')
            {
				if (strKriteria == "")
				{
					strKriteria = " m.kd_unit ='" + Ext.getCmp('cbo_UnitGzPermintaanDiet').getValue() + "'"  ;
				}
				else {
					strKriteria += " and m.kd_unit ='" + Ext.getCmp('cbo_UnitGzPermintaanDiet').getValue() + "'";
			    }
                
            }
	
			
			if (Ext.getCmp('dfTglAwalGzPermintaanDiet').getValue() != "" && Ext.getCmp('dfTglAkhirGzPermintaanDiet').getValue() != "")
            {
                if (strKriteria == "")
				{
					strKriteria = " m.tgl_minta between '" + Ext.get('dfTglAwalGzPermintaanDiet').getValue() + "' and '" + Ext.get('dfTglAkhirGzPermintaanDiet').getValue() + "'" ;
				}
				else {
					strKriteria += " AND m.tgl_minta between '" + Ext.get('dfTglAwalGzPermintaanDiet').getValue() + "' and '" + Ext.get('dfTglAkhirGzPermintaanDiet').getValue() + "'" ;
				}
                
            }
			
		strKriteria= strKriteria + " ORDER BY m.no_minta";
	 return strKriteria;
}

function ValidasiEntryPermintaanGzPermintaanDiet(modul,mBolHapus)
{
	var x = 1;
	if(Ext.getCmp('cbo_UnitGzPermintaanDietLookup').getValue() === '' || Ext.getCmp('cbo_AhliGiziGzPermintaanDietLookup').getValue() === '' ||(Ext.getCmp('dfTglUntukGzPermintaanDietL').getValue() < Ext.getCmp('dfTglMintaGzPermintaanDietL').getValue()) ){
		if(Ext.getCmp('cbo_UnitGzPermintaanDietLookup').getValue() === ''){
			ShowPesanWarningGzPermintaanDiet('Unit tidak boleh kosong', 'Warning');
			x = 0;
		} else if(Ext.getCmp('cbo_AhliGiziGzPermintaanDietLookup').getValue() === ''){
			ShowPesanWarningGzPermintaanDiet('Ahli gizi tidak boleh kosong', 'Warning');
			x = 0;
		} else {
			ShowPesanWarningGzPermintaanDiet('Untuk tanggal tidak boleh kurang dari tanggal permintaan', 'Warning');
			x = 0;
		} 
	}
	return x;
};

function dataAddNew_viGzPermintaanDiet(){
	Ext.getCmp('txtTmpNoPermintaanGzPermintaanDietL').setValue('');
	Ext.getCmp('txtNoPermintaanGzPermintaanDietL').setValue('');
	Ext.getCmp('dfTglMintaGzPermintaanDietL').setValue(now_viGzPermintaanDiet);
	Ext.getCmp('cbo_UnitGzPermintaanDietLookup').setValue('');
	Ext.getCmp('dfTglUntukGzPermintaanDietL').setValue(now_viGzPermintaanDiet);
	Ext.getCmp('cbo_AhliGiziGzPermintaanDietLookup').setValue('');
	Ext.getCmp('txtKetGZPenerimaanL').setValue('');
	Ext.getCmp('txtTmpKdPasienGzPermintaanDietL').setValue('');
	GzPermintaanDiet.vars.kd_unit='';
	dsDataGrdPasien_viGzPermintaanDiet.removeAll();
	dsTRDetailDietGzPermintaanDiet.removeAll();
	
	
	Ext.getCmp('txtNoPermintaanGzPermintaanDietL').enable();
	Ext.getCmp('dfTglMintaGzPermintaanDietL').enable();
	Ext.getCmp('cbo_UnitGzPermintaanDietLookup').enable();
	Ext.getCmp('dfTglUntukGzPermintaanDietL').enable();
	Ext.getCmp('cbo_AhliGiziGzPermintaanDietLookup').enable();
	Ext.getCmp('txtKetGZPenerimaanL').enable();
	Ext.getCmp('btnDeletePermintaan_viGzPermintaanDiet').disable();
	Ext.getCmp('btnAddPasienGzPermintaanDietL').disable();
}


function ShowPesanWarningGzPermintaanDiet(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:290
		}
	);
};

function ShowPesanErrorGzPermintaanDiet(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:290
		}
	);
};


function ShowPesanInfoGzPermintaanDiet(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:290
		}
	);
};