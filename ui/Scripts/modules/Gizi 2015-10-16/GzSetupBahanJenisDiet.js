var dataSource_BahanJenisDiet;
var rowselected_BahanJenisDiet;
var Lookup_BahanJenisDiet;
var disData_BahanJenisDiet=0;
var kdjenisbahanjenisdiet;
var GridBarisBahanJenisDiet=0;
var varKodeBahanBahanJenisDiet;
var vanNamaBahanBahanJenisDiet;
var varKodeSatuanBahanJenisDiet;
var varKeteranganBahanJenisDiet;
var BahanJenisDiet={};
BahanJenisDiet.form={};
BahanJenisDiet.func={};
BahanJenisDiet.vars={};
BahanJenisDiet.func.parent=BahanJenisDiet;
BahanJenisDiet.form.ArrayStore={};
BahanJenisDiet.form.ComboBox={};
BahanJenisDiet.form.DataStore={};
BahanJenisDiet.form.Record={};
BahanJenisDiet.form.Form={};
BahanJenisDiet.form.Grid={};
BahanJenisDiet.form.Panel={};
BahanJenisDiet.form.TextField={};
BahanJenisDiet.form.Button={};

BahanJenisDiet.form.ArrayStore.jenisdietbahanjenisdiet=new Ext.data.ArrayStore({
		id: 0,
		fields: [	
					'kd_jenis','jenis_diet'
				],
		data: []
	});
var gridListHasilBahanJenisDiet={};
gridListHasilBahanJenisDiet.form={};
gridListHasilBahanJenisDiet.func={};
gridListHasilBahanJenisDiet.vars={};
gridListHasilBahanJenisDiet.func.parent=gridListHasilBahanJenisDiet;
gridListHasilBahanJenisDiet.form.ArrayStore={};
gridListHasilBahanJenisDiet.form.ComboBox={};
gridListHasilBahanJenisDiet.form.DataStore={};
gridListHasilBahanJenisDiet.form.Record={};
gridListHasilBahanJenisDiet.form.Form={};
gridListHasilBahanJenisDiet.form.Grid={};
gridListHasilBahanJenisDiet.form.Panel={};
gridListHasilBahanJenisDiet.form.TextField={};
gridListHasilBahanJenisDiet.form.Button={};
gridListHasilBahanJenisDiet.form.ArrayStore.bahanbahanjenisdiet=new Ext.data.ArrayStore({
		id: 0,
		fields: [	
					'kd_jenis','kd_bahan','nama_bahan','kd_satuan','keterangan'
				],
		data: []
	});
CurrentPage.page = getPanelBahanJenisDiet(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function datasourcefunction_BahanJenisDiet(){
	dataSource_BahanJenisDiet.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'vBahanJenisDiet',
                    param : 'd.kd_jenis=~'+kdjenisbahanjenisdiet+'~'
                }			
            }
        );   
    return dataSource_BahanJenisDiet;
	}
	
function getPanelBahanJenisDiet(mod_id) {
    var Field = ['KD_JENIS_BAHANJENISDIET','KD_BAHAN_BAHANJENISDIET','NAMA_BAHANJENISDIET','KD_SATUAN_BAHANJENISDIET','KETERANGAN_BAHANJENISDIET'];
    dataSource_BahanJenisDiet = new WebApp.DataStore({
        fields: Field
    });
	//datasourcefunction_BahanJenisDiet();
    gridListHasilBahanJenisDiet = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dataSource_BahanJenisDiet,
        anchor: '100% 70%',
        columnLines: true,
        autoScroll: false,
        border: true,
        sort: false,
        autoHeight: false,
        layout: 'fit',
        region: 'center',
		autoheight: true,
        height: 200,
        selModel: new Ext.grid.CellSelectionModel({
            singleSelect: true,
            listeners: {
                cellselect: function (sm, row, rec) {
					GridBarisBahanJenisDiet=1;
					var line	=gridListHasilBahanJenisDiet.getSelectionModel().selection.cell[0];
					//alert(dataSource_BahanJenisDiet.getRange()[line].data.KETERANGAN_BAHANJENISDIET);
							
					//rowselected_BahanJenisDiet=dataSource_BahanJenisDiet.getAt(row);
                }
            }
        }),
        listeners: {
           rowdblclick: function (sm, ridx, cidx) {
                /*rowselected_BahanJenisDiet=dataSource_BahanJenisDiet.getAt(ridx);
				disData_BahanJenisDiet=1;
				disabled_data_BahanJenisDiet(disData_BahanJenisDiet);
				ShowLookup_BahanJenisDiet(rowselected_BahanJenisDiet.json);*/
            }
        },
        cm: new Ext.grid.ColumnModel(
                [
				new Ext.grid.RowNumberer({
					header:'No.'
					}),
                    {
                        id: 'colKdBahanViewHasilBahanJenisDiet',
                        header: 'Kode Bahan',
                        dataIndex: 'KD_BAHAN_BAHANJENISDIET',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 15
                    },
                    {
                        id: 'colNamaBahanViewHasilBahanJenisDiet',
                        header: 'Nama Bahan',
                        dataIndex: 'NAMA_BAHANJENISDIET',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50,
						
						editor:new Nci.form.Combobox.autoComplete({
							store	: gridListHasilBahanJenisDiet.form.ArrayStore.bahanbahanjenisdiet,
							select	: function(a,b,c){
								var line	=gridListHasilBahanJenisDiet.getSelectionModel().selection.cell[0];
								dataSource_BahanJenisDiet.getRange()[line].data.KD_BAHAN_BAHANJENISDIET=b.data.kd_bahan;
								dataSource_BahanJenisDiet.getRange()[line].data.NAMA_BAHANJENISDIET=b.data.nama_bahan;
								dataSource_BahanJenisDiet.getRange()[line].data.KD_SATUAN_BAHANJENISDIET=b.data.kd_satuan;
								dataSource_BahanJenisDiet.getRange()[line].data.KETERANGAN_BAHANJENISDIET=b.data.keterangan;
								varKodeBahanBahanJenisDiet=dataSource_BahanJenisDiet.getRange()[line].data.KD_BAHAN_BAHANJENISDIET;
								vanNamaBahanBahanJenisDiet=dataSource_BahanJenisDiet.getRange()[line].data.NAMA_BAHANJENISDIET;
								varKodeSatuanBahanJenisDiet=dataSource_BahanJenisDiet.getRange()[line].data.KD_SATUAN_BAHANJENISDIET;
							/*varKeteranganBahanJenisDiet=dataSource_BahanJenisDiet.getRange()[line].data.KETERANGAN_BAHANJENISDIET;
*/								//save_data_BahanJenisDiet();
								gridListHasilBahanJenisDiet.getView().refresh();
						},
					insert	: function(o){
						return {
							//kd_jenis        : o.kd_jenis,
							kd_bahan 		: o.kd_bahan,
							nama_bahan		: o.nama_bahan,
							kd_satuan		: o.kd_satuan,
							//keterangan		: o.keterangan,
							text			:  '<table style="font-size: 11px;"><tr><td width="50">'+o.kd_bahan+'</td><td width="200">'+o.nama_bahan+'</td></tr></table>',
						}
					},
					param:function(){
							return {
								
								/*kd_unitLama:GzPermintaanDiet.vars.kd_unit,
								kd_unitNew:Ext.getCmp('cbo_UnitGzPermintaanDietLookup').getValue()*/
							}
					},
					url		: baseURL + "index.php/gizi/bahanjenisdiet/getBahanBahanJenisDiet",
					valueField: 'nama_bahan',
					displayField: 'text',
					listWidth: 380
				})
                    },
					{
                        id: 'colKodeSatuanBahanViewHasilBahanJenisDiet',
                        header: 'Kode Satuan',
                        dataIndex: 'KD_SATUAN_BAHANJENISDIET',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50,
						
                    },
					{
                        id: 'colKeteranganBahanViewHasilBahanJenisDiet',
                        header: 'Keterangan',
                        dataIndex: 'KETERANGAN_BAHANJENISDIET',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50,
						editor: new Ext.form.TextField({
							allowBlank: false,
							listeners:{
								blur: function(a){
								//	var line	= this.index;
//									if(a.getValue()=='' || a.getValue()==undefined){
//										ShowPesanErrorgzPenerimaanOrder('Harga beli belum di isi', 'Warning');
//									}else{
//										dsDataGrdDetailOrder_viGzPenerimaanDiet.getRange()[line].data.harga_beli=a.getValue();
//										hasilJumlah();
//									}
								},
								focus: function(a){
									//this.index=gzPenerimaanOrder.form.Grid.a.getSelectionModel().selection.cell[0];
								}
							}
						})	
                    },
                ]
                ),
        viewConfig: {
            forceFit: true
        },
        tbar:[
			{
                id: 'btnTambahBahanJenisDiet',
                text: 'Add Bahan',
                tooltip: 'Add Bahan',
                iconCls: 'add',
				disabled:true, 
                handler: function(){
					var records = new Array();
                    records.push(new dataSource_BahanJenisDiet.recordType());
					dataSource_BahanJenisDiet.add(records);
                }
            },
			{
                id: 'btnDeleteRowBahanJenisDiet',
                text: 'Delete',
                tooltip: 'Delete',
                iconCls: 'Remove',
				disabled:true,
                handler: function (sm, row, rec) {
					if (GridBarisBahanJenisDiet==1) {
						var line	=gridListHasilBahanJenisDiet.getSelectionModel().selection.cell[0];
						var cekKodebahanjenisdiet=dataSource_BahanJenisDiet.getRange()[line].data.KD_BAHAN_BAHANJENISDIET;
						if (cekKodebahanjenisdiet!=undefined)
						{
							delete_data_BahanJenisDiet();
						}
						else
						{
							dataSource_BahanJenisDiet.removeAt(line);
							gridListHasilBahanJenisDiet.getView().refresh();
							GridBarisBahanJenisDiet=0;
						}
					}
                    //alert(datasourcefunction_BahanJenisDiet().totalLength);
					//console.log(datasourcefunction_BahanJenisDiet());
				}
            }
			
		]
    });
	   var EditTextKeterangan = new  Ext.form.TextField(
	   {
			name: 'TxtKeterangan',
            id: 'Txtketerangan',
            width: 80,
	   }
		   );
	   var FormDepanBahanJenisDiet = new Ext.Panel({
        id: mod_id,
        closable: true,
        region: 'center',
        layout: 'form',
        title: 'Setup Bahan Jenis Diet',
        border: false,
        shadhow: true,
        autoScroll: false,
		width: 50,
        iconCls: 'Request',
        margins: '0 5 5 0',
        items: [
			getPanelPencarianBahanJenisDiet(),
			gridListHasilBahanJenisDiet,
            
            
//            PanelPengkajian(),
//            TabPanelPengkajian()
        ],
        listeners: {
            'afterrender': function () {

            }
        },
		tbar:[
			{
                id: 'btnSaveDataBahanJenisDiet',
                text: 'Save Data',
                tooltip: 'Save Data',
                iconCls: 'Save',
                handler: function (sm, row, rec) {
					//alert(dataSource_BahanJenisDiet.getCount());
					save_data_BahanJenisDiet();
					gridListHasilBahanJenisDiet.getView().refresh();
                },
				
            },
			{
        	xtype: 'tbseparator'
       		},
			{
                id: 'btnDeleteDataBahanJenisDiet',
                text: 'Delete All Data',
                tooltip: 'Delete All Data',
                iconCls: 'Remove',
                handler: function (sm, row, rec) {
					var datagridbahanjenisdiet=dataSource_BahanJenisDiet.getCount();
					if(datagridbahanjenisdiet!=0){
                    	delete_all_data_BahanJenisDiet();
					}
					else
						ShowPesanError_BahanJenisDiet('Maaf ! Data tidak ada yang dihapus', 'Error');
					
				}
            }
			,
			{
        	xtype: 'tbseparator'
       		},
			{
                id: 'btnRefreshDataBahanJenisDiet',
                text: 'Refresh',
                tooltip: 'Refresh Data',
                iconCls: 'Refresh',
                handler: function (sm, row, rec) {
                    datasourcefunction_BahanJenisDiet()
					
				}
            }
		]
    });

    return FormDepanBahanJenisDiet;
}
;
function getPanelPencarianBahanJenisDiet() {
    var items = {
        layout: 'column',
        border: false,
		//autoHeight: true,
		//height:50,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 1190,
                height: 50,
                anchor: '100% 100%',
                items: [
                    /*{
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'Kode Jenis Diet '
                    },
                    {
                        x: 110,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 10,
                        xtype: 'textfield',
                        name: 'TxtKdJenisDiet',
                        id: 'TxtKdJenisDiet',
                        width: 80
                    },*/
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'Nama Jenis Diet'
                    },
                    {
                        x: 110,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    BahanJenisDiet.form.ComboBox.jenisdietbahanjenisdiet= new Nci.form.Combobox.autoComplete({
						//id:'TxtNamaJenisTarifCustome',
						store	: BahanJenisDiet.form.ArrayStore.jenisdietbahanjenisdiet,
						select	: function(a,b,c){
									kdjenisbahanjenisdiet=b.data.kd_jenis;
									datasourcefunction_BahanJenisDiet();
									//alert(datasourcefunction_BahanJenisDiet.getCount);
									Ext.getCmp('btnTambahBahanJenisDiet').enable();
									Ext.getCmp('btnDeleteRowBahanJenisDiet').enable();
						},		
						width	: 150,
						insert	: function(o){
							
							return {
								kd_jenis        	:o.kd_jenis,
								jenis_diet        	:o.jenis_diet,
								text				:'<table style="font-size: 11px;"><tr><td width="80">'+o.kd_jenis+'</td><td width="180">'+o.jenis_diet+'</td></tr></table>',
								   }
							//a=alert('hai');
						},
						url		: baseURL + "index.php/gizi/bahanjenisdiet/getJenisDietBahanJenisDiet",
						valueField: 'jenis_diet',
						displayField: 'text',
						listWidth: 260,
						x: 120,
                        y: 10,
						
					}),

                ]
            }
        ]
    };
    return items;
}
;
function ckbox()
{
	var CekBoxKu = new Ext.form.Checkbox({
		id: 'cbKu',
		xtype:'checkbox',
		text: 'Coba',
		x: 520,
        y: 10,
		},
		{xtype: 'tbtext', text: 'Data Pasien Hari Ini ', cls: 'left-label', width: 510}
		);
	return CekBoxKu;
}
function save_data_BahanJenisDiet()
{
	if (ValidasiEntri_BahanJenisDiet(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/main/CreateDataObj",
				params: ParameterSave_BahanJenisDiet(),
				failure: function(o)
				{
					ShowPesanError_BahanJenisDiet('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanSukses_BahanJenisDiet(nmPesanSimpanSukses,nmHeaderSimpanData);
						datasourcefunction_BahanJenisDiet();
						//kdjenisbahanjenisdiet='';
						varKodeBahanBahanJenisDiet='';
						vanNamaBahanBahanJenisDiet='';
						varKodeSatuanBahanJenisDiet='';
						varKeteranganBahanJenisDiet='';
						//ClearText_BahanJenisDiet();
					}
					else 
					{
						ShowPesanError_BahanJenisDiet('Gagal Menyimpan Data ini', 'Error');
						datasourcefunction_BahanJenisDiet
					};
				}
			}
			
		)
	}
}
function edit_data_BahanJenisDiet(){
	disData_BahanJenisDiet=1;
	disabled_data_BahanJenisDiet(disData_BahanJenisDiet);
	ShowLookup_BahanJenisDiet(rowselected_BahanJenisDiet.json);
}
function delete_data_BahanJenisDiet() 
{
		Ext.Msg.show
		(
			{
			   title:nmHeaderHapusData,
			   msg: nmGetValidasiHapus(nmEmp2) ,
			   buttons: Ext.MessageBox.YESNO,
			   width:250,
			   fn: function (btn) 
			   {	
			   		
					if (btn === 'yes') 
					{
							Ext.Ajax.request
							(
								{
									url: WebAppUrl.UrlDeleteData,
									params: paramsDelete_BahanJenisDiet(),
									success: function(o) 
									{
										var cst = Ext.decode(o.responseText);
										if (cst.success === true) 
										{
											ShowPesanSukses_BahanJenisDiet(nmPesanHapusSukses,nmHeaderHapusData);
											datasourcefunction_BahanJenisDiet();
											GridBarisBahanJenisDiet=0
										}
										else if (cst.success === false && cst.pesan===0)
										{
											ShowPesanError_BahanJenisDiet(nmPesanHapusGagal,nmHeaderHapusData);
										}
										else 
										{
											ShowPesanError_BahanJenisDiet(nmPesanHapusError,nmHeaderHapusData);
										};
									}
								}
							)
							
					};
				}
			}
		)
	};
	
function delete_all_data_BahanJenisDiet() 
{
		Ext.Msg.show
		(
			{
			   title:nmHeaderHapusData,
			   msg: nmGetValidasiHapus(nmEmp2) ,
			   buttons: Ext.MessageBox.YESNO,
			   width:250,
			   fn: function (btn) 
			   {	
			   		
					if (btn === 'yes') 
					{
							Ext.Ajax.request
							(
								{
									url: WebAppUrl.UrlDeleteData,
									params: paramsAllDelete_BahanJenisDiet(),
									success: function(o) 
									{
										var cst = Ext.decode(o.responseText);
										if (cst.success === true) 
										{
											ShowPesanSukses_BahanJenisDiet(nmPesanHapusSukses,nmHeaderHapusData);
											datasourcefunction_BahanJenisDiet();
											GridBarisBahanJenisDiet=0
										}
										else if (cst.success === false && cst.pesan===0)
										{
											ShowPesanError_BahanJenisDiet(nmPesanHapusGagal,nmHeaderHapusData);
										}
										else 
										{
											ShowPesanError_BahanJenisDiet(nmPesanHapusError,nmHeaderHapusData);
										};
									}
								}
							)
							
					};
				}
			}
		)
	};
	
function addnew_data_BahanJenisDiet()
{
	ClearText_BahanJenisDiet();/*
	disData_BahanJenisDiet=0;
	disabled_data_BahanJenisDiet(disData_BahanJenisDiet);
	TombolTambahBaru_BahanJenisDiet : Ext.getCmp('TxtKdAhliGz').focus();*/
}
function ValidasiEntri_BahanJenisDiet(modul,mBolHapus)
{
	
	var x = 1;
	var datagridbahanjenisdiet=dataSource_BahanJenisDiet.getCount();
	if(datagridbahanjenisdiet==0){
	ShowPesanError_BahanJenisDiet('Data Belum Diisi Lengkap', 'Warning');
	x = 0;
	}
	for(var i = 0 ; i < dataSource_BahanJenisDiet.getCount();i++)
	{
		var o=dataSource_BahanJenisDiet.getRange()[i].data;
		for(var j = i+1 ; j < dataSource_BahanJenisDiet.getCount();j++)
		{
			var p=dataSource_BahanJenisDiet.getRange()[j].data;
			if (p.KD_BAHAN_BAHANJENISDIET==o.KD_BAHAN_BAHANJENISDIET)
			{
				ShowPesanError_BahanJenisDiet('Bahan Tidak Boleh Sama', 'Warning');
				x = 0;
			}
		}
		
	}
	
	
	return x;
};
function ValidasiEdit_BahanJenisDiet(modul,mBolHapus)
{
	var kode = Ext.getCmp('TxtKdAhliGz').getValue();
	var nama = Ext.getCmp('TxtNamaAhliGz').getValue();
	
	var x = 1;
	if(kode === '' ){
	ShowPesanError_BahanJenisDiet('Data Belum Diisi Lengkap', 'Warning');
	x = 0;
	}
	if( nama === '')
	{
	ShowPesanError_BahanJenisDiet('Data Belum Diisi Lengkap', 'Warning');
	x = 0;	
	}
	return x;
};

function ShowPesanSukses_BahanJenisDiet(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};

function ShowPesanError_BahanJenisDiet(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:290
		}
	);
};
function ShowPesanWarning_BahanJenisDiet(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:290
		}
	);
};
function ClearText_BahanJenisDiet()
{
	var bersih={
		kodeData : Ext.getCmp('TxtKdAhliGz').setValue(""),
		namaData : Ext.getCmp('TxtNamaAhliGz').setValue("")
		}
	return bersih;
}
function disabled_data_BahanJenisDiet(disDatax){
	if (disDatax==1) {
		var dis= 
				{
					kodeData : Ext.getCmp('TxtKdAhliGz').disable(),
				}
	}
	else if (disDatax==0) 
	{
		var dis= 
				{
					kodeData : Ext.getCmp('TxtKdAhliGz').enable(),
				}
	}
	return dis;
	}
function ParameterSave_BahanJenisDiet()
{
var params= 
	{
		Table: 'vBahanJenisDiet'
	}	
	
	params['jumlah']=dataSource_BahanJenisDiet.getCount();
	for(var i = 0 ; i < dataSource_BahanJenisDiet.getCount();i++)
	{
		params['kodeJenisDetJenisDiet-'+i]=kdjenisbahanjenisdiet;
		params['kodeBahanDetJenisDiet-'+i]=dataSource_BahanJenisDiet.data.items[i].data.KD_BAHAN_BAHANJENISDIET;
		params['keteranganDetJenisDiet-'+i]=dataSource_BahanJenisDiet.data.items[i].data.KETERANGAN_BAHANJENISDIET;
		
	}
return params;	
}
function paramsAllDelete_BahanJenisDiet() 
{
    var params =
	{	
		Table:'vBahanJenisDiet',
		pilihan:'hapussemuabaris',
		KdJenisBahanjenisDiet:kdjenisbahanjenisdiet
	};
    return params
};
function paramsDelete_BahanJenisDiet() 
{
	var line	=gridListHasilBahanJenisDiet.getSelectionModel().selection.cell[0];
    var params =
	{	
		Table:'vBahanJenisDiet',
		pilihan:'hapussatubaris',
		KdJenisBahanjenisDiet:kdjenisbahanjenisdiet,
		KdBahanJenisDiet: dataSource_BahanJenisDiet.data.items[line].data.KD_BAHAN_BAHANJENISDIET
	};
    return params
};
function ShowLookup_BahanJenisDiet(rowdata)
{
	if (rowdata == undefined){
        ShowPesanError_BahanJenisDiet('Data Kosong', 'Error');
    }
    else
    {
      datainit_form_BahanJenisDiet(rowdata); 
    }
}
function ShowLookupDelete_BahanJenisDiet(rowdata)
{
      var kode =  rowdata.KD_BAHAN_BAHANJENISDIET;
	  return kode;
}
function datainit_form_BahanJenisDiet(rowdata){
	Ext.getCmp('TxtKdAhliGz').setValue(rowdata.KD_AHLI_GIZI);
	Ext.getCmp('TxtNamaAhliGz').setValue(rowdata.NAMA_AHLI_GIZI);
}