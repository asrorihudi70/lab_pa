var dataSourceGolTarif;
var rowselectedGolTarif;
var LookupGolTarif;
var disDataGolTarif=0;

CurrentPage.page = getPanelGolTarif(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function datasourcefunction_GolTarif(){
	dataSourceGolTarif.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: 100,
                    Sort: 'kd_tarif',
                    Sortdir: 'ASC', 
                    target: 'vGolTarif'
                    //param : kriteria
                }
							
            }
			
        );   
    return dataSourceGolTarif;
	}
	
function getPanelGolTarif(mod_id) {
    var Field = ['KD_TARIF_GOLTARIF','TARIF_GOLTARIF'];
    dataSourceGolTarif = new WebApp.DataStore({
        fields: Field
    });
	datasourcefunction_GolTarif();
	console.log(datasourcefunction_GolTarif());
    var gridListHasilGolTarif = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dataSourceGolTarif,
        anchor: '100% 70%',
        columnLines: false,
        autoScroll: false,
        border: true,
        sort: false,
        autoHeight: false,
        layout: 'fit',
        region: 'center',
        height: 200,
        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
            listeners: {
                rowselect: function (sm, row, rec) {
					rowselectedGolTarif=dataSourceGolTarif.getAt(row);
                }
            }
        }),
        listeners: {
           rowdblclick: function (sm, ridx, cidx) {
                rowselectedGolTarif=dataSourceGolTarif.getAt(ridx);
				disDataGolTarif=1;
				disabled_dataGolTarif(disDataGolTarif);
				ShowLookupGolTarif(rowselectedGolTarif.json);
            }
        },
        cm: new Ext.grid.ColumnModel(
                [
				new Ext.grid.RowNumberer({
					header:'No.'
					}),
                    {
                        id: 'colKdTarifViewHasilGolTarif',
                        header: 'Kode',
                        dataIndex: 'KD_TARIF_GOLTARIF',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 5
                    },
                    {
                        id: 'colTarifViewHasilGolTarif',
                        header: 'Tarif',
                        dataIndex: 'TARIF_GOLTARIF',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                ]
                ),
        viewConfig: {
            forceFit: true
		},
        tbar:[
			{
                id: 'btnEditDataGolTarif',
                text: 'Edit Data',
                tooltip: 'Edit Data',
                iconCls: 'Edit_Tr',
                handler: function (sm, row, rec) {
                    edit_dataGolTarif();
                },
				
            },
		]
    });
	   var FormDepanGolTarif = new Ext.Panel({
        id: mod_id,
        closable: true,
        region: 'center',
        layout: 'form',
        title: 'Setup Golongan Tarif',
        border: false,
        shadhow: true,
        autoScroll: false,
		width: 250,
        iconCls: 'Request',
        margins: '0 5 5 0',
		tbar: [
			
			{
                id: 'btnAddNewDataGolTarif',
                text: 'Add New',
                tooltip: 'add',
                iconCls: 'add',
                handler: function (sm, row, rec) {
                    addnew_dataGolTarif();
				}
            },
			{
        	xtype: 'tbseparator'
       		},
			{
                id: 'btnSaveDataGolTarif',
                text: 'Save Data',
                tooltip: 'Save Data',
                iconCls: 'Save',
                handler: function (sm, row, rec) {
                    save_dataGolTarif();
                },
				
            },
			{
        	xtype: 'tbseparator'
       		},
			{
                id: 'btnDeleteDataGolTarif',
                text: 'Delete Data',
                tooltip: 'Delete Data',
                iconCls: 'Remove',
                handler: function (sm, row, rec) {
					if (rowselectedGolTarif!=undefined)
						delete_dataGolTarif();
					else
						ShowPesanErrorGolTarif('Maaf ! Tidak ada data yang terpilih', 'Error');
				}
            },
			{
        	xtype: 'tbseparator'
       		},
			{
                id: 'btnRefreshDataGolTarif',
                text: 'Refresh',
                tooltip: 'Refresh',
                iconCls: 'Refresh',
                handler: function (sm, row, rec) {
                    datasourcefunction_GolTarif();
                },
				
            }
			],
        items: [
			getPanelPencarianGolTarif(),
			gridListHasilGolTarif,
            
//            PanelPengkajian(),
//            TabPanelPengkajian()
        ],
        listeners: {
            'afterrender': function () {
				
            }
        }
    });
	
	
    return FormDepanGolTarif;

}
;
function getPanelPencarianGolTarif() {
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 1190,
				height: 75,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'Kode '
                    },
                    {
                        x: 110,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 10,
                        xtype: 'textfield',
                        name: 'TxtKdTarif',
                        id: 'TxtKdTarif',
                        width: 80,
						readOnly: true

                    },
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Tarif'
                    },
                    {
                        x: 110,
                        y: 40,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 40,
                        xtype: 'textfield',
                        name: 'TxtTarif',
                        id: 'TxtTarif',
                        width: 200
                    },
                ]
            }
        ]
    };
    return items;
}
;
function save_dataGolTarif()
{
	if (ValidasiEntriGolTarif(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/main/CreateDataObj",
				params: ParameterSaveGolTarif(),
				failure: function(o)
				{
					ShowPesanErrorGolTarif('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanSuksesGolTarif(nmPesanSimpanSukses,nmHeaderSimpanData);
						Ext.getCmp('TxtKdTarif').setValue(cst.kodetarif);
						datasourcefunction_GolTarif();
						disDataGolTarif=0;
						disabled_dataGolTarif(disDataGolTarif);
						ClearTextGolTarif();
						
					}
					else 
					{
						ShowPesanErrorGolTarif('Gagal Menyimpan Data ini', 'Error');
						datasourcefunction_GolTarif();
					};
				}
			}
			
		)
	}
}
function edit_dataGolTarif(){
	disDataGolTarif=1;
	disabled_dataGolTarif(disDataGolTarif);
	ShowLookupGolTarif(rowselectedGolTarif.json);
}
function delete_dataGolTarif() 
{
		Ext.Msg.show
		(
			{
			   title:nmHeaderHapusData,
			   msg: nmGetValidasiHapus(nmEmp2) ,
			   buttons: Ext.MessageBox.YESNO,
			   width:250,
			   fn: function (btn) 
			   {			
						if (btn === 'yes') 
						{
							Ext.Ajax.request
							(
								{
									url: WebAppUrl.UrlDeleteData,
									params: paramsDeleteGolTarif(),
									success: function(o) 
									{
										var cst = Ext.decode(o.responseText);
										if (cst.success === true) 
										{
											ShowPesanSuksesGolTarif(nmPesanHapusSukses,nmHeaderHapusData);
											Ext.getCmp('TxtKdTarif').setValue(cst.kodetarif);
											datasourcefunction_GolTarif();
											ClearTextGolTarif();
											disDataGolTarif=0;
											disabled_dataGolTarif(disDataGolTarif);
											
										}
										else if (cst.success === false && cst.pesan===0)
										{
											ShowPesanErrorGolTarif(nmPesanHapusGagal,nmHeaderHapusData);
										}
										else 
										{
											ShowPesanErrorGolTarif(nmPesanHapusError,nmHeaderHapusData);
										};
									}
								}
							)
							disDataGolTarif=0;
							disabled_dataGolTarif(disDataGolTarif);
						};	
				}
			}
		)
	};
	
function addnew_dataGolTarif()
{
	ClearTextGolTarif();
	disDataGolTarif=0;
	disabled_dataGolTarif(disDataGolTarif);
	TombolKodeTarifGol : Ext.getCmp('TxtTarif').focus();
}
function ValidasiEntriGolTarif(modul,mBolHapus)
{
	//var kode = Ext.getCmp('TxtKdTarif').setValue(AutoKode);
	var nama = Ext.getCmp('TxtTarif').getValue();
	
	var x = 1;
	/*if(kode === 0 ){
	ShowPesanErrorGolTarif('Data Belum Diisi Lengkap', 'Warning');
	x = 0;
	}*/
	if( nama === '')
	{
	ShowPesanErrorGolTarif('Data Belum Diisi Lengkap', 'Warning');
	x = 0;	
	}
	
	
	return x;
};


function ShowPesanSuksesGolTarif(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};

function ShowPesanErrorGolTarif(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:290
		}
	);
};
function ClearTextGolTarif()
{
	var bersih={
		kodeData : Ext.getCmp('TxtKdTarif').setValue(""),
		namaData : Ext.getCmp('TxtTarif').setValue("")
		}
	return bersih;
}
function disabled_dataGolTarif(disDatax){
	if (disDatax==1) {
		var dis= 
				{
					kodeData : Ext.getCmp('TxtKdTarif').disable()
				}
	}
	else if (disDatax==0) 
	{
		var dis= 
				{
					kodeData : Ext.getCmp('TxtKdTarif').enable()
				}
	}
	return dis;
	}
function ParameterSaveGolTarif()
{
var params =
	{
	Table: 'vGolTarif',	
	kodeData : Ext.getCmp('TxtKdTarif').getValue(),
	namaData : Ext.getCmp('TxtTarif').getValue()
	}	
return params;	
}
function paramsDeleteGolTarif() 
{
	
		var params =
		{
			Table:'vGolTarif',
			kodeData : Ext.getCmp('TxtKdTarif').getValue(),
			Data: ShowLookupDeleteGolTarif(rowselectedGolTarif.json)
		};
	return params
};
function ShowLookupGolTarif(rowdata)
{
	if (rowdata == undefined){
        ShowPesanErrorGolTarif('Data Kosong', 'Error');
    }
    else
    {
      datainit_formGolTarif(rowdata); 
    }
}
function ShowLookupDeleteGolTarif(rowdata)
{
      var kode =  rowdata.KD_TARIF_GOLTARIF;
	  return kode;
}
function datainit_formGolTarif(rowdata){
	Ext.getCmp('TxtKdTarif').setValue(rowdata.KD_TARIF_GOLTARIF);
	Ext.getCmp('TxtTarif').setValue(rowdata.TARIF_GOLTARIF);
	}