var dsvTarifCust;
var dsvGolTarif;
var rowselectedTarifCust;
var LookupTarifCust;
var disDataTarifCust=0;
var cek=0;
var kdGolTarifcustomer;
var kdtarifgoltarif;
var stateComboTarifCust=['KD_TARIF_GOLTARIF','TARIF_GOLTARIF'];

var TarifCustomer={};
TarifCustomer.form={};
TarifCustomer.func={};
TarifCustomer.vars={};
TarifCustomer.func.parent=TarifCustomer;
TarifCustomer.form.ArrayStore={};
TarifCustomer.form.ComboBox={};
TarifCustomer.form.DataStore={};
TarifCustomer.form.Record={};
TarifCustomer.form.Form={};
TarifCustomer.form.Grid={};
TarifCustomer.form.Panel={};
TarifCustomer.form.TextField={};
TarifCustomer.form.Button={};

TarifCustomer.form.ArrayStore.jenisdiettarifcust=new Ext.data.ArrayStore({
		id: 0,
		fields: [	
					'kd_jenis','jenis_diet','harga_pokok','harga_jual','tag_berlaku','kd_gol','tarif'
				],
		data: []
	});
	
CurrentPage.page = getPanelTarifCust(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dsTarifCust(){
	dsvTarifCust.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'vTarifCust'
                    //param : kriteria
                }			
            }
        );   
    return dsvTarifCust;
	}
function dsGolTarif(){
	dsvGolTarif.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'vGolTarif',
                    //param : 'kd_tarif=1'
                }			
            }
        );   
    return dsvGolTarif;
	}
	
function getPanelTarifCust(mod_id) {
    var Field = ['KD_JENIS_TARCUS','NAMA_JENISDIET_TARCUS','HARGA_POKOK_TARCUS','HARGA_JUAL_TARCUS','TAG_BERLAKU_TARCUS','KD_GOL_TARCUS','KD_TARIFGOL_TARCUS','KET_TARIF'];
    dsvTarifCust = new WebApp.DataStore({
        fields: Field
    });
	dsTarifCust();
    var gridListHasilTarifCust = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dsvTarifCust,
        anchor: '100% 70%',
        columnLines: false,
        autoScroll: false,
        border: true,
        sort: false,
        autoHeight: false,
        layout: 'fit',
        region: 'center',
        height: 200,
        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
            listeners: {
                rowselect: function (sm, row, rec) {
					rowselectedTarifCust=dsvTarifCust.getAt(row);
                }
            }
        }),
        listeners: {
           rowdblclick: function (sm, ridx, cidx) {
                rowselectedTarifCust=dsvTarifCust.getAt(ridx);
				ShowLookupTarifCust(rowselectedTarifCust.json);
				/*disData=1;
				disabled_data(disData);*/
				
            }
        },
        cm: new Ext.grid.ColumnModel(
                [
				new Ext.grid.RowNumberer({
					header:'No.'
					}),
                    {
                        id: 'colKdTarifCustViewHasilTarifCust',
                        header: 'Kode',
                        dataIndex: 'KD_JENIS_TARCUS',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 5
                    },
                    {
                        id: 'colTarifCustViewHasilTarifCust',
                        header: 'Nama',
                        dataIndex: 'NAMA_JENISDIET_TARCUS',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
					{
                        id: 'colKetTarifCustViewHasilTarifCust',
                        header: 'Tarif',
                        dataIndex: 'KET_TARIF',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 30
                    },
					{
                        id: 'colHargaPokokTarifCustViewHasilTarifCust',
                        header: 'Harga Pokok',
                        dataIndex: 'HARGA_POKOK_TARCUS',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 20
                    },
					{
                        id: 'colHargaJualTarifCustViewHasilTarifCust',
                        header: 'Harga Jual',
                        dataIndex: 'HARGA_JUAL_TARCUS',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 10
                    },
                ]
                ),
        viewConfig: {
            forceFit: true
        },
        tbar:[
			{
                id: 'btnEditDataTarifCust',
                text: 'Edit Data',
                tooltip: 'Edit Data',
                iconCls: 'Edit_Tr',
                handler: function (sm, row, rec) {
                    edit_dataTarifCust();
                },
				
            },
		]
    });
	   var FormDepanTarifCust = new Ext.Panel({
        id: mod_id,
        closable: true,
        region: 'center',
        layout: 'form',
        title: 'Setup Tarif Customer',
        border: false,
        shadhow: true,
        autoScroll: false,
		width: 250,
        iconCls: 'Request',
        margins: '0 5 5 0',
		tbar: [
			
			{
                id: 'btnAddNewDataTarifCust',
                text: 'Add New',
                tooltip: 'addnewtarifcust',
                iconCls: 'add',
                handler: function (sm, row, rec) {
                    addnew_dataTarifCust();
				}
            },
			{
        	xtype: 'tbseparator'
       		},
			{
                id: 'btnSaveDataTarifCust',
                text: 'Save Data',
                tooltip: 'Save Data',
                iconCls: 'Save',
                handler: function (sm, row, rec) {
                    save_dataTarifCust();
					//alert(cek);
                },
				
            },
			{
        	xtype: 'tbseparator'
       		},
			{
                id: 'btnDeleteDataTarifCust',
                text: 'Delete Data',
                tooltip: 'Delete Data',
                iconCls: 'Remove',
                handler: function (sm, row, rec) {
					if (rowselectedTarifCust!=undefined)
                    	delete_dataTarifCust();
					else
						ShowPesanErrorTarifCust('Maaf ! Tidak ada data yang terpilih', 'Error');
				}
            },
			{
        	xtype: 'tbseparator'
       		},
			{
                id: 'btnRefreshDataTarifCust',
                text: 'Refresh',
                tooltip: 'Refresh',
                iconCls: 'Refresh',
                handler: function (sm, row, rec) {
                    refresh_dataTarifCust();
                },
				
            }
			],
        items: [
			getPanelPencarianTarifCust(),
			gridListHasilTarifCust,
            
            
//            PanelPengkajian(),
//            TabPanelPengkajian()
        ],
        listeners: {
            'afterrender': function () {

            }
        }
    });

    return FormDepanTarifCust;

}
;
function refresh_dataTarifCust()
{
	stateComboTarifCust=[null,null];
	//stateComboTarifCust=['KD_TARIF_GOLTARIF','TARIF_GOLTARIF'];
	ComboSetTarifCustomer();
	dsTarifCust();
}
function getPanelPencarianTarifCust() {
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 1190,
                height: 100,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'Kode Jenis Diet '
                    },
                    {
                        x: 110,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 10,
                        xtype: 'textfield',
                        name: 'TxtKdTarifCust',
                        id: 'TxtKdTarifCust',
                        width: 80,
						disabled: true
                    },
					{
                        xtype: 'checkbox',
                        id: 'CekBerlakuTarifCust',
                        hideLabel:false,
                        //checked: false,
						x: 210,
						y: 10,
                        listeners: 
						{
							check: function()
							{
								
								cek=1;
							}
						}
                    },
                    {
						xtype: 'tbtext',
						text: 'Berlaku ',  
						width: 190, 
						x:225,
						y:10
					},
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Nama Jenis Diet'
                    },
                    {
                        x: 110,
                        y: 40,
                        xtype: 'label',
                        text: ' : '
                    },
					
					TarifCustomer.form.ComboBox.jenisdiettarifcust= new Nci.form.Combobox.autoComplete({
						//id:'TxtNamaJenisTarifCustome',
						store	: TarifCustomer.form.ArrayStore.jenisdiettarifcust,
						select	: function(a,b,c){
							Ext.getCmp('TxtKdTarifCust').setValue(b.data.kd_jenis);
							Ext.getCmp('CmbSetTarifCust').setValue(b.data.kd_tarif);
							if (b.data.tag_berlaku==0)
								{Ext.getCmp('CekBerlakuTarifCust').setValue(false);}
							else
								{Ext.getCmp('CekBerlakuTarifCust').setValue(true);}
							
							Ext.getCmp('TxtHrgPokokTarifCust').setValue(b.data.harga_pokok);
							Ext.getCmp('TxtHrgJualTarifCust').setValue(b.data.harga_jual);
							kdGolTarifcustomer=b.data.kd_gol;
							kdtarifgoltarif=b.data.kd_tarif;
							//alert(kdtarifgoltarif+","+kdGolTarifcustomer);
						},
						width	: 150,
						
						insert	: function(o){
							return {
								kd_jenis        	:o.kd_jenis,
								jenis_diet        	:o.jenis_diet,
								tarif				:o.tarif,
								harga_pokok			:o.harga_pokok,
								harga_jual			:o.harga_jual,
								tag_berlaku			:o.tag_berlaku,
								text				:  '<table style="font-size: 11px;"><tr><td width="80">'+o.kd_jenis+'</td><td width="180">'+o.jenis_diet+'</td></tr></table>',
								kd_gol					:o.kd_gol,
								kd_tarif				:o.kd_tarif
							}
						},
						url		: baseURL + "index.php/gizi/tarifcust/getCustTarifCust",
						valueField: 'jenis_diet',
						displayField: 'text',
						listWidth: 260,
						x: 120,
                        y: 40,
						
					}),
					{
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Set Tarif'
                    },
                    {
                        x: 110,
                        y: 70,
                        xtype: 'label',
                        text: ' : '
                    },
                        ComboSetTarifCustomer()
                   	,
                    {
                        x: 290,
                        y: 10,
                        xtype: 'label',
                        text: 'Harga Pokok'
                    },
                    {
                        x: 390,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 400,
                        y: 10,
                        xtype: 'textfield',
                        name: 'TxtHrgPokokTarifCust',
                        id: 'TxtHrgPokokTarifCust',
                        width: 130,
						style: 'text-align: right'
						//align: 'right-align'
                    },
					{
                        x: 290,
                        y: 40,
                        xtype: 'label',
                        text: 'Harga Jual'
                    },
                    {
                        x: 390,
                        y: 40,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 400,
                        y: 40,
                        xtype: 'textfield',
                        name: 'TxtHrgJualTarifCust',
                        id: 'TxtHrgJualTarifCust',
                        width: 130,
						style: 'text-align: right'
						//align: 'right-align'
                    },
                ]
            }
        ]
    };
    return items;
}
;
function ComboSetTarifCustomer()
{
    dsvGolTarif = new WebApp.DataStore({fields: stateComboTarifCust});
	dsGolTarif();
	var CmbSetTarifCustomer = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 70,
			id:'CmbSetTarifCust',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			/*readOnly:true,
			emptyText:'Set Tarif',
			value: 1,*/
			width: 150,
			store: dsvGolTarif,
			valueField: stateComboTarifCust[0],
			displayField: stateComboTarifCust[1],
			//value:selectSetPilihankelompokPasien,
			listeners:
			{
				'select': function(a,b,c)
				{
					kdtarifgoltarif=CmbSetTarifCustomer.value;
					//selectSetPilihankelompokPasien=b.data.displayText;
				}
			}
		}
	);
	return CmbSetTarifCustomer;
}
function save_dataTarifCust()
{
	if (ValidasiEntriTarifCust(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/main/CreateDataObj",
				params: ParameterSaveTarifCust(),
				failure: function(o)
				{
					ShowPesanErrorTarifCust('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanSuksesTarifCust("Data Berhasil Disimpan","Sukses");
						dsTarifCust();
						ClearTextTarifCust();
					}
					else 
					{
						ShowPesanErrorTarifCust('Gagal Menyimpan Data ini', 'Error');
						dsTarifCust();
					};
				}
			}
			
		)
	}
}
function edit_dataTarifCust(){
	ShowLookupTarifCust(rowselectedTarifCust.json);
}
function delete_dataTarifCust() 
{
		Ext.Msg.show
		(
			{
			   title:nmHeaderHapusData,
			   msg: nmGetValidasiHapus(nmEmp2) ,
			   buttons: Ext.MessageBox.YESNO,
			   width:250,
			   fn: function (btn) 
			   {			
					if (btn === 'yes') 
					{
						Ext.Ajax.request
						(
							{
								url: WebAppUrl.UrlDeleteData,
								params: paramsDeleteTarifCust(),
								success: function(o) 
								{
									var cst = Ext.decode(o.responseText);
									if (cst.success === true) 
									{
										ShowPesanSuksesTarifCust(nmPesanHapusSukses,nmHeaderHapusData);
										dsTarifCust();
										ClearTextTarifCust();
									}
									else if (cst.success === false && cst.pesan===0)
									{
										ShowPesanErrorTarifCust(nmPesanHapusGagal,nmHeaderHapusData);
									}
									else 
									{
										ShowPesanErrorTarifCust(nmPesanHapusError,nmHeaderHapusData);
									};
								}
							}
						)
					};
				}
			}
		)
	};
	
function addnew_dataTarifCust()
{
	ClearTextTarifCust();
}
function ValidasiEntriTarifCust(modul,mBolHapus)
{
	var kode = Ext.getCmp('TxtKdTarifCust').getValue();
	var nama = TarifCustomer.form.ComboBox.jenisdiettarifcust.getValue();
	var hrgPokokTarifCust = Ext.getCmp('TxtHrgPokokTarifCust').getValue();
	var hrgJualTarifCust = Ext.getCmp('TxtHrgJualTarifCust').getValue();
	var CekBerlakuTarifCust = Ext.getCmp('CekBerlakuTarifCust').getValue();
	var cmbSetTarif = Ext.getCmp('CmbSetTarifCust').getValue();
	
	var x = 1;
	if(kode === '' || nama === '' || cmbSetTarif ==='' || hrgPokokTarifCust==='' || hrgJualTarifCust==='' ){
	ShowPesanErrorTarifCust('Data Belum Diisi Lengkap', 'Warning');
	x = 0;
	}/*
	if( nama === '')
	{
	ShowPesanError('Data Belum Diisi Lengkap', 'Warning');
	x = 0;	
	}*/
	
	
	return x;
};
function ValidasiEditTarifCust(modul,mBolHapus)
{
	var kode = Ext.getCmp('TxtKdTarifCust').getValue();
	var nama = TarifCustomer.form.ComboBox.jenisdiettarifcust.getValue();
	
	var x = 1;
	if(kode === '' ){
	ShowPesanErrorTarifCust('Data Belum Diisi Lengkap', 'Warning');
	x = 0;
	}
	if( nama === '')
	{
	ShowPesanErrorTarifCust('Data Belum Diisi Lengkap', 'Warning');
	x = 0;	
	}
	return x;
};

function ShowPesanSuksesTarifCust(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};

function ShowPesanErrorTarifCust(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:290
		}
	);
};
function ClearTextTarifCust()
{
	Ext.getCmp('TxtKdTarifCust').setValue("");
	tagBerlaku : Ext.getCmp('CekBerlakuTarifCust').setValue(false),
	TarifCustomer.form.ComboBox.jenisdiettarifcust.setValue("");
	Ext.getCmp('TxtHrgPokokTarifCust').setValue("");
	Ext.getCmp('TxtHrgJualTarifCust').setValue("");	
	Ext.getCmp('CmbSetTarifCust').setValue(null);
	TarifCustomer.form.ComboBox.jenisdiettarifcust.focus();
}
/*function disabled_data(disDatax){
	if (disDatax==1) {
		var dis= 
				{
					kodeData : Ext.getCmp('TxtKdAhliGz').disable(),
				}
	}
	else if (disDatax==0) 
	{
		var dis= 
				{
					kodeData : Ext.getCmp('TxtKdAhliGz').enable(),
				}
	}
	return dis;
	}*/
function ParameterSaveTarifCust()
{
	var tgBerlaku;
	if (Ext.getCmp('CekBerlakuTarifCust').getValue(true))
		tgBerlaku=1;
	else
		tgBerlaku=0;
var params =
	{
	Table: 'vTarifCust',	
	kodeJenisDietTarifCust : Ext.getCmp('TxtKdTarifCust').getValue(),
	namaJenisDietTarifCust : TarifCustomer.form.ComboBox.jenisdiettarifcust.getValue(),
	hrgPokokTarifCust : Ext.getCmp('TxtHrgPokokTarifCust').getValue(),
	hrgJualTarifCust : Ext.getCmp('TxtHrgJualTarifCust').getValue(),
	cekTagBerlakuTarifCustomer : tgBerlaku,
	KetTarif :Ext.getCmp('CmbSetTarifCust').getValue(),
	kdGolTarifCust:kdGolTarifcustomer,
	kodeTarifGolTarif:kdtarifgoltarif
	}	
return params;	
}
function paramsDeleteTarifCust() 
{
    var params =
	{
		Table:'vTarifCust',
		Data: ShowLookupDeleteTarifCust(rowselectedTarifCust.json)
	};
    return params
};
function ShowLookupTarifCust(rowdata)
{
	if (rowdata == undefined){
        ShowPesanErrorTarifCust('Data Kosong', 'Error');
    }
    else
    {
      datainit_formTarifCust(rowdata); 
    }
}
function ShowLookupDeleteTarifCust(rowdata)
{
      var kode =  rowdata.KD_GOL_TARCUS;
	  return kode;
}
function datainit_formTarifCust(rowdata){
	Ext.getCmp('TxtKdTarifCust').setValue(rowdata.KD_JENIS_TARCUS);
	TarifCustomer.form.ComboBox.jenisdiettarifcust.setValue(rowdata.NAMA_JENISDIET_TARCUS);
	Ext.getCmp('CmbSetTarifCust').setValue(rowdata.KD_TARIFGOL_TARCUS);
	if (rowdata.TAG_BERLAKU_TARCUS==0)
		{Ext.getCmp('CekBerlakuTarifCust').setValue(false);}
	else
		{Ext.getCmp('CekBerlakuTarifCust').setValue(true);}
	
	Ext.getCmp('TxtHrgPokokTarifCust').setValue(rowdata.HARGA_POKOK_TARCUS);
	Ext.getCmp('TxtHrgJualTarifCust').setValue(rowdata.HARGA_JUAL_TARCUS);
	kdGolTarifcustomer=rowdata.KD_GOL_TARCUS;
	kdtarifgoltarif=rowdata.KD_TARIFGOL_TARCUS;
	}