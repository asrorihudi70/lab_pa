var dataSource_viGzBilling;
var selectCount_viGzBilling=50;
var NamaForm_viGzBilling="Billing";
var mod_name_viGzBilling="viGzPenerimaan";
var now_viGzBilling= new Date();
var rowSelected_viGzBilling;
var setLookUps_viGzBilling;
var tanggal = now_viGzBilling.format("Y/M/d");
var jam = now_viGzBilling.format("H/i/s");
var tmpkriteria;
var GridDataView_viGzBilling;
var tapungkd_pay;
var FilterNoMinta;
var FilterNoTransaksi;
var FilterTglAwal;
var FilterTglAkhir;
var gridDTPaid;
var tampungtypedata=0;
var grandtotal;
var tampungshiftsekarang;
var statusBayar;


var CurrentGzBilling =
{
    data: Object,
    details: Array,
    row: 0
};

var CurrentData_viGzBilling =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viGzBilling(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

var GzBilling={};
GzBilling.form={};
GzBilling.func={};
GzBilling.vars={};
GzBilling.func.parent=GzBilling;
GzBilling.form.ArrayStore={};
GzBilling.form.ComboBox={};
GzBilling.form.DataStore={};
GzBilling.form.Record={};
GzBilling.form.Form={};
GzBilling.form.Grid={};
GzBilling.form.Panel={};
GzBilling.form.TextField={};
GzBilling.form.Button={};


GzBilling.form.ArrayStore.nominta=new Ext.data.ArrayStore({
		id: 0,
		fields: [	
					'no_minta','nama','alamat'
				],
		data: []
	});

function dataGrid_viGzBilling(mod_idOrder_viGzBilling){	
    var FieldMaster_viGzBilling = 
	[
		'no_terima','no_order','kd_vendor','vendor','tgl_terima',
		'kd_petugas','petugas','keterangan'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viGzBilling = new WebApp.DataStore
	({
        fields: FieldMaster_viGzBilling
    });
    dataGriAwal();
    // Grid Gizi Perencanaan # --------------
	GridDataView_viGzBilling = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viGzBilling,
			autoScroll: true,
			columnLines: true,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viGzBilling = undefined;
							rowSelected_viGzBilling = dataSource_viGzBilling.getAt(row);
							CurrentData_viGzBilling
							CurrentData_viGzBilling.row = row;
							CurrentData_viGzBilling.data = rowSelected_viGzBilling.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viGzBilling = dataSource_viGzBilling.getAt(ridx);
					if (rowSelected_viGzBilling != undefined)
					{
						setLookUp_viGzBilling(rowSelected_viGzBilling.data);
					}
					else
					{
						setLookUp_viGzBilling();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Gizi perencanaan
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header		: 'Status Lunas',
						width		: 20,
						sortable	: false,
						hideable	: false,
						menuDisabled: true,
						align		: 'center',
						dataIndex	: 'lunas',
						id			: 'colStatusPosting_viGzBilling',
						renderer	: function(value, metaData, record, rowIndex, colIndex, store){
							 switch (value){
								 case '1':
									 metaData.css = 'StatusHijau'; 
									 break;
								 case '0':
									 metaData.css = 'StatusMerah';
									 break;
							 }
							 return '';
						}
					},
					{
						header		: 'Status Transaksi',
						width		: 20,
						sortable	: false,
						hideable	: false,
						menuDisabled: true,
						align		: 'center',
						dataIndex	: 'closed',
						id			: 'colClosed_viGzBilling',
						renderer	: function(value, metaData, record, rowIndex, colIndex, store){
							 switch (value){
								 case '1':
									 metaData.css = 'StatusHijau'; 
									 break;
								 case '0':
									 metaData.css = 'StatusMerah';
									 break;
							 }
							 return '';
						}
					},
					{
						header: 'No. Transaksi',
						dataIndex: 'no_transaksi',
						sortable: true,
						width: 35
						
					},
					//-------------- ## --------------
					{
						header: 'No. Minta',
						dataIndex: 'no_permintaan',
						sortable: true,
						width: 35
						
					},
					//-------------- ## --------------
					{
						header:'Tgl Transaksi',
						dataIndex: 'tgl_transaksi',						
						width: 30,
						sortable: true,
						hideable:false,
                        menuDisabled:true,
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.tgl_transaksi);
						}
					},
					//-------------- ## --------------
					{
						header: 'Nama Pemesan',
						dataIndex: 'nama',
						sortable: true,
						hideable:false,
                        menuDisabled:true,
						width: 30
					},
					//-------------- ## --------------
					{
						header: 'Alamat',
						dataIndex: 'alamat',
						sortable: true,
						width: 50
					}
					//-------------- ## --------------
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbarOrder_viGzBilling',
				items: 
				[
					{
						xtype: 'button',
						text: 'Tambah Pembayaran ',
						iconCls: 'Edit_Tr',
						tooltip: 'Add Data',
						id: 'btnTambahBilling_viGzBilling',
						handler: function(sm, row, rec)
						{
							setLookUp_viGzBilling();
						}
					},
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEditBilling_viGzBilling',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viGzBilling != undefined)
							{
								setLookUp_viGzBilling(rowSelected_viGzBilling.data)
							}
							
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viGzBilling, selectCount_viGzBilling, dataSource_viGzBilling),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )
	
	var pencarianGzBilling = new Ext.FormPanel({
        labelAlign: 'top',
        frame:true,
        title: '',
        bodyStyle:'padding:5px 5px 0',
        //width: 600,
        items: [
		{
			layout: 'column',
			border: false,
			items:
			[
				{
					columnWidth:.98,
					layout: 'absolute',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					width: 500,
					height: 70,
					anchor: '100% 100%',
					items:
					[
						{
							x: 10,
							y: 0,
							xtype: 'label',
							text: 'No. Transaksi'
						},
						{
							x: 120,
							y: 0,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 0,
							xtype: 'textfield',
							id: 'TxtNoTransaksinFilterGridDataView_viGzBilling',
							name: 'TxtNoTransaksinFilterGridDataView_viGzBilling',
							width: 130,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										FilterNoTransaksi=Ext.getCmp('TxtNoTransaksinFilterGridDataView_viGzBilling').getValue();
										FilterNoMinta=Ext.getCmp('txtPermintaanFilterGrid_GzBilling').getValue();
										FilterTglAwal=Ext.getCmp('dfTglAwalTransaksiGzBilling').getValue();
										FilterTglAkhir=Ext.getCmp('dfTglAkhirTransaksiGzBilling').getValue();
										FilterBilling(FilterNoTransaksi,FilterNoMinta,FilterTglAwal,FilterTglAkhir);
									} 						
								}
							}
						},
						{
							x: 10,
							y: 30,
							xtype: 'label',
							text: 'No. Permintaan'
						},
						{
							x: 120,
							y: 30,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 30,
							xtype: 'textfield',
							id: 'txtPermintaanFilterGrid_GzBilling',
							name: 'txtPermintaanFilterGrid_GzBilling',
							width: 130,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										FilterNoTransaksi=Ext.getCmp('TxtNoTransaksinFilterGridDataView_viGzBilling').getValue();
										FilterNoMinta=Ext.getCmp('txtPermintaanFilterGrid_GzBilling').getValue();
										FilterTglAwal=Ext.getCmp('dfTglAwalTransaksiGzBilling').getValue();
										FilterTglAkhir=Ext.getCmp('dfTglAkhirTransaksiGzBilling').getValue();
										FilterBilling(FilterNoTransaksi,FilterNoMinta,FilterTglAwal,FilterTglAkhir);
									} 						
								}
							}
						},
						//-------------- ## --------------
						{
							x: 310,
							y: 0,
							xtype: 'label',
							text: 'Tanggal Transaksi'
						},
						{
							x: 400,
							y: 0,
							xtype: 'label',
							text: ':'
						},
						{
							x: 410,
							y: 0,
							xtype: 'datefield',
							id: 'dfTglAwalTransaksiGzBilling',
							format: 'd/M/Y',
							width: 120,
							tabIndex:3,
							value:now_viGzBilling,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										FilterNoTransaksi=Ext.getCmp('TxtNoTransaksinFilterGridDataView_viGzBilling').getValue();
										FilterNoMinta=Ext.getCmp('txtPermintaanFilterGrid_GzBilling').getValue();
										FilterTglAwal=Ext.getCmp('dfTglAwalTransaksiGzBilling').getValue();
										FilterTglAkhir=Ext.getCmp('dfTglAkhirTransaksiGzBilling').getValue();
										FilterBilling(FilterNoTransaksi,FilterNoMinta,FilterTglAwal,FilterTglAkhir);
									} 						
								}
							}
						},
						{
							x: 540,
							y: 0,
							xtype: 'label',
							text: 's/d'
						},
						{
							x: 568,
							y: 0,
							xtype: 'datefield',
							id: 'dfTglAkhirTransaksiGzBilling',
							format: 'd/M/Y',
							width: 120,
							tabIndex:4,
							value:now_viGzBilling,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										FilterNoTransaksi=Ext.getCmp('TxtNoTransaksinFilterGridDataView_viGzBilling').getValue();
										FilterNoMinta=Ext.getCmp('txtPermintaanFilterGrid_GzBilling').getValue();
										FilterTglAwal=Ext.getCmp('dfTglAwalTransaksiGzBilling').getValue();
										FilterTglAkhir=Ext.getCmp('dfTglAkhirTransaksiGzBilling').getValue();
										FilterBilling(FilterNoTransaksi,FilterNoMinta,FilterTglAwal,FilterTglAkhir);
									} 						
								}
							}
						},
						{
							x: 310,
							y: 30,
							xtype: 'label',
							text: '*) Tekan enter untuk mencari'
						},
						
						//----------------------------------------
						{
							x: 568,
							y: 30,
							xtype: 'button',
							text: 'refresh',
							iconCls: 'refresh',
							tooltip: 'refresh',
							style:{paddingLeft:'30px'},
							width:150,
							id: 'BtnRefreshGridDataView_viGzBilling',
							handler: function() 
							{					
								dataGriAwal();
							}                        
						}
					]
				}
			]
		}
		]	
	})

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viGzBilling = new Ext.Panel
    (
		{
			title: NamaForm_viGzBilling,
			iconCls: 'Studi_Lanjut',
			id: mod_idOrder_viGzBilling,
			region: 'center',
			layout: 'form', 
			closable: true,        
			border: false,  
			margins: '0 5 5 0',
			items: [ pencarianGzBilling,
					GridDataView_viGzBilling],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viGzBilling,
		            columns: 21,
		            defaults: {
					scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viGzBilling;
    //-------------- # End form filter # --------------
}

/* function FilterBilling(kriteria)
{
    dataSource_viGzBilling.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'ViewPenerimaanDiet',
                    param : kriteria
                }			
            }
        );   
    return dataSource_viGzBilling;
}
 */
function setLookUp_viGzBilling(rowdata){
    var lebar = 985;
    setLookUps_viGzBilling = new Ext.Window({
        id: Nci.getId(),
        title: NamaForm_viGzBilling, 
        closeAction: 'destroy',        
        width: 900,
        height: 560,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viGzBilling(lebar,rowdata),
        listeners:{
            activate: function(){
				Ext.Ajax.request({
					url: baseURL + "index.php/main/getcurrentshift",
					params: {
						command: '0',
						// parameter untuk url yang dituju (fungsi didalam controller)
					},
					failure: function(o) {
						var cst = Ext.decode(o.responseText);
					},	    
					success: function(o) {
						tampungshiftsekarang=o.responseText
					}
				});
            },
            afterShow: function(){
                this.activate();
            },
            deactivate: function(){
                rowSelected_viGzBilling=undefined;
            }
        }
    });

    setLookUps_viGzBilling.show();
    if (rowdata == undefined){	
    }
    else
    {
        datainit_viGzBilling(rowdata);
    }
}

function getFormItemEntry_viGzBilling(lebar,rowdata){
    var pnlFormDataBasic_viGzBilling = new Ext.FormPanel({
		title: '',
		region: 'north',
		layout: 'form',
		bodyStyle: 'padding:10px 10px 10px 10px',
		anchor: '100%',
		labelWidth: 1,
		autoWidth: true,
		width: lebar,
		border: false,
		items:[
				getItemPanelInputData_viGzBilling(lebar),
				getItemDetailData_viGzBilling(lebar)
			],
			fileUpload: true,
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						hidden:true,
						id: 'btnAdd_viGzBilling',
						handler: function(){
							dataaddnew_viGzBilling();
						}
					},{
						xtype: 'tbseparator'
					},{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						disabled:true,
						id: 'btnSimpan_viGzBilling',
						handler: function()
						{
							datasave_viGzBilling();
						}
					},{
						xtype: 'tbseparator'
					},{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						disabled:true,
						id: 'btnSimpanExit_viGzBilling',
						handler: function()
						{
							datasave_viGzBilling();
							FilterBilling();
							setLookUps_viGzBilling.close();
						}
					},
					{
						xtype:'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Paid',
						id:'btnbayar_viGzBilling',
						iconCls: 'gantidok',
						disabled:true,
						handler: function(){
							setLookupPaidBilling();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Tutup Transaksi',
						iconCls: 'remove',
						disabled:true,
						id: 'btnTutup_viGzBilling',
						handler: function()
						{
							tutupTransaksi(Ext.getCmp('txtNoTransaksiGzBillingL').getValue());
						}
					},{
						xtype: 'tbseparator'
					},
					{
						xtype:'splitbutton',
						text:'Cetak',
						iconCls:'print',
						id:'btnPrint_viGzBilling',
						disabled:true,
						hidden:true,
						handler:function()
						{							
								
						},
						menu: new Ext.menu.Menu({
						items: [
							// these items will render as dropdown menu items when the arrow is clicked:
							{
								xtype: 'button',
								text: 'Print Bill',
								id: 'btnPrintBillGzBilling',
								handler: function()
								{
									printbill();
								}
							},
						]
						})
					}
					
				]
			}
		}
    )

    return pnlFormDataBasic_viGzBilling;
}

function getItemPanelInputData_viGzBilling(lebar) {
    var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 90,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'No. Transaksi'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 0,
								xtype: 'textfield',
								id: 'txtNoTransaksiGzBillingL',
								name: 'txtNoTransaksiGzBillingL',
								width: 130,
								readOnly:true,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									}
								}
							},
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'Tanggal Transaksi'
							},
							{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 30,
								xtype: 'datefield',
								id: 'dfTglTransaksiGzBillingL',
								format: 'd/M/Y',
								width: 150,
								tabIndex:2,
								readOnly:true,
								value:now_viGzBilling,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									}
								}
							},
							{
								x: 10,
								y: 60,
								xtype: 'label',
								text: 'No. Minta'
							},
							{
								x: 120,
								y: 60,
								xtype: 'label',
								text: ':'
							},
							GzBilling.form.ComboBox.nominta= new Nci.form.Combobox.autoComplete({
								store	: GzBilling.form.ArrayStore.nominta,
								select	: function(a,b,c){
									dsDataGrdDetailOrder_viGzBilling.removeAll();
									Ext.getCmp('txtNamaGzBillingL').setValue(b.data.nama);
									Ext.getCmp('txtAlamatGzBillingL').setValue(b.data.alamat);
									cekTransaksi(b.data.no_minta);
									
									Ext.getCmp('btnSimpan_viGzBilling').enable();
									Ext.getCmp('btnSimpanExit_viGzBilling').enable();
									
									
								},
								width	: 150,
								x: 130,
								y: 60,
								insert	: function(o){
									return {
										no_minta		:o.no_minta,
										nama			:o.nama,
										alamat			:o.alamat,
										text			:  '<table style="font-size: 11px;"><tr><td width="80">'+o.no_minta+'</td></tr></table>',
									}
								},
								url		: baseURL + "index.php/gizi/functionBilling/getNoPermintaan",
								valueField: 'no_minta',
								displayField: 'text',
								listWidth: 260
							}),
							//-------------- ## --------------
							{
								x: 320,
								y: 0,
								xtype: 'label',
								text: 'Nama Peminta'
							},
							{
								x: 410,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 420,
								y: 0,
								xtype: 'textfield',
								id: 'txtNamaGzBillingL',
								name: 'txtNamaGzBillingL',
								width: 150,
								tabIndex:1,
								readOnly:true,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									}
								}
							},
							{
								x: 320,
								y: 30,
								xtype: 'label',
								text: 'Alamat'
							},
							{
								x: 410,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							{
								x: 420,
								y: 30,
								xtype: 'textfield',
								id: 'txtAlamatGzBillingL',
								name: 'txtAlamatGzBillingL',
								readOnly:true,
								width: 280,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									}
								}
							}
							
						]
					}
				]
			}
		]		
	};
    return items;
};

function getItemDetailData_viGzBilling(lebar){
    var items ={
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'margin-top: -1px;',
		border:false,
		width: lebar-80,
		height: 375,//300, 
	    tbar:
		[
		],
		items:[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:[
					gridDataViewEdit_viGzBilling(),
					
					{
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 600,
						height: 20,
						anchor: '100% 100%',
						items:
						[
							{
								x: 580,
								y: 10,
								xtype: 'label',
								html: '<b>Total</b>'
								
							},
							{
								x: 620,
								y: 10,	
								xtype: 'textfield',
								name: 'txtDebit',
								id: 'txtDebit',
								align:'right',
								width : 120,
								tabIndex:6,
								style: 'text-align: right'
							},
							{
								x: 745,
								y: 10,	
								xtype: 'textfield',
								name: 'txtCredit',
								id: 'txtCredit',
								align:'right',
								width : 120,
								tabIndex:6,
								style: 'text-align: right'
							}
						]
					}
			
				]	
			}
		]
	};
    return items;
};


var a={};
function gridDataViewEdit_viGzBilling(){
    var FieldGrdKasir_viGzBilling = [];
    dsDataGrdDetailOrder_viGzBilling= new WebApp.DataStore({
        fields: FieldGrdKasir_viGzBilling
    });
    
    GzBilling.form.Grid.a =new Ext.grid.EditorGridPanel({
        store: dsDataGrdDetailOrder_viGzBilling,
        height: 320,
        columnLines: true,
		selModel: new Ext.grid.CellSelectionModel ({
            singleSelect: true,
            listeners:{
                rowselect: function(sm, row, rec){
                }
            }
        }),
        stripeRows: true,
        cm:new Ext.grid.ColumnModel([
        	new Ext.grid.RowNumberer(),	
			{
				dataIndex: 'kd_jenis',
				header: 'Kode',
				menuDisabled:true,
				width: 40
			},
			{			
				dataIndex: 'jenis_diet',
				header: 'Jenis Diet',
				sortable: false,
				menuDisabled:true,
				width: 180
			},
			{
				dataIndex: 'tgl_minta',
				header: 'Tanggal',
				menuDisabled:true,
				width: 80,
				sortable: false,
				renderer: function(v, params, record)
				{
				   return ShowDate(record.data.tgl_minta);

				} 
			},{
				dataIndex: 'harga',
				header: 'Harga',
				xtype:'numbercolumn',
				sortable: false,
				menuDisabled:true,
				align:'right',
				width: 60
			},{
				dataIndex: 'qty',
				header: 'Qty',
				xtype:'numbercolumn',
				sortable: false,
				menuDisabled:true,
				align:'right',
				width: 50
			},
			{
				dataIndex: 'debit',
				header: 'DR',
				xtype:'numbercolumn',
				sortable: false,
				menuDisabled:true,
				width: 60,
				align:'right'
			},
			{
				dataIndex: 'credit',
				header: 'CR',
				xtype:'numbercolumn',
				sortable: false,
				menuDisabled:true,
				width: 60,
				align:'right'
			}
        ]),
		viewConfig:{
			forceFit: true
		}
    });
    return GzBilling.form.Grid.a;
}


function setLookupPaidBilling()
{
	var lebar = 450;
    viewLookupPaid_GzBilling = new Ext.Window
    (
		{
			id: 'viewLookupPaid_GzBilling',
			name: 'viewLookupPaid_GzBilling',
			title: 'Pembayaran Permintaan Diet Umum', 
			closeAction: 'destroy',        
			width: 540,
			height: 420,
			resizable:false,
			emptyText:'Pilih Jenis Pembayaran...',
			autoScroll: false,
			border: true,
			constrain : true,    
			iconCls: 'Studi_Lanjut',
			//bodyStyle: 'padding:10px 10px 10px 10px',
			modal: true,		
			items: [ getItemPanelBiodataPembayaran_viGzBilling(lebar),
					 getItemPanelGridDetail_viGzBilling(lebar)
				   ],
			listeners:
			{
				activate: function()
				{
					
				},
				afterShow: function()
				{
					this.activate();
					
					// ;
				},
				deactivate: function()
				{
					//rowSelected_viApotekResepRWI=undefined;
				}
			},
			fbar:
			[
				{
					xtype: 'label',
					text: 'Total :'
				},
				{
					xtype: 'textfield',
					fieldLabel:'Total',
					name: 'TxtTotalBayarPaid',
					id: 'TxtTotalBayarPaid',
					style: 'text-align: right',
					width: 120,
					readOnly:true
				}
				
			]
		}
    );

    viewLookupPaid_GzBilling.show();
	
	hideCols(Ext.getCmp('idgridDTPaid'));
	showCols(Ext.getCmp('idgridDTPaid'));
	
	Ext.getCmp('TxtNoTransaksiPaid').setValue(Ext.getCmp('txtNoTransaksiGzBillingL').getValue());
	Ext.getCmp('TxtNamaPaid').setValue(Ext.getCmp('txtNamaGzBillingL').getValue());
	cekBayar(Ext.getCmp('txtNoTransaksiGzBillingL').getValue());
}

function getItemPanelBiodataPembayaran_viGzBilling(lebar) 
{ 
	var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: 527,
	    labelAlign: 'right',
	    bodyStyle: 'padding: 5px 5px 5px 5px',
	    border:true,
	    height:140,
	    items:
		[
			{
				columnWidth: .99,
				layout: 'absolute',
				bodyStyle: 'padding: 0px 0px 0px 0px',
				border: true,
				width: 170,
				height: 140,
				anchor: '100% 100%',
				items:
				[
					{
						x: 10,
						y: 10,
						xtype: 'label',
						text: 'No. Transaksi '
					},
					{
						x: 110,
						y: 10,
						xtype: 'label',
						text: ' : '
					},
					{   
						x : 120,
						y : 10,
						xtype: 'textfield',
						name: 'TxtNoTransaksiPaid',
						id: 'TxtNoTransaksiPaid',
						width: 120,
						readOnly:true
					},
					{
						x: 10,
						y: 40,
						xtype: 'label',
						text: 'Nama '
					},
					{
						x: 110,
						y: 40,
						xtype: 'label',
						text: ' : '
					},
					{ 
						x : 120,
						y : 40,
						xtype: 'textfield',
						name: 'TxtNamaPaid',
						id: 'TxtNamaPaid',
						width: 150,
						readOnly:true
					},
					{
						x: 10,
						y: 70,
						xtype: 'label',
						text: 'Pembayaran '
					},
					{
						x: 110,
						y: 70,
						xtype: 'label',
						text: ' : '
					},
					comboJenisBayarGizi(),
					comboPembayaranGizi()
				]
			},
	
		]
	};
	return items;
};



function getItemPanelGridDetail_viGzBilling(lebar) 
{ 
	 var items ={
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'margin-top: -1px;',
		border:false,
		width: 527,
		height: 217,//300, 
	    tbar:
		[
			{
				xtype: 'button',
				text: 'Paid',
				id:'btnPaidLookup_viGzBilling',
				iconCls: 'gantidok',
				handler: function(){
					paid_viGzBilling();
				}
			},
		],
		items:[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
				height: 215,
			    items:
				[
					GridDetailPaid(),
					
			
				]	
			}
		]
	};
    return items;
};
function GridDetailPaid() 
{
    var fldDetailPaid = [];
	
    dsDetailPaid = new WebApp.DataStore({ fields: fldDetailPaid })
    gridDTPaid = new Ext.grid.EditorGridPanel
    (
        {
            title: '',
			id:'idgridDTPaid',
            stripeRows: true,
            store: dsDetailPaid,
            border: true,
            columnLines: true,
            frame: false,
            autoScroll:true,
			height: 215,
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                            // cellSelecteddeskripsi = dsDetailPaid.getAt(row);
                            // CurrentHasilLab.row = row;
                            // CurrentHasilLab.data = cellSelecteddeskripsi;
                            // //FocusCtrlCMRWJ='txtAset';
                        }
                    }
                }
            ),
           cm: new Ext.grid.ColumnModel
            (
			[
				{
					header: 'Kode',
					dataIndex: 'kd_jenis',
					width:30,
					menuDisabled:true
				},
				{
					header:'Deskripsi',
					dataIndex: 'jenis_diet',
					sortable: false,
					hidden:false,
					menuDisabled:true,
					width:70
					
				},
				{
					header:'Qty',
					dataIndex: 'qty',
					sortable: false,
					align: 'right',
					hidden:false,
					menuDisabled:true,
					width:30
					
				},
				{
					header:'Harga',
					dataIndex: 'total',
					sortable: false,
					menuDisabled:true,
					xtype:'numbercolumn',
					width:50,
					align: 'right'
					
				},
				{
					header:'Piutang',
					dataIndex: 'piutang',
					sortable: false,
					menuDisabled:true,
					xtype:'numbercolumn',
					width:50,
					align: 'right',
					hidden: false,
					editor: new Ext.form.NumberField({
						allowBlank: true,
						enableKeyEvents : true,
						listeners:{
							blur: function(a){
								var line	= this.index;
								dsDetailPaid.getRange()[line].data.piutang=a.getValue();
								totalBayar();
							},
							focus: function(a){
								this.index=gridDTPaid.getSelectionModel().selection.cell[0]
							}
							
						}
					})
				},
				{
					header:'Tunai',
					dataIndex: 'tunai',
					sortable: false,
					menuDisabled:true,
					xtype:'numbercolumn',
					width:50,
					align: 'right',
					editor: new Ext.form.NumberField({
						allowBlank: true,
						enableKeyEvents : true,
						listeners:{
							blur: function(a){
								var line	= this.index;
								dsDetailPaid.getRange()[line].data.tunai=a.getValue();
								totalBayar();
							},
							focus: function(a){
								this.index=gridDTPaid.getSelectionModel().selection.cell[0]
							}
							
						}
					})
					
				},
				{
					header:'Disc',
					dataIndex: 'disc',
					width:50,
					align: 'right',
					hidden: false,
					xtype:'numbercolumn',
					sortable: false,
					menuDisabled:true,
					editor: new Ext.form.NumberField({
						allowBlank: true,
						enableKeyEvents : true,
						listeners:{
							blur: function(a){
								var line	= this.index;
								dsDetailPaid.getRange()[line].data.disc=a.getValue();
								totalBayar();
							},
							focus: function(a){
								this.index=gridDTPaid.getSelectionModel().selection.cell[0]
							}
							
						}
					})
					
				},
				{
					header:'urut',
					dataIndex: 'urut',
					width:50,
					align: 'right',
					hidden: false,
					sortable: false,
					menuDisabled:true,
					hidden:true
				},
				{
					header:'sisa',
					dataIndex: 'sisa',
					width:50,
					align: 'right',
					hidden: false,
					sortable: false,
					menuDisabled:true,
					hidden:true
				}
			]
			),
			viewConfig:{forceFit: true}
        }
		
		
    );
	
	

    return gridDTPaid;
};

function hideCols (grid)
{
	if (tampungtypedata==3) {
		grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('tunai'), true);
		grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('disc'), true);
	} else if(tampungtypedata==0) {
		grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('piutang'), true);
		grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('disc'), true);
	} else if(tampungtypedata==1) {
		grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('tunai'), true);
		grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('piutang'), true);
	}
};
function showCols (grid) 
{   
	if (tampungtypedata==3) {
		grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('piutang'), false);
	} else if(tampungtypedata==0) {
		grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('tunai'), false);
	} else if(tampungtypedata==1) {
		grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('disc'), false);
	}
};


function comboJenisBayarGizi() 
{
	var Field = ['JENIS_PAY','DESKRIPSI','TYPE_DATA'];

    dsJenisbyrGizi = new WebApp.DataStore({ fields: Field });
    dsJenisbyrGizi.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000, Sort: 'jenis_pay',
			    Sortdir: 'ASC',
			    target: 'ViewJenisPayGizi',
				param: "TYPE_DATA IN (0,1,3) AND DB_CR='0' ORDER BY Type_data"
			  
			}
		}
	);
	
    var cboJenisByrGizi = new Ext.form.ComboBox
	(
		{
			x : 120,
			y : 70,
		    id: 'cboJenisBayarGizi',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
			fieldLabel: 'Pembayaran',
		    align: 'Right',
		    width:150,
		    store: dsJenisbyrGizi,
		    valueField: 'JENIS_PAY',
		    displayField: 'DESKRIPSI',
			value:'TUNAI',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
					loaddatastorePembayaran(b.data.JENIS_PAY);
					Ext.getCmp('cbPembayaranGizi').enable();
					tampungtypedata=b.data.TYPE_DATA;
					hideCols(Ext.getCmp('idgridDTPaid'));
					showCols(Ext.getCmp('idgridDTPaid'));
					if(statusBayar == 'true'){
						getSisaAngsuranPaid(Ext.getCmp('txtNoTransaksiGzBillingL').getValue());
					} else{
						getGridDetailPaid(Ext.getCmp('txtNoTransaksiGzBillingL').getValue());
					}
					totalBayar();
				}
			}
		}
	);
	
    return cboJenisByrGizi;
};
function loaddatastorePembayaran(jenis_pay)
{
    dsComboBayarGizi.load
	(
		{
		 params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'nama',
					Sortdir: 'ASC',
					target: 'ViewComboBayar',
					param: 'jenis_pay=~'+ jenis_pay+ '~'
				}
	   }
	)      
}

function comboPembayaranGizi()
{
    var Field = ['KD_PAY','JENIS_PAY','PAYMENT'];

    dsComboBayarGizi = new WebApp.DataStore({fields: Field});
	
    var cboPembayaranGizi = new Ext.form.ComboBox
	(
		{
			x : 120,
			y : 100,
		    id: 'cbPembayaranGizi',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
            forceSelection: true,
		    emptyText:'Pilih Pembayaran...',
		    labelWidth:80,
		    align: 'Right',
		    store: dsComboBayarGizi,
		    valueField: 'KD_PAY',
		    displayField: 'PAYMENT',
			disabled:true,
			width:200,
			listeners:
			{
			    'select': function(a, b, c) 
				{
					tapungkd_pay=b.data.KD_PAY;
				}
			}
		}
	);

    return cboPembayaranGizi;
};


function datainit_viGzBilling(rowdata)
{
	if(rowdata.lunas === '0' || rowdata.lunas==0){
		Ext.getCmp('btnSimpan_viGzBilling').disable();
		Ext.getCmp('btnSimpanExit_viGzBilling').disable();
		Ext.getCmp('btnbayar_viGzBilling').enable();
		//getGridDetailDiet(rowdata.no_permintaan);
		getGridLoadDetailDietBayar(rowdata.no_transaksi);
	} else{
		Ext.getCmp('btnSimpan_viGzBilling').disable();
		Ext.getCmp('btnSimpanExit_viGzBilling').disable();
		Ext.getCmp('btnbayar_viGzBilling').disable();
		Ext.getCmp('btnTutup_viGzBilling').enable();
		getGridLoadDetailDietBayar(rowdata.no_transaksi);
		if(rowdata.closed==='1' || rowdata.closed==='1'){
			ShowPesanInfoGzBilling('Pembayaran ini sudah balance dan transaksi telah di tutup','Information');
			Ext.getCmp('btnTutup_viGzBilling').disable();
		} else{
			ShowPesanInfoGzBilling('Pembayaran ini sudah balance','Information');
			Ext.getCmp('btnTutup_viGzBilling').enable();
		}
	}
	GzBilling.form.ComboBox.nominta.setReadOnly(true);
	
	Ext.getCmp('txtNoTransaksiGzBillingL').setValue(rowdata.no_transaksi);
	Ext.getCmp('dfTglTransaksiGzBillingL').setValue(ShowDate(rowdata.tgl_transaksi));
	Ext.getCmp('txtNamaGzBillingL').setValue(rowdata.nama);
	Ext.getCmp('txtAlamatGzBillingL').setValue(rowdata.alamat);
	GzBilling.form.ComboBox.nominta.setValue(rowdata.no_permintaan);
};


function dataGriAwal(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionBilling/getDataGridAwal",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorGzBilling('Error, membaca data grid awal! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dataSource_viGzBilling.removeAll();
					var recs=[],
						recType=dataSource_viGzBilling.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
						dataSource_viGzBilling.add(recs);
					
					
					
					GridDataView_viGzBilling.getView().refresh();
				}
				else 
				{
					ShowPesanErrorGzBilling('Gagal membaca data grid awal', 'Error');
				};
			}
		}
		
	)
	
}

function FilterBilling(no_transaksi,no_minta,tgl_awal,tgl_akhir){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionBilling/getFilterGridAwal",
			params: {
				no_transaksi:no_transaksi,
				no_minta:no_minta,
				tgl_awal:tgl_awal,
				tgl_akhir:tgl_akhir
			},
			failure: function(o)
			{
				ShowPesanErrorGzBilling('Error, membaca data grid awal! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dataSource_viGzBilling.removeAll();
					var recs=[],
						recType=dataSource_viGzBilling.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
						dataSource_viGzBilling.add(recs);
					
					
					
					GridDataView_viGzBilling.getView().refresh();
				}
				else 
				{
					ShowPesanErrorGzBilling('Gagal membaca data grid awal', 'Error');
				};
			}
		}
		
	)
	
}

function cekTransaksi(no_minta){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionBilling/cekTransaksi",
			params: {no_minta:no_minta},
			failure: function(o)
			{
				ShowPesanErrorGzBilling('Error, cek transaksi! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				
				if (cst.success === true) 
				{
					getGridDetailDiet(no_minta);
				}
				else 
				{
					Ext.getCmp('btnSimpan_viGzBilling').disable();
					Ext.getCmp('btnSimpanExit_viGzBilling').disable();
					ShowPesanErrorGzBilling('No minta ini sudah melakukan transaksi, harap periksa kembali no minta', 'Error');
					
				};
			}
		}
	)
}

function cekBayar(no_transaksi){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionBilling/cekBayar",
			params: {no_transaksi:no_transaksi},
			failure: function(o)
			{
				ShowPesanErrorGzBilling('Error, cek pembayaran! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				
				if (cst.success === true) 
				{
					getGridDetailPaid(no_transaksi);
					statusBayar='false';
				}
				else 
				{
					getSisaAngsuranPaid(no_transaksi);
					statusBayar='true';
				};
			}
		}
	)
}

function getGridDetailDiet(no_minta){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionBilling/getGridDetailDiet",
			params: {no_minta:no_minta},
			failure: function(o)
			{
				ShowPesanErrorGzBilling('Error, membaca data diet! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dsDataGrdDetailOrder_viGzBilling.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
					dsDataGrdDetailOrder_viGzBilling.add(recs);
					
					GzBilling.form.Grid.a.getView().refresh();
					hasilJumlah();
				}
				else 
				{
					ShowPesanErrorGzBilling('Gagal membaca Data ini', 'Error');
				};
			}
		}
	)
}


function getGridLoadDetailDietBayar(no_transaksi){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionBilling/getGridLoadPenerimaan",
			params: {no_transaksi:no_transaksi},
			failure: function(o)
			{
				ShowPesanErrorGzBilling('Error, membaca data order! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				
				if (cst.success === true) 
				{
					dsDataGrdDetailOrder_viGzBilling.removeAll();
					var recs=[],
						recType=dsDataGrdDetailOrder_viGzBilling.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						recs.push(new recType(cst.ListDataObj[i]));
					}
					dsDataGrdDetailOrder_viGzBilling.add(recs);
					hasilJumlahLoad();
					GzBilling.form.Grid.a.getView().refresh();
				}
				else 
				{
					ShowPesanErrorGzBilling('Gagal membaca Data ini', 'Error');
				};
			}
		}
	)
}

function getGridDetailPaid(no_transaksi){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionBilling/getGridPaid",
			params: {no_transaksi:no_transaksi},
			failure: function(o)
			{
				ShowPesanErrorGzBilling('Error, membaca detail pembayaran! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				
				if (cst.success === true) 
				{
					dsDetailPaid.removeAll();
					var recs=[],
						recType=dsDetailPaid.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
					dsDetailPaid.add(recs);
					totalBayar();
					gridDTPaid.getView().refresh();
				}
				else 
				{
					ShowPesanWarningGzBilling('Gagal membaca detail pembayaran', 'Warning');
				};
			}
		}
	)
}

function getSisaAngsuranPaid(no_transaksi,tgl_transaksi){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionBilling/getSisaAngsuran",
			params: {no_transaksi:no_transaksi},
			failure: function(o)
			{
				ShowPesanErrorGzBilling('Error, membaca detail pembayaran! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				
				if (cst.success === true) 
				{
					dsDetailPaid.removeAll();
					var recs=[],
						recType=dsDetailPaid.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
					dsDetailPaid.add(recs);
					totalBayar();
					gridDTPaid.getView().refresh();
				}
				else 
				{
					ShowPesanWarningGzBilling('Gagal membaca detail pembayaran', 'Warning');
				};
			}
		}
	)
}

function datasave_viGzBilling(){
	if (ValidasiEntryGzBilling(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/gizi/functionBilling/save",
				params: getParamGzBilling(),
				failure: function(o)
				{
					ShowPesanErrorGzBilling('Error, simpan data! Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoGzBilling('Data berhasil disimpan','Information');
						Ext.getCmp('txtNoTransaksiGzBillingL').setValue(cst.notransaksi);
						dataGriAwal();
						
						Ext.getCmp('btnSimpan_viGzBilling').disable();
						Ext.getCmp('btnSimpanExit_viGzBilling').disable();
						Ext.getCmp('btnbayar_viGzBilling').enable();
					}
					else 
					{
						ShowPesanErrorGzBilling('Gagal Menyimpan Data ini. Error simpan data ini', 'Error');
					};
				}
			}
			
		)
	}
}

function paid_viGzBilling(){
	if (ValidasiPaidGzBilling(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/gizi/functionBilling/paid",
				params: getParamPaidGzBilling(),
				failure: function(o)
				{
					ShowPesanErrorGzBilling('Error, pembayaran! Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						dataGriAwal();
						if(Ext.getCmp('TxtTotalBayarPaid').getValue() >= grandtotal || cst.lunas == 'lunas'){
							ShowPesanInfoGzBilling('Pembayaran berhasil dan pembayaran telah lunas','Information');
							Ext.getCmp('btnSimpan_viGzBilling').disable();
							Ext.getCmp('btnSimpanExit_viGzBilling').disable();
							Ext.getCmp('btnbayar_viGzBilling').disable();
							Ext.getCmp('btnPaidLookup_viGzBilling').disable();
							Ext.getCmp('btnTutup_viGzBilling').enable();
							viewLookupPaid_GzBilling.close();
							getGridLoadDetailDietBayar(Ext.getCmp('txtNoTransaksiGzBillingL').getValue());
						} else{
							ShowPesanInfoGzBilling('Pembayaran berhasil disimpan','Information');
							Ext.getCmp('btnSimpan_viGzBilling').disable();
							Ext.getCmp('btnSimpanExit_viGzBilling').disable();
							Ext.getCmp('btnbayar_viGzBilling').enable();
							Ext.getCmp('btnTutup_viGzBilling').disable();
							viewLookupPaid_GzBilling.close();
							getGridLoadDetailDietBayar(Ext.getCmp('txtNoTransaksiGzBillingL').getValue());
							
						}
						
					}
					else 
					{
						ShowPesanErrorGzBilling('Gagal melakukan pembayaran. Error simpan data ini', 'Error');
					};
				}
			}
			
		)
	}
}

function tutupTransaksi(no_transaksi){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionBilling/tutupTransaksi",
			params: {no_transaksi:no_transaksi},
			failure: function(o)
			{
				ShowPesanErrorGzBilling('Error, tutup transaksi! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dataGriAwal();
					ShowPesanInfoGzBilling('Transaksi telah berhasil ditutup','Information');
					
				}
				else 
				{
					ShowPesanErrorGzBilling('Transaksi gagal ditutup', 'Error');
				};
			}
		}
		
	)
}

function printbill()
{
    Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/main/CreateDataObj",
			params: datacetakbill(),
			failure: function(o)
			{	
				ShowPesanErrorGzBilling('Error hubungi admin' ,'Error');
			},
			success: function(o)
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					
				}
				else if  (cst.success === false && cst.pesan===0)
				{
					ShowPesanWarningGzBilling('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
				}
				else
				{
					ShowPesanErrorGzBilling('Gagal print '  + 'Error');
				}
			}
		}
	)
}


function getParamGzBilling() 
{
    var params =
	{
		NoTransaksi:Ext.getCmp('txtNoTransaksiGzBillingL').getValue(),
		TglTransaksi:Ext.getCmp('dfTglTransaksiGzBillingL').getValue(),		
		NoMinta: GzBilling.form.ComboBox.nominta.getValue(),
		shift:tampungshiftsekarang
	};
	
	params['jumlah']=dsDataGrdDetailOrder_viGzBilling.getCount();
	for(var i = 0 ; i < dsDataGrdDetailOrder_viGzBilling.getCount();i++)
	{
		params['kd_jenis-'+i]=dsDataGrdDetailOrder_viGzBilling.data.items[i].data.kd_jenis;
		params['qty-'+i]=dsDataGrdDetailOrder_viGzBilling.data.items[i].data.qty;
		params['harga-'+i]=dsDataGrdDetailOrder_viGzBilling.data.items[i].data.harga;
		params['urut-'+i]= i+1;
	}
	
    return params
};

function getParamPaidGzBilling() 
{
    var params =
	{
		NoTransaksi:Ext.getCmp('TxtNoTransaksiPaid').getValue(),
		TglBayar:tanggal,		
		KdPay: Ext.getCmp('cbPembayaranGizi').getValue(),
		JumlahBayar:toInteger(Ext.getCmp('TxtTotalBayarPaid').getValue()),
		TglTransaksi:Ext.getCmp('dfTglTransaksiGzBillingL').getValue(),
		shift:tampungshiftsekarang,
		JenisBayar:tampungtypedata //jenis bayar (piutang, discount, tunai)
	};
	
	params['jumlah']=dsDetailPaid.getCount();
	for(var i = 0 ; i < dsDetailPaid.getCount();i++)
	{
		params['total-'+i]=dsDetailPaid.data.items[i].data.total;
		params['sisa-'+i]=dsDetailPaid.data.items[i].data.sisa;
		params['tunai-'+i]=dsDetailPaid.data.items[i].data.tunai;
		params['piutang-'+i]=dsDetailPaid.data.items[i].data.piutang;
		params['disc-'+i]=dsDetailPaid.data.items[i].data.disc;
		params['urut-'+i]=i+1;
	}
	
    return params
};

function datacetakbill(){
	var params =
	{
		Table: 'billprintingGzBilling',
		NoResep:Ext.getCmp('txtNoResepGzBillingL').getValue(),
		NoOut:Ext.getCmp('txtTmpNooutGzBillingL').getValue(),
		TglOut:Ext.getCmp('txtTmpTgloutGzBillingL').getValue(),
		KdPasien:Ext.getCmp('txtKdPasienGzBillingL').getValue(),
		NamaPasien:GzBilling.form.ComboBox.namaPasien.getValue(),
		JenisPasien:Ext.get('cboPilihankelompokPasienAptGzBilling').getValue(),
		Kelas:Ext.get('cbo_UnitGzBillingL').getValue(),
		Dokter:Ext.get('cbo_DokterGzBilling').getValue(),
		Total:Ext.get('txtTotalBayarGzBillingL').getValue(),
		Tot:toInteger(Ext.get('txtTotalBayarGzBillingL').getValue())
		
	}
	params['jumlah']=dsDataGrdDetailOrder_viGzBilling.getCount();
	for(var i = 0 ; i < dsDataGrdDetailOrder_viGzBilling.getCount();i++)
	{
		params['nama_obat-'+i]=dsDataGrdDetailOrder_viGzBilling.data.items[i].data.nama_obat;
		params['jml-'+i]=dsDataGrdDetailOrder_viGzBilling.data.items[i].data.jml;
	}
	
    return params
}

function getCriteriaCariGzBilling()//^^^
{
      	 var strKriteria = "";

			if (Ext.get('TxtNoTransaksinFilterGridDataView_viGzBilling').getValue() != "")
            {
                strKriteria = " upper(t.no_terima) " + "LIKE upper('" + Ext.get('TxtNoTransaksinFilterGridDataView_viGzBilling').getValue() +"%')";
            }
            
            if (Ext.get('txtPermintaanFilterGrid_GzBilling').getValue() != "")//^^^
            {
				if (strKriteria == "")
				{
					strKriteria = " lower(t.no_order) " + "LIKE lower('" + Ext.get('txtPermintaanFilterGrid_GzBilling').getValue() +"%')" ;
				}
				else {
					strKriteria += " lower(t.no_order) " + "LIKE lower('" + Ext.get('txtPermintaanFilterGrid_GzBilling').getValue() +"%')";
				}
            }
			
			if (Ext.get('dfTglAwalTransaksiGzBilling').getValue() != "" && Ext.get('dfTglAkhirTransaksiGzBilling').getValue() != "")
            {
                if (strKriteria == "")
				{
					strKriteria = " t.tgl_terima between '" + Ext.get('dfTglAwalTransaksiGzBilling').getValue() + "' and '" + Ext.get('dfTglAkhirTransaksiGzBilling').getValue() + "'" ;
				}
				else {
					strKriteria += " and t.tgl_terima between '" + Ext.get('dfTglAwalTransaksiGzBilling').getValue() + "' and '" + Ext.get('dfTglAkhirTransaksiGzBilling').getValue() + "'" ;
				}
                
            }
	
		strKriteria= strKriteria + " ORDER BY t.no_terima limit 50"
	 return strKriteria;
}


function ValidasiEntryGzBilling(modul,mBolHapus)
{
	var x = 1;
	if(GzBilling.form.ComboBox.nominta.getValue() === '' || dsDataGrdDetailOrder_viGzBilling.getCount() === 0 ){
		if(GzBilling.form.ComboBox.nominta.getValue() === ''){
			ShowPesanWarningGzBilling('No permintaan tidak boleh kosong', 'Warning');
			x = 0;
		} else {
			ShowPesanWarningGzBilling('Daftar jenis diet tidak boleh kosong', 'Warning');
			x = 0;
		} 
	}
	
	return x;
};

function ValidasiPaidGzBilling(modul,mBolHapus)
{
	var x = 1;
	if(Ext.getCmp('TxtNoTransaksiPaid').getValue() === '' || dsDataGrdDetailOrder_viGzBilling.getCount() === 0  ||  Ext.getCmp('cbPembayaranGizi').getValue() === ''){
		if(Ext.getCmp('TxtNoTransaksiPaid').getValue() === ''){
			ShowPesanWarningGzBilling('No transaksi tidak boleh kosong', 'Warning');
			x = 0;
		} else if(Ext.getCmp('cbPembayaranGizi').getValue() === ''){
			ShowPesanWarningGzBilling('Jenis pembayaran tidak boleh kosong', 'Warning');
			x = 0;
		} else{
			ShowPesanWarningGzBilling('Daftar pembayaran tidak boleh kosong', 'Warning');
			x = 0;
		}
	}
	
	return x;
};


function hasilJumlah(){
	var totDebit=0;
	
	for(var i=0; i < dsDataGrdDetailOrder_viGzBilling.getCount() ; i++){
		var debit=0;
	
		var o=dsDataGrdDetailOrder_viGzBilling.getRange()[i].data;
		if(o.qty != undefined){
			debit=parseFloat(o.qty) * parseFloat(o.harga);
			o.debit=debit;
			
			totDebit += parseFloat(debit);
			//console.log(totDebit);
			if(totDebit == NaN){
				totDebit=0;
			} else{
				totDebit=totDebit;
			}
		}
	}
	
	Ext.getCmp('txtDebit').setValue(toFormat(totDebit));
	
	GzBilling.form.Grid.a.getView().refresh();
}

function hasilJumlahLoad(){
	var totDebit=0;
	var totCredit=0;
	
	for(var i=0; i < dsDataGrdDetailOrder_viGzBilling.getCount() ; i++){
		var debit=0;
		var credit=0;
	
		var o=dsDataGrdDetailOrder_viGzBilling.getRange()[i].data;
		if(o.qty != undefined){
			debit=parseFloat(o.qty) * parseFloat(o.harga);
			
			totDebit += parseFloat(o.debit);
			totCredit += parseFloat(o.credit);
			console.log(totDebit);
			console.log(totCredit);
			if(totDebit == NaN){
				totDebit=0;
			} else{
				totDebit=totDebit;
			}
		}
	}
	Ext.getCmp('txtDebit').setValue('');
	Ext.getCmp('txtCredit').setValue('');
	
	Ext.getCmp('txtDebit').setValue(toFormat(totDebit));
	Ext.getCmp('txtCredit').setValue(toFormat(totCredit));
	
	GzBilling.form.Grid.a.getView().refresh();
}

function totalBayar(){
	var total=0;
	var grtotal=0;
	
	for(var i=0; i < dsDetailPaid.getCount() ; i++){
	
		var o=dsDetailPaid.getRange()[i].data;
		if(o.total != undefined){
			if(tampungtypedata == 0){
				total += parseFloat(o.tunai);
				//console.log(total);
			} else if(tampungtypedata == 1){
				total += parseFloat(o.disc);
			} else{
				total += parseFloat(o.piutang);
			}
			grtotal += parseFloat(o.total);
			
			
			if(total == NaN){
				total=0;
			} else{
				total=total;
			}
		}
	}
	grandtotal=grtotal;
	Ext.getCmp('TxtTotalBayarPaid').setValue(toFormat(total));
}


function ShowPesanWarningGzBilling(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorGzBilling(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:290
		}
	);
};


function ShowPesanInfoGzBilling(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};