var dataSource_viGzDistribusiUmum;
var selectCount_viGzDistribusiUmum=50;
var NamaForm_viGzDistribusiUmum="Distribusi Umum";
var selectCountStatusPostingGzDistribusiUmum='Semua';
var mod_name_viGzDistribusiUmum="viGzDistribusiUmum";
var now_viGzDistribusiUmum= new Date();
var addNew_viGzDistribusiUmum;
var rowSelected_viGzDistribusiUmum;
var setLookUps_viGzDistribusiUmum;
var mNoKunjungan_viGzDistribusiUmum='';
var selectSetUnit;
var cellSelecteddeskripsiRWI;
var tanggal = now_viGzDistribusiUmum.format("d/M/Y");
var jam = now_viGzDistribusiUmum.format("H/i/s");
var tampungshiftsekarang;
var tmpkriteria;
var gridDTLTRHistoryApotekRWI;
var GridDataView_viGzDistribusiUmum;
var FNodistribusi;
var FPetugas;
var FTglawal;
var FTglakhir;

var CurrentGzDistribusiUmum =
{
    data: Object,
    details: Array,
    row: 0
};

var CurrentData_viGzDistribusiUmum =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viGzDistribusiUmum(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

var GzDistribusiUmum={};
GzDistribusiUmum.form={};
GzDistribusiUmum.func={};
GzDistribusiUmum.vars={};
GzDistribusiUmum.func.parent=GzDistribusiUmum;
GzDistribusiUmum.form.ArrayStore={};
GzDistribusiUmum.form.ComboBox={};
GzDistribusiUmum.form.DataStore={};
GzDistribusiUmum.form.Record={};
GzDistribusiUmum.form.Form={};
GzDistribusiUmum.form.Grid={};
GzDistribusiUmum.form.Panel={};
GzDistribusiUmum.form.TextField={};
GzDistribusiUmum.form.Button={};

GzDistribusiUmum.form.ArrayStore.a	= new Ext.data.ArrayStore({
	id: 0,
	fields:['no_minta', 'nama','kd_waktu','waktu',
			'kd_jenis','jenis_diet','realisasi','qty_distribusi','qty_minta'],
	data: []
});

function dataGrid_viGzDistribusiUmum(mod_id_viGzDistribusiUmum){	
    var FieldMaster_viGzDistribusiUmum = 
	[
		'no_distribusi', 'kd_petugas', 'petugas', 'kd_waktu', 'waktu', 'tgl_distribusi','kd_unit', 'nama_unit', 'no_minta'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viGzDistribusiUmum = new WebApp.DataStore
	({
        fields: FieldMaster_viGzDistribusiUmum
    });
    dataGriAwal();
    // Grid Apotek Perencanaan # --------------
	GridDataView_viGzDistribusiUmum = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viGzDistribusiUmum,
			autoScroll: true,
			columnLines: true,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viGzDistribusiUmum = undefined;
							rowSelected_viGzDistribusiUmum = dataSource_viGzDistribusiUmum.getAt(row);
							CurrentData_viGzDistribusiUmum
							CurrentData_viGzDistribusiUmum.row = row;
							CurrentData_viGzDistribusiUmum.data = rowSelected_viGzDistribusiUmum.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viGzDistribusiUmum = dataSource_viGzDistribusiUmum.getAt(ridx);
					if (rowSelected_viGzDistribusiUmum != undefined)
					{
						setLookUp_viGzDistribusiUmum(rowSelected_viGzDistribusiUmum.data);
					}
					else
					{
						setLookUp_viGzDistribusiUmum();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Apotek perencanaan
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header: 'No. Distribusi',
						dataIndex: 'no_distribusi',
						sortable: true,
						width: 35
						
					},
					//-------------- ## --------------
					{
						header:'Tanggal',
						dataIndex: 'tgl_distribusi',						
						width: 30,
						sortable: true,
						hideable:false,
                        menuDisabled:true,
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.tgl_distribusi);
						}
					},
					//-------------- ## --------------
					{
						header: 'Waktu',
						dataIndex: 'waktu',
						sortable: true,
						width: 40
					},
					//-------------- ## --------------
					{
						header: 'Petugas Gizi',
						dataIndex: 'petugas',
						sortable: true,
						width: 40
					},
					//-------------- ## --------------
					{
						header: 'kd_petugas',
						dataIndex: 'kd_petugas',
						sortable: true,
						hidden:true,
						width: 40
					},
					{
						header: 'kd_waktu',
						dataIndex: 'kd_waktu',
						sortable: true,
						hidden:true,
						width: 40
					},
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viGzDistribusiUmum',
				items: 
				[
					{
						xtype: 'button',
						text: 'Tambah Distribusi ',
						iconCls: 'Edit_Tr',
						tooltip: 'Add Data',
						id: 'btnTambah_viGzDistribusiUmum',
						handler: function(sm, row, rec)
						{
							setLookUp_viGzDistribusiUmum();
						}
					},
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viGzDistribusiUmum',
						handler: function(sm, row, rec)
						{
							
							if (rowSelected_viGzDistribusiUmum != undefined)
							{
								setLookUp_viGzDistribusiUmum(rowSelected_viGzDistribusiUmum.data)
							}
							//setLookUp_viGzDistribusiUmum();
							
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viGzDistribusiUmum, selectCount_viGzDistribusiUmum, dataSource_viGzDistribusiUmum),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )
	
	var pencarianGzDistribusiUmum = new Ext.FormPanel({
        labelAlign: 'top',
        frame:true,
        title: '',
        bodyStyle:'padding:5px 5px 0',
        //width: 600,
        items: [
		{
			layout: 'column',
			border: false,
			items:
			[
				{
					columnWidth:.98,
					layout: 'absolute',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					width: 500,
					height: 70,
					anchor: '100% 100%',
					items:
					[
						{
							x: 10,
							y: 0,
							xtype: 'label',
							text: 'No. Distribusi'
						},
						{
							x: 120,
							y: 0,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 0,
							xtype: 'textfield',
							id: 'TxtNoDistribusiFilterGridDataView_viGzDistribusiUmum',
							name: 'TxtNoDistribusiFilterGridDataView_viGzDistribusiUmum',
							emptyText: '',
							width: 130,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										FNodistribusi=Ext.getCmp('TxtNoDistribusiFilterGridDataView_viGzDistribusiUmum').getValue();
										FPetugas=Ext.getCmp('cbo_PetugasGiziGzDistribusiUmum').getValue();
										FTglawal=Ext.getCmp('dfTglAwalGzDistribusiUmum').getValue();
										FTglakhir=Ext.getCmp('dfTglAkhirGzDistribusiUmum').getValue();
										dataGriAwalFilter(FNodistribusi,FPetugas,FTglawal,FTglakhir);
									} 						
								}
							}
						},
						{
							x: 10,
							y: 30,
							xtype: 'label',
							text: 'Petugas Gizi'
						},
						{
							x: 120,
							y: 30,
							xtype: 'label',
							text: ':'
						},
						ComboPetugasGiziGzDistribusiUmum(),
						
						{
							x: 310,
							y: 0,
							xtype: 'label',
							text: 'Tanggal'
						},
						{
							x: 400,
							y: 0,
							xtype: 'label',
							text: ':'
						},
						{
							x: 410,
							y: 0,
							xtype: 'datefield',
							id: 'dfTglAwalGzDistribusiUmum',
							format: 'd/M/Y',
							width: 120,
							tabIndex:3,
							value:now_viGzDistribusiUmum,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										FNodistribusi=Ext.getCmp('TxtNoDistribusiFilterGridDataView_viGzDistribusiUmum').getValue();
										FPetugas=Ext.getCmp('cbo_PetugasGiziGzDistribusiUmum').getValue();
										FTglawal=Ext.getCmp('dfTglAwalGzDistribusiUmum').getValue();
										FTglakhir=Ext.getCmp('dfTglAkhirGzDistribusiUmum').getValue();
										dataGriAwalFilter(FNodistribusi,FPetugas,FTglawal,FTglakhir);
									} 						
								}
							}
						},
						{
							x: 540,
							y: 0,
							xtype: 'label',
							text: 's/d'
						},
						{
							x: 568,
							y: 0,
							xtype: 'datefield',
							id: 'dfTglAkhirGzDistribusiUmum',
							format: 'd/M/Y',
							width: 120,
							tabIndex:4,
							value:now_viGzDistribusiUmum,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										FNodistribusi=Ext.getCmp('TxtNoDistribusiFilterGridDataView_viGzDistribusiUmum').getValue();
										FPetugas=Ext.getCmp('cbo_PetugasGiziGzDistribusiUmum').getValue();
										FTglawal=Ext.getCmp('dfTglAwalGzDistribusiUmum').getValue();
										FTglakhir=Ext.getCmp('dfTglAkhirGzDistribusiUmum').getValue();
										dataGriAwalFilter(FNodistribusi,FPetugas,FTglawal,FTglakhir);
									} 						
								}
							}
						},
						{
							x: 310,
							y: 30,
							xtype: 'label',
							text: '*) Tekan enter untuk mencari'
						},
						//----------------------------------------
						{
							x: 568,
							y: 30,
							xtype: 'button',
							text: 'Refresh',
							iconCls: 'refresh',
							tooltip: 'Cari',
							style:{paddingLeft:'30px'},
							//rowspan: 3,
							width:150,
							id: 'BtnFilterGridDataView_viGzDistribusiUmum',
							handler: function() 
							{					
								dataGriAwal();
							}                        
						}
					]
				}
			]
		}
		]	
						/*				
						//-------------- ## --------------
						
						//-------------- ## --------------
				]
			} */
		
		
	})

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viGzDistribusiUmum = new Ext.Panel
    (
		{
			title: NamaForm_viGzDistribusiUmum,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viGzDistribusiUmum,
			region: 'center',
			layout: 'form', 
			closable: true,        
			border: false,  
			margins: '0 5 5 0',
			items: [ pencarianGzDistribusiUmum,
					GridDataView_viGzDistribusiUmum],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viGzDistribusiUmum,
		            columns: 21,
		            defaults: {
					scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viGzDistribusiUmum;
    //-------------- # End form filter # --------------
}

function refreshDistribusiUmum(kriteria)
{
    dataSource_viGzDistribusiUmum.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'ViewDistribusiUmum',
                    param : kriteria
                }			
            }
        );   
    return dataSource_viGzDistribusiUmum;
}

function setLookUp_viGzDistribusiUmum(rowdata){
    var lebar = 985;
    setLookUps_viGzDistribusiUmum = new Ext.Window({
        id: Nci.getId(),
        title: NamaForm_viGzDistribusiUmum, 
        closeAction: 'destroy',        
        width: 900,
        height: 550,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viGzDistribusiUmum(lebar,rowdata),
        listeners:{
            activate: function(){
				
            },
            afterShow: function(){
                this.activate();
            },
            deactivate: function(){
                rowSelected_viGzDistribusiUmum=undefined;
            }
        }
    });

    setLookUps_viGzDistribusiUmum.show();
	
    if (rowdata == undefined){	
    }
    else
    {
        datainit_viGzDistribusiUmum(rowdata);
    }
}

function getFormItemEntry_viGzDistribusiUmum(lebar,rowdata){
    var pnlFormDataBasic_viGzDistribusiUmum = new Ext.FormPanel({
		title: '',
		region: 'north',
		layout: 'form',
		bodyStyle: 'padding:10px 10px 10px 10px',
		anchor: '100%',
		labelWidth: 1,
		autoWidth: true,
		width: lebar,
		border: false,
		items:[
				getItemPanelInputBiodata_viGzDistribusiUmum(lebar),
				getItemGridTransaksi_viGzDistribusiUmum(lebar)
			],
			fileUpload: true,
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						id: 'btnAdd_viGzDistribusiUmum',
						handler: function(){
							dataaddnew_viGzDistribusiUmum();
						}
					},{
						xtype: 'tbseparator'
					},{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						disabled:true,
						id: 'btnSimpan_viGzDistribusiUmum',
						handler: function()
						{
							datasave_viGzDistribusiUmum();
						}
					},{
						xtype: 'tbseparator'
					},{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						disabled:true,
						id: 'btnSimpanExit_viGzDistribusiUmum',
						handler: function()
						{
							datasave_viGzDistribusiUmum();
							refreshDistribusiUmum();
							setLookUps_viGzDistribusiUmum.close();
						}
					},{
						xtype: 'tbseparator'
					},
					{
						xtype:'splitbutton',
						text:'Cetak',
						iconCls:'print',
						id:'btnPrint_viGzDistribusiUmum',
						disabled:true,
						hidden:true,
						handler:function()
						{							
								
						},
						menu: new Ext.menu.Menu({
						items: [
							// these items will render as dropdown menu items when the arrow is clicked:
							{
								xtype: 'button',
								text: 'Print Bill',
								id: 'btnPrintBillGzDistribusiUmum',
								handler: function()
								{
									printbill();
								}
							},
						]
						})
					}
					
				]
			}//,items:
		}
    )

    return pnlFormDataBasic_viGzDistribusiUmum;
}

function getItemPanelInputBiodata_viGzDistribusiUmum(lebar) {
    var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 90,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'No. Distribusi'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 0,
								xtype: 'textfield',
								id: 'txtNoDistribusiGzDistribusiUmumL',
								name: 'txtNoDistribusiGzDistribusiUmumL',
								width: 130,
								readOnly:true,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									}
								}
							},
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'Tanggal'
							},
							{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 30,
								xtype: 'datefield',
								id: 'dfTglDistribusiGzDistribusiUmumL',
								format: 'd/M/Y',
								width: 130,
								tabIndex:2,
								readOnly:true,
								value:now_viGzDistribusiUmum,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									}
								}
							},
							{
								x: 10,
								y: 60,
								xtype: 'checkbox',
								id: 'chkUmumGzDistribusiUmumL',
								name: 'chkUmumGzDistribusiUmumL',
								disabled: false,
								autoWidth: false,		
								style: { 'margin-top': '2px' },							
								boxLabel: 'Tampilkan pasien yang sudah dilayani ',
								width: 220,
								handler: function (field, value) {
									if (value === true){
										dsDataGrdUmum_viGzDistribusiUmum.removeAll();
										getGridUmumDilayani();
									} else{
										dsDataGrdUmum_viGzDistribusiUmum.removeAll();
										getGridDetailLoad(Ext.getCmp('txtNoDistribusiGzDistribusiUmumL').getValue());
									}
								}
							}, 
							
							//-------------- ## --------------
							
							{
								x: 320,
								y: 0,
								xtype: 'label',
								text: 'Waktu'
							},
							{
								x: 410,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							ComboWaktuGzDistribusiUmumLookup(),
							{
								x: 320,
								y: 30,
								xtype: 'label',
								text: 'Petugas Gizi'
							},
							{
								x: 410,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							ComboPetugasGiziGzDistribusiUmumLookup()
							
						]
					}
				]
			}
		]		
	};
    return items;
};

function getItemGridTransaksi_viGzDistribusiUmum(lebar){
    var items ={
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'margin-top: -1px;',
		border:false,
		width: lebar-80,
		height: 375,//300, 
	    tbar:
		[
			{
				text	: 'Add Umum',
				id		: 'btnAddUmumGzDistribusiUmumL',
				disabled:true,
				tooltip	: nmLookup,
				//disabled: true,
				iconCls	: 'find',
				handler	: function(){
					var records = new Array();
					records.push(new dsDataGrdUmum_viGzDistribusiUmum.recordType());
					dsDataGrdUmum_viGzDistribusiUmum.add(records);
				}
			},{
				xtype: 'tbseparator'
			},{
				xtype: 'button',
				text: 'Delete',
				iconCls: 'remove',
				disabled:true,
				id: 'btnDelete_viGzDistribusiUmum',
				handler: function()
				{
					var line = GzDistribusiUmum.form.Grid.a.getSelectionModel().selection.cell[0];
					var o = dsDataGrdUmum_viGzDistribusiUmum.getRange()[line].data;
					if(dsDataGrdUmum_viGzDistribusiUmum.getCount()>1){
						Ext.Msg.confirm('Warning', 'Apakah data resep obat ini akan dihapus?', function(button){
							if (button == 'yes'){
								if(dsDataGrdUmum_viGzDistribusiUmum.getRange()[line].data.no_out != undefined){
									Ext.Ajax.request
									(
										{
											url: baseURL + "index.php/apotek/functionAPOTEKrwi/hapusBarisGridGzDistribusiUmum",
											params:{no_out:o.no_out, tgl_out:o.tgl_out, kd_prd:o.kd_prd, kd_milik:o.kd_milik, no_urut:o.no_urut} ,
											failure: function(o)
											{
												ShowPesanErrorGzDistribusiUmum('Hubungi Admin', 'Error');
											},	
											success: function(o) 
											{
												var cst = Ext.decode(o.responseText);
												if (cst.success === true) 
												{
													dsDataGrdUmum_viGzDistribusiUmum.removeAt(line);
													GzDistribusiUmum.form.Grid.a.getView().refresh();
													Ext.getCmp('btnSimpan_viGzDistribusiUmum').enable();
													Ext.getCmp('btnSimpanExit_viGzDistribusiUmum').enable();
													Ext.getCmp('btnBayar_viGzDistribusiUmum').disable();
													
												}
												else 
												{
													ShowPesanErrorGzDistribusiUmum('Gagal melakukan penghapusan', 'Error');
												};
											}
										}
										
									)
								}else{
									dsDataGrdUmum_viGzDistribusiUmum.removeAt(line);
									GzDistribusiUmum.form.Grid.a.getView().refresh();
									Ext.getCmp('btnSimpan_viGzDistribusiUmum').enable();
									Ext.getCmp('btnSimpanExit_viGzDistribusiUmum').enable();
								}
							} 
							
						});
					} else{
						ShowPesanErrorGzDistribusiUmum('Data tidak bisa dihapus karena minimal resep 1 obat','Error');
					}
					
				}
			}	
		],
		items:[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:[
					gridDataViewEdit_viGzDistribusiUmum()
				]	
			}
		]
	};
    return items;
};


var a={};
function gridDataViewEdit_viGzDistribusiUmum(){
	chkSelected_viGzDistribusiUmum = new Ext.grid.CheckColumn({
		id: 'chkSelected_viGzDistribusiUmum',
		header: 'Cek',
		align: 'center',						
		dataIndex: 'realisasi',			
		width: 20
	});
	
    var FieldGrdKasir_viGzDistribusiUmum = [];
    dsDataGrdUmum_viGzDistribusiUmum= new WebApp.DataStore({
        fields: FieldGrdKasir_viGzDistribusiUmum
    });
    
    GzDistribusiUmum.form.Grid.a =new Ext.grid.EditorGridPanel({
        store: dsDataGrdUmum_viGzDistribusiUmum,
        height: 340,
        columnLines: true,
		selModel: new Ext.grid.CellSelectionModel ({
            singleSelect: true,
            listeners:{
                rowselect: function(sm, row, rec){
                }
            }
        }),
        stripeRows: true,
        cm:new Ext.grid.ColumnModel([
        	new Ext.grid.RowNumberer(),	
			{
				dataIndex: 'no_minta',
				header: 'No minta',
				sortable: true,
				width: 60,
				editor:new Nci.form.Combobox.autoComplete({
					store	: GzDistribusiUmum.form.ArrayStore.a,
					select	: function(a,b,c){
						var line	= GzDistribusiUmum.form.Grid.a.getSelectionModel().selection.cell[0];
						dsDataGrdUmum_viGzDistribusiUmum.getRange()[line].data.no_minta=b.data.no_minta;
						dsDataGrdUmum_viGzDistribusiUmum.getRange()[line].data.nama=b.data.nama;
						dsDataGrdUmum_viGzDistribusiUmum.getRange()[line].data.kd_jenis=b.data.kd_jenis;
						dsDataGrdUmum_viGzDistribusiUmum.getRange()[line].data.jenis_diet=b.data.jenis_diet;
						dsDataGrdUmum_viGzDistribusiUmum.getRange()[line].data.kd_waktu=b.data.kd_waktu;
						dsDataGrdUmum_viGzDistribusiUmum.getRange()[line].data.qty_minta=b.data.qty_minta;
						dsDataGrdUmum_viGzDistribusiUmum.getRange()[line].data.qty_distribusi=b.data.qty_distribusi;
						
						if(b.data.realisasi == false || b.data.realisasi == 'f'){
							dsDataGrdUmum_viGzDistribusiUmum.getRange()[line].data.realisasi=false;
						} else{
							dsDataGrdUmum_viGzDistribusiUmum.getRange()[line].data.realisasi=b.data.realisasi;
						}
						
						GzDistribusiUmum.form.Grid.a.getView().refresh();
						
						Ext.getCmp('btnDelete_viGzDistribusiUmum').enable();
						Ext.getCmp('btnSimpan_viGzDistribusiUmum').enable();
						Ext.getCmp('btnSimpanExit_viGzDistribusiUmum').enable();
						Ext.getCmp('cbo_WaktuGzDistribusiUmum').setReadOnly(true);
						Ext.getCmp('cbo_PetugasiGzDistribusiUmumLookup').setReadOnly(true);
						
					},
					insert	: function(o){
						return {
							no_minta        : o.no_minta,
							nama			: o.nama,
							realisasi		: o.realisasi,
							kd_jenis		: o.kd_jenis,
							jenis_diet		: o.jenis_diet,
							kd_waktu		: o.kd_waktu,
							qty_minta		: o.qty_minta,
							qty_distribusi	: o.qty_distribusi,
							text			:  '<table style="font-size: 11px;"><tr><td width="100">'+o.no_minta+'</td><td width="200">'+o.nama+'</td><td width="150">'+o.jenis_diet+'</td></tr></table>'
						}
					},
					param:function(){
							return {
								kd_waktu:Ext.getCmp('cbo_WaktuGzDistribusiUmum').getValue()
							}
					},
					url		: baseURL + "index.php/gizi/functionDistribusiUmum/getAutoComGridDetail",
					valueField: 'no_minta',
					displayField: 'text',
					listWidth: 450
				})
			},
			{			
				dataIndex: 'nama',
				header: 'Nama Pemesan',
				sortable: true,
				width: 150
			},{
				dataIndex: 'jenis_diet',
				header: 'Jenis Diet',
				sortable: true,
				width: 70
			},
			{
				dataIndex: 'qty_minta',
				header: 'Qty Minta',
				align: 'right',
				width: 45,
			},{
				dataIndex: 'qty_distribusi',
				header: 'Qty Distribusi',
				sortable: true,
				align: 'right',
				width: 60,
				editor: new Ext.form.NumberField({
					allowBlank: true,
					enableKeyEvents : true,
					listeners:{
						blur: function(a){
							var line	= this.index;
							dsDataGrdUmum_viGzDistribusiUmum.getRange()[line].data.qty_distribusi=a.getValue();
							validasi();
						},
						focus: function(a){
							this.index=GzDistribusiUmum.form.Grid.a.getSelectionModel().selection.cell[0]
						}
						
					}
				})
			},
			chkSelected_viGzDistribusiUmum,
			//-------------- HIDDEN --------------
			{
				dataIndex: 'kd_jenis',
				header: 'kd_jenis',
				hidden: true,
				width: 80
			},
			{
				dataIndex: 'kd_waktu',
				header: 'kd_waktu',
				hidden: true,
				width: 80
			}
			//-------------- ## --------------
        ]),
		 plugins:chkSelected_viGzDistribusiUmum,
		viewConfig:{
			forceFit: true
		}
    });
    return GzDistribusiUmum.form.Grid.a;
}

function ComboPetugasGiziGzDistribusiUmumLookup()
{
    var Field_ahliGiziLookup = ['kd_petugas', 'petugas'];
    ds_PetugasGiziGZPermintaanLookup = new WebApp.DataStore({fields: Field_ahliGiziLookup});
    ds_PetugasGiziGZPermintaanLookup.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'kd_petugas',
					Sortdir: 'ASC',
					target: 'ComboPetugasGizi',
					param: ''
				}
		}
	);
	
    var cbo_PetugasiGzDistribusiUmumLookup = new Ext.form.ComboBox
    (
        {
			x: 420,
			y: 30,
            flex: 1,
			id: 'cbo_PetugasiGzDistribusiUmumLookup',
			valueField: 'kd_petugas',
            displayField: 'petugas',
			store: ds_PetugasGiziGZPermintaanLookup,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:150,
			tabIndex:2,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_PetugasiGzDistribusiUmumLookup;
};

function ComboPetugasGiziGzDistribusiUmum()
{
    var Field_ahliGizi = ['kd_petugas', 'petugas'];
    ds_PetugasGiziGZPermintaan = new WebApp.DataStore({fields: Field_ahliGizi});
    ds_PetugasGiziGZPermintaan.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'kd_petugas',
					Sortdir: 'ASC',
					target: 'ComboPetugasGizi',
					param: ''
				}
		}
	);
	
    var cbo_PetugasGiziGzDistribusiUmum = new Ext.form.ComboBox
    (
        {
			x: 130,
			y: 30,
            flex: 1,
			id: 'cbo_PetugasGiziGzDistribusiUmum',
			valueField: 'kd_petugas',
            displayField: 'petugas',
			store: ds_PetugasGiziGZPermintaan,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:150,
			tabIndex:2,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					FNodistribusi=Ext.getCmp('TxtNoDistribusiFilterGridDataView_viGzDistribusiUmum').getValue();
					FPetugas=Ext.getCmp('cbo_PetugasGiziGzDistribusiUmum').getValue();
					FTglawal=Ext.getCmp('dfTglAwalGzDistribusiUmum').getValue();
					FTglakhir=Ext.getCmp('dfTglAkhirGzDistribusiUmum').getValue();
					dataGriAwalFilter(FNodistribusi,FPetugas,FTglawal,FTglakhir);
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						FNodistribusi=Ext.getCmp('TxtNoDistribusiFilterGridDataView_viGzDistribusiUmum').getValue();
						FPetugas=Ext.getCmp('cbo_PetugasGiziGzDistribusiUmum').getValue();
						FTglawal=Ext.getCmp('dfTglAwalGzDistribusiUmum').getValue();
						FTglakhir=Ext.getCmp('dfTglAkhirGzDistribusiUmum').getValue();
						dataGriAwalFilter(FNodistribusi,FPetugas,FTglawal,FTglakhir);
					} 						
				}
			}
        }
    )    
    return cbo_PetugasGiziGzDistribusiUmum;
};

function ComboUnitGzDistribusiUmum(){
    var Field_Vendor = ['KD_UNIT', 'NAMA_UNIT'];
    ds_unit = new WebApp.DataStore({fields: Field_Vendor});
    ds_unit.load({
        params:{
	        Skip: 0,
	        Take: 1000,
	        Sort: 'kd_unit',
	        Sortdir: 'ASC',
	        target: 'ComboUnitApotek',
	        param: "parent='100' ORDER BY nama_unit"
        }
    });
    var cbo_UnitGzDistribusiUmum = new Ext.form.ComboBox({
			x:410,
			y:0,
            flex: 1,
			id: 'cbo_UnitGzDistribusiUmum',
			valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
			emptyText:'Unit/Ruang',
			store: ds_unit,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:150,
			tabIndex:2,
			listeners:{ 
				'select': function(a,b,c){
					selectSetUnit=b.data.displayText ;
				},
				'specialkey' : function(){
					if (Ext.EventObject.getKey() === 13) {
						tmpkriteria = getCriteriaCariGzDistribusiUmum();
						refreshDistribusiUmum(tmpkriteria);
					} 						
				}
			}
        }
    )    
    return cbo_UnitGzDistribusiUmum;
}

function ComboUnitGzDistribusiUmumLookup()
{
    var Field_Vendor = ['KD_UNIT', 'NAMA_UNIT'];
    ds_unitL = new WebApp.DataStore({fields: Field_Vendor});
    ds_unitL.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'kd_unit',
					Sortdir: 'ASC',
					target: 'ComboUnitApotek',
					param: "parent='100' ORDER BY nama_unit"
				}
		}
	);
	
    var cbo_UnitGzDistribusiUmumLookup = new Ext.form.ComboBox
    (
        {
			x: 420,
			y: 0,
			id: 'cbo_UnitGzDistribusiUmumLookup',
            fieldLabel: 'Unit',
			valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
			store: ds_unitL,
			valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:150,
			tabIndex:3,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					//selectSetUnit=b.data.valueField;
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						/* tmpkriteria = getCriteriaCariApotekResepRWJ();
						refeshRespApotekRWJ(tmpkriteria); */
					} 						
				}
			}
        }
    )    
    return cbo_UnitGzDistribusiUmumLookup;
}

function ComboWaktuGzDistribusiUmumLookup()
{
    var Field_WaktuGzDistribusiUmum = ['kd_waktu', 'waktu'];
    ds_WaktuGzDistribusiUmum = new WebApp.DataStore({fields: Field_WaktuGzDistribusiUmum});
    ds_WaktuGzDistribusiUmum.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sortdir: 'ASC',
					target: 'ComboWaktuGizi',
					param: ""
				}
		}
	);
	
    var cbo_WaktuGzDistribusiUmum = new Ext.form.ComboBox
    (
        {
			x: 420,
			y: 0,
			id: 'cbo_WaktuGzDistribusiUmum',
			valueField: 'kd_waktu',
            displayField: 'waktu',
			store: ds_WaktuGzDistribusiUmum,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:150,
			tabIndex:3,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					Ext.getCmp('btnAddUmumGzDistribusiUmumL').enable();
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_WaktuGzDistribusiUmum;
}

function datainit_viGzDistribusiUmum(rowdata)
{
    var tgl_distribusi2 = rowdata.tgl_distribusi.split(" ");
	
	Ext.getCmp('txtNoDistribusiGzDistribusiUmumL').setValue(rowdata.no_distribusi);
	Ext.getCmp('cbo_WaktuGzDistribusiUmum').setValue(rowdata.waktu);
	Ext.getCmp('cbo_PetugasiGzDistribusiUmumLookup').setValue(rowdata.petugas);
	Ext.getCmp('dfTglDistribusiGzDistribusiUmumL').setValue(tgl_distribusi2[0]);

	getGridDetailLoad(rowdata.no_distribusi);
	
	Ext.getCmp('cbo_WaktuGzDistribusiUmum').setReadOnly(true);
	Ext.getCmp('cbo_PetugasiGzDistribusiUmumLookup').setReadOnly(true);
	Ext.getCmp('dfTglDistribusiGzDistribusiUmumL').setReadOnly(true);
	
	GzDistribusiUmum.vars.kd_waktu=rowdata.kd_waktu;
};

function dataaddnew_viGzDistribusiUmum(){
	Ext.getCmp('txtNoDistribusiGzDistribusiUmumL').setValue('');
	Ext.getCmp('cbo_WaktuGzDistribusiUmum').setValue('');
	Ext.getCmp('cbo_PetugasiGzDistribusiUmumLookup').setValue('');
	Ext.getCmp('dfTglDistribusiGzDistribusiUmumL').setValue(now_viGzDistribusiUmum);
	
	Ext.getCmp('cbo_WaktuGzDistribusiUmum').setReadOnly(false);
	Ext.getCmp('cbo_PetugasiGzDistribusiUmumLookup').setReadOnly(false);
	Ext.getCmp('dfTglDistribusiGzDistribusiUmumL').setReadOnly(false);
	
	dsDataGrdUmum_viGzDistribusiUmum.removeAll();
	
	Ext.getCmp('btnSimpan_viGzDistribusiUmum').enable();
	Ext.getCmp('btnSimpanExit_viGzDistribusiUmum').enable();
	
	GzDistribusiUmum.vars.kd_waktu='';
}

function validasi(){
	for(var i=0; i < dsDataGrdUmum_viGzDistribusiUmum.getCount() ; i++){
		var o=dsDataGrdUmum_viGzDistribusiUmum.getRange()[i].data;
		if(o.qty_distribusi != undefined){
			if(o.qty_distribusi > o.qty_minta){
				ShowPesanWarningGzDistribusiUmum('Qty distribusi tidak boleh lebih dari qty minta', 'Warning');
				o.qty_distribusi=o.qty_minta;
				GzDistribusiUmum.form.Grid.a.getView().refresh();
			} else{
				o.qty_distribusi=o.qty_distribusi;
				GzDistribusiUmum.form.Grid.a.getView().refresh();
			}
		}
	}
}


function dataGriAwal(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionDistribusiUmum/getDataGridAwal",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorGzDistribusiUmum('Error, membaca data grid awal! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dataSource_viGzDistribusiUmum.removeAll();
					var recs=[],
						recType=dataSource_viGzDistribusiUmum.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
						dataSource_viGzDistribusiUmum.add(recs);
					
					
					
					GridDataView_viGzDistribusiUmum.getView().refresh();
				}
				else 
				{
					ShowPesanErrorGzDistribusiUmum('Gagal membaca data grid awal', 'Error');
				};
			}
		}
		
	)
	
}

function dataGriAwalFilter(no_distribusi,kd_petugas,tgl_awal,tgl_akhir){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionDistribusiUmum/getDataGridAwalFilter",
			params: {
				no_distribusi:no_distribusi,
				kd_petugas:kd_petugas,
				tgl_awal:tgl_awal,
				tgl_akhir:tgl_akhir
			},
			failure: function(o)
			{
				ShowPesanErrorGzDistribusiUmum('Error, membaca data grid awal! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dataSource_viGzDistribusiUmum.removeAll();
					var recs=[],
						recType=dataSource_viGzDistribusiUmum.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
						dataSource_viGzDistribusiUmum.add(recs);
					
					
					
					GridDataView_viGzDistribusiUmum.getView().refresh();
				}
				else 
				{
					ShowPesanErrorGzDistribusiUmum('Gagal membaca data grid awal', 'Error');
				};
			}
		}
		
	)
	
}

function getGridDetailLoad(no_distribusi){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionDistribusiUmum/getGridDetailLoad",
			params: {no_distribusi:no_distribusi},
			failure: function(o)
			{
				ShowPesanErrorGzDistribusiUmum('Error, membaca data pasien dilayani! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				
				if (cst.success === true) 
				{
					var recs=[],
						recType=dsDataGrdUmum_viGzDistribusiUmum.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
					dsDataGrdUmum_viGzDistribusiUmum.add(recs);
					
					GzDistribusiUmum.form.Grid.a.getView().refresh();
				}
				else 
				{
					ShowPesanWarningGzDistribusiUmum('Gagal membaca data ini', 'Warning');
				};
			}
		}
		
	)
	
}

function getGridUmumDilayani(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionDistribusiUmum/getGridPasienDilayani",
			params: {
				kd_waktu:GzDistribusiUmum.vars.kd_waktu,
				no_distribusi:Ext.getCmp('txtNoDistribusiGzDistribusiUmumL').getValue()
			},
			failure: function(o)
			{
				ShowPesanErrorGzDistribusiUmum('Error, membaca data pasien dilayani! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				
				if (cst.success === true) 
				{
					var recs=[],
						recType=dsDataGrdUmum_viGzDistribusiUmum.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
					dsDataGrdUmum_viGzDistribusiUmum.add(recs);
					
					GzDistribusiUmum.form.Grid.a.getView().refresh();
				}
				else 
				{
					ShowPesanWarningGzDistribusiUmum('Gagal membaca data ini', 'Warning');
				};
			}
		}
		
	)
	
}



function datasave_viGzDistribusiUmum(){
	if (ValidasiEntryGzDistribusiUmum(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/gizi/functionDistribusiUmum/save",
				params: getParamGzDistribusiUmum(),
				failure: function(o)
				{
					ShowPesanErrorGzDistribusiUmum('Error, simpan data! Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						dataGriAwal();
						ShowPesanInfoGzDistribusiUmum('Data berhasil disimpan','Information');
						Ext.get('txtNoDistribusiGzDistribusiUmumL').setValue(cst.nodistribusi);
					}
					else 
					{
						if(cst.error =='distribusi'){
							ShowPesanErrorgzPenerimaanOrder('Gagal Menyimpan Data ini. Error simpan distribusi', 'Error');
						} else if(cst.error =='detail'){
							ShowPesanErrorgzPenerimaanOrder('Gagal Menyimpan Data ini. Error simpan, distribusi detail', 'Error');
						} else{
							ShowPesanErrorgzPenerimaanOrder('Gagal Menyimpan Data ini. Error simpan, update', 'Error');
						}
					};
				}
			}
			
		)
	}
}

function printbill()
{
    Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/main/CreateDataObj",
			params: datacetakbill(),
			failure: function(o)
			{	
				ShowPesanErrorGzDistribusiUmum('Error hubungi admin' ,'Error');
			},
			success: function(o)
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					
				}
				else if  (cst.success === false && cst.pesan===0)
				{
					ShowPesanWarningGzDistribusiUmum('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
				}
				else
				{
					ShowPesanErrorGzDistribusiUmum('Gagal print '  + 'Error');
				}
			}
		}
	)
}


function getParamGzDistribusiUmum() 
{
    var params =
	{
		NoDistribusi:Ext.getCmp('txtNoDistribusiGzDistribusiUmumL').getValue(),
		Tanggal:Ext.getCmp('dfTglDistribusiGzDistribusiUmumL').getValue(),		
		KdWaktu:Ext.getCmp('cbo_WaktuGzDistribusiUmum').getValue(),
		KdPetugas:Ext.getCmp('cbo_PetugasiGzDistribusiUmumLookup').getValue()
	};
	
	params['jumlah']=dsDataGrdUmum_viGzDistribusiUmum.getCount();
	for(var i = 0 ; i < dsDataGrdUmum_viGzDistribusiUmum.getCount();i++)
	{
		params['no_minta-'+i]=dsDataGrdUmum_viGzDistribusiUmum.data.items[i].data.no_minta;
		params['nama-'+i]=dsDataGrdUmum_viGzDistribusiUmum.data.items[i].data.nama;
		params['kd_waktu-'+i]=dsDataGrdUmum_viGzDistribusiUmum.data.items[i].data.kd_waktu;
		params['kd_jenis-'+i]=dsDataGrdUmum_viGzDistribusiUmum.data.items[i].data.kd_jenis;
		params['qty_minta-'+i]=dsDataGrdUmum_viGzDistribusiUmum.data.items[i].data.qty_minta;
		params['qty_distribusi-'+i]=dsDataGrdUmum_viGzDistribusiUmum.data.items[i].data.qty_distribusi;
		params['realisasi-'+i]=dsDataGrdUmum_viGzDistribusiUmum.data.items[i].data.realisasi;
	}
	
    return params
};

function datacetakbill(){
	var params =
	{
		Table: 'billprintingGzDistribusiUmum',
		NoResep:Ext.getCmp('txtNoResepGzDistribusiUmumL').getValue(),
		NoOut:Ext.getCmp('txtTmpNooutGzDistribusiUmumL').getValue(),
		TglOut:Ext.getCmp('txtTmpTgloutGzDistribusiUmumL').getValue(),
		KdUmum:Ext.getCmp('txtKdUmumGzDistribusiUmumL').getValue(),
		NamaUmum:GzDistribusiUmum.form.ComboBox.namaUmum.getValue(),
		JenisUmum:Ext.get('cboPilihankelompokUmumAptGzDistribusiUmum').getValue(),
		Kelas:Ext.get('cbo_UnitGzDistribusiUmumL').getValue(),
		Dokter:Ext.get('cbo_DokterGzDistribusiUmum').getValue(),
		Total:Ext.get('txtTotalBayarGzDistribusiUmumL').getValue(),
		Tot:toInteger(Ext.get('txtTotalBayarGzDistribusiUmumL').getValue())
		
	}
	params['jumlah']=dsDataGrdUmum_viGzDistribusiUmum.getCount();
	for(var i = 0 ; i < dsDataGrdUmum_viGzDistribusiUmum.getCount();i++)
	{
		params['nama_obat-'+i]=dsDataGrdUmum_viGzDistribusiUmum.data.items[i].data.nama_obat;
		params['jml-'+i]=dsDataGrdUmum_viGzDistribusiUmum.data.items[i].data.jml;
	}
	
    return params
}

function getCriteriaCariGzDistribusiUmum()//^^^
{
      	 var strKriteria = "";

			if (Ext.get('TxtNoDistribusiFilterGridDataView_viGzDistribusiUmum').getValue() != "")
            {
                strKriteria = " upper(d.no_distribusi) " + "LIKE upper('" + Ext.get('TxtNoDistribusiFilterGridDataView_viGzDistribusiUmum').getValue() +"%')";
            }
            
            if (Ext.get('cbo_PetugasGiziGzDistribusiUmum').getValue() != "")//^^^
            {
				if (strKriteria == "")
				{
					strKriteria = " d.kd_petugas= " + "'" + Ext.getCmp('cbo_PetugasGiziGzDistribusiUmum').getValue() +"'" ;
				}
				else {
					strKriteria += " and d.kd_petugas =" + "'" + Ext.getCmp('cbo_PetugasGiziGzDistribusiUmum').getValue() +"'";
				}
            }
			
			if (Ext.get('dfTglAwalGzDistribusiUmum').getValue() != "" && Ext.get('dfTglAkhirGzDistribusiUmum').getValue() != "")
            {
                if (strKriteria == "")
				{
					strKriteria = " d.tgl_distribusi between '" + Ext.get('dfTglAwalGzDistribusiUmum').getValue() + "' and '" + Ext.get('dfTglAkhirGzDistribusiUmum').getValue() + "'" ;
				}
				else {
					strKriteria += " and d.tgl_distribusi between '" + Ext.get('dfTglAwalGzDistribusiUmum').getValue() + "' and '" + Ext.get('dfTglAkhirGzDistribusiUmum').getValue() + "'" ;
				}
                
            }
			
			if (Ext.get('cbo_UnitGzDistribusiUmum').getValue() != "" && Ext.get('cbo_UnitGzDistribusiUmum').getValue() != 'Unit/Ruang')
            {
				if (strKriteria == "")
				{
					strKriteria = " d.kd_unit ='" + Ext.getCmp('cbo_UnitGzDistribusiUmum').getValue() + "'"  ;
				}
				else {
					strKriteria += " and d.kd_unit ='" + Ext.getCmp('cbo_UnitGzDistribusiUmum').getValue() + "'";
			    }
                
            }
	
		strKriteria= strKriteria + " ORDER BY d.no_distribusi LIMIT 50"
	 return strKriteria;
}


function ValidasiEntryGzDistribusiUmum(modul,mBolHapus)
{
	var x = 1;
	if(dsDataGrdUmum_viGzDistribusiUmum.getCount() === 0){
		ShowPesanWarningGzDistribusiUmum('Daftar distribusi diet tidak boleh kosong, minimal 1 permintaan', 'Warning');
		x = 0;
	}

	for(var i=0; i<dsDataGrdUmum_viGzDistribusiUmum.getCount() ; i++){
		var o=dsDataGrdUmum_viGzDistribusiUmum.getRange()[i].data;
		if(o.realisasi == false){
			ShowPesanWarningGzDistribusiUmum('Kolom cek belum di ceklis', 'Warning');
			x = 0;
		}
		for(var j=i+1; j<dsDataGrdUmum_viGzDistribusiUmum.getCount() ; j++){
			var p=dsDataGrdUmum_viGzDistribusiUmum.getRange()[j].data;
			if(o.kd_pasien == p.kd_pasien && o.kd_jenis == p.kd_jenis && o.kd_waktu == p.kd_waktu && o.no_minta == p.no_minta){
				ShowPesanWarningGzDistribusiUmum('Pemesan dengan jenis diet dan waktu yang sama tidak boleh berulang. Hapus salah satu untuk melanjutkan', 'Warning');
				x = 0;
			}
		}
	}
	
	return x;
};

function ValidasiBayarGzDistribusiUmum(modul,mBolHapus)
{
	var x = 1;
	if(Ext.getCmp('cboJenisByrGzDistribusiUmum').getValue() === '' || Ext.getCmp('cboJenisByrGzDistribusiUmum').getValue() === 'TUNAI'){
		ShowPesanWarningGzDistribusiUmum('Pembayaran belum dipilih', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('cboPembayaranRWI').getValue() === '' || Ext.getCmp('cboPembayaranRWI').getValue() === 'Pilih Pembayaran...'){
		ShowPesanWarningGzDistribusiUmum('Jenis pembayaran belum dipilih', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('txtBayarGzDistribusiUmum_Pembayaran').getValue() === ''){
		ShowPesanWarningGzDistribusiUmum('Jumlah pembayaran belum di isi', 'Warning');
		x = 0;
	}
	
	return x;
};


function ShowPesanWarningGzDistribusiUmum(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorGzDistribusiUmum(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:290
		}
	);
};


function ShowPesanInfoGzDistribusiUmum(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};