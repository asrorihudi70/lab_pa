var dataSource_viGzMasterBentukMakananDiet;
var selectCount_viGzMasterBentukMakananDiet=50;
var NamaForm_viGzMasterBentukMakananDiet="Bentuk Makanan";
var mod_name_viGzMasterBentukMakananDiet="Bentuk Makanan";
var now_viGzMasterBentukMakananDiet= new Date();
var rowSelected_viGzMasterBentukMakananDiet;
var setLookUps_viGzMasterBentukMakananDiet;
var tanggal = now_viGzMasterBentukMakananDiet.format("d/M/Y");
var jam = now_viGzMasterBentukMakananDiet.format("H/i/s");
var tmpkriteria;
var DataGridkategoriObat;
var GridDataView_viGzMasterBentukMakananDiet;

var str_kd_bentuk_makanan;


var CurrentData_viGzMasterBentukMakananDiet =
{
	data: Object,
	details: Array,
	row: 0
};

var GzMasterBentukMakananDiet={};
GzMasterBentukMakananDiet.form={};
GzMasterBentukMakananDiet.func={};
GzMasterBentukMakananDiet.vars={};
GzMasterBentukMakananDiet.func.parent=GzMasterBentukMakananDiet;
GzMasterBentukMakananDiet.form.ArrayStore={};
GzMasterBentukMakananDiet.form.ComboBox={};
GzMasterBentukMakananDiet.form.DataStore={};
GzMasterBentukMakananDiet.form.Record={};
GzMasterBentukMakananDiet.form.Form={};
GzMasterBentukMakananDiet.form.Grid={};
GzMasterBentukMakananDiet.form.Panel={};
GzMasterBentukMakananDiet.form.TextField={};
GzMasterBentukMakananDiet.form.Button={};

GzMasterBentukMakananDiet.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_bentuk_makanan', 'nama_makanan'],
	data: []
});

CurrentPage.page = dataGrid_viGzMasterBentukMakananDiet(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dataGrid_viGzMasterBentukMakananDiet(mod_id_viGzMasterBentukMakananDiet){	
 	// Field kiriman dari Project Net.
    var FieldMaster_viGzMasterBentukMakananDiet = 
	[
		'kd_bentuk_makanan', 'nama_makanan' 
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viGzMasterBentukMakananDiet = new WebApp.DataStore
	({
        fields: FieldMaster_viGzMasterBentukMakananDiet
    });
    dataGriGzMasterBentukMakananDiet();
    // Grid gizi Perencanaan # --------------
	GridDataView_viGzMasterBentukMakananDiet = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viGzMasterBentukMakananDiet,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:true,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viGzMasterBentukMakananDiet = undefined;
							rowSelected_viGzMasterBentukMakananDiet = dataSource_viGzMasterBentukMakananDiet.getAt(row);
							CurrentData_viGzMasterBentukMakananDiet
							CurrentData_viGzMasterBentukMakananDiet.row = row;
							CurrentData_viGzMasterBentukMakananDiet.data = rowSelected_viGzMasterBentukMakananDiet.data;
							console.log(rec);
							str_kd_bentuk_makanan		= rec.data.kd_bentuk_makanan;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viGzMasterBentukMakananDiet = dataSource_viGzMasterBentukMakananDiet.getAt(ridx);
					if (rowSelected_viGzMasterBentukMakananDiet != undefined)
					{
						DataInitGzMasterBentukMakananDiet(rowSelected_viGzMasterBentukMakananDiet.data);
						//setLookUp_viGzMasterBentukMakananDiet(rowSelected_viGzMasterBentukMakananDiet.data);
					}
					else
					{
						//setLookUp_viGzMasterBentukMakananDiet();
					}
				},
				// End Function # --------------
				/* rowselect : function(sm, row, rec){
					console.log(sm);
					console.log(row);
					console.log(rec);
				} */
			},
			/**
	        *	Mengatur tampilan pada Grid Setup Unit
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colKode_viGzMasterBentukMakananDiet',
						header: 'Kode Bentuk Makanan',
						dataIndex: 'kd_bentuk_makanan',
						hideable:false,
						menuDisabled: true,
						width: 40
						
					},
					//-------------- ## --------------
					{
						id: 'colNamaUnit_viGzMasterBentukMakananDiet',
						header: 'Nama Makanan',
						dataIndex: 'nama_makanan',
						hideable:false,
						menuDisabled: true,
						width: 90
					},
					//-------------- ## --------------
					{
						id: 'colNoRO_viGzMasterBentukMakananDiet',
						header: 'Harga Pokok',
						dataIndex: 'harga_pokok',
						hideable:false,
						hidden:true,
						menuDisabled: true,
						align:'right',
						hidden : true,
						width: 70
						
					}
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viGzMasterBentukMakananDiet',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Bentuk Makanan',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viGzMasterBentukMakananDiet',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viGzMasterBentukMakananDiet != undefined)
							{
								DataInitGzMasterBentukMakananDiet(rowSelected_viGzMasterBentukMakananDiet.data);
							}
							
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viGzMasterBentukMakananDiet, selectCount_viGzMasterBentukMakananDiet, dataSource_viGzMasterBentukMakananDiet),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)
	
	
	
	var PanelTabGzMasterBentukMakananDiet = new Ext.FormPanel({
        labelAlign: 'top',
		layout:'form',
		border: false,  
		autoHeight:true,
        title: '',
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [PanelInputUnit()]
		
	})
	// Kriteria filter pada Grid # --------------
    var FrmData_viGzMasterBentukMakananDiet = new Ext.Panel
    (
		{
			title: NamaForm_viGzMasterBentukMakananDiet,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viGzMasterBentukMakananDiet,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ PanelTabGzMasterBentukMakananDiet,
					GridDataView_viGzMasterBentukMakananDiet],
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAddkategori_viGzMasterBentukMakananDiet',
						handler: function(){
							AddNewGzMasterBentukMakananDiet();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viGzMasterBentukMakananDiet',
						handler: function()
						{
							loadMask.show();
							dataSave_viGzMasterBentukMakananDiet();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viGzMasterBentukMakananDiet',
						handler: function()
						{
							Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
								if (button == 'yes'){
									loadMask.show();
									dataDelete_viGzMasterBentukMakananDiet();
								}
							});
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_viGzMasterBentukMakananDiet',
						handler: function()
						{
							dataSource_viGzMasterBentukMakananDiet.removeAll();
							dataGriGzMasterBentukMakananDiet();
						}
					},
					{
						xtype: 'tbseparator'
					}
					
				]
			}
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_viGzMasterBentukMakananDiet;
    //-------------- # End form filter # --------------
}

function PanelInputUnit(rowdata){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 55,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'Kode Bentuk Makanan',
								//width : 50,
							},
							{
								x: 180,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 190,
								y: 0,
								xtype: 'textfield',
								id: 'txtKdkategori_GzMasterBentukMakananDiet',
								name: 'txtKdkategori_GzMasterBentukMakananDiet',
								width: 100,
								allowBlank: false,
								maxLength:3,
								readOnly:true,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									},
									blur: function(a){
										if(a.getValue().length > 3){
											ShowPesanWarningGzMasterBentukMakananDiet('Kode unit tidak boleh lebih dari 3 huruf', 'Warning');
										}
									}
								}
							},
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'Nama Makanan'
							},
							{
								x: 180,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							GzMasterBentukMakananDiet.vars.NamaKategori=new Nci.form.Combobox.autoComplete({
								x: 190,
								y: 30,
								tabIndex:2,
								store	: GzMasterBentukMakananDiet.form.ArrayStore.a,
								select	: function(a,b,c){
									console.log(b);
									Ext.getCmp('txtKdkategori_GzMasterBentukMakananDiet').setValue(b.data.kd_bentuk_makanan);
									
									GridDataView_viGzMasterBentukMakananDiet.getView().refresh();
									
								},
								onShowList:function(a){
									dataSource_viGzMasterBentukMakananDiet.removeAll();
									
									var recs=[],
									recType=dataSource_viGzMasterBentukMakananDiet.recordType;
										
									for(var i=0; i<a.length; i++){
										recs.push(new recType(a[i]));
									}
									dataSource_viGzMasterBentukMakananDiet.add(recs);
									
								},
								insert	: function(o){
									return {
										kd_bentuk_makanan      	: o.kd_bentuk_makanan,
										nama_makanan 		: o.nama_makanan,
										text			:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_bentuk_makanan+'</td><td width="200">'+o.nama_makanan+'</td></tr></table>'
									}
								},
								url		: baseURL + "index.php/gizi/functionMasterBentukMakananDiet/getBentukMakananGrid",
								valueField: 'nama_makanan',
								displayField: 'nama_makanan',
								listWidth: 280
							}),
							//-----------------------------------------------------------------
							{
								x: 300,
								y: 0,
								xtype: 'label',
								hidden : true,
								text: 'Harga Pokok'
							},
							{
								x: 400,
								y: 0,
								xtype: 'label',
								hidden : true,
								text: ':'
							},
							{
								x: 410,
								y: 0,
								xtype: 'numberfield',
								id: 'txtHargaPokok_GzMasterBentukMakananDiet',
								name: 'txtHargaPokok_GzMasterBentukMakananDiet',
								width: 120,
								hidden : true,
								tabIndex:3,
								fieldStyle:'text-align:right;'
							}
							
							
						]
					}
				]
			}
		]		
	};
        return items;
}
//------------------end---------------------------------------------------------

function dataGriGzMasterBentukMakananDiet(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionMasterBentukMakananDiet/getBentukMakananGrid",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorGzMasterBentukMakananDiet('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dataSource_viGzMasterBentukMakananDiet.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dataSource_viGzMasterBentukMakananDiet.add(recs);
					
					
					
					GridDataView_viGzMasterBentukMakananDiet.getView().refresh();
				}
				else 
				{
					ShowPesanErrorGzMasterBentukMakananDiet('Gagal membaca data Bentuk Makanan', 'Error');
				};
			}
		}
		
	)
	
}

function dataSave_viGzMasterBentukMakananDiet(){
	//if (ValidasiSaveGzMasterBentukMakananDiet(nmHeaderSimpanData,false) == 1 )
//	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/gizi/functionMasterBentukMakananDiet/saveBentukMakananDiet",
				params: getParamSaveGzMasterBentukMakananDiet(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorGzMasterBentukMakananDiet('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					
					var cst = o.responseText;
					console.log(cst);
					if (cst === 'true') 
					{
						loadMask.hide();
						ShowPesanInfoGzMasterBentukMakananDiet('Berhasil menyimpan data ini','Information');
						dataSource_viGzMasterBentukMakananDiet.removeAll();
						dataGriGzMasterBentukMakananDiet();
						
					}
						else if (cst == 'exist') 
					{
						loadMask.hide();
						ShowPesanInfoGzMasterBentukMakananDiet('Data Telah Diupdate','Informasi');
						dataSource_viGzMasterBentukMakananDiet.removeAll();
						dataGriGzMasterBentukMakananDiet();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorGzMasterBentukMakananDiet('Gagal menyimpan data ini', 'Error');
						dataSource_viGzMasterBentukMakananDiet.removeAll();
						dataGriGzMasterBentukMakananDiet();
					};
				}
			}
			
		)
	//}
}

function dataDelete_viGzMasterBentukMakananDiet(){
	//if (ValidasiSaveGzMasterBentukMakananDiet(nmHeaderSimpanData,false) == 1 )
//	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/gizi/functionMasterBentukMakananDiet/deleteMasterBentukMakananDiet",
				params: getParamDeleteGzMasterBentukMakananDiet(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorGzMasterBentukMakananDiet('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoGzMasterBentukMakananDiet('Berhasil menghapus data ini','Information');
						AddNewGzMasterBentukMakananDiet()
						dataSource_viGzMasterBentukMakananDiet.removeAll();
						dataGriGzMasterBentukMakananDiet();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorGzMasterBentukMakananDiet('Gagal menghapus data ini', 'Error');
						dataSource_viGzMasterBentukMakananDiet.removeAll();
						dataGriGzMasterBentukMakananDiet();
					};
				}
			}
			
		)
	//}
}

function AddNewGzMasterBentukMakananDiet(){
	Ext.getCmp('txtKdkategori_GzMasterBentukMakananDiet').setValue('');
	GzMasterBentukMakananDiet.vars.NamaKategori.setValue('');
	Ext.getCmp('txtHargaPokok_GzMasterBentukMakananDiet').setValue('');
};

function DataInitGzMasterBentukMakananDiet(rowdata){
	Ext.getCmp('txtKdkategori_GzMasterBentukMakananDiet').setValue(rowdata.kd_bentuk_makanan);
	GzMasterBentukMakananDiet.vars.NamaKategori.setValue(rowdata.nama_makanan);
};

function getParamSaveGzMasterBentukMakananDiet(){
	var	params =
	{
		KdBentukMakanan		: Ext.getCmp('txtKdkategori_GzMasterBentukMakananDiet').getValue(),
		NamaMakanan				: GzMasterBentukMakananDiet.vars.NamaKategori.getValue()
	}
   
    return params
};

function getParamDeleteGzMasterBentukMakananDiet(){
	var	params =
	{
		kd_bentuk_makanan		: str_kd_bentuk_makanan,
	}
   
    return params
};

function ValidasiSaveGzMasterBentukMakananDiet(modul,mBolHapus){
	var x = 1;
	if(GzMasterBentukMakananDiet.vars.NamaKategori.getValue() === '' || Ext.getCmp('txtKdkategori_GzMasterBentukMakananDiet').getValue() ===''){
		if(GzMasterBentukMakananDiet.vars.NamaKategori.getValue() === ''){
			loadMask.hide();
			ShowPesanWarningGzMasterBentukMakananDiet('Nama unit masih kosong', 'Warning');
			x = 0;
		} else{
			loadMask.hide();
			ShowPesanWarningGzMasterBentukMakananDiet('Kode unit masih kosong', 'Warning');
			x = 0;
		}
		
	} 
	if(Ext.getCmp('txtKdkategori_GzMasterBentukMakananDiet').getValue().length > 3){
		ShowPesanWarningGzMasterBentukMakananDiet('Kode unit tidak boleh lebih dari 3 huruf', 'Warning');
		x = 0;
	}
	return x;
};



function ShowPesanWarningGzMasterBentukMakananDiet(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorGzMasterBentukMakananDiet(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoGzMasterBentukMakananDiet(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};