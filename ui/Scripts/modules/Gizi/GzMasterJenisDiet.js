var dataSource_viGzMasterJenisDiet;
var selectCount_viGzMasterJenisDiet=50;
var NamaForm_viGzMasterJenisDiet="Jenis Diet";
var mod_name_viGzMasterJenisDiet="Jenis Diet";
var now_viGzMasterJenisDiet= new Date();
var rowSelected_viGzMasterJenisDiet;
var setLookUps_viGzMasterJenisDiet;
var tanggal = now_viGzMasterJenisDiet.format("d/M/Y");
var jam = now_viGzMasterJenisDiet.format("H/i/s");
var tmpkriteria;
var DataGridJenisObat;
var GridDataView_viGzMasterJenisDiet;

//variabel public kode jenis diet
var strkd_jenis;

var CurrentData_viGzMasterJenisDiet =
{
	data: Object,
	details: Array,
	row: 0
};

var GzMasterJenisDiet={};
GzMasterJenisDiet.form={};
GzMasterJenisDiet.func={};
GzMasterJenisDiet.vars={};
GzMasterJenisDiet.func.parent=GzMasterJenisDiet;
GzMasterJenisDiet.form.ArrayStore={};
GzMasterJenisDiet.form.ComboBox={};
GzMasterJenisDiet.form.DataStore={};
GzMasterJenisDiet.form.Record={};
GzMasterJenisDiet.form.Form={};
GzMasterJenisDiet.form.Grid={};
GzMasterJenisDiet.form.Panel={};
GzMasterJenisDiet.form.TextField={};
GzMasterJenisDiet.form.Button={};

GzMasterJenisDiet.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_unit_far', 'nm_unit_far', 'nomor_ro_unit', 'nomorawal','nomor_out_milik'],
	data: []
});

CurrentPage.page = dataGrid_viGzMasterJenisDiet(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dataGrid_viGzMasterJenisDiet(mod_id_viGzMasterJenisDiet){	
 	// Field kiriman dari Project Net.
    var FieldMaster_viGzMasterJenisDiet = 
	[
		'kd_unit_far', 'nm_unit_far', 'nomor_ro_unit', 'nomorawal','nomor_out_milik' 
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viGzMasterJenisDiet = new WebApp.DataStore
	({
        fields: FieldMaster_viGzMasterJenisDiet
    });
    dataGriGzMasterJenisDiet();
    // Grid gizi Perencanaan # --------------
	GridDataView_viGzMasterJenisDiet = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viGzMasterJenisDiet,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:true,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viGzMasterJenisDiet = undefined;
							rowSelected_viGzMasterJenisDiet = dataSource_viGzMasterJenisDiet.getAt(row);
							CurrentData_viGzMasterJenisDiet
							CurrentData_viGzMasterJenisDiet.row = row;
							CurrentData_viGzMasterJenisDiet.data = rowSelected_viGzMasterJenisDiet.data;
							//DataInitGzMasterJenisDiet(rowSelected_viGzMasterJenisDiet.data);
							strkd_jenis = rec.data.kd_jenis;
							console.log(rec);
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viGzMasterJenisDiet = dataSource_viGzMasterJenisDiet.getAt(ridx);
					if (rowSelected_viGzMasterJenisDiet != undefined)
					{
						DataInitGzMasterJenisDiet(rowSelected_viGzMasterJenisDiet.data);
						//setLookUp_viGzMasterJenisDiet(rowSelected_viGzMasterJenisDiet.data);
					}
					else
					{
						//setLookUp_viGzMasterJenisDiet();
					}
				},
				// End Function # --------------
				/* rowselect : function(sm, row, rec){
					console.log(sm);
					console.log(row);
					console.log(rec);
				} */
			},
			/**
	        *	Mengatur tampilan pada Grid Setup Unit
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colKode_viGzMasterJenisDiet',
						header: 'Kode Jenis',
						dataIndex: 'kd_jenis',
						hideable:false,
						menuDisabled: true,
						width: 40
						
					},
					//-------------- ## --------------
					{
						id: 'colNamaUnit_viGzMasterJenisDiet',
						header: 'Jenis Diet',
						dataIndex: 'jenis_diet',
						hideable:false,
						menuDisabled: true,
						width: 90
					},
					//-------------- ## --------------
					{
						id: 'colNoRO_viGzMasterJenisDiet',
						header: 'Harga Pokok',
						dataIndex: 'harga_pokok',
						hideable:false,
						hidden:true,
						menuDisabled: true,
						align:'right',
						width: 70
						
					}
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viGzMasterJenisDiet',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Jenis',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viGzMasterJenisDiet',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viGzMasterJenisDiet != undefined)
							{
								DataInitGzMasterJenisDiet(rowSelected_viGzMasterJenisDiet.data);
							}
							
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viGzMasterJenisDiet, selectCount_viGzMasterJenisDiet, dataSource_viGzMasterJenisDiet),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)	
	var PanelTabGzMasterJenisDiet = new Ext.FormPanel({
        labelAlign: 'top',
		layout:'form',
		border: false,  
		autoHeight:true,
        title: '',
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [PanelInputUnit()]
		
	})
	// Kriteria filter pada Grid # --------------
    var FrmData_viGzMasterJenisDiet = new Ext.Panel
    (
		{
			title: NamaForm_viGzMasterJenisDiet,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viGzMasterJenisDiet,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ PanelTabGzMasterJenisDiet,
					GridDataView_viGzMasterJenisDiet],
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAddJenis_viGzMasterJenisDiet',
						handler: function(){
							AddNewGzMasterJenisDiet();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viGzMasterJenisDiet',
						handler: function()
						{
							loadMask.show();
							dataSave_viGzMasterJenisDiet();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viGzMasterJenisDiet',
						handler: function()
						{
							Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
								if (button == 'yes'){
									loadMask.show();
									dataDelete_viGzMasterJenisDiet();
								}
							});
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_viGzMasterJenisDiet',
						handler: function()
						{
							dataSource_viGzMasterJenisDiet.removeAll();
							dataGriGzMasterJenisDiet();
						}
					},
					{
						xtype: 'tbseparator'
					}
					
				]
			}
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_viGzMasterJenisDiet;
    //-------------- # End form filter # --------------
}

function PanelInputUnit(rowdata){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 55,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'Kode jenis'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 0,
								xtype: 'textfield',
								id: 'txtKdJenis_GzMasterJenisDiet',
								name: 'txtKdJenis_GzMasterJenisDiet',
								width: 100,
								allowBlank: false,
								maxLength:3,
								readOnly:true,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									},
									blur: function(a){
										if(a.getValue().length > 3){
											ShowPesanWarningGzMasterJenisDiet('Kode unit tidak boleh lebih dari 3 huruf', 'Warning');
										}
									}
								}
							},
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'Jenis diet'
							},
							{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							GzMasterJenisDiet.vars.jenisdiet=new Nci.form.Combobox.autoComplete({
								x: 130,
								y: 30,
								tabIndex:2,
								store	: GzMasterJenisDiet.form.ArrayStore.a,
								select	: function(a,b,c){
									console.log(b);
									Ext.getCmp('txtKdJenis_GzMasterJenisDiet').setValue(b.data.kd_jenis);
									
									GridDataView_viGzMasterJenisDiet.getView().refresh();
									
								},
								onShowList:function(a){
									dataSource_viGzMasterJenisDiet.removeAll();
									
									var recs=[],
									recType=dataSource_viGzMasterJenisDiet.recordType;
										
									for(var i=0; i<a.length; i++){
										recs.push(new recType(a[i]));
									}
									dataSource_viGzMasterJenisDiet.add(recs);
									
								},
								insert	: function(o){
									return {
										kd_jenis      	: o.kd_jenis,
										jenis_diet 		: o.jenis_diet,
										harga_pokok 	: o.harga_pokok,
										text			:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_jenis+'</td><td width="200">'+o.jenis_diet+'</td></tr></table>'
									}
								},
								url		: baseURL + "index.php/gizi/functionMasterJenisDiet/getJenisGrid",
								valueField: 'jenis_diet',
								displayField: 'jenis_diet',
								listWidth: 280
							}),
							//-----------------------------------------------------------------
							{
								x: 300,
								y: 0,
								xtype: 'label',
								text: 'Harga Pokok'
							},
							{
								x: 400,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 410,
								y: 0,
								xtype: 'numberfield',
								id: 'txtHargaPokok_GzMasterJenisDiet',
								name: 'txtHargaPokok_GzMasterJenisDiet',
								width: 120,
								tabIndex:3,
								fieldStyle:'text-align:right;'
							}
							
							
						]
					}
				]
			}
		]		
	};
        return items;
}
//------------------end---------------------------------------------------------

function dataGriGzMasterJenisDiet(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionMasterJenisDiet/getJenisGrid",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorGzMasterJenisDiet('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dataSource_viGzMasterJenisDiet.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dataSource_viGzMasterJenisDiet.add(recs);
					
					
					
					GridDataView_viGzMasterJenisDiet.getView().refresh();
				}
				else 
				{
					ShowPesanErrorGzMasterJenisDiet('Gagal membaca data jenis diet', 'Error');
				};
			}
		}
		
	)
	
}

function dataSave_viGzMasterJenisDiet(){
	//if (ValidasiSaveGzMasterJenisDiet(nmHeaderSimpanData,false) == 1 )
//	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/gizi/functionMasterJenisDiet/saveDiet",
				params: getParamSaveGzMasterJenisDiet(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorGzMasterJenisDiet('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					
					var cst = o.responseText;
					console.log(cst);
					if (cst === 'true') 
					{
						loadMask.hide();
						GzMasterJenisDiet.vars.jenisdiet.setValue('');
						Ext.getCmp('txtHargaPokok_GzMasterJenisDiet').setValue('');
						ShowPesanInfoGzMasterJenisDiet('Berhasil menyimpan data ini','Information');
						dataSource_viGzMasterJenisDiet.removeAll();
						dataGriGzMasterJenisDiet();
						
					}
						else if (cst == 'exist') 
					{
						loadMask.hide();
						ShowPesanInfoGzMasterJenisDiet('Data Telah Diupdate','Informasi');
						dataSource_viGzMasterJenisDiet.removeAll();
						dataGriGzMasterJenisDiet();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorGzMasterJenisDiet('Gagal menyimpan data ini', 'Error');
						dataSource_viGzMasterJenisDiet.removeAll();
						dataGriGzMasterJenisDiet();
					};
				}
			}
			
		)
	//}
}

function dataDelete_viGzMasterJenisDiet(){
	//if (ValidasiSaveGzMasterJenisDiet(nmHeaderSimpanData,false) == 1 )
//	{
		Ext.Ajax.request
		(
			{
				//url: baseURL + "index.php/gizi/functionGzMasterJenisDiet/delete",
				url: baseURL + "index.php/gizi/functionMasterJenisDiet/deleteDiet",
				params: getParamDeleteGzMasterJenisDiet(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorGzMasterJenisDiet('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoGzMasterJenisDiet('Berhasil menghapus data ini','Information');
						AddNewGzMasterJenisDiet()
						dataSource_viGzMasterJenisDiet.removeAll();
						dataGriGzMasterJenisDiet();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorGzMasterJenisDiet('Gagal menghapus data ini', 'Error');
						dataSource_viGzMasterJenisDiet.removeAll();
						dataGriGzMasterJenisDiet();
					};
				}
			}
			
		)
	//}
}

function AddNewGzMasterJenisDiet(){
	Ext.getCmp('txtKdJenis_GzMasterJenisDiet').setValue('');
	GzMasterJenisDiet.vars.jenisdiet.setValue('');
	Ext.getCmp('txtHargaPokok_GzMasterJenisDiet').setValue('');
};

function DataInitGzMasterJenisDiet(rowdata){
	Ext.getCmp('txtKdJenis_GzMasterJenisDiet').setValue(rowdata.kd_jenis);
	GzMasterJenisDiet.vars.jenisdiet.setValue(rowdata.jenis_diet);
	Ext.getCmp('txtHargaPokok_GzMasterJenisDiet').setValue(rowdata.harga_pokok);
};

function getParamSaveGzMasterJenisDiet(){
	var	params =
	{
		KdJenis:Ext.getCmp('txtKdJenis_GzMasterJenisDiet').getValue(),
		JenisDiet:GzMasterJenisDiet.vars.jenisdiet.getValue(),
		HargaPokok:Ext.getCmp('txtHargaPokok_GzMasterJenisDiet').getValue()
	}
   
    return params
};

function getParamDeleteGzMasterJenisDiet(){
	var	params =
	{
		KdUnit		: Ext.getCmp('txtKdJenis_GzMasterJenisDiet').getValue(),
		kd_jenis	: strkd_jenis,
	}
   
    return params
};

function ValidasiSaveGzMasterJenisDiet(modul,mBolHapus){
	var x = 1;
	if(GzMasterJenisDiet.vars.jenisdiet.getValue() === '' || Ext.getCmp('txtKdJenis_GzMasterJenisDiet').getValue() ===''){
		if(GzMasterJenisDiet.vars.jenisdiet.getValue() === ''){
			loadMask.hide();
			ShowPesanWarningGzMasterJenisDiet('Nama unit masih kosong', 'Warning');
			x = 0;
		} else{
			loadMask.hide();
			ShowPesanWarningGzMasterJenisDiet('Kode unit masih kosong', 'Warning');
			x = 0;
		}
		
	} 
	if(Ext.getCmp('txtKdJenis_GzMasterJenisDiet').getValue().length > 3){
		ShowPesanWarningGzMasterJenisDiet('Kode unit tidak boleh lebih dari 3 huruf', 'Warning');
		x = 0;
	}
	return x;
};



function ShowPesanWarningGzMasterJenisDiet(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorGzMasterJenisDiet(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoGzMasterJenisDiet(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};