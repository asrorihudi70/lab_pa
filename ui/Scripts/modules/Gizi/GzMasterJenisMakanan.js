var dataSource_viGzMasterJenisMakananDiet;
var selectCount_viGzMasterJenisMakananDiet=50;
var NamaForm_viGzMasterJenisMakananDiet="Jenis Makanan";
var mod_name_viGzMasterJenisMakananDiet="Jenis Makanan";
var now_viGzMasterJenisMakananDiet= new Date();
var rowSelected_viGzMasterJenisMakananDiet;
var setLookUps_viGzMasterJenisMakananDiet;
var tanggal = now_viGzMasterJenisMakananDiet.format("d/M/Y");
var jam = now_viGzMasterJenisMakananDiet.format("H/i/s");
var tmpkriteria;
var DataGridkategoriObat;
var GridDataView_viGzMasterJenisMakananDiet;

var str_kd_Jenis_makanan;


var CurrentData_viGzMasterJenisMakananDiet =
{
	data: Object,
	details: Array,
	row: 0
};

var GzMasterJenisMakananDiet={};
GzMasterJenisMakananDiet.form={};
GzMasterJenisMakananDiet.func={};
GzMasterJenisMakananDiet.vars={};
GzMasterJenisMakananDiet.func.parent=GzMasterJenisMakananDiet;
GzMasterJenisMakananDiet.form.ArrayStore={};
GzMasterJenisMakananDiet.form.ComboBox={};
GzMasterJenisMakananDiet.form.DataStore={};
GzMasterJenisMakananDiet.form.Record={};
GzMasterJenisMakananDiet.form.Form={};
GzMasterJenisMakananDiet.form.Grid={};
GzMasterJenisMakananDiet.form.Panel={};
GzMasterJenisMakananDiet.form.TextField={};
GzMasterJenisMakananDiet.form.Button={};

GzMasterJenisMakananDiet.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_Jenis_makanan', 'nama_makanan'],
	data: []
});

CurrentPage.page = dataGrid_viGzMasterJenisMakananDiet(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dataGrid_viGzMasterJenisMakananDiet(mod_id_viGzMasterJenisMakananDiet){	
 	// Field kiriman dari Project Net.
    var FieldMaster_viGzMasterJenisMakananDiet = 
	[
		'kd_jenis_makanan', 'nama_jenis_makanan' 
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viGzMasterJenisMakananDiet = new WebApp.DataStore
	({
        fields: FieldMaster_viGzMasterJenisMakananDiet
    });
    dataGriGzMasterJenisMakananDiet();
    // Grid gizi Perencanaan # --------------
	GridDataView_viGzMasterJenisMakananDiet = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viGzMasterJenisMakananDiet,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:true,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viGzMasterJenisMakananDiet = undefined;
							rowSelected_viGzMasterJenisMakananDiet = dataSource_viGzMasterJenisMakananDiet.getAt(row);
							CurrentData_viGzMasterJenisMakananDiet
							CurrentData_viGzMasterJenisMakananDiet.row = row;
							CurrentData_viGzMasterJenisMakananDiet.data = rowSelected_viGzMasterJenisMakananDiet.data;
							console.log(rec);
							str_kd_Jenis_makanan		= rec.data.kd_jenis_makanan;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viGzMasterJenisMakananDiet = dataSource_viGzMasterJenisMakananDiet.getAt(ridx);
					if (rowSelected_viGzMasterJenisMakananDiet != undefined)
					{
						DataInitGzMasterJenisMakananDiet(rowSelected_viGzMasterJenisMakananDiet.data);
						//setLookUp_viGzMasterJenisMakananDiet(rowSelected_viGzMasterJenisMakananDiet.data);
					}
					else
					{
						//setLookUp_viGzMasterJenisMakananDiet();
					}
				},
				// End Function # --------------
				/* rowselect : function(sm, row, rec){
					console.log(sm);
					console.log(row);
					console.log(rec);
				} */
			},
			/**
	        *	Mengatur tampilan pada Grid Setup Unit
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer({
						header:'No.',
						width: 40
					}),
					{
						id: 'colKode_viGzMasterJenisMakananDiet',
						header: 'Kode Jenis Makanan',
						dataIndex: 'kd_jenis_makanan',
						hideable:false,
						menuDisabled: true,
						width: 40
						
					},
					//-------------- ## --------------
					{
						id: 'colNamaUnit_viGzMasterJenisMakananDiet',
						header: 'Nama Makanan',
						dataIndex: 'nama_jenis_makanan',
						hideable:false,
						menuDisabled: true,
						width: 90
					}
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viGzMasterJenisMakananDiet',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Jenis Makanan',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viGzMasterJenisMakananDiet',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viGzMasterJenisMakananDiet != undefined)
							{
								DataInitGzMasterJenisMakananDiet(rowSelected_viGzMasterJenisMakananDiet.data);
							}
							
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viGzMasterJenisMakananDiet, selectCount_viGzMasterJenisMakananDiet, dataSource_viGzMasterJenisMakananDiet),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)
	
	
	
	var PanelTabGzMasterJenisMakananDiet = new Ext.FormPanel({
        labelAlign: 'top',
		layout:'form',
		border: false,  
		autoHeight:true,
        title: '',
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [PanelInputUnit()]
		
	})
	// Kriteria filter pada Grid # --------------
    var FrmData_viGzMasterJenisMakananDiet = new Ext.Panel
    (
		{
			title: NamaForm_viGzMasterJenisMakananDiet,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viGzMasterJenisMakananDiet,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ PanelTabGzMasterJenisMakananDiet,
					GridDataView_viGzMasterJenisMakananDiet],
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAddkategori_viGzMasterJenisMakananDiet',
						handler: function(){
							AddNewGzMasterJenisMakananDiet();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viGzMasterJenisMakananDiet',
						handler: function()
						{
							loadMask.show();
							dataSave_viGzMasterJenisMakananDiet();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viGzMasterJenisMakananDiet',
						handler: function()
						{
							Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
								if (button == 'yes'){
									loadMask.show();
									dataDelete_viGzMasterJenisMakananDiet();
								}
							});
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_viGzMasterJenisMakananDiet',
						handler: function()
						{
							dataSource_viGzMasterJenisMakananDiet.removeAll();
							dataGriGzMasterJenisMakananDiet();
						}
					},
					{
						xtype: 'tbseparator'
					}
					
				]
			}
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_viGzMasterJenisMakananDiet;
    //-------------- # End form filter # --------------
}

function PanelInputUnit(rowdata){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 55,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'Kode Jenis Makanan',
								//width : 50,
							},
							{
								x: 180,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 190,
								y: 0,
								xtype: 'textfield',
								id: 'txtKdkategori_GzMasterJenisMakananDiet',
								name: 'txtKdkategori_GzMasterJenisMakananDiet',
								width: 100,
								allowBlank: false,
								maxLength:3,
								readOnly:true,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									},
									blur: function(a){
										if(a.getValue().length > 3){
											ShowPesanWarningGzMasterJenisMakananDiet('Kode unit tidak boleh lebih dari 3 huruf', 'Warning');
										}
									}
								}
							},
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'Nama Makanan'
							},
							{
								x: 180,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							GzMasterJenisMakananDiet.vars.NamaKategori=new Nci.form.Combobox.autoComplete({
								x: 190,
								y: 30,
								tabIndex:2,
								store	: GzMasterJenisMakananDiet.form.ArrayStore.a,
								select	: function(a,b,c){
									console.log(b);
									Ext.getCmp('txtKdkategori_GzMasterJenisMakananDiet').setValue(b.data.kd_Jenis_makanan);
									
									GridDataView_viGzMasterJenisMakananDiet.getView().refresh();
									
								},
								onShowList:function(a){
									dataSource_viGzMasterJenisMakananDiet.removeAll();
									
									var recs=[],
									recType=dataSource_viGzMasterJenisMakananDiet.recordType;
										
									for(var i=0; i<a.length; i++){
										recs.push(new recType(a[i]));
									}
									dataSource_viGzMasterJenisMakananDiet.add(recs);
									
								},
								insert	: function(o){
									console.log(o);
									return {
										kd_jenis_makanan      	: o.kd_jenis_makanan,
										nama_makanan 		: o.nama_jenis_makanan,
										text			:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_jenis_makanan+'</td><td width="200">'+o.nama_jenis_makanan+'</td></tr></table>'
									}
								},
								url		: baseURL + "index.php/gizi/functionMasterJenisMakananDiet/getJenisMakananGrid",
								valueField: 'nama_makanan',
								displayField: 'nama_makanan',
								listWidth: 280
							}),
							//-----------------------------------------------------------------
							{
								x: 300,
								y: 0,
								xtype: 'label',
								hidden : true,
								text: 'Harga Pokok'
							},
							{
								x: 400,
								y: 0,
								xtype: 'label',
								hidden : true,
								text: ':'
							},
							{
								x: 410,
								y: 0,
								xtype: 'numberfield',
								id: 'txtHargaPokok_GzMasterJenisMakananDiet',
								name: 'txtHargaPokok_GzMasterJenisMakananDiet',
								width: 120,
								hidden : true,
								tabIndex:3,
								fieldStyle:'text-align:right;'
							}
							
							
						]
					}
				]
			}
		]		
	};
        return items;
}
//------------------end---------------------------------------------------------

function dataGriGzMasterJenisMakananDiet(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionMasterJenisMakananDiet/getJenisMakananGrid",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorGzMasterJenisMakananDiet('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dataSource_viGzMasterJenisMakananDiet.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dataSource_viGzMasterJenisMakananDiet.add(recs);
					
					
					
					GridDataView_viGzMasterJenisMakananDiet.getView().refresh();
				}
				else 
				{
					ShowPesanErrorGzMasterJenisMakananDiet('Gagal membaca data Jenis Makanan', 'Error');
				};
			}
		}
		
	)
	
}

function dataSave_viGzMasterJenisMakananDiet(){
	//if (ValidasiSaveGzMasterJenisMakananDiet(nmHeaderSimpanData,false) == 1 )
//	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/gizi/functionMasterJenisMakananDiet/saveJenisMakananDiet",
				params: getParamSaveGzMasterJenisMakananDiet(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorGzMasterJenisMakananDiet('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					
					var cst = o.responseText;
					console.log(cst);
					if (cst === 'true') 
					{
						loadMask.hide();
						ShowPesanInfoGzMasterJenisMakananDiet('Berhasil menyimpan data ini','Information');
						dataSource_viGzMasterJenisMakananDiet.removeAll();
						dataGriGzMasterJenisMakananDiet();
						
					}
						else if (cst == 'exist') 
					{
						loadMask.hide();
						ShowPesanInfoGzMasterJenisMakananDiet('Data Telah Diupdate','Informasi');
						dataSource_viGzMasterJenisMakananDiet.removeAll();
						dataGriGzMasterJenisMakananDiet();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorGzMasterJenisMakananDiet('Gagal menyimpan data ini', 'Error');
						dataSource_viGzMasterJenisMakananDiet.removeAll();
						dataGriGzMasterJenisMakananDiet();
					};
				}
			}
			
		)
	//}
}

function dataDelete_viGzMasterJenisMakananDiet(){
	//if (ValidasiSaveGzMasterJenisMakananDiet(nmHeaderSimpanData,false) == 1 )
//	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/gizi/functionMasterJenisMakananDiet/deleteMasterJenisMakananDiet",
				params: getParamDeleteGzMasterJenisMakananDiet(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorGzMasterJenisMakananDiet('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoGzMasterJenisMakananDiet('Berhasil menghapus data ini','Information');
						AddNewGzMasterJenisMakananDiet()
						dataSource_viGzMasterJenisMakananDiet.removeAll();
						dataGriGzMasterJenisMakananDiet();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorGzMasterJenisMakananDiet('Gagal menghapus data ini', 'Error');
						dataSource_viGzMasterJenisMakananDiet.removeAll();
						dataGriGzMasterJenisMakananDiet();
					};
				}
			}
			
		)
	//}
}

function AddNewGzMasterJenisMakananDiet(){
	Ext.getCmp('txtKdkategori_GzMasterJenisMakananDiet').setValue('');
	GzMasterJenisMakananDiet.vars.NamaKategori.setValue('');
	Ext.getCmp('txtHargaPokok_GzMasterJenisMakananDiet').setValue('');
};

function DataInitGzMasterJenisMakananDiet(rowdata){
	Ext.getCmp('txtKdkategori_GzMasterJenisMakananDiet').setValue(rowdata.kd_jenis_makanan);
	GzMasterJenisMakananDiet.vars.NamaKategori.setValue(rowdata.nama_jenis_makanan);
};

function getParamSaveGzMasterJenisMakananDiet(){
	var	params =
	{
		KdJenisMakanan		: Ext.getCmp('txtKdkategori_GzMasterJenisMakananDiet').getValue(),
		NamaJenisMakanan				: GzMasterJenisMakananDiet.vars.NamaKategori.getValue()
	}
   
    return params
};

function getParamDeleteGzMasterJenisMakananDiet(){
	var	params =
	{
		kd_jenis_makanan		: str_kd_Jenis_makanan,
	}
   
    return params
};

function ValidasiSaveGzMasterJenisMakananDiet(modul,mBolHapus){
	var x = 1;
	if(GzMasterJenisMakananDiet.vars.NamaKategori.getValue() === '' || Ext.getCmp('txtKdkategori_GzMasterJenisMakananDiet').getValue() ===''){
		if(GzMasterJenisMakananDiet.vars.NamaKategori.getValue() === ''){
			loadMask.hide();
			ShowPesanWarningGzMasterJenisMakananDiet('Nama unit masih kosong', 'Warning');
			x = 0;
		} else{
			loadMask.hide();
			ShowPesanWarningGzMasterJenisMakananDiet('Kode unit masih kosong', 'Warning');
			x = 0;
		}
		
	} 
	if(Ext.getCmp('txtKdkategori_GzMasterJenisMakananDiet').getValue().length > 3){
		ShowPesanWarningGzMasterJenisMakananDiet('Kode unit tidak boleh lebih dari 3 huruf', 'Warning');
		x = 0;
	}
	return x;
};



function ShowPesanWarningGzMasterJenisMakananDiet(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorGzMasterJenisMakananDiet(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoGzMasterJenisMakananDiet(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};