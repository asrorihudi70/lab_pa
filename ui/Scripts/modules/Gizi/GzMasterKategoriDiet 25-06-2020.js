var dataSource_viGzKategoriDiet;
var selectCount_viGzKategoriDiet=50;
var NamaForm_viGzKategoriDiet="Setup Kategori Diet";
var mod_name_viGzKategoriDiet="Setup Kategori Diet";
var now_viGzKategoriDiet= new Date();
var rowSelected_viGzKategoriDiet;
var setLookUps_viGzKategoriDiet;
var tanggal = now_viGzKategoriDiet.format("d/M/Y");
var jam = now_viGzKategoriDiet.format("H/i/s");
var tmpkriteria;
var DataGridJenisObat;
var GridDataView_viGzKategoriDiet;
var CurrentSelectItem;
var CurrentKdKategori;
var CurrentKdTest;
var CurrentKdKategoriSave;
var CurrentKdTestbSave;
var CurrentSelectAutoComGrid;
var CurrentArrayStoreAutoComGridItemPemeriksaan;
var CurrentArrayStoreAutoComGridMetode;


var CurrentData_viGzKategoriDiet =
{
	data: Object,
	details: Array,
	row: 0
};


CurrentArrayStoreAutoComGridItemPemeriksaan= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_kategori', 'kd_jenis','jenis_diet','harga_pokok'],
	data: []
});

CurrentArrayStoreAutoComGridMetode= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_kategori', 'kd_jenis','jenis_diet','harga_pokok'],
	data: []
});

CurrentPage.page = dataGrid_viGzKategoriDiet(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
loadDataComboItemPemeriksaan_viGzKategoriDiet();

function dataGrid_viGzKategoriDiet(mod_id_viGzKategoriDiet){	
 	// Field kiriman dari Project Net.
    var FieldMaster_viGzKategoriDiet = 
	[
		'kd_kategori', 'kd_jenis','jenis_diet','harga_pokok'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viGzKategoriDiet = new WebApp.DataStore
	({
        fields: FieldMaster_viGzKategoriDiet
    });
	
    // Grid lab Perencanaan # --------------
	GridDataView_viGzKategoriDiet = new Ext.grid.EditorGridPanel
    (
		{
			title: 'Kategori Diet',
			store: dataSource_viGzKategoriDiet,
			autoScroll: false,
			columnLines: true,
			stripeRows: true,
			flex:1,
			frame: true,
			border:true,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel:  new Ext.grid.CellSelectionModel
			(
				{
					// Tanda aktif saat salah satu baris dipilih # --------------
					singleSelect: true,
					listeners:
					{
						cellselect: function(sm, row, rec)
						{
							rowSelected_viGzKategoriDiet = undefined;
							rowSelected_viGzKategoriDiet = dataSource_viGzKategoriDiet.getAt(row);
							CurrentData_viGzKategoriDiet.row = row;
							CurrentData_viGzKategoriDiet.data = rowSelected_viGzKategoriDiet.data;
							CurrentSelectItem=CurrentData_viGzKategoriDiet.data;
							console.log(rowSelected_viGzKategoriDiet);
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viGzKategoriDiet = dataSource_viGzKategoriDiet.getAt(ridx);
					if (rowSelected_viGzKategoriDiet != undefined)
					{
						
					}
					else
					{
						
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Setup Kategori Diet
	        *	Terdiri dari : Judul, Isi dan Event
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					//-------------- ## --------------
					{
						header: 'Kode Kategori',
						dataIndex: 'kd_kategori',
						hideable:false,
						menuDisabled: true,
						hidden: false,
						width: 20
						
					},
					//-------------- ## --------------
					{
						header: 'Kode Jenis',
						dataIndex: 'kd_jenis',
						hideable:false,
						menuDisabled: true,
						hidden: false,
						width: 20,
						// editor: new Ext.form.NumberField({
						// 	allowBlank: false,
						// })
						
					},
					//-------------- ## --------------
					{
						header: 'Jenis Diet',
						dataIndex: 'jenis_diet',
						hideable:false,
						menuDisabled: true,
						width: 100,
						editor		:new Nci.form.Combobox.autoCompleteId({
							//id		: '',
							store	: CurrentArrayStoreAutoComGridItemPemeriksaan,
							select	: function(a,b,c){
								var line	= GridDataView_viGzKategoriDiet.getSelectionModel().selection.cell[0];
								console.log(b);
								//console.log(dataSource_viGzKategoriDiet.getRange()[line].data);
								dataSource_viGzKategoriDiet.getRange()[line].data.kd_jenis = b.data.kd_jenis;
								dataSource_viGzKategoriDiet.getRange()[line].data.jenis_diet = b.data.jenis_diet;
								dataSource_viGzKategoriDiet.getRange()[line].data.harga_pokok = b.data.harga_pokok;
								GridDataView_viGzKategoriDiet.getView().refresh();
								
							},
							insert	: function(o){
								return {
									kd_jenis    : o.kd_jenis,
									jenis_diet 	: o.jenis_diet,
									harga_pokok	: o.harga_pokok,
									text		:  '<table style="font-size: 11px;"><tr><td width="50">'+o.kd_jenis+'</td><td width="200">'+o.jenis_diet+'</td></tr></table>'
								}
							},
							/* param:function(){
									return {
										kdcustomer:Ext.getCmp('txtTmpKdCustomer').getValue()
									}
							}, */
							url		: baseURL + "index.php/gizi/functionSetupKategoriDiet/getItemPemeriksaan",
							valueField: 'jenis_diet',
							displayField: 'text',
							listWidth: 250
						})
					},
					//-------------- ## --------------
					{
						header: 'Harga Pokok',
						dataIndex: 'harga_pokok',
						hideable:false,
						menuDisabled: true,
						width: 70
					},
					//-------------- ## --------------
					{
						header: 'Satuan',
						dataIndex: 'satuan',
						hideable:true,
						hidden: true,
						menuDisabled: true,
						width: 70,
						editor: new Ext.form.TextField({
							allowBlank: false,
						})
					},
					//-------------- ## --------------
					{
						header: 'Metode',
						dataIndex: 'metode',
						hideable:false,
						hidden : true,
						menuDisabled: true,
						width: 70,
						editor: new Nci.form.Combobox.autoCompleteId({
							//id		: '',
							store	: CurrentArrayStoreAutoComGridMetode,
							select	: function(a,b,c){
								var line	= GridDataView_viGzKategoriDiet.getSelectionModel().selection.cell[0];
								dataSource_viGzKategoriDiet.getRange()[line].data.kd_metode=b.data.kd_metode;
								dataSource_viGzKategoriDiet.getRange()[line].data.metode=b.data.metode;
								GridDataView_viGzKategoriDiet.getView().refresh();
								
							},
							insert	: function(o){
								return {
									kd_metode   : o.kd_metode,
									metode 		: o.metode,
									text		:  '<table style="font-size: 11px;"><tr><td width="200">'+o.metode+'</td></tr></table>'
								}
							},
							/* param:function(){
									return {
										kdcustomer:Ext.getCmp('txtTmpKdCustomer').getValue()
									}
							}, */
							url		: baseURL + "index.php/lab/functionSetupItemTest/getMetode",
							valueField: 'metode',
							displayField: 'text',
							listWidth: 250
						})
					},
					//-------------- ## --------------
					{
						header: 'Urut',
						dataIndex: 'urut',
						hideable:false,
						menuDisabled: true,
						align:'right',
						width: 30,
						hidden : true,
						editor: new Ext.form.NumberField({
							allowBlank: false,
						})
					}
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viGzKategoriDiet',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add Item',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						disabled:true,
						id: 'btnAddItem_viGzKategoriDiet',
						handler: function(sm, row, rec)
						{
							var records = new Array();
							records.push(new dataSource_viGzKategoriDiet.recordType());
							dataSource_viGzKategoriDiet.add(records);
							
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						disabled:true,
						id: 'btnSimpan_viGzKategoriDiet',
						handler: function()
						{
							loadMask.show();
							dataSaveItem_viGzKategoriDiet();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						disabled:true,
						id: 'btnDelete_viGzKategoriDiet',
						handler: function()
						{
							Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
								if (button == 'yes'){
									loadMask.show();
									dataDelete_viGzKategoriDiet();
								}
							});
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						disabled:true,
						id: 'btnRefresh_viSetupPabrik',
						handler: function()
						{
							dataSource_viGzKategoriDiet.removeAll();
							dataGridviGzKategoriDiet(CurrentKdKategori);
						}
					},
					{
						xtype: 'tbseparator'
					}
					//-------------- ## --------------
				]
			},
			viewConfig: 
			{
				forceFit: true
			}
		}
	)	
	
	var PanelTab_viGzKategoriDiet = new Ext.FormPanel({
        labelAlign: 'top',
		layout:'form',
		border: false,  
		autoHeight:true,
        title: '',
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [PanelSearchPemeriksaan()]
		
	})
	
	
	// Kriteria filter pada Grid # --------------
    var FrmData_viGzKategoriDiet = new Ext.Panel
    (
		{
			title: NamaForm_viGzKategoriDiet,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viGzKategoriDiet,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ 
				PanelTab_viGzKategoriDiet,
				GridDataView_viGzKategoriDiet,
			],
			/* tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAdd_viGzKategoriDiet',
						handler: function(){
							
						}
					},
					
					
				]
			} */
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_viGzKategoriDiet;
    //-------------- # End form filter # --------------
}

function PanelSearchPemeriksaan(){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 0px 0px 0px 0px',
						border: false,
						width: 500,
						height: 25,
						anchor: '100% 100%',
						items:
						[
							{
								x: 0,
								y: 4,
								xtype: 'label',
								text: 'Nama Jenis Diet :',
								style:{
									fontSize:'11'
								}
							},
							cbJenis_viGzKategoriDiet()
						]
					}
				]
			}
		]		
	};
        return items;
}


function cbJenis_viGzKategoriDiet()
{
    var Field = ['kd_produk','deskripsi','kp_produk','kd_lab'];
    dsJenis_viGzKategoriDiet = new WebApp.DataStore({fields: Field});
	loadDataComboItemPemeriksaan_viGzKategoriDiet();
    cboJenis_viGzKategoriDiet = new Ext.form.ComboBox
    (
        {
            x: 140,
			y: 0,
            id: 'cboJenis_viGzKategoriDiet',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
			hideTrigger		: true,
            store: dsJenis_viGzKategoriDiet,
            valueField: 'kd_produk',
            displayField: 'deskripsi',
            width:200,
            listeners:
			{
				'select': function(a, b, c)
				{
					/* Ext.getCmp('txtNamaPasien_LapPerincianPasienRWI').setValue(b.data.nama);
					Ext.getCmp('txtNoKamar_LapPerincianPasienRWI').setValue(b.data.kamar);
					Ext.getCmp('txtKelas_LapPerincianPasienRWI').setValue(b.data.kelas);
					Ext.getCmp('dtpTglMasukAwalFilterHasilLab').setValue(ShowDate(b.data.tgl_masuk));
					Ext.getCmp('dtpTglMasukAkhirFilterHasilLab').setValue(ShowDate(b.data.tgl_inap));
					KdKasir=b.data.kd_kasir; */
					dataGridviGzKategoriDiet(b.data.kd_produk);
					CurrentKdKategoriSave=b.data.kd_produk;
					CurrentKdKategori=b.data.kd_produk;
					buttonAktif(true);
				},
				keyUp: function(a,b,c){
					
					if(  b.getKey()!=127 ){
						clearTimeout(this.time);
				
						this.time=setTimeout(function(){
							if(cboJenis_viGzKategoriDiet.lastQuery != '' ){
								var value="";
								
								if (value!=cboJenis_viGzKategoriDiet.lastQuery)
								{
									if (a.rendered && a.innerList != null) {
										a.innerList.update(a.loadingText ? '<div class="loading-indicator">' + a.loadingText + '</div>' : '');
										a.restrictHeight();
										a.selectedIndex = 0;
									}
									a.expand();
									Ext.Ajax.request({
										url: baseURL + "index.php/gizi/functionSetupKategoriDiet/getItemTest",
										params: {text:cboJenis_viGzKategoriDiet.lastQuery},
										failure: function(o){
											var cst = Ext.decode(o.responseText);
										},	    
										success: function(o) {
											cboJenis_viGzKategoriDiet.store.removeAll();
											var cst = Ext.decode(o.responseText);

											for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
												var recs    = [],recType = dsJenis_viGzKategoriDiet.recordType;
												var o=cst['listData'][i];
										
												recs.push(new recType(o));
												dsJenis_viGzKategoriDiet.add(recs);
											}
											a.expand();
											if(dsJenis_viGzKategoriDiet.onShowList != undefined)
												dsJenis_viGzKategoriDiet.onShowList(cst[showVar]);
											if(cst['listData'].length>0){
													
												a.doQuery(a.allQuery, true);
												a.expand();
												a.selectText(value.length,value.length);
											}else{
											//	if (a.rendered && a.innerList != null) {
													a.innerList.update(a.loadingText ? '&nbsp; Data Tidak Ada' : '');
													a.restrictHeight();
													a.selectedIndex = 0;
												//}
											}
										}
									});
									value=cboJenis_viGzKategoriDiet.lastQuery;
								}
							}
						},1000);
					}
				} 
			}
        }
    )
    return cboJenis_viGzKategoriDiet;
};

function loadDataComboItemPemeriksaan_viGzKategoriDiet(param){
	
	Ext.Ajax.request({
		url: baseURL + "index.php/gizi/functionSetupKategoriDiet/getItemTest",
		params: {xxx:param},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cboJenis_viGzKategoriDiet.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsJenis_viGzKategoriDiet.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsJenis_viGzKategoriDiet.add(recs);
				console.log(o);
			}
		}
	});
}

function dataGridviGzKategoriDiet(kd_kategori){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionSetupKategoriDiet/getItemGrid",
			params: {kd_kategori : kd_kategori},
			failure: function(o)
			{
				ShowPesanError_viGzKategoriDiet('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dataSource_viGzKategoriDiet.removeAll();
					var recs=[],
						recType=dataSource_viGzKategoriDiet.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dataSource_viGzKategoriDiet.add(recs);
					
					
					
					GridDataView_viGzKategoriDiet.getView().refresh();
				}
				else 
				{
					ShowPesanError_viGzKategoriDiet('Gagal membaca data items', 'Error');
				};
			}
		}
		
	)
	
}


function dataSaveItem_viGzKategoriDiet(){
	if (ValidasiSaveItem_viGzKategoriDiet(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/gizi/functionSetupKategoriDiet/saveItem",
				params: getParamSaveItem_viGzKategoriDiet(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanError_viGzKategoriDiet('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfo_viGzKategoriDiet('Berhasil menyimpan item ini','Information');
						dataSource_viGzKategoriDiet.removeAll();
						dataGridviGzKategoriDiet(CurrentKdKategoriSave);
						
					}
					else 
					{
						loadMask.hide();
					};
				}
			}
			
		)
	}
}

function dataDelete_viGzKategoriDiet(){
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/gizi/functionSetupKategoriDiet/delete",
				params: getParamDeleteItem_viGzKategoriDiet(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanError_viGzKategoriDiet('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfo_viGzKategoriDiet('Berhasil menghapus item ini','Information');
						dataSource_viGzKategoriDiet.removeAll();
						dataGridviGzKategoriDiet(rowSelected_viGzKategoriDiet.data.kd_kategori);
					}
					else 
					{
						loadMask.hide();
						ShowPesanError_viGzKategoriDiet('Gagal menghapus item ini', 'Error');
						dataSource_viGzKategoriDiet.removeAll();
						dataGridviGzKategoriDiet(rowSelected_viGzKategoriDiet.data.kd_kategori);
					};
				}
			}
			
		)
	/* }  */
	
}


function getParamSaveItem_viGzKategoriDiet(){
	var	params =
	{
		kd_kategori : CurrentKdKategoriSave
	}
	params['jumlah']=dataSource_viGzKategoriDiet.getCount();
	for(var i = 0 ; i < dataSource_viGzKategoriDiet.getCount();i++)
	{
		params['kd_jenis-'+i]=dataSource_viGzKategoriDiet.data.items[i].data.kd_jenis
		params['jenis_diet-'+i]=dataSource_viGzKategoriDiet.data.items[i].data.jenis_diet
		params['harga_pokok-'+i]=dataSource_viGzKategoriDiet.data.items[i].data.harga_pokok
	}
   
    return params
}

function getParamDeleteItem_viGzKategoriDiet(){
	console.log(rowSelected_viGzKategoriDiet.data);
	var	params =
	{
		kd_kategori : rowSelected_viGzKategoriDiet.data.kd_kategori,
		kd_jenis	: rowSelected_viGzKategoriDiet.data.kd_jenis
	}
   
    return params
};

function ValidasiSaveData_viGzKategoriDiet(modul,mBolHapus){
	var x = 1;
	if(CurrentKdTest === '' || CurrentKdTest == undefined){
		loadMask.hide();
		ShowPesanWarning_viGzKategoriDiet('Kategori Diet tidak tersedia atau belum di simpan! Untuk melanjutkan penyimpanan nilai normal, simpan terlebih dahulu Kategori Diet yang ada dalam grid.','Warning');
		x = 0;
	} 
	return x;
};

function ValidasiSaveItem_viGzKategoriDiet(modul,mBolHapus){
	var x = 1;
	if(dataSource_viGzKategoriDiet.getCount() === 0){
		loadMask.hide();
		ShowPesanWarning_viGzKategoriDiet('Kategori Diet tidak boleh kosong', 'Warning');
		x = 0;
	}
	for(var i=0; i<dataSource_viGzKategoriDiet.getCount() ; i++){
		var o=dataSource_viGzKategoriDiet.getRange()[i].data;
		// if(o.kd_metode == '' || o.kd_metode == undefined){
		// 	loadMask.hide();
		// 	ShowPesanWarning_viGzKategoriDiet('Metode tidak boleh kosong', 'Warning');
		// 	x = 0;
		// }
		for(var j=i+1; j<dataSource_viGzKategoriDiet.getCount() ; j++){
			var p=dataSource_viGzKategoriDiet.getRange()[j].data;
			/*if(o.item_test == p.item_test){
				loadMask.hide();
				ShowPesanWarning_viGzKategoriDiet('Kategori Diet tidak boleh sama', 'Warning');
				x = 0;
				break;
			}*/
		}
	}
	return x;
};

function ValidasiDeleteItem_viGzKategoriDiet(modul,mBolHapus){
	var x = 1;
	/* var line= GridDataView_viGzKategoriDiet.getSelectionModel().selection.cell[0];
	var o=dataSource_viGzKategoriDiet.getRange()[line].data;
	alert(o.kd_lab)
	alert(o.kd_test) */
	if((o.kd_lab == '' || o.kd_lab == undefined) || (o.kd_test == '' || o.kd_test == undefined)){
		loadMask.hide();
		ShowPesanWarning_viGzKategoriDiet('Pilih item yang akan di hapus','Warning');
		alert(o.kd_lab)
		alert(o.kd_test)
		x = 0;
	}
	return x;
};


function buttonAktif(){
	Ext.getCmp('btnAddItem_viGzKategoriDiet').enable(true);
	Ext.getCmp('btnSimpan_viGzKategoriDiet').enable(true);
	Ext.getCmp('btnDelete_viGzKategoriDiet').enable(true);
	Ext.getCmp('btnRefresh_viSetupPabrik').enable(true);
	//Ext.getCmp('PanelInputNilai_viGzKategoriDiet').enable(true);
}


function ShowPesanWarning_viGzKategoriDiet(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:350
		}
	);
};

function ShowPesanError_viGzKategoriDiet(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfo_viGzKategoriDiet(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};