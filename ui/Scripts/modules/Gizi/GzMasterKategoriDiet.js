var dataSource_viGzMasterKategoriDiet;
var selectCount_viGzMasterKategoriDiet=50;
var NamaForm_viGzMasterKategoriDiet="kategori Diet";
var mod_name_viGzMasterKategoriDiet="kategori Diet";
var now_viGzMasterKategoriDiet= new Date();
var rowSelected_viGzMasterKategoriDiet;
var setLookUps_viGzMasterKategoriDiet;
var tanggal = now_viGzMasterKategoriDiet.format("d/M/Y");
var jam = now_viGzMasterKategoriDiet.format("H/i/s");
var tmpkriteria;
var DataGridkategoriObat;
var GridDataView_viGzMasterKategoriDiet;

var str_kd_kategori;


var CurrentData_viGzMasterKategoriDiet =
{
	data: Object,
	details: Array,
	row: 0
};

var GzMasterKategoriDiet={};
GzMasterKategoriDiet.form={};
GzMasterKategoriDiet.func={};
GzMasterKategoriDiet.vars={};
GzMasterKategoriDiet.func.parent=GzMasterKategoriDiet;
GzMasterKategoriDiet.form.ArrayStore={};
GzMasterKategoriDiet.form.ComboBox={};
GzMasterKategoriDiet.form.DataStore={};
GzMasterKategoriDiet.form.Record={};
GzMasterKategoriDiet.form.Form={};
GzMasterKategoriDiet.form.Grid={};
GzMasterKategoriDiet.form.Panel={};
GzMasterKategoriDiet.form.TextField={};
GzMasterKategoriDiet.form.Button={};

GzMasterKategoriDiet.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_kategori', 'nama_kategori'],
	data: []
});

CurrentPage.page = dataGrid_viGzMasterKategoriDiet(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dataGrid_viGzMasterKategoriDiet(mod_id_viGzMasterKategoriDiet){	
 	// Field kiriman dari Project Net.
    var FieldMaster_viGzMasterKategoriDiet = 
	[
		'kd_kategori', 'nama_kategori' 
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viGzMasterKategoriDiet = new WebApp.DataStore
	({
        fields: FieldMaster_viGzMasterKategoriDiet
    });
    dataGriGzMasterKategoriDiet();
    // Grid gizi Perencanaan # --------------
	GridDataView_viGzMasterKategoriDiet = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viGzMasterKategoriDiet,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:true,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viGzMasterKategoriDiet = undefined;
							rowSelected_viGzMasterKategoriDiet = dataSource_viGzMasterKategoriDiet.getAt(row);
							CurrentData_viGzMasterKategoriDiet
							CurrentData_viGzMasterKategoriDiet.row = row;
							CurrentData_viGzMasterKategoriDiet.data = rowSelected_viGzMasterKategoriDiet.data;
							console.log(rec);
							str_kd_kategori		= rec.data.kd_kategori;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viGzMasterKategoriDiet = dataSource_viGzMasterKategoriDiet.getAt(ridx);
					if (rowSelected_viGzMasterKategoriDiet != undefined)
					{
						DataInitGzMasterKategoriDiet(rowSelected_viGzMasterKategoriDiet.data);
						//setLookUp_viGzMasterKategoriDiet(rowSelected_viGzMasterKategoriDiet.data);
					}
					else
					{
						//setLookUp_viGzMasterKategoriDiet();
					}
				},
				// End Function # --------------
				/* rowselect : function(sm, row, rec){
					console.log(sm);
					console.log(row);
					console.log(rec);
				} */
			},
			/**
	        *	Mengatur tampilan pada Grid Setup Unit
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colKode_viGzMasterKategoriDiet',
						header: 'Kode kategori',
						dataIndex: 'kd_kategori',
						hideable:false,
						menuDisabled: true,
						width: 40
						
					},
					//-------------- ## --------------
					{
						id: 'colNamaUnit_viGzMasterKategoriDiet',
						header: 'kategori Diet',
						dataIndex: 'nama_kategori',
						hideable:false,
						menuDisabled: true,
						width: 90
					},
					//-------------- ## --------------
					{
						id: 'colNoRO_viGzMasterKategoriDiet',
						header: 'Harga Pokok',
						dataIndex: 'harga_pokok',
						hideable:false,
						hidden:true,
						menuDisabled: true,
						align:'right',
						hidden : true,
						width: 70
						
					}
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viGzMasterKategoriDiet',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit kategori',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viGzMasterKategoriDiet',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viGzMasterKategoriDiet != undefined)
							{
								DataInitGzMasterKategoriDiet(rowSelected_viGzMasterKategoriDiet.data);
							}
							
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viGzMasterKategoriDiet, selectCount_viGzMasterKategoriDiet, dataSource_viGzMasterKategoriDiet),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)
	
	
	
	var PanelTabGzMasterKategoriDiet = new Ext.FormPanel({
        labelAlign: 'top',
		layout:'form',
		border: false,  
		autoHeight:true,
        title: '',
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [PanelInputUnit()]
		
	})
	// Kriteria filter pada Grid # --------------
    var FrmData_viGzMasterKategoriDiet = new Ext.Panel
    (
		{
			title: NamaForm_viGzMasterKategoriDiet,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viGzMasterKategoriDiet,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ PanelTabGzMasterKategoriDiet,
					GridDataView_viGzMasterKategoriDiet],
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAddkategori_viGzMasterKategoriDiet',
						handler: function(){
							AddNewGzMasterKategoriDiet();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viGzMasterKategoriDiet',
						handler: function()
						{
							loadMask.show();
							dataSave_viGzMasterKategoriDiet();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viGzMasterKategoriDiet',
						handler: function()
						{
							Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
								if (button == 'yes'){
									loadMask.show();
									dataDelete_viGzMasterKategoriDiet();
								}
							});
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_viGzMasterKategoriDiet',
						handler: function()
						{
							dataSource_viGzMasterKategoriDiet.removeAll();
							dataGriGzMasterKategoriDiet();
						}
					},
					{
						xtype: 'tbseparator'
					}
					
				]
			}
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_viGzMasterKategoriDiet;
    //-------------- # End form filter # --------------
}

function PanelInputUnit(rowdata){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 55,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'Kode kategori'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 0,
								xtype: 'textfield',
								id: 'txtKdkategori_GzMasterKategoriDiet',
								name: 'txtKdkategori_GzMasterKategoriDiet',
								width: 100,
								allowBlank: false,
								maxLength:3,
								readOnly:true,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									},
									blur: function(a){
										if(a.getValue().length > 3){
											ShowPesanWarningGzMasterKategoriDiet('Kode unit tidak boleh lebih dari 3 huruf', 'Warning');
										}
									}
								}
							},
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'kategori diet'
							},
							{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							GzMasterKategoriDiet.vars.NamaKategori=new Nci.form.Combobox.autoComplete({
								x: 130,
								y: 30,
								tabIndex:2,
								store	: GzMasterKategoriDiet.form.ArrayStore.a,
								select	: function(a,b,c){
									console.log(b);
									Ext.getCmp('txtKdkategori_GzMasterKategoriDiet').setValue(b.data.kd_kategori);
									
									GridDataView_viGzMasterKategoriDiet.getView().refresh();
									
								},
								onShowList:function(a){
									dataSource_viGzMasterKategoriDiet.removeAll();
									
									var recs=[],
									recType=dataSource_viGzMasterKategoriDiet.recordType;
										
									for(var i=0; i<a.length; i++){
										recs.push(new recType(a[i]));
									}
									dataSource_viGzMasterKategoriDiet.add(recs);
									
								},
								insert	: function(o){
									return {
										kd_kategori      	: o.kd_kategori,
										nama_kategori 		: o.nama_kategori,
										text			:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_kategori+'</td><td width="200">'+o.nama_kategori+'</td></tr></table>'
									}
								},
								url		: baseURL + "index.php/gizi/functionMasterKategoriDiet/getKategoriGrid",
								valueField: 'nama_kategori',
								displayField: 'nama_kategori',
								listWidth: 280
							}),
							//-----------------------------------------------------------------
							{
								x: 300,
								y: 0,
								xtype: 'label',
								hidden : true,
								text: 'Harga Pokok'
							},
							{
								x: 400,
								y: 0,
								xtype: 'label',
								hidden : true,
								text: ':'
							},
							{
								x: 410,
								y: 0,
								xtype: 'numberfield',
								id: 'txtHargaPokok_GzMasterKategoriDiet',
								name: 'txtHargaPokok_GzMasterKategoriDiet',
								width: 120,
								hidden : true,
								tabIndex:3,
								fieldStyle:'text-align:right;'
							}
							
							
						]
					}
				]
			}
		]		
	};
        return items;
}
//------------------end---------------------------------------------------------

function dataGriGzMasterKategoriDiet(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionMasterKategoriDiet/getKategoriGrid",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorGzMasterKategoriDiet('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dataSource_viGzMasterKategoriDiet.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dataSource_viGzMasterKategoriDiet.add(recs);
					
					
					
					GridDataView_viGzMasterKategoriDiet.getView().refresh();
				}
				else 
				{
					ShowPesanErrorGzMasterKategoriDiet('Gagal membaca data kategori diet', 'Error');
				};
			}
		}
		
	)
	
}

function dataSave_viGzMasterKategoriDiet(){
	//if (ValidasiSaveGzMasterKategoriDiet(nmHeaderSimpanData,false) == 1 )
//	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/gizi/functionMasterKategoriDiet/saveDiet",
				params: getParamSaveGzMasterKategoriDiet(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorGzMasterKategoriDiet('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					
					var cst = o.responseText;
					console.log(cst);
					if (cst === 'true') 
					{
						loadMask.hide();
						ShowPesanInfoGzMasterKategoriDiet('Berhasil menyimpan data ini','Information');
						dataSource_viGzMasterKategoriDiet.removeAll();
						dataGriGzMasterKategoriDiet();
						
					}
						else if (cst == 'exist') 
					{
						loadMask.hide();
						ShowPesanInfoGzMasterKategoriDiet('Data Telah Diupdate','Informasi');
						dataSource_viGzMasterKategoriDiet.removeAll();
						dataGriGzMasterKategoriDiet();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorGzMasterKategoriDiet('Gagal menyimpan data ini', 'Error');
						dataSource_viGzMasterKategoriDiet.removeAll();
						dataGriGzMasterKategoriDiet();
					};
				}
			}
			
		)
	//}
}

function dataDelete_viGzMasterKategoriDiet(){
	//if (ValidasiSaveGzMasterKategoriDiet(nmHeaderSimpanData,false) == 1 )
//	{
		Ext.Ajax.request
		(
			{
				//url: baseURL + "index.php/gizi/functionGzMasterKategoriDiet/delete",
				url: baseURL + "index.php/gizi/functionMasterKategoriDiet/deleteDiet",
				params: getParamDeleteGzMasterKategoriDiet(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorGzMasterKategoriDiet('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoGzMasterKategoriDiet('Berhasil menghapus data ini','Information');
						AddNewGzMasterKategoriDiet()
						dataSource_viGzMasterKategoriDiet.removeAll();
						dataGriGzMasterKategoriDiet();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorGzMasterKategoriDiet('Gagal menghapus data ini', 'Error');
						dataSource_viGzMasterKategoriDiet.removeAll();
						dataGriGzMasterKategoriDiet();
					};
				}
			}
			
		)
	//}
}

function AddNewGzMasterKategoriDiet(){
	Ext.getCmp('txtKdkategori_GzMasterKategoriDiet').setValue('');
	GzMasterKategoriDiet.vars.NamaKategori.setValue('');
	Ext.getCmp('txtHargaPokok_GzMasterKategoriDiet').setValue('');
};

function DataInitGzMasterKategoriDiet(rowdata){
	Ext.getCmp('txtKdkategori_GzMasterKategoriDiet').setValue(rowdata.kd_kategori);
	GzMasterKategoriDiet.vars.NamaKategori.setValue(rowdata.nama_kategori);
};

function getParamSaveGzMasterKategoriDiet(){
	var	params =
	{
		Kdkategori		: Ext.getCmp('txtKdkategori_GzMasterKategoriDiet').getValue(),
		NamaKategori	: GzMasterKategoriDiet.vars.NamaKategori.getValue()
	}
   
    return params
};

function getParamDeleteGzMasterKategoriDiet(){
	var	params =
	{
		kd_kategori		: str_kd_kategori,
	}
   
    return params
};

function ValidasiSaveGzMasterKategoriDiet(modul,mBolHapus){
	var x = 1;
	if(GzMasterKategoriDiet.vars.NamaKategori.getValue() === '' || Ext.getCmp('txtKdkategori_GzMasterKategoriDiet').getValue() ===''){
		if(GzMasterKategoriDiet.vars.NamaKategori.getValue() === ''){
			loadMask.hide();
			ShowPesanWarningGzMasterKategoriDiet('Nama unit masih kosong', 'Warning');
			x = 0;
		} else{
			loadMask.hide();
			ShowPesanWarningGzMasterKategoriDiet('Kode unit masih kosong', 'Warning');
			x = 0;
		}
		
	} 
	if(Ext.getCmp('txtKdkategori_GzMasterKategoriDiet').getValue().length > 3){
		ShowPesanWarningGzMasterKategoriDiet('Kode unit tidak boleh lebih dari 3 huruf', 'Warning');
		x = 0;
	}
	return x;
};



function ShowPesanWarningGzMasterKategoriDiet(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorGzMasterKategoriDiet(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoGzMasterKategoriDiet(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};