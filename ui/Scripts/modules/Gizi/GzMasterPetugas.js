var dataSource_viGzPetugas;
var selectCount_viGzPetugas=50;
var NamaForm_viGzPetugas="Petugas";
var mod_name_viGzPetugas="Petugas";
var now_viGzPetugas= new Date();
var rowSelected_viGzPetugas;
var tanggal = now_viGzPetugas.format("d/M/Y");
var jam = now_viGzPetugas.format("H/i/s");
var GridDataView_viGzPetugas;


var CurrentData_viGzPetugas =
{
	data: Object,
	details: Array,
	row: 0
};

var GzPetugas={};
GzPetugas.form={};
GzPetugas.func={};
GzPetugas.vars={};
GzPetugas.func.parent=GzPetugas;
GzPetugas.form.ArrayStore={};
GzPetugas.form.ComboBox={};
GzPetugas.form.DataStore={};
GzPetugas.form.Record={};
GzPetugas.form.Form={};
GzPetugas.form.Grid={};
GzPetugas.form.Panel={};
GzPetugas.form.TextField={};
GzPetugas.form.Button={};

GzPetugas.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_petugas', 'petugas'],
	data: []
});

CurrentPage.page = dataGrid_viGzPetugas(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dataGrid_viGzPetugas(mod_id_viGzPetugas){	
 	// Field kiriman dari Project Net.
    var FieldMaster_viGzPetugas = 
	[
		'kd_petugas', 'petugas'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viGzPetugas = new WebApp.DataStore
	({
        fields: FieldMaster_viGzPetugas
    });
    dataGriGzPetugas();
    // Grid gizi Perencanaan # --------------
	GridDataView_viGzPetugas = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viGzPetugas,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:true,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viGzPetugas = undefined;
							rowSelected_viGzPetugas = dataSource_viGzPetugas.getAt(row);
							CurrentData_viGzPetugas
							CurrentData_viGzPetugas.row = row;
							CurrentData_viGzPetugas.data = rowSelected_viGzPetugas.data;
							//DataInitGzPetugas(rowSelected_viGzPetugas.data);
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viGzPetugas = dataSource_viGzPetugas.getAt(ridx);
					if (rowSelected_viGzPetugas != undefined)
					{
						DataInitGzPetugas(rowSelected_viGzPetugas.data);
						//setLookUp_viGzPetugas(rowSelected_viGzPetugas.data);
					}
					else
					{
						//setLookUp_viGzPetugas();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Setup PetugasGizi
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header: 'Kode Petugas',
						dataIndex: 'kd_petugas',
						hideable:false,
						menuDisabled: true,
						width: 40
						
					},
					//-------------- ## --------------
					{
						header: 'Nama Petugas',
						dataIndex: 'petugas',
						hideable:false,
						menuDisabled: true,
						width: 90
					}
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viGzPetugas',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Petugas',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viGzPetugas',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viGzPetugas != undefined)
							{
								DataInitGzPetugas(rowSelected_viGzPetugas.data);
							}
							
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viGzPetugas, selectCount_viGzPetugas, dataSource_viGzPetugas),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)	
	var PanelTabGzPetugas = new Ext.FormPanel({
        labelAlign: 'top',
		layout:'form',
		border: false,  
		autoHeight:true,
        title: '',
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [PanelInputPetugasGizi()]
		
	})
	// Kriteria filter pada Grid # --------------
    var FrmData_viGzPetugas = new Ext.Panel
    (
		{
			title: NamaForm_viGzPetugas,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viGzPetugas,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ PanelTabGzPetugas,
					GridDataView_viGzPetugas],
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAddPetugas_viGzPetugas',
						handler: function(){
							AddNewGzPetugas();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viGzPetugas',
						handler: function()
						{
							loadMask.show();
							dataSave_viGzPetugas();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viGzPetugas',
						handler: function()
						{
							Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
								if (button == 'yes'){
									loadMask.show();
									dataDelete_viGzPetugas();
								}
							});
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_viGzPetugas',
						handler: function()
						{
							dataSource_viGzPetugas.removeAll();
							dataGriGzPetugas();
						}
					},
					{
						xtype: 'tbseparator'
					}
					
				]
			}
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_viGzPetugas;
    //-------------- # End form filter # --------------
}

function PanelInputPetugasGizi(rowdata){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 55,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'Kode petugas'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 0,
								xtype: 'textfield',
								id: 'txtKdPetugas_GzPetugas',
								name: 'txtKdPetugas_GzPetugas',
								width: 100,
								allowBlank: false,
								maxLength:2,
								readOnly:true,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									},
									blur: function(a){
										if(a.getValue().length > 2){
											ShowPesanWarningGzPetugas('Kode petugas tidak boleh lebih dari 2 huruf', 'Warning');
										}
									}
								}
							},
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'Nama petugas'
							},
							{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							GzPetugas.vars.namaPetugas=new Nci.form.Combobox.autoComplete({
								x: 130,
								y: 30,
								tabIndex:2,
								store	: GzPetugas.form.ArrayStore.a,
								select	: function(a,b,c){
									
									Ext.getCmp('txtKdPetugas_GzPetugas').setValue(b.data.kd_petugas);
									
									GridDataView_viGzPetugas.getView().refresh();
									
								},
								onShowList:function(a){
									dataSource_viGzPetugas.removeAll();
									
									var recs=[],
									recType=dataSource_viGzPetugas.recordType;
										
									for(var i=0; i<a.length; i++){
										recs.push(new recType(a[i]));
									}
									dataSource_viGzPetugas.add(recs);
									
								},
								insert	: function(o){
									return {
										kd_petugas      : o.kd_petugas,
										petugas 		: o.petugas,
										text			:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_petugas+'</td><td width="200">'+o.petugas+'</td></tr></table>'
									}
								},
								url		: baseURL + "index.php/gizi/functionMasterPetugas/getPetugasGrid",
								valueField: 'petugas',
								displayField: 'text',
								listWidth: 280
							})
							
							
						]
					}
				]
			}
		]		
	};
        return items;
}
//------------------end---------------------------------------------------------

function dataGriGzPetugas(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionMasterPetugas/getPetugasGrid",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorGzPetugas('Error, daftar petugas! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dataSource_viGzPetugas.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dataSource_viGzPetugas.add(recs);
					
					
					
					GridDataView_viGzPetugas.getView().refresh();
				}
				else 
				{
					ShowPesanErrorGzPetugas('Gagal membaca data jenis diet', 'Error');
				};
			}
		}
		
	)
	
}

function dataSave_viGzPetugas(){
	if (ValidasiSaveGzPetugas(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/gizi/functionMasterPetugas/save",
				params: getParamSaveGzPetugas(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorGzPetugas('Error, simpan data! Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoGzPetugas('Berhasil menyimpan data ini','Information');
						Ext.getCmp('txtKdPetugas_GzPetugas').setValue(cst.kdpetugas);
						dataSource_viGzPetugas.removeAll();
						dataGriGzPetugas();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorGzPetugas('Gagal menyimpan data ini', 'Error');
						dataSource_viGzPetugas.removeAll();
						dataGriGzPetugas();
					};
				}
			}
		)
	}
}

function dataDelete_viGzPetugas(){
	if (ValidasiSaveGzPetugas(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/gizi/functionMasterPetugas/delete",
				params: getParamDeleteGzPetugas(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorGzPetugas('Error, hapus data! Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoGzPetugas('Berhasil menghapus data ini','Information');
						AddNewGzPetugas()
						dataSource_viGzPetugas.removeAll();
						dataGriGzPetugas();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorGzPetugas('Gagal menghapus data ini', 'Error');
						dataSource_viGzPetugas.removeAll();
						dataGriGzPetugas();
					};
				}
			}
			
		)
	}
}

function AddNewGzPetugas(){
	Ext.getCmp('txtKdPetugas_GzPetugas').setValue('');
	GzPetugas.vars.namaPetugas.setValue('');
};

function DataInitGzPetugas(rowdata){
	Ext.getCmp('txtKdPetugas_GzPetugas').setValue(rowdata.kd_petugas);
	GzPetugas.vars.namaPetugas.setValue(rowdata.petugas);
};

function getParamSaveGzPetugas(){
	var	params =
	{
		KdPetugas:Ext.getCmp('txtKdPetugas_GzPetugas').getValue(),
		NamaPetugas:GzPetugas.vars.namaPetugas.getValue()
	}
   
    return params
};

function getParamDeleteGzPetugas(){
	var	params =
	{
		KdPetugas:Ext.getCmp('txtKdPetugas_GzPetugas').getValue()
	}
   
    return params
};

function ValidasiSaveGzPetugas(modul,mBolHapus){
	var x = 1;
	if(GzPetugas.vars.namaPetugas.getValue() === ''){
		if(GzPetugas.vars.namaPetugas.getValue() === ''){
			loadMask.hide();
			ShowPesanWarningGzPetugas('Nama petugas masih kosong', 'Warning');
			x = 0;
		}		
	} 
	return x;
};

function ShowPesanWarningGzPetugas(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorGzPetugas(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoGzPetugas(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};