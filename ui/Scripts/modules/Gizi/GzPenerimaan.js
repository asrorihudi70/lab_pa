var dataSource_viGzPenerimaanDiet;
var selectCount_viGzPenerimaanDiet=50;
var NamaForm_viGzPenerimaanDiet="Penerimaan Order";
var selectCountStatusPostingGzPenerimaanDiet='Semua';
var mod_name_viGzPenerimaanDiet="viGzPenerimaan";
var now_viGzPenerimaanDiet= new Date();
var addNew_viGzPenerimaanDiet;
var rowSelected_viGzPenerimaanDiet;
var setLookUps_viGzPenerimaanDiet;
var mNoKunjungan_viGzPenerimaanDiet='';
var selectSetUnit;
var cellSelecteddeskripsiRWI;
var tanggal = now_viGzPenerimaanDiet.format("d/M/Y");
var jam = now_viGzPenerimaanDiet.format("H/i/s");
var tampungshiftsekarang;
var tmpkriteria;
var GridDataView_viGzPenerimaanDiet;

var CurrentGzPenerimaanDiet =
{
    data: Object,
    details: Array,
    row: 0
};

var CurrentData_viGzPenerimaanDiet =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viGzPenerimaanDiet(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

var gzPenerimaanOrder={};
gzPenerimaanOrder.form={};
gzPenerimaanOrder.func={};
gzPenerimaanOrder.vars={};
gzPenerimaanOrder.func.parent=gzPenerimaanOrder;
gzPenerimaanOrder.form.ArrayStore={};
gzPenerimaanOrder.form.ComboBox={};
gzPenerimaanOrder.form.DataStore={};
gzPenerimaanOrder.form.Record={};
gzPenerimaanOrder.form.Form={};
gzPenerimaanOrder.form.Grid={};
gzPenerimaanOrder.form.Panel={};
gzPenerimaanOrder.form.TextField={};
gzPenerimaanOrder.form.Button={};


gzPenerimaanOrder.form.ArrayStore.noorder=new Ext.data.ArrayStore({
		id: 0,
		fields: [	
					'no_order','kd_vendor','vendor'
				],
		data: []
	});

function dataGrid_viGzPenerimaanDiet(mod_idOrder_viGzPenerimaanDiet){	
    var FieldMaster_viGzPenerimaanDiet = 
	[
		'no_terima','no_order','kd_vendor','vendor','tgl_terima',
		'kd_petugas','petugas','keterangan'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viGzPenerimaanDiet = new WebApp.DataStore
	({
        fields: FieldMaster_viGzPenerimaanDiet
    });
    dataGriAwal();
    // Grid Gizi Perencanaan # --------------
	GridDataView_viGzPenerimaanDiet = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viGzPenerimaanDiet,
			autoScroll: true,
			columnLines: true,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viGzPenerimaanDiet = undefined;
							rowSelected_viGzPenerimaanDiet = dataSource_viGzPenerimaanDiet.getAt(row);
							CurrentData_viGzPenerimaanDiet
							CurrentData_viGzPenerimaanDiet.row = row;
							CurrentData_viGzPenerimaanDiet.data = rowSelected_viGzPenerimaanDiet.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viGzPenerimaanDiet = dataSource_viGzPenerimaanDiet.getAt(ridx);
					if (rowSelected_viGzPenerimaanDiet != undefined)
					{
						setLookUp_viGzPenerimaanDiet(rowSelected_viGzPenerimaanDiet.data);
					}
					else
					{
						setLookUp_viGzPenerimaanDiet();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Gizi perencanaan
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header: 'No. Terima',
						dataIndex: 'no_terima',
						sortable: true,
						width: 35
						
					},
					//-------------- ## --------------
					{
						header: 'No. Order',
						dataIndex: 'no_order',
						sortable: true,
						width: 35
						
					},
					//-------------- ## --------------
					{
						header:'Tgl Terima',
						dataIndex: 'tgl_terima',						
						width: 30,
						sortable: true,
						hideable:false,
                        menuDisabled:true,
						// format: 'd/M/Y',
						//filter: {},
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.tgl_terima);
						}
					},
					//-------------- ## --------------
					{
						header: 'Vendor',
						dataIndex: 'vendor',
						sortable: true,
						hideable:false,
                        menuDisabled:true,
						width: 30
					},
					//-------------- ## --------------
					{
						header: 'Keterangan',
						dataIndex: 'keterangan',
						sortable: true,
						width: 50
					},
					//-------------- HIDDEN --------------
					{
						header: 'kd_vendor',
						dataIndex: 'kd_vendor',
						sortable: true,
						hidden:true,
						width: 40
					},
					{
						header: 'Ahli Gizi',
						dataIndex: 'NAMA_UNIT',
						sortable: true,
						hidden:true,
						width: 40
					}
					//-------------- ## --------------
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbarOrder_viGzPenerimaanDiet',
				items: 
				[
					{
						xtype: 'button',
						text: 'Tambah Order ',
						iconCls: 'Edit_Tr',
						tooltip: 'Add Data',
						id: 'btnTambahOrder_viGzPenerimaanDiet',
						handler: function(sm, row, rec)
						{
							setLookUp_viGzPenerimaanDiet();
						}
					}/* ,
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEditOrder_viGzPenerimaanDiet',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viGzPenerimaanDiet != undefined)
							{
								setLookUp_viGzPenerimaanDiet(rowSelected_viGzPenerimaanDiet.data)
							}
							//setLookUp_viGzPenerimaanDiet();
							
						}
					} */
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viGzPenerimaanDiet, selectCount_viGzPenerimaanDiet, dataSource_viGzPenerimaanDiet),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )
	
	var pencarianGzPenerimaanDiet = new Ext.FormPanel({
        labelAlign: 'top',
        frame:true,
        title: '',
        bodyStyle:'padding:5px 5px 0',
        //width: 600,
        items: [
		{
			layout: 'column',
			border: false,
			items:
			[
				{
					columnWidth:.98,
					layout: 'absolute',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					width: 500,
					height: 70,
					anchor: '100% 100%',
					items:
					[
						{
							x: 10,
							y: 0,
							xtype: 'label',
							text: 'No. Penerimaan'
						},
						{
							x: 120,
							y: 0,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 0,
							xtype: 'textfield',
							id: 'TxtNoPenerimaanFilterGridDataView_viGzPenerimaanDiet',
							name: 'TxtNoPenerimaanFilterGridDataView_viGzPenerimaanDiet',
							width: 130,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariGzPenerimaanDiet();
										refreshPenerimaanDiet(tmpkriteria);
									} 						
								}
							}
						},
						{
							x: 10,
							y: 30,
							xtype: 'label',
							text: 'No. Order'
						},
						{
							x: 120,
							y: 30,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 30,
							xtype: 'textfield',
							id: 'txtOrderGZPenerimaanDiet',
							name: 'txtOrderGZPenerimaanDiet',
							width: 130,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariGzPenerimaanDiet();
										refreshPenerimaanDiet(tmpkriteria);
									} 						
								}
							}
						},
						//-------------- ## --------------
						{
							x: 310,
							y: 0,
							xtype: 'label',
							text: 'Tanggal Terima'
						},
						{
							x: 400,
							y: 0,
							xtype: 'label',
							text: ':'
						},
						{
							x: 410,
							y: 0,
							xtype: 'datefield',
							id: 'dfTglAwalPenerimaanGzPenerimaanDiet',
							format: 'd/M/Y',
							width: 120,
							tabIndex:3,
							value:now_viGzPenerimaanDiet,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariGzPenerimaanDiet();
										refreshPenerimaanDiet(tmpkriteria);
									} 						
								}
							}
						},
						{
							x: 540,
							y: 0,
							xtype: 'label',
							text: 's/d'
						},
						{
							x: 568,
							y: 0,
							xtype: 'datefield',
							id: 'dfTglAkhirPenerimaanGzPenerimaanDiet',
							format: 'd/M/Y',
							width: 120,
							tabIndex:4,
							value:now_viGzPenerimaanDiet,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariGzPenerimaanDiet();
										refreshPenerimaanDiet(tmpkriteria);
									} 						
								}
							}
						},
						
						//----------------------------------------
						{
							x: 568,
							y: 30,
							xtype: 'button',
							text: 'Cari',
							iconCls: 'refresh',
							tooltip: 'Cari',
							style:{paddingLeft:'30px'},
							//rowspan: 3,
							width:150,
							id: 'BtnFilterGridDataView_viGzPenerimaanDiet',
							handler: function() 
							{					
								tmpkriteria = getCriteriaCariGzPenerimaanDiet();
								refreshPenerimaanDiet(tmpkriteria);
							}                        
						}
					]
				}
			]
		}
		]	
						/*				
						//-------------- ## --------------
						
						//-------------- ## --------------
				]
			} */
		
		
	})

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viGzPenerimaanDiet = new Ext.Panel
    (
		{
			title: NamaForm_viGzPenerimaanDiet,
			iconCls: 'Studi_Lanjut',
			id: mod_idOrder_viGzPenerimaanDiet,
			region: 'center',
			layout: 'form', 
			closable: true,        
			border: false,  
			margins: '0 5 5 0',
			items: [ pencarianGzPenerimaanDiet,
					GridDataView_viGzPenerimaanDiet],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viGzPenerimaanDiet,
		            columns: 21,
		            defaults: {
					scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viGzPenerimaanDiet;
    //-------------- # End form filter # --------------
}

function refreshPenerimaanDiet(kriteria)
{
    dataSource_viGzPenerimaanDiet.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'ViewPenerimaanDiet',
                    param : kriteria
                }			
            }
        );   
    return dataSource_viGzPenerimaanDiet;
}

function setLookUp_viGzPenerimaanDiet(rowdata){
    var lebar = 985;
    setLookUps_viGzPenerimaanDiet = new Ext.Window({
        id: Nci.getId(),
        title: NamaForm_viGzPenerimaanDiet, 
        closeAction: 'destroy',        
        width: 900,
        height: 580,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viGzPenerimaanDiet(lebar,rowdata),
        listeners:{
            activate: function(){
				
            },
            afterShow: function(){
                this.activate();
            },
            deactivate: function(){
                rowSelected_viGzPenerimaanDiet=undefined;
            }
        }
    });

    setLookUps_viGzPenerimaanDiet.show();
    if (rowdata == undefined){	
    }
    else
    {
        datainit_viGzPenerimaanDiet(rowdata);
    }
}

function getFormItemEntry_viGzPenerimaanDiet(lebar,rowdata){
    var pnlFormDataBasic_viGzPenerimaanDiet = new Ext.FormPanel({
		title: '',
		region: 'north',
		layout: 'form',
		bodyStyle: 'padding:10px 10px 10px 10px',
		anchor: '100%',
		labelWidth: 1,
		autoWidth: true,
		width: lebar,
		border: false,
		items:[
				getItemPanelInputPenerimaan_viGzPenerimaanDiet(lebar),
				getItemDetailPenerimaan_viGzPenerimaanDiet(lebar)
			],
			fileUpload: true,
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						hidden:true,
						id: 'btnAdd_viGzPenerimaanDiet',
						handler: function(){
							dataaddnew_viGzPenerimaanDiet();
						}
					},{
						xtype: 'tbseparator'
					},{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						disabled:true,
						id: 'btnSimpan_viGzPenerimaanDiet',
						handler: function()
						{
							datasave_viGzPenerimaanDiet();
						}
					},{
						xtype: 'tbseparator'
					},{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						disabled:true,
						id: 'btnSimpanExit_viGzPenerimaanDiet',
						handler: function()
						{
							/* var x = datasave_viGzPenerimaanDiet(addNew_viGzPenerimaanDiet);
							datarefresh_viGzPenerimaanDiet();
							if (x===undefined)
							{
								setLookUps_viGzPenerimaanDiet.close();
							} */
							datasave_viGzPenerimaanDiet();
							refreshPenerimaanDiet();
							setLookUps_viGzPenerimaanDiet.close();
						}
					},{
						xtype: 'tbseparator'
					},
					{
						xtype:'splitbutton',
						text:'Cetak',
						iconCls:'print',
						id:'btnPrint_vigzPenerimaanOrder',
						disabled:true,
						hidden:true,
						handler:function()
						{							
								
						},
						menu: new Ext.menu.Menu({
						items: [
							// these items will render as dropdown menu items when the arrow is clicked:
							{
								xtype: 'button',
								text: 'Print Bill',
								id: 'btnPrintBillgzPenerimaanOrder',
								handler: function()
								{
									printbill();
								}
							},
						]
						})
					}
					
				]
			}//,items:
		}
    )

    return pnlFormDataBasic_viGzPenerimaanDiet;
}

function getItemPanelInputPenerimaan_viGzPenerimaanDiet(lebar) {
    var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 115,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'No. Penerimaan'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 0,
								xtype: 'textfield',
								id: 'txtNoPenerimaanGzPenerimaanDietL',
								name: 'txtNoPenerimaanGzPenerimaanDietL',
								width: 130,
								readOnly:true,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									}
								}
							},
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'Tanggal Terima'
							},
							{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 30,
								xtype: 'datefield',
								id: 'dfTglTerimaGzPenerimaanDietL',
								format: 'd/M/Y',
								width: 150,
								tabIndex:2,
								readOnly:true,
								value:now_viGzPenerimaanDiet,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									}
								}
							},
							{
								x: 10,
								y: 60,
								xtype: 'label',
								text: 'No. Order'
							},
							{
								x: 120,
								y: 60,
								xtype: 'label',
								text: ':'
							},
							/* {
								x: 130,
								y: 60,
								xtype: 'textfield',
								id: 'txtNoOrderPenerimaanGzPenerimaanDietL',
								name: 'txtNoOrderPenerimaanGzPenerimaanDietL',
								width: 130,
								readOnly:true,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									}
								}
							}, */
							gzPenerimaanOrder.form.ComboBox.noOrder= new Nci.form.Combobox.autoComplete({
								store	: gzPenerimaanOrder.form.ArrayStore.noorder,
								select	: function(a,b,c){
									Ext.getCmp('cbo_vendorGzPenerimaanDiet').setValue(b.data.kd_vendor);									
									cekDataOrder(b.data.no_order);
									
									Ext.getCmp('btnSimpan_viGzPenerimaanDiet').enable();
									Ext.getCmp('btnSimpanExit_viGzPenerimaanDiet').enable();
									
									Ext.getCmp('cbo_vendorGzPenerimaanDiet').setReadOnly(true);
									
								},
								width	: 150,
								x: 130,
								y: 60,
								insert	: function(o){
									return {
										kd_vendor		:o.kd_vendor,
										vendor			:o.vendor,
										text			:  '<table style="font-size: 11px;"><tr><td width="80">'+o.no_order+'</td></tr></table>',
										no_order		:o.no_order
									}
								},
								url		: baseURL + "index.php/gizi/functionPenerimaanOrder/getAutoComNoOrder",
								valueField: 'no_order',
								displayField: 'text',
								listWidth: 260
							}),
							{
								x: 10,
								y: 90,
								xtype: 'label',
								text: 'Petugas Gizi'
							},
							{
								x: 120,
								y: 90,
								xtype: 'label',
								text: ':'
							},
							ComboAhliGiziGzPenerimaanDiet(),
							
							//-------------- ## --------------
							{
								x: 320,
								y: 0,
								xtype: 'label',
								text: 'Dari Vendor'
							},
							{
								x: 410,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							ComboVendorGzPenerimaanDiet(),
							{
								x: 320,
								y: 30,
								xtype: 'label',
								text: 'Keterangan'
							},
							{
								x: 410,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							{
								x: 420,
								y: 30,	
								xtype: 'textarea',
								name: 'txtKetGZPenerimaanL',
								id: 'txtKetGZPenerimaanL',
								width : 280,
								height : 80,
								tabIndex:6
							}
							
						]
					}
				]
			}
		]		
	};
    return items;
};

function getItemDetailPenerimaan_viGzPenerimaanDiet(lebar){
    var items ={
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'margin-top: -1px;',
		border:false,
		width: lebar-80,
		height: 375,//300, 
	    tbar:
		[
			/* {
				text	: 'Add Pasien',
				id		: 'btnAddPasienGzPenerimaanDietL',
				tooltip	: nmLookup,
				disabled: true,
				iconCls	: 'find',
				handler	: function(){
					var records = new Array();
					records.push(new dsDataGrdDetailOrder_viGzPenerimaanDiet.recordType());
					dsDataGrdDetailOrder_viGzPenerimaanDiet.add(records);
				}
			},{
				xtype: 'tbseparator'
			},{
				xtype: 'button',
				text: 'Delete',
				iconCls: 'remove',
				disabled:true,
				id: 'btnDelete_viGzPenerimaanDiet',
				handler: function()
				{
					var line = gzPenerimaanOrder.form.Grid.a.getSelectionModel().selection.cell[0];
					var o = dsDataGrdDetailOrder_viGzPenerimaanDiet.getRange()[line].data;
					if(dsDataGrdDetailOrder_viGzPenerimaanDiet.getCount()>1){
						Ext.Msg.confirm('Warning', 'Apakah data resep obat ini akan dihapus?', function(button){
							if (button == 'yes'){
								if(dsDataGrdDetailOrder_viGzPenerimaanDiet.getRange()[line].data.no_out != undefined){
									Ext.Ajax.request
									(
										{
											url: baseURL + "index.php/Gizi/functionGizirwi/hapusBarisGridgzPenerimaanOrder",
											params:{no_out:o.no_out, tgl_out:o.tgl_out, kd_prd:o.kd_prd, kd_milik:o.kd_milik, no_urut:o.no_urut} ,
											failure: function(o)
											{
												ShowPesanErrorgzPenerimaanOrder('Hubungi Admin', 'Error');
											},	
											success: function(o) 
											{
												var cst = Ext.decode(o.responseText);
												if (cst.success === true) 
												{
													dsDataGrdDetailOrder_viGzPenerimaanDiet.removeAt(line);
													gzPenerimaanOrder.form.Grid.a.getView().refresh();
													
													Ext.getCmp('btnSimpan_viGzPenerimaanDiet').enable();
													Ext.getCmp('btnSimpanExit_viGzPenerimaanDiet').enable();
													Ext.getCmp('btnBayar_viGzPenerimaanDiet').disable();
													
													
												}
												else 
												{
													ShowPesanErrorgzPenerimaanOrder('Gagal melakukan penghapusan', 'Error');
												};
											}
										}
										
									)
								}else{
									dsDataGrdDetailOrder_viGzPenerimaanDiet.removeAt(line);
									gzPenerimaanOrder.form.Grid.a.getView().refresh();
									Ext.getCmp('btnSimpan_viGzPenerimaanDiet').enable();
									Ext.getCmp('btnSimpanExit_viGzPenerimaanDiet').enable();
								}
							} 
							
						});
					} else{
						ShowPesanErrorgzPenerimaanOrder('Data tidak bisa dihapus karena minimal resep 1 obat','Error');
					}
					
				}
			}	 */
		],
		items:[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:[
					gridDataViewEdit_viGzPenerimaanDiet(),
					
					{
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 600,
						height: 20,
						anchor: '100% 100%',
						items:
						[
							{
								x: 745,
								y: 10,	
								xtype: 'textfield',
								name: 'txtGrandTot',
								id: 'txtGrandTot',
								align:'right',
								width : 120,
								tabIndex:6,
								style: 'text-align: right'
							}
						]
					}
			
				]	
			}
		]
	};
    return items;
};


var a={};
function gridDataViewEdit_viGzPenerimaanDiet(){
    var FieldGrdKasir_viGzPenerimaanDiet = [];
    dsDataGrdDetailOrder_viGzPenerimaanDiet= new WebApp.DataStore({
        fields: FieldGrdKasir_viGzPenerimaanDiet
    });
    
    gzPenerimaanOrder.form.Grid.a =new Ext.grid.EditorGridPanel({
        store: dsDataGrdDetailOrder_viGzPenerimaanDiet,
        height: 320,
        columnLines: true,
		selModel: new Ext.grid.CellSelectionModel ({
            singleSelect: true,
            listeners:{
                rowselect: function(sm, row, rec){
                }
            }
        }),
        stripeRows: true,
        cm:new Ext.grid.ColumnModel([
        	new Ext.grid.RowNumberer(),	
			{
				dataIndex: 'no_minta',
				header: 'No Permintaan',
				sortable: false,
				width: 70
			},
			{			
				dataIndex: 'jenis_diet',
				header: 'Jenis Diet',
				sortable: false,
				width: 200
			},
			{
				dataIndex: 'waktu',
				header: 'Waktu',
				width: 60,
				sortable: false
			},{
				dataIndex: 'qty_order',
				header: 'Qty Order',
				xtype:'numbercolumn',
				sortable: false,
				align:'right',
				width: 50
			},{
				dataIndex: 'harga_beli',
				header: 'Harga Beli',
				xtype:'numbercolumn',
				sortable: false,
				align:'right',
				width: 60,
				editor: new Ext.form.NumberField({
					allowBlank: false,
					listeners:{
						blur: function(a){
							var line	= this.index;
							if(a.getValue()=='' || a.getValue()==undefined){
								ShowPesanErrorgzPenerimaanOrder('Harga beli belum di isi', 'Warning');
							}else{
								dsDataGrdDetailOrder_viGzPenerimaanDiet.getRange()[line].data.harga_beli=a.getValue();
								hasilJumlah();
							}
						},
						focus: function(a){
							this.index=gzPenerimaanOrder.form.Grid.a.getSelectionModel().selection.cell[0];
						}
					}
				})	
			},
			{
				dataIndex: 'qty',
				header: 'Qty Terima',
				xtype:'numbercolumn',
				sortable: false,
				width: 50,
				align:'right',
				editor: new Ext.form.NumberField({
					allowBlank: false,
					listeners:{
						blur: function(a){
							var line	= this.index;
							if(a.getValue()=='' || a.getValue()==undefined){
								ShowPesanErrorgzPenerimaanOrder('Qty terima belum di isi', 'Warning');
							}else{
								hasilJumlah();
								this.index=a.getValue();
							}
						},
						focus: function(a){
							this.index=gzPenerimaanOrder.form.Grid.a.getSelectionModel().selection.cell[0];
						}
					}
				})	
			},
			{
				dataIndex: 'total',
				header: 'Total',
				xtype:'numbercolumn',
				sortable: false,
				width: 60
			},
			//-------------- HIDDEN --------------
			//-------------- ## --------------
			{
				dataIndex: 'kd_waktu',
				header: 'kd_waktu',
				hidden: true,
				width: 80
			},
			//-------------- ## --------------
			{
				dataIndex: 'kd_jenis',
				header: 'kd_jenis',
				hidden: true,
				width: 80
			},
			{
				dataIndex: 'no_order',
				header: 'no_order',
				hidden: true,
				width: 80
			}
			//-------------- ## --------------
        ]),
		viewConfig:{
			forceFit: true
		}
    });
    return gzPenerimaanOrder.form.Grid.a;
}

function ComboAhliGiziGzPenerimaanDiet()
{
    var Field_ahliGiziGzPenerimaanDiet = ['kd_petugas', 'petugas'];
    ds_ahliGiziGzPenerimaanDiet = new WebApp.DataStore({fields: Field_ahliGiziGzPenerimaanDiet});
    ds_ahliGiziGzPenerimaanDiet.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'kd_petugas',
					Sortdir: 'ASC',
					target: 'ComboPetugasGizi',
					param: ''
				}
		}
	);
	
    var cbo_PetugasGiziGzPenerimaanDiet = new Ext.form.ComboBox
    (
        {
			x: 130,
			y: 90,
            flex: 1,
			id: 'cbo_PetugasGiziGzPenerimaanDiet',
			valueField: 'kd_petugas',
            displayField: 'petugas',
			store: ds_ahliGiziGzPenerimaanDiet,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:150,
			tabIndex:2,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_PetugasGiziGzPenerimaanDiet;
};

function ComboVendorGzPenerimaanDiet()
{
    var Field_VendorGzPenerimaanDiet = ['kd_vendor', 'vendor'];
    ds_VendorGzPenerimaanDiet = new WebApp.DataStore({fields: Field_VendorGzPenerimaanDiet});
    ds_VendorGzPenerimaanDiet.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'kd_vendor',
					Sortdir: 'ASC',
					target: 'ComboVendorGizi',
					param: ''
				}
		}
	);
	
    var cbo_vendorGzPenerimaanDiet = new Ext.form.ComboBox
    (
        {
			x: 420,
			y: 0,
            flex: 1,
			id: 'cbo_vendorGzPenerimaanDiet',
			valueField: 'kd_vendor',
            displayField: 'vendor',
			store: ds_VendorGzPenerimaanDiet,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			readOnly:true,
			width:150,
			tabIndex:2,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_vendorGzPenerimaanDiet;
};


function datainit_viGzPenerimaanDiet(rowdata)
{
	var tgl_terima2 = rowdata.tgl_terima.split(" ");
	
	Ext.getCmp('txtNoPenerimaanGzPenerimaanDietL').setValue(rowdata.no_terima);
	Ext.getCmp('dfTglTerimaGzPenerimaanDietL').setValue(tgl_terima2[0]);
	Ext.getCmp('cbo_PetugasGiziGzPenerimaanDiet').setValue(rowdata.petugas);
	Ext.getCmp('cbo_vendorGzPenerimaanDiet').setValue(rowdata.vendor);
	Ext.getCmp('txtKetGZPenerimaanL').setValue(rowdata.keterangan);
	gzPenerimaanOrder.form.ComboBox.noOrder.setValue(rowdata.no_order);
	getGridLoadDetailPenerimaanOrder(rowdata.no_terima);
	//cekDistribusi(rowdata.no_terima);
};


function dataGriAwal(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionPenerimaanOrder/getDataGridAwal",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorgzPenerimaanOrder('Error, membaca data grid awal! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dataSource_viGzPenerimaanDiet.removeAll();
					var recs=[],
						recType=dataSource_viGzPenerimaanDiet.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
						dataSource_viGzPenerimaanDiet.add(recs);
					
					
					
					GridDataView_viGzPenerimaanDiet.getView().refresh();
				}
				else 
				{
					ShowPesanErrorgzPenerimaanOrder('Gagal membaca data grid awal', 'Error');
				};
			}
		}
		
	)
	
}

function cekDataOrder(no_order){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionPenerimaanOrder/cekTerima",
			params: {no_order:no_order},
			failure: function(o)
			{
				ShowPesanErrorgzPenerimaanOrder('Error, cek data order! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				
				if (cst.success === true) 
				{
					getGridDetailPenerimaanOrder(no_order);
				}
				else 
				{
					Ext.getCmp('btnSimpan_viGzPenerimaanDiet').disable();
					Ext.getCmp('btnSimpanExit_viGzPenerimaanDiet').disable();
					ShowPesanErrorgzPenerimaanOrder('No order ini sudah pernah diterima, harap periksa kembali no order', 'Error');
					
				};
			}
		}
	)
}

function cekDistribusi(no_terima){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionPenerimaanOrder/cekDistribusi",
			params: {no_terima:no_terima},
			failure: function(o)
			{
				ShowPesanErrorgzPenerimaanOrder('Error, cek data order! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				
				if (cst.success === true) 
				{
					Ext.getCmp('btnSimpan_viGzPenerimaanDiet').enable();
					Ext.getCmp('btnSimpanExit_viGzPenerimaanDiet').enable();
				}
				else 
				{
					Ext.getCmp('txtNoPenerimaanGzPenerimaanDietL').disable();
					Ext.getCmp('dfTglTerimaGzPenerimaanDietL').disable();
					Ext.getCmp('cbo_PetugasGiziGzPenerimaanDiet').disable();
					Ext.getCmp('cbo_vendorGzPenerimaanDiet').disable();
					Ext.getCmp('txtKetGZPenerimaanL').disable();
					gzPenerimaanOrder.form.ComboBox.noOrder.disable();
					Ext.getCmp('btnSimpan_viGzPenerimaanDiet').disable();
					Ext.getCmp('btnSimpanExit_viGzPenerimaanDiet').disable();
					ShowPesanInfogzPenerimaanOrder('No permintaan ini sudah diDistribusikan', 'Information');
					
				};
			}
		}
	)
}


function getGridDetailPenerimaanOrder(no_order){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionPenerimaanOrder/getGridPenerimaan",
			params: {no_order:no_order},
			failure: function(o)
			{
				ShowPesanErrorgzPenerimaanOrder('Error, membaca data order! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dsDataGrdDetailOrder_viGzPenerimaanDiet.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
					dsDataGrdDetailOrder_viGzPenerimaanDiet.add(recs);
					
					gzPenerimaanOrder.form.Grid.a.getView().refresh();
				}
				else 
				{
					ShowPesanErrorgzPenerimaanOrder('Gagal membaca Data ini', 'Error');
				};
			}
		}
	)
}


function getGridLoadDetailPenerimaanOrder(no_terima){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionPenerimaanOrder/getGridLoadPenerimaan",
			params: {no_terima:no_terima},
			failure: function(o)
			{
				ShowPesanErrorgzPenerimaanOrder('Error, membaca data order! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dsDataGrdDetailOrder_viGzPenerimaanDiet.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						//hasilJumlah();
					}
					dsDataGrdDetailOrder_viGzPenerimaanDiet.add(recs);
					hasilJumlah();
					gzPenerimaanOrder.form.Grid.a.getView().refresh();
					
				}
				else 
				{
					ShowPesanErrorgzPenerimaanOrder('Gagal membaca Data ini', 'Error');
				};
			}
		}
	)
}

function datasave_viGzPenerimaanDiet(){
	if (ValidasiEntrygzPenerimaanOrder(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/gizi/functionPenerimaanOrder/save",
				params: getParamgzPenerimaanOrder(),
				failure: function(o)
				{
					ShowPesanErrorgzPenerimaanOrder('Error, simpan data! Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfogzPenerimaanOrder('Data berhasil disimpan','Information');
						Ext.getCmp('txtNoPenerimaanGzPenerimaanDietL').setValue(cst.noterima);
						dataGriAwal();
						
						Ext.getCmp('btnSimpan_viGzPenerimaanDiet').disable();
						Ext.getCmp('btnSimpanExit_viGzPenerimaanDiet').disable();
						
					}
					else 
					{
						if(cst.error =='terima'){
							ShowPesanErrorgzPenerimaanOrder('Gagal Menyimpan Data ini. Error simpan terima', 'Error');
						} else if(cst.error =='update'){
							ShowPesanErrorgzPenerimaanOrder('Gagal Menyimpan Data ini. Error simpan, update terima detail', 'Error');
						} else if(cst.error =='distribusi'){
							Ext.getCmp('txtNoPenerimaanGzPenerimaanDietL').disable();
							Ext.getCmp('dfTglTerimaGzPenerimaanDietL').disable();
							Ext.getCmp('cbo_PetugasGiziGzPenerimaanDiet').disable();
							Ext.getCmp('cbo_vendorGzPenerimaanDiet').disable();
							Ext.getCmp('txtKetGZPenerimaanL').disable();
							gzPenerimaanOrder.form.ComboBox.noOrder.disable();
							Ext.getCmp('btnSimpan_viGzPenerimaanDiet').disable();
							Ext.getCmp('btnSimpanExit_viGzPenerimaanDiet').disable();
							ShowPesanInfogzPenerimaanOrder('No permintaan ini sudah diDistribusikan', 'Information');
						} else{
							ShowPesanErrorgzPenerimaanOrder('Gagal Menyimpan Data ini. Data order diet ini sudah diterima', 'Error');
						}
						
					};
				}
			}
			
		)
	}
}

function printbill()
{
    Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/main/CreateDataObj",
			params: datacetakbill(),
			failure: function(o)
			{	
				ShowPesanErrorgzPenerimaanOrder('Error hubungi admin' ,'Error');
			},
			success: function(o)
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					
				}
				else if  (cst.success === false && cst.pesan===0)
				{
					ShowPesanWarninggzPenerimaanOrder('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
				}
				else
				{
					ShowPesanErrorgzPenerimaanOrder('Gagal print '  + 'Error');
				}
			}
		}
	)
}


function getParamgzPenerimaanOrder() 
{
    var params =
	{
		NoTerima:Ext.getCmp('txtNoPenerimaanGzPenerimaanDietL').getValue(),
		NoOrder:gzPenerimaanOrder.form.ComboBox.noOrder.getValue(),		
		KdVendor: Ext.getCmp('cbo_vendorGzPenerimaanDiet').getValue(),
		KdPetugas:Ext.getCmp('cbo_PetugasGiziGzPenerimaanDiet').getValue(),
		TglTerima:Ext.getCmp('dfTglTerimaGzPenerimaanDietL').getValue(),
		Ket:Ext.getCmp('txtKetGZPenerimaanL').getValue()
		
	};
	
	params['jumlah']=dsDataGrdDetailOrder_viGzPenerimaanDiet.getCount();
	for(var i = 0 ; i < dsDataGrdDetailOrder_viGzPenerimaanDiet.getCount();i++)
	{
		params['no_order-'+i]=dsDataGrdDetailOrder_viGzPenerimaanDiet.data.items[i].data.no_order;
		params['no_minta-'+i]=dsDataGrdDetailOrder_viGzPenerimaanDiet.data.items[i].data.no_minta;
		params['kd_jenis-'+i]=dsDataGrdDetailOrder_viGzPenerimaanDiet.data.items[i].data.kd_jenis;
		params['kd_waktu-'+i]=dsDataGrdDetailOrder_viGzPenerimaanDiet.data.items[i].data.kd_waktu;
		params['qty-'+i]=dsDataGrdDetailOrder_viGzPenerimaanDiet.data.items[i].data.qty;
		params['qty_order-'+i]=dsDataGrdDetailOrder_viGzPenerimaanDiet.data.items[i].data.qty_order;
		params['harga_beli-'+i]=dsDataGrdDetailOrder_viGzPenerimaanDiet.data.items[i].data.harga_beli;
	}
	
    return params
};


function datacetakbill(){
	var params =
	{
		Table: 'billprintinggzPenerimaanOrder',
		NoResep:Ext.getCmp('txtNoResepGzPenerimaanDietL').getValue(),
		NoOut:Ext.getCmp('txtTmpNooutgzPenerimaanOrderL').getValue(),
		TglOut:Ext.getCmp('txtTmpTgloutgzPenerimaanOrderL').getValue(),
		KdPasien:Ext.getCmp('txtKdPasienGzPenerimaanDietL').getValue(),
		NamaPasien:gzPenerimaanOrder.form.ComboBox.namaPasien.getValue(),
		JenisPasien:Ext.get('cboPilihankelompokPasienAptgzPenerimaanOrder').getValue(),
		Kelas:Ext.get('cbo_UnitgzPenerimaanOrderL').getValue(),
		Dokter:Ext.get('cbo_DokterGzPenerimaanDiet').getValue(),
		Total:Ext.get('txtTotalBayargzPenerimaanOrderL').getValue(),
		Tot:toInteger(Ext.get('txtTotalBayargzPenerimaanOrderL').getValue())
		
	}
	params['jumlah']=dsDataGrdDetailOrder_viGzPenerimaanDiet.getCount();
	for(var i = 0 ; i < dsDataGrdDetailOrder_viGzPenerimaanDiet.getCount();i++)
	{
		params['nama_obat-'+i]=dsDataGrdDetailOrder_viGzPenerimaanDiet.data.items[i].data.nama_obat;
		params['jml-'+i]=dsDataGrdDetailOrder_viGzPenerimaanDiet.data.items[i].data.jml;
	}
	
    return params
}

function getCriteriaCariGzPenerimaanDiet()//^^^
{
      	 var strKriteria = "";

			if (Ext.get('TxtNoPenerimaanFilterGridDataView_viGzPenerimaanDiet').getValue() != "")
            {
                strKriteria = " upper(t.no_terima) " + "LIKE upper('" + Ext.get('TxtNoPenerimaanFilterGridDataView_viGzPenerimaanDiet').getValue() +"%')";
            }
            
            if (Ext.get('txtOrderGZPenerimaanDiet').getValue() != "")//^^^
            {
				if (strKriteria == "")
				{
					strKriteria = " lower(t.no_order) " + "LIKE lower('" + Ext.get('txtOrderGZPenerimaanDiet').getValue() +"%')" ;
				}
				else {
					strKriteria += " lower(t.no_order) " + "LIKE lower('" + Ext.get('txtOrderGZPenerimaanDiet').getValue() +"%')";
				}
            }
			
			if (Ext.get('dfTglAwalPenerimaanGzPenerimaanDiet').getValue() != "" && Ext.get('dfTglAkhirPenerimaanGzPenerimaanDiet').getValue() != "")
            {
                if (strKriteria == "")
				{
					strKriteria = " t.tgl_terima between '" + Ext.get('dfTglAwalPenerimaanGzPenerimaanDiet').getValue() + "' and '" + Ext.get('dfTglAkhirPenerimaanGzPenerimaanDiet').getValue() + "'" ;
				}
				else {
					strKriteria += " and t.tgl_terima between '" + Ext.get('dfTglAwalPenerimaanGzPenerimaanDiet').getValue() + "' and '" + Ext.get('dfTglAkhirPenerimaanGzPenerimaanDiet').getValue() + "'" ;
				}
                
            }
	
		strKriteria= strKriteria + " ORDER BY t.no_terima limit 50"
	 return strKriteria;
}


function ValidasiEntrygzPenerimaanOrder(modul,mBolHapus)
{
	var x = 1;
	if(gzPenerimaanOrder.form.ComboBox.noOrder.getValue() === '' || dsDataGrdDetailOrder_viGzPenerimaanDiet.getCount() === 0 || Ext.getCmp('cbo_PetugasGiziGzPenerimaanDiet').getValue() === ''){
		if(gzPenerimaanOrder.form.ComboBox.noOrder.getValue() === ''){
			ShowPesanWarninggzPenerimaanOrder('No order tidak boleh kosong', 'Warning');
			x = 0;
		} else if(dsDataGrdDetailOrder_viGzPenerimaanDiet.getCount() == 0){
			ShowPesanWarninggzPenerimaanOrder('Daftar terima order tidak boleh kosong', 'Warning');
			x = 0;
		} else {
			ShowPesanWarninggzPenerimaanOrder('Petugas gizi tidak boleh kosong', 'Warning');
			x = 0;
		} 
	}
	
	for(var i=0; i<dsDataGrdDetailOrder_viGzPenerimaanDiet.getCount() ; i++){
		var o=dsDataGrdDetailOrder_viGzPenerimaanDiet.getRange()[i].data;
		if(o.qty == undefined || o.harga_beli == undefined){
			if(o.qty == undefined){
				ShowPesanWarninggzPenerimaanOrder('Qty belum di isi, qty tidak boleh kosong', 'Warning');
				x = 0;
			}else if(o.harga_beli == undefined){
				ShowPesanWarninggzPenerimaanOrder('Harga beli tidak boleh kosong', 'Warning');
				x = 0;
			}
		}
		if(o.qty > o.qty_order){
			ShowPesanWarninggzPenerimaanOrder('Qty melebihi qty order', 'Warning');
			o.qty=o.qty_order;
			x = 0;
		}
	}
	
	return x;
};


function hasilJumlah(){
	var total=0;
	
	for(var i=0; i < dsDataGrdDetailOrder_viGzPenerimaanDiet.getCount() ; i++){
		var jumlahGrid=0;
		var subTotal=0;
	
		var o=dsDataGrdDetailOrder_viGzPenerimaanDiet.getRange()[i].data;
		if(o.qty != undefined){
			if(o.qty <= o.qty_order){
				subTotal=parseFloat(o.qty) * parseFloat(o.harga_beli);
				o.total=subTotal;
				
				total +=subTotal;
				if(total == NaN){
					total=0;
				} else{
					total=total;
				}
			} else{
				ShowPesanWarninggzPenerimaanOrder('Qty terima lebih dari qty order','Warning');
				o.qty=o.qty_order;
			}
		}
	}
	
	Ext.getCmp('txtGrandTot').setValue(toFormat(total));
	
	gzPenerimaanOrder.form.Grid.a.getView().refresh();
}


function ShowPesanWarninggzPenerimaanOrder(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorgzPenerimaanOrder(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:290
		}
	);
};


function ShowPesanInfogzPenerimaanOrder(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};