var dataSource_viGzPenerimaanBahanMakanan;
var selectCount_viGzPenerimaanBahanMakanan=50;
var NamaForm_viGzPenerimaanBahanMakanan="Penerimaan Bahan Makanan";
var selectCountStatusPostingGzPenerimaanBahanMakanan='Semua';
var mod_name_viGzPenerimaanBahanMakanan="viGzPenerimaanBahanMakanan";
var now_viGzPenerimaanBahanMakanan= new Date();
var addNew_viGzPenerimaanBahanMakanan;
var rowSelected_viGzPenerimaanBahanMakanan;
var setLookUps_viGzPenerimaanBahanMakanan;
var tanggal = now_viGzPenerimaanBahanMakanan.format("d/M/Y");
var jam = now_viGzPenerimaanBahanMakanan.format("H/i/s");
var tampungshiftsekarang;
var tmpkriteria;
var NO_MINTA_VAR;
var KODE_BAHAN_VAR;
var QTY_VAR;


var CurrentData_viGzPenerimaanBahanMakanan =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viGzPenerimaanBahanMakanan(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

var GzPenerimaanBahanMakanan={};
GzPenerimaanBahanMakanan.form={};
GzPenerimaanBahanMakanan.func={};
GzPenerimaanBahanMakanan.vars={};
GzPenerimaanBahanMakanan.func.parent=GzPenerimaanBahanMakanan;
GzPenerimaanBahanMakanan.form.ArrayStore={};
GzPenerimaanBahanMakanan.form.ComboBox={};
GzPenerimaanBahanMakanan.form.DataStore={};
GzPenerimaanBahanMakanan.form.Record={};
GzPenerimaanBahanMakanan.form.Form={};
GzPenerimaanBahanMakanan.form.Grid={};
GzPenerimaanBahanMakanan.form.Panel={};
GzPenerimaanBahanMakanan.form.TextField={};
GzPenerimaanBahanMakanan.form.Button={};

GzPenerimaanBahanMakanan.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['no_minta','kd_bahan','nama_bahan', 'kd_sataun','satuan','qty','ket_spek','quantity'],
	data: []
});

function dataGrid_viGzPenerimaanBahanMakanan(mod_id_viGzPenerimaanBahanMakanan){	
    var FieldMaster_viGzPenerimaanBahanMakanan = 
	[
		'NO_TERIMA','TGL_TERIMA','KETERANGAN','POSTED','KD_BAHAN','NO_MINTA','QTY','NAMA_BAHAN','KD_SATUAN','SATUAN','POSTED'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viGzPenerimaanBahanMakanan = new WebApp.DataStore
	({
        fields: FieldMaster_viGzPenerimaanBahanMakanan
    });
    refreshGzPenerimaanBahanMakanan();
    // Grid Apotek Perencanaan # --------------
	var GridDataView_viGzPenerimaanBahanMakanan = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viGzPenerimaanBahanMakanan,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viGzPenerimaanBahanMakanan = undefined;
							rowSelected_viGzPenerimaanBahanMakanan = dataSource_viGzPenerimaanBahanMakanan.getAt(row);
							CurrentData_viGzPenerimaanBahanMakanan
							CurrentData_viGzPenerimaanBahanMakanan.row = row;
							CurrentData_viGzPenerimaanBahanMakanan.data = rowSelected_viGzPenerimaanBahanMakanan.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viGzPenerimaanBahanMakanan = dataSource_viGzPenerimaanBahanMakanan.getAt(ridx);
					if (rowSelected_viGzPenerimaanBahanMakanan != undefined)
					{
						setLookUp_viGzPenerimaanBahanMakanan(rowSelected_viGzPenerimaanBahanMakanan.data);
					}
					else
					{
						setLookUp_viGzPenerimaanBahanMakanan();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Apotek perencanaan
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					/*{
						header		: 'Status Posting',
						width		: 20,
						sortable	: false,
						hideable	: false,
						hidden		: false,
						menuDisabled: true,
						dataIndex	: 'APPROVE',
						id			: 'colStatusPosting_viGzPenerimaanBahanMakanan',
						renderer	: function(value, metaData, record, rowIndex, colIndex, store){
							 switch (value){
								 case 't':
									 metaData.css = 'StatusHijau'; 
									 break;
								 case 'f':
									 metaData.css = 'StatusMerah';
									 break;
							 }
							 return '';
						}
					},*/
					{
						header: 'No. Penerimaan',
						dataIndex: 'NO_TERIMA',
						hideable:false,
						menuDisabled: true,
						width: 25
						
					},
					//-------------- ## --------------
					{
						header: 'Tanggal Terima',
						dataIndex: 'TGL_TERIMA',
						hideable:false,
						menuDisabled: true,
						width: 30,
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.TGL_TERIMA);
						}
					},
					{
						header: 'Keterangan',
						dataIndex: 'KETERANGAN',
						hideable:false,
						menuDisabled: true,
						width: 30
						
					}
					//-------------- ## --------------
					
					//-------------- ## --------------
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viGzPenerimaanBahanMakanan',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Add Data',
						id: 'btnTambah_viGzPenerimaanBahanMakanan',
						handler: function(sm, row, rec)
						{
							setLookUp_viGzPenerimaanBahanMakanan();
						}
					},
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viGzPenerimaanBahanMakanan',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viGzPenerimaanBahanMakanan != undefined)
							{
								setLookUp_viGzPenerimaanBahanMakanan(rowSelected_viGzPenerimaanBahanMakanan.data)
							}
							
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viGzPenerimaanBahanMakanan, selectCount_viGzPenerimaanBahanMakanan, dataSource_viGzPenerimaanBahanMakanan),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )
	
	var pencarianGzPenerimaanBahanMakanan = new Ext.FormPanel({
        labelAlign: 'top',
        frame:true,
        title: '',
        bodyStyle:'padding:5px 5px 0',
        //width: 600,
        items: [
		{
			layout: 'column',
			border: false,
			items:
			[
				{
					columnWidth:.98,
					layout: 'absolute',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					width: 500,
					height: 70,
					anchor: '100% 100%',
					items:
					[
						{
							x: 10,
							y: 0,
							xtype: 'label',
							text: 'No. Penerimaan'
						},
						{
							x: 120,
							y: 0,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 0,
							xtype: 'textfield',
							id: 'TxtFilterGridNoPenerimaanGzPenerimaanBahanMakanan',
							name: 'TxtFilterGridNoPenerimaanGzPenerimaanBahanMakanan',
							width: 350,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariGzPenerimaanBahanMakanan();
										refreshGzPenerimaanBahanMakanan(tmpkriteria);
									} 						
								}
							}
						},
						//----------------------------------------
						{
							x: 483,
							y: 0,
							xtype: 'label',
							text: '* Enter untuk mencari'
						},
						//----------------------------------------
						{
							x: 580,
							y: 30,
							xtype: 'button',
							text: 'Refresh',
							iconCls: 'refresh',
							tooltip: 'Refresh',
							style:{paddingLeft:'30px'},
							width:150,
							id: 'BtnFilterGridCari_viGzPenerimaanBahanMakanan',
							handler: function() 
							{
								
								tmpkriteria = getCriteriaCariGzPenerimaanBahanMakanan();					
								refreshGzPenerimaanBahanMakanan(tmpkriteria);
							}                        
						},
						{
							x: 10,
							y: 30,
							xtype: 'label',
							text: 'Tanggal'
						},
						{
							x: 120,
							y: 30,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 30,
							xtype: 'datefield',
							id: 'dfTglAwalMintaGzPenerimaanBahanMakanan',
							format: 'd/M/Y',
							value:now_viGzPenerimaanBahanMakanan,
							width: 150,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariGzPenerimaanBahanMakanan();
										refreshGzPenerimaanBahanMakanan(tmpkriteria);
									} 						
								}
							}
						},
						{
							x: 290,
							y: 30,
							xtype: 'label',
							text: 's/d'
						},
						{
							x: 320,
							y: 30,
							xtype: 'datefield',
							id: 'dfTglAkhirMintaGzPenerimaanBahanMakanan',
							format: 'd/M/Y',
							value:now_viGzPenerimaanBahanMakanan,
							width: 150,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariGzPenerimaanBahanMakanan();
										refreshGzPenerimaanBahanMakanan(tmpkriteria);
									} 						
								}
							}
						}
					]
				}
			]
		}
		]	
		
	})

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viGzPenerimaanBahanMakanan = new Ext.Panel
    (
		{
			title: NamaForm_viGzPenerimaanBahanMakanan,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viGzPenerimaanBahanMakanan,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ pencarianGzPenerimaanBahanMakanan,
					GridDataView_viGzPenerimaanBahanMakanan],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viGzPenerimaanBahanMakanan,
		            columns: 21,
		            defaults: {
					scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viGzPenerimaanBahanMakanan;
    //-------------- # End form filter # --------------
}

function refreshGzPenerimaanBahanMakanan(kriteria)
{
    dataSource_viGzPenerimaanBahanMakanan.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'ViewGzPenerimaanBahanMakanan',
                    param : kriteria
                }			
            }
        );   
    return dataSource_viGzPenerimaanBahanMakanan;
}

function setLookUp_viGzPenerimaanBahanMakanan(rowdata){
    var lebar = 785;
    setLookUps_viGzPenerimaanBahanMakanan = new Ext.Window({
        id: Nci.getId(),
        title: NamaForm_viGzPenerimaanBahanMakanan, 
        closeAction: 'destroy',        
        width: 700,
        height: 555,
		constrain:true,
		autoHeight:true,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viGzPenerimaanBahanMakanan(lebar,rowdata),
        listeners:{
            activate: function(){
				
            },
            afterShow: function(){
                this.activate();
            },
            deactivate: function(){
                rowSelected_viGzPenerimaanBahanMakanan=undefined;
            }
        }
    });

    setLookUps_viGzPenerimaanBahanMakanan.show();

    if (rowdata == undefined){
	
    }
    else
    {
        datainit_viGzPenerimaanBahanMakanan(rowdata);
    }
}

function getFormItemEntry_viGzPenerimaanBahanMakanan(lebar,rowdata){
    var pnlFormDataBasic_viGzPenerimaanBahanMakanan = new Ext.FormPanel({
		title: '',
		region: 'north',
		layout: 'form',
		bodyStyle: 'padding:10px 10px 10px 10px',
		anchor: '100%',
		labelWidth: 1,
		autoWidth: true,
		width: lebar,
		border: false,
		items:[
				getItemPanelInputData_viGzPenerimaanBahanMakanan(lebar),
				getItemGridDetail_viGzPenerimaanBahanMakanan(lebar)
			],
			fileUpload: true,
		tbar: {
				xtype: 'toolbar',
				items: 
				[
					/*{
						xtype: 'button',
						text: 'Add New',
						disabled: false,
						iconCls: 'add',
						id: 'btnAdd_viGzPenerimaanBahanMakanan',
						handler: function(){
							dataaddnew_viGzPenerimaanBahanMakanan();
						}
					},
					{
						xtype: 'tbseparator'
					},*/
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viGzPenerimaanBahanMakanan',
						handler: function()
						{
							datasave_viGzPenerimaanBahanMakanan();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save & Close',
						disabled: false,
						iconCls: 'saveexit',
						id: 'btnSimpanExit_viGzPenerimaanBahanMakanan',
						handler: function()
						{
							datasave_viGzPenerimaanBahanMakanan();
							refreshGzPenerimaanBahanMakanan();
							setLookUps_viGzPenerimaanBahanMakanan.close();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Posting',
						disabled: false,
						iconCls: 'gantidok',
						id: 'btnPosting_viGzPenerimaanBahanMakanan',
						handler: function()
						{
							dataposting_viGzPenerimaanBahanMakanan();
						}
					},
					{
						xtype: 'tbseparator'
					}
					
				]
			}
		}
    )

    return pnlFormDataBasic_viGzPenerimaanBahanMakanan;
}

function getItemPanelInputData_viGzPenerimaanBahanMakanan(lebar) {
    var items =
	{
		title:'',
		layout:'column',
		items:
		[
			{
				columnWidth: .50,
				layout: 'form',
				labelWidth:110,
				bodyStyle:'padding: 10px',
				border: false,
				items:
				[
					{
						xtype: 'textfield',
						fieldLabel: 'No. Penerimaan ',
						name: 'txtNoPenerimaan_GzPenerimaanBahanMakananL',
						id: 'txtNoPenerimaan_GzPenerimaanBahanMakananL',
						readOnly:true,
						tabIndex:0,
						width: 150
					},
					{
						xtype: 'textfield',
						fieldLabel: 'posted',
						name: 'txtposted',
						id: 'txtposted',
						readOnly:true,
						hidden:true,
						disbale:true,
						width: 150
					},
					{
						xtype: 'datefield',
						fieldLabel: 'Tanggal Terima',
						name: 'dfTglTerima_GzPenerimaanBahanMakananL',
						id	: 'dfTglTerima_GzPenerimaanBahanMakananL',
						format: 'd/M/Y',
						value:now_viGzPenerimaanBahanMakanan,
						tabIndex:1,
						width: 150
					}
				]
			},
			{
				columnWidth: .50,
				layout: 'form',
				labelWidth:80,
				bodyStyle:'padding: 10px',
				border: false,
				items:
				[
					{
						xtype: 'textarea',
						fieldLabel: 'Keterangan ',
						name: 'txtKeteranan_GzPenerimaanBahanMakananL',
						id: 'txtKeteranan_GzPenerimaanBahanMakananL',
						width : 200,
						height : 50,
						tabIndex:3
					}
				]
			}
		
		]
	};
    return items;
};

function getItemGridDetail_viGzPenerimaanBahanMakanan(lebar) 
{
    var items =
	{
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:1px 1px 1px 1px',
		border:true,
		width: lebar-80,
		height: 250,//225, 
		tbar:
		[
			{
				text	: 'Add Bahan',
				id		: 'btnAddPermintaanBahan',
				tooltip	: nmLookup,
				disabled: false,
				iconCls	: 'find',
				handler	: function(){
					var records = new Array();
					records.push(new dsDataGrdJab_viGzPenerimaanBahanMakanan.recordType());
					dsDataGrdJab_viGzPenerimaanBahanMakanan.add(records);
					
					
				}
			},	
			{
				text	: 'Delete',
				id		: 'btnHapusPermintaanBahanGzPenerimaanBahanMakanan',
				tooltip	: nmLookup,
				iconCls	: 'remove',
				disabled: false,
				handler	: function(){
					Ext.Msg.confirm('Hapus bahan', 'Apakah bahan ini akan dihapus?', function(button){
						if (button == 'yes'){
							var line = GzPenerimaanBahanMakanan.form.Grid.a.getSelectionModel().selection.cell[0];
							var o = dsDataGrdJab_viGzPenerimaanBahanMakanan.getRange()[line].data;
							KODE_BAHAN_VAR = o.kd_bahan;
							NO_MINTA_VAR = o.no_minta;
							if(dsDataGrdJab_viGzPenerimaanBahanMakanan.getCount() > 1){
								dsDataGrdJab_viGzPenerimaanBahanMakanan.removeAt(line);
								dataDelete_viGzPenerimaanBahanMakanan();
								GzPenerimaanBahanMakanan.form.Grid.a.getView().refresh();
							} else{
								ShowPesanErrorGzPenerimaanBahanMakanan('Data tidak bisa dihapus karena minimal 1 bahan','Error');
							}
						}
					});
				}
			}	
		],
	    items:
		[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:
				[
					gridDataViewEdit_viGzPenerimaanBahanMakanan()
				]	
			}
			//-------------- ## --------------
		]
	};
    return items;
};

function gridDataViewEdit_viGzPenerimaanBahanMakanan()
{
    var FieldGrdKasir_viGzPenerimaanBahanMakanan = [];
	
    dsDataGrdJab_viGzPenerimaanBahanMakanan= new WebApp.DataStore
	({
        fields: FieldGrdKasir_viGzPenerimaanBahanMakanan
    });
    
    GzPenerimaanBahanMakanan.form.Grid.a =new Ext.grid.EditorGridPanel({
        store: dsDataGrdJab_viGzPenerimaanBahanMakanan,
        //store:dataSource_viGzPenerimaanBahanMakanan,
		height: 220,//220,
		stripeRows: true,
		columnLines: true,
		selModel: new Ext.grid.CellSelectionModel({
	            singleSelect: true,
	            listeners: {
	                rowselect: function(sm, row, rec)
	                {
						
	                }
	            }
        }),
        
        columns: 
		[	
			new Ext.grid.RowNumberer(),
			{			
				dataIndex: 'no_minta',
				header: 'No Permintaan',
				sortable: true,
				width: 100,
				editor:new Nci.form.Combobox.autoComplete({
					store	: GzPenerimaanBahanMakanan.form.ArrayStore.a,
					select	: function(a,b,c){
						var line	= GzPenerimaanBahanMakanan.form.Grid.a.getSelectionModel().selection.cell[0];
						dsDataGrdJab_viGzPenerimaanBahanMakanan.getRange()[line].data.no_minta=b.data.no_minta;
						dsDataGrdJab_viGzPenerimaanBahanMakanan.getRange()[line].data.nama_bahan=b.data.nama_bahan;
						dsDataGrdJab_viGzPenerimaanBahanMakanan.getRange()[line].data.kd_bahan=b.data.kd_bahan;
						dsDataGrdJab_viGzPenerimaanBahanMakanan.getRange()[line].data.kd_satuan=b.data.kd_satuan;
						dsDataGrdJab_viGzPenerimaanBahanMakanan.getRange()[line].data.satuan=b.data.satuan;
						dsDataGrdJab_viGzPenerimaanBahanMakanan.getRange()[line].data.quantity=b.data.qty;
						dsDataGrdJab_viGzPenerimaanBahanMakanan.getRange()[line].data.qty=0;
						
						GzPenerimaanBahanMakanan.form.Grid.a.getView().refresh();
					},
					insert	: function(o){
						return {
							no_minta        : o.no_minta,
							kd_bahan        : o.kd_bahan,
							nama_bahan 		: o.nama_bahan,
							kd_satuan		: o.kd_satuan,
							satuan			: o.satuan,
							qty				: o.qty,
							text			:  '<table style="font-size: 11px;"><tr><td width="70">'+o.no_minta+'</td><td width="200">'+o.nama_bahan+'</td><td width="40" align="right">'+o.satuan+'</td></tr></table>'
						}
					},
					url		: baseURL + "index.php/gizi/viewgzpenerimaanbahanmakanan/getPenerimaan",
					valueField: 'no_terima',
					displayField: 'text',
					listWidth: 200
				})
			},
			{			
				dataIndex: 'kd_bahan',
				header: 'Kode Bahan',
				sortable: true,
				width: 100
			},
			//-------------- ## --------------
			{			
				dataIndex: 'nama_bahan',
				header: 'Nama Bahan',
				sortable: true,
				width: 275,
				
			},
			//-------------- ## --------------
			{
				dataIndex: 'satuan',
				header: 'Satuan',
				sortable: true,
				align:'right',
				width: 110
			},
			//-------------- ## --------------
			{
				dataIndex: 'quantity',
				header: 'Qty Mintaan',
				sortable: true,
				align:'right',
				width: 80,
				editor: new Ext.form.NumberField({
					allowBlank: false,
					listeners:{
						blur: function(a){
							/* var line	= this.index;
							dsDataGrdJab_viGzPenerimaanBahanMakanan.getRange()[line].data.stok_opname=a.getValue(); */
							var line	= this.index;
							if(a.getValue() < 0){
								ShowPesanWarningGzPenerimaanBahanMakanan('Stok opname tidak boleh -', 'Warning');
							}
						},
						focus: function(a){
							this.index=GzPenerimaanBahanMakanan.form.Grid.a.getSelectionModel().selection.cell[0]
						}
					}
				})	
			},
			//-------------- ## --------------
			{
				dataIndex: 'ket_spek',
				header: 'Spesifikasi Khusus',
				sortable: true,
				align:'right',
				width: 130
			},
			//-------------- ## --------------
			{
				dataIndex: 'qty',
				header: 'Qty Terima',
				sortable: true,
				align:'right',
				width: 80,
				editor: new Ext.form.NumberField({
					allowBlank: false,
					listeners:{
						blur: function(a){
							/* var line	= this.index;
							dsDataGrdJab_viGzPenerimaanBahanMakanan.getRange()[line].data.stok_opname=a.getValue(); */
							var line	= this.index;
							if(a.getValue() < 0){
								ShowPesanWarningGzPenerimaanBahanMakanan('Stok opname tidak boleh -', 'Warning');
							}
						},
						focus: function(a){
							this.index=GzPenerimaanBahanMakanan.form.Grid.a.getSelectionModel().selection.cell[0]
						}
					}
				})	
			},

        ],
		viewConfig: 
			{
				forceFit: true
			}

       // plugins:chkSelected_viGzPenerimaanBahanMakanan,
    });
    return  GzPenerimaanBahanMakanan.form.Grid.a;
}

function dataaddnew_viGzPenerimaanBahanMakanan(){
	Ext.getCmp('txtNoPenerimaan_GzPenerimaanBahanMakananL').setValue('');
	Ext.getCmp('txtNoBA_GzPenerimaanBahanMakananL').setValue('');
	Ext.getCmp('txtPetugasBA_GzPenerimaanBahanMakananL').setValue('');
	dsDataGrdJab_viGzPenerimaanBahanMakanan.removeAll();
}

function datainit_viGzPenerimaanBahanMakanan(rowdata)
{
	//console.log(rowdata);
	if(rowdata.POSTED == '1'){
		Ext.getCmp('btnSimpan_viGzPenerimaanBahanMakanan').disable();
		Ext.getCmp('btnSimpanExit_viGzPenerimaanBahanMakanan').disable();
		Ext.getCmp('btnPosting_viGzPenerimaanBahanMakanan').disable();
		Ext.getCmp('btnAddPermintaanBahan').disable();
		Ext.getCmp('btnHapusPermintaanBahanGzPenerimaanBahanMakanan').disable();
	} else{
		Ext.getCmp('btnSimpan_viGzPenerimaanBahanMakanan').enable();
		Ext.getCmp('btnSimpanExit_viGzPenerimaanBahanMakanan').enable();
		Ext.getCmp('btnPosting_viGzPenerimaanBahanMakanan').enable();
		Ext.getCmp('btnAddPermintaanBahan').enable();
		Ext.getCmp('btnHapusPermintaanBahanGzPenerimaanBahanMakanan').enable();
	}

	Ext.getCmp('txtNoPenerimaan_GzPenerimaanBahanMakananL').setValue(rowdata.NO_TERIMA);
	Ext.getCmp('txtposted').setValue(rowdata.POSTED);
	Ext.getCmp('dfTglTerima_GzPenerimaanBahanMakananL').setValue(ShowDate(rowdata.TGL_TERIMA));
	//Ext.getCmp('txtNoBA_GzPenerimaanBahanMakananL').setValue(rowdata.NO_BA_SO);
	Ext.getCmp('txtKeteranan_GzPenerimaanBahanMakananL').setValue(rowdata.KETERANGAN);
	dataGridObatGzPenerimaanBahanMakanan(rowdata.NO_TERIMA);
	
};

function datasave_viGzPenerimaanBahanMakanan(){
	if (ValidasiEntryGzPenerimaanBahanMakanan(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/gizi/viewgzpenerimaanbahanmakanan/save",
				params: getParamGzPenerimaanBahanMakanan(),
				failure: function(o)
				{
					ShowPesanErrorGzPenerimaanBahanMakanan('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					
					var cst = Ext.decode(o.responseText);
					
					if (cst.success === true) 
					{
						console.log(cst);
						ShowPesanInfoGzPenerimaanBahanMakanan(nmPesanSimpanSukses,nmHeaderSimpanData);
						Ext.getCmp('txtNoPenerimaan_GzPenerimaanBahanMakananL').setValue(cst.no_terima);
						refreshGzPenerimaanBahanMakanan();
					}
					else 
					{
						ShowPesanErrorGzPenerimaanBahanMakanan('Gagal Menyimpan Data ini', 'Error');
						refreshGzPenerimaanBahanMakanan();
					};
				}
			}
			
		)
	}
	
}

function dataDelete_viGzPenerimaanBahanMakanan(){
	//if (ValidasiEntryGzPenerimaanBahanMakanan(nmHeaderSimpanData,false) == 1 )
//	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/gizi/viewgzpenerimaanbahanmakanan/delete",
				params: getParamGzPenerimaanBahanMakananDelete(),
				failure: function(o)
				{
					ShowPesanErrorGzPenerimaanBahanMakanan('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoGzPenerimaanBahanMakanan("Data Berhasil DiHapus",nmHeaderSimpanData);
						refreshGzPenerimaanBahanMakanan();
					}
					else 
					{
						ShowPesanErrorGzPenerimaanBahanMakanan('Gagal Menghapus Data ini', 'Error');
						refreshGzPenerimaanBahanMakanan();
					};
				}
			}
			
		)
	//}
}

function dataposting_viGzPenerimaanBahanMakanan(){
	Ext.Msg.confirm('Warning', 'Apakah data ini akan diPosting? Pastikan semua data sudah benar sebelum diPosting', function(button){
		if (button == 'yes'){
			Ext.Ajax.request
			(
				{
					url: baseURL + "index.php/gizi/viewgzpenerimaanbahanmakanan/posting",
					params: getParamGzPenerimaanBahanMakananPosted(),
					failure: function(o)
					{
						ShowPesanErrorResepRWI('Hubungi Admin', 'Error');
					},	
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoGzPenerimaanBahanMakanan('Posting Berhasil','Information');
				 			Ext.getCmp('btnSimpan_viGzPenerimaanBahanMakanan').disable();
							Ext.getCmp('btnSimpanExit_viGzPenerimaanBahanMakanan').disable();
							Ext.getCmp('btnPosting_viGzPenerimaanBahanMakanan').disable();
							Ext.getCmp('btnAddPermintaanBahan').disable();
							Ext.getCmp('btnHapusPermintaanBahanGzPenerimaanBahanMakanan').disable();
							refreshGzPenerimaanBahanMakanan();
						}
						else 
						{
							ShowPesanErrorResepRWI('Gagal melakukan Posting', 'Error');
						};
					}
				}
				
			)
		}
	});
}

function dataGridObatGzPenerimaanBahanMakanan(no_terima){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/viewgzpenerimaanbahanmakanan/getDetailPenerimaan",
			params: {query:no_terima},
			failure: function(o)
			{
				ShowPesanErrorReturRWJ('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dsDataGrdJab_viGzPenerimaanBahanMakanan.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
						dsDataGrdJab_viGzPenerimaanBahanMakanan.add(recs);
					
					
					
					GzPenerimaanBahanMakanan.form.Grid.a.getView().refresh();
				}
				else 
				{
					ShowPesanErrorReturRWJ('Gagal membaca data obat', 'Error');
				};
			}
		}
		
	)
	
}



function getParamGzPenerimaanBahanMakanan() 
{
	var	params =
	{
		no_terima:Ext.getCmp('txtNoPenerimaan_GzPenerimaanBahanMakananL').getValue(),
		tgl_terima:Ext.getCmp('dfTglTerima_GzPenerimaanBahanMakananL').getValue(),
		keterangan:Ext.getCmp('txtKeteranan_GzPenerimaanBahanMakananL').getValue(),
		posted:Ext.getCmp('txtposted').getValue()
	}
	
	params['jumlah']=dsDataGrdJab_viGzPenerimaanBahanMakanan.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viGzPenerimaanBahanMakanan.getCount();i++)
	{
		params['no_minta-'+i]=dsDataGrdJab_viGzPenerimaanBahanMakanan.data.items[i].data.no_minta
		params['kd_bahan-'+i]=dsDataGrdJab_viGzPenerimaanBahanMakanan.data.items[i].data.kd_bahan
		params['nama_bahan-'+i]=dsDataGrdJab_viGzPenerimaanBahanMakanan.data.items[i].data.nama_bahan
		params['satuan-'+i]=dsDataGrdJab_viGzPenerimaanBahanMakanan.data.items[i].data.satuan
		params['qty-'+i]=dsDataGrdJab_viGzPenerimaanBahanMakanan.data.items[i].data.qty
		params['spek-'+i]=dsDataGrdJab_viGzPenerimaanBahanMakanan.data.items[i].data.spek
		params['quantity-'+i]=dsDataGrdJab_viGzPenerimaanBahanMakanan.data.items[i].data.quantity
		params['urut-'+i]=i+1
		
	}
    return params
};

function getParamGzPenerimaanBahanMakananPosted()
{
	var params =
	{
		no_terima:Ext.getCmp('txtNoPenerimaan_GzPenerimaanBahanMakananL').getValue(),
		tgl_terima:Ext.getCmp('dfTglTerima_GzPenerimaanBahanMakananL').getValue(),
	}
	
     params['jumlah']=dsDataGrdJab_viGzPenerimaanBahanMakanan.getCount();
	for(var i = 0 ; i < dsDataGrdJab_viGzPenerimaanBahanMakanan.getCount();i++)
	{
		params['no_minta-'+i]=dsDataGrdJab_viGzPenerimaanBahanMakanan.data.items[i].data.no_minta
		params['kd_bahan-'+i]=dsDataGrdJab_viGzPenerimaanBahanMakanan.data.items[i].data.kd_bahan
		params['nama_bahan-'+i]=dsDataGrdJab_viGzPenerimaanBahanMakanan.data.items[i].data.nama_bahan
		params['satuan-'+i]=dsDataGrdJab_viGzPenerimaanBahanMakanan.data.items[i].data.satuan
		params['qty-'+i]=dsDataGrdJab_viGzPenerimaanBahanMakanan.data.items[i].data.qty
		params['spek-'+i]=dsDataGrdJab_viGzPenerimaanBahanMakanan.data.items[i].data.spek
		params['quantity-'+i]=dsDataGrdJab_viGzPenerimaanBahanMakanan.data.items[i].data.quantity
		params['urut-'+i]=i+1
		
	}
	return params;
}

function getParamGzPenerimaanBahanMakananDelete()
{
	var params =
	{
		no_minta : NO_MINTA_VAR,
		kd_bahan : KODE_BAHAN_VAR,
		no_terima : Ext.getCmp('txtNoPenerimaan_GzPenerimaanBahanMakananL').getValue()
	}
	return params;
}


function getCriteriaCariGzPenerimaanBahanMakanan()//^^^
{
	var strKriteria = "";
	var dawal = new Date(Ext.getCmp('dfTglAwalMintaGzPenerimaanBahanMakanan').getValue());
	var awal = dawal.format('Y-m-d');
	var dakhir = new Date(Ext.getCmp('dfTglAkhirMintaGzPenerimaanBahanMakanan').getValue());
	var akhir = dakhir.format('Y-m-d');

	if (Ext.get('TxtFilterGridNoPenerimaanGzPenerimaanBahanMakanan').getValue() != "" && Ext.get('TxtFilterGridNoPenerimaanGzPenerimaanBahanMakanan').getValue()!=='Nama/Kode obat')
	{
		strKriteria += " no_terima " + "LIKE '%" + Ext.get('TxtFilterGridNoPenerimaanGzPenerimaanBahanMakanan').getValue() +"%' ";
		strKriteria += " AND tgl_terima between '" + awal +"' AND '"+ akhir+"'";
	}else{
		strKriteria += "tgl_terima between '" + awal +"' AND '"+ akhir+"'";
	}

		
		
		strKriteria= strKriteria + " order by no_terima desc limit 30"
	return strKriteria;
}


function ValidasiEntryGzPenerimaanBahanMakanan(modul,mBolHapus)
{
	var x = 1;
	if(Ext.getCmp('dfTglTerima_GzPenerimaanBahanMakananL').getValue() === ''){
		ShowPesanWarningGzPenerimaanBahanMakanan('Tanggal masih kosong', 'Warning');
		x = 0;
	}
	if(dsDataGrdJab_viGzPenerimaanBahanMakanan.getCount() == 0)
	{
		ShowPesanWarningGzPenerimaanBahanMakanan('Bahan masih kosong', 'Warning');
		x = 0;
	}
	
	for(var i=0; i<dsDataGrdJab_viGzPenerimaanBahanMakanan.getCount() ; i++){
		var o=dsDataGrdJab_viGzPenerimaanBahanMakanan.getRange()[i].data;
		var tampKd="";
	
			if(o.no_minta == undefined){
				ShowPesanWarningGzPenerimaanBahanMakanan('Anda Belum Memasukan No. Permintaan', 'Warning');
				x = 0;
			}else if(o.kd_bahan == undefined){
				ShowPesanWarningGzPenerimaanBahanMakanan('Anda Belum Memasukan Bahan, Bahan tidak boleh kosong', 'Warning');
				x = 0;
			} else if(o.qty == 0){
				ShowPesanWarningGzPenerimaanBahanMakanan('Quantity Tidak Boleh Kosong', 'Warning');
				x = 0;
			}
	
		
		else if(o.qty > o.quantity){
			ShowPesanWarningGzPenerimaanBahanMakanan('Jumlah Penerimaan Tidak Boleh Lebih Besar dari Permintaan', 'Warning');
			x = 0;
		}
		
		for(var j=0; j<dsDataGrdJab_viGzPenerimaanBahanMakanan.getCount() ; j++){
			var p=dsDataGrdJab_viGzPenerimaanBahanMakanan.getRange()[j].data;
			if(i != j && o.kd_bahan == p.kd_bahan && o.no_minta == o.no_minta){
				ShowPesanWarningGzPenerimaanBahanMakanan('Tidak boleh ada Bahan yang sama, periksa kembali daftar obat', 'Warning');
				x = 0;
				break;
			}
		}
		
	}
	
	return x;
};


function ValidasiEntryGzPenerimaanBahanMakanan(modul,mBolHapus)
{
	var x = 1;
	if(Ext.getCmp('dfTglTerima_GzPenerimaanBahanMakananL').getValue() === ''){
		ShowPesanWarningGzPenerimaanBahanMakanan('Tanggal masih kosong', 'Warning');
		x = 0;
	}
	if(dsDataGrdJab_viGzPenerimaanBahanMakanan.getCount() == 0)
	{
		ShowPesanWarningGzPenerimaanBahanMakanan('Bahan masih kosong', 'Warning');
		x = 0;
	}
	
	for(var i=0; i<dsDataGrdJab_viGzPenerimaanBahanMakanan.getCount() ; i++){
		var o=dsDataGrdJab_viGzPenerimaanBahanMakanan.getRange()[i].data;
		var tampKd="";
	
			if(o.no_minta == undefined){
				ShowPesanWarningGzPenerimaanBahanMakanan('Anda Belum Memasukan No. Permintaan', 'Warning');
				x = 0;
			}else if(o.kd_bahan == undefined){
				ShowPesanWarningGzPenerimaanBahanMakanan('Anda Belum Memasukan Bahan, Bahan tidak boleh kosong', 'Warning');
				x = 0;
			} else if(o.qty == 0){
				ShowPesanWarningGzPenerimaanBahanMakanan('Quantity Tidak Boleh Kosong', 'Warning');
				x = 0;
			}
	
		
		else if(o.qty > o.quantity){
			ShowPesanWarningGzPenerimaanBahanMakanan('Jumlah Penerimaan Tidak Boleh Lebih Besar dari Permintaan', 'Warning');
			x = 0;
		}
		
		for(var j=0; j<dsDataGrdJab_viGzPenerimaanBahanMakanan.getCount() ; j++){
			var p=dsDataGrdJab_viGzPenerimaanBahanMakanan.getRange()[j].data;
			if(i != j && o.kd_bahan == p.kd_bahan && o.no_minta == o.no_minta){
				ShowPesanWarningGzPenerimaanBahanMakanan('Tidak boleh ada Bahan yang sama, periksa kembali daftar obat', 'Warning');
				x = 0;
				break;
			}
		}
		
	}
	
	return x;
};



function ShowPesanWarningGzPenerimaanBahanMakanan(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorGzPenerimaanBahanMakanan(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};


function ShowPesanInfoGzPenerimaanBahanMakanan(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};