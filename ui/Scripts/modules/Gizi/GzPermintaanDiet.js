
var selectCount_viGzPermintaanDiet=50;
var NamaForm_viGzPermintaanDiet="Permintaan Diet Pasien (Ahli Gizi)";
var NamaFormPerawat_viGzPermintaanDiet = "Permintaan Diet Pasien ( Perawat )";
var selectCountStatusPostingGzPermintaanDiet='Semua';//
var mod_name_viGzPermintaanDiet="viGzPermintaanDiet";
var now_viGzPermintaanDiet= new Date();
var rowSelected_viGzPermintaanDiet;
var rowSelectedGridPasien_viGzPermintaanDiet;
var setLookUps_viGzPermintaanDiet;
var selectSetUnit;
var cellSelecteddeskripsiRWI;
var tanggal = now_viGzPermintaanDiet.format("d/M/Y");
var jam = now_viGzPermintaanDiet.format("H/i/s");
var tmpkriteria;
var gridDTLDiet_GzPermintaanDiet;
var GridDataView_viGzPermintaanDiet;

//HUDI
//03-12-2020
//Combo Baru

var Field_bentuk_makanan = ['kd_bentuk_makanan', 'nama_makanan'];
var ds_bentukMakananPermintaanDiet = new WebApp.DataStore({fields: Field_bentuk_makanan});

var Field_rute = ['KD_RUTE', 'RUTE'];
var ds_rutePermintaanDiet = new WebApp.DataStore({fields: Field_rute});


var Field_frekuensi = ['FREKUENSI'];
var ds_frekuensiPermintaanDiet = new WebApp.DataStore({fields: Field_frekuensi});

var Field_volume = ['VOLUME'];
var ds_volumePermintaanDiet = new WebApp.DataStore({fields: Field_volume});
//=============================================================================


//var ds_rutePermintaanDiet;

var edit_detail_permintaan;

var CurrentGzPermintaanDiet =
{
    data: Object,
    details: Array,
    row: 0
};

var CurrentData_viGzPermintaanDiet =
{
	data: Object,
	details: Array,
	row: 0
};

var CurrentDataPasien_viGzPermintaanDiet =
{
	data: Object,
	details: Array,
	row: 0
};



CurrentPage.page = dataGrid_viGzPermintaanDiet(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

var GzPermintaanDiet={};
GzPermintaanDiet.form={};
GzPermintaanDiet.func={};
GzPermintaanDiet.vars={};
GzPermintaanDiet.func.parent=GzPermintaanDiet;
GzPermintaanDiet.form.ArrayStore={};
GzPermintaanDiet.form.ComboBox={};
GzPermintaanDiet.form.DataStore={};
GzPermintaanDiet.form.Record={};
GzPermintaanDiet.form.Form={};
GzPermintaanDiet.form.Grid={};
GzPermintaanDiet.form.Panel={};
GzPermintaanDiet.form.TextField={};
GzPermintaanDiet.form.Button={};

GzPermintaanDiet.form.ArrayStore.pasien=new Ext.data.ArrayStore({
		id: 0,
		fields: [	
					'kd_pasien','nama','tgl_masuk','jam_masuk','no_kamar','nama_kamar','umur','jenis_makanan_pasien','tktp','rute','alergi','jenis_diet_pasien','diet'
				],
		data: []
	});
	
GzPermintaanDiet.form.ArrayStore.waktu	= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_waktu','waktu'],
	data: []
});

GzPermintaanDiet.form.ArrayStore.jenisdiet	= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_jenis','jenis_diet','harga_pokok'],
	data: []
});

//HUDI
//Store Kategori diet
GzPermintaanDiet.form.ArrayStore.kategoridiet	= new Ext.data.ArrayStore({
	id : 0,
	fields : ['kd_kategori','nama_kategori'],
	data: []
});

//HUDI
//Store Bentuk Makanan
GzPermintaanDiet.form.ArrayStore.bentukmakanan	= new Ext.data.ArrayStore({
	id : 0,
	fields : ['kd_bentuk_makanan','nama_makanan'],
	data: []
});

GzPermintaanDiet.form.ArrayStore.jenismakanan	= new Ext.data.ArrayStore({
	id : 0,
	fields : ['kd_jenis_makanan','nama_jenis_makanan'],
	data: []
});

GzPermintaanDiet.form.ArrayStore.frekuensi	= new Ext.data.ArrayStore({
	id : 0,
	fields : ['frekuensi'],
	data: []
});

GzPermintaanDiet.form.ArrayStore.volume	= new Ext.data.ArrayStore({
	id : 0,
	fields : ['volume'],
	data: []
});

function dataGrid_viGzPermintaanDiet(mod_id_viGzPermintaanDiet){	
    var FieldMaster_viGzPermintaanDiet = 
	[
		'no_minta', 'kd_unit', 'nama_unit', 'tgl_minta', 'jam_minta', 'tgl_makan', 'kd_ahli_gizi', 'keterangan', 'nama_ahli_gizi', 'status_perawat', 'waktu','edit_permintaan','status_posting'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viGzPermintaanDiet = new WebApp.DataStore
	({
        fields: FieldMaster_viGzPermintaanDiet
    });
    dataGriAwal();
    // Grid Apotek Perencanaan # --------------
	GridDataView_viGzPermintaanDiet = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viGzPermintaanDiet,
			autoScroll: true,
			columnLines: true,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viGzPermintaanDiet = undefined;
							rowSelected_viGzPermintaanDiet = dataSource_viGzPermintaanDiet.getAt(row);
							CurrentData_viGzPermintaanDiet
							CurrentData_viGzPermintaanDiet.row = row;
							CurrentData_viGzPermintaanDiet.data = rowSelected_viGzPermintaanDiet.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viGzPermintaanDiet = dataSource_viGzPermintaanDiet.getAt(ridx);
					if (rowSelected_viGzPermintaanDiet != undefined)
					{
						setLookUp_viGzPermintaanDiet(rowSelected_viGzPermintaanDiet.data);
						var status_perawat = rowSelected_viGzPermintaanDiet.data.status_perawat;
						/* if(status_perawat == 'Tidak' || status_perawat == "Tidak" || status_perawat == '' || status_perawat == ""){
							setLookUp_viGzPermintaanDiet(rowSelected_viGzPermintaanDiet.data);
						}else{
							setLookUpPerawat_viGzPermintaanDiet(rowSelected_viGzPermintaanDiet.data);
						} */
					}
					else
					{
						setLookUp_viGzPermintaanDiet();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Apotek perencanaan
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header: 'Posting',
						dataIndex: 'status_posting',
						sortable: true,
						width: 10,
						align : 'center',
						renderer	: function(value, metaData, record, rowIndex, colIndex, store){
							console.log(value);
							switch (value){
								case '1':
									metaData.css = 'StatusHijau'; 
									break;
								case '0':
									metaData.css = 'StatusMerah';
									break;
							}
							return '';
						}
					},
					{
						header: 'No. Permintaan',
						dataIndex: 'no_minta',
						sortable: true,
						width: 25
						
					},
					//-------------- ## --------------
					{
						header: 'Ruangan',
						dataIndex: 'nama_unit',
						sortable: true,
						width: 25
					},
					//-------------- ## --------------
					{
						header:'Tgl Minta',
						dataIndex: 'tgl_minta',						
						width: 15,
						sortable: true,
						hideable:false,
                        menuDisabled:true,
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.tgl_minta);
						}
					},
					//HUDI
					//03-09-2020
					//Penambahan Jam Minta dan Format Time
					//-------------- ## --------------
					{
						header:'Jam Minta',
						dataIndex: 'jam_minta',						
						width: 15,
						sortable: true,
						hideable:false,
						//format : 'H:i:s',
						menuDisabled:true,
						renderer: Ext.util.Format.dateRenderer('H:i:s')
					},
					//-------------- ## --------------
					{
						header: 'Untuk Tanggal',
						dataIndex: 'tgl_makan',
						sortable: true,
						hideable:false,
                        menuDisabled:true,
						width: 15,
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.tgl_makan);
						}
					},
					//-------------- ## --------------
					{
						header: 'Nama Ahli Gizi / Perawat',
						dataIndex: 'nama_ahli_gizi',
						sortable: true,
						width: 20
					},
					//-------------- ## --------------
					{
						header: 'Perawat',
						dataIndex: 'status_perawat',
						sortable: true,
						width: 20
					},
					//-------------- ## --------------
					{
						header: 'kd_ahli_gizi',
						dataIndex: 'kd_ahli_gizi',
						sortable: true,
						width: 40,
						hidden:true
					},
					//-------------- ## --------------
					{
						header: 'kd_unit',
						dataIndex: 'kd_unit',
						sortable: true,
						width: 40,
						hidden:true
					},
					//-------------- ## --------------
					{
						header: 'Waktu Permintaan',
						dataIndex: 'waktu',
						sortable: true,
						width: 20
					},
					//-------------- ## --------------
					{
						header: 'Perubahan Data',
						dataIndex: 'edit_permintaan',
						sortable: true,
						width: 15
					}
					//-------------- ## --------------
					//-------------- ## --------------
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viGzPermintaanDiet',
				items: 
				[
					{
						xtype: 'button',
						text: 'Tambah Permintaan Ahli Gizi',
						iconCls: 'Add',
						tooltip: 'Add Data',
						id: 'btnTambahAhliGizi_viGzPermintaanDiet',
						handler: function(sm, row, rec)
						{
							setLookUp_viGzPermintaanDiet();	
							Ext.getCmp('btnEditDiet_viGzPermintaanDiet').hide(true);
							Ext.getCmp('btnSaveDiet_viGzPermintaanDiet').show(true);	
							Ext.getCmp('btnSeparatorDiet_viGzPermintaanDiet').hide(true);	
							Ext.getCmp('btnUnpostingPermintaan_viGzPermintaanDiet').disable(true);				
						}
					},{
						//HUDI
						//22-09-2020
						//dihidden karena menu di pisah
						xtype: 'button',
						text: 'Tambah Permintaan Perawat',
						iconCls: 'Add',
						tooltip: 'Add Data',
						id: 'btnTambahPerawat_viGzPermintaanDiet',
						hidden : true,
						handler: function(sm, row, rec)
						{
							setLookUpPerawat_viGzPermintaanDiet();	
							Ext.getCmp('btnEditDietPerawat_viGzPermintaanDiet').hide(true);
							Ext.getCmp('btnSaveDietPerawat_viGzPermintaanDiet').show(true);	
							Ext.getCmp('btnSeparatorDiet_viGzPermintaanDiet').hide(true);						
						}
					},
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viGzPermintaanDiet',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viGzPermintaanDiet != undefined)
							{
								setLookUp_viGzPermintaanDiet(rowSelected_viGzPermintaanDiet.data);
								/* if(rowSelected_viGzPermintaanDiet.data.status_perawat == "Ya"){
									setLookUpPerawat_viGzPermintaanDiet(rowSelected_viGzPermintaanDiet.data);
								}else{
									setLookUp_viGzPermintaanDiet(rowSelected_viGzPermintaanDiet.data)
								} */
							} else{
								ShowPesanWarningGzPermintaanDiet('Pilih data yang akan di edit','Warning');
							}
							
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viGzPermintaanDiet, selectCount_viGzPermintaanDiet, dataSource_viGzPermintaanDiet),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )
	
	var pencarianGzPermintaanDiet = new Ext.FormPanel({
        labelAlign: 'top',
        frame:true,
        title: '',
        bodyStyle:'padding:5px 5px 0',
        //width: 600,
        items: [
		{
			layout: 'column',
			border: false,
			items:
			[
				{
					columnWidth:.98,
					layout: 'absolute',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					width: 500,
					height: 70,
					anchor: '100% 100%',
					items:
					[
						{
							x: 10,
							y: 0,
							xtype: 'label',
							text: 'No. Permintaan'
						},
						{
							x: 120,
							y: 0,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 0,
							xtype: 'textfield',
							id: 'TxtNoPermintaanFilterGridDataView_viGzPermintaanDiet',
							name: 'TxtNoPermintaanFilterGridDataView_viGzPermintaanDiet',
							emptyText: '',
							width: 130,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariGzPermintaanDiet();
										refreshPermintaanDiet(tmpkriteria);
									} 						
								}
							}
						},
						{
							x: 10,
							y: 30,
							xtype: 'label',
							text: 'Ahli Gizi'
						},
						{
							x: 120,
							y: 30,
							xtype: 'label',
							text: ':'
						},
						ComboAhliGiziGzPermintaanDiet(),
						//-------------- ## --------------
						{
							x: 310,
							y: 0,
							xtype: 'label',
							text: 'Ruangan'
						},
						{
							x: 400,
							y: 0,
							xtype: 'label',
							text: ':'
						},
						ComboUnitGzPermintaanDiet(),	
						{
							x: 310,
							y: 30,
							xtype: 'label',
							text: 'Tanggal Minta'
						},
						{
							x: 400,
							y: 30,
							xtype: 'label',
							text: ':'
						},
						{
							x: 410,
							y: 30,
							xtype: 'datefield',
							id: 'dfTglAwalGzPermintaanDiet',
							format: 'd/M/Y',
							width: 120,
							tabIndex:3,
							value:now_viGzPermintaanDiet,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariGzPermintaanDiet();
										refreshPermintaanDiet(tmpkriteria);
									} 						
								}
							}
						},
						{
							x: 540,
							y: 30,
							xtype: 'label',
							text: 's/d'
						},
						{
							x: 568,
							y: 30,
							xtype: 'datefield',
							id: 'dfTglAkhirGzPermintaanDiet',
							format: 'd/M/Y',
							width: 120,
							tabIndex:4,
							value:now_viGzPermintaanDiet,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariGzPermintaanDiet();
										refreshPermintaanDiet(tmpkriteria);
									} 						
								}
							}
						},
						//----------------------------------------
						{
							x: 678,
							y: 30,
							xtype: 'button',
							text: 'Cari',
							iconCls: 'refresh',
							tooltip: 'Cari',
							style:{paddingLeft:'30px'},
							width:150,
							id: 'BtnFilterGridDataView_viGzPermintaanDiet',
							handler: function() 
							{				
								tmpkriteria = getCriteriaCariGzPermintaanDiet();
								refreshPermintaanDiet(tmpkriteria);
							}                        
						}
					]
				}
			]
		}
		]	
	})

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viGzPermintaanDiet = new Ext.Panel
    (
		{
			title: NamaForm_viGzPermintaanDiet,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viGzPermintaanDiet,
			region: 'center',
			layout: 'form', 
			closable: true,        
			border: false,  
			margins: '0 5 5 0',
			items: [ 
					pencarianGzPermintaanDiet,
					GridDataView_viGzPermintaanDiet
				],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viGzPermintaanDiet,
		            columns: 21,
		            defaults: {
					scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viGzPermintaanDiet;
    //-------------- # End form filter # --------------
}

function refreshPermintaanDiet(kriteria)
{
    dataSource_viGzPermintaanDiet.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'ViewPermintaanDiet',
                    param : kriteria
                }			
            }
        );   
    return dataSource_viGzPermintaanDiet;
}

function setLookUp_viGzPermintaanDiet(rowdata){
    var lebar = 1090;
    setLookUps_viGzPermintaanDiet = new Ext.Window({
        id: Nci.getId(),
        title: NamaForm_viGzPermintaanDiet, 
        closeAction: 'destroy',        
        width: 1100,
        height: 580,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viGzPermintaanDiet(lebar,rowdata),
        listeners:{
            activate: function(){
				
            },
            afterShow: function(){
				this.activate();
            },
            deactivate: function(){
				console.log("close");
				tmpkriteria = getCriteriaCariGzPermintaanDiet();
				refreshPermintaanDiet(tmpkriteria);
                rowSelected_viGzPermintaanDiet=undefined;
			}
        }
    });

    setLookUps_viGzPermintaanDiet.show();
    if (rowdata == undefined){	
    }
    else
    {
        datainit_viGzPermintaanDiet(rowdata);
    }
}

function getFormItemEntry_viGzPermintaanDiet(lebar,rowdata){
	//HUDI
	//30-10-2020
	//disable button posting dan unposting
	if(rowdata != undefined){
		var btn_deletepermintaan;
		var btn_posting;
		var btn_unposting;
		if(rowdata.status_posting == '0'){
			btn_deletepermintaan = false;
			btn_posting 	= false;
			btn_unposting 	= true;
		}else{
			btn_deletepermintaan = true;
			btn_posting 	= true;
			btn_unposting 	= false;
		}
	}
	//=====================================

    var pnlFormDataBasic_viGzPermintaanDiet = new Ext.FormPanel({
		title: '',
		region: 'north',
		layout: 'form',
		bodyStyle: 'padding:10px 10px 10px 10px',
		anchor: '100%',
		labelWidth: 1,
		autoWidth: true,
		width: lebar,
		height : 850,
		border: false,
		items:[
				getItemPanelInputPermintaan_viGzPermintaanDiet(lebar),
				getItemGridPasien_viGzPermintaanDiet(lebar,rowdata),
				getItemGridDetail_viGzPermintaanDiet(lebar,rowdata)
			],
			fileUpload: true,
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAdd_viGzPermintaanDiet',
						handler: function(){
							gridDTLDiet_GzPermintaanDiet.enable(true);
							dataAddNew_viGzPermintaanDiet();
						}
					},{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						id:'btnDeletePermintaan_viGzPermintaanDiet',
						iconCls: 'remove',
						disabled : btn_deletepermintaan,
						handler:function()
						{
							Ext.Msg.confirm('Warning', 'Apakah data permintaan ini akan di hapus?', function(button){
								if (button == 'yes'){
									loadMask.show();
									Ext.Ajax.request
									(
										{
											url: baseURL + "index.php/gizi/functionPermintaanDiet/deletePermintaan",
											params: {no_minta:Ext.getCmp('txtNoPermintaanGzPermintaanDietL').getValue()},
											failure: function(o)
											{
												loadMask.hide();
												ShowPesanErrorGzPermintaanDiet('Error, delete permintaan! Hubungi Admin', 'Error');
											},	
											success: function(o) 
											{
												var cst = Ext.decode(o.responseText);
												if (cst.success === true) 
												{
													loadMask.hide();
													//getDataGridAwal();
													dataGriAwal();
													ShowPesanInfoGzPermintaanDiet('Penghapusan berhasil','Information');
													setLookUps_viGzPermintaanDiet.close();
												}
												else 
												{
													if(cst.order =='true'){
														loadMask.hide();
														ShowPesanWarningGzPermintaanDiet('Gagal menghapus data ini. Permintaan diet ini sudah diorder', 'Warning');
													} else{
														loadMask.hide();
														ShowPesanWarningGzPermintaanDiet('Gagal menghapus data ini', 'Warning');
													}
													
												};
											}
										}
										
									)
								}
							});
						}
						  
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype : 'button',
						text : 'Posting',
						id : 'btnPostingPermintaan_viGzPermintaanDiet',
						disabled : btn_posting,
						handler : function(){
							var str_noMinta = Ext.getCmp('txtNoPermintaanGzPermintaanDietL').getValue();
							if(str_noMinta != '' || str_noMinta != ""){
								Ext.Ajax.request({
									url: baseURL + "index.php/gizi/functionPermintaanDiet/posting",
									params: {
										no_minta : str_noMinta
									},
									failure: function(o)
									{
										loadMask.hide();
										ShowPesanErrorGzPermintaanDiet('Error, permintaan! Hubungi Admin', 'Error');
									},	
									success: function(o) 
									{
										var cst = Ext.decode(o.responseText);
										if(cst.success == true || cst.success == 'true'){
											Ext.getCmp('btnUnpostingPermintaan_viGzPermintaanDiet').enable(true);
											Ext.getCmp('btnPostingPermintaan_viGzPermintaanDiet').disable(true);
											Ext.getCmp('btnDeletePermintaan_viGzPermintaanDiet').disable(true);
											HideButtonPermintaanDiet(true);
											ShowPesanInfoGzPermintaanDiet('Berhasil Posting.', 'Success');
										}
									}
								})
							}else{
								ShowPesanWarningGzPermintaanDiet('No minta masih kosong!', 'Warning');
							}
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype : 'button',
						text : 'Unposting',
						id : 'btnUnpostingPermintaan_viGzPermintaanDiet',
						disabled : btn_unposting,
						handler : function(){
							var str_noMinta = Ext.getCmp('txtNoPermintaanGzPermintaanDietL').getValue();
							if(str_noMinta != '' || str_noMinta != ""){
								Ext.MessageBox.show({
									title: 'Peringatan',
									msg: 'Anda yakin untuk unposting?',
									buttons: Ext.MessageBox.YESNO,
									icon: Ext.MessageBox.WARNING,
									fn: function(btn){
										if(btn == 'yes'){
											var dlg = Ext.MessageBox.prompt('Permintan diet', 'Masukan keterangan unposting:', function (btn, combo) {
												if (btn == 'ok'){
													// console.log(btn);
													// console.log(combo);
													Ext.Ajax.request({
														url: baseURL + "index.php/gizi/functionPermintaanDiet/unposting",
														params: {
															no_minta		: str_noMinta,
															ket_unposting	: combo
														},
														failure: function(o)
														{
															loadMask.hide();
															ShowPesanErrorGzPermintaanDiet('Error, permintaan! Hubungi Admin', 'Error');
														},	
														success: function(o) 
														{
															var cst = Ext.decode(o.responseText);
															if(cst.success == true || cst.success == 'true'){
																Ext.getCmp('btnUnpostingPermintaan_viGzPermintaanDiet').disable(true);
																Ext.getCmp('btnPostingPermintaan_viGzPermintaanDiet').enable(true);
																Ext.getCmp('btnDeletePermintaan_viGzPermintaanDiet').enable(true);
																ShowButtonPermintaanDiet(true);
																ShowPesanInfoGzPermintaanDiet('Berhasil Unposting.', 'Success');
															}
														}
													})
												}
											});
										}
									}
								});
							}else{
								ShowPesanWarningGzPermintaanDiet('No minta masih kosong!', 'Warning');
							}
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype:'splitbutton',
						text:'Print',
						iconCls:'print',
						id:'btnPrint_viGzPermintaanDiet',
						disabled:true,
						hidden:true,
						handler:function()
						{							
								
						},
						menu: new Ext.menu.Menu({
						items: [
							// these items will render as dropdown menu items when the arrow is clicked:
							{
								xtype: 'button',
								text: 'Print Bill',
								id: 'btnPrintBillGzPermintaanDiet',
								handler: function()
								{
									printbill();
								}
							},
						]
						})
					}
				]
			}//,items:
		}
    )

    return pnlFormDataBasic_viGzPermintaanDiet;
}

function getItemPanelInputPermintaan_viGzPermintaanDiet(lebar) {
    var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 115,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'No. Penerimaan'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 0,
								xtype: 'textfield',
								id: 'txtNoPermintaanGzPermintaanDietL',
								name: 'txtNoPermintaanGzPermintaanDietL',
								width: 130,
								readOnly:true,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									}
								}
							},
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'Tanggal Minta'
							},
							{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 30,
								xtype: 'datefield',
								id: 'dfTglMintaGzPermintaanDietL',
								format: 'd/M/Y',
								width: 150,
								tabIndex:2,
								readOnly:true,
								value:now_viGzPermintaanDiet,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									}
								}
							},
							{
								x: 10,
								y: 60,
								xtype: 'label',
								text: 'Dari Ruangan'
							},
							{
								x: 120,
								y: 60,
								xtype: 'label',
								text: ':'
							},
							ComboUnitGzPermintaanDietLookup(),
							{
								x: 10,
								y: 90,
								xtype: 'label',
								text: 'Untuk Tanggal'
							},
							{
								x: 120,
								y: 90,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 90,
								xtype: 'datefield',
								id: 'dfTglUntukGzPermintaanDietL',
								format: 'd/M/Y',
								width: 150,
								tabIndex:4,
								value:now_viGzPermintaanDiet
							},
							//-------------- ## --------------
							{
								x: 320,
								y: 0,
								xtype: 'label',
								text: 'Ahli Gizi'
							},
							{
								x: 410,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							ComboAhliGiziGzPermintaanDietLookup(),
							{
								x: 320,
								y: 30,
								xtype: 'label',
								text: 'Diagnosa'
							},
							{
								x: 410,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							{
								x: 420,
								y: 30,	
								xtype: 'textarea',
								name: 'txtKetGZPenerimaanL',
								id: 'txtKetGZPenerimaanL',
								width : 280,
								height : 80,
								tabIndex:6
							},
							//-----------------HIDDEN-----------------------
							{
								x: 130,
								y: 0,
								xtype: 'textfield',
								id: 'txtTmpNoPermintaanGzPermintaanDietL',
								name: 'txtTmpNoPermintaanGzPermintaanDietL',
								width: 130,
								readOnly:true,
								hidden:true
							},
							{
								x: 0,
								y: 0,
								xtype: 'textfield',
								id: 'txtTmpKdPasienGzPermintaanDietL',
								name: 'txtTmpKdPasienGzPermintaanDietL',
								width: 130,
								readOnly:true,
								hidden:true
							},
							
						]
					}
				]
			}
		]		
	};
    return items;
};

function getItemGridPasien_viGzPermintaanDiet(lebar,rowdata){
	//HUDI
	//30-10-2020
	//disable button add pasien dan delete pasien
	if(rowdata != undefined){
		var btn_addpasien;
		var btn_deletepasien;
		if(rowdata.status_posting == '0'){
			btn_addpasien	 = false;
			btn_deletepasien = false;
		}else{
			btn_addpasien	 = true;
			btn_deletepasien = true;
		}
	}
	//=====================================

    var items ={
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'margin-top: -1px;',
		border:false,
		width: lebar-80,
		height: 235,//300, 
	    tbar:
		[
			{
				text	: 'Add Pasien',
				id		: 'btnAddPasienGzPermintaanDietL',
				tooltip	: nmLookup,
				disabled: true,
				hidden	: btn_addpasien,
				iconCls	: 'find',
				handler	: function(){
					var records = new Array();
					
					if(Ext.getCmp('txtTmpNoPermintaanGzPermintaanDietL').getValue() == ''){
						//jika permintaan baru
						loadMask.show();
						records.push(new dsDataGrdPasien_viGzPermintaanDiet.recordType());
						dsDataGrdPasien_viGzPermintaanDiet.add(records);
						saveMinta_viGzPermintaanDiet();
						
						dsTRDetailDietGzPermintaanDiet.removeAll();
						GzPermintaanDiet.vars.status_order = 'false';
						
						Ext.getCmp('txtNoPermintaanGzPermintaanDietL').disable();
						Ext.getCmp('dfTglMintaGzPermintaanDietL').disable();
						Ext.getCmp('cbo_UnitGzPermintaanDietLookup').disable();
						Ext.getCmp('dfTglUntukGzPermintaanDietL').disable();
						Ext.getCmp('cbo_AhliGiziGzPermintaanDietLookup').disable();
						Ext.getCmp('txtKetGZPenerimaanL').disable();
					} else{
						records.push(new dsDataGrdPasien_viGzPermintaanDiet.recordType());
						dsDataGrdPasien_viGzPermintaanDiet.add(records);
						
						dsTRDetailDietGzPermintaanDiet.removeAll();
						GzPermintaanDiet.vars.status_order = 'false';
					}
					
					
				}
			},{
				xtype: 'tbseparator'
			},{
				xtype: 'button',
				text: 'Delete',
				iconCls: 'remove',
				disabled:true,
				hidden	: btn_deletepasien,
				id: 'btnDelete_viGzPermintaanDiet',
				handler: function()
				{
					var line = GzPermintaanDiet.form.Grid.a.getSelectionModel().selection.cell[0];
					var o = dsDataGrdPasien_viGzPermintaanDiet.getRange()[line].data;
					if(dsDataGrdPasien_viGzPermintaanDiet.getCount()>1){
						Ext.Msg.confirm('Warning', 'Apakah pasien ini akan dihapus?', function(button){
							if (button == 'yes'){
								if(dsDataGrdPasien_viGzPermintaanDiet.getRange()[line].data.kd_pasien != undefined){
									Ext.Ajax.request
									(
										{
											url: baseURL + "index.php/gizi/functionPermintaanDiet/hapusBarisPasien",
											params:{kd_pasien:o.kd_pasien,no_minta:Ext.getCmp('txtTmpNoPermintaanGzPermintaanDietL').getValue()} ,
											failure: function(o)
											{
												ShowPesanErrorGzPermintaanDiet('Error, delete pasien! Hubungi Admin', 'Error');
											},	
											success: function(o) 
											{
												var cst = Ext.decode(o.responseText);
												if (cst.success === true) 
												{
													dsDataGrdPasien_viGzPermintaanDiet.removeAt(line);
													GzPermintaanDiet.form.Grid.a.getView().refresh();
													Ext.getCmp('btnAddDetDietGzPermintaanDietL').enable();
													ShowPesanInfoGzPermintaanDiet('Berhasil menghapus data pasien ini', 'Information');
													
												}
												else 
												{
													ShowPesanErrorGzPermintaanDiet('Gagal menghapus data pasien ini', 'Error');
												};
											}
										}
										
									)
								}else{
									dsDataGrdPasien_viGzPermintaanDiet.removeAt(line);
									GzPermintaanDiet.form.Grid.a.getView().refresh();
									
								}
							} 
							
						});
					} else{
						ShowPesanErrorGzPermintaanDiet('Data tidak bisa dihapus karena minimal pasien 1','Error');
					}
					
				}
			}	
		],
		items:[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:[
					gridDataViewEdit_viGzPermintaanDiet()
				]	
			}
		]
	};
    return items;
};


//var a={};
function gridDataViewEdit_viGzPermintaanDiet(){
    var FieldGrdKasir_viGzPermintaanDiet = [];
    dsDataGrdPasien_viGzPermintaanDiet= new WebApp.DataStore({
        fields: FieldGrdKasir_viGzPermintaanDiet
    });
    
    GzPermintaanDiet.form.Grid.a =new Ext.grid.EditorGridPanel({
        store: dsDataGrdPasien_viGzPermintaanDiet,
        height: 210,
        columnLines: true,
		selModel: new Ext.grid.CellSelectionModel ({
            singleSelect: true,
            listeners:{
                cellselect: function(sm, row, rec){
					rowSelectedGridPasien_viGzPermintaanDiet = undefined;
					rowSelectedGridPasien_viGzPermintaanDiet = dsDataGrdPasien_viGzPermintaanDiet.getAt(row);
					CurrentDataPasien_viGzPermintaanDiet
					CurrentDataPasien_viGzPermintaanDiet.row = row;
					CurrentDataPasien_viGzPermintaanDiet.data = rowSelectedGridPasien_viGzPermintaanDiet.data;
					
					//console.log(rowSelectedGridPasien_viGzPermintaanDiet.data);
					
					dsTRDetailDietGzPermintaanDiet.removeAll();
					getGridWaktu(Ext.getCmp('txtTmpNoPermintaanGzPermintaanDietL').getValue(),CurrentDataPasien_viGzPermintaanDiet.data.kd_pasien);
					
					if(GzPermintaanDiet.vars.status_order == 'false'){
						Ext.getCmp('btnAddDetDietGzPermintaanDietL').enable();
						Ext.getCmp('btnDeleteDet_viGzPermintaanDiet').enable();
						Ext.getCmp('btnEditDiet_viGzPermintaanDiet').enable();
						Ext.getCmp('btnSaveDiet_viGzPermintaanDiet').enable();
					} else{
						Ext.getCmp('btnAddPasienGzPermintaanDietL').disable();
						Ext.getCmp('btnAddDetDietGzPermintaanDietL').disable();
						Ext.getCmp('btnEditDiet_viGzPermintaanDiet').disable();
						Ext.getCmp('btnSaveDiet_viGzPermintaanDiet').disable();
					}
					Ext.getCmp('txtTmpKdPasienGzPermintaanDietL').setValue(CurrentDataPasien_viGzPermintaanDiet.data.kd_pasien);
					
                },
            }
        }),
        stripeRows: true,
        cm:new Ext.grid.ColumnModel([
        	new Ext.grid.RowNumberer(),	
			{
				dataIndex: 'kd_pasien',
				header: 'Kode Pasien',
				sortable: true,
				hidden: false,
				width: 70,
				editor:new Nci.form.Combobox.autoComplete({
					id		: 'txt_KdPasien',
					//enableKeyEvents:true,
					store	: GzPermintaanDiet.form.ArrayStore.pasien,
					select	: function(a,b,c){
										
						var line	= GzPermintaanDiet.form.Grid.a.getSelectionModel().selection.cell[0];
						dsDataGrdPasien_viGzPermintaanDiet.getRange()[line].data.kd_pasien=b.data.kd_pasien;
						dsDataGrdPasien_viGzPermintaanDiet.getRange()[line].data.tgl_masuk=b.data.tgl_masuk;
						dsDataGrdPasien_viGzPermintaanDiet.getRange()[line].data.jam_masuk=b.data.jam_masuk;
						dsDataGrdPasien_viGzPermintaanDiet.getRange()[line].data.no_kamar=b.data.no_kamar;
						dsDataGrdPasien_viGzPermintaanDiet.getRange()[line].data.nama_kamar=b.data.nama_kamar;
						dsDataGrdPasien_viGzPermintaanDiet.getRange()[line].data.nama=b.data.nama;
						dsDataGrdPasien_viGzPermintaanDiet.getRange()[line].data.umur=b.data.umur;
						
						GzPermintaanDiet.vars.kd_pasien=b.data.kd_pasien;
						GzPermintaanDiet.vars.tgl_masuk=b.data.tgl_masuk;
						GzPermintaanDiet.vars.no_kamar=b.data.no_kamar;
						GzPermintaanDiet.vars.kd_unit=b.data.kd_unit;
						
						loadMask.show();
						
						savePasien_viGzPermintaanDiet();
						
						GzPermintaanDiet.form.Grid.a.getView().refresh();
					},
					insert	: function(o){
						return {
							kd_pasien       : o.kd_pasien,
							tgl_masuk 		: o.tgl_masuk,
							jam_masuk		: o.jam_masuk,
							no_kamar		: o.no_kamar,
							nama_kamar		: o.nama_kamar,
							nama			: o.nama,
							umur			: o.umur,
							text			:  '<table style="font-size: 11px;"><tr><td width="100">'+o.kd_pasien+'</td><td width="200">'+o.nama+'</td></tr></table>',
							kd_unit			: o.kd_unit
						}
					},
					param:function(o){
						/* var tmp_nilai = formatnomedrec(Ext.getCmp('txt_KdPasien').getValue());
						console.log(tmp_nilai); */
						console.log(o.lastQuery);
							return {
								kd_pasien : formatnomedrec(o.lastQuery),
								kd_unitLama:GzPermintaanDiet.vars.kd_unit,
								kd_unitNew:Ext.getCmp('cbo_UnitGzPermintaanDietLookup').getValue()
							}
					},
					url		: baseURL + "index.php/gizi/functionPermintaanDiet/getPasienRWI",
					valueField: 'kd_pasien',
					displayField: 'text',
					listWidth: 380
				})
			},
			{			
				dataIndex: 'nama',
				header: 'Nama Pasien',
				sortable: true,
				width: 250,
				editor:new Nci.form.Combobox.autoComplete({
					//id		: 'ColNamaPasien',
					store	: GzPermintaanDiet.form.ArrayStore.pasien,
					select	: function(a,b,c){
						console.log(b.data);
						var line	= GzPermintaanDiet.form.Grid.a.getSelectionModel().selection.cell[0];
						dsDataGrdPasien_viGzPermintaanDiet.getRange()[line].data.kd_pasien=b.data.kd_pasien;
						dsDataGrdPasien_viGzPermintaanDiet.getRange()[line].data.tgl_masuk=b.data.tgl_masuk;
						dsDataGrdPasien_viGzPermintaanDiet.getRange()[line].data.jam_masuk=b.data.jam_masuk;
						dsDataGrdPasien_viGzPermintaanDiet.getRange()[line].data.no_kamar=b.data.no_kamar;
						dsDataGrdPasien_viGzPermintaanDiet.getRange()[line].data.nama_kamar=b.data.nama_kamar;
						dsDataGrdPasien_viGzPermintaanDiet.getRange()[line].data.nama=b.data.nama;
						dsDataGrdPasien_viGzPermintaanDiet.getRange()[line].data.umur = b.data.umur;
						
						GzPermintaanDiet.vars.kd_pasien=b.data.kd_pasien;
						GzPermintaanDiet.vars.tgl_masuk=b.data.tgl_masuk;
						GzPermintaanDiet.vars.no_kamar=b.data.no_kamar;
						GzPermintaanDiet.vars.kd_unit=b.data.kd_unit;
						
						loadMask.show();
						
						savePasien_viGzPermintaanDiet();
						
						GzPermintaanDiet.form.Grid.a.getView().refresh();
					},
					insert	: function(o){
						console.log(o);
						return {
							kd_pasien       : o.kd_pasien,
							tgl_masuk 		: o.tgl_masuk,
							jam_masuk		: o.jam_masuk,
							no_kamar		: o.No_Kamar,
							nama_kamar		: o.Nama_Kamar,
							nama			: o.nama,
							umur			: o.umur,
							text			:  '<table style="font-size: 11px;"><tr><td width="100">'+o.kd_pasien+'</td><td width="200">'+o.nama+'</td></tr></table>',
							kd_unit			: o.kd_unit
						}
					},
					param:function(){
							return {
								kd_pasien : '',
								kd_unitLama:GzPermintaanDiet.vars.kd_unit,
								kd_unitNew:Ext.getCmp('cbo_UnitGzPermintaanDietLookup').getValue()
							}
					},
					url		: baseURL + "index.php/gizi/functionPermintaanDiet/getPasienRWI",
					valueField: 'nama',
					displayField: 'text',
					listWidth: 380
				})
			},
			{
				dataIndex: 'umur',
				header: 'Umur',
				width: 120,
				align:'left'
			},
			{
				dataIndex: 'tgl_masuk',
				header: 'Tanggal Masuk',
				width: 70,
				align:'center'
			},{
				dataIndex: 'jam_masuk',
				header: 'Jam Masuk',
				sortable: true,
				align:'center',
				width: 85
			},{
				dataIndex: 'no_kamar',
				header: 'No Kamar',
				sortable: true,
				width: 45,
				align:'center'
			},
			{
				dataIndex: 'nama_kamar',
				header: 'Kamar',
				sortable: true,
				width: 65,
				align:'center'
			},
			{
				dataIndex: 'no_minta',
				header: 'no_minta',
				sortable: true,
				width: 65,
				align:'center',
				hidden:true
			},
			{
				dataIndex: 'jenis_makanan_pasien',
				header: 'Bentuk Makanan',
				sortable: true,
				width: 100,
				align:'center'
			},
			{
				dataIndex: 'diet',
				header: 'Jenis Diet',
				sortable: true,
				width: 100,
				align:'center'
			}
        ]),
		viewConfig:{
			forceFit: true
		}
    });
    return GzPermintaanDiet.form.Grid.a;
}



function getItemGridDetail_viGzPermintaanDiet(lebar,rowdata) 
{
	//HUDI
	//30-10-2020
	//disable button add pasien dan delete pasien
	if(rowdata != undefined){
		var btn_adddetail;
		var btn_deletedetail;
		var btn_editdetail;
		var btn_savedetail;
		if(rowdata.status_posting == '0'){
			btn_adddetail	 = false;
			btn_deletedetail = false;
			btn_editdetail	 = false;
			btn_savedetail	 = false;
		}else{
			btn_adddetail	 = true;
			btn_deletedetail = true;
			btn_editdetail	 = true;
			btn_savedetail	 = true;
		}
	}
	//=====================================

	var items =
	{
		title: 'Detail Diet', 
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
		bodyStyle: 'margin-top: -1px;',
		border:true,
		width: lebar-80,
		height: 150,//300, 
	    items:
		[
		
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
				items:
				[
					gridDataViewDetaiDiet_viGzPermintaanDiet()
				]	
			}
			//-------------- ## --------------
		],
		tbar:
		[
			{
				text	: 'Add Diet',
				id		: 'btnAddDetDietGzPermintaanDietL',
				tooltip	: nmLookup,
				iconCls	: 'find',
				disabled:true,
				hidden	: btn_adddetail,
				handler	: function(){
					var records = new Array();
					records.push(new dsTRDetailDietGzPermintaanDiet.recordType());
					dsTRDetailDietGzPermintaanDiet.add(records);

					var thisrow = dsTRDetailDietGzPermintaanDiet.getCount()-1; 
					//

					var date 	= new Date();
					var jam 	= date.format("H");
					var menit   = date.format("i");
					
					if(jam > 0 && jam <= 4){
						if(menit > 0 && menit <= 29){
							jam = 'Pagi';
						}else{
							jam = 'Siang';
						}
					}else if(jam > 4 && jam <= 9){
						jam = 'Siang';
					}else if(jam >= 10 && jam < 15){
							jam = 'Sore';
					}else if( jam >= 15 && jam < 24){
						jam = 'Pagi';
					}
					
					dsTRDetailDietGzPermintaanDiet.getRange()[thisrow].data.waktu = jam;
					gridDTLDiet_GzPermintaanDiet.getView().refresh();
					gridDTLDiet_GzPermintaanDiet.startEditing(thisrow, 3);
				}
			},{
				xtype: 'tbseparator'
			},{
				xtype: 'button',
				text: 'Delete',
				iconCls: 'remove',
				disabled:true,
				hidden : btn_deletedetail,
				id: 'btnDeleteDet_viGzPermintaanDiet',
				handler: function()
				{
					var line = gridDTLDiet_GzPermintaanDiet.getSelectionModel().selection.cell[0];
					var o = dsTRDetailDietGzPermintaanDiet.getRange()[line].data;
					if(dsTRDetailDietGzPermintaanDiet.getCount()>1){
						Ext.Msg.confirm('Warning', 'Apakah jenis diet ini akan dihapus?', function(button){
							if (button == 'yes'){
								if(dsTRDetailDietGzPermintaanDiet.getRange()[line].data.kd_jenis != undefined){
									Ext.Ajax.request
									(
										{
											url: baseURL + "index.php/gizi/functionPermintaanDiet/hapusBarisDetailDiet",
											params: {		
												kd_jenis:o.kd_jenis, 
												kd_waktu:o.kd_waktu, 
												no_minta:Ext.getCmp('txtTmpNoPermintaanGzPermintaanDietL').getValue(),
												kd_pasien:Ext.getCmp('txtTmpKdPasienGzPermintaanDietL').getValue()
											},
											failure: function(o) {
												ShowPesanErrorGzPermintaanDiet('Error, delete detail diet! Hubungi Admin', 'Error');
											},	
											success: function(o) {
												var cst = Ext.decode(o.responseText);
												if (cst.success === true) 
												{
													ShowPesanInfoGzPermintaanDiet('Berhasil menghapus jenis diet ini', 'Information');
													dsTRDetailDietGzPermintaanDiet.removeAt(line);
													gridDTLDiet_GzPermintaanDiet.getView().refresh();
												}
												else 
												{
													ShowPesanErrorGzPermintaanDiet('Gagal menghapus data jenis diet ini', 'Error');
												};
											}
										}
										
									)
								}else{
									dsTRDetailDietGzPermintaanDiet.removeAt(line);
									gridDTLDiet_GzPermintaanDiet.getView().refresh();
								}
							} 
							
						});
					} else{
						ShowPesanErrorGzPermintaanDiet('Data tidak bisa dihapus karena minimal jenis diet 1','Error');
					}
					
				}
			},{
				xtype: 'tbseparator'
			},{
				xtype: 'button',
				text: 'Edit',
				iconCls: 'Edit_Tr',
				disabled:true,
				hidden : btn_editdetail,
				id: 'btnEditDiet_viGzPermintaanDiet',
				handler: function(){
					gridDTLDiet_GzPermintaanDiet.enable(true);
					Ext.getCmp('btnSaveDiet_viGzPermintaanDiet').enable();
					edit_detail_permintaan = true;
				}
			},{
				xtype: 'tbseparator',
				id	 : 'btnSeparatorDiet_viGzPermintaanDiet'
			},{
				xtype: 'button',
				text: 'Save',
				iconCls: 'save',
				disabled:true,
				hidden : btn_savedetail,
				id: 'btnSaveDiet_viGzPermintaanDiet',
				handler: function(){
					loadMask.show();
					saveDetailDiet_viGzPermintaanDiet();
				}
			}	
		]
	};
    return items;
};

function gridDataViewDetaiDiet_viGzPermintaanDiet() 
{
	
	//HUDI
	//16-11-2020
	//Penambahan combo ======================================================================
	var Field_jenis_makanan = ['kd_jenis_makanan', 'nama_jenis_makanan'];
	ds_jenisMakananPermintaanDiet = new WebApp.DataStore({fields: Field_jenis_makanan});
	ds_jenisMakananPermintaanDiet.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'kd_jenis_makanan',
					Sortdir: 'ASC',
					target: 'ComboJenisMakanan',
					param: ''
				}
		}
	);

	/* var Field_rute = ['KD_RUTE', 'RUTE'];
	ds_rutePermintaanDiet = new WebApp.DataStore({fields: Field_rute});
	ds_rutePermintaanDiet.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'kd_rute',
					Sortdir: 'ASC',
					target: 'ComboRute',
					param: ''
				}
		}
	); */

	/* var Field_frekuensi = ['FREKUENSI'];
	ds_frekuensiPermintaanDiet = new WebApp.DataStore({fields: Field_frekuensi});
	ds_frekuensiPermintaanDiet.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: '',
					Sortdir: 'ASC',
					target: 'ComboFrekuensi',
					param: ''
				}
		}
	); */

	/* var Field_volume = ['VOLUME'];
	ds_volumePermintaanDiet = new WebApp.DataStore({fields: Field_volume});
	ds_volumePermintaanDiet.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: '',
					Sortdir: 'ASC',
					target: 'ComboVolume',
					param: ''
				}
		}
	); */
		//================================================================================
	//HUDI
	//16-11-2020
	chkTktp_viGzPermintaanDiet = new Ext.grid.CheckColumn({
		id: 'chkTktp_viGzPermintaanDiet',
		header: 'TKTP',
		align: 'center',						
		dataIndex: 'tktp',	
		width: 50,
		listeners:{
			checkchange: function(column, recordIndex, checked){
				  console.log("cek : " + checked);
			}
		}
		/* listeners:{
			checkchange: function(column, recordIndex, checked){
				console.log(checked);
				alert("checked");
			}
		} */
	});

    var fldDetail = [];
	
    dsTRDetailDietGzPermintaanDiet = new WebApp.DataStore({ fields: fldDetail })
		 
    gridDTLDiet_GzPermintaanDiet = new Ext.grid.EditorGridPanel
    (
        {
            store: dsTRDetailDietGzPermintaanDiet,
            border: false,
            columnLines: true,
            height: 95,
			autoScroll:true,
            selModel: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec, column)
                        {
							//console.log(column);
							//cellSelecteddeskripsiRWI = dsTRDetailDietGzPermintaanDietList.getAt(row);
                            //CurrentGzPermintaanDiet.row = row;
                            //CurrentGzPermintaanDiet.data = cellSelecteddeskripsiRWI;
                        }
                    }
                }
            ),
            stripeRows: true,
            columns:
			[
				{
					header: 'kd waktu',
					dataIndex: 'kd_waktu',
					width:100,
					hidden:true
				},
				{			
					dataIndex: 'waktu',
					header: 'Waktu',
					sortable: false,
					width: 50,	
					editor:new Nci.form.Combobox.autoComplete({
						store	: GzPermintaanDiet.form.ArrayStore.waktu,
						select	: function(a,b,c){
							var line	= gridDTLDiet_GzPermintaanDiet.getSelectionModel().selection.cell[0];
							dsTRDetailDietGzPermintaanDiet.getRange()[line].data.kd_waktu=b.data.kd_waktu;
							dsTRDetailDietGzPermintaanDiet.getRange()[line].data.waktu=b.data.waktu;
							
							GzPermintaanDiet.vars.kd_waktu=b.data.kd_waktu;
							
							gridDTLDiet_GzPermintaanDiet.getView().refresh();

							var thisrow = dsTRDetailDietGzPermintaanDiet.getCount()-1; 
							gridDTLDiet_GzPermintaanDiet.startEditing(thisrow, 3);
						},
						insert	: function(o){
							return {
								kd_waktu	: o.kd_waktu,
								waktu		: o.waktu,
								text		:  '<table style="font-size: 11px;"><tr><td width="100">'+o.waktu+'</td></tr></table>'
							}
						},
						url		: baseURL + "index.php/gizi/functionPermintaanDiet/getWaktu",
						valueField: 'waktu',
						displayField: 'text',
						listWidth: 150
					})
				},
				{			
					dataIndex: 'jenis_diet',
					header: 'Jenis diet',
					sortable: false,
					width: 150,	
					hidden : false,
					editor:new Nci.form.Combobox.autoComplete({
						store	: GzPermintaanDiet.form.ArrayStore.jenisdiet,
						select	: function(a,b,c){
							var line	= gridDTLDiet_GzPermintaanDiet.getSelectionModel().selection.cell[0];
							dsTRDetailDietGzPermintaanDiet.getRange()[line].data.kd_jenis=b.data.kd_jenis;
							dsTRDetailDietGzPermintaanDiet.getRange()[line].data.jenis_diet=b.data.jenis_diet;
							
							GzPermintaanDiet.vars.kd_jenis=b.data.kd_jenis;
							
							/* loadMask.show();
							
							saveDetailDiet_viGzPermintaanDiet(); */
							
							gridDTLDiet_GzPermintaanDiet.getView().refresh();
							
							var thisrow = dsTRDetailDietGzPermintaanDiet.getCount()-1; 
							gridDTLDiet_GzPermintaanDiet.startEditing(thisrow, 7);
						},
						insert	: function(o){
							return {
								kd_jenis	: o.kd_jenis,
								jenis_diet	: o.jenis_diet,
								text		:  '<table style="font-size: 11px;"><tr><td width="100">'+o.jenis_diet+'</td></tr></table>'
							}
						},
						url		: baseURL + "index.php/gizi/functionPermintaanDiet/getJenisDiet",
						valueField: 'jenis_diet',
						displayField: 'text',
						listWidth: 150
					})
				},
				{
					header: 'Kategori Diet',
					dataIndex: 'nama_kategori',
					width:150,
					hidden:false,
					editor:new Nci.form.Combobox.autoComplete({
						store	: GzPermintaanDiet.form.ArrayStore.kategoridiet,
						select	: function(a,b,c){
							var line	= gridDTLDiet_GzPermintaanDiet.getSelectionModel().selection.cell[0];
							//console.log(b.data);
							dsTRDetailDietGzPermintaanDiet.getRange()[line].data.kd_kategori	= b.data.kd_kategori;
							dsTRDetailDietGzPermintaanDiet.getRange()[line].data.nama_kategori	= b.data.nama_kategori;
							
							GzPermintaanDiet.vars.kd_kategori	= b.data.kd_kategori;
							
							//loadMask.show();
							
							//saveDetailDiet_viGzPermintaanDiet();
							
							gridDTLDiet_GzPermintaanDiet.getView().refresh();
						},
						insert	: function(o){
							//console.log(o);
							return {
								kd_kategori	: o.kd_kategori,
								nama_kategori	: o.nama_kategori,
								text		:  '<table style="font-size: 11px;"><tr><td width="100">'+o.nama_kategori+'</td></tr></table>'
							}
						},
						url		: baseURL + "index.php/gizi/functionPermintaanDiet/getKategoriDiet",
						valueField: 'nama_kategori',
						displayField: 'text',
						listWidth: 150
					})
				},
				{
					header: 'Jenis Makanan',
					dataIndex: 'nama_jenis_makanan',
					width:150,
					hidden:false,
					/* editor:new Nci.form.Combobox.autoComplete({
						store	: GzPermintaanDiet.form.ArrayStore.bentukmakanan,
						select	: function(a,b,c){
							var line	= gridDTLDiet_GzPermintaanDiet.getSelectionModel().selection.cell[0];
							console.log(b.data);
							dsTRDetailDietGzPermintaanDiet.getRange()[line].data.kd_jenis_makanan	= b.data.kd_jenis_makanan;
							dsTRDetailDietGzPermintaanDiet.getRange()[line].data.nama_jenis_makanan	= b.data.nama_jenis_makanan;
							
							GzPermintaanDiet.vars.kd_jenis_makanan	= b.data.kd_jenis_makanan;
							console.log(b.data);
							gridDTLDiet_GzPermintaanDiet.getView().refresh();
							
							var thisrow = dsTRDetailDietGzPermintaanDiet.getCount()-1; 
							gridDTLDiet_GzPermintaanDiet.startEditing(thisrow, 4);
						},
						insert	: function(o){
							// console.log(o);
							return {
								kd_jenis_makanan	: o.kd_jenis_makanan,
								nama_jenis_makanan		: o.nama_jenis_makanan,
								text		:  '<table style="font-size: 11px;"><tr><td width="100">'+o.nama_jenis_makanan+'</td></tr></table>'
							}
						},
						url		: baseURL + "index.php/gizi/functionPermintaanDiet/getJenisMakanan",
						params 	: {
							kd_pasien : 0
						},
						valueField: 'nama_jenis_makanan',
						displayField: 'nama_jenis_makanan',
						listWidth: 150
					}) */
					editor		: new Ext.form.ComboBox({
						store : ds_jenisMakananPermintaanDiet,
						// valueField : 'kd_jenis_makanan',
            			displayField: 'nama_jenis_makanan',
						mode: 'local',
						typeAhead: false,
						triggerAction: 'all',
						lazyRender: true,
						listeners:
							{ 
								'select': function(a,b,c)
								{
									var kd_jenis_makanan = b.data.kd_jenis_makanan;
									loadData_BentukMananan(kd_jenis_makanan);

									var thisrow = dsTRDetailDietGzPermintaanDiet.getCount()-1; 
									gridDTLDiet_GzPermintaanDiet.startEditing(thisrow, 4);
								},
								'specialkey' : function(a,b,c)
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										
									} 						
								}
							}
					})
				},
				{
					header: 'kd bentuk makanan',
					dataIndex: 'kd_bentuk_makanan',
					width:100,
					hidden:true
				},
				{
					header: 'Bentuk Makanan',
					dataIndex: 'nama_makanan',
					width:150,
					hidden:false,
					/* editor:new Nci.form.Combobox.autoComplete({
						store	: GzPermintaanDiet.form.ArrayStore.bentukmakanan,
						select	: function(a,b,c){
							var line	= gridDTLDiet_GzPermintaanDiet.getSelectionModel().selection.cell[0];
							//console.log(b.data);
							dsTRDetailDietGzPermintaanDiet.getRange()[line].data.kd_bentuk_makanan	= b.data.kd_bentuk_makanan;
							dsTRDetailDietGzPermintaanDiet.getRange()[line].data.nama_makanan	= b.data.nama_makanan;
							
							GzPermintaanDiet.vars.kd_bentuk_makanan	= b.data.kd_bentuk_makanan;
							
							//loadMask.show();
							
							//saveDetailDiet_viGzPermintaanDiet();
							
							gridDTLDiet_GzPermintaanDiet.getView().refresh();
							
							var thisrow = dsTRDetailDietGzPermintaanDiet.getCount()-1; 
							gridDTLDiet_GzPermintaanDiet.startEditing(thisrow, 6);
						},
						insert	: function(o){
							//console.log(o);
							return {
								kd_bentuk_makanan	: o.kd_bentuk_makanan,
								nama_makanan		: o.nama_makanan,
								text		:  '<table style="font-size: 11px;"><tr><td width="100">'+o.nama_makanan+'</td></tr></table>'
							}
						},
						url		: baseURL + "index.php/gizi/functionPermintaanDiet/getBentukMakanan",
						valueField: 'nama_makanan',
						displayField: 'text',
						listWidth: 150
					}) */
					editor		: new Ext.form.ComboBox({
						store : ds_bentukMakananPermintaanDiet,
						// valueField : 'kd_bentuk_makanan',
            			displayField: 'nama_makanan',
						mode: 'local',
						typeAhead: false,
						triggerAction: 'all',
						lazyRender: true,
						listeners:
							{ 
								'select': function(a,b,c)
								{
									//console.log(b.data);
									var kd_bentuk_makanan = b.data.kd_bentuk_makanan;
									loadData_Rute(kd_bentuk_makanan);
									/* Ext.getCmp('cmb_frekuensi').setValue('');
									Ext.getCmp('cmb_volume').setValue(''); */
									//console.log(dsTRDetailDietGzPermintaanDiet);
									loadData_Frekuensi(kd_bentuk_makanan);
									loadData_Volume(kd_bentuk_makanan);

									var thisrow = dsTRDetailDietGzPermintaanDiet.getCount()-1; 
									gridDTLDiet_GzPermintaanDiet.startEditing(thisrow, 6);
								},
								'specialkey' : function(a,b,c)
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										
									} 						
								}
							}
					})
				},
				// {
				// 	header 		: 'TKTP',
				// 	dataIndex 	: 'tktp',
				// 	width		: 100,

				// },
				chkTktp_viGzPermintaanDiet, //HUDI
				{
					header		: 'Rute',
					dataIndex	: 'rute',
					width		: 80,
					editor		: new Ext.form.ComboBox({
						/* store: new Ext.data.SimpleStore({
							id: 0,
							fields: ['abbr', 'action'],
							data: [
								['suspend', 'Suspend'],
								['activate', 'Activate'],
								['update', 'Update'],
								['delete', 'Delete']
							]
						}),
						displayField: 'action',
						valueField: 'abbr', */
						store : ds_rutePermintaanDiet,
						//valueField: 'KD_RUTE',
            			displayField: 'RUTE',
						mode: 'local',
						typeAhead: false,
						triggerAction: 'all',
						lazyRender: true,
						// emptyText: 'Select action'
						listeners:
							{ 
								'select': function(a,b,c)
								{
									var thisrow = dsTRDetailDietGzPermintaanDiet.getCount()-1; 
									gridDTLDiet_GzPermintaanDiet.startEditing(thisrow, 7);
								},
								'specialkey' : function(a,b,c)
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										
									} 						
								}
							}
					})
				},
				{
					header: 'Frekuensi',
					dataIndex: 'frekuensi',
					width:100,
					/* editor: new Ext.form.TextField({
						listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										var thisrow = dsTRDetailDietGzPermintaanDiet.getCount()-1; 
										gridDTLDiet_GzPermintaanDiet.startEditing(thisrow, 8);
									} 						
								}
							}
					}) */
					
					editor		: new Ext.form.ComboBox({
						id				: 'cmb_frekuensi',
						store			: ds_frekuensiPermintaanDiet,
            			displayField	: 'FREKUENSI',
						mode			: 'local',
						typeAhead		: false,
						triggerAction	: 'all',
						lazyRender		: true,
						listeners		:
							{ 
								'select': function(a,b,c)
								{
									var thisrow = dsTRDetailDietGzPermintaanDiet.getCount()-1; 
									gridDTLDiet_GzPermintaanDiet.startEditing(thisrow, 8);
								},
								'specialkey' : function(a,b,c)
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										
									} 						
								}
							}
					})
				},
				{
					header: 'Volume',
					dataIndex: 'volume',
					width:100,
					// editor: new Ext.form.TextField({
					// 	listeners:
					// 		{ 
					// 			'specialkey' : function()
					// 			{
					// 				if (Ext.EventObject.getKey() === 13) 
					// 				{
					// 					var thisrow = dsTRDetailDietGzPermintaanDiet.getCount()-1; 
					// 					gridDTLDiet_GzPermintaanDiet.startEditing(thisrow, 9);
					// 					/* console.log("volume");
					// 					var date 	= new Date();
					// 					var jam 	= date.format("H");
					// 					console.log(jam); 
					// 					var line	= gridDTLDiet_GzPermintaanDiet.getSelectionModel().selection.cell[0];
					// 					//dsTRDetailDietGzPermintaanDiet.getRange()[line].data.kd_waktu=b.data.kd_waktu;
					// 					dsTRDetailDietGzPermintaanDiet.getRange()[line].data.waktu = jam;
										
					// 					//GzPermintaanDiet.vars.kd_waktu=b.data.kd_waktu;
										
					// 					gridDTLDiet_GzPermintaanDiet.getView().refresh(); */
					// 				} 						
					// 			}
					// 		}
					// })
					editor		: new Ext.form.ComboBox({
						id				: 'cmb_volume',
						store			: ds_volumePermintaanDiet,
            			displayField	: 'VOLUME',
						mode			: 'local',
						typeAhead		: false,
						triggerAction	: 'all',
						lazyRender		: true,
						listeners		:
							{ 
								'select': function(a,b,c)
								{
									var thisrow = dsTRDetailDietGzPermintaanDiet.getCount()-1; 
									gridDTLDiet_GzPermintaanDiet.startEditing(thisrow, 9);
								},
								'specialkey' : function(a,b,c)
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										
									} 						
								}
							}
					})
				},
				// {			
				// 	dataIndex: 'jenis_diet',
				// 	header: 'Jenis Diet',
				// 	sortable: false,
				// 	width: 150,	
				// 	editor:new Nci.form.Combobox.autoComplete({
				// 		store	: GzPermintaanDiet.form.ArrayStore.jenisdiet,
				// 		select	: function(a,b,c){
				// 			var line	= gridDTLDiet_GzPermintaanDiet.getSelectionModel().selection.cell[0];
				// 			dsTRDetailDietGzPermintaanDiet.getRange()[line].data.kd_jenis=b.data.kd_jenis;
				// 			dsTRDetailDietGzPermintaanDiet.getRange()[line].data.jenis_diet=b.data.jenis_diet;
							
				// 			GzPermintaanDiet.vars.kd_jenis=b.data.kd_jenis;
							
				// 			 loadMask.show();
							
				// 			saveDetailDiet_viGzPermintaanDiet(); 
							
				// 			gridDTLDiet_GzPermintaanDiet.getView().refresh();
							
				// 			var thisrow = dsTRDetailDietGzPermintaanDiet.getCount()-1; 
				// 			gridDTLDiet_GzPermintaanDiet.startEditing(thisrow, 14);
				// 		},
				// 		insert	: function(o){
				// 			return {
				// 				kd_jenis	: o.kd_jenis,
				// 				jenis_diet	: o.jenis_diet,
				// 				text		:  '<table style="font-size: 11px;"><tr><td width="100">'+o.jenis_diet+'</td></tr></table>'
				// 			}
				// 		},
				// 		url		: baseURL + "index.php/gizi/functionPermintaanDiet/getJenisDiet",
				// 		valueField: 'jenis_diet',
				// 		displayField: 'text',
				// 		listWidth: 150
				// 	})
				// },
				{
					header: 'kd kategori',
					dataIndex: 'kd_kategori',
					width:100,
					hidden:true
				},
				{
					header: 'Kategori Diet',
					dataIndex: 'nama_kategori',
					width:150,
					hidden:true,
					editor:new Nci.form.Combobox.autoComplete({
						store	: GzPermintaanDiet.form.ArrayStore.kategoridiet,
						select	: function(a,b,c){
							var line	= gridDTLDiet_GzPermintaanDiet.getSelectionModel().selection.cell[0];
							//console.log(b.data);
							dsTRDetailDietGzPermintaanDiet.getRange()[line].data.kd_kategori	= b.data.kd_kategori;
							dsTRDetailDietGzPermintaanDiet.getRange()[line].data.nama_kategori	= b.data.nama_kategori;
							
							GzPermintaanDiet.vars.kd_kategori	= b.data.kd_kategori;
							
							//loadMask.show();
							
							//saveDetailDiet_viGzPermintaanDiet();
							
							gridDTLDiet_GzPermintaanDiet.getView().refresh();

							var thisrow = dsTRDetailDietGzPermintaanDiet.getCount()-1; 
							gridDTLDiet_GzPermintaanDiet.startEditing(thisrow, 10);
						},
						insert	: function(o){
							//console.log(o);
							return {
								kd_kategori	: o.kd_kategori,
								nama_kategori	: o.nama_kategori,
								text		:  '<table style="font-size: 11px;"><tr><td width="100">'+o.nama_kategori+'</td></tr></table>'
							}
						},
						url		: baseURL + "index.php/gizi/functionPermintaanDiet/getKategoriDiet",
						valueField: 'nama_kategori',
						displayField: 'text',
						listWidth: 150
					})
				},
				{
					header: 'kd jenis',
					dataIndex: 'kd_jenis',
					width:100,
					hidden:true
				},
				{
					header: 'kd pasien',
					dataIndex: 'kd_pasien',
					width:100,
					hidden:true
				},
				{
					header: 'Alergi',
					dataIndex: 'alergi',
					width:100,
					hidden:false,
					editor: new Ext.form.TextField({
						listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										
									} 						
								}
							}
					})
				},
				{
					header: 'Keterangan Diet',
					dataIndex: 'diet',
					width:100,
					hidden:false,
					editor: new Ext.form.TextField({
						listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										
									} 						
								}
							}
					})
				}
            ],
			//viewConfig: {forceFit: true}
			//HUDI
			//16-11-2020
			plugins:[new Ext.ux.grid.FilterRow(),
				chkTktp_viGzPermintaanDiet],
			viewConfig:{
				forceFit: true
			}
			
	 
		}
    );
    return gridDTLDiet_GzPermintaanDiet;
};


function ComboAhliGiziGzPermintaanDietLookup()
{
    var Field_ahliGiziLookup = ['KD_AHLI_GIZI', 'NAMA_AHLI_GIZI'];
    ds_ahliGiziGZPermintaanLookup = new WebApp.DataStore({fields: Field_ahliGiziLookup});
    ds_ahliGiziGZPermintaanLookup.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'KD_AHLI_GIZI',
					Sortdir: 'ASC',
					target: 'ComboAhliGizi',
					param: "perawat = 'false' or perawat is null"
				}
		}
	);
	
    var cbo_AhliGiziGzPermintaanDietLookup = new Ext.form.ComboBox
    (
        {
			x: 420,
			y: 0,
            flex: 1,
			id: 'cbo_AhliGiziGzPermintaanDietLookup',
			valueField: 'KD_AHLI_GIZI',
            displayField: 'NAMA_AHLI_GIZI',
			store: ds_ahliGiziGZPermintaanLookup,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:150,
			tabIndex:2,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					Ext.getCmp('btnAddPasienGzPermintaanDietL').enable();
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_AhliGiziGzPermintaanDietLookup;
};

function ComboAhliGiziGzPermintaanDiet()
{
    var Field_ahliGizi = ['KD_AHLI_GIZI', 'NAMA_AHLI_GIZI'];
    ds_ahliGiziGZPermintaan = new WebApp.DataStore({fields: Field_ahliGizi});
    ds_ahliGiziGZPermintaan.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'KD_AHLI_GIZI',
					Sortdir: 'ASC',
					target: 'ComboAhliGizi',
					param: ''
				}
		}
	);
	
    var cbo_AhliGiziGzPermintaanDiet = new Ext.form.ComboBox
    (
        {
			x: 130,
			y: 30,
            flex: 1,
			id: 'cbo_AhliGiziGzPermintaanDiet',
			valueField: 'KD_AHLI_GIZI',
            displayField: 'NAMA_AHLI_GIZI',
			store: ds_ahliGiziGZPermintaan,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:150,
			tabIndex:2,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						tmpkriteria = getCriteriaCariGzPermintaanDiet();
						refreshPermintaanDiet(tmpkriteria);
					} 						
				}
			}
        }
    )    
    return cbo_AhliGiziGzPermintaanDiet;
};


function ComboUnitGzPermintaanDiet(){
    var Field_Vendor = ['KD_UNIT', 'NAMA_UNIT'];
    ds_unit = new WebApp.DataStore({fields: Field_Vendor});
    ds_unit.load({
        params:{
	        Skip: 0,
	        Take: 1000,
	        Sort: 'kd_unit',
	        Sortdir: 'ASC',
	        target: 'ComboUnitGizi',
	        param: "parent='1001'"
        }
    });
    var cbo_UnitGzPermintaanDiet = new Ext.form.ComboBox({
			x:410,
			y:0,
            flex: 1,
			id: 'cbo_UnitGzPermintaanDiet',
			valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
			emptyText:'Unit/Ruang',
			store: ds_unit,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:150,
			tabIndex:2,
			listeners:{ 
				'select': function(a,b,c){
					selectSetUnit=b.data.displayText ;
				},
				'specialkey' : function(){
					if (Ext.EventObject.getKey() === 13) {
						tmpkriteria = getCriteriaCariGzPermintaanDiet();
						refreshPermintaanDiet(tmpkriteria);
					} 						
				}
			}
        }
    )    
    return cbo_UnitGzPermintaanDiet;
}

function ComboUnitGzPermintaanDietLookup()
{
    var Field_Vendor = ['KD_UNIT', 'NAMA_UNIT'];
    ds_unitL = new WebApp.DataStore({fields: Field_Vendor});
    ds_unitL.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'kd_unit',
					Sortdir: 'ASC',
					target: 'ComboUnitGizi',
					param: "parent='1001'"
				}
		}
	);
	
    var cbo_UnitGzPermintaanDietLookup = new Ext.form.ComboBox
    (
        {
			x:130,
			y:60,
			id: 'cbo_UnitGzPermintaanDietLookup',
			store: ds_unitL,
			valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:150,
			tabIndex:3,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_UnitGzPermintaanDietLookup;
}

function datainit_viGzPermintaanDiet(rowdata)
{
	var tgl_minta2 = rowdata.tgl_minta.split(" ");
	var tgl_makan2 = rowdata.tgl_makan.split(" ");
	Ext.getCmp('txtTmpNoPermintaanGzPermintaanDietL').setValue(rowdata.no_minta);
	Ext.getCmp('txtNoPermintaanGzPermintaanDietL').setValue(rowdata.no_minta);
	Ext.getCmp('dfTglMintaGzPermintaanDietL').setValue(tgl_minta2[0]);
	Ext.getCmp('cbo_UnitGzPermintaanDietLookup').setValue(rowdata.nama_unit);
	Ext.getCmp('dfTglUntukGzPermintaanDietL').setValue(tgl_makan2[0]);
	Ext.getCmp('cbo_AhliGiziGzPermintaanDietLookup').setValue(rowdata.nama_ahli_gizi);
	Ext.getCmp('txtKetGZPenerimaanL').setValue(rowdata.keterangan);
	GzPermintaanDiet.vars.kd_unit=rowdata.kd_unit
	
	Ext.getCmp('txtNoPermintaanGzPermintaanDietL').disable();
	Ext.getCmp('dfTglMintaGzPermintaanDietL').disable();
	Ext.getCmp('cbo_UnitGzPermintaanDietLookup').disable();
	Ext.getCmp('dfTglUntukGzPermintaanDietL').disable();
	Ext.getCmp('cbo_AhliGiziGzPermintaanDietLookup').disable();
	Ext.getCmp('txtKetGZPenerimaanL').disable();	
	
	getGridPasien(rowdata.no_minta);
	cekOrder(rowdata.no_minta);
	
};

function dataGriAwal(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionPermintaanDiet/getDataGridAwal",
			params: {
				text:'',
				// perawat : " AND perawat IS NULL "
				perawat : ""
			},
			failure: function(o)
			{
				ShowPesanErrorGzPermintaanDiet('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dataSource_viGzPermintaanDiet.removeAll();
					var recs=[],
						recType=dataSource_viGzPermintaanDiet.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
						dataSource_viGzPermintaanDiet.add(recs);
					
					
					
					GridDataView_viGzPermintaanDiet.getView().refresh();
				}
				else 
				{
					ShowPesanErrorGzPermintaanDiet('Gagal membaca data obat', 'Error');
				};
			}
		}
		
	)
	
}


function getGridPasien(no_minta){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionPermintaanDiet/getGridPasien",
			params: {nominta:no_minta},
			failure: function(o)
			{
				ShowPesanErrorGzPermintaanDiet('Error, pasien! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dsDataGrdPasien_viGzPermintaanDiet.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
					dsDataGrdPasien_viGzPermintaanDiet.add(recs);
					
					GzPermintaanDiet.form.Grid.a.getView().refresh();
				}
				else 
				{
					ShowPesanErrorGzPermintaanDiet('Gagal membaca data pasien ini', 'Error');
				};
			}
		}
		
	)
	
}

function getGridWaktu(no_minta,kd_pasien){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionPermintaanDiet/getGridWaktu",
			params: {nominta:no_minta, kdpasien:kd_pasien},
			failure: function(o)
			{
				ShowPesanErrorGzPermintaanDiet('Error, detail diet! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				//HUDI
				//02-09-2020
				//=============== disable grid detail =============
				if(cst.totalrecords > 0){
					gridDTLDiet_GzPermintaanDiet.disable(true);
					Ext.getCmp('btnAddDetDietGzPermintaanDietL').disable();
					Ext.getCmp('btnDeleteDet_viGzPermintaanDiet').disable();
					Ext.getCmp('btnSaveDiet_viGzPermintaanDiet').disable();
				}
				//=================================================
				if (cst.success === true) 
				{
					var recs=[],
						recType=dsTRDetailDietGzPermintaanDiet.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						if(cst.ListDataObj[i].tktp == 0 ||cst.ListDataObj[i].tktp == "0"){
							cst.ListDataObj[i].tktp = false;
						}else{
							cst.ListDataObj[i].tktp = true;
						}
						//console.log(cst.ListDataObj[i]);
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
					dsTRDetailDietGzPermintaanDiet.add(recs);
					
					gridDTLDiet_GzPermintaanDiet.getView().refresh();
				}
				else 
				{
					ShowPesanErrorGzPermintaanDiet('Gagal membaca data detail diet pasien ini', 'Error');
				};
			}
		}
		
	)
	
}


function cekOrder(no_minta){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionPermintaanDiet/cekOrderPermintaanDiet",
			params: {nominta:no_minta},
			failure: function(o)
			{
				ShowPesanErrorGzPermintaanDiet('Error, cek order! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					Ext.getCmp('btnAddDetDietGzPermintaanDietL').enable();
					Ext.getCmp('btnDelete_viGzPermintaanDiet').enable();
					//Ext.getCmp('btnDeletePermintaan_viGzPermintaanDiet').enable();
					GzPermintaanDiet.vars.status_order='false';
				}
				else 
				{
					ShowPesanInfoGzPermintaanDiet('Permintaan diet ini sudah di order', 'Information');
					Ext.getCmp('btnDeletePermintaan_Perawat_viGzPermintaanDiet').disable();
					GzPermintaanDiet.vars.status_order='true';
				};
			}
		}
		
	)
}

function saveMinta_viGzPermintaanDiet(){
	if (ValidasiEntryPermintaanGzPermintaanDiet(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/gizi/functionPermintaanDiet/saveMinta",
				params: getParamMintaGzPermintaanDiet(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorGzPermintaanDiet('Error, permintaan! Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						Ext.getCmp('txtTmpNoPermintaanGzPermintaanDietL').setValue(cst.nominta);
						Ext.getCmp('txtNoPermintaanGzPermintaanDietL').setValue(cst.nominta);
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorGzPermintaanDiet('Gagal menyimpan data permintaan', 'Error');
					};
				}
			}
		)
	}
}


function savePasien_viGzPermintaanDiet(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionPermintaanDiet/saveMintaPasien",
			params: getParamMintaPasienGzPermintaanDiet(),
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorGzPermintaanDiet('Error, pasien! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					loadMask.hide();
					Ext.getCmp('txtTmpKdPasienGzPermintaanDietL').setValue(cst.kdpasien);
					if(cst.error=='ada'){
						ShowPesanWarningGzPermintaanDiet('Pasien ini susah ada dalam transaksi pemintaan diet hari ini, hapus pasien ini untuk melanjutkan', 'Warning');
						Ext.getCmp('btnDelete_viGzPermintaanDiet').enable();
						Ext.getCmp('btnAddPasienGzPermintaanDietL').disable();
						Ext.getCmp('btnAddDetDietGzPermintaanDietL').disable();
					} else{
						if(GzPermintaanDiet.vars.status_order == 'false'){
							Ext.getCmp('btnAddPasienGzPermintaanDietL').disable();
							Ext.getCmp('btnDelete_viGzPermintaanDiet').disable();
							Ext.getCmp('btnAddDetDietGzPermintaanDietL').enable();
						} else{
							Ext.getCmp('btnAddPasienGzPermintaanDietL').disable();
							Ext.getCmp('btnDelete_viGzPermintaanDiet').disable();
							Ext.getCmp('btnAddDetDietGzPermintaanDietL').disable();
						}
					}
				}
				else 
				{
					loadMask.hide();
					ShowPesanErrorGzPermintaanDiet('Gagal menyimpan data pasien', 'Error');
				};
			}
		}
	)
}

function saveDetailDiet_viGzPermintaanDiet(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionPermintaanDiet/saveMintaPasienDetail",
			params: getParamDetailDietGzPermintaanDiet(),
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorGzPermintaanDiet('Error, detail diet! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					//console.log(cst.update);
					loadMask.hide();
					//HUDI
					//02-09-2020
					if(cst.update == false || cst.update == 'false' || cst.update == "false"){
						ShowPesanInfoGzPermintaanDiet('Data berhasil disimpan','Information');
					}else{
						ShowPesanInfoGzPermintaanDiet('Data berhasil diupdate','Information');
					}
					dataGriAwal()
						Ext.getCmp('btnAddPasienGzPermintaanDietL').disable();
						Ext.getCmp('btnAddDetDietGzPermintaanDietL').disable();
						Ext.getCmp('btnDeleteDet_viGzPermintaanDiet').enable();
						Ext.getCmp('btnEditDiet_viGzPermintaanDiet').enable();
						gridDTLDiet_GzPermintaanDiet.disable(true);

						//28-10-2020
						dsDataGrdPasien_viGzPermintaanDiet.removeAll();
						getGridPasien(cst.no_minta);

					/* if(cst.ada=='0'){
						loadMask.hide();
						dataGriAwal()
						Ext.getCmp('btnAddPasienGzPermintaanDietL').enable();
						Ext.getCmp('btnDeleteDet_viGzPermintaanDiet').enable();
						Ext.getCmp('btnEditDiet_viGzPermintaanDiet').enable();
						Ext.getCmp('txtNoPermintaanGzPermintaanDietL').setValue(Ext.getCmp('txtTmpNoPermintaanGzPermintaanDietL').getValue());
					} else{
						loadMask.hide();
						ShowPesanWarningGzPermintaanDiet('Jenis diet ini sudah ada dengan waktu yang sama, hapus salah satu untuk melanjutkan', 'Error');
						Ext.getCmp('btnAddPasienGzPermintaanDietL').disable();
						Ext.getCmp('btnAddDetDietGzPermintaanDietL').disable();
						Ext.getCmp('btnDeleteDet_viGzPermintaanDiet').enable();
						Ext.getCmp('btnEditDiet_viGzPermintaanDiet').enable();
					} */
				}
				else 
				{
					loadMask.hide();
					ShowPesanErrorGzPermintaanDiet('Gagal menyimpan data detail diet', 'Error');
				};
			}
		}
	)
}


function printbill()
{
    Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/main/CreateDataObj",
			params: datacetakbill(),
			failure: function(o)
			{	
				ShowPesanErrorGzPermintaanDiet('Error hubungi admin' ,'Error');
			},
			success: function(o)
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					
				}
				else if  (cst.success === false && cst.pesan===0)
				{
					ShowPesanWarningGzPermintaanDiet('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
				}
				else
				{
					ShowPesanErrorGzPermintaanDiet('Gagal print '  + 'Error');
				}
			}
		}
	)
}

function getParamMintaGzPermintaanDiet() 
{
    var params =
	{	
		NoMinta		: Ext.getCmp('txtNoPermintaanGzPermintaanDietL').getValue(),
		KdUnit		: Ext.getCmp('cbo_UnitGzPermintaanDietLookup').getValue(),
		TglMinta	:Ext.getCmp('dfTglMintaGzPermintaanDietL').getValue(),
		TglMakan	:Ext.getCmp('dfTglUntukGzPermintaanDietL').getValue(),
		AhliGizi	:Ext.getCmp('cbo_AhliGiziGzPermintaanDietLookup').getValue(),
		Ket			:Ext.getCmp('txtKetGZPenerimaanL').getValue()
	};
    return params
};

function getParamMintaPasienGzPermintaanDiet() 
{
    var params =
	{
		NoMinta:Ext.getCmp('txtTmpNoPermintaanGzPermintaanDietL').getValue(),
		KdUnit: Ext.getCmp('cbo_UnitGzPermintaanDietLookup').getValue(),
		KdPasien:GzPermintaanDiet.vars.kd_pasien,
		TglMasuk:GzPermintaanDiet.vars.tgl_masuk,
		NoKamar:GzPermintaanDiet.vars.no_kamar
	};
	
    return params
};

function getParamDetailDietGzPermintaanDiet() 
{
	console.log(dsTRDetailDietGzPermintaanDiet.getRange()[0].data);
	console.log(GzPermintaanDiet.vars);
	var params =
	{
		NoMinta		: Ext.getCmp('txtTmpNoPermintaanGzPermintaanDietL').getValue(),
		TglMinta	: Ext.getCmp('dfTglMintaGzPermintaanDietL').getValue(),
		KdPasien	: Ext.getCmp('txtTmpKdPasienGzPermintaanDietL').getValue(),
		kd_jenis	: GzPermintaanDiet.vars.kd_jenis,
		kd_kategori : GzPermintaanDiet.vars.kd_kategori,
		kd_waktu	: GzPermintaanDiet.vars.kd_waktu,
		waktu		: dsTRDetailDietGzPermintaanDiet.getRange()[0].data.waktu,
		//kd_kategori	: GzPermintaanDiet.vars.kd_kategori,
		kd_bentuk_makanan : GzPermintaanDiet.vars.kd_bentuk_makanan,
		kd_jenis_makanan : GzPermintaanDiet.vars.kd_jenis_makanan,
		bentuk_makanan : dsTRDetailDietGzPermintaanDiet.getRange()[0].data.nama_makanan,
		jenis_makanan : dsTRDetailDietGzPermintaanDiet.getRange()[0].data.nama_jenis_makanan,
		frekuensi : dsTRDetailDietGzPermintaanDiet.getRange()[0].data.frekuensi,
		volume : dsTRDetailDietGzPermintaanDiet.getRange()[0].data.volume,
		edit_permintaan : edit_detail_permintaan,
		rute	: dsTRDetailDietGzPermintaanDiet.getRange()[0].data.rute,
		alergi	: dsTRDetailDietGzPermintaanDiet.getRange()[0].data.alergi,
		diet	: dsTRDetailDietGzPermintaanDiet.getRange()[0].data.diet,
		tktp	: dsTRDetailDietGzPermintaanDiet.getRange()[0].data.tktp,
	};
	
    return params
};

function datacetakbill(){
	var params =
	{
		Table: 'billprintingGzPermintaanDiet',
		NoResep:Ext.getCmp('txtNoResepGzPermintaanDietL').getValue(),
		NoOut:Ext.getCmp('txtTmpNooutGzPermintaanDietL').getValue(),
		TglOut:Ext.getCmp('txtTmpTgloutGzPermintaanDietL').getValue(),
		KdPasien:Ext.getCmp('txtKdPasienGzPermintaanDietL').getValue(),
		NamaPasien:GzPermintaanDiet.form.ComboBox.namaPasien.getValue(),
		JenisPasien:Ext.get('cboPilihankelompokPasienAptGzPermintaanDiet').getValue(),
		Kelas:Ext.get('cbo_UnitGzPermintaanDietL').getValue(),
		Dokter:Ext.get('cbo_DokterGzPermintaanDiet').getValue(),
		Total:Ext.get('txtTotalBayarGzPermintaanDietL').getValue(),
		Tot:toInteger(Ext.get('txtTotalBayarGzPermintaanDietL').getValue())
		
	}
	params['jumlah']=dsDataGrdPasien_viGzPermintaanDiet.getCount();
	for(var i = 0 ; i < dsDataGrdPasien_viGzPermintaanDiet.getCount();i++)
	{
		params['nama_obat-'+i]=dsDataGrdPasien_viGzPermintaanDiet.data.items[i].data.nama_obat;
		params['jml-'+i]=dsDataGrdPasien_viGzPermintaanDiet.data.items[i].data.jml;
	}
	
    return params
}

function getCriteriaCariGzPermintaanDiet()//^^^
{
      	 var strKriteria = "";

			if (Ext.getCmp('TxtNoPermintaanFilterGridDataView_viGzPermintaanDiet').getValue() != "")
            {
                strKriteria = " m.no_minta " + "LIKE upper('" + Ext.getCmp('TxtNoPermintaanFilterGridDataView_viGzPermintaanDiet').getValue() +"%')";
            }
            
            if (Ext.get('cbo_AhliGiziGzPermintaanDiet').getValue() != "")//^^^
            {
				if (strKriteria == "")
				{
					strKriteria = " lower(a.nama_ahli_gizi) " + "LIKE lower('" + Ext.get('cbo_AhliGiziGzPermintaanDiet').getValue() +"%')" ;
				}
				else {
					strKriteria += " AND lower(a.nama_ahli_gizi) " + "LIKE lower('" + Ext.get('cbo_AhliGiziGzPermintaanDiet').getValue() +"%')')";
				}
            }
			
			if (Ext.getCmp('cbo_UnitGzPermintaanDiet').getValue() != "" && Ext.get('cbo_UnitGzPermintaanDiet').getValue() != 'Unit/Ruang')
            {
				if (strKriteria == "")
				{
					strKriteria = " m.kd_unit ='" + Ext.getCmp('cbo_UnitGzPermintaanDiet').getValue() + "'"  ;
				}
				else {
					strKriteria += " and m.kd_unit ='" + Ext.getCmp('cbo_UnitGzPermintaanDiet').getValue() + "'";
			    }
                
            }
	
			
			if (Ext.getCmp('dfTglAwalGzPermintaanDiet').getValue() != "" && Ext.getCmp('dfTglAkhirGzPermintaanDiet').getValue() != "")
            {
                if (strKriteria == "")
				{
					strKriteria = " m.tgl_minta between '" + Ext.get('dfTglAwalGzPermintaanDiet').getValue() + "' and '" + Ext.get('dfTglAkhirGzPermintaanDiet').getValue() + "'" ;
				}
				else {
					strKriteria += " AND m.tgl_minta between '" + Ext.get('dfTglAwalGzPermintaanDiet').getValue() + "' and '" + Ext.get('dfTglAkhirGzPermintaanDiet').getValue() + "'" ;
				}
                
            }
			
		// strKriteria= strKriteria + " AND perawat IS NULL ORDER BY m.no_minta";
		strKriteria= strKriteria + " ORDER BY m.no_minta";
	 return strKriteria;
}

function ValidasiEntryPermintaanGzPermintaanDiet(modul,mBolHapus)
{
	var x = 1;
	if(Ext.getCmp('cbo_UnitGzPermintaanDietLookup').getValue() === '' || Ext.getCmp('cbo_AhliGiziGzPermintaanDietLookup').getValue() === '' ||(Ext.getCmp('dfTglUntukGzPermintaanDietL').getValue() < Ext.getCmp('dfTglMintaGzPermintaanDietL').getValue()) ){
		if(Ext.getCmp('cbo_UnitGzPermintaanDietLookup').getValue() === ''){
			loadMask.hide();
			ShowPesanWarningGzPermintaanDiet('Unit tidak boleh kosong', 'Warning');
			x = 0;
		} else if(Ext.getCmp('cbo_AhliGiziGzPermintaanDietLookup').getValue() === ''){
			loadMask.hide();
			ShowPesanWarningGzPermintaanDiet('Ahli gizi tidak boleh kosong', 'Warning');
			x = 0;
		} else {
			loadMask.hide();
			ShowPesanWarningGzPermintaanDiet('Untuk tanggal tidak boleh kurang dari tanggal permintaan', 'Warning');
			x = 0;
		} 
	}
	return x;
};

function dataAddNew_viGzPermintaanDiet(){
	Ext.getCmp('txtTmpNoPermintaanGzPermintaanDietL').setValue('');
	Ext.getCmp('txtNoPermintaanGzPermintaanDietL').setValue('');
	Ext.getCmp('dfTglMintaGzPermintaanDietL').setValue(now_viGzPermintaanDiet);
	Ext.getCmp('cbo_UnitGzPermintaanDietLookup').setValue('');
	Ext.getCmp('dfTglUntukGzPermintaanDietL').setValue(now_viGzPermintaanDiet);
	Ext.getCmp('cbo_AhliGiziGzPermintaanDietLookup').setValue('');
	Ext.getCmp('txtKetGZPenerimaanL').setValue('');
	Ext.getCmp('txtTmpKdPasienGzPermintaanDietL').setValue('');
	GzPermintaanDiet.vars.kd_unit='';
	dsDataGrdPasien_viGzPermintaanDiet.removeAll();
	dsTRDetailDietGzPermintaanDiet.removeAll();
	
	
	Ext.getCmp('txtNoPermintaanGzPermintaanDietL').enable();
	Ext.getCmp('dfTglMintaGzPermintaanDietL').enable();
	Ext.getCmp('cbo_UnitGzPermintaanDietLookup').enable();
	Ext.getCmp('dfTglUntukGzPermintaanDietL').enable();
	Ext.getCmp('cbo_AhliGiziGzPermintaanDietLookup').enable();
	Ext.getCmp('txtKetGZPenerimaanL').enable();
	Ext.getCmp('btnDeletePermintaan_viGzPermintaanDiet').disable();
	Ext.getCmp('btnAddPasienGzPermintaanDietL').disable();
	
}


function ShowPesanWarningGzPermintaanDiet(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:290
		}
	);
};

function ShowPesanErrorGzPermintaanDiet(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:290
		}
	);
};


function ShowPesanInfoGzPermintaanDiet(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:290
		}
	);
};

//HUDI
//17-09-2020
//Form Permintaan Untuk Perawat
//=============================================================================
function setLookUpPerawat_viGzPermintaanDiet(rowdata){
    var lebar = 1090;
    setLookUps_viGzPermintaanDiet = new Ext.Window({
        id: Nci.getId(),
        title: NamaFormPerawat_viGzPermintaanDiet, 
        closeAction: 'destroy',        
        width: 1100,
        height: 580,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntryPerawat_viGzPermintaanDiet(lebar,rowdata),
        listeners:{
            activate: function(){
				
            },
            afterShow: function(){
				this.activate();
            },
            deactivate: function(){
				console.log("close");
				refreshPermintaanDiet();
                rowSelected_viGzPermintaanDiet=undefined;
			}
        }
    });

    setLookUps_viGzPermintaanDiet.show();
    if (rowdata == undefined){	
    }
    else
    {
        datainitPerawat_viGzPermintaanDiet(rowdata);
    }
};

function getFormItemEntryPerawat_viGzPermintaanDiet(lebar,rowdata){
	//HUDI
	//30-10-2020
	//Perawat disable button posting dan unposting ==============================
	if(rowdata != undefined){
		var btn_deletepermintaan;
		var btn_posting;
		var btn_unposting;
		if(rowdata.status_posting == '0'){
			btn_deletepermintaan = false;
			btn_posting 	= false;
			btn_unposting 	= true;
		}else{
			btn_deletepermintaan = true;
			btn_posting 	= true;
			btn_unposting 	= false;
		}
	}
	//===========================================================================
    var pnlFormDataBasicPerawat_viGzPermintaanDiet = new Ext.FormPanel({
		title: '',
		region: 'north',
		layout: 'form',
		bodyStyle: 'padding:10px 10px 10px 10px',
		anchor: '100%',
		labelWidth: 1,
		autoWidth: true,
		width: lebar,
		height : 850,
		border: false,
		items:[
				getItemPanelInputPermintaanPerawat_viGzPermintaanDiet(lebar,rowdata),
				getItemGridPasienPerawat_viGzPermintaanDiet(lebar,rowdata),
				getItemGridDetailPerawat_viGzPermintaanDiet(lebar,rowdata)
			],
			fileUpload: true,
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAdd_Perawat_viGzPermintaanDiet',
						handler: function(){
							gridDTLDiet_GzPermintaanDiet.enable(true);
							dataAddNewPerawatan_viGzPermintaanDiet();
						}
					},{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						id:'btnDeletePermintaan_Perawat_viGzPermintaanDiet',
						iconCls: 'remove',
						disabled : btn_deletepermintaan,
						handler:function()
						{
							Ext.Msg.confirm('Warning', 'Apakah data permintaan ini akan di hapus?', function(button){
								if (button == 'yes'){
									loadMask.show();
									Ext.Ajax.request
									(
										{
											url: baseURL + "index.php/gizi/functionPermintaanDiet/deletePermintaan",
											params: {no_minta:Ext.getCmp('txtNoPermintaanPerawatGzPermintaanDietL').getValue()},
											failure: function(o)
											{
												loadMask.hide();
												ShowPesanErrorGzPermintaanDiet('Error, delete permintaan! Hubungi Admin', 'Error');
											},	
											success: function(o) 
											{
												var cst = Ext.decode(o.responseText);
												if (cst.success === true) 
												{
													loadMask.hide();
													//getDataGridAwal();
													dataGriAwal();
													ShowPesanInfoGzPermintaanDiet('Penghapusan berhasil','Information');
													setLookUps_viGzPermintaanDiet.close();
												}
												else 
												{
													if(cst.order =='true'){
														loadMask.hide();
														ShowPesanWarningGzPermintaanDiet('Gagal menghapus data ini. Permintaan diet ini sudah diorder', 'Warning');
													} else{
														loadMask.hide();
														ShowPesanWarningGzPermintaanDiet('Gagal menghapus data ini', 'Warning');
													}
													
												};
											}
										}
										
									)
								}
							});
						}
						  
					},
					//HUDI
					//30-10-2020
					//Perawat Form ===========================================================================================
					{
						xtype: 'tbseparator'
					},
					{
						xtype : 'button',
						text : 'Posting',
						id : 'btnPostingPermintaanPerawat_viGzPermintaanDiet',
						disabled : btn_posting,
						handler : function(){
							var str_noMinta = Ext.getCmp('txtNoPermintaanPerawatGzPermintaanDietL').getValue();
							if(str_noMinta != '' || str_noMinta != ""){
								Ext.Ajax.request({
									url: baseURL + "index.php/gizi/functionPermintaanDiet/posting",
									params: {
										no_minta : str_noMinta
									},
									failure: function(o)
									{
										loadMask.hide();
										ShowPesanErrorGzPermintaanDiet('Error, permintaan! Hubungi Admin', 'Error');
									},	
									success: function(o) 
									{
										var cst = Ext.decode(o.responseText);
										if(cst.success == true || cst.success == 'true'){
											Ext.getCmp('btnUnpostingPermintaanPerawat_viGzPermintaanDiet').enable(true);
											Ext.getCmp('btnPostingPermintaanPerawat_viGzPermintaanDiet').disable(true);
											Ext.getCmp('btnDeletePermintaan_Perawat_viGzPermintaanDiet').disable(true);
											HideButtonPermintaanDietPerawat(true);
											ShowPesanInfoGzPermintaanDiet('Berhasil Posting.', 'Success');
										}
									}
								})
							}else{
								ShowPesanWarningGzPermintaanDiet('No minta masih kosong!', 'Warning');
							}
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype : 'button',
						text : 'Unposting',
						id : 'btnUnpostingPermintaanPerawat_viGzPermintaanDiet',
						disabled : btn_unposting,
						handler : function(){
							var str_noMinta = Ext.getCmp('txtNoPermintaanPerawatGzPermintaanDietL').getValue();
							if(str_noMinta != '' || str_noMinta != ""){
								Ext.MessageBox.show({
									title: 'Peringatan',
									msg: 'Anda yakin untuk unposting?',
									buttons: Ext.MessageBox.YESNO,
									icon: Ext.MessageBox.WARNING,
									fn: function(btn){
										if(btn == 'yes'){
											var dlg = Ext.MessageBox.prompt('Permintan diet', 'Masukan keterangan unposting:', function (btn, combo) {
												if (btn == 'ok'){
													// console.log(btn);
													// console.log(combo);
													Ext.Ajax.request({
														url: baseURL + "index.php/gizi/functionPermintaanDiet/unposting",
														params: {
															no_minta		: str_noMinta,
															ket_unposting	: combo
														},
														failure: function(o)
														{
															loadMask.hide();
															ShowPesanErrorGzPermintaanDiet('Error, permintaan! Hubungi Admin', 'Error');
														},	
														success: function(o) 
														{
															var cst = Ext.decode(o.responseText);
															if(cst.success == true || cst.success == 'true'){
																Ext.getCmp('btnUnpostingPermintaanPerawat_viGzPermintaanDiet').disable(true);
																Ext.getCmp('btnPostingPermintaanPerawat_viGzPermintaanDiet').enable(true);
																Ext.getCmp('btnDeletePermintaan_Perawat_viGzPermintaanDiet').enable(true);
																ShowButtonPermintaanDietPerawat(true);
																ShowPesanInfoGzPermintaanDiet('Berhasil Unposting.', 'Success');
															}
														}
													})
												}
											});
										}
									}
								});
							}else{
								ShowPesanWarningGzPermintaanDiet('No minta masih kosong!', 'Warning');
							}
						}
					},
					{
						xtype: 'tbseparator'
					},
					//==========================================================================================================
					{
						xtype:'splitbutton',
						text:'Print',
						iconCls:'print',
						id:'btnPrint_viGzPermintaanDiet',
						disabled:true,
						hidden:true,
						handler:function()
						{							
								
						},
						menu: new Ext.menu.Menu({
						items: [
							// these items will render as dropdown menu items when the arrow is clicked:
							{
								xtype: 'button',
								text: 'Print Bill',
								id: 'btnPrintBillGzPermintaanDiet',
								handler: function()
								{
									printbill();
								}
							},
						]
						})
					}
				]
			}//,items:
		}
    )

    return pnlFormDataBasicPerawat_viGzPermintaanDiet;
};

function getItemPanelInputPermintaanPerawat_viGzPermintaanDiet(lebar) {
    var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 115,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'No. Penerimaan'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 0,
								xtype: 'textfield',
								id: 'txtNoPermintaanPerawatGzPermintaanDietL',
								name: 'txtNoPermintaanPerawatGzPermintaanDietL',
								width: 130,
								readOnly:true,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									}
								}
							},
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'Tanggal Minta'
							},
							{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 30,
								xtype: 'datefield',
								id: 'dfTglMintaPerawatGzPermintaanDietL',
								format: 'd/M/Y',
								width: 150,
								tabIndex:2,
								readOnly:true,
								value:now_viGzPermintaanDiet,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									}
								}
							},
							{
								x: 10,
								y: 60,
								xtype: 'label',
								text: 'Dari Ruangan'
							},
							{
								x: 120,
								y: 60,
								xtype: 'label',
								text: ':'
							},
							ComboUnitGzPermintaanDietLookup(),
							{
								x: 10,
								y: 90,
								xtype: 'label',
								text: 'Untuk Tanggal'
							},
							{
								x: 120,
								y: 90,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 90,
								xtype: 'datefield',
								id: 'dfTglUntukPerawatGzPermintaanDietL',
								format: 'd/M/Y',
								width: 150,
								tabIndex:4,
								value:now_viGzPermintaanDiet
							},
							//-------------- ## --------------
							{
								x: 320,
								y: 0,
								xtype: 'label',
								text: 'Perawat'
							},
							{
								x: 410,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							ComboPerawatGzPermintaanDietLookup(),
							{
								x: 320,
								y: 30,
								xtype: 'label',
								text: 'Keterangan'
							},
							{
								x: 410,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							{
								x: 420,
								y: 30,	
								xtype: 'textarea',
								name: 'txtKetPerawatGZPenerimaanL',
								id: 'txtKetPerawatGZPenerimaanL',
								width : 280,
								height : 80,
								tabIndex:6
							},
							//-----------------HIDDEN-----------------------
							{
								x: 130,
								y: 0,
								xtype: 'textfield',
								id: 'txtTmpNoPermintaanPerawatGzPermintaanDietL',
								name: 'txtTmpNoPermintaanPerawatGzPermintaanDietL',
								width: 130,
								readOnly:true,
								hidden:true
							},
							{
								x: 0,
								y: 0,
								xtype: 'textfield',
								id: 'txtTmpKdPasienPerawatGzPermintaanDietL',
								name: 'txtTmpKdPasienPerawatGzPermintaanDietL',
								width: 130,
								readOnly:true,
								hidden:true
							},
							
						]
					}
				]
			}
		]		
	};
    return items;
};

function ComboPerawatGzPermintaanDietLookup()
{

    var Field_perawatGiziLookup = ['KD_AHLI_GIZI', 'NAMA_AHLI_GIZI'];
    ds_perawatGiziGZPermintaanLookup = new WebApp.DataStore({fields: Field_perawatGiziLookup});
    ds_perawatGiziGZPermintaanLookup.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'KD_AHLI_GIZI',
					Sortdir: 'ASC',
					target: 'ComboAhliGizi',
					param: "perawat = 'true'"
				}
		}
	);
	
	
    var cbo_PerawatGiziGzPermintaanDietLookup = new Ext.form.ComboBox
    (
        {
			x: 420,
			y: 0,
            flex: 1,
			id: 'cbo_PerawatGiziGzPermintaanDietLookup',
			valueField: 'KD_AHLI_GIZI',
            displayField: 'NAMA_AHLI_GIZI',
			store: ds_perawatGiziGZPermintaanLookup,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:150,
			tabIndex:2,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					Ext.getCmp('btnAddPasienPerawatGzPermintaanDietL').enable();
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_PerawatGiziGzPermintaanDietLookup;
};

function getItemGridPasienPerawat_viGzPermintaanDiet(lebar,rowdata){
	//HUDI
	//30-10-2020
	//Perawat disable button add pasien dan delete pasien
	if(rowdata != undefined){
		var btn_addpasien;
		var btn_deletepasien;
		if(rowdata.status_posting == '0'){
			btn_addpasien	 = false;
			btn_deletepasien = false;
		}else{
			btn_addpasien	 = true;
			btn_deletepasien = true;
		}
	}
	//=====================================
    var items ={
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'margin-top: -1px;',
		border:false,
		width: lebar-80,
		height: 235,//300, 
	    tbar:
		[
			{
				text	: 'Add Pasien',
				id		: 'btnAddPasienPerawatGzPermintaanDietL',
				tooltip	: nmLookup,
				disabled: true,
				iconCls	: 'find',
				hidden 	: btn_addpasien,
				handler	: function(){
					var records = new Array();
					
					if(Ext.getCmp('txtNoPermintaanPerawatGzPermintaanDietL').getValue() == ''){
						//jika permintaan baru
						loadMask.show();
						records.push(new dsDataGrdPasien_viGzPermintaanDiet.recordType());
						dsDataGrdPasien_viGzPermintaanDiet.add(records);
						saveMintaPerawat_viGzPermintaanDiet();
						
						dsTRDetailDietGzPermintaanDiet.removeAll();
						GzPermintaanDiet.vars.status_order = 'false';
						
						Ext.getCmp('txtNoPermintaanPerawatGzPermintaanDietL').disable();
						Ext.getCmp('dfTglMintaPerawatGzPermintaanDietL').disable();
						Ext.getCmp('cbo_UnitGzPermintaanDietLookup').disable();
						Ext.getCmp('dfTglUntukPerawatGzPermintaanDietL').disable();
						Ext.getCmp('cbo_PerawatGiziGzPermintaanDietLookup').disable();
						Ext.getCmp('txtKetPerawatGZPenerimaanL').disable();
					} else{
						records.push(new dsDataGrdPasien_viGzPermintaanDiet.recordType());
						dsDataGrdPasien_viGzPermintaanDiet.add(records);
						
						dsTRDetailDietGzPermintaanDiet.removeAll();
						GzPermintaanDiet.vars.status_order = 'false';
					}
					
					
				}
			},{
				xtype: 'tbseparator'
			},{
				xtype: 'button',
				text: 'Delete',
				iconCls: 'remove',
				disabled:true,
				hidden	: btn_deletepasien,
				id: 'btnDeletePerawat_viGzPermintaanDiet',
				handler: function()
				{
					/* var line = GzPermintaanDiet.form.Grid.a.getSelectionModel().selection.cell[0];
					var o = dsDataGrdPasien_viGzPermintaanDiet.getRange()[line].data;
					if(dsDataGrdPasien_viGzPermintaanDiet.getCount()>1){
						Ext.Msg.confirm('Warning', 'Apakah pasien ini akan dihapus?', function(button){
							if (button == 'yes'){
								if(dsDataGrdPasien_viGzPermintaanDiet.getRange()[line].data.kd_pasien != undefined){
									Ext.Ajax.request
									(
										{
											url: baseURL + "index.php/gizi/functionPermintaanDiet/hapusBarisPasien",
											params:{kd_pasien:o.kd_pasien,no_minta:Ext.getCmp('txtTmpNoPermintaanGzPermintaanDietL').getValue()} ,
											failure: function(o)
											{
												ShowPesanErrorGzPermintaanDiet('Error, delete pasien! Hubungi Admin', 'Error');
											},	
											success: function(o) 
											{
												var cst = Ext.decode(o.responseText);
												if (cst.success === true) 
												{
													dsDataGrdPasien_viGzPermintaanDiet.removeAt(line);
													GzPermintaanDiet.form.Grid.a.getView().refresh();
													Ext.getCmp('btnAddDetDietGzPermintaanDietL').enable();
													ShowPesanInfoGzPermintaanDiet('Berhasil menghapus data pasien ini', 'Information');
													
												}
												else 
												{
													ShowPesanErrorGzPermintaanDiet('Gagal menghapus data pasien ini', 'Error');
												};
											}
										}
										
									)
								}else{
									dsDataGrdPasien_viGzPermintaanDiet.removeAt(line);
									GzPermintaanDiet.form.Grid.a.getView().refresh();
									
								}
							} 
							
						});
					} else{
						ShowPesanErrorGzPermintaanDiet('Data tidak bisa dihapus karena minimal pasien 1','Error');
					} */
					
				}
			}	
		],
		items:[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:[
					gridDataViewEditPerawat_viGzPermintaanDiet()
				]	
			}
		]
	};
    return items;
};

function gridDataViewEditPerawat_viGzPermintaanDiet(){
    var FieldGrdKasir_viGzPermintaanDiet = [];
    dsDataGrdPasien_viGzPermintaanDiet= new WebApp.DataStore({
        fields: FieldGrdKasir_viGzPermintaanDiet
    });
    
    GzPermintaanDiet.form.Grid.a =new Ext.grid.EditorGridPanel({
        store: dsDataGrdPasien_viGzPermintaanDiet,
        height: 210,
        columnLines: true,
		selModel: new Ext.grid.CellSelectionModel ({
            singleSelect: true,
            listeners:{
                cellselect: function(sm, row, rec){
					rowSelectedGridPasien_viGzPermintaanDiet = undefined;
					rowSelectedGridPasien_viGzPermintaanDiet = dsDataGrdPasien_viGzPermintaanDiet.getAt(row);
					CurrentDataPasien_viGzPermintaanDiet
					CurrentDataPasien_viGzPermintaanDiet.row = row;
					CurrentDataPasien_viGzPermintaanDiet.data = rowSelectedGridPasien_viGzPermintaanDiet.data;
					
					dsTRDetailDietGzPermintaanDiet.removeAll();
					getGridWaktu_Perawat(Ext.getCmp('txtTmpNoPermintaanPerawatGzPermintaanDietL').getValue(),CurrentDataPasien_viGzPermintaanDiet.data.kd_pasien);
					console.log(GzPermintaanDiet.vars.status_order);
					if(GzPermintaanDiet.vars.status_order == 'false'){
						Ext.getCmp('btnAddDetPerawatDietGzPermintaanDietL').enable();
						Ext.getCmp('btnDeleteDetPerawat_viGzPermintaanDiet').enable();
						Ext.getCmp('btnEditDietPerawat_viGzPermintaanDiet').enable();
						Ext.getCmp('btnSaveDietPerawat_viGzPermintaanDiet').enable();
					} else{
						Ext.getCmp('btnAddDetPerawatDietGzPermintaanDietL').disable();
						Ext.getCmp('btnDeleteDetPerawat_viGzPermintaanDiet').disable();
						Ext.getCmp('btnEditDietPerawat_viGzPermintaanDiet').disable();
						Ext.getCmp('btnSaveDietPerawat_viGzPermintaanDiet').disable();
					}
					Ext.getCmp('txtTmpKdPasienPerawatGzPermintaanDietL').setValue(CurrentDataPasien_viGzPermintaanDiet.data.kd_pasien);
					
                },
            }
        }),
        stripeRows: true,
        cm:new Ext.grid.ColumnModel([
        	new Ext.grid.RowNumberer(),	
			{
				dataIndex: 'kd_pasien',
				header: 'Kode Pasien',
				sortable: true,
				hidden: false,
				width: 70,
				editor:new Nci.form.Combobox.autoComplete({
					id		: 'txt_KdPasien',
					//enableKeyEvents:true,
					store	: GzPermintaanDiet.form.ArrayStore.pasien,
					select	: function(a,b,c){
										
						var line	= GzPermintaanDiet.form.Grid.a.getSelectionModel().selection.cell[0];
						dsDataGrdPasien_viGzPermintaanDiet.getRange()[line].data.kd_pasien=b.data.kd_pasien;
						dsDataGrdPasien_viGzPermintaanDiet.getRange()[line].data.tgl_masuk=b.data.tgl_masuk;
						dsDataGrdPasien_viGzPermintaanDiet.getRange()[line].data.jam_masuk=b.data.jam_masuk;
						dsDataGrdPasien_viGzPermintaanDiet.getRange()[line].data.no_kamar=b.data.no_kamar;
						dsDataGrdPasien_viGzPermintaanDiet.getRange()[line].data.nama_kamar=b.data.nama_kamar;
						dsDataGrdPasien_viGzPermintaanDiet.getRange()[line].data.nama=b.data.nama;
						dsDataGrdPasien_viGzPermintaanDiet.getRange()[line].data.umur=b.data.umur;
						
						GzPermintaanDiet.vars.kd_pasien=b.data.kd_pasien;
						GzPermintaanDiet.vars.tgl_masuk=b.data.tgl_masuk;
						GzPermintaanDiet.vars.no_kamar=b.data.no_kamar;
						GzPermintaanDiet.vars.kd_unit=b.data.kd_unit;
						
						loadMask.show();
						
						savePasien_viGzPermintaanDiet();
						
						GzPermintaanDiet.form.Grid.a.getView().refresh();
					},
					insert	: function(o){
						return {
							kd_pasien       : o.kd_pasien,
							tgl_masuk 		: o.tgl_masuk,
							jam_masuk		: o.jam_masuk,
							no_kamar		: o.no_kamar,
							nama_kamar		: o.nama_kamar,
							nama			: o.nama,
							umur			: o.umur,
							text			:  '<table style="font-size: 11px;"><tr><td width="100">'+o.kd_pasien+'</td><td width="200">'+o.nama+'</td></tr></table>',
							kd_unit			: o.kd_unit
						}
					},
					param:function(o){
						/* var tmp_nilai = formatnomedrec(Ext.getCmp('txt_KdPasien').getValue());
						console.log(tmp_nilai); */
						console.log(o.lastQuery);
							return {
								kd_pasien : formatnomedrec(o.lastQuery),
								kd_unitLama:GzPermintaanDiet.vars.kd_unit,
								kd_unitNew:Ext.getCmp('cbo_UnitGzPermintaanDietLookup').getValue()
							}
					},
					url		: baseURL + "index.php/gizi/functionPermintaanDiet/getPasienRWI",
					valueField: 'kd_pasien',
					displayField: 'text',
					listWidth: 380
				})
			},
			{			
				dataIndex: 'nama',
				header: 'Nama Pasien',
				sortable: true,
				width: 250,
				editor:new Nci.form.Combobox.autoComplete({
					store	: GzPermintaanDiet.form.ArrayStore.pasien,
					select	: function(a,b,c){
						var line	= GzPermintaanDiet.form.Grid.a.getSelectionModel().selection.cell[0];
						dsDataGrdPasien_viGzPermintaanDiet.getRange()[line].data.kd_pasien=b.data.kd_pasien;
						dsDataGrdPasien_viGzPermintaanDiet.getRange()[line].data.tgl_masuk=b.data.tgl_masuk;
						dsDataGrdPasien_viGzPermintaanDiet.getRange()[line].data.jam_masuk=b.data.jam_masuk;
						dsDataGrdPasien_viGzPermintaanDiet.getRange()[line].data.no_kamar=b.data.no_kamar;
						dsDataGrdPasien_viGzPermintaanDiet.getRange()[line].data.nama_kamar=b.data.nama_kamar;
						dsDataGrdPasien_viGzPermintaanDiet.getRange()[line].data.nama=b.data.nama;
						dsDataGrdPasien_viGzPermintaanDiet.getRange()[line].data.umur=b.data.umur;
						
						GzPermintaanDiet.vars.kd_pasien=b.data.kd_pasien;
						GzPermintaanDiet.vars.tgl_masuk=b.data.tgl_masuk;
						GzPermintaanDiet.vars.no_kamar=b.data.no_kamar;
						GzPermintaanDiet.vars.kd_unit=b.data.kd_unit;
						
						loadMask.show();
						
						savePasienPerawat_viGzPermintaanDiet();
						
						GzPermintaanDiet.form.Grid.a.getView().refresh();
					},
					insert	: function(o){
						return {
							kd_pasien       : o.kd_pasien,
							tgl_masuk 		: o.tgl_masuk,
							jam_masuk		: o.jam_masuk,
							no_kamar		: o.no_kamar,
							nama_kamar		: o.nama_kamar,
							nama			: o.nama,
							umur			: o.umur,
							text			:  '<table style="font-size: 11px;"><tr><td width="100">'+o.kd_pasien+'</td><td width="200">'+o.nama+'</td></tr></table>',
							kd_unit			: o.kd_unit
						}
					},
					param:function(){
							return {
								kd_pasien : '',
								kd_unitLama:GzPermintaanDiet.vars.kd_unit,
								kd_unitNew:Ext.getCmp('cbo_UnitGzPermintaanDietLookup').getValue()
							}
					},
					url		: baseURL + "index.php/gizi/functionPermintaanDiet/getPasienRWI",
					valueField: 'nama',
					displayField: 'text',
					listWidth: 380
				})
			},{
				dataIndex: 'umur',
				header: 'Umur',
				width: 120,
				align:'left'
			},
			{
				dataIndex: 'tgl_masuk',
				header: 'Tanggal Masuk',
				width: 70,
				align:'center'
			},{
				dataIndex: 'jam_masuk',
				header: 'Jam Masuk',
				sortable: true,
				align:'center',
				width: 85
			},{
				dataIndex: 'no_kamar',
				header: 'No Kamar',
				sortable: true,
				width: 45,
				align:'center'
			},
			{
				dataIndex: 'nama_kamar',
				header: 'Kamar',
				sortable: true,
				width: 65,
				align:'center'
			},
			{
				dataIndex: 'no_minta',
				header: 'no_minta',
				sortable: true,
				width: 65,
				align:'center',
				hidden:true
			},
			{
				dataIndex: 'jenis_makanan_pasien',
				header: 'Jenis Makanan',
				sortable: true,
				width: 100,
				align:'center'
			}
        ]),
		viewConfig:{
			forceFit: true
		}
    });
    return GzPermintaanDiet.form.Grid.a;
};

function getItemGridDetailPerawat_viGzPermintaanDiet(lebar,rowdata) 
{
	//HUDI
	//30-10-2020
	//Perawat disable button add pasien dan delete pasien
	if(rowdata != undefined){
		var btn_adddetail;
		var btn_deletedetail;
		var btn_editdetail;
		var btn_savedetail;
		if(rowdata.status_posting == '0'){
			btn_adddetail	 = false;
			btn_deletedetail = false;
			btn_editdetail	 = false;
			btn_savedetail	 = false;
		}else{
			btn_adddetail	 = true;
			btn_deletedetail = true;
			btn_editdetail	 = true;
			btn_savedetail	 = true;
		}
	}
	//=====================================

    var items =
	{
		title: 'Detail Diet', 
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
		bodyStyle: 'margin-top: -1px;',
		border:true,
		width: lebar-80,
		height: 150,//300, 
	    items:
		[
		
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
				items:
				[
					gridDataViewDetaiDietPerawat_viGzPermintaanDiet()
				]	
			}
			//-------------- ## --------------
		],
		tbar:
		[
			{
				text	: 'Add Diet',
				id		: 'btnAddDetPerawatDietGzPermintaanDietL',
				tooltip	: nmLookup,
				iconCls	: 'find',
				disabled:true,
				hidden 	: btn_adddetail,
				handler	: function(){
					/* var records = new Array();
					records.push(new dsTRDetailDietGzPermintaanDiet.recordType());
					dsTRDetailDietGzPermintaanDiet.add(records);

					var thisrow = dsTRDetailDietGzPermintaanDiet.getCount()-1; 
					gridDTLDiet_GzPermintaanDiet.startEditing(thisrow, 1); */

					var records = new Array();
					records.push(new dsTRDetailDietGzPermintaanDiet.recordType());
					dsTRDetailDietGzPermintaanDiet.add(records);

					var thisrow = dsTRDetailDietGzPermintaanDiet.getCount()-1; 
					//

					var date 	= new Date();
					var jam 	= date.format("H");
					var menit   = date.format("i");
					
					if(jam > 0 && jam <= 4){
						if(menit > 0 && menit <= 29){
							jam = 'Pagi';
						}else{
							jam = 'Siang';
						}
					}else if(jam > 4 && jam <= 9){
						jam = 'Siang';
					}else if(jam >= 10 && jam < 15){
							jam = 'Sore';
					}else if( jam >= 15 && jam < 24){
						jam = 'Pagi';
					}
					
					dsTRDetailDietGzPermintaanDiet.getRange()[thisrow].data.waktu = jam;
					gridDTLDiet_GzPermintaanDiet.getView().refresh();
					gridDTLDiet_GzPermintaanDiet.startEditing(thisrow, 3);
				}
			},{
				xtype: 'tbseparator'
			},{
				xtype: 'button',
				text: 'Delete',
				iconCls: 'remove',
				disabled:true,
				hidden	: btn_deletedetail,
				id: 'btnDeleteDetPerawat_viGzPermintaanDiet',
				handler: function()
				{
					var line = gridDTLDiet_GzPermintaanDiet.getSelectionModel().selection.cell[0];
					var o = dsTRDetailDietGzPermintaanDiet.getRange()[line].data;
					if(dsTRDetailDietGzPermintaanDiet.getCount()>1){
						Ext.Msg.confirm('Warning', 'Apakah jenis diet ini akan dihapus?', function(button){
							if (button == 'yes'){
								if(dsTRDetailDietGzPermintaanDiet.getRange()[line].data.kd_jenis != undefined){
									Ext.Ajax.request
									(
										{
											url: baseURL + "index.php/gizi/functionPermintaanDiet/hapusBarisDetailDiet",
											params: {		
												kd_jenis:o.kd_jenis, 
												kd_waktu:o.kd_waktu, 
												no_minta:Ext.getCmp('txtTmpNoPermintaanGzPermintaanDietL').getValue(),
												kd_pasien:Ext.getCmp('txtTmpKdPasienGzPermintaanDietL').getValue()
											},
											failure: function(o) {
												ShowPesanErrorGzPermintaanDiet('Error, delete detail diet! Hubungi Admin', 'Error');
											},	
											success: function(o) {
												var cst = Ext.decode(o.responseText);
												if (cst.success === true) 
												{
													ShowPesanInfoGzPermintaanDiet('Berhasil menghapus jenis diet ini', 'Information');
													dsTRDetailDietGzPermintaanDiet.removeAt(line);
													gridDTLDiet_GzPermintaanDiet.getView().refresh();
												}
												else 
												{
													ShowPesanErrorGzPermintaanDiet('Gagal menghapus data jenis diet ini', 'Error');
												};
											}
										}
										
									)
								}else{
									dsTRDetailDietGzPermintaanDiet.removeAt(line);
									gridDTLDiet_GzPermintaanDiet.getView().refresh();
								}
							} 
							
						});
					} else{
						ShowPesanErrorGzPermintaanDiet('Data tidak bisa dihapus karena minimal jenis diet 1','Error');
					}
					
				}
			},{
				xtype: 'tbseparator'
			},{
				xtype: 'button',
				text: 'Edit',
				iconCls: 'Edit_Tr',
				disabled:true,
				hidden	: btn_editdetail,
				id: 'btnEditDietPerawat_viGzPermintaanDiet',
				handler: function(){
					gridDTLDiet_GzPermintaanDiet.enable(true);
					Ext.getCmp('btnSaveDietPerawat_viGzPermintaanDiet').enable();
					edit_detail_permintaan = true;
				}
			},{
				xtype: 'tbseparator',
				id	 : 'btnSeparatorDiet_viGzPermintaanDiet'
			},{
				xtype: 'button',
				text: 'Save',
				iconCls: 'save',
				disabled:true,
				hidden	: btn_savedetail,
				id: 'btnSaveDietPerawat_viGzPermintaanDiet',
				handler: function(){
					loadMask.show();
					saveDetailDietPerawat_viGzPermintaanDiet();
				}
			}	
		]
	};
    return items;
};

function gridDataViewDetaiDietPerawat_viGzPermintaanDiet() 
{

    var fldDetail = [];
	
    dsTRDetailDietGzPermintaanDiet = new WebApp.DataStore({ fields: fldDetail })
		 
    gridDTLDiet_GzPermintaanDiet = new Ext.grid.EditorGridPanel
    (
        {
            store: dsTRDetailDietGzPermintaanDiet,
            border: false,
            columnLines: true,
            height: 95,
			autoScroll:true,
            selModel: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                            //cellSelecteddeskripsiRWI = dsTRDetailDietGzPermintaanDietList.getAt(row);
                            //CurrentGzPermintaanDiet.row = row;
                            //CurrentGzPermintaanDiet.data = cellSelecteddeskripsiRWI;
                        }
                    }
                }
            ),
            stripeRows: true,
            columns:
			[
				{
					header: 'kd waktu',
					dataIndex: 'kd_waktu',
					width:100,
					hidden:true
				},
				{			
					dataIndex: 'waktu',
					header: 'Waktu',
					sortable: false,
					width: 150,	
					editor:new Nci.form.Combobox.autoComplete({
						store	: GzPermintaanDiet.form.ArrayStore.waktu,
						select	: function(a,b,c){
							var line	= gridDTLDiet_GzPermintaanDiet.getSelectionModel().selection.cell[0];
							dsTRDetailDietGzPermintaanDiet.getRange()[line].data.kd_waktu=b.data.kd_waktu;
							dsTRDetailDietGzPermintaanDiet.getRange()[line].data.waktu=b.data.waktu;
							
							GzPermintaanDiet.vars.kd_waktu=b.data.kd_waktu;
							
							gridDTLDiet_GzPermintaanDiet.getView().refresh();

							var thisrow = dsTRDetailDietGzPermintaanDiet.getCount()-1; 
							gridDTLDiet_GzPermintaanDiet.startEditing(thisrow, 3);
						},
						insert	: function(o){
							return {
								kd_waktu	: o.kd_waktu,
								waktu		: o.waktu,
								text		:  '<table style="font-size: 11px;"><tr><td width="100">'+o.waktu+'</td></tr></table>'
							}
						},
						url		: baseURL + "index.php/gizi/functionPermintaanDiet/getWaktu",
						valueField: 'waktu',
						displayField: 'text',
						listWidth: 150
					})
				},
				{
					header: 'kd bentuk makanan',
					dataIndex: 'kd_bentuk_makanan',
					width:100,
					hidden:true
				},
				{
					header: 'Bentuk Makanan',
					dataIndex: 'nama_makanan',
					width:150,
					hidden:false,
					editor:new Nci.form.Combobox.autoComplete({
						store	: GzPermintaanDiet.form.ArrayStore.bentukmakanan,
						select	: function(a,b,c){
							var line	= gridDTLDiet_GzPermintaanDiet.getSelectionModel().selection.cell[0];
							//console.log(b.data);
							dsTRDetailDietGzPermintaanDiet.getRange()[line].data.kd_bentuk_makanan	= b.data.kd_bentuk_makanan;
							dsTRDetailDietGzPermintaanDiet.getRange()[line].data.nama_makanan	= b.data.nama_makanan;
							
							GzPermintaanDiet.vars.kd_bentuk_makanan	= b.data.kd_bentuk_makanan;
							
							//loadMask.show();
							
							//saveDetailDiet_viGzPermintaanDiet();
							
							gridDTLDiet_GzPermintaanDiet.getView().refresh();
							
							var thisrow = dsTRDetailDietGzPermintaanDiet.getCount()-1; 
							gridDTLDiet_GzPermintaanDiet.startEditing(thisrow, 4);
						},
						insert	: function(o){
							// console.log(o);
							return {
								kd_bentuk_makanan	: o.kd_bentuk_makanan,
								nama_makanan		: o.nama_makanan,
								text		:  '<table style="font-size: 11px;"><tr><td width="100">'+o.nama_makanan+'</td></tr></table>'
							}
						},
						url		: baseURL + "index.php/gizi/functionPermintaanDiet/getBentukMakanan",
						valueField: 'nama_makanan',
						displayField: 'text',
						listWidth: 150
					})
				},{
					header: 'Jenis Makanan',
					dataIndex: 'nama_jenis_makanan',
					width:150,
					hidden:false,
					editor:new Nci.form.Combobox.autoComplete({
						store	: GzPermintaanDiet.form.ArrayStore.bentukmakanan,
						select	: function(a,b,c){
							var line	= gridDTLDiet_GzPermintaanDiet.getSelectionModel().selection.cell[0];
							console.log(b.data);
							dsTRDetailDietGzPermintaanDiet.getRange()[line].data.kd_jenis_makanan	= b.data.kd_jenis_makanan;
							dsTRDetailDietGzPermintaanDiet.getRange()[line].data.nama_jenis_makanan	= b.data.nama_jenis_makanan;
							
							GzPermintaanDiet.vars.kd_jenis_makanan	= b.data.kd_jenis_makanan;
							
							gridDTLDiet_GzPermintaanDiet.getView().refresh();
							
							var thisrow = dsTRDetailDietGzPermintaanDiet.getCount()-1; 
							gridDTLDiet_GzPermintaanDiet.startEditing(thisrow, 5);
						},
						insert	: function(o){
							//console.log(o);
							return {
								kd_jenis_makanan	: o.kd_jenis_makanan,
								nama_jenis_makanan		: o.nama_jenis_makanan,
								text		:  '<table style="font-size: 11px;"><tr><td width="100">'+o.nama_jenis_makanan+'</td></tr></table>'
							}
						},
						url		: baseURL + "index.php/gizi/functionPermintaanDiet/getJenisMakanan",
						valueField: 'nama_jenis_makanan',
						displayField: 'nama_jenis_makanan',
						listWidth: 150
					})
				},
				{			
					dataIndex: 'jenis_diet',
					header: 'Jenis diet',
					sortable: false,
					width: 150,	
					hidden : false,
					editor:new Nci.form.Combobox.autoComplete({
						store	: GzPermintaanDiet.form.ArrayStore.jenisdiet,
						select	: function(a,b,c){
							var line	= gridDTLDiet_GzPermintaanDiet.getSelectionModel().selection.cell[0];
							dsTRDetailDietGzPermintaanDiet.getRange()[line].data.kd_jenis=b.data.kd_jenis;
							dsTRDetailDietGzPermintaanDiet.getRange()[line].data.jenis_diet=b.data.jenis_diet;
							
							GzPermintaanDiet.vars.kd_jenis=b.data.kd_jenis;
							
							/* loadMask.show();
							
							saveDetailDiet_viGzPermintaanDiet(); */
							
							gridDTLDiet_GzPermintaanDiet.getView().refresh();
							
							var thisrow = dsTRDetailDietGzPermintaanDiet.getCount()-1; 
							gridDTLDiet_GzPermintaanDiet.startEditing(thisrow, 7);
						},
						insert	: function(o){
							return {
								kd_jenis	: o.kd_jenis,
								jenis_diet	: o.jenis_diet,
								text		:  '<table style="font-size: 11px;"><tr><td width="100">'+o.jenis_diet+'</td></tr></table>'
							}
						},
						url		: baseURL + "index.php/gizi/functionPermintaanDiet/getJenisDiet",
						valueField: 'jenis_diet',
						displayField: 'text',
						listWidth: 150
					})
				},
				{
					header: 'kd kategori',
					dataIndex: 'kd_kategori',
					width:100,
					hidden:true
				},
				{
					header: 'Kategori Diet',
					dataIndex: 'nama_kategori',
					width:150,
					hidden:false,
					editor:new Nci.form.Combobox.autoComplete({
						store	: GzPermintaanDiet.form.ArrayStore.kategoridiet,
						select	: function(a,b,c){
							var line	= gridDTLDiet_GzPermintaanDiet.getSelectionModel().selection.cell[0];
							//console.log(b.data);
							dsTRDetailDietGzPermintaanDiet.getRange()[line].data.kd_kategori	= b.data.kd_kategori;
							dsTRDetailDietGzPermintaanDiet.getRange()[line].data.nama_kategori	= b.data.nama_kategori;
							
							GzPermintaanDiet.vars.kd_kategori	= b.data.kd_kategori;
							
							//loadMask.show();
							
							//saveDetailDiet_viGzPermintaanDiet();
							
							gridDTLDiet_GzPermintaanDiet.getView().refresh();
						},
						insert	: function(o){
							//console.log(o);
							return {
								kd_kategori	: o.kd_kategori,
								nama_kategori	: o.nama_kategori,
								text		:  '<table style="font-size: 11px;"><tr><td width="100">'+o.nama_kategori+'</td></tr></table>'
							}
						},
						url		: baseURL + "index.php/gizi/functionPermintaanDiet/getKategoriDiet",
						valueField: 'nama_kategori',
						displayField: 'text',
						listWidth: 150
					})
				},
				{
					header: 'kd jenis',
					dataIndex: 'kd_jenis',
					width:100,
					hidden:true
				},
				{
					header: 'kd pasien',
					dataIndex: 'kd_pasien',
					width:100,
					hidden:true
				}
            ],
			viewConfig: {forceFit: true}
			
	 
		}
    );
    return gridDTLDiet_GzPermintaanDiet;
};

function getParamMintaPerawatGzPermintaanDiet() 
{
    var params =
	{	
		NoMinta		: Ext.getCmp('txtNoPermintaanPerawatGzPermintaanDietL').getValue(),
		KdUnit		: Ext.getCmp('cbo_UnitGzPermintaanDietLookup').getValue(),
		TglMinta	: Ext.getCmp('dfTglMintaPerawatGzPermintaanDietL').getValue(),
		TglMakan	: Ext.getCmp('dfTglUntukPerawatGzPermintaanDietL').getValue(),
		AhliGizi	: Ext.getCmp('cbo_PerawatGiziGzPermintaanDietLookup').getValue(),
		Ket			: Ext.getCmp('txtKetPerawatGZPenerimaanL').getValue()
	};
    return params
};

function saveMintaPerawat_viGzPermintaanDiet(){
	if (ValidasiEntryPermintaanPerawatGzPermintaanDiet(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/gizi/functionPermintaanDiet/saveMinta",
				params: getParamMintaPerawatGzPermintaanDiet(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorGzPermintaanDiet('Error, permintaan! Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						Ext.getCmp('txtTmpNoPermintaanPerawatGzPermintaanDietL').setValue(cst.nominta);
						Ext.getCmp('txtNoPermintaanPerawatGzPermintaanDietL').setValue(cst.nominta);
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorGzPermintaanDiet('Gagal menyimpan data permintaan', 'Error');
					};
				}
			}
		)
	}
};

function ValidasiEntryPermintaanPerawatGzPermintaanDiet(modul,mBolHapus)
{
	var x = 1;
	if(Ext.getCmp('cbo_UnitGzPermintaanDietLookup').getValue() === '' || Ext.getCmp('cbo_PerawatGiziGzPermintaanDietLookup').getValue() === '' ||(Ext.getCmp('dfTglUntukPerawatGzPermintaanDietL').getValue() < Ext.getCmp('dfTglMintaPerawatGzPermintaanDietL').getValue()) ){
		if(Ext.getCmp('cbo_UnitGzPermintaanDietLookup').getValue() === ''){
			loadMask.hide();
			ShowPesanWarningGzPermintaanDiet('Unit tidak boleh kosong', 'Warning');
			x = 0;
		} else if(Ext.getCmp('cbo_PerawatGiziGzPermintaanDietLookup').getValue() === ''){
			loadMask.hide();
			ShowPesanWarningGzPermintaanDiet('Ahli gizi tidak boleh kosong', 'Warning');
			x = 0;
		} else {
			loadMask.hide();
			ShowPesanWarningGzPermintaanDiet('Untuk tanggal tidak boleh kurang dari tanggal permintaan', 'Warning');
			x = 0;
		} 
	}
	return x;
};

function getParamMintaPasienPerawatGzPermintaanDiet() 
{
    var params =
	{
		NoMinta		: Ext.getCmp('txtTmpNoPermintaanPerawatGzPermintaanDietL').getValue(),
		KdUnit		: Ext.getCmp('cbo_UnitGzPermintaanDietLookup').getValue(),
		KdPasien	: GzPermintaanDiet.vars.kd_pasien,
		TglMasuk	: GzPermintaanDiet.vars.tgl_masuk,
		NoKamar		: GzPermintaanDiet.vars.no_kamar
	};
	
    return params
};

function savePasienPerawat_viGzPermintaanDiet(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionPermintaanDiet/saveMintaPasien",
			params: getParamMintaPasienPerawatGzPermintaanDiet(),
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorGzPermintaanDiet('Error, pasien! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					loadMask.hide();
					Ext.getCmp('txtTmpKdPasienPerawatGzPermintaanDietL').setValue(cst.kdpasien);
					if(cst.error=='ada'){
						ShowPesanWarningGzPermintaanDiet('Pasien ini susah ada dalam transaksi pemintaan diet hari ini, hapus pasien ini untuk melanjutkan', 'Warning');
						Ext.getCmp('btnDeletePerawat_viGzPermintaanDiet').enable();
						Ext.getCmp('btnAddPasienPerawatGzPermintaanDietL').disable();
						Ext.getCmp('btnAddDetPerawatDietGzPermintaanDietL').disable();
					} else{
						if(GzPermintaanDiet.vars.status_order == 'false'){
							Ext.getCmp('btnAddPasienPerawatGzPermintaanDietL').disable();
							Ext.getCmp('btnDeletePerawat_viGzPermintaanDiet').disable();
							Ext.getCmp('btnAddDetPerawatDietGzPermintaanDietL').enable();
						} else{
							Ext.getCmp('btnAddPasienPerawatGzPermintaanDietL').disable();
							Ext.getCmp('btnDeletePerawat_viGzPermintaanDiet').disable();
							Ext.getCmp('btnAddDetPerawatDietGzPermintaanDietL').disable();
						}
					}
				}
				else 
				{
					loadMask.hide();
					ShowPesanErrorGzPermintaanDiet('Gagal menyimpan data pasien', 'Error');
				};
			}
		}
	)
}

function datainitPerawat_viGzPermintaanDiet(rowdata)
{
	var tgl_minta2 = rowdata.tgl_minta.split(" ");
	var tgl_makan2 = rowdata.tgl_makan.split(" ");
	Ext.getCmp('txtTmpNoPermintaanPerawatGzPermintaanDietL').setValue(rowdata.no_minta);
	Ext.getCmp('txtNoPermintaanPerawatGzPermintaanDietL').setValue(rowdata.no_minta);
	Ext.getCmp('dfTglMintaPerawatGzPermintaanDietL').setValue(tgl_minta2[0]);
	Ext.getCmp('cbo_UnitGzPermintaanDietLookup').setValue(rowdata.nama_unit);
	Ext.getCmp('dfTglUntukPerawatGzPermintaanDietL').setValue(tgl_makan2[0]);
	Ext.getCmp('cbo_PerawatGiziGzPermintaanDietLookup').setValue(rowdata.nama_ahli_gizi);
	Ext.getCmp('txtKetPerawatGZPenerimaanL').setValue(rowdata.keterangan);
	GzPermintaanDiet.vars.kd_unit=rowdata.kd_unit
	
	Ext.getCmp('txtNoPermintaanPerawatGzPermintaanDietL').disable();
	Ext.getCmp('dfTglMintaPerawatGzPermintaanDietL').disable();
	Ext.getCmp('cbo_UnitGzPermintaanDietLookup').disable();
	Ext.getCmp('dfTglUntukPerawatGzPermintaanDietL').disable();
	Ext.getCmp('cbo_PerawatGiziGzPermintaanDietLookup').disable();
	Ext.getCmp('txtKetPerawatGZPenerimaanL').disable();	
	
	getGridPasien(rowdata.no_minta);
	cekOrder_Perawat(rowdata.no_minta);
	
};

function getParamDetailDietPerawatGzPermintaanDiet() 
{
	console.log(dsTRDetailDietGzPermintaanDiet.getRange()[0].data);
	var params =
	{
		NoMinta		: Ext.getCmp('txtTmpNoPermintaanPerawatGzPermintaanDietL').getValue(),
		TglMinta	: Ext.getCmp('dfTglMintaPerawatGzPermintaanDietL').getValue(),
		KdPasien	: Ext.getCmp('txtTmpKdPasienPerawatGzPermintaanDietL').getValue(),
		kd_jenis	: GzPermintaanDiet.vars.kd_jenis,
		kd_waktu	: GzPermintaanDiet.vars.kd_waktu,
		waktu		: dsTRDetailDietGzPermintaanDiet.getRange()[0].data.waktu,
		kd_kategori	: GzPermintaanDiet.vars.kd_kategori,
		kd_bentuk_makanan : GzPermintaanDiet.vars.kd_bentuk_makanan,
		kd_jenis_makanan : GzPermintaanDiet.vars.kd_jenis_makanan,
		frekuensi : dsTRDetailDietGzPermintaanDiet.getRange()[0].data.frekuensi,
		volume : dsTRDetailDietGzPermintaanDiet.getRange()[0].data.volume,
		edit_permintaan : edit_detail_permintaan
	};
	
    return params
};

function saveDetailDietPerawat_viGzPermintaanDiet(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionPermintaanDiet/saveMintaPasienDetail",
			params: getParamDetailDietPerawatGzPermintaanDiet(),
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorGzPermintaanDiet('Error, detail diet! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					//console.log(cst.update);
					loadMask.hide();
					//HUDI
					//02-09-2020
					if(cst.update == false || cst.update == 'false' || cst.update == "false"){
						ShowPesanInfoGzPermintaanDiet('Data berhasil disimpan','Information');
					}else{
						ShowPesanInfoGzPermintaanDiet('Data berhasil diupdate','Information');
					}
					dataGriAwal()
						Ext.getCmp('btnAddPasienPerawatGzPermintaanDietL').disable();
						Ext.getCmp('btnAddDetPerawatDietGzPermintaanDietL').disable();
						Ext.getCmp('btnDeleteDetPerawat_viGzPermintaanDiet').enable();
						Ext.getCmp('btnEditDietPerawat_viGzPermintaanDiet').enable();
						gridDTLDiet_GzPermintaanDiet.disable(true);

						//28-10-2020
						dsDataGrdPasien_viGzPermintaanDiet.removeAll();
						getGridPasien(cst.no_minta);
				}
				else 
				{
					loadMask.hide();
					ShowPesanErrorGzPermintaanDiet('Gagal menyimpan data detail diet', 'Error');
				};
			}
		}
	)
};

function cekOrder_Perawat(no_minta){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionPermintaanDiet/cekOrderPermintaanDiet",
			params: {nominta:no_minta},
			failure: function(o)
			{
				ShowPesanErrorGzPermintaanDiet('Error, cek order! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					//Ext.getCmp('btnAddPasienPerawatGzPermintaanDietL').enable();
					Ext.getCmp('btnDeletePerawat_viGzPermintaanDiet').enable();
					//Ext.getCmp('btnDeletePermintaan_Perawat_viGzPermintaanDiet').enable();
					GzPermintaanDiet.vars.status_order='false';
				}
				else 
				{
					ShowPesanInfoGzPermintaanDiet('Permintaan diet ini sudah di order', 'Information');
					Ext.getCmp('btnDeletePermintaan_Perawat_viGzPermintaanDiet').disable();
					GzPermintaanDiet.vars.status_order='true';
				};
			}
		}
		
	)
};

function getGridWaktu_Perawat(no_minta,kd_pasien){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionPermintaanDiet/getGridWaktu",
			params: {nominta:no_minta, kdpasien:kd_pasien},
			failure: function(o)
			{
				ShowPesanErrorGzPermintaanDiet('Error, detail diet! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				//HUDI
				//02-09-2020
				//=============== disable grid detail =============
				if(cst.totalrecords > 0){
					gridDTLDiet_GzPermintaanDiet.disable(true);
					Ext.getCmp('btnAddDetPerawatDietGzPermintaanDietL').disable();
					Ext.getCmp('btnDeleteDetPerawat_viGzPermintaanDiet').disable();
					Ext.getCmp('btnSaveDietPerawat_viGzPermintaanDiet').disable();
				}
				//=================================================
				if (cst.success === true) 
				{
					var recs=[],
						recType=dsTRDetailDietGzPermintaanDiet.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						//console.log(cst.ListDataObj[i]);
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
					dsTRDetailDietGzPermintaanDiet.add(recs);
					
					gridDTLDiet_GzPermintaanDiet.getView().refresh();
				}
				else 
				{
					ShowPesanErrorGzPermintaanDiet('Gagal membaca data detail diet pasien ini', 'Error');
				};
			}
		}
		
	)
	
};

function dataAddNewPerawatan_viGzPermintaanDiet(){
	Ext.getCmp('txtTmpNoPermintaanPerawatGzPermintaanDietL').setValue('');
	Ext.getCmp('txtNoPermintaanPerawatGzPermintaanDietL').setValue('');
	Ext.getCmp('dfTglMintaPerawatGzPermintaanDietL').setValue(now_viGzPermintaanDiet);
	Ext.getCmp('cbo_UnitGzPermintaanDietLookup').setValue('');
	Ext.getCmp('dfTglUntukPerawatGzPermintaanDietL').setValue(now_viGzPermintaanDiet);
	Ext.getCmp('cbo_PerawatGiziGzPermintaanDietLookup').setValue('');
	Ext.getCmp('txtKetPerawatGZPenerimaanL').setValue('');
	Ext.getCmp('txtTmpKdPasienPerawatGzPermintaanDietL').setValue('');
	GzPermintaanDiet.vars.kd_unit='';
	dsDataGrdPasien_viGzPermintaanDiet.removeAll();
	dsTRDetailDietGzPermintaanDiet.removeAll();
	
	
	Ext.getCmp('txtNoPermintaanPerawatGzPermintaanDietL').enable();
	Ext.getCmp('dfTglMintaPerawatGzPermintaanDietL').enable();
	Ext.getCmp('cbo_UnitGzPermintaanDietLookup').enable();
	Ext.getCmp('dfTglUntukPerawatGzPermintaanDietL').enable();
	Ext.getCmp('cbo_PerawatGiziGzPermintaanDietLookup').enable();
	Ext.getCmp('txtKetPerawatGZPenerimaanL').enable();
	Ext.getCmp('btnDeletePermintaan_Perawat_viGzPermintaanDiet').disable();
	Ext.getCmp('btnAddPasienPerawatGzPermintaanDietL').disable();
	
}

//=============================================================================

//HUDI
//30-10-2020
function HideButtonPermintaanDiet(opsi){
	Ext.getCmp('btnAddPasienGzPermintaanDietL').hide(opsi);
	Ext.getCmp('btnDelete_viGzPermintaanDiet').hide(opsi);
	Ext.getCmp('btnAddDetDietGzPermintaanDietL').hide(opsi);
	Ext.getCmp('btnDeleteDet_viGzPermintaanDiet').hide(opsi);
	Ext.getCmp('btnEditDiet_viGzPermintaanDiet').hide(opsi);
	Ext.getCmp('btnSaveDiet_viGzPermintaanDiet').hide(opsi);

	//Ext.getCmp('ColNamaPasien').setReadOnly(true);
}
function ShowButtonPermintaanDiet(opsi){
	Ext.getCmp('btnAddPasienGzPermintaanDietL').show(opsi);
	Ext.getCmp('btnDelete_viGzPermintaanDiet').show(opsi);
	Ext.getCmp('btnAddDetDietGzPermintaanDietL').show(opsi);
	Ext.getCmp('btnDeleteDet_viGzPermintaanDiet').show(opsi);
	Ext.getCmp('btnEditDiet_viGzPermintaanDiet').show(opsi);
	Ext.getCmp('btnSaveDiet_viGzPermintaanDiet').show(opsi);
}

//HUDI
//30-10-2020
//Perawat ===========================================================
function HideButtonPermintaanDietPerawat(opsi){
	Ext.getCmp('btnAddPasienPerawatGzPermintaanDietL').hide(opsi);
	Ext.getCmp('btnDeletePerawat_viGzPermintaanDiet').hide(opsi);
	Ext.getCmp('btnAddDetPerawatDietGzPermintaanDietL').hide(opsi);
	Ext.getCmp('btnDeleteDetPerawat_viGzPermintaanDiet').hide(opsi);
	Ext.getCmp('btnEditDietPerawat_viGzPermintaanDiet').hide(opsi);
	Ext.getCmp('btnSaveDietPerawat_viGzPermintaanDiet').hide(opsi);

	//Ext.getCmp('ColNamaPasien').setReadOnly(true);
}
function ShowButtonPermintaanDietPerawat(opsi){
	Ext.getCmp('btnAddPasienPerawatGzPermintaanDietL').show(opsi);
	Ext.getCmp('btnDeletePerawat_viGzPermintaanDiet').show(opsi);
	Ext.getCmp('btnAddDetPerawatDietGzPermintaanDietL').show(opsi);
	Ext.getCmp('btnDeleteDetPerawat_viGzPermintaanDiet').show(opsi);
	Ext.getCmp('btnEditDietPerawat_viGzPermintaanDiet').show(opsi);
	Ext.getCmp('btnSaveDietPerawat_viGzPermintaanDiet').show(opsi);
}
//===================================================================

//HUDI
//03-12-2020
//Combo baru untuk detail permintaan
function loadData_BentukMananan(params = '')
{
	ds_bentukMakananPermintaanDiet.load(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'kd_bentuk_makanan',
					Sortdir: 'ASC',
					target: 'ComboBentukMakanan',
					param: "kd_jenis_makanan = '" +params+"'"
				}
		}
	);
};

function loadData_Rute(params = ''){
	ds_rutePermintaanDiet.load(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'kd_rute',
					Sortdir: 'ASC',
					target: 'ComboRute',
					param: "kd_bentuk_makanan = '"+params+"'"
				}
		}
	);
}

function loadData_Frekuensi(params = ''){
	ds_frekuensiPermintaanDiet.load(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: '',
					Sortdir: 'ASC',
					target: 'ComboFrekuensi',
					param: "kd_bentuk_makanan = '"+params+"'"
				}
		}
	);
}

function loadData_Volume(params = ''){
	ds_volumePermintaanDiet.load(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: '',
					Sortdir: 'ASC',
					target: 'ComboVolume',
					param: "kd_bentuk_makanan = '"+params+"'"
				}
		}
	);
}