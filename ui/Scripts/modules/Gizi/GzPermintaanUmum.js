var kdWaktuMintaUmum;
var kdJenisMintaUmum;
var selectCount_viGzPermintaanUmum=50;
var NamaForm_viGzPermintaanUmum="Permintaan Umum";
var selectCountStatusPostingGzPermintaanUmum='Semua';//
var mod_name_viGzPermintaanUmum="viGzPermintaanUmum";
var now_viGzPermintaanUmum= new Date();
var rowSelected_viGzPermintaanUmum;
var rowSelectedGridUmum_viGzPermintaanUmum;
var setLookUps_viGzPermintaanUmum;
var selectSetUnit;
var cellSelecteddeskripsiRWI;
var tanggal = now_viGzPermintaanUmum.format("d/M/Y");
var jam = now_viGzPermintaanUmum.format("H/i/s");
var blnGetNoMintaOto= now_viGzPermintaanUmum.format("m");
var thnGetNoMintaOto= now_viGzPermintaanUmum.format("Y");
var getNoMintaOto=thnGetNoMintaOto+blnGetNoMintaOto;
var tmpkriteria;
var gridDTLUmum_GzPermintaanUmum;
var GridDataView_viGzPermintaanUmum;
var s;
var varNoOtoMinta;
var CurrentGzPermintaanUmum =
{
    data: Object,
    details: Array,
    row: 0
};

var CurrentData_viGzPermintaanUmum =
{
	data: Object,
	details: Array,
	row: 0
};

var CurrentDataUmum_viGzPermintaanUmum =
{
	data: Object,
	details: Array,
	row: 0
};


CurrentPage.page = dataGrid_viGzPermintaanUmum(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

var GzPermintaanUmum={};
GzPermintaanUmum.form={};
GzPermintaanUmum.func={};
GzPermintaanUmum.vars={};
GzPermintaanUmum.func.parent=GzPermintaanUmum;
GzPermintaanUmum.form.ArrayStore={};
GzPermintaanUmum.form.ComboBox={};
GzPermintaanUmum.form.DataStore={};
GzPermintaanUmum.form.Record={};
GzPermintaanUmum.form.Form={};
GzPermintaanUmum.form.Grid={};
GzPermintaanUmum.form.Panel={};
GzPermintaanUmum.form.TextField={};
GzPermintaanUmum.form.Button={};

GzPermintaanUmum.form.ArrayStore.pasien=new Ext.data.ArrayStore({
		id: 0,
		fields: [	
					'kd_pasien','nama','tgl_masuk','jam_masuk','no_kamar','nama_kamar'
				],
		data: []
	});
	
GzPermintaanUmum.form.ArrayStore.waktu	= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_waktu','waktu'],
	data: []
});

GzPermintaanUmum.form.ArrayStore.jenisdiet	= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_jenis','jenis_diet','harga_pokok'],
	data: []
});

function dataGrid_viGzPermintaanUmum(mod_id_viGzPermintaanUmum){	
    var FieldMaster_viGzPermintaanUmum = 
	[
		'no_minta', 'tgl_minta', 'tgl_makan', 'nama', 'alamat', 'keterangan'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viGzPermintaanUmum = new WebApp.DataStore
	({
        fields: FieldMaster_viGzPermintaanUmum
    });
    dataGriAwal();
    // Grid Apotek Perencanaan # --------------
	GridDataView_viGzPermintaanUmum = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viGzPermintaanUmum,
			autoScroll: true,
			columnLines: true,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viGzPermintaanUmum = undefined;
							rowSelected_viGzPermintaanUmum = dataSource_viGzPermintaanUmum.getAt(row);
							CurrentData_viGzPermintaanUmum
							CurrentData_viGzPermintaanUmum.row = row;
							CurrentData_viGzPermintaanUmum.data = rowSelected_viGzPermintaanUmum.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viGzPermintaanUmum = dataSource_viGzPermintaanUmum.getAt(ridx);
					if (rowSelected_viGzPermintaanUmum != undefined)
					{
						setLookUp_viGzPermintaanUmum(rowSelected_viGzPermintaanUmum.data);
						
					}
					else
					{
						setLookUp_viGzPermintaanUmum();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Apotek perencanaan
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header: 'No. Permintaan',
						dataIndex: 'no_minta',
						sortable: true,
						width: 35
						
					},
					//-------------- ## --------------
					{
						header: 'Nama',
						dataIndex: 'nama',
						sortable: true,
						hideable:false,
                        menuDisabled:true,
						width: 30
					},
					{
						header: 'Alamat',
						dataIndex: 'alamat',
						sortable: true,
						hideable:false,
                        menuDisabled:true,
						width: 30
					},
					//-------------- ## --------------
					{
						header:'Tgl Minta',
						dataIndex: 'tgl_minta',						
						width: 30,
						sortable: true,
						hideable:false,
                        menuDisabled:true,
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.tgl_minta);
						}
					},
					//-------------- ## --------------
					{
						header: 'Untuk Tanggal',
						dataIndex: 'tgl_makan',
						sortable: true,
						hideable:false,
                        menuDisabled:true,
						width: 30,
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.tgl_makan);
						}
					},
					{
						header: 'Keterangan',
						dataIndex: 'keterangan',
						sortable: true,
						hideable:false,
                        menuDisabled:true,
						width: 30
					},
					
					
					
					
					//-------------- ## --------------
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viGzPermintaanUmum',
				items: 
				[
					{
						xtype: 'button',
						text: 'Tambah Permintaan ',
						iconCls: 'Edit_Tr',
						tooltip: 'Add Data',
						id: 'btnTambah_viGzPermintaanUmum',
						handler: function(sm, row, rec)
						{
							setLookUp_viGzPermintaanUmum();		
							dataAddNew_viGzPermintaanUmum();							
						}
					},
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viGzPermintaanUmum',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viGzPermintaanUmum != undefined)
							{
								setLookUp_viGzPermintaanUmum(rowSelected_viGzPermintaanUmum.data)
							} else{
								ShowPesanWarningGzPermintaanUmum('Pilih data yang akan di edit','Warning');
							}
							
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viGzPermintaanUmum, selectCount_viGzPermintaanUmum, dataSource_viGzPermintaanUmum),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )
	
	var pencarianGzPermintaanUmum = new Ext.FormPanel({
        labelAlign: 'top',
        frame:true,
        title: '',
        bodyStyle:'padding:5px 5px 0',
        //width: 600,
        items: [
		{
			layout: 'column',
			border: false,
			items:
			[
				{
					columnWidth:.98,
					layout: 'absolute',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					width: 500,
					height: 70,
					anchor: '100% 100%',
					items:
					[
						{
							x: 10,
							y: 0,
							xtype: 'label',
							text: 'No. Permintaan'
						},
						{
							x: 120,
							y: 0,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 0,
							xtype: 'textfield',
							id: 'TxtNoPermintaanFilterGridDataView_viGzPermintaanUmum',
							name: 'TxtNoPermintaanFilterGridDataView_viGzPermintaanUmum',
							emptyText: '',
							width: 130,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariGzPermintaanUmum();
										refreshPermintaanUmum(tmpkriteria);
									} 						
								}
							}
						},
						{
							x: 10,
							y: 30,
							xtype: 'label',
							text: 'Nama'
						},
						{
							x: 120,
							y: 30,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 30,
							xtype: 'textfield',
							id: 'TxtNamaPermintaanFilterGridDataView_viGzPermintaanUmum',
							name: 'TxtNamaPermintaanFilterGridDataView_viGzPermintaanUmum',
							emptyText: '',
							width: 130,
						},
						//-------------- ## --------------
							
						{
							x: 310,
							y: 0,
							xtype: 'label',
							text: 'Tanggal'
						},
						{
							x: 400,
							y: 0,
							xtype: 'label',
							text: ':'
						},
						{
							x: 410,
							y: 0,
							xtype: 'datefield',
							id: 'dfTglAwalGzPermintaanUmum',
							format: 'd/M/Y',
							width: 120,
							tabIndex:3,
							value:now_viGzPermintaanUmum,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariGzPermintaanUmum();
										refreshPermintaanUmum(tmpkriteria);
									} 						
								}
							}
						},
						{
							x: 540,
							y: 0,
							xtype: 'label',
							text: 's/d'
						},
						{
							x: 568,
							y: 0,
							xtype: 'datefield',
							id: 'dfTglAkhirGzPermintaanUmum',
							format: 'd/M/Y',
							width: 120,
							tabIndex:4,
							value:now_viGzPermintaanUmum,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										tmpkriteria = getCriteriaCariGzPermintaanUmum();
										refreshPermintaanUmum(tmpkriteria);
									} 						
								}
							}
						},
						
						//----------------------------------------
						{
							x: 475,
							y: 30,
							xtype: 'button',
							text: 'Cari',
							iconCls: 'find',
							tooltip: 'Cari',
							style:{paddingLeft:'30px'},
							width:150,
							id: 'BtnFilterGridDataView_viGzPermintaanUmum',
							handler: function() 
							{				
								tmpkriteria = getCriteriaCariGzPermintaanUmum();
								refreshPermintaanUmum(tmpkriteria);
							}                        
						},
						{
							x: 568,
							y: 30,
							xtype: 'button',
							text: 'Refresh',
							iconCls: 'refresh',
							tooltip: 'Cari',
							style:{paddingLeft:'30px'},
							width:150,
							id: 'BtnRefreshGridDataView_viGzPermintaanUmum',
							handler: function() 
							{				
								dataGriAwal();
							}                        
						}
					]
				}
			]
		}
		]	
	})

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viGzPermintaanUmum = new Ext.Panel
    (
		{
			title: NamaForm_viGzPermintaanUmum,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viGzPermintaanUmum,
			region: 'center',
			layout: 'form', 
			closable: true,        
			border: false,  
			margins: '0 5 5 0',
			items: [ pencarianGzPermintaanUmum,
					GridDataView_viGzPermintaanUmum],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viGzPermintaanUmum,
		            columns: 21,
		            defaults: {
					scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viGzPermintaanUmum;
    //-------------- # End form filter # --------------
}

function refreshPermintaanUmum(kriteria)
{
    dataSource_viGzPermintaanUmum.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'ViewPermintaanUmum',
                    param : kriteria
                }			
            }
        );   
    return dataSource_viGzPermintaanUmum;
}

function setLookUp_viGzPermintaanUmum(rowdata){
    var lebar = 985;
    setLookUps_viGzPermintaanUmum = new Ext.Window({
        id: Nci.getId(),
        title: NamaForm_viGzPermintaanUmum, 
        closeAction: 'destroy',        
        width: 900,
        height: 550,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viGzPermintaanUmum(lebar,rowdata),
        listeners:{
            activate: function(){
				
            },
            afterShow: function(){
                this.activate();
            },
            deactivate: function(){
                rowSelected_viGzPermintaanUmum=undefined;
            }
        }
    });

    setLookUps_viGzPermintaanUmum.show();
    if (rowdata == undefined){	
    }
    else
    {
        datainit_viGzPermintaanUmum(rowdata);
    }
}

function getFormItemEntry_viGzPermintaanUmum(lebar,rowdata){
    var pnlFormDataBasic_viGzPermintaanUmum = new Ext.FormPanel({
		title: '',
		region: 'north',
		layout: 'form',
		bodyStyle: 'padding:10px 10px 10px 10px',
		anchor: '100%',
		labelWidth: 1,
		autoWidth: true,
		width: lebar,
		border: false,
		items:[
				getItemPanelInputPermintaan_viGzPermintaanUmum(lebar),
				getItemGridUmum_viGzPermintaanUmum(lebar),
				//getItemGridDetail_viGzPermintaanUmum(lebar,rowdata)
			],
			fileUpload: true,
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAdd_viGzPermintaanUmum',
						handler: function(){
							dataAddNew_viGzPermintaanUmum();
						}
					},{
						xtype: 'tbseparator'
					},{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSave_viGzPermintaanUmum',
						handler: function(){
							//dataAddNew_viGzPermintaanUmum();
							//loadMask.show();
							save_dataMintaUmum();
						}
					},{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						id:'btnDeletePermintaan_viGzPermintaanUmum',
						iconCls: 'remove',
						handler:function()
						{
							Ext.Msg.confirm('Warning', 'Apakah data permintaan ini akan di hapus?', function(button){
								if (button == 'yes'){
									//loadMask.show();
									Ext.Ajax.request
									(
										{
											url: baseURL + "index.php/gizi/functionPermintaanUmum/deletePermintaan",
											params: {
														no_minta:Ext.getCmp('TxtNoMintaPermintaanUmumL').getValue()
													},
											failure: function(o)
											{
												//loadMask.hide();
												ShowPesanErrorGzPermintaanUmum('Error, delete permintaan! Hubungi Admin', 'Error');
											},	
											success: function(o) 
											{
												var cst = Ext.decode(o.responseText);
												if (cst.success === true) 
												{
													dataGriAwal();
													ShowPesanInfoGzPermintaanUmum('Penghapusan berhasil','Information');
													setLookUps_viGzPermintaanUmum.close();
												}
												else 
												{
													if(cst.order =='true'){
														ShowPesanWarningGzPermintaanUmum('Gagal menghapus data ini. Permintaan diet ini sudah diorder', 'Warning');
													} else{
														ShowPesanWarningGzPermintaanUmum('Gagal menghapus data ini', 'Warning');
													}
													
												};
											}
										}
										
									)
								}
							});
						}
						  
					}
					
				]
			}//,items:
		}
    )

    return pnlFormDataBasic_viGzPermintaanUmum;
}

function getItemPanelInputPermintaan_viGzPermintaanUmum(lebar) {
    var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 180,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 10,
								xtype: 'label',
								text: 'No Permintaan',
								width: 160,
							},
							{
								x: 110,
								y: 10,
								xtype: 'label',
								text: ' : '
							},
							{
								x: 120,
								y: 10,
								xtype: 'textfield',
								name: 'TxtNoMintaPermintaanUmumL',
								id: 'TxtNoMintaPermintaanUmumL',
								width: 180,
								readOnly: true,
								
							},
							{
								x: 10,
								y: 40,
								xtype: 'label',
								text: 'Tgl Permintaan'
							},
							{
								x: 110,
								y: 40,
								xtype: 'label',
								text: ' : '
							},
							{
								x: 120,
								y: 40,
								name: 'dtpTglMintaUmum',
								xtype: 'datefield',
								id: 'dtpTglMintaUmumL',
								format: 'd/M/Y',
								readOnly:true,
								value: now_viGzPermintaanUmum
								
							},
							{
								x: 230,
								y: 40,
								xtype: 'label',
								text: 'Untuk Tgl'
							},
							{
								x: 300,
								y: 40,
								name: 'dtpTglMakan',
								xtype: 'datefield',
								id: 'dtpTglMakanUmumL',
								format: 'd/M/Y',
								value: now_viGzPermintaanUmum
							},
							{
								x: 10,
								y: 70,
								xtype: 'label',
								text: 'Keterangan',
								width: 160,
							},
							{
								x: 110,
								y: 70,
								xtype: 'label',
								text: ' : '
							},
							{
								x: 120,
								y: 70,
								xtype: 'textarea',
								name: 'TxtKetPermintaanUmumL',
								id: 'TxtKetPermintaanUmumL',
								width: 430,
								
							},
							{
								x: 10,
								y: 140,
								xtype: 'label',
								text: 'Nama'
							},
							{
								x: 110,
								y: 140,
								xtype: 'label',
								text: ' : '
							},
							{
								x: 120,
								y: 140,
								xtype: 'textfield',
								name: 'TxNamaMintaUmumL',
								id: 'TxNamaMintaUmumL',
								width: 150,
							},
							{
								x: 290,
								y: 140,
								xtype: 'label',
								text: 'Alamat'
							},
							{
								x: 339,
								y: 140,
								xtype: 'label',
								text: ' : '
							},
							{
								x: 350,
								y: 140,
								xtype: 'textfield',
								name: 'TxAlamatMintaUmumL',
								id: 'TxAlamatMintaUmumL',
								width: 200,
							},
						]
					}
				]
			}
		]		
	};
    return items;
};

function getItemGridUmum_viGzPermintaanUmum(lebar){
    var items ={
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'margin-top: -1px;',
		border:false,
		width: lebar-80,
		height: 245,//300, 
	    tbar:
		[
			{
				text	: 'Add jenis',
				id		: 'btnAddUmumGzPermintaanUmumL',
				tooltip	: nmLookup,
				iconCls	: 'find',
				handler	: function(){
					var records = new Array();
					//jika permintaan baru
					records.push(new dsDataGrdUmum_viGzPermintaanUmum.recordType());
					dsDataGrdUmum_viGzPermintaanUmum.add(records);
					/*if(Ext.getCmp('txtTmpNoPermintaanGzPermintaanUmumL').getValue() == ''){
						
						saveMinta_viGzPermintaanUmum();
						
						dsTRDetailUmumGzPermintaanUmum.removeAll();
						GzPermintaanUmum.vars.status_order = 'false';
						
						Ext.getCmp('txtNoPermintaanGzPermintaanUmumL').disable();
						Ext.getCmp('dfTglMintaGzPermintaanUmumL').disable();
						Ext.getCmp('cbo_UnitGzPermintaanUmumLookup').disable();
						Ext.getCmp('dfTglUntukGzPermintaanUmumL').disable();
						Ext.getCmp('cbo_AhliGiziGzPermintaanUmumLookup').disable();
						Ext.getCmp('txtKetGZPenerimaanL').disable();
					} else{
						records.push(new dsDataGrdUmum_viGzPermintaanUmum.recordType());
						dsDataGrdUmum_viGzPermintaanUmum.add(records);
						
						dsTRDetailUmumGzPermintaanUmum.removeAll();
						GzPermintaanUmum.vars.status_order = 'false';
					}*/
					
					
				}
			},{
				xtype: 'tbseparator'
			},{
				xtype: 'button',
				text: 'Delete',
				iconCls: 'remove',
				id: 'btnDelete_viGzPermintaanUmum',
				handler: function()
				{
					var line = GzPermintaanUmum.form.Grid.a.getSelectionModel().selection.cell[0];
					var o = dsDataGrdUmum_viGzPermintaanUmum.getRange()[line].data;
					if(dsDataGrdUmum_viGzPermintaanUmum.getCount()>1){
						Ext.Msg.confirm('Warning', 'Apakah yakin akan dihapus?', function(button){
							if (button == 'yes'){
								if(dsDataGrdUmum_viGzPermintaanUmum.getRange()[line].data.kd_jenis != undefined){
									Ext.Ajax.request
									(
										{
											url: baseURL + "index.php/gizi/functionPermintaanUmum/hapusBarisUmum",
											params:{kd_jenis:o.kd_jenis,no_minta:Ext.getCmp('TxtNoMintaPermintaanUmumL').getValue()} ,
											failure: function(o)
											{
												ShowPesanErrorGzPermintaanUmum('Error, delete! Hubungi Admin', 'Error');
											},	
											success: function(o) 
											{
												var cst = Ext.decode(o.responseText);
												if (cst.success === true) 
												{
													dsDataGrdUmum_viGzPermintaanUmum.removeAt(line);
													GzPermintaanUmum.form.Grid.a.getView().refresh();
													//Ext.getCmp('btnAddDetUmumGzPermintaanUmumL').enable();
													ShowPesanInfoGzPermintaanUmum('Berhasil menghapus data ini', 'Information');
													
												}
												else if (cst.success === false && cst.checking === false)
												{
													ShowPesanErrorGzPermintaanUmum('Data tidak ada, Hubungi Admin', 'Error');
												}
												else
												{
													ShowPesanErrorGzPermintaanUmum('Gagal menghapus data ini', 'Error');
												};
											}
										}
										
									)
								}else{
									dsDataGrdUmum_viGzPermintaanUmum.removeAt(line);
									GzPermintaanUmum.form.Grid.a.getView().refresh();
									
								}
							} 
							
						});
					} else{
						ShowPesanErrorGzPermintaanUmum('Data tidak bisa dihapus karena minimal data 1','Error');
					}
					
				}
			}	
		],
		items:[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:[
					gridDataViewEdit_viGzPermintaanUmum()
				]	
			}
		]
	};
    return items;
};


//var a={};
function gridDataViewEdit_viGzPermintaanUmum(){
    var FieldGrdKasir_viGzPermintaanUmum = [];
    dsDataGrdUmum_viGzPermintaanUmum= new WebApp.DataStore({
        fields: FieldGrdKasir_viGzPermintaanUmum
    });
    
    GzPermintaanUmum.form.Grid.a =new Ext.grid.EditorGridPanel({
        store: dsDataGrdUmum_viGzPermintaanUmum,
        height: 210,
        columnLines: true,
		selModel: new Ext.grid.CellSelectionModel ({
            singleSelect: true,
            listeners:{
                cellselect: function(sm, row, rec){
					/* rowSelectedGridUmum_viGzPermintaanUmum = undefined;
					rowSelectedGridUmum_viGzPermintaanUmum = dsDataGrdUmum_viGzPermintaanUmum.getAt(row);
					CurrentDataUmum_viGzPermintaanUmum
					CurrentDataUmum_viGzPermintaanUmum.row = row;
					CurrentDataUmum_viGzPermintaanUmum.data = rowSelectedGridUmum_viGzPermintaanUmum.data;
					
					dsTRDetailUmumGzPermintaanUmum.removeAll();
					getGridWaktu(Ext.getCmp('txtTmpNoPermintaanGzPermintaanUmumL').getValue(),CurrentDataUmum_viGzPermintaanUmum.data.kd_pasien);
					
					if(GzPermintaanUmum.vars.status_order == 'false'){
						Ext.getCmp('btnAddDetUmumGzPermintaanUmumL').enable();
						Ext.getCmp('btnDeleteDet_viGzPermintaanUmum').enable();
					} else{
						Ext.getCmp('btnAddUmumGzPermintaanUmumL').disable();
						Ext.getCmp('btnAddDetUmumGzPermintaanUmumL').disable();
					}
					Ext.getCmp('txtTmpKdUmumGzPermintaanUmumL').setValue(CurrentDataUmum_viGzPermintaanUmum.data.kd_pasien);
					 */
                },
            }
        }),
        stripeRows: true,
        cm:new Ext.grid.ColumnModel([
        	new Ext.grid.RowNumberer(),	
			{
				dataIndex: 'waktu',
				header: 'Waktu',
				sortable: true,
				width: 70,
				editor:new Nci.form.Combobox.autoComplete({
					store	: GzPermintaanUmum.form.ArrayStore.pasien,
					select	: function(a,b,c){
						var line	= GzPermintaanUmum.form.Grid.a.getSelectionModel().selection.cell[0];
						dsDataGrdUmum_viGzPermintaanUmum.getRange()[line].data.waktu=b.data.waktu;
						dsDataGrdUmum_viGzPermintaanUmum.getRange()[line].data.kd_waktu=b.data.kd_waktu;
						kdWaktuMintaUmum=b.data.kd_waktu;
						GzPermintaanUmum.vars.jenis_diet=b.data.jenis_diet;
						/*GzPermintaanUmum.vars.tgl_masuk=b.data.tgl_masuk;
						GzPermintaanUmum.vars.no_kamar=b.data.no_kamar;
						GzPermintaanUmum.vars.kd_unit=b.data.kd_unit; */
						
						//saveUmum_viGzPermintaanUmum();
						
						GzPermintaanUmum.form.Grid.a.getView().refresh();
					},
					insert	: function(o){
						return {
							kd_waktu       	: o.kd_waktu,
							waktu 			: o.waktu,
							text			: '<table style="font-size: 11px;"><tr><td width="50">'+o.kd_waktu+'</td><td width="200">'+o.waktu+'</td></tr></table>'
							//kd_unit			: o.kd_unit
						}
					},
					url		: baseURL + "index.php/gizi/functionPermintaanUmum/getWaktu",
					valueField: 'waktu',
					displayField: 'text',
					listWidth: 380
				})
			},
			{			
				dataIndex: 'jenis_diet',
				header: 'Jenis Diet',
				sortable: true,
				width: 250,
				editor:new Nci.form.Combobox.autoComplete({
					store	: GzPermintaanUmum.form.ArrayStore.pasien,
					select	: function(a,b,c){
						var line	= GzPermintaanUmum.form.Grid.a.getSelectionModel().selection.cell[0];
						dsDataGrdUmum_viGzPermintaanUmum.getRange()[line].data.kd_jenis=b.data.kd_jenis;
						dsDataGrdUmum_viGzPermintaanUmum.getRange()[line].data.jenis_diet=b.data.jenis_diet;
						dsDataGrdUmum_viGzPermintaanUmum.getRange()[line].data.harga=b.data.harga;
						dsDataGrdUmum_viGzPermintaanUmum.getRange()[line].data.totjumlah=0;
						kdJenisMintaUmum=b.data.kd_jenis;
						GzPermintaanUmum.vars.jenis_diet=b.data.jenis_diet;
						/*GzPermintaanUmum.vars.tgl_masuk=b.data.tgl_masuk;
						GzPermintaanUmum.vars.no_kamar=b.data.no_kamar;
						GzPermintaanUmum.vars.kd_unit=b.data.kd_unit; */
						
						//saveUmum_viGzPermintaanUmum();
						
						GzPermintaanUmum.form.Grid.a.getView().refresh();
					},
					insert	: function(o){
						console.log(o);
						return {
							kd_jenis       	: o.kd_jenis,
							jenis_diet 		: o.jenis_diet,
							harga			: o.harga,
							totharga		: o.totjumlah,
							text			: '<table style="font-size: 11px;"><tr><td width="50">'+o.kd_jenis+'</td><td width="200">'+o.jenis_diet+'</td></tr></table>'
							//kd_unit			: o.kd_unit
						}
					},
					url		: baseURL + "index.php/gizi/functionPermintaanUmum/getUmum",
					valueField: 'jenis_diet',
					displayField: 'text',
					listWidth: 380
				})
			},
			{
				dataIndex: 'harga',
				header: 'Harga',
				xtype:'numbercolumn',
				width: 70,
				align:'right'
			},{
				dataIndex: 'jumlah',
				header: 'Jumlah',
				xtype:'numbercolumn',
				sortable: true,
				align:'right',
				width: 40,
				editor: new Ext.form.NumberField({
					allowBlank: true,
					enableKeyEvents : true,
					listeners:{
						blur: function(a){
							var line	= this.index;
							var jumlah = dsDataGrdUmum_viGzPermintaanUmum.data.items[line].data.jumlah;
							var harga =dsDataGrdUmum_viGzPermintaanUmum.data.items[line].data.harga;
							
							dsDataGrdUmum_viGzPermintaanUmum.getRange()[line].set("totjumlah", harga * jumlah);
							//dsDataGrdUmum_viGzPermintaanUmum.getRange()[line].data.jumlah=a.getValue();
							//totalBayar();
						},
						focus: function(a){
							this.index=GzPermintaanUmum.form.Grid.a.getSelectionModel().selection.cell[0]
						},
					}
				})
			},{
				dataIndex: 'totjumlah',
				header: 'Total',
				xtype:'numbercolumn',
				sortable: true,
				width: 70,
				align:'right'
			},
			{
				dataIndex: 'kd_jenis',
				header: 'kd_jenis',
				sortable: true,
				width: 45,
				align:'center',
				hidden:true
			},
			{
				dataIndex: 'kd_waktu',
				header: 'kd waktu',
				sortable: true,
				width: 45,
				align:'center',
				hidden:true
			}
			
        ]),
		viewConfig:{
			forceFit: true
		}
    });
    return GzPermintaanUmum.form.Grid.a;
}
function save_dataMintaUmum()
{
	if (ValidasiEntriMintaUmum(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/gizi/functionPermintaanUmum/save",
				params: ParameterSaveMintaUmum(),
				failure: function(o)
				{
					ShowPesanErrorMintaUmum('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true && cst.simpan===true) 
					{
						ShowPesanSuksesMintaUmum(nmPesanSimpanSukses,nmHeaderSimpanData);
						//Ext.getCmp('TxtKdAhliGz').setValue(cst.kodeahligizi);
						dataGriAwal();
						Ext.getCmp('TxtNoMintaPermintaanUmumL').setValue(getNoMintaOtomatisMintaUmum());
					}
					else 
					{
						ShowPesanErrorMintaUmum('Gagal Menyimpan Data ini', 'Error');
					};
				}
			}
			
		)
	}
}
function ParameterSaveMintaUmum()
{
	var params=
	{
		Table:'vPermintaanUmum',
		jumlahrecord:dsDataGrdUmum_viGzPermintaanUmum.getCount(),
		nomintaumum:varNoOtoMinta,
		tglmintaumum:Ext.getCmp('dtpTglMintaUmumL').getValue(),
		tglmakanumum:Ext.getCmp('dtpTglMakanUmumL').getValue(),
		keterangan:Ext.getCmp('TxtKetPermintaanUmumL').getValue(),
		nama:Ext.getCmp('TxNamaMintaUmumL').getValue(),
		alamat:Ext.getCmp('TxAlamatMintaUmumL').getValue(),
	}
	for(var i = 0 ; i < dsDataGrdUmum_viGzPermintaanUmum.getCount();i++)
	{
		params['kodeWaktuMintaUmum-'+i]=dsDataGrdUmum_viGzPermintaanUmum.getRange()[i].data.kd_waktu;
		params['kodeJenisDietMintaUmum-'+i]=dsDataGrdUmum_viGzPermintaanUmum.data.items[i].data.kd_jenis;
		params['hargaMintaUmum-'+i]=dsDataGrdUmum_viGzPermintaanUmum.data.items[i].data.harga;
		params['jumlahMintaUmum-'+i]=dsDataGrdUmum_viGzPermintaanUmum.data.items[i].data.jumlah;
	}
return params;	
}
function ValidasiEntriMintaUmum(modul,mBolHapus)
{
	//var kode = Ext.getCmp('TxtKdAhliGz').getValue();
	var x = 1;
	var nominta = Ext.getCmp('TxtNoMintaPermintaanUmumL').getValue();
	var keterangan = Ext.getCmp('TxtKetPermintaanUmumL').getValue();
	var nama = Ext.getCmp('TxNamaMintaUmumL').getValue();
	var alamat = Ext.getCmp('TxAlamatMintaUmumL').getValue();
	//var x = 1;
	/*if(kode === '' ){
	ShowPesanErrorAhliGizi('Data Belum Diisi Lengkap', 'Warning');
	x = 0;
	}*/
	if( nama==='' && alamat==='')
	{
		ShowPesanErrorMintaUmum('Data Belum Diisi Lengkap', 'Warning');
	}
	
	var datagridmintaumum=dsDataGrdUmum_viGzPermintaanUmum.getCount();
	if(datagridmintaumum==0){
	ShowPesanErrorMintaUmum('Data Belum Diisi Lengkap', 'Warning');
	x = 0;
	}
	for(var i = 0 ; i < dsDataGrdUmum_viGzPermintaanUmum.getCount();i++)
	{
		var o=dsDataGrdUmum_viGzPermintaanUmum.getRange()[i].data;
		for(var j = i+1 ; j < dsDataGrdUmum_viGzPermintaanUmum.getCount();j++)
		{
			var p=dsDataGrdUmum_viGzPermintaanUmum.getRange()[j].data;
			if (p.kd_jenis==o.kd_jenis)
			{
				ShowPesanErrorMintaUmum('Jenis diet Tidak Boleh Sama', 'Warning');
				x = 0;
			}
		}
		
	}
	return x;
};
function ShowPesanSuksesMintaUmum(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};
function ShowPesanErrorMintaUmum(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:290
		}
	);
};
function getItemGridDetail_viGzPermintaanUmum(lebar,rowdata) 
{
    var items =
	{
		title: 'Detail Umum', 
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
		bodyStyle: 'margin-top: -1px;',
		border:true,
		width: lebar-80,
		height: 150,//300, 
	    items:
		[
		
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
				items:
				[
					gridDataViewDetaiUmum_viGzPermintaanUmum()
				]	
			}
			//-------------- ## --------------
		],
		tbar:
		[
			{
				text	: 'Add Umum',
				id		: 'btnAddDetUmumGzPermintaanUmumL',
				tooltip	: nmLookup,
				iconCls	: 'find',
				disabled:true,
				handler	: function(){
					var records = new Array();
					records.push(new dsTRDetailUmumGzPermintaanUmum.recordType());
					dsTRDetailUmumGzPermintaanUmum.add(records);
					
				}
			},{
				xtype: 'tbseparator'
			},{
				xtype: 'button',
				text: 'Delete',
				iconCls: 'remove',
				disabled:true,
				id: 'btnDeleteDet_viGzPermintaanUmum',
				handler: function()
				{
					var line = gridDTLUmum_GzPermintaanUmum.getSelectionModel().selection.cell[0];
					var o = dsTRDetailUmumGzPermintaanUmum.getRange()[line].data;
					if(dsTRDetailUmumGzPermintaanUmum.getCount()>1){
						Ext.Msg.confirm('Warning', 'Apakah jenis diet ini akan dihapus?', function(button){
							if (button == 'yes'){
								if(dsTRDetailUmumGzPermintaanUmum.getRange()[line].data.kd_jenis != undefined){
									Ext.Ajax.request
									(
										{
											url: baseURL + "index.php/gizi/functionPermintaanUmum/hapusBarisDetailUmum",
											params: {		
												kd_jenis:o.kd_jenis, 
												kd_waktu:o.kd_waktu, 
												no_minta:Ext.getCmp('txtTmpNoPermintaanGzPermintaanUmumL').getValue(),
												kd_pasien:Ext.getCmp('txtTmpKdUmumGzPermintaanUmumL').getValue()
											},
											failure: function(o) {
												ShowPesanErrorGzPermintaanUmum('Error, delete detail diet! Hubungi Admin', 'Error');
											},	
											success: function(o) {
												var cst = Ext.decode(o.responseText);
												if (cst.success === true) 
												{
													ShowPesanInfoGzPermintaanUmum('Berhasil menghapus jenis diet ini', 'Information');
													dsTRDetailUmumGzPermintaanUmum.removeAt(line);
													gridDTLUmum_GzPermintaanUmum.getView().refresh();
												}
												else 
												{
													ShowPesanErrorGzPermintaanUmum('Gagal menghapus data jenis diet ini', 'Error');
												};
											}
										}
										
									)
								}else{
									dsTRDetailUmumGzPermintaanUmum.removeAt(line);
									gridDTLUmum_GzPermintaanUmum.getView().refresh();
								}
							} 
							
						});
					} else{
						ShowPesanErrorGzPermintaanUmum('Data tidak bisa dihapus karena minimal jenis diet 1','Error');
					}
					
				}
			}	
		]
	};
    return items;
};

function gridDataViewDetaiUmum_viGzPermintaanUmum() 
{

    var fldDetail = [];
	
    dsTRDetailUmumGzPermintaanUmum = new WebApp.DataStore({ fields: fldDetail })
		 
    gridDTLUmum_GzPermintaanUmum = new Ext.grid.EditorGridPanel
    (
        {
            store: dsTRDetailUmumGzPermintaanUmum,
            border: false,
            columnLines: true,
            height: 95,
			autoScroll:true,
            selModel: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                            //cellSelecteddeskripsiRWI = dsTRDetailUmumGzPermintaanUmumList.getAt(row);
                            //CurrentGzPermintaanUmum.row = row;
                            //CurrentGzPermintaanUmum.data = cellSelecteddeskripsiRWI;
                        }
                    }
                }
            ),
            stripeRows: true,
            columns:
			[
				{
					header: 'kd waktu',
					dataIndex: 'kd_waktu',
					width:100,
					hidden:true
				},
				{			
					dataIndex: 'waktu',
					header: 'Waktu',
					sortable: false,
					width: 250,	
					editor:new Nci.form.Combobox.autoComplete({
						store	: GzPermintaanUmum.form.ArrayStore.waktu,
						select	: function(a,b,c){
							var line	= gridDTLUmum_GzPermintaanUmum.getSelectionModel().selection.cell[0];
							dsTRDetailUmumGzPermintaanUmum.getRange()[line].data.kd_waktu=b.data.kd_waktu;
							dsTRDetailUmumGzPermintaanUmum.getRange()[line].data.waktu=b.data.waktu;
							
							GzPermintaanUmum.vars.kd_waktu=b.data.kd_waktu;
							
							gridDTLUmum_GzPermintaanUmum.getView().refresh();
						},
						insert	: function(o){
							return {
								kd_waktu	: o.kd_waktu,
								waktu		: o.waktu,
								text		:  '<table style="font-size: 11px;"><tr><td width="100">'+o.waktu+'</td></tr></table>'
							}
						},
						url		: baseURL + "index.php/gizi/functionPermintaanUmum/getWaktu",
						valueField: 'waktu',
						displayField: 'text',
						listWidth: 150
					})
				},
				{			
					dataIndex: 'jenis_diet',
					header: 'Jenis diet',
					sortable: false,
					width: 250,	
					editor:new Nci.form.Combobox.autoComplete({
						store	: GzPermintaanUmum.form.ArrayStore.jenisdiet,
						select	: function(a,b,c){
							var line	= gridDTLUmum_GzPermintaanUmum.getSelectionModel().selection.cell[0];
							dsTRDetailUmumGzPermintaanUmum.getRange()[line].data.kd_jenis=b.data.kd_jenis;
							dsTRDetailUmumGzPermintaanUmum.getRange()[line].data.jenis_diet=b.data.jenis_diet;
							
							GzPermintaanUmum.vars.kd_jenis=b.data.kd_jenis;
							
							loadMask.show();
							
							saveDetailUmum_viGzPermintaanUmum();
							
							gridDTLUmum_GzPermintaanUmum.getView().refresh();
						},
						insert	: function(o){
							return {
								kd_jenis	: o.kd_jenis,
								jenis_diet	: o.jenis_diet,
								text		:  '<table style="font-size: 11px;"><tr><td width="100">'+o.jenis_diet+'</td></tr></table>'
							}
						},
						url		: baseURL + "index.php/gizi/functionPermintaanUmum/getJenisUmum",
						valueField: 'jenis_diet',
						displayField: 'text',
						listWidth: 150
					})
				},
				{
					header: 'kd jenis',
					dataIndex: 'kd_jenis',
					width:100,
					hidden:true
				},
				{
					header: 'kd pasien',
					dataIndex: 'kd_pasien',
					width:100,
					hidden:true
				}
            ],
			viewConfig: {forceFit: true}
			
	 
		}
    );
    return gridDTLUmum_GzPermintaanUmum;
};


function ComboAhliGiziGzPermintaanUmumLookup()
{
    var Field_ahliGiziLookup = ['KD_AHLI_GIZI', 'NAMA_AHLI_GIZI'];
    ds_ahliGiziGZPermintaanLookup = new WebApp.DataStore({fields: Field_ahliGiziLookup});
    ds_ahliGiziGZPermintaanLookup.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'KD_AHLI_GIZI',
					Sortdir: 'ASC',
					target: 'ComboAhliGizi',
					param: ''
				}
		}
	);
	
    var cbo_AhliGiziGzPermintaanUmumLookup = new Ext.form.ComboBox
    (
        {
			x: 420,
			y: 0,
            flex: 1,
			id: 'cbo_AhliGiziGzPermintaanUmumLookup',
			valueField: 'KD_AHLI_GIZI',
            displayField: 'NAMA_AHLI_GIZI',
			store: ds_ahliGiziGZPermintaanLookup,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:150,
			tabIndex:2,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					Ext.getCmp('btnAddUmumGzPermintaanUmumL').enable();
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_AhliGiziGzPermintaanUmumLookup;
};

function ComboAhliGiziGzPermintaanUmum()
{
    var Field_ahliGizi = ['KD_AHLI_GIZI', 'NAMA_AHLI_GIZI'];
    ds_ahliGiziGZPermintaan = new WebApp.DataStore({fields: Field_ahliGizi});
    ds_ahliGiziGZPermintaan.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'KD_AHLI_GIZI',
					Sortdir: 'ASC',
					target: 'ComboAhliGizi',
					param: ''
				}
		}
	);
	
    var cbo_AhliGiziGzPermintaanUmum = new Ext.form.ComboBox
    (
        {
			x: 130,
			y: 30,
            flex: 1,
			id: 'cbo_AhliGiziGzPermintaanUmum',
			valueField: 'KD_AHLI_GIZI',
            displayField: 'NAMA_AHLI_GIZI',
			store: ds_ahliGiziGZPermintaan,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:150,
			tabIndex:2,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						tmpkriteria = getCriteriaCariGzPermintaanUmum();
						refreshPermintaanUmum(tmpkriteria);
					} 						
				}
			}
        }
    )    
    return cbo_AhliGiziGzPermintaanUmum;
};


function ComboUnitGzPermintaanUmum(){
    var Field_Vendor = ['KD_UNIT', 'NAMA_UNIT'];
    ds_unit = new WebApp.DataStore({fields: Field_Vendor});
    ds_unit.load({
        params:{
	        Skip: 0,
	        Take: 1000,
	        Sort: 'kd_unit',
	        Sortdir: 'ASC',
	        target: 'ComboUnitApotek',
	        param: "parent='100' ORDER BY nama_unit"
        }
    });
    var cbo_UnitGzPermintaanUmum = new Ext.form.ComboBox({
			x:410,
			y:0,
            flex: 1,
			id: 'cbo_UnitGzPermintaanUmum',
			valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
			emptyText:'Unit/Ruang',
			store: ds_unit,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:150,
			tabIndex:2,
			listeners:{ 
				'select': function(a,b,c){
					selectSetUnit=b.data.displayText ;
				},
				'specialkey' : function(){
					if (Ext.EventObject.getKey() === 13) {
						tmpkriteria = getCriteriaCariGzPermintaanUmum();
						refreshPermintaanUmum(tmpkriteria);
					} 						
				}
			}
        }
    )    
    return cbo_UnitGzPermintaanUmum;
}

function ComboUnitGzPermintaanUmumLookup()
{
    var Field_Vendor = ['KD_UNIT', 'NAMA_UNIT'];
    ds_unitL = new WebApp.DataStore({fields: Field_Vendor});
    ds_unitL.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: 'kd_unit',
					Sortdir: 'ASC',
					target: 'ComboUnitApotek',
					param: "parent='100' ORDER BY nama_unit"
				}
		}
	);
	
    var cbo_UnitGzPermintaanUmumLookup = new Ext.form.ComboBox
    (
        {
			x:130,
			y:60,
			id: 'cbo_UnitGzPermintaanUmumLookup',
			store: ds_unitL,
			valueField: 'KD_UNIT',
            displayField: 'NAMA_UNIT',
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:150,
			tabIndex:3,
			listeners:
			{ 
				'select': function(a,b,c)
				{
					
				},
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_UnitGzPermintaanUmumLookup;
}

function datainit_viGzPermintaanUmum(rowdata)
{
	var tgl_minta2 = rowdata.tgl_minta.split(" ");
	var tgl_makan2 = rowdata.tgl_makan.split(" ");
	Ext.getCmp('TxtNoMintaPermintaanUmumL').setValue(rowdata.no_minta);
	Ext.getCmp('dtpTglMintaUmumL').setValue(tgl_minta2[0]);
	Ext.getCmp('dtpTglMakanUmumL').setValue(ShowDate(rowdata.tgl_makan));
	Ext.getCmp('TxNamaMintaUmumL').setValue(rowdata.nama);
	Ext.getCmp('TxAlamatMintaUmumL').setValue(rowdata.alamat);
	Ext.getCmp('TxtKetPermintaanUmumL').setValue(rowdata.keterangan);

	Ext.getCmp('TxtNoMintaPermintaanUmumL').setReadOnly(true);
	Ext.getCmp('dtpTglMintaUmumL').setReadOnly(true);
	Ext.getCmp('dtpTglMakanUmumL').setReadOnly(true);
	Ext.getCmp('TxNamaMintaUmumL').setReadOnly(true);
	Ext.getCmp('TxAlamatMintaUmumL').setReadOnly(true);
	Ext.getCmp('TxtKetPermintaanUmumL').setReadOnly(true);
	
	
	getGridUmum(rowdata.no_minta);
	cekOrder(rowdata.no_minta);
	
};

function dataGriAwal(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionPermintaanUmum/getDataGridAwal",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorGzPermintaanUmum('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dataSource_viGzPermintaanUmum.removeAll();
					var recs=[],
						recType=dataSource_viGzPermintaanUmum.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
						dataSource_viGzPermintaanUmum.add(recs);
					
					
					
					GridDataView_viGzPermintaanUmum.getView().refresh();
				}
				else 
				{
					ShowPesanErrorGzPermintaanUmum('Gagal membaca data obat', 'Error');
				};
			}
		}
		
	)
	
}


function getGridUmum(no_minta){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionPermintaanUmum/getGridUmum",
			params: {nominta:no_minta},
			failure: function(o)
			{
				ShowPesanErrorGzPermintaanUmum('Error, pasien! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dsDataGrdUmum_viGzPermintaanUmum.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
					dsDataGrdUmum_viGzPermintaanUmum.add(recs);
					
					GzPermintaanUmum.form.Grid.a.getView().refresh();
				}
				else 
				{
					ShowPesanErrorGzPermintaanUmum('Gagal membaca data pasien ini', 'Error');
				};
			}
		}
		
	)
	
}

function getGridWaktu(no_minta,kd_pasien){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionPermintaanUmum/getGridWaktu",
			params: {nominta:no_minta, kdpasien:kd_pasien},
			failure: function(o)
			{
				ShowPesanErrorGzPermintaanUmum('Error, detail diet! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				
				if (cst.success === true) 
				{
					var recs=[],
						recType=dsTRDetailUmumGzPermintaanUmum.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
					dsTRDetailUmumGzPermintaanUmum.add(recs);
					
					gridDTLUmum_GzPermintaanUmum.getView().refresh();
				}
				else 
				{
					ShowPesanErrorGzPermintaanUmum('Gagal membaca data detail diet pasien ini', 'Error');
				};
			}
		}
		
	)
	
}


function cekOrder(no_minta){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionPermintaanUmum/cekOrderPermintaanUmum",
			params: {nominta:no_minta},
			failure: function(o)
			{
				ShowPesanErrorGzPermintaanUmum('Error, cek order! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					Ext.getCmp('btnAddUmumGzPermintaanUmumL').enable();
					Ext.getCmp('btnDelete_viGzPermintaanUmum').enable();
					Ext.getCmp('btnDeletePermintaan_viGzPermintaanUmum').enable();
					GzPermintaanUmum.vars.status_order='false';
				}
				else 
				{
					ShowPesanInfoGzPermintaanUmum('Permintaan diet ini sudah di order', 'Information');
					Ext.getCmp('btnAddUmumGzPermintaanUmumL').disable();
					Ext.getCmp('btnDelete_viGzPermintaanUmum').disable();
					Ext.getCmp('btnDeletePermintaan_viGzPermintaanUmum').disable();
					Ext.getCmp('btnSave_viGzPermintaanUmum').disable();
					GzPermintaanUmum.vars.status_order='true';
				};
			}
		}
		
	)
}

function saveMinta_viGzPermintaanUmum(){
	if (ValidasiEntryPermintaanGzPermintaanUmum(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/gizi/functionPermintaanUmum/saveMinta",
				params: getParamMintaGzPermintaanUmum(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorGzPermintaanUmum('Error, permintaan! Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						Ext.getCmp('txtTmpNoPermintaanGzPermintaanUmumL').setValue(cst.nominta);
						Ext.getCmp('txtNoPermintaanGzPermintaanUmumL').setValue(cst.nominta);
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorGzPermintaanUmum('Gagal menyimpan data permintaan', 'Error');
					};
				}
			}
		)
	}
}


function saveUmum_viGzPermintaanUmum(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionPermintaanUmum/saveMintaUmum",
			params: getParamMintaUmumGzPermintaanUmum(),
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorGzPermintaanUmum('Error, pasien! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					loadMask.hide();
					Ext.getCmp('txtTmpKdUmumGzPermintaanUmumL').setValue(cst.kdpasien);
					if(cst.error=='ada'){
						ShowPesanWarningGzPermintaanUmum('Umum ini susah ada dalam transaksi pemintaan diet hari ini, hapus pasien ini untuk melanjutkan', 'Warning');
						Ext.getCmp('btnDelete_viGzPermintaanUmum').enable();
						Ext.getCmp('btnAddUmumGzPermintaanUmumL').disable();
						Ext.getCmp('btnAddDetUmumGzPermintaanUmumL').disable();
					} else{
						if(GzPermintaanUmum.vars.status_order == 'false'){
							Ext.getCmp('btnAddUmumGzPermintaanUmumL').disable();
							Ext.getCmp('btnDelete_viGzPermintaanUmum').disable();
							Ext.getCmp('btnAddDetUmumGzPermintaanUmumL').enable();
						} else{
							Ext.getCmp('btnAddUmumGzPermintaanUmumL').disable();
							Ext.getCmp('btnDelete_viGzPermintaanUmum').disable();
							Ext.getCmp('btnAddDetUmumGzPermintaanUmumL').disable();
						}
					}
				}
				else 
				{
					loadMask.hide();
					ShowPesanErrorGzPermintaanUmum('Gagal menyimpan data pasien', 'Error');
				};
			}
		}
	)
}

function saveDetailUmum_viGzPermintaanUmum(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionPermintaanUmum/saveMintaUmumDetail",
			params: getParamDetailUmumGzPermintaanUmum(),
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorGzPermintaanUmum('Error, detail diet! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					if(cst.ada=='0'){
						loadMask.hide();
						dataGriAwal()
						Ext.getCmp('btnAddUmumGzPermintaanUmumL').enable();
						Ext.getCmp('btnDeleteDet_viGzPermintaanUmum').enable();
						Ext.getCmp('txtNoPermintaanGzPermintaanUmumL').setValue(Ext.getCmp('txtTmpNoPermintaanGzPermintaanUmumL').getValue());
					} else{
						loadMask.hide();
						ShowPesanWarningGzPermintaanUmum('Jenis diet ini sudah ada dengan waktu yang sama, hapus salah satu untuk melanjutkan', 'Error');
						Ext.getCmp('btnAddUmumGzPermintaanUmumL').disable();
						Ext.getCmp('btnAddDetUmumGzPermintaanUmumL').disable();
						Ext.getCmp('btnDeleteDet_viGzPermintaanUmum').enable();
					}
				}
				else 
				{
					loadMask.hide();
					ShowPesanErrorGzPermintaanUmum('Gagal menyimpan data detail diet', 'Error');
				};
			}
		}
	)
}


function printbill()
{
    Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/main/CreateDataObj",
			params: datacetakbill(),
			failure: function(o)
			{	
				ShowPesanErrorGzPermintaanUmum('Error hubungi admin' ,'Error');
			},
			success: function(o)
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					
				}
				else if  (cst.success === false && cst.pesan===0)
				{
					ShowPesanWarningGzPermintaanUmum('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
				}
				else
				{
					ShowPesanErrorGzPermintaanUmum('Gagal print '  + 'Error');
				}
			}
		}
	)
}

function getParamMintaGzPermintaanUmum() 
{
    var params =
	{	
		NoMinta: Ext.getCmp('txtNoPermintaanGzPermintaanUmumL').getValue(),
		KdUnit: Ext.getCmp('cbo_UnitGzPermintaanUmumLookup').getValue(),
		TglMinta:Ext.getCmp('dfTglMintaGzPermintaanUmumL').getValue(),
		TglMakan:Ext.getCmp('dfTglUntukGzPermintaanUmumL').getValue(),
		AhliGizi:Ext.getCmp('cbo_AhliGiziGzPermintaanUmumLookup').getValue(),
		Ket:Ext.getCmp('txtKetGZPenerimaanL').getValue()
	};
    return params
};

function getParamMintaUmumGzPermintaanUmum() 
{
    var params =
	{
		NoMinta:Ext.getCmp('txtTmpNoPermintaanGzPermintaanUmumL').getValue(),
		KdUnit: Ext.getCmp('cbo_UnitGzPermintaanUmumLookup').getValue(),
		KdUmum:GzPermintaanUmum.vars.kd_pasien,
		TglMasuk:GzPermintaanUmum.vars.tgl_masuk,
		NoKamar:GzPermintaanUmum.vars.no_kamar
	};
	
    return params
};

function getParamDetailUmumGzPermintaanUmum() 
{
    var params =
	{
		NoMinta:Ext.getCmp('txtTmpNoPermintaanGzPermintaanUmumL').getValue(),
		KdUmum: Ext.getCmp('txtTmpKdUmumGzPermintaanUmumL').getValue(),
		kd_jenis:GzPermintaanUmum.vars.kd_jenis,
		kd_waktu:GzPermintaanUmum.vars.kd_waktu
	};
	
    return params
};

function datacetakbill(){
	var params =
	{
		Table: 'billprintingGzPermintaanUmum',
		NoResep:Ext.getCmp('txtNoResepGzPermintaanUmumL').getValue(),
		NoOut:Ext.getCmp('txtTmpNooutGzPermintaanUmumL').getValue(),
		TglOut:Ext.getCmp('txtTmpTgloutGzPermintaanUmumL').getValue(),
		KdUmum:Ext.getCmp('txtKdUmumGzPermintaanUmumL').getValue(),
		NamaUmum:GzPermintaanUmum.form.ComboBox.namaUmum.getValue(),
		JenisUmum:Ext.get('cboPilihankelompokUmumAptGzPermintaanUmum').getValue(),
		Kelas:Ext.get('cbo_UnitGzPermintaanUmumL').getValue(),
		Dokter:Ext.get('cbo_DokterGzPermintaanUmum').getValue(),
		Total:Ext.get('txtTotalBayarGzPermintaanUmumL').getValue(),
		Tot:toInteger(Ext.get('txtTotalBayarGzPermintaanUmumL').getValue())
		
	}
	params['jumlah']=dsDataGrdUmum_viGzPermintaanUmum.getCount();
	for(var i = 0 ; i < dsDataGrdUmum_viGzPermintaanUmum.getCount();i++)
	{
		params['nama_obat-'+i]=dsDataGrdUmum_viGzPermintaanUmum.data.items[i].data.nama_obat;
		params['jml-'+i]=dsDataGrdUmum_viGzPermintaanUmum.data.items[i].data.jml;
	}
	
    return params
}

function totalBayar(){

	
	for(var i=0; i < dsDataGrdUmum_viGzPermintaanUmum.getCount() ; i++){
		var total=0;
		var o=dsDataGrdUmum_viGzPermintaanUmum.getRange()[i].data;
		if(o.harga != undefined){
			
			total = parseFloat(o.harga)*parseFloat(o.jumlah);
			
			o.totjumlah=total;
			console.log(total);
			console.log(o.totjumlah);
		}
	}
}

function getCriteriaCariGzPermintaanUmum()//^^^
{
      	 var strKriteria = "";

			if (Ext.getCmp('TxtNoPermintaanFilterGridDataView_viGzPermintaanUmum').getValue() != "")
            {
                strKriteria = " no_minta LIKE upper('" + Ext.getCmp('TxtNoPermintaanFilterGridDataView_viGzPermintaanUmum').getValue() +"%')";
            }
            
            if (Ext.get('TxtNamaPermintaanFilterGridDataView_viGzPermintaanUmum').getValue() != "")//^^^
            {
				if (strKriteria == "")
				{
					strKriteria = " lower(nama) LIKE lower('" + Ext.get('TxtNamaPermintaanFilterGridDataView_viGzPermintaanUmum').getValue() +"%')" ;
				}
				else {
					strKriteria += " AND lower(nama) LIKE lower('" + Ext.get('TxtNamaPermintaanFilterGridDataView_viGzPermintaanUmum').getValue() +"%')";
				}
            }
			
			if (Ext.get('dfTglAwalGzPermintaanUmum').getValue() != "" && Ext.get('dfTglAkhirGzPermintaanUmum').getValue() != "")
            {
                if (strKriteria == "")
				{
					strKriteria = " tgl_minta between '" + Ext.get('dfTglAwalGzPermintaanUmum').getValue() + "' and '" + Ext.get('dfTglAkhirGzPermintaanUmum').getValue() + "'" ;
				}
				else {
					strKriteria += " and tgl_minta between '" + Ext.get('dfTglAwalGzPermintaanUmum').getValue() + "' and '" + Ext.get('dfTglAkhirGzPermintaanUmum').getValue() + "'" ;
				}
                
            }
			
		strKriteria= strKriteria + " ORDER BY no_minta";
	 return strKriteria;
}

function ValidasiEntryPermintaanGzPermintaanUmum(modul,mBolHapus)
{
	var x = 1;
	if(Ext.getCmp('cbo_UnitGzPermintaanUmumLookup').getValue() === '' || Ext.getCmp('cbo_AhliGiziGzPermintaanUmumLookup').getValue() === '' ||(Ext.getCmp('dfTglUntukGzPermintaanUmumL').getValue() < Ext.getCmp('dfTglMintaGzPermintaanUmumL').getValue()) ){
		if(Ext.getCmp('cbo_UnitGzPermintaanUmumLookup').getValue() === ''){
			loadMask.hide();
			ShowPesanWarningGzPermintaanUmum('Unit tidak boleh kosong', 'Warning');
			x = 0;
		} else if(Ext.getCmp('cbo_AhliGiziGzPermintaanUmumLookup').getValue() === ''){
			loadMask.hide();
			ShowPesanWarningGzPermintaanUmum('Ahli gizi tidak boleh kosong', 'Warning');
			x = 0;
		} else {
			loadMask.hide();
			ShowPesanWarningGzPermintaanUmum('Untuk tanggal tidak boleh kurang dari tanggal permintaan', 'Warning');
			x = 0;
		} 
	}
	return x;
};

function dataAddNew_viGzPermintaanUmum(){
	GzPermintaanUmum.vars.status_order='';
	
	Ext.getCmp('TxtNoMintaPermintaanUmumL').setValue('');
	Ext.getCmp('dtpTglMintaUmumL').setValue(now_viGzPermintaanUmum);
	Ext.getCmp('dtpTglMakanUmumL').setValue(now_viGzPermintaanUmum);
	Ext.getCmp('TxtKetPermintaanUmumL').setValue('');
	Ext.getCmp('TxNamaMintaUmumL').setValue('');
	Ext.getCmp('TxAlamatMintaUmumL').setValue('');
	
	GzPermintaanUmum.vars.kd_unit='';
	dsDataGrdUmum_viGzPermintaanUmum.removeAll();
	//dsTRDetailUmumGzPermintaanUmum.removeAll();
	
	
	Ext.getCmp('btnAddUmumGzPermintaanUmumL').enable();
	Ext.getCmp('btnDelete_viGzPermintaanUmum').enable();
	Ext.getCmp('dtpTglMakanUmumL').setReadOnly(false);
	Ext.getCmp('TxNamaMintaUmumL').setReadOnly(false);
	Ext.getCmp('TxAlamatMintaUmumL').setReadOnly(false);
	Ext.getCmp('TxtKetPermintaanUmumL').setReadOnly(false); 
	
	getNoMintaOtomatisMintaUmum();
}
function getNoMintaOtomatisMintaUmum()
{
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionPermintaanUmum/getnomintaotomatis",
			params: {no_minta:'MNU'+getNoMintaOto},
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorGzPermintaanUmum('Error mendapatkan no minta otomatis! Hubungi Admin', 'Error');
			},	
				success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					varNoOtoMinta=cst.nomintaotomatis;
				}
				else 
				{
					ShowPesanWarningGzPermintaanUmum('Gagal mendapatkan no minta otomatis', 'Warning');	
				};
			}
		}
										
	)
	return varNoOtoMinta;
}

function ShowPesanWarningGzPermintaanUmum(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:290
		}
	);
};

function ShowPesanErrorGzPermintaanUmum(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:290
		}
	);
};


function ShowPesanInfoGzPermintaanUmum(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:290
		}
	);
};