var dataSourceAhliGizi;
var rowselectedAhliGizi;
var LookupAhliGizi;
var disDataAhliGizi=0;
var GzSetupAhliGizi={};
GzSetupAhliGizi.form={};
GzSetupAhliGizi.func={};
GzSetupAhliGizi.vars={};
GzSetupAhliGizi.func.parent=GzSetupAhliGizi;
GzSetupAhliGizi.form.ArrayStore={};
GzSetupAhliGizi.form.ComboBox={};
GzSetupAhliGizi.form.DataStore={};
GzSetupAhliGizi.form.Record={};
GzSetupAhliGizi.form.Form={};
GzSetupAhliGizi.form.Grid={};
GzSetupAhliGizi.form.Panel={};
GzSetupAhliGizi.form.TextField={};
GzSetupAhliGizi.form.Button={};

GzSetupAhliGizi.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_ahli_gizi', 'nama_ahli_gizi'],
	data: []
});
CurrentPage.page = getPanelAhliGizi(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function datasourcefunction_ahligizi(){
	dataSourceAhliGizi.load
        (
            { 
                params:  
                {   
                    Skip: '', 
                    Take: '',
                    Sort: '',
                    Sortdir: '', 
                    target: 'vAhliGizi'
                    //param : kriteria
                }			
            }
        );   
    return dataSourceAhliGizi;
	}
	
function getPanelAhliGizi(mod_id) {
    var Field = ['KD_AHLI_GIZI','NAMA_AHLI_GIZI'];
    dataSourceAhliGizi = new WebApp.DataStore({
        fields: Field
    });
	datasourcefunction_ahligizi();
    var gridListHasilAhliGizi = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dataSourceAhliGizi,
        anchor: '100% 80%',
        columnLines: false,
        autoScroll: false,
        border: true,
        sort: false,
        autoHeight: false,
        layout: 'fit',
        region: 'center',
        //height: 200,
        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
            listeners: {
                rowselect: function (sm, row, rec) {
					rowselectedAhliGizi=dataSourceAhliGizi.getAt(row);
                }
            }
        }),
        listeners: {
           rowdblclick: function (sm, ridx, cidx) {
                rowselectedAhliGizi=dataSourceAhliGizi.getAt(ridx);
				disDataAhliGizi=1;
				disabled_dataAhliGizi(disDataAhliGizi);
				ShowLookupAhliGizi(rowselectedAhliGizi.json);
            }
        },
        cm: new Ext.grid.ColumnModel(
                [
				new Ext.grid.RowNumberer({
					header:'No.'
					}),
                    {
                        id: 'colKdAhliGzViewHasilAhliGz',
                        header: 'Kode',
                        dataIndex: 'KD_AHLI_GIZI',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 5
                    },
                    {
                        id: 'colNamaAhliGzViewHasilAhliGz',
                        header: 'Nama Ahli Gizi',
                        dataIndex: 'NAMA_AHLI_GIZI',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
						width: 50
                    },
                ]
                ),
        viewConfig: {
            forceFit: true
        },
        tbar:[
			{
                id: 'btnEditDataAhliGz',
                text: 'Edit Data',
                tooltip: 'Edit Data',
                iconCls: 'Edit_Tr',
                handler: function (sm, row, rec) {
                    edit_dataAhliGizi();
                },
				
            },
		]
    });
	   var FormDepanAhliGz = new Ext.Panel({
        id: mod_id,
        closable: true,
        region: 'center',
        layout: 'form',
        title: 'Setup Ahli Gizi',
        border: false,
        shadhow: true,
        autoScroll: false,
		width: 250,
        iconCls: 'Request',
        margins: '0 5 5 0',
		tbar: [
			
			{
                id: 'btnAddNewDataAhliGz',
                text: 'Add New',
                tooltip: 'addAhliGizi',
                iconCls: 'add',
                handler: function (sm, row, rec) {
                    addnew_dataAhliGizi();
				}
            },
			{
        	xtype: 'tbseparator'
       		},
			{
                id: 'btnSaveDataAhliGz',
                text: 'Save Data',
                tooltip: 'Save Data',
                iconCls: 'Save',
                handler: function (sm, row, rec) {
                    save_dataAhliGizi();
                },
				
            },
			{
        	xtype: 'tbseparator'
       		},
			{
                id: 'btnDeleteDataAhliGz',
                text: 'Delete Data',
                tooltip: 'Delete Data',
                iconCls: 'Remove',
                handler: function (sm, row, rec) {
					if (rowselectedAhliGizi!=undefined)
                    	delete_dataAhliGizi();
					else
						ShowPesanErrorAhliGizi('Maaf ! Tidak ada data yang terpilih', 'Error');
				}
            },
			{
        	xtype: 'tbseparator'
       		},
			{
                id: 'btnRefreshDataAhliGz',
                text: 'Refresh',
                tooltip: 'Refresh',
                iconCls: 'Refresh',
                handler: function (sm, row, rec) {
                    datasourcefunction_ahligizi();
                },
				
            }
			],
        items: [
			getPanelPencarianAhliGz(),
			gridListHasilAhliGizi,
            
            
//            PanelPengkajian(),
//            TabPanelPengkajian()
        ],
        listeners: {
            'afterrender': function () {

            }
        }
    });

    return FormDepanAhliGz;

}
;
function getPanelPencarianAhliGz() {
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 1190,
                height: 75,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'Kode Ahli Gizi '
                    },
                    {
                        x: 110,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 10,
                        xtype: 'textfield',
                        name: 'TxtKdAhliGz',
                        id: 'TxtKdAhliGz',
                        width: 80,
						readOnly: true,
						
                    },
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Nama Ahli Gizi'
                    },
                    {
                        x: 110,
                        y: 40,
                        xtype: 'label',
                        text: ' : '
                    },
					GzSetupAhliGizi.vars.ahligizi=new Nci.form.Combobox.autoComplete({
								x: 120,
								y: 40,
								tabIndex:2,
								store	: GzSetupAhliGizi.form.ArrayStore.a,
								select	: function(a,b,c){
									console.log(b);
									Ext.getCmp('TxtKdAhliGz').setValue(b.data.kd_ahli_gizi);
									
								},
								insert	: function(o){
									return {
										kd_ahli_gizi      	: o.kd_ahli_gizi,
										nama_ahli_gizi 		: o.nama_ahli_gizi,
										text			:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_ahli_gizi+'</td><td width="200">'+o.nama_ahli_gizi+'</td></tr></table>'
									}
								},
								url		: baseURL + "index.php/gizi/ahligizi/getGridAutoAhliGizi",
								valueField: 'nama_ahli_gizi',
								displayField: 'nama_ahli_gizi',
								listWidth: 280
							}),
                    /*{
                        x: 120,
                        y: 40,
                        xtype: 'textfield',
                        name: 'TxtNamaAhliGz',
                        id: 'TxtNamaAhliGz',
                        width: 200,
                    },*/
                ]
            }
        ]
    };
    return items;
}
;
function save_dataAhliGizi()
{
	if (ValidasiEntriAhliGizi(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/main/CreateDataObj",
				params: ParameterSaveAhliGizi(),
				failure: function(o)
				{
					ShowPesanErrorAhliGizi('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanSuksesAhliGizi(nmPesanSimpanSukses,nmHeaderSimpanData);
						Ext.getCmp('TxtKdAhliGz').setValue(cst.kodeahligizi);
						datasourcefunction_ahligizi();
						ClearTextAhliGizi();
						disDataAhliGizi=0;
						disabled_dataAhliGizi(disDataAhliGizi);
					}
					else 
					{
						ShowPesanErrorAhliGizi('Gagal Menyimpan Data ini', 'Error');
						datasourcefunction_ahligizi();
					};
				}
			}
			
		)
	}
}
function edit_dataAhliGizi(){
	disDataAhliGizi=1;
	disabled_dataAhliGizi(disDataAhliGizi);
	ShowLookupAhliGizi(rowselectedAhliGizi.json);
}
function delete_dataAhliGizi() 
{
		Ext.Msg.show
		(
			{
			   title:nmHeaderHapusData,
			   msg: nmGetValidasiHapus(nmEmp2) ,
			   buttons: Ext.MessageBox.YESNO,
			   width:250,
			   fn: function (btn) 
			   {			
					if (btn === 'yes') 
					{
						Ext.Ajax.request
						(
							{
								url: WebAppUrl.UrlDeleteData,
								params: paramsDeleteAhliGizi(),
								success: function(o) 
								{
									var cst = Ext.decode(o.responseText);
									if (cst.success === true) 
									{
										ShowPesanSuksesAhliGizi(nmPesanHapusSukses,nmHeaderHapusData);
										Ext.getCmp('TxtKdAhliGz').setValue(cst.kodeahligizi);
										datasourcefunction_ahligizi();
										ClearTextAhliGizi();
										disDataAhliGizi=0;
										disabled_dataAhliGizi(disDataAhliGizi);
										
									}
									else if (cst.success === false && cst.pesan===0)
									{
										ShowPesanErrorAhliGizi(nmPesanHapusGagal,nmHeaderHapusData);
									}
									else 
									{
										ShowPesanErrorAhliGizi(nmPesanHapusError,nmHeaderHapusData);
									};
								}
							}
						)
						disDataAhliGizi=0;
						disabled_dataAhliGizi(disDataAhliGizi);
					};
				}
			}
		)
	};
	
function addnew_dataAhliGizi()
{
	ClearTextAhliGizi();
	disDataAhliGizi=0;
	disabled_dataAhliGizi(disDataAhliGizi);
	TombolUpdate :GzSetupAhliGizi.vars.ahligizi.focus();
}
function ValidasiEntriAhliGizi(modul,mBolHapus)
{
	//var kode = Ext.getCmp('TxtKdAhliGz').getValue();
	var nama = GzSetupAhliGizi.vars.ahligizi.getValue();
	
	var x = 1;
	/*if(kode === '' ){
	ShowPesanErrorAhliGizi('Data Belum Diisi Lengkap', 'Warning');
	x = 0;
	}*/
	if( nama === '')
	{
	ShowPesanErrorAhliGizi('Data Belum Diisi Lengkap', 'Warning');
	x = 0;	
	}
	
	
	return x;
};

function ShowPesanSuksesAhliGizi(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};

function ShowPesanErrorAhliGizi(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:290
		}
	);
};
function ClearTextAhliGizi()
{
	kodeData : Ext.getCmp('TxtKdAhliGz').setValue("");
	namaData : GzSetupAhliGizi.vars.ahligizi.setValue("");
}
function disabled_dataAhliGizi(disDatax){
	if (disDatax==1) {
		var dis= 
				{
					kodeData : Ext.getCmp('TxtKdAhliGz').disable(),
				}
	}
	else if (disDatax==0) 
	{
		var dis= 
				{
					kodeData : Ext.getCmp('TxtKdAhliGz').enable(),
				}
	}
	return dis;
	}
function ParameterSaveAhliGizi()
{
var params =
	{
	Table: 'vAhliGizi',	
	kodeData : Ext.getCmp('TxtKdAhliGz').getValue(),
	namaData : GzSetupAhliGizi.vars.ahligizi.getValue()
	}	
return params;	
}
function paramsDeleteAhliGizi() 
{
    var params =
	{
		Table:'vAhliGizi',
		Data: ShowLookupDeleteAhliGizi(rowselectedAhliGizi.json)
	};
    return params
};
function ShowLookupAhliGizi(rowdata)
{
	if (rowdata == undefined){
        ShowPesanErrorAhliGizi('Data Kosong', 'Error');
    }
    else
    {
      datainit_formAhliGizi(rowdata); 
    }
}
function ShowLookupDeleteAhliGizi(rowdata)
{
      var kode =  rowdata.KD_AHLI_GIZI;
	  return kode;
}
function datainit_formAhliGizi(rowdata){
	Ext.getCmp('TxtKdAhliGz').setValue(rowdata.KD_AHLI_GIZI);
	GzSetupAhliGizi.vars.ahligizi.setValue(rowdata.NAMA_AHLI_GIZI);
	}