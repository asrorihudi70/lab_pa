var dataSource_viGzSetupBahanMakanan;
var selectCount_viGzSetupBahanMakanan=50;
var NamaForm_viGzSetupBahanMakanan="Setup Bahan Makanan";
var mod_name_viGzSetupBahanMakanan="Setup Bahan Makanan";
var now_viGzSetupBahanMakanan= new Date();
var rowSelected_viGzSetupBahanMakanan;
var tanggal = now_viGzSetupBahanMakanan.format("d/M/Y");
var jam = now_viGzSetupBahanMakanan.format("H/i/s");
var GridDataView_viGzSetupBahanMakanan;


var CurrentData_viGzSetupBahanMakanan =
{
	data: Object,
	details: Array,
	row: 0
};

var GzSetupBahanMakanan={};
GzSetupBahanMakanan.form={};
GzSetupBahanMakanan.func={};
GzSetupBahanMakanan.vars={};
GzSetupBahanMakanan.func.parent=GzSetupBahanMakanan;
GzSetupBahanMakanan.form.ArrayStore={};
GzSetupBahanMakanan.form.ComboBox={};
GzSetupBahanMakanan.form.DataStore={};
GzSetupBahanMakanan.form.Record={};
GzSetupBahanMakanan.form.Form={};
GzSetupBahanMakanan.form.Grid={};
GzSetupBahanMakanan.form.Panel={};
GzSetupBahanMakanan.form.TextField={};
GzSetupBahanMakanan.form.Button={};

GzSetupBahanMakanan.form.ArrayStore.bahan= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_bahan', 'nama_bahan', 'kd_satuan'],
	data: []
});

CurrentPage.page = dataGrid_viGzSetupBahanMakanan(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

getSatuanComboGzSetupBahanMakanan();

function dataGrid_viGzSetupBahanMakanan(mod_id_viGzSetupBahanMakanan){	
 	// Field kiriman dari Project Net.
    var FieldMaster_viGzSetupBahanMakanan = 
	[
		'kd_bahan', 'nama_bahan', 'kd_satuan' 
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viGzSetupBahanMakanan = new WebApp.DataStore
	({
        fields: FieldMaster_viGzSetupBahanMakanan
    });
    dataGriGzSetupBahanMakanan();
    // Grid gizi Perencanaan # --------------
	GridDataView_viGzSetupBahanMakanan = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viGzSetupBahanMakanan,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:true,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viGzSetupBahanMakanan = undefined;
							rowSelected_viGzSetupBahanMakanan = dataSource_viGzSetupBahanMakanan.getAt(row);
							CurrentData_viGzSetupBahanMakanan
							CurrentData_viGzSetupBahanMakanan.row = row;
							CurrentData_viGzSetupBahanMakanan.data = rowSelected_viGzSetupBahanMakanan.data;
							//DataInitGzSetupBahanMakanan(rowSelected_viGzSetupBahanMakanan.data);
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viGzSetupBahanMakanan = dataSource_viGzSetupBahanMakanan.getAt(ridx);
					if (rowSelected_viGzSetupBahanMakanan != undefined)
					{
						DataInitGzSetupBahanMakanan(rowSelected_viGzSetupBahanMakanan.data);
						//setLookUp_viGzSetupBahanMakanan(rowSelected_viGzSetupBahanMakanan.data);
					}
					else
					{
						//setLookUp_viGzSetupBahanMakanan();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Setup BahanGizi
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header: 'Kode Bahan',
						dataIndex: 'kd_bahan',
						hideable:false,
						menuDisabled: true,
						width: 40
						
					},
					//-------------- ## --------------
					{
						header: 'Nama Bahan',
						dataIndex: 'nama_bahan',
						hideable:false,
						menuDisabled: true,
						width: 90
					},
					//-------------- ## --------------
					{
						header: 'Satuan',
						dataIndex: 'satuan',
						hideable:false,
						menuDisabled: true,
						width: 90
					},
					//-------------- ## --------------
					{
						header: 'kd_satuan',
						dataIndex: 'kd_satuan',
						hideable:false,
						menuDisabled: true,
						hidden:true,
						width: 90
					}
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viGzSetupBahanMakanan',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Bahan',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viGzSetupBahanMakanan',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viGzSetupBahanMakanan != undefined)
							{
								DataInitGzSetupBahanMakanan(rowSelected_viGzSetupBahanMakanan.data);
							}
							
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viGzSetupBahanMakanan, selectCount_viGzSetupBahanMakanan, dataSource_viGzSetupBahanMakanan),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)	
	var PanelTabGzSetupBahanMakanan = new Ext.FormPanel({
        labelAlign: 'top',
		layout:'form',
		border: false,  
		autoHeight:true,
        title: '',
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [PanelInputBahanGizi()]
		
	})
	// Kriteria filter pada Grid # --------------
    var FrmData_viGzSetupBahanMakanan = new Ext.Panel
    (
		{
			title: NamaForm_viGzSetupBahanMakanan,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viGzSetupBahanMakanan,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ PanelTabGzSetupBahanMakanan,
					GridDataView_viGzSetupBahanMakanan],
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAddBahan_viGzSetupBahanMakanan',
						handler: function(){
							AddNewGzSetupBahanMakanan();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viGzSetupBahanMakanan',
						handler: function()
						{
							loadMask.show();
							dataSave_viGzSetupBahanMakanan();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viGzSetupBahanMakanan',
						handler: function()
						{
							Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
								if (button == 'yes'){
									loadMask.show();
									dataDelete_viGzSetupBahanMakanan();
								}
							});
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_viGzSetupBahanMakanan',
						handler: function()
						{
							dataSource_viGzSetupBahanMakanan.removeAll();
							dataGriGzSetupBahanMakanan();
						}
					},
					{
						xtype: 'tbseparator'
					}
					
				]
			}
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_viGzSetupBahanMakanan;
    //-------------- # End form filter # --------------
}

function PanelInputBahanGizi(rowdata){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 90,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'Kode bahan'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 0,
								xtype: 'textfield',
								id: 'txtKdBahan_GzSetupBahanMakanan',
								name: 'txtKdBahan_GzSetupBahanMakanan',
								width: 100,
								allowBlank: false,
								maxLength:3,
								readOnly:true,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									},
									blur: function(a){
										if(a.getValue().length > 4){
											ShowPesanWarningGzSetupBahanMakanan('Kode bahan tidak boleh lebih dari 4 huruf', 'Warning');
										}
									}
								}
							},
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'Nama bahan'
							},
							{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							GzSetupBahanMakanan.vars.namabahan=new Nci.form.Combobox.autoComplete({
								x: 130,
								y: 30,
								tabIndex:2,
								store	: GzSetupBahanMakanan.form.ArrayStore.bahan,
								select	: function(a,b,c){
									
									Ext.getCmp('txtKdBahan_GzSetupBahanMakanan').setValue(b.data.kd_bahan);
									
									GridDataView_viGzSetupBahanMakanan.getView().refresh();
									
								},
								onShowList:function(a){
									dataSource_viGzSetupBahanMakanan.removeAll();
									
									var recs=[],
									recType=dataSource_viGzSetupBahanMakanan.recordType;
										
									for(var i=0; i<a.length; i++){
										recs.push(new recType(a[i]));
									}
									dataSource_viGzSetupBahanMakanan.add(recs);
									
								},
								insert	: function(o){
									return {
										kd_bahan		: o.kd_bahan,
										nama_bahan 		: o.nama_bahan,
										kd_satuan 		: o.kd_satuan,
										satuan 			: o.satuan,
										text			:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_bahan+'</td><td width="200">'+o.nama_bahan+'</td></tr></table>'
									}
								},
								url		: baseURL + "index.php/gizi/functionSetupBahanMakanan/getBahanMakananGrid",
								valueField: 'nama_bahan',
								displayField: 'text',
								listWidth: 280
							}),
							{
								x: 10,
								y: 60,
								xtype: 'label',
								text: 'Satuan'
							},
							{
								x: 120,
								y: 60,
								xtype: 'label',
								text: ':'
							},
							Q().dropdown({
								id:'cmbSatuan_viGzSetupBahanMakanan',
								x:130,
								y:60,
								width:150
							})
							
							
						]
					}
				]
			}
		]		
	};
        return items;
}
//------------------end---------------------------------------------------------

function getSatuanComboGzSetupBahanMakanan(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionSetupBahanMakanan/getSatuanCombo",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorGzSetupBahanMakanan('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					Q(Ext.getCmp('cmbSatuan_viGzSetupBahanMakanan')).reset();
					Q(Ext.getCmp('cmbSatuan_viGzSetupBahanMakanan')).add(cst.listData);
					console.log(cst.listData);
				}
				else 
				{
					ShowPesanErrorGzSetupBahanMakanan('Gagal membaca data satuan bahan', 'Error');
				};
			}
		}
		
	)
	
}

function dataGriGzSetupBahanMakanan(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionSetupBahanMakanan/getBahanMakananGrid",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorGzSetupBahanMakanan('Error, membaca data! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dataSource_viGzSetupBahanMakanan.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dataSource_viGzSetupBahanMakanan.add(recs);
					
					
					
					GridDataView_viGzSetupBahanMakanan.getView().refresh();
				}
				else 
				{
					ShowPesanErrorGzSetupBahanMakanan('Gagal membaca data satuan bahan', 'Error');
				};
			}
		}
		
	)
	
}

function dataSave_viGzSetupBahanMakanan(){
	if (ValidasiSaveGzSetupBahanMakanan(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/gizi/functionSetupBahanMakanan/save",
				params: getParamSaveGzSetupBahanMakanan(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorGzSetupBahanMakanan('Error, simpan data! Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoGzSetupBahanMakanan('Berhasil menyimpan data ini','Information');
						Ext.getCmp('txtKdBahan_GzSetupBahanMakanan').setValue(cst.kdbahan);
						dataSource_viGzSetupBahanMakanan.removeAll();
						dataGriGzSetupBahanMakanan();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorGzSetupBahanMakanan('Gagal menyimpan data ini', 'Error');
						dataSource_viGzSetupBahanMakanan.removeAll();
						dataGriGzSetupBahanMakanan();
					};
				}
			}
			
		)
	}
}

function dataDelete_viGzSetupBahanMakanan(){
	if (ValidasiSaveGzSetupBahanMakanan(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/gizi/functionSetupBahanMakanan/delete",
				params: getParamDeleteGzSetupBahanMakanan(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorGzSetupBahanMakanan('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoGzSetupBahanMakanan('Berhasil menghapus data ini','Information');
						AddNewGzSetupBahanMakanan()
						dataSource_viGzSetupBahanMakanan.removeAll();
						dataGriGzSetupBahanMakanan();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorGzSetupBahanMakanan('Gagal menghapus data ini', 'Error');
						dataSource_viGzSetupBahanMakanan.removeAll();
						dataGriGzSetupBahanMakanan();
					};
				}
			}
			
		)
	}
}

function AddNewGzSetupBahanMakanan(){
	Ext.getCmp('txtKdBahan_GzSetupBahanMakanan').setValue('');
	GzSetupBahanMakanan.vars.namabahan.setValue('');
	Ext.getCmp('cmbSatuan_viGzSetupBahanMakanan').setValue('');
};

function DataInitGzSetupBahanMakanan(rowdata){
	Ext.getCmp('txtKdBahan_GzSetupBahanMakanan').setValue(rowdata.kd_bahan);
	GzSetupBahanMakanan.vars.namabahan.setValue(rowdata.nama_bahan);
	Ext.getCmp('cmbSatuan_viGzSetupBahanMakanan').setValue(rowdata.kd_satuan);
};

function getParamSaveGzSetupBahanMakanan(){
	var	params =
	{
		KdBahan:Ext.getCmp('txtKdBahan_GzSetupBahanMakanan').getValue(),
		NamaBahan:GzSetupBahanMakanan.vars.namabahan.getValue(),
		KdSatuan:Ext.getCmp('cmbSatuan_viGzSetupBahanMakanan').getValue(),
	}
   
    return params
};

function getParamDeleteGzSetupBahanMakanan(){
	var	params =
	{
		KdBahan:Ext.getCmp('txtKdBahan_GzSetupBahanMakanan').getValue()
	}
   
    return params
};

function ValidasiSaveGzSetupBahanMakanan(modul,mBolHapus){
	var x = 1;
	if(GzSetupBahanMakanan.vars.namabahan.getValue() === '' || Ext.getCmp('cmbSatuan_viGzSetupBahanMakanan').getValue() ===''){
		if(GzSetupBahanMakanan.vars.namabahan.getValue() === ''){
			loadMask.hide();
			ShowPesanWarningGzSetupBahanMakanan('Nama bahan masih kosong', 'Warning');
			x = 0;
		} else{
			loadMask.hide();
			ShowPesanWarningGzSetupBahanMakanan('Satuan masih kosong', 'Warning');
			x = 0;
		}
		
	} 
	return x;
};



function ShowPesanWarningGzSetupBahanMakanan(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorGzSetupBahanMakanan(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoGzSetupBahanMakanan(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};