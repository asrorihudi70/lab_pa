var dataSource_viGzSetupSatuanBahan;
var selectCount_viGzSetupSatuanBahan=50;
var NamaForm_viGzSetupSatuanBahan="Setup Satuan Bahan";
var mod_name_viGzSetupSatuanBahan="Setup Satuan Bahan";
var now_viGzSetupSatuanBahan= new Date();
var rowSelected_viGzSetupSatuanBahan;
var setLookUps_viGzSetupSatuanBahan;
var tanggal = now_viGzSetupSatuanBahan.format("d/M/Y");
var jam = now_viGzSetupSatuanBahan.format("H/i/s");
var tmpkriteria;
var DataGridJenisObat;
var GridDataView_viGzSetupSatuanBahan;
var kodesetupsatuanbahan='';

var CurrentData_viGzSetupSatuanBahan =
{
	data: Object,
	details: Array,
	row: 0
};

var GzSetupSatuanBahan={};
GzSetupSatuanBahan.form={};
GzSetupSatuanBahan.func={};
GzSetupSatuanBahan.vars={};
GzSetupSatuanBahan.func.parent=GzSetupSatuanBahan;
GzSetupSatuanBahan.form.ArrayStore={};
GzSetupSatuanBahan.form.ComboBox={};
GzSetupSatuanBahan.form.DataStore={};
GzSetupSatuanBahan.form.Record={};
GzSetupSatuanBahan.form.Form={};
GzSetupSatuanBahan.form.Grid={};
GzSetupSatuanBahan.form.Panel={};
GzSetupSatuanBahan.form.TextField={};
GzSetupSatuanBahan.form.Button={};

GzSetupSatuanBahan.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_unit_far', 'nm_unit_far', 'nomor_ro_unit', 'nomorawal','nomor_out_milik'],
	data: []
});

CurrentPage.page = dataGrid_viGzSetupSatuanBahan(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dataGrid_viGzSetupSatuanBahan(mod_id_viGzSetupSatuanBahan){	
 	// Field kiriman dari Project Net.
    var FieldMaster_viGzSetupSatuanBahan = 
	[
		'kd_unit_far', 'nm_unit_far', 'nomor_ro_unit', 'nomorawal','nomor_out_milik' 
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viGzSetupSatuanBahan = new WebApp.DataStore
	({
        fields: FieldMaster_viGzSetupSatuanBahan
    });
    dataGriGzSetupSatuanBahan();
    // Grid gizi Perencanaan # --------------
	GridDataView_viGzSetupSatuanBahan = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viGzSetupSatuanBahan,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:true,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viGzSetupSatuanBahan = undefined;
							rowSelected_viGzSetupSatuanBahan = dataSource_viGzSetupSatuanBahan.getAt(row);
							CurrentData_viGzSetupSatuanBahan
							CurrentData_viGzSetupSatuanBahan.row = row;
							CurrentData_viGzSetupSatuanBahan.data = rowSelected_viGzSetupSatuanBahan.data;
							kodesetupsatuanbahan=rowSelected_viGzSetupSatuanBahan.data.kd_satuan;
							//alert(rowSelected_viGzSetupSatuanBahan);
							//DataInitGzSetupSatuanBahan(rowSelected_viGzSetupSatuanBahan.data);
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					kodesetupsatuanbahan=rowSelected_viGzSetupSatuanBahan.data.kd_satuan;
					rowSelected_viGzSetupSatuanBahan = dataSource_viGzSetupSatuanBahan.getAt(ridx);
					if (rowSelected_viGzSetupSatuanBahan != undefined)
					{
						DataInitGzSetupSatuanBahan(rowSelected_viGzSetupSatuanBahan.data);
						//setLookUp_viGzSetupSatuanBahan(rowSelected_viGzSetupSatuanBahan.data);
					}
					else
					{
						//setLookUp_viGzSetupSatuanBahan();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Setup Unit
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header: 'Kode Satuan',
						dataIndex: 'kd_satuan',
						hideable:false,
						menuDisabled: true,
						width: 40
						
					},
					//-------------- ## --------------
					{
						header: 'Satuan',
						dataIndex: 'satuan',
						hideable:false,
						menuDisabled: true,
						width: 90
					}
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viGzSetupSatuanBahan',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Satuan',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viGzSetupSatuanBahan',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viGzSetupSatuanBahan != undefined)
							{
								DataInitGzSetupSatuanBahan(rowSelected_viGzSetupSatuanBahan.data);
							}
							
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viGzSetupSatuanBahan, selectCount_viGzSetupSatuanBahan, dataSource_viGzSetupSatuanBahan),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)	
	var PanelTabGzSetupSatuanBahan = new Ext.FormPanel({
        labelAlign: 'top',
		layout:'form',
		border: false,  
		autoHeight:true,
        title: '',
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [PanelInputUnit()]
		
	})
	// Kriteria filter pada Grid # --------------
    var FrmData_viGzSetupSatuanBahan = new Ext.Panel
    (
		{
			title: NamaForm_viGzSetupSatuanBahan,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viGzSetupSatuanBahan,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ PanelTabGzSetupSatuanBahan,
					GridDataView_viGzSetupSatuanBahan],
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAddSatuan_viGzSetupSatuanBahan',
						handler: function(){
							AddNewGzSetupSatuanBahan();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viGzSetupSatuanBahan',
						handler: function()
						{
							loadMask.show();
							dataSave_viGzSetupSatuanBahan();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viGzSetupSatuanBahan',
						handler: function()
						{
							if (kodesetupsatuanbahan==='')
							{
								ShowPesanErrorGzSetupSatuanBahan('Tidak ada data yang dipilih', 'Error');
							}
							else
							{
								Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
									if (button == 'yes'){
										loadMask.show();
										dataDelete_viGzSetupSatuanBahan();
									}
								});
							}
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_viGzSetupSatuanBahan',
						handler: function()
						{
							kodesetupsatuanbahan='';
							dataSource_viGzSetupSatuanBahan.removeAll();
							dataGriGzSetupSatuanBahan();
						}
					},
					{
						xtype: 'tbseparator'
					}
					
				]
			}
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_viGzSetupSatuanBahan;
    //-------------- # End form filter # --------------
}

function PanelInputUnit(rowdata){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 55,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'Kode satuan'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 0,
								xtype: 'textfield',
								id: 'txtKdSatuan_GzSetupSatuanBahan',
								name: 'txtKdSatuan_GzSetupSatuanBahan',
								width: 100,
								allowBlank: false,
								maxLength:3,
								readOnly:true,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									},
									blur: function(a){
										if(a.getValue().length > 3){
											ShowPesanWarningGzSetupSatuanBahan('Kode unit tidak boleh lebih dari 3 huruf', 'Warning');
										}
									}
								}
							},
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'Satuan'
							},
							{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							GzSetupSatuanBahan.vars.satuan=new Nci.form.Combobox.autoComplete({
								x: 130,
								y: 30,
								tabIndex:2,
								store	: GzSetupSatuanBahan.form.ArrayStore.a,
								select	: function(a,b,c){
									
									Ext.getCmp('txtKdSatuan_GzSetupSatuanBahan').setValue(b.data.kd_satuan);
									
									GridDataView_viGzSetupSatuanBahan.getView().refresh();
									
								},
								onShowList:function(a){
									dataSource_viGzSetupSatuanBahan.removeAll();
									
									var recs=[],
									recType=dataSource_viGzSetupSatuanBahan.recordType;
										
									for(var i=0; i<a.length; i++){
										recs.push(new recType(a[i]));
									}
									dataSource_viGzSetupSatuanBahan.add(recs);
									
								},
								insert	: function(o){
									return {
										kd_satuan   : o.kd_satuan,
										satuan 		: o.satuan,
										text			:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_satuan+'</td><td width="200">'+o.satuan+'</td></tr></table>'
									}
								},
								url		: baseURL + "index.php/gizi/functionSetupSatuan/getSatuanGrid",
								valueField: 'satuan',
								displayField: 'text',
								listWidth: 280
							})
							
							
						]
					}
				]
			}
		]		
	};
        return items;
}
//------------------end---------------------------------------------------------

function dataGriGzSetupSatuanBahan(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gizi/functionSetupSatuan/getSatuanGrid",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorGzSetupSatuanBahan('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dataSource_viGzSetupSatuanBahan.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dataSource_viGzSetupSatuanBahan.add(recs);
					
					
					
					GridDataView_viGzSetupSatuanBahan.getView().refresh();
				}
				else 
				{
					ShowPesanErrorGzSetupSatuanBahan('Gagal membaca data satuan bahan', 'Error');
				};
			}
		}
		
	)
	
}

function dataSave_viGzSetupSatuanBahan(){
	if (ValidasiSaveGzSetupSatuanBahan(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/gizi/functionSetupSatuan/save",
				params: getParamSaveGzSetupSatuanBahan(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorGzSetupSatuanBahan('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoGzSetupSatuanBahan('Berhasil menyimpan data ini','Information');
						dataSource_viGzSetupSatuanBahan.removeAll();
						dataGriGzSetupSatuanBahan();
						Ext.getCmp('txtKdSatuan_GzSetupSatuanBahan').setValue(cst.kodesatuan);
						kodesetupsatuanbahan='';
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorGzSetupSatuanBahan('Gagal menyimpan data ini', 'Error');
						dataSource_viGzSetupSatuanBahan.removeAll();
						dataGriGzSetupSatuanBahan();
					};
				}
			}
			
		)
	}
}

function dataDelete_viGzSetupSatuanBahan(){
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/gizi/functionSetupSatuan/delete",
				params: getParamDeleteGzSetupSatuanBahan(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorGzSetupSatuanBahan('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoGzSetupSatuanBahan('Berhasil menghapus data ini','Information');
						AddNewGzSetupSatuanBahan()
						dataSource_viGzSetupSatuanBahan.removeAll();
						dataGriGzSetupSatuanBahan();
						kodesetupsatuanbahan='';
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorGzSetupSatuanBahan('Gagal menghapus data ini', 'Error');
						dataSource_viGzSetupSatuanBahan.removeAll();
						dataGriGzSetupSatuanBahan();
					};
				}
			}
			
		)
	
}

function AddNewGzSetupSatuanBahan(){
	Ext.getCmp('txtKdSatuan_GzSetupSatuanBahan').setValue('');
	GzSetupSatuanBahan.vars.satuan.setValue('');
};

function DataInitGzSetupSatuanBahan(rowdata){
	Ext.getCmp('txtKdSatuan_GzSetupSatuanBahan').setValue(rowdata.kd_satuan);
	GzSetupSatuanBahan.vars.satuan.setValue(rowdata.satuan);
};

function getParamSaveGzSetupSatuanBahan(){
	var	params =
	{
		KdSatuan:Ext.getCmp('txtKdSatuan_GzSetupSatuanBahan').getValue(),
		NamaSatuan:GzSetupSatuanBahan.vars.satuan.getValue()
	}
   
    return params
};

function getParamDeleteGzSetupSatuanBahan(){
	var	params =
	{
		KdSatuan:kodesetupsatuanbahan
	}
   
    return params
};

function ValidasiSaveGzSetupSatuanBahan(modul,mBolHapus){
	var x = 1;
	if(GzSetupSatuanBahan.vars.satuan.getValue() === '' /*|| Ext.getCmp('txtKdSatuan_GzSetupSatuanBahan').getValue() ===''*/){
		if(GzSetupSatuanBahan.vars.satuan.getValue() === ''){
			loadMask.hide();
			ShowPesanWarningGzSetupSatuanBahan('Nama unit masih kosong', 'Warning');
			x = 0;
		} else{
			loadMask.hide();
			ShowPesanWarningGzSetupSatuanBahan('Kode unit masih kosong', 'Warning');
			x = 0;
		}
		
	} 
	if(Ext.getCmp('txtKdSatuan_GzSetupSatuanBahan').getValue().length > 3){
		ShowPesanWarningGzSetupSatuanBahan('Kode unit tidak boleh lebih dari 3 huruf', 'Warning');
		x = 0;
	}
	return x;
};



function ShowPesanWarningGzSetupSatuanBahan(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorGzSetupSatuanBahan(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoGzSetupSatuanBahan(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};