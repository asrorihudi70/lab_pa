var GFInfoStok={
	vars:{
		appTitle:'Informasi Stok',
		nowDate: new Date()
	},
	form:{
		ArrayStore:{
			main:new Ext.data.ArrayStore({id: 0,fields: ['kd_prd','nama_obat','jml_stok_apt','satuan'],data:[]})
		},
		Grid:{
			main:null
		},
		TextField:{
			no:null
		}
	},
	refresh: function(){
		var $this=this;
		loadMask.show();
		var a=[];
		a.push({name: 'kd_prd',value:$this.form.TextField.no.getValue()});
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			url:baseURL + "index.php/gudang_farmasi/functionGFInfoStok/initList",
			data:a,
			success: function(r){
				loadMask.hide();
				if(r.processResult=='SUCCESS'){
					$this.form.ArrayStore.main.loadData([],false);
					for(var i=0,iLen=r.listData.length; i<iLen ;i++){
						var records=[];
						records.push(new $this.form.ArrayStore.main.recordType());
						$this.form.ArrayStore.main.add(records);
						$this.form.ArrayStore.main.data.items[i].data.kd_prd=r.listData[i].kd_prd;
						$this.form.ArrayStore.main.data.items[i].data.nama_obat=r.listData[i].nama_obat;
						$this.form.ArrayStore.main.data.items[i].data.satuan=r.listData[i].satuan;
						$this.form.ArrayStore.main.data.items[i].data.jml_stok_apt=r.listData[i].jml_stok_apt;
					}
					$this.form.Grid.main.getView().refresh();
				}else{
					Ext.Msg.alert('Gagal',r.processMessage);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
	},
	main: function(modId){
		var $this=this;
		$this.form.Grid.main= new Ext.grid.EditorGridPanel({
			xtype: 'editorgrid',
			title: '',
			store: $this.form.ArrayStore.main,
			autoScroll: true,
			flex: 1,
			columnLines: true,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			colModel: new Ext.grid.ColumnModel([
				new Ext.grid.RowNumberer(),
				{
					header		: 'Kode Obat',
					width		: 100,
					sortable	: true,
					dataIndex	: 'kd_prd'
				},{
					header: 'Nama Obat',
					dataIndex: 'nama_obat',
					sortable: true,
					width: 300
				},{
					header:'Stok',
					align:'right',
					dataIndex: 'jml_stok_apt',						
					width: 100,
					sortable: true
				},{
					header: 'Satuan',
					dataIndex: 'satuan',
					sortable: true,
					width: 100
				}
			])
		});
		
    	return new Ext.FormPanel({
			title: $this.vars.appTitle,
			iconCls: 'Studi_Lanjut',
			id: modId,
			region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
			}, 
			closable: true,        
			border: false,  
			items: [$this.form.Grid.main],
			tbar:[
				{
		            xtype: 'buttongroup',
		            columns: 11,
		            defaults: {
		                scale: 'small'
		        	},
		        	frame: false,
		            items:[
			            { 
							xtype: 'tbtext', 
							text: 'Nama Obat : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},$this.form.TextField.no= new Ext.form.TextField({
							name:'kd_prd',
							width: 100,
							height: 25,
							listeners:{ 
								'specialkey' : function(){
									if (Ext.EventObject.getKey() === 13){
										$this.refresh();
									} 						
								}
							}
						}),{	 
							xtype: 'tbspacer',
							width: 10,
							height: 25
						},{
							xtype: 'button',
							text: 'Cari',
							iconCls: 'refresh',
							tooltip: 'Cari',
							style:{paddingLeft:'30px'},
							width:150,
							handler: function(){					
								$this.refresh();	
							}                        
						}
					]
				}
			]
	 	});
		return $this.form.Panel.search;
	},
	init: function(){
		var $this=this;
		CurrentPage.page = $this.main(CurrentPage.id);
		mainPage.add(CurrentPage.page);
		mainPage.setActiveTab(CurrentPage.id);
		$this.refresh();
	}
};
GFInfoStok.init();