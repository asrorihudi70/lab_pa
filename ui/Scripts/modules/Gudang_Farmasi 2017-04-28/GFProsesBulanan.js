var GFProsesBulanan={
	vars:{
		appTitle:'Proses Bulanan',
		nowDate: new Date(),
		progress:0,
		interval:null
	},
	ArrayStore:{
		main:new Ext.data.ArrayStore({id: 0,fields: ['month','stat'],data:[
			['Januari','t'],
			['Februari','t'],
			['Maret','t'],
			['April','t'],
			['Mei','t'],
			['Juni','t'],
			['July','t'],
			['Agustus','t'],
			['September','t'],
			['Oktober','t'],
			['November','t'],
			['Desember','t']
		]})
	},
	Dropdown:{
		tahun:null,
		bulan:null
	},
	Grid:{
		main:null
	},
	NumberField:{
		year:null
	},
	ProgressBar:{
		process:null
	},
	TextField:{
		no:null
	},
	doInterval:function(){
		var $this=this;
		$this.vars.progress=0;
		var progres=1/30000;
		var a=0;
		$this.vars.interval=setInterval(function(){
			$this.vars.progress+=progres;
			a+=1;
			var res=Math.round($this.vars.progress*100);
			if(res>100)res=100;
			$this.ProgressBar.process.updateProgress($this.vars.progress,res+' %');
			if(a>30000){
				$this.ProgressBar.process.hide();
				clearInterval($this.vars.interval);
			}
		},1);
	},
	doProcess:function(){
		var $this=this;
		$this.ProgressBar.process.updateProgress(0,'0 %');
		$this.ProgressBar.process.show();
		$this.doInterval();
	 	$.ajax({
			url:baseURL + "index.php/gudang_farmasi/functionGFProsesBulanan/doProcess",
			dataType:'JSON',
			type: 'POST',
			cache: false,
			data:{'month':$this.Dropdown.bulan.getValue(),'year':$this.NumberField.year.getValue()},
			success: function(r){
				clearInterval($this.vars.interval);
				if(r.result=='SUCCESS'){
					$this.getPeriode();
					
					var sisa=1-$this.vars.progress;
					var progres=sisa/100;
					var a=0;
					var inter=setInterval(function(){
						$this.vars.progress+=progres;
						a+=1;
						var res=Math.round($this.vars.progress*100);
						if(res>100)res=100;
						$this.ProgressBar.process.updateProgress($this.vars.progress,res+' %');
						if(a>100){
							clearInterval(inter);
							$this.ProgressBar.process.hide();
							Ext.Msg.alert('Sukses',r.message);
						}
					},1);
					
				}else if(r.result=='ERROR'){
					$this.ProgressBar.process.hide();
					$this.ProgressBar.process.updateProgress(0,'Error');
					Ext.Msg.alert('Gagal',r.message);
				}
			},
			xhr: function () {
		        var xhr = new window.XMLHttpRequest();
		        xhr.upload.addEventListener("progress", function (evt) {
		            if (evt.lengthComputable) {
		                var percentComplete = evt.loaded / evt.total;
		                console.log(percentComplete);
		            }
		        }, false);
		        console.log(xhr);
		        xhr.addEventListener("progress", function (evt) {
		            if (evt.lengthComputable) {
		                var percentComplete = evt.loaded / evt.total;
		                console.log(percentComplete);
		            }
		        }, false);
		        return xhr;
		    },
			error: function(jqXHR, exception) {
				$this.ProgressBar.process.hide();
				clearInterval($this.vars.interval);
				$this.ProgressBar.process.updateProgress(0,'Error');
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
	},
	getPeriode:function(){
		var $this=this;
	 	$.ajax({
			url:baseURL + "index.php/gudang_farmasi/functionGFProsesBulanan/getPeriode",
			dataType:'JSON',
			type: 'POST',
			data:{'year':$this.Dropdown.tahun.getValue()},
			success: function(r){
				if(r.result=='SUCCESS'){
					$this.ArrayStore.main.loadData([],false);
					for(var i=0; i<r.data.length ; i++){
						$this.ArrayStore.main.add(new $this.ArrayStore.main.recordType(r.data[i]));
					}
				}else if(r.result=='ERROR'){
					var a=[
						{month:'Januari',stat:'0'},
						{month:'Februari',stat:'0'},
						{month:'Maret',stat:'0'},
						{month:'April',stat:'0'},
						{month:'Mei',stat:'0'},
						{month:'Juni',stat:'0'},
						{month:'July',stat:'0'},
						{month:'Agustus',stat:'0'},
						{month:'September',stat:'0'},
						{month:'Oktober',stat:'0'},
						{month:'November',stat:'0'},
						{month:'Desember',stat:'0'}
					];
					$this.ArrayStore.main.loadData([],false);
					for(var i=0; i<a.length ; i++){
						$this.ArrayStore.main.add(new $this.ArrayStore.main.recordType(a[i]));
					}
				}
			},
			error: function(jqXHR, exception) {
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
	},
	main: function(modId){
		var $this=this;
		var listYear=[];
		listYear.push({id:new Date().getFullYear()+1});
		listYear.push({id:new Date().getFullYear()});
		var year=new Date().getFullYear();
		for(var i=0 ; i<10 ; i++){
			year-=1;
			listYear.push({id:year});
		}
		$this.Grid.main= new Ext.grid.EditorGridPanel({
			store: $this.ArrayStore.main,
			height:320,
			width: 200,
			columnLines: true,
			border:false,
			tbar:[
				'Tahun : ',
				$this.Dropdown.tahun=Q().dropdown({
					displayField  : 'id',
					data:listYear,value:new Date().getFullYear(),
					select:function(){
						$this.getPeriode();
					}
				})
			],
			colModel: new Ext.grid.ColumnModel([
				{
					header		: 'Bulan',
					width		: 100,
					sortable	: true,
					dataIndex	: 'month'
				},{
					header: 'Status',
					sortable: true,
					width: 100,
					dataIndex:'stat',
					renderer	: function(value, metaData, record, rowIndex, colIndex, store){
						 switch (value){
							 case '1':
								 metaData.css = 'StatusHijau'; 
								 break;
							 case '0':
								 metaData.css = 'StatusMerah';
								 break;
						 }
						 return '';
					}
				}
			]),
			viewConfig: {
				forceFit: true
			}
		});
    	return new Ext.FormPanel({
			title: $this.vars.appTitle,
			iconCls: 'Studi_Lanjut',
			id: modId,
			bodyStyle:'padding: 5px',
			region: 'center',
			layout: {
				type:'hbox'
			}, 
			closable: true,        
			border: false,  
			items: [
				new Ext.Panel({
					title:'Informasi Tutup Bulan',
					items:[
						$this.Grid.main
					]
				}),
				new Ext.Panel({
					title:'Proses Bulanan',
					style:{'margin-left':'5px'},
					bodyStyle:'padding: 5px;font-size: 11px;',
					items:[
						Q().input({
							label:'Bulan',
							width: 250,
							items:[
								$this.Dropdown.bulan=Q().dropdown({
									data:[
										{id:1,text:'Januari'},
										{id:2,text:'Februari'},
										{id:3,text:'Maret'},
										{id:4,text:'April'},
										{id:5,text:'Mei'},
										{id:6,text:'Juni'},
										{id:7,text:'Juli'},
										{id:8,text:'Agustus'},
										{id:9,text:'September'},
										{id:10,text:'Oktober'},
										{id:11,text:'November'},
										{id:12,text:'Desember'}
									],
									width: 150,
									value:(new Date().getMonth()+1)
								})
							]
						}),
						Q().input({
							label:'Tahun',
							items:[
								$this.NumberField.year=new Ext.form.NumberField({
									value:new Date().getFullYear(),
									style:{'text-align':'right'},
									width: 50
								})
							]
						})
					],
					bbar:[
						$this.ProgressBar.process=new Ext.ProgressBar({
							width: 180,
							text:'0 %',
							hidden:true,
							value:0
						}),
						'->',
						'-',
						new Ext.Button({
							text:'Proses',
							iconCls: 'gantidok',
							handler:function(){
								$this.doProcess();
							}
						})
					]
				})
			]
	 	});
	},
	init: function(){
		var $this=this;
		CurrentPage.page = $this.main(CurrentPage.id);
		mainPage.add(CurrentPage.page);
		mainPage.setActiveTab(CurrentPage.id);
		$this.getPeriode();
	}
};
GFProsesBulanan.init();