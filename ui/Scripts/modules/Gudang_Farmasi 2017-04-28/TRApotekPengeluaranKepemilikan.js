// Data Source ExtJS # --------------

/**
*	Nama File 		: TRApotekPengeluaranKepemilikan.js
*	Menu 			: 
*	Model id 		: 
*	Keterangan 		: Retur PBF 
*	Di buat tanggal : 21 Agustus 2014
*	Oleh 			: SDY_RI
*/

// Deklarasi Variabel pada Apotek Retur PBF # --------------
var dataSource_viApotekPengeluaranKepemilikan;
var selectCount_viApotekPengeluaranKepemilikan=50;
var NamaForm_viApotekPengeluaranKepemilikan="Pengeluaran Kepemilikan";
var mod_name_viApotekPengeluaranKepemilikan="viApotekPengeluaranKepemilikan";
var now_viApotekPengeluaranKepemilikan= new Date();
var addNew_viApotekPengeluaranKepemilikan;
var rowSelected_viApotekPengeluaranKepemilikan;
var setLookUps_viApotekPengeluaranKepemilikan;
var mNoKunjungan_viApotekPengeluaranKepemilikan='';

var CurrentData_viApotekPengeluaranKepemilikan =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viApotekPengeluaranKepemilikan(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

// End Deklarasi Variabel pada Apotek Perencanaan # --------------

// Start Project Apotek Perencanaan # --------------

// --------------------------------------- # Start Function # ---------------------------------------
// ## Silahkan tempatkan semua fungsi eksekusi disini

// --------------------------------------- # End Function # ---------------------------------------

// --------------------------------------- # Start Form # ---------------------------------------

/**
*	Function : dataGrid_viApotekPengeluaranKepemilikan
*	
*	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
*/

function dataGrid_viApotekPengeluaranKepemilikan(mod_id_viApotekPengeluaranKepemilikan)
{	
    // Field kiriman dari Project Net.
    var FieldMaster_viApotekPengeluaranKepemilikan = 
	[
		 'NO_KUNJUNGAN', 'KD_KELOMPOK', 'KD_UNIT', 'KD_DOKTER', 'KD_CUSTOMER', 'KD_PASIEN', 
		 'TGL_KUNJUNGAN','JAM_KUNJUNGAN', 'TINGGI_BADAN', 'BERAT_BADAN', 'TEKANAN_DRH', 
		 'NADI','ALERGI', 'KELUHAN', 'RUJUK_RAD', 'RUJUK_LAB', 'TAHAP_PROSES', 'PASIEN_BARU',
		 'NAMA_UNIT','KELOMPOK', 'DOKTER', 'CUSTOMER', 'PS_BARU','KD_PENDIDIKAN','KD_STS_MARITAL', 
		 'KD_AGAMA','KD_PEKERJAAN','NAMA','TEMPAT_LAHIR','TGL_LAHIR','JENIS_KELAMIN','ALAMAT', 
		 'NO_TELP','NO_HP','GOL_DARAH','PENDIDIKAN','STS_MARITAL','AGAMA','PEKERJAAN','JNS_KELAMIN',
		 'TAHUN','BULAN','HARI'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viApotekPengeluaranKepemilikan = new WebApp.DataStore
	({
        fields: FieldMaster_viApotekPengeluaranKepemilikan
    });
    
    // Grid Apotek Perencanaan # --------------
	var GridDataView_viApotekPengeluaranKepemilikan = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viApotekPengeluaranKepemilikan,
			autoScroll: true,
			columnLines: true,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viApotekPengeluaranKepemilikan = undefined;
							rowSelected_viApotekPengeluaranKepemilikan = dataSource_viApotekPengeluaranKepemilikan.getAt(row);
							CurrentData_viApotekPengeluaranKepemilikan
							CurrentData_viApotekPengeluaranKepemilikan.row = row;
							CurrentData_viApotekPengeluaranKepemilikan.data = rowSelected_viApotekPengeluaranKepemilikan.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viApotekPengeluaranKepemilikan = dataSource_viApotekPengeluaranKepemilikan.getAt(ridx);
					if (rowSelected_viApotekPengeluaranKepemilikan != undefined)
					{
						setLookUp_viApotekPengeluaranKepemilikan(rowSelected_viApotekPengeluaranKepemilikan.data);
					}
					else
					{
						setLookUp_viApotekPengeluaranKepemilikan();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Apotek perencanaan
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colNoRetur_viApotekPengeluaranKepemilikan',
						header: 'No. Keluar',
						dataIndex: ' ',
						sortable: true,
						width: 35,
						filter:
						{
							type: 'int'
						}
					},
					//-------------- ## --------------
					{
						id: 'colTglRetur_viApotekPengeluaranKepemilikan',
						header:'Tgl Keluar',
						dataIndex: ' ',						
						width: 20,
						sortable: true,
						// format: 'd/M/Y',
						filter: {},
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.TGL_RETUR);
						}
					},
					//-------------- ## --------------
					{
						id: 'colPVF_viApotekPengeluaranKepemilikan',
						header: 'Kepemilikan',
						dataIndex: ' ',
						sortable: true,
						width: 60,
						filter:
						{}
					}
					//-------------- ## --------------
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viApotekPengeluaranKepemilikan',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viApotekPengeluaranKepemilikan',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viApotekPengeluaranKepemilikan != undefined)
							{
								setLookUp_viApotekPengeluaranKepemilikan(rowSelected_viApotekPengeluaranKepemilikan.data)
							}
							else
							{								
								setLookUp_viApotekPengeluaranKepemilikan();
							}
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			bbar : bbar_paging(mod_name_viApotekPengeluaranKepemilikan, selectCount_viApotekPengeluaranKepemilikan, dataSource_viApotekPengeluaranKepemilikan),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viApotekPengeluaranKepemilikan = new Ext.Panel
    (
		{
			title: NamaForm_viApotekPengeluaranKepemilikan,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viApotekPengeluaranKepemilikan,
			region: 'center',
			layout: 'form', 
			closable: true,        
			border: false,  
			margins: '0 5 5 0',
			items: [GridDataView_viApotekPengeluaranKepemilikan],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viApotekPengeluaranKepemilikan,
		            columns: 11,
		            defaults: {
		                scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
						//-------------- ## --------------
			            { 
							xtype: 'tbtext', 
							text: 'No. Keluar : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},						
						{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_NoKeluar_viApotekPengeluaranKepemilikan',
							emptyText: 'No. Keluar',
							width: 100,
							height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										
									} 						
								}
							}
						},
						{	 
							xtype: 'tbspacer',
							width: 10,
							height: 25
						},						
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Tanggal : ', 
							style:{'text-align':'right'},
							width: 60,
							height: 25
						},
						{
							xtype: 'datefield',
							id: 'txtfilterTglAwal_viApotekPengeluaranKepemilikan',
							value: now_viApotekPengeluaranKepemilikan,
							format: 'd/M/Y',
							width: 100,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viApotekPengeluaranKepemilikan();								
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: ' s.d ', 
							style:{'text-align':'center'},
							width: 30,
							height: 25
						},																								
						//-------------- ## --------------
						{
							xtype: 'datefield',
							id: 'txtfilterTglKeluarAkhir_viApotekPengeluaranKepemilikan',
							value: now_viApotekPengeluaranKepemilikan,
							format: 'd/M/Y',
							width: 100,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viApotekPengeluaranKepemilikan();								
									} 						
								}
							}
						},							
						{	 
							xtype: 'tbspacer',
							width: 10,
							height: 25
						},
						{ 
							xtype: 'tbtext', 
							text: 'Kepemilikan : ', 
							style:{'text-align':'right'},
							width: 65,
							height: 25
						},		
						viCombo_Kepemilikan(150, 'cboKepemilikan_viApotekPengeluaranKepemilikanFilter'),						
						//-------------- ## --------------
						{
							xtype: 'button',
							text: 'Cari',
							iconCls: 'refresh',
							tooltip: 'Cari',
							style:{paddingLeft:'30px'},
							//rowspan: 3,
							width:150,
							id: 'BtnFilterGridDataView_viApotekPengeluaranKepemilikan',
							handler: function() 
							{					
								DfltFilterBtn_viApotekPengeluaranKepemilikan = 1;
								DataRefresh_viApotekPengeluaranKepemilikan(getCriteriaFilterGridDataView_viApotekPengeluaranKepemilikan());
							}                        
						},
						//-------------- ## --------------
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viApotekPengeluaranKepemilikan;
    //-------------- # End form filter # --------------
}
// End Function dataGrid_viApotekPengeluaranKepemilikan # --------------

/**
*	Function : setLookUp_viApotekPengeluaranKepemilikan
*	
*	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
*/

function setLookUp_viApotekPengeluaranKepemilikan(rowdata)
{
    var lebar = 985;
    setLookUps_viApotekPengeluaranKepemilikan = new Ext.Window
    (
    {
        id: 'SetLookUps_viApotekPengeluaranKepemilikan',
		name: 'SetLookUps_viApotekPengeluaranKepemilikan',
        title: NamaForm_viApotekPengeluaranKepemilikan, 
        closeAction: 'destroy',        
        width: 1000,
        height: 550,//605,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viApotekPengeluaranKepemilikan(lebar,rowdata), //1
        listeners:
        {
            activate: function()
            {
				
            },
            afterShow: function()
            {
                this.activate();
            },
            deactivate: function()
            {
                rowSelected_viApotekPengeluaranKepemilikan=undefined;
                //datarefresh_viApotekPengeluaranKepemilikan();
				mNoKunjungan_viApotekPengeluaranKepemilikan = '';
            }
        }
    }
    );

    setLookUps_viApotekPengeluaranKepemilikan.show();
    if (rowdata == undefined)
    {
        // dataaddnew_viApotekPengeluaranKepemilikan();
		// Ext.getCmp('btnDelete_viApotekPengeluaranKepemilikan').disable();	
    }
    else
    {
        // datainit_viApotekPengeluaranKepemilikan(rowdata);
    }
}
// End Function setLookUpGridDataView_viApotekPengeluaranKepemilikan # --------------

/**
*	Function : getFormItemEntry_viApotekPengeluaranKepemilikan
*	
*	Sebuah fungsi untuk menampilkan penataan menu dan isian form edit
*/

function getFormItemEntry_viApotekPengeluaranKepemilikan(lebar,rowdata)
{
    var pnlFormDataBasic_viApotekPengeluaranKepemilikan = new Ext.FormPanel
    (
		{
			title: '',
			region: 'north',
			layout: 'form',
			bodyStyle: 'padding:10px 10px 10px 10px',
			anchor: '100%',
			labelWidth: 1,
			autoWidth: true,
			width: lebar,
			border: false,
			//-------------- #items# --------------
			items:
			[
				getItemPanelInputBiodata_viApotekPengeluaranKepemilikan(lebar),
				//-------------- ## -------------- 				
				getItemGridTransaksi_viApotekPengeluaranKepemilikan(lebar),
				//-------------- ## --------------				
			],
			//-------------- #End items# --------------
			fileUpload: true,
			// Tombol pada tollbar Edit Data Pasien
			tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						id: 'btnAdd_viApotekPengeluaranKepemilikan',
						handler: function(){
							dataaddnew_viApotekPengeluaranKepemilikan();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viApotekPengeluaranKepemilikan',
						handler: function()
						{
							datasave_viApotekPengeluaranKepemilikan(addNew_viApotekPengeluaranKepemilikan);
							datarefresh_viApotekPengeluaranKepemilikan();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						id: 'btnSimpanExit_viApotekPengeluaranKepemilikan',
						handler: function()
						{
							var x = datasave_viApotekPengeluaranKepemilikan(addNew_viApotekPengeluaranKepemilikan);
							datarefresh_viApotekPengeluaranKepemilikan();
							if (x===undefined)
							{
								setLookUps_viApotekPengeluaranKepemilikan.close();
							}
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viApotekPengeluaranKepemilikan',
						handler: function()
						{
							datadelete_viApotekPengeluaranKepemilikan();
							datarefresh_viApotekPengeluaranKepemilikan();
							
						}
					},
					//-------------- ## --------------
					{
						xtype:'tbseparator'
					},
					
				]
			}//,items:
		}
    )

    return pnlFormDataBasic_viApotekPengeluaranKepemilikan;
}
// End Function getFormItemEntry_viApotekPengeluaranKepemilikan # --------------

/**
*	Function : getItemPanelInputBiodata_viApotekPengeluaranKepemilikan
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemPanelInputBiodata_viApotekPengeluaranKepemilikan(lebar) 
{
    
    var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		labelWidth: 100,
		autoWidth: true,
		border:true,
		items:
		[			
			{
                xtype: 'compositefield',
                fieldLabel: 'No. Keluar',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					{
						xtype: 'textfield',
						flex: 1,
						width : 120,	
						name: 'txtNoPengeluaranKepemilikan_viApotekPengeluaranKepemilikan',
						id: 'txtNoPengeluaranKepemilikan_viApotekPengeluaranKepemilikan',
						emptyText: 'No Keluar',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},                    
                    //-------------- ## --------------  
					{
						xtype: 'displayfield',
						flex: 1,
						width: 75,
						name: '',
						value: 'Kepemilikan :',
						fieldLabel: 'Label'
					},					
					viCombo_Kepemilikan(415, 'cboPbf_viApotekPengeluaranKepemilikan')									
                ]
            },
            //-------------- ## --------------  
			{
				xtype: 'compositefield',
				fieldLabel: 'Tanggal',
				anchor: '100%',
				//labelSeparator: '',
				width: 199,
				items: 
				[
					{
						xtype: 'datefield',
						id: 'txtDateTanggal_viApotekPengeluaranKepemilikan',
						value: now_viApotekPengeluaranKepemilikan,
						format: 'd/M/Y',
						width: 120,
						listeners:
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									datarefresh_viApotekPengeluaranKepemilikan();								
								} 						
							}
						}
					},
					{
						xtype: 'displayfield',
						flex: 1,
						width: 75,
						name: '',
						value: 'Remark :',
						fieldLabel: 'Label'
					},					
					{
						xtype: 'textfield',
						flex: 1,
						width : 360,	
						name: 'txtRemark_viApotekPengeluaranKepemilikan',
						id: 'txtRemark_viApotekPengeluaranKepemilikan',
						emptyText: 'Remark',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					}
				]
			}
		]
	};
    return items;
};
// End Function getItemPanelInputBiodata_viApotekPengeluaranKepemilikan # --------------


/**
*	Function : getItemGridTransaksi_viApotekPengeluaranKepemilikan
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemGridTransaksi_viApotekPengeluaranKepemilikan(lebar) 
{
    var items =
	{
		//title: 'Detail Transaksi', 
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:1px 1px 1px 1px',
		border:true,
		width: lebar-80,
		height: 400,//225, 
	    items:
		[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:
				[
					gridDataViewEdit_viApotekPengeluaranKepemilikan()
				]	
			}
			//-------------- ## --------------
		]
	};
    return items;
};
// End Function getItemGridTransaksi_viApotekPengeluaranKepemilikan # --------------

/**
*	Function : gridDataViewEdit_viApotekPengeluaranKepemilikan
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function gridDataViewEdit_viApotekPengeluaranKepemilikan()
{
    
    chkSelected_viApotekPengeluaranKepemilikan = new Ext.grid.CheckColumn
	(
		{
			id: 'chkSelected_viApotekPengeluaranKepemilikan',
			header: '',
			align: 'center',						
			dataIndex: 'SELECTED',			
			width: 20
		}
	);

    var FieldGrdKasir_viApotekPengeluaranKepemilikan = 
	[
	];
	
    dsDataGrdJab_viApotekPengeluaranKepemilikan= new WebApp.DataStore
	({
        fields: FieldGrdKasir_viApotekPengeluaranKepemilikan
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJab_viApotekPengeluaranKepemilikan,
        height: 395,//220,
		selModel: new Ext.grid.RowSelectionModel
        (
	        {
	            singleSelect: true,
	            listeners:
	            {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
	        }
        ),
        columns: 
		[
			chkSelected_viApotekPengeluaranKepemilikan,			
			{			
				dataIndex: '',
				header: 'Kode',
				sortable: true,
				width: 100
			},
			{			
				dataIndex: '',
				header: 'M',
				sortable: true,
				width: 20
			},			
			{			
				dataIndex: '',
				header: 'Uraian',
				sortable: true,
				width: 650
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Sat B',
				sortable: true,
				width: 40,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Qty B',
				sortable: true,
				width: 40,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Frac',
				sortable: true,
				width: 35,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Qty K',
				sortable: true,
				width: 50,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			
        ],
        plugins:chkSelected_viApotekPengeluaranKepemilikan,
    }    
    return items;
}
// End Function gridDataViewEdit_viApotekPengeluaranKepemilikan # --------------


function viCombo_Kepemilikan(lebar,Nama_ID)
{
    var Field_Pemilik = ['KD_VENDOR', 'KD_CUSTOMER', 'VENDOR'];
    ds_Pemilik = new WebApp.DataStore({fields: Field_Pemilik});
    
	// viRefresh_Vendor();
	
    var cbo_Pemilik = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'Vendor',
			valueField: 'KD_VENDOR',
            displayField: 'Pemilik',
			emptyText:'Kepemilikan',
			store: ds_Pemilik,
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',                        
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
			listeners:
			{ 
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_Pemilik;
}