var GFPenghapusan={
	vars:{
		selected:null,
		appTitle:'Penghapusan',
		nowDate: new Date()
	},
	form:{
		ArrayStore:{
			main:new Ext.data.ArrayStore({id: 0,fields: ['no_hapus','post_hapus','hps_date','ket_hapus'],data:[]}),
			detail:new Ext.data.ArrayStore({id: 0,fields: ['kd_prd','nama_obat','kd_sat_besar','hapus_ket','qty_hapus','jml_stok_apt','no_hapus'],data:[]}),
			obat:new Ext.data.ArrayStore({id: 0,fields:[],data: []})
		},
		Grid:{
			main:null,
			detail:null
		},
		Panel:{
			search: null,
			input: null
		},
		Window:{
			input:null
		},
		TextArea:{
			remark:null		
		},
		TextField:{
			no:null,
			srchNo: null
		},
		DateField:{
			date: null,
			srchStartDate: null,
			srchLastDate: null
		},
		DisplayField:{
			posting:null
		},
		NumberField:{
			totalQty:null,
			totalHapus:null
		},
		Button:{
			posting:null
		}
	},
	refresh:function(){
		var $this=this;
		loadMask.show();
		var a=[];
		a.push({name: 'no_hapus',value:$this.form.TextField.srchNo.getValue()});
		a.push({name: 'startDate',value:$this.form.DateField.srchStartDate.value});
		a.push({name: 'lastDate',value:$this.form.DateField.srchLastDate.value});
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			url:baseURL + "index.php/gudang_farmasi/functionGFPenghapusan/initList",
			data:a,
			success: function(r){
				loadMask.hide();
				if(r.processResult=='SUCCESS'){
					$this.form.ArrayStore.main.loadData([],false);
					for(var i=0,iLen=r.listData.length; i<iLen ;i++){
						var records=[];
						records.push(new $this.form.ArrayStore.main.recordType());
						$this.form.ArrayStore.main.add(records);
						$this.form.ArrayStore.main.data.items[i].data.post_hapus=r.listData[i].post_hapus;
						$this.form.ArrayStore.main.data.items[i].data.no_hapus=r.listData[i].no_hapus;
						$this.form.ArrayStore.main.data.items[i].data.hps_date=r.listData[i].hps_date;
						$this.form.ArrayStore.main.data.items[i].data.ket_hapus=r.listData[i].ket_hapus;
					}
					$this.form.Grid.main.getView().refresh();
				}else{
					Ext.Msg.alert('Gagal',r.processMessage);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
	},
	posting:function(){
		var $this=this;
		if($this.getParams() != undefined){
			if($this.form.Button.posting.text=='Posting'){
				Ext.Msg.confirm('Konfirmasi', 'Apakah Akan diPosting ?', function (id, value) { 
					if (id === 'yes') { 
						if($this.form.TextField.no.getValue()==''){
							loadMask.show();
							$.ajax({
								url:baseURL + "index.php/gudang_farmasi/functionGFPenghapusan/postingSave",
								dataType:'JSON',
								type: 'POST',
								data:$this.getParams(),
								success: function(r){
									loadMask.hide();
									if(r.processResult=='SUCCESS'){
										$this.form.TextField.no.setValue(r.resultObject.code);
										for(var i=0; i<$this.form.ArrayStore.detail.getCount() ; i++){
											var o=$this.form.ArrayStore.detail.getRange()[i].data;
											o.no_hapus=r.resultObject.code;
										}
										$this.form.Button.posting.setText('Unposting');
										$this.form.DisplayField.posting.setValue('<img src="'+baseURL+'ui/images/icons/16x16/hijau.png"/>');
										Ext.Msg.alert('Sukses','Data Berhasi Disimpan dan diPosting.');
									}else{
										Ext.Msg.alert('Gagal',r.processMessage);
									}
								},
								error: function(jqXHR, exception) {
									loadMask.hide();
									Nci.ajax.ErrorMessage(jqXHR, exception);
								}
							});
						}else{
							loadMask.show();
							$.ajax({
								url:baseURL + "index.php/gudang_farmasi/functionGFPenghapusan/postingUpdate",
								dataType:'JSON',
								type: 'POST',
								data:$this.getParams(),
								success: function(r){
									loadMask.hide();
									if(r.processResult=='SUCCESS'){
										Ext.Msg.alert('Sukses','Data Berhasi Diubah dan diPosting.');
										for(var i=0; i<$this.form.ArrayStore.detail.getCount() ; i++){
											var o=$this.form.ArrayStore.detail.getRange()[i].data;
											o.nu_hapus=$this.form.TextField.no.getValue();
										}
										$this.form.Button.posting.setText('Unposting');
										$this.form.DisplayField.posting.setValue('<img src="'+baseURL+'ui/images/icons/16x16/hijau.png"/>');
									}else{
										Ext.Msg.alert('Gagal',r.processMessage);
									}
								},
								error: function(jqXHR, exception) {
									loadMask.hide();
									Nci.ajax.ErrorMessage(jqXHR, exception);
								}
							});
						}
					} 
				}, this); 
			}else{
				Ext.Msg.confirm('Konfirmasi', 'Apakah Akan diUnposting ?', function (id, value) { 
					if (id === 'yes') { 
						loadMask.show();
						$.ajax({
							url:baseURL + "index.php/gudang_farmasi/functionGFPenghapusan/unposting",
							dataType:'JSON',
							type: 'POST',
							data:{no_hapus:$this.form.TextField.no.getValue()},
							success: function(r){
								loadMask.hide();
								if(r.processResult=='SUCCESS'){
									Ext.Msg.alert('Sukses','Data Berhasi diUnposting.');
									$this.form.Button.posting.setText('Posting');
									$this.form.DisplayField.posting.setValue('<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>');
								}else{
									Ext.Msg.alert('Gagal',r.processMessage);
								}
							},
							error: function(jqXHR, exception) {
								loadMask.hide();
								Nci.ajax.ErrorMessage(jqXHR, exception);
							}
						});
					} 
				}, this); 
			}
		}
	},
	del:function(){
		var $this=this;
		var line	= $this.form.Grid.detail.getSelectionModel().selection.cell[0];
		var o=$this.form.ArrayStore.detail.getRange()[line].data;
		if($this.form.ArrayStore.detail.getCount()>1){
			if(o.no_hapus != undefined && o.no_hapus!=''){
					Ext.Msg.prompt('Name', 'Apakah Data Ini Akan Dihapus?', function(btn, text){
					    if (btn == 'ok'){
					    	loadMask.show();
					    	$.ajax({
								url:baseURL + "index.php/gudang_farmasi/functionGFPenghapusan/deleteDetail",
								dataType:'JSON',
								type: 'POST',
								data:{no_hapus:o.no_hapus,line_hapus:(line+1)},
								success: function(r){
									loadMask.hide();
									if(r.processResult=='SUCCESS'){
										Ext.Msg.alert('Sukses','Data Berhasi Dihapus.');
										$this.form.ArrayStore.detail.removeAt(line);
										$this.form.Grid.detail.getView().refresh();
									}else{
										Ext.Msg.alert('Gagal',r.processMessage);
									}
								},
								error: function(jqXHR, exception) {
									loadMask.hide();
									Nci.ajax.ErrorMessage(jqXHR, exception);
								}
							});
					    }
					});
				
			}else{
				Ext.Msg.confirm('Konfirmasi', 'Apakah ingin menghapus data ini?', function (id, value) { 
					if (id === 'yes') { 
						$this.form.ArrayStore.detail.removeAt(line);
						$this.form.Grid.detail.getView().refresh();
					} 
				}, this); 
			}
		}else{
			Ext.Msg.alert('Gagal','Data tidak bisa dihapus karena minimal mempunyai 1 data.');
		}
	},
	getParams:function(){
		var $this=this;
		var a=[];
		if($this.form.TextArea.remark.getValue()!=''){
			a.push({name: 'ket_hapus',value:$this.form.TextArea.remark.getValue()});
		}else{
			Ext.Msg.alert('Error','Harap Isi Remark.');
			return;
		}
		a.push({name: 'no_hapus',value:$this.form.TextField.no.getValue()});
		a.push({name: 'hps_date',value:$this.form.DateField.date.value});
		if($this.form.ArrayStore.detail.getCount()==0){
			Ext.Msg.alert('Error','Barang Tidak Lengkap.');
			return;
		}
		for(var i=0; i<$this.form.ArrayStore.detail.getCount();i++){
			var o=$this.form.ArrayStore.detail.getRange()[i].data;
			if(o.kd_prd != undefined && o.kd_prd != ''){
					a.push({name: 'kd_prd[]',value:o.kd_prd});
			}else{
				Ext.Msg.alert('Error','Harap Pilih Produk.');
				return;
			}
			if(o.hapus_ket !=''){
				a.push({name: 'hapus_ket[]',value:o.hapus_ket});
			}else{
				Ext.Msg.alert('Error','Harap Isi Keterangan Hapus.');
				return;
			}
			if(o.qty_hapus !=0){
				a.push({name: 'qty_hapus[]',value:o.qty_hapus});
			}else{
				Ext.Msg.alert('Error','Harap Isi Quantity Hapus.');
				return;
			}
		}
		return a;
	},validasiSavePenghapusan:function(){
		var $this=this;
		var x = 1;
		for(var i = 0 ; i < $this.form.ArrayStore.detail.getCount();i++)
		{
			var o=$this.form.ArrayStore.detail.getRange()[i].data;
			for(var j = i+1 ; j < $this.form.ArrayStore.detail.getCount();j++)
			{
				var p=$this.form.ArrayStore.detail.getRange()[j].data;
				if (p.kd_prd==o.kd_prd)
				{
					Ext.Msg.alert('Warning','Obat ada yang sama');
					x = 0;
				}
			}
			
		}
		return x;
	},
	save:function(callback){
		var $this=this;
		if($this.getParams() != undefined){
			/* if ($this.validasiSavePenghapusan()==1)	
			{ */
				if($this.form.TextField.no.getValue()==''){
					loadMask.show();
					$.ajax({
						url:baseURL + "index.php/gudang_farmasi/functionGFPenghapusan/save",
						dataType:'JSON',
						type: 'POST',
						data:$this.getParams(),
						success: function(r){
							loadMask.hide();
							if(r.processResult=='SUCCESS'){
								$this.form.TextField.no.setValue(r.resultObject.code);
								for(var i=0; i<$this.form.ArrayStore.detail.getCount() ; i++){
									var o=$this.form.ArrayStore.detail.getRange()[i].data;
									o.no_hapus=r.resultObject.code;
								}
								Ext.Msg.alert('Sukses','Data Berhasi Disimpan.');
								if(callback != undefined){
									callback();
								}
							}else{
								Ext.Msg.alert('Gagal',r.processMessage);
							}
						},
						error: function(jqXHR, exception) {
							loadMask.hide();
							Nci.ajax.ErrorMessage(jqXHR, exception);
						}
					});
				}else{
					loadMask.show();
					$.ajax({
						url:baseURL + "index.php/gudang_farmasi/functionGFPenghapusan/update",
						dataType:'JSON',
						type: 'POST',
						data:$this.getParams(),
						success: function(r){
							loadMask.hide();
							if(r.processResult=='SUCCESS'){
								Ext.Msg.alert('Sukses','Data Berhasi Diubah.');
								for(var i=0; i<$this.form.ArrayStore.detail.getCount() ; i++){
									var o=$this.form.ArrayStore.detail.getRange()[i].data;
									o.no_hapus=$this.form.TextField.no.getValue();
								}
								if(callback != undefined){
									callback();
								}
							}else{
								Ext.Msg.alert('Gagal',r.processMessage);
							}
						},
						error: function(jqXHR, exception) {
							loadMask.hide();
							Nci.ajax.ErrorMessage(jqXHR, exception);
						}
					});
				}
			//}
		}
	},
	refreshCounting:function(){
		var $this=this;
		var totQty=0,totHapus=0;
		for(var i=0; i<$this.form.ArrayStore.detail.getCount() ; i++){
			var o=$this.form.ArrayStore.detail.getRange()[i].data;
			if(o.jml_stok_apt == undefined || ( o.jml_stok_apt != undefined && o.jml_stok_apt==''))o.jml_stok_apt=0;
			if(o.qty_hapus == undefined || ( o.qty_hapus != undefined && o.qty_hapus==''))o.qty_hapus=0;
			if(o.qty_hapus>o.jml_stok_apt)o.qty_hapus=o.jml_stok_apt;
			totQty+=parseInt(o.jml_stok_apt);
			totHapus+=parseInt(o.qty_hapus);
		}
		$this.form.NumberField.totalQty.setValue(totQty);
		$this.form.NumberField.totalHapus.setValue(totHapus);
		$this.form.Grid.detail.getView().refresh();
	},
	addItem:function(){
		var $this=this;
		if($this.form.Button.posting.text=='Posting'){
			var records = new Array();
			records.push(new $this.form.ArrayStore.detail.recordType());
			$this.form.ArrayStore.detail.add(records);
			$this.refreshCounting();
			var row=$this.form.ArrayStore.detail.getCount()-1;
			$this.form.Grid.detail.startEditing(row,2);
			
		}else{
			Ext.Msg.alert('Informasi','Penyimpanan gagal karena data sudah diPosting, Harap Unposting jika ingin Menyimpan.');
		}
	},
	getGridInput: function(){
		var $this=this;
	    $this.form.Grid.detail = new Ext.grid.EditorGridPanel({
	        store: $this.form.ArrayStore.detail,
	        flex: 1,
	        columnLines: true,
	        stripeRows: true,
			selModel: new Ext.grid.CellSelectionModel ({
	            singleSelect: true,
	            listeners:{
	                rowselect: function(sm, row, rec){
	                }
	            }
	        }),
	        columns:[
				new Ext.grid.RowNumberer(),			
				{			
					dataIndex: 'kd_prd',
					header: 'Kode',
					sortable: true,
					width: 100
				},{			
					dataIndex: 'nama_obat',
					header: 'Uraian',
					sortable: true,
					width: 250,
					editor: new Nci.form.Combobox.autoComplete({
						store	: $this.form.ArrayStore.obat,
						select	: function(a,b,c){
							var line	= $this.form.Grid.detail.getSelectionModel().selection.cell[0];
							if($this.form.ArrayStore.detail.getCount()-1===0)
							{
								$this.form.ArrayStore.detail.getRange()[line].data.nama_obat=b.data.nama_obat;
								$this.form.ArrayStore.detail.getRange()[line].data.kd_prd=b.data.kd_prd;
								$this.form.ArrayStore.detail.getRange()[line].data.kd_sat_besar=b.data.kd_sat_besar;
								$this.form.ArrayStore.detail.getRange()[line].data.jml_stok_apt=b.data.jml_stok_apt;
								$this.form.ArrayStore.detail.getRange()[line].data.min_stok=b.data.min_stok;
								$this.refreshCounting();
								
								var row=$this.form.ArrayStore.detail.getCount()-1;
								$this.form.Grid.detail.startEditing(row,4);
							}
							else
							{
								var VALUE_x=1;
								for(var i = 0 ; i < $this.form.ArrayStore.detail.getCount();i++)
								{
									if($this.form.ArrayStore.detail.getRange()[i].data.kd_prd===b.data.kd_prd)
									{
										VALUE_x=0;
									}
								}
								if(VALUE_x===0){
									Ext.Msg.alert('Perhatian','Anda telah memilih Obat yang sama');
									$this.form.ArrayStore.detail.removeAt(line);
								}else
								{
									$this.form.ArrayStore.detail.getRange()[line].data.nama_obat=b.data.nama_obat;
									$this.form.ArrayStore.detail.getRange()[line].data.kd_prd=b.data.kd_prd;
									$this.form.ArrayStore.detail.getRange()[line].data.kd_sat_besar=b.data.kd_sat_besar;
									$this.form.ArrayStore.detail.getRange()[line].data.jml_stok_apt=b.data.jml_stok_apt;
									$this.form.ArrayStore.detail.getRange()[line].data.min_stok=b.data.min_stok;
									$this.refreshCounting();
									
									var row=$this.form.ArrayStore.detail.getCount()-1;
									$this.form.Grid.detail.startEditing(row,4);
								}
							}
							
						},
						insert	: function(o){
							return {
								kd_prd        	:o.kd_prd,
								nama_obat 		: o.nama_obat,
								kd_sat_besar	: o.kd_sat_besar,
								jml_stok_apt	: o.jml_stok_apt,
								min_stok	: o.min_stok,
								text				:  '<table style="font-size: 11px;"><tr><td width="50">'+o.kd_prd+'</td><td width="200">'+o.nama_obat+'</td><td width="50">'+o.jml_stok_apt+'</td><td width="50">&nbsp;&nbsp;'+o.min_stok+'</td></tr></table>'
							}
						},
						url		: baseURL + "index.php/gudang_farmasi/functionGFPenghapusan/getObat",
						valueField: 'nama_obat',
						displayField: 'text',
						listWidth: 300,
						blur: function(){
							//console.log( $this.form.Grid.detail.getSelectionModel().selection.cell[0])
							$this.refreshCounting();
							var row=$this.form.ArrayStore.detail.getCount()-1;
							$this.form.Grid.detail.startEditing(row,4);
						}
					})
				},{
					dataIndex: 'kd_sat_besar',
					header: 'Satuan',
					sortable: true,
					width: 90
				},{
				xtype:'numbercolumn',
				dataIndex: 'min_stok',
				header: 'Min Stok',
				align: 'right',
				hidden :true,
				sortable: true,
				width: 100	
				},{
					dataIndex: 'hapus_ket',
					header: 'Keterangan',
					sortable: true,
					width: 300,
					editor:new Ext.form.TextField()
				},{
					xtype:'numbercolumn',
					dataIndex: 'jml_stok_apt',
					header: 'Stok',
					align: 'right',
					sortable: true,
					width: 35
				},{
					xtype:'numbercolumn',
					dataIndex: 'qty_hapus',
					header: 'Qty Hapus',
					align: 'right',
					sortable: true,
					width: 75,
					editor:new Ext.form.NumberField({
						value:0,
						listeners:{
							blur: function(a){
								var line	= GFPenghapusan.form.ArrayStore.detail.getCount()-1;
								var d	= $this.form.Grid.detail.getSelectionModel().selection.cell[0];
								//alert(this.indeks)
								console.log(d);
								//console.log(GFPenghapusan.form.ArrayStore.detail.data.items[d].data.min_stok)//(a.getValue());
								if ( parseFloat(a.getValue())   >= parseFloat(GFPenghapusan.form.ArrayStore.detail.data.items[d].data.min_stok) )
								{
									Ext.Msg.alert('Perhatian','Qty sudah mencapai atau melebihi minimum stok.');
								}
								else (parseFloat(a.getValue()) < parseFloat(GFPenghapusan.form.ArrayStore.detail.data.items[d].data.min_stok) )
								{
									GFPenghapusan.form.ArrayStore.detail.data.items[d].data.qty=a.getValue();
								}
								//GFPenghapusan.form.ArrayStore.detail.getRange()[line].data.qty_hapus=a.getValue();
								$this.refreshCounting();
								var row=$this.form.ArrayStore.detail.getCount()-1;
									$this.form.Grid.detail.startEditing(row,2);
							},
							focus:function(){
								this.indeks=$this.form.Grid.detail.getSelectionModel().last;
							},
							specialkey: function(){
								if(Ext.EventObject.getKey()==13){
									var records = new Array();
									/* records.push(new $this.form.ArrayStore.detail.recordType());
									$this.form.ArrayStore.detail.add(records); */
									//$this.refreshCounting();
									var row=$this.form.ArrayStore.detail.getCount()-1;
									$this.form.Grid.detail.startEditing(row,2);
									//$this.refreshCounting();
								}
							}
						}	
					})
				}
	        ]
	    });   
	    return $this.form.Grid.detail;
	},
	getItemInput: function(){
		var $this=this;
		var items =new Ext.Panel({
		    bodyStyle: 'padding:10px 10px 10px 10px',
			border:false,
			height: 100,
			items:[
			{
				layout:'column',
				border:false,
				height: 200,
				items:[
				{
					layout:'column',
					border:false,
					items:[
					{
						xtype:'displayfield',
						value:'No. Hapus :',
						width: 100
					},$this.form.TextField.no=new Ext.form.TextField({
						width : 120,	
						readOnly: true,
						listeners:{
							'specialkey': function(){
								if (Ext.EventObject.getKey() === 13){
								}
							}
						}
					})
					]
				},{
					xtype:'displayfield',
					width:50,
					value:'&nbsp;'
				},{
					layout:'column',
					border:false,
					items:[
					{
						xtype:'displayfield',
						value:'Tanggal :',
						width: 100
					},$this.form.DateField.date= new Ext.form.DateField({
						value: $this.vars.nowDate,
						format: 'd/M/Y',
						readOnly: true,
						width: 120,
						listeners:{ 
							'specialkey' : function(){
								if (Ext.EventObject.getKey() === 13){
								} 						
							}
						}
					})
					]
				},{
					layout:'column',
					border:false,
					items:[
					{
						xtype:'displayfield',
						value:'Remark :',
						width: 100
					},$this.form.TextArea.remark= new Ext.form.TextArea({
						width: 830,
						height: 50,			
						name: 'remark',
						listeners:{
							'specialkey': function(){
								if (Ext.EventObject.getKey() === 9){
									console.log(this)
									var records = new Array();
									records.push(new $this.form.ArrayStore.detail.recordType());
									GFPenghapusan.form.ArrayStore.detail.add(records);
									$this.refreshCounting();
									var row=$this.form.ArrayStore.detail.getCount()-1;
									$this.form.Grid.detail.startEditing(row,2);
								}
							}
						}
					})
					]
				}
				]
			}
			]
		});
	    return items;
	},
	getItemInputGrid: function(){
		var $this=this;
		var items ={
		    layout: {
		    	type:'vbox',
		    	align:'stretch'
		    },
			flex:1, 
		    labelAlign: 'Left',
			border:false,
		    items:[
				$this.getGridInput()
			]
		};
	    return items;
	},
	getFormInput: function(rowdata){
		var $this=this;
		$this.form.Panel.input = new Ext.FormPanel({
			title: '',
			layout: {
				type:'vbox',
				align:'stretch'
			},
			flex:1,
			labelWidth: 1,
			autoWidth: true,
			border: false,
			items:[
				$this.getItemInput(),
				$this.getItemInputGrid()		                
			],
			fileUpload: true,
			fbar:[
				$this.form.DisplayField.posting=new Ext.form.DisplayField({
					value		: '<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>'
				}),{
					xtype:'displayfield',
					value:'Posted'
				},{
					xtype:'displayfield',
					value:'Total Qty : '
				},$this.form.NumberField.totalQty=new Ext.form.NumberField({
					width: 100,
					value:0,
					readOnly:true
				}),{
					xtype:'displayfield',
					value:'Total Hapus : '
				},$this.form.NumberField.totalHapus=new Ext.form.NumberField({
					width: 100,
					value:0,
					readOnly:true
				})
        	],
			tbar: {
				xtype: 'toolbar',
				items: [
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						handler: function(){
							$this.addItem();
						}
					},{
						xtype: 'tbseparator'
					},{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viApotekPenghapusan',
						handler: function(){
							if($this.form.Button.posting.text=='Posting'){
								$this.save();
							}else{
								Ext.Msg.alert('Informasi','Penyimpanan gagal karena data sudah diPosting, Harap Unposting jika ingin Menyimpan.');
							}
						}
					},{
						xtype: 'tbseparator'
					},{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						id: 'btnSimpanExit_viApotekPenghapusan',
						handler: function(){
							if($this.form.Button.posting.text=='Posting'){
								$this.save(function(){
									$this.form.Window.input.close();
								});
							}else{
								Ext.Msg.alert('Informasi','Penyimpanan gagal karena data sudah diPosting, Harap Unposting jika ingin Menyimpan.');
							}
						}
					},{
						xtype: 'tbseparator'
					},{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viApotekPenghapusan',
						handler: function(){
							if($this.form.Button.posting.text=='Posting'){
								$this.del();
							}else{
								Ext.Msg.alert('Error','Data Sudah Diposting, Harap Unposting Jika ingin Menghapus Data.');
							}
						}
					},{
						xtype:'tbseparator'
					},$this.form.Button.posting=new Ext.Button({
						text: 'Posting',
						iconCls: 'gantidok',
						handler: function(){
							$this.posting();
						}
					}),{
						xtype:'tbseparator'
					}
					
				]
			}
		});
    	return $this.form.Panel.input;
    	
	},
	showWindow: function(rowdata){
		var $this=this;
    	$this.form.Window.input = new Ext.Window({
	        title: $this.vars.appTitle, 
	        closeAction: 'destroy',        
	        width: 900,
	        height: 500,
	        resizable:false,
			autoScroll: false,
			layout:{
				type: 'vbox',
				align:'stretch'
			},
	        border: false,
	        constrainHeader : true,    
	        iconCls: 'Studi_Lanjut',
	        modal: true,		
        	items: $this.getFormInput(rowdata), //1
        	listeners:{
	            activate: function(){
					
	            },
	            afterShow: function(){
	                this.activate();
	            },
	            deactivate: function(){
	            }
        	}
    	});
    	loadMask.show();
	    if (rowdata == undefined) {
	    	$.ajax({
				url:baseURL + "index.php/gudang_farmasi/functionGFPenghapusan/initTransaksi",
				dataType:'JSON',
				type: 'GET',
				success: function(r){
					loadMask.hide();
					if(r.processResult=='SUCCESS'){
						$this.form.Window.input.show();
					    $this.form.ArrayStore.detail.loadData([],false);
					    $this.form.Grid.detail.getView().refresh();
					}else{
						Ext.Msg.alert('Error',r.processMessage);
					}
				},
				error: function(jqXHR, exception) {
					loadMask.hide();
					Nci.ajax.ErrorMessage(jqXHR, exception);
				}
			});
	    }else{
	    	$.ajax({
	 			url:baseURL + "index.php/gudang_farmasi/functionGFPenghapusan/getForEdit",
	 			dataType:'JSON',
	 			type: 'POST',
	 			data: {'no_hapus':rowdata.no_hapus},
	 			success: function(r){
	 				loadMask.hide();
	 				if(r.processResult=='SUCCESS'){
	 					$this.form.Window.input.show();
	 					$this.form.TextField.no.setValue(r.resultObject.no_hapus);
	 					$this.form.TextArea.remark.setValue(r.resultObject.ket_hapus);
	 					var date = Date.parseDate(r.resultObject.hps_date, "Y-m-d H:i:s");
	 					$this.form.DateField.date.setValue(date);
	 					if(r.resultObject.post_hapus==1){
	 						$this.form.Button.posting.setText('Unposting');
	 						$this.form.DisplayField.posting.setValue('<img src="'+baseURL+'ui/images/icons/16x16/hijau.png"/>');
	 					}else{
	 						$this.form.DisplayField.posting.setValue('<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>');
	 						$this.form.Button.posting.setText('Posting');
	 					}
	 					$this.form.ArrayStore.detail.loadData([],false);
	 					var recs = [],
	 					recordType=$this.form.ArrayStore.detail.recordType;
	 					for(var i=0, iLen=r.listData.length ; i<iLen ; i++){
	 						recs.push(new recordType(r.listData[i]));
	 					}
	 					$this.form.ArrayStore.detail.add(recs);
	 					$this.form.Grid.detail.getView().refresh();
	 					$this.refreshCounting();
	 				}else{
	 					Ext.Msg.alert('Error',r.processMessage);
	 				}
	 			},
	 			error: function(jqXHR, exception) {
	 				loadMask.hide();
	 				Nci.ajax.ErrorMessage(jqXHR, exception);
	 			}
	 		});
	    }
	},
	main: function(modId){
		var $this=this;
		$this.form.Grid.main= new Ext.grid.EditorGridPanel({
			xtype: 'editorgrid',
			title: '',
			store: $this.form.ArrayStore.main,
			autoScroll: true,
			flex: 1,
			columnLines: true,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel({
				singleSelect: true,
				listeners:{
					rowselect: function(sm, row, rec){
						$this.vars.selected = $this.form.ArrayStore.main.getAt(row);
					}
				}
			}),
			listeners:{
				rowdblclick: function (sm, ridx, cidx){
					$this.vars.selected = $this.form.ArrayStore.main.getAt(ridx);
					if ($this.vars.selected != undefined){
						$this.showWindow($this.vars.selected.data)
					}
				}
			},
			colModel: new Ext.grid.ColumnModel([
				new Ext.grid.RowNumberer(),
				{
					header		: 'Status Posting',
					width		: 20,
					sortable	: false,
					hideable	: true,
					hidden		: false,
					menuDisabled: true,
					dataIndex	: 'post_hapus',
					renderer	: function(value, metaData, record, rowIndex, colIndex, store){
						 switch (value){
							 case '1':
								 metaData.css = 'StatusHijau'; 
								 break;
							 case '0':
								 metaData.css = 'StatusMerah';
								 break;
						 }
						 return '';
					}
				},{
					header: 'No. Hapus',
					dataIndex: 'no_hapus',
					sortable: true,
					width: 35
				},{
					header:'Tgl Hapus',
					dataIndex: 'hps_date',						
					width: 20,
					sortable: true,
					renderer: function(v, params, record){
						return ShowDate(record.data.hps_date);
					}
				},{
					header: 'Remark',
					dataIndex: 'ket_hapus',
					sortable: true,
					width: 60
				}
			]),
			tbar:{
				xtype: 'toolbar',
				id: 'toolbar_viApotekPenghapusan',
				items: [
					{
						xtype: 'button',
						text: 'Tambah',
						iconCls: 'add',
						tooltip: 'Edit Data',
						handler: function(sm, row, rec){
							$this.showWindow();
						}
					},{
						xtype: 'button',
						text: 'Edit',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						handler: function(sm, row, rec){
							if ($this.vars.selected != undefined){
								$this.showWindow($this.vars.selected.data)
							}
						}
					}
				]
			},
			viewConfig:{
				forceFit: true
			}
		});
		
    	$this.form.Panel.search = new Ext.FormPanel({
			title: $this.vars.appTitle,
			iconCls: 'Studi_Lanjut',
			id: modId,
			region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
			}, 
			closable: true,        
			border: false,  
			items: [$this.form.Grid.main],
			tbar:[
				{
		            xtype: 'buttongroup',
		            columns: 11,
		            defaults: {
		                scale: 'small'
		        	},
		        	frame: false,
		            items:[
			            { 
							xtype: 'tbtext', 
							text: 'No. Hapus : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},$this.form.TextField.srchNo= new Ext.form.TextField({
							emptyText: 'No. Hapus',
							name:'no_hapus',
							width: 100,
							height: 25,
							listeners:{ 
								'specialkey' : function(){
									if (Ext.EventObject.getKey() === 13){
										$this.refresh();
									} 						
								}
							}
						}),{	 
							xtype: 'tbspacer',
							width: 10,
							height: 25
						},{ 
							xtype: 'tbtext', 
							text: 'Tgl Hapus : ', 
							style:{'text-align':'right'},
							width: 60,
							height: 25
						},$this.form.DateField.srchStartDate=new Ext.form.DateField({
							name:'startDate',
							value: $this.vars.nowDate,
							format: 'd/M/Y',
							width: 120,
							listeners:{ 
								'specialkey' : function(){
									if (Ext.EventObject.getKey() === 13){
										$this.refresh();							
									} 						
								}
							}
						}),{ 
							xtype: 'tbtext', 
							text: ' s.d ', 
							style:{'text-align':'center'},
							width: 30,
							height: 25
						},$this.form.DateField.srchLastDate=new Ext.form.DateField({
							name: 'lastDate',
							value: $this.vars.nowDate,
							format: 'd/M/Y',
							width: 120,
							listeners:{ 
								'specialkey' : function(){
									if (Ext.EventObject.getKey() === 13){
										$this.refresh();								
									} 						
								}
							}
						}),{	 
							xtype: 'tbspacer',
							width: 10,
							height: 25
						},{
							xtype: 'button',
							text: 'Cari',
							iconCls: 'refresh',
							tooltip: 'Cari',
							style:{paddingLeft:'30px'},
							width:150,
							handler: function(){					
								$this.refresh();	
							}                        
						}
					]
				}
			]
	 	});
		return $this.form.Panel.search;
	},
	init: function(){
		var $this=this;
		CurrentPage.page = $this.main(CurrentPage.id);
		mainPage.add(CurrentPage.page);
		mainPage.setActiveTab(CurrentPage.id);
		$this.refresh();
	}
}
GFPenghapusan.init();