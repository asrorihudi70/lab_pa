var GFPenerimaan={};
GFPenerimaan.form={};
GFPenerimaan.func={};
GFPenerimaan.vars={};
GFPenerimaan.func.parent=GFPenerimaan;
GFPenerimaan.form.Button={};
GFPenerimaan.form.CheckBox={};
GFPenerimaan.form.ComboBox={}
GFPenerimaan.form.DataStore={};
GFPenerimaan.form.DateField={};
GFPenerimaan.form.ArrayStore={};
GFPenerimaan.form.Record={};
GFPenerimaan.form.FormPanel={};//b
GFPenerimaan.form.Grid={};//b
GFPenerimaan.form.Panel={};
GFPenerimaan.form.TextField={};//c
GFPenerimaan.form.Button={};
GFPenerimaan.form.ArrayStore.a	= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_prd','kd_satuan','nama_obat','kd_sat_besar','harga_beli'],
	data: []
});
GFPenerimaan.form.ArrayStore.b	= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_pabrik','pabrik'],
	data: []
});

GFPenerimaan.form.ArrayStore.c	= new Ext.data.ArrayStore({
	id: 0,
	fields:['posting','no_obat_in','tgl_obat_in','vendor','remark'],
	data: []
});
GFPenerimaan.func.refresh=function(){
	loadMask.show();
	$.ajax({
		type: 'POST',
		dataType:'JSON',
		url:baseURL + "index.php/gudang_farmasi/functionGFPenerimaan/initList",
		data:$('#'+GFPenerimaan.form.FormPanel.b.id).find('form').eq(0).serialize(),
		success: function(r){
			loadMask.hide();
			if(r.processResult=='SUCCESS'){
				GFPenerimaan.form.ArrayStore.c.loadData([],false);
				for(var i=0,iLen=r.listData.length; i<iLen ;i++){
					var records=[];
					records.push(new GFPenerimaan.form.ArrayStore.c.recordType());
					GFPenerimaan.form.ArrayStore.c.add(records);
					GFPenerimaan.form.ArrayStore.c.data.items[i].data.posting=r.listData[i].posting
					GFPenerimaan.form.ArrayStore.c.data.items[i].data.no_obat_in=r.listData[i].no_obat_in
					GFPenerimaan.form.ArrayStore.c.data.items[i].data.tgl_obat_in=r.listData[i].tgl_obat_in
					GFPenerimaan.form.ArrayStore.c.data.items[i].data.vendor=r.listData[i].vendor
					GFPenerimaan.form.ArrayStore.c.data.items[i].data.remark=r.listData[i].remark
				}
				GFPenerimaan.form.Grid.b.getView().refresh();
			}else{
				Ext.Msg.alert('Gagal',r.processMessage);
			}
		},
		error: function(jqXHR, exception) {
			loadMask.hide();
			Nci.ajax.ErrorMessage(jqXHR, exception);
		}
	});
}

GFPenerimaan.func.posting=function(posting,kd_unit_far,kd_milik,produkList){
	var param=[];
	param.push({name:'posting',value:posting});
	param.push({name:'kd_unit_far',value:kd_unit_far});
	param.push({name:'kd_milik',value:kd_milik});
	for(var i=0, iLen=produkList.length; i<iLen; i++){
		param.push({name:'kd_prd[]',value:produkList[i]});
	}
}
GFPenerimaan.func.convert=function(str) {
    var date = new Date(str),
        mnth = ("0" + (date.getMonth()+1)).slice(-2),
        day  = ("0" + date.getDate()).slice(-2);
    return [ date.getFullYear(), mnth, day ].join("-");
};

GFPenerimaan.func.save=function(callback){
	var param=$('#'+GFPenerimaan.form.FormPanel.a.id).find('form').eq(0).serializeArray(),e=false;
	if(GFPenerimaan.form.TextField.a.getValue()==''){
		e=true;
		Ext.Msg.alert('Error','Harap Isi PBF.');
		return;
	}
	if(dsDataGrdJab_viApotekPenerimaan.getCount()==0){
		e=true;
		Ext.Msg.alert('Error','Isi Produk Obat.');
		return;
	}
	param.push({name:'count',value:dsDataGrdJab_viApotekPenerimaan.getCount()});
	for(var i=0, iLen=dsDataGrdJab_viApotekPenerimaan.getCount(); i<iLen ; i++){
		var o=dsDataGrdJab_viApotekPenerimaan.data.items[i].data;
		if(o.qty_b==0){
			e=true;
			Ext.Msg.alert('Error','Isi Qty B Pada Baris Ke-'+(i+1));
			return;
		}
		if(o.jml_in_obat==0){
			e=true;
			Ext.Msg.alert('Error','Isi Qty Pada Baris Ke-'+(i+1));
			return;
		}
		if(o.tgl_expire == undefined || o.tgl_expire==''){
			e=true;
			Ext.Msg.alert('Error','Isi Tgl Expire Baris Ke-'+(i+1));
			return;
		}
		if(o.batch == undefined || o.batch==''){
			e=true;
			Ext.Msg.alert('Error','Isi Batch Baris Ke-'+(i+1));
			return;
		}
		param.push({name:'kd_prd-'+i,value: o.kd_prd});
		param.push({name:'jml_in_obt-'+i,value: o.jml_in_obat});
		param.push({name:'hrg_beli_obt-'+i,value: o.harga_beli});
		param.push({name:'tgl_exp-'+i,value: GFPenerimaan.func.convert(o.tgl_expire)});
		param.push({name:'apt_discount-'+i,value: o.apt_discount});
		param.push({name:'ppn_item-'+i,value: o.ppn});
		param.push({name:'apt_disc_rupiah-'+i,value: o.apt_disc_rupiah});
		param.push({name:'frac-'+i,value: o.fractions});
		param.push({name:'boxqty-'+i,value: o.qty_b});
		if(o.tag != undefined && o.tag == true){
			param.push({name:'tag-'+i,value: 1});
		}else{
			param.push({name:'tag-'+i,value: 0});
		}
		if(o.batch != undefined){
			param.push({name:'batch-'+i,value: o.batch});
		}else{
			param.push({name:'batch-'+i,value:''});
		}
		if(o.kd_pabrik != undefined){
			param.push({name:'kd_pabrik-'+i,value:o.kd_pabrik});
		}else{
			param.push({name:'kd_pabrik-'+i,value:''});
		}
	}
	if(GFPenerimaan.form.TextField.b.getValue()==''){
		loadMask.show();
		$.ajax({
			url:baseURL + "index.php/gudang_farmasi/functionGFPenerimaan/save",
			dataType:'JSON',
			type: 'POST',
			data:param,
			success: function(r){
				loadMask.hide();
				if(r.processResult=='SUCCESS'){
					GFPenerimaan.form.TextField.b.setValue(r.resultObject.code);
					for(var i=0; i<dsDataGrdJab_viApotekPenerimaan.getCount() ; i++){
						var o=dsDataGrdJab_viApotekPenerimaan.getRange()[i].data;
						o.no_obat_in=r.resultObject.code;
					}
					Ext.Msg.alert('Sukses','Data Berhasi Disimpan.');
					if(callback != undefined){
						callback();
					}
				}else{
					Ext.Msg.alert('Gagal',r.processMessage);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
	}else{
		loadMask.show();
		$.ajax({
			url:baseURL + "index.php/gudang_farmasi/functionGFPenerimaan/update",
			dataType:'JSON',
			type: 'POST',
			data:param,
			success: function(r){
				loadMask.hide();
				if(r.processResult=='SUCCESS'){
					Ext.Msg.alert('Sukses','Data Berhasi Diubah.');
					for(var i=0; i<dsDataGrdJab_viApotekPenerimaan.getCount() ; i++){
						var o=dsDataGrdJab_viApotekPenerimaan.getRange()[i].data;
						o.no_obat_in=GFPenerimaan.form.TextField.b.getValue();
					}
					if(callback != undefined){
						callback();
					}
				}else{
					Ext.Msg.alert('Gagal',r.processMessage);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
	}
}
var dataSource_viApotekPenerimaan;
var selectCount_viApotekPenerimaan=50;
var NamaForm_viApotekPenerimaan="Penerimaan ";
var selectCountStatusPostingApotekPenerimaan='Belum Posting';
var mod_name_viApotekPenerimaan="viApotekPenerimaan";
var now_viApotekPenerimaan= new Date();
var addNew_viApotekPenerimaan;
var rowSelected_viApotekPenerimaan;
var setLookUps_viApotekPenerimaan;
var mNoKunjungan_viApotekPenerimaan='';
var selectSetVendor;

var CurrentData_viApotekPenerimaan ={
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viApotekPenerimaan(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
//GFPenerimaan.func.refresh();
Q(GFPenerimaan.form.Grid.b).refresh();

function dataGrid_viApotekPenerimaan(mod_id_viApotekPenerimaan){	
	GFPenerimaan.form.Grid.b = Q().table({
		flex: 1,
		rowdblclick: function (sm, ridx, cidx,store){
			rowSelected_viApotekPenerimaan = store.getAt(ridx);
			if (rowSelected_viApotekPenerimaan != undefined){
				setLookUp_viApotekPenerimaan(rowSelected_viApotekPenerimaan.data);
			}else{
				setLookUp_viApotekPenerimaan();
			}
		},
		rowselect: function(sm, row, rec,store){
			rowSelected_viApotekPenerimaan = undefined;
			rowSelected_viApotekPenerimaan = store.getAt(row);
			CurrentData_viApotekPenerimaan
			CurrentData_viApotekPenerimaan.row = row;
			CurrentData_viApotekPenerimaan.data = rowSelected_viApotekPenerimaan.data;
		},
		tbar:{
			xtype: 'toolbar',
			items: [
				{
					xtype: 'button',
					text: 'Tambah',
					iconCls: 'add',
					tooltip: 'Edit Data',
					handler: function(sm, row, rec){
						setLookUp_viApotekPenerimaan();
					}
				},
				{
					xtype: 'button',
					text: 'Ubah',
					iconCls: 'Edit_Tr',
					tooltip: 'Edit Data',
					handler: function(sm, row, rec){
						if(rowSelected_viApotekPenerimaan != undefined){
							setLookUp_viApotekPenerimaan(rowSelected_viApotekPenerimaan.data)
						}else{
							Ext.Msg.alert('Information','Data Belum Dipilih.');
						}
					}
				}
			]
		},
		ajax:function(data,callback){
			loadMask.show();
			var a=$('#'+GFPenerimaan.form.FormPanel.b.id).find('form').eq(0).serialize();
			a+='&start='+data.start+'&size='+data.size;
			$.ajax({
				type: 'POST',
				dataType:'JSON',
				url:baseURL + "index.php/gudang_farmasi/functionGFPenerimaan/initList",
				data:a,
				success: function(r){
					loadMask.hide();
					if(r.processResult=='SUCCESS'){
						callback(r.listData,r.total);
					}else{
						Ext.Msg.alert('Gagal',r.processMessage);
					}
				},
				error: function(jqXHR, exception) {
					loadMask.hide();
					Nci.ajax.ErrorMessage(jqXHR, exception);
				}
			});
//			loadMask.show();
//			var a=[];
//			a.push({name: 'ret_number',value:$this.TextField.srchRetNumber.getValue()});
//			a.push({name: 'startDate',value:$this.DateField.srchStartDate.value});
//			a.push({name: 'lastDate',value:$this.DateField.srchLastDate.value});
//			a.push({name: 'kd_vendor',value:Ext.getCmp('cboPbf_viApotekReturPBFFilter').getValue()});
//			a.push({name: 'start',value:data.start});
//			a.push({name: 'size',value:data.size});
//			$.ajax({
//				type: 'POST',
//				dataType:'JSON',
//				url:baseURL + "index.php/gudang_farmasi/functionGFReturPBF/initList",
//				data:a,
//				success: function(r){
//					loadMask.hide();
//					if(r.processResult=='SUCCESS'){
//						callback(r.listData,r.total);
//					}else{
//						Ext.Msg.alert('Gagal',r.processMessage);
//					}
//				},
//				error: function(jqXHR, exception) {
//					loadMask.hide();
//					Nci.ajax.ErrorMessage(jqXHR, exception);
//				}
//			});
		},
		column:new Ext.grid.ColumnModel([
				new Ext.grid.RowNumberer(),
			{
				header		: 'Status Posting',
				width		: 15,
				sortable	: false,
				hideable	: true,
				hidden		: false,
				menuDisabled: true,
				dataIndex	: 'posting',
				renderer	: function(value, metaData, record, rowIndex, colIndex, store){
					 switch (value){
						 case '1':
							 metaData.css = 'StatusHijau'; 
							 break;
						 case '0':
							 metaData.css = 'StatusMerah';
							 break;
					 }
					 return '';
				}
			},{
				header: 'No. Penerimaan',
				dataIndex: 'no_obat_in',
				sortable: true,
				width: 35
					
			},{
				//xtype:'datecolumn',
				header:'Tgl Penerimaan',
				dataIndex: 'tgl_obat_in',	
				width: 20,
				sortable: true,
				hideable:false,
                menuDisabled:true,
                renderer: function(v, params, record){
					return ShowDate(record.data.tgl_obat_in);
				}
			},{
				header: 'PBF',
				dataIndex: 'vendor',
				sortable: true,
				hideable:false,
                menuDisabled:true,
				width: 50
			},{
				header: 'No Faktur',
				dataIndex: 'remark',
				sortable: true,
				width: 40
			}
		])
	});
//	GFPenerimaan.form.Grid.b = new Ext.grid.EditorGridPanel({
//		xtype: 'editorgrid',
//		title: '',
//		store: GFPenerimaan.form.ArrayStore.c,
//		autoScroll: true,
//		columnLines: true,
//		flex: 1,
//		border:false,
//		anchor: '100% 100%',
//		plugins: [new Ext.ux.grid.FilterRow()],
//		selModel: new Ext.grid.RowSelectionModel({
//				singleSelect: true,
//				listeners:{
//					rowselect: function(sm, row, rec){
//						rowSelected_viApotekPenerimaan = undefined;
//						rowSelected_viApotekPenerimaan = GFPenerimaan.form.ArrayStore.c.getAt(row);
//						CurrentData_viApotekPenerimaan
//						CurrentData_viApotekPenerimaan.row = row;
//						CurrentData_viApotekPenerimaan.data = rowSelected_viApotekPenerimaan.data;
//					}
//				}
//		}),
//		listeners:{
//			rowdblclick: function (sm, ridx, cidx){
//				rowSelected_viApotekPenerimaan = GFPenerimaan.form.ArrayStore.c.getAt(ridx);
//				if (rowSelected_viApotekPenerimaan != undefined){
//					setLookUp_viApotekPenerimaan(rowSelected_viApotekPenerimaan.data);
//				}else{
//					setLookUp_viApotekPenerimaan();
//				}
//			}
//		},
//		colModel: new Ext.grid.ColumnModel([
//				new Ext.grid.RowNumberer(),
//			{
//				header		: 'Status Posting',
//				width		: 15,
//				sortable	: false,
//				hideable	: true,
//				hidden		: false,
//				menuDisabled: true,
//				dataIndex	: 'posting',
//				renderer	: function(value, metaData, record, rowIndex, colIndex, store){
//					 switch (value){
//						 case '1':
//							 metaData.css = 'StatusHijau'; 
//							 break;
//						 case '0':
//							 metaData.css = 'StatusMerah';
//							 break;
//					 }
//					 return '';
//				}
//			},{
//				header: 'No. Penerimaan',
//				dataIndex: 'no_obat_in',
//				sortable: true,
//				width: 35
//					
//			},{
//				//xtype:'datecolumn',
//				header:'Tgl Penerimaan',
//				dataIndex: 'tgl_obat_in',	
//				width: 20,
//				sortable: true,
//				hideable:false,
//                menuDisabled:true,
//                renderer: function(v, params, record){
//					return ShowDate(record.data.tgl_obat_in);
//				}
//			},{
//				header: 'PBF',
//				dataIndex: 'vendor',
//				sortable: true,
//				hideable:false,
//                menuDisabled:true,
//				width: 50
//			},{
//				header: 'No Faktur',
//				dataIndex: 'remark',
//				sortable: true,
//				width: 40
//			}
//		]),
//		tbar: {
//			xtype: 'toolbar',
//			id: 'toolbar_viApotekPenerimaan',
//			items: [
//				{
//					xtype: 'button',
//					text: 'Tambah',
//					iconCls: 'add',
//					tooltip: 'Edit Data',
//					handler: function(sm, row, rec){
//						setLookUp_viApotekPenerimaan();
//					}
//				},
//				{
//					xtype: 'button',
//					text: 'Ubah',
//					iconCls: 'Edit_Tr',
//					tooltip: 'Edit Data',
//					handler: function(sm, row, rec){
//						if(rowSelected_viApotekPenerimaan != undefined){
//							setLookUp_viApotekPenerimaan(rowSelected_viApotekPenerimaan.data)
//						}else{
//							Ext.Msg.alert('Information','Data Belum Dipilih.');
//						}
//					}
//				}
//			]
//		},
//		//bbar : bbar_paging(mod_name_viApotekPenerimaan, selectCount_viApotekPenerimaan, dataSource_viApotekPenerimaan),
//		viewConfig: {
//			forceFit: true
//		}
//	});
	GFPenerimaan.form.FormPanel.b = new Ext.FormPanel({
        //region: 'center',
        border: true,
        title: '',
        bodyStyle:'padding:5px; margin-top: -1px;',
        items: [
		{
			layout: 'column',
			border: false,
			items:[
				{
					columnWidth:.98,
					layout: 'absolute',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					height: 60,
					anchor: '100% 100%',
					items:[
						{
							x: 10,
							y: 0,
							xtype: 'label',
							text: 'No. Penerimaan'
						},{
							x: 120,
							y: 0,
							xtype: 'label',
							text: ':'
						},GFPenerimaan.form.TextField.c=new Ext.form.TextField({
							x: 130,
							y: 0,
							xtype: 'textfield',
							name: 'noPenerima',
							fieldLabel: 'No Penerimaan ',
							width: 130,
							tabIndex:1,
							listeners:{ 
								'specialkey' : function(a,e){
									if (e.getKey() == e.ENTER) {
//										GFPenerimaan.func.refresh();
										Q(GFPenerimaan.form.Grid.b).refresh();
								    }
								}
							}
						}),{
							x: 10,
							y: 30,
							xtype: 'label',
							text: 'Tanggal'
						},{
							x: 120,
							y: 30,
							xtype: 'label',
							text: ':'
						},{
							x: 130,
							y: 30,
							xtype: 'datefield',
							id: 'startDate',
							format: 'Y-m-d',
							width: 120,
							tabIndex:3,
							value:now_viApotekPenerimaan,
							listeners:{ 
								'specialkey' : function(a,e){
									if (e.getKey() == e.ENTER) {
//										GFPenerimaan.func.refresh();
										Q(GFPenerimaan.form.Grid.b).refresh();
								    }
								}
							}
						},{
							x: 260,
							y: 30,
							xtype: 'label',
							text: 's/d'
						},{
							x: 288,
							y: 30,
							xtype: 'datefield',
							id: 'lastDate',
							format: 'Y-m-d',
							width: 120,
							tabIndex:4,
							value:now_viApotekPenerimaan,
							listeners:{ 
								'specialkey' : function(a,e){
									if (e.getKey() == e.ENTER) {
//										GFPenerimaan.func.refresh();
										Q(GFPenerimaan.form.Grid.b).refresh();
								    }
								}
							}
						},{
							x: 450,
							y: 0,
							xtype: 'label',
							text: 'PBF'
						},{
							x: 520,
							y: 0,
							xtype: 'label',
							text: ':'
						},
						viCombo_Vendor(),		
						{
							x: 450,
							y: 30,
							xtype: 'label',
							text: 'Posting'
						},{
							x: 520,
							y: 30,
							xtype: 'label',
							text: ':'
						},
						mComboStatusPostingApotekPenerimaan(),
						{
							x: 690,
							y: 30,
							xtype: 'button',
							text: 'Cari',
							iconCls: 'refresh',
							tooltip: 'Cari',
							style:{paddingLeft:'30px'},
							width:150,
							id: 'posting',
							handler: function() {					
//								GFPenerimaan.func.refresh();
								Q(GFPenerimaan.form.Grid.b).refresh();
							}                        
						}
					]
				}
			]
		}
		]	
	})
    var FrmFilterGridDataView_viApotekPenerimaan = new Ext.Panel({
			title: NamaForm_viApotekPenerimaan,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viApotekPenerimaan,
			layout: {
			    type: 'vbox',
			    align : 'stretch'
			},
			closable: true,        
			border: false,  
			margins: '0 5 5 0',
			items: [ GFPenerimaan.form.FormPanel.b,GFPenerimaan.form.Grid.b],
			tbar:[{
		            xtype: 'buttongroup',
		            columns: 21,
		            defaults: {
		            	scale: 'small'
		        	},
		        	frame: false,
		            items:[]
				}
			]
   });
    return FrmFilterGridDataView_viApotekPenerimaan;
}
function setLookUp_viApotekPenerimaan(rowdata){
    var lebar = 985;
    setLookUps_viApotekPenerimaan = new Ext.Window({
		name: 'SetLookUps_viApotekPenerimaan',
        title: NamaForm_viApotekPenerimaan, 
        closeAction: 'destroy',        
        width: 1000,
        height: 605,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viApotekPenerimaan(lebar,rowdata),
        listeners:{
            activate: function(){
				
            },
            afterShow: function(){
                this.activate();
            },
            deactivate: function(){
                rowSelected_viApotekPenerimaan=undefined;
				mNoKunjungan_viApotekPenerimaan = '';
            }
        }
    });
    if(rowdata == undefined){
    	loadMask.show();
    	 $.ajax({
			url:baseURL + "index.php/gudang_farmasi/functionGFPenerimaan/initTransaksi",
			dataType:'JSON',
			type: 'GET',
			success: function(r){
				loadMask.hide();
				if(r.processResult=='SUCCESS'){
					setLookUps_viApotekPenerimaan.show();
				}else{
					Ext.Msg.alert('Error',r.processMessage);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
    }else{
    	loadMask.show();
    	 $.ajax({
 			url:baseURL + "index.php/gudang_farmasi/functionGFPenerimaan/getForEdit",
 			dataType:'JSON',
 			type: 'POST',
 			data: {'no_obat_in':rowdata.no_obat_in},
 			success: function(r){
 				loadMask.hide();
 				if(r.processResult=='SUCCESS'){
 					setLookUps_viApotekPenerimaan.show();
 					GFPenerimaan.form.TextField.b.setValue(r.resultObject.no_obat_in);
 					GFPenerimaan.form.TextField.a.setValue(r.resultObject.kd_vendor);
 					GFPenerimaan.form.ComboBox.b.setValue(r.resultObject.vendor);
 					var dueDate = Date.parseDate(r.resultObject.due_date, "Y-m-d H:i:s");
 					GFPenerimaan.form.DateField.a.setValue(dueDate);
 					var penerimaanDate = Date.parseDate(r.resultObject.tgl_obat_in, "Y-m-d H:i:s");
 					GFPenerimaan.form.DateField.b.setValue(penerimaanDate);
 					GFPenerimaan.form.TextField.d.setValue(r.resultObject.remark);
 					GFPenerimaan.form.TextField.e.setValue(r.resultObject.no_sj);
 					if(r.resultObject.posting==1){
 						GFPenerimaan.form.Button.a.setText('Unposting');
 						GFPenerimaan.form.Panel.a.update('<img src="'+baseURL+'ui/images/icons/16x16/hijau.png"/>');
 					}else{
 						GFPenerimaan.form.Panel.a.update('<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>');
 						GFPenerimaan.form.Button.a.setText('Posting');
 					}
 					for(var i=0, iLen=r.listData.length ; i<iLen ; i++){
 						var records = new Array();
 						records.push(new dsDataGrdJab_viApotekPenerimaan.recordType());
 						dsDataGrdJab_viApotekPenerimaan.add(records);
 						var o=dsDataGrdJab_viApotekPenerimaan.data.items[i].data;
 						o.kd_prd=r.listData[i].kd_prd;
 						o.nama_obat=r.listData[i].nama_obat;
 						o.kd_sat_besar=r.listData[i].kd_sat_besar;
 						o.harga_beli=r.listData[i].hrg_beli_obt;
 						o.jml_in_obat=r.listData[i].jml_in_obt;
 						o.apt_discount=r.listData[i].apt_discount;
 						o.apt_disc_rupiah=r.listData[i].apt_disc_rupiah;
 						o.ppn=r.listData[i].ppn_item;
 						if(r.listData[i].tag==1){
 							o.tag=true;
 						}else{
 							o.tag=false;
 						}
 						o.kd_pabrik=r.listData[i].kd_pabrik;
 						o.pabrik=r.listData[i].pabrik;
 						o.batch=r.listData[i].batch;
 						o.tgl_expire=r.listData[i].tgl_exp;
 						o.no_obat_in=r.listData[i].no_obat_in;
 						o.fractions=r.listData[i].frac;
 						o.qty_b='';
 					}
 					GFPenerimaan.func.refreshNilai();
 				}else{
 					
 					Ext.Msg.alert('Error',r.processMessage);
 				}
 			},
 			error: function(jqXHR, exception) {
 				loadMask.hide();
 				Nci.ajax.ErrorMessage(jqXHR, exception);
 			}
 		});
    }
}

function getFormItemEntry_viApotekPenerimaan(lebar,rowdata){
    GFPenerimaan.form.FormPanel.a = new Ext.FormPanel({
			title: '',
			region: 'north',
			layout: 'form',
			bodyStyle: 'padding:10px 10px 10px 10px',
			anchor: '100%',
			labelWidth: 1,
			autoWidth: true,
			width: lebar,
			border: false,
			items:[
				getItemPanelInputBiodata_viApotekPenerimaan(lebar),
				getItemGridTransaksi_viApotekPenerimaan(lebar),
				{
					xtype: 'compositefield',
					fieldLabel: ' ',
					labelSeparator: '',
					items: [
						GFPenerimaan.form.Panel.a=new Ext.Panel ({
						    region: 'north',
						    border: false,
						    html: '<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>'
						}),
						{
							columnWidth	: .08,
							layout		: 'form',
							anchor		: '100% 8.0001%',
							border		: false,
							html		: " Posted"
						},{
							xtype: 'displayfield',				
							width: 65,								
							value: 'Sub Total :',
							style:{'text-align':'right','margin-left':'465px'}							
						},GFPenerimaan.form.TextField.subTotal= new Ext.form.NumberField({
		                    name: 'subTotal',
							style:{'text-align':'right','margin-left':'465px'},
		                    width: 80,
		                    value: 0,
		                    readOnly: true
		                }),{
							xtype: 'displayfield',	
							width: 50,								
							value: 'Disc(-):',
							style:{'text-align':'right','margin-left':'465px'}
							
						},GFPenerimaan.form.TextField.discount=new Ext.form.NumberField({
		                    name: 'discountTotal',
							style:{'text-align':'right','margin-left':'465px'},
		                    width: 80,
		                    value: 0,
		                    readOnly: true
		                }),{
							xtype: 'displayfield',				
							width: 50,								
							value: 'Jumlah:',
							style:{'text-align':'right','margin-left':'465px'}
						},GFPenerimaan.form.TextField.total=new Ext.form.NumberField({
		                    name: 'jumlah',
							style:{'text-align':'right','margin-left':'465px'},
		                    width: 80,
		                    value: 0,
		                    readOnly: true
		                })												
		            ]
		        },{
					xtype: 'compositefield',
					fieldLabel: ' ',
					labelSeparator: '',
					items: [
						GFPenerimaan.form.CheckBox.b=new Ext.form.Checkbox({
							xtype: 'checkbox',
							name: 'updateHarga',
							disabled: false,
							autoWidth: false,		
							boxLabel: 'Update Semua Harga Beli ',
							width: 200,
							handler: function(a,b){
								for(var i=0,iLen=dsDataGrdJab_viApotekPenerimaan.getCount(); i<iLen ; i++){
									if(a.checked==true){
										dsDataGrdJab_viApotekPenerimaan.data.items[i].data.tag=true;
									}else{
										dsDataGrdJab_viApotekPenerimaan.data.items[i].data.tag=false;
									}
								}
								GFPenerimaan.form.Grid.a.getView().refresh();
							}
						}),{
							xtype: 'displayfield',				
							width: 65,								
							value: 'Materai :',
							fieldLabel: 'Label',
							style:{'text-align':'right','margin-left':'324px'}							
						},GFPenerimaan.form.TextField.materai=new Ext.form.NumberField({
							style:{'text-align':'right','margin-left':'324px'},
		                    width: 80,
		                    name: 'materai',
		                    value: 0,
		                    readOnly: false,
		                    listeners:{
		                    	blur:function(){
		                    		GFPenerimaan.func.refreshNilai();
		                    	}
		                    }
		                }),{
							xtype: 'displayfield',				
							width: 50,								
							value: 'Ppn(+):',
							fieldLabel: 'Label',
							style:{'text-align':'right','margin-left':'324px'}
							
						},GFPenerimaan.form.TextField.ppn=new Ext.form.NumberField({
							style:{'text-align':'right','margin-left':'324px'},
		                    width: 80,
		                    name:'ppn',
		                    value: 0,
		                    readOnly: true
		                })
//		                {
//							xtype:'button',
//							text:'Acc. Approval',
//							width:70,
//							style:{'text-align':'right','margin-top':'3px','margin-left':'380px'},
//							hideLabel:true,
//							id: 'btnAccApp_viApotekPenerimaan',
//							handler:function(){
//							}   
//						},
		            ]
		        }
			],
			fileUpload: true,
			tbar:{
				xtype: 'toolbar',
				items: [
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						id: 'btnAdd_viApotekPenerimaan',
						handler: function(){
							if(GFPenerimaan.form.Button.a.text=='Posting'){
								var records = new Array();
								records.push(new dsDataGrdJab_viApotekPenerimaan.recordType());
								dsDataGrdJab_viApotekPenerimaan.add(records);
								GFPenerimaan.func.refreshNilai();
							}else{
								Ext.Msg.alert('Informasi','Menambah gagal karena data sudah diPosting, Harap Unposting terlebih dahulu.');
							}
						}
					},{
						xtype: 'tbseparator'
					},{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						handler: function(){
							if(GFPenerimaan.form.Button.a.text=='Posting'){
								GFPenerimaan.func.save();
							}else{
								Ext.Msg.alert('Informasi','Penyimpanan gagal karena data sudah diPosting, Harap Unposting jika ingin Menyimpan.');
							}
						}
					},{
						xtype: 'tbseparator'
					},{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						handler: function(){
							if(GFPenerimaan.form.Button.a.text=='Posting'){
								GFPenerimaan.func.save(function(){
									setLookUps_viApotekPenerimaan.close();
								});
							}else{
								Ext.Msg.alert('Informasi','Penyimpanan gagal karena data sudah diPosting, Harap Unposting jika ingin Menyimpan.');
							}
						}
					},{
						xtype: 'tbseparator'
					},{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						handler: function(){
							if(GFPenerimaan.form.Button.a.text=='Posting'){
								var line	= GFPenerimaan.form.Grid.a.getSelectionModel().selection.cell[0];
								var o=dsDataGrdJab_viApotekPenerimaan.getRange()[line].data;
								if(o.no_obat_in != undefined && o.no_obat_in!=''){
									if(dsDataGrdJab_viApotekPenerimaan.getCount()>1){
										Ext.Msg.prompt('Name', 'Apakah Data Ini Akan Dihapus?', function(btn, text){
										    if (btn == 'ok'){
										    	loadMask.show();
										    	$.ajax({
													url:baseURL + "index.php/gudang_farmasi/functionGFPenerimaan/deleteDetail",
													dataType:'JSON',
													type: 'POST',
													data:{no_obat_in:o.no_obat_in,line:(line+1)},
													success: function(r){
														loadMask.hide();
														if(r.processResult=='SUCCESS'){
															Ext.Msg.alert('Sukses','Data Berhasi Dihapus.');
															dsDataGrdJab_viApotekPenerimaan.removeAt(line);
															GFPenerimaan.form.Grid.a.getView().refresh();
														}else{
															Ext.Msg.alert('Gagal',r.processMessage);
														}
													},
													error: function(jqXHR, exception) {
														loadMask.hide();
														Nci.ajax.ErrorMessage(jqXHR, exception);
													}
												});
										    }
										});
									}else{
										Ext.Msg.alert('Gagal','Data tidak bisa dihapus karena minimal mempunyai 1 data.');
									}
								}else{
									Ext.Msg.confirm('Konfirmasi', 'Apakah ingin menghapus data ini?', function (id, value) { 
										if (id === 'yes') { 
											dsDataGrdJab_viApotekPenerimaan.removeAt(line);
											GFPenerimaan.form.Grid.a.getView().refresh();
										} 
									}, this); 
								}
							}else{
								Ext.Msg.alert('Error','Data Sudah Diposting, Harap Unposting Jika ingin Menghapus Data.');
							}
						}
					},{
						xtype:'tbseparator'
					},GFPenerimaan.form.Button.a=new Ext.Button({
						text: 'Posting',
						iconCls: 'gantidok',
						handler: function(){
							var param=$('#'+GFPenerimaan.form.FormPanel.a.id).find('form').eq(0).serializeArray(),e=false;
							if(GFPenerimaan.form.TextField.a.getValue()==''){
								e=true;
								Ext.Msg.alert('Error','Harap Isi PBF.');
								return;
							}
							if(dsDataGrdJab_viApotekPenerimaan.getCount()==0){
								e=true;
								Ext.Msg.alert('Error','Isi Produk Obat.');
								return;
							}
							if(GFPenerimaan.form.TextField.total.getValue()==''){
								e=true;
								Ext.Msg.alert('Error','Isi Data Obat Dengan Benar.');
								return;
							}
							param.push({name:'count',value:dsDataGrdJab_viApotekPenerimaan.getCount()});
							for(var i=0, iLen=dsDataGrdJab_viApotekPenerimaan.getCount(); i<iLen ; i++){
								var o=dsDataGrdJab_viApotekPenerimaan.data.items[i].data;
								param.push({name:'kd_prd-'+i,value: o.kd_prd});
								param.push({name:'jml_in_obt-'+i,value: o.jml_in_obat});
								param.push({name:'hrg_beli_obt-'+i,value: o.harga_beli});
								param.push({name:'tgl_exp-'+i,value: GFPenerimaan.func.convert(o.tgl_expire)});
								param.push({name:'apt_discount-'+i,value: o.apt_discount});
								param.push({name:'ppn_item-'+i,value: o.ppn});
								param.push({name:'apt_disc_rupiah-'+i,value: o.apt_disc_rupiah});
								param.push({name:'frac-'+i,value: o.fractions});
								param.push({name:'boxqty-'+i,value: o.qty_b});
								if(o.tag != undefined && o.tag == true){
									param.push({name:'tag-'+i,value: 1});
								}else{
									param.push({name:'tag-'+i,value: 0});
								}
								if(o.batch != undefined){
									param.push({name:'batch-'+i,value: o.batch});
								}else{
									param.push({name:'batch-'+i,value:''});
								}
								if(o.kd_pabrik != undefined){
									param.push({name:'kd_pabrik-'+i,value:o.kd_pabrik});
								}else{
									param.push({name:'kd_pabrik-'+i,value:''});
								}
							}
							if(this.text=='Posting'){
								Ext.Msg.confirm('Konfirmasi', 'Apakah Akan diPosting ?', function (id, value) { 
									if (id === 'yes') { 
										if(GFPenerimaan.form.TextField.b.getValue()==''){
											loadMask.show();
											$.ajax({
												url:baseURL + "index.php/gudang_farmasi/functionGFPenerimaan/postingSave",
												dataType:'JSON',
												type: 'POST',
												data:param,
												success: function(r){
													loadMask.hide();
													if(r.processResult=='SUCCESS'){
														GFPenerimaan.form.TextField.b.setValue(r.resultObject.code);
														for(var i=0; i<dsDataGrdJab_viApotekPenerimaan.getCount() ; i++){
															var o=dsDataGrdJab_viApotekPenerimaan.getRange()[i].data;
															o.no_obat_in=r.resultObject.code;
														}
														GFPenerimaan.form.Button.a.setText('Unposting');
														GFPenerimaan.form.Panel.a.update('<img src="'+baseURL+'ui/images/icons/16x16/hijau.png"/>');
														Ext.Msg.alert('Sukses','Data Berhasi Disimpan dan diPosting.');
													}else{
														Ext.Msg.alert('Gagal',r.processMessage);
													}
												},
												error: function(jqXHR, exception) {
													loadMask.hide();
													Nci.ajax.ErrorMessage(jqXHR, exception);
												}
											});
										}else{
											loadMask.show();
											$.ajax({
												url:baseURL + "index.php/gudang_farmasi/functionGFPenerimaan/postingUpdate",
												dataType:'JSON',
												type: 'POST',
												data:param,
												success: function(r){
													loadMask.hide();
													if(r.processResult=='SUCCESS'){
														Ext.Msg.alert('Sukses','Data Berhasi Diubah dan diPosting.');
														for(var i=0; i<dsDataGrdJab_viApotekPenerimaan.getCount() ; i++){
															var o=dsDataGrdJab_viApotekPenerimaan.getRange()[i].data;
															o.no_obat_in=GFPenerimaan.form.TextField.b.getValue();
														}
														GFPenerimaan.form.Button.a.setText('Unposting');
														GFPenerimaan.form.Panel.a.update('<img src="'+baseURL+'ui/images/icons/16x16/hijau.png"/>');
													}else{
														Ext.Msg.alert('Gagal',r.processMessage);
													}
												},
												error: function(jqXHR, exception) {
													loadMask.hide();
													Nci.ajax.ErrorMessage(jqXHR, exception);
												}
											});
										}
									} 
								}, this); 
							}else{
								Ext.Msg.confirm('Konfirmasi', 'Apakah Akan diUnposting ?', function (id, value) { 
									if (id === 'yes') { 
										loadMask.show();
										$.ajax({
											url:baseURL + "index.php/gudang_farmasi/functionGFPenerimaan/unposting",
											dataType:'JSON',
											type: 'POST',
											data:{no_obat_in:GFPenerimaan.form.TextField.b.getValue()},
											success: function(r){
												loadMask.hide();
												if(r.processResult=='SUCCESS'){
													Ext.Msg.alert('Sukses','Data Berhasi diUnposting.');
													for(var i=0; i<dsDataGrdJab_viApotekPenerimaan.getCount() ; i++){
														var o=dsDataGrdJab_viApotekPenerimaan.getRange()[i].data;
														o.no_obat_in=GFPenerimaan.form.TextField.b.getValue();
													}
													GFPenerimaan.form.Button.a.setText('Posting');
													GFPenerimaan.form.Panel.a.update('<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>');
												}else{
													Ext.Msg.alert('Gagal',r.processMessage);
												}
											},
											error: function(jqXHR, exception) {
												loadMask.hide();
												Nci.ajax.ErrorMessage(jqXHR, exception);
											}
										});
									} 
								}, this); 
							}
						}
					}),{
						xtype:'tbseparator'
					}
				]
			}
	})
    return GFPenerimaan.form.FormPanel.a;
}

function getItemPanelInputBiodata_viApotekPenerimaan(lebar) {
    var items ={
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		labelWidth: 100,
		autoWidth: true,
		border:true,
		items:[			
			{
                xtype: 'compositefield',
                fieldLabel: 'No. Penerimaan',
                anchor: '100%',
                width: 250,
                items: [                    
					GFPenerimaan.form.TextField.b=new Ext.form.TextField({
						width : 120,	
						name: 'noPenerima',
						readOnly: true,
						listeners:{
							'specialkey': function() {
								if (Ext.EventObject.getKey() === 13){
								}
							}
						}
					}), {
						xtype: 'displayfield',
						flex: 1,
						width: 70,
						name: '',
						value: 'PBF :',
						fieldLabel: 'Label'
					},					
					viCombo_VendorLookup(),
					GFPenerimaan.form.TextField.a=new Ext.form.TextField({
						name: 'kd_vendor',
						hidden: true
					}),
					{
						xtype: 'displayfield',
						flex: 1,
						width: 90,
						name: '',
						value: 'Jatuh Tempo :',
						fieldLabel: 'Label'
					},GFPenerimaan.form.DateField.a=new Ext.form.DateField({
						name:'jatuhTempo',
						value: now_viApotekPenerimaan,
						format: 'd/M/Y',
						width: 120,
						listeners:{ 
							'specialkey' : function(){
								if (Ext.EventObject.getKey() === 13) {
//									datarefresh_viApotekPenerimaan();								
								} 						
							}
						}
					})
					
                ]
            },{
				xtype: 'compositefield',
				fieldLabel: 'Tanggal',
				anchor: '100%',
				width: 199,
				items: [
					GFPenerimaan.form.DateField.b=new Ext.form.DateField({
						xtype: 'datefield',
						name: 'penerimaan',
						value: now_viApotekPenerimaan,
						format: 'd/M/Y',
						readOnly: true,
						width: 120,
						listeners:{ 
							'specialkey' : function(){
								if (Ext.EventObject.getKey() === 13) {
									datarefresh_viApotekPenerimaan();								
								} 						
							}
						}
					}),{
						xtype: 'displayfield',
						flex: 1,
						width: 70,
						name: '',
						value: 'No Faktur :',
						fieldLabel: 'Label'
					},GFPenerimaan.form.TextField.d= new Ext.form.TextField({
						xtype: 'textfield',
						flex: 1,
						width : 360,	
						name: 'noFaktur',
						listeners:{
							'specialkey': function(){
								if (Ext.EventObject.getKey() === 13) {
									
								};
							}
						}
					}),{
						xtype: 'displayfield',
						flex: 1,
						width: 90,
						name: '',
						value: 'No Surat Jalan :'
					},GFPenerimaan.form.TextField.e= new Ext.form.TextField({
						xtype: 'textfield',
						flex: 1,
						width : 100,	
						name: 'noSuratBayar',
						listeners:{
							'specialkey': function() {
								if (Ext.EventObject.getKey() === 13) {
								};
							}
						}
					}),{
						xtype:'button',
						text:'Bayar',
						width:70,
						hideLabel:true,
						id: 'btnBayar_viApotekPenerimaan',
						handler:function(){
						}   
					}
				]
			}
		]
	};
    return items;
};

function getItemGridTransaksi_viApotekPenerimaan(lebar) {
    var items ={
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'margin-top: -1px;',
		border:false,
		width: lebar-80,
		height: 400,//225, 
	    items:[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:[
					gridDataViewEdit_viApotekPenerimaan()
				]	
			}
		]
	};
    return items;
}
GFPenerimaan.func.refreshNilai=function(){
	var subTotal=0,
		discount=0,
		ppn=0;
	
	for(var i=0; i<dsDataGrdJab_viApotekPenerimaan.getCount() ; i++){
		var subJumlah=0;
		var o=dsDataGrdJab_viApotekPenerimaan.getRange()[i].data;
		if(o.fractions == undefined || o.fractions==''){
			o.fractions=0;
		}
		if(o.jml_in_obat == undefined || o.jml_in_obat==''){
			o.jml_in_obat=0;
		}
		if(o.qty_b == undefined || o.qty_b==''){
			o.qty_b=0;
		}
		if(o.harga_beli == undefined || o.harga_beli==''){
			o.harga_beli=0;
		}
		
		subJumlah=parseInt(o.jml_in_obat)*parseInt(o.harga_beli);
		subTotal+=parseInt(o.jml_in_obat)*parseInt(o.harga_beli);
		if(isNaN(o.apt_disc_rupiah)){
			o.apt_disc_rupiah=0;
		}
		if(isNaN(o.apt_discount)){
			o.apt_discount=0;
		}
		discount+=(((subJumlah/100)*parseInt(o.apt_discount))+parseInt(o.apt_disc_rupiah));
		if(isNaN(subJumlah)){
			subJumlah=0;
		}else{
			subJumlah=subJumlah-(((subJumlah/100)*parseInt(o.apt_discount))+parseInt(o.apt_disc_rupiah));
		}
		o.ppn=10;
		o.jumlah=subJumlah;
		ppn+=((subJumlah/100)*10);
		o.sub_total=subJumlah+((subJumlah/100)*10);
	}
	
	GFPenerimaan.form.TextField.subTotal.setValue(subTotal);
	GFPenerimaan.form.TextField.discount.setValue(discount);
	GFPenerimaan.form.TextField.ppn.setValue(ppn);
	var total=(subTotal+ppn+GFPenerimaan.form.TextField.materai.getValue())-discount;
	GFPenerimaan.form.TextField.total.setValue(total);
	GFPenerimaan.form.Grid.a.getView().refresh();
}
function gridDataViewEdit_viApotekPenerimaan(){
    chkSelected_viApotekPenerimaan = new Ext.grid.CheckColumn({
		id: 'chkSelected_viApotekPenerimaan',
		header: '',
		align: 'center',						
		dataIndex: 'SELECTED',			
		width: 20
	});
    GFPenerimaan.form.CheckBox.a = new Ext.grid.CheckColumn({
		header: 'Tag',
		align: 'center',						
		dataIndex: 'tag',			
		width: 30
	});
    dsDataGrdJab_viApotekPenerimaan= new WebApp.DataStore({
        fields: ['nama_obat','kd_prd','kd_sat_besar','harga_beli','jml_in_obat','qty_b','apt_discount','apt_disc_rupiah','jumlah','ppn','sub_total','tag','kd_pabrik','batch','pabrik','tgl_expire','no_obat_in']
    });
    GFPenerimaan.form.Grid.a = new Ext.grid.EditorGridPanel({
        store: dsDataGrdJab_viApotekPenerimaan,
        height		: 395,
        columnLines: true,
        autoShow: false,
        stripeRows: true,
        cm: new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(),	
			{			
				dataIndex: 'nama_obat',
				header: 'Uraian',
				width: 200,
				editor:new Nci.form.Combobox.autoComplete({
					store	: GFPenerimaan.form.ArrayStore.a,
					select	: function(a,b,c){
						var line	= GFPenerimaan.form.Grid.a.getSelectionModel().selection.cell[0];
						dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.nama_obat=b.data.nama_obat;
						dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.kd_prd=b.data.kd_prd;
						dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.kd_sat_besar=b.data.kd_sat_besar;
						dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.fractions=b.data.fractions;
						dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.harga_beli=b.data.harga_beli;
						dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.kd_pabrik=b.data.kd_pabrik;
						dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.pabrik=b.data.pabrik;
						dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.jml_in_obat='';
						dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.qty_b='';
						GFPenerimaan.func.refreshNilai();
					},
					insert	: function(o){
						return {
							kd_prd        	:o.kd_prd,
							nama_obat 			: o.nama_obat,
							kd_sat_besar		: o.kd_sat_besar,
							fractions		: o.fractions,
							harga_beli		: o.harga_beli,
							kd_pabrik		: o.kd_pabrik,
							pabrik			: o.pabrik,
							text				:  '<table style="font-size: 11px;"><tr><td width="50">'+o.kd_prd+'</td><td width="200">'+o.nama_obat+'</td><td width="100">'+parseInt(o.harga_beli).toLocaleString('id', { style: 'currency', currency: 'IDR' })+'</td></tr></table>'
						}
					},
					url		: baseURL + "index.php/gudang_farmasi/functionGFPenerimaan/getObat",
					valueField: 'nama_obat',
					displayField: 'text',
					listWidth: 350,
					blur: function(){
						GFPenerimaan.func.refreshNilai();
					}
				})
			},{
				dataIndex: 'kd_sat_besar',
				header: 'Sat B',
				width: 40
			},{
				dataIndex: 'qty_b',
				header: 'Qty B',
				align:'right',
				xtype : 'numbercolumn',
				width: 50,
				editor: new Ext.form.NumberField({
					listeners:{
						blur: function(a){
							var line	= this.indeks;
							var b=dsDataGrdJab_viApotekPenerimaan.getRange()[line].data;
							b.qty_b=a.getValue();
							b.jml_in_obat=a.getValue()*b.fractions;
							b.a=false;
							GFPenerimaan.func.refreshNilai();
						},
						focus:function(){
							this.indeks=GFPenerimaan.form.Grid.a.getSelectionModel().selection.cell[0];
						}
					}				
				})
			},{
				dataIndex: 'fractions',
				xtype : 'numbercolumn',
				header: 'Frac',
				align:'right',
				width: 50,
				decimalPrecision: 0
			},{
				dataIndex: 'harga_beli',
				xtype : 'numbercolumn',
				header: 'Harga',
				align:'right',
				width: 70,
				editor: new Ext.form.NumberField({
					decimalPrecision: 0,
					listeners:{
						blur: function(a){
							var line	= this.indeks;
							dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.harga_beli=a.getValue();
							GFPenerimaan.func.refreshNilai();
						},
						focus:function(){
							this.indeks=GFPenerimaan.form.Grid.a.getSelectionModel().selection.cell[0];
						}
					}				
				})
			},{
				dataIndex: 'jml_in_obat',
				xtype : 'numbercolumn',
				decimalPrecision: 0,
				align:'right',
				header: 'Qty',
				width: 50,
				editor: new Ext.form.NumberField({
					decimalPrecision: 0,
					listeners:{
						blur: function(a){
							var line	= this.indeks;
							var b=dsDataGrdJab_viApotekPenerimaan.getRange()[line].data;
							if(b.fractions != 0){
								b.qty_b=parseInt(a.getValue())/parseInt(b.fractions);
							}else{
								b.qty_b=0;
							}
							b.jml_in_obat=a.getValue();
							b.a=true;
							GFPenerimaan.func.refreshNilai();
						},
						focus:function(){
							this.indeks=GFPenerimaan.form.Grid.a.getSelectionModel().selection.cell[0];
						}
					}				
				})	
			},{
				dataIndex: 'apt_discount',
				xtype : 'numbercolumn',
				header: 'Disc%',
				align:'right',
				width: 50,
				editor: new Ext.form.NumberField({
					listeners:{
						blur: function(a){
							var line	= this.indeks;
							if(a.getValue()>100){
								dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.apt_discount=100;
							}else{
								dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.apt_discount=a.getValue();
							}
							
							GFPenerimaan.func.refreshNilai();
						},
						focus:function(){
							this.indeks=GFPenerimaan.form.Grid.a.getSelectionModel().selection.cell[0];
						}
					}				
				})	
			},{
				dataIndex: 'apt_disc_rupiah',
				xtype : 'numbercolumn',
				header: 'Disc Rp',
				align:'right',
				width: 70,
				editor: new Ext.form.NumberField({
					listeners:{
						blur: function(a){
							var line	= this.indeks;
							dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.apt_disc_rupiah=a.getValue();
							GFPenerimaan.func.refreshNilai();
						},
						focus:function(){
							this.indeks=GFPenerimaan.form.Grid.a.getSelectionModel().selection.cell[0];
						}
					}				
				})
			},{
				dataIndex: 'jumlah',
				xtype : 'numbercolumn',
				header: 'Jumlah',
				align:'right',
				width: 70
			},{
				dataIndex: 'ppn',
				xtype : 'numbercolumn',
				header: 'Ppn%',
				align:'right',
				width: 40
			},{
				dataIndex: 'sub_total',
				xtype : 'numbercolumn',
				header: 'Sub Total',
				width: 70,
				align:'right'
			},{
				dataIndex: 'tgl_expire',
				header: 'Tgl Expire',
				width: 80,
				editor: new Ext.form.DateField({
					format: 'd/m/Y',
					width: 80,
					editable: true
				})
			},GFPenerimaan.form.CheckBox.a,{
				dataIndex: 'batch',
				header: 'Batch',
				sortable: true,
				width: 60,
				editor: new Ext.form.TextField()	
			},{
				dataIndex: 'pabrik',
				header: 'Pabrik',
				sortable: true,
				width: 150,
				editor: new Nci.form.Combobox.autoComplete({
					store	: GFPenerimaan.form.ArrayStore.b,
					select	: function(a,b,c){
						var line	= GFPenerimaan.form.Grid.a.getSelectionModel().selection.cell[0];
						dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.kd_pabrik=b.data.kd_pabrik;
						dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.pabrik=b.data.pabrik;
						GFPenerimaan.form.Grid.a.getView().refresh();
					},
					insert	: function(o){
						return {
							kd_pabrik        	:o.kd_pabrik,
							pabrik 			: o.pabrik,
							text				:  '<table style="font-size: 11px;"><tr><td width="50">'+o.kd_pabrik+'</td><td width="200">'+o.pabrik+'</td></tr></table>'
						}
					},
					url		: baseURL + "index.php/gudang_farmasi/functionGFPenerimaan/getPabrik",
					valueField: 'pabrik',
					displayField: 'text',
					listWidth: 200
				})
			}
        ]),
        plugins:[chkSelected_viApotekPenerimaan,GFPenerimaan.form.CheckBox.a]
    });    
    return GFPenerimaan.form.Grid.a;
}

function mComboStatusPostingApotekPenerimaan(){
	var cboStatusPostingApotekPenerimaan = new Ext.form.ComboBox({
        id:'cboStatusPostingApotekPenerimaan',
        x: 530,
        y: 30,
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:true,
        mode: 'local',
        width: 110,
        emptyText:'',
        fieldLabel: 'JENIS',
		tabIndex:5,
		value: 'Semua',
        store: new Ext.data.ArrayStore({
            id: 0,
            fields:
            [
                'Id',
                'displayText'
            ],
            data: [[1, 'Semua'],[2, 'Posting'], [3, 'Belum Posting']]
		}),
        valueField: 'Id',
        displayField: 'displayText',
        value:selectCountStatusPostingApotekPenerimaan,
        listeners:{
			'select': function(a,b,c){
				selectCountStatusPostingApotekPenerimaan=b.data.displayText ;

			}
        }
	});
	return cboStatusPostingApotekPenerimaan;
};

function viCombo_Vendor(){
    var Field_Vendor = ['KD_VENDOR', 'VENDOR'];
    ds_Vendor = new WebApp.DataStore({fields: Field_Vendor});
    ds_Vendor.load({
        params:{
                Skip: 0,
                Take: 1000,
                Sort: 'KD_VENDOR',
                Sortdir: 'ASC',
                target: 'ComboPBF',
                param: ""
            }
    });
    GFPenerimaan.form.ComboBox.a = new Ext.form.ComboBox({
			x:530,
			y:0,
            flex: 1,
			id: 'cbo_Vendor',
			name:'pbf',
            fieldLabel: 'Vendor',
			valueField: 'KD_VENDOR',
            displayField: 'VENDOR',
			store: ds_Vendor,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:190,
			tabIndex:0,
			listeners:{ 
				'select': function(a,b,c){
					selectSetVendor=b.data.displayText ;
					Q(GFPenerimaan.form.Grid.b).refresh();
				},
				'specialkey' : function(){
					if (Ext.EventObject.getKey() === 13) {
//						GFPenerimaan.func.refresh();
						Q(GFPenerimaan.form.Grid.b).refresh();
					} 						
				}
			}
        }
    )    
    return GFPenerimaan.form.ComboBox.a;
}

function viCombo_VendorLookup(){
    GFPenerimaan.form.ComboBox.b = new Ext.form.ComboBox({
            flex: 1,
            name: 'pbf',
            fieldLabel: 'Vendor',
			valueField: 'KD_VENDOR',
            displayField: 'VENDOR',
			store: ds_Vendor,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:190,
			tabIndex:0,
			listeners:{ 
				'select': function(a,b,c){
					selectSetVendor=b.data.displayText ;
					GFPenerimaan.form.TextField.a.setValue(b.data.KD_VENDOR);
//					Q(GFPenerimaan.form.Grid.b).refresh();
				},
				'specialkey' : function(){
					if (Ext.EventObject.getKey() === 13) {
//						Q(GFPenerimaan.form.Grid.b).refresh();
					} 						
				}
			}
        }
    )    
    return GFPenerimaan.form.ComboBox.b;
}