// Data Source ExtJS # --------------

/**
*	Nama File 		: TRApotekPerencanaan.js
*	Menu 			: 
*	Model id 		: 
*	Keterangan 		: Perencanaan / Request Order 
*	Di buat tanggal : 21 Agustus 2014
*	Oleh 			: SDY_RI
*/

// Deklarasi Variabel pada Apotek Perencanaan # --------------

var dataSource_viApotekPerencanaan;
var selectCount_viApotekPerencanaan=50;
var NamaForm_viApotekPerencanaan="Request Order / Perencanaan ";
var mod_name_viApotekPerencanaan="viApotekPerencanaan";
var now_viApotekPerencanaan= new Date();
var addNew_viApotekPerencanaan;
var rowSelected_viApotekPerencanaan;
var setLookUps_viApotekPerencanaan;
var mNoKunjungan_viApotekPerencanaan='';

var CurrentData_viApotekPerencanaan =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viApotekPerencanaan(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

// End Deklarasi Variabel pada Apotek Perencanaan # --------------

// Start Project Apotek Perencanaan # --------------

// --------------------------------------- # Start Function # ---------------------------------------
// ## Silahkan tempatkan semua fungsi eksekusi disini

// --------------------------------------- # End Function # ---------------------------------------

// --------------------------------------- # Start Form # ---------------------------------------

/**
*	Function : dataGrid_viApotekPerencanaan
*	
*	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
*/

function dataGrid_viApotekPerencanaan(mod_id_viApotekPerencanaan)
{	
    // Field kiriman dari Project Net.
    var FieldMaster_viApotekPerencanaan = 
	[
		 'NO_KUNJUNGAN', 'KD_KELOMPOK', 'KD_UNIT', 'KD_DOKTER', 'KD_CUSTOMER', 'KD_PASIEN', 
		 'TGL_KUNJUNGAN','JAM_KUNJUNGAN', 'TINGGI_BADAN', 'BERAT_BADAN', 'TEKANAN_DRH', 
		 'NADI','ALERGI', 'KELUHAN', 'RUJUK_RAD', 'RUJUK_LAB', 'TAHAP_PROSES', 'PASIEN_BARU',
		 'NAMA_UNIT','KELOMPOK', 'DOKTER', 'CUSTOMER', 'PS_BARU','KD_PENDIDIKAN','KD_STS_MARITAL', 
		 'KD_AGAMA','KD_PEKERJAAN','NAMA','TEMPAT_LAHIR','TGL_LAHIR','JENIS_KELAMIN','ALAMAT', 
		 'NO_TELP','NO_HP','GOL_DARAH','PENDIDIKAN','STS_MARITAL','AGAMA','PEKERJAAN','JNS_KELAMIN',
		 'TAHUN','BULAN','HARI'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viApotekPerencanaan = new WebApp.DataStore
	({
        fields: FieldMaster_viApotekPerencanaan
    });
    
    // Grid Apotek Perencanaan # --------------
	var GridDataView_viApotekPerencanaan = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viApotekPerencanaan,
			autoScroll: true,
			columnLines: true,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viApotekPerencanaan = undefined;
							rowSelected_viApotekPerencanaan = dataSource_viApotekPerencanaan.getAt(row);
							CurrentData_viApotekPerencanaan
							CurrentData_viApotekPerencanaan.row = row;
							CurrentData_viApotekPerencanaan.data = rowSelected_viApotekPerencanaan.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viApotekPerencanaan = dataSource_viApotekPerencanaan.getAt(ridx);
					if (rowSelected_viApotekPerencanaan != undefined)
					{
						setLookUp_viApotekPerencanaan(rowSelected_viApotekPerencanaan.data);
					}
					else
					{
						setLookUp_viApotekPerencanaan();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Apotek perencanaan
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colNoMedrec_viApotekPerencanaan',
						header: 'Ro Number',
						dataIndex: 'RO_NUMBER',
						sortable: true,
						width: 35,
						filter:
						{
							type: 'int'
						}
					},
					//-------------- ## --------------
					{
						id: 'colTglRO_viApotekPerencanaan',
						header:'Tgl RO',
						dataIndex: 'TGL_RO',						
						width: 50,
						sortable: true,
						// format: 'd/M/Y',
						filter: {},
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.TGL_RO);
						}
					},
					//-------------- ## --------------
					{
						id: 'colKeterangan_viApotekPerencanaan',
						header: 'Keterangan',
						dataIndex: 'KETERANGAN',
						sortable: true,
						width: 60,
						filter:
						{}
					}
					//-------------- ## --------------
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viApotekPerencanaan',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viApotekPerencanaan',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viApotekPerencanaan != undefined)
							{
								setLookUp_viApotekPerencanaan(rowSelected_viApotekPerencanaan.data)
							}
							else
							{								
								setLookUp_viApotekPerencanaan();
							}
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			bbar : bbar_paging(mod_name_viApotekPerencanaan, selectCount_viApotekPerencanaan, dataSource_viApotekPerencanaan),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viApotekPerencanaan = new Ext.Panel
    (
		{
			title: NamaForm_viApotekPerencanaan,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viApotekPerencanaan,
			region: 'center',
			layout: 'form', 
			closable: true,        
			border: false,  
			margins: '0 5 5 0',
			items: [GridDataView_viApotekPerencanaan],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viApotekPerencanaan,
		            columns: 9,
		            defaults: {
		                scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
			            { 
							xtype: 'tbtext', 
							text: 'Ro. Number : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},
						{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_RoNumber_viApotekPerencanaan',
							emptyText: 'Ro. Number',
							width: 100,
							height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										
									} 						
								}
							}
						},								
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Tgl Ro : ', 
							style:{'text-align':'right'},
							width: 50,
							height: 25
						},
						{
							xtype: 'datefield',
							id: 'txtfilterTglAwal_viApotekPerencanaan',
							value: now_viApotekPerencanaan,
							format: 'd/M/Y',
							width: 120,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viApotekPerencanaan();								
									} 						
								}
							}
						},
						{ 
							xtype: 'tbtext', 
							text: ' s.d ', 
							style:{'text-align':'center'},
							width: 30,
							height: 25
						},																		
						//-------------- ## --------------						
						{
							xtype: 'datefield',
							id: 'txtfilterTglKunjAkhir_viApotekPerencanaan',
							value: now_viApotekPerencanaan,
							format: 'd/M/Y',
							width: 120,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viApotekPerencanaan();								
									} 						
								}
							}
						},	
						
						//-------------- ## --------------
						{
							xtype: 'button',
							text: 'Cari',
							iconCls: 'refresh',
							tooltip: 'Cari',
							style:{paddingLeft:'30px'},
							//rowspan: 3,
							width:150,
							id: 'BtnFilterGridDataView_viApotekPerencanaan',
							handler: function() 
							{					
								DfltFilterBtn_viApotekPerencanaan = 1;
								DataRefresh_viApotekPerencanaan(getCriteriaFilterGridDataView_viApotekPerencanaan());
							}                        
						},
						//-------------- ## --------------
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viApotekPerencanaan;
    //-------------- # End form filter # --------------
}
// End Function dataGrid_viApotekPerencanaan # --------------

/**
*	Function : setLookUp_viApotekPerencanaan
*	
*	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
*/

function setLookUp_viApotekPerencanaan(rowdata)
{
    var lebar = 985;
    setLookUps_viApotekPerencanaan = new Ext.Window
    (
    {
        id: 'SetLookUps_viApotekPerencanaan',
		name: 'SetLookUps_viApotekPerencanaan',
        title: NamaForm_viApotekPerencanaan, 
        closeAction: 'destroy',        
        width: 1000,
        height: 605,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viApotekPerencanaan(lebar,rowdata), //1
        listeners:
        {
            activate: function()
            {
				
            },
            afterShow: function()
            {
                this.activate();
            },
            deactivate: function()
            {
                rowSelected_viApotekPerencanaan=undefined;
                //datarefresh_viApotekPerencanaan();
				mNoKunjungan_viApotekPerencanaan = '';
            }
        }
    }
    );

    setLookUps_viApotekPerencanaan.show();
    if (rowdata == undefined)
    {
        // dataaddnew_viApotekPerencanaan();
		// Ext.getCmp('btnDelete_viApotekPerencanaan').disable();	
    }
    else
    {
        // datainit_viApotekPerencanaan(rowdata);
    }
}
// End Function setLookUpGridDataView_viApotekPerencanaan # --------------

/**
*	Function : getFormItemEntry_viApotekPerencanaan
*	
*	Sebuah fungsi untuk menampilkan penataan menu dan isian form edit
*/

function getFormItemEntry_viApotekPerencanaan(lebar,rowdata)
{
    var pnlFormDataBasic_viApotekPerencanaan = new Ext.FormPanel
    (
		{
			title: '',
			region: 'north',
			layout: 'form',
			bodyStyle: 'padding:10px 10px 10px 10px',
			anchor: '100%',
			width: lebar,
			border: false,
			//-------------- #items# --------------
			items:
			[
				getItemPanelInputPenerimaan_viApotekPerencanaan(lebar),
				//-------------- ## -------------- 				
				getItemGridTransaksi_viApotekPerencanaan(lebar),
				//-------------- ## --------------
        
			],
			//-------------- #End items# --------------
			fileUpload: true,
			// Tombol pada tollbar Edit Data Pasien
			tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						id: 'btnAdd_viApotekPerencanaan',
						handler: function(){
							dataaddnew_viApotekPerencanaan();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viApotekPerencanaan',
						handler: function()
						{
							datasave_viApotekPerencanaan(addNew_viApotekPerencanaan);
							datarefresh_viApotekPerencanaan();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						id: 'btnSimpanExit_viApotekPerencanaan',
						handler: function()
						{
							var x = datasave_viApotekPerencanaan(addNew_viApotekPerencanaan);
							datarefresh_viApotekPerencanaan();
							if (x===undefined)
							{
								setLookUps_viApotekPerencanaan.close();
							}
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viApotekPerencanaan',
						handler: function()
						{
							datadelete_viApotekPerencanaan();
							datarefresh_viApotekPerencanaan();
							
						}
					},
					//-------------- ## --------------
					{
						xtype:'tbseparator'
					},
									
				]
			}
		}
    )

    return pnlFormDataBasic_viApotekPerencanaan;
}
// End Function getFormItemEntry_viApotekPerencanaan # --------------

/**
*	Function : getItemPanelInputPenerimaan_viApotekPerencanaan
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemPanelInputPenerimaan_viApotekPerencanaan(lebar) 
{
    var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		border:true,
		items:
		[			
			{
                xtype: 'compositefield',
                fieldLabel: 'Ro Number',
                anchor: '100%',
                width: 250,
                items: 
                [                    
					{
						xtype: 'textfield',
						flex: 1,
						width : 120,	
						name: 'txtRoNumber_viApotekPerencanaan',
						id: 'txtRoNumber_viApotekPerencanaan',
						emptyText: 'Ro Number',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},                    
                    //-------------- ## --------------  
					{
						xtype: 'displayfield',
						flex: 1,
						width: 70,
						name: '',
						value: 'Ro Date:',
						fieldLabel: 'Label'
					},					
					{
						xtype: 'datefield',
						id: 'txtDateRO_viApotekPerencanaan',
						value: now_viApotekPerencanaan,
						format: 'd/M/Y',
						width: 120,
						listeners:
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									datarefresh_viApotekPerencanaan();								
								} 						
							}
						}
					}
					
                ]
            },
            //-------------- ## --------------  
			{
				xtype: 'compositefield',
				fieldLabel: 'Keterangan',
				anchor: '100%',
				//labelSeparator: '',
				width: 199,
				items: 
				[
					{
						xtype: 'textarea',
						fieldLabel: 'Keterangan',
						emptyText: 'Keterangan',
						id: 'txtKeterangan_viApotekPerencanaan',
						name: 'txtKeterangan_viApotekPerencanaan',
						width: 450,
						height: 50,
					}
				]
			}
		]
	};
    return items;
};
// End Function getItemPanelInputPenerimaan_viApotekPerencanaan # --------------


/**
*	Function : getItemGridTransaksi_viApotekPerencanaan
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemGridTransaksi_viApotekPerencanaan(lebar) 
{
    var items =
	{
		//title: 'Detail Transaksi', 
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:1px 1px 1px 1px',
		border:true,
		width: lebar-80,
		height:425, //225, 
	    items:
		[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:
				[
					gridDataViewEdit_viApotekPerencanaan()
				]	
			}
			//-------------- ## --------------
		]
	};
    return items;
};
// End Function getItemGridTransaksi_viApotekPerencanaan # --------------

/**
*	Function : gridDataViewEdit_viApotekPerencanaan
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function gridDataViewEdit_viApotekPerencanaan()
{
    
    chkSelected_viApotekPerencanaan = new Ext.grid.CheckColumn
	(
		{
			id: 'chkSelected_viApotekPerencanaan',
			header: '',
			align: 'center',						
			dataIndex: 'SELECTED',			
			width: 20
		}
	);

    var FieldGrdKasir_viApotekPerencanaan = 
	[
	];
	
    dsDataGrdJab_viApotekPerencanaan= new WebApp.DataStore
	({
        fields: FieldGrdKasir_viApotekPerencanaan
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJab_viApotekPerencanaan,
        height: 420,
		selModel: new Ext.grid.RowSelectionModel
        (
	        {
	            singleSelect: true,
	            listeners:
	            {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
	        }
        ),
        columns: 
		[
			chkSelected_viApotekPerencanaan,
			{
				dataIndex: '',
				header: 'Kode',
				sortable: true,
				width: 100
			},
			//-------------- ## --------------
			{			
				dataIndex: '',
				header: 'Uraian',
				sortable: true,
				width: 500
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Satuan',
				sortable: true,
				width: 85,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Qty Box',
				sortable: true,
				width: 50,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Frac',
				sortable: true,
				width: 50,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Stok',
				sortable: true,
				width: 50,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Qty',
				sortable: true,
				width: 50,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Ordered',
				sortable: true,
				width: 50,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
        ],
        plugins:chkSelected_viApotekPerencanaan,
    }    
    return items;
}
// End Function gridDataViewEdit_viApotekPerencanaan # --------------




