var GridDataView_perencanaanKePemesanan;
var dataSource_perencanaanKepemesanan;
var selectCount_pemesananKegudang_farmasi=50;
var NamaForm_pemesananKegudang_farmasi="Pemesanan Obat Gudang Farmasi";
var selectCountStatusPostingKegudang_farmasi='Semua';
var mod_name_pemesananKegudang_farmasi="pemesananKegudang_farmasi";
var now_pemesananKegudang_farmasi= new Date();
var rowSelected_pemesananKegudang_farmasi;
var rowSelectedGridobat_pemesananKegudang_farmasi;
var setLookUps_pemesananKegudang_farmasi;
var selectSetUnit;
var cellSelecteddeskripsiRWI;
var tanggal = now_pemesananKegudang_farmasi.format("d/M/Y");
var jam = now_pemesananKegudang_farmasi.format("H/i/s");
var tmpkriteria;
var gridDTLKegudang_farmasi_Kegudang_farmasi;
var GridDataView_pemesananKegudang_farmasi;
var  autocom_pemesanan;
var tab;

var CurrentKegudang_farmasi =
{
    data: Object,
    details: Array,
    row: 0
};

var CurrentData_pemesananKegudang_farmasi =
{
	data: Object,
	details: Array,
	row: 0
};

var CurrentDataobat_pemesananKegudang_farmasi =
{
	data: Object,
	details: Array,
	row: 0
};


CurrentPage.page = dataGrid_pemesananKegudang_farmasi(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

var Kegudang_farmasi={};
Kegudang_farmasi.form={};
Kegudang_farmasi.func={};
Kegudang_farmasi.vars={};
Kegudang_farmasi.func.parent=Kegudang_farmasi;
Kegudang_farmasi.form.ArrayStore={};
Kegudang_farmasi.form.ComboBox={};
Kegudang_farmasi.form.DataStore={};
Kegudang_farmasi.form.Record={};
Kegudang_farmasi.form.Form={};
Kegudang_farmasi.form.Grid={};
Kegudang_farmasi.form.Panel={};
Kegudang_farmasi.form.TextField={};
Kegudang_farmasi.form.Button={};

Kegudang_farmasi.form.ArrayStore.obat=new Ext.data.ArrayStore({
		id: 0,
		fields: [	
					'kd_prd','nama','tgl_masuk','jam_masuk','no_kamar','nama_kamar'
				],
		data: []
	});
	
Kegudang_farmasi.form.ArrayStore.vendor=new Ext.data.ArrayStore({
		id: 0,
		fields: [	
					'kd_vendor','vendor'
				],
		data: []
	});	
	
Kegudang_farmasi.form.ArrayStore.waktu	= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_waktu','waktu'],
	data: []
});

Kegudang_farmasi.form.ArrayStore.jenisKegudang_farmasi	= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_jenis','jenis_Kegudang_farmasi','harga_pokok'],
	data: []
});

function dataGrid_pemesananKegudang_farmasi(mod_id_pemesananKegudang_farmasi){	
    var FieldMaster_pemesananKegudang_farmasi = 
	[
		'po_number', 'kd_milik', 'vendor','kd_unit_far', 'po_date', 'milik'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_pemesananKegudang_farmasi = new WebApp.DataStore
	({
        fields: FieldMaster_pemesananKegudang_farmasi
    });
    dataGriAwal_pemesanan();
    // Grid Apotek pemesanan # --------------
	GridDataView_pemesananKegudang_farmasi = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_pemesananKegudang_farmasi,
			autoScroll: true,
			columnLines: true,
			border:true,
			flag:1,
			height:250,
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_pemesananKegudang_farmasi = undefined;
							rowSelected_pemesananKegudang_farmasi = dataSource_pemesananKegudang_farmasi.getAt(row);
							CurrentData_pemesananKegudang_farmasi
							CurrentData_pemesananKegudang_farmasi.row = row;
							CurrentData_pemesananKegudang_farmasi.data = rowSelected_pemesananKegudang_farmasi.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_pemesananKegudang_farmasi = dataSource_pemesananKegudang_farmasi.getAt(ridx);
					if (rowSelected_pemesananKegudang_farmasi != undefined)
					{	if (rowSelected_pemesananKegudang_farmasi.data.po_status==='t')
						{
						ShowPesanWarningKegudang_farmasi_pemesanan('Data telah diposting', 'Perhatian');
						}
						else{
							
						setLookUp_pemesananKegudang_farmasi(rowSelected_pemesananKegudang_farmasi.data,1);
						
						}
					}
					
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Apotek pemesanan
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header		: 'Status',
						width		: 50,
						sortable	: false,
						hideable	: true,
						hidden		: false,
						menuDisabled: true,
						dataIndex	: 'po_status',
						renderer	: function(value, metaData, record, rowIndex, colIndex, store){
							 switch (value){
								 case 't':
									 metaData.css = 'StatusHijau'; 
									 break;
								 case 'f':
									 metaData.css = 'StatusMerah';
									 break;
							 }
							 return '';
						}
					},
					{
						header: 'No. pemesanan',
						dataIndex: 'po_number',
						sortable: true,
						width: 35
						
					},
					//-------------- ## --------------
					{
						header: 'Vendor',
						dataIndex: 'vendor',
						sortable: true,
						width: 35
					},
					//-------------- ## --------------
					{
						header:'Tgl pemesanan',
						dataIndex: 'po_date',						
						width: 30,
						sortable: true,
						hideable:false,
                        menuDisabled:true,
						renderer: function(v, params, record)
						{
						return ShowDate(record.data.po_date);
						}
					},
					//-------------- ## --------------
					{
						header: 'Milik',
						dataIndex: 'milik',
						sortable: true,
						width: 50
					},
					//-------------- ## --------------
					//-------------- ## --------------
					{
						header: 'kd_milik',
						dataIndex: 'kd_milik',
						sortable: true,
						width: 40,
						hidden:true
					}
					//-------------- ## --------------
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_pemesananKegudang_farmasi',
				items: 
				[
					{
						xtype: 'button',
						text: 'Tambah pemesanan ',
						iconCls: 'Edit_Tr',
						tooltip: 'Add Data',
						id: 'btnTambah_pemesananKegudang_farmasi',
						handler: function(sm, row, rec)
						{
							setLookUp_pemesananKegudang_farmasi();								
						}
					},
					{
						xtype: 'button',
						text: 'Unposting ',
						iconCls: 'remove',
						tooltip: 'Unposing',
						id: 'btnUnpos_pemesananKegudang_farmasi',
						handler: function(sm, row, rec)
						{
							
							var line =GridDataView_pemesananKegudang_farmasi.getSelectionModel().selections.items[0].data;
							if(line.po_status=='f' || line.po_status==false ){
 							ShowPesanWarningKegudang_farmasi_pemesanan('Nomor pemesanan  ini belum diposting', 'Perhatian');
							}else{
								Ext.Ajax.request({
									url			: baseURL + "index.php/gudang_farmasi/functionpemesanan/unposting",
									params		: {
													po_number:line.po_number
										
									},
									failure		: function(o){
									dataGriAwal_pemesanan(Ext.getCmp('TxtFilter_pemesananKegudang_farmasi').getValue(),
															Ext.getCmp('dateFilter_pemesananKegudang_farmasi').getValue(),
															Ext.getCmp('dateFilter2_pemesananKegudang_farmasi').getValue()
															);
									ShowPesanErrorKegudang_farmasi_pemesanan('Unposting error hubungi Admin', 'Error');
									},
									success		: function(o){
										dataGriAwal_pemesanan(Ext.getCmp('TxtFilter_pemesananKegudang_farmasi').getValue(),
										Ext.getCmp('dateFilter_pemesananKegudang_farmasi').getValue(),
										Ext.getCmp('dateFilter2_pemesananKegudang_farmasi').getValue()
										);
										var cst = Ext.decode(o.responseText);
								
										if(cst.success===true){
										ShowPesanInfoKegudang_farmasi_pemesanan('data berhasil diun Posting', 'Simpan');
										}
									}
								});

							}						
						
						}
					},
					{
						xtype: 'button',
						text: 'Batal transaksi ',
						iconCls: 'remove',
						tooltip: 'Unposing',
						id: 'btn_pemesananKegudang_farmasi',
						handler: function(sm, row, rec)
						{	var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan Pembatalan Transaksi:', function(btn, combo){
												console.log(combo);
													if (btn == 'ok')
														{
															var variablebatalhistori=combo;
															if (variablebatalhistori!='')
																{
																var line =GridDataView_pemesananKegudang_farmasi.getSelectionModel().selections.items[0].data;
																 Ext.Ajax.request({
																		url			: baseURL + "index.php/gudang_farmasi/functionpemesanan/batal_transaksi",
																		params		: {
																						po_number:line.po_number,
																						keterangan: variablebatalhistori
																		},
																		failure		: function(o){
																		dataGriAwal_pemesanan(Ext.getCmp('TxtFilter_pemesananKegudang_farmasi').getValue(),
																								Ext.getCmp('dateFilter_pemesananKegudang_farmasi').getValue(),
																								Ext.getCmp('dateFilter2_pemesananKegudang_farmasi').getValue()
																								);
																		ShowPesanErrorKegudang_farmasi_pemesanan('error hubungi Admin Admin', 'Error');
																		},
																		success		: function(o){
																			dataGriAwal_pemesanan(Ext.getCmp('TxtFilter_pemesananKegudang_farmasi').getValue(),
																			Ext.getCmp('dateFilter_pemesananKegudang_farmasi').getValue(),
																			Ext.getCmp('dateFilter2_pemesananKegudang_farmasi').getValue()
																			);
																			var cst = Ext.decode(o.responseText);
																			if(cst.success===true){
																			ShowPesanInfoKegudang_farmasi_pemesanan('data berhasil dihapus', 'hapus');
																			}
																		}
																	});
																}
															else
																{
																	ShowPesanWarningKasirrwj('Silahkan isi alasan terlebih dahaulu','Keterangan');

																}
														}
												}); 
											
							
						}
					}
					/* {
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_pemesananKegudang_farmasi',
						handler: function(sm, row, rec)
						{
							if (rowSelected_pemesananKegudang_farmasi != undefined)
							{
								setLookUp_pemesananKegudang_farmasi(rowSelected_pemesananKegudang_farmasi.data)
							} else{
								ShowPesanWarningKegudang_farmasi_pemesanan('Pilih data yang akan di edit','Warning');
							}
							
						}
					} */
					//-------------- ## --------------
				]
			},
			viewConfig: 
			{
				forceFit: true
			}
		}
    )
	
	var pencarianKegudang_farmasi = new Ext.FormPanel({
        labelAlign: 'top',
        frame:true,
        title: '',
        bodyStyle:'padding:5px 5px 0',
        //width: 600,
        items: [
		{
			layout: 'column',
			border: false,
			items:
			[
				{
					columnWidth:.98,
					layout: 'absolute',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					width: 500,
					height: 70,
					anchor: '100% 100%',
					items:
					[
						{
							x: 10,
							y: 0,
							xtype: 'label',
							text: 'No. pemesanan'
						},
						{
							x: 120,
							y: 0,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 0,
							xtype: 'textfield',
							id: 'TxtFilter_pemesananKegudang_farmasi',
							name: 'TxtFilter_pemesananKegudang_farmasi',
							emptyText: '',
							width: 130,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
									dataGriAwal_pemesanan(Ext.getCmp('TxtFilter_pemesananKegudang_farmasi').getValue(),
												Ext.getCmp('dateFilter_pemesananKegudang_farmasi').getValue(),
												Ext.getCmp('dateFilter2_pemesananKegudang_farmasi').getValue()
									);
									} 						
								}
							}
						},
						{
							x: 10,
							y: 30,
							xtype: 'label',
							text: 'Tgl pemesanan'
						},
						{
							x: 120,
							y: 30,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 30,
							xtype: 'datefield',
							id: 'dateFilter_pemesananKegudang_farmasi',
							name: 'dateFilter_pemesananKegudang_farmasi',
							format: 'd/M/Y',
							value:now_pemesananKegudang_farmasi,
							width: 130,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
									dataGriAwal_pemesanan(Ext.getCmp('TxtFilter_pemesananKegudang_farmasi').getValue(),
									Ext.getCmp('dateFilter_pemesananKegudang_farmasi').getValue(),
									Ext.getCmp('dateFilter2_pemesananKegudang_farmasi').getValue()
									);
									} 						
								}
							}
						},
						{
							x: 270,
							y: 30,
							xtype: 'datefield',
							id: 'dateFilter2_pemesananKegudang_farmasi',
							name: 'dateFilter2_pemesananKegudang_farmasi',
							format: 'd/M/Y',
							value:now_pemesananKegudang_farmasi,
							width: 130,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
									dataGriAwal_pemesanan(Ext.getCmp('TxtFilter_pemesananKegudang_farmasi').getValue(),
									Ext.getCmp('dateFilter_pemesananKegudang_farmasi').getValue(),
									Ext.getCmp('dateFilter2_pemesananKegudang_farmasi').getValue());
									} 						
								}
							}
						},
						{
							x: 280,
							y: 0,
							xtype: 'button',
							text: 'Cari',
							iconCls: 'refresh',
							tooltip: 'Cari',
							style:{paddingLeft:'30px'},
							width:150,
							id: 'BtnFilterGridDataView_pemesananKegudang_farmasi',
							handler: function() 
							{	
									dataGriAwal_pemesanan(Ext.getCmp('TxtFilter_pemesananKegudang_farmasi').getValue(),
									Ext.getCmp('dateFilter_pemesananKegudang_farmasi').getValue(),
									Ext.getCmp('dateFilter2_pemesananKegudang_farmasi').getValue()
									);						
							}                        
						}
					]
				}
			]
		}
		]	
	})

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_pemesananKegudang_farmasi = new Ext.Panel
    (
		{
			title: NamaForm_pemesananKegudang_farmasi,
			iconCls: 'Studi_Lanjut',
			id: mod_id_pemesananKegudang_farmasi,
			region: 'center',
			layout: 'form', 
			closable: true,        
			border: false,  
			margins: '0 5 5 0',
			items: [ pencarianKegudang_farmasi,
					GridDataView_pemesananKegudang_farmasi,
					grid_dua_perencanaan()],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_pemesananKegudang_farmasi,
		            columns: 21,
		            defaults: {
					scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
					]
					//-------------- # End items # --------------
				}
			],
			// map multiple keys to multiple actions by strings and array of codes
			
			//-------------- # End tbar # --------------
       }
	   // map multiple keys to one action by string
    )
    return FrmFilterGridDataView_pemesananKegudang_farmasi;
    //-------------- # End form filter # --------------
}

function dataGriAwal_tetangga(nominta,req_date,req_date2){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gudang_farmasi/functionpemesanan/getDataGridAwal_perencanaan",
			params: {
				no_minta:nominta,
				req_date:req_date,
				req_date2:req_date2
			
			},
			failure: function(o)
			{ loadMask.hide();
				ShowPesanErrorKegudang_farmasi_pemesanan('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dataSource_perencanaanKepemesanan.removeAll();
					var recs=[],
						recType=dataSource_perencanaanKepemesanan.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
						dataSource_perencanaanKepemesanan.add(recs);
					
					
					
					GridDataView_perencanaanKePemesanan.getView().refresh();
				}
				else 
				{
					ShowPesanErrorKegudang_farmasi_pemesanan('Gagal membaca data obat', 'Error');
				};
			}
		}
		
	)
	
}

function grid_dua_perencanaan(){
var FieldMaster_pemesananKegudang_farmasi = 
	[
		'req_number', 'kd_milik', 'nm_unit_far','kd_unit_far', 'req_date', 'milik'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_perencanaanKepemesanan = new WebApp.DataStore
	({
        fields: FieldMaster_pemesananKegudang_farmasi
    });
    dataGriAwal_tetangga();
    // ShowPesanWarningKegudang_farmasi_pemesanan Grid Apotek Perencanaan # --------------
	GridDataView_perencanaanKePemesanan = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_perencanaanKepemesanan,
			autoScroll: true,
			columnLines: true,
			border:false,
			flag:1,
			height:150,
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_pemesananKegudang_farmasi = dataSource_perencanaanKepemesanan.getAt(ridx);
					if (rowSelected_pemesananKegudang_farmasi != undefined)
					{	if (rowSelected_pemesananKegudang_farmasi.data.qty_ordered==='t')
						{
						ShowPesanWarningKegudang_farmasi_pemesanan('nomer pemesanan  ini telah dilayani', 'Perhatian');
						}
						else{
							setLookUp_pemesananKegudang_farmasi(rowSelected_pemesananKegudang_farmasi.data,2)
						
						}
					}
					
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Apotek perencanaan
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header: 'No. perencanaan',
						dataIndex: 'req_number',
						sortable: true,
						width: 35
						
					},
					//-------------- ## --------------
					{
						header: 'Unit farmasi',
						dataIndex: 'nm_unit_far',
						sortable: true,
						width: 35
					},
					//-------------- ## --------------
					{
						header:'Tgl Perencanaan',
						dataIndex: 'req_date',						
						width: 30,
						sortable: true,
						hideable:false,
                        menuDisabled:true,
						renderer: function(v, params, record)
						{
						return ShowDate(record.data.req_date);
						}
					},
					//-------------- ## --------------
					{
						header: 'Milik',
						dataIndex: 'milik',
						sortable: true,
						width: 50
					},
					//-------------- ## --------------
					//-------------- ## --------------
					{
						header: 'kd_milik',
						dataIndex: 'kd_milik',
						sortable: true,
						width: 40,
						hidden:true
					},{
						header: 'kd_unit_far',
						dataIndex: 'kd_unit_far',
						sortable: true,
						width: 40,
						hidden:true
					}
					//-------------- ## --------------
				]
			),
			// Tolbar ke Dua # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )
	
	return GridDataView_perencanaanKePemesanan;
	
}

function setLookUp_pemesananKegudang_farmasi(rowdata,nilai){
    var lebar = 985;
	var savepemesanan_gudangFarmasi=new Ext.KeyMap(document, {
        key: 's',
		ctrl : true,
		alt : true,
		defaultEventAction: 'stopEvent',
        fn: function () {
           save_pemesanan();
        }
    });
    setLookUps_pemesananKegudang_farmasi = new Ext.Window({
        id: Nci.getId(),
        title: NamaForm_pemesananKegudang_farmasi, 
        closeAction: 'destroy',        
        width: 900,
        height: 580,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_pemesananKegudang_farmasi(lebar,rowdata),
        listeners:{
            activate: function(){
				
            },
            afterShow: function(){
                this.activate();
            },
            deactivate: function(){
				savepemesanan_gudangFarmasi.setDisabled(true);
                rowSelected_pemesananKegudang_farmasi=undefined;
            }
        }
    });
	

    setLookUps_pemesananKegudang_farmasi.show();
    if (rowdata == undefined){	
    }
    else
    {
        datainit_pemesananKegudang_farmasi(rowdata,nilai);
    }
}

function getFormItemEntry_pemesananKegudang_farmasi(lebar,rowdata){
    var pnlFormDataBasic_pemesananKegudang_farmasi = new Ext.FormPanel({
		title: '',
		region: 'north',
		layout: 'form',
		bodyStyle: 'padding:10px 10px 10px 10px',
		anchor: '100%',
		labelWidth: 1,
		autoWidth: true,
		width: lebar,
		border: false,
		items:[
				getItemPanelInputpemesanan_pemesananKegudang_farmasi(lebar),
				getItemGridobat_pemesananKegudang_farmasi(lebar),
				],
			fileUpload: true,
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAdd_pemesananKegudang_farmasi',
						handler: function(){
							dataAddNew_pemesananKegudang_farmasi();
						}
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						//id: 'btnAdd_pemesananKegudang_farmasi',
						handler: function(){
							save_pemesanan()//dataAddNew_pemesananKegudang_farmasi();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Posting',
						iconCls: 'save',
						//id: 'btnAdd_pemesananKegudang_farmasi',
						handler: function(){
							if( Ext.getCmp('txtNopemesananKegudang_farmasi').getValue()!="")
							{
								if (dsDataGrdobat_pemesananKegudang_farmasi.getCount()!=0){
								
									Ext.Ajax.request
									(
										{
											url: baseURL + "index.php/gudang_farmasi/functionpemesanan/posting",
											params:{
												no_penerimaan: Ext.getCmp('txtNopemesananKegudang_farmasi').getValue(),
											} ,
											failure: function(o)
											{
												ShowPesanErrorKegudang_farmasi_pemesanan('Error, posting pemesanan! Hubungi Admin', 'Error');
											},	
											success: function(o) 
											{
												var cst = Ext.decode(o.responseText);
												if (cst.success === true) 
												{
													ShowPesanInfoKegudang_farmasi_pemesanan('Berhasil posting pemesanan', 'Information');
													dataGriAwal_pemesanan(Ext.getCmp('TxtFilter_pemesananKegudang_farmasi').getValue(),
														Ext.getCmp('dateFilter_pemesananKegudang_farmasi').getValue(),
														Ext.getCmp('dateFilter2_pemesananKegudang_farmasi').getValue()
													);
													dataGriAwal_tetangga();
				
												}
												else 
												{
													ShowPesanErrorKegudang_farmasi_pemesanan('Gagal posting pemesanan', 'Error');
												};
											}
										}
										
									)
								
								}else{
									ShowPesanErrorKegudang_farmasi_pemesanan('Harap isi dulu detail pemesanan obat', 'Error');
								}
							
							}else{
								ShowPesanErrorKegudang_farmasi_pemesanan('Harap simpan terlebih dahulu untuk melakukan posting!', 'Error');
							}
							
						}
					}
					
					
				]
			}//,items:
		}
    )

    return pnlFormDataBasic_pemesananKegudang_farmasi;
}

function getItemPanelInputpemesanan_pemesananKegudang_farmasi(lebar) {
    autocom_pemesanan=	new Nci.form.Combobox.autoComplete({
					store	: Kegudang_farmasi.form.ArrayStore.vendor,
					select	: function(a,b,c){
							
					},
					insert	: function(o){
						return {
								kd_vendor	: o.kd_vendor,
								vendor		: o.vendor,
							 }
					},
					param:function(){
							return {
						/* 		kd_milikLama:Kegudang_farmasi.vars.kd_milik,
								kd_milikNew:Ext.getCmp('cbo_UnitKegudang_farmasiLookup').getValue()
 */							}
					},
					url		: baseURL + "index.php/gudang_farmasi/functionpemesanan/getvendor",
					valueField:   'vendor',
					displayField: 'vendor',
					listWidth: 250,
					width	 :250,
					x: 130,
					y: 70
				});
    var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 115,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 10,
								xtype: 'label',
								text: 'No. pemesanan'
							},
							{
								x: 120,
								y: 10,
								xtype: 'label',
								text: ':'
							},{
								x: 130,
								y: 10,
								xtype: 'textfield',
								id: 'txtNopemesananKegudang_farmasi',
								name: 'txtNopemesananKegudang_farmasi',
								width: 150,
								readOnly:true,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									}
								}
							},
							
							{
								x: 10,
								y: 40,
								xtype: 'label',
								text: 'Tanggal pemesanan'
							},
							{
								x: 120,
								y: 40,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 40,
								xtype: 'datefield',
								id: 'dfTglpemesananKegudang_farmasi',
								format: 'd/M/Y',
								width: 150,
								tabIndex:2,
								readOnly:true,
								value:now_pemesananKegudang_farmasi,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									}
								}
							},
							{
								x: 10,
								y: 70,
								xtype: 'label',
								text: 'Vendor'
							},
							{
								x: 120,
								y: 70,
								xtype: 'label',
								text: ':'
							},	
							autocom_pemesanan,
							{
								x: 310,
								y: 10,
								xtype: 'label',
								text: 'Keterangan'
							},
							{
								x: 400,
								y: 10,
								xtype: 'label',
								text: ':'
							},{
								x: 410,
								y: 10,
								xtype: 'textarea',
								id: 'Keterangan_Kegudang_farmasi',
								name: 'Keterangan_Kegudang_farmasi',
								width: 250,
								height: 60,
								//readOnly:true,
								tabIndex:2,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									}
								}
							}
						]
					}
				]
			}
		]		
	};
    return items;
};

function getItemGridobat_pemesananKegudang_farmasi(lebar){
    var items ={
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'margin-top: -1px;',
		border:false,
		width: lebar-80,
		height: 450,//300, 
	    tbar:
		[
			{
				text	: 'Add obat',
				id		: 'btnAddobatKegudang_farmasiL',
				tooltip	: nmLookup,
				disabled: false,
				iconCls	: 'find',
				handler	: function()
				{
					var records = new Array();
					records.push(new dsDataGrdobat_pemesananKegudang_farmasi.recordType());
					dsDataGrdobat_pemesananKegudang_farmasi.add(records);
					row=dsDataGrdobat_pemesananKegudang_farmasi.getCount()-1; // menujukan jumlah baris,startEditing(baris dimulai dari 1, kolom 1)
					Kegudang_farmasi.form.Grid.pemesanan.startEditing(row, 2);	
				}
			},{
				xtype: 'tbseparator'
			},{
				xtype: 'button',
				text: 'Delete',
				iconCls: 'remove',
				id: 'btnDelete_pemesananKegudang_farmasi',
				handler: function()
				{
					var line = Kegudang_farmasi.form.Grid.pemesanan.getSelectionModel().selection.cell[0];
					var o = dsDataGrdobat_pemesananKegudang_farmasi.getRange()[line].data;
				
						Ext.Msg.confirm('Warning', 'Apakah obat ini akan dihapus?', function(button){
							if (button == 'yes'){
								if(dsDataGrdobat_pemesananKegudang_farmasi.getRange()[line].data.kd_prd != undefined){
									Ext.Ajax.request
									(
										{
											url: baseURL + "index.php/gudang_farmasi/functionpemesanan/delete",
											params:{
												line:o.line,
												no_penerimaan: Ext.getCmp('txtNopemesananKegudang_farmasi').getValue(),
											} ,
											failure: function(o)
											{
												ShowPesanErrorKegudang_farmasi_pemesanan('Error, delete obat! Hubungi Admin', 'Error');
											},	
											success: function(o) 
											{
												var cst = Ext.decode(o.responseText);
												if (cst.success === true) 
												{
													dsDataGrdobat_pemesananKegudang_farmasi.removeAt(line);
													Kegudang_farmasi.form.Grid.pemesanan.getView().refresh();
													//Ext.getCmp('btnAddDetKegudang_farmasiKegudang_farmasiL').enable();
													ShowPesanInfoKegudang_farmasi_pemesanan('Berhasil menghapus data obat ini', 'Information');
													
												}
												else 
												{
													ShowPesanErrorKegudang_farmasi_pemesanan('Gagal menghapus data obat ini', 'Error');
												};
											}
										}
										
									)
								}else{
									dsDataGrdobat_pemesananKegudang_farmasi.removeAt(line);
									Kegudang_farmasi.form.Grid.pemesanan.getView().refresh();
									
								}
							} 
							
						});
					
					
				}
			}	
		],
		items:[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:[
					gridDataViewEdit_pemesananKegudang_farmasi()
				]	
			}
		]
	};
    return items;
};


//var a={};
function gridDataViewEdit_pemesananKegudang_farmasi(){
    var FieldGrdKasir_pemesananKegudang_farmasi = ['kd_prd'];
    dsDataGrdobat_pemesananKegudang_farmasi= new WebApp.DataStore({
        fields: FieldGrdKasir_pemesananKegudang_farmasi
    });
	var Fieldmilikpemesanan = ['kd_milik','milik'];
    dsgridcombomilik_pemesananKegudang_farmasi = new WebApp.DataStore({ fields: Fieldmilikpemesanan });
	
    loaddatagridkepemilikan_pemesananKegudang_farmasi();
	
    Kegudang_farmasi.form.Grid.pemesanan =new Ext.grid.EditorGridPanel({
        store: dsDataGrdobat_pemesananKegudang_farmasi,
        height: 350,
        columnLines: true,
		selModel: new Ext.grid.CellSelectionModel ({
            singleSelect: true,
            listeners:{
                cellselect: function(sm, row, rec){
					rowSelectedGridobat_pemesananKegudang_farmasi = undefined;
					rowSelectedGridobat_pemesananKegudang_farmasi = dsDataGrdobat_pemesananKegudang_farmasi.getAt(row);
					CurrentDataobat_pemesananKegudang_farmasi
					CurrentDataobat_pemesananKegudang_farmasi.row = row;
					CurrentDataobat_pemesananKegudang_farmasi.data = rowSelectedGridobat_pemesananKegudang_farmasi.data;
					
                },
            }
        }),
        stripeRows: true,
        cm:new Ext.grid.ColumnModel([
        	new Ext.grid.RowNumberer(),	
			{
				dataIndex: 'kd_prd',
				header: 'Kode Prd',
				sortable: true,
				width: 70
			},
			{			
				dataIndex: 'nama_obat',
				header: 'Nama obat',
				sortable: true,
				width: 250,
				editor:new Nci.form.Combobox.autoComplete({
					store	: Kegudang_farmasi.form.ArrayStore.obat,
					select	: function(a,b,c){
						
						var line	= Kegudang_farmasi.form.Grid.pemesanan.getSelectionModel().selection.cell[0];
						if(dsDataGrdobat_pemesananKegudang_farmasi.getCount()-1===0)
						{
							dsDataGrdobat_pemesananKegudang_farmasi.getRange()[line].data.kd_prd=b.data.kd_prd;
							dsDataGrdobat_pemesananKegudang_farmasi.getRange()[line].data.qty=1;
							dsDataGrdobat_pemesananKegudang_farmasi.getRange()[line].data.nama_obat=b.data.nama_obat;
							dsDataGrdobat_pemesananKegudang_farmasi.getRange()[line].data.kd_milik=b.data.kd_milik;
							dsDataGrdobat_pemesananKegudang_farmasi.getRange()[line].data.harga=0;
							dsDataGrdobat_pemesananKegudang_farmasi.getRange()[line].data.jml_order=0;
							dsDataGrdobat_pemesananKegudang_farmasi.getRange()[line].data.jml_receive=1;
						}else{
							var VALUE_x=1;
							for(var i = 0 ; i < dsDataGrdobat_pemesananKegudang_farmasi.getCount();i++)
							{
								if(dsDataGrdobat_pemesananKegudang_farmasi.getRange()[i].data.kd_prd===b.data.kd_prd)
								{
								VALUE_x=0;
								}
							}
							if(VALUE_x===0){
								ShowPesanWarningKegudang_farmasi_pemesanan('Anda telah memilih Obat yang sama', 'Perhatian');
								dsDataGrdobat_pemesananKegudang_farmasi.removeAt(line);
							}else{
								dsDataGrdobat_pemesananKegudang_farmasi.getRange()[line].data.kd_prd=b.data.kd_prd;
								dsDataGrdobat_pemesananKegudang_farmasi.getRange()[line].data.qty=1;
								dsDataGrdobat_pemesananKegudang_farmasi.getRange()[line].data.nama_obat=b.data.nama_obat;
								dsDataGrdobat_pemesananKegudang_farmasi.getRange()[line].data.kd_milik=b.data.kd_milik;
								dsDataGrdobat_pemesananKegudang_farmasi.getRange()[line].data.harga=b.data.harga;
								dsDataGrdobat_pemesananKegudang_farmasi.getRange()[line].data.jml_order=0;
								dsDataGrdobat_pemesananKegudang_farmasi.getRange()[line].data.jml_receive=1;
						
							}
						}
						Kegudang_farmasi.form.Grid.pemesanan.getView().refresh();
						
                        Kegudang_farmasi.form.Grid.pemesanan.startEditing(line, 5);						
					},
					insert	: function(o){
						return {
							jml_stok_apt	: o.jml_stok_apt,
							nama_obat		: o.nama_obat,
							text			:  '<table style="font-size: 11px;"><tr><td width="250">'+o.nama_obat+'</td><td width="50">'+o.satuan+'</td><td width="50">'+formatCurrency(o.harga)+'</td></tr></table>',
							kd_prd			: o.kd_prd,
							kd_milik		: o.kd_milik,
							satuan			: o.satuan,
							harga			: o.harga,
						}
					},
					param:function(){
							return {
						/* 		kd_milikLama:Kegudang_farmasi.vars.kd_milik,
								kd_milikNew:Ext.getCmp('cbo_UnitKegudang_farmasiLookup').getValue()
 */							}
					},
					url		: baseURL + "index.php/gudang_farmasi/functionpemesanan/getstock_obat",
					valueField: 'nama_obat',
					displayField: 'text',
					listWidth: 320
				})
			},
			{
				dataIndex: 'req_number',
				header: 'No Perencanaan',
				sortable: true,
				width: 65,
				align:'center',
				
			},
			{
				dataIndex: 'jml_order',
				header: 'Jumlah diminta',
				sortable: true,
				width: 65,
				align:'right'
			},
			{
				dataIndex: 'jml_receive',
				header: 'Jumlah ACC',
				sortable: true,
				width: 65,
				align:'right',
				editor: new Ext.form.NumberField({
                    allowBlank: true,
                    enableKeyEvents : true,
                    width:30,
					listeners:{
						blur: function(a){
							var line	= Kegudang_farmasi.form.Grid.pemesanan.getSelectionModel().selection.cell[0];
							//var line	= this.index;
							dsDataGrdobat_pemesananKegudang_farmasi.getRange()[line].data.jml_receive=a.getValue(); 
							Kegudang_farmasi.form.Grid.pemesanan.getView().refresh();	
						},
						specialkey: function(){
							if(Ext.EventObject.getKey() == 13){
								var records = new Array();
								records.push(new dsDataGrdobat_pemesananKegudang_farmasi.recordType());
								dsDataGrdobat_pemesananKegudang_farmasi.add(records);
								var nextRow = dsDataGrdobat_pemesananKegudang_farmasi.getCount()-1; 
								Kegudang_farmasi.form.Grid.pemesanan.startEditing(nextRow, 2);
								
							}
						},
					}
                })
			},
			{
				dataIndex: 'harga',
				header: 'Harga',
				sortable: true,
				width: 65,
				align:'right',
				editor: new Ext.form.NumberField({
                    allowBlank: true,
                    enableKeyEvents : true,
                    width:30,
					listeners:{
						blur: function(a){
							var line	= Kegudang_farmasi.form.Grid.pemesanan.getSelectionModel().selection.cell[0];
							/* var line	= this.index; */
							dsDataGrdobat_pemesananKegudang_farmasi.getRange()[line].data.harga=a.getValue();
							Kegudang_farmasi.form.Grid.pemesanan.getView().refresh();	
						},
						specialkey: function(){
							if(Ext.EventObject.getKey() == 13){
								var records = new Array();
								records.push(new dsDataGrdobat_pemesananKegudang_farmasi.recordType());
								dsDataGrdobat_pemesananKegudang_farmasi.add(records);
								var nextRow = dsDataGrdobat_pemesananKegudang_farmasi.getCount()-1; 
								Kegudang_farmasi.form.Grid.pemesanan.startEditing(nextRow, 2);
								
							}
						},
					}
                }),
				renderer: function(v, params, record) 
							{
							return formatCurrency(record.data.harga);
							
							}	
			},
			{
				header		: 'Kepemilikan',
				width		: 100,
				menuDisabled: true,
				dataIndex	: 'milik',
				editor		: gridcbokepemilikan_pemesananKegudang_farmasi= new Ext.form.ComboBox({
					id				: 'gridcbokepemilikan_pemesananKegudang_farmasi',
					typeAhead		: true,
					triggerAction	: 'all',
					lazyRender		: true,
					mode			: 'local',
					emptyText		: '',
					fieldLabel		: ' ',
					align			: 'Right',
					width			: 200,
					store			: dsgridcombomilik_pemesananKegudang_farmasi,
					valueField		: 'milik',
					displayField	: 'milik',
					listeners		:
					{
						select	: function(a,b,c){
							var line	= Kegudang_farmasi.form.Grid.pemesanan.getSelectionModel().selection.cell[0];
							dsDataGrdobat_pemesananKegudang_farmasi.getRange()[line].data.kd_milik=b.data.kd_milik;
						},
						keyUp: function(a,b,c){
							$this1=this;
							if((b.getKey()>=65 && b.getKey()<=90)|| (b.getKey()>=48 && b.getKey()<=57) || b.getKey()==32 || b.getKey()==8 || b.getKey()==46){
								clearTimeout(this.time);

								this.time=setTimeout(function(){
									if($this1.lastQuery != '' ){
										var value=$this1.lastQuery;
										var param={};
										param['text']=$this1.lastQuery;
										loaddatagridkepemilikan_pemesananKegudang_farmasi(param);
									}
								},1000);
							}
						},
						specialkey: function(){
							if(Ext.EventObject.getKey() == 13){
								var records = new Array();
								records.push(new dsDataGrdobat_pemesananKegudang_farmasi.recordType());
								dsDataGrdobat_pemesananKegudang_farmasi.add(records);
								var nextRow = dsDataGrdobat_pemesananKegudang_farmasi.getCount()-1; 
								Kegudang_farmasi.form.Grid.pemesanan.startEditing(nextRow, 2);
								
							}
						}
					}
				})
			},
			{
				dataIndex: 'kd_milik',
				header: 'kd_milik',
				sortable: true,
				width: 65,
				align:'center',
				hidden:true
			},
			{
				dataIndex: 'req_line',
				header: 'req line',
				hidden:true,
				sortable: true,
				width: 65,
				align:'center'
			},{
				dataIndex: 'line',
				header: 'line',
				hidden:true,
				sortable: true,
				width: 65,
				align:'center'
			}
        ]),
		viewConfig:{
			forceFit: true
		}
    });
    return Kegudang_farmasi.form.Grid.pemesanan;
}


function datainit_pemesananKegudang_farmasi(rowdata,nilai)
{//alert(rowdata.vendor);
	Ext.getCmp('txtNopemesananKegudang_farmasi').setValue(rowdata.po_number);
	Ext.getCmp('Keterangan_Kegudang_farmasi').setValue(rowdata.remark);
	autocom_pemesanan.setValue(rowdata.vendor);
	if(nilai===1)
	{
	Ext.getCmp('dfTglpemesananKegudang_farmasi').setValue(ShowDate(rowdata.po_date));
	PO_getGrid_detail_min(rowdata.po_number);	
	}
	if(nilai===2)
	{
	RO_getGrid_detail_min(rowdata.req_number);
	}
	
};

function PO_getGrid_detail_min(po_number){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gudang_farmasi/functionpemesanan/getDataGrid_detail",
			params: {nominta:po_number},
			failure: function(o)
			{loadMask.hide();
				ShowPesanErrorKegudang_farmasi_pemesanan('Error, obat! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
				dsDataGrdobat_pemesananKegudang_farmasi.removeAll();
					var recs=[],
						recType=dsDataGrdobat_pemesananKegudang_farmasi.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
					dsDataGrdobat_pemesananKegudang_farmasi.add(recs);
					
					Kegudang_farmasi.form.Grid.pemesanan.getView().refresh();
				}
				else 
				{
					ShowPesanErrorKegudang_farmasi_pemesanan('Gagal membaca data obat ini', 'Error');
				};
			}
		}
		
	)
	
}


function RO_getGrid_detail_min(req_number){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gudang_farmasi/functionpemesanan/getDataGrid_ro",
			params: {nominta:req_number},
			failure: function(o)
			{loadMask.hide();
				ShowPesanErrorKegudang_farmasi_pemesanan('Error, obat! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
				dsDataGrdobat_pemesananKegudang_farmasi.removeAll();
					var recs=[],
						recType=dsDataGrdobat_pemesananKegudang_farmasi.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
					dsDataGrdobat_pemesananKegudang_farmasi.add(recs);
					
					Kegudang_farmasi.form.Grid.pemesanan.getView().refresh();
				}
				else 
				{
					ShowPesanErrorKegudang_farmasi_pemesanan('Gagal membaca data obat ini', 'Error');
				};
			}
		}
		
	)
	
}



function dataGriAwal_pemesanan(nominta,po_date,po_date2){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gudang_farmasi/functionpemesanan/getDataGridAwal",
			params: {
				no_minta:nominta,
				po_date:po_date,
				po_date2:po_date2
			
			},
			failure: function(o)
			{ loadMask.hide();
				ShowPesanErrorKegudang_farmasi_pemesanan('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dataSource_pemesananKegudang_farmasi.removeAll();
					var recs=[],
						recType=dataSource_pemesananKegudang_farmasi.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
						dataSource_pemesananKegudang_farmasi.add(recs);
					
					
					
					GridDataView_pemesananKegudang_farmasi.getView().refresh();
				}
				else 
				{
					ShowPesanErrorKegudang_farmasi_pemesanan('Gagal membaca data obat', 'Error');
				};
			}
		}
		
	)
	
}

function dataAddNew_pemesananKegudang_farmasi(){
	Ext.getCmp('txtNopemesananKegudang_farmasi').setValue('');
	Ext.getCmp('dfTglpemesananKegudang_farmasi').setValue(now_pemesananKegudang_farmasi);
	dsDataGrdobat_pemesananKegudang_farmasi.removeAll();
	
	Ext.getCmp('txtNopemesananKegudang_farmasi').enable();
	Ext.getCmp('dfTglpemesananKegudang_farmasi').enable();
	
}


function ShowPesanWarningKegudang_farmasi_pemesanan(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:290
		}
	);
};

function ShowPesanErrorKegudang_farmasi_pemesanan(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:290
		}
	);
};


function ShowPesanInfoKegudang_farmasi_pemesanan(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:290
		}
	);
};


function save_pemesanan(mBol){	
	
	if (ValidasiEntrypemesananKegudang_farmasi_pemesanan(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request({
			url			: baseURL + "index.php/gudang_farmasi/functionpemesanan/insert_apt_request",
			params		: getParampemesanan(),
			failure		: function(o){
			dataGriAwal_pemesanan(Ext.getCmp('TxtFilter_pemesananKegudang_farmasi').getValue(),
									Ext.getCmp('dateFilter_pemesananKegudang_farmasi').getValue(),
									Ext.getCmp('dateFilter2_pemesananKegudang_farmasi').getValue()
									);
			ShowPesanErrorKegudang_farmasi_pemesanan('Insert error hubungi Admin Admin', 'Error');
			},
			success		: function(o){
				dataGriAwal_pemesanan(Ext.getCmp('TxtFilter_pemesananKegudang_farmasi').getValue(),
				Ext.getCmp('dateFilter_pemesananKegudang_farmasi').getValue(),
				Ext.getCmp('dateFilter2_pemesananKegudang_farmasi').getValue()
				);
				var cst = Ext.decode(o.responseText);
				if(cst.login_error===true){
				ShowPesanWarningKegudang_farmasi_pemesanan('durasi login Habis Harap login ulang jika tidak bisa hubungi admin', 'Perhatian');
				}
				if(cst.vendor_error===true){
				ShowPesanWarningKegudang_farmasi_pemesanan('Harap Isi  Vendor dengan Benar, isi sesuai pilihan', 'Perhatian');
				}
				if(cst.simpan===false){
				ShowPesanWarningKegudang_farmasi_pemesanan('ro unit gagal dimasukan ke database', 'Perhatian');
				}
				if(cst.simpan_det===false){
				ShowPesanWarningKegudang_farmasi_pemesanan('ro unit detail gagal dimasukan ke database', 'Perhatian');
				}
				if(cst.simpan_det===true){
				ShowPesanInfoKegudang_farmasi_pemesanan('data berhasil disimpan', 'Simpan');
				Ext.getCmp('txtNopemesananKegudang_farmasi').setValue(cst.no_minta);
				PO_getGrid_detail_min(cst.no_minta);
				dataGriAwal_tetangga();
				
				}
			}
			
		});
	}
}

function ValidasiEntrypemesananKegudang_farmasi_pemesanan(modul,mBolHapus)
{
	var x = 1;
	if(dsDataGrdobat_pemesananKegudang_farmasi.getCount() == 0){
		ShowPesanWarningKegudang_farmasi_pemesanan('Daftar obat tidak boleh kosong! Pemesanan minimal 1 obat','Warning')
		x=0;
	} else{
		for(var i=0; i<dsDataGrdobat_pemesananKegudang_farmasi.getCount() ; i++){
			var o=dsDataGrdobat_pemesananKegudang_farmasi.getRange()[i].data;
			if(o.kd_prd == undefined || o.kd_prd ==""){
				ShowPesanWarningKegudang_farmasi_pemesanan('Harap masukan obat atau hapus baris kosong untuk melanjutkan!','Warning')
				x=0;
			}
		}
	}
	return x;
};

function getParampemesanan(){
	var params={
	no_minta 	 :Ext.getCmp('txtNopemesananKegudang_farmasi').getValue(),
	tgl_minta 	 :Ext.getCmp('dfTglpemesananKegudang_farmasi').getValue(),
	jumlah_detil :dsDataGrdobat_pemesananKegudang_farmasi.getCount(),
	keterangan	 : Ext.getCmp('Keterangan_Kegudang_farmasi').getValue(),
	vendor		 :	 autocom_pemesanan.getValue()
	};
					
	
	for(var i=0, iLen= dsDataGrdobat_pemesananKegudang_farmasi.getCount(); i<iLen;i++){
    	var o=dsDataGrdobat_pemesananKegudang_farmasi.getRange()[i].data;
    	params['kd_prd'+i]	= o.kd_prd;
		params['qty'+i]	= o.qty;
    	params['kd_milik'+i]	= o.kd_milik;
		params['line'+i]		= o.line;
		params['req_number'+i]	= o.req_number;
		params['jml_order'+i]	= o.jml_order;
		params['jml_receive'+i]	= o.jml_receive;
		params['req_line'+i]	= o.req_line;
		params['harga'+i]	= o.harga;
		
    }
    return params;
}

function loaddatagridkepemilikan_pemesananKegudang_farmasi(param)
{
	if (param==='' || param===undefined)
	{
		param={
			text: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/gudang_farmasi/functionpemesanan/getKepemilikan",
		params: param,
		failure: function(o)
		{
			var cst = Ext.decode(o.responseText);
		},
		success: function(o) {
			Kegudang_farmasi.form.Grid.pemesanan.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsgridcombomilik_pemesananKegudang_farmasi.recordType;
				var o=cst['listData'][i];

				recs.push(new recType(o));
				dsgridcombomilik_pemesananKegudang_farmasi.add(recs);
				//console.log(o);
			}
		}
	});
}


