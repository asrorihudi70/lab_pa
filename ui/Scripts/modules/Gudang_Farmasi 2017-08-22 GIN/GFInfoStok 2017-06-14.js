var GFInfoStok={
	vars:{
		appTitle:'Informasi Stok',
		nowDate: new Date()
	},
	form:{
		ArrayStore:{
			main:new Ext.data.ArrayStore({id: 0,fields: ['kd_prd','nama_obat','jml_stok_apt','satuan','min_stok'],data:[]})
		},
		Grid:{
			main:null
		},
		TextField:{
			no:null
		}
	},
	refresh: function(){
		var $this=this;
		loadMask.show();
		var a=[];
		a.push({name: 'kd_prd',value:$this.form.TextField.no.getValue()});
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			url:baseURL + "index.php/gudang_farmasi/functionGFInfoStok/initList",
			data:a,
			success: function(r){
				loadMask.hide();
				if(r.processResult=='SUCCESS'){
					$this.form.ArrayStore.main.loadData([],false);
					for(var i=0,iLen=r.listData.length; i<iLen ;i++){
						var records=[];
						records.push(new $this.form.ArrayStore.main.recordType());
						$this.form.ArrayStore.main.add(records);
						$this.form.ArrayStore.main.data.items[i].data.kd_prd=r.listData[i].kd_prd;
						$this.form.ArrayStore.main.data.items[i].data.nama_obat=r.listData[i].nama_obat;
						$this.form.ArrayStore.main.data.items[i].data.satuan=r.listData[i].satuan;
						$this.form.ArrayStore.main.data.items[i].data.jml_stok_apt=r.listData[i].jml_stok_apt;
						$this.form.ArrayStore.main.data.items[i].data.milik=r.listData[i].milik;
						$this.form.ArrayStore.main.data.items[i].data.min_stok=r.listData[i].min_stok;
						$this.form.ArrayStore.main.data.items[i].data.kd_milik=r.listData[i].kd_milik;
						$this.form.ArrayStore.main.data.items[i].data.kd_unit_far=r.listData[i].kd_unit_far;
					}
					$this.form.Grid.main.getView().refresh();
				}else{
					Ext.Msg.alert('Gagal',r.processMessage);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
	},
	getParams:function(){
		var $this=this;
		var params={
			jmllist	: $this.form.ArrayStore.main.getCount()
		};
		for(var i=0; i<$this.form.ArrayStore.main.getCount() ; i++){
			var o=$this.form.ArrayStore.main.getRange()[i].data;
			if(o.kd_prd != undefined || o.kd_prd != ''){
				params['kd_prd-'+i]=o.kd_prd;
				params['kd_milik-'+i]=o.kd_milik;
				params['kd_unit_far-'+i]=o.kd_unit_far;
				params['min_stok-'+i]=o.min_stok;
			}
		}
		
		return params;
	},
	save:function(){
		var $this=this;
		Ext.Msg.confirm('Konfirmasi', 'Apakah anda ingin menyimpan perubahan min stok?', function (id, value) { 
			if (id === 'yes') { 
				loadMask.show();
				$.ajax({
					url:baseURL + "index.php/gudang_farmasi/functionGFInfoStok/save",
					dataType:'JSON',
					type: 'POST',
					data:$this.getParams(),
					success: function(r){
						loadMask.hide();
						if(r.processResult=='SUCCESS'){
							Ext.Msg.alert('Sukses','Data Berhasi Disimpan.');
						}else{
							Ext.Msg.alert('Gagal',r.processMessage);
						}
					},
					error: function(jqXHR, exception) {
						loadMask.hide();
						Nci.ajax.ErrorMessage(jqXHR, exception);
					}
				});
			} 
		}, this);
	},
	
	main: function(modId){
		var $this=this;
		$this.form.Grid.main= new Ext.grid.EditorGridPanel({
			xtype: 'editorgrid',
			title: '',
			store: $this.form.ArrayStore.main,
			autoScroll: true,
			flex: 1,
			columnLines: true,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			colModel: new Ext.grid.ColumnModel([
				new Ext.grid.RowNumberer(),
				{
					header		: 'Kode Obat',
					width		: 100,
					sortable	: true,
					dataIndex	: 'kd_prd'
				},{
					header: 'Nama Obat',
					dataIndex: 'nama_obat',
					sortable: true,
					width: 300
				},{
					header:'Stok',
					align:'right',
					dataIndex: 'jml_stok_apt',						
					width: 100,
					sortable: true
				},{
					header: 'Satuan',
					dataIndex: 'satuan',
					sortable: true,
					width: 100
				},{
					header: 'Kepemilikan',
					dataIndex: 'milik',
					sortable: true,
					width: 100
				},{
					align:'right',
					dataIndex: 'min_stok',
					header: 'Min Stok',
					sortable: true,
					width: 65,
					editor:new Ext.form.NumberField({
						enableKeyEvents:true,
						listeners:{
							keyDown: function(a,b,c){
								Ext.getCmp('BtnSimpanGDInfoStok').enable();
								// if(b.getKey()==13){
									// var line	= this.indeks;
									// var o=$this.form.ArrayStore.main.getRange()[line].data;
									// o.min_stok=a.getValue();
									
									// if(parseFloat(o.min_stok) == 0){
										// Ext.Msg.alert('Warning','Min stok tidak boleh 0');
									// }
								// }
							},
						}
					})
				},{
					header: 'kd_unit_far',
					dataIndex: 'kd_unit_far',
					sortable: true,
					hidden:true,
					width: 100
				},{
					header: 'kd_milik',
					dataIndex: 'kd_milik',
					sortable: true,
					hidden:true,
					width: 100
				},
			]),
			tbar:[
				{
					text	: 'Simpan min stok',
					tooltip	: 'save',
					id		: 'BtnSimpanGDInfoStok',
					iconCls	: 'save',
					disabled: true,
					handler: function(){					
						$this.save();	
					}                        
				}
			]
		});
		
		return new Ext.FormPanel({
			title: $this.vars.appTitle,
			iconCls: 'Studi_Lanjut',
			id: modId,
			region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
			}, 
			closable: true,        
			border: false,  
			items: [$this.form.Grid.main],
			tbar:[
				{
		            xtype: 'buttongroup',
		            columns: 11,
		            defaults: {
		                scale: 'small'
		        	},
		        	frame: false,
		            items:[
			            { 
							xtype: 'tbtext', 
							text: 'Nama Obat : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},$this.form.TextField.no= new Ext.form.TextField({
							name:'kd_prd',
							width: 200,
							height: 25,
							enableKeyEvents:true,
							listeners:{ 
								'specialkey' : function(){
									if (Ext.EventObject.getKey() === 13){
										$this.refresh();
									} 						
								},
								keyDown: function(a,b,c){
									Ext.getCmp('BtnSimpanGDInfoStok').disable();
								}
							}
						}),{	 
							xtype: 'tbspacer',
							width: 10,
							height: 25
						},{
							xtype: 'button',
							text: 'Cari',
							iconCls: 'refresh',
							tooltip: 'Cari',
							id:'BtnCariGFInfoStok',
							style:{paddingLeft:'30px'},
							width:150,
							handler: function(){					
								$this.refresh();	
							}                        
						}
					]
				}
			]
	 	});
		return $this.form.Panel.search;
	},
	init: function(){
		var $this=this;
		CurrentPage.page = $this.main(CurrentPage.id);
		mainPage.add(CurrentPage.page);
		mainPage.setActiveTab(CurrentPage.id);
		$this.refresh();
		shortcut.set({
			code:'main',
			list:[
				{
					key:'ctrl+s',
					fn:function(){
						Ext.getCmp('BtnSimpanGDInfoStok').el.dom.click();
					}
				},
				// {
					// key:'ctrl+f',
					// fn:function(){
						// Ext.getCmp('BtnCariGFInfoStok').el.dom.click();
					// }
				// },
			]
		});
	}
};
GFInfoStok.init();