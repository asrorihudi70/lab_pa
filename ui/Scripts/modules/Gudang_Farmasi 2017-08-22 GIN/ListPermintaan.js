var dataSource_viListPermintaan;
var NamaForm_viListPermintaan="Daftar Permintaan";
var mod_name_viListPermintaan="viListPermintaan";
var rowSelected_viListPermintaan;
var setLookUps_viListPermintaan;
var now_viListPrmintaan= new Date();
var GridDataView_viListPermintaan;


var CurrentData_viListPermintaan =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viListPermintaan(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dataGrid_viListPermintaan(mod_id_viListPermintaan){	
    // Field kiriman dari Project Net.
    var FieldMaster_viListPermintaan = 
	[
		
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viListPermintaan = new WebApp.DataStore
	({
        fields: FieldMaster_viListPermintaan
    });
	
   RefreshDataGrid_viListPermintaan();
   loadDataComboUnitFar_ListPermintaan();
   
    // Grid Apotek Perencanaan # --------------
	GridDataView_viListPermintaan = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: 'Daftar Permintaan',
			store: dataSource_viListPermintaan,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viListPermintaan = undefined;
							rowSelected_viListPermintaan = dataSource_viListPermintaan.getAt(row);
							CurrentData_viListPermintaan
							CurrentData_viListPermintaan.row = row;
							CurrentData_viListPermintaan.data = rowSelected_viListPermintaan.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viListPermintaan = dataSource_viListPermintaan.getAt(ridx);
					if (rowSelected_viListPermintaan != undefined)
					{
						
					}
					else
					{
						
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid List Permintaan
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header: 'M',
						dataIndex: 'kd_milik',
						sortable: false,
						align:'center',
						width: 10
						
					},
					//-------------- ## --------------
					{
						header: 'Ro Number',
						dataIndex: 'no_ro_unit',
						sortable: false,
						width: 40
						
					},
					//-------------- ## --------------
					{
						header: 'Unit Minta',
						dataIndex: 'nm_unit_far',
						sortable: false,
						width: 40
						
					},
					//-------------- ## --------------
					{
						header:'Kode',
						dataIndex: 'kd_prd',						
						width: 25,
						sortable: false,
						hideable:false,
                        menuDisabled:true
					},
					//-------------- ## --------------
					{
						header:'Nama Obat',
						dataIndex: 'nama_obat',						
						width: 60,
						sortable: false,
						hideable:false,
                        menuDisabled:true
					},
					//-------------- ## --------------
					{
						header: 'Satuan',
						dataIndex: 'satuan',
						sortable: false,
						hideable:false,
                        menuDisabled:true,
						width: 25
					},
					//-------------- ## --------------
					{
						header: 'Qty B',
						dataIndex: 'qty_b',
						sortable: false,
						hideable:false,
                        menuDisabled:true,
						align:'right',
						width: 20
					},
					//-------------- ## --------------
					{
						header: 'Frac',
						dataIndex: 'fractions',
						sortable: false,
						hideable:false,
                        menuDisabled:true,
						align:'right',
						width: 20
					},
					//-------------- ## --------------
					{
						header: 'Qty K',
						dataIndex: 'qty',
						sortable: false,
						hideable:false,
                        menuDisabled:true,
						align:'right',
						width: 20
					},
					//-------------- ## --------------
					{
						header: 'Qty Realisasi',
						dataIndex: 'qty_ordered',
						align:'right',
						sortable: false,
						width: 30
					},
					//-------------- ## --------------
					{
						header: 'Status',
						dataIndex: 'status',
						sortable: false,
						width: 30
					}
					//-------------- ## --------------
					
					//-------------- ## --------------
				]
			),
			viewConfig: 
			{
				forceFit: true
			}
		}
    )
	
	var pencarianListPermintaan = new Ext.FormPanel({
        labelAlign: 'top',
        frame:true,
        title: 'Pencarian',
        bodyStyle:'padding:5px 5px 0',
        //width: 600,
        items: [
		{
			layout: 'column',
			border: false,
			items:
			[
				{
					columnWidth:.98,
					layout: 'absolute',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					width: 500,
					height: 80,
					anchor: '100% 100%',
					items:
					[
						{
							x: 10,
							y: 3,
							xtype: 'label',
							text: 'No Permintaan / RO Number'
						},
						{
							x: 150,
							y: 3,
							xtype: 'label',
							text: ':'
						},
						{
							x: 160,
							y: 0,
							xtype: 'textfield',
							id: 'txtFilterNoPermintaan_listPermintaan',
							name: 'txtFilterNoPermintaan_listPermintaan',
							width: 240,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										RefreshDataGrid_viListPermintaan(
											Ext.getCmp('txtFilterNoPermintaan_listPermintaan').getValue(),
											Ext.getCmp('dfTanggalAwalPermintaan_ResepRWJ').getValue(),
											Ext.getCmp('dfTanggalAkhirPermintaan_ResepRWJ').getValue(),
											Ext.getCmp('cboUnitFar_ListPermintaan').getValue(),
											Ext.getCmp('cboStatusAccListPermintaan').getValue()
										);
									} 						
								}
							}
						},
						{
							x: 10,
							y: 30,
							xtype: 'label',
							text: 'Tanggal Permintaan'
						},
						{
							x: 150,
							y: 30,
							xtype: 'label',
							text: ':'
						},
						{
							x: 160,
							y: 30,
							xtype: 'datefield',
							width : 100,	
							format: 'd/M/Y',
							name: 'dfTanggalAwalPermintaan_ResepRWJ',
							id: 'dfTanggalAwalPermintaan_ResepRWJ',
							value:now_viListPrmintaan,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										RefreshDataGrid_viListPermintaan(
											Ext.getCmp('txtFilterNoPermintaan_listPermintaan').getValue(),
											Ext.getCmp('dfTanggalAwalPermintaan_ResepRWJ').getValue(),
											Ext.getCmp('dfTanggalAkhirPermintaan_ResepRWJ').getValue(),
											Ext.getCmp('cboUnitFar_ListPermintaan').getValue(),
											Ext.getCmp('cboStatusAccListPermintaan').getValue()
										);
									} 						
								}
							}
						},
						{
							x: 270,
							y: 33,
							xtype: 'label',
							text: 's/d'
						},
						{
							x: 300,
							y: 30,
							xtype: 'datefield',
							width : 100,	
							format: 'd/M/Y',
							name: 'dfTanggalAkhirPermintaan_ResepRWJ',
							id: 'dfTanggalAkhirPermintaan_ResepRWJ',
							value:now_viListPrmintaan,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										RefreshDataGrid_viListPermintaan(
											Ext.getCmp('txtFilterNoPermintaan_listPermintaan').getValue(),
											Ext.getCmp('dfTanggalAwalPermintaan_ResepRWJ').getValue(),
											Ext.getCmp('dfTanggalAkhirPermintaan_ResepRWJ').getValue(),
											Ext.getCmp('cboUnitFar_ListPermintaan').getValue(),
											Ext.getCmp('cboStatusAccListPermintaan').getValue()
										);
									} 						
								}
							}
						},
						{
							x: 420,
							y: 3,
							xtype: 'label',
							text: 'Unit minta'
						},
						{
							x: 470,
							y: 3,
							xtype: 'label',
							text: ':'
						},
						mComboUnitFarListPermintaan(),
						{
							x: 420,
							y: 30,
							xtype: 'label',
							hidden: true,
							text: 'Status'
						},
						{
							x: 470,
							y: 30,
							xtype: 'label',
							text: ':',
							hidden: true,
						},
						mComboStatusAccListPermintaan(),
						{
							x: 10,
							y: 60,
							xtype: 'label',
							text: '*) Tekan enter untuk mencari',
							style:{'font-size':'10px','color':'darkblue'}
						},
						//----------------------------------------
						{
							x: 680,
							y: 30,
							xtype: 'button',
							text: 'Refresh',
							iconCls: 'refresh',
							style:{paddingLeft:'30px'},
							width:150,
							id: 'BtnFilterGridCari_viListPermintaan',
							handler: function() 
							{					
								RefreshDataGrid_viListPermintaan();
							}                        
						}
					]
				}
			]
		}
		]	
		
	})

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viListPermintaan = new Ext.Panel
    (
		{
			title: NamaForm_viListPermintaan,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viListPermintaan,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ pencarianListPermintaan,
					GridDataView_viListPermintaan],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viListPermintaan,
		            columns: 21,
		            defaults: {
					scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viListPermintaan;
    //-------------- # End form filter # --------------
}


function RefreshDataGrid_viListPermintaan(no_ro_unit,tglAwal,tglAkhir,kd_unit_far,status){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gudang_farmasi/functionListPermintaan/itemGrid",
			params: {
				no_ro_unit:no_ro_unit,
				tglAwal:tglAwal,
				tglAkhir:tglAkhir,
				kd_unit_far:kd_unit_far,
				status:status
			},
			failure: function(o)
			{
				loadMask.hide()
				ShowPesanErrorListPermintaan('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dataSource_viListPermintaan.removeAll();
					var recs=[],
						recType=dataSource_viListPermintaan.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dataSource_viListPermintaan.add(recs);
					
					GridDataView_viListPermintaan.getView().refresh();
				}
				else 
				{
					loadMask.hide()
					ShowPesanErrorListPermintaan('Gagal membaca daftar permintaan', 'Error');
				};
			}
		}
		
	)
}

function ShowPesanWarningListPermintaan(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorListPermintaan(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};


function ShowPesanInfoListPermintaan(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};

function loadDataComboUnitFar_ListPermintaan(param){
	if (param==='' || param===undefined) {
		param={
			text: '0',
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/gudang_farmasi/functionListPermintaan/getUnitFar",
		params: param,
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cboUnitFar_ListPermintaan.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = ds_UnitFar_ListPermintaan.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				ds_UnitFar_ListPermintaan.add(recs);
				console.log(o);
			}
		}
	});
}


function mComboUnitFarListPermintaan()
{
	var Field = ['kd_unit_far','nm_unit_far'];
    ds_UnitFar_ListPermintaan = new WebApp.DataStore({fields: Field});
    cboUnitFar_ListPermintaan = new Ext.form.ComboBox
	(
            {
                x: 480,
                y: 0,
                id:'cboUnitFar_ListPermintaan',
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                mode: 'local',
                selectOnFocus:true,
                forceSelection: true,
                emptyText:'SEMUA',
                fieldLabel: 'Pendaftaran Per Shift ',
                width:200,
                store: ds_UnitFar_ListPermintaan,
                valueField: 'kd_unit_far',
                displayField: 'nm_unit_far',
                value:'',
                listeners:
                {
                        'select': function(a,b,c)
                        {
							RefreshDataGrid_viListPermintaan(
								Ext.getCmp('txtFilterNoPermintaan_listPermintaan').getValue(),
								Ext.getCmp('dfTanggalAwalPermintaan_ResepRWJ').getValue(),
								Ext.getCmp('dfTanggalAkhirPermintaan_ResepRWJ').getValue(),
								Ext.getCmp('cboUnitFar_ListPermintaan').getValue(),
								Ext.getCmp('cboStatusAccListPermintaan').getValue()
							);
                        },
						'specialkey' : function(){
							if (Ext.EventObject.getKey() === 13) {
								RefreshDataGrid_viListPermintaan(
									Ext.getCmp('txtFilterNoPermintaan_listPermintaan').getValue(),
									Ext.getCmp('dfTanggalAwalPermintaan_ResepRWJ').getValue(),
									Ext.getCmp('dfTanggalAkhirPermintaan_ResepRWJ').getValue(),
									Ext.getCmp('cboUnitFar_ListPermintaan').getValue(),
									Ext.getCmp('cboStatusAccListPermintaan').getValue()
								);
							}
						}
                }
            }
	);
	return cboUnitFar_ListPermintaan;
};

function mComboStatusAccListPermintaan(){
  var cboStatusAccListPermintaan = new Ext.form.ComboBox({
        id:'cboStatusAccListPermintaan',
        x: 480,
        y: 30,
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:true,
		editable: false,
		hidden: true,
        mode: 'local',
        width: 200,
        emptyText:'Semua',
        fieldLabel: '',
		tabIndex:5,
        store: new Ext.data.ArrayStore( {
            id: 0,
            fields:[
                'Id',
                'displayText'
            ],
            data: [[1, 'Semua'],[2, 'Belum Acc'], [3, 'Ditolak']]
        }),
        valueField: 'Id',
        displayField: 'displayText',
        value:'',
        listeners:{
			'select': function(a,b,c){
				RefreshDataGrid_viListPermintaan(
					Ext.getCmp('txtFilterNoPermintaan_listPermintaan').getValue(),
					Ext.getCmp('dfTanggalAwalPermintaan_ResepRWJ').getValue(),
					Ext.getCmp('dfTanggalAkhirPermintaan_ResepRWJ').getValue(),
					Ext.getCmp('cboUnitFar_ListPermintaan').getValue(),
					Ext.getCmp('cboStatusAccListPermintaan').getValue()
				);
			},
			'specialkey' : function(){
				if (Ext.EventObject.getKey() === 13) {
					RefreshDataGrid_viListPermintaan(
						Ext.getCmp('txtFilterNoPermintaan_listPermintaan').getValue(),
						Ext.getCmp('dfTanggalAwalPermintaan_ResepRWJ').getValue(),
						Ext.getCmp('dfTanggalAkhirPermintaan_ResepRWJ').getValue(),
						Ext.getCmp('cboUnitFar_ListPermintaan').getValue(),
						Ext.getCmp('cboStatusAccListPermintaan').getValue()
					);
				}
			}
        }
	});
	return cboStatusAccListPermintaan;
};
