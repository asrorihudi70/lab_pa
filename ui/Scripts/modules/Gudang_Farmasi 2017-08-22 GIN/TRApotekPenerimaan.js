var GFPenerimaan={};
GFPenerimaan.form={};
GFPenerimaan.func={};
GFPenerimaan.vars={};
GFPenerimaan.func.parent=GFPenerimaan;
GFPenerimaan.form.Button={};
GFPenerimaan.form.CheckBox={};
GFPenerimaan.form.ComboBox={}
GFPenerimaan.form.DataStore={};
GFPenerimaan.form.DateField={};
GFPenerimaan.form.ArrayStore={};
GFPenerimaan.form.Record={};
GFPenerimaan.form.FormPanel={};//b
GFPenerimaan.form.Grid={};//b
GFPenerimaan.form.Panel={};
GFPenerimaan.form.TextField={};//c
GFPenerimaan.form.Button={};
var cat_prd;
var GFStatusPosting='0';
var tglexpireGFPenerimaan = new Date();
var tglNowGFPenerimaan = new Date();

GFPenerimaan.form.ArrayStore.a	= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_prd','kd_satuan','nama_obat','kd_sat_besar','harga_beli'],
	data: []
});
GFPenerimaan.form.ArrayStore.b	= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_pabrik','pabrik'],
	data: []
});
GFPenerimaan.form.ArrayStore.po	= new Ext.data.ArrayStore({
	id: 0,
	fields:['po_number','kd_prd','kd_satuan','nama_obat','kd_sat_besar','harga_beli'],
	data: []
});

GFPenerimaan.form.ArrayStore.c	= new Ext.data.ArrayStore({
	id: 0,
	fields:['posting','no_obat_in','tgl_obat_in','vendor','remark'],
	data: []
});
GFPenerimaan.func.refresh=function(){
	// loadMask.show();
	// $.ajax({
		// type: 'POST',
		// dataType:'JSON',
		// url:baseURL + "index.php/gudang_farmasi/functionGFPenerimaan/initList",
		// data:$('#'+GFPenerimaan.form.FormPanel.b.id).find('form').eq(0).serialize(),
		// success: function(r){
			// loadMask.hide();
			// if(r.processResult=='SUCCESS'){
				// GFPenerimaan.form.ArrayStore.c.loadData([],false);
				// for(var i=0,iLen=r.listData.length; i<iLen ;i++){
					// var records=[];
					// records.push(new GFPenerimaan.form.ArrayStore.c.recordType());
					// GFPenerimaan.form.ArrayStore.c.add(records);
					// GFPenerimaan.form.ArrayStore.c.data.items[i].data.posting=r.listData[i].posting
					// GFPenerimaan.form.ArrayStore.c.data.items[i].data.no_obat_in=r.listData[i].no_obat_in
					// GFPenerimaan.form.ArrayStore.c.data.items[i].data.tgl_obat_in=r.listData[i].tgl_obat_in
					// GFPenerimaan.form.ArrayStore.c.data.items[i].data.vendor=r.listData[i].vendor
					// GFPenerimaan.form.ArrayStore.c.data.items[i].data.remark=r.listData[i].remark
				// }
				// GFPenerimaan.form.Grid.b.getView().refresh();
			// }else{
				// Ext.Msg.alert('Gagal',r.processMessage);
			// }
		// },
		// error: function(jqXHR, exception) {
			// loadMask.hide();
			// Nci.ajax.ErrorMessage(jqXHR, exception);
		// }
	// });
	var $this=this;
	loadMask.show();
	var store=GFPenerimaan.form.Grid.b.getStore();
	var ser=$('#'+$this.parent.form.FormPanel.b.id).find('form').eq(0).serialize();
	$.ajax({
		type: 'POST',
		dataType:'JSON',
		url:baseURL + "index.php/gudang_farmasi/functionGFPenerimaan/initList",
		data:ser,
		success: function(r){
			loadMask.hide();
			if(r.processResult=='SUCCESS'){
				store.loadData([],false);
				for(var i=0,iLen=r.listData.length; i<iLen ;i++){
					var records=[];
					records.push(new store.recordType());
					store.add(records);
					store.data.items[i].data.posting=r.listData[i].posting
					store.data.items[i].data.no_obat_in=r.listData[i].no_obat_in
					store.data.items[i].data.tgl_obat_in=r.listData[i].tgl_obat_in
					store.data.items[i].data.vendor=r.listData[i].vendor
					store.data.items[i].data.remark=r.listData[i].remark
					store.data.items[i].data.milik=r.listData[i].milik
				}
				$this.parent.form.Grid.b.getView().refresh();
			}else{
				Ext.Msg.alert('Gagal',r.processMessage);
			}
		},
		error: function(jqXHR, exception) {
			loadMask.hide();
			Nci.ajax.ErrorMessage(jqXHR, exception);
		}
	});
}

GFPenerimaan.func.posting=function(posting,kd_unit_far,kd_milik,produkList){
	var param=[];
	param.push({name:'posting',value:posting});
	param.push({name:'kd_unit_far',value:kd_unit_far});
	param.push({name:'kd_milik',value:kd_milik});
	for(var i=0, iLen=produkList.length; i<iLen; i++){
		param.push({name:'kd_prd[]',value:produkList[i]});
	}
}
GFPenerimaan.func.convert=function(str) {
    var date = new Date(str),
        mnth = ("0" + (date.getMonth()+1)).slice(-2),
        day  = ("0" + date.getDate()).slice(-2);
    return [ date.getFullYear(), mnth, day ].join("-");
};

GFPenerimaan.func.save=function(callback){
	var subtot=0;
	
	for(var i=0, iLen=dsDataGrdJab_viApotekPenerimaan.getCount(); i<iLen ; i++){
		var o=dsDataGrdJab_viApotekPenerimaan.data.items[i].data;
		if(o.nama_obat == '' || o.nama_obat== undefined){
			dsDataGrdJab_viApotekPenerimaan.removeAt(i);
		}
	}
	
	var param=$('#'+GFPenerimaan.form.FormPanel.a.id).find('form').eq(0).serializeArray(),e=false;
	if(GFPenerimaan.form.TextField.a.getValue()==''){
		e=true;
		Ext.Msg.alert('Error','Harap Isi PBF.');
		return;
	}
	if(dsDataGrdJab_viApotekPenerimaan.getCount()==0){
		e=true;
		Ext.Msg.alert('Error','Isi Produk Obat.');
		return;
	}
	param.push({name:'count',value:dsDataGrdJab_viApotekPenerimaan.getCount()});
	param.push({name:'startDate',value:Ext.get('startDateGFPenerimaan').getValue()});
	param.push({name:'lastDate',value:Ext.get('lastDateGFPenerimaan').getValue()});
	for(var i=0, iLen=dsDataGrdJab_viApotekPenerimaan.getCount(); i<iLen ; i++){
		var totharga=0;
		var harga_beli_obat=0;
		var o=dsDataGrdJab_viApotekPenerimaan.data.items[i].data;
		var po;
		if(o.qty_b==0){
			e=true;
			Ext.Msg.alert('Error','Isi Qty B Pada Baris Ke-'+(i+1));
			return;
		}
		if(o.jml_in_obat==0){
			e=true;
			Ext.Msg.alert('Error','Isi Qty Pada Baris Ke-'+(i+1));
			return;
		}
		// if(o.tgl_expire == undefined || o.tgl_expire==''){
			// e=true;
			// Ext.Msg.alert('Error','Isi Tgl Expire Baris Ke-'+(i+1));
			// return;
		// }
		if(o.batch == undefined || o.batch==''){
			e=true;
			Ext.Msg.alert('Error','Isi Batch Baris Ke-'+(i+1));
			return;
		}
		if(o.tag_disc == true && (o.tag == false || o.tag == undefined)){			
			e=true;
			Ext.Msg.alert('Error','Ceklis tag pada baris ke-'+(i+1)+', jika tag disc di ceklis!');
			return;
		}
		
		if(o.ketppn == 10){
			//totharga=toInteger(o.harga_beli)+((toInteger(o.harga_beli)*10)/100);
			subtot = parseFloat(o.qty_b) * (toInteger(o.harga_beli) + (toInteger(o.harga_beli)*10)/100);
			totharga = subtot / (parseFloat(o.qty_b) * parseFloat(o.fractions));
		} else{
			//totharga=toInteger(o.harga_beli);
			subtot = parseFloat(o.qty_b) * toInteger(o.harga_beli);
			totharga = subtot / (parseFloat(o.qty_b) * parseInt(o.fractions));
		}
		
		harga_beli_obat = parseFloat(o.harga_beli)/parseFloat(o.fractions);
		
		
		if(o.po_number == "*"){
			po="";
		}else{
			po=o.po_number;
		}
		
		param.push({name:'po_number-'+i,value: po});
		param.push({name:'kd_prd-'+i,value: o.kd_prd});
		param.push({name:'jml_in_obt-'+i,value: o.jml_in_obat});
		param.push({name:'hrg_beli_obt-'+i,value:harga_beli_obat});
		param.push({name:'harga_satuan-'+i,value: totharga});
		param.push({name:'tot_hrg_beli_obt-'+i,value: totharga});
		param.push({name:'tgl_exp-'+i,value: convertTglGFPenerimaan(o.tgl_expire)});
		param.push({name:'apt_discount-'+i,value: o.apt_discount});
		param.push({name:'ppn_item-'+i,value: o.ppn});
		param.push({name:'apt_disc_rupiah-'+i,value: o.apt_disc_rupiah});
		param.push({name:'frac-'+i,value: o.fractions});
		param.push({name:'boxqty-'+i,value: o.qty_b});
		param.push({name:'sub_total-'+i,value: o.sub_total});
		if(o.tag != undefined && o.tag == true){
			param.push({name:'tag-'+i,value: 1});
		}else{
			param.push({name:'tag-'+i,value: 0});
		}
		if(o.tag_disc != undefined && o.tag_disc == true){
			param.push({name:'tag_disc-'+i,value: 1});
		}else{
			param.push({name:'tag_disc-'+i,value: 0});
		}
		if(o.batch != undefined){
			param.push({name:'batch-'+i,value: o.batch});
		}else{
			param.push({name:'batch-'+i,value:''});
		}
		if(o.kd_pabrik != undefined){
			param.push({name:'kd_pabrik-'+i,value:o.kd_pabrik});
		}else{
			param.push({name:'kd_pabrik-'+i,value:''});
		}
	}
	if(GFPenerimaan.form.TextField.b.getValue()==''){
		loadMask.show();
		$.ajax({
			url:baseURL + "index.php/gudang_farmasi/functionGFPenerimaan/save",
			dataType:'JSON',
			type: 'POST',
			data:param,
			success: function(r){
				loadMask.hide();
				if(r.processResult=='SUCCESS'){
					GFPenerimaan.form.TextField.b.setValue(r.resultObject.code);
					for(var i=0; i<dsDataGrdJab_viApotekPenerimaan.getCount() ; i++){
						var o=dsDataGrdJab_viApotekPenerimaan.getRange()[i].data;
						o.no_obat_in=r.resultObject.code;
					}
					Ext.Msg.alert('Sukses','Data Berhasi Disimpan.');
					if(callback != undefined){
						callback();
					}
					Ext.getCmp('btnPostingGFPenerimaan').enable();
					GFPenerimaan.func.refresh();
				}else if(r.processResult=='ERROR DUEDATE') {
					Ext.Msg.alert('Gagal',r.processMessage);
				}
				else {
					Ext.Msg.alert('Gagal',r.processMessage);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
	}else{
		loadMask.show();
		$.ajax({
			url:baseURL + "index.php/gudang_farmasi/functionGFPenerimaan/update",
			dataType:'JSON',
			type: 'POST',
			data:param,
			success: function(r){
				loadMask.hide();
				if(r.processResult=='SUCCESS'){
					Ext.Msg.alert('Sukses','Data Berhasi Diubah.');
					for(var i=0; i<dsDataGrdJab_viApotekPenerimaan.getCount() ; i++){
						var o=dsDataGrdJab_viApotekPenerimaan.getRange()[i].data;
						o.no_obat_in=GFPenerimaan.form.TextField.b.getValue();
					}
					if(callback != undefined){
						callback();
					}
					Ext.getCmp('btnPostingGFPenerimaan').enable();
					GFPenerimaan.func.refresh();
				}else{
					Ext.Msg.alert('Gagal',r.processMessage);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
	}
}
var dataSource_viApotekPenerimaan;
var selectCount_viApotekPenerimaan=50;
var NamaForm_viApotekPenerimaan="Penerimaan ";
var selectCountStatusPostingApotekPenerimaan='Belum Posting';
var mod_name_viApotekPenerimaan="viApotekPenerimaan";
var now_viApotekPenerimaan= new Date();
var addNew_viApotekPenerimaan;
var rowSelected_viApotekPenerimaan;
var setLookUps_viApotekPenerimaan;
var mNoKunjungan_viApotekPenerimaan='';
var selectSetVendor;

var CurrentData_viApotekPenerimaan ={
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viApotekPenerimaan(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
//GFPenerimaan.func.refresh();
Q(GFPenerimaan.form.Grid.b).refresh();
function ShowPesanWarningPenerimaanGF(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorPenerimaanGF(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:290
		}
	);
};


function ShowPesanInfoPenerimaanGF(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:350
		}
	);
};
function dataGrid_viApotekPenerimaan(mod_id_viApotekPenerimaan){	
	GFPenerimaan.form.Grid.b = Q().table({
		flex: 1,
		rowdblclick: function (sm, ridx, cidx,store){
			rowSelected_viApotekPenerimaan = store.getAt(ridx);
			if (rowSelected_viApotekPenerimaan != undefined){
				setLookUp_viApotekPenerimaan(rowSelected_viApotekPenerimaan.data);
			}else{
				setLookUp_viApotekPenerimaan();
			}
		},
		rowselect: function(sm, row, rec,store){
			rowSelected_viApotekPenerimaan = undefined;
			rowSelected_viApotekPenerimaan = store.getAt(row);
			CurrentData_viApotekPenerimaan
			CurrentData_viApotekPenerimaan.row = row;
			CurrentData_viApotekPenerimaan.data = rowSelected_viApotekPenerimaan.data;
			if (rowSelected_viApotekPenerimaan.data.posting==='0')
			{
				Ext.getCmp('btnHapusTrx_viApotekPenerimaanGF').enable();
			}
			else
			{
				Ext.getCmp('btnHapusTrx_viApotekPenerimaanGF').disable();
			}
			GFStatusPosting=rowSelected_viApotekPenerimaan.data.posting;
		},
		tbar:{
			xtype: 'toolbar',
			items: [
				{
					xtype: 'button',
					text: 'Tambah [F1]',
					iconCls: 'add',
					tooltip: 'Edit Data',
					id:'btnTambahPenerimaanGFPenerimaan',
					handler: function(sm, row, rec){
						GFStatusPosting='0';
						setLookUp_viApotekPenerimaan();
					}
				},
				{
					xtype: 'button',
					text: 'Ubah',
					iconCls: 'Edit_Tr',
					tooltip: 'Edit Data',
					handler: function(sm, row, rec){
						if(rowSelected_viApotekPenerimaan != undefined){
							setLookUp_viApotekPenerimaan(rowSelected_viApotekPenerimaan.data)
						}else{
							Ext.Msg.alert('Information','Data Belum Dipilih.');
						}
					}
				},
				{
						xtype: 'button',
						text: 'Hapus Transaksi',
						iconCls: 'remove',
						tooltip: 'Hapus Data',
						disabled:true,
						id: 'btnHapusTrx_viApotekPenerimaanGF',
						handler: function(sm, row, rec)
						{
							var datanya=rowSelected_viApotekPenerimaan.data;
							if (datanya===undefined){
								ShowPesanWarningPenerimaanGF('Belum ada data yang dipilih','Gudang Farmasi');
							}
							else
							{
								 Ext.Msg.show({
									title: 'Hapus Transaksi',
									msg: 'Anda yakin akan menghapus data transaksi ini ?',
									buttons: Ext.MessageBox.YESNO,
									fn: function (btn) {
										if (btn == 'yes')
										{
											
											console.log(datanya);
											var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan Pembatalan Transaksi:', function (btn, combo) {
														if (btn == 'ok')
														{
															var variablebatalhistori_penerimaan = combo;
															if (variablebatalhistori_penerimaan != '')
															{
																 Ext.Ajax.request({
										   
																		url: baseURL + "index.php/apotek/functionAPOTEK/hapusTrxPenerimaanGF",
																		 params: {
																			
																			no_obat_in: datanya.no_obat_in,
																			tgl_obat_in: datanya.tgl_obat_in,
																			alasan: variablebatalhistori_penerimaan
																		
																		},
																		failure: function(o)
																		{
																			 var cst = Ext.decode(o.responseText);
																			ShowPesanWarningPenerimaanGF('Data transaksi tidak dapat dihapus','Gudang Farmasi');
																		},	    
																		success: function(o) {
																			var cst = Ext.decode(o.responseText);
																			if (cst.success===true)
																			{
																				Q(GFPenerimaan.form.Grid.b).refresh();
																				ShowPesanInfoPenerimaanGF('Data transaksi Berhasil dihapus','Gudang Farmasi');
																				Ext.getCmp('btnHapusTrx_viApotekPenerimaanGF').disable();
																			}
																			else
																			{
																				ShowPesanErrorPenerimaanGF('Data transaksi tidak dapat dihapus','Gudang Farmasi');
																			}
																			
																		}
																
																})
															} else
															{
																ShowPesanWarningPenerimaanGF('Silahkan isi alasan terlebih dahaulu', 'Keterangan');

															}
														}

													}); 
											
										}
									},
									icon: Ext.MessageBox.QUESTION
								});
							} 
						}
					},
			]
		},
		ajax:function(data,callback){
			loadMask.show();
			var a=$('#'+GFPenerimaan.form.FormPanel.b.id).find('form').eq(0).serialize();
			a+='&start='+data.start+'&size='+data.size;
			$.ajax({
				type: 'POST',
				dataType:'JSON',
				url:baseURL + "index.php/gudang_farmasi/functionGFPenerimaan/initList",
				data:a,
				success: function(r){
					loadMask.hide();
					if(r.processResult=='SUCCESS'){
						callback(r.listData,r.total);
					}else{
						Ext.Msg.alert('Gagal',r.processMessage);
					}
				},
				error: function(jqXHR, exception) {
					loadMask.hide();
					Nci.ajax.ErrorMessage(jqXHR, exception);
				}
			});
//			loadMask.show();
//			var a=[];
//			a.push({name: 'ret_number',value:$this.TextField.srchRetNumber.getValue()});
//			a.push({name: 'startDate',value:$this.DateField.srchStartDate.value});
//			a.push({name: 'lastDate',value:$this.DateField.srchLastDate.value});
//			a.push({name: 'kd_vendor',value:Ext.getCmp('cboPbf_viApotekReturPBFFilter').getValue()});
//			a.push({name: 'start',value:data.start});
//			a.push({name: 'size',value:data.size});
//			$.ajax({
//				type: 'POST',
//				dataType:'JSON',
//				url:baseURL + "index.php/gudang_farmasi/functionGFReturPBF/initList",
//				data:a,
//				success: function(r){
//					loadMask.hide();
//					if(r.processResult=='SUCCESS'){
//						callback(r.listData,r.total);
//					}else{
//						Ext.Msg.alert('Gagal',r.processMessage);
//					}
//				},
//				error: function(jqXHR, exception) {
//					loadMask.hide();
//					Nci.ajax.ErrorMessage(jqXHR, exception);
//				}
//			});
		},
		column:new Ext.grid.ColumnModel([
				new Ext.grid.RowNumberer(),
			{
				header		: 'Status Posting',
				width		: 15,
				sortable	: false,
				hideable	: true,
				hidden		: false,
				menuDisabled: true,
				dataIndex	: 'posting',
				renderer	: function(value, metaData, record, rowIndex, colIndex, store){
					 switch (value){
						 case '1':
							 metaData.css = 'StatusHijau'; 
							 break;
						 case '0':
							 metaData.css = 'StatusMerah';
							 break;
					 }
					 return '';
				}
			},{
				header: 'No. Penerimaan',
				dataIndex: 'no_obat_in',
				sortable: true,
				menuDisabled: true,
				width: 35
					
			},{
				//xtype:'datecolumn',
				header:'Tgl Penerimaan',
				dataIndex: 'tgl_obat_in',	
				width: 20,
				sortable: true,
				hideable:false,
                menuDisabled:true,
                renderer: function(v, params, record){
					return ShowDate(record.data.tgl_obat_in);
				}
			},{
				header: 'PBF',
				dataIndex: 'vendor',
				sortable: true,
				hideable:false,
                menuDisabled:true,
				width: 50
			},{
				header: 'No Faktur',
				dataIndex: 'remark',
				sortable: true,
				menuDisabled:true,
				width: 40
			},{
				header: 'Kepemilikan',
				dataIndex: 'milik',
				sortable: true,
				menuDisabled:true,
				width: 40
			}
		])
	});
//	GFPenerimaan.form.Grid.b = new Ext.grid.EditorGridPanel({
//		xtype: 'editorgrid',
//		title: '',
//		store: GFPenerimaan.form.ArrayStore.c,
//		autoScroll: true,
//		columnLines: true,
//		flex: 1,
//		border:false,
//		anchor: '100% 100%',
//		plugins: [new Ext.ux.grid.FilterRow()],
//		selModel: new Ext.grid.RowSelectionModel({
//				singleSelect: true,
//				listeners:{
//					rowselect: function(sm, row, rec){
//						rowSelected_viApotekPenerimaan = undefined;
//						rowSelected_viApotekPenerimaan = GFPenerimaan.form.ArrayStore.c.getAt(row);
//						CurrentData_viApotekPenerimaan
//						CurrentData_viApotekPenerimaan.row = row;
//						CurrentData_viApotekPenerimaan.data = rowSelected_viApotekPenerimaan.data;
//					}
//				}
//		}),
//		listeners:{
//			rowdblclick: function (sm, ridx, cidx){
//				rowSelected_viApotekPenerimaan = GFPenerimaan.form.ArrayStore.c.getAt(ridx);
//				if (rowSelected_viApotekPenerimaan != undefined){
//					setLookUp_viApotekPenerimaan(rowSelected_viApotekPenerimaan.data);
//				}else{
//					setLookUp_viApotekPenerimaan();
//				}
//			}
//		},
//		colModel: new Ext.grid.ColumnModel([
//				new Ext.grid.RowNumberer(),
//			{
//				header		: 'Status Posting',
//				width		: 15,
//				sortable	: false,
//				hideable	: true,
//				hidden		: false,
//				menuDisabled: true,
//				dataIndex	: 'posting',
//				renderer	: function(value, metaData, record, rowIndex, colIndex, store){
//					 switch (value){
//						 case '1':
//							 metaData.css = 'StatusHijau'; 
//							 break;
//						 case '0':
//							 metaData.css = 'StatusMerah';
//							 break;
//					 }
//					 return '';
//				}
//			},{
//				header: 'No. Penerimaan',
//				dataIndex: 'no_obat_in',
//				sortable: true,
//				width: 35
//					
//			},{
//				//xtype:'datecolumn',
//				header:'Tgl Penerimaan',
//				dataIndex: 'tgl_obat_in',	
//				width: 20,
//				sortable: true,
//				hideable:false,
//                menuDisabled:true,
//                renderer: function(v, params, record){
//					return ShowDate(record.data.tgl_obat_in);
//				}
//			},{
//				header: 'PBF',
//				dataIndex: 'vendor',
//				sortable: true,
//				hideable:false,
//                menuDisabled:true,
//				width: 50
//			},{
//				header: 'No Faktur',
//				dataIndex: 'remark',
//				sortable: true,
//				width: 40
//			}
//		]),
//		tbar: {
//			xtype: 'toolbar',
//			id: 'toolbar_viApotekPenerimaan',
//			items: [
//				{
//					xtype: 'button',
//					text: 'Tambah',
//					iconCls: 'add',
//					tooltip: 'Edit Data',
//					handler: function(sm, row, rec){
//						setLookUp_viApotekPenerimaan();
//					}
//				},
//				{
//					xtype: 'button',
//					text: 'Ubah',
//					iconCls: 'Edit_Tr',
//					tooltip: 'Edit Data',
//					handler: function(sm, row, rec){
//						if(rowSelected_viApotekPenerimaan != undefined){
//							setLookUp_viApotekPenerimaan(rowSelected_viApotekPenerimaan.data)
//						}else{
//							Ext.Msg.alert('Information','Data Belum Dipilih.');
//						}
//					}
//				}
//			]
//		},
//		//bbar : bbar_paging(mod_name_viApotekPenerimaan, selectCount_viApotekPenerimaan, dataSource_viApotekPenerimaan),
//		viewConfig: {
//			forceFit: true
//		}
//	});
	GFPenerimaan.form.FormPanel.b = new Ext.FormPanel({
        //region: 'center',
        border: true,
        title: '',
        bodyStyle:'padding:5px; margin-top: -1px;',
        items: [
		{
			layout: 'column',
			border: false,
			items:[
				{
					columnWidth:.98,
					layout: 'absolute',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					height: 60,
					anchor: '100% 100%',
					items:[
						{
							x: 10,
							y: 0,
							xtype: 'label',
							text: 'No. Penerimaan'
						},{
							x: 120,
							y: 0,
							xtype: 'label',
							text: ':'
						},GFPenerimaan.form.TextField.c=new Ext.form.TextField({
							x: 130,
							y: 0,
							xtype: 'textfield',
							name: 'noPenerima',
							fieldLabel: 'No Penerimaan ',
							width: 130,
							tabIndex:1,
							listeners:{ 
								'specialkey' : function(a,e){
									if (e.getKey() == e.ENTER) {
//										GFPenerimaan.func.refresh();
										Q(GFPenerimaan.form.Grid.b).refresh();
								    }
								}
							}
						}),{
							x: 10,
							y: 30,
							xtype: 'label',
							text: 'Tanggal'
						},{
							x: 120,
							y: 30,
							xtype: 'label',
							text: ':'
						},{
							x: 130,
							y: 30,
							xtype: 'datefield',
							id: 'startDateGFPenerimaan',
							format: 'Y-m-d',
							width: 120,
							tabIndex:3,
							value:now_viApotekPenerimaan,
							listeners:{ 
								'specialkey' : function(a,e){
									if (e.getKey() == e.ENTER) {
//										GFPenerimaan.func.refresh();
										Q(GFPenerimaan.form.Grid.b).refresh();
								    }
								}
							}
						},{
							x: 260,
							y: 30,
							xtype: 'label',
							text: 's/d'
						},{
							x: 288,
							y: 30,
							xtype: 'datefield',
							id: 'lastDateGFPenerimaan',
							format: 'Y-m-d',
							width: 120,
							tabIndex:4,
							value:now_viApotekPenerimaan,
							listeners:{ 
								'specialkey' : function(a,e){
									if (e.getKey() == e.ENTER) {
//										GFPenerimaan.func.refresh();
										Q(GFPenerimaan.form.Grid.b).refresh();
								    }
								}
							}
						},{
							x: 450,
							y: 0,
							xtype: 'label',
							text: 'PBF'
						},{
							x: 520,
							y: 0,
							xtype: 'label',
							text: ':'
						},
						viCombo_Vendor(),		
						{
							x: 450,
							y: 30,
							xtype: 'label',
							text: 'Posting'
						},{
							x: 520,
							y: 30,
							xtype: 'label',
							text: ':'
						},
						mComboStatusPostingApotekPenerimaan(),
						{
							x: 690,
							y: 30,
							xtype: 'button',
							text: 'Cari',
							iconCls: 'refresh',
							tooltip: 'Cari',
							id:'btnGFCariPenerimaan',
							style:{paddingLeft:'30px'},
							width:150,
							id: 'posting',
							handler: function() {					
//								GFPenerimaan.func.refresh();
								Q(GFPenerimaan.form.Grid.b).refresh();
							}                        
						}
					]
				}
			]
		}
		]	
	})
    var FrmFilterGridDataView_viApotekPenerimaan = new Ext.Panel({
			title: NamaForm_viApotekPenerimaan,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viApotekPenerimaan,
			layout: {
			    type: 'vbox',
			    align : 'stretch'
			},
			closable: true,        
			border: false,  
			margins: '0 5 5 0',
			items: [ GFPenerimaan.form.FormPanel.b,GFPenerimaan.form.Grid.b],
			tbar:[{
		            xtype: 'buttongroup',
		            columns: 21,
		            defaults: {
		            	scale: 'small'
		        	},
		        	frame: false,
		            items:[]
				}
			]
	});
    return FrmFilterGridDataView_viApotekPenerimaan;
}
function shortcuts(){
	shortcut.set({
		code:'lookup',
		list:[
			{
				key:'ctrl+s',
				fn:function(){
					Ext.getCmp('btnSaveGFPenerimaan').el.dom.click();
				}
			},
			{
				key:'ctrl+d',
				fn:function(){
					Ext.getCmp('btnDeleteGFPenerimaan').el.dom.click();
				}
			},{
				key:'f4',
				fn:function(){
					Ext.getCmp('btnPostingGFPenerimaan').el.dom.click();
				}
			},{
				key:'f6',
				fn:function(){
					Ext.getCmp('btnUnPostingGFPenerimaan').el.dom.click();
				}
			},
			{
				key:'f12',
				fn:function(){
					Ext.getCmp('btnCetakGFPenerimaan').el.dom.click();
				}
			},
			{
				key:'esc',
				fn:function(){
					setLookUps_viApotekPenerimaan.close();
				}
			}
		]
	});
}
function setLookUp_viApotekPenerimaan(rowdata){
    var lebar = 985;
    setLookUps_viApotekPenerimaan = new Ext.Window({
		name: 'SetLookUps_viApotekPenerimaan',
        title: NamaForm_viApotekPenerimaan, 
        closeAction: 'destroy',        
        width: 1300,
        height: 605,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viApotekPenerimaan(lebar,rowdata),
        listeners:{
            activate: function(){
				shortcuts();
            },
            afterShow: function(){
                this.activate();
            },
            deactivate: function(){
                rowSelected_viApotekPenerimaan=undefined;
				mNoKunjungan_viApotekPenerimaan = '';;
				shortcut.remove('lookup');
            },
			close: function (){
				shortcut.remove('lookup');
			},
        }
    });
    if(rowdata == undefined){
    	loadMask.show();
    	 $.ajax({
			url:baseURL + "index.php/gudang_farmasi/functionGFPenerimaan/initTransaksi",
			dataType:'JSON',
			type: 'GET',
			success: function(r){
				loadMask.hide();
				if(r.processResult=='SUCCESS'){
					setLookUps_viApotekPenerimaan.show();
				}else{
					Ext.Msg.alert('Error',r.processMessage);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
    }else{
    	loadMask.show();
    	 $.ajax({
 			url:baseURL + "index.php/gudang_farmasi/functionGFPenerimaan/getForEdit",
 			dataType:'JSON',
 			type: 'POST',
 			data: {'no_obat_in':rowdata.no_obat_in},
 			success: function(r){
 				loadMask.hide();
 				if(r.processResult=='SUCCESS'){
 					setLookUps_viApotekPenerimaan.show();
 					GFPenerimaan.form.TextField.b.setValue(r.resultObject.no_obat_in);
 					GFPenerimaan.form.TextField.a.setValue(r.resultObject.kd_vendor);
 					GFPenerimaan.form.ComboBox.b.setValue(r.resultObject.vendor);
 					var dueDate = Date.parseDate(r.resultObject.due_date, "Y-m-d H:i:s");
 					GFPenerimaan.form.DateField.a.setValue(dueDate);
 					var penerimaanDate = Date.parseDate(r.resultObject.tgl_obat_in, "Y-m-d H:i:s");
 					GFPenerimaan.form.DateField.b.setValue(penerimaanDate);
 					GFPenerimaan.form.TextField.d.setValue(r.resultObject.remark);
 					GFPenerimaan.form.TextField.e.setValue(r.resultObject.no_sj);
 					if(r.resultObject.posting==1){
 						GFPenerimaan.form.Panel.a.update('<img src="'+baseURL+'ui/images/icons/16x16/hijau.png"/>');
						Ext.getCmp('btnSaveGFPenerimaan').disable();
						Ext.getCmp('btnAdd_viApotekPenerimaan').disable();
						Ext.getCmp('btnDeleteGFPenerimaan').disable();
						Ext.getCmp('btnPostingGFPenerimaan').disable();
						Ext.getCmp('btnDeleteGFPenerimaan').disable();
						Ext.getCmp('btnUnPostingGFPenerimaan').enable();
						Ext.getCmp('btnCetakGFPenerimaan').enable();
 					}else{
						Ext.getCmp('btnSaveGFPenerimaan').enable();
						Ext.getCmp('btnAdd_viApotekPenerimaan').enable();
						Ext.getCmp('btnDeleteGFPenerimaan').enable();
						Ext.getCmp('btnPostingGFPenerimaan').enable();
						Ext.getCmp('btnDeleteGFPenerimaan').enable();
						Ext.getCmp('btnUnPostingGFPenerimaan').disable();
						Ext.getCmp('btnCetakGFPenerimaan').disable();
 						GFPenerimaan.form.Panel.a.update('<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>');
 					}
 					for(var i=0, iLen=r.listData.length ; i<iLen ; i++){
 						var records = new Array();
 						records.push(new dsDataGrdJab_viApotekPenerimaan.recordType());
 						dsDataGrdJab_viApotekPenerimaan.add(records);
 						var o=dsDataGrdJab_viApotekPenerimaan.data.items[i].data;
						var harga=0;
						var po="";
						harga = parseFloat(r.listData[i].hrg_beli_obt)*parseFloat(r.listData[i].frac);
 						o.kd_prd=r.listData[i].kd_prd;
 						o.nama_obat=r.listData[i].nama_obat;
 						o.kd_sat_besar=r.listData[i].kd_sat_besar;
 						o.harga_beli=harga;
 						o.jml_in_obat=r.listData[i].jml_in_obt;
 						o.apt_discount=r.listData[i].apt_discount;
 						o.apt_disc_rupiah=r.listData[i].apt_disc_rupiah;
 						o.ppn=r.listData[i].ppn_item;
						o.ketppn=10;
						
						if(r.listData[i].po_number == "" || r.listData[i].po_number == null){
							po="*";
						}else{
							po=r.listData[i].po_number;
						}
						o.po_number=po;
						o.gin=r.listData[i].gin;
 						if(r.listData[i].tag==1){
 							o.tag=true;
 						}else{
 							o.tag=false;
 						}
						if(r.listData[i].tag_disc==1){
 							o.tag_disc=true;
 						}else{
 							o.tag_disc=false;
 						}
 						o.kd_pabrik=r.listData[i].kd_pabrik;
 						o.pabrik=r.listData[i].pabrik;
 						o.batch=r.listData[i].batch;
 						o.tgl_expire=r.listData[i].tgl_exp;
 						o.no_obat_in=r.listData[i].no_obat_in;
 						o.fractions=r.listData[i].frac;
 						o.qty_b=r.listData[i].boxqty;
 					}
 					GFPenerimaan.func.refreshNilai();
					
 				}else{
 					
 					Ext.Msg.alert('Error',r.processMessage);
 				}
 			},
 			error: function(jqXHR, exception) {
 				loadMask.hide();
 				Nci.ajax.ErrorMessage(jqXHR, exception);
 			}
 		});
    }
	// shortcuts();
}


function getFormItemEntry_viApotekPenerimaan(lebar,rowdata){
    GFPenerimaan.form.FormPanel.a = new Ext.FormPanel({
			title: '',
			region: 'north',
			layout: 'form',
			bodyStyle: 'padding:10px 10px 10px 10px',
			anchor: '100%',
			labelWidth: 1,
			autoWidth: true,
			width: lebar,
			border: false,
			items:[
				getItemPanelInputBiodata_viApotekPenerimaan(lebar),
				getItemGridTransaksi_viApotekPenerimaan(lebar),
				{
					xtype: 'compositefield',
					fieldLabel: ' ',
					labelSeparator: '',
					items: [
						GFPenerimaan.form.Panel.a=new Ext.Panel ({
						    region: 'north',
						    border: false,
						    html: '<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>'
						}),
						{
							columnWidth	: .08,
							layout		: 'form',
							anchor		: '100% 8.0001%',
							border		: false,
							html		: " Posted"
						},{
							xtype: 'displayfield',				
							width: 65,								
							value: 'Sub Total :',
							style:{'text-align':'right','margin-left':'635px'}							
						},GFPenerimaan.form.TextField.subTotal= new Ext.form.TextField({
		                    name: 'subTotal',
							style:{'text-align':'right','margin-left':'635px'},
		                    width: 120,
		                    value: 0,
		                    readOnly: true
		                }),{
							xtype: 'displayfield',	
							width: 50,								
							value: 'Disc(-):',
							style:{'text-align':'right','margin-left':'635px'}
							
						},GFPenerimaan.form.TextField.discount=new Ext.form.TextField({
		                    name: 'discountTotal',
							style:{'text-align':'right','margin-left':'635px'},
		                    width: 120,
		                    value: 0,
		                    readOnly: true
		                }),{
							xtype: 'displayfield',				
							width: 50,								
							value: 'Jumlah:',
							style:{'text-align':'right','margin-left':'635px'}
						},GFPenerimaan.form.TextField.total=new Ext.form.TextField({
		                    name: 'jumlah',
							style:{'text-align':'right','margin-left':'635px'},
		                    width: 125,
		                    value: 0,
		                    readOnly: true
		                })												
		            ]
		        },{
					xtype: 'compositefield',
					fieldLabel: ' ',
					labelSeparator: '',
					items: [
						GFPenerimaan.form.CheckBox.b=new Ext.form.Checkbox({
							xtype: 'checkbox',
							name: 'updateHarga',
							disabled: true,
							autoWidth: false,		
							boxLabel: 'Update Semua Harga Beli ',
							width: 200,
							handler: function(a,b){
								for(var i=0,iLen=dsDataGrdJab_viApotekPenerimaan.getCount(); i<iLen ; i++){
									if(a.checked==true){
										dsDataGrdJab_viApotekPenerimaan.data.items[i].data.tag=true;
									}else{
										dsDataGrdJab_viApotekPenerimaan.data.items[i].data.tag=false;
									}
								}
								GFPenerimaan.form.Grid.a.getView().refresh();
							}
						}),{
							xtype: 'displayfield',				
							width: 65,								
							value: 'Materai :',
							fieldLabel: 'Label',
							style:{'text-align':'right','margin-left':'496px'}							
						},GFPenerimaan.form.TextField.materai=new Ext.form.TextField({
							style:{'text-align':'right','margin-left':'496px'},
		                    width: 120,
		                    name: 'materai',
		                    value: 0,
		                    readOnly: false,
		                    listeners:{
		                    	blur:function(){
		                    		GFPenerimaan.func.refreshNilai();
		                    	}
		                    }
		                }),{
							xtype: 'displayfield',				
							width: 50,								
							value: 'Ppn(+):',
							fieldLabel: 'Label',
							style:{'text-align':'right','margin-left':'496px'}
							
						},GFPenerimaan.form.TextField.ppn=new Ext.form.TextField({
							style:{'text-align':'right','margin-left':'496px'},
		                    width: 120,
		                    name:'ppn',
		                    value: 0,
		                    readOnly: true
		                })
//		                {
//							xtype:'button',
//							text:'Acc. Approval',
//							width:70,
//							style:{'text-align':'right','margin-top':'3px','margin-left':'380px'},
//							hideLabel:true,
//							id: 'btnAccApp_viApotekPenerimaan',
//							handler:function(){
//							}   
//						},
		            ]
		        }
			],
			fileUpload: true,
			tbar:{
				xtype: 'toolbar',
				items: [
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						id: 'btnAdd_viApotekPenerimaan',
						handler: function(){
							if(GFStatusPosting=='0'){
								var records = new Array();
								records.push(new dsDataGrdJab_viApotekPenerimaan.recordType());
								dsDataGrdJab_viApotekPenerimaan.add(records);
								row=dsDataGrdJab_viApotekPenerimaan.getCount()-1; // menujukan jumlah baris,startEditing(baris dimulai dari 1, kolom 1)
								GFPenerimaan.form.Grid.a .startEditing(row, 2);	
								//GFPenerimaan.func.refreshNilai();
							}else{
								Ext.Msg.alert('Informasi','Menambah gagal karena data sudah diPosting, Harap Unposting terlebih dahulu.');
							}
						}
					},{
						xtype: 'tbseparator'
					},{
						xtype: 'button',
						text: 'Save [CTRL+S]',
						iconCls: 'save',
						id:'btnSaveGFPenerimaan',
						handler: function(){
							if(GFStatusPosting=='0'){
								GFPenerimaan.func.save();
								
							}else{
								Ext.Msg.alert('Informasi','Penyimpanan gagal karena data sudah diPosting, Harap Unposting jika ingin Menyimpan.');
							}
						}
					},{
						xtype: 'tbseparator'
					},{
						xtype: 'button',
						text: 'Save & Close',
						id:'btnSaveCloseGFPenerimaan',
						iconCls: 'saveexit',
						hidden:true,
						handler: function(){
							if(GFStatusPosting=='0'){
								GFPenerimaan.func.save(function(){
									setLookUps_viApotekPenerimaan.close();
								});
							}else{
								Ext.Msg.alert('Informasi','Penyimpanan gagal karena data sudah diPosting, Harap Unposting jika ingin Menyimpan.');
							}
						}
					},{
						xtype: 'tbseparator',
						hidden:true,
					},{
						xtype: 'button',
						text: 'Delete [CTRL+D]',
						id:'btnDeleteGFPenerimaan',
						iconCls: 'remove',
						handler: function(){
							if(GFStatusPosting=='0'){
								var line	= GFPenerimaan.form.Grid.a.getSelectionModel().selection.cell[0];
								var o=dsDataGrdJab_viApotekPenerimaan.getRange()[line].data;
								if(o.no_obat_in != undefined && o.no_obat_in!=''){
									if(dsDataGrdJab_viApotekPenerimaan.getCount()>1){
										Ext.Msg.prompt('Name', 'Apakah Data Ini Akan Dihapus?', function(btn, text){
										    if (btn == 'ok'){
										    	loadMask.show();
										    	$.ajax({
													url:baseURL + "index.php/gudang_farmasi/functionGFPenerimaan/deleteDetail",
													dataType:'JSON',
													type: 'POST',
													data:{no_obat_in:o.no_obat_in,line:(line+1)},
													success: function(r){
														loadMask.hide();
														if(r.processResult=='SUCCESS'){
															Ext.Msg.alert('Sukses','Data Berhasi Dihapus.');
															dsDataGrdJab_viApotekPenerimaan.removeAt(line);
															GFPenerimaan.form.Grid.a.getView().refresh();
														}else{
															Ext.Msg.alert('Gagal',r.processMessage);
														}
													},
													error: function(jqXHR, exception) {
														loadMask.hide();
														Nci.ajax.ErrorMessage(jqXHR, exception);
													}
												});
										    }
										});
									}else{
										Ext.Msg.alert('Gagal','Data tidak bisa dihapus karena minimal mempunyai 1 data.');
									}
								}else{
									Ext.Msg.confirm('Konfirmasi', 'Apakah ingin menghapus data ini?', function (id, value) { 
										if (id === 'yes') { 
											dsDataGrdJab_viApotekPenerimaan.removeAt(line);
											GFPenerimaan.form.Grid.a.getView().refresh();
										} 
									}, this); 
								}
							}else{
								Ext.Msg.alert('Error','Data Sudah Diposting, Harap Unposting Jika ingin Menghapus Data.');
							}
						}
					},{
						xtype:'tbseparator'
					},GFPenerimaan.form.Button.a=new Ext.Button({
						text: 'Posting [F4]',
						iconCls: 'gantidok',
						id:'btnPostingGFPenerimaan',
						disabled:true,
						handler: function(){
							if(GFStatusPosting=='0'){
								var param=$('#'+GFPenerimaan.form.FormPanel.a.id).find('form').eq(0).serializeArray(),e=false;
								if(GFPenerimaan.form.TextField.a.getValue()==''){
									e=true;
									Ext.Msg.alert('Error','Harap Isi PBF.');
									return;
								}
								if(dsDataGrdJab_viApotekPenerimaan.getCount()==0){
									e=true;
									Ext.Msg.alert('Error','Isi Produk Obat.');
									return;
								}
								if(GFPenerimaan.form.TextField.total.getValue()==''){
									e=true;
									Ext.Msg.alert('Error','Isi Data Obat Dengan Benar.');
									return;
								}
								param.push({name:'count',value:dsDataGrdJab_viApotekPenerimaan.getCount()});
								for(var i=0, iLen=dsDataGrdJab_viApotekPenerimaan.getCount(); i<iLen ; i++){
									var harga_beli_obat=0;
									var totharga=0;
									var o=dsDataGrdJab_viApotekPenerimaan.data.items[i].data;
									var po;
									if(o.ketppn == 10){
										//totharga=toInteger(o.harga_beli)+((toInteger(o.harga_beli)*10)/100);
										subtot = parseFloat(o.qty_b) * (toInteger(o.harga_beli) + (toInteger(o.harga_beli)*10)/100);
										totharga = subtot / (parseFloat(o.qty_b) * parseFloat(o.fractions));
									} else{
										//totharga=toInteger(o.harga_beli);
										subtot = parseFloat(o.qty_b) * toInteger(o.harga_beli);
										totharga = subtot / (parseFloat(o.qty_b) * parseInt(o.fractions));
									}
									
									harga_beli_obat = parseFloat(o.harga_beli)/parseFloat(o.fractions);
									
									if(o.po_number == "*"){
										po="";
									}else{
										po=o.po_number;
									}
									
									param.push({name:'po_number-'+i,value: po});
									param.push({name:'kd_prd-'+i,value: o.kd_prd});
									param.push({name:'jml_in_obt-'+i,value: o.jml_in_obat});
									param.push({name:'hrg_beli_obt-'+i,value: harga_beli_obat});
									param.push({name:'harga_satuan-'+i,value: totharga});
									param.push({name:'tgl_exp-'+i,value: convertTglGFPenerimaan(o.tgl_expire)});
									param.push({name:'apt_discount-'+i,value: o.apt_discount});
									param.push({name:'ppn_item-'+i,value: o.ppn});
									param.push({name:'apt_disc_rupiah-'+i,value: o.apt_disc_rupiah});
									param.push({name:'frac-'+i,value: o.fractions});
									param.push({name:'boxqty-'+i,value: o.qty_b});
									param.push({name:'sub_total-'+i,value: o.sub_total});
									if(o.tag != undefined && o.tag == true){
										param.push({name:'tag-'+i,value: 1});
									}else{
										param.push({name:'tag-'+i,value: 0});
									}
									if(o.tag_disc != undefined && o.tag_disc == true){
										param.push({name:'tag_disc-'+i,value: 1});
									}else{
										param.push({name:'tag_disc-'+i,value: 0});
									}
									if(o.batch != undefined){
										param.push({name:'batch-'+i,value: o.batch});
									}else{
										param.push({name:'batch-'+i,value:''});
									}
									if(o.kd_pabrik != undefined){
										param.push({name:'kd_pabrik-'+i,value:o.kd_pabrik});
									}else{
										param.push({name:'kd_pabrik-'+i,value:''});
									}
								}
								
								Ext.Msg.confirm('Konfirmasi', 'Apakah Akan diPosting ?', function (id, value) { 
									if (id === 'yes') { 
										if(GFPenerimaan.form.TextField.b.getValue()!='' && GFStatusPosting=='0'){
											// loadMask.show();
											// $.ajax({
												// url:baseURL + "index.php/gudang_farmasi/functionGFPenerimaan/postingSave",
												// dataType:'JSON',
												// type: 'POST',
												// data:param,
												// success: function(r){
													// loadMask.hide();
													// if(r.processResult=='SUCCESS'){
														// GFPenerimaan.form.TextField.b.setValue(r.resultObject.code);
														// for(var i=0; i<dsDataGrdJab_viApotekPenerimaan.getCount() ; i++){
															// var o=dsDataGrdJab_viApotekPenerimaan.getRange()[i].data;
															// o.no_obat_in=r.resultObject.code;
														// }
														// GFPenerimaan.form.Panel.a.update('<img src="'+baseURL+'ui/images/icons/16x16/hijau.png"/>');
														// Ext.Msg.alert('Sukses','Data Berhasi Disimpan dan diPosting.');
														// GFStatusPosting='1';
														// Ext.getCmp('btnPostingGFPenerimaan').disable();
														// Ext.getCmp('btnDeleteGFPenerimaan').disable();
														// Ext.getCmp('btnSaveCloseGFPenerimaan').disable();
														// Ext.getCmp('btnSaveGFPenerimaan').disable();
														// Ext.getCmp('btnAdd_viApotekPenerimaan').disable();
														// Ext.getCmp('btnUnPostingGFPenerimaan').enable();
														// Ext.getCmp('btnCetakGFPenerimaan').enable();
													// }else{
														// Ext.Msg.alert('Gagal',r.processMessage);
													// }
												// },
												// error: function(jqXHR, exception) {
													// loadMask.hide();
													// Nci.ajax.ErrorMessage(jqXHR, exception);
												// }
											// });
										// }else{
											loadMask.show();
											$.ajax({
												url:baseURL + "index.php/gudang_farmasi/functionGFPenerimaan/postingUpdate",
												dataType:'JSON',
												type: 'POST',
												data:param,
												success: function(r){
													loadMask.hide();
													if(r.processResult=='SUCCESS'){
														Ext.Msg.alert('Sukses','Data Berhasi Diubah dan diPosting.');
														GFStatusPosting='1';
														Ext.getCmp('btnSaveGFPenerimaan').disable();
														Ext.getCmp('btnDeleteGFPenerimaan').disable();
														for(var i=0; i<dsDataGrdJab_viApotekPenerimaan.getCount() ; i++){
															var o=dsDataGrdJab_viApotekPenerimaan.getRange()[i].data;
															o.no_obat_in=GFPenerimaan.form.TextField.b.getValue();
														}
														GFPenerimaan.form.Panel.a.update('<img src="'+baseURL+'ui/images/icons/16x16/hijau.png"/>');
														Ext.getCmp('btnPostingGFPenerimaan').disable();
														Ext.getCmp('btnDeleteGFPenerimaan').disable();
														Ext.getCmp('btnSaveCloseGFPenerimaan').disable();
														Ext.getCmp('btnSaveGFPenerimaan').disable();
														Ext.getCmp('btnAdd_viApotekPenerimaan').disable();
														Ext.getCmp('btnUnPostingGFPenerimaan').enable();
														Ext.getCmp('btnCetakGFPenerimaan').enable();
														GFPenerimaan.func.refresh();
													}else{
														Ext.Msg.alert('Gagal',r.processMessage);
													}
												},
												error: function(jqXHR, exception) {
													loadMask.hide();
													Nci.ajax.ErrorMessage(jqXHR, exception);
												}
											});
										} else{
											if(GFStatusPosting=='1'){					
												Ext.Msg.alert('WARNING','Transaksi ini sudah diPosting!');
											} else if(PengeluaranGF.form.TextField.no.getValue()==''){
												Ext.Msg.alert('WARNING','Simpan terlebih dahulu sebelum diPosting!');
											}
										}
									} 
								}, this); 
							}else{
								Ext.Msg.alert('Error','Gagal. Data Sudah Diposting!');
							}
						}
					}),{
						xtype:'tbseparator'
					},GFPenerimaan.form.Button.a=new Ext.Button({
						text: 'Unposting [F6]',
						iconCls: 'reuse',
						disabled:true,
						id:'btnUnPostingGFPenerimaan',
						handler: function(){
							if(GFStatusPosting=='1'){
								Ext.Msg.confirm('Konfirmasi', 'Apakah Akan diUnposting ?', function (id, value) { 
									if (id === 'yes') { 
										loadMask.show();
										$.ajax({
											url:baseURL + "index.php/gudang_farmasi/functionGFPenerimaan/unposting",
											dataType:'JSON',
											type: 'POST',
											data:{no_obat_in:GFPenerimaan.form.TextField.b.getValue()},
											success: function(r){
												loadMask.hide();
												if(r.processResult=='SUCCESS'){
													Ext.Msg.alert('Sukses','Data Berhasil diUnposting.');
													GFStatusPosting='0';
													for(var i=0; i<dsDataGrdJab_viApotekPenerimaan.getCount() ; i++){
														var o=dsDataGrdJab_viApotekPenerimaan.getRange()[i].data;
														o.no_obat_in=GFPenerimaan.form.TextField.b.getValue();
													}
													GFPenerimaan.form.Panel.a.update('<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>');
													Ext.getCmp('btnPostingGFPenerimaan').enable();
													Ext.getCmp('btnDeleteGFPenerimaan').enable();
													Ext.getCmp('btnSaveCloseGFPenerimaan').enable();
													Ext.getCmp('btnSaveGFPenerimaan').enable();
													Ext.getCmp('btnAdd_viApotekPenerimaan').enable();
													Ext.getCmp('btnUnPostingGFPenerimaan').disable();
													Ext.getCmp('btnCetakGFPenerimaan').disable();
													GFPenerimaan.func.refresh();
												}else{
													Ext.Msg.alert('Gagal',r.processMessage);
												}
											},
											error: function(jqXHR, exception) {
												loadMask.hide();
												Nci.ajax.ErrorMessage(jqXHR, exception);
											}
										});
									} 
								}, this); 
							}else{
								Ext.Msg.alert('Error','Data Belum Diposting!');
							}
						}
					}),{
						xtype:'tbseparator'
					},GFPenerimaan.form.Button.cetak=new Ext.Button({
						text: 'Print [F12]',
						iconCls: 'print',
						id:'btnCetakGFPenerimaan',
						disabled:true,
						handler: function()
						{
							Ext.Msg.confirm('Konfirmasi', 'Siap mencetak bill?', function (id, value) { 
								if (id === 'yes') {
									var params={
										no_obat_in:GFPenerimaan.form.TextField.b.getValue(),
										tgl:GFPenerimaan.form.DateField.b.getValue(),
									} ;
									var form = document.createElement("form");
									form.setAttribute("method", "post");
									form.setAttribute("target", "_blank");
									form.setAttribute("action", baseURL + "index.php/gudang_farmasi/cetakan_faktur_bill/cetakpenerimaan");
									var hiddenField = document.createElement("input");
									hiddenField.setAttribute("type", "hidden");
									hiddenField.setAttribute("name", "data");
									hiddenField.setAttribute("value", Ext.encode(params));
									form.appendChild(hiddenField);
									document.body.appendChild(form);
									form.submit();		
								}
							})
						}
					})
				]
			}
	})
    return GFPenerimaan.form.FormPanel.a;
}

function getItemPanelInputBiodata_viApotekPenerimaan(lebar) {
    var items ={
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		labelWidth: 100,
		autoWidth: true,
		border:true,
		items:[			
			{
                xtype: 'compositefield',
                fieldLabel: 'No. Penerimaan',
                anchor: '100%',
                width: 250,
                items: [                    
					GFPenerimaan.form.TextField.b=new Ext.form.TextField({
						width : 120,	
						name: 'noPenerima',
						readOnly: true,
						listeners:{
							'specialkey': function() {
								if (Ext.EventObject.getKey() === 13){
									// GFPenerimaan.form.TextField.a.focus();
								}
							}
						}
					}), {
						xtype: 'displayfield',
						flex: 1,
						width: 70,
						name: '',
						value: 'PBF :',
						fieldLabel: 'Label'
					},					
					viCombo_VendorLookup(),
					GFPenerimaan.form.TextField.a=new Ext.form.TextField({
						name: 'kd_vendor',
						hidden: true
					}),
					{
						xtype: 'displayfield',
						flex: 1,
						width: 90,
						name: '',
						value: 'Jatuh Tempo :',
						fieldLabel: 'Label'
					},GFPenerimaan.form.DateField.a=new Ext.form.DateField({
						name:'jatuhTempo',
						value: now_viApotekPenerimaan,
						format: 'd/m/Y',
						width: 120,
						listeners:{ 
							'specialkey' : function(){
								if (Ext.EventObject.getKey() === 13) {
//									datarefresh_viApotekPenerimaan();	
									if(GFStatusPosting=='0'){
										var records = new Array();
										records.push(new dsDataGrdJab_viApotekPenerimaan.recordType());
										dsDataGrdJab_viApotekPenerimaan.add(records);
										row=dsDataGrdJab_viApotekPenerimaan.getCount()-1; // menujukan jumlah baris,startEditing(baris dimulai dari 1, kolom 1)
										GFPenerimaan.form.Grid.a .startEditing(row, 2);	
										//GFPenerimaan.func.refreshNilai();
									}else{
										Ext.Msg.alert('Informasi','Menambah gagal karena data sudah diPosting, Harap Unposting terlebih dahulu.');
									}
								} 						
							}
						}
					}),
					{
						xtype: 'displayfield',
						flex: 1,
						width: 200,
						name: '',
						style:{'margin-left':'25px','font-size':'11px'},	
						value: '*) Jika ada obat yang sama dengan batch berbeda maka entry berurutan'
					},
					
                ]
            },{
				xtype: 'compositefield',
				fieldLabel: 'Tanggal',
				anchor: '100%',
				width: 199,
				items: [
					GFPenerimaan.form.DateField.b=new Ext.form.DateField({
						xtype: 'datefield',
						name: 'penerimaan',
						value: now_viApotekPenerimaan,
						format: 'd/m/Y',
						// readOnly: true,
						width: 120,
						listeners:{ 
							'specialkey' : function(){
								if (Ext.EventObject.getKey() === 13) {
									// datarefresh_viApotekPenerimaan();
									GFPenerimaan.form.ComboBox.b.focus();
								} 						
							}
						}
					}),{
						xtype: 'displayfield',
						flex: 1,
						width: 70,
						name: '',
						value: 'No Faktur :',
						fieldLabel: 'Label'
					},GFPenerimaan.form.TextField.d= new Ext.form.TextField({
						xtype: 'textfield',
						flex: 1,
						width : 360,	
						name: 'noFaktur',
						listeners:{
							'specialkey': function(){
								if (Ext.EventObject.getKey() === 13) {
									GFPenerimaan.form.DateField.a.focus();
								};
							},
						}
					}),{
						xtype: 'displayfield',
						flex: 1,
						width: 90,
						name: '',
						hidden:true,
						value: 'No Surat Jalan :'
					},GFPenerimaan.form.TextField.e= new Ext.form.TextField({
						xtype: 'textfield',
						flex: 1,
						hidden:true,
						width : 100,	
						name: 'noSuratBayar',
						listeners:{
							'specialkey': function() {
								if (Ext.EventObject.getKey() === 13) {
								};
							}
						}
					}),{
						xtype:'button',
						text:'Bayar',
						width:70,
						hideLabel:true,
						disabled:true,
						id: 'btnBayar_viApotekPenerimaan',
						handler:function(){
						}   
					},{
						xtype: 'displayfield',
						flex: 1,
						width: 200,
						name: '',
						style:{'font-size':'11px'},	
						value: '  berdasarkan tgl expired yang paling awal.'
					},
					
				]
			}
		]
	};
    return items;
};

function getItemGridTransaksi_viApotekPenerimaan(lebar) {
    var items ={
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'margin-top: -1px;',
		border:false,
		width: lebar-80,
		height: 400,//225, 
	    items:[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:[
					gridDataViewEdit_viApotekPenerimaan()
				]	
			}
		]
	};
    return items;
}
GFPenerimaan.func.refreshNilai=function(){
	var subTotal=0,
		discount=0,
		ppn=0,
		grand=0;
		var tmpppn=0,harga=0;
		
	
	for(var i=0; i<dsDataGrdJab_viApotekPenerimaan.getCount() ; i++){
		var subJumlah=0;
		var jumlah=0;
		var o=dsDataGrdJab_viApotekPenerimaan.getRange()[i].data;
		if(o.fractions == undefined || o.fractions==''){
			o.fractions=0;
		}
		if(o.jml_in_obat == undefined || o.jml_in_obat==''){
			o.jml_in_obat=0;
		}
		if(o.qty_b == undefined || o.qty_b==''){
			o.qty_b=0;
		}
		if(o.harga_beli == undefined || o.harga_beli==''){
			o.harga_beli=0;
		}
		if(o.harga_beli_tmp == undefined || o.harga_beli_tmp==''){
			o.harga_beli_tmp=0;
		}
		
		/* HITUNG HARGA x QTY_B */
		subJumlah=parseFloat(o.qty_b)*(parseFloat(o.harga_beli));
		jumlah=parseFloat(o.qty_b)*(parseFloat(o.harga_beli));
		
		/* HITUNG PPN */
	
		if(o.ketppn == 10){
			/* HITUNG DISCOUNT */
			if(isNaN(o.apt_disc_rupiah)){
				o.apt_disc_rupiah=0;
			}
			if(isNaN(o.apt_discount)){
				o.apt_discount=0;
			}
			
			discount+=(((subJumlah/100)*parseFloat(o.apt_discount))+parseFloat(o.apt_disc_rupiah));
			
			jumlah=jumlah-(((jumlah/100)*parseFloat(o.apt_discount))+parseFloat(o.apt_disc_rupiah));
			
			if(isNaN(subJumlah)){
				subJumlah=0;
			}else{
				subJumlah=subJumlah-(((subJumlah/100)*parseFloat(o.apt_discount))+parseFloat(o.apt_disc_rupiah));
			}
			
			tmpppn = parseFloat(subJumlah)*(parseFloat(o.ppn)/100);
			ppn += parseFloat(tmpppn);
			o.sub_total=parseFloat(subJumlah) + ((parseFloat(o.ppn)/100)*subJumlah);
			
		} else{
			if( cat_prd === o.kd_prd  ){
				subJumlah=parseFloat(o.qty_b)*(parseFloat(o.harga_beli));
				harga = parseFloat(o.harga_beli);
				
				
				if(isNaN(o.apt_disc_rupiah)){
					o.apt_disc_rupiah=0;
				}
				if(isNaN(o.apt_discount)){
					o.apt_discount=0;
				}
				
				discount+=(((subJumlah/100)*parseFloat(o.apt_discount))+parseFloat(o.apt_disc_rupiah));
				
				jumlah=jumlah-(((jumlah/100)*parseFloat(o.apt_discount))+parseFloat(o.apt_disc_rupiah));
				
				if(isNaN(subJumlah)){
					subJumlah=0;
				}else{
					subJumlah=subJumlah-(((subJumlah/100)*parseFloat(o.apt_discount))+parseFloat(o.apt_disc_rupiah));
				}
				
				tmpppn = (parseFloat(subJumlah)*0.1);
				ppn += parseFloat(tmpppn);
				o.sub_total = subJumlah+((parseFloat(o.ppn)/100)*subJumlah);
			} else{
				 

			}
		}
		o.jumlah=jumlah;
		subTotal += o.jumlah;
		grand += parseFloat(subJumlah) + parseFloat(tmpppn);
	}
	//console.log(GFPenerimaan.form.TextField.materai.getValue())
	GFPenerimaan.form.TextField.subTotal.setValue(toFormat(formatNumberDecimal(parseFloat(subTotal))));
	GFPenerimaan.form.TextField.discount.setValue(toFormat(formatNumberDecimal(discount)));
	GFPenerimaan.form.TextField.ppn.setValue(toFormat(formatNumberDecimal(ppn)));
	var total=(grand+parseFloat(GFPenerimaan.form.TextField.materai.getValue()));
	total = parseFloat(total);
	GFPenerimaan.form.TextField.total.setValue(toFormat(formatNumberDecimal(total)));
	GFPenerimaan.form.Grid.a.getView().refresh();
}

GFPenerimaan.func.rrefreshNilai=function(){
	var subTotal=0,
		discount=0,
		ppn=0,
		grand=0;
		var tmpppn=0,harga=0;
		
	
	for(var i=0; i<dsDataGrdJab_viApotekPenerimaan.getCount() ; i++){
		var subJumlah=0;
		var jumlah=0;
		var o=dsDataGrdJab_viApotekPenerimaan.getRange()[i].data;
		if(o.fractions == undefined || o.fractions==''){
			o.fractions=0;
		}
		if(o.jml_in_obat == undefined || o.jml_in_obat==''){
			o.jml_in_obat=0;
		}
		if(o.qty_b == undefined || o.qty_b==''){
			o.qty_b=0;
		}
		if(o.harga_beli == undefined || o.harga_beli==''){
			o.harga_beli=0;
		}
		
		/* HITUNG HARGA x QTY_B */
		subJumlah=parseFloat(o.qty_b)*(parseFloat(o.harga_beli));
		jumlah=parseFloat(o.qty_b)*(parseFloat(o.harga_beli));
		
		/* HITUNG PPN */
		// if(o.ppn=='' || o.ppn==undefined){
			// o.ppn=10;
		// } else{
			// o.ppn=o.ppn;
		// }
		if(o.ketppn == 10){
			/* HITUNG DISCOUNT */
			if(isNaN(o.apt_disc_rupiah)){
				o.apt_disc_rupiah=0;
			}
			if(isNaN(o.apt_discount)){
				o.apt_discount=0;
			}
			
			discount+=(((subJumlah/100)*parseFloat(o.apt_discount))+parseFloat(o.apt_disc_rupiah));
			
			jumlah=jumlah-(((jumlah/100)*parseFloat(o.apt_discount))+parseFloat(o.apt_disc_rupiah));
			
			if(isNaN(subJumlah)){
				subJumlah=0;
			}else{
				subJumlah=subJumlah-(((subJumlah/100)*parseFloat(o.apt_discount))+parseFloat(o.apt_disc_rupiah));
			}
			
			tmpppn = ((subJumlah/100)*parseFloat(o.ppn));
			ppn += parseFloat(tmpppn);
			o.sub_total=parseFloat(subJumlah) + ((parseFloat(subJumlah)/100)*parseFloat(o.ppn));
			//subTotal += parseFloat(o.qty_b) * parseFloat(o.harga_beli);
			//grand += subJumlah + ((subJumlah/100)*10);
		} else{
			if( cat_prd === o.kd_prd  ){
				subJumlah=parseFloat(o.qty_b)*(parseFloat(o.harga_beli));
				harga = parseFloat(o.harga_beli);
				//tmpppn = (10/100) * parseFloat(harga);
				tmpppn = (parseFloat(o.harga_beli)/1.1);
				ppn += parseInt(tmpppn);
				
				if(isNaN(o.apt_disc_rupiah)){
					o.apt_disc_rupiah=0;
				}
				if(isNaN(o.apt_discount)){
					o.apt_discount=0;
				}
				
				discount+=(((subJumlah/100)*parseFloat(o.apt_discount))+parseInt(o.apt_disc_rupiah));
				
				jumlah=jumlah-(((jumlah/100)*parseFloat(o.apt_discount))+parseFloat(o.apt_disc_rupiah));
				
				if(isNaN(subJumlah)){
					subJumlah=0;
				}else{
					// subJumlah=subJumlah-(((subJumlah/100)*parseFloat(o.apt_discount))+parseInt(o.apt_disc_rupiah))*1.1;
					subJumlah=subJumlah+((parseFloat(o.ppn)/100)*subJumlah);
				}
				
				//subJumlah = subJumlah - parseFloat(tmpppn);
				//o.jumlah = parseFloat(subJumlah);
				o.sub_total = parseFloat(subJumlah);
				//subTotal += parseFloat(subJumlah);
				//grand += o.sub_total;
				//o.ketppn = 10;
			} else{
			//	alert('cancel');
				 

			}
		}
		o.jumlah=jumlah;
		subTotal += o.jumlah;
		grand += parseFloat(subJumlah) + parseFloat(tmpppn);
	}
	//console.log(GFPenerimaan.form.TextField.materai.getValue())
	GFPenerimaan.form.TextField.subTotal.setValue(toFormat(formatNumberDecimal(parseFloat(subTotal))));
	GFPenerimaan.form.TextField.discount.setValue(toFormat(formatNumberDecimal(discount)));
	GFPenerimaan.form.TextField.ppn.setValue(toFormat(formatNumberDecimal(ppn)));
	var total=(grand+parseFloat(GFPenerimaan.form.TextField.materai.getValue()));
	total = parseFloat(total);
	GFPenerimaan.form.TextField.total.setValue(toFormat(formatNumberDecimal(total)));
	GFPenerimaan.form.Grid.a.getView().refresh();
}

GFPenerimaan.func.refreshHarga=function(){
	var discount=0,harga=0;
	
	for(var i=0; i<dsDataGrdJab_viApotekPenerimaan.getCount() ; i++){
		
		var subJumlah=0;
		var o=dsDataGrdJab_viApotekPenerimaan.getRange()[i].data;
		if (o.ketppn!=10)
		{
			var tmp=0;
			if(o.harga_beli == undefined || o.harga_beli==''){
				o.harga_beli=0;
			}
			tmp=parseFloat(o.harga_beli);
			
			/* HITUNG HARGA x QTY_B */
			subJumlah=parseFloat(o.qty_b)*(parseFloat(o.harga_beli));
			
			
			/* HITUNG DISCOUNT */
			if(isNaN(o.apt_disc_rupiah)){
				o.apt_disc_rupiah=0;
			}
			if(isNaN(o.apt_discount)){
				o.apt_discount=0;
			}
			discount+=(((subJumlah/100)*parseFloat(o.apt_discount))+parseFloat(o.apt_disc_rupiah));
			if(isNaN(subJumlah)){
				subJumlah=0;
			}else{
				subJumlah=subJumlah-(((subJumlah/100)*parseFloat(o.apt_discount))+parseFloat(o.apt_disc_rupiah));
			}
			
			/* HITUNG PPN */
			o.ppn=10;
			
			harga = parseFloat(o.harga_beli)/1.1;
			o.harga_beli = harga;
			o.harga_beli_tmp = harga;
			o.sub_total=harga+((parseFloat(tmp) * parseFloat(o.ppn))/100);
			GFPenerimaan.func.refreshNilai();
		}
	}
	GFPenerimaan.form.Grid.a.getView().refresh();

}
function gridDataViewEdit_viApotekPenerimaan(){
    chkSelected_viApotekPenerimaan = new Ext.grid.CheckColumn({
		id: 'chkSelected_viApotekPenerimaan',
		header: '',
		align: 'center',						
		dataIndex: 'SELECTED',			
		width: 20
	});
    GFPenerimaan.form.CheckBox.a = new Ext.grid.CheckColumn({
		header: 'Tag',
		align: 'center',						
		dataIndex: 'tag',	
		menuDisabled: true,		
		width: 30
	});
	GFPenerimaan.form.CheckBox.b = new Ext.grid.CheckColumn({
		header: 'Tag Disc',
		align: 'center',						
		dataIndex: 'tag_disc',		
		menuDisabled: true,	
		width: 60
	});
    dsDataGrdJab_viApotekPenerimaan= new WebApp.DataStore({
        fields: ['nama_obat','kd_prd','kd_sat_besar','harga_beli','jml_in_obat','qty_b','apt_discount',
				'apt_disc_rupiah','jumlah','ppn','sub_total','tag','kd_pabrik','batch','pabrik',
				'tgl_expire','no_obat_in','kd_milik','milik']
    });
    GFPenerimaan.form.Grid.a = new Ext.grid.EditorGridPanel({
        store: dsDataGrdJab_viApotekPenerimaan,
        height		: 395,
        columnLines: true,
        autoShow: false,
        stripeRows: true,
		 selModel: new Ext.grid.CellSelectionModel({
            singleSelect: true,
            listeners:{
                cellselect: function(sm, row, rec){
					
					cat_prd=GFPenerimaan.form.Grid.a.getSelectionModel().selection.record.data.kd_prd;
					// alert(cat_prd);
                
				}
            }
        }), 
        cm: new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(),	
			{			
				dataIndex: 'po_number',
				header: 'Po Number',
				width: 80,
				fixed:true,
				menuDisabled: true,
				editor:new Nci.form.Combobox.autoComplete({
					store	: GFPenerimaan.form.ArrayStore.po,
					select	: function(a,b,c){
						var jml=0;
						jml=(parseFloat(b.data.harga_beli)/1.1)
						
						var line	= GFPenerimaan.form.Grid.a.getSelectionModel().selection.cell[0];
						dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.po_number=b.data.po_number
						detailGridObatApotekPenerimaan(b.data.po_number)
						/* dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.nama_obat=b.data.nama_obat;
						dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.kd_prd=b.data.kd_prd;
						dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.kd_sat_besar=b.data.kd_sat_besar;
						dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.fractions=b.data.fractions;
						dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.harga_beli=jml;
						dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.kd_pabrik=b.data.kd_pabrik;
						dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.pabrik=b.data.pabrik;
						dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.jml_in_obat='';
						dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.qty_b='';
						dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.ketppn=10;
						GFPenerimaan.func.refreshNilai(); */
					},
					insert	: function(o){
						return {
							po_number		: o.po_number,
							/* kd_prd        	: o.kd_prd,
							nama_obat 		: o.nama_obat,
							kd_sat_besar	: o.kd_sat_besar,
							fractions		: o.fractions,
							harga_beli		: o.harga_beli,
							kd_pabrik		: o.kd_pabrik,
							pabrik			: o.pabrik, */
							text			:  '<table style="font-size: 11px;"><tr><td width="100">'+o.po_number+'</td></tr></table>'
						}
					},
					param:function(){
						if(GFPenerimaan.form.TextField.a.getValue()==''){
							Ext.Msg.alert('Warning','PBF Harus diisi');
						} else{
							return {
							vendor:GFPenerimaan.form.TextField.a.getValue()
						}
						}
						
					},
					url		: baseURL + "index.php/gudang_farmasi/functionGFPenerimaan/getPONumber",
					valueField: 'po_number',
					displayField: 'text',
					listWidth: 100,
					blur: function(){
						GFPenerimaan.func.refreshNilai();
					}
				})
			},
			{			
				dataIndex: 'nama_obat',
				header: 'Uraian',
				width: 200,
				menuDisabled: true,
				fixed: true,
				editor:new Nci.form.Combobox.autoComplete({
					enableKeyEvents:true,
					store	: GFPenerimaan.form.ArrayStore.a,
					select	: function(a,b,c){
						var jml=0;
						/* harga_beli adalah harga_beli persatuan besar, jika harga_beli persatuan kecil maka harus dikali dengan fractions.
						*	jika harga_beli sudah persatuan besar maka harga_beli dikurangi ppn jika ppn berlaku.
						*/
						// parseFloat(b.data.harga_beli)-
						jml=(parseFloat(b.data.harga_beli)/1.1);
						
						var line	= GFPenerimaan.form.Grid.a.getSelectionModel().selection.cell[0];
						dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.po_number="*";
						dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.nama_obat=b.data.nama_obat;
						dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.kd_prd=b.data.kd_prd;
						dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.kd_sat_besar=b.data.kd_sat_besar;
						dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.fractions=b.data.fractions;
						dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.harga_beli=jml;
						dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.kd_pabrik=b.data.kd_pabrik;
						dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.pabrik=b.data.pabrik;
						dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.jml_in_obat='';
						dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.qty_b='';
						dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.ketppn=10;
						dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.ppn=10;
						dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.kd_milik=b.data.kd_milik;
						dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.milik=b.data.milik;
						dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.harga_satuan_kecil=b.data.harga_satuan_kecil;
						dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.harga_beli_tmp=jml;
						
						GFPenerimaan.form.Grid.a.getView().refresh();
						GFPenerimaan.form.Grid.a.startEditing(line,4);
					},
					insert	: function(o){
						return {
							kd_prd        		: o.kd_prd,
							nama_obat 			: o.nama_obat,
							kd_sat_besar		: o.kd_sat_besar,
							fractions			: o.fractions,
							harga_beli			: o.harga_beli,
							kd_pabrik			: o.kd_pabrik,
							pabrik				: o.pabrik,
							kd_milik			: o.kd_milik,
							milik				: o.milik,
							harga_satuan_kecil	: o.harga_satuan_kecil,
							text				:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_prd+'</td><td width="200">'+o.nama_obat+'</td><td width="100">'+o.kd_sat_besar+'</td><td width="100">'+o.milik+'</td></tr></table>'
						}
					},
					url		: baseURL + "index.php/gudang_farmasi/functionGFPenerimaan/getObat",
					valueField: 'nama_obat',
					displayField: 'text',
					listWidth: 400,
					// keyDown: function(a,b,c){
						// if(b.getKey()==13){
							// GFPenerimaan.func.refreshNilai();
							// var row	= dsDataGrdJab_viApotekPenerimaan.getCount()-1;
							// GFPenerimaan.form.Grid.a.startEditing(row,4);
						// }
					// }
				})
			},{
				dataIndex: 'kd_sat_besar',
				header: 'Sat B',
				fixed: true,
				menuDisabled: true,
				width: 50
			},{
				dataIndex: 'qty_b',
				header: 'Qty B',
				align:'right',
				xtype : 'numbercolumn',
				menuDisabled: true,
				width: 50,
				fixed: true,
				editor: new Ext.form.NumberField({
					enableKeyEvents:true,
					listeners:{
						keyDown: function(a,b,c){
							if(b.getKey()==13){
								var line	= this.indeks;
								var b=dsDataGrdJab_viApotekPenerimaan.getRange()[line].data;
								b.qty_b=a.getValue();
								b.jml_in_obat=a.getValue()*b.fractions;
								b.a=false;
								GFPenerimaan.func.refreshNilai();
								//var line	= GFPenerimaan.form.Grid.a.getSelectionModel().selection.cell[0];
								GFPenerimaan.form.Grid.a.startEditing(line,5);
							}
						},
						focus:function(){
							this.indeks=GFPenerimaan.form.Grid.a.getSelectionModel().selection.cell[0];
						},
						
					}				
				})
			},{
				dataIndex: 'fractions',
				xtype : 'numbercolumn',
				header: 'Frac',
				align:'right',
				width: 50,
				menuDisabled: true,
				fixed: true,
				editor: new Ext.form.NumberField({
					decimalPrecision: 0,
					enableKeyEvents:true,
					listeners:{
						keyDown: function(a,b,c){
							if(b.getKey()==13){
								var line	= this.indeks;
								var b=dsDataGrdJab_viApotekPenerimaan.getRange()[line].data;
								if(b.qty_b != 0){
									b.jml_in_obat=parseInt(a.getValue())*parseInt(b.qty_b);
								}else{
									b.jml_in_obat=0;
								}
								b.fractions=a.getValue();
								b.a=false;
								
								GFPenerimaan.func.refreshNilai();
								GFPenerimaan.form.Grid.a.startEditing(line,6);
							}
						},
						focus:function(){
							this.indeks=GFPenerimaan.form.Grid.a.getSelectionModel().selection.cell[0];
						},
					}				
				})
			},{
				dataIndex: 'harga_beli',
				xtype : 'numbercolumn',
				header: 'Harga',
				align:'right',
				menuDisabled: true,
				width: 100,
				fixed: true,
				editor: new Ext.form.NumberField({
					//decimalPrecision: 0,
					enableKeyEvents:true,
					listeners:{
						keyDown: function(a,b,c){
							if(b.getKey()==13){
								console.log(a.getValue())
								var line	= this.indeks;
								Ext.Msg.confirm('Konfirmasi', 'Harga sebelum Ppn?', function (id, value) { 
									if (id === 'yes') {
										/* dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.harga_beli=a.getValue();
										dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.ketppn=10;
										GFPenerimaan.form.Grid.a.getView().refresh();
										GFPenerimaan.func.refreshNilai();
										GFPenerimaan.form.Grid.a.startEditing(line,8); */
										var o = dsDataGrdJab_viApotekPenerimaan.getRange()[line].data;
										if(formatNumberDecimal(o.harga_beli_tmp) == a.getValue()){
											o.harga_beli=a.getValue();
											o.ketppn=10;
											GFPenerimaan.form.Grid.a.getView().refresh();
											GFPenerimaan.func.refreshNilai();
											GFPenerimaan.form.Grid.a.startEditing(line,8);
										} else{
											var harga=0;
											var harga_kecil = 0;
											harga = parseFloat(a.getValue()) / parseFloat(o.fractions);
											harga_kecil = parseFloat(o.harga_beli_tmp) / parseFloat(o.fractions);
											Ext.Msg.confirm('Konfirmasi', 'Harga satuan akan diubah dari '+ formatNumberDecimal(harga_kecil) +' menjadi '+ harga +'?', function (id, value) { 
												if (id === 'yes') {
													o.harga_beli=a.getValue();
													o.harga_beli_tmp=a.getValue();
													o.ketppn=10;
													GFPenerimaan.form.Grid.a.getView().refresh();
													GFPenerimaan.func.refreshNilai();
													GFPenerimaan.form.Grid.a.startEditing(line,8);												
												} else{
													o.harga_beli=o.harga_beli_tmp;
													o.ketppn=10;
													GFPenerimaan.form.Grid.a.getView().refresh();
													GFPenerimaan.func.refreshNilai();
													GFPenerimaan.form.Grid.a.startEditing(line,8);
												}
											})

										}
									} else{
										var o = dsDataGrdJab_viApotekPenerimaan.getRange()[line].data;
										var harga = 0;
										var harga_kecil = 0;
										harga = (parseFloat(a.getValue()) / parseFloat(o.fractions)) / 1.1;
										harga_kecil = parseFloat(o.harga_beli_tmp) / parseFloat(o.fractions);
										Ext.Msg.confirm('Konfirmasi', 'Harga satuan akan diubah dari '+ formatNumberDecimal(harga_kecil) +' menjadi '+ formatNumberDecimal(harga) +'?', function (id, value) { 
											if (id === 'yes') {
												o.harga_beli = a.getValue();
												o.harga_beli_tmp = o.harga_beli;
												o.ketppn=0;
												GFPenerimaan.form.Grid.a.getView().refresh();
												GFPenerimaan.func.refreshHarga();
												o.ketppn=10;
												GFPenerimaan.form.Grid.a.getView().refresh();
												GFPenerimaan.form.Grid.a.startEditing(line,8); 
											} else{
												o.harga_beli = o.harga_beli_tmp;
												o.harga_beli_tmp = o.harga_beli_tmp;
												o.ketppn=10;
												GFPenerimaan.form.Grid.a.getView().refresh();
												// GFPenerimaan.func.refreshHarga();
												GFPenerimaan.func.refreshNilai();
												// o.ketppn=10;
												GFPenerimaan.form.Grid.a.getView().refresh();
												GFPenerimaan.form.Grid.a.startEditing(line,8); 
											}
										})
										/* dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.harga_beli=a.getValue();
										dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.ketppn=0;
										GFPenerimaan.form.Grid.a.getView().refresh();
										GFPenerimaan.func.refreshHarga();
										dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.ketppn=10;
										GFPenerimaan.form.Grid.a.getView().refresh();
										GFPenerimaan.form.Grid.a.startEditing(line,8); */
									}
								})
							}
						},
						focus:function(){
							this.indeks=GFPenerimaan.form.Grid.a.getSelectionModel().selection.cell[0];
						}
					}				
				})
			},{
				dataIndex: 'jml_in_obat',
				xtype : 'numbercolumn',
				decimalPrecision: 0,
				align:'right',
				header: 'Qty',
				width: 65,
				menuDisabled: true,
				fixed: true,
				editor: new Ext.form.NumberField({
					decimalPrecision: 0,
					enableKeyEvents:true,
					listeners:{
						keyDown: function(a,b,c){
							if(b.getKey()==13){
								var line	= this.indeks;
								var b=dsDataGrdJab_viApotekPenerimaan.getRange()[line].data;
								if(b.fractions != 0){
									b.qty_b=parseInt(a.getValue())/parseInt(b.fractions);
								}else{
									b.qty_b=0;
								}
								b.jml_in_obat=a.getValue();
								b.a=true;
								
								GFPenerimaan.func.refreshNilai();
							}
						},
						focus:function(){
							this.indeks=GFPenerimaan.form.Grid.a.getSelectionModel().selection.cell[0];
						},
						specialkey: function(){
							if(Ext.EventObject.getKey() == 13){
								var records = new Array();
								records.push(new dsDataGrdJab_viApotekPenerimaan.recordType());
								dsDataGrdJab_viApotekPenerimaan.add(records);
							}
						}
					}				
				})	
			},{
				dataIndex: 'apt_discount',
				xtype : 'numbercolumn',
				header: 'Disc%',
				align:'right',
				width: 60,
				menuDisabled: true,
				fixed: true,
				editor: new Ext.form.NumberField({
					enableKeyEvents:true,
					listeners:{
						keyDown: function(a,b,c){
							if(b.getKey()==13){
								var line	= this.indeks;
								if(a.getValue()>100){
									dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.apt_discount=100;
								}else{
									dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.apt_discount=a.getValue();
								}
								
								GFPenerimaan.func.refreshNilai();
								GFPenerimaan.form.Grid.a.startEditing(line,9);
							}
						},
						focus:function(){
							this.indeks=GFPenerimaan.form.Grid.a.getSelectionModel().selection.cell[0];
						}
					}				
				})	
			},{
				dataIndex: 'apt_disc_rupiah',
				xtype : 'numbercolumn',
				header: 'Disc Rp',
				align:'right',
				width: 60,
				menuDisabled: true,
				fixed: true,
				editor: new Ext.form.NumberField({
					enableKeyEvents:true,
					listeners:{
						keyDown: function(a,b,c){
							if(b.getKey()==13){
								var line	= this.indeks;
								dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.apt_disc_rupiah=a.getValue();
								GFPenerimaan.func.refreshNilai();
								GFPenerimaan.form.Grid.a.startEditing(line,11);
							}
						},
						focus:function(){
							this.indeks=GFPenerimaan.form.Grid.a.getSelectionModel().selection.cell[0];
						}
					}				
				})
			},{
				dataIndex: 'jumlah',
				xtype : 'numbercolumn',
				header: 'Jumlah',
				align:'right',
				fixed: true,
				menuDisabled: true,
				width: 100
			},{
				dataIndex: 'ppn',
				xtype : 'numbercolumn',
				header: 'Ppn%',
				align:'right',
				menuDisabled: true,
				width: 60,
				fixed: true,
				editor: new Ext.form.NumberField({
					enableKeyEvents:true,
					listeners:{
						keyDown: function(a,b,c){
							if(b.getKey()==13){
								var line	= this.indeks;
								dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.ppn=a.getValue();
								GFPenerimaan.func.refreshNilai();
								GFPenerimaan.form.Grid.a.startEditing(line,13);
							}
						},
						focus:function(){
							this.indeks=GFPenerimaan.form.Grid.a.getSelectionModel().selection.cell[0];
						}
					}				
				})
			},{
				dataIndex: 'sub_total',
				xtype : 'numbercolumn',
				header: 'Sub Total',
				width: 100,
				fixed: true,
				menuDisabled: true,
				align:'right'
			},{
				dataIndex: 'tgl_expire',
				header: 'Tgl Expire',
				width: 80,
				menuDisabled: true,
				fixed: true,
				editor: new Ext.form.DateField({
					format: 'd/m/Y',
					width: 80,
					editable: true,
					listeners:{
						enableKeyEvents:true,
						keyDown: function(a,b,c){
							if(b.getKey()==13){
								tglexpireGFPenerimaan = a.getValue();
								if(convertTglGFPenerimaan(tglexpireGFPenerimaan) < tglNowGFPenerimaan){
									Ext.Msg.alert('WARNING','Tanggal tindakan tidak boleh kurang dari tanggal sekarang!');
								}else {
									var line	= this.indeks;
									GFPenerimaan.form.Grid.a.startEditing(line,16);
								}
							}
						},
						focus:function(){
							this.indeks=GFPenerimaan.form.Grid.a.getSelectionModel().selection.cell[0];
						},
						specialkey : function(a, b){
							if(convertTglGFPenerimaan(tglexpireGFPenerimaan) < tglNowGFPenerimaan){
								Ext.Msg.alert('WARNING','Tanggal tindakan tidak boleh kurang dari tanggal transaksi!');
							}else {
								if (b.getKey() === b.ENTER || b.getKey() === b.TAB) {
									var line = GFPenerimaan.form.Grid.a.getSelectionModel().selection.cell[0];
									GFPenerimaan.form.Grid.a.startEditing(line,16);
								}
							}
						}
					}
				}),
			renderer: Ext.util.Format.dateRenderer('d/M/Y')
				/* editor: new Ext.form.DateField({
					format: 'd/m/Y',
					width: 80,
					editable: true,
					listeners:{
						blur: function(a){
							var line	= this.indeks;
							//dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.tgl_expire=a.getValue();
							GFPenerimaan.form.Grid.a.startEditing(line,14);
						},
						focus:function(){
							this.indeks=GFPenerimaan.form.Grid.a.getSelectionModel().selection.cell[0];
						}
					}
				}) */
			},GFPenerimaan.form.CheckBox.a,
			GFPenerimaan.form.CheckBox.b,
			{
				dataIndex: 'batch',
				header: 'Batch',
				sortable: true,
				fixed: true,
				menuDisabled: true,
				width: 90,
				editor: new Ext.form.TextField({
					listeners:{
						specialkey: function(){
							if(Ext.EventObject.getKey() == 13){
								var line = GFPenerimaan.form.Grid.a.getSelectionModel().selection.cell[0];
								dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.ketppn=10;
								GFPenerimaan.form.Grid.a.startEditing(line,17);
								/* var records = new Array();
								records.push(new dsDataGrdJab_viApotekPenerimaan.recordType());
								dsDataGrdJab_viApotekPenerimaan.add(records);
								var nextRow = dsDataGrdJab_viApotekPenerimaan.getCount()-1; 
								GFPenerimaan.form.Grid.a.startEditing(nextRow,2); */
							}
						}
					}
				})	
			},{
				dataIndex: 'pabrik',
				header: 'Pabrik',
				sortable: true,
				width: 150,
				fixed: true,
				menuDisabled: true,
				editor: new Nci.form.Combobox.autoComplete({
					store	: GFPenerimaan.form.ArrayStore.b,
					select	: function(a,b,c){
						var line	= GFPenerimaan.form.Grid.a.getSelectionModel().selection.cell[0];
						dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.kd_pabrik=b.data.kd_pabrik;
						dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.pabrik=b.data.pabrik;
						dsDataGrdJab_viApotekPenerimaan.getRange()[line].data.ketppn=10;
						GFPenerimaan.form.Grid.a.getView().refresh();
						var records = new Array();
						records.push(new dsDataGrdJab_viApotekPenerimaan.recordType());
						dsDataGrdJab_viApotekPenerimaan.add(records);
						var nextRow = dsDataGrdJab_viApotekPenerimaan.getCount()-1; 
						GFPenerimaan.form.Grid.a.startEditing(nextRow,2);
					},
					insert	: function(o){
						return {
							kd_pabrik        	:o.kd_pabrik,
							pabrik 				: o.pabrik,
							text				:  '<table style="font-size: 11px;"><tr><td width="50">'+o.kd_pabrik+'</td><td width="200">'+o.pabrik+'</td></tr></table>'
						}
					},
					url		: baseURL + "index.php/gudang_farmasi/functionGFPenerimaan/getPabrik",
					valueField: 'pabrik',
					displayField: 'text',
					listWidth: 200,
					onEnter:function(){
						var records = new Array();
						records.push(new dsDataGrdJab_viApotekPenerimaan.recordType());
						dsDataGrdJab_viApotekPenerimaan.add(records);
						var nextRow = dsDataGrdJab_viApotekPenerimaan.getCount()-1; 
						GFPenerimaan.form.Grid.a.startEditing(nextRow,2);
					},
				}),
			},
			{
				dataIndex: 'gin',
				header: 'Gin',
				width: 70,
				hidden:true,
			},
			{
				dataIndex: 'kd_milik',
				header: 'kd_milik',
				width: 70,
				hidden:true,
			},{
				dataIndex: 'milik',
				header: 'milik',
				width: 70,
				hidden:true,
			},{
				dataIndex: 'ketppn',
				header: 'ketppn',
				width: 70,
				hidden:true,
			},
			{
				id:'kd_prdadsasdad',
				dataIndex: 'kd_prd',
				header: 'kd_prd',
				width: 70,
				hidden:true,
			},
			{
				dataIndex: 'harga_satuan_kecil',
				header: 'harga_satuan_kecil',
				width: 70,
				hidden:true,
			},{
				dataIndex: 'harga_beli_tmp',
				header: 'harga_beli_tmp',
				width: 70,
				hidden:true,
			},
        ]),
        plugins:[chkSelected_viApotekPenerimaan,GFPenerimaan.form.CheckBox.a,GFPenerimaan.form.CheckBox.b]
    });    
    return GFPenerimaan.form.Grid.a;
}

function detailGridObatApotekPenerimaan(po_number){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gudang_farmasi/functionGFPenerimaan/getGridDetailObat",
			params: {
				po_number:po_number
			},
			failure: function(o)
			{
				Ext.Msg.alert('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dsDataGrdJab_viApotekPenerimaan.removeAll();
					var recs=[],
						recType=dsDataGrdJab_viApotekPenerimaan.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
					dsDataGrdJab_viApotekPenerimaan.add(recs);
					GFPenerimaan.form.Grid.a.getView().refresh();
					GFPenerimaan.func.refreshNilai();
				}
				else 
				{
					Ext.Msg.alert('Gagal membaca data obat', 'Error');
				};
			}
		}
		
	)
}

function mComboStatusPostingApotekPenerimaan(){
	var cboStatusPostingApotekPenerimaan = new Ext.form.ComboBox({
        id:'cboStatusPostingApotekPenerimaan',
        x: 530,
        y: 30,
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:true,
        mode: 'local',
        width: 110,
        emptyText:'',
        fieldLabel: 'JENIS',
		tabIndex:5,
		value: 'Semua',
        store: new Ext.data.ArrayStore({
            id: 0,
            fields:
            [
                'Id',
                'displayText'
            ],
            data: [[1, 'Semua'],[2, 'Posting'], [3, 'Belum Posting']]
		}),
        valueField: 'Id',
        displayField: 'displayText',
        value:selectCountStatusPostingApotekPenerimaan,
        listeners:{
			'select': function(a,b,c){
				selectCountStatusPostingApotekPenerimaan=b.data.displayText ;

			}
        }
	});
	return cboStatusPostingApotekPenerimaan;
};

function viCombo_Vendor(){
    var Field_Vendor = ['KD_VENDOR', 'VENDOR', 'TERM'];
    ds_Vendor = new WebApp.DataStore({fields: Field_Vendor});
    ds_Vendor.load({
        params:{
                Skip: 0,
                Take: 1000,
                Sort: 'KD_VENDOR',
                Sortdir: 'ASC',
                target: 'ComboPBF',
                param: ""
            }
    });
    GFPenerimaan.form.ComboBox.a = new Ext.form.ComboBox({
			x:530,
			y:0,
            flex: 1,
			id: 'cbo_Vendor',
			name:'pbf',
            fieldLabel: 'Vendor',
			valueField: 'KD_VENDOR',
            displayField: 'VENDOR',
			emptyText:'SEMUA',
			store: ds_Vendor,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:190,
			tabIndex:0,
			listeners:{ 
				'select': function(a,b,c){
					selectSetVendor=b.data.displayText ;
					Q(GFPenerimaan.form.Grid.b).refresh();
				},
				'specialkey' : function(){
					if (Ext.EventObject.getKey() === 13) {
//						GFPenerimaan.func.refresh();
						Q(GFPenerimaan.form.Grid.b).refresh();
					} 						
				}
			}
        }
    )    
    return GFPenerimaan.form.ComboBox.a;
}

function viCombo_VendorLookup(){
    GFPenerimaan.form.ComboBox.b = new Ext.form.ComboBox({
            flex: 1,
            name: 'pbf',
            fieldLabel: 'Vendor',
			valueField: 'KD_VENDOR',
            displayField: 'VENDOR',
			store: ds_Vendor,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:190,
			tabIndex:0,
			listeners:{ 
				'select': function(a,b,c){
					selectSetVendor=b.data.displayText ;
					GFPenerimaan.form.TextField.a.setValue(b.data.KD_VENDOR);
					var jatuh_tempo = new Date().add(Date.DAY, + b.data.TERM);
					GFPenerimaan.form.DateField.a.setValue(jatuh_tempo);
//					Q(GFPenerimaan.form.Grid.b).refresh();
				},
				'specialkey' : function(){
					if (Ext.EventObject.getKey() === 13) {
//						Q(GFPenerimaan.form.Grid.b).refresh();
						GFPenerimaan.form.TextField.d.focus();
					} 						
				}
			}
        }
    )    
    return GFPenerimaan.form.ComboBox.b;
}

function convertTglGFPenerimaan(str){
	var date = new Date(str),
        mnth = ("0" + (date.getMonth()+1)).slice(-2),
        day  = ("0" + date.getDate()).slice(-2);
    return [ date.getFullYear(), mnth, day ].join("-") + ' 00:00:00';
}
shortcut.set({
	code:'main',
	list:[
		{
			key:'f1',
			fn:function(){
				Ext.getCmp('btnTambahPenerimaanGFPenerimaan').el.dom.click();
			}
		},
	]
});