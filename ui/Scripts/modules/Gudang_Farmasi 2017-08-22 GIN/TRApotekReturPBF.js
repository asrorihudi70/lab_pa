var StatusPostingGFReturPBF='0';
var returPBF={
	coba:function(){
		var $this=this;
	},
	vars:{
		lineDetail:null,
		shortcut:false,
		total:0,
		subTotal:0,
		ppn:0
	},
	ArrayStore:{
		gridDetail:new Ext.data.ArrayStore({fields:[]}),
		comboObat:new Ext.data.ArrayStore({fields:[]}),
		gridMain:new Ext.data.ArrayStore({fields:[]})
	},
	Button:{
		deleted:null,
		posting:null,
		save1:null,
		save2:null,
		addDetail:null,
		unposting:null,
		cetak:null
	},
	ComboBox:{
		vendor:null
	},
	DataStore:{
		vendor1:new WebApp.DataStore({fields: ['KD_VENDOR', 'VENDOR']})
	},
	DateField:{
		date:null,
		srchStartDate:null,
		srchLastDate:null
	},
	DisplayField:{
		posting:null
	},
	Grid:{
		detail:null,
		main:null
	},
	ComboBox:{
		vendor1:null
	},
	TextField:{
		subtotal:null,
		noRetur:null,
		remark:null,
		ppn:null,
		reduksi:null,
		total:null,
		srchRetNumber:null
	},
	delDetail:function(){
		var $this=this;
		var line = $this.Grid.detail.getSelectionModel().selection.cell[0];
		var o = $this.ArrayStore.gridDetail.getRange()[line].data;
		Ext.Msg.confirm('Warning', 'Apakah obat ini akan dihapus?', function(button){
			if (button == 'yes'){
				if(o.kd_prd != undefined && (o.ret_line!= undefined || o.ret_line!= '') ){
					Ext.Ajax.request ({
						url: baseURL + "index.php/gudang_farmasi/functionGFReturPBF/deletedetail",
						params:{
							kd_prd: o.kd_prd,
							ret_number: $this.TextField.srchRetNumber.getValue(),
							ret_line: o.ret_line,
						} ,
						failure: function(o) {
							Ext.Msg.alert('Error','Error, delete obat. Hubungi Admin!');
						},	
						success: function(o) 
						{
							var cst = Ext.decode(o.responseText);
							if (cst.success === true)  {
								$this.ArrayStore.gridDetail.removeAt(line);
								$this.vars.lineDetail=null;
								$this.refreshInput();
								Ext.Msg.alert('Information','Berhasil menghapus data obat ini');
							} else  {
								Ext.Msg.alert('Error','Gagal menghapus data obat ini');
							};
						}
					})
				}else{
					$this.ArrayStore.gridDetail.removeAt(line);
					$this.vars.lineDetail=null;
					$this.refreshInput();
				}
			} 
		});
	},
	addDetail:function(){
		var $this=this;
		if(Ext.getCmp('cboPbf_viApotekReturPBF').getValue()!=''){
			$this.TextField.subtotal.focus();
			$this.ArrayStore.gridDetail.add(new $this.ArrayStore.gridDetail.recordType());
			$this.refreshInput();
		}else{
			Ext.Msg.alert('Gagal','Harap Pilih PBF terlebih dahulu.');
		}
	},
	refresh:function(){
		var $this=this;
		loadMask.show();
		$this.DateField.srchStartDate
		var a=[];
		a.push({name: 'ret_number',value:$this.TextField.srchRetNumber.getValue()});
		a.push({name: 'startDate',value:$this.DateField.srchStartDate.value});
		a.push({name: 'lastDate',value:$this.DateField.srchLastDate.value});
		a.push({name: 'kd_vendor',value:Ext.getCmp('cboPbf_viApotekReturPBFFilter').getValue()});
		$.ajax({
			type: 'POST',
			dataType:'JSON',
			url:baseURL + "index.php/gudang_farmasi/functionGFReturPBF/initList",
			data:a,
			success: function(r){
				loadMask.hide();
				if(r.processResult=='SUCCESS'){
					$this.ArrayStore.gridMain.loadData([],false);
					for(var i=0,iLen=r.listData.length; i<iLen ;i++){
						$this.ArrayStore.gridMain.add(new $this.ArrayStore.gridMain.recordType(r.listData[i]));
					}
					$this.Grid.main.getView().refresh();
				}else{
					Ext.Msg.alert('Gagal',r.processMessage);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
	},
	posting:function(){
		var $this=this;
		if($this.TextField.noRetur.getValue()!=''){
			if($this.getParams() != undefined){
				Ext.Msg.confirm('Konfirmasi', 'Apakah Akan diPosting ?', function (id, value) { 
					if (id === 'yes') { 
						loadMask.show();
						$.ajax({
							url:baseURL + "index.php/gudang_farmasi/functionGFReturPBF/posting",
							dataType:'JSON',
							type: 'POST',
							data:$this.getParams(),
							success: function(r){
								loadMask.hide();
								if(r.processResult=='SUCCESS'){
									Ext.Msg.alert('Sukses','Data Berhasi Diubah dan diPosting.');
									for(var i=0; i<$this.ArrayStore.gridDetail.getCount() ; i++){
										var o=$this.ArrayStore.gridDetail.getRange()[i].data;
										o.ret_number=$this.TextField.noRetur.getValue();
									}
									StatusPostingGFReturPBF='1';
									$this.refreshInput();
									Ext.getCmp('btnSaveGFReturPBF').disable();
									Ext.getCmp('btnDeleteGFReturPBF').disable();
									Ext.getCmp('btnPostingGFReturPBF').disable();
									Ext.getCmp('btnAddDetailGFReturPBF').disable();
									Ext.getCmp('btnUnPostingGFReturPBF').enable();
									Ext.getCmp('btnCetakBuktiGFReturPBF').enable();
								}else{
									Ext.Msg.alert('Gagal',r.processMessage);
								}
							},
							error: function(jqXHR, exception) {
								loadMask.hide();
								Nci.ajax.ErrorMessage(jqXHR, exception);
							}
						});
					} 
				}, this); 
			}
		} else{
			Ext.Msg.alert('Warning','Simpan terlebih dahulu sebelum diPosting!');
		}
	},
	unposting:function(){
		var $this=this;
		if($this.getParams() != undefined){
			Ext.Msg.confirm('Konfirmasi', 'Apakah Akan diUnposting ?', function (id, value) { 
				if (id === 'yes') { 
					loadMask.show();
					$.ajax({
						url:baseURL + "index.php/gudang_farmasi/functionGFReturPBF/unposting",
						dataType:'JSON',
						type: 'POST',
						data:{ret_number:$this.TextField.noRetur.getValue()},
						success: function(r){
							loadMask.hide();
							if(r.processResult=='SUCCESS'){
								Ext.Msg.alert('Sukses','Data Berhasi diUnposting.');
								StatusPostingGFReturPBF='0';
								$this.refreshInput();
								Ext.getCmp('btnSaveGFReturPBF').enable();
								Ext.getCmp('btnDeleteGFReturPBF').enable();
								Ext.getCmp('btnPostingGFReturPBF').enable();
								Ext.getCmp('btnAddDetailGFReturPBF').enable();
								Ext.getCmp('btnUnPostingGFReturPBF').disable();
								Ext.getCmp('btnCetakBuktiGFReturPBF').disable();
							}else{
								Ext.Msg.alert('Gagal',r.processMessage);
							}
						},
						error: function(jqXHR, exception) {
							loadMask.hide();
							Nci.ajax.ErrorMessage(jqXHR, exception);
						}
					});
				} 
			}, this); 
		}
	},
	cetak:function(){
		var $this=this;
		Ext.Msg.confirm('Konfirmasi', 'Siap mencetak bill?', function (id, value) { 
			if (id === 'yes') {
				var params={
					ret_number:$this.TextField.noRetur.getValue()
				} ;
				var form = document.createElement("form");
				form.setAttribute("method", "post");
				form.setAttribute("target", "_blank");
				form.setAttribute("action", baseURL + "index.php/gudang_farmasi/cetakan_faktur_bill/cetakreturpbf");
				var hiddenField = document.createElement("input");
				hiddenField.setAttribute("type", "hidden");
				hiddenField.setAttribute("name", "data");
				hiddenField.setAttribute("value", Ext.encode(params));
				form.appendChild(hiddenField);
				document.body.appendChild(form);
				form.submit();		
			}
		})
	},
	save:function(callback){
		var $this=this;
		if($this.getParams() != undefined){
			/* if ($this.validasiSaveReturPBF()==1)	
			{ */
				if($this.TextField.noRetur.getValue()==''){
					loadMask.show();
					$.ajax({
						url:baseURL + "index.php/gudang_farmasi/functionGFReturPBF/save",
						dataType:'JSON',
						type: 'POST',
						data:$this.getParams(),
						success: function(r){
							loadMask.hide();
							if(r.processResult=='SUCCESS'){
								$this.TextField.noRetur.setValue(r.resultObject.code);
								for(var i=0; i<$this.ArrayStore.gridDetail.getCount() ; i++){
									var o=$this.ArrayStore.gridDetail.getRange()[i].data;
									o.ret_number=r.resultObject.code;
								}
								$this.refreshInput();
								Ext.Msg.alert('Sukses','Data Berhasi Disimpan.');
								Ext.getCmp('btnSaveGFReturPBF').enable();
								Ext.getCmp('btnDeleteGFReturPBF').enable();
								Ext.getCmp('btnPostingGFReturPBF').enable();
								Ext.getCmp('btnAddDetailGFReturPBF').enable();
								Ext.getCmp('btnUnPostingGFReturPBF').disable();
								Ext.getCmp('btnCetakBuktiGFReturPBF').disable();
								if(callback != undefined){
									callback();
								}
							}else{
								Ext.Msg.alert('Gagal',r.processMessage);
							}
						},
						error: function(jqXHR, exception) {
							loadMask.hide();
							Nci.ajax.ErrorMessage(jqXHR, exception);
						}
					});
				}else{
					loadMask.show();
					$.ajax({
						url:baseURL + "index.php/gudang_farmasi/functionGFReturPBF/update",
						dataType:'JSON',
						type: 'POST',
						data:$this.getParams(),
						success: function(r){
							loadMask.hide();
							if(r.processResult=='SUCCESS'){
								Ext.Msg.alert('Sukses','Data Berhasi Diubah.');
								for(var i=0; i<$this.ArrayStore.gridDetail.getCount() ; i++){
									var o=$this.ArrayStore.gridDetail.getRange()[i].data;
									o.ret_number=$this.TextField.noRetur.getValue();
								}
								$this.refreshInput();
								if(callback != undefined){
									callback();
								}
								Ext.getCmp('btnSaveGFReturPBF').enable();
								Ext.getCmp('btnDeleteGFReturPBF').enable();
								Ext.getCmp('btnPostingGFReturPBF').enable();
								Ext.getCmp('btnAddDetailGFReturPBF').enable();
								Ext.getCmp('btnUnPostingGFReturPBF').disable();
								Ext.getCmp('btnCetakBuktiGFReturPBF').disable();
							}else{
								Ext.Msg.alert('Gagal',r.processMessage);
							}
						},
						error: function(jqXHR, exception) {
							loadMask.hide();
							Nci.ajax.ErrorMessage(jqXHR, exception);
						}
					});
				}
			//}
				
		}
	},
	validasiSaveReturPBF:function(){
		var $this=this;
		var x = 1;
		for(var i = 0 ; i < $this.ArrayStore.gridDetail.getCount();i++)
		{
			var o=$this.ArrayStore.gridDetail.getRange()[i].data;
			for(var j = i+1 ; j < $this.ArrayStore.gridDetail.getCount();j++)
			{
				var p=$this.ArrayStore.gridDetail.getRange()[j].data;
				if (p.kd_prd==o.kd_prd)
				{
					Ext.Msg.alert('Warning','Obat ada yang Sama');
					x = 0;
				}
			}
			
		}
		return x;
	},
	getParams:function(){
		var $this=this;
		var a=[];
		if(Ext.getCmp('cboPbf_viApotekReturPBF').getValue()!=''){
			a.push({name: 'kd_vendor',value:Ext.getCmp('cboPbf_viApotekReturPBF').getValue()});
		}else{
			Ext.Msg.alert('Error','Harap Pilih Vendor.');
			$('#'+Ext.getCmp('cboPbf_viApotekReturPBF').id).focus();
			return;
		}
		if($this.TextField.remark.getValue()!=''){
			a.push({name: 'remark',value:$this.TextField.remark.getValue()});
		}else{
			Ext.Msg.alert('Error','Harap Isi Remark.');
			$('#'+$this.TextField.remark.id).focus();
			return;
		}
		a.push({name: 'ret_date',value:$this.DateField.date.value});
		a.push({name: 'ret_number',value:$this.TextField.noRetur.getValue()});
		a.push({name: 'ppn',value:toInteger($this.TextField.ppn.getValue())});
		if($this.ArrayStore.gridDetail.getCount()==0){
			Ext.Msg.alert('Error','Obat harus lebih dari 1.');
			return;
		}
		for(var i=0; i<$this.ArrayStore.gridDetail.getCount();i++){
			var o=$this.ArrayStore.gridDetail.getRange()[i].data;
			if(o.nama_obat != undefined && o.nama_obat != ''){
					a.push({name: 'kd_prd[]',value:o.kd_prd});
			}else{
				Ext.Msg.alert('Error','Harap pilih produk pada baris ke-'+(i+1));
				$this.Grid.detail.startEditing(i,2);
				return;
			}
			if(o.qty != 0 && o.qty !=''){
				a.push({name: 'qty[]',value:o.qty});
			}else{
				Ext.Msg.alert('Error','Harap isi qty produk pada baris ke-'+(i+1));
				$this.Grid.detail.startEditing(i,6);
				return;
			}
			if(o.ret_reduksi != undefined && o.ret_reduksi !=''){
				a.push({name: 'ret_reduksi[]',value:o.ret_reduksi});
			}else{
				a.push({name: 'ret_reduksi[]',value:0});
				// return;
			}
			a.push({name: 'tgl_exp[]',value:o.tgl_exp});
			a.push({name: 'batch[]',value:o.batch});
			a.push({name: 'no_obat_in[]',value:o.no_obat_in});
			a.push({name: 'ppn_item[]',value:o.ppn_item});
			a.push({name: 'rcv_line[]',value:o.rcv_line});
		}
		return a;
	},
	/* setShortcut:function(){
		var $this=this;
		$(window).unbind().keydown(function(event) {
    		if($this.vars.shortcut==true){
    			if(event.ctrlKey &&  event.which==66){
    				$this.addDetail();
    				event.preventDefault();
					return false;
    			}
			   	if(event.ctrlKey &&  event.which==83){
    				$this.save();
    				event.preventDefault();
					return false;
    			}
    			if(event.ctrlKey &&  event.which==68){
    				if($this.Button.deleted.disabled==false)$this.delDetail();
    				event.preventDefault();
					return false;
    			}
    		}
		});
	}, */
	refreshInput:function(){
		var $this=this;
		if($this.ArrayStore.gridDetail.getCount()>0){
			$this.Button.deleted.enable();
		}else{
			$this.Button.deleted.disable();
		}
		if($this.TextField.noRetur.getValue()==''){
			$this.Button.posting.disable();	
			$this.Button.unposting.disable();	
			$this.Button.cetak.disable();	
			Ext.getCmp('cboPbf_viApotekReturPBF').enable();
			$this.TextField.remark.enable();
			$this.DateField.date.enable();
		}else{
			$this.Button.posting.enable();	
			$this.Button.unposting.enable();	
			$this.Button.cetak.enable();	
			Ext.getCmp('cboPbf_viApotekReturPBF').disable();
			$this.TextField.remark.disable();
			$this.DateField.date.disable();
		}
		if(StatusPostingGFReturPBF=='0'){
			$this.Button.deleted.enable();
			$this.Button.save1.enable();
			$this.Button.save2.enable();
			$this.Button.addDetail.enable();
			$this.Button.posting.enable();	
			$this.Button.unposting.disable();
			$this.Button.cetak.disable();
			$this.DisplayField.posting.setValue('<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>');
		}else{
			$this.Button.deleted.disable();
			$this.Button.save1.disable();
			$this.Button.save2.disable();
			$this.Button.addDetail.disable();
			$this.Button.posting.disable();	
			$this.Button.unposting.enable();
			$this.Button.cetak.enable();
			$this.DisplayField.posting.setValue('<img src="'+baseURL+'ui/images/icons/16x16/hijau.png"/>');
		}
		var obat={};
		var subTotal=0;
		var reduksi=0;
		for(var i=0;i<$this.ArrayStore.gridDetail.getRange().length ;i++){
			var o=$this.ArrayStore.gridDetail.getRange()[i].data;
			if(o.harga_beli==undefined || o.harga_beli=='')o.harga_beli=0;
			if(o.qty==undefined || o.qty=='')o.qty=0;
			if(o.ret_reduksi==undefined || o.ret_reduksi=='')o.ret_reduksi=0;
			if(o.jml_in_obt==undefined || o.jml_in_obt=='')o.jml_in_obt=0;
			if(o.kd_prd != undefined){
				if(obat[o.kd_prd+'-'+o.no_obat_in+'-'+o.batch] ==undefined){
					obat[o.kd_prd+'-'+o.no_obat_in+'-'+o.batch]={count:parseFloat(o.qty),lastCount:[parseFloat(o.qty)]};
				}else{
					obat[o.kd_prd+'-'+o.no_obat_in+'-'+o.batch].count+=parseFloat(o.qty);
					obat[o.kd_prd+'-'+o.no_obat_in+'-'+o.batch].lastCount.push(parseFloat(o.qty));
				}
				if(o.jml_in_obt<obat[o.kd_prd+'-'+o.no_obat_in+'-'+o.batch].count){
					var jumCount=0
					for(var j=0; j<(obat[o.kd_prd+'-'+o.no_obat_in+'-'+o.batch].lastCount.length-1);j++){
						jumCount+=obat[o.kd_prd+'-'+o.no_obat_in+'-'+o.batch].lastCount[j];
					}
					o.qty=parseFloat(o.jml_in_obt)-jumCount;
					//Ext.Msg.alert('Error','Quantity tidak boleh lebih dari jumlah Stok.');
				}
			}
			o.jumlah=parseFloat(o.qty)*parseFloat(o.harga_beli);
			subTotal+=o.jumlah;
			reduksi+=parseFloat(o.ret_reduksi);
			
		}
		$this.TextField.subtotal.setValue(toFormat(parseFloat(subTotal)));
		var ppn=(parseFloat(subTotal)/100)*10;
		$this.TextField.ppn.setValue(toFormat(ppn));
		$this.TextField.reduksi.setValue(toFormat(reduksi));
		var total=(parseFloat(subTotal)+ppn)-reduksi;
		$this.vars.total=total;
		$this.vars.subTotal=subTotal;
		$this.vars.ppn=ppn;
		$this.TextField.total.setValue(toFormat(total));
		$this.Grid.detail.getView().refresh();
	}
}

var dataSource_viApotekReturPBF;
var selectCount_viApotekReturPBF=50;
var NamaForm_viApotekReturPBF="Retur PBF ";
var mod_name_viApotekReturPBF="viApotekRetur PBF";
var now_viApotekReturPBF= new Date();
var addNew_viApotekReturPBF;
var rowSelected_viApotekReturPBF;
var setLookUps_viApotekReturPBF;
var mNoKunjungan_viApotekReturPBF='';

var CurrentData_viApotekReturPBF ={
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viApotekReturPBF(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
Q(returPBF.Grid.main).refresh();
function ShowPesanWarningReturPBFGF(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorReturPBFGF(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:290
		}
	);
};


function ShowPesanInfoReturPBFGF(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:350
		}
	);
};
function dataGrid_viApotekReturPBF(mod_id_viApotekReturPBF){	
    var $this=returPBF;
	$this.Grid.main = Q().table({
		border:false,
		rowdblclick: function (sm, ridx, cidx,store){
			rowSelected_viApotekReturPBF = store.getAt(ridx);
			if (rowSelected_viApotekReturPBF != undefined){
				setLookUp_viApotekReturPBF(rowSelected_viApotekReturPBF.data);
			}else{
				setLookUp_viApotekReturPBF();
			}
		},
		rowselect: function(sm, row, rec,store){
			rowSelected_viApotekReturPBF = undefined;
			rowSelected_viApotekReturPBF = store.getAt(row);
			
			console.log(rowSelected_viApotekReturPBF);
			CurrentData_viApotekReturPBF
			CurrentData_viApotekReturPBF.row = row;
			CurrentData_viApotekReturPBF.data = store;
			if (rowSelected_viApotekReturPBF.data.ret_post==='0')
							{
								Ext.getCmp('btnHapusTrx_viApotekReturPBFGF').enable();
							}
							else
							{
								Ext.getCmp('btnHapusTrx_viApotekReturPBFGF').disable();
							}
		},
		tbar:{
			xtype: 'toolbar',
			id: 'toolbar_viApotekReturPBF',
			items: 
			[
				{
					xtype: 'button',
					text: 'Tambah [F1]',
					iconCls: 'add',
					tooltip: 'Add Data',
					id:'btnTambahReturGFReturPBF',
					handler: function(sm, row, rec){
						StatusPostingGFReturPBF='0';
						setLookUp_viApotekReturPBF();
					}
				},{
					xtype: 'button',
					text: 'Ubah',
					iconCls: 'Edit_Tr',
					tooltip: 'Edit Data',
					handler: function(sm, row, rec){
						if (rowSelected_viApotekReturPBF != undefined){
							setLookUp_viApotekReturPBF(rowSelected_viApotekReturPBF.data)
						}
					}
				},
				{
						xtype: 'button',
						text: 'Hapus Transaksi',
						iconCls: 'remove',
						tooltip: 'Hapus Data',
						disabled:true,
						id: 'btnHapusTrx_viApotekReturPBFGF',
						handler: function(sm, row, rec)
						{
							var datanya=rowSelected_viApotekReturPBF.data;
							if (datanya===undefined){
								ShowPesanWarningReturPBFGF('Belum ada data yang dipilih','Gudang Farmasi');
							}
							else
							{
								 Ext.Msg.show({
									title: 'Hapus Transaksi',
									msg: 'Anda yakin akan menghapus data transaksi ini ?',
									buttons: Ext.MessageBox.YESNO,
									fn: function (btn) {
										if (btn == 'yes')
										{
											
											console.log(datanya);
											var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan Pembatalan Transaksi:', function (btn, combo) {
														if (btn == 'ok')
														{
															var variablebatalhistori_returpbf = combo;
															if (variablebatalhistori_returpbf != '')
															{
																 Ext.Ajax.request({
										   
																		url: baseURL + "index.php/apotek/functionAPOTEK/hapusTrxReturPBF",
																		 params: {
																			
																			ret_date: datanya.ret_date,
																			ret_number: datanya.ret_number,
																			alasan: variablebatalhistori_returpbf
																		
																		},
																		failure: function(o)
																		{
																			 var cst = Ext.decode(o.responseText);
																			ShowPesanWarningReturPBFGF('Data transaksi tidak dapat dihapus','Gudang Farmasi');
																		},	    
																		success: function(o) {
																			var cst = Ext.decode(o.responseText);
																			if (cst.success===true)
																			{
																				Q(returPBF.Grid.main).refresh();
																				ShowPesanInfoReturPBFGF('Data transaksi Berhasil dihapus','Gudang Farmasi');
																				Ext.getCmp('btnHapusTrx_viApotekReturPBFGF').disable();
																			}
																			else
																			{
																				ShowPesanErrorReturPBFGF('Data transaksi tidak dapat dihapus','Gudang Farmasi');
																			}
																			
																		}
																
																})
															} else
															{
																ShowPesanWarningReturPBFGF('Silahkan isi alasan terlebih dahaulu', 'Keterangan');

															}
														}

													}); 
											
										}
									},
									icon: Ext.MessageBox.QUESTION
								});
							} 
						}
					},
			]
		},
		ajax:function(data,callback){
			loadMask.show();
			var a=[];
			a.push({name: 'ret_number',value:$this.TextField.srchRetNumber.getValue()});
			a.push({name: 'startDate',value:$this.DateField.srchStartDate.value});
			a.push({name: 'lastDate',value:$this.DateField.srchLastDate.value});
			a.push({name: 'kd_vendor',value:Ext.getCmp('cboPbf_viApotekReturPBFFilter').getValue()});
			a.push({name: 'start',value:data.start});
			a.push({name: 'size',value:data.size});
			$.ajax({
				type: 'POST',
				dataType:'JSON',
				url:baseURL + "index.php/gudang_farmasi/functionGFReturPBF/initList",
				data:a,
				success: function(r){
					loadMask.hide();
					if(r.processResult=='SUCCESS'){
						callback(r.listData,r.total);
					}else{
						Ext.Msg.alert('Gagal',r.processMessage);
					}
				},
				error: function(jqXHR, exception) {
					loadMask.hide();
					Nci.ajax.ErrorMessage(jqXHR, exception);
				}
			});
		},
		column:new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(),
			{
				header		: 'Status Posting',
				width		: 20,
				sortable	: false,
				hideable	: true,
				hidden		: false,
				menuDisabled: true,
				dataIndex	: 'ret_post',
				renderer	: function(value, metaData, record, rowIndex, colIndex, store){
					 switch (value){
						 case '1':
							 metaData.css = 'StatusHijau'; 
							 break;
						 case '0':
							 metaData.css = 'StatusMerah';
							 break;
					 }
					 return '';
				}
			},{
				header: 'No. Retur',
				dataIndex: 'ret_number',
				sortable: false,
				menuDisabled: true,
				width: 35
			},{
				header:'Tgl Retur',
				dataIndex: 'ret_date',						
				width: 20,
				sortable: false,
				menuDisabled: true,
				format: 'd/M/Y',
				filter: {},
				renderer: function(v, params, record){
					return ShowDate(record.data.ret_date);
				}
			},{
				header: 'PBF',
				dataIndex: 'vendor',
				sortable: false,
				menuDisabled: true,
				width: 60
			}
		])
	});

    var FrmFilterGridDataView_viApotekReturPBF = new Ext.Panel({
		title: NamaForm_viApotekReturPBF,
		iconCls: 'Studi_Lanjut',
		id: mod_id_viApotekReturPBF,
		region: 'center',
		layout: 'form', 
		closable: true,        
		border: false,  
		margins: '0 5 5 0',
		items: [$this.Grid.main],
		tbar:[
			{
	            xtype: 'buttongroup',
	            columns: 11,
	            defaults: {
	                scale: 'small'
	        	},
	        	frame: false,
	            items: [
		            { 
						xtype: 'tbtext', 
						text: 'No. Retur : ', 
						style:{'text-align':'right'},
						width: 90,
						height: 25
					},						
					$this.TextField.srchRetNumber=new Ext.form.TextField({
						emptyText: 'No. Retur',
						width: 100,
						listeners:{ 
							'specialkey' : function(){
								if (Ext.EventObject.getKey() === 13) {
									Q(returPBF.Grid.main).refresh();
								} 						
							}
						}
					}),{	 
						xtype: 'tbspacer',
						width: 10,
						height: 25
					},{ 
						xtype: 'tbtext', 
						text: 'Tgl Retur : ', 
						style:{'text-align':'right'},
						width: 60,
						height: 25
					},
					$this.DateField.srchStartDate=new Ext.form.DateField({
						value: now_viApotekReturPBF,
						format: 'd/M/Y',
						width: 120,
						listeners:{ 
							'specialkey' : function(){
								if (Ext.EventObject.getKey() === 13){
									Q(returPBF.Grid.main).refresh();
								} 						
							}
						}
					}),{ 
						xtype: 'tbtext', 
						text: ' s.d ', 
						style:{'text-align':'center'},
						width: 30,
						height: 25
					},$this.DateField.srchLastDate=new Ext.form.DateField({
						value: now_viApotekReturPBF,
						format: 'd/M/Y',
						width: 120,
						listeners:{ 
							'specialkey' : function(){
								if (Ext.EventObject.getKey() === 13){
									Q(returPBF.Grid.main).refresh();
								} 						
							}
						}
					}),{	 
						xtype: 'tbspacer',
						width: 10,
						height: 25
					},{ 
						xtype: 'tbtext', 
						text: 'PBF : ', 
						style:{'text-align':'right'},
						width: 30,
						height: 25
					},		
					viCombo_Vendor(150, 'cboPbf_viApotekReturPBFFilter'),						
					{
						xtype: 'button',
						text: 'Cari',
						iconCls: 'refresh',
						tooltip: 'Cari',
						style:{paddingLeft:'30px'},
						width:150,
						handler: function(){					
							DfltFilterBtn_viApotekReturPBF = 1;
							Q(returPBF.Grid.main).refresh();
						}                        
					}
				]
			}
		]
   });
   return FrmFilterGridDataView_viApotekReturPBF;
}

function setLookUp_viApotekReturPBF(rowdata){
    var lebar = 985;
    var $this=returPBF;
    setLookUps_viApotekReturPBF = new Ext.Window({
        title: NamaForm_viApotekReturPBF, 
        closeAction: 'destroy',        
        width: 900,
        height: 500,
        resizable:false,
        layout:{
        	type:'vbox',
        	align:'stretch'
        },
		autoScroll: false,
        border: false,
        constrain : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viApotekReturPBF(lebar,rowdata), //1
        listeners:{
            activate: function(a){
				// $this.vars.shortcut=true;
				// $this.setShortcut();	
				$this.refreshInput();
				shortcuts();
            },
            afterShow: function(){
                this.activate();
            },
            hide:function(){
            	// $this.vars.shortcut=false;
            	$this.ArrayStore.gridDetail.loadData([],false)
            },
            deactivate: function(){
                rowSelected_viApotekReturPBF=undefined;
				mNoKunjungan_viApotekReturPBF = '';
				shortcut.remove('lookup');
            },
			close: function (){
				shortcut.remove('lookup');
			},
        }
    });
    loadMask.show();
    if (rowdata == undefined){
        $.ajax({
			url:baseURL + "index.php/gudang_farmasi/functionGFReturPBF/initTransaksi",
			dataType:'JSON',
			type: 'GET',
			success: function(r){
				loadMask.hide();
				if(r.processResult=='SUCCESS'){
					setLookUps_viApotekReturPBF.show();
				    $this.refreshInput();
				}else{
					Ext.Msg.alert('Error',r.processMessage);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
    }else{
        $.ajax({
 			url:baseURL + "index.php/gudang_farmasi/functionGFReturPBF/getForEdit",
 			dataType:'JSON',
 			type: 'POST',
 			data: {'ret_number':rowdata.ret_number},
 			success: function(r){
 				loadMask.hide();
 				if(r.processResult=='SUCCESS'){
 					setLookUps_viApotekReturPBF.show();
 					var o=r.resultObject;
 					$this.TextField.noRetur.setValue(o.ret_number);
 					var date = Date.parseDate(o.ret_date, "Y-m-d H:i:s");
 					$this.DateField.date.setValue(date);
 					Ext.getCmp('cboPbf_viApotekReturPBF').setValue(o.kd_vendor);
 					$this.TextField.remark.setValue(o.remark);
 					
 					if(r.resultObject.ret_post==1){
 						StatusPostingGFReturPBF='1';
						Ext.getCmp('btnSaveGFReturPBF').disable();
						Ext.getCmp('btnDeleteGFReturPBF').disable();
						Ext.getCmp('btnPostingGFReturPBF').disable();
						Ext.getCmp('btnAddDetailGFReturPBF').disable();
						Ext.getCmp('btnUnPostingGFReturPBF').enable();
						Ext.getCmp('btnCetakBuktiGFReturPBF').enable();
					}else{
						StatusPostingGFReturPBF='0';
						Ext.getCmp('btnSaveGFReturPBF').enable();
						Ext.getCmp('btnDeleteGFReturPBF').enable();
						Ext.getCmp('btnPostingGFReturPBF').enable();
						Ext.getCmp('btnAddDetailGFReturPBF').enable();
						Ext.getCmp('btnUnPostingGFReturPBF').disable();
						Ext.getCmp('btnCetakBuktiGFReturPBF').disable();
 					}
 					$this.ArrayStore.gridDetail.loadData([],false);
 					for(var i=0, iLen=r.listData.length ; i<iLen ; i++){
 						$this.ArrayStore.gridDetail.add(new $this.ArrayStore.gridDetail.recordType(r.listData[i]));
 					}
 					$this.refreshInput();
 				}else{
 					Ext.Msg.alert('Gagal',r.processMessage);
 				}
 			},
 			error: function(jqXHR, exception) {
 				loadMask.hide();
 				Nci.ajax.ErrorMessage(jqXHR, exception);
 			}
 		});
    }
	shortcuts();
}

function shortcuts(){
	shortcut.set({
		code:'lookup',
		list:[
			{
				key:'ctrl+s',
				fn:function(){
					Ext.getCmp('btnSaveGFReturPBF').el.dom.click();
				}
			},
			{
				key:'ctrl+d',
				fn:function(){
					Ext.getCmp('btnDeleteGFReturPBF').el.dom.click();
				}
			},{
				key:'f4',
				fn:function(){
					Ext.getCmp('btnPostingGFReturPBF').el.dom.click();
				}
			},{
				key:'f6',
				fn:function(){
					Ext.getCmp('btnUnPostingGFReturPBF').el.dom.click();
				}
			},
			{
				key:'f12',
				fn:function(){
					Ext.getCmp('btnCetakBuktiGFReturPBF').el.dom.click();
				}
			},
			{
				key:'esc',
				fn:function(){
					setLookUps_viApotekReturPBF.close();
				}
			}
		]
	});
}

function getFormItemEntry_viApotekReturPBF(lebar,rowdata){
	var $this=returPBF;
    var pnlFormDataBasic_viApotekReturPBF = new Ext.FormPanel({
		title: '',
		bodyStyle: 'padding:5px 5px 5px 5px',
		width: lebar,
		layout:{
			type:'vbox',
			align:'stretch'
		},
		flex:1,
		items:[
			getItemPanelInputBiodata_viApotekReturPBF(lebar),
			getItemGridTransaksi_viApotekReturPBF(lebar),
			new Ext.Panel({
				height:50,
				layout:{
					type:'hbox'
				},
				border:false,
				items:[
					new Ext.Panel({
						flex:1,
						layout:'form',
						border:false,
						items:[
							{
								layout:'column',
								border:false,
								items:[
									$this.DisplayField.posting=new Ext.form.DisplayField({
										value		: '<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>'
									}),{
										xtype:'displayfield',
										style:{'text-align':'right','margin-top':'2px'},
										value:'Posted'
									}
								]
							}
						]
					}),
					new Ext.Panel({
						width: 320,
						flex: 1,
						layout:'form',
						border:false,
						items:[
							{
								layout:'column',
								border:false,
								items:[
									{
										xtype: 'displayfield',				
										width: 80,								
										value: 'Reduksi : ',
										style:{'text-align':'right','margin-top':'5px'},
										fieldLabel: 'Label'
									},$this.TextField.reduksi=new Ext.form.TextField({
					                    xtype: 'textfield',
					                    width: 80,
					                    value: 0,
					                    disabled:true,
					                    style:{'padding':'2px','text-align':'right'}
					                }),
									{
										xtype: 'displayfield',				
										width: 80,								
										value: 'Sub Total : ',
										style:{'text-align':'right','margin-top':'5px'},
										fieldLabel: 'Label'
									},$this.TextField.subtotal=new Ext.form.TextField({
					                    style:{'padding':'2px','text-align':'right'},
					                    width: 80,
					                    value: 0,
					                    disabled:true
					                })
								]
							},{
								layout:'column',
								style:{'margin-top': '3px','text-align':'right'},
								border:false,
								items:[
									{
										xtype: 'displayfield',				
										width: 80,								
										value: 'PPN : ',
										style:{'text-align':'right','margin-top':'5px'},
										fieldLabel: 'Label'
									},$this.TextField.ppn=new Ext.form.TextField({
					                    xtype: 'textfield',
					                    width: 80,
					                    value: 0,
					                    disabled:true,
					                    style:{'padding':'2px','text-align':'right'}
					                }),
									{
										xtype: 'displayfield',				
										width: 80,								
										value: '<b>Total</b> : ',
										fieldLabel: 'Label',
										style:{'text-align':'right','margin-top':'5px'}
									},$this.TextField.total=new Ext.form.TextField({
					                    xtype: 'textfield',
										style:{'padding':'2px','text-align':'right'},
					                    width: 80,
					                    value: 0,
					                    disabled:true
					                })
								]
							}
						]
					})
				]
			})
		],
		fileUpload: true,
		tbar: {
			xtype: 'toolbar',
			items: 
			[	
				$this.Button.save1=new Ext.Button({
					text: 'Save',
					iconCls: 'save [CTRL+S]',
					id: 'btnSaveGFReturPBF',
					handler: function(){
						$this.save();
					}
				}),{
					xtype: 'tbseparator'
				},$this.Button.save2=new Ext.Button({
					text: 'Save & Close',
					iconCls: 'saveexit',
					hidden:true,
					handler: function(){
						$this.save(function(){
							setLookUps_viApotekReturPBF.close();
						});
					}
				}),{
					xtype: 'tbseparator',
					hidden:true,
				},$this.Button.posting=new Ext.Button({
					text: 'Posting [F4]',
					iconCls: 'gantidok',
					id: 'btnPostingGFReturPBF',
					handler: function(){
						$this.posting();
					}
				}),{
					xtype: 'tbseparator'
				},
				$this.Button.unposting=new Ext.Button({
					text: 'Unposting [F6]',
					iconCls: 'reuse',
					id: 'btnUnPostingGFReturPBF',
					handler: function(){
						$this.unposting();
					}
				}),{
					xtype: 'tbseparator'
				},
				$this.Button.cetak=new Ext.Button({
					text: 'Print [F12]',
					iconCls: 'print',
					id: 'btnCetakBuktiGFReturPBF',
					handler: function(){
						$this.cetak();
					}
				}),{
					xtype: 'tbseparator'
				}
			]
		}
	});
    return pnlFormDataBasic_viApotekReturPBF;
}

function getItemPanelInputBiodata_viApotekReturPBF(lebar){
    var $this=returPBF;
    var items ={
	    layout: 'Form',
	    xtype:'fieldset',
	    style:'margin-bottom:-1px;',
	    autoHeight:true,
		items:[	
			{
				layout:'column',
				border:false,
				items:[
					Q().input({
						label:'No. Retur',
						width: 250,
						items:[
							$this.TextField.noRetur=new Ext.form.TextField({
								width : 120,
								disabled:true,
								emptyText: 'No Retur'
							})
						]
					}),
					{ 
						xtype: 'tbtext', 
						text: 'PBF', 
						width: 100,
						height: 25
					},
					{ 
						xtype: 'tbtext', 
						text: ': ', 
						width: 10,
						height: 25
					},
					viCombo_Vendor(415, 'cboPbf_viApotekReturPBF')	
				]
			},{
				layout:'column',
				border:false,
				items:[
					Q().input({
						label:'Tanggal',
						width: 250,
						items:[
							$this.DateField.date=new Ext.form.DateField({
								// disabled:true,
								value: now_viApotekReturPBF,
								format: 'd/M/Y',
								width: 120
							})
						]
					}),
					Q().input({
						label:'Remark',
						width: 500,
						items:[
							$this.TextField.remark=new Ext.form.TextField({
								xtype: 'textfield',
								width : 360,	
								emptyText: 'Remark',
								listeners:{
									'specialkey': function(me, e){
										$this.vars.lineDetail=0;
										if (Ext.EventObject.getKey() === 13){
											if($this.ArrayStore.gridDetail.getCount()==0){
												$this.addDetail();
												$this.vars.lineDetail=$this.ArrayStore.gridDetail.getCount()-1;
												$this.Grid.detail.startEditing($this.ArrayStore.gridDetail.getCount()-1,2);
											}else{
												$this.Grid.detail.startEditing(0,2);
												$this.vars.lineDetail=0;
											}
										}
										if (e.getKey() == e.TAB) {
											if($this.ArrayStore.gridDetail.getCount()==0){
												$this.addDetail();
												$this.vars.lineDetail=$this.ArrayStore.gridDetail.getCount()-1;
												$this.Grid.detail.startEditing($this.ArrayStore.gridDetail.getCount()-1,3);
											}else{
												$this.Grid.detail.startEditing(0,2);
												$this.vars.lineDetail=0;
											}
					                    }
									}
								}
							})
						]
					})
				]
			}
		]
	};
    return items;
};

function getItemGridTransaksi_viApotekReturPBF(lebar) {
    var items ={
	    layout: 'fit',
	    anchor: '100%',	    
		border:false,
		style:{'padding-bottom':'10px'},
		flex: 1,
		width: lebar-80,
	    items:gridDataViewEdit_viApotekReturPBF()
	};
    return items;
};

function gridDataViewEdit_viApotekReturPBF(){
	var $this=returPBF;
	$this.Grid.detail=new Ext.grid.EditorGridPanel({
        store: $this.ArrayStore.gridDetail,
        flex:1,
        style:'margin-top:-1px;',
        stripeRows:true,
        columnLines:true,
		selModel: new Ext.grid.CellSelectionModel({
            singleSelect: true,
            listeners:{
                cellselect: function(sm, row, rec){
					// console.log(row)
                	// $this.vars.lineDetail=row;
                }
            }
        }),
        tbar:[
    		$this.Button.addDetail=new Ext.Button({
				text: 'Add',
				iconCls: 'add',
				tooltip: 'Edit Data',
				id:'btnAddDetailGFReturPBF',
				handler: function(sm, row, rec){
					StatusPostingGFReturPBF='0';
					$this.addDetail();
					$this.vars.lineDetail=$this.ArrayStore.gridDetail.getCount()-1;
					$this.Grid.detail.startEditing($this.ArrayStore.gridDetail.getCount()-1,2);
				}
			}),{
				xtype: 'tbseparator'
			},$this.Button.deleted=new Ext.Button({
				text: 'Delete Item',
				iconCls: 'remove',
				id:'btnDeleteGFReturPBF',
				handler: function(){
					$this.delDetail();
				}
			}),{
				xtype: 'tbseparator'
			}
        ],
        cm: new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(),
			{			
				dataIndex: 'kd_milik',
				header: 'M',
				sortable: false,
				menuDisabled: true,
				width: 20
			},
			{			
				dataIndex: 'no_obat_in',
				header: 'No Penerimaan',
				sortable: false,
				menuDisabled: true,
				width: 60
			},{			
				dataIndex: 'nama_obat',
				header: 'Uraian',
				sortable: false,
				menuDisabled: true,
				flex: 1,
				editor:new Nci.form.Combobox.autoComplete({
					store	: returPBF.ArrayStore.comboObat,
					select	: function(a,b,c){
						var line	= $this.vars.lineDetail;
						if($this.ArrayStore.gridDetail.getCount()-1===0)
						{
							$this.ArrayStore.gridDetail.getRange()[line].data.nama_obat=b.data.nama_obat;
							$this.ArrayStore.gridDetail.getRange()[line].data.kd_milik=b.data.kd_milik;
							$this.ArrayStore.gridDetail.getRange()[line].data.no_obat_in=b.data.no_obat_in;
							$this.ArrayStore.gridDetail.getRange()[line].data.kd_prd=b.data.kd_prd;
							$this.ArrayStore.gridDetail.getRange()[line].data.satuan=b.data.satuan;
							$this.ArrayStore.gridDetail.getRange()[line].data.harga_beli=b.data.harga_beli;
							$this.ArrayStore.gridDetail.getRange()[line].data.jml_in_obt=b.data.jml_in_obt;
							$this.ArrayStore.gridDetail.getRange()[line].data.batch=b.data.batch;
							$this.ArrayStore.gridDetail.getRange()[line].data.tgl_exp=b.data.tgl_exp;
							$this.ArrayStore.gridDetail.getRange()[line].data.min_stok=b.data.min_stok;
							$this.ArrayStore.gridDetail.getRange()[line].data.ppn_item=b.data.ppn_item;
							$this.ArrayStore.gridDetail.getRange()[line].data.rcv_line=b.data.rcv_line;
							$this.refreshInput();
							$this.Grid.detail.startEditing($this.vars.lineDetail,6);
						}else{
							var VALUE_x=1;
							for(var i = 0 ; i < $this.ArrayStore.gridDetail.getCount();i++)
							{
								if($this.ArrayStore.gridDetail.getRange()[i].data.kd_prd===b.data.kd_prd)
								{
									VALUE_x=0;
								}
							}
							if(VALUE_x===0){
								Ext.Msg.alert('Perhatian','Anda telah memilih Obat yang sama');
								$this.ArrayStore.gridDetail.removeAt(line);
							}else{
								$this.ArrayStore.gridDetail.getRange()[line].data.nama_obat=b.data.nama_obat;
								$this.ArrayStore.gridDetail.getRange()[line].data.kd_milik=b.data.kd_milik;
								$this.ArrayStore.gridDetail.getRange()[line].data.no_obat_in=b.data.no_obat_in;
								$this.ArrayStore.gridDetail.getRange()[line].data.kd_prd=b.data.kd_prd;
								$this.ArrayStore.gridDetail.getRange()[line].data.satuan=b.data.satuan;
								$this.ArrayStore.gridDetail.getRange()[line].data.harga_beli=b.data.harga_beli;
								$this.ArrayStore.gridDetail.getRange()[line].data.jml_in_obt=b.data.jml_in_obt;
								$this.ArrayStore.gridDetail.getRange()[line].data.batch=b.data.batch;
								$this.ArrayStore.gridDetail.getRange()[line].data.tgl_exp=b.data.tgl_exp;
								$this.ArrayStore.gridDetail.getRange()[line].data.min_stok=b.data.min_stok;
								$this.ArrayStore.gridDetail.getRange()[line].data.ppn_item=b.data.ppn_item;
								$this.ArrayStore.gridDetail.getRange()[line].data.rcv_line=b.data.rcv_line;
								$this.refreshInput();
								$this.Grid.detail.startEditing($this.vars.lineDetail,6);
							}
						}
					},
					insert	: function(o){
						return {
							kd_prd        	:o.kd_prd,
							nama_obat 		: o.nama_obat,
							satuan			: o.satuan,
							harga_beli		: o.harga_beli,
							jml_in_obt		: o.jml_in_obt,
							no_obat_in		: o.no_obat_in,
							batch			: o.batch,
							tgl_exp			: o.tgl_exp,
							milik			: o.milik,
							jml_stok_apt	: o.jml_stok_apt,
							ppn_item		: o.ppn_item,
							rcv_line		: o.rcv_line,
							text			:  '<table style="font-size: 11px;"><tr><td width="60">'+o.kd_prd+'</td><td width="270">'+o.nama_obat+'</td><td width="170">'+o.no_obat_in+'</td><td width="60">'+o.batch+'</td><td width="50">'+o.jml_stok_apt+'</td><td width="50">'+o.milik+'</td></tr></table>'
						}
					},
					param:function(){
						return {pbf:Ext.getCmp('cboPbf_viApotekReturPBF').getValue()}
					},
					url		: baseURL + "index.php/gudang_farmasi/functionGFReturPBF/getObat",
					valueField: 'nama_obat',
					displayField: 'text',
					listWidth: 450,
					blur: function(){
					}
				})
			},{
				dataIndex: 'satuan',
				header: 'Sat K',
				sortable: false,
				menuDisabled: true,
				width: 40
			},{
				dataIndex: 'harga_beli',
				header: 'Harga',
				xtype:'numbercolumn',
				align:'right',
				sortable: false,
				menuDisabled: true,
				width: 40
			},{
				dataIndex: 'jml_in_obt',
				header: 'Stock',
				xtype:'numbercolumn',
				align:'right',
				sortable: false,
				menuDisabled: true,
				width: 40
			},{
				dataIndex: 'qty',
				xtype:'numbercolumn',
				header: 'Qty',
				sortable: false,
				menuDisabled: true,
				align:'right',
				width: 30,
				editor:new Ext.form.NumberField({
					enableKeyEvents:true,
					listeners:{
						keyDown: function(a,b,c){
							if(b.getKey()==13){
								if (parseFloat(a.getValue()) >= parseFloat($this.ArrayStore.gridDetail.getRange()[this.indeks].data.min_stok))
								{
									Ext.Msg.alert('Perhatian','Qty sudah mencapai atau melebihi minimum stok.');
									//PengeluaranGF.form.ArrayStore.detail.getRange()[line].data.qty=PengeluaranGF.form.ArrayStore.detail.getRange()[line].data.min_stok;
								}
								else (parseFloat(a.getValue()) < parseFloat($this.ArrayStore.gridDetail.getRange()[this.indeks].data.min_stok))
								{
									$this.ArrayStore.gridDetail.getRange()[this.indeks].data.qty=a.getValue();
								}
								//$this.ArrayStore.gridDetail.getRange()[this.indeks].data.qty=a.getValue();
								$this.refreshInput();
								var row=this.indeks;
								$this.Grid.detail.startEditing(row,7);
							}
						},
						focus:function(){
							this.indeks=$this.vars.lineDetail;
						},
						specialkey: function(){
							if(Ext.EventObject.getKey() == 13){
								// var records = new Array();
								// records.push(new $this.ArrayStore.gridDetail.recordType());
								// $this.ArrayStore.gridDetail.add(records);
								$this.vars.lineDetail=$this.ArrayStore.gridDetail.getCount()-1;
								// $this.Grid.detail.startEditing($this.ArrayStore.gridDetail.getCount()-1,2);
								// var line=$this.Grid.detail.getSelectionModel().selection.cell[0];
								$this.Grid.detail.startEditing($this.vars.lineDetail,7);
							}
						}
					}
				})	
			},
			{
				dataIndex: 'ret_reduksi',
				xtype:'numbercolumn',
				header: 'Reduksi',
				sortable: false,
				menuDisabled: true,
				align:'right',
				width: 50,
				editor:new Ext.form.NumberField({
					enableKeyEvents:true,
					listeners:{
						keyDown: function(a,b,c){
							if(b.getKey()==13){
								var row=this.indeks;
								var o = $this.ArrayStore.gridDetail.getRange()[row].data;
								$this.refreshInput();
								o.jumlah=parseFloat(o.jumlah)-parseFloat(o.ret_reduksi);
								$this.Grid.detail.getView().refresh();
								var row=this.indeks + 1;
								$this.Grid.detail.startEditing(row,2);
							}
						},
						focus:function(){
							this.indeks=$this.vars.lineDetail;
						},
						specialkey: function(){
							if(Ext.EventObject.getKey() == 13){
								var records = new Array();
								records.push(new $this.ArrayStore.gridDetail.recordType());
								$this.ArrayStore.gridDetail.add(records);
								$this.vars.lineDetail=$this.ArrayStore.gridDetail.getCount()-1;
								$this.Grid.detail.startEditing($this.ArrayStore.gridDetail.getCount()-1,2);
								// var line=$this.Grid.detail.getSelectionModel().selection.cell[0];
								$this.Grid.detail.startEditing($this.vars.lineDetail,2);
							}
						}
					}
				})	
			},{
				dataIndex: 'jumlah',
				header: 'Jumlah',
				xtype:'numbercolumn',
				sortable: false,
				menuDisabled: true,
				align:'right',
				width: 50
			},{
				dataIndex: 'tgl_exp',
				header: 'Expired',
				format: 'd/m/Y',
				width: 50,
				menuDisabled: true,
				renderer: function(v, params, record){
					return ShowDate(record.data.tgl_exp);
				}
			},{
				dataIndex: 'batch',
				header: 'No. Batch',
				sortable: false,
				menuDisabled: true,
				width: 40
			},{
				dataIndex: 'ppn_item',
				header: 'PPN%',
				align:'right',
				sortable: false,
				menuDisabled: true,
				width: 40
			},
			{
				xtype:'numbercolumn',
				dataIndex: 'min_stok',
				header: 'Min Stok',
				align: 'right',
				hidden :true,
				sortable: true,
				width: 100	
			},
			{
				dataIndex: 'ret_line',
				header: 'ret_line',
				align: 'right',
				hidden :true,
				sortable: true,
				width: 100	
			},
			{
				dataIndex: 'rcv_line',
				header: 'rcv_line',
				align: 'right',
				hidden :true,
				sortable: true,
				width: 100	
			}
        ]),
        viewConfig:{
        	forceFit:true
        }
    });   
    return $this.Grid.detail;
}
function viCombo_Vendor(lebar,Nama_ID){
	var $this=returPBF;
	var b=new WebApp.DataStore({fields: ['KD_VENDOR', 'VENDOR']});
	b.load({
        params:{
            Skip: 0,
            Take: 1000,
            Sort: 'KD_VENDOR',
            Sortdir: 'ASC',
            target: 'ComboPBF',
            param: ""
        }
    });
    var a = new Ext.form.ComboBox ({
        fieldLabel: 'Vendor',
		valueField: 'KD_VENDOR',
        displayField: 'VENDOR',
		emptyText:'PBF',
		store: b,
        width: 200,
        mode: 'local',
        typeAhead: true,
        triggerAction: 'all',                        
        name: Nama_ID,
        lazyRender: true,
        id: Nama_ID,
		listeners:{ 
			select:function(a){
				if(a.id=='cboPbf_viApotekReturPBF'){
					$this.ArrayStore.gridDetail.loadData([],false);
					$this.refreshInput();
				}
			}
		}
    });    
    return a;
}

shortcut.set({
	code:'main',
	list:[
		{
			key:'f1',
			fn:function(){
				Ext.getCmp('btnTambahReturGFReturPBF').el.dom.click();
			}
		},
	]
});