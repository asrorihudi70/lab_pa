
var selectCount_PermintaanKegudang_farmasi=50;
var NamaForm_PermintaanKegudang_farmasi="Permintaan Ke Unit / Gudang Farmasi";
var selectCountStatusPostingKegudang_farmasi='Semua';//
var mod_name_PermintaanKegudang_farmasi="PermintaanKegudang_farmasi";
var now_PermintaanKegudang_farmasi= new Date();
var rowSelected_PermintaanKegudang_farmasi;
var rowSelectedGridobat_PermintaanKegudang_farmasi;
var setLookUps_PermintaanKegudang_farmasi;
var selectSetUnit;
var cellSelecteddeskripsiRWI;
var tanggal = now_PermintaanKegudang_farmasi.format("d/M/Y");
var jam = now_PermintaanKegudang_farmasi.format("H/i/s");
var tmpkriteria;
var gridDTLKegudang_farmasi_Kegudang_farmasi;
var GridDataView_PermintaanKegudang_farmasi;
var cboUnitTujuanKegudang_farmasi;
var ordered;
var current_no_ro_unit;
var currentKdUnitFarTujuanGF;

var CurrentKegudang_farmasi =
{
    data: Object,
    details: Array,
    row: 0
};

var CurrentData_PermintaanKegudang_farmasi =
{
	data: Object,
	details: Array,
	row: 0
};

var CurrentDataobat_PermintaanKegudang_farmasi =
{
	data: Object,
	details: Array,
	row: 0
};


CurrentPage.page = dataGrid_PermintaanKegudang_farmasi(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

var Kegudang_farmasi={};
Kegudang_farmasi.form={};
Kegudang_farmasi.func={};
Kegudang_farmasi.vars={};
Kegudang_farmasi.func.parent=Kegudang_farmasi;
Kegudang_farmasi.form.ArrayStore={};
Kegudang_farmasi.form.ComboBox={};
Kegudang_farmasi.form.DataStore={};
Kegudang_farmasi.form.Record={};
Kegudang_farmasi.form.Form={};
Kegudang_farmasi.form.Grid={};
Kegudang_farmasi.form.Panel={};
Kegudang_farmasi.form.TextField={};
Kegudang_farmasi.form.Button={};

Kegudang_farmasi.form.ArrayStore.obat=new Ext.data.ArrayStore({
		id: 0,
		fields: [	
					'kd_prd','nama','tgl_masuk','jam_masuk','no_kamar','nama_kamar'
				],
		data: []
	});
	
Kegudang_farmasi.form.ArrayStore.waktu	= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_waktu','waktu'],
	data: []
});

Kegudang_farmasi.form.ArrayStore.jenisKegudang_farmasi	= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_jenis','jenis_Kegudang_farmasi','harga_pokok'],
	data: []
});

function dataGrid_PermintaanKegudang_farmasi(mod_id_PermintaanKegudang_farmasi){	
    var FieldMaster_PermintaanKegudang_farmasi = 
	[
		'no_ro_unit', 'kd_milik', 'nm_unit_far','kd_unit_far', 'tgl_ro', 'milik','kd_unit_far_tujuan','nm_unit_far_tujuan'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_PermintaanKegudang_farmasi = new WebApp.DataStore
	({
        fields: FieldMaster_PermintaanKegudang_farmasi
    });
    dataGriAwal();
	loaddatacomboUnitTujuanKegudang_farmasi();
    // Grid Apotek permintaan # --------------
	GridDataView_PermintaanKegudang_farmasi = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_PermintaanKegudang_farmasi,
			autoScroll: true,
			columnLines: true,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_PermintaanKegudang_farmasi = undefined;
							rowSelected_PermintaanKegudang_farmasi = dataSource_PermintaanKegudang_farmasi.getAt(row);
							CurrentData_PermintaanKegudang_farmasi
							CurrentData_PermintaanKegudang_farmasi.row = row;
							CurrentData_PermintaanKegudang_farmasi.data = rowSelected_PermintaanKegudang_farmasi.data;
							console.log(rowSelected_PermintaanKegudang_farmasi.data);
							
							current_no_ro_unit=rowSelected_PermintaanKegudang_farmasi.data.no_ro_unit;
							ordered=rowSelected_PermintaanKegudang_farmasi.data.qty_ordered;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_PermintaanKegudang_farmasi = dataSource_PermintaanKegudang_farmasi.getAt(ridx);
					if (rowSelected_PermintaanKegudang_farmasi != undefined)
					{	if (rowSelected_PermintaanKegudang_farmasi.data.qty_ordered===1)
						{
							// ShowPesanWarningKegudang_farmasi('Permintaan unit ini telah dilayani', 'Warning');
							setLookUp_PermintaanKegudang_farmasi(rowSelected_PermintaanKegudang_farmasi.data, rowSelected_PermintaanKegudang_farmasi.data.qty_ordered);
							ordered='t';
						}
						else{
							
							setLookUp_PermintaanKegudang_farmasi(rowSelected_PermintaanKegudang_farmasi.data, rowSelected_PermintaanKegudang_farmasi.data.qty_ordered);
							ordered='f';
						}
					}
					
					
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Apotek permintaan
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header		: 'Dilayani',
						width		: 30,
						sortable	: false,
						hideable	: true,
						hidden		: false,
						menuDisabled: true,
						dataIndex	: 'qty_ordered',
						renderer	: function(value, metaData, record, rowIndex, colIndex, store){
							 switch (value){
								 case 1:
									 metaData.css = 'StatusHijau'; 
									 break;
								 case 0:
									 metaData.css = 'StatusMerah';
									 break;
							 }
							 return '';
						}
					},
					{
						header: 'No. Permintaan',
						dataIndex: 'no_ro_unit',
						sortable: true,
						width: 35
						
					},
					//-------------- ## --------------
					{
						header: 'Unit farmasi',
						dataIndex: 'nm_unit_far',
						sortable: true,
						width: 35
					},
					//-------------- ## --------------
					{
						header: 'Unit tujuan',
						dataIndex: 'nm_unit_far_tujuan',
						sortable: true,
						width: 35
					},
					//-------------- ## --------------
					{
						header:'Tgl Minta',
						dataIndex: 'tgl_ro',						
						width: 30,
						sortable: true,
						hideable:false,
                        menuDisabled:true,
						renderer: function(v, params, record)
						{
						return ShowDate(record.data.tgl_ro);
						}
					},
					//-------------- ## --------------
					{
						header: 'Milik',
						dataIndex: 'milik',
						sortable: true,
						width: 50
					},
					//-------------- ## --------------
					//-------------- ## --------------
					{
						header: 'kd_milik',
						dataIndex: 'kd_milik',
						sortable: true,
						width: 40,
						hidden:true
					},{
						header: 'kd_unit_far',
						dataIndex: 'kd_unit_far',
						sortable: true,
						width: 40,
						hidden:true
					},
					{
						header: 'kd_unit_far_tujuan',
						dataIndex: 'kd_unit_far_tujuan',
						sortable: true,
						width: 40,
						hidden:true
					}
					//-------------- ## --------------
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_PermintaanKegudang_farmasi',
				items: 
				[
					{
						xtype: 'button',
						text: 'Tambah Permintaan ',
						iconCls: 'Edit_Tr',
						tooltip: 'Add Data',
						id: 'btnTambah_PermintaanKegudang_farmasi',
						handler: function(sm, row, rec)
						{
							setLookUp_PermintaanKegudang_farmasi(undefined, null);								
						}
					},
					{
						xtype: 'button',
						text: 'Hapus permintaan',
						iconCls: 'remove',
						tooltip: 'Hapus permintaan',
						id: 'btnDeleteTrans_PermintaanKegudang_farmasi',
						handler: function(sm, row, rec)
						{
							
							if(ordered == 't'){
								ShowPesanWarningKegudang_farmasi('Permintaan ini sudah dilayani, tidak dapat di Hapus!','Warning')
							} else{
								var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan penghapusan permintaan :', function (btn, combo) {
									if (btn == 'ok')
									{
										var alasanhapusKegudang_farmasi = combo;
										if (alasanhapusKegudang_farmasi != '')
										{
											Ext.Ajax.request
												(
													{
														url: baseURL + "index.php/gudang_farmasi/functionPermintaan/deletetrans",
														params:{
															no_ro_unit:current_no_ro_unit,
															alasan:alasanhapusKegudang_farmasi
														} ,
														failure: function(o)
														{
															loadMask.hide();
															ShowPesanErrorKegudang_farmasi('Hubungi Admin', 'Error');
														},	
														success: function(o) 
														{
															var cst = Ext.decode(o.responseText);
															if (cst.success === true) 
															{
																dsDataGrdobat_PermintaanKegudang_farmasi.removeAll();
																dataGriAwal();
																ShowPesanInfoKegudang_farmasi('Berhasil menghapus permintaan', 'Information');
															}
															else 
															{
																loadMask.hide();
																ShowPesanErrorKegudang_farmasi('Gagal menghapus permintaan', 'Error');
															};
														}
													}
													
												)
										}else
										{
											loadMask.hide();
											ShowPesanErrorKegudang_farmasi('Silahkan isi alasan terlebih dahulu', 'Keterangan');
										}
									}	
								});
							}
							
						}
					},
					/* {
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_PermintaanKegudang_farmasi',
						handler: function(sm, row, rec)
						{
							if (rowSelected_PermintaanKegudang_farmasi != undefined)
							{
								setLookUp_PermintaanKegudang_farmasi(rowSelected_PermintaanKegudang_farmasi.data)
							} else{
								ShowPesanWarningKegudang_farmasi('Pilih data yang akan di edit','Warning');
							}
							
						}
					} */
					//-------------- ## --------------
				]
			},
			viewConfig: 
			{
				forceFit: true
			}
		}
    )
	
	var pencarianKegudang_farmasi = new Ext.FormPanel({
        labelAlign: 'top',
        frame:true,
        title: '',
        bodyStyle:'padding:5px 5px 0',
        //width: 600,
        items: [
		{
			layout: 'column',
			border: false,
			items:
			[
				{
					columnWidth:.98,
					layout: 'absolute',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					width: 500,
					height: 70,
					anchor: '100% 100%',
					items:
					[
						{
							x: 10,
							y: 0,
							xtype: 'label',
							text: 'No. Permintaan'
						},
						{
							x: 120,
							y: 0,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 0,
							xtype: 'textfield',
							id: 'TxtFilter_PermintaanKegudang_farmasi',
							name: 'TxtFilter_PermintaanKegudang_farmasi',
							emptyText: '',
							width: 130,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
									dataGriAwal(Ext.getCmp('TxtFilter_PermintaanKegudang_farmasi').getValue(),
												Ext.getCmp('dateFilter_PermintaanKegudang_farmasi').getValue()
									);
									} 						
								}
							}
						},
						{
							x: 10,
							y: 30,
							xtype: 'label',
							text: 'Tanggal Permintaan'
						},
						{
							x: 120,
							y: 30,
							xtype: 'label',
							text: ':'
						},
						{
							x: 130,
							y: 30,
							xtype: 'datefield',
							id: 'dateFilter_PermintaanKegudang_farmasi',
							name: 'dateFilter_PermintaanKegudang_farmasi',
							format: 'd/M/Y',
							value:now_PermintaanKegudang_farmasi,
							width: 130,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
									dataGriAwal(Ext.getCmp('TxtFilter_PermintaanKegudang_farmasi').getValue(),
									Ext.getCmp('dateFilter_PermintaanKegudang_farmasi').getValue(),
									Ext.getCmp('dateFilter2_PermintaanKegudang_farmasi').getValue()
									);
									} 						
								}
							}
						},
						{
							x: 270,
							y: 30,
							xtype: 'datefield',
							id: 'dateFilter2_PermintaanKegudang_farmasi',
							name: 'dateFilter2_PermintaanKegudang_farmasi',
							format: 'd/M/Y',
							value:now_PermintaanKegudang_farmasi,
							width: 130,
							tabIndex:1,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
									dataGriAwal(Ext.getCmp('TxtFilter_PermintaanKegudang_farmasi').getValue(),
									Ext.getCmp('dateFilter_PermintaanKegudang_farmasi').getValue(),
									Ext.getCmp('dateFilter2_PermintaanKegudang_farmasi').getValue());
									} 						
								}
							}
						},
						{
							x: 280,
							y: 0,
							xtype: 'button',
							text: 'Cari',
							iconCls: 'refresh',
							tooltip: 'Cari',
							style:{paddingLeft:'30px'},
							width:150,
							id: 'BtnFilterGridDataView_PermintaanKegudang_farmasi',
							handler: function() 
							{	
								dataGriAwal(Ext.getCmp('TxtFilter_PermintaanKegudang_farmasi').getValue(),
									Ext.getCmp('dateFilter_PermintaanKegudang_farmasi').getValue(),
									Ext.getCmp('dateFilter2_PermintaanKegudang_farmasi').getValue()
									);						
							}                        
						}
					]
				}
			]
		}
		]	
	})

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_PermintaanKegudang_farmasi = new Ext.Panel
    (
		{
			title: NamaForm_PermintaanKegudang_farmasi,
			iconCls: 'Studi_Lanjut',
			id: mod_id_PermintaanKegudang_farmasi,
			region: 'center',
			layout: 'form', 
			closable: true,        
			border: false,  
			margins: '0 5 5 0',
			items: [ pencarianKegudang_farmasi,
					GridDataView_PermintaanKegudang_farmasi],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_PermintaanKegudang_farmasi,
		            columns: 21,
		            defaults: {
					scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
					]
					//-------------- # End items # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_PermintaanKegudang_farmasi;
    //-------------- # End form filter # --------------
}

function setLookUp_PermintaanKegudang_farmasi(rowdata, order){
    var lebar = 985;
    setLookUps_PermintaanKegudang_farmasi = new Ext.Window({
        id: Nci.getId(),
        title: NamaForm_PermintaanKegudang_farmasi, 
        closeAction: 'destroy',        
        width: 900,
        height: 580,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_PermintaanKegudang_farmasi(lebar,rowdata),
        listeners:{
            activate: function(){
				
            },
            afterShow: function(){
                this.activate();
            },
            deactivate: function(){
                rowSelected_PermintaanKegudang_farmasi=undefined;
            }
        }
    });
	shortcut.set({
		code:'lookup',
		list:[
			{
				key:'ctrl+s',
				fn:function(){
					Ext.getCmp('btnSave_PermintaanKegudang_farmasi').el.dom.click();
				}
			},
			{
				key:'ctrl+d',
				fn:function(){
					Ext.getCmp('btnDelete_PermintaanKegudang_farmasi').el.dom.click();
				}
			},
			// {
				// key:'f4',
				// fn:function(){
					// Ext.getCmp('btnPostingGFPenghapusan').el.dom.click();
				// }
			// },{
				// key:'f6',
				// fn:function(){
					// Ext.getCmp('btnUnPostingGFPenghapusan').el.dom.click();
				// }
			// },
			// {
				// key:'f12',
				// fn:function(){
					// Ext.getCmp('btnCetakBuktiGFPengeluaranKeUnit').el.dom.click();
				// }
			// },
			{
				key:'esc',
				fn:function(){
					setLookUps_PermintaanKegudang_farmasi.close();
				}
			}
		]
	});

    setLookUps_PermintaanKegudang_farmasi.show();
    if (rowdata == undefined){	
    }
    else
    {
        datainit_PermintaanKegudang_farmasi(rowdata);
    }
    if (order != null) {
    	if (order == 't' || order === true || order == 'true') {
			Ext.getCmp('btnAdd_PermintaanKegudang_farmasi').disable();
			Ext.getCmp('btnSave_PermintaanKegudang_farmasi').disable();
			Ext.getCmp('btnAddobatKegudang_farmasiL').disable();
			Ext.getCmp('btnDelete_PermintaanKegudang_farmasi').disable();
			Ext.getCmp('cboUnitTujuanKegudang_farmasi').disable();
			Ext.getCmp('txtKeterangan_farmasiL').disable();
			Kegudang_farmasi.form.Grid.a.disable();
    	}else{
			Ext.getCmp('btnAdd_PermintaanKegudang_farmasi').enable();
			Ext.getCmp('btnSave_PermintaanKegudang_farmasi').enable();
			Ext.getCmp('btnAddobatKegudang_farmasiL').enable();
			Ext.getCmp('btnDelete_PermintaanKegudang_farmasi').enable();
			Ext.getCmp('cboUnitTujuanKegudang_farmasi').enable();
			Ext.getCmp('txtKeterangan_farmasiL').enable();
			Kegudang_farmasi.form.Grid.a.enable();
    	}
    }
}

function getFormItemEntry_PermintaanKegudang_farmasi(lebar,rowdata){
    var pnlFormDataBasic_PermintaanKegudang_farmasi = new Ext.FormPanel({
		title: '',
		region: 'north',
		layout: 'form',
		bodyStyle: 'padding:10px 10px 10px 10px',
		anchor: '100%',
		labelWidth: 1,
		autoWidth: true,
		width: lebar,
		border: false,
		items:[
				getItemPanelInputPermintaan_PermintaanKegudang_farmasi(lebar),
				getItemGridobat_PermintaanKegudang_farmasi(lebar),
				],
			fileUpload: true,
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAdd_PermintaanKegudang_farmasi',
						handler: function(){
							dataAddNew_PermintaanKegudang_farmasi();
						}
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSave_PermintaanKegudang_farmasi',
						handler: function(){
							if(Ext.getCmp('cboUnitTujuanKegudang_farmasi').getValue() == ""){
								ShowPesanWarningKegudang_farmasi('Unit tujuan harus di isi !','Warning');
							} else{
								Datasave_KasirRWJ();
							}
						}
					},
					{
						xtype: 'button',
						text: 'Print',
						iconCls: 'print',
						id: 'btnPrint_PermintaanKegudang_farmasi',
						handler: function(){
							Print();//dataAddNew_PermintaanKegudang_farmasi();
						}
					},
					{
						xtype: 'tbseparator'
					},
					
				]
			}//,items:
		}
    )

    return pnlFormDataBasic_PermintaanKegudang_farmasi;
}

function getItemPanelInputPermintaan_PermintaanKegudang_farmasi(lebar) {
    var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 100,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 10,
								xtype: 'label',
								text: 'No. Permintaan'
							},
							{
								x: 120,
								y: 10,
								xtype: 'label',
								text: ':'
							},{
								x: 130,
								y: 10,
								xtype: 'textfield',
								id: 'txtNoPermintaanKegudang_farmasiL',
								name: 'txtNoPermintaanKegudang_farmasiL',
								width: 150,
								readOnly:true,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									}
								}
							},
							
							{
								x: 10,
								y: 40,
								xtype: 'label',
								text: 'Tanggal Minta'
							},
							{
								x: 120,
								y: 40,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 40,
								xtype: 'datefield',
								id: 'dfTglMintaKegudang_farmasiL',
								format: 'd/M/Y',
								width: 150,
								tabIndex:2,
								readOnly:true,
								value:now_PermintaanKegudang_farmasi,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									}
								}
							},
							{
								x: 330,
								y: 10,
								xtype: 'label',
								text: 'Unit Tujuan'
							},
							{
								x: 410,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							comboUnitTujuanKegudang_farmasi(),
							{
								x: 330,
								y: 40,
								xtype: 'label',
								text: 'Keterangan'
							},
							{
								x: 410,
								y: 40,
								xtype: 'label',
								text: ':'
							},{
								x: 420,
								y: 40,
								xtype: 'textfield',
								id: 'txtKeterangan_farmasiL',
								name: 'txtKeterangan_farmasiL',
								width: 300,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
												var records = new Array();
												
												records.push(new dsDataGrdobat_PermintaanKegudang_farmasi.recordType());
												dsDataGrdobat_PermintaanKegudang_farmasi.add(records);
												row=dsDataGrdobat_PermintaanKegudang_farmasi.getCount()-1; // menujukan jumlah baris,startEditing(baris dimulai dari 1, kolom 1)
												Kegudang_farmasi.form.Grid.a.startEditing(row, 2);
											
										} 						
									}
								}
							},
						]
					}
				]
			}
		]		
	};
    return items;
};

function getItemGridobat_PermintaanKegudang_farmasi(lebar){
    var items ={
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'margin-top: -1px;',
		border:false,
		width: lebar-80,
		height: 450,//300, 
	    tbar:
		[
			{
				text	: 'Add obat',
				id		: 'btnAddobatKegudang_farmasiL',
				tooltip	: nmLookup,
				disabled: false,
				iconCls	: 'find',
				handler	: function()
				{
					// if(Ext.getCmp('cboUnitTujuanKegudang_farmasi').getValue() == ""){
						// ShowPesanWarningKegudang_farmasi('Unit tujuan harus di isi !','Warning');
					// } else{
						var records = new Array();
						
						records.push(new dsDataGrdobat_PermintaanKegudang_farmasi.recordType());
						dsDataGrdobat_PermintaanKegudang_farmasi.add(records);
						row=dsDataGrdobat_PermintaanKegudang_farmasi.getCount()-1; // menujukan jumlah baris,startEditing(baris dimulai dari 1, kolom 1)
						Kegudang_farmasi.form.Grid.a.startEditing(row, 2);
					// }
				}
			},{
				xtype: 'tbseparator'
			},{
				xtype: 'button',
				text: 'Delete',
				iconCls: 'remove',
				id: 'btnDelete_PermintaanKegudang_farmasi',
				handler: function()
				{
					var line = Kegudang_farmasi.form.Grid.a.getSelectionModel().selection.cell[0];
					var o = dsDataGrdobat_PermintaanKegudang_farmasi.getRange()[line].data;
				
						Ext.Msg.confirm('Warning', 'Apakah obat ini akan dihapus?', function(button){
							if (button == 'yes'){
								if(dsDataGrdobat_PermintaanKegudang_farmasi.getRange()[line].data.kd_prd != undefined){
									Ext.Ajax.request({
										url: baseURL + "index.php/gudang_farmasi/functionPermintaan/delete",
										params:{
											kd_prd:o.kd_prd,
											no_penerimaan: Ext.getCmp('txtNoPermintaanKegudang_farmasiL').getValue(),
										} ,
										failure: function(o)
										{
											ShowPesanErrorKegudang_farmasi('Error, delete obat! Hubungi Admin', 'Error');
										},	
										success: function(o) 
										{
											var cst = Ext.decode(o.responseText);
											if (cst.success === true) 
											{
												dsDataGrdobat_PermintaanKegudang_farmasi.removeAt(line);
												Kegudang_farmasi.form.Grid.a.getView().refresh();
												//Ext.getCmp('btnAddDetKegudang_farmasiKegudang_farmasiL').enable();
												ShowPesanInfoKegudang_farmasi('Berhasil menghapus data obat ini', 'Information');
												
											}
											else 
											{
												ShowPesanErrorKegudang_farmasi('Gagal menghapus data obat ini', 'Error');
											};
										}
									})
								}else{
									dsDataGrdobat_PermintaanKegudang_farmasi.removeAt(line);
									Kegudang_farmasi.form.Grid.a.getView().refresh();
								}
							} 
							
						});
					
					
				}
			}	
		],
		items:[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:[
					gridDataViewEdit_PermintaanKegudang_farmasi()
				]	
			}
		]
	};
    return items;
};


//var a={};
function gridDataViewEdit_PermintaanKegudang_farmasi(){
    var FieldGrdKasir_PermintaanKegudang_farmasi = ['kd_prd'];
    dsDataGrdobat_PermintaanKegudang_farmasi= new WebApp.DataStore({
        fields: FieldGrdKasir_PermintaanKegudang_farmasi
    });
    
    Kegudang_farmasi.form.Grid.a =new Ext.grid.EditorGridPanel({
        store: dsDataGrdobat_PermintaanKegudang_farmasi,
        height: 350,
        columnLines: true,
		selModel: new Ext.grid.CellSelectionModel ({
            singleSelect: true,
            listeners:{
                cellselect: function(sm, row, rec){
					rowSelectedGridobat_PermintaanKegudang_farmasi = undefined;
					rowSelectedGridobat_PermintaanKegudang_farmasi = dsDataGrdobat_PermintaanKegudang_farmasi.getAt(row);
					CurrentDataobat_PermintaanKegudang_farmasi
					CurrentDataobat_PermintaanKegudang_farmasi.row = row;
					CurrentDataobat_PermintaanKegudang_farmasi.data = rowSelectedGridobat_PermintaanKegudang_farmasi.data;
					
                },
            }
        }),
        stripeRows: true,
        cm:new Ext.grid.ColumnModel([
        	new Ext.grid.RowNumberer(),	
			{
				dataIndex: 'kd_prd',
				header: 'Kode Prd',
				sortable: true,
				width: 70
			},
			{			
				dataIndex: 'nama_obat',
				header: 'Nama obat',
				sortable: true,
				width: 250,
				editor:new Nci.form.Combobox.autoComplete({
					store	: Kegudang_farmasi.form.ArrayStore.obat,
					select	: function(a,b,c){
						
						var line	= Kegudang_farmasi.form.Grid.a.getSelectionModel().selection.cell[0];
						if(dsDataGrdobat_PermintaanKegudang_farmasi.getCount()-1===0)
						{
							dsDataGrdobat_PermintaanKegudang_farmasi.getRange()[line].data.kd_prd=b.data.kd_prd;
							dsDataGrdobat_PermintaanKegudang_farmasi.getRange()[line].data.qty=0;
							dsDataGrdobat_PermintaanKegudang_farmasi.getRange()[line].data.nama_obat=b.data.nama_obat;
							dsDataGrdobat_PermintaanKegudang_farmasi.getRange()[line].data.kd_milik=b.data.kd_milik;
							dsDataGrdobat_PermintaanKegudang_farmasi.getRange()[line].data.status_app='f';
							dsDataGrdobat_PermintaanKegudang_farmasi.getRange()[line].data.kd_sat_besar=b.data.kd_sat_besar;
							dsDataGrdobat_PermintaanKegudang_farmasi.getRange()[line].data.fractions=b.data.fractions;
						}else{
							var VALUE_x=1;
							for(var i = 0 ; i < dsDataGrdobat_PermintaanKegudang_farmasi.getCount();i++)
							{
								if(dsDataGrdobat_PermintaanKegudang_farmasi.getRange()[i].data.kd_prd===b.data.kd_prd)
								{
									VALUE_x=0;
								}
							}
							if(VALUE_x===0){
								ShowPesanWarningKegudang_farmasi('Anda telah memilih Obat yang sama', 'Perhatian');
								dsDataGrdobat_PermintaanKegudang_farmasi.removeAt(line);
							}else{
								dsDataGrdobat_PermintaanKegudang_farmasi.getRange()[line].data.kd_prd=b.data.kd_prd;
								dsDataGrdobat_PermintaanKegudang_farmasi.getRange()[line].data.qty=0;
								dsDataGrdobat_PermintaanKegudang_farmasi.getRange()[line].data.nama_obat=b.data.nama_obat;
								dsDataGrdobat_PermintaanKegudang_farmasi.getRange()[line].data.kd_milik=b.data.kd_milik;
								dsDataGrdobat_PermintaanKegudang_farmasi.getRange()[line].data.status_app='f';
								dsDataGrdobat_PermintaanKegudang_farmasi.getRange()[line].data.kd_sat_besar=b.data.kd_sat_besar;
								dsDataGrdobat_PermintaanKegudang_farmasi.getRange()[line].data.fractions=b.data.fractions;
							}
						}
						Kegudang_farmasi.form.Grid.a.getView().refresh();	
						Kegudang_farmasi.form.Grid.a.startEditing(line, 4);
					},
					insert	: function(o){
						return {
							jml_stok_apt	: o.jml_stok_apt,
							nama_obat		: o.nama_obat,
							kd_prd			: o.kd_prd,
							qty_b			: o.qty_b,
							nama_unit		: o.nama_unit,
							kd_sat_besar	: o.kd_sat_besar,
							fractions		: o.fractions,
							kd_milik		: o.kd_milik,
							text			:  '<table style="font-size: 11px;"><tr><td width="250">'+o.nama_obat+'</td><td width="120">'+o.nama_unit+'</td><td width="50">'+o.jml_stok_apt+'</td></tr></table>',
							
						}
					},
					param:function(){
						return {
							kd_unit_far:Ext.getCmp('cboUnitTujuanKegudang_farmasi').getValue()
						}
					},
					url		: baseURL + "index.php/gudang_farmasi/functionPermintaan/getstock_obat",
					valueField: 'nama_obat',
					displayField: 'text',
					listWidth: 400
				})
			},
			{
				dataIndex: 'qty_ordered',
				header: 'Diberikan',
				sortable: true,
				width: 50,
			},
			{
				dataIndex: 'kd_sat_besar',
				header: 'Sat B',
				sortable: true,
				width: 65,
			},
			{
				dataIndex: 'qty_b',
				header: 'Qty B',
				sortable: true,
				width: 50,
				align:'right',
				editor: new Ext.form.NumberField({
					listeners:{
						blur:function(a){
							var line	= Kegudang_farmasi.form.Grid.a.getSelectionModel().selection.cell[0];
							var o = dsDataGrdobat_PermintaanKegudang_farmasi.getRange()[line].data;
							if (parseFloat(a.getValue()) > (parseFloat(o.jml_stok_apt)/parseFloat(o.fractions))){
								ShowPesanWarningKegudang_farmasi('Qty tidak cukup!','Warning');
								o.qty_b=parseFloat(o.jml_stok_apt)/parseFloat(o.fractions);
							}else {
								o.qty_b=a.getValue();
							}
							o.qty=a.getValue()*o.fractions;
							Kegudang_farmasi.form.Grid.a.getView().refresh();	
							var row=this.indeks;
							Kegudang_farmasi.form.Grid.a.startEditing(row,6);
						},
						focus:function(){
							this.indeks=Kegudang_farmasi.form.Grid.a.getSelectionModel().selection.cell[0];
						},
						specialkey: function(){
							if(Ext.EventObject.getKey() == 13){
								var nextRow = dsDataGrdobat_PermintaanKegudang_farmasi.getCount()-1; 
								Kegudang_farmasi.form.Grid.a.startEditing(nextRow, 6);
							}
						},
					}
                })
			},
			{
				dataIndex: 'fractions',
				header: 'Frac',
				sortable: true,
				width: 50,
				align:'right',
				// hidden:true
			},
			{
				dataIndex: 'qty',
				header: 'Qty K',
				sortable: true,
				width: 50,
				align:'right',
				editor: new Ext.form.NumberField({
                    allowBlank: true,
                    enableKeyEvents : true,
                    width:30,
					listeners:{
						blur:function(a){
							var line	= Kegudang_farmasi.form.Grid.a.getSelectionModel().selection.cell[0];
							var o = dsDataGrdobat_PermintaanKegudang_farmasi.getRange()[line].data;
							if (parseFloat(a.getValue()) > parseFloat(o.jml_stok_apt)){
								ShowPesanWarningKegudang_farmasi('Qty tidak cukup!','Warning');
								o.qty_b=parseFloat(o.jml_stok_apt)/parseFloat(o.fractions);
								o.qty=o.jml_stok_apt;
							}else {
								o.qty_b=parseFloat(a.getValue())/parseFloat(o.fractions);
								o.qty=a.getValue();
							}
							Kegudang_farmasi.form.Grid.a.getView().refresh();	
							var row=this.indeks;
							Kegudang_farmasi.form.Grid.a.startEditing(row,7);
						},
						focus:function(){
							this.indeks=Kegudang_farmasi.form.Grid.a.getSelectionModel().selection.cell[0];
						},
						specialkey: function(){
							if(Ext.EventObject.getKey() == 13){
								var nextRow = dsDataGrdobat_PermintaanKegudang_farmasi.getCount()-1; 
								Kegudang_farmasi.form.Grid.a.startEditing(nextRow, 7);
								
							}
						},
					}
                })
			},
			{
				dataIndex: 'keterangan_app',
				header: 'Keterangan',
				width: 150,
				editor: new Ext.form.TextField({
					allowBlank: false,
					listeners:{
						specialkey: function(){
							if(Ext.EventObject.getKey() == 13){
								var records = new Array();
								records.push(new dsDataGrdobat_PermintaanKegudang_farmasi.recordType());
								dsDataGrdobat_PermintaanKegudang_farmasi.add(records);
								var nextRow = dsDataGrdobat_PermintaanKegudang_farmasi.getCount()-1; 
								Kegudang_farmasi.form.Grid.a.startEditing(nextRow, 2);
							}
						}
					}
				})
			},
			{
				header		: 'Status',
				width		: 40,
				sortable	: false,
				hideable	: true,
				hidden		: false,
				menuDisabled: true,
				dataIndex	: 'status_app',
				renderer	: function(value, metaData, record, rowIndex, colIndex, store){
					 switch (value){
						 case 't':
							 metaData.css = 'StatusHijau'; 
							 break;
						 case 'f':
							 metaData.css = 'StatusMerah';
							 break;
					 }
					 return '';
				}
			},
			{
				dataIndex: 'kd_milik',
				header: 'kd_milik',
				sortable: true,
				width: 65,
				align:'center',
				hidden:true
			},
			{
				dataIndex: 'jml_stok_apt',
				header: 'jml_stok_apt',
				sortable: true,
				width: 65,
				align:'center',
				hidden:true
			}
        ]),
		viewConfig:{
			forceFit: true
		}
    });
    return Kegudang_farmasi.form.Grid.a;
}


function datainit_PermintaanKegudang_farmasi(rowdata)
{
	//var tgl_ro2 = rowdata.tgl_ro.split(" ");
	//alert(ShowDate(rowdata.tgl_ro));
	Ext.getCmp('txtNoPermintaanKegudang_farmasiL').setValue(rowdata.no_ro_unit);
	Ext.getCmp('dfTglMintaKegudang_farmasiL').setValue(ShowDate(rowdata.tgl_ro));
	Ext.getCmp('cboUnitTujuanKegudang_farmasi').setValue(rowdata.nm_unit_far_tujuan);
	currentKdUnitFarTujuanGF=rowdata.kd_unit_far_tujuan;
	
	getGrid_detail_min(rowdata.no_ro_unit);
};

function getGrid_detail_min(no_ro_unit){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gudang_farmasi/functionPermintaan/getDataGrid_detail",
			params: {nominta:no_ro_unit},
			failure: function(o)
			{
				ShowPesanErrorKegudang_farmasi('Error, obat! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dsDataGrdobat_PermintaanKegudang_farmasi.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
					dsDataGrdobat_PermintaanKegudang_farmasi.add(recs);
					
					Kegudang_farmasi.form.Grid.a.getView().refresh();
				}
				else 
				{
					ShowPesanErrorKegudang_farmasi('Gagal membaca data obat ini', 'Error');
				};
			}
		}
		
	)
	
}

function dataGriAwal(nominta,tgl_ro,tgl_ro2){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gudang_farmasi/functionPermintaan/getDataGridAwal",
			params: {
				no_minta:nominta,
				tgl_ro:tgl_ro,
				tgl_ro2:tgl_ro2
			
			},
			failure: function(o)
			{
				ShowPesanErrorKegudang_farmasi('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dataSource_PermintaanKegudang_farmasi.removeAll();
					var recs=[],
						recType=dataSource_PermintaanKegudang_farmasi.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
						dataSource_PermintaanKegudang_farmasi.add(recs);
					
					
					
					GridDataView_PermintaanKegudang_farmasi.getView().refresh();
				}
				else 
				{
					ShowPesanErrorKegudang_farmasi('Gagal membaca data obat', 'Error');
				};
			}
		}
		
	)
	
}

function ValidasiEntryPermintaanKegudang_farmasi(modul,mBolHapus)
{
	var x = 1;
	/* if(Ext.getCmp('cbo_UnitKegudang_farmasiLookup').getValue() === '' || Ext.getCmp('cbo_AhliGiziKegudang_farmasiLookup').getValue() === '' ||(Ext.getCmp('dfTglUntukKegudang_farmasiL').getValue() < Ext.getCmp('dfTglMintaKegudang_farmasiL').getValue()) ){
		if(Ext.getCmp('cbo_UnitKegudang_farmasiLookup').getValue() === ''){
			loadMask.hide();
			ShowPesanWarningKegudang_farmasi('Unit tidak boleh kosong', 'Warning');
			x = 0;
		} else if(Ext.getCmp('cbo_AhliGiziKegudang_farmasiLookup').getValue() === ''){
			loadMask.hide();
			ShowPesanWarningKegudang_farmasi('Ahli gizi tidak boleh kosong', 'Warning');
			x = 0;
		} else {
			loadMask.hide();
			ShowPesanWarningKegudang_farmasi('Untuk tanggal tidak boleh kurang dari tanggal permintaan', 'Warning');
			x = 0;
		} 
	} */
	return x;
};

function dataAddNew_PermintaanKegudang_farmasi(){
	Ext.getCmp('txtNoPermintaanKegudang_farmasiL').setValue('');
	Ext.getCmp('dfTglMintaKegudang_farmasiL').setValue(now_PermintaanKegudang_farmasi);
	Ext.getCmp('cboUnitTujuanKegudang_farmasi').setValue('');
	dsDataGrdobat_PermintaanKegudang_farmasi.removeAll();
	
	Ext.getCmp('txtNoPermintaanKegudang_farmasiL').enable();
	Ext.getCmp('dfTglMintaKegudang_farmasiL').enable();
	
}


function ShowPesanWarningKegudang_farmasi(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:290
		}
	);
};

function ShowPesanErrorKegudang_farmasi(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:290
		}
	);
};


function ShowPesanInfoKegudang_farmasi(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:290
		}
	);
};

function validasiSavePermintaan(){
		//var $this=this;
		var x = 1;
		if(dsDataGrdobat_PermintaanKegudang_farmasi.getCount() < 1){
			Ext.Msg.alert('Warning','Obat tidak boleh kosong!');
			x=0;
		}
		if(Ext.getCmp('cboUnitTujuanKegudang_farmasi').getValue() < 1){
			Ext.Msg.alert('Warning','Unit tujuan masih kosong!');
			x=0;
		}
		for(var i = 0 ; i < dsDataGrdobat_PermintaanKegudang_farmasi.getCount();i++)
		{
			var o=dsDataGrdobat_PermintaanKegudang_farmasi.getRange()[i].data;
			for(var j = i+1 ; j < dsDataGrdobat_PermintaanKegudang_farmasi.getCount();j++)
			{
				var p=dsDataGrdobat_PermintaanKegudang_farmasi.getRange()[j].data;
				if (p.kd_prd==o.kd_prd)
				{
					Ext.Msg.alert('Warning','Obat ada yang sama');
					x = 0;
				}
			}
			
		}
		return x;
	}
function Datasave_KasirRWJ(mBol){	
	if (validasiSavePermintaan()==1)	
			{
				Ext.Ajax.request({
					url			: baseURL + "index.php/gudang_farmasi/functionPermintaan/insert_ro_unit",
					params		: getParamSavePermintaan(),
					failure		: function(o){
					dataGriAwal(Ext.getCmp('TxtFilter_PermintaanKegudang_farmasi').getValue(),
											Ext.getCmp('dateFilter_PermintaanKegudang_farmasi').getValue(),
											Ext.getCmp('dateFilter2_PermintaanKegudang_farmasi').getValue()
											);
					ShowPesanErrorKegudang_farmasi('Insert error hubungi Admin Admin', 'Error');
					},
					success		: function(o){
					dataGriAwal(Ext.getCmp('TxtFilter_PermintaanKegudang_farmasi').getValue(),
											Ext.getCmp('dateFilter_PermintaanKegudang_farmasi').getValue(),
											Ext.getCmp('dateFilter2_PermintaanKegudang_farmasi').getValue()
											);
						var cst = Ext.decode(o.responseText);
						if(cst.login_error===true){
						ShowPesanWarningKegudang_farmasi('durasi login Habis Harap login ulang jika tidak bisa hubungi admin', 'Perhatian');
						}if(cst.simpan===false){
						ShowPesanWarningKegudang_farmasi('ro unit gagal dimasukan ke database', 'Perhatian');
						}
						if(cst.simpan_det===false){
						ShowPesanWarningKegudang_farmasi('ro unit detail gagal dimasukan ke database', 'Perhatian');
						}if(cst.simpan_det===true){
						ShowPesanInfoKegudang_farmasi('data berhasil disimpan', 'Simpan');
						Ext.getCmp('txtNoPermintaanKegudang_farmasiL').setValue(cst.no_minta);
						}
					}
					
				});
			}
}

function getParamSavePermintaan(){
	var params={
		no_minta 	 		 :Ext.getCmp('txtNoPermintaanKegudang_farmasiL').getValue(),
		tgl_minta 			 :Ext.getCmp('dfTglMintaKegudang_farmasiL').getValue(),
		keterangan 			 :Ext.getCmp('txtKeterangan_farmasiL').getValue(),
		kd_unit_far_tujuan 	 :currentKdUnitFarTujuanGF,
		jumlah_detil :dsDataGrdobat_PermintaanKegudang_farmasi.getCount(),
	};
					
	
	for(var i=0, iLen= dsDataGrdobat_PermintaanKegudang_farmasi.getCount(); i<iLen;i++){
    	var o=dsDataGrdobat_PermintaanKegudang_farmasi.getRange()[i].data;
    	params['kd_prd'+i]	= o.kd_prd;
		params['qty'+i]	= o.qty;
    	params['kd_milik'+i]	= o.kd_milik;
    	params['keterangan_app'+i]	= o.keterangan_app;
    }
    return params;
}

function comboUnitTujuanKegudang_farmasi()
{
 var Field = ['kd_unit_far_tujuan','nm_unit_far_tujuan'];
    dscomboUnitTujuanKegudang_farmasi = new WebApp.DataStore({ fields: Field });
	loaddatacomboUnitTujuanKegudang_farmasi();
	cboUnitTujuanKegudang_farmasi= new Ext.form.ComboBox
	(
		{
			id				: 'cboUnitTujuanKegudang_farmasi',
			x				: 420,
			y				: 10,
			typeAhead		: true,
			triggerAction	: 'all',
			lazyRender		: true,
			mode			: 'local',
			align			: 'Right',
			width			: 200,
			store			: dscomboUnitTujuanKegudang_farmasi,
			valueField		: 'kd_unit_far_tujuan',
			displayField	: 'nm_unit_far_tujuan',
			emptyText		: 'SEMUA',
			listeners		:
			{
				select	: function(a,b,c){
					currentKdUnitFarTujuanGF=b.data.kd_unit_far_tujuan;
				},
				keyUp: function(a,b,c){
					$this1=this;
					if((b.getKey()>=65 && b.getKey()<=90)|| (b.getKey()>=48 && b.getKey()<=57) || b.getKey()==32 || b.getKey()==8 || b.getKey()==46){
						clearTimeout(this.time);

						this.time=setTimeout(function(){
							if($this1.lastQuery != '' ){
								var value=$this1.lastQuery;
								var param={};
								param['text']=$this1.lastQuery;
								loaddatacomboUnitTujuanKegudang_farmasi(param);
							}
						},1000);
					}
				}
			}
		}
	);return cboUnitTujuanKegudang_farmasi;
};

function loaddatacomboUnitTujuanKegudang_farmasi(param)
{
	if (param==='' || param===undefined)
	{
		param={
			text: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/gudang_farmasi/functionPermintaan/getUnitTujuan",
		params: param,
		failure: function(o)
		{
			var cst = Ext.decode(o.responseText);
		},
		success: function(o) {
			cboUnitTujuanKegudang_farmasi.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dscomboUnitTujuanKegudang_farmasi.recordType;
				var o=cst['listData'][i];

				recs.push(new recType(o));
				dscomboUnitTujuanKegudang_farmasi.add(recs);
				//console.log(o);
			}
		}
	});
}
function Print(){
	Ext.Ajax.request({
		url: baseURL + "index.php/gudang_farmasi/cetakan_faktur_bill/CetakBuktiPermintaanUnit",
		params:{ 
			no_minta:Ext.getCmp('txtNoPermintaanKegudang_farmasiL').getValue(),
			unit_tujuan:Ext.getCmp('cboUnitTujuanKegudang_farmasi').getValue(),
		} ,
		failure: function(o)
		{
			ShowPesanErrorKegudang_farmasi('Error, Cetak! Hubungi Admin', 'Error');
		},	
		success: function(o) 
		{
			var cst = Ext.decode(o.responseText);
			if (cst.success === true) 
			{
				ShowPesanInfoKegudang_farmasi('Bukti sedang dicetak!', 'Information');
			}else{
				ShowPesanWarningKegudang_farmasi('Bukti gagal dicetak!','Warning');
			}
		}
	})
}