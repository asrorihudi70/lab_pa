var GridDataView_Permintaanscreen_farmasi;
var GFStatusPostingPengeluaranUnit='0';
var currentRowSelectionPencarianObatGFPengeluaran;
var PencarianLookupGFPengeluaran; // Variabel pembeda getdata u/ pencarian nama obat dan semua kepemilikan atau bukan. False => pencarian berdasarkan nama_obat dan kepemilikan, True => pencarian berdasarkan nama_obat saja
var FocusExitGFPengeluaran=false;
var tanggal = new Date();
tanggal = tanggal.format("Y-m-d");
var PengeluaranGF={
	vars:{
		kdUnit: null,
		lineDetail: null
	},
	form:{
		ArrayStore:{
			posting:new Ext.data.ArrayStore({id: 0,fields:['Id','displayText'],data: [[1,'Semua'],[2,'Posting'],[3,'Belum Posting']]}),
			detail:new Ext.data.ArrayStore({id: 0,fields:['nama_obat','kd_prd','kd_sat_besar','fractions','harga_beli','jml_stok_apt','qty','sisa','jumlah','no_stok_out'],data: []}),
			obat:new Ext.data.ArrayStore({id: 0,fields:[],data: []}),
			main:new Ext.data.ArrayStore({id: 0,fields:['post_out','no_stok_out','tgl_stok_out','nm_unit_far'],data: []})
		},
		DataStore:{
			unit:null
		},
		DateField:{
			stockOut: null
		},
		Grid:{
			detail:null,
			main: null
		},
		Panel:{
			posting: null,
			main: null
		},
		TextField:{
			total: null,
			remark: null,
			no: null
		},
		Button:{
			posting: null,
			unposting:null
		},
		ComboBox:{
			unit: null
		},
		Mask:{
			input: null
		}
	},
	func:{
		parent: PengeluaranGF,
		addItems:function(){
			if(GFStatusPostingPengeluaranUnit=='0'){
				var records = new Array();
				records.push(new PengeluaranGF.form.ArrayStore.detail.recordType());
				PengeluaranGF.form.ArrayStore.detail.add(records);
				PengeluaranGF.func.refreshCounting();
				
				PengeluaranGF.vars.lineDetail=PengeluaranGF.form.ArrayStore.detail.getCount()-1;
				PengeluaranGF.form.Grid.detail.startEditing(PengeluaranGF.vars.lineDetail,3);
			}else{
				Ext.Msg.alert('Informasi','Penyimpanan gagal karena data sudah diPosting, Harap Unposting jika ingin Menyimpan.');
			}
		},
		resetDetail: function(a,b){
			PengeluaranGF.vars.kdUnit=b.data.KODE;
		},
		refreshCounting: function(){
			var o=PengeluaranGF.form.ArrayStore.detail;
			var jum=0;
			for(var i=0; i<o.getCount() ; i++){
				if(o.getRange()[i].data.qty_b == undefined || (o.getRange()[i].data.qty_b != undefined && o.getRange()[i].data.qty_b==''))o.getRange()[i].data.qty_b=0;
				if(o.getRange()[i].data.qty == undefined || (o.getRange()[i].data.qty != undefined && o.getRange()[i].data.qty==''))o.getRange()[i].data.qty=0;
				if(o.getRange()[i].data.jml_stok_apt == undefined || (o.getRange()[i].data.jml_stok_apt != undefined && o.getRange()[i].data.jml_stok_apt==''))o.getRange()[i].data.jml_stok_apt=0;
				if(o.getRange()[i].data.harga_beli == undefined || (o.getRange()[i].data.harga_beli != undefined && o.getRange()[i].data.harga_beli==''))o.getRange()[i].data.harga_beli=0;
				// if(parseInt(o.getRange()[i].data.jml_stok_apt)<parseInt(o.getRange()[i].data.qty)){
					// o.getRange()[i].data.qty=parseInt(o.getRange()[i].data.jml_stok_apt);
				// }
				o.getRange()[i].data.sisa=parseInt(o.getRange()[i].data.jml_stok_apt)-parseInt(o.getRange()[i].data.qty);
				o.getRange()[i].data.jumlah=parseInt(o.getRange()[i].data.harga_beli)* parseInt(o.getRange()[i].data.qty);
				jum+=parseInt(o.getRange()[i].data.jumlah);
			}
			PengeluaranGF.form.TextField.total.setValue(toFormat(jum));
			PengeluaranGF.form.Grid.detail.getView().refresh();
		},
		getParamsDetail: function(){
			var a=[];
			if(PengeluaranGF.vars.kdUnit !=null){
				a.push({name: 'kd_unit_far',value:PengeluaranGF.vars.kdUnit});
			}else{
				Ext.Msg.alert('Error','Harap Pilih Unit.');
				return;
			}
			if(PengeluaranGF.form.TextField.remark.getValue() !=''){
				a.push({name: 'remark',value:PengeluaranGF.form.TextField.remark.getValue()});
			}else{
				Ext.Msg.alert('Error','Harap Isi Remark.');
				return;
			}
			if(PengeluaranGF.form.ArrayStore.detail.getCount()==0){
				Ext.Msg.alert('Error','Detail Tidak Boleh Kosong.');
				return;
			}
			a.push({name: 'tgl_stok_out',value:PengeluaranGF.form.DateField.stockOut.value});
			a.push({name: 'total_out',value:toInteger(PengeluaranGF.form.TextField.total.getValue())});
			a.push({name: 'no_stok_out',value:PengeluaranGF.form.TextField.no.getValue()});
			for(var i=0;i<PengeluaranGF.form.ArrayStore.detail.getCount() ; i++){
				var o=PengeluaranGF.form.ArrayStore.detail.getRange()[i].data;
				if(o.kd_prd != undefined && o.kd_prd != ''){
					a.push({name: 'kd_prd[]',value:o.kd_prd});
					a.push({name: 'jml_out[]',value:o.qty});
					// if(o.qty !=0){
						// a.push({name: 'jml_out[]',value:o.qty});
					// }else{
						// Ext.Msg.alert('Error','Harap Isi Quantity.');
						// return;
					// }
					
					a.push({name: 'harga_beli[]',value:o.harga_beli});
					a.push({name: 'no_minta[]',value:o.no_minta});
					a.push({name: 'out_line[]',value:o.out_line});
				}else{
					PengeluaranGF.form.ArrayStore.detail.removeAt(i);
				}
				
			}
			return a;
		},save: function(callback){
			if(PengeluaranGF.func.getParamsDetail() != undefined){
				if(PengeluaranGF.form.TextField.no.getValue()==''){
					loadMask.show();
					$.ajax({
						url:baseURL + "index.php/gudang_farmasi/functionGFPengeluaran/save",
						dataType:'JSON',
						type: 'POST',
						data:PengeluaranGF.func.getParamsDetail(),
						success: function(r){
							loadMask.hide();
							if(r.processResult=='SUCCESS'){
								PengeluaranGF.form.TextField.no.setValue(r.resultObject.code);
								for(var i=0; i<PengeluaranGF.form.ArrayStore.detail.getCount() ; i++){
									var o=PengeluaranGF.form.ArrayStore.detail.getRange()[i].data;
									o.no_stok_out=r.resultObject.code;
								}
								dataGriAwal_ordr();
								PengeluaranGF.func.refreshData();
								Ext.Msg.alert('Sukses','Data Berhasil Disimpan.');
								
								PengeluaranGF.form.ArrayStore.detail.loadData([],false);
								var recs = [],
								recordType=PengeluaranGF.form.ArrayStore.detail.recordType;
								for(var i=0, iLen=r.listData.length ; i<iLen ; i++){
									recs.push(new recordType(r.listData[i]));
								}
								PengeluaranGF.form.ArrayStore.detail.add(recs);
								PengeluaranGF.form.Grid.detail.getView().refresh();
								PengeluaranGF.func.refreshCounting();
								
								if(callback != undefined){
									callback();
								}
								Ext.getCmp('btnAddGFPengeluaranKeUnit').enable();
								Ext.getCmp('btnSimpanGFPengeluaranKeUnit').enable();
								Ext.getCmp('btnSimpanExit_viApotekPengeluaranKeUnit').enable();
								Ext.getCmp('btnDeleteGFPengeluaranKeUnit').enable();
								Ext.getCmp('btnPostingGFPengeluaranKeUnit').enable();
								Ext.getCmp('btnCetakBuktiGFPengeluaranKeUnit').disable();
								Ext.getCmp('btnUnPostingGFPengeluaranKeUnit').disable();
							}else{
								Ext.Msg.alert('Gagal',r.processMessage);
							}
						},
						error: function(jqXHR, exception) {
							loadMask.hide();
							Nci.ajax.ErrorMessage(jqXHR, exception);
						}
					});
				}else{
					loadMask.show();
					$.ajax({
						url:baseURL + "index.php/gudang_farmasi/functionGFPengeluaran/update",
						dataType:'JSON',
						type: 'POST',
						data:PengeluaranGF.func.getParamsDetail(),
						success: function(r){
							loadMask.hide();
							if(r.processResult=='SUCCESS'){
								Ext.Msg.alert('Sukses','Data Berhasi Diubah.');
								PengeluaranGF.form.ArrayStore.detail.loadData([],false);
								var recs = [],
								recordType=PengeluaranGF.form.ArrayStore.detail.recordType;
								for(var i=0, iLen=r.listData.length ; i<iLen ; i++){
									recs.push(new recordType(r.listData[i]));
								}
								PengeluaranGF.form.ArrayStore.detail.add(recs);
								PengeluaranGF.form.Grid.detail.getView().refresh();
								PengeluaranGF.func.refreshCounting();
								for(var i=0; i<PengeluaranGF.form.ArrayStore.detail.getCount() ; i++){
									var o=PengeluaranGF.form.ArrayStore.detail.getRange()[i].data;
									o.no_stok_out=PengeluaranGF.form.TextField.no.getValue();
								}
								if(callback != undefined){
									callback();
								}
								Ext.getCmp('btnAddGFPengeluaranKeUnit').enable();
								Ext.getCmp('btnSimpanGFPengeluaranKeUnit').enable();
								Ext.getCmp('btnSimpanExit_viApotekPengeluaranKeUnit').enable();
								Ext.getCmp('btnDeleteGFPengeluaranKeUnit').enable();
								Ext.getCmp('btnPostingGFPengeluaranKeUnit').enable();
								Ext.getCmp('btnCetakBuktiGFPengeluaranKeUnit').disable();
								Ext.getCmp('btnUnPostingGFPengeluaranKeUnit').disable();
							}else{
								Ext.Msg.alert('Gagal',r.processMessage);
							}
						},
						error: function(jqXHR, exception) {
							loadMask.hide();
							Nci.ajax.ErrorMessage(jqXHR, exception);
						}
					});
				}
			} 
		},posting: function(){
			if(PengeluaranGF.func.getParamsDetail() != undefined){
				if(PengeluaranGF.form.TextField.no.getValue()!=''){
					Ext.Msg.confirm('Konfirmasi', 'Apakah Akan diPosting ?', function (id, value) { 
						if (id === 'yes') { 
							// if(PengeluaranGF.form.TextField.no.getValue()==''){
								// loadMask.show();
								// $.ajax({
									// url:baseURL + "index.php/gudang_farmasi/functionGFPengeluaran/postingSave",
									// dataType:'JSON',
									// type: 'POST',
									// data:PengeluaranGF.func.getParamsDetail(),
									// success: function(r){
										// loadMask.hide();
										// if(r.processResult=='SUCCESS'){
											// PengeluaranGF.form.TextField.no.setValue(r.resultObject.code);
											// for(var i=0; i<PengeluaranGF.form.ArrayStore.detail.getCount() ; i++){
												// var o=PengeluaranGF.form.ArrayStore.detail.getRange()[i].data;
												// o.no_stok_out=r.resultObject.code;
											// }
											// PengeluaranGF.form.Panel.posting.setValue('<img src="'+baseURL+'ui/images/icons/16x16/hijau.png"/>');
											// Ext.Msg.alert('Sukses','Data Berhasi Disimpan dan diPosting.');
											// GFStatusPostingPengeluaranUnit='1';
										// }else{
											// Ext.Msg.alert('Gagal',r.processMessage);
										// }
									// },
									// error: function(jqXHR, exception) {
										// loadMask.hide();
										// Nci.ajax.ErrorMessage(jqXHR, exception);
									// }
								// });
							// }else{
								loadMask.show();
								$.ajax({
									url:baseURL + "index.php/gudang_farmasi/functionGFPengeluaran/postingUpdate",
									dataType:'JSON',
									type: 'POST',
									data:PengeluaranGF.func.getParamsDetail(),
									success: function(r){
										loadMask.hide();
										if(r.processResult=='SUCCESS'){
											Ext.Msg.alert('Sukses','Data Berhasi Diubah dan diPosting.');
											GFStatusPostingPengeluaranUnit='1';
											for(var i=0; i<PengeluaranGF.form.ArrayStore.detail.getCount() ; i++){
												var o=PengeluaranGF.form.ArrayStore.detail.getRange()[i].data;
												o.no_stok_out=PengeluaranGF.form.TextField.no.getValue();
											}
											PengeluaranGF.form.Panel.posting.setValue('<img src="'+baseURL+'ui/images/icons/16x16/hijau.png"/>');
											Ext.getCmp('btnAddGFPengeluaranKeUnit').disable();
											Ext.getCmp('btnSimpanGFPengeluaranKeUnit').disable();
											Ext.getCmp('btnSimpanExit_viApotekPengeluaranKeUnit').disable();
											Ext.getCmp('btnDeleteGFPengeluaranKeUnit').disable();
											Ext.getCmp('btnPostingGFPengeluaranKeUnit').disable();
											Ext.getCmp('btnCetakBuktiGFPengeluaranKeUnit').enable();
											Ext.getCmp('btnUnPostingGFPengeluaranKeUnit').enable();
											PengeluaranGF.func.initListRefresh(r.listData);
											dataGriAwal_ordr();
										}else{
											Ext.Msg.alert('Gagal',r.processMessage);
										}
									},
									error: function(jqXHR, exception) {
										loadMask.hide();
										Nci.ajax.ErrorMessage(jqXHR, exception);
									}
								});
							// }
						} 
					}, this); 
				} else{
					/*if(GFStatusPostingPengeluaranUnit=='1'){					
						Ext.Msg.alert('WARNING','Transaksi ini sudah diPosting!');
					} else */
					if(PengeluaranGF.form.TextField.no.getValue()==''){
						Ext.Msg.alert('WARNING','Simpan terlebih dahulu sebelum diPosting!');
					}
				}
			}
		},refreshData: function(){
			loadMask.show();
			$.ajax({
				type: 'POST',
				dataType:'JSON',
				url:baseURL + "index.php/gudang_farmasi/functionGFPengeluaran/initList",
				data:$('#'+PengeluaranGF.form.Panel.main.id).find('form').eq(0).serialize(),
				success: function(r){
					loadMask.hide();
					if(r.processResult=='SUCCESS'){
						PengeluaranGF.form.ArrayStore.main.loadData([],false);
						for(var i=0,iLen=r.listData.length; i<iLen ;i++){
							var records=[];
							records.push(new PengeluaranGF.form.ArrayStore.main.recordType());
							PengeluaranGF.form.ArrayStore.main.add(records);
							PengeluaranGF.form.ArrayStore.main.data.items[i].data.post_out=r.listData[i].post_out
							PengeluaranGF.form.ArrayStore.main.data.items[i].data.no_stok_out=r.listData[i].no_stok_out
							PengeluaranGF.form.ArrayStore.main.data.items[i].data.tgl_stok_out=r.listData[i].tgl_stok_out
							PengeluaranGF.form.ArrayStore.main.data.items[i].data.nm_unit_far=r.listData[i].nm_unit_far
							PengeluaranGF.form.ArrayStore.main.data.items[i].data.milik=r.listData[i].milik
						}
						PengeluaranGF.form.Grid.main.getView().refresh();
					}else{
						Ext.Msg.alert('Gagal',r.processMessage);
					}
				},
				error: function(jqXHR, exception) {
					loadMask.hide();
					Nci.ajax.ErrorMessage(jqXHR, exception);
				}
			});
		},refreshDataDetail: function(){
			// loadMask.show();
			var line	= PengeluaranGF.form.Grid.detail.getSelectionModel().selection.cell[0];
			var o=PengeluaranGF.form.ArrayStore.detail.getRange()[line].data;
			Ext.Ajax.request({
				url: baseURL + "index.php/gudang_farmasi/functionGFPengeluaran/initListDetail",
				params: {
					no_stok_out:o.no_stok_out
				},
				failure: function(o)
				{
					Ext.Msg.alert('Gagal','Error refresh data. Hubungi Admin!');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						PengeluaranGF.form.ArrayStore.detail.loadData([],false);
						var recs = [],
						recordType=PengeluaranGF.form.ArrayStore.detail.recordType;
						for(var i=0, iLen=r.listData.length ; i<iLen ; i++){
							recs.push(new recordType(r.listData[i]));
						}
						PengeluaranGF.form.ArrayStore.detail.add(recs);
						PengeluaranGF.form.Grid.detail.getView().refresh();
						PengeluaranGF.func.refreshCounting();
					}
					else 
					{
						Ext.Msg.alert('Gagal',r.processMessage);
					};
				}
			})
		},initListRefresh:function(listData){
			PengeluaranGF.form.ArrayStore.main.loadData([],false);
			for(var i=0,iLen=listData.length; i<iLen ;i++){
				var records=[];
				records.push(new PengeluaranGF.form.ArrayStore.main.recordType());
				PengeluaranGF.form.ArrayStore.main.add(records);
				PengeluaranGF.form.ArrayStore.main.data.items[i].data.post_out=listData[i].post_out
				PengeluaranGF.form.ArrayStore.main.data.items[i].data.no_stok_out=listData[i].no_stok_out
				PengeluaranGF.form.ArrayStore.main.data.items[i].data.tgl_stok_out=listData[i].tgl_stok_out
				PengeluaranGF.form.ArrayStore.main.data.items[i].data.nm_unit_far=listData[i].nm_unit_far
				PengeluaranGF.form.ArrayStore.main.data.items[i].data.milik=listData[i].milik
			}
			PengeluaranGF.form.Grid.main.getView().refresh();
		}
	}
};
var dataSource_viApotekPengeluaranKeUnit;
var selectCount_viApotekPengeluaranKeUnit=50;
var NamaForm_viApotekPengeluaranKeUnit="Pengeluaran Ke Unit ";
var mod_name_viApotekPengeluaranKeUnit="viApotekPengeluaranKeUnit";
var now_viApotekPengeluaranKeUnit= new Date();
var addNew_viApotekPengeluaranKeUnit;
var rowSelected_viApotekPengeluaranKeUnit;
var setLookUps_viApotekPengeluaranKeUnit;
var mNoKunjungan_viApotekPengeluaranKeUnit='';
var CurrentData_viApotekPengeluaranKeUnit ={
	data: Object,
	details: Array,
	row: 0
};
CurrentPage.page = dataGrid_viApotekPengeluaranKeUnit(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
PengeluaranGF.func.refreshData();


function ShowPesanWarningPengeluaranKeUnit(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorPengeluaranKeUnit(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:290
		}
	);
};


function ShowPesanInfoPengeluaranKeUnit(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:350
		}
	);
};
function dataGrid_viApotekPengeluaranKeUnit(mod_id_viApotekPengeluaranKeUnit){	
	PengeluaranGF.form.Grid.main = new Ext.grid.EditorGridPanel({
			title: '',
			store: PengeluaranGF.form.ArrayStore.main,
			autoScroll: true,
			columnLines: false,
			//flex: true,
			border:false,
			height:290,
			anchor: '100%,100%%',
			selModel: new Ext.grid.RowSelectionModel({
				singleSelect: true,
				listeners:{
					rowselect: function(sm, row, rec){
						rowSelected_viApotekPengeluaranKeUnit = undefined;
						rowSelected_viApotekPengeluaranKeUnit = PengeluaranGF.form.ArrayStore.main.getAt(row);
						CurrentData_viApotekPengeluaranKeUnit
						CurrentData_viApotekPengeluaranKeUnit.row = row;
						CurrentData_viApotekPengeluaranKeUnit.data = rowSelected_viApotekPengeluaranKeUnit.data;
						if (rowSelected_viApotekPengeluaranKeUnit.data.post_out==='0')
							{
								Ext.getCmp('btnHapusTrx_viApotekPengeluaranKeUnit').enable();
							}
							else
							{
								Ext.getCmp('btnHapusTrx_viApotekPengeluaranKeUnit').disable();
							}
					}
				}
			}),
			listeners:{
				rowdblclick: function (sm, ridx, cidx){
					rowSelected_viApotekPengeluaranKeUnit = PengeluaranGF.form.ArrayStore.main.getAt(ridx);
					if (rowSelected_viApotekPengeluaranKeUnit != undefined){
						setLookUp_viApotekPengeluaranKeUnit(rowSelected_viApotekPengeluaranKeUnit.data,false);
					}else{
						setLookUp_viApotekPengeluaranKeUnit();
					}
				}
			},
			colModel: new Ext.grid.ColumnModel([
					new Ext.grid.RowNumberer(),
					{
						header		: 'Status Posting',
						width		: 20,
						sortable	: false,
						dataIndex	: 'post_out',
						menuDisabled: true,
						renderer	: function(value, metaData, record, rowIndex, colIndex, store){
							 switch (value){
								 case '1':
									 metaData.css = 'StatusHijau'; 
									 break;
								 case '0':
									 metaData.css = 'StatusMerah';
									 break;
							 }
							 return '';
						}
					},{
						header: 'No. Keluar',
						dataIndex: 'no_stok_out',
						sortable: true,
						width: 35
					},{
						header:'Tgl Pengeluaran',
						dataIndex: 'tgl_stok_out',						
						width: 20,
						sortable: true,
						filter: {},
						renderer: function(v, params, record){
							return ShowDate(record.data.tgl_stok_out);
						}
					},{
						header: 'Unit Tujuan',
						dataIndex: 'nm_unit_far',
						sortable: true,
						flex: 1
					},{
						header: 'Kepemilikan',
						dataIndex: 'milik',
						sortable: true,
						width: 40,
					}
				]
			),
			tbar:{
				xtype: 'toolbar',
				id: 'toolbar_viApotekPengeluaranKeUnit',
				items: [
					{
						xtype: 'button',
						text: 'Tambah [F1]',
						iconCls: 'add',
						tooltip: 'Edit Data',
						id:'btnTambahGFPengeluaran',
						handler: function(sm, row, rec){
							GFStatusPostingPengeluaranUnit='0';
							setLookUp_viApotekPengeluaranKeUnit();
						}
					},{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						handler: function(sm, row, rec){
							if(rowSelected_viApotekPengeluaranKeUnit != undefined){
								setLookUp_viApotekPengeluaranKeUnit(rowSelected_viApotekPengeluaranKeUnit.data,false)
							}else{
								Ext.Msg.alert('Information','Data Belum Dipilih.');
							}
						}
					},
					{
						xtype: 'button',
						text: 'Hapus Transaksi',
						iconCls: 'remove',
						tooltip: 'Hapus Data',
						disabled:true,
						id: 'btnHapusTrx_viApotekPengeluaranKeUnit',
						handler: function(sm, row, rec)
						{
							var datanya=rowSelected_viApotekPengeluaranKeUnit.data;
							if (datanya===undefined){
								ShowPesanWarningPengeluaranKeUnit('Belum ada data yang dipilih','Gudang Farmasi');
							}
							else
							{
								 Ext.Msg.show({
									title: 'Hapus Transaksi',
									msg: 'Anda yakin akan menghapus data transaksi ini ?',
									buttons: Ext.MessageBox.YESNO,
									fn: function (btn) {
										if (btn == 'yes')
										{
											
											console.log(datanya);
											var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan Pembatalan Transaksi:', function (btn, combo) {
														if (btn == 'ok')
														{
															var variablebatalhistori = combo;
															if (variablebatalhistori != '')
															{
																 Ext.Ajax.request({
										   
																		url: baseURL + "index.php/apotek/functionAPOTEK/hapusTrxPengeluaranKeUnit",
																		 params: {
																			
																			no_stok_out: datanya.no_stok_out,
																			tgl_stok_out: datanya.tgl_stok_out,
																			alasan: variablebatalhistori
																		
																		},
																		failure: function(o)
																		{
																			 var cst = Ext.decode(o.responseText);
																			ShowPesanWarningPengeluaranKeUnit('Data transaksi tidak dapat dihapus','Gudang Farmasi');
																		},	    
																		success: function(o) {
																			var cst = Ext.decode(o.responseText);
																			if (cst.success===true)
																			{
																				PengeluaranGF.func.refreshData();
																				ShowPesanInfoPengeluaranKeUnit('Data transaksi Berhasil dihapus','Gudang Farmasi');
																				Ext.getCmp('btnHapusTrx_viApotekPengeluaranKeUnit').disable();
																			}
																			else
																			{
																				ShowPesanErrorPengeluaranKeUnit('Data transaksi tidak dapat dihapus','Gudang Farmasi');
																			}
																			
																		}
																
																})
															} else
															{
																ShowPesanWarningPengeluaranKeUnit('Silahkan isi alasan terlebih dahaulu', 'Keterangan');

															}
														}

													}); 
											
										}
									},
									icon: Ext.MessageBox.QUESTION
								});
							} 
						}
					},
				]
			},
			//bbar : bbar_paging(mod_name_viApotekPengeluaranKeUnit, selectCount_viApotekPengeluaranKeUnit, dataSource_viApotekPengeluaranKeUnit),
			viewConfig:{
				forceFit: true
			}
	})
    PengeluaranGF.form.Panel.main = new Ext.FormPanel({
		title: NamaForm_viApotekPengeluaranKeUnit,
		iconCls: 'Studi_Lanjut',
		id: mod_id_viApotekPengeluaranKeUnit,
		region: 'center',
		closable: true,        
		border: false, 
		items: [
	        {
				 xtype		: 'panel',
				 plain		: false,
				 anchor		: '100%',
				 bodyStyle	: 'padding: 2px;',
				 items	: [
				      	   {
				      		   layout	: 'column',
				      		   border	: false,
				      		   bodyStyle: 'padding: 5px;',
				      		   items	:[
									{
										layout	: 'form',
										border	: false,
										items	: [
														{	
														    xtype: 'buttongroup',
														    columns: 11,
														    defaults: {
														        scale: 'small'
															},
															frame: false,
														    items: 
														    [
														        { 
																	xtype: 'tbtext', 
																	text: 'No. Keluar : ', 
																	style:{'text-align':'right','font-size':'11px'},
																	width: 100
																},						
																PengeluaranGF.form.TextField.srchNo=new Ext.form.TextField({
																	name:'no_stok_out',
																	width: 120,
																	listeners:{ 
																		'specialkey' : function(){
																			if (Ext.EventObject.getKey() === 13){
																				PengeluaranGF.func.refreshData();
																			} 						
																		}
																	}
																})
															]
														},
														{
														    xtype: 'buttongroup',
														    columns: 11,
														    defaults: {
														        scale: 'small'
															},
															frame: false,
														    items: 
														    [
																{ 
																	xtype: 'tbtext', 
																	text: 'Tgl Terima : ', 
																	style:{'text-align':'right','font-size':'11px'},
																	width: 100
																},PengeluaranGF.form.DateField.srchDateStart=new Ext.form.DateField({
																	value: now_viApotekPengeluaranKeUnit,
																	name: 'startDate',
																	format: 'd/M/Y',
																	width: 120,
																	listeners:
																	{ 
																		'specialkey' : function()
																		{
																			if (Ext.EventObject.getKey() === 13) 
																			{
																				PengeluaranGF.func.refreshData();							
																			} 						
																		}
																	}
																}),
																{ 
																	xtype: 'tbtext', 
																	text: ' s.d &nbsp;', 
																	style:{'text-align':'right','font-size':'11px'},
																	width: 30
																},																								
																PengeluaranGF.form.DateField.srchDateEnd=new Ext.form.DateField({
																	value: now_viApotekPengeluaranKeUnit,
																	format: 'd/M/Y',
																	name:'lastDate',
																	width: 120,
																	listeners:
																	{ 
																		'specialkey' : function()
																		{
																			if (Ext.EventObject.getKey() === 13) 
																			{
																				PengeluaranGF.func.refreshData();							
																			} 						
																		}
																	}
																})						
															]
														}   
										  ]},
										  {
											  layout	: 'form',
											  border	: false,
											  items		: [
													{
													    xtype: 'buttongroup',
													    columns: 11,
													    defaults: {
													        scale: 'small'
														},
														frame: false,
													    items: 
													    [
															{ 
																xtype: 'tbtext', 
																text: 'Unit : ', 
																style:{'text-align':'right','font-size':'11px'},
																width: 100
															},		
															viCombo_Unit(150, 'unit'),			
															{xtype: 'tbspacer',height: 3, width:10},
															{
																xtype: 'button',
																text: 'Cari',
																iconCls: 'refresh',
																tooltip: 'Cari',
																width:100,
																id: 'btnCariGFPengerluaranKeUnit',
																handler: function(){	
																	PengeluaranGF.func.refreshData();
																}                        
															}
														]
													},  
													{
													    xtype: 'buttongroup',
													    columns: 11,
													    defaults: {
													        scale: 'small'
														},
														frame: false,
													    items: 
													    [
															{ 
																xtype: 'tbtext', 
																text: 'Posting : ', 
																style:{'text-align':'right','font-size':'11px'},
																width: 100
															},		
															//posting:new Ext.data.ArrayStore({id: 0,fields:['Id','displayText'],data: [[1,'Semua'],[2,'Posting'],[3,'Belum Posting']]}),
															new Ext.form.ComboBox({
														            fieldLabel: 'Posting',
																	valueField: 'displayText',
														            displayField: 'displayText',
																	store: PengeluaranGF.form.ArrayStore.posting,
														            width: 100,
														            value:'Belum Posting',
														            mode: 'local',
														            typeAhead: true,
														            triggerAction: 'all',                        
														            name: 'post',
														            lazyRender: true,
														            id: 'cbPostingGFPengeluranUnit',
																	listeners:{ 
																		'specialkey' : function(){
																			if (Ext.EventObject.getKey() === 13) {
																				PengeluaranGF.func.refreshData();
																			} 						
																		}
																	}
														        }
														    )
														]
													}
								       		   ]
										  }
										
									
									
									  ]
				      	   }
					  ]
				 },
				 PengeluaranGF.form.Grid.main,grid_minta()]
    });
	dataGriAwal_ordr();
    return PengeluaranGF.form.Panel.main;
}
function grid_minta(){
	
	 var FieldMaster_PermintaanKegudang_farmasi = 
	[
		'no_ro_unit', 'kd_milik', 'nm_unit_far','kd_unit_far', 'tgl_ro', 'milik'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_PermintaanKegudang_farmasi = new WebApp.DataStore
	({
        fields: FieldMaster_PermintaanKegudang_farmasi
    });
    // dataGriAwal_ordr();
    // Grid Apotek Perencanaan # --------------
	GridDataView_Permintaanscreen_farmasi = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: 'Permintaan dari Unit farmasi lain',
			store: dataSource_PermintaanKegudang_farmasi,
			autoScroll: true,
			columnLines: true,
			border:true,
			height:140,
			anchor: '100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_PermintaanKegudang_farmasi = dataSource_PermintaanKegudang_farmasi.getAt(ridx);
					if (rowSelected_PermintaanKegudang_farmasi != undefined){
						setLookUp_viApotekPengeluaranKeUnit(rowSelected_PermintaanKegudang_farmasi.data,true);
					}else{
						setLookUp_viApotekPengeluaranKeUnit();
					}
					
				}
				// End Function # --------------
			},
			tbar : [
				{
					xtype 	: 'label',
					text 	: 'Tanggal : ',
					id 		: 'lbl_tgl_awal',
				},
				{
					xtype 	: 'datefield',
					fieldLabel 	: 'textfield',
					id 	 	: 'txt_date_1',
					format 	: 'Y-m-d',
					value 	: tanggal,
					enableKeyEvents: true,
					listeners:{
						'specialkey': function (){
							if (Ext.EventObject.getKey() === 13){
								dataGriAwal_ordr();
							}
						},
					}
				},
				{
					xtype 	: 'label',
					text 	: ' sd ',
					id 		: 'lbl_tgl_akhir',
				},
				{
					xtype 	: 'datefield',
					fieldLabel 	: 'textfield',
					id 	 	: 'txt_date_2',
					format 	: 'Y-m-d',
					value 	: tanggal,
					listeners:{
						'specialkey': function (){
							if (Ext.EventObject.getKey() === 13){
								dataGriAwal_ordr();
							}
						},
					}
				},
			],
			/**
	        *	Mengatur tampilan pada Grid Apotek perencanaan
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header: 'No. Permintaan',
						dataIndex: 'no_ro_unit',
						sortable: true,
						width: 35
						
					},
					//-------------- ## --------------
					{
						header: 'Unit farmasi',
						dataIndex: 'nm_unit_far',
						sortable: true,
						width: 35
					},
					//-------------- ## --------------
					{
						header:'Tgl Minta',
						dataIndex: 'tgl_ro',						
						width: 30,
						sortable: true,
						hideable:false,
                        menuDisabled:true,
						renderer: function(v, params, record)
						{
						return ShowDate(record.data.tgl_ro);
						}
					},
					//-------------- ## --------------
					{
						header: 'Milik',
						dataIndex: 'milik',
						sortable: true,
						width: 50
					},
					//-------------- ## --------------
					//-------------- ## --------------
					{
						header: 'kd_milik',
						dataIndex: 'kd_milik',
						sortable: true,
						width: 40,
						hidden:true
					},{
						header: 'kd_unit_far',
						dataIndex: 'kd_unit_far',
						sortable: true,
						width: 40,
						hidden:true
					}
					//-------------- ## --------------
				]
			),
			
			viewConfig: 
			{
				forceFit: true
			}
		}
    );
	 return GridDataView_Permintaanscreen_farmasi;
}
function dataGriAwal_ordr(nominta,tgl_ro,tgl_ro2){
	/*
	txt_date_1
txt_date_2
	 */
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gudang_farmasi/functionPermintaan/getDataGridAwal_order",
			params: {
				no_minta:nominta,
				tgl_ro:Ext.getCmp('txt_date_1').getValue(),
				tgl_ro2:Ext.getCmp('txt_date_2').getValue()
			
			},
			failure: function(o)
			{
				ShowPesanErrorKegudang_farmasi('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dataSource_PermintaanKegudang_farmasi.removeAll();
					var recs=[],
						recType=dataSource_PermintaanKegudang_farmasi.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
						dataSource_PermintaanKegudang_farmasi.add(recs);
					
					
					
					GridDataView_Permintaanscreen_farmasi.getView().refresh();
				}
				else 
				{
					Ext.Msg.alert('Gagal','load data gagal');
				};
			}
		}
		
	)
	
}

function dataGrid_obat_doubleclick(nominta){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gudang_farmasi/functionPermintaan/getDataGrid_detail_pengeluaran",
			params: {
				no_minta:nominta,
				},
			failure: function(o)
			{
				ShowPesanErrorKegudang_farmasi('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					PengeluaranGF.form.ArrayStore.detail.removeAll();
					var recs=[],
						recType=PengeluaranGF.form.ArrayStore.detail.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
						PengeluaranGF.form.ArrayStore.detail.add(recs);
					
					
					
					PengeluaranGF.form.Grid.detail.getView().refresh();
					PengeluaranGF.func.refreshCounting();
				}
				else 
				{
					Ext.Msg.alert('Gagal','gagal load data');
				};
			}
		}
		
	)
	
}
function shortcuts(){
	shortcut.set({
		code:'lookup',
		list:[
			{
				key:'ctrl+s',
				fn:function(){
					Ext.getCmp('btnSimpanGFPengeluaranKeUnit').el.dom.click();
				}
			},
			{
				key:'ctrl+d',
				fn:function(){
					Ext.getCmp('btnDeleteGFPengeluaranKeUnit').el.dom.click();
				}
			},{
				key:'f4',
				fn:function(){
					Ext.getCmp('btnPostingGFPengeluaranKeUnit').el.dom.click();
				}
			},{
				key:'f6',
				fn:function(){
					Ext.getCmp('btnUnPostingGFPengeluaranKeUnit').el.dom.click();
				}
			},
			{
				key:'f12',
				fn:function(){
					Ext.getCmp('btnCetakBuktiGFPengeluaranKeUnit').el.dom.click();
				}
			},
			{
				key:'esc',
				fn:function(){
					setLookUps_viApotekPengeluaranKeUnit.close();
				}
			}
		]
	});
}
function setLookUp_viApotekPengeluaranKeUnit(rowdata,x_obat_unit_luar){
    var lebar = 985;
    setLookUps_viApotekPengeluaranKeUnit = new Ext.Window({
		name: 'SetLookUps_viApotekPengeluaranKeUnit',
        title: NamaForm_viApotekPengeluaranKeUnit, 
        closeAction: 'destroy',    
        constrain: true,
        width: 950,
        height: 500,
        resizable:false,
		autoScroll: true,
		layout: {
		    type: 'vbox',
		    align : 'stretch'
		}, 
        border: false,
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: [getItemPanelInputPengeluaran_viApotekPengeluaranKeUnit(lebar),
				gridDataViewEdit_viApotekPengeluaranKeUnit()],
        listeners:{
            activate: function(){
				shortcuts();
            },
            afterShow: function(){
            	PengeluaranGF.vars.kdUnit=null;
                this.activate();
            },
            deactivate: function() {
                rowSelected_viApotekPengeluaranKeUnit=undefined;
				mNoKunjungan_viApotekPengeluaranKeUnit = '';
				shortcut.remove('lookup');
            },
			close: function (){
				shortcut.remove('lookup');
			},
        },
        fbar:{
				xtype: 'toolbar',
				items:[
					PengeluaranGF.form.Panel.posting=new Ext.form.DisplayField ({
					    value: '<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>'
					}),{
						columnWidth	: .09,
						border		: false,
						html		: " Posting",
						style:{'margin-right':'690px'}	
					},{
						xtype: 'displayfield',				
						width: 50,								
						value: 'Total:',
						fieldLabel: 'Label'
					},PengeluaranGF.form.TextField.total=new Ext.form.TextField({
	                    name: 'txtDRGridJmlEditData_viApotekPengeluaranKeUnit',
	                    width: 100,
	                    value: 0,
						style:{'text-align':'right'},
	                    readOnly: true
	                })	
				]
			},
			tbar: {
				xtype: 'toolbar',
				items:[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						id: 'btnAddGFPengeluaranKeUnit',
						handler: function()
						{
							
							if(GFStatusPostingPengeluaranUnit=='0'){
								var records = new Array();
								records.push(new PengeluaranGF.form.ArrayStore.detail.recordType());
								PengeluaranGF.form.ArrayStore.detail.add(records);
								PengeluaranGF.func.refreshCounting();
								
								PengeluaranGF.vars.lineDetail=PengeluaranGF.form.ArrayStore.detail.getCount()-1;
								PengeluaranGF.form.Grid.detail.startEditing(PengeluaranGF.vars.lineDetail,3);
							}else{
								Ext.Msg.alert('Informasi','Penyimpanan gagal karena data sudah diPosting, Harap Unposting jika ingin Menyimpan.');
							}
						}
					},{
						xtype: 'tbseparator'
					},{
						xtype: 'button',
						text: 'Save [CTRL+S]',
						iconCls: 'save',
						id: 'btnSimpanGFPengeluaranKeUnit',
						handler: function(){
							// if(GFStatusPostingPengeluaranUnit=='0'){
								PengeluaranGF.func.save();
							// }else{
								// Ext.Msg.alert('Informasi','Penyimpanan gagal karena data sudah diPosting, Harap Unposting jika ingin Menyimpan.');
							// }
						}
					},{
						xtype: 'tbseparator'
					},{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						id: 'btnSimpanExit_viApotekPengeluaranKeUnit',
						hidden:true,
						handler: function(){
							if(GFStatusPostingPengeluaranUnit=='0'){
								PengeluaranGF.func.save(function(){
									setLookUps_viApotekPengeluaranKeUnit.close();
								});
							}else{
								Ext.Msg.alert('Informasi','Penyimpanan gagal karena data sudah diPosting, Harap Unposting jika ingin Menyimpan.');
							}
						}
					},{
						xtype: 'tbseparator',
						hidden:true,
					},{
						xtype: 'button',
						text: 'Delete [CTRL+D]',
						iconCls: 'remove',
						id: 'btnDeleteGFPengeluaranKeUnit',
						handler: function(){
							if(GFStatusPostingPengeluaranUnit=='0'){
								var line	= PengeluaranGF.form.Grid.detail.getSelectionModel().selection.cell[0];
								var o=PengeluaranGF.form.ArrayStore.detail.getRange()[line].data;
								if(o.no_stok_out != undefined && o.no_stok_out!=''){
									if(PengeluaranGF.form.ArrayStore.detail.getCount()>1){
										Ext.Msg.prompt('Name', 'Apakah Data Ini Akan Dihapus?', function(btn, text){
										    if (btn == 'ok'){
										    	loadMask.show();
										    	$.ajax({
													url:baseURL + "index.php/gudang_farmasi/functionGFPengeluaran/deleteDetail",
													dataType:'JSON',
													type: 'POST',
													data:{no_stok_out:o.no_stok_out,out_line:o.out_line},
													success: function(r){
														loadMask.hide();
														if(r.processResult=='SUCCESS'){
															Ext.Msg.alert('Sukses','Data Berhasi Dihapus.');
															PengeluaranGF.form.ArrayStore.detail.removeAt(line);
															PengeluaranGF.form.Grid.detail.getView().refresh();
														}else{
															Ext.Msg.alert('Gagal',r.processMessage);
														}
													},
													error: function(jqXHR, exception) {
														loadMask.hide();
														Nci.ajax.ErrorMessage(jqXHR, exception);
													}
												});
										    }
										});
									}else{
										Ext.Msg.alert('Gagal','Data tidak bisa dihapus karena minimal mempunyai 1 data.');
									}
								}else{
									Ext.Msg.confirm('Konfirmasi', 'Apakah ingin menghapus data ini?', function (id, value) { 
										if (id === 'yes') { 
											PengeluaranGF.form.ArrayStore.detail.removeAt(line);
											PengeluaranGF.form.Grid.detail.getView().refresh();
										} 
									}, this); 
								}
							}else{
								Ext.Msg.alert('Error','Data Sudah Diposting, Harap Unposting Jika ingin Menghapus Data.');
							}
						}
					},{
						xtype:'tbseparator'
					},PengeluaranGF.form.Button.posting= new Ext.Button({
						text: 'Posting [F4]',
						iconCls: 'gantidok',
						disabled:true,
						id:'btnPostingGFPengeluaranKeUnit',
						handler: PengeluaranGF.func.posting
					}),{
						xtype:'tbseparator'
					},PengeluaranGF.form.Button.unpposting= new Ext.Button({
						text: 'Unposting [F6]',
						iconCls: 'reuse',
						disabled:true,
						id:'btnUnPostingGFPengeluaranKeUnit',
						handler: function(){
							if(GFStatusPostingPengeluaranUnit=='1'){
								Ext.Msg.confirm('Konfirmasi', 'Apakah Akan diUnposting ?', function (id, value) { 
									if (id === 'yes') { 
									var a=[];
									a.push({name:'no_stok_out',value:PengeluaranGF.form.TextField.no.getValue()});
									for(var i=0;i<PengeluaranGF.form.ArrayStore.detail.getCount() ; i++){
										var o=PengeluaranGF.form.ArrayStore.detail.getRange()[i].data;
										a.push({name: 'kd_prd[]',value:o.kd_prd});
										a.push({name: 'qty[]',value:o.qty});
									}
										loadMask.show();
										$.ajax({
											url:baseURL + "index.php/gudang_farmasi/functionGFPengeluaran/unposting",
											dataType:'JSON',
											type: 'POST',
											data:a,
											success: function(r){
												loadMask.hide();
												if(r.processResult=='SUCCESS'){
													Ext.Msg.alert('Sukses','Data Berhasi diUnposting.');
													GFStatusPostingPengeluaranUnit='0';
													PengeluaranGF.form.Panel.posting.setValue('<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>');
													Ext.getCmp('btnAddGFPengeluaranKeUnit').enable();
													Ext.getCmp('btnSimpanGFPengeluaranKeUnit').enable();
													Ext.getCmp('btnSimpanExit_viApotekPengeluaranKeUnit').enable();
													Ext.getCmp('btnDeleteGFPengeluaranKeUnit').enable();
													Ext.getCmp('btnPostingGFPengeluaranKeUnit').enable();
													Ext.getCmp('btnCetakBuktiGFPengeluaranKeUnit').disable();
													Ext.getCmp('btnUnPostingGFPengeluaranKeUnit').disable();
													PengeluaranGF.func.initListRefresh(r.listData);
												}else{
													Ext.Msg.alert('Gagal',r.processMessage);
												}
											},
											error: function(jqXHR, exception) {
												loadMask.hide();
												Nci.ajax.ErrorMessage(jqXHR, exception);
											}
										});
									} 
								}, this); 
							}else{
								Ext.Msg.alert('Error','Data Belum Diposting!');
							}
						}
					}),{
						xtype:'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Print [F12]',
						iconCls:'print',
						disabled:true,
						id: 'btnCetakBuktiGFPengeluaranKeUnit',
						handler: function()
						{/*
							Ext.Msg.confirm('Konfirmasi', 'Siap mencetak bill?', function (id, value) { 
								if (id === 'yes') {
									cetakBukti();
								}
							});*/
							var tmp_no = PengeluaranGF.form.TextField.no.getValue();
							tmp_no = tmp_no.replace("/","~");
							tmp_no = tmp_no.replace("/","~");
							tmp_no = tmp_no.replace("/","~");
							console.log(tmp_no);
							var url = baseURL + "index.php/laporan/lap_billing_penunjang/cetak_gudang_farmasi_pengeluaran";
							new Ext.Window({
								title: 'Preview',
								width: 1000,
								height: 600,
								constrain: true,
								modal: true,
								html: "<iframe  style ='width: 100%; height: 100%;' src='" + url+"/"+tmp_no+"/true'></iframe>",
								tbar : [
									{
										xtype   : 'button',
										text    : 'Cetak Direct',
										iconCls : 'print',
										handler : function(){
											window.open(url+"/"+tmp_no+"/false", '_blank');
										}
									}
								]
							}).show();
						}
					},
				]
			}
    });
    loadMask.show();
    if (rowdata == undefined ) {
    	$.ajax({
			url:baseURL + "index.php/gudang_farmasi/functionGFPengeluaran/initTransaksi",
			dataType:'JSON',
			type: 'GET',
			success: function(r){
				loadMask.hide();
				if(r.processResult=='SUCCESS'){
					setLookUps_viApotekPengeluaranKeUnit.show();
					 
				    PengeluaranGF.vars.kdUnit=null;
				    PengeluaranGF.form.ArrayStore.detail.loadData([],false);
				    PengeluaranGF.form.Grid.detail.getView().refresh();
				}else{
					Ext.Msg.alert('Error',r.processMessage);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
    }else if(x_obat_unit_luar===true){
	$.ajax({
			url:baseURL + "index.php/gudang_farmasi/functionGFPengeluaran/initTransaksi",
			dataType:'JSON',
			type: 'GET',
			success: function(r){
				loadMask.hide();
				if(r.processResult=='SUCCESS'){
					setLookUps_viApotekPengeluaranKeUnit.show();
					 console.log(rowdata);
				    PengeluaranGF.vars.kdUnit=null;
					PengeluaranGF.form.ComboBox.unit.setValue(rowdata.nm_unit_far);
					PengeluaranGF.vars.kdUnit=rowdata.kd_unit_far;
					dataGrid_obat_doubleclick(rowdata.no_ro_unit);
				    PengeluaranGF.form.ArrayStore.detail.loadData([],false);
				    PengeluaranGF.form.Grid.detail.getView().refresh();
				}else{
					Ext.Msg.alert('Error',r.processMessage);
				}
			},
			error: function(jqXHR, exception) {
				loadMask.hide();
				Nci.ajax.ErrorMessage(jqXHR, exception);
			}
		});
	}else{
    	$.ajax({
 			url:baseURL + "index.php/gudang_farmasi/functionGFPengeluaran/getForEdit",
 			dataType:'JSON',
 			type: 'POST',
 			data: {'no_stok_out':rowdata.no_stok_out},
 			success: function(r){
 				loadMask.hide();
 				if(r.processResult=='SUCCESS'){
 					setLookUps_viApotekPengeluaranKeUnit.show();
 					PengeluaranGF.form.TextField.no.setValue(r.resultObject.no_stok_out);
 					PengeluaranGF.form.ComboBox.unit.setValue(r.resultObject.nm_unit_far);
 					PengeluaranGF.vars.kdUnit=r.resultObject.kd_unit_far;
 					PengeluaranGF.form.TextField.remark.setValue(r.resultObject.remark);
 					var date = Date.parseDate(r.resultObject.tgl_stok_out, "Y-m-d H:i:s");
 					PengeluaranGF.form.DateField.stockOut.setValue(date);
 					if(r.resultObject.post_out==1){
 						GFStatusPostingPengeluaranUnit='1';
 						PengeluaranGF.form.Panel.posting.setValue('<img src="'+baseURL+'ui/images/icons/16x16/hijau.png"/>');
						Ext.getCmp('btnAddGFPengeluaranKeUnit').disable();
						Ext.getCmp('btnSimpanGFPengeluaranKeUnit').disable();
						Ext.getCmp('btnSimpanExit_viApotekPengeluaranKeUnit').disable();
						Ext.getCmp('btnDeleteGFPengeluaranKeUnit').disable();
						Ext.getCmp('btnPostingGFPengeluaranKeUnit').disable();
						Ext.getCmp('btnCetakBuktiGFPengeluaranKeUnit').enable();
						Ext.getCmp('btnUnPostingGFPengeluaranKeUnit').enable();
 					}else{
 						PengeluaranGF.form.Panel.posting.setValue('<img src="'+baseURL+'ui/images/icons/16x16/merah.png"/>');
 						GFStatusPostingPengeluaranUnit='0';
						Ext.getCmp('btnAddGFPengeluaranKeUnit').enable();
						Ext.getCmp('btnSimpanGFPengeluaranKeUnit').enable();
						Ext.getCmp('btnSimpanExit_viApotekPengeluaranKeUnit').enable();
						Ext.getCmp('btnDeleteGFPengeluaranKeUnit').enable();
						Ext.getCmp('btnPostingGFPengeluaranKeUnit').enable();
						Ext.getCmp('btnCetakBuktiGFPengeluaranKeUnit').disable();
						Ext.getCmp('btnUnPostingGFPengeluaranKeUnit').disable();
 					}
 					PengeluaranGF.form.ArrayStore.detail.loadData([],false);
 					var recs = [],
 					recordType=PengeluaranGF.form.ArrayStore.detail.recordType;
 					for(var i=0, iLen=r.listData.length ; i<iLen ; i++){
 						recs.push(new recordType(r.listData[i]));
 					}
 					PengeluaranGF.form.ArrayStore.detail.add(recs);
 					PengeluaranGF.form.Grid.detail.getView().refresh();
 					PengeluaranGF.func.refreshCounting();
					
 				}else{
 					Ext.Msg.alert('Error',r.processMessage);
 				}
 			},
 			error: function(jqXHR, exception) {
 				loadMask.hide();
 				Nci.ajax.ErrorMessage(jqXHR, exception);
 			}
 		});
    }
	// shortcuts();
}


function getItemPanelInputPengeluaran_viApotekPengeluaranKeUnit(lebar){

    var items =new Ext.Panel({
		height: 80,
		autoScroll: true,
		bodyStyle:'margin: -1px 0;',
		border:true,
		items:[	
			{
				layout:'column',
				border: false,
				bodyStyle:'margin: 3px',
				items:[
					{
						layout: 'column',
						border: false,
						bodyStyle:'margin: 3px',
						items:[
							{
								xtype: 'displayfield',
								value:'No. Keluar :',
								width: 100
							},PengeluaranGF.form.TextField.no= new Ext.form.TextField({
								flex: 1,
								width : 120,	
								readOnly: true,
								listeners:{
									'specialkey': function(){
										if (Ext.EventObject.getKey() === 13){
										}
									}
								}
							})
						]
					},{
						layout: 'column',
						bodyStyle:'margin: 3px',
						border: false,
						items:[
							{
								xtype: 'displayfield',
								value:'Unit :',
								width: 100
							},viCombo_Unit(415, 'cboUnit_viApotekPengeluaranKeUnit')	
						]
					}
					
				]
			},{
				layout:'column',
				bodyStyle:'margin: 3px',
				border: false,
				items:[
					{
						layout: 'column',
						bodyStyle:'margin: 3px',
						border: false,
						items:[
							{
								xtype: 'displayfield',
								value:'Tanggal :',
								width: 100
							},PengeluaranGF.form.DateField.stockOut= new Ext.form.DateField({
									value: now_viApotekPengeluaranKeUnit,
									// readOnly: true,
									format: 'd/M/Y',
									width: 120,
									listeners:{ 
										'specialkey' : function(){
											if (Ext.EventObject.getKey() === 13) {
												datarefresh_viApotekPengeluaranKeUnit();
												PengeluaranGF.form.ComboBox.unit.focus();
											} 						
										}
									}
								})
						]
					},{
						layout: 'column',
						bodyStyle:'margin: 3px',
						border: false,
						items:[
							{
								xtype: 'displayfield',
								value:'Remark :',
								width: 100
							},PengeluaranGF.form.TextField.remark=new Ext.form.TextField({
								width : 360,	
								listeners:{
									'specialkey': function(me, e){
										PengeluaranGF.vars.lineDetail=0;
										if (Ext.EventObject.getKey() === 13){
											if(GFStatusPostingPengeluaranUnit=='0'){
												if(PengeluaranGF.form.ArrayStore.detail.getCount()==0){
													var records = new Array();
													records.push(new PengeluaranGF.form.ArrayStore.detail.recordType());
													PengeluaranGF.form.ArrayStore.detail.add(records);
													PengeluaranGF.func.refreshCounting();
													PengeluaranGF.vars.lineDetail=PengeluaranGF.form.ArrayStore.detail.getCount()-1;
													PengeluaranGF.form.Grid.detail.startEditing(PengeluaranGF.form.ArrayStore.detail.getCount()-1,3);
												}else{
													PengeluaranGF.form.Grid.detail.startEditing(0,3);
													PengeluaranGF.vars.lineDetail=0;
												}
											}else{
												Ext.Msg.alert('Error','Data Sudah Diposting, Harap Unposting Jika ingin Menghapus Data.');
											}
										}
										if (e.getKey() == e.TAB) {
											if(GFStatusPostingPengeluaranUnit=='0'){
												if(PengeluaranGF.form.ArrayStore.detail.getCount()==0){
													var records = new Array();
													records.push(new PengeluaranGF.form.ArrayStore.detail.recordType());
													PengeluaranGF.form.ArrayStore.detail.add(records);
													PengeluaranGF.func.refreshCounting();
													PengeluaranGF.vars.lineDetail=PengeluaranGF.form.ArrayStore.detail.getCount()-1;
													PengeluaranGF.form.Grid.detail.startEditing(PengeluaranGF.form.ArrayStore.detail.getCount()-1,3);
												}else{
													PengeluaranGF.form.Grid.detail.startEditing(0,3);
													PengeluaranGF.vars.lineDetail=0;
												}
											}else{
												Ext.Msg.alert('Error','Data Sudah Diposting, Harap Unposting Jika ingin Menghapus Data.');
											}
					                    }
									}
								}
							})	
						]
					}
					
				]
			}
			
		]
	});
    return items;
};
function gridDataViewEdit_viApotekPengeluaranKeUnit(){
	PengeluaranGF.form.Grid.detail=new Ext.grid.EditorGridPanel({
        store: PengeluaranGF.form.ArrayStore.detail,
        flex: 1,
        columnLines: true,
        // autoShow: false,
        stripeRows: true,
		// viewConfig:{forceFit: true},
        columns: [
			new Ext.grid.RowNumberer(),{			
				dataIndex: 'no_minta',
				header: 'RO Number',
				menuDisabled: true,
				fixed:true,
				width: 85
			},{
				dataIndex: 'kd_milik',
				header: 'M',
				menuDisabled: true,
				fixed:true,
				width: 30	
			},
			{			
				dataIndex: 'nama_obat',
				header: 'Uraian',
				sortable: true,
				width: 200,
				menuDisabled: true,
				fixed: true,
				editor: new Ext.form.TextField({
					allowBlank: false,
					enableKeyEvents:true,
					listeners:{
						keyDown: function(a,b,c){
							if(b.getKey()==13){
								var line	= PengeluaranGF.form.Grid.detail.getSelectionModel().selection.cell[0];
								// if(a.getValue().length < 3){
								if(a.getValue().length < 1){
									if(a.getValue().length != 0){
										Ext.Msg.show({
											title: 'Perhatian',
											msg: 'Kriteria huruf pencarian obat minimal 1 huruf!',
											buttons: Ext.MessageBox.OK,
											fn: function (btn) {
												if (btn == 'ok')
												{
													PengeluaranGF.form.Grid.detail.startEditing(line, 3);
												}
											}
										});
									}
									
								} else{		
									PencarianLookupGFPengeluaran = true;// Variabel pembeda getdata u/nama obat dan semua kepemilikan atau bukan. False => pencarian berdasarkan nama_obat dan kepemilikan, True => pencarian berdasarkan nama_obat saja
									FocusExitGFPengeluaran = false;
									LookUpSearchListGetObat_GFPengeluaran(a.getValue());
								}
							}
						}
					}
				})
			},
			/* {			
				dataIndex: 'nama_obat',
				header: 'Uraian',
				menuDisabled: true,
				fixed:true,
				width: 280,
				editor: new Nci.form.Combobox.autoComplete({
					store	: PengeluaranGF.form.ArrayStore.obat,
					select	: function(a,b,c){
						var line	= PengeluaranGF.form.Grid.detail.getSelectionModel().selection.cell[0];
						if(PengeluaranGF.form.ArrayStore.detail.getCount()-1===0)
						{
							PengeluaranGF.form.ArrayStore.detail.getRange()[line].data.nama_obat=b.data.nama_obat;
							PengeluaranGF.form.ArrayStore.detail.getRange()[line].data.kd_prd=b.data.kd_prd;
							PengeluaranGF.form.ArrayStore.detail.getRange()[line].data.kd_milik=b.data.kd_milik;
							PengeluaranGF.form.ArrayStore.detail.getRange()[line].data.kd_sat_besar=b.data.kd_sat_besar;
							PengeluaranGF.form.ArrayStore.detail.getRange()[line].data.jml_stok_apt=b.data.jml_stok_apt;
							PengeluaranGF.form.ArrayStore.detail.getRange()[line].data.fractions=b.data.fractions;
							PengeluaranGF.form.ArrayStore.detail.getRange()[line].data.harga_beli=b.data.harga_beli;
							PengeluaranGF.form.ArrayStore.detail.getRange()[line].data.min_stok=b.data.min_stok;
							// PengeluaranGF.form.ArrayStore.detail.getRange()[line].data.batch=b.data.batch;
							PengeluaranGF.func.refreshCounting();
							PengeluaranGF.form.Grid.detail.getView().refresh();
							PengeluaranGF.form.Grid.detail.startEditing(line,5);
						}else{
							var VALUE_x=1;
							for(var i = 0 ; i < PengeluaranGF.form.ArrayStore.detail.getCount();i++)
							{
								if(PengeluaranGF.form.ArrayStore.detail.getRange()[i].data.kd_prd===b.data.kd_prd)
								{
									VALUE_x=0;
								}
							}
							if(VALUE_x===0){
								Ext.Msg.alert('Perhatian','Anda telah memilih Obat yang sama');
								PengeluaranGF.form.ArrayStore.detail.removeAt(line);
							}else{
								PengeluaranGF.form.ArrayStore.detail.getRange()[line].data.nama_obat=b.data.nama_obat;
								PengeluaranGF.form.ArrayStore.detail.getRange()[line].data.kd_prd=b.data.kd_prd;
								PengeluaranGF.form.ArrayStore.detail.getRange()[line].data.kd_milik=b.data.kd_milik;
								PengeluaranGF.form.ArrayStore.detail.getRange()[line].data.kd_sat_besar=b.data.kd_sat_besar;
								PengeluaranGF.form.ArrayStore.detail.getRange()[line].data.jml_stok_apt=b.data.jml_stok_apt;
								PengeluaranGF.form.ArrayStore.detail.getRange()[line].data.fractions=b.data.fractions;
								PengeluaranGF.form.ArrayStore.detail.getRange()[line].data.harga_beli=b.data.harga_beli;
								PengeluaranGF.form.ArrayStore.detail.getRange()[line].data.min_stok=b.data.min_stok;
								// PengeluaranGF.form.ArrayStore.detail.getRange()[line].data.batch=b.data.batch;
								PengeluaranGF.func.refreshCounting();
								PengeluaranGF.form.Grid.detail.getView().refresh();
								PengeluaranGF.form.Grid.detail.startEditing(line,5);
							}
						}
					
						
					},
					insert	: function(o){
						return {
							kd_prd        	: o.kd_prd,
							nama_obat 		: o.nama_obat,
							kd_sat_besar	: o.kd_sat_besar,
							fractions		: o.fractions,
							harga_beli		: o.harga_beli,
							jml_stok_apt	: o.jml_stok_apt,
							kd_milik		: o.kd_milik,
							qty_b			: o.qty_b,
							min_stok		: o.min_stok,
							milik			: o.milik,
							text				:  '<table style="font-size: 11px;"><tr><td width="50">'+o.kd_prd+'</td><td width="200">'+o.nama_obat+'</td><td width="80">'+parseInt(o.harga_beli).toLocaleString('id', { style: 'currency', currency: 'IDR' })+'</td><td width="50"><td width="40">'+o.kd_sat_besar+'</td><td width="50" align="left">'+o.qty_b+'</td><td width="50" align="left">'+o.jml_stok_apt+'</td><td width="30" align="left">'+o.min_stok+'</td><td width="40" align="right">'+o.milik+'</td></tr></table>'
						}
					},
					url		: baseURL + "index.php/gudang_farmasi/functionGFPengeluaran/getObat",
					valueField: 'nama_obat',
					displayField: 'text',
					listWidth: 600,
					blur: function(){
						//GFPenerimaan.func.refreshNilai();
					}
				})
			}, */
			{
				dataIndex: 'kd_sat_besar',
				header: 'Sat B',
				menuDisabled: true,
				fixed:true,
				width: 50	
			},{
				xtype:'numbercolumn',
				dataIndex: 'jml_stok_apt',
				header: 'Stok',
				align: 'right',
				menuDisabled: true,
				fixed:true,
				width: 60
			},
			{
				// xtype:'numbercolumn',
				align:'right',
				dataIndex: 'qty_b',
				header: 'Qty B',
				menuDisabled: true,
				width: 60,
				fixed:true,
				align: 'right',
				editor: new Ext.form.NumberField({
					value:0,
					enableKeyEvents:true,
					listeners:{
						keyDown: function(a,b,c){
							if(b.getKey()==13){
								var line	= this.indeks;
								var o = PengeluaranGF.form.ArrayStore.detail.getRange()[line].data;
								if (parseFloat(a.getValue()) > parseFloat(o.jml_stok_apt))
								{
									Ext.Msg.alert('Perhatian','Qty melebihi stok tersedia.');
									o.qty_b = o.jml_stok_apt;
									o.qty = parseFloat(o.qty_b)*parseFloat(o.fractions);
									PengeluaranGF.func.refreshCounting();
								} else  {
									o.qty_b=a.getValue();
									o.qty = parseFloat(o.qty_b)*parseFloat(o.fractions);
									PengeluaranGF.func.refreshCounting();
									var nextRow=PengeluaranGF.form.ArrayStore.detail.getCount()-1;
									PengeluaranGF.form.Grid.detail.startEditing(line,7);
								}
							}
						},
						focus:function(){
							this.indeks=PengeluaranGF.form.Grid.detail.getSelectionModel().selection.cell[0];
						},
					}	
				})
			},{
				// xtype:'numbercolumn',
				align:'right',
				dataIndex: 'fractions',
				header: 'Frac',
				menuDisabled: true,
				fixed:true,
				align: 'right',
				width: 60
			},{
				// xtype:'numbercolumn',
				dataIndex: 'qty',
				header: 'Qty',
				align:'right',
				menuDisabled: true,
				width: 60,
				fixed:true,
				align: 'right',
				editor: new Ext.form.NumberField({
					value:0,
					enableKeyEvents:true,
					listeners:{
						keyDown: function(a,b,c){
							if(b.getKey()==13){
								var line	= this.indeks;
								var o = PengeluaranGF.form.ArrayStore.detail.getRange()[line].data;
								
								// if (parseFloat(a.getValue()) >= parseFloat(PengeluaranGF.form.ArrayStore.detail.getRange()[line].data.min_stok))
								// {
									// Ext.Msg.alert('Perhatian','Qty sudah mencapai atau melebihi minimum stok.');
									// // PengeluaranGF.form.ArrayStore.detail.getRange()[line].data.qty=PengeluaranGF.form.ArrayStore.detail.getRange()[line].data.min_stok;
								// }
								// else (parseFloat(a.getValue()) < parseFloat(PengeluaranGF.form.ArrayStore.detail.getRange()[line].data.min_stok))
								// {
									// PengeluaranGF.form.ArrayStore.detail.getRange()[line].data.qty=a.getValue();
								// }
								
								if (parseFloat(a.getValue()) > parseFloat(o.jml_stok_apt))
								{
									Ext.Msg.alert('Perhatian','Qty melebihi stok tersedia.');
									o.qty = o.jml_stok_apt;
									o.qty_b = parseFloat(o.qty)/parseFloat(o.fractions);
									PengeluaranGF.func.refreshCounting();
								} else  {
									o.qty=a.getValue();
									o.qty_b = parseFloat(o.qty)/parseFloat(o.fractions);
									PengeluaranGF.func.refreshCounting();
									var records = new Array();
									records.push(new PengeluaranGF.form.ArrayStore.detail.recordType());
									PengeluaranGF.form.ArrayStore.detail.add(records);
									var nextRow=PengeluaranGF.form.ArrayStore.detail.getCount()-1;
									PengeluaranGF.form.Grid.detail.startEditing(nextRow,3);
								}
							}
						},
						focus:function(){
							this.indeks=PengeluaranGF.form.Grid.detail.getSelectionModel().selection.cell[0];
						},
					}	
				})
			},{
				xtype:'numbercolumn',
				dataIndex: 'min_stok',
				header: 'Min Stok',
				align: 'right',
				hidden :true,
				menuDisabled: true,
				fixed:true,
				width: 30	
			},{
				xtype:'numbercolumn',
				dataIndex: 'sisa',
				header: 'Sisa',
				align: 'right',
				menuDisabled: true,
				fixed:true,
				width: 60
			},{
				xtype:'numbercolumn',
				dataIndex: 'harga_beli',
				header: 'Harga',
				menuDisabled: true,
				align: 'right',
				fixed:true,
				width: 100	
			},{
				xtype:'numbercolumn',
				dataIndex: 'jumlah',
				header: 'Jumlah &nbsp;&nbsp',
				align: 'right',
				menuDisabled: true,
				fixed:true,
				width: 120	
			},{
				xtype:'numbercolumn',
				dataIndex: 'jml_stok_apt',
				header: 'Jml',
				menuDisabled: true,
				fixed:true,
				hidden:true,
				align: 'right',
				width: 60
			},
			{
				dataIndex: 'out_line',
				header: 'out_line',
				align: 'right',
				sortable: true,
				hidden:true,
				width: 100	
			}
        ]
    });  
    return PengeluaranGF.form.Grid.detail;
}
function viCombo_Unit(lebar,Nama_ID){
    var Field_Unit = ['KODE', 'NAMA_UNIT'];
    PengeluaranGF.form.DataStore.unit = new WebApp.DataStore({fields: Field_Unit});    
    PengeluaranGF.form.DataStore.unit.load({
        params:{
            Skip: 0,
            Take: 1000,
            Sort: 'KD_UNIT',
            Sortdir: 'ASC',
            target: 'ComboAptUnit',
            param: ""
        }
    });
    PengeluaranGF.form.ComboBox.unit = new Ext.form.ComboBox({
        flex: 1,
        fieldLabel: 'Unit',
		valueField: 'NAMA_UNIT',
        displayField: 'NAMA_UNIT',
		store: PengeluaranGF.form.DataStore.unit ,
        width: 150,
        mode: 'local',
        typeAhead: true,
        triggerAction: 'all',                        
        name: Nama_ID,
        lazyRender: true,
        id: Nama_ID,
		listeners:{ 
			'specialkey' : function(){
				if (Ext.EventObject.getKey() === 13){
					PengeluaranGF.form.TextField.remark.focus();
				} 						
			},
			select:PengeluaranGF.func.resetDetail
		}
    });   
    return PengeluaranGF.form.ComboBox.unit;
}

function cetakBukti(){
	var params={
		no_keluar:PengeluaranGF.form.TextField.no.getValue(),
		tgl_keluar:PengeluaranGF.form.DateField.stockOut.value,
		kd_unit_far:PengeluaranGF.vars.kdUnit
	} ;
	var form = document.createElement("form");
	form.setAttribute("method", "post");
	form.setAttribute("target", "_blank");
	form.setAttribute("action", baseURL + "index.php/gudang_farmasi/cetakan_faktur_bill/cetakpengeluaranunit");
	var hiddenField = document.createElement("input");
	hiddenField.setAttribute("type", "hidden");
	hiddenField.setAttribute("name", "data");
	hiddenField.setAttribute("value", Ext.encode(params));
	form.appendChild(hiddenField);
	document.body.appendChild(form);
	form.submit();

}


function LookUpSearchListGetObat_GFPengeluaran(nama_obat)
{
	
	WindowLookUpSearchListGetObat_GFPengeluaran = new Ext.Window
    (
        {
            id: 'pnlLookUpSearchListGetObat_GFPengeluaran',
            title: 'List Pencarian Obat',
            width:620,
            height: 250,
            border: false,
            resizable:false,
            plain: true,
            iconCls: 'icon_lapor',
            modal: true,
            items: [
				// ItempanelListPencarianObatKepemilikan_GFPengeluaran(nama_obat),
				gridListObatPencarianGFPengeluaran()
			],
			listeners:
			{             
				activate: function()
				{
						
				},
				afterShow: function()
				{
					this.activate();

				},
				deactivate: function()
				{
					
				},
				close: function (){
					if(FocusExitGFPengeluaran == false){
						var line	= PengeluaranGF.form.Grid.detail.getSelectionModel().selection.cell[0];
						PengeluaranGF.form.Grid.detail.startEditing(line, 3);	
					}
				}
			}
        }
    );

    WindowLookUpSearchListGetObat_GFPengeluaran.show();
	getListObatSearch_GFPengeluaran(nama_obat);
};

function gridListObatPencarianGFPengeluaran(){
	var fldDetail = ['kd_prd','nama_obat','harga_beli','kd_sat_besar','qty_b','jml_stok_apt','milik'];
	dsGridListPencarianObat_GFPengeluaran = new WebApp.DataStore({ fields: fldDetail });
    GridListPencarianObatColumnModel =  new Ext.grid.ColumnModel([
        new Ext.grid.RowNumberer(),
		{
			dataIndex		: 'kd_prd',
			header			: 'Kode',
			width			: 70,
			menuDisabled	: true,
        },
		{
			dataIndex		: 'nama_obat',
			header			: 'Nama Obat',
			width			: 180,
			menuDisabled	: true,
        },
		{
			dataIndex		: 'qty_b',
			header			: 'Stok B',
			width			: 60,
			align			: 'right',
			menuDisabled	: true,
        },
		{
			dataIndex		: 'kd_sat_besar',
			header			: 'Satuan',
			width			: 55,
			menuDisabled	: true,
        },
		{
			dataIndex		: 'jml_stok_apt',
			header			: 'Stok',
			width			: 45,
			align			: 'right',
			menuDisabled	: true,
        },
		{
			dataIndex		: 'harga_beli',
			header			: 'Harga',
			xtype			: 'numbercolumn',
			align			: 'right',
			format 			: '0,000',
			width			: 60,
			menuDisabled	: true,
        },
		{
			dataIndex		: 'milik',
			header			: 'Kepemilikan',
			width			: 80,
			menuDisabled	: true,
        }
	]);
	
	
	GridListPencarianObat_GFPengeluaran= new Ext.grid.EditorGridPanel({
		id			: 'GrdListPencarianObat_GFPengeluaran',
		stripeRows	: true,
		width		: 610,
		height		: 220,
        store		: dsGridListPencarianObat_GFPengeluaran,
        border		: true,
        frame		: false,
        autoScroll	: true,
        cm			: GridListPencarianObatColumnModel,
		selModel: new Ext.grid.RowSelectionModel({
			singleSelect: true,
			listeners:
			{
				rowselect: function(sm, row, rec)
				{
					currentRowSelectionPencarianObatGFPengeluaran = undefined;
					currentRowSelectionPencarianObatGFPengeluaran = dsGridListPencarianObat_GFPengeluaran.getAt(row);
				}
			}
		}),
		listeners	: {
			rowclick: function( $this, rowIndex, e )
			{
				// trcellCurrentTindakan_KasirRWJ = rowIndex;
    		},
			celldblclick: function(gridView,htmlElement,columnIndex,dataRecord){
				
			},
			keydown : function(e){
				if(e.getKey() == 13){
					var line	= PengeluaranGF.form.Grid.detail.getSelectionModel().selection.cell[0];
					if(PengeluaranGF.form.ArrayStore.detail.getCount()-1===0)
					{
						PengeluaranGF.form.ArrayStore.detail.getRange()[line].data.nama_obat=currentRowSelectionPencarianObatGFPengeluaran.data.nama_obat;
						PengeluaranGF.form.ArrayStore.detail.getRange()[line].data.kd_prd=currentRowSelectionPencarianObatGFPengeluaran.data.kd_prd;
						PengeluaranGF.form.ArrayStore.detail.getRange()[line].data.kd_milik=currentRowSelectionPencarianObatGFPengeluaran.data.kd_milik;
						PengeluaranGF.form.ArrayStore.detail.getRange()[line].data.kd_sat_besar=currentRowSelectionPencarianObatGFPengeluaran.data.kd_sat_besar;
						PengeluaranGF.form.ArrayStore.detail.getRange()[line].data.jml_stok_apt=currentRowSelectionPencarianObatGFPengeluaran.data.jml_stok_apt;
						PengeluaranGF.form.ArrayStore.detail.getRange()[line].data.fractions=currentRowSelectionPencarianObatGFPengeluaran.data.fractions;
						PengeluaranGF.form.ArrayStore.detail.getRange()[line].data.harga_beli=currentRowSelectionPencarianObatGFPengeluaran.data.harga_beli;
						PengeluaranGF.form.ArrayStore.detail.getRange()[line].data.min_stok=currentRowSelectionPencarianObatGFPengeluaran.data.min_stok;
						// PengeluaranGF.form.ArrayStore.detail.getRange()[line].data.batch=currentRowSelectionPencarianObatGFPengeluaran.data.batch;
						PengeluaranGF.func.refreshCounting();
						PengeluaranGF.form.Grid.detail.getView().refresh();
						PengeluaranGF.form.Grid.detail.startEditing(line,5);
					}else{
						var VALUE_x=1;
						for(var i = 0 ; i < PengeluaranGF.form.ArrayStore.detail.getCount();i++)
						{
							if(PengeluaranGF.form.ArrayStore.detail.getRange()[i].data.kd_prd===currentRowSelectionPencarianObatGFPengeluaran.data.kd_prd)
							{
								VALUE_x=0;
							}
						}
						if(VALUE_x===0){
							Ext.Msg.alert('Perhatian','Anda telah memilih Obat yang sama');
							PengeluaranGF.form.ArrayStore.detail.removeAt(line);
						}else{
							PengeluaranGF.form.ArrayStore.detail.getRange()[line].data.nama_obat=currentRowSelectionPencarianObatGFPengeluaran.data.nama_obat;
							PengeluaranGF.form.ArrayStore.detail.getRange()[line].data.kd_prd=currentRowSelectionPencarianObatGFPengeluaran.data.kd_prd;
							PengeluaranGF.form.ArrayStore.detail.getRange()[line].data.kd_milik=currentRowSelectionPencarianObatGFPengeluaran.data.kd_milik;
							PengeluaranGF.form.ArrayStore.detail.getRange()[line].data.kd_sat_besar=currentRowSelectionPencarianObatGFPengeluaran.data.kd_sat_besar;
							PengeluaranGF.form.ArrayStore.detail.getRange()[line].data.jml_stok_apt=currentRowSelectionPencarianObatGFPengeluaran.data.jml_stok_apt;
							PengeluaranGF.form.ArrayStore.detail.getRange()[line].data.fractions=currentRowSelectionPencarianObatGFPengeluaran.data.fractions;
							PengeluaranGF.form.ArrayStore.detail.getRange()[line].data.harga_beli=currentRowSelectionPencarianObatGFPengeluaran.data.harga_beli;
							PengeluaranGF.form.ArrayStore.detail.getRange()[line].data.min_stok=currentRowSelectionPencarianObatGFPengeluaran.data.min_stok;
							// PengeluaranGF.form.ArrayStore.detail.getRange()[line].data.batch=currentRowSelectionPencarianObatGFPengeluaran.data.batch;
							PengeluaranGF.func.refreshCounting();
							PengeluaranGF.form.Grid.detail.getView().refresh();
							PengeluaranGF.form.Grid.detail.startEditing(line,5);
						}
					}
					FocusExitGFPengeluaran = true;
					WindowLookUpSearchListGetObat_GFPengeluaran.close();
				}
			},
		},
		viewConfig	: {forceFit: true}
    });
	return GridListPencarianObat_GFPengeluaran;
}

function ItempanelListPencarianObatKepemilikan_GFPengeluaran(nama_obat){
	 var items=
    (
        {
            id: 'panelListPencarianObatKepemilikan_GFPengeluaran',
			layout:'form',
			border: true,
			bodyStyle:'padding: 2px',
			height: 50,
			items:
			[
				{
					layout: 'absolute',
					bodyStyle: 'padding: 2px ',
					border: true,
					anchor: '100% 100%',
					items:
					[
						{
							x: 10,
							y: 10,
							xtype: 'label',
							text: 'Kepemilikan'
						},
						{
							x: 140,
							y: 10,
							xtype: 'label',
							text: ':'
						},
						ComboKepemilikanPencarianObat_GFPengeluaran(nama_obat),
					]
				}
				
			]	
        }
    );

    return items;
}
function ComboKepemilikanPencarianObat_GFPengeluaran(nama_obat)
{
	var Field = ['kd_milik','milik'];
	ds_kepemilikan_pencarianobat_GFPengeluaran = new WebApp.DataStore({ fields: Field });
	getKepemilikanPencarianObat();
	
	var cboKepemilikanPencarianObat_GFPengeluaran = new Ext.form.ComboBox
	(
		{
			id:'cbKepemilikanPencarianObat_GFPengeluaran',
			x: 160,
			y: 10,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			width: 350,
			// editable: false,
			tabIndex:5,
			store: ds_kepemilikan_pencarianobat_GFPengeluaran,
			valueField: 'kd_milik',
			displayField: 'milik',
			value:'SEMUA KEPEMILIKAN',
			listeners:
			{
				'select': function(a,b,c)
				{
					PencarianLookupGFPengeluaran = false; // Variabel pembeda getdata u/nama obat dan semua kepemilikan atau bukan. False => pencarian berdasarkan nama_obat dan kepemilikan, True => pencarian berdasarkan nama_obat saja
					getListObatSearch_GFPengeluaran(nama_obat);
				},
				'specialkey' : function()
				{
					if(Ext.EventObject.getKey() == 13 || Ext.EventObject.getKey() == 9){
					}			
				}
			}
		}
	);
	return cboKepemilikanPencarianObat_GFPengeluaran;
};

function getListObatSearch_GFPengeluaran(nama_obat){
	Ext.Ajax.request ({
		url: baseURL + "index.php/gudang_farmasi/functionGFPengeluaran/getListObat",
		params: {
			nama_obat:nama_obat
		},
		failure: function(o)
		{
			Ext.Msg.alert('Error','Error menampilkan pencarian obat. Hubungi Admin!');
		},	
		success: function(o) 
		{   
			dsGridListPencarianObat_GFPengeluaran.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				if(cst.totalrecords == 0 ){
					if(PencarianLookupGFPengeluaran == true){						
						Ext.Msg.show({
							title: 'Information',
							msg: 'Tidak ada obat yang sesuai atau kriteria obat kurang!',
							buttons: Ext.MessageBox.OK,
							fn: function (btn) {
								if (btn == 'ok')
								{
									var line	= PengeluaranGF.form.Grid.detail.getSelectionModel().selection.cell[0];
									PengeluaranGF.form.Grid.detail.startEditing(line, 3);
									WindowLookUpSearchListGetObat_GFPengeluaran.close();
								}
							}
						});
					}
				} else{
					var recs=[],
						recType=dsGridListPencarianObat_GFPengeluaran.recordType;
					for(var i=0; i<cst.ListDataObj.length; i++){
						recs.push(new recType(cst.ListDataObj[i]));						
					}
					dsGridListPencarianObat_GFPengeluaran.add(recs);
					GridListPencarianObat_GFPengeluaran.getView().refresh();
					GridListPencarianObat_GFPengeluaran.getSelectionModel().selectRow(0);
					GridListPencarianObat_GFPengeluaran.getView().focusRow(0);
				}
				
			} else {
				Ext.Msg.alert('Error','Gagal membaca data list pencarian obat');
			};
		}
	});
}

function getKepemilikanPencarianObat(){
	Ext.Ajax.request({
		url: baseURL + "index.php/gudang_farmasi/functionGFPengeluaran/getKepemilikan",
		params: {text:'0'},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['ListDataObj'].length; i<iLen; i++){
				var recs    = [],recType =   ds_kepemilikan_pencarianobat_GFPengeluaran.recordType;
				var o=cst['ListDataObj'][i];
				recs.push(new recType(o));
				ds_kepemilikan_pencarianobat_GFPengeluaran.add(recs);
			}
		}
	});
}

shortcut.set({
	code:'main',
	list:[
		{
			key:'f1',
			fn:function(){
				Ext.getCmp('btnTambahGFPengeluaran').el.dom.click();
			}
		},
	]
});