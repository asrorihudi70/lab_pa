
var selectCount_perencanaanKegudang_farmasi=50;
var NamaForm_perencanaanKegudang_farmasi="Perencanaan obat gudang farmasi ";
var selectCountStatusPostingKegudang_farmasi='Semua';
var mod_name_perencanaanKegudang_farmasi="perencanaanKegudang_farmasi";
var now_perencanaanKegudang_farmasi= new Date();
var rowSelected_perencanaanKegudang_farmasi;
var rowSelectedGridobat_perencanaanKegudang_farmasi;
var setLookUps_perencanaanKegudang_farmasi;
var selectSetUnit;
var cellSelecteddeskripsiRWI;
var tanggal = now_perencanaanKegudang_farmasi.format("d/M/Y");
var jam = now_perencanaanKegudang_farmasi.format("H/i/s");
var tmpkriteria;
var gridDTLKegudang_farmasi_Kegudang_farmasi;
var GridDataView_perencanaanKegudang_farmasi;
var statposting;
var reqnumber;
var dsgridcombomilik_perencanaanKegudang_farmasi;
var dsgridcombovendor_perencanaanKegudang_farmasi;

var CurrentKegudang_farmasi =
{
    data: Object,
    details: Array,
    row: 0
};

var CurrentData_perencanaanKegudang_farmasi =
{
	data: Object,
	details: Array,
	row: 0
};

var CurrentDataobat_perencanaanKegudang_farmasi =
{
	data: Object,
	details: Array,
	row: 0
};


CurrentPage.page = dataGrid_perencanaanKegudang_farmasi(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

var Kegudang_farmasi={};
Kegudang_farmasi.form={};
Kegudang_farmasi.func={};
Kegudang_farmasi.vars={};
Kegudang_farmasi.func.parent=Kegudang_farmasi;
Kegudang_farmasi.form.ArrayStore={};
Kegudang_farmasi.form.ComboBox={};
Kegudang_farmasi.form.DataStore={};
Kegudang_farmasi.form.Record={};
Kegudang_farmasi.form.Form={};
Kegudang_farmasi.form.Grid={};
Kegudang_farmasi.form.Panel={};
Kegudang_farmasi.form.TextField={};
Kegudang_farmasi.form.Button={};

Kegudang_farmasi.form.ArrayStore.obat=new Ext.data.ArrayStore({
		id: 0,
		fields: [	
					'kd_prd','nama','tgl_masuk','jam_masuk','no_kamar','nama_kamar'
				],
		data: []
	});
	
Kegudang_farmasi.form.ArrayStore.waktu	= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_waktu','waktu'],
	data: []
});

Kegudang_farmasi.form.ArrayStore.jenisKegudang_farmasi	= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_jenis','jenis_Kegudang_farmasi','harga_pokok'],
	data: []
});

function dataGrid_perencanaanKegudang_farmasi(mod_id_perencanaanKegudang_farmasi){	
    var FieldMaster_perencanaanKegudang_farmasi =[
		'req_number', 'kd_milik', 'nm_unit_far','kd_unit_far', 'req_date', 'milik'
	];
    dataSource_perencanaanKegudang_farmasi = new WebApp.DataStore({
        fields: FieldMaster_perencanaanKegudang_farmasi
    });
    dataGriAwal();
	GridDataView_perencanaanKegudang_farmasi = new Ext.grid.EditorGridPanel({
		xtype: 'editorgrid',
		flex:2,
		store: dataSource_perencanaanKegudang_farmasi,
		autoScroll: true,
		columnLines: true,
		plugins: [new Ext.ux.grid.FilterRow()],
		selModel: new Ext.grid.RowSelectionModel({
			singleSelect: true,
			listeners:{
				rowselect: function(sm, row, rec){
					rowSelected_perencanaanKegudang_farmasi = undefined;
					rowSelected_perencanaanKegudang_farmasi = dataSource_perencanaanKegudang_farmasi.getAt(row);
					CurrentData_perencanaanKegudang_farmasi
					CurrentData_perencanaanKegudang_farmasi.row = row;
					CurrentData_perencanaanKegudang_farmasi.data = rowSelected_perencanaanKegudang_farmasi.data;
					statposting =rowSelected_perencanaanKegudang_farmasi.data.qty_ordered;
					reqnumber =rowSelected_perencanaanKegudang_farmasi.data.req_number;
				}
			}
		}),
		listeners:{
			rowdblclick: function (sm, ridx, cidx){
				rowSelected_perencanaanKegudang_farmasi = dataSource_perencanaanKegudang_farmasi.getAt(ridx);
				if (rowSelected_perencanaanKegudang_farmasi != undefined){	
					if (rowSelected_perencanaanKegudang_farmasi.data.qty_ordered===1){
						ShowPesanWarningKegudang_farmasi('Nomor perencanaan  ini telah dilayani', 'Perhatian');
					}else{
						setLookUp_perencanaanKegudang_farmasi(rowSelected_perencanaanKegudang_farmasi.data);
					}
				}
			}
		},
		colModel: new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(),
			{
				header		: 'Dilayani',
				width		: 30,
				sortable	: false,
				hideable	: true,
				hidden		: false,
				menuDisabled: true,
				dataIndex	: 'qty_ordered',
				renderer	: function(value, metaData, record, rowIndex, colIndex, store){
					 switch (value){
						 case 1:
							 metaData.css = 'StatusHijau'; 
							 break;
						 case 0:
							 metaData.css = 'StatusMerah';
							 break;
					 }
					 return '';
				}
			},{
				header: 'No. perencanaan',
				dataIndex: 'req_number',
				sortable: true,
				width: 35
				
			},{
				header: 'Unit farmasi',
				dataIndex: 'nm_unit_far',
				sortable: true,
				width: 35
			},{
				header:'Tgl Perencanaan',
				dataIndex: 'req_date',						
				width: 30,
				sortable: true,
				hideable:false,
				menuDisabled:true,
				renderer: function(v, params, record)
				{
				return ShowDate(record.data.req_date);
				}
			},{
				header: 'Milik',
				dataIndex: 'milik',
				sortable: true,
				width: 50
			},{
				header: 'kd_milik',
				dataIndex: 'kd_milik',
				sortable: true,
				width: 40,
				hidden:true
			},{
				header: 'kd_unit_far',
				dataIndex: 'kd_unit_far',
				sortable: true,
				width: 40,
				hidden:true
			}
		]),
		tbar:[
			{
				text: 'Tambah perencanaan ',
				iconCls: 'Edit_Tr',
				tooltip: 'Add Data',
				id: 'btnTambah_perencanaanKegudang_farmasi',
				handler: function(sm, row, rec){
					setLookUp_perencanaanKegudang_farmasi();								
				}
			},'-',{
				text: 'Hapus perencanaan',
				iconCls: 'remove',
				tooltip: 'Hapus perencanaan',
				id: 'btnDeleteTrans_perencanaanKegudang_farmasi',
				handler: function(sm, row, rec){
					if(statposting == 't'){
						ShowPesanWarningKegudang_farmasi('Perencanaan ini sudah dilayani, tidak dapat di Hapus!','Warning')
					}else{
						var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan penghapusan perencanaan :', function (btn, combo) {
							if (btn == 'ok'){
								var alasanhapusKegudang_farmasi = combo;
								if (alasanhapusKegudang_farmasi != ''){
									Ext.Ajax.request({
										url: baseURL + "index.php/gudang_farmasi/functionperencanaan/deletetrans",
										params:{
											req_number:reqnumber,
											alasan:alasanhapusKegudang_farmasi
										} ,
										failure: function(o){
											ShowPesanErrorKegudang_farmasi('Hubungi Admin', 'Error');
										},	
										success: function(o){
											var cst = Ext.decode(o.responseText);
											if (cst.success === true){
												dataSource_perencanaanKegudang_farmasi.removeAll();
												dataGriAwal();
												ShowPesanInfoKegudang_farmasi('Berhasil menghapus perencanaan', 'Information');
											}else{
												ShowPesanErrorKegudang_farmasi('Gagal menghapus perencanaan', 'Error');
											}
										}
									});
								}else{
									ShowPesanErrorKegudang_farmasi('Silahkan isi alasan terlebih dahulu', 'Keterangan');
								}
							}	
						});
					}
				}
			},
			/* {
				xtype: 'button',
				text: 'Edit Data',
				iconCls: 'Edit_Tr',
				tooltip: 'Edit Data',
				id: 'btnEdit_perencanaanKegudang_farmasi',
				handler: function(sm, row, rec)
				{
					if (rowSelected_perencanaanKegudang_farmasi != undefined)
					{
						setLookUp_perencanaanKegudang_farmasi(rowSelected_perencanaanKegudang_farmasi.data)
					} else{
						ShowPesanWarningKegudang_farmasi('Pilih data yang akan di edit','Warning');
					}
					
				}
			} */
			//-------------- ## --------------
		],
		viewConfig:{
			forceFit: true
		}
	});
	
	var pencarianKegudang_farmasi = new Ext.FormPanel({
		style:'padding: 5px;',
		border:false,
		layout:'form',
		labelWidth:120,
        items: [
			{
				xtype: 'textfield',
				id: 'TxtFilter_perencanaanKegudang_farmasi',
				name: 'TxtFilter_perencanaanKegudang_farmasi',
				fieldLabel:'No. perencanaan',
				listeners:{ 
					'specialkey' : function(){
						if (Ext.EventObject.getKey() === 13) {
							dataGriAwal(Ext.getCmp('TxtFilter_perencanaanKegudang_farmasi').getValue(),
									Ext.getCmp('dateFilter_perencanaanKegudang_farmasi').getValue()
							);
						} 						
					}
				}
			},{
				layout:'column',
				border:false,
				anchor:'100%',
				items:[
					{
						xtype:'displayfield',
						value:'Tgl. Perencanaan :',
						width: 125,
					},{
						xtype: 'datefield',
						id: 'dateFilter_perencanaanKegudang_farmasi',
						name: 'dateFilter_perencanaanKegudang_farmasi',
						format: 'd/M/Y',
						value:now_perencanaanKegudang_farmasi,
						width: 100,
						// tabIndex:1,
						listeners:{ 
							'specialkey' : function(){
								if (Ext.EventObject.getKey() === 13){
									dataGriAwal(Ext.getCmp('TxtFilter_perencanaanKegudang_farmasi').getValue(),
										Ext.getCmp('dateFilter_perencanaanKegudang_farmasi').getValue(),
										Ext.getCmp('dateFilter2_perencanaanKegudang_farmasi').getValue()
									);
								} 						
							}
						}
					},{
						xtype:'displayfield',
						value:'&nbsp; - &nbsp;',
					},{
						xtype: 'datefield',
						id: 'dateFilter2_perencanaanKegudang_farmasi',
						name: 'dateFilter2_perencanaanKegudang_farmasi',
						format: 'd/M/Y',
						value:now_perencanaanKegudang_farmasi,
						width: 100,
						// tabIndex:1,
						listeners:{ 
							'specialkey' : function(){
								if (Ext.EventObject.getKey() === 13){
								dataGriAwal(Ext.getCmp('TxtFilter_perencanaanKegudang_farmasi').getValue(),
									Ext.getCmp('dateFilter_perencanaanKegudang_farmasi').getValue(),
									Ext.getCmp('dateFilter2_perencanaanKegudang_farmasi').getValue());
								} 						
							}
						}
					},{
						xtype: 'button',
						text: 'Cari',
						iconCls: 'refresh',
						tooltip: 'Cari',
						style:{paddingLeft:'10px'},
						width:150,
						id: 'BtnFilterGridDataView_perencanaanKegudang_farmasi',
						handler: function(){	
							dataGriAwal(Ext.getCmp('TxtFilter_perencanaanKegudang_farmasi').getValue(),
								Ext.getCmp('dateFilter_perencanaanKegudang_farmasi').getValue(),
								Ext.getCmp('dateFilter2_perencanaanKegudang_farmasi').getValue()
							);						
						}                        
					}
				]
			}
		]	
	});
    var FrmFilterGridDataView_perencanaanKegudang_farmasi = new Ext.Panel({
		title: NamaForm_perencanaanKegudang_farmasi,
		iconCls: 'Studi_Lanjut',
		id: mod_id_perencanaanKegudang_farmasi,
		closable: true,   
		layout:{
			type:'vbox',
			align:'stretch'
		},
		border: false,  
		items: [ 
			pencarianKegudang_farmasi,
			GridDataView_perencanaanKegudang_farmasi
		]
   });
    return FrmFilterGridDataView_perencanaanKegudang_farmasi;
}

function setLookUp_perencanaanKegudang_farmasi(rowdata){
    var lebar = 985;
    setLookUps_perencanaanKegudang_farmasi = new Ext.Window({
        id: Nci.getId(),
        title: NamaForm_perencanaanKegudang_farmasi, 
        closeAction: 'destroy',        
        width: 900,
        height: 600,
		layout:'fit',
        resizable:false,
		autoScroll: false,
        border: true,
        constrain : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_perencanaanKegudang_farmasi(lebar,rowdata),
        listeners:{
            activate: function(){
				
            },
            afterShow: function(){
                this.activate();
            },
            deactivate: function(){
                rowSelected_perencanaanKegudang_farmasi=undefined;
            }
        }
    });

    setLookUps_perencanaanKegudang_farmasi.show();
	getListPerencanaan()
    if (rowdata == undefined){	
    }
    else
    {
        datainit_perencanaanKegudang_farmasi(rowdata);
    }
	shortcut.set({
		code:'lookup',
		list:[
			{
				key:'ctrl+s',
				fn:function(){
					Ext.getCmp('btnSave_perencanaanKegudang_farmasi').el.dom.click();
				}
			},
			{
				key:'ctrl+d',
				fn:function(){
					Ext.getCmp('btnDelete_perencanaanKegudang_farmasi').el.dom.click();
				}
			},
			{
				key:'esc',
				fn:function(){
					setLookUps_perencanaanKegudang_farmasi.close();
				}
			}
		]
	});
}

function getFormItemEntry_perencanaanKegudang_farmasi(lebar,rowdata){
    var pnlFormDataBasic_perencanaanKegudang_farmasi = new Ext.Panel({
		// title: '',
		// region: 'north',
		// layout: 'form',
		// bodyStyle: 'padding:10px 10px 10px 10px',
		// anchor: '100%',
		// labelWidth: 1,
		// autoWidth: true,
		// width: lebar,
		layout:{
			type:'vbox',
			align:'stretch'
		},
		border: false,
		items:[
			getItemPanelInputperencanaan_perencanaanKegudang_farmasi(lebar),
			getItemGridobat_perencanaanKegudang_farmasi(lebar),
		],
		tbar: [
			{
				xtype: 'button',
				text: 'Add New',
				iconCls: 'add',
				id: 'btnAdd_perencanaanKegudang_farmasi',
				handler: function(){
					dataAddNew_perencanaanKegudang_farmasi();
				}
			},'-',{
				xtype: 'button',
				text: 'Save',
				iconCls: 'save',
				id: 'btnSave_perencanaanKegudang_farmasi',
				handler: function(){
					save_perencanaan();//dataAddNew_perencanaanKegudang_farmasi();
				}
			},'-'
		]
	});
    return pnlFormDataBasic_perencanaanKegudang_farmasi;
}

function getItemPanelInputperencanaan_perencanaanKegudang_farmasi(lebar) {
    var items ={
		layout:'form',
		border: false,
		bodyStyle:'padding: 5px;',
		items:[
			{
				layout: 'column',
				border: false,
				items:[
					{
						columnWidth:.5,
						layout: 'form',
						border: false,
						labelWidth:120,
						items:[
							{
								xtype: 'textfield',
								fieldLabel:'No. perencanaan',
								id: 'txtNoperencanaanKegudang_farmasi',
								name: 'txtNoperencanaanKegudang_farmasi',
								readOnly:true,
								listeners:{ 
									'specialkey' : function(){
										if (Ext.EventObject.getKey() === 13){
										} 						
									}
								}
							},{
								xtype: 'datefield',
								fieldLabel:'Tgl. Perencanaan',
								id: 'dfTglPerencanaanKegudang_farmasi',
								format: 'd/M/Y',
								width: 100,
								tabIndex:2,
								readOnly:true,
								value:now_perencanaanKegudang_farmasi,
								listeners:{ 
									'specialkey' : function(){
										if (Ext.EventObject.getKey() === 13) {
										} 						
									}
								}
							},	
						]
					},{
						columnWidth:.5,
						layout: 'form',
						border: false,
						labelWidth:120,
						items:[
							{
								fieldLabel:'Keterangan',
								xtype: 'textarea',
								id: 'Keterangan_Kegudang_farmasi',
								name: 'Keterangan_Kegudang_farmasi',
								width: 300,
								tabIndex:3,
								listeners:{ 
									'specialkey' : function(){
										if (Ext.EventObject.getKey() === 9){
											var records = new Array();
											records.push(new dsDataGrdobat_perencanaanKegudang_farmasi.recordType());
											dsDataGrdobat_perencanaanKegudang_farmasi.add(records);
											row=dsDataGrdobat_perencanaanKegudang_farmasi.getCount()-1; // menujukan jumlah baris,startEditing(baris dimulai dari 1, kolom 1)
											Kegudang_farmasi.form.Grid.perencanaan.startEditing(row, 2);	
										} 						
									}
								}
							}
						]
					}
				]
			}
		]		
	};
    return items;
};

function getItemGridobat_perencanaanKegudang_farmasi(lebar){
    var items ={
	    border: true,
		layout:'fit',
		flex:1,
	    tbar:[
			{
				text	: 'Add obat',
				id		: 'btnAddobatKegudang_farmasiL',
				tooltip	: nmLookup,
				disabled: false,
				iconCls	: 'find',
				handler	: function(){
					var records = new Array();
					records.push(new dsDataGrdobat_perencanaanKegudang_farmasi.recordType());
					dsDataGrdobat_perencanaanKegudang_farmasi.add(records);
					row=dsDataGrdobat_perencanaanKegudang_farmasi.getCount()-1; // menujukan jumlah baris,startEditing(baris dimulai dari 1, kolom 1)
					Kegudang_farmasi.form.Grid.perencanaan.startEditing(row, 2);	
				}
			},'-',{
				xtype: 'button',
				text: 'Delete',
				iconCls: 'remove',
				id: 'btnDelete_perencanaanKegudang_farmasi',
				handler: function(){
					var line = Kegudang_farmasi.form.Grid.perencanaan.getSelectionModel().selection.cell[0];
					var o = dsDataGrdobat_perencanaanKegudang_farmasi.getRange()[line].data;
					Ext.Msg.confirm('Warning', 'Apakah obat ini akan dihapus?', function(button){
						if (button == 'yes'){
							if(dsDataGrdobat_perencanaanKegudang_farmasi.getRange()[line].data.kd_prd != undefined){
								Ext.Ajax.request({
									url: baseURL + "index.php/gudang_farmasi/functionperencanaan/delete",
									params:{
										line:o.req_line,
										no_penerimaan: Ext.getCmp('txtNoperencanaanKegudang_farmasi').getValue(),
									} ,
									failure: function(o){
										ShowPesanErrorKegudang_farmasi('Error, delete obat! Hubungi Admin', 'Error');
									},	
									success: function(o){
										var cst = Ext.decode(o.responseText);
										if (cst.success === true){
											dsDataGrdobat_perencanaanKegudang_farmasi.removeAt(line);
											Kegudang_farmasi.form.Grid.perencanaan.getView().refresh();
											//Ext.getCmp('btnAddDetKegudang_farmasiKegudang_farmasiL').enable();
											ShowPesanInfoKegudang_farmasi('Berhasil menghapus data obat ini', 'Information');
											
										}else{
											ShowPesanErrorKegudang_farmasi('Gagal menghapus data obat ini', 'Error');
										}
									}
								});
							}else{
								dsDataGrdobat_perencanaanKegudang_farmasi.removeAt(line);
								Kegudang_farmasi.form.Grid.perencanaan.getView().refresh();
							}
						} 
						
					});
				}
			}	
		],
		items:[
			gridDataViewEdit_perencanaanKegudang_farmasi()
		]
	};
    return items;
};


//var a={};
function gridDataViewEdit_perencanaanKegudang_farmasi(){
    var FieldGrdKasir_perencanaanKegudang_farmasi = ['kd_prd'];
    dsDataGrdobat_perencanaanKegudang_farmasi= new WebApp.DataStore({
        fields: FieldGrdKasir_perencanaanKegudang_farmasi
    });
	var Fieldmilik = ['kd_milik','milik'];
    dsgridcombomilik_perencanaanKegudang_farmasi = new WebApp.DataStore({ fields: Fieldmilik });
	var Fieldvendor = ['kd_vendor','vendor'];
    dsgridcombovendor_perencanaanKegudang_farmasi = new WebApp.DataStore({ fields: Fieldvendor });
    
	loaddatagridkepemilikan_perencanaanKegudang_farmasi();
	loaddatagridvendor_perencanaanKegudang_farmasi();
	
    Kegudang_farmasi.form.Grid.perencanaan =new Ext.grid.EditorGridPanel({
        store: dsDataGrdobat_perencanaanKegudang_farmasi,
        columnLines: true,
		selModel: new Ext.grid.CellSelectionModel ({
            singleSelect: true,
            listeners:{
                cellselect: function(sm, row, rec){
					rowSelectedGridobat_perencanaanKegudang_farmasi = undefined;
					rowSelectedGridobat_perencanaanKegudang_farmasi = dsDataGrdobat_perencanaanKegudang_farmasi.getAt(row);
					CurrentDataobat_perencanaanKegudang_farmasi
					CurrentDataobat_perencanaanKegudang_farmasi.row = row;
					CurrentDataobat_perencanaanKegudang_farmasi.data = rowSelectedGridobat_perencanaanKegudang_farmasi.data;
					
                },
            }
        }),
        stripeRows: true,
		border:false,
		autoScroll:true,
        cm:new Ext.grid.ColumnModel([
        	new Ext.grid.RowNumberer(),	
			{
				dataIndex: 'kd_prd',
				header: 'Kode Prd',
				sortable: true,
				width: 70
			},{			
				dataIndex: 'nama_obat',
				header: 'Nama obat',
				sortable: true,
				width: 200,
				editor:new Nci.form.Combobox.autoComplete({
					store	: Kegudang_farmasi.form.ArrayStore.obat,
					select	: function(a,b,c){
						var line	= Kegudang_farmasi.form.Grid.perencanaan.getSelectionModel().selection.cell[0];
						if(dsDataGrdobat_perencanaanKegudang_farmasi.getCount()-1===0){
							dsDataGrdobat_perencanaanKegudang_farmasi.getRange()[line].data.kd_prd=b.data.kd_prd;
							dsDataGrdobat_perencanaanKegudang_farmasi.getRange()[line].data.qty=1;
							dsDataGrdobat_perencanaanKegudang_farmasi.getRange()[line].data.nama_obat=b.data.nama_obat;
							dsDataGrdobat_perencanaanKegudang_farmasi.getRange()[line].data.kd_milik=b.data.kd_milik;
							if(b.data.jml_pengeluaran == null || b.data.jml_pengeluaran==0){
								dsDataGrdobat_perencanaanKegudang_farmasi.getRange()[line].data.jml_pengeluaran=0;
							} else{
								dsDataGrdobat_perencanaanKegudang_farmasi.getRange()[line].data.jml_pengeluaran=b.data.jml_pengeluaran;
							}
							dsDataGrdobat_perencanaanKegudang_farmasi.getRange()[line].data.jml_stok_apt=b.data.jml_stok_apt;
						}else{
							var VALUE_x=1;
							for(var i = 0 ; i < dsDataGrdobat_perencanaanKegudang_farmasi.getCount();i++){
								if(dsDataGrdobat_perencanaanKegudang_farmasi.getRange()[i].data.kd_prd===b.data.kd_prd){
									VALUE_x=0;
								}
							}
							if(VALUE_x===0){
								ShowPesanWarningKegudang_farmasi('Anda telah memilih Obat yang sama', 'Perhatian');
								dsDataGrdobat_perencanaanKegudang_farmasi.removeAt(line);
							}else{
								dsDataGrdobat_perencanaanKegudang_farmasi.getRange()[line].data.kd_prd=b.data.kd_prd;
								dsDataGrdobat_perencanaanKegudang_farmasi.getRange()[line].data.qty=1;
								dsDataGrdobat_perencanaanKegudang_farmasi.getRange()[line].data.nama_obat=b.data.nama_obat;
								dsDataGrdobat_perencanaanKegudang_farmasi.getRange()[line].data.kd_milik=b.data.kd_milik;
								if(b.data.jml_pengeluaran == null || b.data.jml_pengeluaran==0){
									dsDataGrdobat_perencanaanKegudang_farmasi.getRange()[line].data.jml_pengeluaran=0;
								} else{
									dsDataGrdobat_perencanaanKegudang_farmasi.getRange()[line].data.jml_pengeluaran=b.data.jml_pengeluaran;
								}
								dsDataGrdobat_perencanaanKegudang_farmasi.getRange()[line].data.jml_stok_apt=b.data.jml_stok_apt;
							}
						}
						Kegudang_farmasi.form.Grid.perencanaan.getView().refresh();	
						Kegudang_farmasi.form.Grid.perencanaan.startEditing(line, 5);	
					},
					insert	: function(o){
						return {
							jml_stok_apt	: o.jml_stok_apt,
							nama_obat		: o.nama_obat,
							jml_pengeluaran	: o.jml_pengeluaran,
							text			:  '<table style="font-size: 11px;"><tr><td width="250">'+o.nama_obat+'</td><td width="50">'+o.satuan+'</td></tr></table>',
							kd_prd			: o.kd_prd,
							kd_milik		: o.kd_milik,
							satuan			: o.satuan,
						}
					},
					param:function(){
						return {
						/* 		kd_milikLama:Kegudang_farmasi.vars.kd_milik,
								kd_milikNew:Ext.getCmp('cbo_UnitKegudang_farmasiLookup').getValue()
 */						}
					},
					url		: baseURL + "index.php/gudang_farmasi/functionperencanaan/getstock_obat",
					valueField: 'nama_obat',
					displayField: 'text',
					listWidth: 320
				})
			},{
				dataIndex: 'jml_stok_apt',
				header: 'Stok tersedia',
				sortable: true,
				width: 70,
				align:'right'
			},{
				dataIndex: 'jml_pengeluaran',
				header: 'Jml Keluar',
				sortable: true,
				width: 70,
				align:'right'
			},{
				dataIndex: 'qty',
				header: 'Jml Rencana',
				sortable: true,
				width: 70,
				align:'right',
				editor: new Ext.form.NumberField({
                    allowBlank: true,
                    enableKeyEvents : true,
                    width:30,
					listeners:{
						specialkey: function(){
							if(Ext.EventObject.getKey() == 13){
								var line	= Kegudang_farmasi.form.Grid.perencanaan.getSelectionModel().selection.cell[0];
								Kegudang_farmasi.form.Grid.perencanaan.startEditing(line, 6);
							}
						}
					}
                })
			},{
				header		: 'Kepemilikan',
				width		: 80,
				menuDisabled: true,
				dataIndex	: 'milik',
				editor		: gridcbokepemilikan_perencanaanKegudang_farmasi= new Ext.form.ComboBox({
					id				: 'gridcbokepemilikan_perencanaanKegudang_farmasi',
					typeAhead		: true,
					triggerAction	: 'all',
					lazyRender		: true,
					mode			: 'local',
					emptyText		: '',
					fieldLabel		: ' ',
					align			: 'Right',
					width			: 200,
					store			: dsgridcombomilik_perencanaanKegudang_farmasi,
					valueField		: 'milik',
					displayField	: 'milik',
					listeners		:{
						select	: function(a,b,c){
							var line	= Kegudang_farmasi.form.Grid.perencanaan.getSelectionModel().selection.cell[0];
							dsDataGrdobat_perencanaanKegudang_farmasi.getRange()[line].data.kd_milik=b.data.kd_milik;
							/* var records = new Array();
							records.push(new dsDataGrdobat_perencanaanKegudang_farmasi.recordType());
							dsDataGrdobat_perencanaanKegudang_farmasi.add(records);
							row=dsDataGrdobat_perencanaanKegudang_farmasi.getCount()-1; // menujukan jumlah baris,startEditing(baris dimulai dari 1, kolom 1)
							Kegudang_farmasi.form.Grid.perencanaan.startEditing(row, 2); */
						},
						keyUp: function(a,b,c){
							$this1=this;
							if((b.getKey()>=65 && b.getKey()<=90)|| (b.getKey()>=48 && b.getKey()<=57) || b.getKey()==32 || b.getKey()==8 || b.getKey()==46){
								clearTimeout(this.time);

								this.time=setTimeout(function(){
									if($this1.lastQuery != '' ){
										var value=$this1.lastQuery;
										var param={};
										param['text']=$this1.lastQuery;
										loaddatagridkepemilikan_perencanaanKegudang_farmasi(param);
									}
								},1000);
							}
						},
						specialkey: function(){
							if(Ext.EventObject.getKey() == 13){
								// var records = new Array();
								// records.push(new dsDataGrdobat_perencanaanKegudang_farmasi.recordType());
								// dsDataGrdobat_perencanaanKegudang_farmasi.add(records);
								var line	= Kegudang_farmasi.form.Grid.perencanaan.getSelectionModel().selection.cell[0];
								Kegudang_farmasi.form.Grid.perencanaan.startEditing(line, 7);
								
								// var nextRow = dsDataGrdobat_perencanaanKegudang_farmasi.getCount()-1; 
								// Kegudang_farmasi.form.Grid.perencanaan.startEditing(nextRow, 2);
								
							}
						},
					}
				})
			},{
				header		: 'Vendor',
				width		: 100,
				menuDisabled: true,
				dataIndex	: 'vendor',
				editor		: gridcbovendor_perencanaanKegudang_farmasi= new Ext.form.ComboBox({
					id				: 'gridcbovendor_perencanaanKegudang_farmasi',
					typeAhead		: true,
					triggerAction	: 'all',
					lazyRender		: true,
					mode			: 'local',
					emptyText		: '',
					fieldLabel		: ' ',
					align			: 'Right',
					width			: 200,
					store			: dsgridcombovendor_perencanaanKegudang_farmasi,
					valueField		: 'vendor',
					displayField	: 'vendor',
					listeners		:{
						select	: function(a,b,c){
							var line	= Kegudang_farmasi.form.Grid.perencanaan.getSelectionModel().selection.cell[0];
							dsDataGrdobat_perencanaanKegudang_farmasi.getRange()[line].data.kd_vendor=b.data.kd_vendor;
							/* var records = new Array();
							records.push(new dsDataGrdobat_perencanaanKegudang_farmasi.recordType());
							dsDataGrdobat_perencanaanKegudang_farmasi.add(records);
							row=dsDataGrdobat_perencanaanKegudang_farmasi.getCount()-1; // menujukan jumlah baris,startEditing(baris dimulai dari 1, kolom 1)
							Kegudang_farmasi.form.Grid.perencanaan.startEditing(row, 2); */
						},
						keyUp: function(a,b,c){
							$this1=this;
							if((b.getKey()>=65 && b.getKey()<=90)|| (b.getKey()>=48 && b.getKey()<=57) || b.getKey()==32 || b.getKey()==8 || b.getKey()==46){
								clearTimeout(this.time);

								this.time=setTimeout(function(){
									if($this1.lastQuery != '' ){
										var value=$this1.lastQuery;
										var param={};
										param['text']=$this1.lastQuery;
										loaddatagridkevendor_perencanaanKegudang_farmasi(param);
									}
								},1000);
							}
						},
						specialkey: function(){
							if(Ext.EventObject.getKey() == 13){
								var records = new Array();
								records.push(new dsDataGrdobat_perencanaanKegudang_farmasi.recordType());
								dsDataGrdobat_perencanaanKegudang_farmasi.add(records);
								var nextRow = dsDataGrdobat_perencanaanKegudang_farmasi.getCount()-1; 
								Kegudang_farmasi.form.Grid.perencanaan.startEditing(nextRow, 2);
								
							}
						},
					}
				})
			},{
				dataIndex: 'kd_milik',
				header: 'kd_milik',
				sortable: true,
				width: 65,
				align:'center',
				hidden:true
			},{
				dataIndex: 'kd_vendor',
				header: 'kd_vendor',
				sortable: true,
				width: 65,
				align:'center',
				hidden:true
			},{
				dataIndex: 'req_line',
				header: 'line',
				hidden:true,
				sortable: true,
				width: 65,
				align:'center'
			}
        ]),
		// viewConfig:{
			// forceFit: true
		// }
    });
    return Kegudang_farmasi.form.Grid.perencanaan;
}


function datainit_perencanaanKegudang_farmasi(rowdata)
{
	Ext.getCmp('txtNoperencanaanKegudang_farmasi').setValue(rowdata.req_number);
	Ext.getCmp('dfTglPerencanaanKegudang_farmasi').setValue(ShowDate(rowdata.req_date));
	Ext.getCmp('Keterangan_Kegudang_farmasi').setValue(rowdata.remark);
	
	getGrid_detail_min(rowdata.req_number);
};

function getGrid_detail_min(req_number){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gudang_farmasi/functionperencanaan/getDataGrid_detail",
			params: {nominta:req_number},
			failure: function(o)
			{loadMask.hide();
				ShowPesanErrorKegudang_farmasi('Error, obat! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
				dsDataGrdobat_perencanaanKegudang_farmasi.removeAll();
					var recs=[],
						recType=dsDataGrdobat_perencanaanKegudang_farmasi.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
					dsDataGrdobat_perencanaanKegudang_farmasi.add(recs);
					
					Kegudang_farmasi.form.Grid.perencanaan.getView().refresh();
				}
				else 
				{
					ShowPesanErrorKegudang_farmasi('Gagal membaca data obat ini', 'Error');
				};
			}
		}
		
	)
	
}

function dataGriAwal(nominta,req_date,req_date2){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gudang_farmasi/functionperencanaan/getDataGridAwal",
			params: {
				no_minta:nominta,
				req_date:req_date,
				req_date2:req_date2
			
			},
			failure: function(o)
			{ loadMask.hide();
				ShowPesanErrorKegudang_farmasi('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dataSource_perencanaanKegudang_farmasi.removeAll();
					var recs=[],
						recType=dataSource_perencanaanKegudang_farmasi.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
						dataSource_perencanaanKegudang_farmasi.add(recs);
					
					
					
					GridDataView_perencanaanKegudang_farmasi.getView().refresh();
				}
				else 
				{
					ShowPesanErrorKegudang_farmasi('Gagal membaca data obat', 'Error');
				};
			}
		}
		
	)
	
}

function ValidasiEntryperencanaanKegudang_farmasi(modul,mBolHapus)
{
	var x = 1;
	if(dsDataGrdobat_perencanaanKegudang_farmasi.getCount() == 0){
		ShowPesanWarningKegudang_farmasi('Daftar obat tidak boleh kosong! Minimal 1 obat','Warning')
		x=0;
	} else{
		for(var i=0; i<dsDataGrdobat_perencanaanKegudang_farmasi.getCount() ; i++){
			var o=dsDataGrdobat_perencanaanKegudang_farmasi.getRange()[i].data;
			if(o.kd_prd == undefined || o.kd_prd ==""){
				dsDataGrdobat_perencanaanKegudang_farmasi.removeAt(i);
				Kegudang_farmasi.form.Grid.perencanaan.getView().refresh();
				i--;
			}
		}
		if(dsDataGrdobat_perencanaanKegudang_farmasi.getCount()==0){
			ShowPesanWarningKegudang_farmasi('Tidak Ada Obat!!','Warning')
			x=0;
		}
	}
	return x;
};

function dataAddNew_perencanaanKegudang_farmasi(){
	Ext.getCmp('txtNoperencanaanKegudang_farmasi').setValue('');
	Ext.getCmp('dfTglPerencanaanKegudang_farmasi').setValue(now_perencanaanKegudang_farmasi);
	dsDataGrdobat_perencanaanKegudang_farmasi.removeAll();
	
	Ext.getCmp('txtNoperencanaanKegudang_farmasi').enable();
	Ext.getCmp('dfTglPerencanaanKegudang_farmasi').enable();
	
}


function ShowPesanWarningKegudang_farmasi(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:290
		}
	);
};

function ShowPesanErrorKegudang_farmasi(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:290
		}
	);
};


function ShowPesanInfoKegudang_farmasi(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:290
		}
	);
};


function save_perencanaan(mBol){	
if (ValidasiEntryperencanaanKegudang_farmasi(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request({
			url			: baseURL + "index.php/gudang_farmasi/functionperencanaan/insert_apt_request",
			params		: getParamperencanaan(),
			failure		: function(o){
			dataGriAwal(Ext.getCmp('TxtFilter_perencanaanKegudang_farmasi').getValue(),
									Ext.getCmp('dateFilter_perencanaanKegudang_farmasi').getValue(),
									Ext.getCmp('dateFilter2_perencanaanKegudang_farmasi').getValue()
									);
			ShowPesanErrorKegudang_farmasi('Insert error hubungi Admin Admin', 'Error');
			},
			success		: function(o){
				dataGriAwal(Ext.getCmp('TxtFilter_perencanaanKegudang_farmasi').getValue(),
				Ext.getCmp('dateFilter_perencanaanKegudang_farmasi').getValue(),
				Ext.getCmp('dateFilter2_perencanaanKegudang_farmasi').getValue()
				);
				var cst = Ext.decode(o.responseText);
				if(cst.login_error===true){
				ShowPesanWarningKegudang_farmasi('durasi login Habis Harap login ulang jika tidak bisa hubungi admin', 'Perhatian');
				}
				if(cst.simpan===false){
				ShowPesanWarningKegudang_farmasi('ro unit gagal dimasukan ke database', 'Perhatian');
				}
				if(cst.simpan_det===false){
				ShowPesanWarningKegudang_farmasi('ro unit detail gagal dimasukan ke database', 'Perhatian');
				}
				if(cst.simpan_det===true){
				ShowPesanInfoKegudang_farmasi('data berhasil disimpan', 'Simpan');
				Ext.getCmp('txtNoperencanaanKegudang_farmasi').setValue(cst.no_minta);
				getGrid_detail_min(cst.no_minta);
				}
			}
			
		});
	}
}

function getParamperencanaan(){
	var params={
	no_minta 	 :Ext.getCmp('txtNoperencanaanKegudang_farmasi').getValue(),
	tgl_minta 	 :Ext.getCmp('dfTglPerencanaanKegudang_farmasi').getValue(),
	jumlah_detil :dsDataGrdobat_perencanaanKegudang_farmasi.getCount(),
	keterangan	 : Ext.getCmp('Keterangan_Kegudang_farmasi').getValue(),
	};
					
	
	for(var i=0, iLen= dsDataGrdobat_perencanaanKegudang_farmasi.getCount(); i<iLen;i++){
    	var o=dsDataGrdobat_perencanaanKegudang_farmasi.getRange()[i].data;
    	params['kd_prd'+i]	= o.kd_prd;
		params['qty'+i]	= o.qty;
    	params['kd_milik'+i]	= o.kd_milik;
    	params['kd_vendor'+i]	= o.kd_vendor;
		params['line'+i]	= o.req_line;
    }
    return params;
}


function loaddatagridkepemilikan_perencanaanKegudang_farmasi(param)
{
	if (param==='' || param===undefined)
	{
		param={
			text: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/gudang_farmasi/functionperencanaan/getKepemilikan",
		params: param,
		failure: function(o)
		{
			var cst = Ext.decode(o.responseText);
		},
		success: function(o) {
			dsgridcombomilik_perencanaanKegudang_farmasi.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsgridcombomilik_perencanaanKegudang_farmasi.recordType;
				var o=cst['listData'][i];

				recs.push(new recType(o));
				dsgridcombomilik_perencanaanKegudang_farmasi.add(recs);
				//console.log(o);
			}
		}
	});
}
function loaddatagridvendor_perencanaanKegudang_farmasi(param)
{
	if (param==='' || param===undefined)
	{
		param={
			text: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/gudang_farmasi/functionperencanaan/getVendor",
		params: param,
		failure: function(o)
		{
			var cst = Ext.decode(o.responseText);
		},
		success: function(o) {
			dsgridcombovendor_perencanaanKegudang_farmasi.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsgridcombovendor_perencanaanKegudang_farmasi.recordType;
				var o=cst['listData'][i];

				recs.push(new recType(o));
				dsgridcombovendor_perencanaanKegudang_farmasi.add(recs);
				//console.log(o);
			}
		}
	});
}


function getListPerencanaan(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/gudang_farmasi/functionperencanaan/getDataGrid_detail_obat_rencana",
			params: {
				query:""
			
			},
			failure: function(o)
			{ loadMask.hide();
				ShowPesanErrorKegudang_farmasi('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dsDataGrdobat_perencanaanKegudang_farmasi.removeAll();
					var recs=[],
						recType=dsDataGrdobat_perencanaanKegudang_farmasi.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
						dsDataGrdobat_perencanaanKegudang_farmasi.add(recs);
					
					
					
					Kegudang_farmasi.form.Grid.perencanaan.getView().refresh();
				}
				else 
				{
					ShowPesanErrorKegudang_farmasi('Gagal membaca data obat', 'Error');
				};
			}
		}
		
	)
	
} 


