var CurrentHasilHemodialisa =
{
    data: Object,
    details: Array,
    row: 0
};

var dsTRHasilHemodialisaList;
var grListHasilHemodialisa;
var AddNewDiagnosa = true;
var now = new Date();
var tigaharilalu = new Date().add(Date.DAY, -3);
var rowSelectedHasilHemodialisa;
var FormLookUpsdetailHasilHemodialisa;

var gridDTHasilHemodialisa_pradialisa;
var gridDTHasilHemodialisa_dialisa;
var gridDTHasilHemodialisa_pascadialisa;
var rowSelected_HDPradialisa;
var rowSelected_HDDialisa;
var rowSelected_HDPascadialisa;
var dsTRDetailHasilHemodialisa_pradialisa;
var dsTRDetailHasilHemodialisa_dialisa;
var dsTRDetailHasilHemodialisa_pascadialisa;

var tmpnama_unit_asal;
var tmpkd_dokter_asal;
var tmpjam_masuk;
var tmpnama_dokter_asal;

var CurrentData_HDPradialisa=
{
	data: Object,
	details: Array,
	row: 0
};
var CurrentData_HDDialisa=
{
	data: Object,
	details: Array,
	row: 0
};
var CurrentData_HDPascadialisa=
{
	data: Object,
	details: Array,
	row: 0
};

var SetupHasilHemodialisa={};
SetupHasilHemodialisa.form={};
SetupHasilHemodialisa.func={};
SetupHasilHemodialisa.vars={};
SetupHasilHemodialisa.func.parent=SetupHasilHemodialisa;
SetupHasilHemodialisa.form.ArrayStore={};
SetupHasilHemodialisa.form.ComboBox={};
SetupHasilHemodialisa.form.DataStore={};
SetupHasilHemodialisa.form.Record={};
SetupHasilHemodialisa.form.Form={};
SetupHasilHemodialisa.form.Grid={};
SetupHasilHemodialisa.form.Panel={};
SetupHasilHemodialisa.form.TextField={};
SetupHasilHemodialisa.form.Button={};

SetupHasilHemodialisa.form.ArrayStore.a = new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_pasien','tgl_masuk','jam_masuk','urut_masuk','urut_dia','no_dia','nama','tgl_lahir','jenis_kelamin','alamat','gol_darah','kd_dokter','nama_dokter','no_transaksi','kd_kasir','no_ref','deskripsi','nilai','no_urut',
			'kd_unit','kd_unit_asal','nama_unit','nama_unit_asal','no_transaksi_asal', 'kd_kasir_asal','kd_dokter_asal'],
	data: []
});

CurrentPage.page = getPanelHasilHD(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
//membuat form
function getPanelHasilHD(mod_id) 
{
    var Field = ['kd_pasien','tgl_masuk','jam_masuk','urut_masuk','urut_dia','no_dia','nama','tgl_lahir','jenis_kelamin','alamat','gol_darah','kd_dokter','nama_dokter','no_transaksi','kd_kasir','no_ref','deskripsi','nilai','no_urut',
				'kd_unit','kd_unit_asal','nama_unit','nama_unit_asal','no_transaksi_asal', 'kd_kasir_asal','kd_dokter_asal'];
    dsTRHasilHemodialisaList = new WebApp.DataStore({ fields: Field }); //Data store untuk data hd_kunjungan
    dataGridHasilHemodialisa(); // Fungsi yang memanggil query ambil data hd_kunjungan untuk ditampilkan di datagrid
	grListHasilHemodialisa = new Ext.grid.EditorGridPanel //Grid untuk menampilkan data hd_kunjungan
    (
        {
            stripeRows: true,
            store: dsTRHasilHemodialisaList, //data store yang digunakan dalam grid
            //anchor: '100% 91.9999%',
            columnLines: false,
            autoScroll:true,
            border: false,
			sort :false,
			height:390,
            sm: new Ext.grid.RowSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        rowselect: function(sm, row, rec)
                        {
                            rowSelectedHasilHemodialisa = dsTRHasilHemodialisaList.getAt(row);
							rowSelectedHasilHemodialisa = undefined;
							rowSelectedHasilHemodialisa = dsTRHasilHemodialisaList.getAt(row);
							CurrentHasilHemodialisa
							CurrentHasilHemodialisa.row = row;
							CurrentHasilHemodialisa.data = rowSelectedHasilHemodialisa.data;
                        }
                    }
                }
            ),
            listeners:
            {
				//action jika isi grid di double klik
                rowdblclick: function (sm, ridx, cidx)
                {

                    rowSelectedHasilHemodialisa = dsTRHasilHemodialisaList.getAt(ridx);
                    if (rowSelectedHasilHemodialisa !== undefined)
                    {
						// alert(rowSelectedHasilHemodialisa.data.kd_dokter_asal);
					   PenHasilLookUp(rowSelectedHasilHemodialisa.data); //memanggil fungsi untuk menampilkan window
						dataGridHDPradialisa(Ext.get('TxtPopNoDia').getValue(),0); //load data hd_item_kunjungan berdasarkan no_dia dan jenis_dialisa (pradialisa=0, dialisa=1, pascadialisa=2)
						dataGridHDDialisa(Ext.get('TxtPopNoDia').getValue(),1);
						dataGridHDPascadialisa(Ext.get('TxtPopNoDia').getValue(),2);
    
                    }
                    else
                    {
                        PenHasilLookUp();
                    }
                }
            },
        cm: new Ext.grid.ColumnModel
            (
			
                [
                    new Ext.grid.RowNumberer(),
					{
                        id: 'colViewTglMasukHL',
                        header: 'Tanggal Masuk',
                        dataIndex: 'tgl_masuk',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 75,
                            renderer: function(v, params, record)
                            {
                                    return ShowDate(record.data.tgl_masuk);

							}
                    },
					{
                        id: 'colViewNoTransaksiHL',
                        header: 'No Transaksi',
                        dataIndex: 'no_transaksi',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 50
                    },
                    {
                        id: 'colViewNoMedrecHL',
                        header: 'No Medrec',
                        dataIndex: 'kd_pasien',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 50
                    },
                    {
                        id: 'colViewNamaHL',
                        header: 'Nama Pasien',
                        dataIndex: 'nama',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 130
                    },
					{
                        id: 'colViewAlamatHL',
						header: 'Alamat',
                        width: 170,
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        dataIndex: 'alamat'                        
                    },
                    {
                        id: 'colViewTtlHL',
                        header: 'Tanggal Lahir',
                        dataIndex: 'tgl_lahir',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 65,
                            renderer: function(v, params, record)
                            {
                                    return ShowDate(record.data.tgl_lahir); //agar tampilan tanggal

							}
                    },
                    {
                        id: 'colViewNmDokterHL',
                        header: 'Dokter',
                        dataIndex: 'nama_dokter',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 150
                    },
                    {
                        id: 'colViewUnitHL',
                        header: 'Unit',
                        dataIndex: 'nama_unit_asal',
						sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 90
                    },
					
                   
                ]
            ),
            viewConfig: {forceFit: true},
            tbar:
                [
                    {
                        id: 'btnEditHasilHemodialisa',
                        text: nmEditData,
                        tooltip: nmEditData,
                        iconCls: 'Edit_Tr',
                        handler: function(sm, row, rec)
                        {
                            if (rowSelectedHasilHemodialisa != undefined)
                            {
								PenHasilLookUp(rowSelectedHasilHemodialisa.data);
								dataGridHDPradialisa(Ext.get('TxtPopNoDia').getValue(),0); //load data hd_item_kunjungan berdasarkan no_dia dan jenis_dialisa (pradialisa=0, dialisa=1, pascadialisa=2)
								dataGridHDDialisa(Ext.get('TxtPopNoDia').getValue(),1);
								dataGridHDPascadialisa(Ext.get('TxtPopNoDia').getValue(),2);
                            }
                            else
                            {
								ShowPesanWarningHasilHemodialisa('Pilih data data tabel  ','Edit data');
                            }
                        }
                    }
                ]
            }
	);
	
	
	//form depan awal dan judul tab
    var FormDepanHasilHemodialisa = new Ext.Panel
    (
        {
            id: mod_id,
            closable: true,
            region: 'center',
            layout: 'form',
            title: 'Hasil Hemodialisa',
            border: false,
            shadhow: true,
            autoScroll:false,
            iconCls: 'Request',
            margins: '0 5 5 0',
			//height:200,
			autoHeight:true,
            items: [
                    getItemPanelHasilHemodialisa(), //panel input
                    grListHasilHemodialisa //panel data grid
                   ],
            listeners:
            {
                'afterrender': function()
                {
				}
            }
        }
    );
	
   return FormDepanHasilHemodialisa;

};

//mengatur lookup edit data 
function PenHasilLookUp(rowdata) 
{
    var lebar = 800;
    FormLookUpsdetailHasilHemodialisa = new Ext.Window //window yang berisi data pasien dan grid editor hasil hemodialisa
    (
        {
            id: 'gridHasilHemodialisa',
            title: 'Hasil Hemodialisa',
            closeAction: 'destroy',
            width: lebar,
            height: 500,
            border: false,
            resizable: false,
            plain: true,
            layout: 'fit',
            constrain: true,
            iconCls: 'Request',
            modal: true,
            items: getFormEntryHasilHemodialisa(lebar,rowdata),
            listeners:
            {
                
            }
        }
    );

    FormLookUpsdetailHasilHemodialisa.show();
    if (rowdata === undefined) {
		
	}
	else{
		DataInitHasilHemodialisa(rowdata);
	}

};


//mengatur lookup toolbar
function getFormEntryHasilHemodialisa(lebar,data) 
{
    var pnlHasilHemodialisa = new Ext.FormPanel //panel untuk menampilkan data pasien
    (
        {
            id: 'PanelHasilHemodialisa',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:180,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [viewlookupedit(lebar)], // item yang berisi data pasien,
			tbar:
			[
				{
					id:'btnCetakHasilHD',
					text: 'Cetak',
					tooltip: 'Cetak Hasil',
					iconCls:'print',
					handler: function()
					{
					   getDataHasilHDCetak();
					}
				},
				
			],
			
        }
    );
	// toolbar grid lookup
	var x;
	var GDtabDetailHasilHemodialisa = new Ext.TabPanel   //panel tab untuk menampilkan grid hasil hemodialisa
    (
        {
			id:'GDtabDetailHasilHemodialisa',
			region: 'center',
			activeTab: 0,
			height:272,
			width:777,
			border:true,
			plain: true,
			defaults:
			{
				autoScroll: true
			},
			items: [
					GetDTGridHasilHemodialisa_pradialisa(), //grid hd pradialisa
					GetDTGridHasilHemodialisa_dialisa(), //grid dialisa
					GetDTGridHasilHemodialisa_pascadialisa() //grid pascadialisa
			],
			listeners: {
				'tabchange': function(panel, tab) {
						var a = panel.getActiveTab();
						var idx = panel.items.indexOf(a);
						if ( idx == 0){
							// shortcut.remove('save_dialisa');
							// shortcut.remove('save_pascadialisa');
							// shortcut.set({
								// code:'save_pradialisa',
								// list:[
									// {
										// key:'ctrl+s',
										// fn:function(){
											// Ext.getCmp('btnSimpanHasilHDPradialisa').el.dom.click();
										// }
									// }
								// ]
							// });
							
						}
						if ( idx == 1){
							// shortcut.remove('save_pradialisa');
							// shortcut.remove('save_pascadialisa');
							// shortcut.set({
								// code:'save_dialisa',
								// list:[
									// {
										// key:'ctrl+s',
										// fn:function(){
											// Ext.getCmp('btnSimpanHasilHDDialisa').el.dom.click();
										// }
									// }
								// ]
							// });
						}
						if ( idx == 2){
							// shortcut.remove('save_pradialisa');
							// shortcut.remove('save_dialisa');
							// shortcut.set({
								// code:'save_pascadialisa',
								// list:[
									// {
										// key:'ctrl+s',
										// fn:function(){
											// Ext.getCmp('btnSimpanHasilHDPascadialisa').el.dom.click();
										// }
									// }
								// ]
							// });
						}
				}
			}
        }
		
    );

	var pnlHasilHemodialisa2 = new Ext.FormPanel //panel untuk menampilkan tab grid hasil hemodialisa
    (
        {
            id: 'PanelHasilHemodialisa2',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:5px 5px 5px 5px',
            height:305,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [	GDtabDetailHasilHemodialisa //isi panel adalah tab panel
			
			]
        }
    );

    var FormDepanHasilHemodialisa = new Ext.Panel //panel yang menampung panel untuk menampilkan data pasien dan panel tab hd
	(
		{
		    id: 'FormDepanHasilHemodialisa',
		    region: 'center',
		    width: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: 
			[ 
				pnlHasilHemodialisa,pnlHasilHemodialisa2
			]

		}
	);
	
    return FormDepanHasilHemodialisa
};

 
//LOOKUP hasil 
function viewlookupedit(lebar){ // fungsi untuk menampilkan data pasien
	var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:0px 0px 0px 0px',
	    border:true,
	    height:140,
	    items:
		[
			{
				columnWidth: .99,
				layout: 'absolute',
				bodyStyle: 'padding: 0px 0px 0px 0px',
				border: false,
				width: 100,
				height: 150,
				anchor: '100% 100%',
				items:
				[
					{
						x: 10,
						y: 10,
						xtype: 'label',
						text: 'No. Medrec '
					},
					{
						x: 110,
						y: 10,
						xtype: 'label',
						text: ' : '
					},
					{   
						x : 120,
						y : 10,
						xtype: 'textfield',
						name: 'TxtPopupMedrec',
						id: 'TxtPopupMedrec',
						width: 80,
						readOnly:true
					},
					{
						x: 210,
						y: 10,
						xtype: 'label',
						text: 'Tanggal '
					},
					{
						x: 250,
						y: 10,
						xtype: 'label',
						text: ' : '
					},
					{   //cobacoba
						x : 260,
						y : 10,
						xtype: 'datefield',
						name: 'dPopupTglCekPasien',
						id: 'dPopupTglCekPasien',
						width: 110,
						format: 'd/M/Y',
						value: now,
						readOnly:true
					},
					{
						x: 10,
						y: 40,
						xtype: 'label',
						text: 'Nama Pasien '
					},
					{
						x: 110,
						y: 40,
						xtype: 'label',
						text: ' : '
					},
					{   
						x : 120,
						y : 40,
						xtype: 'textfield',
						name: 'TxtPopupNamaPasien',
						id: 'TxtPopupNamaPasien',
						width: 250,
						readOnly:true
					},
					{
						x: 380,
						y: 20,
						xtype: 'label',
						text: 'Tgl Lahir '
					},
					{   
						x : 380,
						y : 40,
						xtype: 'datefield',
						name: 'dPopupTglLahirPasien',
						id: 'dPopupTglLahirPasien',
						width: 100,
						format: 'd/M/Y',
						readOnly:true
					},
					{
						x: 490,
						y: 20,
						xtype: 'label',
						text: 'Thn '
					},
					{   
						x : 490,
						y : 40,
						xtype: 'textfield',
						name: 'TxtPopupThnLahirPasien',
						id: 'TxtPopupThnLahirPasien',
						width: 30,
						readOnly:true
					},
					{
						x: 530,
						y: 20,
						xtype: 'label',
						text: 'Bln '
					},
					{   
						x : 530,
						y : 40,
						xtype: 'textfield',
						name: 'TxtPopupBlnLahirPasien',
						id: 'TxtPopupBlnLahirPasien',
						width: 30,
						readOnly:true
					},
					{
						x: 570,
						y: 20,
						xtype: 'label',
						text: 'Hari '
					},
					{   
						x : 570,
						y : 40,
						xtype: 'textfield',
						name: 'TxtPopupHariLahirPasien',
						id: 'TxtPopupHariLahirPasien',
						width: 30,
						readOnly:true
					},
					{
						x: 10,
						y: 70,
						xtype: 'label',
						text: 'Alamat '
					},
					{
						x: 110,
						y: 70,
						xtype: 'label',
						text: ' : '
					},
					{   
						x : 120,
						y : 70,
						xtype: 'textfield',
						name: 'TxtPopupAlamat',
						id: 'TxtPopupAlamat',
						width: 480,
						readOnly:true
					},
					{
						x: 10,
						y: 100,
						xtype: 'label',
						text: 'Dokter'
					},
					{
						x: 110,
						y: 100,
						xtype: 'label',
						text: ' : '
					},
					{   
						x : 120,
						y : 100,
						xtype: 'textfield',
						name: 'TxtPopupNamaDokter',
						id: 'TxtPopupNamaDokter',
						width: 250,
						readOnly:true
					},
					//-------HIDDEN---------------------
					{   
						x : 480,
						y : 10,
						xtype: 'datefield',
						name: 'dPopupTglMasuk',
						id: 'dPopupTglMasuk',
						width: 150,
						readOnly:true,
						format: 'd/M/Y',
						hidden:true
					},
					{   
						x : 480,
						y : 10,
						xtype: 'datefield',
						name: 'dPopupJamMasuk',
						id: 'dPopupJamMasuk',
						width: 150,
						readOnly:true,
						format: 'd/M/Y',
						hidden:true
					},
					{   
						x : 120,
						y : 100,
						xtype: 'textfield',
						name: 'TxtUrutMasuk',
						id: 'TxtUrutMasuk',
						width: 240,
						readOnly:true,
						hidden:true
					},
					{   
						x : 120,
						y : 100,
						xtype: 'textfield',
						name: 'TxtNmDokter',
						id: 'TxtNmDokter',
						width: 240,
						readOnly:true,
						hidden:true
					},
					//
					{
						x: 610,
						y: 20,
						xtype: 'label',
						text: 'Jenis Kelamin '
					},
					{   
						x : 610,
						y : 40,
						xtype: 'textfield',
						name: 'TxtJK',
						id: 'TxtJK',
						width: 80,
						readOnly:true,
						hidden:false
					},
					{
						x: 700,
						y: 20,
						xtype: 'label',
						text: 'G. Darah '
					},
					{   
						x : 700,
						y : 40,
						xtype: 'textfield',
						name: 'TxtGDarah',
						id: 'TxtGDarah',
						width: 50,
						readOnly:true,
						hidden:false
					},
					//
					{   
						x : 120,
						y : 100,
						xtype: 'textfield',
						name: 'TxtPopPoli',
						id: 'TxtPopPoli',
						width: 240,
						readOnly:true,
						hidden:true
					},
					{   
						x : 120,
						y : 100,
						xtype: 'textfield',
						name: 'TxtPopKddokter',
						id: 'TxtPopKddokter',
						width: 250,
						readOnly:true,
						hidden:true
					},
					{   
						x : 120,
						y : 100,
						xtype: 'textfield',
						name: 'TxtPopNoDia',
						id: 'TxtPopNoDia',
						width: 250,
						readOnly:true,
						hidden:true
					},
					{   
						x : 120,
						y : 100,
						xtype: 'textfield',
						name: 'TxtPopNoUrut',
						id: 'TxtPopNoUrut',
						width: 250,
						readOnly:true,
						hidden:true
					}
				]
			},
		]
	};
	return items;
}

//------------GRID DALAM LOOK UP TAB PRADIALISA, DIALISA, PASCADIALISA--------------------------------------------------------------------------
function GetDTGridHasilHemodialisa_pradialisa() 
{
	var fm = Ext.form;
    var fldDetailHasilHemodialisa_pradialisa = ['kd_item','item_hd','type_item','kd_dia','dialisa','no_urut','nilai','no_ref','deskripsi','nilai'];
	dsTRDetailHasilHemodialisa_pradialisa = new WebApp.DataStore({ fields: fldDetailHasilHemodialisa_pradialisa })
    
	gridDTHasilHemodialisa_pradialisa = new Ext.grid.EditorGridPanel
    (
        {
            title: 'Pradialisa',
            stripeRows: true,
            store: dsTRDetailHasilHemodialisa_pradialisa,// DATASTORE
            border: false,
            columnLines: true,
            frame: false,
            autoScroll:true,
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
							rowSelected_HDPradialisa = undefined;
							rowSelected_HDPradialisa = dsTRDetailHasilHemodialisa_pradialisa.getAt(row);
							CurrentData_HDPradialisa
							CurrentData_HDPradialisa.row = row;
							CurrentData_HDPradialisa.data = rowSelected_HDPradialisa.data;
                        }
                    }
                }
            ),
           cm: new Ext.grid.ColumnModel
            (
			[
				
				{
					header:'Kode Item',
					dataIndex: 'kd_item',
					sortable: false,
					hidden:false,
					menuDisabled:true,
					width:100
					
				},
				{
					header:'Item',
					dataIndex: 'item_hd',
					sortable: false,
					hidden:false,
					menuDisabled:true,
					width:100
					
				},
				{
					header:'Test',
					dataIndex: 'deskripsi',
					sortable: false,
					hidden:false,
					menuDisabled:true,
					width:200
					
				},
				{
					header:'Hasil',
					dataIndex: 'nilai',
					sortable: false,
					hidden:false,
					menuDisabled:true,
					width:200,
					editor: new fm.TextField({
					allowBlank: false
					})
					
				}
			]
			),
			viewConfig:{forceFit: true},
			tbar:
			[
				{
					text: 'Simpan',
					id: 'btnSimpanHasilHDPradialisa',
					tooltip: nmSimpan,
					iconCls: 'save',
					handler: function()
					{
						Datasave_HasilHDPradialisa(); //Fungsi simpan hd pradialisa
					}
				}
			]
        }
		
    );
	
    return gridDTHasilHemodialisa_pradialisa;
};

function GetDTGridHasilHemodialisa_dialisa() 
{
	var fm = Ext.form;
    var fldDetailHasilHemodialisa_dialisa = ['kd_item','item_hd','type_item','kd_dia','dialisa','no_urut','nilai','no_ref','deskripsi','nilai'];
	
    dsTRDetailHasilHemodialisa_dialisa = new WebApp.DataStore({ fields: fldDetailHasilHemodialisa_dialisa })
    gridDTHasilHemodialisa_dialisa = new Ext.grid.EditorGridPanel
    (
        {
            title: 'Dialisa',
            stripeRows: true,
            store: dsTRDetailHasilHemodialisa_dialisa,
            border: false,
            columnLines: true,
            frame: false,
            autoScroll:true,
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                            rowSelected_HDDialisa = undefined;
							rowSelected_HDDialisa = dsTRDetailHasilHemodialisa_dialisa.getAt(row);
							CurrentData_HDDialisa
							CurrentData_HDDialisa.row = row;
							CurrentData_HDDialisa.data = rowSelected_HDDialisa.data;
							
                        }
                    }
                }
            ),
           cm: new Ext.grid.ColumnModel
            (
			[
				{
					header:'Kode Item',
					dataIndex: 'kd_item',
					sortable: false,
					hidden:false,
					menuDisabled:true,
					width:100
					
				},
				{
					header:'Item',
					dataIndex: 'item_hd',
					sortable: false,
					hidden:false,
					menuDisabled:true,
					width:100
					
				},
				{
					header:'Test',
					dataIndex: 'deskripsi',
					sortable: false,
					hidden:false,
					menuDisabled:true,
					width:200
					
				},
				{
					header:'Hasil',
					dataIndex: 'nilai',
					sortable: false,
					hidden:false,
					menuDisabled:true,
					width:200,
					editor: new fm.TextField({
					allowBlank: false
					})
					
				}

			]
			),
			viewConfig:{forceFit: true},
			tbar:
                [
                    {
                        text: 'Simpan',
						id: 'btnSimpanHasilHDDialisa',
						tooltip: nmSimpan,
						iconCls: 'save',
						handler: function()
						{
							Datasave_HasilHDDialisa();
						   
						}
                    }
                ]
        }
		
		
    );
	
    return gridDTHasilHemodialisa_dialisa;
};

function GetDTGridHasilHemodialisa_pascadialisa() 
{
	var fm = Ext.form;
    var fldDetailHasilHemodialisa_pascadialisa = ['kd_item','item_hd','type_item','kd_dia','dialisa','no_urut','nilai','no_ref','deskripsi','nilai'];
	
    dsTRDetailHasilHemodialisa_pascadialisa = new WebApp.DataStore({ fields: fldDetailHasilHemodialisa_pascadialisa })
    gridDTHasilHemodialisa_pascadialisa = new Ext.grid.EditorGridPanel
    (
        {
            title: 'Pascadialisa',
            stripeRows: true,
            store: dsTRDetailHasilHemodialisa_pascadialisa,
            border: false,
            columnLines: true,
            frame: false,
            autoScroll:true,
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                            rowSelected_HDPascadialisa = undefined;
							rowSelected_HDPascadialisa = dsTRDetailHasilHemodialisa_pascadialisa.getAt(row);
							CurrentData_HDPascadialisa
							CurrentData_HDPascadialisa.row = row;
							CurrentData_HDPascadialisa.data = rowSelected_HDPascadialisa.data;
							
                        }
                    }
                }
            ),
           cm: new Ext.grid.ColumnModel
            (
			[
				{
					header:'Kode Item',
					dataIndex: 'kd_item',
					sortable: false,
					hidden:false,
					menuDisabled:true,
					width:100
					
				},
				{
					header:'Item',
					dataIndex: 'item_hd',
					sortable: false,
					hidden:false,
					menuDisabled:true,
					width:100
					
				},
				{
					header:'Test',
					dataIndex: 'deskripsi',
					sortable: false,
					hidden:false,
					menuDisabled:true,
					width:200
					
				},
				{
					header:'Hasil',
					dataIndex: 'nilai',
					sortable: false,
					hidden:false,
					menuDisabled:true,
					width:200,
					editor: new fm.TextField({
					allowBlank: false
					})
					
				}

			]
			),
			
			viewConfig:{forceFit: true},
			 tbar:
                [
                    {
                        text: 'Simpan',
						id: 'btnSimpanHasilHDPascadialisa',
						tooltip: nmSimpan,
						iconCls: 'save',
						handler: function()
						{
							Datasave_HasilHDPascadialisa();
						   
						}
                    }
                ]
        }
		
		
    );
	
    return gridDTHasilHemodialisa_pascadialisa;
};
//------------------MEMASUKAN DATA YG DIPILIH KEDALAM TEXTBOX YG ADA DALAM LOOKUP--------------------------------------------------
function DataInitHasilHemodialisa(rowdata)
{
	//alert(tmpjam_masuk);
	tmpnama_unit_asal=rowdata.nama_unit_asal;
	tmpkd_dokter_asal=rowdata.kd_dokter_asal;
	tmpjam_masuk=rowdata.jam_masuk;
	tmpnama_dokter_asal=rowdata.nama_dokter_asal;
	Ext.get('TxtPopupMedrec').dom.value= rowdata.kd_pasien;
	Ext.get('TxtPopupNamaPasien').dom.value = rowdata.nama;
	Ext.get('TxtPopupAlamat').dom.value = rowdata.alamat;
	Ext.get('TxtPopupNamaDokter').dom.value = rowdata.nama_dokter;
	Ext.get('dPopupTglLahirPasien').dom.value = ShowDate(rowdata.tgl_lahir);
	Ext.get('dPopupTglMasuk').dom.value = ShowDate(rowdata.tgl_masuk);
	if(rowdata.jenis_kelamin=='t'){
		Ext.get('TxtJK').dom.value = 'Laki-laki';
	}else{
		Ext.get('TxtJK').dom.value = 'Perempuan'
	}
	var gol_darah="";
	if(rowdata.gol_darah == '0'){
		gol_darah='-';
	}else if (rowdata.gol_darah == '1'){
		gol_darah='A+';
	}else if (rowdata.gol_darah == '2'){
		gol_darah='B+';
	}else if (rowdata.gol_darah == '3'){
		gol_darah='AB+';
	}else if (rowdata.gol_darah == '4'){
		gol_darah='0+';
	}else if (rowdata.gol_darah == '5'){
		gol_darah='A-';
	}else if (rowdata.gol_darah == '6'){
		gol_darah='B-';
	}
	Ext.get('TxtGDarah').dom.value = gol_darah;
	Ext.get('TxtPopPoli').dom.value = rowdata.nama_unit;
	Ext.get('TxtNmDokter').dom.value = rowdata.nama_dokter;
	Ext.get('TxtUrutMasuk').dom.value = rowdata.urut_masuk;
	Ext.get('TxtPopKddokter').dom.value = rowdata.kd_dokter;
	Ext.get('TxtPopNoDia').dom.value = rowdata.no_dia;
	setUsia(ShowDate(rowdata.tgl_lahir));	
	
};

//-----------------------------------------MENGHITUNG USIA DALAM LOOKUP------------------------------------------------------------
function setUsia(Tanggal)
{

	Ext.Ajax.request
	( 
		{
		   url: baseURL + "index.php/hemodialisa/functionHasilHemodialisa/getUmur",
		   params: {
					TanggalLahir: ShowDateReal(Tanggal)
		   },
		   
		   success: function(o)
		   {
				var tmphasil = o.responseText;
				var tmp = tmphasil.split(' ');
				if (tmp.length == 6)
				{
					Ext.getCmp('TxtPopupThnLahirPasien').setValue(tmp[0]);
					Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[2]);
					Ext.getCmp('TxtPopupHariLahirPasien').setValue(tmp[4]);
				}
				else if(tmp.length == 4)
				{
					if(tmp[1]== 'years' && tmp[3] == 'day')
					{
						Ext.getCmp('TxtPopupThnLahirPasien').setValue(tmp[0]);
						Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
						Ext.getCmp('TxtPopupHariLahirPasien').setValue(tmp[2]);  
					}else{
					Ext.getCmp('TxtPopupThnLahirPasien').setValue('0');
					Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[0]);
					Ext.getCmp('TxtPopupHariLahirPasien').setValue(tmp[2]);
						  }
				}
				else if(tmp.length == 2 )
				{
					
					if (tmp[1] == 'year' )
					{
						Ext.getCmp('TxtPopupThnLahirPasien').setValue(tmp[0]);
						Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
						Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
					}
					else if (tmp[1] == 'years' )
					{
						Ext.getCmp('TxtPopupThnLahirPasien').setValue(tmp[0]);
						Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
						Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
					}
					else if (tmp[1] == 'mon'  )
					{
						Ext.getCmp('TxtPopupThnLahirPasien').setValue('0');
						Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[0]);
						Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
					}
					else if (tmp[1] == 'mons'  )
					{
						Ext.getCmp('TxtPopupThnLahirPasien').setValue('0');
						Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[0]);
						Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
					}
					else{
							Ext.getCmp('TxtPopupThnLahirPasien').setValue('0');
							Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
							Ext.getCmp('TxtPopupHariLahirPasien').setValue(tmp[0]);
						}
				}
				
				else if(tmp.length == 1)
				{
					Ext.getCmp('TxtPopupThnLahirPasien').setValue('0');
					Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
					Ext.getCmp('TxtPopupHariLahirPasien').setValue('1');
				}else
				{
					alert("Tanggal yang anda masukan lebih besar dari tanggal sekarang");
				}
		   }
		}
	);
}

//AWAL INPUT
function getItemPanelHasilHemodialisa() // isi item pada panel pencarian data hd kunjungan
{
    var items =
    {
        layout:'column',
		bodyStyle: 'padding: 5px 0px 5px 10px',
        border:true,
        items:
        [
			//--------------------------------------------------
            {
                columnWidth:.99,
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 600,
                height: 105,
                anchor: '100% 100%',
                items:
                [
					{
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'No. Medrec  '
                    },
					{
                        x: 145,
                        y: 10,
                        xtype: 'label',
                        text: ':'
                    },
					SetupHasilHemodialisa.vars.kd_pasien=new Nci.form.Combobox.autoCompleteId({ // kode pasien autocompleted
						x: 155,
						y: 10,
						tabIndex:2,
						id		: 'txtComboNoMedrec',
						store	:SetupHasilHemodialisa.form.ArrayStore.a,
						select	: function(a,b,c){
							grListHasilHemodialisa.getView().refresh(); // merefresh data grid agar sesuai dengan inputan 
						},
						onShowList:function(a){
							dsTRHasilHemodialisaList.removeAll();
							var recs=[],
							recType=dsTRHasilHemodialisaList.recordType;
								
							for(var i=0; i<a.length; i++){
								recs.push(new recType(a[i]));
							}
							dsTRHasilHemodialisaList.add(recs);
							
						},
						insert	: function(o){
							return {
								kd_pasien     	 	: o.kd_pasien,
								kd_unit 			: o.kd_unit,
								tgl_masuk			: o.tgl_masuk,
								jam_masuk			: o.jam_masuk,
								urut_masuk			: o.urut_masuk,
								urut_dia			: o.urut_dia,
								no_dia				: o.no_dia,
								nama				: o.nama,
								tgl_lahir			: o.tgl_lahir,
								alamat				: o.alamat,
								kd_dokter			: o.kd_dokter,
								nama_dokter			: o.nama_dokter,
								kd_kasir			: o.kd_kasir,
								kd_unit_asal		: o.kd_unit_asal,
								nama_unit_asal		: o.nama_unit_asal,
								no_transaksi_asal	: o.no_transaksi_asal,
								kd_kasir_asal		: o.kd_kasir_asal,
								text			:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_pasien+'</td></tr></table>' //atribut yang akan ditampilkan pada combo box
							}
						},
						url		: baseURL + "index.php/hemodialisa/functionHasilHemodialisa/getHasilHDGrid", //memanggil fungsi untuk pencarian data di combobox didatabase
						valueField: 'kd_pasien',
						displayField: 'text',
						listWidth: 280
					}),
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Nama Pasien '
                    },
					{
                        x: 145,
                        y: 40,
                        xtype: 'label',
                        text: ':'
                    },
                    {   
                        x : 155,
                        y : 40,
                        xtype: 'textfield',
                        name: 'TxtNamaPasien',
                        id: 'TxtNamaPasien',
                        width: 200,
                        enableKeyEvents: true,
                        listeners:
                        { 
                                'specialkey' : function()
                                {
                                        if (Ext.EventObject.getKey() === 13) //ketika nama pasien di enter, maka akan ditampilkan data sesuai pencarian
                                        {
											dataGridHasilHemodialisa(Ext.get('TxtNamaPasien').getValue(),SetupHasilHemodialisa.vars.kd_pasien.getValue());
                                        } 						
                                }
                        }
                    },
					{
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Tanggal Kunjungan '
                    },
					{
                        x: 145,
                        y: 70,
                        xtype: 'label',
                        text: ':'
                    },
                    {
                        x: 155,
                        y: 70,
                        xtype: 'datefield',
                        id: 'dtpTglAwalFilterHasilHemodialisa',
                        format: 'd/M/Y',
                        value: tigaharilalu,
                        listeners:
                        { 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) //ketika di enter akan ditampilkan data sesuai inputan tanggal
								{
									dataGridHasilHemodialisa(
											Ext.get('TxtNamaPasien').getValue(),SetupHasilHemodialisa.vars.kd_pasien.getValue(),
											Ext.get('dtpTglAwalFilterHasilHemodialisa').getValue(),Ext.get('dtpTglAkhirFilterHasilHemodialisa').getValue()
									);
								} 						
							}
                        }
                    },
					{
                        x: 270,
                        y: 70,
                        xtype: 'label',
                        text: 's/d '
                    },
					{
                        x: 305,
                        y: 70,
                        xtype: 'datefield',
                        id: 'dtpTglAkhirFilterHasilHemodialisa',
                        format: 'd/M/Y',
                        value: now,
                        width: 100,
                        listeners:
                        { 
                                'specialkey' : function()
                                {
                                        if (Ext.EventObject.getKey() === 13) //ketika di enter akan ditampilkan data sesuai inputan tanggal
                                        {
											dataGridHasilHemodialisa(
												Ext.get('TxtNamaPasien').getValue(),SetupHasilHemodialisa.vars.kd_pasien.getValue(),
												Ext.get('dtpTglAwalFilterHasilHemodialisa').getValue(),Ext.get('dtpTglAkhirFilterHasilHemodialisa').getValue()
											);
                                        } 						
                                }
                        }
                    },
					{
                        x: 595,
                        y: 70,
                        xtype:'button',
                        text: 'Refresh',
                        iconCls: 'refresh',
                        anchor: '25%',
                        width: 70,
                        height: 20,
                        hideLabel: false,
                        handler: function(sm, row, rec)
                        {
							dsTRHasilHemodialisaList.removeAll();
							dataGridHasilHemodialisa();
                        }
                    } 
                ]
            },
        ]
    }
    return items;
};

//fungsi untuk menampilkan datagrid hd kunjungan
function dataGridHasilHemodialisa(nama,kode,tgl_awal,tgl_akhir){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/hemodialisa/functionHasilHemodialisa/getGridHasilHemodialisa",
			params: {nama:nama, kode:kode, tgl_awal:tgl_awal, tgl_akhir:tgl_akhir},
			failure: function(o)
			{
				ShowPesanErrorHD('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{

				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dsTRHasilHemodialisaList.removeAll();
					var recs=[],
						recType=dsTRHasilHemodialisaList.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						recs.push(new recType(cst.listData[i]));
					}
					dsTRHasilHemodialisaList.add(recs);
					grListHasilHemodialisa.getView().refresh();
				}
				else 
				{
					ShowPesanErrorHD('Gagal membaca data obat', 'Error');
				};
			}
		}
	)
}
//fungsi untuk menampilkan datagrid hd pradialisa
function dataGridHDPradialisa(no_dia,kd_dia){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/hemodialisa/functionHasilHemodialisa/getGridHD", // memanggil query 
			params: { //parameter 
						NoDia:no_dia,
						KdDia:kd_dia
					},
			failure: function(o)
			{
				ShowPesanErrorHD('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dsTRDetailHasilHemodialisa_pradialisa.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						recs.push(new recType(cst.listData[i]));
					}
					dsTRDetailHasilHemodialisa_pradialisa.add(recs); //data ditampung ke datastore
					gridDTHasilHemodialisa_pradialisa.getView().refresh(); //untuk menampilkan data ke grid
				}
				else 
				{
					ShowPesanErrorHD('Gagal membaca data obat', 'Error');
				};
			}
		}
	)
}
//fungsi untuk menampilkan datagrid hd dialisa
function dataGridHDDialisa(no_dia,kd_dia){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/hemodialisa/functionHasilHemodialisa/getGridHD", //memanggil query
			params: { //parameter
						NoDia:no_dia,
						KdDia:kd_dia
					},
			failure: function(o)
			{
				ShowPesanErrorHD('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dsTRDetailHasilHemodialisa_dialisa.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						recs.push(new recType(cst.listData[i]));
					}
					dsTRDetailHasilHemodialisa_dialisa.add(recs); //data ditampung ke datastore
					gridDTHasilHemodialisa_dialisa.getView().refresh(); // menampilkan data ke grid
				}
				else 
				{
					ShowPesanErrorHD('Gagal membaca data obat', 'Error');
				};
			}
		}
	)	
}
//fungsi untuk menampilkan datagrid hd pascadialisa
function dataGridHDPascadialisa(no_dia,kd_dia){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/hemodialisa/functionHasilHemodialisa/getGridHD", //memanggil query
			params: { //parameter
						NoDia:no_dia,
						KdDia:kd_dia
					},
			failure: function(o)
			{
				ShowPesanErrorHD('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dsTRDetailHasilHemodialisa_pascadialisa.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						recs.push(new recType(cst.listData[i]));
					}
					dsTRDetailHasilHemodialisa_pascadialisa.add(recs); //menampung data ke datastore
					gridDTHasilHemodialisa_pascadialisa.getView().refresh(); //menapilkan data ke grid
				}
				else 
				{
					ShowPesanErrorHD('Gagal membaca data obat', 'Error');
				};
			}
		}
	)
}
//fungsi untuk menyimpan data hd pradialisa
function Datasave_HasilHDPradialisa()
{
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/hemodialisa/functionHasilHemodialisa/saveHasilHD",
			params: getParamSaveHasilHD_pradialisa(),
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorHD('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					loadMask.hide();
					ShowPesanInfoHD('Berhasil menyimpan hasil pradialisa ','Information');
					
					dsTRDetailHasilHemodialisa_pradialisa.removeAll();
					dataGridHDPradialisa(Ext.get('TxtPopNoDia').getValue(),0)
				}
				else 
				{
					loadMask.hide();
					ShowPesanErrorHD('Gagal menyimpan data ini', 'Error');
					dsTRDetailHasilHemodialisa_pradialisa.removeAll();
					dataGridHDPradialisa(Ext.get('TxtPopNoDia').getValue(),0)
				
				};
			}
		}
		
	)
}

//parameter inputan yang digunakan untuk menyimpan data
function getParamSaveHasilHD_pradialisa()
{
	var params =
	{
		NoDia	:	Ext.get('TxtPopNoDia').getValue(),
		KdDia	:   '0',
	}
	
	//perulangan untuk menyimpan banyak baris data
	params['jml'] = dsTRDetailHasilHemodialisa_pradialisa.getCount();
	for(var i = 0 ; i <dsTRDetailHasilHemodialisa_pradialisa.getCount();i++)
	{
		params['kd_item-'+i]=dsTRDetailHasilHemodialisa_pradialisa.data.items[i].data.kd_item;		
		params['no_urut-'+i]=dsTRDetailHasilHemodialisa_pradialisa.data.items[i].data.no_urut;		
		params['nilai-'+i]=dsTRDetailHasilHemodialisa_pradialisa.data.items[i].data.nilai;		
	}
	return params
}
//fungsi untuk menyimpan data hd dialisa
function Datasave_HasilHDDialisa()
{
	Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/hemodialisa/functionHasilHemodialisa/saveHasilHD",
				params: getParamSaveHasilHD_dialisa(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorHD('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoHD('Berhasil menyimpan hasil dialisa','Information');
						
						dsTRDetailHasilHemodialisa_dialisa.removeAll();
						dataGridHDDialisa(Ext.get('TxtPopNoDia').getValue(),1)
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorHD('Gagal menyimpan data ini', 'Error');
						dsTRDetailHasilHemodialisa_dialisa.removeAll();
						dataGridHDDialisa(Ext.get('TxtPopNoDia').getValue(),1)
					
					};
				}
			}
			
		)

}

function getParamSaveHasilHD_dialisa()
{
	var params =
	{
		NoDia	:	Ext.get('TxtPopNoDia').getValue(),
		KdDia	:   '1',
	}
	
	params['jml'] = dsTRDetailHasilHemodialisa_dialisa.getCount();
	for(var i = 0 ; i <dsTRDetailHasilHemodialisa_dialisa.getCount();i++)
	{
		params['kd_item-'+i]=dsTRDetailHasilHemodialisa_dialisa.data.items[i].data.kd_item;		
		params['no_urut-'+i]=dsTRDetailHasilHemodialisa_dialisa.data.items[i].data.no_urut;		
		params['nilai-'+i]=dsTRDetailHasilHemodialisa_dialisa.data.items[i].data.nilai;		
	}
	return params
}

//
function Datasave_HasilHDPascadialisa()
{
	Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/hemodialisa/functionHasilHemodialisa/saveHasilHD",
				params: getParamSaveHasilHD_pascadialisa(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorHD('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoHD('Berhasil menyimpan hasil pascadialisa','Information');
						
						dsTRDetailHasilHemodialisa_pascadialisa.removeAll();
						dataGridHDPascadialisa(Ext.get('TxtPopNoDia').getValue(),2)
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorHD('Gagal menyimpan data ini', 'Error');
						dsTRDetailHasilHemodialisa_pascadialisa.removeAll();
						dataGridHDPascadialisa(Ext.get('TxtPopNoDia').getValue(),2)
					
					};
				}
			}
			
		)

}

function getParamSaveHasilHD_pascadialisa()
{
	var params =
	{
		NoDia	:	Ext.get('TxtPopNoDia').getValue(),
		KdDia	:   '2',
	}
	
	params['jml'] = dsTRDetailHasilHemodialisa_pascadialisa.getCount();
	for(var i = 0 ; i <dsTRDetailHasilHemodialisa_pascadialisa.getCount();i++)
	{
		params['kd_item-'+i]=dsTRDetailHasilHemodialisa_pascadialisa.data.items[i].data.kd_item;		
		params['no_urut-'+i]=dsTRDetailHasilHemodialisa_pascadialisa.data.items[i].data.no_urut;		
		params['nilai-'+i]=dsTRDetailHasilHemodialisa_pascadialisa.data.items[i].data.nilai;		
	}
	return params
}

function ShowPesanWarningHD(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};
function ShowPesanInfoHD(str, modul) {
  Ext.MessageBox.show({
	    title: modul,
	    msg: str,
	    buttons: Ext.MessageBox.OK,
	    icon: Ext.MessageBox.INFO,
		width:250
	});
};

function ShowPesanErrorHD(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};


function getDataHasilHDCetak()
{
	var tmpumur='';
	if(Ext.getCmp('TxtPopupThnLahirPasien').getValue() !== ''){
		if(Ext.getCmp('TxtPopupThnLahirPasien').getValue() ==='0' || Ext.get('TxtPopupThnLahirPasien').getValue() ===0){
			if(Ext.getCmp('TxtPopupBlnLahirPasien').getValue() ==='0' || Ext.getCmp('TxtPopupBlnLahirPasien').getValue() ===0){
				tmpumur=Ext.getCmp('TxtPopupHariLahirPasien').getValue() + ' Hari';
			} else{
				tmpumur=Ext.getCmp('TxtPopupBlnLahirPasien').getValue() + ' Bulan';
			}
		} else{
			tmpumur=Ext.getCmp('TxtPopupThnLahirPasien').getValue() + ' Tahun';
		}
	}
     var params = {
        Tgl: Ext.get('dPopupTglMasuk').getValue(),
        KdPasien: Ext.get('TxtPopupMedrec').getValue(),
        NoDia: Ext.get('TxtPopNoDia').getValue(),
        Nama: Ext.get('TxtPopupNamaPasien').getValue(),        
        JenisKelamin: Ext.get('TxtJK').getValue(),
		Ttl:Ext.get('dPopupTglLahirPasien').getValue(),
		Umur:tmpumur,
        Alamat: Ext.get('TxtPopupAlamat').getValue(),
        Poli: Ext.get('TxtPopPoli').getValue(),
        urutmasuk: Ext.get('TxtUrutMasuk').getValue(),
        Dokter:Ext.get('TxtNmDokter').getValue(),
		NamaUnitAsal:tmpnama_unit_asal,
		KdDokterAsal:tmpkd_dokter_asal,
		JamMasuk:tmpjam_masuk,
		NamaDokterAsal:tmpnama_dokter_asal
    };
	
    var form = document.createElement("form");
	form.setAttribute("method", "post");
	form.setAttribute("target", "_blank");
	form.setAttribute("action", baseURL + "index.php/hemodialisa/functionHasilHemodialisa/CetakHasilHD");
	var hiddenField = document.createElement("input");
	hiddenField.setAttribute("type", "hidden");
	hiddenField.setAttribute("name", "data");
	hiddenField.setAttribute("value", Ext.encode(params));
	form.appendChild(hiddenField);
	document.body.appendChild(form);
	form.submit();
}