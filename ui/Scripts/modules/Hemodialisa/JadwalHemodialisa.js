var dataSource_viJadwalHemodialisa;
var selectCount_viJadwalHemodialisa=50;
var NamaForm_viJadwalHemodialisa="Jadwal Hemodialisa";
var now_viJadwalHemodialisa= new Date();
var rowSelected_viJadwalHemodialisa;
var setLookUps_viJadwalHemodialisa;
var tanggal = now_viJadwalHemodialisa.format("d/M/Y");
var jam = now_viJadwalHemodialisa.format("H/i/s");
var tmpkriteria;
var DataGridJenisObat;
var GridDataView_viJadwalHemodialisa;
var cboPrinter_JadwalHemodialisa;
var cboUnitFar_JadwalHemodialisa;
var GridHasilPradialisa_viJadwalHemodialisa;
var GridHasilDialisa_viJadwalHemodialisa;
var GridHasilPascadialisa_viJadwalHemodialisa;
var dataSourceHasilPradialisa_viJadwalHemodialisa;
var dataSourceHasilDialisa_viJadwalHemodialisa;
var dataSourceHasilPascadialisa_viJadwalHemodialisa;
var dsKodePasien_JadwalHemodialisa;
var currentSelectKdPasien_JadwalHemodialisa;
var currentSelectHari_JadwalHemodialisa;
var NamaPasien_JadwalHemodialisa;
var ArrayStoreNamaPasien;

var CurrentData_viJadwalHemodialisa =
{
	data: Object,
	details: Array,
	row: 0
};

var JadwalHemodialisa={};
JadwalHemodialisa.form={};
JadwalHemodialisa.func={};
JadwalHemodialisa.vars={};
JadwalHemodialisa.func.parent=JadwalHemodialisa;
JadwalHemodialisa.form.ArrayStore={};
JadwalHemodialisa.form.ComboBox={};
JadwalHemodialisa.form.DataStore={};
JadwalHemodialisa.form.Record={};
JadwalHemodialisa.form.Form={};
JadwalHemodialisa.form.Grid={};
JadwalHemodialisa.form.Panel={};
JadwalHemodialisa.form.TextField={};
JadwalHemodialisa.form.Button={};

ArrayStoreNamaPasien = new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_pasien', 'nama'],
	data: []
});

CurrentPage.page = dataGrid_viJadwalHemodialisa(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dataGrid_viJadwalHemodialisa(mod_id_viJadwalHemodialisa){	
 	// Field kiriman dari Project Net.
    var FieldMaster_viJadwalHemodialisa = 
	[
		'kd_pasien', 'nama','hari','nama_hari','jam' 
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viJadwalHemodialisa = new WebApp.DataStore
	({
        fields: FieldMaster_viJadwalHemodialisa
    });
	
	dataGriJadwalHemodialisa();
	loadDataKodePasien_JadwalHemodialisa();
	
    // Grid Apotek Perencanaan # --------------
	GridDataView_viJadwalHemodialisa = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viJadwalHemodialisa,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:true,
			anchor: '100% 100%',
			height:190,
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.CellSelectionModel({
            singleSelect: true,
				listeners:{
					cellselect: function(sm, row, rec){
						rowSelected_viJadwalHemodialisa = undefined;
						rowSelected_viJadwalHemodialisa = dataSource_viJadwalHemodialisa.getAt(row);
						CurrentData_viJadwalHemodialisa
						CurrentData_viJadwalHemodialisa.row = row;
						CurrentData_viJadwalHemodialisa.data = rowSelected_viJadwalHemodialisa.data;
						
						currentSelectHari_JadwalHemodialisa = rowSelected_viJadwalHemodialisa.data.hari;
						currentSelectKdPasien_JadwalHemodialisa = rowSelected_viJadwalHemodialisa.data.kd_pasien;
						RefreshDataGridHasilPradialisa_JadwalHemodialisa(currentSelectKdPasien_JadwalHemodialisa);
						RefreshDataGridHasilDialisa_JadwalHemodialisa(currentSelectKdPasien_JadwalHemodialisa);
						RefreshDataGridHasilPascadialisa_JadwalHemodialisa(currentSelectKdPasien_JadwalHemodialisa);
					}
				}
			}),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viJadwalHemodialisa = dataSource_viJadwalHemodialisa.getAt(ridx);
					if (rowSelected_viJadwalHemodialisa != undefined)
					{
						//DataInitJadwalHemodialisa(rowSelected_viJadwalHemodialisa.data);
						//setLookUp_viJadwalHemodialisa(rowSelected_viJadwalHemodialisa.data);
					}
					else
					{
						//setLookUp_viJadwalHemodialisa();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Setup Milik
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header: 'No. Medrec',
						dataIndex: 'kd_pasien',
						hideable:false,
						menuDisabled: true,
						width: 40
						
					},
					//-------------- ## --------------
					{
						header: 'Nama',
						dataIndex: 'nama',
						hideable:false,
						menuDisabled: true,
						width: 90
					},
					//-------------- ## --------------
					{
						header: 'Hari',
						dataIndex: 'nama_hari',
						hideable:false,
						menuDisabled: true,
						width: 40
					},
					//-------------- ## --------------
					{
						header: 'Jam (Format 24 Jam)',
						dataIndex: 'jam',
						hideable:false,
						width: 40
					},
					//-------------- ## --------------
					{
						header: 'Hari',
						dataIndex: 'hari',
						hideable:false,
						hidden: true,
						width: 40
					}
					//-------------- ## --------------
					
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viJadwalHemodialisa',
				items: 
				[
					//-------------- ## --------------
				]
			},
			viewConfig: 
			{
				forceFit: true
			}
		}
	)
	
	var PanelTabJadwalHemodialisa = new Ext.FormPanel({
        labelAlign: 'top',
		layout:'form',
		border: false,  
		autoHeight:true,
        title: '',
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [PanelInputJadwalHemodialisa()]
		
	})
	
	var TabGrid = new Ext.TabPanel({
		activeTab: 0,
		border: false, 
		plain: true,
		width:950,
		items: [
				gridHasilPradialisa_JadwalHemodialisa(),
				gridHasilDialisa_JadwalHemodialisa(),
				gridHasilPascadialisa_JadwalHemodialisa()
		]
	});
	
	var PanelGriHasilJadwalHemodialisa =new Ext.FormPanel({
        labelAlign: 'top',
		layout:'form',
		border: false,  
		autoHeight:true,
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [TabGrid]
	})
	
	
	var PanelGridJadwalHemodialisa = new Ext.FormPanel({
        labelAlign: 'top',
		layout:'form',
		border: false,  
		autoHeight:true,
        title: '',
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [GridDataView_viJadwalHemodialisa,]
		
	})
	// Kriteria filter pada Grid # --------------
    var FrmData_viJadwalHemodialisa = new Ext.Panel
    (
		{
			title: NamaForm_viJadwalHemodialisa,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viJadwalHemodialisa,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ PanelTabJadwalHemodialisa,
					PanelGridJadwalHemodialisa,PanelGriHasilJadwalHemodialisa],
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAddJenis_viJadwalHemodialisa',
						handler: function(){
							AddNewJadwalHemodialisa();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viJadwalHemodialisa',
						handler: function()
						{
							loadMask.show();
							dataSave_viJadwalHemodialisa();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viJadwalHemodialisa',
						handler: function()
						{
							Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
								if (button == 'yes'){
									loadMask.show();
									dataDelete_viJadwalHemodialisa();
								}
							});
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_viJadwalHemodialisa',
						handler: function()
						{
							dataSource_viJadwalHemodialisa.removeAll();
							dataSourceHasilPradialisa_viJadwalHemodialisa.removeAll();
							dataSourceHasilDialisa_viJadwalHemodialisa.removeAll();
							dataSourceHasilPascadialisa_viJadwalHemodialisa.removeAll();
							dataGriJadwalHemodialisa();
						}
					},
					{
						xtype: 'tbseparator'
					}
					
				]
			}
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_viJadwalHemodialisa;
    //-------------- # End form filter # --------------
}

function PanelInputJadwalHemodialisa(rowdata){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 75,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'No. Medrec'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 0,
								xtype: 'textfield',
								id:'cboAutoCompKdPasien_JadwalHemodialisa',
								width:120,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) //ketika nama pasien di enter, maka akan ditampilkan data sesuai pencarian
										{
											var tmpNoIIMedrec = Ext.get('cboAutoCompKdPasien_JadwalHemodialisa').getValue();
											if (tmpNoIIMedrec.length !== 0 && tmpNoIIMedrec.length < 10)
											{
												var tmpgetNoIIMedrec = formatnomedrec(Ext.get('cboAutoCompKdPasien_JadwalHemodialisa').getValue())
												Ext.getCmp('cboAutoCompKdPasien_JadwalHemodialisa').setValue(tmpgetNoIIMedrec);
												// getItemGridSearch(tmpgetNoIIMedrec);
												dataGridSearchJadwalHemodialisa(tmpgetNoIIMedrec);
											} else
											{
												if (tmpNoIIMedrec.length === 10)
												{
													dataGridSearchJadwalHemodialisa(tmpNoIIMedrec);
													// GetDataPasien(tanggal,asal_pasien,tmpNoIIMedrec);
												} 
											}
										} 						
									}
								}
							}, 
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'Nama'
							},
							{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							NamaPasien_JadwalHemodialisa = new Nci.form.Combobox.autoComplete({
								x: 130,
								y: 30,
								width	: 250,
								id:'cboAutoCompNamaPasien_JadwalHemodialisa',
								store	: ArrayStoreNamaPasien,
								select	: function(a,b,c){
									Ext.getCmp('cboAutoCompKdPasien_JadwalHemodialisa').setValue(b.data.kd_pasien);
									Ext.getCmp('cboAutoCompKdPasien_JadwalHemodialisa').focus();
									//dataGridSearchJadwalHemodialisa(b.data.kd_pasien);
								},
								/* onShowList:function(a){
									dataSource_viJadwalHemodialisa.removeAll();
									
									var recs=[],
									recType=dataSource_viJadwalHemodialisa.recordType;
										
									for(var i=0; i<a.length; i++){
										recs.push(new recType(a[i]));
									}
									dataSource_viJadwalHemodialisa.add(recs);
									
								}, */
								insert	: function(o){
									return {
										kd_pasien       : o.kd_pasien,
										nama			: o.nama,
										text			:  '<table style="font-size: 11px;"><tr><td width="80">'+o.kd_pasien+'</td><td width="170">'+o.nama+'</td></tr></table>'
									}
								},
								url		: baseURL + "index.php/hemodialisa/functionJadwalHemodialisa/getPasien",
								valueField: 'nama',
								displayField: 'text',
								listWidth: 250,
								/* listeners: {
									specialkey: function(){
										if(Ext.EventObject.getKey()==13){
											dataGridSearchJadwalHemodialisa(Ext.getCmp('cboAutoCompKdPasien_JadwalHemodialisa').getValue(),NamaPasien_JadwalHemodialisa.getValue());
										}
									}
								}  */
							}),	
							/* {
								x: 130,
								y: 30,
								xtype: 'textfield',
								id:'txtNamaPasien_JadwalHemodialisa',
								width:250,
								listeners: {
									specialkey: function(){
										if(Ext.EventObject.getKey()==13){
											dataGridSearchJadwalHemodialisa(Ext.getCmp('cboAutoCompKdPasien_JadwalHemodialisa').getValue(),NamaPasien_JadwalHemodialisa);
										}
									}
								}
							}, */
							{
								x: 130,
								y: 55,
								xtype: 'label',
								text: '*) Tekan enter untuk mencari',
								style:{'font-size':'9'},
							},
							// ********** KANAN *************
							{
								x: 400,
								y: 0,
								xtype: 'label',
								text: 'Hari'
							},
							{
								x: 460,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							cboHari_JadwalHemodialisa(),
							{
								x: 400,
								y: 30,
								xtype: 'label',
								text: 'Jam'
							},
							{
								x: 460,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							{
								x: 470,
								y: 30,
								xtype: 'textfield',
								id:'txtJam_JadwalHemodialisa',
								width:40,
								emptyText:'Jam',
								maxLength:2,
								style:{'text-align':'right'},
							},
							{
								x: 515,
								y: 30,
								xtype: 'textfield',
								id:'txtMenit_JadwalHemodialisa',
								width:40,
								emptyText:'Menit',
								maxLength:2,
								style:{'text-align':'right'},
							},
							{
								x: 557,
								y: 40,
								xtype: 'label',
								text: '*) Format 24 Jam',
								style:{'font-size':'9'},
							},
						]
					}
				]
			}
		]		
	};
        return items;
}


function gridHasilPradialisa_JadwalHemodialisa(){
	var FieldPradialisa_viJadwalHemodialisa = 
	[
		'kd_item','item_hd','kd_dia','dialisa','no_dia','no_ref','deskripsi','nilai'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSourceHasilPradialisa_viJadwalHemodialisa = new WebApp.DataStore
	({
        fields: FieldPradialisa_viJadwalHemodialisa
    });
	GridHasilPradialisa_viJadwalHemodialisa = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			id:'GridHasilPradialisa_viJadwalHemodialisa',
			title: 'PRADIALISA',
			store: dataSourceHasilPradialisa_viJadwalHemodialisa,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:true,
			height:150,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.CellSelectionModel
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							
						}
					}
				}
			),
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header: 'Kode Item',
						dataIndex: 'kd_item',
						hideable:false,
						menuDisabled: true,
						width: 40
						
					},
					//-------------- ## --------------
					{
						header: 'Item',
						dataIndex: 'item_hd',
						hideable:false,
						menuDisabled: true,
						width: 90
					},
					//-------------- ## --------------
					{
						header: 'Test',
						dataIndex: 'deskripsi',
						hideable:false,
						menuDisabled: true,
						width: 40
					},
					//-------------- ## --------------
					{
						header: 'Hasil',
						dataIndex: 'nilai',
						hideable:false,
						width: 40
					},
					//-------------- ## --------------
				]
			),
			viewConfig: 
			{
				forceFit: true
			}
		}
	)
	return GridHasilPradialisa_viJadwalHemodialisa;
}

function gridHasilDialisa_JadwalHemodialisa(){
	var FieldDialisa_viJadwalHemodialisa = 
	[
		'kd_item','item_hd','kd_dia','dialisa','no_dia','no_ref','deskripsi','nilai'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSourceHasilDialisa_viJadwalHemodialisa = new WebApp.DataStore
	({
        fields: FieldDialisa_viJadwalHemodialisa
    });
	GridHasilDialisa_viJadwalHemodialisa = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			id:'GridHasilDialisa_viJadwalHemodialisa',
			title: 'DIALISA',
			store: dataSourceHasilDialisa_viJadwalHemodialisa,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:true,
			height:150,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.CellSelectionModel
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							
						}
					}
				}
			),
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header: 'Kode Item',
						dataIndex: 'kd_item',
						hideable:false,
						menuDisabled: true,
						width: 40
						
					},
					//-------------- ## --------------
					{
						header: 'Item',
						dataIndex: 'item_hd',
						hideable:false,
						menuDisabled: true,
						width: 90
					},
					//-------------- ## --------------
					{
						header: 'Test',
						dataIndex: 'deskripsi',
						hideable:false,
						menuDisabled: true,
						width: 40
					},
					//-------------- ## --------------
					{
						header: 'Hasil',
						dataIndex: 'nilai',
						hideable:false,
						width: 40
					},
					//-------------- ## --------------
				]
			),
			viewConfig: 
			{
				forceFit: true
			}
		}
	)
	return GridHasilDialisa_viJadwalHemodialisa;
}

function gridHasilPascadialisa_JadwalHemodialisa(){
	var FieldPascaialisa_viJadwalHemodialisa = 
	[
		'kd_item','item_hd','kd_dia','dialisa','no_dia','no_ref','deskripsi','nilai'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSourceHasilPascadialisa_viJadwalHemodialisa = new WebApp.DataStore
	({
        fields: FieldPascaialisa_viJadwalHemodialisa
    });
	GridHasilPascadialisa_viJadwalHemodialisa = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			id:'GridHasilPascadialisa_viJadwalHemodialisa',
			title: 'PASCADIALISA',
			store: dataSourceHasilPascadialisa_viJadwalHemodialisa,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:true,
			height:150,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.CellSelectionModel
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							
						}
					}
				}
			),
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header: 'Kode Item',
						dataIndex: 'kd_item',
						hideable:false,
						menuDisabled: true,
						width: 40
						
					},
					//-------------- ## --------------
					{
						header: 'Item',
						dataIndex: 'item_hd',
						hideable:false,
						menuDisabled: true,
						width: 90
					},
					//-------------- ## --------------
					{
						header: 'Test',
						dataIndex: 'deskripsi',
						hideable:false,
						menuDisabled: true,
						width: 40
					},
					//-------------- ## --------------
					{
						header: 'Hasil',
						dataIndex: 'nilai',
						hideable:false,
						width: 40
					},
					//-------------- ## --------------
				]
			),
			viewConfig: 
			{
				forceFit: true
			}
		}
	)
	return GridHasilPascadialisa_viJadwalHemodialisa;
}

//------------------end---------------------------------------------------------

function cboHari_JadwalHemodialisa()
{
  var cboHari_JadwalHemodialisa = new Ext.form.ComboBox
	(
		{
                    id:'cboHari_JadwalHemodialisa',
                    x: 470,
                    y: 0,
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender:true,
                    mode: 'local',
                    width: 100,
                    emptyText:'',
					tabIndex:5,
					editable: false,
                    store: new Ext.data.ArrayStore
                    (
						{
								id: 0,
								fields:
								[
									'Id',
									'displayText'
								],
						data: [[1, 'Senin'],[2, 'Selasa'],[3, 'Rabu'],[4, 'Kamis'],[5, "Jum'at"],[6, 'Sabtu'],[7, 'Minggu']]
						}
                    ),
                    valueField: 'Id',
                    displayField: 'displayText',
                    listeners:
                    {
						'select': function(a,b,c)
						{
							/* selectCountStatusPostingApotekReturRWJ=b.data.displayText;
							tmpkriteria = getCriteriaCariApotekReturRWJ();
							refreshReturApotekRWJ(tmpkriteria); */

						}
                    }
		}
	);
	return cboHari_JadwalHemodialisa;
};

/* function comboAutoCompKdPasien_JadwalHemodialisa(){
	var Field = ['kd_pasien','nama'];
	dsKodePasien_JadwalHemodialisa = new WebApp.DataStore({fields: Field});
	loadDataKodePasien_JadwalHemodialisa();
	cboAutoCompKdPasien_JadwalHemodialisa = new Ext.form.ComboBox
	(
		{
			id: 'cboAutoCompKdPasien_JadwalHemodialisa',
			typeAhead: true,
			x: 130,
			y: 0,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			hideTrigger		: true,
			store: dsKodePasien_JadwalHemodialisa,
			valueField: 'kd_pasien',
			displayField: 'kd_pasien',
			emptyText: '',
			width:150,
			listeners:
			{
				'select': function(a, b, c)
				{
					Ext.getCmp('txtNamaPasien_JadwalHemodialisa').setValue(b.data.nama)
				},
				specialkey: function(){
					if(Ext.EventObject.getKey()==13){
						dataGridSearchJadwalHemodialisa(Ext.getCmp('cboAutoCompKdPasien_JadwalHemodialisa').getValue(),NamaPasien_JadwalHemodialisa);
					}
				},
				keyUp: function(a,b,c){
					
					if(  b.getKey()!=127 ){
						clearTimeout(this.time);
				
						this.time=setTimeout(function(){
							if(cboAutoCompKdPasien_JadwalHemodialisa.lastQuery != '' ){
								var value="";
								
								if (value!=cboAutoCompKdPasien_JadwalHemodialisa.lastQuery)
								{
									if (a.rendered && a.innerList != null) {
										a.innerList.update(a.loadingText ? '<div class="loading-indicator">' + a.loadingText + '</div>' : '');
										a.restrictHeight();
										a.selectedIndex = 0;
									}
									a.expand();
									Ext.Ajax.request({
										url: baseURL + "index.php/hemodialisa/functionJadwalHemodialisa/getComboKdPasien",
										params: {
											text:cboAutoCompKdPasien_JadwalHemodialisa.lastQuery,
										},
										failure: function(o){
											var cst = Ext.decode(o.responseText);
										},	    
										success: function(o) {
											cboAutoCompKdPasien_JadwalHemodialisa.store.removeAll();
											var cst = Ext.decode(o.responseText);

											for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
												var recs    = [],recType = dsKodePasien_JadwalHemodialisa.recordType;
												var o=cst['listData'][i];
										
												recs.push(new recType(o));
												dsKodePasien_JadwalHemodialisa.add(recs);
											}
											a.expand();
											if(dsKodePasien_JadwalHemodialisa.onShowList != undefined)
												dsKodePasien_JadwalHemodialisa.onShowList(cst[showVar]);
											if(cst['listData'].length>0){
													
												a.doQuery(a.allQuery, true);
												a.expand();
												a.selectText(value.length,value.length);
											}else{
											//	if (a.rendered && a.innerList != null) {
													a.innerList.update(a.loadingText ? '&nbsp; Data Tidak Ada' : '');
													a.restrictHeight();
													a.selectedIndex = 0;
												//}
											}
										}
									});
									value=cboAutoCompKdPasien_JadwalHemodialisa.lastQuery;
								}
							}
						},1000);
					}
				} 
			}
		}
	)
	return cboAutoCompKdPasien_JadwalHemodialisa;
}; */

function loadDataKodePasien_JadwalHemodialisa(param){
	if (param==='' || param===undefined) {
		param={
			text: '0',
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/hemodialisa/functionJadwalHemodialisa/getComboKdPasien",
		params: {
			text:param,
		},
		failure: function(o){
			loadMask.hide();
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cboAutoCompKdPasien_JadwalHemodialisa.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsKodePasien_JadwalHemodialisa.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsKodePasien_JadwalHemodialisa.add(recs);
				console.log(o);
			}
		}
	});
};

function dataGriJadwalHemodialisa(kd_pasien,nama){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/hemodialisa/functionJadwalHemodialisa/getItemGrid",
			params: {kd_pasien:kd_pasien, nama:nama},
			failure: function(o)
			{
				ShowPesanErrorJadwalHemodialisa('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dataSource_viJadwalHemodialisa.removeAll();
					var recs=[],
						recType=dataSource_viJadwalHemodialisa.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dataSource_viJadwalHemodialisa.add(recs);
					
					
					
					GridDataView_viJadwalHemodialisa.getView().refresh();
				}
				else 
				{
					ShowPesanErrorJadwalHemodialisa('Gagal membaca data obat', 'Error');
				};
			}
		}
		
	)
	
}
function dataGridSearchJadwalHemodialisa(kd_pasien,nama){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/hemodialisa/functionJadwalHemodialisa/getItemGridSearch",
			params: {kd_pasien:kd_pasien,nama:nama},
			failure: function(o)
			{
				ShowPesanErrorJadwalHemodialisa('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dataSource_viJadwalHemodialisa.removeAll();
					var recs=[],
						recType=dataSource_viJadwalHemodialisa.recordType;
					var get_nama='';	
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						get_nama = cst.listData[i].nama; 
					}
					dataSource_viJadwalHemodialisa.add(recs);
					// Ext.getCmp('cboAutoCompNamaPasien_JadwalHemodialisa').setValue(get_nama);
					GridDataView_viJadwalHemodialisa.getView().refresh();
					NamaPasien_JadwalHemodialisa.setValue(get_nama);
					// alert(get_nama);
				}
				else 
				{
					ShowPesanErrorJadwalHemodialisa('Gagal membaca data obat', 'Error');
				};
			}
		}
		
	)
	
}

function RefreshDataGridHasilPradialisa_JadwalHemodialisa(kd_pasien){
	Ext.Ajax.request
	({
		url: baseURL + "index.php/hemodialisa/functionJadwalHemodialisa/getItemGridHasilPra",
		params: {kd_pasien:kd_pasien},
		failure: function(o)
		{
			ShowPesanErrorJadwalHemodialisa('Hubungi Admin', 'Error');
		},	
		success: function(o) 
		{
			var cst = Ext.decode(o.responseText);
			if (cst.success === true) 
			{
				// ********** PRADIALISA ***************
				dataSourceHasilPradialisa_viJadwalHemodialisa.removeAll();
				var recs=[],
					recType=dataSourceHasilPradialisa_viJadwalHemodialisa.recordType;
					
				for(var i=0; i<cst.listDataPra.length; i++){
					recs.push(new recType(cst.listDataPra[i]));
				}
				dataSourceHasilPradialisa_viJadwalHemodialisa.add(recs);
				GridHasilPradialisa_viJadwalHemodialisa.getView().refresh();
			}
			else 
			{
				ShowPesanErrorJadwalHemodialisa('Gagal membaca data obat', 'Error');
			};
		}
	})
}

function RefreshDataGridHasilDialisa_JadwalHemodialisa(kd_pasien){
	Ext.Ajax.request
	({
		url: baseURL + "index.php/hemodialisa/functionJadwalHemodialisa/getItemGridHasilDia",
		params: {kd_pasien:kd_pasien},
		failure: function(o)
		{
			ShowPesanErrorJadwalHemodialisa('Hubungi Admin', 'Error');
		},	
		success: function(o) 
		{
			var cst = Ext.decode(o.responseText);
			if (cst.success === true) 
			{
				// ********** DIALISA ***************
				dataSourceHasilDialisa_viJadwalHemodialisa.removeAll();
				var recs=[],
					recType=dataSourceHasilDialisa_viJadwalHemodialisa.recordType;
					
				for(var i=0; i<cst.listDataDia.length; i++){
					recs.push(new recType(cst.listDataDia[i]));
				}
				dataSourceHasilDialisa_viJadwalHemodialisa.add(recs);
				GridHasilDialisa_viJadwalHemodialisa.getView().refresh();
			}
			else 
			{
				ShowPesanErrorJadwalHemodialisa('Gagal membaca data obat', 'Error');
			};
		}
	})
}

function RefreshDataGridHasilPascadialisa_JadwalHemodialisa(kd_pasien){
	Ext.Ajax.request
	({
		url: baseURL + "index.php/hemodialisa/functionJadwalHemodialisa/getItemGridHasilPasca",
		params: {kd_pasien:kd_pasien},
		failure: function(o)
		{
			ShowPesanErrorJadwalHemodialisa('Hubungi Admin', 'Error');
		},	
		success: function(o) 
		{
			var cst = Ext.decode(o.responseText);
			if (cst.success === true) 
			{				
				// ********** PASCADIALISA ***************
				dataSourceHasilPascadialisa_viJadwalHemodialisa.removeAll();
				var recs=[],
					recType=dataSourceHasilPascadialisa_viJadwalHemodialisa.recordType;
					
				for(var i=0; i<cst.listDataPasca.length; i++){
					recs.push(new recType(cst.listDataPasca[i]));
				}
				dataSourceHasilPascadialisa_viJadwalHemodialisa.add(recs);
				GridHasilPascadialisa_viJadwalHemodialisa.getView().refresh();
			}
			else 
			{
				ShowPesanErrorJadwalHemodialisa('Gagal membaca data obat', 'Error');
			};
		}
	})
}


function dataSave_viJadwalHemodialisa(){
	if (ValidasiSaveJadwalHemodialisa(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/hemodialisa/functionJadwalHemodialisa/save",
				params: getParamSaveJadwalHemodialisa(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorJadwalHemodialisa('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoJadwalHemodialisa('Berhasil menyimpan data ini','Information');
						dataSource_viJadwalHemodialisa.removeAll();
						dataGriJadwalHemodialisa(cst.kd_pasien);
						alert(cst.kd_pasien);
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorJadwalHemodialisa('Gagal menyimpan data ini', 'Error');
						dataSource_viJadwalHemodialisa.removeAll();
						dataGriJadwalHemodialisa();
					};
				}
			}
			
		)
	}
}

function dataDelete_viJadwalHemodialisa(){
	if (ValidasiDeleteJadwalHemodialisa(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/hemodialisa/functionJadwalHemodialisa/delete",
				params: getParamDeleteJadwalHemodialisa(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorJadwalHemodialisa('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoJadwalHemodialisa('Berhasil menghapus data ini','Information');
						AddNewJadwalHemodialisa()
						dataSource_viJadwalHemodialisa.removeAll();
						dataGriJadwalHemodialisa();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorJadwalHemodialisa('Gagal menghapus data ini', 'Error');
						dataSource_viJadwalHemodialisa.removeAll();
						dataGriJadwalHemodialisa();
					};
				}
			}
			
		)
	}
}

function AddNewJadwalHemodialisa(){
	Ext.getCmp('cboHari_JadwalHemodialisa').setValue('');
	Ext.getCmp('cboAutoCompKdPasien_JadwalHemodialisa').setValue('');
	NamaPasien_JadwalHemodialisa.setValue('');
	Ext.getCmp('txtJam_JadwalHemodialisa').setValue('');
	Ext.getCmp('txtMenit_JadwalHemodialisa').setValue('');
	dataSource_viJadwalHemodialisa.removeAll();
	dataSourceHasilDialisa_viJadwalHemodialisa.removeAll();
	dataSourceHasilPascadialisa_viJadwalHemodialisa.removeAll();
	dataSourceHasilPradialisa_viJadwalHemodialisa.removeAll();
};

function DataInitJadwalHemodialisa(rowdata){
	Ext.getCmp('cboHari_JadwalHemodialisa').setValue(rowdata.groups.substring(0, 4));
	Ext.getCmp('cboPrinter_JadwalHemodialisa').setValue(rowdata.alamat_printer);
	Ext.getCmp('cboUnitFar_JadwalHemodialisa').setValue(rowdata.kd_unit_far);
};

function getParamSaveJadwalHemodialisa(){
	var jam;
	jam = Ext.getCmp('txtJam_JadwalHemodialisa').getValue() + ':' + Ext.getCmp('txtMenit_JadwalHemodialisa').getValue();
	var	params =
	{
		hari:Ext.getCmp('cboHari_JadwalHemodialisa').getValue(),
		kd_pasien:Ext.getCmp('cboAutoCompKdPasien_JadwalHemodialisa').getValue(),
		jam:jam,
	}
   
    return params
};

function getParamDeleteJadwalHemodialisa(){
	var	params =
	{
		kd_pasien:Ext.getCmp('cboAutoCompKdPasien_JadwalHemodialisa').getValue(),
		hari:currentSelectHari_JadwalHemodialisa,
	}
   
    return params
};

function ValidasiSaveJadwalHemodialisa(modul,mBolHapus){
	var x = 1;
	var valJam = parseInt(Ext.getCmp('txtJam_JadwalHemodialisa').getValue());
	var valMenit = parseInt(Ext.getCmp('txtMenit_JadwalHemodialisa').getValue());
	if (valJam > 23)
	{
		loadMask.hide();
		ShowPesanWarningJadwalHemodialisa('Jam tidak boleh lebih dari angka 23', 'Warning');
		x = 0;
	}
	else if (valMenit > 59)
	{
		loadMask.hide();
		ShowPesanWarningJadwalHemodialisa('Menit tidak boleh lebih dari angka 59', 'Warning');
		x = 0;
	}
	
	if(Ext.get('cboHari_JadwalHemodialisa').getValue() === ''){
		loadMask.hide();
		ShowPesanWarningJadwalHemodialisa('Hari masih kosong', 'Warning');
		x = 0;
	} 
	
	if(Ext.get('cboAutoCompKdPasien_JadwalHemodialisa').getValue() === ''){
		loadMask.hide();
		ShowPesanWarningJadwalHemodialisa('No. Medrec belum di isi', 'Warning');
		x = 0;
	} 
	
	if(Ext.get('txtJam_JadwalHemodialisa').getValue() === ''){
		loadMask.hide();
		ShowPesanWarningJadwalHemodialisa('Jam masih kosong', 'Warning');
		x = 0;
	} 
	
	if(Ext.get('txtMenit_JadwalHemodialisa').getValue() === ''){
		loadMask.hide();
		ShowPesanWarningJadwalHemodialisa('Menit masih kosong', 'Warning');
		x = 0;
	} 
	return x;
};

function ValidasiDeleteJadwalHemodialisa(modul,mBolHapus){
	var x = 1;
	if(currentSelectKdPasien_JadwalHemodialisa === '' || currentSelectKdPasien_JadwalHemodialisa === undefined){
		loadMask.hide();
		ShowPesanWarningJadwalHemodialisa('Pilih data yang akan dihapus!', 'Warning');
		x = 0;
	} 
	
	if(currentSelectHari_JadwalHemodialisa === '' || currentSelectHari_JadwalHemodialisa === undefined){
		loadMask.hide();
		ShowPesanWarningJadwalHemodialisa('Pilih data yang akan dihapus!', 'Warning');
		x = 0;
	} 
	
	return x;
};



function ShowPesanWarningJadwalHemodialisa(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorJadwalHemodialisa(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoJadwalHemodialisa(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};