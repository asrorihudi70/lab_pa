var dataSource_viPenJasHemodialisa;
var NamaForm_viPenJasHemodialisa="Penata Jasa Hemodialisa";
var now_viPenJasHemodialisa= new Date();
var tglGridBawah = now_viPenJasHemodialisa.format("d/M/Y");
var rowSelected_viPenJasHemodialisa;
var GridDataView_viPenJasHemodialisa;
var selectSetPerseorangan_PenJasHemodialisa;
var selectSetPerusahaan_PenJasHemodialisa;
var selectSetAsuransi_PenJasHemodialisa;
var selectSetGDarah_PenJasHemodialisa;
var selectSetJK_PenJasHemodialisa;
var shifHemodialisa;
var cboAutoCompKdPasien_PenJasHemodialisa;

var dsPerorangan_PenJasHemodialisa;
var dsPerusahaan_PenJasHemodialisa;
var dsAsuransi_PenJasHemodialisa;
var dsKodePasien_PenJasHemodialisa;
var kd_unit_hd='';	
	


var CurrentData_viPenJasHemodialisa =
{
	data: Object,
	details: Array,
	row: 0
};

var PenJasHemodialisa={};
PenJasHemodialisa.form={};
PenJasHemodialisa.func={};
PenJasHemodialisa.vars={};
PenJasHemodialisa.func.parent=PenJasHemodialisa;
PenJasHemodialisa.form.ArrayStore={};
PenJasHemodialisa.form.ComboBox={};
PenJasHemodialisa.form.DataStore={};
PenJasHemodialisa.form.Record={};
PenJasHemodialisa.form.Form={};
PenJasHemodialisa.form.Grid={};
PenJasHemodialisa.form.Panel={};
PenJasHemodialisa.form.TextField={};
PenJasHemodialisa.form.Button={};

PenJasHemodialisa.form.ArrayStore.kdpasien= new Ext.data.ArrayStore({
	id: 0,
	fields: [
				'no_transaksi','kd_kasir','tgl_transaksi','kd_spesial','kamar','tgl_lahir','kd_pasien','nama','nama_unit_asli',
				'alamat','kd_dokter','kd_unit','nama_unit','urut_masuk','dokter','Jenis_kelamin','gol_darah','kd_customer','customer',
				'no_kamar','kd_spesial','tgl','kd_tarif','kelpasien'
			],
	data: []
});
PenJasHemodialisa.form.ArrayStore.nama= new Ext.data.ArrayStore({
	id: 0,
	fields: [
				'no_transaksi','kd_kasir','tgl_transaksi','kd_spesial','kamar','tgl_lahir','kd_pasien','nama','nama_unit_asli',
				'alamat','kd_dokter','kd_unit','nama_unit','urut_masuk','dokter','Jenis_kelamin','gol_darah','kd_customer','customer',
				'no_kamar','kd_spesial','tgl','kd_tarif','kelpasien'
			],
	data: []
});

PenJasHemodialisa.form.ArrayStore.notransaksi= new Ext.data.ArrayStore({
	id: 0,
	fields: [
				'no_transaksi','kd_kasir','tgl_transaksi','kd_spesial','kamar','tgl_lahir','kd_pasien','nama','nama_unit_asli',
				'alamat','kd_dokter','kd_unit','nama_unit','urut_masuk','dokter','Jenis_kelamin','gol_darah','kd_customer','customer',
				'no_kamar','kd_spesial','tgl','kd_tarif','kelpasien'
			],
	data: []
});

PenJasHemodialisa.form.ArrayStore.produk=new Ext.data.ArrayStore({
	id: 0,
	fields: [
				'uraian','kd_tarif','kd_produk','tgl_transaksi','tgl_berlaku','harga','qty'
			],
	data: []
});

CurrentPage.page = dataGrid_viPenJasHemodialisa(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
shortcut.set({
		code:'penata_jasa',
		list:[
			{
				key:'ctrl+s',
				fn:function(){
					Ext.getCmp('btnSimpan_viPenJasHemodialisa').el.dom.click();
				}
			}
		]
	});
function dataGrid_viPenJasHemodialisa(mod_id_viPenJasHemodialisa){	
    var FieldMaster_viPenJasHemodialisa = 
	[
		'kd_vendor', 'vendor', 'contact', 'alamat', 'kota', 'telepon1', 'telepon2',
		'fax', 'kd_pos', 'negara', 'norek', 'term'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viPenJasHemodialisa = new WebApp.DataStore
	({
        fields: FieldMaster_viPenJasHemodialisa
    });
	
	shiftbagian_PenJasHemodialisa();
	//loadDataKodePasien_PenJasHemodialisa();
	
	
    // Grid Apotek Perencanaan # --------------
	GridDataView_viPenJasHemodialisa = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: 'Item Test',
			store: dataSource_viPenJasHemodialisa,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:true,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.CellSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						cellselect: function(sm, row, rec)
						{
							rowSelected_viPenJasHemodialisa = dataSource_viPenJasHemodialisa.getAt(row);
						}
					}
				}
			),
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viPenJasHemodialisa = dataSource_viPenJasHemodialisa.getAt(ridx);
					if (rowSelected_viPenJasHemodialisa != undefined)
					{
						
					}
					else
					{
						
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Setup vendor
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header: 'kd_tarif',
						dataIndex: 'kd_tarif',
						width:250,
						menuDisabled:true,
						hidden :true

					},
					{
						header: 'Kode Produk',
						dataIndex: 'kd_produk',
						width:100,
						menuDisabled:true,
						hidden:true
					},
					{	id:'nama_pemereksaaan_lab',
						dataIndex: 'deskripsi',
						header: 'Nama Pemeriksaan',
						sortable: true,
						menuDisabled:true,
						width: 320,
						editor:new Nci.form.Combobox.autoComplete({
							store	: PenJasHemodialisa.form.ArrayStore.produk,
							select	: function(a,b,c){
								var line = GridDataView_viPenJasHemodialisa.getSelectionModel().selection.cell[0];
								dataSource_viPenJasHemodialisa.data.items[line].data.deskripsi=b.data.deskripsi;
								dataSource_viPenJasHemodialisa.data.items[line].data.uraian=b.data.uraian;
								dataSource_viPenJasHemodialisa.data.items[line].data.kd_tarif=b.data.kd_tarif;
								dataSource_viPenJasHemodialisa.data.items[line].data.kd_produk=b.data.kd_produk;
								dataSource_viPenJasHemodialisa.data.items[line].data.tgl_transaksi=b.data.tgl_transaksi;
								dataSource_viPenJasHemodialisa.data.items[line].data.tgl_berlaku=b.data.tgl_berlaku;
								dataSource_viPenJasHemodialisa.data.items[line].data.harga=b.data.harga;
								dataSource_viPenJasHemodialisa.data.items[line].data.qty=b.data.qty;
								dataSource_viPenJasHemodialisa.data.items[line].data.jumlah=b.data.jumlah;

								GridDataView_viPenJasHemodialisa.getView().refresh();
								
							},
							insert	: function(o){
								// console.log(o);
								return {
									uraian        	: o.uraian,
									kd_tarif 		: o.kd_tarif,
									kd_produk		: o.kd_produk,
									tgl_transaksi	: o.tgl_transaksi,
									tgl_berlaku		: o.tgl_berlaku,
									harga			: o.harga,
									qty				: o.qty,
									deskripsi		: o.deskripsi,
									jumlah			: o.jumlah,
									text			:  '<table style="font-size: 11px;"><tr><td width="60">'+o.kd_produk+'</td><td width="150">'+o.deskripsi+'</td></tr></table>'
								}
							},
							param	: function(){
							var kd_cus_gettarif;
							if(Ext.get('cboKelPasien_PenJasHemodialisa').getValue()=='Perseorangan'){
								kd_cus_gettarif=Ext.getCmp('cboPerseorangan_PenJasHemodialisa').getValue();
							}else if(Ext.get('cboKelPasien_PenJasHemodialisa').getValue()=='Perusahaan'){
								kd_cus_gettarif=Ext.getCmp('cboPerusahaan_PenJasHemodialisa').getValue();
							}else {
								kd_cus_gettarif=Ext.getCmp('cboAsuransi_PenJasHemodialisa').getValue();
							}
							var params={};
								params['kd_unit']="";
								params['kd_customer']=kd_cus_gettarif;
								params['kd_unit_hd']=kd_unit_hd;
								return params;
							},
							url		: baseURL + "index.php/hemodialisa/functionPenJasHemodialisa/getProduk",
							valueField: 'deskripsi',
							displayField: 'text',
							listWidth: 210
						})
					},
					{
						header: 'Tanggal Transaksi',
						dataIndex: 'tgl_transaksi',
						width: 130,
						menuDisabled:true,
						renderer: function(v, params, record)
						{
							if(record.data.tgl_transaksi == undefined){
								record.data.tgl_transaksi=tglGridBawah;
								return record.data.tgl_transaksi;
							} else{
								if(record.data.tgl_transaksi.substring(5, 4) == '-'){
									return ShowDate(record.data.tgl_transaksi);
								} else{
									var tgl=record.data.tgl_transaksi.split("/");

									if(tgl[2].length == 4 && isNaN(tgl[1])){
										return record.data.tgl_transaksi;
									} else{
										return ShowDate(record.data.tgl_transaksi);
									}
								}

							}
						}
					},
					{
						header: 'Tanggal Berlaku',
						dataIndex: 'tgl_berlaku',
						width: 130,
						menuDisabled:true,
						hidden: true,
						renderer: function(v, params, record)
						{
							if(record.data.tgl_berlaku == undefined || record.data.tgl_berlaku == null){
								record.data.tgl_berlaku=tglGridBawah;
								return record.data.tgl_berlaku;
							} else{
								if(record.data.tgl_berlaku.substring(5, 4) == '-'){
									return ShowDate(record.data.tgl_berlaku);
								} else{
									var tglb=record.data.tgl_berlaku.split("-");

									if(tglb[2].length == 4 && isNaN(tglb[1])){
										return record.data.tgl_berlaku;
									} else{
										return ShowDate(record.data.tgl_berlaku);
									}
								}

							}
						}
					},
					{
						header: 'Harga',
						align: 'right',
						hidden: false,
						menuDisabled:true,
						dataIndex: 'harga',
						width:100,
						renderer: function(v, params, record)
						{
							return formatCurrency(record.data.harga);
						}
					},
					{	id:'qty_lab',
						header: 'Qty',
						width:91,
						align: 'right',
						menuDisabled:true,
						dataIndex: 'qty',
						editor: new Ext.form.NumberField ({}),
					},
					{
						header: 'Dokter',
						width:91,
						menuDisabled:true,
						dataIndex: 'jumlah',
						hidden: true,

					},
					//-------------- ## --------------
					
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viPenJasHemodialisa',
				items: 
				[
					{
						xtype: 'button',
						text: 'Tambah Item Pemeriksaan',
						iconCls: 'Edit_Tr',
						tooltip: 'Tambah Data',
						disabled:true,
						id: 'btnTambahItemPemeriksaan_viPenJasHemodialisa',
						handler: function(sm, row, rec)
						{
							if(Ext.getCmp('cboUnitHD_PenJasHemodialisa').getValue() == 'Pilih...'){
								ShowPesanWarningPenJasHemodialisa('Pilih Unit HD!','Warning');
							}else{
								var records = new Array();
								records.push(new dataSource_viPenJasHemodialisa.recordType());
								dataSource_viPenJasHemodialisa.add(records);
								Ext.getCmp('btnHpsBrsItem_viPenJasHemodialisa').enable();		
							}
												
						}
					},
					{
						id:'btnHpsBrsItem_viPenJasHemodialisa',
						text: 'Hapus Baris',
						tooltip: 'Hapus Baris',
						disabled:true,
						iconCls: 'RemoveRow',
						handler: function()
						{
							if (dataSource_viPenJasHemodialisa.getCount() > 0 ) {
								if (rowSelected_viPenJasHemodialisa != undefined)
								{
									Ext.Msg.confirm('Warning', 'Apakah item pemeriksaan ini akan dihapus?', function(button){
										if (button == 'yes'){
											if(rowSelected_viPenJasHemodialisa != undefined) {
												var line = GridDataView_viPenJasHemodialisa.getSelectionModel().selection.cell[0];
												dataSource_viPenJasHemodialisa.removeAt(line);
												GridDataView_viPenJasHemodialisa.getView().refresh();
											}
										}
									})
								} else {
									ShowPesanWarningPenJasHemodialisa('Pilih record ','Hapus data');
								}
							}
						}
					},
					//-------------- ## --------------
				]
			},
			viewConfig: 
			{
				forceFit: true
			}
		}
	)	
	var PanelTabPenJasHemodialisa = new Ext.FormPanel({
        labelAlign: 'top',
		layout:'form',
		border: false,  
		autoHeight:true,
        title: '',
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [PanelInputDataPasien_PenJasHemodialisa()]
		
	})
	// Kriteria filter pada Grid # --------------
    var FrmData_viPenJasHemodialisa = new Ext.Panel
    (
		{
			title: NamaForm_viPenJasHemodialisa,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viPenJasHemodialisa,
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ PanelTabPenJasHemodialisa,
					GridDataView_viPenJasHemodialisa],
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAddJenis_viPenJasHemodialisa',
						handler: function(){
							AddNewPenJasHemodialisa();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viPenJasHemodialisa',
						handler: function()
						{
							loadMask.show();
							dataSave_viPenJasHemodialisa();
						}
					},
					{
						xtype: 'tbseparator'
					}
					
				]
			}
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_viPenJasHemodialisa;
    //-------------- # End form filter # --------------
}

function PanelInputDataPasien_PenJasHemodialisa(){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 10px 10px 10px 10px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 210,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'Unit HD'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							comboUnitHD_PenJasHemodialisa(),
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'No Transaksi'
							},
							{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							PenJasHemodialisa.vars.notransaksi=new Nci.form.Combobox.autoCompleteId({
								x: 130,
								y: 30,
								tabIndex:1,
								width: 100,
								id		: 'txtAutoCompNotransaksi_PenJasHemodialisa',
								store	: PenJasHemodialisa.form.ArrayStore.notransaksi,
								select	: function(a,b,c){
									PenJasHemodialisa.vars.kdpasien.setValue(b.data.kd_pasien);
									PenJasHemodialisa.vars.nama.setValue(b.data.nama);
									dataGriPenJasHemodialisa(b.data.no_transaksi,b.data.kd_kasir,b);
									
									// # ---DISABLE---#
									PenJasHemodialisa.vars.kdpasien.disable();
									PenJasHemodialisa.vars.nama.disable();
									Ext.getCmp('txtUnit_PenJasHemodialisa').disable();
									Ext.getCmp('dftTglLahir_PenJasHemodialisa').disable();
									Ext.getCmp('txtAlamat_PenJasHemodialisa').disable();
									Ext.getCmp('cboJK_PenJasHemodialisa').disable();
									Ext.getCmp('cboGolDarah_PenJasHemodialisa').disable();
									Ext.getCmp('cboKelPasien_PenJasHemodialisa').disable();
									Ext.getCmp('cboPerseorangan_PenJasHemodialisa').disable();
									Ext.getCmp('cboPerusahaan_PenJasHemodialisa').disable();
									Ext.getCmp('cboAsuransi_PenJasHemodialisa').disable();
									Ext.getCmp('txtDokterPengirim_PenJasHemodialisa').disable();
									
								},
								/* onShowList:function(a){
									dataSource_viPenJasHemodialisa.removeAll();
									
									var recs=[],
									recType=dataSource_viPenJasHemodialisa.recordType;
										
									for(var i=0; i<a.length; i++){
										recs.push(new recType(a[i]));
									}
									dataSource_viPenJasHemodialisa.add(recs);
									
								}, */
								width	: 120,
								insert	: function(o){
									return {
										no_transaksi		:o.no_transaksi,
										tgl_transaksi		:o.tgl_transaksi,
										kd_spesial			:o.kd_spesial,
										kamar				:o.kamar,
										tgl_lahir			:o.tgl_lahir,
										gol_darah			:o.gol_darah,
										jenis_kelamin		:o.jenis_kelamin,
										dokter				:o.dokter,
										urut_masuk			:o.urut_masuk,
										nama_unit			:o.nama_unit,
										alamat				:o.alamat,
										kd_pasien        	:o.kd_pasien,
										nama        		:o.nama,
										kd_dokter			:o.kd_dokter,
										kd_unit				:o.kd_unit,
										kd_customer			:o.kd_customer,
										kd_kasir			:o.kd_kasir,
										nama_kamar			:o.nama_kamar,
										no_kamar			:o.no_kamar,
										customer			:o.customer,
										nama_unit_asli		:o.nama_unit_asli,
										tgl					:o.tgl,
										kd_tarif			:o.kd_tarif,
										kelpasien			:o.kelpasien,
										text				:  '<table style="font-size: 11px;"><tr><td width="100">'+o.kd_pasien+'</td><td width="70" align="left">'+o.no_transaksi+'</td><td width="130">'+o.nama+'</td><th width="110" align="left"><b>'+o.nama_unit_asli+'</b></th><td width="90">'+o.tgl+'</td></tr></table>',
										kd_pasien	 		:o.kd_pasien
									}
								},
								param:function(){
									var a=0;
									return {
										tanggal:Ext.getCmp('dtfTglKunjung_PenJasHemodialisa').getValue(),
										a:0
									}
								},
								url		: baseURL + "index.php/hemodialisa/functionPenJasHemodialisa/getPasien",
								valueField: 'no_transaksi',
								displayField: 'text',
								listWidth: 350
							}),
							{
								x: 10,
								y: 60,
								xtype: 'label',
								text: 'No. Medrec'
							},
							{
								x: 120,
								y: 60,
								xtype: 'label',
								text: ':'
							},
							//comboAutoCompKdPasien_PenJasHemodialisa(),
							PenJasHemodialisa.vars.kdpasien=new Nci.form.Combobox.autoCompleteId({
								x: 130,
								y: 60,
								tabIndex:2,
								width: 120,
								id		: 'txtAutoCompKdPasien_PenJasHemodialisa',
								store	: PenJasHemodialisa.form.ArrayStore.kdpasien,
								select	: function(a,b,c){
									PenJasHemodialisa.vars.nama.setValue(b.data.nama);
									dataGriPenJasHemodialisa(b.data.no_transaksi,b.data.kd_kasir,b);
									
									// # ---DISABLE---#
									PenJasHemodialisa.vars.notransaksi.disable();
									PenJasHemodialisa.vars.nama.disable();
									Ext.getCmp('txtUnit_PenJasHemodialisa').disable();
									Ext.getCmp('dftTglLahir_PenJasHemodialisa').disable();
									Ext.getCmp('txtAlamat_PenJasHemodialisa').disable();
									Ext.getCmp('cboJK_PenJasHemodialisa').disable();
									Ext.getCmp('cboGolDarah_PenJasHemodialisa').disable();
									Ext.getCmp('cboKelPasien_PenJasHemodialisa').disable();
									Ext.getCmp('cboPerseorangan_PenJasHemodialisa').disable();
									Ext.getCmp('cboPerusahaan_PenJasHemodialisa').disable();
									Ext.getCmp('cboAsuransi_PenJasHemodialisa').disable();
									Ext.getCmp('txtDokterPengirim_PenJasHemodialisa').disable();
									
								},
								/* onShowList:function(a){
									dataSource_viPenJasHemodialisa.removeAll();
									
									var recs=[],
									recType=dataSource_viPenJasHemodialisa.recordType;
										
									for(var i=0; i<a.length; i++){
										recs.push(new recType(a[i]));
									}
									dataSource_viPenJasHemodialisa.add(recs);
									
								}, */
								width	: 120,
								insert	: function(o){
									return {
										no_transaksi		:o.no_transaksi,
										tgl_transaksi		:o.tgl_transaksi,
										kd_spesial			:o.kd_spesial,
										kamar				:o.kamar,
										tgl_lahir			:o.tgl_lahir,
										gol_darah			:o.gol_darah,
										jenis_kelamin		:o.jenis_kelamin,
										dokter				:o.dokter,
										urut_masuk			:o.urut_masuk,
										nama_unit			:o.nama_unit,
										alamat				:o.alamat,
										kd_pasien        	:o.kd_pasien,
										nama        		:o.nama,
										kd_dokter			:o.kd_dokter,
										kd_unit				:o.kd_unit,
										kd_customer			:o.kd_customer,
										kd_kasir			:o.kd_kasir,
										nama_kamar			:o.nama_kamar,
										no_kamar			:o.no_kamar,
										customer			:o.customer,
										nama_unit_asli		:o.nama_unit_asli,
										tgl					:o.tgl,
										kd_tarif			:o.kd_tarif,
										kelpasien			:o.kelpasien,
										text				:  '<table style="font-size: 11px;"><tr><td width="100">'+o.kd_pasien+'</td><td width="70" align="left">'+o.no_transaksi+'</td><td width="130">'+o.nama+'</td><td width="110" align="left">'+o.nama_unit_asli+'</td><td width="90">'+o.tgl+'</td></tr></table>',
										kd_pasien	 		:o.kd_pasien
									}
								},
								param:function(){
									var a=0;
									return {
										tanggal:Ext.getCmp('dtfTglKunjung_PenJasHemodialisa').getValue(),
										a:1
									}
								},
								url		: baseURL + "index.php/hemodialisa/functionPenJasHemodialisa/getPasien",
								valueField: 'kd_pasien',
								displayField: 'text',
								listWidth: 350
							}),
							{
								x: 10,
								y: 90,
								xtype: 'label',
								text: 'Unit Asal'
							},
							{
								x: 120,
								y: 90,
								xtype: 'label',
								text: ':'
							},
							{
								x:130,
								y:90,	
								xtype: 'textfield',
								name: 'txtUnit_PenJasHemodialisa',
								id: 'txtUnit_PenJasHemodialisa',
								tabIndex:3,
								readOnly:true,
								width: 180
							},
							{
								x: 10,
								y: 120,
								xtype: 'label',
								text: 'Dokter Pengirim'
							},
							{
								x: 120,
								y: 120,
								xtype: 'label',
								text: ':'
							},
							{
								x:130,
								y:120,	
								xtype: 'textfield',
								name: 'txtDokterPengirim_PenJasHemodialisa',
								id: 'txtDokterPengirim_PenJasHemodialisa',
								readOnly:true,
								tabIndex:3,
								width: 200
							},
							{
								x: 10,
								y: 150,
								xtype: 'label',
								text: 'Dokter Dialisa'
							},
							{
								x: 120,
								y: 150,
								xtype: 'label',
								text: ':'
							},
							mComboDOKTER_PenJasHemodialisa(),
							{
								x: 10,
								y: 180,
								xtype: 'label',
								text: 'Kelompok Pasien'
							},
							{
								x: 120,
								y: 180,
								xtype: 'label',
								text: ':'
							},
							comboKelompokPasien_PenJasHemodialisa(),
							comboPerseorangan_PenJasHemodialisa(),
							comboPerusahaan_PenJasHemodialisa(),
							comboAsuransi_PenJasHemodialisa(),
							{
								x: 440,
								y: 150,
								xtype: 'textfield',
								fieldLabel: 'Nama Peserta',
								maxLength: 200,
								name: 'txtNamaPesertaAsuransi_PenJasHemodialisa',
								id: 'txtNamaPesertaAsuransi_PenJasHemodialisa',
								width: 150,
								emptyText:'Nama Peserta Asuransi',
								hidden: true,
							},
							{
								x: 600,
								y: 150,
								xtype: 'textfield',
								fieldLabel: 'No. Askes',
								maxLength: 200,
								name: 'txtNoAskes_PenJasHemodialisa',
								id: 'txtNoAskes_PenJasHemodialisa',
								emptyText:'No Askes',
								width: 150,
								hidden: true,
							},
							{
								x: 760,
								y: 150,
								xtype: 'textfield',
								fieldLabel: 'No. SJP',
								maxLength: 200,
								name: 'txtNoSJP_PenJasHemodialisa',
								id: 'txtNoSJP_PenJasHemodialisa',
								emptyText:'No SJP',
								width: 150,
								hidden: true,
							},
							
							//-------------- # KANAN # --------------
							{
								x: 420,
								y: 0,
								xtype: 'label',
								text: 'Tanggal Kunjung'
							},
							{
								x: 540,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 550,
								y: 0,
								xtype: 'datefield',
								name: 'dtfTglKunjung_PenJasHemodialisa',
								id: 'dtfTglKunjung_PenJasHemodialisa',
								tabIndex:6,
								width: 110,
								format: 'd/M/Y',
								value: now_viPenJasHemodialisa,
							},
							{
								x: 420,
								y: 30,
								xtype: 'label',
								text: 'Nama Pasien'
							},
							{
								x: 540,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							PenJasHemodialisa.vars.nama=new Nci.form.Combobox.autoCompleteId({
								x: 550,
								y: 30,
								tabIndex:7,
								width: 200,
								id		: 'txtAutoCompNamaPasien_PenJasHemodialisa',
								store	: PenJasHemodialisa.form.ArrayStore.nama,
								select	: function(a,b,c){
									PenJasHemodialisa.vars.kdpasien.setValue(b.data.kd_pasien);
									dataGriPenJasHemodialisa(b.data.no_transaksi,b.data.kd_kasir,b);
									
									// # ---DISABLE---#
									PenJasHemodialisa.vars.notransaksi.disable();
									PenJasHemodialisa.vars.kdpasien.disable();
									Ext.getCmp('txtUnit_PenJasHemodialisa').disable();
									Ext.getCmp('dftTglLahir_PenJasHemodialisa').disable();
									Ext.getCmp('txtAlamat_PenJasHemodialisa').disable();
									Ext.getCmp('cboJK_PenJasHemodialisa').disable();
									Ext.getCmp('cboGolDarah_PenJasHemodialisa').disable();
									Ext.getCmp('cboKelPasien_PenJasHemodialisa').disable();
									Ext.getCmp('cboPerseorangan_PenJasHemodialisa').disable();
									Ext.getCmp('cboPerusahaan_PenJasHemodialisa').disable();
									Ext.getCmp('cboAsuransi_PenJasHemodialisa').disable();
									Ext.getCmp('txtDokterPengirim_PenJasHemodialisa').disable();
									
								},
								/* onShowList:function(a){
									dataSource_viPenJasHemodialisa.removeAll();
									
									var recs=[],
									recType=dataSource_viPenJasHemodialisa.recordType;
										
									for(var i=0; i<a.length; i++){
										recs.push(new recType(a[i]));
									}
									dataSource_viPenJasHemodialisa.add(recs);
									
								}, */
								insert	: function(o){
									return {
										no_transaksi		:o.no_transaksi,
										tgl_transaksi		:o.tgl_transaksi,
										kd_spesial			:o.kd_spesial,
										kamar				:o.kamar,
										tgl_lahir			:o.tgl_lahir,
										gol_darah			:o.gol_darah,
										jenis_kelamin		:o.jenis_kelamin,
										dokter				:o.dokter,
										urut_masuk			:o.urut_masuk,
										nama_unit			:o.nama_unit,
										alamat				:o.alamat,
										kd_pasien        	:o.kd_pasien,
										nama        		:o.nama,
										kd_dokter			:o.kd_dokter,
										kd_unit				:o.kd_unit,
										kd_customer			:o.kd_customer,
										kd_kasir			:o.kd_kasir,
										nama_kamar			:o.nama_kamar,
										no_kamar			:o.no_kamar,
										customer			:o.customer,
										nama_unit_asli		:o.nama_unit_asli,
										tgl					:o.tgl,
										kd_tarif			:o.kd_tarif,
										kelpasien			:o.kelpasien,
										text				:  '<table style="font-size: 11px;"><tr><td width="100">'+o.kd_pasien+'</td><td width="70" align="left">'+o.no_transaksi+'</td><td width="130">'+o.nama+'</td><td width="110" align="left">'+o.nama_unit_asli+'</td><td width="90">'+o.tgl+'</td></tr></table>',
										kd_pasien	 		:o.kd_pasien
									}
								},
								param:function(){
									var a=0;
									return {
										tanggal:Ext.getCmp('dtfTglKunjung_PenJasHemodialisa').getValue(),
										a:2
									}
								},
								url		: baseURL + "index.php/hemodialisa/functionPenJasHemodialisa/getPasien",
								valueField: 'nama',
								displayField: 'text',
								listWidth: 350
							}),
							{
								x: 420,
								y: 60,
								xtype: 'label',
								text: 'Tanggal Lahir'
							},
							{
								x: 540,
								y: 60,
								xtype: 'label',
								text: ':'
							},
							{
								x: 550,
								y: 60,
								xtype: 'datefield',
								name: 'dftTglLahir_PenJasHemodialisa',
								id: 'dftTglLahir_PenJasHemodialisa',
								tabIndex:8,
								format: 'd/M/Y',
								value: now_viPenJasHemodialisa,
								width: 100
							},
							{
								x: 420,
								y: 90,
								xtype: 'label',
								text: 'Alamat'
							},
							{
								x: 540,
								y: 90,
								xtype: 'label',
								text: ':'
							},
							{
								x: 550,
								y: 90,
								xtype: 'textfield',
								name: 'txtAlamat_PenJasHemodialisa',
								id: 'txtAlamat_PenJasHemodialisa',
								tabIndex:11,
								width: 300
							},
							{
								x: 420,
								y: 120,
								xtype: 'label',
								text: 'Jenis Kelamin'
							},
							{
								x: 540,
								y: 120,
								xtype: 'label',
								text: ':'
							},
							comboJK_PenJasHemodialisa(),
							{
								x: 680,
								y: 120,
								xtype: 'label',
								text: 'Gol. Darah'
							},
							{
								x: 760,
								y: 120,
								xtype: 'label',
								text: ':'
							},
							comboGDarah_PenJasHemodialisa(),
							
							// ---------------# HIDDEN #-------------------- //
							{
								x: 760,
								y: 150,
								xtype: 'textfield',
								fieldLabel: 'Kd unit',
								name: 'txtKdUnit_PenJasHemodialisa',
								id: 'txtKdUnit_PenJasHemodialisa',
								width: 150,
								hidden: true,
							},
							{
								x: 760,
								y: 150,
								xtype: 'textfield',
								fieldLabel: 'Kd dokter',
								name: 'txtKdDokter_PenJasHemodialisa',
								id: 'txtKdDokter_PenJasHemodialisa',
								width: 150,
								hidden: true,
							},
							{
								x: 760,
								y: 150,
								xtype: 'textfield',
								fieldLabel: 'Kd dokter pengirim',
								name: 'txtKdDokterPengirim_PenJasHemodialisa',
								id: 'txtKdDokterPengirim_PenJasHemodialisa',
								width: 150,
								hidden: true,
							},
							{
								x: 760,
								y: 150,
								xtype: 'textfield',
								fieldLabel: 'Urut masuk',
								name: 'txtUrutMasuk_PenJasHemodialisa',
								id: 'txtUrutMasuk_PenJasHemodialisa',
								width: 150,
								hidden: true,
							},
							{
								x: 760,
								y: 150,
								xtype: 'textfield',
								fieldLabel: 'kd customer',
								name: 'txtKdCustomer_PenJasHemodialisa',
								id: 'txtKdCustomer_PenJasHemodialisa',
								width: 150,
								hidden: true,
							},
							{
								x: 760,
								y: 150,
								xtype: 'textfield',
								fieldLabel: 'kd kasir',
								name: 'txtKdKasir_PenJasHemodialisa',
								id: 'txtKdKasir_PenJasHemodialisa',
								width: 150,
								hidden: true,
							},
							{
								x: 760,
								y: 150,
								xtype: 'textfield',
								fieldLabel: 'kdspeisal',
								name: 'txtKdSpesial_PenJasHemodialisa',
								id: 'txtKdSpesial_PenJasHemodialisa',
								width: 150,
								hidden: true,
							},
							{
								x: 760,
								y: 150,
								xtype: 'datefield',
								name: 'dftTgltransaksiasal_PenJasHemodialisa',
								id: 'dftTgltransaksiasal_PenJasHemodialisa',
								tabIndex:8,
								format: 'd/M/Y',
								value: now_viPenJasHemodialisa,
								width: 150,
								hidden: true,
							},
							{
								x: 760,
								y: 150,
								xtype: 'textfield',
								fieldLabel: 'notransaksiasal',
								name: 'txtNoTransaksiAsal_PenJasHemodialisa',
								id: 'txtNoTransaksiAsal_PenJasHemodialisa',
								width: 150,
								hidden: true,
							},
							{
								x: 760,
								y: 150,
								xtype: 'textfield',
								fieldLabel: 'nokamar',
								name: 'txtNoKamar_PenJasHemodialisa',
								id: 'txtNoKamar_PenJasHemodialisa',
								width: 150,
								hidden: true,
							},
							
							
						]
					}
				]
			}
		]		
	};
        return items;
}
//------------------end---------------------------------------------------------


function loadDataKodePasien_PenJasHemodialisa(param){
	if (param==='' || param===undefined) {
		param={
			text: '0',
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/hemodialisa/functionPenJasHemodialisa/getPasien",
		params: {
			text:param,
			tanggal:'',
			a:1
		},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cboAutoCompKdPasien_PenJasHemodialisa.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsKodePasien_PenJasHemodialisa.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsKodePasien_PenJasHemodialisa.add(recs);
				console.log(o);
			}
		}
	});
};

function comboAutoCompKdPasien_PenJasHemodialisa(){
	var Field = ['no_transaksi','kd_kasir','tgl_transaksi','kd_spesial','kamar','tgl_lahir','kd_pasien','nama','nama_unit_asli',
				'alamat','kd_dokter','kd_unit','nama_unit','urut_masuk','dokter','Jenis_kelamin','gol_darah','kd_customer','customer',
				'no_kamar','kd_spesial','tgl','kd_tarif','kelpasien'];
	dsKodePasien_PenJasHemodialisa = new WebApp.DataStore({fields: Field});
	loadDataKodePasien_PenJasHemodialisa();
	cboAutoCompKdPasien_PenJasHemodialisa = new Ext.form.ComboBox
	(
		{
			id: 'cboAutoCompKdPasien_PenJasHemodialisa',
			typeAhead: true,
			x: 130,
			y: 30,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			hideTrigger		: true,
			store: dsKodePasien_PenJasHemodialisa,
			valueField: 'kd_pasien',
			displayField: 'kode',
			emptyText: '',
			width:150,
			listeners:
			{
				'select': function(a, b, c)
				{
					PenJasHemodialisa.vars.nama.setValue(b.data.nama);
					dataGriPenJasHemodialisa(b.data.no_transaksi,b.data.kd_kasir,b);
					Ext.getCmp('cboAutoCompKdPasien_PenJasHemodialisa').setValue('');
					Ext.getCmp('cboAutoCompKdPasien_PenJasHemodialisa').setValue(b.data.kd_pasien);
					// # ---DISABLE---#
					PenJasHemodialisa.vars.notransaksi.disable();
					PenJasHemodialisa.vars.nama.disable();
					Ext.getCmp('txtUnit_PenJasHemodialisa').disable();
					Ext.getCmp('dftTglLahir_PenJasHemodialisa').disable();
					Ext.getCmp('txtAlamat_PenJasHemodialisa').disable();
					Ext.getCmp('cboJK_PenJasHemodialisa').disable();
					Ext.getCmp('cboGolDarah_PenJasHemodialisa').disable();
					Ext.getCmp('cboKelPasien_PenJasHemodialisa').disable();
					Ext.getCmp('cboPerseorangan_PenJasHemodialisa').disable();
					Ext.getCmp('cboPerusahaan_PenJasHemodialisa').disable();
					Ext.getCmp('cboAsuransi_PenJasHemodialisa').disable();
					Ext.getCmp('txtDokterPengirim_PenJasHemodialisa').disable();
				},
				keyUp: function(a,b,c){
					
					if(  b.getKey()!=127 ){
						clearTimeout(this.time);
				
						this.time=setTimeout(function(){
							if(cboAutoCompKdPasien_PenJasHemodialisa.lastQuery != '' ){
								var value="";
								
								if (value!=cboAutoCompKdPasien_PenJasHemodialisa.lastQuery)
								{
									if (a.rendered && a.innerList != null) {
										a.innerList.update(a.loadingText ? '<div class="loading-indicator">' + a.loadingText + '</div>' : '');
										a.restrictHeight();
										a.selectedIndex = 0;
									}
									a.expand();
									Ext.Ajax.request({
										url: baseURL + "index.php/hemodialisa/functionPenJasHemodialisa/getPasien",
										params: {
											text:cboAutoCompKdPasien_PenJasHemodialisa.lastQuery,
											tanggal:Ext.getCmp('dtfTglKunjung_PenJasHemodialisa').getValue(),
											a:1
										},
										failure: function(o){
											var cst = Ext.decode(o.responseText);
										},	    
										success: function(o) {
											cboAutoCompKdPasien_PenJasHemodialisa.store.removeAll();
											var cst = Ext.decode(o.responseText);

											for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
												var recs    = [],recType = dsKodePasien_PenJasHemodialisa.recordType;
												var o=cst['listData'][i];
										
												recs.push(new recType(o));
												dsKodePasien_PenJasHemodialisa.add(recs);
											}
											a.expand();
											if(dsKodePasien_PenJasHemodialisa.onShowList != undefined)
												dsKodePasien_PenJasHemodialisa.onShowList(cst[showVar]);
											if(cst['listData'].length>0){
													
												a.doQuery(a.allQuery, true);
												a.expand();
												a.selectText(value.length,value.length);
											}else{
											//	if (a.rendered && a.innerList != null) {
													a.innerList.update(a.loadingText ? '&nbsp; Data Tidak Ada' : '');
													a.restrictHeight();
													a.selectedIndex = 0;
												//}
											}
										}
									});
									value=cboAutoCompKdPasien_PenJasHemodialisa.lastQuery;
								}
							}
						},1000);
					}
				} 
			}
		}
	)
	return cboAutoCompKdPasien_PenJasHemodialisa;
};

function mComboDOKTER_PenJasHemodialisa()
{
    var Field = ['KD_DOKTER','NAMA'];

    dsdokter_viPenJasHemodialisa = new WebApp.DataStore({ fields: Field });
    dsdokter_viPenJasHemodialisa.load
	(
            {
                params:
                    {
                        Skip: 0,
                        Take: 1000,
                        Sort: 'nama',
                        Sortdir: 'ASC',
                        target: 'ViewDokterPenunjang',
                        param: "kd_unit = '41'"
                    }
            }
	);

    var cboDOKTER_viPenJasHemodialisa = new Ext.form.ComboBox
	(
		{
			id: 'cboDOKTER_viPenJasHemodialisa',
			x: 130,
			y: 150,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			emptyText: '',
			fieldLabel:  ' ',
			align: 'Right',
			width: 230,
			store: dsdokter_viPenJasHemodialisa,
			valueField: 'KD_DOKTER',
			displayField: 'NAMA',
			editable: true,
			listeners:
			{
				'select': function(a, b, c)
				{
					Ext.getCmp('btnTambahItemPemeriksaan_viPenJasHemodialisa').enable();
					Ext.getCmp('txtKdDokter_PenJasHemodialisa').setValue(b.data.KD_DOKTER);
				}
			}
		}
	);

    return cboDOKTER_viPenJasHemodialisa;
};

function comboKelompokPasien_PenJasHemodialisa(){
	var cboKelPasien_PenJasHemodialisa = new Ext.form.ComboBox
	({
		id:'cboKelPasien_PenJasHemodialisa',
		x: 130,
		y: 180,
		mode: 'local',
		width: 130,
		forceSelection: true,
		lazyRender:true,
		triggerAction: 'all',
		editable: false,
		store: new Ext.data.ArrayStore
		(
			{
			id: 0,
			fields:
			[
				'Id',
				'displayText'
			],
			   data: [[1, 'Perseorangan'], [2, 'Perusahaan'], [3, 'Asuransi']]
			}
		),
		valueField: 'Id',
		displayField: 'displayText',
		//value:selectSetKelPasienLab,
		selectOnFocus: true,
		tabIndex:22,
		listeners:
		{
			'select': function(a, b, c)
				{
				   //Combo_Select(b.data.displayText);
				   if(b.data.displayText == 'Perseorangan')
				   {
						Ext.getCmp('txtNamaPesertaAsuransi_PenJasHemodialisa').hide()
						Ext.getCmp('txtNoAskes_PenJasHemodialisa').hide()
						Ext.getCmp('txtNoSJP_PenJasHemodialisa').hide()
						Ext.getCmp('cboPerseorangan_PenJasHemodialisa').show()
						Ext.getCmp('cboAsuransi_PenJasHemodialisa').hide()
						Ext.getCmp('cboPerusahaan_PenJasHemodialisa').hide()
				   }
				   else if(b.data.displayText == 'Perusahaan')
				   {
						Ext.getCmp('txtNamaPesertaAsuransi_PenJasHemodialisa').hide()
						Ext.getCmp('txtNoAskes_PenJasHemodialisa').hide()
						Ext.getCmp('txtNoSJP_PenJasHemodialisa').hide()
						Ext.getCmp('cboPerseorangan_PenJasHemodialisa').hide()
						Ext.getCmp('cboAsuransi_PenJasHemodialisa').hide()
						Ext.getCmp('cboPerusahaan_PenJasHemodialisa').show()
				   }
				   else if(b.data.displayText == 'Asuransi')
				   {
						Ext.getCmp('txtNamaPesertaAsuransi_PenJasHemodialisa').show()
						Ext.getCmp('txtNoAskes_PenJasHemodialisa').show()
						Ext.getCmp('txtNoSJP_PenJasHemodialisa').show()
						Ext.getCmp('cboPerseorangan_PenJasHemodialisa').hide()
						Ext.getCmp('cboAsuransi_PenJasHemodialisa').show()
						Ext.getCmp('cboPerusahaan_PenJasHemodialisa').hide()
				   }
				},
			'render': function(a,b,c)
			{
				Ext.getCmp('txtNamaPesertaAsuransi_PenJasHemodialisa').hide();
				Ext.getCmp('txtNoAskes_PenJasHemodialisa').hide();
				Ext.getCmp('txtNoSJP_PenJasHemodialisa').hide();
				Ext.getCmp('cboPerseorangan_PenJasHemodialisa').hide();
				Ext.getCmp('cboAsuransi_PenJasHemodialisa').hide();
				Ext.getCmp('cboPerusahaan_PenJasHemodialisa').hide();

			}
		}

	});
	return cboKelPasien_PenJasHemodialisa;
};

function comboPerseorangan_PenJasHemodialisa()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerorangan_PenJasHemodialisa = new WebApp.DataStore({fields: Field});
    dsPerorangan_PenJasHemodialisa.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
				Sort: 'customer',
			    Sortdir: 'ASC',
			    target: 'ViewComboKontrakCustomer',
			    param: 'jenis_cust=0'
			}
		}
	)
    var cboPerseorangan_PenJasHemodialisa = new Ext.form.ComboBox
	(
		{
			id:'cboPerseorangan_PenJasHemodialisa',
			x: 280,
			y: 180,
			editable: false,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			hidden: true,
			fieldLabel: 'Jenis',
			tabIndex:23,
			width:150,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			store:dsPerorangan_PenJasHemodialisa,
			value:selectSetPerseorangan_PenJasHemodialisa,
			listeners:
			{
				'select': function(a,b,c)
				{
				    selectSetPerseorangan_PenJasHemodialisa=b.data.displayText ;
				}
			}
		}
	);
	return cboPerseorangan_PenJasHemodialisa;
};

function comboPerusahaan_PenJasHemodialisa()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaan_PenJasHemodialisa = new WebApp.DataStore({fields: Field});
    dsPerusahaan_PenJasHemodialisa.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
				Sort: 'customer',
			    Sortdir: 'ASC',
			    target: 'ViewComboKontrakCustomer',
			    param: 'jenis_cust=1'
			}
		}
	)
    var cboPerusahaan_PenJasHemodialisa = new Ext.form.ComboBox
	(
		{
		    id: 'cboPerusahaan_PenJasHemodialisa',
			x: 280,
			y: 180,
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
			hidden: true,
		    fieldLabel: 'Perusahaan',
		    align: 'Right',
			width:150,
		    store: dsPerusahaan_PenJasHemodialisa,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
		    listeners:
			{
			    'select': function(a,b,c)
				{
					selectSetPerusahaan_PenJasHemodialisa=b.data.valueField ;
				}
			}
		}
	);

    return cboPerusahaan_PenJasHemodialisa;
};

function comboAsuransi_PenJasHemodialisa()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    dsAsuransi_PenJasHemodialisa = new WebApp.DataStore({fields: Field_poli_viDaftar});

	dsAsuransi_PenJasHemodialisa.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'customer',
                Sortdir: 'ASC',
                target:'ViewComboKontrakCustomer',
                param: 'jenis_cust=2'
            }
        }
    )
    var cboAsuransi_PenJasHemodialisa = new Ext.form.ComboBox
	(
		{
			id:'cboAsuransi_PenJasHemodialisa',
			x: 280,
			y: 180,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			hidden: true,
			fieldLabel: 'Asuransi',
			align: 'Right',
			width:150,
			store: dsAsuransi_PenJasHemodialisa,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetAsuransi_PenJasHemodialisa=b.data.valueField ;
				}
			}
		}
	);
	return cboAsuransi_PenJasHemodialisa;
};


function comboJK_PenJasHemodialisa()
{
    var cboJK_PenJasHemodialisa = new Ext.form.ComboBox
	(
		{
			id:'cboJK_PenJasHemodialisa',
			x: 550,
            y: 120,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			tabIndex:3,
			selectOnFocus:true,
			forceSelection: true,
			editable: false,
			width:105,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [['true', 'Laki - Laki'], ['false', 'Perempuan']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetJK_PenJasHemodialisa=b.data.valueField ;
				},
			}
		}
	);
	return cboJK_PenJasHemodialisa;
};

function comboGDarah_PenJasHemodialisa()
{
    var cboGolDarah_PenJasHemodialisa = new Ext.form.ComboBox
	(
		{
			id:'cboGolDarah_PenJasHemodialisa',
			x: 780,
			y: 120,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			tabIndex:3,
			selectOnFocus:true,
			forceSelection: true,
			fieldLabel: 'Gol. Darah ',
			width:85,
			editable: false,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[0, '-'], [1, 'A+'],[2, 'B+'], [3, 'AB+'],[4, 'O+'], [5, 'A-'], [6, 'B-']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			//value:selectSetGDarah_PenJasHemodialisa,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetGDarah_PenJasHemodialisa=b.data.displayText ;
				},

			}
		}
	);
	return cboGolDarah_PenJasHemodialisa;
};

function comboUnitHD_PenJasHemodialisa()
{
    var cboUnitHD_PenJasHemodialisa = new Ext.form.ComboBox
	(
		{
			id:'cboUnitHD_PenJasHemodialisa',
			x: 130,
			y: 0,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			tabIndex:3,
			selectOnFocus:true,
			forceSelection: true,
			fieldLabel: 'Unit HD ',
			width:85,
			editable: false,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[0, 'HD Pav'], [1, 'HD IRD']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:'Pilih...',
			listeners:
			{
				'select': function(a,b,c)
				{
					kd_unit_hd =b.data.Id;
				},

			}
		}
	);
	return cboUnitHD_PenJasHemodialisa;
};
function getDokterPengirim_PenJasHemodialisa(no_transaksi,kd_kasir){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/hemodialisa/functionPenJasHemodialisa/getDokterPengirim",
			params:{no_transaksi:no_transaksi,kd_kasir:kd_kasir} ,
			failure: function(o)
			{
				ShowPesanErrorPenJasHemodialisa('Hubungi Admin', 'Error');
			},
			success: function(o)
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					Ext.getCmp('txtDokterPengirim_PenJasHemodialisa').setValue(cst.nama);
					Ext.getCmp('txtKdDokterPengirim_PenJasHemodialisa').setValue(cst.kd_dokter);
				}
				else
				{
					ShowPesanErrorPenJasHemodialisa('Gagal membaca dokter pengirim', 'Error');
				};
			}
		}

	)
}

function shiftbagian_PenJasHemodialisa(){
	Ext.Ajax.request(
	{
		url: baseURL + "index.php/hemodialisa/functionPenJasHemodialisa/getCurrentShiftHd",
		params: {
			command: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
		},
		failure: function(o)
		{
			var cst = Ext.decode(o.responseText);
		},
		success: function(o) {
			shifHemodialisa=o.responseText;
		}
	});
}


function dataGriPenJasHemodialisa(no_transaksi,kd_kasir,b){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/hemodialisa/functionPenJasHemodialisa/getItemPemeriksaan",
			params: {
				no_transaksi:no_transaksi,
				kd_kasir:kd_kasir
			},
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorPenJasHemodialisa('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dataSource_viPenJasHemodialisa.removeAll();
					var recs=[],
						recType=dataSource_viPenJasHemodialisa.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						recs.push(new recType(cst.ListDataObj[i]));
					}
					dataSource_viPenJasHemodialisa.add(recs);
					GridDataView_viPenJasHemodialisa.getView().refresh();
					
					if(dataSource_viPenJasHemodialisa.getCount() > 0){
						if(PenJasHemodialisa.vars.notransaksi.getValue() == ''){
							PenJasHemodialisa.vars.notransaksi.setValue(b.data.no_transaksi);
						}
						Ext.getCmp('txtUnit_PenJasHemodialisa').setValue(b.data.nama_unit);
						Ext.getCmp('cboDOKTER_viPenJasHemodialisa').setValue(b.data.dokter);
						Ext.getCmp('txtKdDokter_PenJasHemodialisa').setValue(b.data.kd_dokter);
						getDokterPengirim_PenJasHemodialisa(b.data.no_transaksi,b.data.kd_kasir);
						Ext.getCmp('txtDokterPengirim_PenJasHemodialisa').setValue(b.data.dokter);
						Ext.getCmp('cboDOKTER_viPenJasHemodialisa').disable();
						Ext.getCmp('btnSimpan_viPenJasHemodialisa').disable();
						Ext.getCmp('btnTambahItemPemeriksaan_viPenJasHemodialisa').disable();
						Ext.getCmp('btnHpsBrsItem_viPenJasHemodialisa').disable();
						Ext.getCmp('cboUnitHD_PenJasHemodialisa').disable();
						if(b.data.kd_unit == 305){
							Ext.getCmp('cboUnitHD_PenJasHemodialisa').setValue('HD IRD');
							kd_unit_hd=1;
						}else{
							Ext.getCmp('cboUnitHD_PenJasHemodialisa').setValue('HD PAV');
							kd_unit_hd=0;
						}
					} else{
						Ext.getCmp('txtUnit_PenJasHemodialisa').setValue(b.data.nama_unit_asli);
						Ext.getCmp('txtDokterPengirim_PenJasHemodialisa').setValue(b.data.dokter);
						Ext.getCmp('txtKdDokterPengirim_PenJasHemodialisa').setValue(b.data.kd_dokter);
						Ext.getCmp('btnSimpan_viPenJasHemodialisa').enable();
						Ext.getCmp('btnTambahItemPemeriksaan_viPenJasHemodialisa').enable();
					}
					
					Ext.getCmp('dftTgltransaksiasal_PenJasHemodialisa').setValue(ShowDate(b.data.tgl_transaksi));
					Ext.getCmp('txtNoTransaksiAsal_PenJasHemodialisa').setValue(b.data.no_transaksi);
					Ext.getCmp('dftTglLahir_PenJasHemodialisa').setValue(ShowDate(b.data.tgl_lahir));
					Ext.getCmp('txtAlamat_PenJasHemodialisa').setValue(b.data.alamat);
					
					if (b.data.jenis_kelamin === 't')
					{
						Ext.getCmp('cboJK_PenJasHemodialisa').setValue('Laki-laki');
					}else
					{
						Ext.getCmp('cboJK_PenJasHemodialisa').setValue('Perempuan');
					}
					
					if (b.data.gol_darah === '0')
					{
						Ext.getCmp('cboGolDarah_PenJasHemodialisa').setValue('-');
					}else if (b.data.gol_darah === '1')
					{
						Ext.getCmp('cboGolDarah_PenJasHemodialisa').setValue('A+');
					}else if (b.data.gol_darah === '2')
					{
						Ext.getCmp('cboGolDarah_PenJasHemodialisa').setValue('B+');
					}else if (b.data.gol_darah === '3')
					{
						Ext.getCmp('cboGolDarah_PenJasHemodialisa').setValue('AB+');
					}else if (b.data.gol_darah === '4')
					{
						Ext.getCmp('cboGolDarah_PenJasHemodialisa').setValue('O+');
					}else if (b.data.gol_darah === '5')
					{
						Ext.getCmp('cboGolDarah_PenJasHemodialisa').setValue('A-');
					}else if (b.data.gol_darah === '6')
					{
						Ext.getCmp('cboGolDarah_PenJasHemodialisa').setValue('B-');
					}
					
					if(b.data.kelpasien == 'Perseorangan'){
						Ext.getCmp('cboKelPasien_PenJasHemodialisa').setValue(b.data.kelpasien);
						Ext.getCmp('cboPerseorangan_PenJasHemodialisa').setValue(b.data.kd_customer);
						Ext.getCmp('cboPerseorangan_PenJasHemodialisa').show();
					} else if(b.data.kelpasien == 'Perusahaan'){
						Ext.getCmp('cboKelPasien_PenJasHemodialisa').setValue(b.data.kelpasien);
						Ext.getCmp('cboPerusahaan_PenJasHemodialisa').setValue(b.data.kd_customer);
						Ext.getCmp('cboPerusahaan_PenJasHemodialisa').show();
					} else{
						Ext.getCmp('cboKelPasien_PenJasHemodialisa').setValue(b.data.kelpasien);
						Ext.getCmp('cboAsuransi_PenJasHemodialisa').setValue(b.data.kd_customer);
						Ext.getCmp('cboAsuransi_PenJasHemodialisa').show();
					}
					
					// # ----HIDDEN----#
					Ext.getCmp('txtKdUnit_PenJasHemodialisa').setValue(b.data.kd_unit);
					Ext.getCmp('txtKdCustomer_PenJasHemodialisa').setValue(b.data.kd_customer);
					Ext.getCmp('txtUrutMasuk_PenJasHemodialisa').setValue(b.data.urut_masuk);
					Ext.getCmp('txtKdKasir_PenJasHemodialisa').setValue(b.data.kd_kasir);
					Ext.getCmp('txtKdSpesial_PenJasHemodialisa').setValue(b.data.kd_spesial);
					Ext.getCmp('txtNoKamar_PenJasHemodialisa').setValue(b.data.no_kamar);
				}
				else 
				{
					loadMask.hide();
					ShowPesanErrorPenJasHemodialisa('Gagal membaca data pemeriksaan hemodialisa', 'Error');
				};
			}
		}
		
	)
	
}

function dataSave_viPenJasHemodialisa(){
	if (ValidasiSavePenJasHemodialisa(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/hemodialisa/functionPenJasHemodialisa/save",
				params: getParamSavePenJasHemodialisa(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorPenJasHemodialisa('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoPenJasHemodialisa('Berhasil menyimpan data ini','Information');
						Ext.getCmp('cboDOKTER_viPenJasHemodialisa').disable();
						Ext.getCmp('btnSimpan_viPenJasHemodialisa').disable();
						Ext.getCmp('cboUnitHD_PenJasHemodialisa').disable();
						PenJasHemodialisa.vars.notransaksi.setValue(cst.notrans);
						if(PenJasHemodialisa.vars.kdpasien.getValue() == ''){
							PenJasHemodialisa.vars.kdpasien.setValue(cst.kdPasien);
							Ext.getCmp('txtUnit_PenJasHemodialisa').setValue(cst.namaunit);
							Ext.getCmp('txtDokterPengirim_PenJasHemodialisa').setValue('-');
							
							
							PenJasHemodialisa.vars.notransaksi.disable();
							PenJasHemodialisa.vars.kdpasien.disable();
							Ext.getCmp('txtUnit_PenJasHemodialisa').disable();
							Ext.getCmp('txtDokterPengirim_PenJasHemodialisa').disable();
							Ext.getCmp('cboDOKTER_viPenJasHemodialisa').disable();
							Ext.getCmp('cboKelPasien_PenJasHemodialisa').disable();
							Ext.getCmp('cboPerseorangan_PenJasHemodialisa').disable();
							Ext.getCmp('cboPerusahaan_PenJasHemodialisa').disable();
							Ext.getCmp('cboAsuransi_PenJasHemodialisa').disable();
							Ext.getCmp('txtNamaPesertaAsuransi_PenJasHemodialisa').disable();
							Ext.getCmp('txtNoAskes_PenJasHemodialisa').disable();
							Ext.getCmp('txtNoSJP_PenJasHemodialisa').disable();
							Ext.getCmp('dtfTglKunjung_PenJasHemodialisa').disable();
							PenJasHemodialisa.vars.nama.disable();
							Ext.getCmp('dftTglLahir_PenJasHemodialisa').disable();
							Ext.getCmp('txtAlamat_PenJasHemodialisa').disable();
							Ext.getCmp('cboJK_PenJasHemodialisa').disable();
							Ext.getCmp('cboGolDarah_PenJasHemodialisa').disable();
							Ext.getCmp('btnTambahItemPemeriksaan_viPenJasHemodialisa').disable();
							Ext.getCmp('btnHpsBrsItem_viPenJasHemodialisa').disable();
							Ext.getCmp('btnSimpan_viPenJasHemodialisa').disable();
							
						} 
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorPenJasHemodialisa('Gagal menyimpan data ini', 'Error');
					};
				}
			}
			
		)
	}
}

function AddNewPenJasHemodialisa(){
	Ext.getCmp('cboUnitHD_PenJasHemodialisa').setValue('Pilih...');
	Ext.getCmp('cboUnitHD_PenJasHemodialisa').enable();
	PenJasHemodialisa.vars.notransaksi.setValue('');
	PenJasHemodialisa.vars.kdpasien.setValue('');
	Ext.getCmp('txtUnit_PenJasHemodialisa').setValue('');
	Ext.getCmp('txtDokterPengirim_PenJasHemodialisa').setValue('');
	Ext.getCmp('cboDOKTER_viPenJasHemodialisa').setValue('');
	Ext.getCmp('cboKelPasien_PenJasHemodialisa').setValue('');
	Ext.getCmp('cboPerseorangan_PenJasHemodialisa').setValue('');
	Ext.getCmp('cboPerusahaan_PenJasHemodialisa').setValue('');
	Ext.getCmp('cboAsuransi_PenJasHemodialisa').setValue('');
	Ext.getCmp('txtNamaPesertaAsuransi_PenJasHemodialisa').setValue('');
	Ext.getCmp('txtNoAskes_PenJasHemodialisa').setValue('');
	Ext.getCmp('txtNoSJP_PenJasHemodialisa').setValue('');
	Ext.getCmp('dtfTglKunjung_PenJasHemodialisa').setValue(now_viPenJasHemodialisa);
	PenJasHemodialisa.vars.nama.setValue('');
	Ext.getCmp('dftTglLahir_PenJasHemodialisa').setValue(now_viPenJasHemodialisa);
	Ext.getCmp('txtAlamat_PenJasHemodialisa').setValue('');
	Ext.getCmp('cboJK_PenJasHemodialisa').setValue('');
	Ext.getCmp('cboGolDarah_PenJasHemodialisa').setValue('');
	Ext.getCmp('txtKdUnit_PenJasHemodialisa').setValue('');
	Ext.getCmp('txtKdDokter_PenJasHemodialisa').setValue('');
	Ext.getCmp('txtKdDokterPengirim_PenJasHemodialisa').setValue('');
	Ext.getCmp('txtUrutMasuk_PenJasHemodialisa').setValue('');
	Ext.getCmp('txtKdCustomer_PenJasHemodialisa').setValue('');
	Ext.getCmp('txtKdKasir_PenJasHemodialisa').setValue('');
	Ext.getCmp('txtKdSpesial_PenJasHemodialisa').setValue('');
	Ext.getCmp('dftTgltransaksiasal_PenJasHemodialisa').setValue(now_viPenJasHemodialisa);
	Ext.getCmp('txtNoTransaksiAsal_PenJasHemodialisa').setValue('');
	Ext.getCmp('txtNoKamar_PenJasHemodialisa').setValue('');
	dataSource_viPenJasHemodialisa.removeAll();
	
	PenJasHemodialisa.vars.notransaksi.enable();
	PenJasHemodialisa.vars.kdpasien.enable();
	Ext.getCmp('txtUnit_PenJasHemodialisa').enable();
	Ext.getCmp('txtDokterPengirim_PenJasHemodialisa').enable();
	Ext.getCmp('cboDOKTER_viPenJasHemodialisa').enable();
	Ext.getCmp('cboKelPasien_PenJasHemodialisa').enable();
	Ext.getCmp('cboPerseorangan_PenJasHemodialisa').enable();
	Ext.getCmp('cboPerusahaan_PenJasHemodialisa').enable();
	Ext.getCmp('cboAsuransi_PenJasHemodialisa').enable();
	Ext.getCmp('txtNamaPesertaAsuransi_PenJasHemodialisa').enable();
	Ext.getCmp('txtNoAskes_PenJasHemodialisa').enable();
	Ext.getCmp('txtNoSJP_PenJasHemodialisa').enable();
	Ext.getCmp('dtfTglKunjung_PenJasHemodialisa').enable();
	PenJasHemodialisa.vars.nama.enable();
	Ext.getCmp('dftTglLahir_PenJasHemodialisa').enable();
	Ext.getCmp('txtAlamat_PenJasHemodialisa').enable();
	Ext.getCmp('cboJK_PenJasHemodialisa').enable();
	Ext.getCmp('cboGolDarah_PenJasHemodialisa').enable();
	Ext.getCmp('txtKdUnit_PenJasHemodialisa').enable();
	Ext.getCmp('txtKdDokter_PenJasHemodialisa').enable();
	Ext.getCmp('txtKdDokterPengirim_PenJasHemodialisa').enable();
	Ext.getCmp('txtUrutMasuk_PenJasHemodialisa').enable();
	Ext.getCmp('txtKdCustomer_PenJasHemodialisa').enable();
	Ext.getCmp('txtKdKasir_PenJasHemodialisa').enable();
	Ext.getCmp('txtKdSpesial_PenJasHemodialisa').enable();
	Ext.getCmp('dftTgltransaksiasal_PenJasHemodialisa').enable();
	Ext.getCmp('txtNoTransaksiAsal_PenJasHemodialisa').enable();
	Ext.getCmp('txtNoKamar_PenJasHemodialisa').enable();
};


function getParamSavePenJasHemodialisa(){
	var KdCust='';
	var pasienBaru;

	if(Ext.get('cboKelPasien_PenJasHemodialisa').getValue()=='Perseorangan'){
		KdCust=Ext.getCmp('cboPerseorangan_PenJasHemodialisa').getValue();
	}else if(Ext.get('cboKelPasien_PenJasHemodialisa').getValue()=='Perusahaan'){
		KdCust=Ext.getCmp('cboPerusahaan_PenJasHemodialisa').getValue();
	}else {
		KdCust=Ext.getCmp('cboAsuransi_PenJasHemodialisa').getValue();
	}

	if(PenJasHemodialisa.vars.kdpasien.getValue() == ''){
		pasienBaru=1;
	} else{
		pasienBaru=0;
	}

    var params =
	{
		KdTransaksi: PenJasHemodialisa.vars.notransaksi.getValue(),
		KdPasien:PenJasHemodialisa.vars.kdpasien.getValue(),
		NmPasien:PenJasHemodialisa.vars.nama.getValue(),
		Ttl:Ext.getCmp('dftTglLahir_PenJasHemodialisa').getValue(),
		Alamat:Ext.getCmp('txtAlamat_PenJasHemodialisa').getValue(),
		JK:Ext.getCmp('cboJK_PenJasHemodialisa').getValue(),
		GolDarah:Ext.getCmp('cboGolDarah_PenJasHemodialisa').getValue(),
		KdUnit: Ext.getCmp('txtKdUnit_PenJasHemodialisa').getValue(),
		KdDokter:Ext.getCmp('cboDOKTER_viPenJasHemodialisa').getValue(),
		Tgl: Ext.getCmp('dtfTglKunjung_PenJasHemodialisa').getValue(),
		TglTransaksiAsal:Ext.getCmp('dftTgltransaksiasal_PenJasHemodialisa').getValue(),
		urutmasuk:Ext.getCmp('txtUrutMasuk_PenJasHemodialisa').getValue(),
		KdCusto:KdCust,
		TmpCustoLama:KdCust, //Ext.getCmp('txtKdCustomerLamaHide').getValue(),//jika customer dengan transaksi lama
		NamaPesertaAsuransi:Ext.getCmp('txtNamaPesertaAsuransi_PenJasHemodialisa').getValue(),
		NoAskes:Ext.getCmp('txtNoAskes_PenJasHemodialisa').getValue(),
		NoSJP:Ext.getCmp('txtNoSJP_PenJasHemodialisa').getValue(),
		Shift: shifHemodialisa,
		TmpNotransaksi:Ext.getCmp('txtNoTransaksiAsal_PenJasHemodialisa').getValue(),
		KdKasirAsal:Ext.getCmp('txtKdKasir_PenJasHemodialisa').getValue(),
		KdSpesial:Ext.getCmp('txtKdSpesial_PenJasHemodialisa').getValue(),
		Kamar:Ext.getCmp('txtNoKamar_PenJasHemodialisa').getValue(),
		pasienBaru:pasienBaru,
		List:list(),
		kd_unit_hd:kd_unit_hd
		//KdProduk:''//dsTRDetailPenJasLabList.data.items[CurrentPenJasLab.row].data.KD_PRODUK

	};
	
    return params
};

function list(){
	var x='';
	var arr=[];
	for(var i = 0 ; i < dataSource_viPenJasHemodialisa.getCount();i++)
	{

		if (dataSource_viPenJasHemodialisa.data.items[i].data.kd_produk != '' && dataSource_viPenJasHemodialisa.data.items[i].data.deskripsi != '')
		{
			var o={};
			var y='';
			var z='@@##$$@@';
			o['URUT']= dataSource_viPenJasHemodialisa.data.items[i].data.urut;
			o['KD_PRODUK']= dataSource_viPenJasHemodialisa.data.items[i].data.kd_produk;
			o['QTY']= dataSource_viPenJasHemodialisa.data.items[i].data.qty;
			o['TGL_TRANSAKSI']= dataSource_viPenJasHemodialisa.data.items[i].data.tgl_transaksi;
			o['TGL_BERLAKU']= dataSource_viPenJasHemodialisa.data.items[i].data.tgl_berlaku;
			o['HARGA']= dataSource_viPenJasHemodialisa.data.items[i].data.harga;
			o['KD_TARIF']= dataSource_viPenJasHemodialisa.data.items[i].data.kd_tarif;
			o['cito']= dataSource_viPenJasHemodialisa.data.items[i].data.cito;
			arr.push(o);

		};
	}

	return Ext.encode(arr);
}

function getParamDeletePenJasHemodialisa(){
	var	params =
	{
		KdVendor:Ext.getCmp('txtNoTransaksi_PenJasHemodialisa').getValue()
	}
   
    return params
};

function ValidasiSavePenJasHemodialisa(modul,mBolHapus){
	var x = 1;
	if(PenJasHemodialisa.vars.nama.getValue() === ''){
		loadMask.hide();
		ShowPesanWarningPenJasHemodialisa('Nama pasien tidak boleh kosong!', 'Warning');
		x = 0;
	} 
	if(Ext.getCmp('cboDOKTER_viPenJasHemodialisa').getValue() == '' ){
		loadMask.hide();
		ShowPesanWarningPenJasHemodialisa('Dokter dialisa tidak boleh kosong!', 'Warning');
		x = 0;
	}
	if(dataSource_viPenJasHemodialisa.getCount() == 0){
		loadMask.hide();
		ShowPesanWarningPenJasHemodialisa('List item pemeriksaan masih kosong!', 'Warning');
		x = 0;
	}
	
	
	for(var i = 0 ; i < dataSource_viPenJasHemodialisa.getCount();i++){
		var o = dataSource_viPenJasHemodialisa.getRange()[i].data;
	
		if(o.kd_produk == '' || o.kd_produk == undefined){
			loadMask.hide();
			ShowPesanWarningPenJasHemodialisa('Item pemeriksaan masih kosong!', 'Warning');
			x = 0;
		}
		for(var j=0; j<dataSource_viPenJasHemodialisa.getCount();j++){
			var p=dataSource_viPenJasHemodialisa.getRange()[j].data;
			if(i != j && o.kd_produk == p.kd_produk){
				loadMask.hide();
				ShowPesanWarningPenJasHemodialisa('Item pemeriksaan tidak boleh sama!', 'Warning');
				x = 0;
			}
		}
	}
	return x;
};



function ShowPesanWarningPenJasHemodialisa(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorPenJasHemodialisa(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoPenJasHemodialisa(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};