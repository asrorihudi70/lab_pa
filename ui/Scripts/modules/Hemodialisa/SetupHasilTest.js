var NamaForm_viSetupHasilTest="Setup Hasil Test";
var mod_name_viSetupHasilTest="Setup Hasil Test";
var selectCount_viSetupHasilTest=50;
var now_viSetupHasilTest= new Date();
var tanggal = now_viSetupHasilTest.format("d/M/Y");
var jam = now_viSetupHasilTest.format("H/i/s");

var dataSource_viSetupHasilTest;
var dataSource_viSetupReferensiHasil;
var dataSource_viSetupItemDialisa;
var rowSelected_viSetupHasilTest;
var rowSelected_viSetupReferensiHasil;
var rowSelected_viSetupItemDialisa;
var GridDataView_viSetupHasilTest;
var GridDataView_viSetupReferensiHasil;
var GridDataView_viSetupItemDialisa;

var cboItemHasil;
var dKdItem;
var dItemHd;
var dsgridcombo_item;

var CurrentData_viSetupHasilTest =
{
	data: Object,
	details: Array,
	row: 0
};

var CurrentData_viSetupReferensiHasil =
{
	data: Object,
	details: Array,
	row: 0
};

var CurrentData_viSetupItemDialisa =
{
	data: Object,
	details: Array,
	row: 0
};


var SetupHasilTest={};
SetupHasilTest.form={};
SetupHasilTest.func={};
SetupHasilTest.vars={};
SetupHasilTest.func.parent=SetupHasilTest;
SetupHasilTest.form.ArrayStore={};
SetupHasilTest.form.ComboBox={};
SetupHasilTest.form.DataStore={};
SetupHasilTest.form.Record={};
SetupHasilTest.form.Form={};
SetupHasilTest.form.Grid={};
SetupHasilTest.form.Panel={};
SetupHasilTest.form.TextField={};
SetupHasilTest.form.Button={};

SetupHasilTest.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_item','item_hd','type_item','type_item_des','no_ref','kd_item','item', 'deskripsi','kd_dia'],
	data: []
});

SetupHasilTest.form.ArrayStore.b= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_item','item_hd'],
	data: []
});

CurrentPage.page = dataGrid_viSetupHasilTest(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dataGrid_viSetupHasilTest(mod_id_viSetupHasilTest){	
 	// Field kiriman dari Project Net.
    var FieldMaster_viSetupHasilTest = 
	[
		'kd_item','item_hd','type_item','type_item_des'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viSetupHasilTest = new WebApp.DataStore
	({
        fields: FieldMaster_viSetupHasilTest
    });
	dataGridItemHasil_SetupHasilTest();
	dataGridRefHasil();
	dataGridHasilDialisa(0);
	loadDataComboItemHasil();
	var FieldItem=['kd_item', 'item_hd'];
	dsgridcombo_item = new WebApp.DataStore({ fields: FieldItem });
	GridDataView_viSetupHasilTest = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viSetupHasilTest,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:true,
			height:270,
			anchor: '100% 100%',
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viSetupHasilTest = undefined;
							rowSelected_viSetupHasilTest = dataSource_viSetupHasilTest.getAt(row);
							CurrentData_viSetupHasilTest
							CurrentData_viSetupHasilTest.row = row;
							CurrentData_viSetupHasilTest.data = rowSelected_viSetupHasilTest.data;
							
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viSetupHasilTest = dataSource_viSetupHasilTest.getAt(ridx);
					if (rowSelected_viSetupHasilTest != undefined)
					{
						DataInitSetupHasilTest(rowSelected_viSetupHasilTest.data);
					}
					else
					{
					}
				}
				
				// End Function # --------------
			},
			
			/**
	        *	Mengatur tampilan pada Grid Setup Unit
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colKode_viSetupHasilTest',
						header: 'Kode Item',
						dataIndex: 'kd_item', //harus sama dengan nama kolom didatabase
						hideable:false,
						menuDisabled: true,
						width: 10
						
					},
					//-------------- ## --------------
					{
						id: 'colItem_viSetupHasilTest',
						header: 'Item',
						dataIndex: 'item_hd',
						hideable:false,
						menuDisabled: true,
						width: 20
					},
					{
						id: 'coltype_item_des_viSetupHasilTest',
						header: 'Type Item',
						dataIndex: 'type_item_des',
						hideable:false,
						menuDisabled: true,
						width: 20
					},
					//-------------- ## --------------
					{
						id: 'colTypeItem_viSetupHasilTest',
						header: 'Type Item',
						dataIndex: 'type_item',
						hidden:true,
						menuDisabled: true,
						width: 40
					}
					//-------------- ## --------------
					
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viSetupHasilTest',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viSetupHasilTest',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viSetupHasilTest != undefined)
							{
								DataInitSetupHasilTest(rowSelected_viSetupHasilTest.data);
							}
							
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viSetupHasilTest, selectCount_viSetupHasilTest, dataSource_viSetupHasilTest),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)	
	
	var FieldMaster_viSetupReferensiHasil = 
	[
		'no_ref','kd_item','item', 'deskripsi'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viSetupReferensiHasil = new WebApp.DataStore
	({
        fields: FieldMaster_viSetupReferensiHasil
    });
	
	GridDataView_viSetupReferensiHasil = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viSetupReferensiHasil,
			autoScroll: false,
			columnLines: true,
			flex:1,
			height:270,
			border:true,
			anchor: '100% 100%',
			//plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viSetupReferensiHasil = undefined;
							rowSelected_viSetupReferensiHasil = dataSource_viSetupReferensiHasil.getAt(row);
							CurrentData_viSetupReferensiHasil
							CurrentData_viSetupReferensiHasil.row = row;
							CurrentData_viSetupReferensiHasil.data = rowSelected_viSetupReferensiHasil.data;
							//DataInitSetupHasilTest(rowSelected_viSetupHasilTest.data);
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viSetupReferensiHasil = dataSource_viSetupReferensiHasil.getAt(ridx);
					if (rowSelected_viSetupReferensiHasil != undefined)
					{
						DataInitSetupReferensiHasil(rowSelected_viSetupReferensiHasil.data);
						//setLookUp_viSetupHasilTest(rowSelected_viSetupHasilTest.data);
					}
					else
					{
						//setLookUp_viSetupHasilTest();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Setup Unit
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colNo_ref_viSetupReferensiHasil',
						header: 'No. Ref',
						dataIndex: 'no_ref',
						hideable:false,
						menuDisabled: true,
						width: 10 
						
					},
					//-------------- ## --------------
					{
						id: 'colItem_viSetupReferensiHasil',
						header: 'Item',
						dataIndex: 'item_hd',
						hideable:false,
						menuDisabled: true,
						width: 20
					},
					//-------------- ## --------------
					{
						id: 'colDeskripsi_viSetupReferensiHasil',
						header: 'Deskripsi',
						dataIndex: 'deskripsi',
						hideable:false,
						menuDisabled: true,
						width: 20
					}
					//-------------- ## --------------
					
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viSetupReferensiHasil',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viSetupReferensiHasil',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viSetupReferensiHasil != undefined)
							{
								DataInitSetupReferensiHasil(rowSelected_viSetupReferensiHasil.data);
							}
							
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viSetupHasilTest, selectCount_viSetupHasilTest, dataSource_viSetupHasilTest),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)	
	//
	
	var FieldMaster_viSetupItemDialisa = 
	[
		'kd_item','item_hd','kd_dia'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viSetupItemDialisa = new WebApp.DataStore
	({
        fields: FieldMaster_viSetupItemDialisa
    });
	
	GridDataView_viSetupItemDialisa = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viSetupItemDialisa,
			autoScroll: false,
			columnLines: true,
			flex:1,
			height:500,
			border:true,
			anchor: '100% 100%',
			//plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.CellSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						cellselect: function(sm, row, rec)
						{
							rowSelected_viSetupItemDialisa = undefined;
							rowSelected_viSetupItemDialisa = dataSource_viSetupItemDialisa.getAt(row);
							CurrentData_viSetupItemDialisa
							CurrentData_viSetupItemDialisa.row = row;
							CurrentData_viSetupItemDialisa.data = rowSelected_viSetupItemDialisa.data;
							dKdItem = CurrentData_viSetupItemDialisa.data.kd_item;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viSetupItemDialisa = dataSource_viSetupItemDialisa.getAt(ridx);
					if (rowSelected_viSetupItemDialisa != undefined)
					{
						
					}
					else
					{
						
					}
				},
				render: function(){
					Ext.getCmp('btnAddRowItemDialisa').disable();
					Ext.getCmp('btnSimpanItemDialisa').disable();
					Ext.getCmp('btnDeleteItemDialisa').disable();
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Setup Unit
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
								
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colKd_Item_viSetupItemDialisa',
						header: 'Kode Item',
						dataIndex: 'kd_item',
						hideable:false,
						menuDisabled: true,
						width: 5
						
					},
					//-------------- ## --------------
					{
						id		: 'col_Item_viSetupItemDialisa',
						header: 'Item',
						dataIndex: 'item_hd',
						hideable:false,
						menuDisabled: true,
						width: 30,
						editor:new Nci.form.Combobox.autoComplete({
							
							store	: SetupHasilTest.form.ArrayStore.b,
							select	: function(a,b,c){
							var line = GridDataView_viSetupItemDialisa.getSelectionModel().selection.cell[0];
								dataSource_viSetupItemDialisa.data.items[line].data.kd_item=b.data.kd_item;
								dataSource_viSetupItemDialisa.data.items[line].data.item_hd=b.data.item_hd;
								GridDataView_viSetupItemDialisa.getView().refresh();
							},
							insert	: function(o){
								return {
									kd_item        	: o.kd_item,
									item_hd 		: o.item_hd,
									text			:  '<table style="font-size: 11px;"><tr><td width="60">'+o.kd_item+'</td><td width="150">'+o.item_hd+'</td></tr></table>'
								}
							},
							url		: baseURL + "index.php/hemodialisa/functionSetupHasilTest/getItemHasilTest",
							valueField: 'deskripsi',
							displayField: 'text',
							listWidth: 210
						})
					}
					//-------------- ## --------------
					
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viSetupItemDialisa',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add Row',
						iconCls: 'Edit_Tr',
						id: 'btnAddRowItemDialisa',
						handler: function(){
							var records = new Array();
							records.push(new dataSource_viSetupItemDialisa.recordType());
							dataSource_viSetupItemDialisa.add(records);
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpanItemDialisa',
						handler: function()
						{
							loadMask.show();
							dataSave_ItemDialisa();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete Row',
						iconCls: 'remove',
						id: 'btnDeleteItemDialisa',
						handler: function()
						{
							var line = GridDataView_viSetupItemDialisa.getSelectionModel().selection.cell[0];
							dataSource_viSetupItemDialisa.removeAt(line);
							GridDataView_viSetupItemDialisa.getView().refresh();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefreshItemDialisa',
						handler: function()
						{
							dataSource_viSetupItemDialisa.removeAll();
							dataGridHasilDialisa(Ext.getCmp('cboJenisDialisa').getValue());
							Ext.getCmp('btnAddRowItemDialisa').disable();
							Ext.getCmp('btnSimpanItemDialisa').disable();
							Ext.getCmp('btnDeleteItemDialisa').disable();
						}
					},
					{
						xtype: 'tbseparator'
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viSetupHasilTest, selectCount_viSetupHasilTest, dataSource_viSetupHasilTest),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)
	
	var PanelInputSetupHasil = new Ext.FormPanel({
		labelAlign: 'top',
		layout:'form',
		border: false,  
		autoHeight:true,
        title: '',
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [GetFormSetupItemHasil()],
		tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAdd_SetupHasil',
						handler: function(){
							AddNewSetupHasil();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_SetupHasil',
						handler: function()
						{
							loadMask.show();
							dataSave_viSetupHasil();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_SetupHasil',
						handler: function()
						{
							Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
								if (button == 'yes'){
									loadMask.show();
									dataDelete_viSetupHasil();
								}
							});
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_SetupHasil',
						handler: function()
						{
							dataSource_viSetupHasilTest.removeAll();
							dataGridItemHasil_SetupHasilTest();
						}
					},
					{
						xtype: 'tbseparator'
					}
				]
			}
	})
	
	var PanelInputSetupReferensiHasil = new Ext.FormPanel({
		labelAlign: 'top',
		layout:'form',
		border: false,  
		autoHeight:true,
        title: '',
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [GetFormSetupReferensiHasil()],
		tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAdd_SetupReferensiHasil',
						handler: function(){
							AddNewSetupRefHasil();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_SetupReferensiHasil',
						handler: function()
						{
							loadMask.show();
							dataSave_SetupRefHasil();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_SetupReferensiHasil',
						handler: function()
						{
							Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
								if (button == 'yes'){
									loadMask.show();
									dataDelete_viSetupRefHasil();
								}
							});
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_SetupReferensiHasil',
						handler: function()
						{
							dataSource_viSetupReferensiHasil.removeAll();
							dataGridRefHasil();
						}
					},
					{
						xtype: 'tbseparator'
					}
					
				]
			}

	})
	
	var PanelInputSetupItemDialisa = new Ext.FormPanel({
		labelAlign: 'top',
		layout:'form',
		border: false,  
		autoHeight:true,
        title: '',
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [GetFormSetupItemDialisa()],
		tbar:[
			{
				xtype: 'button',
				text: 'Add New',
				iconCls: 'add',
				id: 'btnAddNewItemDialisa',
				handler: function(){
					GridDataView_viSetupItemDialisa.store.removeAll();
					var records = new Array();
					records.push(new dataSource_viSetupItemDialisa.recordType());
					dataSource_viSetupItemDialisa.add(records);
					Ext.getCmp('btnAddRowItemDialisa').enable();
					Ext.getCmp('btnSimpanItemDialisa').enable();
					Ext.getCmp('btnDeleteItemDialisa').enable();
				}
			},
			{
				xtype: 'tbseparator'
			},
			{
				xtype: 'button',
				text: 'Delete',
				iconCls: 'remove',
				id: 'btnDeleteDataItemDialisa',
				handler: function()
				{
					if(( dKdItem == '' || dKdItem == undefined) ){
						ShowPesanWarningSetupHasil('Pilih data yang akan di hapus', 'Warning');
					} else{
						Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
							if (button == 'yes'){
								loadMask.show();
								dataDelete_ItemDialisa();
							}
						});
					}
				}
			},
		
		]

	})
	
	var FrmHasilTest= new Ext.Panel(
	{
        layout: {
        type: 'accordion',
        titleCollapse: true,
        multi: true,
        fill: false,
        animate: false, 
		id: 'accordionNavigationContainerHemodialisa',
        flex: 1
        },

        
		width: 1000,
		height: 900,
		defaults: {
		},
		layoutConfig: {
			titleCollapse: false,
			animate: true,
			activeOnTop: false
		},
		items: [{
			title: 'Setup Item Hasil',
			height: 450,
			items:[
				PanelInputSetupHasil,
				GridDataView_viSetupHasilTest
			]
		},{
			title: 'Setup Referensi Hasil',
			height: 450,
			items:[
				PanelInputSetupReferensiHasil,
				GridDataView_viSetupReferensiHasil
			]
		},{
			title: 'Setup Item Dialisa',
			height: 450,
			items:[
				PanelInputSetupItemDialisa,
				GridDataView_viSetupItemDialisa
			]
		}]
	}
	);
	
	//LAYAR FORM UTAMA 
	var FrmData_viSetupHasilTest = new Ext.Panel
    (
		{
			title: NamaForm_viSetupHasilTest,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viSetupHasilTest,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			// KONTEN DARI LAYAR FORM UTAMA
			items: [ FrmHasilTest],
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_viSetupHasilTest;
    //-------------- # End form filter # --------------
}

function GetFormSetupItemHasil(){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						// width: 500,
						height: 100,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'Kode Item'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 0,
								xtype: 'textfield',
								id: 'txtKdItem_SetupItemHasil',
								name: 'txtKdItem_SetupItemHasil',
								width: 100,
								allowBlank: false,
								readOnly: true,
								maxLength:3,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									},
									blur: function(a){
										
									}
								}
							},
							
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'Item'
							},
							{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 30,
								xtype: 'textfield',
								id: 'txtItem_SetupItemHasil',
								name: 'txtItem_SetupItemHasil',
								width: 200,
								allowBlank: false,
								maxLength:3,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									},
									blur: function(a){
										
									}
								}
							},
							
							{
								x: 10,
								y: 60,
								xtype: 'label',
								text: 'Type Item'
							},
							{
								x: 120,
								y: 60,
								xtype: 'label',
								text: ':'
							},
							comboTypeItemHasil()
						]
					}
				]
			}
		]		
	};
        return items;
}

function comboTypeItemHasil(){
  var cboTypeItemHasil = new Ext.form.ComboBox({
        id:'cboTypeItemHasil',
        x: 130,
        y: 60,
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:true,
		editable: false,
        mode: 'local',
        width: 120,
        emptyText:'',
		tabIndex:3,
        store: new Ext.data.ArrayStore( {
            id: 0,
            fields:[
                'Id',
                'displayText'
            ],
            data: [[0, 'Acountable'],[1, 'Text'], [2, 'Lookup']]
        }),
        valueField: 'Id',
        displayField: 'displayText',
        value:0,
        listeners:{
			'select': function(a,b,c){
			}
        }
	});
	return cboTypeItemHasil;
};

function GetFormSetupReferensiHasil(){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 100,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'No. Ref'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 0,
								xtype: 'textfield',
								id: 'txtNoRef_SetupReferensiHasil',
								name: 'txtNoRef_SetupReferensiHasil',
								width: 100,
								allowBlank: false,
								maxLength:3,
								readOnly: true,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									},
									blur: function(a){
										
									}
								}
							},
							
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'Item'
							},
							{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							mComboItemHasil(),
							
							{
								x: 10,
								y: 60,
								xtype: 'label',
								text: 'Deskripsi'
							},
							{
								x: 120,
								y: 60,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 60,
								xtype: 'textfield',
								id: 'txtDes_SetupReferensiHasil',
								name: 'txtDes_SetupReferensiHasil',
								width: 200,
								allowBlank: false,
								maxLength:3,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									},
									blur: function(a){
										
									}
								}
							}
						]
					}
				]
			}
		]		
	};
        return items;
}

function GetFormSetupItemDialisa(){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 30,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'Dialisa'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							comboJenisDialisa()
						]
					}
				]
			}
		]		
	};
        return items;
}

function comboJenisDialisa(){
  var cboJenisDialisa = new Ext.form.ComboBox({
        id:'cboJenisDialisa',
        x: 130,
        y: 0,
        typeAhead: true,
        triggerAction: 'all',
        lazyRender:true,
		editable: false,
        mode: 'local',
        width: 120,
        emptyText:'',
		tabIndex:3,
        store: new Ext.data.ArrayStore( {
            id: 0,
            fields:[
                'Id',
                'displayText'
            ],
            data: [[0, 'Pradialisa'],[1, 'Dialisa'], [2, 'Pascadialisa']]
        }),
        valueField: 'Id',
        displayField: 'displayText',
        value:0,
        listeners:{
			'select': function(a,b,c){
				if(b.data.displayText == 'Pradialisa')
				{
					dataSource_viSetupItemDialisa.removeAll();
					dataGridHasilDialisa(0);
				}else if (b.data.displayText == 'Dialisa')
				{
					dataSource_viSetupItemDialisa.removeAll();
					dataGridHasilDialisa(1);
				}else if(b.data.displayText == 'Pascadialisa')
				{
					dataSource_viSetupItemDialisa.removeAll();
					dataGridHasilDialisa(2);
				}
			}
        }
	});
	return cboJenisDialisa;
};

function dataGridItemHasil_SetupHasilTest(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/hemodialisa/functionSetupHasilTest/getGridItemTest",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorSetupHasil('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dataSource_viSetupHasilTest.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						recs.push(new recType(cst.listData[i]));
					}
					dataSource_viSetupHasilTest.add(recs);
					GridDataView_viSetupHasilTest.getView().refresh();
				}
				else 
				{
					ShowPesanErrorSetupHasil('Gagal membaca data obat', 'Error');
				}
			}
		}
		
	)
	
}

function AddNewSetupHasil(){
	Ext.getCmp('txtKdItem_SetupItemHasil').setValue('');
	Ext.getCmp('txtItem_SetupItemHasil').setValue('');
};

function dataSave_viSetupHasil(){
	if (ValidasiSaveSetupHasil(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/hemodialisa/functionSetupHasilTest/save",
				params: getParamSaveSetupHasil(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupHasil('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupHasil('Berhasil menyimpan data ini','Information');
						Ext.getCmp('txtKdItem_SetupItemHasil').setValue(cst.kode);
						dataSource_viSetupHasilTest.removeAll();
						dataGridItemHasil_SetupHasilTest();
						loadDataComboItemHasil();
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupHasil('Gagal menyimpan data ini', 'Error');
						dataSource_viSetupHasilTest.removeAll();
						dataGridItemHasil_SetupHasilTest();
					}
				}
			}
			
		)
	}
}

function getParamSaveSetupHasil(){
	var type_item="";
	if(Ext.getCmp('cboTypeItemHasil').getValue()=='Acountable')
	{
		type_item=0;
	}else if(Ext.getCmp('cboTypeItemHasil').getValue()=='Text'){
		type_item=1;
	}else if(Ext.getCmp('cboTypeItemHasil').getValue()=='Lookup'){
		type_item=2;
	}else{
		type_item=Ext.getCmp('cboTypeItemHasil').getValue();
	}
	
	var	params =
	{
		KdItem:Ext.getCmp('txtKdItem_SetupItemHasil').getValue(),
		Item:Ext.getCmp('txtItem_SetupItemHasil').getValue(),
		TypeItem:type_item,
		
	}
    return params
};

function ValidasiSaveSetupHasil(modul,mBolHapus){
	var x = 1;
	if(Ext.getCmp('txtItem_SetupItemHasil').getValue() ===''){
		loadMask.hide();
		ShowPesanWarningSetupHasil('Item masih kosong', 'Warning');
		x = 0;
	}
	if(Ext.getCmp('cboTypeItemHasil').getValue() ===''){
		loadMask.hide();
		ShowPesanWarningSetupHasil('Type item belum dipilih', 'Warning');
		x = 0;
	}
	return x;
};

function ShowPesanWarningSetupHasil(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};
function ShowPesanInfoSetupHasil(str, modul) {
  Ext.MessageBox.show({
	    title: modul,
	    msg: str,
	    buttons: Ext.MessageBox.OK,
	    icon: Ext.MessageBox.INFO,
		width:250
	});
};

function ShowPesanErrorSetupHasil(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function dataDelete_viSetupHasil(){
	if (ValidasiSaveSetupHasil(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/hemodialisa/functionSetupHasilTest/delete",
				params: getParamDeleteSetupHasil(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupHasil('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupHasil('Berhasil menghapus data ini','Information');
						AddNewSetupHasil()
						dataSource_viSetupHasilTest.removeAll();
						dataGridItemHasil_SetupHasilTest();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupHasil('Gagal menghapus data ini', 'Error');
						dataSource_viSetupHasilTest.removeAll();
						dataGridItemHasil_SetupHasilTest();
					}
				}
			}
			
		)
	}
}

function getParamDeleteSetupHasil(){
	var	params =
	{
		KdItem:Ext.getCmp('txtKdItem_SetupItemHasil').getValue()
	}
   
    return params
};

function DataInitSetupHasilTest(rowdata){
	Ext.getCmp('txtKdItem_SetupItemHasil').setValue(rowdata.kd_item);
	Ext.getCmp('txtItem_SetupItemHasil').setValue(rowdata.item_hd);
	Ext.getCmp('cboTypeItemHasil').setValue(rowdata.type_item_des);
};

function loadDataComboItemHasil(param){
	if (param==='' || param===undefined) {
		param={
			text: '0',
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/hemodialisa/functionSetupHasilTest/getItemHasil",
		params: param,
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cboItemHasil.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType =   ds_ItemHasil.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				ds_ItemHasil.add(recs);
				console.log(o);
			}
		}
	});
}

function mComboItemHasil()
{
	var Field = ['kd_item','item_hd'];
    ds_ItemHasil = new WebApp.DataStore({fields: Field});
	loadDataComboItemHasil();
    cboItemHasil = new Ext.form.ComboBox
	(
		{
			x: 130,
			y: 30,
			id:'cboItemHasil',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Silahkan Pilih...',
			fieldLabel: 'Pendaftaran Per Shift ',
			width:200,
			store: ds_ItemHasil,
			valueField: 'kd_item',
			displayField: 'item_hd',
			value:'Pilih Item',
			listeners:
			{
					'select': function(a,b,c)
					{
						selectSetPilihan=b.data.displayText ;
					}
			}
		}
	);
	return cboItemHasil;
};

function dataGridRefHasil(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/hemodialisa/functionSetupHasilTest/getGridRefHasil",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorSetupHasil('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dataSource_viSetupReferensiHasil.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						recs.push(new recType(cst.listData[i]));
					}
					dataSource_viSetupReferensiHasil.add(recs);
					GridDataView_viSetupReferensiHasil.getView().refresh();
				}
				else 
				{
					ShowPesanErrorSetupHasil('Gagal membaca data obat', 'Error');
				}
			}
		}
		
	)
	
}

function DataInitSetupReferensiHasil(rowdata){
	Ext.getCmp('txtNoRef_SetupReferensiHasil').setValue(rowdata.no_ref);
	Ext.getCmp('cboItemHasil').setValue(rowdata.item_hd);
	Ext.getCmp('txtDes_SetupReferensiHasil').setValue(rowdata.deskripsi);
}
function AddNewSetupRefHasil(){
	Ext.getCmp('txtNoRef_SetupReferensiHasil').setValue('');
	Ext.getCmp('cboItemHasil').setValue('');
	Ext.getCmp('txtDes_SetupReferensiHasil').setValue('');
};

function dataSave_SetupRefHasil(){
	if (ValidasiSaveSetupRefHasil(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/hemodialisa/functionSetupHasilTest/saveRef",
				params: getParamSaveSetupRefHasil(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupHasil('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupHasil('Berhasil menyimpan data ini','Information');
						Ext.getCmp('txtNoRef_SetupReferensiHasil').setValue(cst.kode);
						dataSource_viSetupReferensiHasil.removeAll();
						dataGridRefHasil();
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupHasil('Gagal menyimpan data ini', 'Error');
						dataSource_viSetupReferensiHasil.removeAll();
						dataGridRefHasil();
					}
				}
			}
		)
	}
}

function getParamSaveSetupRefHasil(){
	var	params =
	{
		NoRef:Ext.getCmp('txtNoRef_SetupReferensiHasil').getValue(),
		KdItem:Ext.getCmp('cboItemHasil').getValue(),
		Desk:Ext.getCmp('txtDes_SetupReferensiHasil').getValue(),
		
	}
    return params
};

function ValidasiSaveSetupRefHasil(modul,mBolHapus){
	var x = 1;
	if(Ext.getCmp('txtDes_SetupReferensiHasil').getValue() ===''){
		loadMask.hide();
		ShowPesanWarningSetupHasil('Deskripsi masih kosong', 'Warning');
		x = 0;
	}
	return x;
};

function dataDelete_viSetupRefHasil(){
	if (ValidasiSaveSetupRefHasil(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/hemodialisa/functionSetupHasilTest/deleteRef",
				params: getParamDeleteSetupRefHasil(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupHasil('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupHasil('Berhasil menghapus data ini','Information');
						AddNewSetupRefHasil()
						dataSource_viSetupReferensiHasil.removeAll();
						dataGridRefHasil();
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupHasil('Gagal menghapus data ini', 'Error');
						dataSource_viSetupReferensiHasil.removeAll();
						dataGridRefHasil();
					};
				}
			}
			
		)
	}
}
function getParamDeleteSetupRefHasil(){
	var	params =
	{
		NoRef:Ext.getCmp('txtNoRef_SetupReferensiHasil').getValue()
	}
   
    return params
};

function dataGridHasilDialisa(param){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/hemodialisa/functionSetupHasilTest/getGridHasilDialisa",
			params: {kd_dia:param},
			failure: function(o)
			{
				ShowPesanErrorSetupHasil('Hubungi Admin', 'Error');('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dataSource_viSetupItemDialisa.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						recs.push(new recType(cst.listData[i]));
					}
					dataSource_viSetupItemDialisa.add(recs);
					GridDataView_viSetupItemDialisa.getView().refresh();
				}
				else 
				{
					ShowPesanErrorSetupHasil('Gagal membaca data obat', 'Error');
				}
			}
		}
		
	)
	
}

function dataGriItemHasilDialisa(item_hd,b){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/hemodialisa/functionSetupHasilTest/getItemHasilTest",
			params: {
				item_hd:item_hd
			},
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorSetupHasil('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					var recs=[],
						recType=dataSource_viSetupItemDialisa.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						recs.push(new recType(cst.ListDataObj[i]));
					}
					dataSource_viSetupItemDialisa.add(recs);
					GridDataView_viSetupItemDialisa.getView().refresh();
					
					Ext.getCmp('colKd_Item_viSetupItemDialisa').setValue(b.data.kd_item);
				}
				else 
				{
					loadMask.hide();
					ShowPesanErrorSetupHasil('Gagal membaca data pemeriksaan hemodialisa', 'Error');
				}
			}
		}
		
	)
	
}

function dataSave_ItemDialisa(){
	if (ValidasiSaveItemDialisa(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/hemodialisa/functionSetupHasilTest/saveItemDialisa",
				params: getParamSaveItemDialisa(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupHasil('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupHasil('Berhasil menyimpan data ini','Information');
						
						dataSource_viSetupItemDialisa.removeAll();
						dataGridHasilDialisa(Ext.getCmp('cboJenisDialisa').getValue());
						Ext.getCmp('btnAddRowItemDialisa').disable();
						Ext.getCmp('btnSimpanItemDialisa').disable();
						Ext.getCmp('btnDeleteItemDialisa').disable();
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupHasil('Gagal menyimpan data ini', 'Error');
						dataSource_viSetupItemDialisa.removeAll();
						dataGridHasilDialisa(Ext.getCmp('cboJenisDialisa').getValue());
					
					}
				}
			}
			
		)
	}
}

function ValidasiSaveItemDialisa(modul,mBolHapus){
	var x = 1;
	if(dataSource_viSetupItemDialisa.getCount() <= 0){
		loadMask.hide();
		ShowPesanWarningSetupHasil('Jumlah baris minimal 1', 'Warning');
		x = 0;
	} else{
		for(var i=0; i<dataSource_viSetupItemDialisa.getCount() ; i++){
			var o=dataSource_viSetupItemDialisa.getRange()[i].data;
			if(o.item_hd == undefined || o.item_hd == ''){
				loadMask.hide();
				ShowPesanWarningSetupHasil('Item tidak boleh kosong', 'Warning');
				x = 0;
			}
		}
	}
	
	return x;
};

function getParamSaveItemDialisa(){
	var	params =
	{
		KdDia:Ext.getCmp('cboJenisDialisa').getValue()
	}
	
	params['jml']=dataSource_viSetupItemDialisa.getCount();
	for(var i = 0 ; i <dataSource_viSetupItemDialisa.getCount();i++)
	{
		params['kd_item-'+i]=dataSource_viSetupItemDialisa.data.items[i].data.kd_item;	
	}
   
    return params
};

function dataDelete_ItemDialisa(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/hemodialisa/functionSetupHasilTest/deleteItemDialisa",
			params: getParamDeleteItemDialisa(),
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorSetupHasil('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					loadMask.hide();
					ShowPesanInfoSetupHasil('Berhasil menghapus data ini','Information');
					
					dataSource_viSetupItemDialisa.removeAll();
					dataGridHasilDialisa(Ext.getCmp('cboJenisDialisa').getValue());
					
					Ext.getCmp('btnAddRowItemDialisa').disable();
					Ext.getCmp('btnSimpanItemDialisa').disable();
					Ext.getCmp('btnDeleteItemDialisa').disable();
				}
				else 
				{
					loadMask.hide();
					ShowPesanErrorSetupHasil('Gagal menghapus data ini', 'Error');
					dataSource_viSetupItemDialisa.removeAll();
					dataGridHasilDialisa(Ext.getCmp('cboJenisDialisa').getValue());
					
				}
			}
		}
	)
}

function getParamDeleteItemDialisa(){
	var	params =
	{
		kd_item : dKdItem,
		kd_dia :Ext.getCmp('cboJenisDialisa').getValue()
	}
   
    return params
};

