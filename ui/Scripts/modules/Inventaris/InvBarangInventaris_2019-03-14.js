var dataSource_viInvMasterBarangInventaris;
var selectCount_viInvMasterBarangInventaris=50;
var NamaForm_viInvMasterBarangInventaris="Barang Inventaris";
var mod_name_viInvMasterBarangInventaris="Barang Inventaris";
var now_viInvMasterBarangInventaris= new Date();
var rowSelected_viInvMasterBarangInventaris;
var setLookUps_viInvMasterBarangInventaris;
var tanggal = now_viInvMasterBarangInventaris.format("d/M/Y");
var jam = now_viInvMasterBarangInventaris.format("H/i/s");
var tmpkriteria;
var DataGridJenisObat;
var GridDataView_viInvMasterBarangInventaris;


var CurrentData_viInvMasterBarangInventaris =
{
	data: Object,
	details: Array,
	row: 0
};

var InvMasterBarangInventaris={};
InvMasterBarangInventaris.form={};
InvMasterBarangInventaris.func={};
InvMasterBarangInventaris.vars={};
InvMasterBarangInventaris.func.parent=InvMasterBarangInventaris;
InvMasterBarangInventaris.form.ArrayStore={};
InvMasterBarangInventaris.form.ComboBox={};
InvMasterBarangInventaris.form.DataStore={};
InvMasterBarangInventaris.form.Record={};
InvMasterBarangInventaris.form.Form={};
InvMasterBarangInventaris.form.Grid={};
InvMasterBarangInventaris.form.Panel={};
InvMasterBarangInventaris.form.TextField={};
InvMasterBarangInventaris.form.Button={};

InvMasterBarangInventaris.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_vendor', 'vendor', 'contact', 'alamat', 'kota', 'telepon1', 'telepon2',
			'fax', 'kd_pos', 'negara', 'norek', 'term'],
	data: []
});

CurrentPage.page = dataGrid_viInvMasterBarangInventaris(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dataGrid_viInvMasterBarangInventaris(mod_id_viInvMasterBarangInventaris){	
 	
 	// Field kiriman dari Project Net.
    var FieldMaster_viInvMasterBarangInventaris = 
	[
		'kd_vendor', 'vendor', 'contact', 'alamat', 'kota', 'telepon1', 'telepon2',
		'fax', 'kd_pos', 'negara', 'norek', 'term'
	];


	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viInvMasterBarangInventaris = new WebApp.DataStore
	({
        fields: FieldMaster_viInvMasterBarangInventaris
    });
    dataGriInvMasterBarangInventaris();
    // Grid Apotek Perencanaan # --------------
	GridDataView_viInvMasterBarangInventaris = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viInvMasterBarangInventaris,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:true,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viInvMasterBarangInventaris = undefined;
							rowSelected_viInvMasterBarangInventaris = dataSource_viInvMasterBarangInventaris.getAt(row);
							CurrentData_viInvMasterBarangInventaris
							CurrentData_viInvMasterBarangInventaris.row = row;
							CurrentData_viInvMasterBarangInventaris.data = rowSelected_viInvMasterBarangInventaris.data;
							//DataInitInvMasterBarangInventaris(rowSelected_viInvMasterBarangInventaris.data);
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viInvMasterBarangInventaris = dataSource_viInvMasterBarangInventaris.getAt(ridx);
					if (rowSelected_viInvMasterBarangInventaris != undefined)
					{
						DataInitInvMasterBarangInventaris(rowSelected_viInvMasterBarangInventaris.data);
						//setLookUp_viInvMasterBarangInventaris(rowSelected_viInvMasterBarangInventaris.data);
					}
					else
					{
						//setLookUp_viInvMasterBarangInventaris();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Setup vendor
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header: 'Kode Kelompok',
						dataIndex: 'kd_inv',
						hideable:false,
						menuDisabled: true,
						width: 40
						
					},
					//-------------- ## --------------
					{
						header: 'No Urut',
						dataIndex: 'no_urut_brg',
						hideable:false,
						menuDisabled: true,
						width: 30
					},
					//-------------- ## --------------
					{
						header: 'Nama Barang',
						dataIndex: 'nama_brg',
						hideable:false,
						menuDisabled: true,
						width: 90
						
					},
					//-------------- ## --------------
					{
						header: 'Satuan',
						dataIndex: 'satuan',
						hideable:false,
						menuDisabled: true,
						width: 50
					},
					//-------------- ## --------------
					{
						header: 'Kelompok',
						dataIndex: 'nama_sub',
						hideable:false,
						menuDisabled: true,
						width: 50
					}
					//-------------- ## --------------
					
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viInvMasterBarangInventaris',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viInvMasterBarangInventaris',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viInvMasterBarangInventaris != undefined)
							{
								DataInitInvMasterBarangInventaris(rowSelected_viInvMasterBarangInventaris.data);
							}
							
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viInvMasterBarangInventaris, selectCount_viInvMasterBarangInventaris, dataSource_viInvMasterBarangInventaris),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)	
	var PanelTabInvMasterBarangInventaris = new Ext.FormPanel({
        labelAlign: 'top',
		layout:'form',
		border: false,  
		autoHeight:true,
        title: '',
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [PanelInputVendor()]
		
	})
	// Kriteria filter pada Grid # --------------
    var FrmData_viInvMasterBarangInventaris = new Ext.Panel
    (
		{
			title: NamaForm_viInvMasterBarangInventaris,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viInvMasterBarangInventaris,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ PanelTabInvMasterBarangInventaris,
					GridDataView_viInvMasterBarangInventaris],
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAddBarang_viInvMasterBarangInventaris',
						handler: function(){
							AddNewInvMasterBarangInventaris();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpanBarang_viInvMasterBarangInventaris',
						handler: function()
						{
							loadMask.show();
							dataSave_viInvMasterBarangInventaris();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDeleteBarang_viInvMasterBarangInventaris',
						handler: function()
						{
							Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
								if (button == 'yes'){
									loadMask.show();
									dataDelete_viInvMasterBarangInventaris();
								}
							});
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_viInvMasterBarangInventaris',
						handler: function()
						{
							loadMask.show();
							dataSource_viInvMasterBarangInventaris.removeAll();
							dataGriInvMasterBarangInventaris();
							loadMask.hide();
						}
					},
					{
						xtype: 'tbseparator'
					}
					
				]
			}
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_viInvMasterBarangInventaris;
    //-------------- # End form filter # --------------
}

function PanelInputVendor(rowdata){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 120,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'Kode Kelompok'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							InvMasterBarangInventaris.vars.masterbarang=new Nci.form.Combobox.autoComplete({
								x: 130,
								y: 0,
								width: 110,
								tabIndex:2,
								store	: InvMasterBarangInventaris.form.ArrayStore.a,
								select	: function(a,b,c){
									console.log(b);
									Ext.getCmp('txtSubKelompok_InvMasterBarangInventaris').setValue(b.data.nama_sub);
									
								},
								insert	: function(o){
									return {
										kd_inv      	: o.kd_inv,
										inv_kd_inv 		: o.inv_kd_inv,
										nama_sub		: o.nama_sub,
										text			:  '<table style="font-size: 11px;"><tr><td width="100">'+o.kd_inv+'</td><td width="100">'+o.inv_kd_inv+'</td><td width="450">'+o.nama_sub+'</td></tr></table>'
									}
								},
								url		: baseURL + "index.php/inventaris/functionMasterBarangInventaris/getGridAutoKelompokBarang_BarangInv",
								valueField: 'kd_inv',
								displayField: 'text',
								listWidth: 620
							}),
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'Sub Kelompok'
							},
							{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 30,	
								xtype: 'textfield',
								name: 'txtSubKelompok_InvMasterBarangInventaris',
								id: 'txtSubKelompok_InvMasterBarangInventaris',
								width: 250,
								readOnly:true
							},
							/* InvMasterBarangInventaris.vars.nama=new Nci.form.Combobox.autoComplete({
								x: 130,
								y: 30,
								tabIndex:2,
								store	: InvMasterBarangInventaris.form.ArrayStore.a,
								select	: function(a,b,c){
									
									Ext.getCmp('txtKdVendor_InvMasterBarangInventaris').setValue(b.data.kd_vendor);
									Ext.getCmp('txtKontak_InvMasterBarangInventaris').setValue(b.data.contact);
									Ext.getCmp('txtAlamat_InvMasterBarangInventaris').setValue(b.data.alamat);
									Ext.getCmp('txtKota_InvMasterBarangInventaris').setValue(b.data.kota);
									Ext.getCmp('txtKodePos_InvMasterBarangInventaris').setValue(b.data.kd_pos);
									Ext.getCmp('txtNegara_InvMasterBarangInventaris').setValue(b.data.negara);
									Ext.getCmp('txtTelepon_InvMasterBarangInventaris').setValue(b.data.telepon1);
									Ext.getCmp('txtTelepon2_InvMasterBarangInventaris').setValue(b.data.telepon2);
									Ext.getCmp('txtFax_InvMasterBarangInventaris').setValue(b.data.fax);
									Ext.getCmp('txtNorek_InvMasterBarangInventaris').setValue(b.data.norek);
									Ext.getCmp('txtTempo_InvMasterBarangInventaris').setValue(b.data.term);
									GridDataView_viInvMasterBarangInventaris.getView().refresh();
									
								},
								onShowList:function(a){
									dataSource_viInvMasterBarangInventaris.removeAll();
									
									var recs=[],
									recType=dataSource_viInvMasterBarangInventaris.recordType;
										
									for(var i=0; i<a.length; i++){
										recs.push(new recType(a[i]));
									}
									dataSource_viInvMasterBarangInventaris.add(recs);
									
								},
								insert	: function(o){
									return {
										kd_vendor      	: o.kd_vendor,
										vendor 			: o.vendor,
										contact			: o.contact,
										alamat			: o.alamat,
										kota			: o.kota,
										telepon1		: o.telepon1,
										telepon2		: o.telepon2,
										fax				: o.fax,
										kd_pos			: o.kd_pos,
										negara			: o.negara,
										term			: o.term,
										norek			: o.norek,
										text			:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_vendor+'</td><td width="200">'+o.vendor+'</td></tr></table>'
									}
								},
								url		: baseURL + "index.php/apotek/functionInvMasterBarangInventaris/getVendorGrid",
								valueField: 'vendor',
								displayField: 'text',
								listWidth: 280
							}), */
							{
								x: 10,
								y: 60,
								xtype: 'label',
								text: 'Nama Barang'
							},
							{
								x: 120,
								y: 60,
								xtype: 'label',
								text: ':'
							},
							{
								x:130,
								y:60,	
								xtype: 'textfield',
								name: 'txtNamaBarang_InvMasterBarangInventaris',
								id: 'txtNamaBarang_InvMasterBarangInventaris',
								width: 300
							},
							{
								x: 10,
								y: 90,
								xtype: 'label',
								text: 'Satuan'
							},
							{
								x: 120,
								y: 90,
								xtype: 'label',
								text: ':'
							},
							comboSatuan_InvMasterBarangInventaris(),
							/* {
								x:130,
								y:90,	
								xtype: 'textfield',
								name: 'txtSatuan_InvMasterBarangInventaris',
								id: 'txtSatuan_InvMasterBarangInventaris',
								width : 110
							}, */
							
							//-------------- ## --------------
							{
								x: 250,
								y: 0,
								xtype: 'label',
								text: 'No Urut'
							},
							{
								x: 300,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 310,
								y: 0,
								xtype: 'textfield',
								//xtype: 'numberfield',
								name: 'txtNoUrut_InvMasterBarangInventaris',
								id: 'txtNoUrut_InvMasterBarangInventaris',
								tabIndex:6,
								width: 70,
								readOnly:true,
								style: 'text-align: right'
							}
						]
					}
				]
			}
		]		
	};
        return items;
}
//------------------end---------------------------------------------------------


function dataGriInvMasterBarangInventaris(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/inventaris/functionMasterBarangInventaris/getGridBarang",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorInvMasterBarangInventaris('Error grid! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dataSource_viInvMasterBarangInventaris.removeAll()
					var recs=[],
						recType=dataSource_viInvMasterBarangInventaris.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dataSource_viInvMasterBarangInventaris.add(recs);
					
					
					
					GridDataView_viInvMasterBarangInventaris.getView().refresh();
				}
				else 
				{
					ShowPesanErrorInvMasterBarangInventaris('Gagal membaca data obat', 'Error');
				};
			}
		}
		
	)
	
}

function dataSave_viInvMasterBarangInventaris(){
	if (ValidasiSaveInvMasterBarangInventaris(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/inventaris/functionMasterBarangInventaris/save",
				params: getParamSaveInvMasterBarangInventaris(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorInvMasterBarangInventaris('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoInvMasterBarangInventaris('Berhasil menyimpan data ini','Information');
						Ext.getCmp('txtNoUrut_InvMasterBarangInventaris').setValue(cst.NoUrutBrg);
						dataGriInvMasterBarangInventaris();
						//console.log(cst);
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorInvMasterBarangInventaris('Gagal menyimpan data ini', 'Error');
						dataGriInvMasterBarangInventaris();
					};
				}
			}
			
		)
	}
}

function dataDelete_viInvMasterBarangInventaris(){
	if (ValidasiDeleteInvMasterBarangInventaris(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/inventaris/functionMasterBarangInventaris/delete",
				params: getParamDeleteInvMasterBarangInventaris(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorInvMasterBarangInventaris('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoInvMasterBarangInventaris('Berhasil menghapus data ini','Information');
						AddNewInvMasterBarangInventaris()
						dataGriInvMasterBarangInventaris();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorInvMasterBarangInventaris('Gagal menghapus data ini', 'Error');
						dataGriInvMasterBarangInventaris();
					};
				}
			}
			
		)
	}
}

function comboSatuan_InvMasterBarangInventaris(){
    var Field_Satuan = ['kd_satuan', 'satuan'];
    ds_satuanInvMasterBarangInventaris = new WebApp.DataStore({fields: Field_Satuan});
    ds_satuanInvMasterBarangInventaris.load({
        params:{
	        Skip: 0,
	        Take: 1000,
	        Sort: 'kd_satuan',
	        Sortdir: 'ASC',
	        target: 'ComboSatuanMasterBarangInventaris',
	        param: ""
        }
    });
    var cbSatuan_InvMasterBarangInventaris = new Ext.form.ComboBox({
			x:130,
			y:90,
            flex: 1,
			id: 'cboSatuan_InvMasterBarangInventaris',
			valueField: 'kd_satuan',
            displayField: 'satuan',
			store: ds_satuanInvMasterBarangInventaris,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
			width:110,
			listeners:{ 
				'select': function(a,b,c){
					selectSetUnit=b.data.displayText ;
				},
				'specialkey' : function(){
					if (Ext.EventObject.getKey() === 13) {
						
					} 						
				}
			}
        }
    )    
    return cbSatuan_InvMasterBarangInventaris;
}

function AddNewInvMasterBarangInventaris(){
	InvMasterBarangInventaris.vars.masterbarang.setValue('');
	Ext.getCmp('txtSubKelompok_InvMasterBarangInventaris').setValue('');
	Ext.getCmp('txtNoUrut_InvMasterBarangInventaris').setValue('');
	Ext.getCmp('txtNamaBarang_InvMasterBarangInventaris').setValue('');
	Ext.getCmp('cboSatuan_InvMasterBarangInventaris').setValue('');
	
};

function DataInitInvMasterBarangInventaris(rowdata){
	InvMasterBarangInventaris.vars.masterbarang.setValue(rowdata.kd_inv);
	Ext.getCmp('txtSubKelompok_InvMasterBarangInventaris').setValue(rowdata.nama_sub);
	Ext.getCmp('txtNoUrut_InvMasterBarangInventaris').setValue(rowdata.no_urut_brg);
	Ext.getCmp('txtNamaBarang_InvMasterBarangInventaris').setValue(rowdata.nama_brg);
	Ext.getCmp('cboSatuan_InvMasterBarangInventaris').setValue(rowdata.kd_satuan);
};

function getParamSaveInvMasterBarangInventaris(){
	var	params =
	{
		KdKelompok:InvMasterBarangInventaris.vars.masterbarang.getValue(),
		NoUrutBrg:Ext.getCmp('txtNoUrut_InvMasterBarangInventaris').getValue(),
		KdSatuan:Ext.getCmp('cboSatuan_InvMasterBarangInventaris').getValue(),
		NamaBrg:Ext.getCmp('txtNamaBarang_InvMasterBarangInventaris').getValue(),
		minStok: 0
	}
   
    return params
};

function getParamDeleteInvMasterBarangInventaris(){
	var	params =
	{
		NoUrutBrg:Ext.getCmp('txtNoUrut_InvMasterBarangInventaris').getValue(),
	}
   
    return params
};

function ValidasiSaveInvMasterBarangInventaris(modul,mBolHapus){
	var x = 1;
	if(InvMasterBarangInventaris.vars.masterbarang.getValue() === ''){
		loadMask.hide();
		ShowPesanWarningInvMasterBarangInventaris('Kode kelompok masih kosong', 'Warning');
		x = 0;
	} 
	return x;
};

function ValidasiDeleteInvMasterBarangInventaris(modul,mBolHapus){
	var x = 1;
	if(Ext.getCmp('txtNoUrut_InvMasterBarangInventaris').getValue() === ''){
		loadMask.hide();
		ShowPesanWarningInvMasterBarangInventaris('No urut masih kosong', 'Warning');
		x = 0;
	} 
	return x;
};



function ShowPesanWarningInvMasterBarangInventaris(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorInvMasterBarangInventaris(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoInvMasterBarangInventaris(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};