var dataSource_PembukuanInventaris_Inv;
var dataSourceNoReg_PembukuanInventaris_Inv;
var dsvPembukuanInventaris_Inv;
var selectCount_PembukuanInventaris_Inv=50;
var NamaForm_PembukuanInventaris_Inv="Pembukuan Inventaris";
var selectCountStatusPostingInvPembukuanInventaris='Semua';
var mod_name_PembukuanInventaris_Inv="PembukuanInventaris_Inv";
var now_PembukuanInventaris_Inv= new Date();
var addNew_PembukuanInventaris_Inv;
var rowSelected_PembukuanInventaris_Inv;
var setLookUps_PembukuanInventaris_Inv;
var setLookUpsNoReg_PembukuanInventaris_Inv;
var dataRegisterPembukuanInv;
var tahun = now_PembukuanInventaris_Inv.format("Y");
var tanggal = now_PembukuanInventaris_Inv.format("d/M/Y");
var jam = now_PembukuanInventaris_Inv.format("H/i/s");
var tmpkriteria;
var NoRegisterPanelTanahPembukuanInv;
var NoRegisterPanelBangunanPembukuanInv;
var NoRegisterPanelAngkutanPembukuanInv;
var NoRegisterPanelBarangLainPembukuanInv;
var KdMerkPanelAngkutanPembukuanInv;
var KdMerkPanelBarangLainPembukuanInv;
var KdSatuanPanelBarangLainPembukuanInv;
var DataGridObat;
var DataGridSubObat;
var DataGridSatuanBesar;
var DataGridSatuan;
var DataGridGolongan;

var dsvStorePanelTanah;
var dsvStorePanelBangunan;
var dsvStorePanelAngkutan;
var dsvStorePanelBarangLain;
var kodeKondisiPembukuanInv;
var dsvComboPengadaanKondisiInv;
var dsvComboPengadaanSumberDanaPembukuanInv;
var dsvComboPengadaanPerolehanPembukuanInv;
var dsvComboUnitBrgHakTanahTanahInv;
var dsvComboUnitBrgStatusTanahBangunanInv;
var dsvComboUnitBrgKategoriBangunanInv;

var listViewPembukuanInv;

var addNewPanelTanah=0;
var addNewPanelBangunan=0;
var addNewPanelAngkutan=0;
var addNewPanelBarangLain=0;

var noRegPanelTanah;
var noRegPanelBangunan;
var noRegPanelAngkutan;
var noRegPanelBarangLain;
var sendDataArray = [];
var firstGrid;
		
var myDataListView;
var arrayNoRegPembukuanInv={};
var CurrentData_PembukuanInventaris_Inv =
{
	data: Object,
	details: Array,
	row: 0
};


var InvPembukuanInventaris={};
InvPembukuanInventaris.form={};
InvPembukuanInventaris.func={};
InvPembukuanInventaris.vars={};
InvPembukuanInventaris.func.parent=InvPembukuanInventaris;
InvPembukuanInventaris.form.ArrayStore={};
InvPembukuanInventaris.form.ComboBox={};
InvPembukuanInventaris.form.DataStore={};
InvPembukuanInventaris.form.Record={};
InvPembukuanInventaris.form.Form={};
InvPembukuanInventaris.form.Grid={};
InvPembukuanInventaris.form.Panel={};
InvPembukuanInventaris.form.TextField={};
InvPembukuanInventaris.form.Button={};
InvPembukuanInventaris.form.ArrayStore.pembukuantanahinv= new Ext.data.ArrayStore({
	id: 0,
	fields:['no_register', 'nama_pengurus','alamat_pengurus','luas_tanah','alamat'],
	data: []
});
InvPembukuanInventaris.form.ArrayStore.pembukuanbangunaninv= new Ext.data.ArrayStore({
	id: 0,
	fields:[],
	data: []
});
InvPembukuanInventaris.form.ArrayStore.pembukuanangkutaninv= new Ext.data.ArrayStore({
	id: 0,
	fields:[],
	data: []
});
InvPembukuanInventaris.form.ArrayStore.pembukuanbaranglaininv= new Ext.data.ArrayStore({
	id: 0,
	fields:[],
	data: []
});
InvPembukuanInventaris.form.ArrayStore.merktipepembukuanbaranglaininv= new Ext.data.ArrayStore({
	id: 0,
	fields:[],
	data: []
});

InvPembukuanInventaris.form.ArrayStore.merktipepembukuanangkutaninv= new Ext.data.ArrayStore({
	id: 0,
	fields:[],
	data: []
});
InvPembukuanInventaris.form.ArrayStore.satuanpembukuanbaranglaininv= new Ext.data.ArrayStore({
	id: 0,
	fields:[],
	data: []
});
/*InvPembukuanInventaris.form.ArrayStore.golongan= new Ext.data.ArrayStore({
	id: 0,
	fields:['apt_kd_golongan', 'apt_golongan'],
	data: []
});

InvPembukuanInventaris.form.ArrayStore.satuan= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_satuan', 'satuan'],
	data: []
});

InvPembukuanInventaris.form.ArrayStore.satuanBesar= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_sat_besar', 'keterangan'],
	data: []
});*/


CurrentPage.page = dataGrid_PembukuanInventaris_Inv(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dataGrid_PembukuanInventaris_Inv(mod_id_PembukuanInventaris_Inv){	
 	var PanelTabInvPembukuanInventaris = new Ext.FormPanel({
        labelAlign: 'top',
		layout:'form',
        frame:true,
        title: '',
        bodyStyle:'padding:5px 5px 0',
        //width: 600,
        items: [getPenelItemSetup_PembukuanInventaris_Inv()]	
		
	})
	// Kriteria filter pada Grid # --------------
    var FrmData_PembukuanInventaris_Inv = new Ext.Panel
    (
		{
			title: NamaForm_PembukuanInventaris_Inv,
			iconCls: 'Studi_Lanjut',
			id: mod_id_PembukuanInventaris_Inv,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ PanelTabInvPembukuanInventaris],
					//GridDataView_PembukuanInventaris_Inv],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_PembukuanInventaris_Inv,
		            columns: 21,
		            defaults: {
					scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_PembukuanInventaris_Inv;
    //-------------- # End form filter # --------------
}
/*function getDataListViewPanelTanahPembukuanInv(data)
{
	myDataListView = data;
	return myDataListView;
}*/
function getlisViewPanelTanahPembukuanInv(){
		//myDataListView = sendDataArray;
		dsvStorePanelTanah = new Ext.data.ArrayStore({
		   fields: ['no_reg'],
		});
		//dataAutoCompletePanelTanahPembukuanInv
		//dsvStore.loadData(myDataListView);
	    listViewPembukuanInv = new Ext.list.ListView({
			store: dsvStorePanelTanah,
	        //multiSelect: true,
	        emptyText: 'No images to display',
	        reserveScrollOffset: true,
	
	        columns: [{
	            header: 'No Reg Barang',
	            width: .5,
	            dataIndex: 'no_reg'
	        },]
	    });
	   
	    // put it in a Panel so it looks pretty
	    var panel = new Ext.Panel({
	        id:'images-view',
	        width:225,
	        height:115,
			x: 567,
			y: 12,
	        collapsible:true,
	        layout:'fit',
	        title:'Daftar No Reg Barang',
	        items: listViewPembukuanInv
	    });
		return panel;
}
function getlisViewPanelBangunanPembukuanInv(){
		dsvStorePanelBangunan = new Ext.data.ArrayStore({
		   fields: ['no_reg'],
		});
	    listViewPembukuanInv = new Ext.list.ListView({
	        store: dsvStorePanelBangunan,
	        //multiSelect: true,
	        emptyText: 'No images to display',
	        reserveScrollOffset: true,
	
	        columns: [{
	            header: 'No Reg Barang',
	            width: .5,
	            dataIndex: 'no_reg'
	        },]
	    });
	   
	    // put it in a Panel so it looks pretty
	    var panel = new Ext.Panel({
	        id:'images-view',
	        width:225,
	        height:115,
			x: 597,
			y: 12,
	        collapsible:true,
	        layout:'fit',
	        title:'Daftar No Reg Barang',
	        items: listViewPembukuanInv
	    });
		return panel;
}
function getlisViewPanelAngkutanPembukuanInv(){
		dsvStorePanelAngkutan = new Ext.data.ArrayStore({
		   fields: ['no_reg'],
		});
	    listViewPembukuanInv = new Ext.list.ListView({
	        store: dsvStorePanelAngkutan,
	        //multiSelect: true,
	        emptyText: 'No images to display',
	        reserveScrollOffset: true,
	
	        columns: [{
	            header: 'No Reg Barang',
	            width: .5,
	            dataIndex: 'no_reg'
	        },]
	    });
	   
	    // put it in a Panel so it looks pretty
	    var panel = new Ext.Panel({
	        id:'images-view',
	        width:225,
	        height:115,
			x: 597,
			y: 12,
	        collapsible:true,
	        layout:'fit',
	        title:'Daftar No Reg Barang',
	        items: listViewPembukuanInv
	    });
		return panel;
}
function getlisViewPanelBarangLainPembukuanInv(){
		dsvStorePanelBarangLain = new Ext.data.ArrayStore({
		   fields: ['no_reg'],
		});
	    listViewPembukuanInv = new Ext.list.ListView({
	        store: dsvStorePanelBarangLain,
	        //multiSelect: true,
	        emptyText: 'No images to display',
	        reserveScrollOffset: true,
	
	        columns: [{
	            header: 'No Reg Barang',
	            width: .5,
	            dataIndex: 'no_reg'
	        },]
	    });
	   
	    // put it in a Panel so it looks pretty
	    var panel = new Ext.Panel({
	        id:'images-view',
	        width:225,
	        height:115,
			x: 567,
			y: 12,
	        collapsible:true,
	        layout:'fit',
	        title:'Daftar No Reg Barang',
	        items: listViewPembukuanInv
	    });
		return panel;
}

function setLookUp_PembukuanInventaris_Inv(panelpilihan){
    var lebar = 985;
    setLookUps_PembukuanInventaris_Inv = new Ext.Window({
        id: Nci.getId(),
        title: 'Daftar Penerimaan Inventaris', 
        closeAction: 'destroy',        
        width: 800,
        height: 350,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_PembukuanInventaris_Inv(lebar,panelpilihan),
        listeners:{
            activate: function(){
				
            },
            afterShow: function(){
                this.activate();
            },
            deactivate: function(){
                rowSelected_PembukuanInventaris_Inv=undefined;
            }
        }
    });

    setLookUps_PembukuanInventaris_Inv.show();
}
function getFormItemEntry_PembukuanInventaris_Inv(lebar,panelpilihan){
    var pnlFormDataBasic_PembukuanInventaris_Inv = new Ext.FormPanel({
		title: '',
		region: 'north',
		layout: 'form',
		bodyStyle: 'padding:10px 10px 10px 10px',
		anchor: '100%',
		labelWidth: 1,
		autoWidth: true,
		width: lebar,
		border: false,
		items:[
				getItemPanelInputPembukuan_PembukuanInventaris_Inv(lebar,panelpilihan),
				getItemGridCariPenerimaan_PembukuanInventaris_Inv(lebar,panelpilihan),
			],
			fileUpload: true,
			
		}
    )

    return pnlFormDataBasic_PembukuanInventaris_Inv;
}
function getCriteriaCariInvPembukuanInventaris(kdinv,panelpilihan)//^^^
{
	var strKriteria;
	if (panelpilihan==='tanah' || panelpilihan==='bangunan' || panelpilihan==='angkutan')
	{
		strKriteria = "LEFT(IMB.KD_INV,3)='"+kdinv+"' ";
	}
	else if(panelpilihan==='baranglain')
	{
		strKriteria = "";
		//strKriteria = "LEFT(IMB.KD_INV,3)<>'101'  AND  LEFT(IMB.KD_INV,3)<>'311'  AND  LEFT(IMB.KD_INV,3)<>'203' ";
	}
	if (Ext.get('tglAwalCariPenerimaanPembukuanInventaris_Inv').getValue() != "" && Ext.get('tglAkhirCariPenerimaanPembukuanInventaris_Inv').getValue() != "")
	{
		if (strKriteria == "")
		{
			strKriteria = " iti.tgl_terima between '" + Ext.get('tglAwalCariPenerimaanPembukuanInventaris_Inv').getValue() + "' and '" + Ext.get('tglAkhirCariPenerimaanPembukuanInventaris_Inv').getValue() + "'" ;
		}
		else {
			strKriteria += " and iti.tgl_terima between '" + Ext.get('tglAwalCariPenerimaanPembukuanInventaris_Inv').getValue() + "' and '" + Ext.get('tglAkhirCariPenerimaanPembukuanInventaris_Inv').getValue() + "'" ;
		}
	}
		strKriteria= strKriteria + "AND";
		strKriteria= strKriteria + " ITI.STATUS_POSTING = 1 AND TGL_POSTING IS NOT NULL AND ITID.SISA > 0 order by no_terima asc limit 50"
	return strKriteria;
}
function refreshPembukuanInventaris(kriteria)
{
    dataSource_PembukuanInventaris_Inv.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'ViewPembukuanInventaris',
                    param : kriteria
                }			
            }
        );   
    return dataSource_PembukuanInventaris_Inv;
}
function getItemPanelInputPembukuan_PembukuanInventaris_Inv(lebar,panelpilihan) {
    var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 50,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 10,
								xtype: 'label',
								text: 'Tanggal'
							},
							{
								x: 120,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 10,
								xtype: 'datefield',
								id: 'tglAwalCariPenerimaanPembukuanInventaris_Inv',
								name: 'tglAwalCariPenerimaanPembukuanInventaris_Inv',
								width: 200,
								format: 'd/M/Y',
								width: 120,
								tabIndex:3,
								value: now_PembukuanInventaris_Inv,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											if (panelpilihan==='tanah')
											{
												tmpkriteria = getCriteriaCariInvPembukuanInventaris(101,panelpilihan);
											}
											else if (panelpilihan==='bangunan')
											{
												tmpkriteria = getCriteriaCariInvPembukuanInventaris(311,panelpilihan);
											}
											else if (panelpilihan==='angkutan')
											{
												tmpkriteria = getCriteriaCariInvPembukuanInventaris(203,panelpilihan);
											}
											else if (panelpilihan==='baranglain')
											{
												tmpkriteria = getCriteriaCariInvPembukuanInventaris(0,panelpilihan);
											}
											refreshPembukuanInventaris(tmpkriteria);
										} 						
									}
								}
							},
							
							{
								x: 260,
								y: 10,
								xtype: 'label',
								text: 'Sampai'
							},
							{
								x: 3070,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 310,
								y: 10,
								xtype: 'datefield',
								id: 'tglAkhirCariPenerimaanPembukuanInventaris_Inv',
								name: 'tglAkhirCariPenerimaanPembukuanInventaris_Inv',
								width: 200,
								format: 'd/M/Y',
								width: 120,
								tabIndex:3,
								value: now_PembukuanInventaris_Inv,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											if (panelpilihan==='tanah')
											{
												tmpkriteria = getCriteriaCariInvPembukuanInventaris(101,panelpilihan);
											}
											else if (panelpilihan==='bangunan')
											{
												tmpkriteria = getCriteriaCariInvPembukuanInventaris(311,panelpilihan);
											}
											else if (panelpilihan==='angkutan')
											{
												tmpkriteria = getCriteriaCariInvPembukuanInventaris(203,panelpilihan);
											}
											else if (panelpilihan==='baranglain')
											{
												tmpkriteria = getCriteriaCariInvPembukuanInventaris(0,panelpilihan);
											}
											refreshPembukuanInventaris(tmpkriteria);
										} 						
									}
								}
							},
							{
								x: 450,
								y: 10,
								xtype: 'label',
								text: '*) Tekan enter untuk mencari'
							},
							
						]
					}
				]
			}
		]		
	};
    return items;
};

function getItemGridCariPenerimaan_PembukuanInventaris_Inv(lebar,panelpilihan){
    var items ={
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'margin-top: -1px;',
		border:false,
		width: lebar-80,
		height: 250,//300, 
		items:[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:[
					gridDataViewEdit_PembukuanInventaris_Inv(panelpilihan),
				]	
			}
			
		],
		
	};
    return items;
};


//var a={};
function gridDataViewEdit_PembukuanInventaris_Inv(panelpilihan){
	
    var FieldGrdKasir_PembukuanInventaris_Inv = ['no_terima','tgl_terima','no_urut_brg','nama_brg','harga_beli','jumlah_in','sisa','harga','vendor','kd_inv','satuan','nama_sub','full_name'];
    dataSource_PembukuanInventaris_Inv= new WebApp.DataStore({
        fields: FieldGrdKasir_PembukuanInventaris_Inv
    });
    
    InvPembukuanInventaris.form.Grid.a =new Ext.grid.EditorGridPanel({
        store: dataSource_PembukuanInventaris_Inv,
        height: 215,
        columnLines: false,
		autoScroll: true,
        border: true,
        sort: false,
        layout: 'fit',
        region: 'center',
		selModel: new Ext.grid.RowSelectionModel ({
            singleSelect: true,
            listeners:{
                rowselect: function(sm, row, rec){
                },
            }
        }),
        stripeRows: true,
        cm:new Ext.grid.ColumnModel([
        	new Ext.grid.RowNumberer(),	
			{
						header: 'No. Terima',
						dataIndex: 'no_terima',
						sortable: true,
						width: 100,
						
					},
					//-------------- ## --------------
					{
						header: 'Tgl Terima',
						dataIndex: 'tgl_terima',
						sortable: true,
						width: 100
					},
					{
						header: 'Sub Kelompok',
						dataIndex: 'nama_sub',
						sortable: true,
						width: 100
					},
					{
						header: 'No. Urut',
						dataIndex: 'no_urut_brg',
						sortable: true,
						width: 100
					},
					//-------------- ## --------------
					{
						header:'Barang',
						dataIndex: 'nama_brg',			
						width: 100,
						sortable: true,
					},
					//-------------- ## --------------
					{
						header: 'Harga',
						dataIndex: 'harga_beli',
						xtype: 'numbercolumn',	
						align: 'right',	
						sortable: true,
						width: 100,
						
					},
					{
						header: 'Jumlah',
						dataIndex: 'jumlah_in',
						xtype: 'numbercolumn',	
						align: 'right',	
						sortable: true,
						width: 100,
						
					},
					{
						header: 'Total',
						dataIndex: 'harga',
						xtype: 'numbercolumn',	
						align: 'right',	
						sortable: true,
						width: 100,
						//value:'ooo'
					},
					{
						header: 'Petugas',
						dataIndex: 'full_name',
						sortable: true,
						width: 100,
						//value:'ooo'
					},
        ]),
		viewConfig:{
			forceFit: true
		},
		listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					console.log(panelpilihan);
					//Ext.getCmp('btnEdit_PenerimaanInventaris_Inv').click;
					rowSelected_PembukuanInventaris_Inv = dataSource_PembukuanInventaris_Inv.getAt(ridx);
					var b=rowSelected_PembukuanInventaris_Inv.data;
					if (panelpilihan==='tanah')
					{
						InvPembukuanInventaris.vars.pembukuantanahinv.setValue("");
						NoRegisterPanelTanahPembukuanInv=undefined;
						Ext.getCmp('TxtKdKelompokUnitBarangPanelTanahPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.kd_inv);
						Ext.getCmp('TxtUrutanUnitBarangPanelTanahPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.no_urut_brg);
						Ext.getCmp('TxtKelompokPanelTanahPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.nama_sub);
						Ext.getCmp('TxtTanahPanelTanahPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.nama_brg);
						Ext.getCmp('TxtNomorTerimaPengadaanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.no_terima);
						Ext.getCmp('tglTerimaPengadaanInventaris_Inv').setValue(ShowDate(b.tgl_terima));
						Ext.getCmp('TxtDariPengadaanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.vendor);
						
						
										
						Ext.getCmp('TxtLuasTanahUnitBarangPanelTanahPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue("");
						Ext.getCmp('TxtAlamatUnitBarangPanelTanahPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue("");
						Ext.getCmp('cmbHakAtasTanahPanelTanahPembukuanInventaris_Inv').setValue("");
						Ext.getCmp('tglBukuUnitBarangPanelTanahPembukuanInventaris_Inv').setValue(tanggal);
						
						
						Ext.getCmp('TxtNoSertifikatPanelTanahPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue("");
						Ext.getCmp('tglNoSertifikatPanelTanahPembukuanInventaris_Inv').setValue(tanggal);
						Ext.getCmp('TxtNomorRegisterKlikTombolPanelTanahFilterGridDataView_PembukuanInventaris_Inv').setValue("");
						Ext.getCmp('TxtDIgunakanUntukPanelTanahPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue("");
						
						Ext.getCmp('cmbPerolehanPengadaanPanelTanahPembukuanInventaris_Inv').setValue("");
						Ext.getCmp('cmbSumberDanaPengadaanPanelTanahPembukuanInventaris_Inv').setValue("");
						
						Ext.getCmp('cmbKondisiPengadaanPanelTanahPembukuanInventaris_Inv').setValue("");
						Ext.getCmp('TxtHargaPengadaanFilterGridDataView_PembukuanInventaris_Inv').setValue("");
						Ext.getCmp('TxtPengurusPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue("");
						Ext.getCmp('TxtAlamatPengurusPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue("");
						Ext.getCmp('TxtKeteranganPengurusPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue("");
					}
					else if (panelpilihan==='bangunan')
					{
						InvPembukuanInventaris.vars.pembukuanbangunaninv.setValue("");
						NoRegisterPanelBangunanPembukuanInv=undefined;
						Ext.getCmp('TxtKdKelompokUnitBarangPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.kd_inv);
						Ext.getCmp('TxtUrutanUnitBarangPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.no_urut_brg);
						Ext.getCmp('TxtKelompokPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.nama_sub);
						Ext.getCmp('TxtBangunanPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.nama_brg);
						Ext.getCmp('TxtNomorTerimaPengadaanBangunanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.no_terima);
						Ext.getCmp('tglTerimaPengadaanBangunanInventaris_Inv').setValue(ShowDate(b.tgl_terima));
						Ext.getCmp('TxtDariPengadaanBangunanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.vendor);
						Ext.getCmp('TxtLuasLantaiUnitBarangPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue("");
						Ext.getCmp('TxtJumlahLantaiUnitBarangPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue("");
						Ext.getCmp('CekKonstBetonInventaris_Inv').setValue(false);
						Ext.getCmp('TxtLuasTanahUnitBarangPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue("");
						Ext.getCmp('TxtLetakAlamatPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue("");
						Ext.getCmp('tglBukuUnitBarangPanelBangunanPembukuanInventaris_Inv').setValue(tanggal);
						Ext.getCmp('TxtDokumenPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue("");
						//Ext.getCmp('TxtDIgunakanUntukPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue("");
						Ext.getCmp('TxtDidirikanTahunAwalPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue("");
						Ext.getCmp('TxtDidirikanTahunAkhirPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue("");
						Ext.getCmp('cmbStatusTanahUnitBarangPanelBangunanPembukuanInventaris_Inv').setValue("");
						Ext.getCmp('tglDokumenPanelBangunanPembukuanInventaris_Inv').setValue(tanggal);
						Ext.getCmp('TxtNomorRegisterUnitBarangKlikTombolPanelBangunanFilterGridDataView_PembukuanInventaris_Inv').setValue("");
						Ext.getCmp('TxtTahunDigunakanPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue("");
						Ext.getCmp('TxtNoSertifikatTanahPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue("");
						Ext.getCmp('cmbKategoriUnitBarangPanelBangunanPembukuanInventaris_Inv').setValue("");
						Ext.getCmp('cmbPerolehanPengadaanPanelBangunanPembukuanInventaris_Inv').setValue("");
						Ext.getCmp('cmbSumberDanaPengadaanPanelBangunanPembukuanInventaris_Inv').setValue("");
						Ext.getCmp('cmbKondisiPengadaanPanelBangunanPembukuanInventaris_Inv').setValue("");
						Ext.getCmp('TxtHargaPengadaanBangunanFilterGridDataView_PembukuanInventaris_Inv').setValue("");
						Ext.getCmp('TxtPengurusBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue("");
						Ext.getCmp('TxtAlamatPengurusBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue("");
						Ext.getCmp('TxtKeteranganPengurusBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue("");
					}
					else if (panelpilihan==='angkutan')
					{
						InvPembukuanInventaris.vars.pembukuanangkutaninv.setValue("");
						NoRegisterPanelAngkutanPembukuanInv=undefined;
						Ext.getCmp('TxtKdKelompokUnitBarangPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.kd_inv);
						Ext.getCmp('TxtUrutanUnitBarangPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.no_urut_brg);
						Ext.getCmp('TxtKelompokPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.nama_sub);
						Ext.getCmp('TxtNamaAlatPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.nama_brg);
						Ext.getCmp('TxtNomorTerimaPengadaanAngkutanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.no_terima);
						Ext.getCmp('tglTerimaPengadaanAngkutanInventaris_Inv').setValue(ShowDate(b.tgl_terima));
						Ext.getCmp('TxtDariPengadaanAngkutanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.vendor);
						
						
						InvPembukuanInventaris.vars.merktipepembukuanangkutaninv.setValue("");
						Ext.getCmp('TxtTahunProduksiUnitBarangPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue("");
						Ext.getCmp('TxtNoSeriUnitBarangPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue("");
						Ext.getCmp('TxtNoMesinPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue("");
						Ext.getCmp('tglBukuUnitBarangPanelAngkutanPembukuanInventaris_Inv').setValue(tanggal);
						
						Ext.getCmp('TxtDokumenPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue("");
						Ext.getCmp('TxtPabrikNegaraPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue("");
						Ext.getCmp('TxtNoRangkaPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue("");
						Ext.getCmp('tglDokumenPanelAngkutanPembukuanInventaris_Inv').setValue(tanggal);
						Ext.getCmp('TxtNomorRegisterUnitBarangKlikTombolPanelAngkutanFilterGridDataView_PembukuanInventaris_Inv').setValue("");
						Ext.getCmp('TxtCCUnitBarangPanelAngkutanFilterGridDataView_PembukuanInventaris_Inv').setValue("");
						Ext.getCmp('TxtNoPolUnitBarangPanelAngkutanFilterGridDataView_PembukuanInventaris_Inv').setValue("");
						Ext.getCmp('TxtNoBPKBUnitBarangPanelAngkutanFilterGridDataView_PembukuanInventaris_Inv').setValue("");
						
						Ext.getCmp('cmbPerolehanPengadaanPanelAngkutanPembukuanInventaris_Inv').setValue("");
						Ext.getCmp('cmbSumberDanaPengadaanPanelAngkutanPembukuanInventaris_Inv').setValue("");
						
						Ext.getCmp('cmbKondisiPengadaanPanelAngkutanPembukuanInventaris_Inv').setValue("");
						Ext.getCmp('TxtHargaPengadaanAngkutanFilterGridDataView_PembukuanInventaris_Inv').setValue("");
						Ext.getCmp('TxtPengurusAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue("");
						Ext.getCmp('TxtAlamatPengurusAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue("");
						Ext.getCmp('TxtKeteranganPengurusAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue("");
					}
						
					else if (panelpilihan==='baranglain')
					{
						InvPembukuanInventaris.vars.pembukuanbaranglaininv.setValue("");
						NoRegisterPanelBarangLainPembukuanInv=undefined;
						Ext.getCmp('TxtKdKelompokUnitBarangPanelBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.kd_inv);
						Ext.getCmp('TxtUrutanUnitBarangPanelBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.no_urut_brg);
						Ext.getCmp('TxtKelompokPanelBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.nama_sub);
						Ext.getCmp('TxtBarangPanelBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.nama_brg);
						Ext.getCmp('TxtNomorTerimaPengadaanBarangLainFilterGridDataView_PembukuanInventaris_Inv').setValue(b.no_terima);
						Ext.getCmp('tglTerimaPengadaanBarangLainInventaris_Inv').setValue(ShowDate(b.tgl_terima));
						Ext.getCmp('TxtDariPengadaanBarangLainFilterGridDataView_PembukuanInventaris_Inv').setValue(b.vendor);
						
						
						InvPembukuanInventaris.vars.merktipepembukuanbaranglaininv.setValue("");
						Ext.getCmp('TxtJumlahUnitBarangPanelBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.jumlah_in);
						Ext.getCmp('TxtSpesifikasiUnitBarangPanelBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue("");
						Ext.getCmp('tglBukuUnitBarangPanelBarangLainPembukuanInventaris_Inv').setValue(tanggal);
						

						Ext.getCmp('TxtDokumenPanelBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue("");
						InvPembukuanInventaris.vars.satuanpembukuanbaranglaininv.setValue("");
						Ext.getCmp('tglDokumenPanelBarangLainPembukuanInventaris_Inv').setValue(tanggal);
						//Ext.getCmp('TxtNomorRegisterKlikTombolPanelBarangLainFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.no_register);
						Ext.getCmp('cmbPerolehanPengadaanPanelBarangLainPembukuanInventaris_Inv').setValue("");
						Ext.getCmp('cmbSumberDanaPengadaanPanelBarangLainPembukuanInventaris_Inv').setValue("");
						Ext.getCmp('cmbKondisiPengadaanPanelBarangLainPembukuanInventaris_Inv').setValue("");
						Ext.getCmp('TxtHargaPengadaanBarangLainFilterGridDataView_PembukuanInventaris_Inv').setValue(b.harga_beli);
						Ext.getCmp('TxtPengurusBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue("");
						Ext.getCmp('TxtAlamatPengurusBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue("");
						Ext.getCmp('TxtKeteranganPengurusBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue("");
					}
					
					setLookUps_PembukuanInventaris_Inv.close();
					
				}
				// End Function # --------------
			},
    });
    return InvPembukuanInventaris.form.Grid.a;
}

//------------------------------LookUp No Reg--------------------///
function setLookUpNoReg_PembukuanInventaris_Inv(panelpilihan){
    var lebar = 985;
    setLookUpsNoReg_PembukuanInventaris_Inv = new Ext.Window({
        id: Nci.getId(),
        title: 'Nomor Reg Barang', 
        closeAction: 'destroy',        
        width: 1000,
        height: 380,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntryNoReg_PembukuanInventaris_Inv(lebar,panelpilihan),
        listeners:{
            activate: function(){
				
            },
            afterShow: function(){
                this.activate();
            },
            deactivate: function(){
                rowSelected_PembukuanInventaris_Inv=undefined;
            }
        }
    });

    setLookUpsNoReg_PembukuanInventaris_Inv.show();
}
function getFormItemEntryNoReg_PembukuanInventaris_Inv(lebar,panelpilihan){
    var pnlFormDataBasic_PembukuanInventaris_Inv = new Ext.FormPanel({
		title: '',
		region: 'north',
		layout: 'form',
		bodyStyle: 'padding:10px 10px 10px 10px',
		anchor: '100%',
		labelWidth: 1,
		autoWidth: true,
		width: lebar,
		border: false,
		items:[
				getItemPanelInputPembukuanNoReg_PembukuanInventaris_Inv(lebar,panelpilihan),
				getItemGridCariPenerimaanNoReg_PembukuanInventaris_Inv(lebar,panelpilihan),
				{
                                       xtype: 'checkbox',
                                       id: 'CekDragDropPembukuanInv',
                                       hideLabel:false,
                                       boxLabel: 'Pilih Semua',
                                       checked: false,
                                       listeners: 
                                       {
                                            check: function()
                                            {
                                               if(Ext.getCmp('CekDragDropPembukuanInv').getValue()===true)
                                                {
                                                     firstGrid.getSelectionModel().selectAll();
                                                }
                                                else
                                                {
                                                    firstGrid.getSelectionModel().clearSelections();
                                                }
                                            }
                                       }
                                    },
				{
						xtype: 'button',
						text: 'Pilih',
						iconCls: 'add',
						id: 'btnPilihNoReg_PembukuanInventaris_Inv',
						handler: function(){
							sendDataArray=[];
                            secondGridStore.each(function(record){
								
                            	var recordArray = [record.get("no_reg")];
                            	sendDataArray.push(recordArray);
															
							});
							if (panelpilihan==='tanah')
							{
								dsvStorePanelTanah.removeAll();
								dsvStorePanelTanah.loadData(sendDataArray);
							}
							else if (panelpilihan==='bangunan')
							{
								dsvStorePanelBangunan.removeAll();
								dsvStorePanelBangunan.loadData(sendDataArray);
							}
							else if (panelpilihan==='angkutan')
							{
								dsvStorePanelAngkutan.removeAll();
								dsvStorePanelAngkutan.loadData(sendDataArray);
							}
							else if (panelpilihan==='baranglain')
							{
								dsvStorePanelBarangLain.removeAll();
								dsvStorePanelBarangLain.loadData(sendDataArray);
							}
							//console.log(sendDataArray);	
								
							if (sendDataArray.length === 0)
							{
                                 ShowPesanWarningInvPembukuanInventaris('Isi daftar pilihan no reg dengan drag and drop','Laporan')
                            }else
							{
								setLookUpsNoReg_PembukuanInventaris_Inv.close();
							}
							//setLookUpsNoReg_PembukuanInventaris_Inv.close();
							//Ext.getCmp('TxtNomorRegisterKlikTombolPanelTanahFilterGridDataView_PembukuanInventaris_Inv').setValue();
						}
				},
				
			],
			fileUpload: true,
			
		}
    )

    return pnlFormDataBasic_PembukuanInventaris_Inv;
}
function getCriteriaCariNoRegInvPembukuanInventaris(noterima,nourutbrg)//^^^
{
	var strKriteria;
	strKriteria = "no_terima = '"+noterima+"' and no_urut_brg = '"+nourutbrg+"' and no_register='' and no_urut_brg not in (select no_reg from inv_inventaris where no_terima = '"+noterima+"' and no_urut_brg = '"+nourutbrg+"') ";
	return strKriteria;
}
function refreshNoRegPembukuanInventaris(kriteria)
{
    dataSource_PembukuanInventaris_Inv.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'ViewNoRegPembukuanInventaris',
                    param : kriteria
                }			
            }
        );   
    return dataSource_PembukuanInventaris_Inv;
}
function getItemPanelInputPembukuanNoReg_PembukuanInventaris_Inv(lebar,panelpilihan) {
    var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 40,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 10,
								xtype: 'label',
								text: 'Drag dan Drop untuk memilih data'
							},
							
						]
					}
				]
			}
		]		
	};
    return items;
};

function getItemGridCariPenerimaanNoReg_PembukuanInventaris_Inv(lebar,panelpilihan){
    var items ={
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'margin-top: -1px;',
		border:false,
		width: lebar-80,
		height: 219,//300, 
		items:[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:[
					gridDataViewEditNoReg_PembukuanInventaris_Inv(panelpilihan),
				]	
			}
			
		],
		
	};
    return items;
};


//var a={};
function gridDataViewEditNoReg_PembukuanInventaris_Inv(panelpilihan){
	
    var FieldGrdNoReg_PembukuanInventaris_Inv = ['no_terima','no_urut_brg','no_reg','no_register','nama_brg'];
    dataSource_PembukuanInventaris_Inv= new WebApp.DataStore({
        fields: FieldGrdNoReg_PembukuanInventaris_Inv
    });
 	var fields = [
		{name: 'no_terima', mapping : 'no_terima'},
		{name: 'no_reg', mapping : 'no_reg'}
	];
	var cols = [
		{ id : 'no_terima', header: "No. Terima", width: 160, sortable: true, dataIndex: 'no_terima'},
		{header: "No Reg Barang", width: 50, sortable: true, dataIndex: 'no_reg'}
	];
	
	 	firstGrid = new Ext.grid.GridPanel({
            ddGroup          : 'secondGridDDGroup',
            store            : dataSource_PembukuanInventaris_Inv,
            autoScroll       : true,
            columnLines      : true,
            border           : true,
            enableDragDrop   : true,
            height           : 200,
            stripeRows       : true,
            trackMouseOver   : true,
            title            : 'Daftar No Reg Barang',
            anchor           : '100% 100%',
            plugins          : [new Ext.ux.grid.FilterRow()],
            colModel         : new Ext.grid.ColumnModel
                            (
                                    [
                                            new Ext.grid.RowNumberer(),
                                            {
                                                    id: 'colNoTerima_CariNoregPembukuanInv',
                                                    header: 'No. Terima',
                                                    dataIndex: 'no_terima',
                                                    sortable: true,
                                            },
                                            {
                                                    id: 'colNoRegBrg_CariNoregPembukuanInv',
                                                    header: 'No Reg Barang',
                                                    dataIndex: 'no_reg',
                                                    sortable: true,
                                                    width: 50
                                            }
                                    ]
                                ),
   			  listeners : {
                    afterrender : function(comp) {
                    var firstGridDropTargetEl = firstGrid.getView().scroller.dom;
                    var firstGridDropTarget = new Ext.dd.DropTarget(firstGridDropTargetEl, {
                            ddGroup    : 'firstGridDDGroup',
                            notifyDrop : function(ddSource, e, data){
                                    var records =  ddSource.dragData.selections;
                                    Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
                                    firstGrid.store.add(records);
                                    firstGrid.store.sort('no_terima', 'ASC');
                                    return true
                            }
                    });
                    }
                },
             viewConfig: 
                    {
                            forceFit: true
                    }
        });

        secondGridStore = new Ext.data.JsonStore({
            fields : fields,
            root   : 'records'
        });

        // create the destination Grid
        var secondGrid = new Ext.grid.GridPanel({
                    ddGroup          : 'firstGridDDGroup',
                    store            : secondGridStore,
                    columns          : cols,
                    enableDragDrop   : true,
                    height           : 200,
                    stripeRows       : true,
                    autoExpandColumn : 'no_terima',
                    title            : 'Daftar Pilihan No Reg Barang',
                    listeners : {
                    afterrender : function(comp) {
                    var secondGridDropTargetEl = secondGrid.getView().scroller.dom;
                    var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
                            ddGroup    : 'secondGridDDGroup',
                            notifyDrop : function(ddSource, e, data){
                                    var records =  ddSource.dragData.selections;
                                    Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
                                    secondGrid.store.add(records);
                                    secondGrid.store.sort('no_terima', 'ASC');
                                    return true
                            }
                    });
                    }
                },
                viewConfig: 
                    {
                            forceFit: true
                    }
        });
		
		var FrmTabs_NoRegPembukuanInv = new Ext.Panel
        (
		{
		    id: 'formTab',
		    closable: true,
		    region: 'center',
		    layout: 'column',
            height       : 250,
			
//		    title:  'Pilih Unit',
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding: 0px 0px 0px 0px',
		    border: false,
//		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
		    anchor: '99%',
		    //iconCls: icons_viInformasiUnitdokter,
		    items: 
			[
				{
					columnWidth: .50,
					layout: 'form',
					border: false,
					autoScroll: true,
					bodyStyle: 'padding: 10px 10px 10px 10px',
					items:
					[firstGrid
						
					]
				},
				{
					columnWidth: .50,
					layout: 'form',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					anchor: '100% 100%',
					items:
					[secondGrid
						
					]
				},
			]
		});
		return FrmTabs_NoRegPembukuanInv;
    /*InvPembukuanInventaris.form.Grid.b =new Ext.grid.EditorGridPanel({
        store: dataSource_PembukuanInventaris_Inv,
        height: 215,
        columnLines: false,
		autoScroll: true,
        border: true,
        sort: false,
        layout: 'fit',
        region: 'center',
		stripeRows: true,
		selModel: new Ext.grid.RowSelectionModel ({
            singleSelect: false,
            listeners:{
                rowselect: function(sm, row, rec){
					arrayNoRegPembukuanInv['noReg']=rec.data.no_reg;
					console.log(arrayNoRegPembukuanInv['noReg']);
					
					
                },
            }
        }),
        stripeRows: true,
        cm:new Ext.grid.ColumnModel([
        	new Ext.grid.RowNumberer(),	
					{
						header: 'No. Terima',
						dataIndex: 'no_terima',
						sortable: true,
						width: 100
					},
					//-------------- ## --------------
					{
						header:'No. Reg Barang',
						dataIndex: 'no_reg',			
						width: 100,
						sortable: true,
					},
					
        ]),
		viewConfig:{
			forceFit: true
		},
		listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					/*console.log(panelpilihan);
					//Ext.getCmp('btnEdit_PenerimaanInventaris_Inv').click;
					rowSelected_PembukuanInventaris_Inv = dataSource_PembukuanInventaris_Inv.getAt(ridx);
					var b=rowSelected_PembukuanInventaris_Inv.data;
					
					
					setLookUpsNoReg_PembukuanInventaris_Inv.close();
					
				}
				// End Function # --------------
			},
    });
    return InvPembukuanInventaris.form.Grid.b;*/
}
function dsComboUnitBrgHakTanahTanahInv()
{
	dsvComboUnitBrgHakTanahTanahInv.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'vComboUnitBrgHakTanahTanahInv',
                    //param : 'kd_tarif=1'
                }			
            }
        );   
    return dsvComboUnitBrgHakTanahTanahInv;
}
function funcHakAtasTanahPanelTanahPembukuanInventaris_Inv()
{
	var Field =['id_hak_tanah','hak_tanah'];
    dsvComboUnitBrgHakTanahTanahInv = new WebApp.DataStore({fields: Field});
	dsComboUnitBrgHakTanahTanahInv();
	var funcHakAtasTanahPanelTanahPembukuanInventaris = new Ext.form.ComboBox
	(
		{
			x: 130,
			y: 200,
			id:'cmbHakAtasTanahPanelTanahPembukuanInventaris_Inv',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			/*readOnly:true,
			emptyText:'Set Tarif',
			value: 1,*/
			width: 100,
			store: dsvComboUnitBrgHakTanahTanahInv,
			valueField: 'id_hak_tanah',
			displayField: 'hak_tanah',
			//value:selectSetPilihankelompokPasien,
			listeners:
			{
				'select': function(a,b,c)
				{
				}
			}
		}
	);
	return funcHakAtasTanahPanelTanahPembukuanInventaris;
}
function dsComboPengadaanPerolehanPembukuanInv()
{
	dsvComboPengadaanPerolehanPembukuanInv.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'vComboPengadaanPerolehanPembukuanInv',
                    //param : 'kd_tarif=1'
                }			
            }
        );   
    return dsvComboPengadaanPerolehanPembukuanInv;
}
function funcPerolehanPengadaanPanelTanahPembukuanInventaris_Inv()
{
	var Field =['kd_perolehan','perolehan'];
    dsvComboPengadaanPerolehanPembukuanInv = new WebApp.DataStore({fields: Field});
	dsComboPengadaanPerolehanPembukuanInv();
	var funcPerolehanPengadaanPanelTanahPembukuanInventaris = new Ext.form.ComboBox
	(
		{
			x: 130,
			y: 40,
			id:'cmbPerolehanPengadaanPanelTanahPembukuanInventaris_Inv',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			/*readOnly:true,
			emptyText:'Set Tarif',
			value: 1,*/
			width: 255,
			store: dsvComboPengadaanPerolehanPembukuanInv,
			valueField: 'kd_perolehan',
			displayField: 'perolehan',
			//value:selectSetPilihankelompokPasien,
			listeners:
			{
				'select': function(a,b,c)
				{
				}
			}
		}
	);
	return funcPerolehanPengadaanPanelTanahPembukuanInventaris;
}
function dsComboPengadaanSumberDanaPembukuanInv()
{
	dsvComboPengadaanSumberDanaPembukuanInv.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'vComboPengadaanSumberDanaPembukuanInv',
                    //param : 'kd_tarif=1'
                }			
            }
        );   
    return dsvComboPengadaanSumberDanaPembukuanInv;
}
function funcSumberDanaPengadaanPanelTanahPembukuanInventaris_Inv()
{
	var Field =['kd_dana','sumber_dana'];
    dsvComboPengadaanSumberDanaPembukuanInv = new WebApp.DataStore({fields: Field});
	dsComboPengadaanSumberDanaPembukuanInv();
	var funcSumberDanaPengadaaPanelTanahPembukuanInventaris = new Ext.form.ComboBox
	(
		{
			x: 130,
			y: 70,
			id:'cmbSumberDanaPengadaanPanelTanahPembukuanInventaris_Inv',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			/*readOnly:true,
			emptyText:'Set Tarif',
			value: 1,*/
			width: 255,
			store: dsvComboPengadaanSumberDanaPembukuanInv,
			valueField: 'kd_dana',
			displayField: 'sumber_dana',
			//value:selectSetPilihankelompokPasien,
			listeners:
			{
				'select': function(a,b,c)
				{
				}
			}
		}
	);
	return funcSumberDanaPengadaaPanelTanahPembukuanInventaris;
}
function dsComboPengadaanKondisiInv(kode)
{
	dsvComboPengadaanKondisiInv.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'vComboPengadaanKondisiInv',
                    param : 'inv_kd_kondisi =~'+kode+'~'
                }			
            }
        );   
    return dsvComboPengadaanKondisiInv;
}
function funcKondisiPengadaanPanelTanahPembukuanInventaris_Inv()
{
	var Field =['kd_kondisi','kondisi'];
    dsvComboPengadaanKondisiInv = new WebApp.DataStore({fields: Field });
	dsComboPengadaanKondisiInv(1000);
	var funcKondisiPengadaanPanelTanahPembukuanInventaris = new Ext.form.ComboBox
	(
		{
			x: 440,
			y: 40,
			id:'cmbKondisiPengadaanPanelTanahPembukuanInventaris_Inv',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			/*readOnly:true,
			emptyText:'Set Tarif',
			value: 1,*/
			width: 290,
			store: dsvComboPengadaanKondisiInv,
			valueField: 'kd_kondisi',
			displayField: 'kondisi',
			//value:selectSetPilihankelompokPasien,
			listeners:
			{
				'select': function(a,b,c)
				{
				}
			}
		}
	);
	return funcKondisiPengadaanPanelTanahPembukuanInventaris;
}
//--->
function dsComboUnitBrgStatusTanahBangunanInv()
{
	dsvComboUnitBrgStatusTanahBangunanInv.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'vComboUnitBrgStatusTanahBangunanInv',
                    //param : 'inv_kd_kondisi =~'+kode+'~'
                }			
            }
        );   
    return dsvComboUnitBrgStatusTanahBangunanInv;
}
function funcStatusTanahUnitBarangPanelBangunanPembukuanInventaris_Inv()
{
	var field=['kd_status_tanah','status_tanah'];
    dsvComboUnitBrgStatusTanahBangunanInv = new WebApp.DataStore({fields: field});
	dsComboUnitBrgStatusTanahBangunanInv();
	var funcStatusTanahUnitBarangPanelBangunanPembukuanInventaris = new Ext.form.ComboBox
	(
		{
			x: 330,
			y: 160,
			id:'cmbStatusTanahUnitBarangPanelBangunanPembukuanInventaris_Inv',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			/*readOnly:true,
			emptyText:'Set Tarif',
			value: 1,*/
			width: 155,
			store: dsvComboUnitBrgStatusTanahBangunanInv,
			valueField: 'kd_status_tanah',
			displayField: 'status_tanah',
			//value:selectSetPilihankelompokPasien,
			listeners:
			{
				'select': function(a,b,c)
				{
				}
			}
		}
	);
	return funcStatusTanahUnitBarangPanelBangunanPembukuanInventaris;
}
function dsComboUnitBrgKategoriBangunanInv()
{
	dsvComboUnitBrgKategoriBangunanInv.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'vComboUnitBrgKategoriBangunanInv',
                    //param : 'inv_kd_kondisi =~'+kode+'~'
                }			
            }
        );   
    return dsvComboUnitBrgKategoriBangunanInv;
}
function funcKategoriUnitBarangPanelBangunanPembukuanInventaris_Inv()
{
	var field=['kd_kategori','kategori'];
    dsvComboUnitBrgKategoriBangunanInv = new WebApp.DataStore({fields: field});
	dsComboUnitBrgKategoriBangunanInv();
	var funcKategoriUnitBarangPanelBangunanPembukuanInventaris = new Ext.form.ComboBox
	(
		{
			x: 615,
			y: 190,
			id:'cmbKategoriUnitBarangPanelBangunanPembukuanInventaris_Inv',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			/*readOnly:true,
			emptyText:'Set Tarif',
			value: 1,*/
			width: 143,
			store: dsvComboUnitBrgKategoriBangunanInv,
			valueField: 'kd_kategori',
			displayField: 'kategori',
			//value:selectSetPilihankelompokPasien,
			listeners:
			{
				'select': function(a,b,c)
				{
				}
			}
		}
	);
	return funcKategoriUnitBarangPanelBangunanPembukuanInventaris;
}
function funcPerolehanPengadaanPanelBangunanPembukuanInventaris_Inv()
{
    var Field =['kd_perolehan','perolehan'];
    dsvComboPengadaanPerolehanPembukuanInv = new WebApp.DataStore({fields: Field});
	dsComboPengadaanPerolehanPembukuanInv();
	var funcPerolehanPengadaanPanelBangunanPembukuanInventaris = new Ext.form.ComboBox
	(
		{
			x: 130,
			y: 40,
			id:'cmbPerolehanPengadaanPanelBangunanPembukuanInventaris_Inv',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			/*readOnly:true,
			emptyText:'Set Tarif',
			value: 1,*/
			width: 255,
			store: dsvComboPengadaanPerolehanPembukuanInv,
			valueField: 'kd_perolehan',
			displayField: 'perolehan',
			//value:selectSetPilihankelompokPasien,
			listeners:
			{
				'select': function(a,b,c)
				{
				}
			}
		}
	);
	return funcPerolehanPengadaanPanelBangunanPembukuanInventaris;
}
function funcSumberDanaPengadaanPanelBangunanPembukuanInventaris_Inv()
{
    var Field =['kd_dana','sumber_dana'];
    dsvComboPengadaanSumberDanaPembukuanInv = new WebApp.DataStore({fields: Field});
	dsComboPengadaanSumberDanaPembukuanInv();
	var funcSumberDanaPengadaaPanelBangunanPembukuanInventaris = new Ext.form.ComboBox
	(
		{
			x: 130,
			y: 70,
			id:'cmbSumberDanaPengadaanPanelBangunanPembukuanInventaris_Inv',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			/*readOnly:true,
			emptyText:'Set Tarif',
			value: 1,*/
			width: 255,
			store: dsvComboPengadaanSumberDanaPembukuanInv,
			valueField: 'kd_dana',
			displayField: 'sumber_dana',
			//value:selectSetPilihankelompokPasien,
			listeners:
			{
				'select': function(a,b,c)
				{
				}
			}
		}
	);
	return funcSumberDanaPengadaaPanelBangunanPembukuanInventaris;
}

function funcKondisiPengadaanPanelBangunanPembukuanInventaris_Inv()
{
    var Field =['kd_kondisi','kondisi'];
    dsvComboPengadaanKondisiInv = new WebApp.DataStore({fields: Field});
	dsComboPengadaanKondisiInv(2000);
	var funcKondisiPengadaanPanelBangunanPembukuanInventaris = new Ext.form.ComboBox
	(
		{
			x: 440,
			y: 40,
			id:'cmbKondisiPengadaanPanelBangunanPembukuanInventaris_Inv',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			/*readOnly:true,
			emptyText:'Set Tarif',
			value: 1,*/
			width: 290,
			store: dsvComboPengadaanKondisiInv,
			valueField: 'kd_kondisi',
			displayField: 'kondisi',
			//value:selectSetPilihankelompokPasien,
			listeners:
			{
				'select': function(a,b,c)
				{
				}
			}
		}
	);
	return funcKondisiPengadaanPanelBangunanPembukuanInventaris;
}
//----->
function funcPerolehanPengadaanPanelAngkutanPembukuanInventaris_Inv()
{
    var Field =['kd_perolehan','perolehan'];
    dsvComboPengadaanPerolehanPembukuanInv = new WebApp.DataStore({fields: Field});
	dsComboPengadaanPerolehanPembukuanInv();
	var funcPerolehanPengadaanPanelAngkutanPembukuanInventaris = new Ext.form.ComboBox
	(
		{
			x: 130,
			y: 40,
			id:'cmbPerolehanPengadaanPanelAngkutanPembukuanInventaris_Inv',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			/*readOnly:true,
			emptyText:'Set Tarif',
			value: 1,*/
			width: 255,
			store: dsvComboPengadaanPerolehanPembukuanInv,
			valueField: 'kd_perolehan',
			displayField: 'perolehan',
			//value:selectSetPilihankelompokPasien,
			listeners:
			{
				'select': function(a,b,c)
				{
				}
			}
		}
	);
	return funcPerolehanPengadaanPanelAngkutanPembukuanInventaris;
}
function funcSumberDanaPengadaanPanelAngkutanPembukuanInventaris_Inv()
{
    var Field =['kd_dana','sumber_dana'];
    dsvComboPengadaanSumberDanaPembukuanInv = new WebApp.DataStore({fields: Field});
	dsComboPengadaanSumberDanaPembukuanInv();
	var funcSumberDanaPengadaaPanelAngkutanPembukuanInventaris = new Ext.form.ComboBox
	(
		{
			x: 130,
			y: 70,
			id:'cmbSumberDanaPengadaanPanelAngkutanPembukuanInventaris_Inv',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			/*readOnly:true,
			emptyText:'Set Tarif',
			value: 1,*/
			width: 255,
			store: dsvComboPengadaanSumberDanaPembukuanInv,
			valueField: 'kd_dana',
			displayField: 'sumber_dana',
			//value:selectSetPilihankelompokPasien,
			listeners:
			{
				'select': function(a,b,c)
				{
				}
			}
		}
	);
	return funcSumberDanaPengadaaPanelAngkutanPembukuanInventaris;
}
function funcKondisiPengadaanPanelAngkutanPembukuanInventaris_Inv()
{
    var Field =['kd_kondisi','kondisi'];
    dsvComboPengadaanKondisiInv = new WebApp.DataStore({fields: Field});
	dsComboPengadaanKondisiInv(3000);
	var funcKondisiPengadaanPanelAngkutanPembukuanInventaris = new Ext.form.ComboBox
	(
		{
			x: 440,
			y: 40,
			id:'cmbKondisiPengadaanPanelAngkutanPembukuanInventaris_Inv',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			/*readOnly:true,
			emptyText:'Set Tarif',
			value: 1,*/
			width: 290,
			store: dsvComboPengadaanKondisiInv,
			valueField: 'kd_kondisi',
			displayField: 'kondisi',
			//value:selectSetPilihankelompokPasien,
			listeners:
			{
				'select': function(a,b,c)
				{
				}
			}
		}
	);
	return funcKondisiPengadaanPanelAngkutanPembukuanInventaris;
}
//--->
function funcPerolehanPengadaanPanelBarangLainPembukuanInventaris_Inv()
{
    var Field =['kd_perolehan','perolehan'];
    dsvComboPengadaanPerolehanPembukuanInv = new WebApp.DataStore({fields: Field});
	dsComboPengadaanPerolehanPembukuanInv();
	var funcPerolehanPengadaanPanelBarangLainPembukuanInventaris = new Ext.form.ComboBox
	(
		{
			x: 130,
			y: 40,
			id:'cmbPerolehanPengadaanPanelBarangLainPembukuanInventaris_Inv',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			/*readOnly:true,
			emptyText:'Set Tarif',
			value: 1,*/
			width: 255,
			store: dsvComboPengadaanPerolehanPembukuanInv,
			valueField: 'kd_perolehan',
			displayField: 'perolehan',
			//value:selectSetPilihankelompokPasien,
			listeners:
			{
				'select': function(a,b,c)
				{
				}
			}
		}
	);
	return funcPerolehanPengadaanPanelBarangLainPembukuanInventaris;
}
function funcSumberDanaPengadaanPanelBarangLainPembukuanInventaris_Inv()
{
    var Field =['kd_dana','sumber_dana'];
    dsvComboPengadaanSumberDanaPembukuanInv = new WebApp.DataStore({fields: Field});
	dsComboPengadaanSumberDanaPembukuanInv();
	var funcSumberDanaPengadaaPanelBarangLainPembukuanInventaris = new Ext.form.ComboBox
	(
		{
			x: 130,
			y: 70,
			id:'cmbSumberDanaPengadaanPanelBarangLainPembukuanInventaris_Inv',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			/*readOnly:true,
			emptyText:'Set Tarif',
			value: 1,*/
			width: 255,
			store: dsvComboPengadaanSumberDanaPembukuanInv,
			valueField: 'kd_dana',
			displayField: 'sumber_dana',
			//value:selectSetPilihankelompokPasien,
			listeners:
			{
				'select': function(a,b,c)
				{
				}
			}
		}
	);
	return funcSumberDanaPengadaaPanelBarangLainPembukuanInventaris;
}
function funcKondisiPengadaanPanelBarangLainPembukuanInventaris_Inv()
{
    var Field =['kd_kondisi','kondisi'];
    dsvComboPengadaanKondisiInv = new WebApp.DataStore({fields: Field});
	dsComboPengadaanKondisiInv(4000);
	var funcKondisiPengadaanPanelBarangLainPembukuanInventaris = new Ext.form.ComboBox
	(
		{
			x: 440,
			y: 40,
			id:'cmbKondisiPengadaanPanelBarangLainPembukuanInventaris_Inv',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			/*readOnly:true,
			emptyText:'Set Tarif',
			value: 1,*/
			width: 290,
			store: dsvComboPengadaanKondisiInv,
			valueField: 'kd_kondisi',
			displayField: 'kondisi',
			//value:selectSetPilihankelompokPasien,
			listeners:
			{
				'select': function(a,b,c)
				{
				}
			}
		}
	);
	return funcKondisiPengadaanPanelBarangLainPembukuanInventaris;
}
function getPenelItemSetup_PembukuanInventaris_Inv(lebar)
{
    var items =
	{
		xtype:'tabpanel',
		plain:true,
		activeTab: 0,
		height:450,
		defaults: {
			bodyStyle:'padding:5px',
			autoScroll: true
	    },
	    items:[
			
				PanelTanah(),
				PanelBangunan(),
				PanelAngkutan(),
				PanelBarangLain(),
		]
	}
    return items;
};

function dataAutoCompletePanelTanahPembukuanInv(noregister){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/inventaris/functionPembukuanInv/getDataDetailAutoCompletePanelTanah",
			params: {text:noregister},
			failure: function(o)
			{
				ShowPesanErrorPembukuanInventaris_Inv('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					//dsvStore.removeAll();
					sendDataArray=[]
					for(var i=0; i<cst.listData.length; i++){
						
						sendDataArray.push([cst.listData[i].no_reg]);
						console.log(sendDataArray);
						
					}
					dsvStorePanelTanah.removeAll();
					dsvStorePanelTanah.loadData(sendDataArray);
				}
				else 
				{
					ShowPesanErrorPenerimaanInventaris_Inv('Gagal membaca data', 'Error');
				};
			}
		}
		
	)
	
}
function FieldsetUnitBarangPanelTanah()
{
	
	var items =
	{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 223,
						items:
						[
							{
								x: 10,
								y: 10,
								xtype: 'label',
								text: 'No. Register'
							},
							{
								x: 120,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							InvPembukuanInventaris.vars.pembukuantanahinv=new Nci.form.Combobox.autoComplete({
								x: 130,
								y: 10,
								width:100,
								tabIndex:2,
								store	: InvPembukuanInventaris.form.ArrayStore.pembukuantanahinv,
								select	: function(a,b,c){
										dataAutoCompletePanelTanahPembukuanInv(b.data.no_register);
										Ext.getCmp('TxtKdKelompokUnitBarangPanelTanahPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.kd_inv);
										Ext.getCmp('TxtUrutanUnitBarangPanelTanahPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.no_urut_brg);
										Ext.getCmp('TxtLuasTanahUnitBarangPanelTanahPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.luas_tanah);
										Ext.getCmp('TxtAlamatUnitBarangPanelTanahPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.alamat);
										Ext.getCmp('cmbHakAtasTanahPanelTanahPembukuanInventaris_Inv').setValue(b.data.id_hak_tanah);
										Ext.getCmp('tglBukuUnitBarangPanelTanahPembukuanInventaris_Inv').setValue(ShowDate(b.data.tgl_buku));
										Ext.getCmp('TxtKelompokPanelTanahPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.nama_sub);
										Ext.getCmp('TxtTanahPanelTanahPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.nama_brg);
										Ext.getCmp('TxtNoSertifikatPanelTanahPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.no_dokumen);
										Ext.getCmp('tglNoSertifikatPanelTanahPembukuanInventaris_Inv').setValue(ShowDate(b.data.tgl_dokumen));
										Ext.getCmp('TxtNomorRegisterKlikTombolPanelTanahFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.no_reg);
										Ext.getCmp('TxtDIgunakanUntukPanelTanahPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.penggunaan);
										Ext.getCmp('TxtNomorTerimaPengadaanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.no_terima);
										Ext.getCmp('tglTerimaPengadaanInventaris_Inv').setValue(ShowDate(b.data.tgl_terima));
										Ext.getCmp('cmbPerolehanPengadaanPanelTanahPembukuanInventaris_Inv').setValue(b.data.kd_perolehan);
										Ext.getCmp('cmbSumberDanaPengadaanPanelTanahPembukuanInventaris_Inv').setValue(b.data.kd_dana);
										Ext.getCmp('TxtDariPengadaanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.vendor);
										Ext.getCmp('cmbKondisiPengadaanPanelTanahPembukuanInventaris_Inv').setValue(b.data.kd_kondisi);
										Ext.getCmp('TxtHargaPengadaanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.hrg_buku);
										Ext.getCmp('TxtPengurusPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.pengurus);
										Ext.getCmp('TxtAlamatPengurusPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.alamat_pengurus);
										Ext.getCmp('TxtKeteranganPengurusPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.keterangan);
										NoRegisterPanelTanahPembukuanInv=b.data.no_register;
								},
								insert	: function(o){
									return {
										no_register			: o.no_register,
										no_urut_brg			: o.no_urut_brg,
										kd_perolehan 		: o.kd_perolehan,
										kd_dana 			: o.kd_dana,
										kd_kondisi 			: o.kd_kondisi,
										no_terima 			: o.no_terima,
										tgl_buku 			: o.tgl_buku,
										tgl_terima			: o.tgl_terima,
										hrg_buku 			: o.hrg_buku,
										tgl_perolehan 		: o.tgl_perolehan,
										nama_pengurus 		: o.nama_pengurus,
										alamat_pengurus 	: o.alamat_pengurus,
										no_dokumen 			: o.no_dokumen,
										tgl_dokumen 		: o.tgl_dokumen,
										luas_tanah 			: o.luas_tanah,
										alamat 				: o.alamat,
										keterangan 			: o.keterangan,
										sumber_dana 		: o.sumber_dana,
										perolehan 			: o.perolehan,
										kondisi 			: o.kondisi,
										nama_brg 			: o.nama_brg,
										kd_inv 				: o.kd_inv,
										nama_sub 			: o.nama_sub,
										satuan 				: o.satuan,
										vendor 				: o.vendor,
										hak_tanah 			: o.hak_tanah,
										id_hak_tanah 		: o.id_hak_tanah,
										penggunaan 			: o.penggunaan,
										no_reg				: o.no_reg,
										text				: '<table style="font-size: 11px;"><tr><td width="70">'+o.no_register+'</td><td width="70">'+o.no_urut_brg+'</td><td width="250">'+o.nama_brg+'</td><td width="400">'+o.alamat+'</td></tr></table>'
									}
								},
								url		: baseURL + "index.php/inventaris/functionPembukuanInv/getDataAutoKomplitTanah",
								valueField: 'no_register',
								displayField: 'text',
								listWidth: 580
							}),
							{
								x: 10,
								y: 40,
								xtype: 'label',
								text: 'Kode Kelompok'
							},
							{
								x: 120,
								y: 40,
								xtype: 'label',
								text: ':'
							},{
								x: 130,
								y: 40,
								xtype: 'textfield',
								id: 'TxtKdKelompokUnitBarangPanelTanahPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtKdKelompokUnitBarangPanelTanahPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 100,
								tabIndex:1,
								readOnly: true
							},
							{
								x: 10,
								y: 70,
								xtype: 'label',
								text: 'Urutan'
							},
							{
								x: 120,
								y: 70,
								xtype: 'label',
								text: ':'
							},{
							x: 130,
							y: 70,
							xtype: 'textfield',
							id: 'TxtUrutanUnitBarangPanelTanahPembukuanFilterGridDataView_PembukuanInventaris_Inv',
							name: 'TxtUrutanUnitBarangPanelTanahPembukuanFilterGridDataView_PembukuanInventaris_Inv',
							emptyText: '',
							width: 100,
							tabIndex:1,
							readOnly: true
							},
							{
								x: 10,
								y: 100,
								xtype: 'label',
								text: 'Luas Tanah'
							},
							{
								x: 120,
								y: 100,
								xtype: 'label',
								text: ':'
							},{
								x: 130,
								y: 100,
								xtype: 'numberfield',
								id: 'TxtLuasTanahUnitBarangPanelTanahPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtLuasTanahUnitBarangPanelTanahPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 50,
								tabIndex:1,
							},
							{
								x: 10,
								y: 130,
								xtype: 'label',
								text: 'Alamat'
							},
							{
								x: 120,
								y: 130,
								xtype: 'label',
								text: ':'
							},{
								x: 130,
								y: 130,
								xtype: 'textarea',
								id: 'TxtAlamatUnitBarangPanelTanahPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtAlamatUnitBarangPanelTanahPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 597,
								tabIndex:1,
							},
							{
								x: 10,
								y: 200,
								xtype: 'label',
								text: 'Hak Atas Tanah'
							},
							{
								x: 120,
								y: 200,
								xtype: 'label',
								text: ':'
							},
							funcHakAtasTanahPanelTanahPembukuanInventaris_Inv(),
							{
								x: 235,
								y: 10,
								xtype: 'label',
								text: 'Tgl Buku'
							},
							{
								x: 290,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 300,
								y: 10,
								xtype: 'datefield',
								id: 'tglBukuUnitBarangPanelTanahPembukuanInventaris_Inv',
								name: 'tglBukuUnitBarangPanelTanahPembukuanInventaris_Inv',
								width: 120,
								format: 'd/M/Y',
								tabIndex:3,
								value: now_PembukuanInventaris_Inv,
							},
							{
								x: 235,
								y: 40,
								xtype: 'label',
								text: 'Kelompok'
							},
							{
								x: 290,
								y: 40,
								xtype: 'label',
								text: ':'
							},{
								x: 300,
								y: 40,
								xtype: 'textfield',
								id: 'TxtKelompokPanelTanahPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtKelompokPanelTanahPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 250,
								tabIndex:1,
								readOnly: true
							},
							{
								x: 235,
								y: 70,
								xtype: 'label',
								text: 'Tanah'
							},
							{
								x: 290,
								y: 70,
								xtype: 'label',
								text: ':'
							},{
								x: 300,
								y: 70,
								xtype: 'textfield',
								id: 'TxtTanahPanelTanahPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtTanahPanelTanahPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 250,
								tabIndex:1,
								readOnly: true
							},
							{
								x: 235,
								y: 100,
								xtype: 'label',
								text: 'No Sertifikat'
							},
							{
								x: 293,
								y: 100,
								xtype: 'label',
								text: ':'
							},{
								x: 300,
								y: 100,
								xtype: 'textfield',
								id: 'TxtNoSertifikatPanelTanahPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtNoSertifikatPanelTanahPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 150,
								tabIndex:1,
							},
							{
								x: 453,
								y: 100,
								xtype: 'datefield',
								id: 'tglNoSertifikatPanelTanahPembukuanInventaris_Inv',
								name: 'tglNoSertifikatPanelTanahPembukuanInventaris_Inv',
								width: 100,
								format: 'd/M/Y',
								tabIndex:3,
								value: now_PembukuanInventaris_Inv,
							},
							{
								x: 430,
								y: 10,
								xtype: 'button',
								text: 'No Reg Barang',
								iconCls: 'find',
								tooltip: 'find',
								style:{paddingLeft:'30px'},
								width:150,
								id: 'BtnNoRegDataView_PembukuanInventaris_Inv',
								handler: function() 
								{				
									var noTerimaCariNoReg=Ext.getCmp('TxtNomorTerimaPengadaanFilterGridDataView_PembukuanInventaris_Inv').getValue();
									var noUrutBrgCariNoReg=Ext.getCmp('TxtUrutanUnitBarangPanelTanahPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue();
									
									if (noTerimaCariNoReg==='' || noUrutBrgCariNoReg==='')
									{
										ShowPesanErrorInvPembukuanInventaris('Item No terima dan Kelompok masih kosong', 'Warning');
									}
									else
									{
										tmpkriteria = getCriteriaCariNoRegInvPembukuanInventaris(noTerimaCariNoReg,noUrutBrgCariNoReg);
										NoRegPanelTanah='tanah';	
										setLookUpNoReg_PembukuanInventaris_Inv(NoRegPanelTanah);
										refreshNoRegPembukuanInventaris(tmpkriteria);
									}
								}
							},
							getlisViewPanelTanahPembukuanInv(),
							{
								x: 767,
								y: 10,
								xtype: 'textfield',
								id: 'TxtNomorRegisterKlikTombolPanelTanahFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtNomorRegisterKlikTombolPanelTanahFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 160,
								tabIndex:1,
								readOnly : true,
								hidden:true
							},
							{
								x: 235,
								y: 200,
								xtype: 'label',
								text: 'Digunakan Untuk'
							},
							{
								x: 320,
								y: 200,
								xtype: 'label',
								text: ':'
							},
							{
								x: 330,
								y: 200,
								xtype: 'textfield',
								id: 'TxtDIgunakanUntukPanelTanahPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtDIgunakanUntukPanelTanahPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 400,
								tabIndex:1,
							},
						]
					}
				]
		}
	return items;
}
function FieldsetPengadaanPanelTanah()
{
	var items =
	{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 110,
						items:
						[
							{
								x: 10,
								y: 10,
								xtype: 'label',
								text: 'Nomor Terima'
							},
							{
								x: 120,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 10,
								xtype: 'textfield',
								id: 'TxtNomorTerimaPengadaanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtNomorTerimaPengadaanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 100,
								tabIndex:1,
								readOnly: true
							},
							{
								x: 235,
								y: 10,
								xtype: 'datefield',
								id: 'tglTerimaPengadaanInventaris_Inv',
								name: 'tglTerimaPengadaanInventaris_Inv',
								width: 150,
								format: 'd/M/Y',
								readOnly: true,
								tabIndex:3,
								value: now_PembukuanInventaris_Inv,
							},
							{
								x: 10,
								y: 40,
								xtype: 'label',
								text: 'Perolehan'
							},
							{
								x: 120,
								y: 40,
								xtype: 'label',
								text: ':'
							},
							funcPerolehanPengadaanPanelTanahPembukuanInventaris_Inv(),
							{
								x: 10,
								y: 70,
								xtype: 'label',
								text: 'Sumber Dana'
							},
							{
								x: 120,
								y: 70,
								xtype: 'label',
								text: ':'
							},
							funcSumberDanaPengadaanPanelTanahPembukuanInventaris_Inv(),
							{
								x: 390,
								y: 10,
								xtype: 'label',
								text: 'Dari'
							},
							{
								x: 430,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 440,
								y: 10,
								xtype: 'textfield',
								id: 'TxtDariPengadaanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtDariPengadaanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 290,
								tabIndex:1,
								readOnly: true
							},
							{
								x: 390,
								y: 40,
								xtype: 'label',
								text: 'Kondisi'
							},
							{
								x: 430,
								y: 40,
								xtype: 'label',
								text: ':'
							},
							funcKondisiPengadaanPanelTanahPembukuanInventaris_Inv(),
							{
								x: 390,
								y: 70,
								xtype: 'label',
								text: 'Harga'
							},
							{
								x: 430,
								y: 70,
								xtype: 'label',
								text: ':'
							},
							{
								x: 440,
								y: 70,
								xtype: 'numberfield',
								style: 'text-align:right;',
								id: 'TxtHargaPengadaanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtHargaPengadaanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 200,
								tabIndex:1,	
							},
						]
					}
				]
		}
	return items;
}
function FieldsetPengurusPanelTanah()
{
	var items =
	{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 110,
						items:
						[
							{
								x: 10,
								y: 10,
								xtype: 'label',
								text: 'Pengurus'
							},
							{
								x: 120,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
							x: 130,
							y: 10,
							xtype: 'textfield',
							id: 'TxtPengurusPembukuanFilterGridDataView_PembukuanInventaris_Inv',
							name: 'TxtPengurusPembukuanFilterGridDataView_PembukuanInventaris_Inv',
							emptyText: '',
							width: 100,
							tabIndex:1,
							
							},
							{
								x: 240,
								y: 10,
								xtype: 'label',
								text: 'Alamat'
							},
							{
								x: 290,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 300,
								y: 10,
								xtype: 'textfield',
								id: 'TxtAlamatPengurusPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtAlamatPengurusPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 340,
								tabIndex:1,
							},
							{
								x: 10,
								y: 40,
								xtype: 'label',
								text: 'Keterangan'
							},
							{
								x: 120,
								y: 40,
								xtype: 'label',
								text: ':'
							},{
							x: 130,
							y: 40,
							xtype: 'textarea',
							id: 'TxtKeteranganPengurusPembukuanFilterGridDataView_PembukuanInventaris_Inv',
							name: 'TxtKeteranganPengurusPembukuanFilterGridDataView_PembukuanInventaris_Inv',
							emptyText: '',
							width: 510,
							tabIndex:1,
							
						},
						
						]
					}
				]
		}
	return items;
}

//disini ada lagi
function dataAutoCompletePanelBangunanPembukuanInv(noregister){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/inventaris/functionPembukuanInv/getDataDetailAutoCompletePanelBangunan",
			params: {text:noregister},
			failure: function(o)
			{
				ShowPesanErrorPembukuanInventaris_Inv('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					sendDataArray=[]
					for(var i=0; i<cst.listData.length; i++){
						
						sendDataArray.push([cst.listData[i].no_reg]);
						console.log(sendDataArray);
						
					}
					dsvStorePanelBangunan.removeAll();
					dsvStorePanelBangunan.loadData(sendDataArray);
					
				}
				else 
				{
					ShowPesanErrorPenerimaanInventaris_Inv('Gagal membaca data', 'Error');
				};
			}
		}
		
	)
	
}
function FieldsetUnitBarangPanelBangunan()
{
	var items =
	{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 223,
						items:
						[
							{
								x: 10,
								y: 10,
								xtype: 'label',
								text: 'No. Register'
							},
							{
								x: 120,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							InvPembukuanInventaris.vars.pembukuanbangunaninv=new Nci.form.Combobox.autoComplete({
								x: 130,
								y: 10,
								width:130,
								tabIndex:2,
								store	: InvPembukuanInventaris.form.ArrayStore.pembukuanbangunaninv,
								select	: function(a,b,c){
									console.log(b);
									dataAutoCompletePanelBangunanPembukuanInv(b.data.no_register);
									Ext.getCmp('TxtKdKelompokUnitBarangPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.kd_inv);
									Ext.getCmp('TxtUrutanUnitBarangPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.no_urut_brg);
									Ext.getCmp('TxtLuasLantaiUnitBarangPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.jml_luas);
									Ext.getCmp('TxtJumlahLantaiUnitBarangPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.jml_lantai);
									if (b.data.beton==='t')
									{
										Ext.getCmp('CekKonstBetonInventaris_Inv').setValue(true);
									}
									else
									{
										Ext.getCmp('CekKonstBetonInventaris_Inv').setValue(false);
									}
									Ext.getCmp('TxtLuasTanahUnitBarangPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.luas_tanah);
									Ext.getCmp('TxtLetakAlamatPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.alamat_tanah);
									Ext.getCmp('tglBukuUnitBarangPanelBangunanPembukuanInventaris_Inv').setValue(ShowDate(b.data.tgl_buku));
									Ext.getCmp('TxtKelompokPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.nama_sub);
									Ext.getCmp('TxtBangunanPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.nama_brg);
									Ext.getCmp('TxtDokumenPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.no_dokumen);
									//Ext.getCmp('TxtDIgunakanUntukPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.penggunaan);
									Ext.getCmp('TxtDidirikanTahunAwalPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.thn_bangun);
									Ext.getCmp('TxtDidirikanTahunAkhirPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.thn_selesai);
									Ext.getCmp('cmbStatusTanahUnitBarangPanelBangunanPembukuanInventaris_Inv').setValue(b.data.kd_status_tanah);
									Ext.getCmp('tglDokumenPanelBangunanPembukuanInventaris_Inv').setValue(ShowDate(b.data.tgl_dokumen));
									Ext.getCmp('TxtNomorRegisterUnitBarangKlikTombolPanelBangunanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.no_reg);
									Ext.getCmp('TxtTahunDigunakanPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.thn_guna);
									Ext.getCmp('TxtNoSertifikatTanahPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.no_kode_tanah);
									Ext.getCmp('cmbKategoriUnitBarangPanelBangunanPembukuanInventaris_Inv').setValue(b.data.kd_kategori);
									Ext.getCmp('TxtNomorTerimaPengadaanBangunanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.no_terima);
									Ext.getCmp('tglTerimaPengadaanBangunanInventaris_Inv').setValue(ShowDate(b.data.tgl_terima));
									Ext.getCmp('cmbPerolehanPengadaanPanelBangunanPembukuanInventaris_Inv').setValue(b.data.kd_perolehan);
									Ext.getCmp('cmbSumberDanaPengadaanPanelBangunanPembukuanInventaris_Inv').setValue(b.data.kd_dana);
									Ext.getCmp('TxtDariPengadaanBangunanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.vendor);
									Ext.getCmp('cmbKondisiPengadaanPanelBangunanPembukuanInventaris_Inv').setValue(b.data.kd_kondisi);
									Ext.getCmp('TxtHargaPengadaanBangunanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.hrg_buku);
									Ext.getCmp('TxtPengurusBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.pengurus);
									Ext.getCmp('TxtAlamatPengurusBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.alamat_pengurus);
									Ext.getCmp('TxtKeteranganPengurusBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.keterangan);
									NoRegisterPanelBangunanPembukuanInv=b.data.no_register;
								},
								insert	: function(o){
									return {
										no_register			: o.no_register,
										no_urut_brg			: o.no_urut_brg,
										kd_perolehan 		: o.kd_perolehan,
										kd_dana 			: o.kd_dana,
										kd_kondisi 			: o.kd_kondisi,
										no_terima 			: o.no_terima,
										tgl_buku 			: o.tgl_buku,
										tgl_terima			: o.tgl_terima,
										hrg_buku 			: o.hrg_buku,
										tgl_perolehan 		: o.tgl_perolehan,
										nama_pengurus 		: o.nama_pengurus,
										alamat_pengurus 	: o.alamat_pengurus,
										no_dokumen 			: o.no_dokumen,
										tgl_dokumen 		: o.tgl_dokumen,
										thn_bangun 			: o.thn_bangun,
										thn_selesai 		: o.thn_selesai,
										thn_guna 			: o.thn_guna,
										jml_lantai 			: o.jml_lantai,
										jml_luas 			: o.jml_luas,
										keterangan 			: o.keterangan,
										sumber_dana 		: o.sumber_dana,
										perolehan 			: o.perolehan,
										kondisi 			: o.kondisi,
										kategori			: o.kategori,
										nama_brg 			: o.nama_brg,
										kd_inv 				: o.kd_inv,
										nama_sub 			: o.nama_sub,
										satuan 				: o.satuan,
										vendor 				: o.vendor,
										kd_status_tanah 	: o.kd_status_tanah,
										luas_tanah 			: o.luas_tanah,
										status_tanah 		: o.status_tanah,
										beton 				: o.beton,
										no_kode_tanah 		: o.no_kode_tanah,
										alamat_tanah 		: o.alamat_tanah,
										no_reg				: o.no_reg,
										text			:  '<table style="font-size: 11px;"><tr><td width="70">'+o.no_register+'</td><td width="70">'+o.no_urut_brg+'</td><td width="250">'+o.nama_brg+'</td><td width="400">'+o.alamat_tanah+'</td></tr></table>'
									}
								},
								url		: baseURL + "index.php/inventaris/functionPembukuanInv/getDataAutoKomplitBangunan",
								valueField: 'no_register',
								displayField: 'text',
								listWidth: 580
							}),
							{
								x: 10,
								y: 40,
								xtype: 'label',
								text: 'Kode Kelompok'
							},
							{
								x: 120,
								y: 40,
								xtype: 'label',
								text: ':'
							},{
								x: 130,
								y: 40,
								xtype: 'textfield',
								id: 'TxtKdKelompokUnitBarangPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtKdKelompokUnitBarangPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 130,
								tabIndex:1,
							},
							{
								x: 10,
								y: 70,
								xtype: 'label',
								text: 'Urutan'
							},
							{
								x: 120,
								y: 70,
								xtype: 'label',
								text: ':'
							},{
							x: 130,
							y: 70,
							xtype: 'textfield',
							id: 'TxtUrutanUnitBarangPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
							name: 'TxtUrutanUnitBarangPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
							emptyText: '',
							width: 130,
							tabIndex:1,
							},
							{
								x: 10,
								y: 100,
								xtype: 'label',
								text: 'Luas Lantai'
							},
							{
								x: 120,
								y: 100,
								xtype: 'label',
								text: ':'
							},{
								x: 130,
								y: 100,
								xtype: 'textfield',
								id: 'TxtLuasLantaiUnitBarangPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtLuasLantaiUnitBarangPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 50,
								tabIndex:1,
							},
							{
								x: 10,
								y: 130,
								xtype: 'label',
								text: 'Jumlah Lantai'
							},
							{
								x: 120,
								y: 130,
								xtype: 'label',
								text: ':'
							},{
								x: 130,
								y: 130,
								xtype: 'textfield',
								id: 'TxtJumlahLantaiUnitBarangPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtJumlahLantaiUnitBarangPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 50,
								tabIndex:1,
							},
							{
							xtype: 'checkbox',
							id: 'CekKonstBetonInventaris_Inv',
							hideLabel:false,
							//checked: false,
							x: 183,
							y: 130,
							listeners: 
							{
								check: function()
								{
									
									//cek=1;
								}
							}
							},
							{
								xtype: 'tbtext',
								text: 'Konst Beton ',  
								width: 190, 
								x:203,
								y:130
							},
							{
								x: 10,
								y: 160,
								xtype: 'label',
								text: 'Luas Tanah'
							},
							{
								x: 120,
								y: 160,
								xtype: 'label',
								text: ':'
							},{
								x: 130,
								y: 160,
								xtype: 'textfield',
								id: 'TxtLuasTanahUnitBarangPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtLuasTanahUnitBarangPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 50,
								tabIndex:1,
							},
							{
								x: 183,
								y: 160,
								xtype: 'label',
								text: 'M2'
							},
							{
								x: 10,
								y: 190,
								xtype: 'label',
								text: 'Letak / Alamat'
							},
							{
								x: 120,
								y: 190,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 190,
								xtype: 'textfield',
								id: 'TxtLetakAlamatPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtLetakAlamatPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 355,
								tabIndex:1,
							},
							{
								x: 265,
								y: 10,
								xtype: 'label',
								text: 'Tgl Buku'
							},
							{
								x: 320,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 330,
								y: 10,
								xtype: 'datefield',
								id: 'tglBukuUnitBarangPanelBangunanPembukuanInventaris_Inv',
								name: 'tglBukuUnitBarangPanelBangunanPembukuanInventaris_Inv',
								width: 150,
								format: 'd/M/Y',
								tabIndex:3,
								value: now_PembukuanInventaris_Inv,
							},
							{
								x: 265,
								y: 40,
								xtype: 'label',
								text: 'Kelompok'
							},
							{
								x: 320,
								y: 40,
								xtype: 'label',
								text: ':'
							},{
								x: 330,
								y: 40,
								xtype: 'textfield',
								id: 'TxtKelompokPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtKelompokPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 250,
								tabIndex:1,
							},
							{
								x: 265,
								y: 70,
								xtype: 'label',
								text: 'Bangunan'
							},
							{
								x: 320,
								y: 70,
								xtype: 'label',
								text: ':'
							},{
								x: 330,
								y: 70,
								xtype: 'textfield',
								id: 'TxtBangunanPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtBangunanPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 250,
								tabIndex:1,
							},
							{
								x: 265,
								y: 100,
								xtype: 'label',
								text: 'Dokumen'
							},
							{
								x: 323,
								y: 100,
								xtype: 'label',
								text: ':'
							},{
								x: 330,
								y: 100,
								xtype: 'textfield',
								id: 'TxtDokumenPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtDokumenPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 150,
								tabIndex:1,
							},
							{
								x: 265,
								y: 130,
								xtype: 'label',
								text: 'Didirikan'
							},
							{
								x: 323,
								y: 130,
								xtype: 'label',
								text: ':'
							},{
								x: 330,
								y: 130,
								xtype: 'textfield',
								id: 'TxtDidirikanTahunAwalPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtDidirikanTahunAwalPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 50,
								tabIndex:1,
							},
							{
								x: 390,
								y: 130,
								xtype: 'label',
								text: 'Sampai'
							},{
								x: 435,
								y: 130,
								xtype: 'textfield',
								id: 'TxtDidirikanTahunAkhirPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtDidirikanTahunAkhirPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 50,
								tabIndex:1,
							},
							{
								x: 255,
								y: 160,
								xtype: 'label',
								text: 'Status Tanah'
							},
							{
								x: 323,
								y: 160,
								xtype: 'label',
								text: ':'
							},
							funcStatusTanahUnitBarangPanelBangunanPembukuanInventaris_Inv(),
							{
								x: 485,
								y: 100,
								xtype: 'datefield',
								id: 'tglDokumenPanelBangunanPembukuanInventaris_Inv',
								name: 'tglDokumenPanelBangunanPembukuanInventaris_Inv',
								width: 100,
								format: 'd/M/Y',
								tabIndex:3,
								value: now_PembukuanInventaris_Inv,
							},
							{
								x: 460,
								y: 10,
								xtype: 'button',
								text: 'No Reg Barang',
								iconCls: 'find',
								tooltip: 'find',
								style:{paddingLeft:'30px'},
								width:150,
								id: 'BtnNoRegBangunanDataView_PembukuanInventaris_Inv',
								handler: function() 
								{				
									var noTerimaCariNoReg=Ext.getCmp('TxtNomorTerimaPengadaanBangunanFilterGridDataView_PembukuanInventaris_Inv').getValue();
									var noUrutBrgCariNoReg=Ext.getCmp('TxtUrutanUnitBarangPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue();
									console.log(noTerimaCariNoReg);
									console.log(noUrutBrgCariNoReg);
									if (noTerimaCariNoReg==='' || noUrutBrgCariNoReg==='')
									{
										ShowPesanErrorInvPembukuanInventaris('Item No terima masih kosong', 'Warning');
									}
									else
									{
										tmpkriteria = getCriteriaCariNoRegInvPembukuanInventaris(noTerimaCariNoReg,noUrutBrgCariNoReg);
										NoRegPanelBangunan='bangunan';	
										setLookUpNoReg_PembukuanInventaris_Inv(NoRegPanelBangunan);
										refreshNoRegPembukuanInventaris(tmpkriteria);
									}
								}
							},
							getlisViewPanelBangunanPembukuanInv(),
							{
								x: 597,
								y: 10,
								xtype: 'textfield',
								id: 'TxtNomorRegisterUnitBarangKlikTombolPanelBangunanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtNomorRegisterUnitBarangKlikTombolPanelBangunanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 160,
								tabIndex:1,
								hidden: true
							},
							{
								x: 495,
								y: 130,
								xtype: 'label',
								text: 'Tahun Digunakan'
							},
							{
								x: 605,
								y: 130,
								xtype: 'label',
								text: ':'
							},{
								x: 615,
								y: 130,
								xtype: 'textfield',
								id: 'TxtTahunDigunakanPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtTahunDigunakanPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 143,
								tabIndex:1,
							},
							{
								x: 495,
								y: 160,
								xtype: 'label',
								text: 'No. Sertifikat Tanah'
							},
							{
								x: 605,
								y: 160,
								xtype: 'label',
								text: ':'
							},{
								x: 615,
								y: 160,
								xtype: 'textfield',
								id: 'TxtNoSertifikatTanahPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtNoSertifikatTanahPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 143,
								tabIndex:1,
							},
							{
								x: 495,
								y: 190,
								xtype: 'label',
								text: 'Kategori'
							},
							{
								x: 605,
								y: 190,
								xtype: 'label',
								text: ':'
							},
							funcKategoriUnitBarangPanelBangunanPembukuanInventaris_Inv()
						]
					}
				]
		}
	return items;
}
function FieldsetPengadaanPanelBangunan()
{
	var items =
	{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 110,
						items:
						[
							{
								x: 10,
								y: 10,
								xtype: 'label',
								text: 'Nomor Terima'
							},
							{
								x: 120,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 10,
								xtype: 'textfield',
								id: 'TxtNomorTerimaPengadaanBangunanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtNomorTerimaPengadaanBangunanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 100,
								tabIndex:1,
							},
							{
								x: 235,
								y: 10,
								xtype: 'datefield',
								id: 'tglTerimaPengadaanBangunanInventaris_Inv',
								name: 'tglTerimaPengadaanBangunanInventaris_Inv',
								width: 150,
								format: 'd/M/Y',
								readOnly: true,
								tabIndex:3,
								value: now_PembukuanInventaris_Inv,
							},
							{
								x: 10,
								y: 40,
								xtype: 'label',
								text: 'Perolehan'
							},
							{
								x: 120,
								y: 40,
								xtype: 'label',
								text: ':'
							},
							funcPerolehanPengadaanPanelBangunanPembukuanInventaris_Inv(),
							{
								x: 10,
								y: 70,
								xtype: 'label',
								text: 'Sumber Dana'
							},
							{
								x: 120,
								y: 70,
								xtype: 'label',
								text: ':'
							},
							funcSumberDanaPengadaanPanelBangunanPembukuanInventaris_Inv(),
							{
								x: 390,
								y: 10,
								xtype: 'label',
								text: 'Dari'
							},
							{
								x: 430,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 440,
								y: 10,
								xtype: 'textfield',
								id: 'TxtDariPengadaanBangunanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtDariPengadaanBangunanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 290,
								tabIndex:1,
							},
							{
								x: 390,
								y: 40,
								xtype: 'label',
								text: 'Kondisi'
							},
							{
								x: 430,
								y: 40,
								xtype: 'label',
								text: ':'
							},
							funcKondisiPengadaanPanelBangunanPembukuanInventaris_Inv(),
							{
								x: 390,
								y: 70,
								xtype: 'label',
								text: 'Harga'
							},
							{
								x: 430,
								y: 70,
								xtype: 'label',
								text: ':'
							},
							{
								x: 440,
								y: 70,
								xtype: 'numberfield',
								style: 'text-align:right;',
								id: 'TxtHargaPengadaanBangunanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtHargaPengadaanBangunanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 200,
								tabIndex:1,
							},
						]
					}
				]
		}
	return items;
}
function FieldsetPengurusPanelBangunan()
{
	var items =
	{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 110,
						items:
						[
							{
								x: 10,
								y: 10,
								xtype: 'label',
								text: 'Pengurus'
							},
							{
								x: 120,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
							x: 130,
							y: 10,
							xtype: 'textfield',
							id: 'TxtPengurusBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
							name: 'TxtPengurusBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
							emptyText: '',
							width: 100,
							tabIndex:1,
							
							},
							{
								x: 240,
								y: 10,
								xtype: 'label',
								text: 'Alamat'
							},
							{
								x: 290,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 300,
								y: 10,
								xtype: 'textfield',
								id: 'TxtAlamatPengurusBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtAlamatPengurusBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 340,
								tabIndex:1,
							},
							{
								x: 10,
								y: 40,
								xtype: 'label',
								text: 'Keterangan'
							},
							{
								x: 120,
								y: 40,
								xtype: 'label',
								text: ':'
							},{
							x: 130,
							y: 40,
							xtype: 'textarea',
							id: 'TxtKeteranganPengurusBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
							name: 'TxtKeteranganPengurusBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
							emptyText: '',
							width: 510,
							tabIndex:1,
							
						},
						
						]
					}
				]
		}
	return items;
}

//disini ada lagi
function dataAutoCompletePanelAngkutanPembukuanInv(noregister){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/inventaris/functionPembukuanInv/getDataDetailAutoCompletePanelAngkutan",
			params: {text:noregister},
			failure: function(o)
			{
				ShowPesanErrorPembukuanInventaris_Inv('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					sendDataArray=[]
					for(var i=0; i<cst.listData.length; i++){
						
						sendDataArray.push([cst.listData[i].no_reg]);
						console.log(sendDataArray);
						
					}
					dsvStorePanelAngkutan.removeAll();
					dsvStorePanelAngkutan.loadData(sendDataArray);
					
				}
				else 
				{
					ShowPesanErrorPenerimaanInventaris_Inv('Gagal membaca data', 'Error');
				};
			}
		}
		
	)
	
}
function FieldsetUnitBarangPanelAngkutan()
{
	var items =
	{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 223,
						items:
						[
							{
								x: 10,
								y: 10,
								xtype: 'label',
								text: 'No. Reg. Buku'
							},
							{
								x: 120,
								y: 10,
								xtype: 'label',
								text: ':'
							},
								InvPembukuanInventaris.vars.pembukuanangkutaninv=new Nci.form.Combobox.autoComplete({
								x: 130,
								y: 10,
								width:130,
								tabIndex:2,
								store	: InvPembukuanInventaris.form.ArrayStore.pembukuanangkutaninv,
								select	: function(a,b,c){
									console.log(b);
									dataAutoCompletePanelAngkutanPembukuanInv(b.data.no_register);
									Ext.getCmp('TxtKdKelompokUnitBarangPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.kd_inv);
									Ext.getCmp('TxtUrutanUnitBarangPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.no_urut_brg);
									KdMerkPanelAngkutanPembukuanInv=b.data.kd_merk;
									InvPembukuanInventaris.vars.merktipepembukuanangkutaninv.setValue(b.data.merk_type);
									Ext.getCmp('TxtTahunProduksiUnitBarangPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.thn_buat);
									Ext.getCmp('TxtNoSeriUnitBarangPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.no_seri);
									Ext.getCmp('TxtNoMesinPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.no_mesin);
									Ext.getCmp('tglBukuUnitBarangPanelAngkutanPembukuanInventaris_Inv').setValue(ShowDate(b.data.tgl_buku));
									Ext.getCmp('TxtKelompokPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.nama_sub);
									Ext.getCmp('TxtNamaAlatPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.nama_brg);
									Ext.getCmp('TxtDokumenPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.no_dokumen);
									Ext.getCmp('TxtPabrikNegaraPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.pabrik);
									Ext.getCmp('TxtNoRangkaPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.no_rangka);
									Ext.getCmp('tglDokumenPanelAngkutanPembukuanInventaris_Inv').setValue(ShowDate(b.data.tgl_dokumen));
									Ext.getCmp('TxtNomorRegisterUnitBarangKlikTombolPanelAngkutanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.no_register);
									Ext.getCmp('TxtCCUnitBarangPanelAngkutanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.cc);
									Ext.getCmp('TxtNoPolUnitBarangPanelAngkutanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.no_pol);
									Ext.getCmp('TxtNoBPKBUnitBarangPanelAngkutanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.no_stnk);
									Ext.getCmp('TxtNomorTerimaPengadaanAngkutanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.no_terima);
									Ext.getCmp('tglTerimaPengadaanAngkutanInventaris_Inv').setValue(ShowDate(b.data.tgl_terima));
									Ext.getCmp('cmbPerolehanPengadaanPanelAngkutanPembukuanInventaris_Inv').setValue(b.data.kd_perolehan);
									Ext.getCmp('cmbSumberDanaPengadaanPanelAngkutanPembukuanInventaris_Inv').setValue(b.data.kd_Dana);
									Ext.getCmp('TxtDariPengadaanAngkutanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.vendor);
									Ext.getCmp('cmbKondisiPengadaanPanelAngkutanPembukuanInventaris_Inv').setValue(b.data.kd_kondisi);
									Ext.getCmp('TxtHargaPengadaanAngkutanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.hrg_buku);
									Ext.getCmp('TxtPengurusAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.nama_pengurus);
									Ext.getCmp('TxtAlamatPengurusAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.alamat_pengurus);
									Ext.getCmp('TxtKeteranganPengurusAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.keterangan);
									NoRegisterPanelAngkutanPembukuanInv=b.data.no_register;
								},
								insert	: function(o){
									return {
										no_register			: o.no_register,
										no_urut_brg			: o.no_urut_brg,
										kd_perolehan 		: o.kd_perolehan,
										kd_dana 			: o.kd_dana,
										kd_kondisi 			: o.kd_kondisi,
										no_terima 			: o.no_terima,
										tgl_buku 			: o.tgl_buku,
										tgl_terima			: o.tgl_terima,
										hrg_buku 			: o.hrg_buku,
										tgl_perolehan 		: o.tgl_perolehan,
										nama_pengurus 		: o.nama_pengurus,
										alamat_pengurus 	: o.alamat_pengurus,
										no_dokumen 			: o.no_dokumen,
										tgl_dokumen 		: o.tgl_dokumen,
										no_rangka 			: o.no_rangka,
										no_pol 				: o.no_pol,
										pabrik 				: o.pabrik,
										no_mesin 			: o.no_mesin,
										no_stnk 			: o.no_stnk,
										no_seri 			: o.no_seri,
										cc 					: o.cc,
										thn_buat 			: o.thn_buat,
										keterangan 			: o.keterangan,
										kd_merk				: o.kd_merk,
										sumber_dana 		: o.sumber_dana,
										perolehan 			: o.perolehan,
										kondisi 			: o.kondisi,
										nama_brg 			: o.nama_brg,
										kd_inv 				: o.kd_inv,
										nama_sub 			: o.nama_sub,
										merk_type 			: o.merk_type,
										vendor 				: o.vendor,
										text			:  '<table style="font-size: 11px;"><tr><td width="100">'+o.no_register+'</td><td width="50">'+o.no_urut_brg+'</td><td width="550">'+o.nama_brg+'</td><td width="300">'+o.no_rangka+'</td></tr></table>'
									}
								},
								url		: baseURL + "index.php/inventaris/functionPembukuanInv/getDataAutoKomplitAngkutan",
								valueField: 'no_register',
								displayField: 'text',
								listWidth: 580
							}),
							{
								x: 10,
								y: 40,
								xtype: 'label',
								text: 'Kode Kelompok'
							},
							{
								x: 120,
								y: 40,
								xtype: 'label',
								text: ':'
							},{
								x: 130,
								y: 40,
								xtype: 'textfield',
								id: 'TxtKdKelompokUnitBarangPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtKdKelompokUnitBarangPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 130,
								tabIndex:1,
							},
							{
								x: 10,
								y: 70,
								xtype: 'label',
								text: 'Urutan'
							},
							{
								x: 120,
								y: 70,
								xtype: 'label',
								text: ':'
							},{
							x: 130,
							y: 70,
							xtype: 'textfield',
							id: 'TxtUrutanUnitBarangPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
							name: 'TxtUrutanUnitBarangPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
							emptyText: '',
							width: 130,
							tabIndex:1,
							},
							{
								x: 10,
								y: 100,
								xtype: 'label',
								text: 'Merk / Tipe'
							},
							{
								x: 120,
								y: 100,
								xtype: 'label',
								text: ':'
							},InvPembukuanInventaris.vars.merktipepembukuanangkutaninv=new Nci.form.Combobox.autoComplete({
								x: 130,
								y: 100,
								width:130,
								tabIndex:2,
								store	: InvPembukuanInventaris.form.ArrayStore.merktipepembukuanangkutaninv,
								select	: function(a,b,c){
									InvPembukuanInventaris.vars.merktipepembukuanangkutaninv.setValue(b.data.merk_type);
									KdMerkPanelAngkutanPembukuanInv=b.data.kd_merk;
								},
								insert	: function(o){
									return {
										kd_merk 			: o.kd_merk,
										inv_kd_merk 		: o.inv_kd_merk,
										merk_type 			: o.merk_type,
										text			:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_merk+'</td><td width="70">'+o.merk_type+'</td></tr></table>'
									}
								},
								url		: baseURL + "index.php/inventaris/functionPembukuanInv/getDataAutoKomplitMerkTipeAngkutan",
								valueField: 'kd_merk',
								displayField: 'text',
								listWidth: 580
							}),
							{
								x: 10,
								y: 130,
								xtype: 'label',
								text: 'Tahun Produksi'
							},
							{
								x: 120,
								y: 130,
								xtype: 'label',
								text: ':'
							},{
								x: 130,
								y: 130,
								xtype: 'textfield',
								id: 'TxtTahunProduksiUnitBarangPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtTahunProduksiUnitBarangPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 70,
								tabIndex:1,
							},
							{
								x: 10,
								y: 160,
								xtype: 'label',
								text: 'No. Seri'
							},
							{
								x: 120,
								y: 160,
								xtype: 'label',
								text: ':'
							},{
								x: 130,
								y: 160,
								xtype: 'textfield',
								id: 'TxtNoSeriUnitBarangPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtNoSeriUnitBarangPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 130,
								tabIndex:1,
							},
							{
								x: 10,
								y: 190,
								xtype: 'label',
								text: 'No. Mesin'
							},
							{
								x: 120,
								y: 190,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 190,
								xtype: 'textfield',
								id: 'TxtNoMesinPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtNoMesinPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 130,
								tabIndex:1,
							},
							{
								x: 265,
								y: 10,
								xtype: 'label',
								text: 'Tgl Buku'
							},
							{
								x: 320,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 330,
								y: 10,
								xtype: 'datefield',
								id: 'tglBukuUnitBarangPanelAngkutanPembukuanInventaris_Inv',
								name: 'tglBukuUnitBarangPanelAngkutanPembukuanInventaris_Inv',
								width: 150,
								format: 'd/M/Y',
								tabIndex:3,
								value: now_PembukuanInventaris_Inv,
							},
							{
								x: 265,
								y: 40,
								xtype: 'label',
								text: 'Kelompok'
							},
							{
								x: 320,
								y: 40,
								xtype: 'label',
								text: ':'
							},{
								x: 330,
								y: 40,
								xtype: 'textfield',
								id: 'TxtKelompokPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtKelompokPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 250,
								tabIndex:1,
							},
							{
								x: 265,
								y: 70,
								xtype: 'label',
								text: 'Nama Alat'
							},
							{
								x: 320,
								y: 70,
								xtype: 'label',
								text: ':'
							},{
								x: 330,
								y: 70,
								xtype: 'textfield',
								id: 'TxtNamaAlatPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtNamaAlatPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 250,
								tabIndex:1,
							},
							{
								x: 265,
								y: 100,
								xtype: 'label',
								text: 'Dokumen'
							},
							{
								x: 323,
								y: 100,
								xtype: 'label',
								text: ':'
							},{
								x: 330,
								y: 100,
								xtype: 'textfield',
								id: 'TxtDokumenPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtDokumenPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 150,
								tabIndex:1,
							},
							{
								x: 255,
								y: 130,
								xtype: 'label',
								text: 'Pabrik/Negara'
							},
							{
								x: 323,
								y: 130,
								xtype: 'label',
								text: ':'
							},{
								x: 330,
								y: 130,
								xtype: 'textfield',
								id: 'TxtPabrikNegaraPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtPabrikNegaraPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 428,
								tabIndex:1,
							},
							{
								x: 265,
								y: 160,
								xtype: 'label',
								text: 'No. Rangka'
							},
							{
								x: 323,
								y: 160,
								xtype: 'label',
								text: ':'
							},
							{
								x: 330,
								y: 160,
								xtype: 'textfield',
								id: 'TxtNoRangkaPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtNoRangkaPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 428,
								tabIndex:1,
							},{
								x: 485,
								y: 100,
								xtype: 'datefield',
								id: 'tglDokumenPanelAngkutanPembukuanInventaris_Inv',
								name: 'tglDokumenPanelAngkutanPembukuanInventaris_Inv',
								width: 100,
								format: 'd/M/Y',
								tabIndex:3,
								value: now_PembukuanInventaris_Inv,
							},
							{
								x: 460,
								y: 10,
								xtype: 'button',
								text: 'No Reg Barang',
								iconCls: 'find',
								tooltip: 'find',
								style:{paddingLeft:'30px'},
								width:150,
								id: 'BtnNoRegAngkutanDataView_PembukuanInventaris_Inv',
								handler: function() 
								{				
									var noTerimaCariNoReg=Ext.getCmp('TxtNomorTerimaPengadaanAngkutanFilterGridDataView_PembukuanInventaris_Inv').getValue();
									var noUrutBrgCariNoReg=Ext.getCmp('TxtUrutanUnitBarangPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue();
									
									if (noTerimaCariNoReg==='' || noUrutBrgCariNoReg==='')
									{
										ShowPesanErrorInvPembukuanInventaris('Item No terima dan Kelompok masih kosong', 'Warning');
									}
									else
									{
										tmpkriteria = getCriteriaCariNoRegInvPembukuanInventaris(noTerimaCariNoReg,noUrutBrgCariNoReg);
										NoRegPanelAngkutan='angkutan';	
										setLookUpNoReg_PembukuanInventaris_Inv(NoRegPanelAngkutan);
										refreshNoRegPembukuanInventaris(tmpkriteria);
									}
								}
							},
							getlisViewPanelAngkutanPembukuanInv(),
							{
								x: 597,
								y: 10,
								xtype: 'textfield',
								id: 'TxtNomorRegisterUnitBarangKlikTombolPanelAngkutanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtNomorRegisterUnitBarangKlikTombolPanelAngkutanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 160,
								tabIndex:1,
								hidden: true
							},
							{
								x: 265,
								y: 190,
								xtype: 'label',
								text: 'CC'
							},
							{
								x: 323,
								y: 190,
								xtype: 'label',
								text: ':'
							},
							{
								x: 330,
								y: 190,
								xtype: 'textfield',
								id: 'TxtCCUnitBarangPanelAngkutanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtCCUnitBarangPanelAngkutanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 50,
								tabIndex:1,
							},
							{
								x: 390,
								y: 190,
								xtype: 'label',
								text: 'No. Polisi'
							},
							{
								x: 448,
								y: 190,
								xtype: 'label',
								text: ':'
							},
							{
								x: 458,
								y: 190,
								xtype: 'textfield',
								id: 'TxtNoPolUnitBarangPanelAngkutanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtNoPolUnitBarangPanelAngkutanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 90,
								tabIndex:1,
							},
							{
								x: 558,
								y: 190,
								xtype: 'label',
								text: 'No. BPKB'
							},
							{
								x: 616,
								y: 190,
								xtype: 'label',
								text: ':'
							},
							{
								x: 626,
								y: 190,
								xtype: 'textfield',
								id: 'TxtNoBPKBUnitBarangPanelAngkutanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtNoBPKBUnitBarangPanelAngkutanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 132,
								tabIndex:1,
							},
						]
					}
				]
		}
	return items;
}
function FieldsetPengadaanPanelAngkutan()
{
	var items =
	{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 110,
						items:
						[
							{
								x: 10,
								y: 10,
								xtype: 'label',
								text: 'Nomor Terima'
							},
							{
								x: 120,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 10,
								xtype: 'textfield',
								id: 'TxtNomorTerimaPengadaanAngkutanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtNomorTerimaPengadaanAngkutanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 100,
								tabIndex:1,
							},
							{
								x: 235,
								y: 10,
								xtype: 'datefield',
								id: 'tglTerimaPengadaanAngkutanInventaris_Inv',
								name: 'tglTerimaPengadaanAngkutanInventaris_Inv',
								width: 150,
								format: 'd/M/Y',
								readOnly: true,
								tabIndex:3,
								value: now_PembukuanInventaris_Inv,
							},
							{
								x: 10,
								y: 40,
								xtype: 'label',
								text: 'Perolehan'
							},
							{
								x: 120,
								y: 40,
								xtype: 'label',
								text: ':'
							},
							funcPerolehanPengadaanPanelAngkutanPembukuanInventaris_Inv(),
							{
								x: 10,
								y: 70,
								xtype: 'label',
								text: 'Sumber Dana'
							},
							{
								x: 120,
								y: 70,
								xtype: 'label',
								text: ':'
							},
							funcSumberDanaPengadaanPanelAngkutanPembukuanInventaris_Inv(),
							{
								x: 390,
								y: 10,
								xtype: 'label',
								text: 'Dari'
							},
							{
								x: 430,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 440,
								y: 10,
								xtype: 'textfield',
								id: 'TxtDariPengadaanAngkutanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtDariPengadaanAngkutanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 290,
								tabIndex:1,
							},
							{
								x: 390,
								y: 40,
								xtype: 'label',
								text: 'Kondisi'
							},
							{
								x: 430,
								y: 40,
								xtype: 'label',
								text: ':'
							},
							funcKondisiPengadaanPanelAngkutanPembukuanInventaris_Inv(),
							{
								x: 390,
								y: 70,
								xtype: 'label',
								text: 'Harga'
							},
							{
								x: 430,
								y: 70,
								xtype: 'label',
								text: ':'
							},
							{
								x: 440,
								y: 70,
								xtype: 'numberfield',
								style: 'text-align:right;',
								id: 'TxtHargaPengadaanAngkutanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtHargaPengadaanAngkutanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 200,
								tabIndex:1,
							},
						]
					}
				]
		}
	return items;
}
function FieldsetPengurusPanelAngkutan()
{
	var items =
	{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 110,
						items:
						[
							{
								x: 10,
								y: 10,
								xtype: 'label',
								text: 'Pengurus'
							},
							{
								x: 120,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
							x: 130,
							y: 10,
							xtype: 'textfield',
							id: 'TxtPengurusAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
							name: 'TxtPengurusAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
							emptyText: '',
							width: 100,
							tabIndex:1,
							
							},
							{
								x: 240,
								y: 10,
								xtype: 'label',
								text: 'Alamat'
							},
							{
								x: 290,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 300,
								y: 10,
								xtype: 'textfield',
								id: 'TxtAlamatPengurusAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtAlamatPengurusAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 340,
								tabIndex:1,
							},
							{
								x: 10,
								y: 40,
								xtype: 'label',
								text: 'Keterangan'
							},
							{
								x: 120,
								y: 40,
								xtype: 'label',
								text: ':'
							},{
							x: 130,
							y: 40,
							xtype: 'textarea',
							id: 'TxtKeteranganPengurusAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
							name: 'TxtKeteranganPengurusAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv',
							emptyText: '',
							width: 510,
							tabIndex:1,
							
						},
						
						]
					}
				]
		}
	return items;
}
//disini ada lagi
function dataAutoCompletePanelBarangLainPembukuanInv(noregister){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/inventaris/functionPembukuanInv/getDataDetailAutoCompletePanelBarangLain",
			params: {text:noregister},
			failure: function(o)
			{
				ShowPesanErrorPembukuanInventaris_Inv('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					sendDataArray=[]
					for(var i=0; i<cst.listData.length; i++){
						
						sendDataArray.push([cst.listData[i].no_reg]);
						console.log(sendDataArray);
						
					}
					dsvStorePanelBarangLain.removeAll();
					dsvStorePanelBarangLain.loadData(sendDataArray);
					
				}
				else 
				{
					ShowPesanErrorPenerimaanInventaris_Inv('Gagal membaca data', 'Error');
				};
			}
		}
		
	)
	
}
function FieldsetUnitBarangPanelBarangLain()
{
	var items =
	{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 223,
						items:
						[
							{
								x: 10,
								y: 10,
								xtype: 'label',
								text: 'No. Register'
							},
							{
								x: 120,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							InvPembukuanInventaris.vars.pembukuanbaranglaininv=new Nci.form.Combobox.autoComplete({
								x: 130,
								y: 10,
								width:100,
								tabIndex:2,
								store	: InvPembukuanInventaris.form.ArrayStore.pembukuanbaranglaininv,
								select	: function(a,b,c){
									console.log(b);
									dataAutoCompletePanelBarangLainPembukuanInv(b.data.no_register);
									Ext.getCmp('TxtKdKelompokUnitBarangPanelBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.kd_inv);
									Ext.getCmp('TxtUrutanUnitBarangPanelBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.no_urut_brg);
									KdMerkPanelBarangLainPembukuanInv=b.data.kd_merk;
									InvPembukuanInventaris.vars.merktipepembukuanbaranglaininv.setValue(b.data.merk_type);
									Ext.getCmp('TxtJumlahUnitBarangPanelBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.jumlah);
									Ext.getCmp('TxtSpesifikasiUnitBarangPanelBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.spesifikasi);
									Ext.getCmp('tglBukuUnitBarangPanelBarangLainPembukuanInventaris_Inv').setValue(ShowDate(b.data.tgl_buku));
									Ext.getCmp('TxtKelompokPanelBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.nama_sub);
									Ext.getCmp('TxtBarangPanelBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.nama_brg);
									Ext.getCmp('TxtDokumenPanelBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.no_dokumen);
									Ext.getCmp('TxtSatuanPanelBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.satuan);
									Ext.getCmp('tglDokumenPanelBarangLainPembukuanInventaris_Inv').setValue(ShowDate(b.data.tgl_dokumen));
									//Ext.getCmp('TxtNomorRegisterKlikTombolPanelBarangLainFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.no_register);
									Ext.getCmp('TxtNomorTerimaPengadaanBarangLainFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.no_terima);
									Ext.getCmp('tglTerimaPengadaanBarangLainInventaris_Inv').setValue(ShowDate(b.data.tgl_terima));
									Ext.getCmp('cmbPerolehanPengadaanPanelBarangLainPembukuanInventaris_Inv').setValue(b.data.perolehan);
									Ext.getCmp('cmbSumberDanaPengadaanPanelBarangLainPembukuanInventaris_Inv').setValue(b.data.sumber_dana);
									Ext.getCmp('TxtDariPengadaanBarangLainFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.vendor);
									Ext.getCmp('cmbKondisiPengadaanPanelBarangLainPembukuanInventaris_Inv').setValue(b.data.kondisi);
									Ext.getCmp('TxtHargaPengadaanBarangLainFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.hrg_buku);
									Ext.getCmp('TxtPengurusBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.nama_pengurus);
									Ext.getCmp('TxtAlamatPengurusBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.alamat_pengurus);
									Ext.getCmp('TxtKeteranganPengurusBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv').setValue(b.data.keterangan);
									NoRegisterPanelBarangLainPembukuanInv=b.data.no_register;
								},
								insert	: function(o){
									return {
										no_register			: o.no_register,
										no_urut_brg			: o.no_urut_brg,
										kd_perolehan 		: o.kd_perolehan,
										kd_dana 			: o.kd_dana,
										kd_kondisi 			: o.kd_kondisi,
										no_terima 			: o.no_terima,
										tgl_buku 			: o.tgl_buku,
										tgl_terima			: o.tgl_terima,
										hrg_buku 			: o.hrg_buku,
										tgl_perolehan 		: o.tgl_perolehan,
										nama_pengurus 		: o.nama_pengurus,
										alamat_pengurus 	: o.alamat_pengurus,
										no_dokumen 			: o.no_dokumen,
										tgl_dokumen 		: o.tgl_dokumen,
										spesifikasi 		: o.spesifikasi,
										jumlah 				: o.jumlah,
										keterangan 			: o.keterangan,
										sumber_dana 		: o.sumber_dana,
										perolehan 			: o.perolehan,
										satuan 				: o.satuan,
										kondisi 			: o.kondisi,
										nama_brg 			: o.nama_brg,
										kd_inv 				: o.kd_inv,
										nama_sub 			: o.nama_sub,
										merk_type 			: o.merk_type,
										kd_merk 			: o.kd_merk,
										no_reg 				: o.no_reg,
										vendor 				: o.vendor,
										text			:  '<table style="font-size: 11px;"><tr><td width="70">'+o.no_register+'</td><td width="70">'+o.no_urut_brg+'</td><td width="250">'+o.nama_brg+'</td><td width="400">'+o.keterangan+'</td></tr></table>'
									}
								},
								url		: baseURL + "index.php/inventaris/functionPembukuanInv/getDataAutoKomplitBarangLain",
								valueField: 'no_register',
								displayField: 'text',
								listWidth: 580
							}),
							{
								x: 10,
								y: 40,
								xtype: 'label',
								text: 'Kode Kelompok'
							},
							{
								x: 120,
								y: 40,
								xtype: 'label',
								text: ':'
							},{
								x: 130,
								y: 40,
								xtype: 'textfield',
								id: 'TxtKdKelompokUnitBarangPanelBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtKdKelompokUnitBarangPanelBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 100,
								tabIndex:1,
							},
							{
								x: 10,
								y: 70,
								xtype: 'label',
								text: 'Urutan'
							},
							{
								x: 120,
								y: 70,
								xtype: 'label',
								text: ':'
							},{
							x: 130,
							y: 70,
							xtype: 'textfield',
							id: 'TxtUrutanUnitBarangPanelBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv',
							name: 'TxtUrutanUnitBarangPanelBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv',
							emptyText: '',
							width: 100,
							tabIndex:1,
							},
							{
								x: 10,
								y: 100,
								xtype: 'label',
								text: 'Merk / Tipe'
							},
							{
								x: 120,
								y: 100,
								xtype: 'label',
								text: ':'
							},InvPembukuanInventaris.vars.merktipepembukuanbaranglaininv=new Nci.form.Combobox.autoComplete({
								x: 130,
								y: 100,
								width:100,
								tabIndex:2,
								store	: InvPembukuanInventaris.form.ArrayStore.merktipepembukuanbaranglaininv,
								select	: function(a,b,c){
									InvPembukuanInventaris.vars.merktipepembukuanbaranglaininv.setValue(b.data.merk_type);
									KdMerkPanelBarangLainPembukuanInv=b.data.kd_merk;
								},
								insert	: function(o){
									return {
										kd_merk 			: o.kd_merk,
										inv_kd_merk 		: o.inv_kd_merk,
										merk_type 			: o.merk_type,
										text				:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_merk+'</td><td width="70">'+o.merk_type+'</td></tr></table>'
									}
								},
								url		: baseURL + "index.php/inventaris/functionPembukuanInv/getDataAutoKomplitMerkTipeBarangLain",
								valueField: 'kd_merk',
								displayField: 'text',
								listWidth: 580
							}),
							{
								x: 10,
								y: 130,
								xtype: 'label',
								text: 'Jumlah'
							},
							{
								x: 120,
								y: 130,
								xtype: 'label',
								text: ':'
							},{
								x: 130,
								y: 130,
								xtype: 'numberfield',
								id: 'TxtJumlahUnitBarangPanelBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtJumlahUnitBarangPanelBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 100,
								tabIndex:1,
							},
							{
								x: 10,
								y: 160,
								xtype: 'label',
								text: 'Spesifikasi'
							},
							{
								x: 120,
								y: 160,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 160,
								xtype: 'textarea',
								id: 'TxtSpesifikasiUnitBarangPanelBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtSpesifikasiUnitBarangPanelBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 597,
								tabIndex:1,
							},
							{
								x: 235,
								y: 10,
								xtype: 'label',
								text: 'Tgl Buku'
							},
							{
								x: 290,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 300,
								y: 10,
								xtype: 'datefield',
								id: 'tglBukuUnitBarangPanelBarangLainPembukuanInventaris_Inv',
								name: 'tglBukuUnitBarangPanelBarangLainPembukuanInventaris_Inv',
								width: 150,
								format: 'd/M/Y',
								tabIndex:3,
								value: now_PembukuanInventaris_Inv,
							},
							{
								x: 235,
								y: 40,
								xtype: 'label',
								text: 'Kelompok'
							},
							{
								x: 290,
								y: 40,
								xtype: 'label',
								text: ':'
							},{
								x: 300,
								y: 40,
								xtype: 'textfield',
								id: 'TxtKelompokPanelBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtKelompokPanelBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 250,
								tabIndex:1,
							},
							{
								x: 235,
								y: 70,
								xtype: 'label',
								text: 'Barang'
							},
							{
								x: 290,
								y: 70,
								xtype: 'label',
								text: ':'
							},{
								x: 300,
								y: 70,
								xtype: 'textfield',
								id: 'TxtBarangPanelBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtBarangPanelBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 250,
								tabIndex:1,
							},
							{
								x: 235,
								y: 100,
								xtype: 'label',
								text: 'Dokumen'
							},
							{
								x: 293,
								y: 100,
								xtype: 'label',
								text: ':'
							},{
								x: 300,
								y: 100,
								xtype: 'textfield',
								id: 'TxtDokumenPanelBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtDokumenPanelBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 150,
								tabIndex:1,
							},
							{
								x: 235,
								y: 130,
								xtype: 'label',
								text: 'Satuan'
							},
							{
								x: 293,
								y: 130,
								xtype: 'label',
								text: ':'
							},InvPembukuanInventaris.vars.satuanpembukuanbaranglaininv=new Nci.form.Combobox.autoComplete({
								x: 300,
								y: 130,
								width:100,
								tabIndex:2,
								store	: InvPembukuanInventaris.form.ArrayStore.satuanpembukuanbaranglaininv,
								select	: function(a,b,c){
									InvPembukuanInventaris.vars.satuanpembukuanbaranglaininv.setValue(b.data.satuan);
									KdSatuanPanelBarangLainPembukuanInv=b.data.kd_satuan;
								},
								insert	: function(o){
									return {
										kd_satuan 			: o.kd_satuan,
										satuan 				: o.satuan,
										text				:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_satuan+'</td><td width="70">'+o.satuan+'</td></tr></table>'
									}
								},
								url		: baseURL + "index.php/inventaris/functionPembukuanInv/getDataAutoKomplitSatuanBarangLain",
								valueField: 'kd_satuan',
								displayField: 'text',
								listWidth: 580
							}),
							{
								x: 453,
								y: 100,
								xtype: 'datefield',
								id: 'tglDokumenPanelBarangLainPembukuanInventaris_Inv',
								name: 'tglDokumenPanelBarangLainPembukuanInventaris_Inv',
								width: 100,
								format: 'd/M/Y',
								tabIndex:3,
								value: now_PembukuanInventaris_Inv,
							},
							{
								x: 430,
								y: 10,
								xtype: 'button',
								text: 'No Reg Barang',
								iconCls: 'find',
								tooltip: 'find',
								style:{paddingLeft:'30px'},
								width:150,
								id: 'BtnNoRegBarangLainDataView_PembukuanInventaris_Inv',
								handler: function() 
								{			
									var noTerimaCariNoReg=Ext.getCmp('TxtNomorTerimaPengadaanBarangLainFilterGridDataView_PembukuanInventaris_Inv').getValue();
									var noUrutBrgCariNoReg=Ext.getCmp('TxtUrutanUnitBarangPanelBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue();
									
									if (noTerimaCariNoReg==='' || noUrutBrgCariNoReg==='')
									{
										ShowPesanErrorInvPembukuanInventaris('Item No terima dan Kelompok masih kosong', 'Warning');
									}
									else
									{
										tmpkriteria = getCriteriaCariNoRegInvPembukuanInventaris(noTerimaCariNoReg,noUrutBrgCariNoReg);
										NoRegPanelBarangLain='baranglain';	
										setLookUpNoReg_PembukuanInventaris_Inv(NoRegPanelBarangLain);
										refreshNoRegPembukuanInventaris(tmpkriteria);
									}
								}
							},
							getlisViewPanelBarangLainPembukuanInv(),
							{
								x: 567,
								y: 10,
								xtype: 'textfield',
								id: 'TxtNomorRegisterKlikTombolPanelBarangLainFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtNomorRegisterKlikTombolPanelBarangLainFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 160,
								tabIndex:1,
								hidden: true
							},
							
						]
					}
				]
		}
	return items;
}
function FieldsetPengadaanPanelBarangLain()
{
	var items =
	{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 110,
						items:
						[
							{
								x: 10,
								y: 10,
								xtype: 'label',
								text: 'Nomor Terima'
							},
							{
								x: 120,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 10,
								xtype: 'textfield',
								id: 'TxtNomorTerimaPengadaanBarangLainFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtNomorTerimaPengadaanBarangLainFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 100,
								tabIndex:1,
							},
							{
								x: 235,
								y: 10,
								xtype: 'datefield',
								id: 'tglTerimaPengadaanBarangLainInventaris_Inv',
								name: 'tglTerimaPengadaanBarangLainInventaris_Inv',
								width: 150,
								format: 'd/M/Y',
								readOnly: true,
								tabIndex:3,
								value: now_PembukuanInventaris_Inv,
							},
							{
								x: 10,
								y: 40,
								xtype: 'label',
								text: 'Perolehan'
							},
							{
								x: 120,
								y: 40,
								xtype: 'label',
								text: ':'
							},
							funcPerolehanPengadaanPanelBarangLainPembukuanInventaris_Inv(),
							{
								x: 10,
								y: 70,
								xtype: 'label',
								text: 'Sumber Dana'
							},
							{
								x: 120,
								y: 70,
								xtype: 'label',
								text: ':'
							},
							funcSumberDanaPengadaanPanelBarangLainPembukuanInventaris_Inv(),
							{
								x: 390,
								y: 10,
								xtype: 'label',
								text: 'Dari'
							},
							{
								x: 430,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 440,
								y: 10,
								xtype: 'textfield',
								id: 'TxtDariPengadaanBarangLainFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtDariPengadaanBarangLainFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 290,
								tabIndex:1,
							},
							{
								x: 390,
								y: 40,
								xtype: 'label',
								text: 'Kondisi'
							},
							{
								x: 430,
								y: 40,
								xtype: 'label',
								text: ':'
							},
							funcKondisiPengadaanPanelBarangLainPembukuanInventaris_Inv(),
							{
								x: 390,
								y: 70,
								xtype: 'label',
								text: 'Harga'
							},
							{
								x: 430,
								y: 70,
								xtype: 'label',
								text: ':'
							},
							{
								x: 440,
								y: 70,
								xtype: 'numberfield',
								style: 'text-align:right;',
								id: 'TxtHargaPengadaanBarangLainFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtHargaPengadaanBarangLainFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 200,
								tabIndex:1,
							},
						]
					}
				]
		}
	return items;
}
function FieldsetPengurusPanelBarangLain()
{
	var items =
	{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 110,
						items:
						[
							{
								x: 10,
								y: 10,
								xtype: 'label',
								text: 'Pengurus'
							},
							{
								x: 120,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
							x: 130,
							y: 10,
							xtype: 'textfield',
							id: 'TxtPengurusBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv',
							name: 'TxtPengurusBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv',
							emptyText: '',
							width: 100,
							tabIndex:1,
							
							},
							{
								x: 240,
								y: 10,
								xtype: 'label',
								text: 'Alamat'
							},
							{
								x: 290,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 300,
								y: 10,
								xtype: 'textfield',
								id: 'TxtAlamatPengurusBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								name: 'TxtAlamatPengurusBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv',
								emptyText: '',
								width: 340,
								tabIndex:1,
							},
							{
								x: 10,
								y: 40,
								xtype: 'label',
								text: 'Keterangan'
							},
							{
								x: 120,
								y: 40,
								xtype: 'label',
								text: ':'
							},{
							x: 130,
							y: 40,
							xtype: 'textarea',
							id: 'TxtKeteranganPengurusBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv',
							name: 'TxtKeteranganPengurusBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv',
							emptyText: '',
							width: 510,
							tabIndex:1,
							
						},
						
						]
					}
				]
		}
	return items;
}
function PanelTanah(rowdata){
	var items = 
	{
		title:'Tanah',
		layout:'form',

		autoScroll: true,
		items:
		[
			{
				xtype: 'fieldset',
					title: 'Unit Barang',
					items: 
					[
						FieldsetUnitBarangPanelTanah()
					]
			},
			{
				xtype: 'fieldset',
					title: 'Pengadaan',
					items: 
					[
						FieldsetPengadaanPanelTanah()
					]
			},
			{
				xtype: 'fieldset',
					title: 'Pengurus',
					items: 
					[
						FieldsetPengurusPanelTanah()
					]
			},
			
		],
		tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAddPanelTanah_PembukuanInventaris_Inv',
						handler: function(){
							dataaddnewpaneltanah_PembukuanInventaris_Inv();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpanPanelTanah_PembukuanInventaris_Inv',
						handler: function()
						{
							datasave_PembukuanInventaris_Inv("tanah");
						}
					}
				]
			}
    };
        return items;
}
function dataaddnewpaneltanah_PembukuanInventaris_Inv(){
	addNewPanelTanah='tanah';
	setLookUp_PembukuanInventaris_Inv(addNewPanelTanah);
}
function PanelBangunan(rowdata){
	var items = 
	{
		title:'Bangunan',
		layout:'form',
		autoScroll: true,
		
		items:
		[
			{
				xtype: 'fieldset',
					title: 'Unit Barang',
					items: 
					[
						FieldsetUnitBarangPanelBangunan()
					]
			},
			{
				xtype: 'fieldset',
					title: 'Pengadaan',
					items: 
					[
						FieldsetPengadaanPanelBangunan()
					]
			},
			{
				xtype: 'fieldset',
					title: 'Pengurus',
					items: 
					[
						FieldsetPengurusPanelBangunan()
					]
			},
			
		],
		tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAddPanelBangunan_PembukuanInventaris_Inv',
						handler: function(){
							dataaddnewpanelbangunan_PembukuanInventaris_Inv();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpanPanelBangunan_PembukuanInventaris_Inv',
						handler: function()
						{
							datasave_PembukuanInventaris_Inv("bangunan");
						}
					}
				]
			}
    };
        return items;
}
function dataaddnewpanelbangunan_PembukuanInventaris_Inv(){
	addNewPanelBangunan='bangunan';
	setLookUp_PembukuanInventaris_Inv(addNewPanelBangunan);
}
function PanelAngkutan(rowdata){
	var items = 
	{
		title:'Angkutan',
		layout:'form',
		autoScroll: true,
		
		items:
		[
			{
				xtype: 'fieldset',
					title: 'Unit Barang',
					items: 
					[
						FieldsetUnitBarangPanelAngkutan()
					]
			},
			{
				xtype: 'fieldset',
					title: 'Pengadaan',
					items: 
					[
						FieldsetPengadaanPanelAngkutan()
					]
			},
			{
				xtype: 'fieldset',
					title: 'Pengurus',
					items: 
					[
						FieldsetPengurusPanelAngkutan()
					]
			},
			
		],
		tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAddPanelTAngkutan_PembukuanInventaris_Inv',
						handler: function(){
							dataaddnewpanelangkutan_PembukuanInventaris_Inv();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpanPanelAngkutan_PembukuanInventaris_Inv',
						handler: function()
						{
							datasave_PembukuanInventaris_Inv("angkutan");
						}
					}
				]
			}
    };
        return items;
}
function dataaddnewpanelangkutan_PembukuanInventaris_Inv(){
	addNewPanelAngkutan='angkutan';
	setLookUp_PembukuanInventaris_Inv(addNewPanelAngkutan);
}
function PanelBarangLain(rowdata){
	var items = 
	{
		title:'Barang Lain',
		layout:'form',
		autoScroll: true,
		
		items:
		[
			{
				xtype: 'fieldset',
					title: 'Unit Barang',
					items: 
					[
						FieldsetUnitBarangPanelBarangLain()
					]
			},
			{
				xtype: 'fieldset',
					title: 'Pengadaan',
					items: 
					[
						FieldsetPengadaanPanelBarangLain()
					]
			},
			{
				xtype: 'fieldset',
					title: 'Pengurus',
					items: 
					[
						FieldsetPengurusPanelBarangLain()
					]
			},
			
		],
		tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAddPanelBarangLain_PembukuanInventaris_Inv',
						handler: function(){
							dataaddnewpanelbaranglain_PembukuanInventaris_Inv();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpanPanelBarangLain_PembukuanInventaris_Inv',
						handler: function()
						{
							datasave_PembukuanInventaris_Inv("baranglain");
						}
					}
				]
			}
    };
        return items;
}
function dataaddnewpanelbaranglain_PembukuanInventaris_Inv(){
	addNewPanelBarangLain='baranglain';
	setLookUp_PembukuanInventaris_Inv(addNewPanelBarangLain);
}

function datasave_PembukuanInventaris_Inv(panel){
	if (ValidasiEntryPembukuanInv(nmHeaderSimpanData,false,panel) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/inventaris/functionPembukuanInv/save",
				params: getParamPembukuanInv(panel),
				failure: function(o)
				{
					ShowPesanErrorInvPembukuanInventaris('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoInvPembukuanInventaris(nmPesanSimpanSukses,nmHeaderSimpanData);
						
						if (panel==='tanah')
						{
							NoRegisterPanelTanahPembukuanInv=undefined;
							dataAutoCompletePanelTanahPembukuanInv(cst.noregister);
							InvPembukuanInventaris.vars.pembukuantanahinv.setValue(cst.noregister);
						}
						else if (panel==='bangunan')
						{
							NoRegisterPanelBangunanPembukuanInv=undefined;
							dataAutoCompletePanelBangunanPembukuanInv(cst.noregister);
							InvPembukuanInventaris.vars.pembukuanbangunaninv.setValue(cst.noregister);
						}
						else if (panel==='angkutan')
						{
							NoRegisterPanelAngkutanPembukuanInv=undefined;
							dataAutoCompletePanelAngkutanPembukuanInv(cst.noregister);
							InvPembukuanInventaris.vars.pembukuanangkutaninv.setValue(cst.noregister);
						}
						else if (panel==='baranglain')
						{
							NoRegisterPanelBarangLainPembukuanInv=undefined;
							dataAutoCompletePanelBarangLainPembukuanInv(cst.noregister);
							InvPembukuanInventaris.vars.pembukuanbaranglaininv.setValue(cst.noregister);
						}
					}
					else 
					{
						if (cst.sisa!=0)
						{
							ShowPesanErrorInvPembukuanInventaris('Gagal Menyimpan Data ini', 'Error');
						}
						else
						{
							ShowPesanWarningInvPembukuanInventaris('Data ini telah terbukukan semua','Perhatian');
						}
					};
				}
			}
			
		)
	}
}

function getParamPembukuanInv(panel){
	if (panel==='tanah')
	{
		var hasilNoRegisterPanelTanahPembukuanInv
		console.log(hasilNoRegisterPanelTanahPembukuanInv);
		if (NoRegisterPanelTanahPembukuanInv!=undefined)
		{
			hasilNoRegisterPanelTanahPembukuanInv = NoRegisterPanelTanahPembukuanInv;
		}
		else
		{
			hasilNoRegisterPanelTanahPembukuanInv = tahun;
			console.log(hasilNoRegisterPanelTanahPembukuanInv);
		}	
		
		var	params =
		{
			Panel:panel,
			NoRegister:hasilNoRegisterPanelTanahPembukuanInv,
			jumlahrecord:dsvStorePanelTanah.getCount(),
			KdKelompok:Ext.getCmp('TxtKdKelompokUnitBarangPanelTanahPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			Urutan:Ext.getCmp('TxtUrutanUnitBarangPanelTanahPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			Kelompok:Ext.getCmp('TxtKelompokPanelTanahPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			Tanah:Ext.getCmp('TxtTanahPanelTanahPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			NoTerima:Ext.getCmp('TxtNomorTerimaPengadaanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			TglTerima:Ext.getCmp('tglTerimaPengadaanInventaris_Inv').getValue(),
			Vendor:Ext.getCmp('TxtDariPengadaanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			LuasTanah:Ext.getCmp('TxtLuasTanahUnitBarangPanelTanahPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			Alamat:Ext.getCmp('TxtAlamatUnitBarangPanelTanahPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			HakTanah:Ext.getCmp('cmbHakAtasTanahPanelTanahPembukuanInventaris_Inv').getValue(),
			TglBuku:Ext.getCmp('tglBukuUnitBarangPanelTanahPembukuanInventaris_Inv').getValue(),
			NoSertifikat:Ext.getCmp('TxtNoSertifikatPanelTanahPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			TglSertifikat:Ext.getCmp('tglNoSertifikatPanelTanahPembukuanInventaris_Inv').getValue(),
			DigunakanUntuk:Ext.getCmp('TxtDIgunakanUntukPanelTanahPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			Perolehan:Ext.getCmp('cmbPerolehanPengadaanPanelTanahPembukuanInventaris_Inv').getValue(),
			TglPerolehan:Ext.getCmp('tglTerimaPengadaanInventaris_Inv').getValue(),
			SumberDana:Ext.getCmp('cmbSumberDanaPengadaanPanelTanahPembukuanInventaris_Inv').getValue(),
			Kondisi:Ext.getCmp('cmbKondisiPengadaanPanelTanahPembukuanInventaris_Inv').getValue(),
			Harga:Ext.getCmp('TxtHargaPengadaanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			Pengurus:Ext.getCmp('TxtPengurusPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			AlamatPengurus:Ext.getCmp('TxtAlamatPengurusPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			Keterangan:Ext.getCmp('TxtKeteranganPengurusPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
		}
		for(var i = 0 ; i < dsvStorePanelTanah.getCount();i++)
		{
			params['NoRegBarang-'+i]=dsvStorePanelTanah.getRange()[i].data.no_reg;
		}
		
	}
	else if (panel==='bangunan')
	{
		var hasilNoRegisterPanelBangunanPembukuanInv
		if (NoRegisterPanelBangunanPembukuanInv!=undefined)
		{
			hasilNoRegisterPanelBangunanPembukuanInv = NoRegisterPanelBangunanPembukuanInv;
		}
		else
		{
			hasilNoRegisterPanelBangunanPembukuanInv = tahun;
		}	
		
		var konstBeton;
		if (Ext.getCmp('CekKonstBetonInventaris_Inv').getValue(true))
			konstBeton="t";
		else
			konstBeton="f";
			
		var	params =
		{
			Panel:panel,
			NoRegister:hasilNoRegisterPanelBangunanPembukuanInv,
			jumlahrecord:dsvStorePanelBangunan.getCount(),
			KdKelompok:Ext.getCmp('TxtKdKelompokUnitBarangPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			Urutan:Ext.getCmp('TxtUrutanUnitBarangPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			LuasLantai:Ext.getCmp('TxtLuasLantaiUnitBarangPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			JumlahLantai:Ext.getCmp('TxtJumlahLantaiUnitBarangPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			KonstBeton:konstBeton,
			LuasTanah:Ext.getCmp('TxtLuasTanahUnitBarangPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			Alamat:Ext.getCmp('TxtLetakAlamatPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			TglBuku:Ext.getCmp('tglBukuUnitBarangPanelBangunanPembukuanInventaris_Inv').getValue(),
			Kelompok:Ext.getCmp('TxtKelompokPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			Bangunan:Ext.getCmp('TxtBangunanPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			Dokumen:Ext.getCmp('TxtDokumenPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			TglDokumen:Ext.getCmp('tglDokumenPanelBangunanPembukuanInventaris_Inv').getValue(),
			ThnAwal:Ext.getCmp('TxtDidirikanTahunAwalPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			ThnAkhir:Ext.getCmp('TxtDidirikanTahunAkhirPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			StatusTanah:Ext.getCmp('cmbStatusTanahUnitBarangPanelBangunanPembukuanInventaris_Inv').getValue(),
			ThnGuna:Ext.getCmp('TxtTahunDigunakanPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			NoSertifikat:Ext.getCmp('TxtNoSertifikatTanahPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			Kategori:Ext.getCmp('cmbKategoriUnitBarangPanelBangunanPembukuanInventaris_Inv').getValue(),
			NoTerima:Ext.getCmp('TxtNomorTerimaPengadaanBangunanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			TglTerima:Ext.getCmp('tglTerimaPengadaanBangunanInventaris_Inv').getValue(),
			Vendor:Ext.getCmp('TxtDariPengadaanBangunanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			Perolehan:Ext.getCmp('cmbPerolehanPengadaanPanelBangunanPembukuanInventaris_Inv').getValue(),
			TglPerolehan:Ext.getCmp('tglTerimaPengadaanBangunanInventaris_Inv').getValue(),
			SumberDana:Ext.getCmp('cmbSumberDanaPengadaanPanelBangunanPembukuanInventaris_Inv').getValue(),
			Kondisi:Ext.getCmp('cmbKondisiPengadaanPanelBangunanPembukuanInventaris_Inv').getValue(),
			Harga:Ext.getCmp('TxtHargaPengadaanBangunanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			Pengurus:Ext.getCmp('TxtPengurusBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			AlamatPengurus:Ext.getCmp('TxtAlamatPengurusBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			Keterangan:Ext.getCmp('TxtKeteranganPengurusBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
		}
		for(var i = 0 ; i < dsvStorePanelBangunan.getCount();i++)
		{
			params['NoRegBarang-'+i]=dsvStorePanelBangunan.getRange()[i].data.no_reg;
		}
	}
	else if (panel==='angkutan')
	{
		var hasilNoRegisterPanelAngkutanPembukuanInv
		if (NoRegisterPanelAngkutanPembukuanInv!=undefined)
		{
			hasilNoRegisterPanelAngkutanPembukuanInv = NoRegisterPanelAngkutanPembukuanInv;
		}
		else
		{
			hasilNoRegisterPanelAngkutanPembukuanInv = tahun;
		}	
			
		var	params =
		{
			Panel:panel,
			NoRegister:hasilNoRegisterPanelAngkutanPembukuanInv,
			jumlahrecord:dsvStorePanelAngkutan.getCount(),
			KdKelompok:Ext.getCmp('TxtKdKelompokUnitBarangPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			Urutan:Ext.getCmp('TxtUrutanUnitBarangPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			KodeMerk:KdMerkPanelAngkutanPembukuanInv,
			MerkTipe:InvPembukuanInventaris.vars.merktipepembukuanangkutaninv.getValue(),
			ThnProduksi:Ext.getCmp('TxtTahunProduksiUnitBarangPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			NoSeri:Ext.getCmp('TxtNoSeriUnitBarangPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			NoMesin:Ext.getCmp('TxtNoMesinPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			TglBuku:Ext.getCmp('tglBukuUnitBarangPanelAngkutanPembukuanInventaris_Inv').getValue(),
			Kelompok:Ext.getCmp('TxtKelompokPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			NamaAlat:Ext.getCmp('TxtNamaAlatPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			Dokumen:Ext.getCmp('TxtDokumenPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			TglDokumen:Ext.getCmp('tglDokumenPanelAngkutanPembukuanInventaris_Inv').getValue(),
			Pabrik:Ext.getCmp('TxtPabrikNegaraPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			NoRangka:Ext.getCmp('TxtNoRangkaPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			CC:Ext.getCmp('TxtCCUnitBarangPanelAngkutanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			NoPol:Ext.getCmp('TxtNoPolUnitBarangPanelAngkutanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			NoBPKB:Ext.getCmp('TxtNoBPKBUnitBarangPanelAngkutanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			NoTerima:Ext.getCmp('TxtNomorTerimaPengadaanAngkutanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			TglTerima:Ext.getCmp('tglTerimaPengadaanAngkutanInventaris_Inv').getValue(),
			Vendor:Ext.getCmp('TxtDariPengadaanAngkutanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			Perolehan:Ext.getCmp('cmbPerolehanPengadaanPanelAngkutanPembukuanInventaris_Inv').getValue(),
			TglPerolehan:Ext.getCmp('tglTerimaPengadaanAngkutanInventaris_Inv').getValue(),
			SumberDana:Ext.getCmp('cmbSumberDanaPengadaanPanelAngkutanPembukuanInventaris_Inv').getValue(),
			Kondisi:Ext.getCmp('cmbKondisiPengadaanPanelAngkutanPembukuanInventaris_Inv').getValue(),
			Harga:Ext.getCmp('TxtHargaPengadaanAngkutanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			Pengurus:Ext.getCmp('TxtPengurusAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			AlamatPengurus:Ext.getCmp('TxtAlamatPengurusAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			Keterangan:Ext.getCmp('TxtKeteranganPengurusAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
		}
		for(var i = 0 ; i < dsvStorePanelAngkutan.getCount();i++)
		{
			params['NoRegBarang-'+i]=dsvStorePanelAngkutan.getRange()[i].data.no_reg;
		}
	}
	else if (panel==='baranglain')
	{
		var hasilNoRegisterPanelBarangLainPembukuanInv
		if (NoRegisterPanelBarangLainPembukuanInv!=undefined)
		{
			hasilNoRegisterPanelBarangLainPembukuanInv = NoRegisterPanelBarangLainPembukuanInv;
		}
		else
		{
			hasilNoRegisterPanelBarangLainPembukuanInv = tahun;
		}	
			
		var	params =
		{
			Panel:panel,
			NoRegister:hasilNoRegisterPanelBarangLainPembukuanInv,
			jumlahrecord:dsvStorePanelBarangLain.getCount(),
			KdKelompok:Ext.getCmp('TxtKdKelompokUnitBarangPanelBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			Urutan:Ext.getCmp('TxtUrutanUnitBarangPanelBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			KodeMerk:KdMerkPanelBarangLainPembukuanInv,
			MerkTipe:InvPembukuanInventaris.vars.merktipepembukuanbaranglaininv.getValue(),
			Jumlah:Ext.getCmp('TxtJumlahUnitBarangPanelBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			Spesifikasi:Ext.getCmp('TxtSpesifikasiUnitBarangPanelBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			TglBuku:Ext.getCmp('tglBukuUnitBarangPanelBarangLainPembukuanInventaris_Inv').getValue(),
			Kelompok:Ext.getCmp('TxtKelompokPanelBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			Barang:Ext.getCmp('TxtBarangPanelBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			Dokumen:Ext.getCmp('TxtDokumenPanelBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			TglDokumen:Ext.getCmp('tglDokumenPanelBarangLainPembukuanInventaris_Inv').getValue(),
			Satuan:KdSatuanPanelBarangLainPembukuanInv,
			NoTerima:Ext.getCmp('TxtNomorTerimaPengadaanBarangLainFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			TglTerima:Ext.getCmp('tglTerimaPengadaanBarangLainInventaris_Inv').getValue(),
			Vendor:Ext.getCmp('TxtDariPengadaanBarangLainFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			Perolehan:Ext.getCmp('cmbPerolehanPengadaanPanelBarangLainPembukuanInventaris_Inv').getValue(),
			TglPerolehan:Ext.getCmp('tglTerimaPengadaanAngkutanInventaris_Inv').getValue(),
			SumberDana:Ext.getCmp('cmbSumberDanaPengadaanPanelBarangLainPembukuanInventaris_Inv').getValue(),
			Kondisi:Ext.getCmp('cmbKondisiPengadaanPanelBarangLainPembukuanInventaris_Inv').getValue(),
			Harga:Ext.getCmp('TxtHargaPengadaanBarangLainFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			Pengurus:Ext.getCmp('TxtPengurusBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			AlamatPengurus:Ext.getCmp('TxtAlamatPengurusBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
			Keterangan:Ext.getCmp('TxtKeteranganPengurusBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue(),
		}
		for(var i = 0 ; i < dsvStorePanelBarangLain.getCount();i++)
		{
			params['NoRegBarang-'+i]=dsvStorePanelBarangLain.getRange()[i].data.no_reg;
		}
	}
    return params
};

function ValidasiEntryPembukuanInv(modul,mBolHapus,panel){
	var x = 1;
	if (panel==='tanah')
	{
		o=dsvStorePanelTanah.getCount();
		if (o===0)
		{
			ShowPesanWarningInvPembukuanInventaris('No reg barang masih kosong', 'Warning');
			x = 0;
		}
		if(Ext.getCmp('TxtKdKelompokUnitBarangPanelTanahPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue() === ''){
			ShowPesanWarningInvPembukuanInventaris('Kode Kelompok masih kosong', 'Warning');
			x = 0;
		}	 
		if(Ext.getCmp('TxtUrutanUnitBarangPanelTanahPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue() === ''){
			ShowPesanWarningInvPembukuanInventaris('Urutan masih kosong', 'Warning');
			x = 0;
		}
		if(Ext.getCmp('TxtKelompokPanelTanahPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue() === ''){
			ShowPesanWarningInvPembukuanInventaris('Kelompok masih kosong', 'Warning');
			x = 0;
		}
		if(Ext.getCmp('TxtTanahPanelTanahPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue() === ''){
			ShowPesanWarningInvPembukuanInventaris('Tanah masih kosong', 'Warning');
			x = 0;
		}
		if(Ext.getCmp('TxtNomorTerimaPengadaanFilterGridDataView_PembukuanInventaris_Inv').getValue() === ''){
			ShowPesanWarningInvPembukuanInventaris('Nomor Terima masih kosong', 'Warning');
			x = 0;
		}
	}
	else if (panel==='bangunan')
	{
		o=dsvStorePanelBangunan.getCount();
		if (o===0)
		{
			ShowPesanWarningInvPembukuanInventaris('No reg barang masih kosong', 'Warning');
			x = 0;
		}
		if(Ext.getCmp('TxtKdKelompokUnitBarangPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue() === ''){
			ShowPesanWarningInvPembukuanInventaris('Kode Kelompok masih kosong', 'Warning');
			x = 0;
		}	 
		if(Ext.getCmp('TxtUrutanUnitBarangPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue() === ''){
			ShowPesanWarningInvPembukuanInventaris('Urutan masih kosong', 'Warning');
			x = 0;
		}
		if(Ext.getCmp('TxtKelompokPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue() === ''){
			ShowPesanWarningInvPembukuanInventaris('Kelompok masih kosong', 'Warning');
			x = 0;
		}
		if(Ext.getCmp('TxtBangunanPanelBangunanPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue() === ''){
			ShowPesanWarningInvPembukuanInventaris('Tanah masih kosong', 'Warning');
			x = 0;
		}
		if(Ext.getCmp('TxtNomorTerimaPengadaanBangunanFilterGridDataView_PembukuanInventaris_Inv').getValue() === ''){
			ShowPesanWarningInvPembukuanInventaris('Nomor Terima masih kosong', 'Warning');
			x = 0;
		}
	}
	else if (panel==='angkutan')
	{
		o=dsvStorePanelAngkutan.getCount();
		if (o===0)
		{
			ShowPesanWarningInvPembukuanInventaris('No reg barang masih kosong', 'Warning');
			x = 0;
		}
		if(Ext.getCmp('TxtKdKelompokUnitBarangPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue() === ''){
			ShowPesanWarningInvPembukuanInventaris('Kode Kelompok masih kosong', 'Warning');
			x = 0;
		}	 
		if(Ext.getCmp('TxtUrutanUnitBarangPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue() === ''){
			ShowPesanWarningInvPembukuanInventaris('Urutan masih kosong', 'Warning');
			x = 0;
		}
		if(Ext.getCmp('TxtKelompokPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue() === ''){
			ShowPesanWarningInvPembukuanInventaris('Kelompok masih kosong', 'Warning');
			x = 0;
		}
		if(Ext.getCmp('TxtNamaAlatPanelAngkutanPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue() === ''){
			ShowPesanWarningInvPembukuanInventaris('Tanah masih kosong', 'Warning');
			x = 0;
		}
		if(Ext.getCmp('TxtNomorTerimaPengadaanAngkutanFilterGridDataView_PembukuanInventaris_Inv').getValue() === ''){
			ShowPesanWarningInvPembukuanInventaris('Nomor Terima masih kosong', 'Warning');
			x = 0;
		}
	}
	else if (panel==='baranglain')
	{
		o=dsvStorePanelBarangLain.getCount();
		if (o===0)
		{
			ShowPesanWarningInvPembukuanInventaris('No reg barang masih kosong', 'Warning');
			x = 0;
		}
		if(Ext.getCmp('TxtKdKelompokUnitBarangPanelBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue() === ''){
			ShowPesanWarningInvPembukuanInventaris('Kode Kelompok masih kosong', 'Warning');
			x = 0;
		}	 
		if(Ext.getCmp('TxtUrutanUnitBarangPanelBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue() === ''){
			ShowPesanWarningInvPembukuanInventaris('Urutan masih kosong', 'Warning');
			x = 0;
		}
		if(Ext.getCmp('TxtKelompokPanelBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue() === ''){
			ShowPesanWarningInvPembukuanInventaris('Kelompok masih kosong', 'Warning');
			x = 0;
		}
		if(Ext.getCmp('TxtBarangPanelBarangLainPembukuanFilterGridDataView_PembukuanInventaris_Inv').getValue() === ''){
			ShowPesanWarningInvPembukuanInventaris('Tanah masih kosong', 'Warning');
			x = 0;
		}
		if(Ext.getCmp('TxtNomorTerimaPengadaanBarangLainFilterGridDataView_PembukuanInventaris_Inv').getValue() === ''){
			ShowPesanWarningInvPembukuanInventaris('Nomor Terima masih kosong', 'Warning');
			x = 0;
		}
	}
	return x;
};


//------------------------------Sub  Obat----------------------------------------------
function DataInitPanelSubObat(rowdata){
	Ext.getCmp('txtKdSubObat_InvPembukuanInventarisL').setValue(rowdata.kd_sub_jns);
	InvPembukuanInventaris.vars.sub.setValue(rowdata.sub_);
};

function dataaddnewSub_PembukuanInventaris_Inv(){
	Ext.getCmp('txtKdSubObat_InvPembukuanInventarisL').setValue('');
	InvPembukuanInventaris.vars.sub.setValue('');
}

function datasaveSub_PembukuanInventaris_Inv(){
	if (ValidasiEntrySubObat(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionInvPembukuanInventaris/saveSub",
				params: getParamSetupSubObat(),
				failure: function(o)
				{
					ShowPesanErrorInvPembukuanInventaris('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoInvPembukuanInventaris(nmPesanSimpanSukses,nmHeaderSimpanData);
						Ext.get('txtKdSubObat_InvPembukuanInventarisL').dom.value=cst.kdsub;
						dsDataPembukuan_Inventaris_Inv.reload();
					}
					else 
					{
						ShowPesanErrorInvPembukuanInventaris('Gagal Menyimpan Data ini', 'Error');
						dsDataPembukuan_Inventaris_Inv.reload();
					};
				}
			}
			
		)
	}
}

function getParamSetupSubObat() {
	var	params =
	{
		KdSubJns:Ext.getCmp('txtKdSubObat_InvPembukuanInventarisL').getValue(),
		Sub:InvPembukuanInventaris.vars.sub.getValue()
	}
   
    return params
};

function ValidasiEntrySubObat(modul,mBolHapus){
	var x = 1;
	if(InvPembukuanInventaris.vars.sub.getValue() === ''){
		ShowPesanWarningInvPembukuanInventaris('Sub  obat masih kosong', 'Warning');
		x = 0;
	} 
	return x;
};


//------------------------------GOLONGAN Obat----------------------------------------------
function DataInitPanelGolongan(rowdata){
	Ext.getCmp('txtKdGolObat_InvPembukuanInventarisL').setValue(rowdata.apt_kd_golongan);
	InvPembukuanInventaris.vars.golongan.setValue(rowdata.apt_golongan);
};

function dataaddnewGolongan_PembukuanInventaris_Inv(){
	Ext.getCmp('txtKdGolObat_InvPembukuanInventarisL').setValue('');
	InvPembukuanInventaris.vars.golongan.setValue('');
}

function datasaveGolongan_PembukuanInventaris_Inv(){
	if (ValidasiEntryGolongan(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionInvPembukuanInventaris/saveGolongan",
				params: getParamSetupGolongan(),
				failure: function(o)
				{
					ShowPesanErrorInvPembukuanInventaris('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoInvPembukuanInventaris(nmPesanSimpanSukses,nmHeaderSimpanData);
						Ext.get('txtKdGolObat_InvPembukuanInventarisL').dom.value=cst.kdgolongan;
						dsDataGrdGolongan_viSetupMasterObat.reload();
					}
					else 
					{
						ShowPesanErrorInvPembukuanInventaris('Gagal Menyimpan Data ini', 'Error');
						dsDataGrdGolongan_viSetupMasterObat.reload();
					};
				}
			}
			
		)
	}
}

function getParamSetupGolongan() {
	var	params =
	{
		KdGolongan:Ext.getCmp('txtKdGolObat_InvPembukuanInventarisL').getValue(),
		Golongan:InvPembukuanInventaris.vars.golongan.getValue()
	}
   
    return params
};

function ValidasiEntryGolongan(modul,mBolHapus){
	var x = 1;
	if(InvPembukuanInventaris.vars.golongan.getValue() === ''){
		ShowPesanWarningInvPembukuanInventaris('Golongan obat masih kosong', 'Warning');
		x = 0;
	} 
	return x;
};



//------------------------------SATUAN ----------------------------------------------
function DataInitPanelSatuan(rowdata){
	Ext.getCmp('txtKdSatuan_InvPembukuanInventarisL').setValue(rowdata.kd_satuan);
	InvPembukuanInventaris.vars.satuan.setValue(rowdata.satuan);
};

function dataaddnewSatuan_PembukuanInventaris_Inv(){
	Ext.getCmp('txtKdSatuan_InvPembukuanInventarisL').setValue('');
	InvPembukuanInventaris.vars.satuan.setValue('');
}

function datasaveSatuan_PembukuanInventaris_Inv(){
	if (ValidasiEntrySatuan(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionInvPembukuanInventaris/saveSatuan",
				params: getParamSetupSatuan(),
				failure: function(o)
				{
					ShowPesanErrorInvPembukuanInventaris('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoInvPembukuanInventaris(nmPesanSimpanSukses,nmHeaderSimpanData);
						dsDataGrdSatuan_viSetupMasterObat.reload();
					}
					else 
					{
						ShowPesanErrorInvPembukuanInventaris('Gagal Menyimpan Data ini', 'Error');
						dsDataGrdSatuan_viSetupMasterObat.reload();
					};
				}
			}
			
		)
	}
}

function getParamSetupSatuan(){
	var	params =
	{
		KdSatuan:Ext.getCmp('txtKdSatuan_InvPembukuanInventarisL').getValue(),
		Satuan:InvPembukuanInventaris.vars.satuan.getValue()
	}
   
    return params
};

function ValidasiEntrySatuan(modul,mBolHapus){
	var x = 1;
	if(Ext.getCmp('txtKdSatuan_InvPembukuanInventarisL').getValue() === ''){
		ShowPesanWarningInvPembukuanInventaris('Kode Satuan masih kosong', 'Warning');
		x = 0;
	}
	if(InvPembukuanInventaris.vars.satuan.getValue() === ''){
		ShowPesanWarningInvPembukuanInventaris('Satuan masih kosong', 'Warning');
		x = 0;
	} 
	if(Ext.getCmp('txtKdSatuan_InvPembukuanInventarisL').getValue().length > 10){
		ShowPesanWarningInvPembukuanInventaris('Kode satuan kecil tidak boleh lebih dari 10 huruf', 'Warning');
		x = 0;
	}
	return x;
};


//------------------------------SATUAN BESAR----------------------------------------------
function DataInitPanelSatuanBesar(rowdata){
	Ext.getCmp('txtKdSatuanBesar_InvPembukuanInventarisL').setValue(rowdata.kd_sat_besar);
	InvPembukuanInventaris.vars.satuanBesar.setValue(rowdata.keterangan);
};

function dataaddnewSatuanBesar_PembukuanInventaris_Inv(){
	Ext.getCmp('txtKdSatuanBesar_InvPembukuanInventarisL').setValue('');
	InvPembukuanInventaris.vars.satuanBesar.setValue('');
}

function datasaveSatuanBesar_PembukuanInventaris_Inv(){
	if (ValidasiEntrySatuanBesar(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionInvPembukuanInventaris/saveSatuanBesar",
				params: getParamSetupSatuanBesar(),
				failure: function(o)
				{
					ShowPesanErrorInvPembukuanInventaris('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoInvPembukuanInventaris(nmPesanSimpanSukses,nmHeaderSimpanData);
						//Ext.get('txtKdSatuanBesar_InvPembukuanInventarisL').dom.value=cst.kdsatbesar;
						dsDataGrdSatuanBesar_viSetupMasterObat.reload();
					}
					else 
					{
						ShowPesanErrorInvPembukuanInventaris('Gagal Menyimpan Data ini', 'Error');
						dsDataGrdSatuanBesar_viSetupMasterObat.reload();
					};
				}
			}
			
		)
	}
}

function getParamSetupSatuanBesar(){
	var	params =
	{
		KdSatBesar:Ext.getCmp('txtKdSatuanBesar_InvPembukuanInventarisL').getValue(),
		Keterangan:InvPembukuanInventaris.vars.satuanBesar.getValue()
	}
   
    return params
};

function ValidasiEntrySatuanBesar(modul,mBolHapus){
	var x = 1;
	if(Ext.getCmp('txtKdSatuanBesar_InvPembukuanInventarisL').getValue() === ''){
		ShowPesanWarningInvPembukuanInventaris('Kode satuan besar satuan masih kosong', 'Warning');
		x = 0;
	}
	if(InvPembukuanInventaris.vars.satuanBesar.getValue() === ''){
		ShowPesanWarningInvPembukuanInventaris('Keterangan satuan masih kosong', 'Warning');
		x = 0;
	}	
	if(Ext.getCmp('txtKdSatuanBesar_InvPembukuanInventarisL').getValue().length > 10){
		ShowPesanWarningInvPembukuanInventaris('Kode satuan besar tidak boleh lebih dari 10 huruf', 'Warning');
		x = 0;
	}
	return x;
};



function ShowPesanWarningInvPembukuanInventaris(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorInvPembukuanInventaris(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoInvPembukuanInventaris(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};// JavaScript Document