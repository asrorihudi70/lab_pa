var kdWaktuMintaUmum;
var kdJenisMintaUmum;
var selectCount_PenerimaanInventaris_Inv = 50;
var NamaForm_PenerimaanInventaris_Inv = "Penerimaan Inventaris";
var selectCountStatusPostingPenerimaanInventaris_Inv = 'Semua'; //
var mod_name_PenerimaanInventaris_Inv = "PenerimaanInventaris_Inv";
var now_PenerimaanInventaris_Inv = new Date();
var rowSelected_PenerimaanInventaris_Inv;
var rowSelectedGridUmum_PenerimaanInventaris_Inv;
var setLookUps_PenerimaanInventaris_Inv;
var selectSetUnit;
var mod_name_viTerima = "viTerimaInv";
var cellSelecteddeskripsiRWI;
var tanggal = now_PenerimaanInventaris_Inv.format("d/M/Y");
var jam = now_PenerimaanInventaris_Inv.format("H/i/s");
var blnGetNoTerimaOto = now_PenerimaanInventaris_Inv.format("m");
var thnGetNoTerimaOto = now_PenerimaanInventaris_Inv.format("Y");
var getNoTerimaOto = thnGetNoTerimaOto + "-" + blnGetNoTerimaOto;
var tmpkriteria;
var gridDTLUmum_PenerimaanInventaris_Inv;
var GridDataView_PenerimaanInventaris_Inv;
var s;
var varNoOtoTerima;
var dsvComboVendor;
var stateComboVendor = ['kd_vendor', 'vendor'];
var kodeVendorLookUpForCmbBoxInv;
var varNoUrutPenerimaanInv = 0;
var noUrutBrgPenerimaanInv;
var noTerimaLookUp;
var cekPostPenerimaanInv;
var totalSemuaBrgterima = 0;
var CurrentPenerimaanInventaris_Inv = {
	data: Object,
	details: Array,
	row: 0
};

var CurrentData_PenerimaanInventaris_Inv = {
	data: Object,
	details: Array,
	row: 0
};

var CurrentDataUmum_PenerimaanInventaris_Inv = {
	data: Object,
	details: Array,
	row: 0
};

var InvPenerimaanInventaris = {};
InvPenerimaanInventaris.form = {};
InvPenerimaanInventaris.func = {};
InvPenerimaanInventaris.vars = {};
InvPenerimaanInventaris.func.parent = InvPenerimaanInventaris;
InvPenerimaanInventaris.form.ArrayStore = {};
InvPenerimaanInventaris.form.ComboBox = {};
InvPenerimaanInventaris.form.DataStore = {};
InvPenerimaanInventaris.form.Record = {};
InvPenerimaanInventaris.form.Form = {};
InvPenerimaanInventaris.form.Grid = {};
InvPenerimaanInventaris.form.Panel = {};
InvPenerimaanInventaris.form.TextField = {};
InvPenerimaanInventaris.form.Button = {};

InvPenerimaanInventaris.form.ArrayStore.penerimaaninv = new Ext.data.ArrayStore({
	id: 0,
	fields: ['no_urut_brg', 'kd_inv', 'nama_brg'],
	data: []
});
InvPenerimaanInventaris.form.ArrayStore.satuanpenerimaaninv = new Ext.data.ArrayStore({
	id: 0,
	fields: ['kd_satuan', 'satuan'],
	data: []
});
CurrentPage.page = dataGrid_PenerimaanInventaris_Inv(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);


function dataGriAwalPenerimaanInvLookup(rowdata) {
	if (rowdata != undefined) {
		noTerimaLookUp = '';
		noTerimaLookUp = rowdata.no_terima;
	}
	Ext.Ajax.request({
			url: baseURL + "index.php/inventaris/functionPenerimaanInventaris/getDataGridAwalLookUp",
			params: {
				text: noTerimaLookUp
			},
			failure: function (o) {
				ShowPesanErrorPenerimaanInventaris_Inv('Hubungi Admin', 'Error');
			},
			success: function (o) {
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) {
					cekPostPenerimaanInv = parseInt(cst.status);
					dsDataGrdUmum_PenerimaanInventaris_Inv.removeAll();
					var recs = [],
						recType = dsDataGrdUmum_PenerimaanInventaris_Inv.recordType;
					var total = 0;
					for (var i = 0; i < cst.ListDataObj.length; i++) {
						recs.push(new recType(cst.ListDataObj[i]));
						total += parseFloat(cst.ListDataObj[i].sub_total);
					}
					var a = toInteger(total);
					console.log(a);
					dsDataGrdUmum_PenerimaanInventaris_Inv.add(recs);
					InvPenerimaanInventaris.form.Grid.a.getView().refresh();
					var tgl_spk = cst.tglspk.split(" ");
					Ext.getCmp('txtNoPenerimaanInventaris_Inv').setValue(cst.noterima);
					Ext.getCmp('txtNoFakturPenerimaanInventaris_Inv').setValue(cst.nofaktur);
					Ext.getCmp('txtNoSPKPenerimaanInventaris_Inv').setValue(cst.nospk);
					Ext.getCmp('cmbFindVendorPenerimaanInventaris_Inv').setValue(cst.vendor);
					Ext.getCmp('txtGrandTotal_InvPenerimaanInvL').setValue(toFormat(a));
					Ext.getCmp('tglSPKPenerimaanInventaris_Inv').setValue(ShowDate(cst.tglspk));
					Ext.getCmp('tglPenerimaanInventaris_Inv').setValue(ShowDate(cst.tglterima));
					Ext.getCmp('TxtKeteranganPenerimaanFilterGridDataView_PermintaanInventaris_Inv').setValue(cst.ket);
					varNoUrutPenerimaanInv = cst.nourut;
					if (cst.status === 'Yes') {
						ShowPesanSuksesMintaUmum('Data Penerimaan Ini Telah Diposting', 'Informasi');
						Ext.getCmp('txtNoPenerimaanInventaris_Inv').disable();
						Ext.getCmp('txtNoFakturPenerimaanInventaris_Inv').disable();
						Ext.getCmp('txtNoSPKPenerimaanInventaris_Inv').disable();
						Ext.getCmp('cmbFindVendorPenerimaanInventaris_Inv').disable();
						Ext.getCmp('txtGrandTotal_InvPenerimaanInvL').disable();
						Ext.getCmp('tglSPKPenerimaanInventaris_Inv').disable();
						Ext.getCmp('tglPenerimaanInventaris_Inv').disable();
						Ext.getCmp('TxtKeteranganPenerimaanFilterGridDataView_PermintaanInventaris_Inv').disable();
						Ext.getCmp('btnAdd_PenerimaanInventaris_Inv').disable();
						Ext.getCmp('btnSave_PenerimaanInventaris_Inv').disable();
						Ext.getCmp('btnDeletePenerimaan_PenerimaanInventaris_Inv').disable();
						Ext.getCmp('btnPosting_viInvPenerimaanInventaris_Inv').hide();
						Ext.getCmp('btnAddBarangInvPenerimaanInventarisL').disable();
						Ext.getCmp('btnDelete_viInvPenerimaanInventaris').disable();
						// y
						Ext.getCmp('btnUnPosting_viInvPenerimaanInventaris_Inv').enable();
						//Ext.getCmp('btnUpdate_PenerimaanInventaris_Inv').enable();
						// end y
						InvPenerimaanInventaris.form.Grid.a.disable();
					} else {
						Ext.getCmp('txtNoPenerimaanInventaris_Inv').enable();
						Ext.getCmp('txtNoFakturPenerimaanInventaris_Inv').enable();
						Ext.getCmp('txtNoSPKPenerimaanInventaris_Inv').enable();
						Ext.getCmp('cmbFindVendorPenerimaanInventaris_Inv').enable();
						Ext.getCmp('txtGrandTotal_InvPenerimaanInvL').enable();
						Ext.getCmp('txtNoSPKPenerimaanInventaris_Inv').enable();
						Ext.getCmp('tglSPKPenerimaanInventaris_Inv').enable();
						Ext.getCmp('tglPenerimaanInventaris_Inv').enable();
						Ext.getCmp('TxtKeteranganPenerimaanFilterGridDataView_PermintaanInventaris_Inv').enable();
						Ext.getCmp('btnAdd_PenerimaanInventaris_Inv').enable();
						Ext.getCmp('btnSave_PenerimaanInventaris_Inv').enable();
						Ext.getCmp('btnDeletePenerimaan_PenerimaanInventaris_Inv').enable();
						Ext.getCmp('btnPosting_viInvPenerimaanInventaris_Inv').enable();
						Ext.getCmp('btnAddBarangInvPenerimaanInventarisL').enable();
						Ext.getCmp('btnDelete_viInvPenerimaanInventaris').enable();
						// y
						Ext.getCmp('btnUnPosting_viInvPenerimaanInventaris_Inv').hide();
						//Ext.getCmp('btnUpdate_PenerimaanInventaris_Inv').show();
						// end y
						InvPenerimaanInventaris.form.Grid.a.enable();
					}
				} else {
					ShowPesanErrorPenerimaanInventaris_Inv('Gagal membaca data', 'Error');
				};
			}
		}

	)

}

function getDataPenerimaanInvLookup(no_terimanya) {
	/* if (rowdata != undefined) {
		noTerimaLookUp = '';
		noTerimaLookUp = rowdata.no_terima;
	} */
	Ext.Ajax.request({
			url: baseURL + "index.php/inventaris/functionPenerimaanInventaris/getDataGridAwalLookUp",
			params: {
				text: no_terimanya
			},
			failure: function (o) {
				ShowPesanErrorPenerimaanInventaris_Inv('Hubungi Admin', 'Error');
			},
			success: function (o) {
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) {
					cekPostPenerimaanInv = parseInt(cst.status);
					dsDataGrdUmum_PenerimaanInventaris_Inv.removeAll();
					var recs = [],
						recType = dsDataGrdUmum_PenerimaanInventaris_Inv.recordType;
					var total = 0;
					for (var i = 0; i < cst.ListDataObj.length; i++) {
						recs.push(new recType(cst.ListDataObj[i]));
						total += parseFloat(cst.ListDataObj[i].sub_total);
					}
					var a = toInteger(total);
					console.log(a);
					dsDataGrdUmum_PenerimaanInventaris_Inv.add(recs);
					InvPenerimaanInventaris.form.Grid.a.getView().refresh();
					var tgl_spk = cst.tglspk.split(" ");
					Ext.getCmp('txtNoPenerimaanInventaris_Inv').setValue(cst.noterima);
					Ext.getCmp('txtNoFakturPenerimaanInventaris_Inv').setValue(cst.nofaktur);
					Ext.getCmp('txtNoSPKPenerimaanInventaris_Inv').setValue(cst.nospk);
					Ext.getCmp('cmbFindVendorPenerimaanInventaris_Inv').setValue(cst.vendor);
					Ext.getCmp('txtGrandTotal_InvPenerimaanInvL').setValue(toFormat(a));
					Ext.getCmp('tglSPKPenerimaanInventaris_Inv').setValue(ShowDate(cst.tglspk));
					Ext.getCmp('tglPenerimaanInventaris_Inv').setValue(ShowDate(cst.tglterima));
					Ext.getCmp('TxtKeteranganPenerimaanFilterGridDataView_PermintaanInventaris_Inv').setValue(cst.ket);
					varNoUrutPenerimaanInv = cst.nourut;
					if (cst.status === 'Yes') {
						ShowPesanSuksesMintaUmum('Data Penerimaan Ini Telah Diposting', 'Informasi');
						Ext.getCmp('txtNoPenerimaanInventaris_Inv').disable();
						Ext.getCmp('txtNoFakturPenerimaanInventaris_Inv').disable();
						Ext.getCmp('txtNoSPKPenerimaanInventaris_Inv').disable();
						Ext.getCmp('cmbFindVendorPenerimaanInventaris_Inv').disable();
						Ext.getCmp('txtGrandTotal_InvPenerimaanInvL').disable();
						Ext.getCmp('tglSPKPenerimaanInventaris_Inv').disable();
						Ext.getCmp('tglPenerimaanInventaris_Inv').disable();
						Ext.getCmp('TxtKeteranganPenerimaanFilterGridDataView_PermintaanInventaris_Inv').disable();
						Ext.getCmp('btnAdd_PenerimaanInventaris_Inv').disable();
						Ext.getCmp('btnSave_PenerimaanInventaris_Inv').disable();
						Ext.getCmp('btnDeletePenerimaan_PenerimaanInventaris_Inv').disable();
						Ext.getCmp('btnPosting_viInvPenerimaanInventaris_Inv').hide();
						Ext.getCmp('btnAddBarangInvPenerimaanInventarisL').disable();
						Ext.getCmp('btnDelete_viInvPenerimaanInventaris').disable();
						// y
						Ext.getCmp('btnUnPosting_viInvPenerimaanInventaris_Inv').enable();
						//Ext.getCmp('btnUpdate_PenerimaanInventaris_Inv').enable();
						// end y
						InvPenerimaanInventaris.form.Grid.a.disable();
					} else {
						Ext.getCmp('txtNoPenerimaanInventaris_Inv').enable();
						Ext.getCmp('txtNoFakturPenerimaanInventaris_Inv').enable();
						Ext.getCmp('txtNoSPKPenerimaanInventaris_Inv').enable();
						Ext.getCmp('cmbFindVendorPenerimaanInventaris_Inv').enable();
						Ext.getCmp('txtGrandTotal_InvPenerimaanInvL').enable();
						Ext.getCmp('txtNoSPKPenerimaanInventaris_Inv').enable();
						Ext.getCmp('tglSPKPenerimaanInventaris_Inv').enable();
						Ext.getCmp('tglPenerimaanInventaris_Inv').enable();
						Ext.getCmp('TxtKeteranganPenerimaanFilterGridDataView_PermintaanInventaris_Inv').enable();
						Ext.getCmp('btnAdd_PenerimaanInventaris_Inv').enable();
						Ext.getCmp('btnSave_PenerimaanInventaris_Inv').enable();
						Ext.getCmp('btnDeletePenerimaan_PenerimaanInventaris_Inv').enable();
						Ext.getCmp('btnPosting_viInvPenerimaanInventaris_Inv').enable();
						Ext.getCmp('btnAddBarangInvPenerimaanInventarisL').enable();
						Ext.getCmp('btnDelete_viInvPenerimaanInventaris').enable();
						// y
						Ext.getCmp('btnUnPosting_viInvPenerimaanInventaris_Inv').hide();
						//Ext.getCmp('btnUpdate_PenerimaanInventaris_Inv').show();
						// end y
						InvPenerimaanInventaris.form.Grid.a.enable();
					}
				} else {
					ShowPesanErrorPenerimaanInventaris_Inv('Data tidak ada', 'Error');
					dataAddNew_PenerimaanInventaris_Inv();
				};
			}
		}

	)

}

function dataGrid_PenerimaanInventaris_Inv(mod_id_PenerimaanInventaris_Inv) {
	var FieldMaster_PenerimaanInventaris_Inv = [
		'no_terima', 'kd_vendor', 'tgl_terima', 'tgl_posting', 'status'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
	dataSource_PenerimaanInventaris_Inv = new WebApp.DataStore({
		fields: FieldMaster_PenerimaanInventaris_Inv
	});

	//refreshPenerimaanInventaris("left(no_terima,1)='2'");
	refreshPenerimaanInventaris();
	// Grid Apotek Perencanaan # --------------
	GridDataView_PenerimaanInventaris_Inv = new Ext.grid.EditorGridPanel({
		xtype: 'editorgrid',
		title: '',
		store: dataSource_PenerimaanInventaris_Inv,
		autoScroll: true,
		columnLines: true,
		border: false,
		anchor: '100% 76%',
		plugins: [new Ext.ux.grid.FilterRow()],
		selModel: new Ext.grid.RowSelectionModel
		// Tanda aktif saat salah satu baris dipilih # --------------
		({
			singleSelect: true,
			listeners: {
				rowselect: function (sm, row, rec) {
					rowSelected_PenerimaanInventaris_Inv = undefined;
					rowSelected_PenerimaanInventaris_Inv = dataSource_PenerimaanInventaris_Inv.getAt(row);
					CurrentData_PenerimaanInventaris_Inv
					CurrentData_PenerimaanInventaris_Inv.row = row;
					CurrentData_PenerimaanInventaris_Inv.data = rowSelected_PenerimaanInventaris_Inv.data;

				}
			}
		}),
		// Proses eksekusi baris yang dipilih # --------------
		listeners: {
			// Function saat ada event double klik maka akan muncul form view # --------------
			rowdblclick: function (sm, ridx, cidx) {
				//Ext.getCmp('btnEdit_PenerimaanInventaris_Inv').click;
				rowSelected_PenerimaanInventaris_Inv = dataSource_PenerimaanInventaris_Inv.getAt(ridx);
				if (rowSelected_PenerimaanInventaris_Inv != undefined) {
					setLookUp_PenerimaanInventaris_Inv(rowSelected_PenerimaanInventaris_Inv.data);
				} else {
					setLookUp_PenerimaanInventaris_Inv();

				}
			}
			// End Function # --------------
		},
		/**
		 *	Mengatur tampilan pada Grid Apotek perencanaan
		 *	Terdiri dari : Judul, Isi dan Event
		 *	Isi pada Grid di dapat dari pemangilan dari Net.
		 */
		colModel: new Ext.grid.ColumnModel(
			[
				new Ext.grid.RowNumberer(),
				{
					header: 'No. Terima',
					dataIndex: 'no_terima',
					sortable: true,
					width: 35

				},
				//-------------- ## --------------
				{
					header: 'Kode Vendor',
					dataIndex: 'kd_vendor',
					sortable: true,
					hideable: false,
					menuDisabled: true,
					width: 30
				},
				{
					header: 'Tgl. Terima',
					dataIndex: 'tgl_terima',
					sortable: true,
					hideable: false,
					menuDisabled: true,
					width: 30,
					renderer: function (v, params, record) {
						return ShowDate(record.data.tgl_terima);
					}
				},
				//-------------- ## --------------
				{
					header: 'Status Posting',
					dataIndex: 'status',
					sortable: true,
					hideable: false,
					menuDisabled: true,
					width: 30,
				},




				//-------------- ## --------------
			]
		),
		// Tolbar ke Dua # --------------
		tbar: {
			xtype: 'toolbar',
			id: 'toolbar_PenerimaanInventaris_Inv',
			items: [{
					xtype: 'button',
					text: 'Tambah Penerimaan ',
					iconCls: 'Edit_Tr',
					tooltip: 'Add Data',
					id: 'btnTambah_PenerimaanInventaris_Inv',
					handler: function (sm, row, rec) {

						setLookUp_PenerimaanInventaris_Inv();
						dataAddNew_PenerimaanInventaris_Inv();

					}
				},
				{
					xtype: 'button',
					text: 'Edit Data',
					iconCls: 'Edit_Tr',
					tooltip: 'Edit Data',
					id: 'btnEdit_PenerimaanInventaris_Inv',
					handler: function (sm, row, rec) {
						if (rowSelected_PenerimaanInventaris_Inv != undefined) {

							setLookUp_PenerimaanInventaris_Inv(rowSelected_PenerimaanInventaris_Inv.data);

						} else {
							ShowPesanWarningPenerimaanInventaris_Inv('Pilih data yang akan di edit', 'Warning');
						}

					}
				}
				//-------------- ## --------------
			]
		},
		bbar: bbar_paging(mod_name_PenerimaanInventaris_Inv, 100, dataSource_PenerimaanInventaris_Inv),
		// End Tolbar ke Dua # --------------
		// Button Bar Pagging # --------------
		// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
		//bbar : bbar_paging(mod_name_PenerimaanInventaris_Inv, selectCount_PenerimaanInventaris_Inv, dataSource_PenerimaanInventaris_Inv),
		// End Button Bar Pagging # --------------
		viewConfig: {
			forceFit: true
		}
	})

	var pencarianPenerimaanInventaris_Inv = new Ext.FormPanel({
		labelAlign: 'top',
		frame: true,
		title: '',
		bodyStyle: 'padding:5px 5px 0',
		//width: 600,
		items: [{
			layout: 'column',
			border: false,
			items: [{
				columnWidth: .98,
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: false,
				width: 500,
				height: 90,
				anchor: '100% 100%',
				items: [

					{
						x: 10,
						y: 0,
						xtype: 'label',
						text: 'No. Penerimaan'
					},
					{
						x: 120,
						y: 0,
						xtype: 'label',
						text: ':'
					},
					{
						x: 130,
						y: 0,
						xtype: 'textfield',
						id: 'TxtFilterGridNoPenerimaanInvPenerimaanInventaris',
						name: 'TxtFilterGridNoPenerimaanInvPenerimaanInventaris',
						width: 350,
						tabIndex: 1,
						listeners: {
							'specialkey': function () {
								if (Ext.EventObject.getKey() === 13) {
									tmpkriteria = getCriteriaCariInvPenerimaanInventaris();
									refreshPenerimaanInventaris(tmpkriteria);
								}
							}
						}
					},
					{
						x: 10,
						y: 30,
						xtype: 'label',
						text: 'Tanggal Terima'
					},
					{
						x: 120,
						y: 30,
						xtype: 'label',
						text: ':'
					},
					{
						x: 130,
						y: 30,
						xtype: 'datefield',
						id: 'dfTglAwalInvPenerimaanInventaris',
						format: 'd/M/Y',
						value: now_PenerimaanInventaris_Inv,
						width: 150,
						tabIndex: 1,
						listeners: {
							'specialkey': function () {
								if (Ext.EventObject.getKey() === 13) {
									tmpkriteria = getCriteriaCariInvPenerimaanInventaris();
									refreshPenerimaanInventaris(tmpkriteria);
								}
							}
						}
					},
					{
						x: 290,
						y: 30,
						xtype: 'label',
						text: 's/d'
					},
					{
						x: 320,
						y: 30,
						xtype: 'datefield',
						id: 'dfTglAkhirInvPenerimaanInventaris',
						format: 'd/M/Y',
						value: now_PenerimaanInventaris_Inv,
						width: 150,
						tabIndex: 1,
						listeners: {
							'specialkey': function () {
								if (Ext.EventObject.getKey() === 13) {
									tmpkriteria = getCriteriaCariInvPenerimaanInventaris();
									refreshPenerimaanInventaris(tmpkriteria);
								}
							}
						}
					},
					//----------------------------------------
					{
						x: 10,
						y: 60,
						xtype: 'label',
						text: '*) Enter untuk mencari'
					},
					//----------------------------------------
					{
						x: 290,
						y: 60,
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						tooltip: 'refresh',
						style: {
							paddingLeft: '30px'
						},
						width: 150,
						id: 'BtnRefreshGridDataView_PenerimaanInventaris_Inv',
						handler: function () {
							//refreshPenerimaanInventaris("left(no_terima,1)='2'");
							refreshPenerimaanInventaris();
						}
					}
				]
			}]
		}]
	})

	// Kriteria filter pada Grid # --------------
	var FrmFilterGridDataView_PenerimaanInventaris_Inv = new Ext.Panel({
		title: NamaForm_PenerimaanInventaris_Inv,
		iconCls: 'Studi_Lanjut',
		id: mod_id_PenerimaanInventaris_Inv,
		region: 'center',
		layout: 'form',
		closable: true,
		border: false,
		margins: '0 5 5 0',
		items: [pencarianPenerimaanInventaris_Inv,
			GridDataView_PenerimaanInventaris_Inv
		],
		tbar: [{
			//-------------- # Untuk mengelompokkan pencarian # --------------
			xtype: 'buttongroup',
			//title: 'Pencarian ' + NamaForm_PenerimaanInventaris_Inv,
			columns: 21,
			defaults: {
				scale: 'small'
			},
			frame: false,
			//-------------- ## --------------
			items: []
			//-------------- # End items # --------------
			//-------------- # End mengelompokkan pencarian # --------------
		}]
		//-------------- # End tbar # --------------
	})
	return FrmFilterGridDataView_PenerimaanInventaris_Inv;
	//-------------- # End form filter # --------------
}

function refreshPenerimaanInventaris(kriteria) {
	dataSource_PenerimaanInventaris_Inv.load({
		params: {
			Skip: 0,
			Take: 100,
			Sort: '',
			Sortdir: 'ASC',
			target: 'ViewPenerimaanInventaris',
			param: kriteria
		}
	});
	return dataSource_PenerimaanInventaris_Inv;
}

function getCriteriaCariInvPenerimaanInventaris() //^^^
{
	var strKriteria = "";

	if (Ext.get('TxtFilterGridNoPenerimaanInvPenerimaanInventaris').getValue() != "") {
		strKriteria = " no_terima " + "LIKE '%" + Ext.get('TxtFilterGridNoPenerimaanInvPenerimaanInventaris').getValue() + "%' ";
	}
	if (Ext.get('dfTglAwalInvPenerimaanInventaris').getValue() != "" && Ext.get('dfTglAkhirInvPenerimaanInventaris').getValue() != "") {
		if (strKriteria == "") {
			strKriteria = " tgl_terima between '" + Ext.get('dfTglAwalInvPenerimaanInventaris').getValue() + "' and '" + Ext.get('dfTglAkhirInvPenerimaanInventaris').getValue() + "'";
		} else {
			strKriteria += " and tgl_terima between '" + Ext.get('dfTglAwalInvPenerimaanInventaris').getValue() + "' and '" + Ext.get('dfTglAkhirInvPenerimaanInventaris').getValue() + "'";
		}
	}
	strKriteria = strKriteria;
	return strKriteria;
}

function setLookUp_PenerimaanInventaris_Inv(rowdata) {
	var lebar = 985;
	setLookUps_PenerimaanInventaris_Inv = new Ext.Window({
		id: Nci.getId(),
		title: NamaForm_PenerimaanInventaris_Inv,
		closeAction: 'destroy',
		width: 900,
		height: 550,
		resizable: false,
		autoScroll: false,
		border: true,
		constrainHeader: true,
		iconCls: 'Studi_Lanjut',
		modal: true,
		items: getFormItemEntry_PenerimaanInventaris_Inv(lebar, rowdata),
		listeners: {
			activate: function () {

			},
			afterShow: function () {
				this.activate();
			},
			deactivate: function () {
				rowSelected_PenerimaanInventaris_Inv = undefined;
			}
		}
	});

	setLookUps_PenerimaanInventaris_Inv.show();
	if (rowdata == undefined) {} else {
		dataGriAwalPenerimaanInvLookup(rowdata);
	}
}

function getFormItemEntry_PenerimaanInventaris_Inv(lebar, rowdata) {
	var pnlFormDataBasic_PenerimaanInventaris_Inv = new Ext.FormPanel({
		title: '',
		region: 'north',
		layout: 'form',
		bodyStyle: 'padding:10px 10px 10px 10px',
		anchor: '100%',
		labelWidth: 1,
		autoWidth: true,
		width: lebar,
		border: false,
		items: [
			getItemPanelInputPenerimaan_PenerimaanInventaris_Inv(lebar),
			getItemGridUmum_PenerimaanInventaris_Inv(lebar),
			{
				layout: 'column',
				border: false,
				items: [{
					columnWidth: .99,
					layout: 'absolute',
					bodyStyle: 'padding: 5px 5px 5px 5px',
					border: false,
					width: 510,
					height: 50,
					anchor: '100% 100%',
					items: [{
							x: 610,
							y: 5,
							xtype: 'label',
							text: 'Total'
						},
						{
							x: 650,
							y: 5,
							xtype: 'label',
							text: ':'
						},
						{
							x: 690,
							y: 5,
							xtype: 'textfield',
							style: 'text-align:right;',
							name: 'txtGrandTotal_InvPenerimaanInvL',
							id: 'txtGrandTotal_InvPenerimaanInvL',
							readOnly: true,
							tabIndex: 0,
							width: 110
						},

					]
				}]
			}
		],
		fileUpload: true,
		tbar: {
			xtype: 'toolbar',
			items: [{
					xtype: 'button',
					text: 'Add New',
					iconCls: 'add',
					id: 'btnAdd_PenerimaanInventaris_Inv',
					handler: function () {
						dataAddNew_PenerimaanInventaris_Inv();
					}
				}, {
					xtype: 'tbseparator'
				}, {
					xtype: 'button',
					text: 'Save',
					iconCls: 'save',
					id: 'btnSave_PenerimaanInventaris_Inv',
					handler: function () {
						//dataAddNew_PenerimaanInventaris_Inv();
						//loadMask.show();
						save_dataTerimaInv();
					}
				},
				{
					xtype: 'tbseparator'
				},
				// {
				// 	xtype: 'button',
				// 	text: 'Update',
				// 	iconCls: 'save',
				// 	id: 'btnUpdate_PenerimaanInventaris_Inv',
				// 	handler: function () {
				// 		update_dataTerimaInv();
				// 	}
				// }, {
				// 	xtype: 'tbseparator'
				// },
				{
					xtype: 'button',
					text: 'Delete',
					id: 'btnDeletePenerimaan_PenerimaanInventaris_Inv',
					iconCls: 'remove',
					handler: function () {
						Ext.Msg.confirm('Warning', 'Apakah data Penerimaan ini akan di hapus?', function (button) {
							if (button == 'yes') {
								//loadMask.show();
								Ext.Ajax.request({
										url: baseURL + "index.php/inventaris/functionPenerimaanInventaris/deleteSemuaPenerimaan",
										params: {
											no_terima: Ext.getCmp('txtNoPenerimaanInventaris_Inv').getValue(),
										},
										failure: function (o) {
											//loadMask.hide();
											ShowPesanErrorPenerimaanInventaris_Inv('Error, Hapus Penerimaan! Hubungi Admin', 'Error');
										},
										success: function (o) {
											var cst = Ext.decode(o.responseText);
											if (cst.success === true) {
												dataGriAwalPenerimaanInv();
												ShowPesanInfoPenerimaanInventaris_Inv('Penghapusan berhasil', 'Information');
												setLookUps_PenerimaanInventaris_Inv.close();
											} else {
												ShowPesanWarningPenerimaanInventaris_Inv('Gagal menghapus data ini', 'Warning');
											};
										}
									}

								)
							}
						});
					}

				},
				{
					xtype: 'tbseparator'
				},
				{
					xtype: 'button',
					text: 'Posting',
					disabled: true,
					iconCls: 'gantidok',
					id: 'btnPosting_viInvPenerimaanInventaris_Inv',
					handler: function () {
						posting_DataTerimaPenerimaanInv();
					}
				},
				// {
				// 	xtype: 'tbseparator'
				// },
				{
					xtype: 'button',
					text: 'Unposting',
					disabled: true,
					iconCls: 'gantidok',
					id: 'btnUnPosting_viInvPenerimaanInventaris_Inv',
					handler: function () {
						unposting_DataTerimaPenerimaanInv();
					}
				},
			]
		} //,items:
	})

	return pnlFormDataBasic_PenerimaanInventaris_Inv;
}

function dsComboVendor() {
	dsvComboVendor.load({
		params: {
			Skip: 0,
			Take: '',
			Sort: '',
			Sortdir: 'ASC',
			target: 'vComboVendor',
			//param : 'kd_tarif=1'
		}
	});
	return dsvComboVendor;
}

function funcFindVendorPenerimaanInventaris_Inv() {

	dsvComboVendor = new WebApp.DataStore({
		fields: stateComboVendor
	});
	dsComboVendor();
	//var text = '<table style="font-size: 11px;"><tr><td width="100">'+stateComboVendor[0]+'</td><td width="100">'+stateComboVendor[1]+'</td></tr></table>'
	var funcFindVendorPenerimaanInventaris = new Ext.form.ComboBox({
		x: 130,
		y: 60,
		id: 'cmbFindVendorPenerimaanInventaris_Inv',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		selectOnFocus: true,
		//hideTrigger:true,
		/*readOnly:true,
		emptyText:'Set Tarif',
		value: 1,*/
		width: 350,
		store: dsvComboVendor,
		valueField: 'kd_vendor',
		displayField: 'vendor',
		//value:selectSetPilihankelompokPasien,
		listeners: {
			'select': function (a, b, c) {
				kodeVendorLookUpForCmbBoxInv = this.value;
			}
		}
	});
	return funcFindVendorPenerimaanInventaris;
}

function getItemPanelInputPenerimaan_PenerimaanInventaris_Inv(lebar) {
	var items = {
		layout: 'form',
		border: true,
		bodyStyle: 'padding: 5px',
		items: [{
			layout: 'column',
			border: false,
			items: [{
				columnWidth: .98,
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: false,
				width: 500,
				height: 190,
				anchor: '100% 100%',
				items: [{
						x: 10,
						y: 0,
						xtype: 'label',
						text: 'No Terima'
					},
					{
						x: 120,
						y: 0,
						xtype: 'label',
						text: ':'
					},
					{
						x: 130,
						y: 0,
						xtype: 'textfield',
						id: 'txtNoPenerimaanInventaris_Inv',
						name: 'txtNoPenerimaanInventaris_Inv',
						width: 150,
						allowBlank: false,
						//readOnly: true,
						tabIndex: 1,
						listeners: {
							'specialkey': function () {
								if (Ext.EventObject.getKey() === 13) {
									
									getDataPenerimaanInvLookup(Ext.getCmp('txtNoPenerimaanInventaris_Inv').getValue());
								}
							}
							// y
							// 'specialkey': function () {
							// 	// keycode: 13 enter, 9 Tab
							// 	if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9) {
							// 		//secondGridStore.removeAll();
							// 		//dsGridPenerimaanInv(Ext.getCmp('txtNoPenerimaanInventaris_Inv').getValue());
							// 		//alert("tes");
							// 	}
							// }
							// end y
						}
					},
					{
						x: 490,
						y: 0,
						xtype: 'label',
						text: 'Tgl Terima'
					},
					{
						x: 560,
						y: 0,
						xtype: 'label',
						text: ':'
					},
					{
						x: 580,
						y: 0,
						xtype: 'datefield',
						id: 'tglPenerimaanInventaris_Inv',
						name: 'tglPenerimaanInventaris_Inv',
						width: 200,
						format: 'd/M/Y',
						width: 120,
						tabIndex: 3,
						value: now_PenerimaanInventaris_Inv,
					},
					{
						x: 10,
						y: 30,
						xtype: 'label',
						text: 'No Faktur'
					},
					{
						x: 120,
						y: 30,
						xtype: 'label',
						text: ':'
					},
					{
						x: 130,
						y: 30,
						xtype: 'textfield',
						id: 'txtNoFakturPenerimaanInventaris_Inv',
						name: 'txtNoFakturPenerimaanInventaris_Inv',
						width: 350,
						listeners: {
							/* 'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									
								} 						
							},
							blur: function(a){
								
							} */
						}
					},
					{
						x: 580,
						y: 30,
						xtype: 'datefield',
						id: 'tglFakturPenerimaanInventaris_Inv',
						name: 'tglFakturPenerimaanInventaris_Inv',
						width: 200,
						format: 'd/M/Y',
						width: 120,
						tabIndex: 3,
						hidden: true,
						value: now_PenerimaanInventaris_Inv,
					},
					{
						x: 10,
						y: 60,
						xtype: 'label',
						text: 'Vendor'
					},
					{
						x: 120,
						y: 60,
						xtype: 'label',
						text: ':'
					},
					funcFindVendorPenerimaanInventaris_Inv(),
					{
						x: 10,
						y: 90,
						xtype: 'label',
						text: 'Keterangan'
					},
					{
						x: 120,
						y: 90,
						xtype: 'label',
						text: ':'
					}, {
						x: 130,
						y: 90,
						xtype: 'textarea',
						id: 'TxtKeteranganPenerimaanFilterGridDataView_PermintaanInventaris_Inv',
						name: 'TxtKeteranganPenerimaanFilterGridDataView_PermintaanInventaris_Inv',
						emptyText: '',
						width: 350,
						tabIndex: 1,

					},
					{
						x: 10,
						y: 160,
						xtype: 'label',
						text: 'No SPK'
					},
					{
						x: 120,
						y: 160,
						xtype: 'label',
						text: ':'
					},
					{
						x: 130,
						y: 160,
						xtype: 'textfield',
						id: 'txtNoSPKPenerimaanInventaris_Inv',
						name: 'txtNoSPKPenerimaanInventaris_Inv',
						width: 350,
						tabIndex: 1,
						listeners: {
							/* 'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									
								} 						
							},
							blur: function(a){
								
							} */
						}
					},
					{
						x: 490,
						y: 160,
						xtype: 'label',
						text: 'Tanggal SPK'
					},
					{
						x: 560,
						y: 160,
						xtype: 'label',
						text: ':'
					},
					{
						x: 580,
						y: 160,
						xtype: 'datefield',
						id: 'tglSPKPenerimaanInventaris_Inv',
						name: 'tglSPKPenerimaanInventaris_Inv',
						width: 200,
						format: 'd/M/Y',
						width: 120,
						tabIndex: 3,
						value: now_PenerimaanInventaris_Inv,
					}

				]
			}]
		}]
	};
	return items;
};

function getItemGridUmum_PenerimaanInventaris_Inv(lebar) {
	var items = {
		layout: 'form',
		anchor: '100%',
		labelAlign: 'Left',
		bodyStyle: 'margin-top: -1px;',
		border: false,
		width: lebar - 80,
		height: 250, //300, 
		tbar: [{
			text: 'Add Barang',
			id: 'btnAddBarangInvPenerimaanInventarisL',
			tooltip: nmLookup,
			iconCls: 'find',
			handler: function () {
				var records = new Array();
				records.push(new dsDataGrdUmum_PenerimaanInventaris_Inv.recordType());
				dsDataGrdUmum_PenerimaanInventaris_Inv.add(records);

				/*if(Ext.getCmp('txtTmpNoPermintaanGzPermintaanUmumL').getValue() == ''){
					
					saveMinta_viGzPermintaanUmum();
					
					dsTRDetailUmumGzPermintaanUmum.removeAll();
					GzPermintaanUmum.vars.status_order = 'false';
					
					Ext.getCmp('txtNoPermintaanGzPermintaanUmumL').disable();
					Ext.getCmp('dfTglMintaGzPermintaanUmumL').disable();
					Ext.getCmp('cbo_UnitGzPermintaanUmumLookup').disable();
					Ext.getCmp('dfTglUntukGzPermintaanUmumL').disable();
					Ext.getCmp('cbo_AhliGiziGzPermintaanUmumLookup').disable();
					Ext.getCmp('txtKetGZPenerimaanL').disable();
				} else{
					records.push(new dsDataGrdUmum_viGzPermintaanUmum.recordType());
					dsDataGrdUmum_viGzPermintaanUmum.add(records);
					
					dsTRDetailUmumGzPermintaanUmum.removeAll();
					GzPermintaanUmum.vars.status_order = 'false';
				}*/


			}
		}, {
			xtype: 'tbseparator'
		}, {
			xtype: 'button',
			text: 'Delete',
			iconCls: 'remove',
			id: 'btnDelete_viInvPenerimaanInventaris',
			handler: function () {
				var line = InvPenerimaanInventaris.form.Grid.a.getSelectionModel().selection.cell[0];
				var o = dsDataGrdUmum_PenerimaanInventaris_Inv.getRange()[line].data;
				if (dsDataGrdUmum_PenerimaanInventaris_Inv.getCount() > 1) {
					Ext.Msg.confirm('Warning', 'Apakah yakin akan dihapus ?', function (button) {
						if (button == 'yes') {
							if (dsDataGrdUmum_PenerimaanInventaris_Inv.getRange()[line].data.kd_inv != undefined) {
								Ext.Ajax.request({
										url: baseURL + "index.php/inventaris/functionPenerimaanInventaris/deletePenerimaan",
										params: {
											no_terima: Ext.getCmp('txtNoPenerimaanInventaris_Inv').getValue(),
											no_urut_brg: noUrutBrgPenerimaanInv
										},
										failure: function (o) {
											ShowPesanErrorMintaUmum('Error, delete! Hubungi Admin', 'Error');
										},
										success: function (o) {
											var cst = Ext.decode(o.responseText);
											if (cst.success === true) {
												dsDataGrdUmum_PenerimaanInventaris_Inv.removeAt(line);
												InvPenerimaanInventaris.form.Grid.a.getView().refresh();
												ShowPesanSuksesMintaUmum('Berhasil menghapus data ini', 'Information');

											} else if (cst.success === false && cst.checking === false) {
												ShowPesanErrorMintaUmum('Data tidak ada, Hubungi Admin', 'Error');
											} else {
												ShowPesanErrorMintaUmum('Gagal menghapus data ini', 'Error');
											};
										}
									}

								)
							} else {
								dsDataGrdUmum_PenerimaanInventaris_Inv.removeAt(line);
								varNoUrutPenerimaanInv = parseInt(varNoUrutPenerimaanInv) - 1;
								InvPenerimaanInventaris.form.Grid.a.getView().refresh();

							}
						}

					});
				} else {
					//ShowPesanErrorGzPermintaanUmum('Data tidak bisa dihapus karena minimal data 1', 'Error');
					// y
					ShowPesanErrorMintaUmum('Data tidak bisa dihapus karena minimal data 1', 'Error');
					// end y
				}

			}
		}],
		items: [{
				labelWidth: 105,
				layout: 'form',
				labelAlign: 'Left',
				border: false,
				items: [
					gridDataViewEdit_PenerimaanInventaris_Inv(),

				]
			}

		],

	};
	return items;
};


//var a={};
function gridDataViewEdit_PenerimaanInventaris_Inv() {
	var FieldGrdKasir_PenerimaanInventaris_Inv = ['no_terima', 'kd_vendor', 'vendor', 'tgl_terima', 'tgl_posting', 'no_urut_brg', 'nama_brg', 'no_urut', 'satuan', 'jumlah_in', 'harga_beli', 'status', 'sub_total', 'keterangan'];
	dsDataGrdUmum_PenerimaanInventaris_Inv = new WebApp.DataStore({
		fields: FieldGrdKasir_PenerimaanInventaris_Inv
	});

	InvPenerimaanInventaris.form.Grid.a = new Ext.grid.EditorGridPanel({
		store: dsDataGrdUmum_PenerimaanInventaris_Inv,
		height: 250,
		columnLines: true,
		selModel: new Ext.grid.CellSelectionModel({
			singleSelect: true,
			listeners: {
				cellselect: function (sm, row, rec) {
					noUrutBrgPenerimaanInv = 0;
					noUrutBrgPenerimaanInv = sm.selection.record.data.no_urut_brg;
					/* rowSelectedGridUmum_PenerimaanInventaris_Inv = undefined;
					rowSelectedGridUmum_PenerimaanInventaris_Inv = dsDataGrdUmum_PenerimaanInventaris_Inv.getAt(row);
					CurrentDataUmum_PenerimaanInventaris_Inv
					CurrentDataUmum_PenerimaanInventaris_Inv.row = row;
					CurrentDataUmum_PenerimaanInventaris_Inv.data = rowSelectedGridUmum_PenerimaanInventaris_Inv.data;
					
					dsTRDetailUmumPenerimaanInventaris_Inv.removeAll();
					getGridWaktu(Ext.getCmp('txtTmpNoPenerimaanPenerimaanInventaris_InvL').getValue(),CurrentDataUmum_PenerimaanInventaris_Inv.data.kd_pasien);
					
					if(PenerimaanInventaris_Inv.vars.status_order == 'false'){
						Ext.getCmp('btnAddDetUmumPenerimaanInventaris_InvL').enable();
						Ext.getCmp('btnDeleteDet_PenerimaanInventaris_Inv').enable();
					} else{
						Ext.getCmp('btnAddUmumPenerimaanInventaris_InvL').disable();
						Ext.getCmp('btnAddDetUmumPenerimaanInventaris_InvL').disable();
					}
					Ext.getCmp('txtTmpKdUmumPenerimaanInventaris_InvL').setValue(CurrentDataUmum_PenerimaanInventaris_Inv.data.kd_pasien);
					 */
				},
			}
		}),
		stripeRows: true,
		cm: new Ext.grid.ColumnModel([
			new Ext.grid.RowNumberer(),
			{
				header: 'Kd. Kelompok',
				dataIndex: 'kd_inv',
				sortable: true,
				width: 35,


			},
			//-------------- ## --------------
			{
				header: 'No Urut Barang',
				dataIndex: 'no_urut_brg',
				sortable: true,
				width: 30
			},
			{
				header: 'No Urut',
				dataIndex: 'no_urut',
				sortable: true,
				width: 30
			},
			{
				header: 'Nama Barang',
				dataIndex: 'nama_brg',
				sortable: true,
				width: 130,
				editor: new Nci.form.Combobox.autoComplete({
					store: InvPenerimaanInventaris.form.ArrayStore.penerimaaninv,
					select: function (a, b, c) {

						var line = InvPenerimaanInventaris.form.Grid.a.getSelectionModel().selection.cell[0];
						var no_urut = dsDataGrdUmum_PenerimaanInventaris_Inv.getRange()[line].data.no_urut;
						var grandTotal = toInteger(Ext.getCmp('txtGrandTotal_InvPenerimaanInvL').getValue());

						if (no_urut === undefined) {
							varNoUrutPenerimaanInv = parseInt(varNoUrutPenerimaanInv) + 1;
							dsDataGrdUmum_PenerimaanInventaris_Inv.getRange()[line].data.no_urut = varNoUrutPenerimaanInv;
						}
						dsDataGrdUmum_PenerimaanInventaris_Inv.getRange()[line].data.kd_inv = b.data.kd_inv;
						dsDataGrdUmum_PenerimaanInventaris_Inv.getRange()[line].data.no_urut_brg = b.data.no_urut_brg;
						dsDataGrdUmum_PenerimaanInventaris_Inv.getRange()[line].data.nama_brg = b.data.nama_brg;
						dsDataGrdUmum_PenerimaanInventaris_Inv.data.items[line].data.satuan = b.data.satuan;
						dsDataGrdUmum_PenerimaanInventaris_Inv.data.items[line].data.jumlah_in = 0;
						dsDataGrdUmum_PenerimaanInventaris_Inv.data.items[line].data.harga_beli = 0;
						dsDataGrdUmum_PenerimaanInventaris_Inv.data.items[line].data.sub_total = 0;
						if (grandTotal === '') {
							Ext.getCmp('txtGrandTotal_InvPenerimaanInvL').setValue(0);
						}
						InvPenerimaanInventaris.form.Grid.a.getView().refresh();
					},
					insert: function (o) {
						return {
							no_urut_brg: o.no_urut_brg,
							kd_inv: o.kd_inv,
							nama_brg: o.nama_brg,
							satuan: o.satuan,
							text: '<table style="font-size: 11px;"><tr><td width="80">' + o.kd_inv + '</td><td width="50">' + o.no_urut_brg + '</td><td width="250">' + o.nama_brg + '</td></tr></table>'
						}
					},
					url: baseURL + "index.php/inventaris/functionPenerimaanInventaris/getBarang",
					valueField: 'nama_brg',
					displayField: 'text',
					listWidth: 400
				})
			},
			//-------------- ## --------------
			{
				header: 'Satuan',
				dataIndex: 'satuan',
				width: 30,
				sortable: true,
			},
			//-------------- ## --------------
			{
				header: 'Jumlah',
				dataIndex: 'jumlah_in',
				xtype: 'numbercolumn',
				align: 'right',
				sortable: true,
				width: 30,
				editor: new Ext.form.NumberField({
					allowBlank: true,
					enableKeyEvents: true,
					listeners: {
						blur: function (a) {
							var line = this.index;
							var jumlah = dsDataGrdUmum_PenerimaanInventaris_Inv.data.items[line].data.jumlah_in;
							var harga = dsDataGrdUmum_PenerimaanInventaris_Inv.data.items[line].data.harga_beli;
							dsDataGrdUmum_PenerimaanInventaris_Inv.getRange()[line].set("sub_total", harga * jumlah);
							totalSemuaBrgterima = totalSemuaBrgterima + parseInt(dsDataGrdUmum_PenerimaanInventaris_Inv.data.items[line].data.sub_total);
							Ext.getCmp('txtGrandTotal_InvPenerimaanInvL').setValue(toFormat(totalSemuaBrgterima));
						},
						focus: function (a) {
							this.index = InvPenerimaanInventaris.form.Grid.a.getSelectionModel().selection.cell[0]
						},
					}
				})
			},
			{
				header: 'Harga',
				dataIndex: 'harga_beli',
				xtype: 'numbercolumn',
				align: 'right',
				sortable: true,
				width: 55,
				editor: new Ext.form.NumberField({
					allowBlank: true,
					enableKeyEvents: true,
					listeners: {
						blur: function (a) {
							var line = this.index;
							var jumlah = dsDataGrdUmum_PenerimaanInventaris_Inv.data.items[line].data.jumlah_in;
							var harga = dsDataGrdUmum_PenerimaanInventaris_Inv.data.items[line].data.harga_beli;
							dsDataGrdUmum_PenerimaanInventaris_Inv.getRange()[line].set("sub_total", harga * jumlah);
							totalSemuaBrgterima = totalSemuaBrgterima + parseInt(dsDataGrdUmum_PenerimaanInventaris_Inv.data.items[line].data.sub_total);
							Ext.getCmp('txtGrandTotal_InvPenerimaanInvL').setValue(toFormat(totalSemuaBrgterima));

						},
						focus: function (a) {
							this.index = InvPenerimaanInventaris.form.Grid.a.getSelectionModel().selection.cell[0]
						},
					}
				})
			},
			{
				header: 'Sub Total',
				dataIndex: 'sub_total',
				xtype: 'numbercolumn',
				align: 'right',
				sortable: true,
				width: 55,
				//value:'ooo'
			},
		]),
		viewConfig: {
			forceFit: true
		}
	});
	return InvPenerimaanInventaris.form.Grid.a;
}

function save_dataTerimaInv() {
	if (ValidasiEntriTerimaInv(nmHeaderSimpanData, false) == 1) {
		//console.log(InvPenerimaanInventaris.form.Grid.a);
		Ext.Ajax.request({
				url: baseURL + "index.php/inventaris/functionPenerimaanInventaris/save",
				params: ParameterSaveTerimaInv(),
				failure: function (o) {
					ShowPesanErrorMintaUmum('Hubungi Admin', 'Error');
				},
				success: function (o) {
					var cst = Ext.decode(o.responseText);
					if (cst.success === true && cst.simpan === true) {
						ShowPesanSuksesMintaUmum(nmPesanSimpanSukses, nmHeaderSimpanData);
						//Ext.getCmp('TxtKdAhliGz').setValue(cst.kodeahliinventaris);
						// y
						noTerimaLookUp = '';
						noTerimaLookUp = Ext.getCmp('txtNoPenerimaanInventaris_Inv').getValue();
						// end y
						dataGriAwalPenerimaanInvLookup(rowdata = undefined);
						// y
						//Ext.getCmp('txtNoPenerimaanInventaris_Inv').setValue(varNoOtoTerima);
						//end y
						Ext.getCmp('btnPosting_viInvPenerimaanInventaris_Inv').enable();
						// y contoh menampilkan pesan
						// } else if (cst.success === false) {
						// 	ShowPesanErrorMintaUmum(cst.pesan, 'Error');
						// end y
					} else {
						ShowPesanErrorMintaUmum("Gagal Menyimpan data Ini", 'Error');
					};
				}
			}

		)
	}
}

// y
function update_dataTerimaInv() {
	if (ValidasiEntriTerimaInvY(nmHeaderSimpanData, false) == 1) {
		//console.log(InvPenerimaanInventaris.form.Grid.a);
		Ext.Ajax.request({
				url: baseURL + "index.php/inventaris/functionPenerimaanInventaris/updated",
				params: ParameterSaveTerimaInv(),
				failure: function (o) {
					ShowPesanErrorMintaUmum('Hubungi Admin', 'Error');
				},
				success: function (o) {
					var cst = Ext.decode(o.responseText);
					if (cst.success === true && cst.simpan === true) {
						ShowPesanSuksesMintaUmum('Data Berhasil Di Update', 'Informasi');
						//Ext.getCmp('TxtKdAhliGz').setValue(cst.kodeahliinventaris);
						// y
						//noTerimaLookUp = '';
						//noTerimaLookUp = Ext.getCmp('txtNoPenerimaanInventaris_Inv').getValue();
						// end y
						dataGriAwalPenerimaanInvLookup(rowdata = undefined);
						// y
						//Ext.getCmp('txtNoPenerimaanInventaris_Inv').setValue(varNoOtoTerima);
						//end y
						Ext.getCmp('btnPosting_viInvPenerimaanInventaris_Inv').enable();
					} else {
						ShowPesanErrorMintaUmum("Gagal Menyimpan data Ini", 'Error');
					};
				}
			}

		)
	}
}
// end y

function ParameterSaveTerimaInv() {
	/*var noterima_txt=Ext.getCmp('txtNoPenerimaanInventaris_Inv').getValue();
		if (noterima_txt==='')
		{
			getNoTerimaOtomatisPenerimaanInv();
			
		}*/
	var params = {
		//Table:'vPenerimaanInventaris',
		jumlahrecord: dsDataGrdUmum_PenerimaanInventaris_Inv.getCount(),
		p_noterima: Ext.getCmp('txtNoPenerimaanInventaris_Inv').getValue(),
		p_kdvendor: kodeVendorLookUpForCmbBoxInv,
		p_tglterima: Ext.getCmp('tglPenerimaanInventaris_Inv').getValue(),
		p_nofaktur: Ext.getCmp('txtNoFakturPenerimaanInventaris_Inv').getValue(),
		p_keterangan: Ext.getCmp('TxtKeteranganPenerimaanFilterGridDataView_PermintaanInventaris_Inv').getValue(),
		p_nospk: Ext.getCmp('txtNoSPKPenerimaanInventaris_Inv').getValue(),
		tglspk: Ext.getCmp('tglSPKPenerimaanInventaris_Inv').getValue(),

	}
	for (var i = 0; i < dsDataGrdUmum_PenerimaanInventaris_Inv.getCount(); i++) {
		params['kodeKelompokTerimaInv-' + i] = dsDataGrdUmum_PenerimaanInventaris_Inv.getRange()[i].data.kd_inv;
		params['noUrutBrgTerimaInv-' + i] = dsDataGrdUmum_PenerimaanInventaris_Inv.data.items[i].data.no_urut_brg;
		params['noUrutTerimaInv-' + i] = dsDataGrdUmum_PenerimaanInventaris_Inv.data.items[i].data.no_urut;
		params['namaBrgterimaInv-' + i] = dsDataGrdUmum_PenerimaanInventaris_Inv.data.items[i].data.nama_brg;
		params['satuanTerimaInv-' + i] = dsDataGrdUmum_PenerimaanInventaris_Inv.data.items[i].data.satuan;
		params['jumlahTerimaInv-' + i] = dsDataGrdUmum_PenerimaanInventaris_Inv.data.items[i].data.jumlah_in;
		params['hargaTerimaInv-' + i] = dsDataGrdUmum_PenerimaanInventaris_Inv.data.items[i].data.harga_beli;
		params['subTotalTerimaInv-' + i] = dsDataGrdUmum_PenerimaanInventaris_Inv.data.items[i].data.sub_total;
	}
	return params;
}

function ValidasiEntriTerimaInv(modul, mBolHapus) {
	//var kode = Ext.getCmp('TxtKdAhliGz').getValue();
	var x = 1;
	var nominta_validasi = Ext.getCmp('txtNoPenerimaanInventaris_Inv').getValue();
	/*var keterangan = Ext.getCmp('TxtKetPenerimaanInventarisL').getValue();
	var nama = Ext.getCmp('TxNamaMintaUmumL').getValue();
	var alamat = Ext.getCmp('TxAlamatMintaUmumL').getValue();
	//var x = 1;
	/*if(kode === '' ){
	ShowPesanErrorAhliinventaris('Data Belum Diisi Lengkap', 'Warning');
	x = 0;
	}*/
	console.log(nominta_validasi);
	if( nominta_validasi.trim()==='')
	{
		ShowPesanErrorMintaUmum('Nomor penerimaan harap diisi', 'Warning');
		x = 0;
	}
	

	var datagridterimainventaris = dsDataGrdUmum_PenerimaanInventaris_Inv.getCount();
	if (datagridterimainventaris == 0) {
		ShowPesanErrorMintaUmum('Data Belum Diisi Lengkap', 'Warning');
		x = 0;
	}
	for (var i = 0; i < dsDataGrdUmum_PenerimaanInventaris_Inv.getCount(); i++) {
		var o = dsDataGrdUmum_PenerimaanInventaris_Inv.getRange()[i].data;
		if (o.kd_inv != undefined) {
			for (var j = i + 1; j < dsDataGrdUmum_PenerimaanInventaris_Inv.getCount(); j++) {
				var p = dsDataGrdUmum_PenerimaanInventaris_Inv.getRange()[j].data;
				if (p.no_urut_brg == o.no_urut_brg) {
					ShowPesanErrorMintaUmum('Barang Tidak Boleh Sama', 'Warning');
					x = 0;
				}
			}
		} else {
			ShowPesanErrorMintaUmum('Data Belum Diisi Lengkap', 'Warning');
			x = 0;
		}

	}
	return x;
};

// y
function ValidasiEntriTerimaInvY(modul, mBolHapus) {

	var x = 1;
	var datagridterimainventaris = dsDataGrdUmum_PenerimaanInventaris_Inv.getCount();

	if (Ext.get('txtNoPenerimaanInventaris_Inv').getValue() === '') {
		x = 0;
		if (mBolHapus === false) {
			ShowPesanErrorMintaUmum("No Penerimaan Wajib Terisi", 'Warning');
		}
	} else if (datagridterimainventaris == 0) {
		ShowPesanErrorMintaUmum('Data Belum Diisi Lengkap', 'Warning');
		x = 0;
	} else {
		for (var i = 0; i < dsDataGrdUmum_PenerimaanInventaris_Inv.getCount(); i++) {
			var o = dsDataGrdUmum_PenerimaanInventaris_Inv.getRange()[i].data;
			if (o.kd_inv != undefined) {
				for (var j = i + 1; j < dsDataGrdUmum_PenerimaanInventaris_Inv.getCount(); j++) {
					var p = dsDataGrdUmum_PenerimaanInventaris_Inv.getRange()[j].data;
					if (p.no_urut_brg == o.no_urut_brg) {
						ShowPesanErrorMintaUmum('Barang Tidak Boleh Sama', 'Warning');
						x = 0;
					}
				}
			} else {
				ShowPesanErrorMintaUmum('Data Belum Diisi Lengkap', 'Warning');
				x = 0;
			}
		}
	}
	return x;
};
// end y

function posting_DataTerimaPenerimaanInv() {
	Ext.Msg.confirm('Warning', 'Apakah data ini akan diPosting? Pastikan semua data sudah benar sebelum diPosting', function (button) {
		if (button == 'yes') {
			Ext.Ajax.request({
					url: baseURL + "index.php/inventaris/functionPenerimaanInventaris/posting",
					params: ParameterPostingTerimaInv(),
					failure: function (o) {
						ShowPesanErrorMintaUmum('Hubungi Admin', 'Error');
					},
					success: function (o) {
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) {
							ShowPesanSuksesMintaUmum('Posting Berhasil', nmHeaderSimpanData);
							noTerimaLookUp = '';
							noTerimaLookUp = Ext.getCmp('txtNoPenerimaanInventaris_Inv').getValue();
							dataGriAwalPenerimaanInvLookup(rowdata = undefined);
							Ext.getCmp('txtNoPenerimaanInventaris_Inv').disable();
							Ext.getCmp('txtNoFakturPenerimaanInventaris_Inv').disable();
							Ext.getCmp('txtNoSPKPenerimaanInventaris_Inv').disable();
							Ext.getCmp('cmbFindVendorPenerimaanInventaris_Inv').disable();
							Ext.getCmp('txtGrandTotal_InvPenerimaanInvL').disable();
							Ext.getCmp('tglSPKPenerimaanInventaris_Inv').disable();
							Ext.getCmp('tglPenerimaanInventaris_Inv').disable();
							Ext.getCmp('TxtKeteranganPenerimaanFilterGridDataView_PermintaanInventaris_Inv').disable();
							Ext.getCmp('btnAdd_PenerimaanInventaris_Inv').disable();
							Ext.getCmp('btnSave_PenerimaanInventaris_Inv').disable();
							Ext.getCmp('btnDeletePenerimaan_PenerimaanInventaris_Inv').disable();
							Ext.getCmp('btnUnPosting_viInvPenerimaanInventaris_Inv').show();
							Ext.getCmp('btnAddBarangInvPenerimaanInventarisL').disable();
							Ext.getCmp('btnDelete_viInvPenerimaanInventaris').disable();
							InvPenerimaanInventaris.form.Grid.a.disable();
						} else {
							ShowPesanErrorMintaUmum('Gagal Menyimpan Data ini', 'Error');
						};
					}
				}

			)
		}
	});

}

// y
function unposting_DataTerimaPenerimaanInv() {

	Ext.Msg.confirm('Warning', 'Apakah anda yakin akan membatalkan data posting ini?', function (button) {
		if (button == 'yes') {
			Ext.Ajax.request({
					url: baseURL + "index.php/inventaris/functionPenerimaanInventaris/unposting",
					params: ParameterPostingTerimaInv(),
					failure: function (o) {
						ShowPesanErrorMintaUmum('Hubungi Admin', 'Error');
					},
					success: function (o) {
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) {
							ShowPesanSuksesMintaUmum('Unposting Berhasil', 'Informasi');
							noTerimaLookUp = '';
							noTerimaLookUp = Ext.getCmp('txtNoPenerimaanInventaris_Inv').getValue();
							dataGriAwalPenerimaanInvLookup(rowdata = undefined);
							Ext.getCmp('txtNoPenerimaanInventaris_Inv').enable();
							Ext.getCmp('txtNoFakturPenerimaanInventaris_Inv').enable();
							Ext.getCmp('txtNoSPKPenerimaanInventaris_Inv').enable();
							Ext.getCmp('cmbFindVendorPenerimaanInventaris_Inv').enable();
							Ext.getCmp('txtGrandTotal_InvPenerimaanInvL').enable();
							Ext.getCmp('tglSPKPenerimaanInventaris_Inv').enable();
							Ext.getCmp('tglPenerimaanInventaris_Inv').enable();
							Ext.getCmp('TxtKeteranganPenerimaanFilterGridDataView_PermintaanInventaris_Inv').enable();
							Ext.getCmp('btnAdd_PenerimaanInventaris_Inv').enable();
							Ext.getCmp('btnSave_PenerimaanInventaris_Inv').enable();
							Ext.getCmp('btnDeletePenerimaan_PenerimaanInventaris_Inv').enable();
							Ext.getCmp('btnPosting_viInvPenerimaanInventaris_Inv').show();
							Ext.getCmp('btnAddBarangInvPenerimaanInventarisL').enable();
							Ext.getCmp('btnDelete_viInvPenerimaanInventaris').enable();
							//Ext.getCmp('btnDelete_viInvPenerimaanInventaris').show();
							InvPenerimaanInventaris.form.Grid.a.enable();
						} else {
							ShowPesanErrorMintaUmum('Gagal Menyimpan Data ini', 'Error');
						};
					}
				}

			)
		}
	});

}
// end y

function ParameterPostingTerimaInv() {
	var params = {
		//Table:'vPenerimaanInventaris',
		jumlahrecord: dsDataGrdUmum_PenerimaanInventaris_Inv.getCount(),
		p_noterima: Ext.getCmp('txtNoPenerimaanInventaris_Inv').getValue(),
		p_tglterima: Ext.getCmp('tglPenerimaanInventaris_Inv').getValue()
	}
	for (var i = 0; i < dsDataGrdUmum_PenerimaanInventaris_Inv.getCount(); i++) {
		params['kodeKelompokTerimaInv-' + i] = dsDataGrdUmum_PenerimaanInventaris_Inv.getRange()[i].data.kd_inv;
		params['noUrutBrgTerimaInv-' + i] = dsDataGrdUmum_PenerimaanInventaris_Inv.data.items[i].data.no_urut_brg;
		params['jumlahTerimaInv-' + i] = dsDataGrdUmum_PenerimaanInventaris_Inv.data.items[i].data.jumlah_in;
	}
	return params;
}

function ShowPesanSuksesMintaUmum(str, modul) {
	Ext.MessageBox.show({
		title: modul,
		msg: str,
		buttons: Ext.MessageBox.OK,
		icon: Ext.MessageBox.INFO,
		width: 250
	});
};

function ShowPesanErrorMintaUmum(str, modul) {
	Ext.MessageBox.show({
		title: modul,
		msg: str,
		buttons: Ext.MessageBox.OK,
		icon: Ext.MessageBox.ERROR,
		width: 290
	});
};





function ComboAhliinventarisPenerimaanInventaris_InvLookup() {
	var Field_ahliinventarisLookup = ['KD_AHLI_inventaris', 'NAMA_AHLI_inventaris'];
	ds_ahliinventarisGZPenerimaanLookup = new WebApp.DataStore({
		fields: Field_ahliinventarisLookup
	});
	ds_ahliinventarisGZPenerimaanLookup.load({
		params: {
			Skip: 0,
			Take: 1000,
			Sort: 'KD_AHLI_inventaris',
			Sortdir: 'ASC',
			target: 'ComboAhliinventaris',
			param: ''
		}
	});

	var cbo_AhliinventarisPenerimaanInventaris_InvLookup = new Ext.form.ComboBox({
		x: 420,
		y: 0,
		flex: 1,
		id: 'cbo_AhliinventarisPenerimaanInventaris_InvLookup',
		valueField: 'KD_AHLI_inventaris',
		displayField: 'NAMA_AHLI_inventaris',
		store: ds_ahliinventarisGZPenerimaanLookup,
		mode: 'local',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		width: 150,
		tabIndex: 2,
		listeners: {
			'select': function (a, b, c) {
				Ext.getCmp('btnAddUmumPenerimaanInventaris_InvL').enable();
			},
			'specialkey': function () {
				if (Ext.EventObject.getKey() === 13) {

				}
			}
		}
	})
	return cbo_AhliinventarisPenerimaanInventaris_InvLookup;
};

function ComboAhliinventarisPenerimaanInventaris_Inv() {
	var Field_ahliinventaris = ['KD_AHLI_inventaris', 'NAMA_AHLI_inventaris'];
	ds_ahliinventarisGZPenerimaan = new WebApp.DataStore({
		fields: Field_ahliinventaris
	});
	ds_ahliinventarisGZPenerimaan.load({
		params: {
			Skip: 0,
			Take: 1000,
			Sort: 'KD_AHLI_inventaris',
			Sortdir: 'ASC',
			target: 'ComboAhliinventaris',
			param: ''
		}
	});

	var cbo_AhliinventarisPenerimaanInventaris_Inv = new Ext.form.ComboBox({
		x: 130,
		y: 30,
		flex: 1,
		id: 'cbo_AhliinventarisPenerimaanInventaris_Inv',
		valueField: 'KD_AHLI_inventaris',
		displayField: 'NAMA_AHLI_inventaris',
		store: ds_ahliinventarisGZPenerimaan,
		mode: 'local',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		width: 150,
		tabIndex: 2,
		listeners: {
			'select': function (a, b, c) {

			},
			'specialkey': function () {
				if (Ext.EventObject.getKey() === 13) {
					tmpkriteria = getCriteriaCariPenerimaanInventaris_Inv();
					refreshPenerimaanInventaris(tmpkriteria);
				}
			}
		}
	})
	return cbo_AhliinventarisPenerimaanInventaris_Inv;
};


function ComboUnitPenerimaanInventaris_Inv() {
	var Field_Vendor = ['KD_UNIT', 'NAMA_UNIT'];
	ds_unit = new WebApp.DataStore({
		fields: Field_Vendor
	});
	ds_unit.load({
		params: {
			Skip: 0,
			Take: 1000,
			Sort: 'kd_unit',
			Sortdir: 'ASC',
			target: 'ComboUnitApotek',
			param: "parent='100' ORDER BY nama_unit"
		}
	});
	var cbo_UnitPenerimaanInventaris_Inv = new Ext.form.ComboBox({
		x: 410,
		y: 0,
		flex: 1,
		id: 'cbo_UnitPenerimaanInventaris_Inv',
		valueField: 'KD_UNIT',
		displayField: 'NAMA_UNIT',
		emptyText: 'Unit/Ruang',
		store: ds_unit,
		mode: 'local',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		width: 150,
		tabIndex: 2,
		listeners: {
			'select': function (a, b, c) {
				selectSetUnit = b.data.displayText;
			},
			'specialkey': function () {
				if (Ext.EventObject.getKey() === 13) {
					tmpkriteria = getCriteriaCariPenerimaanInventaris_Inv();
					refreshPenerimaanInventaris(tmpkriteria);
				}
			}
		}
	})
	return cbo_UnitPenerimaanInventaris_Inv;
}

function ComboUnitPenerimaanInventaris_InvLookup() {
	var Field_Vendor = ['KD_UNIT', 'NAMA_UNIT'];
	ds_unitL = new WebApp.DataStore({
		fields: Field_Vendor
	});
	ds_unitL.load({
		params: {
			Skip: 0,
			Take: 1000,
			Sort: 'kd_unit',
			Sortdir: 'ASC',
			target: 'ComboUnitApotek',
			param: "parent='100' ORDER BY nama_unit"
		}
	});

	var cbo_UnitPenerimaanInventaris_InvLookup = new Ext.form.ComboBox({
		x: 130,
		y: 60,
		id: 'cbo_UnitPenerimaanInventaris_InvLookup',
		store: ds_unitL,
		valueField: 'KD_UNIT',
		displayField: 'NAMA_UNIT',
		mode: 'local',
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		width: 150,
		tabIndex: 3,
		listeners: {
			'select': function (a, b, c) {

			},
			'specialkey': function () {
				if (Ext.EventObject.getKey() === 13) {

				}
			}
		}
	})
	return cbo_UnitPenerimaanInventaris_InvLookup;
}

function datainit_PenerimaanInventaris_Inv(rowdata) {
	var tgl_terima2 = rowdata.tgl_terima.split(" ");
	Ext.getCmp('txtNoPenerimaanInventaris_Inv').setValue(rowdata.no_terima);
};

function dataGriAwalPenerimaanInv() {
	Ext.Ajax.request({
			url: baseURL + "index.php/inventaris/functionPenerimaanInventaris/getDataGridAwal",
			params: {
				text: ''
			},
			failure: function (o) {
				ShowPesanErrorPenerimaanInventaris_Inv('Hubungi Admin', 'Error');
			},
			success: function (o) {
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) {
					dataSource_PenerimaanInventaris_Inv.removeAll();
					var recs = [],
						recType = dataSource_PenerimaanInventaris_Inv.recordType;

					for (var i = 0; i < cst.ListDataObj.length; i++) {

						recs.push(new recType(cst.ListDataObj[i]));

					}
					dataSource_PenerimaanInventaris_Inv.add(recs);



					GridDataView_PenerimaanInventaris_Inv.getView().refresh();
				} else {
					ShowPesanErrorPenerimaanInventaris_Inv('Gagal membaca data ', 'Error');
				};
			}
		}

	)

}



function getGridUmum(no_minta) {
	Ext.Ajax.request({
			url: baseURL + "index.php/inventaris/functionPenerimaanInventaris/getGridUmum",
			params: {
				nominta: no_minta
			},
			failure: function (o) {
				ShowPesanErrorPenerimaanInventaris_Inv('Error, pasien! Hubungi Admin', 'Error');
			},
			success: function (o) {
				var cst = Ext.decode(o.responseText);

				if (cst.success === true) {

					var recs = [],
						recType = dsDataGrdUmum_PenerimaanInventaris_Inv.recordType;

					for (var i = 0; i < cst.ListDataObj.length; i++) {

						recs.push(new recType(cst.ListDataObj[i]));

					}
					dsDataGrdUmum_PenerimaanInventaris_Inv.add(recs);

					PenerimaanInventaris_Inv.form.Grid.a.getView().refresh();
				} else {
					ShowPesanErrorPenerimaanInventaris_Inv('Gagal membaca data pasien ini', 'Error');
				};
			}
		}

	)

}

function getGridWaktu(no_minta, kd_pasien) {
	Ext.Ajax.request({
			url: baseURL + "index.php/inventaris/functionPenerimaanInventaris/getGridWaktu",
			params: {
				nominta: no_minta,
				kdpasien: kd_pasien
			},
			failure: function (o) {
				ShowPesanErrorPenerimaanInventaris_Inv('Error, detail diet! Hubungi Admin', 'Error');
			},
			success: function (o) {
				var cst = Ext.decode(o.responseText);

				if (cst.success === true) {
					var recs = [],
						recType = dsTRDetailUmumPenerimaanInventaris_Inv.recordType;

					for (var i = 0; i < cst.ListDataObj.length; i++) {

						recs.push(new recType(cst.ListDataObj[i]));

					}
					dsTRDetailUmumPenerimaanInventaris_Inv.add(recs);

					gridDTLUmum_PenerimaanInventaris_Inv.getView().refresh();
				} else {
					ShowPesanErrorPenerimaanInventaris_Inv('Gagal membaca data detail diet pasien ini', 'Error');
				};
			}
		}

	)

}


function cekOrder(no_minta) {
	Ext.Ajax.request({
			url: baseURL + "index.php/inventaris/functionPenerimaanInventaris/cekOrderPenerimaanInventaris",
			params: {
				nominta: no_minta
			},
			failure: function (o) {
				ShowPesanErrorPenerimaanInventaris_Inv('Error, cek order! Hubungi Admin', 'Error');
			},
			success: function (o) {
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) {
					Ext.getCmp('btnAddUmumPenerimaanInventaris_InvL').enable();
					Ext.getCmp('btnDelete_PenerimaanInventaris_Inv').enable();
					Ext.getCmp('btnDeletePenerimaan_PenerimaanInventaris_Inv').enable();
					PenerimaanInventaris_Inv.vars.status_order = 'false';
				} else {
					ShowPesanInfoPenerimaanInventaris_Inv('Penerimaan diet ini sudah di order', 'Information');
					Ext.getCmp('btnAddUmumPenerimaanInventaris_InvL').disable();
					Ext.getCmp('btnDelete_PenerimaanInventaris_Inv').disable();
					Ext.getCmp('btnDeletePenerimaan_PenerimaanInventaris_Inv').disable();
					Ext.getCmp('btnSave_PenerimaanInventaris_Inv').disable();
					PenerimaanInventaris_Inv.vars.status_order = 'true';
				};
			}
		}

	)
}

function saveMinta_PenerimaanInventaris_Inv() {
	if (ValidasiEntryPenerimaanPenerimaanInventaris_Inv(nmHeaderSimpanData, false) == 1) {
		Ext.Ajax.request({
			url: baseURL + "index.php/inventaris/functionPenerimaanInventaris/saveMinta",
			params: getParamMintaPenerimaanInventaris_Inv(),
			failure: function (o) {
				loadMask.hide();
				ShowPesanErrorPenerimaanInventaris_Inv('Error, Penerimaan! Hubungi Admin', 'Error');
			},
			success: function (o) {
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) {
					loadMask.hide();
					Ext.getCmp('txtTmpNoPenerimaanPenerimaanInventaris_InvL').setValue(cst.nominta);
					Ext.getCmp('txtNoPenerimaanPenerimaanInventaris_InvL').setValue(cst.nominta);
				} else {
					loadMask.hide();
					ShowPesanErrorPenerimaanInventaris_Inv('Gagal menyimpan data Penerimaan', 'Error');
				};
			}
		})
	}
}


function save_PenerimaanInventaris_Inv() {
	Ext.Ajax.request({
		url: baseURL + "index.php/inventaris/functionPenerimaanInventaris/saveMintaUmum",
		params: getParamMintaUmumPenerimaanInventaris_Inv(),
		failure: function (o) {
			loadMask.hide();
			ShowPesanErrorPenerimaanInventaris_Inv('Error, pasien! Hubungi Admin', 'Error');
		},
		success: function (o) {
			var cst = Ext.decode(o.responseText);
			if (cst.success === true) {
				loadMask.hide();
				Ext.getCmp('txtTmpKdUmumPenerimaanInventaris_InvL').setValue(cst.kdpasien);
				if (cst.error == 'ada') {
					ShowPesanWarningPenerimaanInventaris_Inv('Umum ini susah ada dalam transaksi pemintaan diet hari ini, hapus pasien ini untuk melanjutkan', 'Warning');
					Ext.getCmp('btnDelete_PenerimaanInventaris_Inv').enable();
					Ext.getCmp('btnAddUmumPenerimaanInventaris_InvL').disable();
					Ext.getCmp('btnAddDetUmumPenerimaanInventaris_InvL').disable();
				} else {
					if (PenerimaanInventaris_Inv.vars.status_order == 'false') {
						Ext.getCmp('btnAddUmumPenerimaanInventaris_InvL').disable();
						Ext.getCmp('btnDelete_PenerimaanInventaris_Inv').disable();
						Ext.getCmp('btnAddDetUmumPenerimaanInventaris_InvL').enable();
					} else {
						Ext.getCmp('btnAddUmumPenerimaanInventaris_InvL').disable();
						Ext.getCmp('btnDelete_PenerimaanInventaris_Inv').disable();
						Ext.getCmp('btnAddDetUmumPenerimaanInventaris_InvL').disable();
					}
				}
			} else {
				loadMask.hide();
				ShowPesanErrorPenerimaanInventaris_Inv('Gagal menyimpan data pasien', 'Error');
			};
		}
	})
}

function saveDetailUmum_PenerimaanInventaris_Inv() {
	Ext.Ajax.request({
		url: baseURL + "index.php/inventaris/functionPenerimaanInventaris/saveMintaUmumDetail",
		params: getParamDetailUmumPenerimaanInventaris_Inv(),
		failure: function (o) {
			loadMask.hide();
			ShowPesanErrorPenerimaanInventaris_Inv('Error, detail diet! Hubungi Admin', 'Error');
		},
		success: function (o) {
			var cst = Ext.decode(o.responseText);
			if (cst.success === true) {

				if (cst.ada == '0') {
					loadMask.hide();
					dataGriAwalPenerimaanInv()
					Ext.getCmp('btnAddUmumPenerimaanInventaris_InvL').enable();
					Ext.getCmp('btnDeleteDet_PenerimaanInventaris_Inv').enable();
					Ext.getCmp('txtNoPenerimaanPenerimaanInventaris_InvL').setValue(Ext.getCmp('txtTmpNoPenerimaanPenerimaanInventaris_InvL').getValue());
				} else {
					loadMask.hide();
					ShowPesanWarningPenerimaanInventaris_Inv('Jenis diet ini sudah ada dengan waktu yang sama, hapus salah satu untuk melanjutkan', 'Error');
					Ext.getCmp('btnAddUmumPenerimaanInventaris_InvL').disable();
					Ext.getCmp('btnAddDetUmumPenerimaanInventaris_InvL').disable();
					Ext.getCmp('btnDeleteDet_PenerimaanInventaris_Inv').enable();
				}
			} else {
				loadMask.hide();
				ShowPesanErrorPenerimaanInventaris_Inv('Gagal menyimpan data detail diet', 'Error');
			};
		}
	})
}


function printbill() {
	Ext.Ajax.request({
		url: baseURL + "index.php/main/CreateDataObj",
		params: datacetakbill(),
		failure: function (o) {
			ShowPesanErrorPenerimaanInventaris_Inv('Error hubungi admin', 'Error');
		},
		success: function (o) {
			var cst = Ext.decode(o.responseText);
			if (cst.success === true) {

			} else if (cst.success === false && cst.pesan === 0) {
				ShowPesanWarningPenerimaanInventaris_Inv('Data tidak berhasil di simpan ' + cst.pesan, 'Simpan Data');
			} else {
				ShowPesanErrorPenerimaanInventaris_Inv('Gagal print ' + 'Error');
			}
		}
	})
}

function getParamMintaPenerimaanInventaris_Inv() {
	var params = {
		NoMinta: Ext.getCmp('txtNoPenerimaanPenerimaanInventaris_InvL').getValue(),
		KdUnit: Ext.getCmp('cbo_UnitPenerimaanInventaris_InvLookup').getValue(),
		TglMinta: Ext.getCmp('dfTglMintaPenerimaanInventaris_InvL').getValue(),
		TglMakan: Ext.getCmp('dfTglUntukPenerimaanInventaris_InvL').getValue(),
		Ahliinventaris: Ext.getCmp('cbo_AhliinventarisPenerimaanInventaris_InvLookup').getValue(),
		Ket: Ext.getCmp('txtKetGZPenerimaanL').getValue()
	};
	return params
};

function getParamMintaUmumPenerimaanInventaris_Inv() {
	var params = {
		NoMinta: Ext.getCmp('txtTmpNoPenerimaanPenerimaanInventaris_InvL').getValue(),
		KdUnit: Ext.getCmp('cbo_UnitPenerimaanInventaris_InvLookup').getValue(),
		KdUmum: PenerimaanInventaris_Inv.vars.kd_pasien,
		TglMasuk: PenerimaanInventaris_Inv.vars.tgl_masuk,
		NoKamar: PenerimaanInventaris_Inv.vars.no_kamar
	};

	return params
};

function getParamDetailUmumPenerimaanInventaris_Inv() {
	var params = {
		NoMinta: Ext.getCmp('txtTmpNoPenerimaanPenerimaanInventaris_InvL').getValue(),
		KdUmum: Ext.getCmp('txtTmpKdUmumPenerimaanInventaris_InvL').getValue(),
		kd_jenis: PenerimaanInventaris_Inv.vars.kd_jenis,
		kd_waktu: PenerimaanInventaris_Inv.vars.kd_waktu
	};

	return params
};

function datacetakbill() {
	var params = {
		Table: 'billprintingPenerimaanInventaris_Inv',
		NoResep: Ext.getCmp('txtNoResepPenerimaanInventaris_InvL').getValue(),
		NoOut: Ext.getCmp('txtTmpNooutPenerimaanInventaris_InvL').getValue(),
		TglOut: Ext.getCmp('txtTmpTgloutPenerimaanInventaris_InvL').getValue(),
		KdUmum: Ext.getCmp('txtKdUmumPenerimaanInventaris_InvL').getValue(),
		NamaUmum: PenerimaanInventaris_Inv.form.ComboBox.namaUmum.getValue(),
		JenisUmum: Ext.get('cboPilihankelompokUmumAptPenerimaanInventaris_Inv').getValue(),
		Kelas: Ext.get('cbo_UnitPenerimaanInventaris_InvL').getValue(),
		Dokter: Ext.get('cbo_DokterPenerimaanInventaris_Inv').getValue(),
		Total: Ext.get('txtTotalBayarPenerimaanInventaris_InvL').getValue(),
		Tot: toInteger(Ext.get('txtTotalBayarPenerimaanInventaris_InvL').getValue())

	}
	params['jumlah'] = dsDataGrdUmum_PenerimaanInventaris_Inv.getCount();
	for (var i = 0; i < dsDataGrdUmum_PenerimaanInventaris_Inv.getCount(); i++) {
		params['nama_-' + i] = dsDataGrdUmum_PenerimaanInventaris_Inv.data.items[i].data.nama_;
		params['jml-' + i] = dsDataGrdUmum_PenerimaanInventaris_Inv.data.items[i].data.jml;
	}

	return params
}

function totalBayar() {


	for (var i = 0; i < dsDataGrdUmum_PenerimaanInventaris_Inv.getCount(); i++) {
		var total = 0;
		var o = dsDataGrdUmum_PenerimaanInventaris_Inv.getRange()[i].data;
		if (o.harga != undefined) {

			total = parseFloat(o.harga) * parseFloat(o.jumlah);

			o.totjumlah = total;
			console.log(total);
			console.log(o.totjumlah);
		}
	}
}

function getCriteriaCariPenerimaanInventaris_Inv() //^^^
{
	var strKriteria = "";

	if (Ext.getCmp('TxtNoPenerimaanFilterGridDataView_PenerimaanInventaris_Inv').getValue() != "") {
		strKriteria = " no_minta LIKE upper('" + Ext.getCmp('TxtNoPenerimaanFilterGridDataView_PenerimaanInventaris_Inv').getValue() + "%')";
	}

	if (Ext.get('TxtNamaPenerimaanFilterGridDataView_PenerimaanInventaris_Inv').getValue() != "") //^^^
	{
		if (strKriteria == "") {
			strKriteria = " lower(nama) LIKE lower('" + Ext.get('TxtNamaPenerimaanFilterGridDataView_PenerimaanInventaris_Inv').getValue() + "%')";
		} else {
			strKriteria += " AND lower(nama) LIKE lower('" + Ext.get('TxtNamaPenerimaanFilterGridDataView_PenerimaanInventaris_Inv').getValue() + "%')";
		}
	}

	if (Ext.get('dfTglAwalPenerimaanInventaris_Inv').getValue() != "" && Ext.get('dfTglAkhirPenerimaanInventaris_Inv').getValue() != "") {
		if (strKriteria == "") {
			strKriteria = " tgl_minta between '" + Ext.get('dfTglAwalPenerimaanInventaris_Inv').getValue() + "' and '" + Ext.get('dfTglAkhirPenerimaanInventaris_Inv').getValue() + "'";
		} else {
			strKriteria += " and tgl_minta between '" + Ext.get('dfTglAwalPenerimaanInventaris_Inv').getValue() + "' and '" + Ext.get('dfTglAkhirPenerimaanInventaris_Inv').getValue() + "'";
		}

	}

	strKriteria = strKriteria + " ORDER BY no_minta";
	return strKriteria;
}

function ValidasiEntryPenerimaanPenerimaanInventaris_Inv(modul, mBolHapus) {
	var x = 1;
	if (Ext.getCmp('cbo_UnitPenerimaanInventaris_InvLookup').getValue() === '' || Ext.getCmp('cbo_AhliinventarisPenerimaanInventaris_InvLookup').getValue() === '' || (Ext.getCmp('dfTglUntukPenerimaanInventaris_InvL').getValue() < Ext.getCmp('dfTglMintaPenerimaanInventaris_InvL').getValue())) {
		if (Ext.getCmp('cbo_UnitPenerimaanInventaris_InvLookup').getValue() === '') {
			loadMask.hide();
			ShowPesanWarningPenerimaanInventaris_Inv('Unit tidak boleh kosong', 'Warning');
			x = 0;
		} else if (Ext.getCmp('cbo_AhliinventarisPenerimaanInventaris_InvLookup').getValue() === '') {
			loadMask.hide();
			ShowPesanWarningPenerimaanInventaris_Inv('Ahli inventaris tidak boleh kosong', 'Warning');
			x = 0;
		} else {
			loadMask.hide();
			ShowPesanWarningPenerimaanInventaris_Inv('Untuk tanggal tidak boleh kurang dari tanggal Penerimaan', 'Warning');
			x = 0;
		}
	}
	return x;
};

function dataAddNew_PenerimaanInventaris_Inv() {
	// y
	//Ext.getCmp('txtNoPenerimaanInventaris_Inv').setValue('');
	Ext.getCmp('btnUnPosting_viInvPenerimaanInventaris_Inv').hide();
	// end y
	//y
	getNoTerimaOtomatisPenerimaanInv();
	// end y
	varNoUrutPenerimaanInv = 0;
	Ext.getCmp('txtNoFakturPenerimaanInventaris_Inv').setValue('');
	Ext.getCmp('tglFakturPenerimaanInventaris_Inv').setValue(now_PenerimaanInventaris_Inv);
	Ext.getCmp('tglSPKPenerimaanInventaris_Inv').setValue(now_PenerimaanInventaris_Inv);
	Ext.getCmp('cmbFindVendorPenerimaanInventaris_Inv').setValue('');
	Ext.getCmp('TxtKeteranganPenerimaanFilterGridDataView_PermintaanInventaris_Inv').setValue('');
	Ext.getCmp('txtNoSPKPenerimaanInventaris_Inv').setValue('');
	dsDataGrdUmum_PenerimaanInventaris_Inv.removeAll();
}

function getNoTerimaOtomatisPenerimaanInv() {
	Ext.Ajax.request({
			url: baseURL + "index.php/inventaris/functionPenerimaanInventaris/getnoterimaotomatis",
			params: {
				no_terima: getNoTerimaOto
			},
			failure: function (o) {
				loadMask.hide();
				ShowPesanErrorPenerimaanInventaris_Inv('Error mendapatkan no terima otomatis! Hubungi Admin', 'Error');
			},
			success: function (o) {
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) {
					varNoOtoTerima = cst.noterimaotomatis;
					//Ext.getCmp('txtNoPenerimaanInventaris_Inv').setValue(varNoOtoTerima);
				} else {
					ShowPesanWarningPenerimaanInventaris_Inv('Gagal mendapatkan no minta otomatis', 'Warning');
				};
			}
		}

	)
	return varNoOtoTerima;
}

function ShowPesanWarningPenerimaanInventaris_Inv(str, modul) {
	Ext.MessageBox.show({
		title: modul,
		msg: str,
		buttons: Ext.MessageBox.OK,
		icon: Ext.MessageBox.WARNING,
		width: 290
	});
};

function ShowPesanErrorPenerimaanInventaris_Inv(str, modul) {
	Ext.MessageBox.show({
		title: modul,
		msg: str,
		buttons: Ext.MessageBox.OK,
		icon: Ext.MessageBox.ERROR,
		width: 290
	});
};


function ShowPesanInfoPenerimaanInventaris_Inv(str, modul) {
	Ext.MessageBox.show({
		title: modul,
		msg: str,
		buttons: Ext.MessageBox.OK,
		icon: Ext.MessageBox.INFO,
		width: 290
	});
};