var dataSource_RuangTempatInventaris_Inv;
var dsCekKondisiDataRuangTempat_Inventaris_Inv;
var dsvComboGedungRuangTempatInv;
var dsvComboDeptRuangTempatInv;
var selectCount_RuangTempatInventaris_Inv=50;
var NamaForm_RuangTempatInventaris_Inv="Distribusi Inventaris";
var selectCountStatusPostingInvRuangTempatInventaris='Semua';
var mod_name_RuangTempatInventaris_Inv="RuangTempatInventaris_Inv";
var now_RuangTempatInventaris_Inv= new Date();
var addNew_RuangTempatInventaris_Inv;
var rowSelected_RuangTempatInventaris_Inv;
var setLookUps_RuangTempatInventaris_Inv;
var tanggalMasuk = now_RuangTempatInventaris_Inv.format("Y-m-d 00:00:00");
var tanggal = now_RuangTempatInventaris_Inv.format("d/M/Y");
var jam = now_RuangTempatInventaris_Inv.format("H/i/s");
var thnGetNoGetRuangOto= now_RuangTempatInventaris_Inv.format("Y");
var tmpkriteria;
var DataGridObat;
var DataGridSubObat;
var DataGridSatuanBesar;
var DataGridSatuan;
var DataGridGolongan;
var lokasiRuangTempat;
var JmlRuangTempatInv;
var jumlahRecRuangTempat='';
var jumlahCekKondisi='';
var noRegRuangCekKondisi;
var noRegisterCekKondisi;
var CurrentData_RuangTempatInventaris_Inv =
{
	data: Object,
	details: Array,
	row: 0
};


var InvRuangTempatInventaris={};
InvRuangTempatInventaris.form={};
InvRuangTempatInventaris.func={};
InvRuangTempatInventaris.vars={};
InvRuangTempatInventaris.func.parent=InvRuangTempatInventaris;
InvRuangTempatInventaris.form.ArrayStore={};
InvRuangTempatInventaris.form.ComboBox={};
InvRuangTempatInventaris.form.DataStore={};
InvRuangTempatInventaris.form.Record={};
InvRuangTempatInventaris.form.Form={};
InvRuangTempatInventaris.form.Grid={};
InvRuangTempatInventaris.form.Panel={};
InvRuangTempatInventaris.form.TextField={};
InvRuangTempatInventaris.form.Button={};

InvRuangTempatInventaris.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_milik', 'milik'],
	data: []
});
InvRuangTempatInventaris.form.ArrayStore.lokasiTempat= new Ext.data.ArrayStore({
	id: 0,
	fields:[],
	data: []
});

InvRuangTempatInventaris.form.ArrayStore.golongan= new Ext.data.ArrayStore({
	id: 0,
	fields:['apt_kd_golongan', 'apt_golongan'],
	data: []
});

InvRuangTempatInventaris.form.ArrayStore.satuan= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_satuan', 'satuan'],
	data: []
});

InvRuangTempatInventaris.form.ArrayStore.satuanBesar= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_sat_besar', 'keterangan'],
	data: []
});


CurrentPage.page = dataGrid_RuangTempatInventaris_Inv(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dataGrid_RuangTempatInventaris_Inv(mod_id_RuangTempatInventaris_Inv){	
 	var PanelTabInvRuangTempatInventaris = new Ext.FormPanel({
        labelAlign: 'top',
		layout:'form',
        frame:true,
        title: '',
        bodyStyle:'padding:5px 5px 0',
        //width: 600,
        items: [getPenelItemSetup_RuangTempatInventaris_Inv()]	
		
	})
	// Kriteria filter pada Grid # --------------
    var FrmData_RuangTempatInventaris_Inv = new Ext.Panel
    (
		{
			title: NamaForm_RuangTempatInventaris_Inv,
			iconCls: 'Studi_Lanjut',
			id: mod_id_RuangTempatInventaris_Inv,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ PanelTabInvRuangTempatInventaris],
					//GridDataView_RuangTempatInventaris_Inv],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_RuangTempatInventaris_Inv,
		            columns: 21,
		            defaults: {
					scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_RuangTempatInventaris_Inv;
    //-------------- # End form filter # --------------
}
function dsComboGedungRuangTempatInv()
{
	dsvComboGedungRuangTempatInv.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'vComboGedungRuangTempatInv',
                    //param : 'inv_kd_kondisi =~'+kode+'~'
                }			
            }
        );   
    return dsvComboGedungRuangTempatInv;
}
function funcNamaGedungRuangTempatInventaris_Inv()
{
    var Field =['kd_jns_lokasi','jns_lokasi'];
    dsvComboGedungRuangTempatInv = new WebApp.DataStore({fields: Field});
	dsComboGedungRuangTempatInv();
	var funcNamaGedungRuangTempatInventaris = new Ext.form.ComboBox
	(
		{
			x: 130,
			y: 10,
			id:'cmbNamaGedungRuangTempatInventaris_Inv',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			/*readOnly:true,
			emptyText:'Set Tarif',
			value: 1,*/
			width: 260,
			store: dsvComboGedungRuangTempatInv,
			valueField: 'kd_jns_lokasi',
			displayField: 'jns_lokasi',
			//value:selectSetPilihankelompokPasien,
			listeners:
			{
				'select': function(a,b,c)
				{
					////.log(b.data.jns_lokasi)
					lokasiRuangTempat=b.data.jns_lokasi;
					dsComboDeptRuangTempatInv(b.data.kd_jns_lokasi)
					////.log(c)
				}
			}
		}
	);
	return funcNamaGedungRuangTempatInventaris;
}
function dsComboDeptRuangTempatInv(kodejenis)
{
	dsvComboDeptRuangTempatInv.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'vComboDeptRuangTempatInv',
                    param : 'il.kd_jns_lokasi =~'+kodejenis+'~'
                }			
            }
        );   
    return dsvComboDeptRuangTempatInv;
}
function funcDeptRuangTempatInventaris_Inv()
{
	var Field =['kd_jns_lokasi','kd_lokasi','kode_lokasi','lokasi'];
    dsvComboDeptRuangTempatInv = new WebApp.DataStore({fields: Field});
	dsComboDeptRuangTempatInv();
	var funcDeptRuangTempatInventaris = new Ext.form.ComboBox
	(
		{
			x: 130,
			y: 40,
			id:'cmbDeptRuangTempatInventaris_Inv',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			/*readOnly:true,
			emptyText:'Set Tarif',
			value: 1,*/
			width: 260,
			store: dsvComboDeptRuangTempatInv,
			valueField: 'kd_lokasi',
			displayField: 'lokasi',
			//value:selectSetPilihankelompokPasien,
			listeners:
			{
				'select': function(a,b,c)
				{
					dsRuangTempatInventaris(b.data.kode_lokasi)
					Ext.Ajax.request
					(
						{
							url: baseURL + "index.php/inventaris/functionruangtempatlokasiinventaris/getData",
							params: {
									no_ruang:b.data.kode_lokasi,
									} ,
							failure: function(o)
									{
									ShowPesanErrorMintaUmum('Error, Hubungi Admin', 'Error');
									},	
							success: function(o) 
									{
									var cst = Ext.decode(o.responseText);
									if (cst.success === true) 
									{
										Ext.getCmp('TxtKeteranganRuangTempatFilterGridDataView_RuangTempatInventaris_Inv').setValue(cst.ket);
									}
									else if (cst.success === false)
									{
										ShowPesanErrorMintaUmum('Data tidak ada, Hubungi Admin', 'Error');
									};
							}
						}
					)
				}
			}
		}
	);
	return funcDeptRuangTempatInventaris;
}
function getPenelItemSetup_RuangTempatInventaris_Inv(lebar)
{
    var items =
	{
		xtype:'tabpanel',
		plain:true,
		activeTab: 0,
		height:600,
		defaults: {
			bodyStyle:'padding:5px',
			autoScroll: false
	    },
	    items:[
				PanelRuang(),
				//PanelCekKondisi()
		]
	}
    return items;
};

//sub form tab1
function PanelRuang(rowdata){
	var items = 
	{
		title:'Distribusi',
		layout:'form',
		border: false,
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 100,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 10,
								xtype: 'label',
								text: 'Nama Gedung'
							},
							{
								x: 120,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							funcNamaGedungRuangTempatInventaris_Inv(),
							
							{
								x: 10,
								y: 40,
								xtype: 'label',
								text: 'Dept / Ruang'
							},
							{
								x: 120,
								y: 40,
								xtype: 'label',
								text: ':'
							},
							funcDeptRuangTempatInventaris_Inv(),
							{
								x: 400,
								y: 10,
								xtype: 'label',
								text: 'Keterangan'
							},
							{
								x: 465,
								y: 10,
								xtype: 'label',
								text: ':'
							},{
							x: 475,
							y: 10,
							xtype: 'textarea',
							id: 'TxtKeteranganRuangTempatFilterGridDataView_RuangTempatInventaris_Inv',
							name: 'TxtKeteranganRuangTempatFilterGridDataView_RuangTempatInventaris_Inv',
							emptyText: '',
							width: 260,
							tabIndex:1,
							
						},
						
						]
					}
				]
			},
			{
				
						layout: 'form',
						//bodyStyle:'padding: 5px',
						border: true,
						tbar:
						[
							{
								text	: 'Add Barang',
								id		: 'btnAddBarangInvRuangTempatInventarisL',
								tooltip	: nmLookup,
								iconCls	: 'find',
								handler	: function(){
									var records = new Array();
									records.push(new dataSource_RuangTempatInventaris_Inv.recordType());
									dataSource_RuangTempatInventaris_Inv.add(records);
									////.log(dataSource_RuangTempatInventaris_Inv.getCount()-1);
									//getNoRegRuangOtomatis(dataSource_RuangTempatInventaris_Inv.getCount()-1);
								}
							},{
								xtype: 'tbseparator'
							},{
								xtype: 'button',
								text: 'Delete',
								iconCls: 'remove',
								id: 'btnDelete_viInvRuangTempatInventaris',
								handler: function()
								{
									var line = DataGridRuangTempatInventaris.getSelectionModel().selection.cell[0];
									var o = dataSource_RuangTempatInventaris_Inv.getRange()[line].data;
									if(dataSource_RuangTempatInventaris_Inv.getCount()>1){
										if(dataSource_RuangTempatInventaris_Inv.getRange()[line].data.no_reg_ruang != undefined){
													ShowPesanWarningInvRuangTempatInventaris('Tidak bisa menghapus data ini', 'Perhatian');			
										}else{
											Ext.Msg.confirm('Warning', 'Apakah yakin akan dihapus ?', function(button){
												if (button == 'yes'){
													
														dataSource_RuangTempatInventaris_Inv.removeAt(line);
														DataGridRuangTempatInventaris.getView().refresh();
														
													
												} 
												
											});
										}
									} else{
										ShowPesanErrorInvRuangTempatInventaris('Data tidak bisa dihapus karena minimal data 1','Error');
									}
									
								}
							}	
						],
						items:
						[
							gridRuang_RuangTempatInventaris()
						]
					
				
				
			}
		],
		tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAdd_RuangTempatInventaris_Inv',
						handler: function(){
							Ext.getCmp('cmbNamaGedungRuangTempatInventaris_Inv').setValue('');
							Ext.getCmp('cmbDeptRuangTempatInventaris_Inv').setValue('');
							Ext.getCmp('TxtKeteranganRuangTempatFilterGridDataView_RuangTempatInventaris_Inv').setValue('');
							dataSource_RuangTempatInventaris_Inv.reload();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_RuangTempatInventaris_Inv',
						handler: function()
						{
							datasave_RuangTempatInventaris_Inv();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_RuangTempatInventaris_Inv',
						handler: function()
						{
							dataSource_RuangTempatInventaris_Inv.reload();
							//dsDataGrd_viSetupMasterObat.reload();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Cek Kondisi',
						iconCls: 'find',
						id: 'btnCekKon_RuangTempatInventaris_Inv',
						handler: function()
						{
							//setLookUp_CekKondisiRuangTempatInventaris_Inv();
							
							if(dataSource_RuangTempatInventaris_Inv.getCount()!=0){
								var line = DataGridRuangTempatInventaris.getSelectionModel().selection.cell[0];
								var o = dataSource_RuangTempatInventaris_Inv.getRange()[line].data;
								////.log(dataSource_RuangTempatInventaris_Inv.getRange()[line].data);
								
							if (dataSource_RuangTempatInventaris_Inv.getRange()[line].data.no_reg_ruang != undefined)	
							{
								if(dataSource_RuangTempatInventaris_Inv.getRange()[line].data.kd_inv != undefined){
									Ext.Ajax.request
									(
										{
											url: baseURL + "index.php/inventaris/vigridcekkondisibarang/getData",
											params: {
													no_reg_ruang:dataSource_RuangTempatInventaris_Inv.getRange()[line].data.no_reg_ruang,
													//no_urut_brg:noUrutBrgPenerimaanInv
													} ,
											failure: function(o)
											{
												ShowPesanErrorInvRuangTempatInventaris('Error, delete! Hubungi Admin', 'Error');
											},	
											success: function(o) 
											{
												var cst = Ext.decode(o.responseText);
												if (cst.success === true) 
												{
													setLookUp_CekKondisiRuangTempatInventaris_Inv();
													noRegRuangCekKondisi=dataSource_RuangTempatInventaris_Inv.getRange()[line].data.no_reg_ruang;
													noRegisterCekKondisi=dataSource_RuangTempatInventaris_Inv.getRange()[line].data.no_register;
													dsCekKondisiRuangTempatInventaris(dataSource_RuangTempatInventaris_Inv.getRange()[line].data.no_reg_ruang);
													Ext.getCmp('txtNomorCekKondisi_InvRuangTempatInventarisL').setValue(Ext.getCmp('cmbNamaGedungRuangTempatInventaris_Inv').getValue()+Ext.getCmp('cmbDeptRuangTempatInventaris_Inv').getValue());
													Ext.getCmp('txtLokasiTempatCekKondisi_InvRuangTempatInventarisL').setValue(lokasiRuangTempat);
													Ext.getCmp('txtNamaBarangRuangTempatInventaris_Inv').setValue(dataSource_RuangTempatInventaris_Inv.getRange()[line].data.nama_brg);
													Ext.getCmp('txtNoInventarisRuangTempatInventaris_Inv').setValue(dataSource_RuangTempatInventaris_Inv.getRange()[line].data.kd_inv);
													Ext.getCmp('txtUrutanRuangTempatInventaris_Inv').setValue(dataSource_RuangTempatInventaris_Inv.getRange()[line].data.no_urut_brg);
													Ext.getCmp('TxtJumlahRuangTempatFilterGridDataView_RuangTempatInventaris_Inv').setValue(dataSource_RuangTempatInventaris_Inv.getRange()[line].data.jumlah);
												}
												else if (cst.success === false)
												{
													ShowPesanErrorInvRuangTempatInventaris('Data tidak ada, Hubungi Admin', 'Error');
												}
											}
										}
														
									)
								}else{
									setLookUp_CekKondisiRuangTempatInventaris_Inv();
								}
							}else
							{
								ShowPesanErrorInvRuangTempatInventaris('Simpan data terlebih dahulu','Error')
							}
								
							} else{
								ShowPesanErrorInvRuangTempatInventaris('Pilih data terlebih dahulu','Error');
							}
							
						}
					},
					
				]
			}
    };
        return items;
}
function setLookUp_CekKondisiRuangTempatInventaris_Inv(rowdata){
    var lebar = 985;
    setLookUps_CekKondisiRuangTempatInventaris_Inv = new Ext.Window({
        id: Nci.getId(),
        title: 'Cek Kondisi', 
        closeAction: 'destroy',        
        width: 900,
        height: 450,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_CekKondisiRuangTempatInventaris_Inv(lebar,rowdata),
        listeners:{
            activate: function(){
				
            },
            afterShow: function(){
                this.activate();
            },
            deactivate: function(){
                rowSelected_RuangTempatInventaris_Inv=undefined;
            }
        }
    });

    setLookUps_CekKondisiRuangTempatInventaris_Inv.show();
    if (rowdata == undefined){	
    }
    else
    {
        //datainit_BarangInventaris_Inv(rowdata);
    }
}
function getFormItemEntry_CekKondisiRuangTempatInventaris_Inv(lebar,rowdata){
    var pnlFormDataBasic_CekKondisiRuangTempatInventaris_Inv = new Ext.FormPanel({
		title: '',
		region: 'north',
		layout: 'form',
		bodyStyle: 'padding:10px 10px 10px 10px',
		anchor: '100%',
		labelWidth: 1,
		autoWidth: true,
		width: lebar,
		border: false,
		tbar: {
				xtype: 'toolbar',
				items: 
				[
				
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpanKondisi_RuangTempatInventaris_Inv',
						handler: function()
						{
							datasaveCekKondisi_RuangTempatInventaris_Inv();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefreshKondisi_RuangTempatInventaris_Inv',
						handler: function()
						{
							dsCekKondisiDataRuangTempat_Inventaris_Inv.reload();
							//dsDataGrd_viSetupMasterObat.reload();
						}
					},
					
				]
			},
		items:[
				getItemPanelCekKondisi_RuangTempatInventaris_Inv(lebar),
				gridCekKondisi_RuangTempatInventaris(lebar,rowdata),
			],
			fileUpload: true,
			
		}
    )

    return pnlFormDataBasic_CekKondisiRuangTempatInventaris_Inv;
}

//sub form tab2
function getItemPanelCekKondisi_RuangTempatInventaris_Inv() {
    var items = 
	{
		layout:'form',
		border: true,
		//bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						//bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 150,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 10,
								xtype: 'label',
								text: 'Nomor'
							},
							{
								x: 120,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 10,
								xtype: 'textfield',
								name: 'txtNomorCekKondisi_InvRuangTempatInventarisL',
								id: 'txtNomorCekKondisi_InvRuangTempatInventarisL',
								readOnly:true,
								//tabIndex:1,
								width: 150
							},
							{
								x: 10,
								y: 40,
								xtype: 'label',
								text: 'Lokasi / Tempat :'
							},
							{
								x: 120,
								y: 40,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 40,
								xtype: 'textfield',
								name: 'txtLokasiTempatCekKondisi_InvRuangTempatInventarisL',
								id: 'txtLokasiTempatCekKondisi_InvRuangTempatInventarisL',
								readOnly:true,
								tabIndex:1,
								width: 250
							},
							{
								x: 10,
								y: 70,
								xtype: 'label',
								text: 'Nama Barang'
							},
							{
								x: 120,
								y: 70,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 70,
								xtype: 'textfield',
								id: 'txtNamaBarangRuangTempatInventaris_Inv',
								name: 'txtNamaBarangRuangTempatInventaris_Inv',
								width: 250,
								readOnly:true,
								tabIndex:1,
							},
							{
								x: 10,
								y: 100,
								xtype: 'label',
								text: 'No. Inventaris'
							},
							{
								x: 120,
								y: 100,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 100,
								xtype: 'textfield',
								id: 'txtNoInventarisRuangTempatInventaris_Inv',
								name: 'txtNoInventarisRuangTempatInventaris_Inv',
								width: 150,
								readOnly:true,
								tabIndex:1,
							},
							{
								x: 385,
								y: 70,
								xtype: 'label',
								text: 'Urutan'
							},
							{
								x: 450,
								y: 70,
								xtype: 'label',
								text: ':'
							},
							{
								x: 460,
								y: 70,
								xtype: 'textfield',
								id: 'txtUrutanRuangTempatInventaris_Inv',
								name: 'txtUrutanRuangTempatInventaris_Inv',
								width: 90,
								readOnly:true,
								tabIndex:1,
							},
							{
								x: 385,
								y: 100,
								xtype: 'label',
								text: 'Jumlah'
							},
							{
								x: 450,
								y: 100,
								xtype: 'label',
								text: ':'
							},{
							x: 460,
							y: 100,
							xtype: 'numberfield',
							style: 'text-align:right;',
							id: 'TxtJumlahRuangTempatFilterGridDataView_RuangTempatInventaris_Inv',
							name: 'TxtJumlahRuangTempatFilterGridDataView_RuangTempatInventaris_Inv',
							emptyText: '',
							width: 90,
							tabIndex:1,
							readOnly: true,
						},
						
						]
					}
				]
			}
		]		
	};
    return items;
};


//sub form tab3

//------------------end---------------------------------------------------------
function dsRuangTempatInventaris(noruang)
{
	dataSource_RuangTempatInventaris_Inv.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: '',
					Sortdir: 'ASC',
					target: 'ViGridRuangTempat',
					param: 'IDR.NO_RUANG=~'+noruang+'~ order by tgl_masuk ASC'
				}
		}
	);
	return dataSource_RuangTempatInventaris_Inv
}
function getJumlahRuangTempat(kdInv,noRegister)
{
	var jumlah
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/inventaris/functionruangtempatlokasiinventaris/getJumlah",
			params: {
						kdkelompok:kdInv,
						noregister:noRegister
					} ,
			failure: function(o)
					{
						ShowPesanErrorInvRuangTempatInventaris('Error, Hubungi Admin', 'Error');
					},	
			success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							
							var line = dataSource_RuangTempatInventaris_Inv.getCount()-1;
							JmlRuangTempatInv=cst.jumlah;
							alert(JmlRuangTempatInv);
						}
						else if (cst.success === false)
						{
							ShowPesanErrorInvRuangTempatInventaris('Data tidak ada, Hubungi Admin', 'Error');
						}
						//dataSource_RuangTempatInventaris_Inv.getRange()[line].data.jumlah=cst.jumlah;
					}
		}
														
	)
	return jumlah;
}
function gridRuang_RuangTempatInventaris(){
    var FieldGrd_viRuangTempatInventaris = ['no_reg_ruang', 'no_ruang','no_register','tgl_masuk','jumlah_in','jumlah','jml_kurang','jml_pindah','old_kurang','old_pindah','no_baris','kd_inv','no_urut_brg','nama_brg','nama_sub','ket','satuan'];
    
	dataSource_RuangTempatInventaris_Inv= new WebApp.DataStore({
		fields: FieldGrd_viRuangTempatInventaris
    });
	
    dsRuangTempatInventaris()
   
    DataGridRuangTempatInventaris =new Ext.grid.EditorGridPanel({
        xtype: 'editorgrid',
		id:'gridRuangTempat_Inv',
		store: dataSource_RuangTempatInventaris_Inv,
        height:600,
		width:'100%',
        autoScroll: true,
		columnLines: true,
		border:false,
		anchor: '100% 84%',
		//flex:1,
		
		listeners:
		{
			// Function saat ada event double klik maka akan muncul form view # --------------
			rowdblclick: function (sm, ridx, cidx)
			{
				
			}
			// End Function # --------------
		},
		selModel: new Ext.grid.CellSelectionModel ({
            singleSelect: true,
            listeners:{
                cellselect: function(sm, row, rec){
					//DataInitPanelObat(rowSelected_RuangTempatInventaris_Inv.data);
					//.log(dataSource_RuangTempatInventaris_Inv);
                }
            }
        }),
        stripeRows: true,
        cm:new Ext.grid.ColumnModel([
        	new Ext.grid.RowNumberer(),	
			{
				dataIndex: 'no_reg_ruang',
				header: 'No Reg Ruang',
				//xtype: 'datecolumn',
				sortable: true,
				width: 50,
				hidden: true	
			},
			{
				dataIndex: 'tgl_masuk',
				header: 'Tanggal Masuk ',
				//xtype: 'datecolumn',
				sortable: true,
				width: 50,
				renderer: function(v, params, record)
				{
					if (record.data.tgl_masuk===undefined)
						return tanggal;
					else
						return ShowDate(record.data.tgl_masuk);
				},	
			},
			{
				dataIndex: 'kd_inv',
				header: 'Kode Kelompok',
				sortable: true,
				width: 50,
				
			},
			{
				dataIndex: 'no_urut_brg',
				header: 'Urutan ',
				sortable: true,
				width: 50
			},
			{
				dataIndex: 'no_register',
				header: 'No Register ',
				sortable: true,
				width: 50,
				editor:new Nci.form.Combobox.autoComplete({
							store	: InvRuangTempatInventaris.form.ArrayStore.lokasiTempat,
							select	: function(a,b,c){
								console.log(a);
								var line	=DataGridRuangTempatInventaris.getSelectionModel().selection.cell[0];
								dataSource_RuangTempatInventaris_Inv.getRange()[line].data.jumlah=a.resp.jumlah;
								dataSource_RuangTempatInventaris_Inv.getRange()[line].data.jumlah_in=a.resp.jumlah_in;
								dataSource_RuangTempatInventaris_Inv.getRange()[line].data.tgl_masuk=tanggalMasuk;
								dataSource_RuangTempatInventaris_Inv.getRange()[line].data.kd_inv=b.data.kd_inv;
								dataSource_RuangTempatInventaris_Inv.getRange()[line].data.no_urut_brg=b.data.no_urut_brg;
								dataSource_RuangTempatInventaris_Inv.getRange()[line].data.no_register=b.data.no_register;
								dataSource_RuangTempatInventaris_Inv.getRange()[line].data.nama_brg=b.data.nama_brg;
								dataSource_RuangTempatInventaris_Inv.getRange()[line].data.nama_sub=b.data.nama_sub;
								////.log(cst);
								////.log(b);
								//sdataSource_RuangTempatInventaris_Inv.getRange()[line].data.jumlah=b.data.jumlah;
								dataSource_RuangTempatInventaris_Inv.getRange()[line].data.ket=b.data.ket;
								dataSource_RuangTempatInventaris_Inv.getRange()[line].data.satuan=b.data.satuan;
								//alert(b.data.no_register);
								DataGridRuangTempatInventaris.getView().refresh();
						},
					insert	: function(o,b,c){
						var cst = Ext.decode(o.responseText);
						return {
							kd_inv 			: o.kd_inv,
							no_register		: o.no_register,
							no_urut_brg		: o.no_urut_brg,
							nama_brg		: o.nama_brg,
							nama_sub		: o.nama_sub,
							jumlah			: o.jumlah,
							ket		        : o.ket,
							satuan			: o.satuan,
							text			:  '<table style="font-size: 11px;"><tr><td width="100">'+o.kd_inv+'</td><td width="70">'+o.no_register+'</td><td width="70">'+o.no_urut_brg+'</td><td width="100">'+o.nama_brg+'</td><td width="100">'+o.nama_sub+'</td><td width="200">'+o.ket+'</td><td width="200">'+o.satuan+'</td></tr></table>',
						}
					},
					param:function(){
							return {
								
							}
					},
					url		: baseURL + "index.php/inventaris/functionruangtempatlokasiinventaris/getAutoKomplitDataRuangTempat",
					valueField: 'no_register',
					displayField: 'text',
					listWidth: 800
				})
			},
			{
				dataIndex: 'nama_brg',
				header: 'Barang',
				width: 50
			},
			{
				dataIndex: 'nama_sub',
				header: 'Kelompok',
				width: 50
			},
			{
				dataIndex: 'satuan',
				header: 'Satuan',
				width: 50
			},
			{
				dataIndex: 'jumlah_in',
				header: 'Jumlah Terima',
				xtype: 'numbercolumn',
				width: 50,
				
			},{
				dataIndex: 'jumlah',
				header: 'Jumlah',
				xtype: 'numbercolumn',
				width: 50,
				editor: new Ext.form.NumberField({
					allowBlank: true,
					enableKeyEvents: true,
					listeners: {
						blur: function (a) {
							var line = DataGridRuangTempatInventaris.getSelectionModel().selection.cell[0];
							var jumlah = dataSource_RuangTempatInventaris_Inv.data.items[line].data.jumlah_in;
							console.log(a)
							console.log(a.value)
							console.log(jumlah)
							if (parseInt(a.value) > parseInt(jumlah)){
								console.log(true)
								ShowPesanWarningInvRuangTempatInventaris('Jumlah yang anda masukkan melebihi jumlah terima barang', 'Perhatian');		
								dataSource_RuangTempatInventaris_Inv.getRange()[line].set("jumlah",jumlah);
							}else{
								console.log(false)
							}
							/*var harga = dsDataGrdUmum_PenerimaanInventaris_Inv.data.items[line].data.harga_beli;
							dsDataGrdUmum_PenerimaanInventaris_Inv.getRange()[line].set("sub_total", harga * jumlah);
							totalSemuaBrgterima = totalSemuaBrgterima + parseInt(dsDataGrdUmum_PenerimaanInventaris_Inv.data.items[line].data.sub_total);
							Ext.getCmp('txtGrandTotal_InvPenerimaanInvL').setValue(toFormat(totalSemuaBrgterima)); */
						},
						focus: function (a) {
							//this.index = InvPenerimaanInventaris.form.Grid.a.getSelectionModel().selection.cell[0]
						},
					}
				})
			},
			{
				dataIndex: 'ket',
				header: 'Keterangan/Spek',
				width: 50
			}
			//-------------- ## --------------
        ]),
		viewConfig:{
			forceFit: true
		}
    });
    return DataGridRuangTempatInventaris;
}
function dsCekKondisiRuangTempatInventaris(noregruang)
{
	dsCekKondisiDataRuangTempat_Inventaris_Inv.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: '',
					Sortdir: 'ASC',
					target: 'ViGridCekKondisiBarang',
					param: 'no_reg_ruang=~'+noregruang+'~'
				}
		}
	);
	return dsCekKondisiDataRuangTempat_Inventaris_Inv
}
function getNoRegRuangOtomatis(line)
{
	Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/inventaris/functionruangtempatlokasiinventaris/getnoregruangoto",
				params: {noregruang: thnGetNoGetRuangOto},
				failure: function(o)
				{
					ShowPesanErrorInvRuangTempatInventaris('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						////.log(dataSource_RuangTempatInventaris_Inv.data.items[line].data.no_reg_ruang)//=cst.noregruang;
					}
					else 
					{
						ShowPesanErrorInvRuangTempatInventaris('Gagal Mendapatkan No Reg Ruang', 'Error');
						dataSource_RuangTempatInventaris_Inv.reload();
					};
				}
			}
			
		)
}
function gridCekKondisi_RuangTempatInventaris(rowdata){
    var FieldRuangTempat_Inventaris_Inv = ['no_reg_ruang', 'tgl_cek', 'jml_b', 'jml_rr', 'jml_rb', 'keterangan', 'no_baris'];
    
	dsCekKondisiDataRuangTempat_Inventaris_Inv= new WebApp.DataStore({
		fields: FieldRuangTempat_Inventaris_Inv
    });
	//var noRegRuang=rowdata.no_reg_ruang;  
    dsCekKondisiRuangTempatInventaris()
   
    DataGridCekKondisiInventaris =new Ext.grid.EditorGridPanel({
        xtype: 'editorgrid',
		store: dsCekKondisiDataRuangTempat_Inventaris_Inv,
        height:210,
		width:'100%',
        columnLines: true,
		border: true,
		flex:1,
		tbar:
						[
							{
								text	: 'Add Kondisi',
								id		: 'btnAddKondisiInvRuangTempatInventarisL',
								tooltip	: nmLookup,
								iconCls	: 'find',
								handler	: function(){
									var records = new Array();
									var line;
									records.push(new dsCekKondisiDataRuangTempat_Inventaris_Inv.recordType());
									dsCekKondisiDataRuangTempat_Inventaris_Inv.add(records);
									line = dsCekKondisiDataRuangTempat_Inventaris_Inv.getCount()-1;
									dsCekKondisiDataRuangTempat_Inventaris_Inv.getRange()[line].data.jml_b=0;
									dsCekKondisiDataRuangTempat_Inventaris_Inv.getRange()[line].data.jml_rr=0;
									dsCekKondisiDataRuangTempat_Inventaris_Inv.getRange()[line].data.jml_rb=0;
									jumlahCekKondisi=parseInt(Ext.getCmp('TxtJumlahRuangTempatFilterGridDataView_RuangTempatInventaris_Inv').getValue());
									
								}
							},{
								xtype: 'tbseparator'
							},{
								xtype: 'button',
								text: 'Delete',
								iconCls: 'remove',
								id: 'btnDeleteKondisi_viInvRuangTempatInventaris',
								handler: function()
								{
									var line = DataGridCekKondisiInventaris.getSelectionModel().selection.cell[0];
									var o = dsCekKondisiDataRuangTempat_Inventaris_Inv.getRange()[line].data;
									if(dsCekKondisiDataRuangTempat_Inventaris_Inv.getCount()>1){
										if(dsCekKondisiDataRuangTempat_Inventaris_Inv.getRange()[line].data.no_reg_ruang != undefined){
													ShowPesanWarningInvRuangTempatInventaris('Tidak bisa menghapus data ini', 'Perhatian');			
										}else{
											Ext.Msg.confirm('Warning', 'Apakah yakin akan dihapus ?', function(button){
												if (button == 'yes'){
													
														dsCekKondisiDataRuangTempat_Inventaris_Inv.removeAt(line);
														DataGridCekKondisiInventaris.getView().refresh();
												} 
												
											});
										}
									} else{
										ShowPesanErrorInvRuangTempatInventaris('Data tidak bisa dihapus karena minimal data 1','Error');
									}
									
								}
							}	
						],
		listeners:
		{
			// Function saat ada event double klik maka akan muncul form view # --------------
			/*rowdblclick: function (sm, ridx, cidx)
			{
				rowSelected_RuangTempatInventaris_Inv = dsDataRuangTempat_Inventaris_Inv.getAt(ridx);
				if (rowSelected_RuangTempatInventaris_Inv != undefined)
				{
					DataInitPanelSubObat(rowSelected_RuangTempatInventaris_Inv.data);
				}
			}*/
			// End Function # --------------
		},
		selModel: new Ext.grid.CellSelectionModel ({
            singleSelect: true,
            listeners:{
                rowselect: function(sm, row, rec){
					//DataInitPanelSubObat(rowSelected_RuangTempatInventaris_Inv.data);
                }
            }
        }),
        stripeRows: true,
        cm:new Ext.grid.ColumnModel([
        	new Ext.grid.RowNumberer(),	
			{
				dataIndex: 'no_reg_ruang',
				header: 'No Reg Ruang',
				sortable: true,
				width: 70,
				hidden: true,
				renderer: function(v, params, record)
				{
					
					if (record.data.no_reg_ruang===undefined)
					{	
						 return noRegRuangCekKondisi;
					}
					else
					{
						return record.data.no_reg_ruang;
					}
				}
				
			},
			{
				dataIndex: 'tgl_cek',
				header: 'Tanggal Cek',
				sortable: true,
				width: 70,
				renderer: function(v, params, record)
				{
					if (record.data.tgl_cek===undefined)
					{	
						 return tanggal;
					}
					else
					{
						return ShowDate(record.data.tgl_cek);
					}
				}
			},
			{
				dataIndex: 'jml_b',
				header: 'Baik',
				xtype: 'numbercolumn',
				align: 'right',
				width: 70,
				editor: new Ext.form.NumberField({
							allowBlank: false,
							listeners:{
								blur: function(a){
								},
								focus: function(a){
								}
							}
						}),
			},
			{
				dataIndex: 'jml_rr',
				header: 'Rusak Ringan',
				xtype: 'numbercolumn',
				align: 'right',
				width: 70,
				editor: new Ext.form.NumberField({
							allowBlank: false,
							listeners:{
								blur: function(a){
								},
								focus: function(a){
								}
							}
						}),		
			},
			{
				dataIndex: 'jml_rb',
				header: 'Rusak Berat',
				xtype: 'numbercolumn',
				align: 'right',
				width: 70,
				editor: new Ext.form.NumberField({
							allowBlank: false,
							listeners:{
								blur: function(a){
								},
								focus: function(a){
								}
							}
						}),		
			},
			{
				dataIndex: 'keterangan',
				header: 'Keterangan',
				width: 70,
				editor: new Ext.form.TextField({
							allowBlank: false,
							listeners:{
								blur: function(a){
								},
								focus: function(a){
								}
							}
						})	
			}
			//-------------- ## --------------
        ]),
		viewConfig:{
			forceFit: true
		}
    });
    return DataGridCekKondisiInventaris;
}

function datasave_RuangTempatInventaris_Inv(){
	if (ValidasiRuangTempat(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/inventaris/functionruangtempatlokasiinventaris/save",
				params: getParamRuangTempat(),
				failure: function(o)
				{
					ShowPesanErrorInvRuangTempatInventaris('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoInvRuangTempatInventaris(nmPesanSimpanSukses,nmHeaderSimpanData);
						dataSource_RuangTempatInventaris_Inv.reload();
					}
					else 
					{
						ShowPesanErrorInvRuangTempatInventaris('Gagal Menyimpan Data ini', 'Error');
						dataSource_RuangTempatInventaris_Inv.reload();
					};
				}
			}
			
		)
	}
}

function getParamRuangTempat(){
	var	params =
	{
		jumlahrecord:dataSource_RuangTempatInventaris_Inv.getCount(),
		tahun:thnGetNoGetRuangOto,
		kdLokasi:Ext.getCmp('cmbDeptRuangTempatInventaris_Inv').getValue(),
		KdJnsLokasi:Ext.getCmp('cmbNamaGedungRuangTempatInventaris_Inv').getValue(),
		noRuang:Ext.getCmp('cmbNamaGedungRuangTempatInventaris_Inv').getValue()+Ext.getCmp('cmbDeptRuangTempatInventaris_Inv').getValue(),
		ket:Ext.getCmp('TxtKeteranganRuangTempatFilterGridDataView_RuangTempatInventaris_Inv').getValue(),
	}
	for(var i = 0 ; i < dataSource_RuangTempatInventaris_Inv.getCount();i++)
	{
		params['tglMasuk-'+i]=dataSource_RuangTempatInventaris_Inv.data.items[i].data.tgl_masuk;
		params['kdKelompok-'+i]=dataSource_RuangTempatInventaris_Inv.data.items[i].data.kd_inv;
		params['noregister-'+i]=dataSource_RuangTempatInventaris_Inv.data.items[i].data.no_register;
		params['noregruang-'+i]=dataSource_RuangTempatInventaris_Inv.data.items[i].data.no_reg_ruang;
		params['urutan-'+i]=dataSource_RuangTempatInventaris_Inv.data.items[i].data.no_urut_brg;
		params['barang-'+i]=dataSource_RuangTempatInventaris_Inv.data.items[i].data.nama_brg;
		params['kelompok-'+i]=dataSource_RuangTempatInventaris_Inv.data.items[i].data.nama_sub;
		params['satuan-'+i]=dataSource_RuangTempatInventaris_Inv.data.items[i].data.satuan;
		params['jumlah-'+i]=dataSource_RuangTempatInventaris_Inv.data.items[i].data.jumlah;
		params['keterangan-'+i]=dataSource_RuangTempatInventaris_Inv.data.items[i].data.ket;
		
	}
    return params
};

function ValidasiRuangTempat(modul,mBolHapus){
	var x = 1;
	if(Ext.getCmp('cmbDeptRuangTempatInventaris_Inv').getValue() === '' ||Ext.getCmp('cmbNamaGedungRuangTempatInventaris_Inv').getValue() === ''){
		ShowPesanWarningInvRuangTempatInventaris('Nama gedung & ruang masih kosong', 'Warning');
		x = 0;
	} 
	for(var i = 0 ; i < dataSource_RuangTempatInventaris_Inv.getCount();i++)
	{
		var o=dataSource_RuangTempatInventaris_Inv.getRange()[i].data;
		if (o.kd_inv != undefined)
		{
			for(var j = i+1 ; j < dataSource_RuangTempatInventaris_Inv.getCount();j++)
			{
				var p=dataSource_RuangTempatInventaris_Inv.getRange()[j].data;
				if (p.no_register==o.no_register && p.no_urut_brg==o.no_urut_brg)
				{
					ShowPesanWarningInvRuangTempatInventaris('Barang Tidak Boleh Sama', 'Warning');
					x = 0;
				}
			}
		}
		else
		{
			ShowPesanWarningInvRuangTempatInventaris('Data Belum Diisi Lengkap', 'Warning');
			x = 0;
		}
		
	}
	return x;
};

function datasaveCekKondisi_RuangTempatInventaris_Inv(){
	if (ValidasiCekKondisiRuangTempat(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/inventaris/functionruangtempatlokasiinventaris/saveKondisi",
				params: getParamCekKondisiRuangTempat(),
				failure: function(o)
				{
					ShowPesanErrorInvRuangTempatInventaris('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoInvRuangTempatInventaris(nmPesanSimpanSukses,nmHeaderSimpanData);
						dsCekKondisiDataRuangTempat_Inventaris_Inv.reload();
					}
					else 
					{
						ShowPesanErrorInvRuangTempatInventaris('Gagal Menyimpan Data ini', 'Error');
						dsCekKondisiDataRuangTempat_Inventaris_Inv.reload();
					};
				}
			}
			
		)
	}
}

function getParamCekKondisiRuangTempat(){
	var	params=
	{
		jumlahrecord:dsCekKondisiDataRuangTempat_Inventaris_Inv.getCount(),
		noregister:noRegisterCekKondisi,
		nourutbrg:Ext.getCmp('txtUrutanRuangTempatInventaris_Inv').getValue(),
		noRuang:Ext.getCmp('cmbNamaGedungRuangTempatInventaris_Inv').getValue()+Ext.getCmp('cmbDeptRuangTempatInventaris_Inv').getValue(),
	}
	
   	for(var i = 0 ; i < dsCekKondisiDataRuangTempat_Inventaris_Inv.getCount();i++)
	{
		params['NoRegRuang-'+i]=noRegRuangCekKondisi;
		params['tglCek-'+i]=tanggal;
		params['Baik-'+i]=dsCekKondisiDataRuangTempat_Inventaris_Inv.data.items[i].data.jml_b;
		params['RusakRingan-'+i]=dsCekKondisiDataRuangTempat_Inventaris_Inv.data.items[i].data.jml_rr;
		params['RusakBerat-'+i]=dsCekKondisiDataRuangTempat_Inventaris_Inv.data.items[i].data.jml_rb;
		params['Keterangan-'+i]=dsCekKondisiDataRuangTempat_Inventaris_Inv.data.items[i].data.keterangan;
	}
    return params
};

function ValidasiCekKondisiRuangTempat(modul,mBolHapus){
	var x = 1;
	for(var i = 0 ; i < dsCekKondisiDataRuangTempat_Inventaris_Inv.getCount();i++)
	{
		var o=dsCekKondisiDataRuangTempat_Inventaris_Inv.getRange()[i].data;
		var a=parseInt(Ext.getCmp('TxtJumlahRuangTempatFilterGridDataView_RuangTempatInventaris_Inv').getValue());
		if ( ( parseInt(o.jml_b)+parseInt(o.jml_rr)+parseInt(o.jml_rb) )  > a)
		{
			ShowPesanWarningInvRuangTempatInventaris('Jumlah kondisi melebihi jumlah barang', 'Warning');
			x = 0;
		}
		
	}
	return x;
}; 
//------------------------------Sub  Obat----------------------------------------------
function DataInitPanelSubObat(rowdata){
	Ext.getCmp('txtKdSubObat_InvRuangTempatInventarisL').setValue(rowdata.kd_sub_jns);
	InvRuangTempatInventaris.vars.sub.setValue(rowdata.sub_);
};

function dataaddnewSub_RuangTempatInventaris_Inv(){
	Ext.getCmp('txtKdSubObat_InvRuangTempatInventarisL').setValue('');
	InvRuangTempatInventaris.vars.sub.setValue('');
}

function datasaveSub_RuangTempatInventaris_Inv(){
	if (ValidasiEntrySubObat(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionInvRuangTempatInventaris/saveSub",
				params: getParamSetupSubObat(),
				failure: function(o)
				{
					ShowPesanErrorInvRuangTempatInventaris('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoInvRuangTempatInventaris(nmPesanSimpanSukses,nmHeaderSimpanData);
						Ext.get('txtKdSubObat_InvRuangTempatInventarisL').dom.value=cst.kdsub;
						dsDataRuangTempat_Inventaris_Inv.reload();
					}
					else 
					{
						ShowPesanErrorInvRuangTempatInventaris('Gagal Menyimpan Data ini', 'Error');
						dsDataRuangTempat_Inventaris_Inv.reload();
					};
				}
			}
			
		)
	}
}

function getParamSetupSubObat() {
	var	params =
	{
		KdSubJns:Ext.getCmp('txtKdSubObat_InvRuangTempatInventarisL').getValue(),
		Sub:InvRuangTempatInventaris.vars.sub.getValue()
	}
   
    return params
};

function ValidasiEntrySubObat(modul,mBolHapus){
	var x = 1;
	if(InvRuangTempatInventaris.vars.sub.getValue() === ''){
		ShowPesanWarningInvRuangTempatInventaris('Sub  obat masih kosong', 'Warning');
		x = 0;
	} 
	return x;
};


//------------------------------GOLONGAN Obat----------------------------------------------
function DataInitPanelGolongan(rowdata){
	Ext.getCmp('txtKdGolObat_InvRuangTempatInventarisL').setValue(rowdata.apt_kd_golongan);
	InvRuangTempatInventaris.vars.golongan.setValue(rowdata.apt_golongan);
};

function dataaddnewGolongan_RuangTempatInventaris_Inv(){
	Ext.getCmp('txtKdGolObat_InvRuangTempatInventarisL').setValue('');
	InvRuangTempatInventaris.vars.golongan.setValue('');
}

function datasaveGolongan_RuangTempatInventaris_Inv(){
	if (ValidasiEntryGolongan(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionInvRuangTempatInventaris/saveGolongan",
				params: getParamSetupGolongan(),
				failure: function(o)
				{
					ShowPesanErrorInvRuangTempatInventaris('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoInvRuangTempatInventaris(nmPesanSimpanSukses,nmHeaderSimpanData);
						Ext.get('txtKdGolObat_InvRuangTempatInventarisL').dom.value=cst.kdgolongan;
						dsDataGrdGolongan_viSetupMasterObat.reload();
					}
					else 
					{
						ShowPesanErrorInvRuangTempatInventaris('Gagal Menyimpan Data ini', 'Error');
						dsDataGrdGolongan_viSetupMasterObat.reload();
					};
				}
			}
			
		)
	}
}

function getParamSetupGolongan() {
	var	params =
	{
		KdGolongan:Ext.getCmp('txtKdGolObat_InvRuangTempatInventarisL').getValue(),
		Golongan:InvRuangTempatInventaris.vars.golongan.getValue()
	}
   
    return params
};

function ValidasiEntryGolongan(modul,mBolHapus){
	var x = 1;
	if(InvRuangTempatInventaris.vars.golongan.getValue() === ''){
		ShowPesanWarningInvRuangTempatInventaris('Golongan obat masih kosong', 'Warning');
		x = 0;
	} 
	return x;
};



//------------------------------SATUAN ----------------------------------------------
function DataInitPanelSatuan(rowdata){
	Ext.getCmp('txtKdSatuan_InvRuangTempatInventarisL').setValue(rowdata.kd_satuan);
	InvRuangTempatInventaris.vars.satuan.setValue(rowdata.satuan);
};

function dataaddnewSatuan_RuangTempatInventaris_Inv(){
	Ext.getCmp('txtKdSatuan_InvRuangTempatInventarisL').setValue('');
	InvRuangTempatInventaris.vars.satuan.setValue('');
}

function datasaveSatuan_RuangTempatInventaris_Inv(){
	if (ValidasiEntrySatuan(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionInvRuangTempatInventaris/saveSatuan",
				params: getParamSetupSatuan(),
				failure: function(o)
				{
					ShowPesanErrorInvRuangTempatInventaris('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoInvRuangTempatInventaris(nmPesanSimpanSukses,nmHeaderSimpanData);
						dsDataGrdSatuan_viSetupMasterObat.reload();
					}
					else 
					{
						ShowPesanErrorInvRuangTempatInventaris('Gagal Menyimpan Data ini', 'Error');
						dsDataGrdSatuan_viSetupMasterObat.reload();
					};
				}
			}
			
		)
	}
}

function getParamSetupSatuan(){
	var	params =
	{
		KdSatuan:Ext.getCmp('txtKdSatuan_InvRuangTempatInventarisL').getValue(),
		Satuan:InvRuangTempatInventaris.vars.satuan.getValue()
	}
   
    return params
};

function ValidasiEntrySatuan(modul,mBolHapus){
	var x = 1;
	if(Ext.getCmp('txtKdSatuan_InvRuangTempatInventarisL').getValue() === ''){
		ShowPesanWarningInvRuangTempatInventaris('Kode Satuan masih kosong', 'Warning');
		x = 0;
	}
	if(InvRuangTempatInventaris.vars.satuan.getValue() === ''){
		ShowPesanWarningInvRuangTempatInventaris('Satuan masih kosong', 'Warning');
		x = 0;
	} 
	if(Ext.getCmp('txtKdSatuan_InvRuangTempatInventarisL').getValue().length > 10){
		ShowPesanWarningInvRuangTempatInventaris('Kode satuan kecil tidak boleh lebih dari 10 huruf', 'Warning');
		x = 0;
	}
	return x;
};


//------------------------------SATUAN BESAR----------------------------------------------
function DataInitPanelSatuanBesar(rowdata){
	Ext.getCmp('txtKdSatuanBesar_InvRuangTempatInventarisL').setValue(rowdata.kd_sat_besar);
	InvRuangTempatInventaris.vars.satuanBesar.setValue(rowdata.keterangan);
};

function dataaddnewSatuanBesar_RuangTempatInventaris_Inv(){
	Ext.getCmp('txtKdSatuanBesar_InvRuangTempatInventarisL').setValue('');
	InvRuangTempatInventaris.vars.satuanBesar.setValue('');
}

function datasaveSatuanBesar_RuangTempatInventaris_Inv(){
	if (ValidasiEntrySatuanBesar(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionInvRuangTempatInventaris/saveSatuanBesar",
				params: getParamSetupSatuanBesar(),
				failure: function(o)
				{
					ShowPesanErrorInvRuangTempatInventaris('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoInvRuangTempatInventaris(nmPesanSimpanSukses,nmHeaderSimpanData);
						//Ext.get('txtKdSatuanBesar_InvRuangTempatInventarisL').dom.value=cst.kdsatbesar;
						dsDataGrdSatuanBesar_viSetupMasterObat.reload();
					}
					else 
					{
						ShowPesanErrorInvRuangTempatInventaris('Gagal Menyimpan Data ini', 'Error');
						dsDataGrdSatuanBesar_viSetupMasterObat.reload();
					};
				}
			}
			
		)
	}
}

function getParamSetupSatuanBesar(){
	var	params =
	{
		KdSatBesar:Ext.getCmp('txtKdSatuanBesar_InvRuangTempatInventarisL').getValue(),
		Keterangan:InvRuangTempatInventaris.vars.satuanBesar.getValue()
	}
   
    return params
};

function ValidasiEntrySatuanBesar(modul,mBolHapus){
	var x = 1;
	if(Ext.getCmp('txtKdSatuanBesar_InvRuangTempatInventarisL').getValue() === ''){
		ShowPesanWarningInvRuangTempatInventaris('Kode satuan besar satuan masih kosong', 'Warning');
		x = 0;
	}
	if(InvRuangTempatInventaris.vars.satuanBesar.getValue() === ''){
		ShowPesanWarningInvRuangTempatInventaris('Keterangan satuan masih kosong', 'Warning');
		x = 0;
	}	
	if(Ext.getCmp('txtKdSatuanBesar_InvRuangTempatInventarisL').getValue().length > 10){
		ShowPesanWarningInvRuangTempatInventaris('Kode satuan besar tidak boleh lebih dari 10 huruf', 'Warning');
		x = 0;
	}
	return x;
};



function ShowPesanWarningInvRuangTempatInventaris(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorInvRuangTempatInventaris(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoInvRuangTempatInventaris(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};// JavaScript Document