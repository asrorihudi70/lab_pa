var dataSourceCaraPerolehan;
var rowselectedCaraPerolehan;
var LookupCaraPerolehan;
var disDataCaraPerolehan=0;
var InvSetupCaraPerolehan={};
var gridListHasilCaraPerolehan;

InvSetupCaraPerolehan.form={};
InvSetupCaraPerolehan.func={};
InvSetupCaraPerolehan.vars={};
InvSetupCaraPerolehan.func.parent=InvSetupCaraPerolehan;
InvSetupCaraPerolehan.form.ArrayStore={};
InvSetupCaraPerolehan.form.ComboBox={};
InvSetupCaraPerolehan.form.DataStore={};
InvSetupCaraPerolehan.form.Record={};
InvSetupCaraPerolehan.form.Form={};
InvSetupCaraPerolehan.form.Grid={};
InvSetupCaraPerolehan.form.Panel={};
InvSetupCaraPerolehan.form.TextField={};
InvSetupCaraPerolehan.form.Button={};

InvSetupCaraPerolehan.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_perolehan', 'perolehan'],
	data: []
});
CurrentPage.page = getPanelCaraPerolehan(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);


function getPanelCaraPerolehan(mod_id) {
    var Field = ['KD_AHLI_GIZI','NAMA_AHLI_GIZI'];
    dataSourceCaraPerolehan = new WebApp.DataStore({
        fields: Field
    });
	
	datasourcefunction_CaraPerolehan();
	
    gridListHasilCaraPerolehan = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dataSourceCaraPerolehan,
        anchor: '100% 80%',
        columnLines: false,
        autoScroll: false,
        border: true,
        sort: false,
        autoHeight: false,
        layout: 'fit',
        region: 'center',
        //height: 200,
        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
            listeners: {
                rowselect: function (sm, row, rec) {
					rowselectedCaraPerolehan=dataSourceCaraPerolehan.getAt(row);
                }
            }
        }),
        listeners: {
           rowdblclick: function (sm, ridx, cidx) {
                rowselectedCaraPerolehan=dataSourceCaraPerolehan.getAt(ridx);
				disDataCaraPerolehan=1;
				disabled_dataCaraPerolehan(disDataCaraPerolehan);
				if(rowselectedCaraPerolehan != undefined){
					datainit_formCaraPerolehan(rowselectedCaraPerolehan.data)
				}
            }
        },
        cm: new Ext.grid.ColumnModel(
                [
				new Ext.grid.RowNumberer({
					header:'No.'
					}),
                    {
                        header: 'ID',
                        dataIndex: 'kd_perolehan',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 5
                    },
                    {
                        header: 'Cara Perolehan',
                        dataIndex: 'perolehan',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
						width: 50
                    },
                ]
                ),
        viewConfig: {
            forceFit: true
        },
        tbar:[
			{
                id: 'btnEditDataCaraPerolehan',
                text: 'Edit Data',
                tooltip: 'Edit Data',
                iconCls: 'Edit_Tr',
                handler: function (sm, row, rec) {
                    edit_dataCaraPerolehan(rowselectedCaraPerolehan.data);
                },
				
            },
		]
    });
	   var FormDepanCaraPerolehan = new Ext.Panel({
        id: mod_id,
        closable: true,
        region: 'center',
        layout: 'form',
        title: 'Setup Cara Perolehan',
        border: false,
        shadhow: true,
        autoScroll: false,
		width: 250,
        iconCls: 'Request',
        margins: '0 5 5 0',
		tbar: [
			
			{
                id: 'btnAddNewDataCaraPerolehan',
                text: 'Add New',
                tooltip: 'addCaraPerolehan',
                iconCls: 'add',
                handler: function (sm, row, rec) {
                    addnew_dataCaraPerolehan();
				}
            },
			{
        	xtype: 'tbseparator'
       		},
			{
                id: 'btnSaveDataCaraPerolehan',
                text: 'Save Data',
                tooltip: 'Save Data',
                iconCls: 'Save',
                handler: function (sm, row, rec) {
                    save_dataCaraPerolehan();
                },
				
            },
			{
        	xtype: 'tbseparator'
       		},
			{
                id: 'btnDeleteDataCaraPerolehan',
                text: 'Delete Data',
                tooltip: 'Delete Data',
                iconCls: 'Remove',
                handler: function (sm, row, rec) {
					if (rowselectedCaraPerolehan!=undefined)
                    	delete_dataCaraPerolehan();
					else
						ShowPesanErrorCaraPerolehan('Maaf ! Tidak ada data yang terpilih', 'Error');
				}
            },
			{
        	xtype: 'tbseparator'
       		},
			{
                id: 'btnRefreshDataCaraPerolehan',
                text: 'Refresh',
                tooltip: 'Refresh',
                iconCls: 'Refresh',
                handler: function (sm, row, rec) {
                    datasourcefunction_CaraPerolehan();
                },
				
            }
			],
        items: [
			getPanelPencarianCaraPerolehan(),
			gridListHasilCaraPerolehan,
            
            
//            PanelPengkajian(),
//            TabPanelPengkajian()
        ],
        listeners: {
            'afterrender': function () {

            }
        }
    });

    return FormDepanCaraPerolehan;

}
;
function getPanelPencarianCaraPerolehan() {
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 1190,
                height: 75,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'ID '
                    },
                    {
                        x: 110,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 10,
                        xtype: 'textfield',
                        name: 'TxtKdCaraPerolehan',
                        id: 'TxtKdCaraPerolehan',
                        width: 80,
						readOnly: true,
						
                    },
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Cara Perolehan'
                    },
                    {
                        x: 110,
                        y: 40,
                        xtype: 'label',
                        text: ' : '
                    },
					InvSetupCaraPerolehan.vars.CaraPerolehan=new Nci.form.Combobox.autoComplete({
						x: 120,
						y: 40,
						tabIndex:2,
						store	: InvSetupCaraPerolehan.form.ArrayStore.a,
						select	: function(a,b,c){
							console.log(b);
							Ext.getCmp('TxtKdCaraPerolehan').setValue(b.data.kd_perolehan);
							
							gridListHasilCaraPerolehan.getView().refresh();
						},
						onShowList:function(a){
							dataSourceCaraPerolehan.removeAll();
							
							var recs=[],
							recType=dataSourceCaraPerolehan.recordType;
								
							for(var i=0; i<a.length; i++){
								recs.push(new recType(a[i]));
							}
							dataSourceCaraPerolehan.add(recs);
							
						},
						insert	: function(o){
							return {
								kd_perolehan      	: o.kd_perolehan,
								perolehan 			: o.perolehan,
								text				:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_perolehan+'</td><td width="200">'+o.perolehan+'</td></tr></table>'
							}
						},
						url		: baseURL + "index.php/inventaris/functionSetupCaraPerolehan/getDataPerolehanGrid",
						valueField: 'perolehan',
						displayField: 'perolehan',
						listWidth: 280
					}),
                ]
            }
        ]
    };
    return items;
};

function datasourcefunction_CaraPerolehan(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/inventaris/functionSetupCaraPerolehan/getDataPerolehanGrid",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorCaraPerolehan('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dataSourceCaraPerolehan.removeAll();
					var recs=[],
						recType=dataSourceCaraPerolehan.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dataSourceCaraPerolehan.add(recs);
					
					gridListHasilCaraPerolehan.getView().refresh();
				}
				else 
				{
					ShowPesanErrorCaraPerolehan('Gagal membaca data', 'Error');
				};
			}
		}
		
	)
	
}


function save_dataCaraPerolehan()
{
	if (ValidasiEntriCaraPerolehan(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/inventaris/functionSetupCaraPerolehan/save",
				params: ParameterSaveCaraPerolehan(),
				failure: function(o)
				{
					ShowPesanErrorCaraPerolehan('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanSuksesCaraPerolehan(nmPesanSimpanSukses,nmHeaderSimpanData);
						Ext.getCmp('TxtKdCaraPerolehan').setValue(cst.kode);
						datasourcefunction_CaraPerolehan();
						//ClearTextCaraPerolehan();
						disDataCaraPerolehan=1;
						disabled_dataCaraPerolehan(disDataCaraPerolehan);
					}
					else 
					{
						ShowPesanErrorCaraPerolehan('Gagal Menyimpan Data ini', 'Error');
						datasourcefunction_CaraPerolehan();
					};
				}
			}
			
		)
	}
}
function edit_dataCaraPerolehan(rowdata){
	disDataCaraPerolehan=1;
	disabled_dataCaraPerolehan(disDataCaraPerolehan);
	ShowLookupCaraPerolehan(rowdata);
}

function delete_dataCaraPerolehan() 
{
		Ext.Msg.show
		(
			{
			   title:nmHeaderHapusData,
			   msg: "Apa anda yakin akan menghapus data ini?" ,
			   buttons: Ext.MessageBox.YESNO,
			   width:250,
			   fn: function (btn) 
			   {			
					if (btn === 'yes') 
					{
						Ext.Ajax.request
						(
							{
								url: baseURL + "index.php/inventaris/functionSetupCaraPerolehan/delete",
								params: paramsDeleteCaraPerolehan(),
								success: function(o) 
								{
									var cst = Ext.decode(o.responseText);
									if (cst.success === true) 
									{
										ShowPesanSuksesCaraPerolehan(nmPesanHapusSukses,nmHeaderHapusData);
										datasourcefunction_CaraPerolehan();
										ClearTextCaraPerolehan();
										disDataCaraPerolehan=0;
										disabled_dataCaraPerolehan(disDataCaraPerolehan);
										
									}
									else if (cst.success === false && cst.pesan===0)
									{
										ShowPesanErrorCaraPerolehan(nmPesanHapusGagal,nmHeaderHapusData);
									}
									else 
									{
										ShowPesanErrorCaraPerolehan(nmPesanHapusError,nmHeaderHapusData);
									};
								}
							}
						)
						disDataCaraPerolehan=0;
						disabled_dataCaraPerolehan(disDataCaraPerolehan);
					};
				}
			}
		)
	};

function ShowLookupCaraPerolehan(rowdata) {
	if (rowdata == undefined){
        ShowPesanErrorCaraPerolehan('Data Kosong', 'Error');
    }
    else {
		datainit_formCaraPerolehan(rowdata); 
    }
}	

function datainit_formCaraPerolehan(rowdata){
	Ext.getCmp('TxtKdCaraPerolehan').setValue(rowdata.kd_perolehan);
	InvSetupCaraPerolehan.vars.CaraPerolehan.setValue(rowdata.perolehan);
}
	
function ParameterSaveCaraPerolehan()
{
	var params = {
		kodeData : Ext.getCmp('TxtKdCaraPerolehan').getValue(),
		namaData : InvSetupCaraPerolehan.vars.CaraPerolehan.getValue()
	}	
	return params;	
}

function paramsDeleteCaraPerolehan() 
{
    var params =
	{
		kodeData : Ext.getCmp('TxtKdCaraPerolehan').getValue()
	};
    return params
};
	
function addnew_dataCaraPerolehan() {
	ClearTextCaraPerolehan();
	disDataCaraPerolehan=0;
	disabled_dataCaraPerolehan(disDataCaraPerolehan);
	TombolUpdate :InvSetupCaraPerolehan.vars.CaraPerolehan.focus();
}

function ValidasiEntriCaraPerolehan(modul,mBolHapus) {
	var nama = InvSetupCaraPerolehan.vars.CaraPerolehan.getValue();
	
	var x = 1;
	if( nama === '') {
		ShowPesanErrorCaraPerolehan('Cara perolehan tidak boleh kosong', 'Warning');
		x = 0;	
	}
	
	return x;
};

function disabled_dataCaraPerolehan(disDatax){
	if (disDatax==1) {
		var dis=  {
			kodeData : Ext.getCmp('TxtKdCaraPerolehan').disable(),
		}
	} else if (disDatax==0)  {
		var dis=  {
			kodeData : Ext.getCmp('TxtKdCaraPerolehan').enable(),
		}
	}
	return dis;
}
	
function ClearTextCaraPerolehan() {
	kodeData : Ext.getCmp('TxtKdCaraPerolehan').setValue("");
	namaData : InvSetupCaraPerolehan.vars.CaraPerolehan.setValue("");
}

function ShowPesanSuksesCaraPerolehan(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};

function ShowPesanErrorCaraPerolehan(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:290
		}
	);
};






