var dataSourceKategoriBangunan;
var rowselectedKategoriBangunan;
var LookupKategoriBangunan;
var disDataKategoriBangunan=0;
var InvSetupKategoriBangunan={};
var gridListHasilKategoriBangunan;


InvSetupKategoriBangunan.form={};
InvSetupKategoriBangunan.func={};
InvSetupKategoriBangunan.vars={};
InvSetupKategoriBangunan.func.parent=InvSetupKategoriBangunan;
InvSetupKategoriBangunan.form.ArrayStore={};
InvSetupKategoriBangunan.form.ComboBox={};
InvSetupKategoriBangunan.form.DataStore={};
InvSetupKategoriBangunan.form.Record={};
InvSetupKategoriBangunan.form.Form={};
InvSetupKategoriBangunan.form.Grid={};
InvSetupKategoriBangunan.form.Panel={};
InvSetupKategoriBangunan.form.TextField={};
InvSetupKategoriBangunan.form.Button={};

InvSetupKategoriBangunan.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_kategori', 'kategori'],
	data: []
});
CurrentPage.page = getPanelKategoriBangunan(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
	
function getPanelKategoriBangunan(mod_id) {
    var Field = ['kd_kategori','kategori'];
    dataSourceKategoriBangunan = new WebApp.DataStore({
        fields: Field
    });
	
	datasourcefunction_KategoriBangunan();
    
	gridListHasilKategoriBangunan = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dataSourceKategoriBangunan,
        anchor: '100% 80%',
        columnLines: false,
        autoScroll: false,
        border: true,
        sort: false,
        autoHeight: false,
        layout: 'fit',
        region: 'center',
        //height: 200,
        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
            listeners: {
                rowselect: function (sm, row, rec) {
					rowselectedKategoriBangunan=dataSourceKategoriBangunan.getAt(row);
                }
            }
        }),
        listeners: {
           rowdblclick: function (sm, ridx, cidx) {
                rowselectedKategoriBangunan=dataSourceKategoriBangunan.getAt(ridx);
				disDataKategoriBangunan=1;
				disabled_dataKategoriBangunan(disDataKategoriBangunan);
				if(rowselectedKategoriBangunan != undefined){
					datainit_formKategoriBangunan(rowselectedKategoriBangunan.data);
				}
            }
        },
        cm: new Ext.grid.ColumnModel(
                [
				new Ext.grid.RowNumberer({
					header:'No.'
					}),
                    {
                        header: 'ID',
                        dataIndex: 'kd_kategori',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 5
                    },
                    {
                        header: 'Kategori',
                        dataIndex: 'kategori',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
						width: 50
                    },
                ]
                ),
        viewConfig: {
            forceFit: true
        },
        tbar:[
			{
                id: 'btnEditDataKategoriBangunan',
                text: 'Edit Data',
                tooltip: 'Edit Data',
                iconCls: 'Edit_Tr',
                handler: function (sm, row, rec) {
                    edit_dataKategoriBangunan(rowselectedKategoriBangunan.data);
                },
				
            },
		]
    });
	   var FormDepanKategoriBangunan = new Ext.Panel({
        id: mod_id,
        closable: true,
        region: 'center',
        layout: 'form',
        title: 'Setup Kategori Bangunan',
        border: false,
        shadhow: true,
        autoScroll: false,
		width: 250,
        iconCls: 'Request',
        margins: '0 5 5 0',
		tbar: [
			{
                id: 'btnAddNewDataKategoriBangunan',
                text: 'Add New',
                tooltip: 'addKategoriBangunan',
                iconCls: 'add',
                handler: function (sm, row, rec) {
                    addnew_dataKategoriBangunan();
				}
            },
			{
				xtype: 'tbseparator'
       		},
			{
                id: 'btnSaveDataKategoriBangunan',
                text: 'Save Data',
                tooltip: 'Save Data',
                iconCls: 'Save',
                handler: function (sm, row, rec) {
                    save_dataKategoriBangunan();
                },
				
            },
			{
				xtype: 'tbseparator'
       		},
			{
                id: 'btnDeleteDataKategoriBangunan',
                text: 'Delete Data',
                tooltip: 'Delete Data',
                iconCls: 'Remove',
                handler: function (sm, row, rec) {
					if (rowselectedKategoriBangunan!=undefined)
                    	delete_dataKategoriBangunan();
					else
						ShowPesanErrorKategoriBangunan('Maaf ! Tidak ada data yang terpilih', 'Error');
				}
            },
			{
				xtype: 'tbseparator'
       		},
			{
                id: 'btnRefreshDataKategoriBangunan',
                text: 'Refresh',
                tooltip: 'Refresh',
                iconCls: 'Refresh',
                handler: function (sm, row, rec) {
                    datasourcefunction_KategoriBangunan();
                },
				
            }
			],
        items: [
			getPanelPencarianKategoriBangunan(),
			gridListHasilKategoriBangunan,
        ],
        listeners: {
            'afterrender': function () {

            }
        }
    });

    return FormDepanKategoriBangunan;

}
;
function getPanelPencarianKategoriBangunan() {
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 1190,
                height: 75,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'ID '
                    },
                    {
                        x: 110,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 10,
                        xtype: 'textfield',
                        name: 'TxtKdKategoriBangunan',
                        id: 'TxtKdKategoriBangunan',
                        width: 80,
						readOnly: true,
						
                    },
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Kategori'
                    },
                    {
                        x: 110,
                        y: 40,
                        xtype: 'label',
                        text: ' : '
                    },
					InvSetupKategoriBangunan.vars.KategoriBangunan=new Nci.form.Combobox.autoComplete({
						x: 120,
						y: 40,
						tabIndex:2,
						store	: InvSetupKategoriBangunan.form.ArrayStore.a,
						select	: function(a,b,c){
							console.log(b);
							Ext.getCmp('TxtKdKategoriBangunan').setValue(b.data.kd_kategori);
							
							gridListHasilKategoriBangunan.getView().refresh();
						},
						onShowList:function(a){
							dataSourceKategoriBangunan.removeAll();
							
							var recs=[],
							recType=dataSourceKategoriBangunan.recordType;
								
							for(var i=0; i<a.length; i++){
								recs.push(new recType(a[i]));
							}
							dataSourceKategoriBangunan.add(recs);
							
						},
						insert	: function(o){
							return {
								kd_kategori     : o.kd_kategori,
								kategori 		: o.kategori,
								text			:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_kategori+'</td><td width="200">'+o.kategori+'</td></tr></table>'
							}
						},
						url		: baseURL + "index.php/inventaris/functionSetupKategori/getDataGridKategori",
						valueField: 'kategori',
						displayField: 'kategori',
						listWidth: 280
					}),
          
                ]
            }
        ]
    };
    return items;
};

function datasourcefunction_KategoriBangunan(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/inventaris/functionSetupKategori/getDataGridKategori",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorKategoriBangunan('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dataSourceKategoriBangunan.removeAll();
					var recs=[],
						recType=dataSourceKategoriBangunan.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dataSourceKategoriBangunan.add(recs);
					
					gridListHasilKategoriBangunan.getView().refresh();
				}
				else 
				{
					ShowPesanErrorKategoriBangunan('Gagal membaca data', 'Error');
				};
			}
		}
		
	)
	
}


function save_dataKategoriBangunan()
{
	if (ValidasiEntriKategoriBangunan(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/inventaris/functionSetupKategori/save",
				params: ParameterSaveKategoriBangunan(),
				failure: function(o)
				{
					ShowPesanErrorKategoriBangunan('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanSuksesKategoriBangunan(nmPesanSimpanSukses,nmHeaderSimpanData);
						Ext.getCmp('TxtKdKategoriBangunan').setValue(cst.kode);
						datasourcefunction_KategoriBangunan();
						disDataKategoriBangunan=1;
						disabled_dataKategoriBangunan(disDataKategoriBangunan);
					}
					else 
					{
						ShowPesanErrorKategoriBangunan('Gagal Menyimpan Data ini', 'Error');
						datasourcefunction_KategoriBangunan();
					};
				}
			}
			
		)
	}
}
function edit_dataKategoriBangunan(rowdata){
	disDataKategoriBangunan=1;
	disabled_dataKategoriBangunan(disDataKategoriBangunan);
	ShowLookupKategoriBangunan(rowdata);
}

function delete_dataKategoriBangunan() 
{
		Ext.Msg.show
		(
			{
			   title:nmHeaderHapusData,
			   msg: "Apa anda yakin akan menghapus data ini?" ,
			   buttons: Ext.MessageBox.YESNO,
			   width:250,
			   fn: function (btn) 
			   {			
					if (btn === 'yes') 
					{
						Ext.Ajax.request
						(
							{
								url		: baseURL + "index.php/inventaris/functionSetupKategori/delete",
								params: paramsDeleteKategoriBangunan(),
								success: function(o) 
								{
									var cst = Ext.decode(o.responseText);
									if (cst.success === true) 
									{
										ShowPesanSuksesKategoriBangunan(nmPesanHapusSukses,nmHeaderHapusData);
										Ext.getCmp('TxtKdKategoriBangunan').setValue(cst.kodeKategoriBangunan);
										datasourcefunction_KategoriBangunan();
										ClearTextKategoriBangunan();
										disDataKategoriBangunan=0;
										disabled_dataKategoriBangunan(disDataKategoriBangunan);
										
									}
									else if (cst.success === false && cst.pesan===0)
									{
										ShowPesanErrorKategoriBangunan(nmPesanHapusGagal,nmHeaderHapusData);
									}
									else 
									{
										ShowPesanErrorKategoriBangunan(nmPesanHapusError,nmHeaderHapusData);
									};
								}
							}
						)
						disDataKategoriBangunan=0;
						disabled_dataKategoriBangunan(disDataKategoriBangunan);
					};
				}
			}
		)
	};
	
function addnew_dataKategoriBangunan()
{
	ClearTextKategoriBangunan();
	disDataKategoriBangunan=0;
	disabled_dataKategoriBangunan(disDataKategoriBangunan);
	TombolUpdate :InvSetupKategoriBangunan.vars.KategoriBangunan.focus();
}

function ParameterSaveKategoriBangunan() {
	var params = {
		kodeData : Ext.getCmp('TxtKdKategoriBangunan').getValue(),
		namaData : InvSetupKategoriBangunan.vars.KategoriBangunan.getValue()
	}	
	return params;	
}

function paramsDeleteKategoriBangunan() 
{
    var params = {
		kodeData : Ext.getCmp('TxtKdKategoriBangunan').getValue(),
	};
    return params
};

function ShowLookupKategoriBangunan(rowdata)
{
	if (rowdata == undefined){
        ShowPesanErrorKategoriBangunan('Data Kosong', 'Error');
    }
    else
    {
      datainit_formKategoriBangunan(rowdata); 
    }
}

function datainit_formKategoriBangunan(rowdata){
	Ext.getCmp('TxtKdKategoriBangunan').setValue(rowdata.kd_kategori);
	InvSetupKategoriBangunan.vars.KategoriBangunan.setValue(rowdata.kategori);
}

function ClearTextKategoriBangunan() {
	kodeData : Ext.getCmp('TxtKdKategoriBangunan').setValue("");
	namaData : InvSetupKategoriBangunan.vars.KategoriBangunan.setValue("");
}

function disabled_dataKategoriBangunan(disDatax){
	if (disDatax==1) {
		var dis={
			kodeData : Ext.getCmp('TxtKdKategoriBangunan').disable(),
		}
	} else if (disDatax==0) {
		var dis= {
			kodeData : Ext.getCmp('TxtKdKategoriBangunan').enable(),
		}
	}
	return dis;
}

function ValidasiEntriKategoriBangunan(modul,mBolHapus)
{
	var nama = InvSetupKategoriBangunan.vars.KategoriBangunan.getValue();
	
	var x = 1;
	if( nama === '') {
		ShowPesanErrorKategoriBangunan('Data Belum Diisi Lengkap', 'Warning');
		x = 0;	
	}
	
	return x;
};

function ShowPesanSuksesKategoriBangunan(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};

function ShowPesanErrorKategoriBangunan(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:290
		}
	);
};


