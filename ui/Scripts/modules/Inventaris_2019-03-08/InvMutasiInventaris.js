var dataSourcePenambahan_MutasiInventaris_Inv;
var dataSourcePemindahan_MutasiInventaris_Inv;
var dataSourcePengurangan_MutasiInventaris_Inv;
var selectCount_MutasiInventaris_Inv=50;
var NamaForm_MutasiInventaris_Inv="Mutasi";
var selectCountStatusPostingInvMutasiInventaris='Semua';
var mod_name_MutasiInventaris_Inv="MutasiInventaris_Inv";
var now_MutasiInventaris_Inv= new Date();
var addNew_MutasiInventaris_Inv;
var rowSelected_MutasiInventaris_Inv;
var setLookUps_MutasiInventaris_Inv;
var tanggal = now_MutasiInventaris_Inv.format("d/M/Y");
var jam = now_MutasiInventaris_Inv.format("H/i/s");
var tmpkriteria;
var DataGridObat;
var DataGridSubObat;
var DataGridSatuanBesar;
var DataGridSatuan;
var DataGridGolongan;


var CurrentData_MutasiInventaris_Inv =
{
	data: Object,
	details: Array,
	row: 0
};


var InvMutasiInventaris={};
InvMutasiInventaris.form={};
InvMutasiInventaris.func={};
InvMutasiInventaris.vars={};
InvMutasiInventaris.func.parent=InvMutasiInventaris;
InvMutasiInventaris.form.ArrayStore={};
InvMutasiInventaris.form.ComboBox={};
InvMutasiInventaris.form.DataStore={};
InvMutasiInventaris.form.Record={};
InvMutasiInventaris.form.Form={};
InvMutasiInventaris.form.Grid={};
InvMutasiInventaris.form.Panel={};
InvMutasiInventaris.form.TextField={};
InvMutasiInventaris.form.Button={};

InvMutasiInventaris.form.ArrayStore.mutasiPenambahan= new Ext.data.ArrayStore({
	id: 0,
	fields:[],
	data: []
});
InvMutasiInventaris.form.ArrayStore.mutasiPemindahan= new Ext.data.ArrayStore({
	id: 0,
	fields:[],
	data: []
});

InvMutasiInventaris.form.ArrayStore.mutasiPengurangan= new Ext.data.ArrayStore({
	id: 0,
	fields:[],
	data: []
});

InvMutasiInventaris.form.ArrayStore.gridMutasiPenambahan= new Ext.data.ArrayStore({
	id: 0,
	fields:[],
	data: []
});
InvMutasiInventaris.form.ArrayStore.gridMutasiPemindahan= new Ext.data.ArrayStore({
	id: 0,
	fields:[],
	data: []
});
InvMutasiInventaris.form.ArrayStore.gridMutasiPengurangan= new Ext.data.ArrayStore({
	id: 0,
	fields:[],
	data: []
});
InvMutasiInventaris.form.ArrayStore.gridMutasiLokasiBaruPemindahan= new Ext.data.ArrayStore({
	id: 0,
	fields:[],
	data: []
});
InvMutasiInventaris.form.ArrayStore.gridMutasiJumlahPindahPemindahan= new Ext.data.ArrayStore({
	id: 0,
	fields:[],
	data: []
});

InvMutasiInventaris.form.ArrayStore.satuanBesar= new Ext.data.ArrayStore({
	id: 0,
	fields:[],
	data: []
});


CurrentPage.page = dataGrid_MutasiInventaris_Inv(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dataGrid_MutasiInventaris_Inv(mod_id_MutasiInventaris_Inv){	
 	var PanelTabInvMutasiInventaris = new Ext.FormPanel({
        labelAlign: 'top',
		layout:'form',
        frame:true,
        title: '',
        bodyStyle:'padding:5px 5px 0',
        //width: 600,
        items: [getPenelItemSetup_MutasiInventaris_Inv()]	
		
	})
	// Kriteria filter pada Grid # --------------
    var FrmData_MutasiInventaris_Inv = new Ext.Panel
    (
		{
			title: NamaForm_MutasiInventaris_Inv,
			iconCls: 'Studi_Lanjut',
			id: mod_id_MutasiInventaris_Inv,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ PanelTabInvMutasiInventaris],
					//GridDataView_MutasiInventaris_Inv],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_MutasiInventaris_Inv,
		            columns: 21,
		            defaults: {
					scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_MutasiInventaris_Inv;
    //-------------- # End form filter # --------------
}
function funcNamaGedungMutasiInventaris_Inv()
{
    /*dsvGolTarif = new WebApp.DataStore({fields: stateComboTarifCust});
	dsGolTarif();*/
	var funcNamaGedungMutasiInventaris = new Ext.form.ComboBox
	(
		{
			x: 130,
			y: 10,
			id:'cmbNamaGedungMutasiInventaris_Inv',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			/*readOnly:true,
			emptyText:'Set Tarif',
			value: 1,*/
			width: 260,/*
			store: dsvGolTarif,
			valueField: stateComboTarifCust[0],
			displayField: stateComboTarifCust[1],*/
			//value:selectSetPilihankelompokPasien,
			listeners:
			{
				'select': function(a,b,c)
				{
				}
			}
		}
	);
	return funcNamaGedungMutasiInventaris;
}
function funcGedungMutasiInventaris_Inv()
{
    /*dsvGolTarif = new WebApp.DataStore({fields: stateComboTarifCust});
	dsGolTarif();*/
	var funcDeptMutasiInventaris = new Ext.form.ComboBox
	(
		{
			x: 130,
			y: 40,
			id:'cmbDeptMutasiInventaris_Inv',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			/*readOnly:true,
			emptyText:'Set Tarif',
			value: 1,*/
			width: 320,/*
			store: dsvGolTarif,
			valueField: stateComboTarifCust[0],
			displayField: stateComboTarifCust[1],*/
			//value:selectSetPilihankelompokPasien,
			listeners:
			{
				'select': function(a,b,c)
				{
				}
			}
		}
	);
	return funcDeptMutasiInventaris;
}
function getPenelItemSetup_MutasiInventaris_Inv(lebar)
{
    var items =
	{
		xtype:'tabpanel',
		plain:true,
		activeTab: 0,
		height:600,
		defaults: {
			bodyStyle:'padding:5px',
			autoScroll: false
	    },
	    items:[
				PanelPenambahan(),
				PanelPemindahan(),
				PanelPengurangan(),
		]
	}
    return items;
};

//sub form tab1
function PanelPenambahan(rowdata){
	var items = 
	{
		title:'Penambahan',
		layout:'form',
		border: false,
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 110,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 10,
								xtype: 'label',
								text: 'Nomor'
							},
							{
								x: 120,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							InvMutasiInventaris.vars.mutasiPenambahan=new Nci.form.Combobox.autoComplete({
								x: 130,
								y: 10,
								width:100,
								tabIndex:2,
								store	: InvMutasiInventaris.form.ArrayStore.mutasiPenambahan,
								select	: function(a,b,c){
									dsPenambahan_MutasiInv(b.data.no_mutasi);
									Ext.getCmp('tglMutasiPenambahanInventaris_Inv').setValue(ShowDate(b.data.tgl_mutasi));
									Ext.getCmp('TxtKeteranganMutasiPenambahanFilterGridDataView_MutasiInventaris_Inv').setValue(b.data.keterangan);
								},
								insert	: function(o){
									return {
										no_mutasi	: o.no_mutasi,
										tgl_mutasi	: o.tgl_mutasi,
										keterangan	: o.keterangan,
										jen_mutasi	: o.jen_mutasi,
										text		: '<table style="font-size: 11px;"><tr><td width="70">'+o.no_mutasi+'</td><td width="250">'+o.keterangan+'</td></tr></table>'
									}
								},
								url		: baseURL + "index.php/inventaris/functionMutasiInv/getDataAutoKomplitPenambahanMutasi",
								valueField: 'no_mutasi',
								displayField: 'text',
								listWidth: 280
							}),
							{
								x: 240,
								y: 10,
								xtype: 'label',
								text: 'Tanggal'
							},
							{
								x: 290,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 300,
								y: 10,
								xtype: 'datefield',
								id: 'tglMutasiPenambahanInventaris_Inv',
								name: 'tglMutasiPenambahanInventaris_Inv',
								width: 150,
								format: 'd/M/Y',
								tabIndex:3,
								readOnly:true,
								value: now_MutasiInventaris_Inv,
							},
							{
								x: 10,
								y: 40,
								xtype: 'label',
								text: 'Keterangan'
							},
							{
								x: 120,
								y: 40,
								xtype: 'label',
								text: ':'
							},{
							x: 130,
							y: 40,
							xtype: 'textarea',
							id: 'TxtKeteranganMutasiPenambahanFilterGridDataView_MutasiInventaris_Inv',
							name: 'TxtKeteranganMutasiPenambahanFilterGridDataView_MutasiInventaris_Inv',
							emptyText: '',
							width: 320,
							tabIndex:1,
							
						},
						
						]
					}
				]
			},
			{
				
						layout: 'form',
						//bodyStyle:'padding: 5px',
						border: true,
						tbar:
						[
							{
								text	: 'Add Barang',
								id		: 'btnAddPenambahanBarangInvMutasiInventarisL',
								tooltip	: nmLookup,
								iconCls	: 'find',
								handler	: function(){
									var records = new Array();
									records.push(new dataSourcePenambahan_MutasiInventaris_Inv.recordType());
									dataSourcePenambahan_MutasiInventaris_Inv.add(records);
									console.log(dataSourcePenambahan_MutasiInventaris_Inv.getCount()-1);
									//getNoRegRuangOtomatis(dataSource_RuangTempatInventaris_Inv.getCount()-1);
								}
							},{
								xtype: 'tbseparator'
							},{
								xtype: 'button',
								text: 'Delete',
								iconCls: 'remove',
								id: 'btnDeletePenambahanBarangInvMutasiInventaris',
								handler: function()
								{
									var line = DataGridPenambahanMutasiInventaris.getSelectionModel().selection.cell[0];
									var o = dataSourcePenambahan_MutasiInventaris_Inv.getRange()[line].data;
									if(dataSourcePenambahan_MutasiInventaris_Inv.getCount()>1){
										if(dataSourcePenambahan_MutasiInventaris_Inv.getRange()[line].data.no_urut_brg != undefined){
													ShowPesanWarningInvMutasiInventaris('Tidak bisa menghapus data ini', 'Perhatian');			
										}else{
											Ext.Msg.confirm('Warning', 'Apakah yakin akan dihapus ?', function(button){
												if (button == 'yes'){
													
														dataSourcePenambahan_MutasiInventaris_Inv.removeAt(line);
														DataGridPenambahanMutasiInventaris.getView().refresh();
												} 
												
											});
										}
									} else{
										ShowPesanErrorInvMutasiInventaris('Data tidak bisa dihapus karena minimal data 1','Error');
									}
									
								}
							}	
						],
						items:
						[
							gridPenambahan_MutasiInventaris()
						]
					
				
				
			}
		],
		tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAdd_MutasiPenambahanInventaris_Inv',
						handler: function(){
							addnewPenambahan_MutasiInventaris_Inv();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_MutasiPenambahanInventaris_Inv',
						handler: function()
						{
							datasavePenambahan_MutasiInventaris_Inv();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_MutasiPenambahanInventaris_Inv',
						handler: function()
						{
							dataSourcePenambahan_MutasiInventaris_Inv.reload();
						}
					}
				]
			}
    };
        return items;
}

function PanelPemindahan(rowdata){
	var items = 
	{
		title:'Pemindahan',
		layout:'form',
		border: false,
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 110,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 10,
								xtype: 'label',
								text: 'Nomor'
							},
							{
								x: 120,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							InvMutasiInventaris.vars.mutasiPemindahan=new Nci.form.Combobox.autoComplete({
								x: 130,
								y: 10,
								width:100,
								tabIndex:2,
								store	: InvMutasiInventaris.form.ArrayStore.mutasiPemindahan,
								select	: function(a,b,c){
									dsPemindahan_MutasiInv(b.data.no_mutasi);
									Ext.getCmp('tglMutasiPemindahanInventaris_Inv').setValue(ShowDate(b.data.tgl_mutasi));
									Ext.getCmp('TxtKeteranganMutasiPemindahanFilterGridDataView_MutasiInventaris_Inv').setValue(b.data.keterangan);
								},
								insert	: function(o){
									return {
										no_mutasi	: o.no_mutasi,
										tgl_mutasi	: o.tgl_mutasi,
										keterangan	: o.keterangan,
										jen_mutasi	: o.jen_mutasi,
										text		: '<table style="font-size: 11px;"><tr><td width="70">'+o.no_mutasi+'</td><td width="70">'+o.tgl_mutasi+'</td><td width="250">'+o.keterangan+'</td></tr></table>'
									}
								},
								url		: baseURL + "index.php/inventaris/functionMutasiInv/getDataAutoKomplitPemindahanMutasi",
								valueField: 'no_mutasi',
								displayField: 'text',
								listWidth: 380
							}),
							{
								x: 240,
								y: 10,
								xtype: 'label',
								text: 'Tanggal'
							},
							{
								x: 290,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 300,
								y: 10,
								xtype: 'datefield',
								id: 'tglMutasiPemindahanInventaris_Inv',
								name: 'tglMutasiPemindahanInventaris_Inv',
								width: 150,
								format: 'd/M/Y',
								tabIndex:3,
								value: now_MutasiInventaris_Inv,
							},
							{
								x: 10,
								y: 40,
								xtype: 'label',
								text: 'Keterangan'
							},
							{
								x: 120,
								y: 40,
								xtype: 'label',
								text: ':'
							},{
							x: 130,
							y: 40,
							xtype: 'textarea',
							id: 'TxtKeteranganMutasiPemindahanFilterGridDataView_MutasiInventaris_Inv',
							name: 'TxtKeteranganMutasiPemindahanFilterGridDataView_MutasiInventaris_Inv',
							emptyText: '',
							width: 320,
							tabIndex:1,
							
						},
						
						]
					}
				]
			},
			{
				
						layout: 'form',
						//bodyStyle:'padding: 5px',
						border: true,
						tbar:
						[
							{
								text	: 'Add Barang',
								id		: 'btnAddPemindahanBarangInvMutasiInventarisL',
								tooltip	: nmLookup,
								iconCls	: 'find',
								handler	: function(){
									var records = new Array();
									records.push(new dataSourcePemindahan_MutasiInventaris_Inv.recordType());
									dataSourcePemindahan_MutasiInventaris_Inv.add(records);
									console.log(dataSourcePemindahan_MutasiInventaris_Inv.getCount()-1);
									//getNoRegRuangOtomatis(dataSource_RuangTempatInventaris_Inv.getCount()-1);
								}
							},{
								xtype: 'tbseparator'
							},{
								xtype: 'button',
								text: 'Delete',
								iconCls: 'remove',
								id: 'btnDeletePemindahanBarangInvMutasiInventaris',
								handler: function()
								{
									var line = DataGridPemindahanMutasiInventaris.getSelectionModel().selection.cell[0];
									var o = dataSourcePemindahan_MutasiInventaris_Inv.getRange()[line].data;
									if(dataSourcePemindahan_MutasiInventaris_Inv.getCount()>1){
										if(dataSourcePemindahan_MutasiInventaris_Inv.getRange()[line].data.no_reg_ruang != undefined){
													ShowPesanWarningInvMutasiInventaris('Tidak bisa menghapus data ini', 'Perhatian');			
										}else{
											Ext.Msg.confirm('Warning', 'Apakah yakin akan dihapus ?', function(button){
												if (button == 'yes'){
													
														dataSourcePemindahan_MutasiInventaris_Inv.removeAt(line);
														DataGridPemindahanMutasiInventaris.getView().refresh();
														
													
												} 
												
											});
										}
									} else{
										ShowPesanErrorInvMutasiInventaris('Data tidak bisa dihapus karena minimal data 1','Error');
									}
									
								}
							}	
						],
						items:
						[
							gridPemindahan_MutasiInventaris()
						]
					
				
				
			}
		],
		tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAdd_MutasiPemindahanInventaris_Inv',
						handler: function(){
							addnewPemindahan_MutasiInventaris_Inv();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_MutasiPemindahanInventaris_Inv',
						handler: function()
						{
							datasavePemindahan_MutasiInventaris_Inv();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_MutasiPemindahanInventaris_Inv',
						handler: function()
						{
							dataSourcePemindahan_MutasiInventaris_Inv.reload();
						}
					}
				]
			}
    };
        return items;
}
function PanelPengurangan(rowdata){
	var items = 
	{
		title:'Pengurangan',
		layout:'form',
		border: false,
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 110,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 10,
								xtype: 'label',
								text: 'Nomor'
							},
							{
								x: 120,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							InvMutasiInventaris.vars.mutasiPengurangan=new Nci.form.Combobox.autoComplete({
								x: 130,
								y: 10,
								width:100,
								tabIndex:2,
								store	: InvMutasiInventaris.form.ArrayStore.mutasiPengurangan,
								select	: function(a,b,c){
									dsPengurangan_MutasiInv(b.data.no_mutasi);
									Ext.getCmp('tglMutasiPenguranganInventaris_Inv').setValue(ShowDate(b.data.tgl_mutasi));
									Ext.getCmp('TxtKeteranganMutasiPenguranganFilterGridDataView_MutasiInventaris_Inv').setValue(b.data.keterangan);
								},
								insert	: function(o){
									return {
										no_mutasi	: o.no_mutasi,
										tgl_mutasi	: o.tgl_mutasi,
										keterangan	: o.keterangan,
										jen_mutasi	: o.jen_mutasi,
										text		: '<table style="font-size: 11px;"><tr><td width="70">'+o.no_mutasi+'</td><td width="70">'+o.tgl_mutasi+'</td><td width="250">'+o.keterangan+'</td></tr></table>'
									}
								},
								url		: baseURL + "index.php/inventaris/functionMutasiInv/getDataAutoKomplitPenguranganMutasi",
								valueField: 'no_mutasi',
								displayField: 'text',
								listWidth: 380
							}),
							{
								x: 240,
								y: 10,
								xtype: 'label',
								text: 'Tanggal'
							},
							{
								x: 290,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 300,
								y: 10,
								xtype: 'datefield',
								id: 'tglMutasiPenguranganInventaris_Inv',
								name: 'tglMutasiPenguranganInventaris_Inv',
								width: 150,
								format: 'd/M/Y',
								tabIndex:3,
								value: now_MutasiInventaris_Inv,
							},
							{
								x: 10,
								y: 40,
								xtype: 'label',
								text: 'Keterangan'
							},
							{
								x: 120,
								y: 40,
								xtype: 'label',
								text: ':'
							},{
							x: 130,
							y: 40,
							xtype: 'textarea',
							id: 'TxtKeteranganMutasiPenguranganFilterGridDataView_MutasiInventaris_Inv',
							name: 'TxtKeteranganMutasiPenguranganFilterGridDataView_MutasiInventaris_Inv',
							emptyText: '',
							width: 320,
							tabIndex:1,
							
						},
						
						]
					}
				]
			},
			{
				
						layout: 'form',
						//bodyStyle:'padding: 5px',
						border: true,
						tbar:
						[
							{
								text	: 'Add Barang',
								id		: 'btnAddPenguranganBarangInvMutasiInventarisL',
								tooltip	: nmLookup,
								iconCls	: 'find',
								handler	: function(){
									var records = new Array();
									records.push(new dataSourcePengurangan_MutasiInventaris_Inv.recordType());
									dataSourcePengurangan_MutasiInventaris_Inv.add(records);
									console.log(dataSourcePengurangan_MutasiInventaris_Inv.getCount()-1);
									//getNoRegRuangOtomatis(dataSource_RuangTempatInventaris_Inv.getCount()-1);
								}
							},{
								xtype: 'tbseparator'
							},{
								xtype: 'button',
								text: 'Delete',
								iconCls: 'remove',
								id: 'btnDeletePenguranganBarangInvMutasiInventaris',
								handler: function()
								{
									var line = DataGridPenguranganMutasiInventaris.getSelectionModel().selection.cell[0];
									var o = dataSourcePengurangan_MutasiInventaris_Inv.getRange()[line].data;
									if(dataSourcePengurangan_MutasiInventaris_Inv.getCount()>1){
										if(dataSourcePengurangan_MutasiInventaris_Inv.getRange()[line].data.no_reg_ruang != undefined){
													ShowPesanErrorInvMutasiInventaris('Tidak bisa menghapus data ini', 'Perhatian');			
										}else{
											Ext.Msg.confirm('Warning', 'Apakah yakin akan dihapus ?', function(button){
												if (button == 'yes'){
													
														dataSourcePengurangan_MutasiInventaris_Inv.removeAt(line);
														DataGridPenguranganMutasiInventaris.getView().refresh();
														
													
												} 
												
											});
										}
									} else{
										ShowPesanErrorInvMutasiInventaris('Data tidak bisa dihapus karena minimal data 1','Error');
									}
									
								}
							}	
						],
						items:
						[
							gridPengurangan_MutasiInventaris()
						]
					
				
				
			}
		],
		tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAdd_MutasiPenguranganInventaris_Inv',
						handler: function(){
							addnewPengurangan_MutasiInventaris_Inv();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_MutasiPenguranganInventaris_Inv',
						handler: function()
						{
							datasavePengurangan_MutasiInventaris_Inv();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_MutasiPenguranganInventaris_Inv',
						handler: function()
						{
							dataSourcePengurangan_MutasiInventaris_Inv.reload();
						}
					}
				]
			}
    };
        return items;
}
function dsPenambahan_MutasiInv(nomutasi)
{
	dataSourcePenambahan_MutasiInventaris_Inv.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: '',
					Sortdir: 'ASC',
					target: 'ViGridPenambahanMutasi',
					param: 'IDM.NO_MUTASI =~'+nomutasi+'~ ORDER BY IDM.NO_BARIS'
				}
		}
	);  
	return dataSourcePenambahan_MutasiInventaris_Inv;
}
function gridPenambahan_MutasiInventaris(){
    var FieldGrd_viPenambahanMutasiInventaris = ['tgl_masuk','no_mutasi', 'kd_inv','no_urut_brg','nama_brg','satuan','jml_masuk','no_ruang','nama_sub','no_baris','no_register'];
    
	dataSourcePenambahan_MutasiInventaris_Inv= new WebApp.DataStore({
		fields: FieldGrd_viPenambahanMutasiInventaris
    });
	dsPenambahan_MutasiInv();
    DataGridPenambahanMutasiInventaris =new Ext.grid.EditorGridPanel({
        xtype: 'editorgrid',
		store: dataSourcePenambahan_MutasiInventaris_Inv,
        height:600,
		width:'100%',
        autoScroll: true,
        columnLines: true,
		border: true,
		anchor: '100% 84%',
		flex:1,
		listeners:
		{
			// Function saat ada event double klik maka akan muncul form view # --------------
			rowdblclick: function (sm, ridx, cidx)
			{
				
			}
			// End Function # --------------
		},
		selModel: new Ext.grid.CellSelectionModel ({
            singleSelect: true,
            listeners:{
                rowselect: function(sm, row, rec){
                }
            }
        }),
        stripeRows: true,
        cm:new Ext.grid.ColumnModel([
        	new Ext.grid.RowNumberer(),	
			{
				dataIndex: 'kd_inv',
				header: 'Kode Kelompok ',
				sortable: true,
				width: 70
			},
			{
				dataIndex: 'tgl_masuk',
				header: 'Tanggal Masuk ',
				sortable: true,
				hidden:true,
				width: 70
			},
			{
				dataIndex: 'no_urut_brg',
				header: 'Urutan ',
				width: 70
			},
			{
				dataIndex: 'no_register',
				header: 'No Register ',
				width: 70,
				editor:new Nci.form.Combobox.autoComplete({
							store	: InvMutasiInventaris.form.ArrayStore.gridMutasiPenambahan,
							select	: function(a,b,c){
								var line	=DataGridPenambahanMutasiInventaris.getSelectionModel().selection.cell[0];
								dataSourcePenambahan_MutasiInventaris_Inv.getRange()[line].data.no_register=b.data.no_register;
								dataSourcePenambahan_MutasiInventaris_Inv.getRange()[line].data.tgl_masuk=b.data.tgl_masuk;
								dataSourcePenambahan_MutasiInventaris_Inv.getRange()[line].data.jml_masuk=b.data.jml;
								dataSourcePenambahan_MutasiInventaris_Inv.getRange()[line].data.kd_inv=b.data.kd_inv;
								dataSourcePenambahan_MutasiInventaris_Inv.getRange()[line].data.no_urut_brg=b.data.no_urut_brg;
								dataSourcePenambahan_MutasiInventaris_Inv.getRange()[line].data.nama_brg=b.data.nama_brg;
								dataSourcePenambahan_MutasiInventaris_Inv.getRange()[line].data.no_ruang=b.data.no_ruang;
								dataSourcePenambahan_MutasiInventaris_Inv.getRange()[line].data.nama_sub=b.data.nama_sub;
								dataSourcePenambahan_MutasiInventaris_Inv.getRange()[line].data.ket=b.data.ket;
								dataSourcePenambahan_MutasiInventaris_Inv.getRange()[line].data.satuan=b.data.satuan;
								DataGridPenambahanMutasiInventaris.getView().refresh();
						},
					insert	: function(o,b,c){
						return {
							tgl_masuk		: o.tgl_masuk,
							kd_inv 			: o.kd_inv,
							no_register		: o.no_register,
							no_urut_brg		: o.no_urut_brg,
							no_reg_ruang	: o.no_reg_ruang,
							nama_brg		: o.nama_brg,
							nama_sub		: o.nama_sub,
							jml				: o.jml,
							no_ruang		: o.no_ruang,
							ket		        : o.ket,
							satuan			: o.satuan,
							text			:  '<table style="font-size: 11px;"><tr><td width="100">'+o.kd_inv+'</td><td width="70">'+o.no_register+'</td><td width="70">'+o.no_urut_brg+'</td><td width="100">'+o.nama_brg+'</td><td width="100">'+o.nama_sub+'</td><td width="200">'+o.ket+'</td><td width="200">'+o.satuan+'</td></tr></table>',
						}
					},
					param:function(){
							return {
								
							}
					},
					url		: baseURL + "index.php/inventaris/functionMutasiInv/getAutoKomplitGridMutasi",
					valueField: 'no_register',
					displayField: 'text',
					listWidth: 800
				})
			},
			{
				dataIndex: 'nama_brg',
				header: 'Barang',
				width: 170
			},
			{
				dataIndex: 'satuan',
				header: 'Satuan',
				width: 70
			},
			{
				dataIndex: 'jml_masuk',
				xtype:'numbercolumn',
				align:'right',
				header: 'Jumlah',
				width: 70
			},
			{
				dataIndex: 'nama_sub',
				header: 'Kelompok',
				width: 70
			},
			//-------------- ## --------------
        ]),
		viewConfig:{
			forceFit: true
		}
    });
    return DataGridPenambahanMutasiInventaris;
}
function dsPemindahan_MutasiInv(nomutasi)
{
	dataSourcePemindahan_MutasiInventaris_Inv.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: '',
					Sortdir: 'ASC',
					target: 'ViGridPemindahanMutasi',
					param: 'IDM.NO_MUTASI =~'+nomutasi+'~ AND IDR.NO_REGISTER=IDM.NO_REGISTER ORDER BY IDM.NO_BARIS'
				}
		}
	);  
	return dataSourcePemindahan_MutasiInventaris_Inv;
}
function gridPemindahan_MutasiInventaris(){
    var FieldGrd_viPemindahanMutasiInventaris = ['no_mutasi', 'kd_inv','no_urut_brg','nama_brg','no_ruang_baru','ruang_baru','jml_pindah','no_register','jml_masuk','no_ruang_lama','ruang_lama','no_reg_ruang','satuan','nama_sub','no_baris'];
    
	dataSourcePemindahan_MutasiInventaris_Inv= new WebApp.DataStore({
		fields: FieldGrd_viPemindahanMutasiInventaris
    });  
    
   	dsPemindahan_MutasiInv();
    DataGridPemindahanMutasiInventaris =new Ext.grid.EditorGridPanel({
        xtype: 'editorgrid',
		store: dataSourcePemindahan_MutasiInventaris_Inv,
        height:600,
		width:'100%',
		autoScroll: true,
        columnLines: true,
		border: true,
		anchor: '100% 64%',
		flex:1,
		listeners:
		{
			// Function saat ada event double klik maka akan muncul form view # --------------
			rowdblclick: function (sm, ridx, cidx)
			{
				
			}
			// End Function # --------------
		},
		selModel: new Ext.grid.CellSelectionModel ({
            singleSelect: true,
            listeners:{
                rowselect: function(sm, row, rec){
                }
            }
        }),
        stripeRows: true,
        cm:new Ext.grid.ColumnModel([
        	new Ext.grid.RowNumberer(),	
			{
				dataIndex: 'kd_inv',
				header: 'Kode Kelompok ',
				sortable: true,
				width: 70
			},
			{
				dataIndex: 'no_urut_brg',
				header: 'Urutan ',
				sortable: true,
				width: 70
			},
			{
				dataIndex: 'no_register',
				header: 'No Register ',
				sortable: true,
				width: 70,
				editor:new Nci.form.Combobox.autoComplete({
							store	: InvMutasiInventaris.form.ArrayStore.gridMutasiPemindahan,
							select	: function(a,b,c){
								var line	=DataGridPemindahanMutasiInventaris.getSelectionModel().selection.cell[0];
								dataSourcePemindahan_MutasiInventaris_Inv.getRange()[line].data.no_register=b.data.no_register;
								dataSourcePemindahan_MutasiInventaris_Inv.getRange()[line].data.tgl_masuk=b.data.tgl_masuk;
								dataSourcePemindahan_MutasiInventaris_Inv.getRange()[line].data.jml_masuk=b.data.jml_masuk;
								dataSourcePemindahan_MutasiInventaris_Inv.getRange()[line].data.jml_pindah=b.data.jml_pindah;
								dataSourcePemindahan_MutasiInventaris_Inv.getRange()[line].data.no_reg_ruang=b.data.no_reg_ruang;
								dataSourcePemindahan_MutasiInventaris_Inv.getRange()[line].data.no_ruang_lama=b.data.no_ruang_lama;
								dataSourcePemindahan_MutasiInventaris_Inv.getRange()[line].data.ruang_lama=b.data.ruang_lama;
								dataSourcePemindahan_MutasiInventaris_Inv.getRange()[line].data.no_ruang_baru=b.data.no_ruang_baru;
								dataSourcePemindahan_MutasiInventaris_Inv.getRange()[line].data.ruang_baru=b.data.ruang_baru;
								dataSourcePemindahan_MutasiInventaris_Inv.getRange()[line].data.kd_inv=b.data.kd_inv;
								dataSourcePemindahan_MutasiInventaris_Inv.getRange()[line].data.no_urut_brg=b.data.no_urut_brg;
								dataSourcePemindahan_MutasiInventaris_Inv.getRange()[line].data.nama_brg=b.data.nama_brg;
								dataSourcePemindahan_MutasiInventaris_Inv.getRange()[line].data.nama_sub=b.data.nama_sub;
								dataSourcePemindahan_MutasiInventaris_Inv.getRange()[line].data.ket=b.data.ket;
								dataSourcePemindahan_MutasiInventaris_Inv.getRange()[line].data.satuan=b.data.satuan;
								DataGridPemindahanMutasiInventaris.getView().refresh();
						},
					insert	: function(o,b,c){
						return {
							tgl_masuk		: o.tgl_masuk,
							kd_inv 			: o.kd_inv,
							no_register		: o.no_register,
							no_urut_brg		: o.no_urut_brg,
							no_ruang_lama		: o.no_ruang_lama,
							ruang_lama			: o.ruang_lama,
							no_ruang_baru		: o.no_ruang_baru,
							ruang_baru			: o.ruang_baru,
							no_reg_ruang	: o.no_reg_ruang,
							nama_brg		: o.nama_brg,
							nama_sub		: o.nama_sub,
							jml_masuk				: o.jml_masuk,
							jml_pindah				: o.jml_pindah,
							satuan			: o.satuan,
							text			:  '<table style="font-size: 11px;"><tr><td width="100">'+o.kd_inv+'</td><td width="70">'+o.no_register+'</td><td width="70">'+o.no_reg_ruang+'</td><td width="70">'+o.no_urut_brg+'</td><td width="100">'+o.nama_brg+'</td><td width="100">'+o.nama_sub+'</td><td width="200">'+o.satuan+'</td></tr></table>',
						}
					},
					param:function(){
							return {
								
							}
					},
					url		: baseURL + "index.php/inventaris/functionMutasiInv/getAutoKomplitPemindahanGridMutasi",
					valueField: 'no_register',
					displayField: 'text',
					listWidth: 500
				})
				//hidden:true,
			},
			{
				dataIndex: 'nama_brg',
				header: 'Barang',
				width: 70
			},
			{
				dataIndex: 'no_ruang_lama',
				header: 'No Ruang Lama',
				width: 70
			},
			{
				dataIndex: 'ruang_lama',
				header: 'Lokasi Lama',
				width: 70,
				renderer: function(v, params, record)
				{
					var lokasi='Tidak Diketahui';
					if (record.data.ruang_lama==undefined)
						return lokasi;
					else
						return record.data.ruang_lama;
				},	
			},
			{
				xtype:'numbercolumn',
				align:'right',
				dataIndex: 'jml_masuk',
				header: 'Jumlah',
				width: 70
			},
			{
				dataIndex: 'no_reg_ruang',
				header: 'No Reg Ruang',
				width: 70,
				hidden:true,
			},
			{
				dataIndex: 'no_ruang_baru',
				header: 'No Ruang Baru',
				width: 70
			},
			
			{
				
				dataIndex: 'ruang_baru',
				header: 'Lokasi Baru',
				width: 70,
				renderer: function(v, params, record)
				{
					var lokasi='Tidak Diketahui';
					if (record.data.ruang_baru==undefined)
						return lokasi;
					else
						return record.data.ruang_baru;
				},	
				editor:new Nci.form.Combobox.autoComplete({
							store	: InvMutasiInventaris.form.ArrayStore.gridMutasiLokasiBaruPemindahan,
							select	: function(a,b,c){
								var line	=DataGridPemindahanMutasiInventaris.getSelectionModel().selection.cell[0];
								dataSourcePemindahan_MutasiInventaris_Inv.getRange()[line].data.no_ruang_baru=b.data.no_ruang;
								dataSourcePemindahan_MutasiInventaris_Inv.getRange()[line].data.ruang_baru=b.data.lokasi;
								DataGridPemindahanMutasiInventaris.getView().refresh();
						},
					insert	: function(o,b,c){
						return {
							no_ruang		: o.no_ruang,
							lokasi 			: o.lokasi,
							text			:  '<table style="font-size: 11px;"><tr><td width="100">'+o.no_ruang+'</td><td width="70">'+o.lokasi+'</td></tr></table>',
						}
					},
					param:function(){
							return {
								
							}
					},
					url		: baseURL + "index.php/inventaris/functionMutasiInv/getAutoKomplitGridLokasiPemindahanMutasi",
					valueField: 'no_ruang_baru',
					displayField: 'text',
					listWidth: 200
				})
			},
			{
				xtype:'numbercolumn',
				align:'right',
				dataIndex: 'jml_pindah',
				header: 'Jumlah Pindah',
				width: 70,
				editor: new Ext.form.NumberField({
							allowBlank: false,
							listeners:{
								blur: function(a){
								},
								focus: function(a){
								}
							}
						}),
			},
			{
				dataIndex: 'satuan',
				header: 'Satuan',
				width: 70
			},
			{
				dataIndex: 'nama_sub',
				header: 'Kelompok',
				width: 70
			}
			//-------------- ## --------------
        ]),
		viewConfig:{
			forceFit: true
		}
    });
    return DataGridPemindahanMutasiInventaris;
}
function dsPengurangan_MutasiInv(nomutasi)
{
	dataSourcePengurangan_MutasiInventaris_Inv.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: '',
					Sortdir: 'ASC',
					target: 'ViGridPenguranganMutasi',
					param: 'IDM.NO_MUTASI =~'+nomutasi+'~ AND IDR.NO_REGISTER=IDM.NO_REGISTER ORDER BY IDM.NO_BARIS'
				}
		}
	);  
	return dataSourcePengurangan_MutasiInventaris_Inv;
}
function gridPengurangan_MutasiInventaris(){
    var FieldGrd_viPenguranganMutasiInventaris = ['no_mutasi', 'kd_inv','no_urut_brg','nama_brg','no_ruang_lama','no_ruang_baru','ruang_lama','ruang_baru','lokasi','jml_aktual_lama','no_register','jml_kurang','jml_sisa','hrg_kurang','no_reg_ruang','satuan','nama_sub','no_baris'];
    
	dataSourcePengurangan_MutasiInventaris_Inv= new WebApp.DataStore({
		fields: FieldGrd_viPenguranganMutasiInventaris
    });
	dsPengurangan_MutasiInv();
    DataGridPenguranganMutasiInventaris =new Ext.grid.EditorGridPanel({
        xtype: 'editorgrid',
		store: dataSourcePengurangan_MutasiInventaris_Inv,
        height:600,
		width:'100%',
        autoScroll: true,
        columnLines: true,
		border: true,
		anchor: '100% 64%',
		flex:1,
		listeners:
		{
			// Function saat ada event double klik maka akan muncul form view # --------------
			rowdblclick: function (sm, ridx, cidx)
			{
				
			}
			// End Function # --------------
		},
		selModel: new Ext.grid.CellSelectionModel ({
            singleSelect: true,
            listeners:{
                rowselect: function(sm, row, rec){
                }
            }
        }),
        stripeRows: true,
        cm:new Ext.grid.ColumnModel([
        	new Ext.grid.RowNumberer(),	
			{
				dataIndex: 'kd_inv',
				header: 'Kode Kelompok ',
				sortable: true,
				width: 70
			},
			{
				dataIndex: 'no_urut_brg',
				header: 'Urutan ',
				sortable: true,
				width: 70
			},
			{
				dataIndex: 'no_register',
				header: 'No Register ',
				sortable: true,
				width: 70,
				editor:new Nci.form.Combobox.autoComplete({
							store	: InvMutasiInventaris.form.ArrayStore.gridMutasiPengurangan,
							select	: function(a,b,c){
								var line	=DataGridPenguranganMutasiInventaris.getSelectionModel().selection.cell[0];
								dataSourcePengurangan_MutasiInventaris_Inv.getRange()[line].data.no_register=b.data.no_register;
								dataSourcePengurangan_MutasiInventaris_Inv.getRange()[line].data.tgl_masuk=b.data.tgl_masuk;
								dataSourcePengurangan_MutasiInventaris_Inv.getRange()[line].data.jml_aktual_lama=b.data.jml_masuk;
								dataSourcePengurangan_MutasiInventaris_Inv.getRange()[line].data.jml_kurang=b.data.jml_kurang;
								dataSourcePengurangan_MutasiInventaris_Inv.getRange()[line].data.kd_inv=b.data.kd_inv;
								dataSourcePengurangan_MutasiInventaris_Inv.getRange()[line].data.no_urut_brg=b.data.no_urut_brg;
								if (b.data.no_ruang_baru=='')
								{
									dataSourcePengurangan_MutasiInventaris_Inv.getRange()[line].data.no_ruang=b.data.no_ruang_lama;
								}
								else
								{
									dataSourcePengurangan_MutasiInventaris_Inv.getRange()[line].data.no_ruang=b.data.no_ruang_baru;
								}
								//console.log(b.data.ruang_baru);
								if (b.data.ruang_baru==null)
								{ 
									dataSourcePengurangan_MutasiInventaris_Inv.getRange()[line].data.lokasi=b.data.ruang_lama;
								}
								 else
								{
									dataSourcePengurangan_MutasiInventaris_Inv.getRange()[line].data.lokasi=b.data.ruang_baru;
								} 
								dataSourcePengurangan_MutasiInventaris_Inv.getRange()[line].data.nama_brg=b.data.nama_brg;
								dataSourcePengurangan_MutasiInventaris_Inv.getRange()[line].data.nama_sub=b.data.nama_sub;
								dataSourcePengurangan_MutasiInventaris_Inv.getRange()[line].data.hrg_kurang=b.data.hrg_kurang;
								dataSourcePengurangan_MutasiInventaris_Inv.getRange()[line].data.jml_sisa=b.data.jml_sisa;
								dataSourcePengurangan_MutasiInventaris_Inv.getRange()[line].data.ket=b.data.ket;
								dataSourcePengurangan_MutasiInventaris_Inv.getRange()[line].data.satuan=b.data.satuan;
								DataGridPenguranganMutasiInventaris.getView().refresh();
						},
					insert	: function(o,b,c){
						return {
							tgl_masuk		: o.tgl_masuk,
							kd_inv 			: o.kd_inv,
							no_register		: o.no_register,
							no_urut_brg		: o.no_urut_brg,
							no_ruang_lama		: o.no_ruang_lama,
							ruang_lama			: o.ruang_lama,
							no_ruang_baru		: o.no_ruang_baru,
							ruang_baru			: o.ruang_baru,
							no_reg_ruang	: o.no_reg_ruang,
							hrg_kurang		: o.hrg_kurang,
							jml_sisa		: o.jml_sisa,
							nama_brg		: o.nama_brg,
							nama_sub		: o.nama_sub,
							jml_masuk				: o.jml_masuk,
							jml_pindah				: o.jml_pindah,
							jml_kurang				: o.jml_kurang,
							satuan			: o.satuan,
							text			:  '<table style="font-size: 11px;"><tr><td width="100">'+o.kd_inv+'</td><td width="70">'+o.no_register+'</td><td width="70">'+o.no_reg_ruang+'</td><td width="70">'+o.no_urut_brg+'</td><td width="100">'+o.nama_brg+'</td><td width="100">'+o.nama_sub+'</td><td width="200">'+o.satuan+'</td></tr></table>',
						}
					},
					param:function(){
							return {
								
							}
					},
					url		: baseURL + "index.php/inventaris/functionMutasiInv/getAutoKomplitPenguranganGridMutasi",
					valueField: 'no_register',
					displayField: 'text',
					listWidth: 800
				})
			},
			{
				dataIndex: 'nama_brg',
				header: 'Barang',
				width: 70
			},
			{
				dataIndex: 'no_ruang',
				header: 'No. Ruang',
				width: 70,/* 
				renderer: function(v, params, record)
				{
					//var lokasi='Tidak Diketahui';
					console.log(record.data);
					if (record.data.no_ruang==undefined)
						return record.data.no_ruang_lama;
					else
						return record.data.no_ruang_baru;
				}, */
			},
			{
				dataIndex: 'lokasi',
				header: 'Lokasi',
				width: 70/* ,
				renderer: function(v, params, record)
				{
					var lokasi='Tidak Diketahui';
					if (record.data.ruang_baru==undefined)
						return lokasi;
					else
						return record.data.ruang_baru;
				}, */
			},
			{
				xtype:'numbercolumn',
				align:'right',
				dataIndex: 'jml_aktual_lama',
				header: 'Jumlah',
				width: 70
			},
			{
				xtype:'numbercolumn',
				align:'right',
				dataIndex: 'jml_kurang',
				header: 'Jumlah Kurang',
				width: 70,
				editor: new Ext.form.NumberField({
							allowBlank: false,
							listeners:{
								blur: function(a){
									var line	= DataGridPenguranganMutasiInventaris.getSelectionModel().selection.cell[0];
										var jumlah_masuk = dataSourcePengurangan_MutasiInventaris_Inv.data.items[line].data.jml_aktual_lama;
										var jumlah_kurang = dataSourcePengurangan_MutasiInventaris_Inv.data.items[line].data.jml_kurang;
										if (jumlah_masuk<jumlah_kurang)
										{
											ShowPesanErrorInvMutasiInventaris('Jumlah Kurang Jangan Melebihi Jumlah Barang', 'Error');
											dataSourcePengurangan_MutasiInventaris_Inv.getRange()[line].set("jml_kurang", 0);
										}
										else
										{
											dataSourcePengurangan_MutasiInventaris_Inv.getRange()[line].set("jml_sisa", jumlah_masuk - jumlah_kurang);
										}
										
									
								},
								focus: function(a){
								}
							}
						}),
			},
			{
				xtype:'numbercolumn',
				align:'right',
				dataIndex: 'hrg_kurang',
				header: 'Harga',
				width: 70,/* 
				editor: new Ext.form.NumberField({
							allowBlank: false,
							listeners:{
								blur: function(a){
								},
								focus: function(a){
								}
							}
						}), */
			},
			{
				xtype:'numbercolumn',
				align:'right',
				dataIndex: 'jml_sisa',
				header: 'Jumlah Sisa',
				width: 70
			},
			{
				dataIndex: 'satuan',
				header: 'Satuan',
				width: 70
			},
			{
				dataIndex: 'nama_sub',
				header: 'Kelompok',
				width: 70
			}
			//-------------- ## --------------
        ]),
		viewConfig:{
			forceFit: true
		}
    });
    return DataGridPenguranganMutasiInventaris;
}

//------------------GRID SUB  OBAT---------------------------------------------------------
function gridCekKondisi_MutasiInventaris(){
    var FieldMutasi_Inventaris_Inv = ['kd_sub_jns', 'sub_'];
    
	dsDataMutasi_Inventaris_Inv= new WebApp.DataStore({
		fields: FieldMutasi_Inventaris_Inv
    });
	dsDataMutasi_Inventaris_Inv.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: '',
					Sortdir: 'ASC',
					target: 'ViGridSubObat',
					param: ''
				}
		}
	);  
    
   
    DataGridSubObat =new Ext.grid.EditorGridPanel({
        xtype: 'editorgrid',
		store: dsDataMutasi_Inventaris_Inv,
        height:190,
		width:'100%',
        columnLines: true,
		border: true,
		flex:1,
		listeners:
		{
			// Function saat ada event double klik maka akan muncul form view # --------------
			rowdblclick: function (sm, ridx, cidx)
			{
				rowSelected_MutasiInventaris_Inv = dsDataMutasi_Inventaris_Inv.getAt(ridx);
				if (rowSelected_MutasiInventaris_Inv != undefined)
				{
					DataInitPanelSubObat(rowSelected_MutasiInventaris_Inv.data);
				}
			}
			// End Function # --------------
		},
		selModel: new Ext.grid.CellSelectionModel ({
            singleSelect: true,
            listeners:{
                rowselect: function(sm, row, rec){
					DataInitPanelSubObat(rowSelected_MutasiInventaris_Inv.data);
                }
            }
        }),
        stripeRows: true,
        cm:new Ext.grid.ColumnModel([
        	new Ext.grid.RowNumberer(),	
			{
				dataIndex: 'kd_sub_jns',
				header: 'Kode Sub ',
				sortable: true,
				width: 70
			},
			{
				dataIndex: 'sub_',
				header: 'Sub  obat',
				width: 70
			}
			//-------------- ## --------------
        ]),
		viewConfig:{
			forceFit: true
		}
    });
    return DataGridSubObat;
}

//------------------GRID GOLONGAN OBAT---------------------------------------------------------
function gridGolonganObat_viSetupMasterObat(){
    var FieldGrdGolongan_viSetupMasterObat = ['apt_kd_golongan', 'apt_golongan'];
    
	dsDataGrdGolongan_viSetupMasterObat= new WebApp.DataStore({
		fields: FieldGrdGolongan_viSetupMasterObat
    });
	dsDataGrdGolongan_viSetupMasterObat.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: '',
					Sortdir: 'ASC',
					target: 'ViGridGolObat',
					param: ''
				}
		}
	);  
    
   
    DataGridGolongan =new Ext.grid.EditorGridPanel({
        xtype: 'editorgrid',
		title: 'List Golongan Obat',
		store: dsDataGrdGolongan_viSetupMasterObat,
        height: 300,
		width : '100%',
        columnLines: true,
		border: true,
		flex:1,
		listeners:
		{
			// Function saat ada event double klik maka akan muncul form view # --------------
			rowdblclick: function (sm, ridx, cidx)
			{
				rowSelected_MutasiInventaris_Inv = dsDataGrdGolongan_viSetupMasterObat.getAt(ridx);
				if (rowSelected_MutasiInventaris_Inv != undefined)
				{
					DataInitPanelGolongan(rowSelected_MutasiInventaris_Inv.data);
				}
			}
			// End Function # --------------
		},
		selModel: new Ext.grid.CellSelectionModel ({
            singleSelect: true,
            listeners:{
                rowselect: function(sm, row, rec){
					DataInitPanelGolongan(rowSelected_MutasiInventaris_Inv.data);
                }
            }
        }),
        stripeRows: true,
        cm:new Ext.grid.ColumnModel([
        	new Ext.grid.RowNumberer(),	
			{
				dataIndex: 'apt_kd_golongan',
				header: 'Kode Golongan',
				sortable: true,
				width: 70
			},
			{
				dataIndex: 'apt_golongan',
				header: 'Golongan',
				width: 70
			}
			//-------------- ## --------------
        ]),
		viewConfig:{
			forceFit: true
		}
    });
    return DataGridGolongan;
}

//------------------GRID SATUAN OBAT---------------------------------------------------------
function gridSatuan_viSetupMasterObat(){
    var FieldGrdSatuan_viSetupMasterObat = ['kd_satuan', 'satuan'];
    
	dsDataGrdSatuan_viSetupMasterObat= new WebApp.DataStore({
		fields: FieldGrdSatuan_viSetupMasterObat
    });
	dsDataGrdSatuan_viSetupMasterObat.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: '',
					Sortdir: 'ASC',
					target: 'ViGridSatuan',
					param: ''
				}
		}
	);  
    
   
    DataGridSatuan =new Ext.grid.EditorGridPanel({
        xtype: 'editorgrid',
		title: 'List Satuan Kecil',
		store: dsDataGrdSatuan_viSetupMasterObat,
        height: 300,
		width:550,
        columnLines: true,
		border: true,
		flex:1,
		listeners:
		{
			// Function saat ada event double klik maka akan muncul form view # --------------
			rowdblclick: function (sm, ridx, cidx)
			{
				rowSelected_MutasiInventaris_Inv = dsDataGrdSatuan_viSetupMasterObat.getAt(ridx);
				if (rowSelected_MutasiInventaris_Inv != undefined)
				{
					DataInitPanelSatuan(rowSelected_MutasiInventaris_Inv.data);
				}
			}
			// End Function # --------------
		},
		selModel: new Ext.grid.CellSelectionModel ({
            singleSelect: true,
            listeners:{
                rowselect: function(sm, row, rec){
					DataInitPanelSatuan(rowSelected_MutasiInventaris_Inv.data);
                }
            }
        }),
        stripeRows: true,
        cm:new Ext.grid.ColumnModel([
        	new Ext.grid.RowNumberer(),	
			{
				dataIndex: 'kd_satuan',
				header: 'Kode Satuan',
				sortable: true,
				width: 70
			},
			{
				dataIndex: 'satuan',
				header: 'Satuan',
				width: 70
			}
			//-------------- ## --------------
        ]),
		viewConfig:{
			forceFit: true
		}
    });
    return DataGridSatuan;
}

//------------------GRID SATUAN BESAR OBAT---------------------------------------------------------
function gridSatuanBesar_viSetupMasterObat(){
    var FieldGrdSatuanBesar_viSetupMasterObat = ['kd_sat_besar', 'keterangan'];
    
	dsDataGrdSatuanBesar_viSetupMasterObat= new WebApp.DataStore({
		fields: FieldGrdSatuanBesar_viSetupMasterObat
    });
	dsDataGrdSatuanBesar_viSetupMasterObat.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: '',
					Sortdir: 'ASC',
					target: 'ViGridSatuanBesar',
					param: ''
				}
		}
	);  
    
   
    DataGridSatuanBesar =new Ext.grid.EditorGridPanel({
        xtype: 'editorgrid',
		title: 'List Satuan Besar',
		store: dsDataGrdSatuanBesar_viSetupMasterObat,
        height: 300,
		width:550,
        columnLines: true,
		border: true,
		flex:1,
		listeners:
		{
			// Function saat ada event double klik maka akan muncul form view # --------------
			rowdblclick: function (sm, ridx, cidx)
			{
				rowSelected_MutasiInventaris_Inv = dsDataGrdSatuanBesar_viSetupMasterObat.getAt(ridx);
				if (rowSelected_MutasiInventaris_Inv != undefined)
				{
					DataInitPanelSatuanBesar(rowSelected_MutasiInventaris_Inv.data);
				}
			}
			// End Function # --------------
		},
		selModel: new Ext.grid.CellSelectionModel ({
            singleSelect: true,
            listeners:{
                rowselect: function(sm, row, rec){
					DataInitPanelSatuanBesar(rowSelected_MutasiInventaris_Inv.data);
                }
            }
        }),
        stripeRows: true,
        cm:new Ext.grid.ColumnModel([
        	new Ext.grid.RowNumberer(),	
			{
				dataIndex: 'kd_sat_besar',
				header: 'Kode Satuan',
				sortable: true,
				width: 70
			},
			{
				dataIndex: 'keterangan',
				header: 'Keterangan',
				width: 70
			}
			//-------------- ## --------------
        ]),
		viewConfig:{
			forceFit: true
		}
    });
    return DataGridSatuanBesar;
}


//------------------------------ Obat----------------------------------------------
function DataInitPanelObat(rowdata){
	Ext.getCmp('txtKdObat_InvMutasiInventarisL').setValue(rowdata.kd_jns_obt);
	InvMutasiInventaris.vars.nama.setValue(rowdata.nama_);
};

function addnewPenambahan_MutasiInventaris_Inv(){
	Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/inventaris/functionMutasiInv/getNoOtoMutasi",
				params: {
							jenmutasi:1
						},
				failure: function(o)
				{
					ShowPesanErrorInvMutasiInventaris('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						InvMutasiInventaris.vars.mutasiPenambahan.setValue(cst.nomutasi);
						Ext.getCmp('tglMutasiPenambahanInventaris_Inv').setValue(now_MutasiInventaris_Inv);
						Ext.getCmp('TxtKeteranganMutasiPenambahanFilterGridDataView_MutasiInventaris_Inv').setValue('');
						dsPenambahan_MutasiInv(cst.nomutasi);
						//dataSourcePenambahan_MutasiInventaris_Inv.reload();
					}
					else 
					{
						ShowPesanErrorInvMutasiInventaris('Gagal Mendapatkan No Mutasi', 'Error');
					};
				}
			}
			
		)
}
function addnewPemindahan_MutasiInventaris_Inv(){
	Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/inventaris/functionMutasiInv/getNoOtoMutasi",
				params: {
							jenmutasi:2
						},
				failure: function(o)
				{
					ShowPesanErrorInvMutasiInventaris('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						InvMutasiInventaris.vars.mutasiPemindahan.setValue(cst.nomutasi);
						Ext.getCmp('tglMutasiPemindahanInventaris_Inv').setValue(now_MutasiInventaris_Inv);
						Ext.getCmp('TxtKeteranganMutasiPemindahanFilterGridDataView_MutasiInventaris_Inv').setValue('');
						dsPemindahan_MutasiInv(cst.nomutasi);
					}
					else 
					{
						ShowPesanErrorInvMutasiInventaris('Gagal Mendapatkan No Mutasi', 'Error');
					};
				}
			}
			
		)
}
function addnewPengurangan_MutasiInventaris_Inv(){
	Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/inventaris/functionMutasiInv/getNoOtoMutasi",
				params: {
							jenmutasi:3
						},
				failure: function(o)
				{
					ShowPesanErrorInvMutasiInventaris('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						InvMutasiInventaris.vars.mutasiPengurangan.setValue(cst.nomutasi);
						Ext.getCmp('tglMutasiPenguranganInventaris_Inv').setValue(now_MutasiInventaris_Inv);
						Ext.getCmp('TxtKeteranganMutasiPenguranganFilterGridDataView_MutasiInventaris_Inv').setValue('');
						dsPengurangan_MutasiInv(cst.nomutasi);
					}
					else 
					{
						ShowPesanErrorInvMutasiInventaris('Gagal Mendapatkan No Mutasi', 'Error');
					};
				}
			}
			
		)
}
function datasavePenambahan_MutasiInventaris_Inv(){
	if (ValidasiEntryPenambahanMutasi(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/inventaris/functionMutasiInv/saveMutasi",
				params: getParamPenambahanMutasi(),
				failure: function(o)
				{
					ShowPesanErrorInvMutasiInventaris('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoInvMutasiInventaris(nmPesanSimpanSukses,nmHeaderSimpanData);
						dataSourcePenambahan_MutasiInventaris_Inv.reload();
					}
					else 
					{
						ShowPesanErrorInvMutasiInventaris('Gagal Menyimpan Data ini', 'Error');
						dataSourcePenambahan_MutasiInventaris_Inv.reload();
					};
				}
			}
			
		)
	}
}

function getParamPenambahanMutasi(){
	var	params =
	{
		panel:'penambahan',
		jumlahrec:dataSourcePenambahan_MutasiInventaris_Inv.getCount(),
		nomutasi:InvMutasiInventaris.vars.mutasiPenambahan.getValue(),
		tglmutasi:Ext.getCmp('tglMutasiPenambahanInventaris_Inv').getValue(),
		keterangan:Ext.getCmp('TxtKeteranganMutasiPenambahanFilterGridDataView_MutasiInventaris_Inv').getValue()
	}
   	for(var i = 0 ; i < dataSourcePenambahan_MutasiInventaris_Inv.getCount();i++)
	{
		params['tglMasuk-'+i]=dataSourcePenambahan_MutasiInventaris_Inv.data.items[i].data.tgl_masuk;
		params['kdKelompok-'+i]=dataSourcePenambahan_MutasiInventaris_Inv.data.items[i].data.kd_inv;
		params['noregister-'+i]=dataSourcePenambahan_MutasiInventaris_Inv.data.items[i].data.no_register;
		params['urutan-'+i]=dataSourcePenambahan_MutasiInventaris_Inv.data.items[i].data.no_urut_brg;
		params['barang-'+i]=dataSourcePenambahan_MutasiInventaris_Inv.data.items[i].data.nama_brg;
		params['kelompok-'+i]=dataSourcePenambahan_MutasiInventaris_Inv.data.items[i].data.nama_sub;
		params['satuan-'+i]=dataSourcePenambahan_MutasiInventaris_Inv.data.items[i].data.satuan;
		params['noruang-'+i]=dataSourcePenambahan_MutasiInventaris_Inv.data.items[i].data.no_ruang;
		params['jumlah-'+i]=dataSourcePenambahan_MutasiInventaris_Inv.data.items[i].data.jml_masuk;
		params['keterangan-'+i]=dataSourcePenambahan_MutasiInventaris_Inv.data.items[i].data.ket;
		
	}
    return params
};

function ValidasiEntryPenambahanMutasi(modul,mBolHapus){
	var x = 1;
	if(InvMutasiInventaris.vars.mutasiPenambahan.getValue() === ''){
		ShowPesanWarningInvMutasiInventaris('Nomor masih kosong', 'Warning');
		x = 0;
	} 
	if (dataSourcePenambahan_MutasiInventaris_Inv.getCount()===0)
		{
			ShowPesanWarningInvMutasiInventaris('Data Belum Diisi Lengkap', 'Warning');
			x = 0;
		}
	for(var i = 0 ; i < dataSourcePenambahan_MutasiInventaris_Inv.getCount();i++)
	{
		var o=dataSourcePenambahan_MutasiInventaris_Inv.getRange()[i].data;
		//console.log(o.jml_pindah);
		if (o.jml_pindah !== undefined)
		/*{
			for(var j = i+1 ; j < dataSourcePenambahan_MutasiInventaris_Inv.getCount();j++)
			{
				var p=dataSourcePenambahan_MutasiInventaris_Inv.getRange()[j].data;
				if (p.no_urut_brg==o.no_urut_brg)
				{
					ShowPesanWarningInvMutasiInventaris('Barang Tidak Boleh Sama', 'Warning');
					x = 0;
				}
			}
		}
		else*/
		{
			ShowPesanWarningInvMutasiInventaris('Data Belum Diisi Lengkap', 'Warning');
			x = 0;
		}
		else
		{
			x = 1;
		}
		
	}
	return x;
};

function datasavePemindahan_MutasiInventaris_Inv(){
	if (ValidasiEntryPemindahanMutasi(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/inventaris/functionMutasiInv/saveMutasi",
				params: getParamPemindahanMutasi(),
				failure: function(o)
				{
					ShowPesanErrorInvMutasiInventaris('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoInvMutasiInventaris(nmPesanSimpanSukses,nmHeaderSimpanData);
						dataSourcePemindahan_MutasiInventaris_Inv.reload();
					}
					else 
					{
						ShowPesanErrorInvMutasiInventaris('Gagal Menyimpan Data ini', 'Error');
						dataSourcePemindahan_MutasiInventaris_Inv.reload();
					};
				}
			}
			
		)
	}
}

function getParamPemindahanMutasi(){
	var	params =
	{
		panel:'pemindahan',
		jumlahrec:dataSourcePemindahan_MutasiInventaris_Inv.getCount(),
		nomutasi:InvMutasiInventaris.vars.mutasiPemindahan.getValue(),
		tglmutasi:Ext.getCmp('tglMutasiPemindahanInventaris_Inv').getValue(),
		keterangan:Ext.getCmp('TxtKeteranganMutasiPemindahanFilterGridDataView_MutasiInventaris_Inv').getValue()
	}
   	for(var i = 0 ; i < dataSourcePemindahan_MutasiInventaris_Inv.getCount();i++)
	{
		params['kdKelompok-'+i]=dataSourcePemindahan_MutasiInventaris_Inv.data.items[i].data.kd_inv;
		params['noregister-'+i]=dataSourcePemindahan_MutasiInventaris_Inv.data.items[i].data.no_register;
		params['noregruang-'+i]=dataSourcePemindahan_MutasiInventaris_Inv.data.items[i].data.no_reg_ruang;
		params['noruanglama-'+i]=dataSourcePemindahan_MutasiInventaris_Inv.data.items[i].data.no_ruang_lama;
		params['ruanglama-'+i]=dataSourcePemindahan_MutasiInventaris_Inv.data.items[i].data.ruang_lama;
		params['noruangbaru-'+i]=dataSourcePemindahan_MutasiInventaris_Inv.data.items[i].data.no_ruang_baru;
		params['ruangbaru-'+i]=dataSourcePemindahan_MutasiInventaris_Inv.data.items[i].data.ruang_baru;
		params['urutan-'+i]=dataSourcePemindahan_MutasiInventaris_Inv.data.items[i].data.no_urut_brg;
		params['barang-'+i]=dataSourcePemindahan_MutasiInventaris_Inv.data.items[i].data.nama_brg;
		params['kelompok-'+i]=dataSourcePemindahan_MutasiInventaris_Inv.data.items[i].data.nama_sub;
		params['satuan-'+i]=dataSourcePemindahan_MutasiInventaris_Inv.data.items[i].data.satuan;
		params['jumlah-'+i]=dataSourcePemindahan_MutasiInventaris_Inv.data.items[i].data.jml_masuk;
		params['jumlahpindah-'+i]=dataSourcePemindahan_MutasiInventaris_Inv.data.items[i].data.jml_pindah;
		params['keterangan-'+i]=dataSourcePemindahan_MutasiInventaris_Inv.data.items[i].data.ket;
		
	}
    return params
};

function ValidasiEntryPemindahanMutasi(modul,mBolHapus){
	var x = 1;
	if(InvMutasiInventaris.vars.mutasiPemindahan.getValue() === ''){
		ShowPesanWarningInvMutasiInventaris('Nomor masih kosong', 'Warning');
		x = 0;
	} 
	for(var i = 0 ; i < dataSourcePemindahan_MutasiInventaris_Inv.getCount();i++)
	{
		var o=dataSourcePemindahan_MutasiInventaris_Inv.getRange()[i].data;
		if (o.jml_pindah === undefined)
		/*{
			for(var j = i+1 ; j < dataSourcePenambahan_MutasiInventaris_Inv.getCount();j++)
			{
				var p=dataSourcePenambahan_MutasiInventaris_Inv.getRange()[j].data;
				if (p.no_urut_brg==o.no_urut_brg)
				{
					ShowPesanWarningInvMutasiInventaris('Barang Tidak Boleh Sama', 'Warning');
					x = 0;
				}
			}
		}
		else*/
		{
			ShowPesanWarningInvMutasiInventaris('Data Belum Diisi Lengkap', 'Warning');
			x = 0;
		}
		
	}
	return x;
};

function datasavePengurangan_MutasiInventaris_Inv(){
	if (ValidasiEntryPenguranganMutasi(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/inventaris/functionMutasiInv/saveMutasi",
				params: getParamPenguranganMutasi(),
				failure: function(o)
				{
					ShowPesanErrorInvMutasiInventaris('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoInvMutasiInventaris(nmPesanSimpanSukses,nmHeaderSimpanData);
						dataSourcePengurangan_MutasiInventaris_Inv.reload();
					}
					else 
					{
						ShowPesanErrorInvMutasiInventaris('Gagal Menyimpan Data ini', 'Error');
						dataSourcePengurangan_MutasiInventaris_Inv.reload();
					};
				}
			}
			
		)
	}
}

function getParamPenguranganMutasi(){
	var	params =
	{
		panel:'pengurangan',
		jumlahrec:dataSourcePengurangan_MutasiInventaris_Inv.getCount(),
		nomutasi:InvMutasiInventaris.vars.mutasiPengurangan.getValue(),
		tglmutasi:Ext.getCmp('tglMutasiPenguranganInventaris_Inv').getValue(),
		keterangan:Ext.getCmp('TxtKeteranganMutasiPenguranganFilterGridDataView_MutasiInventaris_Inv').getValue()
	}
   	for(var i = 0 ; i < dataSourcePengurangan_MutasiInventaris_Inv.getCount();i++)
	{
		params['tglMasuk-'+i]=dataSourcePengurangan_MutasiInventaris_Inv.data.items[i].data.tgl_masuk;
		params['kdKelompok-'+i]=dataSourcePengurangan_MutasiInventaris_Inv.data.items[i].data.kd_inv;
		params['noregister-'+i]=dataSourcePengurangan_MutasiInventaris_Inv.data.items[i].data.no_register;
		params['noregruang-'+i]=dataSourcePengurangan_MutasiInventaris_Inv.data.items[i].data.no_reg_ruang;
		params['noruangbaru-'+i]=dataSourcePengurangan_MutasiInventaris_Inv.data.items[i].data.no_ruang_baru;
		params['ruangbaru-'+i]=dataSourcePengurangan_MutasiInventaris_Inv.data.items[i].data.lokasi;
		params['urutan-'+i]=dataSourcePengurangan_MutasiInventaris_Inv.data.items[i].data.no_urut_brg;
		params['barang-'+i]=dataSourcePengurangan_MutasiInventaris_Inv.data.items[i].data.nama_brg;
		params['kelompok-'+i]=dataSourcePengurangan_MutasiInventaris_Inv.data.items[i].data.nama_sub;
		params['satuan-'+i]=dataSourcePengurangan_MutasiInventaris_Inv.data.items[i].data.satuan;
		params['jumlah-'+i]=dataSourcePengurangan_MutasiInventaris_Inv.data.items[i].data.jml_aktual_lama;
		params['jumlahkurang-'+i]=dataSourcePengurangan_MutasiInventaris_Inv.data.items[i].data.jml_kurang;
		params['hrgkurang-'+i]=dataSourcePengurangan_MutasiInventaris_Inv.data.items[i].data.hrg_kurang;
		params['keterangan-'+i]=dataSourcePengurangan_MutasiInventaris_Inv.data.items[i].data.ket;
		
	}
	console.log(dataSourcePengurangan_MutasiInventaris_Inv.data);
    return params
};

function ValidasiEntryPenguranganMutasi(modul,mBolHapus){
	var x = 1;
	if(InvMutasiInventaris.vars.mutasiPengurangan.getValue() === ''){
		ShowPesanWarningInvMutasiInventaris('Nama  obat masih kosong', 'Warning');
		x = 0;
	} 
	for(var i = 0 ; i < dataSourcePengurangan_MutasiInventaris_Inv.getCount();i++)
	{
		var o=dataSourcePengurangan_MutasiInventaris_Inv.getRange()[i].data;
		if (o.jml_kurang === undefined)
		/*{
			for(var j = i+1 ; j < dataSourcePenambahan_MutasiInventaris_Inv.getCount();j++)
			{
				var p=dataSourcePenambahan_MutasiInventaris_Inv.getRange()[j].data;
				if (p.no_urut_brg==o.no_urut_brg)
				{
					ShowPesanWarningInvMutasiInventaris('Barang Tidak Boleh Sama', 'Warning');
					x = 0;
				}
			}
		}
		else*/
		{
			ShowPesanWarningInvMutasiInventaris('Data Belum Diisi Lengkap', 'Warning');
			x = 0;
		}
		
	}
	return x;
};
//------------------------------Sub  Obat----------------------------------------------
function DataInitPanelSubObat(rowdata){
	Ext.getCmp('txtKdSubObat_InvMutasiInventarisL').setValue(rowdata.kd_sub_jns);
	InvMutasiInventaris.vars.sub.setValue(rowdata.sub_);
};

function dataaddnewSub_MutasiInventaris_Inv(){
	Ext.getCmp('txtKdSubObat_InvMutasiInventarisL').setValue('');
	InvMutasiInventaris.vars.sub.setValue('');
}

function datasaveSub_MutasiInventaris_Inv(){
	if (ValidasiEntrySubObat(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionInvMutasiInventaris/saveSub",
				params: getParamSetupSubObat(),
				failure: function(o)
				{
					ShowPesanErrorInvMutasiInventaris('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoInvMutasiInventaris(nmPesanSimpanSukses,nmHeaderSimpanData);
						Ext.get('txtKdSubObat_InvMutasiInventarisL').dom.value=cst.kdsub;
						dsDataMutasi_Inventaris_Inv.reload();
					}
					else 
					{
						ShowPesanErrorInvMutasiInventaris('Gagal Menyimpan Data ini', 'Error');
						dsDataMutasi_Inventaris_Inv.reload();
					};
				}
			}
			
		)
	}
}

function getParamSetupSubObat() {
	var	params =
	{
		KdSubJns:Ext.getCmp('txtKdSubObat_InvMutasiInventarisL').getValue(),
		Sub:InvMutasiInventaris.vars.sub.getValue()
	}
   
    return params
};

function ValidasiEntrySubObat(modul,mBolHapus){
	var x = 1;
	if(InvMutasiInventaris.vars.sub.getValue() === ''){
		ShowPesanWarningInvMutasiInventaris('Sub  obat masih kosong', 'Warning');
		x = 0;
	} 
	return x;
};


//------------------------------GOLONGAN Obat----------------------------------------------
function DataInitPanelGolongan(rowdata){
	Ext.getCmp('txtKdGolObat_InvMutasiInventarisL').setValue(rowdata.apt_kd_golongan);
	InvMutasiInventaris.vars.golongan.setValue(rowdata.apt_golongan);
};

function dataaddnewGolongan_MutasiInventaris_Inv(){
	Ext.getCmp('txtKdGolObat_InvMutasiInventarisL').setValue('');
	InvMutasiInventaris.vars.golongan.setValue('');
}

function datasaveGolongan_MutasiInventaris_Inv(){
	if (ValidasiEntryGolongan(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionInvMutasiInventaris/saveGolongan",
				params: getParamSetupGolongan(),
				failure: function(o)
				{
					ShowPesanErrorInvMutasiInventaris('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoInvMutasiInventaris(nmPesanSimpanSukses,nmHeaderSimpanData);
						Ext.get('txtKdGolObat_InvMutasiInventarisL').dom.value=cst.kdgolongan;
						dsDataGrdGolongan_viSetupMasterObat.reload();
					}
					else 
					{
						ShowPesanErrorInvMutasiInventaris('Gagal Menyimpan Data ini', 'Error');
						dsDataGrdGolongan_viSetupMasterObat.reload();
					};
				}
			}
			
		)
	}
}

function getParamSetupGolongan() {
	var	params =
	{
		KdGolongan:Ext.getCmp('txtKdGolObat_InvMutasiInventarisL').getValue(),
		Golongan:InvMutasiInventaris.vars.golongan.getValue()
	}
   
    return params
};

function ValidasiEntryGolongan(modul,mBolHapus){
	var x = 1;
	if(InvMutasiInventaris.vars.golongan.getValue() === ''){
		ShowPesanWarningInvMutasiInventaris('Golongan obat masih kosong', 'Warning');
		x = 0;
	} 
	return x;
};



//------------------------------SATUAN ----------------------------------------------
function DataInitPanelSatuan(rowdata){
	Ext.getCmp('txtKdSatuan_InvMutasiInventarisL').setValue(rowdata.kd_satuan);
	InvMutasiInventaris.vars.satuan.setValue(rowdata.satuan);
};

function dataaddnewSatuan_MutasiInventaris_Inv(){
	Ext.getCmp('txtKdSatuan_InvMutasiInventarisL').setValue('');
	InvMutasiInventaris.vars.satuan.setValue('');
}

function datasaveSatuan_MutasiInventaris_Inv(){
	if (ValidasiEntrySatuan(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionInvMutasiInventaris/saveSatuan",
				params: getParamSetupSatuan(),
				failure: function(o)
				{
					ShowPesanErrorInvMutasiInventaris('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoInvMutasiInventaris(nmPesanSimpanSukses,nmHeaderSimpanData);
						dsDataGrdSatuan_viSetupMasterObat.reload();
					}
					else 
					{
						ShowPesanErrorInvMutasiInventaris('Gagal Menyimpan Data ini', 'Error');
						dsDataGrdSatuan_viSetupMasterObat.reload();
					};
				}
			}
			
		)
	}
}

function getParamSetupSatuan(){
	var	params =
	{
		KdSatuan:Ext.getCmp('txtKdSatuan_InvMutasiInventarisL').getValue(),
		Satuan:InvMutasiInventaris.vars.satuan.getValue()
	}
   
    return params
};

function ValidasiEntrySatuan(modul,mBolHapus){
	var x = 1;
	if(Ext.getCmp('txtKdSatuan_InvMutasiInventarisL').getValue() === ''){
		ShowPesanWarningInvMutasiInventaris('Kode Satuan masih kosong', 'Warning');
		x = 0;
	}
	if(InvMutasiInventaris.vars.satuan.getValue() === ''){
		ShowPesanWarningInvMutasiInventaris('Satuan masih kosong', 'Warning');
		x = 0;
	} 
	if(Ext.getCmp('txtKdSatuan_InvMutasiInventarisL').getValue().length > 10){
		ShowPesanWarningInvMutasiInventaris('Kode satuan kecil tidak boleh lebih dari 10 huruf', 'Warning');
		x = 0;
	}
	return x;
};


//------------------------------SATUAN BESAR----------------------------------------------
function DataInitPanelSatuanBesar(rowdata){
	Ext.getCmp('txtKdSatuanBesar_InvMutasiInventarisL').setValue(rowdata.kd_sat_besar);
	InvMutasiInventaris.vars.satuanBesar.setValue(rowdata.keterangan);
};

function dataaddnewSatuanBesar_MutasiInventaris_Inv(){
	Ext.getCmp('txtKdSatuanBesar_InvMutasiInventarisL').setValue('');
	InvMutasiInventaris.vars.satuanBesar.setValue('');
}

function datasaveSatuanBesar_MutasiInventaris_Inv(){
	if (ValidasiEntrySatuanBesar(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionInvMutasiInventaris/saveSatuanBesar",
				params: getParamSetupSatuanBesar(),
				failure: function(o)
				{
					ShowPesanErrorInvMutasiInventaris('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoInvMutasiInventaris(nmPesanSimpanSukses,nmHeaderSimpanData);
						//Ext.get('txtKdSatuanBesar_InvMutasiInventarisL').dom.value=cst.kdsatbesar;
						dsDataGrdSatuanBesar_viSetupMasterObat.reload();
					}
					else 
					{
						ShowPesanErrorInvMutasiInventaris('Gagal Menyimpan Data ini', 'Error');
						dsDataGrdSatuanBesar_viSetupMasterObat.reload();
					};
				}
			}
			
		)
	}
}

function getParamSetupSatuanBesar(){
	var	params =
	{
		KdSatBesar:Ext.getCmp('txtKdSatuanBesar_InvMutasiInventarisL').getValue(),
		Keterangan:InvMutasiInventaris.vars.satuanBesar.getValue()
	}
   
    return params
};

function ValidasiEntrySatuanBesar(modul,mBolHapus){
	var x = 1;
	if(Ext.getCmp('txtKdSatuanBesar_InvMutasiInventarisL').getValue() === ''){
		ShowPesanWarningInvMutasiInventaris('Kode satuan besar satuan masih kosong', 'Warning');
		x = 0;
	}
	if(InvMutasiInventaris.vars.satuanBesar.getValue() === ''){
		ShowPesanWarningInvMutasiInventaris('Keterangan satuan masih kosong', 'Warning');
		x = 0;
	}	
	if(Ext.getCmp('txtKdSatuanBesar_InvMutasiInventarisL').getValue().length > 10){
		ShowPesanWarningInvMutasiInventaris('Kode satuan besar tidak boleh lebih dari 10 huruf', 'Warning');
		x = 0;
	}
	return x;
};



function ShowPesanWarningInvMutasiInventaris(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorInvMutasiInventaris(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoInvMutasiInventaris(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};// JavaScript Document