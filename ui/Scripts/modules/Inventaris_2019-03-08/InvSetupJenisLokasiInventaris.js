var dataSourceJenisLokasi;
var rowselectedJenisLokasi;
var LookupJenisLokasi;
var disDataJenisLokasi=0;
var InvSetupJenisLokasi={};
var gridListHasilJenisLokasi;

InvSetupJenisLokasi.form={};
InvSetupJenisLokasi.func={};
InvSetupJenisLokasi.vars={};
InvSetupJenisLokasi.func.parent=InvSetupJenisLokasi;
InvSetupJenisLokasi.form.ArrayStore={};
InvSetupJenisLokasi.form.ComboBox={};
InvSetupJenisLokasi.form.DataStore={};
InvSetupJenisLokasi.form.Record={};
InvSetupJenisLokasi.form.Form={};
InvSetupJenisLokasi.form.Grid={};
InvSetupJenisLokasi.form.Panel={};
InvSetupJenisLokasi.form.TextField={};
InvSetupJenisLokasi.form.Button={};

InvSetupJenisLokasi.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_jns_lokasi', 'jns_lokasi'],
	data: []
});
CurrentPage.page = getPanelJenisLokasi(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelJenisLokasi(mod_id) {
    var Field = ['kd_jns_lokasi','jns_lokasi'];
    dataSourceJenisLokasi = new WebApp.DataStore({
        fields: Field
    });
	datasourcefunction_JenisLokasi();
    gridListHasilJenisLokasi = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dataSourceJenisLokasi,
        anchor: '100% 80%',
        columnLines: false,
        autoScroll: false,
        border: true,
        sort: false,
        autoHeight: false,
        layout: 'fit',
        region: 'center',
        //height: 200,
        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
            listeners: {
                rowselect: function (sm, row, rec) {
					rowselectedJenisLokasi=dataSourceJenisLokasi.getAt(row);
                }
            }
        }),
        listeners: {
           rowdblclick: function (sm, ridx, cidx) {
                rowselectedJenisLokasi=dataSourceJenisLokasi.getAt(ridx);
				disDataJenisLokasi=1;
				disabled_dataJenisLokasi(disDataJenisLokasi);
				datainit_formJenisLokasi(rowselectedJenisLokasi.data);
            }
        },
        cm: new Ext.grid.ColumnModel(
                [
				new Ext.grid.RowNumberer({
					header:'No.'
					}),
                    {
                        header: 'Kode Jenis Lokasi',
                        dataIndex: 'kd_jns_lokasi',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 7
                    },
                    {
                        header: 'Nama Jenis Lokasi',
                        dataIndex: 'jns_lokasi',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
						width: 50
                    },
                ]
                ),
        viewConfig: {
            forceFit: true
        },
        tbar:[
			{
                id: 'btnEditDataJenisLokasi',
                text: 'Edit Data',
                tooltip: 'Edit Data',
                iconCls: 'Edit_Tr',
                handler: function (sm, row, rec) {
                    edit_dataJenisLokasi(rowselectedJenisLokasi.data);
                },
				
            },
		]
    });
	   var FormDepanJenisLokasi = new Ext.Panel({
        id: mod_id,
        closable: true,
        region: 'center',
        layout: 'form',
        title: 'Setup Lokasi (Gedung/Unit)',
        border: false,
        shadhow: true,
        autoScroll: false,
		width: 250,
        iconCls: 'Request',
        margins: '0 5 5 0',
		tbar: [
			
			{
                id: 'btnAddNewDataJenisLokasi',
                text: 'Add New',
                tooltip: 'addJenisLokasi',
                iconCls: 'add',
                handler: function (sm, row, rec) {
                    addnew_dataJenisLokasi();
				}
            },
			{
        	xtype: 'tbseparator'
       		},
			{
                id: 'btnSaveDataJenisLokasi',
                text: 'Save Data',
                tooltip: 'Save Data',
                iconCls: 'Save',
                handler: function (sm, row, rec) {
                    save_dataJenisLokasi();
                },
				
            },
			{
        	xtype: 'tbseparator'
       		},
			{
                id: 'btnDeleteDataJenisLokasi',
                text: 'Delete Data',
                tooltip: 'Delete Data',
                iconCls: 'Remove',
                handler: function (sm, row, rec) {
					if (rowselectedJenisLokasi!=undefined)
                    	delete_dataJenisLokasi();
					else
						ShowPesanErrorJenisLokasi('Maaf ! Tidak ada data yang terpilih', 'Error');
				}
            },
			{
        	xtype: 'tbseparator'
       		},
			{
                id: 'btnRefreshDataJenisLokasi',
                text: 'Refresh',
                tooltip: 'Refresh',
                iconCls: 'Refresh',
                handler: function (sm, row, rec) {
                    datasourcefunction_JenisLokasi();
                },
				
            }
			],
        items: [
			getPanelPencarianJenisLokasi(),
			gridListHasilJenisLokasi,
        ],
        listeners: {
            'afterrender': function () {

            }
        }
    });

    return FormDepanJenisLokasi;

}
;
function getPanelPencarianJenisLokasi() {
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 1190,
                height: 75,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'Kode Jenis Lokasi '
                    },
                    {
                        x: 130,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 140,
                        y: 10,
                        xtype: 'textfield',
                        name: 'TxtKdJenisLokasi_InvSetupJenisLokasi',
                        id: 'TxtKdJenisLokasi_InvSetupJenisLokasi',
                        width: 80
						
                    },
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Nama Jenis Lokasi'
                    },
                    {
                        x: 130,
                        y: 40,
                        xtype: 'label',
                        text: '  '
                    },
					InvSetupJenisLokasi.vars.JenisLokasi=new Nci.form.Combobox.autoComplete({
								x: 140,
						y: 40,
						tabIndex:2,
						store	: InvSetupJenisLokasi.form.ArrayStore.a,
						select	: function(a,b,c){
							console.log(b);
							Ext.getCmp('TxtKdJenisLokasi_InvSetupJenisLokasi').setValue(b.data.kd_jns_lokasi);
							
							gridListHasilJenisLokasi.getView().refresh();
						},
						onShowList:function(a){
							dataSourceJenisLokasi.removeAll();
							
							var recs=[],
							recType=dataSourceJenisLokasi.recordType;
								
							for(var i=0; i<a.length; i++){
								recs.push(new recType(a[i]));
							}
							dataSourceJenisLokasi.add(recs);
							
						},
						insert	: function(o){
							return {
								kd_jns_lokasi   : o.kd_jns_lokasi,
								jns_lokasi 		: o.jns_lokasi,
								text			:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_jns_lokasi+'</td><td width="200">'+o.jns_lokasi+'</td></tr></table>'
							}
						},
						url		: baseURL + "index.php/inventaris/functionSetupJenisLokasi/getDataGridJenisLokasi",
						valueField: 'jns_lokasi',
						displayField: 'jns_lokasi',
						listWidth: 280
					}),
                ]
            }
        ]
    };
    return items;
};


function datasourcefunction_JenisLokasi(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/inventaris/functionSetupJenisLokasi/getDataGridJenisLokasi",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorKategoriBangunan('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dataSourceJenisLokasi.removeAll();
					var recs=[],
						recType=dataSourceJenisLokasi.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dataSourceJenisLokasi.add(recs);
					
					gridListHasilJenisLokasi.getView().refresh();
				}
				else 
				{
					ShowPesanErrorKategoriBangunan('Gagal membaca data', 'Error');
				};
			}
		}
		
	)
	
}

function save_dataJenisLokasi()
{
	if (ValidasiEntriJenisLokasi(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/inventaris/functionSetupJenisLokasi/save",
				params: ParameterSaveJenisLokasi(),
				failure: function(o)
				{
					ShowPesanErrorJenisLokasi('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanSuksesJenisLokasi(nmPesanSimpanSukses,nmHeaderSimpanData);
						datasourcefunction_JenisLokasi();
						disDataJenisLokasi=1;
						disabled_dataJenisLokasi(disDataJenisLokasi);
					}
					else 
					{
						ShowPesanErrorJenisLokasi('Gagal Menyimpan Data ini', 'Error');
						datasourcefunction_JenisLokasi();
					};
				}
			}
			
		)
	}
}
function edit_dataJenisLokasi(rowdata){
	disDataJenisLokasi=1;
	disabled_dataJenisLokasi(disDataJenisLokasi);
	ShowLookupJenisLokasi(rowdata);
}
function delete_dataJenisLokasi() 
{
		Ext.Msg.show
		(
			{
			   title:nmHeaderHapusData,
			   msg: "Apakah anda yakin akan menghapus data ini?" ,
			   buttons: Ext.MessageBox.YESNO,
			   width:250,
			   fn: function (btn) 
			   {			
					if (btn === 'yes') 
					{
						Ext.Ajax.request
						(
							{
								url: baseURL + "index.php/inventaris/functionSetupJenisLokasi/delete",
								params: paramsDeleteJenisLokasi(),
								success: function(o) 
								{
									var cst = Ext.decode(o.responseText);
									if (cst.success === true) 
									{
										ShowPesanSuksesJenisLokasi(nmPesanHapusSukses,nmHeaderHapusData);
										Ext.getCmp('TxtKdJenisLokasi_InvSetupJenisLokasi').setValue(cst.kodeJenisLokasi);
										datasourcefunction_JenisLokasi();
										ClearTextJenisLokasi();
										disDataJenisLokasi=0;
										disabled_dataJenisLokasi(disDataJenisLokasi);
										
									}
									else if (cst.success === false && cst.pesan===0)
									{
										ShowPesanErrorJenisLokasi(nmPesanHapusGagal,nmHeaderHapusData);
									}
									else 
									{
										ShowPesanErrorJenisLokasi(nmPesanHapusError,nmHeaderHapusData);
									};
								}
							}
						)
						disDataJenisLokasi=0;
						disabled_dataJenisLokasi(disDataJenisLokasi);
					};
				}
			}
		)
	};
	
function addnew_dataJenisLokasi()
{
	ClearTextJenisLokasi();
	disDataJenisLokasi=0;
	disabled_dataJenisLokasi(disDataJenisLokasi);
	TombolUpdate :InvSetupJenisLokasi.vars.JenisLokasi.focus();
}

function ShowLookupJenisLokasi(rowdata) {
	if (rowdata == undefined){
        ShowPesanErrorJenisLokasi('Data Kosong', 'Error');
    } else {
      datainit_formJenisLokasi(rowdata); 
    }
}

function datainit_formJenisLokasi(rowdata){
	Ext.getCmp('TxtKdJenisLokasi_InvSetupJenisLokasi').setValue(rowdata.kd_jns_lokasi);
	InvSetupJenisLokasi.vars.JenisLokasi.setValue(rowdata.jns_lokasi);
	console.log(rowdata);
}

function ParameterSaveJenisLokasi() {
	var params = {
		kodeData : Ext.getCmp('TxtKdJenisLokasi_InvSetupJenisLokasi').getValue(),
		namaData : InvSetupJenisLokasi.vars.JenisLokasi.getValue()
	}	
	return params;	
}

function paramsDeleteJenisLokasi() {
    var params = {
		kodeData : Ext.getCmp('TxtKdJenisLokasi_InvSetupJenisLokasi').getValue(),
	};
    return params
};

function ClearTextJenisLokasi() {
	kodeData : Ext.getCmp('TxtKdJenisLokasi_InvSetupJenisLokasi').setValue("");
	namaData : InvSetupJenisLokasi.vars.JenisLokasi.setValue("");
}

function disabled_dataJenisLokasi(disDatax){
	if (disDatax==1) {
		var dis= {
			kodeData : Ext.getCmp('TxtKdJenisLokasi_InvSetupJenisLokasi').disable(),
		}
	} else if (disDatax==0) {
		var dis= {
			kodeData : Ext.getCmp('TxtKdJenisLokasi_InvSetupJenisLokasi').enable(),
		}
	}
	return dis;
}

function ValidasiEntriJenisLokasi(modul,mBolHapus) {
	var nama = InvSetupJenisLokasi.vars.JenisLokasi.getValue();
	
	var x = 1;
	if( nama === '') {
		ShowPesanErrorJenisLokasi('Jenis lokasi tidak boleh kosong', 'Warning');
		x = 0;	
	}
	return x;
};

function ShowPesanSuksesJenisLokasi(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};

function ShowPesanErrorJenisLokasi(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:290
		}
	);
};


