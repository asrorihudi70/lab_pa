var dataSource_viInvKondisiBarang;
var selectCount_viInvKondisiBarang=50;
var NamaForm_viInvKondisiBarang="Setup Kondisi Barang";
var mod_name_viInvKondisiBarang="Setup Kondisi Barang";
var now_viInvKondisiBarang= new Date();
var rowSelected_viInvKondisiBarang;
var setLookUps_viInvKondisiBarang;
var tanggal = now_viInvKondisiBarang.format("d/M/Y");
var jam = now_viInvKondisiBarang.format("H/i/s");
var tmpkriteria;
var DataGridJenisObat;
var GridDataView_viInvKondisiBarang;
var IdKlas_produkegorySetEquipmentKlasKondisiBarang_produkView='1000000000';
var dsLookProdukList;
var treeKlas_KondisiBarang_viInvKondisiBarang

var CurrentData_viInvKondisiBarang =
{
	data: Object,
	details: Array,
	row: 0
};

var InvKondisiBarang={};
InvKondisiBarang.form={};
InvKondisiBarang.func={};
InvKondisiBarang.vars={};
InvKondisiBarang.func.parent=InvKondisiBarang;
InvKondisiBarang.form.ArrayStore={};
InvKondisiBarang.form.ComboBox={};
InvKondisiBarang.form.DataStore={};
InvKondisiBarang.form.Record={};
InvKondisiBarang.form.Form={};
InvKondisiBarang.form.Grid={};
InvKondisiBarang.form.Panel={};
InvKondisiBarang.form.TextField={};
InvKondisiBarang.form.Button={};

InvKondisiBarang.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_unit_far', 'nm_unit_far', 'nomor_ro_unit', 'nomorawal','nomor_out_milik'],
	data: []
});

CurrentPage.page = dataGrid_viInvKondisiBarang(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dataGrid_viInvKondisiBarang(mod_id_viInvKondisiBarang){	 
	// Field kiriman dari Project Net.
    var FieldMaster_viInvKondisiBarang = 
	[
		'kd_kondisi', 'kondisi', 'inv_kd_kondisi' 
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viInvKondisiBarang = new WebApp.DataStore
	({
        fields: FieldMaster_viInvKondisiBarang
    });
   	GetStrTreeSetEquipment();
    // Grid Apotek Perencanaan # --------------
	GridDataView_viInvKondisiBarang = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viInvKondisiBarang,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:true,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viInvKondisiBarang = undefined;
							rowSelected_viInvKondisiBarang = dataSource_viInvKondisiBarang.getAt(row);
							CurrentData_viInvKondisiBarang
							CurrentData_viInvKondisiBarang.row = row;
							CurrentData_viInvKondisiBarang.data = rowSelected_viInvKondisiBarang.data;
							/* Ext.getCmp('txtKodeKondisiBBar_viInvKondisiBarang').setValue(sm.selections.items[0].data.inv_kd_kondisi);
							Ext.getCmp('txtKondisiBBar_viInvKondisiBarang').setValue(sm.selections.items[0].data.kondisi); */
							DataInitInvKondisiBarang(rowSelected_viInvKondisiBarang.data);
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viInvKondisiBarang = dataSource_viInvKondisiBarang.getAt(ridx);
					if (rowSelected_viInvKondisiBarang != undefined)
					{
						DataInitInvKondisiBarang(rowSelected_viInvKondisiBarang.data);
						
						//setLookUp_viInvKondisiBarang(rowSelected_viInvKondisiBarang.data);
					}
					else
					{
						//setLookUp_viInvKondisiBarang();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Setup Unit
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header: 'kd_kondisi',
						dataIndex: 'kd_kondisi',
						hideable:false,
						menuDisabled: true,
						width: 40
						
					},
					//-------------- ## --------------
					{
						header: 'Kode',
						dataIndex: 'inv_kd_kondisi',
						hideable:false,
						menuDisabled: true,
						width: 40,
						hidden:true
					},
					{
						header: 'Kondisi',
						dataIndex: 'kondisi',
						hideable:false,
						menuDisabled: true,
						width: 90
					},
					//-------------- ## --------------
					
					
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viInvKondisiBarang',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viInvKondisiBarang',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viInvKondisiBarang != undefined)
							{
								DataInitInvKondisiBarang(rowSelected_viInvKondisiBarang.data);
							}
							
						}
					}
					//-------------- ## --------------
				]
			},
			bbar: 
			{
				
				xtype: 'toolbar',
				id: 'toolbar2_viInvKondisiBarang',
				items: 
				[
					{
						xtype: 'textfield',
						id: 'txtKodeKondisiBBar_viInvKondisiBarang',
						disabled: true,
						width: 100
					},
					{
						xtype: 'tbseparator',
					},
					{
						xtype: 'textfield',
						id: 'txtKondisiBBar_viInvKondisiBarang',
						width: 613
					}
					//-------------- ## --------------
				],
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viInvKondisiBarang, selectCount_viInvKondisiBarang, dataSource_viInvKondisiBarang),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)	
	
	// Kriteria filter pada Grid # --------------
    var FrmData_viInvKondisiBarang = new Ext.Panel
    (
		{
			title: NamaForm_viInvKondisiBarang,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viInvKondisiBarang,
			//region: 'center',
			layout: {
				type:'hbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ 
					itemsTreeListKondisiBarangGridDataView_viInvKondisiBarang(),
					GridDataView_viInvKondisiBarang
				   ],
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAddJenis_viInvKondisiBarang',
						handler: function(){
							AddNewInvKondisiBarang();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viInvKondisiBarang',
						handler: function()
						{
							loadMask.show();
							dataSave_viInvKondisiBarang();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viInvKondisiBarang',
						handler: function()
						{
							Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
								if (button == 'yes'){
									loadMask.show();
									dataDelete_viInvKondisiBarang();
								}
							});
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_viSetupKondisiBarangInv',
						handler: function()
						{
							dataSource_viInvKondisiBarang.removeAll();
							dataGriInvKondisiBarang();
						}
					},
					{
						xtype: 'tbseparator'
					}
					
				]
			},
			//GetStrTreeSetEquipment();
			//-------------- # End tbar # --------------
       }
    )
	
    return FrmData_viInvKondisiBarang;
	//loadMask.show();
	
    //-------------- # End form filter # --------------
}
//------------------end---------------------------------------------------------



function GetStrTreeSetEquipment()
{
	 Ext.Ajax.request
	 (
		{
			url: WebAppUrl.UrlExecProcess,
			params: 
			{
				UserID: strUser, // 'Admin',
				ModuleID: 'ProsesGetKondisiBarang',
				Params:	" and kd_kondisi<>''"
			},
			success : function(resp) 
			{
				loadMask.hide();
				var cst = Ext.decode(resp.responseText);
				StrTreeSetKondisiBarang= cst.arr;
			},
			failure:function()
			{
			    loadMask.hide();
			}
		}
	);
};


function itemsTreeListKondisiBarangGridDataView_viInvKondisiBarang()
{	
		
	treeKlas_KondisiBarang_viInvKondisiBarang= new Ext.tree.TreePanel
	(
		{
			autoScroll: true,
			split: true,
			id:'paneltreekondisibarang',
			height:400,
			width:200,
			loader: new Ext.tree.TreeLoader(),
			listeners: 
			{
				click: function(n) 
				{
					strTreeCriteriKondisiBarang_viKondisiBarang=n.attributes
					rowSelectedTreeSetEquipmentKlas_produk=n.attributes;
					if (strTreeCriteriKondisiBarang_viKondisiBarang.id != ' 0')
					{
						if (strTreeCriteriKondisiBarang_viKondisiBarang.leaf == false)
						{
						
						}
						else
						{
							console.log(n.attributes);
							dataGriInvKondisiBarang(n.attributes.id);
							InvKondisiBarang.vars.inv_kd_kondisi=n.attributes.id;
							
							if(n.attributes.id=='1000000000'){
								GetStrTreeSetEquipment();
								rootTreeSetEquipmentKlas_produkView = new Ext.tree.AsyncTreeNode
								(
									{
										expanded: true,
										text:'MASTER',
										id:IdKlas_produkegorySetEquipmentKlasKondisiBarang_produkView,
										children: StrTreeSetKondisiBarang,
										autoScroll: true
									}
								) 
								treeKlas_KondisiBarang_viInvKondisiBarang.setRootNode(rootTreeSetEquipmentKlas_produkView);
							} 
							
						};
					}
					else
					{
						
					};
				},
				
				
			}
		}
	);
	
	rootTreeSetEquipmentKlas_produkView = new Ext.tree.AsyncTreeNode
	(
		{
			expanded: false,
			text:'MASTER',
			id:IdKlas_produkegorySetEquipmentKlasKondisiBarang_produkView,
			//children: StrTreeSetKondisiBarang,
			autoScroll: true
		}
	)  
  
	treeKlas_KondisiBarang_viInvKondisiBarang.setRootNode(rootTreeSetEquipmentKlas_produkView);  
	
    var pnlTreeFormDataWindowPopup_viInvKondisiBarang = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                
				//-------------- ## -------------- 
                treeKlas_KondisiBarang_viInvKondisiBarang,
                //-------------- ## -------------- 
            ],
            //-------------- #End items# --------------
        }
    )

    return pnlTreeFormDataWindowPopup_viInvKondisiBarang;
}

//===================================================================================================================================================

function dataGriInvKondisiBarang(inv_kd_kondisi){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/inventaris/functionSetupKondisiBarang/getKondisiBarangGrid",
			params: {id:inv_kd_kondisi},
			failure: function(o)
			{
				ShowPesanErrorInvKondisiBarang('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dataSource_viInvKondisiBarang.removeAll();
					var recs=[],
						recType=dataSource_viInvKondisiBarang.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dataSource_viInvKondisiBarang.add(recs);
					
					
					
					GridDataView_viInvKondisiBarang.getView().refresh();
				}
				else 
				{
					ShowPesanErrorInvKondisiBarang('Gagal membaca data', 'Error');
				};
			}
		}
		
	)
}

function dataSave_viInvKondisiBarang(){
	if (ValidasiSaveInvKondisiBarang(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/inventaris/functionSetupKondisiBarang/save",
				params: getParamSaveInvKondisiBarang(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorInvKondisiBarang('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoInvKondisiBarang('Berhasil menyimpan data ini','Information');
						Ext.getCmp('txtKodeKondisiBBar_viInvKondisiBarang').setValue(cst.kode)
						dataSource_viInvKondisiBarang.removeAll();
						dataGriInvKondisiBarang(InvKondisiBarang.vars.inv_kd_kondisi);
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorInvKondisiBarang('Gagal menyimpan data ini', 'Error');
						dataSource_viInvKondisiBarang.removeAll();
						dataGriInvKondisiBarang(InvKondisiBarang.vars.inv_kd_kondisi);
					};
				}
			}
			
		)
	}
}

function dataDelete_viInvKondisiBarang(){
	if (ValidasiSaveInvKondisiBarang(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/inventaris/functionSetupKondisiBarang/delete",
				params: getParamDeleteInvKondisiBarang(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorInvKondisiBarang('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoInvKondisiBarang('Berhasil menghapus data ini','Information');
						AddNewInvKondisiBarang()
						dataSource_viInvKondisiBarang.removeAll();
						dataGriInvKondisiBarang(InvKondisiBarang.vars.kd_kondisi);
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorInvKondisiBarang('Gagal menghapus data ini', 'Error');
						dataSource_viInvKondisiBarang.removeAll();
						dataGriInvKondisiBarang(InvKondisiBarang.vars.kd_kondisi);
					};
				}
			}
			
		)
	}
}

function AddNewInvKondisiBarang(){
	Ext.getCmp('txtKodeKondisiBBar_viInvKondisiBarang').setValue('');
	Ext.getCmp('txtKondisiBBar_viInvKondisiBarang').setValue('');
	InvKondisiBarang.vars.inv_kd_kondisi=InvKondisiBarang.vars.inv_kd_kondisi;
};

function DataInitInvKondisiBarang(rowdata){
	Ext.getCmp('txtKodeKondisiBBar_viInvKondisiBarang').setValue(rowdata.kd_kondisi);
	Ext.getCmp('txtKondisiBBar_viInvKondisiBarang').setValue(rowdata.kondisi);
	InvKondisiBarang.vars.inv_kd_kondisi=rowdata.inv_kd_kondisi;
};

function getParamSaveInvKondisiBarang(){
	var	params =
	{
		KdKondisi:Ext.getCmp('txtKodeKondisiBBar_viInvKondisiBarang').getValue(),
		Kondisi:Ext.getCmp('txtKondisiBBar_viInvKondisiBarang').getValue(),
		InvKdKondisi:InvKondisiBarang.vars.inv_kd_kondisi
	}
   
    return params
};

function getParamDeleteInvKondisiBarang(){
	var	params =
	{
		KdKondisi:Ext.getCmp('txtKodeKondisiBBar_viInvKondisiBarang').getValue()
	}
   
    return params
};

function ValidasiSaveInvKondisiBarang(modul,mBolHapus){
	var x = 1;
	if(Ext.getCmp('txtKondisiBBar_viInvKondisiBarang').getValue() === '' ){
		loadMask.hide();
		ShowPesanWarningInvKondisiBarang('Kondisi tidak boleh kosong', 'Warning');
		x = 0;
	} 
	
	return x;
};



function ShowPesanWarningInvKondisiBarang(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorInvKondisiBarang(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoInvKondisiBarang(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};