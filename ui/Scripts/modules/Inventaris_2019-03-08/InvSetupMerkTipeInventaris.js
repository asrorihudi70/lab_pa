var dataSource_viInvMerkBarang;
var selectCount_viInvMerkBarang=50;
var NamaForm_viInvMerkBarang="Setup Merk Barang";
var mod_name_viInvMerkBarang="Setup Merk Barang";
var now_viInvMerkBarang= new Date();
var rowSelected_viInvMerkBarang;
var setLookUps_viInvMerkBarang;
var tanggal = now_viInvMerkBarang.format("d/M/Y");
var jam = now_viInvMerkBarang.format("H/i/s");
var tmpkriteria;
var DataGridJenisObat;
var GridDataView_viInvMerkBarang;
var rootTreeMerk_InvMerkBarang='1000000000';
var dsLookProdukList;
var treeRoot_MerkBarang_viInvMerkBarang;

var CurrentData_viInvMerkBarang =
{
	data: Object,
	details: Array,
	row: 0
};

var InvMerkBarang={};
InvMerkBarang.form={};
InvMerkBarang.func={};
InvMerkBarang.vars={};
InvMerkBarang.func.parent=InvMerkBarang;
InvMerkBarang.form.ArrayStore={};
InvMerkBarang.form.ComboBox={};
InvMerkBarang.form.DataStore={};
InvMerkBarang.form.Record={};
InvMerkBarang.form.Form={};
InvMerkBarang.form.Grid={};
InvMerkBarang.form.Panel={};
InvMerkBarang.form.TextField={};
InvMerkBarang.form.Button={};

InvMerkBarang.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_unit_far', 'nm_unit_far', 'nomor_ro_unit', 'nomorawal','nomor_out_milik'],
	data: []
});

CurrentPage.page = dataGrid_viInvMerkBarang(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dataGrid_viInvMerkBarang(mod_id_viInvMerkBarang){	 
	// Field kiriman dari Project Net.
    var FieldMaster_viInvMerkBarang = 
	[
		'kd_merk', 'inv_kd_merk', 'merk_type'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viInvMerkBarang = new WebApp.DataStore
	({
        fields: FieldMaster_viInvMerkBarang
    });
   	GetStrTreeSetMerk();
    // Grid Apotek Perencanaan # --------------
	GridDataView_viInvMerkBarang = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viInvMerkBarang,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:true,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viInvMerkBarang = undefined;
							rowSelected_viInvMerkBarang = dataSource_viInvMerkBarang.getAt(row);
							CurrentData_viInvMerkBarang
							CurrentData_viInvMerkBarang.row = row;
							CurrentData_viInvMerkBarang.data = rowSelected_viInvMerkBarang.data;
							DataInitInvMerkBarang(rowSelected_viInvMerkBarang.data);
							/* Ext.getCmp('txtKodeMerkBBar_viInvMerkBarang').setValue(sm.selections.items[0].data.inv_kd_merk);
							Ext.getCmp('txtMerkTypeBBar_viInvMerkBarang').setValue(sm.selections.items[0].data.merk_type); */
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viInvMerkBarang = dataSource_viInvMerkBarang.getAt(ridx);
					if (rowSelected_viInvMerkBarang != undefined)
					{
						DataInitInvMerkBarang(rowSelected_viInvMerkBarang.data);
						//setLookUp_viInvMerkBarang(rowSelected_viInvMerkBarang.data);
					}
					else
					{
						//setLookUp_viInvMerkBarang();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Setup Unit
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header: 'kd_merk',
						dataIndex: 'kd_merk',
						hideable:false,
						menuDisabled: true,
						width: 40
						
					},
					//-------------- ## --------------
					{
						header: 'Kode',
						dataIndex: 'inv_kd_merk',
						hideable:false,
						menuDisabled: true,
						width: 40,
						hidden:true
					},
					{
						header: 'Jenis Barang',
						dataIndex: 'merk_type',
						hideable:false,
						menuDisabled: true,
						width: 90
					},
					//-------------- ## --------------
					
					
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viInvMerkBarang',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viInvMerkBarang',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viInvMerkBarang != undefined)
							{
								DataInitInvMerkBarang(rowSelected_viInvMerkBarang.data);
							}
							
						}
					}
					//-------------- ## --------------
				]
			},
			bbar: 
			{
				
				xtype: 'toolbar',
				id: 'toolbar2_viInvMerkBarang',
				items: 
				[
					{
						xtype: 'textfield',
						id: 'txtKodeMerkBBar_viInvMerkBarang',
						disabled: true,
						width: 100
					},
					{
						xtype: 'tbseparator',
					},
					{
						xtype: 'textfield',
						id: 'txtMerkTypeBBar_viInvMerkBarang',
						width: 613
					}
					//-------------- ## --------------
				],
				
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viInvMerkBarang, selectCount_viInvMerkBarang, dataSource_viInvMerkBarang),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)	
	
	// Kriteria filter pada Grid # --------------
    var FrmData_viInvMerkBarang = new Ext.Panel
    (
		{
			title: NamaForm_viInvMerkBarang,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viInvMerkBarang,
			//region: 'center',
			layout: {
				type:'hbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ itemsTreeListMerkBarangGridDataView_viInvMerkBarang(),
					GridDataView_viInvMerkBarang],
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAddJenis_viInvMerkBarang',
						handler: function(){
							AddNewInvMerkBarang();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viInvMerkBarang',
						handler: function()
						{
							loadMask.show();
							dataSave_viInvMerkBarang();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viInvMerkBarang',
						handler: function()
						{
							Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
								if (button == 'yes'){
									loadMask.show();
									dataDelete_viInvMerkBarang();
								}
							});
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_viSetupMerkTipeInv',
						handler: function()
						{
							dataSource_viInvMerkBarang.removeAll();
							dataGriInvMerkBarang(InvMerkBarang.vars.inv_kd_merk);
						}
					},
					{
						xtype: 'tbseparator'
					}
					
				]
			},
			//GetStrTreeSetMerk();
			//-------------- # End tbar # --------------
       }
    )
	
    return FrmData_viInvMerkBarang;
	//loadMask.show();
	
    //-------------- # End form filter # --------------
}
//------------------end---------------------------------------------------------



function GetStrTreeSetMerk()
{
	 Ext.Ajax.request
	 (
		{
			url: WebAppUrl.UrlExecProcess,
			params: 
			{
				UserID: strUser, // 'Admin',
				ModuleID: 'ProsesGetMerkBarang',
				Params:	" and kd_merk<>''"
			},
			success : function(resp) 
			{
				loadMask.hide();
				var cst = Ext.decode(resp.responseText);
				StrTreeSetKelompokBarang= cst.arr;
			},
			failure:function()
			{
			    loadMask.hide();
			}
		}
	);
};


function itemsTreeListMerkBarangGridDataView_viInvMerkBarang()
{	
		
	treeRoot_MerkBarang_viInvMerkBarang= new Ext.tree.TreePanel
	(
		{
			autoScroll: true,
			split: true,
			height:460,
			width:230,
			loader: new Ext.tree.TreeLoader(),
			listeners: 
			{
				click: function(n) 
				{
					strTreeCriteriKelompokBarang_viInvMerkBarang=n.attributes
					
					if (strTreeCriteriKelompokBarang_viInvMerkBarang.id != ' 0')
					{
						if (strTreeCriteriKelompokBarang_viInvMerkBarang.leaf == false)
						{
							
						}
						else
						{
							console.log(n.attributes.id);
							dataGriInvMerkBarang(n.attributes.id);
							InvMerkBarang.vars.inv_kd_merk=n.attributes.id;
							
							if(n.attributes.id=='1000000000'){
								GetStrTreeSetMerk();
								rootTreeSetEquipmentKlas_produkView = new Ext.tree.AsyncTreeNode
								(
									{
										expanded: true,
										text:'MASTER',
										id:rootTreeMerk_InvMerkBarang,
										children: StrTreeSetKelompokBarang,
										autoScroll: true
									}
								) 
								treeRoot_MerkBarang_viInvMerkBarang.setRootNode(rootTreeSetEquipmentKlas_produkView);
							} 
							
						};
					}
					else
					{
						
					};
				}
				
				
			}
		}
	);
	
	rootTreeSetEquipmentKlas_produkView = new Ext.tree.AsyncTreeNode
	(
		{
			expanded: false,
			text:'MASTER',
			id:rootTreeMerk_InvMerkBarang,
			children: StrTreeSetKelompokBarang,
			autoScroll: true
		}
	)  
  
	treeRoot_MerkBarang_viInvMerkBarang.setRootNode(rootTreeSetEquipmentKlas_produkView);  
	
    var pnlTreeFormDataWindowPopup_viInvMerkBarang = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                
				//-------------- ## -------------- 
                treeRoot_MerkBarang_viInvMerkBarang,
                //-------------- ## -------------- 
            ],
            //-------------- #End items# --------------
        }
    )

    return pnlTreeFormDataWindowPopup_viInvMerkBarang;
}

//===================================================================================================================================================

function dataGriInvMerkBarang(inv_kd_merk){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/inventaris/functionSetupMerkBarang/getMerkBarangGrid",
			params: {id:inv_kd_merk},
			failure: function(o)
			{
				ShowPesanErrorInvMerkBarang('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dataSource_viInvMerkBarang.removeAll();
					var recs=[],
						recType=dataSource_viInvMerkBarang.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dataSource_viInvMerkBarang.add(recs);
					
					
					
					GridDataView_viInvMerkBarang.getView().refresh();
				}
				else 
				{
					ShowPesanErrorInvMerkBarang('Gagal membaca data obat', 'Error');
				};
			}
		}
		
	)
}

function dataSave_viInvMerkBarang(){
	if (ValidasiSaveInvMerkBarang(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/inventaris/functionSetupMerkBarang/save",
				params: getParamSaveInvMerkBarang(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorInvMerkBarang('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoInvMerkBarang('Berhasil menyimpan data ini','Information');
						Ext.getCmp('txtKodeMerkBBar_viInvMerkBarang').setValue(cst.kode);
						dataSource_viInvMerkBarang.removeAll();
						dataGriInvMerkBarang(InvMerkBarang.vars.inv_kd_merk);
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorInvMerkBarang('Gagal menyimpan data ini', 'Error');
						dataSource_viInvMerkBarang.removeAll();
						dataGriInvMerkBarang(InvMerkBarang.vars.inv_kd_merk);
					};
				}
			}
			
		)
	}
}

function dataDelete_viInvMerkBarang(){
	if (ValidasiSaveInvMerkBarang(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/inventaris/functionSetupMerkBarang/delete",
				params: getParamDeleteInvMerkBarang(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorInvMerkBarang('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoInvMerkBarang('Berhasil menghapus data ini','Information');
						AddNewInvMerkBarang()
						dataSource_viInvMerkBarang.removeAll();
						dataGriInvMerkBarang(InvMerkBarang.vars.inv_kd_merk);
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorInvMerkBarang('Gagal menghapus data ini', 'Error');
						dataSource_viInvMerkBarang.removeAll();
						dataGriInvMerkBarang(InvMerkBarang.vars.inv_kd_merk);
					};
				}
			}
			
		)
	}
}

function AddNewInvMerkBarang(){
	Ext.getCmp('txtKodeMerkBBar_viInvMerkBarang').setValue('');
	Ext.getCmp('txtMerkTypeBBar_viInvMerkBarang').setValue('');
	InvMerkBarang.vars.inv_kd_merk=InvMerkBarang.vars.inv_kd_merk;
};

function DataInitInvMerkBarang(rowdata){
	Ext.getCmp('txtKodeMerkBBar_viInvMerkBarang').setValue(rowdata.kd_merk);
	Ext.getCmp('txtMerkTypeBBar_viInvMerkBarang').setValue(rowdata.merk_type);
	InvMerkBarang.vars.inv_kd_merk=rowdata.inv_kd_merk;
};

function getParamSaveInvMerkBarang(){
	var	params =
	{
		KdMerk:Ext.getCmp('txtKodeMerkBBar_viInvMerkBarang').getValue(),
		Merk:Ext.getCmp('txtMerkTypeBBar_viInvMerkBarang').getValue(),
		InvKdMerk:InvMerkBarang.vars.inv_kd_merk
	}
   
    return params
};

function getParamDeleteInvMerkBarang(){
	var	params =
	{
		KdMerk:Ext.getCmp('txtKodeMerkBBar_viInvMerkBarang').getValue()
	}
   
    return params
};

function ValidasiSaveInvMerkBarang(modul,mBolHapus){
	var x = 1;
	if(Ext.getCmp('txtMerkTypeBBar_viInvMerkBarang').getValue() === '' ){
		loadMask.hide();
		ShowPesanWarningInvMerkBarang('Merk tidak boleh kosong', 'Warning');
		x = 0;
	} 
	return x;
};


function ShowPesanWarningInvMerkBarang(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorInvMerkBarang(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoInvMerkBarang(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};