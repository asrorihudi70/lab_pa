var TrviJdwlOperasi={};
TrviJdwlOperasi.form={};
TrviJdwlOperasi.func={};
TrviJdwlOperasi.vars={};
TrviJdwlOperasi.func.parent=TrviJdwlOperasi;
TrviJdwlOperasi.form.ArrayStore={};
TrviJdwlOperasi.form.ComboBox={};
TrviJdwlOperasi.form.DataStore={};
TrviJdwlOperasi.form.Record={};
TrviJdwlOperasi.form.Form={};
TrviJdwlOperasi.form.Grid={};
var tglorder;
var pnlTRSetAsisten_jadwaloperasiOK;
TrviJdwlOperasi.form.Panel={};
TrviJdwlOperasi.form.TextField={};
TrviJdwlOperasi.form.Button={};
var cbo_viComboJenisTindakan_viJdwlOperasi;
var dsvSetPerawatJadwalOperasiOK;
var dsvComboSubSpesialisJadwalOperasiOK;
var dsvcomboSpesialis_OKJadwalOperasiOK;
var order_dari_rwj    = false;
var order_dari_igd    = false;
var order_dari_rwi    = false;
var kdUnitAsalPasien  = '';
var noKamarAsalPasien = '';
var tglMasukPasien    = '';
var urutMasukPasien   = '';
var dsvComboTindakanOperasiJadwalOperasiOK;
var dsvComboDokterJadwalOperasiOK;
var ds_Poli_viDaftar_kamarOk;
var ds_Poli_viDaftar_SubSpesialisasi;
var dsvSetAsistenJadwalOperasiOK;
var dsvSetInstrumenJadwalOperasiOK;
var dsvSetSirkulasiJadwalOperasiOK;
var dsvComboJenisOperasiJadwalOperasiOK;
var kodeKasirNya='RWJ/IGD';
var cellCurrentLab='';
var dspasienorder_mng;
var cbopasienorder_mng;
var  checkColumn_penata__lab;
var dsGetdetail_order_OK;
var gridGetdetail_order_OK;
var secondGridStore2;
var ds_Poli_viDaftar_SubKamarOperasi;
 var  var_tampung_lab_order=false;
var dsTrDokter	= new WebApp.DataStore({ fields: ['KD_DOKTER','NAMA','KD_JOB','KD_PRODUK'] });
var dataTrDokter = [];

var fields = [
	{name: 'kd_asisten', mapping : 'kd_asisten'},
	{name: 'nama', mapping : 'nama'},
	{name: 'jenis', mapping : 'jenis'}
];
secondGridStore2 = new Ext.data.JsonStore({
	fields : fields,
	root   : 'records'
});
TrviJdwlOperasi.form.ArrayStore.produk=new Ext.data.ArrayStore({
		id: 0,
		fields: [
					'uraian','kd_tarif','kd_produk','tgl_transaksi','tgl_berlaku','harga','qty'
				],
		data: []
	});

TrviJdwlOperasi.form.ArrayStore.kodepasien=new Ext.data.ArrayStore({
		id: 0,
		fields: [
					
				],
		data: []
	});

TrviJdwlOperasi.form.ArrayStore.pasien=new Ext.data.ArrayStore({
		id: 0,
		fields: [
					
				],
		data: []
	});

TrviJdwlOperasi.form.ArrayStore.notransaksi=new Ext.data.ArrayStore({
		id: 0,
		fields: [
					
				],
		data: []
	});

var CurrentviJdwlOperasi =
{
    data: Object,
    details: Array,
    row: 0
};
var CurrentBatalviJdwlOperasi =
{
    data: Object,
    details: Array,
    row: 0
};
var tanggaltransaksitampung;
var mRecordRwj = Ext.data.Record.create
(
    [
       {name: 'DESKRIPSI2', mapping:'DESKRIPSI2'},
       {name: 'KD_PRODUK', mapping:'KD_PRODUK'},
       {name: 'DESKRIPSI', mapping:'DESKRIPSI'},
       {name: 'KD_TARIF', mapping:'KD_TARIF'},
       {name: 'HARGA', mapping:'HARGA'},
       {name: 'QTY', mapping:'QTY'},
       {name: 'TGL_TRANSAKSI', mapping:'TGL_TRANSAKSI'},
       {name: 'DESC_REQ', mapping:'DESC_REQ'},
      // {name: 'KD_TARIF', mapping:'KD_TARIF'},
       {name: 'URUT', mapping:'URUT'}
    ]
);
var CurrentDiagnosa =
{
    data: Object,
    details: Array,
    row: 0
};

var FormLookUpGantidokter;
var mRecordDiagnosa = Ext.data.Record.create
(
    [
       {name: 'KASUS', mapping:'KASUS'},
       {name: 'KD_PENYAKIT', mapping:'KD_PENYAKIT'},
       {name: 'PENYAKIT', mapping:'PENYAKIT'},
       //{name: 'KD_TARIF', mapping:'KD_TARIF'},
      // {name: 'HARGA', mapping:'HARGA'},
       {name: 'STAT_DIAG', mapping:'STAT_DIAG'},
       {name: 'TGL_TRANSAKSI', mapping:'TGL_TRANSAKSI'},
      // {name: 'DESC_REQ', mapping:'DESC_REQ'},
      // {name: 'KD_TARIF', mapping:'KD_TARIF'},
       {name: 'URUT_MASUK', mapping:'URUT_MASUK'}
    ]
);

var now = new Date();
var rowSelectedDiagnosa;
var cellSelecteddeskripsi;
var nowTglTransaksi = new Date();
var nowSekarang = new Date();
var TanggalSekarang = nowSekarang.format("d/M/Y");
var nowTglTransaksiGrid = new Date();
var tglGridBawah = nowTglTransaksiGrid.format("d/M/Y");
var tigaharilalu = new Date().add(Date.DAY, -3);

var selectSetGDarahLab;
var selectSetJKLab;
var selectSetKelPasienLab;
var selectSetPerseoranganLab;
var selectSetPerusahaanLab;
var selectSetAsuransiLab;

var dsTRDetailviJdwlOperasiList;
var TmpNotransaksi='';
var KdKasirAsal='';
var TglTransaksiAsal='';
var databaru = 0;
var No_Kamar='';
var Kd_Spesial=0;
var intervalTime;
var gridDTJadwalOperasi;
var grListviJdwlOperasi;
var tampungshiftsekarang;

//var FocusCtrlCMRWJ;
var vkode_customer;
CurrentPage.page = getPanelviJdwlOperasi(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
//viewGridOrderAll_RASEPRWI();


clearInterval(intervalTime);
clearInterval();
//membuat form
function getPanelviJdwlOperasi(mod_id)
{

/* intervalTime = setInterval(function(){
total_pasien_order_mng();
load_data_pasienorder();
viewGridOrderAll_RASEPRWI();
}, 150000);  */

    var FormDepanviJdwlOperasi = new Ext.Panel
    (
        {
            id: mod_id,
            closable: true,
            region: 'center',
            layout: 'form',
            title: 'Jadwal Operasi',
            border: false,
            shadhow: true,
            autoScroll:true,
            iconCls: 'Request',
            margins: '0 5 5 0',
            items: [
						//getItemPanelInputviJdwlOperasi(),
						getFormItemEntry_viJdwlOperasi(),
						getFormEntryviJdwlOperasi()
                   ],
			tbar:
            [
				'-',
				{
                        text: 'Tambah Baru',
                        id: 'btnAddviJdwlOperasi',
                        iconCls: 'add',
                        handler: function()
						{
							bersihkanSemuaSetelahSimpanOK();
							Ext.getCmp('btnSimpanviJdwlOperasi').enable();
						}
				},
				'-',
				{
                        text: 'Simpan',
                        id: 'btnSimpanviJdwlOperasi',
                        tooltip: nmSimpan,
                        iconCls: 'save',
                        handler: function()
						{
							//loadMask.show();
							Datasave_viJdwlOperasi(false);
							//alert(Ext.getCmp('cboPerseoranganLab').getValue());
						}
				},
				'-',
				{
                        text: 'Batal jadwal',
                        id: 'btnBatalviJdwlOperasi',
                        tooltip: nmHapus,
                        iconCls: 'remove',
                        handler: function()
						{
							DataBatalJadwal_viJdwlOperasi(false);
						}
				}
				

			],
            listeners:
            {
                'afterrender': function()
                {

				},
				 'beforeclose': function(){clearInterval(intervalTime);}


            }
        }
    );

   return FormDepanviJdwlOperasi;

};


/* function load_data_pasienorder(param)
{
	//console.log(param);
	if (param==='' || param===undefined)
	{
		param={
			text: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/kamar_operasi/functionKamarOperasi/getpasien_order",
		params: param,
		failure: function(o)
		{
			var cst = Ext.decode(o.responseText);
		},
		success: function(o) {
			cbopasienorder_mng.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dspasienorder_mng.recordType;
				var o=cst['listData'][i];

				recs.push(new recType(o));
				dspasienorder_mng.add(recs);
				//console.log(o);
			}
		}
	});
} */

function total_pasien_order_mng()
{

	clearInterval(intervalTime);
	clearInterval();
	Ext.Ajax.request({
		url: baseURL + "index.php/main/functionLABPoliklinik/gettotalpasienorder_mng",
		params: {
			command: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
		},
		failure: function(o)
		{
			var cst = Ext.decode(o.responseText);
		},
		success: function(o) {
			var cst = Ext.decode(o.responseText);

			Ext.getCmp('txtcounttr').setValue(cst.jumlahpasien);
			//console.log(cst);

		}
	});
};

function getFormEntryviJdwlOperasi(lebar,data)
{

 var GDtabDetailviJdwlOperasi = new Ext.Panel
    (
        {
			id:'GDtabDetailviJdwlOperasi',
			region: 'center',
			activeTab: 0,
			height:380,
			//autoHeight:true,
			border:false,
			plain: true,
			defaults:{autoScroll: true},
			items:
			[
				grid_jad_ok()//Panel tab detail transaksi
				//-------------- ## --------------
			]
        }

    );

    return GDtabDetailviJdwlOperasi
};

function viewGridOrderAll_RASEPRWI(namax){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/kamar_operasi/functionKamarOperasi/getpasien_order",
			params: {
				nama	: namax,
				tgl_op	: Ext.getCmp('TglOperasi_viJdwlOperasi').getValue(),
				rwj_cb	: order_dari_rwj,
				igd_cb	: order_dari_igd,
				rwi_cb	: order_dari_rwi,
			},
			failure: function(o)
			{
				ShowPesanWarningviJdwlOperasi('Hubungi Admin', 'Error');
			},
			success: function(o)
			{   
				// dsGetdetail_order_OK
				dsGetdetail_order_OK.removeAll();
				var cst = Ext.decode(o.responseText);

				if (cst.processResult === 'SUCCESS')
				{


					var recs=[],
					recType=dsGetdetail_order_OK.recordType;

					for(var i=0; i<cst.listData.length; i++){

						recs.push(new recType(cst.listData[i]));


					}
					dsGetdetail_order_OK.add(recs);

					// console.log(dsGetdetail_order_OK);
					// gridGetdetail_order_OK.getView().refresh();

				}
				else
				{
					ShowPesanWarningviJdwlOperasi('Gagal membaca Data ini', 'Error');
				};
			}
		}

	);
}
function Getdetail_order_OK()
{
    dsGetdetail_order_OK = new WebApp.DataStore({ fields: ['kd_pasien','nama']});



    gridGetdetail_order_OK = new Ext.grid.EditorGridPanel
    (
        {
            // title: 'Order Unit Lain',
            stripeRows: true,
            store: dsGetdetail_order_OK,
			height:400,
			anchor: '100%',
            border: false,
            columnLines: true,
			frame: false,
			width:'100%',
            autoScroll:true,
            // sm: new Ext.grid.CellSelectionModel
            sm: new Ext.grid.RowSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
						}
                    }
                }
            ),
            cm: new Ext.grid.ColumnModel
				(
					[
						new Ext.grid.RowNumberer(),
						{
							header: 'Medrec',
							dataIndex: 'kd_pasien',
							width:100,
							menuDisabled:true,
							hidden :false

						},{
							header: 'Nama',
							dataIndex: 'nama',
							width:250,
							menuDisabled:true,
							hidden :false
						 },{
							header: 'alamat',
							dataIndex: 'alamat',
							width:200,
							menuDisabled:true,
							hidden :false
						 },{
							header: 'Asal Pasien',
							dataIndex: 'asal_pasien',
							width:150,
							menuDisabled:true,
							hidden :false

						}


					   ]
				),
				listeners: {
					rowdblclick: function (sm, ridx, cidx)
					{
						cellSelecteddeskripsi = dsGetdetail_order_OK.getAt(ridx);
						console.log(cellSelecteddeskripsi);
						// cbo_viComboJenisTindakan_viJdwlOperasi
						/* CurrentviJdwlOperasi.row = row;
						
						CurrentviJdwlOperasi.data = cellSelecteddeskripsi; */
						var_tampung_lab_order=true;
						TrviJdwlOperasi.form.ComboBox.namapasien.setValue(cellSelecteddeskripsi.data.nama);
						Ext.getCmp('txtNoMedrec_viJdwlOperasi').setValue(cellSelecteddeskripsi.data.kd_pasien);
						Ext.getCmp('txtAlamat_viJdwlOperasi').setValue(cellSelecteddeskripsi.data.alamat);
						Ext.getCmp('txtJam_viJdwlOperasi').setValue(cellSelecteddeskripsi.data.jam);
						Ext.getCmp('txtMenit_viJdwlOperasi').setValue(cellSelecteddeskripsi.data.menit);
						Ext.getCmp('TglOperasi_viJdwlOperasi').setValue(ShowDate(cellSelecteddeskripsi.data.tgl_op));
						kdUnitAsalPasien  = cellSelecteddeskripsi.data.kd_unit_knj;
						noKamarAsalPasien = cellSelecteddeskripsi.data.no_kamar_asal;
						tglMasukPasien    = cellSelecteddeskripsi.data.tgl_masuk_knj;
						urutMasukPasien   = cellSelecteddeskripsi.data.urut_masuk_knj;
						Ext.getCmp('cboSpesialisasi_SetupTindakanasi_OK').setValue(cellSelecteddeskripsi.data.kd_spesial);
						load_data_sub_spesialis_OK(cellSelecteddeskripsi.data.kd_spesial);
						Ext.getCmp('cboJenisOperasi_SetupTindakanasi_OK').setValue(cellSelecteddeskripsi.data.kd_jenis_op);
						
						var params = {
							kd_jenis_op : cellSelecteddeskripsi.data.kd_jenis_op,
							kd_sub_spc  : cellSelecteddeskripsi.data.kd_sub_spc,
						};
						dsComboTindakanOperasiJadwalOperasiOK(params, cellSelecteddeskripsi.data.kd_tindakan);

						tglorder=cellSelecteddeskripsi.data.tglorder;
						if(cellSelecteddeskripsi.data.asal_pasien=='rawat jalan'){
							Ext.getCmp('rdRWJOK_viJdwlOperasi').setValue(true);
							Ext.getCmp('rdRWIOK_viJdwlOperasi').setValue(false);
						}else if(cellSelecteddeskripsi.data.asal_pasien=='rawat Inap'){
							Ext.getCmp('rdRWJOK_viJdwlOperasi').setValue(false);
							Ext.getCmp('rdRWIOK_viJdwlOperasi').setValue(true);
						}
						Ext.getCmp('cbo_viComboKamar_viJdwlOperasi').setValue(cellSelecteddeskripsi.data.no_kamar);
						pnlTRSetAsisten_jadwaloperasiOK.close();
					},
				},
				tbar :[
				{
					xtype: 'label',
					text: 'Cari berdasarkan nama : ' 
				},
				{xtype: 'tbspacer',height: 3, width:5},
				{
					x: 40,
					y: 40,
					xtype: 'textfield',
					name: 'txtNamaOrderJadwalOperasi_KamarOperasi',
					id: 'txtNamaOrderJadwalOperasi_KamarOperasi',
					width: 120,
					listeners: 
					{ 
						'specialkey' : function()
						{
							if (Ext.EventObject.getKey() === 13) 
							{
								viewGridOrderAll_RASEPRWI(Ext.getCmp('txtNamaOrderJadwalOperasi_KamarOperasi').getValue());
							} 						
						}
					}
				},
				//mComboorder(),
				{xtype: 'tbspacer',height: 3, width:5},
				{
					xtype: 'label',
					text: 'Tgl. Operasi : ' 
				},
				{
					xtype: 'datefield',
					fieldLabel: 's/d ',
					id: 'dtpTglFilterOrder',
					name: 'dtpTglFilterOrder',
					format: 'd/M/Y',
					value: now,
					anchor: '100%',
					listeners:
					{
						'specialkey': function ()
						{
							if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10)
							{
								
							}
						}
					}
				},
				{xtype: 'tbspacer',height: 3, width:30},
				{
					xtype 	: 'label',
					text  	: 'RWJ',
					x 		: 320,
					x 		: 40,
				},
				{
					x 			: 340,
					y 			: 40, 
					xtype 		: 'checkbox',
					fieldLabel 	: 'RWJ',
					labelAlign 	: 'left',
					id			: 'checkboxRWJ',
					// header: 'Active?',
					// dataIndex: 'active', // model property to bind to
					width: 30,
					listeners 	: {
						change 	: function(a, value, c){
							order_dari_rwj = value;
						},
					}
				},
				{
					xtype 	: 'label',
					text  	: 'IGD',
					x 		: 400,
					x 		: 40,
				},
				{
					x 			: 440,
					y 			: 40, 
					xtype 		: 'checkbox',
					fieldLabel 	: 'IGD',
					labelAlign 	: 'left',
					id			: 'checkboxIGD',
					// header: 'Active?',
					// dataIndex: 'active', // model property to bind to
					width: 30,
					listeners 	: {
						change 	: function(a, value, c){
							order_dari_igd = value;
						},
					}
				},
				{
					xtype 	: 'label',
					text  	: 'RWI',
					x 		: 460,
					x 		: 40,
				},
				{
					x 			: 500,
					y 			: 40, 
					xtype 		: 'checkbox',
					fieldLabel 	: 'RWI',
					labelAlign 	: 'left',
					id			: 'checkboxRWI',
					// header: 'Active?',
					// dataIndex: 'active', // model property to bind to
					width: 30,
					listeners 	: {
						change 	: function(a, value, c){
							order_dari_rwi = value;
						},
					}
				},
				{xtype: 'tbspacer',height: 3, width:10},
				{
					x: 290,
					y: 40,
					xtype: 'button',
					text: 'Refresh Order',
					iconCls: 'refresh',
					hideLabel: false,
					handler: function(sm, row, rec) {
						//total_pasien_order_mng()
						viewGridOrderAll_RASEPRWI()
					}
				},
				],
        }


    );

    return gridGetdetail_order_OK;
};


function GetDTGridviJdwlOperasi()
{
    var fldDetail = [ 'tgl_op','jam_op_det', 'jam_op', 'durasi','jam_selesai', 'tgl_jadwal', 'kd_pasien','kd_dokter','nama_dokter', 'nama_pasien', 'alamat_pasien', 'kd_unit', 
						'no_kamar', 'nama_kamar', 'kd_unit_asal', 'no_kamar_asal','kd_tindakan', 'tindakan','kd_dokter','id_ket_ok', 'keterangan', 'sub_spesialisasi','jenis_op','spesialisasi','status'];

    dsTRDetailviJdwlOperasiList = new WebApp.DataStore({ fields: fldDetail })
	ViewGridBawahLookupviJdwlOperasi(" AND oj.tgl_op = '"+now.format("Y-m-d")+"'") ;
    gridDTJadwalOperasi = new Ext.grid.EditorGridPanel
    (
        {
            title: 'Jadwal Operasi',
            stripeRows: true,
            store: dsTRDetailviJdwlOperasiList,
            border: false,
			height:380,
			width:'100%',
            columnLines: true,
            frame: false,
            autoScroll:true,
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                            cellSelecteddeskripsi = dsTRDetailviJdwlOperasiList.getAt(row);
                            CurrentviJdwlOperasi.row = row;
                            CurrentviJdwlOperasi.data = cellSelecteddeskripsi;
							CurrentBatalviJdwlOperasi.data = cellSelecteddeskripsi;

							if(sm.selection.record.data.status == 1 || sm.selection.record.data.status == "1"){
								Ext.getCmp('btnBatalviJdwlOperasi').disable();
							}else{
								Ext.getCmp('btnBatalviJdwlOperasi').enable();
							}
						}
                    }
                }
            ), 
            cm: TRviJdwlOperasiColumModel(),
			listeners: {
				rowclick: function( $this, rowIndex, e )
				{
					console.log($this);
					cellCurrentLab = rowIndex;
				},
				celldblclick: function(gridView,htmlElement,columnIndex,dataRecord){
					var waktuopOK=gridView.store.data.items[htmlElement].data.jam_op;
					var jamopOK = waktuopOK.split(":");
					var duropOK=gridView.store.data.items[htmlElement].data.durasi;
					var durasiopOK = duropOK.split(" ");
					var tanggalOperasi1 = gridView.store.data.items[htmlElement].data.tgl_op.split(" ");
					var tanggalJadwal1 = gridView.store.data.items[htmlElement].data.tgl_jadwal.split(" ");
					//console.log(tanggalOperasi1);
					Ext.getCmp('txtJam_viJdwlOperasi').setValue(jamopOK[0]);
					Ext.getCmp('txtMenit_viJdwlOperasi').setValue(jamopOK[1]);
					Ext.getCmp('TglOperasi_viJdwlOperasi').setValue(tanggalOperasi1[0]);
					Ext.getCmp('cbo_viComboKamar_viJdwlOperasi').setValue(gridView.store.data.items[htmlElement].data.no_kamar);
					Ext.getCmp('cbo_viComboDokter_viJdwlOperasi').setValue(gridView.store.data.items[htmlElement].data.kd_dokter);
					Ext.getCmp('txtNoMedrec_viJdwlOperasi').setValue(gridView.store.data.items[htmlElement].data.kd_pasien);
					TrviJdwlOperasi.form.ComboBox.namapasien.setValue(gridView.store.data.items[htmlElement].data.nama_pasien);
					Ext.getCmp('txtAlamat_viJdwlOperasi').setValue(gridView.store.data.items[htmlElement].data.alamat_pasien);
					Ext.getCmp('txtDurasi_viJdwlOperasi').setValue(durasiopOK[0]);
					Ext.getCmp('DateTgljadwal_viJdwlOperasi').setValue(tanggalJadwal1[0]);
					Ext.getCmp('cbo_viComboJenisTindakan_viJdwlOperasi').setValue(gridView.store.data.items[htmlElement].data.tindakan);
					Ext.getCmp('cboSubSpesialisasi_SetupTindakanasi_OK').setValue(gridView.store.data.items[htmlElement].data.sub_spesialisasi);
					Ext.getCmp('cboJenisOperasi_SetupTindakanasi_OK').setValue(gridView.store.data.items[htmlElement].data.jenis_op);
					Ext.getCmp('cboSpesialisasi_SetupTindakanasi_OK').setValue(gridView.store.data.items[htmlElement].data.spesialisasi);
					// Ext.getCmp('txtKeterangan_viJdwlOperasi').setValue(gridView.store.data.items[htmlElement].data.keterangan);
					Ext.getCmp('btnSimpanviJdwlOperasi').disable();
				}
			},
			tbar :[
				// viComboTanggal_viJdwlOperasi(),
				/*{
					xtype 	: 'datefield',
					id 		: 'TglOperasi_Filter',						
					format 	: 'd/M/Y',
					width 	: 120,
					value 	:now,
					listeners:
					{ 
						'specialkey' : function()
						{
							if (Ext.EventObject.getKey() === 13) 
							{
								// datarefresh_viJdwlOperasi();				
								var tmp_tgl = Ext.getCmp('TglOperasi_Filter').getValue();
								ViewGridBawahLookupviJdwlOperasi(" AND oj.tgl_op = '"+tmp_tgl.format('Y-m-d')+"'");				
							} 						
						},
						'select' : function(a, b, c){
							ViewGridBawahLookupviJdwlOperasi(" AND oj.tgl_op = '"+a.value+"'");
						}
					}
				},
				{
					xtype 	: 'label',
					text 	: '|',
				},*/
				{
					xtype 	: 'button',
					id 		: 'btnGridOrderUnitLain',
					text	: 'Order unit Lain',
					width 	: 120,
					iconCls	: 'Edit_Tr',
					handler : function(){
						getPanelOrder_unit_lain();
					}
				},
			],
        }


    );

    return gridDTJadwalOperasi;
};


function grid_jad_ok(){
	var panel_jad_ok = new Ext.Panel({
		//title: 'Order Unit Lain',
		id:'grid_jad_ok',
        fileUpload: true,
        region: 'north',
        layout: 'column',
        height:'100%',
        anchor: '100%',
        width: '100%',
        border: false,
        items: [
        // Getdetail_order_OK(),
        GetDTGridviJdwlOperasi()
        ]
    });
	return panel_jad_ok;
}
function TRviJdwlOperasiColumModel()
{

	
    return new Ext.grid.ColumnModel
    (
        [ 
            new Ext.grid.RowNumberer(),
			{
				dataIndex: 'status',
				header: 'Status Operasi',
				sortable: true,
				hidden: false,
				width: 120,
				renderer: function(value, metaData, record, rowIndex, colIndex, store){
					switch (value){
						case '1':
							metaData.css = 'StatusHijau'; // 
							break;
						case '0':
							metaData.css = 'StatusMerah'; // rejected
							break;
					}
					return '';
				}
			},
			{
				dataIndex: 'tgl_op',
				header: 'Tanggal Operasi',
				sortable: true,
				width: 120,
				renderer: function(v, params, record)
				{
					return ShowDate(record.data.tgl_op);
				},	
			},
			{
				dataIndex: 'jam_op',
				header: 'Jam Operasi',
				sortable: true,
				width: 120
			},
			{
				dataIndex: 'nama_kamar',
				header: 'Kamar',
				sortable: true,
				width: 120
			},
			//-------------- ## --------------
			{
				dataIndex: 'kd_pasien',
				header: 'No. MedRec',
				sortable: true,
				width: 100
			},
			//-------------- ## --------------			
			{			
				dataIndex: 'nama_pasien',
				header: 'Nama Pasien',
				sortable: true,
				width: 150
			},
			//-------------- ## --------------
			{
				dataIndex: 'alamat_pasien',
				header: 'Alamat',
				sortable: true,
				width: 150,
				/* renderer: function(v, params, record) 
				{
					
				} */			
			},
			//-------------- ## --------------
			{
				dataIndex: 'nama_dokter',
				header: 'Dokter',
				sortable: true,
				width: 120,
				/* renderer: function(v, params, record) 
				{
					
				} */	
			},
			//-------------- ## --------------
			{
				dataIndex: 'tindakan',
				header: 'Tindakan',
				sortable: true,
				width: 150,
				/* renderer: function(v, params, record) 
				{
					
				} */	
			},
			//-------------- ## --------------
			{
				dataIndex: 'durasi',
				header: 'Durasi',
				sortable: true,
				width: 70,
				/* renderer: function(v, params, record) 
				{
					
				} */	
			},
			//-------------- ## --------------
			{
				dataIndex: 'jam_selesai',
				header: 'Jam Selesai',
				sortable: true,
				width: 80,
				/* renderer: function(v, params, record) 
				{
					
				} */	
			},
			//-------------- ## --------------
			{
				dataIndex: 'keterangan',
				header: 'Keterangan',
				sortable: true,
				width: 200,
				/* renderer: function(v, params, record) 
				{
					
				} */	
			}

        ]
    )
};

function RecordBaruRWJ()
{
	var tgltranstoday = Ext.getCmp('dtpKunjunganL').getValue();
	var p = new mRecordRwj
	(
		{
			'DESKRIPSI2':'',
			'KD_PRODUK':'',
		    'DESKRIPSI':'',
		    'KD_TARIF':'',
		    'HARGA':'',
		    'QTY':'',
		    'TGL_TRANSAKSI':tgltranstoday.format('Y/m/d'),
		    'DESC_REQ':'',
		    'KD_TARIF':'',
		    'URUT':''
		}
	);

	return p;
};


//tampil grid bawah penjas lab
function ViewGridBawahLookupviJdwlOperasi(criteria = "")
{
     var strKriteriaOK='';
    // strKriteriaOK = " oj.status=0 "+criteria+" order by oj.tgl_op, oj.jam_op desc";
   
	
	// MENAMBAHKAN KRITERIA KAMAR 
	
	Ext.Ajax.request
	({
		//store	: KasirLABDataStoreProduk,
		url 	: baseURL + "index.php/main/functionOK/GetKamarOK",
		params 	:  {
			text:''
		},
		failure : function(o)
		{
			ShowPesanWarningviJdwlOperasi("Gagal membaca Konfigurasi kamar operasi!",'WARNING');
		},
		success : function(o)
		{
			var cst = Ext.decode(o.responseText);
			//console.log( Ext.decode(o.responseText));
			if (cst.success == true){
				if (Ext.getCmp('rdRWIOK_viJdwlOperasi').getValue() === true) {
					var tgl_op    = Ext.getCmp('TglOperasi_viJdwlOperasi').getValue();
					strKriteriaOK +=" left(ojp.kd_unit_asal, 1) = '1' and ";	
					strKriteriaOK +=" ojp.tgl_op = '"+tgl_op.format('Y-m-d')+"' and";	
				}else if (Ext.getCmp('rdRWJOK_viJdwlOperasi').getValue() === true) {
					var tgl_masuk = Ext.getCmp('TglKunjungan_viJdwlOperasi').getValue();
					var tgl_op    = Ext.getCmp('TglOperasi_viJdwlOperasi').getValue();
					strKriteriaOK +=" left(ojp.kd_unit_asal, 1) in ('2') and ";	
					strKriteriaOK +=" (ojp.tgl_masuk_kunj = '"+tgl_masuk.format('Y-m-d')+"' or";	
					strKriteriaOK +=" ojp.tgl_op = '"+tgl_op.format('Y-m-d')+"') and";	
				}else{
					var tgl_masuk = Ext.getCmp('TglKunjungan_viJdwlOperasi').getValue();
					var tgl_op    = Ext.getCmp('TglOperasi_viJdwlOperasi').getValue();
					strKriteriaOK +=" left(ojp.kd_unit_asal, 1) in ('3') and ";	
					strKriteriaOK +=" (ojp.tgl_masuk_kunj = '"+tgl_masuk.format('Y-m-d')+"' or";	
					strKriteriaOK +=" ojp.tgl_op = '"+tgl_op.format('Y-m-d')+"') and";	
				}
				// strKriteriaOK = " oj.status=0 "+criteria+" and";
				strKriteriaOK +="  k.kd_unit ='"+cst.kamar+"' and ojp.no_kamar = '"+Ext.getCmp('cbo_viComboKamar_viJdwlOperasi').getValue()+"' order by oj.tgl_op desc";
				// console.log(strKriteriaOK);
				dsTRDetailviJdwlOperasiList.load
				(
					{
						params:
						{
							Skip: 0,
							Take: 1000,
							Sort: '',
							Sortdir: 'DESC',
							target: 'ViewDetailGridBawahviJdwlOperasi',
							param: strKriteriaOK
						}
					}
				);
				return dsTRDetailviJdwlOperasiList;
				
			}else{
				ShowPesanWarningviJdwlOperasi("Konfigurasi kamar operasi tidak ditemukan!",'WARNING');
			}
		}
	})
   
}

function ViewGridBawahviJdwlOperasi(no_transaksi,data)
{
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/main/functionLAB/getItemPemeriksaan",
			params: {no_transaksi:no_transaksi},
			failure: function(o)
			{
				//ShowPesanErrorviJdwlOperasi('Hubungi Admin', 'Error');
			},
			success: function(o)
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					dsTRDetailviJdwlOperasiList.removeAll();
					var recs=[],
						recType=dsTRDetailviJdwlOperasiList.recordType;

					for(var i=0; i<cst.ListDataObj.length; i++){

						recs.push(new recType(cst.ListDataObj[i]));

					}
					dsTRDetailviJdwlOperasiList.add(recs);

					gridDTJadwalOperasi.getView().refresh();

					if(dsTRDetailviJdwlOperasiList.getCount() > 0 ){
						//Ext.getCmp('cboDOKTER_viviJdwlOperasi').setReadOnly(true);
						TmpNotransaksi=data.no_transaksi;
						KdKasirAsal=data.kd_kasir;
						TglTransaksiAsal=data.tgl_transaksi;
						Kd_Spesial=data.kd_spesial;
						No_Kamar=data.no_kamar;
						//kunj_urut_masuk=data.urut_masuk;
						getDokterPengirim(data.no_transaksi,data.kd_kasir);
						//TrviJdwlOperasi.form.ComboBox.notransaksi.setValue(data.no_transaksi);
						Ext.getCmp('txtNamaUnitLab').setValue(data.nama_unit);
						Ext.getCmp('txtKdUnitLab').setValue(data.kd_unit);
						Ext.getCmp('txtKdDokter').setValue(data.kd_dokter);
						Ext.getCmp('cboDOKTER_viviJdwlOperasi').setValue(data.kd_dokter);
						Ext.getCmp('txtAlamatL').setValue(data.alamat);
						Ext.getCmp('dtpTtlL').setValue(ShowDate(data.tgl_lahir));
						Ext.getCmp('txtKdUrutMasuk').setValue(data.urut_masuk);
												console.log(data.urut_masuk);
						Ext.getCmp('cboJKviJdwlOperasi').setValue(data.jenis_kelamin);
						if (data.jenis_kelamin === 't')
						{
							Ext.getCmp('cboJKviJdwlOperasi').setValue('Laki-laki');
						}else
						{
							Ext.getCmp('cboJKviJdwlOperasi').setValue('Perempuan');
						}
						Ext.getCmp('cboJKviJdwlOperasi').disable();

						Ext.getCmp('cboGDRviJdwlOperasi').setValue(data.gol_darah);
						if (data.gol_darah === '0')
						{
							Ext.getCmp('cboGDRviJdwlOperasi').setValue('-');
						}else if (data.gol_darah === '1')
						{
							Ext.getCmp('cboGDRviJdwlOperasi').setValue('A+');
						}else if (data.gol_darah === '2')
						{
							Ext.getCmp('cboGDRviJdwlOperasi').setValue('B+');
						}else if (data.gol_darah === '3')
						{
							Ext.getCmp('cboGDRviJdwlOperasi').setValue('AB+');
						}else if (data.gol_darah === '4')
						{
							Ext.getCmp('cboGDRviJdwlOperasi').setValue('O+');
						}else if (data.gol_darah === '5')
						{
							Ext.getCmp('cboGDRviJdwlOperasi').setValue('A-');
						}else if (data.gol_darah === '6')
						{
							Ext.getCmp('cboGDRviJdwlOperasi').setValue('B-');
						}
						Ext.getCmp('cboGDRviJdwlOperasi').disable();

						Ext.getCmp('txtKdCustomerLamaHide').setValue(data.kd_customer);

						//tampung kd customer untuk transaksi selain kunjungan langsung
						vkode_customer = data.kd_customer;
						//Ext.getCmp('cboKelPasienLab').enable();

						if(data.kelpasien == 'Perseorangan'){
							Ext.getCmp('cboKelPasienLab').setValue(data.kelpasien);
							Ext.getCmp('cboPerseoranganLab').setValue(data.kd_customer);
							Ext.getCmp('cboPerseoranganLab').show();
						} else if(data.kelpasien == 'Perusahaan'){
							Ext.getCmp('cboKelPasienLab').setValue(data.kelpasien);
							Ext.getCmp('cboPerusahaanRequestEntryLab').setValue(data.kd_customer);
							Ext.getCmp('cboPerusahaanRequestEntryLab').show();
						} else{
							Ext.getCmp('cboKelPasienLab').setValue(data.kelpasien);
							Ext.getCmp('cboAsuransiLab').setValue(data.kd_customer);
							Ext.getCmp('cboAsuransiLab').show();
						}
						Ext.getCmp('txtDokterPengirimL').disable();
						Ext.getCmp('txtNamaUnitLab').disable();
						Ext.getCmp('txtAlamatL').disable();
						Ext.getCmp('cboJKviJdwlOperasi').disable();
						Ext.getCmp('cboGDRviJdwlOperasi').disable();
						Ext.getCmp('dtpTtlL').disable();
						Ext.getCmp('dtpKunjunganL').disable();
						Ext.getCmp('btnLookupItemPemeriksaan').disable();
						Ext.getCmp('btnHpsBrsItemLabL').disable();
						
						if (var_tampung_lab_order===true)
						{
						Ext.getCmp('cboDOKTER_viviJdwlOperasi').enable();
						Ext.getCmp('btnSimpanviJdwlOperasi').hide();
						gridDTJadwalOperasi.disable();
						Ext.getCmp('cboKelPasienLab').enable();
						Ext.getCmp('cboAsuransiLab').enable();
						Ext.getCmp('cboPerseoranganLab').enable();
						Ext.getCmp('cboPerusahaanRequestEntryLab').enable();
						Ext.getCmp('btnSimpanviJdwlOperasi_kiriman').show();
						
						}else{
						Ext.getCmp('cboDOKTER_viviJdwlOperasi').disable();
						Ext.getCmp('btnSimpanviJdwlOperasi').show();
						Ext.getCmp('btnSimpanviJdwlOperasi').disable();
						Ext.getCmp('btnSimpanviJdwlOperasi_kiriman').hide();
						
						gridDTJadwalOperasi.enable();
						Ext.getCmp('cboKelPasienLab').disable();
						Ext.getCmp('cboAsuransiLab').disable();
						Ext.getCmp('cboPerseoranganLab').disable();
						Ext.getCmp('cboPerusahaanRequestEntryLab').disable();
						var_tampung_lab_order=false;	
						}
						
					} else {
						TmpNotransaksi=data.no_transaksi;
						KdKasirAsal=data.kd_kasir;
						TglTransaksiAsal=data.tgl_transaksi;
						Kd_Spesial=data.kd_spesial;
						No_Kamar=data.no_kamar;
						Ext.getCmp('btnSimpanviJdwlOperasi').show();
						Ext.getCmp('btnSimpanviJdwlOperasi_kiriman').hide();
						Ext.getCmp('txtNamaUnitLab').setValue(data.nama_unit_asli);
						Ext.getCmp('txtKdUnitLab').setValue(data.kd_unit);
						Ext.getCmp('txtDokterPengirimL').setValue(data.dokter);
						Ext.getCmp('txtAlamatL').setValue(data.alamat);
						Ext.getCmp('dtpTtlL').setValue(ShowDate(data.tgl_lahir));

						Ext.getCmp('cboJKviJdwlOperasi').setValue(data.jenis_kelamin);
						if (data.jenis_kelamin === 't')
						{
							Ext.getCmp('cboJKviJdwlOperasi').setValue('Laki-laki');
						}else
						{
							Ext.getCmp('cboJKviJdwlOperasi').setValue('Perempuan');
						}
						Ext.getCmp('cboJKviJdwlOperasi').disable();

						Ext.getCmp('cboGDRviJdwlOperasi').setValue(data.gol_darah);
						if (data.gol_darah === '0')
						{
							Ext.getCmp('cboGDRviJdwlOperasi').setValue('-');
						}else if (data.gol_darah === '1')
						{
							Ext.getCmp('cboGDRviJdwlOperasi').setValue('A+');
						}else if (data.gol_darah === '2')
						{
							Ext.getCmp('cboGDRviJdwlOperasi').setValue('B+');
						}else if (data.gol_darah === '3')
						{
							Ext.getCmp('cboGDRviJdwlOperasi').setValue('AB+');
						}else if (data.gol_darah === '4')
						{
							Ext.getCmp('cboGDRviJdwlOperasi').setValue('O+');
						}else if (data.gol_darah === '5')
						{
							Ext.getCmp('cboGDRviJdwlOperasi').setValue('A-');
						}else if (data.gol_darah === '6')
						{
							Ext.getCmp('cboGDRviJdwlOperasi').setValue('B-');
						}
						Ext.getCmp('cboGDRviJdwlOperasi').disable();
						Ext.getCmp('txtKdCustomerLamaHide').setValue(data.kd_customer);
						vkode_customer = data.kd_customer;
						Ext.getCmp('cboKelPasienLab').enable();
						if(data.kelpasien == 'Perseorangan'){
							Ext.getCmp('cboKelPasienLab').setValue(data.kelpasien);
							Ext.getCmp('cboPerseoranganLab').setValue(data.kd_customer);
							Ext.getCmp('cboPerseoranganLab').show();
						} else if(data.kelpasien == 'Perusahaan'){
							Ext.getCmp('cboKelPasienLab').setValue(data.kelpasien);
							Ext.getCmp('cboPerusahaanRequestEntryLab').setValue(data.kd_customer);
							Ext.getCmp('cboPerusahaanRequestEntryLab').show();
						} else{
							Ext.getCmp('cboKelPasienLab').setValue(data.kelpasien);
							Ext.getCmp('cboAsuransiLab').setValue(data.kd_customer);
							Ext.getCmp('cboAsuransiLab').show();
						}

						Ext.getCmp('txtDokterPengirimL').disable();
						Ext.getCmp('txtNamaUnitLab').disable();
						Ext.getCmp('cboDOKTER_viviJdwlOperasi').enable();
						Ext.getCmp('txtAlamatL').disable();
						Ext.getCmp('cboJKviJdwlOperasi').disable();
						Ext.getCmp('cboGDRviJdwlOperasi').disable();
						Ext.getCmp('cboKelPasienLab').enable();
						Ext.getCmp('dtpTtlL').disable();
						Ext.getCmp('dtpKunjunganL').disable();
						Ext.getCmp('btnLookupItemPemeriksaan').enable();
						Ext.getCmp('btnHpsBrsItemLabL').disable();
						Ext.getCmp('btnSimpanviJdwlOperasi').enable();
						Ext.getCmp('cboKelPasienLab').disable();
						Ext.getCmp('cboAsuransiLab').disable();
						Ext.getCmp('cboPerseoranganLab').disable();
						Ext.getCmp('cboPerusahaanRequestEntryLab').disable();
						gridDTJadwalOperasi.enable();
						var_tampung_lab_order=false;
					
					
					}
				}
			}
		}

	)
};

//---------------------------------------------------------------------------------------///

//---------------------------------------------------------------------------------------///
function getParamDetailTransaksiOK()
{
	
	var jamOp=Ext.getCmp('txtJam_viJdwlOperasi').getValue()+":"+Ext.getCmp('txtMenit_viJdwlOperasi').getValue()+":00";
	
	var params =
	{
		tglOp: Ext.getCmp('TglOperasi_viJdwlOperasi').getValue().format("Y-m-d"),
		NoKamar:Ext.getCmp('cbo_viComboKamar_viJdwlOperasi').getValue(),
		KdDokter:Ext.getCmp('cbo_viComboDokter_viJdwlOperasi').getValue(),
		KdUnitAsal:kdUnitAsalPasien,
		NoKamarAsal:noKamarAsalPasien,
		tglMasuk:tglMasukPasien,
		urutMasuk:urutMasukPasien,
		KdPasien:Ext.getCmp('txtNoMedrec_viJdwlOperasi').getValue(),
		NmPasien:TrviJdwlOperasi.form.ComboBox.namapasien.getValue(),
		Alamat:Ext.getCmp('txtAlamat_viJdwlOperasi').getValue(),
		Durasi:Ext.getCmp('txtDurasi_viJdwlOperasi').getValue(),
		tgljadwal:Ext.getCmp('DateTgljadwal_viJdwlOperasi').getValue(),
		jamOperasi:jamOp,
		key_data:'ok_default_kamar',
		kdJenisTindakan:Ext.getCmp('cbo_viComboJenisTindakan_viJdwlOperasi').getValue(),
		// keterangan:Ext.getCmp('txtKeterangan_viJdwlOperasi').getValue(),
		tglorder:tglorder,
		kd_sub_spc:Ext.getCmp('cboSubSpesialisasi_SetupTindakanasi_OK').getValue(),
		kd_jenis_op:Ext.getCmp('cboJenisOperasi_SetupTindakanasi_OK').getValue(),
		
	};
	
	//params['KodeUnitSekarang']=kdUnit;	
	/* params['jumlahperawat']=secondGridStore.getCount();
	for(var i = 0 ; i < secondGridStore.getCount();i++)
	{
		params['kd_dokter-'+i]=secondGridStore.data.items[i].data.kd_perawat;
	} */
	console.log(secondGridStore2.data);
	params['jumlahasisten']=secondGridStore2.getCount();
	for(var i = 0 ; i < secondGridStore2.getCount();i++)
	{
		params['kd_asisten-'+i]=secondGridStore2.data.items[i].data.kd_asisten;
		params['jenis_asisten-'+i]=secondGridStore2.data.items[i].data.jenis;
	}
	
	/* params['jumlahinstrumen']=secondGridStore3.getCount();
	for(var i = 0 ; i < secondGridStore3.getCount();i++)
	{
		params['kd_instrumen-'+i]=secondGridStore3.data.items[i].data.kd_asisten;
	}
	
	params['jumlahsirkulasi']=secondGridStore4.getCount();
	for(var i = 0 ; i < secondGridStore4.getCount();i++)
	{
		params['kd_sirkulasi-'+i]=secondGridStore4.data.items[i].data.kd_asisten;
	} */
    return params
};
function getParamDetailBatalJadwalOK()
{
	var datanya=CurrentBatalviJdwlOperasi.data.data;
	var params =
	{
		tglOp: datanya.tgl_op,
		jamOperasi:datanya.jam_op_det,
		kdUnit:datanya.kd_unit,
		kdUnitAsal:datanya.kd_unit_asal,
		kdPasien:datanya.kd_pasien,
		NoKamar:datanya.no_kamar,
		KdDokter:datanya.kd_dokter,
		idKet:datanya.id_ket_ok,
		key_data:'ok_default_kamar',
	};
	/* params['jumlahperawat']=secondGridStore.getCount();
	for(var i = 0 ; i < secondGridStore.getCount();i++)
	{
		params['kd_dokter-'+i]=secondGridStore.data.items[i].data.kd_perawat;
	} */
	
	params['jumlahasisten']=secondGridStore2.getCount();
	for(var i = 0 ; i < secondGridStore2.getCount();i++)
	{
		params['kd_asisten-'+i]=secondGridStore2.data.items[i].data.kd_asisten;
		params['jenis_asisten-'+i]=secondGridStore2.data.items[i].data.jenis;
	}
    return params
};
function GetListCountDetailTransaksi()
{

	var x=0;
	for(var i = 0 ; i < dsTRDetailviJdwlOperasiList.getCount();i++)
	{
		if (dsTRDetailviJdwlOperasiList.data.items[i].data.KD_PRODUK != '' || dsTRDetailviJdwlOperasiList.data.items[i].data.DESKRIPSI  != '')
		{
			x += 1;
		};
	}
	return x;

};

function getArrviJdwlOperasi()
{
	var x='';
	var arr=[];
	for(var i = 0 ; i < dsTRDetailviJdwlOperasiList.getCount();i++)
	{

		if (dsTRDetailviJdwlOperasiList.data.items[i].data.kd_produk != '' && dsTRDetailviJdwlOperasiList.data.items[i].data.deskripsi != '')
		{
			var o={};
			var y='';
			var z='@@##$$@@';
			o['URUT']= dsTRDetailviJdwlOperasiList.data.items[i].data.urut;
			o['KD_PRODUK']= dsTRDetailviJdwlOperasiList.data.items[i].data.kd_produk;
			o['QTY']= dsTRDetailviJdwlOperasiList.data.items[i].data.qty;
			o['TGL_TRANSAKSI']= dsTRDetailviJdwlOperasiList.data.items[i].data.tgl_transaksi;
			o['TGL_BERLAKU']= dsTRDetailviJdwlOperasiList.data.items[i].data.tgl_berlaku;
			o['HARGA']= dsTRDetailviJdwlOperasiList.data.items[i].data.harga;
			o['KD_TARIF']= dsTRDetailviJdwlOperasiList.data.items[i].data.kd_tarif;
			o['cito']= dsTRDetailviJdwlOperasiList.data.items[i].data.cito;
			arr.push(o);

		};
	}

	return Ext.encode(arr);
};


function getFormItemEntry_viJdwlOperasi(lebar,rowdata)
{
    var pnlFormDataBasic_viJdwlOperasi = new Ext.FormPanel
    (
		{
			title: '',
			region: 'north',
			layout: 'form',
			//bodyStyle: 'padding:10px 10px 10px 10px',
			anchor: '100%',
			width: lebar,
			border: false,
			//-------------- #items# --------------
			items:
			[
				getItemPanelInformasi_viJdwlOperasi(lebar),
				getItemPanelInputJadwal_viJdwlOperasi(lebar), 				
				//getItemGridTransaksi_viJdwlOperasi(lebar)
			],
			//-------------- #End items# --------------
			fileUpload: true,
			// Tombol pada tollbar Edit Data Pasien
			/* tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						id: 'btnAdd_viJdwlOperasi',
						handler: function(){
							dataaddnew_viJdwlOperasi();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viJdwlOperasi',
						handler: function()
						{
							datasave_viJdwlOperasi(addNew_viJdwlOperasi);
							datarefresh_viJdwlOperasi();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						id: 'btnSimpanExit_viJdwlOperasi',
						handler: function()
						{
							var x = datasave_viJdwlOperasi(addNew_viJdwlOperasi);
							datarefresh_viJdwlOperasi();
							if (x===undefined)
							{
								setLookUps_viJdwlOperasi.close();
							}
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viJdwlOperasi',
						handler: function()
						{
							datadelete_viJdwlOperasi();
							datarefresh_viJdwlOperasi();							
						}
					},
					//-------------- ## --------------
					{
						xtype:'tbseparator'
					}
					//-------------- ## --------------					
				]
			} *///,items:
		}
    )

    return pnlFormDataBasic_viJdwlOperasi;
}

function getItemPanelInformasi_viJdwlOperasi(lebar) 
{
	var rdJenisModul_viJdwlOperasi = new Ext.form.RadioGroup
	({  
        columns: 2, 
        width: 250,
        items: 
		[  
			{
				boxLabel: 'Rawat Jalan', 
				width:50,
				id: 'rdRWJOK_viJdwlOperasi', 
				name: 'rdOK_viJdwlOperasi', 
				checked:true,
				inputValue: '1'
			},  
			{
				boxLabel: 'UGD', 
				width:50,
				id: 'rdIGDOK_viJdwlOperasi', 
				name: 'rdOK_viJdwlOperasi', 
				checked:true,
				inputValue: '1'
			},  
			//-------------- ## --------------
			{
				boxLabel: 'Rawat Inap', 
				width:50,
				id: 'rdRWIOK_viJdwlOperasi', 
				name: 'rdOK_viJdwlOperasi', 
				inputValue: '2'
			} 
			//-------------- ## --------------
        ],
		listeners: {
				change: function(field, newValue, oldValue) {
					if (Ext.getCmp('rdRWIOK_viJdwlOperasi').getValue()===true){
						kodeKasirNya='RWI';
						Ext.getCmp('TglKunjungan_viJdwlOperasi').disable();
					}else if (Ext.getCmp('rdRWJOK_viJdwlOperasi').getValue()===true){
						kodeKasirNya='RWJ';
						Ext.getCmp('TglKunjungan_viJdwlOperasi').enable();
					}else{
						kodeKasirNya='IGD';
						Ext.getCmp('TglKunjungan_viJdwlOperasi').enable();
					}
					Ext.getCmp('txtNoMedrec_viJdwlOperasi').setValue('');
					TrviJdwlOperasi.form.ComboBox.namapasien.setValue('');
					Ext.getCmp('txtAlamat_viJdwlOperasi').setValue(''); 
					kdUnitAsalPasien='';
					noKamarAsalPasien='';
					tglMasukPasien='';
					urutMasukPasien='';
					ViewGridBawahLookupviJdwlOperasi();		
				}
			}		
    });
	
    var items =
	{
	    layout: 'Form',
	    anchor: '100%',
		title: 'Informasi Jadwal',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		//border:true,
		items:
		[	
			{
				xtype: 'compositefield',
				fieldLabel: 'Asal',
				anchor: '100%',
				width: 199,
				items: 
				[
					rdJenisModul_viJdwlOperasi	
				]
			},
			{
				xtype: 'compositefield',
				fieldLabel: 'Tgl. kunjungan',
				anchor: '100%',
				width: 199,
				items: 
				[
					{
						xtype: 'datefield',
						id: 'TglKunjungan_viJdwlOperasi',						
						format: 'd/M/Y',
						width: 120,
						value:now,
						listeners:
						{ 

							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									// datarefresh_viJdwlOperasi();	
									var tmp_kamar 	= Ext.getCmp('cbo_viComboKamar_viJdwlOperasi').getValue();
									var tmp_tgl 	= Ext.getCmp('TglOperasi_viJdwlOperasi').getValue();
									if (tmp_kamar.length > 0) {
										criteria_kamar = " AND ojp.no_kamar = '"+tmp_kamar+"'";
									}else{
										criteria_kamar = "";
									}
									ViewGridBawahLookupviJdwlOperasi(" AND oj.tgl_op = '"+tmp_tgl.format('Y-m-d')+"'"+criteria_kamar);				
								} 						
							},
						}
					}
				]
			},
			{
				xtype: 'compositefield',
				fieldLabel: 'Tgl. Operasi',
				anchor: '100%',
				width: 199,
				items: 
				[
					{
						xtype: 'datefield',
						id: 'TglOperasi_viJdwlOperasi',						
						format: 'd/M/Y',
						width: 120,
						value:now,
						listeners:
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									// datarefresh_viJdwlOperasi();	
									var tmp_kamar 	= Ext.getCmp('cbo_viComboKamar_viJdwlOperasi').getValue();
									var tmp_tgl 	= Ext.getCmp('TglOperasi_viJdwlOperasi').getValue();
									if (tmp_kamar.length > 0) {
										criteria_kamar = " AND ojp.no_kamar = '"+tmp_kamar+"'";
									}else{
										criteria_kamar = "";
									}
									ViewGridBawahLookupviJdwlOperasi(" AND oj.tgl_op = '"+tmp_tgl.format('Y-m-d')+"'"+criteria_kamar);				
								} 						
							},
							'select' : function(a, b, c){
								var tmp_kamar 	= Ext.getCmp('cbo_viComboKamar_viJdwlOperasi').getValue();
								if (tmp_kamar.length > 0) {
									criteria_kamar = " AND ojp.no_kamar = '"+tmp_kamar+"'";
								}else{
									criteria_kamar = "";
								}
								ViewGridBawahLookupviJdwlOperasi(" AND oj.tgl_op = '"+a.value+"'"+criteria_kamar);
							}
						}
					},
					{
						xtype: 'displayfield',				
						width: 50,								
						value: 'Kamar:',
						fieldLabel: 'Label',
					},
					viComboKamar_viJdwlOperasi(),
					{
						xtype 	: 'button',
						text 	: 'Refresh',
						handler : function(){
							ViewGridBawahLookupviJdwlOperasi();
						}
					}
				]
			}

		]
	}
	return items;
}
function dsComboKamarOperasiJadwalOperasiOK()
{
	dsvComboKamarOperasiJadwalOperasiOK.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
					Sortdir: 'ASC', 
                    target: 'vComboKamarOperasiJadwalOperasiOK',
					//param:'kd_spesial='+kriteria
                }			
            }
        );   
    return dsvComboKamarOperasiJadwalOperasiOK;
}
function viComboKamar_viJdwlOperasi()
{
	var Field =['kd_unit','no_kamar','nama_kamar','jumlah_bed','digunakan'];
    dsvComboKamarOperasiJadwalOperasiOK = new WebApp.DataStore({fields: Field});
	dsComboKamarOperasiJadwalOperasiOK();
  	var cbo_viComboKamar_viJdwlOperasi = new Ext.form.ComboBox
    (
    
        {
            id:"cbo_viComboKamar_viJdwlOperasi",
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'Pilih Kamar..',
            fieldLabel: "Kamar",     
            width:230,
            store:dsvComboKamarOperasiJadwalOperasiOK,
            valueField: 'no_kamar',
            displayField: 'nama_kamar',
            listeners:  
            {
				'select': function(a,b,c)
				{
					var tmp_kamar 	= b.data.no_kamar;
					var tmp_tgl 	= Ext.getCmp('TglOperasi_viJdwlOperasi').getValue();
					if (tmp_kamar.length > 0) {
						criteria_kamar = " AND ojp.no_kamar = '"+tmp_kamar+"'";
					}else{
						criteria_kamar = "";
					}
					ViewGridBawahLookupviJdwlOperasi();
				},
				'render': function (c) {
				    c.getEl().on('keypress', function (e) {
				        if (e.getKey() == 13 ) //atau Ext.EventObject.ENTER
				            Ext.getCmp('txtNoMedrec_viJdwlOperasi').focus();
				    }, c);
				}
            }
        }
    );
    return cbo_viComboKamar_viJdwlOperasi;
};

function viComboTanggal_viJdwlOperasi()
{
	var Field =['tgl_op'];
    viComboTanggal_viJdwlOperasi = new WebApp.DataStore({fields: Field});
  	var cbo_ComboTanggal_viJdwlOperasi = new Ext.form.ComboBox
    (
    
        {
            id:"cbo_ComboTanggal_viJdwlOperasi",
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'Pilih Tanggal Operasi ...',
            fieldLabel: "Tanggal Operasi",     
            width:230,
            store:dsTRDetailviJdwlOperasiList,
            valueField: 'tgl_op',
            displayField: 'tgl_op',
            listeners:  
            {
				'select': function(a,b,c)
				{
					
				},
            }
        }
    );
    return cbo_ComboTanggal_viJdwlOperasi;
};

function getItemPanelInputJadwal_viJdwlOperasi(lebar) 
{
    var items =
	{
	    layout: 'Fit',
	    anchor: '100%',
	    width: lebar- 10,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		border:false,
		height:210,
		items:
		[		
			
					{ //awal panel biodata pasien
						columnWidth:.99,
						layout: 'absolute',
						border: false,
						bodyStyle: 'padding:0px 0px 0px 0px',
						width: 100,
						height: 125,
						anchor: '100% 100%',
						items:
						[
                            //bagian pinggir kiri
							{
								x: 10,
								y: 10,
								xtype: 'label',
								text: 'No. Medrec  '
							},
							{
								x: 120,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								xtype: 'textfield',		
								x : 130,
								y : 10,
								emptyText: 'No medrec',
								fieldLabel: '',		
								width: 100,
								name: 'txtNoMedrec_viJdwlOperasi',
								id: 'txtNoMedrec_viJdwlOperasi',
								enableKeyEvents: true,
								listeners:{
									'specialkey': function (){
										if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9){
											kodeKasirNya = "RWI";

											if (Ext.getCmp('rdRWJOK_viJdwlOperasi').getValue() == true) {
												kodeKasirNya = "RWJ";
											}else if(Ext.getCmp('rdIGDOK_viJdwlOperasi').getValue() == true){
												kodeKasirNya = "IGD";
											}
											var a=0;
											Ext.Ajax.request
											({
												//store	: KasirLABDataStoreProduk,
												url 	: baseURL + "index.php/main/functionOK/getPasien",
												params 	:  {
													kd_kasir:kodeKasirNya,
													a:1,
													text: Ext.getCmp('txtNoMedrec_viJdwlOperasi').getValue(),
													tgl_kunjungan : Ext.getCmp('TglKunjungan_viJdwlOperasi').getValue()
												},
												failure : function(o)
												{
													ShowPesanWarningviJdwlOperasi("Pasien tidak ditemukan!",'WARNING');
												},
												success : function(o)
												{
													var cst = Ext.decode(o.responseText);
													if (cst.processResult == 'SUCCESS'){
														if (cst.listData.length>0){
															Ext.getCmp('txtNoMedrec_viJdwlOperasi').setValue(cst.noMedrec);
															TrviJdwlOperasi.form.ComboBox.namapasien.setValue(cst.listData[0].nama);
															Ext.getCmp('txtAlamat_viJdwlOperasi').setValue(cst.listData[0].alamat); 
															kdUnitAsalPasien=cst.listData[0].kd_unit;
															noKamarAsalPasien=cst.listData[0].no_kamar;
															tglMasukPasien=cst.listData[0].masuk;
															urutMasukPasien=cst.listData[0].urut_masuk;
															dsComboTindakanOperasiJadwalOperasiOK(cst.listData[0].kd_unit);
														}else{
															ShowPesanWarningviJdwlOperasi("Pasien tidak ditemukan!",'WARNING');
															Ext.getCmp('txtNoMedrec_viJdwlOperasi').setValue(cst.noMedrec);
															TrviJdwlOperasi.form.ComboBox.namapasien.setValue('');
															Ext.getCmp('txtAlamat_viJdwlOperasi').setValue(''); 
															kdUnitAsalPasien='';
															noKamarAsalPasien='';
															tglMasukPasien='';
															urutMasukPasien='';
														}
														
													}else{
														ShowPesanWarningviJdwlOperasi("Pasien tidak ditemukan!",'WARNING');
													}
												}
											})
										}
										
									},
									'render': function (c) {
									    c.getEl().on('keypress', function (e) {
									        if (e.getKey() == 13 ) //atau Ext.EventObject.ENTER
									            Ext.getCmp('cbo_viComboDokter_viJdwlOperasi').focus();
									    }, c);
									}
								}
							},
							/* TrviJdwlOperasi.form.ComboBox.kdpasien= new Nci.form.Combobox.autoComplete({
								store	: TrviJdwlOperasi.form.ArrayStore.kodepasien,
								enableKeyEvents  	: true,
								select	: function(a,b,c){
									TrviJdwlOperasi.form.ComboBox.namapasien.setValue(b.data.nama);
									Ext.getCmp('txtAlamat_viJdwlOperasi').setValue(b.data.alamat); 
									kdUnitAsalPasien=b.data.kd_unit;
									noKamarAsalPasien=b.data.no_kamar;
									tglMasukPasien=b.data.masuk;
									urutMasukPasien=b.data.urut_masuk;
								},
								width	: 100,
								insert	: function(o){
									return {
										nama        		:o.nama,
										tgl_transaksi		:o.tgl_transaksi,
										kd_spesial			:o.kd_spesial,
										kamar				:o.kamar,
										masuk				:o.masuk,
										tgl_lahir			:o.tgl_lahir,
										gol_darah			:o.gol_darah,
										jenis_kelamin		:o.jenis_kelamin,
										dokter				:o.dokter,
										urut_masuk			:o.urut_masuk,
										nama_unit			:o.nama_unit,
										alamat				:o.alamat,
										kd_pasien        	:o.kd_pasien,
										kd_dokter			:o.kd_dokter,
										kd_unit				:o.kd_unit,
										kd_customer			:o.kd_customer,
										kd_kasir			:o.kd_kasir,
										nama_kamar			:o.nama_kamar,
										no_kamar			:o.no_kamar,
										customer			:o.customer,
										nama_unit_asli		:o.nama_unit_asli,
										tgl					:o.tgl,
										kd_tarif			:o.kd_tarif,
										kelpasien			:o.kelpasien,
										text				:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_pasien+'</td><td width="70" align="left">'+o.no_transaksi+'</td><td width="130">'+o.nama+'</td><td width="110" align="left">'+o.nama_unit_asli+'</td><td width="70">'+o.tgl+'</td></tr></table>',
										no_transaksi		:o.no_transaksi,
									}
								},
								param:function(){
									var a=0;
									return {
										kd_kasir:kodeKasirNya,
										a:1
									}
								},
								url		: baseURL + "index.php/main/functionOK/getPasien",
								valueField: 'kd_pasien',
								displayField: 'text',
								listWidth: 480,
								x : 130,
								y : 10,
								emptyText: 'No medrec'
							}), */
							{
								x: 10,
								y: 40,
								xtype: 'label',
								text: 'Alamat '
							},
							{
								x: 120,
								y: 40,
								xtype: 'label',
								text: ':'
							},
							{
								xtype: 'textfield',		
								x: 130,
								y: 40,
								fieldLabel: '',		
								width: 470,
								name: 'txtAlamat_viJdwlOperasi',
								id: 'txtAlamat_viJdwlOperasi',
								emptyText: 'Alamat',
								disabled:true
							},
							{
								x: 10,
								y: 70,
								xtype: 'label',
								text: 'Tgl. Jadwal  '
							},
							{
								x: 120,
								y: 70,
								xtype: 'label',
								text: ':'
							},
							{
								xtype: 'datefield',
								fieldLabel: 'Tanggal',
								x: 130,
								y: 70,
								id: 'DateTgljadwal_viJdwlOperasi',
								name: 'DateTgljadwal_viJdwlOperasi',
								width: 130,
								format: 'd/M/Y',
								value:now,
								disabled:false
							},
							{
								x: 10,
								y: 100,
								xtype: 'label',
								text: 'Jam Operasi'
							},
							{
								x: 120,
								y: 100,
								xtype: 'label',
								text: ':'
							},
							{
								xtype: 'textfield',
								x: 130,
								y: 100,
								fieldLabel: 'Jam',
								id: 'txtJam_viJdwlOperasi',
								name: 'txtJam_viJdwlOperasi',
								width: 50,
								emptyText:'Jam',
								maxLength:2,
								listeners : {
					                'render': function (c) {
					                    c.getEl().on('keypress', function (e) {
					                        if (e.getKey() == 13 ) //atau Ext.EventObject.ENTER
					                            Ext.getCmp('txtMenit_viJdwlOperasi').focus();
					                    }, c);
					                }
								}
							},
							{
								xtype: 'textfield',
								x: 190,
								y: 100,
								fieldLabel: 'Menit',
								id: 'txtMenit_viJdwlOperasi',
								name: 'txtMenit_viJdwlOperasi',
								width: 50,
								emptyText:'Menit',
								maxLength:2,
								listeners : {
					                'render': function (c) {
					                    c.getEl().on('keypress', function (e) {
					                        if (e.getKey() == 13 ) //atau Ext.EventObject.ENTER
					                            Ext.getCmp('txtDurasi_viJdwlOperasi').focus();
					                    }, c);
					                }
								}
							},
							
							/*{
								x: 10,
								y: 130,
								xtype: 'label',
								text: 'Spesialisasi'
							},
							{
								x: 120,
								y: 130,
								xtype: 'label',
								text: ':'
							},//{
								mcomboSpesialis_OKasiOKPoliklinik(),*/
							//},
							/*{
								x: 280,
								y: 130,
								xtype: 'label',
								text: 'Sub Spesial'
							},
							{
								x: 390,
								y: 130,
								xtype: 'label',
								text: ':'
							},//{
								mComboSubSpesialisasiOKPoliklinik(),*/
							//},
							{
								x: 10,
								y: 130,
								xtype: 'label',
								text: 'Spesialisasi '
							},
							{
								x: 120,
								y: 130,
								xtype: 'label',
								text: ':'
							},
								comboSpesialis_OK(),
							{
								x: 360,
								y: 130,
								xtype: 'label',
								text: 'Sub Spesialisasi'
							},
							{
								x: 470,
								y: 130,
								xtype: 'label',
								text: ':'
							},
								comboSubSpesialis_OK(),
							{
								x: 10,
								y: 160,
								xtype: 'label',
								text: 'Jenis Operasi'
							},
							{
								x: 120,
								y: 160,
								xtype: 'label',
								text: ':'
							},
								comboJenisOperasi_OK(),
								// mComboKamarOK_SubSpesialisasi(),
							{
								x: 360,
								y: 160,
								xtype: 'label',
								text: 'Jenis Tindakan'
							},
							{
								x: 470,
								y: 160,
								xtype: 'label',
								text: ':'
							},//{
								viComboJenisTindakan_viJdwlOperasi(),
							// },
							/*{
								x: 10,
								y: 190,
								xtype: 'label',
								text: 'Keterangan '
							},
							{
								x: 120,
								y: 190,
								xtype: 'label',
								text: ':'
							},
							{
								xtype: 'textfield',
								x: 130,
								y: 190,
								fieldLabel: 'Keterangan',
								id: 'txtKeterangan_viJdwlOperasi',
								name: 'txtKeterangan_viJdwlOperasi',
								width: 470
							},*/
							{
								x: 280,
								y: 10,
								xtype: 'label',
								text: 'Nama Pasien '
							},
							{
								x: 390,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							TrviJdwlOperasi.form.ComboBox.namapasien= new Nci.form.Combobox.autoComplete({
								store	: TrviJdwlOperasi.form.ArrayStore.pasien,
								select	: function(a,b,c){
									Ext.getCmp('txtNoMedrec_viJdwlOperasi').setValue(b.data.kd_pasien);
									Ext.getCmp('txtAlamat_viJdwlOperasi').setValue(b.data.alamat); 
									kdUnitAsalPasien=b.data.kd_unit;
									noKamarAsalPasien=b.data.no_kamar;
									tglMasukPasien=b.data.masuk;
									urutMasukPasien=b.data.urut_masuk;
									// dsComboTindakanOperasiJadwalOperasiOK(b.data.kd_unit);
								},
								width	: 200,
								insert	: function(o){
									return {
										nama        		:o.nama,
										tgl_transaksi		:o.tgl_transaksi,
										kd_spesial			:o.kd_spesial,
										kamar				:o.kamar,
										masuk				:o.masuk,
										tgl_lahir			:o.tgl_lahir,
										gol_darah			:o.gol_darah,
										jenis_kelamin		:o.jenis_kelamin,
										dokter				:o.dokter,
										urut_masuk			:o.urut_masuk,
										nama_unit			:o.nama_unit,
										alamat				:o.alamat,
										kd_pasien        	:o.kd_pasien,
										kd_dokter			:o.kd_dokter,
										kd_unit				:o.kd_unit,
										kd_customer			:o.kd_customer,
										kd_kasir			:o.kd_kasir,
										nama_kamar			:o.nama_kamar,
										no_kamar			:o.no_kamar,
										customer			:o.customer,
										nama_unit_asli		:o.nama_unit_asli,
										tgl					:o.tgl,
										kd_tarif			:o.kd_tarif,
										kelpasien			:o.kelpasien,
										text				:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_pasien+'</td><td width="70" align="left">'+o.no_transaksi+'</td><td width="130">'+o.nama+'</td><td width="110" align="left">'+o.nama_unit_asli+'</td><td width="70">'+o.tgl+'</td></tr></table>',
										no_transaksi		:o.no_transaksi,
									}
								},
								param:function(){
									var a=0;
									return {
										kd_kasir:kodeKasirNya,
										a:2,
										tgl_kunjungan : Ext.getCmp('TglKunjungan_viJdwlOperasi').getValue()
									}
								},
								url		: baseURL + "index.php/main/functionOK/getPasien",
								valueField: 'nama',
								displayField: 'text',
								listWidth: 480,
								x : 400,
								y : 10,
								emptyText: 'Nama Pasien'
							}),
							/* {
								xtype: 'textfield',		
								x: 400,
								y: 10,
								fieldLabel: '',		
								width: 200,
								name: 'txtNama_viJdwlOperasi',
								id: 'txtNama_viJdwlOperasi',
								emptyText: 'Nama',
								disabled:true
							}, */
							{
								x: 280,
								y: 70,
								xtype: 'label',
								text: 'Dokter '
							},
							{
								x: 390,
								y: 70,
								xtype: 'label',
								text: ':'
							},
							 viComboDokter_viJdwlOperasi(),
							{
								x: 280,
								y: 100,
								xtype: 'label',
								text: 'Durasi'
							},
							{
								x: 390,
								y: 100,
								xtype: 'label',
								text: ':'
							},
							{
								xtype: 'numberfield',					
								fieldLabel: '',		
								x: 400,
								y: 100,								
								width: 80,
								name: 'txtDurasi_viJdwlOperasi',
								id: 'txtDurasi_viJdwlOperasi',
								emptyText: '',
								listeners:{
					                'render': function (c) {
					                    c.getEl().on('keypress', function (e) {
					                        if (e.getKey() == 13 ) //atau Ext.EventObject.ENTER
					                            Ext.getCmp('cboSpesialisasi_SetupTindakanasi_OK').focus();
					                    }, c);
					                }
								}
							},
							{
								xtype: 'displayfield',				
								width: 180,			
								x: 490,
								y: 100,									
								value: '(Dalam Menit)',
								fieldLabel: 'Label',
								id: 'lblKetDurasi_viJdwlOperasi',
								name: 'lblKetDurasi_viJdwlOperasi'
							},
							{
								xtype 	: 'button',
								text 	: 'Asisten',
								id 		: 'idButtonAsisten',
								x 		: 620,
								y 		: 0,
								// listeners: {
								handler : function(){
									var tmp_medrec = Ext.getCmp('txtNoMedrec_viJdwlOperasi').getValue();
									if (tmp_medrec.length ) {}
									GetTabSetAsistenJadwalOK();
								}
								// }
							},
							// tabOptionalDokterPerawatJadwalOk()
							

						]
					},
			
		] 
	};
    return items;
};
function dsComboDokterJadwalOperasiOK()
{
	dsvComboDokterJadwalOperasiOK.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'vComboDokterJadwalOperasiOK',
                }			
            }
        );   
    return dsvComboDokterJadwalOperasiOK;
}
function dsGridSetPerawatJadwalOperasiOK(kriteria)
{
	dsvSetPerawatJadwalOperasiOK.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'vDDGridSetPerawatJadwalOperasiOK',
					param : kriteria
                }			
            }
        );   
    return dsvSetPerawatJadwalOperasiOK;
}
function dsGridSetAsistenJadwalOperasiOK(kriteria)
{
	dsvSetAsistenJadwalOperasiOK.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'vDDGridSetAsistenJadwalOperasiOK',
					param : kriteria
                }			
            }
        );   
    return dsvSetAsistenJadwalOperasiOK;
}

function dsGridSetInstrumenJadwalOperasiOK(kriteria)
{
	dsvSetInstrumenJadwalOperasiOK.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'vDDGridSetAsistenJadwalOperasiOK',
					param : kriteria
                }			
            }
        );   
    return dsvSetInstrumenJadwalOperasiOK;
}

function dsGridSetSirkulasiJadwalOperasiOK(kriteria)
{
	dsvSetSirkulasiJadwalOperasiOK.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'vDDGridSetAsistenJadwalOperasiOK',
					param : kriteria
                }			
            }
        );   
    return dsvSetSirkulasiJadwalOperasiOK;
}
function viComboDokter_viJdwlOperasi()
{
	var Field =['kd_dokter','nama'];
    dsvComboDokterJadwalOperasiOK = new WebApp.DataStore({fields: Field});
	dsComboDokterJadwalOperasiOK();
  var cbo_viComboDokter_viJdwlOperasi = new Ext.form.ComboBox
    (
    
        {
            id:"cbo_viComboDokter_viJdwlOperasi",
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'Pilih Dokter..',
            fieldLabel: "Dokter",  
			x: 400,
			y: 70,			
            width:200,
            store:dsvComboDokterJadwalOperasiOK,
            valueField: 'kd_dokter',
            displayField: 'nama',
            listeners:  
            {
                'render': function (c) {
                    c.getEl().on('keypress', function (e) {
                        if (e.getKey() == 13 ) //atau Ext.EventObject.ENTER
                            Ext.getCmp('txtJam_viJdwlOperasi').focus();
                    }, c);
                }
            }
        }
    );
    return cbo_viComboDokter_viJdwlOperasi;
};
/*function tabOptionalDokterPerawatJadwalOk()
{
	var tabs = new Ext.TabPanel({
		width: 600,
		height: 225,
		x: 620,
		y: 30,
		activeTab: 0,
		items: [GetTabSetAsistenJadwalOK()]//,GetTabSetAsistenJadwalOK(),GetTabSetInstrumenJadwalOK(),GetTabSetSirkulasiJadwalOK()]
	});
	return tabs;
}*/

function GetTabSetPerawatJadwalOK()
{
	var pnlTRSetPerawat_jadwaloperasiOK = new Ext.Panel({
        title: 'Set Perawat',
        id: 'tabSetPerawat_jadwaloperasiOK',
        fileUpload: true,
        region: 'north',
        layout: 'column',
        height: '100%',
        anchor: '100%',
        width: '100%',
        border: false,
        items: [gridDataViewEditSetPerawat_JadwalOperasi_Ok()],
		tbar :[
				{
					xtype: 'label',
					text: 'Cari Perawat: ' ,
					
				},
				{xtype: 'tbspacer',height: 3, width:5},
				 {
					x: 40,
					y: 40,
					xtype: 'textfield',
					//fieldLabel: 'No. Medrec',
					name: 'txtcariperawat',
					id: 'txtcariperawat',
					width: 150,
					listeners:
					{
						'specialkey': function ()
                            {
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                {
                                    dsGridSetPerawatJadwalOperasiOK(Ext.getCmp('txtcariperawat').getValue());
                                }
                            }
					}
				}
			]
    });

    return pnlTRSetPerawat_jadwaloperasiOK;
}

function GetTabSetAsistenJadwalOK()
{
	pnlTRSetAsisten_jadwaloperasiOK = new Ext.Window({
	title 		: 'Set Asisten',
	id 			: 'tabSetAsisten_jadwaloperasiOK',
	// fileUpload 	: true,
	// region 		: 'north',
	layout 		: 'column',
	height 		: '100%',
	// anchor 		: '100%',
	width 		: 750,
	border 		: false,
	resizable 	: false,
	plain 		: true,
	closeAction : 'destroy',
	modal 		: true,
	items: [
		gridDataViewEditSetAsisten_JadwalOperasi_Ok()
	],
	tbar :[
				{
					xtype: 'label',
					text: 'Cari Asisten: ' ,
					
				},
				{xtype: 'tbspacer',height: 3, width:5},
				 {
					x: 40,
					y: 40,
					xtype: 'textfield',
					//fieldLabel: 'No. Medrec',
					name: 'txtcariasisten',
					id: 'txtcariasisten',
					width: 150,
					listeners:
					{
						'specialkey': function ()
                            {
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                {
                                    dsGridSetAsistenJadwalOperasiOK(Ext.getCmp('txtcariasisten').getValue());
                                }
                            }
					}
				}
			]
    });

    pnlTRSetAsisten_jadwaloperasiOK.show();
}

function GetTabSetInstrumenJadwalOK()
{
	var pnlTRSetInstrumen_jadwaloperasiOK = new Ext.Panel({
        title: 'Set Instrumen',
        id: 'tabSetInstrumen_jadwaloperasiOK',
        fileUpload: true,
        region: 'north',
        layout: 'column',
        height: '100%',
        anchor: '100%',
        width: '100%',
        border: false,
        items: [gridDataViewEditSetInstrumen_JadwalOperasi_Ok()],
		tbar :[
				{
					xtype: 'label',
					text: 'Cari Instrumen: ' ,
					
				},
				{xtype: 'tbspacer',height: 3, width:5},
				 {
					x: 40,
					y: 40,
					xtype: 'textfield',
					//fieldLabel: 'No. Medrec',
					name: 'txtcariinstrumen',
					id: 'txtcariinstrumen',
					width: 150,
					listeners:
					{
						'specialkey': function ()
                            {
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                {
                                    dsGridSetInstrumenJadwalOperasiOK(Ext.getCmp('txtcariinstrumen').getValue());
                                }
                            }
					}
				}
			]
    });

    return pnlTRSetInstrumen_jadwaloperasiOK;
}

function GetTabSetSirkulasiJadwalOK()
{
	var pnlTRSetSirkulasi_jadwaloperasiOK = new Ext.Panel({
        title: 'Set Sirkulasi',
        id: 'tabSetSirkulasi_jadwaloperasiOK',
        fileUpload: true,
        region: 'north',
        layout: 'column',
        height: '100%',
        anchor: '100%',
        width: '100%',
        border: false,
        items: [gridDataViewEditSetSirkulasi_JadwalOperasi_Ok()],
		tbar :[
				{
					xtype: 'label',
					text: 'Cari Sirkulasi: ' ,
					
				},
				{xtype: 'tbspacer',height: 3, width:5},
				 {
					x: 40,
					y: 40,
					xtype: 'textfield',
					//fieldLabel: 'No. Medrec',
					name: 'txtcarisirkulasi',
					id: 'txtcarisirkulasi',
					width: 150,
					listeners:
					{
						'specialkey': function ()
                            {
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                {
                                    dsGridSetSirkulasiJadwalOperasiOK(Ext.getCmp('txtcarisirkulasi').getValue());
                                }
                            }
					}
				}
			]
    });

    return pnlTRSetSirkulasi_jadwaloperasiOK;
}

function gridDataViewEditSetPerawat_JadwalOperasi_Ok(){
	
    var FieldGrdSetPerawat_JadwalOperasi_Ok = ['kd_perawat','nama_perawat'];
    dsvSetPerawatJadwalOperasiOK= new WebApp.DataStore({
        fields: FieldGrdSetPerawat_JadwalOperasi_Ok
    });
	dsGridSetPerawatJadwalOperasiOK();
 	var fields = [
		{name: 'kd_perawat', mapping : 'kd_perawat'},
		{name: 'nama_perawat', mapping : 'nama_perawat'}
	];
	var cols = [
		new Ext.grid.RowNumberer(),
		{header: "Nama perawat", width: 50, sortable: true, dataIndex: 'nama_perawat'},
		{
				dataIndex: 'jenis',
				header: 'Jenis',
				width: 50,
				align:'center',
				editor: new Ext.form.ComboBox ( {
					id				: 'gridcbojenis_JadwalOK',
					typeAhead		: true,
					triggerAction	: 'all',
					lazyRender		: true,
					mode			: 'local',
					selectOnFocus	: true,
					forceSelection	: true,
					width			: 50,
					anchor			: '95%',
					value			: 1,
					store			: new Ext.data.ArrayStore({
						id		: 0,
						fields	:['Id','displayText'],
						data	: [[0, 'Asisten'],[1, 'Sirkulasi'],[2, 'Instrumen']]
					}),
					valueField	: 'displayText',
					displayField: 'displayText',
					value		: '',
					listeners	: {
						select	: function(a,b,c){
							/* if(b.data.Id == 1) {
								Ext.getCmp('btnRacikan_viApotekResepRWJ').enable();
								formulaRacikanResepRWJ();
							} else{
								var line	= AptResepRWJ.form.Grid.a.getSelectionModel().selection.cell[0];
								Ext.getCmp('btnRacikan_viApotekResepRWJ').disable();
								AptResepRWJ.form.Grid.a.startEditing(line, 9);	
							} */
						}
					}
				})
		}
	];
	
	 	firstGrid = new Ext.grid.GridPanel({
            ddGroup          : 'secondGridDDGroup',
            store            : dsvSetPerawatJadwalOperasiOK,
            autoScroll       : true,
            columnLines      : true,
            border           : true,
            enableDragDrop   : true,
            height           : 150,
            stripeRows       : true,
            trackMouseOver   : true,
            //title            : 'Daftar No Reg Barang',
            anchor           : '100% 100%',
            plugins          : [new Ext.ux.grid.FilterRow()],
            colModel         : new Ext.grid.ColumnModel
                            (
                                    [
                                            new Ext.grid.RowNumberer(),
                                            {
                                                    id: 'colNamaDok_setPerawatJwdOpOk',
                                                    header: 'Nama perawat',
                                                    dataIndex: 'nama_perawat',
                                                    sortable: true,
                                                    width: 20
                                            }
                                    ]
                                ),
   			  listeners : {
                    afterrender : function(comp) {
                    var firstGridDropTargetEl = firstGrid.getView().scroller.dom;
                    var firstGridDropTarget = new Ext.dd.DropTarget(firstGridDropTargetEl, {
                            ddGroup    : 'firstGridDDGroup',
                            notifyDrop : function(ddSource, e, data){
                                    var records =  ddSource.dragData.selections;
                                    Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
                                    firstGrid.store.add(records);
                                    firstGrid.store.sort('kd_perawat', 'ASC');
                                    return true
                            }
                    });
                    }
                },
             viewConfig: 
                    {
                            forceFit: true
                    }
        });

        secondGridStore = new Ext.data.JsonStore({
            fields : fields,
            root   : 'records'
        });

        // create the destination Grid
        var secondGrid = new Ext.grid.EditorGridPanel({
                    ddGroup          : 'firstGridDDGroup',
                    store            : secondGridStore,
                    columns          : cols,
                    enableDragDrop   : true,
                    height           : 150,
                    stripeRows       : true,
                    autoExpandColumn : 'nama',
                   // title            : 'Daftar Pilihan No Reg Barang',
                    listeners : {
                    afterrender : function(comp) {
                    var secondGridDropTargetEl = secondGrid.getView().scroller.dom;
                    var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
                            ddGroup    : 'secondGridDDGroup',
                            notifyDrop : function(ddSource, e, data){
                                    var records =  ddSource.dragData.selections;
                                    Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
                                    secondGrid.store.add(records);
                                    secondGrid.store.sort('kd_perawat', 'ASC');
                                    return true
                            }
                    });
                    }
                },
                viewConfig: 
                    {
                            forceFit: true
                    }
        });
		
		var FrmTabs_jadwalOpOk = new Ext.Panel
        (
		{
		    id: 'formTab',
		    region: 'center',
		    layout: 'column',
            height       : 250,
			
			//title:  'Set Perawat',
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding: 0px 0px 0px 0px',
		    border: false,
//		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
			width: '100%',
		    anchor: '99%',
		    //iconCls: icons_viInformasiUnitdokter,
		    items: 
			[
				{
					columnWidth: .50,
					layout: 'form',
					border: false,
					autoScroll: true,
					bodyStyle: 'padding: 10px 10px 10px 10px',
					items:
					[firstGrid
						
					]
				},
				{
					columnWidth: .50,
					layout: 'form',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					anchor: '100% 100%',
					items:
					[secondGrid
						
					]
				},
			]
		});
		return FrmTabs_jadwalOpOk;
}
function gridDataViewEditSetAsisten_JadwalOperasi_Ok(){
	
    var FieldGrdSetPerawat_JadwalOperasi_Ok = ['kd_asisten','nama'];
    dsvSetAsistenJadwalOperasiOK= new WebApp.DataStore({
        fields: FieldGrdSetPerawat_JadwalOperasi_Ok
    });
	var FieldJenisAsisten = ['kd_jenis_asisten','asisten'];
	var dsJenisAsisten_viviJdwlOperasi = new WebApp.DataStore({ fields: FieldJenisAsisten });
    dsJenisAsisten_viviJdwlOperasi.load
	(
            {
                params:
                    {
                        Skip: 0,
                        Take: 1000,

                        Sort: 'nama',
                        Sortdir: 'ASC',
                        target: 'vAsistenOperasi',
                        //param: "kd_unit = '41'"
                    }
            }
	);
	dsGridSetAsistenJadwalOperasiOK();
	var cols = [
		new Ext.grid.RowNumberer(),
		{header: "Nama asisten", width: 50, sortable: true, dataIndex: 'nama'},
		{
				dataIndex 	: 'jenis',
				header 		: 'Jenis',
				width 		: 50,
				align 		:'center',
				editor 		: new Ext.form.ComboBox ( {
					id				: 'cbojenisAsisten_JadwalOK',
					typeAhead		: true,
					triggerAction	: 'all',
					lazyRender		: true,
					mode			: 'local',
					width			: 50,
					store: dsJenisAsisten_viviJdwlOperasi,
					valueField: 'asisten',
					displayField: 'asisten',
					listeners	: {
						select	: function(a,b,c){
						}
					}
				})
		}
	];
	
	 	var firstGrid2 = new Ext.grid.GridPanel({
            ddGroup          : 'secondGridDDGroup2',
            store            : dsvSetAsistenJadwalOperasiOK,
			// columns          : cols,
            autoScroll       : true,
            columnLines      : true,
            border           : true,
            enableDragDrop   : true,
            height           : 150,
            stripeRows       : true,
            trackMouseOver   : true,
            //title            : 'Daftar No Reg Barang',
            anchor           : '100% 100%',
            plugins          : [new Ext.ux.grid.FilterRow()],
            colModel         : new Ext.grid.ColumnModel
                            (
                                    [
                                            new Ext.grid.RowNumberer(),
                                            {
                                                    id: 'colNamaDok_setAsistenJwdOpOk',
                                                    header: 'Nama asisten',
                                                    dataIndex: 'nama',
                                                    sortable: true,
                                                    width: 20
                                            }
                                    ]
                                ),
   			  listeners : {
                    afterrender : function(comp) {
						var firstGridDropTargetEl2 = firstGrid2.getView().scroller.dom;
						var firstGridDropTarget2   = new Ext.dd.DropTarget(firstGridDropTargetEl2, {
								ddGroup    : 'firstGridDDGroup2',
								notifyDrop : function(ddSource, e, data){
										console.log(ddSource);
										var records2 =  ddSource.dragData.selections;
										Ext.each(records2, ddSource.grid.store.remove, ddSource.grid.store);
										firstGrid2.store.add(records2);
										firstGrid2.store.sort('kd_asisten', 'ASC');
										return true
								}
						});
                    },
                },
             viewConfig: 
                    {
                            forceFit: true
                    }
        });


        // create the destination Grid
        var secondGrid2 = new Ext.grid.GridPanel({
                    ddGroup          : 'firstGridDDGroup2',
                    store            : secondGridStore2,
					columns          : cols,
					autoScroll       : true,
					columnLines      : true,
					border           : true,
					enableDragDrop   : true,
					height           : 150,
					stripeRows       : true,
					trackMouseOver   : true,
					plugins          : [new Ext.ux.grid.RowEditor()],
                    // autoExpandColumn : 'nama',
                   // title            : 'Daftar Pilihan No Reg Barang',
                    listeners : {
                    afterrender : function(comp) {
						var secondGridDropTargetEl2 = secondGrid2.getView().scroller.dom;
						var secondGridDropTarget2   = new Ext.dd.DropTarget(secondGridDropTargetEl2, {
								ddGroup    : 'secondGridDDGroup2',
								notifyDrop : function(ddSource, e, data){
										var records2 =  ddSource.dragData.selections;
										Ext.each(records2, ddSource.grid.store.remove, ddSource.grid.store);
										secondGrid2.store.add(records2);
										secondGrid2.store.sort('kd_asisten', 'ASC');
										return true
								}
						});
	                    },
                	},
                viewConfig: 
                    {
                            forceFit: true
                    }
        });
		
		var FrmTabsAsisten_jadwalOpOk = new Ext.Panel
        (
		{
		    id 		 	: 'formTab2',
		    region 	 	: 'center',
		    layout 	 	: 'column',
            height      : '100%',
			
			//title 	:  'Set Asisten',
		    itemCls 	: 'blacklabel',
		    bodyStyle 	: 'padding: 0px 0px 0px 0px',
		    border 		: false,
//		    bodyStyle 	: 'background:#FFFFFF;',
		    shadhow 	: true,
		    margins 	: '0 5 5 0',
			width 		: '100%',
		    // anchor 		: '99%',
		    //iconCls: icons_viInformasiUnitdokter,
		    items: 
			[
				{
					columnWidth: .50,
					layout: 'form',
					border: false,
					autoScroll: true,
					bodyStyle: 'padding: 10px 10px 10px 10px',
					items:
					[firstGrid2
						
					]
				},
				{
					columnWidth: .50,
					layout: 'form',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					anchor: '100% 100%',
					items:
					[secondGrid2
						
					]
				},
			]
		});
		return FrmTabsAsisten_jadwalOpOk;
}

function gridDataViewEditSetInstrumen_JadwalOperasi_Ok(){
	
    var FieldGrdSetPerawat_JadwalOperasi_Ok = ['kd_asisten','nama'];
    dsvSetInstrumenJadwalOperasiOK= new WebApp.DataStore({
        fields: FieldGrdSetPerawat_JadwalOperasi_Ok
    });
	dsGridSetInstrumenJadwalOperasiOK();
 	var fields = [
		{name: 'kd_asisten', mapping : 'kd_asisten'},
		{name: 'nama', mapping : 'nama'}
	];
	var cols = [
		new Ext.grid.RowNumberer(),
		{header: "Nama ", width: 20, sortable: true, dataIndex: 'nama'}
	];
	
	 	firstGrid3 = new Ext.grid.GridPanel({
            ddGroup          : 'secondGridDDGroup3',
            store            : dsvSetInstrumenJadwalOperasiOK,
            autoScroll       : true,
            columnLines      : true,
            border           : true,
            enableDragDrop   : true,
            height           : 150,
            stripeRows       : true,
            trackMouseOver   : true,
            //title            : 'Daftar No Reg Barang',
            anchor           : '100% 100%',
            plugins          : [new Ext.ux.grid.FilterRow()],
            colModel         : new Ext.grid.ColumnModel
                            (
                                    [
                                            new Ext.grid.RowNumberer(),
                                            {
                                                    id: 'colNamaDok_setInstrumenJwdOpOk',
                                                    header: 'Nama ',
                                                    dataIndex: 'nama',
                                                    sortable: true,
                                                    width: 20
                                            }
                                    ]
                                ),
   			  listeners : {
                    afterrender : function(comp) {
                    var firstGridDropTargetEl3 = firstGrid3.getView().scroller.dom;
                    var firstGridDropTarget3 = new Ext.dd.DropTarget(firstGridDropTargetEl3, {
                            ddGroup    : 'firstGridDDGroup3',
                            notifyDrop : function(ddSource, e, data){
                                    var records3 =  ddSource.dragData.selections;
                                    Ext.each(records3, ddSource.grid.store.remove, ddSource.grid.store);
                                    firstGrid3.store.add(records3);
                                    firstGrid3.store.sort('kd_asisten', 'ASC');
                                    return true
                            }
                    });
                    }
                },
             viewConfig: 
                    {
                            forceFit: true
                    }
        });

        secondGridStore3 = new Ext.data.JsonStore({
            fields : fields,
            root   : 'records'
        });

        // create the destination Grid
        var secondGrid3 = new Ext.grid.GridPanel({
                    ddGroup          : 'firstGridDDGroup3',
                    store            : secondGridStore3,
                    columns          : cols,
                    enableDragDrop   : true,
                    height           : 150,
                    stripeRows       : true,
                    autoExpandColumn : 'nama',
                   // title            : 'Daftar Pilihan No Reg Barang',
                    listeners : {
                    afterrender : function(comp) {
                    var secondGridDropTargetEl3 = secondGrid3.getView().scroller.dom;
                    var secondGridDropTarget3 = new Ext.dd.DropTarget(secondGridDropTargetEl3, {
                            ddGroup    : 'secondGridDDGroup3',
                            notifyDrop : function(ddSource, e, data){
                                    var records3 =  ddSource.dragData.selections;
                                    Ext.each(records3, ddSource.grid.store.remove, ddSource.grid.store);
                                    secondGrid3.store.add(records3);
                                    secondGrid3.store.sort('kd_asisten', 'ASC');
                                    return true
                            }
                    });
                    }
                },
                viewConfig: 
                    {
                            forceFit: true
                    }
        });
		
		var FrmTabsInstrumen_jadwalOpOk = new Ext.Panel
        (
		{
		    id: 'formTab3',
		    region: 'center',
		    layout: 'column',
            height       : 250,
			
			//title:  'Set Asisten',
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding: 0px 0px 0px 0px',
		    border: false,
//		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
			width: '100%',
		    anchor: '99%',
		    //iconCls: icons_viInformasiUnitdokter,
		    items: 
			[
				{
					columnWidth: .50,
					layout: 'form',
					border: false,
					autoScroll: true,
					bodyStyle: 'padding: 10px 10px 10px 10px',
					items:
					[firstGrid3
						
					]
				},
				{
					columnWidth: .50,
					layout: 'form',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					anchor: '100% 100%',
					items:
					[secondGrid3
						
					]
				},
			]
		});
		return FrmTabsInstrumen_jadwalOpOk;
}

function gridDataViewEditSetSirkulasi_JadwalOperasi_Ok(){
	
    var FieldGrdSetPerawat_JadwalOperasi_Ok = ['kd_asisten','nama'];
    dsvSetSirkulasiJadwalOperasiOK= new WebApp.DataStore({
        fields: FieldGrdSetPerawat_JadwalOperasi_Ok
    });
	dsGridSetSirkulasiJadwalOperasiOK();
 	var fields = [
		{name: 'kd_asisten', mapping : 'kd_asisten'},
		{name: 'nama', mapping : 'nama'}
	];
	var cols = [
		new Ext.grid.RowNumberer(),
		{header: "Nama ", width: 20, sortable: true, dataIndex: 'nama'}
	];
	
	 	firstGrid4 = new Ext.grid.GridPanel({
            ddGroup          : 'secondGridDDGroup4',
            store            : dsvSetSirkulasiJadwalOperasiOK,
            autoScroll       : true,
            columnLines      : true,
            border           : true,
            enableDragDrop   : true,
            height           : 150,
            stripeRows       : true,
            trackMouseOver   : true,
            //title            : 'Daftar No Reg Barang',
            anchor           : '100% 100%',
            plugins          : [new Ext.ux.grid.FilterRow()],
            colModel         : new Ext.grid.ColumnModel
                            (
                                    [
                                            new Ext.grid.RowNumberer(),
                                            {
                                                    id: 'colNamaDok_setInstrumenJwdOpOk',
                                                    header: 'Nama ',
                                                    dataIndex: 'nama',
                                                    sortable: true,
                                                    width: 20
                                            }
                                    ]
                                ),
   			  listeners : {
                    afterrender : function(comp) {
                    var firstGridDropTargetEl4 = firstGrid4.getView().scroller.dom;
                    var firstGridDropTarget4 = new Ext.dd.DropTarget(firstGridDropTargetEl4, {
                            ddGroup    : 'firstGridDDGroup4',
                            notifyDrop : function(ddSource, e, data){
                                    var records4 =  ddSource.dragData.selections;
                                    Ext.each(records4, ddSource.grid.store.remove, ddSource.grid.store);
                                    firstGrid4.store.add(records4);
                                    firstGrid4.store.sort('kd_asisten', 'ASC');
                                    return true
                            }
                    });
                    }
                },
             viewConfig: 
                    {
                            forceFit: true
                    }
        });

        secondGridStore4 = new Ext.data.JsonStore({
            fields : fields,
            root   : 'records'
        });

        // create the destination Grid
        var secondGrid4 = new Ext.grid.GridPanel({
                    ddGroup          : 'firstGridDDGroup4',
                    store            : secondGridStore4,
                    columns          : cols,
                    enableDragDrop   : true,
                    height           : 150,
                    stripeRows       : true,
                    autoExpandColumn : 'nama',
                   // title            : 'Daftar Pilihan No Reg Barang',
                    listeners : {
                    afterrender : function(comp) {
                    var secondGridDropTargetEl4 = secondGrid4.getView().scroller.dom;
                    var secondGridDropTarget4 = new Ext.dd.DropTarget(secondGridDropTargetEl4, {
                            ddGroup    : 'secondGridDDGroup4',
                            notifyDrop : function(ddSource, e, data){
                                    var records4 =  ddSource.dragData.selections;
                                    Ext.each(records4, ddSource.grid.store.remove, ddSource.grid.store);
                                    secondGrid4.store.add(records4);
                                    secondGrid4.store.sort('kd_asisten', 'ASC');
                                    return true
                            }
                    });
                    }
                },
                viewConfig: 
                    {
                            forceFit: true
                    }
        });
		
		var FrmTabsSirkulasi_jadwalOpOk = new Ext.Panel
        (
		{
		    id: 'formTab4',
		    region: 'center',
		    layout: 'column',
            height       : 250,
			
			//title:  'Set Asisten',
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding: 0px 0px 0px 0px',
		    border: false,
//		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
			width: '100%',
		    anchor: '99%',
		    //iconCls: icons_viInformasiUnitdokter,
		    items: 
			[
				{
					columnWidth: .50,
					layout: 'form',
					border: false,
					autoScroll: true,
					bodyStyle: 'padding: 10px 10px 10px 10px',
					items:
					[firstGrid4
						
					]
				},
				{
					columnWidth: .50,
					layout: 'form',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: false,
					anchor: '100% 100%',
					items:
					[secondGrid4
						
					]
				},
			]
		});
		return FrmTabsSirkulasi_jadwalOpOk;
}

function dsComboTindakanOperasiJadwalOperasiOK(params, kd_tindakan = null)
{
	dsvComboTindakanOperasiJadwalOperasiOK.load({
		params:
		{
			Skip: 0,
			Take: 1000,
			Sort: '',
			Sortdir: 'DESC',
			target: 'vComboTindakanOperasiJadwalOperasiOK',
			param: ''
		}
	});
	/*Ext.Ajax.request({
		url: baseURL + "index.php/kamar_operasi/functionKamarOperasi/get_tindakan_",
		params:params,
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cbo_viComboJenisTindakan_viJdwlOperasi.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsvComboTindakanOperasiJadwalOperasiOK.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsvComboTindakanOperasiJadwalOperasiOK.add(recs);
				console.log(o);
			}

			if (kd_tindakan != null) {
				Ext.getCmp('cbo_viComboJenisTindakan_viJdwlOperasi').setValue(kd_tindakan);
			}
		}
	});*/
	
	/* dsvComboTindakanOperasiJadwalOperasiOK.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
					Sortdir: 'ASC', 
                    target: 'vComboTindakanOperasiJadwalOperasiOK',
					param:kriteria
                }			
            }
        );   
    return dsvComboTindakanOperasiJadwalOperasiOK; */
}
function viComboJenisTindakan_viJdwlOperasi()
{
	var Field                              =['kd_tindakan','tindakan'];
	dsvComboTindakanOperasiJadwalOperasiOK = new WebApp.DataStore({fields: Field});
	dsComboTindakanOperasiJadwalOperasiOK();
	cbo_viComboJenisTindakan_viJdwlOperasi = new Ext.form.ComboBox
	(
    
        {
			x: 480,
			y: 160,	
			id:"cbo_viComboJenisTindakan_viJdwlOperasi",
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih Jenis Tindakan..',
			fieldLabel: "Jenis Tindakan",       
			width	: 170,
			store:dsvComboTindakanOperasiJadwalOperasiOK,
			valueField: 'kd_tindakan',
			displayField: 'tindakan',
			listeners:{
				select : function(a, b){
					/*Ext.Ajax.request({
						url: baseURL + "index.php/kamar_operasi/Kamar_operasi/get_combo",
						params: {
							kd_tindakan : b.data.kd_tindakan
						},
						failure: function (o){
						},
						success: function (o) {
							var cst = Ext.decode(o.responseText);
							if (cst.status === true) {
								Ext.getCmp('cboSpesialisasi_SetupTindakanasi_OK').setValue(cst.data[0].kd_spesial);
								Ext.getCmp('cboSubSpesialisasi_SetupTindakanasi_OK').setValue(cst.data[0].kd_sub_spc);
								Ext.getCmp('cboJenisOperasi_SetupTindakanasi_OK').setValue(cst.data[0].kd_jenis_op);
							}
						}
					});*/
				},
				'render': function (c) {
				    /*c.getEl().on('keypress', function (e) {
				        if (e.getKey() == 13 ) //atau Ext.EventObject.ENTER
				            // Ext.getCmp('txtKeterangan_viJdwlOperasi').focus();
				    }, c);*/
				}
			}
        }
    );
    return cbo_viComboJenisTindakan_viJdwlOperasi;
};

/* function getItemGridTransaksi_viJdwlOperasi(lebar) 
{
    var items =
	{
		title: '', 
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		border:true,
		width: lebar-80,
		height:200, 
	    items:
		[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:
				[
					//gridDataViewEdit_viJdwlOperasi(),
					
				]	
			}
			//-------------- ## --------------
		]
	};
    return items;
}; */

/* function gridDataViewEdit_viJdwlOperasi()
{
    
    var FieldGrdKasir_viJdwlOperasi = 
	[
	];
	
    dsDataGrdJab_viJdwlOperasi= new WebApp.DataStore
	({
        fields: FieldGrdKasir_viJdwlOperasi
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJab_viJdwlOperasi,
        height: 150,
		selModel: new Ext.grid.RowSelectionModel
        (
	        {
	            singleSelect: true,
	            listeners:
	            {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
	        }
        ),
        columns: 
		[
			{
				dataIndex: '',
				header: 'Status Operasi',
				sortable: true,
				width: 120
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'No. MedRec',
				sortable: true,
				width: 100
			},
			//-------------- ## --------------			
			{			
				dataIndex: '',
				header: 'Nama Pasien',
				sortable: true,
				width: 150
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Alamat',
				sortable: true,
				width: 150,
				renderer: function(v, params, record) 
				{
					
				}			
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Dokter',
				sortable: true,
				width: 120,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Tindakan',
				sortable: true,
				width: 150,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Durasi',
				sortable: true,
				width: 70,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Selesai',
				sortable: true,
				width: 80,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Keterangan',
				sortable: true,
				width: 200,
				renderer: function(v, params, record) 
				{
					
				}	
			}
        ]
    }    
    return items;
} */


function DataBatalJadwal_viJdwlOperasi(mBol)
{
	if (ValidasiBatalviJdwlOperasi(nmHeaderSimpanData,false) == 1 )
	{ 
		Ext.Msg.show
                (
                        {
                            title: 'Hapus Data',
                            msg: 'Apakah data ini akan dihapus ?',
                            buttons: Ext.MessageBox.YESNO,
                            fn: function (btn)
                            {
                                if (btn == 'yes')
                                {
                                    Ext.Ajax.request
									 (
										{
											url: baseURL + "index.php/kamar_operasi/functionKamarOperasi/bataljadwaldetailjadwaloperasi",
											params: getParamDetailBatalJadwalOK(),
											failure: function(o)
											{	//var_tampung_lab_order=false;
												loadMask.hide();
												ShowPesanErrorviJdwlOperasi('Error! Gagal menyimpan data ini. Hubungi Admin', 'Error');
												//ViewGridBawahviJdwlOperasi(TrviJdwlOperasi.form.ComboBox.notransaksi.getValue());
											},
											success: function(o)
											{  	//var_tampung_lab_order=false;
												var cst = Ext.decode(o.responseText);
												if (cst.success === true)
												{
													loadMask.hide();
													ShowPesanInfoviJdwlOperasi("Berhasil menghapus data ini","Information");
													ViewGridBawahLookupviJdwlOperasi();
													CurrentBatalviJdwlOperasi.data.data=undefined;
												}
												else
												{
													loadMask.hide();
													ShowPesanErrorviJdwlOperasi('Gagal menyimpan data ini. Hubungi Admin', 'Error');
												};
											}
										}
									) 
                                }
                            },
                            icon: Ext.MessageBox.QUESTION
                        }
                ); 
	}
	else
	{
		if(mBol === true)
		{
			loadMask.hide();
			return false;
		};
	};
};
function Datasave_viJdwlOperasi(mBol)
{
	if (ValidasiEntryviJdwlOperasi(nmHeaderSimpanData,false) == 1 )
	{ 
		Ext.Ajax.request
		 (
			{
				url: baseURL + "index.php/kamar_operasi/functionKamarOperasi/savedetailjadwaloperasi",
				params: getParamDetailTransaksiOK(),
				failure: function(o)
				{	//var_tampung_lab_order=false;
					loadMask.hide();
					tglorder='';
					ShowPesanErrorviJdwlOperasi('Error! Gagal menyimpan data ini. Hubungi Admin', 'Error');
					//ViewGridBawahviJdwlOperasi(TrviJdwlOperasi.form.ComboBox.notransaksi.getValue());
				},
				success: function(o)
				{  	//var_tampung_lab_order=false;
					var cst = Ext.decode(o.responseText);
					Ext.getCmp('cboSpesialisasi_SetupTindakanasi_OK').setValue();
					Ext.getCmp('cboJenisOperasi_SetupTindakanasi_OK').setValue();
					Ext.getCmp('cboSubSpesialisasi_SetupTindakanasi_OK').setValue();
					Ext.getCmp('cbo_viComboJenisTindakan_viJdwlOperasi').setValue();
					if (cst.success === true)
					{
						loadMask.hide();
						// load_data_pasienorder();
						viewGridOrderAll_RASEPRWI();
						//ShowPesanInfoviJdwlOperasi("Berhasil menyimpan data ini","Information");
						if (cst.status_kamar === 1)
						{
							tglorder='';
							ShowPesanErrorviJdwlOperasi('Maaf !! waktu yang anda tentukan telah ada yang mengisi. ', 'Error');
							// load_data_pasienorder();
							viewGridOrderAll_RASEPRWI();
						}
						else
						{
							ShowPesanInfoviJdwlOperasi("Berhasil menyimpan data ini","Information");
							var tmp_tgl = Ext.getCmp('TglOperasi_viJdwlOperasi').getValue();
							ViewGridBawahLookupviJdwlOperasi(" AND oj.tgl_op = '"+tmp_tgl.format("Y-m-d")+"'") ;
							tglorder='';
							// load_data_pasienorder();
							viewGridOrderAll_RASEPRWI();
							bersihkanSemuaSetelahSimpanOK();
						}

					}else if(cst.jadwal=== true && cst.success===false){
						loadMask.hide();
						ShowPesanWarningviJdwlOperasi('Pasien sudah dijadwalkan beroperasi', 'Masih terdaftar untuk operasi');
						// load_data_pasienorder();
						// viewGridOrderAll_RASEPRWI();
					}else if(cst.ok_kamar=== true && cst.success===false){
						loadMask.hide();
						ShowPesanWarningviJdwlOperasi('Sudah ada penjadwalan di ruangan '+cst.kamar+' pada tanggal '+cst.tgl_op+' jam '+cst.jam_op, 'Peringatan');
						// load_data_pasienorder();
						// viewGridOrderAll_RASEPRWI();
					}else
					{
						loadMask.hide();
						ShowPesanErrorviJdwlOperasi('Gagal menyimpan data ini. Hubungi Admin', 'Error');
						// load_data_pasienorder();
						// viewGridOrderAll_RASEPRWI();
					};
				}
			}
		)

	}
	else
	{
		if(mBol === true)
		{
			loadMask.hide();
			return false;
		};
	}; 

};



function getDokterPengirim(no_transaksi,kd_kasir){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/main/functionLAB/getDokterPengirim",
			params:{no_transaksi:no_transaksi,kd_kasir:kd_kasir} ,
			failure: function(o)
			{
				ShowPesanErrorviJdwlOperasi('Hubungi Admin', 'Error');
			},
			success: function(o)
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					Ext.getCmp('txtDokterPengirimL').setValue(cst.nama);
				}
				else
				{
					ShowPesanErrorviJdwlOperasi('Gagal membaca dokter pengirim', 'Error');
				};
			}
		}

	)
}

function bersihkanSemuaSetelahSimpanOK()
{
	tglorder='';
	Ext.getCmp('txtJam_viJdwlOperasi').setValue("");
	Ext.getCmp('txtMenit_viJdwlOperasi').setValue("");
	Ext.getCmp('TglOperasi_viJdwlOperasi').setValue(now);
	Ext.getCmp('cbo_viComboKamar_viJdwlOperasi').setValue("");
	Ext.getCmp('cbo_viComboDokter_viJdwlOperasi').setValue("");
	Ext.getCmp('txtNoMedrec_viJdwlOperasi').setValue(""),
	TrviJdwlOperasi.form.ComboBox.namapasien.setValue(""),
	Ext.getCmp('txtAlamat_viJdwlOperasi').setValue(""),
	Ext.getCmp('txtDurasi_viJdwlOperasi').setValue(""),
	Ext.getCmp('DateTgljadwal_viJdwlOperasi').setValue(now),
	Ext.getCmp('cbo_viComboJenisTindakan_viJdwlOperasi').setValue(""),
	// Ext.getCmp('txtKeterangan_viJdwlOperasi').setValue("");
	/* secondGridStore.removeAll();
	dsGridSetPerawatJadwalOperasiOK(); */
	secondGridStore2.removeAll();
	dsGridSetAsistenJadwalOperasiOK();
	dsGridSetInstrumenJadwalOperasiOK();
	dsGridSetSirkulasiJadwalOperasiOK();
}
function ValidasiEntryviJdwlOperasi(modul,mBolHapus)
{
	var x = 1;
		var tanggalOperasi = new Date(Ext.getCmp('TglOperasi_viJdwlOperasi').getValue()).toDateString();
		var currenttime = new Date(nowSekarang).toDateString();
		var valJam = parseInt(Ext.getCmp('txtJam_viJdwlOperasi').getValue());
		var valMenit = parseInt(Ext.getCmp('txtMenit_viJdwlOperasi').getValue());
		if (valJam > 23)
		{
			loadMask.hide();
			ShowPesanWarningviJdwlOperasi('Jam tidak boleh lebih dari angka 23', 'Warning');
			x = 0;
		}
		else if (valMenit > 59)
		{
			loadMask.hide();
			ShowPesanWarningviJdwlOperasi('Menit tidak boleh lebih dari angka 59', 'Warning');
			x = 0;
		}/* 
		else if (tanggalOperasi < currenttime) //|| (tanggalOperasi<=TanggalSekarang && ))
		{
			
			loadMask.hide();
			ShowPesanWarningviJdwlOperasi('Tanggal operasi tidak boleh kurang dari tanggal hari sekarang', 'Warning');
			x = 0;
		} */
		else if (Ext.getCmp('TglOperasi_viJdwlOperasi').getValue() == '')
		{
		
			loadMask.hide();
			ShowPesanWarningviJdwlOperasi('Tanggal operasi tidak boleh kosong', 'Warning');
			x = 0;
		}
		else if (Ext.getCmp('cbo_viComboKamar_viJdwlOperasi').getValue() == 'Pilih Kamar..' || Ext.getCmp('cbo_viComboKamar_viJdwlOperasi').getValue()  === '')
		{
			
			loadMask.hide();
			ShowPesanWarningviJdwlOperasi('Kamar tidak boleh kosong', 'Warning');
			x = 0;
		}
		else if (Ext.getCmp('txtNoMedrec_viJdwlOperasi').getValue() == '')
		{
			loadMask.hide();
			ShowPesanWarningviJdwlOperasi('Belum ada pasien yang dipilih', 'Warning');
			x = 0;
		}
		else if (Ext.get('cbo_viComboDokter_viJdwlOperasi').getValue() == 'Pilih Dokter..' || Ext.get('cbo_viComboDokter_viJdwlOperasi').getValue() == '')
		{
			loadMask.hide();
			ShowPesanWarningviJdwlOperasi('Dokter tidak boleh kosong', 'Warning');
			x = 0;
		}
		else if (Ext.getCmp('txtJam_viJdwlOperasi').getValue() == '' || Ext.getCmp('txtMenit_viJdwlOperasi').getValue()  === '')
		{
			loadMask.hide();
			ShowPesanWarningviJdwlOperasi('Jam operasi tidak boleh kosong', 'Warning');
			x = 0;
		}
		else if (Ext.get('txtDurasi_viJdwlOperasi').getValue() == '')
		{
			loadMask.hide();
			ShowPesanWarningviJdwlOperasi('Durasi tidak boleh kosong','Warning');
			x = 0;
		}  else if(Ext.get('cbo_viComboJenisTindakan_viJdwlOperasi').getValue() == 'Pilih Jenis Tindakan..' || Ext.get('cbo_viComboJenisTindakan_viJdwlOperasi').getValue() == ''){
			loadMask.hide();
			ShowPesanWarningviJdwlOperasi('Jenis tindakan tidak boleh kosong','Warning');
			x = 0;
		} 
	
	return x;
};

function ValidasiBatalviJdwlOperasi(modul,mBolHapus)
{
	var x = 1;
		if(CurrentBatalviJdwlOperasi.data.data == undefined){
			loadMask.hide();
			ShowPesanWarningviJdwlOperasi('Pilih terlebih dahulu pasien yang akan dibatalkan','Warning');
			x = 0;
		} 
	
	return x;
};
function ShowPesanWarningviJdwlOperasi(str, modul)
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorviJdwlOperasi(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoviJdwlOperasi(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};


//combo jenis kelamin
function mComboJK()
{
    var cboJKviJdwlOperasi = new Ext.form.ComboBox
	(
		{
			id:'cboJKviJdwlOperasi',
			x: 495,
            y: 100,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			tabIndex:3,
			selectOnFocus:true,
			forceSelection: true,
			fieldLabel: 'Jenis Kelamin ',
			editable: false,
			width:95,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Laki - Laki'], [2, 'Perempuan']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			//value:selectSetJKLab,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetJKLab=b.data.valueField ;
				},
			}
		}
	);
	return cboJKviJdwlOperasi;
};

//combo golongan darah
function mComboGDarah()
{
    var cboGDRviJdwlOperasi = new Ext.form.ComboBox
	(
		{
			id:'cboGDRviJdwlOperasi',
			x: 690,
            y: 100,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			tabIndex:3,
			selectOnFocus:true,
			forceSelection: true,
			fieldLabel: 'Gol. Darah ',
			width:50,
			editable: false,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[0, '-'], [1, 'A+'],[2, 'B+'], [3, 'AB+'],[4, 'O+'], [5, 'A-'], [6, 'B-']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			//value:selectSetGDarahLab,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetGDarahLab=b.data.displayText ;
				},

			}
		}
	);
	return cboGDRviJdwlOperasi;
};

//Combo Dokter Lookup
function mComboDOKTER()
{
    var Field = ['KD_DOKTER','NAMA'];

    dsdokter_viviJdwlOperasi = new WebApp.DataStore({ fields: Field });
    dsdokter_viviJdwlOperasi.load
	(
            {
                params:
                    {
                        Skip: 0,
                        Take: 1000,

                        Sort: 'nama',
                        Sortdir: 'ASC',
                        target: 'ViewDokterPenunjang',
                        param: "kd_unit = '41'"
                    }
            }
	);

    var cboDOKTER_viviJdwlOperasi = new Ext.form.ComboBox
	(
		{
			id: 'cboDOKTER_viviJdwlOperasi',
			x: 130,
			y: 130,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			emptyText: '',
			fieldLabel:  ' ',
			align: 'Right',
			width: 230,
			store: dsdokter_viviJdwlOperasi,
			valueField: 'KD_DOKTER',
			displayField: 'NAMA',
			editable: true,
			listeners:
			{
				'select': function(a, b, c)
				{
					if(var_tampung_lab_order===true)
					{
					var_tampung_lab_order=true;
					Ext.getCmp('btnLookupItemPemeriksaan').disable();
					}else{
					var_tampung_lab_order=false;
					Ext.getCmp('btnLookupItemPemeriksaan').enable();
					}
				}
			}
		}
	);

    return cboDOKTER_viviJdwlOperasi;
};





function mComboorder()
{


 var Field = ['no_transaksi','kd_kasir','tgl_transaksi','kd_spesial','kamar','tgl_lahir','kd_pasien','nama','nama_unit_asli',
				'alamat','kd_dokter','kd_unit','nama_unit','urut_masuk','dokter','Jenis_kelamin',
				'gol_darah','kd_customer','customer',
				'no_kamar','kd_spesial','tgl','kd_tarif','kelpasien','display'];
    dspasienorder_mng = new WebApp.DataStore({ fields: Field });
	total_pasien_order_mng();
	load_data_pasienorder();
	cbopasienorder_mng= new Ext.form.ComboBox
	(
		{
			id: 'cbopasienorder_mng',
			typeAhead		: true,
			triggerAction	: 'all',
			lazyRender		: true,
			mode			: 'local',
			emptyText: '',
			fieldLabel:  ' ',
			align: 'Right',
			width: 230,
			store: dspasienorder_mng,
			valueField: 'kd_pasien',
			displayField: 'display',
			//hideTrigger		: true,
			listeners:
			{
				select	: function(a,b,c){
								    var_tampung_lab_order=true;
									ViewGridBawahviJdwlOperasi(b.data.no_transaksi,b.data);
									Ext.getCmp('txtNoMedrec_viJdwlOperasi').disable();
									Ext.getCmp('txtNoMedrec_viJdwlOperasi').setValue(b.data.kd_pasien);
									TrviJdwlOperasi.form.ComboBox.pasien.disable();
									TrviJdwlOperasi.form.ComboBox.pasien.setValue(b.data.nama);
									TrviJdwlOperasi.form.ComboBox.notransaksi.setValue(b.data.no_transaksi);
									TrviJdwlOperasi.form.ComboBox.notransaksi.setReadOnly(true);


									Ext.getCmp('btnLookupItemPemeriksaan').disable();
									Ext.getCmp('btnHpsBrsItemLabL').disable();
									Ext.getCmp('btnSimpanviJdwlOperasi').disable();
									Ext.getCmp('btnSimpanviJdwlOperasi_kiriman').hide();
									TrviJdwlOperasi.vars.kd_tarif=b.data.kd_tarif;

								},
				   keyUp: function(a,b,c){
				    	$this1=this;
				    	if((b.getKey()>=65 && b.getKey()<=90)|| (b.getKey()>=48 && b.getKey()<=57) || b.getKey()==32 || b.getKey()==8 || b.getKey()==46){
				    		clearTimeout(this.time);

				    		this.time=setTimeout(function(){
			    				if($this1.lastQuery != '' ){
				    				var value=$this1.lastQuery;
				    				var param={};
		        		    		param['text']=$this1.lastQuery;
		        		    		load_data_pasienorder(param);
									viewGridOrderAll_RASEPRWI(param);

			    				}
		    		    	},1000);
				    	}
				    }
			}
		}
	);return cbopasienorder_mng;
};


function mCboKelompokpasien(){
 var cboKelPasienLab = new Ext.form.ComboBox
	(
		{

			id:'cboKelPasienLab',
			fieldLabel: 'Kelompok Pasien',
			x: 130,
			y: 160,
			mode: 'local',
			width: 130,
			//anchor: '95%',
			forceSelection: true,
			lazyRender:true,
			triggerAction: 'all',
			editable: false,
			store: new Ext.data.ArrayStore
			(
				{
				id: 0,
				fields:
				[
					'Id',
					'displayText'
				],
				   data: [[1, 'Perseorangan'], [2, 'Perusahaan'], [3, 'Asuransi']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetKelPasienLab,
			selectOnFocus: true,
			tabIndex:22,
			listeners:
			{
				'select': function(a, b, c)
					{
					   Combo_Select(b.data.displayText);
					   if(b.data.displayText == 'Perseorangan')
					   {

					   }
					   else if(b.data.displayText == 'Perusahaan')
					   {

					   }
					   else if(b.data.displayText == 'Asuransi')
					   {

					   }
					},
				'render': function(a,b,c)
				{
					Ext.getCmp('txtNamaPesertaAsuransiLab').hide();
					Ext.getCmp('txtNoAskesLab').hide();
					Ext.getCmp('txtNoSJPLab').hide();
					Ext.getCmp('cboPerseoranganLab').hide();
					Ext.getCmp('cboAsuransiLab').hide();
					Ext.getCmp('cboPerusahaanRequestEntryLab').hide();

				}
			}

		}
	);
	return cboKelPasienLab;
};

function mComboPerseorangan()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPeroranganLabRequestEntry = new WebApp.DataStore({fields: Field});
    dsPeroranganLabRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboKontrakCustomer',
			    param: 'customer.status=true and kontraktor.jenis_cust=0 order by customer.customer'
			}
		}
	)
    var cboPerseoranganLab = new Ext.form.ComboBox
	(
		{
			id:'cboPerseoranganLab',
			x: 280,
			y: 160,
			editable: false,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			fieldLabel: 'Jenis',
			tabIndex:23,
			width:150,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			store:dsPeroranganLabRequestEntry,
			value:selectSetPerseoranganLab,
			listeners:
			{
				'select': function(a,b,c)
				{
				    selectSetPerseoranganLab=b.data.displayText ;
				}
			}
		}
	);
	return cboPerseoranganLab;
};

function mComboPerusahaan()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboKontrakCustomer',
			    param: 'customer.status=true and kontraktor.jenis_cust=1 order by customer.customer'
			}
		}
	)
    var cboPerusahaanRequestEntryLab = new Ext.form.ComboBox
	(
		{
		    id: 'cboPerusahaanRequestEntryLab',
			x: 280,
			y: 160,
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    fieldLabel: 'Perusahaan',
		    align: 'Right',
			width:150,
		    store: dsPerusahaanRequestEntry,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
		    listeners:
			{
			    'select': function(a,b,c)
				{
					selectSetPerusahaanLab=b.data.valueField ;
				}
			}
		}
	);

    return cboPerusahaanRequestEntryLab;
};

function mComboAsuransi()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: "customer.status=true and kontraktor.jenis_cust=2 order by customer.customer"
            }
        }
    )
    var cboAsuransiLab = new Ext.form.ComboBox
	(
		{
			id:'cboAsuransiLab',
			x: 280,
			y: 160,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			fieldLabel: 'Asuransi',
			align: 'Right',
			width:150,
			//anchor: '95%',
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetAsuransiLab=b.data.valueField ;
				}
			}
		}
	);
	return cboAsuransiLab;
};


function Combo_Select(combo)
{
   var value = combo

   if(value == "Perseorangan")
   {
        Ext.getCmp('txtNamaPesertaAsuransiLab').hide()
        Ext.getCmp('txtNoAskesLab').hide()
        Ext.getCmp('txtNoSJPLab').hide()
        Ext.getCmp('cboPerseoranganLab').show()
        Ext.getCmp('cboAsuransiLab').hide()
        Ext.getCmp('cboPerusahaanRequestEntryLab').hide()


   }
   else if(value == "Perusahaan")
   {
        Ext.getCmp('txtNamaPesertaAsuransiLab').hide()
        Ext.getCmp('txtNoAskesLab').hide()
        Ext.getCmp('txtNoSJPLab').hide()
        Ext.getCmp('cboPerseoranganLab').hide()
        Ext.getCmp('cboAsuransiLab').hide()
        Ext.getCmp('cboPerusahaanRequestEntryLab').show()

   }
   else
       {
         Ext.getCmp('txtNamaPesertaAsuransiLab').show()
         Ext.getCmp('txtNoAskesLab').show()
         Ext.getCmp('txtNoSJPLab').show()
         Ext.getCmp('cboPerseoranganLab').hide()
         Ext.getCmp('cboAsuransiLab').show()
         Ext.getCmp('cboPerusahaanRequestEntryLab').hide()

       }
}



function addNewData(){
	TmpNotransaksi='';
	KdKasirAsal='';
	TglTransaksiAsal='';
	Kd_Spesial='';
	vkode_customer = '';
	No_Kamar='';
	TrviJdwlOperasi.vars.kd_tarif='';
	Ext.getCmp('btnSimpanviJdwlOperasi_kiriman').hide();
	Ext.getCmp('btnSimpanviJdwlOperasi').show();
	Ext.getCmp('txtNoMedrec_viJdwlOperasi').setValue('');
	Ext.getCmp('txtDokterPengirimL').setValue('');
	TrviJdwlOperasi.form.ComboBox.notransaksi.setValue('');
	Ext.getCmp('txtNamaUnitLab').setValue('');
	Ext.getCmp('txtKdUnitLab').setValue('');
	Ext.getCmp('txtKdDokter').setValue('');
	Ext.getCmp('cboDOKTER_viviJdwlOperasi').setValue('');
	Ext.getCmp('txtAlamatL').setValue('');
	TrviJdwlOperasi.form.ComboBox.pasien.setValue('');
	Ext.getCmp('dtpTtlL').setValue(nowTglTransaksi);
	Ext.getCmp('txtKdUrutMasuk').setValue('');
	Ext.getCmp('cboJKviJdwlOperasi').setValue('');
	Ext.getCmp('cboGDRviJdwlOperasi').setValue('')
	Ext.getCmp('txtKdCustomerLamaHide').setValue('');//tampung kd customer untuk transaksi selain kunjungan langsung
	Ext.getCmp('cboKelPasienLab').setValue('');
	//Ext.getCmp('txtCustomerLamaHide').setValue('');
	Ext.getCmp('cboAsuransiLab').setValue('');
	Ext.getCmp('txtNamaPesertaAsuransiLab').setValue('');
	Ext.getCmp('txtNoSJPLab').setValue('');
	Ext.getCmp('txtNoAskesLab').setValue('');
	Ext.getCmp('cboPerusahaanRequestEntryLab').setValue('');
	Ext.getCmp('txtKdDokterPengirim').setValue('');

	//Ext.getCmp('txtCustomerLamaHide').hide();
	Ext.getCmp('cboAsuransiLab').hide();
	Ext.getCmp('txtNamaPesertaAsuransiLab').hide();
	Ext.getCmp('txtNoSJPLab').hide();
	Ext.getCmp('txtNoAskesLab').hide();
	Ext.getCmp('cboPerusahaanRequestEntryLab').hide();
	Ext.getCmp('cboKelPasienLab').show();

	Ext.getCmp('btnLookupItemPemeriksaan').disable();
	Ext.getCmp('btnHpsBrsItemLabL').disable();

	TrviJdwlOperasi.form.ComboBox.notransaksi.enable();
	Ext.getCmp('txtDokterPengirimL').enable();
	Ext.getCmp('txtNamaUnitLab').enable();
	Ext.getCmp('cboDOKTER_viviJdwlOperasi').enable();
	TrviJdwlOperasi.form.ComboBox.pasien.enable();
	Ext.getCmp('txtNoMedrec_viJdwlOperasi').enable();
	Ext.getCmp('txtAlamatL').enable();
	Ext.getCmp('cboJKviJdwlOperasi').enable();
	Ext.getCmp('cboGDRviJdwlOperasi').enable();
	Ext.getCmp('cboKelPasienLab').enable();
	Ext.getCmp('dtpTtlL').enable();
	Ext.getCmp('dtpKunjunganL').enable();
	Ext.getCmp('btnSimpanviJdwlOperasi').enable();
	TrviJdwlOperasi.form.ComboBox.notransaksi.setReadOnly(false);
	Ext.getCmp('txtNamaUnitLab').setReadOnly(true);
	//Ext.getCmp('cboDOKTER_viviJdwlOperasi').setReadOnly(false);
	TrviJdwlOperasi.form.ComboBox.pasien.setReadOnly(false);
	Ext.getCmp('txtAlamatL').setReadOnly(false);
	Ext.getCmp('cboJKviJdwlOperasi').setReadOnly(false);
	Ext.getCmp('cboGDRviJdwlOperasi').setReadOnly(false);
	Ext.getCmp('cboKelPasienLab').setReadOnly(false);
	Ext.getCmp('dtpTtlL').setReadOnly(false);
	//Ext.getCmp('dtpKunjunganL').setReadOnly(true);
	gridDTJadwalOperasi.enable();
	//ViewGridBawahLookupviJdwlOperasi('');
	ViewGridBawahviJdwlOperasi('');
	var_tampung_lab_order=false;
}


function PilihDokterLookUp(NoTrans,TglTrans,kdPrdk,kdTrf,tglBerlaku,trf)
{


	RefreshDataTrDokter(NoTrans,TglTrans,kdPrdk);

	 var FormPilihDokter =
	  {
			layout: 'column',
						border: false,
						anchor: '90%',
						items:
						[

							{
								columnWidth: 0.18,
								layout: 'form',
								labelWidth:70,
								border: false,
								items:
								[
								{
								xtype:'button',
								text:'Simpan',
							 	width:70,
								hideLabel:true,
								id: 'BtnOktrDokter',
								handler:function()
								{
								if(dataTrDokter.length == '' || dataTrDokter.length== 'undefined')
								{
									var jumlah =0;
								}
								else
								{
									var jumlah = dataTrDokter.length;
								}
								for(var i = 0 ;i< dsTrDokter.getCount()  ;i++)
								{
								dataTrDokter.push({
								index		: i,
								no_tran 	: NoTrans,
								//urut		: Urt,
								tgl_trans 	: TglTrans,
								kd_produk 	: kdPrdk,
								kd_tarif	: kdTrf,
								tgl_berlaku	: tglBerlaku,
								tarif		: trf,
								kd_dokter 	: dsTrDokter.data.items[i].data.KD_DOKTER ,
								kd_job 		: dsTrDokter.data.items[i].data.KD_JOB ,
								});
								}
								FormLookUDokter.close();
								}
								},
								],
								},
								{
								columnWidth: 0.2,
								layout: 'form',
								labelWidth:70,
								border: false,
								items:
								[
								{
								xtype:'button',
								text:'Batal' ,
								width:70,
								hideLabel:true,
								id: 'BtnCancelGantiDokter',
								handler:function()
									{
								dataTrDokter = [];
								FormLookUDokter.close()
									}
								}
								]
								}


								]
  }

    var GridTrDokterColumnModel =  new Ext.grid.ColumnModel([
         new Ext.grid.RowNumberer(),
        {
        	 header			: 'kd_dokter',
        	 dataIndex		: 'KD_DOKTER',
			 width			: 80,
        	 menuDisabled	: true,
        	 hidden 		: false
        },{
            header			:'Nama Dokter',
            dataIndex		: 'NAMA',
            sortable		: false,
            hidden			: false,
			menuDisabled	: true,
			width			: 200,
            editor			: getTrDokter(dsTrDokter)
	    },{
            header			: 'Job',
            dataIndex		: 'KD_JOB',
            width			:150,
		   	menuDisabled	:true,
			editor			: JobDokter(),

        },
        ]
    );

  var GridPilihDokter= new Ext.grid.EditorGridPanel({
        title		: '',
		id			: 'GridDokterTr',
		stripeRows	: true,
		width		: 490,
		height		: 245,
        store		: RefreshDataTrDokter(NoTrans,TglTrans,kdPrdk),
        border		: false,
        frame		: false,
        anchor		: '100% 60%',
        autoScroll	: true,
        cm			: GridTrDokterColumnModel,

        //viewConfig	: {forceFit: true},
		listeners	: {
			rowclick: function( $this, rowIndex, e )
			{
        	trcellCurrentTindakan = rowIndex;
    		},
			celldblclick: function(gridView,htmlElement,columnIndex,dataRecord){
   		  }
		}
    });



	var PanelTrDokter = new Ext.Panel
	({
            id: 'PanelPilitTrDokter',
            region: 'center',
			width: 490,
			height: 260,
            border:false,
            layout: 'form',
            frame:true,
            anchor: '100% 8.0001%',
            autoScroll:false,

            items:
            [
			GridPilihDokter,
			{xtype: 'tbspacer',height: 3, width:100},
			FormPilihDokter
			]
	})


    var lebar = 500;
    var FormLookUDokter = new Ext.Window
    (
        {
            id: 'gridTrDokter',
            title: 'Pilih Dokter Tindakan',
            closeAction: 'destroy',
            width: lebar,
            height: 270,
            border: false,
            resizable: false,
            plain: true,
            layout: 'column',
            iconCls: 'Request',
            modal: true,
			tbar		:[
						{
						xtype	: 'button',
						id		: 'btnaddtrdokter',
						iconCls	: 'add',
						text	: 'Tambah Data Dokter',
						handler	:  function()
						{
						dataTrDokter = [];
						TambahBaristrDokter(dsTrDokter);
						}
						},
						'-',
						{
						xtype	: 'button',
						id		: 'btndeltrdokter',
						iconCls	: 'remove',
						text	: 'Delete Data Dokter',
						handler	: function()
						{
						    if (dsTrDokter.getCount() > 0 )
							{
                            if(trcellCurrentTindakan != undefined)
							{
                             HapusDataTrDokter(dsTrDokter);
                            }
                        	}else{
                            ShowPesanWarningDiagnosa('Pilih record ','Hapus data');
                    		}
							}
						},
						'-',
			],
           	items: [
			//GridPilihDokter,

			PanelTrDokter
			],
            listeners:
            {
            }
        }
    );


 FormLookUDokter.show();




};

var mtrDataDokter = Ext.data.Record.create([
	   {name: 'KD_DOKTER', mapping:'KD_DOKTER'},
	   {name: 'NAMA', mapping:'NAMA'},
	   {name: 'KD_JOB', mapping:'KD_JOB'},
	]);

function TambahBaristrDokter(store){
    var x=true;
    if (x === true) {
        var p = BaristrDokter();
        store.insert(store.getCount(), p);
    }
}

function BaristrDokter(){
	var p = new mtrDataDokter({
		'KD_DOKTER':'',
		'NAMA':'',
	    'KD_JOB':'',
	});
	return p;
};

function getTrDokter(store){
	var trDokterData = new Ext.form.ComboBox({
	   typeAhead: true,
	   triggerAction: 'all',
	   lazyRender: true,
	   mode: 'local',
	   hideTrigger:true,
	   forceSelection: true,
	   selectOnFocus:true,
	   store: GetDokter(),
	   valueField: 'NAMA',
	   displayField: 'NAMA',
	   anchor: '95%',
	   listeners:{
		   select: function(a, b, c){
			  store.data.items[trcellCurrentTindakan].data.KD_DOKTER = b.data.KD_DOKTER;
		   }
	   }
	});
	return trDokterData;
}

function GetDokter(){
	var dataDokter  = new WebApp.DataStore({ fields: ['KD_DOKTER','NAMA'] });
	dataDokter.load({
		params: {
			Skip: 0,
			Take: 50,
			target:'ViewComboDokter',
			param: ''
		}
	});
	return dataDokter;
}

function JobDokter(){
	var combojobdokter = new Ext.form.ComboBox({
		typeAhead		: true,
		triggerAction	: 'all',
		lazyRender		: true,
		mode			: 'local',
		anchor 			: '96.8%',
		emptyText		: '',
		store			: new Ext.data.ArrayStore({
			fields	: ['Id','displayText'],
			data	: [[1, 'Dokter'],[2, 'Dokter Anastesi']]
		}),
		valueField		: 'displayText',
		displayField	: 'displayText'
	});
	return combojobdokter;
};


function RefreshDataTrDokter(NoTrans,TglTrans,kdPrdk)
{
	dsTrDokter.load({
		params	: {
			Skip	: 0,
			Take	: 50,
			target	:'ViewTrDokter',
			param	:"b.kd_kasir='06' and b.no_transaksi=~"+NoTrans+"~ and b.tgl_transaksi=~"+TglTrans+"~ and a.kd_produk = ~"+kdPrdk+"~"
		}
	});

	return dsTrDokter;
}

function HapusDataTrDokter(store)
{

    if ( trcellCurrentTindakan != undefined ){
        if (store.data.items[trcellCurrentTindakan].data.KD_DOKTER != '' && store.data.items[trcellCurrentTindakan].data.KD_PRODUK != ''){
					DataDeleteTrDokter(store);
					dsTrDokter.removeAt(trcellCurrentTindakan);
				}

        }/*else{
            dsTrDokter.removeAt(trcellCurrentTindakan);
        }*/


}

function DataDeleteTrDokter(store){
    Ext.Ajax.request({
        url: baseURL + "index.php/main/DeleteDataObj",
        params:
		{
			Table			:'ViewTrDokter',
			no_transaksi	:Ext.getCmp('txtNoTransaksiKasirIGD').getValue(),
			kd_unit			:Ext.getCmp('txtKdUnitIGD').getValue(),
			kd_produk		:store.data.items[trcellCurrentTindakan].data.KD_PRODUK,
			kd_kasir		:'06',
			kd_dokter		:store.data.items[trcellCurrentTindakan].data.KD_DOKTER,
			urut			:JwdKamarOperasiRJ.dsGridTindakan.data.items[cellCurrentTindakan].data.URUT,
			tgl_transaksi	:JwdKamarOperasiRJ.dsGridTindakan.data.items[cellCurrentTindakan].data.TGL_TRANSAKSI,
		},
		//getParamDataDeleteKasirIGDDetail(),
        success: function(o){
            var cst = Ext.decode(o.responseText);
            if (cst.success === true){
                ShowPesanInfoDiagnosa(nmPesanHapusSukses,nmHeaderHapusData);
                store.removeAt(CurrentKasirIGD.row);
				RefreshDataKasirIGDDetail(Ext.getCmp('txtNoTransaksiKasirIGD').getValue());
                RefreshDataTrDokter(Ext.getCmp('txtNoTransaksiKasirIGD').getValue(),JwdKamarOperasiRJ.dsGridTindakan.data.items[cellCurrentTindakan].data.URUT,JwdKamarOperasiRJ.dsGridTindakan.data.items[cellCurrentTindakan].data.TGL_TRANSAKSI,JwdKamarOperasiRJ.dsGridTindakan.data.items[cellCurrentTindakan].data.KD_PRODUK);
				trcellCurrentTindakan=undefined;
            }else{
                ShowPesanInfoDiagnosa(nmPesanHapusError,nmHeaderHapusData);
				RefreshDataKasirIGDDetail(Ext.getCmp('txtNoTransaksiKasirIGD').getValue());
				RefreshDataTrDokter(Ext.getCmp('txtNoTransaksiKasirIGD').getValue(),JwdKamarOperasiRJ.dsGridTindakan.data.items[cellCurrentTindakan].data.URUT,JwdKamarOperasiRJ.dsGridTindakan.data.items[cellCurrentTindakan].data.TGL_TRANSAKSI,JwdKamarOperasiRJ.dsGridTindakan.data.items[cellCurrentTindakan].data.KD_PRODUK);
            }
        }
    });
}


function mcomboSpesialis_OKasiOKPoliklinik()
{
    var Field = ['KD_SPESIAL', 'SPESIALISASI'];

    ds_Poli_viDaftar_KamarOperasi = new WebApp.DataStore({fields: Field});
    ds_Poli_viDaftar_KamarOperasi.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'spesialisasi',
                                    Sortdir: 'ASC',
                                    target: 'ViewSetupSpesial',
                                    param: 'kd_spesial <> 0'
                                }
                    }
            )

    var cboPoliklinikKamarOperasi = new Ext.form.ComboBox
            (
                    {
						id 				: 'cboPoliklinikKamarOperasi',
						typeAhead 		: true,
						triggerAction 	: 'all',
						// lazyRender: true,
						mode 			: 'local',
						// selectOnFocus 	: true,
						// forceSelection 	: true,
						x 				: 130,
						y 				: 130,
						emptyText 		: 'Pilih Spesialisasi...',
						fieldLabel 		: 'Spesialisasi ',
						align 			: 'Right',
						store 			: ds_Poli_viDaftar_KamarOperasi,
						valueField 		: 'KD_SPESIAL',
						displayField 	: 'SPESIALISASI',
						enableKeyEvents : true,    
						width 			: 120,
						listeners:
						{
							'select': function (obj, data, c)
							{
								console.log(data.data);
								loadDataSubSpesialisasi_KamarOperasi("kd_spesial = '"+data.data.KD_SPESIAL+"'");
							},
						}
                    }
            )

    return cboPoliklinikKamarOperasi;
};

function loadDataSubSpesialisasi_KamarOperasi(criteria = ""){
    var Field = ['KD_SUB_SPC', 'SUB_SPESIALISASI'];
	/*ds_Poli_viDaftar_SubKamarOperasi = new WebApp.DataStore({fields: Field});
	ds_Poli_viDaftar_SubKamarOperasi.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'sub_spesialisasi',
                Sortdir: 'ASC',
                target: 'ViewSubSpesial',
                param: criteria
            }
        }
    )*/

    Ext.Ajax.request({
        url: baseURL +  "index.php/general/viewsubspesial/read",
        params: {
        },
        success: function(response) {
            //var mystore = Ext.data.StoreManager.lookup('CustomerDataStore');
            /*var cst  = Ext.decode(response.responseText);
            console.log(cst);
            //dsDataPerawatPenindak_KASIR_RWI.load(myData);
            for(var i=0,iLen=cst['data'].length; i<iLen; i++){
                var recs    = [],recType = dsDataPerawatPenindak_KASIR_RWI.recordType;
                var o       = cst['data'][i];
                recs.push(new recType(o));
                dsDataPerawatPenindak_KASIR_RWI.add(recs);
            }*/
        },
        //jsonData: Ext.JSON.encode(queryform.getValues())
    });
};

function mComboSubSpesialisasiOKPoliklinik(criteria)
{
	loadDataSubSpesialisasi_KamarOperasi();
    var cboPoliklinikKamarOperasi = new Ext.form.ComboBox
            (
                    {
						id 				: 'cboSubPoliklinikKamarOperasi',
						typeAhead 		: true,
						triggerAction 	: 'all',
						// lazyRender: true,
						mode 			: 'local',
						// selectOnFocus 	: true,
						// forceSelection 	: true,
						x 				: 400,
						y 				: 130,
						emptyText 		: 'Pilih Spesialisasi...',
						fieldLabel 		: 'Spesialisasi ',
						align 			: 'Right',
						store 			: ds_Poli_viDaftar_SubKamarOperasi,
						valueField 		: 'KD_SUB_SPC',
						displayField 	: 'SUB_SPESIALISASI',
						enableKeyEvents : true,    
						width 			: 200,
						listeners:
						{
							'select': function (a, b, c)
							{
							},
						}
                    }
            )

    return cboPoliklinikKamarOperasi;
};


function getPanelOrder_unit_lain()
{
	pnlTRSetAsisten_jadwaloperasiOK = new Ext.Window({
	title 		: 'Order unit lain',
	id 			: 'tabSetAsisten_jadwaloperasiOK',
	// fileUpload 	: true,
	// region 		: 'north',
	layout 		: 'column',
	height 		: '100%',
	// anchor 		: '100%',
	width 		: 750,
	border 		: false,
	resizable 	: false,
	plain 		: true,
	closeAction : 'destroy',
	modal 		: true,
	items: [
		Getdetail_order_OK()
	],
	listeners:
	{
		// 'afterrender': function()
		// {
		// 	viewGridOrderAll_RASEPRWI();
		// }
	}
	//load_data_pasienorder();
	/*tbar :[
				{
					xtype: 'label',
					text: 'Cari Asisten: ' ,
					
				},
				{xtype: 'tbspacer',height: 3, width:5},
				 {
					x: 40,
					y: 40,
					xtype: 'textfield',
					//fieldLabel: 'No. Medrec',
					name: 'txtcariasisten',
					id: 'txtcariasisten',
					width: 150,
					listeners:
					{
						'specialkey': function ()
                            {
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                {
                                    dsGridSetAsistenJadwalOperasiOK(Ext.getCmp('txtcariasisten').getValue());
                                }
                            }
					}
				}
			]*/
    });

    pnlTRSetAsisten_jadwaloperasiOK.show();
}


function comboSpesialis_OK()
{ 
 
	var Field = ['kd_spesial','spesialisasi'];

    dsspesialisasi_SetupTindakanasi_OK = new WebApp.DataStore({ fields: Field });
	
	load_data_spesialis_OK();
	cboSpesialisasi_SetupTindakanasi_OK= new Ext.form.ComboBox
	(
		{
			x: 130,
			y: 130,	
			id				: 'cboSpesialisasi_SetupTindakanasi_OK',
			typeAhead		: true,
			triggerAction	: 'all',
			lazyRender		: true,
			mode			: 'local',
			emptyText		: '',
			fieldLabel		:  '',
			align			: 'Right',
			// disabled 		: true,
			width			: 170,
			store			: dsspesialisasi_SetupTindakanasi_OK,
			valueField		: 'kd_spesial',
			displayField	: 'spesialisasi',
			//hideTrigger		: true,
			listeners:
			{
				select	: function(a,b,c){
					// Ext.getCmp('cboSubSpesialisasi_SetupTindakanasi_OK').setValue("");
					// Ext.getCmp('cboJenisOperasi_SetupTindakanasi_OK').setValue("");
					load_data_sub_spesialis_OK(" kd_spesial = '"+b.data.kd_spesial+"' ");
					Ext.getCmp('cboSubSpesialisasi_SetupTindakanasi_OK').setValue('');	
				},
				/*keyUp: function(a,b,c){
					$this1=this;
					if((b.getKey()>=65 && b.getKey()<=90)|| (b.getKey()>=48 && b.getKey()<=57) || b.getKey()==32 || b.getKey()==8 || b.getKey()==46){
						clearTimeout(this.time);
					
						this.time=setTimeout(function(){
							if($this1.lastQuery != '' ){
								var value=$this1.lastQuery;
								var param={};
								param['text']=$this1.lastQuery;
								load_data_spesialis_OK($this1.lastQuery);

								
							}
						},1000);
					}
				},*/
				'render': function (c) {
				    c.getEl().on('keypress', function (e) {
				        if (e.getKey() == 13 ) //atau Ext.EventObject.ENTER
				            Ext.getCmp('cboSubSpesialisasi_SetupTindakanasi_OK').focus();
				    }, c);
				}
				
			}
		}
	);return cboSpesialisasi_SetupTindakanasi_OK;
};

function comboSubSpesialis_OK()
{ 
 
	var Field = ['kd_sub_spc','sub_spesialisasi','kd_spesial'];

    dssubspesialisasi_SetupTindakanasi_OK = new WebApp.DataStore({ fields: Field });
	
	load_data_sub_spesialis_OK('');
	cboSubSpesialisasi_SetupTindakanasi_OK= new Ext.form.ComboBox
	(
		{
			x: 480,
			y: 130,
			id				: 'cboSubSpesialisasi_SetupTindakanasi_OK',
			typeAhead		: true,
			triggerAction	: 'all',
			lazyRender		: true,
			mode			: 'local',
			emptyText		: '',
			fieldLabel		:  '',
			align			: 'Right',
			width			: 170,
			// disabled 		: true,
			store			: dssubspesialisasi_SetupTindakanasi_OK,
			valueField		: 'kd_sub_spc',
			displayField	: 'sub_spesialisasi',
			//hideTrigger		: true,
			listeners:
			{
				select	: function(a,b,c){
					Ext.getCmp('cboJenisOperasi_SetupTindakanasi_OK').setValue("");

					var params = {
						kd_jenis_op : Ext.getCmp('cboJenisOperasi_SetupTindakanasi_OK').getValue(),
						kd_sub_spc  : b.data.kd_sub_spc,
					};
					dsComboTindakanOperasiJadwalOperasiOK(params);
				},
				/*keyUp: function(a,b,c){
					$this1=this;
					if((b.getKey()>=65 && b.getKey()<=90)|| (b.getKey()>=48 && b.getKey()<=57) || b.getKey()==32 || b.getKey()==8 || b.getKey()==46){
						clearTimeout(this.time);
					
						this.time=setTimeout(function(){
							if($this1.lastQuery != '' ){
								var value=$this1.lastQuery;
								var param={};
								param['text']=$this1.lastQuery;
								load_data_sub_spesialis_OK($this1.lastQuery);

								
							}
						},1000);
					}
				},*/
				'render': function (c) {
				    c.getEl().on('keypress', function (e) {
				        if (e.getKey() == 13 ) //atau Ext.EventObject.ENTER
				            Ext.getCmp('cboJenisOperasi_SetupTindakanasi_OK').focus();
				    }, c);
				}
				
			}
		}
	);
	return cboSubSpesialisasi_SetupTindakanasi_OK;
};


function comboJenisOperasi_OK()
{ 
 
	var Field = ['kd_jenis_op','jenis_op'];

    dsjenisoperasi_SetupTindakanasi_OK = new WebApp.DataStore({ fields: Field });
	
	load_data_jenis_operasi_OK();
	cboJenisOperasi_SetupTindakanasi_OK= new Ext.form.ComboBox
	(
		{
			x				: 130,
			y				: 160,
			id				: 'cboJenisOperasi_SetupTindakanasi_OK',
			typeAhead		: true,
			triggerAction	: 'all',
			lazyRender		: true,
			mode			: 'local',
			emptyText		: '',
			fieldLabel		:  '',
			align			: 'Right',
			// disabled 		: true,
			width			: 170,
			store			: dsjenisoperasi_SetupTindakanasi_OK,
			valueField		: 'kd_jenis_op',
			displayField	: 'jenis_op',
			//hideTrigger		: true,
			listeners:
			{
				select	: function(a,b,c){
					// console.log(b);
					var params = {
						kd_jenis_op : b.data.kd_jenis_op,
						kd_sub_spc  : Ext.getCmp('cboSubSpesialisasi_SetupTindakanasi_OK').getValue(),
					};
					dsComboTindakanOperasiJadwalOperasiOK(params);
				},
				/*keyUp: function(a,b,c){
					$this1=this;
					if((b.getKey()>=65 && b.getKey()<=90)|| (b.getKey()>=48 && b.getKey()<=57) || b.getKey()==32 || b.getKey()==8 || b.getKey()==46){
						clearTimeout(this.time);
					
						this.time=setTimeout(function(){
							if($this1.lastQuery != '' ){
								var value=$this1.lastQuery;
								var param={};
								param['text']=$this1.lastQuery;
								load_data_jenis_operasi_OK($this1.lastQuery);

								
							}
						},1000);
					}
				},*/
				'render': function (c) {
				    c.getEl().on('keypress', function (e) {
				        if (e.getKey() == 13 ) //atau Ext.EventObject.ENTER
				            Ext.getCmp('cbo_viComboJenisTindakan_viJdwlOperasi').focus();
				    }, c);
				}
				
			}
		}
	);return cboJenisOperasi_SetupTindakanasi_OK;
};


function load_data_spesialis_OK(param)
{
	dsspesialisasi_SetupTindakanasi_OK.load({
		params:
		{
			Skip: 0,
			Take: 1000,
			Sort: '',
			Sortdir: 'DESC',
			target: 'vComboSpesialisJadwalOperasiOK',
			param: ''
		}
	});
	/*Ext.Ajax.request(
	{
		url: baseURL + "index.php/kamar_operasi/functionSetupTindakan/getSpesialisasi",
		params:{
			command: param
		} ,
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
		success: function(o) {
			cboSpesialisasi_SetupTindakanasi_OK.store.removeAll();
				var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsspesialisasi_SetupTindakanasi_OK.recordType;
				var o=cst['listData'][i];
				
				recs.push(new recType(o));
				dsspesialisasi_SetupTindakanasi_OK.add(recs);
				//console.log(o);
			}
		}
	});*/
}


function load_data_sub_spesialis_OK_rev(params = '')
{
	dssubspesialisasi_SetupTindakanasi_OK.load({
		params:
		{
			Skip: 0,
			Take: 1000,
			Sort: '',
			Sortdir: 'DESC',
			target: 'vComboSubSpesialisJadwalOperasiOK',
			param: params
		}
	});
}

function load_data_sub_spesialis_OK(params = '')
{
	dssubspesialisasi_SetupTindakanasi_OK.load({
		params:
		{
			Skip: 0,
			Take: 1000,
			Sort: '',
			Sortdir: 'DESC',
			target: 'vComboSubSpesialisJadwalOperasiOK',
			param: params
		}
	});
}


function load_data_jenis_operasi_OK()
{
	dsjenisoperasi_SetupTindakanasi_OK.load({
		params:
		{
			Skip: 0,
			Take: 1000,
			Sort: '',
			Sortdir: 'DESC',
			target: 'vComboJenisOperasiJadwalOperasiOK',
			param: ''
		}
	});
	/*Ext.Ajax.request(
	{
		url: baseURL + "index.php/kamar_operasi/functionSetupTindakan/getJenisOperasi",
		params:{
			command: 0
		} ,
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
		success: function(o) {
			cboJenisOperasi_SetupTindakanasi_OK.store.removeAll();
				var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsjenisoperasi_SetupTindakanasi_OK.recordType;
				var o=cst['listData'][i];
				
				recs.push(new recType(o));
				dsjenisoperasi_SetupTindakanasi_OK.add(recs);
				//console.log(o);
			}
		}
	});*/
}