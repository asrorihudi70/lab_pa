var TrviRencanaOperasi={};
TrviRencanaOperasi.form={};
TrviRencanaOperasi.func={};
TrviRencanaOperasi.vars={};
TrviRencanaOperasi.func.parent=TrviRencanaOperasi;
TrviRencanaOperasi.form.ArrayStore={};
TrviRencanaOperasi.form.ComboBox={};
TrviRencanaOperasi.form.DataStore={};
TrviRencanaOperasi.form.Record={};
TrviRencanaOperasi.form.Form={};
TrviRencanaOperasi.form.Grid={};
TrviRencanaOperasi.form.Panel={};
TrviRencanaOperasi.form.TextField={};
TrviRencanaOperasi.form.Button={};
var gridDTRencanaOperasi={};
gridDTRencanaOperasi.form={};
gridDTRencanaOperasi.func={};
gridDTRencanaOperasi.vars={};
gridDTRencanaOperasi.func.parent=gridDTRencanaOperasi;
gridDTRencanaOperasi.form.ArrayStore={};
gridDTRencanaOperasi.form.ComboBox={};
gridDTRencanaOperasi.form.DataStore={};
gridDTRencanaOperasi.form.Record={};
gridDTRencanaOperasi.form.Form={};
gridDTRencanaOperasi.form.Grid={};
gridDTRencanaOperasi.form.Panel={};
gridDTRencanaOperasi.form.TextField={};
gridDTRencanaOperasi.form.Button={};
gridDTRencanaOperasi.form.ArrayStore.rencanaoperasi=new Ext.data.ArrayStore({
		id: 0,
		fields: [	
					'kd_jenis','kd_bahan','nama_bahan','kd_satuan','keterangan'
				],
		data: []
	});
var dsvSetPerawatRencanaOperasiOK;
var dsvComboSubSpesialisRencanaOperasiOK;
var dsvComboSpesialisRencanaOperasiOK;
var kdUnitAsalPasien='';
var noKamarAsalPasien='';
var tglMasukPasien='';
var urutMasukPasien='';
var dsvComboTindakanOperasiRencanaOperasiOK;
var dsvComboDokterRencanaOperasiOK;
var dsvSetAsistenRencanaOperasiOK;
var dsvComboJenisOperasiRencanaOperasiOK;
var kodeKasirNya='RWJ/IGD';
var cellCurrentLab='';
var dspasienorder_mng;
var cbopasienorder_mng;
var  checkColumn_penata__lab;
var dsGetdetail_order;
var gridGetdetail_order;
 var  var_tampung_lab_order=false;
var dsTrDokter	= new WebApp.DataStore({ fields: ['KD_DOKTER','NAMA','KD_JOB','KD_PRODUK'] });
var dataTrDokter = [];

TrviRencanaOperasi.form.ArrayStore.produk=new Ext.data.ArrayStore({
		id: 0,
		fields: [
					'uraian','kd_tarif','kd_produk','tgl_transaksi','tgl_berlaku','harga','qty'
				],
		data: []
	});

TrviRencanaOperasi.form.ArrayStore.kodepasien=new Ext.data.ArrayStore({
		id: 0,
		fields: [
					'no_transaksi','kd_kasir','tgl_transaksi','kd_spesial','kamar','tgl_lahir','kd_pasien','nama','nama_unit_asli',
					'alamat','kd_dokter','kd_unit','nama_unit','urut_masuk','dokter','Jenis_kelamin','gol_darah','kd_customer','customer',
					'no_kamar','kd_spesial','tgl','kd_tarif','kelpasien'
				],
		data: []
	});

TrviRencanaOperasi.form.ArrayStore.pasien=new Ext.data.ArrayStore({
		id: 0,
		fields: [
					'no_transaksi','kd_kasir','tgl_transaksi','kd_spesial','kamar','tgl_lahir','kd_pasien','nama','nama_unit_asli',
					'alamat','kd_dokter','kd_unit','nama_unit','urut_masuk','dokter','Jenis_kelamin','gol_darah','kd_customer','customer',
					'no_kamar','kd_spesial','tgl','kd_tarif','kelpasien'
				],
		data: []
	});

TrviRencanaOperasi.form.ArrayStore.notransaksi=new Ext.data.ArrayStore({
		id: 0,
		fields: [
					'no_transaksi','kd_kasir','tgl_transaksi','kd_spesial','kamar','tgl_lahir','kd_pasien','nama','nama_unit_asli',
					'alamat','kd_dokter','kd_unit','nama_unit','urut_masuk','dokter','Jenis_kelamin','gol_darah','kd_customer','customer',
					'no_kamar','kd_spesial','tgl','kd_tarif','kelpasien'
				],
		data: []
	});

var CurrentviRencanaOperasi =
{
    data: Object,
    details: Array,
    row: 0
};

var tanggaltransaksitampung;
var mRecordRwj = Ext.data.Record.create
(
    [
       {name: 'DESKRIPSI2', mapping:'DESKRIPSI2'},
       {name: 'KD_PRODUK', mapping:'KD_PRODUK'},
       {name: 'DESKRIPSI', mapping:'DESKRIPSI'},
       {name: 'KD_TARIF', mapping:'KD_TARIF'},
       {name: 'HARGA', mapping:'HARGA'},
       {name: 'QTY', mapping:'QTY'},
       {name: 'TGL_TRANSAKSI', mapping:'TGL_TRANSAKSI'},
       {name: 'DESC_REQ', mapping:'DESC_REQ'},
      // {name: 'KD_TARIF', mapping:'KD_TARIF'},
       {name: 'URUT', mapping:'URUT'}
    ]
);
var CurrentDiagnosa =
{
    data: Object,
    details: Array,
    row: 0
};

var FormLookUpGantidokter;
var mRecordDiagnosa = Ext.data.Record.create
(
    [
       {name: 'KASUS', mapping:'KASUS'},
       {name: 'KD_PENYAKIT', mapping:'KD_PENYAKIT'},
       {name: 'PENYAKIT', mapping:'PENYAKIT'},
       //{name: 'KD_TARIF', mapping:'KD_TARIF'},
      // {name: 'HARGA', mapping:'HARGA'},
       {name: 'STAT_DIAG', mapping:'STAT_DIAG'},
       {name: 'TGL_TRANSAKSI', mapping:'TGL_TRANSAKSI'},
      // {name: 'DESC_REQ', mapping:'DESC_REQ'},
      // {name: 'KD_TARIF', mapping:'KD_TARIF'},
       {name: 'URUT_MASUK', mapping:'URUT_MASUK'}
    ]
);

var now = new Date();
var rowSelectedDiagnosa;
var cellSelecteddeskripsi;
var nowTglTransaksi = new Date();
var nowSekarang = new Date();
var TanggalSekarang = nowSekarang.format("d/M/Y");
var nowTglTransaksiGrid = new Date();
var tglGridBawah = nowTglTransaksiGrid.format("d/M/Y");
var tigaharilalu = new Date().add(Date.DAY, -3);

var selectSetGDarahLab;
var selectSetJKLab;
var selectSetKelPasienLab;
var selectSetPerseoranganLab;
var selectSetPerusahaanLab;
var selectSetAsuransiLab;

var dsTRDetailviRencanaOperasiList;
var TmpNotransaksi='';
var KdKasirAsal='';
var TglTransaksiAsal='';
var databaru = 0;
var No_Kamar='';
var Kd_Spesial=0;

var gridDTRencanaOperasi;
var grListviRencanaOperasi;
var tampungshiftsekarang;

//var FocusCtrlCMRWJ;
var vkode_customer;
CurrentPage.page = getPanelviRencanaOperasi(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);



//membuat form
function getPanelviRencanaOperasi(mod_id)
{

/* var i = setInterval(function(){
total_pasien_order_mng();
load_data_pasienorder();
viewGridOrderAll_RASEPRWI();
}, 150000); */

    var FormDepanviRencanaOperasi = new Ext.Panel
    (
        {
            id: mod_id,
            closable: true,
            region: 'center',
            layout: 'form',
            title: 'Rencana Operasi',
            border: false,
            shadhow: true,
            autoScroll:false,
            iconCls: 'Request',
            margins: '0 5 5 0',
            items: [
						//getItemPanelInputviRencanaOperasi(),
						getFormItemEntry_viRencanaOperasi(),
						getFormEntryviRencanaOperasi()
                   ],
			tbar:
            [
				'-',
				{
                        text: 'Add new',
                        id: 'btnAddviRencanaOperasi',
                        iconCls: 'add',
                        handler: function()
						{
							bersihkanSemuaSetelahSimpanOK();
							Ext.getCmp('btnSimpanviRencanaOperasi').enable();
						}
				},
				'-',
				{
                        text: 'Save',
                        id: 'btnSimpanviRencanaOperasi',
                        tooltip: nmSimpan,
                        iconCls: 'save',
                        handler: function()
						{
							//loadMask.show();
							Datasave_viRencanaOperasi(false);
							//alert(Ext.getCmp('cboPerseoranganLab').getValue());
						}
				}
				

			],
            listeners:
            {
                'afterrender': function()
                {
					Ext.Ajax.request(
					{
						url: baseURL + "index.php/main/functionLAB/getCurrentShiftLab",
						params: {
							command: '0',
							// parameter untuk url yang dituju (fungsi didalam controller)
						},
						failure: function(o)
						{
							var cst = Ext.decode(o.responseText);
						},
						success: function(o) {
							tampungshiftsekarang=o.responseText;
						}
					});
				},
				// 'beforeclose': function(){clearInterval(i);}


            }
        }
    );

   return FormDepanviRencanaOperasi;

};


function load_data_pasienorder(param)
{
	//console.log(param);
	if (param==='' || param===undefined)
	{
		param={
			text: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/main/functionLABPoliklinik/getPasien",
		params: param,
		failure: function(o)
		{
			var cst = Ext.decode(o.responseText);
		},
		success: function(o) {
			cbopasienorder_mng.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dspasienorder_mng.recordType;
				var o=cst['listData'][i];

				recs.push(new recType(o));
				dspasienorder_mng.add(recs);
				//console.log(o);
			}
		}
	});
}

function total_pasien_order_mng()
{
	Ext.Ajax.request({
		url: baseURL + "index.php/main/functionLABPoliklinik/gettotalpasienorder_mng",
		params: {
			command: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
		},
		failure: function(o)
		{
			var cst = Ext.decode(o.responseText);
		},
		success: function(o) {
			var cst = Ext.decode(o.responseText);

			Ext.getCmp('txtcounttr').setValue(cst.jumlahpasien);
			//console.log(cst);

		}
	});
};

function getFormEntryviRencanaOperasi(lebar,data)
{

 var GDtabDetailviRencanaOperasi = new Ext.TabPanel
    (
        {
			id:'GDtabDetailviRencanaOperasi',
			region: 'center',
			activeTab: 0,
			height:'100%',
			//autoHeight:true,
			border:false,
			plain: true,
			defaults:{autoScroll: true},
			items:
			[
				grid_ren_ok()//Panel tab detail transaksi
				//-------------- ## --------------
			]
        }

    );

    return GDtabDetailviRencanaOperasi
};







function GetDTGridviRencanaOperasi()
{
    var fldDetail = [ 'tgl_op', 'jam_op', 'durasi','jam_selesai', 'tgl_Rencana', 'kd_pasien','kd_dokter','nama_dokter', 'nama_pasien', 'alamat_pasien', 'kd_unit', 
						'no_kamar', 'nama_kamar', 'kd_unit_asal', 'no_kamar_asal','kd_jenis_op','jenis_op','kd_spesial','spesialisasi','kd_sub_spc','sub_spesialisasi','kd_tindakan', 'tindakan','kd_dokter', 'keterangan',
						'status'];

    dsTRDetailviRencanaOperasiList = new WebApp.DataStore({ fields: fldDetail })
	ViewGridBawahLookupviRencanaOperasi() ;
    gridDTRencanaOperasi = new Ext.grid.EditorGridPanel
    (
        {
            title: '',
            stripeRows: true,
            store: dsTRDetailviRencanaOperasiList,
            border: true,
			height:180,
			width:'100%',
            columnLines: true,
            frame: true,
            autoScroll:true,
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                            cellSelecteddeskripsi = dsTRDetailviRencanaOperasiList.getAt(row);
                            CurrentviRencanaOperasi.row = row;
                            CurrentviRencanaOperasi.data = cellSelecteddeskripsi;
                        }
                    }
                }
            ), 
            cm: TRviRencanaOperasiColumModel(),
			listeners: {
						rowclick: function( $this, rowIndex, e )
						{
						cellCurrentLab = rowIndex;
						},
						celldblclick: function(gridView,htmlElement,columnIndex,dataRecord){
								
							}
						},
						tbar:[
								{
									id: 'btnTambahRencanaGridOK',
									text: 'Tambah Rencana',
									tooltip: 'Tambah Rencana',
									iconCls: 'add',
									handler: function(){
										var records = new Array();
										records.push(new dsTRDetailviRencanaOperasiList.recordType());
										dsTRDetailviRencanaOperasiList.add(records);
									}
								},
								{
									id: 'btnHapusBarisRencanaGridOK',
									text: 'Hapus Baris',
									tooltip: 'Hapus Baris',
									iconCls: 'remove',
									handler: function(){
										var line	=gridDTRencanaOperasi.getSelectionModel().selection.cell[0];
										dsTRDetailviRencanaOperasiList.removeAt(line);
									}
								},
							]
        }
		


    );

    return gridDTRencanaOperasi;
};


function grid_ren_ok(){
	var panel_ren_ok = new Ext.Panel({
		title: 'Rencana Operasi',
		id:'grid_ren_ok',
        fileUpload: true,
        region: 'north',
        layout: 'column',
        height:'100%',
        anchor: '100%',
        width: '100%',
        border: false,
        items: [GetDTGridviRencanaOperasi()]
    });
	return panel_ren_ok;
}
function TRviRencanaOperasiColumModel()
{

	
    return new Ext.grid.ColumnModel
    (
        [ 
            new Ext.grid.RowNumberer(),
			{
				dataIndex: '',
				header: 'Kode',
				sortable: true,
				width: 100,
				editor:new Nci.form.Combobox.autoComplete({
							store	: gridDTRencanaOperasi.form.ArrayStore.rencanaoperasi,
							select	: function(a,b,c){
								var line	=gridDTRencanaOperasi.getSelectionModel().selection.cell[0];
								gridDTRencanaOperasi.getView().refresh();
						},
					insert	: function(o){
						return {
							/* //kd_jenis        : o.kd_jenis,
							kd_bahan 		: o.kd_bahan,
							nama_bahan		: o.nama_bahan,
							kd_satuan		: o.kd_satuan,
							//keterangan		: o.keterangan,
							text			:  '<table style="font-size: 11px;"><tr><td width="50">'+o.kd_bahan+'</td><td width="200">'+o.nama_bahan+'</td></tr></table>', */
						}
					},
					param:function(){
							return {
								
							}
					},
					url		: baseURL + "index.php/gizi/bahanjenisdiet/getrencanaoperasi",
					valueField: 'nama_bahan',
					displayField: 'text',
					listWidth: 380
				})				
			},
			{
				dataIndex: '',
				header: 'Deskripsi',
				sortable: true,
				width: 400
			},
			{
				dataIndex: '',
				header: 'Keterangan',
				sortable: true,
				width: 400
			},
			 {
				dataIndex: 'status',
				header: 'Status',
				sortable: true,
				width: 100
			},

        ]
    )
};

function RecordBaruRWJ()
{
	var tgltranstoday = Ext.getCmp('dtpKunjunganL').getValue();
	var p = new mRecordRwj
	(
		{
			'DESKRIPSI2':'',
			'KD_PRODUK':'',
		    'DESKRIPSI':'',
		    'KD_TARIF':'',
		    'HARGA':'',
		    'QTY':'',
		    'TGL_TRANSAKSI':tgltranstoday.format('Y/m/d'),
		    'DESC_REQ':'',
		    'KD_TARIF':'',
		    'URUT':''
		}
	);

	return p;
};


//tampil grid bawah penjas lab
function ViewGridBawahLookupviRencanaOperasi()
{
     var strKriteriaOK='';
    strKriteriaOK = " oj.status=0 order by oj.tgl_op,oj.jam_op desc";

    dsTRDetailviRencanaOperasiList.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: '',
			    Sortdir: 'DESC',
			    target: 'ViewDetailGridBawahviRencanaOperasi',
			    param: strKriteriaOK
			}
		}
	);
    return dsTRDetailviRencanaOperasiList;
}

function ViewGridBawahviRencanaOperasi(no_transaksi,data)
{
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/main/functionLAB/getItemPemeriksaan",
			params: {no_transaksi:no_transaksi},
			failure: function(o)
			{
				//ShowPesanErrorviRencanaOperasi('Hubungi Admin', 'Error');
			},
			success: function(o)
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					dsTRDetailviRencanaOperasiList.removeAll();
					var recs=[],
						recType=dsTRDetailviRencanaOperasiList.recordType;

					for(var i=0; i<cst.ListDataObj.length; i++){

						recs.push(new recType(cst.ListDataObj[i]));

					}
					dsTRDetailviRencanaOperasiList.add(recs);

					gridDTRencanaOperasi.getView().refresh();

					if(dsTRDetailviRencanaOperasiList.getCount() > 0 ){
						//Ext.getCmp('cboDOKTER_viviRencanaOperasi').setReadOnly(true);
						TmpNotransaksi=data.no_transaksi;
						KdKasirAsal=data.kd_kasir;
						TglTransaksiAsal=data.tgl_transaksi;
						Kd_Spesial=data.kd_spesial;
						No_Kamar=data.no_kamar;
						//kunj_urut_masuk=data.urut_masuk;
						getDokterPengirim(data.no_transaksi,data.kd_kasir);
						//TrviRencanaOperasi.form.ComboBox.notransaksi.setValue(data.no_transaksi);
						Ext.getCmp('txtNamaUnitLab').setValue(data.nama_unit);
						Ext.getCmp('txtKdUnitLab').setValue(data.kd_unit);
						Ext.getCmp('txtKdDokter').setValue(data.kd_dokter);
						Ext.getCmp('cboDOKTER_viviRencanaOperasi').setValue(data.kd_dokter);
						Ext.getCmp('txtAlamatL').setValue(data.alamat);
						Ext.getCmp('dtpTtlL').setValue(ShowDate(data.tgl_lahir));
						Ext.getCmp('txtKdUrutMasuk').setValue(data.urut_masuk);
												console.log(data.urut_masuk);
						Ext.getCmp('cboJKviRencanaOperasi').setValue(data.jenis_kelamin);
						if (data.jenis_kelamin === 't')
						{
							Ext.getCmp('cboJKviRencanaOperasi').setValue('Laki-laki');
						}else
						{
							Ext.getCmp('cboJKviRencanaOperasi').setValue('Perempuan');
						}
						Ext.getCmp('cboJKviRencanaOperasi').disable();

						Ext.getCmp('cboGDRviRencanaOperasi').setValue(data.gol_darah);
						if (data.gol_darah === '0')
						{
							Ext.getCmp('cboGDRviRencanaOperasi').setValue('-');
						}else if (data.gol_darah === '1')
						{
							Ext.getCmp('cboGDRviRencanaOperasi').setValue('A+');
						}else if (data.gol_darah === '2')
						{
							Ext.getCmp('cboGDRviRencanaOperasi').setValue('B+');
						}else if (data.gol_darah === '3')
						{
							Ext.getCmp('cboGDRviRencanaOperasi').setValue('AB+');
						}else if (data.gol_darah === '4')
						{
							Ext.getCmp('cboGDRviRencanaOperasi').setValue('O+');
						}else if (data.gol_darah === '5')
						{
							Ext.getCmp('cboGDRviRencanaOperasi').setValue('A-');
						}else if (data.gol_darah === '6')
						{
							Ext.getCmp('cboGDRviRencanaOperasi').setValue('B-');
						}
						Ext.getCmp('cboGDRviRencanaOperasi').disable();

						Ext.getCmp('txtKdCustomerLamaHide').setValue(data.kd_customer);

						//tampung kd customer untuk transaksi selain kunjungan langsung
						vkode_customer = data.kd_customer;
						//Ext.getCmp('cboKelPasienLab').enable();

						if(data.kelpasien == 'Perseorangan'){
							Ext.getCmp('cboKelPasienLab').setValue(data.kelpasien);
							Ext.getCmp('cboPerseoranganLab').setValue(data.kd_customer);
							Ext.getCmp('cboPerseoranganLab').show();
						} else if(data.kelpasien == 'Perusahaan'){
							Ext.getCmp('cboKelPasienLab').setValue(data.kelpasien);
							Ext.getCmp('cboPerusahaanRequestEntryLab').setValue(data.kd_customer);
							Ext.getCmp('cboPerusahaanRequestEntryLab').show();
						} else{
							Ext.getCmp('cboKelPasienLab').setValue(data.kelpasien);
							Ext.getCmp('cboAsuransiLab').setValue(data.kd_customer);
							Ext.getCmp('cboAsuransiLab').show();
						}
						Ext.getCmp('txtDokterPengirimL').disable();
						Ext.getCmp('txtNamaUnitLab').disable();
						Ext.getCmp('txtAlamatL').disable();
						Ext.getCmp('cboJKviRencanaOperasi').disable();
						Ext.getCmp('cboGDRviRencanaOperasi').disable();
						Ext.getCmp('dtpTtlL').disable();
						Ext.getCmp('dtpKunjunganL').disable();
						Ext.getCmp('btnLookupItemPemeriksaan').disable();
						Ext.getCmp('btnHpsBrsItemLabL').disable();
						
						if (var_tampung_lab_order===true)
						{
						Ext.getCmp('cboDOKTER_viviRencanaOperasi').enable();
						Ext.getCmp('btnSimpanviRencanaOperasi').hide();
						gridDTRencanaOperasi.disable();
						Ext.getCmp('cboKelPasienLab').enable();
						Ext.getCmp('cboAsuransiLab').enable();
						Ext.getCmp('cboPerseoranganLab').enable();
						Ext.getCmp('cboPerusahaanRequestEntryLab').enable();
						Ext.getCmp('btnSimpanviRencanaOperasi_kiriman').show();
						
						}else{
						Ext.getCmp('cboDOKTER_viviRencanaOperasi').disable();
						Ext.getCmp('btnSimpanviRencanaOperasi').show();
						Ext.getCmp('btnSimpanviRencanaOperasi').disable();
						Ext.getCmp('btnSimpanviRencanaOperasi_kiriman').hide();
						
						gridDTRencanaOperasi.enable();
						Ext.getCmp('cboKelPasienLab').disable();
						Ext.getCmp('cboAsuransiLab').disable();
						Ext.getCmp('cboPerseoranganLab').disable();
						Ext.getCmp('cboPerusahaanRequestEntryLab').disable();
						var_tampung_lab_order=false;	
						}
						
					} else {
						TmpNotransaksi=data.no_transaksi;
						KdKasirAsal=data.kd_kasir;
						TglTransaksiAsal=data.tgl_transaksi;
						Kd_Spesial=data.kd_spesial;
						No_Kamar=data.no_kamar;
						Ext.getCmp('btnSimpanviRencanaOperasi').show();
						Ext.getCmp('btnSimpanviRencanaOperasi_kiriman').hide();
						Ext.getCmp('txtNamaUnitLab').setValue(data.nama_unit_asli);
						Ext.getCmp('txtKdUnitLab').setValue(data.kd_unit);
						Ext.getCmp('txtDokterPengirimL').setValue(data.dokter);
						Ext.getCmp('txtAlamatL').setValue(data.alamat);
						Ext.getCmp('dtpTtlL').setValue(ShowDate(data.tgl_lahir));

						Ext.getCmp('cboJKviRencanaOperasi').setValue(data.jenis_kelamin);
						if (data.jenis_kelamin === 't')
						{
							Ext.getCmp('cboJKviRencanaOperasi').setValue('Laki-laki');
						}else
						{
							Ext.getCmp('cboJKviRencanaOperasi').setValue('Perempuan');
						}
						Ext.getCmp('cboJKviRencanaOperasi').disable();

						Ext.getCmp('cboGDRviRencanaOperasi').setValue(data.gol_darah);
						if (data.gol_darah === '0')
						{
							Ext.getCmp('cboGDRviRencanaOperasi').setValue('-');
						}else if (data.gol_darah === '1')
						{
							Ext.getCmp('cboGDRviRencanaOperasi').setValue('A+');
						}else if (data.gol_darah === '2')
						{
							Ext.getCmp('cboGDRviRencanaOperasi').setValue('B+');
						}else if (data.gol_darah === '3')
						{
							Ext.getCmp('cboGDRviRencanaOperasi').setValue('AB+');
						}else if (data.gol_darah === '4')
						{
							Ext.getCmp('cboGDRviRencanaOperasi').setValue('O+');
						}else if (data.gol_darah === '5')
						{
							Ext.getCmp('cboGDRviRencanaOperasi').setValue('A-');
						}else if (data.gol_darah === '6')
						{
							Ext.getCmp('cboGDRviRencanaOperasi').setValue('B-');
						}
						Ext.getCmp('cboGDRviRencanaOperasi').disable();
						Ext.getCmp('txtKdCustomerLamaHide').setValue(data.kd_customer);
						vkode_customer = data.kd_customer;
						Ext.getCmp('cboKelPasienLab').enable();
						if(data.kelpasien == 'Perseorangan'){
							Ext.getCmp('cboKelPasienLab').setValue(data.kelpasien);
							Ext.getCmp('cboPerseoranganLab').setValue(data.kd_customer);
							Ext.getCmp('cboPerseoranganLab').show();
						} else if(data.kelpasien == 'Perusahaan'){
							Ext.getCmp('cboKelPasienLab').setValue(data.kelpasien);
							Ext.getCmp('cboPerusahaanRequestEntryLab').setValue(data.kd_customer);
							Ext.getCmp('cboPerusahaanRequestEntryLab').show();
						} else{
							Ext.getCmp('cboKelPasienLab').setValue(data.kelpasien);
							Ext.getCmp('cboAsuransiLab').setValue(data.kd_customer);
							Ext.getCmp('cboAsuransiLab').show();
						}

						Ext.getCmp('txtDokterPengirimL').disable();
						Ext.getCmp('txtNamaUnitLab').disable();
						Ext.getCmp('cboDOKTER_viviRencanaOperasi').enable();
						Ext.getCmp('txtAlamatL').disable();
						Ext.getCmp('cboJKviRencanaOperasi').disable();
						Ext.getCmp('cboGDRviRencanaOperasi').disable();
						Ext.getCmp('cboKelPasienLab').enable();
						Ext.getCmp('dtpTtlL').disable();
						Ext.getCmp('dtpKunjunganL').disable();
						Ext.getCmp('btnLookupItemPemeriksaan').enable();
						Ext.getCmp('btnHpsBrsItemLabL').disable();
						Ext.getCmp('btnSimpanviRencanaOperasi').enable();
						Ext.getCmp('cboKelPasienLab').disable();
						Ext.getCmp('cboAsuransiLab').disable();
						Ext.getCmp('cboPerseoranganLab').disable();
						Ext.getCmp('cboPerusahaanRequestEntryLab').disable();
						gridDTRencanaOperasi.enable();
						var_tampung_lab_order=false;
					
					
					}
				}
			}
		}

	)
};

//---------------------------------------------------------------------------------------///

//---------------------------------------------------------------------------------------///
function getParamDetailTransaksiOK()
{
	
	var jamOp=Ext.getCmp('txtJam_viRencanaOperasi').getValue()+":"+Ext.getCmp('txtMenit_viRencanaOperasi').getValue()+":00";
	
	var params =
	{
		tglOp: Ext.getCmp('TglOperasi_viRencanaOperasi').getValue().format("Y-m-d"),
		NoKamar:Ext.getCmp('cbo_viComboKamar_viRencanaOperasi').getValue(),
		KdDokter:Ext.getCmp('cbo_viComboDokter_viRencanaOperasi').getValue(),
		KdUnitAsal:kdUnitAsalPasien,
		NoKamarAsal:noKamarAsalPasien,
		tglMasuk:tglMasukPasien,
		urutMasuk:urutMasukPasien,
		KdPasien:TrviRencanaOperasi.form.ComboBox.kdpasien.getValue(),
		NmPasien:Ext.getCmp('txtNama_viRencanaOperasi').getValue(),
		Alamat:Ext.getCmp('txtAlamat_viRencanaOperasi').getValue(),
		Durasi:Ext.getCmp('txtDurasi_viRencanaOperasi').getValue(),
		tglRencana:Ext.getCmp('DateTglRencana_viRencanaOperasi').getValue(),
		jamOperasi:jamOp,
		key_data:'ok_default_kamar',
		kdSpesial: Ext.getCmp('cbo_viComboSpescialis_viRencanaOperasi').getValue(),
		kdSubSpesialis:Ext.getCmp('cbo_viComboSubSpescialis_viRencanaOperasi').getValue(),
		kdJenisOperasi: Ext.getCmp('cbo_viComboJenisOperasi_viRencanaOperasi').getValue(),
		kdJenisTindakan:Ext.getCmp('cbo_viComboJenisTindakan_viRencanaOperasi').getValue(),
		keterangan:Ext.getCmp('txtKeterangan_viRencanaOperasi').getValue(),
	};
	
	//params['KodeUnitSekarang']=kdUnit;	
	params['jumlahperawat']=secondGridStore.getCount();
	for(var i = 0 ; i < secondGridStore.getCount();i++)
	{
		params['kd_dokter-'+i]=secondGridStore.data.items[i].data.kd_dokter;
	}
	
	params['jumlahasisten']=secondGridStore2.getCount();
	for(var i = 0 ; i < secondGridStore2.getCount();i++)
	{
		params['kd_asisten-'+i]=secondGridStore2.data.items[i].data.kd_asisten;
	}
    return params
};

function GetListCountDetailTransaksi()
{

	var x=0;
	for(var i = 0 ; i < dsTRDetailviRencanaOperasiList.getCount();i++)
	{
		if (dsTRDetailviRencanaOperasiList.data.items[i].data.KD_PRODUK != '' || dsTRDetailviRencanaOperasiList.data.items[i].data.DESKRIPSI  != '')
		{
			x += 1;
		};
	}
	return x;

};

function getArrviRencanaOperasi()
{
	var x='';
	var arr=[];
	for(var i = 0 ; i < dsTRDetailviRencanaOperasiList.getCount();i++)
	{

		if (dsTRDetailviRencanaOperasiList.data.items[i].data.kd_produk != '' && dsTRDetailviRencanaOperasiList.data.items[i].data.deskripsi != '')
		{
			var o={};
			var y='';
			var z='@@##$$@@';
			o['URUT']= dsTRDetailviRencanaOperasiList.data.items[i].data.urut;
			o['KD_PRODUK']= dsTRDetailviRencanaOperasiList.data.items[i].data.kd_produk;
			o['QTY']= dsTRDetailviRencanaOperasiList.data.items[i].data.qty;
			o['TGL_TRANSAKSI']= dsTRDetailviRencanaOperasiList.data.items[i].data.tgl_transaksi;
			o['TGL_BERLAKU']= dsTRDetailviRencanaOperasiList.data.items[i].data.tgl_berlaku;
			o['HARGA']= dsTRDetailviRencanaOperasiList.data.items[i].data.harga;
			o['KD_TARIF']= dsTRDetailviRencanaOperasiList.data.items[i].data.kd_tarif;
			o['cito']= dsTRDetailviRencanaOperasiList.data.items[i].data.cito;
			arr.push(o);

		};
	}

	return Ext.encode(arr);
};


function getFormItemEntry_viRencanaOperasi(lebar,rowdata)
{
    var pnlFormDataBasic_viRencanaOperasi = new Ext.FormPanel
    (
		{
			title: '',
			region: 'north',
			layout: 'form',
			//bodyStyle: 'padding:10px 10px 10px 10px',
			anchor: '100%',
			width: lebar,
			border: false,
			//-------------- #items# --------------
			items:
			[
				getItemPanelInformasi_viRencanaOperasi(lebar),
				getItemPanelInputRencana_viRencanaOperasi(lebar), 				
				//getItemGridTransaksi_viRencanaOperasi(lebar)
			],
			//-------------- #End items# --------------
			fileUpload: true,
			// Tombol pada tollbar Edit Data Pasien
			/* tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						id: 'btnAdd_viRencanaOperasi',
						handler: function(){
							dataaddnew_viRencanaOperasi();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viRencanaOperasi',
						handler: function()
						{
							datasave_viRencanaOperasi(addNew_viRencanaOperasi);
							datarefresh_viRencanaOperasi();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						id: 'btnSimpanExit_viRencanaOperasi',
						handler: function()
						{
							var x = datasave_viRencanaOperasi(addNew_viRencanaOperasi);
							datarefresh_viRencanaOperasi();
							if (x===undefined)
							{
								setLookUps_viRencanaOperasi.close();
							}
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viRencanaOperasi',
						handler: function()
						{
							datadelete_viRencanaOperasi();
							datarefresh_viRencanaOperasi();							
						}
					},
					//-------------- ## --------------
					{
						xtype:'tbseparator'
					}
					//-------------- ## --------------					
				]
			} *///,items:
		}
    )

    return pnlFormDataBasic_viRencanaOperasi;
}

function getItemPanelInformasi_viRencanaOperasi(lebar) 
{
    var items =
	{
	    layout: 'Form',
	    anchor: '100%',
		title: 'Informasi Rencana',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		//border:true,
		items:
		[	
			{
				xtype: 'compositefield',
				fieldLabel: 'Tgl. Operasi',
				anchor: '100%',
				width: 199,
				items: 
				[
					{
						xtype: 'datefield',
						id: 'TglOperasi_viRencanaOperasi',						
						format: 'd/M/Y',
						width: 120,
						value:now,
						listeners:
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									datarefresh_viRencanaOperasi();								
								} 						
							}
						}
					},
					{
						xtype: 'displayfield',				
						width: 50,								
						value: 'Kamar:',
						fieldLabel: 'Label',
					},
					viComboKamar_viRencanaOperasi()
				]
			}

		]
	}
	return items;
}
function dsComboKamarOperasiRencanaOperasiOK()
{
	dsvComboKamarOperasiRencanaOperasiOK.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
					Sortdir: 'ASC', 
                    target: 'vComboKamarOperasiRencanaOperasiOK',
					//param:'kd_spesial='+kriteria
                }			
            }
        );   
    return dsvComboKamarOperasiRencanaOperasiOK;
}
function viComboKamar_viRencanaOperasi()
{
	var Field =['kd_unit','no_kamar','nama_kamar','jumlah_bed','digunakan'];
    dsvComboKamarOperasiRencanaOperasiOK = new WebApp.DataStore({fields: Field});
	dsComboKamarOperasiRencanaOperasiOK();
  var cbo_viComboKamar_viRencanaOperasi = new Ext.form.ComboBox
    (
    
        {
            id:"cbo_viComboKamar_viRencanaOperasi",
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'Pilih Kamar..',
            fieldLabel: "Kamar",     
            width:230,
            store:dsvComboKamarOperasiRencanaOperasiOK,
            valueField: 'no_kamar',
            displayField: 'nama_kamar',
            listeners:  
            {
				'select': function(a,b,c)
				{
					if (b.data.jumlah_bed === b.data.digunakan)
					{
						ShowPesanErrorviRencanaOperasi(b.data.nama_kamar+' sudah penuh', 'Error');
						Ext.getCmp('cbo_viComboKamar_viRencanaOperasi').setValue('');
					}
				},
            }
        }
    );
    return cbo_viComboKamar_viRencanaOperasi;
};

function getItemPanelInputRencana_viRencanaOperasi(lebar) 
{
    var items =
	{
	    layout: 'Fit',
	    anchor: '100%',
	    width: lebar- 10,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		border:false,
		height:250,
		items:
		[		
			
					{ //awal panel biodata pasien
						columnWidth:.99,
						layout: 'absolute',
						border: false,
						bodyStyle: 'padding:0px 0px 0px 0px',
						width: 100,
						height: 125,
						anchor: '100% 100%',
						items:
						[
                            //bagian pinggir kiri
							{
								x: 10,
								y: 10,
								xtype: 'label',
								text: 'No. Medrec  '
							},
							{
								x: 120,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							TrviRencanaOperasi.form.ComboBox.kdpasien= new Nci.form.Combobox.autoComplete({
								store	: TrviRencanaOperasi.form.ArrayStore.kodepasien,
								select	: function(a,b,c){
									Ext.getCmp('txtNama_viRencanaOperasi').setValue(b.data.nama);
									Ext.getCmp('txtAlamat_viRencanaOperasi').setValue(b.data.alamat); 
									kdUnitAsalPasien=b.data.kd_unit;
									noKamarAsalPasien=b.data.no_kamar;
									tglMasukPasien=b.data.masuk;
									urutMasukPasien=b.data.urut_masuk;
								},
								width	: 100,
								insert	: function(o){
									return {
										nama        		:o.nama,
										tgl_transaksi		:o.tgl_transaksi,
										kd_spesial			:o.kd_spesial,
										kamar				:o.kamar,
										masuk				:o.masuk,
										tgl_lahir			:o.tgl_lahir,
										gol_darah			:o.gol_darah,
										jenis_kelamin		:o.jenis_kelamin,
										dokter				:o.dokter,
										urut_masuk			:o.urut_masuk,
										nama_unit			:o.nama_unit,
										alamat				:o.alamat,
										kd_pasien        	:o.kd_pasien,
										kd_dokter			:o.kd_dokter,
										kd_unit				:o.kd_unit,
										kd_customer			:o.kd_customer,
										kd_kasir			:o.kd_kasir,
										nama_kamar			:o.nama_kamar,
										no_kamar			:o.no_kamar,
										customer			:o.customer,
										nama_unit_asli		:o.nama_unit_asli,
										tgl					:o.tgl,
										kd_tarif			:o.kd_tarif,
										kelpasien			:o.kelpasien,
										text				:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_pasien+'</td><td width="70" align="left">'+o.no_transaksi+'</td><td width="130">'+o.nama+'</td><td width="110" align="left">'+o.nama_unit_asli+'</td><td width="70">'+o.tgl+'</td></tr></table>',
										no_transaksi		:o.no_transaksi,
									}
								},
								param:function(){
									var a=0;
									return {
										kd_kasir:kodeKasirNya,
										a:1
									}
								},
								url		: baseURL + "index.php/main/functionOK/getPasien",
								valueField: 'kd_pasien',
								displayField: 'text',
								listWidth: 480,
								x : 130,
								y : 10,
								emptyText: 'No medrec'
							}),
							{
								x: 10,
								y: 40,
								xtype: 'label',
								text: 'Alamat '
							},
							{
								x: 120,
								y: 40,
								xtype: 'label',
								text: ':'
							},
							{
								xtype: 'textfield',		
								x: 130,
								y: 40,
								fieldLabel: '',		
								width: 470,
								name: 'txtAlamat_viRencanaOperasi',
								id: 'txtAlamat_viRencanaOperasi',
								emptyText: 'Alamat',
								disabled:true
							},
							{
								x: 10,
								y: 70,
								xtype: 'label',
								text: 'Tgl. Rencana  '
							},
							{
								x: 120,
								y: 70,
								xtype: 'label',
								text: ':'
							},
							{
								xtype: 'datefield',
								fieldLabel: 'Tanggal',
								x: 130,
								y: 70,
								id: 'DateTglRencana_viRencanaOperasi',
								name: 'DateTglRencana_viRencanaOperasi',
								width: 130,
								format: 'd/M/Y',
								value:now,
								disabled:true
							},
							{
								x: 10,
								y: 100,
								xtype: 'label',
								text: 'Jam Operasi'
							},
							{
								x: 120,
								y: 100,
								xtype: 'label',
								text: ':'
							},
							{
								xtype: 'textfield',
								x: 130,
								y: 100,
								fieldLabel: 'Jam',
								id: 'txtJam_viRencanaOperasi',
								name: 'txtJam_viRencanaOperasi',
								width: 50,
								emptyText:'Jam',
								maxLength:2,
							},
							{
								xtype: 'textfield',
								x: 190,
								y: 100,
								fieldLabel: 'Menit',
								id: 'txtMenit_viRencanaOperasi',
								name: 'txtMenit_viRencanaOperasi',
								width: 50,
								emptyText:'Menit',
								maxLength:2,
							},
							
							{
								x: 10,
								y: 130,
								xtype: 'label',
								text: 'Spesialis'
							},
							{
								x: 120,
								y: 130,
								xtype: 'label',
								text: ':'
							},
							viComboSpescialis_viRencanaOperasi(),
							{
								x: 10,
								y: 160,
								xtype: 'label',
								text: 'Jenis Operasi '
							},
							{
								x: 120,
								y: 160,
								xtype: 'label',
								text: ':'
							},
							viComboJenis_viRencanaOperasi(),
							{
								x: 10,
								y: 190,
								xtype: 'label',
								text: 'Keterangan '
							},
							{
								x: 120,
								y: 190,
								xtype: 'label',
								text: ':'
							},
							{
								xtype: 'textfield',
								x: 130,
								y: 190,
								fieldLabel: 'Keterangan',
								id: 'txtKeterangan_viRencanaOperasi',
								name: 'txtKeterangan_viRencanaOperasi',
								width: 470
							},
							{
								x: 280,
								y: 10,
								xtype: 'label',
								text: 'Nama Pasien '
							},
							{
								x: 390,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								xtype: 'textfield',		
								x: 400,
								y: 10,
								fieldLabel: '',		
								width: 200,
								name: 'txtNama_viRencanaOperasi',
								id: 'txtNama_viRencanaOperasi',
								emptyText: 'Nama',
								disabled:true
							},
							{
								x: 280,
								y: 70,
								xtype: 'label',
								text: 'Dokter '
							},
							{
								x: 390,
								y: 70,
								xtype: 'label',
								text: ':'
							},
							 viComboDokter_viRencanaOperasi(),
							{
								x: 280,
								y: 100,
								xtype: 'label',
								text: 'Durasi'
							},
							{
								x: 390,
								y: 100,
								xtype: 'label',
								text: ':'
							},
							{
								xtype: 'numberfield',					
								fieldLabel: '',		
								x: 400,
								y: 100,								
								width: 80,
								name: 'txtDurasi_viRencanaOperasi',
								id: 'txtDurasi_viRencanaOperasi',
								emptyText: '',
							},
							{
								xtype: 'displayfield',				
								width: 180,			
								x: 490,
								y: 100,									
								value: '(Dalam Menit)',
								fieldLabel: 'Label',
								id: 'lblKetDurasi_viRencanaOperasi',
								name: 'lblKetDurasi_viRencanaOperasi'
							},
							{
								x: 310,
								y: 130,
								xtype: 'label',
								text: 'Sub. Spesialis'
							},
							{
								x: 400,
								y: 130,
								xtype: 'label',
								text: ':'
							},
							viComboSubSpescialis_viRencanaOperasi(),
							{
								x: 310,
								y: 160,
								xtype: 'label',
								text: 'Jenis Tindakan'
							},
							{
								x: 400,
								y: 160,
								xtype: 'label',
								text: ':'
							},
							viComboJenisTindakan_viRencanaOperasi(),
							

						]
					},
			
		] 
	};
    return items;
};
function dsComboDokterRencanaOperasiOK()
{
	dsvComboDokterRencanaOperasiOK.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'vComboDokterRencanaOperasiOK',
                }			
            }
        );   
    return dsvComboDokterRencanaOperasiOK;
}


function viComboDokter_viRencanaOperasi()
{
	var Field =['kd_dokter','nama'];
    dsvComboDokterRencanaOperasiOK = new WebApp.DataStore({fields: Field});
	dsComboDokterRencanaOperasiOK();
  var cbo_viComboDokter_viRencanaOperasi = new Ext.form.ComboBox
    (
    
        {
            id:"cbo_viComboDokter_viRencanaOperasi",
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'Pilih Dokter..',
            fieldLabel: "Dokter",  
			x: 400,
			y: 70,			
            width:200,
            store:dsvComboDokterRencanaOperasiOK,
            valueField: 'kd_dokter',
            displayField: 'nama',
            listeners:  
            {
            }
        }
    );
    return cbo_viComboDokter_viRencanaOperasi;
};




function dsComboSpesialisRencanaOperasiOK()
{
	dsvComboSpesialisRencanaOperasiOK.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'vComboSpesialisRencanaOperasiOK',
                }			
            }
        );   
    return dsvComboSpesialisRencanaOperasiOK;
}

function viComboSpescialis_viRencanaOperasi()
{
	var Field =['kd_spesial','spesialisasi'];
    dsvComboSpesialisRencanaOperasiOK = new WebApp.DataStore({fields: Field});
	dsComboSpesialisRencanaOperasiOK();
  var cbo_viComboSpescialis_viRencanaOperasi = new Ext.form.ComboBox
    (
    
        {
            id:"cbo_viComboSpescialis_viRencanaOperasi",
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
			x: 130,
			y: 130,
            emptyText:'Pilih Specialis..',
            fieldLabel: "Spesialis",     
            width:180,
            store: dsvComboSpesialisRencanaOperasiOK,
            valueField: 'kd_spesial',
            displayField: 'spesialisasi',
            listeners:  
            {
				'select': function(a,b,c)
				{
					dsComboSubSpesialisRencanaOperasiOK(b.data.kd_spesial);
				},
            }
        }
    );
    return cbo_viComboSpescialis_viRencanaOperasi;
};
function dsComboSubSpesialisRencanaOperasiOK(kriteria)
{
	dsvComboSubSpesialisRencanaOperasiOK.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
					Sortdir: 'ASC', 
                    target: 'vComboSubSpesialisRencanaOperasiOK',
					param:'kd_spesial='+kriteria
                }			
            }
        );   
    return dsvComboSubSpesialisRencanaOperasiOK;
}
function viComboSubSpescialis_viRencanaOperasi()
{
var Field =['kd_sub_spc','kd_spesial','sub_spesialisasi'];
    dsvComboSubSpesialisRencanaOperasiOK = new WebApp.DataStore({fields: Field});
	//dsComboSubSpesialisRencanaOperasiOK();
  var cbo_viComboSubSpescialis_viRencanaOperasi = new Ext.form.ComboBox
    (
    
        {
            id:"cbo_viComboSubSpescialis_viRencanaOperasi",
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
			x: 410,
			y: 130,	
            mode: 'local',
            emptyText:'Pilih Sub Specialis..',
            fieldLabel: "Sub Spesialis",     
            width:190,
            store:dsvComboSubSpesialisRencanaOperasiOK,
            valueField: 'kd_sub_spc',
            displayField: 'sub_spesialisasi',
            listeners:  
            {
				'select': function(a,b,c)
				{
					var kri='ss.kd_spesial='+b.data.kd_spesial+' and ss.kd_sub_spc='+b.data.kd_sub_spc;
					dsComboTindakanOperasiRencanaOperasiOK(kri) ;
				},
            }
        }
    );
    return cbo_viComboSubSpescialis_viRencanaOperasi;
};
function dsComboJenisOperasiRencanaOperasiOK()
{
	dsvComboJenisOperasiRencanaOperasiOK.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
					Sortdir: 'ASC', 
                    target: 'vComboJenisOperasiRencanaOperasiOK',
					//param:'kd_spesial='+kriteria
                }			
            }
        );   
    return dsvComboJenisOperasiRencanaOperasiOK;
}
function viComboJenis_viRencanaOperasi()
{
	var Field =['kd_jenis_op','jenis_op'];
    dsvComboJenisOperasiRencanaOperasiOK = new WebApp.DataStore({fields: Field});
	dsComboJenisOperasiRencanaOperasiOK();
  var cbo_viComboJenisOperasi_viRencanaOperasi = new Ext.form.ComboBox
    (
    
        {
            id:"cbo_viComboJenisOperasi_viRencanaOperasi",
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'Pilih Jenis Operasi..',
            fieldLabel: "Jenis Operasi",    
			x: 130,
			y: 160,				
            width:180,
            store:dsvComboJenisOperasiRencanaOperasiOK,
            valueField: 'kd_jenis_op',
            displayField: 'jenis_op',
            listeners:  
            {
            }
        }
    );
    return cbo_viComboJenisOperasi_viRencanaOperasi;
};
function dsComboTindakanOperasiRencanaOperasiOK(kriteria)
{
	dsvComboTindakanOperasiRencanaOperasiOK.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
					Sortdir: 'ASC', 
                    target: 'vComboTindakanOperasiRencanaOperasiOK',
					param:kriteria
                }			
            }
        );   
    return dsvComboTindakanOperasiRencanaOperasiOK;
}
function viComboJenisTindakan_viRencanaOperasi()
{
	var Field =['kd_tindakan','tindakan'];
    dsvComboTindakanOperasiRencanaOperasiOK = new WebApp.DataStore({fields: Field});
	
  var cbo_viComboJenisTindakan_viRencanaOperasi = new Ext.form.ComboBox
    (
    
        {
            id:"cbo_viComboJenisTindakan_viRencanaOperasi",
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
			x: 410,
			y: 160,	
            mode: 'local',
            emptyText:'Pilih Jenis Tindakan..',
            fieldLabel: "Jenis Tindakan",           
            width:190,
            store:dsvComboTindakanOperasiRencanaOperasiOK,
            valueField: 'kd_tindakan',
            displayField: 'tindakan',
            listeners:  
            {
            }
        }
    );
    return cbo_viComboJenisTindakan_viRencanaOperasi;
};

/* function getItemGridTransaksi_viRencanaOperasi(lebar) 
{
    var items =
	{
		title: '', 
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		border:true,
		width: lebar-80,
		height:200, 
	    items:
		[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:
				[
					//gridDataViewEdit_viRencanaOperasi(),
					
				]	
			}
			//-------------- ## --------------
		]
	};
    return items;
}; */

/* function gridDataViewEdit_viRencanaOperasi()
{
    
    var FieldGrdKasir_viRencanaOperasi = 
	[
	];
	
    dsDataGrdJab_viRencanaOperasi= new WebApp.DataStore
	({
        fields: FieldGrdKasir_viRencanaOperasi
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJab_viRencanaOperasi,
        height: 150,
		selModel: new Ext.grid.RowSelectionModel
        (
	        {
	            singleSelect: true,
	            listeners:
	            {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
	        }
        ),
        columns: 
		[
			{
				dataIndex: '',
				header: 'Status Operasi',
				sortable: true,
				width: 120
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'No. MedRec',
				sortable: true,
				width: 100
			},
			//-------------- ## --------------			
			{			
				dataIndex: '',
				header: 'Nama Pasien',
				sortable: true,
				width: 150
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Alamat',
				sortable: true,
				width: 150,
				renderer: function(v, params, record) 
				{
					
				}			
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Dokter',
				sortable: true,
				width: 120,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Tindakan',
				sortable: true,
				width: 150,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Durasi',
				sortable: true,
				width: 70,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Selesai',
				sortable: true,
				width: 80,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Keterangan',
				sortable: true,
				width: 200,
				renderer: function(v, params, record) 
				{
					
				}	
			}
        ]
    }    
    return items;
} */


function Datasave_viRencanaOperasi(mBol)
{
	if (ValidasiEntryviRencanaOperasi(nmHeaderSimpanData,false) == 1 )
	{ 
		Ext.Ajax.request
		 (
			{
				url: baseURL + "index.php/kamar_operasi/functionKamarOperasi/savedetailRencanaoperasi",
				params: getParamDetailTransaksiOK(),
				failure: function(o)
				{	//var_tampung_lab_order=false;
					loadMask.hide();
					ShowPesanErrorviRencanaOperasi('Error! Gagal menyimpan data ini. Hubungi Admin', 'Error');
					//ViewGridBawahviRencanaOperasi(TrviRencanaOperasi.form.ComboBox.notransaksi.getValue());
				},
				success: function(o)
				{  	//var_tampung_lab_order=false;
					var cst = Ext.decode(o.responseText);
					if (cst.success === true)
					{
						loadMask.hide();
						//ShowPesanInfoviRencanaOperasi("Berhasil menyimpan data ini","Information");
						if (cst.status_kamar === 1)
						{
							ShowPesanErrorviRencanaOperasi('Maaf !! waktu yang anda tentukan telah ada yang mengisi. ', 'Error');
						}
						else
						{
							ShowPesanInfoviRencanaOperasi("Berhasil menyimpan data ini","Information");
							ViewGridBawahLookupviRencanaOperasi() ;
							bersihkanSemuaSetelahSimpanOK();
						}
					}
					else
					{
						loadMask.hide();
						ShowPesanErrorviRencanaOperasi('Gagal menyimpan data ini. Hubungi Admin', 'Error');
					};
				}
			}
		)

	}
	else
	{
		if(mBol === true)
		{
			loadMask.hide();
			return false;
		};
	}; 

};

function Dataupdate_order_viRencanaOperasi(mBol)
{
	if (ValidasiEntryviRencanaOperasi(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		 (
			{
				url: baseURL + "index.php/main/functionLAB/update_dokter",
				params: getParamDetailTransaksiLAB(),
				failure: function(o)
				{	var_tampung_lab_order=false;
					loadMask.hide();
					ShowPesanErrorviRencanaOperasi('Error! Gagal menyimpan data ini. Hubungi Admin', 'Error');
					ViewGridBawahviRencanaOperasi(TrviRencanaOperasi.form.ComboBox.notransaksi.getValue());
				},
				success: function(o)
				{  	
					total_pasien_order_mng();
					load_data_pasienorder();
					var_tampung_lab_order=false;
					var cst = Ext.decode(o.responseText);
					if (cst.success === true)
					{
						loadMask.hide();
						ShowPesanInfoviRencanaOperasi("Berhasil menyimpan data ini","Information");
						TrviRencanaOperasi.form.ComboBox.notransaksi.setValue(cst.notrans);
						TrviRencanaOperasi.form.ComboBox.kdpasien.setValue(cst.kdPasien);
						if(cst.kdPasien.substring(0, 1) ==='L'){
							Ext.getCmp('txtNamaUnitLab').setValue('Laboratorium');
							Ext.getCmp('txtDokterPengirimL').setValue('-');
						}
						Ext.getCmp('cboDOKTER_viviRencanaOperasi').disable();
						Ext.getCmp('btnLookupItemPemeriksaan').disable();
						Ext.getCmp('btnHpsBrsItemLabL').disable();
						Ext.getCmp('btnSimpanviRencanaOperasi').disable();
						Ext.getCmp('btnSimpanviRencanaOperasi_kiriman').hide();
						Ext.getCmp('btnSimpanviRencanaOperasi').show();
						ViewGridBawahviRencanaOperasi(TrviRencanaOperasi.form.ComboBox.notransaksi.getValue());
						Ext.getCmp('cboKelPasienLab').disable();
						Ext.getCmp('cboAsuransiLab').disable();
						Ext.getCmp('cboPerseoranganLab').disable();
						Ext.getCmp('cboPerusahaanRequestEntryLab').disable();
						//RefreshDataFilterPenJasRad();
						if(mBol === false)
						{
							loadMask.hide();
							ViewGridBawahviRencanaOperasi(TrviRencanaOperasi.form.ComboBox.notransaksi.getValue());
						};
					}
					else
					{
						loadMask.hide();
						ShowPesanErrorviRencanaOperasi('Gagal menyimpan data ini. Hubungi Admin', 'Error');
					};
				}
			}
		)

	}
	else
	{
		if(mBol === true)
		{
			loadMask.hide();
			return false;
		};
	};

};

function getDokterPengirim(no_transaksi,kd_kasir){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/main/functionLAB/getDokterPengirim",
			params:{no_transaksi:no_transaksi,kd_kasir:kd_kasir} ,
			failure: function(o)
			{
				ShowPesanErrorviRencanaOperasi('Hubungi Admin', 'Error');
			},
			success: function(o)
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					Ext.getCmp('txtDokterPengirimL').setValue(cst.nama);
				}
				else
				{
					ShowPesanErrorviRencanaOperasi('Gagal membaca dokter pengirim', 'Error');
				};
			}
		}

	)
}

function bersihkanSemuaSetelahSimpanOK()
{
	Ext.getCmp('txtJam_viRencanaOperasi').setValue("");
	Ext.getCmp('txtMenit_viRencanaOperasi').setValue("");
	Ext.getCmp('TglOperasi_viRencanaOperasi').setValue(now);
	Ext.getCmp('cbo_viComboKamar_viRencanaOperasi').setValue("");
	Ext.getCmp('cbo_viComboDokter_viRencanaOperasi').setValue("");
	TrviRencanaOperasi.form.ComboBox.kdpasien.setValue(""),
	Ext.getCmp('txtNama_viRencanaOperasi').setValue(""),
	Ext.getCmp('txtAlamat_viRencanaOperasi').setValue(""),
	Ext.getCmp('txtDurasi_viRencanaOperasi').setValue(""),
	Ext.getCmp('DateTglRencana_viRencanaOperasi').setValue(now),
	Ext.getCmp('cbo_viComboSpescialis_viRencanaOperasi').setValue(""),
	Ext.getCmp('cbo_viComboSubSpescialis_viRencanaOperasi').setValue(""),
	Ext.getCmp('cbo_viComboJenisOperasi_viRencanaOperasi').setValue(""),
	Ext.getCmp('cbo_viComboJenisTindakan_viRencanaOperasi').setValue(""),
	Ext.getCmp('txtKeterangan_viRencanaOperasi').setValue("");
	secondGridStore.removeAll();
	dsGridSetPerawatRencanaOperasiOK();
	secondGridStore2.removeAll();
	dsGridSetAsistenRencanaOperasiOK();
}
function ValidasiEntryviRencanaOperasi(modul,mBolHapus)
{
	var x = 1;
		var tanggalOperasi = new Date(Ext.getCmp('TglOperasi_viRencanaOperasi').getValue()).toDateString();
		var currenttime = new Date(nowSekarang).toDateString();
		var valJam = parseInt(Ext.getCmp('txtJam_viRencanaOperasi').getValue());
		var valMenit = parseInt(Ext.getCmp('txtMenit_viRencanaOperasi').getValue());
		if (valJam > 23)
		{
			loadMask.hide();
			ShowPesanWarningviRencanaOperasi('Jam tidak boleh lebih dari angka 23', 'Warning');
			x = 0;
		}
		else if (valMenit > 59)
		{
			loadMask.hide();
			ShowPesanWarningviRencanaOperasi('Menit tidak boleh lebih dari angka 59', 'Warning');
			x = 0;
		}
		else if (tanggalOperasi < currenttime) //|| (tanggalOperasi<=TanggalSekarang && ))
		{
			
			loadMask.hide();
			ShowPesanWarningviRencanaOperasi('Tanggal operasi tidak boleh kurang dari tanggal hari sekarang', 'Warning');
			x = 0;
		}
		else if (Ext.getCmp('TglOperasi_viRencanaOperasi').getValue() == '')
		{
		
			loadMask.hide();
			ShowPesanWarningviRencanaOperasi('Tanggal operasi tidak boleh kosong', 'Warning');
			x = 0;
		}
		else if (Ext.getCmp('cbo_viComboKamar_viRencanaOperasi').getValue() == 'Pilih Kamar..' || Ext.getCmp('cbo_viComboKamar_viRencanaOperasi').getValue()  === '')
		{
			
			loadMask.hide();
			ShowPesanWarningviRencanaOperasi('Kamar tidak boleh kosong', 'Warning');
			x = 0;
		}
		else if (TrviRencanaOperasi.form.ComboBox.kdpasien.getValue() == '')
		{
			loadMask.hide();
			ShowPesanWarningviRencanaOperasi('Belum ada pasien yang dipilih', 'Warning');
			x = 0;
		}
		else if (Ext.get('cbo_viComboDokter_viRencanaOperasi').getValue() == 'Pilih Dokter..' || Ext.get('cbo_viComboDokter_viRencanaOperasi').getValue() == '')
		{
			loadMask.hide();
			ShowPesanWarningviRencanaOperasi('Dokter tidak boleh kosong', 'Warning');
			x = 0;
		}
		else if (Ext.getCmp('txtJam_viRencanaOperasi').getValue() == '' || Ext.getCmp('txtMenit_viRencanaOperasi').getValue()  === '')
		{
			loadMask.hide();
			ShowPesanWarningviRencanaOperasi('Jam operasi tidak boleh kosong', 'Warning');
			x = 0;
		}
		else if (Ext.get('txtDurasi_viRencanaOperasi').getValue() == '')
		{
			loadMask.hide();
			ShowPesanWarningviRencanaOperasi('Durasi tidak boleh kosong','Warning');
			x = 0;
		}
		  else if(Ext.get('cbo_viComboSpescialis_viRencanaOperasi').getValue() == 'Pilih Specialis..' || Ext.get('cbo_viComboSpescialis_viRencanaOperasi').getValue() == ''){
			loadMask.hide();
			ShowPesanWarningviRencanaOperasi('Spesialis tidak boleh kosong','Warning');
			x = 0;
		} 
		  else if(Ext.get('cbo_viComboSubSpescialis_viRencanaOperasi').getValue() == 'Pilih Sub Specialis..' || Ext.get('cbo_viComboSubSpescialis_viRencanaOperasi').getValue() == ''){
			loadMask.hide();
			ShowPesanWarningviRencanaOperasi('Sub spesialis tidak boleh kosong','Warning');
			x = 0;
		}
		  else if(Ext.get('cbo_viComboJenisOperasi_viRencanaOperasi').getValue() == 'Pilih Jenis Operasi..' || Ext.get('cbo_viComboJenisOperasi_viRencanaOperasi').getValue() == ''){
			loadMask.hide();
			ShowPesanWarningviRencanaOperasi('Jenis Operasi tidak boleh kosong','Warning');
			x = 0;
		}  else if(Ext.get('cbo_viComboJenisTindakan_viRencanaOperasi').getValue() == 'Pilih Jenis Tindakan..' || Ext.get('cbo_viComboJenisTindakan_viRencanaOperasi').getValue() == ''){
			loadMask.hide();
			ShowPesanWarningviRencanaOperasi('Jenis tindakan tidak boleh kosong','Warning');
			x = 0;
		} 
	
	return x;
};


function ShowPesanWarningviRencanaOperasi(str, modul)
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorviRencanaOperasi(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoviRencanaOperasi(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};


//combo jenis kelamin
function mComboJK()
{
    var cboJKviRencanaOperasi = new Ext.form.ComboBox
	(
		{
			id:'cboJKviRencanaOperasi',
			x: 495,
            y: 100,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			tabIndex:3,
			selectOnFocus:true,
			forceSelection: true,
			fieldLabel: 'Jenis Kelamin ',
			editable: false,
			width:95,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Laki - Laki'], [2, 'Perempuan']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			//value:selectSetJKLab,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetJKLab=b.data.valueField ;
				},
			}
		}
	);
	return cboJKviRencanaOperasi;
};

//combo golongan darah
function mComboGDarah()
{
    var cboGDRviRencanaOperasi = new Ext.form.ComboBox
	(
		{
			id:'cboGDRviRencanaOperasi',
			x: 690,
            y: 100,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			tabIndex:3,
			selectOnFocus:true,
			forceSelection: true,
			fieldLabel: 'Gol. Darah ',
			width:50,
			editable: false,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[0, '-'], [1, 'A+'],[2, 'B+'], [3, 'AB+'],[4, 'O+'], [5, 'A-'], [6, 'B-']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			//value:selectSetGDarahLab,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetGDarahLab=b.data.displayText ;
				},

			}
		}
	);
	return cboGDRviRencanaOperasi;
};

//Combo Dokter Lookup
function mComboDOKTER()
{
    var Field = ['KD_DOKTER','NAMA'];

    dsdokter_viviRencanaOperasi = new WebApp.DataStore({ fields: Field });
    dsdokter_viviRencanaOperasi.load
	(
            {
                params:
                    {
                        Skip: 0,
                        Take: 1000,

                        Sort: 'nama',
                        Sortdir: 'ASC',
                        target: 'ViewDokterPenunjang',
                        param: "kd_unit = '41'"
                    }
            }
	);

    var cboDOKTER_viviRencanaOperasi = new Ext.form.ComboBox
	(
		{
			id: 'cboDOKTER_viviRencanaOperasi',
			x: 130,
			y: 130,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			emptyText: '',
			fieldLabel:  ' ',
			align: 'Right',
			width: 230,
			store: dsdokter_viviRencanaOperasi,
			valueField: 'KD_DOKTER',
			displayField: 'NAMA',
			editable: true,
			listeners:
			{
				'select': function(a, b, c)
				{
					if(var_tampung_lab_order===true)
					{
					var_tampung_lab_order=true;
					Ext.getCmp('btnLookupItemPemeriksaan').disable();
					}else{
					var_tampung_lab_order=false;
					Ext.getCmp('btnLookupItemPemeriksaan').enable();
					}
				}
			}
		}
	);

    return cboDOKTER_viviRencanaOperasi;
};








function mCboKelompokpasien(){
 var cboKelPasienLab = new Ext.form.ComboBox
	(
		{

			id:'cboKelPasienLab',
			fieldLabel: 'Kelompok Pasien',
			x: 130,
			y: 160,
			mode: 'local',
			width: 130,
			//anchor: '95%',
			forceSelection: true,
			lazyRender:true,
			triggerAction: 'all',
			editable: false,
			store: new Ext.data.ArrayStore
			(
				{
				id: 0,
				fields:
				[
					'Id',
					'displayText'
				],
				   data: [[1, 'Perseorangan'], [2, 'Perusahaan'], [3, 'Asuransi']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetKelPasienLab,
			selectOnFocus: true,
			tabIndex:22,
			listeners:
			{
				'select': function(a, b, c)
					{
					   Combo_Select(b.data.displayText);
					   if(b.data.displayText == 'Perseorangan')
					   {

					   }
					   else if(b.data.displayText == 'Perusahaan')
					   {

					   }
					   else if(b.data.displayText == 'Asuransi')
					   {

					   }
					},
				'render': function(a,b,c)
				{
					Ext.getCmp('txtNamaPesertaAsuransiLab').hide();
					Ext.getCmp('txtNoAskesLab').hide();
					Ext.getCmp('txtNoSJPLab').hide();
					Ext.getCmp('cboPerseoranganLab').hide();
					Ext.getCmp('cboAsuransiLab').hide();
					Ext.getCmp('cboPerusahaanRequestEntryLab').hide();

				}
			}

		}
	);
	return cboKelPasienLab;
};

function mComboPerseorangan()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPeroranganLabRequestEntry = new WebApp.DataStore({fields: Field});
    dsPeroranganLabRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboKontrakCustomer',
			    param: 'customer.status=true and kontraktor.jenis_cust=0 order by customer.customer'
			}
		}
	)
    var cboPerseoranganLab = new Ext.form.ComboBox
	(
		{
			id:'cboPerseoranganLab',
			x: 280,
			y: 160,
			editable: false,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			fieldLabel: 'Jenis',
			tabIndex:23,
			width:150,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			store:dsPeroranganLabRequestEntry,
			value:selectSetPerseoranganLab,
			listeners:
			{
				'select': function(a,b,c)
				{
				    selectSetPerseoranganLab=b.data.displayText ;
				}
			}
		}
	);
	return cboPerseoranganLab;
};

function mComboPerusahaan()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboKontrakCustomer',
			    param: 'customer.status=true and kontraktor.jenis_cust=1 order by customer.customer'
			}
		}
	)
    var cboPerusahaanRequestEntryLab = new Ext.form.ComboBox
	(
		{
		    id: 'cboPerusahaanRequestEntryLab',
			x: 280,
			y: 160,
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    fieldLabel: 'Perusahaan',
		    align: 'Right',
			width:150,
		    store: dsPerusahaanRequestEntry,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
		    listeners:
			{
			    'select': function(a,b,c)
				{
					selectSetPerusahaanLab=b.data.valueField ;
				}
			}
		}
	);

    return cboPerusahaanRequestEntryLab;
};

function mComboAsuransi()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: "customer.status=true and kontraktor.jenis_cust=2 order by customer.customer"
            }
        }
    )
    var cboAsuransiLab = new Ext.form.ComboBox
	(
		{
			id:'cboAsuransiLab',
			x: 280,
			y: 160,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			fieldLabel: 'Asuransi',
			align: 'Right',
			width:150,
			//anchor: '95%',
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetAsuransiLab=b.data.valueField ;
				}
			}
		}
	);
	return cboAsuransiLab;
};


function Combo_Select(combo)
{
   var value = combo

   if(value == "Perseorangan")
   {
        Ext.getCmp('txtNamaPesertaAsuransiLab').hide()
        Ext.getCmp('txtNoAskesLab').hide()
        Ext.getCmp('txtNoSJPLab').hide()
        Ext.getCmp('cboPerseoranganLab').show()
        Ext.getCmp('cboAsuransiLab').hide()
        Ext.getCmp('cboPerusahaanRequestEntryLab').hide()


   }
   else if(value == "Perusahaan")
   {
        Ext.getCmp('txtNamaPesertaAsuransiLab').hide()
        Ext.getCmp('txtNoAskesLab').hide()
        Ext.getCmp('txtNoSJPLab').hide()
        Ext.getCmp('cboPerseoranganLab').hide()
        Ext.getCmp('cboAsuransiLab').hide()
        Ext.getCmp('cboPerusahaanRequestEntryLab').show()

   }
   else
       {
         Ext.getCmp('txtNamaPesertaAsuransiLab').show()
         Ext.getCmp('txtNoAskesLab').show()
         Ext.getCmp('txtNoSJPLab').show()
         Ext.getCmp('cboPerseoranganLab').hide()
         Ext.getCmp('cboAsuransiLab').show()
         Ext.getCmp('cboPerusahaanRequestEntryLab').hide()

       }
}



function addNewData(){
	TmpNotransaksi='';
	KdKasirAsal='';
	TglTransaksiAsal='';
	Kd_Spesial='';
	vkode_customer = '';
	No_Kamar='';
	TrviRencanaOperasi.vars.kd_tarif='';
	Ext.getCmp('btnSimpanviRencanaOperasi_kiriman').hide();
	Ext.getCmp('btnSimpanviRencanaOperasi').show();
	TrviRencanaOperasi.form.ComboBox.kdpasien.setValue('');
	Ext.getCmp('txtDokterPengirimL').setValue('');
	TrviRencanaOperasi.form.ComboBox.notransaksi.setValue('');
	Ext.getCmp('txtNamaUnitLab').setValue('');
	Ext.getCmp('txtKdUnitLab').setValue('');
	Ext.getCmp('txtKdDokter').setValue('');
	Ext.getCmp('cboDOKTER_viviRencanaOperasi').setValue('');
	Ext.getCmp('txtAlamatL').setValue('');
	TrviRencanaOperasi.form.ComboBox.pasien.setValue('');
	Ext.getCmp('dtpTtlL').setValue(nowTglTransaksi);
	Ext.getCmp('txtKdUrutMasuk').setValue('');
	Ext.getCmp('cboJKviRencanaOperasi').setValue('');
	Ext.getCmp('cboGDRviRencanaOperasi').setValue('')
	Ext.getCmp('txtKdCustomerLamaHide').setValue('');//tampung kd customer untuk transaksi selain kunjungan langsung
	Ext.getCmp('cboKelPasienLab').setValue('');
	//Ext.getCmp('txtCustomerLamaHide').setValue('');
	Ext.getCmp('cboAsuransiLab').setValue('');
	Ext.getCmp('txtNamaPesertaAsuransiLab').setValue('');
	Ext.getCmp('txtNoSJPLab').setValue('');
	Ext.getCmp('txtNoAskesLab').setValue('');
	Ext.getCmp('cboPerusahaanRequestEntryLab').setValue('');
	Ext.getCmp('txtKdDokterPengirim').setValue('');

	//Ext.getCmp('txtCustomerLamaHide').hide();
	Ext.getCmp('cboAsuransiLab').hide();
	Ext.getCmp('txtNamaPesertaAsuransiLab').hide();
	Ext.getCmp('txtNoSJPLab').hide();
	Ext.getCmp('txtNoAskesLab').hide();
	Ext.getCmp('cboPerusahaanRequestEntryLab').hide();
	Ext.getCmp('cboKelPasienLab').show();

	Ext.getCmp('btnLookupItemPemeriksaan').disable();
	Ext.getCmp('btnHpsBrsItemLabL').disable();

	TrviRencanaOperasi.form.ComboBox.notransaksi.enable();
	Ext.getCmp('txtDokterPengirimL').enable();
	Ext.getCmp('txtNamaUnitLab').enable();
	Ext.getCmp('cboDOKTER_viviRencanaOperasi').enable();
	TrviRencanaOperasi.form.ComboBox.pasien.enable();
	TrviRencanaOperasi.form.ComboBox.kdpasien.enable();
	Ext.getCmp('txtAlamatL').enable();
	Ext.getCmp('cboJKviRencanaOperasi').enable();
	Ext.getCmp('cboGDRviRencanaOperasi').enable();
	Ext.getCmp('cboKelPasienLab').enable();
	Ext.getCmp('dtpTtlL').enable();
	Ext.getCmp('dtpKunjunganL').enable();
	Ext.getCmp('btnSimpanviRencanaOperasi').enable();
	TrviRencanaOperasi.form.ComboBox.notransaksi.setReadOnly(false);
	Ext.getCmp('txtNamaUnitLab').setReadOnly(true);
	//Ext.getCmp('cboDOKTER_viviRencanaOperasi').setReadOnly(false);
	TrviRencanaOperasi.form.ComboBox.pasien.setReadOnly(false);
	Ext.getCmp('txtAlamatL').setReadOnly(false);
	Ext.getCmp('cboJKviRencanaOperasi').setReadOnly(false);
	Ext.getCmp('cboGDRviRencanaOperasi').setReadOnly(false);
	Ext.getCmp('cboKelPasienLab').setReadOnly(false);
	Ext.getCmp('dtpTtlL').setReadOnly(false);
	//Ext.getCmp('dtpKunjunganL').setReadOnly(true);
	gridDTRencanaOperasi.enable();
	//ViewGridBawahLookupviRencanaOperasi('');
	ViewGridBawahviRencanaOperasi('');
	var_tampung_lab_order=false;
}


function PilihDokterLookUp(NoTrans,TglTrans,kdPrdk,kdTrf,tglBerlaku,trf)
{


	RefreshDataTrDokter(NoTrans,TglTrans,kdPrdk);

	 var FormPilihDokter =
	  {
			layout: 'column',
						border: false,
						anchor: '90%',
						items:
						[

							{
								columnWidth: 0.18,
								layout: 'form',
								labelWidth:70,
								border: false,
								items:
								[
								{
								xtype:'button',
								text:'Simpan',
							 	width:70,
								hideLabel:true,
								id: 'BtnOktrDokter',
								handler:function()
								{
								if(dataTrDokter.length == '' || dataTrDokter.length== 'undefined')
								{
									var jumlah =0;
								}
								else
								{
									var jumlah = dataTrDokter.length;
								}
								for(var i = 0 ;i< dsTrDokter.getCount()  ;i++)
								{
								dataTrDokter.push({
								index		: i,
								no_tran 	: NoTrans,
								//urut		: Urt,
								tgl_trans 	: TglTrans,
								kd_produk 	: kdPrdk,
								kd_tarif	: kdTrf,
								tgl_berlaku	: tglBerlaku,
								tarif		: trf,
								kd_dokter 	: dsTrDokter.data.items[i].data.KD_DOKTER ,
								kd_job 		: dsTrDokter.data.items[i].data.KD_JOB ,
								});
								}
								FormLookUDokter.close();
								}
								},
								],
								},
								{
								columnWidth: 0.2,
								layout: 'form',
								labelWidth:70,
								border: false,
								items:
								[
								{
								xtype:'button',
								text:'Batal' ,
								width:70,
								hideLabel:true,
								id: 'BtnCancelGantiDokter',
								handler:function()
									{
								dataTrDokter = [];
								FormLookUDokter.close()
									}
								}
								]
								}


								]
  }

    var GridTrDokterColumnModel =  new Ext.grid.ColumnModel([
         new Ext.grid.RowNumberer(),
        {
        	 header			: 'kd_dokter',
        	 dataIndex		: 'KD_DOKTER',
			 width			: 80,
        	 menuDisabled	: true,
        	 hidden 		: false
        },{
            header			:'Nama Dokter',
            dataIndex		: 'NAMA',
            sortable		: false,
            hidden			: false,
			menuDisabled	: true,
			width			: 200,
            editor			: getTrDokter(dsTrDokter)
	    },{
            header			: 'Job',
            dataIndex		: 'KD_JOB',
            width			:150,
		   	menuDisabled	:true,
			editor			: JobDokter(),

        },
        ]
    );

  var GridPilihDokter= new Ext.grid.EditorGridPanel({
        title		: '',
		id			: 'GridDokterTr',
		stripeRows	: true,
		width		: 490,
		height		: 245,
        store		: RefreshDataTrDokter(NoTrans,TglTrans,kdPrdk),
        border		: false,
        frame		: false,
        anchor		: '100% 60%',
        autoScroll	: true,
        cm			: GridTrDokterColumnModel,

        //viewConfig	: {forceFit: true},
		listeners	: {
			rowclick: function( $this, rowIndex, e )
			{
        	trcellCurrentTindakan = rowIndex;
    		},
			celldblclick: function(gridView,htmlElement,columnIndex,dataRecord){
   		  }
		}
    });



	var PanelTrDokter = new Ext.Panel
	({
            id: 'PanelPilitTrDokter',
            region: 'center',
			width: 490,
			height: 260,
            border:false,
            layout: 'form',
            frame:true,
            anchor: '100% 8.0001%',
            autoScroll:false,

            items:
            [
			GridPilihDokter,
			{xtype: 'tbspacer',height: 3, width:100},
			FormPilihDokter
			]
	})


    var lebar = 500;
    var FormLookUDokter = new Ext.Window
    (
        {
            id: 'gridTrDokter',
            title: 'Pilih Dokter Tindakan',
            closeAction: 'destroy',
            width: lebar,
            height: 270,
            border: false,
            resizable: false,
            plain: true,
            layout: 'column',
            iconCls: 'Request',
            modal: true,
			tbar		:[
						{
						xtype	: 'button',
						id		: 'btnaddtrdokter',
						iconCls	: 'add',
						text	: 'Tambah Data Dokter',
						handler	:  function()
						{
						dataTrDokter = [];
						TambahBaristrDokter(dsTrDokter);
						}
						},
						'-',
						{
						xtype	: 'button',
						id		: 'btndeltrdokter',
						iconCls	: 'remove',
						text	: 'Delete Data Dokter',
						handler	: function()
						{
						    if (dsTrDokter.getCount() > 0 )
							{
                            if(trcellCurrentTindakan != undefined)
							{
                             HapusDataTrDokter(dsTrDokter);
                            }
                        	}else{
                            ShowPesanWarningDiagnosa('Pilih record ','Hapus data');
                    		}
							}
						},
						'-',
			],
           	items: [
			//GridPilihDokter,

			PanelTrDokter
			],
            listeners:
            {
            }
        }
    );


 FormLookUDokter.show();




};

var mtrDataDokter = Ext.data.Record.create([
	   {name: 'KD_DOKTER', mapping:'KD_DOKTER'},
	   {name: 'NAMA', mapping:'NAMA'},
	   {name: 'KD_JOB', mapping:'KD_JOB'},
	]);

function TambahBaristrDokter(store){
    var x=true;
    if (x === true) {
        var p = BaristrDokter();
        store.insert(store.getCount(), p);
    }
}

function BaristrDokter(){
	var p = new mtrDataDokter({
		'KD_DOKTER':'',
		'NAMA':'',
	    'KD_JOB':'',
	});
	return p;
};

function getTrDokter(store){
	var trDokterData = new Ext.form.ComboBox({
	   typeAhead: true,
	   triggerAction: 'all',
	   lazyRender: true,
	   mode: 'local',
	   hideTrigger:true,
	   forceSelection: true,
	   selectOnFocus:true,
	   store: GetDokter(),
	   valueField: 'NAMA',
	   displayField: 'NAMA',
	   anchor: '95%',
	   listeners:{
		   select: function(a, b, c){
			  store.data.items[trcellCurrentTindakan].data.KD_DOKTER = b.data.KD_DOKTER;
		   }
	   }
	});
	return trDokterData;
}

function GetDokter(){
	var dataDokter  = new WebApp.DataStore({ fields: ['KD_DOKTER','NAMA'] });
	dataDokter.load({
		params: {
			Skip: 0,
			Take: 50,
			target:'ViewComboDokter',
			param: ''
		}
	});
	return dataDokter;
}

function JobDokter(){
	var combojobdokter = new Ext.form.ComboBox({
		typeAhead		: true,
		triggerAction	: 'all',
		lazyRender		: true,
		mode			: 'local',
		anchor 			: '96.8%',
		emptyText		: '',
		store			: new Ext.data.ArrayStore({
			fields	: ['Id','displayText'],
			data	: [[1, 'Dokter'],[2, 'Dokter Anastesi']]
		}),
		valueField		: 'displayText',
		displayField	: 'displayText'
	});
	return combojobdokter;
};


function RefreshDataTrDokter(NoTrans,TglTrans,kdPrdk)
{
	dsTrDokter.load({
		params	: {
			Skip	: 0,
			Take	: 50,
			target	:'ViewTrDokter',
			param	:"b.kd_kasir='06' and b.no_transaksi=~"+NoTrans+"~ and b.tgl_transaksi=~"+TglTrans+"~ and a.kd_produk = ~"+kdPrdk+"~"
		}
	});

	return dsTrDokter;
}

function HapusDataTrDokter(store)
{

    if ( trcellCurrentTindakan != undefined ){
        if (store.data.items[trcellCurrentTindakan].data.KD_DOKTER != '' && store.data.items[trcellCurrentTindakan].data.KD_PRODUK != ''){
					DataDeleteTrDokter(store);
					dsTrDokter.removeAt(trcellCurrentTindakan);
				}

        }/*else{
            dsTrDokter.removeAt(trcellCurrentTindakan);
        }*/


}

function DataDeleteTrDokter(store){
    Ext.Ajax.request({
        url: baseURL + "index.php/main/DeleteDataObj",
        params:
		{
			Table			:'ViewTrDokter',
			no_transaksi	:Ext.getCmp('txtNoTransaksiKasirIGD').getValue(),
			kd_unit			:Ext.getCmp('txtKdUnitIGD').getValue(),
			kd_produk		:store.data.items[trcellCurrentTindakan].data.KD_PRODUK,
			kd_kasir		:'06',
			kd_dokter		:store.data.items[trcellCurrentTindakan].data.KD_DOKTER,
			urut			:JwdKamarOperasiRJ.dsGridTindakan.data.items[cellCurrentTindakan].data.URUT,
			tgl_transaksi	:JwdKamarOperasiRJ.dsGridTindakan.data.items[cellCurrentTindakan].data.TGL_TRANSAKSI,
		},
		//getParamDataDeleteKasirIGDDetail(),
        success: function(o){
            var cst = Ext.decode(o.responseText);
            if (cst.success === true){
                ShowPesanInfoDiagnosa(nmPesanHapusSukses,nmHeaderHapusData);
                store.removeAt(CurrentKasirIGD.row);
				RefreshDataKasirIGDDetail(Ext.getCmp('txtNoTransaksiKasirIGD').getValue());
                RefreshDataTrDokter(Ext.getCmp('txtNoTransaksiKasirIGD').getValue(),JwdKamarOperasiRJ.dsGridTindakan.data.items[cellCurrentTindakan].data.URUT,JwdKamarOperasiRJ.dsGridTindakan.data.items[cellCurrentTindakan].data.TGL_TRANSAKSI,JwdKamarOperasiRJ.dsGridTindakan.data.items[cellCurrentTindakan].data.KD_PRODUK);
				trcellCurrentTindakan=undefined;
            }else{
                ShowPesanInfoDiagnosa(nmPesanHapusError,nmHeaderHapusData);
				RefreshDataKasirIGDDetail(Ext.getCmp('txtNoTransaksiKasirIGD').getValue());
				RefreshDataTrDokter(Ext.getCmp('txtNoTransaksiKasirIGD').getValue(),JwdKamarOperasiRJ.dsGridTindakan.data.items[cellCurrentTindakan].data.URUT,JwdKamarOperasiRJ.dsGridTindakan.data.items[cellCurrentTindakan].data.TGL_TRANSAKSI,JwdKamarOperasiRJ.dsGridTindakan.data.items[cellCurrentTindakan].data.KD_PRODUK);
            }
        }
    });
}

