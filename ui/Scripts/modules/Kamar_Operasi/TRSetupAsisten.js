var dataSource_viSetupAsistenOperasi;
var selectCount_viSetupAsistenOperasi = 50;
var NamaForm_viSetupAsistenOperasi = "Setup Asisten Operasi";
var mod_name_viSetupAsistenOperasi = "Setup Asisten Operasi";
var now_viSetupAsistenOperasi = new Date();
var rowSelected_viSetupAsistenOperasi;
var setLookUps_viSetupAsistenOperasi;
var tanggal = now_viSetupAsistenOperasi.format("d/M/Y");
var jam = now_viSetupAsistenOperasi.format("H/i/s");
var tmpkriteria;
var DataGridJenisObat;
var GridDataView_viSetupAsistenOperasi;
var a;


var CurrentData_viSetupAsistenOperasi = {
	data: Object,
	details: Array,
	row: 0
};

var SetupAsistenOperasi = {};
SetupAsistenOperasi.form = {};
SetupAsistenOperasi.func = {};
SetupAsistenOperasi.vars = {};
SetupAsistenOperasi.func.parent = SetupAsistenOperasi;
SetupAsistenOperasi.form.ArrayStore = {};
SetupAsistenOperasi.form.ComboBox = {};
SetupAsistenOperasi.form.DataStore = {};
SetupAsistenOperasi.form.Record = {};
SetupAsistenOperasi.form.Form = {};
SetupAsistenOperasi.form.Grid = {};
SetupAsistenOperasi.form.Panel = {};
SetupAsistenOperasi.form.TextField = {};
SetupAsistenOperasi.form.Button = {};

SetupAsistenOperasi.form.ArrayStore.a = new Ext.data.ArrayStore({
	id: 0,
	fields: ['kd_asisten', 'nama'],
	data: []
});

CurrentPage.page = dataGrid_viSetupAsistenOperasi(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dataGrid_viSetupAsistenOperasi(mod_id_viSetupAsistenOperasi) {
	// Field kiriman dari Project Net.
	var FieldMaster_viSetupAsistenOperasi = [
		'kd_asisten', 'nama'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
	dataSource_viSetupAsistenOperasi = new WebApp.DataStore({
		fields: FieldMaster_viSetupAsistenOperasi
	});
	dataGriSetupAsistenOperasi();
	// Grid lab Perencanaan # --------------
	GridDataView_viSetupAsistenOperasi = new Ext.grid.EditorGridPanel({
		xtype: 'editorgrid',
		title: '',
		store: dataSource_viSetupAsistenOperasi,
		autoScroll: false,
		columnLines: true,
		flex: 1,
		border: true,
		anchor: '100% 100%',
		plugins: [new Ext.ux.grid.FilterRow()],
		selModel: new Ext.grid.RowSelectionModel
		// Tanda aktif saat salah satu baris dipilih # --------------
		({
			singleSelect: true,
			listeners: {
				rowselect: function (sm, row, rec) {
					rowSelected_viSetupAsistenOperasi = undefined;
					rowSelected_viSetupAsistenOperasi = dataSource_viSetupAsistenOperasi.getAt(row);
					CurrentData_viSetupAsistenOperasi
					CurrentData_viSetupAsistenOperasi.row = row;
					CurrentData_viSetupAsistenOperasi.data = rowSelected_viSetupAsistenOperasi.data;
					//DataInitSetupAsistenOperasi(rowSelected_viSetupAsistenOperasi.data);
					//DataInitSetupAsistenOperasi(rowSelected_viSetupAsistenOperasi.data);
				}
			}
		}),
		// Proses eksekusi baris yang dipilih # --------------
		listeners: {
			// Function saat ada event double klik maka akan muncul form view # --------------
			rowdblclick: function (sm, ridx, cidx) {
				rowSelected_viSetupAsistenOperasi = dataSource_viSetupAsistenOperasi.getAt(ridx);
				if (rowSelected_viSetupAsistenOperasi != undefined) {
					DataInitSetupAsistenOperasi(rowSelected_viSetupAsistenOperasi.data);
					//setLookUp_viSetupAsistenOperasi(rowSelected_viSetupAsistenOperasi.data);
				} else {
					//setLookUp_viSetupAsistenOperasi();
				}
			}
			// End Function # --------------
		},
		/**
		 *	Mengatur tampilan pada Grid Setup Unit
		 *	Terdiri dari : Judul, Isi dan Event
		 *	Isi pada Grid di dapat dari pemangilan dari Net.
		 */
		colModel: new Ext.grid.ColumnModel(
			[
				new Ext.grid.RowNumberer(),
				{
					header: 'Kode Asisten',
					dataIndex: 'kd_asisten',
					hideable: false,
					menuDisabled: true,
					width: 40

				},
				//-------------- ## --------------
				{
					header: 'Nama',
					dataIndex: 'nama',
					hideable: false,
					menuDisabled: true,
					width: 90
				}
				//-------------- ## --------------
			]
		),
		//  tbar: 
		// {
		// 	xtype: 'toolbar',
		// 	id: 'toolbar_viSetupAsistenOperasi',
		// 	items: 
		// 	[
		// 		{
		// 			xtype: 'button',
		// 			text: 'Edit Asisten',
		// 			iconCls: 'Edit_Tr',
		// 			tooltip: 'Edit Data',
		// 			id: 'btnEdit_viSetupAsistenOperasi',
		// 			handler: function(sm, row, rec)
		// 			{
		// 				if (rowSelected_viSetupAsistenOperasi != undefined)
		// 				{
		// 					DataInitSetupAsistenOperasi(rowSelected_viSetupAsistenOperasi.data);
		// 				} 
		// 			}
		// 		}
		// 		//-------------- ## --------------
		// 	]
		// }, 
		// End Tolbar ke Dua # --------------
		// Button Bar Pagging # --------------
		// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
		//bbar : bbar_paging(mod_name_viSetupAsistenOperasi, selectCount_viSetupAsistenOperasi, dataSource_viSetupAsistenOperasi),
		// End Button Bar Pagging # --------------
		viewConfig: {
			forceFit: true
		}
	})
	var PanelTabSetupAsistenOperasi = new Ext.FormPanel({
		labelAlign: 'top',
		layout: 'form',
		border: false,
		autoHeight: true,
		title: '',
		bodyStyle: 'padding:5px 5px 5px 5px',
		items: [PanelInputUnit()]

	})
	// Kriteria filter pada Grid # --------------
	var FrmData_viSetupAsistenOperasi = new Ext.Panel({
		title: NamaForm_viSetupAsistenOperasi,
		iconCls: 'Studi_Lanjut',
		id: mod_id_viSetupAsistenOperasi,
		//region: 'center',
		layout: {
			type: 'vbox',
			align: 'stretch'

		},
		closable: true,
		autoScroll: false,
		border: false,
		margins: '0 5 5 0',
		items: [PanelTabSetupAsistenOperasi,
			GridDataView_viSetupAsistenOperasi
		],
		tbar: {
			xtype: 'toolbar',
			items: [{
					xtype: 'button',
					text: 'Add New',
					iconCls: 'add',
					id: 'btnAddAsisten_viSetupAsistenOperasi',
					handler: function () {
						AddNewSetupAsistenOperasi();
					}
				},
				{
					xtype: 'tbseparator'
				},
				{
					xtype: 'button',
					text: 'Save',
					iconCls: 'save',
					id: 'btnSimpan_viSetupAsistenOperasi',
					handler: function () {
						loadMask.show();
						dataSave_viSetupAsistenOperasi();
					}
				},
				{
					xtype: 'tbseparator'
				},
				{
					xtype: 'button',
					text: 'Delete',
					iconCls: 'remove',
					id: 'btnDelete_viSetupAsistenOperasi',
					handler: function () {
						Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function (button) {
							if (button == 'yes') {
								loadMask.show();
								dataDelete_viSetupAsistenOperasi();
							}
						});
					}
				},
				{
					xtype: 'tbseparator'
				},
				{
					xtype: 'button',
					text: 'Refresh',
					iconCls: 'refresh',
					id: 'btnRefresh_viSetupPabrik',
					handler: function () {
						dataSource_viSetupAsistenOperasi.removeAll();
						dataGriSetupAsistenOperasi();
					}
				},
				{
					xtype: 'tbseparator'
				}

			]
		}
		//-------------- # End tbar # --------------
	})
	return FrmData_viSetupAsistenOperasi;
	//-------------- # End form filter # --------------
}

function PanelInputUnit(rowdata) {
	var items = {
		layout: 'form',
		border: true,
		bodyStyle: 'padding: 5px',
		items: [{
			layout: 'column',
			border: false,
			items: [{
				columnWidth: .98,
				layout: 'absolute',
				bodyStyle: 'padding: 10px 10px 10px 10px',
				border: false,
				width: 500,
				height: 80,
				anchor: '100% 100%',
				items: [{
						x: 10,
						y: 0,
						xtype: 'label',
						text: 'Kode asisten'
					},
					{
						x: 120,
						y: 0,
						xtype: 'label',
						text: ':'
					},
					{
						x: 130,
						y: 0,
						xtype: 'textfield',
						id: 'txtKdAsisten_SetupAsistenOperasi',
						name: 'txtKdAsisten_SetupAsistenOperasi',
						width: 100,
						allowBlank: false,
						readOnly: true,
						disabled: true,
						tabIndex: 1,
						listeners: {
							'specialkey': function () {
								if (Ext.EventObject.getKey() === 13) {

								}
							},

						}
					},
					{
						x: 10,
						y: 30,
						xtype: 'label',
						text: 'Nama'
					},
					{
						x: 120,
						y: 30,
						xtype: 'label',
						text: ':'
					},
					SetupAsistenOperasi.vars.nama = new Nci.form.Combobox.autoComplete({
						x: 130,
						y: 30,
						tabIndex: 2,
						id: 'txtComboAsisten_SetupAsistenOperasi',
						store: SetupAsistenOperasi.form.ArrayStore.a,
						select: function (a, b, c) {

							Ext.getCmp('txtKdAsisten_SetupAsistenOperasi').setValue(b.data.kd_asisten);

							GridDataView_viSetupAsistenOperasi.getView().refresh();

						},
						// onShowList: function (a) {
						// 	dataSource_viSetupAsistenOperasi.removeAll();

						// 	var recs = [],
						// 		recType = dataSource_viSetupAsistenOperasi.recordType;

						// 	for (var i = 0; i < a.length; i++) {
						// 		recs.push(new recType(a[i]));
						// 	}
						// 	dataSource_viSetupAsistenOperasi.add(recs);

						// },
						// insert: function (o) {
						// 	return {
						// 		kd_asisten: o.kd_asisten,
						// 		nama: o.nama,
						// 		text: '<table style="font-size: 11px;"><tr><td width="70">' + o.kd_asisten + '</td><td width="200">' + o.nama + '</td></tr></table>'
						// 	}
						// },
						// url: baseURL + "index.php/kamar_operasi/functionSetupAsistenOperasi/getItemGrid",
						// valueField: 'nama',
						// displayField: 'text',
						// listWidth: 280

						// y auto complete nama asisten
						insert: function (o) {
							return {
								kd_dokter: o.kd_dokter,
								nama: o.nama,
								text: '<table style="font-size: 11px;"><tr><td width="70">' + o.kd_dokter + '</td><td width="200">' + o.nama + '</td></tr></table>'
							}
						},
						url: baseURL + "index.php/kamar_operasi/functionSetupAsistenOperasi/getItemNama",
						valueField: 'nama',
						displayField: 'text',
						listWidth: 280
						// end y auto complete nama asisten

					}),
					{
						x: 10,
						y: 60,
						xtype: 'label',
						text: 'Aktif'
					},
					{
						x: 120,
						y: 60,
						xtype: 'label',
						text: ':'
					},
					{
						xtype: 'checkbox',
						id: 'CbAktif_SetupAsistenOperasi',
						hideLabel: false,
						checked: true,
						x: 130,
						y: 60,
						listeners: {
							check: function () {

								//cek=1;
							}
						}
					},


				]
			}]
		}]
	};
	return items;
}
//------------------end---------------------------------------------------------

function dataGriSetupAsistenOperasi() {
	Ext.Ajax.request({
			url: baseURL + "index.php/kamar_operasi/functionSetupAsistenOperasi/getItemGrid",
			params: {
				text: ''
			},
			failure: function (o) {
				ShowPesanErrorSetupAsistenOperasi('Hubungi Admin', 'Error');
			},
			success: function (o) {
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) {

					var recs = [],
						recType = dataSource_viSetupAsistenOperasi.recordType;

					for (var i = 0; i < cst.listData.length; i++) {

						recs.push(new recType(cst.listData[i]));

					}
					dataSource_viSetupAsistenOperasi.add(recs);



					GridDataView_viSetupAsistenOperasi.getView().refresh();
				} else {
					ShowPesanErrorSetupAsistenOperasi('Gagal membaca data items', 'Error');
				};
			}
		}

	)

}

function dataSave_viSetupAsistenOperasi() {
	if (ValidasiSaveSetupAsistenOperasi(nmHeaderSimpanData, false) == 1) {
		Ext.Ajax.request({
				url: baseURL + "index.php/kamar_operasi/functionSetupAsistenOperasi/save",
				params: getParamSaveSetupAsistenOperasi(),
				failure: function (o) {
					loadMask.hide();
					ShowPesanErrorSetupAsistenOperasi('Hubungi Admin', 'Error');
				},
				success: function (o) {
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) {
						loadMask.hide();
						ShowPesanInfoSetupAsistenOperasi('Berhasil menyimpan data ini', 'Information');
						Ext.getCmp('txtKdAsisten_SetupAsistenOperasi').setValue(cst.kode);
						dataSource_viSetupAsistenOperasi.removeAll();
						dataGriSetupAsistenOperasi();

					} else {
						loadMask.hide();
						ShowPesanErrorSetupAsistenOperasi('Gagal menyimpan data ini', 'Error');
						dataSource_viSetupAsistenOperasi.removeAll();
						dataGriSetupAsistenOperasi();
					};
				}
			}

		)
	}
}

function dataDelete_viSetupAsistenOperasi() {
	if (ValidasiSaveSetupAsistenOperasi(nmHeaderSimpanData, false) == 1) {
		Ext.Ajax.request({
				url: baseURL + "index.php/kamar_operasi/functionSetupAsistenOperasi/delete",
				params: getParamDeleteSetupAsistenOperasi(),
				failure: function (o) {
					loadMask.hide();
					ShowPesanErrorSetupAsistenOperasi('Hubungi Admin', 'Error');
				},
				success: function (o) {
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) {
						loadMask.hide();
						ShowPesanInfoSetupAsistenOperasi('Berhasil menghapus data ini', 'Information');
						AddNewSetupAsistenOperasi()
						dataSource_viSetupAsistenOperasi.removeAll();
						dataGriSetupAsistenOperasi();

					} else {
						loadMask.hide();
						ShowPesanErrorSetupAsistenOperasi('Gagal menghapus data ini', 'Error');
						dataSource_viSetupAsistenOperasi.removeAll();
						dataGriSetupAsistenOperasi();
					};
				}
			}

		)
	}
}

function AddNewSetupAsistenOperasi() {
	Ext.getCmp('txtKdAsisten_SetupAsistenOperasi').setValue('');
	SetupAsistenOperasi.vars.nama.setValue('');
	SetupAsistenOperasi.vars.nama.focus();
	//code assiten here
	//Ext.getCmp('cboSpesialis_ass_job').setValue('');
	Aktif: Ext.getCmp('CbAktif_SetupAsistenOperasi').setValue(true);
};

function DataInitSetupAsistenOperasi(rowdata) {
	Ext.getCmp('txtKdAsisten_SetupAsistenOperasi').setValue(rowdata.kd_asisten);
	SetupAsistenOperasi.vars.nama.setValue(rowdata.nama);
	//code assiten here
	//Ext.getCmp('cboSpesialis_ass_job').setValue(rowdata.);
	Ext.getCmp('CbAktif_SetupAsistenOperasi').setValue(rowdata.aktif);
};

function getParamSaveSetupAsistenOperasi() {
	var params = {
		KdAsisten: Ext.getCmp('txtKdAsisten_SetupAsistenOperasi').getValue(),
		Nama: SetupAsistenOperasi.vars.nama.getValue(),
		//code assiten here
		Aktif: Ext.getCmp('CbAktif_SetupAsistenOperasi').getValue()
	}

	return params
};

function getParamDeleteSetupAsistenOperasi() {
	var params = {
		KdAsisten: Ext.getCmp('txtKdAsisten_SetupAsistenOperasi').getValue()
	}

	return params
};

function ValidasiSaveSetupAsistenOperasi(modul, mBolHapus) {
	var x = 1;
	if (SetupAsistenOperasi.vars.nama.getValue() === '') {
		if (SetupAsistenOperasi.vars.nama.getValue() === '') {
			loadMask.hide();
			ShowPesanWarningSetupAsistenOperasi('nama masih kosong', 'Warning');
			x = 0;
		}

	}
	return x;
};



function ShowPesanWarningSetupAsistenOperasi(str, modul) {
	Ext.MessageBox.show({
		title: modul,
		msg: str,
		buttons: Ext.MessageBox.OK,
		icon: Ext.MessageBox.WARNING,
		width: 250
	});
};

function ShowPesanErrorSetupAsistenOperasi(str, modul) {
	Ext.MessageBox.show({
		title: modul,
		msg: str,
		buttons: Ext.MessageBox.OK,
		icon: Ext.MessageBox.ERROR,
		width: 250
	});
};

function ShowPesanInfoSetupAsistenOperasi(str, modul) {
	Ext.MessageBox.show({
		title: modul,
		msg: str,
		buttons: Ext.MessageBox.OK,
		icon: Ext.MessageBox.INFO,
		width: 250
	});
};