var dsvDokter;
var dsvDokter;
var rowselectedDokter='';
var LookupDokter;
var disDataDokter=0;
var cek=0;
var kdSetupDokter;
var kdDokterDokter;
var kodeDokter;
var stateComboDokter=['KD_Dokter_Dokter','Dokter_Dokter'];
var pilihAksiDokter;
var dsvDoubleGridDokterSetupOperasiOK;
var secondGridStore;
var sendDataArray=[];
var kodeDokterSimpan='';
var namaDokterSimpan='';
var dsvGridDokterInap;
var secondGrid;
var firstgridSetupDok;
var chkdokluar;
var SetupDokter={};
SetupDokter.form={};
SetupDokter.func={};
SetupDokter.vars={};
SetupDokter.func.parent=SetupDokter;
SetupDokter.form.ArrayStore={};
SetupDokter.form.ComboBox={};
SetupDokter.form.DataStore={};
SetupDokter.form.Record={};
SetupDokter.form.Form={};
SetupDokter.form.Grid={};
SetupDokter.form.Panel={};
SetupDokter.form.TextField={};
SetupDokter.form.Button={};

SetupDokter.form.ArrayStore.jenisdietDokter=new Ext.data.ArrayStore({
		id: 0,
		fields: [	
					'kd_jenis','jenis_diet','harga_pokok'
				],
		data: []
	});
	
CurrentPage.page = getPanelDokter(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dsGridDoubleGridDokterSetupOperasiOK(){
	dsvDoubleGridDokterSetupOperasiOK.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'vGridDokterSetupOK',
                }			
            }
        );   
    return dsvDoubleGridDokterSetupOperasiOK;
}
function dsGridDokterInapOperasi(namaDokter){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/kamar_operasi/functionSetupDokter/getDokterInap",
			params: {
						kdunit:"ok_default_kamar",
						nama:namaDokter
					},
			failure: function(o)
			{
				ShowPesanErrorDokter('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					for(var i=0; i<cst.listData.length; i++){
						
						var recs    = [],recType = secondGridStore.recordType;
						var o=cst.listData[i];
						recs    = [];
						recs.push(new recType(o));
						
						secondGridStore.add(recs);
					}
					
				}
				else 
				{
					ShowPesanErrorDokter('Gagal membaca data', 'Error');
				};
			}
		}
		
	)
	
}
function dsGridDokterAllOperasi(namaDokter){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/kamar_operasi/functionSetupDokter/getDokterAll",
			params: {
						nama:namaDokter
					},
			failure: function(o)
			{
				ShowPesanErrorDokter('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					for(var i=0; i<cst.listData.length; i++){
						
						var recs    = [],recType = dsvDoubleGridDokterSetupOperasiOK.recordType;
						var o=cst.listData[i];
						recs    = [];
						recs.push(new recType(o));
						
						dsvDoubleGridDokterSetupOperasiOK.add(recs);
					}
					
				}
				else 
				{
					ShowPesanErrorDokter('Gagal membaca data', 'Error');
				};
			}
		}
		
	)
	
}
function loadDataKamarOperasi_SetupDokter(){
	
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/kamar_operasi/functionSetupDokter/getDefaultKamar",
			params: {text:''
					},
			failure: function(o)
			{
				ShowPesanErrorDokter('Error, pasien! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				
				if (cst.success === true) 
				{
					dsKamarOperasi_SetupDokter.load({
						params:{
							Skip: 0,
							Take: 1000,
							Sort: 'nama_unit',
							Sortdir: 'ASC',
							target: 'ViewSetupUnit',
							param: "parent = '71' and kd_unit in ("+cst.kamar_ok+")"
						}
					});
				}
				else 
				{
					ShowPesanErrorDokter('Gagal membaca data pasien ini', 'Error');
				};
			}
		}
		
	)
	
	return dsKamarOperasi_SetupDokter
}
function mComboKamarOperasi_SetupDokter(){
	var Field = ['KD_UNIT','NAMA_UNIT'];
    dsKamarOperasi_SetupDokter = new WebApp.DataStore({ fields: Field });
    loadDataKamarOperasi_SetupDokter();
    var cbounitKamarOperasi_SetupDokter = new Ext.form.ComboBox({
		id: 'cboKamarOperasi_SetupDokter',
		
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		//disabled:true,
		fieldLabel:  ' ',
		align: 'Right',
		width: 100,
		emptyText:'Pilih Kamar',
		store: dsKamarOperasi_SetupDokter,
		valueField: 'KD_UNIT',
		displayField: 'NAMA_UNIT',
		//value:'All',
		editable: false,
		listeners:{
			'select': function(a,b,c){
				
			}
		}
	});
    return cbounitKamarOperasi_SetupDokter;
	
};
function gridDataViewEditDoubleGridDokter_SetupOperasi_Ok(mod_id){
	
    var FieldGrdDoubleGridDokter_SetupOperasi_Ok = ['kd_dokter','nama'];
    dsvDoubleGridDokterSetupOperasiOK= new WebApp.DataStore({
        fields: FieldGrdDoubleGridDokter_SetupOperasi_Ok
    });
	dsGridDoubleGridDokterSetupOperasiOK();
 	var fields = [
		{name: 'kd_dokter', mapping : 'kd_dokter'},
		{name: 'nama', mapping : 'nama'}
	];
	
	
	 	firstgridSetupDok = new Ext.grid.GridPanel({
            ddGroup          : 'secondGridDDGroup',
            store            : dsvDoubleGridDokterSetupOperasiOK,
            autoScroll       : true,
            columnLines      : true,
            border           : true,
            enableDragDrop   : true,
            height           : 450,
            stripeRows       : true,
            trackMouseOver   : true,
            //title            : 'Daftar No Reg Barang',
            anchor           : '100% 100%',
            plugins          : [new Ext.ux.grid.FilterRow()],
            colModel         : new Ext.grid.ColumnModel
                            (
                                    [
                                            new Ext.grid.RowNumberer(),
                                            {
                                                    id: 'colNamaDok_DoubleGridDokterJwdOpOk',
                                                    header: 'Nama dokter',
                                                    dataIndex: 'nama',
                                                    sortable: true,
                                                    width: 20
                                            }
                                    ]
                                ),
   			  listeners : {
                    afterrender : function(comp) {
                    var firstgridSetupDokDropTargetEl = firstgridSetupDok.getView().scroller.dom;
                    var firstgridSetupDokDropTarget = new Ext.dd.DropTarget(firstgridSetupDokDropTargetEl, {
                            ddGroup    : 'firstgridSetupDokDDGroup',
                            notifyDrop : function(ddSource, e, data){
                                    var records =  ddSource.dragData.selections;
                                    Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
                                    firstgridSetupDok.store.add(records);
                                    firstgridSetupDok.store.sort('kd_dokter', 'ASC');
                                    return true
                            }
                    });
                    }
                },
             viewConfig: 
                    {
                            forceFit: true
                    }
        });

       secondGridStore = new Ext.data.JsonStore({
            fields : FieldGrdDoubleGridDokter_SetupOperasi_Ok,
            //root   : 'records'
        }); 
		chkdokluar = new Ext.grid.CheckColumn
		(
			{
				id: 'chkdokluar',
				header: 'Dokter Luar',
				align: 'center',
				disabled:false,
				sortable: true,
				dataIndex: 'dokter_luar',
				anchor: '10% 100%',
				width: 30,
				/* renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                    switch (value) {
                        case 't':
                            return true;
							
                            break;
                        case 'f':
                            return false;
                            break;
                    }
                    return false;
                }  */
		
			}
		); 
        // create the destination Grid
        secondGrid = new Ext.grid.EditorGridPanel({
                    ddGroup          : 'firstgridSetupDokDDGroup',
                    store            : secondGridStore,
                    columns          : [
											new Ext.grid.RowNumberer(),
											{header: "Kode dokter", width: 100, sortable: true, dataIndex: 'kd_dokter',hidden:true},
											{header: "Nama dokter", width: 100, sortable: true, dataIndex: 'nama'},
											chkdokluar,
									   ],
                    enableDragDrop   : true,
                    height           : 450,
                    stripeRows       : true,
					plugins: [new Ext.ux.grid.FilterRow(),chkdokluar],
                    autoExpandColumn : 'nama',
                   // title            : 'Daftar Pilihan No Reg Barang',
                    listeners : {
                    afterrender : function(comp) {
						var secondGridDropTargetEl = secondGrid.getView().scroller.dom;
						var secondGridDropTarget = new Ext.dd.DropTarget(secondGridDropTargetEl, {
								ddGroup    : 'secondGridDDGroup',
								notifyDrop : function(ddSource, e, data){
										var records =  ddSource.dragData.selections;
										Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
										//console.log(data.selections[0].data.nama);
										secondGrid.store.add(records);
										kodeDokterSimpan=data.selections[0].data.kd_dokter;
										namaDokterSimpan=data.selections[0].data.nama;
										data.selections[0].data.dokter_luar=false;
										//secondGrid.store.sort('kd_dokter', 'ASC');
										return true
										
								}
						});
						},
					rowclick: function (sm, row, rec) {
						
						rowselectedDokter=secondGridStore.getAt(row);
					}
                },
                viewConfig: 
                    {
                            forceFit: true
                    }
        });
		
		var FrmTabs_SetupOpOk = new Ext.Panel
        (
		{
		    id: 'formTab',
		    region: 'center',
		    layout: 'column',
            height       : 550,
			
			//title:  'Set dokter',
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding: 0px 0px 0px 0px',
		    border: false,
//		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
			width: '100%',
		    anchor: '99%',
		    //iconCls: icons_viInformasiUnitdokter,
		    items: 
			[
				{
					columnWidth: .49,
					layout: 'form',
					border: true,
					title:'Dokter Inap',
					autoScroll: true,
					bodyStyle: 'padding: 10px 10px 10px 10px',
					items:
					[secondGrid
						
					],
					tbar :[
						{
							xtype: 'label',
							text: 'Nama Dokter: ' ,
							
						},
						{xtype: 'tbspacer',height: 3, width:5},
						 {
							x: 40,
							y: 40,
							xtype: 'textfield',
							//fieldLabel: 'No. Medrec',
							name: 'txtcaridokterinapall',
							id: 'txtcaridokterinapall',
							width: 150,
							listeners:
							{
								'specialkey': function ()
									{
										if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
										{
											secondGridStore.removeAll();
											dsGridDokterInapOperasi(Ext.getCmp('txtcaridokterinapall').getValue());
										}
									}
							}
						},
						/* {
							text: 'Cari',
							id: 'btnCariDokterInapOperasi',
							iconCls: 'find',
							handler: function()
							{
								secondGridStore.removeAll();
								dsGridDokterInapOperasi(Ext.getCmp('txtcaridokterinapall').getValue());
							}
						}, */
						{
							xtype: 'label',
							text: 'Kamar: ' ,
							
						},
						mComboKamarOperasi_SetupDokter()
					]
				},
				{
					columnWidth: .50,
					layout: 'form',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: true,
					title:'Semua Dokter',
					anchor: '100% 100%',
					items:
					[
						firstgridSetupDok,
						
						
						
					],
					tbar :[
						{
							xtype: 'label',
							text: 'Cari Dokter: ' ,
							
						},
						{xtype: 'tbspacer',height: 3, width:5},
						 {
							x: 40,
							y: 40,
							xtype: 'textfield',
							//fieldLabel: 'No. Medrec',
							name: 'txtcaridokterall',
							id: 'txtcaridokterall',
							width: 150,
							listeners:
							{
								'specialkey': function ()
									{
										/* if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
										{
											dsGridSetPerawatJadwalOperasiOK(Ext.getCmp('txtcariperawat').getValue());
										} */
									}
							}
						},
						{
							text: 'Cari',
							id: 'btnCariDokterAllOperasi',
							iconCls: 'find',
							handler: function()
							{
								dsvDoubleGridDokterSetupOperasiOK.removeAll();
								dsGridDokterAllOperasi(Ext.getCmp('txtcaridokterall').getValue());
							}
						}
					]
				},
			]
		});
		return FrmTabs_SetupOpOk;
}
function dsDokter(){
	dsvDokter.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'vDokter'
                    //param : kriteria
                }			
            }
        );   
    return dsvDokter;
	}
/* function dsDokter(){
	dsvDokter.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'vDokter',
                    //param : 'kd_Dokter=1'
                }			
            }
        );   
    return dsvDokter;
	} */
	
function getPanelDokter(mod_id) {
    
	   var FormDepanDokter = new Ext.Panel({
        id: mod_id,
        closable: true,
        region: 'center',
        layout: 'form',
        title: 'Setup Dokter',
        border: false,
        shadhow: true,
        autoScroll: false,
		width: 250,
        iconCls: 'Request',
        margins: '0 5 5 0',
		tbar: [
			{
                id: 'btnSaveDataDokter',
                text: 'Save Data',
                tooltip: 'Save Data',
                iconCls: 'Save',
                handler: function (sm, row, rec) {
					save_dataDokter();
						
                },
				
            },
			{
        	xtype: 'tbseparator'
       		},
			{
                id: 'btnDeleteDataDokter',
                text: 'Delete Data',
                tooltip: 'Delete Data',
                iconCls: 'Remove',
                handler: function (sm, row, rec) {
					console.log(rowselectedDokter);
					if (rowselectedDokter===undefined || rowselectedDokter==='')
					{
						ShowPesanErrorDokter('Maaf ! Tidak ada data yang terpilih', 'Error');
                    	
					}
					else
						delete_dataDokter(rowselectedDokter.data.kd_dokter);
				}
            },
			{
        	xtype: 'tbseparator'
       		},
			{
                id: 'btnRefreshDataDokter',
                text: 'Refresh',
                tooltip: 'Refresh',
                iconCls: 'Refresh',
                handler: function (sm, row, rec) {
                    refresh_dataDokter();
                },
				
            }
			],
        items: [
			gridDataViewEditDoubleGridDokter_SetupOperasi_Ok(),
			
            
            
//            PanelPengkajian(),
//            TabPanelPengkajian()
        ],
        listeners: {
            'afterrender': function () {
					dsGridDokterInapOperasi();
            }
        }
    });

    return FormDepanDokter;

}
;
function refresh_dataDokter()
{
	stateComboDokter=[null,null];
	//stateComboDokter=['KD_Dokter_Dokter','Dokter_Dokter'];
	ComboSetSetupDokter();
	//dsDokter();
	dsGridDokterInapOperasi();
}

function ComboSetSetupDokter()
{
    dsvDokter = new WebApp.DataStore({fields: stateComboDokter});
	//dsDokter();
	dsGridDokterInapOperasi();
	var CmbSetSetupDokter = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 70,
			id:'CmbSetDokter',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			/*readOnly:true,
			emptyText:'Set Dokter',
			value: 1,*/
			width: 150,
			store: dsvDokter,
			valueField: stateComboDokter[0],
			displayField: stateComboDokter[1],
			//value:selectSetPilihankelompokPasien,
			listeners:
			{
				'select': function(a,b,c)
				{
					kdDokterDokter=CmbSetSetupDokter.value;
					//selectSetPilihankelompokPasien=b.data.displayText;
				}
			}
		}
	);
	return CmbSetSetupDokter;
}
function getDataJenisDiet_(getkodeJenisDiet_)
{
	Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/gizi/Dokter/getDokter/"+getkodeJenisDiet_,
				//params: ParameterGetKodeJenisDiet_(getkodeJenisDiet_),
				failure: function(o)
				{
					ShowPesanErrorDokter('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanSuksesDokter("Data Berhasil Disimpan","Sukses");
						dsDokter();
						dsGridDokterInapOperasi();
						ClearTextDokter();
					}
					else 
					{
						ShowPesanErrorDokter('Gagal Menyimpan Data ini', 'Error');
						dsDokter();
						dsGridDokterInapOperasi();
					};
				}
			}
			
		)
}
function save_dataDokter()
{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/kamar_operasi/functionSetupDokter/SimpanDokterInap",
				params: ParameterSaveDokter(),
				failure: function(o)
				{
					ShowPesanErrorDokter('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanSuksesDokter("Data Berhasil Disimpan","Sukses");
						secondGridStore.removeAll();
						dsGridDokterInapOperasi();
					}
					else 
					{
						ShowPesanErrorDokter('Gagal Menyimpan Data ini', 'Error');
						secondGridStore.removeAll();
						dsGridDokterInapOperasi();
					};
				}
			}
			
		)
	
}
function edit_dataDokter(){
	ShowLookupDokter(rowselectedDokter.json);
}
function delete_dataDokter(kodeDokter) 
{
		Ext.Msg.show
		(
			{
			   title:nmHeaderHapusData,
			   msg: 'Apakah data akan dihapus?' ,
			   buttons: Ext.MessageBox.YESNO,
			   width:250,
			   fn: function (btn) 
			   {			
					if (btn === 'yes') 
					{
						Ext.Ajax.request
						(
							{
								url: baseURL + "index.php/kamar_operasi/functionSetupDokter/HapusDokterInap",
								params: {
											kdunit:"ok_default_kamar",
											kddokter:kodeDokter
										},
								failure: function(o)
										{
											ShowPesanErrorDokter('Hubungi Admin', 'Error');
										},
								success: function(o) 
								{
									var cst = Ext.decode(o.responseText);
									if (cst.success === true) 
									{
										if (cst.dokter===0)
										{
											var line	=secondGrid.getSelectionModel().selection.cell[0];
											secondGridStore.removeAt(line);
											rowselectedDokter='';
										}
										else
										{
											secondGridStore.removeAll();
											for(var i=0; i<cst.listData.length; i++){
							
												var recs    = [],recType = secondGridStore.recordType;
												var o=cst.listData[i];
												recs    = [];
												recs.push(new recType(o));
												
												secondGridStore.add(recs);
												dsGridDokterAllOperasi();
											}
											ShowPesanSuksesDokter("Data Berhasil Dihapus","Sukses");
											rowselectedDokter='';
											dsGridDokterInapOperasi();
										}
										
									}
									else 
									{
										ShowPesanErrorDokter(nmPesanHapusError,nmHeaderHapusData);
									};
								}
							}
						)
					};
				}
			}
		)
	};
	
function addnew_dataDokter()
{
	ClearTextDokter();
}
function ValidasiEntriDokter(modul,mBolHapus)
{
	
	
	var x = 1;
	if (kodeDokterSimpan===undefined || kodeDokterSimpan==='')
		ShowPesanErrorDokter('Maaf ! Tidak ada dokter yang terpilih', 'Error');
	x = 0;
	/*
	if( nama === '')
	{
	ShowPesanError('Data Belum Diisi Lengkap', 'Warning');
	x = 0;	
	}*/
	
	
	return x;
};
function ValidasiEditDokter(modul,mBolHapus)
{
	var kode = Ext.getCmp('TxtKdDokter').getValue();
	var nama = SetupDokter.form.ComboBox.jenisdietDokter.getValue();
	
	var x = 1;
	if(kode === '' ){
	ShowPesanErrorDokter('Data Belum Diisi Lengkap', 'Warning');
	x = 0;
	}
	if( nama === '')
	{
	ShowPesanErrorDokter('Data Belum Diisi Lengkap', 'Warning');
	x = 0;	
	}
	return x;
};

function ShowPesanSuksesDokter(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};

function ShowPesanErrorDokter(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:290
		}
	);
};
function ClearTextDokter()
{
	Ext.getCmp('TxtKdDokter').setValue("");
	tagBerlaku : Ext.getCmp('CekBerlakuDokter').setValue(false),
	SetupDokter.form.ComboBox.jenisdietDokter.setValue("");
	Ext.getCmp('TxtHrgPokokDokter').setValue("");
	Ext.getCmp('TxtHrgJualDokter').setValue("");	
	Ext.getCmp('CmbSetDokter').setValue(null);
	SetupDokter.form.ComboBox.jenisdietDokter.focus();
}
/*function disabled_data(disDatax){
	if (disDatax==1) {
		var dis= 
				{
					kodeData : Ext.getCmp('TxtKdAhli').disable(),
				}
	}
	else if (disDatax==0) 
	{
		var dis= 
				{
					kodeData : Ext.getCmp('TxtKdAhli').enable(),
				}
	}
	return dis;
	}*/
function ParameterGetKodeJenisDiet_(getkodeJenisDiet_)
{
	var params =
	{
		Table:'vDokter',
		kodeJenisDiet_: getkodeJenisDiet_
	}
}
function ParameterSaveDokter()
{
	var params={};
	params['jumlah']=secondGridStore.getCount();
	for(var i = 0 ; i < secondGridStore.getCount();i++)
	{
		params['kodedok-'+i]=secondGridStore.data.items[i].data.kd_dokter;
		params['nmdok-'+i]=secondGridStore.data.items[i].data.nama;
		params['dokluar-'+i]=secondGridStore.data.items[i].data.dokter_luar;
	}
return params;	
}
function paramsDeleteDokter(rowdata) 
{
    var params =
	{
		Table:'vDokter',
		kode :  rowdata.KD__TARCUS,
	    kodeJen : rowdata.KD_JENIS_TARCUS,
		//Data: ShowLookupDeleteDokter(rowselectedDokter.json)
	};
    return params
};
function ShowLookupDokter(rowdata)
{
	if (rowdata == undefined){
        ShowPesanErrorDokter('Data Kosong', 'Error');
    }
    else
    {
      datainit_formDokter(rowdata); 
    }
}
function ShowLookupDeleteDokter(rowdata)
{
      var kode =  rowdata.KD__TARCUS;
	  var kodeJen = rowdata.KD_JENIS_TARCUS;
	  return kode;
}
function datainit_formDokter(rowdata){
	Ext.getCmp('TxtKdDokter').setValue(rowdata.KD_JENIS_TARCUS);
	SetupDokter.form.ComboBox.jenisdietDokter.setValue(rowdata.NAMA_JENISDIET_TARCUS);
	Ext.getCmp('CmbSetDokter').setValue(rowdata.KD_Dokter_TARCUS);
	if (rowdata.TAG_BERLAKU_TARCUS==0)
		{Ext.getCmp('CekBerlakuDokter').setValue(false);}
	else
		{Ext.getCmp('CekBerlakuDokter').setValue(true);}
	
	Ext.getCmp('TxtHrgPokokDokter').setValue(rowdata.HARGA_POKOK_TARCUS);
	Ext.getCmp('TxtHrgJualDokter').setValue(rowdata.HARGA_JUAL_TARCUS);
	kdSetupDokter=rowdata.KD__TARCUS;
	kdDokterDokter=rowdata.KD_Dokter_TARCUS;
	pilihAksiDokter='update';
	}