var dataSource_viSetupItemPerencanaan;
var selectCount_viSetupItemPerencanaan=50;
var NamaForm_viSetupItemPerencanaan="Setup Item Perencanaan";
var mod_name_viSetupItemPerencanaan="Setup Item Perencanaan";
var now_viSetupItemPerencanaan= new Date();
var rowSelected_viSetupItemPerencanaan;
var setLookUps_viSetupItemPerencanaan;
var tanggal = now_viSetupItemPerencanaan.format("d/M/Y");
var jam = now_viSetupItemPerencanaan.format("H/i/s");
var tmpkriteria;
var DataGridJenisObat;
var GridDataView_viSetupItemPerencanaan;
var a;


var CurrentData_viSetupItemPerencanaan =
{
	data: Object,
	details: Array,
	row: 0
};

var SetupItemPerencanaan={};
SetupItemPerencanaan.form={};
SetupItemPerencanaan.func={};
SetupItemPerencanaan.vars={};
SetupItemPerencanaan.func.parent=SetupItemPerencanaan;
SetupItemPerencanaan.form.ArrayStore={};
SetupItemPerencanaan.form.ComboBox={};
SetupItemPerencanaan.form.DataStore={};
SetupItemPerencanaan.form.Record={};
SetupItemPerencanaan.form.Form={};
SetupItemPerencanaan.form.Grid={};
SetupItemPerencanaan.form.Panel={};
SetupItemPerencanaan.form.TextField={};
SetupItemPerencanaan.form.Button={};

SetupItemPerencanaan.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_sub_spc', 'spesialisasi'],
	data: []
});

CurrentPage.page = dataGrid_viSetupItemPerencanaan(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dataGrid_viSetupItemPerencanaan(mod_id_viSetupItemPerencanaan){	
 	// Field kiriman dari Project Net.
    var FieldMaster_viSetupItemPerencanaan = 
	[
		'kd_item_rencana', 'nama_item','jenis' 
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viSetupItemPerencanaan = new WebApp.DataStore
	({
        fields: FieldMaster_viSetupItemPerencanaan
    });
	dataGriSetupItemPerencanaan();
    //load_data_Perencanaan();
    // Grid lab Perencanaan # --------------
	GridDataView_viSetupItemPerencanaan = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viSetupItemPerencanaan,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:true,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viSetupItemPerencanaan = undefined;
							rowSelected_viSetupItemPerencanaan = dataSource_viSetupItemPerencanaan.getAt(row);
							CurrentData_viSetupItemPerencanaan
							CurrentData_viSetupItemPerencanaan.row = row;
							CurrentData_viSetupItemPerencanaan.data = rowSelected_viSetupItemPerencanaan.data;
							DataInitSetupItemPerencanaan(rowSelected_viSetupItemPerencanaan.data);
							//DataInitSetupItemPerencanaan(rowSelected_viSetupItemPerencanaan.data);
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viSetupItemPerencanaan = dataSource_viSetupItemPerencanaan.getAt(ridx);
					if (rowSelected_viSetupItemPerencanaan != undefined)
					{
						DataInitSetupItemPerencanaan(rowSelected_viSetupItemPerencanaan.data);
						//setLookUp_viSetupItemPerencanaan(rowSelected_viSetupItemPerencanaan.data);
					}
					else
					{
						//setLookUp_viSetupItemPerencanaan();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Setup Unit
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header: 'Kode item',
						dataIndex: 'kd_item_rencana',
						hideable:false,
						menuDisabled: true,
						width: 40
						
					},
					//-------------- ## --------------
					{
						header: 'Nama Item',
						dataIndex: 'nama_item',
						hideable:false,
						menuDisabled: true,
						width: 90
					},
					//-------------- ## --------------
					{
						header: 'Jenis',
						dataIndex: 'jenis',
						hideable:false,
						menuDisabled: true,
						width: 90
					},
					//-------------- ## --------------
					
					//-------------- ## --------------
				]
			),
			/* tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viSetupItemPerencanaan',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viSetupItemPerencanaan',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viSetupItemPerencanaan != undefined)
							{
								DataInitSetupItemPerencanaan(rowSelected_viSetupItemPerencanaan.data);
							}
							
						}
					}
					//-------------- ## --------------
				]
			}, */
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viSetupItemPerencanaan, selectCount_viSetupItemPerencanaan, dataSource_viSetupItemPerencanaan),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)	
	var PanelTabSetupItemPerencanaan = new Ext.FormPanel({
        labelAlign: 'top',
		layout:'form',
		border: false,  
		autoHeight:true,
        title: '',
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [PanelInputUnit()]
		
	})
	// Kriteria filter pada Grid # --------------
    var FrmData_viSetupItemPerencanaan = new Ext.Panel
    (
		{
			title: NamaForm_viSetupItemPerencanaan,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viSetupItemPerencanaan,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ PanelTabSetupItemPerencanaan,
					GridDataView_viSetupItemPerencanaan],
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAddAsisten_viSetupItemPerencanaan',
						handler: function(){
							AddNewSetupItemPerencanaan();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viSetupItemPerencanaan',
						handler: function()
						{
							
							dataSave_viSetupItemPerencanaan();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viSetupItemPerencanaan',
						handler: function()
						{
							Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
								if (button == 'yes'){
									loadMask.show();
									dataDelete_viSetupItemPerencanaan();
								}
							});
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_viSetupPabrik',
						handler: function()
						{
							dataSource_viSetupItemPerencanaan.removeAll();
							dataGriSetupItemPerencanaan();
						}
					},
					{
						xtype: 'tbseparator'
					}
					
				]
			}
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_viSetupItemPerencanaan;
    //-------------- # End form filter # --------------
}

function PanelInputUnit(rowdata){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 85,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'Kode Item'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 0,
								xtype: 'textfield',
								id: 'txtKditem_rencana_SetupItemPerencanaan',
								name: 'txtKditem_rencana_SetupItemPerencanaan',
								width: 100,
								allowBlank: false,
								//readOnly: true,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									},
									
								}
							},
							
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'Nama Item'
							},
							{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 30,
								xtype: 'textfield',
								id: 'txtItemPerencanaan_SetupItemPerencanaan',
								name: 'txtItemPerencanaan_SetupItemPerencanaan',
								width: 200,
								allowBlank: false,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									},
									
								}
							},
							{
								x: 10,
								y: 60,
								xtype: 'label',
								text: 'Jenis'
							},
							{
								x: 120,
								y: 60,
								xtype: 'label',
								text: ':'
							},
							comboJenisPerencanaan(),
							
							
						]
					}
				]
			}
		]		
	};
        return items;
}
//------------------end---------------------------------------------------------

function load_data_Perencanaan(param)
{

	Ext.Ajax.request(
	{
		url: baseURL + "index.php/kamar_operasi/functionSetupItemPerencanaan/getItemRencana",
		params:{
			command: param
		} ,
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
		success: function(o) {
			cboPerencanaan_SetupItemPerencanaan.store.removeAll();
				var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsPerencanaan_SetupItemPerencanaan.recordType;
				var o=cst['listData'][i];
				
				recs.push(new recType(o));
				dsPerencanaan_SetupItemPerencanaan.add(recs);
				//console.log(o);
			}
		}
	});
}

function comboJenisPerencanaan()
{ 
 
	var Field = ['kd_spesial','spesialisasi'];

    dsPerencanaan_SetupItemPerencanaan = new WebApp.DataStore({ fields: Field });
	
	cboPerencanaan_SetupItemPerencanaan= new Ext.form.ComboBox
	(
		{
			x				: 130,
			y				: 60,
			id				: 'cboPerencanaan_SetupItemPerencanaan',
			typeAhead		: true,
			triggerAction	: 'all',
			lazyRender		: true,
			mode			: 'local',
			emptyText		: '',
			fieldLabel		:  '',
			align			: 'Right',
			width			: 70,
			store			: new Ext.data.ArrayStore({
								id: 0,
								fields: ['Id', 'displayText'],
								data: [[1, 'OHP'], [2, 'AHP']]
							}),
			valueField: 'displayText',
			displayField: 'displayText',
			value: 'OHP',
			//hideTrigger		: true,
			listeners:
			{
				select	: function(a,b,c){
					//dataGriSetupItemPerencanaan(b.data.kd_spesial);	
				},
				/* keyUp: function(a,b,c){
					$this1=this;
					if((b.getKey()>=65 && b.getKey()<=90)|| (b.getKey()>=48 && b.getKey()<=57) || b.getKey()==32 || b.getKey()==8 || b.getKey()==46){
						clearTimeout(this.time);
					
						this.time=setTimeout(function(){
							if($this1.lastQuery != '' ){
								var value=$this1.lastQuery;
								var param={};
								param['text']=$this1.lastQuery;
								load_data_Perencanaan($this1.lastQuery);

								
							}
						},1000);
					}
				}, */
				
			}
		}
	);return cboPerencanaan_SetupItemPerencanaan;
};

function dataGriSetupItemPerencanaan(kd_item_rencana){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/kamar_operasi/functionSetupItemPerencanaan/getItemGrid",
			params: {
				kd_item_rencana:kd_item_rencana
			},
			failure: function(o)
			{
				ShowPesanErrorSetupItemPerencanaan('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dataSource_viSetupItemPerencanaan.removeAll();
					var recs=[],
						recType=dataSource_viSetupItemPerencanaan.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dataSource_viSetupItemPerencanaan.add(recs);
					
					
					
					GridDataView_viSetupItemPerencanaan.getView().refresh();
				}
				else 
				{
					ShowPesanErrorSetupItemPerencanaan('Gagal membaca data items', 'Error');
				};
			}
		}
		
	)
	
}

function dataSave_viSetupItemPerencanaan(){
	if (ValidasiSaveSetupItemPerencanaan(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/kamar_operasi/functionSetupItemPerencanaan/cek_data",
				params: getParamSaveSetupItemPerencanaan(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupItemPerencanaan('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					var kode = Ext.getCmp('txtKditem_rencana_SetupItemPerencanaan').getValue();
					if (cst.success === true) 
					{
						Ext.Msg.confirm('Warning', 'Data dengan kode '+kode+' sudah tersedia dalam database apakah anda akan mengubahnya ?', function(button){
						if (button == 'yes'){
							loadMask.show();
							Ext.Ajax.request
							(
								{
									url: baseURL + "index.php/kamar_operasi/functionSetupItemPerencanaan/save",
									params: getParamSaveSetupItemPerencanaan(),
									failure: function(o)
									{
										loadMask.hide();
										ShowPesanErrorSetupItemPerencanaan('Hubungi Admin', 'Error');
									},	
									success: function(o) 
									{
										var cst = Ext.decode(o.responseText);
										if (cst.success === true) 
										{
											loadMask.hide();
											ShowPesanInfoSetupItemPerencanaan('Berhasil mengubah data dengan kode '+kode+'','Information');
											Ext.getCmp('txtKditem_rencana_SetupItemPerencanaan').setValue(cst.kode);
											dataSource_viSetupItemPerencanaan.removeAll();
											dataGriSetupItemPerencanaan();
											
										}
										else 
										{
											loadMask.hide();
											ShowPesanErrorSetupItemPerencanaan('Gagal mengubah data dengan kode '+kode+'', 'Error');
											dataSource_viSetupItemPerencanaan.removeAll();
											dataGriSetupItemPerencanaan();
										};
									}
								}
								
							)
						}else{
							loadMask.hide();
							}
						})
					}
					else 
					{
						loadMask.show();
						Ext.Ajax.request
						(
							{
								url: baseURL + "index.php/kamar_operasi/functionSetupItemPerencanaan/save",
								params: getParamSaveSetupItemPerencanaan(),
								failure: function(o)
								{
									loadMask.hide();
									ShowPesanErrorSetupItemPerencanaan('Hubungi Admin', 'Error');
								},	
								success: function(o) 
								{
									var cst = Ext.decode(o.responseText);
									if (cst.success === true) 
									{
										loadMask.hide();
										ShowPesanInfoSetupItemPerencanaan('Berhasil menyimpan data ini','Information');
										Ext.getCmp('txtKditem_rencana_SetupItemPerencanaan').setValue(cst.kode);
										dataSource_viSetupItemPerencanaan.removeAll();
										dataGriSetupItemPerencanaan();
										
									}
									else 
									{
										loadMask.hide();
										ShowPesanErrorSetupItemPerencanaan('Gagal menyimpan data ini', 'Error');
										dataSource_viSetupItemPerencanaan.removeAll();
										dataGriSetupItemPerencanaan();
									};
								}
							}
							
						)
					};
				}
			}
		)
		
	}
}

function dataDelete_viSetupItemPerencanaan(){
	if (ValidasiSaveSetupItemPerencanaan(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/kamar_operasi/functionSetupItemPerencanaan/delete",
				params: getParamDeleteSetupItemPerencanaan(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupItemPerencanaan('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupItemPerencanaan('Berhasil menghapus data ini','Information');
						AddNewSetupItemPerencanaan()
						dataSource_viSetupItemPerencanaan.removeAll();
						dataGriSetupItemPerencanaan();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupItemPerencanaan('Gagal menghapus data ini', 'Error');
						dataSource_viSetupItemPerencanaan.removeAll();
						dataGriSetupItemPerencanaan();
					};
				}
			}
			
		)
	}
}

function AddNewSetupItemPerencanaan(){
	Ext.getCmp('txtKditem_rencana_SetupItemPerencanaan').setValue('');
	Ext.getCmp('cboPerencanaan_SetupItemPerencanaan').setValue('');
	Ext.getCmp('txtItemPerencanaan_SetupItemPerencanaan').setValue('');
};

function DataInitSetupItemPerencanaan(rowdata){
	Ext.getCmp('txtKditem_rencana_SetupItemPerencanaan').setValue(rowdata.kd_item_rencana);
	Ext.getCmp('cboPerencanaan_SetupItemPerencanaan').setValue(rowdata.jenis);
	Ext.getCmp('txtItemPerencanaan_SetupItemPerencanaan').setValue(rowdata.nama_item);
};

function getParamSaveSetupItemPerencanaan(){
	var	params =
	{
		Kditem_rencana:Ext.getCmp('txtKditem_rencana_SetupItemPerencanaan').getValue(),
		jenisItem:Ext.getCmp('cboPerencanaan_SetupItemPerencanaan').getValue(),
		namaItem:Ext.getCmp('txtItemPerencanaan_SetupItemPerencanaan').getValue()
	}
   
    return params
};

function getParamDeleteSetupItemPerencanaan(){
	var	params =
	{
		Kditem_rencana:Ext.getCmp('txtKditem_rencana_SetupItemPerencanaan').getValue()
	}
   
    return params
};

function ValidasiSaveSetupItemPerencanaan(modul,mBolHapus){
	var x = 1;
	if(Ext.getCmp('cboPerencanaan_SetupItemPerencanaan').getValue() === ''){
		if(Ext.getCmp('cboPerencanaan_SetupItemPerencanaan').getValue() === ''){
			loadMask.hide();
			ShowPesanWarningSetupItemPerencanaan('Perencanaan masih kosong', 'Warning');
			x = 0;
		}
		
	} 
	if(Ext.getCmp('txtItemPerencanaan_SetupItemPerencanaan').getValue() === ''){
		if(Ext.getCmp('txtItemPerencanaan_SetupItemPerencanaan').getValue() === ''){
			loadMask.hide();
			ShowPesanWarningSetupItemPerencanaan('Item Perencanaan masih kosong', 'Warning');
			x = 0;
		}
		
	} 
	return x;
};



function ShowPesanWarningSetupItemPerencanaan(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorSetupItemPerencanaan(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoSetupItemPerencanaan(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};