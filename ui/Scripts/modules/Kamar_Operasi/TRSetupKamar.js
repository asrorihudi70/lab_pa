var dsvKamar;
var dsvKamar;
var rowselectedKamar;
var LookupKamar;
var disDataKamar=0;
var cek=0;
var kdSetupKamar;
var kdKamarKamar;
var kodeKamar;
var stateComboKamar=['KD_Kamar_Kamar','Kamar_Kamar'];
var pilihAksiKamar;
var SetupKamar={};
SetupKamar.form={};
SetupKamar.func={};
SetupKamar.vars={};
SetupKamar.func.parent=SetupKamar;
SetupKamar.form.ArrayStore={};
SetupKamar.form.ComboBox={};
SetupKamar.form.DataStore={};
SetupKamar.form.Record={};
SetupKamar.form.Form={};
SetupKamar.form.Grid={};
SetupKamar.form.Panel={};
SetupKamar.form.TextField={};
SetupKamar.form.Button={};

SetupKamar.form.ArrayStore.Kamar=new Ext.data.ArrayStore({
		id: 0,
		fields: [	
					'kd_Kamar','Kamar_diet','harga_pokok'
				],
		data: []
	});
	
CurrentPage.page = getPanelKamar(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dsKamar(){
	dsvKamar.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'vKamarOperasi'
                    //param : kriteria
                }			
            }
        );   
    return dsvKamar;
	}

	
function getPanelKamar(mod_id) {
    var Field = ['no_kamar','nama_kamar'];
    dsvKamar = new WebApp.DataStore({
        fields: Field
    });
	dsKamar();
    var gridListHasilKamar = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dsvKamar,
        anchor: '100% 70%',
        columnLines: false,
        autoScroll: false,
        border: true,
        sort: false,
        autoHeight: false,
        layout: 'fit',
        region: 'center',
        height: 200,
        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
            listeners: {
                rowselect: function (sm, row, rec) {
					rowselectedKamar=dsvKamar.getAt(row);
                }
            }
        }),
        listeners: {
           rowdblclick: function (sm, ridx, cidx) {
                rowselectedKamar=dsvKamar.getAt(ridx);
				ShowLookupKamar(rowselectedKamar.json);
				/*disData=1;
				disabled_data(disData);*/
				
            }
        },
        cm: new Ext.grid.ColumnModel(
                [
				new Ext.grid.RowNumberer({
					header:'No.'
					}),
                    {
                        id: 'colNoKamarViewHasilKamar',
                        header: 'No Kamar',
                        dataIndex: 'no_kamar',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 5
                    },
                    {
                        id: 'colNamaKamarViewHasilKamar',
                        header: 'Nama Kamar',
                        dataIndex: 'nama_kamar',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
					
                ]
                ),
        viewConfig: {
            forceFit: true
        },
        tbar:[
			{
                id: 'btnEditDataKamar',
                text: 'Edit Data',
                tooltip: 'Edit Data',
                iconCls: 'Edit_Tr',
                handler: function (sm, row, rec) {
                    edit_dataKamar();
                },
				
            },
		]
    });
	   var FormDepanKamar = new Ext.Panel({
        id: mod_id,
        closable: true,
        region: 'center',
        layout: 'form',
        title: 'Setup Kamar',
        border: false,
        shadhow: true,
        autoScroll: false,
		width: 250,
        iconCls: 'Request',
        margins: '0 5 5 0',
		tbar: [
			
			{
                id: 'btnAddNewDataKamar',
                text: 'Add New',
                tooltip: 'addnewKamar',
                iconCls: 'add',
                handler: function (sm, row, rec) {
                    addnew_dataKamar();
				}
            },
			{
        	xtype: 'tbseparator'
       		},
			{
                id: 'btnSaveDataKamar',
                text: 'Save Data',
                tooltip: 'Save Data',
                iconCls: 'Save',
                handler: function (sm, row, rec) {
                    save_dataKamar();
					//alert(cek);
                },
				
            },
			{
        	xtype: 'tbseparator'
       		},
			{
                id: 'btnDeleteDataKamar',
                text: 'Delete Data',
                tooltip: 'Delete Data',
                iconCls: 'Remove',
                handler: function (sm, row, rec) {
					if (rowselectedKamar!=undefined)
                    	delete_dataKamar();
					else
						ShowPesanErrorKamar('Maaf ! Tidak ada data yang terpilih', 'Error');
				}
            },
			{
        	xtype: 'tbseparator'
       		},
			{
                id: 'btnRefreshDataKamar',
                text: 'Refresh',
                tooltip: 'Refresh',
                iconCls: 'Refresh',
                handler: function (sm, row, rec) {
                    refresh_dataKamar();
                },
				
            }
			],
        items: [
			getPanelPencarianKamar(),
			gridListHasilKamar,
            
            
//            PanelPengkajian(),
//            TabPanelPengkajian()
        ],
        listeners: {
            'afterrender': function () {

            }
        }
    });

    return FormDepanKamar;

}
;
function refresh_dataKamar()
{
	dsKamar();
}
function getPanelPencarianKamar() {
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 1190,
                height: 80,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'Kode'
                    },
                    {
                        x: 110,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 10,
                        xtype: 'textfield',
                        name: 'TxtKdKamar',
                        id: 'TxtKdKamar',
                        width: 80,
						disabled: true
                    },
					
                    
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Nama Kamar'
                    },
                    {
                        x: 110,
                        y: 40,
                        xtype: 'label',
                        text: ' : '
                    },
					
					SetupKamar.form.ComboBox.Kamar= new Nci.form.Combobox.autoComplete({
						//id:'TxtNamaKamarKamarome',
						store	: SetupKamar.form.ArrayStore.Kamar,
						select	: function(a,b,c){
							Ext.getCmp('TxtKdKamar').setValue(b.data.no_kamar);
							/* var getKode_Gz=b.data.kd_Kamar
							alert(getKode_Gz); */
							 /* Ext.Ajax.request
								(
									{
										url: baseURL + "index.php/gizi/Kamar/getKamar/",
										params:{getKode_Gz:b.data.kd_Kamar},
										failure: function(o)
										{
											ShowPesanErrorKamar('Hubungi Admin', 'Error');
										},	
										success: function(o) 
										{
											console.log(o.responseText);
											 var cst = Ext.decode(o.responseText);
											if (cst.listData != '') 
											{	
												Ext.getCmp('TxtKdKamar').setValue(cst.dataKode);
												Ext.getCmp('CmbSetKamar').setValue(cst.dataKodeKamar);
												if (cst.dataTagBerlaku==0)
													{Ext.getCmp('CekBerlakuKamar').setValue(false);}
												else
													{Ext.getCmp('CekBerlakuKamar').setValue(true);}
												
												Ext.getCmp('TxtHrgPokokKamar').setValue(cst.dataHargapokok);
												Ext.getCmp('TxtHrgJualKamar').setValue(cst.dataHargaJual);
												kdSetupKamar=cst.dataKode;
												kodeKamar=cst.dataKodeKamar;
												pilihAksiKamar='update';
											}
											else 
											{
												Ext.getCmp('CmbSetKamar').setValue('');
												Ext.getCmp('TxtKdKamar').setValue(b.data.kd_Kamar);
												Ext.getCmp('TxtHrgPokokKamar').setValue(b.data.harga_pokok);
												Ext.getCmp('CekBerlakuKamar').setValue(false);
												Ext.getCmp('TxtHrgPokokKamar').setValue('');
												Ext.getCmp('TxtHrgJualKamar').setValue('');
												kdSetupKamar=cst.dataKode;
												kodeKamar=cst.dataKodeKamar;
												pilihAksiKamar='insert';
											}; 
										}
									}
									
								) */ 
						},
						width	: 200,
						
						insert	: function(o){
							return {
								no_kamar        	:o.no_kamar,
								nama_kamar        	:o.nama_kamar,
								text				:  '<table style="font-size: 11px;"><tr><td width="80">'+o.no_kamar+'</td><td width="180">'+o.nama_kamar+'</td></tr></table>',
							}
						},
						url		: baseURL + "index.php/kamar_operasi/functionSetupKamar/getKamarGrid",
						valueField: 'nama_kamar',
						displayField: 'text',
						listWidth: 260,
						x: 120,
                        y: 40,
						
					}),
					
                ]
            }
        ]
    };
    return items;
}
;
function ComboSetSetupKamar()
{
    dsvKamar = new WebApp.DataStore({fields: stateComboKamar});
	dsKamar();
	var CmbSetSetupKamar = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 70,
			id:'CmbSetKamar',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			/*readOnly:true,
			emptyText:'Set Kamar',
			value: 1,*/
			width: 150,
			store: dsvKamar,
			valueField: stateComboKamar[0],
			displayField: stateComboKamar[1],
			//value:selectSetPilihankelompokPasien,
			listeners:
			{
				'select': function(a,b,c)
				{
					kdKamarKamar=CmbSetSetupKamar.value;
					//selectSetPilihankelompokPasien=b.data.displayText;
				}
			}
		}
	);
	return CmbSetSetupKamar;
}
function getData_Gz(getkode_Gz)
{
	Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/gizi/Kamar/getKamar/"+getkode_Gz,
				//params: ParameterGetKode_Gz(getkode_Gz),
				failure: function(o)
				{
					ShowPesanErrorKamar('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanSuksesKamar("Data Berhasil Disimpan","Sukses");
						dsKamar();
						
					}
					else 
					{
						ShowPesanErrorKamar('Gagal Menyimpan Data ini', 'Error');
						dsKamar();
					};
				}
			}
			
		)
}
function save_dataKamar()
{
	if (ValidasiEntriKamar(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/kamar_operasi/functionSetupKamar/save",
				params: ParameterSaveKamar(),
				failure: function(o)
				{
					ShowPesanErrorKamar('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanSuksesKamar("Data Berhasil Disimpan","Sukses");
						dsKamar();
						Ext.getCmp("TxtKdKamar").setValue(cst.kode);
					}
					else 
					{
						ShowPesanErrorKamar('Gagal Menyimpan Data ini', 'Error');
						dsKamar();
					};
				}
			}
			
		)
	}
}
function edit_dataKamar(){
	ShowLookupKamar(rowselectedKamar.json);
}
function delete_dataKamar() 
{
	// console.log(rowselectedKamar.json);
		Ext.Msg.show
		(
			{
			   title 	: nmHeaderHapusData,
			   msg 		: "Yakin untuk menghapus data "+rowselectedKamar.json.nama_kamar+" ?",
			   buttons 	: Ext.MessageBox.YESNO,
			   width 	: 250,
			   fn 		: function (btn) {			
					if (btn === 'yes') 
					{
						Ext.Ajax.request
						(
							{
								url: baseURL + "index.php/kamar_operasi/functionSetupKamar/delete",
								params: paramsDeleteKamar(rowselectedKamar.json),
								success: function(o) 
								{
									var cst = Ext.decode(o.responseText);
									if (cst.success === true) 
									{
										ShowPesanSuksesKamar(nmPesanHapusSukses,nmHeaderHapusData);
										dsKamar();
										ClearTextKamar();
									}
									else if (cst.success === false && cst.pesan===0)
									{
										ShowPesanErrorKamar(nmPesanHapusGagal,nmHeaderHapusData);
									}
									else 
									{
										ShowPesanErrorKamar(nmPesanHapusError,nmHeaderHapusData);
									};
								}
							}
						)
					};
				}
			}
		)
	};
	
function addnew_dataKamar()
{
	ClearTextKamar();
}
function ValidasiEntriKamar(modul,mBolHapus)
{
	var kode = Ext.getCmp('TxtKdKamar').getValue();
	var nama = SetupKamar.form.ComboBox.Kamar.getValue();
	
	var x = 1;
	if(nama === '' ){
	ShowPesanErrorKamar('Data Belum Diisi Lengkap', 'Warning');
	x = 0;
	}/*
	if( nama === '')
	{
	ShowPesanError('Data Belum Diisi Lengkap', 'Warning');
	x = 0;	
	}*/
	
	
	return x;
};
function ValidasiEditKamar(modul,mBolHapus)
{
	var kode = Ext.getCmp('TxtKdKamar').getValue();
	var nama = SetupKamar.form.ComboBox.Kamar.getValue();
	
	var x = 1;
	if(kode === '' ){
	ShowPesanErrorKamar('Data Belum Diisi Lengkap', 'Warning');
	x = 0;
	}
	if( nama === '')
	{
	ShowPesanErrorKamar('Data Belum Diisi Lengkap', 'Warning');
	x = 0;	
	}
	return x;
};

function ShowPesanSuksesKamar(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};

function ShowPesanErrorKamar(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:290
		}
	);
};
function ClearTextKamar()
{
	Ext.getCmp('TxtKdKamar').setValue("");
	SetupKamar.form.ComboBox.Kamar.setValue("");
	SetupKamar.form.ComboBox.Kamar.focus();
}
/*function disabled_data(disDatax){
	if (disDatax==1) {
		var dis= 
				{
					kodeData : Ext.getCmp('TxtKdAhliGz').disable(),
				}
	}
	else if (disDatax==0) 
	{
		var dis= 
				{
					kodeData : Ext.getCmp('TxtKdAhliGz').enable(),
				}
	}
	return dis;
	}*/
function ParameterGetKode_Gz(getkode_Gz)
{
	var params =
	{
		Table:'vKamar',
		kode_Gz: getkode_Gz
	}
}
function ParameterSaveKamar()
{
	
var params =
	{
	KdKamar : Ext.getCmp('TxtKdKamar').getValue(),
	Nama : SetupKamar.form.ComboBox.Kamar.getValue()
	}	
return params;	
}
function paramsDeleteKamar(rowdata) 
{
    var params =
	{
		kode :  rowdata.no_kamar,
		//Data: ShowLookupDeleteKamar(rowselectedKamar.json)
	};
    return params
};
function ShowLookupKamar(rowdata)
{
	if (rowdata == undefined){
        ShowPesanErrorKamar('Data Kosong', 'Error');
    }
    else
    {
      datainit_formKamar(rowdata); 
    }
}
function ShowLookupDeleteKamar(rowdata)
{
      var kode =  rowdata.KD__;
	  var kodeJen = rowdata.KD_Kamar_;
	  return kode;
}
function datainit_formKamar(rowdata){
	Ext.getCmp('TxtKdKamar').setValue(rowdata.no_kamar);
	SetupKamar.form.ComboBox.Kamar.setValue(rowdata.nama_kamar);
	//pilihAksiKamar='update';
	}