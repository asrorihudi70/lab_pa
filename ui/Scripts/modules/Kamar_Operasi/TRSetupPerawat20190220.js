var dataSource_viSetupPerawat;
var selectCount_viSetupPerawat=50;
var NamaForm_viSetupPerawat="Setup Perawat";
var mod_name_viSetupPerawat="Setup Perawat";
var now_viSetupPerawat= new Date();
var rowSelected_viSetupPerawat;
var setLookUps_viSetupPerawat;
var tanggal = now_viSetupPerawat.format("d/M/Y");
var jam = now_viSetupPerawat.format("H/i/s");
var tmpkriteria;
var DataGridJenisObat;
var GridDataView_viSetupPerawat;
var a;


var CurrentData_viSetupPerawat =
{
	data: Object,
	details: Array,
	row: 0
};

var SetupPerawat={};
SetupPerawat.form={};
SetupPerawat.func={};
SetupPerawat.vars={};
SetupPerawat.func.parent=SetupPerawat;
SetupPerawat.form.ArrayStore={};
SetupPerawat.form.ComboBox={};
SetupPerawat.form.DataStore={};
SetupPerawat.form.Record={};
SetupPerawat.form.Form={};
SetupPerawat.form.Grid={};
SetupPerawat.form.Panel={};
SetupPerawat.form.TextField={};
SetupPerawat.form.Button={};

SetupPerawat.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_perawat', 'nama_perawat'],
	data: []
});

CurrentPage.page = dataGrid_viSetupPerawat(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dataGrid_viSetupPerawat(mod_id_viSetupPerawat){	
 	// Field kiriman dari Project Net.
    var FieldMaster_viSetupPerawat = 
	[
		'kd_perawat', 'nama_perawat' 
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viSetupPerawat = new WebApp.DataStore
	({
        fields: FieldMaster_viSetupPerawat
    });
    dataGriSetupPerawat();
    // Grid lab Perencanaan # --------------
	GridDataView_viSetupPerawat = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viSetupPerawat,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:true,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viSetupPerawat = undefined;
							rowSelected_viSetupPerawat = dataSource_viSetupPerawat.getAt(row);
							CurrentData_viSetupPerawat
							CurrentData_viSetupPerawat.row = row;
							CurrentData_viSetupPerawat.data = rowSelected_viSetupPerawat.data;
							DataInitSetupPerawat(rowSelected_viSetupPerawat.data);
							//DataInitSetupPerawat(rowSelected_viSetupPerawat.data);
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viSetupPerawat = dataSource_viSetupPerawat.getAt(ridx);
					if (rowSelected_viSetupPerawat != undefined)
					{
						DataInitSetupPerawat(rowSelected_viSetupPerawat.data);
						//setLookUp_viSetupPerawat(rowSelected_viSetupPerawat.data);
					}
					else
					{
						//setLookUp_viSetupPerawat();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Setup Unit
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header: 'Kode Perawat',
						dataIndex: 'kd_perawat',
						hideable:false,
						menuDisabled: true,
						width: 40
						
					},
					//-------------- ## --------------
					{
						header: 'Nama',
						dataIndex: 'nama_perawat',
						hideable:false,
						menuDisabled: true,
						width: 90
					}
					//-------------- ## --------------
				]
			),
			/* tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viSetupPerawat',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viSetupPerawat',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viSetupPerawat != undefined)
							{
								DataInitSetupPerawat(rowSelected_viSetupPerawat.data);
							}
							
						}
					}
					//-------------- ## --------------
				]
			}, */
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viSetupPerawat, selectCount_viSetupPerawat, dataSource_viSetupPerawat),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)	
	var PanelTabSetupPerawat = new Ext.FormPanel({
        labelAlign: 'top',
		layout:'form',
		border: false,  
		autoHeight:true,
        title: '',
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [PanelInputUnit()]
		
	})
	// Kriteria filter pada Grid # --------------
    var FrmData_viSetupPerawat = new Ext.Panel
    (
		{
			title: NamaForm_viSetupPerawat,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viSetupPerawat,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ PanelTabSetupPerawat,
					GridDataView_viSetupPerawat],
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAddAsisten_viSetupPerawat',
						handler: function(){
							AddNewSetupPerawat();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viSetupPerawat',
						handler: function()
						{
							loadMask.show();
							dataSave_viSetupPerawat();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viSetupPerawat',
						handler: function()
						{
							Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
								if (button == 'yes'){
									loadMask.show();
									dataDelete_viSetupPerawat();
								}
							});
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_viSetupPabrik',
						handler: function()
						{
							dataSource_viSetupPerawat.removeAll();
							dataGriSetupPerawat();
						}
					},
					{
						xtype: 'tbseparator'
					}
					
				]
			}
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_viSetupPerawat;
    //-------------- # End form filter # --------------
}

function PanelInputUnit(rowdata){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 85,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'Kode asisten'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 0,
								xtype: 'textfield',
								id: 'txtKdAsisten_SetupPerawat',
								name: 'txtKdAsisten_SetupPerawat',
								width: 100,
								allowBlank: false,
								readOnly: true,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									},
									
								}
							},
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'Nama'
							},
							{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							SetupPerawat.vars.nama_perawat=new Nci.form.Combobox.autoComplete({
								x: 130,
								y: 30,
								tabIndex:2,
								id		: 'txtComboAsisten_SetupPerawat',
								store	: SetupPerawat.form.ArrayStore.a,
								select	: function(a,b,c){
									
									Ext.getCmp('txtKdAsisten_SetupPerawat').setValue(b.data.kd_perawat);
									
									GridDataView_viSetupPerawat.getView().refresh();
									
								},
								onShowList:function(a){
									dataSource_viSetupPerawat.removeAll();
									
									var recs=[],
									recType=dataSource_viSetupPerawat.recordType;
										
									for(var i=0; i<a.length; i++){
										recs.push(new recType(a[i]));
									}
									dataSource_viSetupPerawat.add(recs);
									
								},
								insert	: function(o){
									return {
										kd_perawat      	: o.kd_perawat,
										nama 			: o.nama_perawat,
										text			:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_perawat+'</td><td width="200">'+o.nama+'</td></tr></table>'
									}
								},
								url		: baseURL + "index.php/kamar_operasi/functionSetupPerawat/getItemGrid",
								valueField: 'nama',
								displayField: 'text',
								listWidth: 280
							}),
							{
								x: 10,
								y: 60,
								xtype: 'label',
								text: 'Aktif'
							},
							{
								x: 120,
								y: 60,
								xtype: 'label',
								text: ':'
							},
							{
								xtype: 'checkbox',
								id: 'CbAktif_SetupPerawat',
								hideLabel:false,
								checked:true,
								x: 130,
								y: 60,
								listeners: 
								{
									check: function()
									{
										
										//cek=1;
									}
								}
							},
							
							
						]
					}
				]
			}
		]		
	};
        return items;
}
//------------------end---------------------------------------------------------

function dataGriSetupPerawat(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/kamar_operasi/functionSetupPerawat/getItemGrid",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorSetupPerawat('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dataSource_viSetupPerawat.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dataSource_viSetupPerawat.add(recs);
					
					
					
					GridDataView_viSetupPerawat.getView().refresh();
				}
				else 
				{
					ShowPesanErrorSetupPerawat('Gagal membaca data items', 'Error');
				};
			}
		}
		
	)
	
}

function dataSave_viSetupPerawat(){
	if (ValidasiSaveSetupPerawat(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/kamar_operasi/functionSetupPerawat/save",
				params: getParamSaveSetupPerawat(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupPerawat('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupPerawat('Berhasil menyimpan data ini','Information');
						Ext.getCmp('txtKdAsisten_SetupPerawat').setValue(cst.kode);
						dataSource_viSetupPerawat.removeAll();
						dataGriSetupPerawat();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupPerawat('Gagal menyimpan data ini', 'Error');
						dataSource_viSetupPerawat.removeAll();
						dataGriSetupPerawat();
					};
				}
			}
			
		)
	}
}

function dataDelete_viSetupPerawat(){
	if (ValidasiSaveSetupPerawat(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/kamar_operasi/functionSetupPerawat/delete",
				params: getParamDeleteSetupPerawat(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupPerawat('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupPerawat('Berhasil menghapus data ini','Information');
						AddNewSetupPerawat()
						dataSource_viSetupPerawat.removeAll();
						dataGriSetupPerawat();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupPerawat('Gagal menghapus data ini', 'Error');
						dataSource_viSetupPerawat.removeAll();
						dataGriSetupPerawat();
					};
				}
			}
			
		)
	}
}

function AddNewSetupPerawat(){
	Ext.getCmp('txtKdAsisten_SetupPerawat').setValue('');
	SetupPerawat.vars.nama_perawat.setValue('');
	Aktif:Ext.getCmp('CbAktif_SetupPerawat').setValue(true);
};

function DataInitSetupPerawat(rowdata){
	Ext.getCmp('txtKdAsisten_SetupPerawat').setValue(rowdata.kd_perawat);
	SetupPerawat.vars.nama_perawat.setValue(rowdata.nama_perawat);
	Ext.getCmp('CbAktif_SetupPerawat').setValue(rowdata.aktif);
};

function getParamSaveSetupPerawat(){
	var	params =
	{
		KdAsisten:Ext.getCmp('txtKdAsisten_SetupPerawat').getValue(),
		Nama:SetupPerawat.vars.nama_perawat.getValue(),
		Aktif:Ext.getCmp('CbAktif_SetupPerawat').getValue()
	}
   
    return params
};

function getParamDeleteSetupPerawat(){
	var	params =
	{
		KdAsisten:Ext.getCmp('txtKdAsisten_SetupPerawat').getValue()
	}
   
    return params
};

function ValidasiSaveSetupPerawat(modul,mBolHapus){
	var x = 1;
	if(SetupPerawat.vars.nama_perawat.getValue() === ''){
		if(SetupPerawat.vars.nama_perawat.getValue() === ''){
			loadMask.hide();
			ShowPesanWarningSetupPerawat('nama masih kosong', 'Warning');
			x = 0;
		}
		
	} 
	return x;
};



function ShowPesanWarningSetupPerawat(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorSetupPerawat(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoSetupPerawat(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};