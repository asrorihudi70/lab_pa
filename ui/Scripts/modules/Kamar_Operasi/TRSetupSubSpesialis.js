var dataSource_viSetupSubSpesialis;
var selectCount_viSetupSubSpesialis=50;
var NamaForm_viSetupSubSpesialis="Setup Sub Spesialis";
var mod_name_viSetupSubSpesialis="Setup Sub Spesialis";
var now_viSetupSubSpesialis= new Date();
var rowSelected_viSetupSubSpesialis;
var setLookUps_viSetupSubSpesialis;
var tanggal = now_viSetupSubSpesialis.format("d/M/Y");
var jam = now_viSetupSubSpesialis.format("H/i/s");
var tmpkriteria;
var DataGridJenisObat;
var GridDataView_viSetupSubSpesialis;
var a;


var CurrentData_viSetupSubSpesialis =
{
	data: Object,
	details: Array,
	row: 0
};

var SetupSubSpesialis={};
SetupSubSpesialis.form={};
SetupSubSpesialis.func={};
SetupSubSpesialis.vars={};
SetupSubSpesialis.func.parent=SetupSubSpesialis;
SetupSubSpesialis.form.ArrayStore={};
SetupSubSpesialis.form.ComboBox={};
SetupSubSpesialis.form.DataStore={};
SetupSubSpesialis.form.Record={};
SetupSubSpesialis.form.Form={};
SetupSubSpesialis.form.Grid={};
SetupSubSpesialis.form.Panel={};
SetupSubSpesialis.form.TextField={};
SetupSubSpesialis.form.Button={};

SetupSubSpesialis.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_sub_spc', 'spesialisasi'],
	data: []
});

CurrentPage.page = dataGrid_viSetupSubSpesialis(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dataGrid_viSetupSubSpesialis(mod_id_viSetupSubSpesialis){	
 	// Field kiriman dari Project Net.
    var FieldMaster_viSetupSubSpesialis = 
	[
		'kd_sub_spc', 'spesialisasi' 
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viSetupSubSpesialis = new WebApp.DataStore
	({
        fields: FieldMaster_viSetupSubSpesialis
    });
    load_data_spesialis();
    // Grid lab Perencanaan # --------------
	GridDataView_viSetupSubSpesialis = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viSetupSubSpesialis,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:true,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viSetupSubSpesialis = undefined;
							rowSelected_viSetupSubSpesialis = dataSource_viSetupSubSpesialis.getAt(row);
							CurrentData_viSetupSubSpesialis
							CurrentData_viSetupSubSpesialis.row = row;
							CurrentData_viSetupSubSpesialis.data = rowSelected_viSetupSubSpesialis.data;
							DataInitSetupSubSpesialis(rowSelected_viSetupSubSpesialis.data);
							//DataInitSetupSubSpesialis(rowSelected_viSetupSubSpesialis.data);
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viSetupSubSpesialis = dataSource_viSetupSubSpesialis.getAt(ridx);
					if (rowSelected_viSetupSubSpesialis != undefined)
					{
						DataInitSetupSubSpesialis(rowSelected_viSetupSubSpesialis.data);
						//setLookUp_viSetupSubSpesialis(rowSelected_viSetupSubSpesialis.data);
					}
					else
					{
						//setLookUp_viSetupSubSpesialis();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Setup Unit
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header: 'Kode Sub',
						dataIndex: 'kd_sub_spc',
						hideable:false,
						menuDisabled: true,
						width: 40
						
					},
					//-------------- ## --------------
					{
						header: 'Sub Spesialisasi',
						dataIndex: 'sub_spesialisasi',
						hideable:false,
						menuDisabled: true,
						width: 90
					},
					//-------------- ## --------------
					{
						header: 'Spesialisasi',
						dataIndex: 'spesialisasi',
						hideable:false,
						menuDisabled: true,
						width: 90
					},
					//-------------- ## --------------
					{
						header: 'Kode spesial',
						dataIndex: 'kd_spesial',
						hideable:false,
						menuDisabled: true,
						width: 40,
						hidden:true
						
					},
					//-------------- ## --------------
				]
			),
			/* tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viSetupSubSpesialis',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viSetupSubSpesialis',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viSetupSubSpesialis != undefined)
							{
								DataInitSetupSubSpesialis(rowSelected_viSetupSubSpesialis.data);
							}
							
						}
					}
					//-------------- ## --------------
				]
			}, */
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viSetupSubSpesialis, selectCount_viSetupSubSpesialis, dataSource_viSetupSubSpesialis),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)	
	var PanelTabSetupSubSpesialis = new Ext.FormPanel({
        labelAlign: 'top',
		layout:'form',
		border: false,  
		autoHeight:true,
        title: '',
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [PanelInputUnit()]
		
	})
	// Kriteria filter pada Grid # --------------
    var FrmData_viSetupSubSpesialis = new Ext.Panel
    (
		{
			title: NamaForm_viSetupSubSpesialis,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viSetupSubSpesialis,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ PanelTabSetupSubSpesialis,
					GridDataView_viSetupSubSpesialis],
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAddAsisten_viSetupSubSpesialis',
						handler: function(){
							AddNewSetupSubSpesialis();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viSetupSubSpesialis',
						handler: function()
						{
							loadMask.show();
							dataSave_viSetupSubSpesialis();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viSetupSubSpesialis',
						handler: function()
						{
							Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
								if (button == 'yes'){
									loadMask.show();
									dataDelete_viSetupSubSpesialis();
								}
							});
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_viSetupPabrik',
						handler: function()
						{
							dataSource_viSetupSubSpesialis.removeAll();
							dataGriSetupSubSpesialis();
						}
					},
					{
						xtype: 'tbseparator'
					}
					
				]
			}
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_viSetupSubSpesialis;
    //-------------- # End form filter # --------------
}

function PanelInputUnit(rowdata){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 85,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'Kode Sub'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 0,
								xtype: 'textfield',
								id: 'txtKdSub_SetupSubSpesialis',
								name: 'txtKdSub_SetupSubSpesialis',
								width: 100,
								allowBlank: false,
								readOnly: true,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									},
									
								}
							},
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'Spesialisasi'
							},
							{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							comboSpesialis(),
							{
								x: 10,
								y: 60,
								xtype: 'label',
								text: 'Sub Spesialisasi'
							},
							{
								x: 120,
								y: 60,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 60,
								xtype: 'textfield',
								id: 'txtSubSpesialisasi_SetupSubSpesialis',
								name: 'txtSubSpesialisasi_SetupSubSpesialis',
								width: 200,
								allowBlank: false,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									},
									
								}
							},
							
							
						]
					}
				]
			}
		]		
	};
        return items;
}
//------------------end---------------------------------------------------------

function load_data_spesialis(param)
{

	Ext.Ajax.request(
	{
		url: baseURL + "index.php/kamar_operasi/functionSetupSubSpesialis/getSpesialisasi",
		params:{
			command: param
		} ,
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
		success: function(o) {
			cboSpesialisasi_SetupSubSpesialisasi.store.removeAll();
				var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsspesialisasi_SetupSubSpesialisasi.recordType;
				var o=cst['listData'][i];
				
				recs.push(new recType(o));
				dsspesialisasi_SetupSubSpesialisasi.add(recs);
				//console.log(o);
			}
		}
	});
}

function comboSpesialis()
{ 
 
	var Field = ['kd_spesial','spesialisasi'];

    dsspesialisasi_SetupSubSpesialisasi = new WebApp.DataStore({ fields: Field });
	
	load_data_spesialis();
	cboSpesialisasi_SetupSubSpesialisasi= new Ext.form.ComboBox
	(
		{
			x				: 130,
			y				: 30,
			id				: 'cboSpesialisasi_SetupSubSpesialisasi',
			typeAhead		: true,
			triggerAction	: 'all',
			lazyRender		: true,
			mode			: 'local',
			emptyText		: '',
			fieldLabel		:  '',
			align			: 'Right',
			width			: 170,
			store			: dsspesialisasi_SetupSubSpesialisasi,
			valueField		: 'kd_spesial',
			displayField	: 'spesialisasi',
			//hideTrigger		: true,
			listeners:
			{
				select	: function(a,b,c){
					dataGriSetupSubSpesialis(b.data.kd_spesial);	
				},
				keyUp: function(a,b,c){
					$this1=this;
					if((b.getKey()>=65 && b.getKey()<=90)|| (b.getKey()>=48 && b.getKey()<=57) || b.getKey()==32 || b.getKey()==8 || b.getKey()==46){
						clearTimeout(this.time);
					
						this.time=setTimeout(function(){
							if($this1.lastQuery != '' ){
								var value=$this1.lastQuery;
								var param={};
								param['text']=$this1.lastQuery;
								load_data_spesialis($this1.lastQuery);

								
							}
						},1000);
					}
				},
				
			}
		}
	);return cboSpesialisasi_SetupSubSpesialisasi;
};

function dataGriSetupSubSpesialis(kd_spesial){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/kamar_operasi/functionSetupSubSpesialis/getItemGrid",
			params: {
				kd_spesial:kd_spesial
			},
			failure: function(o)
			{
				ShowPesanErrorSetupSubSpesialis('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dataSource_viSetupSubSpesialis.removeAll();
					var recs=[],
						recType=dataSource_viSetupSubSpesialis.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dataSource_viSetupSubSpesialis.add(recs);
					
					
					
					GridDataView_viSetupSubSpesialis.getView().refresh();
				}
				else 
				{
					ShowPesanErrorSetupSubSpesialis('Gagal membaca data items', 'Error');
				};
			}
		}
		
	)
	
}

function dataSave_viSetupSubSpesialis(){
	if (ValidasiSaveSetupSubSpesialis(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/kamar_operasi/functionSetupSubSpesialis/save",
				params: getParamSaveSetupSubSpesialis(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupSubSpesialis('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupSubSpesialis('Berhasil menyimpan data ini','Information');
						Ext.getCmp('txtKdSub_SetupSubSpesialis').setValue(cst.kode);
						dataSource_viSetupSubSpesialis.removeAll();
						dataGriSetupSubSpesialis();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupSubSpesialis('Gagal menyimpan data ini', 'Error');
						dataSource_viSetupSubSpesialis.removeAll();
						dataGriSetupSubSpesialis();
					};
				}
			}
			
		)
	}
}

function dataDelete_viSetupSubSpesialis(){
	if (ValidasiSaveSetupSubSpesialis(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/kamar_operasi/functionSetupSubSpesialis/delete",
				params: getParamDeleteSetupSubSpesialis(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupSubSpesialis('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupSubSpesialis('Berhasil menghapus data ini','Information');
						AddNewSetupSubSpesialis()
						dataSource_viSetupSubSpesialis.removeAll();
						dataGriSetupSubSpesialis();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupSubSpesialis('Gagal menghapus data ini', 'Error');
						dataSource_viSetupSubSpesialis.removeAll();
						dataGriSetupSubSpesialis();
					};
				}
			}
			
		)
	}
}

function AddNewSetupSubSpesialis(){
	Ext.getCmp('txtKdSub_SetupSubSpesialis').setValue('');
	Ext.getCmp('cboSpesialisasi_SetupSubSpesialisasi').setValue('');
	Ext.getCmp('txtSubSpesialisasi_SetupSubSpesialis').setValue('');
};

function DataInitSetupSubSpesialis(rowdata){
	Ext.getCmp('txtKdSub_SetupSubSpesialis').setValue(rowdata.kd_sub_spc);
	Ext.getCmp('cboSpesialisasi_SetupSubSpesialisasi').setValue(rowdata.kd_spesial);
	Ext.getCmp('txtSubSpesialisasi_SetupSubSpesialis').setValue(rowdata.sub_spesialisasi);
};

function getParamSaveSetupSubSpesialis(){
	var	params =
	{
		KdSub:Ext.getCmp('txtKdSub_SetupSubSpesialis').getValue(),
		KdSpesial:Ext.getCmp('cboSpesialisasi_SetupSubSpesialisasi').getValue(),
		SubSpesialisasi:Ext.getCmp('txtSubSpesialisasi_SetupSubSpesialis').getValue()
	}
   
    return params
};

function getParamDeleteSetupSubSpesialis(){
	var	params =
	{
		KdSub:Ext.getCmp('txtKdSub_SetupSubSpesialis').getValue()
	}
   
    return params
};

function ValidasiSaveSetupSubSpesialis(modul,mBolHapus){
	var x = 1;
	if(Ext.getCmp('cboSpesialisasi_SetupSubSpesialisasi').getValue() === ''){
		if(Ext.getCmp('cboSpesialisasi_SetupSubSpesialisasi').getValue() === ''){
			loadMask.hide();
			ShowPesanWarningSetupSubSpesialis('Spesialisasi masih kosong', 'Warning');
			x = 0;
		}
		
	} 
	if(Ext.getCmp('txtSubSpesialisasi_SetupSubSpesialis').getValue() === ''){
		if(Ext.getCmp('txtSubSpesialisasi_SetupSubSpesialis').getValue() === ''){
			loadMask.hide();
			ShowPesanWarningSetupSubSpesialis('Sub spesialisasi masih kosong', 'Warning');
			x = 0;
		}
		
	} 
	return x;
};



function ShowPesanWarningSetupSubSpesialis(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorSetupSubSpesialis(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoSetupSubSpesialis(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};