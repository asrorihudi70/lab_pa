var dsvJenisOperasi;
var dsvJenisOperasi;
var rowselectedJenisOperasi;
var LookupJenisOperasi;
var disDataJenisOperasi=0;
var cek=0;
var kdSetupJenisOperasi;
var kdJenisOperasiJenisOperasi;
var kodeJenisOperasi;
var stateComboJenisOperasi=['KD_JenisOperasi_JenisOperasi','JenisOperasi_JenisOperasi'];
var pilihAksiJenisOperasi;
var SetupJenisOperasi={};
SetupJenisOperasi.form={};
SetupJenisOperasi.func={};
SetupJenisOperasi.vars={};
SetupJenisOperasi.func.parent=SetupJenisOperasi;
SetupJenisOperasi.form.ArrayStore={};
SetupJenisOperasi.form.ComboBox={};
SetupJenisOperasi.form.DataStore={};
SetupJenisOperasi.form.Record={};
SetupJenisOperasi.form.Form={};
SetupJenisOperasi.form.Grid={};
SetupJenisOperasi.form.Panel={};
SetupJenisOperasi.form.TextField={};
SetupJenisOperasi.form.Button={};

SetupJenisOperasi.form.ArrayStore.JenisOperasi=new Ext.data.ArrayStore({
		id: 0,
		fields: [	
					'kd_jenis','jenis_diet','harga_pokok'
				],
		data: []
	});
	
CurrentPage.page = getPanelJenisOperasi(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dsJenisOperasi(){
	dsvJenisOperasi.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'vJenisOperasi'
                    //param : kriteria
                }			
            }
        );   
    return dsvJenisOperasi;
	}

	
function getPanelJenisOperasi(mod_id) {
    var Field = ['kd_jenis_op','jenis_op'];
    dsvJenisOperasi = new WebApp.DataStore({
        fields: Field
    });
	dsJenisOperasi();
    var gridListHasilJenisOperasi = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dsvJenisOperasi,
        anchor: '100% 70%',
        columnLines: false,
        autoScroll: false,
        border: true,
        sort: false,
        autoHeight: false,
        layout: 'fit',
        region: 'center',
        height: 200,
        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
            listeners: {
                rowselect: function (sm, row, rec) {
					rowselectedJenisOperasi=dsvJenisOperasi.getAt(row);
                }
            }
        }),
        listeners: {
           rowdblclick: function (sm, ridx, cidx) {
                rowselectedJenisOperasi=dsvJenisOperasi.getAt(ridx);
				ShowLookupJenisOperasi(rowselectedJenisOperasi.json);
				/*disData=1;
				disabled_data(disData);*/
				
            }
        },
        cm: new Ext.grid.ColumnModel(
                [
				new Ext.grid.RowNumberer({
					header:'No.'
					}),
                    {
                        id: 'colKdJenisOperasiViewHasilJenisOperasi',
                        header: 'Kode',
                        dataIndex: 'kd_jenis_op',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 5
                    },
                    {
                        id: 'colJenisOperasiViewHasilJenisOperasi',
                        header: 'Nama Jenis',
                        dataIndex: 'jenis_op',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
					
                ]
                ),
        viewConfig: {
            forceFit: true
        },
        tbar:[
			{
                id: 'btnEditDataJenisOperasi',
                text: 'Edit Data',
                tooltip: 'Edit Data',
                iconCls: 'Edit_Tr',
                handler: function (sm, row, rec) {
                    edit_dataJenisOperasi();
                },
				
            },
		]
    });
	   var FormDepanJenisOperasi = new Ext.Panel({
        id: mod_id,
        closable: true,
        region: 'center',
        layout: 'form',
        title: 'Setup Jenis',
        border: false,
        shadhow: true,
        autoScroll: false,
		width: 250,
        iconCls: 'Request',
        margins: '0 5 5 0',
		tbar: [
			
			{
                id: 'btnAddNewDataJenisOperasi',
                text: 'Add New',
                tooltip: 'addnewJenisOperasi',
                iconCls: 'add',
                handler: function (sm, row, rec) {
                    addnew_dataJenisOperasi();
				}
            },
			{
        	xtype: 'tbseparator'
       		},
			{
                id: 'btnSaveDataJenisOperasi',
                text: 'Save Data',
                tooltip: 'Save Data',
                iconCls: 'Save',
                handler: function (sm, row, rec) {
                    save_dataJenisOperasi();
					//alert(cek);
                },
				
            },
			{
        	xtype: 'tbseparator'
       		},
			{
                id: 'btnDeleteDataJenisOperasi',
                text: 'Delete Data',
                tooltip: 'Delete Data',
                iconCls: 'Remove',
                handler: function (sm, row, rec) {
					if (rowselectedJenisOperasi!=undefined)
                    	delete_dataJenisOperasi();
					else
						ShowPesanErrorJenisOperasi('Maaf ! Tidak ada data yang terpilih', 'Error');
				}
            },
			{
        	xtype: 'tbseparator'
       		},
			{
                id: 'btnRefreshDataJenisOperasi',
                text: 'Refresh',
                tooltip: 'Refresh',
                iconCls: 'Refresh',
                handler: function (sm, row, rec) {
                    refresh_dataJenisOperasi();
                },
				
            }
			],
        items: [
			getPanelPencarianJenisOperasi(),
			gridListHasilJenisOperasi,
            
            
//            PanelPengkajian(),
//            TabPanelPengkajian()
        ],
        listeners: {
            'afterrender': function () {

            }
        }
    });

    return FormDepanJenisOperasi;

}
;
function refresh_dataJenisOperasi()
{
	dsJenisOperasi();
}
function getPanelPencarianJenisOperasi() {
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 1190,
                height: 80,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'Kode'
                    },
                    {
                        x: 110,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 10,
                        xtype: 'textfield',
                        name: 'TxtKdJenisOperasi',
                        id: 'TxtKdJenisOperasi',
                        width: 80,
						disabled: true
                    },
					
                    
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Nama Jenis'
                    },
                    {
                        x: 110,
                        y: 40,
                        xtype: 'label',
                        text: ' : '
                    },
					
					SetupJenisOperasi.form.ComboBox.JenisOperasi= new Nci.form.Combobox.autoComplete({
						//id:'TxtNamaJenisJenisOperasiome',
						store	: SetupJenisOperasi.form.ArrayStore.JenisOperasi,
						select	: function(a,b,c){
							Ext.getCmp('TxtKdJenisOperasi').setValue(b.data.kd_jenis_op);
							/* var getKode_Gz=b.data.kd_jenis
							alert(getKode_Gz); */
							 /* Ext.Ajax.request
								(
									{
										url: baseURL + "index.php/gizi/JenisOperasi/getJenisOperasi/",
										params:{getKode_Gz:b.data.kd_jenis},
										failure: function(o)
										{
											ShowPesanErrorJenisOperasi('Hubungi Admin', 'Error');
										},	
										success: function(o) 
										{
											console.log(o.responseText);
											 var cst = Ext.decode(o.responseText);
											if (cst.listData != '') 
											{	
												Ext.getCmp('TxtKdJenisOperasi').setValue(cst.dataKode);
												Ext.getCmp('CmbSetJenisOperasi').setValue(cst.dataKodeJenisOperasi);
												if (cst.dataTagBerlaku==0)
													{Ext.getCmp('CekBerlakuJenisOperasi').setValue(false);}
												else
													{Ext.getCmp('CekBerlakuJenisOperasi').setValue(true);}
												
												Ext.getCmp('TxtHrgPokokJenisOperasi').setValue(cst.dataHargapokok);
												Ext.getCmp('TxtHrgJualJenisOperasi').setValue(cst.dataHargaJual);
												kdSetupJenisOperasi=cst.dataKode;
												kodeJenisOperasi=cst.dataKodeJenisOperasi;
												pilihAksiJenisOperasi='update';
											}
											else 
											{
												Ext.getCmp('CmbSetJenisOperasi').setValue('');
												Ext.getCmp('TxtKdJenisOperasi').setValue(b.data.kd_jenis);
												Ext.getCmp('TxtHrgPokokJenisOperasi').setValue(b.data.harga_pokok);
												Ext.getCmp('CekBerlakuJenisOperasi').setValue(false);
												Ext.getCmp('TxtHrgPokokJenisOperasi').setValue('');
												Ext.getCmp('TxtHrgJualJenisOperasi').setValue('');
												kdSetupJenisOperasi=cst.dataKode;
												kodeJenisOperasi=cst.dataKodeJenisOperasi;
												pilihAksiJenisOperasi='insert';
											}; 
										}
									}
									
								) */ 
						},
						width	: 200,
						
						insert	: function(o){
							return {
								kd_jenis_op        	:o.kd_jenis_op,
								jenis_op        	:o.jenis_op,
								text				:  '<table style="font-size: 11px;"><tr><td width="80">'+o.kd_jenis_op+'</td><td width="180">'+o.jenis_op+'</td></tr></table>',
							}
						},
						url		: baseURL + "index.php/kamar_operasi/functionSetupJenisOperasi/getJenisGrid",
						valueField: 'jenis_op',
						displayField: 'text',
						listWidth: 260,
						x: 120,
                        y: 40,
						
					}),
					
                ]
            }
        ]
    };
    return items;
}
;
function ComboSetSetupJenisOperasi()
{
    dsvJenisOperasi = new WebApp.DataStore({fields: stateComboJenisOperasi});
	dsJenisOperasi();
	var CmbSetSetupJenisOperasi = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 70,
			id:'CmbSetJenisOperasi',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			/*readOnly:true,
			emptyText:'Set JenisOperasi',
			value: 1,*/
			width: 150,
			store: dsvJenisOperasi,
			valueField: stateComboJenisOperasi[0],
			displayField: stateComboJenisOperasi[1],
			//value:selectSetPilihankelompokPasien,
			listeners:
			{
				'select': function(a,b,c)
				{
					kdJenisOperasiJenisOperasi=CmbSetSetupJenisOperasi.value;
					//selectSetPilihankelompokPasien=b.data.displayText;
				}
			}
		}
	);
	return CmbSetSetupJenisOperasi;
}
function getData_Gz(getkode_Gz)
{
	Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/gizi/JenisOperasi/getJenisOperasi/"+getkode_Gz,
				//params: ParameterGetKode_Gz(getkode_Gz),
				failure: function(o)
				{
					ShowPesanErrorJenisOperasi('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanSuksesJenisOperasi("Data Berhasil Disimpan","Sukses");
						dsJenisOperasi();
						
					}
					else 
					{
						ShowPesanErrorJenisOperasi('Gagal Menyimpan Data ini', 'Error');
						dsJenisOperasi();
					};
				}
			}
			
		)
}
function save_dataJenisOperasi()
{
	if (ValidasiEntriJenisOperasi(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/kamar_operasi/functionSetupJenisOperasi/save",
				params: ParameterSaveJenisOperasi(),
				failure: function(o)
				{
					ShowPesanErrorJenisOperasi('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanSuksesJenisOperasi("Data Berhasil Disimpan","Sukses");
						dsJenisOperasi();
						Ext.getCmp("TxtKdJenisOperasi").setValue(cst.kode);
					}
					else 
					{
						ShowPesanErrorJenisOperasi('Gagal Menyimpan Data ini', 'Error');
						dsJenisOperasi();
					};
				}
			}
			
		)
	}
}
function edit_dataJenisOperasi(){
	ShowLookupJenisOperasi(rowselectedJenisOperasi.json);
}
function delete_dataJenisOperasi() 
{
		Ext.Msg.show
		(
			{
			   title:nmHeaderHapusData,
			   msg: nmGetValidasiHapus(nmEmp2) ,
			   buttons: Ext.MessageBox.YESNO,
			   width:250,
			   fn: function (btn) 
			   {			
					if (btn === 'yes') 
					{
						Ext.Ajax.request
						(
							{
								url: baseURL + "index.php/kamar_operasi/functionSetupJenisOperasi/delete",
								params: paramsDeleteJenisOperasi(rowselectedJenisOperasi.json),
								success: function(o) 
								{
									var cst = Ext.decode(o.responseText);
									if (cst.success === true) 
									{
										ShowPesanSuksesJenisOperasi(nmPesanHapusSukses,nmHeaderHapusData);
										dsJenisOperasi();
										ClearTextJenisOperasi();
									}
									else if (cst.success === false && cst.pesan===0)
									{
										ShowPesanErrorJenisOperasi(nmPesanHapusGagal,nmHeaderHapusData);
									}
									else 
									{
										ShowPesanErrorJenisOperasi(nmPesanHapusError,nmHeaderHapusData);
									};
								}
							}
						)
					};
				}
			}
		)
	};
	
function addnew_dataJenisOperasi()
{
	ClearTextJenisOperasi();
}
function ValidasiEntriJenisOperasi(modul,mBolHapus)
{
	var kode = Ext.getCmp('TxtKdJenisOperasi').getValue();
	var nama = SetupJenisOperasi.form.ComboBox.JenisOperasi.getValue();
	
	var x = 1;
	if(nama === '' ){
	ShowPesanErrorJenisOperasi('Data Belum Diisi Lengkap', 'Warning');
	x = 0;
	}/*
	if( nama === '')
	{
	ShowPesanError('Data Belum Diisi Lengkap', 'Warning');
	x = 0;	
	}*/
	
	
	return x;
};
function ValidasiEditJenisOperasi(modul,mBolHapus)
{
	var kode = Ext.getCmp('TxtKdJenisOperasi').getValue();
	var nama = SetupJenisOperasi.form.ComboBox.JenisOperasi.getValue();
	
	var x = 1;
	if(kode === '' ){
	ShowPesanErrorJenisOperasi('Data Belum Diisi Lengkap', 'Warning');
	x = 0;
	}
	if( nama === '')
	{
	ShowPesanErrorJenisOperasi('Data Belum Diisi Lengkap', 'Warning');
	x = 0;	
	}
	return x;
};

function ShowPesanSuksesJenisOperasi(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};

function ShowPesanErrorJenisOperasi(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:290
		}
	);
};
function ClearTextJenisOperasi()
{
	Ext.getCmp('TxtKdJenisOperasi').setValue("");
	SetupJenisOperasi.form.ComboBox.JenisOperasi.setValue("");
	SetupJenisOperasi.form.ComboBox.JenisOperasi.focus();
}
/*function disabled_data(disDatax){
	if (disDatax==1) {
		var dis= 
				{
					kodeData : Ext.getCmp('TxtKdAhliGz').disable(),
				}
	}
	else if (disDatax==0) 
	{
		var dis= 
				{
					kodeData : Ext.getCmp('TxtKdAhliGz').enable(),
				}
	}
	return dis;
	}*/
function ParameterGetKode_Gz(getkode_Gz)
{
	var params =
	{
		Table:'vJenisOperasi',
		kode_Gz: getkode_Gz
	}
}
function ParameterSaveJenisOperasi()
{
	
var params =
	{
	KdJenis : Ext.getCmp('TxtKdJenisOperasi').getValue(),
	Nama : SetupJenisOperasi.form.ComboBox.JenisOperasi.getValue()
	}	
return params;	
}
function paramsDeleteJenisOperasi(rowdata) 
{
    var params =
	{
		kode :  rowdata.kd_jenis_op,
		//Data: ShowLookupDeleteJenisOperasi(rowselectedJenisOperasi.json)
	};
    return params
};
function ShowLookupJenisOperasi(rowdata)
{
	if (rowdata == undefined){
        ShowPesanErrorJenisOperasi('Data Kosong', 'Error');
    }
    else
    {
      datainit_formJenisOperasi(rowdata); 
    }
}
function ShowLookupDeleteJenisOperasi(rowdata)
{
      var kode =  rowdata.KD__;
	  var kodeJen = rowdata.KD_JENIS_;
	  return kode;
}
function datainit_formJenisOperasi(rowdata){
	Ext.getCmp('TxtKdJenisOperasi').setValue(rowdata.kd_jenis_op);
	SetupJenisOperasi.form.ComboBox.JenisOperasi.setValue(rowdata.jenis_op);
	//pilihAksiJenisOperasi='update';
	}