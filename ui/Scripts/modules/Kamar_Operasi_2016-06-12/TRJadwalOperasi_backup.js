var dataSource_viJdwlOperasi;
var selectCount_viJdwlOperasi=50;
var NamaForm_viJdwlOperasi="Jadwal Operasi ";
var mod_name_viJdwlOperasi="viJadwalOperasi";
var now_viJdwlOperasi= new Date();
var addNew_viJdwlOperasi;
var rowSelected_viJdwlOperasi;
var setLookUps_viJdwlOperasi;
var mNoKunjungan_viJdwlOperasi='';

var CurrentData_viJdwlOperasi =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viJdwlOperasi(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

// --------------------------------------- # Start Function # ---------------------------------------
// ## Silahkan tempatkan semua fungsi eksekusi disini

// --------------------------------------- # End Function # ---------------------------------------

// --------------------------------------- # Start Form # ---------------------------------------

/**
*	Function : dataGrid_viJdwlOperasi
*	
*	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
*/

function dataGrid_viJdwlOperasi(mod_id_viJdwlOperasi)
{	
    // Field kiriman dari Project Net.
    var FieldMaster_viJdwlOperasi = 
	[
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viJdwlOperasi = new WebApp.DataStore
	({
        fields: FieldMaster_viJdwlOperasi
    });
    
    // Grid # --------------
	var GridDataView_viJdwlOperasi = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viJdwlOperasi,
			autoScroll: true,
			columnLines: true,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viJdwlOperasi = undefined;
							rowSelected_viJdwlOperasi = dataSource_viJdwlOperasi.getAt(row);
							CurrentData_viJdwlOperasi
							CurrentData_viJdwlOperasi.row = row;
							CurrentData_viJdwlOperasi.data = rowSelected_viJdwlOperasi.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viJdwlOperasi = dataSource_viJdwlOperasi.getAt(ridx);
					if (rowSelected_viJdwlOperasi != undefined)
					{
						setLookUp_viJdwlOperasi(rowSelected_viJdwlOperasi.data);
					}
					else
					{
						setLookUp_viJdwlOperasi();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colNoMedrec_viJdwlOperasi',
						header: 'No. Medrec',
						dataIndex: 'KD_PASIEN',
						sortable: true,
						width: 35,
						filter:
						{
							type: 'int'
						}
					},
					//-------------- ## --------------
					{
						id: 'colNMPASIEN_viJdwlOperasi',
						header: 'Nama',
						dataIndex: 'NAMA',
						sortable: true,
						width: 35,
						filter:
						{}
					},
					//-------------- ## --------------
					{
						id: 'colALAMAT_viJdwlOperasi',
						header:'Alamat',
						dataIndex: 'ALAMAT',
						width: 60,
						sortable: true,
						filter: {}
					},
					//-------------- ## --------------
					{
						id: 'colTglKunj_viJdwlOperasi',
						header:'Tgl Kunjungan',
						dataIndex: 'TGL_KUNJUNGAN',						
						width: 50,
						sortable: true,
						// format: 'd/M/Y',
						filter: {},
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.TGL_KUNJUNGAN);
						}
					}
					// ,
					//-------------- ## --------------
					// {
						// id: 'colPoliTujuan_viJdwlOperasi',
						// header:'Poli Tujuan',
						// dataIndex: 'NAMA_UNIT',
						// width: 50,
						// sortable: true,
						// filter: {}
					// }
					//-------------- ## --------------
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viJdwlOperasi',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viJdwlOperasi',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viJdwlOperasi != undefined)
							{
								setLookUp_viJdwlOperasi(rowSelected_viJdwlOperasi.data)
							}
							else
							{								
								setLookUp_viJdwlOperasi();
							}
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			bbar : bbar_paging(mod_name_viJdwlOperasi, selectCount_viJdwlOperasi, dataSource_viJdwlOperasi),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viJdwlOperasi = new Ext.Panel
    (
		{
			title: NamaForm_viJdwlOperasi,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viJdwlOperasi,
			region: 'center',
			layout: 'form', 
			closable: true,        
			border: false,  
			margins: '0 5 5 0',
			items: [GridDataView_viJdwlOperasi],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viJdwlOperasi,
		            columns: 9,
		            defaults: {
		                scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
			            { 
							xtype: 'tbtext', 
							text: 'Jenis Transaksi : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},
						viComboJenisTransaksi_viJdwlOperasi(125, "ComboJenisTransaksi_viJdwlOperasi"),	
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'No. : ', 
							style:{'text-align':'right'},
							width: 45,
							height: 25
						},
						{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_NoMedrec_viJdwlOperasi',
							emptyText: 'No. Medrec',
							width: 300,
							height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										DfltFilterBtn_viJdwlOperasi = 1;
										DataRefresh_viJdwlOperasi(getCriteriaFilterGridDataView_viJdwlOperasi());
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Tgl Kunjungan : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},
						{
							xtype: 'datefield',
							id: 'txtfilterTglKunjAwal_viJdwlOperasi',
							value: now_viJdwlOperasi,
							format: 'd/M/Y',
							width: 120,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viJdwlOperasi();								
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Kelompok : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},
						viComboJenisTransaksi_viJdwlOperasi(125, "ComboKelompok_viJdwlOperasi"),	
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Nama : ', 
							style:{'text-align':'right'},
							width: 45,
							height: 25
						},
						{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_NAMA_viJdwlOperasi',
							emptyText: 'Nama',
							width: 300,
							height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										DfltFilterBtn_viJdwlOperasi = 1;
										DataRefresh_viJdwlOperasi(getCriteriaFilterGridDataView_viJdwlOperasi());								
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 's/d Tgl : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},
						{
							xtype: 'datefield',
							id: 'txtfilterTglKunjAkhir_viJdwlOperasi',
							value: now_viJdwlOperasi,
							format: 'd/M/Y',
							width: 120,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viJdwlOperasi();								
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{
							xtype: 'button',
							text: 'Cari',
							iconCls: 'refresh',
							tooltip: 'Cari',
							// style:{paddingLeft:'30px'},
							//rowspan: 3,
							width:150,
							id: 'BtnFilterGridDataView_viJdwlOperasi',
							handler: function() 
							{					
								DfltFilterBtn_viJdwlOperasi = 1;
								DataRefresh_viJdwlOperasi(getCriteriaFilterGridDataView_viJdwlOperasi());
							}                        
						},
						//-------------- ## --------------
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viJdwlOperasi;
    //-------------- # End form filter # --------------
}
// End Function dataGrid_viJdwlOperasi # --------------

function viComboJenisTransaksi_viJdwlOperasi(lebar,Nama_ID)
{
  var cbo_viComboJenisTransaksi_viJdwlOperasi = new Ext.form.ComboBox
    (
    
        {
            id:Nama_ID,
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'Pilih Transaksi..',
            fieldLabel: "Transaksi",
            value:1,        
            width:lebar,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields: 
                    [
                        'IdTrnsksi',
                        'displayTextTrnsksi'
                    ],
                    data: [['all', "Semua"],[1, "Bayar"],[2, "Belum Bayar"]]
                }
            ),
            valueField: 'IdTrnsksi',
            displayField: 'displayTextTrnsksi',
            listeners:  
            {
            }
        }
    );
    return cbo_viComboJenisTransaksi_viJdwlOperasi;
};

/**
*	Function : setLookUp_viJdwlOperasi
*	
*	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
*/

function setLookUp_viJdwlOperasi(rowdata)
{
    var lebar = 760;
    setLookUps_viJdwlOperasi = new Ext.Window
    (
    {
        id: 'SetLookUps_viJdwlOperasi',
		name: 'SetLookUps_viJdwlOperasi',
        title: NamaForm_viJdwlOperasi, 
        closeAction: 'destroy',        
        width: 775,
        height: 580,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viJdwlOperasi(lebar,rowdata), //1
        listeners:
        {
            activate: function()
            {
				
            },
            afterShow: function()
            {
                this.activate();
            },
            deactivate: function()
            {
                rowSelected_viJdwlOperasi=undefined;
                //datarefresh_viJdwlOperasi();
				mNoKunjungan_viJdwlOperasi = '';
            }
        }
    }
    );

    setLookUps_viJdwlOperasi.show();
    if (rowdata == undefined)
    {
        // dataaddnew_viJdwlOperasi();
		// Ext.getCmp('btnDelete_viJdwlOperasi').disable();	
    }
    else
    {
        // datainit_viJdwlOperasi(rowdata);
    }
}
// End Function setLookUpGridDataView_viJdwlOperasi # --------------

/**
*	Function : getFormItemEntry_viJdwlOperasi
*	
*	Sebuah fungsi untuk menampilkan penataan menu dan isian form edit
*/

function getFormItemEntry_viJdwlOperasi(lebar,rowdata)
{
    var pnlFormDataBasic_viJdwlOperasi = new Ext.FormPanel
    (
		{
			title: '',
			region: 'north',
			layout: 'form',
			bodyStyle: 'padding:10px 10px 10px 10px',
			anchor: '100%',
			width: lebar,
			border: false,
			//-------------- #items# --------------
			items:
			[
				getItemPanelInformasi_viJdwlOperasi(lebar),
				getItemPanelInputJadwal_viJdwlOperasi(lebar), 				
				getItemGridTransaksi_viJdwlOperasi(lebar)
			],
			//-------------- #End items# --------------
			fileUpload: true,
			// Tombol pada tollbar Edit Data Pasien
			tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						id: 'btnAdd_viJdwlOperasi',
						handler: function(){
							dataaddnew_viJdwlOperasi();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viJdwlOperasi',
						handler: function()
						{
							datasave_viJdwlOperasi(addNew_viJdwlOperasi);
							datarefresh_viJdwlOperasi();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						id: 'btnSimpanExit_viJdwlOperasi',
						handler: function()
						{
							var x = datasave_viJdwlOperasi(addNew_viJdwlOperasi);
							datarefresh_viJdwlOperasi();
							if (x===undefined)
							{
								setLookUps_viJdwlOperasi.close();
							}
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viJdwlOperasi',
						handler: function()
						{
							datadelete_viJdwlOperasi();
							datarefresh_viJdwlOperasi();							
						}
					},
					//-------------- ## --------------
					{
						xtype:'tbseparator'
					}
					//-------------- ## --------------					
				]
			}//,items:
		}
    )

    return pnlFormDataBasic_viJdwlOperasi;
}
// End Function getFormItemEntry_viJdwlOperasi # --------------

/**
*	Function : getItemPanelInputJadwal_viJdwlOperasi
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function viComboKamar_viJdwlOperasi()
{
  var cbo_viComboKamar_viJdwlOperasi = new Ext.form.ComboBox
    (
    
        {
            id:"cbo_viComboKamar_viJdwlOperasi",
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'Pilih Kamar..',
            fieldLabel: "Kamar",
            value:1,        
            width:430,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields: 
                    [
                        'IdKamar',
                        'displayTextKamar'
                    ],
                    data: [[1, "011 VVIP"],[2, "012 VVIP"]]
                }
            ),
            valueField: 'IdKamar',
            displayField: 'displayTextKamar',
            listeners:  
            {
            }
        }
    );
    return cbo_viComboKamar_viJdwlOperasi;
};

function getItemPanelInformasi_viJdwlOperasi(lebar) 
{
	var rdJenisModul_viJdwlOperasi = new Ext.form.RadioGroup
	({  
        columns: 2, 
        width: 250,
        items: 
		[  
			{
				boxLabel: 'Rawat Jalan/UGD', 
				width:50,
				id: 'rdRWJ_viJdwlOperasi', 
				name: 'rdRWJ_viJdwlOperasi', 
				inputValue: '1'
			},  
			//-------------- ## --------------
			{
				boxLabel: 'Rawat Inap', 
				width:50,
				id: 'rdRWI_viJdwlOperasi', 
				name: 'rdRWI_viJdwlOperasi', 
				inputValue: '2'
			} 
			//-------------- ## --------------
        ]  
    });
	
    var items =
	{
	    layout: 'Form',
	    anchor: '100%',
		title: 'Informasi Jadwal',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		border:true,
		items:
		[	
			{
				xtype: 'compositefield',
				fieldLabel: 'Asal',
				anchor: '100%',
				width: 199,
				items: 
				[
					rdJenisModul_viJdwlOperasi	
				]
			},
			{
				xtype: 'compositefield',
				fieldLabel: 'Tgl. Operasi',
				anchor: '100%',
				width: 199,
				items: 
				[
					{
						xtype: 'datefield',
						id: 'TglOperasi_viJdwlOperasi',						
						format: 'd/M/Y',
						width: 120,
						listeners:
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									datarefresh_viJdwlOperasi();								
								} 						
							}
						}
					},
					{
						xtype: 'displayfield',				
						width: 50,								
						value: 'Kamar:',
						fieldLabel: 'Label',
					},
					viComboKamar_viJdwlOperasi()
				]
			}

		]
	}
	return items;
}

function getItemPanelInputJadwal_viJdwlOperasi(lebar) 
{
    var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		border:true,
		items:
		[			
			{
				xtype: 'compositefield',
				fieldLabel: 'No. Medrec',
				anchor: '100%',
				//labelSeparator: '',
				width: 199,
				items: 
				[
					{
						xtype: 'textfield',
						flex: 1,
						fieldLabel: 'Label',
						width: 100,
						name: 'txtNoMedrec_viJdwlOperasi',
						id: 'txtNoMedrec_viJdwlOperasi',
						style:{'text-align':'right'},
						emptyText: 'No. Medrec',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},
					//-------------- ## --------------
					{
						xtype: 'displayfield',
						flex: 1,
						width: 35,
						name: '',
						value: 'Nama:',
						fieldLabel: 'Label'
					},
					{
						xtype: 'textfield',
						flex: 1,
						fieldLabel: 'Label',														
						width : 465,	
						name: 'txtNama_viJdwlOperasi',
						id: 'txtNama_viJdwlOperasi',
						emptyText: 'Nama',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					}
					//-------------- ## --------------
				]
			},	
			//-------------- ## --------------				
			{
				xtype: 'compositefield',
				fieldLabel: 'Alamat',
				name: 'compAlamat_viJdwlOperasi',
				id: 'compAlamat_viJdwlOperasi',
				items: 
				[
					{
						xtype: 'textfield',					
						fieldLabel: '',								
						anchor: '100%',
						width: 610,
						name: 'txtAlamat_viJdwlOperasi',
						id: 'txtAlamat_viJdwlOperasi',
						emptyText: 'Alamat',
					}
				]
			},
			//-------------- ## --------------	
			{
				xtype: 'compositefield',
				fieldLabel: 'Tgl. Jadwal',
				name: 'compTgljadwal_viJdwlOperasi',
				id: 'compTgljadwal_viJdwlOperasi',
				items: 
				[
					{
						xtype: 'datefield',
						fieldLabel: 'Tanggal',
						id: 'DateTgljadwal_viJdwlOperasi',
						name: 'DateTgljadwal_viJdwlOperasi',
						width: 130,
						format: 'd/M/Y',
					},
					//-------------- ## --------------
					{
						xtype: 'displayfield',				
						width: 55,								
						value: 'Dokter:',
						fieldLabel: 'Label',
						id: 'lblDokter_viJdwlOperasi',
						name: 'lblDokter_viJdwlOperasi'
					},
					viComboDokter_viJdwlOperasi()
					//-------------- ## --------------
				]
			},
			//-------------- ## --------------	
			{
				xtype: 'compositefield',
				fieldLabel: 'Jam Operasi',
				name: 'compJamOperasi_viJdwlOperasi',
				id: 'compJamOperasi_viJdwlOperasi',
				items: 
				[
					{
						xtype: 'datefield',
						fieldLabel: 'Jam Operasi',
						id: 'DateJamOperasi_viJdwlOperasi',
						name: 'DateJamOperasi_viJdwlOperasi',
						width: 130,
						format: 'd/M/Y',
					},
					{
						xtype: 'displayfield',				
						width: 55,								
						value: 'Durasi:',
						fieldLabel: 'Label',
						id: 'lblDurasi_viJdwlOperasi',
						name: 'lblDurasi_viJdwlOperasi'
					},
					{
						xtype: 'textfield',					
						fieldLabel: '',								
						width: 80,
						name: 'txtDurasi_viJdwlOperasi',
						id: 'txtDurasi_viJdwlOperasi',
						emptyText: '',
					},
					{
						xtype: 'displayfield',				
						width: 80,								
						value: '(Dalam Menit)',
						fieldLabel: 'Label',
						id: 'lblKetDurasi_viJdwlOperasi',
						name: 'lblKetDurasi_viJdwlOperasi'
					},
					{
						xtype:'button',
						text:'Set Perawat',
						width:120,
						hideLabel:true,
						id: 'btnsetperawat_viJdwlOperasi',
						handler:function()
						{
							
						}   
					},
					{
						xtype:'button',
						text:'Set Asisten',
						width:120,
						hideLabel:true,
						id: 'btnsetasisten_viJdwlOperasi',
						handler:function()
						{
							
						}   
					}					
				]
			},
			{
				xtype: 'compositefield',
				fieldLabel: 'Spesialis',
				name: 'compspesialis_viJdwlOperasi',
				id: 'compspesialis_viJdwlOperasi',
				items: 
				[
					viComboSpescialis_viJdwlOperasi(),
					{
						xtype: 'displayfield',				
						width: 80,								
						value: 'Sub. Spesialis',
						fieldLabel: 'Label',
						id: 'lblsubSpesialis_viJdwlOperasi',
						name: 'lblsubSpesialis_viJdwlOperasi'
					},	
					viComboSubSpescialis_viJdwlOperasi()
				]
			},
			{
				xtype: 'compositefield',
				fieldLabel: 'Jenis Operasi',
				name: 'compjnsoperasi_viJdwlOperasi',
				id: 'compjnsoperasi_viJdwlOperasi',
				items: 
				[
					viComboJenis_viJdwlOperasi(),
					{
						xtype: 'displayfield',				
						width: 80,								
						value: 'Jenis Tindakan',
						fieldLabel: 'Label',
						id: 'lbljenistindakan_viJdwlOperasi',
						name: 'lbljenistindakan_viJdwlOperasi'
					},
					viComboJenisTindakan_viJdwlOperasi()
				]
			},
			{
				xtype: 'textfield',
				fieldLabel: 'Keterangan',
				id: 'txtKeterangan_viJdwlOperasi',
				name: 'txtKeterangan_viJdwlOperasi',
				width: 610				
			}	
			//-------------- ## --------------
		]
	};
    return items;
};
// End Function getItemPanelInputJadwal_viJdwlOperasi # --------------

function viComboJenisTindakan_viJdwlOperasi()
{
  var cbo_viComboJenisTindakan_viJdwlOperasi = new Ext.form.ComboBox
    (
    
        {
            id:"cbo_viComboJenisTindakan_viJdwlOperasi",
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'Pilih Jenis Tindakan..',
            fieldLabel: "Jenis Tindakan",           
            width:340,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields: 
                    [
                        'IdJenisTindakan',
                        'displayTextJenisTindakan'
                    ],
                    data: [[1, ""],[2, ""]]
                }
            ),
            valueField: 'IdJenisTindakan',
            displayField: 'displayTextJenisTindakan',
            listeners:  
            {
            }
        }
    );
    return cbo_viComboJenisTindakan_viJdwlOperasi;
};

function viComboJenis_viJdwlOperasi()
{
  var cbo_viComboJenisOperasi_viJdwlOperasi = new Ext.form.ComboBox
    (
    
        {
            id:"cbo_viComboJenisOperasi_viJdwlOperasi",
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'Pilih Jenis Operasi..',
            fieldLabel: "Jenis Operasi",           
            width:180,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields: 
                    [
                        'IdJenisOperasi',
                        'displayTextJenisOperasi'
                    ],
                    data: [[1, "Kecil"],[2, "Sedang"]]
                }
            ),
            valueField: 'IdJenisOperasi',
            displayField: 'displayTextJenisOperasi',
            listeners:  
            {
            }
        }
    );
    return cbo_viComboJenisOperasi_viJdwlOperasi;
};

function viComboSubSpescialis_viJdwlOperasi()
{
  var cbo_viComboSubSpescialis_viJdwlOperasi = new Ext.form.ComboBox
    (
    
        {
            id:"cbo_viComboSubSpescialis_viJdwlOperasi",
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'Pilih Sub Specialis..',
            fieldLabel: "Sub Spesialis",
            value:1,        
            width:340,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields: 
                    [
                        'IdSpesialisSub',
                        'displayTextSpesialisSub'
                    ],
                    data: [[1, "Penyakit Dalam"],[2, "Jantung"]]
                }
            ),
            valueField: 'IdSpesialisSub',
            displayField: 'displayTextSpesialisSub',
            listeners:  
            {
            }
        }
    );
    return cbo_viComboSubSpescialis_viJdwlOperasi;
};

function viComboSpescialis_viJdwlOperasi()
{
  var cbo_viComboSpescialis_viJdwlOperasi = new Ext.form.ComboBox
    (
    
        {
            id:"cbo_viComboSpescialis_viJdwlOperasi",
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'Pilih Specialis..',
            fieldLabel: "Spesialis",
            value:1,        
            width:180,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields: 
                    [
                        'IdSpesialis',
                        'displayTextSpesialis'
                    ],
                    data: [[1, "Penyakit Dalam"],[2, "Jantung"]]
                }
            ),
            valueField: 'IdSpesialis',
            displayField: 'displayTextSpesialis',
            listeners:  
            {
            }
        }
    );
    return cbo_viComboSpescialis_viJdwlOperasi;
};

function viComboDokter_viJdwlOperasi()
{
  var cbo_viComboDokter_viJdwlOperasi = new Ext.form.ComboBox
    (
    
        {
            id:"cbo_viComboDokter_viJdwlOperasi",
            typeAhead: true,
            triggerAction: 'all',
            lazyRender:true,
            mode: 'local',
            emptyText:'Pilih Dokter..',
            fieldLabel: "Dokter",
            value:1,        
            width:415,
            store: new Ext.data.ArrayStore
            (
                {
                    id: 0,
                    fields: 
                    [
                        'IdDokter',
                        'displayTextDokter'
                    ],
                    data: [[1, "SIGIT WIRIYATMO"],[2, "BUDI HERMAWAN"],[3, "TONI KUSNANDAR"]]
                }
            ),
            valueField: 'IdDokter',
            displayField: 'displayTextDokter',
            listeners:  
            {
            }
        }
    );
    return cbo_viComboDokter_viJdwlOperasi;
};

/**
*	Function : getItemGridTransaksi_viJdwlOperasi
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemGridTransaksi_viJdwlOperasi(lebar) 
{
    var items =
	{
		title: '', 
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		border:true,
		width: lebar-80,
		height:200, 
	    items:
		[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:
				[
					gridDataViewEdit_viJdwlOperasi(),
					{ 
						xtype: 'tbspacer',
						width: 15,
						id: 'spcorder_viJdwlOperasi',
						height: 5
					},
					{
						xtype:'button',
						text:'Daftar Order',
						width:120,					
						hideLabel:true,
						id: 'btnorder_viJdwlOperasi',
						handler:function()
						{
						}   
					}
				]	
			}
			//-------------- ## --------------
		]
	};
    return items;
};
// End Function getItemGridTransaksi_viJdwlOperasi # --------------

/**
*	Function : gridDataViewEdit_viJdwlOperasi
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function gridDataViewEdit_viJdwlOperasi()
{
    
    var FieldGrdKasir_viJdwlOperasi = 
	[
	];
	
    dsDataGrdJab_viJdwlOperasi= new WebApp.DataStore
	({
        fields: FieldGrdKasir_viJdwlOperasi
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJab_viJdwlOperasi,
        height: 150,
		selModel: new Ext.grid.RowSelectionModel
        (
	        {
	            singleSelect: true,
	            listeners:
	            {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
	        }
        ),
        columns: 
		[
			{
				dataIndex: '',
				header: 'Status Operasi',
				sortable: true,
				width: 120
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'No. MedRec',
				sortable: true,
				width: 100
			},
			//-------------- ## --------------			
			{			
				dataIndex: '',
				header: 'Nama Pasien',
				sortable: true,
				width: 150
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Alamat',
				sortable: true,
				width: 150,
				renderer: function(v, params, record) 
				{
					
				}			
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Dokter',
				sortable: true,
				width: 120,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Tindakan',
				sortable: true,
				width: 150,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Durasi',
				sortable: true,
				width: 70,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Selesai',
				sortable: true,
				width: 80,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Keterangan',
				sortable: true,
				width: 200,
				renderer: function(v, params, record) 
				{
					
				}	
			}
        ]
    }    
    return items;
}
// End Function gridDataViewEdit_viJdwlOperasi # --------------

// ## MENU LOOKUP ## ------------------------------------------------------------------

// ## MENU LOOKUP - LOOKUP PRODUK/ TINDAKAN ## ------------------------------------------------------------------

/**
*   Function : FormSetLookupProduk_viJdwlOperasi
*   
*   Sebuah fungsi untuk menampilkan windows popup List Produk
*/

function FormSetLookupProduk_viJdwlOperasi(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryLookupProduk_viJdwlOperasi = new Ext.Window
    (
        {
            id: 'FormGrdLookupProduk_viJdwlOperasi',
            title: 'List Produk',
            closeAction: 'destroy',
            closable:true,
            width: 760,
            height: 473,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
			//padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryProdukGridDataView_viJdwlOperasi(subtotal,NO_KUNJUNGAN),
                //-------------- ## --------------
            ],
            tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						id: 'btnAddTreeProduk_viJdwlOperasi',
						handler: function()
						{
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDeleteTreeProduk_viJdwlOperasi',
						handler: function()
						{
						}
					},
					//-------------- ## --------------
				]
			}
        }
    );
    vWinFormEntryLookupProduk_viJdwlOperasi.show();
    mWindowGridLookupLookupProduk  = vWinFormEntryLookupProduk_viJdwlOperasi; 
}
// End Function FormSetLookupProduk_viJdwlOperasi # --------------

/**
*   Function : getFormItemEntryProdukGridDataView_viJdwlOperasi
*   
*   Sebuah fungsi untuk menampilkan isian form produk
*/
function getFormItemEntryProdukGridDataView_viJdwlOperasi(subtotal,NO_KUNJUNGAN)
{
	var lebar = 515;
	var itemsListProdukGridDataView_viJdwlOperasi = 
    {
        xtype: 'panel',
        title: '',
        name: 'PanelListProdukGridDataView_viJdwlOperasi',
        id: 'PanelListProdukGridDataView_viJdwlOperasi',        
        bodyStyle: 'padding: 5px;',
        layout: 'hbox',
        height: 1170,
        border:false,
        items: 
        [           
            itemsTreeListProdukGridDataView_viJdwlOperasi(),
            //-------------- ## -------------- 
            { 
                xtype: 'tbspacer',
                width: 5,
            },
            //-------------- ## -------------- 
            getItemTreeGridTransaksiGridDataView_viJdwlOperasi(lebar),
            //-------------- ## -------------- 
        ]
    }
    return itemsListProdukGridDataView_viJdwlOperasi;
}
// End Function getFormItemEntryProdukGridDataView_viJdwlOperasi # --------------

/**
*   Function : itemsTreeListProdukGridDataView_viJdwlOperasi
*   
*   Sebuah fungsi untuk menampilkan tree prosuk
*/
function itemsTreeListProdukGridDataView_viJdwlOperasi()
{
	var children = 
	[
		{
		     text:'First Level Child 1',
		     children:
		     [
		     	{
		        	text:'Second Level Child 1',
		        	leaf:true
		        },
		        //-------------- ## -------------- 
		        {
		        	text:'Second Level Child 2',
		        	leaf:true
		        },
		        //-------------- ## -------------- 
		    ]
		},
		{
		     text:'First Level Child 2',
		     children:
		     [
		     	{
		        	text:'Second Level Child 1',
		        	leaf:true
		        },
		        //-------------- ## -------------- 
		        {
		        	text:'Second Level Child 2',
		        	leaf:true
		        },
		        //-------------- ## -------------- 
		    ]
		},
	];

    var tree = new Ext.tree.TreePanel
    (
    	{
        	loader:new Ext.tree.TreeLoader(),
        	width:200,
        	height:340,
        	renderTo:Ext.getBody(),
        	root:new Ext.tree.AsyncTreeNode
        	(
        		{
             		expanded:true,
             		leaf:false,
             		text:'Tree Root',
             		children:children
             	}
            )
    	}
    );

    var pnlTreeFormDataWindowPopup_viJdwlOperasi = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                {
		            xtype: 'radiogroup',
		            //fieldLabel: 'Single Column',
		            itemCls: 'x-check-group-alt',
		            columns: 1,
		            items: 
		            [
		                {
		                	boxLabel: 'Semua Unit', 
		                	name: 'ckboxTreeSemuaUnit_viJdwlOperasi',
		                	id: 'ckboxTreeSemuaUnit_viJdwlOperasi',  
		                	inputValue: 1,
		                },
		                //-------------- ## -------------- 
		                {
		                	boxLabel: 'Unit Penyakit Dalam', 
		                	name: 'ckboxTreePerUnit_viJdwlOperasi',
		                	id: 'ckboxTreePerUnit_viJdwlOperasi',  
		                	inputValue: 2, 
		                	checked: true,
		                },
		                //-------------- ## -------------- 
		            ]
		        },
				//-------------- ## -------------- 
                tree,
                //-------------- ## -------------- 
            ],
            //-------------- #End items# --------------
        }
    )

    return pnlTreeFormDataWindowPopup_viJdwlOperasi;
}
// End Function itemsTreeListProdukGridDataView_viJdwlOperasi # --------------

/**
*   Function : getItemTreeGridTransaksiGridDataView_viJdwlOperasi
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/

function getItemTreeGridTransaksiGridDataView_viJdwlOperasi(lebar) 
{
    var items =
    {
        //title: 'Detail Transaksi', 
        layout: 'form',
        anchor: '100%',     
        labelAlign: 'Left',
        bodyStyle: 'padding:2px 2px 2px 2px',
        border:true,
        width: lebar,
        height:402, 
        items:
        [
            {
                labelWidth:105,
                layout: 'form',
                labelAlign: 'Left',
                border: false,
                items:
                [
                    gridTreeDataViewEditGridDataView_viJdwlOperasi()
                ]   
            }
            //-------------- ## --------------
        ]
    };
    return items;
};
// End Function getItemTreeGridTransaksiGridDataView_viJdwlOperasi # --------------

/**
*   Function : gridTreeDataViewEditGridDataView_viJdwlOperasi
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/

function gridTreeDataViewEditGridDataView_viJdwlOperasi()
{
    
    var FieldTreeGrdKasirGridDataView_viJdwlOperasi = 
    [
    ];
    
    dsDataTreeGrdJabGridDataView_viJdwlOperasi= new WebApp.DataStore
    ({
        fields: FieldTreeGrdKasirGridDataView_viJdwlOperasi
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataTreeGrdJabGridDataView_viJdwlOperasi,
        height: 395,
        selModel: new Ext.grid.RowSelectionModel
        (
            {
                singleSelect: true,
                listeners:
                {
                    rowselect: function(sm, row, rec)
                    {
                    }
                }
            }
        ),
        columns: 
        [
            {
                dataIndex: '',
                header: 'Cek',
                sortable: true,
                width: 50
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Deskripsi Produk',
                sortable: true,
                width: 330,
                renderer: function(v, params, record) 
                {
                    
                }           
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Tarif',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }   
            },
            //-------------- ## --------------
        ]
    }    
    return items;
}
// End Function gridTreeDataViewEditGridDataView_viJdwlOperasi # --------------

// ## END MENU LOOKUP - LOOKUP PRODUK/ TINDAKAN ## ------------------------------------------------------------------

// ## MENU LOOKUP - LOOKUP KONSULTASI ## ------------------------------------------------------------------

/**
*   Function : FormSetLookupKonsultasi_viJdwlOperasi
*   
*   Sebuah fungsi untuk menampilkan windows popup Konsultasi
*/

function FormSetLookupKonsultasi_viJdwlOperasi(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryKonsultasi_viJdwlOperasi = new Ext.Window
    (
        {
            id: 'FormGrdLookupKonsultasi_viJdwlOperasi',
            title: 'Konsultasi',
            closeAction: 'destroy',
            closable:true,
            width: 390,
            height: 125,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
            //padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryLookupKonsultasi_viJdwlOperasi(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryKonsultasi_viJdwlOperasi.show();
    mWindowGridLookupKonsultasi  = vWinFormEntryKonsultasi_viJdwlOperasi; 
};

// End Function FormSetLookupKonsultasi_viJdwlOperasi # --------------

/**
*   Function : getFormItemEntryLookupKonsultasi_viJdwlOperasi
*   
*   Sebuah fungsi untuk menampilkan isian form konsultasi
*/
function getFormItemEntryLookupKonsultasi_viJdwlOperasi(subtotal,NO_KUNJUNGAN)
{
    var lebar = 500;
    var pnlFormDataKonsultasiWindowPopup_viJdwlOperasi = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                getItemPanelInputKonsultasiDataView_viJdwlOperasi(lebar),
                //-------------- ## --------------
            ],
            //-------------- #End items# --------------
        }
    )
    return pnlFormDataKonsultasiWindowPopup_viJdwlOperasi;
}
// End Function getFormItemEntryLookupKonsultasi_viJdwlOperasi # --------------

/**
*   Function : getItemPanelInputKonsultasiDataView_viJdwlOperasi
*   
*   Sebuah fungsi untuk menampilkan isian form konsultasi
*/

function getItemPanelInputKonsultasiDataView_viJdwlOperasi(lebar) 
{
    var items =
    {
        title: '',
        layout: 'Form',
        anchor: '100%',
        width: lebar,
        labelAlign: 'Left',
        bodyStyle: 'padding:1px 1px 1px 1px',
        border:false,
        items:
        [           
            {
                xtype: 'compositefield',
                fieldLabel: 'Asal Unit',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_viJdwlOperasi(250,'CmboAsalUnit_viJdwlOperasi'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
            {
                xtype: 'compositefield',
                fieldLabel: 'Konsultasi ke',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_viJdwlOperasi(250,'CmboKonsultasike_viJdwlOperasi'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
            {
                xtype: 'compositefield',
                fieldLabel: 'Dokter',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_viJdwlOperasi(250,'CmboDokter_viJdwlOperasi'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
        ]
    };
    return items;
}
// End Function getItemPanelInputKonsultasiDataView_viJdwlOperasi # --------------

// ## END MENU LOOKUP - LOOKUP KONSULTASI ## ------------------------------------------------------------------

// ## MENU LOOKUP - LOOKUP GANTI DOKTER ## ------------------------------------------------------------------

/**
*   Function : FormSetLookupGantiDokter_viJdwlOperasi
*   
*   Sebuah fungsi untuk menampilkan windows popup Ganti Dokter
*/

function FormSetLookupGantiDokter_viJdwlOperasi(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryGantiDokter_viJdwlOperasi = new Ext.Window
    (
        {
            id: 'FormGrdLookupGantiDokter_viJdwlOperasi',
            title: 'Ganti Dokter',
            closeAction: 'destroy',
            closable:true,
            width: 390,
            height: 125,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
            //padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryLookupGantiDokter_viJdwlOperasi(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryGantiDokter_viJdwlOperasi.show();
    mWindowGridLookupGantiDokter  = vWinFormEntryGantiDokter_viJdwlOperasi; 
};

// End Function FormSetLookupGantiDokter_viJdwlOperasi # --------------

/**
*   Function : getFormItemEntryLookupGantiDokter_viJdwlOperasi
*   
*   Sebuah fungsi untuk menampilkan isian form Ganti Dokter
*/
function getFormItemEntryLookupGantiDokter_viJdwlOperasi(subtotal,NO_KUNJUNGAN)
{
    var lebar = 500;
    var pnlFormDataGantiDokterWindowPopup_viJdwlOperasi = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                getItemPanelInputGantiDokterDataView_viJdwlOperasi(lebar),
                //-------------- ## --------------
            ],
            //-------------- #End items# --------------
        }
    )
    return pnlFormDataGantiDokterWindowPopup_viJdwlOperasi;
}
// End Function getFormItemEntryLookupGantiDokter_viJdwlOperasi # --------------

/**
*   Function : getItemPanelInputGantiDokterDataView_viJdwlOperasi
*   
*   Sebuah fungsi untuk menampilkan isian form Ganti Dokter
*/

function getItemPanelInputGantiDokterDataView_viJdwlOperasi(lebar) 
{
    var items =
    {
        title: '',
        layout: 'Form',
        anchor: '100%',
        width: lebar,
        labelAlign: 'Left',
        bodyStyle: 'padding:1px 1px 1px 1px',
        border:false,
        items:
        [           
            {
                xtype: 'compositefield',
                fieldLabel: 'Tanggal',
                anchor: '100%',
                width: 250,
                items: 
                [
                    {
                        xtype: 'textfield',
                        id: 'TxtTglGantiDokter_viJdwlOperasi',
                        emptyText: '01',
                        width: 30,
                    },
                    //-------------- ## --------------  
                    {
                        xtype: 'textfield',
                        id: 'TxtBlnGantiDokter_viJdwlOperasi',
                        emptyText: '01',
                        width: 30,
                    },
                    //-------------- ## --------------  
                    {
                        xtype: 'textfield',
                        id: 'TxtThnGantiDokter_viJdwlOperasi',
                        emptyText: '2014',
                        width: 50,
                    },
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
            {
                xtype: 'compositefield',
                fieldLabel: 'Dokter Asal',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_viJdwlOperasi(250,'CmboDokterAsal_viJdwlOperasi'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
            {
                xtype: 'compositefield',
                fieldLabel: 'Dokter Ganti',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_viJdwlOperasi(250,'CmboDokterGanti_viJdwlOperasi'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
        ]
    };
    return items;
}
// End Function getItemPanelInputGantiDokterDataView_viJdwlOperasi # --------------

// ## END MENU LOOKUP - LOOKUP GANTI DOKTER ## ------------------------------------------------------------------

// ## END MENU LOOKUP ## ------------------------------------------------------------------

// ## MENU PEMBAYARAN ## ------------------------------------------------------------------

// ## MENU PEMBAYARAN - PEMBAYARAN ## ------------------------------------------------------------------

/**
*   Function : FormSetPembayaran_viJdwlOperasi
*   
*   Sebuah fungsi untuk menampilkan windows popup Pembayaran
*/

function FormSetPembayaran_viJdwlOperasi(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryPembayaran_viJdwlOperasi = new Ext.Window
    (
        {
            id: 'FormGrdPembayaran_viJdwlOperasi',
            title: 'Pembayaran',
            closeAction: 'destroy',
            closable:true,
            width: 760,
            height: 473,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
			//padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryGridDataView_viJdwlOperasi(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryPembayaran_viJdwlOperasi.show();
    mWindowGridLookupPembayaran  = vWinFormEntryPembayaran_viJdwlOperasi; 
};

// End Function FormSetPembayaran_viJdwlOperasi # --------------

/**
*   Function : getFormItemEntryGridDataView_viJdwlOperasi
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/
function getFormItemEntryGridDataView_viJdwlOperasi(subtotal,NO_KUNJUNGAN)
{
    var lebar = 760;
    var pnlFormDataWindowPopup_viJdwlOperasi = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
			items:
			[
				getItemPanelInputBiodataGridDataView_viJdwlOperasi(lebar),
				//-------------- ## -------------- 
				getItemGridTransaksiGridDataView_viJdwlOperasi(lebar),
				//-------------- ## --------------
				{
					xtype: 'compositefield',
					fieldLabel: ' ',
					anchor: '100%',
					//labelSeparator: '',
					width: 199,
					style:{'margin-top':'7px'},
					items: 
					[
						{
		                    xtype:'button',
		                    text:'Ok',
		                    width:70,
		                    //style:{'margin-left':'190px','margin-top':'7px'},
		                    hideLabel:true,
		                    id: 'btnOkGridTransaksiPembayaranGridDataView_viJdwlOperasi',
		                    handler:function()
		                    {
		                    }   
		                },
		                //-------------- ## --------------
		                {
		                    xtype:'button',
		                    text:'Cancel',
		                    width:70,
		                    //style:{'margin-left':'190px','margin-top':'7px'},
		                    hideLabel:true,
		                    id: 'btnCancelGridTransaksiPembayaranGridDataView_viJdwlOperasi',
		                    handler:function()
		                    {
		                    }   
		                },
		                //-------------- ## --------------
		                {
                            xtype: 'textfield',
                            id: 'txtBiayaGridTransaksiPembayaranGridDataView_viJdwlOperasi',
                            name: 'txtBiayaGridTransaksiPembayaranGridDataView_viJdwlOperasi',
							style:{'text-align':'right','margin-left':'150px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
		                //-------------- ## --------------
		                {
                            xtype: 'textfield',
                            id: 'txtPiutangGridTransaksiPembayaranGridDataView_viJdwlOperasi',
                            name: 'txtPiutangGridTransaksiPembayaranGridDataView_viJdwlOperasi',
							style:{'text-align':'right','margin-left':'146px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
		                //-------------- ## --------------
		                {
                            xtype: 'textfield',
                            id: 'txtTunaiGridTransaksiPembayaranGridDataView_viJdwlOperasi',
                            name: 'txtTunaiGridTransaksiPembayaranGridDataView_viJdwlOperasi',
							style:{'text-align':'right','margin-left':'142px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
		                //-------------- ## --------------
		                {
                            xtype: 'textfield',
                            id: 'txtDiscGridTransaksiPembayaranGridDataView_viJdwlOperasi',
                            name: 'txtDiscGridTransaksiPembayaranGridDataView_viJdwlOperasi',
							style:{'text-align':'right','margin-left':'138px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
		                //-------------- ## --------------
					]
				},	
			],
			//-------------- #End items# --------------
        }
    )
    return pnlFormDataWindowPopup_viJdwlOperasi;
}
// End Function getFormItemEntryGridDataView_viJdwlOperasi # --------------

/**
*   Function : getItemPanelInputBiodataGridDataView_viJdwlOperasi
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/

function getItemPanelInputBiodataGridDataView_viJdwlOperasi(lebar) 
{
    var items =
    {
        title: 'Infromasi Pasien',
        layout: 'Form',
        anchor: '100%',
        width: lebar- 80,
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        items:
        [           
            {
				xtype: 'textfield',
				fieldLabel: 'No. Transaksi',
				id: 'txtNoTransaksiPembayaranGridDataView_viJdwlOperasi',
				name: 'txtNoTransaksiPembayaranGridDataView_viJdwlOperasi',
				emptyText: 'No. Transaksi',
				readOnly: true,
				flex: 1,
				width: 195,				
			},
			//-------------- ## --------------
			{
				xtype: 'textfield',
				fieldLabel: 'Nama Pasien',
				id: 'txtNamaPasienPembayaranGridDataView_viJdwlOperasi',
				name: 'txtNamaPasienPembayaranGridDataView_viJdwlOperasi',
				emptyText: 'Nama Pasien',
				flex: 1,
				anchor: '100%',					
			},
			//-------------- ## --------------
			{
				xtype: 'compositefield',
				fieldLabel: 'Pembayaran',
				anchor: '100%',
				width: 250,
				items: 
				[
					viComboJenisTransaksi_viJdwlOperasi(195,'CmboPembayaranGridDataView_viJdwlOperasi'),
					//-------------- ## --------------	
				]
			},
			//-------------- ## --------------
			{
				xtype: 'compositefield',
				fieldLabel: ' ',
				anchor: '100%',
				labelSeparator: '',
				width: 250,
				items: 
				[
					viComboJenisTransaksi_viJdwlOperasi(195,'CmboPembayaranDuaGridDataView_viJdwlOperasi'),
					//-------------- ## --------------	
				]
			},
			//-------------- ## --------------
        ]
    };
    return items;
}
// End Function getItemPanelInputBiodataGridDataView_viJdwlOperasi # --------------

/**
*   Function : getItemGridTransaksiGridDataView_viJdwlOperasi
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/

function getItemGridTransaksiGridDataView_viJdwlOperasi(lebar) 
{
    var items =
    {
        //title: 'Detail Transaksi', 
        layout: 'form',
        anchor: '100%',     
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        width: lebar-80,
        height:245, 
        items:
        [
            {
                labelWidth:105,
                layout: 'form',
                labelAlign: 'Left',
                border: false,
                items:
                [
                    gridDataViewEditGridDataView_viJdwlOperasi()
                ]   
            }
            //-------------- ## --------------
        ]
    };
    return items;
};
// End Function getItemGridTransaksiGridDataView_viJdwlOperasi # --------------

/**
*   Function : gridDataViewEditGridDataView_viJdwlOperasi
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/

function gridDataViewEditGridDataView_viJdwlOperasi()
{
    
    var FieldGrdKasirGridDataView_viJdwlOperasi = 
    [
    ];
    
    dsDataGrdJabGridDataView_viJdwlOperasi= new WebApp.DataStore
    ({
        fields: FieldGrdKasirGridDataView_viJdwlOperasi
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJabGridDataView_viJdwlOperasi,
        height: 220,
        selModel: new Ext.grid.RowSelectionModel
        (
            {
                singleSelect: true,
                listeners:
                {
                    rowselect: function(sm, row, rec)
                    {
                    }
                }
            }
        ),
        columns: 
        [
            {
                dataIndex: '',
                header: 'Kode',
                sortable: true,
                width: 50
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Deskripsi',
                sortable: true,
                width: 200
            },
            //-------------- ## --------------          
            {           
                dataIndex: '',
                header: 'Qty',
                sortable: true,
                width: 40
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Biaya',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }           
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Piutang',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }   
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Tunai',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }   
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Disc',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }   
            },
            //-------------- ## --------------
        ]
    }    
    return items;
}
// End Function gridDataViewEditGridDataView_viJdwlOperasi # --------------

// ## END MENU PEMBAYARAN - PEMBAYARAN ## ------------------------------------------------------------------

// ## MENU PEMBAYARAN - TRANSFER KE RAWAT INAP ## ------------------------------------------------------------------

/**
*   Function : FormSetTransferPembayaran_viJdwlOperasi
*   
*   Sebuah fungsi untuk menampilkan windows popup Transfer ke Rawat Inap
*/

function FormSetTransferPembayaran_viJdwlOperasi(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryTransferPembayaran_viJdwlOperasi = new Ext.Window
    (
        {
            id: 'FormGrdTrnsferPembayaran_viJdwlOperasi',
            title: 'Pemindahan Tagihan Ke Unit Rawat Inap',
            closeAction: 'destroy',
            closable:true,
            width: 380,
            height: 450,
            border: false,
            plain: true,
            resizable:false,
            layout: 'form',
            iconCls: 'Edit_Tr',
            constrainHeader : true,
            //padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormTransferPembayaranItemEntryGridDataView_viJdwlOperasi(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryTransferPembayaran_viJdwlOperasi.show();
    mWindowGridLookupTransferPembayaran  = vWinFormEntryTransferPembayaran_viJdwlOperasi; 
};

// End Function FormSetTransferPembayaran_viJdwlOperasi # --------------

/**
*   Function : getFormTransferPembayaranItemEntryGridDataView_viJdwlOperasi
*   
*   Sebuah fungsi untuk menampilkan isian form transfer ke rawat inap
*/
function getFormTransferPembayaranItemEntryGridDataView_viJdwlOperasi(subtotal,NO_KUNJUNGAN)
{
    var pnlFormTransferPembayaranDataWindowPopup_viJdwlOperasi = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            items:
            //-------------- #items# --------------
            [
                {
                    xtype: 'fieldset',
                    title: 'Tagihan dipindahkan ke',
                    frame: false,
                    width: 350,
                    height: 165,
                    items: 
                    [
                        {
                            xtype: 'textfield',
                            fieldLabel: 'No. Transaksi',
                            id: 'txtNoTransaksiTransaksiTransferPembayaran_viJdwlOperasi',
                            name: 'txtNoTransaksiTransaksiTransferPembayaran_viJdwlOperasi',
                            width: 130,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
							xtype: 'compositefield',
							fieldLabel: 'Tgl. Lahir',
							anchor: '100%',
							width: 130,
							items: 
							[
								{
									xtype: 'datefield',
									fieldLabel: 'Tanggal',
									id: 'DateTransaksiTransferPembayaran_viJdwlOperasi',
									name: 'DateTransaksiTransferPembayaran_viJdwlOperasi',
									width: 130,
									format: 'd/M/Y',
								}
								//-------------- ## --------------				
							]
						},
						//-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'No. Rekam Medis',
                            id: 'txtNoRekamMedisTransaksiTransferPembayaran_viJdwlOperasi',
                            name: 'txtNoRekamMedisTransaksiTransferPembayaran_viJdwlOperasi',
                            width: 130,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Nama Pasien',
                            id: 'txtNamaPasienTransaksiTransferPembayaran_viJdwlOperasi',
                            name: 'txtNamaPasienTransaksiTransferPembayaran_viJdwlOperasi',
                            width: 222,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Unit Perawatan',
                            id: 'txtUnitPerawatanTransaksiTransferPembayaran_viJdwlOperasi',
                            name: 'txtUnitPerawatanTransaksiTransferPembayaran_viJdwlOperasi',
                            width: 222,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                    ]
                },
                //-------------- ## --------------
                {
                    xtype: 'fieldset',
                    title: '',
                    frame: false,
                    width: 350,
                    height: 125,
                    items: 
                    [
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Jumlah biaya',
                            id: 'txtJumlhBiayaTransaksiTransferPembayaran_viJdwlOperasi',
                            name: 'txtJumlhBiayaTransaksiTransferPembayaran_viJdwlOperasi',
                            width: 180,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'PAID',
                            id: 'txtPAIDTransaksiTransferPembayaran_viJdwlOperasi',
                            name: 'txtPAIDTransaksiTransferPembayaran_viJdwlOperasi',
                            width: 180,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Jmlh dipindahkan',
                            id: 'txtJumlahDipindahkanTransaksiTransferPembayaran_viJdwlOperasi',
                            name: 'txtJumlahDipindahkanTransaksiTransferPembayaran_viJdwlOperasi',
                            width: 180,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Saldo Tagihan',
                            id: 'txtSaldoTagihanTransaksiTransferPembayaran_viJdwlOperasi',
                            name: 'txtSaldoTagihanTransaksiTransferPembayaran_viJdwlOperasi',
                            width: 180,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                    ]
                },
                //-------------- ## --------------
                {
                    xtype: 'fieldset',
                    title: 'Alasan Transfer',
                    frame: false,
                    width: 350,
                    height: 62,
                    items: 
                    [
						{
				            xtype: 'combo',
				            fieldLabel: 'Alsn Transfer',
				            labelSeparator: '',
				        },
						//-------------- ## --------------
                    ]
                },
                //-------------- ## --------------
                {
					xtype: 'compositefield',
					fieldLabel: ' ',
					anchor: '100%',
					width: 160,
					items: 
					[
						{
		                    xtype:'button',
		                    text:'Ok',
		                    width:70,
		                    style:{'margin-left':'205px'},
		                    hideLabel:true,
		                    id: 'btnOkTransferPembayaran_viJdwlOperasi',
		                    handler:function()
		                    {
		                        
		                    }   
		                },
						//-------------- ## --------------
						{
		                    xtype:'button',
		                    text:'Cancel',
		                    width:70,
		                    style:{'margin-left':'205px'},
		                    hideLabel:true,
		                    id: 'btnCancelTransferPembayaran_viJdwlOperasi',
		                    handler:function()
		                    {
		                        
		                    }   
		                },
						//-------------- ## --------------
					]
				},
                //-------------- ## --------------
            ]
            //-------------- #items# --------------
        }
    )
    return pnlFormTransferPembayaranDataWindowPopup_viJdwlOperasi;
}
// End Function getFormTransferPembayaranItemEntryGridDataView_viJdwlOperasi # --------------

// ## END MENU PEMBAYARAN - TRANSFER KE RAWAT INAP ## ------------------------------------------------------------------

// ## MENU PEMBAYARAN - EDIT TARIF ## ------------------------------------------------------------------

/**
*   Function : FormSetEditTarif_viJdwlOperasi
*   
*   Sebuah fungsi untuk menampilkan windows popup Edit Tarif
*/

function FormSetEditTarif_viJdwlOperasi(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryEditTarif_viJdwlOperasi = new Ext.Window
    (
        {
            id: 'FormGrdEditTarif_viJdwlOperasi',
            title: 'Edit Tarif',
            closeAction: 'destroy',
            closable:true,
            width: 475,
            height: 395,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
            //padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryEditTarifGridDataView_viJdwlOperasi(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryEditTarif_viJdwlOperasi.show();
    mWindowGridLookupEditTarif  = vWinFormEntryEditTarif_viJdwlOperasi; 
};

// End Function FormSetEditTarif_viJdwlOperasi # --------------

/**
*   Function : getFormItemEntryEditTarifGridDataView_viJdwlOperasi
*   
*   Sebuah fungsi untuk menampilkan isian form edit tarif
*/
function getFormItemEntryEditTarifGridDataView_viJdwlOperasi(subtotal,NO_KUNJUNGAN)
{
    var lebar = 760;
    var pnlFormDataEditTarifWindowPopup_viJdwlOperasi = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                getItemPanelInputProdukEditTarifGridDataView_viJdwlOperasi(lebar),
                //-------------- ## -------------- 
                getItemGridTransaksiEditTarifGridDataView_viJdwlOperasi(lebar),
                //-------------- ## --------------
                {
                    xtype: 'compositefield',
                    fieldLabel: ' ',
                    anchor: '100%',
                    //labelSeparator: '',
                    width: 199,
                    style:{'margin-top':'7px'},
                    items: 
                    [
                        {
                            xtype:'button',
                            text:'Ok',
                            width:70,
                            //style:{'margin-left':'190px','margin-top':'7px'},
                            hideLabel:true,
                            id: 'btnOkGridTransaksiEditTarifGridDataView_viJdwlOperasi',
                            handler:function()
                            {
                            }   
                        },
                        //-------------- ## --------------
                        {
                            xtype:'button',
                            text:'Cancel',
                            width:70,
                            //style:{'margin-left':'190px','margin-top':'7px'},
                            hideLabel:true,
                            id: 'btnCancelGridTransaksiEditTarifGridDataView_viJdwlOperasi',
                            handler:function()
                            {
                            }   
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            id: 'txtTarifLamaGridTransaksiEditTarifGridDataView_viJdwlOperasi',
                            name: 'txtTarifLamaGridTransaksiEditTarifGridDataView_viJdwlOperasi',
                            style:{'text-align':'right','margin-left':'60px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            id: 'txtTarifBaruGridTransaksiEditTarifGridDataView_viJdwlOperasi',
                            name: 'txtTarifBaruGridTransaksiEditTarifGridDataView_viJdwlOperasi',
                            style:{'text-align':'right','margin-left':'56px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                    ]
                },  
            ],
            //-------------- #End items# --------------
        }
    )
    return pnlFormDataEditTarifWindowPopup_viJdwlOperasi;
}
// End Function getFormItemEntryEditTarifGridDataView_viJdwlOperasi # --------------

/**
*   Function : getItemPanelInputProdukEditTarifGridDataView_viJdwlOperasi
*   
*   Sebuah fungsi untuk menampilkan isian form edit tarif
*/

function getItemPanelInputProdukEditTarifGridDataView_viJdwlOperasi(lebar) 
{
    var items =
    {
        title: 'Tarif Produk',
        layout: 'Form',
        anchor: '100%',
        width: lebar- 80,
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        items:
        [           
            {
                xtype: 'textfield',
                fieldLabel: 'Produk',
                id: 'txtProdukEditTarifGridDataView_viJdwlOperasi',
                name: 'txtProdukEditTarifGridDataView_viJdwlOperasi',
                emptyText: 'Produk',
                readOnly: true,
                flex: 1,
                width: 195,             
            },
            //-------------- ## --------------
        ]
    };
    return items;
}
// End Function getItemPanelInputProdukEditTarifGridDataView_viJdwlOperasi # --------------

/**
*   Function : getItemGridTransaksiEditTarifGridDataView_viJdwlOperasi
*   
*   Sebuah fungsi untuk menampilkan isian form edit tarif
*/

function getItemGridTransaksiEditTarifGridDataView_viJdwlOperasi(lebar) 
{
    var items =
    {
        title: 'Perubahan Tarif Menjadi', 
        layout: 'form',
        anchor: '100%',     
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        width: lebar,
        height:245, 
        items:
        [
            {
                labelWidth:105,
                layout: 'form',
                labelAlign: 'Left',
                border: false,
                items:
                [
                    gridDataViewEditGridEditTarifDataView_viJdwlOperasi()
                ]   
            }
            //-------------- ## --------------
        ]
    };
    return items;
};
// End Function getItemGridTransaksiEditTarifGridDataView_viJdwlOperasi # --------------

/**
*   Function : gridDataViewEditGridEditTarifDataView_viJdwlOperasi
*   
*   Sebuah fungsi untuk menampilkan isian form edit tarif
*/

function gridDataViewEditGridEditTarifDataView_viJdwlOperasi()
{
    
    var FieldGrdKasirGridDataView_viJdwlOperasi = 
    [
    ];
    
    dsDataGrdJabGridDataView_viJdwlOperasi= new WebApp.DataStore
    ({
        fields: FieldGrdKasirGridDataView_viJdwlOperasi
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJabGridDataView_viJdwlOperasi,
        height: 200,
        selModel: new Ext.grid.RowSelectionModel
        (
            {
                singleSelect: true,
                listeners:
                {
                    rowselect: function(sm, row, rec)
                    {
                    }
                }
            }
        ),
        columns: 
        [
            {
                dataIndex: '',
                header: 'Komponen',
                sortable: true,
                width: 200
            },
            //-------------- ## --------------          
            {           
                dataIndex: '',
                header: 'Tarif Lama',
                sortable: true,
                width: 100
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Tarif Baru',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }           
            },
            //-------------- ## --------------
        ]
    }    
    return items;
}
// End Function gridDataViewEditGridEditTarifDataView_viJdwlOperasi # --------------

// ## END MENU PEMBAYARAN - EDIT TARIF ## ------------------------------------------------------------------

// ## MENU PEMBAYARAN - POSTING MANUAL ## ------------------------------------------------------------------

/**
*   Function : FormSetPostingManual_viJdwlOperasi
*   
*   Sebuah fungsi untuk menampilkan windows popup Posting Manual
*/

function FormSetPostingManual_viJdwlOperasi(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryPostingManual_viJdwlOperasi = new Ext.Window
    (
        {
            id: 'FormGrdPostingManual_viJdwlOperasi',
            title: 'Posting Manual',
            closeAction: 'destroy',
            closable:true,
            width: 475,
            height: 395,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
            //padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryPostingManualGridDataView_viJdwlOperasi(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryPostingManual_viJdwlOperasi.show();
    mWindowGridLookupPostingManual  = vWinFormEntryPostingManual_viJdwlOperasi; 
};

// End Function FormSetPostingManual_viJdwlOperasi # --------------

/**
*   Function : getFormItemEntryPostingManualGridDataView_viJdwlOperasi
*   
*   Sebuah fungsi untuk menampilkan isian form Posting Manual
*/
function getFormItemEntryPostingManualGridDataView_viJdwlOperasi(subtotal,NO_KUNJUNGAN)
{
    var lebar = 760;
    var pnlFormDataPostingManualWindowPopup_viJdwlOperasi = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                getItemPanelInputProdukPostingManualGridDataView_viJdwlOperasi(lebar),
                //-------------- ## -------------- 
                getItemGridTransaksiPostingManualGridDataView_viJdwlOperasi(lebar),
                //-------------- ## --------------
                {
                    xtype: 'compositefield',
                    fieldLabel: ' ',
                    anchor: '100%',
                    //labelSeparator: '',
                    width: 199,
                    style:{'margin-top':'7px'},
                    items: 
                    [
                        {
                            xtype:'button',
                            text:'Ok',
                            width:70,
                            //style:{'margin-left':'190px','margin-top':'7px'},
                            hideLabel:true,
                            id: 'btnOkGridTransaksiPostingManualGridDataView_viJdwlOperasi',
                            handler:function()
                            {
                            }   
                        },
                        //-------------- ## --------------
                        {
                            xtype:'button',
                            text:'Cancel',
                            width:70,
                            //style:{'margin-left':'190px','margin-top':'7px'},
                            hideLabel:true,
                            id: 'btnCancelGridTransaksiPostingManualGridDataView_viJdwlOperasi',
                            handler:function()
                            {
                            }   
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            id: 'txtTarifLamaGridTransaksiPostingManualGridDataView_viJdwlOperasi',
                            name: 'txtTarifLamaGridTransaksiPostingManualGridDataView_viJdwlOperasi',
                            style:{'text-align':'right','margin-left':'60px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            id: 'txtTarifBaruGridTransaksiPostingManualGridDataView_viJdwlOperasi',
                            name: 'txtTarifBaruGridTransaksiPostingManualGridDataView_viJdwlOperasi',
                            style:{'text-align':'right','margin-left':'56px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                    ]
                },  
            ],
            //-------------- #End items# --------------
        }
    )
    return pnlFormDataPostingManualWindowPopup_viJdwlOperasi;
}
// End Function getFormItemEntryPostingManualGridDataView_viJdwlOperasi # --------------

/**
*   Function : getItemPanelInputProdukPostingManualGridDataView_viJdwlOperasi
*   
*   Sebuah fungsi untuk menampilkan isian form Posting Manual
*/

function getItemPanelInputProdukPostingManualGridDataView_viJdwlOperasi(lebar) 
{
    var items =
    {
        title: 'Tarif Produk',
        layout: 'Form',
        anchor: '100%',
        width: lebar- 80,
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        items:
        [           
            {
                xtype: 'textfield',
                fieldLabel: 'Produk',
                id: 'txtProdukPostingManualGridDataView_viJdwlOperasi',
                name: 'txtProdukPostingManualGridDataView_viJdwlOperasi',
                emptyText: 'Produk',
                readOnly: true,
                flex: 1,
                width: 195,             
            },
            //-------------- ## --------------
        ]
    };
    return items;
}
// End Function getItemPanelInputProdukPostingManualGridDataView_viJdwlOperasi # --------------

/**
*   Function : getItemGridTransaksiPostingManualGridDataView_viJdwlOperasi
*   
*   Sebuah fungsi untuk menampilkan isian form Posting Manual
*/

function getItemGridTransaksiPostingManualGridDataView_viJdwlOperasi(lebar) 
{
    var items =
    {
        title: 'Perubahan Tarif Menjadi', 
        layout: 'form',
        anchor: '100%',     
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        width: lebar,
        height:245, 
        items:
        [
            {
                labelWidth:105,
                layout: 'form',
                labelAlign: 'Left',
                border: false,
                items:
                [
                    gridDataViewEditGridPostingManualDataView_viJdwlOperasi()
                ]   
            }
            //-------------- ## --------------
        ]
    };
    return items;
};
// End Function getItemGridTransaksiPostingManualGridDataView_viJdwlOperasi # --------------

/**
*   Function : gridDataViewEditGridPostingManualDataView_viJdwlOperasi
*   
*   Sebuah fungsi untuk menampilkan isian form Posting Manual
*/

function gridDataViewEditGridPostingManualDataView_viJdwlOperasi()
{
    
    var FieldGrdKasirGridDataView_viJdwlOperasi = 
    [
    ];
    
    dsDataGrdJabGridDataView_viJdwlOperasi= new WebApp.DataStore
    ({
        fields: FieldGrdKasirGridDataView_viJdwlOperasi
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJabGridDataView_viJdwlOperasi,
        height: 200,
        selModel: new Ext.grid.RowSelectionModel
        (
            {
                singleSelect: true,
                listeners:
                {
                    rowselect: function(sm, row, rec)
                    {
                    }
                }
            }
        ),
        columns: 
        [
            {
                dataIndex: '',
                header: 'Komponen',
                sortable: true,
                width: 200
            },
            //-------------- ## --------------          
            {           
                dataIndex: '',
                header: 'Tarif Lama',
                sortable: true,
                width: 100
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Tarif Baru',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }           
            },
            //-------------- ## --------------
        ]
    }    
    return items;
}
// End Function gridDataViewEditGridPostingManualDataView_viJdwlOperasi # --------------

// ## END MENU PEMBAYARAN - POSTING MANUAL ## ------------------------------------------------------------------

// ## END MENU PEMBAYARAN ## ------------------------------------------------------------------

// --------------------------------------- # End Form # ---------------------------------------

// End Project # --------------