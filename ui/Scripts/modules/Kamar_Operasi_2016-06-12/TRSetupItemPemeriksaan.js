var dataSourceItemPemOK_ItemPemeriksaanOK_OK;
var dataSourceItemPem_ItemPemeriksaanOK_OK;
var selectCount_ItemPemeriksaanOK_OK=50;
var NamaForm_ItemPemeriksaanOK_OK="Item Pemeriksaan";
var selectCountStatusPostingOKItemPemeriksaanOK='Semua';
var mod_name_ItemPemeriksaanOK_OK="ItemPemeriksaanOK_OK";
var now_ItemPemeriksaanOK_OK= new Date();
var addNew_ItemPemeriksaanOK_OK;
var rowSelected_ItemPemeriksaanOK_OK;
var setLookUps_ItemPemeriksaanOK_OK;
var tanggal = now_ItemPemeriksaanOK_OK.format("d/M/Y");
var jam = now_ItemPemeriksaanOK_OK.format("H/i/s");
var tmpkriteria;
var DataGridObat;
var DataGridSubObat;
var DataGridSatuanBesar;
var DataGridSatuan;
var DataGridGolongan;
var firstgridSetupOperasi_IPO;
var secondGridStoreSetupOperasi_IPO;
var secondGridSetupOperasi_IPO;
var dsvDoubleGridItemPemOperasi_IPOSetupOperasiOK;
var firstgridSetupAnastesi_IPO;
var secondGridStoreSetupAnastesi_IPO;
var secondGridSetupAnastesi_IPO;
var dsvDoubleGridItemPemAnastesi_IPOSetupAnastesiOK;
var CurrentData_ItemPemeriksaanOK_OK =
{
	data: Object,
	details: Array,
	row: 0
};


var OKItemPemeriksaanOK={};
OKItemPemeriksaanOK.form={};
OKItemPemeriksaanOK.func={};
OKItemPemeriksaanOK.vars={};
OKItemPemeriksaanOK.func.parent=OKItemPemeriksaanOK;
OKItemPemeriksaanOK.form.ArrayStore={};
OKItemPemeriksaanOK.form.ComboBox={};
OKItemPemeriksaanOK.form.DataStore={};
OKItemPemeriksaanOK.form.Record={};
OKItemPemeriksaanOK.form.Form={};
OKItemPemeriksaanOK.form.Grid={};
OKItemPemeriksaanOK.form.Panel={};
OKItemPemeriksaanOK.form.TextField={};
OKItemPemeriksaanOK.form.Button={};

OKItemPemeriksaanOK.form.ArrayStore.ItemPemeriksaanItemPemOK= new Ext.data.ArrayStore({
	id: 0,
	fields:[],
	data: []
});
OKItemPemeriksaanOK.form.ArrayStore.ItemPemeriksaanItemPem= new Ext.data.ArrayStore({
	id: 0,
	fields:[],
	data: []
});


OKItemPemeriksaanOK.form.ArrayStore.gridItemPemeriksaanItemPemOK= new Ext.data.ArrayStore({
	id: 0,
	fields:[],
	data: []
});
OKItemPemeriksaanOK.form.ArrayStore.gridItemPemeriksaanItemPem= new Ext.data.ArrayStore({
	id: 0,
	fields:[],
	data: []
});
OKItemPemeriksaanOK.form.ArrayStore.gridItemPemeriksaanLokasiBaruItemPem= new Ext.data.ArrayStore({
	id: 0,
	fields:[],
	data: []
});
OKItemPemeriksaanOK.form.ArrayStore.gridItemPemeriksaanJumlahPindahItemPem= new Ext.data.ArrayStore({
	id: 0,
	fields:[],
	data: []
});

OKItemPemeriksaanOK.form.ArrayStore.satuanBesar= new Ext.data.ArrayStore({
	id: 0,
	fields:[],
	data: []
});


CurrentPage.page = dataGrid_ItemPemeriksaanOK_OK(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dataGrid_ItemPemeriksaanOK_OK(mod_id_ItemPemeriksaanOK_OK){	
 	var PanelTabOKItemPemeriksaanOK = new Ext.FormPanel({
        labelAlign: 'top',
		layout:'form',
        frame:true,
        title: '',
        bodyStyle:'padding:5px 5px 0',
        //width: 600,
        items: [getPenelItemSetup_ItemPemeriksaanOK_OK()]	
		
	})
	// Kriteria filter pada Grid # --------------
    var FrmData_ItemPemeriksaanOK_OK = new Ext.Panel
    (
		{
			title: NamaForm_ItemPemeriksaanOK_OK,
			iconCls: 'Studi_Lanjut',
			id: mod_id_ItemPemeriksaanOK_OK,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ PanelTabOKItemPemeriksaanOK],
					//GridDataView_ItemPemeriksaanOK_OK],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_ItemPemeriksaanOK_OK,
		            columns: 21,
		            defaults: {
					scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_ItemPemeriksaanOK_OK;
    //-------------- # End form filter # --------------
}
function funcNamaGedungItemPemeriksaanOK_OK()
{
    /*dsvGolTarif = new WebApp.DataStore({fields: stateComboTarifCust});
	dsGolTarif();*/
	var funcNamaGedungItemPemeriksaanOK = new Ext.form.ComboBox
	(
		{
			x: 130,
			y: 10,
			id:'cmbNamaGedungItemPemeriksaanOK_OK',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			/*readOnly:true,
			emptyText:'Set Tarif',
			value: 1,*/
			width: 260,/*
			store: dsvGolTarif,
			valueField: stateComboTarifCust[0],
			displayField: stateComboTarifCust[1],*/
			//value:selectSetPilihankelompokPasien,
			listeners:
			{
				'select': function(a,b,c)
				{
				}
			}
		}
	);
	return funcNamaGedungItemPemeriksaanOK;
}
function funcGedungItemPemeriksaanOK_OK()
{
    /*dsvGolTarif = new WebApp.DataStore({fields: stateComboTarifCust});
	dsGolTarif();*/
	var funcDeptItemPemeriksaanOK = new Ext.form.ComboBox
	(
		{
			x: 130,
			y: 40,
			id:'cmbDeptItemPemeriksaanOK_OK',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			/*readOnly:true,
			emptyText:'Set Tarif',
			value: 1,*/
			width: 320,/*
			store: dsvGolTarif,
			valueField: stateComboTarifCust[0],
			displayField: stateComboTarifCust[1],*/
			//value:selectSetPilihankelompokPasien,
			listeners:
			{
				'select': function(a,b,c)
				{
				}
			}
		}
	);
	return funcDeptItemPemeriksaanOK;
}
function getPenelItemSetup_ItemPemeriksaanOK_OK(lebar)
{
    var items =
	{
		xtype:'tabpanel',
		plain:true,
		activeTab: 0,
		height:600,
		defaults: {
			bodyStyle:'padding:5px',
			autoScroll: false
	    },
	    items:[
				PanelItemPemOK(),
				PanelItemPem(),
		]
	}
    return items;
};

//sub form tab1
function PanelItemPemOK(rowdata){
	var items = 
	{
		title:'Item Pemeriksaan OK',
		layout:'form',
		border: false,
		items:
		[
			{
				xtype:'tabpanel',
			plain:true,
			activeTab: 0,
			height:600,
			defaults: {
				bodyStyle:'padding:5px',
				autoScroll: false
			},
			items:[
					PanelItemPemOKOperasi(),
					PanelItemPemOKAnastesi(),
			],
			listeners: {
            'afterrender': function () {
					dsGridItemKelompokOperasi();
					dsGridItemKelompokAnastesi();
					
				}
			}
			
			}
			
		]
    };
        return items;
}
function PanelItemPemOKOperasi(){
var items = 
	{
		title:'Kelompok Operasi',
		layout:'form',
		border: false,
		items:
		[
			gridDataViewEditDoubleGridDokter_SetupOperasi_Ok()
		]
	}
	return items;
}
function dsGridDoubleGridItemPemOperasi_IPOSetupOperasiOK(){
	dsvDoubleGridItemPemOperasi_IPOSetupOperasiOK.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'vGridItemPemOperasiSetupOK',
                }			
            }
        );   
    return dsvDoubleGridItemPemOperasi_IPOSetupOperasiOK;
}
function dsGridItemKelompokOperasi(namaItem){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/kamar_operasi/functionItemPemeriksaanOK/getItemKelompokOperasi",
			params: {
						kdunit:"ok_default_kamar",
						nama:namaItem
					},
			failure: function(o)
			{
				ShowPesanErrorOKItemPemeriksaanOK('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					for(var i=0; i<cst.listData.length; i++){
						
						var recs    = [],recType = secondGridStoreSetupOperasi_IPO.recordType;
						var o=cst.listData[i];
						recs    = [];
						recs.push(new recType(o));
						
						secondGridStoreSetupOperasi_IPO.add(recs);
					}
					
				}
				else 
				{
					ShowPesanErrorOKItemPemeriksaanOK('Gagal membaca data', 'Error');
				};
			}
		}
		
	)
	
}
function datasaveItemKelompokOperasi(kditem,item){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/kamar_operasi/functionItemPemeriksaanOK/simpanItemKelompokOperasi",
			params: {
						kditem:kditem,
						namaitem:item
					},
			failure: function(o)
			{
				ShowPesanErrorOKItemPemeriksaanOK('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					ShowPesanInfoOKItemPemeriksaanOK("Item berhasil disimpan","Sukses");
					secondGridStoreSetupOperasi_IPO.removeAll();
					dsGridItemKelompokOperasi();
					dsGridDoubleGridItemPemAnastesi_IPOSetupAnastesiOK();
					dsGridDoubleGridItemPemOperasi_IPOSetupOperasiOK();
				}
				else 
				{
					ShowPesanErrorOKItemPemeriksaanOK('Gagal menyimpan data', 'Error');
				};
			}
		}
		
	)
	
}
function datahapusItemKelompokOperasi(kditem){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/kamar_operasi/functionItemPemeriksaanOK/hapusItemKelompokOperasi",
			params: {
						kditem:kditem
					},
			failure: function(o)
			{
				ShowPesanErrorOKItemPemeriksaanOK('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					secondGridStoreSetupOperasi_IPO.removeAll();
					dsGridItemKelompokOperasi();
					dsGridDoubleGridItemPemAnastesi_IPOSetupAnastesiOK();
					dsGridDoubleGridItemPemOperasi_IPOSetupOperasiOK();
				}
				else 
				{
					ShowPesanErrorOKItemPemeriksaanOK('Gagal memproses data', 'Error');
				};
			}
		}
		
	)
	
}
function gridDataViewEditDoubleGridDokter_SetupOperasi_Ok(mod_id){
	
    var FieldGrdDoubleGridItemPemOperasi_IPOSetupOperasi_Ok = ['kd_item','item'];
    dsvDoubleGridItemPemOperasi_IPOSetupOperasiOK= new WebApp.DataStore({
        fields: FieldGrdDoubleGridItemPemOperasi_IPOSetupOperasi_Ok
    });
	dsGridDoubleGridItemPemOperasi_IPOSetupOperasiOK();
 	var fields = [
		{name: 'kd_item', mapping : 'kd_item'},
		{name: 'item', mapping : 'item'}
	];
	
	
	 	firstgridSetupOperasi_IPO = new Ext.grid.GridPanel({
            ddGroup          : 'secondGridDDGroup',
            store            : dsvDoubleGridItemPemOperasi_IPOSetupOperasiOK,
            autoScroll       : true,
            columnLines      : true,
            border           : true,
            enableDragDrop   : true,
            height           : 450,
            stripeRows       : true,
            trackMouseOver   : true,
            //title            : 'Daftar No Reg Barang',
            anchor           : '100% 100%',
            plugins          : [new Ext.ux.grid.FilterRow()],
            colModel         : new Ext.grid.ColumnModel
                            (
                                    [
                                            new Ext.grid.RowNumberer(),
                                            {
                                                    id: 'colNamaItem_DoubleGridIPO',
                                                    header: 'Nama item',
                                                    dataIndex: 'item',
                                                    sortable: true,
                                                    width: 20
                                            }
                                    ]
                                ),
   			  listeners : {
                    afterrender : function(comp) {
                    var firstgridSetupOperasi_IPODropTargetEl = firstgridSetupOperasi_IPO.getView().scroller.dom;
                    var firstgridSetupOperasi_IPODropTarget = new Ext.dd.DropTarget(firstgridSetupOperasi_IPODropTargetEl, {
                            ddGroup    : 'firstgridSetupDokDDGroup',
                            notifyDrop : function(ddSource, e, data){
                                    var records =  ddSource.dragData.selections;
                                    Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
                                    firstgridSetupOperasi_IPO.store.add(records);
									datahapusItemKelompokOperasi(data.selections[0].data.kd_item);
                                    firstgridSetupOperasi_IPO.store.sort('kd_item', 'ASC');
                                    return true
                            }
                    });
                    }
                },
             viewConfig: 
                    {
                            forceFit: true
                    }
        });

       secondGridStoreSetupOperasi_IPO = new Ext.data.JsonStore({
            fields : FieldGrdDoubleGridItemPemOperasi_IPOSetupOperasi_Ok,
            //root   : 'records'
        }); 
		
        // create the destination Grid
        secondGridSetupOperasi_IPO = new Ext.grid.GridPanel({
                    ddGroup          : 'firstgridSetupDokDDGroup',
                    store            : secondGridStoreSetupOperasi_IPO,
                    columns          : [
											new Ext.grid.RowNumberer(),
											{header: "Kode item", width: 100, sortable: true, dataIndex: 'kd_item',hidden:true},
											{header: "Nama item", width: 100, sortable: true, dataIndex: 'item'},
									   ],
                    enableDragDrop   : true,
                    height           : 450,
                    stripeRows       : true,
                    autoExpandColumn : 'nama',
                   // title            : 'Daftar Pilihan No Reg Barang',
                    listeners : {
                    afterrender : function(comp) {
						var secondGridDropSetupOperasi_IPOTargetEl = secondGridSetupOperasi_IPO.getView().scroller.dom;
						var secondGridDropSetupOperasi_IPOTarget = new Ext.dd.DropTarget(secondGridDropSetupOperasi_IPOTargetEl, {
								ddGroup    : 'secondGridDDGroup',
								notifyDrop : function(ddSource, e, data){
										var records =  ddSource.dragData.selections;
										Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
										//console.log(data.selections[0].data.nama);
										secondGridSetupOperasi_IPO.store.add(records);
										datasaveItemKelompokOperasi(data.selections[0].data.kd_item,data.selections[0].data.item);
										//secondGrid.store.sort('kd_dokter', 'ASC');
										return true
										
								}
						});
						},
					rowclick: function (sm, row, rec) {
						
						rowselectedDokter=secondGridStoreSetupOperasi_IPO.getAt(row);
					}
                },
                viewConfig: 
                    {
                            forceFit: true
                    }
        });
		
		var FrmTabs_SetupOpOk = new Ext.Panel
        (
		{
		    id: 'formTab',
		    region: 'center',
		    layout: 'column',
            height       : 550,
			
			//title:  'Set dokter',
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding: 0px 0px 0px 0px',
		    border: false,
//		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
			width: '100%',
		    anchor: '99%',
		    //iconCls: icons_viInformasiUnitdokter,
		    items: 
			[
				{
					columnWidth: .49,
					layout: 'form',
					border: true,
					title:'Item Terpilih',
					autoScroll: true,
					bodyStyle: 'padding: 10px 10px 10px 10px',
					items:
					[secondGridSetupOperasi_IPO
						
					],
					tbar :[
						{
							xtype: 'label',
							text: 'Cari Item: ' ,
							
						},
						{xtype: 'tbspacer',height: 3, width:5},
						 {
							x: 40,
							y: 40,
							xtype: 'textfield',
							//fieldLabel: 'No. Medrec',
							name: 'txtcariitemop_IPO',
							id: 'txtcariitemop_IPO',
							width: 150,
							listeners:
							{
								'specialkey': function ()
									{
										if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
										{
											
										}
									}
							}
						},
						{
							text: 'Cari',
							id: 'btnCaritxtitemop_IPOOperasi',
							iconCls: 'find',
							handler: function()
							{
								secondGridStoreSetupOperasi_IPO.removeAll();
								dsGridItemKelompokOperasi(Ext.getCmp('txtcariitemop_IPO').getValue());
							}
						},
					]
				},
				{
					columnWidth: .50,
					layout: 'form',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: true,
					title:'Semua Item',
					anchor: '100% 100%',
					items:
					[
						firstgridSetupOperasi_IPO,
						
						
						
					],
					tbar :[
						{
							xtype: 'label',
							text: 'Cari Item: ' ,
							
						},
						{xtype: 'tbspacer',height: 3, width:5},
						 {
							x: 40,
							y: 40,
							xtype: 'textfield',
							//fieldLabel: 'No. Medrec',
							name: 'txtcariitemop_IPOAll',
							id: 'txtcariitemop_IPOAll',
							width: 150,
							listeners:
							{
								'specialkey': function ()
									{
										/* if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
										{
											dsGridSetPerawatJadwalOperasiOK(Ext.getCmp('txtcariperawat').getValue());
										} */
									}
							}
						},
						{
							text: 'Cari',
							id: 'btnCariitemop_IPOAllOperasi',
							iconCls: 'find',
							handler: function()
							{
								dsvDoubleGridItemPemOperasi_IPOSetupOperasiOK.removeAll();
								txtcariitemop_IPO(Ext.getCmp('txtcariitemop_IPOAll').getValue());
							}
						}
					]
				},
			]
		});
		return FrmTabs_SetupOpOk;
}
function PanelItemPemOKAnastesi(){
var items = 
	{
		title:'Kelompok Anastesi',
		layout:'form',
		border: false,
		items:
		[
			gridDataViewEditDoubleGridDokter_SetupAnastesi_Ok()
		]
	}
	return items;
}
function dsGridDoubleGridItemPemAnastesi_IPOSetupAnastesiOK(){
	dsvDoubleGridItemPemAnastesi_IPOSetupAnastesiOK.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'vGridItemPemOperasiSetupOK',
                }			
            }
        );   
    return dsvDoubleGridItemPemAnastesi_IPOSetupAnastesiOK;
}
function dsGridItemKelompokAnastesi(namaItem){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/kamar_operasi/functionItemPemeriksaanOK/getItemKelompokAnastesi",
			params: {
						kdunit:"ok_default_kamar",
						nama:namaItem
					},
			failure: function(o)
			{
				ShowPesanErrorOKItemPemeriksaanOK('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					for(var i=0; i<cst.listData.length; i++){
						
						var recs    = [],recType = secondGridStoreSetupAnastesi_IPO.recordType;
						var o=cst.listData[i];
						recs    = [];
						recs.push(new recType(o));
						
						secondGridStoreSetupAnastesi_IPO.add(recs);
					}
					
				}
				else 
				{
					ShowPesanErrorOKItemPemeriksaanOK('Gagal membaca data', 'Error');
				};
			}
		}
		
	)
	
}
function datasaveItemKelompokAnastesi(kditem,item){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/kamar_operasi/functionItemPemeriksaanOK/simpanItemKelompokAnastesi",
			params: {
						kditem:kditem,
						namaitem:item
					},
			failure: function(o)
			{
				ShowPesanErrorOKItemPemeriksaanOK('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					ShowPesanInfoOKItemPemeriksaanOK("Item berhasil disimpan","Sukses");
					secondGridStoreSetupAnastesi_IPO.removeAll();
					dsGridDoubleGridItemPemAnastesi_IPOSetupAnastesiOK();
					dsGridDoubleGridItemPemOperasi_IPOSetupOperasiOK();
					dsGridItemKelompokAnastesi();
				}
				else 
				{
					ShowPesanErrorOKItemPemeriksaanOK('Gagal menyimpan data', 'Error');
				};
			}
		}
		
	)
	
}
function datahapusItemKelompokAnastesi(kditem){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/kamar_operasi/functionItemPemeriksaanOK/hapusItemKelompokAnastesi",
			params: {
						kditem:kditem
					},
			failure: function(o)
			{
				ShowPesanErrorOKItemPemeriksaanOK('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					secondGridStoreSetupAnastesi_IPO.removeAll();
					dsGridItemKelompokOperasi();
					dsGridItemKelompokAnastesi();
					dsGridDoubleGridItemPemAnastesi_IPOSetupAnastesiOK();
					dsGridDoubleGridItemPemOperasi_IPOSetupOperasiOK();
				}
				else 
				{
					ShowPesanErrorOKItemPemeriksaanOK('Gagal memproses data', 'Error');
				};
			}
		}
		
	)
	
}
function gridDataViewEditDoubleGridDokter_SetupAnastesi_Ok(mod_id){
	
    var FieldGrdDoubleGridItemPemAnastesi_IPOSetupAnastesi_Ok = ['kd_item','item'];
    dsvDoubleGridItemPemAnastesi_IPOSetupAnastesiOK= new WebApp.DataStore({
        fields: FieldGrdDoubleGridItemPemAnastesi_IPOSetupAnastesi_Ok
    });
	dsGridDoubleGridItemPemAnastesi_IPOSetupAnastesiOK();
 	var fields = [
		{name: 'kd_item', mapping : 'kd_item'},
		{name: 'item', mapping : 'item'}
	];
	
	
	 	firstgridSetupAnastesi_IPO = new Ext.grid.GridPanel({
            ddGroup          : 'secondGridDDGroup',
            store            : dsvDoubleGridItemPemAnastesi_IPOSetupAnastesiOK,
            autoScroll       : true,
            columnLines      : true,
            border           : true,
            enableDragDrop   : true,
            height           : 450,
            stripeRows       : true,
            trackMouseOver   : true,
            //title            : 'Daftar No Reg Barang',
            anchor           : '100% 100%',
            plugins          : [new Ext.ux.grid.FilterRow()],
            colModel         : new Ext.grid.ColumnModel
                            (
                                    [
                                            new Ext.grid.RowNumberer(),
                                            {
                                                    id: 'colNamaItemAn_DoubleGridIPO',
                                                    header: 'Nama item',
                                                    dataIndex: 'item',
                                                    sortable: true,
                                                    width: 20
                                            }
                                    ]
                                ),
   			  listeners : {
                    afterrender : function(comp) {
                    var firstgridSetupAnastesi_IPODropTargetEl = firstgridSetupAnastesi_IPO.getView().scroller.dom;
                    var firstgridSetupAnastesi_IPODropTarget = new Ext.dd.DropTarget(firstgridSetupAnastesi_IPODropTargetEl, {
                            ddGroup    : 'firstgridSetupDokDDGroup',
                            notifyDrop : function(ddSource, e, data){
                                    var records =  ddSource.dragData.selections;
                                    Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
                                    firstgridSetupAnastesi_IPO.store.add(records);
									datahapusItemKelompokAnastesi(data.selections[0].data.kd_item);
                                    firstgridSetupAnastesi_IPO.store.sort('kd_item', 'ASC');
                                    return true
                            }
                    });
                    }
                },
             viewConfig: 
                    {
                            forceFit: true
                    }
        });

       secondGridStoreSetupAnastesi_IPO = new Ext.data.JsonStore({
            fields : FieldGrdDoubleGridItemPemAnastesi_IPOSetupAnastesi_Ok,
            //root   : 'records'
        }); 
		
        // create the destination Grid
        secondGridSetupAnastesi_IPO = new Ext.grid.GridPanel({
                    ddGroup          : 'firstgridSetupDokDDGroup',
                    store            : secondGridStoreSetupAnastesi_IPO,
                    columns          : [
											new Ext.grid.RowNumberer(),
											{header: "Kode item", width: 100, sortable: true, dataIndex: 'kd_item',hidden:true},
											{header: "Nama item", width: 100, sortable: true, dataIndex: 'item'},
									   ],
                    enableDragDrop   : true,
                    height           : 450,
                    stripeRows       : true,
                    autoExpandColumn : 'nama',
                   // title            : 'Daftar Pilihan No Reg Barang',
                    listeners : {
                    afterrender : function(comp) {
						var secondGridDropSetupAnastesi_IPOTargetEl = secondGridSetupAnastesi_IPO.getView().scroller.dom;
						var secondGridDropSetupAnastesi_IPOTarget = new Ext.dd.DropTarget(secondGridDropSetupAnastesi_IPOTargetEl, {
								ddGroup    : 'secondGridDDGroup',
								notifyDrop : function(ddSource, e, data){
										var records =  ddSource.dragData.selections;
										Ext.each(records, ddSource.grid.store.remove, ddSource.grid.store);
										//console.log(data.selections[0].data.nama);
										secondGridSetupAnastesi_IPO.store.add(records);
										datasaveItemKelompokAnastesi(data.selections[0].data.kd_item,data.selections[0].data.item);
										//secondGrid.store.sort('kd_dokter', 'ASC');
										return true
										
								}
						});
						},
					rowclick: function (sm, row, rec) {
						
						//rowselectedDokter=secondGridStoreSetupOperasi_IPO.getAt(row);
					}
                },
                viewConfig: 
                    {
                            forceFit: true
                    }
        });
		
		var FrmTabs_SetupAnOk = new Ext.Panel
        (
		{
		    id: 'formTab',
		    region: 'center',
		    layout: 'column',
            height       : 550,
			
			//title:  'Set dokter',
		    itemCls: 'blacklabel',
		    bodyStyle: 'padding: 0px 0px 0px 0px',
		    border: false,
//		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    margins: '0 5 5 0',
			width: '100%',
		    anchor: '99%',
		    //iconCls: icons_viInformasiUnitdokter,
		    items: 
			[
				{
					columnWidth: .49,
					layout: 'form',
					border: true,
					title:'Dokter Inap',
					autoScroll: true,
					bodyStyle: 'padding: 10px 10px 10px 10px',
					items:
					[secondGridSetupAnastesi_IPO
						
					],
					tbar :[
						{
							xtype: 'label',
							text: 'Cari Item: ' ,
							
						},
						{xtype: 'tbspacer',height: 3, width:5},
						 {
							x: 40,
							y: 40,
							xtype: 'textfield',
							//fieldLabel: 'No. Medrec',
							name: 'txtcariiteman_IPO',
							id: 'txtcariiteman_IPO',
							width: 150,
							listeners:
							{
								'specialkey': function ()
									{
										if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
										{
											
										}
									}
							}
						},
						{
							text: 'Cari',
							id: 'btnCaritxtiteman_IPOOperasi',
							iconCls: 'find',
							handler: function()
							{
								secondGridStoreSetupAnastesi_IPO.removeAll();
								dsGridItemKelompokAnastesi(Ext.getCmp('txtcariiteman_IPO').getValue());
							}
						},
					]
				},
				{
					columnWidth: .50,
					layout: 'form',
					bodyStyle: 'padding: 10px 10px 10px 10px',
					border: true,
					title:'Semua Item',
					anchor: '100% 100%',
					items:
					[
						firstgridSetupAnastesi_IPO,
						
						
						
					],
					tbar :[
						{
							xtype: 'label',
							text: 'Cari Item: ' ,
							
						},
						{xtype: 'tbspacer',height: 3, width:5},
						 {
							x: 40,
							y: 40,
							xtype: 'textfield',
							//fieldLabel: 'No. Medrec',
							name: 'txtcariiteman_IPOAll',
							id: 'txtcariiteman_IPOAll',
							width: 150,
							listeners:
							{
								'specialkey': function ()
									{
										/* if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
										{
											dsGridSetPerawatJadwalOperasiOK(Ext.getCmp('txtcariperawat').getValue());
										} */
									}
							}
						},
						{
							text: 'Cari',
							id: 'btnCariiteman_IPOAllOperasi',
							iconCls: 'find',
							handler: function()
							{
								dsvDoubleGridItemPemAnastesi_IPOSetupAnastesiOK.removeAll();
								dsGridItemKelompokAnastesi(Ext.getCmp('txtcariiteman_IPOAll').getValue());
							}
						}
					]
				},
			]
		});
		return FrmTabs_SetupAnOk;
}
function PanelItemPem(rowdata){
	var items = 
	{
		title:'Item Pemeriksaan',
		layout:'form',
		border: false,
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 80,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 10,
								xtype: 'label',
								text: 'Kode Item'
							},
							{
								x: 120,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 10,
								xtype: 'textfield',
								//fieldLabel: 'No. Medrec',
								name: 'txtkodeitem_IP',
								id: 'txtkodeitem_IP',
								width: 100,
								disabled:true,
								listeners:
								{
									'specialkey': function ()
										{
											if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
											{
											}
										}
								}
							},
							
							{
								x: 10,
								y: 40,
								xtype: 'label',
								text: 'Nama Item'
							},
							{
								x: 120,
								y: 40,
								xtype: 'label',
								text: ':'
							},
							OKItemPemeriksaanOK.vars.NamaItemPemeriksaanItemPem=new Nci.form.Combobox.autoComplete({
								x: 130,
								y: 40,
								width:200,
								tabIndex:2,
								store	: OKItemPemeriksaanOK.form.ArrayStore.ItemPemeriksaanItemPem,
								select	: function(a,b,c){
									Ext.getCmp('txtkodeitem_IP').setValue(b.data.kd_item);
								},
								insert	: function(o){
									return {
										kd_item	: o.kd_item,
										item	: o.item,
										text		: '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_item+'</td><td width="70">'+o.item+'</td></tr></table>'
									}
								},
								url		: baseURL + "index.php/kamar_operasi/functionItemPemeriksaanOK/getDataAutoKomplitItemPemItemPemeriksaan",
								valueField: 'item',
								displayField: 'text',
								listWidth: 250
							}),
						
						]
					}
				]
			},
			{
				
						layout: 'form',
						//bodyStyle:'padding: 5px',
						border: true,
						items:
						[
							gridItemPem_ItemPemeriksaanOK()
						]
					
				
				
			}
		],
		tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAdd_ItemPemeriksaanItemPemOK_OK',
						handler: function(){
							addnewItemPem_ItemPemeriksaanOK_OK();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_ItemPemeriksaanItemPemOK_OK',
						handler: function()
						{
							datasaveItemPem_ItemPemeriksaanOK_OK();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnHapus_ItemPemeriksaanItemPemOK_OK',
						handler: function()
						{
							datadeleteItemPem_ItemPemeriksaanOK_OK();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_ItemPemeriksaanItemPemOK_OK',
						handler: function()
						{
							dataSourceItemPem_ItemPemeriksaanOK_OK.reload();
						}
					}
				]
			}
    };
        return items;
}

function dsItemPemOK_ItemPemeriksaanOK(noItemPemeriksaan)
{
	dataSourceItemPemOK_ItemPemeriksaanOK_OK.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: '',
					Sortdir: 'ASC',
					target: 'ViGridItemPemOKItemPemeriksaan',
					param: 'IDM.NO_ItemPemeriksaan =~'+noItemPemeriksaan+'~ ORDER BY IDM.NO_BARIS'
				}
		}
	);  
	return dataSourceItemPemOK_ItemPemeriksaanOK_OK;
}
function gridItemPemOK_ItemPemeriksaanOK(){
    var FieldGrd_viItemPemOKItemPemeriksaanOK = ['tgl_masuk','no_ItemPemeriksaan', 'kd_OK','no_urut_brg','nama_brg','satuan','jml_masuk','no_ruang','nama_sub','no_baris','no_register'];
    
	dataSourceItemPemOK_ItemPemeriksaanOK_OK= new WebApp.DataStore({
		fields: FieldGrd_viItemPemOKItemPemeriksaanOK
    });
	dsItemPemOK_ItemPemeriksaanOK();
    DataGridItemPemOKItemPemeriksaanOK =new Ext.grid.EditorGridPanel({
        xtype: 'editorgrid',
		store: dataSourceItemPemOK_ItemPemeriksaanOK_OK,
        height:600,
		width:'100%',
        autoScroll: true,
        columnLines: true,
		border: true,
		anchor: '100% 84%',
		flex:1,
		listeners:
		{
			// Function saat ada event double klik maka akan muncul form view # --------------
			rowdblclick: function (sm, ridx, cidx)
			{
				
			}
			// End Function # --------------
		},
		selModel: new Ext.grid.CellSelectionModel ({
            singleSelect: true,
            listeners:{
                rowselect: function(sm, row, rec){
                }
            }
        }),
        stripeRows: true,
        cm:new Ext.grid.ColumnModel([
        	new Ext.grid.RowNumberer(),	
			{
				dataIndex: 'kd_item',
				header: 'Kode Item ',
				sortable: true,
				width: 70
			},
			{
				dataIndex: 'nama_item',
				header: 'Nama Item Pemeriksaan',
				sortable: true,
				width: 70
			},
			
			//-------------- ## --------------
        ]),
		viewConfig:{
			forceFit: true
		}
    });
    return DataGridItemPemOKItemPemeriksaanOK;
}
function dsItemPem_ItemPemeriksaanOK(noItemPemeriksaan)
{
	dataSourceItemPem_ItemPemeriksaanOK_OK.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: '',
					Sortdir: 'ASC',
					target: 'ViGridItemPemItemPemeriksaan',
				}
		}
	);  
	return dataSourceItemPem_ItemPemeriksaanOK_OK;
}
function gridItemPem_ItemPemeriksaanOK(){
    var FieldGrd_viItemPemItemPemeriksaanOK = ['kd_item', 'nama_item'];
    
	dataSourceItemPem_ItemPemeriksaanOK_OK= new WebApp.DataStore({
		fields: FieldGrd_viItemPemItemPemeriksaanOK
    });  
    
   	dsItemPem_ItemPemeriksaanOK();
    DataGridItemPemItemPemeriksaanOK =new Ext.grid.EditorGridPanel({
        xtype: 'editorgrid',
		store: dataSourceItemPem_ItemPemeriksaanOK_OK,
        height:600,
		width:500,
		autoScroll: true,
        columnLines: true,
		border: true,
		anchor: '100% 64%',
		flex:1,
		listeners:
		{
			// Function saat ada event double klik maka akan muncul form view # --------------
			rowdblclick: function (sm, ridx, cidx)
			{
				
			}
			// End Function # --------------
		},
		selModel: new Ext.grid.RowSelectionModel ({
            singleSelect: true,
            listeners:{
                rowselect: function(sm, row, rec){
					var cellSelecteddeskripsi;
					cellSelecteddeskripsi = dataSourceItemPem_ItemPemeriksaanOK_OK.getAt(row);
					CurrentData_ItemPemeriksaanOK_OK.data = cellSelecteddeskripsi;
					Ext.getCmp('txtkodeitem_IP').setValue(sm.selections.items[0].data.kd_item);
					OKItemPemeriksaanOK.vars.NamaItemPemeriksaanItemPem.setValue(sm.selections.items[0].data.nama_item);
                }
            }
        }),
        stripeRows: true,
        cm:new Ext.grid.ColumnModel([
        	new Ext.grid.RowNumberer(),	
			{
				dataIndex: 'kd_item',
				header: 'Kode Item ',
				sortable: true,
				width: 70
			},
			{
				dataIndex: 'nama_item',
				header: 'Nama Item Pemeriksaan',
				sortable: true,
				width: 70
			},
			//-------------- ## --------------
        ]),
		viewConfig:{
			forceFit: true
		}
    });
    return DataGridItemPemItemPemeriksaanOK;
}


//------------------GRID SUB  OBAT---------------------------------------------------------
function gridCekKondisi_ItemPemeriksaanOK(){
    var FieldItemPemeriksaan_OK_OK = ['kd_sub_jns', 'sub_'];
    
	dsDataItemPemeriksaan_OK_OK= new WebApp.DataStore({
		fields: FieldItemPemeriksaan_OK_OK
    });
	dsDataItemPemeriksaan_OK_OK.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: '',
					Sortdir: 'ASC',
					target: 'ViGridSubObat',
					param: ''
				}
		}
	);  
    
   
    DataGridSubObat =new Ext.grid.EditorGridPanel({
        xtype: 'editorgrid',
		store: dsDataItemPemeriksaan_OK_OK,
        height:190,
		width:'100%',
        columnLines: true,
		border: true,
		flex:1,
		listeners:
		{
			// Function saat ada event double klik maka akan muncul form view # --------------
			rowdblclick: function (sm, ridx, cidx)
			{
				rowSelected_ItemPemeriksaanOK_OK = dsDataItemPemeriksaan_OK_OK.getAt(ridx);
				if (rowSelected_ItemPemeriksaanOK_OK != undefined)
				{
					DataInitPanelSubObat(rowSelected_ItemPemeriksaanOK_OK.data);
				}
			}
			// End Function # --------------
		},
		selModel: new Ext.grid.CellSelectionModel ({
            singleSelect: true,
            listeners:{
                rowselect: function(sm, row, rec){
					DataInitPanelSubObat(rowSelected_ItemPemeriksaanOK_OK.data);
                }
            }
        }),
        stripeRows: true,
        cm:new Ext.grid.ColumnModel([
        	new Ext.grid.RowNumberer(),	
			{
				dataIndex: 'kd_sub_jns',
				header: 'Kode Sub ',
				sortable: true,
				width: 70
			},
			{
				dataIndex: 'sub_',
				header: 'Sub  obat',
				width: 70
			}
			//-------------- ## --------------
        ]),
		viewConfig:{
			forceFit: true
		}
    });
    return DataGridSubObat;
}

//------------------GRID GOLONGAN OBAT---------------------------------------------------------
function gridGolonganObat_viSetupMasterObat(){
    var FieldGrdGolongan_viSetupMasterObat = ['apt_kd_golongan', 'apt_golongan'];
    
	dsDataGrdGolongan_viSetupMasterObat= new WebApp.DataStore({
		fields: FieldGrdGolongan_viSetupMasterObat
    });
	dsDataGrdGolongan_viSetupMasterObat.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: '',
					Sortdir: 'ASC',
					target: 'ViGridGolObat',
					param: ''
				}
		}
	);  
    
   
    DataGridGolongan =new Ext.grid.EditorGridPanel({
        xtype: 'editorgrid',
		title: 'List Golongan Obat',
		store: dsDataGrdGolongan_viSetupMasterObat,
        height: 300,
		width : '100%',
        columnLines: true,
		border: true,
		flex:1,
		listeners:
		{
			// Function saat ada event double klik maka akan muncul form view # --------------
			rowdblclick: function (sm, ridx, cidx)
			{
				rowSelected_ItemPemeriksaanOK_OK = dsDataGrdGolongan_viSetupMasterObat.getAt(ridx);
				if (rowSelected_ItemPemeriksaanOK_OK != undefined)
				{
					DataInitPanelGolongan(rowSelected_ItemPemeriksaanOK_OK.data);
				}
			}
			// End Function # --------------
		},
		selModel: new Ext.grid.CellSelectionModel ({
            singleSelect: true,
            listeners:{
                rowselect: function(sm, row, rec){
					DataInitPanelGolongan(rowSelected_ItemPemeriksaanOK_OK.data);
                }
            }
        }),
        stripeRows: true,
        cm:new Ext.grid.ColumnModel([
        	new Ext.grid.RowNumberer(),	
			{
				dataIndex: 'apt_kd_golongan',
				header: 'Kode Golongan',
				sortable: true,
				width: 70
			},
			{
				dataIndex: 'apt_golongan',
				header: 'Golongan',
				width: 70
			}
			//-------------- ## --------------
        ]),
		viewConfig:{
			forceFit: true
		}
    });
    return DataGridGolongan;
}

//------------------GRID SATUAN OBAT---------------------------------------------------------
function gridSatuan_viSetupMasterObat(){
    var FieldGrdSatuan_viSetupMasterObat = ['kd_satuan', 'satuan'];
    
	dsDataGrdSatuan_viSetupMasterObat= new WebApp.DataStore({
		fields: FieldGrdSatuan_viSetupMasterObat
    });
	dsDataGrdSatuan_viSetupMasterObat.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: '',
					Sortdir: 'ASC',
					target: 'ViGridSatuan',
					param: ''
				}
		}
	);  
    
   
    DataGridSatuan =new Ext.grid.EditorGridPanel({
        xtype: 'editorgrid',
		title: 'List Satuan Kecil',
		store: dsDataGrdSatuan_viSetupMasterObat,
        height: 300,
		width:550,
        columnLines: true,
		border: true,
		flex:1,
		listeners:
		{
			// Function saat ada event double klik maka akan muncul form view # --------------
			rowdblclick: function (sm, ridx, cidx)
			{
				rowSelected_ItemPemeriksaanOK_OK = dsDataGrdSatuan_viSetupMasterObat.getAt(ridx);
				if (rowSelected_ItemPemeriksaanOK_OK != undefined)
				{
					DataInitPanelSatuan(rowSelected_ItemPemeriksaanOK_OK.data);
				}
			}
			// End Function # --------------
		},
		selModel: new Ext.grid.CellSelectionModel ({
            singleSelect: true,
            listeners:{
                rowselect: function(sm, row, rec){
					DataInitPanelSatuan(rowSelected_ItemPemeriksaanOK_OK.data);
                }
            }
        }),
        stripeRows: true,
        cm:new Ext.grid.ColumnModel([
        	new Ext.grid.RowNumberer(),	
			{
				dataIndex: 'kd_satuan',
				header: 'Kode Satuan',
				sortable: true,
				width: 70
			},
			{
				dataIndex: 'satuan',
				header: 'Satuan',
				width: 70
			}
			//-------------- ## --------------
        ]),
		viewConfig:{
			forceFit: true
		}
    });
    return DataGridSatuan;
}

//------------------GRID SATUAN BESAR OBAT---------------------------------------------------------
function gridSatuanBesar_viSetupMasterObat(){
    var FieldGrdSatuanBesar_viSetupMasterObat = ['kd_sat_besar', 'keterangan'];
    
	dsDataGrdSatuanBesar_viSetupMasterObat= new WebApp.DataStore({
		fields: FieldGrdSatuanBesar_viSetupMasterObat
    });
	dsDataGrdSatuanBesar_viSetupMasterObat.load
	(
		{
			params:
				{
					Skip: 0,
					Take: 1000,
					Sort: '',
					Sortdir: 'ASC',
					target: 'ViGridSatuanBesar',
					param: ''
				}
		}
	);  
    
   
    DataGridSatuanBesar =new Ext.grid.EditorGridPanel({
        xtype: 'editorgrid',
		title: 'List Satuan Besar',
		store: dsDataGrdSatuanBesar_viSetupMasterObat,
        height: 300,
		width:550,
        columnLines: true,
		border: true,
		flex:1,
		listeners:
		{
			// Function saat ada event double klik maka akan muncul form view # --------------
			rowdblclick: function (sm, ridx, cidx)
			{
				rowSelected_ItemPemeriksaanOK_OK = dsDataGrdSatuanBesar_viSetupMasterObat.getAt(ridx);
				if (rowSelected_ItemPemeriksaanOK_OK != undefined)
				{
					DataInitPanelSatuanBesar(rowSelected_ItemPemeriksaanOK_OK.data);
				}
			}
			// End Function # --------------
		},
		selModel: new Ext.grid.CellSelectionModel ({
            singleSelect: true,
            listeners:{
                rowselect: function(sm, row, rec){
					DataInitPanelSatuanBesar(rowSelected_ItemPemeriksaanOK_OK.data);
                }
            }
        }),
        stripeRows: true,
        cm:new Ext.grid.ColumnModel([
        	new Ext.grid.RowNumberer(),	
			{
				dataIndex: 'kd_sat_besar',
				header: 'Kode Satuan',
				sortable: true,
				width: 70
			},
			{
				dataIndex: 'keterangan',
				header: 'Keterangan',
				width: 70
			}
			//-------------- ## --------------
        ]),
		viewConfig:{
			forceFit: true
		}
    });
    return DataGridSatuanBesar;
}


//------------------------------ Obat----------------------------------------------
function DataInitPanelObat(rowdata){
	Ext.getCmp('txtKdObat_OKItemPemeriksaanOKL').setValue(rowdata.kd_jns_obt);
	OKItemPemeriksaanOK.vars.nama.setValue(rowdata.nama_);
};

function addnewItemPemOK_ItemPemeriksaanOK_OK(){
	Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/OK/functionItemPemeriksaanOK/getNoOtoItemPemeriksaan",
				params: {
							jenItemPemeriksaan:1
						},
				failure: function(o)
				{
					ShowPesanErrorOKItemPemeriksaanOK('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						OKItemPemeriksaanOK.vars.NamaItemPemeriksaanItemPemOK.setValue(cst.noItemPemeriksaan);
						Ext.getCmp('tglItemPemeriksaanItemPemOKOK_OK').setValue(now_ItemPemeriksaanOK_OK);
						Ext.getCmp('TxtKeteranganItemPemeriksaanItemPemOKFilterGridDataView_ItemPemeriksaanOK_OK').setValue('');
						dsItemPemOK_ItemPemeriksaanOK(cst.noItemPemeriksaan);
						//dataSourceItemPemOK_ItemPemeriksaanOK_OK.reload();
					}
					else 
					{
						ShowPesanErrorOKItemPemeriksaanOK('Gagal Mendapatkan No ItemPemeriksaan', 'Error');
					};
				}
			}
			
		)
}
function addnewItemPem_ItemPemeriksaanOK_OK(){
	Ext.getCmp('txtkodeitem_IP').setValue("");
	OKItemPemeriksaanOK.vars.NamaItemPemeriksaanItemPem.setValue("");
	OKItemPemeriksaanOK.vars.NamaItemPemeriksaanItemPem.focus();
}

function datasaveItemPemOK_ItemPemeriksaanOK_OK(){
	if (ValidasiEntryItemPemOKItemPemeriksaan(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/OK/functionItemPemeriksaanOK/saveItemPemeriksaan",
				params: getParamItemPemOKItemPemeriksaan(),
				failure: function(o)
				{
					ShowPesanErrorOKItemPemeriksaanOK('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoOKItemPemeriksaanOK(nmPesanSimpanSukses,nmHeaderSimpanData);
						dataSourceItemPemOK_ItemPemeriksaanOK_OK.reload();
					}
					else 
					{
						ShowPesanErrorOKItemPemeriksaanOK('Gagal Menyimpan Data ini', 'Error');
						dataSourceItemPemOK_ItemPemeriksaanOK_OK.reload();
					};
				}
			}
			
		)
	}
}

function getParamItemPemOKItemPemeriksaan(){
	var	params =
	{
		panel:'ItemPemOK',
		jumlahrec:dataSourceItemPemOK_ItemPemeriksaanOK_OK.getCount(),
		noItemPemeriksaan:OKItemPemeriksaanOK.vars.NamaItemPemeriksaanItemPemOK.getValue(),
		tglItemPemeriksaan:Ext.getCmp('tglItemPemeriksaanItemPemOKOK_OK').getValue(),
		keterangan:Ext.getCmp('TxtKeteranganItemPemeriksaanItemPemOKFilterGridDataView_ItemPemeriksaanOK_OK').getValue()
	}
   	for(var i = 0 ; i < dataSourceItemPemOK_ItemPemeriksaanOK_OK.getCount();i++)
	{
		params['tglMasuk-'+i]=dataSourceItemPemOK_ItemPemeriksaanOK_OK.data.items[i].data.tgl_masuk;
		params['kdKelompok-'+i]=dataSourceItemPemOK_ItemPemeriksaanOK_OK.data.items[i].data.kd_OK;
		params['noregister-'+i]=dataSourceItemPemOK_ItemPemeriksaanOK_OK.data.items[i].data.no_register;
		params['urutan-'+i]=dataSourceItemPemOK_ItemPemeriksaanOK_OK.data.items[i].data.no_urut_brg;
		params['barang-'+i]=dataSourceItemPemOK_ItemPemeriksaanOK_OK.data.items[i].data.nama_brg;
		params['kelompok-'+i]=dataSourceItemPemOK_ItemPemeriksaanOK_OK.data.items[i].data.nama_sub;
		params['satuan-'+i]=dataSourceItemPemOK_ItemPemeriksaanOK_OK.data.items[i].data.satuan;
		params['noruang-'+i]=dataSourceItemPemOK_ItemPemeriksaanOK_OK.data.items[i].data.no_ruang;
		params['jumlah-'+i]=dataSourceItemPemOK_ItemPemeriksaanOK_OK.data.items[i].data.jml_masuk;
		params['keterangan-'+i]=dataSourceItemPemOK_ItemPemeriksaanOK_OK.data.items[i].data.ket;
		
	}
    return params
};

function ValidasiEntryItemPemOKItemPemeriksaan(modul,mBolHapus){
	var x = 1;
	if(OKItemPemeriksaanOK.vars.NamaItemPemeriksaanItemPemOK.getValue() === ''){
		ShowPesanWarningOKItemPemeriksaanOK('Nomor masih kosong', 'Warning');
		x = 0;
	} 
	if (dataSourceItemPemOK_ItemPemeriksaanOK_OK.getCount()===0)
		{
			ShowPesanWarningOKItemPemeriksaanOK('Data Belum Diisi Lengkap', 'Warning');
			x = 0;
		}
	for(var i = 0 ; i < dataSourceItemPemOK_ItemPemeriksaanOK_OK.getCount();i++)
	{
		var o=dataSourceItemPemOK_ItemPemeriksaanOK_OK.getRange()[i].data;
		//console.log(o.jml_pindah);
		if (o.jml_pindah !== undefined)
		/*{
			for(var j = i+1 ; j < dataSourceItemPemOK_ItemPemeriksaanOK_OK.getCount();j++)
			{
				var p=dataSourceItemPemOK_ItemPemeriksaanOK_OK.getRange()[j].data;
				if (p.no_urut_brg==o.no_urut_brg)
				{
					ShowPesanWarningOKItemPemeriksaanOK('Barang Tidak Boleh Sama', 'Warning');
					x = 0;
				}
			}
		}
		else*/
		{
			ShowPesanWarningOKItemPemeriksaanOK('Data Belum Diisi Lengkap', 'Warning');
			x = 0;
		}
		else
		{
			x = 1;
		}
		
	}
	return x;
};

function datadeleteItemPem_ItemPemeriksaanOK_OK(){
	if (ValidasiHapusItemPemItemPemeriksaan(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Msg.show
                (
                        {
                            title: 'Hapus Data',
                            msg: 'Apakah data ini akan dihapus ?',
                            buttons: Ext.MessageBox.YESNO,
                            fn: function (btn)
                            {
                                if (btn == 'yes')
                                {
                                    Ext.Ajax.request
									(
										{
											url: baseURL + "index.php/kamar_operasi/functionItemPemeriksaanOK/delete",
											params: getParamItemPemItemPemeriksaan(),
											failure: function(o)
											{
												ShowPesanErrorOKItemPemeriksaanOK('Hubungi Admin', 'Error');
											},	
											success: function(o) 
											{
												var cst = Ext.decode(o.responseText);
												if (cst.success === true) 
												{
													ShowPesanInfoOKItemPemeriksaanOK("Berhasil menghapus data ini",nmHeaderSimpanData);
													dataSourceItemPem_ItemPemeriksaanOK_OK.reload();
													addnewItemPem_ItemPemeriksaanOK_OK();
													OKItemPemeriksaanOK.vars.NamaItemPemeriksaanItemPem.focus();
													CurrentData_ItemPemeriksaanOK_OK.data.data = undefined;
												}
												else 
												{
													ShowPesanErrorOKItemPemeriksaanOK('Gagal Menyimpan Data ini', 'Error');
													dataSourceItemPem_ItemPemeriksaanOK_OK.reload();
												};
											}
										}
										
									)
                                }
                            },
                            icon: Ext.MessageBox.QUESTION
                        }
                ); 
		
	}
}
function datasaveItemPem_ItemPemeriksaanOK_OK(){
	if (ValidasiEntryItemPemItemPemeriksaan(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/kamar_operasi/functionItemPemeriksaanOK/save",
				params: getParamItemPemItemPemeriksaan(),
				failure: function(o)
				{
					ShowPesanErrorOKItemPemeriksaanOK('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoOKItemPemeriksaanOK(nmPesanSimpanSukses,nmHeaderSimpanData);
						dataSourceItemPem_ItemPemeriksaanOK_OK.reload();
						Ext.getCmp('txtkodeitem_IP').setValue(cst.kd_item);
						OKItemPemeriksaanOK.vars.NamaItemPemeriksaanItemPem.focus();
						CurrentData_ItemPemeriksaanOK_OK.data.data = undefined;
					}
					else 
					{
						ShowPesanErrorOKItemPemeriksaanOK('Gagal Menyimpan Data ini', 'Error');
						dataSourceItemPem_ItemPemeriksaanOK_OK.reload();
					};
				}
			}
			
		)
	}
}

function getParamItemPemItemPemeriksaan(){
	var	params =
	{
		kodeItemPemeriksaan:Ext.getCmp('txtkodeitem_IP').getValue(),
		namaItemPemeriksaan:OKItemPemeriksaanOK.vars.NamaItemPemeriksaanItemPem.getValue()
	}
    return params
};

function ValidasiEntryItemPemItemPemeriksaan(modul,mBolHapus){
	var x = 1;
	if(OKItemPemeriksaanOK.vars.NamaItemPemeriksaanItemPem.getValue() === ''){
		ShowPesanWarningOKItemPemeriksaanOK('Nama masih kosong', 'Warning');
		x = 0;
	} 
	return x;
};
function ValidasiHapusItemPemItemPemeriksaan(modul,mBolHapus){
	var x = 1;
	if(CurrentData_ItemPemeriksaanOK_OK.data.data == undefined){
		ShowPesanWarningOKItemPemeriksaanOK('Tidak ada data yang dipilih', 'Warning');
		x = 0;
	} 
	return x;
};





//------------------------------Sub  Obat----------------------------------------------
function DataInitPanelSubObat(rowdata){
	Ext.getCmp('txtKdSubObat_OKItemPemeriksaanOKL').setValue(rowdata.kd_sub_jns);
	OKItemPemeriksaanOK.vars.sub.setValue(rowdata.sub_);
};

function dataaddnewSub_ItemPemeriksaanOK_OK(){
	Ext.getCmp('txtKdSubObat_OKItemPemeriksaanOKL').setValue('');
	OKItemPemeriksaanOK.vars.sub.setValue('');
}

function datasaveSub_ItemPemeriksaanOK_OK(){
	if (ValidasiEntrySubObat(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionOKItemPemeriksaanOK/saveSub",
				params: getParamSetupSubObat(),
				failure: function(o)
				{
					ShowPesanErrorOKItemPemeriksaanOK('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoOKItemPemeriksaanOK(nmPesanSimpanSukses,nmHeaderSimpanData);
						Ext.get('txtKdSubObat_OKItemPemeriksaanOKL').dom.value=cst.kdsub;
						dsDataItemPemeriksaan_OK_OK.reload();
					}
					else 
					{
						ShowPesanErrorOKItemPemeriksaanOK('Gagal Menyimpan Data ini', 'Error');
						dsDataItemPemeriksaan_OK_OK.reload();
					};
				}
			}
			
		)
	}
}

function getParamSetupSubObat() {
	var	params =
	{
		KdSubJns:Ext.getCmp('txtKdSubObat_OKItemPemeriksaanOKL').getValue(),
		Sub:OKItemPemeriksaanOK.vars.sub.getValue()
	}
   
    return params
};

function ValidasiEntrySubObat(modul,mBolHapus){
	var x = 1;
	if(OKItemPemeriksaanOK.vars.sub.getValue() === ''){
		ShowPesanWarningOKItemPemeriksaanOK('Sub  obat masih kosong', 'Warning');
		x = 0;
	} 
	return x;
};


//------------------------------GOLONGAN Obat----------------------------------------------
function DataInitPanelGolongan(rowdata){
	Ext.getCmp('txtKdGolObat_OKItemPemeriksaanOKL').setValue(rowdata.apt_kd_golongan);
	OKItemPemeriksaanOK.vars.golongan.setValue(rowdata.apt_golongan);
};

function dataaddnewGolongan_ItemPemeriksaanOK_OK(){
	Ext.getCmp('txtKdGolObat_OKItemPemeriksaanOKL').setValue('');
	OKItemPemeriksaanOK.vars.golongan.setValue('');
}

function datasaveGolongan_ItemPemeriksaanOK_OK(){
	if (ValidasiEntryGolongan(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionOKItemPemeriksaanOK/saveGolongan",
				params: getParamSetupGolongan(),
				failure: function(o)
				{
					ShowPesanErrorOKItemPemeriksaanOK('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoOKItemPemeriksaanOK(nmPesanSimpanSukses,nmHeaderSimpanData);
						Ext.get('txtKdGolObat_OKItemPemeriksaanOKL').dom.value=cst.kdgolongan;
						dsDataGrdGolongan_viSetupMasterObat.reload();
					}
					else 
					{
						ShowPesanErrorOKItemPemeriksaanOK('Gagal Menyimpan Data ini', 'Error');
						dsDataGrdGolongan_viSetupMasterObat.reload();
					};
				}
			}
			
		)
	}
}

function getParamSetupGolongan() {
	var	params =
	{
		KdGolongan:Ext.getCmp('txtKdGolObat_OKItemPemeriksaanOKL').getValue(),
		Golongan:OKItemPemeriksaanOK.vars.golongan.getValue()
	}
   
    return params
};

function ValidasiEntryGolongan(modul,mBolHapus){
	var x = 1;
	if(OKItemPemeriksaanOK.vars.golongan.getValue() === ''){
		ShowPesanWarningOKItemPemeriksaanOK('Golongan obat masih kosong', 'Warning');
		x = 0;
	} 
	return x;
};



//------------------------------SATUAN ----------------------------------------------
function DataInitPanelSatuan(rowdata){
	Ext.getCmp('txtKdSatuan_OKItemPemeriksaanOKL').setValue(rowdata.kd_satuan);
	OKItemPemeriksaanOK.vars.satuan.setValue(rowdata.satuan);
};

function dataaddnewSatuan_ItemPemeriksaanOK_OK(){
	Ext.getCmp('txtKdSatuan_OKItemPemeriksaanOKL').setValue('');
	OKItemPemeriksaanOK.vars.satuan.setValue('');
}

function datasaveSatuan_ItemPemeriksaanOK_OK(){
	if (ValidasiEntrySatuan(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionOKItemPemeriksaanOK/saveSatuan",
				params: getParamSetupSatuan(),
				failure: function(o)
				{
					ShowPesanErrorOKItemPemeriksaanOK('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoOKItemPemeriksaanOK(nmPesanSimpanSukses,nmHeaderSimpanData);
						dsDataGrdSatuan_viSetupMasterObat.reload();
					}
					else 
					{
						ShowPesanErrorOKItemPemeriksaanOK('Gagal Menyimpan Data ini', 'Error');
						dsDataGrdSatuan_viSetupMasterObat.reload();
					};
				}
			}
			
		)
	}
}

function getParamSetupSatuan(){
	var	params =
	{
		KdSatuan:Ext.getCmp('txtKdSatuan_OKItemPemeriksaanOKL').getValue(),
		Satuan:OKItemPemeriksaanOK.vars.satuan.getValue()
	}
   
    return params
};

function ValidasiEntrySatuan(modul,mBolHapus){
	var x = 1;
	if(Ext.getCmp('txtKdSatuan_OKItemPemeriksaanOKL').getValue() === ''){
		ShowPesanWarningOKItemPemeriksaanOK('Kode Satuan masih kosong', 'Warning');
		x = 0;
	}
	if(OKItemPemeriksaanOK.vars.satuan.getValue() === ''){
		ShowPesanWarningOKItemPemeriksaanOK('Satuan masih kosong', 'Warning');
		x = 0;
	} 
	if(Ext.getCmp('txtKdSatuan_OKItemPemeriksaanOKL').getValue().length > 10){
		ShowPesanWarningOKItemPemeriksaanOK('Kode satuan kecil tidak boleh lebih dari 10 huruf', 'Warning');
		x = 0;
	}
	return x;
};


//------------------------------SATUAN BESAR----------------------------------------------
function DataInitPanelSatuanBesar(rowdata){
	Ext.getCmp('txtKdSatuanBesar_OKItemPemeriksaanOKL').setValue(rowdata.kd_sat_besar);
	OKItemPemeriksaanOK.vars.satuanBesar.setValue(rowdata.keterangan);
};

function dataaddnewSatuanBesar_ItemPemeriksaanOK_OK(){
	Ext.getCmp('txtKdSatuanBesar_OKItemPemeriksaanOKL').setValue('');
	OKItemPemeriksaanOK.vars.satuanBesar.setValue('');
}

function datasaveSatuanBesar_ItemPemeriksaanOK_OK(){
	if (ValidasiEntrySatuanBesar(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/apotek/functionOKItemPemeriksaanOK/saveSatuanBesar",
				params: getParamSetupSatuanBesar(),
				failure: function(o)
				{
					ShowPesanErrorOKItemPemeriksaanOK('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanInfoOKItemPemeriksaanOK(nmPesanSimpanSukses,nmHeaderSimpanData);
						//Ext.get('txtKdSatuanBesar_OKItemPemeriksaanOKL').dom.value=cst.kdsatbesar;
						dsDataGrdSatuanBesar_viSetupMasterObat.reload();
					}
					else 
					{
						ShowPesanErrorOKItemPemeriksaanOK('Gagal Menyimpan Data ini', 'Error');
						dsDataGrdSatuanBesar_viSetupMasterObat.reload();
					};
				}
			}
			
		)
	}
}

function getParamSetupSatuanBesar(){
	var	params =
	{
		KdSatBesar:Ext.getCmp('txtKdSatuanBesar_OKItemPemeriksaanOKL').getValue(),
		Keterangan:OKItemPemeriksaanOK.vars.satuanBesar.getValue()
	}
   
    return params
};

function ValidasiEntrySatuanBesar(modul,mBolHapus){
	var x = 1;
	if(Ext.getCmp('txtKdSatuanBesar_OKItemPemeriksaanOKL').getValue() === ''){
		ShowPesanWarningOKItemPemeriksaanOK('Kode satuan besar satuan masih kosong', 'Warning');
		x = 0;
	}
	if(OKItemPemeriksaanOK.vars.satuanBesar.getValue() === ''){
		ShowPesanWarningOKItemPemeriksaanOK('Keterangan satuan masih kosong', 'Warning');
		x = 0;
	}	
	if(Ext.getCmp('txtKdSatuanBesar_OKItemPemeriksaanOKL').getValue().length > 10){
		ShowPesanWarningOKItemPemeriksaanOK('Kode satuan besar tidak boleh lebih dari 10 huruf', 'Warning');
		x = 0;
	}
	return x;
};



function ShowPesanWarningOKItemPemeriksaanOK(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorOKItemPemeriksaanOK(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoOKItemPemeriksaanOK(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};// JavaScript Document