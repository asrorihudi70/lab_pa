var dsvJenisAsisten;
var dsvJenisAsisten;
var rowselectedJenisAsisten;
var LookupJenisAsisten;
var disDataJenisAsisten=0;
var cek=0;
var kdSetupJenisAsisten;
var kdJenisAsistenJenisAsisten;
var kodeJenisAsisten;
var stateComboJenisAsisten=['KD_Asisten_Asisten','Asisten_Asisten'];
var pilihAksiJenisAsisten;
var SetupJenisAsisten={};
SetupJenisAsisten.form={};
SetupJenisAsisten.func={};
SetupJenisAsisten.vars={};
SetupJenisAsisten.func.parent=SetupJenisAsisten;
SetupJenisAsisten.form.ArrayStore={};
SetupJenisAsisten.form.ComboBox={};
SetupJenisAsisten.form.DataStore={};
SetupJenisAsisten.form.Record={};
SetupJenisAsisten.form.Form={};
SetupJenisAsisten.form.Grid={};
SetupJenisAsisten.form.Panel={};
SetupJenisAsisten.form.TextField={};
SetupJenisAsisten.form.Button={};

SetupJenisAsisten.form.ArrayStore.JenisAsisten=new Ext.data.ArrayStore({
		id: 0,
		fields: [	
					'kd_Asisten','Asisten_diet','harga_pokok'
				],
		data: []
	});
	
CurrentPage.page = getPanelJenisAsisten(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dsJenisAsisten(){
	dsvJenisAsisten.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'vAsistenOperasi'
                    //param : kriteria
                }			
            }
        );   
    return dsvJenisAsisten;
	}

	
function getPanelJenisAsisten(mod_id) {
    var Field = ['kd_jenis_asisten','asisten'];
    dsvJenisAsisten = new WebApp.DataStore({
        fields: Field
    });
	dsJenisAsisten();
    var gridListHasilJenisAsisten = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dsvJenisAsisten,
        anchor: '100% 70%',
        columnLines: false,
        autoScroll: false,
        border: true,
        sort: false,
        autoHeight: false,
        layout: 'fit',
        region: 'center',
        height: 200,
        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
            listeners: {
                rowselect: function (sm, row, rec) {
					rowselectedJenisAsisten=dsvJenisAsisten.getAt(row);
                }
            }
        }),
        listeners: {
           rowdblclick: function (sm, ridx, cidx) {
                rowselectedJenisAsisten=dsvJenisAsisten.getAt(ridx);
				ShowLookupJenisAsisten(rowselectedJenisAsisten.json);
				/*disData=1;
				disabled_data(disData);*/
				
            }
        },
        cm: new Ext.grid.ColumnModel(
                [
				new Ext.grid.RowNumberer({
					header:'No.'
					}),
                    {
                        id: 'colNoJenisAsistenViewHasilJenisAsisten',
                        header: 'Kode Jenis Asisten',
                        dataIndex: 'kd_jenis_asisten',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 5
                    },
                    {
                        id: 'colNamaJenisAsistenViewHasilJenisAsisten',
                        header: ' Jenis Asisten',
                        dataIndex: 'asisten',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
					
                ]
                ),
        viewConfig: {
            forceFit: true
        },
        tbar:[
			{
                id: 'btnEditDataJenisAsisten',
                text: 'Edit Data',
                tooltip: 'Edit Data',
                iconCls: 'Edit_Tr',
                handler: function (sm, row, rec) {
                    edit_dataJenisAsisten();
                },
				
            },
		]
    });
	   var FormDepanJenisAsisten = new Ext.Panel({
        id: mod_id,
        closable: true,
        region: 'center',
        layout: 'form',
        title: 'Setup Jenis Asisten',
        border: false,
        shadhow: true,
        autoScroll: false,
		width: 250,
        iconCls: 'Request',
        margins: '0 5 5 0',
		tbar: [
			
			{
                id: 'btnAddNewDataJenisAsisten',
                text: 'Add New',
                tooltip: 'addnewJenisAsisten',
                iconCls: 'add',
                handler: function (sm, row, rec) {
                    addnew_dataJenisAsisten();
				}
            },
			{
        	xtype: 'tbseparator'
       		},
			{
                id: 'btnSaveDataJenisAsisten',
                text: 'Save Data',
                tooltip: 'Save Data',
                iconCls: 'Save',
                handler: function (sm, row, rec) {
                    save_dataJenisAsisten();
					//alert(cek);
                },
				
            },
			{
        	xtype: 'tbseparator'
       		},
			{
                id: 'btnDeleteDataJenisAsisten',
                text: 'Delete Data',
                tooltip: 'Delete Data',
                iconCls: 'Remove',
                handler: function (sm, row, rec) {
					if (rowselectedJenisAsisten!=undefined)
                    	delete_dataJenisAsisten();
					else
						ShowPesanErrorJenisAsisten('Maaf ! Tidak ada data yang terpilih', 'Error');
				}
            },
			{
        	xtype: 'tbseparator'
       		},
			{
                id: 'btnRefreshDataJenisAsisten',
                text: 'Refresh',
                tooltip: 'Refresh',
                iconCls: 'Refresh',
                handler: function (sm, row, rec) {
                    refresh_dataJenisAsisten();
                },
				
            }
			],
        items: [
			getPanelPencarianJenisAsisten(),
			gridListHasilJenisAsisten,
            
            
//            PanelPengkajian(),
//            TabPanelPengkajian()
        ],
        listeners: {
            'afterrender': function () {

            }
        }
    });

    return FormDepanJenisAsisten;

}
;
function refresh_dataJenisAsisten()
{
	dsJenisAsisten();
}
function getPanelPencarianJenisAsisten() {
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 1190,
                height: 80,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'Kode'
                    },
                    {
                        x: 110,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 10,
                        xtype: 'textfield',
                        name: 'TxtKdJenisJenisAsisten',
                        id: 'TxtKdJenisJenisAsisten',
                        width: 80,
						disabled: true
                    },
					
                    
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Jenis Asisten'
                    },
                    {
                        x: 110,
                        y: 40,
                        xtype: 'label',
                        text: ' : '
                    },
					
					SetupJenisAsisten.form.ComboBox.JenisAsisten= new Nci.form.Combobox.autoComplete({
						//id:'TxtNamaJenisAsistenJenisAsistenome',
						store	: SetupJenisAsisten.form.ArrayStore.JenisAsisten,
						select	: function(a,b,c){
							Ext.getCmp('TxtKdJenisJenisAsisten').setValue(b.data.kd_jenis_asisten);
							/* var getKode_Gz=b.data.kd_JenisAsisten
							alert(getKode_Gz); */
							 /* Ext.Ajax.request
								(
									{
										url: baseURL + "index.php/gizi/JenisAsisten/getJenisAsisten/",
										params:{getKode_Gz:b.data.kd_JenisAsisten},
										failure: function(o)
										{
											ShowPesanErrorJenisAsisten('Hubungi Admin', 'Error');
										},	
										success: function(o) 
										{
											console.log(o.responseText);
											 var cst = Ext.decode(o.responseText);
											if (cst.listData != '') 
											{	
												Ext.getCmp('TxtKdJenisJenisAsisten').setValue(cst.dataKode);
												Ext.getCmp('CmbSetJenisAsisten').setValue(cst.dataKodeJenisAsisten);
												if (cst.dataTagBerlaku==0)
													{Ext.getCmp('CekBerlakuJenisAsisten').setValue(false);}
												else
													{Ext.getCmp('CekBerlakuJenisAsisten').setValue(true);}
												
												Ext.getCmp('TxtHrgPokokJenisAsisten').setValue(cst.dataHargapokok);
												Ext.getCmp('TxtHrgJualJenisAsisten').setValue(cst.dataHargaJual);
												kdSetupJenisAsisten=cst.dataKode;
												kodeJenisAsisten=cst.dataKodeJenisAsisten;
												pilihAksiJenisAsisten='update';
											}
											else 
											{
												Ext.getCmp('CmbSetJenisAsisten').setValue('');
												Ext.getCmp('TxtKdJenisJenisAsisten').setValue(b.data.kd_JenisAsisten);
												Ext.getCmp('TxtHrgPokokJenisAsisten').setValue(b.data.harga_pokok);
												Ext.getCmp('CekBerlakuJenisAsisten').setValue(false);
												Ext.getCmp('TxtHrgPokokJenisAsisten').setValue('');
												Ext.getCmp('TxtHrgJualJenisAsisten').setValue('');
												kdSetupJenisAsisten=cst.dataKode;
												kodeJenisAsisten=cst.dataKodeJenisAsisten;
												pilihAksiJenisAsisten='insert';
											}; 
										}
									}
									
								) */ 
						},
						width	: 200,
						
						insert	: function(o){
							return {
								kd_jenis_asisten        	:o.kd_jenis_asisten,
								asisten        	:o.asisten,
								text				:  '<table style="font-size: 11px;"><tr><td width="80">'+o.kd_jenis_asisten+'</td><td width="180">'+o.asisten+'</td></tr></table>',
							}
						},
						url		: baseURL + "index.php/kamar_operasi/functionSetupJenisAsisten/getAsistenGrid",
						valueField: 'asisten',
						displayField: 'text',
						listWidth: 260,
						x: 120,
                        y: 40,
						
					}),
					
                ]
            }
        ]
    };
    return items;
}
;
function ComboSetSetupJenisAsisten()
{
    dsvJenisAsisten = new WebApp.DataStore({fields: stateComboJenisAsisten});
	dsJenisAsisten();
	var CmbSetSetupJenisAsisten = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 70,
			id:'CmbSetJenisAsisten',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			/*readOnly:true,
			emptyText:'Set JenisAsisten',
			value: 1,*/
			width: 150,
			store: dsvJenisAsisten,
			valueField: stateComboJenisAsisten[0],
			displayField: stateComboJenisAsisten[1],
			//value:selectSetPilihankelompokPasien,
			listeners:
			{
				'select': function(a,b,c)
				{
					kdJenisAsistenJenisAsisten=CmbSetSetupJenisAsisten.value;
					//selectSetPilihankelompokPasien=b.data.displayText;
				}
			}
		}
	);
	return CmbSetSetupJenisAsisten;
}
function getData_Gz(getkode_Gz)
{
	Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/gizi/JenisAsisten/getJenisAsisten/"+getkode_Gz,
				//params: ParameterGetKode_Gz(getkode_Gz),
				failure: function(o)
				{
					ShowPesanErrorJenisAsisten('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanSuksesJenisAsisten("Data Berhasil Disimpan","Sukses");
						dsJenisAsisten();
						
					}
					else 
					{
						ShowPesanErrorJenisAsisten('Gagal Menyimpan Data ini', 'Error');
						dsJenisAsisten();
					};
				}
			}
			
		)
}
function save_dataJenisAsisten()
{
	if (ValidasiEntriJenisAsisten(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/kamar_operasi/functionSetupJenisAsisten/save",
				params: ParameterSaveJenisAsisten(),
				failure: function(o)
				{
					ShowPesanErrorJenisAsisten('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						ShowPesanSuksesJenisAsisten("Data Berhasil Disimpan","Sukses");
						dsJenisAsisten();
						Ext.getCmp("TxtKdJenisJenisAsisten").setValue(cst.kode);
					}
					else 
					{
						ShowPesanErrorJenisAsisten('Gagal Menyimpan Data ini', 'Error');
						dsJenisAsisten();
					};
				}
			}
			
		)
	}
}
function edit_dataJenisAsisten(){
	ShowLookupJenisAsisten(rowselectedJenisAsisten.json);
}
function delete_dataJenisAsisten() 
{
		Ext.Msg.show
		(
			{
			   title:"Setup Jenis Asisten",
			   msg: "Apakah yakin akan menghapus data ini ?" ,
			   buttons: Ext.MessageBox.YESNO,
			   width:250,
			   fn: function (btn) 
			   {			
					if (btn === 'yes') 
					{
						Ext.Ajax.request
						(
							{
								url: baseURL + "index.php/kamar_operasi/functionSetupJenisAsisten/delete",
								params: paramsDeleteJenisAsisten(rowselectedJenisAsisten.json),
								success: function(o) 
								{
									var cst = Ext.decode(o.responseText);
									if (cst.success === true) 
									{
										ShowPesanSuksesJenisAsisten(nmPesanHapusSukses,nmHeaderHapusData);
										dsJenisAsisten();
										ClearTextJenisAsisten();
									}
									else if (cst.success === false && cst.pesan===0)
									{
										ShowPesanErrorJenisAsisten(nmPesanHapusGagal,nmHeaderHapusData);
									}
									else 
									{
										ShowPesanErrorJenisAsisten(nmPesanHapusError,nmHeaderHapusData);
									};
								}
							}
						)
					};
				}
			}
		)
	};
	
function addnew_dataJenisAsisten()
{
	ClearTextJenisAsisten();
}
function ValidasiEntriJenisAsisten(modul,mBolHapus)
{
	var kode = Ext.getCmp('TxtKdJenisJenisAsisten').getValue();
	var nama = SetupJenisAsisten.form.ComboBox.JenisAsisten.getValue();
	
	var x = 1;
	if(nama === '' ){
	ShowPesanErrorJenisAsisten('Data Belum Diisi Lengkap', 'Warning');
	x = 0;
	}/*
	if( nama === '')
	{
	ShowPesanError('Data Belum Diisi Lengkap', 'Warning');
	x = 0;	
	}*/
	
	
	return x;
};
function ValidasiEditJenisAsisten(modul,mBolHapus)
{
	var kode = Ext.getCmp('TxtKdJenisJenisAsisten').getValue();
	var nama = SetupJenisAsisten.form.ComboBox.JenisAsisten.getValue();
	
	var x = 1;
	if(kode === '' ){
	ShowPesanErrorJenisAsisten('Data Belum Diisi Lengkap', 'Warning');
	x = 0;
	}
	if( nama === '')
	{
	ShowPesanErrorJenisAsisten('Data Belum Diisi Lengkap', 'Warning');
	x = 0;	
	}
	return x;
};

function ShowPesanSuksesJenisAsisten(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};

function ShowPesanErrorJenisAsisten(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:290
		}
	);
};
function ClearTextJenisAsisten()
{
	Ext.getCmp('TxtKdJenisJenisAsisten').setValue("");
	SetupJenisAsisten.form.ComboBox.JenisAsisten.setValue("");
	SetupJenisAsisten.form.ComboBox.JenisAsisten.focus();
}
/*function disabled_data(disDatax){
	if (disDatax==1) {
		var dis= 
				{
					kodeData : Ext.getCmp('TxtKdAhliGz').disable(),
				}
	}
	else if (disDatax==0) 
	{
		var dis= 
				{
					kodeData : Ext.getCmp('TxtKdAhliGz').enable(),
				}
	}
	return dis;
	}*/
function ParameterGetKode_Gz(getkode_Gz)
{
	var params =
	{
		Table:'vAsisten',
		kode_Gz: getkode_Gz
	}
}
function ParameterSaveJenisAsisten()
{
	
var params =
	{
	KdJenisAsisten : Ext.getCmp('TxtKdJenisJenisAsisten').getValue(),
	Nama : SetupJenisAsisten.form.ComboBox.JenisAsisten.getValue()
	}	
return params;	
}
function paramsDeleteJenisAsisten(rowdata) 
{
    var params =
	{
		kode :  rowdata.kd_jenis_asisten,
		//Data: ShowLookupDeleteJenisAsisten(rowselectedJenisAsisten.json)
	};
    return params
};
function ShowLookupJenisAsisten(rowdata)
{
	if (rowdata == undefined){
        ShowPesanErrorJenisAsisten('Data Kosong', 'Error');
    }
    else
    {
      datainit_formJenisAsisten(rowdata); 
    }
}
function ShowLookupDeleteJenisAsisten(rowdata)
{
      var kode =  rowdata.KD__;
	  var kodeJen = rowdata.KD_JenisAsisten_;
	  return kode;
}
function datainit_formJenisAsisten(rowdata){
	Ext.getCmp('TxtKdJenisJenisAsisten').setValue(rowdata.kd_jenis_asisten);
	SetupJenisAsisten.form.ComboBox.JenisAsisten.setValue(rowdata.asisten);
	//pilihAksiJenisAsisten='update';
	}