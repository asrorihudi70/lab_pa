var dataSource_viSetupTindakan;
var selectCount_viSetupTindakan=50;
var NamaForm_viSetupTindakan="Setup Tindakan";
var mod_name_viSetupTindakan="Setup Tindakan";
var now_viSetupTindakan= new Date();
var rowSelected_viSetupTindakan;
var setLookUps_viSetupTindakan;
var tanggal = now_viSetupTindakan.format("d/M/Y");
var jam = now_viSetupTindakan.format("H/i/s");
var tmpkriteria;
var DataGridJenisObat;
var GridDataView_viSetupTindakan;
var a;
var tmpkdtindakan;
var tmptindakan;
var ArrayStoreTindakan;
var setLookupNewTindakan_SetupTindakan;
var tmpnewtindakan;
var tmpnewkdtindakan;


var CurrentData_viSetupTindakan =
{
	data: Object,
	details: Array,
	row: 0
};

var SetupTindakan={};
SetupTindakan.form={};
SetupTindakan.func={};
SetupTindakan.vars={};
SetupTindakan.func.parent=SetupTindakan;
SetupTindakan.form.ArrayStore={};
SetupTindakan.form.ComboBox={};
SetupTindakan.form.DataStore={};
SetupTindakan.form.Record={};
SetupTindakan.form.Form={};
SetupTindakan.form.Grid={};
SetupTindakan.form.Panel={};
SetupTindakan.form.TextField={};
SetupTindakan.form.Button={};

ArrayStoreTindakan= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_tindakan', 'tindakan'],
	data: []
});

CurrentPage.page = dataGrid_viSetupTindakan(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dataGrid_viSetupTindakan(mod_id_viSetupTindakan){	
 	// Field kiriman dari Project Net.
    var FieldMaster_viSetupTindakan = 
	[
		'kd_spesial', 'spesialisasi', 'kd_sub_spc', 'sub_spesialisasi', 'kd_jenis_op', 'jenis_op', 'kd_tindakan','tindakan'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viSetupTindakan = new WebApp.DataStore
	({
        fields: FieldMaster_viSetupTindakan
    });
    load_data_spesialis();
	load_data_jenis_operasi();
	//load_data_sub_spesialis();
    // Grid lab Perencanaan # --------------
	GridDataView_viSetupTindakan = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viSetupTindakan,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:true,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viSetupTindakan = undefined;
							rowSelected_viSetupTindakan = dataSource_viSetupTindakan.getAt(row);
							CurrentData_viSetupTindakan
							CurrentData_viSetupTindakan.row = row;
							CurrentData_viSetupTindakan.data = rowSelected_viSetupTindakan.data;
							DataInitSetupTindakan(rowSelected_viSetupTindakan.data);
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viSetupTindakan = dataSource_viSetupTindakan.getAt(ridx);
					if (rowSelected_viSetupTindakan != undefined)
					{
						DataInitSetupTindakan(rowSelected_viSetupTindakan.data);
						//setLookUp_viSetupTindakan(rowSelected_viSetupTindakan.data);
					}
					else
					{
						
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Setup Unit
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					//-------------- ## --------------
					{
						header: 'Spesialisasi',
						dataIndex: 'spesialisasi',
						hideable:false,
						menuDisabled: true,
						width: 70
					},
					
					//-------------- ## --------------
					{
						header: 'Sub Spesialisasi',
						dataIndex: 'sub_spesialisasi',
						hideable:false,
						menuDisabled: true,
						width: 70
					},
					//-------------- ## --------------
					{
						header: 'Jenis Operasi',
						dataIndex: 'jenis_op',
						hideable:false,
						menuDisabled: true,
						width: 80
					},
					//-------------- ## --------------
					{
						header: 'Tindakan',
						dataIndex: 'tindakan',
						hideable:false,
						menuDisabled: true,
						width: 90
					},
					//-------------- ## --------------
					{
						header: 'Kode spesial',
						dataIndex: 'kd_spesial',
						hideable:false,
						menuDisabled: true,
						width: 40,
						hidden:true
					},
					//-------------- ## --------------
					{
						header: 'Kode Sub',
						dataIndex: 'kd_sub_spc',
						hideable:false,
						menuDisabled: true,
						width: 40,
						hidden:true,
					},
					//-------------- ## --------------
					{
						header: 'Kode jenis operasi',
						dataIndex: 'kd_jenis_op',
						hideable:false,
						menuDisabled: true,
						width: 40,
						hidden:true,
					},
					//-------------- ## --------------
					{
						header: 'Kode tindakan',
						dataIndex: 'kd_tindakan',
						hideable:false,
						menuDisabled: true,
						width: 40,
						hidden:true,
					},
					//-------------- ## --------------
					
				]
			),
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viSetupTindakan, selectCount_viSetupTindakan, dataSource_viSetupTindakan),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)	
	var PanelTabSetupTindakan = new Ext.FormPanel({
        labelAlign: 'top',
		layout:'form',
		border: false,  
		autoHeight:true,
        title: '',
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [PanelAwal()]
		
	})
	// Kriteria filter pada Grid # --------------
    var FrmData_viSetupTindakan = new Ext.Panel
    (
		{
			title: NamaForm_viSetupTindakan,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viSetupTindakan,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ PanelTabSetupTindakan,
					GridDataView_viSetupTindakan],
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAddSetupTindakan_viSetupTindakan',
						handler: function(){
							AddNewSetupTindakan();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viSetupTindakan',
						handler: function()
						{
							loadMask.show();
							dataSave_viSetupTindakan();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viSetupTindakan',
						handler: function()
						{
							Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
								if (button == 'yes'){
									loadMask.show();
									dataDelete_viSetupTindakan();
								}
							});
						}
					},
					{
						xtype: 'tbseparator'
					},
					/* {
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_viSetupPabrik',
						handler: function()
						{
							dataSource_viSetupTindakan.removeAll();
							//dataGriSetupTindakan();
						}
					},
					{
						xtype: 'tbseparator'
					} */
					
				]
			}
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_viSetupTindakan;
    //-------------- # End form filter # --------------
}

function PanelAwal(){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 120,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'Spesialisasi'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							comboSpesialis(),
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'Sub Spesialisasi'
							},
							{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							comboSubSpesialis(),
							{
								x: 10,
								y: 60,
								xtype: 'label',
								text: 'Jenis Operasi '
							},
							{
								x: 120,
								y: 60,
								xtype: 'label',
								text: ':'
							},
							comboJenisOperasi(),
							{
								x: 10,
								y: 90,
								xtype: 'label',
								text: 'Tindakan '
							},
							{
								x: 120,
								y: 90,
								xtype: 'label',
								text: ':'
							},
							tmptindakan=new Nci.form.Combobox.autoComplete({
								x: 130,
								y: 90,
								width	: 300,
								id		: 'txtComboTindakan_SetupPerawat',
								store	: ArrayStoreTindakan,
								select	: function(a,b,c){
									tmptindakan=b.data.tindakan;
									tmpkdtindakan=b.data.kd_tindakan;
									
									GridDataView_viSetupTindakan.getView().refresh();
									
								},
								/* onShowList:function(a){
									dataSource_viSetupTindakan.removeAll();
									
									var recs=[],
									recType=dataSource_viSetupTindakan.recordType;
										
									for(var i=0; i<a.length; i++){
										recs.push(new recType(a[i]));
									}
									dataSource_viSetupTindakan.add(recs);
									
								}, */
								insert	: function(o){
									return {
										kd_tindakan     : o.kd_tindakan,
										tindakan 		: o.tindakan,
										text			:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_tindakan+'</td><td width="200">'+o.tindakan+'</td></tr></table>'
									}
								},
								url		: baseURL + "index.php/kamar_operasi/functionSetupTindakan/getTindakan",
								valueField: 'tindakan',
								displayField: 'text',
								listWidth: 280
							}),
							{
								x: 440,
								y: 90,
								xtype: 'button',
								text: 'Tindakan baru',
								iconCls: 'Edit_Tr',
								id: 'btnEdit_viSetupTindakan',
								handler: function(sm, row, rec)
								{
									LookupNewTindakan_SetupTindakan();
									
								}
							}
						]
					}
				]
			}
		]		
	};
        return items;
}
function LookupNewTindakan_SetupTindakan()
{
    setLookupNewTindakan_SetupTindakan = new Ext.Window({
        id: 'setLookupNewTindakan_SetupTindakan',
		name: 'setLookupNewTindakan_SetupTindakan',
        title: 'Input Tindakan Medis Baru', 
        closeAction: 'destroy',        
        width: 440,
        height: 150,
        resizable:false,
		autoScroll: false,
        border: true,
        constrain : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: [ 
				PanelTindakan()
			   ],
		tbar:[
			{
				xtype: 'button',
				text: 'Add New',
				iconCls: 'add',
				id: 'btnAddTindakan_viSetupTindakan',
				handler: function(){
					AddNewTindakan_SetupTindakan();
				}
			},
			{
				xtype: 'tbseparator'
			},
			{
				xtype: 'button',
				text: 'Save',
				iconCls: 'save',
				id: 'btnSimpanTindakan_viSetupTindakan',
				handler: function()
				{
					loadMask.show();
					dataSaveTindakan_viSetupTindakan();
				}
			},
			{
				xtype: 'tbseparator'
			},
			{
				xtype: 'button',
				text: 'Delete',
				iconCls: 'remove',
				id: 'btnDeleteTindakan_viSetupTindakan',
				handler: function()
				{
					Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
						if (button == 'yes'){
							loadMask.show();
							dataDeleteTindakan_viSetupTindakan();
						}
					});
				}
			},
			{
				xtype: 'tbseparator'
			},
		],
        listeners:
        {
            activate: function()
            {
				
            },
            afterShow: function()
            {
                this.activate();
				
            },
            deactivate: function()
            {
				
            }
        }
    }
    );

    setLookupNewTindakan_SetupTindakan.show();
}

function PanelTindakan(){
	var items = 
	{
		layout:'form',
		border: false,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: true,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						//width: 250,
						height: 80,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 10,
								xtype: 'label',
								text: 'Kode Tindakan'
							},
							{
								x: 100,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 110,
								y: 10,
								xtype: 'textfield',
								id: 'txtNewKdTindakan_SetupPerawat',
								width: 100,
								readOnly: true
							},
							{
								x: 10,
								y: 40,
								xtype: 'label',
								text: 'Tindakan'
							},
							{
								x: 100,
								y: 40,
								xtype: 'label',
								text: ':'
							},
							tmpnewtindakan=new Nci.form.Combobox.autoComplete({
								x: 110,
								y: 40,
								width	: 290,
								id		: 'txtComboNewTindakan_SetupPerawat',
								store	: ArrayStoreTindakan,
								select	: function(a,b,c){
									tmpnewtindakan=b.data.tindakan;
									Ext.getCmp('txtNewKdTindakan_SetupPerawat').setValue(b.data.kd_tindakan);
								},
								insert	: function(o){
									return {
										kd_tindakan     : o.kd_tindakan,
										tindakan 		: o.tindakan,
										text			:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_tindakan+'</td><td width="200">'+o.tindakan+'</td></tr></table>'
									}
								},
								url		: baseURL + "index.php/kamar_operasi/functionSetupTindakan/getTindakan",
								valueField: 'tindakan',
								displayField: 'text',
								listWidth: 280
							}),
						]
					}
				]
			}
		]		
	};
        return items;
}
//------------------end---------------------------------------------------------

function load_data_spesialis(param)
{

	Ext.Ajax.request(
	{
		url: baseURL + "index.php/kamar_operasi/functionSetupTindakan/getSpesialisasi",
		params:{
			command: param
		} ,
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
		success: function(o) {
			cboSpesialisasi_SetupTindakanasi.store.removeAll();
				var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsspesialisasi_SetupTindakanasi.recordType;
				var o=cst['listData'][i];
				
				recs.push(new recType(o));
				dsspesialisasi_SetupTindakanasi.add(recs);
				//console.log(o);
			}
		}
	});
}

function comboSpesialis()
{ 
 
	var Field = ['kd_spesial','spesialisasi'];

    dsspesialisasi_SetupTindakanasi = new WebApp.DataStore({ fields: Field });
	
	load_data_spesialis();
	cboSpesialisasi_SetupTindakanasi= new Ext.form.ComboBox
	(
		{
			x				: 130,
			y				: 0,
			id				: 'cboSpesialisasi_SetupTindakanasi',
			typeAhead		: true,
			triggerAction	: 'all',
			lazyRender		: true,
			mode			: 'local',
			emptyText		: '',
			fieldLabel		:  '',
			align			: 'Right',
			width			: 170,
			store			: dsspesialisasi_SetupTindakanasi,
			valueField		: 'kd_spesial',
			displayField	: 'spesialisasi',
			//hideTrigger		: true,
			listeners:
			{
				select	: function(a,b,c){
					Ext.getCmp('cboSubSpesialisasi_SetupTindakanasi').setValue("");
					Ext.getCmp('cboJenisOperasi_SetupTindakanasi').setValue("");
					load_data_sub_spesialis(b.data.kd_spesial);	
				},
				keyUp: function(a,b,c){
					$this1=this;
					if((b.getKey()>=65 && b.getKey()<=90)|| (b.getKey()>=48 && b.getKey()<=57) || b.getKey()==32 || b.getKey()==8 || b.getKey()==46){
						clearTimeout(this.time);
					
						this.time=setTimeout(function(){
							if($this1.lastQuery != '' ){
								var value=$this1.lastQuery;
								var param={};
								param['text']=$this1.lastQuery;
								load_data_spesialis($this1.lastQuery);

								
							}
						},1000);
					}
				},
				
			}
		}
	);return cboSpesialisasi_SetupTindakanasi;
};

function load_data_sub_spesialis(kd_spesial)
{

	Ext.Ajax.request(
	{
		url: baseURL + "index.php/kamar_operasi/functionSetupTindakan/getSubSpesialisasi",
		params:{
			kd_spesial: kd_spesial
		} ,
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
		success: function(o) {
			cboSubSpesialisasi_SetupTindakanasi.store.removeAll();
				var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dssubspesialisasi_SetupTindakanasi.recordType;
				var o=cst['listData'][i];
				
				recs.push(new recType(o));
				dssubspesialisasi_SetupTindakanasi.add(recs);
				//console.log(o);
			}
		}
	});
}

function comboSubSpesialis()
{ 
 
	var Field = ['kd_sub_spc','sub_spesialisasi','kd_spesial'];

    dssubspesialisasi_SetupTindakanasi = new WebApp.DataStore({ fields: Field });
	
	load_data_sub_spesialis();
	cboSubSpesialisasi_SetupTindakanasi= new Ext.form.ComboBox
	(
		{
			x				: 130,
			y				: 30,
			id				: 'cboSubSpesialisasi_SetupTindakanasi',
			typeAhead		: true,
			triggerAction	: 'all',
			lazyRender		: true,
			mode			: 'local',
			emptyText		: '',
			fieldLabel		:  '',
			align			: 'Right',
			width			: 170,
			store			: dssubspesialisasi_SetupTindakanasi,
			valueField		: 'kd_sub_spc',
			displayField	: 'sub_spesialisasi',
			//hideTrigger		: true,
			listeners:
			{
				select	: function(a,b,c){
					Ext.getCmp('cboJenisOperasi_SetupTindakanasi').setValue("");
				},
				keyUp: function(a,b,c){
					$this1=this;
					if((b.getKey()>=65 && b.getKey()<=90)|| (b.getKey()>=48 && b.getKey()<=57) || b.getKey()==32 || b.getKey()==8 || b.getKey()==46){
						clearTimeout(this.time);
					
						this.time=setTimeout(function(){
							if($this1.lastQuery != '' ){
								var value=$this1.lastQuery;
								var param={};
								param['text']=$this1.lastQuery;
								load_data_sub_spesialis($this1.lastQuery);

								
							}
						},1000);
					}
				},
				
			}
		}
	);return cboSubSpesialisasi_SetupTindakanasi;
};

function load_data_jenis_operasi()
{

	Ext.Ajax.request(
	{
		url: baseURL + "index.php/kamar_operasi/functionSetupTindakan/getJenisOperasi",
		params:{
			command: 0
		} ,
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
		success: function(o) {
			cboJenisOperasi_SetupTindakanasi.store.removeAll();
				var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsjenisoperasi_SetupTindakanasi.recordType;
				var o=cst['listData'][i];
				
				recs.push(new recType(o));
				dsjenisoperasi_SetupTindakanasi.add(recs);
				//console.log(o);
			}
		}
	});
}

function comboJenisOperasi()
{ 
 
	var Field = ['kd_jenis_op','jenis_op'];

    dsjenisoperasi_SetupTindakanasi = new WebApp.DataStore({ fields: Field });
	
	load_data_jenis_operasi();
	cboJenisOperasi_SetupTindakanasi= new Ext.form.ComboBox
	(
		{
			x				: 130,
			y				: 60,
			id				: 'cboJenisOperasi_SetupTindakanasi',
			typeAhead		: true,
			triggerAction	: 'all',
			lazyRender		: true,
			mode			: 'local',
			emptyText		: '',
			fieldLabel		:  '',
			align			: 'Right',
			width			: 170,
			store			: dsjenisoperasi_SetupTindakanasi,
			valueField		: 'kd_jenis_op',
			displayField	: 'jenis_op',
			//hideTrigger		: true,
			listeners:
			{
				select	: function(a,b,c){
					dataGriSetupTindakan(Ext.getCmp('cboJenisOperasi_SetupTindakanasi').getValue(),Ext.getCmp('cboSubSpesialisasi_SetupTindakanasi').getValue());
				},
				keyUp: function(a,b,c){
					$this1=this;
					if((b.getKey()>=65 && b.getKey()<=90)|| (b.getKey()>=48 && b.getKey()<=57) || b.getKey()==32 || b.getKey()==8 || b.getKey()==46){
						clearTimeout(this.time);
					
						this.time=setTimeout(function(){
							if($this1.lastQuery != '' ){
								var value=$this1.lastQuery;
								var param={};
								param['text']=$this1.lastQuery;
								load_data_jenis_operasi($this1.lastQuery);

								
							}
						},1000);
					}
				},
				
			}
		}
	);return cboJenisOperasi_SetupTindakanasi;
};

function dataGriSetupTindakan(kd_jenis_op,kd_sub_spc){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/kamar_operasi/functionSetupTindakan/getItemGrid",
			params: {
				kd_jenis_op:kd_jenis_op, 
				kd_sub_spc:kd_sub_spc
			},
			failure: function(o)
			{
				ShowPesanErrorSetupTindakan('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dataSource_viSetupTindakan.removeAll();
					var recs=[],
						recType=dataSource_viSetupTindakan.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dataSource_viSetupTindakan.add(recs);
					
					
					
					GridDataView_viSetupTindakan.getView().refresh();
				}
				else 
				{
					ShowPesanErrorSetupTindakan('Gagal membaca data items', 'Error');
				};
			}
		}
		
	)
	
}

function dataSave_viSetupTindakan(){
	if (ValidasiSaveSetupTindakan(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/kamar_operasi/functionSetupTindakan/save",
				params: getParamSaveSetupTindakan(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupTindakan('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupTindakan('Berhasil menyimpan data ini','Information');
						dataSource_viSetupTindakan.removeAll();
						dataGriSetupTindakan(Ext.getCmp('cboJenisOperasi_SetupTindakanasi').getValue(),Ext.getCmp('cboSubSpesialisasi_SetupTindakanasi').getValue());
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupTindakan('Gagal menyimpan data ini', 'Error');
						dataSource_viSetupTindakan.removeAll();
						dataGriSetupTindakan(Ext.getCmp('cboJenisOperasi_SetupTindakanasi').getValue(),Ext.getCmp('cboSubSpesialisasi_SetupTindakanasi').getValue());
					};
				}
			}
			
		)
	}
}

function dataSaveTindakan_viSetupTindakan(){
	if (ValidasiSaveTindakanSetupTindakan(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/kamar_operasi/functionSetupTindakan/savetindakan",
				params: getParamSaveTindakanSetupTindakan(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupTindakan('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupTindakan('Berhasil menyimpan data ini','Information');
						Ext.getCmp('txtNewKdTindakan_SetupPerawat').setValue(cst.kode);
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupTindakan('Gagal menyimpan data ini', 'Error');
					};
				}
			}
			
		)
	}
}

function dataDelete_viSetupTindakan(){
	if (ValidasiSaveSetupTindakan(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/kamar_operasi/functionSetupTindakan/delete",
				params: getParamDeleteSetupTindakan(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupTindakan('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupTindakan('Berhasil menghapus data ini','Information');
						dataSource_viSetupTindakan.removeAll();
						dataGriSetupTindakan(Ext.getCmp('cboJenisOperasi_SetupTindakanasi').getValue(),Ext.getCmp('cboSubSpesialisasi_SetupTindakanasi').getValue());
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupTindakan('Gagal menghapus data ini', 'Error');
						dataSource_viSetupTindakan.removeAll();
						dataGriSetupTindakan(Ext.getCmp('cboJenisOperasi_SetupTindakanasi').getValue(),Ext.getCmp('cboSubSpesialisasi_SetupTindakanasi').getValue());
					};
				}
			}
			
		)
	}
}

function dataDeleteTindakan_viSetupTindakan(){
	if (ValidasiDeleteTindakanSetupTindakan(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/kamar_operasi/functionSetupTindakan/deletetindakan",
				params: getParamDeleteTindakanSetupTindakan(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupTindakan('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupTindakan('Berhasil menghapus data ini','Information');
						AddNewTindakan_SetupTindakan();
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupTindakan('Gagal menghapus data ini', 'Error');
					};
				}
			}
			
		)
	}
}

function AddNewSetupTindakan(){
	Ext.getCmp('cboSubSpesialisasi_SetupTindakanasi').setValue("");
	Ext.getCmp('cboJenisOperasi_SetupTindakanasi').setValue(""),
	tmptindakan.getValue().setValue(""),
	Ext.getCmp('cboSpesialisasi_SetupTindakanasi').setValue(""),
	dataSource_viSetupTindakan.removeAll();
	tmpkdtindakan="";
};

function AddNewTindakan_SetupTindakan(){
	Ext.getCmp('txtNewKdTindakan_SetupPerawat').setValue("");
	tmpnewtindakan.setValue("");
};

function DataInitSetupTindakan(rowdata){
	Ext.getCmp('cboSpesialisasi_SetupTindakanasi').setValue(rowdata.kd_spesial),
	Ext.getCmp('cboSubSpesialisasi_SetupTindakanasi').setValue(rowdata.kd_sub_spc);
	Ext.getCmp('cboJenisOperasi_SetupTindakanasi').setValue(rowdata.kd_jenis_op);
	tmptindakan.setValue(rowdata.tindakan);
	tmpkdtindakan=rowdata.kd_tindakan;
	console.log(tmpkdtindakan)
};

function getParamSaveSetupTindakan(){
	var	params =
	{
		kd_jenis_op:Ext.getCmp('cboJenisOperasi_SetupTindakanasi').getValue(),
		kd_tindakan:tmpkdtindakan,
		kd_sub_spc:Ext.getCmp('cboSubSpesialisasi_SetupTindakanasi').getValue()
	}
   
    return params
};

function getParamSaveTindakanSetupTindakan(){
	var	params =
	{
		kd_tindakan:Ext.getCmp('txtNewKdTindakan_SetupPerawat').getValue(),
		tindakan:tmpnewtindakan.getValue()
	}
   
    return params
};

function getParamDeleteSetupTindakan(){
	var	params =
	{
		kd_jenis_op:Ext.getCmp('cboJenisOperasi_SetupTindakanasi').getValue(),
		kd_tindakan:tmpkdtindakan,
		kd_sub_spc:Ext.getCmp('cboSubSpesialisasi_SetupTindakanasi').getValue()
	}
   
    return params
};

function getParamDeleteTindakanSetupTindakan(){
	var	params =
	{
		kd_tindakan:Ext.getCmp('txtNewKdTindakan_SetupPerawat').getValue(),
	}
   
    return params
};


function ValidasiSaveSetupTindakan(modul,mBolHapus){
	var x = 1;
	if(Ext.getCmp('cboJenisOperasi_SetupTindakanasi').getValue() === '' || tmptindakan.getValue() === '' || Ext.getCmp('cboSubSpesialisasi_SetupTindakanasi').getValue() === ''){
		if(Ext.getCmp('cboJenisOperasi_SetupTindakanasi').getValue() === ''){
			loadMask.hide();
			ShowPesanWarningSetupTindakan('Jenis Operasi tidak boleh kosong', 'Warning');
			x = 0;
		} else if(tmptindakan.getValue() === ''){
			loadMask.hide();
			ShowPesanWarningSetupTindakan('Tindakan tidak boleh kosong', 'Warning');
			x = 0;
		} else{
			loadMask.hide();
			ShowPesanWarningSetupTindakan('Jenis Operasi tidak boleh kosong', 'Warning');
			x = 0;
		}
	}
	
	return x;
};

function ValidasiSaveTindakanSetupTindakan(modul,mBolHapus){
	var x = 1;
	if(tmpnewtindakan.getValue() === ''){
			loadMask.hide();
			ShowPesanWarningSetupTindakan('Tindakan tidak boleh kosong', 'Warning');
			x = 0;
		} 
	
	return x;
};

function ValidasiDeleteTindakanSetupTindakan(modul,mBolHapus){
	var x = 1;
	if(Ext.getCmp('txtNewKdTindakan_SetupPerawat').getValue() === ''){
			loadMask.hide();
			ShowPesanWarningSetupTindakan('Pilih tindakan yang akan di hapus', 'Warning');
			x = 0;
		} 
	
	return x;
};



function ShowPesanWarningSetupTindakan(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorSetupTindakan(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoSetupTindakan(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};