
var CurrentHistory =
{
    data: Object,
    details: Array,
    row: 0
};
var FormDepan2KasirLABRAD;
var CurrentPenataJasaKasirLABRAD =
{
    data: Object,
    details: Array,
    row: 0
};
var tampungshiftsekarang;
var tampungtypedata;
var tanggaltransaksitampung;
var kdpaytransfer='T1';
var mRecordKasirLABRAD = Ext.data.Record.create
(
    [
       {name: 'KD_PRODUK', mapping:'KD_PRODUK'},
       {name: 'DESKRIPSI2', mapping:'DESKRIPSI2'},
       {name: 'DESKRIPSI', mapping:'DESKRIPSI'},
       {name: 'KD_TARIF', mapping:'KD_TARIF'},
       {name: 'HARGA', mapping:'HARGA'},
       {name: 'QTY', mapping:'QTY'},
       {name: 'TGL_TRANSAKSI', mapping:'TGL_TRANSAKSI'},
       {name: 'DESC_REQ', mapping:'DESC_REQ'},
       {name: 'URUT', mapping:'URUT'}
    ]
);
var selectindukunitLABRADKasir='Radiologi';
var cellSelecteddeskripsi;
var cellSelected2deskripsi;
var Kdtransaksi;
var tapungkd_pay;
var dsTRDetailHistoryList;
var AddNewHistory = true;
var selectCountHistory = 50;
var now = new Date();
var rowSelectedHistory;

var FormLookUpsdetailTRHistory;
var now = new Date();
var cellSelectedtutup;
var vkd_unit;
var kdcustomeraa;
var nowTglTransaksi = new Date();

var labelisi;
var jenispay;
var variablehistori;
var selectCountStatusByr_viKasirLABRADKasir='Belum Lunas';
var tranfertujuan='Rawat Inap';
var dsTRKasirLABRADKasirList;
var dsTRDetailKasirLABRADKasirList;
var AddNewKasirLABRADKasir = true;
var selectCountKasirLABRADKasir = 50;
var now = new Date();
var rowSelectedKasirLABRADKasir;

var FormLookUpsdetailTRKasirLABRAD;
var valueStatusCMKasirLABRADView='All';
var nowTglTransaksi = new Date();
var dsComboBayar;
var vkode_customer;
var vflag;
 var gridDTLTRKasirLABRAD;
var notransaksi ;
var tgltrans;
var kodepasien;
var namapasien;
var kodeunit;
var namaunit;
var kodepay;
var kdkasir;
var uraianpay;
CurrentPage.page = getPanelKasirLABRAD(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelKasirLABRAD(mod_id) 
{

    var Field = ['KD_DOKTER','NO_TRANSAKSI','KD_UNIT','KD_PASIEN','NAMA','NAMA_UNIT','ALAMAT',
	'TANGGAL_TRANSAKSI','NAMA_DOKTER','KD_CUSTOMER','CUSTOMER','URUT_MASUK','LUNAS',
	'KET_PAYMENT','CARA_BAYAR','JENIS_PAY','KD_PAY','POSTING','TYPE_DATA','CO_STATUS','KD_KASIR',
	'NAMAUNITASAL','NO_TRANSAKSI_ASAL','KD_KASIR_ASAL','FLAG'];
    dsTRKasirLABRADKasirList = new WebApp.DataStore({ fields: Field });
    refeshKasirLABRADKasir();
    var grListTRKasirLABRAD = new Ext.grid.EditorGridPanel
    (
        {
            stripeRows: true,
            store: dsTRKasirLABRADKasirList,
          
            columnLines: false,
            autoScroll:true,
			   anchor: '100% 45%',
			//height:325,
            border: false,
			sort :false,
            sm: new Ext.grid.RowSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        rowselect: function(sm, row, rec)
                        {
							 rowSelectedKasirLABRADKasir = dsTRKasirLABRADKasirList.getAt(row);
							
							
                        }
                    }
                }
            ),
            listeners:
            {
                rowdblclick: function (sm, ridx, cidx)
                {
			           
                    rowSelectedKasirLABRADKasir = dsTRKasirLABRADKasirList.getAt(ridx);
				
                    if (rowSelectedKasirLABRADKasir != undefined)
                    { 	if ( rowSelectedKasirLABRADKasir.data.LUNAS=='f' )
							   {
									KasirLookUpLABRAD(rowSelectedKasirLABRADKasir.data);
									
							   }else
							   {
							   ShowPesanWarningKasirLABRAD('Pembayaran telah balance','Pembayaran');
							   }
                    }else{
					ShowPesanWarningKasirLABRAD('Silahkan Pilih data   ','Pembayaran');
					      }
                   
                },
				rowclick: function (sm, ridx, cidx)
                {
						cellSelectedtutup=rowSelectedKasirLABRADKasir.data.NO_TRANSAKSI;
				
						if ( rowSelectedKasirLABRADKasir.data.LUNAS=='t' && rowSelectedKasirLABRADKasir.data.CO_STATUS =='t')
						   {
						   
						     Ext.getCmp('btnEditKasirLABRAD').disable();
						     Ext.getCmp('btnTutupTransaksiKasirLABRAD').disable();
						   	 Ext.getCmp('btnHpsBrsKasirLABRAD').disable();
							 Ext.getCmp('btnLookupKasirLABRAD').disable();
						     Ext.getCmp('btnSimpanLABRAD').disable();
						   	 Ext.getCmp('btnHpsBrsLABRAD').disable();
							   	 Ext.getCmp('btnTransferKasirLABRAD').disable();
							 
						   }
						   else if ( rowSelectedKasirLABRADKasir.data.LUNAS=='f' && rowSelectedKasirLABRADKasir.data.CO_STATUS =='f')
						   {
						        Ext.getCmp('btnTutupTransaksiKasirLABRAD').disable();
						   	    Ext.getCmp('btnHpsBrsKasirLABRAD').enable();
								Ext.getCmp('btnEditKasirLABRAD').enable();
								Ext.getCmp('btnLookupKasirLABRAD').enable();
								Ext.getCmp('btnSimpanLABRAD').enable();
								Ext.getCmp('btnHpsBrsLABRAD').enable();
								Ext.getCmp('btnTransferKasirLABRAD').enable();
						       
							}
							   else if ( rowSelectedKasirLABRADKasir.data.LUNAS=='t' && rowSelectedKasirLABRADKasir.data.CO_STATUS =='f')
						   {
						        Ext.getCmp('btnEditKasirLABRAD').disable();
						        Ext.getCmp('btnTutupTransaksiKasirLABRAD').enable();
						   	    Ext.getCmp('btnHpsBrsKasirLABRAD').enable();
								Ext.getCmp('btnLookupKasirLABRAD').disable();
								Ext.getCmp('btnSimpanLABRAD').disable();
								Ext.getCmp('btnHpsBrsLABRAD').disable();
								Ext.getCmp('btnTransferKasirLABRAD').disable();
						       
							}
				         
							kdkasirasal=rowSelectedKasirLABRADKasir.data.KD_KASIR_ASAL;
							notransaksiasal=rowSelectedKasirLABRADKasir.data.NO_TRANSAKSI_ASAL;
							notransaksi= rowSelectedKasirLABRADKasir.data.NO_TRANSAKSI ;
                                                        tgltrans =rowSelectedKasirLABRADKasir.data.TANGGAL_TRANSAKSI;
							kodepasien=rowSelectedKasirLABRADKasir.data.KD_PASIEN;
							namapasien=rowSelectedKasirLABRADKasir.data.NAMA;
							kodeunit =rowSelectedKasirLABRADKasir.data.KD_UNIT;
							namaunit =rowSelectedKasirLABRADKasir.data.NAMA_UNIT;
							kodepay=rowSelectedKasirLABRADKasir.data.KD_PAY;
							kdkasir=rowSelectedKasirLABRADKasir.data.KD_KASIR;
							uraianpay=rowSelectedKasirLABRADKasir.data.CARA_BAYAR;
							kdcustomeraa=rowSelectedKasirLABRADKasir.data.KD_CUSTOMER;
							Ext.Ajax.request(
								{
									//url: "./home.mvc/getModule",
									//url: baseURL + "index.php/main/getTrustee",
									url: baseURL + "index.php/main/getcurrentshift",
									 params: {
										//UserID: 'Admin',
										command: '0',
										// parameter untuk url yang dituju (fungsi didalam controller)
									},
									failure: function(o)
									{
										 var cst = Ext.decode(o.responseText);
										
									},	    
									success: function(o) {
										
									
										//var cst = Ext.decode(o.responseText);
										tampungshiftsekarang=o.responseText
										//Ext.get('txtNilaiShift').dom.value =tampungshiftsekarang ;



									}
								
								});
							
							//alert(notransaksi)
                          RefreshDatahistoribayar(rowSelectedKasirLABRADKasir.data.NO_TRANSAKSI);
				   		RefreshDataKasirLABRADDetail(rowSelectedKasirLABRADKasir.data.NO_TRANSAKSI);
         
                } 
                   
				
            },
        cm: new Ext.grid.ColumnModel
            (
                [
                   //CUSTOMER
					 {
                        id: 'colLUNAScoba',
                        header: 'Status Lunas',
                        dataIndex: 'LUNAS',
                        sortable: true,
                        width: 90,
                        align:'center',
                        renderer: function(value, metaData, record, rowIndex, colIndex, store)
                        {
                             switch (value)
                             {
                                 case 't':
                                         metaData.css = 'StatusHijau'; // 
                                         break;
                                 case 'f':
                                        metaData.css = 'StatusMerah'; // rejected

                                         break;
                             }
                             return '';
                        }
                    },
					 {
                        id: 'coltutuptr',
                        header: 'Tutup transaksi',
                        dataIndex: 'CO_STATUS',
                        sortable: true,
                        width: 90,
                        align:'center',
                       renderer: function(value, metaData, record, rowIndex, colIndex, store)
                        {
                             switch (value)
                             {
                                 case 't':
                                         metaData.css = 'StatusHijau'; // 
                                         break;
                                 case 'f':
                                        metaData.css = 'StatusMerah'; // rejected

                                         break;
                             }
                             return '';
                        }
                    },
                    {
                        id: 'colReqIdViewKasirLABRAD',
                        header: 'No. Transaksi',
                        dataIndex: 'NO_TRANSAKSI',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 80
                    },
                    {
                        id: 'colTglKasirLABRADViewKasirLABRAD',
                        header: 'Tgl Transaksi',
                        dataIndex: 'TANGGAL_TRANSAKSI',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 75,
                            renderer: function(v, params, record)
                            {
                                    return ShowDate(record.data.TANGGAL_TRANSAKSI);

                        }
                    },
					{
                        header: 'No. Medrec',
                        width: 65,
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        dataIndex: 'KD_PASIEN',
                        id: 'colKasirLABRADerViewKasirLABRAD'
                    },
					{
                        header: 'Pasien',
                        width: 190,
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        dataIndex: 'NAMA',
                        id: 'colKasirLABRADerViewKasirLABRAD'
                    },
                    {
                        id: 'colLocationViewKasirLABRAD',
                        header: 'Alamat',
                        dataIndex: 'ALAMAT',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 170
                    },
                    
                    {
                        id: 'colDeptViewKasirLABRAD',
                        header: 'Dokter',
                        dataIndex: 'NAMA_DOKTER',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 150
                    },
                    {
                        id: 'colunitViewKasirLABRAD',
                        header: 'Unit',
                        dataIndex: 'NAMA_UNIT',
						sortable: false,
						hideable:false,
						menuDisabled:true,
						hidden:true,
                        width: 90
                    },
					  {
                        id: 'colasaluViewKasirLABRAD',
                        header: 'Unit asal',
                        dataIndex: 'NAMAUNITASAL',
						sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 90
                    },
					     {
                        id: 'colcustomerViewKasirLABRAD',
                        header: 'Kel. Pasien',
                        dataIndex: 'CUSTOMER',
						sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 90
                    }
					
                   
                ]
            )	,	viewConfig: {forceFit: true},
            tbar:
                [
                    {
                        id: 'btnEditKasirLABRAD',
                        text: 'Pembayaran',
                        tooltip: nmEditData,
                        iconCls: 'Edit_Tr',
                        handler: function(sm, row, rec)
                        {
                            if (rowSelectedKasirLABRADKasir != undefined)
                            {
                                    KasirLookUpLABRAD(rowSelectedKasirLABRADKasir.data);
                            }
                            else
                            {
							ShowPesanWarningKasirLABRAD('Pilih data tabel  ','Pembayaran');
                                    //alert('');
                            }
                        }, disabled :true
                    }, ' ', ' ','' + ' ','' + ' ','' + ' ','' + ' ', ' ','' + '-',
					
					{
                        id: 'btnTransferKasirLABRAD',
                        text: 'Transfer',
                        tooltip: nmEditData,
                        iconCls: 'Edit_Tr',
						disabled :true,
                        handler: function(sm, row, rec)
                        {
			
						TransferLookUp();
						Ext.Ajax.request
                                                                ( {
                                                                           url: baseURL + "index.php/main/GetPasienTranferLab_rad",
                                                                           params: {
                                                                           notr: notransaksiasal,
																		   kdkasir:kdkasirasal
                                                                           },
                                                                           success: function(o)
                                                                           {
																			if   (o.responseText==""){
																			ShowPesanWarningKasirLABRAD('Pasien Belum terdaftar dirawat Inap ','Transfer');}
																			else{ var tmphasil = o.responseText;
																			   var tmp = tmphasil.split("<>");
																			   Ext.get(txtTranfernoTransaksiLABRAD).dom.value=tmp[3];
																			   Ext.get(dtpTanggaltransaksiRujuanLABRAD).dom.value=ShowDate(tmp[4]);
																			   Ext.get(txtTranfernomedrecLABRAD).dom.value=tmp[5];
																			   Ext.get(txtTranfernamapasienLABRAD).dom.value=tmp[2];
																			   Ext.get(txtTranferunitLABRAD).dom.value=tmp[1];
																				Trkdunit2=tmp[6];

																			   var kasir=tmp[7];
																		      Ext.Ajax.request
																								( {
																							   url: baseURL + "index.php/main/GettotalTranfer",
																							   params: {
																							   notr: notransaksi
																							   },
																							   success: function(o)
																							   { 
																								Ext.get(txtjumlahbiayasal).dom.value=formatCurrency(o.responseText);
																								Ext.get(txtjumlahtranfer).dom.value=formatCurrency(o.responseText);
																								
																							   }
																							   });
																							   
																		   }
																							
																		   }
                                                                           
                                                                           });
                      
                        }, //disabled :true
                    }, ' ', ' ','' + ' ','' + ' ','' + ' ','' + ' ', ' ','' + '-',
					{
								text: 'Tutup Transaksi',
								id: 'btnTutupTransaksiKasirLABRAD',
								tooltip: nmHapus,
								iconCls: 'remove',
								handler: function()
									{
									if(cellSelectedtutup=='' || cellSelectedtutup=='undefined')
									{
									ShowPesanWarningKasirLABRAD ('Pilih data tabel  ','Pembayaran');
									}
									 else
										{	
									
									   UpdateTutuptransaksi(false);	
								        Ext.getCmp('btnEditKasirLABRAD').disable();
										Ext.getCmp('btnTutupTransaksiKasirLABRAD').disable();
						   	            Ext.getCmp('btnHpsBrsKasirLABRAD').disable();
										}										
								    },
									disabled:true
					}
                ]
            }
	);
	
	var LegendViewCMRequest = new Ext.Panel
	(
            {
            id: 'LegendViewCMRequest',
            region: 'center',
            border:false,
            bodyStyle: 'padding:0px 7px 0px 7px',
            layout: 'column',
            frame:true,
            //height:32,
            anchor: '100% 8.0001%',
            autoScroll:false,
            items:
            [
                {
                    columnWidth: .033,
                    layout: 'form',
                    style:{'margin-top':'-1px'},
                    //height:32,
                    anchor: '100% 8.0001%',
                    border: false,
                    html: '<img src="'+baseURL+'ui/images/icons/16x16/hijau.png" class="text-desc-legend"/>'
                },
                {
                    columnWidth: .08,
                    layout: 'form',
                    //height:32,
                    anchor: '100% 8.0001%',
                    style:{'margin-top':'1px'},
                    border: false,
                    html: " Lunas"
                },
                {
                    columnWidth: .033,
                    layout: 'form',
                    style:{'margin-top':'-1px'},
                    border: false,
                    //height:35,
                    anchor: '100% 8.0001%',
                    //html: '<img src="./images/icons/16x16/merah.png" class="text-desc-legend"/>'
                    html: '<img src="'+baseURL+'ui/images/icons/16x16/merah.png" class="text-desc-legend"/>'
                },
                {
                    columnWidth: .1,
                    layout: 'form',
                    //height:32,
                    anchor: '100% 8.0001%',
                    style:{'margin-top':'1px'},
                    border: false,
                    html: " Belum Lunas"
                }
            ]

        }
    )
	 var GDtabDetailLABRAD = new Ext.TabPanel   
    (
        {
        id:'GDtabDetailLABRAD',
        region: 'center',
        activeTab: 0,
	    anchor: '100% 40%',
        border:false,
        plain: true,
        defaults:
                {
                autoScroll: true
				},
                items: [
					GetDTLTRHistoryGrid(),GetDTLTRLABRADGrid() 
		                //-------------- ## --------------
					],
				listeners:
					{
				  
					}
		}
		
    );

    var FormDepanKasirLABRAD = new Ext.Panel
    (
        {
            id: mod_id,
            closable: true,
            region: 'center',
            layout: 'form',
            title: 'Kasir Lab & Rad',
            border: false,
            shadhow: true,
            iconCls: 'Request',
            margins: '0 5 5 0',
            items: [
			{
				xtype:'panel',
				layout: 'column',
				plain:true,
				activeTab: 0,
				height:160,
				defaults:
				{
				bodyStyle:'padding:10px',
				autoScroll: true
				},
				items:[
					   {
						columnWidth:.99,
						height:150,
						layout: 'form',
						bodyStyle: 'padding:6px 6px 3px 3px',
							border: true,
						anchor: '100% 100%',
						items:
						[mComboUnitInduk(),
							{
									xtype: 'textfield',
									fieldLabel: ' No. Medrec' + ' ',
									id: 'txtFilterKasirNomedrec',
									anchor : '80%',
									onInit: function() { },
									listeners:
									{
										'specialkey' : function()
										{
											var tmpNoMedrec = Ext.get('txtFilterKasirNomedrec').getValue()
											if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10 )
											{
												if(tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10 )
													{
													   
														 var tmpgetNoMedrec = formatnomedrec(Ext.get('txtFilterKasirNomedrec').getValue())
														 Ext.getCmp('txtFilterKasirNomedrec').setValue(tmpgetNoMedrec);
														var tmpkriteria = getCriteriaFilter_viDaftar();
														 RefreshDataFilterKasirLABRADKasir();
												  
													}
													else
														{
															if (tmpNoMedrec.length === 10)
																{
																 RefreshDataFilterKasirLABRADKasir();
																}
																else
																Ext.getCmp('txtFilterKasirNomedrec').setValue('')
														}
											}
										}

									}
							},
				
							{	 
							xtype: 'tbspacer',
							height: 3
							},	
							{
									xtype: 'textfield',
									fieldLabel: ' Pasien' + ' ',
									id: 'TxtFilterGridDataView_NAMA_viKasirLABRADKasir',
									anchor :'80%',
									 enableKeyEvents: true,
									listeners:
											{ 
										
												'keyup' : function()
												{

												  RefreshDataFilterKasirLABRADKasir();
							
												}
											}
							},
							{	 
							xtype: 'tbspacer',
							height: 3
							},
							getItemPanelcombofilter(),
							getItemPaneltgl_filter()
						]},
						
						]}
			,
			grListTRKasirLABRAD,GDtabDetailLABRAD,LegendViewCMRequest],
			//GetDTLTRHistoryGrid()
            tbar:
            [
            
				
            ],
            listeners:
            {
                'afterrender': function()
                {
                    loaddatastoreunitRadLab();
                    
                  /*  Ext.Ajax.request
                    (
                        {
                            url: baseURL + "index.php/main/CreateDataObj",
                            params: dataparam_viradlab(),
                            success: function(o)
                            {
                                var cst = Ext.decode(o.responseText);
                                if (cst.success === true)
                                {
                                    Ext.getCmp('cboUnitInduk').setValue(cst.namaunit);
                                }
                            }
                        }
                    )*/
       
                }
            }
        }
    );

   return FormDepanKasirLABRAD;
  
};

function dataparam_viradlab()
{
    var paramsload_viradlab =
    {
        Table: 'ViewComboUnitRadLab'
    }
    return paramsload_viradlab;
}

var dsunitinduk_viKasirLABRADKasir;

function loaddatastoreunitRadLab()
{
    dsunitinduk_viKasirLABRADKasir.load
    (
        {
        params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'kd_unit',
                Sortdir: 'ASC',
                target: 'ComboUnitRadLab',
                param: ""
            }
        }
    );
}

function TransferLookUp(rowdata) 
{
    var lebar = 440;
    FormLookUpsdetailTRTransfer = new Ext.Window
    (
        {
            id: 'gridTransfer',
            title: 'Transfer',
            closeAction: 'destroy',
            width: lebar,
            height: 410,
            border: false,
            resizable: false,
            plain: false,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryTRTransfer(lebar),
            listeners:
            {
                
            }
        }
    );

    FormLookUpsdetailTRTransfer.show();
  //  Transferbaru();

};

function getFormEntryTRTransfer(lebar) 
{
    var pnlTRTransfer = new Ext.FormPanel
    (
        {
            id: 'PanelTRTransfer',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:425,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [getItemPanelInputTransfer(lebar),getItemPanelButtonTransfer(lebar)],
           tbar:
            [
               
               
            ]
        }
    );
 
    var FormDepanTransfer = new Ext.Panel
	(
		{
		    id: 'FormDepanTransfer',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: 
			[
			pnlTRTransfer	
				
			]

		}
	);

    return FormDepanTransfer
};

function getItemPanelInputTransfer(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 0px',
		border:false,
		height:330,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
				
			    layout: 'form',
				height:330,
			    border: false,
			    items:
				[
					getTransfertujuan(lebar),
					    {	 
															xtype: 'tbspacer',
															height: 5
						},	
						getItemPanelNoTransksiTransfer(lebar),
						{	 
															xtype: 'tbspacer',									
															height:3
						},
						mComboalasan_transfer()
						
						
					
				]
			},
		]
	};
    return items;
};

function mComboalasan_transfer() 
{
	var Field = ['KD_ALASAN','ALASAN'];

    var dsalasan_transfer = new WebApp.DataStore({ fields: Field });
    dsalasan_transfer.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
				Sort: 'kd_alasan',
			    Sortdir: 'ASC',
			    target: 'ComboAlasanTransfer',
                param: "" //+"~ )"
			}
		}
	);
	
    var cboalasan_transfer = new Ext.form.ComboBox
	(
		{
		    id: 'cboalasan_transfer',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel:  ' Alasan Transfer ',
		    align: 'Right',
			  width: 100,
		    anchor:'100%',
		    store: dsalasan_transfer,
		    valueField: 'ALASAN',
		    displayField: 'ALASAN',
		    listeners:
			{
			
			    'select': function(a, b, c) 
				{	
			    }

			}
		}
	);
	
    return cboalasan_transfer;
};

function getItemPanelNoTransksiTransfer(lebar) 
{
    var items =
	{
		Width:lebar,
		height:110,
	    layout: 'column',
	    border: true,
		
		
	    items:
		[
			{

			    columnWidth: .990,
			    layout: 'form',
				Width:lebar-10,
				labelWidth:130,
			    border: false,
			    items:
				[
					
					
				{	 
					xtype: 'tbspacer',
					height:3
				}, 

									{
                                        xtype: 'textfield',
                                        fieldLabel: 'Jumlah Biaya',
                                        maxLength: 200,
                                        name: 'txtjumlahbiayasal',
                                        id: 'txtjumlahbiayasal',
                                        width: 100,
											style:{'text-align':'right'},
										readOnly:true,
                                        anchor: '95%'
                                     },
									{
                                        xtype: 'textfield',
                                        fieldLabel: 'Paid',
                                        maxLength: 200,
                                        name: 'txtpaid',
                                        id: 'txtpaid',
									    readOnly:true,
										align: 'right',
										style:{'text-align':'right'},
                                        width: 100,
										value:0,
                                        anchor: '95%'
                                     }
									 ,
									  {
                                        xtype: 'textfield',
                                        fieldLabel: 'Jumlah dipindahkan',
                                        maxLength: 200,
                                        name: 'txtjumlahtranfer',
                                        id: 'txtjumlahtranfer',
										align: 'right',
										readOnly:true,
										style:{'text-align':'right'},
                                        width: 100,
                                        anchor: '95%'
                                     },
									   {
                                        xtype: 'textfield',
                                        fieldLabel: 'Saldo tagihan',
                                        maxLength: 200,
                                        name: 'txtsaldotagihan',
                                        id: 'txtsaldotagihan',
										style:{'text-align':'right'},
										readOnly:true,
										value:0,
                                        width: 100,
                                        anchor: '95%'
                                     }
						
					
					
				]
			}
			
		]
	}
    return items;
};

function getTransfertujuan(lebar) 
{
    var items =
	{
		Width:lebar-2,
		height:170,
	    layout: 'form',
	    border: true,
		labelWidth:130,
	    items:
		[
			
					{	 
					xtype: 'tbspacer',
					height: 2
					},
					mComboTransferTujuan(),
						
									{
                                        xtype: 'textfield',
                                        fieldLabel: 'No Transaksi',
                                        //maxLength: 200,
                                        name: 'txtTranfernoTransaksiLABRAD',
                                        id: 'txtTranfernoTransaksiLABRAD',
										labelWidth:130,
										readonly :true,
                                        width: 100,
                                        anchor: '95%'
                                     },	
					{
					    xtype: 'datefield',
					    fieldLabel: 'Tanggal ',
					    id: 'dtpTanggaltransaksiRujuanLABRAD',
					    name: 'dtpTanggaltransaksiRujuanLABRAD',
					    format: 'd/M/Y',
						readOnly : true,
					  //  value: now,
					    labelWidth:130,
                        width: 100,
                        anchor: '95%'
					},
					{
                                        xtype: 'textfield',
                                        fieldLabel: 'No Medrec',
                                        //maxLength: 200,
                                        name: 'txtTranfernomedrecLABRAD',
                                        id: 'txtTranfernomedrecLABRAD',
										labelWidth:130,
										readOnly : true,
                                        width: 100,
                                        anchor: '95%'
                                     },
									 {
                                        xtype: 'textfield',
                                        fieldLabel: 'Nama Pasien',
                                        //maxLength: 200,
                                        name: 'txtTranfernamapasienLABRAD',
                                        id: 'txtTranfernamapasienLABRAD',
										readOnly : true,
										labelWidth:130,
                                        width: 100,
                                        anchor: '95%'
                                     },{
                                        xtype: 'textfield',
                                        fieldLabel: 'Unit Perawatan',
                                        //maxLength: 200,
                                        name: 'txtTranferunitLABRAD',
                                        id: 'txtTranferunitLABRAD',
										readOnly : true,
										labelWidth:130,
                                        width: 100,
                                        anchor: '95%'
                                     }
		
					
					
				//]
			//}
			
		]
	}
    return items;
};
function getItemPanelButtonTransfer(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
		height:30,
		anchor:'100%',
		style:{'margin-top':'-1px'},
	    items:
		[
			{
				layout: 'hBox',
				width:400,
				border: false,
				bodyStyle: 'padding:5px 0px 5px 5px',
				defaults: { margins: '3 3 3 3' },
				anchor: '90%',
				layoutConfig: 
				{
					align: 'middle',
					pack:'end'
				},
				items:
				[
					{
						xtype:'button',
						text:'Simpan',
						width:70,
						style:{'margin-left':'0px','margin-top':'0px'},
						hideLabel:true,
						id: 'btnOkTransfer',
						handler:function()
						{
                                                    TransferData(false);
                                                }
					},
					{
							xtype:'button',
							text:'Tutup',
							width:70,
							hideLabel:true,
							id: 'btnCancelTransfer',
							handler:function() 
							{
								FormLookUpsdetailTRTransfer.close();
							}
					}
				]
			}
		]
	}
    return items;
};

function getParamTransferRwi() 
{


    var params =
	{
        KDkasirIGD:kdkasir,
		Kdcustomer:kdcustomeraa,
		TrKodeTranskasi: notransaksi,
		KdUnit: kodeunit,
		Kdpay: kdpaytransfer,
		Jumlahtotal: Ext.get(txtjumlahbiayasal).dom.value,
		Tglasal:  ShowDate(tgltrans),
		Shift: tampungshiftsekarang,
		TRKdTransTujuan:Ext.get(txtTranfernoTransaksiLABRAD).dom.value,
		KdpasienIGDtujuan: Ext.get(txtTranfernomedrecLABRAD).dom.value,
		TglTranasksitujuan : Ext.get(dtpTanggaltransaksiRujuanLABRAD).dom.value,
		KDunittujuan : Trkdunit2,
		KDalasan :Ext.get(cboalasan_transfer).dom.value,
		KasirRWI:kdkasirasal
		
		
	
	};
    return params
};
function TransferData(mBol) 
{	
	//if (ValidasiEntryCMRwi(nmHeaderSimpanData,false) == 1 )
	//{//ShowPesanWarningKasirrwi,ShowPesanInfoKasirrwi,notransaksi
			Ext.Ajax.request
			 (
				{
					//url: "./Datapool.mvc/CreateDataObj",
					url: baseURL + "index.php/main/functionKasirPenunjang/saveTransfer",
					params: getParamTransferRwi(),
					failure: function(o)
					{
					
					ShowPesanWarningKasirLABRAD('Transfer Tidak berhasil silahkan hubungi admin', 'Gagal');
					RefreshDataFilterKasirLABRADKasir();
					},	
					success: function(o) 
					{
						RefreshDataFilterKasirLABRADKasir();
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoKasirLABRAD('Transfer Berhasil','transfer ');
							//RefreshDataKasirRwi();
		
							
						}
						else 
						{
								ShowPesanWarningKasirLABRAD('Transfer Tidak berhasil silahkan hubungi admin', 'Gagal');
						};
					}
				}
			)
		
	//}
	//else
	//{
	//	if(mBol === true)
	//	{
		//	return false;
		//};
	//};//ShowPesanInfoKasirigd,ShowPesanWarningKasirigd
	
};


function RefreshDataKasirLABRADDetail(no_transaksi) 
{

    var strKriteriaLABRAD='';
    //strKriteriaLABRAD = 'where no_transaksi = ~' + no_transaksi + '~'
    strKriteriaLABRAD = "\"no_transaksi\" = ~" + no_transaksi + "~";
    //strKriteriaLABRAD = 'no_transaksi = ~0000004~';
   
    dsTRDetailKasirLABRADList.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'tgl_transaksi',
			    //Sort: 'tgl_transaksi',
			    Sortdir: 'ASC',
			    target: 'ViewDetailRWJGridBawah',
			    param: strKriteriaLABRAD
			}
		}
	);
    return dsTRDetailKasirLABRADList;
};


function GetDTLTRLABRADGrid() 
{
    var fldDetail = ['KD_PRODUK','DESKRIPSI','DESKRIPSI2','KD_TARIF','HARGA','QTY','DESC_REQ','TGL_BERLAKU','NO_TRANSAKSI','URUT','DESC_STATUS','TGL_TRANSAKSI'];
	
    dsTRDetailKasirLABRADList = new WebApp.DataStore({ fields: fldDetail })
   RefreshDataKasirLABRADDetail() ;
    var gridDTLTRLABRAD = new Ext.grid.EditorGridPanel
    (
        {
            title: 'Input Produk',
            stripeRows: true,
            store: dsTRDetailKasirLABRADList,
            border: true,
            columnLines: true,
            frame: false,
            anchor: '100%',
             autoScroll:true,
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                            cellSelected2deskripsi = dsTRDetailKasirLABRADList.getAt(row);
                            CurrentPenataJasaKasirLABRAD.row = row;
                            CurrentPenataJasaKasirLABRAD.data = cellSelected2deskripsi;
                           //FocusCtrlCMLABRAD='txtAset'; */
                        }
                    }
                }
            ),
            cm: TRRawatJalanColumModel(),
			tbar:
                [
                       
                      
					{
							text: 'Tambah Produk',
							id: 'btnLookupKasirLABRAD',
							tooltip: nmLookup,
							iconCls: 'find',
							 disabled :true,
							handler: function()
							{
						  
								var p = RecordBaruLABRAD();
									var str='';
									//if (Ext.get('txtKdUnitLABRAD').dom.value  != undefined && Ext.get('txtKdUnitLABRAD').dom.value  != '')
									//{
											//str = ' where kd_dokter =~' + Ext.get('txtKdDokter').dom.value  + '~';
											//str = "\"kd_dokter\" = ~" + Ext.get('txtKdDokter').dom.value  + "~";
											str =  kodeunit;
									//};
									FormLookupKasirLABRAD(str, dsTRDetailKasirLABRADList, p, true, '', true);
									//FormLookupKasirLABRAD(str, dsTRDetailKasirLABRADList, p, true, '', true);
							}
						},
                       
							 {
								text: 'Simpan',
								id: 'btnSimpanLABRAD',
								tooltip: nmSimpan,
								iconCls: 'save',
								 disabled :true,
								handler: function()
									{
							
                                                                        Datasave_KasirLABRAD(false);
                                                                        Ext.getCmp('btnLookupKasirLABRAD').disable();
                                                                        Ext.getCmp('btnSimpanLABRAD').disable();
                                                                        Ext.getCmp('btnHpsBrsLABRAD').disable();
								   
								    }
							},  
							{
                                id:'btnHpsBrsLABRAD',
                                text: 'Hapus Baris',
                                tooltip: 'Hapus Baris',
								 disabled :true,
                                iconCls: 'RemoveRow',
                                handler: function()
                                {
                                        if (dsTRDetailKasirLABRADList.getCount() > 0 )
                                        {
                                                if (cellSelected2deskripsi != undefined)
                                                {
                                                        if(CurrentPenataJasaKasirLABRAD != undefined)
                                                        {
                                                                HapusBarisLABRAD();
																 Ext.getCmp('btnLookupKasirLABRAD').disable();
																 Ext.getCmp('btnSimpanLABRAD').disable();
																 Ext.getCmp('btnHpsBrsLABRAD').disable();
                                                        }
                                                }
                                                else
                                                {
                                                        ShowPesanWarningLABRAD('Pilih record ','Hapus data');
                                                }
                                        }
                                }
                        }
						
                ],
             viewConfig:{forceFit: true}
        }
		
		
    );
	
	

    return gridDTLTRLABRAD;
};

function HapusBarisLABRAD()
{
    if ( cellSelected2deskripsi != undefined )
    {
        if (cellSelected2deskripsi.data.DESKRIPSI2 != '' && cellSelected2deskripsi.data.KD_PRODUK != '')
        {
		
           
                                          
											var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan penghapusan:', function(btn, combo){
											if (btn == 'ok')
														{
														variablehistori=combo;
														DataDeleteKasirLABRADDetail();
														dsTRDetailKasirLABRADList.removeAt(CurrentPenataJasaKasirLABRAD.row);
														}
												});

												
                                         
                                
               
        }
        else
        {
			//dsTRDetailDiagnosaList.removeAt(CurrentDiagnosa.row);
            dsTRDetailKasirLABRADList.removeAt(CurrentPenataJasaKasirLABRAD.row);
        };
    }

};

function DataDeleteKasirLABRADDetail()
{
    Ext.Ajax.request
    (
        {
                //url: "./Datapool.mvc/DeleteDataObj",
            url: baseURL + "index.php/main/DeleteDataObj",
            params:  getParamDataDeleteKasirLABRADDetail(),
            success: function(o)
            {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true)
                {ShowPesanInfoKasirLABRAD(nmPesanHapusSukses,nmHeaderHapusData);
                    dsTRDetailKasirLABRADList.removeAt(CurrentPenataJasaKasirLABRAD.row);
                    cellSelecteddeskripsi=undefined;
                    RefreshDataKasirLABRADDetail(notransaksi);
                    AddNewKasirLABRAD = false;
                }
                else if (cst.success === false && cst.pesan === 0 )
                {
                    ShowPesanWarningKasirLABRAD(nmPesanHapusGagal, nmHeaderHapusData);
                }
                else
                {
                    ShowPesanWarningKasirLABRAD(nmPesanHapusError,nmHeaderHapusData);
                };
            }
        }
    )
};

function getParamDataDeleteKasirLABRADDetail()
{

    var params =
    {
		Table: 'ViewTrKasirRwj',
        TrKodeTranskasi: CurrentPenataJasaKasirLABRAD.data.data.NO_TRANSAKSI,
		TrTglTransaksi:  CurrentPenataJasaKasirLABRAD.data.data.TGL_TRANSAKSI,
		TrKdPasien :	 CurrentPenataJasaKasirLABRAD.data.data.KD_PASIEN,
		TrKdNamaPasien : namapasien,	
		TrKdUnit :		kodeunit,
		TrNamaUnit :namaunit,
		Uraian :		 CurrentPenataJasaKasirLABRAD.data.data.DESKRIPSI2,
		AlasanHapus : variablehistori,
		TrHarga :		 CurrentPenataJasaKasirLABRAD.data.data.HARGA,
		TrKdProduk :CurrentPenataJasaKasirLABRAD.data.data.KD_PRODUK,
        RowReq: CurrentPenataJasaKasirLABRAD.data.data.URUT,
        Hapus:2
    };
	
    return params
};


function Datasave_KasirLABRAD(mBol) 
{	
	Ext.Ajax.request
			 (
				{
				
					url: baseURL + "index.php/main/functionKasirPenunjang/savedetailpenyakit",
					params: getParamDetailTransaksiLABRAD(),
					failure: function(o)
					{
					ShowPesanWarningLABRAD('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
					RefreshDataKasirLABRADDetail(notransaksi);
					},	
					success: function(o) 
					{
						RefreshDataKasirLABRADDetail(notransaksi);
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoKasirLABRAD(nmPesanSimpanSukses,nmHeaderSimpanData);
							RefreshDataFilterKasirLABRAD();
							if(mBol === false)
							{
							RefreshDataKasirLABRADDetail(notransaksi);
							};
						}
						else 
						{
						ShowPesanWarningKasirLABRAD('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
						};
					}
				}
			)
		

	
};

function getParamDetailTransaksiLABRAD() 
{
    var params =
	{
		
		TrKodeTranskasi: notransaksi,
		KdUnit: kodeunit,
		//DeptId:Ext.get('txtKdDokter').dom.value ,tampungshiftsekarang
		Tgl: tgltrans,
		Shift: tampungshiftsekarang,
		List:getArrDetailTrLABRAD(),
		JmlField: mRecordLABRAD.prototype.fields.length-4,
		JmlList:GetListCountPenataDetailTransaksi(),
		Hapus:1,
		Ubah:0
	};
    return params
};
function getArrDetailTrLABRAD()
{
	var x='';
	for(var i = 0 ; i < dsTRDetailKasirLABRADList.getCount();i++)
	{
		if (dsTRDetailKasirLABRADList.data.items[i].data.KD_PRODUK != '' && dsTRDetailKasirLABRADList.data.items[i].data.DESKRIPSI != '')
		{
			var y='';
			var z='@@##$$@@';
			
			y = 'URUT=' + dsTRDetailKasirLABRADList.data.items[i].data.URUT
			y += z + dsTRDetailKasirLABRADList.data.items[i].data.KD_PRODUK
			y += z + dsTRDetailKasirLABRADList.data.items[i].data.QTY
			y += z + ShowDate(dsTRDetailKasirLABRADList.data.items[i].data.TGL_BERLAKU)
			y += z +dsTRDetailKasirLABRADList.data.items[i].data.HARGA
			y += z +dsTRDetailKasirLABRADList.data.items[i].data.KD_TARIF
			y += z +dsTRDetailKasirLABRADList.data.items[i].data.URUT
			
			
			if (i === (dsTRDetailKasirLABRADList.getCount()-1))
			{
				x += y 
			}
			else
			{
				x += y + '##[[]]##'
			};
		};
	}	
	
	return x;
};


function RecordBaruLABRAD()
{

	var p = new mRecordLABRAD
	(
		{
			'DESKRIPSI2':'',
			'KD_PRODUK':'',
		    'DESKRIPSI':'', 
		    'KD_TARIF':'', 
		    'HARGA':'',
		    'QTY':'',
		    'TGL_TRANSAKSI':tanggaltransaksitampung, 
		    'DESC_REQ':'',
		    'KD_TARIF':'',
		    'URUT':''
		}
	);
	
	return p;
};
var mRecordLABRAD = Ext.data.Record.create
(
    [
       {name: 'DESKRIPSI2', mapping:'DESKRIPSI2'},
       {name: 'KD_PRODUK', mapping:'KD_PRODUK'},
       {name: 'DESKRIPSI', mapping:'DESKRIPSI'},
       {name: 'KD_TARIF', mapping:'KD_TARIF'},
       {name: 'HARGA', mapping:'HARGA'},
       {name: 'QTY', mapping:'QTY'},
       {name: 'TGL_TRANSAKSI', mapping:'TGL_TRANSAKSI'},
       {name: 'DESC_REQ', mapping:'DESC_REQ'},
      // {name: 'KD_TARIF', mapping:'KD_TARIF'},
       {name: 'URUT', mapping:'URUT'}
    ]
);


function TRRawatJalanColumModel() 
{
    return new Ext.grid.ColumnModel
    (
        [
            new Ext.grid.RowNumberer(),
            {
                id: 'coleskripsiLABRAD',
                header: 'Uraian',
                dataIndex: 'DESKRIPSI2',
               // width:.30,
				menuDisabled:true,
				hidden :true
                
            },
            {
                id: 'colKdProduk',
                header: 'Kode Produk',
                dataIndex: 'KD_PRODUK',
               width:100,
				menuDisabled:true,
                hidden:true
            },
            {
                id: 'colDeskripsiLABRAD',
                header:'Nama Produk',
                dataIndex: 'DESKRIPSI',
                sortable: false,
                hidden:false,
				menuDisabled:true,
                width:250
                
            }
			,
            {
               header: 'Tanggal Transaksi',
               dataIndex: 'TGL_TRANSAKSI',
              width:100,
			   	menuDisabled:true,
              renderer: function(v, params, record)
                    {
                        
                        return ShowDate(record.data.TGL_TRANSAKSI);
                    }
            },
            {
                id: 'colHARGALABRAD',
                header: 'Harga',
				align: 'right',
				hidden: true,
				menuDisabled:true,
                dataIndex: 'HARGA',
              //  width:.10,
				renderer: function(v, params, record) 
							{
							return formatCurrency(record.data.HARGA);
							
							}	
            },
            {
                id: 'colProblemLABRAD',
                header: 'Qty',
               width:'100%',
				align: 'right',
				menuDisabled:true,
                dataIndex: 'QTY',
                editor: new Ext.form.TextField
                (
                    {
                        id:'fieldcolProblemLABRAD',
                        allowBlank: true,
                        enableKeyEvents : true,
                        width:100,
						listeners:
							{ 
								'specialkey' : function()
								{
									
											Dataupdate_KasirLABRAD(false);
											//RefreshDataFilterKasirLABRAD();
									        //RefreshDataFilterKasirLABRAD();

								}
							}
                    }
                ),
				
				
              
            },

            {
                id: 'colImpactLABRAD',
                header: 'CR',
               // width:80,
                dataIndex: 'IMPACT',
				hidden: true,
                editor: new Ext.form.TextField
                (
                        {
                                id:'fieldcolImpactLABRAD',
                                allowBlank: true,
                                enableKeyEvents : true,
                                width:30
                        }
                )
				
            }

        ]
    )
};


function getItemPaneltgl_filter() 
{
  var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .35,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'datefield',
					    fieldLabel: 'Tanggal ',
					    id: 'dtpTglAwalFilterKasirLABRAD',
					    name: 'dtpTglAwalFilterKasirLABRAD',
					    format: 'd/M/Y',
						//readOnly : true,
					    value: now,
					    anchor: '96.7%',
					    listeners:
						{
						
						 'specialkey' : function()
                            {
                                var tmpNoMedrec = Ext.get('txtFilterKasirNomedrec').getValue()
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10 )
                                {
								RefreshDataFilterKasirLABRADKasir();
								}
						}
						}
					}
				]
			},
			
			{
			    columnWidth: .35,
			    layout: 'form',
			    border: false,
				labelWidth:60,
			    items:
				[ 
		
					{
					    xtype: 'datefield',
					    fieldLabel: 's/d ',
					    id: 'dtpTglAkhirFilterKasirLABRAD',
					    name: 'dtpTglAkhirFilterKasirLABRAD',
					    format: 'd/M/Y',
						//readOnly : true,
					    value: now,
					    anchor: '100%',
						   listeners:
						{
						
						 'specialkey' : function()
                            {
                                var tmpNoMedrec = Ext.get('txtFilterKasirNomedrec').getValue()
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10 )
                                {
								RefreshDataFilterKasirLABRADKasir();
								}
						}
						}
					}
				]
			}
		]
	}
    return items;
};
function getItemPanelcombofilter() 
{
  var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .35,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					mComboStatusBayar_viKasirLABRADKasir()
				]
			},
			 
			{
			    columnWidth: .35,
			    layout: 'form',
			    border: false,
				labelWidth:60,
			    items:
				[ 
		
				mComboUnit_viKasirLABRADKasir()
				]
			}
		]
	}
    return items;
};


function mComboStatusBayar_viKasirLABRADKasir()
{
  var cboStatus_viKasirLABRADKasir = new Ext.form.ComboBox
	(
		{
			id:'cboStatus_viKasirLABRADKasir',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
		    anchor :'96%',
			emptyText:'',
			fieldLabel: 'Status Lunas',
			//width:60,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Semua'],[2, 'Lunas'], [3, 'Belum Lunas']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCountStatusByr_viKasirLABRADKasir,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectCountStatusByr_viKasirLABRADKasir=b.data.displayText ;
					//RefreshDataSetDokter();
					RefreshDataFilterKasirLABRADKasir();

				}
			}
		}
	);
	return cboStatus_viKasirLABRADKasir;
};





function mComboTransferTujuan()
{
  var cboTransferTujuan = new Ext.form.ComboBox
	(
		{
			id:'cboTransferTujuan',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
		    anchor :'96%',
			emptyText:'',
			fieldLabel: 'Transfer',
			//width:60,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Gawat Darurat'],[2, 'Rawat Inap'], [3, 'Rawat Jalan']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:tranfertujuan,
			listeners:
			{
				'select': function(a,b,c)
				{
					tranfertujuan=b.data.displayText ;
					//RefreshDataSetDokter();
					RefreshDataFilterKasirLABRADKasir();

				}
			}
		}
	);
	return cboTransferTujuan;
};

function mComboUnitInduk()
{
    var Field = ['kd_bagian','nama_unit'];

   dsunitinduk_viKasirLABRADKasir = new WebApp.DataStore({ fields: Field });

  var cboUnitInduk = new Ext.form.ComboBox
	(
		{
			id:'cboUnitInduk',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
            anchor :'22,7%',
			emptyText:'',
			fieldLabel: 'Pilih Poli',
			store: dsunitinduk_viKasirLABRADKasir,
			valueField: 'kd_bagian',
			displayField: 'nama_unit',
			value: selectindukunitLABRADKasir,
			listeners:
			{
                           
                            'select': function(a,b,c)
                            {
                                selectindukunitLABRADKasir=b.data.displayText ;
                                RefreshDataFilterKasirLABRADKasir();
                            }
			}
		}
	);
	return cboUnitInduk;
};

function mComboUnit_viKasirLABRADKasir() 
{
    var Field = ['KD_UNIT','NAMA_UNIT'];

    dsunit_viKasirLABRADKasir = new WebApp.DataStore({ fields: Field });
    dsunit_viKasirLABRADKasir.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			  
                            Sort: 'kd_unit',
			    Sortdir: 'ASC',
			    target: 'ComboUnit',
                            param: "" //+"~ )"
			}
		}
	);
	
    var cboUNIT_viKasirLABRADKasir = new Ext.form.ComboBox
	(
		{
		    id: 'cboUNIT_viKasirLABRADKasir',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
		    fieldLabel:  ' Poli ',
		    align: 'Right',
			 //width: 100,
		    anchor:'100%',
		    store: dsunit_viKasirLABRADKasir,
		    valueField: 'KD_UNIT',
		    displayField: 'NAMA_UNIT',
            value:'All',
		    listeners:
			{
                            'select': function(a, b, c) 
                            {					       
                                RefreshDataFilterKasirLABRADKasir();
                            }
                        }
		}
	);
	
    return cboUNIT_viKasirLABRADKasir;
};



function KasirLookUpLABRAD(rowdata) 
{
    var lebar = 580;
    FormLookUpsdetailTRKasirLABRAD = new Ext.Window
    (
        {
            id: 'gridKasirLABRAD',
            title: 'Kasir Lab & Rad',
            closeAction: 'destroy',
            width: lebar,
            height: 500,
            border: false,
            resizable: false,
            plain: true,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryKasirLABRAD(lebar),
            listeners:
            {
                
            }
        }
    );

    FormLookUpsdetailTRKasirLABRAD.show();
    if (rowdata == undefined) 
	{
        KasirLABRADAddNew();
    }
    else 
	{
        TRKasirLABRADInit(rowdata)
    }

};



function getFormEntryKasirLABRAD(lebar) 
{
    var pnlTRKasirLABRAD = new Ext.FormPanel
    (
        {
            id: 'PanelTRKasirLABRAD',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
             height:130,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [getItemPanelInputKasir(lebar)],
           tbar:
            [
             
            ]
        }
    );
 var x;
 var paneltotal = new Ext.Panel
	(
            {
            id: 'paneltotal',
            region: 'center',
            border:false,
            bodyStyle: 'padding:0px 7px 0px 7px',
            layout: 'column',
            frame:true,
            height:67,
            anchor: '100% 8.0000%',
            autoScroll:false,
            items:
            [
                
				
				{
					xtype: 'compositefield',
					anchor: '100%',
					labelSeparator: '',
					border:true,
					style:{'margin-top':'5px'},
					items: 
					[
		                {
                   
                    layout: 'form',
                    style:{'text-align':'right','margin-left':'370px'},
                    border: false,
                    html: " Total :"
						},
						{
                            xtype: 'textfield',
                            id: 'txtJumlah2EditData_viKasirLABRAD',
                            name: 'txtJumlah2EditData_viKasirLABRAD',
							style:{'text-align':'right','margin-left':'390px'},
                            width: 82,
                            readOnly: true,
                        },
						
		                //-------------- ## --------------
					]
				},
				 {
					xtype: 'compositefield',
					anchor: '100%',
					labelSeparator: '',
					border:true,
					style:{'margin-top':'5px'},
					items: 
					[
						{
                   
                    layout: 'form',
						style:{'text-align':'right','margin-left':'370px'},
						border: false,
						html: " Bayar :"
						},
		                {
                            xtype: 'numberfield',
                            id: 'txtJumlahEditData_viKasirLABRAD',
                            name: 'txtJumlahEditData_viKasirLABRAD',
							style:{'text-align':'right','margin-left':'390px'},
                            width: 82,
							
                            //readOnly: true,
                        }
		                //-------------- ## --------------
					]
				}
            ]

        }
    )
 var GDtabDetailKasirLABRAD = new Ext.Panel   
    (
        {
        id:'GDtabDetailKasirLABRAD',
        region: 'center',
        activeTab: 0,
	height:350,
        anchor: '100% 100%',
        border:false,
        plain: true,
        defaults:{autoScroll: true},
        items: [GetDTLTRKasirLABRADGrid(),paneltotal],
        tbar:
        [
            {
                text: 'Bayar',
                id: 'btnSimpanKasirLABRAD',
                tooltip: nmSimpan,
                iconCls: 'save',
                handler: function()
                    {
                        Datasave_KasirLABRADKasir(false);
                        FormDepan2KasirLABRAD.close()
                        //Ext.getCmp('btnEditKasirLABRAD').disable();
                        //Ext.getCmp('btnTutupTransaksiKasirLABRAD').disable();
                        //Ext.getCmp('btnHpsBrsKasirLABRAD').disable();
                    }
           },
            {
                xtype:'splitbutton',
                text:'Cetak',
                iconCls:'print',
                id:'btnPrint_viDaftar',
                handler:function()
                {		
                },
                menu: new Ext.menu.Menu({
                items: [
                    {
                        xtype: 'button',
                        text: 'Print Bill',
                        id: 'btnPrintBillRadLab',
                        handler: function()
                        {
                            printbillRadLab();
                        }
                    },
                    {
                        xtype: 'button',
                        text: 'Print Kwitansi',
                        id: 'btnPrintKwitansiRadLab',
                        handler: function()
                        {
                             printkwitansiRadLab();
                        }
                    }
                ]
                })
            }
        ]

        }
		
		
    );
	
   
   
   var pnlTRKasirLABRAD2 = new Ext.FormPanel
    (
        {
            id: 'PanelTRKasirLABRAD2',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
             height:380,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [	GDtabDetailKasirLABRAD,
			
			
			]
        }
    );

	
   
   
    FormDepan2KasirLABRAD = new Ext.Panel
	(
		{
		    id: 'FormDepan2KasirLABRAD',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
			height:380,
		    shadhow: true,
		    items: [pnlTRKasirLABRAD,pnlTRKasirLABRAD2,
			
				
			]
		}			
				

			

		
	);

    return FormDepan2KasirLABRAD
};






function TambahBarisKasirLABRAD()
{
    var x=true;

    if (x === true)
    {
        var p = RecordBaruKasirLABRAD();
        dsTRDetailKasirLABRADKasirList.insert(dsTRDetailKasirLABRADKasirList.getCount(), p);
    };
};





function GetDTLTRHistoryGrid() 
{

    var fldDetail = ['NO_TRANSAKSI','TGL_BAYAR','DESKRIPSI','URUT','BAYAR','USERNAME'];
	
    dsTRDetailHistoryList = new WebApp.DataStore({ fields: fldDetail })
		 
    var gridDTLTRHistory = new Ext.grid.EditorGridPanel
    (
        {
            title: 'History Bayar',
            stripeRows: true,
            store: dsTRDetailHistoryList,
            border: false,
            columnLines: true,
            frame: false,
			anchor: '100% 25%',
                autoScroll:true,
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                            cellSelecteddeskripsi = dsTRDetailHistoryList.getAt(row);
                            CurrentHistory.row = row;
                            CurrentHistory.data = cellSelecteddeskripsi;
                        }
                    }
                }
            ),
            cm: TRHistoryColumModel(),
					viewConfig: {forceFit: true}
			,

			      tbar:
                [
                       
                                             
							    {
                                id:'btnHpsBrsKasirLABRAD',
                                text: 'Hapus Pembayaran',
                                tooltip: 'Hapus Baris',
                                iconCls: 'RemoveRow',
								//hidden :true,
                                handler: function()
                                {
                                        if (dsTRDetailHistoryList.getCount() > 0 )
                                        {
                                                if (cellSelecteddeskripsi != undefined)
                                                {
                                                        if(CurrentHistory != undefined)
                                                        {
                                                                HapusBarisDetailbayar();
                                                        }
                                                }
                                                else
                                                {
                                                        ShowPesanWarningKasirLABRAD('Pilih record ','Hapus data');
                                                }
                                        }
										Ext.getCmp('btnEditKasirLABRAD').disable();
										Ext.getCmp('btnTutupTransaksiKasirLABRAD').disable();
										Ext.getCmp('btnHpsBrsKasirLABRAD').disable();
                                },
								disabled :true
                        }
                        
							 
							
                ]
             
        }
		
		
    );
	
	

    return gridDTLTRHistory;
};

function TRHistoryColumModel() 
{
    return new Ext.grid.ColumnModel
    (//'','','','','',''
        [
           new Ext.grid.RowNumberer(),
            {
                id: 'colKdTransaksi',
                header: 'No. Transaksi',
                dataIndex: 'NO_TRANSAKSI',
                width:100,
					menuDisabled:true,
                hidden:false
            },
			{
                id: 'colTGlbayar',
                header: 'Tgl Bayar',
                dataIndex: 'TGL_BAYAR',
					menuDisabled:true,
				width:100,
				renderer: function(v, params, record)
                {
                   return ShowDate(record.data.TGL_BAYAR);

                }
                
            },
			{
                id: 'coleurutmasuk',
                header: 'urut Bayar',
                dataIndex: 'URUT',
				//hidden:true
                
            }
            
            ,
			{
                id: 'colePembayaran',
                header: 'Pembayaran',
                dataIndex: 'DESKRIPSI',
				width:150,
				hidden:false
                
            }
			,
			{
                id: 'colJumlah',
                header: 'Jumlah',
				width:150,
				align :'right',
                dataIndex: 'BAYAR',
				hidden:false,
				renderer: function(v, params, record)
                {
                   return formatCurrency(record.data.BAYAR);

                }
                
            }
			,
			
			{
                id: 'coletglmasuk',
                header: 'tgl masuk',
                dataIndex: '',
				hidden:true
                
            }
            ,
            {
                id: 'colStatHistory',
                header: 'History',
                width:130,
				menuDisabled:true,
                dataIndex: '',
				hidden:true
              
				
				
              
            },
            {
                id: 'colPetugasHistory',
                header: 'Petugas',
                width:130,
				menuDisabled:true,
                dataIndex: 'USERNAME',	
				//hidden:true
                
				
				
              
            }
			

        ]
    )
};
function HapusBarisDetailbayar()
{
    if ( cellSelecteddeskripsi != undefined )
    {
        if (cellSelecteddeskripsi.data.NO_TRANSAKSI != '' && cellSelecteddeskripsi.data.URUT != '')
        {
		
           
                                          //'NO_TRANSAKSI','TGL_BAYAR','DESKRIPSI','URUT','BAYAR','USERNAME'
											var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan penghapusan:', function(btn, combo){
											if (btn == 'ok')
														{
														variablehistori=combo;
																		if (variablehistori!='')
																		{
																		DataDeleteKasirLABRADKasirDetail();
																		}
																		else
																		{
																			ShowPesanWarningKasirLABRAD('Silahkan isi alasan terlebih dahaulu','Keterangan');
																	
																		}
														}
														
												});
											

												
                                         
                                
               
        }
        else
        {
            dsTRDetailHistoryList.removeAt(CurrentHistory.row);
        };
    }
};

function DataDeleteKasirLABRADKasirDetail()
{
    Ext.Ajax.request
    (
        {
            url: baseURL + "index.php/main/functionKasirPenunjang/deletedetail_bayar",
            params:  getParamDataDeleteKasirLABRADKasirDetail(),
            success: function(o)
            {
			//	RefreshDatahistoribayar(Kdtransaksi);
				 RefreshDataFilterKasirLABRADKasir();
				 RefreshDatahistoribayar('0');
				 RefreshDataKasirLABRADDetail('0');
                var cst = Ext.decode(o.responseText);
                if (cst.success === true)
                {
                    ShowPesanInfoKasirLABRAD(nmPesanHapusSukses,nmHeaderHapusData);
								//alert(Kdtransaksi);					 
						
					//refeshKasirLABRADKasir();
                }
                else if (cst.success === false && cst.pesan === 0 )
                {
                    ShowPesanWarningKasirLABRAD(nmPesanHapusGagal, nmHeaderHapusData);
                }
                else
                {
                    ShowPesanWarningKasirLABRAD(nmPesanHapusError,nmHeaderHapusData);
                };
            }
        }
    )
};

function getParamDataDeleteKasirLABRADKasirDetail()
{
    var params =
    {
	//'NO_TRANSAKSI','TGL_BAYAR','DESKRIPSI','URUT','BAYAR','USERNAME'
		//Table: 'ViewKasirLABRAD',// 
        TrKodeTranskasi: CurrentHistory.data.data.NO_TRANSAKSI,
		TrTglbayar:  CurrentHistory.data.data.TGL_BAYAR,
		Urut:  CurrentHistory.data.data.URUT,
		Tgltransaksi :tgltrans, 
        Kodepasein:kodepasien,
		NamaPasien:namapasien,
        KodeUnit: kodeunit,
        Namaunit:namaunit,
        Kodepay:kodepay,
        Uraian:uraianpay,
		KDkasir: kdkasir
		
    };
	Kdtransaksi=CurrentHistory.data.data.NO_TRANSAKSI;
    return params
};



function GetDTLTRKasirLABRADGrid() 
{
    var fldDetail = ['KD_PRODUK','DESKRIPSI','DESKRIPSI2','KD_TARIF','HARGA','QTY','DESC_REQ','TGL_BERLAKU','NO_TRANSAKSI','URUT','DESC_STATUS','TGL_TRANSAKSI','BAYARTR','DISCOUNT','PIUTANG'];
	
    dsTRDetailKasirLABRADKasirList = new WebApp.DataStore({ fields: fldDetail })
   RefreshDataKasirLABRADKasirDetail() ;
  gridDTLTRKasirLABRAD = new Ext.grid.EditorGridPanel
    (
        {
            title: 'Detail Bayar',
            stripeRows: true,
			id: 'gridDTLTRKasirLABRAD',
            store: dsTRDetailKasirLABRADKasirList,
            border: false,
            columnLines: true,
            frame: false,
            anchor: '100%',
			height:230,
            autoScroll:true,
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        
                    }
                }
            ),
            cm: TRKasirRawatJalanColumModel()
        }
		
		
    );
	
	

    return gridDTLTRKasirLABRAD;
};

function TRKasirRawatJalanColumModel() 
{
    return new Ext.grid.ColumnModel
    (
        [
            new Ext.grid.RowNumberer(),
            {
                id: 'coleskripsiKasirLABRAD',
                header: 'Uraian',
                dataIndex: 'DESKRIPSI2',
                width:250,
				menuDisabled:true,
				hidden :true
                
            },
            {
                id: 'colKdProduk',
                header: 'Kode Produk',
                dataIndex: 'KD_PRODUK',
                width:100,
				menuDisabled:true,
                hidden:true
            },
            {
                id: 'colDeskripsiKasirLABRAD',
                header:'Nama Produk',
                dataIndex: 'DESKRIPSI',
                sortable: false,
                hidden:false,
				menuDisabled:true,
                width:250
                
            },
			   {
                id: 'colURUTKasirLABRAD',
                header:'Urut',
                dataIndex: 'URUT',
                sortable: false,
                hidden:true,
				menuDisabled:true,
                width:250
                
            }
			,
            {
               header: 'Tanggal Transaksi',
               dataIndex: 'TGL_TRANSAKSI',
               width: 130,
			    hidden:true,
			   menuDisabled:true,
               renderer: function(v, params, record)
                    {
                        
                        return ShowDate(record.data.TGL_TRANSAKSI);
                    }
            },
            {
                id: 'colQtyKasirLABRAD',
                header: 'Qty',
                width:91,
				align: 'right',
				menuDisabled:true,
                dataIndex: 'QTY',
             
				
				
              
            },
            {
                id: 'colHARGAKasirLABRAD',
                header: 'Harga',
				align: 'right',
				hidden: false,
				menuDisabled:true,
                dataIndex: 'HARGA',
                width:100,
				
				renderer: function(v, params, record) 
							{
							return formatCurrency(record.data.HARGA);
							
							}	
            },

            {
                id: 'colPiutangKasirLABRAD',
                header: 'Puitang',
                width:80,
                dataIndex: 'PIUTANG',
				align: 'right',
				//hidden: false,
			
				 editor: new Ext.form.TextField
                (
                    {
                        id:'fieldcolPuitangLABRAD',
                        allowBlank: true,
                        enableKeyEvents : true,
                        width:30,
						
                    }
                )
             ,
				renderer: function(v, params, record) 
							{
							
							//getTotalDetailProduk();
						
								return formatCurrency(record.data.PIUTANG);
							
						
							
							
							}
            },
			{
                id: 'colTunaiKasirLABRAD',
                header: 'Tunai',
                width:80,
                dataIndex: 'BAYARTR',
				align: 'right',
				hidden: false,
							 editor: new Ext.form.TextField
								(
									{
										id:'fieldcolTunaiLABRAD',
										allowBlank: true,
										enableKeyEvents : true,
										width:30,
										
									}
								),
				renderer: function(v, params, record) 
							{
							
							getTotalDetailProduk();
					
							return formatCurrency(record.data.BAYARTR);
							
							
							}
				
            },
			{
                id: 'colDiscountKasirLABRAD',
                header: 'Discount',
                width:80,
                dataIndex: 'DISCOUNT',
				align: 'right',
				hidden: false,
				editor: new Ext.form.TextField
								(
									{
										id:'fieldcolDiscountLABRAD',
										allowBlank: true,
										enableKeyEvents : true,
										width:30,
										
									}
								)
				,
					renderer: function(v, params, record) 
							{
							return formatCurrency(record.data.DISCOUNT);
							
							
							}
							 
				
            }

        ]
    )
};




function RecordBaruKasirLABRAD()
{

	var p = new mRecordKasirLABRAD
	(
		{
			'DESKRIPSI2':'',
			'KD_PRODUK':'',
		    'DESKRIPSI':'', 
		    'KD_TARIF':'', 
		    'HARGA':'',
		    'QTY':'',
		    'TGL_TRANSAKSI':tanggaltransaksitampung, 
		    'DESC_REQ':'',
		    'KD_TARIF':'',
		    'URUT':''
		}
	);
	
	return p;
};
function TRKasirLABRADInit(rowdata)
{
    AddNewKasirLABRADKasir = false;
	
	vkd_unit = rowdata.KD_UNIT;
	RefreshDataKasirLABRADKasirDetail(rowdata.NO_TRANSAKSI);
	Ext.get('txtNoTransaksiKasirLABRADKasir').dom.value = rowdata.NO_TRANSAKSI;
	tanggaltransaksitampung = rowdata.TANGGAL_TRANSAKSI;
        Ext.get('dtpTanggalDetransaksi').dom.value = ShowDate(rowdata.TANGGAL_TRANSAKSI);
	Ext.get('txtNoMedrecDetransaksi').dom.value= rowdata.KD_PASIEN;
	Ext.get('txtNamaPasienDetransaksi').dom.value = rowdata.NAMA;
	Ext.get('cboPembayaran').dom.value= rowdata.KET_PAYMENT;
	Ext.get('cboJenisByr').dom.value= rowdata.CARA_BAYAR; // take the displayField value 
	loaddatastorePembayaran(rowdata.JENIS_PAY);
	vkode_customer = rowdata.KD_CUSTOMER;
	tampungtypedata=0;
	tapungkd_pay='TU';
	jenispay=1;
	var vkode_customer = rowdata.LUNAS


	showCols(Ext.getCmp('gridDTLTRKasirLABRAD'));
	hideCols(Ext.getCmp('gridDTLTRKasirLABRAD'));
	vflag= rowdata.FLAG;
	//(rowdata.NO_TRANSAKSI;

	
	Ext.Ajax.request(
	{
	   
	    url: baseURL + "index.php/main/getcurrentshift",
		 params: {
	        
	        command: '0',
		
	    },
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
	    success: function(o) {
	
			tampungshiftsekarang=o.responseText




	    }
	
	});

	
	
};

function mEnabledKasirLABRADCM(mBol)
{

	 Ext.get('btnLookupKasirLABRAD').dom.disabled=mBol;
	 Ext.get('btnHpsBrsKasirLABRAD').dom.disabled=mBol;
};


///---------------------------------------------------------------------------------------///
function KasirLABRADAddNew() 
{
    AddNewKasirLABRADKasir = true;
	Ext.get('txtNoTransaksiKasirLABRADKasir').dom.value = '';
    Ext.get('dtpTanggalDetransaksi').dom.value = nowTglTransaksi.format('d/M/Y');
	Ext.get('txtNoMedrecDetransaksi').dom.value='';
	Ext.get('txtNamaPasienDetransaksi').dom.value = '';
	
	//Ext.get('txtKdUrutMasuk').dom.value = '';
	Ext.get('cboStatus_viKasirLABRADKasir').dom.value= ''
	rowSelectedKasirLABRADKasir=undefined;
	dsTRDetailKasirLABRADKasirList.removeAll();
	mEnabledKasirLABRADCM(false);
	

};

function RefreshDataKasirLABRADKasirDetail(no_transaksi) 
{
    var strKriteriaKasirLABRAD='';
   
    strKriteriaKasirLABRAD = 'no_transaksi = ~' + no_transaksi + '~';

    dsTRDetailKasirLABRADKasirList.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'tgl_transaksi',
			    //Sort: 'tgl_transaksi',
			    Sortdir: 'ASC',
			    target: 'ViewDetailbayar',
			    param: strKriteriaKasirLABRAD
			}
		}
	);
    return dsTRDetailKasirLABRADKasirList;
};

function RefreshDatahistoribayar(no_transaksi) 
{
    var strKriteriaKasirLABRAD='';
   
    strKriteriaKasirLABRAD = 'no_transaksi= ~' + no_transaksi + '~';

    dsTRDetailHistoryList.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'tgl_transaksi',
			    Sortdir: 'ASC',
			    target: 'ViewHistoryBayar',
			    param: strKriteriaKasirLABRAD
			}
		}
	);
    return dsTRDetailHistoryList;
};


///---------------------------------------------------------------------------------------///

function GetListCountPenataDetailTransaksi()
{
	
	var x=0;
	for(var i = 0 ; i < dsTRDetailKasirLABRADList.getCount();i++)
	{
		if (dsTRDetailKasirLABRADList.data.items[i].data.KD_PRODUK != '' || dsTRDetailKasirLABRADList.data.items[i].data.DESKRIPSI  != '')
		{
			x += 1;
		};
	}
	return x;
	
};

///---------------------------------------------------------------------------------------///
function getParamDetailTransaksiKasirLABRAD() 
{
	if(tampungtypedata==='')
		{
		tampungtypedata=0;
		};
		
    var params =
	{
	
	//	Table:'',
	
		kdUnit : vkd_unit,
		TrKodeTranskasi: Ext.get('txtNoTransaksiKasirLABRADKasir').getValue(),
		Tgl: Ext.get('dtpTanggalDetransaksi').dom.value,
		Shift: tampungshiftsekarang,
		kdKasir: kdkasir,
		bayar: tapungkd_pay,
		Flag:vflag,
		Typedata: tampungtypedata,
		Totalbayar:getTotalDetailProduk(),
		List:getArrDetailTrKasirLABRAD(),
		JmlField: mRecordKasirLABRAD.prototype.fields.length-4,
		JmlList:GetListCountDetailTransaksi(),
		
		Hapus:1,
		Ubah:0
	};
    return params
};

function paramUpdateTransaksi()
{
 var params =
	{
   TrKodeTranskasi: cellSelectedtutup,
   KDkasir:kdkasir
	};
	return params
};

function GetListCountDetailTransaksi()
{
	
	var x=0;
	for(var i = 0 ; i < dsTRDetailKasirLABRADKasirList.getCount();i++)
	{
		if (dsTRDetailKasirLABRADKasirList.data.items[i].data.KD_PRODUK != '' || dsTRDetailKasirLABRADKasirList.data.items[i].data.DESKRIPSI  != '')
		{
			x += 1;
		};
	}
	return x;
	
};

function getTotalDetailProduk()
{
var TotalProduk=0;
var bayar;
	var tampunggrid ;
	var x='';
	for(var i = 0 ; i < dsTRDetailKasirLABRADKasirList.getCount();i++)
	{		

			var recordterakhir;
		 
			//alert(TotalProduk);
		if (tampungtypedata==0)
		{
		tampunggrid = parseInt(dsTRDetailKasirLABRADKasirList.data.items[i].data.BAYARTR);
			
			//recordterakhir= tampunggrid
			//TotalProduk.toString().replace(/./gi, "");
			TotalProduk+= tampunggrid
		  
			recordterakhir=TotalProduk
			Ext.get('txtJumlah2EditData_viKasirLABRAD').dom.value=formatCurrency(recordterakhir);
		}
		if(tampungtypedata==3)
		{
			tampunggrid = parseInt(dsTRDetailKasirLABRADKasirList.data.items[i].data.PIUTANG);
			//TotalProduk.toString().replace(/./gi, "");
			//recordterakhir=tampunggrid
			TotalProduk+=tampunggrid
		     recordterakhir=TotalProduk
			Ext.get('txtJumlah2EditData_viKasirLABRAD').dom.value=formatCurrency(recordterakhir);
		}
		if(tampungtypedata==1)
		{
		   tampunggrid = parseInt(dsTRDetailKasirLABRADKasirList.data.items[i].data.DISCOUNT);
	
			TotalProduk+=tampunggrid
		    recordterakhir=TotalProduk
			Ext.get('txtJumlah2EditData_viKasirLABRAD').dom.value=formatCurrency(recordterakhir);
		}
	
		
		
	}	
	bayar = Ext.get('txtJumlah2EditData_viKasirLABRAD').getValue();
	return bayar;
};







function getArrDetailTrKasirLABRAD()
{
	var x='';
	for(var i = 0 ; i < dsTRDetailKasirLABRADKasirList.getCount();i++)
	{
		if (dsTRDetailKasirLABRADKasirList.data.items[i].data.KD_PRODUK != '' && dsTRDetailKasirLABRADKasirList.data.items[i].data.DESKRIPSI != '')
		{
			var y='';
			var z='@@##$$@@';
			
			y = 'URUT=' + dsTRDetailKasirLABRADKasirList.data.items[i].data.URUT
			y += z + dsTRDetailKasirLABRADKasirList.data.items[i].data.KD_PRODUK
			y += z + dsTRDetailKasirLABRADKasirList.data.items[i].data.QTY
			y += z +dsTRDetailKasirLABRADKasirList.data.items[i].data.HARGA
			y += z +dsTRDetailKasirLABRADKasirList.data.items[i].data.KD_TARIF
			y += z +dsTRDetailKasirLABRADKasirList.data.items[i].data.URUT
			y += z +dsTRDetailKasirLABRADKasirList.data.items[i].data.BAYARTR
			y += z +dsTRDetailKasirLABRADKasirList.data.items[i].data.PIUTANG
			y += z +dsTRDetailKasirLABRADKasirList.data.items[i].data.DISCOUNT
			
			
			
			if (i === (dsTRDetailKasirLABRADKasirList.getCount()-1))
			{
				x += y 
			}
			else
			{
				x += y + '##[[]]##'
			};
		};
	}	
	
	return x;
};


function getItemPanelInputKasir(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 0px',
		border:true,
		height:110,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
			    layout: 'form',
			    border: false,
			    items:
				[
					getItemPanelNoTransksiLABRADKasir(lebar),getItemPanelmedreckasir(lebar),getItemPanelUnitKasir(lebar)	
				]
			}
		]
	};
    return items;
};



function getItemPanelUnitKasir(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
				mComboJenisByrView()
				
				]
			},{
			    columnWidth: .60,
			    layout: 'form',
				labelWidth:0.9,
			    border: false,
			    items:
				[mComboPembayaran()
	
				]
			}
		]
	}
    return items;
};



function loaddatastorePembayaran(jenis_pay)
{
          dsComboBayar.load
                (
                    {
                     params:
							{
								Skip: 0,
								Take: 1000,
								Sort: 'nama',
								Sortdir: 'ASC',
								target: 'ViewComboBayar',
								param: 'jenis_pay=~'+ jenis_pay+ '~'
							}
                   }
                )
}

function mComboPembayaran()
{
    var Field = ['KD_PAY','JENIS_PAY','PAYMENT'];

    dsComboBayar = new WebApp.DataStore({fields: Field});


    var cboPembayaran = new Ext.form.ComboBox
	(
		{
		    id: 'cboPembayaran',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
            forceSelection: true,
		    emptyText:'Pilih Pembayaran...',
		    labelWidth:80,
		    align: 'Right',
		    store: dsComboBayar,
		    valueField: 'KD_PAY',
		    displayField: 'PAYMENT',
            anchor: '100%',
			listeners:
			{
			    'select': function(a, b, c) 
				{
				
						tapungkd_pay=b.data.KD_PAY;
						//getTotalDetailProduk();
					
			   
				},
				  

			}
		}
	);

    return cboPembayaran;
};


function mComboJenisByrView() 
{
	var Field = ['JENIS_PAY','DESKRIPSI','TYPE_DATA'];

    dsJenisbyrView = new WebApp.DataStore({ fields: Field });
    dsJenisbyrView.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
		
                            Sort: 'jenis_pay',
			    Sortdir: 'ASC',
			    target: 'ComboJenis',
			  
			}
		}
	);
	
    var cboJenisByr = new Ext.form.ComboBox
	(
		{
	
		    id: 'cboJenisByr',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: '',
			fieldLabel: 'Pembayaran      ',

		    align: 'Right',
		    anchor:'100%',
		    store: dsJenisbyrView,
		    valueField: 'JENIS_PAY',
		    displayField: 'DESKRIPSI',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
					
						 loaddatastorePembayaran(b.data.JENIS_PAY);
						 tampungtypedata=b.data.TYPE_DATA;
						 jenispay=b.data.JENIS_PAY;
						 showCols(Ext.getCmp('gridDTLTRKasirLABRAD'));
						 hideCols(Ext.getCmp('gridDTLTRKasirLABRAD'));
						getTotalDetailProduk();
						Ext.get('cboPembayaran').dom.value='Pilih Pembayaran...';
				
					
			   
				},
				  

			}
		}
	);
	
    return cboJenisByr;
};
    function hideCols (grid)
	{
		if (tampungtypedata==3)
		{
			grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('BAYARTR'), true);
			grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('DISCOUNT'), true);
		}
		else if(tampungtypedata==0)
		{
			grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('PIUTANG'), true);
			grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('DISCOUNT'), true);
		}
		else if(tampungtypedata==1)
		{
			grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('PIUTANG'), true);
			grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('BAYARTR'), true);
			
		}
	};
      function showCols (grid) {   
	  	if (tampungtypedata==3)
			{
			grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('PIUTANG'), false);
		
			}
			else if(tampungtypedata==0)
			{
				grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('BAYARTR'), false);
			}
			else if(tampungtypedata==1)
			{
				grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('DISCOUNT'), false);
			}
	
	};



function getItemPanelDokter(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel:'Dokter  ',
					    name: 'txtKdDokter',
					    id: 'txtKdDokter',
						//emptyText:nmNomorOtomatis,
						readOnly:true,
					    anchor: '99%'
					},{
					    xtype: 'textfield',
					    fieldLabel: 'Kelompok Pasien',
						//hideLabel:true,
						readOnly:true,
					    name: 'txtCustomer',
					    id: 'txtCustomer',
					    anchor: '99%',
						listeners: 
						{ 
							
						}
					}
				]
			},
			{
			    columnWidth: .600,
			    layout: 'form',
			    border: false,
				labelWidth:2,
			    items:
				[
					{
						xtype: 'textfield',
					   // fieldLabel:'Unit : ',
					    name: 'txtNamaDokter',
					    id: 'txtNamaDokter',
						//emptyText:nmNomorOtomatis,
						readOnly:true,
					    anchor: '100%'
					},
					{
						xtype: 'textfield',
					   // fieldLabel:'Unit : ',
					    name: 'txtKdUrutMasuk',
					    id: 'txtKdUrutMasuk',
						//emptyText:nmNomorOtomatis,
						readOnly:true,
						hidden:true,
					    anchor: '100%'
					}
				]
			}
		]
	}
    return items;
};

function getItemPanelNoTransksiLABRADKasir(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel:  'No. Transaksi ',
					    name: 'txtNoTransaksiKasirLABRADKasir',
					    id: 'txtNoTransaksiKasirLABRADKasir',
						emptyText:nmNomorOtomatis,
						readOnly:true,
					    anchor: '99%'
					}
				]
			},
			{
			    columnWidth: .60,
			    layout: 'form',
			    border: false,
				labelWidth:55,
			    items:
				[
					{
					    xtype: 'datefield',
					    fieldLabel: 'Tanggal ',
					    id: 'dtpTanggalDetransaksi',
					    name: 'dtpTanggalDetransaksi',
					    format: 'd/M/Y',
						readOnly : true,
					    value: now,
					    anchor: '100%'
					}
				]
			}
		]
	}
    return items;
};


function getItemPanelmedreckasir(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel:   'No. Medrec ',
					    name: 'txtNoMedrecDetransaksi',
					    id: 'txtNoMedrecDetransaksi',
						readOnly:true,
					    anchor: '99%',
					    listeners: 
						{ 
							
						}
					}
				]
			},
			{
			    columnWidth: .60,
			    layout: 'form',
			    border: false,
				labelWidth:2,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel: '',
						//hideLabel:true,
						readOnly:true,
					    name: 'txtNamaPasienDetransaksi',
					    id: 'txtNamaPasienDetransaksi',
					    anchor: '100%',
						listeners: 
						{ 
							
						}
					}
				]
			}
		]
	}
    return items;
};


function RefreshDataKasirLABRADKasir() 
{
    dsTRKasirLABRADKasirList.load
    (
        {
            params:
            {
                Skip: 0,
                Take: selectCountKasirLABRADKasir,
                Sort: 'tgl_transaksi',
                //Sort: 'tgl_transaksi',
                Sortdir: 'ASC',
                target:'ViewKasirLABRAD',
                param : ''
            }		
        }
    );
	
    rowSelectedKasirLABRADKasir = undefined;
    return dsTRKasirLABRADKasirList;
};

function refeshKasirLABRADKasir()
{
dsTRKasirLABRADKasirList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountKasirLABRADKasir, 
					//Sort: 'no_transaksi',
                     Sort: '',
					//Sort: 'no_transaksi',
					Sortdir: 'ASC', 
					target:'ViewKasirLABRAD',
					param : ''
				}			
			}
		);   
		return dsTRKasirLABRADKasirList;
}

function RefreshDataFilterKasirLABRADKasir() 
{

	var KataKunci='';
	
	 if (Ext.get('txtFilterKasirNomedrec').getValue() != '')
    {
		if (KataKunci == '')
		{
                        KataKunci = '   LOWER(kode_pasien) like  LOWER( ~' + Ext.get('txtFilterKasirNomedrec').getValue() + '%~)';
			
		}
		else
		{
		
                        KataKunci += ' and  LOWER(kode_pasien) like  LOWER( ~' + Ext.get('txtFilterKasirNomedrec').getValue() + '%~)';
		};

	};
	
	 if (Ext.get('TxtFilterGridDataView_NAMA_viKasirLABRADKasir').getValue() != '')
    {
		if (KataKunci == '')
		{
                        KataKunci = '  LOWER(nama) like  LOWER( ~' + Ext.get('TxtFilterGridDataView_NAMA_viKasirLABRADKasir').getValue() + '%~)';
			
		}
		else
		{
		
                        KataKunci += ' and  LOWER(nama) like  LOWER( ~' + Ext.get('TxtFilterGridDataView_NAMA_viKasirLABRADKasir').getValue() + '%~)';
		};

	};
	
	
	 if (Ext.get('cboUNIT_viKasirLABRADKasir').getValue() != '' && Ext.get('cboUNIT_viKasirLABRADKasir').getValue() != 'All')
    {
		if (KataKunci == '')
		{
	
                        KataKunci = '  LOWER(nama_unit)like  LOWER(~%' + Ext.get('cboUNIT_viKasirLABRADKasir').getValue() + '%~)';
		}
		else
		{
	
                        KataKunci += ' and LOWER(nama_unit) like  LOWER(~%' + Ext.get('cboUNIT_viKasirLABRADKasir').getValue() + '%~)';
		};
	};

		
	if (Ext.get('cboStatus_viKasirLABRADKasir').getValue() == 'Lunas')
	{
		if (KataKunci == '')
		{

                        KataKunci = '   lunas = ~true~';
		}
		else
		{
		
                        KataKunci += ' and lunas =  ~true~';
		};
	
	};
		

		if (Ext.get('cboStatus_viKasirLABRADKasir').getValue() == 'Semua')
	{
		
		
		
	};
	if (Ext.get('cboStatus_viKasirLABRADKasir').getValue() == 'Belum Lunas')
	{
		if (KataKunci == '')
		{
		
                        KataKunci = '   lunas = ~false~';
		}
		else
		{
	
                        KataKunci += ' and lunas =  ~false~';
		};
		
		
	};
	   if (Ext.get('cboUnitInduk').getValue() ==='Radiologi')
	{
		if (KataKunci == '')
		{
				//alert ('');
                        KataKunci = '   kd_bagian = 5';
		}
		else
		{
			
                        KataKunci += ' and  kd_bagian = 5';
		};
		
		
	};
	if (Ext.get('cboUnitInduk').getValue() === 'Laboratorium')
	{
		if (KataKunci == '')
		{
				//alert ('');
                        KataKunci = '   kd_bagian = 4';
		}
		else
		{
			
                        KataKunci += ' and  kd_bagian = 4';
		};
		
		
	};
	
	
		
	if (Ext.get('dtpTglAwalFilterKasirLABRAD').getValue() != '')
	{
		if (KataKunci == '')
		{                      
						KataKunci = " (tgl_transaksi between ~" + Ext.get('dtpTglAwalFilterKasirLABRAD').getValue() + "~ and ~" + Ext.get('dtpTglAkhirFilterKasirLABRAD').getValue() + "~)";
		}
		else
		{
			
                        KataKunci += " and (tgl_transaksi between ~" + Ext.get('dtpTglAwalFilterKasirLABRAD').getValue() + "~ and ~" + Ext.get('dtpTglAkhirFilterKasirLABRAD').getValue() + "~)";
		};
	
	};
	
 
	
    if (KataKunci != undefined ||KataKunci != '' ) 
    {  
		dsTRKasirLABRADKasirList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountKasirLABRADKasir, 
					//Sort: 'no_transaksi',
                                        Sort: 'tgl_transaksi',
					//Sort: 'no_transaksi',
					Sortdir: 'ASC', 
					target:'ViewKasirLABRAD',
					param : KataKunci
				}			
			}
		);   
    }
	else
	{
	
	dsTRKasirLABRADKasirList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountKasirLABRADKasir, 
					//Sort: 'no_transaksi',
                                        Sort: 'tgl_transaksi',
					//Sort: 'no_transaksi',
					Sortdir: 'ASC', 
					target:'ViewKasirLABRAD',
					param : ''
				}			
			}
		);   
	};
    
	return dsTRKasirLABRADKasirList;
};



function Datasave_KasirLABRADKasir(mBol) 
{	
	if (ValidasiEntryCMKasirLABRAD(nmHeaderSimpanData,false) == 1 )
	{
			Ext.Ajax.request
			 (
				{
					//url: "./Datapool.mvc/CreateDataObj",
					url: baseURL + "index.php/main/functionKasirPenunjang/savepembayaran",
					params: getParamDetailTransaksiKasirLABRAD(),
					failure: function(o)
					{
					ShowPesanWarningKasirLABRAD('Data Belum Simpan segera Hubungi Admin', 'Gagal');
					 RefreshDataFilterKasirLABRADKasir();
					},	
					success: function(o) 
					{
						RefreshDatahistoribayar('0');
					 RefreshDataKasirLABRADDetail('0');
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoKasirLABRAD(nmPesanSimpanSukses,nmHeaderSimpanData);
							RefreshDataKasirLABRADKasirDetail(Ext.get('txtNoTransaksiKasirLABRADKasir').dom.value);
							//RefreshDataKasirLABRADKasir();
							if(mBol === false)
							{
									 RefreshDataFilterKasirLABRADKasir();
							};
						}
						else 
						{
								ShowPesanWarningKasirLABRAD('Data Belum Simpan segera Hubungi Admin', 'Gagal');
						};
					}
				}
			)
		
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
	
};
function UpdateTutuptransaksi(mBol) 
{	
	
			Ext.Ajax.request
			 (
				{
					//url: "./Datapool.mvc/CreateDataObj",
					url: baseURL + "index.php/main/functionKasirPenunjang/ubah_co_status_transksi",
					params: paramUpdateTransaksi(),
					failure: function(o)
					{
						ShowPesanWarningKasirLABRAD('Data gagal tersimpan segera hubungi Admin', 'Gagal');
					 RefreshDataFilterKasirLABRADKasir();
					},	
					success: function(o) 
					{
						//RefreshDatahistoribayar(Ext.get('cellSelectedtutup);
					
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoKasirLABRAD(nmPesanSimpanSukses,nmHeaderSimpanData);
							
							//RefreshDataKasirLABRADKasir();
							if(mBol === false)
							{
									 RefreshDataFilterKasirLABRADKasir();
							};
							cellSelectedtutup='';
						}
						else 
						{
								ShowPesanWarningKasirLABRAD('Data gagal tersimpan segera hubungi Admin', 'Gagal');
								cellSelectedtutup='';
						};
					}
				}
			)
		
	
	
};

function ValidasiEntryCMKasirLABRAD(modul,mBolHapus)
{
	var x = 1;
	
	if((Ext.get('txtNoTransaksiKasirLABRADKasir').getValue() == '') 
	
	|| (Ext.get('txtNoMedrecDetransaksi').getValue() == '') 
	|| (Ext.get('txtNamaPasienDetransaksi').getValue() == '')
	|| (Ext.get('dtpTanggalDetransaksi').getValue() == '') 
	|| dsTRDetailKasirLABRADKasirList.getCount() === 0 
	|| (Ext.get('cboPembayaran').getValue() == '') 
	||(Ext.get('cboPembayaran').getValue() == 'Pilih Pembayaran...') )
	{
		if (Ext.get('txtNoTransaksiKasirLABRADKasir').getValue() == '' && mBolHapus === true) 
		{
			x = 0;
		}
		else if (Ext.get('cboPembayaran').getValue() == ''||Ext.get('cboPembayaran').getValue() == 'Pilih Pembayaran...') 
		{
			ShowPesanWarningKasirLABRAD(('Data pembayaran tidak  boleh kosong'), modul);
			x = 0;
		}
		else if (Ext.get('txtNoMedrecDetransaksi').getValue() == '') 
		{
			ShowPesanWarningKasirLABRAD(('Data no. medrec tidak boleh kosong'), modul);
			x = 0;
		}
		else if (Ext.get('txtNamaPasienDetransaksi').getValue() == '') 
		{
			ShowPesanWarningKasirLABRAD('Nama pasien belum terisi', modul);
			x = 0;
		}
		else if (Ext.get('dtpTanggalDetransaksi').getValue() == '') 
		{
			ShowPesanWarningKasirLABRAD(('Data tanggal kunjungan tidak boleh kosong'), modul);
			x = 0;
		}
		//cboPembayaran
	
		else if (dsTRDetailKasirLABRADKasirList.getCount() === 0) 
		{
			ShowPesanWarningKasirLABRAD('Data dalam tabel kosong',modul);
			x = 0;
		};
	};
	return x;
};




function ShowPesanWarningKasirLABRAD(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorKasirLABRAD(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoKasirLABRAD(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};

function printbillRadLab()
{
    Ext.Ajax.request
    (
        {
                url: baseURL + "index.php/main/CreateDataObj",
                params: dataparamcetakbill_vikasirDaftarRadLab(),
                success: function(o)
                {
                        var cst = Ext.decode(o.responseText);
                        if (cst.success === true)
                        {

                        }
                        else if  (cst.success === false && cst.pesan===0)
                        {
                                ShowPesanWarning_viDaftar('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
                        }
                        else
                        {
                                ShowPesanError_viDaftar('Data tidak berhasil di simpan '  + cst.pesan,'Simpan Data');
                        }
                }
        }
    );
};

var tmpkdkasirRadLab = '1';
function dataparamcetakbill_vikasirDaftarRadLab()
{
    var paramscetakbill_vikasirDaftarRadLab =
		{      
                    Table: 'DirectPrintingRadLab',
                    No_TRans: Ext.get('txtNoTransaksiKasirLABRADKasir').getValue(),
                    KdKasir : tmpkdkasirRadLab
		};
    return paramscetakbill_vikasirDaftarRadLab;
};

function printkwitansiRadLab()
{
    Ext.Ajax.request
    (
        {
                url: baseURL + "index.php/main/CreateDataObj",
                params: dataparamcetakkwitansi_vikasirDaftarRadLab(),
                success: function(o)
                {
                        var cst = Ext.decode(o.responseText);
                        if (cst.success === true)
                        {
                            
                        }
                        else if  (cst.success === false && cst.pesan !== '')
                        {
                                ShowPesanWarningKasirrwj('tidak berhasil melakukan pencetakan '  + cst.pesan,'Cetak Data');
                        }
                        else
                        {
                                ShowPesanWarningKasirrwj('tidak berhasil melakukan pencetakan '  + cst.pesan,'Cetak Data');
                        }
                }
        }
    );
};

function dataparamcetakkwitansi_vikasirDaftarRadLab()
{
    var paramscetakbill_vikasirDaftarRadLab =
		{      
                    Table: 'DirectKwitansiRadLab',
                    No_TRans: Ext.get('txtNoTransaksiKasirLABRADKasir').getValue(),
		};
    return paramscetakbill_vikasirDaftarRadLab;
};

