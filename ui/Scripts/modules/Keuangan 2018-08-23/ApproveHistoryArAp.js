var winFormLookup_ApproveHistoryArAp;
var dsGLLookup_ApproveHistoryArAp;
var selectedrowGLLookup_ApproveHistoryArAp;
var mCancel=true;
var nASALFORM_CSAR =1;
var nASALFORM_CSAP =2;
 
function FormLookup_ApproveHistoryArAp(criteria, criteriaGrd,JdlForm,nASALFORM)
{	
	winFormLookup_ApproveHistoryArAp = new Ext.Window
	(
		{
		    id: 'winFormLookup_ApproveHistoryArAp',
		    title: 'History '+JdlForm,
		    closeAction: 'destroy',
		    closable: true,
		    width: 500,
		    height: 480,
		    border: false,
		    plain: true,
		    resizable: false,
		    layout: 'border',
		    iconCls: 'find',
		    modal: true,
			autoScroll: true,
		    items: [getItemFormLookup_ApproveHistoryArAp(criteria,criteriaGrd,nASALFORM,JdlForm)],
		}
	);
	
	winFormLookup_ApproveHistoryArAp.show();
}

function getItemFormLookup_ApproveHistoryArAp(criteria,criteriaGrd,nASALFORM,JdlForm)
{
	var pnlButtonLookup_ApproveHistoryArAp = new Ext.Panel
	(
		{
			id: 'pnlButtonLookup_ApproveHistoryArAp',
			layout: 'hbox',
			border: false,
			region: 'south',
			defaults: { margins: '0 5 0 0' },
			style: { 'margin-left': '4px', 'margin-top': '3px' },
			anchor: '96.5%',
			layoutConfig:
			{
				padding: '3',
				pack: 'end',
				align: 'middle'
			},
			items:
			[
				{ xtype: 'label', cls: 'left-label', width: 120, text: 'Total Sudah Bayar :' },
				// {
				// 	xtype:'textfield',
				// 	name: 'txtTotal_ApproveHistoryArAp',
				// 	id: 'txtTotal_ApproveHistoryArAp',
				// 	width:100,
				// 	readOnly:true,
				// 	style: 'font-weight: bold;text-align: right'
				// 	// hidden:true
				// },
				{
					xtype:'textfield',
					name: 'txtTotalPaid_ApproveHistoryArAp',
					id: 'txtTotalPaid_ApproveHistoryArAp',
					width:100,
					readOnly:true,
					style: 'font-weight: bold;text-align: right'
					// hidden:true
				},
				// {
				// 	xtype:'textfield',
				// 	name: 'txtTotalRemain_ApproveHistoryArAp',
				// 	id: 'txtTotalRemain_ApproveHistoryArAp',
				// 	width:100,
				// 	readOnly:true,
				// 	style: 'font-weight: bold;text-align: right'
				// 	// hidden:true
				// },
				{
					xtype: 'button',
					width: 70,
					text: 'Ok',
					handler: function()
					{
						winFormLookup_ApproveHistoryArAp.close();
					}
				}
			],      
			listeners: 
			{
				activate: function()
				{		
					// CalcTotalRemain_ApproveHistoryArAp();
					CalcTotalpaid_ApproveHistoryArAp();
					// CalcTotalamount_ApproveHistoryArAp();
				}												
			}	
		}
	);
	
	var frmListLookup_ApproveHistoryArAp = new Ext.Panel
	(
		{
			id: 'frmListLookup_ApproveHistoryArAp',
			layout: 'form',
			region: 'center',
			autoScroll: true,
			items: 
			[
				getGridListLookup_ApproveHistoryArAp(criteria,criteriaGrd,nASALFORM,JdlForm),
				pnlButtonLookup_ApproveHistoryArAp,
			],
		}
	);
	
	return frmListLookup_ApproveHistoryArAp;
}

function getGridListLookup_ApproveHistoryArAp(criteria,criteriaGrd,nASALFORM,JdlForm)
{
	var fields_ApproveHistoryArAp = 
	[
		'CSAR_NUMBER','CSAR_DATE','ARO_NUMBER','ARO_DATE','NOMOR','TGL','AMOUNT','PAID','REMAIN'
	];
	dsGLLookup_ApproveHistoryArAp = new WebApp.DataStore({ fields: fields_ApproveHistoryArAp });

	RefreshDataLookup_ApproveHistoryArAp(criteria,criteriaGrd,nASALFORM);
	if (nASALFORM == nASALFORM_CSAR)
	{	
		var gridListLookup_ApproveHistoryArAp = new Ext.grid.EditorGridPanel
		(
			{
				stripeRows: true,
				id: 'gridListLookup_ApproveHistoryArAp',
				store: dsGLLookup_ApproveHistoryArAp,
				height:353,
				anchor: '100% 90%',
				columnLines:true,
				bodyStyle: 'padding:0px',
				// autoScroll: true,
				border: false,
				viewConfig : 
				{
					forceFit: true
				},				
				sm: new Ext.grid.RowSelectionModel
				(
					{ 
						singleSelect: false,
						listeners:
						{ 
							rowselect: function(sm, row, rec) 
							{
								selectedrowGLLookup_ApproveHistoryArAp = dsGLLookup_ApproveHistoryArAp.getAt(row);
							}						
						}
					}
				),
				cm: new Ext.grid.ColumnModel
				(
					[
						new Ext.grid.RowNumberer(),
						{ 
							id: 'colNoARLookup_ApproveHistoryArAp',
							header: 'Nomor',
							dataIndex: 'NOMOR',
							// hidden: true,
							renderer: function(value, cell) 
							{
								var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
								return str;
							}						
						},
						// { 
						// 	id: 'colNoARLookup_ApproveHistoryArAp',
						// 	header: 'Tanggal App',
						// 	dataIndex: 'TGL',
						// 	// hidden: true,
						// 	renderer: function(v, params, record) 
						// 	{
						// 		return ShowDate(record.data.TGL);
						// 	},	
						// },
						{ 
							id: 'colAmountARLookupApproveHistoryArAp',
							header: 'Amount',
							dataIndex: 'AMOUNT',
							width:100,	
							align:'right',						
							renderer: function(v, params, record) 
							{
								var str = "<div style='white-space:normal;padding:2px 20px 2px 2px'>" + formatCurrencyDec(record.data.AMOUNT) + "</div>";
								return str;
								 
							}	
						},
						{ 
							id: 'colPaidARLookupApproveHistoryArAp',
							header: 'Paid',
							dataIndex: 'PAID',
							width:100,	
							align:'right',						
							renderer: function(v, params, record) 
							{
								var str = "<div style='white-space:normal;padding:2px 20px 2px 2px'>" + formatCurrencyDec(record.data.PAID) + "</div>";
								return str;
								 
							}	
						},
						{ 
							id: 'colRemainARLookupApproveHistoryArAp',
							header: 'Remain',
							dataIndex: 'REMAIN',
							width:100,	
							align:'right',						
							renderer: function(v, params, record) 
							{
								var str = "<div style='white-space:normal;padding:2px 20px 2px 2px'>" + formatCurrencyDec(record.data.REMAIN) + "</div>";
								return str;
								 
							}	
						},
						{
							id: 'spacer_',
							header: '',
							dataIndex: 'spacer_',
							width: 10,
						}
					]
				),
				listeners:
				{
					'afterrender': function()
					{
						this.store.on("datachanged", function()
							{
								// CalcTotalRemain_ApproveHistoryArAp();
								CalcTotalpaid_ApproveHistoryArAp();
								// CalcTotalamount_ApproveHistoryArAp();
							}
						);
					}
				}
			}
		);
	}else
	{
		var gridListLookup_ApproveHistoryArAp = new Ext.grid.EditorGridPanel
		(
			{
				stripeRows: true,
				id: 'gridListLookup_ApproveHistoryArAp',
				store: dsGLLookup_ApproveHistoryArAp,
				height:353,
				anchor: '100% 90%',
				columnLines:true,
				bodyStyle: 'padding:0px',
				// autoScroll: true,
				border: false,
				viewConfig : 
				{
					forceFit: true
				},				
				sm: new Ext.grid.RowSelectionModel
				(
					{ 
						singleSelect: false,
						listeners:
						{ 
							rowselect: function(sm, row, rec) 
							{
								selectedrowGLLookup_ApproveHistoryArAp = dsGLLookup_ApproveHistoryArAp.getAt(row);
							}						
						}
					}
				),
				cm: new Ext.grid.ColumnModel
				(
					[
						new Ext.grid.RowNumberer(),
						{ 
							id: 'colNoAPLookup_ApproveHistoryArAp',
							header: 'Nomor',
							dataIndex: 'NOMOR',
							// hidden: true,
							renderer: function(value, cell) 
							{
								var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
								return str;
							}						
						},
						// { 
						// 	id: 'colNoGLLookup_ApproveHistoryArAp',
						// 	header: 'Tanggal App',
						// 	dataIndex: 'APOH_DATE',
						// 	// hidden: true,
						// 	renderer: function(v, params, record) 
						// 	{
						// 		return ShowDate(record.data.AROH_DATE);
						// 	},	
						// },
						{ 
							id: 'colAmountAPLookupApproveHistoryArAp',
							header: 'Amount',
							dataIndex: 'AMOUNT',
							width:100,	
							align:'right',						
							renderer: function(v, params, record) 
							{
								var str = "<div style='white-space:normal;padding:2px 20px 2px 2px'>" + formatCurrencyDec(record.data.AMOUNT) + "</div>";
								return str;
								 
							}	
						},
						{ 
							id: 'colPaidAPLookupApproveHistoryArAp',
							header: 'Paid',
							dataIndex: 'PAID',
							width:100,	
							align:'right',						
							renderer: function(v, params, record) 
							{
								var str = "<div style='white-space:normal;padding:2px 20px 2px 2px'>" + formatCurrencyDec(record.data.PAID) + "</div>";
								return str;
								 
							}	
						},
						{ 
							id: 'colRemainAPookupApproveHistoryArAp',
							header: 'Remain',
							dataIndex: 'REMAIN',
							width:100,	
							align:'right',						
							renderer: function(v, params, record) 
							{
								var str = "<div style='white-space:normal;padding:2px 20px 2px 2px'>" + formatCurrencyDec(record.data.REMAIN) + "</div>";
								return str;
								 
							}	
						},
						{
							id: 'spacer_',
							header: '',
							dataIndex: 'spacer_',
							width: 10,
						}
					]
				),
				listeners:
				{
					'afterrender': function()
					{
						this.store.on("datachanged", function()
							{
								// CalcTotalRemain_ApproveHistoryArAp();
								CalcTotalpaid_ApproveHistoryArAp();
								// CalcTotalamount_ApproveHistoryArAp();
							}
						);
					}
				}
			}
		);
	};
	
	return gridListLookup_ApproveHistoryArAp;
}

function RefreshDataLookup_ApproveHistoryArAp(criteria,criteriaGrd,nASALFORM)
{
	var StrTarget='';
	if (nASALFORM == nASALFORM_CSAR )
	{	
		StrTarget='ViewAROHistory'
		// criteriaGrd = " Where CSAR.CUST_CODE='" + criteria  + "'"
		criteriaGrd = " Where CSAR.CSAR_NUMBER='" + criteria  + "' and CSAR.CSAR_DATE='"+ criteriaGrd +"'"
	}else
	{
		StrTarget='ViewAPOHistory'
		// criteriaGrd = " Where CSAP.VEND_CODE='" + criteria  + "'"
		criteriaGrd = " Where CSAP.CSAP_NUMBER='" + criteria  + "' and CSAP.CSAP_DATE='"+ criteriaGrd +"'"
	};

	dsGLLookup_ApproveHistoryArAp.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: '',
			    Sortdir: '',
			    target: StrTarget,
			    param: criteriaGrd 
			}
		}
	);
	
	return dsGLLookup_ApproveHistoryArAp;
}

function CalcTotalRemain_ApproveHistoryArAp(idx)
{
    var total=Ext.num(0);
	var nilai=Ext.num(0);
	for(var i=0;i < dsGLLookup_ApproveHistoryArAp.getCount();i++)
	{
		nilai=dsGLLookup_ApproveHistoryArAp.data.items[i].data.REMAIN;
		total += nilai;	
	}
	Ext.get('txtTotalRemain_ApproveHistoryArAp').dom.value=formatCurrencyDec(total);
};

function CalcTotalpaid_ApproveHistoryArAp(idx)
{
    var total=Ext.num(0);
	var nilai=Ext.num(0);
	for(var i=0;i < dsGLLookup_ApproveHistoryArAp.getCount();i++)
	{
		nilai=dsGLLookup_ApproveHistoryArAp.data.items[i].data.PAID;
		total += nilai;	
	}
	Ext.get('txtTotalPaid_ApproveHistoryArAp').dom.value=formatCurrencyDec(total);
};

function CalcTotalamount_ApproveHistoryArAp(idx)
{
    var total=Ext.num(0);
	var nilai=Ext.num(0);
	for(var i=0;i < dsGLLookup_ApproveHistoryArAp.getCount();i++)
	{
		nilai=dsGLLookup_ApproveHistoryArAp.data.items[i].data.AMOUNT;
		total += nilai;	
	}
	Ext.get('txtTotal_ApproveHistoryArAp').dom.value=formatCurrencyDec(total);
};