var CurrentTR_LPJ = 
{
    data: Object,
    details:Array, 
    row: 0
};

var mRecord_LPJ = Ext.data.Record.create
	(
		[
		   {name: 'ACCOUNT', mapping:'ACCOUNT'},
		   {name: 'NAMAACCOUNT', mapping:'NAMAACCOUNT'},
		   {name: 'DESCRIPTION', mapping:'DESCRIPTION'},
		   {name: 'VALUE', mapping:'VALUE'},
		   {name: 'LINE', mapping:'LINE'}
		]
	);

var mRecordDTLItem_LPJ = Ext.data.Record.create
(
	[	
		{name: 'CSO_NUMBER', mapping:'CSO_NUMBER'},
		{name: 'CSO_DATE', mapping:'CSO_DATE'},
		{name: 'ACI_CODE', mapping:'ACI_CODE'},
		{name: 'ACI_DESCRIPTION', mapping:'ACI_DESCRIPTION'},
		{name: 'LINE', mapping:'LINE'},
		{name: 'VALUE', mapping:'VALUE'},
		{name: 'NOTE', mapping:'NOTE'},
		{name: 'ACI_CODE_TEMP', mapping:'ACI_CODE_TEMP'},
	]
);
var KDkategori_LPJ='6';
var KDkategori_PENGEMBALIAN='7';
var REFFIDKBS_LPJ; //diisi dilookupsp3d.js
var REFFIDLPJ_LPJ='23';
var mCtrFokus;
var selectUnitKerja_LPJ;
var dsUnitKerja_LPJ;
var dsTRList_LPJ;
var dsTmp_LPJ;
var dsDTLTRList_LPJ;
var DataAddNew_LPJ = true;
var selectCount_LPJ=50;
var now_LPJ = new Date();
var selectUnitKerja_LPJ;
var selectBayar_LPJ;
var selectAktivaLancar_LPJ;
var rowSelected_LPJ;
var TRLookUps_LPJ;
var no_sp3d;
var sts_pengembalian;
var sts_preview;
var Strno_pengembalian =""
var strketerangan_pengembalian =""
var strJumlah_pengembalian=0;
var saldoAkunLPJ= 0 ;
var NamaForm_LPJ="Realisasi";
var thn = '';
var cellSelectedDet_LPJ;
var CurrentDTLItem_LPJ =
{
    data: Object,
    details:Array, 
    row: 0
};
var  GridDTLItem_LPJ;

CurrentPage.page=getPanel_LPJ(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanel_LPJ(mod_id)
{
    var Field = 
	[
	 'cso_number', 
	 'cso_date', 
	 'personal', 
	 'account',  
	 'pay_code', 
	 'pay_no', 
	 'currency', 
	 'kurs', 
	 'type',  
	 'amount', 
	 'notes', 
	 'no_tag',  
	 'date_tag', 
	 'amountkurs',
	 'kategori', 
	 'kd_user', 
	 'kd_unit_kerja',  
	 'nama_unit_kerja', 
	 'payment', 
	 'namaaccount',  
	 'approve' ,
	 'approve_tmp' ,
	 'referensi',
	 'referensi_tmp' ,
	 'kd_jns_semester', 
	 'jenis_sms', 
	 'is_pengembalian',
	 'pengembalian',
	 'jumlah', 
	 'filepath',
	 'reff_id',
	 'no_ppd'
	]
    dsTRList_LPJ = new WebApp.DataStore({ fields: Field });
        
  
    var chkApprove_LPJ = new Ext.grid.CheckColumn
	(
		{
			id: 'chkApprove_LPJ',
			header: "Approved",
			align: 'center',
			disable:true,
			dataIndex: 'approve_tmp',
			width: 70
		}
	);	
  
    var grListTR_LPJ = new Ext.grid.EditorGridPanel
	(
		{
			/*stripeRows: true,
			store: dsTRList_LPJ,
			anchor: '100% 100%',
			columnLines:true,
			border:false,*/
			
			id:'grListTR_LPJ',
			xtype: 'editorgrid',
			stripeRows: true,
            title: '',
            store: dsTRList_LPJ,
            autoScroll: true,
            columnLines: true,
            border:false,  
            anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        rowselect: function(sm, row, rec)
                        {
                            rowSelected_LPJ = undefined;
                            rowSelected_LPJ = dsTRList_LPJ.getAt(row);
                        }
                    }
                }
            ),
/*			sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelected_LPJ = dsTRList_LPJ.getAt(row);
						}
					}
				}
			),*/			
			//cm: new Ext.grid.ColumnModel
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'CSO_Number',
						header: 'No. ' + NamaForm_LPJ,//Pengeluaran',
						dataIndex: 'cso_number',
						sortable: true,			
						width :100,
						filter: {}
					}, 
					{
						id: 'Referensi',
						header: 'No. Referensi',
						dataIndex: 'referensi',
						sortable: true,			
						width :100,
						filter: {}
					}, 
					{
						//xtype: 'datecolumn',
						header: 'Tanggal',
						width: 100,
						sortable: true,
						dataIndex: 'cso_date',
						id:'cso_date',
						renderer: function(v, params, record) 
						{
							return ShowDate(record.data.cso_date);
						},
						filter: {}									
					}, 
					{						
						id: 'Personal',
						header: "Dibayarkan Kepada",
						dataIndex: 'personal',
						width :130,
						filter: {}						
					},
					{
						id: 'Nama_Unit_Kerja',
						header: 'Unit Kerja',
						dataIndex: 'nama_unit_kerja',
						width :180,
						filter: {}						
					},
					{
						id: 'NOTES',
						header: "Keterangan",
						dataIndex: 'notes',
						width :200,
						filter: {}						
					}, 
					{
						id: 'Amount',
						header: "Jumlah (Rp.)",
						align:'right',
						dataIndex: 'amount',
						renderer: function(v, params, record) 
						{
							return formatCurrencyDec(record.data.amount);
						},	
						width :80,
						filter: {}						
					},
					 chkApprove_LPJ
				]
			),

			//plugins: chkApprove_LPJ,
			tbar: 
			[				
				{
					id: 'btnEdit_LPJ',
					text: 'Edit Data',
					tooltip: 'Edit Data',
					iconCls: 'Edit_Tr',
					handler: function(sm, row, rec) 
					{ 
						if (rowSelected_LPJ != undefined)
						{
							LookUpForm_LPJ(rowSelected_LPJ.data);
							console.log(rowSelected_LPJ.data.approve);
							ButtonDisabled_LPJ(rowSelected_LPJ.data.approve);
							// Ext.getCmp('btnSimpan').setDisabled(true);
							// Ext.getCmp('btnSimpanKeluar').setDisabled(true);
						}
						else
						{
							LookUpForm_LPJ();
						}
					}
				},' ','-'
				,
				{
					xtype: 'checkbox',
					id: 'chkWithTgl_LPJ',					
					hideLabel:true,					
					checked: true,
					handler: function() 
					{
						if (this.getValue()===true)
						{
							Ext.get('dtpTglAwalFilter_LPJ').dom.disabled=false;
							Ext.get('dtpTglAkhirFilter_LPJ').dom.disabled=false;
							Ext.get('dtpTglAwalFilter_LPJ').dom.readOnly=true;	
							Ext.get('dtpTglAkhirFilter_LPJ').dom.readOnly=true;
						}
						else
						{
							Ext.get('dtpTglAwalFilter_LPJ').dom.disabled=true;
							Ext.get('dtpTglAkhirFilter_LPJ').dom.disabled=true;							
						};
					}
				}
				, ' ','-','Tanggal : ', ' ',
				{
					xtype: 'datefield',
					fieldLabel: 'Dari Tanggal : ',
					id: 'dtpTglAwalFilter_LPJ',
					format: 'd/M/Y',
					value:now_LPJ,
					width:100,
					onInit: function() { }
				}, ' ', ' s/d ',' ', {
					xtype: 'datefield',
					fieldLabel: 'Sd /',
					id: 'dtpTglAkhirFilter_LPJ',
					format: 'd/M/Y',
					value:now_LPJ,
					width:100
				}
			],
			bbar:new WebApp.PaggingBar
			(
				{
					displayInfo: true,
					store: dsTRList_LPJ,
					pageSize: selectCount_LPJ,
					displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
					emptyMsg: "Tidak ada record&nbsp;&nbsp;"		
				}
			),
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					
					if (rowSelected_LPJ != undefined)
						{
							LookUpForm_LPJ(rowSelected_LPJ.data);
							ButtonDisabled_LPJ(rowSelected_LPJ.data.approve_tmp);
							// Ext.getCmp('btnSimpan').setDisabled(true);
							// Ext.getCmp('btnSimpanKeluar').setDisabled(true);
						}
						else
						{
							LookUpForm_LPJ();
						}
				}, 
				'afterrender': function(){ 
					RefreshDataFilter_LPJ(true);	
				}
				// End Function # --------------
			},
			viewConfig: {forceFit: true} 			
		}
	);


        // from LPJ
	var FormTR_LPJ = new Ext.Panel
	(
		{
			id: mod_id,
			closable:true,
			region: 'center',
			layout: 'form',
			title: NamaForm_LPJ, //'Penerimaan Mahasiswa',          
			border: false,           
			shadhow: true,
			iconCls: 'Penerimaan',
			 margins:'0 5 5 0',
			items: [grListTR_LPJ],
			tbar: 
			[
				'No. '+ NamaForm_LPJ +' : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: 'No ' + NamaForm_LPJ + ' :',//Pengeluaran : ',
					id: 'txtNoFilter_LPJ',                   
					//anchor: '25%',
					width:120,
					listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										RefreshDataFilter_LPJ(true);					
									} 						
								}
							},
					onInit: function() { }
				},'-',' ','Unit Kerja'+': ', ' ',mComboUnitKerjaView_LPJ(),
				//  ' ','-',
				// 	'Maks.Data : ', ' ',mComboMaksData_LPJ(),
					' ','->',
				{
					xtype: 'checkbox',
					id: 'chkFilterApprovedLPJ',
					boxLabel: 'Approved'
				},
				
				{
					xtype: 'button',
					tooltip: 'Tampilkan',
					iconCls: 'refresh',
					text: 'Tampilkan',
					anchor: '25%',
					handler: function(sm, row, rec) 
					{
						RefreshDataFilter_LPJ();
					}
				}
			],
			/* listeners: 
			{ 
				'afterrender': function() 
				{           
					//RefreshDataFilter_LPJ(true);
					RefreshDataFilter_LPJ(true);	
					//RefreshData_LPJ();			 
				}
			} */
		}
	);
	return FormTR_LPJ

};
    // end function get panel main data
 
 ///---------------------------------------------------------------------------------------///
   
   
function LookUpForm_LPJ(rowdata)
{
	var lebar=735;
	TRLookUps_LPJ = new Ext.Window
	(
		{
			id: 'LookUpForm_LPJ',
			title: NamaForm_LPJ,
			closeAction: 'destroy',
			width: lebar,
			height: 560, 
			border: false,
			resizable:false,
			plain: true,
			layout: 'fit',
			iconCls: 'Penerimaan',
			modal: true,
			items: getFormEntryTR_LPJ(lebar),
			listeners:
			{
				activate: function() 
				{					
				},
				afterShow: function() 
				{ 
					this.activate(); 
				},
				deactivate: function()
				{
					rowSelected_LPJ=undefined;
					 RefreshDataFilter_LPJ(true);
				}
			}
		}
	);
	
	TRLookUps_LPJ.show();
	if (rowdata == undefined)
	{
		AddNew_LPJ();
		sts_pengembalian =false;
	}
	else
	{
		DataInit_LPJ(rowdata)
	}	
	
};
   
function getFormEntryTR_LPJ(lebar) 
{
	var HighGrid = 215;
	var pnlTR_LPJ = new Ext.FormPanel
	(
		{
			id: 'pnlTR_LPJ',
			fileUpload: true,
			region: 'north',
			layout: 'fit',
			bodyStyle: 'padding:10px 10px 10px 10px',			
			anchor: '100%', 
			width:lebar,			
			border: false,			
			items: [getItemPanelInput_LPJ(lebar)],
			tbar: 
			[
				{
					text: 'Tambah',
					id:'btnTambah',
					tooltip: 'Tambah Record Baru ',
					iconCls: 'add',
					//handler: function() { TRDataAddNew_LPJ(pnlTR_LPJ) }
					handler: function() 
					{ 
						AddNew_LPJ();
						//ButtonDisabled_LPJ(false);
						/*Ext.getCmp('btnSimpan').setDisabled(false);knp di disable, lupa lagi
						Ext.getCmp('btnSimpanKeluar').setDisabled(false);*/
					}
				}, '-', 
				{
					text: 'Simpan',
					id:'btnSimpan',
					tooltip: 'Rekam Data ',
					iconCls: 'save',
					//handler: function() { TRDatasave_LPJ(pnlTR_LPJ) }
					handler: function() 
					{ 
						/*Ext.getCmp('btnSimpan').setDisabled(true);knp di disable, lupa lagi
						Ext.getCmp('btnSimpanKeluar').setDisabled(true);*/
						Datasave_LPJ(false);
						RefreshDataFilter_LPJ(false);
					}
				}, '-', 
				{
					text: 'Simpan & Keluar',
					id:'btnSimpanKeluar',
					tooltip: 'Rekam Data & Keluar ',
					iconCls: 'saveexit',
					//handler: function() { TRDatasave_LPJ(pnlTR_LPJ) }
					handler: function() 
					{ 
						/*Ext.getCmp('btnSimpan').setDisabled(true);knp di disable, lupa lagi
						Ext.getCmp('btnSimpanKeluar').setDisabled(true);*/
						Datasave_LPJ(true);
						RefreshDataFilter_LPJ(false);
						TRLookUps_LPJ.close();
					}
				}, '-', 
				{
					text: 'Hapus',
					id:'btnHapus',
					tooltip: 'Remove the selected item',
					iconCls: 'remove',
					handler: function() 
					{ 
						Ext.Msg.show
						(
							{
							   title:'Hapus',
							   msg: 'Apakah transaksi ini akan dihapus ?', 
							   buttons: Ext.MessageBox.YESNO,
							   fn: function (btn) 
							   {			
								   if (btn =='yes') 
									{
										DataDelete_LPJ();
										RefreshDataFilter_LPJ(false);
										Ext.getCmp('btnSimpan').setDisabled(false);
										Ext.getCmp('btnSimpanKeluar').setDisabled(false);
									} 
							   },
							   icon: Ext.MessageBox.QUESTION
							}
						);
					}
				}, '-', 
				{
					text: 'Lookup',
					id:'btnLookup',
					tooltip: 'Lookup Account',
					iconCls: 'find',
					handler: function() 
					{
						/*if (mCtrFokus==="txtReferensi_LPJ")//txtAliasReferensi_LPJ
						{*/
							mCtrFokus="";
							var StrKriteria;
							if (selectUnitKerja_LPJ != "")
							{
								StrKriteria = " WHERE APP_SP3D = '1' AND SP3.KD_UNIT_KERJA='" + selectUnitKerja_LPJ + "' "
								StrKriteria += " and (( TAHAP_PROSES in(2) AND AC.NO_Tag is null ) or (TAHAP_PROSES in(3) AND AC.NO_Tag is not null )) AND  SP3.TAHUN_ANGGARAN_TA = " + thn
							}
							else
							{
								//StrKriteria = " WHERE APP_SP3D = 1  and TAHAP_PROSES in(4,3)  "
								StrKriteria = " WHERE APP_SP3D = '1'  and ((  TAHAP_PROSES in(2) AND AC.NO_Tag is null  ) or ( TAHAP_PROSES in(3) AND AC.NO_Tag is not null  )) AND  SP3.TAHUN_ANGGARAN_TA = " + thn						
							}							
							var p = new mRecord_LPJ
							(
								{
									ACCOUNT: '',
									NAMAACCOUNT: '',
									DESCRIPTION: Ext.get('txtCatatan_LPJ').getValue(),
									VALUE: '',								
									LINE:''
								}
							);							
							FormLookupsp3d(dsDTLTRList_LPJ,p, StrKriteria,nASALFORM_LPJ,thn);
						/*}
						else
						{		
							var p = new mRecord_LPJ
							(
								{
									ACCOUNT: '',
									NAMAACCOUNT: '',
									DESCRIPTION: Ext.get('txtCatatan_LPJ').getValue(),
									VALUE: '',								
									LINE:''
								}
							);
							FormLookupAccount(" Where left(A.Account,1) in ('1','2','3','5')  AND A.TYPE ='D'  ",dsDTLTRList_LPJ,p,true,'',true);
						}*/
					}
				}, '-',
				{
				    text: 'Approve',
					id:'btnApprove',
				    tooltip: 'Approve',
				    iconCls: 'approve',
				    handler: function() 
					{ 
						if(ValidasiAppEntry_LPJ('approve data') == 1){
							FormApprove(Ext.get('txtTotal_LPJ').getValue(),'8',
							Ext.get('txtNo_LPJ').getValue(),Ext.get('txtCatatan_LPJ').dom.value,Ext.get('dtpTanggal_LPJ').getValue())
						}
					}
				}
				, '-', '->', '-',
				{
					text: 'Cetak',
					tooltip: 'Print',
					iconCls: 'print',					
					handler: function() 
					{
					if(Validasi_LPJView() == 1)
						{
							/*var criteria = GetCriteria_LPJView();
							ShowReport('', '020401', criteria);*/
							Form_CetakKwitansi('',1,Ext.get('txtTerimaDari_LPJ').dom.value,'','IKIP SILIWANGI');
						}
					}
				}
			]
		}
	);  // end Head panel
		
	var Total = new Ext.Panel
	(
		{
			frame: false,
			layout: 'form',
			width: 500,
			border:false,
			id:'PnlTotalLPJ',
			labelAlign:'right',
			labelWidth:50,
			style: 
			{
				//'margin-top': '5.9px',
				//'margin-left': '457px'
				'margin-top': '4px',
				'margin-left': '330px'
			},
			items: 
			[
			    {
					xtype: 'compositefield',
					items:
					[
						{
							xtype: 'button',
							text: 'Pengembalian',								
							id: 'btnPengembalian_bpk',
							handler: function()
							{
								call_pengembalian("preview");
							}
						},
						{
								xtype: 'displayfield',
								flex: 1,
								width: 50,
								name: '',
								value: 'Total :',
								fieldLabel: '',
								id: 'lblTotal_LPJ',
								name: 'lblTotal_LPJ'
						},
						{
							xtype: 'textfield',
							id:'txtTotal_LPJ',
							name:'txtTotal_LPJ',
							fieldLabel: '',
							readOnly:true,
							style:
							{	
								'text-align':'right',
								'font-weight':'bold'
							},
							value:'0',
							width: 185
						}
					]
				}
			
			
				
			]
		}
	);

	var TotalItem = new Ext.Panel
	(
		{
			frame: false,
			layout: 'form',
			width: 500,
			border:false,
			id:'PnlTotalItemDtl',
			labelAlign:'right',
			labelWidth:50,
			style: 
			{
				'margin-top': '5.9px',
				'margin-left': '400px'
			},
			items: 
			[
				{
					xtype: 'compositefield',
					items:
					[
						{
							xtype: 'displayfield',
							flex: 1,
							width: 50,
							name: '',
							value: 'Total :',
							fieldLabel: '',
							id: 'lblTotalItem_LPJ',
							name: 'lblTotalItem_LPJ'
						},
						{
							xtype: 'textfield',
							id:'txtTotalItem_LPJ',
							name:'txtTotalItem_LPJ',
							fieldLabel: '',
							readOnly:true,
							style:
							{	
								'text-align':'right',
								'font-weight':'bold'
							},
							value:'0',
							width: 185
						}
					]
				}
			]
		}
	);

	var NoteItem = new Ext.Panel
	(
		{
			frame: false,
			layout: 'form',
			// width: 500,
			anchor:'100% 100%',
			border:false,
			id:'PnlNoteDtlItem',
			labelAlign:'right',
			labelWidth:50,
			style: 
			{
				'margin-top': '5.9px',
				'margin-left': '0px'
			},
			items: 
			[
				{
					xtype: 'compositefield',
					fieldLabel: 'Note ',
					items:
					[
						{
							xtype: 'textfield',
							id:'txtNoteItem_LPJ',
							name:'txtNoteItem_LPJ',
							fieldLabel: '',
							width: 650
						}
					]
				}
			]
		}
	);


	var GDtabDetail_LPJ = new Ext.TabPanel
	(
		{
			region: 'center',
			id: 'GDtabDetail_LPJ',
			activeTab: 0,
			//anchor: '100% 46%',
			anchor: '100% 60%',
			border: false,
			plain: true,
			defaults: 
			{
				autoScroll: false
			},
			items: //GetDTLTRGrid_LPJ(),
			[
				{  	
					title: 'Detail ' + NamaForm_LPJ, 
					id:'tabDtlRealisasi_LPJ', 
					frame: false,					
					border:false,					
					items: 
					[
						GetDTLTRGrid_LPJ(HighGrid),
						Total
					],      
					listeners: 
					{
						activate: function()
						{
							Ext.getCmp('btnHapusBaris_LPJ').setVisible(false);
							Ext.getCmp('btnTambahBaris_LPJ').setVisible(false);
							// Ext.getCmp('txtNoteItem_LPJ').setVisible(false);
						}												
					}						
				},
				/* {  	
					title: 'Detail Rincian' ,
					id:'tabDtlRincian_LPJ',
					frame: false,
					border: false,					
					items:
					[
						NoteItem,
						GetGridDTLItem_LPJ(HighGrid),
						TotalItem
					],      
					listeners: 
					{
						activate: function()
						{
							if(cellSelectedDet_LPJ == undefined || cellSelectedDet_LPJ == '')
							{
								dsDTLItem_LPJ.removeAll();
								Ext.getCmp('txtNoteItem_LPJ').setValue('');
								Ext.getCmp('txtTotalItem_LPJ').setValue('');
								alert('Detail Realisasi belum dipilih');

							}else
							{
								Ext.getCmp('btnHapusBaris_LPJ').setVisible(true);
								Ext.getCmp('btnTambahBaris_LPJ').setVisible(true);
								// cellSelectedDet_LPJ = undefined;
								// Ext.getCmp('txtNoteItem_LPJ').setVisible(true);
								CalcTotalItem_LPJ();								
							}
						},
						deactivate:function()
						{
							// RefreshDataDetail_LPJ(Ext.get('txtNo_LPJ').dom.value())
						}
					}
				} */
			],
			tbar: 
			[
			{
				text: 'Tambah Baris',
				id:'btnTambahBaris_LPJ',
				tooltip: 'Tambah Record Baru ',
				iconCls: 'add',				
				handler: function() 
				{ 
				  /*if (dsDTLTRList_LPJ.getCount() <= 0)
				  {
				   alert('LPJ Belum di isi');
				  }else{
					TambahBaris_LPJ();
					}*/
					TambahBarisKomp_LPJ();
				}
			}, ' ', 
			{
				text: 'Hapus Baris',
				id:'btnHapusBaris_LPJ',
				tooltip: 'Remove the selected item',
				iconCls: 'remove',
				handler: function()
				{
					if (dsDTLTRList_LPJ.getCount() > 0 )
					{						
						if (cellSelectedDet_LPJ != undefined)
						{
							if(CurrentTR_LPJ != undefined)
							{
								HapusBaris_LPJ();
							}
						}
						else
						{
							ShowPesanWarning_LPJ('Silahkan pilih dahulu baris yang akan dihapus','Hapus baris');
						}
					}
				}
			}
		] 
	}
);
		

	var Formload_LPJ = new Ext.Panel
	(
		{
			id: 'Formload_LPJ',
			region: 'center',
			width:'100%',
			anchor:'100%',
			layout: 'form',
			title: '',
			bodyStyle: 'padding:15px',
			border: true,
			bodyStyle: 'background:#FFFFFF;',
			shadhow: true,
			//iconCls: 'GL',
			items: [pnlTR_LPJ, GDtabDetail_LPJ]//,Total]							  

		}
	);
	
	//RefreshDataDetail_LPJ();
	return Formload_LPJ						
};
 ///---------------------------------------------------------------------------------------///
 
function Cetak_LPJ()
{
	var strKriteria;
	
	if (Ext.get('txtNo_LPJ').dom.value !='' && Ext.get('dtpTanggal_LPJ').dom.value !='')
	{
		strKriteria = 'cso_date=' + Ext.get('dtpTanggal_LPJ').dom.value;
		strKriteria += '##@@## cso_number=' + Ext.get('txtNo_LPJ').dom.value;
		strKriteria += '##@@##' + 1;		
		ShowReport('', '231009', strKriteria);	
	};
};
function GetParamPblpj()
{
	var x = ''
	x= Ext.get('txtNo_LPJ').dom.value +'##';
	x+= REFFIDLPJ_LPJ +'##';
	return x;
};

function GetParamlpj()
{
	var x = ''
	x= Ext.get('txtNo_LPJ').dom.value +'##';
	x+= REFFIDKBS_LPJ +'##';
	return x;
};

function call_pengembalian(strpreview)
{
	var totalkembali			=	Ext.num(0);
	totalkembali 				=   getAmountValidasi_LPJ(Ext.get('txtJml_LPJ').dom.value) - getAmountValidasi_LPJ(Ext.get('txtTotal_LPJ').dom.value);
	strketerangan_pengembalian 	=	Ext.get('txtCatatan_LPJ').getValue();
	console.log(strketerangan_pengembalian);
	
	Ext.Ajax.request ({
		url: baseURL + "index.php/keuangan/functionLPJ/cek_pengembalian",
		params: {
			no_referensi_lpj : Ext.get('txtNo_LPJ').getValue()
		},
		failure: function(o)
		{
			//ShowPesanError_kbs('Error menampilkan data PPD !', 'Error');
		},	
		success: function(o) 
		{   
			
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				console.log(cst.pesan);
				console.log(cst.kembali);
				Strno_pengembalian = cst.no_pengembalian;
				setFrmLookUp_PengembalianLPJ(Strno_pengembalian,strpreview,totalkembali,strketerangan_pengembalian);
				
			} 
		}
	});
// if (Ext.get('txtJml_LPJ').dom.value != '' && Ext.get('txtNo_LPJ').dom.value !='')
	/* Ext.Ajax.request
	 (
		{
			url: "./Module.mvc/ExecProc",
			params: 
			{
				UserID: 'Admin',
				ModuleID: 'cekPengembalian',
				params:	GetParamlpj()
			},
			success: function(o) 
			{
			    var cst = Ext.decode(o.responseText);
				
				if (cst.pesan != "")
				{
					Strno_pengembalian = cst.nomor
					strJumlah_pengembalian = cst.jumlah
				}
				strketerangan_pengembalian =Ext.get('txtCatatan_LPJ').getValue();
				   var totalkembali=Ext.num(0);
					totalkembali =   getAmountValidasi_LPJ(Ext.get('txtJml_LPJ').dom.value) - getAmountValidasi_LPJ(Ext.get('txtTotal_LPJ').dom.value);
					if (totalkembali > 0 )
					{
					 
					   if (strpreview =="simpan")
					    {
						if (totalkembali != cst.jumlah)
							{
								setFrmLookUp_PengembalianLPJ(Strno_pengembalian,strketerangan_pengembalian,totalkembali,strpreview)
							}
						} 
						else
						{
							setFrmLookUp_PengembalianLPJ(Strno_pengembalian,strketerangan_pengembalian,totalkembali,strpreview)
						}
					}
					else
					{
					 if (totalkembali = '0')
						 {
						  // alert('Detail Belum di isi');
						 }else{
						   alert('melebihi anggaran');
						 }
					}
				
			}

		}
		); */
} 

var gridDTLTR_LPJ='';
function GetDTLTRGrid_LPJ(HighGrid) 
{
	// grid untuk detail transaksi	
	var fldDetail = ['cso_number', 'cso_date', 'line', 'account', 'description', 'value', 'posted', 'namaaccount']
	
	dsDTLTRList_LPJ = new WebApp.DataStore({ fields: fldDetail })
	dsTmp_LPJ= new WebApp.DataStore({ fields: fldDetail })
	
	gridDTLTR_LPJ = new Ext.grid.EditorGridPanel
	(
		{
			// title: 'Detail ' + NamaForm_LPJ,
			stripeRows: false,//true,
			store: dsDTLTRList_LPJ,
			id:'Dttlgrid_LPJ',
			border: false,
			columnLines:true,
			frame:true,
			height:HighGrid,
			anchor: '100% 100%',			
			sm: new Ext.grid.CellSelectionModel//RowSelectionModel
			(
				{
					singleSelect: true,
					listeners: 
					{
						cellselect: function(sm, row, rec)
						{
							cellSelectedDet_LPJ =dsDTLTRList_LPJ.getAt(row);
							CurrentTR_LPJ.row = row;
							GetDataRincianItem_LPJ(cellSelectedDet_LPJ.data.cso_number, FormatDateReport(Ext.getCmp('dtpTanggal_LPJ').getValue()),cellSelectedDet_LPJ.data.account);
							//CurrentTRGL.data = cellSelectedGLDet;
						}
					}
				}
			),
			cm: TRDetailColumModel_LPJ()
			, viewConfig: 
			{
				forceFit: true
			}
		}
	);
			
	return gridDTLTR_LPJ;
};

var dsDTLItem_LPJ;
function GetGridDTLItem_LPJ(HighGrid) 
{
	var fldDetailItem = 
	[
		'CSO_NUMBER', 'CSO_DATE', 'ACI_CODE', 'ACI_DESCRIPTION','LINE', 'VALUE', 'NOTE','ACI_CODE_TEMP'
	]
	
	dsDTLItem_LPJ = new WebApp.DataStore({ fields: fldDetailItem })
	
	GridDTLItem_LPJ = new Ext.grid.EditorGridPanel
	(
		{			
			stripeRows: true,
			store: dsDTLItem_LPJ,
			id:'GridDTLItem_LPJ',
			border: false,
			columnLines:true,
			frame:true,
			height: HighGrid-55,// 162,//193,
			anchor: '100% 100%',			
			sm: new Ext.grid.CellSelectionModel
			(
				{
					singleSelect: true,
					listeners: 
					{
						cellselect: function(sm, row, rec)
						{
							cellSlctdDTLItem_LPJ =dsDTLItem_LPJ.getAt(row);
							CurrentDTLItem_LPJ.row = row;
						},
						'focus' : function()
						{
						}
					}
				}
			),
			cm: TRDetailColumModelItem_LPJ(), 
			viewConfig: 
			{
				forceFit: true
			},
			listeners:
			{
				'afterrender': function()
				{
					this.store.on("load", function()
						{
							CalcTotalItem_LPJ();
						} 
					);
					this.store.on("datachanged", function()
						{
							CalcTotalItem_LPJ();						
						}
					);
				},
				activate: function()
				{	
					if(cellSelectedDet_LPJ == undefined){
						ShowPesanWarning_LPJ("Daftar "+NamaForm_LPJ+" belum dipilih.", "Penerimaan Mahasiswa");
					}
				}
			}
		}
	);			
	return GridDTLItem_LPJ;
}

function TRDetailColumModelItem_LPJ() 
{
	return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{
				id: 'colcsonoItem_LPJ',
				name: 'colcsonoItem_LPJ',
				header: "cso number",
				dataIndex: 'CSO_NUMBER',
				hidden: true,
				anchor: '30%'
			},
			{
				id: 'colcsodateItem_LPJ',
				name: 'colcsodateItem_LPJ',
				header: "PERIODE",
				dataIndex: 'CSO_DATE',
				hidden: true,
				anchor: '30%'
			},
			{
				id: 'colkdItem_LPJ',
				name: 'colkdItem_LPJ',
				header: "Kode item",
				dataIndex: 'ACI_CODE',
				// anchor: '10%',
				width:40,
				// hidden: true,
				// 	renderer: function(v, params, record) 
				// 	{
				// 		return record.data.KD_KOMPONEN;
				// 	}
				editor: new Ext.form.TextField
				(
					{
						id:'fieldkdItem_LPJ',
						allowBlank: false,
						enableKeyEvents : true,
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									var p = new mRecordDTLItem_LPJ
									(
										{
											CSO_NUMBER: Ext.getCmp('txtNoPembayaran_LPJ').getValue(),
											CSO_DATE: Ext.getCmp('dtpTanggal_LPJ').getValue(),
											ACI_CODE: '',
											ACI_DESCRIPTION: '',
											LINE: cellSlctdDTLItem_LPJ.data.LINE,
											VALUE: cellSlctdDTLItem_LPJ.data.VALUE,
											NOTE: '',
											ACI_CODE_TEMP: cellSlctdDTLItem_LPJ.data.ACI_CODE_TEMP,
										}
									);
									var criteria='';
									if (Ext.get('fieldkdItem_LPJ').dom.value !=""){

										criteria+=" AND ACI_CODE like '" + Ext.get('fieldkdItem_LPJ').dom.value + "%' ";
									}
									FormLookUpItem_LPJ(criteria,dsDTLItem_LPJ,p,false,CurrentDTLItem_LPJ.row,true);
								} 
							}
						}
					}
				)
			},
			{
				id: 'colNamaItem_LPJ',
				name: 'colNamaItem_LPJ',
				header: "Description",
				dataIndex: 'ACI_DESCRIPTION',
				anchor: '30%',
				editor: new Ext.form.TextField
				(
					{
						id:'fieldANamaItem_LPJ',
						allowBlank: false,
						enableKeyEvents : true,
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									var p = new mRecordDTLItem_LPJ
									(
										{
											CSO_NUMBER: Ext.getCmp('txtNoPembayaran_LPJ').getValue(),
											CSO_DATE: Ext.getCmp('dtpTanggal_LPJ').getValue(),
											ACI_CODE: '',
											ACI_DESCRIPTION: '',
											LINE: cellSlctdDTLItem_LPJ.data.LINE,
											VALUE: cellSlctdDTLItem_LPJ.data.VALUE,
											NOTE: '',
											ACI_CODE_TEMP: cellSlctdDTLItem_LPJ.data.ACI_CODE_TEMP
										}
									);
									var criteria='';
									if (Ext.get('fieldANamaItem_LPJ').dom.value !=""){

										criteria+="AND ACI_DESCRIPTION like '" + Ext.get('fieldANamaItem_LPJ').dom.value + "%' ";
									}
									FormLookUpItem_LPJ(criteria,dsDTLItem_LPJ,p,false,CurrentDTLItem_LPJ.row,true);
								} 
							}
						}
					}
				)
			},
			{
				id: 'colKet_LPJ',
				name: 'colKet_LPJ',
				header: "Keterangan",
				dataIndex: 'NOTE',
				anchor: '30%',
				hidden:true,
/*				editor: new Ext.form.TextField
				(
					{
						id:'fieldKet_LPJ',
						allowBlank: true,
						enableKeyEvents : true,
						listeners: 
						{ 
							
						}
					}
				)*/
			},
			{
				id: 'colJumlahbyr_LPJ',				
				name: 'colJumlahbyr_LPJ',
				header: "Jumlah (Rp.)",
				anchor: '15%',
				dataIndex: 'VALUE', 
				align:'right',
				renderer: function(v, params, record) 
				{
					var str = "<div style='white-space:normal;padding:2px 10px 2px 2px;'>" + formatCurrency(record.data.VALUE) + "</div>";
					return str;
				},
				editor: new Ext.form.NumberField(
					{
						id:'fieldJumlahbyr_LPJ',
						allowBlank: false,
						enableKeyEvents : true,
						// readOnly: true,
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{									
									CalcTotalItem_LPJ(CurrentDTLItem_LPJ.row);
								}
							},
							'blur' : function()
							{									
								CalcTotalItem_LPJ(CurrentDTLItem_LPJ.row);
							}
						}
					}
				)
			},
			{
				id: 'colkdItemTemp_LPJ',
				name: 'colkdItemTemp_LPJ',
				header: "Kode Item",
				dataIndex: 'ACI_CODE_TEMP',
				anchor: '30%',
				hidden:true
			}, 
		]
	)
};

function CalcTotalItem_LPJ(idx)
{
    var total=Ext.num(0);
	var nilai=Ext.num(0);	
	for(var i=0;i < dsDTLItem_LPJ.getCount();i++)
	{
		nilai=dsDTLItem_LPJ.data.items[i].data.VALUE;
		// total += getNumber(nilai);
		total += nilai;
	}		
	Ext.get('txtTotalItem_LPJ').dom.value=formatCurrencyDec(total);
};

//end Tambahan by Eza

function INPUTDetailSP3D_LPJ(rowdata)
{		
	var strKriteria_LPJ
    strKriteria_LPJ = " where NO_SP3D_RKAT='" + no_sp3d + "' "
	/* dsDTLTRList_LPJ.load
	(
		{ 
			params: 
			{ 	
				Skip: 0,
				Take: 200,
				Sort: 'Line',
				Sortdir: 'ASC',
				target:'VIviewsp3d_lpj',
				param: strKriteria_LPJ
			}
		}
	);	
	ProsesCekTotalLPJ(selectAktivaLancar_LPJ,false) */
	
	Ext.Ajax.request ({
		url: baseURL + "index.php/keuangan/functionLPJ/getDetailPPD",
		params: {
			tahun_anggaran_ta		:	rowdata.tahun_anggaran_ta,
			kd_unit_kerja			:	rowdata.kd_unit_kerja,
			no_sp3d					:	rowdata.no_sp3d_rkat,
			tgl_sp3d				:	rowdata.tgl_sp3d_rkat,
			prioritas				:	rowdata.prioritas,
			deskripsi				:	Ext.getCmp('txtCatatan_LPJ').getValue(),
		},
		failure: function(o)
		{
			ShowPesanError_kbs('Error menampilkan data detail PPD !', 'Error');
		},	
		success: function(o) 
		{   
			dsDTLTRList_LPJ.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsDTLTRList_LPJ.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsDTLTRList_LPJ.add(recs);
				console.log(dsDTLTRList_LPJ.data);
				// insertToGridDetailEntryKBS(dsDTLTRList_kbs);
				gridDTLTR_LPJ.getView().refresh();
			} else {
				ShowPesanError_kbs('Gagal menampilkan data detail PPD', 'Error');
			};
		}
	});
	return dsDTLTRList_LPJ;
}

function RefreshDataDetail_LPJ(no_lpj)
{	
	Ext.Ajax.request ({
		url: baseURL + "index.php/keuangan/functionLPJ/getDetailLPJ",
		params: {
			no_lpj: no_lpj
		},
		failure: function(o)
		{
			ShowPesanError_LPJ('Error menampilkan data Realisasi !', 'Error');
		},	
		success: function(o) 
		{   
			dsDTLTRList_LPJ.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsDTLTRList_LPJ.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsDTLTRList_LPJ.add(recs);
			} else {
				ShowPesanError_LPJ('Gagal menampilkan data Realisasi', 'Error');
			};
		}
	});
	return dsDTLTRList_LPJ;
    /* var strKriteria_LPJ
    strKriteria_LPJ = "Where CD.CSO_Number ='" + Rowdata + "' ";	
	dsDTLTRList_LPJ.load
	(
		{ 
			params: 
			{ 	
				Skip: 0,
				Take: 200,
				Sort: 'Line',
				Sortdir: 'ASC',
				target:'viViewPenerimaanMhsDetail',
				param: strKriteria_LPJ
			}
		}
	);
	return dsDTLTRList_LPJ; */
}


function RefreshDataDTLItem_LPJ(cso_no,cso_date,acc)
{
    var strKriteriaDTLItem_LPJ;
    strKriteriaDTLItem_LPJ ="where CSO_NUMBER='"+ cso_no +"' AND CSO_DATE='"+ cso_date +"' AND ACCOUNT='"+ acc +"'"
	dsDTLItem_LPJ.load
	(
		{ 
			params: 
			{ 	
				Skip: 0,
				Take: 50,
				Sort: 'ACI_CODE',
				Sortdir: 'ASC',
				target:'ViewRincianItem',
				param: strKriteriaDTLItem_LPJ
			}
		}
	);
	return dsDTLItem_LPJ;
}

function Simpan_Pengembalian(Strno_pengembalian, strketerangan_pengembalian,strJumlah_pengembalian,strtgl_pengembalian)
{
   /* if (Strno_pengembalian == "") 
	{
		{ */
			Ext.Ajax.request
			 (
				{
					 url: baseURL + "index.php/keuangan/functionLPJ/savepengembalian",
					 params:  getParam_Pengembalian(Strno_pengembalian, strketerangan_pengembalian,strJumlah_pengembalian,strtgl_pengembalian), 
					 success: function(o) 
					 {
						
						    var cst = Ext.decode(o.responseText);
							if (cst.success === true)
							{
								ShowPesanInfo_LPJ('Data berhasil di simpan','simpan Data');
								Ext.getCmp('btnLookup').setDisabled(true);
							}
							else if (cst.success === false && cst.pesan === 0 )
							{
								ShowPesanWarning_LPJ('Data tidak berhasil di simpan ' +  cst.pesan ,'Edit Data');
							}
							else 
							{
								ShowPesanError_LPJ('Data tidak berhasil di simpan'  + cst.pesan ,'Edit Data');
							}
												
					}
				}
			)
		/* }
	} */
	/* else
	{
	{
			Ext.Ajax.request
			 (
				{
					 url: "./Datapool.mvc/UpdateDataObj",
					 params:  getParam_Pengembalian(Strno_pengembalian, strketerangan_pengembalian,strJumlah_pengembalian,strtgl_pengembalian), 
					 success: function(o) 
					 {
						
						    var cst = Ext.decode(o.responseText);
							if (cst.success === true)
							{
								ShowPesanInfo_LPJ('Data berhasil di Edit','simpan Data');
							}
							else if (cst.success === false && cst.pesan === 0 )
							{
								ShowPesanWarning_LPJ('Data tidak berhasil di simpan ' +  cst.pesan ,'Edit Data');
							}
							else {
								ShowPesanError_LPJ('Data tidak berhasil di simpan'  + cst.pesan ,'Edit Data');
							}
												
					}
				}
			)
		}
	
	
	} */
}

///---------------------------------------------------------------------------------------///
			
function TRDetailColumModel_LPJ() 
{
	return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(), 
			{
				id: 'Account_LPJ',
				name: 'Account_LPJ',
				header: "Account",
				dataIndex: 'account',
				sortable: false,
				anchor: '10%',
				editor: new Ext.form.TextField
				(
					{
						id:'fieldAcc_LPJ',
						allowBlank: false,
						enableKeyEvents : true,
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									var p = new mRecord_LPJ
									(
										{
											account: '',
											namaaccount: '',
											description: Ext.get('txtCatatan_LPJ').getValue(),
											value: '',								
											line:''
										}
									);
									FormLookupAccount(" Where left(A.Account,1) in ('1','2','3','5')  and A.Account like '" 
										+ Ext.get('fieldAcc_LPJ').dom.value + "%'  AND A.TYPE ='D'  ",dsDTLTRList_LPJ,p,true,'',false);
								} 
							}
						}
					}
				),
				width: 70
			}, 
			{
				id: 'Name_LPJ',
				name: 'Name_LPJ',
				header: "Nama Account",
				dataIndex: 'namaaccount',
				anchor: '30%',
				editor: new Ext.form.TextField
				(
					{
						id:'fieldAccName_LPJ',
						allowBlank: false,
						enableKeyEvents : true,
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									var p = new mRecord_LPJ
									(
										{
											account: '',
											namaaccount: '',
											description: Ext.get('txtCatatan_LPJ').getValue(),		
											value: '',								
											line:''
										}
									);
									FormLookupAccount(" Where left(A.Account,1) in ('1','2','3','5')  and A.Name like '%" 
										+ Ext.get('fieldAccName_LPJ').dom.value + "%' AND A.TYPE ='D' ",dsDTLTRList_LPJ,p,true,'',false);									
								} 
							}
						}
					}
				)
			}, 
			{
				id: 'Description_LPJ',
				name: 'Description_LPJ',
				header: "Keterangan",
				anchor: '30%',
				dataIndex: 'description', 
				editor: new Ext.form.TextField
				(	
					{
						allowBlank: true
					}
				)
			}, 
			{
				id: 'Value_LPJ',				
				name: 'Value_LPJ',
				header: "Jumlah (Rp.)",
				anchor: '15%',
				dataIndex: 'value', 
				align:'right',
				renderer: function(v, params, record) 
				{
				  
					return formatCurrencyDec(record.data.value);					
				},	
				editor: new Ext.form.NumberField
				(
					{
						id:'fieldDB_LPJ',
						allowBlank: false,
						enableKeyEvents : true,
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{									
									CalcTotal_LPJ(CurrentTR_LPJ.row);									
								}
							},
							'blur' : function()
							{									
								CalcTotal_LPJ(CurrentTR_LPJ.row);									
							}
						}
					}
				)
			}
		]
	)
};

///---------------------------------------------------------------------------------------///
function DataInit_LPJ(rowdata) 
{
	console.log(rowdata);
	DataAddNew_LPJ = false;
	Ext.get('comboUnitKerja_LPJ').dom.value = rowdata.nama_unit_kerja;	
	selectUnitKerja_LPJ = rowdata.kd_unit_kerja;
	
	Ext.get('comboBayar_LPJ').dom.value= rowdata.payment;
	selectBayar_LPJ = rowdata.pay_code;
	
	Ext.get('comboAktivaLancar_LPJ').dom.value= rowdata.namaaccount;
	selectAktivaLancar_LPJ = rowdata.account;

    Ext.get('txtNo_LPJ').dom.value = rowdata.cso_number;
	
    Ext.get('txtTerimaDari_LPJ').dom.value = rowdata.personal;
	Ext.get('dtpTanggal_LPJ').dom.value = ShowDate(rowdata.cso_date);   
	
	Ext.get('txtTotal_LPJ').dom.value = formatCurrencyDec(rowdata.amount);
	Ext.get('txtJml_LPJ').dom.value = formatCurrencyDec(rowdata.jumlah);
	Ext.get('txtCatatan_LPJ').dom.value = rowdata.notes;
	Ext.get('txtNoPembayaran_LPJ').dom.value = rowdata.pay_no;
	Ext.get('txtReferensi_LPJ').dom.value = rowdata.referensi;	
	Ext.get('txtPathfile_LPJ').dom.value = rowdata.filepath;
	Ext.getCmp('ChkApprove_LPJ').setValue(rowdata.approve);	
	RefreshDataDetail_LPJ(Ext.get('txtNo_LPJ').dom.value);
	REFFIDKBS_LPJ=rowdata.reff_id;
	
};

///---------------------------------------------------------------------------------------///
function AddNew_LPJ() 
{
	DataAddNew_LPJ = true;	
	sts_pengembalian= false;
	Ext.get('txtNo_LPJ').dom.value = '';	
	Ext.getCmp('ChkApprove_LPJ').setValue(false);	
	Ext.get('txtTerimaDari_LPJ').dom.value = '';
	Ext.get('txtNoPembayaran_LPJ').dom.value = '';
	Ext.get('txtTotal_LPJ').dom.value = '0';
	Ext.get('txtCatatan_LPJ').dom.value = '';	
	Ext.get('comboUnitKerja_LPJ').dom.value='';
	Ext.get('comboBayar_LPJ').dom.value='';
	Ext.get('comboAktivaLancar_LPJ').dom.value = '';
	Ext.get('txtReferensi_LPJ').dom.value = '';	
	// Ext.get('txtAliasReferensi_LPJ').dom.value = '';	
	Ext.get('txtJml_LPJ').dom.value = '0';	
	// Ext.get('dtpTanggal_LPJ').dom.value = ShowDate(now_LPJ);
	Ext.get('txtPathfile_LPJ').dom.value = '';	
	
	dsDTLTRList_LPJ.removeAll();
	selectUnitKerja_LPJ='';
	selectBayar_LPJ='';
	selectAktivaLancar_LPJ='';
	rowSelected_LPJ=undefined;
	cellSelectedDet_LPJ = undefined;

	ButtonDisabled_LPJ(false);
	Ext.getCmp('btnLookup').setDisabled(false);

	// dsDTLItem_LPJ.removeAll();
	Ext.getCmp('txtNoteItem_LPJ').setValue('');
	Ext.getCmp('btnHapusBaris_LPJ').setVisible(false);
	Ext.getCmp('btnTambahBaris_LPJ').setVisible(false);
};
function getParam_Pengembalian(Strno_pengembalian, strketerangan_pengembalian,strJumlah_pengembalian,strtgl_pengembalian)
{
	var params = 
	{
		Table: 'viACC_CSO_LPJ',
		CSO_Number:Strno_pengembalian,
		CSO_Date:strtgl_pengembalian,
		Unit_Kerja:selectUnitKerja_LPJ,
		Personal:Ext.get('txtTerimaDari_LPJ').getValue(),
		Account:selectAktivaLancar_LPJ,
		Pay_Code:selectBayar_LPJ,
		Pay_No:Ext.get('txtNoPembayaran_LPJ').getValue(),
		Currency:'Rp',
		Kurs:'1',
		Type:'0',  //0=penerimaan
		GC_Code:'0',  //0=penerimaan
		Kd_User:'0',
		Amount:strJumlah_pengembalian,
		Notes1:strketerangan_pengembalian,		
		KD_UNIT_KERJA:selectUnitKerja_LPJ,		
		REFERENSI:Ext.get('txtNo_LPJ').dom.value, 
		REFERENSI_LPJ:Ext.get('txtReferensi_LPJ').dom.value, 
		REFF_ID:REFFIDLPJ_LPJ, 
		IS_PENGEMBALIAN : 1,
		IS_DETAIL:0,
		KATEGORI:5,		
		IS_APPROVE:0,
		url:Ext.get('txtPathfile_LPJ').getValue(),
		//List:getArrDetail_Pengembalian(Strno_pengembalian, strketerangan_pengembalian,strJumlah_pengembalian,strtgl_pengembalian),	
		JmlField:4,		
		JmlList:1		
	};
	return params
};
function getArrDetail_Pengembalian(Strno_pengembalian, strketerangan_pengembalian,strJumlah_pengembalian,strtgl_pengembalian)
{
	var x='';
	
		var y='';
		var z='@@##$$@@';
		
		// if (DataAddNew_LPJ === true)
		
	    y = 'Line=' + "1"
		
		y += z + 'Account='+ dsDTLTRList_LPJ.data.items[0].data.ACCOUNT
		y += z + 'Value=' + strJumlah_pengembalian
		y += z + 'DESCRIPTION=' + strketerangan_pengembalian
		x += y 
	return x;
};
function getParam_LPJ() 
{
	var tmp_amount = getAmount_LPJ(Ext.get('txtTotal_LPJ').getValue());
	
	var params = 
	{
		Table: 'viACC_CSO_LPJ',   				
		CSO_Number:Ext.get('txtNo_LPJ').getValue(),
		CSO_Date:Ext.get('dtpTanggal_LPJ').getValue(),
		Unit_Kerja:selectUnitKerja_LPJ,
		Personal:Ext.get('txtTerimaDari_LPJ').getValue(),
		Account:selectAktivaLancar_LPJ,
		Pay_Code:selectBayar_LPJ,
		Pay_No:Ext.get('txtNoPembayaran_LPJ').getValue(),
		Currency:'Rp',
		Kurs:'1',
		Type:'1',  //0=penerimaan // 1 = pengeluaran
		GC_Code:'0',  //0=penerimaan
		Kd_User:'0',
		// Amount:getAmount_LPJ(Ext.get('txtTotal_LPJ').getValue()),
		Amount: tmp_amount.substring(0,tmp_amount.length-3), //menghilangkan ,00
		Notes1:Ext.get('txtCatatan_LPJ').getValue(),		
		//KD_UNIT_KERJA:Ext.getCmp('comboUnitKerja_LPJ').getValue(),
		KD_UNIT_KERJA:selectUnitKerja_LPJ,
		REFERENSI:Ext.get('txtReferensi_LPJ').dom.value, 
		KATEGORI:4,
		REFF_ID:REFFIDKBS_LPJ,
		IS_DETAIL:0,
		IS_APPROVE:0,
		url:Ext.get('txtPathfile_LPJ').dom.value, 
		List:getArrDetail_LPJ(),
		JmlField:4,		
		JmlList:dsDTLTRList_LPJ.getCount(),
		// ListItem:getArrDetailItem_LPJ(),
		JmlFieldItem:7,		
		// JmlListItem:dsDTLItem_LPJ.getCount(),
		IS_ITEM:0
	};
	
	params['jumlah']=dsDTLTRList_LPJ.getCount()
	for (var L=0; L<dsDTLTRList_LPJ.getCount(); L++)
	{
		var tmp_value = dsDTLTRList_LPJ.data.items[L].data.value; //menghilangkan ,00
		params['account_detail'+L]		=dsDTLTRList_LPJ.data.items[L].data.account;
		params['name_detail'+L]			=dsDTLTRList_LPJ.data.items[L].data.name;
		params['description_detail'+L]	=dsDTLTRList_LPJ.data.items[L].data.description;
		params['value'+L]				= tmp_value,
		params['line'+L]				=dsDTLTRList_LPJ.data.items[L].data.line;
		params['kd_unit_kerja'+L]		=selectUnitKerja_LPJ;
	}
	return params
};

function getParamApprove_LPJ(TglApprove, NoteApprove) 
{	
	var params = 
	{
		IS_APPROVE:1,
		IS_BATAL:0,
		Date_Tag:TglApprove,
		note_tag:NoteApprove,
		CSO_Number:Ext.get('txtNo_LPJ').getValue(),
		CSO_Date:Ext.get('dtpTanggal_LPJ').getValue(),
		Unit_Kerja:selectUnitKerja_LPJ,
		Personal:Ext.get('txtTerimaDari_LPJ').getValue(),
		Account:selectAktivaLancar_LPJ,
		Pay_Code:selectBayar_LPJ,
		Pay_No:Ext.get('txtNoPembayaran_LPJ').getValue(),
		Currency:'Rp',
		Kurs:'1',
		Type:'1', //0=penerimaan // 1 = pengeluaran
		GC_Code:'0', //0=penerimaan
		Kd_User:'0',
		Amount:getAmount_LPJ(Ext.get('txtTotal_LPJ').getValue()),
		Notes1:Ext.get('txtCatatan_LPJ').getValue(),
		KD_UNIT_KERJA:selectUnitKerja_LPJ,//Ext.getCmp('comboUnitKerja_kbs').getValue(),
		REFERENSI:Ext.get('txtReferensi_LPJ').dom.value,
		KATEGORI:4, //4 => LPJ
		JmlList:dsDTLTRList_LPJ.getCount()
	};
	
	params['jumlah']=dsDTLTRList_LPJ.getCount()
	for (var L=0; L<dsDTLTRList_LPJ.getCount(); L++)
	{
		var tmp_value = dsDTLTRList_LPJ.data.items[L].data.value; //menghilangkan ,00
		params['account_detail'+L]		=dsDTLTRList_LPJ.data.items[L].data.account;
		params['name_detail'+L]			=dsDTLTRList_LPJ.data.items[L].data.name;
		params['description_detail'+L]	=dsDTLTRList_LPJ.data.items[L].data.description;
		params['value'+L]				= tmp_value.substring(0,tmp_value.length-3),
		params['line'+L]				=dsDTLTRList_LPJ.data.items[L].data.line;
		params['kd_unit_kerja'+L]		=selectUnitKerja_LPJ;
	}
	return params
};
function getParamApprove_Pengembalian(TglApprove, NoteApprove,Strno_pengembalian, strketerangan_pengembalian,strJumlah_pengembalian) 
{	
	var params = 
	{
		Table: 'viACC_CSO_LPJ',   		
		IS_APPROVE:1,
		CSO_Number:Strno_pengembalian,
		CSO_Date:Ext.get('dtpTanggal_LPJ').getValue(),
		No_Tag:Strno_pengembalian, 
		Account:selectAktivaLancar_LPJ,
		Date_Tag:TglApprove,
		Amount:strJumlah_pengembalian,		
		note_tag:NoteApprove,
		IS_DEBITinTotal:0,
		Description:strketerangan_pengembalian,
		url:Ext.get('txtPathfile_LPJ').dom.value, 
		IS_DETAIL:1,
		KD_UNIT_KERJA:selectUnitKerja_LPJ,
		REFERENSI:Ext.get('txtReferensi_LPJ').dom.value,
		REFF_ID:REFFIDLPJ_LPJ,
		KATEGORI:KDkategori_PENGEMBALIAN,
		List:getArrDetail_Pengembalian(Strno_pengembalian, strketerangan_pengembalian,strJumlah_pengembalian,TglApprove),	
		JmlField:4,		
		JmlList:1				
	};
	return params
};

///---------------------------------------------------------------------------------------///

function getAmount_LPJ(dblNilai)
{
    var dblAmount;
    dblAmount = dblNilai.replace('Rp.', '')
    for (var i = 0; i < dblAmount.length; i++) {
        var x = dblAmount.substr(i, 1)
        if (x === '.') {
            dblAmount = dblAmount.replace('.', '');
        }
    }    
    return dblAmount;
};

function getAmountValidasi_LPJ(dblNilai)
{
    var dblAmount;
    dblAmount = dblNilai.replace('Rp.', '')
    for (var i = 0; i < dblAmount.length; i++) {
        var x = dblAmount.substr(i, 1)
        if (x === '.') {
            dblAmount = dblAmount.replace('.', '');
        }
    }    
    return dblAmount.replace(',', '.');;
};

function getItemPanelInput_LPJ(lebar)
{
	var items = 
	{
		layout:'fit',
		anchor:'100%',
		width: lebar - 36,
		height: 200,//130,
		labelAlign: 'right',
		bodyStyle: 'padding:7px 7px 7px 7px',
		items:
		[
			{
				columnWidth:.9,
				width:lebar-36,
				layout: 'form',
				border:false,
				items: 
				[
					getItemPanelNo_LPJ(lebar),
					getItemPanelTerimaDari_LPJ(lebar),
					getItemPanelAktivaLancar_LPJ(lebar),
					getItemPanelPayModePayNumber_LPJ(lebar),
					getItemPanelCatatan_LPJ(lebar)
				]
			}
		]
	};
	return items;			
};

function getItemPanelAktivaLancar_LPJ(lebar)
{
	var items = 			
	{
	layout:'column',
	border:false,
	width:lebar-36,
	items:
		[
			{
				// columnWidth:0.98,
				columnWidth:.5,
				layout: 'form',
				border:false,
				labelWidth:111,
				items: 
				[
					mcomboAktivaLancar_LPJ(),					
				]
			},
			{
				columnWidth:.5,
				region:'Right',
				border:false,				
				layout: 'form',
				items: 				
				[mcomboUnitKerja_LPJ()]
			}
		]
	}
	return items;	
}; 

function getItemPanelCatatan_LPJ(lebar)
{
	var items = 			
	{
	layout:'column',
	border:false,
	width:lebar-36,
	items:
		[
			{
				columnWidth:0.98,
				layout: 'form',
				border:false,
				labelWidth:111,
				items: 
				[
					{
						xtype: 'compositefield',
						fieldLabel: 'Referensi ',
						items: 
						[
							{
								xtype:'textfield',
								fieldLabel: '',
								name: 'txtReferensi_LPJ',
								id: 'txtReferensi_LPJ',
								readOnly: true,
								//anchor:'49%',
								width:217,
								listeners:
								{ 
									focus: function() 
									{
										mCtrFokus="txtReferensi_LPJ";
									},
									'specialkey': function()
									{
										if(Ext.EventObject.getKey() === 13)
										{
											var StrKriteria=""											
											if (selectUnitKerja_LPJ != "")
											{
												StrKriteria = " WHERE AC.KATEGORI='5' and APP_SP3D = 1  and TAHAP_PROSES in(3,4)  AND SP3.KD_UNIT_KERJA='" + selectUnitKerja_LPJ + "' "							
											}
											else
											{
												StrKriteria = " WHERE AC.KATEGORI='5' and APP_SP3D = 1 and TAHAP_PROSES in(3,4) "
											}							
											var p = new mRecord_LPJ
											(
												{
													ACCOUNT: '',
													NAMAACCOUNT: '',
													DESCRIPTION: Ext.get('txtCatatan_LPJ').getValue(),
													VALUE: '',								
													LINE:''
												}
											);							
											FormLookupsp3d(dsDTLTRList_LPJ,p, StrKriteria,nASALFORM_LPJ,thn);
										}
									}
									
								}
							},
							{
								xtype:'spacer',
								width:50
							},
							{
								xtype:'displayfield',
								value:'Jml. PPD :'
							},
							{
								xtype:'textfield',
								fieldLabel: '',
								name: 'txtJml_LPJ',
								id: 'txtJml_LPJ',
								readOnly:true,
								width:230,
								style:
								{	
									'text-align':'right',
									'font-weight':'bold'
								}
							}
						]
					},
					{
						xtype:'textfield',
						fieldLabel: 'Keterangan ',
						name: 'txtCatatan_LPJ',
						id: 'txtCatatan_LPJ',
						autoCreate: {tag: 'input', type: 'text', size: '', autocomplete: 'off', maxlength: '350'},
						//maxlength: 20,
						//enforceMaxLength: true,
						//msgTarget: 'under',
						anchor:'99.9%'
					},
					{
						xtype: 'compositefield',
						fieldLabel: 'Path file :',
						anchor: '100%',
						labelSeparator: '',
						name: 'compPathfile_LPJ',
						id: 'compPathfile_LPJ',
						items: 
						[
							{
								xtype: 'textfield',
								flex: 1,
								fieldLabel: '',
								width: 480,
								readOnly:true,
								name: 'txtPathfile_LPJ',
								id: 'txtPathfile_LPJ'
							},
							{
								xtype:'button',
								text:'Upload',
								tooltip: 'Upload',
								id:'btnuplfile_LPJ',
								name:'btnuplfile_LPJ',
								handler:function()
								{
									FormLookupUploadfile(1,'uploads')
								}
							},
							{
								xtype:'button',
								text:'View',
								tooltip: 'View',
								id:'btnvwfile_LPJ',
								name:'btnvwfile_LPJ',
								handler:function()
								{ViewFile_LPJ()}
							}				
						]
					},
				]
			}
		]
	}
	return items;	
}; 



function getItemPanelNo_LPJ(lebar)
{
	var items = 			
	{
	layout:'column',
	border:false,
	width:lebar-36,
	items:
		[
			{
				columnWidth:.5,
				layout: 'form',
				border:false,
				labelWidth:111,
				items: 
				[
					{
						xtype:'textfield',
						fieldLabel: 'No. ' +  NamaForm_LPJ,
						name: 'txtNo_LPJ',
						id: 'txtNo_LPJ',
						readOnly:true,
						anchor:'95%'
					}
				]
			},
			{
				columnWidth:.5,
				layout: 'form',
				border:false,
				labelWidth:100,
				items: 
				[
					{
						xtype:'checkbox',
						fieldLabel: 'Approve',
						name: 'ChkApprove_LPJ',
						id: 'ChkApprove_LPJ',
						disabled:true,
						anchor:'95%'
					}
				]
			}
		]
	}
	return items;	
};

function getItemPanelPayModePayNumber_LPJ(lebar)
{
	var items = 			
	{
	layout:'column',
	width:lebar-36,
	border:false,
	items:
	[
		{
			columnWidth:.5,
			layout: 'form',
			border:false,
			labelWidth:111,
			items: 
			[mcomboBayar_LPJ()]			
		},
		{
			columnWidth:.5,
			layout: 'form',
			border:false,
			items: 
			[
				{
					xtype:'textfield',
					fieldLabel: 'No. Pembayaran ',
					name: 'txtNoPembayaran_LPJ',
					id: 'txtNoPembayaran_LPJ',
					anchor:'96%'						
				}
			]
		}
	]
	}
	return items;	
}

function getItemPanelTerimaDari_LPJ(lebar)
{
	var items = 			
	{
	layout:'column',
	width:lebar-36,
	border:false,
	items:
		[
			{
				columnWidth:.5,
				layout: 'form',
				border:false,
				labelWidth:111,
				items: 
				[
					{
						xtype:'textfield',
						fieldLabel: 'Dibayarkan Kepada',
						name: 'txtTerimaDari_LPJ',
						id: 'txtTerimaDari_LPJ',
						anchor:'95%'
					}
				]
			},
			{
				columnWidth:.5,
				layout: 'form',
				border:false,
				items: 
				[
					{
						xtype: 'datefield',
                        fieldLabel: 'Tanggal ',
                        id: 'dtpTanggal_LPJ',
                        name: 'dtpTanggal_LPJ',
                        format: 'd/M/Y',
						value:now_LPJ,
                        anchor: '70%'
					}
				]
			}
		]
	}
	return items;	
}

function mcomboUnitKerja_LPJ()
{
	var Field = ['kd_unit', 'nama_unit', 'unitkerja'];
	dsUnitKerja_LPJ = new WebApp.DataStore({ fields: Field });
	
  var comboUnitKerja_LPJ = new Ext.form.ComboBox
	(
		{
			id:'comboUnitKerja_LPJ',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih Unit Kerja...',
			fieldLabel: 'Unit Kerja ',			
			align:'Right',
			anchor:'96%',
			store: dsUnitKerja_LPJ,
			valueField: 'kd_unit',
			displayField: 'nama_unit',
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectUnitKerja_LPJ=b.data.kd_unit ;
				} 
			}
		}
	);
	
	/* dsUnitKerja_LPJ.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'KD_UNIT_KERJA',
			    Sortdir: 'ASC',
			    target: 'viCboUnitKerja',
			    // param: gstrListUnitKerja + "##@@##" + 0
			    // param: "kdunit="+ gstrListUnitKerja
			    param: ""
			}
		}
	); */
	
	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionPlafondUnitKerja/getUnitKerjaInput",
		params: {
			text:''
		},
		failure: function(o)
		{
			ShowPesanError_LPJ('Error menampilkan data unit kerja !', 'Error');
		},	
		success: function(o) 
		{   
			dsUnitKerja_LPJ.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsUnitKerja_LPJ.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsUnitKerja_LPJ.add(recs);
				// gridDTLTR_PaguGNRL.getView().refresh();
			} else {
				ShowPesanError_LPJ('Gagal menampilkan data unit kerja', 'Error');
			};
		}
	});
	return comboUnitKerja_LPJ;
} ;

function mcomboAktivaLancar_LPJ()
{
	var Field = ['account','name','groups','akun'];
	dsAktivaLancarLPJ = new WebApp.DataStore({ fields: Field });
	/* dsAktivaLancarLPJ.load
	(
		{
			params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: '',
			    Sortdir: 'ASC',
			    target: 'viewCboaktivaFilter',
			    param: 'BPK'
			}
		}
	); */
	Ext.Ajax.request({
		url: baseURL + "index.php/keuangan/functionKBS/getAktivaLancar",
		params: {query:''},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			comboAktivaLancar_LPJ.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsAktivaLancarLPJ.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsAktivaLancarLPJ.add(recs);
			}
		}
	});
 var comboAktivaLancar_LPJ = new Ext.form.ComboBox
	(
		{
			id:'comboAktivaLancar_LPJ',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih Kas / Bank...',
			fieldLabel: 'Kas / Bank ',			
			align:'Right',
			anchor:'95%',
			store: dsAktivaLancarLPJ,
			valueField: 'account',
			displayField: 'akun',
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectAktivaLancar_LPJ=b.data.account ;
					ProsesCekTotalLPJ(b.data.Account,true)
				} 
			}
		}
	);
	
	return comboAktivaLancar_LPJ;
} ;

function RefreshData_LPJ()
{	
	dsTRList_LPJ.load
	(
		{ 
			params: 
			{ 	
				Skip: 0,
				Take: selectCount_LPJ,
				Sort: 'CSO_DATE',
				Sortdir: 'ASC',
				target:'viviewBPK',
				param: " Where KATEGORI='" + KDkategori_LPJ + "' AND U.KD_UNIT_KERJA IN ("+gstrListUnitKerja+")" 
			}
		}
	);
	return dsTRList_LPJ;
};

// function mComboMaksData_LPJ()
// {
//   var cboMaksDataLPJ = new Ext.form.ComboBox
// 	(
// 		{
// 			id:'cboMaksDataLPJ',
// 			typeAhead: true,
// 			triggerAction: 'all',
// 			lazyRender:true,
// 			mode: 'local',
// 			emptyText:'',
// 			fieldLabel: 'Maks.Data ',			
// 			width:50,
// 			store: new Ext.data.ArrayStore
// 			(
// 				{
// 					id: 0,
// 					fields: 
// 					[
// 						'Id',
// 						'displayText'
// 					],
// 				data: [[1, 50], [2, 100],[3, 200],[4, 500],[5, 1000]]
// 				}
// 			),
// 			valueField: 'Id',
// 			displayField: 'displayText',
// 			value:selectCount_LPJ,
// 			listeners:  
// 			{
// 				'select': function(a,b,c)
// 				{   
// 					selectCount_LPJ=b.data.displayText ;
// 					RefreshDataFilter_LPJ(false);
// 				} 
// 			}
// 		}
// 	);
// 	return cboMaksDataLPJ;
// };

function GetCriteriaGridUtama(){
	var criteria = '';
	var criteria_approved = '';
	var criteria_lpj = '';
	var criteria_tgl = '';
	
	
	if(Ext.getCmp('chkFilterApprovedLPJ').getValue() == true){
		criteria_approved = " and a.no_tag is not null and a. date_tag is not null ";
	}else{
		criteria_approved = " and a.no_tag isnull and a. date_tag isnull ";
		
	}
	if (Ext.getCmp('txtNoFilter_LPJ').getValue() != ''){
		criteria_lpj = " and a.cso_number ='"+ Ext.getCmp('txtNoFilter_LPJ').getValue() +"' ";
	}

	if (Ext.getCmp('chkWithTgl_LPJ').getValue() == true){
		criteria_tgl = " and a.cso_date between '"+ Ext.get('dtpTglAwalFilter_LPJ').getValue() +"'  and '"+ Ext.get('dtpTglAkhirFilter_LPJ').getValue() +"' ";
	}

	criteria = " "+criteria_approved+" "+criteria_tgl+" "+criteria_lpj+" ";
	
	if (Ext.getCmp('comboUnitKerja_LPJView').getValue() == '000' || Ext.getCmp('comboUnitKerja_LPJView').getValue() == 000){
		criteria = "SEMUA~"+criteria;
		
	}else{
		criteria = "  and a.kd_unit_kerja = '"+Ext.getCmp('comboUnitKerja_LPJView').getValue()+"' "+criteria;
	}
	
	return criteria;
}

function RefreshDataFilter_LPJ(mBol) 
{   
	var criteria = GetCriteriaGridUtama();
	dsTRList_LPJ.removeAll();
	dsTRList_LPJ.load({
		params:{
			Skip: 0,
			Take: selectCount_LPJ,
			Sort: '',
			Sortdir: 'ASC',
			target: 'vi_viewdata_lpj',
			param: criteria
		},
		callback:function(){
			var grListGridTR_lpj=Ext.getCmp('grListTR_LPJ').getStore().data.items;
				for(var i=0,iLen=grListGridTR_lpj.length; i<iLen;i++){
					
					if(grListGridTR_lpj[i].data.approve == 'true'){
						grListGridTR_lpj[i].data.approve_tmp = true;
						
					} 
				}
			Ext.getCmp('grListTR_LPJ').getView().refresh();
		}
	}); 
	
	/* var strLPJ='';
	strLPJ=" Where KATEGORI='" + KDkategori_LPJ + "' "
	if (Ext.get('txtNoFilter_LPJ').getValue() != '') 
	{
		if (strLPJ ==='')
		{
			strLPJ = " Where CS.CSO_NUMBER like '%" + Ext.get('txtNoFilter_LPJ').getValue() + "%' ";
		}
		else
		{
			strLPJ += " AND CS.CSO_NUMBER like '%" + Ext.get('txtNoFilter_LPJ').getValue() + "%' ";
		}
	};
	if (Ext.getCmp('comboUnitKerja_LPJView').getValue() != '') 
	{
		if (strLPJ ==='')
		{
			strLPJ = " Where CS.KD_UNIT_KERJA ='" + Ext.getCmp('comboUnitKerja_LPJView').getValue()  + "' ";
		}
		else
		{
			strLPJ += " AND CS.KD_UNIT_KERJA ='" + Ext.getCmp('comboUnitKerja_LPJView').getValue()  + "' ";
		}
	};
	
	if (Ext.getCmp('chkWithTgl_LPJ').getValue() === true) 
	{
		if (strLPJ ==='')
		{
			strLPJ = " Where CS.CSO_DATE >='" + FormatDateReport(Ext.getCmp('dtpTglAwalFilter_LPJ').getValue())  + "' ";
			strLPJ += " and  CS.CSO_DATE <='" + FormatDateReport(Ext.getCmp('dtpTglAkhirFilter_LPJ').getValue())  + "' ";
		}
		else
		{
			strLPJ += " and (CS.CSO_DATE >='" + FormatDateReport(Ext.getCmp('dtpTglAwalFilter_LPJ').getValue())  + "' ";
			strLPJ += " and  CS.CSO_DATE <='" + FormatDateReport(Ext.getCmp('dtpTglAkhirFilter_LPJ').getValue())  + "') ";
		}
	};	
     
	if(Ext.getCmp('chkFilterApprovedLPJ').getValue() === true)
		{
			if(strLPJ != "")
			{
				strLPJ += " and LEN(CS.NO_TAG) <> 0 ";
			}
			else
			{
				strLPJ = " Where LEN(CS.NO_TAG) <> 0 ";
			}
			
		}else{
			if(strLPJ != "")
			{
				strLPJ += " and LEN(CS.NO_TAG) is null  ";
			}
			else
			{
				strLPJ = " Where LEN(CS.NO_TAG) is null  ";
			}
		}	
		

		 // strLPJ+="and U.KD_UNIT_KERJA IN ("+gstrListUnitKerja+")"
		 strLPJ+=""

    if (strLPJ != undefined) 
    {  
		dsTRList_LPJ.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCount_LPJ, 
					Sort: 'CSO_DATE', 
					Sortdir: 'ASC', 
					target:'viviewBPK',
					param: strLPJ
				}			
			}
		);        
    }
	else
	{
	    RefreshDataFilter_LPJ(true);
	} */
};


function mcomboBayar_LPJ()
{
	var Field = ['pay_code', 'payment'];
	dsBayarLPJ = new WebApp.DataStore({ fields: Field });
	/* dsBayarLPJ.load
	(
		{
			params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: '',
			    Sortdir: 'ASC',
			    target: 'viCboJnsBayar',
			    param: ''
			}
		}
	); */
	
	Ext.Ajax.request({
		url: baseURL + "index.php/keuangan/functionGeneral/getJenisBayar",
		params: {query:''},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			comboBayar_LPJ.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsBayarLPJ.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsBayarLPJ.add(recs);
			}
		}
	});
  var comboBayar_LPJ = new Ext.form.ComboBox
	(
		{
			id:'comboBayar_LPJ',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih Jenis Pembayaran...',
			fieldLabel: 'Jenis Pembayaran ',			
			align:'Right',
			anchor:'95%',
			store: dsBayarLPJ,
			valueField: 'pay_code',
			displayField: 'payment',
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectBayar_LPJ=b.data.pay_code ;
				} 
			}
		}
	);
	
	return comboBayar_LPJ;
} ;

function Approve_LPJ(TglApprove, NoteApprove) 
{
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/keuangan/functionLPJ/approve",
			params:  getParamApprove_LPJ(TglApprove, NoteApprove), 
			success: function(o) 
			{				
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					Ext.getCmp('ChkApprove_LPJ').setValue(true)
					//ShowPesanInfo_LPJ('Data berhasil di Approve','Approve');
					RefreshDataFilter_LPJ(false);
					
					Ext.Ajax.request ({
						url: baseURL + "index.php/keuangan/functionLPJ/cek_pengembalian",
						params: {
							no_referensi_lpj : Ext.get('txtNo_LPJ').getValue()
						},
						failure: function(o)
						{
							//ShowPesanError_kbs('Error menampilkan data PPD !', 'Error');
						},	
						success: function(o) 
						{   
							
							var cst = Ext.decode(o.responseText);
							if (cst.success === true) {
								if(cst.kembali == true){
									Strno_pengembalian 			= cst.no_pengembalian;
									strketerangan_pengembalian	= '';
									strJumlah_pengembalian 		= cst.jumlah;
									Approve_Pengembalian(TglApprove, NoteApprove,Strno_pengembalian, strketerangan_pengembalian,strJumlah_pengembalian)
								}else{
									ShowPesanInfo_LPJ('Data berhasil di Approve','Approve'); // jika tidak ada pengembalian
								}
							} else{
								ShowPesanInfo_LPJ('Gagal cek pengembalian!','Approve'); // jika tidak ada pengembalian
							}
						}
					});
				
					RefreshDataFilter_LPJ(false);								
				}
				else if (cst.success === false && cst.pesan === 0 )
				{
					ShowPesanWarning_LPJ('Data tidak berhasil di Approve','Edit Data');
				}
				else 
				{
					ShowPesanError_LPJ('Data tidak berhasil di Approve, ' + cst.pesan,'Approve');
				}										
			}
		}
	)
}


function Approve_Pengembalian(TglApprove, NoteApprove,Strno_pengembalian, strketerangan_pengembalian,strJumlah_pengembalian) 
{
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/keuangan/functionLPJ/approve_pengembalian",
			params:  getParamApprove_Pengembalian(TglApprove, NoteApprove,Strno_pengembalian, strketerangan_pengembalian,strJumlah_pengembalian) , 
			success: function(o) 
			{				
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					ShowPesanInfo_LPJ('Data berhasil di Approve','Approve');
					RefreshDataFilter_LPJ(false);								
				}
				else 
				{
					ShowPesanError_LPJ('Data tidak berhasil di Approve, ' + cst.pesan ,'Approve');
				}										
			}
		}
	)
}

function Datasave_LPJ(IsExit) 
{
	if (ValidasiEntry_LPJ('Simpan Data') == 1 )
	{
		// if (DataAddNew_LPJ == true) 
		// {
			
		    Ext.Ajax.request
				(
					{
					    //url: "./Datapool.mvc/CreateDataObj",
						url: baseURL + "index.php/keuangan/functionLPJ/save",
					    params: getParam_LPJ(),
					    success: function(o) {
					        var cst = Ext.decode(o.responseText);

					        if (cst.success === true) 
							{
								if (IsExit===false)
								{
									Ext.get('txtNo_LPJ').dom.value = cst.CSO_Number;
								}
					            RefreshDataFilter_LPJ(false);
								RefreshDataDetail_LPJ(Ext.get('txtNo_LPJ').dom.value);		
								DataAddNew_LPJ=false;
								console.log(sts_pengembalian);
								// if (sts_pengembalian== true && REFFIDKBS_LPJ == '22')
								if (sts_pengembalian== true )
								{
									// alert('Datasave_LPJ save');
									call_pengembalian("simpan");
								}else
								{
									ShowPesanInfo_LPJ('Data berhasil di simpan', 'Simpan Data');
									// Ext.getCmp('btnLookup').setDisabled(true);
								}
					        }
					        else if (cst.success === false && cst.pesan === 0) 
							{
					            ShowPesanWarning_LPJ('Data tidak berhasil di simpan ' + cst.pesan , 'Simpan Data');
					        }
					        else {
					            ShowPesanError_LPJ('Data tidak berhasil di simpan, Error : ' + cst.pesan, 'Simpan Data');
					        }
					    }
					}
				)
		// }
		
	}
	else
	{
		/*Ext.getCmp('btnSimpan').setDisabled(false);knp di disable, lupa lagi
		Ext.getCmp('btnSimpanKeluar').setDisabled(false);														*/
	}
};

function ValidasiEntry_LPJ(modul)
{
	var x = 1;
	console.log(selectAktivaLancar_LPJ);
	if(selectAktivaLancar_LPJ=='' || selectAktivaLancar_LPJ == undefined)
	{
		ShowPesanWarning_LPJ('Kas / Bank belum di isi',modul);
		x=0;
	}
	if (selectBayar_LPJ=='' || selectBayar_LPJ== undefined)
	{
		ShowPesanWarning_LPJ('Jenis pembayaran belum di isi',modul);
		x=0;
	};
	if (selectUnitKerja_LPJ=='' || selectUnitKerja_LPJ== undefined)
	{
		ShowPesanWarning_LPJ(gstrSatker+' belum di isi',modul);
		x=0;
	};
	if ( Ext.get('comboUnitKerja_LPJView').getValue() =='' )
	{
	
	ShowPesanWarning_LPJ(gstrSatker+' belum di isi',modul);
		x=0;
	};
	/*if (parseFloat(getAmountValidasi_LPJ(Ext.get('txtTotal_LPJ').getValue())) > parseFloat(getAmountValidasi_LPJ(Ext.get('txtJml_LPJ').getValue())))
	{
		ShowPesanWarning_LPJ('Melebihi anggaran '+gstrSp3d,modul);
		x=0;
	};*/

	if (Ext.get('txtReferensi_LPJ').getValue() == '' )
	{
		ShowPesanWarning_LPJ('Referensi belum di isi',modul);
		x=0;
	};
	if (Ext.get('txtTerimaDari_LPJ').getValue() == '' )
	{
		ShowPesanWarning_LPJ('Dibayarkan Kepada belum di isi',modul);
		x=0;
	};

	if (Ext.getCmp('txtCatatan_LPJ').getValue().length>350){
		ShowPesanWarning_LPJ("Keterangan Tidak boleh lebih dari 350 Karakter",modul);
		x=0;
	}

	if (getAmount_LPJ(Ext.get('txtTotal_LPJ').getValue()) > saldoAkunLPJ)
	{
		ShowPesanWarning_LPJ(' Total Melebihi Saldo Akun',modul);
		x=0;
	};

	/* if (dsDTLItem_LPJ.getCount() != 0 )
	{
		if (CekRowGridDetail_LPJ(modul) === 0 )
		{
			x=0;
		};		
		if (Ext.get('txtNoteItem_LPJ').getValue() == '' )
		{
			ShowPesanWarning_LPJ('Note belum di isi',modul);
			x=0;
		};
		if (parseFloat(getAmountValidasi_LPJ(Ext.get('txtTotal_LPJ').getValue())) != parseFloat(getAmountValidasi_LPJ(Ext.get('txtTotalItem_LPJ').getValue())))
		{
			ShowPesanWarning_LPJ('Total Detail Realisasi belum sama dengan Total Detail Rincian ',modul);
			x=0;
		};
	} */
	return x;
};

function CekRowGridDetail_LPJ(modul)
{
	var x=1;
	if (dsDTLItem_LPJ != undefined || dsDTLItem_LPJ!='')
	{
		for (var i = 0; i < dsDTLItem_LPJ.getCount(); i++) 
		{
			if (CekRowGridNimGanda_LPJ(dsDTLItem_LPJ.data.items[i].data.ACI_CODE) == 0)
			{
				x=0
				ShowPesanWarning_LPJ('Kode item '+ dsDTLItem_LPJ.data.items[i].data.ACI_DESCRIPTION +' ganda ', modul);
			}
		}
	}
	else
	{
		x=0;
	};
	return x;
};

function CekRowGridNimGanda_LPJ(acicode)
{
	var x='';
	for (var i = 0; i < dsDTLItem_LPJ.getCount(); i++) 
	{
		if (acicode == dsDTLItem_LPJ.data.items[i].data.ACI_CODE)
		{
			x += 1	
		}
	}
	if (x > 1)
	{  
		x=0
	}

	return x;
};
function ValidasiAppEntry_LPJ(modul)
{
	var x = 1;
	if(Ext.get('txtNo_LPJ').getValue()== '' )
	{
		ShowPesanWarning_LPJ('Nomor LPJ belum di isi',modul);
		x=0;
	}
	//if (getAmountValidasi_LPJ(Ext.get('txtTotal_LPJ').getValue()) > getAmountValidasi_LPJ(Ext.get('txtJml_LPJ').getValue()))
	if (parseFloat(getAmountValidasi_LPJ(Ext.get('txtTotal_LPJ').getValue())) > parseFloat(getAmountValidasi_LPJ(Ext.get('txtJml_LPJ').getValue())))
	{
		ShowPesanWarning_LPJ('Melebihi anggaran '+gstrSp3d,modul);
		x=0;
	};
	return x;
};

function ShowPesanWarning_LPJ(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};

function ShowPesanError_LPJ(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR
		}
	);
};

function ShowPesanInfo_LPJ(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO
		}
	);
};

function HapusBaris_LPJ()
{
	if (cellSelectedDet_LPJ.data.Account != '')
	{
		Ext.Msg.show
		(
			{
			   title:'Hapus Baris',
			   msg: 'Apakah baris ini akan dihapus ?' + ' ' + 'Baris :'+ ' ' + (CurrentTR_LPJ.row + 1) + ' dengan Account : '+ ' ' + cellSelectedDet_LPJ.data.ACCOUNT ,
			   buttons: Ext.MessageBox.YESNO,
			   fn: function (btn) 
			   {			
				   if (btn =='yes') 
					{
					    dsDTLTRList_LPJ.removeAt(CurrentTR_LPJ.row);
					    CalcTotal_LPJ();
						HapusBaris_LPJDB(cellSelectedDet_LPJ.data.LINE);
					    cellSelectedDet_LPJ = undefined;
					} 
			   },
			   icon: Ext.MessageBox.QUESTION
			}
		);
	}
	else
	{
	    dsDTLTRList_LPJ.removeAt(CurrentTR_LPJ.row);
	    CalcTotal_LPJ();
	    cellSelectedDet_LPJ = undefined;
	}
	
	
};

function TambahBaris_LPJ()
{
	
	var p = new mRecord_LPJ
	(
		{
			ACCOUNT: '',
			NAMAACCOUNT: '',
			DESCRIPTION: Ext.get('txtCatatan_LPJ').getValue(),		
			VALUE: '',								
			LINE:''
		}
	);

	dsDTLTRList_LPJ.insert(dsDTLTRList_LPJ.getCount(), p);	 
	//this.startEditing(0, 1);
};

function TambahBarisKomp_LPJ()
{
	var p = new mRecordDTLItem_LPJ
	(
		{
			CSO_NUMBER: Ext.getCmp('txtNoPembayaran_LPJ').getValue(),
			CSO_DATE: Ext.getCmp('dtpTanggal_LPJ').getValue(),
			ACI_CODE: '',
			ACI_DESCRIPTION: '',
			LINE: '',
			VALUE: '',
			NOTE: '',
			ACI_CODE_TEMP: ''
		}
	);

	dsDTLItem_LPJ.insert(dsDTLItem_LPJ.getCount(), p);
	Ext.getCmp('GridDTLItem_LPJ').startEditing(dsDTLItem_LPJ.getCount()-1,1)
};

function DataDelete_LPJ() 
{
	if (ValidasiEntry_LPJ('Hapus Data') == 1 )
	{	
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/keuangan/functionLPJ/deleteLPJ",
				params:  getParam_LPJ(), 
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true)					
					{
							ShowPesanInfo_LPJ('Data berhasil di hapus','Hapus Data');
							RefreshDataFilter_LPJ(false);
							AddNew_LPJ();
					}
					else if (cst.success === false  )
					{
						ShowPesanWarning_LPJ(cst.pesan ,'Hapus Data');
					}
					
				}
			}
		)
	}
};

function HapusBaris_LPJ()
{
	if (cellSlctdDTLItem_LPJ.data.ACI_CODE != undefined || cellSlctdDTLItem_LPJ.data.ACI_CODE != '')
	{
		if (cellSlctdDTLItem_LPJ.data.ACI_CODE != '')
		{
			Ext.Msg.show
			(
				{
				   title:'Hapus Baris',
				   msg: 'Apakah baris ini akan dihapus ?' + ' ' + 'Baris :'+ ' ' + (CurrentTR_LPJ.row + 1) + ' dengan kode Item : '+ ' ' + cellSlctdDTLItem_LPJ.data.ACI_CODE ,
				   buttons: Ext.MessageBox.YESNO,
				   fn: function (btn) 
				   {
					   if (btn =='yes')
						{
							HapusBarisKomp_LPJDB(cellSlctdDTLItem_LPJ.data.LINE,cellSlctdDTLItem_LPJ.data.ACI_CODE);
							dsDTLItem_LPJ.removeAt(CurrentDTLItem_LPJ.row);
							cellSlctdDTLItem_LPJ = undefined;
						} 
				   },
				   icon: Ext.MessageBox.QUESTION
				}
			);
		}
		else
		{
			dsDTLItem_LPJ.removeAt(CurrentDTLItem_LPJ.row);
			CalcTotalItem_LPJ();
			cellSlctdDTLItem_LPJ = undefined;
		}
	}
};

function HapusBarisKomp_LPJDB(Line,aci_code) 
{	
	Ext.Ajax.request
	(
		{
			url: "./Datapool.mvc/DeleteDataObj",
			params: getParamItem_LPJHapusBaris(Line,aci_code), 
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);

				if (cst.success === true) 
				{
					RefreshDataFilter_LPJ(false);
					ShowPesanInfo_LPJ('Data berhasil dihapus','Hapus Baris')
				}
				else
				{
					ShowPesanWarning_LPJ(cst.pesan,'Hapus Baris')
				}
				CalcTotalItem_LPJ();
			}
		}
	)	
};

function getParamHpsItem_LPJ(Line,kdKomp) 
{	
	var params = 
	{
		Table: 'viACC_CSO',
		IS_DETAIL:1,
		IS_:0,
		CSO_Number:Ext.get('txtNo_LPJ').getValue(),
		CSO_Date:Ext.get('dtpTanggal_LPJ').getValue(),
		LINE:Line,
		ACI_CODE:kdKomp,
		Amount: getAmount_LPJ(Ext.get('txtTotal_LPJ').getValue())
				
	};
	return params
};

function getArrDetail_LPJ()
{
	var x='';
	for(var i = 0 ; i < dsDTLTRList_LPJ.getCount();i++)
	{
		var y='';
		var z='@@##$$@@';
		
		// if (DataAddNew_LPJ === true)
		if (dsDTLTRList_LPJ.data.items[i].data.LINE==="")
		{
			y = "Line=" + (i+1)
		}
		else
		{	
			y = 'Line=' + dsDTLTRList_LPJ.data.items[i].data.LINE
		}		
		y += z + 'Account='+dsDTLTRList_LPJ.data.items[i].data.ACCOUNT
		y += z + 'Value=' + dsDTLTRList_LPJ.data.items[i].data.VALUE
		y += z + 'DESCRIPTION=' + dsDTLTRList_LPJ.data.items[i].data.DESCRIPTION
		if (i === (dsDTLTRList_LPJ.getCount()-1))
		{
			x += y 
		}
		else
		{
			x += y + '##[[]]##'
		}
	}		
	return x;
};

function getArrDetailItem_LPJ()
{
	var x='';
	for(var i = 0 ; i < dsDTLItem_LPJ.getCount();i++)
	{
		var y='';
		var z='@@##$$@@';
		
		// if (DataAddNew_LPJ === true)
		if (dsDTLItem_LPJ.data.items[i].data.LINE==="")
		{
			y = "Line=" + (i+1)
		}
		else
		{	
			y = 'Line=' + dsDTLItem_LPJ.data.items[i].data.LINE
		}		
		y += z + 'CSO_NUMBER='+Ext.getCmp('txtNo_LPJ').getValue()
		y += z + 'CSO_DATE='+ ShowDate(Ext.getCmp('dtpTanggal_LPJ').getValue())
		y += z + 'ACI_CODE='+ dsDTLItem_LPJ.data.items[i].data.ACI_CODE
		y += z + 'VALUE=' + dsDTLItem_LPJ.data.items[i].data.VALUE
		y += z + 'NOTE=' + Ext.getCmp('txtNoteItem_LPJ').getValue()
		y += z + 'ACI_CODE_TEMP='+dsDTLItem_LPJ.data.items[i].data.ACI_CODE_TEMP
		if (i === (dsDTLItem_LPJ.getCount()-1))
		{
			x += y 
		}
		else
		{
			x += y + '##[[]]##'
		}
	}		
	return x;
};


function CalcTotal_LPJ(idx)
{
    var total=Ext.num(0);
	var nilai=Ext.num(0);			
	for(var i=0;i < dsDTLTRList_LPJ.getCount();i++)
	{
		nilai=dsDTLTRList_LPJ.data.items[i].data.value;				

		total += nilai;
	}	
	
		Ext.get('txtTotal_LPJ').dom.value=formatCurrencyDec(total);
	    var totalkembali=Ext.num(0);
		totalkembali =   parseFloat(getAmountValidasi_LPJ(Ext.get('txtJml_LPJ').dom.value)) - parseFloat(getAmountValidasi_LPJ(Ext.get('txtTotal_LPJ').dom.value));
		console.log(totalkembali);
		/* if(totalkembali == 0){
			console.log('nol');
		} */
		
		if ((totalkembali !== 0) && (totalkembali > 0) )
		{
			console.log('masuk sini');
			sts_pengembalian=true;
		}else{
			sts_pengembalian=false;
		}
	
};

function ButtonDisabled_LPJ(mBol)
{
	//Ext.get('btnTambahBaris_LPJ').dom.disabled=mBol;
	//Ext.get('btnHapusBaris_LPJ').dom.disabled=mBol;
	Ext.getCmp('btnSimpan').setDisabled(mBol);
	Ext.getCmp('btnSimpanKeluar').setDisabled(mBol);
	Ext.getCmp('btnPengembalian_bpk').setDisabled(mBol);
	Ext.getCmp('btnHapus').setDisabled(mBol);
	Ext.getCmp('btnApprove').setDisabled(mBol);
	/*Ext.get('btnSimpan').dom.disabled=mBol;
	Ext.get('btnSimpanKeluar').dom.disabled=mBol;
	Ext.get('btnHapus').dom.disabled=mBol;	
	Ext.get('btnApprove').dom.disabled=mBol;	
	Ext.get('btnPengembalian_bpk').dom.disabled=mBol;*/
};


function HapusBaris_LPJDB(Line) 
{	
	Ext.Ajax.request
	(
		{
			url: "./Datapool.mvc/DeleteDataObj",
			params: getParam_LPJHapusBaris(Line), 
			success: function(o) 
			{
				var cst = o.responseText;
				if (cst == '{"success":true}') 
				{						
						RefreshDataFilter_LPJ(false);						
					}
					else if (cst == '{"pesan":0,"success":false}' )
					{
						
					}
					else 
					{
						
					}
			}
		}
	)
	
};

function getParam_LPJHapusBaris(Line) 
{
	var params = 
	{
		Table: 'viACC_CSO_LPJ',   		
		IS_DETAIL:1,		
		KATEGORI:KDkategori_LPJ,		
		REFF_ID:REFFIDKBS_LPJ,
		CSO_Number:Ext.get('txtNo_LPJ').getValue(),
		CSO_Date:Ext.get('dtpTanggal_LPJ').getValue(),
		Line:Line,
		Amount: getAmount_LPJ(Ext.get('txtTotal_LPJ').getValue()),
		KD_UNIT_KERJA:selectUnitKerja_LPJ,
		REFERENSI:Ext.get('txtReferensi_LPJ').dom.value,
				
	};
	return params
};

function getParamItem_LPJHapusBaris(Line,aci_code) 
{
	var params = 
	{
		Table: 'viACC_CSO_LPJ',   		
		IS_DETAIL:1,		
		KATEGORI:KDkategori_LPJ,		
		REFF_ID:REFFIDKBS_LPJ,
		CSO_Number:Ext.get('txtNo_LPJ').getValue(),
		CSO_Date:Ext.get('dtpTanggal_LPJ').getValue(),
		Line:Line,
		ACI_CODE:aci_code,
		Amount: getAmount_LPJ(Ext.get('txtTotal_LPJ').getValue()),
		KD_UNIT_KERJA:selectUnitKerja_LPJ,
		REFERENSI:Ext.get('txtReferensi_LPJ').dom.value,
		IS_ITEM:1
				
	};
	return params
};


function mComboUnitKerjaView_LPJ()
{
	var Field = ['kd_unit', 'nama_unit', 'unitkerja'];
	dsUnitKerja_LPJ = new WebApp.DataStore({ fields: Field });

  var comboUnitKerja_LPJView = new Ext.form.ComboBox
	(
		{
			id:'comboUnitKerja_LPJView',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih Unit Kerja...',
			fieldLabel: 'Unit Kerja',			
			align:'Right',
			anchor:'100%',
			store: dsUnitKerja_LPJ,
			valueField: 'kd_unit',
			displayField: 'nama_unit',
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectUnitKerja_LPJ=b.data.kd_unit ;
				} 
			}
		}
	);
	/* 
	dsUnitKerja_LPJ.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'KD_UNIT_KERJA',
			    Sortdir: 'ASC',
			    target: 'viCboUnitKerja',
			    // param: gstrListUnitKerja + "##@@##" + 0
			    // param: "kdunit="+ gstrListUnitKerja
			    param: ""
			}
		}
	); */
	
	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionPlafondUnitKerja/getUnitKerja",
		params: {
			text:''
		},
		failure: function(o)
		{
			ShowPesanError_LPJ('Error menampilkan data unit kerja !', 'Error');
		},	
		success: function(o) 
		{   
			dsUnitKerja_LPJ.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsUnitKerja_LPJ.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsUnitKerja_LPJ.add(recs);
			} else {
				ShowPesanError_LPJ('Gagal menampilkan data unit kerja', 'Error');
			};
		}
	});
	return comboUnitKerja_LPJView;
} ;

/* Cetak gstrSp3d */
function GetCriteria_LPJView(jab,satker,menyetujui)
{
	var strKriteria = '';
	
	if (Ext.get('txtNo_LPJ').getValue() != '') 
	{
		//strKriteria = " AND AC.CSO_Number ='" + Ext.get('txtNo_LPJ').getValue() + "'";
		strKriteria = Ext.get('txtNo_LPJ').dom.value;
		strKriteria += '##'+1;
		strKriteria += '##'+Ext.get('txtTerimaDari_LPJ').dom.value;	
		strKriteria += '##'+Ext.get('txtTotal_LPJ').dom.value;
		strKriteria += '##'+Ext.get('comboAktivaLancar_LPJ').dom.value;
		strKriteria += '##'+ShowDate(Ext.getCmp('dtpTanggal_LPJ').getValue());
		strKriteria += '##'+strKdUser;
		strKriteria += '##'+jab; //jabatan
		strKriteria += '##'+satker; //satuan kerja
		strKriteria += '##'+ menyetujui//'Dr. H. Euis Eti Rohaeti, M.Pd'; //nama yang mengetahui
		// strKriteria += '##'+'196812091993032002'; //nip yang mengetahui
		CetakBukti_LPJ(strKriteria);
		
	};
	return strKriteria;
};

function CetakBukti_LPJ(strParam) 
{	
    Ext.Ajax.request
	(
		{
			url: "./Module.mvc/ExecProc",
			params: 
			{
				UserID: 'Admin',
				ModuleID: 'clsDataPrintBill_CashOut',
				Params:	strParam
			},
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true )
				{
					tmpdataprint_penerimaanMhs=cst.data;
					console.log(printerList);
						var printer = 'Keuangan-Print';//"LX-310-xxx"//document.getElementById('cmbPrinterList').value;
						var cmd = "doPrint";
						console.log(printer);
						cmd = cmd+"#"+printer+"#"+btoa(tmpdataprint_penerimaanMhs);
						console.log(cmd);
						ws.send(cmd);

				}
				else
				{
					ShowPesanWarning_PenerimaanMhs('Data tidak berhasil diCetak ' + cst.msg, 'Bukti Pembayaran');
				}
			}

		}
	);
}

function Validasi_LPJView()
{
	var x=1;
	
	if(Ext.getCmp('txtNo_LPJ').getValue() == '')
	{
		ShowPesanWarning_LPJView('No. '+gstrSp3d+' belum di isi','Laporan '+gstrSp3d);
		x=0;
	}
	
	return x;
};

function ShowPesanWarning_LPJView(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};

function GetParamProsesCekTotalLPJ(param)
{
	var x = ''
	x=param;
	x += "##"+Ext.get('dtpTanggal_LPJ').getValue()
	
	
	return x;
};
function ProsesCekTotalLPJ(param,view)
{
	 Ext.Ajax.request
	 (
		{
			url: "./Module.mvc/ExecProc",
			params: 
			{
				UserID: 'Admin',
				ModuleID: 'clsCekTotalAkun',
				Params:	GetParamProsesCekTotalLPJ(param)			
			},
			success: function(o) 
			{
			var cst = Ext.decode(o.responseText);
				if (cst.success === true )
				{
				saldoAkunLPJ = cst.jumlah;
				if (view==true)
				{
					ShowPesanWarning_LPJView('Akun Memiliki Saldo : ' + formatCurrencyDec(cst.jumlah) ,'SALDO');
					}
				}else
				{
				saldoAkunLPJ =0;
				if (view==true)
				{
				ShowPesanWarning_LPJView('Akun Tidak Memiliki Saldo','SALDO');
				}
				}
			}

		}
		);
	
};

function ViewFile_LPJ()
{
new Ext.Window({
    height: 500,
    width: 500,
    bodyCfg: {
        tag: 'iframe',
        //src: 'bills/bill123.pdf',
		src: Ext.get('txtPathfile_LPJ').getValue(),
        style: 'border: 0 none'
    },
    modal: true
}).show();  
}

function GetDataRincianItem_LPJ(cso_no,cso_date,acc) 
{   
    Ext.Ajax.request
    (
        {
            url: "./Module.mvc/ExecProc",
            params: 
            {
                UserID: 'Admin',
                ModuleID: 'clsGetRincianItem_LPJ',
                Params: cso_no+'##' +cso_date+'##'+acc+'##'
            },
            success: function(o) 
            {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true )
                {
                	Ext.getCmp('txtNoteItem_LPJ').setValue(cst.note);
                	RefreshDataDTLItem_LPJ(cst.cso_no,cst.cso_date,cst.acc);
                }
                else
                {   
                    
                    ShowPesanWarning_LPJ('Data tidak ditemukan', 'Detail Rincian');
                   
                }
            }

        }
    );
}


///=============LOOKUP KOMPONEN Entry Penerimaan MHS=============================
var rowSelectedItem_LPJ;
var vWinFormLookupItem_LPJ;


function FormLookUpItem_LPJ(criteria,dsStore,p,mBolAddNew,idx,mBolLookup) 
{
	
    vWinFormLookupItem_LPJ = new Ext.Window
	(
		{
			id: 'vWinFormLookupItem_LPJ',
			title: 'Lookup Item',
			closeAction: 'hide',
			closable:false,
			width: 450,
			height: 420,
			border: false,
			plain: true,
			resizable:false,
			layout: 'form',
			iconCls: 'find',
			modal: true,                                   
			items: 
			[
				GetPanelLookUpItem_LPJ(criteria,dsStore,p,mBolAddNew,idx,mBolLookup)
			],
			listeners:
				{ 
					activate: function()
					{ } 
				}
		}
	);

    vWinFormLookupItem_LPJ.show();
};	

function GetPanelLookUpItem_LPJ(criteria,dsStore,p,mBolAddNew,idx,mBolLookup)
{
	var FormPnlLookUpItem_LPJ = new Ext.Panel    
	(
		{
			id: 'FormPnlLookUpItem_LPJ',
			region: 'center',
			layout: 'form',
			title: '',
			bodyStyle: 'padding:15px',
			anchor:'100%',
			border: true,
			bodyStyle: 'background:#FFFFFF;',
			height: 392,
			shadhow: true,
			items: 
			[
				fnGetDTLGridLookUpItem_LPJ(criteria),
				{
					layout: 'hBox',
					border: false,
					defaults: { margins: '0 5 0 0' },
					style:{'margin-left':'11px','margin-top':'2px'},
					anchor: '94%',
					layoutConfig: 
					{
						padding: '3',
						pack: 'end',
						align: 'middle'
					},
					items:
					[
						{
							xtype:'button',
							text:'Ok',
							width:70,
							style:{'margin-left':'3px','margin-top':'2px'},
							hideLabel:true,
							id: 'btnOkItem_LPJ',
							handler:function()
							{
								/*if (p != undefined && rowSelectedItem_LPJ != undefined)
								{
										p.data.ACI_CODE=rowSelectedItem_LPJ.data.ACI_CODE;
										p.data.ACI_DESCRIPTION=rowSelectedItem_LPJ.data.ACI_DESCRIPTION;
										
										   dsStore.removeAt(dsStore.getCount()-1);
										   dsStore.insert(dsStore.getCount(), p);
								}*/
								if (p != undefined && rowSelectedItem_LPJ != undefined)
								{
									if (mBolAddNew === true || mBolAddNew === undefined)
									{
										p.data.ACI_CODE=rowSelectedItem_LPJ.data.ACI_CODE;
										p.data.ACI_DESCRIPTION=rowSelectedItem_LPJ.data.ACI_DESCRIPTION;
										if (mBolLookup === true)
										{
										   dsStore.insert(dsStore.getCount(), p);
										}
										else
										{
										   dsStore.removeAt(dsStore.getCount()-1);
										   dsStore.insert(dsStore.getCount(), p);
										}										
									}
									else
									{
										if (dsStore != undefined)
										{
											p.data.ACI_CODE=rowSelectedItem_LPJ.data.ACI_CODE;
											p.data.ACI_DESCRIPTION=rowSelectedItem_LPJ.data.ACI_DESCRIPTION;
											
											dsStore.removeAt(idx);
											dsStore.insert(idx, p);
										}										
									}
									vWinFormLookupItem_LPJ.close();
									
								}
								else
								{
									ShowPesanWarning_LPJ('Belum ada item yang di pilih','Lookup Item');
								};
							}
						},
						{
								xtype:'button',
								text:'Cancel',
								width:70,
								style:{'margin-left':'5px','margin-top':'2px'},
								hideLabel:true,
								id: 'btnCancelAcc',
								handler:function() 
								{
									vWinFormLookupItem_LPJ.close();
								}
						}
					]
				}
			]
        }
	);
	
	return FormPnlLookUpItem_LPJ;
};

///---------------------------------------------------------------------------------------///

function fnGetDTLGridLookUpItem_LPJ(criteria) 
{  
	var fldDetail = ['ACI_CODE','ACCOUNT','ACI_DESCRIPTION']
	var dsLookUpItem_LPJ = new WebApp.DataStore({ fields: fldDetail });
	var strParam="WHERE ACCOUNT='"+ cellSelectedDet_LPJ.data.ACCOUNT +"' " + criteria 
	
	var vGridLookUpItem_LPJ = new Ext.grid.EditorGridPanel
	(
		{ 
			id:'vGridLookUpItem_LPJ',
			title: '',
			stripeRows: true,
			store: dsLookUpItem_LPJ,
			height:353,
			columnLines:true,
			bodyStyle: 'padding:0px',
			border: false,		
			sm: new Ext.grid.RowSelectionModel
			(
				{ 
					singleSelect: true,
					listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedItem_LPJ = dsLookUpItem_LPJ.getAt(row);
						}
					}
			}
				),
			cm: fnGridLookUpItem_LPJ(),
			viewConfig: { forceFit: true }
		});
	
	dsLookUpItem_LPJ.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				Sort: 'ACI_CODE', 
				Sortdir: 'ASC', 
				target:'ViewLookupRincianItem',
				param: strParam
			} 
		}
	);
	
	return vGridLookUpItem_LPJ;
};

function fnGridLookUpItem_LPJ() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{ 
				id: 'colkdItemLookUpItem_LPJ',
				header: "Kd item",
				dataIndex: 'ACI_CODE',
				width: 60
			},
			{ 
				id: 'colDescLookUpItem_LPJ',
				header: "Description",
				dataIndex: 'ACI_DESCRIPTION',
				width: 200
            },
		]
	)
};
///=============END LOOKUP KOMPONEN Entry Penerimaan MHS=============================
var winFormLookupsp3d;
var dsGLLookupsp3d;
var selectedrowGLLookupsp3d;

var CurrentTRsp3d = 
{
    data: Object,
    details:Array, 
    row: 0
};

var nASALFORM_KBS =1;
var nASALFORM_LPJ =2;
var nASALFORM_KasKeluarkecil =3;
var nASALFORM_NONMHS =4;
var nASALFORM_CSAR =5;
var nASALFORM_CSAPForm =6;

function FormLookupsp3d(ds,record,criteria, nASALFORM, thnAngg)
{
	winFormLookupsp3d = new Ext.Window
	(		
		{
		    id: 'winFormLookupsp3d',
		    title: 'Lookup PPD',
		    closeAction: 'destroy',
		    closable: true,
		    width: 670,
		    height: 450,
		    border: false,
		    plain: true,
		    resizable: false,
		    layout: 'form',
		    iconCls: '',
		    modal: true,
			autoScroll: true,
		    items: 
			[
				{

					xtype: 'compositefield',
					// fieldLabel: 'No. '+gstrSp3d,
					style: { 'margin-left': '-90px' },
					width: 640,
					items: 
					[
						
						{ 
							xtype: 'tbtext', text: 'Tahun:', cls: 'left-label', width: 50
						},
						mCombo_TahunAnggaran(150,'cboThnLookupSP3D',thnAngg),
						{ 
							xtype: 'tbtext', text: '', cls: 'left-label', width: 20
						},
						{ 
							xtype: 'tbtext', text: 'No. PPD:', cls: 'left-label', width: 80
						},
						{
							xtype:'textfield',
							// fieldLabel: 'No. '+gstrSp3d,
							name: 'txtNoSP3Dlookup1',
							id: 'txtNoSP3Dlookup1',
							width:200,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										/* var sql = " where SP3.NO_SP3D_RKAT like '%" + Ext.get('txtNoSP3Dlookup1').getValue() + "%'"
										if(nASALFORM == nASALFORM_LPJ){
											sql += " AND APP_SP3D = 1 and (( TAHAP_PROSES in(2) AND AC.NO_Tag is null ) or (TAHAP_PROSES in(3) AND AC.NO_Tag is not null )) AND  SP3.TAHUN_ANGGARAN_TA = " + thn;
											RefreshDataLookupkbs(sql);
										}else if(nASALFORM == nASALFORM_CSAPForm){
											sql += " AND APP_SP3D = 1 and (( TAHAP_PROSES in(2) AND AC.NO_Tag is null ) or (TAHAP_PROSES in(3) AND AC.NO_Tag is not null )) AND  SP3.TAHUN_ANGGARAN_TA = " + thn;
											RefreshDataLookupkbs(sql);
										}
										else{
											sql += " and APP_SP3D = '1' and  TAHAP_PROSES in (2) AND SP3.TAHUN_ANGGARAN_TA = "+ thnAngg //Ext.get('cboThnLookupSP3D').getValue()
											RefreshDataLookupsp3d(sql);
										}		 */				
										
										RefreshDataLookupsp3d(Ext.getCmp('cboThnLookupSP3D').getValue(),Ext.getCmp('txtNoSP3Dlookup1').getValue(),Ext.getCmp('comboUnitKerja_LPJ').getValue());
									} 						
								}
							}
						},
						{ 
							xtype: 'tbtext', text: '', cls: 'left-label', width: 20
						},
						{
							xtype: 'button',
							tooltip: 'Tampilkan',
							iconCls: 'refresh',
							text: 'Tampilkan',
							id: 'btnRefreshLookupSP3D',
							handler: function()
							{
								
								/* var sql = " where SP3.NO_SP3D_RKAT like '%" + Ext.get('txtNoSP3Dlookup1').getValue() + "%'"
								if(nASALFORM == nASALFORM_LPJ){
									sql += " AND APP_SP3D = 1  and (( TAHAP_PROSES in(2) AND AC.NO_Tag is null ) or (TAHAP_PROSES in(3) AND AC.NO_Tag is not null )) AND  SP3.TAHUN_ANGGARAN_TA = " + thn;
									RefreshDataLookupkbs(sql);
								}else{
									sql += " and APP_SP3D = '1' and  TAHAP_PROSES in (2) AND SP3.TAHUN_ANGGARAN_TA = "+ thnAngg //Ext.get('cboThnLookupSP3D').getValue()
									RefreshDataLookupsp3d(sql);
								}	 */
								RefreshDataLookupsp3d(Ext.getCmp('cboThnLookupSP3D').getValue(),Ext.getCmp('txtNoSP3Dlookup1').getValue(),Ext.getCmp('comboUnitKerja_LPJ').getValue());
				
							}
						}
					]
				},
				getItemFormLookupsp3d(ds,record,criteria,nASALFORM,thnAngg)
			]			
		}
	);
	
	if(Ext.getCmp('comboUnitKerja_LPJ').getValue() != '' && Ext.getCmp('comboUnitKerja_LPJ').getValue() != undefined){
		winFormLookupsp3d.show();
	}else{
		ShowPesanInfo_LPJ('Pilih unit kerja terlebih dahulu!', 'Warning');
	}
}

function mCombo_TahunAnggaran(lebar,NamaCbo) 
{
	var Field = ['TAHUN_ANGGARAN_TA', 'CLOSED_TA', 'TMP_TAHUN'];
	dsListTahun_TahunAnggaran = new WebApp.DataStore({ fields: Field });
	// RefreshComboTahun_TahunAnggaran();
	var currYear = parseInt(now.format('Y'));
	var combo_TahunAnggaran = new Ext.form.ComboBox
	(
		{
		    id: NamaCbo,
		    name: NamaCbo,			
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			align:'Right',
			disabled:true,
			// anchor:'100%',//'70%',			
			width: lebar,
			// store: dsListTahun_TahunAnggaran,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: 
					[						
						[currYear + 1,currYear + 1], 
						[currYear,currYear ]
					]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			// value:gstrTahunAngg +'-'+(Ext.num(gstrTahunAngg)+1),
			value:Ext.num(now.format('Y')),
			listeners:
			{
				'select': function(a,b,c)
				{
				    SelectThn_TahunAnggaran = b.data.displayText;
				}
			}
		}
	);
	
	return combo_TahunAnggaran;
}
function getItemFormLookupsp3d(ds,record,criteria,nASALFORM,thnAngg)
{	
	var frmListLookupsp3d = new Ext.Panel
	(
		{
			id: 'frmListLookupsp3d',	    
			autoScroll: true,
			items: 
			[
				getGridListsp3d(criteria,nASALFORM),				
				{
				    layout: 'hBox',
				    border: false,
				    defaults: { margins: '0 5 0 0' },
				    style: { 'margin-left': '4px', 'margin-top': '3px' },
				    anchor: '96.5%',
				    layoutConfig:
					{
					    padding: '3',
					    pack: 'end',
					    align: 'middle'
					},
				    items:
					[
						{
						    xtype: 'button',
							width: 70,
						    text: 'Ok',
							handler: function()
							{
								//CekNOLookup(nASALFORM)
								
								Ext.get('txtReferensi_LPJ').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.referensi2;
								Ext.get('comboUnitKerja_LPJ').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.nama_unit_kerja;
								Ext.getCmp('comboUnitKerja_LPJ').setValue(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.kd_unit_kerja);
								Ext.get('txtCatatan_LPJ').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.keterangan;
								selectUnitKerja_LPJ = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.kd_unit_kerja;
								
								INPUTDetailSP3D_LPJ(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data);
								//alert(selectUnitKerja_LPJ);
								Ext.get('txtTotal_LPJ').dom.value=formatCurrencyDec(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.jumlah);
								Ext.get('txtJml_LPJ').dom.value=formatCurrencyDec(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.jumlah);
								winFormLookupsp3d.close();	
							}
						},
						{
						    xtype: 'button',
							width: 70,
						    text: 'Cancel',
							handler: function()
							{
								winFormLookupsp3d.close();
							}
						}
					]
				}
			]
		}
	);
			
	return frmListLookupsp3d;
}
var gridListLookupsp3d;
function getGridListsp3d(criteria,nASALFORM)
{
	/* var _fields = 
	[
		'NOMOR','AliasNOMOR', 'TANGGAL','REFERENSI', 
		'JML','UNIT','KD_UNIT',
		'Personal','Account','Pay_No','Pay_Code','name', 'Payment','Notes1'
	]; */
	
	var _fields = 
	[
		'selected','tahun_anggaran_ta', 'kd_unit_kerja', 'no_sp3d_rkat', 'tmpno_sp3d_rkat', 'tgl_sp3d_rkat',
		'jumlah', 'jenis_sp3d', 'nama_unit_kerja', 'app_sp3d',
		'tahun_anggaran_ta_tmp','kd_unit_kerja_tmp','no_sp3d_rkat_tmp', 'tgl_sp3d_rkat_tmp','keterangan','referensi','referensi2',
		'jalur','prioritas'
	];
	
	dsGLLookupsp3d = new WebApp.DataStore({ fields: _fields });
	
	/* if(nASALFORM == nASALFORM_LPJ){
		RefreshDataLookupkbs(criteria);
	}else{
		RefreshDataLookupsp3d(criteria);
	}  */
	
	gridListLookupsp3d = new Ext.grid.EditorGridPanel
	(
		{
			stripeRows: true,
			id: 'gridListLookupsp3d',
			store: dsGLLookupsp3d,
			height:353,
			anchor: '100% 90%',
			columnLines:true,
			bodyStyle: 'padding:0px',
			autoScroll: true,
			border: false,
			viewConfig : 
			{
				forceFit: true
			},		
			sm: new Ext.grid.RowSelectionModel
			(
				{ 
					singleSelect: false,
					listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							selectedrowGLLookupsp3d = dsGLLookupsp3d.getAt(row);
							CurrentTRsp3d.row = row;	
						}						
					}
				}
			),
			//plugins: [new Ext.ux.grid.FilterRow()],
			//cm: fnGridLookupsp3dColumnModel(),
			cm: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),					
					{ 
						id: 'colNosp3dGLLookupsp3d',
						header: 'No. PPD',
						dataIndex: 'no_sp3d_rkat',
						renderer: function(value, cell) 
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
							return str;
						}						
					},
					{ 
						id: 'colaliasNosp3dGLLookupsp3d',
						header: 'No. PPD',
						hidden: true,
						dataIndex: 'no_sp3d_rkat',
						renderer: function(value, cell) 
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
							return str;
						}						
					},
					{ 
						id: 'colNoRefsp3dGLLookupsp3d',
						header: 'No. Referensi',
						dataIndex: 'referensi',
						renderer: function(value, cell) 
						{
							var str
							if(value != null){
								str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
							}else{
								str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + "-" + "</div>";
							}
							return str;
						}			
					},
					{ 
						id: 'colTGLsp3dGLLookupsp3d',
						header: 'Tanggal',
						dataIndex: 'tgl_sp3d_rkat',
						width:75,
						renderer: function(v, params, record) 
						{							
							return ShowDate(record.data.tgl_sp3d_rkat);
						}			
					},
					{ 
						id: 'colUKsp3dGLLookupsp3d',
						header: 'Unit Kerja',
						dataIndex: 'nama_unit_kerja',
						width:110,
						renderer: function(value, cell) 
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
							return str;
						}			
					},
					{ 
						id: 'colJMLsp3dGLLookupsp3d',
						header: 'JML',
						dataIndex: 'jumlah',
						width:100,	
						align:'right',						
						renderer: function(v, params, record) 
						{
							var str = "<div style='white-space:normal;padding:2px 20px 2px 2px'>" + formatCurrencyDec(record.data.jumlah) + "</div>";
							return str;
							 
						}	
					}
				]
			),
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					Ext.get('txtReferensi_LPJ').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.referensi2;
					Ext.get('comboUnitKerja_LPJ').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.nama_unit_kerja;
					Ext.getCmp('comboUnitKerja_LPJ').setValue(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.kd_unit_kerja);
					Ext.get('txtCatatan_LPJ').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.keterangan;
					selectUnitKerja_LPJ = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.kd_unit_kerja;
					
					INPUTDetailSP3D_LPJ(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data);
					Ext.get('txtTotal_LPJ').dom.value=formatCurrencyDec(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.jumlah);
					Ext.get('txtJml_LPJ').dom.value=formatCurrencyDec(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.jumlah);
					winFormLookupsp3d.close();
					
				
					
				},
				'afterrender': function(){ 
					// console.log();
					RefreshDataLookupsp3d(Ext.getCmp('cboThnLookupSP3D').getValue(),Ext.getCmp('txtNoSP3Dlookup1').getValue(),Ext.getCmp('comboUnitKerja_LPJ').getValue());
				}
				// End Function # --------------
			},
		}
	);
	
	return gridListLookupsp3d;
}

/*function fnGridLookupsp3dColumnModel() 
{
    return new Ext.grid.ColumnModel
	(
		[	
    		new Ext.grid.RowNumberer(),					
			{ 
				id: 'colNosp3dGLLookupsp3d',
				header: 'No. '+gstrSp3d,
				dataIndex: 'NOMOR',
				hidden: true,
				renderer: function(value, cell) 
				{
					var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
					return str;
				}					
			},
			{ 
				id: 'colaliasNosp3dGLLookupsp3d',
				header: 'No. '+gstrSp3d,
				dataIndex: 'AliasNOMOR',
				renderer: function(value, cell) 
				{
					var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
					return str;
				}						
			},
			{ 
				id: 'colNoRefsp3dGLLookupsp3d',
				header: 'No. Referensi',
				dataIndex: 'REFERENSI',
				renderer: function(value, cell) 
				{
					var str
					if(value != null){
						str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
					}else{
						str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + "-" + "</div>";
					}
					return str;
				}			
			},
			{ 
				id: 'colTGLsp3dGLLookupsp3d',
				header: 'Tanggal',
				dataIndex: 'TANGGAL',
				width:70,
				renderer: function(v, params, record) 
				{							
					return ShowDate(record.data.TANGGAL);
				}			
			},
			{ 
				id: 'colUKsp3dGLLookupsp3d',
				header: 'Unit Kerja',
				dataIndex: 'UNIT',
				width:125,
				renderer: function(v, params, record) 
				{							
					return ShowDate(record.data.UNIT);
				}			
			},
			{ 
				id: 'colJMLsp3dGLLookupsp3d',
				header: 'JML',
				dataIndex: 'JML',	
				align:'right',						
				renderer: function(v, params, record) 
				{
					return formatCurrency(record.data.JML);
				}	
			}
		]
	)
};*/

function RefreshDataLookupsp3d(tahun_anggaran_ta,no_sp3d,kd_unit_kerja)
{	
	/* dsGLLookupsp3d.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: '',
			    Sortdir: '',
			    target: 'viViewLookupSP3D',
			    param: criteria
			}
		}
	); */
	
	Ext.Ajax.request ({
		url: baseURL + "index.php/keuangan/functionLPJ/LoadPPD",
		params: {
			no_sp3d: no_sp3d,
			tahun_anggaran_ta : tahun_anggaran_ta,
			kd_unit_kerja : kd_unit_kerja
		},
		failure: function(o)
		{
			ShowPesanError_kbs('Error menampilkan data PPD !', 'Error');
		},	
		success: function(o) 
		{   
			dsGLLookupsp3d.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsGLLookupsp3d.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsGLLookupsp3d.add(recs);
				console.log(dsGLLookupsp3d);
				gridListLookupsp3d.getView().refresh();
			} else {
				ShowPesanError_kbs('Gagal menampilkan data PPD', 'Error');
			};
		}
	});
	
	return dsGLLookupsp3d;
}

function RefreshDataLookupkbs(criteria)
{
	dsGLLookupsp3d.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: '',
			    Sortdir: '',
			    target: 'viViewLookupKBS',
			    param: criteria
			}
		}
	);
	return dsGLLookupsp3d;
}

function CekNOLookup(nASALFORM)
{
	var x;
	Ext.Ajax.request
	(
		{
			url: "./Datapool.mvc/CreateDataObj",
			params: getParam_lookup(nASALFORM),
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{

					//alert(nASALFORM+' - '+nASALFORM_KBS)
					if (nASALFORM == nASALFORM_KBS)
					{

						//alert(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.NOMOR)

						Ext.get('txtReferensi_LPJ').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.NOMOR;
						// Ext.get('txtAliasReferensi_kbs').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.AliasNOMOR;
						Ext.get('comboUnitKerja_LPJ').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.UNIT;
						Ext.getCmp('comboUnitKerja_LPJ').setValue(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.KD_UNIT);
						Ext.get('txtCatatan_kbs').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Notes1;
						selectUnitKerja_LPJ = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.KD_UNIT;
						
						INPUTDetailSP3D_kbs();
						//alert(selectUnitKerja_LPJ);
						Ext.get('txtTotal_LPJ').dom.value=formatCurrencyDec(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.JML);
						Ext.get('txtJml_LPJ').dom.value=formatCurrencyDec(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.JML);
						REFFID_SP3D_kbs='01'; //Di isi KODE sumber referensi 
				
						winFormLookupsp3d.close();
					}
					else if (nASALFORM == nASALFORM_LPJ)
					{
						//get gstrSp3d
						if(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.REFERENSI != null){
							Ext.get('txtReferensi_LPJ').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.REFERENSI;
							// Ext.get('txtAliasReferensi_LPJ').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.REFERENSI;
						}else{
							Ext.get('txtReferensi_LPJ').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.NOMOR;
							// Ext.get('txtAliasReferensi_LPJ').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.AliasNOMOR;
						}
						
						Ext.get('txtNoPembayaran_LPJ').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Pay_No;
						Ext.get('txtCatatan_LPJ').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.keterangan;
						
						if(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Personal != ""){
							Ext.get('txtTerimaDari_LPJ').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Personal;
						}
						
						no_sp3d = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.NOMOR;
						
						//Satuan Kerja
						Ext.get('comboUnitKerja_LPJ').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.UNIT;
						Ext.getCmp('comboUnitKerja_LPJ').setValue(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.KD_UNIT);
						selectUnitKerja_LPJ = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.KD_UNIT;
						
						//Jenis Pembayaran
						if(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Pay_Code != ""){
							Ext.get('comboBayar_LPJ').dom.value= dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Payment;
							Ext.getCmp('comboBayar_LPJ').setValue(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Pay_Code);
							selectBayar_LPJ = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Pay_Code;
						}
						
						//Akun
						if(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Account != "" ){
						
							Ext.get('comboAktivaLancar_LPJ').dom.value= dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.name;
							Ext.getCmp('comboAktivaLancar_LPJ').setValue(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Account);
							
						selectAktivaLancar_LPJ = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Account
						}
						
						
						INPUTDetailSP3D_LPJ();
						Ext.get('txtTotal_LPJ').dom.value=formatCurrencyDec(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.JML);		
						Ext.get('txtJml_LPJ').dom.value=formatCurrencyDec(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.JML);							
						if ((dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.REFERENSI == null) || (dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.REFERENSI == ''))
						{
							REFFIDKBS_LPJ='01'; //Di isi KODE sumber referensi sp3d
							Ext.getCmp('btnPengembalian_bpk').setDisabled(true);
						}else
						{
							REFFIDKBS_LPJ='22'; //Di isi KODE sumber referensi kbs
							Ext.getCmp('btnPengembalian_bpk').setDisabled(false);
						}

						winFormLookupsp3d.close();
					}
					else if (nASALFORM == nASALFORM_NONMHS)
					{
						//get gstrSp3d
						Ext.get('txtReferensi_PenerimaanNonMhs').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.NOMOR;
						Ext.get('txtNoPembayaran_PenerimaanNonMhs').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Pay_No;
						//Ext.get('txtCatatan_PenerimaanNonMhs').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Notes1;
						Ext.get('txtTerimaDari_PenerimaanNonMhs').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Personal;
						no_sp3d = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.NOMOR;
						
						//Satuan Kerja
						Ext.getCmp('comboUnitKerja_PenerimaanNonMhs').setValue(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.KD_UNIT);
						Ext.get('comboUnitKerja_PenerimaanNonMhs').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.UNIT;
						selectUnitKerja_PenerimaanNonMhs = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.KD_UNIT;
						
						//Jenis Pembayaran
						Ext.getCmp('comboBayar_PenerimaanNonMhs').setValue(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Pay_Code);
						Ext.get('comboBayar_PenerimaanNonMhs').dom.value= dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Payment;
						selectBayar_PenerimaanNonMhs = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Pay_Code;
						
						//Akun
						Ext.getCmp('comboAktivaLancar_PenerimaanNonMhs').setValue(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Account);
						Ext.get('comboAktivaLancar_PenerimaanNonMhs').dom.value= dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.name;
						selectAktivaLancar_PenerimaanNonMhs = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Account
						
						INPUTDetailSP3D_PenerimaanNonMhs();
						Ext.get('txtTotal_PenerimaanNonMhs').dom.value=formatCurrency(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.JML);								
						winFormLookupsp3d.close();
						
					}else if (nASALFORM == nASALFORM_KasKeluarkecil)
					{
						Ext.get('txtreferensi_KasKeluarkecil').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.NOMOR;
						INPUTDetailSP3D_KasKeluarkecil();
						Ext.get('txtTotal_KasKeluarkecil').dom.value=formatCurrency(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.JML);								
						winFormLookupsp3d.close();
					}else if (nASALFORM == nASALFORM_CSAPForm)
					{
						//get gstrSp3d
						if(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.REFERENSI != null){
							Ext.get('txtReferensi_CSAPForm').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.REFERENSI;
							// Ext.get('txtAliasReferensi_LPJ').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.REFERENSI;
						}else{
							Ext.get('txtReferensi_CSAPForm').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.NOMOR;
							// Ext.get('txtAliasReferensi_LPJ').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.AliasNOMOR;
						}
						
						Ext.get('txtNoPembayaran_CSAPForm').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Pay_No;
						Ext.get('txtCatatan_CSAPForm').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Notes1;
						
						// if(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Personal != ""){
						// 	Ext.get('txtTerimaDari_LPJ').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Personal;
						// }
						
						no_sp3d = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.NOMOR;
						
						//Satuan Kerja
						// Ext.get('comboUnitKerja_LPJ').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.UNIT;
						// Ext.getCmp('comboUnitKerja_LPJ').setValue(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.KD_UNIT);
						// selectUnitKerja_LPJ = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.KD_UNIT;
						
						//Jenis Pembayaran
						if(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Pay_Code != ""){
							Ext.get('comboBayar_CSAPForm').dom.value= dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Payment;
							Ext.getCmp('comboBayar_CSAPForm').setValue(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Pay_Code);
							selectBayar_CSAPForm = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Pay_Code;
						}
						
						//Akun
						if(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Account != "" ){
						
							Ext.get('comboAktivaLancar_CSAPForm').dom.value= dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.name;
							Ext.getCmp('comboAktivaLancar_CSAPForm').setValue(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Account);
							selectAktivaLancar_CSAPForm = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Account
						}
						
						
						INPUTDetailSP3D_CSAPForm();
						Ext.get('txtTotal_CSAPForm').dom.value=formatCurrencyDec(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.JML);		
						// Ext.get('txtJml_LPJ').dom.value=formatCurrencyDec(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.JML);							
						if ((dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.REFERENSI == null) || (dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.REFERENSI == ''))
						{
							REFFIDKBS_LPJ='01'; //Di isi KODE sumber referensi sp3d
							// Ext.getCmp('btnPengembalian_bpk').setDisabled(true);
						}else
						{
							REFFIDKBS_LPJ='22'; //Di isi KODE sumber referensi kbs
							// Ext.getCmp('btnPengembalian_bpk').setDisabled(false);
						}
						winFormLookupsp3d.close();
					}
				}
				else 
				{
					ShowPesanWarning_lookup('Data telah digunakan dengan no. transaksi ' + cst.pesan , 'Lookup Data');
					x=0;
					return x;
				}
			}
		}
	)	
}

function getParam_lookup(nForm) 
{
	var jalur;
	if (nForm == nASALFORM_KBS){
		jalur = 1;
	}else if(nForm == nASALFORM_LPJ){
		jalur = 2;
	}else if(nForm == nASALFORM_CSAPForm){
		jalur = 2;
	}

	var params = 
	{
		Table: 'cekNoTransaksi',
		REFERENSI:selectedrowGLLookupsp3d.data.REFERENSI,
		NO:selectedrowGLLookupsp3d.data.NOMOR,
		JALUR:jalur
	};
	return params
};

function ShowPesanWarning_lookup(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};

function ShowPesanError_lookup(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR
		}
	);
};

var rowSelectedLookApprove;
var vWinFormEntryApprove;
var now = new Date();
var vPesan;
var vCekPeriode;

function FormApprove(jumlah,type,nomor,Keterangan,tgl) {

    vWinFormEntryApprove = new Ext.Window
	(
		{
		    id: 'FormApprove',
		    title: 'Approve',
		    closeAction: 'hide',
		    closable: false,
		    width: 400,
		    height: 200,
		    border: false,
		    plain: true,
		    resizable: false,
		    layout: 'form',
		    iconCls: 'approve',
		    modal: true,
		    items:
			[
				ItemDlgApprove(jumlah,type,nomor,Keterangan,tgl)
			],
		    listeners:
				{
				    activate: function()
				    
				    {  }
				}
		}
	);
    vWinFormEntryApprove.show();
};


function ItemDlgApprove(jumlah,type,nomor,Keterangan,tgl) 
{	
	if (tgl==undefined)
	{		
		tgl=now;
	}
	
	var PnlLapApprove = new Ext.Panel
	(
		{ 
			id: 'PnlLapApprove',
			fileUpload: true,
			layout: 'form',
			height: '100',
			anchor: '100%',
			bodyStyle: 'padding:15px',
			border: true,
			items: 
			[
				getItemApprove_Nomor(nomor),
				getItemApprove_Tgl(tgl),
				getItemApprove_Jml(jumlah),
				getItemApprove_Notes(Keterangan),
				{
					layout: 'hBox',
					border: false,
					defaults: { margins: '0 5 0 0' },
					style:{'margin-left':'30px','margin-top':'5px'},
					anchor: '94%',
					layoutConfig: 
					{
						padding: '3',
						pack: 'end',
						align: 'middle'
					},
					items: 
					[
						{
							xtype: 'button',
							text: 'Ok',
							width: 70,
							hideLabel: true,
							id: 'btnOkLapApprove',
							handler: function() 
							{
								if (ValidasiEntryNomorApp('Approve') == 1 )
								{										
									if (type==='0')
									{
										PenerimaanApprove(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtNomorApp').getValue(),Ext.get('txtCatatanApp').getValue());
										RefreshDataPenerimaanFilter(false);
									};
									if (type==='2')
									{
										Approve_PenerimaanMhs(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtCatatanApp').getValue())										
									};
									if (type==='3')
									{
										Approve_PenerimaanNonMhs(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};
									if (type==='4')
									{
										Approve_KasKeluar(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};
									if (type==='5')
									{
										Approve_KasKeluarkecil(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};
									if (type==='6')
									{
										Approve_kbs(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};
									if (type==='7')
									{										
										Approve_RekapSP3D(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};		
									if (type==='8')
									{
										Approve_LPJ(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};									
									if (type==='9')
									{
										Approve_PaguGNRL(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};	
									if (type==='10')
									{
										Approve_viKembaliBDATM(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtCatatanApp').getValue())										
									};
									if (type==='11')
									{
										// Approve_OpenArForm(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtNomorApp').getValue())
										Approve_OpenArForm(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtNomorApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};
									if (type==='12')
									{
										Approve_OpenApForm(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtNomorApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};
									if (type==='13')
									{
										Approve_AdjustArForm(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtNomorApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};
									if (type==='14')
									{
										Approve_AdjustApForm(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtNomorApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};
									vWinFormEntryApprove.close();	
								}
							}
						},
						{
							xtype: 'button',
							text: 'Cancel',
							width: 70,
							hideLabel: true,
							id: 'btnCancelLapApprove',
							handler: function() 
							{
								vWinFormEntryApprove.close();
							}
						}
					]
				}
			]
		}
	);
 
    return PnlLapApprove
};

function getItemApprove_Nomor(nomor)
{
	var items = 			
	{
	    layout:'column',
	    border:false,	
	    items:
		[
			{
				columnWidth:1,
				layout: 'form',
				labelWidth: 65,
				labelAlign:'right',
				readOnly:true,
				border:false,				
				items: 
				[ 
					{
						xtype:'textfield',
						fieldLabel: 'No.Reff. ',
						name: 'txtNomorApp',
						id: 'txtNomorApp',						
						value:nomor,
						readOnly:true,
						anchor:'95%'
					}
				]
			}
		]
	}
	return items;
};

function getItemApprove_Notes(Keterangan)
{
	var items = 			
	{
	    layout:'form',
	    border:false,	
	    items:
		[
			{
				columnWidth:1,
				layout: 'form',
				labelWidth: 65,
				labelAlign:'right',
				border:false,
				items: 
				[ 
					{
						xtype:'textfield',
						fieldLabel: 'Catatan ',
						name: 'txtCatatanApp',
						id: 'txtCatatanApp',
						value:Keterangan,
						anchor:'95%'
					}
				]
			}
		]
	}
	return items;
};

function getItemApprove_Tgl(tgl)
{
	var items = 			
	{
	    layout:'form',
	    border:false,	
	    items:
		[
			{
				columnWidth:1,
				layout: 'form',
				labelWidth: 65,
				labelAlign:'right',
				border:false,
				items: 
				[
					{
						xtype: 'datefield',					
                        fieldLabel: 'Tanggal ',
                        id: 'dtpTanggalApp',
                        name: 'dtpTanggalApp',
                        format: 'd/M/Y',						
						value:tgl,
                        anchor: '50%'
					}
				]
			}
		]
	}
	return items;
};

function getItemApprove_Jml(jumlah)
{
	var items = 			
	{
	    layout:'column',
	    border:false,	
	    items:
		[			
			{
				columnWidth:1,
				layout: 'form',
				labelWidth: 65,
				labelAlign:'right',
				border:false,
				items: 
				[ 
					{
						xtype:'textfield',
						fieldLabel: 'Jumlah ',
						name: 'txtJumlah',
						id: 'txtJumlah',
						anchor:'95%',
						value:jumlah,
						readOnly:true
					}
				]
			}
		]
	}
	return items;
};

function ValidasiEntryNomorApp(modul)
{
	var x = 1;
	if (Ext.get('txtNomorApp').getValue() == '' )
	{
		if (Ext.get('txtNomorApp').getValue() == '')
		{
			ShowPesanWarningApprove('Nomor belum di isi',modul);
			x=0;
		}		
	}
	return x;
};

function ShowPesanWarningApprove(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};

var addNew_PengembalianLPJ;
var dataSource_PengembalianLPJ;
var selectCount_PengembalianLPJ=50;
var rowSelected_PengembalianLPJ;
var setLookUps_PengembalianLPJ;
var NMform_PengembalianLPJ='Pengembalian LPJ';
var idunitkerja;
var totalviewkembali;
var thn = '';
// function setFrmLookUp_PengembalianLPJ(Strno_pengembalian,strketerangan_pengembalian,totalkembali,strpreview)
function setFrmLookUp_PengembalianLPJ(Strno_pengembalian,strpreview,totalkembali,strketerangan_pengembalian)

{
    var lebar = 480;
	
	totalviewkembali = formatCurrencyDec(totalkembali);
	console.log(totalkembali);
	//totalkembali = formatCurrency(totalkembali);

    setLookUps_PengembalianLPJ = new Ext.Window
    (
        {
            id: 'SetLookUps_PengembalianLPJ',
            title: NMform_PengembalianLPJ, 
            closeAction: 'destroy',
            y:90,
            width: lebar,
            height: 184,
            resizable:false,
            border: false,
            plain: true,
            layout: 'fit',
            iconCls: 'Penerimaan',
            modal: true,
            // items: getFormEntry_PengembalianLPJ(lebar,Strno_pengembalian,strketerangan_pengembalian,totalkembali),
            items: getFormEntry_PengembalianLPJ(lebar,Strno_pengembalian,strketerangan_pengembalian,totalkembali),
            listeners:
            {
                activate: function() 
                {
                },
                afterShow: function() 
                { 
                    this.activate(); 
					setLookUps_PengembalianLPJ.close();					
                },
                deactivate: function()
                {
                    					
                }
            }
        }
    );      
	if (strpreview =="simpan")
	{
		console.log(strpreview);
		Ext.getCmp('btnOkdata_PengembalianLPJ').setDisabled(false);
	}
	else
	{
		console.log(strpreview);
		Ext.getCmp('btnOkdata_PengembalianLPJ').setDisabled(true);
	}
    setLookUps_PengembalianLPJ.show();
}



function getFormEntry_PengembalianLPJ(lebar,Strno_pengembalian,strketerangan_pengembalian,totalkembali)
{
	console.log(strketerangan_pengembalian);
    var pnlForm_PengembalianLPJ = new Ext.FormPanel
    (
        {
            region: 'center',
            padding: '5px',
            layout: 'fit',
            items: 
			[
                {
                    xtype: 'panel',
                    layout: 'form',
                    height: 70,
                    padding: '5px',
                    labelWidth: 100,
                    items: 
					[
                        //getItemOp_PengembalianLPJ(),
						{
							xtype: 'compositefield',
							fieldLabel: 'No. Pengembalian',
							anchor: '100%',
							items: 
							[
								{
									xtype: 'textfield',
									id: 'txtNoIntegrasi_PengembalianLPJ',
									labelSeparator: '',
									width: 120,
									fieldLabel: '',
									value:Strno_pengembalian,
									readOnly:true
								},
								{
									xtype: 'datefield',
									fieldLabel: 'Tanggal ',
									id: 'dtpTanggal_Pengembalian',
									name: 'dtpTanggal_Pengembalian',
									format: 'd/M/Y',
									value:now_LPJ,
									anchor: '100%'
								}
							]
						},
						{
							xtype: 'textfield',
							id: 'txtJumlah_PengembalianLPJ',
							labelSeparator: '',
							width: 300,
							fieldLabel: 'Jumlah',
							value:totalviewkembali,
							readOnly:true,
							style:
								{	
									'text-align':'right',
									'font-weight':'bold'
								}
						},
						
						{
							xtype: 'textfield',
							id: 'txtKeterangan_PengembalianLPJ',
							labelSeparator: '',
							width: 300,
							fieldLabel: 'Catatan',
							value:strketerangan_pengembalian,
							readOnly:false
						},
						{
							layout: 'hBox',
							border: false,
							defaults: { margins: '0 5 0 0' },
							style:{'margin-left':'21px','margin-top':'5px'},
							anchor: '100%',
							layoutConfig: 
							{
								padding: '3',
								pack: 'end',
								align: 'middle'
							},
							items: 
							[
								{
									xtype: 'button',
									text: 'Simpan',
									width: 70,
									hideLabel: true,
									id: 'btnOkdata_PengembalianLPJ',
									handler: function() 
									{
										if(ValidasiEntry_PengembalianLPJ() == 1){
											Simpan_Pengembalian(
												Ext.get('txtNoIntegrasi_PengembalianLPJ').getValue(),
												Ext.get('txtKeterangan_PengembalianLPJ').getValue(),
												totalkembali,
												Ext.get('dtpTanggal_Pengembalian').getValue()
												// Ext.get('txtNoPembayaran_PengembalianLPJ').getValue()
											)									
											setLookUps_PengembalianLPJ.close();	
										}else{
											ShowPesanWarning_PengembalianLPJ('Data tidak berhasil di integrasikan','Pengembalian');
										}
									}
								},
								{
									xtype: 'button',
									text: 'Cancel',
									width: 70,
									hideLabel: true,
									id: 'btnCanceldata_PengembalianLPJ',
									handler: function() 
									{
										setLookUps_PengembalianLPJ.close();
									}
								},
								{
									xtype: 'button',
									text: 'Cetak',
									width: 70,
									hideLabel: true,
									id: 'btnCetakdata_PengembalianLPJ',
									handler: function() 
									{
										if(ValidasiEntrySet_PengembalianLPJ() == 1)
											{
												var criteria = GetCriteria_PengembalianView();
												ShowReport('', '020408', criteria);
											}
									}
								}
							]
						}
                    ]
                }
            ]            
        }
    );
        
    return pnlForm_PengembalianLPJ;     
}

function getItemOp_PengembalianLPJ()
{
	var cboOP_PengembalianLPJ = new Ext.form.ComboBox
	(
		{
			id:'opt_PengembalianLPJ',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Jenis Pembayaran',		
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: [[1, "Gaji"], [2, "THR"], [3, "TTT"], [4, "TAT"], [5, "Honor Mengajar"], [6, "Transport"]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			listeners:  
			{
				
			}
		}
	);
	return cboOP_PengembalianLPJ;
}

function ValidasiEntry_PengembalianLPJ()
{
	var x = 1;
	
	if (Ext.get('txtKeterangan_PengembalianLPJ').getValue()=='')
	{
		ShowPesanWarning_PengembalianLPJ('Keterangan belum di isi','integrasi SIM SDM');
		x=0;
	};
	
											
	return x;
};

function ValidasiEntrySet_PengembalianLPJ(modul)
{
    var x = 1;
	if (Ext.getCmp('txtNoIntegrasi_PengembalianLPJ').getValue() === '')
	{
		
		ShowPesanWarning_PengembalianLPJ(' No. Pengembalian Masih kosong',modul);
		x=0;
	}

    return x;
}


function GetCriteria_PengembalianView()
{
    var x = Ext.getCmp('txtNoIntegrasi_PengembalianLPJ').getValue() ;
	
    return x;
}
function ShowPesanWarning_PengembalianLPJ(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.WARNING,
           width :250
        }
    )
}

function ShowPesanError_PengembalianLPJ(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.ERROR,
           width :250
        }
    )
}

function ShowPesanInfo_PengembalianLPJ(str,modul)
{
    Ext.MessageBox.show
    (
        {
           title: modul,
           msg:str,
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.INFO,
           width :250
        }
    )
}