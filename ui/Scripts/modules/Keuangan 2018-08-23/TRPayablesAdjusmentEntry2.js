var dataSource_viTRPayablesAdjusmentEntry;
var selectCount_viTRPayablesAdjusmentEntry = 50;
var NamaForm_viTRPayablesAdjusmentEntry = "Payables Adjusment Entry ";
var mod_name_viTRPayablesAdjusmentEntry = "viTRPayablesAdjusmentEntry";
var now_viTRPayablesAdjusmentEntry = new Date();
var addNew_viTRPayablesAdjusmentEntry;
var rowSelected_viTRPayablesAdjusmentEntry;
var setLookUps_viTRPayablesAdjusmentEntry;
var mNoKunjungan_viTRPayablesAdjusmentEntry = '';

var CurrentData_viTRPayablesAdjusmentEntry =
        {
            data: Object,
            details: Array,
            row: 0
        };

CurrentPage.page = dataGrid_viTRPayablesAdjusmentEntry(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dataGrid_viTRPayablesAdjusmentEntry(mod_id_viTRPayablesAdjusmentEntry)
{

    var FieldMaster_viTRPayablesAdjusmentEntry =
            [
                'NO_KUNJUNGAN', 'KD_KELOMPOK', 'KD_UNIT', 'KD_DOKTER', 'KD_Vendor', 'KD_PASIEN',
                'TGL_KUNJUNGAN', 'JAM_KUNJUNGAN', 'TINGGI_BADAN', 'BERAT_BADAN', 'TEKANAN_DRH',
                'NADI', 'ALERGI', 'KELUHAN', 'RUJUK_RAD', 'RUJUK_LAB', 'TAHAP_PROSES', 'PASIEN_BARU',
                'NAMA_UNIT', 'KELOMPOK', 'DOKTER', 'Vendor', 'PS_BARU', 'KD_PENDIDIKAN', 'KD_STS_MARITAL',
                'KD_AGAMA', 'KD_PEKERJAAN', 'NAMA', 'TEMPAT_LAHIR', 'TGL_LAHIR', 'JENIS_KELAMIN', 'ALAMAT',
                'NO_TELP', 'NO_HP', 'GOL_DARAH', 'PENDIDIKAN', 'STS_MARITAL', 'AGAMA', 'PEKERJAAN', 'JNS_KELAMIN',
                'TAHUN', 'BULAN', 'HARI'
            ];

    dataSource_viTRPayablesAdjusmentEntry = new WebApp.DataStore
            ({
                fields: FieldMaster_viTRPayablesAdjusmentEntry
            });


    var GridDataView_viTRPayablesAdjusmentEntry = new Ext.grid.EditorGridPanel
            (
                    {
                        xtype: 'editorgrid',
                        title: '',
                        store: dataSource_viTRPayablesAdjusmentEntry,
                        autoScroll: true,
                        columnLines: true,
                        border: false,
                        anchor: '100% 100%',
                        plugins: [new Ext.ux.grid.FilterRow()],
                        selModel: new Ext.grid.RowSelectionModel
                                // Tanda aktif saat salah satu baris dipilih # --------------
                                        (
                                                {
                                                    singleSelect: true,
                                                    listeners:
                                                            {
                                                                rowselect: function (sm, row, rec)
                                                                {
                                                                    rowSelected_viTRPayablesAdjusmentEntry = undefined;
                                                                    rowSelected_viTRPayablesAdjusmentEntry = dataSource_viTRPayablesAdjusmentEntry.getAt(row);
                                                                    CurrentData_viTRPayablesAdjusmentEntry
                                                                    CurrentData_viTRPayablesAdjusmentEntry.row = row;
                                                                    CurrentData_viTRPayablesAdjusmentEntry.data = rowSelected_viTRPayablesAdjusmentEntry.data;
                                                                }
                                                            }
                                                }
                                        ),
                                // Proses eksekusi baris yang dipilih # --------------
                                listeners:
                                        {
                                            // Function saat ada event double klik maka akan muncul form view # --------------
                                            rowdblclick: function (sm, ridx, cidx)
                                            {
                                                rowSelected_viTRPayablesAdjusmentEntry = dataSource_viTRPayablesAdjusmentEntry.getAt(ridx);
                                                if (rowSelected_viTRPayablesAdjusmentEntry != undefined)
                                                {
                                                    setLookUp_viTRPayablesAdjusmentEntry(rowSelected_viTRPayablesAdjusmentEntry.data);
                                                }
                                                else
                                                {
                                                    setLookUp_viTRPayablesAdjusmentEntry();
                                                }
                                            }
                                            // End Function # --------------
                                        },
                                /**
                                 *	Mengatur tampilan pada Grid Kasir Rawat Jalan
                                 *	Terdiri dari : Judul, Isi dan Event
                                 *	Isi pada Grid di dapat dari pemangilan dari Net.
                                 */
                                colModel: new Ext.grid.ColumnModel
                                        (
                                                [
                                                    new Ext.grid.RowNumberer(),
                                                    {
                                                        id: 'colNumber_viTRPayablesAdjusmentEntry',
                                                        header: 'Number',
                                                        dataIndex: '',
                                                        sortable: true,
                                                        width: 35,
                                                        filter:
                                                                {
                                                                    type: 'int'
                                                                }
                                                    },
                                                    //-------------- ## --------------
                                                    {
                                                        id: 'colTglJournal_viTRPayablesAdjusmentEntry',
                                                        header: 'Tanggal',
                                                        dataIndex: '',
                                                        sortable: true,
                                                        width: 35,
                                                        filter:
                                                                {}
                                                    },
                                                    //-------------- ## --------------
                                                    {
                                                        id: 'colCustFilter_viTRPayablesAdjusmentEntry',
                                                        header: 'Notes',
                                                        dataIndex: '',
                                                        width: 200,
                                                        sortable: true,
                                                        filter: {}
                                                    },
                                                ]
                                                ),
                                // Tolbar ke Dua # --------------
                                tbar:
                                        {
                                            xtype: 'toolbar',
                                            id: 'toolbar_viTRPayablesAdjusmentEntry',
                                            items:
                                                    [
                                                        {
                                                            xtype: 'button',
                                                            text: 'Edit Data',
                                                            iconCls: 'Edit_Tr',
                                                            tooltip: 'Edit Data',
                                                            id: 'btnEdit_viTRPayablesAdjusmentEntry',
                                                            handler: function (sm, row, rec)
                                                            {
                                                                if (rowSelected_viTRPayablesAdjusmentEntry != undefined)
                                                                {
                                                                    setLookUp_viTRPayablesAdjusmentEntry(rowSelected_viTRPayablesAdjusmentEntry.data)
                                                                }
                                                                else
                                                                {
                                                                    setLookUp_viTRPayablesAdjusmentEntry();
                                                                }
                                                            }
                                                        }
                                                        //-------------- ## --------------
                                                    ]
                                        },
                                // End Tolbar ke Dua # --------------
                                // Button Bar Pagging # --------------
                                // Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
                                bbar: bbar_paging(mod_name_viTRPayablesAdjusmentEntry, selectCount_viTRPayablesAdjusmentEntry, dataSource_viTRPayablesAdjusmentEntry),
                                // End Button Bar Pagging # --------------
                                viewConfig:
                                        {
                                            forceFit: true
                                        }
                            }
                            )

                    // Kriteria filter pada Grid # --------------
                    var FrmFilterGridDataView_viTRPayablesAdjusmentEntry = new Ext.Panel
                            (
                                    {
                                        title: NamaForm_viTRPayablesAdjusmentEntry,
                                        iconCls: 'Studi_Lanjut',
                                        id: mod_id_viTRPayablesAdjusmentEntry,
                                        region: 'center',
                                        layout: 'form',
                                        closable: true,
                                        border: false,
                                        margins: '0 5 5 0',
                                        items: [GridDataView_viTRPayablesAdjusmentEntry],
                                        tbar:
                                                [
                                                    {
                                                        //-------------- # Untuk mengelompokkan pencarian # --------------
                                                        xtype: 'buttongroup',
                                                        //title: 'Pencarian ' + NamaForm_viTRPayablesAdjusmentEntry,
                                                        columns: 12,
                                                        defaults: {
                                                            scale: 'small'
                                                        },
                                                        frame: false,
                                                        //-------------- ## --------------
                                                        items:
                                                                [
                                                                    {
                                                                        xtype: 'tbtext',
                                                                        text: 'Number : ',
                                                                        style: {'text-align': 'left'},
                                                                        width: 90,
                                                                        height: 25
                                                                    },
                                                                    //viCombo_JournalCode(125, "ComboFilterJournalCode_viTRPayablesAdjusmentEntry"),	
                                                                    //-------------- ## --------------
                                                                    {
                                                                        xtype: 'textfield',
                                                                        id: 'TxtNumberFilter_viGeneralLedger',
                                                                        emptyText: 'Number...',
                                                                        width: 150,
                                                                    },
                                                                    {
                                                                        xtype: 'tbspacer',
                                                                        width: 15,
                                                                        height: 25
                                                                    },
                                                                    //-------------- ## --------------
                                                                    {
                                                                        xtype: 'tbtext',
                                                                        text: 'Tanggal : ',
                                                                        style: {'text-align': 'left'},
                                                                        width: 50,
                                                                        height: 25
                                                                    },
                                                                    //-------------- ## --------------						
                                                                    {
                                                                        xtype: 'datefield',
                                                                        id: 'txtfilterTglKunjAwal_viTRPayablesAdjusmentEntry',
                                                                        value: now_viTRPayablesAdjusmentEntry,
                                                                        format: 'd/M/Y',
                                                                        width: 120,
                                                                        listeners:
                                                                                {
                                                                                    'specialkey': function ()
                                                                                    {
                                                                                        if (Ext.EventObject.getKey() === 13)
                                                                                        {
                                                                                            datarefresh_viTRPayablesAdjusmentEntry();
                                                                                        }
                                                                                    }
                                                                                }
                                                                    },
                                                                    {
                                                                        xtype: 'tbtext',
                                                                        text: 's/d ',
                                                                        style: {'text-align': 'center'},
                                                                        width: 50,
                                                                        height: 25
                                                                    },
                                                                    {
                                                                        xtype: 'datefield',
                                                                        id: 'txtfilterTglKunjAkhir_viTRPayablesAdjusmentEntry',
                                                                        value: now_viTRPayablesAdjusmentEntry,
                                                                        format: 'd/M/Y',
                                                                        width: 120,
                                                                        listeners:
                                                                                {
                                                                                    'specialkey': function ()
                                                                                    {
                                                                                        if (Ext.EventObject.getKey() === 13)
                                                                                        {
                                                                                            datarefresh_viTRPayablesAdjusmentEntry();
                                                                                        }
                                                                                    }
                                                                                }
                                                                    },
                                                                    //-------------- ## --------------
                                                                    // { 
                                                                    // xtype: 'tbspacer',
                                                                    // width: 15,
                                                                    // height: 25
                                                                    // },
                                                                    //-------------- ## --------------						
                                                                    {
                                                                        xtype: 'tbspacer',
                                                                        width: 15,
                                                                        height: 25
                                                                    },
                                                                    {
                                                                        xtype: 'button',
                                                                        text: 'Cari',
                                                                        iconCls: 'refresh',
                                                                        tooltip: 'Cari',
                                                                        style: {paddingLeft: '30px'},
                                                                        //rowspan: 3,
                                                                        width: 150,
                                                                        id: 'BtnFilterGridDataView_viTRPayablesAdjusmentEntry',
                                                                        handler: function ()
                                                                        {
                                                                            DfltFilterBtn_viTRPayablesAdjusmentEntry = 1;
                                                                            DataRefresh_viTRPayablesAdjusmentEntry(getCriteriaFilterGridDataView_viTRPayablesAdjusmentEntry());
                                                                        }
                                                                    },
                                                                    //-------------- ## --------------
                                                                ]
                                                                //-------------- # End items # --------------
                                                                //-------------- # End mengelompokkan pencarian # --------------
                                                    }
                                                ]
                                                //-------------- # End tbar # --------------
                                    }
                            )
                    return FrmFilterGridDataView_viTRPayablesAdjusmentEntry;
                    //-------------- # End form filter # --------------
                }
// End Function dataGrid_viTRPayablesAdjusmentEntry # --------------

        /**
         *	Function : setLookUp_viTRPayablesAdjusmentEntry
         *	
         *	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
         */

        function setLookUp_viTRPayablesAdjusmentEntry(rowdata)
        {
            var lebar = 760;
            setLookUps_viTRPayablesAdjusmentEntry = new Ext.Window
                    (
                            {
                                id: 'SetLookUps_viTRPayablesAdjusmentEntry',
                                name: 'SetLookUps_viTRPayablesAdjusmentEntry',
                                title: NamaForm_viTRPayablesAdjusmentEntry,
                                closeAction: 'destroy',
                                width: 775,
                                height: 610,
                                resizable: false,
                                autoScroll: false,
                                border: true,
                                constrainHeader: true,
                                iconCls: 'Studi_Lanjut',
                                modal: true,
                                items: getFormItemEntry_viTRPayablesAdjusmentEntry(lebar, rowdata), //1
                                listeners:
                                        {
                                            activate: function ()
                                            {

                                            },
                                            afterShow: function ()
                                            {
                                                this.activate();
                                            },
                                            deactivate: function ()
                                            {
                                                rowSelected_viTRPayablesAdjusmentEntry = undefined;
                                                //datarefresh_viTRPayablesAdjusmentEntry();
                                                mNoKunjungan_viTRPayablesAdjusmentEntry = '';
                                            }
                                        }
                            }
                    );

            setLookUps_viTRPayablesAdjusmentEntry.show();
            if (rowdata == undefined)
            {
                // dataaddnew_viTRPayablesAdjusmentEntry();
                // Ext.getCmp('btnDelete_viTRPayablesAdjusmentEntry').disable();	
            }
            else
            {
                // datainit_viTRPayablesAdjusmentEntry(rowdata);
            }
        }
// End Function setLookUpGridDataView_viTRPayablesAdjusmentEntry # --------------

        /**
         *	Function : getFormItemEntry_viTRPayablesAdjusmentEntry
         *	
         *	Sebuah fungsi untuk menampilkan penataan menu dan isian form edit
         */

        function getFormItemEntry_viTRPayablesAdjusmentEntry(lebar, rowdata)
        {
            var pnlFormDataBasic_viTRPayablesAdjusmentEntry = new Ext.FormPanel
                    (
                            {
                                title: '',
                                region: 'north',
                                layout: 'form',
                                bodyStyle: 'padding:10px 10px 10px 10px',
                                anchor: '100%',
                                width: lebar,
                                height: 600,
                                border: false,
                                //-------------- #items# --------------
                                items:
                                        [
                                            getItemPanelInputBiodata_viTRPayablesAdjusmentEntry(lebar),
                                            //-------------- ## -------------- 
                                            //getItemDataKunjungan_viTRPayablesAdjusmentEntry(lebar), 
                                            //-------------- ## --------------
                                            getItemGridTransaksi_viTRPayablesAdjusmentEntry(lebar),
                                            //-------------- ## --------------
                                            {
                                                xtype: 'compositefield',
                                                fieldLabel: ' ',
                                                anchor: '100%',
                                                labelSeparator: '',
                                                //width: 199,
                                                style: {'margin-top': '7px'},
                                                items:
                                                        [
                                                            {
                                                                xtype: 'button',
                                                                text: 'Posting',
                                                                width: 70,
                                                                style: {'text-align': 'right', 'margin-left': '10px'},
                                                                hideLabel: true,
                                                                id: 'btnFindRecord_viTRPayablesAdjusmentEntry',
                                                                name: 'btnFindRecord_viTRPayablesAdjusmentEntry',
                                                                handler: function ()
                                                                {
                                                                }
                                                            },
                                                            {
                                                                xtype: 'textfield',
                                                                id: 'txtJumlah1EditData_viTRPayablesAdjusmentEntry',
                                                                name: 'txtJumlah1EditData_viTRPayablesAdjusmentEntry',
                                                                style: {'text-align': 'right', 'margin-left': '345px'},
                                                                width: 100,
                                                                value: 0,
                                                                readOnly: true,
                                                            },
                                                            {
                                                                xtype: 'textfield',
                                                                id: 'txtJumlah2EditData_viTRPayablesAdjusmentEntry',
                                                                name: 'txtJumlah2EditData_viTRPayablesAdjusmentEntry',
                                                                style: {'text-align': 'right', 'margin-left': '350px'},
                                                                width: 100,
                                                                value: 0,
                                                                readOnly: true,
                                                            },
                                                                    //-------------- ## --------------
                                                        ]
                                            },
                                            //-------------- ## --------------
                                        ],
                                //-------------- #End items# --------------
                                fileUpload: true,
                                // Tombol pada tollbar Edit Data Pasien
                                tbar:
                                        {
                                            xtype: 'toolbar',
                                            items:
                                                    [
                                                        {
                                                            xtype: 'button',
                                                            text: 'Add',
                                                            iconCls: 'add',
                                                            id: 'btnAdd_viTRPayablesAdjusmentEntry',
                                                            handler: function () {
                                                                dataaddnew_viTRPayablesAdjusmentEntry();
                                                            }
                                                        },
                                                        //-------------- ## --------------
                                                        {
                                                            xtype: 'tbseparator'
                                                        },
                                                        //-------------- ## --------------
                                                        {
                                                            xtype: 'button',
                                                            text: 'Save',
                                                            iconCls: 'save',
                                                            id: 'btnSimpan_viTRPayablesAdjusmentEntry',
                                                            handler: function ()
                                                            {
                                                                datasave_viTRPayablesAdjusmentEntry(addNew_viTRPayablesAdjusmentEntry);
                                                                datarefresh_viTRPayablesAdjusmentEntry();
                                                            }
                                                        },
                                                        //-------------- ## --------------
                                                        {
                                                            xtype: 'tbseparator'
                                                        },
                                                        //-------------- ## --------------
                                                        {
                                                            xtype: 'button',
                                                            text: 'Save & Close',
                                                            iconCls: 'saveexit',
                                                            id: 'btnSimpanExit_viTRPayablesAdjusmentEntry',
                                                            handler: function ()
                                                            {
                                                                var x = datasave_viTRPayablesAdjusmentEntry(addNew_viTRPayablesAdjusmentEntry);
                                                                datarefresh_viTRPayablesAdjusmentEntry();
                                                                if (x === undefined)
                                                                {
                                                                    setLookUps_viTRPayablesAdjusmentEntry.close();
                                                                }
                                                            }
                                                        },
                                                        //-------------- ## --------------
                                                        {
                                                            xtype: 'tbseparator'
                                                        },
                                                        //-------------- ## --------------
                                                        {
                                                            xtype: 'button',
                                                            text: 'Delete',
                                                            iconCls: 'remove',
                                                            id: 'btnDelete_viTRPayablesAdjusmentEntry',
                                                            handler: function ()
                                                            {
                                                                datadelete_viTRPayablesAdjusmentEntry();
                                                                datarefresh_viTRPayablesAdjusmentEntry();

                                                            }
                                                        },
                                                        //-------------- ## --------------
                                                        {
                                                            xtype: 'tbseparator'
                                                        },
                                                        //-------------- ## --------------
                                                        {
                                                            text: 'Lookup ',
                                                            iconCls: 'find',
                                                            menu:
                                                                    [
                                                                        {
                                                                            text: 'Find Record',
                                                                            id: 'btnLookUpRecord_viTRPayablesAdjusmentEntry',
                                                                            iconCls: 'find',
                                                                            handler: function () {
                                                                                FormSetLookupRecord_viTRPayablesAdjusmentEntry('1', '2');
                                                                            },
                                                                        },
                                                                                //-------------- ## --------------
                                                                    ],
                                                        },
                                                                //-------------- ## --------------


                                                    ]
                                        }//,items:
                            }
                    )

            return pnlFormDataBasic_viTRPayablesAdjusmentEntry;
        }
// End Function getFormItemEntry_viTRPayablesAdjusmentEntry # --------------

        /**
         *	Function : getItemPanelInputBiodata_viTRPayablesAdjusmentEntry
         *	
         *	Sebuah fungsi untuk menampilkan isian form edit data
         */

        function getItemPanelInputBiodata_viTRPayablesAdjusmentEntry(lebar)
        {
            var rdType_viTRPayablesAdjusmentEntry = new Ext.form.RadioGroup
                    ({
                        fieldLabel: '',
                        columns: 2,
                        width: 200, //400,
                        border: false,
                        items:
                                [
                                    {
                                        boxLabel: 'Debit',
                                        width: 50,
                                        id: 'rdDebit_viTRPayablesAdjusmentEntry',
                                        name: 'rdDebit_viTRPayablesAdjusmentEntry',
                                        inputValue: '1'
                                    },
                                    //-------------- ## --------------
                                    {
                                        boxLabel: 'Kredit',
                                        width: 50,
                                        id: 'rdKredit_viTRPayablesAdjusmentEntry',
                                        name: 'rdKredit_viTRPayablesAdjusmentEntry',
                                        inputValue: '2'
                                    }
                                    //-------------- ## --------------
                                ]
                    });
            //-------------- ## --------------

            var items =
                    {
                        layout: 'Form',
                        anchor: '100%',
                        width: lebar - 80,
                        labelAlign: 'Left',
                        bodyStyle: 'padding:10px 10px 10px 10px',
                        border: true,
                        items:
                                [
                                    {
                                        xtype: 'compositefield',
                                        fieldLabel: 'Number',
                                        anchor: '100%',
                                        //labelSeparator: '',
                                        width: 199,
                                        items:
                                                [
                                                    {
                                                        xtype: 'textfield',
                                                        flex: 1,
                                                        fieldLabel: 'Label',
                                                        width: 100,
                                                        name: 'txtNumber_viTRPayablesAdjusmentEntry',
                                                        id: 'txtNumber_viTRPayablesAdjusmentEntry',
                                                        emptyText: 'Number',
                                                        listeners:
                                                                {
                                                                    'specialkey': function ()
                                                                    {
                                                                        if (Ext.EventObject.getKey() === 13)
                                                                        {
                                                                        }
                                                                        ;
                                                                    }
                                                                }
                                                    },
                                                    {
                                                        xtype: 'displayfield',
                                                        width: 300,
                                                        value: ' ',
                                                        fieldLabel: 'Label',
                                                    },
                                                    {
                                                        xtype: 'displayfield',
                                                        width: 65,
                                                        value: 'Trans. Date',
                                                        fieldLabel: 'Label',
                                                    },
                                                    {
                                                        xtype: 'datefield',
                                                        fieldLabel: 'Tanggal',
                                                        name: 'TransDate_viTRPayablesAdjusmentEntry',
                                                        id: 'TransDate_viTRPayablesAdjusmentEntry',
                                                        format: 'd/M/Y',
                                                        width: 130,
                                                        value: now_viTRPayablesAdjusmentEntry,
                                                        //style:{'text-align':'left','margin-left':'210px'},
                                                    },
                                                ]
                                    },
                                    {
                                        xtype: 'compositefield',
                                        fieldLabel: 'Vendor',
                                        name: 'compVendor_viTRPayablesAdjusmentEntry',
                                        id: 'compVendor_viTRPayablesAdjusmentEntry',
                                        items:
                                                [
                                                    viCombo_VendorAP(405, "ComboVendorAP_viTRPayablesAdjusmentEntry"),
                                                    {
                                                        xtype: 'displayfield',
                                                        width: 65,
                                                        value: 'Due. Date',
                                                        fieldLabel: 'Label',
                                                    },
                                                    {
                                                        xtype: 'datefield',
                                                        fieldLabel: 'Tanggal',
                                                        name: 'DueDate_viTRPayablesAdjusmentEntry',
                                                        id: 'DueDate_viTRPayablesAdjusmentEntry',
                                                        format: 'd/M/Y',
                                                        width: 130,
                                                        value: now_viTRPayablesAdjusmentEntry,
                                                        //style:{'text-align':'left','margin-left':'210px'},
                                                    },
                                                ]
                                    },
                                    {
                                        xtype: 'compositefield',
                                        fieldLabel: 'Notes ',
                                        name: 'compNotes_viTRPayablesAdjusmentEntry',
                                        id: 'compNotes_viTRPayablesAdjusmentEntry',
                                        items:
                                                [
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: '',
                                                        width: 610,
                                                        name: 'txtNotes_viTRPayablesAdjusmentEntry',
                                                        id: 'txtNotes_viTRPayablesAdjusmentEntry',
                                                        emptyText: ' ',
                                                    },
                                                ]
                                    },
                                    {
                                        xtype: 'compositefield',
                                        fieldLabel: 'Periode',
                                        name: 'compPeriode_viTRPayablesAdjusmentEntry',
                                        id: 'compPeriode_viTRPayablesAdjusmentEntry',
                                        items:
                                                [
                                                    viCombo_PeriodeRL(110, "ComboPeriodeRL_viGeneralLedger"),
                                                    {
                                                        xtype: 'button',
                                                        text: 'Load',
                                                        width: 70,
                                                        //style:{'margin-left':'190px','margin-top':'7px'},
                                                        hideLabel: true,
                                                        id: 'btnLoadPeriode_viTRPayablesAdjusmentEntry',
                                                        handler: function ()
                                                        {
                                                        }
                                                    }

                                                ]
                                    },
                                    //-------------- ## --------------	


                                ]
                    };
            return items;
        }
        ;
// End Function getItemPanelInputBiodata_viTRPayablesAdjusmentEntry # --------------

        /**
         *	Function : getItemDataKunjungan_viTRPayablesAdjusmentEntry
         *	
         *	Sebuah fungsi untuk menampilkan isian form edit data
         */

        function getItemDataKunjungan_viTRPayablesAdjusmentEntry(lebar)
        {
            var items =
                    {
                        layout: 'form',
                        anchor: '100%',
                        labelAlign: 'Left',
                        bodyStyle: 'padding:10px 10px 10px 10px',
                        width: lebar - 80,
                        height: 30,
                        items:
                                [
                                ]
                    };
            return items;
        }
        ;
// End Function getItemDataKunjungan_viTRPayablesAdjusmentEntry # --------------

        /**
         *	Function : getItemGridTransaksi_viTRPayablesAdjusmentEntry
         *	
         *	Sebuah fungsi untuk menampilkan isian form edit data
         */

        function getItemGridTransaksi_viTRPayablesAdjusmentEntry(lebar)
        {
            var items =
                    {
                        title: '',
                        layout: 'form',
                        anchor: '100%',
                        labelAlign: 'Left',
                        bodyStyle: 'padding:10px 10px 10px 10px',
                        border: true,
                        width: lebar - 80,
                        height: 380,
                        items:
                                [
                                    {
                                        labelWidth: 105,
                                        layout: 'form',
                                        labelAlign: 'Left',
                                        border: false,
                                        items:
                                                [
                                                    gridDataViewEdit_viTRPayablesAdjusmentEntry()
                                                ]
                                    }
                                    //-------------- ## --------------
                                ]
                    };
            return items;
        }
        ;
// End Function getItemGridTransaksi_viTRPayablesAdjusmentEntry # --------------

        /**
         *	Function : gridDataViewEdit_viTRPayablesAdjusmentEntry
         *	
         *	Sebuah fungsi untuk menampilkan isian form edit data
         */

        function gridDataViewEdit_viTRPayablesAdjusmentEntry()
        {

            var FieldGrdKasir_viTRPayablesAdjusmentEntry =
                    [
                    ];

            dsDataGrdJab_viTRPayablesAdjusmentEntry = new WebApp.DataStore
                    ({
                        fields: FieldGrdKasir_viTRPayablesAdjusmentEntry
                    });

            var items =
                    {
                        xtype: 'editorgrid',
                        store: dsDataGrdJab_viTRPayablesAdjusmentEntry,
                        height: 355,
                        selModel: new Ext.grid.RowSelectionModel
                                (
                                        {
                                            singleSelect: true,
                                            listeners:
                                                    {
                                                        rowselect: function (sm, row, rec)
                                                        {
                                                        }
                                                    }
                                        }
                                ),
                        columns:
                                [
                                    {
                                        dataIndex: '',
                                        header: 'Account',
                                        sortable: true,
                                        width: 75
                                    },
                                    //-------------- ## --------------
                                    {
                                        dataIndex: '',
                                        header: 'Name',
                                        sortable: true,
                                        width: 200
                                    },
                                    //-------------- ## --------------			
                                    {
                                        dataIndex: '',
                                        header: 'Description',
                                        sortable: true,
                                        width: 200
                                    },
                                    //-------------- ## --------------
                                    {
                                        dataIndex: '',
                                        header: 'Debit',
                                        sortable: true,
                                        width: 100,
                                        renderer: function (v, params, record)
                                        {

                                        }
                                    },
                                    {
                                        dataIndex: '',
                                        header: 'Credit',
                                        sortable: true,
                                        width: 100,
                                        renderer: function (v, params, record)
                                        {

                                        }
                                    },
                                ]
                    }
            return items;
        }
// End Function gridDataViewEdit_viTRPayablesAdjusmentEntry # --------------

// ## MENU LOOKUP ## ------------------------------------------------------------------

// ## MENU LOOKUP - LOOKUP PRODUK/ TINDAKAN ## ------------------------------------------------------------------

        /**
         *   Function : FormSetLookupRecord_viTRPayablesAdjusmentEntry
         *   
         *   Sebuah fungsi untuk menampilkan windows popup Ganti Dokter
         */

        function FormSetLookupRecord_viTRPayablesAdjusmentEntry(subtotal, NO_KUNJUNGAN)
        {
            vWinFormEntryGantiDokter_viTRPayablesAdjusmentEntry = new Ext.Window
                    (
                            {
                                id: 'FormGrdLookupRecord_viTRPayablesAdjusmentEntry',
                                title: 'Find Record',
                                closeAction: 'destroy',
                                closable: true,
                                width: 390,
                                height: 125,
                                border: false,
                                plain: true,
                                resizable: false,
                                constrainHeader: true,
                                layout: 'form',
                                iconCls: 'Edit_Tr',
                                //padding: '8px',
                                modal: true,
                                items:
                                        [
                                            getFormItemEntryLookupfindrecord_viTRPayablesAdjusmentEntry(subtotal, NO_KUNJUNGAN),
                                        ],
                                listeners:
                                        {
                                            activate: function ()
                                            {
                                            }
                                        }
                            }
                    );
            vWinFormEntryGantiDokter_viTRPayablesAdjusmentEntry.show();
            mWindowGridLookupGantiDokter = vWinFormEntryGantiDokter_viTRPayablesAdjusmentEntry;
        }
        ;

// End Function FormSetLookupRecord_viTRPayablesAdjusmentEntry # --------------

        /**
         *   Function : getFormItemEntryLookupfindrecord_viTRPayablesAdjusmentEntry
         *   
         *   Sebuah fungsi untuk menampilkan isian form Ganti Dokter
         */
        function getFormItemEntryLookupfindrecord_viTRPayablesAdjusmentEntry(subtotal, NO_KUNJUNGAN)
        {
            var lebar = 500;
            var pnlFormDataFindRecordWindowPopup_viTRPayablesAdjusmentEntry = new Ext.FormPanel
                    (
                            {
                                title: '',
                                region: 'center',
                                layout: 'anchor',
                                padding: '8px',
                                bodyStyle: 'padding:10px 0px 10px 10px;',
                                fileUpload: true,
                                //-------------- #items# --------------
                                items:
                                        [
                                            getItemPanelInputFindRecordDataView_viTRPayablesAdjusmentEntry(lebar),
                                                    //-------------- ## --------------
                                        ],
                                //-------------- #End items# --------------
                            }
                    )
            return pnlFormDataFindRecordWindowPopup_viTRPayablesAdjusmentEntry;
        }
// End Function getFormItemEntryLookupfindrecord_viTRPayablesAdjusmentEntry # --------------

        /**
         *   Function : getItemPanelInputFindRecordDataView_viTRPayablesAdjusmentEntry
         *   
         *   Sebuah fungsi untuk menampilkan isian form Ganti Dokter
         */

        function getItemPanelInputFindRecordDataView_viTRPayablesAdjusmentEntry(lebar)
        {
            var items =
                    {
                        title: '',
                        layout: 'Form',
                        anchor: '100%',
                        width: lebar,
                        labelAlign: 'Left',
                        bodyStyle: 'padding:1px 1px 1px 1px',
                        border: false,
                        items:
                                [
                                    {
                                        xtype: 'compositefield',
                                        fieldLabel: 'Field Name',
                                        anchor: '100%',
                                        width: 250,
                                        items:
                                                [
                                                    viCombo_ReferenceRL(250, 'CmboFindReference_viTRPayablesAdjusmentEntry'),
                                                            //-------------- ## --------------  
                                                ]
                                    },
                                    //-------------- ## --------------
                                    {
                                        xtype: 'compositefield',
                                        fieldLabel: 'Operator',
                                        anchor: '100%',
                                        width: 250,
                                        items:
                                                [
                                                    viCombo_OperatorRL(250, 'CmboOperator_viTRPayablesAdjusmentEntry'),
                                                            //-------------- ## --------------  
                                                ]
                                    },
                                    //-------------- ## --------------
                                    {
                                        xtype: 'compositefield',
                                        fieldLabel: 'Key Word',
                                        anchor: '100%',
                                        width: 250,
                                        items:
                                                [
                                                    {
                                                        xtype: 'textfield',
                                                        id: 'TxtKeyWord_viTRPayablesAdjusmentEntry',
                                                        emptyText: ' ',
                                                        width: 150,
                                                    },
                                                ]
                                    },
                                    //-------------- ## --------------
                                    // {
                                    // xtype: 'compositefield',
                                    // id: 'findcancelbtn',
                                    // fieldLabel: 'find',
                                    // anchor: '100%',
                                    // //labelSeparator: '',
                                    // width: 199,
                                    // style:{'margin-top':'7px'},					
                                    // items: 
                                    // [
                                    // {
                                    // xtype:'button',
                                    // text:'Ok',
                                    // width:70,
                                    // //style:{'margin-left':'190px','margin-top':'7px'},
                                    // style:{'text-align':'right','margin-left':'60px'},
                                    // hideLabel:true,
                                    // id: 'btnFindRecord_viTRPayablesAdjusmentEntry',
                                    // name: 'btnFindRecord_viTRPayablesAdjusmentEntry',
                                    // handler:function()
                                    // {
                                    // }   
                                    // },
                                    // //-------------- ## --------------
                                    // {
                                    // xtype:'button',
                                    // text:'Cancel',
                                    // width:70,
                                    // //style:{'margin-left':'190px','margin-top':'7px'},
                                    // style:{'text-align':'right','margin-left':'90px'},
                                    // hideLabel:true,
                                    // id: 'btnCancelRecord_viTRPayablesAdjusmentEntry',
                                    // name: 'btnCancelRecord_viTRPayablesAdjusmentEntry',
                                    // handler:function()
                                    // {
                                    // }   
                                    // },                        
                                    // //-------------- ## --------------
                                    // ]
                                    // },  
                                ]
                    };
            return items;
        }
// End Function getItemPanelInputFindRecordDataView_viTRPayablesAdjusmentEntry # --------------


        function viCombo_VendorAP(lebar, Nama_ID)
        {
            var Field_VendorAP = ['KD_Vendor', 'Vendor'];
            ds_VendorAP = new WebApp.DataStore({fields: Field_VendorAP});

            // viRefresh_Journal();

            var cbo_VendorAP = new Ext.form.ComboBox
                    (
                            {
                                flex: 1,
                                fieldLabel: 'Vendor',
                                valueField: 'KD_Vendor',
                                displayField: 'Vendor',
                                emptyText: 'Vendor...',
                                store: ds_VendorAP,
                                width: lebar,
                                mode: 'local',
                                typeAhead: true,
                                triggerAction: 'all',
                                name: Nama_ID,
                                lazyRender: true,
                                id: Nama_ID,
                                listeners:
                                        {
                                            'specialkey': function ()
                                            {
                                                if (Ext.EventObject.getKey() === 13)
                                                {

                                                }
                                            }
                                        }
                            }
                    )
            return cbo_VendorAP;
        }

        function viCombo_OperatorRL(lebar, Nama_ID)
        {
            var Field_OperatorRL = ['OPERATOR'];
            ds_OperatorRL = new WebApp.DataStore({fields: Field_OperatorRL});

            // viRefresh_Journal();

            var cbo_OperatorRL = new Ext.form.ComboBox
                    (
                            {
                                flex: 1,
                                fieldLabel: 'Operator',
                                valueField: 'Operator',
                                displayField: 'Operator',
                                emptyText: 'Operator',
                                store: ds_OperatorRL,
                                width: lebar,
                                mode: 'local',
                                typeAhead: true,
                                triggerAction: 'all',
                                name: Nama_ID,
                                lazyRender: true,
                                id: Nama_ID,
                                listeners:
                                        {
                                            'specialkey': function ()
                                            {
                                                if (Ext.EventObject.getKey() === 13)
                                                {

                                                }
                                            }
                                        }
                            }
                    )
            return cbo_OperatorRL;
        }

        function viCombo_PeriodeRL(lebar, Nama_ID)
        {
            var Field_JournalRL = ['MONTH'];
            ds_JournalRL = new WebApp.DataStore({fields: Field_JournalRL});

            // viRefresh_Journal();

            var cbo_PeriodeBulanRL = new Ext.form.ComboBox
                    (
                            {
                                flex: 1,
                                fieldLabel: 'Journal',
                                valueField: 'MONTH',
                                displayField: 'Month',
                                emptyText: 'Periode Bulan...',
                                store: ds_JournalRL,
                                width: lebar,
                                mode: 'local',
                                typeAhead: true,
                                triggerAction: 'all',
                                name: Nama_ID,
                                lazyRender: true,
                                id: Nama_ID,
                                listeners:
                                        {
                                            'specialkey': function ()
                                            {
                                                if (Ext.EventObject.getKey() === 13)
                                                {

                                                }
                                            }
                                        }
                            }
                    )
            return cbo_PeriodeBulanRL;
        }

        function viCombo_ReferenceRL(lebar, Nama_ID)
        {
            var Field_ReferenceRL = ['REFERENCE'];
            ds_ReferenceRL = new WebApp.DataStore({fields: Field_ReferenceRL});

            var cbo_ReferenceRL = new Ext.form.ComboBox
                    (
                            {
                                flex: 1,
                                fieldLabel: 'Reference',
                                valueField: 'REFERENCE',
                                displayField: 'REFERENCE',
                                emptyText: 'Reference...',
                                store: ds_ReferenceRL,
                                width: lebar,
                                mode: 'local',
                                typeAhead: true,
                                triggerAction: 'all',
                                name: Nama_ID,
                                lazyRender: true,
                                id: Nama_ID,
                                listeners:
                                        {
                                            'specialkey': function ()
                                            {
                                                if (Ext.EventObject.getKey() === 13)
                                                {

                                                }
                                            }
                                        }
                            }
                    )
            return cbo_ReferenceRL;
        }

        function viCombo_PayMode(lebar, Nama_ID)
        {
            var Field_PayMode = [' '];
            ds_PayMode = new WebApp.DataStore({fields: Field_PayMode});

            var cbo_PayMode = new Ext.form.ComboBox
                    (
                            {
                                flex: 1,
                                fieldLabel: '',
                                valueField: '',
                                displayField: '',
                                emptyText: 'Pay mode...',
                                store: ds_PayMode,
                                width: lebar,
                                mode: 'local',
                                typeAhead: true,
                                triggerAction: 'all',
                                name: Nama_ID,
                                lazyRender: true,
                                id: Nama_ID,
                                listeners:
                                        {
                                            'specialkey': function ()
                                            {
                                                if (Ext.EventObject.getKey() === 13)
                                                {

                                                }
                                            }
                                        }
                            }
                    )
            return cbo_PayMode;
        }
        function viCombo_Curr(lebar, Nama_ID)
        {
            var Field_Curr = [' '];
            ds_Curr = new WebApp.DataStore({fields: Field_Curr});

            var cbo_PayModeCurr = new Ext.form.ComboBox
                    (
                            {
                                flex: 1,
                                fieldLabel: '',
                                valueField: '',
                                displayField: '',
                                emptyText: 'Rp...',
                                store: ds_Curr,
                                width: lebar,
                                mode: 'local',
                                typeAhead: true,
                                triggerAction: 'all',
                                name: Nama_ID,
                                lazyRender: true,
                                id: Nama_ID,
                                listeners:
                                        {
                                            'specialkey': function ()
                                            {
                                                if (Ext.EventObject.getKey() === 13)
                                                {

                                                }
                                            }
                                        }
                            }
                    )
            return cbo_PayModeCurr;
        }