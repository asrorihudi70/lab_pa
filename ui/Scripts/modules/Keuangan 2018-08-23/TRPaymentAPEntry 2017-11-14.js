// Data Source ExtJS # --------------

/**
 *	Nama File 		: TRTRPaymentAPEntry.js
 *	Menu 			: 
 *	Model id 		: 
 *	Keterangan 		: Kasir Rawat Jalan adalah proses untuk pembayaran pasien pada rawat jalan
 *	Di buat tanggal : 13 Agustus 2014
 *	Di EDIT tanggal : 14 Agustus 2014
 *	Oleh 			: ADE. S
 *	Editing			: SDY_RI
 */

// Deklarasi Variabel pada Kasir Rawat Jalan # --------------

var dataSource_viTRPaymentAPEntry;
var selectCount_viTRPaymentAPEntry = 50;
var NamaForm_viTRPaymentAPEntry = "Payment A/P Entry ";
var mod_name_viTRPaymentAPEntry = "viTRPaymentAPEntry";
var now_viTRPaymentAPEntry = new Date();
var addNew_viTRPaymentAPEntry;
var rowSelected_viTRPaymentAPEntry;
var setLookUps_viTRPaymentAPEntry;
var mNoKunjungan_viTRPaymentAPEntry = '';

var CurrentData_viTRPaymentAPEntry =
        {
            data: Object,
            details: Array,
            row: 0
        };

CurrentPage.page = dataGrid_viTRPaymentAPEntry(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

// End Deklarasi Variabel pada Kasir Rawat Jalan # --------------

// Start Project Kasir Rawat Jalan # --------------

// --------------------------------------- # Start Function # ---------------------------------------
// ## Silahkan tempatkan semua fungsi eksekusi disini

// --------------------------------------- # End Function # ---------------------------------------

// --------------------------------------- # Start Form # ---------------------------------------

/**
 *	Function : dataGrid_viTRPaymentAPEntry
 *	
 *	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
 */

function dataGrid_viTRPaymentAPEntry(mod_id_viTRPaymentAPEntry)
{
    // Field kiriman dari Project Net.
    var FieldMaster_viTRPaymentAPEntry =
            [
                'NO_KUNJUNGAN', 'KD_KELOMPOK', 'KD_UNIT', 'KD_DOKTER', 'KD_CUSTOMER', 'KD_PASIEN',
                'TGL_KUNJUNGAN', 'JAM_KUNJUNGAN', 'TINGGI_BADAN', 'BERAT_BADAN', 'TEKANAN_DRH',
                'NADI', 'ALERGI', 'KELUHAN', 'RUJUK_RAD', 'RUJUK_LAB', 'TAHAP_PROSES', 'PASIEN_BARU',
                'NAMA_UNIT', 'KELOMPOK', 'DOKTER', 'CUSTOMER', 'PS_BARU', 'KD_PENDIDIKAN', 'KD_STS_MARITAL',
                'KD_AGAMA', 'KD_PEKERJAAN', 'NAMA', 'TEMPAT_LAHIR', 'TGL_LAHIR', 'JENIS_KELAMIN', 'ALAMAT',
                'NO_TELP', 'NO_HP', 'GOL_DARAH', 'PENDIDIKAN', 'STS_MARITAL', 'AGAMA', 'PEKERJAAN', 'JNS_KELAMIN',
                'TAHUN', 'BULAN', 'HARI'
            ];
    // Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viTRPaymentAPEntry = new WebApp.DataStore
            ({
                fields: FieldMaster_viTRPaymentAPEntry
            });

    // Grid Kasir Rawat Jalan # --------------
    var GridDataView_viTRPaymentAPEntry = new Ext.grid.EditorGridPanel
            (
                    {
                        xtype: 'editorgrid',
                        title: '',
                        store: dataSource_viTRPaymentAPEntry,
                        autoScroll: true,
                        columnLines: true,
                        border: false,
                        anchor: '100% 100%',
                        plugins: [new Ext.ux.grid.FilterRow()],
                        selModel: new Ext.grid.RowSelectionModel
                                // Tanda aktif saat salah satu baris dipilih # --------------
                                        (
                                                {
                                                    singleSelect: true,
                                                    listeners:
                                                            {
                                                                rowselect: function (sm, row, rec)
                                                                {
                                                                    rowSelected_viTRPaymentAPEntry = undefined;
                                                                    rowSelected_viTRPaymentAPEntry = dataSource_viTRPaymentAPEntry.getAt(row);
                                                                    CurrentData_viTRPaymentAPEntry
                                                                    CurrentData_viTRPaymentAPEntry.row = row;
                                                                    CurrentData_viTRPaymentAPEntry.data = rowSelected_viTRPaymentAPEntry.data;
                                                                }
                                                            }
                                                }
                                        ),
                                // Proses eksekusi baris yang dipilih # --------------
                                listeners:
                                        {
                                            // Function saat ada event double klik maka akan muncul form view # --------------
                                            rowdblclick: function (sm, ridx, cidx)
                                            {
                                                rowSelected_viTRPaymentAPEntry = dataSource_viTRPaymentAPEntry.getAt(ridx);
                                                if (rowSelected_viTRPaymentAPEntry != undefined)
                                                {
                                                    setLookUp_viTRPaymentAPEntry(rowSelected_viTRPaymentAPEntry.data);
                                                } else
                                                {
                                                    setLookUp_viTRPaymentAPEntry();
                                                }
                                            }
                                            // End Function # --------------
                                        },
                                /**
                                 *	Mengatur tampilan pada Grid Kasir Rawat Jalan
                                 *	Terdiri dari : Judul, Isi dan Event
                                 *	Isi pada Grid di dapat dari pemangilan dari Net.
                                 */
                                colModel: new Ext.grid.ColumnModel
                                        (
                                                [
                                                    new Ext.grid.RowNumberer(),
                                                    {
                                                        id: 'colVoNumber_viTRPaymentAPEntry',
                                                        header: 'VO Number',
                                                        dataIndex: '',
                                                        sortable: true,
                                                        width: 35,
                                                        filter:
                                                                {
                                                                    type: 'int'
                                                                }
                                                    },
                                                    //-------------- ## --------------
                                                    {
                                                        id: 'colTglJournal_viTRPaymentAPEntry',
                                                        header: 'Tanggal',
                                                        dataIndex: '',
                                                        sortable: true,
                                                        width: 35,
                                                        filter:
                                                                {}
                                                    },
                                                    //-------------- ## --------------
                                                    {
                                                        id: 'colCustFilter_viTRPaymentAPEntry',
                                                        header: 'Notes',
                                                        dataIndex: '',
                                                        width: 200,
                                                        sortable: true,
                                                        filter: {}
                                                    },
                                                ]
                                                ),
                                // Tolbar ke Dua # --------------
                                tbar:
                                        {
                                            xtype: 'toolbar',
                                            id: 'toolbar_viTRPaymentAPEntry',
                                            items:
                                                    [
                                                        {
                                                            xtype: 'button',
                                                            text: 'Edit Data',
                                                            iconCls: 'Edit_Tr',
                                                            tooltip: 'Edit Data',
                                                            id: 'btnEdit_viTRPaymentAPEntry',
                                                            handler: function (sm, row, rec)
                                                            {
                                                                if (rowSelected_viTRPaymentAPEntry != undefined)
                                                                {
                                                                    setLookUp_viTRPaymentAPEntry(rowSelected_viTRPaymentAPEntry.data)
                                                                } else
                                                                {
                                                                    setLookUp_viTRPaymentAPEntry();
                                                                }
                                                            }
                                                        }
                                                        //-------------- ## --------------
                                                    ]
                                        },
                                // End Tolbar ke Dua # --------------
                                // Button Bar Pagging # --------------
                                // Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
                                bbar: bbar_paging(mod_name_viTRPaymentAPEntry, selectCount_viTRPaymentAPEntry, dataSource_viTRPaymentAPEntry),
                                // End Button Bar Pagging # --------------
                                viewConfig:
                                        {
                                            forceFit: true
                                        }
                            }
                            )

                    // Kriteria filter pada Grid # --------------
                    var FrmFilterGridDataView_viTRPaymentAPEntry = new Ext.Panel
                            (
                                    {
                                        title: NamaForm_viTRPaymentAPEntry,
                                        iconCls: 'Studi_Lanjut',
                                        id: mod_id_viTRPaymentAPEntry,
                                        region: 'center',
                                        layout: 'form',
                                        closable: true,
                                        border: false,
                                        margins: '0 5 5 0',
                                        items: [GridDataView_viTRPaymentAPEntry],
                                        tbar:
                                                [
                                                    {
                                                        //-------------- # Untuk mengelompokkan pencarian # --------------
                                                        xtype: 'buttongroup',
                                                        //title: 'Pencarian ' + NamaForm_viTRPaymentAPEntry,
                                                        columns: 12,
                                                        defaults: {
                                                            scale: 'small'
                                                        },
                                                        frame: false,
                                                        //-------------- ## --------------
                                                        items:
                                                                [
                                                                    {
                                                                        xtype: 'tbtext',
                                                                        text: 'VO Number : ',
                                                                        style: {'text-align': 'left'},
                                                                        width: 90,
                                                                        height: 25
                                                                    },
                                                                    //viCombo_JournalCode(125, "ComboFilterJournalCode_viTRPaymentAPEntry"),	
                                                                    //-------------- ## --------------
                                                                    {
                                                                        xtype: 'textfield',
                                                                        id: 'TxtNumberFilter_viGeneralLedger',
                                                                        emptyText: 'Number...',
                                                                        width: 150,
                                                                    },
                                                                    {
                                                                        xtype: 'tbspacer',
                                                                        width: 15,
                                                                        height: 25
                                                                    },
                                                                    //-------------- ## --------------
                                                                    {
                                                                        xtype: 'tbtext',
                                                                        text: 'Tanggal : ',
                                                                        style: {'text-align': 'left'},
                                                                        width: 50,
                                                                        height: 25
                                                                    },
                                                                    //-------------- ## --------------						
                                                                    {
                                                                        xtype: 'datefield',
                                                                        id: 'txtfilterTglKunjAwal_viTRPaymentAPEntry',
                                                                        value: now_viTRPaymentAPEntry,
                                                                        format: 'd/M/Y',
                                                                        width: 120,
                                                                        listeners:
                                                                                {
                                                                                    'specialkey': function ()
                                                                                    {
                                                                                        if (Ext.EventObject.getKey() === 13)
                                                                                        {
                                                                                            datarefresh_viTRPaymentAPEntry();
                                                                                        }
                                                                                    }
                                                                                }
                                                                    },
                                                                    {
                                                                        xtype: 'tbtext',
                                                                        text: 's/d ',
                                                                        style: {'text-align': 'center'},
                                                                        width: 50,
                                                                        height: 25
                                                                    },
                                                                    {
                                                                        xtype: 'datefield',
                                                                        id: 'txtfilterTglKunjAkhir_viTRPaymentAPEntry',
                                                                        value: now_viTRPaymentAPEntry,
                                                                        format: 'd/M/Y',
                                                                        width: 120,
                                                                        listeners:
                                                                                {
                                                                                    'specialkey': function ()
                                                                                    {
                                                                                        if (Ext.EventObject.getKey() === 13)
                                                                                        {
                                                                                            datarefresh_viTRPaymentAPEntry();
                                                                                        }
                                                                                    }
                                                                                }
                                                                    },
                                                                    //-------------- ## --------------
                                                                    // { 
                                                                    // xtype: 'tbspacer',
                                                                    // width: 15,
                                                                    // height: 25
                                                                    // },
                                                                    //-------------- ## --------------						
                                                                    {
                                                                        xtype: 'tbspacer',
                                                                        width: 15,
                                                                        height: 25
                                                                    },
                                                                    {
                                                                        xtype: 'button',
                                                                        text: 'Cari',
                                                                        iconCls: 'refresh',
                                                                        tooltip: 'Cari',
                                                                        style: {paddingLeft: '30px'},
                                                                        //rowspan: 3,
                                                                        width: 150,
                                                                        id: 'BtnFilterGridDataView_viTRPaymentAPEntry',
                                                                        handler: function ()
                                                                        {
                                                                            DfltFilterBtn_viTRPaymentAPEntry = 1;
                                                                            DataRefresh_viTRPaymentAPEntry(getCriteriaFilterGridDataView_viTRPaymentAPEntry());
                                                                        }
                                                                    },
                                                                    //-------------- ## --------------
                                                                ]
                                                                //-------------- # End items # --------------
                                                                //-------------- # End mengelompokkan pencarian # --------------
                                                    }
                                                ]
                                                //-------------- # End tbar # --------------
                                    }
                            )
                    return FrmFilterGridDataView_viTRPaymentAPEntry;
                    //-------------- # End form filter # --------------
                }
// End Function dataGrid_viTRPaymentAPEntry # --------------

        /**
         *	Function : setLookUp_viTRPaymentAPEntry
         *	
         *	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
         */

        function setLookUp_viTRPaymentAPEntry(rowdata)
        {
            var lebar = 760;
            setLookUps_viTRPaymentAPEntry = new Ext.Window
                    (
                            {
                                id: 'SetLookUps_viTRPaymentAPEntry',
                                name: 'SetLookUps_viTRPaymentAPEntry',
                                title: NamaForm_viTRPaymentAPEntry,
                                closeAction: 'destroy',
                                width: 775,
                                height: 610,
                                resizable: false,
                                autoScroll: false,
                                border: true,
                                constrainHeader: true,
                                iconCls: 'Studi_Lanjut',
                                modal: true,
                                items: getFormItemEntry_viTRPaymentAPEntry(lebar, rowdata), //1
                                listeners:
                                        {
                                            activate: function ()
                                            {

                                            },
                                            afterShow: function ()
                                            {
                                                this.activate();
                                            },
                                            deactivate: function ()
                                            {
                                                rowSelected_viTRPaymentAPEntry = undefined;
                                                //datarefresh_viTRPaymentAPEntry();
                                                mNoKunjungan_viTRPaymentAPEntry = '';
                                            }
                                        }
                            }
                    );

            setLookUps_viTRPaymentAPEntry.show();
            if (rowdata == undefined)
            {
                // dataaddnew_viTRPaymentAPEntry();
                // Ext.getCmp('btnDelete_viTRPaymentAPEntry').disable();	
            } else
            {
                // datainit_viTRPaymentAPEntry(rowdata);
            }
        }
// End Function setLookUpGridDataView_viTRPaymentAPEntry # --------------

        /**
         *	Function : getFormItemEntry_viTRPaymentAPEntry
         *	
         *	Sebuah fungsi untuk menampilkan penataan menu dan isian form edit
         */

        function getFormItemEntry_viTRPaymentAPEntry(lebar, rowdata)
        {
            var pnlFormDataBasic_viTRPaymentAPEntry = new Ext.FormPanel
                    (
                            {
                                title: '',
                                region: 'north',
                                layout: 'form',
                                bodyStyle: 'padding:10px 10px 10px 10px',
                                anchor: '100%',
                                width: lebar,
                                border: false,
                                //-------------- #items# --------------
                                items:
                                        [
                                            getItemPanelInputBiodata_viTRPaymentAPEntry(lebar),
                                            //-------------- ## -------------- 
                                            //getItemDataKunjungan_viTRPaymentAPEntry(lebar), 
                                            //-------------- ## --------------
                                            getItemGridTransaksi_viTRPaymentAPEntry(lebar),
                                            //-------------- ## --------------
                                            {
                                                xtype: 'compositefield',
                                                fieldLabel: ' ',
                                                anchor: '100%',
                                                labelSeparator: '',
                                                //width: 199,
                                                style: {'margin-top': '7px'},
                                                items:
                                                        [
                                                            {
                                                                xtype: 'button',
                                                                text: 'Posting',
                                                                width: 70,
                                                                style: {'text-align': 'right', 'margin-left': '10px'},
                                                                hideLabel: true,
                                                                id: 'btnFindRecord_viTRPaymentAPEntry',
                                                                name: 'btnFindRecord_viTRPaymentAPEntry',
                                                                handler: function ()
                                                                {
                                                                }
                                                            },
                                                            // {
                                                            // xtype: 'textfield',
                                                            // id: 'txtJumlah1EditData_viTRPaymentAPEntry',
                                                            // name: 'txtJumlah1EditData_viTRPaymentAPEntry',
                                                            // style:{'text-align':'right','margin-left':'420px'},
                                                            // width: 100,
                                                            // value: 0,
                                                            // readOnly: true,
                                                            // },		                
                                                            {
                                                                xtype: 'textfield',
                                                                id: 'txtJumlah2EditData_viTRPaymentAPEntry',
                                                                name: 'txtJumlah2EditData_viTRPaymentAPEntry',
                                                                style: {'text-align': 'right', 'margin-left': '450px'},
                                                                width: 100,
                                                                value: 0,
                                                                readOnly: true,
                                                            },
                                                                    //-------------- ## --------------
                                                        ]
                                            },
                                            //-------------- ## --------------
                                        ],
                                //-------------- #End items# --------------
                                fileUpload: true,
                                // Tombol pada tollbar Edit Data Pasien
                                tbar:
                                        {
                                            xtype: 'toolbar',
                                            items:
                                                    [
                                                        {
                                                            xtype: 'button',
                                                            text: 'Add',
                                                            iconCls: 'add',
                                                            id: 'btnAdd_viTRPaymentAPEntry',
                                                            handler: function () {
                                                                dataaddnew_viTRPaymentAPEntry();
                                                            }
                                                        },
                                                        //-------------- ## --------------
                                                        {
                                                            xtype: 'tbseparator'
                                                        },
                                                        //-------------- ## --------------
                                                        {
                                                            xtype: 'button',
                                                            text: 'Save',
                                                            iconCls: 'save',
                                                            id: 'btnSimpan_viTRPaymentAPEntry',
                                                            handler: function ()
                                                            {
                                                                datasave_viTRPaymentAPEntry(addNew_viTRPaymentAPEntry);
                                                                datarefresh_viTRPaymentAPEntry();
                                                            }
                                                        },
                                                        //-------------- ## --------------
                                                        {
                                                            xtype: 'tbseparator'
                                                        },
                                                        //-------------- ## --------------
                                                        {
                                                            xtype: 'button',
                                                            text: 'Save & Close',
                                                            iconCls: 'saveexit',
                                                            id: 'btnSimpanExit_viTRPaymentAPEntry',
                                                            handler: function ()
                                                            {
                                                                var x = datasave_viTRPaymentAPEntry(addNew_viTRPaymentAPEntry);
                                                                datarefresh_viTRPaymentAPEntry();
                                                                if (x === undefined)
                                                                {
                                                                    setLookUps_viTRPaymentAPEntry.close();
                                                                }
                                                            }
                                                        },
                                                        //-------------- ## --------------
                                                        {
                                                            xtype: 'tbseparator'
                                                        },
                                                        //-------------- ## --------------
                                                        {
                                                            xtype: 'button',
                                                            text: 'Delete',
                                                            iconCls: 'remove',
                                                            id: 'btnDelete_viTRPaymentAPEntry',
                                                            handler: function ()
                                                            {
                                                                datadelete_viTRPaymentAPEntry();
                                                                datarefresh_viTRPaymentAPEntry();

                                                            }
                                                        },
                                                        //-------------- ## --------------
                                                        {
                                                            xtype: 'tbseparator'
                                                        },
                                                        //-------------- ## --------------
                                                        {
                                                            text: 'Lookup ',
                                                            iconCls: 'find',
                                                            menu:
                                                                    [
                                                                        {
                                                                            text: 'Find Record',
                                                                            id: 'btnLookUpRecord_viTRPaymentAPEntry',
                                                                            iconCls: 'find',
                                                                            handler: function () {
                                                                                FormSetLookupRecord_viTRPaymentAPEntry('1', '2');
                                                                            },
                                                                        },
                                                                                //-------------- ## --------------
                                                                    ],
                                                        },
                                                                //-------------- ## --------------


                                                    ]
                                        }//,items:
                            }
                    )

            return pnlFormDataBasic_viTRPaymentAPEntry;
        }
// End Function getFormItemEntry_viTRPaymentAPEntry # --------------

        /**
         *	Function : getItemPanelInputBiodata_viTRPaymentAPEntry
         *	
         *	Sebuah fungsi untuk menampilkan isian form edit data
         */

        function getItemPanelInputBiodata_viTRPaymentAPEntry(lebar)
        {
            var rdType_viTRPaymentAPEntry = new Ext.form.RadioGroup
                    ({
                        fieldLabel: '',
                        columns: 2,
                        width: 200, //400,
                        border: false,
                        items:
                                [
                                    {
                                        boxLabel: 'Debit',
                                        width: 50,
                                        id: 'rdDebit_viTRPaymentAPEntry',
                                        name: 'rdDebit_viTRPaymentAPEntry',
                                        inputValue: '1'
                                    },
                                    //-------------- ## --------------
                                    {
                                        boxLabel: 'Kredit',
                                        width: 50,
                                        id: 'rdKredit_viTRPaymentAPEntry',
                                        name: 'rdKredit_viTRPaymentAPEntry',
                                        inputValue: '2'
                                    }
                                    //-------------- ## --------------
                                ]
                    });
            //-------------- ## --------------

            var items =
                    {
                        layout: 'Form',
                        anchor: '100%',
                        width: lebar - 80,
                        labelAlign: 'Left',
                        bodyStyle: 'padding:10px 10px 10px 10px',
                        border: true,
                        items:
                                [
                                    {
                                        xtype: 'compositefield',
                                        fieldLabel: 'VO Number',
                                        anchor: '100%',
                                        //labelSeparator: '',
                                        width: 199,
                                        items:
                                                [
                                                    {
                                                        xtype: 'textfield',
                                                        flex: 1,
                                                        fieldLabel: 'Label',
                                                        width: 100,
                                                        name: 'txtVONumber_viTRPaymentAPEntry',
                                                        id: 'txtVONumber_viTRPaymentAPEntry',
                                                        emptyText: 'VO Number',
                                                        listeners:
                                                                {
                                                                    'specialkey': function ()
                                                                    {
                                                                        if (Ext.EventObject.getKey() === 13)
                                                                        {
                                                                        }
                                                                        ;
                                                                    }
                                                                }
                                                    },
                                                    {
                                                        xtype: 'displayfield',
                                                        width: 370,
                                                        value: ' ',
                                                        fieldLabel: 'Label',
                                                    },
                                                    {
                                                        xtype: 'datefield',
                                                        fieldLabel: 'Tanggal',
                                                        name: 'DueDate_viTRPaymentAPEntry',
                                                        id: 'DueDate_viTRPaymentAPEntry',
                                                        format: 'd/M/Y',
                                                        width: 130,
                                                        value: now_viTRPaymentAPEntry,
                                                        //style:{'text-align':'left','margin-left':'210px'},
                                                    },
                                                            // {
                                                            // xtype: 'checkbox',
                                                            // id: 'chkUpdateApproveGL',
                                                            // name: 'chkUpdateApproveGL',
                                                            // disabled: false,
                                                            // autoWidth: false,		
                                                            // style: { 'margin-top': '2px' },							
                                                            // boxLabel: 'Approved ',
                                                            // width: 70
                                                            // },
                                                            //-------------- ## --------------
                                                ]
                                    },
                                    {
                                        xtype: 'compositefield',
                                        fieldLabel: 'Payment To',
                                        name: 'compCustomer_viTRPaymentAPEntry',
                                        id: 'compCustomer_viTRPaymentAPEntry',
                                        items:
                                                [
                                                    viCombo_CustomerAP(610, "ComboCustomerAP_viTRPaymentAPEntry"),
                                                ]
                                    },
                                    {
                                        xtype: 'compositefield',
                                        fieldLabel: 'Cash/Bank',
                                        name: 'compCashBank_viTRPaymentAPEntry',
                                        id: 'compCashBank_viTRPaymentAPEntry',
                                        items:
                                                [
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: '',
                                                        width: 110,
                                                        name: 'txtCash_viTRPaymentAPEntry',
                                                        id: 'txtCash_viTRPaymentAPEntry',
                                                        emptyText: ' ',
                                                    },
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: '',
                                                        width: 495,
                                                        name: 'txtBank_viTRPaymentAPEntry',
                                                        id: 'txtBank_viTRPaymentAPEntry',
                                                        emptyText: ' ',
                                               },
                                                            //-------------- ## --------------
                                                ]
                                    },
                                    //-------------- ## --------------				
                                    {
                                        xtype: 'compositefield',
                                        fieldLabel: 'Pay. Mode',
                                        name: 'compPayMode_viTRPaymentAPEntry',
                                        id: 'compPayMode_viTRPaymentAPEntry',
                                        items:
                                                [
                                                    viCombo_PayMode(110, 'cboPayMode_PaymentAPEntry'),
                                                    //-------------- ## --------------
                                                    {
                                                        xtype: 'displayfield',
                                                        width: 80,
                                                        value: 'Pay. Number:',
                                                        fieldLabel: 'Label',
                                                    },
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: '',
                                                        width: 200,
                                                        name: 'txtPayNumber_viTRPaymentAPEntry',
                                                        id: 'txtPayNumber_viTRPaymentAPEntry',
                                                        emptyText: 'Pay Number',
                                               },
                                                            //-------------- ## --------------
                                                ]
                                    },
                                    //-------------- ## --------------	
                                    {
                                        xtype: 'compositefield',
                                        fieldLabel: 'Currency',
                                        name: 'compcurr_viTRPaymentAPEntry',
                                        id: 'compcurr_viTRPaymentAPEntry',
                                        items:
                                                [
                                                    viCombo_Curr(50, 'cboCurr_PaymentAPEntry'),
                                                    //-------------- ## --------------
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: '',
                                                        width: 55,
                                                        name: 'txtKurs_viTRPaymentAPEntry',
                                                        id: 'txtKurs_viTRPaymentAPEntry',
                                                        emptyText: '1',
                                                    },
                                                    {
                                                        xtype: 'displayfield',
                                                        width: 80,
                                                        value: 'Amount(Kurs):',
                                                        fieldLabel: 'Label',
                                                    },
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: '',
                                                        width: 200,
                                                        name: 'txtPayNumber_viTRPaymentAPEntry',
                                                        id: 'txtPayNumber_viTRPaymentAPEntry',
                                                        emptyText: 'Kurs..',
                                               },
                                                            //-------------- ## --------------
                                                ]
                                    },
                                    {
                                        xtype: 'compositefield',
                                        fieldLabel: 'Notes ',
                                        name: 'compNotes_viTRPaymentAPEntry',
                                        id: 'compNotes_viTRPaymentAPEntry',
                                        items:
                                                [
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: '',
                                                        width: 610,
                                                        name: 'txtNotes_viTRPaymentAPEntry',
                                                        id: 'txtNotes_viTRPaymentAPEntry',
                                                        emptyText: ' ',
                                                    },
                                                ]
                                    },
                                    {
                                        xtype: 'compositefield',
                                        fieldLabel: ' ',
                                        labelSeparator: '',
                                        name: 'compNotes2_viTRPaymentAPEntry',
                                        id: 'compNotes2_viTRPaymentAPEntry',
                                        items:
                                                [
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: '',
                                                        width: 610,
                                                        name: 'txtNotes2_viTRPaymentAPEntry',
                                                        id: 'txtNotes2_viTRPaymentAPEntry',
                                                        emptyText: ' ',
                                                    },
                                                ]
                                    },
                                    {
                                        xtype: 'compositefield',
                                        fieldLabel: 'Budget Number',
                                        labelSeparator: '',
                                        name: 'compNotes3_viTRPaymentAPEntry',
                                        id: 'compNotes3_viTRPaymentAPEntry',
                                        items:
                                                [
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: '',
                                                        width: 610,
                                                        name: 'txtNotes3_viTRPaymentAPEntry',
                                                        id: 'txtNotes3_viTRPaymentAPEntry',
                                                        emptyText: ' ',
                                                    },
                                                ]
                                    },
                                    {
                                        xtype: 'compositefield',
                                        fieldLabel: 'Budget',
                                        labelSeparator: '',
                                        name: 'compBudget_viTRPaymentAPEntry',
                                        id: 'compBudget_viTRPaymentAPEntry',
                                        items:
                                                [
                                                    {
                                                        xtype: 'checkbox',
                                                        allowBlank: false,
                                                        //width: 610,
                                                        // name: 'txtNotes3_viTRPaymentAPEntry',
                                                        // id: 'txtNotes3_viTRPaymentAPEntry',
                                                        // emptyText: ' ',
                                                    },
                                                ]
                                    },
                                    {
                                        xtype: 'compositefield',
                                        fieldLabel: 'Periode',
                                        name: 'compPeriode_viTRPaymentAPEntry',
                                        id: 'compPeriode_viTRPaymentAPEntry',
                                        items:
                                                [
                                                    viCombo_PeriodeRL(110, "ComboPeriodeRL_viGeneralLedger"),
                                                    {
                                                        xtype: 'button',
                                                        text: 'Load',
                                                        width: 70,
                                                        //style:{'margin-left':'190px','margin-top':'7px'},
                                                        hideLabel: true,
                                                        id: 'btnLoadPeriode_viTRPaymentAPEntry',
                                                        handler: function ()
                                                        {
                                                        }
                                                    }

                                                ]
                                    },
                                    //-------------- ## --------------	


                                ]
                    };
            return items;
        }
        ;
// End Function getItemPanelInputBiodata_viTRPaymentAPEntry # --------------

        /**
         *	Function : getItemDataKunjungan_viTRPaymentAPEntry
         *	
         *	Sebuah fungsi untuk menampilkan isian form edit data
         */

        function getItemDataKunjungan_viTRPaymentAPEntry(lebar)
        {
            var items =
                    {
                        layout: 'form',
                        anchor: '100%',
                        labelAlign: 'Left',
                        bodyStyle: 'padding:10px 10px 10px 10px',
                        width: lebar - 80,
                        height: 30,
                        items:
                                [
                                    // {
                                    // xtype: 'compositefield',
                                    // fieldLabel: 'Dari',	
                                    // labelAlign: 'Left',
                                    // //labelSeparator: '',
                                    // labelWidth:70,
                                    // defaults: 
                                    // {
                                    // flex: 1
                                    // },
                                    // items: 
                                    // [					
                                    // {
                                    // xtype:'button',
                                    // text:'01',
                                    // tooltip: '01',
                                    // id:'btnTrans_vi01',
                                    // name:'btnTrans_vi01',
                                    // width: 20,
                                    // handler:function()
                                    // {

                                    // }
                                    // },
                                    // {
                                    // xtype:'button',
                                    // text:'02',
                                    // tooltip: '02',
                                    // id:'btnTrans_vi02',
                                    // name:'btnTrans_vi02',
                                    // width: 20,
                                    // handler:function()
                                    // {

                                    // }
                                    // },
                                    // // {
                                    // // xtype:'button',
                                    // // text:'03',
                                    // // tooltip: '03',
                                    // // id:'btnTrans_vi03',
                                    // // name:'btnTrans_vi03',
                                    // // width: 20,
                                    // // handler:function()
                                    // // {

                                    // // }
                                    // // },
                                    // // {
                                    // // xtype:'button',
                                    // // text:'04',
                                    // // tooltip: '04',
                                    // // id:'btnTrans_vi04',
                                    // // name:'btnTrans_vi04',
                                    // // width: 20,
                                    // // handler:function()
                                    // // {

                                    // // }
                                    // // },
                                    // // {
                                    // // xtype:'button',
                                    // // text:'05',
                                    // // tooltip: '05',
                                    // // id:'btnTrans_vi05',
                                    // // name:'btnTrans_vi05',
                                    // // width: 20,
                                    // // handler:function()
                                    // // {

                                    // // }
                                    // // },
                                    // // {
                                    // // xtype:'button',
                                    // // text:'06',
                                    // // tooltip: '06',
                                    // // id:'btnTrans_vi06',
                                    // // name:'btnTrans_vi06',
                                    // // width: 20,
                                    // // handler:function()
                                    // // {

                                    // // }
                                    // // },
                                    // // {
                                    // // xtype:'button',
                                    // // text:'07',
                                    // // tooltip: '07',
                                    // // id:'btnTrans_vi07',
                                    // // name:'btnTrans_vi07',
                                    // // width: 20,
                                    // // handler:function()
                                    // // {

                                    // // }
                                    // // },
                                    // // {
                                    // // xtype:'button',
                                    // // text:'08',
                                    // // tooltip: '08',
                                    // // id:'btnTrans_vi08',
                                    // // name:'btnTrans_vi08',
                                    // // width: 20,
                                    // // handler:function()
                                    // // {

                                    // // }
                                    // // },
                                    // // {
                                    // // xtype:'button',
                                    // // text:'09',
                                    // // tooltip: '09',
                                    // // id:'btnTrans_vi09',
                                    // // name:'btnTrans_vi09',
                                    // // width: 20,
                                    // // handler:function()
                                    // // {

                                    // // }
                                    // // },
                                    // // {
                                    // // xtype:'button',
                                    // // text:'10',
                                    // // tooltip: '10',
                                    // // id:'btnTrans_vi10',
                                    // // name:'btnTrans_vi10',
                                    // // width: 20,
                                    // // handler:function()
                                    // // {

                                    // // }
                                    // // },
                                    // // {
                                    // // xtype:'button',
                                    // // text:'11',
                                    // // tooltip: '11',
                                    // // id:'btnTrans_vi11',
                                    // // name:'btnTrans_vi11',
                                    // // width: 20,
                                    // // handler:function()
                                    // // {

                                    // // }
                                    // // },
                                    // // {
                                    // // xtype:'button',
                                    // // text:'12',
                                    // // tooltip: '12',
                                    // // id:'btnTrans_vi12',
                                    // // name:'btnTrans_vi12',
                                    // // width: 20,
                                    // // handler:function()
                                    // // {

                                    // // }
                                    // // },
                                    // ]
                                    // },
                                    //-------------- ## --------------		

                                ]
                    };
            return items;
        }
        ;
// End Function getItemDataKunjungan_viTRPaymentAPEntry # --------------

        /**
         *	Function : getItemGridTransaksi_viTRPaymentAPEntry
         *	
         *	Sebuah fungsi untuk menampilkan isian form edit data
         */

        function getItemGridTransaksi_viTRPaymentAPEntry(lebar)
        {
            var items =
                    {
                        title: '',
                        layout: 'form',
                        anchor: '100%',
                        labelAlign: 'Left',
                        bodyStyle: 'padding:10px 10px 10px 10px',
                        border: true,
                        width: lebar - 80,
                        height: 220,
                        items:
                                [
                                    {
                                        labelWidth: 105,
                                        layout: 'form',
                                        labelAlign: 'Left',
                                        border: false,
                                        items:
                                                [
                                                    gridDataViewEdit_viTRPaymentAPEntry()
                                                ]
                                    }
                                    //-------------- ## --------------
                                ]
                    };
            return items;
        }
        ;
// End Function getItemGridTransaksi_viTRPaymentAPEntry # --------------

        /**
         *	Function : gridDataViewEdit_viTRPaymentAPEntry
         *	
         *	Sebuah fungsi untuk menampilkan isian form edit data
         */

        function gridDataViewEdit_viTRPaymentAPEntry()
        {

            var FieldGrdKasir_viTRPaymentAPEntry =
                    [
                    ];

            dsDataGrdJab_viTRPaymentAPEntry = new WebApp.DataStore
                    ({
                        fields: FieldGrdKasir_viTRPaymentAPEntry
                    });

            var items =
                    {
                        xtype: 'editorgrid',
                        store: dsDataGrdJab_viTRPaymentAPEntry,
                        height: 200,
                        selModel: new Ext.grid.RowSelectionModel
                                (
                                        {
                                            singleSelect: true,
                                            listeners:
                                                    {
                                                        rowselect: function (sm, row, rec)
                                                        {
                                                        }
                                                    }
                                        }
                                ),
                        columns:
                                [
                                    {
                                        dataIndex: '',
                                        header: 'Account',
                                        sortable: true,
                                        width: 75
                                    },
                                    //-------------- ## --------------
                                    {
                                        dataIndex: '',
                                        header: 'Name',
                                        sortable: true,
                                        width: 200
                                    },
                                    //-------------- ## --------------			
                                    {
                                        dataIndex: '',
                                        header: 'Description',
                                        sortable: true,
                                        width: 200
                                    },
                                    //-------------- ## --------------
                                    {
                                        dataIndex: '',
                                        header: 'Amount (Rp)',
                                        sortable: true,
                                        width: 100,
                                        renderer: function (v, params, record)
                                        {

                                        }
                                    },
                                ]
                    }
            return items;
        }
// End Function gridDataViewEdit_viTRPaymentAPEntry # --------------

// ## MENU LOOKUP ## ------------------------------------------------------------------

// ## MENU LOOKUP - LOOKUP PRODUK/ TINDAKAN ## ------------------------------------------------------------------

        /**
         *   Function : FormSetLookupRecord_viTRPaymentAPEntry
         *   
         *   Sebuah fungsi untuk menampilkan windows popup Ganti Dokter
         */

        function FormSetLookupRecord_viTRPaymentAPEntry(subtotal, NO_KUNJUNGAN)
        {
            vWinFormEntryGantiDokter_viTRPaymentAPEntry = new Ext.Window
                    (
                            {
                                id: 'FormGrdLookupRecord_viTRPaymentAPEntry',
                                title: 'Find Record',
                                closeAction: 'destroy',
                                closable: true,
                                width: 390,
                                height: 125,
                                border: false,
                                plain: true,
                                resizable: false,
                                constrainHeader: true,
                                layout: 'form',
                                iconCls: 'Edit_Tr',
                                //padding: '8px',
                                modal: true,
                                items:
                                        [
                                            getFormItemEntryLookupfindrecord_viTRPaymentAPEntry(subtotal, NO_KUNJUNGAN),
                                        ],
                                listeners:
                                        {
                                            activate: function ()
                                            { }
                                        }
                            }
                    );
            vWinFormEntryGantiDokter_viTRPaymentAPEntry.show();
            mWindowGridLookupGantiDokter = vWinFormEntryGantiDokter_viTRPaymentAPEntry;
        }
        ;

// End Function FormSetLookupRecord_viTRPaymentAPEntry # --------------

        /**
         *   Function : getFormItemEntryLookupfindrecord_viTRPaymentAPEntry
         *   
         *   Sebuah fungsi untuk menampilkan isian form Ganti Dokter
         */
        function getFormItemEntryLookupfindrecord_viTRPaymentAPEntry(subtotal, NO_KUNJUNGAN)
        {
            var lebar = 500;
            var pnlFormDataFindRecordWindowPopup_viTRPaymentAPEntry = new Ext.FormPanel
                    (
                            {
                                title: '',
                                region: 'center',
                                layout: 'anchor',
                                padding: '8px',
                                bodyStyle: 'padding:10px 0px 10px 10px;',
                                fileUpload: true,
                                //-------------- #items# --------------
                                items:
                                        [
                                            getItemPanelInputFindRecordDataView_viTRPaymentAPEntry(lebar),
                                                    //-------------- ## --------------
                                        ],
                                //-------------- #End items# --------------
                            }
                    )
            return pnlFormDataFindRecordWindowPopup_viTRPaymentAPEntry;
        }
// End Function getFormItemEntryLookupfindrecord_viTRPaymentAPEntry # --------------

        /**
         *   Function : getItemPanelInputFindRecordDataView_viTRPaymentAPEntry
         *   
         *   Sebuah fungsi untuk menampilkan isian form Ganti Dokter
         */

        function getItemPanelInputFindRecordDataView_viTRPaymentAPEntry(lebar)
        {
            var items =
                    {
                        title: '',
                        layout: 'Form',
                        anchor: '100%',
                        width: lebar,
                        labelAlign: 'Left',
                        bodyStyle: 'padding:1px 1px 1px 1px',
                        border: false,
                        items:
                      [
                                    {
                                        xtype: 'compositefield',
                                        fieldLabel: 'Field Name',
                                        anchor: '100%',
                                        width: 250,
                                        items:
                                                [
                                                    viCombo_ReferenceRL(250, 'CmboFindReference_viTRPaymentAPEntry'),
                                                            //-------------- ## --------------  
                                                ]
                                    },
                                    //-------------- ## --------------
                                    {
                                        xtype: 'compositefield',
                                        fieldLabel: 'Operator',
                                        anchor: '100%',
                                        width: 250,
                                        items:
                                                [
                                                    viCombo_OperatorRL(250, 'CmboOperator_viTRPaymentAPEntry'),
                                                            //-------------- ## --------------  
                                                ]
                                    },
                                    //-------------- ## --------------
                                    {
                                        xtype: 'compositefield',
                                        fieldLabel: 'Key Word',
                                        anchor: '100%',
                                        width: 250,
                                        items:
                                                [
                                                    {
                                                        xtype: 'textfield',
                                                        id: 'TxtKeyWord_viTRPaymentAPEntry',
                                                        emptyText: ' ',
                                                        width: 150,
                                                    },
                                                ]
                                    },
                                    //-------------- ## --------------
                                    // {
                                    // xtype: 'compositefield',
                                    // id: 'findcancelbtn',
                                    // fieldLabel: 'find',
                                    // anchor: '100%',
                                    // //labelSeparator: '',
                                    // width: 199,
                                    // style:{'margin-top':'7px'},					
                                    // items: 
                                    // [
                                    // {
                                    // xtype:'button',
                                    // text:'Ok',
                                    // width:70,
                                    // //style:{'margin-left':'190px','margin-top':'7px'},
                                    // style:{'text-align':'right','margin-left':'60px'},
                                    // hideLabel:true,
                                    // id: 'btnFindRecord_viTRPaymentAPEntry',
                                    // name: 'btnFindRecord_viTRPaymentAPEntry',
                                    // handler:function()
                                    // {
                                    // }   
                                    // },
                                    // //-------------- ## --------------
                                    // {
                                    // xtype:'button',
                                    // text:'Cancel',
                                    // width:70,
                                    // //style:{'margin-left':'190px','margin-top':'7px'},
                                    // style:{'text-align':'right','margin-left':'90px'},
                                    // hideLabel:true,
                                    // id: 'btnCancelRecord_viTRPaymentAPEntry',
                                    // name: 'btnCancelRecord_viTRPaymentAPEntry',
                                    // handler:function()
                                    // {
                                    // }   
                                    // },                        
                                    // //-------------- ## --------------
                                    // ]
                                    // },  
                                ]
                    };
            return items;
        }
// End Function getItemPanelInputFindRecordDataView_viTRPaymentAPEntry # --------------


        function viCombo_CustomerAP(lebar, Nama_ID)
        {
            var Field_CustomerAP = ['KD_CUSTOMER', 'CUSTOMER'];
            ds_CustomerAP = new WebApp.DataStore({fields: Field_CustomerAP});

            // viRefresh_Journal();

            var cbo_CustomerAP = new Ext.form.ComboBox
                    (
                            {
                                flex: 1,
                                fieldLabel: 'Customer',
                                valueField: 'KD_Customer',
                                displayField: 'Customer',
                                emptyText: 'Payment To...',
                                store: ds_CustomerAP,
                                width: lebar,
                                mode: 'local',
                                typeAhead: true,
                                triggerAction: 'all',
                                name: Nama_ID,
                                lazyRender: true,
                                id: Nama_ID,
                                listeners:
                                        {
                                            'specialkey': function ()
                                            {
                                                if (Ext.EventObject.getKey() === 13)
                                                {

                                                }
                                            }
                                        }
                            }
                    )
            return cbo_CustomerAP;
        }

        function viCombo_OperatorRL(lebar, Nama_ID)
        {
            var Field_OperatorRL = ['OPERATOR'];
            ds_OperatorRL = new WebApp.DataStore({fields: Field_OperatorRL});

            // viRefresh_Journal();

            var cbo_OperatorRL = new Ext.form.ComboBox
                    (
                            {
                                flex: 1,
                                fieldLabel: 'Operator',
                                valueField: 'Operator',
                                displayField: 'Operator',
                                emptyText: 'Operator',
                                store: ds_OperatorRL,
                                width: lebar,
                                mode: 'local',
                                typeAhead: true,
                                triggerAction: 'all',
                                name: Nama_ID,
                                lazyRender: true,
                                id: Nama_ID,
                                listeners:
                                        {
                                            'specialkey': function ()
                                            {
                                                if (Ext.EventObject.getKey() === 13)
                                                {

                                                }
                                            }
                                        }
                            }
                    )
            return cbo_OperatorRL;
        }

        function viCombo_PeriodeRL(lebar, Nama_ID)
        {
            var Field_JournalRL = ['MONTH'];
            ds_JournalRL = new WebApp.DataStore({fields: Field_JournalRL});

            // viRefresh_Journal();

            var cbo_PeriodeBulanRL = new Ext.form.ComboBox
                    (
                            {
                                flex: 1,
                                fieldLabel: 'Journal',
                                valueField: 'MONTH',
                                displayField: 'Month',
                                emptyText: 'Periode Bulan...',
                                store: ds_JournalRL,
                                width: lebar,
                                mode: 'local',
                                typeAhead: true,
                                triggerAction: 'all',
                                name: Nama_ID,
                                lazyRender: true,
                                id: Nama_ID,
                                listeners:
                                        {
                                            'specialkey': function ()
                                            {
                                                if (Ext.EventObject.getKey() === 13)
                                                {

                                                }
                                            }
                                        }
                            }
                    )
            return cbo_PeriodeBulanRL;
        }

        function viCombo_ReferenceRL(lebar, Nama_ID)
        {
            var Field_ReferenceRL = ['REFERENCE'];
            ds_ReferenceRL = new WebApp.DataStore({fields: Field_ReferenceRL});

            var cbo_ReferenceRL = new Ext.form.ComboBox
                    (
                            {
                                flex: 1,
                                fieldLabel: 'Reference',
                                valueField: 'REFERENCE',
                                displayField: 'REFERENCE',
                                emptyText: 'Reference...',
                                store: ds_ReferenceRL,
                                width: lebar,
                                mode: 'local',
                                typeAhead: true,
                                triggerAction: 'all',
                                name: Nama_ID,
                                lazyRender: true,
                                id: Nama_ID,
                                listeners:
                                        {
                                            'specialkey': function ()
                                            {
                                                if (Ext.EventObject.getKey() === 13)
                                                {

                                                }
                                            }
                                        }
                            }
                    )
            return cbo_ReferenceRL;
        }

        function viCombo_PayMode(lebar, Nama_ID)
        {
            var Field_PayMode = [' '];
            ds_PayMode = new WebApp.DataStore({fields: Field_PayMode});

            var cbo_PayMode = new Ext.form.ComboBox
                    (
                            {
                                flex: 1,
                                fieldLabel: '',
                                valueField: '',
                                displayField: '',
                                emptyText: 'Pay mode...',
                                store: ds_PayMode,
                                width: lebar,
                                mode: 'local',
                                typeAhead: true,
                                triggerAction: 'all',
                                name: Nama_ID,
                                lazyRender: true,
                                id: Nama_ID,
                                listeners:
                                        {
                                            'specialkey': function ()
                                            {
                                                if (Ext.EventObject.getKey() === 13)
                                                {

                                                }
                                            }
                                        }
                            }
                    )
            return cbo_PayMode;
        }
        function viCombo_Curr(lebar, Nama_ID)
        {
            var Field_Curr = [' '];
            ds_Curr = new WebApp.DataStore({fields: Field_Curr});

            var cbo_PayModeCurr = new Ext.form.ComboBox
                    (
                            {
                                flex: 1,
                                fieldLabel: '',
                                valueField: '',
                                displayField: '',
                                emptyText: 'Rp...',
                                store: ds_Curr,
                                width: lebar,
                                mode: 'local',
                                typeAhead: true,
                                triggerAction: 'all',
                                name: Nama_ID,
                                lazyRender: true,
                                id: Nama_ID,
                                listeners:
                                        {
                                            'specialkey': function ()
                                            {
                                                if (Ext.EventObject.getKey() === 13)
                                                {

                                                }
                                            }
                                        }
                            }
                    )
            return cbo_PayModeCurr;
        }