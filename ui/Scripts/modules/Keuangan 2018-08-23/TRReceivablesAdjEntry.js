
var CurrentTR_AdjustArForm = 
{
    data: Object,
    details:Array, 
    row: 0
};

var CurrentSelected_AdjustArForm = 
{
    data: Object,
    details:Array, 
    row: 0
};
var mRecord_AdjustArForm = Ext.data.Record.create
	(
		[
		   {name: 'ACCOUNT', mapping:'ACCOUNT'},
		   {name: 'NAMAACCOUNT', mapping:'NAMAACCOUNT'},
		   {name: 'Description', mapping:'DESCRIPTION'},
		   {name: 'VALUE', mapping:'VALUE'},
		   {name: 'ARAD_LINE', mapping:'ARAD_LINE'}
		]
	);

var mRecord = Ext.data.Record.create
(
	[	 
		{name:'ARA_NUMBER', mapping: 'ARA_NUMBER'}, 
		{name:'ARA_DATE', mapping: 'ARA_DATE'}, 
		{name:'AMOUNT', mapping: 'AMOUNT'}, 
		{name:'PAID', mapping: 'PAID'},
		{name:'REMAIN', mapping: 'REMAIN'},
	]
);

var KDkategori_AdjustArForm='1';
var dsTRList_AdjustArForm;
var dsTmp_AdjustArForm;
var dsDTLTRList_AdjustArForm;
var DataAddNew_AdjustArForm = true;
var selectCount_AdjustArForm=50;
var now_AdjustArForm = new Date();
var rowSelected_AdjustArForm;
var TRLookUps_AdjustArForm;
var focusRef_AdjustArForm;
var no_sp3d;
var cellSelectedDet_AdjustArForm;
var cellSelectedDetAkdAdj_AdjustArForm;
var dsCboEntryCustAdjustArForm;
var selectCboEntryCust_AdjustArForm;
var StrAccCust_AdjustArForm;
var strDue_AdjustArForm;
var StrNmAccCust_AdjustArForm;
var radTipe_AdjustArForm = 1;
var strInfo;
var isDb=true;
var isCr=true;
var StrNmAccKas_AdjustArForm;
var StrAccKas_AdjustArForm;
var strTotalRecord;
var NamaForm_AdjustArForm="Adjusment AR";
var criteriaTanggalFind;

CurrentPage.page=getPanel_AdjustArForm(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanel_AdjustArForm(mod_id)
{
    var Field = 
	[
		'ara_number','ara_date','duedate','cust_code','customer','customername','totaldebit','totalkredit','type','no_tag',"date_tag",'notes','posted'
	]
    dsTRList_AdjustArForm = new WebApp.DataStore({ fields: Field });
        
  
    grListTR_AdjustArForm = new Ext.grid.EditorGridPanel
	(
		{
			stripeRows: true,
			xtype: 'editorgrid',
			store: dsTRList_AdjustArForm,
			anchor: '100% 100%',
			columnLines:true,
			border:false,
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			//sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelected_AdjustArForm = dsTRList_AdjustArForm.getAt(row);
						}
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, row, rec)
				{
					rowSelected_AdjustArForm = dsTRList_AdjustArForm.getAt(row);
					
					CurrentSelected_AdjustArForm.row = rec;
					CurrentSelected_AdjustArForm.data = rowSelected_AdjustArForm;

					if (rowSelected_AdjustArForm != undefined)
						{
							//DataAddNew_OpenArForm=false;
							LookUpForm_AdjustArForm(rowSelected_AdjustArForm.data);
					}
					else
					{
							//DataAddNew_OpenArForm=true;
							LookUpForm_AdjustArForm();
					}
				}
			},
			colModel: new Ext.grid.ColumnModel			
			//cm: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header		: 'Posted',
						width		: 60,
						sortable	: false,
						hideable	: true,
						hidden		: false,
						menuDisabled: true,
						dataIndex	: 'posted',
						id			: 'colStatusPosting_viAdjustAR',
						renderer	: function(value, metaData, record, rowIndex, colIndex, store){
							 switch (value){
								 case 't':
									 metaData.css = 'StatusHijau'; 
									 break;
								 case 'f':
									 metaData.css = 'StatusMerah';
									 break;
							 }
							 return '';
						}
					},
					{
						id: 'ColARA_NUMBER',
						header: 'Nomor',
						dataIndex: 'ara_number',
						sortable: true,			
						width :100,
						filter: {}
					}, 
					{
						//xtype: 'datecolumn',
						header: 'Tanggal',
						width: 100,
						sortable: true,
						dataIndex: 'ara_date',
						id:'ColARA_DATE',
						renderer: function(v, params, record) 
						{
							return ShowDate(record.data.ara_date);
						},
						filter: {}			
					}, 
					{						
						id: 'ColCustomer',
						header: "Terima Dari",
						dataIndex: 'customer',
						width :130,
						filter: {}
					},
					{
						id: 'ColNotes',
						header: "Keterangan",
						dataIndex: 'Notes',
						width :200,
						filter: {}
					}, 
					{
						id: 'ColDAmount',
						header: "Debit",
						align:'right',
						dataIndex: 'debit',
						renderer: function(v, params, record) 
						{
							return formatCurrencyDec(record.data.debit);
						},	
						width :80,
						filter: {}
					}, 
										{
						id: 'ColKAmount',
						header: "Kredit",
						align:'right',
						dataIndex: 'kredit',
						renderer: function(v, params, record) 
						{
							return formatCurrencyDec(record.data.kredit);
						},	
						width :80,
						filter: {}
					},
					// {
					// 	id: 'ColAmount',
					// 	header: "Jumlah (Rp)",
					// 	align:'right',
					// 	dataIndex: 'AMOUNT',
					// 	renderer: function(v, params, record) 
					// 	{
					// 		return formatCurrency(record.data.AMOUNT);
					// 	},	
					// 	width :80,
					// 	filter: {}
					// },
				]
			),

			tbar: 
			[	
				{
					id: 'btnAdd_AdjustArForm',
					text: 'Add Data',
					tooltip: 'Edit Data',
					iconCls: 'AddRow',
					handler: function(sm, row, rec) 
					{ 
						LookUpForm_AdjustArForm();
					}
				},' ','-',
				{
					id: 'btnEdit_AdjustArForm',
					text: 'Edit Data',
					tooltip: 'Edit Data',
					iconCls: 'Edit_Tr',
					handler: function(sm, row, rec) 
					{ 
						if (rowSelected_AdjustArForm != undefined)
						{
							LookUpForm_AdjustArForm(rowSelected_AdjustArForm.data);
							ButtonDisabled_AdjustArForm(rowSelected_AdjustArForm.data.Type_Approve);
						}
						else
						{
							ShowPesanWarning_AdjustArForm('Pilih data yang akan di edit terlebih dahulu!','Warning');
						}
					}
				},' ','-'
				,
				{
					xtype: 'checkbox',
					id: 'chkWithTgl_AdjustArForm',					
					hideLabel:true,
					checked: true,
					handler: function() 
					{
						if (this.getValue()===true)
						{
							Ext.get('dtpTglAwalFilter_AdjustArForm').dom.disabled=false;
							Ext.get('dtpTglAkhirFilter_AdjustArForm').dom.disabled=false;
							Ext.get('dtpTglAwalFilter_AdjustArForm').dom.readOnly=true;	
							Ext.get('dtpTglAkhirFilter_AdjustArForm').dom.readOnly=true;
						}
						else
						{
							Ext.get('dtpTglAwalFilter_AdjustArForm').dom.disabled=true;
							Ext.get('dtpTglAkhirFilter_AdjustArForm').dom.disabled=true;							
						};
					}
				}
				, ' ','-','Tanggal : ', ' ',
				{
					xtype: 'datefield',
					fieldLabel: 'Dari Tanggal : ',
					id: 'dtpTglAwalFilter_AdjustArForm',
					format: 'd/M/Y',
					value:now_AdjustArForm,
					width:100,
					onInit: function() { },
					listeners:
					{ 
						'specialkey' : function()
						{
							if (Ext.EventObject.getKey() === 13) 
							{
								RefreshDataFilter_AdjustArForm(false);					
							} 						
						}
					},
				}, ' ', ' s/d ',' ', {
					xtype: 'datefield',
					fieldLabel: 'Sd /',
					id: 'dtpTglAkhirFilter_AdjustArForm',
					format: 'd/M/Y',
					value:now_AdjustArForm,
					width:100,
					listeners:
					{ 
						'specialkey' : function()
						{
							if (Ext.EventObject.getKey() === 13) 
							{
								RefreshDataFilter_AdjustArForm(false);					
							} 						
						}
					},
				},' ','->',
				{
					id: 'btnFind_AdjustArForm',
					text: ' Find',
					tooltip: 'Find Record',
					iconCls: 'find',
					handler: function(sm, row, rec) 
					{ 
						fnFindDlg_AdjustArForm();	
					}
				}
			],
			bbar:new WebApp.PaggingBar
			(
				{
					displayInfo: true,
					store: dsTRList_AdjustArForm,
					pageSize: 50,
					displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
					emptyMsg: "Tidak ada record&nbsp;&nbsp;"		
				}
			),
			viewConfig: {forceFit: true} 			
		}
	);


	var FormTR_AdjustArForm = new Ext.Panel
	(
		{
			id: mod_id,
			closable:true,
			region: 'center',
			layout: 'form',
			title: NamaForm_AdjustArForm, 
			border: false,           
			shadhow: true,
			iconCls: 'Penerimaan',
			 margins:'0 5 5 0',
			items: [grListTR_AdjustArForm],
			tbar: 
			[
				'Nomor : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: 'No : ',
					id: 'txtNoFilter_AdjustArForm',        
					width:120,
					listeners:
					{ 
						'specialkey' : function()
						{
							if (Ext.EventObject.getKey() === 13) 
							{
								RefreshDataFilter_AdjustArForm(false);					
							} 						
						}
					},
					onInit: function() { }
				}, 
				{ xtype: 'tbseparator' },
				{
					xtype: 'checkbox',
					id: 'chkFilterApproved_AdjustArForm',
					boxLabel: 'Approved',
					listeners: 
					{
						check: function()
						{
						   RefreshDataFilter_AdjustArForm(false);
						}
					}
				}, ' ','-',
					' ',mComboMaksData_AdjustArForm(),
					' ','->',
				{					
					xtype: 'button',
					tooltip: 'Tampilkan',
					iconCls: 'refresh',
					text: 'Tampilkan',
					anchor: '25%',
					handler: function(sm, row, rec) 
					{
						RefreshDataFilter_AdjustArForm(false);
					}
				}
			],
			listeners: 
			{ 
				'activate': function() 
				{           
					RefreshDataFilter_AdjustArForm(true);		 
				}
			}
		}
	);
	return FormTR_AdjustArForm

};
    // end function get panel main data
 
 //---------------------------------------------------------------------------------------///
   
   
function LookUpForm_AdjustArForm(rowdata)
{
	var lebar=750;//735;
	TRLookUps_AdjustArForm = new Ext.Window
	(
		{
			id: 'LookUpForm_AdjustArForm',
			title: NamaForm_AdjustArForm,
			closeAction: 'destroy',
			width: lebar,
			height: 450, 
			border: false,
			resizable:false,
			plain: true,
			layout: 'fit',
			iconCls: 'Penerimaan',
			modal: true,
			items: getFormEntryTR_AdjustArForm(lebar),
			listeners:
			{
				activate: function() 
				{
					
				},
				afterShow: function() 
				{ 
					this.activate(); 
				},
				deactivate: function()
				{
					// rowSelected_AdjustArForm=undefined;
					 // RefreshDataFilter_AdjustArForm(true);
				}
			}
		}
	);
	
	TRLookUps_AdjustArForm.show();
	if (rowdata == undefined)
	{
		AddNew_AdjustArForm();
	}
	else
	{
		DataInit_AdjustArForm(rowdata)
	}	
	
};
   
function getFormEntryTR_AdjustArForm(lebar) 
{
	var pnlTR_AdjustArForm = new Ext.FormPanel
	(
		{
			id: 'pnlTR_AdjustArForm',
			fileUpload: true,
			region: 'north',
			layout: 'fit',
			bodyStyle: 'padding:10px 10px 10px 10px',			
			anchor: '100%', 
			width:lebar,
			border: false,
			items: [getItemPanelInput_AdjustArForm(lebar)],
			tbar: 
			[
				{
					text: 'Tambah',
					id:'btnTambah',
					tooltip: 'Tambah Record Baru ',
					iconCls: 'add',
					//handler: function() { TRDataAddNew_AdjustArForm(pnlTR_AdjustArForm) }
					handler: function() 
					{ 
						AddNew_AdjustArForm();
						ButtonDisabled_AdjustArForm(false);
					}
				}, '-', 
				{
					text: 'Simpan',
					id:'btnSimpan_AdjustArForm',
					tooltip: 'Rekam Data ',
					iconCls: 'save',
					//handler: function() { TRDatasave_AdjustArForm(pnlTR_AdjustArForm) }
					handler: function() 
					{ 
						CalcTotalCr_AdjustArForm();
						CalcTotalDb_AdjustArForm();		
						Datasave_AdjustArForm(false);
						RefreshDataFilter_AdjustArForm(false);
					}
				}, '-', 
				{
					text: 'Simpan & Keluar',
					id:'btnSimpanKeluar_AdjustArForm',
					tooltip: 'Rekam Data & Keluar ',
					iconCls: 'saveexit',
					//handler: function() { TRDatasave_AdjustArForm(pnlTR_AdjustArForm) }
					handler: function() 
					{ 
						CalcTotalCr_AdjustArForm();
						CalcTotalDb_AdjustArForm();	
						Datasave_AdjustArForm(true);
						RefreshDataFilter_AdjustArForm(false);
						TRLookUps_AdjustArForm.close();
					}
				}, '-', 
				{
					text: 'Hapus',
					id:'btnHapus_AdjustArForm',
					tooltip: 'Remove the selected item',
					iconCls: 'remove',
					handler: function() 
					{ 
						Ext.Msg.show
						(
							{
							   title:'Hapus',
							   msg: 'Apakah transaksi ini akan dihapus ?', 
							   buttons: Ext.MessageBox.YESNO,
							   fn: function (btn) 
							   {			
								   if (btn =='yes') 
									{
										CalcTotalCr_AdjustArForm();
										CalcTotalDb_AdjustArForm();	
										DataDelete_AdjustArForm();
										RefreshDataFilter_AdjustArForm(false);
									} 
							   },
							   icon: Ext.MessageBox.QUESTION
							}
						);
					}
				}, //'-', 
				{
					text: 'Lookup',
					id:'btnLookup',
					tooltip: 'Lookup Account',
					iconCls: 'find',
					hidden:true,
					handler: function() 
					{

						if (focusRef_AdjustArForm == "ref")
						{
							focusRef_AdjustArForm="";
							var StrKriteria;
							StrKriteria = " WHERE APP_SP3D = 1 and SUBSTRING(AC.ARA_NUMBER,0,5)='BKBS'"		
							var p = new mRecord_AdjustArForm
							(
								{
									ACCOUNT: '',
									NAMAACCOUNT: '',
									Description: Ext.getCmp('txtCatatan_AdjustArForm').getValue(),
									VALUE: '',				
									ARAD_LINE:''
								}
							);							
							FormLookupsp3d(dsDTLTRList_AdjustArForm,p, StrKriteria,nASALFORM_NONMHS);
						}
						else
						{
							var p = new mRecord_AdjustArForm
							(
								{
									ACCOUNT: '',
									NAMAACCOUNT: '',
									Description: Ext.getCmp('txtCatatan_AdjustArForm').getValue(),
									VALUE: '',		
									ARAD_LINE:''
								}
							);
							FormLookupAccount(" Where A.TYPE ='D'  ",dsDTLTRList_AdjustArForm,p,true,'',true);
						}
					}
				}, '-',
				{
				    text: 'Approve',
					id:'btnApprove_AdjustArForm',
				    tooltip: 'Approve',
				    iconCls: 'approve',
				    handler: function() 
					{ 
						if (ValidasiApprove_AdjustArForm('Approve')==1)
						{
							// if (CekApp_AdjustArForm==0)
							// {
								FormApproveAdjust(getTotalDebDetailGrid_AdjustArForm(),'13',Ext.get('txtNo_AdjustArForm').getValue(),
								Ext.get('txtCatatan_AdjustArForm').dom.value,Ext.get('dtpTanggal_AdjustArForm').dom.value,selectCboEntryCust_AdjustArForm)
								// var criteria = Ext.get('txtNo_AdjustArForm').getValue();
								// var criteriaDate = Ext.get('dtpTanggal_AdjustArForm').getValue();
								// FormLookup_AkadPiutang(selectCboEntryCust_AdjustArForm,criteria,criteriaDate,'Payable','2',
								// 	Ext.get('txtTotalDb_AdjustArForm').getValue(),StrAccCust_AdjustArForm,Ext.get('txtCatatan_AdjustArForm').getValue());
							// }else
							// {
							// 	FormLookup_ApproveHistoryArAp(Ext.get('txtNo_AdjustArForm').getValue(),Ext.get('dtpTanggal_AdjustArForm').getValue(),'Pembayaran',KDkategori_AdjustArForm)
							// }
						}
					}
				}
				, '-', '->', '-',
				{
					text: 'Cetak',
					tooltip: 'Print',
					iconCls: 'print',					
					// hidden:true,
					handler: function() {Cetak_AdjustArForm()}
				}
			]
		}
	);  // end Head panel
	
	var GDtabDetail_AdjustArForm = new Ext.TabPanel
	(
		{
			region: 'center',
			id: 'GDtabDetail_AdjustArForm',
			activeTab: 0,
			//anchor: '100% 46%',
			anchor: '100% 47%',
			border: false,
			plain: true,
			defaults: 
			{
				autoScroll: false
			},
			items:GetDTLTRGrid_AdjustArForm(),			
			tbar: 
			[
			{
				text: 'Tambah Baris',
				id:'btnTambahBaris_AdjustArForm',
				tooltip: 'Tambah Record Baru ',
				iconCls: 'add',				
				handler: function() 
				{
					if(Ext.getCmp('ChkApproveEntry_AdjustArForm').getValue() == true || Ext.getCmp('ChkApproveEntry_AdjustArForm').getValue() == 'true'){
						ShowPesanError_AdjustArForm('Adjustment piutang sudah di approve, tambah baris tidak dapat dilakukan!','Error');
					} else{
						if(ValidasiTambahbaris_AdjustArForm('Tambah baris')==1)
						{
							TambahBaris_AdjustArForm();	
						}
					}
					
				}
			}, '-', 
			{
				text: 'Hapus Baris',
				id:'btnHapusBaris_AdjustArForm',
				tooltip: 'Remove the selected item',
				iconCls: 'remove',
				handler: function()
					{
						if(Ext.getCmp('ChkApproveEntry_AdjustArForm').getValue() == true){
							ShowPesanError_AdjustArForm('Adjustment piutang sudah di aprrove, hapus tidak dapat dilakukan!','Error');
						} else{
							if (dsDTLTRList_AdjustArForm.getCount() > 0 )
							{						
								if (cellSelectedDet_AdjustArForm != undefined)
								{
									if(CurrentTR_AdjustArForm != undefined)
									{
										HapusBaris_AdjustArForm();
									}
								}
								else
								{
									ShowPesanWarning_AdjustArForm('Silahkan pilih dahulu baris yang akan dihapus','Hapus baris');
								}
							}
						}
					}
			},
			{ xtype: 'tbseparator' },
			{
				id:'btnBalance_AdjustArForm',
				text: ' Auto Balance',
				tooltip: 'Auto Balance',
				iconCls: 'refresh',
				handler: function() 
				{
					if(Ext.getCmp('ChkApproveEntry_AdjustArForm').getValue() == true){
						ShowPesanError_AdjustArForm('Adjustment piutang sudah di aprrove, auto balance tidak dapat dilakukan!','Error');
					} else{
						CalcTotalCr_AdjustArForm();
						CalcTotalDb_AdjustArForm();	
						AutoBalance_AdjustArForm();
					}
				}
			}
		] 
	}
);
	var Total = new Ext.Panel
	(
		{
			frame: false,
			layout: 'form',
			width: 500,
			border:false,
			id:'PnlTotal_AdjustArForm',
			labelAlign:'right',
			labelWidth:50,
			style: 
			{
				'margin-top': '5.9px',
				'margin-left': '360px'
			},
			items: 
			[
				{
					xtype: 'compositefield',
					fieldLabel: 'Total',
					anchor: '100%',
					items: 
					[
						{
							xtype: 'textfield',
							id:'txtTotalDb_AdjustArForm',
							name:'txtTotalDb_AdjustArForm',
							fieldLabel: 'Total ',
							readOnly:true,
							style:
							{	
								'text-align':'right',
								'font-weight':'bold'
							},
							value:'0',
							width: 145
						},
						{
							xtype: 'textfield',
							id:'txtTotalCr_AdjustArForm',
							name:'txtTotalCr_AdjustArForm',
							// fieldLabel: 'Total ',
							readOnly:true,
							style:
							{	
								'text-align':'right',
								'font-weight':'bold'
							},
							value:'0',
							width: 145
						}
					]
				}
			]
		}
	);
	var Formload_AdjustArForm = new Ext.Panel
	(
		{
			id: 'Formload_AdjustArForm',
			region: 'center',
			width:'100%',
			anchor:'100%',
			layout: 'form',
			title: '',
			bodyStyle: 'padding:15px',
			border: true,
			bodyStyle: 'background:#FFFFFF;',
			shadhow: true,
			//iconCls: 'GL',
			items: [pnlTR_AdjustArForm, GDtabDetail_AdjustArForm,Total]							  

		}
	);
	
	return Formload_AdjustArForm						
};
 //---------------------------------------------------------------------------------------///
 
function Cetak_AdjustArForm()
{
	var strKriteria;
	if (Ext.get('txtNo_AdjustArForm').dom.value !='' )
	{
		// strKriteria = Ext.get('txtNo_AdjustArForm').dom.value + '##'; //01
		// strKriteria +=ShowDate(Ext.getCmp('dtpTanggal_AdjustArForm').getValue()) + '##'; //04
		// strKriteria += Ext.getCmp('txtTotalDb_AdjustArForm').getValue() + '##'; //05
		// strKriteria += '##'+1+'##';	

		strKriteria = Ext.get('txtNo_AdjustArForm').dom.value + '##'; //01
		strKriteria += 1+'##';
		strKriteria += Ext.get('cboCustEntryARForm').dom.value + '##'; //02
		strKriteria += Ext.getCmp('txtTotalDb_AdjustArForm').getValue() + '##'; //03
		strKriteria += '-' +'##'; //04
		strKriteria +=ShowDate(Ext.getCmp('dtpTanggal_AdjustArForm').getValue()) + '##'; //05
		ShowReport('', '010518', strKriteria);	
		
	};
};
 
 
function GetDTLTRGrid_AdjustArForm() 
{
	// grid untuk detail transaksi	
	var fldDetail = 
	['ara_number','ara_date','arad_line','account','description','value','isdebit','posted','name','debit','kredit']
	
	dsDTLTRList_AdjustArForm = new WebApp.DataStore({ fields: fldDetail })
	dsTmp_AdjustArForm= new WebApp.DataStore({ fields: fldDetail })
	
	gridDTLTR_AdjustArForm = new Ext.grid.EditorGridPanel
	(
		{
			title: 'Dari Detail ' + NamaForm_AdjustArForm,
			stripeRows: true,
			id:'Dttlgrid_AdjustArForm',
			store: dsDTLTRList_AdjustArForm,
			border: false,
			columnLines:true,
			frame:true,
			anchor: '100% 100%',			
			height	:90,
			sm: new Ext.grid.CellSelectionModel
			(
				{
					singleSelect: true,
					listeners: 
					{
						cellselect: function(sm, row, rec)
						{
							cellSelectedDet_AdjustArForm =dsDTLTRList_AdjustArForm.getAt(row);
							CurrentTR_AdjustArForm.row = row;
							CurrentTR_AdjustArForm.data = cellSelectedDet_AdjustArForm.Data;
						}
					}
				}
			),
			cm: TRDetailColumModel_AdjustArForm()
			, viewConfig: 
			{
				forceFit: true
			},
			listeners:
			{
				'afterrender': function()
				{
					this.store.on(
						"load", function()
						{
							getTotalKreDebDetail_AdjustArForm();
						}
					);
					this.store.on(
						"datachanged", function()
						{
							getTotalKreDebDetail_AdjustArForm();
						}
					);
				}
			}
		}
	);
			
	return gridDTLTR_AdjustArForm;
};

function RefreshDataDetail_AdjustArForm(no,tgl)
{	
    var strKriteria_AdjustArForm
    // strKriteria_AdjustArForm = "where ART.ARA_NUMBER ='"+ no +"' and ART.ARA_DATE='"+ ShowDate(tgl) + "' ";
	// dsDTLTRList_AdjustArForm.load
	// (
		// { 
			// params: 
			// { 	
				// Skip: 0,
				// Take: 20,
				// Sort: '',
				// Sortdir: 'ASC',
				// target:'ViewAR_ADJUST_DETAIL',
				// param: strKriteria_AdjustArForm
			// }
		// }
	// );
	// return dsDTLTRList_AdjustArForm;
	Ext.Ajax.request({
		url: baseURL + "index.php/keuangan/functionAdjustPiutang/getListDetailAccount",
		params: {
			ara_number : no,
			ara_date : tgl,
		},
		success: function(o) {
			var cst = Ext.decode(o.responseText);
			if (cst.success === true) 
			{
				dsDTLTRList_AdjustArForm.removeAll();
				var recs=[],
					recType=dsDTLTRList_AdjustArForm.recordType;
				for(var i=0; i<cst.listData.length; i++){
					recs.push(new recType(cst.listData[i]));						
				}
				dsDTLTRList_AdjustArForm.add(recs);
				gridDTLTR_AdjustArForm.getView().refresh();
				
				CalcTotalCr_AdjustArForm();
				CalcTotalDb_AdjustArForm();	
			}
			else {
				ShowPesanError_AdjustArForm('Gagal menampilkan list penerimaan piutang!', 'Simpan Data');
			}
		}
	})
}

//---------------------------------------------------------------------------------------///
			
function TRDetailColumModel_AdjustArForm() 
{
	return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(), 
			{
				id: 'Account_AdjustArForm',
				name: 'Account_AdjustArForm',
				header: "Account",
				dataIndex: 'account',
				sortable: false,
				anchor: '10%',
				editor: new Ext.form.TextField
				(
					{
						id:'fieldAcc_AdjustArForm',
						allowBlank: false,
						enableKeyEvents : true,
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									LookUpAccount_AREntry(" Where A.Account like '" + Ext.get('fieldAcc_AdjustArForm').dom.value + "%'  AND A.TYPE ='D'  ");
								} 
							}
						}
					}
				),
				width: 70
			}, 
			{
				id: 'Name_AdjustArForm',
				name: 'Name_AdjustArForm',
				header: "Nama Account",
				dataIndex: 'name',
				anchor: '30%',
				editor: new Ext.form.TextField
				(
					{
						id:'fieldAccName_AdjustArForm',
						allowBlank: false,
						enableKeyEvents : true,
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									LookUpAccount_AREntry(" Where A.Name like '%"+ Ext.get('fieldAccName_AdjustArForm').dom.value + "%'  AND A.TYPE ='D'  ");
								} 
							}
						}
					}
				)
			}, 
			{
				id: 'DESC_AdjustArForm',
				name: 'DESC_AdjustArForm',
				header: "Keterangan",
				anchor: '30%',
				dataIndex: 'description', 
				editor: new Ext.form.TextField
				(	
					{
						allowBlank: true
					}
				)
			}, 
			{
				dataIndex: 'debit',
				header: 'Debit',
				sortable: true,
				width: 100,
				align: 'right',
				editor: new Ext.form.NumberField
				(
					{
						id:'fnumDebEntry_AdjustArForm',
						allowBlank: false,
						enableKeyEvents : true,
						listeners: 
						{ 
							keyDown: function(a,b,c){
								var line	= gridDTLTR_AdjustArForm.getSelectionModel().selection.cell[0];
								var o=dsDTLTRList_AdjustArForm.getRange()[line].data;
								if(b.getKey()==13){
									if(o.account == '' || o.account == undefined){
										Ext.Msg.show({
											title: 'WARNING',
											msg: 'Account masih kosong, isi account terlebih dahulu!',
											buttons: Ext.MessageBox.OK,
											fn: function (btn) {
												if (btn == 'ok')
												{
													o.debit = 0;
													gridDTLTR_AdjustArForm.getView().refresh();
													gridDTLTR_AdjustArForm.startEditing(line, 4);
												}
											}
										});
									} else{										
										onecolumnentry_AdjustArForm(true);	
										CalcTotalCr_AdjustArForm();
										CalcTotalDb_AdjustArForm();
										gridDTLTR_AdjustArForm.startEditing(line, 5);
									}
								} else{
									onecolumnentry_AdjustArForm(true);	
									CalcTotalCr_AdjustArForm();
									CalcTotalDb_AdjustArForm();
								}
							}
						}
					}
				),
				renderer: function(v, params, record) 
				{
					return formatCurrencyDec(record.data.debit);
				}
			},{
				dataIndex: 'kredit',
				header: 'Kredit',
				sortable: true,
				width: 100,
				align: 'right',
				editor: new Ext.form.NumberField
				(
					{
						id:'fnumKreEntry_AdjustArForm',
						allowBlank: false,
						enableKeyEvents : true,
						listeners: 
						{ 
							keyDown: function(a,b,c){
								// var line = dsDTLTRList_AdjustArForm.getCount()-1; 
								var line	= gridDTLTR_AdjustArForm.getSelectionModel().selection.cell[0];
								var o=dsDTLTRList_AdjustArForm.getRange()[line].data;
									if(b.getKey()==13){
										if(o.account == '' || o.account == undefined){
											Ext.Msg.show({
												title: 'WARNING',
												msg: 'Account masih kosong, isi account terlebih dahulu!',
												buttons: Ext.MessageBox.OK,
												fn: function (btn) {
													if (btn == 'ok')
													{
														o.debit = 0;
														gridDTLTR_AdjustArForm.getView().refresh();
														gridDTLTR_AdjustArForm.startEditing(line, 4);
													}
												}
											});
										} else{	
											if(((o.debit == 0 || o.debit == undefined) && (o.kredit != 0 || o.kredit != undefined )) || ((o.debit != 0 || o.debit != undefined) && (o.kredit == 0 || o.kredit == undefined ))){
												onecolumnentry_AdjustArForm(true);	
												CalcTotalCr_AdjustArForm();
												CalcTotalDb_AdjustArForm();
												if(ValidasiTambahbaris_AdjustArForm('Tambah baris')==1)
												{
													TambahBaris_AdjustArForm();
												}
											} else{
												ShowPesanWarning_AdjustArForm('Isi salah satu, debit atau kredit untuk melanjutkan!','Warning')
											}
										}
									} else{
										onecolumnentry_AdjustArForm(true);	
										CalcTotalCr_AdjustArForm();
										CalcTotalDb_AdjustArForm();
									}
							}
						}
					},
				),
				renderer: function(v, params, record) 
				{
					return formatCurrencyDec(record.data.kredit);
				}
			},
			{
				dataIndex: 'line',
				header: 'line',
				hidden: true,
				width: 100,
				align: 'right',
			}
		]
	)
};

//---------------------------------------------------------------------------------------///
function DataInit_AdjustArForm(rowdata) 
{
	DataAddNew_AdjustArForm = false;

	Ext.get('txtNo_AdjustArForm').dom.readOnly=true;
	Ext.get('cboCustEntryARForm').dom.value = rowdata.customer;
	selectCboEntryCust_AdjustArForm = rowdata.cust_code;
    Ext.get('txtNo_AdjustArForm').dom.value = rowdata.ara_number;
	Ext.get('dtpTanggal_AdjustArForm').dom.value = ShowDate(rowdata.ara_date);
	Ext.get('dtpTanggalDueday_AdjustArForm').dom.value = ShowDate(rowdata.due_date);   
	Ext.get('txtCatatan_AdjustArForm').dom.value = rowdata.notes;
	
	if(rowdata.type == 0)
	{
		Ext.getCmp('radCrEntry_AdjustArForm').setValue(true);
		Ext.getCmp('radDbEntry_AdjustArForm').setValue(false);
	}
	else
	{
		Ext.getCmp('radCrEntry_AdjustArForm').setValue(false);
		Ext.getCmp('radDbEntry_AdjustArForm').setValue(true);
	}
	radTipe_AdjustArForm = rowdata.type;
	RefreshDataDetail_AdjustArForm(rowdata.ara_number,rowdata.ara_date);
	// GetCustAcc_AdjustArForm(rowdata.Cust_Code);
	Ext.getCmp('radCrEntry_AdjustArForm').setDisabled(true);
	Ext.getCmp('radDbEntry_AdjustArForm').setDisabled(true);
	// GetAkunKas_AdjustArForm('CBAR');
	if(rowdata.posted==='t')
	{
		Ext.getCmp('btnApprove_AdjustArForm').disable();
		Ext.getCmp('btnSimpan_AdjustArForm').disable();
		Ext.getCmp('btnSimpanKeluar_AdjustArForm').disable();
		Ext.getCmp('btnHapus_AdjustArForm').disable();
		Ext.getCmp('ChkApproveEntry_AdjustArForm').setValue(true);
	} else{
		Ext.getCmp('btnApprove_AdjustArForm').enable();
		Ext.getCmp('btnSimpan_AdjustArForm').enable();
		Ext.getCmp('btnSimpanKeluar_AdjustArForm').enable();
		Ext.getCmp('btnHapus_AdjustArForm').enable();
		Ext.getCmp('ChkApproveEntry_AdjustArForm').setValue(false);

	}	
};

//---------------------------------------------------------------------------------------///
function AddNew_AdjustArForm() 
{
	DataAddNew_AdjustArForm = true;	
	now_AdjustArForm= new Date;	
	Ext.getCmp('btnApprove_AdjustArForm').disable();
	Ext.get('txtNo_AdjustArForm').dom.readOnly=false;	
	Ext.get('txtNo_AdjustArForm').dom.value = '';	
	Ext.getCmp('ChkApproveEntry_AdjustArForm').setValue(false);	
	Ext.get('txtNoPembayaran_AdjustArForm').dom.value = '';
	Ext.get('txtTotalDb_AdjustArForm').dom.value = '0';
	Ext.get('txtTotalCr_AdjustArForm').dom.value = '0';
	Ext.get('txtCatatan_AdjustArForm').dom.value = 'Summary from cashier post : ADJUST AR';
	Ext.get('dtpTanggal_AdjustArForm').dom.value = ShowDateAkuntansi(now_AdjustArForm);
	Ext.get('dtpTanggalDueday_AdjustArForm').dom.value = ShowDateAkuntansi(now_AdjustArForm);
	Ext.get('cboCustEntryARForm').dom.value = '';
	Ext.getCmp('cboCustEntryARForm').setValue('');
	selectCboEntryCust_AdjustArForm = '';
	Ext.getCmp('radDbEntry_AdjustArForm').setValue(true);
	Ext.getCmp('radCrEntry_AdjustArForm').setValue(false);
	Ext.getCmp('radCrEntry_AdjustArForm').setDisabled(false);
	Ext.getCmp('radDbEntry_AdjustArForm').setDisabled(false);

	dsDTLTRList_AdjustArForm.removeAll();
	rowSelected_AdjustArForm=undefined;	
	isDb=true;
	isCr=true;
	GetAkunKas_AdjustArForm('CBAR');
	Ext.getCmp('btnApprove_AdjustArForm').enable();
	Ext.getCmp('btnSimpan_AdjustArForm').enable();
	Ext.getCmp('btnSimpanKeluar_AdjustArForm').enable();
	Ext.getCmp('btnHapus_AdjustArForm').enable();
	Ext.getCmp('ChkApproveEntry_AdjustArForm').setValue(false);	
};

function getParam_AdjustArForm() 
{
	var params = 
	{
		ara_number: Ext.getCmp('txtNo_AdjustArForm').getValue(),
		ara_date: Ext.get('dtpTanggal_AdjustArForm').getValue(),
		due_date: Ext.get('dtpTanggalDueday_AdjustArForm').getValue(),
		cust_code: selectCboEntryCust_AdjustArForm,
		type: radTipe_AdjustArForm,
		amount:getTotalDebDetailGrid_AdjustArForm(),//getAmount_AdjustArForm(Ext.getCmp('txtTotalDb_AdjustArForm').getValue()),
		notes: Ext.getCmp('txtCatatan_AdjustArForm').getValue(),
	};
	
	params['jumlah']=dsDTLTRList_AdjustArForm.getCount();
	for(var i = 0 ; i < dsDTLTRList_AdjustArForm.getCount();i++)
	{
		params['account-'+i]=dsDTLTRList_AdjustArForm.data.items[i].data.account;
		params['description-'+i]=dsDTLTRList_AdjustArForm.data.items[i].data.description;
		params['line-'+i]=dsDTLTRList_AdjustArForm.data.items[i].data.line;
		if((dsDTLTRList_AdjustArForm.data.items[i].data.debit != 0 || dsDTLTRList_AdjustArForm.data.items[i].data.debit != undefined) && (dsDTLTRList_AdjustArForm.data.items[i].data.kredit == 0 || dsDTLTRList_AdjustArForm.data.items[i].data.kredit == undefined)){
			params['value-'+i]=dsDTLTRList_AdjustArForm.data.items[i].data.debit;
			params['isdebit-'+i]=true;
		} else{
			params['value-'+i]=dsDTLTRList_AdjustArForm.data.items[i].data.kredit;
			params['isdebit-'+i]=false;
		}
	}
	return params
};

//---------------------------------------------------------------------------------------///

function getAmount_AdjustArForm(dblNilai)
{
    var dblAmount;
    dblAmount = dblNilai.replace('Rp.', '')
    for (var i = 0; i < dblAmount.length; i++) {
        var x = dblAmount.substr(i, 1)
        if (x === '.') {
            dblAmount = dblAmount.replace('.', '');
        }
    }    
    // return Ext.num(dblAmount)
    return dblAmount.replace(',', '.')
};

function getAmount2_AdjustArForm(dblNilai)
{
    var dblAmount;
    //dblAmount = dblNilai.replace('Rp.', '')
    for (var i = 0; i < dblAmount.length; i++) {
        var x = dblAmount.substr(i, 1)
        if (x === '.') {
            dblAmount = dblAmount.replace('.', '');
        }
    }    
    // return Ext.num(dblAmount)
    return dblAmount.replace(',00', '')
};

function getItemPanelInput_AdjustArForm(lebar)
{
	var items = 
	{
		layout:'fit',
		anchor:'100%',
		width: lebar - 36,
		height: 140,//130,
		labelAlign: 'right',
		bodyStyle: 'padding:7px 7px 7px 7px',
		items:
		[
			{
				columnWidth:.9,
				width:lebar-36,
				layout: 'form',
				border:false,
				items: 
				[
					getItemPanelNo_AdjustArForm(lebar),
					getItemPanelTerimaDari_AdjustArForm(lebar),
					getItemPanelCatatan_AdjustArForm(lebar),
					getItemPanelAktivaLancar_AdjustArForm(lebar),
				]
			}
		]
	};
	return items;			
};

function getItemPanelAktivaLancar_AdjustArForm(lebar)
{
	var items = 			
	{
	layout:'column',
	border:false,
	width:lebar-36,
	items:
		[
			{
				// columnWidth:0.98,
				columnWidth:.5,
				layout: 'form',
				border:false,
				// labelWidth:111,
				items: 
				[
					{
						xtype:'textfield',
						fieldLabel: 'Referensi ',
						name: 'txtNoPembayaran_AdjustArForm',
						id: 'txtNoPembayaran_AdjustArForm',
						anchor:'96%',
						readOnly:true,					
						hidden:true					
					},
					
				]
			},
			{
				columnWidth:.5,
				region:'Right',
				border:false,				
				layout: 'form',
				labelWidth:111,
				items: 				
				[

					{
						xtype: 'compositefield',
						fieldLabel: 'Tipe ',
						anchor: '100%',
						items: 
						[
							{
								xtype: 'radiogroup',
								id: 'rgTipeTransEntry_AdjustArForm',
								columns: 2,
								width: 120,
								items:
								[
									{
										boxLabel: 'Debit',
										autoWidth: true,
										inputValue: 1,
										name: 'radTipeTransEntry_AdjustArForm',
										id: 'radDbEntry_AdjustArForm'
									},
									{											
										boxLabel: 'Kredit',
										autoWidth: true,
										inputValue: 0,
										name: 'radTipeTransEntry_AdjustArForm',
										id: 'radCrEntry_AdjustArForm'
									}
									
								],
								listeners:
								{
									'change': function(rg, rc)
									{
										radTipe_AdjustArForm = rc.getId().toString() == "radCrEntry_AdjustArForm" ? 0 : 1;
										
										dsDTLTRList_AdjustArForm.removeAll();
									}
								}
							},	
						]
					}
				]
			},
			{
				columnWidth:.2,
				region:'Right',
				border:false,				
				layout: 'form',
				items: 				
				[
					{
						xtype:'checkbox',
						fieldLabel: 'Posted ',
						name: 'ChkApproveEntry_AdjustArForm',
						id: 'ChkApproveEntry_AdjustArForm',
						disabled:true,
						anchor:'96%',
					}
				]
			}
		]
	}
	return items;	
}; 

function getItemPanelCatatan_AdjustArForm(lebar)
{
	var items = 			
	{
	layout:'column',
	border:false,
	width:lebar-36,
	items:
		[
			{
				columnWidth:0.98,
				layout: 'form',
				border:false,
				labelWidth:111,
				items: 
				[
					{
						xtype:'textarea',
						fieldLabel: 'Keterangan ',
						name: 'txtCatatan_AdjustArForm',
						id: 'txtCatatan_AdjustArForm',
						anchor:'99.9%',
						height: 39,
						value:'Summary from cashier post : ADJUST AR',
						autoCreate: {tag: 'input', maxlength: '255'}
					}
				]
			}
		]
	}
	return items;	
}; 

function getItemPanelNo_AdjustArForm(lebar)
{
	var items = 			
	{
	layout:'column',
	border:false,
	width:lebar-36,
	items:
		[
			{
				columnWidth:.5,
				layout: 'form',
				border:false,
				labelWidth:111,
				items: 
				[
					{
						xtype:'textfield',
						fieldLabel: 'Nomor ',
						name: 'txtNo_AdjustArForm',
						id: 'txtNo_AdjustArForm',
						readOnly:false,
						anchor:'95%',
						listeners: 
						{
							'blur' : function()
							{									
																
							},
						}
					}
				]
			},
			{
				columnWidth:.5,
				layout: 'form',
				border:false,
				labelWidth:100,
				items: 
				[
					{
						xtype: 'datefield',
                        fieldLabel: 'Tanggal ',
                        id: 'dtpTanggal_AdjustArForm',
                        name: 'dtpTanggal_AdjustArForm',
                        format: 'd/M/Y',
						value:now_AdjustArForm,
                        anchor: '70%'
					}
				]
			}
		]
	}
	return items;	
};

function getItemPanelTerimaDari_AdjustArForm(lebar)
{
	var items = 			
	{
	layout:'column',
	width:lebar-36,
	border:false,
	items:
		[
			{
				columnWidth:.5,
				layout: 'form',
				border:false,
				labelWidth:111,
				items: 
				[
					{
						xtype: 'compositefield',
						fieldLabel: 'Customer ',
						anchor: '100%',
						items: 
						[
							mCboEntryAdjustArForm(),
							{
								xtype: 'button',
								id: 'btnAddCustomer_AdjustArForm',
								iconCls: 'add',
								handler: function()
								{
									SetAddCustomerLookUp(Ext.getCmp('cboCustEntryARForm').getStore());
								}
							},
						]
					}
				]
			},
			{
				columnWidth:.5,
				layout: 'form',
				border:false,
				items: 
				[
					{
						xtype: 'datefield',
                        fieldLabel: 'Jatuh tempo ',
                        id: 'dtpTanggalDueday_AdjustArForm',
                        name: 'dtpTanggalDueday_AdjustArForm',
                        format: 'd/M/Y',
						value:now_AdjustArForm,
                        anchor: '70%',
                        // readOnly:true
					}
				]
			}
		]
	}
	return items;	
}

function RefreshData_AdjustArForm()
{			
	dsTRList_AdjustArForm.load
	(
		{ 
			params: 
			{ 	
				Skip: 0,
				Take: selectCount_AdjustArForm,
				Sort: 'ARA_DATE',
				Sortdir: 'ASC',
				target:'viACC_AR_ADJUST',
				param: " "
			}
		}
	);
	return dsTRList_AdjustArForm;
};

function mComboMaksData_AdjustArForm()
{
  var cboMaksDataAdjustArForm = new Ext.form.ComboBox
	(
		{
			id:'cboMaksDataAdjustArForm',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Maks.Data ',			
			width:50,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[1, 50], [2, 100],[3, 200],[4, 500],[5, 1000]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCount_AdjustArForm,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectCount_AdjustArForm=b.data.displayText ;
					RefreshDataFilter_AdjustArForm(false);
				} 
			},
			hidden: true
		}
	);
	return cboMaksDataAdjustArForm;
};
function mCboEntryAdjustArForm()
{
	var cboCustEntryARForm;
	var Fields = ['kd_customer', 'customer', 'customer_name','account','term'];
    dsCboEntryCust_AdjustArForm = new WebApp.DataStore({ fields: Fields });
	
	// dsCboEntryCust_AdjustArForm.load
	// (
		// { 
			// params: 
			// { 
				// Skip: 0, 
				// Take: 1000, 
				// Sort: 'Customer', 
				// Sortdir: 'ASC', 
				// target: 'viewCboCustLengkap',
				// param: ''
			// } 
		// }
	// );
	
	Ext.Ajax.request({
		url: baseURL + "index.php/keuangan/functionGeneral/getCustomer",
		params: {query:''},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cboCustEntryARForm.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsCboEntryCust_AdjustArForm.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsCboEntryCust_AdjustArForm.add(recs);
			}
		}
	});
    
    cboCustEntryARForm = new Ext.form.ComboBox
	(
		{
		    id: 'cboCustEntryARForm',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: 'Pilih customer ...',
		    fieldLabel: 'Customer ',
		    align: 'right',
		    anchor:'95%',
		    listWidth:350,
		    store: dsCboEntryCust_AdjustArForm,
		    valueField: 'kd_customer',
		    displayField: 'customer_name',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
					var strParam;
			        selectCboEntryCust_AdjustArForm = b.data.kd_customer;
			        // strDue_AdjustArForm = b.data.term;
					GetCustAcc_AdjustArForm(b.data.kd_customer);			        
					var due_date = new Date().add(Date.DAY, + b.data.term);
					Ext.getCmp('dtpTanggalDueday_AdjustArForm').setValue(due_date);
			        
			    }
			}
		}
	);
	
	return cboCustEntryARForm;
};


function RefreshDataFilter_AdjustArForm(mBol) 
{   
	var strCrt_AdjustArForm='';
	Ext.Ajax.request({
		url: baseURL + "index.php/keuangan/functionAdjustPiutang/getInitList",
		params: {
			tgl_awal : Ext.getCmp('dtpTglAwalFilter_AdjustArForm').getValue(),
			tgl_akhir : Ext.getCmp('dtpTglAkhirFilter_AdjustArForm').getValue(),
			posted : Ext.getCmp('chkFilterApproved_AdjustArForm').getValue(),
			ara_number : Ext.getCmp('txtNoFilter_AdjustArForm').getValue(),
		},
		success: function(o) {
			var cst = Ext.decode(o.responseText);
			if (cst.success === true) 
			{
				dsTRList_AdjustArForm.removeAll();
				var recs=[],
					recType=dsTRList_AdjustArForm.recordType;
				for(var i=0; i<cst.listData.length; i++){
					recs.push(new recType(cst.listData[i]));						
				}
				dsTRList_AdjustArForm.add(recs);
				grListTR_AdjustArForm.getView().refresh();
			}
			else {
				ShowPesanError_AdjustArForm('Gagal menampilkan list penerimaan piutang!', 'Simpan Data');
			}
		}
	})
};

function Approve_AdjustArForm(TglApprove,NoApprove,NoteApprove,jml,nomor,tgl) 
{
	loadMask.show();
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/keuangan/functionGeneral/approve",
			params:  getParamApprove_AdjustArForm(TglApprove,NoApprove, NoteApprove,jml,nomor,tgl), 
			success: function(o) 
			{				
				var cst = Ext.decode(o.responseText);
				loadMask.hide();
				if (cst.success === true)
				{
					ShowPesanInfo_AdjustArForm('Data berhasil di Approve','Approve');
					Ext.getCmp('btnApprove_AdjustArForm').disable();
					Ext.getCmp('btnSimpan_AdjustArForm').disable();
					Ext.getCmp('btnSimpanKeluar_AdjustArForm').disable();
					Ext.getCmp('btnHapus_AdjustArForm').disable();
					Ext.getCmp('ChkApproveEntry_AdjustArForm').setValue(true);	
					
					// RefreshDataFilter_AdjustArForm(false);	
					dsTRListFakturAmount_ApproveAdjust.removeAll();	
					
				}
				else if (cst.success === false && cst.pesan === 0 )
				{
					ShowPesanWarning_AdjustArForm('Data tidak berhasil di Approve, ' + cst.pesan,'Edit Data');
				}
				else 
				{
					ShowPesanError_AdjustArForm('Data tidak berhasil di Approve, ' + cst.pesan,'Approve');
				}										
			}
		}
	)
}

function Datasave_AdjustArForm(IsExit) 
{
	if (ValidasiEntry_AdjustArForm('Simpan Data') == 1 )
	{
		// if (DataAddNew_AdjustArForm == true) 
		// {
		    Ext.Ajax.request
				(
					{
					    // url: "./Datapool.mvc/CreateDataObj",
						
						url: baseURL + "index.php/keuangan/functionAdjustPiutang/save",
					    params: getParam_AdjustArForm(),
					    success: function(o) {
					        var cst = Ext.decode(o.responseText);
					        if (cst.success === true) 
							{
					            ShowPesanInfo_AdjustArForm('Data berhasil di simpan', 'Simpan Data');
								if (IsExit===false)
								{
									Ext.get('txtNo_AdjustArForm').dom.value = cst.ara_number;
								}
					            RefreshDataFilter_AdjustArForm(false);
								RefreshDataDetail_AdjustArForm(Ext.get('txtNo_AdjustArForm').getValue(),Ext.get('dtpTanggal_AdjustArForm').getValue());
								DataAddNew_AdjustArForm=false;
								Ext.get('txtNo_AdjustArForm').dom.readOnly=true;
					        }
					        else if (cst.success === false && cst.pesan === 0) 
							{
					            ShowPesanWarning_AdjustArForm('Data tidak berhasil di simpan ' + cst.pesan , 'Simpan Data');
					        }
					        else {
					            ShowPesanError_AdjustArForm('Data tidak berhasil di simpan, Error : ' + cst.pesan, 'Simpan Data');
					        }
					    }
					}
				)
		// }
		// else 
		// {
			// Ext.Ajax.request
			 // (
				// {
					 // url: "./Datapool.mvc/UpdateDataObj",
					 // params:  getParam_AdjustArForm(), 
					 // success: function(o) 
					 // {
						
						    // var cst = Ext.decode(o.responseText);
							// if (cst.success === true)
							// {
								// ShowPesanInfo_AdjustArForm('Data berhasil di edit','Edit Data');
								// RefreshDataFilter_AdjustArForm(false);
								// RefreshDataDetail_AdjustArForm(Ext.get('txtNo_AdjustArForm').getValue(),Ext.get('dtpTanggal_AdjustArForm').getValue());
							// }
							// else if (cst.success === false && cst.pesan === 0 )
							// {
								// ShowPesanWarning_AdjustArForm('Data tidak berhasil di edit ' +  cst.pesan ,'Edit Data');
							// }
							// else {
								// ShowPesanError_AdjustArForm('Data tidak berhasil di edit '  + cst.pesan ,'Edit Data');
							// }
												
					// }
				// }
			// )
		// }
	}
};

function ValidasiEntry_AdjustArForm(modul)
{
	var x = 1;
	
	// if (Ext.get('txtNo_AdjustArForm').getValue()=='')
	// {
		// ShowPesanWarning_AdjustArForm('Nomor belum diisi',modul);
		// x=0;
	// }
	if(selectCboEntryCust_AdjustArForm == '' || selectCboEntryCust_AdjustArForm == undefined)
	{
		ShowPesanWarning_AdjustArForm('Customer belum dipilih',modul);
		x=0;
	}else if ( dsDTLTRList_AdjustArForm.getCount() <= 0)
	{
		ShowPesanWarning_AdjustArForm('Detai Adjusment AR belum di isi',modul);
		x=0;
	}else if( getAmount_AdjustArForm(Ext.get('txtTotalCr_AdjustArForm').getValue()) != getAmount_AdjustArForm(Ext.get('txtTotalDb_AdjustArForm').getValue()))
	{
		ShowPesanWarning_AdjustArForm('Detai Adjusment AR tidak Balanced',modul);
		x=0;
	}else if (Ext.get('txtCatatan_AdjustArForm').getValue().length > 255)
	{
		ShowPesanWarning_CSAPForm('Keterangan Tidak boleh lebih dari 255 Karakter',modul);
		x=0;
	};
	
	for(var i = 0 ; i < dsDTLTRList_AdjustArForm.getCount();i++)
	{
		var o = dsDTLTRList_AdjustArForm.data.items[i].data;
		if(o.account == '' || o.account == undefined){
			dsDTLTRList_AdjustArForm.removeAt(i);
		}
	}
	
	return x;
};

function ValidasiApprove_AdjustArForm(modul)
{
	var x = 1;
	if(Ext.get('txtNo_AdjustArForm').getValue() == '')
	{
		ShowPesanWarning_AdjustArForm('Data belum disimpan',modul);
		x=0;
	}else if(selectCboEntryCust_AdjustArForm == '' || selectCboEntryCust_AdjustArForm == undefined)
	{
		ShowPesanWarning_AdjustArForm('Customer belum dipilih',modul);
		x=0;
	}else if ( dsDTLTRList_AdjustArForm.getCount() <= 0)
	{
		ShowPesanWarning_AdjustArForm('Detai Adjusment AR belum di isi',modul);
		x=0;
	}else if( getAmount_AdjustArForm(Ext.get('txtTotalCr_AdjustArForm').getValue()) != getAmount_AdjustArForm(Ext.get('txtTotalDb_AdjustArForm').getValue()))
	{
		ShowPesanWarning_AdjustArForm('Detai Adjusment AR tidak Balanced',modul);
		x=0;
	}
	return x;
};

function ValidasiTambahbaris_AdjustArForm(modul)
{
	var x = 1;
	if(selectCboEntryCust_AdjustArForm == '' || selectCboEntryCust_AdjustArForm == undefined)
	{
		ShowPesanWarning_AdjustArForm('Customer belum dipilih',modul);
		x=0;
	}else if(dsDTLTRList_AdjustArForm.getCount() >=2)
	{
		ShowPesanWarning_AdjustArForm('Tidak boleh lebih dari 2 baris!',modul);
		x=0;
	}
	// else if ( dsDTLTRList_AdjustArForm.getCount() >= 1)
	// {
		// if(parseFloat(Ext.getCmp('fnumDebEntry_AdjustArForm').getValue()) == 0 || parseFloat(Ext.getCmp('fnumDebEntry_AdjustArForm').getValue())==undefined)
		// {
			// ShowPesanWarning_AdjustArForm('Nilai debit belum diinput',modul);
			// x=0;
		// }
	// }
	
	return x;
};

function ShowPesanWarning_AdjustArForm(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};

function ShowPesanError_AdjustArForm(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR
		}
	);
};

function ShowPesanInfo_AdjustArForm(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO
		}
	);
};

function HapusBaris_AdjustArForm()
{
	if (cellSelectedDet_AdjustArForm.data.account != '' && cellSelectedDet_AdjustArForm.data.account != undefined)
	{
		Ext.Msg.show
		(
			{
			   title:'Hapus Baris',
			   msg: 'Apakah baris ini akan dihapus ?' + ' ' + 'Baris :'+ ' ' + (CurrentTR_AdjustArForm.row + 1) + ' dengan Account : '+ ' ' + cellSelectedDet_AdjustArForm.data.account ,
			   buttons: Ext.MessageBox.YESNO,
			   fn: function (btn) 
			   {			
				   if (btn =='yes') 
					{
						var line = gridDTLTR_AdjustArForm.getSelectionModel().selection.cell[0];
						var o = dsDTLTRList_AdjustArForm.getRange()[line].data;
						if (o.line != '' && o.line != undefined){
							dsDTLTRList_AdjustArForm.removeAt(CurrentTR_AdjustArForm.row);
							HapusBaris_AdjustArFormDB(cellSelectedDet_AdjustArForm.data.line);
					    	cellSelectedDet_AdjustArForm = undefined;
						} else{
							dsDTLTRList_AdjustArForm.removeAt(CurrentTR_AdjustArForm.row);
							CalcTotalCr_AdjustArForm();
							CalcTotalDb_AdjustArForm();
	    					cellSelectedDet_AdjustArForm = undefined;
						}

					} 
			   },
			   icon: Ext.MessageBox.QUESTION
			}
		);
	}
	else
	{
	    dsDTLTRList_AdjustArForm.removeAt(CurrentTR_AdjustArForm.row);
	    cellSelectedDet_AdjustArForm = undefined;
	}
	
	
};

function TambahBaris_AdjustArForm()
{
	var x=true;
	
	if (dsDTLTRList_AdjustArForm.getCount() > 0)
	{
		if(dsDTLTRList_AdjustArForm.data.items[dsDTLTRList_AdjustArForm.getCount()-1].data.account==='')
		{
			ShowPesanWarning_AdjustArForm('Customer belum dipilih','Tambah baris');
			x=false;
		}
		
		if(dsDTLTRList_AdjustArForm.data.items[dsDTLTRList_AdjustArForm.getCount()-1].data.account != '' && StrAccCust_AdjustArForm != undefined)
		{
			if (dsDTLTRList_AdjustArForm.data.items[dsDTLTRList_AdjustArForm.getCount()-1].data.account == StrAccCust_AdjustArForm)
			{
				isDb=false;
			}
		}else{
			isDb=false;
		}
	} 
	
	if (x==true){
		if (isDb==false){
			TambahBarisKosong_AdjustArForm()
		} else{
			TambahBarisDb_AdjustArForm()
		}
	}
};

function TambahBarisKosong_AdjustArForm()
{
	var records = new Array();
	records.push(new dsDTLTRList_AdjustArForm.recordType());
	dsDTLTRList_AdjustArForm.add(records);
	
	var row =dsDTLTRList_AdjustArForm.getCount()-1;
	gridDTLTR_AdjustArForm.startEditing(row, 1);
}

function TambahBarisDb_AdjustArForm()
{
	for(var i=0,iLen=strTotalRecord; i<iLen ;i++){
		var records=[];
		records.push(new dsDTLTRList_AdjustArForm.recordType());
		dsDTLTRList_AdjustArForm.add(records);
		dsDTLTRList_AdjustArForm.data.items[i].data.account=StrAccCust_AdjustArForm;
		dsDTLTRList_AdjustArForm.data.items[i].data.name=StrNmAccCust_AdjustArForm;
		dsDTLTRList_AdjustArForm.data.items[i].data.description=Ext.get('txtCatatan_AdjustArForm').getValue();
		dsDTLTRList_AdjustArForm.data.items[i].data.kredit=0;
		dsDTLTRList_AdjustArForm.data.items[i].data.debit=0;
	}
	gridDTLTR_AdjustArForm.getView().refresh();
	
	var row =dsDTLTRList_AdjustArForm.getCount()-1;
	gridDTLTR_AdjustArForm.startEditing(row, 4);
}
function TambahBarisCr_AdjustArForm()
{
	var p = new mRecord_AdjustArForm
	(
		{
			ACCOUNT:StrAccKas_AdjustArForm,
			NAMAACCOUNT: StrNmAccKas_AdjustArForm,
			Description: Ext.get('txtCatatan_AdjustArForm').getValue(),		
			VALUE: 0,				
			ARAD_LINE:'',
			Kredit:0,
			Debit:0
		}
	);
	dsDTLTRList_AdjustArForm.insert(dsDTLTRList_AdjustArForm.getCount(), p);
/*	if (radTipe_AdjustArForm == 0)
	{
		Ext.getCmp('fnumKreEntry_AdjustArForm').setDisabled(true);
		Ext.getCmp('fnumDebEntry_AdjustArForm').setDisabled(false);
	}else
	{
		Ext.getCmp('fnumKreEntry_AdjustArForm').setDisabled(false);
		Ext.getCmp('fnumDebEntry_AdjustArForm').setDisabled(true);
	}*/
}
function DataDelete_AdjustArForm() 
{
	if (ValidasiEntry_AdjustArForm('Hapus Data') == 1 )
	{	
		Ext.Ajax.request
		(
			{
				// url: "./Datapool.mvc/DeleteDataObj",
				url: baseURL + "index.php/keuangan/functionAdjustPiutang/delete",
				params:  getParam_AdjustArForm(), 
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true)					
					{
							ShowPesanInfo_AdjustArForm('Data berhasil di hapus','Hapus Data');
							RefreshDataFilter_AdjustArForm(false);
							AddNew_AdjustArForm();
					}else 
					{
						ShowPesanError_AdjustArForm(cst.pesan,'Hapus Data');
					}
				}
			}
		)
	}
};


function CekArrKosong_AdjustArForm()
{
	var x=1;
	for(var i = 0 ; i < dsDTLTRList_AdjustArForm.getCount();i++)
	{
		if (dsDTLTRList_AdjustArForm.data.items[i].data.account=='' || dsDTLTRList_AdjustArForm.data.items[i].data.ACCOUNT==undefined){
			x=0;
		}

	}		
	return x;
};

function getArrDetail_AdjustArForm()
{
	
	var x='';

	for(var i = 0 ; i < dsDTLTRList_AdjustArForm.getCount();i++)
	{
		var y='';
		var z='@@##$$@@';
		
		// if (DataAddNew_AdjustArForm === true)
		if (dsDTLTRList_AdjustArForm.data.items[i].data.ARAD_LINE==="")
		{
			y = "ARAD_LINE=" + 0 
		}
		else
		{	
			y = 'ARAD_LINE=' + dsDTLTRList_AdjustArForm.data.items[i].data.ARAD_LINE
		}		
		y += z + 'ACCOUNT='+dsDTLTRList_AdjustArForm.data.items[i].data.ACCOUNT
		y += z + 'DESCRIPTION=' + dsDTLTRList_AdjustArForm.data.items[i].data.Description
		if ( dsDTLTRList_AdjustArForm.data.items[i].data.Debit != 0 )
		{
			y += z + 'Value=' + parseFloat(dsDTLTRList_AdjustArForm.data.items[i].data.Debit)
			y += z + 'IsDebit=1'
		}
		else
		{
			y += z + 'Value=' + parseFloat(dsDTLTRList_AdjustArForm.data.items[i].data.Kredit)
			y += z + 'IsDebit=0'
		}
		// y += z + 'VALUE=' + dsDTLTRList_AdjustArForm.data.items[i].data.VALUE
		// y += z + 'ISDEBIT=' + is_debit //dsDTLTRList_AdjustArForm.data.items[i].data.ISDEBIT
		y += z + 'POSTED=' + dsDTLTRList_AdjustArForm.data.items[i].data.POSTED
		y += z + 'ARA_NUMBER='+dsDTLTRList_AdjustArForm.data.items[i].data.ARA_NUMBER;
		y += z + 'ARA_DATE='+dsDTLTRList_AdjustArForm.data.items[i].data.ARA_DATE;
		if (i === (dsDTLTRList_AdjustArForm.getCount()-1))
		{
			x += y 
		}
		else
		{
			x += y + '##[[]]##'
		}
	}		
	return x;
};


function CalcTotalDb_AdjustArForm(idx)
{
	var totalDebit=0;
	for(var i=0;i < dsDTLTRList_AdjustArForm.getCount();i++)
	{
		var o = dsDTLTRList_AdjustArForm.getRange()[i].data;
		if(o.account != '' || o.account != undefined){			
			totalDebit += parseFloat(dsDTLTRList_AdjustArForm.data.items[i].data.debit);
		} else{
			totalDebit += 0;
		}
	}
	Ext.get('txtTotalDb_AdjustArForm').dom.value=formatCurrencyDec(totalDebit);
};

function CalcTotalCr_AdjustArForm(idx)
{
	var totalKredit=0;
	for(var i=0;i < dsDTLTRList_AdjustArForm.getCount();i++)
	{
		var o = dsDTLTRList_AdjustArForm.getRange()[i].data;
		if(o.account != '' || o.account != undefined){			
			totalKredit += parseFloat(o.kredit);	
		} else{
			totalKredit += 0;
		}
	}
	Ext.get('txtTotalCr_AdjustArForm').dom.value=formatCurrencyDec(totalKredit);
};

function ButtonDisabled_AdjustArForm(mBol)
{
	Ext.get('btnTambahBaris_AdjustArForm').dom.disabled=mBol;
	Ext.get('btnHapusBaris_AdjustArForm').dom.disabled=mBol;
	Ext.get('btnSimpan_AdjustArForm').dom.disabled=mBol;
	Ext.get('btnSimpanKeluar_AdjustArForm').dom.disabled=mBol;
	Ext.get('btnHapus_AdjustArForm').dom.disabled=mBol;	
	Ext.get('btnApprove_AdjustArForm').dom.disabled=mBol;	
};	


function HapusBaris_AdjustArFormDB(Line) 
{	
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/keuangan/functionAdjustPiutang/deleteRow",
			params: getParam_AdjustArFormHapusBaris(Line), 
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success == true) 
				{						
					RefreshDataFilter_AdjustArForm(false);			
					ShowPesanInfo_AdjustArForm('Data berhasil dihapus','Information');
				}
				else 
				{
					ShowPesanError_AdjustArForm('Data gagal dihapus','Error');
				}
			}
		}
	)
	
};

function getParam_AdjustArFormHapusBaris(Line) 
{
	var params = 
	{
		ara_number:Ext.get('txtNo_AdjustArForm').getValue(),
		ara_date:Ext.get('dtpTanggal_AdjustArForm').getValue(),
		line:Line,	
	};
	return params
};

function getParamApprove_AdjustArForm(TglApprove, NoApprove,NoteApprove,jml,nomor,tgl) 
{
	//alert(getArrDetailFakturAdjust_AdjustArForm()+'-'+jml)
	var notag;
	var dateTag;
	if (dsTRListFakturAmount_ApproveAdjust.getCount()>0){

		if (radTipe_AdjustArForm == 1)
		{
			strInfo='ARAF-DB'	
		}else{
			strInfo='ARAF-CR'	
		}

		notag=nomor;
		dateTag=tgl;

	}

	else{

		if (radTipe_AdjustArForm == 1)
		{
			strInfo='ARA-DB'	
		}else{
			strInfo='ARA-CR'	
		}

		notag=NoApprove;
		dateTag=TglApprove;
	}
	
	var params = 
	{
		Table: 'viACC_AR_ADJUST',
		ara_number:Ext.get('txtNo_AdjustArForm').getValue(),
		ara_date:Ext.get('dtpTanggal_AdjustArForm').getValue(),
		cust_code:selectCboEntryCust_AdjustArForm,
		due_date:Ext.get('dtpTanggalDueday_AdjustArForm').getValue(),
		amount:getTotalDebDetailGrid_AdjustArForm(),//getAmount_AdjustArForm(Ext.get('txtTotalDb_AdjustArForm').getValue()),
		notes:NoteApprove,
		type: radTipe_AdjustArForm,
        info: strInfo,
		ar_number:nomor,
		ar_date:tgl,
	};
	params['jumlah_trans']=dsDTLTRList_AdjustArForm.getCount();
	for(var i = 0 ; i < dsDTLTRList_AdjustArForm.getCount();i++)
	{
		params['account_trans-'+i]=dsDTLTRList_AdjustArForm.data.items[i].data.account;
		params['description_trans-'+i]=dsDTLTRList_AdjustArForm.data.items[i].data.description;
		params['line_trans-'+i]=dsDTLTRList_AdjustArForm.data.items[i].data.line;
		if((dsDTLTRList_AdjustArForm.data.items[i].data.debit != 0 || dsDTLTRList_AdjustArForm.data.items[i].data.debit != undefined) && (dsDTLTRList_AdjustArForm.data.items[i].data.kredit == 0 || dsDTLTRList_AdjustArForm.data.items[i].data.kredit == undefined)){
			params['value_trans-'+i]=dsDTLTRList_AdjustArForm.data.items[i].data.debit;
			params['isdebit_trans-'+i]=true;
		} else{
			params['value_trans-'+i]=dsDTLTRList_AdjustArForm.data.items[i].data.kredit;
			params['isdebit_trans-'+i]=false;
		}
	}
	
	params['jumlah_approve']=dsTRListFakturAmount_ApproveAdjust.getCount();
	for(var i = 0 ; i < dsTRListFakturAmount_ApproveAdjust.getCount();i++)
	{
		params['reff_approve-'+i]=dsTRListFakturAmount_ApproveAdjust.data.items[i].data.REFF;
		params['reff_date_approve-'+i]=dsTRListFakturAmount_ApproveAdjust.data.items[i].data.REFF_DATE;
		params['ar_number_approve-'+i]=Ext.getCmp('txtfilterNo_ApproveAdjust').getValue();
		params['ar_date_approve-'+i]=Ext.get('dtpfilterTanggal_ApproveAdjust').getValue();
		params['paid_approve-'+i]=dsTRListFakturAmount_ApproveAdjust.data.items[i].data.PAID;
		params['type_approve-'+i]='1';
		params['line_approve-'+i]=dsTRListFakturAmount_ApproveAdjust.data.items[i].data.LINE;
	}
	return params
};

function GetCustAcc_AdjustArForm(param)
{
	 Ext.Ajax.request
	 (
		{
			// url: "./Module.mvc/ExecProc",
			url: baseURL + "index.php/keuangan/functionGeneral/getInfoCustomer",
			params: 
			{
				kd_customer:param
			},
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true )
				{
				 	StrAccCust_AdjustArForm = cst.account;
				 	StrNmAccCust_AdjustArForm = cst.name;
				 	strDue_AdjustArForm = cst.term;
				 	strTotalRecord = cst.totalrecords;
				}
				else
				{
					ShowPesanInfo_AdjustArForm('Customer tidak ditemukan','Customer');
				}
			}

		}
		);
};

function onecolumnentry_AdjustArForm(isDebit)
{
	var db = 0;
	var cr = 0;
	if(CurrentTR_AdjustArForm.row === dsDTLTRList_AdjustArForm.getCount() - 1)
	{
		if (Ext.get('fnumKreEntry_AdjustArForm') != null )
		{
			if (Ext.num(Ext.getCmp('fnumKreEntry_AdjustArForm').getValue()) != null)
			{
				cr = parseFloat(Ext.getCmp('fnumKreEntry_AdjustArForm').getValue());
			}
		}
		
		if (Ext.get('fnumDebEntry_AdjustArForm') != null )
		{
			if (Ext.num(Ext.getCmp('fnumDebEntry_AdjustArForm').getValue()) != null)
			{
				db = parseFloat(Ext.getCmp('fnumDebEntry_AdjustArForm').getValue());
			}
		}
	}
	else
	{
		cr = parseFloat(dsDTLTRList_AdjustArForm.data.items[CurrentTR_AdjustArForm.row].data.Kredit);
		db = parseFloat(dsDTLTRList_AdjustArForm.data.items[CurrentTR_AdjustArForm.row].data.Debit);
	}
	
	// jika entry di debit, di Kredit diisi 0
	if(isDebit)
	{
		if(db != 0 && cr != 0)
		{
			// alert('isdebit 1'+isDebit)
			Ext.getCmp('fnumKreEntry_AdjustArForm').setValue(0);
			dsDTLTRList_AdjustArForm.data.items[CurrentTR_AdjustArForm.row].data.Kredit = 0;
		}
	}
	else
	{
		if(db != 0 && cr != 0)
		{
			Ext.getCmp('fnumDebEntry_AdjustArForm').setValue(0);
			dsDTLTRList_AdjustArForm.data.items[CurrentTR_AdjustArForm.row].data.Debit = 0;
		}
	}
}

function getTotalKreDebDetail_AdjustArForm()
{
	var total = [0,0]; // [0] -> Kredit, [1] -> debit
	var debitRow = 0;
	var kreditRow = 0;
	if(dsDTLTRList_AdjustArForm.getCount() > 0)
	{
		for(var i = 0;i < dsDTLTRList_AdjustArForm.getCount();i++)
		{
			kreditRow = dsDTLTRList_AdjustArForm.data.items[i].data.kredit;
			debitRow = dsDTLTRList_AdjustArForm.data.items[i].data.debit;
			// if (kreditRow != undefined)
			// {
				total[0] = total[0] + parseFloat(kreditRow);
				total[1] = total[1] + parseFloat(debitRow);
			// }
		}
	}
	
	Ext.getCmp('txtTotalCr_AdjustArForm').setValue(formatCurrencyDec(total[0]));
	Ext.getCmp('txtTotalDb_AdjustArForm').setValue(formatCurrencyDec(total[1]));
}


function getTotalDebDetailGrid_AdjustArForm()
{
	var total = 0; // [0] -> Kredit, [1] -> debit
	var debitRow = 0;
	//var kreditRow = 0;
	if(dsDTLTRList_AdjustArForm.getCount() > 0)
	{
		for(var i = 0;i < dsDTLTRList_AdjustArForm.getCount();i++)
		{
			debitRow = dsDTLTRList_AdjustArForm.data.items[i].data.debit;
				total= total + parseFloat(debitRow);
		}
	}
	
	return total;
}


function GetDueDate_AdjustArForm(param)
{
	 Ext.Ajax.request
	 (
		{
			url: "./Module.mvc/ExecProc",
			params:
			{
				UserID: 'Admin',
				ModuleID: 'GetDueDayCustomer',
				Params:	param 
			},
			success: function(o) 
			{
			var cst = Ext.decode(o.responseText);
				if (cst.success === true )
				{
				 	Ext.get('dtpTanggalDueday_AdjustArForm').dom.value=ShowDate(cst.DueDay)
				}
				else
				{
					ShowPesanInfoRekapSP3D	('Customer tidak ditemukan','Customer');
				}
			}

		}
		);
};

function GetAkunKas_AdjustArForm(param)
{
	 Ext.Ajax.request
	 (
		{
			url: "./Module.mvc/ExecProc",
			params:
			{
				UserID: 'Admin',
				ModuleID: 'GetAccKasArAp',
				Params:	param 
			},
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true )
				{
				 	StrNmAccKas_AdjustArForm=cst.NamaAcc
				 	StrAccKas_AdjustArForm=cst.Account
				}
				else
				{
					//ShowPesanInfoRekapSP3D	('Kas tidak ditemukan','Customer');
				}
			}

		}
		);
};

////---------------------FIND DIALOG---------------
var nowFind_AdjustArForm = new Date();
var selectcboOperator_AdjustArForm;
var selectcboField_AdjustArForm;
//var frmFindDlg_AdjustArForm= fnFindDlg_AdjustArForm();
//frmFindDlg_AdjustArForm.show();
var winFind_AdjustArForm;

function fnFindDlg_AdjustArForm()
{  	
    winFind_AdjustArForm = new Ext.Window
	(
		{ 
			id: 'winFindx_AdjustArForm',
			title: 'Find',
			closeAction: 'destroy',
			width:350,
			height: 200,					
			border: false,
			resizable:false,
			plain: true,
			layout: 'fit',
			iconCls: 'find',
			modal: true,
			items: [ItemFindDlg_AdjustArForm()]
			
		}
	);

	winFind_AdjustArForm.show();
	
    return winFind_AdjustArForm; 
};

function ItemFindDlg_AdjustArForm() 
{	
	var PnlFindDlg_AdjustArForm = new Ext.Panel
	(
		{ 
			id: 'PnlFindDlg_AdjustArForm',
			fileUpload: true,
			layout: 'anchor',
			// width:350,
			height: 90,
			anchor: '100%',
			bodyStyle: 'padding:5px',
			border: true,
			items: 
			[

				{
					xtype: 'compositefield',
					fieldLabel: '',
					anchor: '100%',
					items: 
					[
						{
							xtype: 'fieldset',
							title: '',
							width: 325,
							height: 125, 
							items: 
							[
								mComboFindField_AdjustArForm(),
								mComboOperator_AdjustArForm(),
								getItemFindKeyWord_AdjustArForm(),
								getItemFindTgl_AdjustArForm(),
								
				
							]
						}						
					]
				},
				{
					layout: 'hBox',
					border: false,
					defaults: { margins: '0 5 0 0' },
					style:{'margin-left':'21px','margin-top':'5px'},
					anchor: '100%',
					layoutConfig: 
					{
						padding: '3',
						pack: 'end',
						align: 'middle'
					},
					items: 
					[
						{
							xtype: 'button',
							text: 'Ok',
							width: 70,
							hideLabel: true,
							id: 'btnOkLap_AdjustArForm',
							handler: function() 
							{
								if (ValidasiReport_AdjustArForm()==1){
									RefreshDataFindRecord_AdjustArForm()
								}
								
							}
						},
						{
							xtype: 'button',
							text: 'Cancel',
							width: 70,
							hideLabel: true,
							id: 'btnCancelLap_AdjustArForm',
							handler: function() 
							{
								winFind_AdjustArForm.close();
							}
						}
					]
				}
			]
		}
	);
 
    return PnlFindDlg_AdjustArForm;
};

function RefreshDataFindRecord_AdjustArForm() 
{   
	var strparam='';
	if(selectcboOperator_AdjustArForm==1){

		if (selectcboField_AdjustArForm=="AR.ARA_Date"){

			strparam=" WHERE "+selectcboField_AdjustArForm+" like '"+ ShowDateAkuntansi(Ext.getCmp('dtpTglFind_AdjustArForm').getValue()) + "%' "; 

		} else{
			strparam=" WHERE "+selectcboField_AdjustArForm+" like '"+ Ext.getCmp('txtKeyWordFind_AdjustArForm').getValue() + "%' "; 

		}		
		
	} else if (selectcboOperator_AdjustArForm==2){

		if (selectcboField_AdjustArForm=="AR.ARA_Date"){

			strparam=" WHERE "+selectcboField_AdjustArForm+" = '"+ ShowDateAkuntansi(Ext.getCmp('dtpTglFind_AdjustArForm').getValue()) + "' "; 

		} else{
			strparam=" WHERE "+selectcboField_AdjustArForm+" = '"+ Ext.getCmp('txtKeyWordFind_AdjustArForm').getValue() + "' "; 

		}

	} else if (selectcboOperator_AdjustArForm==3){
		 
		if (selectcboField_AdjustArForm=="AR.ARA_Date"){

			strparam=" WHERE "+selectcboField_AdjustArForm+" < '"+ ShowDateAkuntansi(Ext.getCmp('dtpTglFind_AdjustArForm').getValue()) + "' "; 

		} else{
			strparam=" WHERE "+selectcboField_AdjustArForm+" < '"+ Ext.getCmp('txtKeyWordFind_AdjustArForm').getValue() + "' ";

		}

	} else if (selectcboOperator_AdjustArForm==4){

		if (selectcboField_AdjustArForm=="AR.ARA_Date"){

			strparam=" WHERE "+selectcboField_AdjustArForm+" > '"+ ShowDateAkuntansi(Ext.getCmp('dtpTglFind_AdjustArForm').getValue()) + "' "; 

		} else{
			strparam=" WHERE "+selectcboField_AdjustArForm+" > '"+ Ext.getCmp('txtKeyWordFind_AdjustArForm').getValue() + "' ";

		}
		
	} else if (selectcboOperator_AdjustArForm==5){
		if (selectcboField_AdjustArForm=="AR.ARA_Date"){

			strparam=" WHERE "+selectcboField_AdjustArForm+" <= '"+ ShowDateAkuntansi(Ext.getCmp('dtpTglFind_AdjustArForm').getValue()) + "' "; 

		} else{
			strparam=" WHERE "+selectcboField_AdjustArForm+" <= '"+ Ext.getCmp('txtKeyWordFind_AdjustArForm').getValue() + "' ";

		}
		
		
	} else if (selectcboOperator_AdjustArForm==6){
		if (selectcboField_AdjustArForm=="ART.ARA_Date"){

			strparam=" WHERE "+selectcboField_AdjustArForm+" >= '"+ ShowDateAkuntansi(Ext.getCmp('dtpTglFind_AdjustArForm').getValue()) + "' "; 

		} else{
			strparam=" WHERE "+selectcboField_AdjustArForm+" >= '"+ Ext.getCmp('txtKeyWordFind_AdjustArForm').getValue() + "' ";

		}
		
	} else if (selectcboOperator_AdjustArForm==7){
		if (selectcboField_AdjustArForm=="AR.ARA_Date"){

			strparam=" WHERE "+selectcboField_AdjustArForm+" <> '"+ ShowDateAkuntansi(Ext.getCmp('dtpTglFind_AdjustArForm').getValue()) + "' "; 

		} else{
			strparam=" WHERE "+selectcboField_AdjustArForm+" <> '"+ Ext.getCmp('txtKeyWordFind_AdjustArForm').getValue() + "' ";

		}
		
	} 
	
    // {  
		// dsTRList_AdjustArForm.load
		// (
			// { 
				// params:  
				// {   
					// Skip: 0, 
					// Take: selectCount_AdjustArForm, 
					// Sort: 'ARA_DATE', 
					// Sortdir: 'ASC', 
					// target:'viACC_AR_ADJUST',
					// param: strparam
				// }			
			// }
		// );        
    // }
	Ext.Ajax.request({
		url: baseURL + "index.php/keuangan/functionAdjustPiutang/getDataFind",
		params: {
			criteria : strparam
		},
		success: function(o) {
			var cst = Ext.decode(o.responseText);
			if (cst.success === true) 
			{
				dsTRList_AdjustArForm.removeAll();
				var recs=[],
					recType=dsTRList_AdjustArForm.recordType;
				for(var i=0; i<cst.listData.length; i++){
					recs.push(new recType(cst.listData[i]));						
				}
				dsTRList_AdjustArForm.add(recs);
				grListTR_AdjustArForm.getView().refresh();
			}
			else {
				ShowPesanError_AdjustArForm('Gagal menampilkan list penerimaan piutang yang di cari!', 'Simpan Data');
			}
		}
	})
	
};


function ValidasiReport_AdjustArForm()
{
	var x=1;
	
	if(selectcboField_AdjustArForm == undefined || selectcboField_AdjustArForm == "")
	{
		ShowPesanWarningFindDlg_AdjustArForm('Field belum dipilih','Find');
		x=0;		
	};

	if(selectcboOperator_AdjustArForm ==undefined || selectcboOperator_AdjustArForm== "")
	{
		
		ShowPesanWarningFindDlg_AdjustArForm('Operator belum di pilih','Find');
		x=0;
			
	};

	if(Ext.getCmp('txtKeyWordFind_AdjustArForm').getValue()== "" && criteriaTanggalFind == false)
	{
		
		ShowPesanWarningFindDlg_AdjustArForm('Keyword belum di isi','Find');
		x=0;	
	};

	return x;
};

function ShowPesanWarningFindDlg_AdjustArForm(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING,
		   width:180
		}
	);
};

function getItemFindTgl_AdjustArForm() 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
		{
			xtype: 'compositefield',
			fieldLabel: '-',
			anchor: '100%',
			items: 
			[
				 {
					columnWidth: 1,
					layout: 'form',
					border: false,
					labelAlign: 'left',
					labelWidth: 100,
					items:
					[
						{
							xtype: 'datefield',
							fieldLabel: 'Periode ',
							id: 'dtpTglFind_AdjustArForm',
							format: 'd/M/Y',
							disabled:true,
							value:now_AdjustArForm,
							width:190

						}
					]
		    	}
			]
		}
		   
		]
	}
    return items;
};

function getItemFindKeyWord_AdjustArForm() 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
		{
			xtype: 'compositefield',
			fieldLabel: '-',
			anchor: '100%',
			items: 
			[
				 {
					columnWidth: 1,
					layout: 'form',
					border: false,
					labelAlign: 'left',
					labelWidth: 100,
					items:
					[
						{
							xtype: 'textfield',
							fieldLabel: 'Key Word ',
							id: 'txtKeyWordFind_AdjustArForm',
							name: 'txtKeyWordFind_AdjustArForm',
							width:190
						}
					]
		    	}
			 
			]
		}
		   
		]
	}
    return items;
};

function mComboFindField_AdjustArForm()
{
	var cboFindField_AdjustArForm = new Ext.form.ComboBox
	(
		{
			id:'cboFindField_AdjustArForm',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih Field',
			width:190,
			fieldLabel: 'Field Name',	
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: [
							["AR.ARA_NUMBER", "Number"], 
							["AR.ARA_Date", "Date"],
							["AR.Cust_Code", "Cust Code"], 
							["C.Customer", "Customer"],
							["AR.Notes", "Notes"]
						  ]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			listeners:  
			{
				'select': function(a,b,c)
				{
					selectcboField_AdjustArForm=b.data.Id;

					if (b.data.Id=="AR.ARA_Date"){
						Ext.getCmp('txtKeyWordFind_AdjustArForm').disable();
						Ext.getCmp('dtpTglFind_AdjustArForm').enable();
						criteriaTanggalFind = true;
					} else{
						Ext.getCmp('dtpTglFind_AdjustArForm').disable();
						Ext.getCmp('txtKeyWordFind_AdjustArForm').enable();
						criteriaTanggalFind = false;
					}
				}

			}
			
		}
	);
	return cboFindField_AdjustArForm;
}


function mComboOperator_AdjustArForm()
{
	var cboFindOperator_AdjustArForm = new Ext.form.ComboBox
	(
		{
			id:'cboFindOperator_AdjustArForm',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			width:190,
			emptyText:'Pilih Operator',
			fieldLabel: 'Operator ',
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: 
					[
						[1, "Like"], 
						[2, "="],
						[3, "<"],
						[4, ">"],
						[5, "<="], 
						[6, ">="],
						[7, "<>"]
					]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			listeners:  
			{
				'select': function(a,b,c)
				{
					selectcboOperator_AdjustArForm=b.data.Id;
				}

			}
		}
	);
	return cboFindOperator_AdjustArForm;
}

//////da
var rowSelectedFakturAmount_ApproveAdjust;
var dsTRListFakturAmount_ApproveAdjust;
var fldDetail_FakturAmount2 =['ID','REFF','AMOUNT','AMOUNT_ADD','NOTES','REFF_DATE','PAID','REMAIN']  
	
	dsTRListFakturAmount_ApproveAdjust = new WebApp.DataStore({ fields: fldDetail_FakturAmount2 })

	var mRecord_ApproveAdjust = Ext.data.Record.create
	(
		[
		   {name: 'ID', mapping:'ID'},
		   {name: 'REFF', mapping:'REFF'},
		   {name: 'AMOUNT', mapping:'AMOUNT'},
		   {name: 'AMOUNT_ADD', mapping:'AMOUNT_ADD'},
		   {name: 'NOTES', mapping:'NOTES'},
		   {name: 'REFF_DATE', mapping:'REFF_DATE'},
		   {name: 'PAID', mapping:'PAID'},
		   {name: 'REMAIN', mapping:'REMAIN'}
		]
	);



function getArrDetailFakturAdjust_AdjustArForm(jml)
{
	
	
	var x='';

	for(var i = 0 ; i < dsTRListFakturAmount_ApproveAdjust.getCount();i++)
	{
		var y='';
		var z='@@##$$@@';
		
		y ='ID='+dsTRListFakturAmount_ApproveAdjust.data.items[i].data.ID		
		y += z + 'ARO_NUMBER='+dsTRListFakturAmount_ApproveAdjust.data.items[i].data.REFF
		y += z + 'ARO_DATE=' + dsTRListFakturAmount_ApproveAdjust.data.items[i].data.REFF_DATE
		y += z + 'ARA_NUMBER='+Ext.getCmp('txtNo_AdjustArForm').getValue()//dsTRListFakturAmount_ApproveAdjust.data.items[i].data.ARA_NUMBER
		y += z + 'ARA_DATE='+Ext.get('dtpTanggalAppAdjust').getValue()//dsTRListFakturAmount_ApproveAdjust.data.items[i].data.ARA_DATE
		y += z + 'ARAJI_NUMBER='+ Ext.getCmp('txtNo_AdjustArForm').getValue()//+dsTRListFakturAmount_ApproveAdjust.data.items[i].data.ARAJI_NUMBER
		y += z + 'ARAJI_DATE='+ Ext.get('dtpTanggalAppAdjust').getValue()//+dsTRListFakturAmount_ApproveAdjust.data.items[i].data.ARAJI_DATE
		y += z + 'PAID=' + jml//dsTRListFakturAmount_ApproveAdjust.data.items[i].data.PAID
		y += z + 'TYPE=1'// + dsTRListFakturAmount_ApproveAdjust.data.items[i].data.POSTED
		if (i === (dsTRListFakturAmount_ApproveAdjust.getCount()-1))
		{
			x += y 
		}
		else
		{
			x += y + '##[[]]##'
		}
	}		
	return x;
};

function AutoBalance_AdjustArForm()
{
	var rows = dsDTLTRList_AdjustArForm.getCount();
	var tCR = Ext.num(parseFloat(GetAngkaAdjustArForm(Ext.getCmp('txtTotalCr_AdjustArForm').getValue())));
	var tDB = Ext.num(parseFloat(GetAngkaAdjustArForm(Ext.getCmp('txtTotalDb_AdjustArForm').getValue())));
	var selisih = Math.abs(tCR-tDB);
	var rowCR = 0;
	var rowDB = 0;
	
	if(rows > 0)
	{
		for(var i=0; i<rows; i++)
		{
			rowCR = Ext.num(parseFloat(GetAngkaAdjustArForm(dsDTLTRList_AdjustArForm.data.items[i].data.kredit)));
			rowDB = Ext.num(parseFloat(GetAngkaAdjustArForm(dsDTLTRList_AdjustArForm.data.items[i].data.debit)));
			if(rowCR != 0 && rowDB != 0){}
			else if(rowCR == 0 && rowDB == 0)
			{
				if(tCR < tDB)
				{
					if(selisih > 0)
					{
						rowCR = selisih;
						selisih = 0;
						var p = new mRecord_AdjustArForm
						(
							{
								account: dsDTLTRList_AdjustArForm.data.items[i].data.account,
								name: dsDTLTRList_AdjustArForm.data.items[i].data.name,
								description: dsDTLTRList_AdjustArForm.data.items[i].data.description,
								value: rowCR,//dsDTLTRList_AdjustArForm.data.items[i].data.Account,
								line: '',//dsDTLTRList_AdjustArForm.data.items[i].data.Description,
								kredit:rowCR,
								debit:0
															
							}
						);
						dsDTLTRList_AdjustArForm.removeAt(i);
						dsDTLTRList_AdjustArForm.insert(i, p);
					}
				}
				else
				{
					if(selisih > 0)
					{
						rowDB = selisih;
						selisih = 0;
						var p = new mRecord_AdjustArForm
						(
							{
								account: dsDTLTRList_AdjustArForm.data.items[i].data.account,
								name: dsDTLTRList_AdjustArForm.data.items[i].data.name,
								description: dsDTLTRList_AdjustArForm.data.items[i].data.description,
								value: rowDB,//dsDTLTRList_AdjustArForm.data.items[i].data.Account,
								line: '',//dsDTLTRList_AdjustArForm.data.items[i].data.Description,
								kredit:0,
								debit:rowDB				
							}
						);
						dsDTLTRList_AdjustArForm.removeAt(i);
						dsDTLTRList_AdjustArForm.insert(i, p);
					}
				}
			}
			else
			{
				if(tCR > tDB)
				{
					if(rowCR > selisih && selisih != 0)
					{
						rowCR -= selisih;
						selisih = 0;
						var p = new mRecord_AdjustArForm
						(
							{
								account: dsDTLTRList_AdjustArForm.data.items[i].data.account,
								name: dsDTLTRList_AdjustArForm.data.items[i].data.name,
								description: dsDTLTRList_AdjustArForm.data.items[i].data.description,
								value: rowCR,//dsDTLTRList_AdjustArForm.data.items[i].data.Account,
								line: '',//dsDTLTRList_AdjustArForm.data.items[i].data.Description,
								kredit:rowCR,
								debit:0							
							}
						);
						dsDTLTRList_AdjustArForm.removeAt(i);
						dsDTLTRList_AdjustArForm.insert(i, p);
					}
				}
				else
				{
					if(rowDB > selisih && selisih != 0)
					{
						rowDB -= selisih;
						selisih = 0;						
						var p = new mRecord_AdjustArForm
						(
							{
								account: dsDTLTRList_AdjustArForm.data.items[i].data.account,
								name: dsDTLTRList_AdjustArForm.data.items[i].data.name,
								description: dsDTLTRList_AdjustArForm.data.items[i].data.description,
								value: rowDB,//dsDTLTRList_AdjustArForm.data.items[i].data.Account,
								line: '',//dsDTLTRList_AdjustArForm.data.items[i].data.Description,
								kredit:0,
								debit:rowDB							
							}
						);
						dsDTLTRList_AdjustArForm.removeAt(i);
						dsDTLTRList_AdjustArForm.insert(i, p);
					}
				}
			}
		}
	}
	CalcTotalCr_AdjustArForm();
	CalcTotalDb_AdjustArForm();	
}

function GetAngkaAdjustArForm(str)
{
	//alert(str)
	for (var i = 0; i < str.length; i++) 
	{
		var y = str.substr(i, 1)
		if (y === '.') 
		{
			str = str.replace('.', '');
		}
	};
	
	return str;
};


function LookUpAccount_AREntry(criteria)
{
	
	WindowLookUpAccount_AREntry = new Ext.Window
    (
        {
            id: 'pnlLookUpAccount_AREntry',
            title: 'Lookup Account',
            width:650,
            height: 350,
            border: false,
            resizable:false,
            plain: true,
            iconCls: 'icon_lapor',
            modal: true,
            items: [
				getGridListSearchAccount_AREntry()
			],
			listeners:
			{             
				activate: function()
				{
						
				},
				afterShow: function()
				{
					this.activate();

				},
				deactivate: function()
				{
					
				},
				close: function (){
					
				}
			}
        }
    );

    WindowLookUpAccount_AREntry.show();
	getListSearchAccount_AREntry(criteria);
};

function getGridListSearchAccount_AREntry(){
	var fldDetail = ['account','name','nama_parent'];
	dsGridListAccount_AREntry = new WebApp.DataStore({ fields: fldDetail });
	
    GridListAccountColumnModelAREntry =  new Ext.grid.ColumnModel([
        new Ext.grid.RowNumberer(),
		{
			dataIndex		: 'account',
			header			: 'No. Account',
			width			: 80,
			menuDisabled	: true,
        },
		{
			dataIndex		: 'name',
			header			: 'Nama',
			width			: 140,
			menuDisabled	: true,
        },
		{
			dataIndex		: 'nama_parent',
			header			: 'Parent',
			width			: 120,
			menuDisabled	: true,
        },
	]);
	
	
	GridListAccount_AREntry= new Ext.grid.EditorGridPanel({
		id			: 'GrdListPencarianAccount_AREntry',
		stripeRows	: true,
		width		: 640,
		height		: 270,
        store		: dsGridListAccount_AREntry,
        border		: true,
        frame		: false,
        autoScroll	: true,
        cm			: GridListAccountColumnModelAREntry,
		selModel: new Ext.grid.RowSelectionModel({
			singleSelect: true,
			listeners:
			{
				rowselect: function(sm, row, rec)
				{
					currentRowSelectionSearchAccountAREntry = undefined;
					currentRowSelectionSearchAccountAREntry = dsGridListAccount_AREntry.getAt(row);
				}
			}
		}),
		listeners	: {
			rowclick: function( $this, rowIndex, e )
			{
				
    		},
			celldblclick: function(gridView,htmlElement,columnIndex,dataRecord){
				var line	= gridDTLTR_AdjustArForm.getSelectionModel().selection.cell[0];
				dsDTLTRList_AdjustArForm.getRange()[line].data.cito="Tidak";
				dsDTLTRList_AdjustArForm.getRange()[line].data.account=currentRowSelectionSearchAccountAREntry.data.account;
				dsDTLTRList_AdjustArForm.getRange()[line].data.name=currentRowSelectionSearchAccountAREntry.data.name;
				dsDTLTRList_AdjustArForm.getRange()[line].data.description=Ext.get('txtCatatan_AdjustArForm').getValue();
				dsDTLTRList_AdjustArForm.getRange()[line].data.debit=0;
				dsDTLTRList_AdjustArForm.getRange()[line].data.kredit=0;
				
				gridDTLTR_AdjustArForm.getView().refresh();
				gridDTLTR_AdjustArForm.startEditing(line, 4);	
				
				WindowLookUpAccount_AREntry.close();
			},
			'keydown' : function(e){
				if(e.getKey() == 13){
					var line	= gridDTLTR_AdjustArForm.getSelectionModel().selection.cell[0];
					dsDTLTRList_AdjustArForm.getRange()[line].data.cito="Tidak";
					dsDTLTRList_AdjustArForm.getRange()[line].data.account=currentRowSelectionSearchAccountAREntry.data.account;
					dsDTLTRList_AdjustArForm.getRange()[line].data.name=currentRowSelectionSearchAccountAREntry.data.name;
					dsDTLTRList_AdjustArForm.getRange()[line].data.description=Ext.get('txtCatatan_AdjustArForm').getValue();
					dsDTLTRList_AdjustArForm.getRange()[line].data.debit=0;
					dsDTLTRList_AdjustArForm.getRange()[line].data.kredit=0;
					
					gridDTLTR_AdjustArForm.getView().refresh();
					gridDTLTR_AdjustArForm.startEditing(line, 4);	
					
					WindowLookUpAccount_AREntry.close();
				}
			},
		},
		viewConfig	: {forceFit: true}
    });
	return GridListAccount_AREntry;
}


function getListSearchAccount_AREntry(criteria){
	Ext.Ajax.request ({
		url: baseURL + "index.php/keuangan/functionGeneral/getListAccount",
		params: {
			criteria:criteria
		},
		failure: function(o)
		{
			Ext.Msg.show({
				title: 'Error',
				msg: 'Error menampilkan list account. Hubungi Admin!',
				buttons: Ext.MessageBox.OK,
				fn: function (btn) {
					if (btn == 'ok')
					{
						var line	= gridDTLTR_AdjustAR.getSelectionModel().selection.cell[0];
						gridDTLTR_AdjustAR.startEditing(line, 1);	
					}
				}
			});
		},	
		success: function(o) 
		{   
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				if(cst.totalrecords == 0 ){
					Ext.Msg.show({
						title: 'Information',
						msg: 'Tidak ada account yang sesuai atau kriteria account kurang!',
						buttons: Ext.MessageBox.OK,
						fn: function (btn) {
							if (btn == 'ok')
							{
								var line	= gridDTLTR_AdjustAR.getSelectionModel().selection.cell[0];
								gridDTLTR_AdjustAR.startEditing(line, 4);
								WindowLookUpAccount_AREntry.close();
							}
						}
					});
				} else{
					dsGridListAccount_AREntry.removeAll();
					var recs=[],
						recType=dsGridListAccount_AREntry.recordType;
					for(var i=0; i<cst.ListDataObj.length; i++){
						recs.push(new recType(cst.ListDataObj[i]));						
					}
					dsGridListAccount_AREntry.add(recs);
					GridListAccount_AREntry.getView().refresh();
					GridListAccount_AREntry.getSelectionModel().selectRow(0);
					GridListAccount_AREntry.getView().focusRow(0);
				}
				
			} else {
				Ext.Msg.show({
					title: 'Error',
					msg: 'Error menampilkan list account. Hubungi Admin!',
					buttons: Ext.MessageBox.OK,
					fn: function (btn) {
						if (btn == 'ok')
						{
							var line	= gridDTLTR_AdjustAR.getSelectionModel().selection.cell[0];
							gridDTLTR_AdjustAR.startEditing(line, 1);	
						}
					}
				});
			};
		}
	});
}
