// Data Source ExtJS # --------------

/**
 *	Nama File 		: TRTRReceivablesAdjEntry.js
 *	Menu 			: 
 *	Model id 		: 
 *	Keterangan 		: 
 *	Di buat tanggal : 13 Agustus 2014
 *	Di EDIT tanggal : 14 Agustus 2014
 *	Oleh 			: ADE. S
 *	Editing			: SDY_RI
 */

// Deklarasi Variabel pada Kasir Rawat Jalan # --------------

var dataSource_viTRReceivablesAdjEntry;
var selectCount_viTRReceivablesAdjEntry = 50;
var NamaForm_viTRReceivablesAdjEntry = "Payables Adjusment Entry ";
var mod_name_viTRReceivablesAdjEntry = "viTRReceivablesAdjEntry";
var now_viTRReceivablesAdjEntry = new Date();
var addNew_viTRReceivablesAdjEntry;
var rowSelected_viTRReceivablesAdjEntry;
var setLookUps_viTRReceivablesAdjEntry;
var mNoKunjungan_viTRReceivablesAdjEntry = '';

var CurrentData_viTRReceivablesAdjEntry =
        {
            data: Object,
            details: Array,
            row: 0
        };

CurrentPage.page = dataGrid_viTRReceivablesAdjEntry(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

// End Deklarasi Variabel pada Kasir Rawat Jalan # --------------

// Start Project Kasir Rawat Jalan # --------------

// --------------------------------------- # Start Function # ---------------------------------------
// ## Silahkan tempatkan semua fungsi eksekusi disini

// --------------------------------------- # End Function # ---------------------------------------

// --------------------------------------- # Start Form # ---------------------------------------

/**
 *	Function : dataGrid_viTRReceivablesAdjEntry
 *	
 *	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
 */

function dataGrid_viTRReceivablesAdjEntry(mod_id_viTRReceivablesAdjEntry)
{
    // Field kiriman dari Project Net.
    var FieldMaster_viTRReceivablesAdjEntry =
            [
                'NO_KUNJUNGAN', 'KD_KELOMPOK', 'KD_UNIT', 'KD_DOKTER', 'KD_Customer', 'KD_PASIEN',
                'TGL_KUNJUNGAN', 'JAM_KUNJUNGAN', 'TINGGI_BADAN', 'BERAT_BADAN', 'TEKANAN_DRH',
                'NADI', 'ALERGI', 'KELUHAN', 'RUJUK_RAD', 'RUJUK_LAB', 'TAHAP_PROSES', 'PASIEN_BARU',
                'NAMA_UNIT', 'KELOMPOK', 'DOKTER', 'Customer', 'PS_BARU', 'KD_PENDIDIKAN', 'KD_STS_MARITAL',
                'KD_AGAMA', 'KD_PEKERJAAN', 'NAMA', 'TEMPAT_LAHIR', 'TGL_LAHIR', 'JENIS_KELAMIN', 'ALAMAT',
                'NO_TELP', 'NO_HP', 'GOL_DARAH', 'PENDIDIKAN', 'STS_MARITAL', 'AGAMA', 'PEKERJAAN', 'JNS_KELAMIN',
                'TAHUN', 'BULAN', 'HARI'
            ];
    // Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viTRReceivablesAdjEntry = new WebApp.DataStore
            ({
                fields: FieldMaster_viTRReceivablesAdjEntry
            });

    // Grid Kasir Rawat Jalan # --------------
    var GridDataView_viTRReceivablesAdjEntry = new Ext.grid.EditorGridPanel
            (
                    {
                        xtype: 'editorgrid',
                        title: '',
                        store: dataSource_viTRReceivablesAdjEntry,
                        autoScroll: true,
                        columnLines: true,
                        border: false,
                        anchor: '100% 100%',
                        plugins: [new Ext.ux.grid.FilterRow()],
                        selModel: new Ext.grid.RowSelectionModel
                                // Tanda aktif saat salah satu baris dipilih # --------------
                                        (
                                                {
                                                    singleSelect: true,
                                                    listeners:
                                                            {
                                                                rowselect: function (sm, row, rec)
                                                                {
                                                                    rowSelected_viTRReceivablesAdjEntry = undefined;
                                                                    rowSelected_viTRReceivablesAdjEntry = dataSource_viTRReceivablesAdjEntry.getAt(row);
                                                                    CurrentData_viTRReceivablesAdjEntry
                                                                    CurrentData_viTRReceivablesAdjEntry.row = row;
                                                                    CurrentData_viTRReceivablesAdjEntry.data = rowSelected_viTRReceivablesAdjEntry.data;
                                                                }
                                                            }
                                                }
                                        ),
                                // Proses eksekusi baris yang dipilih # --------------
                                listeners:
                                        {
                                            // Function saat ada event double klik maka akan muncul form view # --------------
                                            rowdblclick: function (sm, ridx, cidx)
                                            {
                                                rowSelected_viTRReceivablesAdjEntry = dataSource_viTRReceivablesAdjEntry.getAt(ridx);
                                                if (rowSelected_viTRReceivablesAdjEntry != undefined)
                                                {
                                                    setLookUp_viTRReceivablesAdjEntry(rowSelected_viTRReceivablesAdjEntry.data);
                                                }
                                                else
                                                {
                                                    setLookUp_viTRReceivablesAdjEntry();
                                                }
                                            }
                                            // End Function # --------------
                                        },
                                /**
                                 *	Mengatur tampilan pada Grid Kasir Rawat Jalan
                                 *	Terdiri dari : Judul, Isi dan Event
                                 *	Isi pada Grid di dapat dari pemangilan dari Net.
                                 */
                                colModel: new Ext.grid.ColumnModel
                                        (
                                                [
                                                    new Ext.grid.RowNumberer(),
                                                    {
                                                        id: 'colNumber_viTRReceivablesAdjEntry',
                                                        header: 'Number',
                                                        dataIndex: '',
                                                        sortable: true,
                                                        width: 35,
                                                        filter:
                                                                {
                                                                    type: 'int'
                                                                }
                                                    },
                                                    //-------------- ## --------------
                                                    {
                                                        id: 'colTglJournal_viTRReceivablesAdjEntry',
                                                        header: 'Tanggal',
                                                        dataIndex: '',
                                                        sortable: true,
                                                        width: 35,
                                                        filter:
                                                                {}
                                                    },
                                                    //-------------- ## --------------
                                                    {
                                                        id: 'colCustFilter_viTRReceivablesAdjEntry',
                                                        header: 'Notes',
                                                        dataIndex: '',
                                                        width: 200,
                                                        sortable: true,
                                                        filter: {}
                                                    },
                                                ]
                                                ),
                                // Tolbar ke Dua # --------------
                                tbar:
                                        {
                                            xtype: 'toolbar',
                                            id: 'toolbar_viTRReceivablesAdjEntry',
                                            items:
                                                    [
                                                        {
                                                            xtype: 'button',
                                                            text: 'Edit Data',
                                                            iconCls: 'Edit_Tr',
                                                            tooltip: 'Edit Data',
                                                            id: 'btnEdit_viTRReceivablesAdjEntry',
                                                            handler: function (sm, row, rec)
                                                            {
                                                                if (rowSelected_viTRReceivablesAdjEntry != undefined)
                                                                {
                                                                    setLookUp_viTRReceivablesAdjEntry(rowSelected_viTRReceivablesAdjEntry.data)
                                                                }
                                                                else
                                                                {
                                                                    setLookUp_viTRReceivablesAdjEntry();
                                                                }
                                                            }
                                                        }
                                                        //-------------- ## --------------
                                                    ]
                                        },
                                // End Tolbar ke Dua # --------------
                                // Button Bar Pagging # --------------
                                // Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
                                bbar: bbar_paging(mod_name_viTRReceivablesAdjEntry, selectCount_viTRReceivablesAdjEntry, dataSource_viTRReceivablesAdjEntry),
                                // End Button Bar Pagging # --------------
                                viewConfig:
                                        {
                                            forceFit: true
                                        }
                            }
                            )

                    // Kriteria filter pada Grid # --------------
                    var FrmFilterGridDataView_viTRReceivablesAdjEntry = new Ext.Panel
                            (
                                    {
                                        title: NamaForm_viTRReceivablesAdjEntry,
                                        iconCls: 'Studi_Lanjut',
                                        id: mod_id_viTRReceivablesAdjEntry,
                                        region: 'center',
                                        layout: 'form',
                                        closable: true,
                                        border: false,
                                        margins: '0 5 5 0',
                                        items: [GridDataView_viTRReceivablesAdjEntry],
                                        tbar:
                                                [
                                                    {
                                                        //-------------- # Untuk mengelompokkan pencarian # --------------
                                                        xtype: 'buttongroup',
                                                        //title: 'Pencarian ' + NamaForm_viTRReceivablesAdjEntry,
                                                        columns: 12,
                                                        defaults: {
                                                            scale: 'small'
                                                        },
                                                        frame: false,
                                                        //-------------- ## --------------
                                                        items:
                                                                [
                                                                    {
                                                                        xtype: 'tbtext',
                                                                        text: 'Number : ',
                                                                        style: {'text-align': 'left'},
                                                                        width: 90,
                                                                        height: 25
                                                                    },
                                                                    //viCombo_JournalCode(125, "ComboFilterJournalCode_viTRReceivablesAdjEntry"),	
                                                                    //-------------- ## --------------
                                                                    {
                                                                        xtype: 'textfield',
                                                                        id: 'TxtNumberFilter_viGeneralLedger',
                                                                        emptyText: 'Number...',
                                                                        width: 150,
                                                                    },
                                                                    {
                                                                        xtype: 'tbspacer',
                                                                        width: 15,
                                                                        height: 25
                                                                    },
                                                                    //-------------- ## --------------
                                                                    {
                                                                        xtype: 'tbtext',
                                                                        text: 'Tanggal : ',
                                                                        style: {'text-align': 'left'},
                                                                        width: 50,
                                                                        height: 25
                                                                    },
                                                                    //-------------- ## --------------						
                                                                    {
                                                                        xtype: 'datefield',
                                                                        id: 'txtfilterTglKunjAwal_viTRReceivablesAdjEntry',
                                                                        value: now_viTRReceivablesAdjEntry,
                                                                        format: 'd/M/Y',
                                                                        width: 120,
                                                                        listeners:
                                                                                {
                                                                                    'specialkey': function ()
                                                                                    {
                                                                                        if (Ext.EventObject.getKey() === 13)
                                                                                        {
                                                                                            datarefresh_viTRReceivablesAdjEntry();
                                                                                        }
                                                                                    }
                                                                                }
                                                                    },
                                                                    {
                                                                        xtype: 'tbtext',
                                                                        text: 's/d ',
                                                                        style: {'text-align': 'center'},
                                                                        width: 50,
                                                                        height: 25
                                                                    },
                                                                    {
                                                                        xtype: 'datefield',
                                                                        id: 'txtfilterTglKunjAkhir_viTRReceivablesAdjEntry',
                                                                        value: now_viTRReceivablesAdjEntry,
                                                                        format: 'd/M/Y',
                                                                        width: 120,
                                                                        listeners:
                                                                                {
                                                                                    'specialkey': function ()
                                                                                    {
                                                                                        if (Ext.EventObject.getKey() === 13)
                                                                                        {
                                                                                            datarefresh_viTRReceivablesAdjEntry();
                                                                                        }
                                                                                    }
                                                                                }
                                                                    },
                                                                    //-------------- ## --------------
                                                                    // { 
                                                                    // xtype: 'tbspacer',
                                                                    // width: 15,
                                                                    // height: 25
                                                                    // },
                                                                    //-------------- ## --------------						
                                                                    {
                                                                        xtype: 'tbspacer',
                                                                        width: 15,
                                                                        height: 25
                                                                    },
                                                                    {
                                                                        xtype: 'button',
                                                                        text: 'Cari',
                                                                        iconCls: 'refresh',
                                                                        tooltip: 'Cari',
                                                                        style: {paddingLeft: '30px'},
                                                                        //rowspan: 3,
                                                                        width: 150,
                                                                        id: 'BtnFilterGridDataView_viTRReceivablesAdjEntry',
                                                                        handler: function ()
                                                                        {
                                                                            DfltFilterBtn_viTRReceivablesAdjEntry = 1;
                                                                            DataRefresh_viTRReceivablesAdjEntry(getCriteriaFilterGridDataView_viTRReceivablesAdjEntry());
                                                                        }
                                                                    },
                                                                    //-------------- ## --------------
                                                                ]
                                                                //-------------- # End items # --------------
                                                                //-------------- # End mengelompokkan pencarian # --------------
                                                    }
                                                ]
                                                //-------------- # End tbar # --------------
                                    }
                            )
                    return FrmFilterGridDataView_viTRReceivablesAdjEntry;
                    //-------------- # End form filter # --------------
                }
// End Function dataGrid_viTRReceivablesAdjEntry # --------------

        /**
         *	Function : setLookUp_viTRReceivablesAdjEntry
         *	
         *	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
         */

        function setLookUp_viTRReceivablesAdjEntry(rowdata)
        {
            var lebar = 760;
            setLookUps_viTRReceivablesAdjEntry = new Ext.Window
                    (
                            {
                                id: 'SetLookUps_viTRReceivablesAdjEntry',
                                name: 'SetLookUps_viTRReceivablesAdjEntry',
                                title: NamaForm_viTRReceivablesAdjEntry,
                                closeAction: 'destroy',
                                width: 775,
                                height: 610,
                                resizable: false,
                                autoScroll: false,
                                border: true,
                                constrainHeader: true,
                                iconCls: 'Studi_Lanjut',
                                modal: true,
                                items: getFormItemEntry_viTRReceivablesAdjEntry(lebar, rowdata), //1
                                listeners:
                                        {
                                            activate: function ()
                                            {

                                            },
                                            afterShow: function ()
                                            {
                                                this.activate();
                                            },
                                            deactivate: function ()
                                            {
                                                rowSelected_viTRReceivablesAdjEntry = undefined;
                                                //datarefresh_viTRReceivablesAdjEntry();
                                                mNoKunjungan_viTRReceivablesAdjEntry = '';
                                            }
                                        }
                            }
                    );

            setLookUps_viTRReceivablesAdjEntry.show();
            if (rowdata == undefined)
            {
                // dataaddnew_viTRReceivablesAdjEntry();
                // Ext.getCmp('btnDelete_viTRReceivablesAdjEntry').disable();	
            }
            else
            {
                // datainit_viTRReceivablesAdjEntry(rowdata);
            }
        }
// End Function setLookUpGridDataView_viTRReceivablesAdjEntry # --------------

        /**
         *	Function : getFormItemEntry_viTRReceivablesAdjEntry
         *	
         *	Sebuah fungsi untuk menampilkan penataan menu dan isian form edit
         */

        function getFormItemEntry_viTRReceivablesAdjEntry(lebar, rowdata)
        {
            var pnlFormDataBasic_viTRReceivablesAdjEntry = new Ext.FormPanel
                    (
                            {
                                title: '',
                                region: 'north',
                                layout: 'form',
                                bodyStyle: 'padding:10px 10px 10px 10px',
                                anchor: '100%',
                                width: lebar,
                                height: 600,
                                border: false,
                                //-------------- #items# --------------
                                items:
                                        [
                                            getItemPanelInputBiodata_viTRReceivablesAdjEntry(lebar),
                                            //-------------- ## -------------- 
                                            //getItemDataKunjungan_viTRReceivablesAdjEntry(lebar), 
                                            //-------------- ## --------------
                                            getItemGridTransaksi_viTRReceivablesAdjEntry(lebar),
                                            //-------------- ## --------------
                                            {
                                                xtype: 'compositefield',
                                                fieldLabel: ' ',
                                                anchor: '100%',
                                                labelSeparator: '',
                                                //width: 199,
                                                style: {'margin-top': '7px'},
                                                items:
                                                        [
                                                            {
                                                                xtype: 'button',
                                                                text: 'Posting',
                                                                width: 70,
                                                                style: {'text-align': 'right', 'margin-left': '10px'},
                                                                hideLabel: true,
                                                                id: 'btnFindRecord_viTRReceivablesAdjEntry',
                                                                name: 'btnFindRecord_viTRReceivablesAdjEntry',
                                                                handler: function ()
                                                                {
                                                                }
                                                            },
                                                            {
                                                                xtype: 'textfield',
                                                                id: 'txtJumlah1EditData_viTRReceivablesAdjEntry',
                                                                name: 'txtJumlah1EditData_viTRReceivablesAdjEntry',
                                                                style: {'text-align': 'right', 'margin-left': '345px'},
                                                                width: 100,
                                                                value: 0,
                                                                readOnly: true,
                                                            },
                                                            {
                                                                xtype: 'textfield',
                                                                id: 'txtJumlah2EditData_viTRReceivablesAdjEntry',
                                                                name: 'txtJumlah2EditData_viTRReceivablesAdjEntry',
                                                                style: {'text-align': 'right', 'margin-left': '350px'},
                                                                width: 100,
                                                                value: 0,
                                                                readOnly: true,
                                                            },
                                                                    //-------------- ## --------------
                                                        ]
                                            },
                                            //-------------- ## --------------
                                        ],
                                //-------------- #End items# --------------
                                fileUpload: true,
                                // Tombol pada tollbar Edit Data Pasien
                                tbar:
                                        {
                                            xtype: 'toolbar',
                                            items:
                                                    [
                                                        {
                                                            xtype: 'button',
                                                            text: 'Add',
                                                            iconCls: 'add',
                                                            id: 'btnAdd_viTRReceivablesAdjEntry',
                                                            handler: function () {
                                                                dataaddnew_viTRReceivablesAdjEntry();
                                                            }
                                                        },
                                                        //-------------- ## --------------
                                                        {
                                                            xtype: 'tbseparator'
                                                        },
                                                        //-------------- ## --------------
                                                        {
                                                            xtype: 'button',
                                                            text: 'Save',
                                                            iconCls: 'save',
                                                            id: 'btnSimpan_viTRReceivablesAdjEntry',
                                                            handler: function ()
                                                            {
                                                                datasave_viTRReceivablesAdjEntry(addNew_viTRReceivablesAdjEntry);
                                                                datarefresh_viTRReceivablesAdjEntry();
                                                            }
                                                        },
                                                        //-------------- ## --------------
                                                        {
                                                            xtype: 'tbseparator'
                                                        },
                                                        //-------------- ## --------------
                                                        {
                                                            xtype: 'button',
                                                            text: 'Save & Close',
                                                            iconCls: 'saveexit',
                                                            id: 'btnSimpanExit_viTRReceivablesAdjEntry',
                                                            handler: function ()
                                                            {
                                                                var x = datasave_viTRReceivablesAdjEntry(addNew_viTRReceivablesAdjEntry);
                                                                datarefresh_viTRReceivablesAdjEntry();
                                                                if (x === undefined)
                                                                {
                                                                    setLookUps_viTRReceivablesAdjEntry.close();
                                                                }
                                                            }
                                                        },
                                                        //-------------- ## --------------
                                                        {
                                                            xtype: 'tbseparator'
                                                        },
                                                        //-------------- ## --------------
                                                        {
                                                            xtype: 'button',
                                                            text: 'Delete',
                                                            iconCls: 'remove',
                                                            id: 'btnDelete_viTRReceivablesAdjEntry',
                                                            handler: function ()
                                                            {
                                                                datadelete_viTRReceivablesAdjEntry();
                                                                datarefresh_viTRReceivablesAdjEntry();

                                                            }
                                                        },
                                                        //-------------- ## --------------
                                                        {
                                                            xtype: 'tbseparator'
                                                        },
                                                        //-------------- ## --------------
                                                        {
                                                            text: 'Lookup ',
                                                            iconCls: 'find',
                                                            menu:
                                                                    [
                                                                        {
                                                                            text: 'Find Record',
                                                                            id: 'btnLookUpRecord_viTRReceivablesAdjEntry',
                                                                            iconCls: 'find',
                                                                            handler: function () {
                                                                                FormSetLookupRecord_viTRReceivablesAdjEntry('1', '2');
                                                                            },
                                                                        },
                                                                                //-------------- ## --------------
                                                                    ],
                                                        },
                                                                //-------------- ## --------------


                                                    ]
                                        }//,items:
                            }
                    )

            return pnlFormDataBasic_viTRReceivablesAdjEntry;
        }
// End Function getFormItemEntry_viTRReceivablesAdjEntry # --------------

        /**
         *	Function : getItemPanelInputBiodata_viTRReceivablesAdjEntry
         *	
         *	Sebuah fungsi untuk menampilkan isian form edit data
         */

        function getItemPanelInputBiodata_viTRReceivablesAdjEntry(lebar)
        {
            var rdType_viTRReceivablesAdjEntry = new Ext.form.RadioGroup
                    ({
                        fieldLabel: '',
                        columns: 2,
                        width: 200, //400,
                        border: false,
                        items:
                                [
                                    {
                                        boxLabel: 'Debit',
                                        width: 50,
                                        id: 'rdDebit_viTRReceivablesAdjEntry',
                                        name: 'rdDebit_viTRReceivablesAdjEntry',
                                        inputValue: '1'
                                    },
                                    //-------------- ## --------------
                                    {
                                        boxLabel: 'Kredit',
                                        width: 50,
                                        id: 'rdKredit_viTRReceivablesAdjEntry',
                                        name: 'rdKredit_viTRReceivablesAdjEntry',
                                        inputValue: '2'
                                    }
                                    //-------------- ## --------------
                                ]
                    });
            //-------------- ## --------------

            var items =
                    {
                        layout: 'Form',
                        anchor: '100%',
                        width: lebar - 80,
                        labelAlign: 'Left',
                        bodyStyle: 'padding:10px 10px 10px 10px',
                        border: true,
                        items:
                                [
                                    {
                                        xtype: 'compositefield',
                                        fieldLabel: 'Number',
                                        anchor: '100%',
                                        //labelSeparator: '',
                                        width: 199,
                                        items:
                                                [
                                                    {
                                                        xtype: 'textfield',
                                                        flex: 1,
                                                        fieldLabel: 'Label',
                                                        width: 100,
                                                        name: 'txtNumber_viTRReceivablesAdjEntry',
                                                        id: 'txtNumber_viTRReceivablesAdjEntry',
                                                        emptyText: 'Number',
                                                        listeners:
                                                                {
                                                                    'specialkey': function ()
                                                                    {
                                                                        if (Ext.EventObject.getKey() === 13)
                                                                        {
                                                                        }
                                                                        ;
                                                                    }
                                                                }
                                                    },
                                                    {
                                                        xtype: 'displayfield',
                                                        width: 300,
                                                        value: ' ',
                                                        fieldLabel: 'Label',
                                                    },
                                                    {
                                                        xtype: 'displayfield',
                                                        width: 65,
                                                        value: 'Trans. Date',
                                                        fieldLabel: 'Label',
                                                    },
                                                    {
                                                        xtype: 'datefield',
                                                        fieldLabel: 'Tanggal',
                                                        name: 'TransDate_viTRReceivablesAdjEntry',
                                                        id: 'TransDate_viTRReceivablesAdjEntry',
                                                        format: 'd/M/Y',
                                                        width: 130,
                                                        value: now_viTRReceivablesAdjEntry,
                                                        //style:{'text-align':'left','margin-left':'210px'},
                                                    },
                                                ]
                                    },
                                    {
                                        xtype: 'compositefield',
                                        fieldLabel: 'Customer',
                                        name: 'compCustomer_viTRReceivablesAdjEntry',
                                        id: 'compCustomer_viTRReceivablesAdjEntry',
                                        items:
                                                [
                                                    viCombo_CustomerAP(405, "ComboCustomerAP_viTRReceivablesAdjEntry"),
                                                    {
                                                        xtype: 'displayfield',
                                                        width: 65,
                                                        value: 'Due. Date',
                                                        fieldLabel: 'Label',
                                                    },
                                                    {
                                                        xtype: 'datefield',
                                                        fieldLabel: 'Tanggal',
                                                        name: 'DueDate_viTRReceivablesAdjEntry',
                                                        id: 'DueDate_viTRReceivablesAdjEntry',
                                                        format: 'd/M/Y',
                                                        width: 130,
                                                        value: now_viTRReceivablesAdjEntry,
                                                        //style:{'text-align':'left','margin-left':'210px'},
                                                    },
                                                ]
                                    },
                                    {
                                        xtype: 'compositefield',
                                        fieldLabel: 'Notes ',
                                        name: 'compNotes_viTRReceivablesAdjEntry',
                                        id: 'compNotes_viTRReceivablesAdjEntry',
                                        items:
                                                [
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: '',
                                                        width: 610,
                                                        name: 'txtNotes_viTRReceivablesAdjEntry',
                                                        id: 'txtNotes_viTRReceivablesAdjEntry',
                                                        emptyText: ' ',
                                                    },
                                                ]
                                    },
                                    {
                                        xtype: 'compositefield',
                                        fieldLabel: 'Periode',
                                        name: 'compPeriode_viTRReceivablesAdjEntry',
                                        id: 'compPeriode_viTRReceivablesAdjEntry',
                                        items:
                                                [
                                                    viCombo_PeriodeRL(110, "ComboPeriodeRL_viGeneralLedger"),
                                                    {
                                                        xtype: 'button',
                                                        text: 'Load',
                                                        width: 70,
                                                        //style:{'margin-left':'190px','margin-top':'7px'},
                                                        hideLabel: true,
                                                        id: 'btnLoadPeriode_viTRReceivablesAdjEntry',
                                                        handler: function ()
                                                        {
                                                        }
                                                    }

                                                ]
                                    },
                                    //-------------- ## --------------	


                                ]
                    };
            return items;
        }
        ;
// End Function getItemPanelInputBiodata_viTRReceivablesAdjEntry # --------------

        /**
         *	Function : getItemDataKunjungan_viTRReceivablesAdjEntry
         *	
         *	Sebuah fungsi untuk menampilkan isian form edit data
         */

        function getItemDataKunjungan_viTRReceivablesAdjEntry(lebar)
        {
            var items =
                    {
                        layout: 'form',
                        anchor: '100%',
                        labelAlign: 'Left',
                        bodyStyle: 'padding:10px 10px 10px 10px',
                        width: lebar - 80,
                        height: 30,
                        items:
                                [
                                    // {
                                    // xtype: 'compositefield',
                                    // fieldLabel: 'Dari',	
                                    // labelAlign: 'Left',
                                    // //labelSeparator: '',
                                    // labelWidth:70,
                                    // defaults: 
                                    // {
                                    // flex: 1
                                    // },
                                    // items: 
                                    // [					
                                    // {
                                    // xtype:'button',
                                    // text:'01',
                                    // tooltip: '01',
                                    // id:'btnTrans_vi01',
                                    // name:'btnTrans_vi01',
                                    // width: 20,
                                    // handler:function()
                                    // {

                                    // }
                                    // },
                                    // {
                                    // xtype:'button',
                                    // text:'02',
                                    // tooltip: '02',
                                    // id:'btnTrans_vi02',
                                    // name:'btnTrans_vi02',
                                    // width: 20,
                                    // handler:function()
                                    // {

                                    // }
                                    // },
                                    // // {
                                    // // xtype:'button',
                                    // // text:'03',
                                    // // tooltip: '03',
                                    // // id:'btnTrans_vi03',
                                    // // name:'btnTrans_vi03',
                                    // // width: 20,
                                    // // handler:function()
                                    // // {

                                    // // }
                                    // // },
                                    // // {
                                    // // xtype:'button',
                                    // // text:'04',
                                    // // tooltip: '04',
                                    // // id:'btnTrans_vi04',
                                    // // name:'btnTrans_vi04',
                                    // // width: 20,
                                    // // handler:function()
                                    // // {

                                    // // }
                                    // // },
                                    // // {
                                    // // xtype:'button',
                                    // // text:'05',
                                    // // tooltip: '05',
                                    // // id:'btnTrans_vi05',
                                    // // name:'btnTrans_vi05',
                                    // // width: 20,
                                    // // handler:function()
                                    // // {

                                    // // }
                                    // // },
                                    // // {
                                    // // xtype:'button',
                                    // // text:'06',
                                    // // tooltip: '06',
                                    // // id:'btnTrans_vi06',
                                    // // name:'btnTrans_vi06',
                                    // // width: 20,
                                    // // handler:function()
                                    // // {

                                    // // }
                                    // // },
                                    // // {
                                    // // xtype:'button',
                                    // // text:'07',
                                    // // tooltip: '07',
                                    // // id:'btnTrans_vi07',
                                    // // name:'btnTrans_vi07',
                                    // // width: 20,
                                    // // handler:function()
                                    // // {

                                    // // }
                                    // // },
                                    // // {
                                    // // xtype:'button',
                                    // // text:'08',
                                    // // tooltip: '08',
                                    // // id:'btnTrans_vi08',
                                    // // name:'btnTrans_vi08',
                                    // // width: 20,
                                    // // handler:function()
                                    // // {

                                    // // }
                                    // // },
                                    // // {
                                    // // xtype:'button',
                                    // // text:'09',
                                    // // tooltip: '09',
                                    // // id:'btnTrans_vi09',
                                    // // name:'btnTrans_vi09',
                                    // // width: 20,
                                    // // handler:function()
                                    // // {

                                    // // }
                                    // // },
                                    // // {
                                    // // xtype:'button',
                                    // // text:'10',
                                    // // tooltip: '10',
                                    // // id:'btnTrans_vi10',
                                    // // name:'btnTrans_vi10',
                                    // // width: 20,
                                    // // handler:function()
                                    // // {

                                    // // }
                                    // // },
                                    // // {
                                    // // xtype:'button',
                                    // // text:'11',
                                    // // tooltip: '11',
                                    // // id:'btnTrans_vi11',
                                    // // name:'btnTrans_vi11',
                                    // // width: 20,
                                    // // handler:function()
                                    // // {

                                    // // }
                                    // // },
                                    // // {
                                    // // xtype:'button',
                                    // // text:'12',
                                    // // tooltip: '12',
                                    // // id:'btnTrans_vi12',
                                    // // name:'btnTrans_vi12',
                                    // // width: 20,
                                    // // handler:function()
                                    // // {

                                    // // }
                                    // // },
                                    // ]
                                    // },
                                    //-------------- ## --------------		

                                ]
                    };
            return items;
        }
        ;
// End Function getItemDataKunjungan_viTRReceivablesAdjEntry # --------------

        /**
         *	Function : getItemGridTransaksi_viTRReceivablesAdjEntry
         *	
         *	Sebuah fungsi untuk menampilkan isian form edit data
         */

        function getItemGridTransaksi_viTRReceivablesAdjEntry(lebar)
        {
            var items =
                    {
                        title: '',
                        layout: 'form',
                        anchor: '100%',
                        labelAlign: 'Left',
                        bodyStyle: 'padding:10px 10px 10px 10px',
                        border: true,
                        width: lebar - 80,
                        height: 380,
                        items:
                                [
                                    {
                                        labelWidth: 105,
                                        layout: 'form',
                                        labelAlign: 'Left',
                                        border: false,
                                        items:
                                                [
                                                    gridDataViewEdit_viTRReceivablesAdjEntry()
                                                ]
                                    }
                                    //-------------- ## --------------
                                ]
                    };
            return items;
        }
        ;
// End Function getItemGridTransaksi_viTRReceivablesAdjEntry # --------------

        /**
         *	Function : gridDataViewEdit_viTRReceivablesAdjEntry
         *	
         *	Sebuah fungsi untuk menampilkan isian form edit data
         */

        function gridDataViewEdit_viTRReceivablesAdjEntry()
        {

            var FieldGrdKasir_viTRReceivablesAdjEntry =
                    [
                    ];

            dsDataGrdJab_viTRReceivablesAdjEntry = new WebApp.DataStore
                    ({
                        fields: FieldGrdKasir_viTRReceivablesAdjEntry
                    });

            var items =
                    {
                        xtype: 'editorgrid',
                        store: dsDataGrdJab_viTRReceivablesAdjEntry,
                        height: 355,
                        selModel: new Ext.grid.RowSelectionModel
                                (
                                        {
                                            singleSelect: true,
                                            listeners:
                                                    {
                                                        rowselect: function (sm, row, rec)
                                                        {
                                                        }
                                                    }
                                        }
                                ),
                        columns:
                                [
                                    {
                                        dataIndex: '',
                                        header: 'Account',
                                        sortable: true,
                                        width: 75
                                    },
                                    //-------------- ## --------------
                                    {
                                        dataIndex: '',
                                        header: 'Name',
                                        sortable: true,
                                        width: 200
                                    },
                                    //-------------- ## --------------			
                                    {
                                        dataIndex: '',
                                        header: 'Description',
                                        sortable: true,
                                        width: 200
                                    },
                                    //-------------- ## --------------
                                    {
                                        dataIndex: '',
                                        header: 'Debit',
                                        sortable: true,
                                        width: 100,
                                        renderer: function (v, params, record)
                                        {

                                        }
                                    },
                                    {
                                        dataIndex: '',
                                        header: 'Credit',
                                        sortable: true,
                                        width: 100,
                                        renderer: function (v, params, record)
                                        {

                                        }
                                    },
                                ]
                    }
            return items;
        }
// End Function gridDataViewEdit_viTRReceivablesAdjEntry # --------------

// ## MENU LOOKUP ## ------------------------------------------------------------------

// ## MENU LOOKUP - LOOKUP PRODUK/ TINDAKAN ## ------------------------------------------------------------------

        /**
         *   Function : FormSetLookupRecord_viTRReceivablesAdjEntry
         *   
         *   Sebuah fungsi untuk menampilkan windows popup Ganti Dokter
         */

        function FormSetLookupRecord_viTRReceivablesAdjEntry(subtotal, NO_KUNJUNGAN)
        {
            vWinFormEntryGantiDokter_viTRReceivablesAdjEntry = new Ext.Window
                    (
                            {
                                id: 'FormGrdLookupRecord_viTRReceivablesAdjEntry',
                                title: 'Find Record',
                                closeAction: 'destroy',
                                closable: true,
                                width: 390,
                                height: 125,
                                border: false,
                                plain: true,
                                resizable: false,
                                constrainHeader: true,
                                layout: 'form',
                                iconCls: 'Edit_Tr',
                                //padding: '8px',
                                modal: true,
                                items:
                                        [
                                            getFormItemEntryLookupfindrecord_viTRReceivablesAdjEntry(subtotal, NO_KUNJUNGAN),
                                        ],
                                listeners:
                                        {
                                            activate: function ()
                                            {
                                            }
                                        }
                            }
                    );
            vWinFormEntryGantiDokter_viTRReceivablesAdjEntry.show();
            mWindowGridLookupGantiDokter = vWinFormEntryGantiDokter_viTRReceivablesAdjEntry;
        }
        ;

// End Function FormSetLookupRecord_viTRReceivablesAdjEntry # --------------

        /**
         *   Function : getFormItemEntryLookupfindrecord_viTRReceivablesAdjEntry
         *   
         *   Sebuah fungsi untuk menampilkan isian form Ganti Dokter
         */
        function getFormItemEntryLookupfindrecord_viTRReceivablesAdjEntry(subtotal, NO_KUNJUNGAN)
        {
            var lebar = 500;
            var pnlFormDataFindRecordWindowPopup_viTRReceivablesAdjEntry = new Ext.FormPanel
                    (
                            {
                                title: '',
                                region: 'center',
                                layout: 'anchor',
                                padding: '8px',
                                bodyStyle: 'padding:10px 0px 10px 10px;',
                                fileUpload: true,
                                //-------------- #items# --------------
                                items:
                                        [
                                            getItemPanelInputFindRecordDataView_viTRReceivablesAdjEntry(lebar),
                                                    //-------------- ## --------------
                                        ],
                                //-------------- #End items# --------------
                            }
                    )
            return pnlFormDataFindRecordWindowPopup_viTRReceivablesAdjEntry;
        }
// End Function getFormItemEntryLookupfindrecord_viTRReceivablesAdjEntry # --------------

        /**
         *   Function : getItemPanelInputFindRecordDataView_viTRReceivablesAdjEntry
         *   
         *   Sebuah fungsi untuk menampilkan isian form Ganti Dokter
         */

        function getItemPanelInputFindRecordDataView_viTRReceivablesAdjEntry(lebar)
        {
            var items =
                    {
                        title: '',
                        layout: 'Form',
                        anchor: '100%',
                        width: lebar,
                        labelAlign: 'Left',
                        bodyStyle: 'padding:1px 1px 1px 1px',
                        border: false,
                        items:
                                [
                                    {
                                        xtype: 'compositefield',
                                        fieldLabel: 'Field Name',
                                        anchor: '100%',
                                        width: 250,
                                        items:
                                                [
                                                    viCombo_ReferenceRL(250, 'CmboFindReference_viTRReceivablesAdjEntry'),
                                                            //-------------- ## --------------  
                                                ]
                                    },
                                    //-------------- ## --------------
                                    {
                                        xtype: 'compositefield',
                                        fieldLabel: 'Operator',
                                        anchor: '100%',
                                        width: 250,
                                        items:
                                                [
                                                    viCombo_OperatorRL(250, 'CmboOperator_viTRReceivablesAdjEntry'),
                                                            //-------------- ## --------------  
                                                ]
                                    },
                                    //-------------- ## --------------
                                    {
                                        xtype: 'compositefield',
                                        fieldLabel: 'Key Word',
                                        anchor: '100%',
                                        width: 250,
                                        items:
                                                [
                                                    {
                                                        xtype: 'textfield',
                                                        id: 'TxtKeyWord_viTRReceivablesAdjEntry',
                                                        emptyText: ' ',
                                                        width: 150,
                                                    },
                                                ]
                                    },
                                    //-------------- ## --------------
                                    // {
                                    // xtype: 'compositefield',
                                    // id: 'findcancelbtn',
                                    // fieldLabel: 'find',
                                    // anchor: '100%',
                                    // //labelSeparator: '',
                                    // width: 199,
                                    // style:{'margin-top':'7px'},					
                                    // items: 
                                    // [
                                    // {
                                    // xtype:'button',
                                    // text:'Ok',
                                    // width:70,
                                    // //style:{'margin-left':'190px','margin-top':'7px'},
                                    // style:{'text-align':'right','margin-left':'60px'},
                                    // hideLabel:true,
                                    // id: 'btnFindRecord_viTRReceivablesAdjEntry',
                                    // name: 'btnFindRecord_viTRReceivablesAdjEntry',
                                    // handler:function()
                                    // {
                                    // }   
                                    // },
                                    // //-------------- ## --------------
                                    // {
                                    // xtype:'button',
                                    // text:'Cancel',
                                    // width:70,
                                    // //style:{'margin-left':'190px','margin-top':'7px'},
                                    // style:{'text-align':'right','margin-left':'90px'},
                                    // hideLabel:true,
                                    // id: 'btnCancelRecord_viTRReceivablesAdjEntry',
                                    // name: 'btnCancelRecord_viTRReceivablesAdjEntry',
                                    // handler:function()
                                    // {
                                    // }   
                                    // },                        
                                    // //-------------- ## --------------
                                    // ]
                                    // },  
                                ]
                    };
            return items;
        }
// End Function getItemPanelInputFindRecordDataView_viTRReceivablesAdjEntry # --------------


        function viCombo_CustomerAP(lebar, Nama_ID)
        {
            var Field_CustomerAP = ['KD_Customer', 'Customer'];
            ds_CustomerAP = new WebApp.DataStore({fields: Field_CustomerAP});

            // viRefresh_Journal();

            var cbo_CustomerAP = new Ext.form.ComboBox
                    (
                            {
                                flex: 1,
                                fieldLabel: 'Customer',
                                valueField: 'KD_Customer',
                                displayField: 'Customer',
                                emptyText: 'Customer...',
                                store: ds_CustomerAP,
                                width: lebar,
                                mode: 'local',
                                typeAhead: true,
                                triggerAction: 'all',
                                name: Nama_ID,
                                lazyRender: true,
                                id: Nama_ID,
                                listeners:
                                        {
                                            'specialkey': function ()
                                            {
                                                if (Ext.EventObject.getKey() === 13)
                                                {

                                                }
                                            }
                                        }
                            }
                    )
            return cbo_CustomerAP;
        }

        function viCombo_OperatorRL(lebar, Nama_ID)
        {
            var Field_OperatorRL = ['OPERATOR'];
            ds_OperatorRL = new WebApp.DataStore({fields: Field_OperatorRL});

            // viRefresh_Journal();

            var cbo_OperatorRL = new Ext.form.ComboBox
                    (
                            {
                                flex: 1,
                                fieldLabel: 'Operator',
                                valueField: 'Operator',
                                displayField: 'Operator',
                                emptyText: 'Operator',
                                store: ds_OperatorRL,
                                width: lebar,
                                mode: 'local',
                                typeAhead: true,
                                triggerAction: 'all',
                                name: Nama_ID,
                                lazyRender: true,
                                id: Nama_ID,
                                listeners:
                                        {
                                            'specialkey': function ()
                                            {
                                                if (Ext.EventObject.getKey() === 13)
                                                {

                                                }
                                            }
                                        }
                            }
                    )
            return cbo_OperatorRL;
        }

        function viCombo_PeriodeRL(lebar, Nama_ID)
        {
            var Field_JournalRL = ['MONTH'];
            ds_JournalRL = new WebApp.DataStore({fields: Field_JournalRL});

            // viRefresh_Journal();

            var cbo_PeriodeBulanRL = new Ext.form.ComboBox
                    (
                            {
                                flex: 1,
                                fieldLabel: 'Journal',
                                valueField: 'MONTH',
                                displayField: 'Month',
                                emptyText: 'Periode Bulan...',
                                store: ds_JournalRL,
                                width: lebar,
                                mode: 'local',
                                typeAhead: true,
                                triggerAction: 'all',
                                name: Nama_ID,
                                lazyRender: true,
                                id: Nama_ID,
                                listeners:
                                        {
                                            'specialkey': function ()
                                            {
                                                if (Ext.EventObject.getKey() === 13)
                                                {

                                                }
                                            }
                                        }
                            }
                    )
            return cbo_PeriodeBulanRL;
        }

        function viCombo_ReferenceRL(lebar, Nama_ID)
        {
            var Field_ReferenceRL = ['REFERENCE'];
            ds_ReferenceRL = new WebApp.DataStore({fields: Field_ReferenceRL});

            var cbo_ReferenceRL = new Ext.form.ComboBox
                    (
                            {
                                flex: 1,
                                fieldLabel: 'Reference',
                                valueField: 'REFERENCE',
                                displayField: 'REFERENCE',
                                emptyText: 'Reference...',
                                store: ds_ReferenceRL,
                                width: lebar,
                                mode: 'local',
                                typeAhead: true,
                                triggerAction: 'all',
                                name: Nama_ID,
                                lazyRender: true,
                                id: Nama_ID,
                                listeners:
                                        {
                                            'specialkey': function ()
                                            {
                                                if (Ext.EventObject.getKey() === 13)
                                                {

                                                }
                                            }
                                        }
                            }
                    )
            return cbo_ReferenceRL;
        }

        function viCombo_PayMode(lebar, Nama_ID)
        {
            var Field_PayMode = [' '];
            ds_PayMode = new WebApp.DataStore({fields: Field_PayMode});

            var cbo_PayMode = new Ext.form.ComboBox
                    (
                            {
                                flex: 1,
                                fieldLabel: '',
                                valueField: '',
                                displayField: '',
                                emptyText: 'Pay mode...',
                                store: ds_PayMode,
                                width: lebar,
                                mode: 'local',
                                typeAhead: true,
                                triggerAction: 'all',
                                name: Nama_ID,
                                lazyRender: true,
                                id: Nama_ID,
                                listeners:
                                        {
                                            'specialkey': function ()
                                            {
                                                if (Ext.EventObject.getKey() === 13)
                                                {

                                                }
                                            }
                                        }
                            }
                    )
            return cbo_PayMode;
        }
        function viCombo_Curr(lebar, Nama_ID)
        {
            var Field_Curr = [' '];
            ds_Curr = new WebApp.DataStore({fields: Field_Curr});

            var cbo_PayModeCurr = new Ext.form.ComboBox
                    (
                            {
                                flex: 1,
                                fieldLabel: '',
                                valueField: '',
                                displayField: '',
                                emptyText: 'Rp...',
                                store: ds_Curr,
                                width: lebar,
                                mode: 'local',
                                typeAhead: true,
                                triggerAction: 'all',
                                name: Nama_ID,
                                lazyRender: true,
                                id: Nama_ID,
                                listeners:
                                        {
                                            'specialkey': function ()
                                            {
                                                if (Ext.EventObject.getKey() === 13)
                                                {

                                                }
                                            }
                                        }
                            }
                    )
            return cbo_PayModeCurr;
        }