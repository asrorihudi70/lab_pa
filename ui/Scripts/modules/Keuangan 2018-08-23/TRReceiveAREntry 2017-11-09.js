
var now = new Date();
now.format('Y-m-d');
var dsSpesialisasiPenerimaan_Piutang;
var ListHasilPenerimaan_Piutang;
var rowSelectedHasilPenerimaan_Piutang;
var dsKelasPenerimaan_Piutang;
var dsKamarPenerimaan_Piutang;
var xz=0;
var dataSource_Penerimaan_Piutang;
var dataSource_detPenerimaan_Piutang;
var dsPerawatPenerimaan_Piutang;
var rowSelected_viPenerimaan_Piutang;
var account_nci_Penerimaan_Piutang;
var creteria_Penerimaan_Piutang="";
var account_payment_Penerimaan_Piutang;
var  grid_detail_Penerimaan_Piutang;
var ds_Penerimaan_Piutang_grid_detail;
var Account_Penerimaan_Piutang_ds=new Ext.data.ArrayStore({id: 0,fields: ['text','account','name'],data: []});
var Pembayaran_Penerimaan_Piutang_ds=new Ext.data.ArrayStore({id: 0,fields: ['text','pay_code','payment'],data: []});

var gridListHasilPenerimaan_Piutang;
CurrentPage.page = getPanelPenerimaan_Piutang(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
function getPanelPenerimaan_Piutang(mod_id) {
    var Field = ['posted', 'csar_date', 'csar_number','account', 'name', 'cust_code', 'pay_code', 'payment',
         'arf_number', 'notes', 'amount','urut'];
    dataSource_Penerimaan_Piutang = new WebApp.DataStore({
        fields: Field
    });

   load_Penerimaan_Piutang("");
   gridListHasilPenerimaan_Piutang = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dataSource_Penerimaan_Piutang,
        anchor: '100% 80%',
        columnLines: false,
        autoScroll: false,
        border: true,
        sort: false,
        autoHeight: false,
        layout: 'fit',
        region: 'center',
        height: 150,
        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
            listeners: {
                rowselect: function (sm, row, rec) {
                    //rowSelected_viPenerimaan_Piutang = dataSource_Penerimaan_Piutang.getAt(row);
                }
            }
        }),
        listeners: {
            rowdblclick: function (sm, ridx, cidx) {
                rowSelected_viPenerimaan_Piutang = dataSource_Penerimaan_Piutang.getAt(ridx);
                console.log(rowSelected_viPenerimaan_Piutang);
                if (rowSelected_viPenerimaan_Piutang !== undefined)
                {
                      
                    HasilPenerimaan_PiutangLookUp(rowSelected_viPenerimaan_Piutang);
                }
                else
                {
                    HasilPenerimaan_PiutangLookUp();
                }
            }
        },
        cm: new Ext.grid.ColumnModel(
                [
                    new Ext.grid.RowNumberer(),
                    {
                        id: 'colCodeViewstatusPenerimaan_Piutang',
                        header: 'Posting',
                        dataIndex: 'posted',
                        sortable: true,
                        width: 90,
                        style: 'text-align: center',
                        renderer: function(value, metaData, record, rowIndex, colIndex, store)
                        {
                             switch (value)
                             { 
                                 case 't':
                                         metaData.css = 'StatusHijau'; //
                                         break;
                                 case 'f':
                                        metaData.css = 'StatusMerah'; // rejected

                                         break;
                             }
                             return '';
                        }
                    },{
                        id: 'colCodeViewHasilPenerimaan_Piutang',
                        header: 'No CSAR',
                        dataIndex: 'csar_number',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 75
                    },
                    {
                        id: 'colaccountViewHasilPenerimaan_Piutang',
                        header: 'Akun',
                        dataIndex: 'account',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colnama_accountViewHasilPenerimaan_Piutang',
                        header: 'Nama Akun',
                        dataIndex: 'name',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 170
                    },
                    {
                        id: 'col_Amaounts_Penerimaan_Piutang',
                        header: 'Jumlah',
                        dataIndex: 'amount',
                        sortable: false,
                        align : 'right',
                        hideable: false,
                        menuDisabled: true,
                        width: 170,
                        renderer : function (v, params, record)
                        {
                        return formatCurrency(record.data.amount);
                        },
                    }
                ]
                ),
        tbar: {
            xtype: 'toolbar',
            items: [
                        {
                            xtype: 'button',
                            text: 'Baru',
                            iconCls: 'AddRow',
                            id: 'btnNew_Penerimaan_Piutang',
                            handler: function ()
                            {
                                HasilPenerimaan_PiutangLookUp();
                            }
                        } ,
                    ]
        },
        viewConfig: {
            forceFit: true
        }
    });
    var FormDepanPenerimaan_Piutang = new Ext.Panel({
        id: mod_id,
        closable: true,
        region: 'center',
        layout: 'form',
        title: 'Penerimaan_Piutang',
        border: false,
        shadhow: true,
        autoScroll: false,
        iconCls: 'Request',
        margins: '0 5 5 0',
        items: [
            getPanelPencarianPenerimaan_Piutang(),
            gridListHasilPenerimaan_Piutang
        ],
        listeners: {
            'afterrender': function () {
                Ext.getCmp('cboStatus_PostingPenerimaan_Piutang').setValue('Semua');
            }
        }
    });
    return FormDepanPenerimaan_Piutang;
}
;
function getPanelPencarianPenerimaan_Piutang() {
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 1190,
                height: 100,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'No CSAR '
                    },
                    {
                        x: 110,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 10,
                        xtype: 'textfield',
                        name: 'TxtCodePenerimaan_Piutang',
                        id: 'TxtCodePenerimaan_Piutang',
                        width: 150,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
                                        	load_Penerimaan_Piutang(Ext.getCmp('TxtCodePenerimaan_Piutang').getValue(),Ext.getCmp('TxtTgl_voucher_Penerimaan_Piutang').getValue(),Ext.getCmp('TxtTgl_voucher_Penerimaan_Piutang2').getValue());
                                        }

                                    }
                                }
                    },
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Tanggal '
                    },
                    {
                        x: 110,
                        y: 40,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 40,
                        xtype: 'datefield',
                        name: 'TxtTgl_voucher_Penerimaan_Piutang',
                        id: 'TxtTgl_voucher_Penerimaan_Piutang',
                        format:'d/M/Y',
                        width: 200,
                        value :now,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
                                        	load_Penerimaan_Piutang(Ext.getCmp('TxtCodePenerimaan_Piutang').getValue(),Ext.getCmp('TxtTgl_voucher_Penerimaan_Piutang').getValue(),Ext.getCmp('TxtTgl_voucher_Penerimaan_Piutang2').getValue());
                                        }
                                    }
                                }
                    },
                    {
                        x: 330,
                        y: 40,
                        xtype: 'datefield',
                        name: 'TxtTgl_voucher_Penerimaan_Piutang2',
                        id: 'TxtTgl_voucher_Penerimaan_Piutang2',
                        format:'d/M/Y',
                        width: 200,
                        value :now,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
                                        	load_Penerimaan_Piutang(Ext.getCmp('TxtCodePenerimaan_Piutang').getValue(),Ext.getCmp('TxtTgl_voucher_Penerimaan_Piutang').getValue(),Ext.getCmp('TxtTgl_voucher_Penerimaan_Piutang2').getValue());
                                        }
                                    }
                                }
                    },
                    {
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Status Posting '
                    },
                    {
                        x: 110,
                        y: 70,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboStatus_PostingPenerimaan_Piutang()
                ]
            }
        ]
    };
    return items;
}
;
function creteria_Penerimaan_Piutang2(){
var msec =new Date();
    msec.parse(" 2012-01-01");
    if (Ext.getCmp('TxtCodePenerimaan_Piutang').getValue()!=="" && Ext.getCmp('TxtTgl_voucher_Penerimaan_Piutang').getValue()==="" )
    {
    creteria_Penerimaan_Piutang="lower(cso_number) like lower('%"+ Ext.getCmp('TxtCodePenerimaan_Piutang').getValue()+"%')";            
    }
    if (Ext.getCmp('TxtCodePenerimaan_Piutang').getValue()==="" && Ext.getCmp('TxtTgl_voucher_Penerimaan_Piutang').getValue()!=="" )
    {
    creteria_Penerimaan_Piutang="cso_date in('"+ msec +"')";
        
    }
    if (Ext.getCmp('TxtCodePenerimaan_Piutang').getValue()!=="" && Ext.getCmp('TxtTgl_voucher_Penerimaan_Piutang').getValue()!=="" )
    {
    creteria_Penerimaan_Piutang="lower(cso_number) like lower('"+ Ext.getCmp('TxtCodePenerimaan_Piutang').getValue()+"%') and cso_date in ('"+ Ext.getCmp('TxtTgl_voucher_Penerimaan_Piutang').getValue()+"')";
        
    }
    if (Ext.getCmp('TxtCodePenerimaan_Piutang').getValue()==="" && Ext.getCmp('TxtTgl_voucher_Penerimaan_Piutang').getValue()==="" )
    {
    creteria_Penerimaan_Piutang="";
    }

    return creteria_Penerimaan_Piutang;
}
var FormLookUpdetailPenerimaan_Piutang;
function HasilPenerimaan_PiutangLookUp(rowdata) {
    FormLookUpdetailPenerimaan_Piutang = new Ext.Window({
        id: 'gridHasilPenerimaan_Piutang',
        title: 'Penerimaan Piutang',
        closeAction: 'destroy',
        width: 800,
        height: 600,
        border: false,
        resizable: false,
        plain: true,
        layout: 'fit',
        constrain: true,
        iconCls: 'Request',
        modal: true,
        items: [
                form_Penerimaan_Piutang(),
                ],
        listeners: {
        }
    });
    FormLookUpdetailPenerimaan_Piutang.show();
    if (rowdata === undefined||rowdata === "") {
        new_data_Penerimaan_Piutang();
    } else {
        datainit_Penerimaan_Piutang(rowdata);
    }

}
;
function form_Penerimaan_Piutang() {
    var Isi_form_Penerimaan_Piutang = new Ext.Panel
            (
                    {
                        id: 'form_Penerimaan_Piutang',
                        closable: true,
                        region: 'center',
                        layout: 'form',
                        itemCls: 'blacklabel',
                        bodyStyle: 'padding: 0px 0px 0px 0px',
                        border: true,
                        shadhow: true,
                        width: 800,
                        height: 200,
                        iconCls: '',
                        items:
                                [
                                    PanelPenerimaan_Piutang(),
                                    getTabPanelReffTagihan()                               
                                ],
                        tbar:
                                {
                                    xtype: 'toolbar',
                                    items:
                                            [
                                                {
                                                    xtype: 'button',
                                                    text: 'Baru',
                                                    iconCls: 'AddRow',
                                                    id: 'btnNew_Penerimaan_Piutang2',
                                                    handler: function ()
                                                    {
                                                        new_data_Penerimaan_Piutang();
                                                    }
                                                },
                                                {
                                                    xtype: 'button',
                                                    text: 'Simpan',
                                                    iconCls: 'save',
                                                    id: 'btnSimpan_Penerimaan_Piutang',
                                                    handler: function ()
                                                    {
                                                    	if(tmptab.title = 'referensi Tagihan')
                                                    	{
                                                    		SavingData_Penerimaan_Piutang();	
                                                    	}else if (tmptab.title = 'Detail Tagihan') {
                                                    		SavingData_Penerimaan_Piutang_detail();
                                                    	}
                                                    	// var tmptab = Ext.getCmp('tabpanellookup').getActiveTab();                                                        
                                                        // console.log(tmptab.title);
                                                    }
                                                },
                                                {
                                                    xtype: 'button',
                                                    text: 'Hapus',
                                                    iconCls: 'remove',
                                                    id: 'btnHapus_Penerimaan_Piutang',
                                                    handler: function ()
                                                    {
                                                        if (Ext.getCmp('TxtVoucherPenerimaan_Piutang').getValue()==="")
                                                        {
                                                            ShowPesanWarningPenerimaan_Piutang('Tidak dapat Hapus data, sebelum disave', 'Perhatian');        
                                                        }else{
                                                                Ext.Msg.show({
                                                                    title: nmHapusBaris,
                                                                    msg: 'Anda yakin akan menghapus data Ini ',
                                                                    buttons: Ext.MessageBox.YESNO,
                                                                    fn: function (btn) {
                                                                        if (btn == 'yes') {
                                                                            DeletePenerimaanPiutang();
                                                                        }
                                                                    },
                                                                    icon: Ext.MessageBox.QUESTION
                                                                });
                                                                
                                                            
                                                        }
                                                
                                                    }
                                                },
                                                {
                                                    xtype: 'button',
                                                    text: 'Posting',
                                                   // iconCls: 'save',
                                                    id: 'btnposting_Penerimaan_Piutang',
                                                    handler: function ()
                                                    {
                                                        if (Ext.getCmp('TxtVoucherPenerimaan_Piutang').getValue()==="")
                                                        {
                                                        	ShowPesanWarningPenerimaan_Piutang('Tidak dapat diposting data sebelumnya disave', 'Perhatian');        
                                                        }else{
                                                       		Ext.Msg.show({
                                                                    title: 'Posting',
                                                                    msg: 'Anda yakin akan menghapus data Ini, Data yang sudah di Posting tidak Bisa Di Rubah Kembali',
                                                                    buttons: Ext.MessageBox.YESNO,
                                                                    fn: function (btn) {
                                                                        if (btn == 'yes') {
                                                                            PostPenerimaanPiutang();
                                                                        }
                                                                    },
                                                                    icon: Ext.MessageBox.QUESTION
                                                                });
                                                        }
                                                
                                                    }
                                                },
                                                 
                                            ]
                                }
                    }
            );
    return Isi_form_Penerimaan_Piutang;
}
;
function Posting_LookUp_Penerimaan_Piutang(rowdata) {
    xz=0;
    FormLook_Posting_Penerimaan_Piutang = new Ext.Window({
        id: 'FormLook_Posting_Penerimaan_Piutang',
        title: 'Posting',
        closeAction: 'destroy',
        width: 400,
        height: 240,
        border: false,
        resizable: false,
        plain: true,
        layout: 'fit',
        constrain: true,
        iconCls: 'Request',
        modal: true,
        items: [
                PanelPenerimaan_Piutang_posting(),
                ],
        listeners: {
        },
        fbar:
            {
                xtype: 'toolbar',
                items:
                        [
                            {
                                xtype: 'button',
                                text: 'Posting',
                                iconCls: '',
                                id: 'btnSimpanpost_Penerimaan_Piutang',
                                handler: function ()
                                {
                                    PostData_Penerimaan_Piutang()
                                }
                            }
                        ]
            }
                    
    });
    FormLook_Posting_Penerimaan_Piutang.show();
    datainit_post_Penerimaan_Piutang(rowdata);
    
};

function getTabPanelReffTagihan(){

    var items ={
        xtype:'tabpanel',
        id:'tabpanellookup',
        activeTab: 0,
        plain: true,
        defferedRender: true,
        defaults:{
            bodyStyle:'padding:0px',
            autoScroll: true
        },
        items:[
             Getgrid_general_Penerimaan_Piutang(),
             Getgrid_detail_Penerimaan_Piutang(),
             Getgrid_histori_Penerimaan_Piutang(),
        ],
        listeners: {
            tabchange: function (homepnl, value, oldValue) {
                tmptab = Ext.getCmp('tabpanellookup').getActiveTab();
                // console.log(tmptab.title);
                // if (tmptab.title = 'Detail Tagihan') {
                //     if (rowSelected_general === undefined) {

                //     }else{                    	
                        
                //         loaddatadetailfaktur_reff(rowSelected_general.data.arf_number);
                //     }
                    
                // };  
            }
        }
        
        
    };
    return items;
}

var tmptab = '';

var grid_general_Penerimaan_Piutang;
var rowSelected_general;
function Getgrid_general_Penerimaan_Piutang(data) {

    var fldgeneral = ['tag','arf_number', 'arf_date', 'cust_code',  'paid', 'amount', 'posted','due_date','sisa','bayar'];
    ds_Penerimaan_Piutang_grid_general = new WebApp.DataStore({fields: fldgeneral});
    grid_general_Penerimaan_Piutang = new Ext.grid.EditorGridPanel({
        title: '',
        id: 'grid_general_Penerimaan_Piutang',
        stripeRows: true,
        store: ds_Penerimaan_Piutang_grid_general,
        border: false,
        frame: false,
        height: 312,
        width: 815,
        anchor: '100%',
        autoScroll: true,
        sm: new Ext.grid.CellSelectionModel({
            singleSelect: true,
            listeners: {
                cellselect: function (sm, row, rec) {
                    rowSelected_general = undefined;
                    rowSelected_general = ds_Penerimaan_Piutang_grid_general.getAt(row);
                    loaddatadetailfaktur_reff(rowSelected_general.data.arf_number);
                    loaddatahistoryfaktur_reff(rowSelected_general.data.arf_number);
                }
            }
        }),
        cm: get_column_general_Penerimaan_Piutang(),
       viewConfig: {forceFit: true},
       bbar:[
                    {
                        xtype: 'tbspacer',
                        width: 580
                    },
                     {
                
                        xtype: 'label',
                        text: 'Total : '
                    },
                   
                    {
                        xtype: 'textfield',
                        name: 'Txttotal_jumlah_peneriman_general',
                        id: 'Txttotal_jumlah_peneriman_general',
                        width: 150,
                        readOnly:true,
                        style: 'text-align: right',
                        value: 0,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {

                                        }

                                    }
                                }
                    }
        
        ],
    });

    var items ={
        title: 'referensi Tagihan',
        layout: 'form',
        tabIndex: 19,
        items:[
            grid_general_Penerimaan_Piutang
        ]    
    }
    return items;
}

function get_column_general_Penerimaan_Piutang() {
  return new Ext.grid.ColumnModel([
        new Ext.grid.RowNumberer(),
        {
            id: 'coltagPilihData',
            header: 'Tag',
            dataIndex: 'tag',
            width:20,
            xtype:'checkcolumn',
            listeners:{
                'checkchange' : function(column, record, checked){
                    alert('checked');
                }
            }
        },
        {
            id: Nci.getId(),
            header: 'No. Tagihan',
            dataIndex: 'arf_number',
            width: 200,
            hidden: false,
            menuDisabled: true
        }, {
            id: Nci.getId(),
            header: 'Total Tagihan',
            dataIndex: 'amount',
            sortable: true,
            width: 100,
            align:'right',
            renderer: function(v, params, record)
            {
                return formatCurrency(record.data.amount);
            }
        }, {
            id: Nci.getId(),
            header: 'Sisa Tagihan',
            dataIndex: 'sisa',
            width: 100,
            menuDisabled: true,
            hidden: false,
            align:'right',
            renderer: function(v, params, record)
            {
                return formatCurrency(record.data.sisa);
            }
        },
        {
            id: Nci.getId(),
            header: 'Nilai (Rp.)',
            dataIndex: 'bayar',
            width: 100,
            menuDisabled: true,
            hidden: false,
            align:'right',
            editor: new Ext.form.NumberField ({
                        allowBlank: false
                    }),
            renderer: function(v, params, record)
            {
                total();
                return formatCurrency(record.data.bayar);
            }
        }
    ]);
}

var rowSelected_detail;
function Getgrid_detail_Penerimaan_Piutang(data) {
    var fldDetail = ['no_transaksi', 'jumlah', 'sisa',  'kd_pasien', 'nama', 'no_sjp','nama_unit','bayar','kd_kasir','arf_number'];
    ds_Penerimaan_Piutang_grid_detail = new WebApp.DataStore({fields: fldDetail});
    grid_detail_Penerimaan_Piutang = new Ext.grid.EditorGridPanel({
        title: '',
        id: 'grid_detail_Penerimaan_Piutang',
        stripeRows: true,
        store: ds_Penerimaan_Piutang_grid_detail,
        border: false,
        frame: false,
        height: 312,
        width: 815,
        anchor: '100%',
        autoScroll: true,
        sm: new Ext.grid.CellSelectionModel({
            singleSelect: true,
            listeners: {
                cellselect: function (sm, row, rec) {
                    rowSelected_detail = undefined;
                    rowSelected_detail = ds_Penerimaan_Piutang_grid_detail.getAt(row);
                }
            }
        }),
        cm: get_column_detail_Penerimaan_Piutang(),
       viewConfig: {forceFit: true},
       bbar:[
                    {
                        xtype: 'tbspacer',
                        width: 580
                    },
                    {
                
                        xtype: 'label',
                        text: 'Total : '
                    },                   
                    {
                        xtype: 'textfield',
                        name: 'Txttotal_jumlah_peneriman_Detail',
                        id: 'Txttotal_jumlah_peneriman_Detail',
                        width: 150,
                        readOnly:true,
                        style: 'text-align: right',
                        value: 0,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {

                                        }

                                    }
                                }
                    }
        
        ],
    });

    var items ={
        title: 'Detail Tagihan',
        layout: 'form',
        tabIndex: 19,
        items:[
            grid_detail_Penerimaan_Piutang
        ]
    };
    return items;
}
function get_column_detail_Penerimaan_Piutang() {
  return new Ext.grid.ColumnModel([
        new Ext.grid.RowNumberer(),
        
        {
            id: Nci.getId(),
            header: 'No. Transaksi',
            dataIndex: 'no_transaksi',
            width: 80,
            hidden: false,
            menuDisabled: true
        }, {
            id: Nci.getId(),
            header: 'No. Medrec',
            dataIndex: 'kd_pasien',
            sortable: true,
            width: 150,
            align: 'center',
        }, {
            id: Nci.getId(),
            header: 'Nama Pasien',
            dataIndex: 'nama',
            width: 150,
            menuDisabled: true,
            hidden: false,
        },{
            id: Nci.getId(),
            header: 'No. SEP',
            dataIndex: 'no_sjp',
            width: 150,
            menuDisabled: true,
            hidden: false,
        },{
            id: Nci.getId(),
            header: 'Nama Unit',
            dataIndex: 'nama_unit',
            width: 150,
            menuDisabled: true,
            hidden: false,
        },
        {
            id: Nci.getId(),
            header: 'Total Tagihan',
            dataIndex: 'jumlah',
            value:1,
            hidden: false,
            menuDisabled: true,
            width: 100,
            align:'right',
            // editor: new Ext.form.NumberField ({
            //             allowBlank: false
            //         }),
            renderer: function(v, params, record)
            {
                return formatCurrency(record.data.jumlah);
            }
        },
        {
            id: Nci.getId(),
            header: 'Sisa Tagihan',
            dataIndex: 'sisa',
            value:1,
            hidden: false,
            menuDisabled: true,
            width: 100,
            align:'right',
            // editor: new Ext.form.NumberField ({
            //             allowBlank: false
            //         }),
            renderer: function(v, params, record)
            {
                return formatCurrency(record.data.sisa);
            }
        },
        {
            id: Nci.getId(),
            header: 'Nilai (Rp)',
            dataIndex: 'bayar',
            value:1,
            hidden: false,
            menuDisabled: true,
            width: 100,
            align:'right',
            editor: new Ext.form.NumberField ({
                        allowBlank: false
                    }),
            renderer: function(v, params, record)
            {

                
                if (rowSelected_detail === undefined) {
                    return formatCurrency(record.data.bayar);
                }else{
                    
                    if (record.data.bayar === rowSelected_detail.data.jumlah) {
                        return formatCurrency(rowSelected_detail.data.bayar);
                    }else if (record.data.bayar > rowSelected_detail.data.jumlah) {
                        record.data.bayar = rowSelected_detail.data.jumlah;
                        rowSelected_detail.data.bayar = record.data.bayar;
                        totalDetail();
                        return formatCurrency(record.data.bayar);
                        
                    }else{
                        rowSelected_detail.data.bayar = record.data.bayar;
                        totalDetail();
                        return formatCurrency(record.data.bayar);
                        
                    }

                }

                
                
            }
        }
    ]);
}
var recordbayardetail = 0;
var grid_histori_Penerimaan_Piutang;
function Getgrid_histori_Penerimaan_Piutang(data) {

    var fldhistori = ['arf_number', 'csar_number', 'date_posted',  'jumlah'];
    ds_Penerimaan_Piutang_grid_histori = new WebApp.DataStore({fields: fldhistori});
    grid_histori_Penerimaan_Piutang = new Ext.grid.EditorGridPanel({
        title: '',
        id: 'grid_histori_Penerimaan_Piutang',
        stripeRows: true,
        store: ds_Penerimaan_Piutang_grid_histori,
        border: false,
        frame: false,
        height: 312,
        width: 815,
        anchor: '100%',
        autoScroll: true,
        sm: new Ext.grid.CellSelectionModel({
            singleSelect: true,
            listeners: {
                cellselect: function (sm, row, rec) {

                }
            }
        }),
        cm: get_column_histori_Penerimaan_Piutang(),
        viewConfig: {forceFit: true},
       bbar:[
                    {
                        xtype: 'tbspacer',
                        width: 580
                    },
                     {
                
                        xtype: 'label',
                        text: 'Total : '
                    },
                   
                    {
                        xtype: 'textfield',
                        name: 'Txttotal_jumlah_peneriman_History',
                        id: 'Txttotal_jumlah_peneriman_History',
                        width: 150,
                        readOnly:true,
                        style: 'text-align: right',
                        value: 0,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {

                                        }

                                    }
                                }
                    }        
        ],
    });

    var items ={
        title: 'histori Pembayaran',
        layout: 'form',
        tabIndex: 19,
        items:[
            grid_histori_Penerimaan_Piutang
        ]
    };
    return items;
}
function get_column_histori_Penerimaan_Piutang() {
  return new Ext.grid.ColumnModel([
        new Ext.grid.RowNumberer(),
        
        {
            id: Nci.getId(),
            header: 'No. Tagihan',
            dataIndex: 'arf_number',
            width: 200,
            hidden: false,
            menuDisabled: true,

        }, 
        {
            id: Nci.getId(),
            header: 'No. CSAR',
            dataIndex: 'csar_number',
            width: 200,
            hidden: false,
            menuDisabled: true
        },
        {
            id: Nci.getId(),
            header: 'Tanggal Bayar',
            dataIndex: 'date_posted',
            sortable: true,
            width: 100
        }, 
        {
            id: Nci.getId(),
            header: 'Jumlah Bayar',
            dataIndex: 'jumlah',
            width: 150,
            menuDisabled: true,
            hidden: false,
            align:'right',
            renderer: function(v, params, record)
            {
                return formatCurrency(record.data.jumlah);
            }
        }
    ]);
}


function parampost_Penerimaan_Piutang()
{
    var params =
            {
            no_voucher  :Ext.getCmp('Txt_post_VoucherPenerimaan_Piutang').getValue(),
            catatan     :Ext.getCmp('Txt_Post_catatan_Penerimaan_Piutang').getValue(),
            };
   return params;
}
function PostData_Penerimaan_Piutang()
{
/*  if (account_payment.getValue()===""){
    ShowPesanWarningPenerimaan_Piutang('Type Bayar Belum diisi', 'Perhatian');  
    }else{ */
        Ext.Ajax.request
        (
            {   
                url: baseURL + "index.php/anggaran/Penerimaan_Piutang/post",
                params: parampost_Penerimaan_Piutang(),
                failure: function (o){
                    load_Penerimaan_Piutang(Ext.getCmp('TxtCodePenerimaan_Piutang').getValue(),Ext.getCmp('TxtTgl_voucher_Penerimaan_Piutang').getValue(),Ext.getCmp('TxtTgl_voucher_Penerimaan_Piutang2').getValue());
                    ShowPesanWarningPenerimaan_Piutang('Proses Simpan Tidak berhasil silahkan hubungi admin', 'Gagal');
                },
                success: function (o){
                    load_Penerimaan_Piutang(Ext.getCmp('TxtCodePenerimaan_Piutang').getValue(),Ext.getCmp('TxtTgl_voucher_Penerimaan_Piutang').getValue(),Ext.getCmp('TxtTgl_voucher_Penerimaan_Piutang2').getValue());
                    var cst = Ext.decode(o.responseText);
                    if (cst.success === true){
                        ShowPesanInfoPenerimaan_Piutang('Proses Simpan Berhasil', 'Save');
                           Ext.getCmp('btnSimpan_Penerimaan_Piutang').setDisabled(true);
                           Ext.getCmp('btnposting_Penerimaan_Piutang').setDisabled(true);
                           Ext.getCmp('btnSimpanpost_Penerimaan_Piutang').setDisabled(true);
                            
                        load_Penerimaan_Piutang_detail(Ext.getCmp('TxtVoucherPenerimaan_Piutang').getValue());                          
                        
                    }
                     else{
                        if (cst.cari === true){
                        loadMask.hide();
                        ShowPesanWarningPenerimaan_Piutang('Login Ulang User Tidak teridentifikasi', 'User');   
                        }else{
                        ShowPesanWarningPenerimaan_Piutang('Proses Simpan Tidak berhasil silahkan hubungi admin', 'Gagal');
                        }
                    };
                }
            }
        );
//  }
};



function PanelPenerimaan_Piutang_posting() {
    var items = new Ext.Panel
            (
                    {
                        title: '',
                        id: 'PanelDataPosting_Penerimaan_Piutang',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        bodyStyle: 'padding:0px 0px 0px 0px',
                        border: true,
                        autoscroll: true,
                        items: [
                            {
                                layout: 'absolute',
                                bodyStyle: 'padding: 5px 5px 5px 5px',
                                border: false,
                                width: 400,
                                height: 240,
                                anchor: '100% 100%',
                                items: [
                                    {
                                        x: 10,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'No CSAR '
                                    },
                                    {
                                        x: 110,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 10,
                                        xtype: 'textfield',
                                        name: 'Txt_post_VoucherPenerimaan_Piutang',
                                        id: 'Txt_post_VoucherPenerimaan_Piutang',
                                        width: 200,
                                        readOnly: false,
                                        disabled:true,
                        
                                    },
                                    {
                                        x: 10,
                                        y: 40,
                                        xtype: 'label',
                                        text: 'Tanggal '
                                    },
                                    {
                                        x: 110,
                                        y: 40,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 40,
                                        xtype: 'datefield',
                                        name: 'Txt_post_tanggal_Penerimaan_Piutang',
                                        id: 'Txt_post_tanggal_Penerimaan_Piutang',
                                        format: 'Y-m-d',
                                        value:now,
                                        disabled:true,
                                        width: 200,
                                    },
                                    {
                                        x: 10,
                                        y: 70,
                                        xtype: 'label',
                                        text: 'Catatan '
                                    },
                                    {
                                        x: 110,
                                        y: 70,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 70,
                                        xtype: 'textarea',
                                        name: 'Txt_Post_catatan_Penerimaan_Piutang',
                                        id: 'Txt_Post_catatan_Penerimaan_Piutang',
                                        width: 200,
                                        height : 50,
                                       // readOnly: false,
                                        //disabled:true,
                                    },
                                    {
                                        x: 10,
                                        y: 130,
                                        xtype: 'label',
                                        text: 'Total '
                                    },
                                    {
                                        x: 110,
                                        y: 130,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 130,
                                        xtype: 'textfield',
                                        name: 'Txt_post_total_Penerimaan_Piutang',
                                        id: 'Txt_post_total_Penerimaan_Piutang',
                                        style: 'text-align: right',
                                        width: 200,
                                        readOnly: false,
                                        disabled:true,
                                    }
                                ]
                            }
                        ]
                    });
    return items;
};

function datainit_post_Penerimaan_Piutang(rowdata)
{//
    Ext.getCmp('Txt_post_VoucherPenerimaan_Piutang').setValue("P"+Ext.getCmp('TxtVoucherPenerimaan_Piutang').getValue());
    Ext.getCmp('Txt_post_total_Penerimaan_Piutang').setValue(Ext.getCmp('Txttotal_jumlah_peneriman').getValue());
}

function PanelPenerimaan_Piutang() {
     account_nci_Penerimaan_Piutang= Nci.form.Combobox.autoComplete({
                x: 120,
                y: 70,
                store   : Account_Penerimaan_Piutang_ds,
                keydown:function(text,e){
                            var nav=navigator.platform.match("Mac");
                            if (e.keyCode == 9 || e.keyCode == 13){
                                 // Ext.getCmp('TxtPopuPenerimaPenerimaan_Piutang').focus();
                            }
                },
                select  : function(a,b,c){
                                
                                Ext.getCmp('TxtPopupNamaPenerimaan_Piutang').setValue(b.data.name);
                },
                insert  : function(o){
                    return {
                        account         :o.account,
                        name            : o.name,
                        text            :  '<table style="font-size: 11px;"><tr><td width="50">'+o.account+'</td><td width="200">'+o.name+'</td></tr></table>'
                    }
                },
                keypress:function(c,e){
                    if (e.keyCode == 13 || e.keyCode == 9){
                        // Ext.getCmp('TxtPopuPenerimaPenerimaan_Piutang').focus();
                    }
                },
                url: baseURL + "index.php/anggaran/Penerimaan_Piutang/accounts",
                valueField: 'account',
                displayField: 'text',
                listWidth: 300,
                width:100,
                enableKeyEvents:true,
                tabIndex: 1
            });
    
    var items = new Ext.Panel
            (
                    {
                        title: '',
                        id: 'PanelDataPenerimaan_Piutang',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        bodyStyle: 'padding:0px 0px 0px 0px',
                        border: true,
                        autoscroll: true,
                        items: [
                            {
                                layout: 'absolute',
                                bodyStyle: 'padding: 5px 5px 5px 5px',
                                border: false,
                                width: 500,
                                height: 200,
                                anchor: '100% 100%',
                                items: [
                                    {
                                        x: 10,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'No CSAR '
                                    },
                                    {
                                        x: 110,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 10,
                                        xtype: 'textfield',
                                        name: 'TxtVoucherPenerimaan_Piutang',
                                        id: 'TxtVoucherPenerimaan_Piutang',
                                        width: 180,
                                        readOnly: false,
                                        disabled:true,
                        
                                    },
                                    {
                                        x: 310,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'Tanggal '
                                    },
                                    {
                                        x: 370,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 380,
                                        y: 10,
                                        xtype: 'datefield',
                                        name: 'Txt_tanggal_Penerimaan_Piutang',
                                        id: 'Txt_tanggal_Penerimaan_Piutang',
                                        format: 'Y-m-d',
                                        value:now,
                                        disabled:true,
                                        width: 100,
                                    },
                                    {
                                        x: 10,
                                        y: 40,
                                        xtype: 'label',
                                        text: 'Diterima Dari '
                                    },
                                    {
                                        x: 110,
                                        y: 40,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    mComboCustomer_Penerimaan_Piutang(),
                                    {
                                        x: 10,
                                        y: 70,
                                        xtype: 'label',
                                        text: 'Akun '
                                    },
                                    {
                                        x: 110,
                                        y: 70,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    account_nci_Penerimaan_Piutang,
                                    {
                                        x: 230,
                                        y: 70,
                                        xtype: 'textfield',
                                        name: 'TxtPopupNamaPenerimaan_Piutang',
                                        id: 'TxtPopupNamaPenerimaan_Piutang',
                                        width: 340,
                                        readOnly: false,
                                        enableKeyEvents: true,
                                        tabIndex: 2,
                                        listeners:{
                                            keypress:function(c,e){
                                                if (e.keyCode == 13){
                                                    // Ext.getCmp('cboKode_Pay_Penerimaan_Piutang').focus();
                                                }
                                            }
                                        }
                                    },
                                    {
                                        x: 10,
                                        y: 100,
                                        xtype: 'label',
                                        text: 'Jenis Pembayaran'
                                    },
                                    {
                                        x: 110,
                                        y: 100,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    mComboKode_Pay_Penerimaan_Piutang(),
                                    // {
                                    //     x: 310,
                                    //     y: 100,
                                    //     xtype: 'label',
                                    //     text: 'referensi'
                                    // },
                                    // {
                                    //     x: 370,
                                    //     y: 100,
                                    //     xtype: 'label',
                                    //     text: ' : '
                                    // },
                                    // mComboFaktur_Reff_Penerimaan_Piutang(),
                                    // {
                                    //     x: 380,
                                    //     y: 100,
                                    //     xtype: 'textfield',
                                    //     name: 'TxtPopureferensiPenerimaan_Piutang',
                                    //     id: 'TxtPopureferensiPenerimaan_Piutang',
                                    //     width: 190,
                                    //     readOnly: false,
                                    //     enableKeyEvents: true,
                                    //     tabIndex: 2,
                                    //     listeners:{
                                    //         keypress:function(c,e){
                                    //             if (e.keyCode == 13){
                                    //                 // Ext.getCmp('cboKode_Pay_Penerimaan_Piutang').focus();
                                    //             }
                                    //         }
                                    //     }
                                    // },
                                    {
                                        x: 10,
                                        y: 130,
                                        xtype: 'label',
                                        text: 'Catatan'
                                    },
                                    {
                                        x: 110,
                                        y: 130,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 130,
                                        xtype: 'textarea',
                                        name: 'TxtPopureCatatanPenerimaan_Piutang',
                                        id: 'TxtPopureCatatanPenerimaan_Piutang',
                                        width: 450,
										height : 60
                                    },
                                ]
                            }
                        ]
                    });
    return items;
}
;
var urut = 'NULL';
function datainit_Penerimaan_Piutang(rowdata)
{
    xxx=new Date(rowdata.data.csar_date);
    Ext.getCmp('TxtVoucherPenerimaan_Piutang').setValue(rowdata.data.csar_number);
    Ext.getCmp('Txt_tanggal_Penerimaan_Piutang').setValue(xxx);
    Ext.getCmp('cboCustomer_Penerimaan_Piutang').setValue(rowdata.data.customer);
    tmpkodepay = rowdata.data.pay_code;
    tmpCustkode = rowdata.data.cust_code;
    account_nci_Penerimaan_Piutang.setValue(rowdata.data.account);
    Ext.getCmp('TxtPopupNamaPenerimaan_Piutang').setValue(rowdata.data.name);
    Ext.getCmp('cboKode_Pay_Penerimaan_Piutang').setValue(rowdata.data.payment);
    // Ext.getCmp('cboFaktur_Reff_Penerimaan_Piutang').setValue(rowdata.data.arf_number);
    Ext.getCmp('TxtPopureCatatanPenerimaan_Piutang').setValue(rowdata.data.notes);
    Get_load_Penerimaan_Piutang_detail(rowdata.data.csar_number);
    Get_load_Penerimaan_Piutang_general(rowdata.data.csar_number);
    urut = rowdata.data.urut;
    if(rowSelected_viPenerimaan_Piutang.data.posted==='true' || rowSelected_viPenerimaan_Piutang.data.posted==='t')
    {
    	Ext.getCmp('btnSimpan_Penerimaan_Piutang').setDisabled(true);
    	Ext.getCmp('btnHapus_Penerimaan_Piutang').setDisabled(true);
    	Ext.getCmp('btnposting_Penerimaan_Piutang').setDisabled(true);
    }else{
        Ext.getCmp('btnSimpan_Penerimaan_Piutang').setDisabled(false);
    	Ext.getCmp('btnHapus_Penerimaan_Piutang').setDisabled(false);
    	Ext.getCmp('btnposting_Penerimaan_Piutang').setDisabled(false);
    }
    
}

function new_data_Penerimaan_Piutang()
{   
    Ext.getCmp('TxtVoucherPenerimaan_Piutang').setValue();
    Ext.getCmp('Txt_tanggal_Penerimaan_Piutang').setValue(now);
    Ext.getCmp('cboCustomer_Penerimaan_Piutang').setValue();
    tmpkodepay = '';
    tmpCustkode = '';
    account_nci_Penerimaan_Piutang.setValue();
    Ext.getCmp('TxtPopupNamaPenerimaan_Piutang').setValue();
    Ext.getCmp('cboKode_Pay_Penerimaan_Piutang').setValue();
    // Ext.getCmp('cboFaktur_Reff_Penerimaan_Piutang').setValue();
    Ext.getCmp('TxtPopureCatatanPenerimaan_Piutang').setValue();
    ds_Penerimaan_Piutang_grid_detail.removeAll();
    // grid_detail_Penerimaan_Piutang.getView().refresh();
    Ext.getCmp('Txttotal_jumlah_peneriman_general').setValue(0);
    urut = 'NULL';
    Ext.getCmp('btnSimpan_Penerimaan_Piutang').setDisabled(false);
	Ext.getCmp('btnHapus_Penerimaan_Piutang').setDisabled(false);
	Ext.getCmp('btnposting_Penerimaan_Piutang').setDisabled(false);
    
}


function SavingData_Penerimaan_Piutang()
{ 
    if (Ext.getCmp('cboCustomer_Penerimaan_Piutang').getValue()===""){
    	ShowPesanWarningPenerimaan_Piutang('Pengirim Belum diisi', 'Perhatian');    
    }
    else{
	    if (account_nci_Penerimaan_Piutang.getValue()===""){
	         ShowPesanWarningPenerimaan_Piutang('Akun Belum diisi', 'Perhatian');
	    }else{
	        if (Ext.getCmp('cboKode_Pay_Penerimaan_Piutang').getValue()===""){
	        ShowPesanWarningPenerimaan_Piutang('Type Bayar Belum diisi', 'Perhatian');  
	        }else{
	            Ext.Ajax.request
	            (
	                {   
	                    url: baseURL + "index.php/anggaran/Penerimaan_Piutang/save",
	                    params: paramsaving_permintaan(),
	                    failure: function (o)
	                    {
	                        // load_Penerimaan_Piutang(Ext.getCmp('TxtCodePenerimaan_Piutang').getValue(),Ext.getCmp('TxtTgl_voucher_Penerimaan_Piutang').getValue(),Ext.getCmp('TxtTgl_voucher_Penerimaan_Piutang2').getValue());
	                        ShowPesanWarningPenerimaan_Piutang('Proses Simpan Tidak berhasil silahkan hubungi admin', 'Gagal');
	                    },
	                    success: function (o)
	                    {
	                        //creteria_Penerimaan_Piutang2();
	                        // load_Penerimaan_Piutang(Ext.getCmp('TxtCodePenerimaan_Piutang').getValue(),Ext.getCmp('TxtTgl_voucher_Penerimaan_Piutang').getValue(),Ext.getCmp('TxtTgl_voucher_Penerimaan_Piutang2').getValue());
	                    
	                        var cst = Ext.decode(o.responseText);
	                        if (cst.success === true)
	                        {
	                            Ext.getCmp('TxtVoucherPenerimaan_Piutang').setValue(cst.csar_number);
	                            // Get_load_Penerimaan_Piutang_detail(Ext.getCmp('cboFaktur_Reff_Penerimaan_Piutang').getValue(),cst.csar_number);                         
	                            ShowPesanInfoPenerimaan_Piutang('Proses Simpan Berhasil', 'Save');
	                            load_Penerimaan_Piutang("");
	                        }
	                         
	                        else
	                        {
	                            if (cst.cari === true){
	                            loadMask.hide();
	                            ShowPesanWarningPenerimaan_Piutang('Login Ulang User Tidak teridentifikasi', 'User');   
	                            }else{
	                            ShowPesanWarningPenerimaan_Piutang('Proses Simpan Tidak berhasil silahkan hubungi admin', 'Gagal');
	                            }
	                        };
	                    }
	                }
	            );
	            
        	}        
    }

    }
    
};
function paramsaving_permintaan()
{
    var tmpceklis = '';
    var params =
            {
            csar_number :Ext.getCmp('TxtVoucherPenerimaan_Piutang').getValue(),
            csar_date   :Ext.getCmp('Txt_tanggal_Penerimaan_Piutang').getValue(),
            cust_code   :tmpCustkode,
            account     :account_nci_Penerimaan_Piutang.getValue(),
            pay_code    :tmpkodepay,
            notes       :Ext.getCmp('TxtPopureCatatanPenerimaan_Piutang').getValue(),
            amount      :xz,
            urut		: urut
            };
            params['jumlah']=ds_Penerimaan_Piutang_grid_general.getCount();
               for (var L=0; L<ds_Penerimaan_Piutang_grid_general.getCount(); L++)
                {
                    if (ds_Penerimaan_Piutang_grid_general.data.items[L].data.tag != '') {
                        tmpceklis = 'true';
                    }else{
                        tmpceklis = 'false';
                    }
                    
                            params['arf_number'+L]      = ds_Penerimaan_Piutang_grid_general.data.items[L].data.arf_number; 
                            params['arf_date'+L]        = ds_Penerimaan_Piutang_grid_general.data.items[L].data.arf_date;
                            params['bayar'+L]           = ds_Penerimaan_Piutang_grid_general.data.items[L].data.bayar;
                            params['tag'+L]             = tmpceklis;
                    
                }   
   return params;    
}


function SavingData_Penerimaan_Piutang_detail()
{ 
    if (Ext.getCmp('cboCustomer_Penerimaan_Piutang').getValue()===""){
    	ShowPesanWarningPenerimaan_Piutang('Pengirim Belum diisi', 'Perhatian');    
    }
    else{
	    if (account_nci_Penerimaan_Piutang.getValue()===""){
	         ShowPesanWarningPenerimaan_Piutang('Akun Belum diisi', 'Perhatian');
	    }else{
	        if (Ext.getCmp('cboKode_Pay_Penerimaan_Piutang').getValue()===""){
	        ShowPesanWarningPenerimaan_Piutang('Type Bayar Belum diisi', 'Perhatian');  
	        }else{
	            Ext.Ajax.request
	            (
	                {   
	                    url: baseURL + "index.php/anggaran/Penerimaan_Piutang/savedetail",
	                    params: paramsaving_permintaan_Piutang_detail(),
	                    failure: function (o)
	                    {
	                        // load_Penerimaan_Piutang(Ext.getCmp('TxtCodePenerimaan_Piutang').getValue(),Ext.getCmp('TxtTgl_voucher_Penerimaan_Piutang').getValue(),Ext.getCmp('TxtTgl_voucher_Penerimaan_Piutang2').getValue());
	                        ShowPesanWarningPenerimaan_Piutang('Proses Simpan Tidak berhasil silahkan hubungi admin', 'Gagal');
	                    },
	                    success: function (o)
	                    {
	                        //creteria_Penerimaan_Piutang2();
	                        // load_Penerimaan_Piutang(Ext.getCmp('TxtCodePenerimaan_Piutang').getValue(),Ext.getCmp('TxtTgl_voucher_Penerimaan_Piutang').getValue(),Ext.getCmp('TxtTgl_voucher_Penerimaan_Piutang2').getValue());
	                    
	                        var cst = Ext.decode(o.responseText);
	                        if (cst.success === true)
	                        {
	                            Ext.getCmp('TxtVoucherPenerimaan_Piutang').setValue(cst.csar_number);
	                            // Get_load_Penerimaan_Piutang_detail(Ext.getCmp('cboFaktur_Reff_Penerimaan_Piutang').getValue(),cst.csar_number);                         
	                            ShowPesanInfoPenerimaan_Piutang('Proses Simpan Berhasil', 'Save');
	                            load_Penerimaan_Piutang("");
	                        }
	                         
	                        else
	                        {
	                            if (cst.cari === true){
	                            loadMask.hide();
	                            ShowPesanWarningPenerimaan_Piutang('Login Ulang User Tidak teridentifikasi', 'User');   
	                            }else{
	                            ShowPesanWarningPenerimaan_Piutang('Proses Simpan Tidak berhasil silahkan hubungi admin', 'Gagal');
	                            }
	                        };
	                    }
	                }
	            );
	            
        	}        
    }

    }
    
};
function paramsaving_permintaan_Piutang_detail()
{
	var tmpjumlahrecord = 0;
    var params =
            {
            csar_number :Ext.getCmp('TxtVoucherPenerimaan_Piutang').getValue(),
            csar_date   :Ext.getCmp('Txt_tanggal_Penerimaan_Piutang').getValue(),
            cust_code   :tmpCustkode,
            account     :account_nci_Penerimaan_Piutang.getValue(),
            pay_code    :tmpkodepay,
            notes       :Ext.getCmp('TxtPopureCatatanPenerimaan_Piutang').getValue(),
            amount      :xz,
            urut		: urut
            };
            // var fldDetail = ['no_transaksi', 'jumlah', 'sisa',  'kd_pasien', 'nama', 'no_sjp','nama_unit','bayar'];
            for (var L=0; L<ds_Penerimaan_Piutang_grid_detail.getCount(); L++)
            {
            	
            		params['arf_number'+L]		  = rowSelected_general.data.arf_number; 
                	params['no_transaksi'+L]      = ds_Penerimaan_Piutang_grid_detail.data.items[L].data.no_transaksi;
                	params['kd_kasir'+L]          = ds_Penerimaan_Piutang_grid_detail.data.items[L].data.kd_kasir;
                	params['bayar'+L]             = ds_Penerimaan_Piutang_grid_detail.data.items[L].data.bayar;
                	tmpjumlahrecord ++;
            	
            }
            params['jumlah']=tmpjumlahrecord;
   
   return params;    
}

function load_Penerimaan_Piutang(cso_number,tgl,tgl2){
        Ext.Ajax.request({
            url: baseURL + "index.php/anggaran/Penerimaan_Piutang/select",
            params: {
                csar_number:cso_number,
                tgl:tgl,
                tgl2:tgl2,
                posting:selectCount_PostingPenerimaan_Piutang
            },
            failure: function(o){   
                loadMask.hide();
                ShowPesanWarningPenerimaan_Piutang('Hubungi Admin', 'Error');
            },  
            success: function(o){
            var cst = Ext.decode(o.responseText);
            if (cst.success === true){
                dataSource_Penerimaan_Piutang.removeAll();
                var recs=[],
                recType=dataSource_Penerimaan_Piutang.recordType;
                for(var i=0; i<cst.ListDataObj.length; i++){
                    recs.push(new recType(cst.ListDataObj[i]));
                }
                dataSource_Penerimaan_Piutang.add(recs);
                gridListHasilPenerimaan_Piutang.getView().refresh();
            } 
            }
        })
}



function ShowPesanWarningPenerimaan_Piutang(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING,
                        width: 250
                    }
            );
}
;

function ShowPesanInfoPenerimaan_Piutang(str, modul) {
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO,
                        width: 250
                    }
            );
}
;

function DeleteData_Penerimaan_Piutang(line)
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/anggaran/Penerimaan_Piutang/delete",
                        params: paramdelete(line),
                        failure: function (o){
                            //creteria_Penerimaan_Piutang2();
                            load_Penerimaan_Piutang(Ext.getCmp('TxtCodePenerimaan_Piutang').getValue(),Ext.getCmp('TxtTgl_voucher_Penerimaan_Piutang').getValue(),Ext.getCmp('TxtTgl_voucher_Penerimaan_Piutang2').getValue());
                            loadMask.hide();
                            ShowPesanWarningPenerimaan_Piutang('Hubungi Admin', 'Error');
                        },
                        success: function (o)
                        {   //creteria_Penerimaan_Piutang2();
                            load_Penerimaan_Piutang(Ext.getCmp('TxtCodePenerimaan_Piutang').getValue(),Ext.getCmp('TxtTgl_voucher_Penerimaan_Piutang').getValue(),Ext.getCmp('TxtTgl_voucher_Penerimaan_Piutang2').getValue());
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true){
                                
                                loadMask.hide();
                                ShowPesanInfoPenerimaan_Piutang('Data berhasil di hapus', 'Information');
                                 ds_Penerimaan_Piutang_grid_detail.removeAt(line);
                                 // grid_detail_Penerimaan_Piutang.getView().refresh(); 
                            }
                            else{
                                 if (cst.tidak_ada_line === true){
                                
                                 ds_Penerimaan_Piutang_grid_detail.removeAt(line);
                                 // grid_detail_Penerimaan_Piutang.getView().refresh(); 
                                }else{
                                loadMask.hide();
                                ShowPesanWarningPenerimaan_Piutang('Gagal menghapus data', 'Error');
                                }
                            }
                            ;
                        }
                    }

            );
}
;
function total(){
    xz=0;
    // console.log(ds_Penerimaan_Piutang_grid_general);
    for (var L=0; L<ds_Penerimaan_Piutang_grid_general.getCount(); L++)
            {

                // console.log(ds_Penerimaan_Piutang_grid_general.data.items[L].data.tag);
                if (ds_Penerimaan_Piutang_grid_general.data.items[L].data.tag === 'true' || ds_Penerimaan_Piutang_grid_general.data.items[L].data.tag === true) 
                {

                    if (ds_Penerimaan_Piutang_grid_general.data.items[L].data.bayar==="" ||
                     ds_Penerimaan_Piutang_grid_general.data.items[L].data.bayar==undefined||
                     ds_Penerimaan_Piutang_grid_general.data.items[L].data.bayar==="NaN")
                    {
                        ds_Penerimaan_Piutang_grid_general.data.items[L].data.bayar=0;
                        xz=parseInt(xz)+parseInt(ds_Penerimaan_Piutang_grid_general.data.items[L].data.bayar);
                    }else{

                        xz=parseInt(xz)+parseInt(ds_Penerimaan_Piutang_grid_general.data.items[L].data.bayar);
                    }
                }else{
                    // xz = 0;
                }
                
            }
            Ext.getCmp('Txttotal_jumlah_peneriman_general').setValue(formatCurrency(xz));
}

function totalDetail(){
    xz=0;
    for (var L=0; L<ds_Penerimaan_Piutang_grid_detail.getCount(); L++)
            {
                 if (ds_Penerimaan_Piutang_grid_detail.data.items[L].data.bayar==="" ||
                     ds_Penerimaan_Piutang_grid_detail.data.items[L].data.bayar==undefined||
                     ds_Penerimaan_Piutang_grid_detail.data.items[L].data.bayar==="NaN")
                    {
                        ds_Penerimaan_Piutang_grid_detail.data.items[L].data.bayar=0;
                        xz=parseInt(xz)+parseInt(ds_Penerimaan_Piutang_grid_detail.data.items[L].data.bayar);
                    }else{

                        xz=parseInt(xz)+parseInt(ds_Penerimaan_Piutang_grid_detail.data.items[L].data.bayar);
                    }
                
            }
            Ext.getCmp('Txttotal_jumlah_peneriman_Detail').setValue(formatCurrency(xz));
}

function load_Penerimaan_Piutang_detail(criteria){
            Ext.Ajax.request({
            url: baseURL + "index.php/anggaran/Penerimaan_Piutang/getdatadetailfaktur",
            params: {criteria:criteria},
            failure: function(o){   
                loadMask.hide();
                ShowPesanWarningPenerimaan_Piutang('Hubungi Admin', 'Error');
            },  
            success: function(o){
            var cst = Ext.decode(o.responseText);
            if (cst.success === true){
                ds_Penerimaan_Piutang_grid_detail.removeAll();
                var recs=[],
                recType=ds_Penerimaan_Piutang_grid_detail.recordType;
                for(var i=0; i<cst.ListDataObj.length; i++){
                    recs.push(new recType(cst.ListDataObj[i]));
                }
                ds_Penerimaan_Piutang_grid_detail.add(recs);
                // grid_detail_Penerimaan_Piutang.getView().refresh();
            } 
            }
        })
}

function Get_load_Penerimaan_Piutang_detail(csar){
            Ext.Ajax.request({
            url: baseURL + "index.php/anggaran/Penerimaan_Piutang/loaddatadetailcsar",
            params: {csar:csar},
            failure: function(o){   
                loadMask.hide();
                ShowPesanWarningPenerimaan_Piutang('Hubungi Admin', 'Error');
            },  
            success: function(o){
            var cst = Ext.decode(o.responseText);
            if (cst.success === true){
                ds_Penerimaan_Piutang_grid_detail.removeAll();
                var recs=[],
                recType=ds_Penerimaan_Piutang_grid_detail.recordType;
                for(var i=0; i<cst.ListDataObj.length; i++){
                    recs.push(new recType(cst.ListDataObj[i]));
                }
                ds_Penerimaan_Piutang_grid_detail.add(recs);
                // grid_detail_Penerimaan_Piutang.getView().refresh();
            } 
            }
        })
}

function Get_load_Penerimaan_Piutang_general(csar){
    Ext.Ajax.request({
            url: baseURL + "index.php/anggaran/Penerimaan_Piutang/loaddatageneralcsar",
            params: {
                csar:csar
            },
            failure: function(o){   
                loadMask.hide();
                ShowPesanWarningPenerimaan_Piutang('Hubungi Admin', 'Error');
            },  
            success: function(o){
            var cst = Ext.decode(o.responseText);
            if (cst.success === true){
                ds_Penerimaan_Piutang_grid_general.removeAll();
                var recs=[],
                recType=ds_Penerimaan_Piutang_grid_general.recordType;
                for(var i=0; i<cst.ListDataObj.length; i++){
                    recs.push(new recType(cst.ListDataObj[i]));
                }
                ds_Penerimaan_Piutang_grid_general.add(recs);
                grid_general_Penerimaan_Piutang.getView().refresh();
            } 
            }
        });
}

var selectCount_PostingPenerimaan_Piutang = 'Semua';
function mComboStatus_PostingPenerimaan_Piutang()
{
    var cboStatus_PostingPenerimaan_Piutang = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 70,
                        id: 'cboStatus_PostingPenerimaan_Piutang',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        width: 150,
                        emptyText: '',
                        fieldLabel: 'Jenis',
                        store: new Ext.data.ArrayStore
                                (
                                        {
                                            id: 0,
                                            fields:
                                                    [
                                                        'Id',
                                                        'displayText'
                                                    ],
                                            data: [[1, 'Semua'], [2, 'Posting'], [3, 'Belum Posting']]
                                        }
                                ),
                        valueField: 'Id',
                        displayField: 'displayText',
                        value: selectCount_PostingPenerimaan_Piutang,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectCount_PostingPenerimaan_Piutang = b.data.displayText;
                                        load_Penerimaan_Piutang(Ext.getCmp('TxtCodePenerimaan_Piutang').getValue(),Ext.getCmp('TxtTgl_voucher_Penerimaan_Piutang').getValue(),Ext.getCmp('TxtTgl_voucher_Penerimaan_Piutang2').getValue());
                                    }
                                }
                    }
            );
    return cboStatus_PostingPenerimaan_Piutang;
}
;
var tmpkodepay;
function mComboKode_Pay_Penerimaan_Piutang()
{
    var Field = ['KODE', 'NAMA'];

    dsKode_Pay_Penerimaan_Piutang = new WebApp.DataStore({fields: Field});
    dsKode_Pay_Penerimaan_Piutang.load
        (
            {
            params:
                    {
                        Skip: 0,
                        Take: 1000,
                        Sort: 'pay_code',
                        Sortdir: 'ASC',
                        target: 'Pay_ACC',
                        param: "" //+"~ )"
                    }
            }
        );

    var cboKode_Pay_Penerimaan_Piutang = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 100,
                        id: 'cboKode_Pay_Penerimaan_Piutang',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        emptyText: '',
                        align: 'Right',
                        editable: false,
                        width: 180,
                        // listWidth: 40,
                        store: dsKode_Pay_Penerimaan_Piutang,
                        valueField: 'KODE',
                        displayField: 'NAMA',
                        selectOnFocus: true,
                        enableKeyEvents:true,
                        tabIndex: 3,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        tmpkodepay = b.data.KODE;
                                        // Ext.getCmp('TxtPopunama_type_Penerimaan_Piutang').setValue(b.data.NAMA);
                                        // Ext.getCmp('TxtPopukode_xpayPenerimaan_Piutang').focus();
                                    },
                                    keypress:function(c,e){
                                        if (e.keyCode == 13){
                                            // Ext.getCmp('TxtPopukode_xpayPenerimaan_Piutang').focus();
                                        } else if (e.keyCode == 9){
                                             // Ext.getCmp('TxtPopukode_xpayPenerimaan_Piutang').focus();
                                        }
                                    },

                                }
                    }
            );

    return cboKode_Pay_Penerimaan_Piutang;
}
;

var tmpCustkode;
function mComboCustomer_Penerimaan_Piutang()
{
    var Field = ['KODE', 'NAMA'];

    dsCustomer_Penerimaan_Piutang = new WebApp.DataStore({fields: Field});
    dsCustomer_Penerimaan_Piutang.load
        (
            {
            params:
                    {
                        Skip: 0,
                        Take: 1000,
                        Sort: 'pay_code',
                        Sortdir: 'ASC',
                        target: 'ViewComboCustomer_Keuangan',
                        param: ""
                    }
            }
        );

    var cboCustomer_Penerimaan_Piutang = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 40,
                        id: 'cboCustomer_Penerimaan_Piutang',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        emptyText: '',
                        align: 'Right',
                        editable: false,
                        width: 280,
                        store: dsCustomer_Penerimaan_Piutang,
                        valueField: 'KODE',
                        displayField: 'NAMA',
                        selectOnFocus: true,
                        enableKeyEvents:true,
                        tabIndex: 3,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        tmpCustkode = b.data.KODE;
                                        // dsFaktur_Reff_Penerimaan_Piutang.removeAll();
                                        // Ext.getCmp('cboFaktur_Reff_Penerimaan_Piutang').setValue('');
                                        loaddatafaktur_reff(tmpCustkode);

                                    },
                                    keypress:function(c,e){
                                        if (e.keyCode == 13){
                                        
                                        } else if (e.keyCode == 9){
                                            
                                        }
                                    },

                                }
                    }
            );

    return cboCustomer_Penerimaan_Piutang;
}
;

function DeletePenerimaanPiutang()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/anggaran/Penerimaan_Piutang/delete",
                        params: paramdelete(),
                        failure: function (o){
                            loadMask.hide();
                            ShowPesanWarningPenerimaan_Piutang('Hubungi Admin', 'Error');
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true){
                                
                                loadMask.hide();
                                ShowPesanInfoPenerimaan_Piutang('Data berhasil di hapus', 'Information');
                                FormLookUpdetailPenerimaan_Piutang.destroy();
                                load_Penerimaan_Piutang("");
                            }
                            ;
                        }
                    }

            );
}
;
function paramdelete(linex)
{
    var params =
            {
                csar_number  	:Ext.getCmp('TxtVoucherPenerimaan_Piutang').getValue(),
                csar_date    	:Ext.getCmp('Txt_tanggal_Penerimaan_Piutang').getValue(),
                urut 			:  urut
            };
    return params;
}

function PostPenerimaanPiutang()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/anggaran/Penerimaan_Piutang/post",
                        params: paramPost(),
                        failure: function (o){
                            loadMask.hide();
                            ShowPesanWarningPenerimaan_Piutang('Hubungi Admin', 'Error');
                        },
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true){
                                
                                loadMask.hide();
                                ShowPesanInfoPenerimaan_Piutang('Data berhasil di Posting', 'Information');
                                load_Penerimaan_Piutang("");
                                FormLookUpdetailPenerimaan_Piutang.destroy();
                            }
                            ;
                        }
                    }

            );
}
;

function paramPost()
{
    var params =
            {
                csar_number :Ext.getCmp('TxtVoucherPenerimaan_Piutang').getValue(),
            	csar_date   :Ext.getCmp('Txt_tanggal_Penerimaan_Piutang').getValue(),
            	amount      :xz,
            
            };
            tmpjumlahrecord = 0;
            for (var L=0; L<ds_Penerimaan_Piutang_grid_general.getCount(); L++)
            {
                if (ds_Penerimaan_Piutang_grid_general.data.items[L].data.tag === 'true' || ds_Penerimaan_Piutang_grid_general.data.items[L].data.tag === true) {
                    params['arf_number'+L]        = ds_Penerimaan_Piutang_grid_general.data.items[L].data.arf_number; 
                    params['arf_date'+L]          = ds_Penerimaan_Piutang_grid_general.data.items[L].data.arf_date;
                    params['jumlah'+L]             = ds_Penerimaan_Piutang_grid_general.data.items[L].data.bayar;
                    tmpjumlahrecord ++;
                }else{

                }
                
            }
            params['jumlah']=tmpjumlahrecord;
            params['jumlah_detail']=ds_Penerimaan_Piutang_grid_detail.getCount()
            for (var L=0; L<ds_Penerimaan_Piutang_grid_detail.getCount(); L++)
            {
                params['arf_number_detail'+L]        = ds_Penerimaan_Piutang_grid_detail.data.items[L].data.arf_number;
                params['no_transaksi_detail'+L]      = ds_Penerimaan_Piutang_grid_detail.data.items[L].data.no_transaksi;
                params['jumlah_detail'+L]            = ds_Penerimaan_Piutang_grid_detail.data.items[L].data.bayar;
                params['kd_kasir_detail'+L]          = ds_Penerimaan_Piutang_grid_detail.data.items[L].data.kd_kasir;
            }
   
   return params;  
}


function loaddatafaktur_reff(cust_code){
    Ext.Ajax.request({
            url: baseURL + "index.php/anggaran/Penerimaan_Piutang/loaddatageneralfaktur",
            params: {
                cust_code:cust_code
            },
            failure: function(o){   
                loadMask.hide();
                ShowPesanWarningPenerimaan_Piutang('Hubungi Admin', 'Error');
            },  
            success: function(o){
            var cst = Ext.decode(o.responseText);
            if (cst.success === true){
                ds_Penerimaan_Piutang_grid_general.removeAll();
                var recs=[],
                recType=ds_Penerimaan_Piutang_grid_general.recordType;
                for(var i=0; i<cst.ListDataObj.length; i++){
                    recs.push(new recType(cst.ListDataObj[i]));
                }
                ds_Penerimaan_Piutang_grid_general.add(recs);
                grid_general_Penerimaan_Piutang.getView().refresh();
            } 
            }
        });
}

function loaddatadetailfaktur_reff(arf_number){
    Ext.Ajax.request({
            url: baseURL + "index.php/anggaran/Penerimaan_Piutang/loaddatadetailfaktur",
            params: {
                arf_number:arf_number
            },
            failure: function(o){   
                loadMask.hide();
                ShowPesanWarningPenerimaan_Piutang('Hubungi Admin', 'Error');
            },  
            success: function(o){
            var cst = Ext.decode(o.responseText);
	            if (cst.success === true){
	                ds_Penerimaan_Piutang_grid_detail.removeAll();
	                var recs=[],
	                recType=ds_Penerimaan_Piutang_grid_detail.recordType;
	                var pembayaran = rowSelected_general.data.bayar;
	                // var tmpsisa = 0;
	                for(var i=0; i<cst.ListDataObj.length; i++){
	                	if (rowSelected_general.data.bayar == rowSelected_general.data.sisa) {
	                		recs.push(new recType({
	                    				no_transaksi : cst.ListDataObj[i].no_transaksi,
	                                    jumlah 		 : cst.ListDataObj[i].jumlah,
	                                    sisa 		 : cst.ListDataObj[i].sisa,
	                                    kd_pasien 	 : cst.ListDataObj[i].kd_pasien, 
	                                    nama 		 : cst.ListDataObj[i].nama,
	                                    no_sjp 	 	 : cst.ListDataObj[i].no_sjp,
	                                    nama_unit 	 : cst.ListDataObj[i].nama_unit,
	                                    bayar 	 	 : cst.ListDataObj[i].sisa
	                               }));
	                	}else{	                		
	                		if (pembayaran >= cst.ListDataObj[i].jumlah) {
	                			recs.push(new recType({
	                    				no_transaksi : cst.ListDataObj[i].no_transaksi,
	                                    jumlah 		 : cst.ListDataObj[i].jumlah,
	                                    sisa 		 : cst.ListDataObj[i].sisa,
	                                    kd_pasien 	 : cst.ListDataObj[i].kd_pasien, 
	                                    nama 		 : cst.ListDataObj[i].nama,
	                                    no_sjp 	 	 : cst.ListDataObj[i].no_sjp,
	                                    nama_unit 	 : cst.ListDataObj[i].nama_unit,
	                                    bayar 	 	 : cst.ListDataObj[i].jumlah
	                               }));
	                			pembayaran = pembayaran - cst.ListDataObj[i].jumlah;
	                		}else{
	                			recs.push(new recType({
	                    				no_transaksi : cst.ListDataObj[i].no_transaksi,
	                                    jumlah 		 : cst.ListDataObj[i].jumlah,
	                                    sisa 		 : cst.ListDataObj[i].sisa,
	                                    kd_pasien 	 : cst.ListDataObj[i].kd_pasien, 
	                                    nama 		 : cst.ListDataObj[i].nama,
	                                    no_sjp 	 	 : cst.ListDataObj[i].no_sjp,
	                                    nama_unit 	 : cst.ListDataObj[i].nama_unit,
	                                    bayar 	 	 : pembayaran
	                               }));
	                			pembayaran = 0;
	                		}
	                	}	                    
	                }
	                ds_Penerimaan_Piutang_grid_detail.add(recs);
	                grid_detail_Penerimaan_Piutang.getView().refresh();
	            } 
            }
        });
}

function loaddatahistoryfaktur_reff(arf_number){
    Ext.Ajax.request({
            url: baseURL + "index.php/anggaran/Penerimaan_Piutang/loaddatahistorypembayaran",
            params: {
                arf_number:arf_number
            },
            failure: function(o){   
                loadMask.hide();
                ShowPesanWarningPenerimaan_Piutang('Hubungi Admin', 'Error');
            },  
            success: function(o){
            var cst = Ext.decode(o.responseText);
            if (cst.success === true){
                ds_Penerimaan_Piutang_grid_histori.removeAll();
                var recs=[],
                recType=ds_Penerimaan_Piutang_grid_histori.recordType;
                for(var i=0; i<cst.ListDataObj.length; i++){
                    recs.push(new recType(cst.ListDataObj[i]));
                }
                ds_Penerimaan_Piutang_grid_histori.add(recs);
                grid_histori_Penerimaan_Piutang.getView().refresh();
            }
        }
    });
}

function hitung(bayar){	
	if (bayar == rowSelected_general.data.amount) {
		loaddatadetailfaktur_reff(rowSelected_general.data.arf_number);
	}else{
		loaddatadetailfaktur_reff(rowSelected_general.data.arf_number);
	}
}
