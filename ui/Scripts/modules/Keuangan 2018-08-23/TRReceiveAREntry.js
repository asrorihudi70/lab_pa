
var CurrentTR_CSARForm = 
{
    data: Object,
    details:Array, 
    row: 0
};
var CurrentTRAkdAdj_CSARForm = 
{
    data: Object,
    details:Array, 
    row: 0
};
var CurrentSelected_CSARForm=
{
    data: Object,
    details:Array, 
    row: 0
};

var mRecord_CSARForm = Ext.data.Record.create
	(
		[
		   {name: 'ACCOUNT', mapping:'ACCOUNT'},
		   {name: 'NAMAACCOUNT', mapping:'NAMAACCOUNT'},
		   {name: 'DESC', mapping:'DESC'},
		   {name: 'VALUE', mapping:'VALUE'},
		   {name: 'CSARD_LINE', mapping:'CSARD_LINE'}
		]
	);

var mRecord = Ext.data.Record.create
(
	[	 
		{name:'CSAR_NUMBER', mapping: 'CSAR_NUMBER'}, 
		{name:'CSAR_DATE', mapping: 'CSAR_DATE'}, 
		{name:'ARO_NUMBER', mapping: 'ARO_NUMBER'}, 
		{name:'ARO_DATE', mapping: 'ARO_DATE'}, 
		{name:'AROH_NUMBER', mapping: 'AROH_NUMBER'},
		{name:'AROH_DATE', mapping: 'AROH_DATE'},
		{name:'AMOUNT', mapping: 'AMOUNT'}, 
		{name:'PAID', mapping: 'PAID'},
		{name:'REMAIN', mapping: 'REMAIN'},
	]
);

var KDkategori_CSARForm='1';	
var dsTRList_CSARForm;
var dsTmp_CSARForm;
var dsDTLTRList_CSARForm;
var DataAddNew_CSARForm = true;
var selectCount_CSARForm=50;
var now_CSARForm = new Date();
var selectBayar_CSARForm;
var selectAktivaLancar_CSARForm;
var rowSelected_CSARForm;
var TRLookUps_CSARForm;
var focusRef_CSARForm;
var no_sp3d;
var cellSelectedDet_CSARForm;
var cellSelectedDetAkdAdj_CSARForm;
var dsCboEntryCustCSARForm;
var selectCboEntryCustARForm;
var StrAccCust_CSARForm;
var StrNmAccCust_CSARForm;
var StrDueDay_CSARForm;
var CekApp=0;
var gridDTLTR_CSARForm;
var currentRowSelectionSearchAccountAREntry;
var grListTR_CSARForm;
var criteriaTanggalFindCSAR;

var NamaForm_CSARForm="Penerimaan Piutang";
var tempstrKdUser;

CurrentPage.page=getPanel_CSARForm(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanel_CSARForm(mod_id)
{
    var Field = 
	[
	'csar_number','csar_date','kd_customer','customer','account','name','pay_code','payment',
	'pay_no','currency','kurs','amount','notes','posted','kd_user'
	]
    dsTRList_CSARForm = new WebApp.DataStore({ fields: Field });
        
    grListTR_CSARForm = new Ext.grid.EditorGridPanel
	(
		{
			stripeRows: true,
			xtype: 'editorgrid',
			store: dsTRList_CSARForm,
			anchor: '100% 100%',
			columnLines:true,
			border:false,
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			//sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelected_CSARForm = dsTRList_CSARForm.getAt(row);
						}
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, row, rec)
				{
					rowSelected_CSAPForm = dsTRList_CSARForm.getAt(row);
					
					CurrentSelected_CSARForm.row = rec;
					CurrentSelected_CSARForm.data = rowSelected_CSARForm;

					if (rowSelected_CSARForm != undefined)
						{
							//DataAddNew_OpenArForm=false;
							LookUpForm_CSARForm(rowSelected_CSARForm.data);
					}
					else
					{
							//DataAddNew_OpenArForm=true;
							LookUpForm_CSARForm();
					}
				}
			},
			colModel: new Ext.grid.ColumnModel			
			//cm: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header		: 'Status Posting',
						width		: 60,
						sortable	: false,
						hideable	: true,
						hidden		: false,
						menuDisabled: true,
						dataIndex	: 'posted',
						id			: 'colStatusPosting_viCSAR',
						renderer	: function(value, metaData, record, rowIndex, colIndex, store){
							 switch (value){
								 case 't':
									 metaData.css = 'StatusHijau'; 
									 break;
								 case 'f':
									 metaData.css = 'StatusMerah';
									 break;
							 }
							 return '';
						}
					},
					{
						id: 'ColCSAR_NUMBER',
						header: 'Nomor',
						dataIndex: 'csar_number',
						sortable: true,			
						width :100,
						filter: {}
					}, 
					{
						//xtype: 'datecolumn',
						header: 'Tanggal',
						width: 100,
						sortable: true,
						dataIndex: 'csar_date',
						id:'ColCSAR_DATE',
						renderer: function(v, params, record) 
						{
							return ShowDateAkuntansi(record.data.csar_date);
						},
						filter: {}			
					}, 
					{						
						id: 'ColCUSTOMER',
						header: "Terima Dari",
						dataIndex: 'customer',
						width :130,
						filter: {}
					},
					{
						id: 'ColNotes',
						header: "Keterangan",
						dataIndex: 'notes',
						width :200,
						filter: {},
						renderer: function(value, cell) 
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px;text-align: left;'>" + value+ "</div>";
							return str;
						}
					}, 
					{
						id: 'ColAmount',
						header: "Jumlah (Rp)",
						align:'right',
						dataIndex: 'amount',
						renderer: function(v, params, record) 
						{
							return formatCurrencyDec(record.data.amount);
						},	
						width :80,
						filter: {}
					},
				]
			),
			tbar: 
			[				
				{
					id: 'btnAdd_CSARForm',
					text: 'Add Data',
					tooltip: 'Edit Data',
					iconCls: 'AddRow',
					handler: function(sm, row, rec) 
					{ 
						LookUpForm_CSARForm();
					}
				},' ','-',
				{
					id: 'btnEdit_CSARForm',
					text: 'Edit Data',
					tooltip: 'Edit Data',
					iconCls: 'Edit_Tr',
					handler: function(sm, row, rec) 
					{ 
						if (rowSelected_CSARForm != undefined)
						{
							LookUpForm_CSARForm(rowSelected_CSARForm.data);
							ButtonDisabled_CSARForm(rowSelected_CSARForm.data.Type_Approve);
						}
						else
						{
							LookUpForm_CSARForm();
						}
					}
				},' ','-'
				,
				{
					xtype: 'checkbox',
					id: 'chkWithTgl_CSARForm',					
					hideLabel:true,
					checked: true,
					handler: function() 
					{
						if (this.getValue()===true)
						{
							Ext.get('dtpTglAwalFilter_CSARForm').dom.disabled=false;
							Ext.get('dtpTglAkhirFilter_CSARForm').dom.disabled=false;
							Ext.get('dtpTglAwalFilter_CSARForm').dom.readOnly=true;	
							Ext.get('dtpTglAkhirFilter_CSARForm').dom.readOnly=true;
						}
						else
						{
							Ext.get('dtpTglAwalFilter_CSARForm').dom.disabled=true;
							Ext.get('dtpTglAkhirFilter_CSARForm').dom.disabled=true;							
						};
					}
				}
				, ' ','-','Tanggal : ', ' ',
				{
					xtype: 'datefield',
					fieldLabel: 'Dari Tanggal : ',
					id: 'dtpTglAwalFilter_CSARForm',
					format: 'd/M/Y',
					value:now_CSARForm,
					width:100,
					onInit: function() { },
					listeners: 
					{
						'specialkey' : function()
						{
							if (Ext.EventObject.getKey() === 13) 
							{
								RefreshDataFilter_CSARForm(false);
							}
						}
					}	
					
				}, ' ', ' s/d ',' ', {
					xtype: 'datefield',
					fieldLabel: 'Sd /',
					id: 'dtpTglAkhirFilter_CSARForm',
					format: 'd/M/Y',
					value:now_CSARForm,
					width:100,
					listeners: 
					{
						'specialkey' : function()
						{
							if (Ext.EventObject.getKey() === 13) 
							{
								RefreshDataFilter_CSARForm(false);
							}
						}
					}	
				},' ','->',
				{
					id: 'btnFind_CSARForm',
					text: ' Find',
					tooltip: 'Find Record',
					iconCls: 'find',
					handler: function(sm, row, rec) 
					{ 
						fnFindDlg_CSARForm();	
					}
				}
			],
			// bbar:new WebApp.PaggingBar
			// (
				// {
					// displayInfo: true,
					// store: dsTRList_CSARForm,
					// pageSize: 50,
					// displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
					// emptyMsg: "Tidak ada record&nbsp;&nbsp;"		
				// }
			// ),
			viewConfig: {forceFit: true} 			
		}
	);


	var FormTR_CSARForm = new Ext.Panel
	(
		{
			id: mod_id,
			closable:true,
			region: 'center',
			layout: 'form',
			title: NamaForm_CSARForm, 
			border: false,           
			shadhow: true,
			iconCls: 'Penerimaan',
			 margins:'0 5 5 0',
			items: [grListTR_CSARForm],
			tbar: 
			[
				'Nomor : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: 'No : ',
					id: 'txtNoFilter_CSARForm',                   
					//anchor: '25%',
					width:120,
					listeners:
					{ 
						'specialkey' : function()
						{
							if (Ext.EventObject.getKey() === 13) 
							{
								RefreshDataFilter_CSARForm(false);					
							} 						
						}
					},
					onInit: function() { }
				},
				{ xtype: 'tbseparator' },
				{
					xtype: 'checkbox',
					id: 'chkFilterApproved_OpenArForm',
					boxLabel: 'Posting',
					listeners: 
					{
						check: function()
						{
						   RefreshDataFilter_CSARForm(false);
						}
					}
				}, ' ','-',
					' ',mComboMaksData_CSARForm(),
					' ','->',
				{					
					xtype: 'button',
					tooltip: 'Tampilkan',
					iconCls: 'refresh',
					text: 'Tampilkan',
					anchor: '25%',
					handler: function(sm, row, rec) 
					{
						RefreshDataFilter_CSARForm(false);
					}
				}
			],
			listeners: 
			{ 
				'afterrender': function() 
				{           
					RefreshDataFilter_CSARForm(true);
					//RefreshDataFilter_CSARForm(true);				 
				}
			}
		}
	);
	return FormTR_CSARForm

};
    // end function get panel main data
 
 //---------------------------------------------------------------------------------------///
   
   
function LookUpForm_CSARForm(rowdata)
{
	var lebar=735;
	TRLookUps_CSARForm = new Ext.Window
	(
		{
			id: 'LookUpForm_CSARForm',
			title: NamaForm_CSARForm,
			closeAction: 'destroy',
			width: lebar,
			height: 500, 
			border: false,
			resizable:false,
			plain: true,
			layout: 'fit',
			iconCls: 'Penerimaan',
			modal: true,
			items: getFormEntryTR_CSARForm(lebar),
			listeners:
			{
				activate: function() 
				{
					
				},
				afterShow: function() 
				{ 
					this.activate(); 
				},
				deactivate: function()
				{
					// rowSelected_CSARForm=undefined;
					 RefreshDataFilter_CSARForm(true);
				}
			}
		}
	);
	
	TRLookUps_CSARForm.show();
	if (rowdata == undefined)
	{
		AddNew_CSARForm();
	}
	else
	{
		DataInit_CSARForm(rowdata)
	}	
	
};
   
function getFormEntryTR_CSARForm(lebar) 
{
	var pnlTR_CSARForm = new Ext.FormPanel
	(
		{
			id: 'pnlTR_CSARForm',
			fileUpload: true,
			region: 'north',
			layout: 'fit',
			bodyStyle: 'padding:10px 10px 10px 10px',			
			anchor: '100%', 
			width:lebar,			
			border: false,			
			items: [getItemPanelInput_CSARForm(lebar)],
			tbar: 
			[
				{
					text: 'Tambah',
					id:'btnTambah',
					tooltip: 'Tambah Record Baru ',
					iconCls: 'add',
					handler: function() 
					{ 
						AddNew_CSARForm();
						ButtonDisabled_CSARForm(false);
					}
				}, '-', 
				{
					text: 'Simpan',
					id:'btnSimpan',
					tooltip: 'Rekam Data ',
					iconCls: 'save',
					handler: function() 
					{ 
						CalcTotal_CSARForm();
						Datasave_CSARForm(false);
						RefreshDataFilter_CSARForm(false);
					}
				}, '-', 
				{
					text: 'Simpan & Keluar',
					id:'btnSimpanKeluar',
					tooltip: 'Rekam Data & Keluar ',
					iconCls: 'saveexit',
					handler: function() 
					{ 
						CalcTotal_CSARForm();
						Datasave_CSARForm(true);
						RefreshDataFilter_CSARForm(false);
						TRLookUps_CSARForm.close();
					}
				}, '-', 
				{
					text: 'Hapus',
					id:'btnHapus',
					tooltip: 'Remove the selected item',
					iconCls: 'remove',
					handler: function() 
					{ 
						if(Ext.getCmp('ChkApproveEntry_CSARForm').getValue() == true || Ext.getCmp('ChkApproveEntry_CSARForm').getValue() == 'true'){
							ShowPesanError_CSARForm('Penerimaan sudah di approve, hapus tidak dapat dilakukan!','Error');
						} else{							
							Ext.Msg.show
							(
								{
								   title:'Hapus',
								   msg: 'Apakah penerimaan ini akan dihapus ?', 
								   buttons: Ext.MessageBox.YESNO,
								   fn: function (btn) 
								   {			
									   if (btn =='yes') 
										{
											DataDelete_CSARForm();
											RefreshDataFilter_CSARForm(false);
										} 
								   },
								   icon: Ext.MessageBox.QUESTION
								}
							);
						}
					}
				}, '-', 
				{
					text: 'Lookup',
					id:'btnLookup',
					tooltip: 'Lookup Account',
					iconCls: 'find',
					hidden:true,
					handler: function() 
					{
						if (focusRef_CSARForm == "ref")
						{
							focusRef_CSARForm="";
							var StrKriteria;
							StrKriteria = " WHERE APP_SP3D = 1 and SUBSTRING(AC.CSAR_NUMBER,0,5)='BKBS'"		
							var p = new mRecord_CSARForm
							(
								{
									ACCOUNT: '',
									NAMAACCOUNT: '',
									DESC: Ext.getCmp('txtCatatan_CSARForm').getValue(),
									VALUE: '',								
									CSARD_LINE:''
								}
							);							
							FormLookupsp3d(dsDTLTRList_CSARForm,p, StrKriteria,nASALFORM_NONMHS);
						}
						else
						{
							var p = new mRecord_CSARForm
							(
								{
									ACCOUNT: '',
									NAMAACCOUNT: '',
									DESC: Ext.getCmp('txtCatatan_CSARForm').getValue(),
									VALUE: '',								
									CSARD_LINE:''
								}
							);
							FormLookupAccount(" Where A.TYPE ='D'  ",dsDTLTRList_CSARForm,p,true,'',true);
						}
					}
				 }, //'-',
				{
				    text: 'Approve',
					id:'btnApprove',
				    tooltip: 'Approve',
				    iconCls: 'approve',
				    handler: function() 
					{ 
						if (ValidasiApprove_CSAPRorm('Approve')==1)
						{
							if (Ext.getCmp('ChkApproveEntry_CSARForm').getValue()==false)
							{
									var criteria = Ext.get('txtNo_CSARForm').getValue();
									var criteriaDate = Ext.get('dtpTanggal_CSARForm').getValue();
									FormLookup_AkadPiutang(selectCboEntryCustARForm,criteria,criteriaDate,'Akad','1',
										CalcTotalDetailGrid_CSARForm(),selectAktivaLancar_CSARForm,StrAccCust_CSARForm,Ext.get('txtCatatan_CSARForm').getValue(),'','');
							}else
							{
								viewHistoryApproved();
							}
						}
						
					}
				}
				, '-', '->', '-',
				{
					text: 'Cetak',
					tooltip: 'Print',
					iconCls: 'print',					
					// hidden:true,
					handler: function() {Cetak_CSARForm()}
				}
			]
		}
	); 

	var GDtabDetail_CSARForm = new Ext.TabPanel
	(
		{
			region: 'center',
			id: 'GDtabDetail_CSARForm',
			activeTab: 0,
			//anchor: '100% 46%',
			anchor: '100% 47%',
			border: false,
			plain: true,
			defaults: 
			{
				autoScroll: false
			},
			items: GetDTLTRGrid_CSARForm(),
			tbar: 
			[
			{
				text: 'Tambah Baris',
				id:'btnTambahBaris',
				tooltip: 'Tambah Record Baru ',
				iconCls: 'add',				
				handler: function() 
				{ 
					if(Ext.getCmp('ChkApproveEntry_CSARForm').getValue() == true || Ext.getCmp('ChkApproveEntry_CSARForm').getValue() == 'true'){
						ShowPesanError_CSARForm('Penerimaan sudah di approve, tambah baris tidak dapat dilakukan!','Error');
					} else{	
						TambahBaris_CSARForm();
					}
				}
			}, '-', 
			{
				text: 'Hapus Baris',
				id:'btnHapusBaris',
				tooltip: 'Remove the selected item',
				iconCls: 'remove',
				handler: function()
					{
						if(Ext.getCmp('ChkApproveEntry_CSARForm').getValue() == true || Ext.getCmp('ChkApproveEntry_CSARForm').getValue() == 'true'){
							ShowPesanError_CSARForm('Penerimaan sudah di approve, hapus baris tidak dapat dilakukan!','Error');
						} else{	
							if (dsDTLTRList_CSARForm.getCount() > 0 )
							{						
								if (cellSelectedDet_CSARForm != undefined)
								{
									if(CurrentTR_CSARForm != undefined)
									{
										HapusBaris_CSARForm();
									}
								}
								else
								{
									ShowPesanWarning_CSARForm('Silahkan pilih dahulu baris yang akan dihapus','Hapus baris');
								}
							}
						}
					}
			}
		] 
	}
);

	var Total = new Ext.Panel
		(
			{
				frame: false,
				layout: 'form',
				width: 500,
				border:false,
				id:'PnlTotal_CSARForm',
				labelAlign:'right',
				labelWidth:50,
				style: 
				{
					'margin-top': '5.9px',
					'margin-left': '457px'
				},
				items: 
				[
					{
						xtype: 'textfield',
						id:'txtTotal_CSARForm',
						name:'txtTotal_CSARForm',
						fieldLabel: 'Total ',
						readOnly:true,
						style:
						{	
							'text-align':'right',
							'font-weight':'bold'
						},
						value:'0',
						width: 185
					}
				]
			}
		);	
	var Formload_CSARForm = new Ext.Panel
	(
		{
			id: 'Formload_CSARForm',
			region: 'center',
			width:'100%',
			anchor:'100%',
			layout: 'form',
			title: '',
			bodyStyle: 'padding:15px',
			border: true,
			bodyStyle: 'background:#FFFFFF;',
			shadhow: true,
			//iconCls: 'GL',
			items: [pnlTR_CSARForm, GDtabDetail_CSARForm,Total]//[pnlTR_CSARForm, GDtabDetail_CSARForm]//,Total]

		}
	);
	
	//RefreshDataDetail_CSARForm();
	return Formload_CSARForm						
};
 //---------------------------------------------------------------------------------------///
 
function Cetak_CSARForm()
{
	var strKriteria;
	if (Ext.get('txtNo_CSARForm').dom.value !='' )
	{
		strKriteria = Ext.get('txtNo_CSARForm').dom.value + '##'; //01
		strKriteria += 1+'##';
		strKriteria += Ext.get('cboCustEntryARForm').dom.value + '##'; //02
		//strKriteria +=  selectAktivaLancar_CSARForm + '-' + Ext.get('comboAktivaLancar_CSARForm').dom.value +'##'; //03
		strKriteria += Ext.getCmp('txtTotal_CSARForm').getValue() + '##'; //05
		strKriteria += Ext.get('comboAktivaLancar_CSARForm').dom.value +'##'; //03
		strKriteria += ShowDateAkuntansi(Ext.getCmp('dtpTanggal_CSARForm').getValue()) + '##'; //04
		ShowReport('', '010517', strKriteria);	

	};
};
 
 
function GetDTLTRGrid_CSARForm() 
{
	// grid untuk detail transaksi	
	var fldDetail = ['CSAR_NUMBER','CSAR_DATE','CSARD_LINE','account','desc','value','posted', 'name']
	
	dsDTLTRList_CSARForm = new WebApp.DataStore({ fields: fldDetail })
	dsTmp_CSARForm= new WebApp.DataStore({ fields: fldDetail })
	
	gridDTLTR_CSARForm = new Ext.grid.EditorGridPanel
	(
		{
			title: 'Dari Detail ' + NamaForm_CSARForm,
			stripeRows: true,
			id:'Dttlgrid_CSARForm',
			store: dsDTLTRList_CSARForm,
			border: false,
			columnLines:true,
			frame:true,
			height:130,
			anchor: '100% 100%',			
			sm: new Ext.grid.CellSelectionModel
			(
				{
					singleSelect: true,
					listeners: 
					{
						cellselect: function(sm, row, rec)
						{
							cellSelectedDet_CSARForm =dsDTLTRList_CSARForm.getAt(row);
							CurrentTR_CSARForm.row = row;
							//CurrentTRGL.data = cellSelectedGLDet;
						}
					}
				}
			),
			cm: TRDetailColumModel_CSARForm()
			, viewConfig: 
			{
				forceFit: true
			}
		}
	);
			
	return gridDTLTR_CSARForm;
};


function RefreshDataDetail_CSARForm(no,tgl)
{	
	Ext.Ajax.request({
		url: baseURL + "index.php/keuangan/functionPenerimaanPiutang/initListDetailAccount",
		params: {
			csar_number:no,
			csar_date:tgl
		},
		success: function(o) {
			var cst = Ext.decode(o.responseText);
			if (cst.success === true) 
			{
				dsDTLTRList_CSARForm.removeAll();
				var recs=[],
				recType=dsDTLTRList_CSARForm.recordType;
				for(var i=0; i<cst.total_record; i++){
					recs.push(new recType(cst.listData[i]));						
				}
				dsDTLTRList_CSARForm.add(recs);
				gridDTLTR_CSARForm.getView().refresh();
			}
			else {
				ShowPesanError_CSARForm('Gagal membaca list detail penerimaan piutang!' , 'Simpan Data');
			}
		}
	})
}
//---------------------------------------------------------------------------------------///
			
function TRDetailColumModel_CSARForm() 
{
	return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(), 
			{
				id: 'Account_CSARForm',
				name: 'Account_CSARForm',
				header: "Account",
				dataIndex: 'account',
				sortable: false,
				anchor: '10%',
				editor: new Ext.form.TextField({
					id:'fieldAcc_CSARForm',
					allowBlank: false,
					enableKeyEvents : true,
					listeners: 
					{ 
						'specialkey' : function()
						{
							if (Ext.EventObject.getKey() === 13) 
							{
								LookUpAccount_AREntry(" Where A.Account like '" + Ext.get('fieldAcc_CSARForm').dom.value + "%'  AND A.TYPE ='D'  ");
							} 
						}
					}
				}),
				width: 70
			}, 
			{
				id: 'Name_CSARForm',
				name: 'Name_CSARForm',
				header: "Nama Account",
				dataIndex: 'name',
				anchor: '30%',
				editor: new Ext.form.TextField({
					id:'fieldAccName_CSARForm',
					allowBlank: false,
					enableKeyEvents : true,
					listeners: 
					{ 
						'specialkey' : function()
						{
							if (Ext.EventObject.getKey() === 13) 
							{
								LookUpAccount_AREntry(" Where A.Name like '" + Ext.get('fieldAccName_CSARForm').dom.value + "%'  AND A.TYPE ='D'  ");									
							} 
						}
					}
				})
			}, 
			{
				id: 'DESC_CSARForm',
				name: 'DESC_CSARForm',
				header: "Keterangan",
				anchor: '35%',
				dataIndex: 'desc', 
				editor: new Ext.form.TextField({
					allowBlank: true
				})
			}, 
			{
				id: 'Value_CSARForm',				
				name: 'Value_CSARForm',
				header: "Jumlah (Rp)",
				anchor: '15%',
				xtype:'numbercolumn',
				dataIndex: 'value', 
				align:'right',
				editor: new Ext.form.NumberField({
					allowBlank: false,
					enableKeyEvents : true,
					listeners: 
					{ 
						keyDown: function(a,b,c){
							if(b.getKey()==13){
								var line = gridDTLTR_CSARForm.getSelectionModel().selection.cell[0];
								var o = dsDTLTRList_CSARForm.getRange()[line].data;		
								if (o.account != '' || o.account != undefined){
									CalcTotal_CSARForm(CurrentTR_CSARForm.row);	
									TambahBaris_CSARForm();
								}		
							}
						}
					}
				})
			},
			{
				id: 'Value_CSARForm',	
				header: "LINE",
				dataIndex: 'line', 
				hidden:true,
			}
		]
	)
};

//---------------------------------------------------------------------------------------///
function DataInit_CSARForm(rowdata) 
{
	DataAddNew_CSARForm = false;
	tempstrKdUser=rowdata.kd_user;
	Ext.get('cboCustEntryARForm').dom.value = rowdata.customer;
	selectCboEntryCustARForm = rowdata.kd_customer;
	
	Ext.get('comboBayar_CSARForm').dom.value= rowdata.payment;
	selectBayar_CSARForm = rowdata.pay_code;
	//alert()
	Ext.get('comboAktivaLancar_CSARForm').dom.value= rowdata.account + ' - '+rowdata.name;
	selectAktivaLancar_CSARForm = rowdata.account;

    Ext.get('txtNo_CSARForm').dom.value = rowdata.csar_number;
	Ext.getCmp('dtpTanggal_CSARForm').disable(); 
	  
	Ext.get('txtTerimaDari_CSARForm').dom.value = rowdata.customer;
	Ext.get('dtpTanggal_CSARForm').dom.value = ShowDateAkuntansi(rowdata.csar_date);   
	
	Ext.get('txtTotal_CSARForm').dom.value = formatCurrencyDec(rowdata.amount);
	Ext.get('txtCatatan_CSARForm').dom.value = rowdata.notes;
	Ext.get('txtNoPembayaran_CSARForm').dom.value = rowdata.pay_no;
	Ext.getCmp('cboPengembalian_CSARForm').setValue('');	
	RefreshDataDetail_CSARForm(rowdata.csar_number,rowdata.csar_date);
	
	CekApp=rowdata.posted;
	if(rowdata.posted == 't'){		
		Ext.getCmp('ChkApproveEntry_CSARForm').setValue(true);
		Ext.getCmp('btnSimpan').disable();
		Ext.getCmp('btnSimpanKeluar').disable();
		Ext.getCmp('btnHapus').disable();
	} else{
		Ext.getCmp('ChkApproveEntry_CSARForm').setValue(false);
		Ext.getCmp('btnSimpan').enable();
		Ext.getCmp('btnSimpanKeluar').enable();
		Ext.getCmp('btnHapus').enable();
	}
	
	Ext.get('txtNo_CSARForm').dom.readOnly=true;
	Ext.getCmp('cboCustEntryARForm').setReadOnly(true);
	Ext.getCmp('comboBayar_CSARForm').setReadOnly(true);
	Ext.getCmp('comboAktivaLancar_CSARForm').setReadOnly(true);
	
};

//---------------------------------------------------------------------------------------///
function AddNew_CSARForm() 
{
	DataAddNew_CSARForm = true;	
	// tempstrKdUser=strKdUser;
	tempstrKdUser='';
	Ext.get('txtNo_CSARForm').dom.readOnly=false;
	Ext.getCmp('dtpTanggal_CSARForm').enable(); 	
	Ext.get('txtNo_CSARForm').dom.value = '';	
	Ext.getCmp('ChkApproveEntry_CSARForm').setValue(false);	
	Ext.get('txtTerimaDari_CSARForm').dom.value = '';
	Ext.get('txtNoPembayaran_CSARForm').dom.value = '';
	Ext.get('txtTotal_CSARForm').dom.value = '0';
	Ext.get('txtCatatan_CSARForm').dom.value = 'Summary from cashier post : Receive AR';	
	Ext.get('comboBayar_CSARForm').dom.value='';
	Ext.get('comboAktivaLancar_CSARForm').dom.value = '';
	Ext.getCmp('cboPengembalian_CSARForm').setValue('0');	
	Ext.get('dtpTanggal_CSARForm').dom.value = ShowDateAkuntansi(now_CSARForm);
	Ext.get('cboCustEntryARForm').dom.value = '';
	Ext.getCmp('cboCustEntryARForm').setValue('');
	selectCboEntryCustARForm = '';
	// Ext.getCmp('txtTotalAkdAdj_CSARForm').setValue(0);

	dsDTLTRList_CSARForm.removeAll();
	rowSelected_CSARForm=undefined;
	Ext.getCmp('ChkApproveEntry_CSARForm').setValue(false);
	Ext.getCmp('btnSimpan').enable();
	Ext.getCmp('btnSimpanKeluar').enable();
	Ext.getCmp('btnHapus').enable();
	// dsDTLTRListAkdAjd_CSARForm.removeAll();
	// Ext.getCmp('tabDtlAkdAdj_CSARForm').disable();
	CekApp=0
};

function getParam_CSARForm() 
{
	var params = 
	{		
		csar_number:Ext.get('txtNo_CSARForm').getValue(),
		csar_date:Ext.get('dtpTanggal_CSARForm').getValue(),
		kd_customer:selectCboEntryCustARForm,
		account:selectAktivaLancar_CSARForm,
		pay_code:selectBayar_CSARForm,
		csar_pay_no:Ext.get('txtNoPembayaran_CSARForm').getValue(),
		currency:'IDR',//'Rp',
		kurs:'1',
		amount:CalcTotalDetailGrid_CSARForm(),//getAmount_CSARForm(Ext.get('txtTotal_CSARForm').getValue()),
		notes:Ext.get('txtCatatan_CSARForm').getValue(),		
		no_tag:'',
		date_tag:'',
		amountkurs:'1',
		kd_user:tempstrKdUser,
		is_detail:0,
		is_approve:0,		
	};
	
	params['jumlah']=dsDTLTRList_CSARForm.getCount();
	for(var i = 0 ; i < dsDTLTRList_CSARForm.getCount();i++)
	{
		params['account-'+i]=dsDTLTRList_CSARForm.data.items[i].data.account;
		params['desc-'+i]=dsDTLTRList_CSARForm.data.items[i].data.desc;
		params['value-'+i]=dsDTLTRList_CSARForm.data.items[i].data.value;
		params['line-'+i]=dsDTLTRList_CSARForm.data.items[i].data.line;
	}
	return params
};
//---------------------------------------------------------------------------------------///

function getAmount_CSARForm(dblNilai)
{
    var dblAmount;
    dblAmount = dblNilai.replace('Rp.', '')
    for (var i = 0; i < dblAmount.length; i++) {
        var x = dblAmount.substr(i, 1)
        if (x === '.') {
            dblAmount = dblAmount.replace('.', '');
        }
    }    
    // return Ext.num(dblAmount)
    return dblAmount
};

function getAmountGrid_CSARForm(dblNilai)
{
    var dblAmount;
    dblAmount = dblNilai.replace('Rp.', '')
    for (var i = 0; i < dblAmount.length; i++) {
        var x = dblAmount.substr(i, 1)
        if (x === '.') {
            dblAmount = dblAmount.replace('.', '');
        }
    }    
    // return Ext.num(dblAmount)
    return dblAmount.replace(',', '.')
};

function getItemPanelInput_CSARForm(lebar)
{
	var items = 
	{
		layout:'fit',
		anchor:'100%',
		width: lebar - 36,
		height: 160,//130,
		labelAlign: 'right',
		bodyStyle: 'padding:7px 7px 7px 7px',
		items:
		[
			{
				columnWidth:.9,
				width:lebar-36,
				layout: 'form',
				border:false,
				items: 
				[
					getItemPanelNo_CSARForm(lebar),
					getItemPanelTerimaDari_CSARForm(lebar),
					getItemPanelAktivaLancar_CSARForm(lebar),
					getItemPanelPayModePayNumber_CSARForm(lebar),
					getItemPanelCatatan_CSARForm(lebar),
					getItemPanelPengembalian_CSARForm(lebar)
				]
			}
		]
	};
	return items;			
};

function getItemPanelAktivaLancar_CSARForm(lebar)
{
	var items = 			
	{
	layout:'column',
	border:false,
	width:lebar-36,
	items:
		[
			{
				// columnWidth:0.98,
				columnWidth:.5,
				layout: 'form',
				border:false,
				labelWidth:111,
				items: 
				[
					mcomboAktivaLancar_CSARForm(),					
				]
			},
			{
				columnWidth:.5,
				layout: 'form',
				border:false,
				items: 
				[
					{
						xtype:'textfield',
						fieldLabel: 'No. Pembayaran ',
						name: 'txtNoPembayaran_CSARForm',
						id: 'txtNoPembayaran_CSARForm',
						anchor:'96%'						
					}
				]
			}
		]
	}
	return items;	
}; 

function getItemPanelCatatan_CSARForm(lebar)
{
	var items = 			
	{
	layout:'column',
	border:false,
	width:lebar-36,
	items:
		[
			{
				columnWidth:0.98,
				layout: 'form',
				border:false,
				labelWidth:111,
				items: 
				[
					{
						xtype:'textarea',
						fieldLabel: 'Keterangan ',
						name: 'txtCatatan_CSARForm',
						id: 'txtCatatan_CSARForm',
						anchor:'99.9%',
						value:'Summary from cashier post : Receive AR',
						height: 39,
						autoCreate: {tag: 'input', maxlength: '255'}
					}
				]
			}
		]
	}
	return items;	
}; 

function getItemPanelPengembalian_CSARForm(lebar)
{
	var items = 			
	{
	layout:'column',
	border:false,
	width:lebar-36,
	items:
		[
			{
				// columnWidth:0.98,
				columnWidth:.5,
				layout: 'form',
				border:false,
				labelWidth:111,
				items: 
				[
					mComboPengembalian_CSARForm()					
				]
			}
		]
	}
	return items;	
}; 



function getItemPanelNo_CSARForm(lebar)
{
	var items = 			
	{
	layout:'column',
	border:false,
	width:lebar-36,
	items:
		[
			{
				columnWidth:.5,
				layout: 'form',
				border:false,
				labelWidth:111,
				items: 
				[
					{
						xtype:'textfield',
						fieldLabel: 'Nomor ',
						name: 'txtNo_CSARForm',
						id: 'txtNo_CSARForm',
						readOnly:false,
						anchor:'95%',
						listeners: 
						{
							'blur' : function()
							{	
								if (DataAddNew_CSARForm == true)	{
									CekDataInput_CSARForm();
								}							
							},
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									if (DataAddNew_CSARForm == true)	{
										CekDataInput_CSARForm();
									}
								}
							}
						}	
					}
				]
			},
			{
				columnWidth:.5,
				layout: 'form',
				border:false,
				labelWidth:100,
				items: 
				[
					{
						xtype:'checkbox',
						fieldLabel: 'Approve',
						name: 'ChkApproveEntry_CSARForm',
						id: 'ChkApproveEntry_CSARForm',
						disabled:true,
						anchor:'95%'
					}
				]
			}
		]
	}
	return items;	
};

function getItemPanelPayModePayNumber_CSARForm(lebar)
{
	var items = 			
	{
	layout:'column',
	width:lebar-36,
	border:false,
	items:
	[
		{
			columnWidth:.5,
			layout: 'form',
			border:false,
			labelWidth:111,
			items: 
			[mcomboBayar_CSARForm()]			
		}		
	]
	}
	return items;	
}

function getItemPanelTerimaDari_CSARForm(lebar)
{
	var items = 			
	{
	layout:'column',
	width:lebar-36,
	border:false,
	items:
		[
			{
				columnWidth:.5,
				layout: 'form',
				border:false,
				labelWidth:111,
				items: 
				[
					mCboEntryCSARForm(),
					{
						xtype:'textfield',
						fieldLabel: 'Terima Dari ',
						name: 'txtTerimaDari_CSARForm',
						id: 'txtTerimaDari_CSARForm',
						anchor:'95%',
						hidden:true
					}
				]
			},
			{
				columnWidth:.5,
				layout: 'form',
				border:false,
				items: 
				[
					{
						xtype: 'datefield',
                        fieldLabel: 'Tanggal ',
                        id: 'dtpTanggal_CSARForm',
                        name: 'dtpTanggal_CSARForm',
                        format: 'd/M/Y',
						value:now_CSARForm,
                        anchor: '70%'
					}
				]
			}
		]
	}
	return items;	
}

function mcomboAktivaLancar_CSARForm()
{
	var Field = ['account','name','groups','akun'];
	dsAktivaLancar_CSARForm = new WebApp.DataStore({ fields: Field });
	
	Ext.Ajax.request({
		url: baseURL + "index.php/keuangan/functionGeneral/getAktivaLancar",
		params: {query:''},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			comboAktivaLancar_CSARForm.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsAktivaLancar_CSARForm.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsAktivaLancar_CSARForm.add(recs);
			}
		}
	});
	
 var comboAktivaLancar_CSARForm = new Ext.form.ComboBox
	(
		{
			id:'comboAktivaLancar_CSARForm',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih Kas / Bank...',
			fieldLabel: 'Ke Kas / Bank ',			
			align:'Right',
			anchor:'95%',
			listWidth:400,
			store: dsAktivaLancar_CSARForm,
			valueField: 'account',
			displayField: 'akun',
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectAktivaLancar_CSARForm=b.data.account ;
				} 
			}
		}
	);
	
	return comboAktivaLancar_CSARForm;
} ;

function RefreshData_CSARForm()
{			
	dsTRList_CSARForm.load
	(
		{ 
			params: 
			{ 	
				Skip: 0,
				Take: selectCount_CSARForm,
				Sort: 'CSAR_DATE',
				Sortdir: 'ASC',
				target:'viACC_CSAR',
				param: " "
			}
		}
	);
	return dsTRList_CSARForm;
};

function mComboMaksData_CSARForm()
{
  var cboMaksDataCSARForm = new Ext.form.ComboBox
	(
		{
			id:'cboMaksDataCSARForm',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Maks.Data ',			
			width:50,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[1, 50], [2, 100],[3, 200],[4, 500],[5, 1000]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCount_CSARForm,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectCount_CSARForm=b.data.displayText ;
					RefreshDataFilter_CSARForm(false);
				} 
			},
			hidden:true
		}
	);
	return cboMaksDataCSARForm;
};

function mCboEntryCSARForm()
{
	var Fields = ['kd_customer', 'customer', 'customer_name'];
    dsCboEntryCustCSARForm = new WebApp.DataStore({ fields: Fields });
	
	Ext.Ajax.request({
		url: baseURL + "index.php/keuangan/functionGeneral/getCustomer",
		params: {query:''},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cboCustEntryARForm.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsCboEntryCustCSARForm.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsCboEntryCustCSARForm.add(recs);
			}
		}
	});
    
    var cboCustEntryARForm = new Ext.form.ComboBox
	(
		{
		    id: 'cboCustEntryARForm',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: 'Pilih customer ...',
		    fieldLabel: 'Customer ',
		    align: 'right',
		    anchor:'95%',
		    listWidth:350,
		    store: dsCboEntryCustCSARForm,
		    valueField: 'kd_customer',
		    displayField: 'customer_name',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        selectCboEntryCustARForm = b.data.kd_customer;
			        GetCustAcc_CSARForm(b.data.kd_customer);
			    }
			}
		}
	);
	
	return cboCustEntryARForm;
};

function RefreshDataFilter_CSARForm(mBol) 
{   
	Ext.Ajax.request({
		url: baseURL + "index.php/keuangan/functionPenerimaanPiutang/getInitList",
		params: {
			tgl_awal : Ext.getCmp('dtpTglAwalFilter_CSARForm').getValue(),
			tgl_akhir : Ext.getCmp('dtpTglAkhirFilter_CSARForm').getValue(),
			posted : Ext.getCmp('chkFilterApproved_OpenArForm').getValue(),
			csar_number : Ext.getCmp('txtNoFilter_CSARForm').getValue(),
		},
		success: function(o) {
			var cst = Ext.decode(o.responseText);
			if (cst.success === true) 
			{
				dsTRList_CSARForm.removeAll();
				var recs=[],
					recType=dsTRList_CSARForm.recordType;
				for(var i=0; i<cst.listData.length; i++){
					recs.push(new recType(cst.listData[i]));						
				}
				dsTRList_CSARForm.add(recs);
				grListTR_CSARForm.getView().refresh();
			}
			else {
				ShowPesanError_CSARForm('Gagal menampilkan list penerimaan piutang!', 'Simpan Data');
			}
		}
	})
	/* var strCrt_CSARForm='';
	
	if (Ext.get('txtNoFilter_CSARForm').getValue() != '') 
	{
		if (strCrt_CSARForm ==='')
		{
			strCrt_CSARForm = " Where CSAR_NUMBER like '" + Ext.get('txtNoFilter_CSARForm').getValue() + "%' ";
		}
		else
		{
			strCrt_CSARForm += " AND CSAR_NUMBER like '" + Ext.get('txtNoFilter_CSARForm').getValue() + "%' ";
		}
	};
	
	if (Ext.getCmp('chkWithTgl_CSARForm').getValue() === true) 
	{
		if (strCrt_CSARForm ==='')
		{
			strCrt_CSARForm = " Where CSA.CSAR_DATE >='" + FormatDateReport(Ext.getCmp('dtpTglAwalFilter_CSARForm').getValue())  + "' ";
			strCrt_CSARForm += " and  CSA.CSAR_DATE <='" + FormatDateReport(Ext.getCmp('dtpTglAkhirFilter_CSARForm').getValue())  + "' ";
		}
		else
		{
			strCrt_CSARForm += " and (CSA.CSAR_DATE >='" + FormatDateReport(Ext.getCmp('dtpTglAwalFilter_CSARForm').getValue())  + "' ";
			strCrt_CSARForm += " and  CSA.CSAR_DATE <='" + FormatDateReport(Ext.getCmp('dtpTglAkhirFilter_CSARForm').getValue())  + "') ";
		}
	};	

	if(Ext.getCmp('chkFilterApproved_OpenArForm').getValue() === true)
	{
		if (strCrt_CSARForm ==='')
		{
			strCrt_CSARForm = " Where (CSA.NO_TAG IS Not NULL AND CSA.NO_TAG <> '') ";
		}
		else
		{
			strCrt_CSARForm += " AND (CSA.NO_TAG IS Not NULL AND CSA.NO_TAG <> '') ";
		}
	}else{
		if (strCrt_CSARForm ==='')
		{
			strCrt_CSARForm = " Where (CSA.NO_TAG IS NULL OR CSA.NO_TAG ='') ";
		}
		else
		{
			strCrt_CSARForm += " AND (CSA.NO_TAG IS NULL OR CSA.NO_TAG ='') ";
		}
	};
    if (strCrt_CSARForm != undefined) 
    {  
		// dsTRList_CSARForm.load
		// (
			// { 
				// params:  
				// {   
					// Skip: 0, 
					// Take: selectCount_CSARForm, 
					// Sort: 'CSAR_DATE', 
					// Sortdir: 'ASC', 
					// target:'viACC_CSAR',
					// param: strCrt_CSARForm
				// }			
			// }
		// );    
		
    }
	else
	{
	    RefreshDataFilter_CSARForm(true);
	} */
};


function mcomboBayar_CSARForm()
{
	var Field = ['pay_code', 'payment'];
	dsBayar_CSARForm = new WebApp.DataStore({ fields: Field });
	
	Ext.Ajax.request({
		url: baseURL + "index.php/keuangan/functionGeneral/getJenisBayar",
		params: {query:''},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			comboBayar_CSARForm.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsBayar_CSARForm.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsBayar_CSARForm.add(recs);
			}
		}
	});

  var comboBayar_CSARForm = new Ext.form.ComboBox
	(
		{
			id:'comboBayar_CSARForm',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih Jenis Pembayaran...',
			fieldLabel: 'Jenis Pembayaran ',			
			align:'Right',
			anchor:'95%',
			store: dsBayar_CSARForm,
			valueField: 'pay_code',
			displayField: 'payment',
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectBayar_CSARForm=b.data.pay_code ;
				} 
			}
		}
	);
	
	return comboBayar_CSARForm;
} ;

function Datasave_CSARForm(IsExit) 
{
	if (ValidasiEntry_CSARForm('Simpan Data') == 1 )
	{
		Ext.Ajax.request({
			url: baseURL + "index.php/keuangan/functionPenerimaanPiutang/save",
			params: getParam_CSARForm(),
			success: function(o) {
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					ShowPesanInfo_CSARForm('Data berhasil di simpan', 'Simpan Data');
					if (IsExit===false)
					{
						Ext.get('txtNo_CSARForm').dom.value = cst.csar_number;
					}
					RefreshDataFilter_CSARForm(false);
					DataAddNew_CSARForm=false;
					Ext.get('txtNo_CSARForm').dom.readOnly=true;
					RefreshDataDetail_CSARForm(cst.csar_number,Ext.get('dtpTanggal_CSARForm').getValue());
				}
				else if (cst.success === false && cst.pesan === 0) 
				{
					ShowPesanWarning_CSARForm('Data tidak berhasil di simpan ' + cst.pesan , 'Simpan Data');
				}
				else {
					ShowPesanError_CSARForm('Data tidak berhasil di simpan, Error : ' + cst.pesan, 'Simpan Data');
				}
			}
		})
	}
};

function ValidasiEntry_CSARForm(modul)
{
	var x = 1;
	// if(Ext.get('txtNo_CSARForm').getValue()=='')
	// {
		// ShowPesanWarning_CSARForm('Nomor belum di isi',modul);
		// x=0;
	// }
	// else 
	if(selectCboEntryCustARForm == '' || selectCboEntryCustARForm == undefined)
	{
		ShowPesanWarning_CSARForm('Customer belum dipilih',modul);
		x=0;
	}else if(Ext.get('comboAktivaLancar_CSARForm').getValue()=='')
	{
		ShowPesanWarning_CSARForm('Kas / Bank belum di isi',modul);
		x=0;
	}else if (Ext.get('comboBayar_CSARForm').getValue()=='')
	{
		ShowPesanWarning_CSARForm('Jenis pembayaran belum di isi',modul);
		x=0;
	}else if ( dsDTLTRList_CSARForm.getCount() <= 0)
	{
		ShowPesanWarning_CSARForm('Detai Receive AR belum di isi',modul);
		x=0;
	}else if (Ext.get('txtCatatan_CSARForm').getValue().length > 255)
	{
		ShowPesanWarning_CSAPForm('Keterangan Tidak boleh lebih dari 255 Karakter',modul);
		x=0;
	};
	
	for(var i = 0 ; i < dsDTLTRList_CSARForm.getCount();i++)
	{
		var o = dsDTLTRList_CSARForm.data.items[i].data;
		if(o.account == '' || o.account == undefined){
			dsDTLTRList_CSARForm.removeAt(i);
		}
	}
	
	return x;
};

function ValidasiApprove_CSAPRorm(modul)
{
	var x = 1;
	if(Ext.get('txtNo_CSARForm').getValue() == '')
	{
		ShowPesanWarning_CSARForm('Data belum disimpan',modul);
		x=0;
	}
	return x;
};

function ShowPesanWarning_CSARForm(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};

function ShowPesanError_CSARForm(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR
		}
	);
};

function ShowPesanInfo_CSARForm(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO
		}
	);
};

function HapusBaris_CSARForm()
{
	if (cellSelectedDet_CSARForm.data.account != '' && cellSelectedDet_CSARForm.data.account != undefined)
	{
		Ext.Msg.show
		(
			{
			   title:'Hapus Baris',
			   msg: 'Apakah baris ini akan dihapus ?' + ' ' + 'Baris :'+ ' ' + (CurrentTR_CSARForm.row + 1) + ' dengan Account : '+ ' ' + cellSelectedDet_CSARForm.data.account ,
			   buttons: Ext.MessageBox.YESNO,
			   fn: function (btn) 
			   {			
				   if (btn =='yes') 
					{
						var line = gridDTLTR_CSARForm.getSelectionModel().selection.cell[0];
						var o = dsDTLTRList_CSARForm.getRange()[line].data;
						if (o.line != '' && o.line != undefined){
							dsDTLTRList_CSARForm.removeAt(CurrentTR_CSARForm.row);
					    	CalcTotal_CSARForm();
							HapusBaris_CSARFormDB(o.line);
					    	cellSelectedDet_CSARForm = undefined;
						} else{
							dsDTLTRList_CSARForm.removeAt(CurrentTR_CSARForm.row);
							CalcTotal_CSARForm();
							cellSelectedDet_CSARForm = undefined;
						}

					} 
			   },
			   icon: Ext.MessageBox.QUESTION
			}
		);
	}
	else
	{
	    dsDTLTRList_CSARForm.removeAt(CurrentTR_CSARForm.row);
	    CalcTotal_CSARForm();
	    cellSelectedDet_CSARForm = undefined;
	}
	
	
};

function TambahBaris_CSARForm()
{
	var records = new Array();
	records.push(new dsDTLTRList_CSARForm.recordType());
	dsDTLTRList_CSARForm.add(records);
	
	var row =dsDTLTRList_CSARForm.getCount()-1;
	gridDTLTR_CSARForm.startEditing(row, 1);
};


function DataDelete_CSARForm() 
{
	if (ValidasiEntry_CSARForm('Hapus Data') == 1 )
	{	
		Ext.Ajax.request({
			url: baseURL + "index.php/keuangan/functionPenerimaanPiutang/delete",
			params:  getParam_CSARForm(), 
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)					
				{
					ShowPesanInfo_CSARForm('Data berhasil di hapus','Hapus Data');
					
					RefreshDataFilter_CSARForm(false);
					AddNew_CSARForm();
				}
				else if (cst.success === false && cst.pesan === 0 )
				{
					ShowPesanWarning_CSARForm('Data tidak berhasil di hapus, data tersebut belum ada','Hapus Data');
				}
				else 
				{
					ShowPesanError_CSARForm('Data tidak berhasil di hapus','Hapus Data');
				}
			}
		})
	}
};

function getArrDetail_CSARForm()
{
	var x='';
	for(var i = 0 ; i < dsDTLTRList_CSARForm.getCount();i++)
	{
		var y='';
		var z='@@##$$@@';
		
		// if (DataAddNew_CSARForm === true)
		if (dsDTLTRList_CSARForm.data.items[i].data.CSARD_LINE==="")
		{
			y = "Line=" + 0 
		}
		else
		{	
			y = 'Line=' + dsDTLTRList_CSARForm.data.items[i].data.CSARD_LINE
		}		
		y += z + 'ACCOUNT='+dsDTLTRList_CSARForm.data.items[i].data.ACCOUNT
		y += z + 'DESC=' + dsDTLTRList_CSARForm.data.items[i].data.DESC
		y += z + 'VALUE=' + dsDTLTRList_CSARForm.data.items[i].data.VALUE
		y += z + 'POSTED=' + dsDTLTRList_CSARForm.data.items[i].data.POSTED
		if (i === (dsDTLTRList_CSARForm.getCount()-1))
		{
			x += y 
		}
		else
		{
			x += y + '##[[]]##'
		}
	}		
	return x;
};

function getArrDetailAkdAdj_CSARForm(TglApprove)
{
	var x='';
	for(var i = 0 ; i < dsDTLTRListAkdAjd_CSARForm.getCount();i++)
	{
		var y='';
		var z='@@##$$@@';
		y += 'CSAR_NUMBER='+Ext.get('txtNo_CSARForm').getValue()
		y += z + 'CSAR_DATE='+Ext.get('dtpTanggal_CSARForm').getValue()
		y += z + 'ARO_NUMBER='+dsDTLTRListAkdAjd_CSARForm.data.items[i].data.ARO_NUMBER
		y += z + 'ARO_DATE='+ShowDateAkuntansi(dsDTLTRListAkdAjd_CSARForm.data.items[i].data.ARO_DATE)
		y += z + 'AROH_NUMBER='+dsDTLTRListAkdAjd_CSARForm.data.items[i].data.AROH_NUMBER
		y += z + 'AROH_DATE='+ShowDateAkuntansi(TglApprove)
		y += z + 'AMOUNT='+dsDTLTRListAkdAjd_CSARForm.data.items[i].data.AMOUNT
		y += z + 'PAID='+ getAmountGrid_CSARForm(Ext.get('txtTotal_CSARForm').getValue())  //dsDTLTRListAkdAjd_CSARForm.data.items[i].data.PAID
		y += z + 'REMAIN='+dsDTLTRListAkdAjd_CSARForm.data.items[i].data.REMAIN
		if (i === (dsDTLTRListAkdAjd_CSARForm.getCount()-1))
		{
			x += y 
		}
		else
		{
			x += y + '##[[]]##'
		}
	}		
	return x;
};

function CekArrKosong_CSARForm()
{
	var x=1;
	for(var i = 0 ; i < dsDTLTRList_CSARForm.getCount();i++)
	{
		if (dsDTLTRList_CSARForm.data.items[i].data.ACCOUNT=='' || dsDTLTRList_CSARForm.data.items[i].data.ACCOUNT==undefined){
			x=0;
		}

	}		
	return x;
};

function CalcTotal_CSARForm(idx)
{
    var total=0;
	var nilai=0;
	for(var i=0;i < dsDTLTRList_CSARForm.getCount();i++)
	{
		var o = dsDTLTRList_CSARForm.data.items[i].data;
		if(o.value != undefined){
			total += parseInt(o.value);
		} 
	}	
	Ext.getCmp('txtTotal_CSARForm').setValue(toFormat(total));
};

function CalcTotalDetailGrid_CSARForm()
{
    var total=0;
	var nilai=0;
	for(var i=0;i < dsDTLTRList_CSARForm.getCount();i++)
	{
		var o = dsDTLTRList_CSARForm.data.items[i].data;
		if(o.value != undefined){
			total += parseInt(o.value);
		} 
	}	
	return total;
};

function CalcTotalAkdAdj_CSARForm(idx)
{
    var total=Ext.num(0);
	var nilai=Ext.num(0);
	for(var i=0;i < dsDTLTRListAkdAjd_CSARForm.getCount();i++)
	{
		nilai=dsDTLTRListAkdAjd_CSARForm.data.items[i].data.AMOUNT;
		total += nilai;
	}
	Ext.get('txtTotalAkdAdj_CSARForm').dom.value=formatCurrencyDec(total);
};

function ButtonDisabled_CSARForm(mBol)
{
	Ext.get('btnTambahBaris').dom.disabled=mBol;
	Ext.get('btnHapusBaris').dom.disabled=mBol;
	Ext.get('btnSimpan').dom.disabled=mBol;
	Ext.get('btnSimpanKeluar').dom.disabled=mBol;
	Ext.get('btnHapus').dom.disabled=mBol;	
	Ext.get('btnApprove').dom.disabled=mBol;	
};


function HapusBaris_CSARFormDB(line) 
{	
	Ext.Ajax.request({
		url: baseURL + "index.php/keuangan/functionPenerimaanPiutang/deleteRow",
		params:{
			csar_number : Ext.get('txtNo_CSARForm').getValue(),
			csar_date : Ext.get('dtpTanggal_CSARForm').getValue(),
			amount : CalcTotalDetailGrid_CSARForm(),
			line : line
		} ,
		failure: function(o)
		{
			ShowPesanError_CSARForm('Hubungi Admin', 'Error');
		},	
		success: function(o) 
		{
			var cst = Ext.decode(o.responseText);
			if (cst.success === true) 
			{
				ShowPesanInfo_CSARForm("Data berhasil dihapus.","Information");
			}
			else 
			{
				ShowPesanError_CSARForm('Gagal melakukan penghapusan', 'Error');
			};
		}
	})
	
};

function getParam_CSARFormHapusBaris(Line) 
{
	var params = 
	{
		Table: 'viACC_CSAR',   		
		IS_DETAIL:1,
		CSAR_NUMBER:Ext.get('txtNo_CSARForm').getValue(),
		CSAR_DATE:Ext.get('dtpTanggal_CSARForm').getValue(),
		CSARD_LINE:Line,		
		// KATEGORI:KDkategori_CSARForm,
		// Amount: getAmount_CSARForm(Ext.get('txtTotal_CSARForm').getValue())
	};
	return params
};

function mComboPengembalian_CSARForm()
{
  var cboPengembalian_CSARForm = new Ext.form.ComboBox
	(
		{
			id:'cboPengembalian_CSARForm',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			hidden:true,
			fieldLabel: 'Pengembalian Dana',			
			width:80,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Ya'], [0, 'Tidak']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:'',
			listeners:  
			{
				'select': function(a,b,c)
				{   
					// selectCount_CSARForm=b.data.displayText ;
					// RefreshDataFilter_CSARForm(false);
				} 
			}
		}
	);
	return cboPengembalian_CSARForm;
};

function INPUTDetailSP3D_CSARForm()
{	
    var strKriteria_CSARForm
    strKriteria_CSARForm = " where NO_SP3D_RKAT='" + no_sp3d + "' "
	dsDTLTRList_CSARForm.load
	(
		{ 
			params: 
			{ 	
				Skip: 0,
				Take: 20,
				Sort: 'Line',
				Sortdir: 'ASC',
				target:'VIviewsp3d_lpj',
				param: strKriteria_CSARForm
			}
		}
	);	
	return dsDTLTRList_CSARForm;
}

function GetCustAcc_CSARForm(kd_customer)
{
	 Ext.Ajax.request
	 (
		{
			url: baseURL + "index.php/keuangan/functionGeneral/getInfoCustomer",
			params: 
			{
				kd_customer:kd_customer 
			},
			success: function(o) 
			{
			var cst = Ext.decode(o.responseText);
				if (cst.success === true )
				{
				 	StrAccCust_CSARForm = cst.Account;
				 	StrNmAccCust_CSARForm=cst.NamaAcc;
				 	StrDueDay_CSARForm=cst.DueDay;
				}
				else
				{
					ShowPesanInfo_CSARForm	('Customer tidak ditemukan','Customer');
				}
			}

		}
		);
};

////--------------FIND DIALOG-------------------------
var nowFind_CSARForm = new Date();
var selectcboOperator_CSARForm;
var selectcboField_CSARForm;
//var frmFindDlg_CSARForm= fnFindDlg_CSARForm();
//frmFindDlg_CSARForm.show();
var winFind_CSARForm;

function fnFindDlg_CSARForm()
{  	
    winFind_CSARForm = new Ext.Window
	(
		{ 
			id: 'winFindx_CSARForm',
			title: 'Find',
			closeAction: 'destroy',
			width:350,
			height: 200,					
			border: false,
			resizable:false,
			plain: true,
			layout: 'fit',
			iconCls: 'find',
			modal: true,
			items: [ItemFindDlg_CSARForm()]
			
		}
	);

	winFind_CSARForm.show();
	
    return winFind_CSARForm; 
};

function ItemFindDlg_CSARForm() 
{	
	var PnlFindDlg_CSARForm = new Ext.Panel
	(
		{ 
			id: 'PnlFindDlg_CSARForm',
			fileUpload: true,
			layout: 'anchor',
			// width:350,
			height: 90,
			anchor: '100%',
			bodyStyle: 'padding:5px',
			border: true,
			items: 
			[

				{
					xtype: 'compositefield',
					fieldLabel: '',
					anchor: '100%',
					items: 
					[
						{
							xtype: 'fieldset',
							title: '',
							width: 325,
							height: 125, 
							items: 
							[
								mComboFindField_CSARForm(),
								mComboOperator_CSARForm(),
								getItemFindKeyWord_CSARForm(),
								getItemFindTgl_CSARForm(),
								
				
							]
						}						
					]
				},
				{
					layout: 'hBox',
					border: false,
					defaults: { margins: '0 5 0 0' },
					style:{'margin-left':'21px','margin-top':'5px'},
					anchor: '100%',
					layoutConfig: 
					{
						padding: '3',
						pack: 'end',
						align: 'middle'
					},
					items: 
					[
						{
							xtype: 'button',
							text: 'Ok',
							width: 70,
							hideLabel: true,
							id: 'btnOkLap_CSARForm',
							handler: function() 
							{
								if (ValidasiReport_CSARForm()==1){
									RefreshDataFindRecord_CSARForm()
								}
								
							}
						},
						{
							xtype: 'button',
							text: 'Cancel',
							width: 70,
							hideLabel: true,
							id: 'btnCancelLap_CSARForm',
							handler: function() 
							{
								winFind_CSARForm.close();
							}
						}
					]
				}
			]
		}
	);
 
    return PnlFindDlg_CSARForm;
};

function RefreshDataFindRecord_CSARForm() 
{   
	var strparam='';
	if(selectcboOperator_CSARForm==1){

		if (selectcboField_CSARForm=="CSA.CSAR_DATE"){

			strparam=" WHERE "+selectcboField_CSARForm+" like '"+ ShowDateAkuntansi(Ext.getCmp('dtpTglFind_CSARForm').getValue()) + "%' "; 

		} else{
			strparam=" WHERE "+selectcboField_CSARForm+" like '"+ Ext.getCmp('txtKeyWordFind_CSARForm').getValue() + "%' "; 

		}		
		
	} else if (selectcboOperator_CSARForm==2){

		if (selectcboField_CSARForm=="CSA.CSAR_DATE"){

			strparam=" WHERE "+selectcboField_CSARForm+" = '"+ ShowDateAkuntansi(Ext.getCmp('dtpTglFind_CSARForm').getValue()) + "' "; 

		} else{
			strparam=" WHERE "+selectcboField_CSARForm+" = '"+ Ext.getCmp('txtKeyWordFind_CSARForm').getValue() + "' "; 

		}

	} else if (selectcboOperator_CSARForm==3){
		 
		if (selectcboField_CSARForm=="CSA.CSAR_DATE"){

			strparam=" WHERE "+selectcboField_CSARForm+" < '"+ ShowDateAkuntansi(Ext.getCmp('dtpTglFind_CSARForm').getValue()) + "' "; 

		} else{
			strparam=" WHERE "+selectcboField_CSARForm+" < '"+ Ext.getCmp('txtKeyWordFind_CSARForm').getValue() + "' ";

		}

	} else if (selectcboOperator_CSARForm==4){

		if (selectcboField_CSARForm=="CSA.CSAR_DATE"){

			strparam=" WHERE "+selectcboField_CSARForm+" > '"+ ShowDateAkuntansi(Ext.getCmp('dtpTglFind_CSARForm').getValue()) + "' "; 

		} else{
			strparam=" WHERE "+selectcboField_CSARForm+" > '"+ Ext.getCmp('txtKeyWordFind_CSARForm').getValue() + "' ";

		}
		
	} else if (selectcboOperator_CSARForm==5){
		if (selectcboField_CSARForm=="CSA.CSAR_DATE"){

			strparam=" WHERE "+selectcboField_CSARForm+" <= '"+ ShowDateAkuntansi(Ext.getCmp('dtpTglFind_CSARForm').getValue()) + "' "; 

		} else{
			strparam=" WHERE "+selectcboField_CSARForm+" <= '"+ Ext.getCmp('txtKeyWordFind_CSARForm').getValue() + "' ";

		}
		
		
	} else if (selectcboOperator_CSARForm==6){
		if (selectcboField_CSARForm=="CSA.CSAR_DATE"){

			strparam=" WHERE "+selectcboField_CSARForm+" >= '"+ ShowDateAkuntansi(Ext.getCmp('dtpTglFind_CSARForm').getValue()) + "' "; 

		} else{
			strparam=" WHERE "+selectcboField_CSARForm+" >= '"+ Ext.getCmp('txtKeyWordFind_CSARForm').getValue() + "' ";

		}
		
	} else if (selectcboOperator_CSARForm==7){
		if (selectcboField_CSARForm=="CSA.CSAR_DATE"){

			strparam=" WHERE "+selectcboField_CSARForm+" <> '"+ ShowDateAkuntansi(Ext.getCmp('dtpTglFind_CSARForm').getValue()) + "' "; 

		} else{
			strparam=" WHERE "+selectcboField_CSARForm+" <> '"+ Ext.getCmp('txtKeyWordFind_CSARForm').getValue() + "' ";

		}
		
	} 
	
    // {  
		// dsTRList_CSARForm.load
		// (
			// { 
				// params:  
				// {   
					// Skip: 0, 
					// Take: selectCount_CSARForm, 
					// Sort: 'CSAR_DATE', 
					// Sortdir: 'ASC', 
					// target:'viACC_CSAR',
					// param: strparam
				// }			
			// }
		// );        
    // }
	Ext.Ajax.request({
		url: baseURL + "index.php/keuangan/functionPenerimaanPiutang/getDataFind",
		params: {
			criteria:strparam
		},
		success: function(o) {
			var cst = Ext.decode(o.responseText);
			if (cst.success === true) 
			{
				dsTRList_CSARForm.removeAll();
				var recs=[],
					recType=dsTRList_CSARForm.recordType;
				for(var i=0; i<cst.listData.length; i++){
					recs.push(new recType(cst.listData[i]));						
				}
				dsTRList_CSARForm.add(recs);
				grListTR_CSARForm.getView().refresh();
			}
			else {
				ShowPesanError_CSARForm('Gagal menampilkan list penerimaan piutang!', 'Simpan Data');
			}
		}
	})
};


function ValidasiReport_CSARForm()
{
	var x=1;
	
	if(selectcboField_CSARForm == undefined || selectcboField_CSARForm == "")
	{
		ShowPesanWarningFindDlg_CSARForm('Field belum dipilih','Find');
		x=0;		
	};

	if(selectcboOperator_CSARForm ==undefined || selectcboOperator_CSARForm== "")
	{
		
		ShowPesanWarningFindDlg_CSARForm('Operator belum di pilih','Find');
		x=0;
			
	};

	if(Ext.getCmp('txtKeyWordFind_CSARForm').getValue()== "" && criteriaTanggalFindCSAR == false)
	{
		
		ShowPesanWarningFindDlg_CSARForm('Keyword belum di isi','Find');
		x=0;
			
	};

	return x;
};

function ShowPesanWarningFindDlg_CSARForm(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING,
		   width:180
		}
	);
};

function getItemFindTgl_CSARForm() 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
		{
			xtype: 'compositefield',
			fieldLabel: '-',
			anchor: '100%',
			items: 
			[
				 {
					columnWidth: 1,
					layout: 'form',
					border: false,
					labelAlign: 'left',
					labelWidth: 100,
					items:
					[
						{
							xtype: 'datefield',
							fieldLabel: 'Periode ',
							id: 'dtpTglFind_CSARForm',
							format: 'd/M/Y',
							disabled:true,
							value:now,
							width:190

						}
					]
		    	}
			]
		}
		   
		]
	}
    return items;
};

function getItemFindKeyWord_CSARForm() 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
		{
			xtype: 'compositefield',
			fieldLabel: '-',
			anchor: '100%',
			items: 
			[
				 {
					columnWidth: 1,
					layout: 'form',
					border: false,
					labelAlign: 'left',
					labelWidth: 100,
					items:
					[
						{
							xtype: 'textfield',
							fieldLabel: 'Key Word ',
							id: 'txtKeyWordFind_CSARForm',
							name: 'txtKeyWordFind_CSARForm',
							width:190
						}
					]
		    	}
			 
			]
		}
		   
		]
	}
    return items;
};

function mComboFindField_CSARForm()
{
	var cboFindField_CSARForm = new Ext.form.ComboBox
	(
		{
			id:'cboFindField_CSARForm',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih Field',
			width:190,
			fieldLabel: 'Field Name',	
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: [
							["CSA.CSAR_NUMBER", "Number"], 
							["CSA.CSAR_DATE", "Date"],
							["CSA.KD_CUSTOMER", "Cust Code"], 
							["C.CUSTOMER", "Customer"],
							["NOTES1", "Notes"]
						  ]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			listeners:  
			{
				'select': function(a,b,c)
				{
					selectcboField_CSARForm=b.data.Id;

					if (b.data.Id=="CSA.CSAR_DATE"){
						Ext.getCmp('txtKeyWordFind_CSARForm').disable();
						Ext.getCmp('dtpTglFind_CSARForm').enable();
						criteriaTanggalFindCSAR=true;
					} else{
						Ext.getCmp('dtpTglFind_CSARForm').disable();
						Ext.getCmp('txtKeyWordFind_CSARForm').enable();
						criteriaTanggalFindCSAR=false;
					}
				}

			}
			
		}
	);
	return cboFindField_CSARForm;
}


function mComboOperator_CSARForm()
{
	var cboFindOperator_CSARForm = new Ext.form.ComboBox
	(
		{
			id:'cboFindOperator_CSARForm',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			width:190,
			emptyText:'Pilih Operator',
			fieldLabel: 'Operator ',
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: 
					[
						[1, "Like"], 
						[2, "="],
						[3, "<"],
						[4, ">"],
						[5, "<="], 
						[6, ">="],
						[7, "<>"]
					]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			listeners:  
			{
				'select': function(a,b,c)
				{
					selectcboOperator_CSARForm=b.data.Id;
				}

			}
		}
	);
	return cboFindOperator_CSARForm;
}

function CekDataInput_CSARForm(){
		Ext.Ajax.request
	(
		{
			url: "./Module.mvc/ExecProc",
			params: 
			{
				UserID: 'Admin',
				ModuleID: 'clsProcessCekReceiveAR',
				Params:	Ext.get('txtNo_CSARForm').dom.value
			},
			success: function(o) 
			{												
				var cst = Ext.decode(o.responseText);
				if (cst.success === true )
				{	

					DataAddNew_CSARForm = false;
					tempstrKdUser=cst.KD_USER;
					Ext.get('txtNo_CSARForm').dom.readOnly=true;
					Ext.get('cboCustEntryARForm').dom.value = cst.CUSTOMER;
					selectCboEntryCustARForm = cst.KD_CUSTOMER;
					
					Ext.get('comboBayar_CSARForm').dom.value= cst.PAYMENT;
					selectBayar_CSARForm = cst.PAY_CODE;
					//alert()
					Ext.get('comboAktivaLancar_CSARForm').dom.value= cst.ACCOUNT + ' - '+cst.NAMAACCOUNT;
					selectAktivaLancar_CSARForm = cst.ACCOUNT;

				    Ext.get('txtNo_CSARForm').dom.value = cst.CSAR_NUMBER;
					Ext.getCmp('dtpTanggal_CSARForm').disable(); 
					//Ext.getCmp('dtpTanggal_CSARForm').readOnly=true;    
					Ext.get('txtTerimaDari_CSARForm').dom.value = cst.CUSTOMER;
					Ext.get('dtpTanggal_CSARForm').dom.value = ShowDateAkuntansi(cst.CSAR_DATE);   
					
					Ext.get('txtTotal_CSARForm').dom.value = formatCurrencyDec(cst.AMOUNT);
					Ext.get('txtCatatan_CSARForm').dom.value = cst.NOTES1;
					Ext.get('txtNoPembayaran_CSARForm').dom.value = cst.PAY_NO;
					Ext.getCmp('ChkApproveEntry_CSARForm').setValue(cst.APPROVE);
					Ext.getCmp('cboPengembalian_CSARForm').setValue('');//(rowdata.IS_PEMGEMBALIAN);	
					RefreshDataDetail_CSARForm(cst.csar_number,cst.csar_date);
					// GetCustAcc_CSARForm(cst.kd_customer);
					CekApp=cst.posted;

				}
				else
				{
					//ShowPesanWarning_PenerimaanMhs('Data tidak berhasil', 'Cash Out');
				}
			}

		}
	);
}

function LookUpAccount_AREntry(criteria)
{
	
	WindowLookUpAccount_AREntry = new Ext.Window
    (
        {
            id: 'pnlLookUpAccount_AREntry',
            title: 'Lookup Account',
            width:650,
            height: 350,
            border: false,
            resizable:false,
            plain: true,
            iconCls: 'icon_lapor',
            modal: true,
            items: [
				getGridListSearchAccount_AREntry()
			],
			listeners:
			{             
				activate: function()
				{
						
				},
				afterShow: function()
				{
					this.activate();

				},
				deactivate: function()
				{
					
				},
				close: function (){
					
				}
			}
        }
    );

    WindowLookUpAccount_AREntry.show();
	getListSearchAccount_AREntry(criteria);
};

function getGridListSearchAccount_AREntry(){
	var fldDetail = ['account','name','nama_parent'];
	dsGridListAccount_AREntry = new WebApp.DataStore({ fields: fldDetail });
	
    GridListAccountColumnModelAREntry =  new Ext.grid.ColumnModel([
        new Ext.grid.RowNumberer(),
		{
			dataIndex		: 'account',
			header			: 'No. Account',
			width			: 80,
			menuDisabled	: true,
        },
		{
			dataIndex		: 'name',
			header			: 'Nama',
			width			: 140,
			menuDisabled	: true,
        },
		{
			dataIndex		: 'nama_parent',
			header			: 'Parent',
			width			: 120,
			menuDisabled	: true,
        },
	]);
	
	
	GridListAccount_AREntry= new Ext.grid.EditorGridPanel({
		id			: 'GrdListPencarianAccount_AREntry',
		stripeRows	: true,
		width		: 640,
		height		: 270,
        store		: dsGridListAccount_AREntry,
        border		: true,
        frame		: false,
        autoScroll	: true,
        cm			: GridListAccountColumnModelAREntry,
		selModel: new Ext.grid.RowSelectionModel({
			singleSelect: true,
			listeners:
			{
				rowselect: function(sm, row, rec)
				{
					currentRowSelectionSearchAccountAREntry = undefined;
					currentRowSelectionSearchAccountAREntry = dsGridListAccount_AREntry.getAt(row);
				}
			}
		}),
		listeners	: {
			rowclick: function( $this, rowIndex, e )
			{
				
    		},
			celldblclick: function(gridView,htmlElement,columnIndex,dataRecord){
				var line	= gridDTLTR_CSARForm.getSelectionModel().selection.cell[0];
				dsDTLTRList_CSARForm.getRange()[line].data.cito="Tidak";
				dsDTLTRList_CSARForm.getRange()[line].data.account=currentRowSelectionSearchAccountAREntry.data.account;
				dsDTLTRList_CSARForm.getRange()[line].data.name=currentRowSelectionSearchAccountAREntry.data.name;
				dsDTLTRList_CSARForm.getRange()[line].data.desc=Ext.get('txtCatatan_CSARForm').getValue();
				dsDTLTRList_CSARForm.getRange()[line].data.value=0,00;
				
				gridDTLTR_CSARForm.getView().refresh();
				gridDTLTR_CSARForm.startEditing(line, 4);	
				
				WindowLookUpAccount_AREntry.close();
			},
			'keydown' : function(e){
				if(e.getKey() == 13){
					var line	= gridDTLTR_CSARForm.getSelectionModel().selection.cell[0];
					dsDTLTRList_CSARForm.getRange()[line].data.cito="Tidak";
					dsDTLTRList_CSARForm.getRange()[line].data.account=currentRowSelectionSearchAccountAREntry.data.account;
					dsDTLTRList_CSARForm.getRange()[line].data.name=currentRowSelectionSearchAccountAREntry.data.name;
					dsDTLTRList_CSARForm.getRange()[line].data.desc=Ext.get('txtCatatan_CSARForm').getValue();
					dsDTLTRList_CSARForm.getRange()[line].data.value=0,00;
					
					gridDTLTR_CSARForm.getView().refresh();
					gridDTLTR_CSARForm.startEditing(line, 4);	
					
					WindowLookUpAccount_AREntry.close();
				}
			},
		},
		viewConfig	: {forceFit: true}
    });
	return GridListAccount_AREntry;
}


function getListSearchAccount_AREntry(criteria){
	Ext.Ajax.request ({
		url: baseURL + "index.php/keuangan/functionGeneral/getListAccount",
		params: {
			criteria:criteria
		},
		failure: function(o)
		{
			Ext.Msg.show({
				title: 'Error',
				msg: 'Error menampilkan list account. Hubungi Admin!',
				buttons: Ext.MessageBox.OK,
				fn: function (btn) {
					if (btn == 'ok')
					{
						var line	= gridDTLTR_CSARForm.getSelectionModel().selection.cell[0];
						gridDTLTR_CSARForm.startEditing(line, 1);	
					}
				}
			});
		},	
		success: function(o) 
		{   
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				if(cst.totalrecords == 0 ){
					Ext.Msg.show({
						title: 'Information',
						msg: 'Tidak ada account yang sesuai atau kriteria account kurang!',
						buttons: Ext.MessageBox.OK,
						fn: function (btn) {
							if (btn == 'ok')
							{
								var line	= gridDTLTR_CSARForm.getSelectionModel().selection.cell[0];
								gridDTLTR_CSARForm.startEditing(line, 4);
								WindowLookUpAccount_AREntry.close();
							}
						}
					});
				} else{
					dsGridListAccount_AREntry.removeAll();
					var recs=[],
						recType=dsGridListAccount_AREntry.recordType;
					for(var i=0; i<cst.ListDataObj.length; i++){
						recs.push(new recType(cst.ListDataObj[i]));						
					}
					dsGridListAccount_AREntry.add(recs);
					GridListAccount_AREntry.getView().refresh();
					GridListAccount_AREntry.getSelectionModel().selectRow(0);
					GridListAccount_AREntry.getView().focusRow(0);
				}
				
			} else {
				Ext.Msg.show({
					title: 'Error',
					msg: 'Error menampilkan list account. Hubungi Admin!',
					buttons: Ext.MessageBox.OK,
					fn: function (btn) {
						if (btn == 'ok')
						{
							var line	= gridDTLTR_CSARForm.getSelectionModel().selection.cell[0];
							gridDTLTR_CSARForm.startEditing(line, 1);	
						}
					}
				});
			};
		}
	});
}


function viewHistoryApproved(){
	winFormLookup_HistoryApprovedAR = new Ext.Window
	(
		{
		    id: 'winFormLookup_HistoryApprovedAR',
		    title: 'Form Approve',
		    closeAction: 'destroy',
		    closable: true,
		    width: 650,
		    height: 480,
		    border: false,
		    plain: true,
		    resizable: false,
		    layout: 'border',
		    iconCls: 'find',
		    modal: true,
			autoScroll: true,
		    items: [getItemFormLookup_HistoryApprovedAR()],
			fbar:
			[
				
				{ xtype: 'label', cls: 'left-label', width: 110, text: 'Jumlah :' },
				{
					xtype:'textfield',
					name: 'txtTotal_HistoryApprovedAR',
					id: 'txtTotal_HistoryApprovedAR',
					width:100,
					readOnly:true,
					style: 'font-weight: bold;text-align: right'
					// hidden:true
				},
				
			],
		}
	);
	
	winFormLookup_HistoryApprovedAR.show();
}

function getItemFormLookup_HistoryApprovedAR()
{
	var pnlButtonLookup_HistoryApprovedAR = new Ext.Panel
	(
		{
			id: 'pnlButtonLookup_HistoryApprovedAR',
			layout: 'hbox',
			border: false,
			region: 'south',
			defaults: { margins: '0 5 0 0' },
			style: { 'margin-left': '4px', 'margin-top': '3px' },
			anchor: '96.5%',
			layoutConfig:
			{
				padding: '3',
				pack: 'end',
				align: 'middle'
			},
			      
			listeners: 
			{
				activate: function()
				{		
					CalcTotal_HistoryApprovedAR();
					CheckboxLineSetTrue(false);
				}												
			}	
		}
	);
	
	var frmListLookup_HistoryApprovedAR = new Ext.Panel
	(
		{
			id: 'frmListLookup_HistoryApprovedAR',
			layout: 'form',
			region: 'center',
			autoScroll: true,
			items: 
			[
				getGridListLookup_HistoryApprovedAR(),
				pnlButtonLookup_HistoryApprovedAR,
			],
		}
	);
	
	return frmListLookup_HistoryApprovedAR;
}
function getListHistoryApproved_HistoryApprovedAR(){	
	Ext.Ajax.request({
		url: baseURL + "index.php/keuangan/functionPenerimaanPiutang/getFakturPiutangApproved",
		params: {
			csar_number : Ext.getCmp('txtNo_CSARForm').getValue(),
			csar_date : Ext.getCmp('dtpTanggal_CSARForm').getValue()
		},
		success: function(o) {
			var cst = Ext.decode(o.responseText);
			if (cst.success === true) 
			{
				dsGLLookup_HistoryApprovedAR.removeAll();
				var recs=[],
				recType=dsGLLookup_HistoryApprovedAR.recordType;
				for(var i=0; i<cst.total_record; i++){
					recs.push(new recType(cst.listData[i]));						
				}
				dsGLLookup_HistoryApprovedAR.add(recs);
				gridListLookup_HistoryApprovedAR.getView().refresh();
			}
			else {
				ShowPesanError_CSARForm('Gagal membaca list detail approved!' , 'Error');
			}
		}
	})
}

function getGridListLookup_HistoryApprovedAR()
{
	getListHistoryApproved_HistoryApprovedAR();
	var fields_HistoryApprovedAR = 
	[
		'nomor','tanggal','cust_code','amount','paid','remain','notes','due_date','posted','jenis_piu','type'
	];
	dsGLLookup_HistoryApprovedAR = new WebApp.DataStore({ fields: fields_HistoryApprovedAR });
	
	gridListLookup_HistoryApprovedAR = new Ext.grid.EditorGridPanel
	(
		{
			stripeRows: true,
			id: 'gridListLookup_HistoryApprovedAR',
			store: dsGLLookup_HistoryApprovedAR,
			height:353,
			anchor: '100% 98%',
			columnLines:true,
			bodyStyle: 'padding:0px',
			border: true,
			frame: true,
			plugins: [new Ext.ux.grid.FilterRow()],
			viewConfig : 
			{
				forceFit: true
			},				
			sm: new Ext.grid.RowSelectionModel
			(
				{ 
					singleSelect: false,
					listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							// selectedrowGLLookup_HistoryApprovedAR = dsGLLookup_HistoryApprovedAR.getAt(row);
						}						
					}
				}
			),
			cm: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{ 
						id: 'colNoGLLookup_HistoryApprovedAR',
						header: 'No. Piutang',
						dataIndex: 'nomor',
						width:130,	
						renderer: function(value, cell) 
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
							return str;
						},
						filter: {}						
					},
					{ 
						id: 'colTGLoGLLookup_HistoryApprovedAR',
						header: 'Tanggal',
						dataIndex: 'tanggal',
						width:100,	
						renderer: function(v, params, record) 
						{
							return ShowDate(record.data.tanggal);
						},
						filter: {}					
					},
					{ 
						id: 'colJMLGLLookupHistoryApprovedAR',
						header: 'Amount',
						dataIndex: 'amount',
						width:100,	
						align:'right',						
						renderer: function(v, params, record) 
						{
							var str = "<div style='white-space:normal;padding:2px 20px 2px 2px'>" + formatCurrencyDec(record.data.amount) + "</div>";
							return str;
							 
						}	
					}
					,{ 
						id: 'colPaidGLLookupHistoryApprovedAR',
						name: 'colPaidGLLookupHistoryApprovedAR',
						header: 'Paid',
						dataIndex: 'paid',
						width:100,
						xtype:'numbercolumn',
						align:'right'
					},
					{ 
						id: 'colRemainGLLookupHistoryApprovedAR',
						header: 'Remain',
						dataIndex: 'remain',
						width:100,	
						align:'right',						
						renderer: function(v, params, record) 
						{
							var str = "<div style='white-space:normal;padding:2px 20px 2px 2px'>" + formatCurrencyDec(record.data.remain) + "</div>";
							return str;
							 
						}	
					}
				]
			)
		}
	);
	
	return gridListLookup_HistoryApprovedAR;
}

function CalcTotal_HistoryApprovedAR()
{
    var total=0;
	var nilai=0;
	for(var i=0;i < dsGLLookup_HistoryApprovedAR.getCount();i++)
	{
		var o = dsGLLookup_HistoryApprovedAR.data.items[i].data;
		total += parseInt(o.paid);	
	}
	Ext.get('txtTotal_HistoryApprovedAR').dom.value=formatCurrencyDec(total);
};