
var now = new Date();
now.format('Y-m-d');
var dsSpesialisasipenerimaan;
var ListHasilpenerimaan;
var rowSelectedHasilpenerimaan;
var dsKelaspenerimaan;
var dsKamarpenerimaan;
var xz=0;
var dataSource_penerimaan;
var dataSource_detpenerimaan;
var dsPerawatpenerimaan;
var rowSelected_vipenerimaan;
var account_nci_penerimaan;
var creteria_penerimaan="";
var account_payment_penerimaan;
var  grid_detail_penerimaan;
var ds_penerimaan_grid_detail;
var Account_penerimaan_ds=new Ext.data.ArrayStore({id: 0,fields: ['text','account','name'],data: []});
var Pembayaran_penerimaan_ds=new Ext.data.ArrayStore({id: 0,fields: ['text','pay_code','payment'],data: []});
var tmptglawal;
var tmptglakhir;
var temporary_kode_unit_import;
var gridListHasilpenerimaan;
CurrentPage.page = getPanelpenerimaan(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
function getPanelpenerimaan(mod_id) {
    var Field = ['cso_number', 'personal', 'nama_account','account', 'alamat', 'kota', 'negara', 'kd_pos',
         'telepon1', 'telepon2', 'fax', 'amount','notes1'];
    dataSource_penerimaan = new WebApp.DataStore({
        fields: Field
    });

    load_penerimaan("");
   gridListHasilpenerimaan = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dataSource_penerimaan,
        anchor: '100%',
        //columnLines: false,
        autoScroll: true,
        border: false,
        frame: false,
        //sort: false,
        //autoHeight: false,
        //layout: 'fit',
        //region: 'center',
        height: 150,

        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
            listeners: {
                rowselect: function (sm, row, rec) {
                    rowSelected_vipenerimaan = dataSource_penerimaan.getAt(row);
                }
            }
        }),
        listeners: {
            rowdblclick: function (sm, ridx, cidx) {
                rowSelected_vipenerimaan = dataSource_penerimaan.getAt(ridx);
                console.log(rowSelected_vipenerimaan);
				tmpnotransaksi = '';
			   tmpkdpasien = '';
			   temporary_kode_unit_import = '';
			   tmpDataPasienImport = '';
                if (rowSelected_vipenerimaan !== undefined)
                {
					/*console.log(rowSelected_vipenerimaan);
					  if(rowSelected_vipenerimaan.data.statusl==='true' || rowSelected_vipenerimaan.data.statusl==='t')
					{
						ShowPesanWarningpenerimaan('Data yang sudah di posting tidak bisa di ubah', 'Posting');
									
					}else{*/
				   		HasilpenerimaanLookUp(rowSelected_vipenerimaan);
					//}
                }
                else
                {
                    HasilpenerimaanLookUp();
                }
            }
        },
        cm: new Ext.grid.ColumnModel(
                [
                    new Ext.grid.RowNumberer(),
					{
                        id: 'colCodeViewstatuspenerimaan',
                        header: 'Posting',
                        dataIndex: 'statusl',
                        sortable: true,
                        width: 90,
						style: 'text-align: center',
						renderer: function(value, metaData, record, rowIndex, colIndex, store)
                        {
                             switch (value)
                             { 
                                 case 't':
                                         metaData.css = 'StatusHijau'; //
                                         break;
                                 case 'f':
                                        metaData.css = 'StatusMerah'; // rejected

                                         break;
                             }
                             return '';
                        }
                    },{
                        id: 'colCodeViewHasilpenerimaan',
                        header: 'No Voucher',
                        dataIndex: 'cso_number',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 75
                    },
					{
                        id: 'colCodeViewHasilpenerimaan',
                        header: 'Tanggal',
                        dataIndex: 'cso_date',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 75,
						renderer: function(v, params, record){
							return ShowDate(record.data.cso_date);
						}
                    },
                    {
                        id: 'colNameViewHasilpenerimaan',
                        header: 'Terima Dari',
                        dataIndex: 'personal',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 90
                    },
                    {
                        id: 'colCatatanViewHasilpenerimaan',
                        header: 'Catatan',
                        dataIndex: 'notes1',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 350
                    },
                    {
                        id: 'colaccountViewHasilpenerimaan',
                        header: 'Akun',
                        dataIndex: 'account',
                        hidden:true,
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colnama_accountViewHasilpenerimaan',
                        header: 'Nama Akun',
                        hidden:true,
                        dataIndex: 'nama_account',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 170
                    },
                    {
                        id: 'col_Amaounts_penerimaan',
                        header: 'Jumlah',
                        dataIndex: 'amount',
                        sortable: false,
                        align : 'right',
                        hideable: false,
                        menuDisabled: true,
                        width: 70,
                        renderer : function (v, params, record)
                        {
                        return formatCurrency(record.data.amount);
                        },
                    }
                ]
                ),
        tbar: {
            xtype: 'toolbar',
            items: [
						{
							xtype: 'button',
							text: 'Baru',
							iconCls: 'AddRow',
							id: 'btnNew_penerimaan',
							handler: function ()
							{
								HasilpenerimaanLookUp();
							}
						} ,{
							xtype: 'button',
							text: 'Hapus',
							iconCls: 'remove',
							id: 'btnHapus_penerimaan',
							handler: function ()
							{
								
								if (rowSelected_vipenerimaan == undefined) {
									ShowPesanWarningpenerimaan('Harap Pilih terlebih dahulu data.', 'Gagal');
								}else if(rowSelected_vipenerimaan.data.statusl === 't' || rowSelected_vipenerimaan.data.statusl === 'true'){
									ShowPesanWarningpenerimaan('Data telah diposting.', 'Gagal');
								} else {
									Ext.Msg.show({
										title: nmHapusBaris,
										msg: 'Anda yakin akan menghapus data ini ?',
										buttons: Ext.MessageBox.YESNO,
										fn: function (btn) {
											if (btn == 'yes') {
												DeleteSemuaData_penerimaan(rowSelected_vipenerimaan);
													/* ds_penerimaan_grid_detail.removeAt(line);
													grid_detail_penerimaan.getView().refresh(); */
											}
										},
										icon: Ext.MessageBox.QUESTION
									});
								}
							}
						} ,
					]
        },
        viewConfig: {
            forceFit: true
        }
    });
    var FormDepanpenerimaan = new Ext.Panel({
        id: mod_id,
        closable: true,
        region: 'center',
        layout: 'form',
        title: 'Penerimaan',
        border: false,
        shadhow: true,
        autoScroll: false,
        iconCls: 'Request',
        margins: '0 5 5 0',
        items: [
            getPanelPencarianpenerimaan(),
            gridListHasilpenerimaan
        ],
        listeners: {
            'afterrender': function () {
                Ext.getCmp('cboStatus_PostingPenerimaan').setValue('Semua');
            }
        }
    });
    return FormDepanpenerimaan;
}
;
function getPanelPencarianpenerimaan() {
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 1190,
                height: 100,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'No Voucher '
                    },
                    {
                        x: 110,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 10,
                        xtype: 'textfield',
                        name: 'TxtCodepenerimaan',
                        id: 'TxtCodepenerimaan',
						width: 150,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
                                            load_penerimaan(Ext.getCmp('TxtCodepenerimaan').getValue(),Ext.getCmp('TxtTgl_voucher_penerimaan').getValue(),Ext.getCmp('TxtTgl_voucher_penerimaan2').getValue());
                                        }

                                    }
                                }
                    },
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Tanggal '
                    },
                    {
                        x: 110,
                        y: 40,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 40,
                        xtype: 'datefield',
                        name: 'TxtTgl_voucher_penerimaan',
                        id: 'TxtTgl_voucher_penerimaan',
                        format:'d/M/Y',
                        width: 200,
						value :now,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
                                            load_penerimaan(Ext.getCmp('TxtCodepenerimaan').getValue(),Ext.getCmp('TxtTgl_voucher_penerimaan').getValue(),Ext.getCmp('TxtTgl_voucher_penerimaan2').getValue());
                                        }
                                    }
                                }
                    },
					{
                        x: 330,
                        y: 40,
                        xtype: 'datefield',
                        name: 'TxtTgl_voucher_penerimaan2',
                        id: 'TxtTgl_voucher_penerimaan2',
                        format:'d/M/Y',
                        width: 200,
						value :now,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
                                            load_penerimaan(Ext.getCmp('TxtCodepenerimaan').getValue(),Ext.getCmp('TxtTgl_voucher_penerimaan').getValue(),Ext.getCmp('TxtTgl_voucher_penerimaan2').getValue());
                                        }
                                    }
                                }
                    },
                    {
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Status Posting '
                    },
                    {
                        x: 110,
                        y: 70,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboStatus_PostingPenerimaan()
                ]
            }
        ]
    };
    return items;
}
;
function creteria_penerimaan2(){
var msec =new Date();
	msec.parse(" 2012-01-01");
	if (Ext.getCmp('TxtCodepenerimaan').getValue()!=="" && Ext.getCmp('TxtTgl_voucher_penerimaan').getValue()==="" )
	{
	creteria_penerimaan="lower(cso_number) like lower('%"+ Ext.getCmp('TxtCodepenerimaan').getValue()+"%')";			
	}
	if (Ext.getCmp('TxtCodepenerimaan').getValue()==="" && Ext.getCmp('TxtTgl_voucher_penerimaan').getValue()!=="" )
	{
	creteria_penerimaan="cso_date in('"+ msec +"')";
		
	}
	if (Ext.getCmp('TxtCodepenerimaan').getValue()!=="" && Ext.getCmp('TxtTgl_voucher_penerimaan').getValue()!=="" )
	{
	creteria_penerimaan="lower(cso_number) like lower('"+ Ext.getCmp('TxtCodepenerimaan').getValue()+"%') and cso_date in ('"+ Ext.getCmp('TxtTgl_voucher_penerimaan').getValue()+"')";
		
	}
	if (Ext.getCmp('TxtCodepenerimaan').getValue()==="" && Ext.getCmp('TxtTgl_voucher_penerimaan').getValue()==="" )
	{
	creteria_penerimaan="";
	}

	return creteria_penerimaan;
}
function HasilpenerimaanLookUp(rowdata) {
    FormLookUpdetailpenerimaan = new Ext.Window({
        id: 'gridHasilpenerimaan',
        title: 'Penerimaan',
        closeAction: 'destroy',
        width: 800,
        height: 600,
        border: false,
        resizable: false,
        plain: true,
        layout: 'fit',
        constrain: true,
        iconCls: 'Request',
        modal: true,
        items: [
				form_penerimaan(),
				],
        listeners: {
        }
    });
    account_nci_penerimaan.focus(false, 200);
    FormLookUpdetailpenerimaan.show();
    if (rowdata === undefined||rowdata === "") {
	    new_data_penerimaan();
    } else {
        datainit_penerimaan(rowdata);
    }

}
;
function form_penerimaan() {
    var Isi_form_penerimaan = new Ext.Panel
            (
                    {
                        id: 'form_penerimaan',
                        closable: true,
                        region: 'center',
                        layout: 'form',
                        itemCls: 'blacklabel',
                        bodyStyle: 'padding: 0px 0px 0px 0px',
                        border: true,
                        shadhow: true,
                        width: 800,
						height: 200,
						iconCls: '',
                        items:
                                [
                                    Panelpenerimaan(),
									Getgrid_detail_penerimaan()
                                ],
                        tbar:
                                {
                                    xtype: 'toolbar',
                                    items:
                                            [
												{
													xtype: 'button',
													text: 'Baru',
													iconCls: 'AddRow',
													id: 'btnNew_penerimaan2',
													handler: function ()
													{
														new_data_penerimaan();
													}
												},
                                                {
                                                    xtype: 'button',
                                                    text: 'Simpan',
                                                    iconCls: 'save',
                                                    id: 'btnSimpan_penerimaan',
                                                    handler: function ()
                                                    {
                                                    	var tmpcekjumlah = 0;
                                                    	// console.log(ds_penerimaan_grid_detail);
                                                    	for (var L=0; L<ds_penerimaan_grid_detail.getCount(); L++)
														{
															// console.log(ds_penerimaan_grid_detail.data.items[L].data.account);
															if (ds_penerimaan_grid_detail.data.items[L].data.account === 'Data Belum Termaping' || ds_penerimaan_grid_detail.data.items[L].data.account === '' || ds_penerimaan_grid_detail.data.items[L].data.account === null || ds_penerimaan_grid_detail.data.items[L].data.account === 'null') {
																tmpcekjumlah ++;
															}else{

															};
														}

														if (tmpcekjumlah === 1) {
															ShowPesanInfopenerimaan('Data Tidak Bisa Di Simpan Karena Ada Produk Yang Belum Termaping Dengan Account','Info')
														}else{
															SavingData_permintaan();
														}
                                                    	// console.log(tmpDataPasienImport);
                                                        // 
                                                    }
                                                },
                                                {
                                                    xtype: 'button',
                                                    text: 'Posting',
                                                    id: 'btnposting_penerimaan',
                                                    handler: function ()
                                                    {
														if (Ext.getCmp('TxtVoucherpenerimaan').getValue()==="")
														{
														ShowPesanWarningpenerimaan('Tidak dapat diposting data sebelumnya disave', 'Perhatian');		
														}else{
														Posting_LookUp_penerimaan()
														}
												
                                                    }
                                                },
                                                {
                                                    xtype: 'button',
                                                    text: 'Import',
                                                    id: 'btnImport_penerimaan',
                                                    handler: function ()
                                                    {
                                                        // if (Ext.getCmp('TxtVoucherpenerimaan').getValue()==="")
                                                        // {
                                                        // ShowPesanWarningpenerimaan('Tidak dapat diposting data sebelumnya disave', 'Perhatian');        
                                                        // }else{
                                                        ImportDataPenerimaanTunaiLookUp()
                                                        // }
                                                
                                                    }
                                                },
                                                 
                                            ]
                                }
                    }
            );
    return Isi_form_penerimaan;
}
;
var FormLook_Posting_penerimaan;
function Posting_LookUp_penerimaan(rowdata) {
	xz=0;
    FormLook_Posting_penerimaan = new Ext.Window({
        id: 'FormLook_Posting_penerimaan',
        title: 'Posting',
        closeAction: 'destroy',
        width: 400,
        height: 240,
        border: false,
        resizable: false,
        plain: true,
        layout: 'fit',
        constrain: true,
        iconCls: 'Request',
        modal: true,
        items: [
				Panelpenerimaan_posting(),
				],
        listeners: {
        },
		fbar:
			{
				xtype: 'toolbar',
				items:
						[
							{
								xtype: 'button',
								text: 'Posting',
								iconCls: '',
								id: 'btnSimpanpost_penerimaan',
								handler: function ()
								{
									PostData_penerimaan()
								}
							}
						]
			}
                    
    });
    FormLook_Posting_penerimaan.show();
    datainit_post_penerimaan(rowdata);
    
};
function parampost_penerimaan()
{
    var params =
            {
            no_voucher	:Ext.getCmp('Txt_post_Voucherpenerimaan').getValue(),
			catatan		:Ext.getCmp('Txt_Post_catatan_penerimaan').getValue(),
			};
   return params;
}
function PostData_penerimaan()
{
/* 	if (account_payment.getValue()===""){
	ShowPesanWarningpenerimaan('Type Bayar Belum diisi', 'Perhatian');	
	}else{ */
		Ext.Ajax.request
		(
			{	
				url: baseURL + "index.php/anggaran/penerimaan/post",
				params: parampost_penerimaan(),
				failure: function (o){
					load_penerimaan(Ext.getCmp('TxtCodepenerimaan').getValue(),Ext.getCmp('TxtTgl_voucher_penerimaan').getValue(),Ext.getCmp('TxtTgl_voucher_penerimaan2').getValue());
					ShowPesanWarningpenerimaan('Proses Simpan Tidak berhasil silahkan hubungi admin', 'Gagal');
				},
				success: function (o){
					load_penerimaan(Ext.getCmp('TxtCodepenerimaan').getValue(),Ext.getCmp('TxtTgl_voucher_penerimaan').getValue(),Ext.getCmp('TxtTgl_voucher_penerimaan2').getValue());
					var cst = Ext.decode(o.responseText);
					if (cst.success === true){
						ShowPesanInfopenerimaan('Proses Simpan Berhasil', 'Save');
						   Ext.getCmp('btnSimpan_penerimaan').setDisabled(true);
						   Ext.getCmp('btnposting_penerimaan').setDisabled(true);
						   Ext.getCmp('btnSimpanpost_penerimaan').setDisabled(true);
						   Ext.getCmp('btnImport_penerimaan').setDisabled(true);
						   FormLook_Posting_penerimaan.destroy();
							
						load_penerimaan_detail(Ext.getCmp('TxtVoucherpenerimaan').getValue());							
						
					}
					 else{
						if (cst.cari === true){
						loadMask.hide();
						ShowPesanWarningpenerimaan('Login Ulang User Tidak teridentifikasi', 'User');	
						}else{
						ShowPesanWarningpenerimaan('Proses Simpan Tidak berhasil silahkan hubungi admin', 'Gagal');
						}
					};
				}
			}
		);
//	}
};



function Panelpenerimaan_posting() {
    var items = new Ext.Panel
            (
                    {
                        title: '',
                        id: 'PanelDataPosting_penerimaan',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        bodyStyle: 'padding:0px 0px 0px 0px',
                        border: true,
                        autoscroll: true,
                        items: [
                            {
                                layout: 'absolute',
                                bodyStyle: 'padding: 5px 5px 5px 5px',
                                border: false,
                                width: 400,
                                height: 240,
                                anchor: '100% 100%',
                                items: [
                                    {
                                        x: 10,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'No Voucher '
                                    },
                                    {
                                        x: 110,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 10,
                                        xtype: 'textfield',
                                        name: 'Txt_post_Voucherpenerimaan',
                                        id: 'Txt_post_Voucherpenerimaan',
                                        width: 200,
                                        readOnly: false,
										disabled:true,
                        
                                    },
                                    {
                                        x: 10,
                                        y: 40,
                                        xtype: 'label',
                                        text: 'Tanggal '
                                    },
                                    {
                                        x: 110,
                                        y: 40,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 40,
                                        xtype: 'datefield',
                                        name: 'Txt_post_tanggal_penerimaan',
                                        id: 'Txt_post_tanggal_penerimaan',
										format: 'Y-m-d',
										value:now,
										disabled:true,
                                        width: 200,
                                    },
                                    {
                                        x: 10,
                                        y: 70,
                                        xtype: 'label',
                                        text: 'Catatan '
                                    },
                                    {
                                        x: 110,
                                        y: 70,
                                        xtype: 'label',
                                        text: ' : '
                                    },
									{
                                        x: 120,
                                        y: 70,
                                        xtype: 'textarea',
                                        name: 'Txt_Post_catatan_penerimaan',
                                        id: 'Txt_Post_catatan_penerimaan',
                                        width: 200,
										height : 50,
                                       // readOnly: false,
										//disabled:true,
                                    },
									{
                                        x: 10,
                                        y: 130,
                                        xtype: 'label',
                                        text: 'Total '
                                    },
                                    {
                                        x: 110,
                                        y: 130,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 130,
                                        xtype: 'textfield',
                                        name: 'Txt_post_total_penerimaan',
                                        id: 'Txt_post_total_penerimaan',
										style: 'text-align: right',
                                        width: 200,
                                        readOnly: false,
										disabled:true,
                                    }
								]
                            }
                        ]
                    });
    return items;
};

function datainit_post_penerimaan(rowdata)
{//
	Ext.getCmp('Txt_post_Voucherpenerimaan').setValue("P"+Ext.getCmp('TxtVoucherpenerimaan').getValue());
    Ext.getCmp('Txt_post_total_penerimaan').setValue(Ext.getCmp('Txttotal_jumlah_peneriman').getValue());
	Ext.getCmp('Txt_Post_catatan_penerimaan').setValue(Ext.getCmp('Txt_catatan_penerimaan').getValue());
}

function Panelpenerimaan() {    
	 account_nci_penerimaan= Nci.form.Combobox.autoComplete({
		        x: 120,
                y: 40,
				store	: Account_penerimaan_ds,
                keydown:function(text,e){
                    var nav=navigator.platform.match("Mac");
                    if (e.keyCode == 9 || e.keyCode == 13){
                         Ext.getCmp('TxtPopuPenerimapenerimaan').focus(false, 200);
                    }
                },
				select	: function(a,b,c){								
						Ext.getCmp('TxtPopupNamapenerimaan').setValue(b.data.name);
                        Ext.getCmp('TxtPopuPenerimapenerimaan').focus(false, 200);                        
				},
				insert	: function(o){
					return {
						account         : o.account,
						name 			: o.name,
						text			:  '<table style="font-size: 11px;"><tr><td width="50">'+o.account+'</td><td width="200">'+o.name+'</td></tr></table>'
					}
				},
                keypress:function(c,e){
                    console.log('a');
                    if (e.keyCode == 13 || e.keyCode == 9){
                        Ext.getCmp('TxtPopuPenerimapenerimaan').focus(false, 200);
                    }
                },
				url: baseURL + "index.php/anggaran/penerimaan/accounts",
				valueField: 'account',
				displayField: 'text',
                tabIndex: 1,
				listWidth: 300,
				width:100,
                enableKeyEvents:true,
                tabIndex: 1
			});
	var items = new Ext.Panel
            (
                    {
                        title: '',
                        id: 'PanelDatapenerimaan',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        bodyStyle: 'padding:0px 0px 0px 0px',
                        border: true,
                        autoscroll: true,
                        items: [
                            {
                                layout: 'absolute',
                                bodyStyle: 'padding: 5px 5px 5px 5px',
                                border: false,
                                width: 500,
                                height: 200,
                                anchor: '100% 100%',
                                items: [
                                    {
                                        x: 10,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'No Voucher '
                                    },
                                    {
                                        x: 110,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 10,
                                        xtype: 'textfield',
                                        name: 'TxtVoucherpenerimaan',
                                        id: 'TxtVoucherpenerimaan',
                                        width: 180,
                                        readOnly: false,
										disabled:true,
                        
                                    },
                                    {
                                        x: 310,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'Tanggal '
                                    },
                                    {
                                        x: 370,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 380,
                                        y: 10,
                                        xtype: 'datefield',
                                        name: 'Txt_tanggal_penerimaan',
                                        id: 'Txt_tanggal_penerimaan',
										format: 'Y-m-d',
										value:now,
										disabled:false,
                                        width: 100,
                                    },
                                    {
                                        x: 230,
                                        y: 40,
                                        xtype: 'label',
                                        text: 'Nama Akun '
                                    },
                                    {
                                        x: 290,
                                        y: 40,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 300,
                                        y: 40,
                                        xtype: 'textfield',
                                        name: 'TxtPopupNamapenerimaan',
                                        id: 'TxtPopupNamapenerimaan',
                                        width: 280,
										disabled:true,
                                    },
                                    {
                                        x: 10,
                                        y: 70,
                                        xtype: 'label',
                                        text: 'Terima Dari '
                                    },
                                    {
                                        x: 110,
                                        y: 70,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 70,
                                        xtype: 'textfield',
                                        name: 'TxtPopuPenerimapenerimaan',
                                        id: 'TxtPopuPenerimapenerimaan',
                                        width: 360,
                                        tabIndex: 2,
                                        readOnly: false,
                                        enableKeyEvents: true,
                                        tabIndex: 2,
                                        listeners:{
                                            keypress:function(c,e){
                                                if (e.keyCode == 13){
                                                    Ext.getCmp('cboKode_Pay_penerimaan').focus();
                                                }
                                            }
                                        }
                                    },
									{
                                        x: 285,
                                        y: 100,
                                        xtype: 'label',
                                        text: 'kode',
										hidden: true
                                    },
                                    {
                                        x: 330,
                                        y: 100,
                                        xtype: 'label',
                                        text: ' : ',
										hidden: true
                                    },
                                    
                                    {
                                        x: 10,
                                        y: 40,
                                        xtype: 'label',
                                        text: 'Akun '
                                    },
                                    {
                                        x: 110,
                                        y: 40,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    account_nci_penerimaan,
									{
                                        x: 10,
                                        y: 100,
                                        xtype: 'label',
                                        text: 'Cara Penerimaan'
                                    },
                                    {
                                        x: 110,
                                        y: 100,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    mComboKode_Pay_penerimaan(),
									{
                                        x: 280,
                                        y: 100,
                                        xtype: 'label',
                                        text: 'Nomor Pembayaran'
                                    },
                                    {
                                        x: 390,
                                        y: 100,
                                        xtype: 'label',
                                        text: ' : '
                                    },
									{
                                        x: 400,
                                        y: 100,
                                        xtype: 'textfield',
                                        name: 'TxtPopukode_xpaypenerimaan',
                                        id: 'TxtPopukode_xpaypenerimaan',
										//hidden: true,
                                        width: 240,
                                        tabIndex: 4,
                                        readOnly: false,
                                        enableKeyEvents: true,
                                        listeners:{
                                            keypress:function(c,e){
                                                if (e.keyCode == 13 || e.keyCode == 9){
                                                    Ext.getCmp('Txt_catatan_penerimaan').focus();
                                                }
                                            }
                                        }
                                    },
									{
                                        x: 10,
                                        y: 130,
                                        xtype: 'label',
                                        text: 'Catatan '
                                    },
                                    {
                                        x: 110,
                                        y: 130,
                                        xtype: 'label',
                                        text: ' : '
                                    },
									{
                                        x: 120,
                                        y: 130,
                                        xtype: 'textarea',
                                        name: 'Txt_catatan_penerimaan',
                                        id: 'Txt_catatan_penerimaan',
                                        width: 460,
                                        tabIndex: 5,
										height : 50,
                                        readOnly: false
                                    }
								]
                            }
                        ]
                    });
    return items;
}
;
function datainit_penerimaan(rowdata)
{
	xxx=new Date(rowdata.data.cso_date);
    Ext.getCmp('TxtVoucherpenerimaan').setValue(rowdata.data.cso_number);
    Ext.getCmp('TxtPopupNamapenerimaan').setValue(rowdata.data.nama_account);
    Ext.getCmp('Txt_tanggal_penerimaan').setValue(xxx);
    Ext.getCmp('TxtPopuPenerimapenerimaan').setValue(rowdata.data.personal);
    // Ext.getCmp('cboKode_Pay_penerimaan').setValue(rowdata.data.payment);
    account_nci_penerimaan.setValue(rowdata.data.account);
	// account_payment_penerimaan.setValue();
    tmpkodepay = rowdata.data.pay_code;
	Ext.getCmp('cboKode_Pay_penerimaan').setValue(rowdata.data.payment);
	Ext.getCmp('TxtPopukode_xpaypenerimaan').setValue(rowdata.data.pay_no);
	Ext.getCmp('Txt_catatan_penerimaan').setValue(rowdata.data.notes1);
	load_penerimaan_detail(rowdata.data.cso_number);
	
	if(rowdata.data.statusl==='true' || rowdata.data.statusl==='t')
    {
        // form
        Ext.getCmp('btnSimpan_penerimaan').setDisabled(true);
        Ext.getCmp('btnposting_penerimaan').setDisabled(true);
        Ext.getCmp('btnImport_penerimaan').setDisabled(true);
        //grid
        Ext.getCmp('btnbaris_penerimaan').setDisabled(true);
        Ext.getCmp('btnhapus_baris_penerimaan').setDisabled(true);
        Ext.getCmp('btncek_data_mapping_penerimaan').setDisabled(true);
    }else{
        // form
        Ext.getCmp('btnSimpan_penerimaan').setDisabled(false);
        Ext.getCmp('btnposting_penerimaan').setDisabled(false);
        Ext.getCmp('btnImport_penerimaan').setDisabled(false);
        //grid
        Ext.getCmp('btnbaris_penerimaan').setDisabled(false);
        Ext.getCmp('btnhapus_baris_penerimaan').setDisabled(false);
        Ext.getCmp('btncek_data_mapping_penerimaan').setDisabled(false);

    }
	
}

function new_data_penerimaan()
{	Ext.getCmp('btnSimpan_penerimaan').setDisabled(false);
	Ext.getCmp('btnposting_penerimaan').setDisabled(false);
	Ext.getCmp('btnImport_penerimaan').setDisabled(false);
	Ext.getCmp('btnbaris_penerimaan').setDisabled(false);
    Ext.getCmp('btnhapus_baris_penerimaan').setDisabled(false);
    Ext.getCmp('btncek_data_mapping_penerimaan').setDisabled(false);
   // console.log(rowdata.cso_number)
    Ext.getCmp('TxtVoucherpenerimaan').setValue();
    Ext.getCmp('TxtPopupNamapenerimaan').setValue();
    Ext.getCmp('Txt_tanggal_penerimaan').setValue(now);
    Ext.getCmp('TxtPopuPenerimapenerimaan').setValue();
    account_nci_penerimaan.setValue();
	// account_payment_penerimaan.setValue();
    // Ext.getCmp('cboKode_Pay_penerimaan').setValue('');
	Ext.getCmp('cboKode_Pay_penerimaan').setValue();
	Ext.getCmp('TxtPopukode_xpaypenerimaan').setValue();
	Ext.getCmp('Txt_catatan_penerimaan').setValue();
	ds_penerimaan_grid_detail.removeAll();
	grid_detail_penerimaan.getView().refresh();
	Ext.getCmp('Txttotal_jumlah_peneriman').setValue(0);
	
}


function SavingData_permintaan()
{ 
if (ds_penerimaan_grid_detail.getCount()===0){
	 ShowPesanWarningpenerimaan('Isi kolom dan baris didalam tabel', 'Perhatian');
}else{
	if (Ext.getCmp('TxtPopuPenerimapenerimaan').getValue()===""){
	ShowPesanWarningpenerimaan('Pengirim Belum diisi', 'Perhatian');	
	}
	else{
	if (account_nci_penerimaan.getValue()===""){
		 ShowPesanWarningpenerimaan('Akun Belum diisi', 'Perhatian');
	}else{
		if (Ext.getCmp('cboKode_Pay_penerimaan').getValue()===""){
		ShowPesanWarningpenerimaan('Type Bayar Belum diisi', 'Perhatian');	
		}else{
		    Ext.Ajax.request
			(
				{	
					url: baseURL + "index.php/anggaran/penerimaan/save",
					params: paramsaving_permintaan(),
					failure: function (o)
					{
						load_penerimaan(Ext.getCmp('TxtCodepenerimaan').getValue(),Ext.getCmp('TxtTgl_voucher_penerimaan').getValue(),Ext.getCmp('TxtTgl_voucher_penerimaan2').getValue());
						ShowPesanWarningpenerimaan('Proses Simpan Tidak berhasil silahkan hubungi admin', 'Gagal');
					},
					success: function (o)
					{
						//creteria_penerimaan2();
						load_penerimaan(Ext.getCmp('TxtCodepenerimaan').getValue(),Ext.getCmp('TxtTgl_voucher_penerimaan').getValue(),Ext.getCmp('TxtTgl_voucher_penerimaan2').getValue());
					
						var cst = Ext.decode(o.responseText);
						if (cst.success === true)
						{
							Ext.getCmp('TxtVoucherpenerimaan').setValue(cst.no_voucher);
							load_penerimaan_detail(cst.no_voucher);							
							ShowPesanInfopenerimaan('Proses Simpan Berhasil', 'Save');
						}
						 
						else
						{
							if (cst.cari === true){
							loadMask.hide();
							ShowPesanWarningpenerimaan('Login Ulang User Tidak teridentifikasi', 'User');	
							}else{
							ShowPesanWarningpenerimaan('Proses Simpan Tidak berhasil silahkan hubungi admin', 'Gagal');
							}
						};
					}
				}
			);
			
			}
		
	}

	}
	}
};
function paramsaving_permintaan()
{
    var params =
            {
            no_voucher	:Ext.getCmp('TxtVoucherpenerimaan').getValue(),
			pengirim	:Ext.getCmp('TxtPopuPenerimapenerimaan').getValue(),
			type_bayar	:tmpkodepay,
			tgl_cso		:Ext.getCmp('Txt_tanggal_penerimaan').getValue(),
			account		:account_nci_penerimaan.getValue(),
			notes  		:Ext.getCmp('Txt_catatan_penerimaan').getValue(),
			pay_no		:Ext.getCmp('TxtPopukode_xpaypenerimaan').getValue(),
			amount		:xz,
            datapasien  :tmpDataPasienImport,
			
			};
			params['jumlah']=ds_penerimaan_grid_detail.getCount()
			for (var L=0; L<ds_penerimaan_grid_detail.getCount(); L++)
			{
				params['account_detail'+L]		=ds_penerimaan_grid_detail.data.items[L].data.account;
				params['name_detail'+L]			=ds_penerimaan_grid_detail.data.items[L].data.name;
				params['description_detail'+L]	=ds_penerimaan_grid_detail.data.items[L].data.description;
				params['value'+L]				=ds_penerimaan_grid_detail.data.items[L].data.value;
				params['line'+L]				=ds_penerimaan_grid_detail.data.items[L].data.line;
			}
   
   return params;
								
										
								
	
	
}


function load_penerimaan(cso_number,tgl,tgl2){
	//dataSource_penerimaan
		Ext.Ajax.request({
			url: baseURL + "index.php/anggaran/penerimaan/select",
			params: {
				cso_number:cso_number,
				tgl:tgl,
				tgl2:tgl2,
                posting:selectCount_PostingPenerimaan
			},
			failure: function(o){  	
				loadMask.hide();
				ShowPesanWarningpenerimaan('Hubungi Admin', 'Error');
			},	
			success: function(o){
			var cst = Ext.decode(o.responseText);
			if (cst.success === true){
				dataSource_penerimaan.removeAll();
				var recs=[],
				recType=dataSource_penerimaan.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));
				}
				dataSource_penerimaan.add(recs);
				gridListHasilpenerimaan.getView().refresh();
			} 
			}
		})
}



function ShowPesanWarningpenerimaan(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING,
                        width: 250
                    }
            );
}
;

function ShowPesanInfopenerimaan(str, modul) {
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO,
                        width: 250
                    }
            );
}
;

function DeleteSemuaData_penerimaan(row)
{
	console.log(row);
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/anggaran/penerimaan/delete_cso",
                        params: {
							cso_number : row.data.cso_number,
							cso_date : row.data.cso_date
						},
                        failure: function (o){
							creteria_penerimaan2();
							
							loadMask.hide();
                            ShowPesanWarningpenerimaan('Hubungi Admin', 'Error');
                        },
                        success: function (o)
                        { 	//creteria_penerimaan2();
							
							var cst = Ext.decode(o.responseText);
                            if (cst.success === true){
								
								loadMask.hide();
                                ShowPesanInfopenerimaan('Data berhasil di hapus', 'Information');
                                load_penerimaan("");
							}
							else{
								 if (cst.tidak_ada_line === true){
								
								 ShowPesanWarningpenerimaan('Gagal menghapus data', 'Error');
								}else{
                                loadMask.hide();
                                
								}
							}
                            ;
                        }
                    }

            ); 
}
function DeleteData_penerimaan(line)
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/anggaran/penerimaan/delete",
                        params: paramdelete(line),
                        failure: function (o){
							//creteria_penerimaan2();
							load_penerimaan(Ext.getCmp('TxtCodepenerimaan').getValue(),Ext.getCmp('TxtTgl_voucher_penerimaan').getValue(),Ext.getCmp('TxtTgl_voucher_penerimaan2').getValue());
							loadMask.hide();
                            ShowPesanWarningpenerimaan('Hubungi Admin', 'Error');
                        },
                        success: function (o)
                        { 	//creteria_penerimaan2();
							load_penerimaan(Ext.getCmp('TxtCodepenerimaan').getValue(),Ext.getCmp('TxtTgl_voucher_penerimaan').getValue(),Ext.getCmp('TxtTgl_voucher_penerimaan2').getValue());
							var cst = Ext.decode(o.responseText);
                            if (cst.success === true){
								
								loadMask.hide();
                                ShowPesanInfopenerimaan('Data berhasil di hapus', 'Information');
                                 ds_penerimaan_grid_detail.removeAt(line);
                                 grid_detail_penerimaan.getView().refresh(); 
							}
							else{
								 if (cst.tidak_ada_line === true){
								
								 ds_penerimaan_grid_detail.removeAt(line);
                                 grid_detail_penerimaan.getView().refresh(); 
								}else{
                                loadMask.hide();
                                ShowPesanWarningpenerimaan('Gagal menghapus data', 'Error');
								}
							}
                            ;
                        }
                    }

            );
}
;
function paramdelete(linex)
{
    var params =
            {
            no_voucher	:Ext.getCmp('TxtVoucherpenerimaan').getValue(),
			
			};
			params['line']=ds_penerimaan_grid_detail.data.items[linex].data.line;
  return params;
}

function Getgrid_detail_penerimaan(data) {

    var fldDetail = ['description', 'account', 'name',  'value', 'line'];
    ds_penerimaan_grid_detail = new WebApp.DataStore({fields: fldDetail});
    grid_detail_penerimaan = new Ext.grid.EditorGridPanel({
        title: 'Penerimaan',
        id: 'grid_detail_penerimaan',
        stripeRows: true,
        store: ds_penerimaan_grid_detail,
        border: false,
        frame: false,
        height: 325,
		width: 815,
        anchor: '100%',
        autoScroll: true,
        sm: new Ext.grid.CellSelectionModel({
            singleSelect: true,
            listeners: {
                cellselect: function (sm, row, rec) {
                    /* var linepanatajasarwi = ds_penerimaan_grid_detail.getAt(row);
                    if (linepanatajasarwi.data.kd_pasien === undefined || linepanatajasarwi.data.kd_pasien === "")
                    {
                    } else {
                    } */

                }
            }
        }),
        cm: get_column_detail_penerimaan(),
		tbar:[
		{
                text: 'Tambah data',
                id: 'btnbaris_penerimaan',
                tooltip: nmLookup,
                iconCls: 'AddRow',
                handler: function () {

                    var records = new Array();
                    records.push(new ds_penerimaan_grid_detail.recordType());
                   //  recType=ds_penerimaan_grid_detail.recordType;
                   //  records.push(new recType({
                   //      description: '',
                   //      account:'',
                   //      name:'',
                   //      value:0
                   // }))
                    ds_penerimaan_grid_detail.add(records);
					
                }
        },{
                text: 'Hapus data',
                id: 'btnhapus_baris_penerimaan',
                tooltip: nmLookup,
                iconCls: 'RemoveRow',
                handler: function () {
					var line = grid_detail_penerimaan.getSelectionModel().selection.cell[0];
                    if (grid_detail_penerimaan.getSelectionModel().selection == null) {
                        ShowPesanWarningpenerimaan('Harap Pilih terlebih dahulu data.', 'Gagal');
                    } else {
                        Ext.Msg.show({
                            title: nmHapusBaris,
                            msg: 'Anda yakin akan menghapus data dengan Akun' + ' : ' + ds_penerimaan_grid_detail.getRange()[line].data.account,
                            buttons: Ext.MessageBox.YESNO,
                            fn: function (btn) {
                                if (btn == 'yes') {
									DeleteData_penerimaan(line);
                                        /* ds_penerimaan_grid_detail.removeAt(line);
                                        grid_detail_penerimaan.getView().refresh(); */
                                }
                            },
                            icon: Ext.MessageBox.QUESTION
                        });
                    }
                }
        },
        {
                text: 'Cek data Mapping Account Produk',
                id: 'btncek_data_mapping_penerimaan',
                tooltip: nmLookup,
                iconCls: 'Informasi',
                handler: function () {
                	
                	if (tmpnotransaksi === '') {
                		ShowPesanInfopenerimaan('Sumber Data Bukan Dari Proses Import Data','info');
                	}else{
                		CekDataMappingPenerimaanTunaiLookUp();
                        if (tmpunit === 'Rawat Inap') {
                            refeshCekDataImporPenerimaanRWI(tmpnotransaksi,tmpkdpasien);
                        }else{
                            refeshCekDataImporPenerimaan(tmpnotransaksi,tmpkdpasien);
                        }
                		
                	}
                	
                	// console.log();
                }
        } 			
		
		],
		bbar:[		
					{
                
                        xtype: 'label',
                        text: '*Warna Merah Berarti Belum Termaping',
                        cls: 'my-label-style',
                    },
					{
						xtype: 'tbspacer',
						width: 410
					},
					{
                
                        xtype: 'label',
                        text: 'Total : '
                    },
                   
					{
                        xtype: 'textfield',
                        name: 'Txttotal_jumlah_peneriman',
                        id: 'Txttotal_jumlah_peneriman',
						width: 150,
						disabled:true,
						style: 'text-align: right',
						value: 0,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {

                                        }

                                    }
                                }
                    }
		
		]
       /*  viewConfig: {forceFit: true} */
    });

    return grid_detail_penerimaan;
}
function get_column_detail_penerimaan() {
  return new Ext.grid.ColumnModel([
        new Ext.grid.RowNumberer(),
		
		{
            id: Nci.getId(),
            header: 'Akun',
            width: 150,
            hidden: false,
            menuDisabled: true,
            dataIndex: 'account',
			editor: new Nci.form.Combobox.autoComplete({
		        x: 120,
                y: 40,
				store	: Account_penerimaan_ds,
				select	: function(a,b,c){
								var line = grid_detail_penerimaan.getSelectionModel().selection.cell[0];
								console.log(grid_detail_penerimaan); //name=b.data.name;
								//Ext.getCmp('TxtPopupNamapenerimaan').setValue(b.data.name);
								ds_penerimaan_grid_detail.data.items[line].data.name=b.data.name;
								//account_nci_penerimaan.setValue(b.data.account);
								},
				insert	: function(o){
					return {
						account         : o.account,
						name 			: o.name,
						text			: '<table style="font-size: 11px;"><tr><td width="50">'+o.account+'</td><td width="200">'+o.name+'</td></tr></table>'
					}
				},
				url: baseURL + "index.php/anggaran/penerimaan/accounts",
				valueField: 'account',
				displayField: 'text',
				listWidth: 300,
				width:100
			}),
			 renderer : function(value, meta) {
					if(value === 'null' || value === null) {
					        meta.style = "background-color:red;width: 150;";

					    }
					    return value;
			}
        }, {
            id: Nci.getId(),
            header: 'Nama',
            dataIndex: 'name',
            sortable: true,
            width: 250,
           renderer : function(value, meta) {
					if(value === 'null' || value === null) {
					        meta.style = "background-color:red;width: 250;";
					    }
					    return value;
			}
            // align: 'center',
        }, {
            id: Nci.getId(),
            header: 'Deskripsi',
            dataIndex: 'description',
            width: 250,
            menuDisabled: true,
            hidden: false,
			renderer : function(value, meta) {
					if(value === 'null' || value === null) {
					        meta.style = "background-color:red;width: 250;";
					    }
					    return value;
			},
			editor:{	
			xtype:'textfield',
			allowBlank:false
			}
		},{
            id: Nci.getId(),
            header: 'Nilai',
            dataIndex: 'value',
			value:1,
            hidden: false,
            menuDisabled: true,
            width: 100,
			align:'right',
            editor: new Ext.form.NumberField ({
						allowBlank: false
					}),
			renderer : function(value, meta) {
				total();
				if(value === 'null' || value === null) {
			        meta.style = "background-color:red;width: 100;";
			    }
				return formatCurrency(value);
			}
        },
   //      {	
			// id: Nci.getId(),
   //          header: 'No Anggaran',
   //          dataIndex: 'tgl_transaksi',
   //          width: 130,
   //          menuDisabled: true,
   //          renderer : function(value, meta) {
			// 	if(value === 'null' || value === null) {
			// 	        meta.style = "background-color:red;";
			// 	    }
			// 	    return value;
			// }
           
   //      },
   //      {
   //          id: Nci.getId(),
   //          header: 'Nama Anggaran',
   //          dataIndex: 'namadok',
   //          menuDisabled: true,
   //          hidden: false,
   //          width: 150,
   //          renderer : function(value, meta) {
			// 	if(value === 'null' || value === null) {
			// 	        meta.style = "background-color:red;";
			// 	    }
			// 	    return value;
			// }
   //      },
    ]);
}
function total(){
 xz=0;
for (var L=0; L<ds_penerimaan_grid_detail.getCount(); L++)
			{
				if (ds_penerimaan_grid_detail.data.items[L].data.value==="" ||
				 ds_penerimaan_grid_detail.data.items[L].data.value==undefined||
				 ds_penerimaan_grid_detail.data.items[L].data.value==="NaN" )
				{
					ds_penerimaan_grid_detail.data.items[L].data.value=0;
					xz=parseFloat(xz)+parseFloat(ds_penerimaan_grid_detail.data.items[L].data.value);
				}else{
					
					xz=parseFloat(xz)+parseFloat(ds_penerimaan_grid_detail.data.items[L].data.value);
				}
			}
			Ext.getCmp('Txttotal_jumlah_peneriman').setValue(formatCurrency(xz));
}

function load_penerimaan_detail(criteria){
	//dataSource_penerimaan
		Ext.Ajax.request({
			url: baseURL + "index.php/anggaran/penerimaan/select_detail",
			params: {criteria:criteria},
			failure: function(o){  	
				loadMask.hide();
				ShowPesanWarningpenerimaan('Hubungi Admin', 'Error');
			},	
			success: function(o){
			var cst = Ext.decode(o.responseText);
			if (cst.success === true){
				ds_penerimaan_grid_detail.removeAll();
				var recs=[],
				recType=ds_penerimaan_grid_detail.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));
				}
				ds_penerimaan_grid_detail.add(recs);
				grid_detail_penerimaan.getView().refresh();
			} 
			}
		})
}
var selectCount_PostingPenerimaan = 'Semua';
function mComboStatus_PostingPenerimaan()
{
    var cboStatus_PostingPenerimaan = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 70,
                        id: 'cboStatus_PostingPenerimaan',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        width: 150,
                        emptyText: '',
                        fieldLabel: 'Jenis',
                        store: new Ext.data.ArrayStore
                                (
                                        {
                                            id: 0,
                                            fields:
                                                    [
                                                        'Id',
                                                        'displayText'
                                                    ],
                                            data: [[1, 'Semua'], [2, 'Posting'], [3, 'Belum Posting']]
                                        }
                                ),
                        valueField: 'Id',
                        displayField: 'displayText',
                        value: selectCount_PostingPenerimaan,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectCount_PostingPenerimaan = b.data.displayText;
                                        load_penerimaan(Ext.getCmp('TxtCodepenerimaan').getValue(),Ext.getCmp('TxtTgl_voucher_penerimaan').getValue(),Ext.getCmp('TxtTgl_voucher_penerimaan2').getValue());
                                    }
                                }
                    }
            );
    return cboStatus_PostingPenerimaan;
}
;
var tmpkodepay;
function mComboKode_Pay_penerimaan()
{
    var Field = ['KODE', 'NAMA'];

    dsKode_Pay_penerimaan = new WebApp.DataStore({fields: Field});
    dsKode_Pay_penerimaan.load
        (
            {
            params:
                    {
                        Skip: 0,
                        Take: 1000,
                        Sort: 'pay_code',
                        Sortdir: 'ASC',
                        target: 'Pay_ACC',
                        param: "" //+"~ )"
                    }
            }
        );

    var cboKode_Pay_penerimaan = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 100,
                        id: 'cboKode_Pay_penerimaan',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        emptyText: '',
                        align: 'Right',
                        editable: false,
                        width: 150,
                        listWidth: 150,
                        store: dsKode_Pay_penerimaan,
                        valueField: 'KODE',
                        displayField: 'NAMA',
                        selectOnFocus: true,
                        enableKeyEvents:true,
                        tabIndex: 3,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        tmpkodepay = b.data.KODE;
                                        // Ext.getCmp('TxtPopunama_type_penerimaan').setValue(b.data.NAMA);
                                        Ext.getCmp('TxtPopukode_xpaypenerimaan').focus();
                                    },
                                    keypress:function(c,e){
                                        if (e.keyCode == 13){
                                            Ext.getCmp('TxtPopukode_xpaypenerimaan').focus();
                                        } else if (e.keyCode == 9){
                                             Ext.getCmp('TxtPopukode_xpaypenerimaan').focus();
                                        }
                                    },

                                }
                    }
            );

    return cboKode_Pay_penerimaan;
}
;

var FormLookUpImportDataPenerimaanTunai;
function ImportDataPenerimaanTunaiLookUp() {
    FormLookUpImportDataPenerimaanTunai = new Ext.Window({
        id: 'WindowFormLookUpImportDataPenerimaanTunai',
        title: 'Import Data',
        closeAction: 'destroy',
        width: 710,
        height: 500,
        border: false,
        resizable: false,
        plain: true,
        layout: 'fit',
        constrain: true,
        iconCls: 'Request',
        modal: true,
        items: [
                form_ImportDataPenerimaanTunai(),
                ],
        listeners: {
             show: function() {
               tmpnotransaksi = '';
			   tmpkdpasien = '';
			   temporary_kode_unit_import = '';
			   tmpDataPasienImport = '';
            },
			'destroy': function(){
				tmpnotransaksi = tmpclosenotransaksi;
				tmpkdpasien = tmpclosekdpasien;
				temporary_kode_unit_import = temporary_closekd_unit_import;
				tmpDataPasienImport = tmpcloseDataPasienImport;
			}
        }
    });
    FormLookUpImportDataPenerimaanTunai.show();
}
;

var tmpnotransaksi = '';
var tmpkdpasien = '';
var tmpDataPasienImport = '';
function form_ImportDataPenerimaanTunai() {
    var Isi_form_ImportDataPenerimaanTunai = new Ext.Panel
    (
        {
            id: 'Isi_form_ImportDataPenerimaanTunai',
            closable: true,
            region: 'center',
            // layout: 'form',
			layout:{
				type:'vbox',
				align:'stretch'
			},
            itemCls: 'blacklabel',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: true,
            shadhow: true,
            width: 310,
            height: 200,
            iconCls: '',
            items:
                    [
                        Panel_Import_DataPenerimaanTunai(),
                        Getgrid_detail_Import_DataPenerimaanTunai()
                    ],
            tbar:
                {
                    xtype: 'toolbar',
                    items:
                            [
                                {
                                    xtype: 'button',
                                    text: 'Import',
                                    iconCls: 'save',
                                    id: 'btnSimpan_Import_penerimaan',
                                    handler: function ()
                                    {
                                    	
                                    	//get data yang di pilih atau di centang
                                    	var SelectedCheckbox=grid_detail_Import_DataPenerimaanTunai.getSelectionModel();
										for(i=0;i<SelectedCheckbox.selections.length;i++){
										    tmpnotransaksi += "'" + SelectedCheckbox.selections.items[i].data.no_transaksi + "',";
											tmpkdpasien += "'" + SelectedCheckbox.selections.items[i].data.kd_pasien + "',";
										    tmpDataPasienImport += "'" + SelectedCheckbox.selections.items[i].data.no_transaksi + "'," + "'" + SelectedCheckbox.selections.items[i].data.kd_kasir + "'," + "'" + SelectedCheckbox.selections.items[i].data.tgl_transaksi + "',|==|";
										    temporary_kode_unit_import += "'" + SelectedCheckbox.selections.items[i].data.kd_unit + "',";
										}
                                        // console.log(tmpnotransaksi);
										//AKHIR
										tmpclosenotransaksi = tmpnotransaksi;
										tmpclosekdpasien=tmpkdpasien;
										temporary_closekd_unit_import=temporary_kode_unit_import;
										tmpcloseDataPasienImport = tmpDataPasienImport;
										tmpunit = Ext.get('cboUnitPosting_PenerimaanTunai').getValue();
										var tmpdetailunit = Ext.get('cbocombounit_PenerimaanTunai').getValue();
										tmptglawal = formatDate(Ext.getCmp('TxtTgl_Awal_Import_PenerimaanTunai').getValue());
										tmptglakhir = formatDate(Ext.getCmp('TxtTgl_Akhir_Import_PenerimaanTunai').getValue());
                                        if (tmpnotransaksi != '') {
                                            Ext.getCmp('Txt_catatan_penerimaan').setValue('PENDAPATAN '+tmpunit+' ('+tmpdetailunit+') TANGGAL '+tmptglawal+' s/d '+ tmptglakhir);
                                            if (tmpunit === 'Rawat Inap') {
                                                refeshDataImporPenerimaanRWI(tmpnotransaksi,tmpkdpasien);
                                            }else{
                                                refeshDataImporPenerimaan(tmpnotransaksi,tmpkdpasien);
                                            }
                                        }else{
                                            
                                        }

										
										
                                        FormLookUpImportDataPenerimaanTunai.destroy();                                                        
                                    }
                                },
                                {
                                    xtype: 'button',
                                    text: 'Batal',
                                    iconCls: 'close',
                                    id: 'btnBatal_Import_penerimaan',
                                    handler: function ()
                                    {
										
                                        FormLookUpImportDataPenerimaanTunai.destroy();                                               
                                    }
                                },
                                 
                            ]
                },
				bbar:[      
                    {
                        xtype: 'tbspacer',
                        width: 500
                    },
                    {
                
                        xtype: 'label',
                        text: 'Total : '
                    },
                   
                    {
                        xtype: 'textfield',
                        name: 'Txttotal_jumlah_Import_peneriman',
                        id: 'Txttotal_jumlah_Import_peneriman',
                        width: 150,
                        disabled:true,
                        style: 'text-align: right',
                        value: 0,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {

                                        }

                                    }
                                }
                    }
        
                ]
        }
    );
    return Isi_form_ImportDataPenerimaanTunai;
}
;

var tmpclosenotransaksi = '';
var tmpclosekdpasien = '';
var temporary_closekd_unit_import = '';
var tmpcloseDataPasienImport = '';


var tmpunit = '';

function Panel_Import_DataPenerimaanTunai() {
    
    var items = new Ext.Panel
            (
                    {
                        title: '',
                        id: 'Panel_Import_DataPenerimaanTunai',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        bodyStyle: 'padding:0px 0px 0px 0px',
                        border: true,
                        autoscroll: true,
                        items: [
                            {
                                layout: 'absolute',
                                bodyStyle: 'padding: 5px 5px 5px 5px',
                                border: false,
                                width: 200,
                                height: 200,
                                anchor: '100% 100%',
                                items: [
                                    {
                                        x: 10,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'Customer '
                                    },
                                    {
                                        x: 110,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    mComboCustomer_Import_PenerimaanTunai(),
                                    {
                                        x: 10,
                                        y: 40,
                                        xtype: 'label',
                                        text: 'Unit '
                                    },
                                    {
                                        x: 110,
                                        y: 40,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    mComboUnitPosting_PenerimaanTunai(),

                                    mComboUnit_PenerimaanTunai(),
                                    {
                                        x: 10,
                                        y: 100,
                                        xtype: 'label',
                                        text: 'Tanggal '
                                    },
                                    {
                                        x: 110,
                                        y: 100,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 100,
                                        xtype: 'datefield',
                                        name: 'TxtTgl_Awal_Import_PenerimaanTunai',
                                        id: 'TxtTgl_Awal_Import_PenerimaanTunai',
                                        format:'d/M/Y',
                                        width: 100,
                                        value :now,
                                        listeners:
                                                {
                                                    'specialkey': function ()
                                                    {
                                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                                        {

                                                        }
                                                    }
                                                }
                                    },
                                    {
                                        x: 230,
                                        y: 105,
                                        xtype: 'label',
                                        text: 's/d '
                                    },
                                    {
                                        x: 250,
                                        y: 100,
                                        xtype: 'datefield',
                                        name: 'TxtTgl_Akhir_Import_PenerimaanTunai',
                                        id: 'TxtTgl_Akhir_Import_PenerimaanTunai',
                                        format:'d/M/Y',
                                        width: 100,
                                        value :now,
                                        listeners:
                                                {
                                                    'specialkey': function ()
                                                    {
                                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                                        {

                                                        }
                                                    }
                                                }
                                    },
                                    {
                                        x: 10,
                                        y: 130,
                                        xtype: 'label',
                                        text: 'Shift '
                                    },
                                    {
                                        x: 110,
                                        y: 130,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 130,
                                        xtype: 'checkboxgroup',
                                        id: 'cgShift',
                                        itemCls: 'x-check-group-alt',
                                        columns: 4,
                                        boxMaxWidth: 400,
                                        items: [
                                                    {boxLabel: 'Semua',checked:true,name: 'Shift_All',id : 'Shift_All',handler: function (field, value) {if (value === true){Ext.getCmp('Shift_1').setValue(true);Ext.getCmp('Shift_2').setValue(true);Ext.getCmp('Shift_3').setValue(true);Ext.getCmp('Shift_1').disable();Ext.getCmp('Shift_2').disable();Ext.getCmp('Shift_3').disable();}else{Ext.getCmp('Shift_1').setValue(false);Ext.getCmp('Shift_2').setValue(false);Ext.getCmp('Shift_3').setValue(false);Ext.getCmp('Shift_1').enable();Ext.getCmp('Shift_2').enable();Ext.getCmp('Shift_3').enable();}}},
                                                    {boxLabel: 'Shift 1',checked:true,disabled:true,name: 'Shift_1',id : 'Shift_1'},
                                                    {boxLabel: 'Shift 2',checked:true,disabled:true,name: 'Shift_2',id : 'Shift_2'},
                                                    {boxLabel: 'Shift 3',checked:true,disabled:true,name: 'Shift_3',id : 'Shift_3'}
                                                ]
                                    },
                                    {
                                        x: 395,
                                        y: 165,
                                        xtype: 'button',
                                        text : 'Cari',
                                        iconCls: 'refresh',
                                        name: 'BtnCariDataImport',
                                        id: 'BtnCariDataImport',
                                        width: 80,
                                        handler: function ()
                                        {
                                            if (Ext.getCmp('cboCustomer_Import_PenerimaanTunai').getValue()=== '' || Ext.getCmp('cboUnitPosting_PenerimaanTunai').getValue()=== '' || Ext.getCmp('cbocombounit_PenerimaanTunai').getValue()=== '') {
                                                ShowPesanInfopenerimaan('Silahkan Lengkapi Data Pencarian Terlebih Dahulu', 'Peringatan');
                                            }
                                            else{
                                                refeshHasilImporPenerimaan();
                                            }
                                            
                                        }
                                    },
                                ]
                            }
                        ]
                    });
    return items;
}
;

var ds_grid_detail_Import_DataPenerimaanTunai;
var grid_detail_Import_DataPenerimaanTunai;
function Getgrid_detail_Import_DataPenerimaanTunai(data) {
    var selectModel = new Ext.grid.CheckboxSelectionModel();
    var fldDetail = ['tag','no_transaksi', 'tgl_transaksi', 'kd_unit', 'kd_pay',
                    'uraian', 'harga', 'nama',  'kd_kasir', 'co_status', 'kd_pasien','nama_unit'];
    ds_grid_detail_Import_DataPenerimaanTunai = new WebApp.DataStore({fields: fldDetail});
    grid_detail_Import_DataPenerimaanTunai = new Ext.grid.EditorGridPanel({
        title: 'Penerimaan',
        id: 'grid_detail_penerimaan',
        stripeRows: true,
        store: ds_grid_detail_Import_DataPenerimaanTunai,
        border: false,
        frame: false,
		flex:1,
        // height: 235,
        width: 825,
        anchor: '100%',
        autoScroll: true,
        sm : selectModel,
        cm: new Ext.grid.ColumnModel([
                new Ext.grid.RowNumberer(),
                selectModel,
                {
                    id: 'colNotransImport',
                    header: 'No. Transaksi',
                    dataIndex: 'no_transaksi',
                    width: 80,
                    hidden: false,
                    menuDisabled: true
                }, {
                    id: 'colkd_pasienImport',
                    header: 'No. Medrec',
                    dataIndex: 'kd_pasien',
                    sortable: true,
                    width: 150,
                    align: 'center',
                }, {
                    id: 'colNamaImport',
                    header: 'Nama',
                    dataIndex: 'nama',
                    width: 150,
                    menuDisabled: true,
                    hidden: false
                },{
                    id: 'colTanggalImport',
                    header: 'Tanggal',
                    dataIndex: 'tgl_transaksi',
                    value:1,
                    hidden: false,
                    menuDisabled: true,
                    width: 100,
                    renderer: function(v, params, record){
                        return ShowDate(record.data.tgl_transaksi);
                    }
                },
				/*{ 
                    id: 'colShiftImport',
                    header: 'Shift',
                    dataIndex: 'shift',
                    width: 130,
                    menuDisabled: true,
                   
                },*/
				{
                    id: 'colJumlahImport',
                    header: 'Jumlah',
                    dataIndex: 'harga',
                    menuDisabled: true,
                    hidden: false,
                    width: 150,
                    align:'right',
                    renderer: function(v, params, record)
                    {
                        return formatCurrency(record.data.harga);
                    }
                },
            ]),
        viewConfig: {forceFit: true},
    });

    return grid_detail_Import_DataPenerimaanTunai;
}

var tmpkdCUstomer;
var tmpcodeunitposting;
function mComboUnitPosting_PenerimaanTunai()
{
    var Field = ['KODE', 'NAMA'];

    dsUnitPosting_PenerimaanTunai = new WebApp.DataStore({fields: Field});
    dsUnitPosting_PenerimaanTunai.load
        (
            {
            params:
                    {
                        Skip: 0,
                        Take: 1000,
                        Sort: 'pay_code',
                        Sortdir: 'ASC',
                        target: 'ViewComboUnitImportData',
                        param: ""
                    }
            }
        );

    var cboUnitPosting_PenerimaanTunai = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 40,
                        id: 'cboUnitPosting_PenerimaanTunai',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        emptyText: '',
                        align: 'Right',
                        editable: false,
                        width: 200,
                        store: dsUnitPosting_PenerimaanTunai,
                        valueField: 'KODE',
                        displayField: 'NAMA',
                        selectOnFocus: true,
                        enableKeyEvents:true,
                        tabIndex: 3,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        tmpcodeunitposting = b.data.KODE;
                                        params = '';
										var tabel_unit = '';
                                        if (b.data.KODE === '1'  || b.data.KODE === 1) {
                                            params = "parent = '1' union select 'ALL','999'";
											tabel_unit = 'unit';
                                        }else if (b.data.KODE === '6' || b.data.KODE === 6){
                                             params = " union select 'ALL','999' order by nama";
											 tabel_unit = 'apt_unit';
                                        }else{
                                             params = "left(kd_unit,1) = '"+ b.data.KODE +"'  union select 'ALL','999'";
											 tabel_unit = 'unit';
                                        }
                                        getloaddataunitpenerimaantunai(params,tabel_unit);
                                        Ext.getCmp('cbocombounit_PenerimaanTunai').setValue('ALL');
										tmpparent = '999';
                                    },
                                    keypress:function(c,e){
                                        if (e.keyCode == 13){
                                        } else if (e.keyCode == 9){
                                        }
                                    },

                                }
                    }
            );

    return cboUnitPosting_PenerimaanTunai;
}
;

function mComboCustomer_Import_PenerimaanTunai()
{
    var Field = ['KODE', 'NAMA'];

    dsCustomer_Import_PenerimaanTunai = new WebApp.DataStore({fields: Field});
    dsCustomer_Import_PenerimaanTunai.load
        (
            {
            params:
                    {
                        Skip: 0,
                        Take: 1000,
                        Sort: 'pay_code',
                        Sortdir: 'ASC',
                        target: 'ViewComboCustomer_Keuangan',
                        param: ""
                    }
            }
        );

    var cboCustomer_Import_PenerimaanTunai = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 10,
                        id: 'cboCustomer_Import_PenerimaanTunai',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        emptyText: '',
                        align: 'Right',
                        editable: false,
                        width: 200,
                        store: dsCustomer_Import_PenerimaanTunai,
                        valueField: 'KODE',
                        displayField: 'NAMA',
                        selectOnFocus: true,
                        enableKeyEvents:true,
                        tabIndex: 3,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        tmpkdCUstomer = b.data.KODE;
                                    },
                                    keypress:function(c,e){
                                        if (e.keyCode == 13){

                                        } else if (e.keyCode == 9){

                                        }
                                    },

                                }
                    }
            );

    return cboCustomer_Import_PenerimaanTunai;
}
;

function getloaddataunitpenerimaantunai(params,tabel_unit){
    dscombounit_PenerimaanTunai.load
        (
            {
            params:
                    {
                        Skip: 0,
                        Take: 1000,
                        Sort: tabel_unit,
                        Sortdir: 'ASC',
                        target: 'ViewComboUnitPenerimaanPiutang',
                        param: params
                    }
            }
        );
    return dscombounit_PenerimaanTunai
}
var dscombounit_PenerimaanTunai;
var tmpparent;
function mComboUnit_PenerimaanTunai()
{
    var Field = ['KODE', 'NAMA'];

    dscombounit_PenerimaanTunai = new WebApp.DataStore({fields: Field});    

    var cbocombounit_PenerimaanTunai = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 70,
                        id: 'cbocombounit_PenerimaanTunai',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        emptyText: '',
                        align: 'Right',
                        editable: true,
                        width: 200,
                        store: dscombounit_PenerimaanTunai,
                        valueField: 'KODE',
                        displayField: 'NAMA',
                        selectOnFocus: true,
                        enableKeyEvents:true,
                        tabIndex: 3,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        tmpparent = b.data.KODE;
										
                                    },
                                    keypress:function(c,e){
                                        if (e.keyCode == 13){

                                        } else if (e.keyCode == 9){

                                        }
                                    },

                                }
                    }
            );

    return cbocombounit_PenerimaanTunai;
}
;

var tmptanggalawalimport;
var tmptanggalakhirimport;
function refeshHasilImporPenerimaan() {
    // var tmpshift = '';
    // var tmpshift2 = '';
    tmptanggalawalimport = Ext.getCmp('TxtTgl_Awal_Import_PenerimaanTunai').getValue();
	tmptanggalakhirimport = Ext.getCmp('TxtTgl_Akhir_Import_PenerimaanTunai').getValue();
    if (Ext.getCmp('Shift_All').getValue() != true) {
        if (Ext.getCmp('Shift_1').getValue() === true) {
            tmpshift = '1';
       }
       if (Ext.getCmp('Shift_2').getValue() === true) {
            if (tmpshift != '') {
                tmpshift += ',2';
            }else{
                tmpshift = '2';
            }            
       }
       if (Ext.getCmp('Shift_3').getValue() === true) {
            if (tmpshift != '') {
                tmpshift += ',3';
            }else{
                tmpshift = ',3';
            }
       }
       if (Ext.getCmp('Shift_1').getValue() === true && Ext.getCmp('Shift_2').getValue() === true && Ext.getCmp('Shift_3').getValue() === true) {
            tmpshift2 = '4';
       }else{
           tmpshift2 = ''; 
       }    
    }else{
        tmpshift = '1,2,3';
        tmpshift2 = '4';
    }
    if (Ext.getCmp('cbocombounit_PenerimaanTunai').getValue() === 'ALL' || Ext.getCmp('cbocombounit_PenerimaanTunai').getValue() === '999') {
        tmpkode_unit = Ext.getCmp('cboUnitPosting_PenerimaanTunai').getValue();
		parent = Ext.getCmp('cbocombounit_PenerimaanTunai').getValue();
    }else{
        tmpkode_unit = Ext.getCmp('cbocombounit_PenerimaanTunai').getValue();
		parent = Ext.getCmp('cboUnitPosting_PenerimaanTunai').getValue();
    }
	loadMask.show();
    Ext.Ajax.request({
        url: baseURL + "index.php/anggaran/penerimaan/select_transaksi_detail",
        params: {
            kd_unit:tmpkode_unit,
            tgl:Ext.getCmp('TxtTgl_Awal_Import_PenerimaanTunai').getValue(),
            tgl2:Ext.getCmp('TxtTgl_Akhir_Import_PenerimaanTunai').getValue(),
            customer:tmpkdCUstomer,
            Shift:tmpshift,
            Shift2:tmpshift2,
            parent:parent
        },
        failure: function(o){   
            loadMask.hide();
            ShowPesanWarningpenerimaan('Hubungi Admin', 'Error');
        },  
        success: function(o){
		
        var cst = Ext.decode(o.responseText);
        if (cst.success === true){
			
            if (cst.ListDataObj.length === 0) {
                ShowPesanInfopenerimaan('Tidak Ada Data', 'Info');
            }else{
                 ds_grid_detail_Import_DataPenerimaanTunai.removeAll();
                var recs=[],
                recType=ds_grid_detail_Import_DataPenerimaanTunai.recordType;
                for(var i=0; i<cst.ListDataObj.length; i++){
                    recs.push(new recType(cst.ListDataObj[i]));
                }
                ds_grid_detail_Import_DataPenerimaanTunai.add(recs);
                grid_detail_Import_DataPenerimaanTunai.getView().refresh();
				totalJumlahPencarianDataImport();
            }
			loadMask.hide();
        }
    }
})
}

var xztotal;
function totalJumlahPencarianDataImport(){
 xztotal=0;
for (var L=0; L<ds_grid_detail_Import_DataPenerimaanTunai.getCount(); L++)
            {
                if (ds_grid_detail_Import_DataPenerimaanTunai.data.items[L].data.harga==="" ||
                 ds_grid_detail_Import_DataPenerimaanTunai.data.items[L].data.harga==undefined||
                 ds_grid_detail_Import_DataPenerimaanTunai.data.items[L].data.harga==="NaN" )
                {
                    ds_grid_detail_Import_DataPenerimaanTunai.data.items[L].data.harga=0;
                    xztotal=parseFloat(xztotal)+parseFloat(ds_grid_detail_Import_DataPenerimaanTunai.data.items[L].data.harga);
                }else{
                    
                    xztotal=parseFloat(xztotal)+parseFloat(ds_grid_detail_Import_DataPenerimaanTunai.data.items[L].data.harga);
                }
            }
            Ext.getCmp('Txttotal_jumlah_Import_peneriman').setValue(formatCurrency(xztotal));
}

function refeshDataImporPenerimaan(params,paramkdpasien) {
	// var tmpshift = '';
 //    var tmpshift2 = '';
    tmptanggalawalimport = tmptglawal;
    tmptanggalakhirimport = tmptglakhir;
    if (Ext.getCmp('Shift_All').getValue() != true) {
        if (Ext.getCmp('Shift_1').getValue() === true) {
            tmpshift = '1';
       }
       if (Ext.getCmp('Shift_2').getValue() === true) {
            if (tmpshift != '') {
                tmpshift += ',2';
            }else{
                tmpshift = '2';
            }            
       }
       if (Ext.getCmp('Shift_3').getValue() === true) {
            if (tmpshift != '') {
                tmpshift += ',3';
            }else{
                tmpshift = ',3';
            }
       }
       if (Ext.getCmp('Shift_1').getValue() === true && Ext.getCmp('Shift_2').getValue() === true && Ext.getCmp('Shift_3').getValue() === true) {
            tmpshift2 = '4';
       }else{
           tmpshift2 = ''; 
       }    
    }else{
        tmpshift = '1,2,3';
        tmpshift2 = '4';
    }

    /*if (tmpparent === '999' || Ext.getCmp('cbocombounit_PenerimaanTunai').getValue() === 'ALL') {
        //parent = Ext.getCmp('cbocombounit_PenerimaanTunai').getValue();
		parent = Ext.getCmp('cboUnitPosting_PenerimaanTunai').getValue();	
		tmpkode_unit = Ext.getCmp('cbocombounit_PenerimaanTunai').getValue();
    }else{
        //parent = Ext.getCmp('cboUnitPosting_PenerimaanTunai').getValue();	
			parent = Ext.getCmp('cboUnitPosting_PenerimaanTunai').getValue();	
			tmpkode_unit = Ext.getCmp('cbocombounit_PenerimaanTunai').getValue();
    } */
	parent = Ext.getCmp('cboUnitPosting_PenerimaanTunai').getValue();	
	tmpkode_unit = Ext.getCmp('cbocombounit_PenerimaanTunai').getValue();
    /*if (Ext.getCmp('cbocombounit_PenerimaanTunai').getValue() === 'ALL') {
        tmpkode_unit = Ext.getCmp('cboUnitPosting_PenerimaanTunai').getValue();
    }else{
        tmpkode_unit = Ext.getCmp('cbocombounit_PenerimaanTunai').getValue();
    }*/
	
    Ext.Ajax.request({
        url: baseURL + "index.php/anggaran/penerimaan/select_import_transaksi_detail",
        params: {
            kd_unit:tmpkode_unit,
            tgl:Ext.getCmp('TxtTgl_Awal_Import_PenerimaanTunai').getValue(),
            tgl2:Ext.getCmp('TxtTgl_Akhir_Import_PenerimaanTunai').getValue(),
            customer:tmpkdCUstomer,
            Shift:tmpshift,
            Shift2:tmpshift2,
            parent:parent,
            no_transaksi: params,
            kd_pasien: paramkdpasien,
			kode_unit_fix : temporary_kode_unit_import
        },
        failure: function(o){   
            loadMask.hide();
            ShowPesanWarningpenerimaan('Hubungi Admin', 'Error');
        },  
        success: function(o){
        var cst = Ext.decode(o.responseText);
        if (cst.success === true){
            if (cst.ListDataObj.length === 0) {
                ShowPesanInfopenerimaan('Tidak Ada Data', 'Info');
            }else{
                 ds_penerimaan_grid_detail.removeAll();
                var recs=[],
                recType=ds_penerimaan_grid_detail.recordType;
                for(var i=0; i<cst.ListDataObj.length; i++){
                    recs.push(new recType(cst.ListDataObj[i]));
                }
                ds_penerimaan_grid_detail.add(recs);
                grid_detail_penerimaan.getView().refresh();
            }
        }
    }
})
}

function refeshDataImporPenerimaanRWI(params,paramkdpasien) {
// var tmptglawal;
// var tmptglakhir;
    tmptanggalawalimport = tmptglawal;
	tmptanggalakhirimport = tmptglakhir;
    if (Ext.getCmp('Shift_All').getValue() != true) {
        if (Ext.getCmp('Shift_1').getValue() === true) {
            tmpshift = '1';
       }
       if (Ext.getCmp('Shift_2').getValue() === true) {
            if (tmpshift != '') {
                tmpshift += ',2';
            }else{
                tmpshift = '2';
            }            
       }
       if (Ext.getCmp('Shift_3').getValue() === true) {
            if (tmpshift != '') {
                tmpshift += ',3';
            }else{
                tmpshift = ',3';
            }
       }
       if (Ext.getCmp('Shift_1').getValue() === true && Ext.getCmp('Shift_2').getValue() === true && Ext.getCmp('Shift_3').getValue() === true) {
            tmpshift2 = '4';
       }else{
           tmpshift2 = ''; 
       }    
    }else{
        tmpshift = '1,2,3';
        tmpshift2 = '4';
    }

    /*if (tmpparent === '999') {
        parent = Ext.getCmp('cboUnitPosting_PenerimaanTunai').getValue();
    }else{
        parent = tmpparent;
    }

    if (Ext.getCmp('cbocombounit_PenerimaanTunai').getValue() === 'ALL') {
        tmpkode_unit = Ext.getCmp('cboUnitPosting_PenerimaanTunai').getValue();
    }else{
        tmpkode_unit = Ext.getCmp('cbocombounit_PenerimaanTunai').getValue();
    }*/
	
	tmpkode_unit = Ext.getCmp('cboUnitPosting_PenerimaanTunai').getValue();
	parent = Ext.getCmp('cboUnitPosting_PenerimaanTunai').getValue();
	
    Ext.Ajax.request({
        url: baseURL + "index.php/anggaran/penerimaan/select_import_transaksi_detail_rwi",
        params: {
            kd_unit:tmpkode_unit,
            tgl:Ext.getCmp('TxtTgl_Awal_Import_PenerimaanTunai').getValue(),
            tgl2:Ext.getCmp('TxtTgl_Akhir_Import_PenerimaanTunai').getValue(),
            customer:tmpkdCUstomer,
            Shift:tmpshift,
            Shift2:tmpshift2,
            parent:parent,
            no_transaksi: params,
            kd_pasien: paramkdpasien
        },
        failure: function(o){   
            loadMask.hide();
            ShowPesanWarningpenerimaan('Hubungi Admin', 'Error');
        },  
        success: function(o){
        var cst = Ext.decode(o.responseText);
        if (cst.success === true){
            if (cst.ListDataObj.length === 0) {
                ShowPesanInfopenerimaan('Tidak Ada Data', 'Info');
            }else{
                 ds_penerimaan_grid_detail.removeAll();
                var recs=[],
                recType=ds_penerimaan_grid_detail.recordType;
                for(var i=0; i<cst.ListDataObj.length; i++){
                    recs.push(new recType(cst.ListDataObj[i]));
                }
                ds_penerimaan_grid_detail.add(recs);
                grid_detail_penerimaan.getView().refresh();
            }
        }
    }
})
};

var FormLookUpCekDataMappingPenerimaanTunai;
function CekDataMappingPenerimaanTunaiLookUp() {
    FormLookUpCekDataMappingPenerimaanTunai = new Ext.Window({
        id: 'WindowFormLookUpCekDataMappingDataPenerimaanTunai',
        title: 'Cek Data Mapping Produk - Account',
        closeAction: 'destroy',
        width: 700,
        height: 500,
        border: false,
        resizable: false,
        plain: true,
        layout: 'fit',
        constrain: true,
        iconCls: 'Request',
        modal: true,
        items: [
                form_CekDataPenerimaanTunai(),
                ],
        listeners: {
            'destroy' : function(){
                if (tmpunit === 'Rawat Inap') 
                {
                    refeshDataImporPenerimaanRWIClose(tmpnotransaksi,tmpkdpasien);
                }else{
                    refeshDataImporPenerimaanClose(tmpnotransaksi,tmpkdpasien);
                }
            }
            
        }
    });
    FormLookUpCekDataMappingPenerimaanTunai.show();
}
;
function form_CekDataPenerimaanTunai() {
    var Isi_form_CekDataPenerimaanTunai = new Ext.Panel
    (
        {
            id: 'Isi_form_CekDataPenerimaanTunai',
            closable: true,
            region: 'center',
            layout: 'form',
            itemCls: 'blacklabel',
            bodyStyle: 'padding: 0px 0px 0px 0px',
            border: true,
            shadhow: true,
            width: 400,
            height: 200,
            iconCls: '',
            items:
                    [
                        Getgrid_detail_Cek_DataPenerimaanTunai()
                    ]
        }
    );
    return Isi_form_CekDataPenerimaanTunai;
}
;

var ds_grid_detail_Cek_DataPenerimaanTunai;
var grid_detail_Cek_DataPenerimaanTunai;
function Getgrid_detail_Cek_DataPenerimaanTunai(data) {
    // var selectModel = new Ext.grid.CheckboxSelectionModel();
    var fldDetail = ['kdproduk','kd_produk', 'kd_unit', 'nama_unit', 'account',  'name', 'description',
                    'value'];
    ds_grid_detail_Cek_DataPenerimaanTunai = new WebApp.DataStore({fields: fldDetail});
    grid_detail_Cek_DataPenerimaanTunai = new Ext.grid.EditorGridPanel({
        // title: 'Penerimaan',
        id: 'grid_detail_penerimaan',
        stripeRows: true,
        store: ds_grid_detail_Cek_DataPenerimaanTunai,
        border: false,
        frame: false,
        height: 465,
        width: 815,
        anchor: '100%',
        autoScroll: true,
        // sm : selectModel,
        cm: new Ext.grid.ColumnModel([
                new Ext.grid.RowNumberer(),
                // selectModel,
                {
                    id: 'colNotransImport',
                    header: 'kd. produk',
                    dataIndex: 'kdproduk',
                    width: 70,
                    hidden: false,
                    menuDisabled: true,
                    renderer : function(value, meta) {
					    if(value === 'null' || value === null) {
					        meta.style = "background-color:red;width: 70;";
					    }
					    return value;
					}
                }, 
                {
                    id: 'colNamaImport',
                    header: 'kd. Unit',
                    dataIndex: 'kd_unit',
                    width: 50,
                    menuDisabled: true,
                    hidden: false,
                    renderer : function(value, meta) {
                        if(value === 'null' || value === null) {
                            meta.style = "background-color:red;width: 50;";
                        }
                        return value;
                    }
                },
                {
                    id: 'colNamaUnitImport',
                    header: 'Nama Unit',
                    dataIndex: 'nama_unit',
                    width: 150,
                    menuDisabled: true,
                    hidden: false,
                    renderer : function(value, meta) {
					    if(value === 'null' || value === null) {
					        meta.style = "background-color:red;width: 150;";
					    }
					    return value;
					}
                },
                {
                    id: 'colTanggalImport',
                    header: 'Account',
                    dataIndex: 'account',
                    value:1,
                    hidden: false,
                    menuDisabled: true,
                    width: 100,
                    editor: new Nci.form.Combobox.autoComplete({
                        store   : Account_penerimaan_ds,
                        select  : function(a,b,c){
                                var line = grid_detail_Cek_DataPenerimaanTunai.getSelectionModel().selection.cell[0];
                                console.log(b.data);
                                var kd_produk = ds_grid_detail_Cek_DataPenerimaanTunai.data.items[line].data.kdproduk;
                                var kd_unit = ds_grid_detail_Cek_DataPenerimaanTunai.data.items[line].data.kd_unit;
                                var account = b.data.account;
                                SimpanDataAccountInterface(kd_produk,kd_unit,account);
                                },
                        insert  : function(o){
                            return {
                                account         : o.account,
                                name            : o.name,
                                text            :  '<table style="font-size: 11px;"><tr><td width="50">'+o.account+'</td><td width="200">'+o.name+'</td></tr></table>'
                            }
                        },
                        url: baseURL + "index.php/anggaran/penerimaan/dataaccountinterface",
                        valueField: 'account',
                        displayField: 'text',
                        listWidth: 200,
                        width:200
                    }),
                    renderer : function(value, meta) {
					    if(value === 'null' || value === null) {
					        meta.style = "background-color:red;width: 100;";
					    }
					    return value;
					}
                },
                { 
                    id: 'colShiftImport',
                    header: 'Deskripsi',
                    dataIndex: 'description',
                    width: 250,
                    menuDisabled: true,
                    renderer : function(value, meta) {
					    if(value === 'null' || value === null) {
					        meta.style = "background-color:red;width: 250;";
					    }
					    return value;
					}
                   
                },
                {
                    id: 'colJumlahImport',
                    header: 'Jumlah',
                    dataIndex: 'value',
                    menuDisabled: true,
                    hidden: true,
                    width: 150,
                    align:'right',
                    renderer: function(v, params, record)
                    {
                        return formatCurrency(record.data.value);
                    }
                },
            ]),
        // tbar: {
        //     xtype: 'toolbar',
        //     items: [
        //                 {
        //                     xtype: 'button',
        //                     text: 'Simpan',
        //                     iconCls: 'save',
        //                     id: 'btnsave_MapingAccountPengeluaran',
        //                     handler: function ()
        //                     {
        //                         SimpanDataAccountInterface();
        //                     }
        //                 } ,
        //             ]
        // },
        viewConfig: {forceFit: false},
    });

    return grid_detail_Cek_DataPenerimaanTunai;
}

var tmpshift = '';
var tmpshift2 = '';
function refeshCekDataImporPenerimaan(params,paramkdpasien) {
    if (tmpparent === '999') {
        parent = tmpcodeunitposting;
    }else{
        parent = tmpparent;
    }
    Ext.Ajax.request({
        url: baseURL + "index.php/anggaran/penerimaan/cekdata_mapping_produk",
        params: {
            kd_unit:tmpcodeunitposting,
            tgl:tmptanggalawalimport,
            tgl2:tmptanggalakhirimport,
            customer:tmpkdCUstomer,
            Shift:tmpshift,
            Shift2:tmpshift2,
            parent:parent,
            no_transaksi: params,
            kd_pasien: paramkdpasien,
			kode_unit_fix : temporary_kode_unit_import
        },
        failure: function(o){   
            loadMask.hide();
            ShowPesanWarningpenerimaan('Hubungi Admin', 'Error');
        },  
        success: function(o){
        var cst = Ext.decode(o.responseText);
        if (cst.success === true){
            if (cst.ListDataObj.length === 0) {
                ShowPesanInfopenerimaan('Tidak Ada Data', 'Info');
            }else{
                 ds_grid_detail_Cek_DataPenerimaanTunai.removeAll();
                var recs=[],
                recType=ds_grid_detail_Cek_DataPenerimaanTunai.recordType;
                for(var i=0; i<cst.ListDataObj.length; i++){
                    recs.push(new recType(cst.ListDataObj[i]));
                }
                ds_grid_detail_Cek_DataPenerimaanTunai.add(recs);
                grid_detail_Cek_DataPenerimaanTunai.getView().refresh();
            }
        }
    }
})
}

function refeshCekDataImporPenerimaanRWI(params,paramkdpasien) {

    if (tmpparent === '999') {
        parent = tmpcodeunitposting;
    }else{
        parent = tmpparent;
    }
    Ext.Ajax.request({
        url: baseURL + "index.php/anggaran/penerimaan/cekdata_mapping_produk_rwi",
        params: {
            kd_unit:tmpcodeunitposting,
            tgl:tmptanggalawalimport,
            tgl2:tmptanggalakhirimport,
            customer:tmpkdCUstomer,
            Shift:tmpshift,
            Shift2:tmpshift2,
            parent:parent,
            no_transaksi: params,
            kd_pasien: paramkdpasien
        },
        failure: function(o){   
            loadMask.hide();
            ShowPesanWarningpenerimaan('Hubungi Admin', 'Error');
        },  
        success: function(o){
        var cst = Ext.decode(o.responseText);
        if (cst.success === true){
            if (cst.ListDataObj.length === 0) {
                ShowPesanInfopenerimaan('Tidak Ada Data', 'Info');
            }else{
                ds_grid_detail_Cek_DataPenerimaanTunai.removeAll();
                var recs=[],
                recType=ds_grid_detail_Cek_DataPenerimaanTunai.recordType;
                for(var i=0; i<cst.ListDataObj.length; i++){
                    recs.push(new recType(cst.ListDataObj[i]));
                }
                ds_grid_detail_Cek_DataPenerimaanTunai.add(recs);
                grid_detail_Cek_DataPenerimaanTunai.getView().refresh();
            }
        }
    }
})
};

function formatDate(date) {
  var monthNames = [
    "January", "February", "March",
    "April", "May", "June", "July",
    "August", "September", "October",
    "November", "December"
  ];

  var day = date.getDate();
  var monthIndex = date.getMonth();
  var year = date.getFullYear();

  return day + ' ' + monthNames[monthIndex] + ' ' + year;
}


function SimpanDataAccountInterface(produk,unit,account){
    Ext.Ajax.request
            (
                {   
                    url: baseURL + "index.php/anggaran/penerimaan/savedataaccountinterface",
                    params: {produk:produk,unit:unit,account:account},
                    failure: function (o){
                        ShowPesanWarningpenerimaan('Proses Simpan Tidak berhasil silahkan hubungi admin', 'Gagal');
                    },
                    success: function (o){
                        var cst = Ext.decode(o.responseText);
                        if (cst.success === true){
                            if (tmpunit === 'Rawat Inap') {
                                refeshCekDataImporPenerimaanRWI(tmpnotransaksi,tmpkdpasien);
                            }else{
                                refeshCekDataImporPenerimaan(tmpnotransaksi,tmpkdpasien);
                            }
                        }
                         else{
                            ShowPesanWarningpenerimaan('Proses Simpan Tidak berhasil silahkan hubungi admin', 'Gagal');
                        };
                    }
                }
            );
}

function refeshDataImporPenerimaanClose(params,paramkdpasien) {
    tmptanggalawalimport = tmptglawal;
    tmptanggalakhirimport = tmptglakhir;
	//var tmpparent = '';
	var tmptmpcodeunitposting = '';
	if(parent.length > 1){
		tmpparent = tmpcodeunitposting;
		tmptmpcodeunitposting = parent;
	}else{
		tmpparent = parent;
		tmptmpcodeunitposting = tmpcodeunitposting;
	}
	console.log(parent);
	console.log(tmpparent);
	console.log(tmptmpcodeunitposting);
	console.log(tmpcodeunitposting);
    Ext.Ajax.request({
        url: baseURL + "index.php/anggaran/penerimaan/select_import_transaksi_detail",
        params: {
            kd_unit:tmptmpcodeunitposting,
            tgl:tmptanggalawalimport,
            tgl2:tmptanggalakhirimport,
            customer:tmpkdCUstomer,
            Shift:tmpshift,
            Shift2:tmpshift2,
            parent:tmpparent,
            no_transaksi: params,
            kd_pasien: paramkdpasien,
			kode_unit_fix : temporary_kode_unit_import
        },
        failure: function(o){   
            loadMask.hide();
            ShowPesanWarningpenerimaan('Hubungi Admin', 'Error');
        },  
        success: function(o){
        var cst = Ext.decode(o.responseText);
        if (cst.success === true){
            if (cst.ListDataObj.length === 0) {
                ShowPesanInfopenerimaan('Tidak Ada Data', 'Info');
            }else{
                 ds_penerimaan_grid_detail.removeAll();
                var recs=[],
                recType=ds_penerimaan_grid_detail.recordType;
                for(var i=0; i<cst.ListDataObj.length; i++){
                    recs.push(new recType(cst.ListDataObj[i]));
                }
                ds_penerimaan_grid_detail.add(recs);
                grid_detail_penerimaan.getView().refresh();
            }
        }
    }
})
}

function refeshDataImporPenerimaanRWIClose(params,paramkdpasien) {
    tmptanggalawalimport = tmptglawal;
    tmptanggalakhirimport = tmptglakhir;

    Ext.Ajax.request({
        url: baseURL + "index.php/anggaran/penerimaan/select_import_transaksi_detail_rwi",
        params: {
            kd_unit:tmpcodeunitposting,
            tgl:tmptanggalawalimport,
            tgl2:tmptanggalakhirimport,
            customer:tmpkdCUstomer,
            Shift:tmpshift,
            Shift2:tmpshift2,
            parent:parent,
            no_transaksi: params,
            kd_pasien: paramkdpasien
        },
        failure: function(o){   
            loadMask.hide();
            ShowPesanWarningpenerimaan('Hubungi Admin', 'Error');
        },  
        success: function(o){
        var cst = Ext.decode(o.responseText);
        if (cst.success === true){
            if (cst.ListDataObj.length === 0) {
                ShowPesanInfopenerimaan('Tidak Ada Data', 'Info');
            }else{
                 ds_penerimaan_grid_detail.removeAll();
                var recs=[],
                recType=ds_penerimaan_grid_detail.recordType;
                for(var i=0; i<cst.ListDataObj.length; i++){
                    recs.push(new recType(cst.ListDataObj[i]));
                }
                ds_penerimaan_grid_detail.add(recs);
                grid_detail_penerimaan.getView().refresh();
            }
        }
    }
})
};



