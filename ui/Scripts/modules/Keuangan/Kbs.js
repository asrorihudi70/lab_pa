var CurrentTR_kbs = 
{
    data: Object,
    details:Array, 
    row: 0
};

var mRecord_kbs = Ext.data.Record.create
	(
		[
		   {name: 'ACCOUNT', mapping:'ACCOUNT'},
		   {name: 'NAMAACCOUNT', mapping:'NAMAACCOUNT'},
		   {name: 'DESCRIPTION', mapping:'DESCRIPTION'},
		   {name: 'VALUE', mapping:'VALUE'},
		   {name: 'LINE', mapping:'LINE'}
		]
	);

var KDkategori_kbs='5';
var REFFID_SP3D_kbs;

var mCtrFokus="txtReferensi_kbs";//"txtReferensi_kbs";	
var selectUnitKerja_kbs;
var dsUnitKerja_kbs;
var dsTRList_kbs;
var dsTmp_kbs;
var dsDTLTRList_kbs;
var DataAddNew_kbs = true;
var selectCount_kbs=50;
var now_kbs = new Date();
var selectBayar_kbs;
var selectAktivaLancar_kbs;
var rowSelected_kbs;
var TRLookUps_kbs;
var kdUnit_kbs;
var saldoAkunKBS = 0;
var NamaForm_kbs= "Pencairan Sementara";
var thn = '';
var cellSelectedDet_kbs;
var grListTR_kbs;
var now = new Date();
CurrentPage.page=getPanel_kbs(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanel_kbs(mod_id)
{
    var Field = 
	[
	'cso_number', 'cso_date', 'personal', 'account', 'pay_code',
	'pay_no', 'currency', 'kurs', 'type', 'amount', 'notes',
	'no_tag', 'date_tag', 'amountkurs','kategori',
	'kd_user', 'kd_unit_kerja', 'nama_unit_kerja', 'payment', 'namaaccount','approve', 'referensi','jumlah','approve_tmp'
	]
    dsTRList_kbs = new WebApp.DataStore({ fields: Field });
        
  
    var chkApprove_kbs = new Ext.grid.CheckColumn
	(
		{
			id: 'chkApprove_kbs',
			header: "Approved",
			align: 'center',
			disable:true,
			dataIndex: 'approve_tmp',
			width: 70
		}
	);	
  
    grListTR_kbs = new Ext.grid.EditorGridPanel
	(
		{
			id: 'grListTR_kbs',
			stripeRows: true,
			store: dsTRList_kbs,
			anchor: '100% 100%',
			columnLines:true,
			border:false,
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelected_kbs = dsTRList_kbs.getAt(row);
						}
					}
				}
			),			
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'CSO_Number',
						header: 'No. ' + NamaForm_kbs,
						dataIndex: 'cso_number',
						sortable: true,			
						width :100,
						filter: {}
					}, 
					{
						id: 'Referensi',
						header: 'No. Referensi',
						dataIndex: 'referensi',
						sortable: true,			
						width :100,
						filter: {}
					},
					{
						//xtype: 'datecolumn',
						header: 'Tanggal',
						width: 100,
						sortable: true,
						dataIndex: 'cso_date',
						id:'CSO_Date',
						renderer: function(v, params, record) 
						{
							return ShowDate(record.data.cso_date);
						},
						filter: {}			
					}, 
					{						
						id: 'Personal',
						header: "Dibayarkan Kepada",
						dataIndex: 'personal',
						width :130,
						filter: {}
					},
					{
						id: 'Nama_Unit_Kerja',
						header: 'Unit Kerja',
						dataIndex: 'nama_unit_kerja',
						width :180,
						filter: {}
					},
					{
						id: 'colNotes',
						header: "Keterangan",
						dataIndex: 'notes',
						width :200,
						filter: {}
					}, 
					{
						id: 'Amount',
						header: "Jumlah (Rp.)",
						align:'right',
						dataIndex: 'amount',
						renderer: function(v, params, record) 
						{
							return formatCurrencyDec(record.data.amount);
						},	
						width :80,
						filter: {}
					},
					 chkApprove_kbs
				]
			),

			//plugins: chkApprove_kbs,
			tbar: 
			[				
				{
					id: 'btnEdit_kbs',
					text: 'Edit Data',
					tooltip: 'Edit Data',
					iconCls: 'Edit_Tr',
					handler: function(sm, row, rec) 
					{ 
						if (rowSelected_kbs != undefined)
						{
							LookUpForm_kbs(rowSelected_kbs.data);
							// if (rowSelected_kbs.data.APPROVE)
							// {
							// 	ButtonDisabled_kbs(true);
							// }else
							// {
							// 	ButtonDisabled_kbs(false);
							// }
						}
						else
						{
							LookUpForm_kbs();
						}
					}
				},' ','-',
				{
					xtype: 'checkbox',
					id: 'chkWithTgl_kbs',					
					hideLabel:true,
					checked: true,
					handler: function()
					{
						if (this.getValue()===true)
						{
							Ext.get('dtpTglAwalFilter_kbs').dom.disabled=false;
							Ext.get('dtpTglAkhirFilter_kbs').dom.disabled=false;
							Ext.get('dtpTglAwalFilter_kbs').dom.readOnly=true;
							Ext.get('dtpTglAkhirFilter_kbs').dom.readOnly=true;
						}
						else
						{
							Ext.get('dtpTglAwalFilter_kbs').dom.disabled=true;
							Ext.get('dtpTglAkhirFilter_kbs').dom.disabled=true;							
						};
					}
				}, 
				' ','-','Tanggal : ', ' ',
				{
					xtype: 'datefield',
					fieldLabel: 'Dari Tanggal : ',
					id: 'dtpTglAwalFilter_kbs',
					format: 'd/M/Y',
					value:now_kbs.format('d/M/Y'),
					width:100,
					onInit: function() { }
				}, ' ', ' s/d ',' ', {
					xtype: 'datefield',
					fieldLabel: 'Sd /',
					id: 'dtpTglAkhirFilter_kbs',
					format: 'd/M/Y',
					value:now_kbs.format('d/M/Y'),
					width:100
				}
			],
			bbar:new WebApp.PaggingBar
			(
				{
					displayInfo: true,
					store: dsTRList_kbs,
					pageSize: selectCount_kbs,
					displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
					emptyMsg: "Tidak ada record&nbsp;&nbsp;"		
				}
			),
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					
					if (rowSelected_kbs != undefined)
					{
						LookUpForm_kbs(rowSelected_kbs.data);
						
					}
					else
					{
						LookUpForm_kbs();
					}
				}, 
				'afterrender': function(){ 
					RefreshDataFilter_kbs(true);	
				}
				// End Function # --------------
			},
			viewConfig: {forceFit: true} 			
		}
	);


        // from PenerimaanMhs
	var FormTR_kbs = new Ext.Panel
	(
		{
			id: mod_id,
			closable:true,
			region: 'center',
			layout: 'form',
			title: 'Pencairan Sementara', //'Penerimaan Mahasiswa',          
			border: false,           
			shadhow: true,
			iconCls: 'Penerimaan',
			 margins:'0 5 5 0',
			items: [grListTR_kbs],
			tbar: 
			[
				'No. ' + NamaForm_kbs +' : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: 'No. ' + NamaForm_kbs+' : ',
					id: 'txtNoFilter_kbs',                   
					//anchor: '25%',
					width:120,
					listeners:
					{ 
						'specialkey' : function()
						{
							if (Ext.EventObject.getKey() === 13) 
							{
								RefreshDataFilter_kbs(true);
							}
						}
					},
					onInit: function() { }
				},'-',' ','Unit Kerja'+': ', ' ',mComboUnitKerjaView_kbs(),
				//  ' ','-',
				// 	'Maks.Data : ', ' ',mComboMaksData_kbs(),
					' ','->',
					
				{
					xtype: 'checkbox',
					id: 'chkFilterApproved_kbsView',
					boxLabel: 'Approved'
				},
				{					
					xtype: 'button',
					tooltip: 'Tampilkan',
					iconCls: 'refresh',
					text: 'Tampilkan',
					anchor: '25%',
					handler: function(sm, row, rec) 
					{
						RefreshDataFilter_kbs(false);
					}
				}
			],
			listeners: 
			{ 
				'afterrender': function() 
				{           
					// RefreshDataFilter_kbs(true);
					//RefreshDataFilter_kbs(true);				 
				}
			}
		}
	);
	return FormTR_kbs

};
    // end function get panel main data
 
 ///---------------------------------------------------------------------------------------///
   
   
function LookUpForm_kbs(rowdata)
{
	var lebar=735;
	TRLookUps_kbs = new Ext.Window
	(
		{
			id: 'LookUpForm_kbs',
			title: 'Pencairan Sementara',
			closeAction: 'destroy',
			width: lebar,
			height: 540, 
			border: false,
			resizable:false,
			plain: true,
			layout: 'fit',
			iconCls: 'Penerimaan',
			modal: true,
			items: getFormEntryTR_kbs(lebar),
			listeners:
			{
				activate: function() 
				{					
				},
				afterShow: function() 
				{ 
					this.activate(); 
				},
				deactivate: function()
				{
					rowSelected_kbs=undefined;
					 RefreshDataFilter_kbs(true);
				}
			}
		}
	);
	
	TRLookUps_kbs.show();
	if (rowdata == undefined)
	{
		AddNew_kbs();
	}
	else
	{
		DataInit_kbs(rowdata)
	}	
	
};
   
function getFormEntryTR_kbs(lebar) 
{
	var pnlTR_kbs = new Ext.FormPanel
	(
		{
			id: 'pnlTR_kbs',
			fileUpload: true,
			region: 'north',
			layout: 'fit',
			bodyStyle: 'padding:10px 10px 10px 10px',			
			anchor: '100%', 
			width:lebar,			
			border: false,			
			items: [getItemPanelInput_kbs(lebar)],
			tbar: 
			[
				{
					text: 'Tambah',
					id:'btnTambah',
					tooltip: 'Tambah Record Baru ',
					iconCls: 'add',
					//handler: function() { TRDataAddNew_kbs(pnlTR_kbs) }
					handler: function() 
					{ 
						AddNew_kbs();
						ButtonDisabled_kbs(false);
					}
				}, '-', 
				{
					text: 'Simpan',
					id:'btnSimpan',
					tooltip: 'Rekam Data ',
					iconCls: 'save',
					//handler: function() { TRDatasave_kbs(pnlTR_kbs) }
					handler: function() 
					{ 
						//Ext.getCmp('btnSimpan').setDisabled(true);
						//Ext.getCmp('btnSimpanKeluar').setDisabled(true);		
						Datasave_kbs(false);
						RefreshDataFilter_kbs(false);
					}
				}, '-', 
				{
					text: 'Simpan & Keluar',
					id:'btnSimpanKeluar',
					tooltip: 'Rekam Data & Keluar ',
					iconCls: 'saveexit',
					//handler: function() { TRDatasave_kbs(pnlTR_kbs) }
					handler: function() 
					{ 
						Ext.getCmp('btnSimpan').setDisabled(true);
						Ext.getCmp('btnSimpanKeluar').setDisabled(true);		

						Datasave_kbs(true);
						RefreshDataFilter_kbs(false);
						TRLookUps_kbs.close();
					}
				}, '-', 
				{
					text: 'Hapus',
					id:'btnHapus',
					tooltip: 'Remove the selected item',
					iconCls: 'remove',
					handler: function() 
					{ 
						Ext.Msg.show
						(
							{
							   title:'Hapus',
							   msg: 'Apakah transaksi ini akan dihapus ?', 
							   buttons: Ext.MessageBox.YESNO,
							   fn: function (btn) 
							   {			
								   if (btn =='yes') 
									{
										DataDelete_kbs();
										RefreshDataFilter_kbs(false);									
									} 
							   },
							   icon: Ext.MessageBox.QUESTION
							}
						);
					}
				}, '-', 
				{
					text: 'Lookup',
					id:'btnLookup',
					tooltip: 'Lookup Account',
					iconCls: 'find',
					handler: function() 
					{

						if (mCtrFokus==="txtReferensi_kbs")//"txtReferensi_kbs")
						{
							var StrKriteria;
							if (selectUnitKerja_kbs != "")
							{
							
							//Update dang 22092016
								//StrKriteria = " WHERE ARH.APP_RKP_SP3D=1 AND APP_SP3D = 1 AND SP3.KD_UNIT_KERJA='" + selectUnitKerja_kbs + "'  and IS_JALUR in (1) and TAHAP_PROSES in (2) AND SP3.TAHUN_ANGGARAN_TA = " + thn
								StrKriteria = " WHERE ARH.APP_RKP_SP3D=1 AND APP_SP3D = 1 AND SP3.KD_UNIT_KERJA='" + selectUnitKerja_kbs + "'  and SP3.IS_JALUR in (1) and TAHAP_PROSES in (2) AND SP3.TAHUN_ANGGARAN_TA = " + thn
							}
							else
							{
								//StrKriteria = " WHERE APP_SP3D = 1 and  TAHAP_PROSES in (2) "
								////Update dang 22092016
							  StrKriteria = " WHERE ARH.APP_RKP_SP3D=1 AND APP_SP3D = 1  and TAHAP_PROSES in (2) AND AC.REFERENSI is null  AND SP3.TAHUN_ANGGARAN_TA  = " + thn
							}
							var p = new mRecord_kbs
							(
								{
									ACCOUNT: 'a',
									NAMAACCOUNT: 'b',
									DESCRIPTION: 'c',//Ext.get('txtCatatan_kbs').getValue(),//varDesc2,//Ext.get('txtCatatan_kbs').getValue(),
									VALUE: '',								
									LINE:''
								}
							);							
							FormLookupsp3d(dsDTLTRList_kbs,p, StrKriteria,nASALFORM_KBS,thn);
						}
						//ditutup untuk validasi textboxt referensi
						// else
						// {		
							// var p = new mRecord_kbs
							// (
								// {
									// ACCOUNT: '',
									// NAMAACCOUNT: '',
									// DESCRIPTION: Ext.get('txtCatatan_kbs').getValue(),
									// VALUE: '',								
									// LINE:''
								// }
							// );
							// FormLookupAccount(" Where left(Account,1) in ('1','2','3','5')  AND TYPE ='D'  ",dsDTLTRList_kbs,p,true,'',true);
						// }
					}
				}, '-',
				{
				    text: 'Approve',
					id:'btnApprove',
				    tooltip: 'Approve',
				    iconCls: 'approve',
				    handler: function() 
					{ 
						// if(ValidasiAppEntry_kbs('approve data') == 1){
							FormApprove(Ext.get('txtTotal_kbs').getValue(),'6',Ext.get('txtNo_kbs').getValue(),Ext.get('txtCatatan_kbs').getValue(),Ext.get('dtpTanggal_kbs').dom.value)
						//  	}
					}
				}
				, /*'-',
				{
				    text: 'Batal KBS',
					id:'btnUnApprove',
				    tooltip: 'Batal KBS',
				    iconCls: 'remove',
				    handler: function() 
					{ 
						UnApprove_kbs()
					}
				}
				,*/ '-', '->', '-',
				{
					text: 'Cetak',
					tooltip: 'Print',
					iconCls: 'print',					
					handler: function() 
					{
					if(Validasi_viKBS() == 1)
						{
							var criteria = GetCriteria_viKBS();
							ShowReport('', '011509', criteria);
						}
					}
				}
			]
		}
	);  // end Head panel
		
	var GDtabDetail_kbs = new Ext.TabPanel
	(
		{
			region: 'center',
			id: 'GDtabDetail_kbs',
			activeTab: 0,
			//anchor: '100% 46%',
			anchor: '100% 48%',
			border: false,
			plain: true,
			defaults: 
			{
				autoScroll: false
			},
			items: GetDTLTRGrid_kbs(),
			tbar: 
			[
			{
				text: 'Tambah Baris',
				id:'btnTambahBaris',
				tooltip: 'Tambah Record Baru ',
				iconCls: 'add',
				hidden:true,
				handler: function() 
				{ 
					TambahBaris_kbs();
				}
			}, '-', 
			{
				text: 'Hapus Baris',
				id:'btnHapusBaris',
				tooltip: 'Remove the selected item',
				iconCls: 'remove',
				hidden:true,
				handler: function()
					{
						if (dsDTLTRList_kbs.getCount() > 0 )
						{						
							if (cellSelectedDet_kbs != undefined)
							{
								if(CurrentTR_kbs != undefined)
								{
									HapusBaris_kbs();
								}
							}
							else
							{
								ShowPesanWarning_kbs('Silahkan pilih dahulu baris yang akan dihapus','Hapus baris');
							}
						}
					}
			}
		] 
	}
);
			
	var Total = new Ext.Panel
	(
		{
			frame: false,
			layout: 'form',
			width: 500,
			border:false,
			id:'PnlTotalPenerimaanMhs',
			labelAlign:'right',
			labelWidth:50,
			style: 
			{
				'margin-top': '5.9px',
				'margin-left': '457px'
			},
			items: 
			[
				{
					xtype: 'textfield',
					id:'txtTotal_kbs',
					name:'txtTotal_kbs',
					fieldLabel: 'Total ',
					readOnly:true,
					style:
					{	
						'text-align':'right',
						'font-weight':'bold'
					},
					// value:'0',
					width: 185
				}
			]
		}
	);

	var Formload_kbs = new Ext.Panel
	(
		{
			id: 'Formload_kbs',
			region: 'center',
			width:'100%',
			anchor:'100%',
			layout: 'form',
			title: '',
			bodyStyle: 'padding:15px',
			border: true,
			bodyStyle: 'background:#FFFFFF;',
			shadhow: true,
			//iconCls: 'GL',
			items: [pnlTR_kbs, GDtabDetail_kbs,Total]							  

		}
	);
	
	//RefreshDataDetail_kbs();
	return Formload_kbs						
};
 ///---------------------------------------------------------------------------------------///
 
function Cetak_kbs()
{
	var strKriteria;
	
	if (Ext.get('txtNo_kbs').dom.value !='' && Ext.get('dtpTanggal_kbs').dom.value !='')
	{
		strKriteria = 'cso_date=' + Ext.get('dtpTanggal_kbs').dom.value;
		strKriteria += '##@@## cso_number=' + Ext.get('txtNo_kbs').dom.value;
		strKriteria += '##@@##' + 1;		
		ShowReport('', '231009', strKriteria);	
	};
};
 
 var gridDTLTR_kbs='';
function GetDTLTRGrid_kbs() 
{
	// grid untuk detail transaksi	
	var fldDetail = ['cso_number', 'cso_date', 'line', 'account', 'description', 'value', 'posted', 'namaaccount']
	
	dsDTLTRList_kbs = new WebApp.DataStore({ fields: fldDetail })
	dsTmp_kbs= new WebApp.DataStore({ fields: fldDetail })
	
	gridDTLTR_kbs = new Ext.grid.EditorGridPanel
	(
		{
			title: 'Detail Pencairan Sementara' ,
			stripeRows: true,
			store: dsDTLTRList_kbs,
			id:'Dttlgrid_kbs',
			border: false,
			columnLines:true,
			frame:true,
			anchor: '100% 100%',			
			sm: new Ext.grid.CellSelectionModel
			(
				{
					singleSelect: true,
					listeners: 
					{
						cellselect: function(sm, row, rec)
						{
							cellSelectedDet_kbs =dsDTLTRList_kbs.getAt(row);
							CurrentTR_kbs.row = row;
							//CurrentTRGL.data = cellSelectedGLDet;
						}
					}
				}
			),
			cm: TRDetailColumModel_kbs()
			, viewConfig: 
			{
				forceFit: true
			}
		}
	);
			
	return gridDTLTR_kbs;
};


function GetParamProsesCekTotalRekap(param)
{
	var x = ''
	x=param;
	x += "##"+Ext.get('txtReferensi_kbs').dom.value
	
	
	return x;
};
function INPUTDetailSP3D_kbs(rowdata)
{
    /* 
	#### AWAL 
	var strKriteria_kbs
    strKriteria_kbs = " WHERE NOMOR='" + Ext.get('txtReferensi_kbs').dom.value + "' " // + "##" + Ext.get('txtCatatan_kbs').dom.value
	dsDTLTRList_kbs.load
	(
		{ 
			params: 
			{ 	
				Skip: 0,
				Take: 20,
				Sort: 'Line',
				Sortdir: 'ASC',
				target:'VIviewsp3d_kbs',
				param: strKriteria_kbs
			}
		}
	);	
	return dsDTLTRList_kbs; */
	
	Ext.Ajax.request ({
		url: baseURL + "index.php/keuangan/functionKBS/getDetailPPD",
		params: {
			tahun_anggaran_ta		:	rowdata.tahun_anggaran_ta,
			kd_unit_kerja			:	rowdata.kd_unit_kerja,
			no_sp3d					:	rowdata.no_sp3d_rkat,
			tgl_sp3d				:	rowdata.tgl_sp3d_rkat,
			prioritas				:	rowdata.prioritas,
			deskripsi				:	Ext.getCmp('txtCatatan_kbs').getValue(),
		},
		failure: function(o)
		{
			ShowPesanError_kbs('Error menampilkan data detail PPD !', 'Error');
		},	
		success: function(o) 
		{   
			dsDTLTRList_kbs.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsDTLTRList_kbs.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsDTLTRList_kbs.add(recs);
				console.log(dsDTLTRList_kbs.data);
				// insertToGridDetailEntryKBS(dsDTLTRList_kbs);
				gridDTLTR_kbs.getView().refresh();
			} else {
				ShowPesanError_kbs('Gagal menampilkan data detail PPD', 'Error');
			};
		}
	});
	
}


function RefreshDataDetail_kbs(no_kbs)
{	

	console.log(no_kbs);
    /* var strKriteria_kbs
    strKriteria_kbs = "Where CD.CSO_Number ='" + Rowdata + "' ";	
	dsDTLTRList_kbs.load
	(
		{ 
			params: 
			{ 	
				Skip: 0,
				Take: 20,
				Sort: 'Line',
				Sortdir: 'ASC',
				target:'VIViewPenerimaanMhsDetail',
				param: strKriteria_kbs
			}
		}
	); */
	Ext.Ajax.request ({
		url: baseURL + "index.php/keuangan/functionKBS/getDetailKBS",
		params: {
			no_kbs: no_kbs
		},
		failure: function(o)
		{
			ShowPesanError_kbs('Error menampilkan data Pencairan Sementara !', 'Error');
		},	
		success: function(o) 
		{   
			dsDTLTRList_kbs.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsDTLTRList_kbs.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsDTLTRList_kbs.add(recs);
			} else {
				ShowPesanError_kbs('Gagal menampilkan data Pencairan Sementara', 'Error');
			};
		}
	});
	return dsDTLTRList_kbs;
}

///---------------------------------------------------------------------------------------///
			
function TRDetailColumModel_kbs() 
{
	return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(), 
			{
				id: 'Account_kbs',
				name: 'Account_kbs',
				header: "Account",
				dataIndex: 'account',
				sortable: false,
				anchor: '10%',
				editor: new Ext.form.TextField
				(
					{
						id:'fieldAcc_kbs',
						allowBlank: false,
						enableKeyEvents : true,
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{

									//varDesc2

									var varDesc3='';
									if (dsDTLTRList_kbs.getCount()>0){
										varDesc3=dsDTLTRList_kbs.data.items[0].data.description;
									}

									var p = new mRecord_kbs
									(
										{
											account: '',
											namaaccount: '',
											description: varDesc3,//Ext.get('txtCatatan_kbs').getValue(),
											value: '',								
											line:''
										}
									);
									FormLookupAccount(" Where left(A.Account,1) in ('1','2','3','5')  and A.Account like '" 
										+ Ext.get('fieldAcc_kbs').dom.value + "%'  AND A.TYPE ='D'  ",dsDTLTRList_kbs,p,true,'',false);
								} 
							}
						}
					}
				),
				width: 70
			}, 
			{
				id: 'Name_kbs',
				name: 'Name_kbs',
				header: "Nama Account",
				dataIndex: 'namaaccount',
				anchor: '30%',
				editor: new Ext.form.TextField
				(
					{
						id:'fieldAccName_kbs',
						allowBlank: false,
						enableKeyEvents : true,
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									var varDesc3='';
									if (dsDTLTRList_kbs.getCount()>0){
										varDesc3=dsDTLTRList_kbs.data.items[0].data.description;
									}

									var p = new mRecord_kbs
									(
										{
											account: '',
											namaaccount: '',
											description: varDesc3,//Ext.get('txtCatatan_kbs').getValue(),		
											value: '',								
											line:''
										}
									);
									FormLookupAccount(" Where left(A.Account,1) in ('1','2','3','5')  and A.Name like '%" 
										+ Ext.get('fieldAccName_kbs').dom.value + "%' AND A.TYPE ='D' ",dsDTLTRList_kbs,p,true,'',false);									
								} 
							}
						}
					}
				)
			}, 
			{
				id: 'Description_kbs',
				name: 'Description_kbs',
				header: "Keterangan",
				anchor: '30%',
				dataIndex: 'description',				
				editor: new Ext.form.TextField
				(	
					{
						allowBlank: true
					}
				)
			}, 
			{
				id: 'Value_kbs',				
				name: 'Value_kbs',
				header: "Jumlah (Rp.)",
				anchor: '15%',
				dataIndex: 'value', 
				align:'right',
				renderer: function(v, params, record) 
				{
				  
					return formatCurrencyDec(record.data.value);					
				},	
				editor: new Ext.form.NumberField
				(
					{
						id:'fieldDB_kbs',
						allowBlank: false,
						enableKeyEvents : true,
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{									
									CalcTotal_kbs(CurrentTR_kbs.row);									
								}
							},
							'blur' : function()
							{									
								CalcTotal_kbs(CurrentTR_kbs.row);									
							}
						}
					}
				)
			} 
			// ,{
			// 	id: 'JenisPrior_KBS',
			// 	name: 'JenisPrior_KBS',
			// 	header: "Prioritas",
			// 	anchor: '30%',
			// 	dataIndex: 'PRIORITAS_SP3D',
			// 	hidden:true
			// }, 
			// {
			// 	id: 'urut_KBS',
			// 	name: 'urut_KBS',
			// 	header: "Urut",
			// 	anchor: '30%',
			// 	dataIndex: 'URUT_SP3D',
			// 	hidden:true
			// }, 
			// {
			// 	id: 'NoProg_KBS',
			// 	name: 'NoProg_KBS',
			// 	header: "No Program",
			// 	anchor: '30%',
			// 	dataIndex: 'NO_PROGRAM_SP3D',
			// 	hidden:true
			// }, 
			// {
			// 	id: 'Jenisrkat_KBS',
			// 	name: 'Jenisrkat_KBS',
			// 	header: "Jenis Rkat",
			// 	anchor: '30%',
			// 	dataIndex: 'JNS_RKAT',
			// 	hidden:true
			// }
		]
	)
};

///---------------------------------------------------------------------------------------///
function DataInit_kbs(rowdata) 
{

	DataAddNew_kbs = false;
	Ext.get('comboUnitKerja_kbs').dom.value = rowdata.nama_unit_kerja;	
	selectUnitKerja_kbs = rowdata.kd_unit_kerja;
	
	Ext.get('comboBayar_kbs').dom.value= rowdata.payment;
	selectBayar_kbs = rowdata.pay_code;
	
	Ext.get('comboAktivaLancar_kbs').dom.value= rowdata.account + ' - '+rowdata.namaaccount;//rowdata.NAMAACCOUNT;
	selectAktivaLancar_kbs = rowdata.account;

    Ext.get('txtNo_kbs').dom.value = rowdata.cso_number;
	
    Ext.get('txtTerimaDari_kbs').dom.value = rowdata.personal;
	Ext.get('dtpTanggal_kbs').dom.value = ShowDate(rowdata.cso_date);   
	
	console.log(rowdata.jumlah);
	Ext.get('txtTotal_kbs').dom.value = formatCurrencyDec(rowdata.amount);
	Ext.get('txtJml_kbs').dom.value = formatCurrencyDec(rowdata.jumlah);
	Ext.get('txtCatatan_kbs').dom.value = rowdata.notes;
	Ext.get('txtNoPembayaran_kbs').dom.value = rowdata.pay_no;
	Ext.get('txtReferensi_kbs').dom.value = rowdata.referensi;	
	Ext.getCmp('ChkApprove_kbs').setValue(rowdata.approve);	
	RefreshDataDetail_kbs(Ext.get('txtNo_kbs').dom.value);
	// ButtonDisabled_kbs(true);
	if (rowdata.APPROVE)
	{
		ButtonDisabled_kbs(true);
	}else
	{
		ButtonDisabled_kbs(false);
	}
	if (Ext.get('txtNo_kbs').dom.value != '')
	{
		Ext.getCmp('btnLookup').setDisabled(true);
	}
	 
};

///---------------------------------------------------------------------------------------///
function AddNew_kbs() 
{
	DataAddNew_kbs = true;	
		
	Ext.get('txtNo_kbs').dom.value = '';	
	Ext.getCmp('ChkApprove_kbs').setValue(false);	
	Ext.get('txtTerimaDari_kbs').dom.value = '';
	Ext.get('txtNoPembayaran_kbs').dom.value = '';
	Ext.get('txtTotal_kbs').dom.value = '0';
	Ext.get('txtJml_kbs').dom.value = '0';
	Ext.get('txtCatatan_kbs').dom.value = '';	
	Ext.get('comboUnitKerja_kbs').dom.value='';
	Ext.get('comboBayar_kbs').dom.value='';
	Ext.get('comboAktivaLancar_kbs').dom.value = '';
	Ext.get('txtReferensi_kbs').dom.value = '';	
	// Ext.get('dtpTanggal_kbs').dom.value = ShowDate(now_kbs);
	dsDTLTRList_kbs.removeAll();
	selectUnitKerja_kbs='';
	selectBayar_kbs='';
	rowSelected_kbs=undefined;			
	ButtonDisabled_kbs(false);
	
	Ext.Ajax.request ({
		url: baseURL + "index.php/keuangan/functionKBS/get_akun_kas",
		params: {
			text:''
		},
		failure: function(o)
		{
			ShowPesanError_kbs('Error menampilkan data unit kerja !', 'Error');
		},	
		success: function(o) 
		{   
			
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				console.log(cst.nama_akun,cst.akun);
				Ext.getCmp('comboAktivaLancar_kbs').setValue(cst.akun+' - '+cst.nama_akun);	
				selectAktivaLancar_kbs=cst.akun;
			}
		}
	});
	
	
};

function getParam_kbs() 
{
	var tmp_amount = getAmount_kbs(Ext.get('txtTotal_kbs').getValue());
	var params = 
	{
		Table: 'viACC_CSO_KBS',
		CSO_Number:Ext.get('txtNo_kbs').getValue(),
		CSO_Date:Ext.get('dtpTanggal_kbs').getValue(),
		Unit_Kerja:selectUnitKerja_kbs,
		Personal:Ext.get('txtTerimaDari_kbs').getValue(),
		Account:selectAktivaLancar_kbs,
		Pay_Code:selectBayar_kbs,
		Pay_No:Ext.get('txtNoPembayaran_kbs').getValue(),
		Currency:'Rp',
		Kurs:'1',
		Type:'1', //0=penerimaan // 1 = pengeluaran
		GC_Code:'0', //0=penerimaan
		Kd_User:'0',
		Amount: tmp_amount.substring(0,tmp_amount.length-3), //menghilangkan ,00
		Notes1:Ext.get('txtCatatan_kbs').getValue(),
		KD_UNIT_KERJA:selectUnitKerja_kbs,//Ext.getCmp('comboUnitKerja_kbs').getValue(),
		REFERENSI:Ext.get('txtReferensi_kbs').dom.value,
		KATEGORI:3, //3 => KBS
		IS_DETAIL:0,
		IS_APPROVE:0,
		REFF_ID:REFFID_SP3D_kbs,
		//List:getArrDetail_kbs(),
		JmlField:4,
		JmlList:dsDTLTRList_kbs.getCount()
	};
	
	params['jumlah']=dsDTLTRList_kbs.getCount()
	for (var L=0; L<dsDTLTRList_kbs.getCount(); L++)
	{
		var tmp_value = dsDTLTRList_kbs.data.items[L].data.value; //menghilangkan ,00
		params['account_detail'+L]		=dsDTLTRList_kbs.data.items[L].data.account;
		params['name_detail'+L]			=dsDTLTRList_kbs.data.items[L].data.name;
		params['description_detail'+L]	=dsDTLTRList_kbs.data.items[L].data.description;
		params['value'+L]				= tmp_value,
		params['line'+L]				=dsDTLTRList_kbs.data.items[L].data.line;
		params['kd_unit_kerja'+L]		=selectUnitKerja_kbs;
	}
	
	return params
};

function getParamUnApprove_kbs() 
{	
	var params = 
	{
		Table: 'viACC_CSO_KBS',   		
		IS_APPROVE:1,
		IS_BATAL:1,
		CSO_Number:Ext.get('txtNo_kbs').getValue(),
		CSO_Date:Ext.get('dtpTanggal_kbs').getValue(),
		No_Tag:Ext.get('txtNo_kbs').getValue(), 
		Account:selectAktivaLancar_kbs,
		Date_Tag:TglApprove,
		Amount:getAmount_kbs(Ext.get('txtTotal_kbs').getValue()),		
		note_tag:NoteApprove,
		IS_DEBITinTotal:0,
		Description:Ext.get('txtCatatan_kbs').getValue(),
		kd_user:strKdUser,
		List:getArrDetail_kbs(),	
		IS_DETAIL:1,
		KATEGORI:KDkategori_kbs,
		REFF_ID:REFFID_SP3D_kbs,
		JmlField:4,
		JmlList:dsDTLTRList_kbs.getCount()					
	};
	return params
};

function getParamApprove_kbs(TglApprove, NoteApprove) 
{	
	var params = 
	{		
		IS_APPROVE:1,
		IS_BATAL:0,
		Date_Tag:TglApprove,
		note_tag:NoteApprove,
		CSO_Number:Ext.get('txtNo_kbs').getValue(),
		CSO_Date:Ext.get('dtpTanggal_kbs').getValue(),
		Unit_Kerja:selectUnitKerja_kbs,
		Personal:Ext.get('txtTerimaDari_kbs').getValue(),
		Account:selectAktivaLancar_kbs,
		Pay_Code:selectBayar_kbs,
		Pay_No:Ext.get('txtNoPembayaran_kbs').getValue(),
		Currency:'Rp',
		Kurs:'1',
		Type:'1', //0=penerimaan // 1 = pengeluaran
		GC_Code:'0', //0=penerimaan
		Kd_User:'0',
		Amount:getAmount_kbs(Ext.get('txtTotal_kbs').getValue()),
		Notes1:Ext.get('txtCatatan_kbs').getValue(),
		KD_UNIT_KERJA:selectUnitKerja_kbs,//Ext.getCmp('comboUnitKerja_kbs').getValue(),
		REFERENSI:Ext.get('txtReferensi_kbs').dom.value,
		KATEGORI:3, //3 => KBS
		REFF_ID:REFFID_SP3D_kbs,
		//List:getArrDetail_kbs(),
		JmlField:4,
		JmlList:dsDTLTRList_kbs.getCount()
					
	};
	
	params['jumlah']=dsDTLTRList_kbs.getCount()
	for (var L=0; L<dsDTLTRList_kbs.getCount(); L++)
	{
		params['account_detail'+L]		=dsDTLTRList_kbs.data.items[L].data.account;
		params['name_detail'+L]			=dsDTLTRList_kbs.data.items[L].data.name;
		params['description_detail'+L]	=dsDTLTRList_kbs.data.items[L].data.description;
		params['value'+L]				=dsDTLTRList_kbs.data.items[L].data.value;
		params['line'+L]				=dsDTLTRList_kbs.data.items[L].data.line;
		params['kd_unit_kerja'+L]		=selectUnitKerja_kbs;
	}
	return params
};

///---------------------------------------------------------------------------------------///

function getAmount_kbs(dblNilai)
{
    var dblAmount;
    dblAmount = dblNilai.replace('Rp.', '')
    for (var i = 0; i < dblAmount.length; i++) {
        var x = dblAmount.substr(i, 1)
        if (x === '.') {
            dblAmount = dblAmount.replace('.', '');
        }
    }    
	

    return dblAmount
};

function getItemPanelInput_kbs(lebar)
{
	var items = 
	{
		layout:'fit',
		anchor:'100%',
		width: lebar - 36,
		height: 185,//130,
		labelAlign: 'right',
		bodyStyle: 'padding:7px 7px 7px 7px',
		items:
		[
			{
				columnWidth:.9,
				width:lebar-36,
				layout: 'form',
				border:false,
				items: 
				[
					getItemPanelNo_kbs(lebar),
					getItemPanelTerimaDari_kbs(lebar),
					getItemPanelAktivaLancar_kbs(lebar),
					getItemPanelPayModePayNumber_kbs(lebar),
					getItemPanelCatatan_kbs(lebar)
				]
			}
		]
	};
	return items;			
};

function getItemPanelAktivaLancar_kbs(lebar)
{
	var items = 			
	{
	layout:'column',
	border:false,
	width:lebar-36,
	items:
		[
			{
				// columnWidth:0.98,
				columnWidth:.5,
				layout: 'form',
				border:false,
				labelWidth:111,
				items: 
				[
					mcomboAktivaLancar_kbs(),					
				]
			},
			{
				columnWidth:.5,
				region:'Right',
				border:false,				
				layout: 'form',
				items: 				
				[mcomboUnitKerja_kbs()]
			}
		]
	}
	return items;	
}; 

function getItemPanelCatatan_kbs(lebar)
{
	var items = 			
	{
	layout:'column',
	border:false,
	width:lebar-36,
	items:
		[
			{
				columnWidth:0.98,
				layout: 'form',
				border:false,
				labelWidth:111,
				items: 
				[
					{
						xtype: 'compositefield',
						fieldLabel: 'Referensi ',
						items: 
						[
							{
								xtype:'textfield',
								fieldLabel: '',
								name: 'txtReferensi_kbs',
								id: 'txtReferensi_kbs',
								readOnly: true,
								//anchor:'49%',
								width:217,
								// hidden: true,
								listeners:
								{ 
									focus: function() 
									{
									},
									'specialkey': function()
									{
										if(Ext.EventObject.getKey() === 13)
										{
											var StrKriteria=""											
											if (selectUnitKerja_kbs != "")
											{
												StrKriteria = " WHERE APP_SP3D = 1   and  TAHAP_PROSES in (1,2) AND SP3.KD_UNIT_KERJA='" + selectUnitKerja_kbs + "' "							
											}
											else
											{
												StrKriteria = " WHERE APP_SP3D = 1  and  TAHAP_PROSES in (1,2) "
											}							
											var p = new mRecord_kbs
											(
												{
													ACCOUNT: '',
													NAMAACCOUNT: '',
													DESCRIPTION: Ext.get('txtCatatan_kbs').getValue(),
													VALUE: '',								
													LINE:''
												}
											);							
											FormLookupsp3d(dsDTLTRList_kbs,p, StrKriteria,nASALFORM_KBS,thn);								
										}
									}
								}
							},
							{
								xtype:'spacer',
								width:55
							},
							{
								xtype:'displayfield',
								value:'Jml. PPD'
							},
							{
								xtype:'textfield',
								fieldLabel: '',
								name: 'txtJml_kbs',
								id: 'txtJml_kbs',
								readOnly:true,
								width:230,
								style:
								{	
									'text-align':'right',
									'font-weight':'bold'
								}
							}
						]
					},
					{
						xtype:'textfield',
						fieldLabel: 'Keterangan ',
						name: 'txtCatatan_kbs',
						id: 'txtCatatan_kbs',
						autoCreate: {tag: 'input', type: 'text', size: '', autocomplete: 'off', maxlength: '65'},
						anchor:'99.9%'
					}
				]
			}
		]
	}
	return items;	
}; 



function getItemPanelNo_kbs(lebar)
{
	var items = 			
	{
	layout:'column',
	border:false,
	width:lebar-36,
	items:
		[
			{
				columnWidth:.5,
				layout: 'form',
				border:false,
				labelWidth:111,
				items:
				[
					{
						xtype:'textfield',
						fieldLabel: 'No. ' + NamaForm_kbs, //Pencairan ',
						name: 'txtNo_kbs',
						id: 'txtNo_kbs',
						readOnly:true,
						anchor:'95%'
					}
				]
			},
			{
				columnWidth:.5,
				layout: 'form',
				border:false,
				labelWidth:100,
				items: 
				[
					{
						xtype:'checkbox',
						fieldLabel: 'Approve ',
						name: 'ChkApprove_kbs',
						id: 'ChkApprove_kbs',
						disabled:true,
						anchor:'95%'
					}
				]
			}
		]
	}
	return items;	
};

function getItemPanelPayModePayNumber_kbs(lebar)
{
	var items = 			
	{
	layout:'column',
	width:lebar-36,
	border:false,
	items:
	[
		{
			columnWidth:.5,
			layout: 'form',
			border:false,
			labelWidth:111,
			items: 
			[mcomboBayar_kbs()]			
		},
		{
			columnWidth:.5,
			layout: 'form',
			border:false,
			items: 
			[
				{
					xtype:'textfield',
					fieldLabel: 'No. Pembayaran ',
					name: 'txtNoPembayaran_kbs',
					id: 'txtNoPembayaran_kbs',
					anchor:'96%'						
				}
			]
		}
	]
	}
	return items;	
}

function getItemPanelTerimaDari_kbs(lebar)
{
	var items = 			
	{
	layout:'column',
	width:lebar-36,
	border:false,
	items:
		[
			{
				columnWidth:.5,
				layout: 'form',
				border:false,
				labelWidth:111,
				items: 
				[
					{
						xtype:'textfield',
						fieldLabel: 'Dibayarkan Kepada ',
						name: 'txtTerimaDari_kbs',
						id: 'txtTerimaDari_kbs',
						anchor:'95%'
					}
				]
			},
			{
				columnWidth:.5,
				layout: 'form',
				border:false,
				items: 
				[
					{
						xtype: 'datefield',
                        fieldLabel: 'Tanggal ',
                        id: 'dtpTanggal_kbs',
                        name: 'dtpTanggal_kbs',
                        format: 'd/M/Y',
						value:now_kbs.format('d/M/Y'),
                        anchor: '70%'
					}
				]
			}
		]
	}
	return items;	
}

function mcomboUnitKerja_kbs()
{
	var Field = ['kd_unit', 'nama_unit', 'unitkerja'];
	dsUnitKerja_kbs = new WebApp.DataStore({ fields: Field });
	
  var comboUnitKerja_kbs = new Ext.form.ComboBox
	(
		{
			id:'comboUnitKerja_kbs',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih Unit Kerja...',
			fieldLabel: 'Unit Kerja',			
			align:'Right',
			anchor:'96%',
			store: dsUnitKerja_kbs,
			valueField: 'kd_unit',
			displayField: 'nama_unit',
			listeners:  
			{
				'select': function(a,b,c)
				{   
					//selectAktivaLancar_kbs = "";
					//Ext.getCmp('comboAktivaLancar_kbs').setValue("");
				//	Ext.get('comboAktivaLancar_kbs').dom.value = "";
					
					selectUnitKerja_kbs=b.data.kd_unit ;
					kdUnit_kbs=b.data.kd_unit ;
					
					var kriteria
					if (kdUnit_kbs != undefined){
						kriteria = " and Account like '%"+ kdUnit_kbs +"'";
					}else{
						kriteria = " ";
					}
					
					/*dsAktivaLancarPenerimaanMhs.load
					(
						{
							params:
							{
								Skip: 0,
								Take: 1000,
								Sort: '',
								Sortdir: 'ASC',
								target: 'viCboAktivaLancar',
								param: kriteria
							}
						}
					);*/
					
				} 
			}
		}
	);
	
	/* dsUnitKerja_kbs.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'KD_UNIT_KERJA',
			    Sortdir: 'ASC',
			    target: 'viCboUnitKerja',
			    // param: gstrListUnitKerja + "##@@##" + 0
			    //param: "kdunit="+gstrListUnitKerja
			    param: ""
			}
		}
	); */
	
	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionPlafondUnitKerja/getUnitKerjaInput",
		params: {
			text:''
		},
		failure: function(o)
		{
			ShowPesanError_kbs('Error menampilkan data unit kerja !', 'Error');
		},	
		success: function(o) 
		{   
			dsUnitKerja_kbs.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsUnitKerja_kbs.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsUnitKerja_kbs.add(recs);
				// gridDTLTR_PaguGNRL.getView().refresh();
			} else {
				ShowPesanError_kbs('Gagal menampilkan data unit kerja', 'Error');
			};
		}
	});
	
	return comboUnitKerja_kbs;
} ;

function mcomboAktivaLancar_kbs()
{
   var strfilterkbs='';
	strfilterkbs="KBS"


	var Field = ['account','name','groups','akun'];
	dsAktivaLancarPenerimaanMhs = new WebApp.DataStore({ fields: Field });
	/* dsAktivaLancarPenerimaanMhs.load
	(
		{
			params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: '',
			    Sortdir: 'ASC',
			    target: 'viewCboaktivaFilter',
			    param: strfilterkbs
			}
		}
	); */
	
	Ext.Ajax.request({
		url: baseURL + "index.php/keuangan/functionKBS/getAktivaLancar",
		params: {query:''},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			comboAktivaLancar_kbs.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsAktivaLancarPenerimaanMhs.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsAktivaLancarPenerimaanMhs.add(recs);
			}
		}
	});
 var comboAktivaLancar_kbs = new Ext.form.ComboBox
	(
		{
			id:'comboAktivaLancar_kbs',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih Kas / Bank...',
			fieldLabel: 'Kas / Bank ',			
			align:'Right',
			anchor:'95%',
			store: dsAktivaLancarPenerimaanMhs,
			valueField: 'account',
			displayField: 'akun',
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectAktivaLancar_kbs=b.data.account ;
				
					// ProsesCekTotal(b.data.Account,true);
				} 
			}
		}
	);
	
	return comboAktivaLancar_kbs;
} ;

function RefreshData_kbs()
{			
	
	dsTRList_kbs.load
	(
		{ 
			params: 
			{ 	
				Skip: 0,
				Take: selectCount_kbs,
				Sort: 'CSO_DATE',
				Sortdir: 'ASC',
				target:'VIViewPenerimaanMhs',
				param: " Where KATEGORI='" + KDkategori_kbs + "' "
			}
		}
	);
	return dsTRList_kbs;
};

// function mComboMaksData_kbs()
// {
//   var cboMaksDataKBS = new Ext.form.ComboBox
// 	(
// 		{
// 			id:'cboMaksDataKBS',
// 			typeAhead: true,
// 			triggerAction: 'all',
// 			lazyRender:true,
// 			mode: 'local',
// 			emptyText:'',
// 			fieldLabel: 'Maks.Data ',			
// 			width:50,
// 			store: new Ext.data.ArrayStore
// 			(
// 				{
// 					id: 0,
// 					fields: 
// 					[
// 						'Id',
// 						'displayText'
// 					],
// 				data: [[1, 50], [2, 100],[3, 200],[4, 500],[5, 1000]]
// 				}
// 			),
// 			valueField: 'Id',
// 			displayField: 'displayText',
// 			value:selectCount_kbs,
// 			listeners:  
// 			{
// 				'select': function(a,b,c)
// 				{   
// 					selectCount_kbs=b.data.displayText ;
// 					RefreshDataFilter_kbs(false);
// 				} 
// 			}
// 		}
// 	);
// 	return cboMaksDataKBS;
// };

function GetCriteriaGridUtama(){
	var criteria = '';
	var criteria_approved = '';
	var criteria_kbs = '';
	var criteria_tgl = '';
	
	
	
	if(Ext.getCmp('chkFilterApproved_kbsView').getValue() == true){
		criteria_approved = " and a.no_tag is not null and a. date_tag is not null ";
	}else{
		criteria_approved = " and a.no_tag isnull and a. date_tag isnull ";
		
	}
	if (Ext.getCmp('txtNoFilter_kbs').getValue() != ''){
		criteria_kbs = " and a.cso_number ='"+ Ext.getCmp('txtNoFilter_kbs').getValue() +"' ";
	}

	if (Ext.getCmp('chkWithTgl_kbs').getValue() == true){
		criteria_tgl = " and a.cso_date between '"+ Ext.get('dtpTglAwalFilter_kbs').getValue() +"'  and '"+ Ext.get('dtpTglAkhirFilter_kbs').getValue() +"' ";
	}

	criteria = " "+criteria_approved+" "+criteria_tgl+" "+criteria_kbs+" ";
	
	if (Ext.getCmp('comboUnitKerja_kbsView').getValue() == '000' || Ext.getCmp('comboUnitKerja_kbsView').getValue() == 000){
		criteria = "SEMUA~"+criteria;
		
	}else{
		criteria = "  and a.kd_unit_kerja = '"+Ext.getCmp('comboUnitKerja_kbsView').getValue()+"' "+criteria;
	}
	return criteria;
}

function RefreshDataFilter_kbs(mBol) 
{   
	var criteria = GetCriteriaGridUtama();

	dsTRList_kbs.removeAll();
	dsTRList_kbs.load({
		params:{
			Skip: 0,
			Take: selectCount_kbs,
			Sort: '',
			Sortdir: 'ASC',
			target: 'vi_viewdata_kbs',
			param: criteria
		},
		callback:function(){
			var grListGridTR_kbs=Ext.getCmp('grListTR_kbs').getStore().data.items;
				for(var i=0,iLen=grListGridTR_kbs.length; i<iLen;i++){
					
					if(grListGridTR_kbs[i].data.approve == 't'){
						grListGridTR_kbs[i].data.approve_tmp = true;
						
					} 
				}
			Ext.getCmp('grListTR_kbs').getView().refresh();
		}
	}); 
	/* var strPenerimaanMhs='';
	strPenerimaanMhs=" Where KATEGORI='" + KDkategori_kbs + "' "
	if (Ext.get('txtNoFilter_kbs').getValue() != '') 
	{
		if (strPenerimaanMhs ==='')
		{
			strPenerimaanMhs = " Where CS.CSO_NUMBER like '%" + Ext.get('txtNoFilter_kbs').getValue() + "%' ";
		}
		else
		{
			strPenerimaanMhs += " AND CS.CSO_NUMBER like '%" + Ext.get('txtNoFilter_kbs').getValue() + "%' ";
		}
	}else
	{
		if (Ext.getCmp('chkWithTgl_kbs').getValue() === true) 
		{
			if (strPenerimaanMhs ==='')
			{
				strPenerimaanMhs = " Where CS.CSO_DATE >='" + FormatDateReport(Ext.getCmp('dtpTglAwalFilter_kbs').getValue())  + "' ";
				strPenerimaanMhs += " and  CS.CSO_DATE <='" + FormatDateReport(Ext.getCmp('dtpTglAkhirFilter_kbs').getValue())  + "' ";
			}
			else
			{
				strPenerimaanMhs += " and (CS.CSO_DATE >='" + FormatDateReport(Ext.getCmp('dtpTglAwalFilter_kbs').getValue())  + "' ";
				strPenerimaanMhs += " and  CS.CSO_DATE <='" + FormatDateReport(Ext.getCmp('dtpTglAkhirFilter_kbs').getValue())  + "') ";
			}	
		};
	
	}
	if (Ext.getCmp('comboUnitKerja_kbsView').getValue() != '') 
	{
		if (strPenerimaanMhs ==='')
		{
			strPenerimaanMhs = " Where CS.KD_UNIT_KERJA ='" + Ext.getCmp('comboUnitKerja_kbsView').getValue()  + "' ";
		}
		else
		{
			strPenerimaanMhs += " AND CS.KD_UNIT_KERJA ='" + Ext.getCmp('comboUnitKerja_kbsView').getValue()  + "' ";
		}
	};
	if(Ext.getCmp('chkFilterApproved_kbsView').getValue() === true)
		{
			if(strPenerimaanMhs != "")
			{
				strPenerimaanMhs += " and LEN(CS.NO_TAG) <> 0 ";
			}
			else
			{
				strPenerimaanMhs = " Where LEN(CS.NO_TAG) <> 0 ";
			}
			
		}else{
			if(strPenerimaanMhs != "")
			{
				strPenerimaanMhs += " and LEN(CS.NO_TAG) is null  ";
			}
			else
			{
				strPenerimaanMhs = " Where LEN(CS.NO_TAG) is null  ";
			}
		}
	
	//strPenerimaanMhs+="and U.KD_UNIT_KERJA IN ("+gstrListUnitKerja+")"
	strPenerimaanMhs+=""
    if (strPenerimaanMhs != undefined) 
    {  
		dsTRList_kbs.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCount_kbs, 
					Sort: 'CSO_DATE', 
					Sortdir: 'ASC', 
					target:'VIViewPenerimaanMhs',
					param: strPenerimaanMhs
				}			
			}
		);        
    }
	else
	{
	    RefreshDataFilter_kbs(true);
	} */
};


function mcomboBayar_kbs()
{
	var Field = ['pay_code', 'payment'];
	dsBayarPenerimaanMhs = new WebApp.DataStore({ fields: Field });
	/* dsBayarPenerimaanMhs.load
	(
		{
			params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: '',
			    Sortdir: 'ASC',
			    target: 'viCboJnsBayar',
			    param: ''
			}
		}
	); */
	
	Ext.Ajax.request({
		url: baseURL + "index.php/keuangan/functionGeneral/getJenisBayar",
		params: {query:''},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			comboBayar_kbs.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsBayarPenerimaanMhs.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsBayarPenerimaanMhs.add(recs);
			}
		}
	});

  var comboBayar_kbs = new Ext.form.ComboBox
	(
		{
			id:'comboBayar_kbs',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih Jenis Pembayaran...',
			fieldLabel: 'Jenis Pembayaran ',			
			align:'Right',
			anchor:'95%',
			store: dsBayarPenerimaanMhs,
			valueField: 'pay_code',
			displayField: 'payment',
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectBayar_kbs=b.data.pay_code ;
				} 
			}
		}
	);
	
	return comboBayar_kbs;
} ;

function Approve_kbs(TglApprove, NoteApprove) 
{
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/keuangan/functionKBS/approve",
			params:  getParamApprove_kbs(TglApprove, NoteApprove), 
			success: function(o) 
			{				
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					Ext.getCmp('ChkApprove_kbs').setValue(true)
					ShowPesanInfo_kbs('Data berhasil di Approve','Approve');
					RefreshDataFilter_kbs(false);								
				}
				else if (cst.success === false && cst.pesan === 0 )
				{
					ShowPesanWarning_kbs('Data tidak berhasil di Approve','Edit Data');
				}
				else 
				{
					ShowPesanError_kbs('Data tidak berhasil di Approve, ' + cst.pesan,'Approve');
				}										
			}
		}
	)
}

function UnApprove_kbs() 
{
	Ext.Ajax.request
	(
		{
			url: "./Datapool.mvc/UpdateDataObj",
			params:  getParamUnApprove_kbs(), 
			success: function(o) 
			{				
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					ShowPesanInfo_kbs('Proses Batal KBS Berhasil','Batal KBS');
					RefreshDataFilter_kbs(false);								
				}
				else if (cst.success === false && cst.pesan === 0 )
				{
					ShowPesanWarning_kbs('Proses Batal KBS Tidak Berhasil','Batal KBS');
				}
				else 
				{
					ShowPesanError_kbs('Proses Batal KBS Tidak Berhasil','Batal KBS');
				}										
			}
		}
	)
}

function Datasave_kbs(IsExit) 
{
	if (ValidasiEntry_kbs('Simpan Data') == 1 )
	{
		// if (DataAddNew_kbs == true) 
		// {
		    Ext.Ajax.request
				(
					{
					    //url: "./Datapool.mvc/CreateDataObj",
						url: baseURL + "index.php/keuangan/functionKBS/save",
					    params: getParam_kbs(),
					    success: function(o) {
					        var cst = Ext.decode(o.responseText);
							// Ext.getCmp('btnSimpan').setDisabled(false);
							// Ext.getCmp('btnSimpanKeluar').setDisabled(false);

					        if (cst.success === true) 
							{
					            ShowPesanInfo_kbs('Data berhasil di simpan', 'Simpan Data');
								if (IsExit===false)
								{
									Ext.get('txtNo_kbs').dom.value = cst.CSO_Number;
								}
					            RefreshDataFilter_kbs(false);
								RefreshDataDetail_kbs(Ext.get('txtNo_kbs').dom.value);
								DataAddNew_kbs=false;
								Ext.getCmp('btnLookup').setDisabled(true);
					        }
					        else if (cst.success === false && cst.pesan === 0) 
							{
					            ShowPesanWarning_kbs('Data tidak berhasil di simpan ' + cst.pesan , 'Simpan Data');
					        }
					        else {
					            ShowPesanError_kbs('Data tidak berhasil di simpan, Error : ' + cst.pesan, 'Simpan Data');
					        }
					    }
					}
				)
		// }
		/* else 
		{
			Ext.Ajax.request
			 (
				{
					 url: "./Datapool.mvc/UpdateDataObj",
					 params:  getParam_kbs(), 
					 success: function(o) 
					 {
						
						    var cst = Ext.decode(o.responseText);
							Ext.getCmp('btnSimpan').setDisabled(false);
							Ext.getCmp('btnSimpanKeluar').setDisabled(false);		

							if (cst.success === true)
							{
								ShowPesanInfo_kbs('Data berhasil di edit','Edit Data');
								RefreshDataFilter_kbs(false);								
								RefreshDataDetail_kbs(Ext.get('txtNo_kbs').dom.value);
								Ext.getCmp('btnLookup').setDisabled(true);
							}
							else if (cst.success === false && cst.pesan === 0 )
							{
								ShowPesanWarning_kbs('Data tidak berhasil di edit ' +  cst.pesan ,'Edit Data');
							}
							else {
								ShowPesanError_kbs('Data tidak berhasil di edit '  + cst.pesan ,'Edit Data');
							}
												
					}
				}
			)
		} */
	}
	// else
	// {
		// Ext.getCmp('btnSimpan').setDisabled(false);
		// Ext.getCmp('btnSimpanKeluar').setDisabled(false);		
	// }
};

function ValidasiEntry_kbs(modul)
{
	var x = 1;
	/* if(Ext.get('comboAktivaLancar_kbs').getValue()== '' )
	{
		ShowPesanWarning_kbs('Kas / Bank belum di isi',modul);
		x=0;
	} */
	if(selectAktivaLancar_kbs == undefined || selectAktivaLancar_kbs == '' )
	{
		ShowPesanWarning_kbs('Kas / Bank belum di isi',modul);
		x=0;
	}else
	{
		ProsesCekTotal(selectAktivaLancar_kbs,false);
	}
	
	if (selectBayar_kbs== undefined || selectBayar_kbs== '')
	{
		ShowPesanWarning_kbs('Jenis pembayaran belum di isi',modul);
		x=0;
	};
	if (selectUnitKerja_kbs== undefined || selectUnitKerja_kbs== '')
	{
		ShowPesanWarning_kbs(gstrSatker+' belum di isi',modul);
		x=0;
	};
	if (getAmount_kbs(Ext.get('txtTotal_kbs').getValue()) > getAmount_kbs(Ext.get('txtJml_kbs').getValue()))
	{
		ShowPesanWarning_kbs('Melebihi anggaran '+gstrSp3d,modul);
		x=0;
	};
	
	/* if (getAmount_kbs(Ext.get('txtTotal_kbs').getValue()) > saldoAkunKBS)
	{
		ShowPesanWarning_kbs(' Total Melebihi Saldo Akun',modul);
		x=0;
	}; */
	
	return x;
};

function ValidasiAppEntry_kbs(modul)
{
	var x = 1;

	ProsesCekTotal(selectAktivaLancar_kbs,false);
	
	if(Ext.get('txtNo_kbs').getValue()== '' )
	{
		ShowPesanWarning_kbs('Nomor KBS belum di isi',modul);
		x=0;
	}
	if (getAmount_kbs(Ext.get('txtTotal_kbs').getValue()) > getAmount_kbs(Ext.get('txtJml_kbs').getValue()))
	{
		ShowPesanWarning_kbs('Melebihi anggaran '+gstrSp3d,modul);
		x=0;
	};
	/* if (getAmount_kbs(Ext.get('txtTotal_kbs').getValue()) > saldoAkunKBS)
	{
		ShowPesanWarning_kbs('Total Melebihi Saldo Akun',modul);
		x=0;
	};
	 */
	return x;
};

function ShowPesanWarning_kbs(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};

function ShowPesanError_kbs(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR
		}
	);
};

function ShowPesanInfo_kbs(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO
		}
	);
};

function HapusBaris_kbs()
{
	if (cellSelectedDet_kbs.data.Account != '')
	{
		Ext.Msg.show
		(
			{
			   title:'Hapus Baris',
			   msg: 'Apakah baris ini akan dihapus ?' + ' ' + 'Baris :'+ ' ' + (CurrentTR_kbs.row + 1) + ' dengan Account : '+ ' ' + cellSelectedDet_kbs.data.ACCOUNT ,
			   buttons: Ext.MessageBox.YESNO,
			   fn: function (btn) 
			   {			
				   if (btn =='yes') 
					{
					    dsDTLTRList_kbs.removeAt(CurrentTR_kbs.row);
					    CalcTotal_kbs();
						HapusBaris_kbsDB(cellSelectedDet_kbs.data.LINE);
					    cellSelectedDet_kbs = undefined;
					} 
			   },
			   icon: Ext.MessageBox.QUESTION
			}
		);
	}
	else
	{
	    dsDTLTRList_kbs.removeAt(CurrentTR_kbs.row);
	    CalcTotal_kbs();
	    cellSelectedDet_kbs = undefined;
	}
	
	
};

function TambahBaris_kbs()
{
	var varDesc='';
	if (dsDTLTRList_kbs.getCount()>0){
		varDesc=dsDTLTRList_kbs.data.items[0].data.DESCRIPTION;
	}

	var p = new mRecord_kbs
	(
		{
			ACCOUNT: '',
			NAMAACCOUNT: '',
			DESCRIPTION: varDesc,//Ext.get('txtCatatan_kbs').getValue(),		
			VALUE: '',								
			LINE:''
		}
	);

	dsDTLTRList_kbs.insert(dsDTLTRList_kbs.getCount(), p);	 
	//this.startEditing(0, 1);
};


function DataDelete_kbs() 
{
	if (ValidasiEntry_kbs('Hapus Data') == 1 )
	{	
		Ext.Ajax.request
		(
			{
				// url: "./Datapool.mvc/DeleteDataObj",
				url: baseURL + "index.php/keuangan/functionKBS/deleteKBS",
				params:  getParam_kbs(), 
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true)					
					{
							ShowPesanInfo_kbs('Data berhasil di hapus','Hapus Data');
							RefreshDataFilter_kbs(false);
							AddNew_kbs();
					}
					else if (cst.success === false && cst.pesan === 0 )
					{
						ShowPesanWarning_kbs('Data tidak berhasil di hapus, data tersebut belum ada','Hapus Data');
					}
					else 
					{
						ShowPesanError_kbs('Data tidak berhasil di hapus','Hapus Data');
					}
				}
			}
		)
	}
};

function getArrDetail_kbs()
{
	var x='';
	for(var i = 0 ; i < dsDTLTRList_kbs.getCount();i++)
	{
		var y='';
		var z='@@##$$@@';
		
		// if (DataAddNew_kbs === true)
		if (dsDTLTRList_kbs.data.items[i].data.LINE==="")
		{
			y = "Line=" + (i+1)
		}
		else
		{	
			y = 'Line=' + dsDTLTRList_kbs.data.items[i].data.LINE
		}		
		y += z + 'Account='+dsDTLTRList_kbs.data.items[i].data.ACCOUNT
		y += z + 'Value=' + dsDTLTRList_kbs.data.items[i].data.VALUE
		y += z + 'DESCRIPTION=' + dsDTLTRList_kbs.data.items[i].data.DESCRIPTION
		// y += z + 'PRIORITAS_SP3D=' + dsDTLTRList_kbs.data.items[i].data.PRIORITAS_SP3D
		// y += z + 'URUT_SP3D=' + dsDTLTRList_kbs.data.items[i].data.URUT_SP3D
		// y += z + 'NO_PROGRAM_SP3D=' + dsDTLTRList_kbs.data.items[i].data.NO_PROGRAM_SP3D
		// y += z + 'JNS_RKAT=' + dsDTLTRList_kbs.data.items[i].data.JNS_RKAT
		if (i === (dsDTLTRList_kbs.getCount()-1))
		{
			x += y 
		}
		else
		{
			x += y + '##[[]]##'
		}
	}		
	return x;
};


function CalcTotal_kbs(idx)
{
    var total=Ext.num(0);
	var nilai=Ext.num(0);			
	for(var i=0;i < dsDTLTRList_kbs.getCount();i++)
	{
		
		nilai=dsDTLTRList_kbs.data.items[i].data.value;				
		// total += getNumber(nilai);
		total += nilai;
	}	
	Ext.get('txtTotal_kbs').dom.value=formatCurrencyDec(total);
};

function ButtonDisabled_kbs(mBol)
{
	// Ext.get('btnTambahBaris').dom.disabled=mBol;
	// Ext.get('btnHapusBaris').dom.disabled=mBol;
	// Ext.get('btnSimpan').dom.disabled=mBol;
	// Ext.get('btnSimpanKeluar').dom.disabled=mBol;
	// Ext.get('btnHapus').dom.disabled=mBol;	
	// Ext.get('btnApprove').dom.disabled=mBol;	

	Ext.getCmp('btnTambahBaris').setDisabled(mBol);
	Ext.getCmp('btnHapusBaris').setDisabled(mBol);
	Ext.getCmp('btnHapus').setDisabled(mBol);
	Ext.getCmp('btnApprove').setDisabled(mBol);
	Ext.getCmp('btnSimpan').setDisabled(mBol);
	Ext.getCmp('btnSimpanKeluar').setDisabled(mBol);
	Ext.getCmp('btnLookup').setDisabled(mBol);
};


function HapusBaris_kbsDB(Line) 
{	
	Ext.Ajax.request
	(
		{
			url: "./Datapool.mvc/DeleteDataObj",
			params: getParam_kbsHapusBaris(Line), 
			success: function(o) 
			{
				var cst = o.responseText;
				if (cst == '{"success":true}') 
				{						
						RefreshDataFilter_kbs(false);						
					}
					else if (cst == '{"pesan":0,"success":false}' )
					{
						
					}
					else 
					{
						
					}
			}
		}
	)
	
};

function getParam_kbsHapusBaris(Line) 
{
	var params = 
	{
		Table: 'viACC_CSO_KBS',   		
		IS_DETAIL:1,
		CSO_Number:Ext.get('txtNo_kbs').getValue(),
		CSO_Date:Ext.get('dtpTanggal_kbs').getValue(),
		Line:Line,		
		KATEGORI:KDkategori_kbs,
		REFF_ID:REFFID_SP3D_kbs,
		Amount: getAmount_kbs(Ext.get('txtTotal_kbs').getValue()),
		KD_UNIT_KERJA:selectUnitKerja_kbs,
		REFERENSI:Ext.get('txtReferensi_kbs').dom.value
	};
	return params
};


function mComboUnitKerjaView_kbs()
{
	var Field = ['kd_unit', 'nama_unit', 'unitkerja'];
	dsUnitKerja_kbs = new WebApp.DataStore({ fields: Field });

  var comboUnitKerja_kbsView = new Ext.form.ComboBox
	(
		{
			id:'comboUnitKerja_kbsView',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih Unit Kerja...',
			fieldLabel: 'Unit Kerja',			
			align:'Right',
			anchor:'100%',
			store: dsUnitKerja_kbs,
			valueField: 'kd_unit',
			displayField: 'nama_unit',
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectUnitKerja_kbs=b.data.kd_unit ;
				} 
			}
		}
	);
	

	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran_module/functionPlafondUnitKerja/getUnitKerja",
		params: {
			text:''
		},
		failure: function(o)
		{
			ShowPesanError_kbs('Error menampilkan data unit kerja !', 'Error');
		},	
		success: function(o) 
		{   
			dsUnitKerja_kbs.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsUnitKerja_kbs.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsUnitKerja_kbs.add(recs);
			} else {
				ShowPesanError_kbs('Gagal menampilkan data unit kerja', 'Error');
			};
		}
	});
	return comboUnitKerja_kbsView;
} ;

/* Cetak gstrSp3d */
function GetCriteria_viKBS()
{
	var strKriteria = '';
	
	if (Ext.get('txtNo_kbs').getValue() != '') 
	{
		//strKriteria = " Where AC.CSO_Number ='" + Ext.get('txtNo_kbs').getValue() + "'";
		strKriteria = Ext.get('txtNo_kbs').dom.value;
		strKriteria += '##'+1;
		strKriteria += '##'+Ext.get('txtTerimaDari_kbs').dom.value;	
		strKriteria += '##'+Ext.get('txtTotal_kbs').dom.value;
		strKriteria += '##'+Ext.get('comboAktivaLancar_kbs').dom.value;
		strKriteria += '##'+ShowDate(Ext.getCmp('dtpTanggal_kbs').getValue());		

};
	return strKriteria;
};

function Validasi_viKBS()
{
	var x=1;
	
	if(Ext.getCmp('txtNo_kbs').getValue() == '')
	{
		//ShowPesanWarning_viKBS('No. '+gstrSp3d+' belum di isi','Laporan '+gstrSp3d);
		ShowPesanWarning_viKBS('No. ' + NamaForm_kbs+' belum di isi','Laporan '+NamaForm_kbs);
		x=0;
	}
	
	return x;
};

function ShowPesanWarning_viKBS(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};

function GetParamProsesCekTotal(param)
{
	var x = ''
	x=param;
	x += "##"+Ext.get('dtpTanggal_kbs').getValue()
	return x;
};

function ProsesCekTotal(param,view)
{
	 Ext.Ajax.request
	 (
		{
			url: "./Module.mvc/ExecProc",
			params: 
			{
				UserID: 'Admin',
				ModuleID: 'clsCekTotalAkun',
				Params:	GetParamProsesCekTotal(param)			
			},
			success: function(o) 
			{
			var cst = Ext.decode(o.responseText);
				if (cst.success === true )
				{
				saldoAkunKBS = cst.jumlah;
				  if (view== true)
				   {
					ShowPesanWarning_viKBS('Akun Memiliki Saldo : ' + formatCurrencyDec(cst.jumlah) ,'SALDO');
					}
				}else
				{
				saldoAkunKBS =0;
				 if (view== true)
				   {
				ShowPesanWarning_viKBS('Akun Tidak Memiliki Saldo','SALDO');
				}
				}
			}

		}
		);
	
};


var winFormLookupsp3d;
var dsGLLookupsp3d;
var selectedrowGLLookupsp3d;

var CurrentTRsp3d = 
{
    data: Object,
    details:Array, 
    row: 0
};

var nASALFORM_KBS =1;
var nASALFORM_LPJ =2;
var nASALFORM_KasKeluarkecil =3;
var nASALFORM_NONMHS =4;
var nASALFORM_CSAR =5;
var nASALFORM_CSAPForm =6;

function FormLookupsp3d(ds,record,criteria, nASALFORM, thnAngg)
{
	winFormLookupsp3d = new Ext.Window
	(		
		{
		    id: 'winFormLookupsp3d',
		    title: 'Lookup PPD',
		    closeAction: 'destroy',
		    closable: true,
		    width: 670,
		    height: 450,
		    border: false,
		    plain: true,
		    resizable: false,
		    layout: 'form',
		    iconCls: '',
		    modal: true,
			autoScroll: true,
		    items: 
			[
				{

					xtype: 'compositefield',
					// fieldLabel: 'No. '+gstrSp3d,
					style: { 'margin-left': '-90px' },
					width: 640,
					items: 
					[
						
						{ 
							xtype: 'tbtext', text: 'Tahun:', cls: 'left-label', width: 50
						},
						mCombo_TahunAnggaran(150,'cboThnLookupSP3D',thnAngg),
						{ 
							xtype: 'tbtext', text: '', cls: 'left-label', width: 20
						},
						{ 
							xtype: 'tbtext', text: 'No. PPD:', cls: 'left-label', width: 80
						},
						{
							xtype:'textfield',
							// fieldLabel: 'No. '+gstrSp3d,
							name: 'txtNoSP3Dlookup1',
							id: 'txtNoSP3Dlookup1',
							width:200,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										/* var sql = " where SP3.NO_SP3D_RKAT like '%" + Ext.get('txtNoSP3Dlookup1').getValue() + "%'"
										if(nASALFORM == nASALFORM_LPJ){
											sql += " AND APP_SP3D = 1 and (( TAHAP_PROSES in(2) AND AC.NO_Tag is null ) or (TAHAP_PROSES in(3) AND AC.NO_Tag is not null )) AND  SP3.TAHUN_ANGGARAN_TA = " + thn;
											RefreshDataLookupkbs(sql);
										}else if(nASALFORM == nASALFORM_CSAPForm){
											sql += " AND APP_SP3D = 1 and (( TAHAP_PROSES in(2) AND AC.NO_Tag is null ) or (TAHAP_PROSES in(3) AND AC.NO_Tag is not null )) AND  SP3.TAHUN_ANGGARAN_TA = " + thn;
											RefreshDataLookupkbs(sql);
										}
										else{
											sql += " and APP_SP3D = '1' and  TAHAP_PROSES in (2) AND SP3.TAHUN_ANGGARAN_TA = "+ thnAngg //Ext.get('cboThnLookupSP3D').getValue()
											RefreshDataLookupsp3d(sql);
										}		 */				
										
										RefreshDataLookupsp3d(Ext.getCmp('cboThnLookupSP3D').getValue(),Ext.getCmp('txtNoSP3Dlookup1').getValue(),Ext.getCmp('comboUnitKerja_kbs').getValue());
									} 						
								}
							}
						},
						{ 
							xtype: 'tbtext', text: '', cls: 'left-label', width: 20
						},
						{
							xtype: 'button',
							tooltip: 'Tampilkan',
							iconCls: 'refresh',
							text: 'Tampilkan',
							id: 'btnRefreshLookupSP3D',
							handler: function()
							{
								
								/* var sql = " where SP3.NO_SP3D_RKAT like '%" + Ext.get('txtNoSP3Dlookup1').getValue() + "%'"
								if(nASALFORM == nASALFORM_LPJ){
									sql += " AND APP_SP3D = 1  and (( TAHAP_PROSES in(2) AND AC.NO_Tag is null ) or (TAHAP_PROSES in(3) AND AC.NO_Tag is not null )) AND  SP3.TAHUN_ANGGARAN_TA = " + thn;
									RefreshDataLookupkbs(sql);
								}else{
									sql += " and APP_SP3D = '1' and  TAHAP_PROSES in (2) AND SP3.TAHUN_ANGGARAN_TA = "+ thnAngg //Ext.get('cboThnLookupSP3D').getValue()
									RefreshDataLookupsp3d(sql);
								}	 */
								RefreshDataLookupsp3d(Ext.getCmp('cboThnLookupSP3D').getValue(),Ext.getCmp('txtNoSP3Dlookup1').getValue(),Ext.getCmp('comboUnitKerja_kbs').getValue());
				
							}
						}
					]
				},
				getItemFormLookupsp3d(ds,record,criteria,nASALFORM,thnAngg)
			]			
		}
	);
	
	if(Ext.getCmp('comboUnitKerja_kbs').getValue() != '' && Ext.getCmp('comboUnitKerja_kbs').getValue() != undefined){
		winFormLookupsp3d.show();
	}else{
		ShowPesanInfo_kbs('Pilih unit kerja terlebih dahulu!', 'Warning');
	}
}

function mCombo_TahunAnggaran(lebar,NamaCbo) 
{
	var Field = ['TAHUN_ANGGARAN_TA', 'CLOSED_TA', 'TMP_TAHUN'];
	dsListTahun_TahunAnggaran = new WebApp.DataStore({ fields: Field });
	// RefreshComboTahun_TahunAnggaran();
	var currYear = parseInt(now.format('Y'));
	var combo_TahunAnggaran = new Ext.form.ComboBox
	(
		{
		    id: NamaCbo,
		    name: NamaCbo,			
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			align:'Right',
			disabled:true,
			// anchor:'100%',//'70%',			
			width: lebar,
			// store: dsListTahun_TahunAnggaran,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: 
					[						
						[currYear + 1,currYear + 1], 
						[currYear,currYear ]
					]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			// value:gstrTahunAngg +'-'+(Ext.num(gstrTahunAngg)+1),
			value:Ext.num(now.format('Y')),
			listeners:
			{
				'select': function(a,b,c)
				{
				    SelectThn_TahunAnggaran = b.data.displayText;
				}
			}
		}
	);
	
	return combo_TahunAnggaran;
}
function getItemFormLookupsp3d(ds,record,criteria,nASALFORM,thnAngg)
{	
	var frmListLookupsp3d = new Ext.Panel
	(
		{
			id: 'frmListLookupsp3d',	    
			autoScroll: true,
			items: 
			[
				getGridListsp3d(criteria,nASALFORM),				
				{
				    layout: 'hBox',
				    border: false,
				    defaults: { margins: '0 5 0 0' },
				    style: { 'margin-left': '4px', 'margin-top': '3px' },
				    anchor: '96.5%',
				    layoutConfig:
					{
					    padding: '3',
					    pack: 'end',
					    align: 'middle'
					},
				    items:
					[
						{
						    xtype: 'button',
							width: 70,
						    text: 'Ok',
							handler: function()
							{
								//CekNOLookup(nASALFORM)
								
								Ext.get('txtReferensi_kbs').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.no_sp3d_rkat;
								Ext.get('comboUnitKerja_kbs').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.nama_unit_kerja;
								Ext.getCmp('comboUnitKerja_kbs').setValue(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.kd_unit_kerja);
								Ext.get('txtCatatan_kbs').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.keterangan;
								selectUnitKerja_kbs = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.kd_unit_kerja;
								
								INPUTDetailSP3D_kbs(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data);
								Ext.get('txtTotal_kbs').dom.value=formatCurrencyDec(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.jumlah);
								Ext.get('txtJml_kbs').dom.value=formatCurrencyDec(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.jumlah);
								winFormLookupsp3d.close();	
							}
						},
						{
						    xtype: 'button',
							width: 70,
						    text: 'Cancel',
							handler: function()
							{
								winFormLookupsp3d.close();
							}
						}
					]
				}
			]
		}
	);
			
	return frmListLookupsp3d;
}
var gridListLookupsp3d;
function getGridListsp3d(criteria,nASALFORM)
{
	/* var _fields = 
	[
		'NOMOR','AliasNOMOR', 'TANGGAL','REFERENSI', 
		'JML','UNIT','KD_UNIT',
		'Personal','Account','Pay_No','Pay_Code','name', 'Payment','Notes1'
	]; */
	
	var _fields = 
	[
		'selected','tahun_anggaran_ta', 'kd_unit_kerja', 'no_sp3d_rkat', 'tmpno_sp3d_rkat', 'tgl_sp3d_rkat',
		'jumlah', 'jenis_sp3d', 'nama_unit_kerja', 'app_sp3d',
		'tahun_anggaran_ta_tmp','kd_unit_kerja_tmp','no_sp3d_rkat_tmp', 'tgl_sp3d_rkat_tmp',
		'jalur','prioritas','referensi','keterangan'
	];
	
	dsGLLookupsp3d = new WebApp.DataStore({ fields: _fields });
	
	/* if(nASALFORM == nASALFORM_LPJ){
		RefreshDataLookupkbs(criteria);
	}else{
		RefreshDataLookupsp3d(criteria);
	}  */
	
	gridListLookupsp3d = new Ext.grid.EditorGridPanel
	(
		{
			stripeRows: true,
			id: 'gridListLookupsp3d',
			store: dsGLLookupsp3d,
			height:353,
			anchor: '100% 90%',
			columnLines:true,
			bodyStyle: 'padding:0px',
			autoScroll: true,
			border: false,
			viewConfig : 
			{
				forceFit: true
			},		
			sm: new Ext.grid.RowSelectionModel
			(
				{ 
					singleSelect: false,
					listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							selectedrowGLLookupsp3d = dsGLLookupsp3d.getAt(row);
							CurrentTRsp3d.row = row;	
						}						
					}
				}
			),
			//plugins: [new Ext.ux.grid.FilterRow()],
			//cm: fnGridLookupsp3dColumnModel(),
			cm: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),					
					{ 
						id: 'colNosp3dGLLookupsp3d',
						header: 'No. PPD',
						dataIndex: 'no_sp3d_rkat',
						renderer: function(value, cell) 
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
							return str;
						}						
					},
					{ 
						id: 'colaliasNosp3dGLLookupsp3d',
						header: 'No. PPD',
						hidden: true,
						dataIndex: 'no_sp3d_rkat',
						renderer: function(value, cell) 
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
							return str;
						}						
					},
					{ 
						id: 'colNoRefsp3dGLLookupsp3d',
						header: 'No. Referensi',
						dataIndex: 'referensi',
						renderer: function(value, cell) 
						{
							var str
							if(value != null){
								str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
							}else{
								str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + "-" + "</div>";
							}
							return str;
						}			
					},
					{ 
						id: 'colTGLsp3dGLLookupsp3d',
						header: 'Tanggal',
						dataIndex: 'tgl_sp3d_rkat',
						width:75,
						renderer: function(v, params, record) 
						{							
							return ShowDate(record.data.tgl_sp3d_rkat);
						}			
					},
					{ 
						id: 'colUKsp3dGLLookupsp3d',
						header: 'Unit Kerja',
						dataIndex: 'nama_unit_kerja',
						width:110,
						renderer: function(value, cell) 
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
							return str;
						}			
					},
					{ 
						id: 'colJMLsp3dGLLookupsp3d',
						header: 'JML',
						dataIndex: 'jumlah',
						width:100,	
						align:'right',						
						renderer: function(v, params, record) 
						{
							var str = "<div style='white-space:normal;padding:2px 20px 2px 2px'>" + formatCurrencyDec(record.data.jumlah) + "</div>";
							return str;
							 
						}	
					}
				]
			),
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					Ext.get('txtReferensi_kbs').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.no_sp3d_rkat;
					Ext.get('comboUnitKerja_kbs').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.nama_unit_kerja;
					Ext.getCmp('comboUnitKerja_kbs').setValue(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.kd_unit_kerja);
					Ext.get('txtCatatan_kbs').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.keterangan;
					selectUnitKerja_kbs = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.kd_unit_kerja;
					
					INPUTDetailSP3D_kbs(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data);
					//alert(selectUnitKerja_kbs);
					Ext.get('txtTotal_kbs').dom.value=formatCurrencyDec(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.jumlah);
					Ext.get('txtJml_kbs').dom.value=formatCurrencyDec(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.jumlah);
					winFormLookupsp3d.close();
					
				
					
				},
				'afterrender': function(){ 
					// console.log();
					RefreshDataLookupsp3d(Ext.getCmp('cboThnLookupSP3D').getValue(),Ext.getCmp('txtNoSP3Dlookup1').getValue(),Ext.getCmp('comboUnitKerja_kbs').getValue());
				}
				// End Function # --------------
			},
		}
	);
	
	return gridListLookupsp3d;
}

/*function fnGridLookupsp3dColumnModel() 
{
    return new Ext.grid.ColumnModel
	(
		[	
    		new Ext.grid.RowNumberer(),					
			{ 
				id: 'colNosp3dGLLookupsp3d',
				header: 'No. '+gstrSp3d,
				dataIndex: 'NOMOR',
				hidden: true,
				renderer: function(value, cell) 
				{
					var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
					return str;
				}					
			},
			{ 
				id: 'colaliasNosp3dGLLookupsp3d',
				header: 'No. '+gstrSp3d,
				dataIndex: 'AliasNOMOR',
				renderer: function(value, cell) 
				{
					var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
					return str;
				}						
			},
			{ 
				id: 'colNoRefsp3dGLLookupsp3d',
				header: 'No. Referensi',
				dataIndex: 'REFERENSI',
				renderer: function(value, cell) 
				{
					var str
					if(value != null){
						str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
					}else{
						str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + "-" + "</div>";
					}
					return str;
				}			
			},
			{ 
				id: 'colTGLsp3dGLLookupsp3d',
				header: 'Tanggal',
				dataIndex: 'TANGGAL',
				width:70,
				renderer: function(v, params, record) 
				{							
					return ShowDate(record.data.TANGGAL);
				}			
			},
			{ 
				id: 'colUKsp3dGLLookupsp3d',
				header: 'Unit Kerja',
				dataIndex: 'UNIT',
				width:125,
				renderer: function(v, params, record) 
				{							
					return ShowDate(record.data.UNIT);
				}			
			},
			{ 
				id: 'colJMLsp3dGLLookupsp3d',
				header: 'JML',
				dataIndex: 'JML',	
				align:'right',						
				renderer: function(v, params, record) 
				{
					return formatCurrency(record.data.JML);
				}	
			}
		]
	)
};*/

function RefreshDataLookupsp3d(tahun_anggaran_ta,no_sp3d,kd_unit_kerja)
{	
	/* dsGLLookupsp3d.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: '',
			    Sortdir: '',
			    target: 'viViewLookupSP3D',
			    param: criteria
			}
		}
	); */
	
	Ext.Ajax.request ({
		url: baseURL + "index.php/keuangan/functionKBS/LoadPPD",
		params: {
			no_sp3d: no_sp3d,
			tahun_anggaran_ta : tahun_anggaran_ta,
			kd_unit_kerja : kd_unit_kerja
		},
		failure: function(o)
		{
			ShowPesanError_kbs('Error menampilkan data PPD !', 'Error');
		},	
		success: function(o) 
		{   
			dsGLLookupsp3d.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsGLLookupsp3d.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsGLLookupsp3d.add(recs);
				console.log(dsGLLookupsp3d);
				gridListLookupsp3d.getView().refresh();
			} else {
				ShowPesanError_kbs('Gagal menampilkan data PPD', 'Error');
			};
		}
	});
	
	return dsGLLookupsp3d;
}

function RefreshDataLookupkbs(criteria)
{
	dsGLLookupsp3d.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: '',
			    Sortdir: '',
			    target: 'viViewLookupKBS',
			    param: criteria
			}
		}
	);
	return dsGLLookupsp3d;
}

function CekNOLookup(nASALFORM)
{
	var x;
	Ext.Ajax.request
	(
		{
			url: "./Datapool.mvc/CreateDataObj",
			params: getParam_lookup(nASALFORM),
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{

					//alert(nASALFORM+' - '+nASALFORM_KBS)
					if (nASALFORM == nASALFORM_KBS)
					{

						//alert(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.NOMOR)

						Ext.get('txtReferensi_kbs').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.NOMOR;
						// Ext.get('txtAliasReferensi_kbs').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.AliasNOMOR;
						Ext.get('comboUnitKerja_kbs').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.UNIT;
						Ext.getCmp('comboUnitKerja_kbs').setValue(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.KD_UNIT);
						Ext.get('txtCatatan_kbs').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Notes1;
						selectUnitKerja_kbs = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.KD_UNIT;
						
						INPUTDetailSP3D_kbs();
						//alert(selectUnitKerja_kbs);
						Ext.get('txtTotal_kbs').dom.value=formatCurrencyDec(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.JML);
						Ext.get('txtJml_kbs').dom.value=formatCurrencyDec(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.JML);
						REFFID_SP3D_kbs='01'; //Di isi KODE sumber referensi 
				
						winFormLookupsp3d.close();
					}
					else if (nASALFORM == nASALFORM_LPJ)
					{
						//get gstrSp3d
						if(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.REFERENSI != null){
							Ext.get('txtReferensi_LPJ').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.REFERENSI;
							// Ext.get('txtAliasReferensi_LPJ').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.REFERENSI;
						}else{
							Ext.get('txtReferensi_LPJ').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.NOMOR;
							// Ext.get('txtAliasReferensi_LPJ').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.AliasNOMOR;
						}
						
						Ext.get('txtNoPembayaran_LPJ').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Pay_No;
						Ext.get('txtCatatan_LPJ').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Notes1;
						
						if(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Personal != ""){
							Ext.get('txtTerimaDari_LPJ').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Personal;
						}
						
						no_sp3d = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.NOMOR;
						
						//Satuan Kerja
						Ext.get('comboUnitKerja_LPJ').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.UNIT;
						Ext.getCmp('comboUnitKerja_LPJ').setValue(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.KD_UNIT);
						selectUnitKerja_LPJ = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.KD_UNIT;
						
						//Jenis Pembayaran
						if(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Pay_Code != ""){
							Ext.get('comboBayar_LPJ').dom.value= dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Payment;
							Ext.getCmp('comboBayar_LPJ').setValue(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Pay_Code);
							selectBayar_LPJ = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Pay_Code;
						}
						
						//Akun
						if(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Account != "" ){
						
							Ext.get('comboAktivaLancar_LPJ').dom.value= dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.name;
							Ext.getCmp('comboAktivaLancar_LPJ').setValue(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Account);
							
						selectAktivaLancar_LPJ = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Account
						}
						
						
						INPUTDetailSP3D_LPJ();
						Ext.get('txtTotal_LPJ').dom.value=formatCurrencyDec(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.JML);		
						Ext.get('txtJml_LPJ').dom.value=formatCurrencyDec(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.JML);							
						if ((dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.REFERENSI == null) || (dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.REFERENSI == ''))
						{
							REFFIDKBS_LPJ='01'; //Di isi KODE sumber referensi sp3d
							Ext.getCmp('btnPengembalian_bpk').setDisabled(true);
						}else
						{
							REFFIDKBS_LPJ='22'; //Di isi KODE sumber referensi kbs
							Ext.getCmp('btnPengembalian_bpk').setDisabled(false);
						}

						winFormLookupsp3d.close();
					}
					else if (nASALFORM == nASALFORM_NONMHS)
					{
						//get gstrSp3d
						Ext.get('txtReferensi_PenerimaanNonMhs').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.NOMOR;
						Ext.get('txtNoPembayaran_PenerimaanNonMhs').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Pay_No;
						//Ext.get('txtCatatan_PenerimaanNonMhs').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Notes1;
						Ext.get('txtTerimaDari_PenerimaanNonMhs').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Personal;
						no_sp3d = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.NOMOR;
						
						//Satuan Kerja
						Ext.getCmp('comboUnitKerja_PenerimaanNonMhs').setValue(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.KD_UNIT);
						Ext.get('comboUnitKerja_PenerimaanNonMhs').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.UNIT;
						selectUnitKerja_PenerimaanNonMhs = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.KD_UNIT;
						
						//Jenis Pembayaran
						Ext.getCmp('comboBayar_PenerimaanNonMhs').setValue(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Pay_Code);
						Ext.get('comboBayar_PenerimaanNonMhs').dom.value= dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Payment;
						selectBayar_PenerimaanNonMhs = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Pay_Code;
						
						//Akun
						Ext.getCmp('comboAktivaLancar_PenerimaanNonMhs').setValue(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Account);
						Ext.get('comboAktivaLancar_PenerimaanNonMhs').dom.value= dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.name;
						selectAktivaLancar_PenerimaanNonMhs = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Account
						
						INPUTDetailSP3D_PenerimaanNonMhs();
						Ext.get('txtTotal_PenerimaanNonMhs').dom.value=formatCurrency(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.JML);								
						winFormLookupsp3d.close();
						
					}else if (nASALFORM == nASALFORM_KasKeluarkecil)
					{
						Ext.get('txtreferensi_KasKeluarkecil').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.NOMOR;
						INPUTDetailSP3D_KasKeluarkecil();
						Ext.get('txtTotal_KasKeluarkecil').dom.value=formatCurrency(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.JML);								
						winFormLookupsp3d.close();
					}else if (nASALFORM == nASALFORM_CSAPForm)
					{
						//get gstrSp3d
						if(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.REFERENSI != null){
							Ext.get('txtReferensi_CSAPForm').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.REFERENSI;
							// Ext.get('txtAliasReferensi_LPJ').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.REFERENSI;
						}else{
							Ext.get('txtReferensi_CSAPForm').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.NOMOR;
							// Ext.get('txtAliasReferensi_LPJ').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.AliasNOMOR;
						}
						
						Ext.get('txtNoPembayaran_CSAPForm').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Pay_No;
						Ext.get('txtCatatan_CSAPForm').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Notes1;
						
						// if(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Personal != ""){
						// 	Ext.get('txtTerimaDari_LPJ').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Personal;
						// }
						
						no_sp3d = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.NOMOR;
						
						//Satuan Kerja
						// Ext.get('comboUnitKerja_LPJ').dom.value = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.UNIT;
						// Ext.getCmp('comboUnitKerja_LPJ').setValue(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.KD_UNIT);
						// selectUnitKerja_LPJ = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.KD_UNIT;
						
						//Jenis Pembayaran
						if(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Pay_Code != ""){
							Ext.get('comboBayar_CSAPForm').dom.value= dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Payment;
							Ext.getCmp('comboBayar_CSAPForm').setValue(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Pay_Code);
							selectBayar_CSAPForm = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Pay_Code;
						}
						
						//Akun
						if(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Account != "" ){
						
							Ext.get('comboAktivaLancar_CSAPForm').dom.value= dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.name;
							Ext.getCmp('comboAktivaLancar_CSAPForm').setValue(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Account);
							selectAktivaLancar_CSAPForm = dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.Account
						}
						
						
						INPUTDetailSP3D_CSAPForm();
						Ext.get('txtTotal_CSAPForm').dom.value=formatCurrencyDec(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.JML);		
						// Ext.get('txtJml_LPJ').dom.value=formatCurrencyDec(dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.JML);							
						if ((dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.REFERENSI == null) || (dsGLLookupsp3d.data.items[CurrentTRsp3d.row].data.REFERENSI == ''))
						{
							REFFIDKBS_LPJ='01'; //Di isi KODE sumber referensi sp3d
							// Ext.getCmp('btnPengembalian_bpk').setDisabled(true);
						}else
						{
							REFFIDKBS_LPJ='22'; //Di isi KODE sumber referensi kbs
							// Ext.getCmp('btnPengembalian_bpk').setDisabled(false);
						}
						winFormLookupsp3d.close();
					}
				}
				else 
				{
					ShowPesanWarning_lookup('Data telah digunakan dengan no. transaksi ' + cst.pesan , 'Lookup Data');
					x=0;
					return x;
				}
			}
		}
	)	
}

function getParam_lookup(nForm) 
{
	var jalur;
	if (nForm == nASALFORM_KBS){
		jalur = 1;
	}else if(nForm == nASALFORM_LPJ){
		jalur = 2;
	}else if(nForm == nASALFORM_CSAPForm){
		jalur = 2;
	}

	var params = 
	{
		Table: 'cekNoTransaksi',
		REFERENSI:selectedrowGLLookupsp3d.data.REFERENSI,
		NO:selectedrowGLLookupsp3d.data.NOMOR,
		JALUR:jalur
	};
	return params
};

function ShowPesanWarning_lookup(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};

function ShowPesanError_lookup(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR
		}
	);
};

var rowSelectedLookApprove;
var vWinFormEntryApprove;
var now = new Date();
var vPesan;
var vCekPeriode;

function FormApprove(jumlah,type,nomor,Keterangan,tgl) {

    vWinFormEntryApprove = new Ext.Window
	(
		{
		    id: 'FormApprove',
		    title: 'Approve',
		    closeAction: 'hide',
		    closable: false,
		    width: 400,
		    height: 200,
		    border: false,
		    plain: true,
		    resizable: false,
		    layout: 'form',
		    iconCls: 'approve',
		    modal: true,
		    items:
			[
				ItemDlgApprove(jumlah,type,nomor,Keterangan,tgl)
			],
		    listeners:
				{
				    activate: function()
				    
				    {  }
				}
		}
	);
    vWinFormEntryApprove.show();
};


function ItemDlgApprove(jumlah,type,nomor,Keterangan,tgl) 
{	
	if (tgl==undefined)
	{		
		tgl=now;
	}
	
	var PnlLapApprove = new Ext.Panel
	(
		{ 
			id: 'PnlLapApprove',
			fileUpload: true,
			layout: 'form',
			height: '100',
			anchor: '100%',
			bodyStyle: 'padding:15px',
			border: true,
			items: 
			[
				getItemApprove_Nomor(nomor),
				getItemApprove_Tgl(tgl),
				getItemApprove_Jml(jumlah),
				getItemApprove_Notes(Keterangan),
				{
					layout: 'hBox',
					border: false,
					defaults: { margins: '0 5 0 0' },
					style:{'margin-left':'30px','margin-top':'5px'},
					anchor: '94%',
					layoutConfig: 
					{
						padding: '3',
						pack: 'end',
						align: 'middle'
					},
					items: 
					[
						{
							xtype: 'button',
							text: 'Ok',
							width: 70,
							hideLabel: true,
							id: 'btnOkLapApprove',
							handler: function() 
							{
								if (ValidasiEntryNomorApp('Approve') == 1 )
								{										
									if (type==='0')
									{
										PenerimaanApprove(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtNomorApp').getValue(),Ext.get('txtCatatanApp').getValue());
										RefreshDataPenerimaanFilter(false);
									};
									if (type==='2')
									{
										Approve_PenerimaanMhs(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtCatatanApp').getValue())										
									};
									if (type==='3')
									{
										Approve_PenerimaanNonMhs(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};
									if (type==='4')
									{
										Approve_KasKeluar(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};
									if (type==='5')
									{
										Approve_KasKeluarkecil(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};
									if (type==='6')
									{
										Approve_kbs(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};
									if (type==='7')
									{										
										Approve_RekapSP3D(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};		
									if (type==='8')
									{
										Approve_LPJ(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};									
									if (type==='9')
									{
										Approve_PaguGNRL(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};	
									if (type==='10')
									{
										Approve_viKembaliBDATM(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtCatatanApp').getValue())										
									};
									if (type==='11')
									{
										// Approve_OpenArForm(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtNomorApp').getValue())
										Approve_OpenArForm(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtNomorApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};
									if (type==='12')
									{
										Approve_OpenApForm(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtNomorApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};
									if (type==='13')
									{
										Approve_AdjustArForm(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtNomorApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};
									if (type==='14')
									{
										Approve_AdjustApForm(Ext.get('dtpTanggalApp').getValue(),Ext.get('txtNomorApp').getValue(),Ext.get('txtCatatanApp').getValue())
									};
									vWinFormEntryApprove.close();	
								}
							}
						},
						{
							xtype: 'button',
							text: 'Cancel',
							width: 70,
							hideLabel: true,
							id: 'btnCancelLapApprove',
							handler: function() 
							{
								vWinFormEntryApprove.close();
							}
						}
					]
				}
			]
		}
	);
 
    return PnlLapApprove
};

function getItemApprove_Nomor(nomor)
{
	var items = 			
	{
	    layout:'column',
	    border:false,	
	    items:
		[
			{
				columnWidth:1,
				layout: 'form',
				labelWidth: 65,
				labelAlign:'right',
				readOnly:true,
				border:false,				
				items: 
				[ 
					{
						xtype:'textfield',
						fieldLabel: 'No.Reff. ',
						name: 'txtNomorApp',
						id: 'txtNomorApp',						
						value:nomor,
						readOnly:true,
						anchor:'95%'
					}
				]
			}
		]
	}
	return items;
};

function getItemApprove_Notes(Keterangan)
{
	var items = 			
	{
	    layout:'form',
	    border:false,	
	    items:
		[
			{
				columnWidth:1,
				layout: 'form',
				labelWidth: 65,
				labelAlign:'right',
				border:false,
				items: 
				[ 
					{
						xtype:'textfield',
						fieldLabel: 'Catatan ',
						name: 'txtCatatanApp',
						id: 'txtCatatanApp',
						value:Keterangan,
						anchor:'95%'
					}
				]
			}
		]
	}
	return items;
};

function getItemApprove_Tgl(tgl)
{
	var items = 			
	{
	    layout:'form',
	    border:false,	
	    items:
		[
			{
				columnWidth:1,
				layout: 'form',
				labelWidth: 65,
				labelAlign:'right',
				border:false,
				items: 
				[
					{
						xtype: 'datefield',					
                        fieldLabel: 'Tanggal ',
                        id: 'dtpTanggalApp',
                        name: 'dtpTanggalApp',
                        format: 'd/M/Y',						
						value:tgl,
                        anchor: '50%'
					}
				]
			}
		]
	}
	return items;
};

function getItemApprove_Jml(jumlah)
{
	var items = 			
	{
	    layout:'column',
	    border:false,	
	    items:
		[			
			{
				columnWidth:1,
				layout: 'form',
				labelWidth: 65,
				labelAlign:'right',
				border:false,
				items: 
				[ 
					{
						xtype:'textfield',
						fieldLabel: 'Jumlah ',
						name: 'txtJumlah',
						id: 'txtJumlah',
						anchor:'95%',
						value:jumlah,
						readOnly:true
					}
				]
			}
		]
	}
	return items;
};

function ValidasiEntryNomorApp(modul)
{
	var x = 1;
	if (Ext.get('txtNomorApp').getValue() == '' )
	{
		if (Ext.get('txtNomorApp').getValue() == '')
		{
			ShowPesanWarningApprove('Nomor belum di isi',modul);
			x=0;
		}		
	}
	return x;
};

function ShowPesanWarningApprove(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};