/*var mRecordItem_OpenApForm = Ext.data.Record.create
(
	[
	   {name: 'ACCOUNT', mapping:'ACCOUNT'},
	   {name: 'NAMAACCOUNT', mapping:'NAMAACCOUNT'},
	   {name: 'Description', mapping:'Description'},
	   {name: 'Debit', mapping:'Debit'},
	   {name: 'Kredit', mapping:'Kredit'},
	   {name: 'Line', mapping:'Line'}
	]
);*/
var CurrentTR_OpenApForm = 
{
    data: Object,
    details:Array, 
    row: 0
};

var dsTRList_OpenApForm;
var DataAddNew_OpenApForm = true;
var selectCount_OpenApForm=50;
var now_OpenApForm = new Date();
var rowSelected_OpenApForm=undefined;
var TRLookUps_OpenApForm;
var focusRef_OpenApForm;
var cellSelectedDet_OpenApForm;
var dsCboEntryVendOpenApForm;
var selectCboEntryVendApForm;
var NamaForm_OpenApForm="Faktur AP";
//var StrAccVend;
var StrNmAccVend;
var strDue_DayApForm;
var StrAccKas_OpenApForm;
var StrNmAccKas_OpenApForm;
var StrApp_OpenApForm=false;
var selectAktivaLancar_OpenApForm;
var cellSelectedListItem_OpenApForm;
var CurrentTRListItem_OpenApForm=
{
    data: Object,
    details:Array, 
    row: 0
};
var CurrentSelected_OpenApForm=
{
    data: Object,
    details:Array, 
    row: 0
};
var IsImport_OpenApForm=0;

var rowSelectedImport_OpenApForm;
var CurrentSelectedImport_OpenApForm=
{
    data: Object,
    details:Array, 
    row: 0
};
var dsTRListFaktur_OpenApForm;
var flddsTRListFaktur_OpenApForm =['NO_TRANSAKSI','TGL_TRANSAKSI','JUMLAH','KD_USER']  
dsTRListFaktur_OpenApForm = new WebApp.DataStore({ fields: flddsTRListFaktur_OpenApForm })
var mRecordListFaktur_OpenApForm = Ext.data.Record.create
	(
		[
		   {name: 'NO_TRANSAKSI', mapping:'NO_TRANSAKSI'},
		   {name: 'TGL_TRANSAKSI', mapping:'TGL_TRANSAKSI'},
		   {name: 'JUMLAH', mapping:'JUMLAH'},
		   {name: 'KD_USER', mapping:'KD_USER'}
		]
	);
//var tmpRowSelectListFaktur_OpenApForm;

var StrAccVend_OpenApForm;

CurrentPage.page=getPanel_OpenApForm(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanel_OpenApForm(mod_id)
{
    var Field = 
	[
		'APF_NUMBER','APF_DATE','VEND_CODE','VENDOR','DUE_DATE','AMOUNT','PAID','NOTES','NO_TAG','DATE_TAG','POSTED','ACCOUNT','NAMAACCOUNT'
	]
    dsTRList_OpenApForm = new WebApp.DataStore({ fields: Field });
        
  
    var chkApprove_OpenApForm = new Ext.grid.CheckColumn
	(
		{
			id: 'chkApprove_OpenApForm',
			header: "APPROVE",
			align: 'center',
			disable:true,
			dataIndex: 'POSTED',
			width: 70,
			renderer	: function(value, metaData, record, rowIndex, colIndex, store){
							 switch (value){
								 case 't':
									 metaData.css = 'StatusHijau'; 
									 break;
								 case 'f':
									 metaData.css = 'StatusMerah';
									 break;
							 }
							 return '';
						}
		}
	);	
  
    var grListTR_OpenApForm = new Ext.grid.EditorGridPanel
	(
		{
			stripeRows: true,
			xtype: 'editorgrid',
			store: dsTRList_OpenApForm,
			anchor: '100% 100%',
			columnLines:true,
			border:false,
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			//sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelected_OpenApForm = dsTRList_OpenApForm.getAt(row);
						}
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, row, rec)
				{
					rowSelected_OpenApForm = dsTRList_OpenApForm.getAt(row);
					
					CurrentSelected_OpenApForm.row = rec;
					CurrentSelected_OpenApForm.data = rowSelected_OpenApForm;

					if (rowSelected_OpenApForm != undefined)
						{
							//DataAddNew_OpenArForm=false;
							LookUpForm_OpenApForm(rowSelected_OpenApForm.data);
					}
					else
					{
							//DataAddNew_OpenArForm=true;
							LookUpForm_OpenApForm();
					}
				}
			},
			colModel: new Ext.grid.ColumnModel			
			//cm: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'ColAPF_NUMBER',
						header: 'Nomor',
						dataIndex: 'APF_NUMBER',
						sortable: true,			
						width :100,
						filter: {}
					}, 
					{
						//xtype: 'datecolumn',
						header: 'Tanggal',
						width: 100,
						sortable: true,
						dataIndex: 'APF_DATE',
						id:'ColAPF_DATE',
						renderer: function(v, params, record) 
						{
							return ShowDateAkuntansi(record.data.APF_DATE);
						},
						filter: {}			
					}, 
					{						
						id: 'ColVENDOR',
						header: "Terima Dari",
						dataIndex: 'VENDOR',
						width :130,
						filter: {}
					},
					{
						id: 'ColNotes',
						header: "Keterangan",
						dataIndex: 'NOTES',
						width :200,
						filter: {}
					}, 
					{
						id: 'ColAmount',
						header: "Jumlah (Rp)",
						align:'right',
						dataIndex: 'AMOUNT',
						renderer: function(v, params, record) 
						{
							return formatCurrencyDec(record.data.AMOUNT);
						},	
						width :80,
						filter: {}
					},
					{
						id: 'ColPaid',
						header: "Bayar (Rp)",
						align:'right',
						dataIndex: 'PAID',
						renderer: function(v, params, record) 
						{
							return formatCurrencyDec(record.data.PAID);
						},	
						width :80,
						filter: {}
					},
					{
						header		: 'Approve',
						width		: 60,
						sortable	: false,
						hideable	: true,
						hidden		: false,
						menuDisabled: true,
						dataIndex	: 'POSTED',
						id			: 'chkApprove_OpenApForm',
						renderer	: function(value, metaData, record, rowIndex, colIndex, store){
							 switch (value){
								 case 't':
									 metaData.css = 'StatusHijau'; 
									 break;
								 case 'f':
									 metaData.css = 'StatusMerah';
									 break;
							 }
							 return '';
						}
					}
					 //chkApprove_OpenApForm
				]
			),

			//plugins: chkApprove_OpenApForm,
			tbar: 
			[				
				{
					id: 'btnEdit_OpenApForm',
					text: 'Edit Data',
					tooltip: 'Edit Data',
					iconCls: 'Edit_Tr',
					handler: function(sm, row, rec) 
					{ 
						if (rowSelected_OpenApForm != undefined)
						{
							LookUpForm_OpenApForm(rowSelected_OpenApForm.data);
							ButtonDisabled_OpenApForm(rowSelected_OpenApForm.data.Type_Approve);
						}
						else
						{
							LookUpForm_OpenApForm();
						}
					}
				},' ','-'
				,
				{
					xtype: 'checkbox',
					id: 'chkWithTgl_OpenApForm',					
					hideLabel:true,
					checked: true,
					handler: function() 
					{
						if (this.getValue()===true)
						{
							Ext.get('dtpTglAwalFilter_OpenApForm').dom.disabled=false;
							Ext.get('dtpTglAkhirFilter_OpenApForm').dom.disabled=false;
							Ext.get('dtpTglAwalFilter_OpenApForm').dom.readOnly=true;	
							Ext.get('dtpTglAkhirFilter_OpenApForm').dom.readOnly=true;
						}
						else
						{
							Ext.get('dtpTglAwalFilter_OpenApForm').dom.disabled=true;
							Ext.get('dtpTglAkhirFilter_OpenApForm').dom.disabled=true;							
						};
					}
				}
				, ' ','-','Tanggal : ', ' ',
				{
					xtype: 'datefield',
					fieldLabel: 'Dari Tanggal : ',
					id: 'dtpTglAwalFilter_OpenApForm',
					format: 'd/M/Y',
					value:now_OpenApForm,
					width:100,
					onInit: function() { }
				}, ' ', ' s/d ',' ', {
					xtype: 'datefield',
					fieldLabel: 'Sd /',
					id: 'dtpTglAkhirFilter_OpenApForm',
					format: 'd/M/Y',
					value:now_OpenApForm,
					width:100
				},' ','->',
				{
					id: 'btnFind_OpenApForm',
					text: ' Find',
					tooltip: 'Find Record',
					iconCls: 'find',
					handler: function(sm, row, rec) 
					{ 
						fnFindDlg_OpenApForm();	
					}
				}
			],
			bbar:new WebApp.PaggingBar
			(
				{
					displayInfo: true,
					store: dsTRList_OpenApForm,
					pageSize: 50,
					displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
					emptyMsg: "Tidak ada record&nbsp;&nbsp;"		
				}
			),
			viewConfig: {forceFit: true} 			
		}
	);


	var FormTR_OpenApForm = new Ext.Panel
	(
		{
			id: mod_id,
			closable:true,
			region: 'center',
			layout: 'form',
			title: NamaForm_OpenApForm, 
			border: false,           
			shadhow: true,
			iconCls: 'Penerimaan',
			 margins:'0 5 5 0',
			items: [grListTR_OpenApForm],
			tbar: 
			[
				'Nomor : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: 'No  : ',
					id: 'txtNoFilter_OpenApForm',                   
					//anchor: '25%',
					width:120,
					listeners:
					{ 
						'specialkey' : function()
						{
							if (Ext.EventObject.getKey() === 13) 
							{
								RefreshDataFilter_OpenApForm(false);					
							} 						
						}
					},
					onInit: function() { }
				}, 
				{ xtype: 'tbseparator' },
				{
					xtype: 'checkbox',
					id: 'chkFilterApproved_OpenApForm',
					boxLabel: 'Approved'
				},' ','-',
					//'Maks.Data : ', 
					' ',mComboMaksData_OpenApForm(),
					' ','->',
				{					
					xtype: 'button',
					tooltip: 'Tampilkan',
					iconCls: 'refresh',
					text: 'Tampilkan',
					anchor: '25%',
					handler: function(sm, row, rec) 
					{
						RefreshDataFilter_OpenApForm(false);
					}
				}
			],
			listeners: 
			{ 
				'afterrender': function() 
				{           
					RefreshDataFilter_OpenApForm(true);
					//RefreshDataFilter_OpenApForm(true);				 
				}
			}
		}
	);
	return FormTR_OpenApForm

};
    // end function get panel main data
 
 //---------------------------------------------------------------------------------------///
   
   
function LookUpForm_OpenApForm(rowdata)
{
	var lebar=700;
	TRLookUps_OpenApForm = new Ext.Window
	(
		{
			id: 'LookUpForm_OpenApForm',
			title: NamaForm_OpenApForm,
			closeAction: 'destroy',
			width: lebar,
			height: 525,//230,//420, 
			border: false,
			resizable:false,
			plain: true,
			layout: 'fit',
			iconCls: 'Penerimaan',
			modal: true,
			items: getFormEntryTR_OpenApForm(lebar),
			listeners:
			{
				activate: function() 
				{
					
				},
				afterShow: function() 
				{ 
					this.activate(); 
				},
				deactivate: function()
				{
					 rowSelected_OpenApForm=undefined;
					 RefreshDataFilter_OpenApForm(true);
				}
			}
		}
	);
	
	TRLookUps_OpenApForm.show();
	if (rowdata == undefined)
	{
		AddNew_OpenApForm();
	}
	else
	{
		DataInit_OpenApForm(rowdata)
	}	
	
};
   
function getFormEntryTR_OpenApForm(lebar) 
{
	var pnlTR_OpenApForm = new Ext.FormPanel
	(
		{
			id: 'pnlTR_OpenApForm',
			fileUpload: true,
			region: 'north',
			layout: 'fit',
			bodyStyle: 'padding: 10px 10px 10px 10px',			
			anchor: '100%', 
			width:lebar - 10,
			height:220,
			border: false,
			items: [getItemPanelInput_OpenApForm(lebar)],
			tbar: 
			[
				{
					text: 'Tambah',
					id:'btnTambah',
					tooltip: 'Tambah Record Baru ',
					iconCls: 'add',
					handler: function() 
					{ 
						AddNew_OpenApForm();
						ButtonDisabled_OpenApForm(false);
					}
				}, '-', 
				{
					text: 'Simpan',
					id:'btnSimpan_OpenApForm',
					tooltip: 'Rekam Data ',
					iconCls: 'save',
					handler: function() 
					{ 
						//alert(getArrDetail_OpenApForm())
						if (CekArrDetail_OpenApForm()==1){
							Datasave_OpenApForm(false);
							//RefreshDataFilter_OpenApForm(false);
						} else{
							ShowPesanWarning_OpenApForm('Item tidak boleh kosong','Simpan')
						}
					}
				}, '-', 
				{
					text: 'Simpan & Keluar',
					id:'btnSimpanKeluar_OpenApForm',
					tooltip: 'Rekam Data & Keluar ',
					iconCls: 'saveexit',
					handler: function() 
					{ 
						
						//alert(getArrDetail_OpenApForm())
						if (CekArrDetail_OpenApForm()==1){
							Datasave_OpenApForm(true);
							//RefreshDataFilter_OpenApForm(false);
							TRLookUps_OpenApForm.close();
						} else{
							ShowPesanWarning_OpenApForm('Item tidak boleh kosong','Simpan')
						}
					}
				}, '-', 
				{
					text: 'Hapus',
					id:'btnHapus_OpenApForm',
					tooltip: 'Remove the selected item',
					iconCls: 'remove',
					handler: function() 
					{ 

						if (DataAddNew_OpenApForm==false){
							Ext.Msg.show
							(
								{
								   title:'Hapus',
								   msg: 'Apakah transaksi ini akan dihapus ?', 
								   buttons: Ext.MessageBox.YESNO,
								   fn: function (btn) 
								   {			
									   if (btn =='yes') 
										{
											DataDelete_OpenApForm();
											//RefreshDataFilter_OpenApForm(false);
										} 
								   },
								   icon: Ext.MessageBox.QUESTION
								}
							);
						}	

					}
				}, '-',
				{
				    text: 'Approve',
					id:'btnApprove_OpenApForm',
				    tooltip: 'Approve',
				    iconCls: 'approve',
				    handler: function() 
					{ 
						if (ValidasiApp_OpenApForm('Approve Data',false) == 1 )
						{
							//txtTotalDebit_OpenApForm
							var JmlApp;
							if (CekArrItemImport_OpenApForm()==0){
								JmlApp=Ext.get('txtAmount_OpenApForm').getValue()
							} else{
								JmlApp=formatCurrency(CalcTotalJurnalOnly_OpenApForm());
							}
							FormApprove(JmlApp,'12',Ext.get('txtNo_OpenApForm').getValue(),
							Ext.get('txtCatatan_OpenApForm').getValue(),Ext.get('dtpTanggal_OpenApForm').getValue())
							
							RefreshDataFilter_OpenApForm(false);
						}
						// Approve_OpenApForm(Ext.get('dtpTanggal_OpenApForm').dom.value,Ext.get('txtNo_OpenApForm').dom.value)
						RefreshDataFilter_OpenApForm(false);
					}
				}
				, '-', '->', '-',
				{
					xtype: 'splitbutton',
					text: 'Cetak',
					iconCls: 'print',
					id: 'btnCetak_OpenApForm',
					handler: function (){
					},
					menu: new Ext.menu.Menu({
						items: [
							{
								xtype: 'button',
								text: 'Faktur',
								id: 'btnPrintFakturFakturAP',
								handler: function()
								{  
									Cetak_OpenApForm()
								}
							},
							{
								xtype: 'button',
								text: 'Surat Tagihan',
								id: 'btnPrintSuratTagihanFakturAP',
								handler: function()
								{
									Cetak_SuratTagihanAPFaktur();
								}
							}
						]
					})
				}
				
			]
		}
	);  // end Head panel

	var TotalItem_OpenApForm = new Ext.Panel
	(
		{
			frame: false,
			layout: 'form',
			width: 650,
			border:false,
			id:'PnlTotal_OpenApForm',
			labelAlign:'right',
			labelWidth:50,
			style: 
			{
				'margin-top': '5.9px',
				'margin-left': '380px'
			},
			items: 
			[
				{
					xtype: 'compositefield',
					width: 650,
					items:
					[
						{
							xtype: 'button',
							text: 'Import Data',								
							id: 'btnImportdata_OpenApForm',
							handler: function()
							{								
								//blnIS_IMPORT=1;
								 //setFrmLookUp_viImportDataBNI()									
								//FormLookupUploadfile(3,'uploads/PembayaranCsvBJB')
								//setFrmLookUpImportData_OpenApForm();

								if (DataAddNew_OpenApForm==false){
									//alert('i'+ rowdata.CUST_CODE+' - '+rowdata.CUSTOMER)
									dsTRListImport_OpenApForm.removeAll();
									setFrmLookUpImportData_OpenApForm(rowdata.VEND_CODE,rowdata.VENDOR);
								} else{
									if (selectCboEntryVendApForm=='' || selectCboEntryVendApForm == undefined){
										ShowPesanWarning_OpenApForm('Vendor belum di pilih','Import Data')
									}else{
										//alert('n'+ selectCboEntryCustARForm + ' - '+Ext.get('cboCustEntryARForm').getValue())
										dsTRListImport_OpenApForm.removeAll();
										setFrmLookUpImportData_OpenApForm(selectCboEntryVendApForm,Ext.get('cboVendorEntryApForm').getValue());
									}
									
								}
							}
						},
						{
							xtype: 'displayfield',
							id:'dspfldTotalMHS_OpenApForm',
							name:'dspfldTotalMHS_OpenApForm',							
							value:'Total :',

						},
						{
							xtype: 'textfield',
							id:'txtTotalItem_OpenApForm',
							name:'txtTotalItem_OpenApForm',
							//fieldLabel: 'Total ',
							readOnly:true,
							style:
							{	
								'text-align':'right',
								'font-weight':'bold'
							},
							value:'0',
							width: 125
						}
					]
				}		
			]
		}
	);
	
	
	
	var TotalJurnal_OpenApForm = new Ext.Panel
	(
		{
			frame: false,
			layout: 'form',
			width: 650,
			border:false,
			id:'PnlTotalDtlMhs_OpenApForm',
			labelAlign:'right',
			labelWidth:50,
			style: 
			{
				'margin-top': '5.9px',
				'margin-left': '320px'//105
			},
			items: 
			[
				{
					xtype: 'compositefield',
					width: 650,
					items:
					[
				
						{
							xtype: 'displayfield',
							id:'dspfldTotalMHS_OpenApForm',
							name:'dspfldTotalMHS_OpenApForm',							
							value:'Total :',

						},						
						{
							xtype: 'textfield',
							id:'txtTotalDebit_OpenApForm',
							name:'txtTotalDebit_OpenApForm',
							fieldLabel: '',
							readOnly:true,
							style:
							{	
								'text-align':'right',
								'font-weight':'bold'
							},
							value:'0',
							width: 125
						},
						/*{
							xtype: 'displayfield',
							id:'dspfldTotalMHS2_OpenApForm',
							name:'dspfldTotalMHS2_OpenApForm',							
							value:'Kredit :',

						},*/						
						{
							xtype: 'textfield',
							id:'txtTotalCredit_OpenApForm',
							name:'txtTotalCredit_OpenApForm',
							fieldLabel: '',
							readOnly:true,
							style:
							{	
								'text-align':'right',
								'font-weight':'bold'
							},
							value:'0',
							width: 125
						}
					]
				}
			]
		}
	);

		var GDtabDetail_OpenApForm = new Ext.TabPanel
	(
		{
			region: 'center',
			id: 'GDtabDetail_OpenApForm',
			activeTab: 0,		
			anchor: '100% 100%',	
			//bodyStyle: 'padding:15px',		
			border: false,
			plain: true,
			defaults: 
			{
				autoScroll: false
			},
			items: 
			[
				{  	
					title: 'Item' ,
					id:'tabDtlItem_OpenApForm', 
					frame: false,					
					border:false,					
					items: 
					[
						GetDTLTRGridItem_OpenApForm(),
						TotalItem_OpenApForm
					],      
					listeners: 
					{
						activate: function()
						{		
							/*mActiveTab = "1";
							CalcTotal_OpenApForm();
							CalcTotalMHS_OpenApForm();
							Ext.getCmp('btnTambahBaris').setVisible(false);
							Ext.getCmp('btnHapusBaris').setVisible(false);*/

						}												
					},
					tbar:
					[
						{
							text: 'Tambah Baris',
							id:'btnTambahBaris_OpenApForm',
							tooltip: 'Tambah Record Baru ',
							iconCls: 'add',				
							handler: function() 
							{ 
								TambahBarisItem_OpenApForm();
								
								if (IsImport_OpenApForm==1){

									dsDTLTRListItem_OpenApForm.removeAll();
									dsTRListFaktur_OpenApForm.removeAll();
									IsImport_OpenApForm=0;
								}
								///NewRow_OpenArForm=true;

								//if (DataAddNew_OpenArForm == false){
									//Ext.getCmp('FieldcolItem_OpenArForm').enable();
								/*	//NewRow_OpenArForm=true;
								} else if (DataAddNew_OpenArForm == true){
									Ext.getCmp('FieldcolItem_OpenArForm').enable();
									//NewRow_OpenArForm=true;
								}
*/
								
							}
						}, '-', 
						{
							text: 'Hapus Baris',
							id:'btnHapusBaris_OpenApForm',
							tooltip: 'Remove the selected item',
							iconCls: 'remove',
							handler: function()
								{
									if (dsDTLTRListItem_OpenApForm.getCount() > 0 )
									{						
										if (cellSelectedListItem_OpenApForm != undefined)
										{
											//if(CurrentTRListItem_OpenArForm != undefined)
											//{
												HapusBaris_OpenApForm();
												IsImport_OpenApForm=0;
											//}
										}
										else
										{
											ShowPesanWarning_OpenApForm('Silahkan pilih dahulu baris yang akan dihapus','Hapus baris');
										}
									}
								}
						}
					]

				},
				{  	
					title: 'Jurnal' ,
					id:'tabDtlJurnal_OpenApForm', 					
					frame: false,
					border:false,					
					items:
					[
						GetGridDTLJurnal_OpenApForm(),
						TotalJurnal_OpenApForm
					],      
					listeners: 
					{
						activate: function()
						{			
							/*mActiveTab = "2";
							Ext.getCmp('btnTambahBaris').setVisible(false);
							Ext.getCmp('btnHapusBaris').setVisible(false);
							CalcTotalMHS_OpenApForm();*/
						}
					}
				}
			] 
		}
	);	

	var Formload_OpenApForm = new Ext.Panel
	(
		{
			id: 'Formload_OpenApForm',
			region: 'center',
			width:'100%',
			anchor:'100%',
			layout: 'form',
			title: '',
			bodyStyle: 'padding:15px',
			border: true,
			bodyStyle: 'background:#FFFFFF;',
			shadhow: true,
			//iconCls: 'GL',
			items: [pnlTR_OpenApForm,GDtabDetail_OpenApForm]

		}
	);
	
	return Formload_OpenApForm						
};


function TambahBarisItem_OpenApForm()
{
	var p = new mRecordItem_OpenApForm
		(
			{
				LINE:'',
				ITEM_CODE: '',
				ITEM_DESC: '',
				ACCOUNT: '',//Ext.get('txtCatatan_PenerimaanMhs').getValue(),
				DESCRIPTION: '',								
				VALUE:0,
				NO_TRANSAKSI: '',
				TGL_TRANSAKSI: '',								
				IS_IMPORT: 0,
				DISKON: 0,
				PPN:0,
				MATERAI:0,
				TOTAL:0
			}
		);

	dsDTLTRListItem_OpenApForm.insert(dsDTLTRListItem_OpenApForm.getCount(), p);
};

function HapusBaris_OpenApForm()
{
	if (DataAddNew_OpenApForm == false){
		//alert(cellSelectedListItem_OpenApForm.data.ITEM_CODE)

		if (cellSelectedListItem_OpenApForm.data.ITEM_CODE!=''){// && cellSelectedListItem_OpenApForm.data.ITEM_DESC!=''){
			//alert('a')

					if (CurrentTRListItem_OpenApForm.row >= 0)
					{
						//alert('b')
						Ext.Msg.show
						(
							{
							   title:'Hapus Baris',
							   msg: 'Apakah baris ini akan dihapus ?' + ' ' + 'Baris :'+ ' ' + (CurrentTRListItem_OpenApForm.row + 1) + ' dengan Account : '+ ' ' + cellSelectedListItem_OpenApForm.data.ACCOUNT ,
							   buttons: Ext.MessageBox.YESNO,
							   fn: function (btn) 
							   {			
								   if (btn =='yes') 
									{
										if (cellSelectedListItem_OpenApForm.data.LINE !='' && cellSelectedListItem_OpenApForm.data.LINE !=undefined){
											dsDTLTRListItem_OpenApForm.removeAt(CurrentTRListItem_OpenApForm.row);
										    //CalcTotal_OpenArForm();
										    CalcTotalItem_OpenApForm();
											CalcTotalJurnal_OpenApForm();
											HapusBaris_OpenApFormDB(cellSelectedListItem_OpenApForm.data);
										    cellSelectedListItem_OpenApForm = undefined;
										} else{
											dsDTLTRListItem_OpenApForm.removeAt(CurrentTRListItem_OpenApForm.row);
					    					//CalcTotal_OpenArForm();
					    					cellSelectedListItem_OpenApForm = undefined;
										}
									    
									} 
							   },
							   icon: Ext.MessageBox.QUESTION
							}
						);
					}
					else
					{
					    dsDTLTRListItem_OpenApForm.removeAt(CurrentTRListItem_OpenApForm.row);
					     CalcTotalItem_OpenApForm();
						CalcTotalJurnal_OpenApForm();
					    //CalcTotal_OpenArForm();
					    cellSelectedListItem_OpenApForm = undefined;

					}

		} else{
			dsDTLTRListItem_OpenApForm.removeAt(CurrentTRListItem_OpenApForm.row);
			 CalcTotalItem_OpenApForm();
			CalcTotalJurnal_OpenApForm();
		    //CalcTotal_OpenArForm();
		    cellSelectedListItem_OpenApForm = undefined;
		   // alert('e')

		}

	}else{
		dsDTLTRListItem_OpenApForm.removeAt(CurrentTRListItem_OpenApForm.row);
		 CalcTotalItem_OpenApForm();
		CalcTotalJurnal_OpenApForm();
		    //CalcTotal_OpenArForm();
		    cellSelectedListItem_OpenApForm = undefined;
		   //alert('f')

	}	
	
	
};


function getParamHapusBaris_OpenApForm(row) 
{
	var params = 
	{
		
		Table: 'viACC_AP_OPEN',
		apf_number:Ext.getCmp('txtNo_OpenApForm').getValue(),
		APF_DATE:ShowDateAkuntansi(Ext.getCmp('dtpTanggal_OpenApForm').getValue()),
		LINE:row.LINE,
		ITEM_CODE:row.ITEM_CODE,
		ACCOUNT:row.ACCOUNT,
		VALUE:row.VALUE,
		BARIS:1	
	
	};
	return params
};

function HapusBaris_OpenApFormDB(row) 
{	
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/anggaran/fakturAP/delete_baris",
			params: getParamHapusBaris_OpenApForm(row), 
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
						RefreshDataFilter_OpenArForm(false);
						RefreshDataItem_OpenArForm(Ext.getCmp('txtNo_OpenApForm').getValue(),ShowDateAkuntansi(Ext.getCmp('dtpTanggal_OpenApForm').getValue()));
						RefreshDataJurnal_OpenApForm(Ext.getCmp('txtNo_OpenApForm').getValue(),ShowDateAkuntansi(Ext.getCmp('dtpTanggal_OpenApForm').getValue()));
						ShowPesanInfo_OpenApForm(" Data berhasil di hapus","Hapus Data")
				}
				
			}
		}
	)
	
};
 

var dsTmpItem_OpenApForm;
var dsDTLTRListItem_OpenApForm;

var mRecordItem_OpenApForm = Ext.data.Record.create
	(
		[
		   {name: 'LINE', mapping:'LINE'},
		   {name: 'ITEM_CODE', mapping:'ITEM_CODE'},
		   {name: 'ITEM_DESC', mapping:'ITEM_DESC'},
		   {name: 'ACCOUNT', mapping:'ACCOUNT'},
		   {name: 'DESCRIPTION', mapping:'DESCRIPTION'},
		   {name: 'VALUE', mapping:'VALUE'},
		   {name: 'NO_TRANSAKSI', mapping:'NO_TRANSAKSI'},
		   {name: 'TGL_TRANSAKSI', mapping:'TGL_TRANSAKSI'},
		   {name: 'IS_IMPORT', mapping:'IS_IMPORT'},
		   {name: 'DISC', mapping:'DISC'},
		   {name: 'PPN', mapping:'PPN'},
		   {name: 'MATERAI', mapping:'MATERAI'},
		   {name: 'TOTAL', mapping: 'TOTAL'}

		]
	);


function RefreshDataItem_OpenApForm(no,tgl)
{			
	dsDTLTRListItem_OpenApForm.load
	(
		{ 
			params: 
			{ 	
				Skip: 0,
				Take: 100,
				Sort: 'LINE',
				Sortdir: 'ASC',
				target:'viewOpenApDetailItem',
				param: 'apf_number = ~'+no+'~ and item_code <>~~ order by line '//'1'+'#'+'2017-07-13'//" Where KATEGORI='" + KDkategori_PenerimaanMhs + "' "				
			}
		}
	);
return dsDTLTRListItem_OpenApForm;
};

function GetDTLTRGridItem_OpenApForm() 
{
	// grid untuk detail transaksi	
	var fldDetail =['LINE','ITEM_CODE', 'ITEM_DESC', 'ACCOUNT', 'DESCRIPTION', 'VALUE','NO_TRANSAKSI','TGL_TRANSAKSI','IS_IMPORT','DISC','PPN','MATERAI','TOTAL']  
	
	dsDTLTRListItem_OpenApForm = new WebApp.DataStore({ fields: fldDetail })
	dsTmpItem_OpenApForm= new WebApp.DataStore({ fields: fldDetail })
	
	var gridDTLTR_OpenApForm = new Ext.grid.EditorGridPanel
	(
		{			
			stripeRows: false,
			store: dsDTLTRListItem_OpenApForm,
			id:'Dttlgrid_OpenApForm',
			border: false,
			columnLines:true,
			frame:true,
			height: 188,
			anchor: '100% 100%',			
			sm: new Ext.grid.RowSelectionModel
			(
				{
					singleSelect: true,
					listeners: 
					{
						rowselect: function(sm, row, rec)
						{
							//cellSelectedDet_OpenApForm =dsDTLTRListItem_OpenApForm.getAt(row);
							//CurrentTR_OpenApForm.row = row;
							//lineDetMHS = row;
							
							
							cellSelectedListItem_OpenApForm =dsDTLTRListItem_OpenApForm.getAt(row);
							CurrentTRListItem_OpenApForm.row = row;
							CurrentTRListItem_OpenApForm.data = cellSelectedListItem_OpenApForm.data;
							//alert(cellSelectedListItem_OpenApForm.data.LINE)
							//alert(getArrDetail_OpenApForm())

						},
						'focus' : function()
						{
							// alert("Detail Penerimaan'")
						}
					}
				}
			),
			cm: TRDetailColumModel_OpenApForm(), 
			viewConfig: 
			{
				forceFit: true
			},
			listeners:
			{
				'afterrender': function()
				{
					this.store.on("load", function()
						{
							CalcTotalItem_OpenApForm();
						} 
					);
					this.store.on("datachanged", function()
						{
							CalcTotalItem_OpenApForm();						
						}
					);
				}
			}
		}
	);
			
	return gridDTLTR_OpenApForm;
};

function TRDetailColumModel_OpenApForm() 
{
	return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
						{
				id: 'colAcc_OpenApForm',
				name: 'colAcc_OpenApForm',
				header: "Account",
				dataIndex: 'ACCOUNT',
				sortable: false,
				hidden: true,
				//anchor: '5%',
				width: 100,
				editor: new Ext.form.TextField
				(
					{
						id:'FieldcolAcc_OpenApForm',
						allowBlank: false,
						enableKeyEvents : true,
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								
															
								} 
							}
						}
					}
				)
			}, 
			{
				id: 'colItem_OpenApForm',
				name: 'colItem_OpenApForm',
				header: "Item",
				dataIndex: 'ITEM_CODE',
				sortable: false,
				//hidden:true,
				//anchor: '5%',
				width: 100,
				editor: new Ext.form.TextField
				(
					{
						id:'FieldcolItem_OpenApForm',
						allowBlank: false,
						enableKeyEvents : true,
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									//if (DataAddNew_OpenArForm == true){

										var strKriteria='';

											if (Ext.getCmp('FieldcolItem_OpenApForm').getValue()!=''){
												strKriteria="  Item_Code like '" + Ext.getCmp('FieldcolItem_OpenApForm').getValue() + "%' and  I.item_group='1' "; 
											}
											else{
												strKriteria="  I.item_group='1' "; 
												
											}
											var p = new mRecordItem_OpenApForm
											(
												{
													LINE:'',
													ITEM_CODE: '',
													ITEM_DESC: '',
													ACCOUNT: '',
													DESCRIPTION: '',								
													VALUE: 0,
													NO_TRANSAKSI: '',
													TGL_TRANSAKSI: '',								
													IS_IMPORT: 0,
													DISC:0,
													PPN:0,
													MATERAI:0,
													TOTAL:0
												}
											);
										//alert(CurrentTRListItem_OpenArForm.row);
										FormLookupItemGrid_OpenApForm(strKriteria,dsDTLTRListItem_OpenApForm,p,true,CurrentTRListItem_OpenApForm.row,false);								
										//rowSelectedDetail_viDiklat=undefined;									
								} 
							}
						}
					}
				)
			}, 
			{
				id: 'ColDesc_OpenApForm',
				name: 'ColDesc_OpenApForm',
				header: "Deskripsi",
				dataIndex: 'DESCRIPTION',
				width: 300,
				renderer: function(v, params, record) 
				{
					var str = "<div style='white-space:normal;padding:2px 2px 2px 2px;'>" + record.data.DESCRIPTION + "</div>";
					return str;
				}
			}, 
			{
				id: 'ColAmount_OpenApForm',
				name: 'ColAmount_OpenApForm',
				header: "Jumlah",
				width: 100,
				align:'right',
				dataIndex: 'VALUE',
				renderer: function(v, params, record) 
				{
					var str = "<div style='white-space:normal;padding:2px 10px 2px 2px;'>" + formatCurrencyDec(record.data.VALUE) + "</div>";
					return str;
				},
				editor: new Ext.form.NumberField
				(
					{
						id:'FieldColAmount_OpenApForm',
						allowBlank: false,
						enableKeyEvents : true,
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									CalcTotalItem_OpenApForm();
									CalcTotalJurnal_OpenApForm();
									if (CekArrKosong_OpenApForm()==1){
											TambahBarisItem_OpenApForm();
										}
								} 
							},
						/*	'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 9) 
								{
										CalcTotalItem_OpenApForm();
										CalcTotalJurnal_OpenApForm();
										if (CekArrKosong_OpenApForm()==1){
											TambahBarisItem_OpenApForm();
										}
								} 
							},*/
							'blur' : function()
							{									
								CalcTotalItem_OpenApForm();
								CalcTotalJurnal_OpenApForm();							
							},
							'change': function(){	
							   CalcTotalItem_OpenApForm();
								CalcTotalJurnal_OpenApForm();			
							},
							'keyup': {
							       fn: function(obj) {	
							      		 CalcTotalItem_OpenApForm();
										CalcTotalJurnal_OpenApForm();
							      },
							      delay: 1000
							}
						}
						
					}
				)

			}
		]
	)
};

function CalcTotalItem_OpenApForm(idx)
{
    var total=Ext.num(0);
	var nilai=Ext.num(0);
	for(var i=0;i < dsDTLTRListItem_OpenApForm.getCount();i++)
	{
		nilai=parseFloat(dsDTLTRListItem_OpenApForm.data.items[i].data.VALUE);
		total += parseFloat(nilai);
		//total +=  nilai
	}	
	//alert(total);
	console.log(total);
	Ext.getCmp('txtTotalItem_OpenApForm').setValue(formatCurrencyDec(total));
	Ext.getCmp('txtAmount_OpenApForm').setValue(formatCurrencyDec(total));

};

function CalcTotalJurnal_OpenApForm()
{
   	var totalDebit=Ext.num(0);
    var totalKredit=Ext.num(0);
	for(var i=0;i < dsDTLJurnal_OpenApForm.getCount();i++)
	{
		if ((GetAngka2_OpenApForm(dsDTLJurnal_OpenApForm.data.items[i].data.debit === 0)) || GetAngka2_OpenApForm((dsDTLJurnal_OpenApForm.data.items[i].data.debit === '')))
		{
			dsDTLJurnal_OpenApForm.data.items[i].data.debit=0;
		}		
		if ((GetAngka2_OpenApForm(dsDTLJurnal_OpenApForm.data.items[i].data.credit === 0)) || GetAngka2_OpenApForm(dsDTLJurnal_OpenApForm.data.items[i].data.credit === ''))
		{
			dsDTLJurnal_OpenApForm.data.items[i].data.credit=0;
		}
		totalDebit += parseFloat(GetAngka2_OpenApForm(dsDTLJurnal_OpenApForm.data.items[i].data.debit));
		totalKredit += parseFloat(GetAngka2_OpenApForm(dsDTLJurnal_OpenApForm.data.items[i].data.credit));
		
	}
	Ext.getCmp('txtTotalDebit_OpenApForm').setValue(formatCurrencyDec(totalDebit));//
	Ext.getCmp('txtTotalCredit_OpenApForm').setValue(formatCurrencyDec(totalKredit));//

};

function CalcTotalJurnalOnly_OpenApForm()
{
   	var totalDebit=Ext.num(0);
    var totalKredit=Ext.num(0);
	for(var i=0;i < dsDTLJurnal_OpenApForm.getCount();i++)
	{
		if ((GetAngka2_OpenApForm(dsDTLJurnal_OpenApForm.data.items[i].data.debit === 0)) || GetAngka2_OpenApForm((dsDTLJurnal_OpenApForm.data.items[i].data.debit === '')))
		{
			dsDTLJurnal_OpenApForm.data.items[i].data.debit=0;
		}		
		if ((GetAngka2_OpenApForm(dsDTLJurnal_OpenApForm.data.items[i].data.CREDIT === 0)) || GetAngka2_OpenApForm(dsDTLJurnal_OpenApForm.data.items[i].data.CREDIT === ''))
		{
			dsDTLJurnal_OpenApForm.data.items[i].data.credit=0;
		}
		totalDebit += parseFloat(GetAngka2_OpenApForm(dsDTLJurnal_OpenApForm.data.items[i].data.debit));
		totalKredit += parseFloat(GetAngka2_OpenApForm(dsDTLJurnal_OpenApForm.data.items[i].data.credit));
		
	}
	return totalDebit;
	//Ext.getCmp('txtTotalDebit_OpenApForm').setValue(formatCurrency(totalDebit));//
	//Ext.getCmp('txtTotalCredit_OpenApForm').setValue(formatCurrency(totalKredit));//

};

function GetAngka2_OpenApForm(str)
{
	// for (var i = 0; i < str.length; i++) 
	// {
	// 	var y = str.substr(i, 1)
	// 	if (y === '.') 
	// 	{
	// 		str = str.replace('.', '');
	// 	}
	// };
	return str;
};


var dsDTLJurnal_OpenApForm;
var dsTmpJurnal_OpenApForm;

var mRecordJurnal_OpenApForm = Ext.data.Record.create
	(
		[
		   {name: 'LINE', mapping:'LINE'},
		   {name: 'ACCOUNT', mapping:'ACCOUNT'},
		   {name: 'DESCRIPTION', mapping:'DESCRIPTION'},
		   {name: 'DEBIT', mapping:'DEBIT'},
		   {name: 'CREDIT', mapping:'CREDIT'}
		]
	);


function RefreshDataJurnal_OpenApForm(no,tgl)
{			
	dsDTLJurnal_OpenApForm.load
	(
		{ 
			params: 
			{ 	
				Skip: 0,
				Take: 50,
				Sort: 'LINE',
				Sortdir: 'ASC',
				target:'viewOpenApDetail',
				param: no+'#'+tgl//'1'+'#'+'2017-07-13'//" Where KATEGORI='" + KDkategori_PenerimaanMhs + "' "				
			}
		}
	);
return dsDTLJurnal_OpenApForm;
};



function GetGridDTLJurnal_OpenApForm() 
{
	var fldDetail = 
	[
		
		'line', 'account', 'description', 'debit', 'credit'
	]	
	
	dsDTLJurnal_OpenApForm = new WebApp.DataStore({ fields: fldDetail })
	dsTmpJurnal_OpenApForm = new WebApp.DataStore({ fields: fldDetail })
	
	var GridDTLJurnal_OpenApForm = new Ext.grid.EditorGridPanel
	(
		{			
			stripeRows: true,
			store: dsDTLJurnal_OpenApForm,
			id:'GridDTLJurnal_OpenApForm',
			border: false,
			columnLines:true,
			frame:true,
			height: 215,
			anchor: '100% 100%',			
			sm: new Ext.grid.CellSelectionModel
			(
				{
					singleSelect: true,
					listeners: 
					{
						cellselect: function(sm, row, rec)
						{
							/*cellSlctdDetMHS_OpenApForm =dsDTLJurnal_OpenApForm.getAt(row);
							CurrentTRMHS_OpenApForm.row = row;
							nRowSelect_OpenApForm = row;
							sTrColAccount_OpenApForm=cellSlctdDetMHS_OpenApForm.data.NIM;

							Ext.get('txtTahun_OpenApForm').dom.value=cellSlctdDetMHS_OpenApForm.data.THN_AJAR_AWAL; //hanca
							Ext.get('txtThnakhir_OpenApForm').dom.value=cellSlctdDetMHS_OpenApForm.data.THN_AKHIR;
							Ext.getCmp('cboSemester_OpenApForm').setValue(cellSlctdDetMHS_OpenApForm.data.JNS_SEMESTER);
							*/
						},
						'focus' : function()
						{
							//alert("Detail Mahasiswa")
						}
					}
				}
			),
			cm: TRDetailColumJurnalModel_OpenApForm(), 
			viewConfig: 
			{
				forceFit: true
			},
			listeners:
			{
				'afterrender': function(sm, row, rec)
				{
					this.store.on("load", function()
						{

							CalcTotalJurnal_OpenApForm();
						} 
					);
					this.store.on("datachanged", function()
						{
							CalcTotalJurnal_OpenApForm();						
						}
					);
					CalcTotalJurnal_OpenApForm();

				},
				activate: function()
				{	
					CalcTotalJurnal_OpenApForm();
					
				}
			}
		}
	);			
	return GridDTLJurnal_OpenApForm;
}

function TRDetailColumJurnalModel_OpenApForm() 
{
	return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(), 
			{
				id: 'ColAccount_OpenApForm',
				name: 'ColAccount_OpenApForm',
				header: "Account",
				dataIndex: 'account',
				//anchor: '30%',
				width:100
			},{
				id: 'ColDesc_OpenApForm',
				name: 'ColDesc_OpenApForm',
				header: "Deskripsi",
				dataIndex: 'description',
				//anchor: '30%',
				width:200
			},
			{
				id: 'ColDebit_OpenApForm',
				name: 'ColDebit_OpenApForm',
				header: "Debit",
				dataIndex: 'debit',
				//anchor: '30%',
				width:100,
				renderer: function(value, cell) 
				{
					var str = "<div style='white-space:normal;padding:2px 10px 2px 2px;text-align: right;'>" + formatCurrencyDec(value) + "</div>";
					return str;
				}
			},
			{
				id: 'ColCredit_OpenApForm',
				name: 'ColCredit_OpenApForm',
				header: "Kredit",
				dataIndex: 'credit',
				//align:'right',
				//anchor: '30%',
				width:100,
				renderer: function(value, cell) 
				{
					var str = "<div style='white-space:normal;padding:2px 10px 2px 2px;text-align: right;'>" + formatCurrencyDec(value) + "</div>";
					return str;
				}
			}

		]
	)
};


var setLookUpsImportData_OpenApForm;
var nowImportData_OpenApForm = new Date();
var NMformImportData_OpenApForm='Import Item';

function setFrmLookUpImportData_OpenApForm(code,name)
{
    var lebar = 450;
    setLookUpsImportData_OpenApForm = new Ext.Window
    (
        {
            id: 'SetLookUpsImportData_OpenApForm',
            title: NMformImportData_OpenApForm, 
            closeAction: 'destroy',
            // y:90,
            width: lebar,
            height: 400,//210,//155,
            resizable:false,
            border: false,
            plain: true,
            layout: 'fit',
            iconCls: 'Penerimaan',
            modal: true,
            items: getFormEntryImportData_OpenApForm(lebar),
            listeners:
            {
                activate: function() 
                {
                },
                afterShow: function() 
                { 
                    this.activate(); 
					setLookUpsImportData_OpenApForm.close();					
                },
                deactivate: function()
                {
                    					
                },
                'afterrender': function() 
				{ 
					//Ext.getCmp('GListImport_OpenApForm').hide();          
					Ext.getCmp('cmbVendor_OpenApForm').disable();	
					Ext.get('cmbVendor_OpenApForm').dom.value=code;	
					Ext.getCmp('cmbVendor_OpenApForm').setValue(name); 
					//alert(code +' - '+name)
					selectVendor_OpenApForm=code;				 
				}
          
            }
        }
    );        
    setLookUpsImportData_OpenApForm.show();
}

function getFormEntryImportData_OpenApForm(lebar)
{
    var pnlFormImportData_OpenApForm = new Ext.FormPanel
    (
        {
            region: 'center',
            padding: '5px',
            layout: 'fit',
            border:false,
            items: 
			[
                {
                    xtype: 'panel',
                    layout: 'form',
                    height: 390,//200,//145,
                    padding: '5px',
                    labelWidth: 100,
                    border:false,
                    items: 
					[
						{
							xtype: 'compositefield',
							fieldLabel: 'Vendor :',
							anchor: '100%',
							labelSeparator: '',
							name: 'compkmpImportData_OpenApForm',
							id: 'compkmpImportData_OpenApForm',
							//hidden: true,
							items: 
							[
								comboVendor_OpenApForm()		
							]
						},
						{
							xtype: 'compositefield',
							fieldLabel: 'Jenis :',
							anchor: '100%',
							labelSeparator: '',
							name: 'compkmpImportData2_OpenApForm',
							id: 'compkmpImportData2_OpenApForm',
							//hidden: true,
							items: 
							[
								ComboUnit_OpenApForm()	
							]
						},
						{
							xtype: 'compositefield',
							fieldLabel: 'Date :',
							anchor: '100%',
							labelSeparator: '',
							name: 'compPathfiletipeImportData_OpenApForm',
							id: 'compPathfiletipeImportData_OpenApForm',
							//hidden: true,
							items: 
							[
								{
									xtype: 'datefield',
									flex: 1,
									//fieldLabel: 'Tgl. Pembayaran',
									width: 143,
									format: 'd/M/Y',
									value: nowImportData_OpenApForm,
									name: 'DtpAwalTransaksi_OpenApForm',									
									id: 'DtpAwalTransaksi_OpenApForm'

								},
								{
									xtype: 'displayfield',
									id:'dspfldTotalMHS_OpenApForm',
									name:'dspfldTotalMHS_OpenApForm',							
									value:'To',


								},
								{
									xtype: 'datefield',
									flex: 1,
									//fieldLabel: 'Tgl. Pembayaran',
									width: 143,
									format: 'd/M/Y',
									value: nowImportData_OpenApForm,
									name: 'DtpAkhirTransaksi_OpenApForm',									
									id: 'DtpAkhirTransaksi_OpenApForm'

								},
							]
						},
						GetGridListImport_OpenApForm(),

						{
							layout: 'hBox',
							border: false,
							defaults: { margins: '0 5 0 0' },
							style:{'margin-left':'150px','margin-top':'5px'},
							anchor: '100%',
							items: 
							[
								{
									xtype: 'button',
									text: 'Tampilkan',
									width: 90,
									hideLabel: true,
									id: 'btnOkImportData_OpenApForm',
									handler: function() 
									{
										//dsTRListFaktur_OpenApForm.removeAll();
										if (selectUnit_OpenApForm==6){

											
											// loadMask.show();
											RefreshDataImportFaktur_OpenApForm(selectVendor_OpenApForm,Ext.get('DtpAwalTransaksi_OpenApForm').getValue(),Ext.get('DtpAkhirTransaksi_OpenApForm').getValue())
											//setLookUpsImportData_OpenApForm.close();
											//Ext.getCmp('GListImport_OpenApForm').show();
											//Ext.getCmp('btnImportData_OpenApForm').show();
											//Ext.getCmp('btnOkImportData_OpenApForm').hide();
										} else if (selectUnit_OpenApForm==0){
											ShowPesanWarning_OpenApForm("Pilih Jenis","Import Item")
										} else{
											ShowPesanWarning_OpenApForm("Sementara Belum Aktif","Import Item")
										}
										
									}
								},
								{
									xtype: 'button',
									text: 'Import',
									width: 90,
									hideLabel: true,
									id: 'btnImportData_OpenApForm',
									handler: function() 
									{
										var notransAp='';
										var tgltransAp='';
										/*var shiftransAp='';
										var kdkasirtransAp='';*/
										var jmlAp=0;
										var selected=0;
										var jmlselected=0;//cekjmlItemTrue_OpenApForm();


										for(var i = 0 ; i < dsTRListImport_OpenApForm.getCount();i++)
										{
											
											if (dsTRListImport_OpenApForm.data.items[i].data.status==true || dsTRListImport_OpenApForm.data.items[i].data.status=='true')
											{
												//alert(dsTRListImport_OpenArForm.data.items[i].data.NO_TRANSAKSI);
												jmlselected+=1;
												var p = new mRecordListFaktur_OpenApForm
													(
														{
															NO_TRANSAKSI:dsTRListImport_OpenApForm.data.items[i].data.NO_TRANSAKSI,
															TGL_TRANSAKSI:dsTRListImport_OpenApForm.data.items[i].data.TGL_TRANSAKSI,
															JUMLAH: dsTRListImport_OpenApForm.data.items[i].data.JUMLAH,
															KD_USER: dsTRListImport_OpenApForm.data.items[i].data.KD_USER
														}
													);
												console.log(p);
												dsTRListFaktur_OpenApForm.insert(dsTRListFaktur_OpenApForm.getCount(), p);

												if (jmlselected==1){

													notransAp=dsTRListImport_OpenApForm.data.items[i].data.NO_TRANSAKSI;
													tgltransAp=dsTRListImport_OpenApForm.data.items[i].data.TGL_TRANSAKSI;

												} else if (jmlselected>1){
													
													notransAp=notransAp+'#'+dsTRListImport_OpenApForm.data.items[i].data.NO_TRANSAKSI;
													tgltransAp=tgltransAp+'#'+dsTRListImport_OpenApForm.data.items[i].data.TGL_TRANSAKSI;
												}


												selected=i;
												jmlAp=jmlAp+1;
	
											}
												
										}	


										//alert(selected)	
										//alert(jmlselected)								
											if (jmlselected==1){
												ExecuteImportDataItemOne_OpenApForm(notransAp,tgltransAp,selectUnit_OpenApForm,selectCboEntryVendApForm);
											} else{
												ExecuteImportDataItem_OpenApForm(notransAp,tgltransAp,selectUnit_OpenApForm,jmlAp,selectCboEntryVendApForm);
											}
											
											IsImport_OpenApForm=1;
											//rowSelectedImport_OpenApForm=undefined;
											setLookUpsImportData_OpenApForm.close();
											Ext.getCmp('btnTambahBaris_OpenApForm').setDisabled(true);
										
									}
								},
								{
									xtype: 'button',
									text: 'Cancel',
									width: 70,
									hideLabel: true,
									id: 'btnCanceldataImportData_OpenApForm',
									handler: function() 
									{
										dsTRListFaktur_OpenApForm.removeAll();
										selectUnit_OpenApForm=0;
										Ext.getCmp('comboUnit_OpenApForm').reset();
										selectVendor_OpenApForm=undefined;
										Ext.getCmp('cmbVendor_OpenApForm').reset();

										setLookUpsImportData_OpenApForm.close();
										Ext.getCmp('btnTambahBaris_OpenApForm').setDisabled(false);
									}
								}
							]
						},
						
                    ]
                }
            ]            
        }
    );
        
    return pnlFormImportData_OpenApForm;     
}

function cekjmlItemTrue_OpenApForm(){
	var x=0;
	for(var i = 0 ; i < dsTRListImport_OpenApForm.getCount();i++)
	{
		
		if (dsTRListImport_OpenApForm.data.items[i].data.STATUS==true || dsTRListImport_OpenApForm.data.items[i].data.STATUS=='true')
		{
			x+=1;
		}
	}

	return x;
}		

var rowSelectedImport_OpenApForm;
var dsTRListImport_OpenApForm;
var fldDetail_IDA =['ID','NO_TRANSAKSI','TGL_TRANSAKSI','JUMLAH','KD_USER','STATUS']  
	
	dsTRListImport_OpenApForm = new WebApp.DataStore({ fields: fldDetail_IDA })

function RefreshDataImportFaktur_OpenApForm(no,tgl,tgl2)
{			
	dsTRListImport_OpenApForm.load
	(
		{ 
			params: 
			{ 	
				Skip: 0,
				Take: 50,
				Sort: 'LINE',
				Sortdir: 'ASC',
				target:'viewImportListFakturAP',
				param: no+'#'+tgl+'#'+tgl2//'1'+'#'+'2017-07-13'//" Where KATEGORI='" + KDkategori_PenerimaanMhs + "' "				
			}
		}
	);
return dsTRListImport_OpenApForm;
};

function ExecuteImportDataItem_OpenApForm(no,tgl,item,jml,kd_vendor)
{			
	dsDTLTRListItem_OpenApForm.load
	(
		{ 
			params: 
			{ 	
				Skip: 0,
				Take: 100,
				Sort: 'LINE',
				Sortdir: 'ASC',
				target:'viImportItemOpenAP',
				param: no+'##@@##'+tgl+'##@@##'+item+'#'+jml+'#'+kd_vendor//'1'+'#'+'2017-07-13'//" Where KATEGORI='" + KDkategori_PenerimaanMhs + "' "				
			}
		}
	);
return dsDTLTRListItem_OpenApForm;
};

function ExecuteImportDataItemOne_OpenApForm(no,tgl,item,kd_vendor)
{			
	dsDTLTRListItem_OpenApForm.load
	(
		{ 
			params: 
			{ 	
				Skip: 0,
				Take: 100,
				Sort: 'LINE',
				Sortdir: 'ASC',
				target:'viImportItemOpenOneAP',
				param: no+'#'+tgl+'#'+item+'#'+kd_vendor//'1'+'#'+'2017-07-13'//" Where KATEGORI='" + KDkategori_PenerimaanMhs + "' "				
			}
		}
	);
return dsDTLTRListItem_OpenApForm;
};

var fm = Ext.form;
function GetGridListImport_OpenApForm(){

	var chkStatusMhs_OpenApForm = new Ext.grid.CheckColumn({
        header: "",
        align:'center',
        dataIndex: 'status',
        //value:true,
        width: 30,
        hidden:false
    });
	

    var GridListImport_OpenApForm = new Ext.grid.EditorGridPanel
	(
		{	
			id:'GListImport_OpenApForm',
			stripeRows: true,
			xtype: 'editorgrid',
			store: dsTRListImport_OpenApForm,
			//anchor: '100% 100%',
			width:415,
			columnLines:true,
			border:true,
			autoScroll:true,
			height:240,
			//plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			//sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedImport_OpenApForm = dsTRListImport_OpenApForm.getAt(row);
							//tmpRowSelectListFaktur_OpenApForm=rowSelectedImport_OpenApForm.data.NO_TRANSAKSI;
							//alert(tmpRowSelectListFaktur_OpenApForm)
						}
					}
				}
			),
			listeners:
			{
				/*rowdblclick: function (sm, row, rec)
				{
					rowSelectedImport_OpenApForm = dsTRListImport_OpenApForm.getAt(row);
					
					CurrentSelectedImport_OpenApForm.row = rec;
					CurrentSelectedImport_OpenApForm.data = rowSelectedImport_OpenApForm;

					
						var p = new mRecordListFaktur_OpenApForm
							(
								{
									NO_TRANSAKSI:rowSelectedImport_OpenApForm.data.NO_TRANSAKSI,
									TGL_TRANSAKSI: rowSelectedImport_OpenApForm.data.TGL_TRANSAKSI,
									JUMLAH: rowSelectedImport_OpenApForm.data.JUMLAH,
									KD_USER: rowSelectedImport_OpenApForm.data.KD_USER
								}
							);

						dsTRListFaktur_OpenApForm.insert(dsTRListFaktur_OpenApForm.getCount(), p);
					
						
					IsImport_OpenApForm=1;

					ExecuteImportDataItem_OpenApForm(rowSelectedImport_OpenApForm.data.NO_TRANSAKSI,rowSelectedImport_OpenApForm.data.TGL_TRANSAKSI,selectUnit_OpenApForm);
					rowSelectedImport_OpenApForm=undefined;
					setLookUpsImportData_OpenApForm.close();
					Ext.getCmp('btnTambahBaris_OpenApForm').setDisabled(true);

				},*/
				'afterrender': function()
				{
					
					

					this.store.on("load", function()
						{
							//alert(dsTRListImport_OpenArForm.getCount())//CalcTotalItem_OpenArForm();
						} 
					);
					this.store.on("datachanged", function()
						{
							if (dsTRListImport_OpenApForm.getCount()==0){
								loadMask.hide();
								ShowPesanWarning_OpenApForm('Tidak ada data','Import Item')

							} else if (dsTRListImport_OpenApForm.getCount()>0){
								loadMask.hide();

							}		
						}
					);

				}
			},
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					chkStatusMhs_OpenApForm,
					{
						id: 'ColNoTrans_OpenApForm',
						header: 'Nomor',
						dataIndex: 'NO_TRANSAKSI',
						sortable: true,	
						align:'left',		
						width :150,
						//filter: {}
					}, 
					{
						header: 'Tanggal',
						width: 120,
						sortable: true,
						align:'center',
						dataIndex: 'TGL_TRANSAKSI',
						id:'ColDateTrans_OpenApForm',
						renderer: function(v, params, record) 
						{
							return ShowDateAkuntansi(record.data.TGL_TRANSAKSI);
						}
						//filter: {}			
					}, 
					
					{
						id: 'ColAmountTrans_OpenApForm',
						header: "Jumlah (Rp)",
						align:'right',
						dataIndex: 'JUMLAH',
						xtype : 'numbercolumn',
						width: 10,
						renderer: function(v, params, record) 
						{
							// toFormat(formatNumberDecimal(discount))
							var str = "<div style='white-space:normal;padding:2px 10px 2px 2px;'>" + formatCurrencyDec(record.data.JUMLAH) + "</div>";
							return str;
						},
							width :150,
						//filter: {}
					},

					{
						id: 'idLabel',
						header: "",
						width: 10,
						
					},
				]
			),
			plugins: chkStatusMhs_OpenApForm,
			viewConfig: {forceFit: true} 			
		}
	);

	return GridListImport_OpenApForm;
};

var selectUnit_OpenApForm=0;
function ComboUnit_OpenApForm()
{
  	var comboUnit_OpenApForm = new Ext.form.ComboBox
	(
		{
			id:'comboUnit_OpenApForm',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih ',
			//fieldLabel: gstrSatker+' ',			
			align:'Right',
			width:310,
			//anchor:'100%',
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[6, 'Obat (Apotik)']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectUnit_OpenApForm=b.data.Id;
				} 
			}
		}
	);
	
	/*dsUnit_OpenApForm.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'KD_BAGIAN',
			    Sortdir: 'ASC',
			    target: 'viewBAGIAN',
			    param: ''//gstrListUnitKerja + "##@@##" + 0
			}
		}
	);*/
	
	return comboUnit_OpenApForm;
} ;
/*function ComboUnit_OpenApForm()
{
	var Field = ['KD_BAGIAN', 'BAGIAN'];
	dsUnit_OpenApForm = new WebApp.DataStore({ fields: Field });
	
  	var comboUnit_OpenApForm = new Ext.form.ComboBox
	(
		{
			id:'comboUnit_OpenApForm',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih ',
			//fieldLabel: gstrSatker+' ',			
			align:'Right',
			width:225,
			//anchor:'100%',
			store: dsUnit_OpenApForm,
			valueField: 'KD_BAGIAN',
			displayField: 'BAGIAN',
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectUnit_OpenApForm=b.data.KD_BAGIAN;
				} 
			}
		}
	);
	
	dsUnit_OpenApForm.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'KD_BAGIAN',
			    Sortdir: 'ASC',
			    target: 'viewBAGIAN',
			    param: ''//gstrListUnitKerja + "##@@##" + 0
			}
		}
	);
	
	return comboUnit_OpenApForm;
} ;*/

var selectVendor_OpenApForm;
function comboVendor_OpenApForm()
{
	var Field = ['Vend_Code', 'Vendor', 'VendorName','DUE_DAY','ACCOUNT'];
	dsVendor_OpenApForm = new WebApp.DataStore({ fields: Field });
	dsVendor_OpenApForm.load
	(
		{
			params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'Vendor',
			    Sortdir: 'ASC',
			    target: 'viewCboVendLengkap',
			    param: ''
			}
		}
	);
	
 var cmbVendor_OpenApForm = new Ext.form.ComboBox
	(
		{
			id:'cmbVendor_OpenApForm',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih Vendor',
			//fieldLabel: 'Kas / Bank ',			
			align:'Right',
			width:310,
			//anchor:'100%',
			store: dsVendor_OpenApForm,
		    valueField: 'Vend_Code',
		    displayField: 'VendorName',
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectVendor_OpenApForm=b.data.Vend_Code ;
				} 
			}
		}
	);
	
	return cmbVendor_OpenApForm;
} ;
 //---------------------------------------------------------------------------------------///
 
function Cetak_SuratTagihanAPFaktur()
{
	var params={
		apf_number:Ext.getCmp('txtNo_OpenApForm').getValue(),
		apf_date:Ext.get('dtpTanggal_OpenApForm').getValue(),
		kd_vendor:Ext.getCmp('cboVendorEntryApForm').getValue(),
		vendor:Ext.get('cboVendorEntryApForm').getValue(),
	} 

	var form = document.createElement("form");
	form.setAttribute("method", "post");
	form.setAttribute("target", "_blank");
	form.setAttribute("action", baseURL + "index.php/keuangan/cetak_faktur_ap/cetakSuratTagihan");
	var hiddenField = document.createElement("input");
	hiddenField.setAttribute("type", "hidden");
	hiddenField.setAttribute("name", "data");
	hiddenField.setAttribute("value", Ext.encode(params));
	form.appendChild(hiddenField);
	document.body.appendChild(form);
	form.submit();	
};

function Cetak_OpenApForm()
{
	var params={
		apf_number:Ext.getCmp('txtNo_OpenApForm').getValue(),
		apf_date:Ext.get('dtpTanggal_OpenApForm').getValue(),
		kd_vendor:Ext.getCmp('cboVendorEntryApForm').getValue(),
		vendor:Ext.get('cboVendorEntryApForm').getValue(),
	} 

	var form = document.createElement("form");
	form.setAttribute("method", "post");
	form.setAttribute("target", "_blank");
	form.setAttribute("action", baseURL + "index.php/keuangan/cetak_faktur_ap/cetakFaktur");
	var hiddenField = document.createElement("input");
	hiddenField.setAttribute("type", "hidden");
	hiddenField.setAttribute("name", "data");
	hiddenField.setAttribute("value", Ext.encode(params));
	form.appendChild(hiddenField);
	document.body.appendChild(form);
	form.submit();	
};

//---------------------------------------------------------------------------------------///
			

//---------------------------------------------------------------------------------------///
function DataInit_OpenApForm(rowdata) 
{
	DataAddNew_OpenApForm = false;
	Ext.get('txtNo_OpenApForm').dom.readOnly=true;
	Ext.getCmp('dtpTanggal_OpenApForm').disable();
	Ext.get('cboVendorEntryApForm').dom.value = rowdata.VENDOR;
	selectCboEntryVendApForm = rowdata.VEND_CODE;
    Ext.get('txtNo_OpenApForm').dom.value = rowdata.APF_NUMBER;
	Ext.get('dtpTanggal_OpenApForm').dom.value = ShowDateAkuntansi(rowdata.APF_DATE);   
	Ext.get('txtCatatan_OpenApForm').dom.value = rowdata.NOTES;
	Ext.get('txtAmount_OpenApForm').dom.value = formatCurrencyDec(rowdata.AMOUNT);
	if (rowdata.POSTED === 'f'){
		Ext.getCmp('ChkApprove_OpenApForm').setValue(false);
	}else{
		Ext.getCmp('ChkApprove_OpenApForm').setValue(true);
	}
	
	Ext.get('dtpDueDate_OpenApForm').dom.value = ShowDateAkuntansi(rowdata.DUE_DATE);
	GetVendorAcc_OpenApForm(rowdata.VEND_CODE);
	StrAccVend_OpenApForm=rowdata.ACCOUNT;
	// GetAkunKas_OpenApForm('CBAP');
	StrApp_OpenApForm=rowdata.POSTED;
	if (StrApp_OpenApForm===false || StrApp_OpenApForm==='f')
	{
		Ext.getCmp('btnSimpanKeluar_OpenApForm').setDisabled(false);
		Ext.getCmp('btnHapus_OpenApForm').setDisabled(false);
		Ext.getCmp('btnSimpan_OpenApForm').setDisabled(false);
		Ext.getCmp('btnApprove_OpenApForm').setDisabled(false);
		Ext.getCmp('btnTambahBaris_OpenApForm').setDisabled(false);
		Ext.getCmp('btnHapusBaris_OpenApForm').setDisabled(false);
	}else
	{
		Ext.getCmp('btnSimpanKeluar_OpenApForm').setDisabled(true);
		Ext.getCmp('btnHapus_OpenApForm').setDisabled(true);
		Ext.getCmp('btnSimpan_OpenApForm').setDisabled(true);
		Ext.getCmp('btnApprove_OpenApForm').setDisabled(true);
		Ext.getCmp('btnTambahBaris_OpenApForm').setDisabled(true);
		Ext.getCmp('btnHapusBaris_OpenApForm').setDisabled(true);
	}

	Ext.get('comboAktivaLancar_OpenApForm').dom.value = rowdata.NAMAACCOUNT;
	selectAktivaLancar_OpenApForm = rowdata.ACCOUNT;

	Ext.getCmp('btnImportdata_OpenApForm').disable();	

	RefreshDataItem_OpenApForm(rowdata.APF_NUMBER,ShowDateAkuntansi(rowdata.APF_DATE));
	RefreshDataJurnal_OpenApForm(rowdata.APF_NUMBER,ShowDateAkuntansi(rowdata.APF_DATE));
	dsTRListFaktur_OpenApForm.removeAll();
	
};

//---------------------------------------------------------------------------------------///
function AddNew_OpenApForm() 
{
	DataAddNew_OpenApForm = true;	
	now_OpenApForm = new Date();
	Ext.getCmp('btnImportdata_OpenApForm').enable();	
	Ext.get('txtNo_OpenApForm').dom.readOnly=false;
	Ext.getCmp('dtpTanggal_OpenApForm').enable();
	Ext.get('txtNo_OpenApForm').dom.value = '';	
	Ext.getCmp('ChkApprove_OpenApForm').setValue(false);
	Ext.get('txtAmount_OpenApForm').dom.value = '';
	Ext.get('txtCatatan_OpenApForm').dom.value = 'Summary from cashier post : Open AP';	
	Ext.get('dtpTanggal_OpenApForm').dom.value = ShowDateAkuntansi(now_OpenApForm);
	Ext.get('cboVendorEntryApForm').dom.value = '';
	Ext.getCmp('cboVendorEntryApForm').setValue('');
	selectCboEntryVendApForm = '';
	StrAccVend_OpenApForm='';
	Ext.get('dtpDueDate_OpenApForm').dom.value = ShowDateAkuntansi(now_OpenApForm);
	GetAkunKas_OpenApForm('CBAP');
	rowSelected_OpenApForm=undefined;			
	StrApp_OpenApForm=false;
	Ext.getCmp('btnSimpanKeluar_OpenApForm').setDisabled(false);
	Ext.getCmp('btnHapus_OpenApForm').setDisabled(false);
	Ext.getCmp('btnSimpan_OpenApForm').setDisabled(false);
	Ext.getCmp('btnApprove_OpenApForm').setDisabled(false);
	Ext.getCmp('btnCetak_OpenApForm').setDisabled(false);

	Ext.getCmp('btnTambahBaris_OpenApForm').setDisabled(false);
	Ext.getCmp('btnHapusBaris_OpenApForm').setDisabled(false);
	// Ext.get('comboAktivaLancar_OpenApForm').dom.value = '';
	// selectAktivaLancar_OpenApForm = '';


	Ext.getCmp('comboAktivaLancar_OpenApForm').reset();
	selectAktivaLancar_OpenApForm = '';

	dsDTLTRListItem_OpenApForm.removeAll();
	dsDTLJurnal_OpenApForm.removeAll();
	dsTRListFaktur_OpenApForm.removeAll();

	Ext.getCmp('txtTotalItem_OpenApForm').setValue(formatCurrency(0));
	Ext.getCmp('txtAmount_OpenApForm').setValue(formatCurrency(0));
	Ext.getCmp('txtTotalDebit_OpenApForm').setValue(formatCurrency(0));//
	Ext.getCmp('txtTotalCredit_OpenApForm').setValue(formatCurrency(0));//

	//Ext.get('cboVendorEntryApForm').dom.value = "DR EVITA";
	//selectCboEntryVendApForm = '0000000084';


};

function getParam_OpenApForm() 
{
	var params = 
	{
		Table: 'viACC_AP_OPEN',
		apf_number:Ext.get('txtNo_OpenApForm').getValue(),
		apf_date:Ext.get('dtpTanggal_OpenApForm').getValue(),
		vend_code:selectCboEntryVendApForm,
		due_date:Ext.get('dtpDueDate_OpenApForm').getValue(),
		amount:getAmount_OpenApForm(Ext.get('txtAmount_OpenApForm').getValue()),
		paid:0,
		notes:Ext.get('txtCatatan_OpenApForm').getValue(),
		account:StrAccVend_OpenApForm,//selectAktivaLancar_OpenApForm,
		no_tag:'',
		date_tag:'',
		posted:0,
		is_import:IsImport_OpenApForm,
		List:getArrDetail_OpenApForm(),	
		JmlField:12,		
		//JmlList:dsDTLTRListItem_OpenApForm.getCount(),
		List2:getArrListFaktur_OpenApForm(),	
		JmlField2:4,		
		JmlList2:dsTRListFaktur_OpenApForm.getCount()
	};
	for (var L=0; L<dsDTLTRListItem_OpenApForm.getCount(); L++)
	{
		if (dsDTLTRListItem_OpenApForm.data.items[L].data.ITEM_CODE !== ''){
			params['ITEM_CODE'+L]             =dsDTLTRListItem_OpenApForm.data.items[L].data.ITEM_CODE;
			params['ACCOUNT'+L]         =dsDTLTRListItem_OpenApForm.data.items[L].data.ACCOUNT;
			params['ITEM_GROUP'+L]                 =dsDTLTRListItem_OpenApForm.data.items[L].data.ITEM_GROUP;
			if (L==0){
					params['LINE'+L]=2 
				} else if (L >= 1){
					params['LINE'+L]=L+2 
				}
			//params['LINE'+L]               =dsDTLTRListItem_OpenArForm.data.items[L].data.LINE;
			params['VALUE'+L]               =dsDTLTRListItem_OpenApForm.data.items[L].data.VALUE;
			params['DESKRIPSI'+L]               =dsDTLTRListItem_OpenApForm.data.items[L].data.DESCRIPTION;
			params['JmlList']               =L+1;
		}
	}
	return params
};

function getParamDelete_OpenApForm() 
{
	var params = 
	{
		Table: 'viACC_AP_OPEN',
		apf_number:Ext.get('txtNo_OpenApForm').getValue(),
		APF_DATE:Ext.get('dtpTanggal_OpenApForm').getValue(),
		IS_IMPORT:IsImport_OpenApForm,
	};
	return params
};

function getArrListFaktur_OpenApForm()
{
	var x='';
	for(var i = 0 ; i < dsTRListFaktur_OpenApForm.getCount();i++)
	{
		var y='';
		var z='@@##$$@@';
		
		
			y = 'NO_TRANSAKSI=' + dsTRListFaktur_OpenApForm.data.items[i].data.NO_TRANSAKSI	
			//y += z + 'ITEM_DESC='+dsDTLTRListItem_OpenArForm.data.items[i].data.ITEM_DESC
			y += z + 'TGL_TRANSAKSI=' + dsTRListFaktur_OpenApForm.data.items[i].data.TGL_TRANSAKSI
			y += z + 'JUMLAH=' + dsTRListFaktur_OpenApForm.data.items[i].data.JUMLAH
			y += z + 'KD_USER=' + dsTRListFaktur_OpenApForm.data.items[i].data.KD_USER


			if (i === (dsTRListFaktur_OpenApForm.getCount()-1))
			{
				x += y 
			}
			else
			{
				x += y + '##[[]]##'
			}
				
		//}
			
		
	}		
	return x;
};


function getArrDetail_OpenApForm()
{
	var x='';
	for(var i = 0 ; i < dsDTLTRListItem_OpenApForm.getCount();i++)
	{
		var y='';
		var z='@@##$$@@';
		
		// if (DataAddNew_OpenArForm === true)
		/*if (dsDTLTRListItem_OpenArForm.data.items[i].data.ITEM_CODE==""){
			alert('index:'+ i +'-'+dsDTLTRListItem_OpenArForm.data.items[i].data.ITEM_CODE);
		}*/
		//{
		if (dsDTLTRListItem_OpenApForm.data.items[i].data.ITEM_CODE!="" || dsDTLTRListItem_OpenApForm.data.items[i].data.ITEM_DESC!=""){

		
			y = 'ITEM_CODE=' + dsDTLTRListItem_OpenApForm.data.items[i].data.ITEM_CODE	
			//y += z + 'ITEM_DESC='+dsDTLTRListItem_OpenArForm.data.items[i].data.ITEM_DESC
			y += z + 'ACCOUNT=' + dsDTLTRListItem_OpenApForm.data.items[i].data.ACCOUNT
			y += z + 'DESCRIPTION=' + dsDTLTRListItem_OpenApForm.data.items[i].data.DESCRIPTION
			y += z + 'VALUE=' + dsDTLTRListItem_OpenApForm.data.items[i].data.VALUE
			if (i==0){
				y += z + 'LINE='+2 
			} else if (i >= 1){
				y += z + 'LINE='+ (i + 2) 
			} 
			y += z + 'NO_TRANSAKSI=' + dsDTLTRListItem_OpenApForm.data.items[i].data.NO_TRANSAKSI
			y += z + 'TGL_TRANSAKSI=' + dsDTLTRListItem_OpenApForm.data.items[i].data.TGL_TRANSAKSI
			y += z + 'IS_IMPORT=' + dsDTLTRListItem_OpenApForm.data.items[i].data.IS_IMPORT
			y += z + 'DISC=' + dsDTLTRListItem_OpenApForm.data.items[i].data.DISC
			y += z + 'PPN=' + dsDTLTRListItem_OpenApForm.data.items[i].data.PPN
			y += z + 'MATERAI=' + dsDTLTRListItem_OpenApForm.data.items[i].data.MATERAI
			y += z + 'TOTAL=' + dsDTLTRListItem_OpenApForm.data.items[i].data.TOTAL
			
			

			if (i === (dsDTLTRListItem_OpenApForm.getCount()-1))
			{
				x += y 
			}
			else
			{
				x += y + '##[[]]##'
			}
				
		}
			
		
	}		
	return x;
};

function CekArrItemImport_OpenApForm()
{

	var x=0;
	if (dsDTLTRListItem_OpenApForm.getCount()==0){
	x=0
	}else {	

		for(var i = 0 ; i < dsDTLTRListItem_OpenApForm.getCount();i++)
		{

			if (dsDTLTRListItem_OpenApForm.data.items[i].data.IS_IMPORT==1 || dsDTLTRListItem_OpenApForm.data.items[i].data.IS_IMPORT=="1"){
			
				x=1
			}
	
		}
	}
		
	return x;

};

function CekArrDetail_OpenApForm()
{

	var x=1;
	if (dsDTLTRListItem_OpenApForm.getCount()==0){
	x=0
	}else {	

		for(var i = 0 ; i < dsDTLTRListItem_OpenApForm.getCount();i++)
		{

			if (dsDTLTRListItem_OpenApForm.data.items[i].data.ITEM_CODE=="" || dsDTLTRListItem_OpenApForm.data.items[i].data.ITEM_DESC==""){
			
			x=1
			}
	
		}
	}
		
	return x;

};

function CekArrKosong_OpenApForm()
{
	var x=1;
	for(var i = 0 ; i < dsDTLTRListItem_OpenApForm.getCount();i++)
	{
		if (dsDTLTRListItem_OpenApForm.data.items[i].data.ITEM_CODE=='' || dsDTLTRListItem_OpenApForm.data.items[i].data.ITEM_CODE==undefined){
			x=0;
		}

	}		
	return x;
};

function getParamApprove_OpenApForm(TglApprove, NoApprove,NoteApprove)
{
	var params = 
	{
		Table: 'viACC_AP_OPEN',
		apf_number:Ext.get('txtNo_OpenApForm').getValue(),
		apf_date:Ext.get('dtpTanggal_OpenApForm').getValue(),
		vend_code:selectCboEntryVendApForm,
		due_date:Ext.get('dtpDueDate_OpenApForm').getValue(),
		amount:getAmountFocus_OpenApForm(Ext.get('txtAmount_OpenApForm').getValue()),
		paid:0,
		notes:Ext.get('txtCatatan_OpenApForm').getValue(),
		no_tag:NoApprove,
		date_tag:TglApprove,
		//kd_user:strKdUser,
		type: 1,
        info: 'APO',
		posted:1,
		account:StrAccVend_OpenApForm,
		List:getArrDetail_OpenApForm(),	
		JmlField:12,		
		JmlList:dsDTLTRListItem_OpenApForm.getCount(),
	};
	return params
};

//---------------------------------------------------------------------------------------///


function getAmount_OpenApForm(dblNilai)
{
    var dblAmount;
    dblAmount = dblNilai.replace('Rp.', '')
    for (var i = 0; i < dblAmount.length; i++) {
        var x = dblAmount.substr(i, 1)
        if (x === '.') {
            dblAmount = dblAmount.replace('.', '');
        }
    }    
    // return Ext.num(dblAmount)
    return dblAmount.replace(',', '.')
};

function GetAngka_OpenApForm(str,ket)
{
	for (var i = 0; i < str.length; i++) 
	{
		var y = str.substr(i, 1)
		if (y === '.') 
		{
			str = str.replace('.', '');
		}
	};
	
	return str;
};

function getAmountFocus_OpenApForm(dblNilai)
{
    var dblAmount;
    dblAmount = dblNilai
    for (var i = 0; i < dblAmount.length; i++) {
        var x = dblAmount.substr(i, 1)
        if (x === '.') {
            dblAmount = dblAmount.replace('.', '');
        }
    }    
    return dblAmount.replace(',', '.') //Ext.num(dblAmount)
};

function getItemPanelInput_OpenApForm(lebar)
{
	var items = 
	{
		layout:'form',
		anchor:'100%',
		width: lebar - 36,
		height: 150,//130,
		labelAlign: 'right',
		bodyStyle: 'padding:7px 7px 7px 7px',
		items:
		[
			{
				columnWidth:.9,
				width:lebar-36,
				layout: 'form',
				border:false,
				items: 
				[
					getItemPanelNo_OpenApForm(lebar),
					getItemPanelTerimaDari_OpenApForm(lebar),
					getItemPanelTglDue_OpenApForm(lebar),
					getItemPanelAktivaLancar_OpenApForm(lebar),
					getItemPanelCatatan_OpenApForm(lebar)
				]
			}
		]
	};
	return items;			
};

function getItemPanelAktivaLancar_OpenApForm(lebar)
{
	var items = 			
	{
	layout:'column',
	border:false,
	width:lebar-36,
	items:
		[
			{
				columnWidth:.4,
				layout: 'form',
				border:false,
				labelWidth:111,
				items: 
				[
					mcomboAktivaLancar_OpenApForm(),
				]
			},
			{
				// columnWidth:0.98,
				columnWidth:.5,
				layout: 'form',
				border:false,
				labelWidth:111,
				items: 
				[
					{
						xtype:'textfield',
						fieldLabel: 'Amount ',
						name: 'txtAmount_OpenApForm',
						id: 'txtAmount_OpenApForm',
						//anchor:'100%',
						anchor:'95%',
						baseChars: '0123456789.',
						enableKeyEvents : true,
						style:
						{	
							'text-align':'right',
							'font-weight':'bold'
						},
						listeners:
						{
							'keyup': function()
							{
								// Ext.getCmp('txtAmount_OpenApForm').setValue(formatCurrency(this.getValue()));
								// ValidasiEntryJurnal_OpenApForm('Jurnal');
							},
							'blur': function()
							{
								this.setValue(formatCurrencyDec(this.getValue()));
								// ValidasiEntryJurnal_OpenApForm('Jurnal');
							},
							'focus': function()
							{
								// this.setValue(getNumber(this.getValue()));
								this.setValue(getAmountFocus_OpenApForm(this.getValue()));
							}
						}	
					}
				]
			}
		]
	}
	return items;	
}; 

function getItemPanelCatatan_OpenApForm(lebar)
{
	var items = 			
	{
	layout:'column',
	border:false,
	width:lebar-36,
	items:
		[
			{
				columnWidth:0.98,
				layout: 'form',
				border:false,
				labelWidth:111,
				items: 
				[
					{
						xtype:'textarea',
						fieldLabel: 'Keterangan ',
						name: 'txtCatatan_OpenApForm',
						id: 'txtCatatan_OpenApForm',
						value:'Summary from cashier post : Open AP',
						// anchor:'99.9%',
						width:480,
						height:40,
						autoCreate: {tag: 'input', maxlength: '100'}
					}
				]
			}
		]
	}
	return items;	
}; 

function getItemPanelNo_OpenApForm(lebar)
{
	var items = 			
	{
	layout:'column',
	border:false,
	width:lebar-36,
	items:
		[
			{
				columnWidth:.5,
				layout: 'form',
				border:false,
				labelWidth:111,
				items: 
				[
					{
						xtype:'textfield',
						fieldLabel: 'Nomor',
						name: 'txtNo_OpenApForm',
						id: 'txtNo_OpenApForm',
						readOnly:false,
						anchor:'95%',
						listeners: 
						{
							'blur' : function()
							{									
								//alert('Lost Focus');//CalcTotal_PenerimaanNonMhs(CurrentTR_PenerimaanNonMhs.row);									
							}, 
						}	
					}
				]
			},
			{
				columnWidth:.5,
				layout: 'form',
				border:false,
				labelWidth:100,
				items: 
				[
					{
						xtype:'checkbox',
						fieldLabel: 'Approve',
						name: 'ChkApprove_OpenApForm',
						id: 'ChkApprove_OpenApForm',
						disabled:true,
						anchor:'95%'
					}
				]
			}
		]
	}
	return items;	
};

function getItemPanelTerimaDari_OpenApForm(lebar)
{
	var items = 			
	{
	layout:'column',
	width:lebar-36,
	border:false,
	items:
		[
			{
				columnWidth:1,
				layout: 'form',
				border:false,
				labelWidth:111,
				items: 
				[
					mCboEntryVendOpenApForm(),
				]
			}
		]
	}
	return items;	
}

function getItemPanelTglDue_OpenApForm(lebar)
{
	var items = 			
	{
	layout:'column',
	width:lebar-36,
	border:false,
	items:
		[
			{
				columnWidth:.4,
				layout: 'form',
				border:false,
				labelWidth:111,
				items: 
				[
					{
						xtype: 'datefield',
                        fieldLabel: 'Tanggal ',
                        id: 'dtpTanggal_OpenApForm',
                        name: 'dtpTanggal_OpenApForm',
                        format: 'd/M/Y',
						value:now_OpenApForm,
                        anchor: '100%',
                        listeners:
						{
							'keyup': function()
							{
								var strparam;
								strparam=Ext.get('dtpTanggal_OpenApForm').getValue()+'###@###'+strDue_DayApForm+'###@###';
								GetDueDate_OpenApForm(strparam);
							},
							'blur': function()
							{
								var strparam;
								strparam=Ext.get('dtpTanggal_OpenApForm').getValue()+'###@###'+strDue_DayApForm+'###@###';
								GetDueDate_OpenApForm(strparam);
							},
							'focus': function()
							{
								var strparam;
								strparam=Ext.get('dtpTanggal_OpenApForm').getValue()+'###@###'+strDue_DayApForm+'###@###';
								GetDueDate_OpenApForm(strparam);
							}
						}	
					}
				]
			},
			{
				columnWidth:.5,
				layout: 'form',
				border:false,
				items: 
				[
					{
						xtype: 'datefield',
                        fieldLabel: 'Due date ',
                        id: 'dtpDueDate_OpenApForm',
                        name: 'dtpDueDate_OpenApForm',
                        format: 'd/M/Y',
						value:now_OpenApForm,
                        anchor: '100%'
                        // readOnly:true,
                        // disabled:true
					}
				]
			}
		]
	}
	return items;	
}
function RefreshData_OpenApForm()
{			
	dsTRList_OpenApForm.load
	(
		{ 
			params: 
			{ 	
				Skip: 0,
				Take: selectCount_OpenApForm,
				Sort: 'APF_DATE',
				Sortdir: 'ASC',
				target:'viACC_AP_OPEN',
				param: " "
			}
		}
	);
	return dsTRList_OpenApForm;
};

function mComboMaksData_OpenApForm()
{
  var cboMaksDataOpenApForm = new Ext.form.ComboBox
	(
		{
			id:'cboMaksDataOpenApForm',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Maks.Data ',			
			width:50,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[1, 50], [2, 100],[3, 200],[4, 500],[5, 1000]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCount_OpenApForm,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectCount_OpenApForm=b.data.displayText ;
					RefreshDataFilter_OpenApForm(false);
				} 
			},
			hidden:true
		}
	);
	return cboMaksDataOpenApForm;
};

function mCboEntryVendOpenApForm()
{
	var Fields = ['Vend_Code', 'Vendor', 'VendorName','DUE_DAY','ACCOUNT'];
    dsCboEntryVendOpenApForm = new WebApp.DataStore({ fields: Fields });
	
	dsCboEntryVendOpenApForm.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				Sort: 'Vendor', 
				Sortdir: 'ASC', 
				target: 'viewCboVendLengkap',
				param: ''
			} 
		}
	);
    
    var cboVendorEntryApForm = new Ext.form.ComboBox
	(
		{
		    id: 'cboVendorEntryApForm',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: 'Pilih Vendor ...',
		    fieldLabel: 'Vendor ',
		    align: 'right',
		    anchor:'90%',
		    store: dsCboEntryVendOpenApForm,
		    valueField: 'Vend_Code',
		    displayField: 'VendorName',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
					var strparam;
			        selectCboEntryVendApForm = b.data.Vend_Code;
			        strDue_DayApForm=b.data.DUE_DAY
			        StrAccVend_OpenApForm = b.data.Account;
			        strparam=Ext.get('dtpTanggal_OpenApForm').getValue()+'###@###'+strDue_DayApForm+'###@###';
					GetDueDate_OpenApForm(strparam);		
									
					GetVendorAcc_OpenApForm(selectCboEntryVendApForm);
					// ValidasiEntryJurnal_OpenApForm();
					// Ext.getCmp('dtpDueDate_OpenApForm').setValue(ShowDateAkuntansi(now_OpenApForm.setDate(now_OpenApForm.getDate() + b.data.DUE_DAY)));

			    }
			}
		}
	);
	
	return cboVendorEntryApForm;
};

function RefreshDataFilter_OpenApForm(mBol) 
{   
	var strCrt_OpenApForm='';
	if (Ext.get('txtNoFilter_OpenApForm').getValue() != '') 
	{
		if (strCrt_OpenApForm ==='')
		{
			strCrt_OpenApForm = " APF_NUMBER like '" + Ext.get('txtNoFilter_OpenApForm').getValue() + "%' ";
		}
		else
		{
			strCrt_OpenApForm += " AND APF_NUMBER like '" + Ext.get('txtNoFilter_OpenApForm').getValue() + "%' ";
		}
	};
	
	if (Ext.getCmp('chkWithTgl_OpenApForm').getValue() === true) 
	{
		if (strCrt_OpenApForm ==='')
		{
			strCrt_OpenApForm = "  APO.APF_DATE >='" + FormatDateReport(Ext.getCmp('dtpTglAwalFilter_OpenApForm').getValue())  + "' ";
			strCrt_OpenApForm += " and  APO.APF_DATE <='" + FormatDateReport(Ext.getCmp('dtpTglAkhirFilter_OpenApForm').getValue())  + "' ";
		}
		else
		{
			strCrt_OpenApForm += " and (APO.APF_DATE >='" + FormatDateReport(Ext.getCmp('dtpTglAwalFilter_OpenApForm').getValue())  + "' ";
			strCrt_OpenApForm += " and  APO.APF_DATE <='" + FormatDateReport(Ext.getCmp('dtpTglAkhirFilter_OpenApForm').getValue())  + "') ";
		}
	};	

	if(Ext.getCmp('chkFilterApproved_OpenApForm').getValue() === true)
	{
		if (strCrt_OpenApForm ==='')
		{
			strCrt_OpenApForm = "  POSTED='t'";
		}
		else
		{
			strCrt_OpenApForm += " AND POSTED='t'";
		}
	}else{
		if (strCrt_OpenApForm ==='')
		{
			strCrt_OpenApForm = "  POSTED='f'";
		}
		else
		{
			strCrt_OpenApForm += " AND POSTED='f'";
		}
	};

    if (strCrt_OpenApForm != undefined) 
    {  
		dsTRList_OpenApForm.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCount_OpenApForm, 
					Sort: 'APF_DATE', 
					Sortdir: 'ASC', 
					target:'viACC_AP_OPEN',
					param: strCrt_OpenApForm
				}			
			}
		);        
    }
	else
	{
	    RefreshDataFilter_OpenApForm(true);
	}
};

function Approve_OpenApForm(TglApprove, NomApprove,NoteApprove) 
{
	loadMask.show();
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/anggaran/fakturAP/post",
			params:  getParamApprove_OpenApForm(TglApprove, NomApprove,NoteApprove), 
			success: function(o) 
			{				
				var cst = Ext.decode(o.responseText);
				loadMask.hide();
				if (cst.success === true)
				{
					ShowPesanInfo_OpenApForm('Data berhasil di Approve','Approve');
					RefreshDataFilter_OpenApForm(false);
					Ext.getCmp('ChkApprove_OpenApForm').setValue(true);
					Ext.getCmp('btnApprove_OpenApForm').setDisabled(true);
					Ext.getCmp('btnTambahBaris_OpenApForm').setDisabled(true);
					Ext.getCmp('btnHapusBaris_OpenApForm').setDisabled(true);							
				}
				else if (cst.success === false && cst.pesan === 0 )
				{
					ShowPesanWarning_OpenApForm('Data tidak berhasil di Approve, '+ cst.pesan,'Edit Data');
				}
				else 
				{
					ShowPesanError_OpenApForm('Data tidak berhasil di Approve, '+ cst.pesan,'Approve');
				}										
			}
		}
	)
}

function Datasave_OpenApForm(IsExit) 
{
	if (ValidasiEntry_OpenApForm('Simpan Data') == 1 )
	{
		/* if (DataAddNew_OpenApForm == true) 
		{ */
		    Ext.Ajax.request
				(
					{
					    url: baseURL + "index.php/anggaran/fakturAP/save",
					    params: getParam_OpenApForm(),
					    success: function(o) {
					        var cst = Ext.decode(o.responseText);
					        if (cst.success === true) 
							{
					            Ext.getCmp('btnImportdata_OpenApForm').disable();	
					            ShowPesanInfo_OpenApForm('Data berhasil di simpan', 'Simpan Data');

								if (IsExit===false)
								{
									Ext.get('txtNo_OpenApForm').dom.value = cst.noapo;
									if(StrApp_OpenApForm === false || StrApp_OpenApForm === 'f')
									{
										Ext.getCmp('btnApprove_OpenApForm').setDisabled(false);
										Ext.getCmp('btnCetak_OpenApForm').setDisabled(false);											
									}else
									{
										Ext.getCmp('btnApprove_OpenApForm').setDisabled(true);
										Ext.getCmp('btnCetak_OpenApForm').setDisabled(true);
									}	
								}
					            RefreshDataFilter_OpenApForm(false);
					            RefreshDataItem_OpenApForm(cst.noapo,ShowDateAkuntansi(Ext.getCmp('dtpTanggal_OpenApForm').getValue()));
								RefreshDataJurnal_OpenApForm(cst.noapo,ShowDateAkuntansi(Ext.getCmp('dtpTanggal_OpenApForm').getValue()));
								DataAddNew_OpenApForm=false;
								dsTRListFaktur_OpenApForm.removeAll();
								selectUnit_OpenApForm=0;
								Ext.getCmp('comboUnit_OpenApForm').reset();
								selectVendor_OpenApForm=undefined;
								Ext.getCmp('cmbVendor_OpenApForm').reset();
								Ext.get('txtNo_OpenApForm').dom.readOnly=true;
								// ValidasiEntryJurnal_OpenApForm();
					        }
					        else if (cst.success === false && cst.pesan === 0) 
							{
					            ShowPesanWarning_OpenApForm('Data tidak berhasil di simpan : , ' + cst.pesan , 'Simpan Data');
					        }
					        else {
					            ShowPesanError_OpenApForm('Data tidak berhasil di simpan, Error : ' + cst.pesan, 'Simpan Data');
					        }
					    }
					}
				)
		/* }
		else 
		{
			Ext.Ajax.request
			 (
				{
					 url: "./Datapool.mvc/UpdateDataObj",
					 params:  getParam_OpenApForm(), 
					 success: function(o) 
					 {
						
						    var cst = Ext.decode(o.responseText);
							if (cst.success === true)
							{
								ShowPesanInfo_OpenApForm('Data berhasil di edit','Edit Data');
								RefreshDataFilter_OpenApForm(false);
								RefreshDataItem_OpenApForm(cst.noapo,ShowDateAkuntansi(Ext.getCmp('dtpTanggal_OpenApForm').getValue()));
								RefreshDataJurnal_OpenApForm(cst.noapo,ShowDateAkuntansi(Ext.getCmp('dtpTanggal_OpenApForm').getValue()));
								dsTRListFaktur_OpenApForm.removeAll();
								selectUnit_OpenApForm=0;
								Ext.getCmp('comboUnit_OpenApForm').reset();
								selectVendor_OpenApForm=undefined;
								Ext.getCmp('cmbVendor_OpenApForm').reset();
							}
							else if (cst.success === false && cst.pesan === 0 )
							{
								ShowPesanWarning_OpenApForm('Data tidak berhasil di edit, ' +  cst.pesan ,'Edit Data');
							}
							else {
								ShowPesanError_OpenApForm('Data tidak berhasil di edit, '  + cst.pesan ,'Edit Data');
							}
												
					}
				}
			)
		} */
	}
};

function ValidasiEntry_OpenApForm(modul)
{
	var x = 1;
	/* if(Ext.get('txtNo_OpenApForm').getValue()=='')
	{
		ShowPesanWarning_OpenApForm('Nomor belum di isi',modul);
		x=0;
	}
	else  */if(selectCboEntryVendApForm == '' || selectCboEntryVendApForm == undefined)
	{
		// ShowPesanWarning_OpenApForm('Customer belum di pilih',modul);
		ShowPesanWarning_OpenApForm('Vendor belum dipilih');
		x=0;
	}
	else if(Ext.get('txtAmount_OpenApForm').getValue()=='' || Ext.get('txtAmount_OpenApForm').getValue()==0)
	{
		// ShowPesanWarning_OpenApForm('Amount belum di isi',modul);
		ShowPesanWarning_OpenApForm('Amount belum diisi');
		x=0;
	}else if (Ext.get('txtAmount_OpenApForm').getValue()=='0' || Ext.get('txtAmount_OpenApForm').getValue()=='0,00' || Ext.get('txtAmount_OpenApForm').getValue()=='0.00'){
		ShowPesanWarning_OpenArForm('Amount belum diisi');
		x=0;
	}
	else if (Ext.get('txtCatatan_OpenApForm').getValue().length > 100)
	{
		ShowPesanWarning_CSAPForm('Keterangan Tidak boleh lebih dari 100 Karakter',modul);
		x=0;
	};
	if (CekRowGridDetail_OpenApForm(modul) === 0 )
    {
        x=0;
    };
	return x;
};

function CekRowGridDetail_OpenApForm(modul)
{
    var x=1;
    if (dsDTLTRListItem_OpenApForm  != undefined || dsDTLTRListItem_OpenApForm !='')
    {
        if (dsDTLTRListItem_OpenApForm .getCount() == 0)
        {
            x=0;
            ShowPesanWarning_OpenApForm('Item belum di isi', modul);
        }
        else
        {
            for (var i = 0; i < dsDTLTRListItem_OpenApForm .getCount(); i++) 
            {
                if (dsDTLTRListItem_OpenApForm .data.items[i].data.ITEM_CODE === '' || dsDTLTRListItem_OpenApForm .data.items[i].data.ITEM_CODE === undefined)
                {
                    x=0;
                    if (dsDTLTRListItem_OpenApForm .data.items[i].data.ITEM_CODE != undefined)
                    {
/*                        ShowPesanWarning_OpenApForm('Account Dengan kode ' + dsDTLTRListItem_OpenApForm .data.items[i].data.ITEM_CODE + ' Belum Di Isi', modul);
                    }else
                    {*/
                        ShowPesanWarning_OpenApForm('Data tidak berhasil di simpan, Terdapat baris yang kosong', modul);
                    }
                }                              
                /* if (CekRowGridDiag_OpenApForm(dsDTLTRListItem_OpenApForm .data.items[i].data.ITEM_CODE) == 0)
                {
                    x=0
                    ShowPesanWarning_OpenApForm('Account ' + dsDTLTRListItem_OpenApForm .data.items[i].data.ITEM_CODE +' sudah ada, silahkan pilih Account yang lain ', modul);
                } */
            }
        }
    }
    else
    {   
        x=0;
    };
    return x;
};

function CekRowGridDiag_OpenApForm(Kdacc)
{
    var x='';
    for (var i = 0; i < dsDTLTRListItem_OpenApForm .getCount(); i++) 
    {
        if (Kdacc == dsDTLTRListItem_OpenApForm .data.items[i].data.ITEM_CODE)
        {
            x += 1  
        }
    }
    if (x > 1)
    {  
        x=0
    }

    return x;
};

function ValidasiDelete_OpenApForm(modul)
{
	var x = 1;

	if(Ext.get('txtNo_OpenApForm').getValue()=='')
	{
		// ShowPesanWarning_OpenApForm('Amount belum di isi',modul);
		//ShowPesanWarning_OpenApForm('Amount belum diisi');
		x=0;
	}
	return x;
};

function ValidasiApp_OpenApForm(modul,mBolHapus)
{
    var x = 1;
	/* if (Ext.get('txtNo_OpenApForm').dom.value =='' )
	{
		x=0;
		if (mBolHapus === false)
		{
			ShowPesanWarning_OpenApForm("Nomor belum terisi",modul);
		}
	} */
    return x;
}
function ShowPesanWarning_OpenApForm(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};

function ShowPesanError_OpenApForm(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR
		}
	);
};

function ShowPesanInfo_OpenApForm(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO
		}
	);
};

function DataDelete_OpenApForm() 
{
	if (ValidasiDelete_OpenApForm('Hapus Data') == 1 )
	{	
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/anggaran/fakturAP/delete",
				params:  getParamDelete_OpenApForm(), 
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true)					
					{
							ShowPesanInfo_OpenApForm('Data berhasil di hapus','Hapus Data');
							RefreshDataFilter_OpenApForm(false);
							AddNew_OpenApForm();
							//CalcTotalItem_OpenApForm();
							//CalcTotalJurnal_OpenApForm();

								dsTRListFaktur_OpenApForm.removeAll();
								selectUnit_OpenApForm=0;
								//Ext.getCmp('comboUnit_OpenApForm').reset();
								//selectVendor_OpenApForm=undefined;
								//Ext.getCmp('cmbVendor_OpenApForm').reset();
					}
					else if (cst.success === false && cst.pesan === 0 )
					{
						ShowPesanWarning_OpenApForm('Data tidak berhasil di hapus, data tersebut belum ada','Hapus Data');
					}
					else 
					{
						ShowPesanError_OpenApForm('Data tidak berhasil di hapus, '+ cst.pesan,'Hapus Data');
					}
				}
			}
		)
	}
};

function ButtonDisabled_OpenApForm(mBol)
{
	Ext.get('btnSimpan_OpenApForm').dom.disabled=mBol;
	Ext.get('btnSimpanKeluar_OpenApForm').dom.disabled=mBol;
	Ext.get('btnHapus_OpenApForm').dom.disabled=mBol;	
	Ext.get('btnApprove_OpenApForm').dom.disabled=mBol;	
	Ext.get('btnCetak_OpenApForm').dom.disabled=mBol;	
};

function GetVendorAcc_OpenApForm(kode)
{
	
	 Ext.Ajax.request
	 (
		{
			url: baseURL + "index.php/anggaran/fakturAP/getvendaccvendor",
            params: {kodevendor : kode},
			success: function(o) 
			{
			var cst = Ext.decode(o.responseText);
				if (cst.success === true )
				{
				 	StrAccVend_OpenApForm = cst.ListDataObj[0].account;
				 	strDue_DayApForm=cst.ListDataObj[0].term;
				 	StrNmAccVend=cst.ListDataObj[0].nameacc;
				}
				else
				{
					ShowPesanInfoRekapSP3D	('Vendor tidak ditemukan','Vendor');
				}
			}

		}
		);
};

function GetDueDate_OpenApForm(param)
{
	 Ext.Ajax.request
	 (
		{
			url: baseURL + "index.php/anggaran/fakturAR/getduedatecustomer_filterbytgl",
            params: {Params: param},
			success: function(o) 
			{
			var cst = Ext.decode(o.responseText);
				if (cst.success === true )
				{
				 	Ext.get('dtpDueDate_OpenApForm').dom.value=ShowDateAkuntansi(cst.DueDay)
				}
				else
				{
					// ShowPesanInfoRekapSP3D	('Customer tidak ditemukan','Customer');
				}
			}

		}
		);
};
function GetAkunKas_OpenApForm(param)
{
	 Ext.Ajax.request
	 (
		{
			url: "./Module.mvc/ExecProc",
			params:
			{
				UserID: 'Admin',
				ModuleID: 'GetAccKasArAp',
				Params:	param 
			},
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true )
				{
				 	// StrNmAccKas_OpenApForm=cst.NamaAcc
				 	// StrAccKas_OpenApForm=cst.Account
				 	Ext.get('comboAktivaLancar_OpenApForm').dom.value = cst.NamaAcc;
					selectAktivaLancar_OpenApForm = cst.Account;
				}
				else
				{
					//ShowPesanInfoRekapSP3D	('Kas tidak ditemukan','Customer');
				}
			}

		}
		);
};

function mcomboAktivaLancar_OpenApForm()
{
	var Field = ['Account','Name','Groups','AKUN'];
	dsAktivaLancarPenerimaanMhs = new WebApp.DataStore({ fields: Field });
	dsAktivaLancarPenerimaanMhs.load
	(
		{
			params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: '',
			    Sortdir: 'ASC',
			    target: 'viCboAktivaLancar',
			    param: ''
			}
		}
	);
	
 var comboAktivaLancar_OpenApForm = new Ext.form.ComboBox
	(
		{
			id:'comboAktivaLancar_OpenApForm',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih Kas / Bank...',
			fieldLabel: 'Kas / Bank ',
			align:'Right',
			listWidth:300,
			anchor:'100%',
			width:150,
			hidden:true,
			store: dsAktivaLancarPenerimaanMhs,
			valueField: 'Account',
			displayField: 'AKUN',
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectAktivaLancar_OpenApForm=b.data.Account ;
				} 
			}
		}
	);
	
	return comboAktivaLancar_OpenApForm;
} ;

var rowSelectedLookItem_OpenApForm;
var mWindowGridLookupItem_OpenApForm;
var CurrentDataLookupItem_OpenApForm =
	{
	    data: Object,
	    details: Array,
	    row: 0
	};
	
var dsLookupListItem_OpenApForm;

function FormLookupItemGrid_OpenApForm(criteria,dsStore,p,mBolAddNew,idx,mBolLookup) 
{
	
    var vWinFormEntry = new Ext.Window
	(
		{
			id: 'FormGrdPgwLookup_OpenApForm',
			title: 'Lookup Item',
			closable:true,
			width: 450,
			height: 400,
			border: true,
			plain: true,
			resizable:false,
			layout: 'form',
			iconCls: 'find',
			modal: true,                                   
			items: 
			[
				fnGetDTLGridLookUpItem_OpenApForm(criteria,dsStore,p,mBolAddNew,idx,mBolLookup,vWinFormEntry),
				{
					xtype:'button',
					text:'Ok',
					width:70,
					style:{'margin-left':'360px','margin-top':'7px'},
					hideLabel:true,
					id: 'btnOkAcc',
					handler:function()
					{
						if (p != undefined && rowSelectedLookItem_OpenApForm != undefined)
						{
							

								if (dsStore != undefined)
								{
									
									p.data.ITEM_CODE=rowSelectedLookItem_OpenApForm.data.ITEM_CODE;
									p.data.ITEM_DESC=rowSelectedLookItem_OpenApForm.data.ITEM_DESC;		
									p.data.ACCOUNT=rowSelectedLookItem_OpenApForm.data.ACCOUNT;
									p.data.DESCRIPTION=rowSelectedLookItem_OpenApForm.data.ITEM_DESC;
									
									dsStore.removeAt(idx);
									dsStore.insert(idx, p);
								}
							
						}
						rowSelectedLookItem_OpenApForm=undefined;
						vWinFormEntry.close();
					}	
				}
			],
			listeners:
				{ 
					activate: function()
					{ } 
				}
		}
	);
    vWinFormEntry.show();
	mWindowGridLookupItem_OpenApForm  = vWinFormEntry; 
};

///---------------------------------------------------------------------------------------///

function fnGetDTLGridLookUpItem_OpenApForm(criteria,dsStore,p,mBolAddNew,idx,mBolLookup,vWinFormEntry) 
{  
	var fldDetail = 
	[
		'ITEM_CODE','ITEM_DESC','ITEM_GROUP','ACCOUNT'
	];
	
	dsLookupListItem_OpenApForm = new WebApp.DataStore({ fields: fldDetail });
	var vGridLookAccFormEntry_OpenApForm = new Ext.grid.EditorGridPanel
	(
		{ 
			id:'vGridLookAccFormEntry_OpenApForm',
			title: '',
			stripeRows: true,
			store: dsLookupListItem_OpenApForm,
			height:330,
			columnLines:true,
			bodyStyle: 'padding:0px',
			border: false,		
			sm: new Ext.grid.RowSelectionModel
			(
				{ 
					singleSelect: true,
					listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedLookItem_OpenApForm = dsLookupListItem_OpenApForm.getAt(row);
							CurrentDataLookupItem_OpenApForm.row = row;
					        CurrentDataLookupItem_OpenApForm.data = rowSelectedLookItem_OpenApForm;
						}
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, row, rec)
				{
					rowSelectedLookItem_OpenApForm = dsLookupListItem_OpenApForm.getAt(row);
					CurrentDataLookupItem_OpenApForm.row = rec;
					CurrentDataLookupItem_OpenApForm.data = rowSelectedLookItem_OpenApForm;
					
					if (p != undefined && rowSelectedLookItem_OpenApForm != undefined)
						{
							
								if (dsStore != undefined)
								{
									
									p.data.ITEM_CODE=rowSelectedLookItem_OpenApForm.data.ITEM_CODE;
									p.data.ITEM_DESC=rowSelectedLookItem_OpenApForm.data.ITEM_DESC;		
									p.data.ACCOUNT=rowSelectedLookItem_OpenApForm.data.ACCOUNT;
									p.data.DESCRIPTION=rowSelectedLookItem_OpenApForm.data.ITEM_DESC;
									//p.data.VALUE=rowSelectedLookItem_OpenApForm.data.GOLONGAN_GAPOK;
									
									dsStore.removeAt(idx);
									dsStore.insert(idx, p);
								}
							
						}
						rowSelectedLookItem_OpenApForm=undefined;
						mWindowGridLookupItem_OpenApForm.close();
				}
			},
			cm: fnGridLookAccColumnModel_OpenApForm(),
			viewConfig: { forceFit: true }
		});
		
	dsLookupListItem_OpenApForm.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 100, 
				Sort: 'Item_Code', 
				Sortdir: 'ASC', 
				target:'viewFakItem',
				param: criteria
			} 
		}
	);
	return vGridLookAccFormEntry_OpenApForm;
};



function fnGridLookAccColumnModel_OpenApForm() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{ 
				id: 'colAkunlookup_OpenApFormDesc',
				header: "Account",
				dataIndex: 'ACCOUNT',
				width: 100
			},
			{ 
				id: 'colItemDescLookup_OpenApFormDesc',
				header: "Desc",
				dataIndex: 'ITEM_DESC',
				width: 300
			},
			/*{ 
				id: 'colItemLookup_OpenApForm',
				header: "Item",
				dataIndex: 'Item_Code',
				width: 70
			},*/
			

		]
	)
};
///---------------------------------------------------------------------------------------///
///=======FIND DIALOG-========
var nowFind_OpenApForm = new Date();
var selectcboOperator_OpenApForm;
var selectcboField_OpenApForm;
var winFind_OpenApForm;

function fnFindDlg_OpenApForm()
{  	
    winFind_OpenApForm = new Ext.Window
	(
		{ 
			id: 'winFindx_OpenApForm',
			title: 'Find',
			closeAction: 'destroy',
			width:350,
			height: 200,					
			border: false,
			resizable:false,
			plain: true,
			layout: 'fit',
			iconCls: 'find',
			modal: true,
			items: [ItemFindDlg_OpenApForm()]
			
		}
	);

	winFind_OpenApForm.show();
	
    return winFind_OpenApForm; 
};

function ItemFindDlg_OpenApForm() 
{	
	var PnlFindDlg_OpenApForm = new Ext.Panel
	(
		{ 
			id: 'PnlFindDlg_OpenApForm',
			fileUpload: true,
			layout: 'anchor',
			// width:350,
			height: 90,
			anchor: '100%',
			bodyStyle: 'padding:5px',
			border: true,
			items: 
			[

				{
					xtype: 'compositefield',
					fieldLabel: '',
					anchor: '100%',
					items: 
					[
						{
							xtype: 'fieldset',
							title: '',
							width: 325,
							height: 125, 
							items: 
							[
								mComboFindField_OpenApForm(),
								mComboOperator_OpenApForm(),
								getItemFindKeyWord_OpenApForm(),
								getItemFindTgl_OpenApForm(),
								
				
							]
						}						
					]
				},
				{
					layout: 'hBox',
					border: false,
					defaults: { margins: '0 5 0 0' },
					style:{'margin-left':'21px','margin-top':'5px'},
					anchor: '100%',
					layoutConfig: 
					{
						padding: '3',
						pack: 'end',
						align: 'middle'
					},
					items: 
					[
						{
							xtype: 'button',
							text: 'Ok',
							width: 70,
							hideLabel: true,
							id: 'btnOkLap_OpenApForm',
							handler: function() 
							{
								if (ValidasiReport_OpenApForm()==1){
									RefreshDataFindRecord_OpenApForm()
								}
								
							}
						},
						{
							xtype: 'button',
							text: 'Cancel',
							width: 70,
							hideLabel: true,
							id: 'btnCancelLap_OpenApForm',
							handler: function() 
							{
								winFind_OpenApForm.close();
							}
						}
					]
				}
			]
		}
	);
 
    return PnlFindDlg_OpenApForm;
};

function RefreshDataFindRecord_OpenApForm() 
{   
	var strparam='';
	if(selectcboOperator_OpenApForm==1){

		if (selectcboField_OpenApForm=="APF_DATE"){

			strparam="  "+selectcboField_OpenApForm+" like '"+ ShowDateAkuntansi(Ext.getCmp('dtpTglFind_OpenApForm').getValue()) + "%' "; 

		} else{
			strparam="  "+selectcboField_OpenApForm+" like '"+ Ext.getCmp('txtKeyWordFind_OpenApForm').getValue() + "%' "; 

		}		
		
	} else if (selectcboOperator_OpenApForm==2){

		if (selectcboField_OpenApForm=="APF_DATE"){

			strparam="  "+selectcboField_OpenApForm+" = '"+ ShowDateAkuntansi(Ext.getCmp('dtpTglFind_OpenApForm').getValue()) + "' "; 

		} else{
			strparam="  "+selectcboField_OpenApForm+" = '"+ Ext.getCmp('txtKeyWordFind_OpenApForm').getValue() + "' "; 

		}

	} else if (selectcboOperator_OpenApForm==3){
		 
		if (selectcboField_OpenApForm=="APF_DATE"){

			strparam="  "+selectcboField_OpenApForm+" < '"+ ShowDateAkuntansi(Ext.getCmp('dtpTglFind_OpenApForm').getValue()) + "' "; 

		} else{
			strparam="  "+selectcboField_OpenApForm+" < '"+ Ext.getCmp('txtKeyWordFind_OpenApForm').getValue() + "' ";

		}

	} else if (selectcboOperator_OpenApForm==4){

		if (selectcboField_OpenApForm=="APF_DATE"){

			strparam="  "+selectcboField_OpenApForm+" > '"+ ShowDateAkuntansi(Ext.getCmp('dtpTglFind_OpenApForm').getValue()) + "' "; 

		} else{
			strparam="  "+selectcboField_OpenApForm+" > '"+ Ext.getCmp('txtKeyWordFind_OpenApForm').getValue() + "' ";

		}
		
	} else if (selectcboOperator_OpenApForm==5){
		if (selectcboField_OpenApForm=="APF_DATE"){

			strparam="  "+selectcboField_OpenApForm+" <= '"+ ShowDateAkuntansi(Ext.getCmp('dtpTglFind_OpenApForm').getValue()) + "' "; 

		} else{
			strparam="  "+selectcboField_OpenApForm+" <= '"+ Ext.getCmp('txtKeyWordFind_OpenApForm').getValue() + "' ";

		}
		
		
	} else if (selectcboOperator_OpenApForm==6){
		if (selectcboField_OpenApForm=="APF_DATE"){

			strparam="  "+selectcboField_OpenApForm+" >= '"+ ShowDateAkuntansi(Ext.getCmp('dtpTglFind_OpenApForm').getValue()) + "' "; 

		} else{
			strparam="  "+selectcboField_OpenApForm+" >= '"+ Ext.getCmp('txtKeyWordFind_OpenApForm').getValue() + "' ";

		}
		
	} else if (selectcboOperator_OpenApForm==7){
		if (selectcboField_OpenApForm=="APF_DATE"){

			strparam="  "+selectcboField_OpenApForm+" <> '"+ ShowDateAkuntansi(Ext.getCmp('dtpTglFind_OpenApForm').getValue()) + "' "; 

		} else{
			strparam="  "+selectcboField_OpenApForm+" <> '"+ Ext.getCmp('txtKeyWordFind_OpenApForm').getValue() + "' ";

		}
		
	} 
	
    {  
		dsTRList_OpenApForm.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCount_OpenApForm, 
					Sort: 'APF_DATE', 
					Sortdir: 'ASC', 
					target:'viACC_AP_OPEN',
					param: strparam
				}			
			}
		);        
    }
	
};


function ValidasiReport_OpenApForm()
{
	var x=1;
	
	if(selectcboField_OpenApForm == undefined || selectcboField_OpenApForm == "")
	{
		ShowPesanWarningFindDlg_OpenApForm('Field belum dipilih','Find');
		x=0;		
	};

	if(selectcboOperator_OpenApForm ==undefined || selectcboOperator_OpenApForm== "")
	{
		
		ShowPesanWarningFindDlg_OpenApForm('Operator belum di pilih','Find');
		x=0;
			
	};

	if(Ext.getCmp('txtKeyWordFind_OpenApForm').getValue()== "")
	{
		
		ShowPesanWarningFindDlg_OpenApForm('Keyword belum di isi','Find');
		x=0;
			
	};

	return x;
};

function ShowPesanWarningFindDlg_OpenApForm(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING,
		   width:180
		}
	);
};

function getItemFindTgl_OpenApForm() 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
		{
			xtype: 'compositefield',
			fieldLabel: '-',
			anchor: '100%',
			items: 
			[
				 {
					columnWidth: 1,
					layout: 'form',
					border: false,
					labelAlign: 'left',
					labelWidth: 100,
					items:
					[
						{
							xtype: 'datefield',
							fieldLabel: 'Periode ',
							id: 'dtpTglFind_OpenApForm',
							format: 'd/M/Y',
							disabled:true,
							value:now_OpenApForm,
							width:190

						}
					]
		    	}
			]
		}
		   
		]
	}
    return items;
};

function getItemFindKeyWord_OpenApForm() 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
		{
			xtype: 'compositefield',
			fieldLabel: '-',
			anchor: '100%',
			items: 
			[
				 {
					columnWidth: 1,
					layout: 'form',
					border: false,
					labelAlign: 'left',
					labelWidth: 100,
					items:
					[
						{
							xtype: 'textfield',
							fieldLabel: 'Key Word ',
							id: 'txtKeyWordFind_OpenApForm',
							name: 'txtKeyWordFind_OpenApForm',
							width:190
						}
					]
		    	}
			 
			]
		}
		   
		]
	}
    return items;
};

function mComboFindField_OpenApForm()
{
	var cboFindField_OpenApForm = new Ext.form.ComboBox
	(
		{
			id:'cboFindField_OpenApForm',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih Field',
			width:190,
			fieldLabel: 'Field Name',	
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: [
							["APF_NUMBER", "Faktur Number"], 
							["APF_DATE", "Faktur Date"],
							["APO.VEND_CODE", "Vend Code"], 
							["v.VENDOR", "Vendor"],
							["NOTES", "Notes"]
						  ]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			listeners:  
			{
				'select': function(a,b,c)
				{
					selectcboField_OpenApForm=b.data.Id;

					if (b.data.Id=="APF_DATE"){
						Ext.getCmp('txtKeyWordFind_OpenApForm').disable();
						Ext.getCmp('dtpTglFind_OpenApForm').enable();
					} else{
						Ext.getCmp('dtpTglFind_OpenApForm').disable();
						Ext.getCmp('txtKeyWordFind_OpenApForm').enable();
					}
				}

			}
			
		}
	);
	return cboFindField_OpenApForm;
}


function mComboOperator_OpenApForm()
{
	var cboFindOperator_OpenApForm = new Ext.form.ComboBox
	(
		{
			id:'cboFindOperator_OpenApForm',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			width:190,
			emptyText:'Pilih Operator',
			fieldLabel: 'Operator ',
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: 
					[
						[1, "Like"], 
						[2, "="],
						[3, "<"],
						[4, ">"],
						[5, "<="], 
						[6, ">="],
						[7, "<>"]
					]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			listeners:  
			{
				'select': function(a,b,c)
				{
					selectcboOperator_OpenApForm=b.data.Id;
				}

			}
		}
	);
	return cboFindOperator_OpenApForm;
}
