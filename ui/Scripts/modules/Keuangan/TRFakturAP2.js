// Data Source ExtJS # --------------
// Deklarasi Variabel pada Kasir Rawat Jalan # --------------

var dataSource_viFakturAp;
var selectCount_viFakturAp = 50;
var NamaForm_viFakturAp = "Faktur AP";
var mod_name_viFakturAp = "viFakturAp";
var now_viFakturAp = new Date();
var addNew_viFakturAp;
var rowSelected_viFakturAp;
var setLookUps_viFakturAp;
var mNoKunjungan_viFakturAp = '';

var CurrentData_viFakturAp =
        {
            data: Object,
            details: Array,
            row: 0
        };

CurrentPage.page = dataGrid_viFakturAp(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

// End Deklarasi Variabel pada Kasir Rawat Jalan # --------------

// Start Project Kasir Rawat Jalan # --------------

// --------------------------------------- # Start Function # ---------------------------------------
// ## Silahkan tempatkan semua fungsi eksekusi disini

// --------------------------------------- # End Function # ---------------------------------------

// --------------------------------------- # Start Form # ---------------------------------------

/**
 *	Function : dataGrid_viFakturAp
 *	
 *	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
 */

function dataGrid_viFakturAp(mod_id_viFakturAp)
{
    // Field kiriman dari Project Net.
    var FieldMaster_viFakturAp =
            [
                'NO_KUNJUNGAN', 'KD_KELOMPOK', 'KD_UNIT', 'KD_DOKTER', 'KD_Vendor', 'KD_PASIEN',
                'TGL_KUNJUNGAN', 'JAM_KUNJUNGAN', 'TINGGI_BADAN', 'BERAT_BADAN', 'TEKANAN_DRH',
                'NADI', 'ALERGI', 'KELUHAN', 'RUJUK_RAD', 'RUJUK_LAB', 'TAHAP_PROSES', 'PASIEN_BARU',
                'NAMA_UNIT', 'KELOMPOK', 'DOKTER', 'Vendor', 'PS_BARU', 'KD_PENDIDIKAN', 'KD_STS_MARITAL',
                'KD_AGAMA', 'KD_PEKERJAAN', 'NAMA', 'TEMPAT_LAHIR', 'TGL_LAHIR', 'JENIS_KELAMIN', 'ALAMAT',
                'NO_TELP', 'NO_HP', 'GOL_DARAH', 'PENDIDIKAN', 'STS_MARITAL', 'AGAMA', 'PEKERJAAN', 'JNS_KELAMIN',
                'TAHUN', 'BULAN', 'HARI'
            ];
    // Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viFakturAp = new WebApp.DataStore
            ({
                fields: FieldMaster_viFakturAp
            });

    // Grid Kasir Rawat Jalan # --------------
    var GridDataView_viFakturAp = new Ext.grid.EditorGridPanel
            (
                    {
                        xtype: 'editorgrid',
                        title: '',
                        store: dataSource_viFakturAp,
                        autoScroll: true,
                        columnLines: true,
                        border: false,
                        anchor: '100% 100%',
                        plugins: [new Ext.ux.grid.FilterRow()],
                        selModel: new Ext.grid.RowSelectionModel
                                // Tanda aktif saat salah satu baris dipilih # --------------
                                        (
                                                {
                                                    singleSelect: true,
                                                    listeners:
                                                            {
                                                                rowselect: function (sm, row, rec)
                                                                {
                                                                    rowSelected_viFakturAp = undefined;
                                                                    rowSelected_viFakturAp = dataSource_viFakturAp.getAt(row);
                                                                    CurrentData_viFakturAp
                                                                    CurrentData_viFakturAp.row = row;
                                                                    CurrentData_viFakturAp.data = rowSelected_viFakturAp.data;
                                                                }
                                                            }
                                                }
                                        ),
                                // Proses eksekusi baris yang dipilih # --------------
                                listeners:
                                        {
                                            // Function saat ada event double klik maka akan muncul form view # --------------
                                            rowdblclick: function (sm, ridx, cidx)
                                            {
                                                rowSelected_viFakturAp = dataSource_viFakturAp.getAt(ridx);
                                                if (rowSelected_viFakturAp != undefined)
                                                {
                                                    setLookUp_viFakturAp(rowSelected_viFakturAp.data);
                                                }
                                                else
                                                {
                                                    setLookUp_viFakturAp();
                                                }
                                            }
                                            // End Function # --------------
                                        },
                                /**
                                 *	Mengatur tampilan pada Grid Kasir Rawat Jalan
                                 *	Terdiri dari : Judul, Isi dan Event
                                 *	Isi pada Grid di dapat dari pemangilan dari Net.
                                 */
                                colModel: new Ext.grid.ColumnModel
                                        (
                                                [
                                                    new Ext.grid.RowNumberer(),
                                                    {
                                                        id: 'colNoFak_viFakturAp',
                                                        header: 'Faktur Number',
                                                        dataIndex: ' ',
                                                        sortable: true,
                                                        width: 35,
                                                        filter:
                                                                {
                                                                    type: 'int'
                                                                }
                                                    },
                                                    //-------------- ## --------------
                                                    {
                                                        id: 'colNMCust_viFakturAp',
                                                        header: 'Vendor',
                                                        dataIndex: ' ',
                                                        sortable: true,
                                                        width: 50,
                                                        filter:
                                                                {}
                                                    },
                                                    {
                                                        id: 'colTglFak_viFakturAp',
                                                        header: 'Tgl Faktur',
                                                        dataIndex: ' ',
                                                        width: 35,
                                                        sortable: true,
                                                        // format: 'd/M/Y',
                                                        filter: {},
                                                        renderer: function (v, params, record)
                                                        {
                                                            return ShowDate(record.data.TGL_FAKTUR);
                                                        }
                                                    },
                                                    //-------------- ## --------------
                                                    {
                                                        id: 'colNotes_viFakturAp',
                                                        header: 'Notes',
                                                        dataIndex: ' ',
                                                        width: 100,
                                                        sortable: true,
                                                        filter: {}
                                                    }
                                                    //-------------- ## --------------
                                                ]
                                                ),
                                // Tolbar ke Dua # --------------
                                tbar:
                                        {
                                            xtype: 'toolbar',
                                            id: 'toolbar_viFakturAp',
                                            items:
                                                    [
                                                        {
                                                            xtype: 'button',
                                                            text: 'Edit Data',
                                                            iconCls: 'Edit_Tr',
                                                            tooltip: 'Edit Data',
                                                            id: 'btnEdit_viFakturAp',
                                                            handler: function (sm, row, rec)
                                                            {
                                                                if (rowSelected_viFakturAp != undefined)
                                                                {
                                                                    setLookUp_viFakturAp(rowSelected_viFakturAp.data)
                                                                }
                                                                else
                                                                {
                                                                    setLookUp_viFakturAp();
                                                                }
                                                            }
                                                        }
                                                        //-------------- ## --------------
                                                    ]
                                        },
                                // End Tolbar ke Dua # --------------
                                // Button Bar Pagging # --------------
                                // Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
                                bbar: bbar_paging(mod_name_viFakturAp, selectCount_viFakturAp, dataSource_viFakturAp),
                                // End Button Bar Pagging # --------------
                                viewConfig:
                                        {
                                            forceFit: true
                                        }
                            }
                            )

                    // Kriteria filter pada Grid # --------------
                    var FrmFilterGridDataView_viFakturAp = new Ext.Panel
                            (
                                    {
                                        title: NamaForm_viFakturAp,
                                        iconCls: 'Studi_Lanjut',
                                        id: mod_id_viFakturAp,
                                        region: 'center',
                                        layout: 'form',
                                        closable: true,
                                        border: false,
                                        margins: '0 5 5 0',
                                        items: [GridDataView_viFakturAp],
                                        tbar:
                                                [
                                                    {
                                                        //-------------- # Untuk mengelompokkan pencarian # --------------
                                                        xtype: 'buttongroup',
                                                        //title: 'Pencarian ' + NamaForm_viFakturAp,
                                                        columns: 9,
                                                        defaults: {
                                                            scale: 'small'
                                                        },
                                                        frame: false,
                                                        //-------------- ## --------------
                                                        items:
                                                                [
                                                                    {
                                                                        xtype: 'tbtext',
                                                                        text: 'Faktur Number : ',
                                                                        style: {'text-align': 'right'},
                                                                        width: 100,
                                                                        height: 25
                                                                    },
                                                                    {
                                                                        xtype: 'textfield',
                                                                        id: 'TxtFilterGridDataView_NoFaktur_viFakturAp',
                                                                        emptyText: 'No. Faktur',
                                                                        width: 150,
                                                                        height: 25,
                                                                        listeners:
                                                                                {
                                                                                    'specialkey': function ()
                                                                                    {
                                                                                        if (Ext.EventObject.getKey() === 13)
                                                                                        {
                                                                                            DfltFilterBtn_viFakturAp = 1;
                                                                                            DataRefresh_viFakturAp(getCriteriaFilterGridDataView_viFakturAp());
                                                                                        }
                                                                                    }
                                                                                }
                                                                    },
                                                                    {
                                                                        xtype: 'tbtext',
                                                                        text: 'Tgl Faktur : ',
                                                                        style: {'text-align': 'right'},
                                                                        width: 70,
                                                                        height: 25
                                                                    },
                                                                    {
                                                                        xtype: 'datefield',
                                                                        id: 'txtfilterTglFakAwal_viFakturAp',
                                                                        value: now_viFakturAp,
                                                                        format: 'd/M/Y',
                                                                        width: 120,
                                                                        listeners:
                                                                                {
                                                                                    'specialkey': function ()
                                                                                    {
                                                                                        if (Ext.EventObject.getKey() === 13)
                                                                                        {
                                                                                            datarefresh_viFakturAp();
                                                                                        }
                                                                                    }
                                                                                }
                                                                    },
                                                                    //-------------- ## --------------
                                                                    {
                                                                        xtype: 'tbtext',
                                                                        text: 's/d Tgl : ',
                                                                        style: {'text-align': 'center'},
                                                                        width: 50,
                                                                        height: 25
                                                                    },
                                                                    {
                                                                        xtype: 'datefield',
                                                                        id: 'txtfilterTglFakAkhir_viFakturAp',
                                                                        value: now_viFakturAp,
                                                                        format: 'd/M/Y',
                                                                        width: 120,
                                                                        listeners:
                                                                                {
                                                                                    'specialkey': function ()
                                                                                    {
                                                                                        if (Ext.EventObject.getKey() === 13)
                                                                                        {
                                                                                            datarefresh_viFakturAp();
                                                                                        }
                                                                                    }
                                                                                }
                                                                    },
                                                                    {
                                                                        xtype: 'button',
                                                                        text: 'Cari',
                                                                        iconCls: 'refresh',
                                                                        tooltip: 'Cari',
                                                                        style: {paddingLeft: '30px'},
                                                                        //rowspan: 3,
                                                                        width: 150,
                                                                        id: 'BtnFilterGridDataView_viFakturAp',
                                                                        handler: function ()
                                                                        {
                                                                            DfltFilterBtn_viFakturAp = 1;
                                                                            DataRefresh_viFakturAp(getCriteriaFilterGridDataView_viFakturAp());
                                                                        }
                                                                    },
                                                                    //-------------- ## --------------
                                                                ]
                                                                //-------------- # End items # --------------
                                                                //-------------- # End mengelompokkan pencarian # --------------
                                                    }
                                                ]
                                                //-------------- # End tbar # --------------
                                    }
                            )
                    return FrmFilterGridDataView_viFakturAp;
                    //-------------- # End form filter # --------------
                }
// End Function dataGrid_viFakturAp # --------------

        /**
         *	Function : setLookUp_viFakturAp
         *	
         *	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
         */

        function setLookUp_viFakturAp(rowdata)
        {
            var lebar = 860;
            setLookUps_viFakturAp = new Ext.Window
                    (
                            {
                                id: 'SetLookUps_viFakturAp',
                                name: 'SetLookUps_viFakturAp',
                                title: NamaForm_viFakturAp,
                                closeAction: 'destroy',
                                width: 875,
                                height: 580,
                                resizable: false,
                                autoScroll: false,
                                border: true,
                                constrainHeader: true,
                                iconCls: 'Studi_Lanjut',
                                modal: true,
                                items: getFormItemEntry_viFakturAp(lebar, rowdata), //1
                                listeners:
                                        {
                                            activate: function ()
                                            {

                                            },
                                            afterShow: function ()
                                            {
                                                this.activate();
                                            },
                                            deactivate: function ()
                                            {
                                                rowSelected_viFakturAp = undefined;
                                                //datarefresh_viFakturAp();
                                                mNoKunjungan_viFakturAp = '';
                                            }
                                        }
                            }
                    );

            setLookUps_viFakturAp.show();
            if (rowdata == undefined)
            {
                // dataaddnew_viFakturAp();
                // Ext.getCmp('btnDelete_viFakturAp').disable();	
            }
            else
            {
                // datainit_viFakturAp(rowdata);
            }
        }
// End Function setLookUpGridDataView_viFakturAp # --------------

        /**
         *	Function : getFormItemEntry_viFakturAp
         *	
         *	Sebuah fungsi untuk menampilkan penataan menu dan isian form edit
         */

        function getFormItemEntry_viFakturAp(lebar, rowdata)
        {
            var pnlFormDataBasic_viFakturAp = new Ext.FormPanel
                    (
                            {
                                title: '',
                                region: 'north',
                                layout: 'form',
                                bodyStyle: 'padding:10px 10px 10px 10px',
                                anchor: '100%',
                                width: lebar,
                                border: false,
                                //-------------- #items# --------------
                                items: [getItemPanelInputBiodata_viFakturAp(lebar), getItemDataKunjungan_viFakturAp(lebar)], //,getItemPanelBawah_viFakturAp(lebar)],
                                //-------------- #End items# --------------
                                fileUpload: true,
                                // Tombol pada tollbar Edit Data Pasien
                                tbar:
                                        {
                                            xtype: 'toolbar',
                                            items:
                                                    [
                                                        {
                                                            xtype: 'button',
                                                            text: 'Add',
                                                            iconCls: 'add',
                                                            id: 'btnAdd_viFakturAp',
                                                            handler: function () {
                                                                dataaddnew_viFakturAp();
                                                            }
                                                        },
                                                        //-------------- ## --------------
                                                        {
                                                            xtype: 'tbseparator'
                                                        },
                                                        //-------------- ## --------------
                                                        {
                                                            xtype: 'button',
                                                            text: 'Save',
                                                            iconCls: 'save',
                                                            id: 'btnSimpan_viFakturAp',
                                                            handler: function ()
                                                            {
                                                                datasave_viFakturAp(addNew_viFakturAp);
                                                                datarefresh_viFakturAp();
                                                            }
                                                        },
                                                        //-------------- ## --------------
                                                        {
                                                            xtype: 'tbseparator'
                                                        },
                                                        //-------------- ## --------------
                                                        {
                                                            xtype: 'button',
                                                            text: 'Save & Close',
                                                            iconCls: 'saveexit',
                                                            id: 'btnSimpanExit_viFakturAp',
                                                            handler: function ()
                                                            {
                                                                var x = datasave_viFakturAp(addNew_viFakturAp);
                                                                datarefresh_viFakturAp();
                                                                if (x === undefined)
                                                                {
                                                                    setLookUps_viFakturAp.close();
                                                                }
                                                            }
                                                        },
                                                        //-------------- ## --------------
                                                        {
                                                            xtype: 'tbseparator'
                                                        },
                                                        //-------------- ## --------------
                                                        {
                                                            xtype: 'button',
                                                            text: 'Delete',
                                                            iconCls: 'remove',
                                                            id: 'btnDelete_viFakturAp',
                                                            handler: function ()
                                                            {
                                                                datadelete_viFakturAp();
                                                                datarefresh_viFakturAp();

                                                            }
                                                   },
                                                    ]
                                        }//,items:
                            }
                    )

            return pnlFormDataBasic_viFakturAp;
        }
// End Function getFormItemEntry_viFakturAp # --------------

        /**
         *	Function : getItemPanelInputBiodata_viFakturAp
         *	
         *	Sebuah fungsi untuk menampilkan isian form edit data
         */

        function getItemPanelInputBiodata_viFakturAp(lebar)
        {
            var items =
                    {
                        layout: 'Form',
                        anchor: '100%',
                        width: lebar - 80,
                        labelAlign: 'Left',
                        bodyStyle: 'padding:10px 10px 10px 10px',
                        border: true,
                        items:
                                [
                                    {
                                        xtype: 'compositefield',
                                        fieldLabel: 'Faktur Number',
                                        anchor: '100%',
                                        //labelSeparator: '',
                                        width: 199,
                                        items:
                                                [
                                                    {
                                                        xtype: 'textfield',
                                                        flex: 1,
                                                        fieldLabel: 'Label',
                                                        width: 200,
                                                        name: 'txtFakturNum_viFakturAp',
                                                        id: 'txtFakturNum_viFakturAp',
                                                        style: {'text-align': 'left'},
                                                        emptyText: 'Faktur Number',
                                                        listeners:
                                                                {
                                                                    'specialkey': function ()
                                                                    {
                                                                        if (Ext.EventObject.getKey() === 13)
                                                                        {
                                                                        }
                                                                        ;
                                                                    }
                                                                }
                                                    },
                                                    //-------------- ## --------------
                                                    {
                                                        xtype: 'displayfield',
                                                        flex: 1,
                                                        width: 340,
                                                        name: '',
                                                        value: ' ',
                                                        fieldLabel: 'Label'
                                                    },
                                                    {
                                                        xtype: 'displayfield',
                                                        flex: 1,
                                                        width: 35,
                                                        name: '',
                                                        value: 'Date:',
                                                        fieldLabel: 'Label'
                                                    },
                                                    {
                                                        xtype: 'datefield',
                                                        id: 'dtpTglFaktur_viFakturAp',
                                                        value: now_viFakturAp,
                                                        format: 'd/M/Y',
                                                        width: 120,
                                                        listeners:
                                                                {
                                                                    'specialkey': function ()
                                                                    {
                                                                        if (Ext.EventObject.getKey() === 13)
                                                                        {
                                                                            datarefresh_viFakturAp();
                                                                        }
                                                                    }
                                                                }
                                                    },
                                                    //-------------- ## --------------
                                                ]
                                    },
                                    //-------------- ## --------------				
                                    {
                                        xtype: 'compositefield',
                                        fieldLabel: 'Vendor',
                                        name: 'compVendor_viFakturAp',
                                        id: 'compVendor_viFakturAp',
                                        items:
                                                [
                                                    viCombo_VendorFakAR(515, 'ComboCust_fakAR'),
                                                    {
                                                        xtype: 'displayfield',
                                                        flex: 1,
                                                        width: 65,
                                                        name: '',
                                                        value: 'Due Date:',
                                                        fieldLabel: 'Label'
                                                    },
                                                    {
                                                        xtype: 'datefield',
                                                        id: 'dtpDueDate_viFakturAp',
                                                        value: now_viFakturAp,
                                                        format: 'd/M/Y',
                                                        width: 120,
                                                        listeners:
                                                                {
                                                                    'specialkey': function ()
                                                                    {
                                                                        if (Ext.EventObject.getKey() === 13)
                                                                        {
                                                                            datarefresh_viFakturAp();
                                                                        }
                                                                    }
                                                                }
                                                    },
                                                ]
                                    },
                                    //-------------- ## --------------	
                                    {
                                        xtype: 'compositefield',
                                        fieldLabel: 'Notes',
                                        name: 'compNotes_viFakturAp',
                                        id: 'compNotes_viFakturAp',
                                        items:
                                                [
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: '',
                                                        width: 515,
                                                        name: 'txtNotes_viFakturAp',
                                                        id: 'txtNotes_viFakturAp',
                                                        emptyText: 'Notes....'
                                                    },
                                                    {
                                                        xtype: 'displayfield',
                                                        width: 65,
                                                        value: 'Reference:',
                                                        fieldLabel: 'Label',
                                                        id: 'lblReference_viFakturAp',
                                                        name: 'lblReference_viFakturAp'
                                                    },
                                                    {
                                                        xtype: 'textfield',
                                                        fieldLabel: '',
                                                        width: 120,
                                                        name: 'txtReference_viFakturAp',
                                                        id: 'txtReference_viFakturAp',
                                                        emptyText: 'Reference'
                                                    }
                                                    //-------------- ## --------------
                                                ]
                                    }
                                    //-------------- ## --------------				
                                ]
                    };
            return items;
        }
        ;
// End Function getItemPanelInputBiodata_viFakturAp # --------------

        /**
         *	Function : getItemDataKunjungan_viFakturAp
         *	
         *	Sebuah fungsi untuk menampilkan isian form edit data
         */

        function getItemDataKunjungan_viFakturAp(lebar)
        {
            var items =
                    {
                        layout: 'form',
                        anchor: '100%',
                        labelAlign: 'Left',
                        bodyStyle: 'padding:10px 10px 10px 10px',
                        width: lebar - 80,
                        height: 400,
                        items:
                                [
                                    {
                                        xtype: 'panel',
                                        title: '',
                                        border: false,
                                        height: 375,
                                        items:
                                                [
                                                    {
                                                        xtype: 'tabpanel',
                                                        activeTab: 0,
                                                        height: 370,
                                                        items:
                                                                [
                                                                    {
                                                                        xtype: 'panel',
                                                                        title: 'Faktur',
                                                                        padding: '4px',
                                                                        items:
                                                                                [
                                                                                    gridDataTrans_viFakturAp(),
                                                                                    {
                                                                                        xtype: 'tbspacer',
                                                                                        height: 5
                                                                                    },
                                                                                    {
                                                                                        xtype: 'textfield',
                                                                                        id: 'txttotalfaktur_viFakturAp',
                                                                                        name: 'txttotalfaktur_viFakturAp',
                                                                                        style: {'text-align': 'right', 'margin-left': '700px'},
                                                                                        width: 107,
                                                                                        value: 0,
                                                                                        readOnly: true,
                                                                                    }
                                                                                ]
                                                                    },
                                                                    {
                                                                        xtype: 'panel',
                                                                        title: 'Journal',
                                                                        padding: '4px',
                                                                        items:
                                                                                [
                                                                                    gridDataTransPntJasa_viFakturAp(),
                                                                                    {
                                                                                        xtype: 'tbspacer',
                                                                                        height: 5
                                                                                    },
                                                                                    {
                                                                                        xtype: 'textfield',
                                                                                        id: 'txttotaltranspntjasa_viFakturAp',
                                                                                        name: 'txttotaltranspntjasa_viFakturAp',
                                                                                        style: {'text-align': 'right', 'margin-left': '700px'},
                                                                                        width: 107,
                                                                                        value: 0,
                                                                                        readOnly: true,
                                                                                    }
                                                                                ]
                                                                    }
                                                                ]
                                                    },
                                                ]
                                    }
                                ]
                    };
            return items;
        }
        ;
// End Function getItemDataKunjungan_viFakturAp # --------------

        /**
         *	Function : getItemPanelBawah_viFakturAp
         *	
         *	Sebuah fungsi untuk menampilkan isian form edit data
         */

        /**
         *	Function : gridDataTrans_viFakturAp
         *	
         *	Sebuah fungsi untuk menampilkan isian form edit data
         */

        function gridDataTrans_viFakturAp()
        {

            var FieldGrdKasir_viFakturAp =
                    [
                    ];

            dsDataGrdJab_viFakturAp = new WebApp.DataStore
                    ({
                        fields: FieldGrdKasir_viFakturAp
                    });

            var items =
                    {
                        xtype: 'editorgrid',
                        store: dsDataGrdJab_viFakturAp,
                        height: 300,
                        selModel: new Ext.grid.RowSelectionModel
                                (
                                        {
                                            singleSelect: true,
                                            listeners:
                                                    {
                                                        rowselect: function (sm, row, rec)
                                                        {
                                                        }
                                                    }
                                        }
                                ),
                        columns:
                                [
                                    {
                                        dataIndex: '',
                                        header: 'Item#',
                                        sortable: true,
                                        width: 100
                                    },
                                    //-------------- ## --------------
                                    {
                                        dataIndex: '',
                                        header: 'Description',
                                        sortable: true,
                                        width: 600
                                    },
                                    //-------------- ## --------------			
                                    {
                                        dataIndex: '',
                                        header: 'Amount',
                                        sortable: true,
                                        width: 100
                                    },
                                ]
                    }
            return items;
        }

        function gridDataTransPntJasa_viFakturAp()
        {

            var FieldGrdKasir_viFakturAp =
                    [
                    ];

            dsDataGrdJab_viFakturAp = new WebApp.DataStore
                    ({
                        fields: FieldGrdKasir_viFakturAp
                    });

            var items =
                    {
                        xtype: 'editorgrid',
                        store: dsDataGrdJab_viFakturAp,
                        height: 300,
                        selModel: new Ext.grid.RowSelectionModel
                                (
                                        {
                                            singleSelect: true,
                                            listeners:
                                                    {
                                                        rowselect: function (sm, row, rec)
                                                        {
                                                        }
                                                    }
                                        }
                                ),
                        columns:
                                [
                                    {
                                        dataIndex: '',
                                        header: 'Item#',
                                        sortable: true,
                                        width: 100
                                    },
                                    //-------------- ## --------------
                                    {
                                        dataIndex: '',
                                        header: 'Description',
                                        sortable: true,
                                        width: 600
                                    },
                                    //-------------- ## --------------			
                                    {
                                        dataIndex: '',
                                        header: 'Amount',
                                        sortable: true,
                                        width: 100
                                    },
                                ]
                    }
            return items;
        }

// ## MENU LOOKUP ## ------------------------------------------------------------------

// ## MENU LOOKUP - LOOKUP PRODUK/ TINDAKAN ## ------------------------------------------------------------------

        function getItemPanelInputGantiDokterDataView_viFakturAp(lebar)
        {
            var items =
                    {
                        title: '',
                        layout: 'Form',
                        anchor: '100%',
                        width: lebar,
                        labelAlign: 'Left',
                        bodyStyle: 'padding:1px 1px 1px 1px',
                        border: false,
                        items:
                                [
                                    {
                                        xtype: 'compositefield',
                                        fieldLabel: 'Tanggal',
                                        anchor: '100%',
                                        width: 250,
                                        items:
                                                [
                                                    {
                                                        xtype: 'textfield',
                                                        id: 'TxtTglGantiDokter_viFakturAp',
                                                        emptyText: '01',
                                                        width: 30,
                                                    },
                                                    //-------------- ## --------------  
                                                    {
                                                        xtype: 'textfield',
                                                        id: 'TxtBlnGantiDokter_viFakturAp',
                                                        emptyText: '01',
                                                        width: 30,
                                                    },
                                                    //-------------- ## --------------  
                                                    {
                                                        xtype: 'textfield',
                                                        id: 'TxtThnGantiDokter_viFakturAp',
                                                        emptyText: '2014',
                                                        width: 50,
                                                    },
                                                            //-------------- ## --------------  
                                                ]
                                    },
                                    //-------------- ## --------------
                                    {
                                        xtype: 'compositefield',
                                        fieldLabel: 'Dokter Asal',
                                        anchor: '100%',
                                        width: 250,
                                        items:
                                                [
                                                    //-------------- ## --------------  
                                                ]
                                    },
                                    //-------------- ## --------------
                                    {
                                        xtype: 'compositefield',
                                        fieldLabel: 'Dokter Ganti',
                                        anchor: '100%',
                                        width: 250,
                                        items:
                                                [
                                                    //-------------- ## --------------  
                                                ]
                                    },
                                    //-------------- ## --------------
                                ]
                    };
            return items;
        }

        function viComboKamar_viFakturAp()
        {
            var cbo_viComboKamar_viFakturAp = new Ext.form.ComboBox
                    (
                            {
                                id: "cbo_viComboKamar_viFakturAp",
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                emptyText: 'Pilih Kamar..',
                                fieldLabel: "Kamar",
                                value: 1,
                                width: 180,
                                store: new Ext.data.ArrayStore
                                        (
                                                {
                                                    id: 0,
                                                    fields:
                                                            [
                                                                'IdKamar',
                                                                'displayTextKamar'
                                                            ],
                                                    data: [[1, "011 VVIP"], [2, "012 VVIP"]]
                                                }
                                        ),
                                valueField: 'IdKamar',
                                displayField: 'displayTextKamar',
                                listeners:
                                        {
                                        }
                            }
                    );
            return cbo_viComboKamar_viFakturAp;
        }
        ;

        function viCombo_VendorFakAR(lebar, Nama_ID)
        {
            var Field_VendorFakAR = ['KD_Vendor', 'Vendor'];
            ds_VendorFakAR = new WebApp.DataStore({fields: Field_VendorFakAR});

            // viRefresh_Journal();

            var cbo_VendorFakAR = new Ext.form.ComboBox
                    (
                            {
                                flex: 1,
                                fieldLabel: 'Vendor',
                                valueField: 'KD_Vendor',
                                displayField: 'Vendor',
                                emptyText: 'Vendor...',
                                store: ds_VendorFakAR,
                                width: lebar,
                                mode: 'local',
                                typeAhead: true,
                                triggerAction: 'all',
                                name: Nama_ID,
                                lazyRender: true,
                                id: Nama_ID,
                                listeners:
                                        {
                                            'specialkey': function ()
                                            {
                                                if (Ext.EventObject.getKey() === 13)
                                                {

                                                }
                                            }
                                        }
                            }
                    )
            return cbo_VendorFakAR;
        }