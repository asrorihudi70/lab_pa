
var now = new Date();
now.format('Y-m-d');
var dsSpesialisasiFaktur_AR;
var ListHasilFaktur_AR;
var rowSelectedHasilFaktur_AR;
var dsKelasFaktur_AR;
var dsKamarFaktur_AR;
var xz=0;
var dataSource_Faktur_AR;
var dataSource_detFaktur_AR;
var dsPerawatFaktur_AR;
var rowSelected_viFaktur_AR;
var account_nci_Faktur_AR;
var creteria_Faktur_AR="";
var account_payment_Faktur_AR;
var  grid_detail_Faktur_AR;
var ds_Faktur_AR_grid_detail;
var Account_Faktur_AR_ds=new Ext.data.ArrayStore({id: 0,fields: ['text','account','name'],data: []});
var Pembayaran_Faktur_AR_ds=new Ext.data.ArrayStore({id: 0,fields: ['text','pay_code','payment'],data: []});

var gridListHasilFaktur_AR;
CurrentPage.page = getPanelFaktur_AR(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
function getPanelFaktur_AR(mod_id) {
    var Field = ['arf_number', 'date', 'cust_code','customer', 'posted','notes','reff','due_date'];
    dataSource_Faktur_AR = new WebApp.DataStore({
        fields: Field
    });

    load_Faktur_AR("");
   gridListHasilFaktur_AR = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dataSource_Faktur_AR,
        anchor: '100% 80%',
        columnLines: false,
        autoScroll: false,
        border: true,
        sort: false,
        autoHeight: false,
        layout: 'fit',
        region: 'center',
        height: 150,
        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
            listeners: {
                rowselect: function (sm, row, rec) {
                    //rowSelected_viFaktur_AR = dataSource_Faktur_AR.getAt(row);
                }
            }
        }),
        listeners: {
            rowdblclick: function (sm, ridx, cidx) {
                rowSelected_viFaktur_AR = dataSource_Faktur_AR.getAt(ridx);
                console.log(rowSelected_viFaktur_AR);
                if (rowSelected_viFaktur_AR !== undefined)
                {
                    console.log(rowSelected_viFaktur_AR);
                      if(rowSelected_viFaktur_AR.data.posted==='true' || rowSelected_viFaktur_AR.data.posted==='t')
                     {
                        // ShowPesanWarningFaktur_AR('Data yang sudah di posting tidak bisa di ubah', 'Posting');
                        HasilFaktur_ARLookUp(rowSelected_viFaktur_AR);
                                    
                     }else{
                        HasilFaktur_ARLookUp(rowSelected_viFaktur_AR);
                     }
                }
                else
                {
                    HasilFaktur_ARLookUp();
                }
            }
        },
        cm: new Ext.grid.ColumnModel(
                [
                    new Ext.grid.RowNumberer(),
                    {
                        id: 'colCodeViewstatusFaktur_AR',
                        header: 'Posting',
                        dataIndex: 'posted',
                        sortable: true,
                        width: 90,
                        style: 'text-align: center',
                        renderer: function(value, metaData, record, rowIndex, colIndex, store)
                        {
                             switch (value)
                             { 
                                 case 't':
                                         metaData.css = 'StatusHijau'; //
                                         break;
                                 case 'f':
                                        metaData.css = 'StatusMerah'; // rejected

                                         break;
                             }
                             return '';
                        }
                    },{
                        id: 'colCodeViewHasilFaktur_AR',
                        header: 'No Faktur',
                        dataIndex: 'arf_number',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 75
                    },
                    {
                        id: 'colaccountViewHasilFaktur_AR',
                        header: 'Tanggal',
                        dataIndex: 'date',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
                    {
                        id: 'colnama_accountViewHasilFaktur_AR',
                        header: 'Customer',
                        dataIndex: 'customer',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 170
                    }
                ]
                ),
        tbar: {
            xtype: 'toolbar',
            items: [
                        {
                            xtype: 'button',
                            text: 'Baru',
                            iconCls: 'AddRow',
                            id: 'btnNew_Faktur_AR',
                            handler: function ()
                            {
                                HasilFaktur_ARLookUp();
                            }
                        } ,
                    ]
        },
        viewConfig: {
            forceFit: true
        }
    });
    var FormDepanFaktur_AR = new Ext.Panel({
        id: mod_id,
        closable: true,
        region: 'center',
        layout: 'form',
        title: 'Faktur_AR',
        border: false,
        shadhow: true,
        autoScroll: false,
        iconCls: 'Request',
        margins: '0 5 5 0',
        items: [
            getPanelPencarianFaktur_AR(),
            gridListHasilFaktur_AR
        ],
        listeners: {
            'afterrender': function () {
                Ext.getCmp('cboStatus_PostingFaktur_AR').setValue('Semua');
            }
        }
    });
    return FormDepanFaktur_AR;
}
;
function getPanelPencarianFaktur_AR() {
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 1190,
                height: 100,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'No Faktur '
                    },
                    {
                        x: 110,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 10,
                        xtype: 'textfield',
                        name: 'TxtCodeFaktur_AR',
                        id: 'TxtCodeFaktur_AR',
                        width: 150,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
                                            load_Faktur_AR(Ext.getCmp('TxtCodeFaktur_AR').getValue(),Ext.getCmp('TxtTgl_voucher_Faktur_AR').getValue(),Ext.getCmp('TxtTgl_voucher_Faktur_AR2').getValue());
                                        }

                                    }
                                }
                    },
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Tanggal '
                    },
                    {
                        x: 110,
                        y: 40,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 40,
                        xtype: 'datefield',
                        name: 'TxtTgl_voucher_Faktur_AR',
                        id: 'TxtTgl_voucher_Faktur_AR',
                        format:'d/M/Y',
                        width: 200,
                        value :now,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
                                            load_Faktur_AR(Ext.getCmp('TxtCodeFaktur_AR').getValue(),Ext.getCmp('TxtTgl_voucher_Faktur_AR').getValue(),Ext.getCmp('TxtTgl_voucher_Faktur_AR2').getValue());
                                        }
                                    }
                                }
                    },
                    {
                        x: 330,
                        y: 40,
                        xtype: 'datefield',
                        name: 'TxtTgl_voucher_Faktur_AR2',
                        id: 'TxtTgl_voucher_Faktur_AR2',
                        format:'d/M/Y',
                        width: 200,
                        value :now,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
                                            load_Faktur_AR(Ext.getCmp('TxtCodeFaktur_AR').getValue(),Ext.getCmp('TxtTgl_voucher_Faktur_AR').getValue(),Ext.getCmp('TxtTgl_voucher_Faktur_AR2').getValue());
                                        }
                                    }
                                }
                    },
                    {
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Status Posting '
                    },
                    {
                        x: 110,
                        y: 70,
                        xtype: 'label',
                        text: ' : '
                    },
                    mComboStatus_PostingFaktur_AR()
                ]
            }
        ]
    };
    return items;
}
;
function creteria_Faktur_AR2(){
var msec =new Date();
    msec.parse(" 2012-01-01");
    if (Ext.getCmp('TxtCodeFaktur_AR').getValue()!=="" && Ext.getCmp('TxtTgl_voucher_Faktur_AR').getValue()==="" )
    {
    creteria_Faktur_AR="lower(cso_number) like lower('%"+ Ext.getCmp('TxtCodeFaktur_AR').getValue()+"%')";            
    }
    if (Ext.getCmp('TxtCodeFaktur_AR').getValue()==="" && Ext.getCmp('TxtTgl_voucher_Faktur_AR').getValue()!=="" )
    {
    creteria_Faktur_AR="cso_date in('"+ msec +"')";
        
    }
    if (Ext.getCmp('TxtCodeFaktur_AR').getValue()!=="" && Ext.getCmp('TxtTgl_voucher_Faktur_AR').getValue()!=="" )
    {
    creteria_Faktur_AR="lower(cso_number) like lower('"+ Ext.getCmp('TxtCodeFaktur_AR').getValue()+"%') and cso_date in ('"+ Ext.getCmp('TxtTgl_voucher_Faktur_AR').getValue()+"')";
        
    }
    if (Ext.getCmp('TxtCodeFaktur_AR').getValue()==="" && Ext.getCmp('TxtTgl_voucher_Faktur_AR').getValue()==="" )
    {
    creteria_Faktur_AR="";
    }

    return creteria_Faktur_AR;
}
function HasilFaktur_ARLookUp(rowdata) {
    FormLookUpdetailFaktur_AR = new Ext.Window({
        id: 'gridHasilFaktur_AR',
        title: 'Faktur AR',
        closeAction: 'destroy',
        width: 800,
        height: 600,
        border: false,
        resizable: false,
        plain: true,
        layout: 'fit',
        constrain: true,
        iconCls: 'Request',
        modal: true,
        items: [
                form_Faktur_AR(),
                ],
        listeners: {
        }
    });
    FormLookUpdetailFaktur_AR.show();
    if (rowdata === undefined||rowdata === "") {
        new_data_Faktur_AR();
    } else {
        datainit_Faktur_AR(rowdata);
    }

}
;
function form_Faktur_AR() {
    var Isi_form_Faktur_AR = new Ext.Panel
            (
                    {
                        id: 'form_Faktur_AR',
                        closable: true,
                        region: 'center',
                        layout: 'form',
                        itemCls: 'blacklabel',
                        bodyStyle: 'padding: 0px 0px 0px 0px',
                        border: true,
                        shadhow: true,
                        width: 800,
                        height: 200,
                        iconCls: '',
                        items:
                                [
                                    PanelFaktur_AR(),
                                    Getgrid_detail_Faktur_AR()
                                ],
                        tbar:
                                {
                                    xtype: 'toolbar',
                                    items:
                                            [
                                                {
                                                    xtype: 'button',
                                                    text: 'Baru',
                                                    iconCls: 'AddRow',
                                                    id: 'btnNew_Faktur_AR2',
                                                    handler: function ()
                                                    {
                                                        new_data_Faktur_AR();
                                                    }
                                                },
                                                {
                                                    xtype: 'button',
                                                    text: 'Simpan',
                                                    iconCls: 'save',
                                                    id: 'btnSimpan_Faktur_AR',
                                                    handler: function ()
                                                    {
                                                        SavingData_Faktur_AR()
                                                    }
                                                },
                                                {
                                                    xtype: 'button',
                                                    text: 'Posting',
                                                    id: 'btnposting_Faktur_AR',
                                                    handler: function ()
                                                    {
                                                        if (Ext.getCmp('TxtVoucherFaktur_AR').getValue()==="")
                                                        {
                                                            ShowPesanWarningFaktur_AR('Tidak dapat diposting, data sebelum di Simpan', 'Perhatian');        
                                                        }else{
                                                            Posting_LookUp_Faktur_AR()
                                                        }
                                                
                                                    }
                                                },
                                                {
                                                    xtype: 'button',
                                                    text: 'Print',
                                                    id: 'btnPrint_Faktur_AR',
                                                    handler: function ()
                                                    {         
                                                        if (Ext.getCmp('TxtVoucherFaktur_AR').getValue()==="")
                                                        {
                                                            ShowPesanWarningFaktur_AR('Tidak dapat di print, data sebelum di Simpan', 'Perhatian');        
                                                        }else{
                                                             Print_LookUp_Faktur_AR()
                                                        }                                              
                                                       
                                                    }
                                                },                                                 
                                            ]
                                }
                    }
            );
    return Isi_form_Faktur_AR;
}
;
var FormLook_Posting_Faktur_AR;
function Posting_LookUp_Faktur_AR(rowdata) {
    xz=0;
    FormLook_Posting_Faktur_AR = new Ext.Window({
        id: 'FormLook_Posting_Faktur_AR',
        title: 'Posting',
        closeAction: 'destroy',
        width: 400,
        height: 240,
        border: false,
        resizable: false,
        plain: true,
        layout: 'fit',
        constrain: true,
        iconCls: 'Request',
        modal: true,
        items: [
                PanelFaktur_AR_posting(),
                ],
        listeners: {
        },
        fbar:
            {
                xtype: 'toolbar',
                items:
                        [
                            {
                                xtype: 'button',
                                text: 'Posting',
                                iconCls: '',
                                id: 'btnSimpanpost_Faktur_AR',
                                handler: function ()
                                {
                                    PostData_Faktur_AR()
                                }
                            }
                        ]
            }
                    
    });
    FormLook_Posting_Faktur_AR.show();
    datainit_post_Faktur_AR(rowdata);
    
};
function parampost_Faktur_AR()
{
    var params =
            {
            arf_number  :Ext.getCmp('Txt_post_VoucherFaktur_AR').getValue(),
            catatan     :Ext.getCmp('Txt_Post_catatan_Faktur_AR').getValue(),
            };
   return params;
}
function PostData_Faktur_AR()
{
        Ext.Ajax.request
        (
            {   
                url: baseURL + "index.php/anggaran/fakturAR/post",
                params: parampost_Faktur_AR(),
                failure: function (o){
                    load_Faktur_AR(Ext.getCmp('TxtCodeFaktur_AR').getValue(),Ext.getCmp('TxtTgl_voucher_Faktur_AR').getValue(),Ext.getCmp('TxtTgl_voucher_Faktur_AR2').getValue());
                    ShowPesanWarningFaktur_AR('Proses Simpan Tidak berhasil silahkan hubungi admin', 'Gagal');
                },
                success: function (o){
                    load_Faktur_AR(Ext.getCmp('TxtCodeFaktur_AR').getValue(),Ext.getCmp('TxtTgl_voucher_Faktur_AR').getValue(),Ext.getCmp('TxtTgl_voucher_Faktur_AR2').getValue());
                    var cst = Ext.decode(o.responseText);
                    if (cst.success === true){
                        ShowPesanInfoFaktur_AR('Proses Posting Berhasil', 'Posting');
                           Ext.getCmp('btnSimpan_Faktur_AR').setDisabled(true);
                           Ext.getCmp('btnposting_Faktur_AR').setDisabled(true);
                           Ext.getCmp('btnSimpanpost_Faktur_AR').setDisabled(true);
                            FormLook_Posting_Faktur_AR.destroy();
                         load_Faktur_AR_detail(Ext.getCmp('TxtVoucherFaktur_AR').getValue());                          
                        
                    }
                     else{
                        if (cst.cari === true){
                        loadMask.hide();
                        ShowPesanWarningFaktur_AR('Login Ulang User Tidak teridentifikasi', 'User');   
                        }else{
                        ShowPesanWarningFaktur_AR('Proses Simpan Tidak berhasil silahkan hubungi admin', 'Gagal');
                        }
                    };
                }
            }
        );
//  }
};



function PanelFaktur_AR_posting() {
    var items = new Ext.Panel
            (
                    {
                        title: '',
                        id: 'PanelDataPosting_Faktur_AR',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        bodyStyle: 'padding:0px 0px 0px 0px',
                        border: true,
                        autoscroll: true,
                        items: [
                            {
                                layout: 'absolute',
                                bodyStyle: 'padding: 5px 5px 5px 5px',
                                border: false,
                                width: 400,
                                height: 240,
                                anchor: '100% 100%',
                                items: [
                                    {
                                        x: 10,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'No Voucher '
                                    },
                                    {
                                        x: 110,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 10,
                                        xtype: 'textfield',
                                        name: 'Txt_post_VoucherFaktur_AR',
                                        id: 'Txt_post_VoucherFaktur_AR',
                                        width: 200,
                                        readOnly: false,
                                        disabled:true,
                        
                                    },
                                    {
                                        x: 10,
                                        y: 40,
                                        xtype: 'label',
                                        text: 'Tanggal '
                                    },
                                    {
                                        x: 110,
                                        y: 40,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 40,
                                        xtype: 'datefield',
                                        name: 'Txt_post_tanggal_Faktur_AR',
                                        id: 'Txt_post_tanggal_Faktur_AR',
                                        format: 'Y-m-d',
                                        value:now,
                                        disabled:true,
                                        width: 200,
                                    },
                                    {
                                        x: 10,
                                        y: 70,
                                        xtype: 'label',
                                        text: 'Catatan '
                                    },
                                    {
                                        x: 110,
                                        y: 70,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 70,
                                        xtype: 'textarea',
                                        name: 'Txt_Post_catatan_Faktur_AR',
                                        id: 'Txt_Post_catatan_Faktur_AR',
                                        width: 200,
                                        height : 50,
                                       // readOnly: false,
                                        //disabled:true,
                                    },
                                    {
                                        x: 10,
                                        y: 130,
                                        xtype: 'label',
                                        text: 'Total '
                                    },
                                    {
                                        x: 110,
                                        y: 130,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 130,
                                        xtype: 'textfield',
                                        name: 'Txt_post_total_Faktur_AR',
                                        id: 'Txt_post_total_Faktur_AR',
                                        style: 'text-align: right',
                                        width: 200,
                                        readOnly: false,
                                        disabled:true,
                                    }
                                ]
                            }
                        ]
                    });
    return items;
};

function datainit_post_Faktur_AR(rowdata)
{//
    Ext.getCmp('Txt_post_VoucherFaktur_AR').setValue(Ext.getCmp('TxtVoucherFaktur_AR').getValue());
    Ext.getCmp('Txt_post_total_Faktur_AR').setValue(Ext.getCmp('Txttotal_jumlah_peneriman').getValue());
}

function PanelFaktur_AR() {

    var items = new Ext.Panel
            (
                    {
                        title: '',
                        id: 'PanelDataFaktur_AR',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        bodyStyle: 'padding:0px 0px 0px 0px',
                        border: true,
                        autoscroll: true,
                        items: [
                            {
                                layout: 'absolute',
                                bodyStyle: 'padding: 5px 5px 5px 5px',
                                border: false,
                                width: 500,
                                height: 200,
                                anchor: '100% 100%',
                                items: [
                                    {
                                        x: 10,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'No Faktur '
                                    },
                                    {
                                        x: 110,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 10,
                                        xtype: 'textfield',
                                        name: 'TxtVoucherFaktur_AR',
                                        id: 'TxtVoucherFaktur_AR',
                                        width: 280,
                                        readOnly : true
                        
                                    },
                                    {
                                        x: 410,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'Tanggal '
                                    },
                                    {
                                        x: 470,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 480,
                                        y: 10,
                                        xtype: 'datefield',
                                        name: 'Txt_tanggal_Faktur_AR',
                                        id: 'Txt_tanggal_Faktur_AR',
                                        format: 'd/M/Y',
                                        value:now,
                                        disabled:true,
                                        width: 100,
                                    },
                                    {
                                        x: 10,
                                        y: 40,
                                        xtype: 'label',
                                        text: 'Customer '
                                    },
                                    {
                                        x: 110,
                                        y: 40,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    mComboCustomer_Faktur_AR(),
                                    {
                                        x: 410,
                                        y: 40,
                                        xtype: 'label',
                                        text: 'Due Date '
                                    },
                                    {
                                        x: 470,
                                        y: 40,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 480,
                                        y: 40,
                                        xtype: 'datefield',
                                        name: 'dtPopuDue_DateFaktur_AR',
                                        id: 'dtPopuDue_DateFaktur_AR',
                                        format: 'd/M/Y',
                                        value:now,
                                        width: 100,
                                    },
                                    {
                                        x: 10,
                                        y: 70,
                                        xtype: 'label',
                                        text: 'Reference '
                                    },
                                    {
                                        x: 110,
                                        y: 70,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 70,
                                        xtype: 'textfield',
                                        name: 'TxtPopuReference_Faktur_AR',
                                        id: 'TxtPopuReference_Faktur_AR',
                                        width: 280,
                                    },
                                    {
                                        x: 10,
                                        y: 100,
                                        xtype: 'label',
                                        text: 'Notes'
                                    },
                                    {
                                        x: 110,
                                        y: 100,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 100,
                                        xtype: 'textarea',
                                        name: 'Txt_catatan_Faktur_AR',
                                        id: 'Txt_catatan_Faktur_AR',
                                        width: 460,
                                        tabIndex: 5,
                                        height : 90,
                                        readOnly: false
                                    }
                                ]
                            }
                        ]
                    });
    return items;
}
;
function datainit_Faktur_AR(rowdata)
{
    if (rowSelected_viFaktur_AR.data.posted==='true' || rowSelected_viFaktur_AR.data.posted==='t') {
        Ext.getCmp('btnSimpan_Faktur_AR').setDisabled(true);
        Ext.getCmp('btnposting_Faktur_AR').setDisabled(true);
        Ext.getCmp('btnImport_Faktur_AR').setDisabled(true);
        Ext.getCmp('btnhapus_baris_Faktur_AR').setDisabled(true);
    }else{
        Ext.getCmp('btnSimpan_Faktur_AR').setDisabled(false);
        Ext.getCmp('btnposting_Faktur_AR').setDisabled(false);
        Ext.getCmp('btnImport_Faktur_AR').setDisabled(false);
        Ext.getCmp('btnhapus_baris_Faktur_AR').setDisabled(false);
    }
    xxx=new Date(rowdata.data.date);
    yyy = new Date(rowdata.data.due_date)
    Ext.getCmp('TxtVoucherFaktur_AR').setValue(rowdata.data.arf_number);
    Ext.getCmp('cboCustomer_Faktur_AR').setValue(rowdata.data.customer);
    tmpnamaCUstomer = rowdata.data.customer;
    tmpkdCUstomer = rowdata.data.cust_code;
    Ext.getCmp('Txt_tanggal_Faktur_AR').setValue(xxx);
    Ext.getCmp('dtPopuDue_DateFaktur_AR').setValue(yyy);

    Ext.getCmp('Txt_catatan_Faktur_AR').setValue(rowdata.data.notes);
    Ext.getCmp('TxtPopuReference_Faktur_AR').setValue(rowdata.data.reff);
    load_Faktur_AR_detail(rowdata.data.arf_number);
    
}

function new_data_Faktur_AR()
{   
    Ext.getCmp('btnSimpan_Faktur_AR').setDisabled(false);
    Ext.getCmp('btnImport_Faktur_AR').setDisabled(false);
     Ext.getCmp('btnhapus_baris_Faktur_AR').setDisabled(false);
    Ext.getCmp('btnposting_Faktur_AR').setDisabled(false);
    Ext.getCmp('TxtVoucherFaktur_AR').setValue();
    Ext.getCmp('cboCustomer_Faktur_AR').setValue();
    tmpkdCUstomer = '';
    Ext.getCmp('dtPopuDue_DateFaktur_AR').setValue(now);
    Ext.getCmp('TxtPopuReference_Faktur_AR').setValue();
    Ext.getCmp('Txt_catatan_Faktur_AR').setValue('');
    ds_Faktur_AR_grid_detail.removeAll();
    grid_detail_Faktur_AR.getView().refresh();
    Ext.getCmp('Txttotal_jumlah_peneriman').setValue(0);
      
}

function SavingData_Faktur_AR()
{ 
if (ds_Faktur_AR_grid_detail.getCount()===0){
     ShowPesanWarningFaktur_AR('Isi kolom dan baris didalam tabel', 'Perhatian');
}else{      

    Ext.Ajax.request
        (
            {   
                url: baseURL + "index.php/anggaran/fakturAR/save",
                params: paramsaving_Faktur_AR(),
                failure: function (o)
                {
                    load_Faktur_AR(Ext.getCmp('TxtCodeFaktur_AR').getValue(),Ext.getCmp('TxtTgl_voucher_Faktur_AR').getValue(),Ext.getCmp('TxtTgl_voucher_Faktur_AR2').getValue());
                    ShowPesanWarningFaktur_AR('Proses Simpan Tidak berhasil silahkan hubungi admin', 'Gagal');
                },
                success: function (o)
                {
                    //creteria_Faktur_AR2();
                    load_Faktur_AR(Ext.getCmp('TxtCodeFaktur_AR').getValue(),Ext.getCmp('TxtTgl_voucher_Faktur_AR').getValue(),Ext.getCmp('TxtTgl_voucher_Faktur_AR2').getValue());
                
                    var cst = Ext.decode(o.responseText);
                    if (cst.success === true)
                    {
                        Ext.getCmp('TxtVoucherFaktur_AR').setValue(cst.arf_number);
                        load_Faktur_AR_detail(cst.arf_number);                         
                        ShowPesanInfoFaktur_AR('Proses Simpan Berhasil', 'Save');
                    }
                     
                    else
                    {
                        if (cst.cari === true){
                        loadMask.hide();
                        ShowPesanWarningFaktur_AR('Login Ulang User Tidak teridentifikasi', 'User');   
                        }else{
                        ShowPesanWarningFaktur_AR('Proses Simpan Tidak berhasil silahkan hubungi admin', 'Gagal');
                        }
                    };
                }
            }
        );    
    }
};
function paramsaving_Faktur_AR()
{
    var params =
            {
            arf_number  :Ext.getCmp('TxtVoucherFaktur_AR').getValue(),
            arf_date    :Ext.getCmp('Txt_tanggal_Faktur_AR').getValue(),
            cust_code   :tmpkdCUstomer,
            Paid        :0,
            Amount      :xz,
            Posted      :'false',
            Due_date    :Ext.getCmp('dtPopuDue_DateFaktur_AR').getValue(),
            Reff        :Ext.getCmp('TxtPopuReference_Faktur_AR').getValue(),
            note        :Ext.getCmp('Txt_catatan_Faktur_AR').getValue(),
            };
            params['jumlah']=ds_Faktur_AR_grid_detail.getCount()
            for (var L=0; L<ds_Faktur_AR_grid_detail.getCount(); L++)
            {
                params['kd_kasir'+L]             =ds_Faktur_AR_grid_detail.data.items[L].data.kd_kasir;
                params['no_transaksi'+L]         =ds_Faktur_AR_grid_detail.data.items[L].data.notrans;
                params['urut'+L]                 =ds_Faktur_AR_grid_detail.data.items[L].data.urut;
                params['jumlah'+L]               =ds_Faktur_AR_grid_detail.data.items[L].data.jumlah;
            }
   
   return params;
                                
}


function load_Faktur_AR(cso_number,tgl,tgl2){
        Ext.Ajax.request({
            url: baseURL + "index.php/anggaran/fakturAR/select",
            params: {
                arf_number:cso_number,
                tgl:tgl,
                tgl2:tgl2,
                posting:selectCount_PostingFaktur_AR
            },
            failure: function(o){   
                loadMask.hide();
                ShowPesanWarningFaktur_AR('Hubungi Admin', 'Error');
            },  
            success: function(o){
            var cst = Ext.decode(o.responseText);
            if (cst.success === true){
                dataSource_Faktur_AR.removeAll();
                var recs=[],
                recType=dataSource_Faktur_AR.recordType;
                for(var i=0; i<cst.ListDataObj.length; i++){
                    recs.push(new recType(cst.ListDataObj[i]));
                }
                dataSource_Faktur_AR.add(recs);
                gridListHasilFaktur_AR.getView().refresh();
            } 
            }
        })
}



function ShowPesanWarningFaktur_AR(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING,
                        width: 250
                    }
            );
}
;

function ShowPesanInfoFaktur_AR(str, modul) {
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO,
                        width: 250
                    }
            );
}
;

function DeleteData_Faktur_AR(line)
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/anggaran/fakturAR/delete",
                        params: paramdelete(line),
                        failure: function (o){
                            //creteria_Faktur_AR2();
                            load_Faktur_AR(Ext.getCmp('TxtCodeFaktur_AR').getValue(),Ext.getCmp('TxtTgl_voucher_Faktur_AR').getValue(),Ext.getCmp('TxtTgl_voucher_Faktur_AR2').getValue());
                            loadMask.hide();
                            ShowPesanWarningFaktur_AR('Hubungi Admin', 'Error');
                        },
                        success: function (o)
                        {
                            load_Faktur_AR(Ext.getCmp('TxtCodeFaktur_AR').getValue(),Ext.getCmp('TxtTgl_voucher_Faktur_AR').getValue(),Ext.getCmp('TxtTgl_voucher_Faktur_AR2').getValue());
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true){
                                
                                loadMask.hide();
                                ShowPesanInfoFaktur_AR('Data berhasil di hapus', 'Information');
                                 ds_Faktur_AR_grid_detail.removeAt(line);
                                 grid_detail_Faktur_AR.getView().refresh(); 
                            }
                            else{
                                 if (cst.tidak_ada_line === true){                                
                                 ds_Faktur_AR_grid_detail.removeAt(line);
                                 grid_detail_Faktur_AR.getView().refresh(); 
                                }else{
                                loadMask.hide();
                                ShowPesanWarningFaktur_AR('Gagal menghapus data', 'Error');
                                }
                            }
                            ;
                        }
                    }

            );
}
;
function paramdelete(linex)
{
    var params =
            {
                arf_number  :Ext.getCmp('TxtVoucherFaktur_AR').getValue(),
                no_transaksi : ds_Faktur_AR_grid_detail.getRange()[linex].data.notrans,
                kd_kasir : ds_Faktur_AR_grid_detail.getRange()[linex].data.kd_kasir,
                urut :  ds_Faktur_AR_grid_detail.getRange()[linex].data.urut,
            };
    return params;
}

function Getgrid_detail_Faktur_AR(data) {

    var fldDetail = ['notrans', 'kd_pasien', 'nama',  'unit', 'kd_kasir','urut','jumlah'];
    ds_Faktur_AR_grid_detail = new WebApp.DataStore({fields: fldDetail});
    grid_detail_Faktur_AR = new Ext.grid.EditorGridPanel({
        title: 'Detail Faktur AR',
        id: 'grid_detail_Faktur_AR',
        stripeRows: true,
        store: ds_Faktur_AR_grid_detail,
        border: false,
        frame: false,
        height: 325,
        width: 815,
        anchor: '100%',
        autoScroll: true,
        sm: new Ext.grid.CellSelectionModel({
            singleSelect: true,
            listeners: {
                cellselect: function (sm, row, rec) {

                }
            }
        }),
        cm: get_column_detail_Faktur_AR(),
        tbar:[
        {
                text: 'Import',
                id: 'btnImport_Faktur_AR',
                tooltip: nmLookup,
                iconCls: 'AddRow',
                handler: function () {
                    var tmpcustomer = Ext.getCmp('cboCustomer_Faktur_AR').getValue();
                    if (tmpcustomer === '') {
                         ShowPesanWarningFaktur_AR('Harap Pilih customer dahulu.', 'Gagal');
                    }else{
                        ImportDataFakturLookUp();
                    }                    
                }
        },{
                text: 'Hapus data',
                id: 'btnhapus_baris_Faktur_AR',
                tooltip: nmLookup,
                iconCls: 'RemoveRow',
                handler: function () {
                    if (grid_detail_Faktur_AR.getSelectionModel().selection === 'null' || grid_detail_Faktur_AR.getSelectionModel().selection === null) {
                        ShowPesanWarningFaktur_AR('Harap Pilih Data Yang Akan Di Hapus Terlebih Dahulu', 'Warning');
                    }else{
                        var line = grid_detail_Faktur_AR.getSelectionModel().selection.cell[0];
                        if (grid_detail_Faktur_AR.getSelectionModel().selection == null) {
                            ShowPesanWarningFaktur_AR('Harap Pilih terlebih dahulu data.', 'Gagal');
                        } else {
                            Ext.Msg.show({
                                title: nmHapusBaris,
                                msg: 'Anda yakin akan menghapus data dengan Nama' + ' : ' + ds_Faktur_AR_grid_detail.getRange()[line].data.nama,
                                buttons: Ext.MessageBox.YESNO,
                                fn: function (btn) {
                                    if (btn == 'yes') {
                                        DeleteData_Faktur_AR(line);
                                    }
                                },
                                icon: Ext.MessageBox.QUESTION
                            });
                        }
                    }
                    
                }
        }           
        
        ],
        bbar:[
                    {
                        xtype: 'tbspacer',
                        width: 580
                    },
                     {
                
                        xtype: 'label',
                        text: 'Total : '
                    },
                   
                    {
                        xtype: 'textfield',
                        name: 'Txttotal_jumlah_peneriman',
                        id: 'Txttotal_jumlah_peneriman',
                        width: 150,
                        readOnly:true,
                        style: 'text-align: right',
                        value: 0,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {

                                        }

                                    }
                                }
                    }
        
        ],
		viewConfig: {forceFit: true}
    });

    return grid_detail_Faktur_AR;
}
function get_column_detail_Faktur_AR() {
  return new Ext.grid.ColumnModel([
        new Ext.grid.RowNumberer(),
        
        {
            id: Nci.getId(),
            header: 'No Transaksi',
            width: 80,
            hidden: false,
            menuDisabled: true,
            dataIndex: 'notrans'
        }, 
        {
            id: Nci.getId(),
            header: 'No Medrec',
            dataIndex: 'kd_pasien',
            sortable: true,
            width: 80,
            align: 'center',
        }, 
        {
            id: Nci.getId(),
            header: 'Nama Pasien',
            dataIndex: 'nama',
            width: 300,
            menuDisabled: true,
            hidden: false
        },
        {
            id: Nci.getId(),
            header: 'Nama Unit',
            dataIndex: 'unit',
            width: 100,
            menuDisabled: true,
            hidden: false
        },
        {
            id: Nci.getId(),
            header: 'Nilai (Rp)',
            dataIndex: 'jumlah',
            value:1,
            hidden: false,
            menuDisabled: true,
            width: 150,
            align:'right',
            editor: new Ext.form.NumberField ({
                        allowBlank: false
                    }),
            renderer: function(v, params, record)
            {
            total();
            return formatCurrency(record.data.jumlah);
            }
        }
    ]);
}
function total(){
 xz=0;
for (var L=0; L<ds_Faktur_AR_grid_detail.getCount(); L++)
            {
                if (ds_Faktur_AR_grid_detail.data.items[L].data.jumlah==="" ||
                 ds_Faktur_AR_grid_detail.data.items[L].data.jumlah==undefined||
                 ds_Faktur_AR_grid_detail.data.items[L].data.jumlah==="NaN" )
                {
                    ds_Faktur_AR_grid_detail.data.items[L].data.value=0;
                    xz=parseInt(xz)+parseInt(ds_Faktur_AR_grid_detail.data.items[L].data.jumlah);
                }else{
                    
                    xz=parseInt(xz)+parseInt(ds_Faktur_AR_grid_detail.data.items[L].data.jumlah);
                }
            }
            Ext.getCmp('Txttotal_jumlah_peneriman').setValue(formatCurrency(xz));
}

function load_Faktur_AR_detail(criteria){
        Ext.Ajax.request({
            url: baseURL + "index.php/anggaran/fakturAR/select_detail",
            params: {criteria:criteria},
            failure: function(o){   
                loadMask.hide();
                ShowPesanWarningFaktur_AR('Hubungi Admin', 'Error');
            },  
            success: function(o){
            var cst = Ext.decode(o.responseText);
            if (cst.success === true){
                ds_Faktur_AR_grid_detail.removeAll();
                var recs=[],
                recType=ds_Faktur_AR_grid_detail.recordType;
                for(var i=0; i<cst.ListDataObj.length; i++){
                    recs.push(new recType(cst.ListDataObj[i]));
                }
                ds_Faktur_AR_grid_detail.add(recs);
                grid_detail_Faktur_AR.getView().refresh();
            } 
            }
        })
}
var selectCount_PostingFaktur_AR = 'Semua';
function mComboStatus_PostingFaktur_AR()
{
    var cboStatus_PostingFaktur_AR = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 70,
                        id: 'cboStatus_PostingFaktur_AR',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        width: 150,
                        emptyText: '',
                        fieldLabel: 'Jenis',
                        store: new Ext.data.ArrayStore
                                (
                                        {
                                            id: 0,
                                            fields:
                                                    [
                                                        'Id',
                                                        'displayText'
                                                    ],
                                            data: [[1, 'Semua'], [2, 'Posting'], [3, 'Belum Posting']]
                                        }
                                ),
                        valueField: 'Id',
                        displayField: 'displayText',
                        value: selectCount_PostingFaktur_AR,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectCount_PostingFaktur_AR = b.data.displayText;
                                        load_Faktur_AR(Ext.getCmp('TxtCodeFaktur_AR').getValue(),Ext.getCmp('TxtTgl_voucher_Faktur_AR').getValue(),Ext.getCmp('TxtTgl_voucher_Faktur_AR2').getValue());
                                    }
                                }
                    }
            );
    return cboStatus_PostingFaktur_AR;
}
;
var tmpkdCUstomer;
var tmpnamaCUstomer;
function mComboCustomer_Faktur_AR()
{
    var Field = ['KODE', 'NAMA'];

    dsCustomer_Faktur_AR = new WebApp.DataStore({fields: Field});
    dsCustomer_Faktur_AR.load
        (
            {
            params:
                    {
                        Skip: 0,
                        Take: 1000,
                        Sort: 'pay_code',
                        Sortdir: 'ASC',
                        target: 'ViewComboCustomer_Keuangan',
                        param: ""
                    }
            }
        );

    var cboCustomer_Faktur_AR = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 40,
                        id: 'cboCustomer_Faktur_AR',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        emptyText: '',
                        align: 'Right',
                        editable: false,
                        width: 280,
                        // listWidth: 180,
                        store: dsCustomer_Faktur_AR,
                        valueField: 'KODE',
                        displayField: 'NAMA',
                        selectOnFocus: true,
                        enableKeyEvents:true,
                        tabIndex: 3,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        tmpkdCUstomer = b.data.KODE;
                                        tmpnamaCUstomer = b.data.NAMA;
                                        // penambahan tanggal jatuh tempo bayar (Due_date) berdasarkan jenis Customer
                                        getduedatecustomer(b.data.KODE);
                                    },
                                    keypress:function(c,e){
                                        if (e.keyCode == 13){

                                        } else if (e.keyCode == 9){

                                        }
                                    },

                                }
                    }
            );

    return cboCustomer_Faktur_AR;
}
;

var FormLookUpImportDataFaktur;
function ImportDataFakturLookUp() {
    FormLookUpImportDataFaktur = new Ext.Window({
        id: 'WindowFormLookUpImportDataFaktur',
        title: 'Import Data',
        closeAction: 'destroy',
        width: 700,
        height: 500,
        border: false,
        resizable: false,
        plain: true,
        layout: 'fit',
        constrain: true,
        iconCls: 'Request',
        modal: true,
        items: [
				form_ImportDataFaktur(),
				],
        listeners: {
        }
    });
    FormLookUpImportDataFaktur.show();
    Ext.getCmp('TxtCustomerImport').setValue(tmpnamaCUstomer);

    

}
;

function form_ImportDataFaktur() {
    var Isi_form_ImportDataFaktur = new Ext.Panel
            (
                    {
                        id: 'form_ImportDataFaktur',
                        closable: true,
                        region: 'center',
                        layout: 'form',
                        itemCls: 'blacklabel',
                        bodyStyle: 'padding: 0px 0px 0px 0px',
                        border: true,
                        shadhow: true,
                        width: 300,
						height: 200,
						iconCls: '',
                        items:
                                [
                                    Panel_Import_Data_Faktur_AR(),
									Getgrid_detail_Import_Data_Faktur_AR()
                                ],
                        tbar:
                                {
                                    xtype: 'toolbar',
                                    items:
                                            [
                                                {
                                                    xtype: 'button',
                                                    text: 'Import',
                                                    iconCls: 'save',
                                                    id: 'btnSimpan_penerimaan',
                                                    handler: function ()
                                                    {
                                                        var mRecordRad = Ext.data.Record.create([
                                                           {name: 'notrans', mapping:'notrans'},
                                                           {name: 'kd_pasien', mapping:'kd_pasien'},
                                                           {name: 'nama', mapping:'nama'},
                                                           {name: 'unit', mapping:'unit'}
                                                        ]);
                                                        // ds_Faktur_AR_grid_detail.removeAll();
                                                        var p;
                                                        var x = 1;
                                                        var records = [];
                                                        recType=ds_Faktur_AR_grid_detail.recordType;
                                                        for(var i = 0 ; i < ds_grid_detail_Import_Data_Faktur_AR.getCount();i++){
                                                            if (ds_grid_detail_Import_Data_Faktur_AR.data.items[i].data.tag === 'true' || ds_grid_detail_Import_Data_Faktur_AR.data.items[i].data.tag === true){
                                                               records.push(new recType({
                                                                                    notrans: ds_grid_detail_Import_Data_Faktur_AR.data.items[i].data.no_transaksi,
                                                                                    kd_pasien:ds_grid_detail_Import_Data_Faktur_AR.data.items[i].data.kd_pasien,
                                                                                    nama:ds_grid_detail_Import_Data_Faktur_AR.data.items[i].data.nama,
                                                                                    unit:ds_grid_detail_Import_Data_Faktur_AR.data.items[i].data.nama_unit, 
                                                                                    jumlah:ds_grid_detail_Import_Data_Faktur_AR.data.items[i].data.harga,
                                                                                    kd_kasir:ds_grid_detail_Import_Data_Faktur_AR.data.items[i].data.kd_kasir,
                                                                                    urut:x,
                                                                               }))
                                                            }else{
                                                                // ShowPesanInfoFaktur_AR('Belum Ada Data Yang Di Pilih', 'Peringatan');
                                                            }
                                                            x++;
                                                        }
                                                        ds_Faktur_AR_grid_detail.add(records);
                                                        grid_detail_Faktur_AR.getView().refresh();
                                                        FormLookUpImportDataFaktur.destroy();                                                        
                                                    }
                                                },
                                                {
                                                    xtype: 'button',
                                                    text: 'Batal',
                                                    iconCls: 'close',
                                                    id: 'btnBatal_penerimaan',
                                                    handler: function ()
                                                    {
														FormLookUpImportDataFaktur.destroy();												
                                                    }
                                                },
                                                 
                                            ]
                                }
                    }
            );
    return Isi_form_ImportDataFaktur;
}
;

function Panel_Import_Data_Faktur_AR() {
    
	var items = new Ext.Panel
            (
                    {
                        title: '',
                        id: 'PanelDatapenerimaan',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        bodyStyle: 'padding:0px 0px 0px 0px',
                        border: true,
                        autoscroll: true,
                        items: [
                            {
                                layout: 'absolute',
                                bodyStyle: 'padding: 5px 5px 5px 5px',
                                border: false,
                                width: 200,
                                height: 150,
                                anchor: '100% 100%',
                                items: [
                                    {
                                        x: 10,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'Customer '
                                    },
                                    {
                                        x: 110,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 10,
                                        xtype: 'textfield',
                                        name: 'TxtCustomerImport',
                                        id: 'TxtCustomerImport',
                                        width: 180,
                                        readOnly: false,
										disabled:true,
                        			},
                        			{
                                        x: 10,
                                        y: 40,
                                        xtype: 'label',
                                        text: 'Unit '
                                    },
                                    {
                                        x: 110,
                                        y: 40,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    mComboUnitPosting_Faktur_AR(),
                        			{
                                        x: 10,
                                        y: 70,
                                        xtype: 'label',
                                        text: 'Tanggal '
                                    },
                                    {
                                        x: 110,
                                        y: 70,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 70,
                                        xtype: 'datefield',
                                        name: 'TxtTgl_Awal_Posting_Faktur_AR',
                                        id: 'TxtTgl_Awal_Posting_Faktur_AR',
                                        format:'d/M/Y',
                                        width: 100,
                                        value :now,
                                        listeners:
                                                {
                                                    'specialkey': function ()
                                                    {
                                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                                        {

                                                        }
                                                    }
                                                }
                                    },
                                    {
                                        x: 230,
                                        y: 75,
                                        xtype: 'label',
                                        text: 's/d '
                                    },
                                    {
                                        x: 250,
                                        y: 70,
                                        xtype: 'datefield',
                                        name: 'TxtTgl_Akhir_Posting_Faktur_AR',
                                        id: 'TxtTgl_Akhir_Posting_Faktur_AR',
                                        format:'d/M/Y',
                                        width: 100,
                                        value :now,
                                        listeners:
                                                {
                                                    'specialkey': function ()
                                                    {
                                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                                        {

                                                        }
                                                    }
                                                }
                                    },
                                    {
                                        x: 10,
                                        y: 100,
                                        xtype: 'label',
                                        text: 'Tanggal '
                                    },
                                    {
                                        x: 110,
                                        y: 100,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 100,
                                        xtype: 'checkboxgroup',
                                        id: 'cgShift',
                                        itemCls: 'x-check-group-alt',
                                        columns: 4,
                                        boxMaxWidth: 400,
                                        items: [
                                                    {boxLabel: 'Semua',checked:true,name: 'Shift_All',id : 'Shift_All',handler: function (field, value) {if (value === true){Ext.getCmp('Shift_1').setValue(true);Ext.getCmp('Shift_2').setValue(true);Ext.getCmp('Shift_3').setValue(true);Ext.getCmp('Shift_1').disable();Ext.getCmp('Shift_2').disable();Ext.getCmp('Shift_3').disable();}else{Ext.getCmp('Shift_1').setValue(false);Ext.getCmp('Shift_2').setValue(false);Ext.getCmp('Shift_3').setValue(false);Ext.getCmp('Shift_1').enable();Ext.getCmp('Shift_2').enable();Ext.getCmp('Shift_3').enable();}}},
                                                    {boxLabel: 'Shift 1',checked:true,disabled:true,name: 'Shift_1',id : 'Shift_1'},
                                                    {boxLabel: 'Shift 2',checked:true,disabled:true,name: 'Shift_2',id : 'Shift_2'},
                                                    {boxLabel: 'Shift 3',checked:true,disabled:true,name: 'Shift_3',id : 'Shift_3'}
                                                ]
                                    },
                                    {
                                        x: 395,
                                        y: 125,
                                        xtype: 'button',
                                        text : 'Cari',
                                        iconCls: 'refresh',
                                        name: 'BtnCariDataImport',
                                        id: 'BtnCariDataImport',
                                        width: 80,
                                        handler: function ()
                                        {
                                            if (Ext.getCmp('cboUnitPosting_Faktur_AR').getValue()=== '') {
                                                ShowPesanInfoFaktur_AR('Silahkan Pilih Unit Terlebih Dahulu', 'Peringatan');
                                                Ext.getCmp('cboUnitPosting_Faktur_AR').focus();
                                            }else{
                                                refeshHasilImporFAR();
                                            }
                                            
                                        }
                                    },
								]
                            }
                        ]
                    });
    return items;
}
;
var ds_grid_detail_Import_Data_Faktur_AR;
var grid_detail_Import_Data_Faktur_AR;
function Getgrid_detail_Import_Data_Faktur_AR(data) {

    var fldDetail = ['tag','no_transaksi', 'tgl_transaksi', 'shift',  'kd_unit', 'kd_pay',
                    'uraian', 'harga', 'nama',  'kd_kasir', 'co_status', 'kd_pasien','nama_unit'];
    ds_grid_detail_Import_Data_Faktur_AR = new WebApp.DataStore({fields: fldDetail});
    grid_detail_Import_Data_Faktur_AR = new Ext.grid.EditorGridPanel({
        title: 'Penerimaan',
        id: 'grid_detail_penerimaan',
        stripeRows: true,
        store: ds_grid_detail_Import_Data_Faktur_AR,
        border: false,
        frame: false,
        height: 325,
		width: 815,
        anchor: '100%',
        autoScroll: true,
        sm: new Ext.grid.CellSelectionModel({
            singleSelect: true,
            listeners: {
                cellselect: function (sm, row, rec) {

                }
            }
        }),
        cm: get_column_detail_Import_Faktur_AR(),
       viewConfig: {forceFit: true}
    });

    return grid_detail_Import_Data_Faktur_AR;
}

function get_column_detail_Import_Faktur_AR() {
  return new Ext.grid.ColumnModel([
        new Ext.grid.RowNumberer(),
		{
            id: 'coltagImport',
            header: 'Tag',
            dataIndex: 'tag',
            width:10,
            xtype:'checkcolumn'
        },
		{
            id: 'colNotransImport',
            header: 'No. Transaksi',
            dataIndex: 'no_transaksi',
            width: 80,
            hidden: false,
            menuDisabled: true
        }, {
            id: 'colkd_pasienImport',
            header: 'No. Medrec',
            dataIndex: 'kd_pasien',
            sortable: true,
            width: 150,
            align: 'center',
        }, {
            id: 'colNamaImport',
            header: 'Nama',
            dataIndex: 'nama',
            width: 150,
            menuDisabled: true,
            hidden: false
		},{
            id: 'colTanggalImport',
            header: 'Tanggal',
            dataIndex: 'tgl_transaksi',
			value:1,
            hidden: false,
            menuDisabled: true,
            width: 100,
            renderer: function(v, params, record){
                return ShowDate(record.data.tgl_transaksi);
            }
        },{	
			id: 'colShiftImport',
            header: 'Shift',
            dataIndex: 'shift',
            width: 130,
            menuDisabled: true,
           
        },{
            id: 'colJumlahImport',
            header: 'Jumlah',
            dataIndex: 'harga',
            menuDisabled: true,
            hidden: false,
            width: 150,
            align:'right',
            renderer: function(v, params, record)
            {
                return formatCurrency(record.data.harga);
            }
        },
    ]);
}

function mComboUnitPosting_Faktur_AR()
{
    var Field = ['KODE', 'NAMA'];

    dsUnitPosting_Faktur_AR = new WebApp.DataStore({fields: Field});
    dsUnitPosting_Faktur_AR.load
        (
            {
            params:
                    {
                        Skip: 0,
                        Take: 1000,
                        Sort: 'pay_code',
                        Sortdir: 'ASC',
                        target: 'ViewComboUnitImportData',
                        param: ""
                    }
            }
        );

    var cboUnitPosting_Faktur_AR = new Ext.form.ComboBox
            (
                    {
                        x: 120,
                        y: 40,
                        id: 'cboUnitPosting_Faktur_AR',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        emptyText: '',
                        align: 'Right',
                        editable: false,
                        width: 180,
                        store: dsUnitPosting_Faktur_AR,
                        valueField: 'KODE',
                        displayField: 'NAMA',
                        selectOnFocus: true,
                        enableKeyEvents:true,
                        tabIndex: 3,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        // tmpkdCUstomer = b.data.KODE;
                                        // Ext.getCmp('TxtPopunama_type_Faktur_AR').setValue(b.data.NAMA);
                                        // Ext.getCmp('TxtPopukode_xpayFaktur_AR').focus();
                                    },
                                    keypress:function(c,e){
                                        if (e.keyCode == 13){
                                            // Ext.getCmp('TxtPopukode_xpayFaktur_AR').focus();
                                        } else if (e.keyCode == 9){
                                             // Ext.getCmp('TxtPopukode_xpayFaktur_AR').focus();
                                        }
                                    },

                                }
                    }
            );

    return cboUnitPosting_Faktur_AR;
}
;

function refeshHasilImporFAR() {
    var tmpshift = '';
    var tmpshift2 = '';
    if (Ext.getCmp('Shift_All').getValue() != true) {
        if (Ext.getCmp('Shift_1').getValue() === true) {
            tmpshift = '1';
       }
       if (Ext.getCmp('Shift_2').getValue() === true) {
            if (tmpshift != '') {
                tmpshift += ',2';
            }else{
                tmpshift = '2';
            }            
       }
       if (Ext.getCmp('Shift_3').getValue() === true) {
            if (tmpshift != '') {
                tmpshift += ',3';
            }else{
                tmpshift = ',3';
            }
       }
       if (Ext.getCmp('Shift_1').getValue() === true && Ext.getCmp('Shift_2').getValue() === true && Ext.getCmp('Shift_3').getValue() === true) {
            tmpshift2 = '4';
       }else{
           tmpshift2 = ''; 
       }    
    }else{
        tmpshift = '1,2,3';
        tmpshift2 = '4';
    }
    Ext.Ajax.request({
        url: baseURL + "index.php/anggaran/fakturAR/select_transaksi_detail",
        params: {
            kd_unit:Ext.getCmp('cboUnitPosting_Faktur_AR').getValue(),
            tgl:Ext.getCmp('TxtTgl_Awal_Posting_Faktur_AR').getValue(),
            tgl2:Ext.getCmp('TxtTgl_Akhir_Posting_Faktur_AR').getValue(),
            customer:tmpkdCUstomer,
            Shift:tmpshift,
            Shift2:tmpshift2
        },
        failure: function(o){   
            loadMask.hide();
            ShowPesanWarningFaktur_AR('Hubungi Admin', 'Error');
        },  
        success: function(o){
        var cst = Ext.decode(o.responseText);
        if (cst.success === true){
            if (cst.ListDataObj.length === 0) {
                ShowPesanInfoFaktur_AR('Tidak Ada Data', 'Info');
            }else{
                 ds_grid_detail_Import_Data_Faktur_AR.removeAll();
                var recs=[],
                recType=ds_grid_detail_Import_Data_Faktur_AR.recordType;
                for(var i=0; i<cst.ListDataObj.length; i++){
                    recs.push(new recType(cst.ListDataObj[i]));
                }
                ds_grid_detail_Import_Data_Faktur_AR.add(recs);
                grid_detail_Import_Data_Faktur_AR.getView().refresh();
            }
        }
    }
})
}

function getduedatecustomer(kode){
    Ext.Ajax.request
    (
        {
            url: baseURL + "index.php/anggaran/fakturAR/getduedatecustomer",
            params: {kodecustomer : kode},
            failure: function (o){
                ShowPesanWarningFaktur_AR('Hubungi Admin', 'Error');
            },
            success: function (o)
            {
                
                var cst = Ext.decode(o.responseText);
                if (cst.success === true){
                    var date = new Date();
                    // add a day
                    Ext.getCmp('dtPopuDue_DateFaktur_AR').setValue(date.getDate() + cst.due_date);
                }
            }
        }

    );
}


var FormLook_Print_Faktur_AR;
function Print_LookUp_Faktur_AR(rowdata) {
    xz=0;
    FormLook_Print_Faktur_AR = new Ext.Window({
        id: 'FormLook_Print_Faktur_AR',
        title: 'Print Faktur',
        closeAction: 'destroy',
        width: 300,
        height: 150,
        border: false,
        resizable: false,
        plain: true,
        layout: 'fit',
        constrain: true,
        iconCls: 'Request',
        modal: true,
        items: [
                PanelPrintFaktur_AR(),
                ],
        listeners: {
        },
        fbar:
            {
                xtype: 'toolbar',
                items:
                        [
                            {
                                xtype: 'button',
                                text: 'Print',
                                iconCls: '',
                                id: 'btnSimpanpost_Faktur_AR',
                                handler: function ()
                                {

                                    // console.log(Ext.getCmp('cbfaktur').checked);
                                    PrintData_Faktur_AR()
                                }
                            }
                        ]
            }
                    
    });
    FormLook_Print_Faktur_AR.show();
    // datainit_post_Faktur_AR(rowdata);    
};

function PanelPrintFaktur_AR() {
    var items = new Ext.Panel
            (
                    {
                        title: '',
                        id: 'PanelDataPrint_Faktur_AR',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        bodyStyle: 'padding:0px 0px 0px 0px',
                        border: true,
                        autoscroll: true,
                        items: [
                            {
                                xtype : 'fieldset',
                                title : 'Jenis Cetakan',
                                layout: 'absolute',
                                bodyStyle: 'padding: 5px 5px 5px 5px',
                                border: true,
                                anchor: '99% 99%',
                                items: [
                                    {
                                        x: 10,
                                        y: 10,
                                        xtype: 'checkboxgroup',
                                        id: 'cbgPilihanPrint',
                                        itemCls: 'x-check-group-alt',
                                        columns: 2,
                                        boxMaxWidth: 300,
                                        items: [
                                            {xtype: 'checkbox', boxLabel: 'Faktur', name: 'cbfaktur', id: 'cbfaktur',checked:true},
                                            {xtype: 'checkbox', boxLabel: 'Surat Tagihan', name: 'cbst', id: 'cbst',checked:true}
                                        ],
                                        listeners: {
                                            change: function (checkbox, newValue, oldValue, eOpts) {
                                            }
                                        }
                                    }
                                ]
                            }
                        ]
                    });
    return items;
};

function PrintData_Faktur_AR()
{   
    if(Ext.getCmp('cbfaktur').checked == false && Ext.getCmp('cbst').checked == false){
        ShowPesanWarningFaktur_AR('Jenis Cetakan Belum Ada Yang Di Pilih', 'Warning');
    }else{
        Ext.Ajax.request
        (
            {   
                url: baseURL + "index.php/anggaran/cetakfakturar/Cetak",
                params: paramPrint_Faktur_AR(),
                failure: function (o){
                    ShowPesanWarningFaktur_AR('Proses Cetak Tidak berhasil silahkan hubungi admin', 'Gagal');
                },
                success: function (o){
                    if (cst.success === true){
                        ShowPesanInfoFaktur_AR('Data Sedang Di Cetak, Harap Tunggu Sebentar', 'Cetak');                       
                        FormLook_Print_Faktur_AR.destroy();
                    }
                    else{
                        ShowPesanWarningFaktur_AR('Proses Cetak Tidak berhasil silahkan hubungi admin', 'Gagal');
                    };
                }
            }
        );
    }
    
        
};

function paramPrint_Faktur_AR()
{
    var tmpfaktur = '';
    var tmpsurat_tagihan = ''
    if (Ext.getCmp('cbfaktur').checked == true) {
        tmpfaktur = 'Faktur';
    }
    if(Ext.getCmp('cbst').checked == true){
        tmpsurat_tagihan = 'Tagihan';
    }
    var params =
            {
            Faktur          : tmpfaktur,
            surat_tagihan   : tmpsurat_tagihan,
            no_faktur       : Ext.getCmp('TxtVoucherFaktur_AR').getValue(),
            total           : Ext.getCmp('Txttotal_jumlah_peneriman').getValue(),
            };
   return params;
}

