/*var mRecordItem_OpenArForm = Ext.data.Record.create
(
	[
	   {name: 'ACCOUNT', mapping:'ACCOUNT'},
	   {name: 'NAMAACCOUNT', mapping:'NAMAACCOUNT'},
	   {name: 'Description', mapping:'Description'},
	   {name: 'Debit', mapping:'Debit'},
	   {name: 'Kredit', mapping:'Kredit'},
	   {name: 'Line', mapping:'Line'}
	]
);*/
var CurrentTRListItem_OpenArForm = 
{
    data: Object,
    details:Array, 
    row: 0
};

var CurrentSelected_OpenArForm = 
{
    data: Object,
    details:Array, 
    row: 0
};
var dsTRListImport_OpenArForm;
var gridDTLTR_OpenArForm;
var dsTRList_OpenArForm;
var GridListImport_OpenArForm;
var DataAddNew_OpenArForm = true;
var selectCount_OpenArForm=50;
var now_OpenArForm = new Date();
var rowSelected_OpenArForm;
var TRLookUps_OpenArForm;
var focusRef_OpenArForm;
var cellSelectedListItem_OpenArForm;
var dsCboEntryCustOpenArForm;
var selectCboEntryCustARForm;
var NamaForm_OpenArForm="Faktur AR";
var StrAccCust_OpenArForm;
var StrNmAccCust_OpenArForm;
var StrDue_Day;
var StrApp_OpenArForm=true;
var selectAktivaLancar_OpenArForm;
var NewRow_OpenArForm=true;
var InitItem_OpenArForm=0;
var CurrentSelectedImport_OpenArForm=
{
    data: Object,
    details:Array, 
    row: 0
};


var dsTRListFaktur_OpenArForm;
var flddsTRListFaktur_OpenArForm =['NO_TRANSAKSI','TGL_TRANSAKSI','KD_KASIR','SHIFT','KD_USER']  
dsTRListFaktur_OpenArForm = new WebApp.DataStore({ fields: flddsTRListFaktur_OpenArForm })
var mRecordListFaktur_OpenArForm = Ext.data.Record.create
	(
		[
		   {name: 'NO_TRANSAKSI', mapping:'NO_TRANSAKSI'},
		   {name: 'TGL_TRANSAKSI', mapping:'TGL_TRANSAKSI'},
		   {name: 'KD_KASIR', mapping:'KD_KASIR'},
		   {name: 'SHIFT', mapping:'SHIFT'},
		   {name: 'KD_USER', mapping:'KD_USER'}
		]
	);

	var IsImport_OpenArForm=0;
	var chkAll=1;
	var chk1=0;
	var chk2=0;
	var chk3=0;

CurrentPage.page=getPanel_OpenArForm(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanel_OpenArForm(mod_id)
{
    var Field = 
	[
		'ARO_NUMBER','ARO_DATE','CUST_CODE','CUSTOMER','DUE_DATE','AMOUNT',
		'PAID','NOTES','NO_TAG','DATE_TAG','APPROVE','ACCOUNT','NAMAACCOUNT'
	]
    dsTRList_OpenArForm = new WebApp.DataStore({ fields: Field });
        
  
    var chkApprove_OpenArForm = new Ext.grid.CheckColumn
	(
		{
			id: 'chkApprove_OpenArForm',
			header: "APPROVE",
			align: 'center',
			disable:true,
			dataIndex: 'APPROVE',
			width: 70,
			renderer	: function(value, metaData, record, rowIndex, colIndex, store){
							 switch (value){
								 case 't':
									 metaData.css = 'StatusHijau'; 
									 break;
								 case 'f':
									 metaData.css = 'StatusMerah';
									 break;
							 }
							 return '';
						}
		}
	);	
  
    var grListTR_OpenArForm = new Ext.grid.EditorGridPanel
	(
		{
			stripeRows: true,
			xtype: 'editorgrid',
			store: dsTRList_OpenArForm,
			anchor: '100% 100%',
			columnLines:true,
			border:false,
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			//sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelected_OpenArForm = dsTRList_OpenArForm.getAt(row);
						}
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, row, rec)
				{
					rowSelected_OpenArForm = dsTRList_OpenArForm.getAt(row);
					
					CurrentSelected_OpenArForm.row = rec;
					CurrentSelected_OpenArForm.data = rowSelected_OpenArForm;

					if (rowSelected_OpenArForm != undefined)
						{
							//DataAddNew_OpenArForm=false;
							LookUpForm_OpenArForm(rowSelected_OpenArForm.data);
					}
					else
					{
							//DataAddNew_OpenArForm=true;
							LookUpForm_OpenArForm();
					}
				}
			},		
			colModel: new Ext.grid.ColumnModel
			//cm: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'ColARO_NUMBER',
						header: 'Nomor',
						dataIndex: 'ARO_NUMBER',
						sortable: true,			
						width :100,
						filter: {}
					}, 
					{
						//xtype: 'datecolumn',
						header: 'Tanggal',
						width: 100,
						sortable: true,
						dataIndex: 'ARO_DATE',
						id:'ColARO_DATE',
						renderer: function(v, params, record) 
						{
							return ShowDateAkuntansi(record.data.ARO_DATE);
						},
						filter: {}			
					}, 
					{						
						id: 'ColCUSTOMER',
						header: "Terima Dari",
						dataIndex: 'CUSTOMER',
						width :130,
						filter: {}
					},
					{
						id: 'ColNotes',
						header: "Keterangan",
						dataIndex: 'NOTES',
						width :200,
						filter: {}
					}, 
					{
						id: 'colAmount',
						header: "Jumlah (Rp)",
						align:'right',
						dataIndex: 'AMOUNT',
						renderer: function(v, params, record) 
						{
							return formatCurrencyDec(record.data.AMOUNT);
						},	
						width :80,
						filter: {}
					},
					{
						id: 'colpaid',
						header: "Bayar (Rp)",
						align:'right',
						dataIndex: 'PAID',
						renderer: function(v, params, record) 
						{
							return formatCurrencyDec(record.data.PAID);
						},	
						width :80,
						filter: {}
					},{
						header		: 'Approve',
						width		: 60,
						sortable	: false,
						hideable	: true,
						hidden		: false,
						menuDisabled: true,
						dataIndex	: 'APPROVE',
						id			: 'chkApprove_OpenArForm',
						renderer	: function(value, metaData, record, rowIndex, colIndex, store){
							 switch (value){
								 case 't':
									 metaData.css = 'StatusHijau'; 
									 break;
								 case 'f':
									 metaData.css = 'StatusMerah';
									 break;
							 }
							 return '';
						}
					}
					 //chkApprove_OpenArForm
				]
			),

			//plugins: chkApprove_OpenArForm,
			tbar: 
			[				
				{
					id: 'btnEdit_OpenArForm',
					text: 'Edit Data',
					tooltip: 'Edit Data',
					iconCls: 'Edit_Tr',
					handler: function(sm, row, rec) 
					{ 
						if (rowSelected_OpenArForm != undefined)
						{
							//DataAddNew_OpenArForm=false;
							LookUpForm_OpenArForm(rowSelected_OpenArForm.data);
							
							// ButtonDisabled_OpenArForm(rowSelected_OpenArForm.data.Type_Approve);
						}
						else
						{
							//DataAddNew_OpenArForm=true;
							LookUpForm_OpenArForm();
						}
					}
				},' ','-'
				,
				{
					xtype: 'checkbox',
					id: 'chkWithTgl_OpenArForm',					
					hideLabel:true,
					checked: true,
					handler: function() 
					{
						if (this.getValue()===true)
						{
							Ext.get('dtpTglAwalFilter_OpenArForm').dom.disabled=false;
							Ext.get('dtpTglAkhirFilter_OpenArForm').dom.disabled=false;
							Ext.get('dtpTglAwalFilter_OpenArForm').dom.readOnly=true;	
							Ext.get('dtpTglAkhirFilter_OpenArForm').dom.readOnly=true;
						}
						else
						{
							Ext.get('dtpTglAwalFilter_OpenArForm').dom.disabled=true;
							Ext.get('dtpTglAkhirFilter_OpenArForm').dom.disabled=true;							
						};
					}
				}
				, ' ','-','Tanggal : ', ' ',
				{
					xtype: 'datefield',
					fieldLabel: 'Dari Tanggal : ',
					id: 'dtpTglAwalFilter_OpenArForm',
					format: 'd/M/Y',
					value:now_OpenArForm,
					width:100,
					enableKeyEvents: true,
					listeners:{
						'specialkey': function (){
							if (Ext.EventObject.getKey() === 13){
								RefreshDataFilter_OpenArForm(false);
							}
						}
					},
					onInit: function() { }
				}, ' ', ' s/d ',' ', {
					xtype: 'datefield',
					fieldLabel: 'Sd /',
					id: 'dtpTglAkhirFilter_OpenArForm',
					format: 'd/M/Y',
					value:now_OpenArForm,
					width:100,
					enableKeyEvents: true,
					listeners:{
						'specialkey': function (){
							if (Ext.EventObject.getKey() === 13){
								RefreshDataFilter_OpenArForm(false);
							}
						}
					},
				},' ','->',
				{
					id: 'btnFind_OpenArForm',
					text: ' Find',
					tooltip: 'Find Record',
					iconCls: 'find',
					handler: function(sm, row, rec) 
					{ 
						fnFindDlg_OpenArForm();	
					}
				}
			],
			bbar:new WebApp.PaggingBar
			(
				{
					displayInfo: true,
					store: dsTRList_OpenArForm,
					pageSize: 50,
					displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
					emptyMsg: "Tidak ada record&nbsp;&nbsp;"		
				}
			),
			viewConfig: {forceFit: true} 			
		}
	);


	var FormTR_OpenArForm = new Ext.Panel
	(
		{
			id: mod_id,
			closable:true,
			region: 'center',
			layout: 'form',
			title: NamaForm_OpenArForm, 
			border: false,           
			shadhow: true,
			iconCls: 'Penerimaan',
			 margins:'0 5 5 0',
			items: [grListTR_OpenArForm],
			tbar: 
			[
				'Nomor : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: 'Nomor : ',
					id: 'txtNoFilter_OpenArForm',                   
					//anchor: '25%',
					width:120,
					listeners:
					{ 
						'specialkey' : function()
						{
							if (Ext.EventObject.getKey() === 13) 
							{
								RefreshDataFilter_OpenArForm(false);					
							} 						
						}
					},
					onInit: function() { }
				},
				{ xtype: 'tbseparator' },
				{
					xtype: 'checkbox',
					id: 'chkFilterApproved_OpenArForm',
					boxLabel: 'Approved'
				}, ' ','-',
					//'Maks.Data : ', 
					' ',mComboMaksData_OpenArForm(),
					' ','->',
				{					
					xtype: 'button',
					tooltip: 'Tampilkan',
					iconCls: 'refresh',
					text: 'Tampilkan',
					anchor: '25%',
					handler: function(sm, row, rec) 
					{
						RefreshDataFilter_OpenArForm(false);
					}
				}
			],
			listeners: 
			{ 
				'afterrender': function() 
				{           
					RefreshDataFilter_OpenArForm(true);
					//RefreshDataFilter_OpenArForm(true);				 
				}

			}
		}
	);
	return FormTR_OpenArForm

};
    // end function get panel main data
 
 //---------------------------------------------------------------------------------------///
   
   
function LookUpForm_OpenArForm(rowdata)
{
	var lebar=700;
	TRLookUps_OpenArForm = new Ext.Window
	(
		{
			id: 'LookUpForm_OpenArForm',
			title: NamaForm_OpenArForm,
			closeAction: 'destroy',
			width: lebar,
			height: 525,//230,//230, 
			// height: 430, 
			border: false,
			resizable:false,
			plain: true,
			layout: 'fit',
			iconCls: 'Penerimaan',
			modal: true,
			items: getFormEntryTR_OpenArForm(rowdata,lebar),
			listeners:
			{
				activate: function() 
				{
					
				},
				afterShow: function() 
				{ 
					this.activate(); 
				},
				deactivate: function()
				{
					rowSelected_OpenArForm=undefined;
					 RefreshDataFilter_OpenArForm(true);
				}
			}
		}
	);
	
	TRLookUps_OpenArForm.show();
	if (rowdata == undefined)
	{
		AddNew_OpenArForm();
	}
	else
	{
		DataInit_OpenArForm(rowdata)
	}	
	
};
   
function getFormEntryTR_OpenArForm(rowdata,lebar) 
{
	var pnlTR_OpenArForm = new Ext.FormPanel
	(
		{
			id: 'pnlTR_OpenArForm',
			fileUpload: true,
			region: 'north',
			layout: 'fit',
			bodyStyle: 'padding:10px 10px 10px 10px',			
			// anchor: '100%', 
			width:lebar - 10,
			height:220,
			border: false,			
			items: [getItemPanelInput_OpenArForm(rowdata,lebar)],
			tbar: 
			[
				{
					text: 'Tambah',
					id:'btnTambah',
					tooltip: 'Tambah Record Baru ',
					iconCls: 'add',
					handler: function() 
					{ 
						AddNew_OpenArForm();
						ButtonDisabled_OpenArForm(false);
					}
				}, '-', 
				{
					text: 'Simpan',
					id:'btnSimpan',
					tooltip: 'Rekam Data ',
					iconCls: 'save',
					handler: function() 
					{ 
						//alert(CekArrDetail_OpenArForm())
						//if (CekArrDetail_OpenArForm()==1){
							Datasave_OpenArForm(false);
							//alert('masuk')
							//alert(getArrDetail_OpenArForm());
							//RefreshDataFilter_OpenArForm(false);
						/* } else{
							ShowPesanWarning_OpenArForm('Item tidak boleh kosong','Simpan')
						} */
						
					}
				}, '-', 
				{
					text: 'Simpan & Keluar',
					id:'btnSimpanKeluar',
					tooltip: 'Rekam Data & Keluar ',
					iconCls: 'saveexit',
					handler: function() 
					{
						//if (CekArrDetail_OpenArForm()==1){
							Datasave_OpenArForm(false);
							RefreshDataFilter_OpenArForm(false);
							TRLookUps_OpenArForm.close();

						/* } else{
							ShowPesanWarning_OpenArForm('Item tidak boleh kosong','Simpan')
						} */
							 
						
					}
				}, '-', 
				{
					text: 'Hapus',
					id:'btnHapus',
					tooltip: 'Remove the selected item',
					iconCls: 'remove',
					handler: function() 
					{ 
						if (DataAddNew_OpenArForm==false){
							Ext.Msg.show
							(
								{
								   title:'Hapus',
								   msg: 'Apakah transaksi ini akan dihapus ?', 
								   buttons: Ext.MessageBox.YESNO,
								   fn: function (btn) 
								   {			
									   if (btn =='yes') 
										{
											DataDelete_OpenArForm();
											//RefreshDataFilter_OpenArForm(false);
										} 
								   },
								   icon: Ext.MessageBox.QUESTION
								}
							);

						}

					}
				}, '-',
				{
				    text: 'Approve',
					id:'btnApprove',
				    tooltip: 'Approve',
				    iconCls: 'approve',
				    handler: function() 
					{ 
						if (ValidasiApp_OpenArForm('Simpan Data',false) == 1 )
						{
							FormApprove(Ext.get('txtAmount_OpenArForm').getValue(),'11',Ext.get('txtNo_OpenArForm').getValue(),
							Ext.get('txtCatatan_OpenArForm').getValue(),Ext.get('dtpTanggal_OpenArForm').getValue())
							
							RefreshDataFilter_OpenArForm(false);
						}
					}
				}
				, '-', '->', '-',
				{
					xtype: 'splitbutton',
					text: 'Cetak',
					iconCls: 'print',
					id: 'btnCetak',
					handler: function (){
					},
					menu: new Ext.menu.Menu({
						items: [
							{
								xtype: 'button',
								text: 'Faktur',
								id: 'btnPrintFakturFakturAR',
								handler: function()
								{  
									Cetak_OpenArForm()
								}
							},
							{
								xtype: 'button',
								text: 'Surat Tagihan',
								id: 'btnPrintSuratTagihanFakturAR',
								handler: function()
								{
									Cetak_SuratTagihanARFaktur();
								}
							}
						]
					})
				}
			]
		}
	);  // end Head panel

	var TotalItem_OpenArForm = new Ext.Panel
	(
		{
			frame: false,
			layout: 'form',
			width: 650,
			border:false,
			id:'PnlTotalItem_OpenArForm',
			labelAlign:'right',
			labelWidth:50,
			style: 
			{
				'margin-top': '5.9px',
				'margin-left': '380px'
			},
			items: 
			[
				{
					xtype: 'compositefield',
					width: 650,
					items:
					[
						{
							xtype: 'button',
							text: 'Import Data',								
							id: 'btnImportdata_OpenArForm',
							handler: function()
							{								
								
								if (DataAddNew_OpenArForm==false){
									//alert('i'+ rowdata.CUST_CODE+' - '+rowdata.CUSTOMER)
									dsTRListImport_OpenArForm.removeAll();
									setFrmLookUpImportData_OpenArForm(rowdata.CUST_CODE,rowdata.CUSTOMER);
								} else{
									if (selectCboEntryCustARForm=='' || selectCboEntryCustARForm == undefined){
										ShowPesanWarning_OpenArForm('Customer belum di pilih','Import Data')
									}else{
										//alert('n'+ selectCboEntryCustARForm + ' - '+Ext.get('cboCustEntryARForm').getValue())
										dsTRListImport_OpenArForm.removeAll();
										setFrmLookUpImportData_OpenArForm(selectCboEntryCustARForm,Ext.get('cboCustEntryARForm').getValue());
									}
									
								}
								
							}
						},
						{
							xtype: 'displayfield',
							id:'dspfldTotalMHS_OpenArForm',
							name:'dspfldTotalMHS_OpenArForm',							
							value:'Total :',

						},
						{
							xtype: 'textfield',
							id:'txtTotalItem_OpenArForm',
							name:'txtTotalItem_OpenArForm',
							//fieldLabel: 'Total ',
							readOnly:true,
							style:
							{	
								'text-align':'right',
								'font-weight':'bold'
							},
							value:'0',
							width: 125
						}
					]
				}		
			]
		}
	);
	
	
	
	var TotalJurnal_OpenArForm = new Ext.Panel
	(
		{
			frame: false,
			layout: 'form',
			width: 650,
			border:false,
			id:'PnlTotalDtlMhs_OpenArForm',
			labelAlign:'right',
			labelWidth:50,
			style: 
			{
				'margin-top': '5.9px',
				'margin-left': '320px'//105
			},
			items: 
			[
				{
					xtype: 'compositefield',
					width: 650,
					items:
					[
				
						{
							xtype: 'displayfield',
							id:'dspfldTotalMHS_OpenArForm',
							name:'dspfldTotalMHS_OpenArForm',							
							value:'Total :',

						},						
						{
							xtype: 'textfield',
							id:'txtdebit_OpenArForm',
							name:'txtdebit_OpenArForm',
							fieldLabel: '',
							readOnly:true,
							style:
							{	
								'text-align':'right',
								'font-weight':'bold'
							},
							value:'0',
							width: 125
						},
						/*{
							xtype: 'displayfield',
							id:'dspfldTotalMHS2_OpenArForm',
							name:'dspfldTotalMHS2_OpenArForm',							
							value:'Kredit :',

						},*/						
						{
							xtype: 'textfield',
							id:'txtcredit_OpenArForm',
							name:'txtcredit_OpenArForm',
							fieldLabel: '',
							readOnly:true,
							style:
							{	
								'text-align':'right',
								'font-weight':'bold'
							},
							value:'0',
							width: 125
						}
					]
				}
			]
		}
	);

		var GDtabDetail_OpenArForm = new Ext.TabPanel
	(
		{
			region: 'center',
			id: 'GDtabDetail_OpenArForm',
			activeTab: 0,		
			anchor: '100% 100%',	
			//bodyStyle: 'padding:15px',		
			border: false,
			plain: true,
			defaults: 
			{
				autoScroll: false
			},
			items: 
			[
				{  	
					title: 'Item' ,
					id:'tabDtlItem_OpenArForm', 
					frame: false,					
					border:false,					
					items: 
					[
						GetDTLTRGridItem_OpenArForm(),
						TotalItem_OpenArForm
					],      
					listeners: 
					{
						activate: function()
						{		
							/*mActiveTab = "1";
							CalcTotalItem_OpenArForm();
							CalcTotalMHS_OpenArForm();
							Ext.getCmp('btnTambahBaris_OpenArForm').setVisible(false);
							Ext.getCmp('btnHapusBaris_OpenArForm').setVisible(false);*/

						}												
					},
					tbar:
					[
						{
							text: 'Tambah Baris',
							id:'btnTambahBaris_OpenArForm',
							tooltip: 'Tambah Record Baru ',
							iconCls: 'add',				
							handler: function() 
							{ 
								TambahBarisItem_OpenArForm();
								///NewRow_OpenArForm=true;

								//if (DataAddNew_OpenArForm == false){
									//Ext.getCmp('FieldcolItem_OpenArForm').enable();
								/*	//NewRow_OpenArForm=true;
								} else if (DataAddNew_OpenArForm == true){
									Ext.getCmp('FieldcolItem_OpenArForm').enable();
									//NewRow_OpenArForm=true;
								}
*/
								
							}
						}, '-', 
						{
							text: 'Hapus Baris',
							id:'btnHapusBaris_OpenArForm',
							tooltip: 'Remove the selected item',
							iconCls: 'remove',
							handler: function()
								{
									if (dsDTLTRListItem_OpenArForm.getCount() > 0 )
									{						
										if (cellSelectedListItem_OpenArForm != undefined)
										{
											//if(CurrentTRListItem_OpenArForm != undefined)
											//{
												HapusBaris_OpenArForm();
											//}
										}
										else
										{
											ShowPesanWarning_OpenArForm('Silahkan pilih dahulu baris yang akan dihapus','Hapus baris');
										}
									}
								}
						}
					]						
				},
				{  	
					title: 'Jurnal' ,
					id:'tabDtlJurnal_OpenArForm', 					
					frame: false,
					border:false,					
					items:
					[
						GetGridDTLJurnal_OpenArForm(),
						TotalJurnal_OpenArForm
					],      
					listeners: 
					{
						activate: function()
						{			
							/*mActiveTab = "2";
							Ext.getCmp('btnTambahBaris_OpenArForm').setVisible(false);
							Ext.getCmp('btnHapusBaris_OpenArForm').setVisible(false);
							CalcTotalMHS_OpenArForm();*/
						}
					}
				}
			] 
		}
	);

	
	var Formload_OpenArForm = new Ext.Panel
	(
		{
			id: 'Formload_OpenArForm',
			region: 'center',
			width:'100%',
			anchor:'100%',
			layout: 'form',
			title: '',
			bodyStyle: 'padding:15px',
			border: true,
			bodyStyle: 'background:#FFFFFF;',
			shadhow: true,
			//iconCls: 'GL',
			items: [pnlTR_OpenArForm,GDtabDetail_OpenArForm]

		}
	);
	
	return Formload_OpenArForm						
};

function TambahBarisItem_OpenArForm()
{
	var p = new mRecordItem_OpenArForm
		(
			{
				LINE:'',
				ITEM_CODE: '',
				ITEM_DESC: '',
				ACCOUNT: '',//Ext.get('txtCatatan_PenerimaanMhs').getValue(),
				DESCRIPTION: '',								
				VALUE:0
			}
		);

	dsDTLTRListItem_OpenArForm.insert(dsDTLTRListItem_OpenArForm.getCount(), p);
};

function HapusBaris_OpenArForm()
{
	
	if (DataAddNew_OpenArForm == false){
		console.log(cellSelectedListItem_OpenArForm.data);
		if (cellSelectedListItem_OpenArForm.data.ITEM_CODE!=''){ //&& cellSelectedListItem_OpenArForm.data.ITEM_DESC!=''){

					if (CurrentTRListItem_OpenArForm.row >= 0)
					{
						Ext.Msg.show
						(
							{
							   title:'Hapus Baris',
							   msg: 'Apakah baris ini akan dihapus ?' + ' ' + 'Baris :'+ ' ' + (CurrentTRListItem_OpenArForm.row + 1) + ' dengan Account : '+ ' ' + cellSelectedListItem_OpenArForm.data.ACCOUNT ,
							   buttons: Ext.MessageBox.YESNO,
							   fn: function (btn) 
							   {			
								   if (btn =='yes') 
									{
										if (cellSelectedListItem_OpenArForm.data.LINE !='' && cellSelectedListItem_OpenArForm.data.LINE !=undefined){
											dsDTLTRListItem_OpenArForm.removeAt(CurrentTRListItem_OpenArForm.row);
										    //CalcTotal_OpenArForm();
										    CalcTotalItem_OpenArForm();
											CalcTotalJurnal_OpenArForm();
											HapusBaris_OpenArFormDB(cellSelectedListItem_OpenArForm.data);
										    cellSelectedListItem_OpenArForm = undefined;
										} else{
											dsDTLTRListItem_OpenArForm.removeAt(CurrentTRListItem_OpenArForm.row);
										    //CalcTotal_OpenArForm();
										    cellSelectedListItem_OpenArForm = undefined;
										}

									} 
							   },
							   icon: Ext.MessageBox.QUESTION
							}
						);
					}
					else
					{
					    dsDTLTRListItem_OpenArForm.removeAt(CurrentTRListItem_OpenArForm.row);
					    //CalcTotal_OpenArForm();
					    cellSelectedListItem_OpenArForm = undefined;
					}

		} else{
			dsDTLTRListItem_OpenArForm.removeAt(CurrentTRListItem_OpenArForm.row);
		    //CalcTotal_OpenArForm();
		    cellSelectedListItem_OpenArForm = undefined;

		}

	}else{
		dsDTLTRListItem_OpenArForm.removeAt(CurrentTRListItem_OpenArForm.row);
		    //CalcTotal_OpenArForm();
		    cellSelectedListItem_OpenArForm = undefined;

	}	
	
	
};


function getParamHapusBaris_OpenArForm(row) 
{
	var params = 
	{
		
		Table: 'viACC_AR_OPEN',
		arf_number:Ext.getCmp('txtNo_OpenArForm').getValue(),
		//ARO_DATE:ShowDateAkuntansi(Ext.getCmp('dtpTanggal_OpenArForm').getValue()),
		LINE:row.LINE,
		ITEM_CODE:row.ITEM_CODE,
		ACCOUNT:row.ACCOUNT,
		VALUE:row.VALUE,
		BARIS:1	
	
	};
	return params
};

function HapusBaris_OpenArFormDB(row) 
{	
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/anggaran/fakturAR/delete_baris",
			params: getParamHapusBaris_OpenArForm(row), 
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				//alert(cst.success);
				if (cst.success === true) 
				{						
						RefreshDataFilter_OpenArForm(false);
						RefreshDataItem_OpenArForm(Ext.getCmp('txtNo_OpenArForm').getValue(),ShowDateAkuntansi(Ext.getCmp('dtpTanggal_OpenArForm').getValue()));
						RefreshDataJurnal_OpenArForm(Ext.getCmp('txtNo_OpenArForm').getValue(),ShowDateAkuntansi(Ext.getCmp('dtpTanggal_OpenArForm').getValue()));

						
						ShowPesanInfo_OpenArForm(" Data berhasil di hapus","Hapus Data")

				}
				/* else 
				{
					ShowPesanError_OpenArForm(" Data gagal di hapus","Hapus Data")
				} */
			}
		}
	)
	
};

var dsDTLTRListItem_OpenArForm;
var dsTmp_OpenArForm;
var dsDTLTRListItem_OpenArForm;

var mRecordItem_OpenArForm = Ext.data.Record.create
	(
		[
			{name: 'LINE', mapping:'LINE'},
		   {name: 'ITEM_CODE', mapping:'ITEM_CODE'},
		   {name: 'ITEM_DESC', mapping:'ITEM_DESC'},
		   {name: 'ACCOUNT', mapping:'ACCOUNT'},
		   {name: 'DESCRIPTION', mapping:'DESCRIPTION'},
		   {name: 'VALUE', mapping:'VALUE'}
		]
	);

function RefreshDataItem_OpenArForm(no,tgl)
{			
	dsDTLTRListItem_OpenArForm.load
	(
		{ 
			params: 
			{ 	
				Skip: 0,
				Take: 100,
				Sort: 'ITEM_CODE',
				Sortdir: 'ASC',
				target:'viewOpenArDetailItem',
				param: 'arf_number = ~'+no+'~ and item_code <>~~  order by line '//no+'#'+tgl//'1'+'#'+'2017-07-13'//" Where KATEGORI='" + KDkategori_PenerimaanMhs + "' "				
			}
		}
	);
return dsDTLTRListItem_OpenArForm;
};


function GetDTLTRGridItem_OpenArForm() 
{
	// grid untuk detail transaksi	
	var fldDetail = ['LINE','ITEM_CODE', 'ITEM_DESC', 'ACCOUNT', 'DESCRIPTION', 'VALUE'] 
	
	dsDTLTRListItem_OpenArForm = new WebApp.DataStore({ fields: fldDetail })
	dsTmp_OpenArForm= new WebApp.DataStore({ fields: fldDetail })
	

	gridDTLTR_OpenArForm = new Ext.grid.EditorGridPanel
	(
		{			
			stripeRows: false,
			store: dsDTLTRListItem_OpenArForm,
			id:'Dttlgrid_OpenArForm',
			border: false,
			columnLines:true,
			stripeRows:true,
			frame:true,
			height: 188,
			anchor: '100% 100%',			
			sm: new Ext.grid.RowSelectionModel
			(
				{
					singleSelect: true,
					listeners: 
					{
						rowselect: function(sm, row, rec)
						{
							//cellSelectedListItem_OpenArForm= undefined;
							cellSelectedListItem_OpenArForm =dsDTLTRListItem_OpenArForm.getAt(row);
							CurrentTRListItem_OpenArForm.row = row;
							CurrentTRListItem_OpenArForm.data = cellSelectedListItem_OpenArForm.data;
							//alert(cellSelectedListItem_OpenArForm.data.LINE)

							
						},
						'focus' : function()
						{
							// alert("Detail Penerimaan'")
						}
					}
				}
			),
			cm: TRDetailItemColumModel_OpenArForm(), 
			viewConfig: 
			{
				forceFit: true
			},
			listeners:
			{
				'afterrender': function()
				{
					this.store.on("load", function()
						{
							CalcTotalItem_OpenArForm();
						} 
					);
					this.store.on("datachanged", function()
						{
							CalcTotalItem_OpenArForm();						
						}
					);

					/*if (DataAddNew_OpenArForm == false){
						Ext.getCmp('FieldcolItem_OpenArForm').disable();
						//NewRow_OpenArForm=true;
					} else if (DataAddNew_OpenArForm == true){
						Ext.getCmp('FieldcolItem_OpenArForm').enable();
						//NewRow_OpenArForm=true;
					}*/
				}
			}
		}
	);
			
	return gridDTLTR_OpenArForm;
};

function TRDetailItemColumModel_OpenArForm() 
{
	return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(), 
			{
				id: 'colAcc_OpenArForm',
				name: 'colAcc_OpenArForm',
				header: "Account",
				dataIndex: 'ACCOUNT',
				sortable: false,
				hidden : true,
				//anchor: '5%',
				width: 100,
				editor: new Ext.form.TextField
				(
					{
						id:'FieldcolAcc_OpenArForm',
						allowBlank: false,
						enableKeyEvents : true,
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
																
								} 
							}
						}
					}
				)
			}, 
			{
				id: 'colItem_OpenArForm',
				name: 'colItem_OpenArForm',
				header: "Item",
				dataIndex: 'ITEM_CODE',
				sortable: false,
				//anchor: '5%',
				width: 100,
				editor: new Ext.form.TextField
				(
					{
						id:'FieldcolItem_OpenArForm',
						allowBlank: false,
						enableKeyEvents : true,
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									//if (DataAddNew_OpenArForm == true){

										var strKriteria='';

											if (Ext.getCmp('FieldcolItem_OpenArForm').getValue()!=''){
												strKriteria="  I.Item_Code like '" + Ext.getCmp('FieldcolItem_OpenArForm').getValue() + "%' AND I.item_group='0'"; 
											}else{
												strKriteria="  I.item_group='0' "; 
												
											}
											var p = new mRecordItem_OpenArForm
											(
												{
													LINE:'',
													ITEM_CODE: '',
													ITEM_DESC: '',
													ACCOUNT: '',
													DESCRIPTION: '',								
													VALUE: 0
												}
											);
										//alert(CurrentTRListItem_OpenArForm.row);
										FormLookupItemGrid_OpenArForm(strKriteria,dsDTLTRListItem_OpenArForm,p,true,CurrentTRListItem_OpenArForm.row,false);								
										//rowSelected_OpenArForm=undefined;	
										cellSelectedListItem_OpenArForm=undefined;								
								} 
							}
						}
					}
				)
			}, 
			{
				id: 'ColDesc_OpenArForm',
				name: 'ColDesc_OpenArForm',
				header: "Deskripsi",
				dataIndex: 'DESCRIPTION',
				width: 300,
				renderer: function(v, params, record) 
				{
					var str = "<div style='white-space:normal;padding:2px 2px 2px 2px;'>" + record.data.DESCRIPTION + "</div>";
					return str;
				}
			}, 
			{
				id: 'ColAmount_OpenArForm',
				name: 'ColAmount_OpenArForm',
				header: "Jumlah",
				//anchor: '30%',
				width: 100,
				dataIndex: 'VALUE', 
				align:'right',
				renderer: function(v, params, record) 
				{
					var str = "<div style='white-space:normal;padding:2px 10px 2px 2px;'>" + formatCurrency(record.data.VALUE) + "</div>";
					return str;
				},
				editor: new Ext.form.NumberField
				(
					{
						id:'FieldColAmount_OpenArForm',
						allowBlank: false,
						enableKeyEvents : true,
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
										CalcTotalItem_OpenArForm();
										CalcTotalJurnal_OpenArForm();

										if (CekArrKosong_KasKeluar()==1){
											TambahBarisItem_OpenArForm();
										}
								} 
							},
							/*'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 9) 
								{
										CalcTotalItem_OpenArForm();
										CalcTotalJurnal_OpenArForm();
										if (CekArrKosong_KasKeluar()==1){
											TambahBarisItem_OpenArForm();
										}
								} 
							},*/
							'blur' : function()
							{									
								CalcTotalItem_OpenArForm();
								CalcTotalJurnal_OpenArForm();							
							},
							'change': function(){	
							    CalcTotalItem_OpenArForm();
								CalcTotalJurnal_OpenArForm();			
							},
							'keyup': {
							       fn: function(obj) {	
							      		 CalcTotalItem_OpenArForm();
										CalcTotalJurnal_OpenArForm();	
							      },
							      delay: 1000
							}
						}
						
					}
				)
			}, 

		]
	)
};


function CalcTotalItem_OpenArForm(idx)
{
    var total=Ext.num(0);
	var nilai=Ext.num(0);
	for(var i=0;i < dsDTLTRListItem_OpenArForm.getCount();i++)
	{
		nilai=dsDTLTRListItem_OpenArForm.data.items[i].data.VALUE;
		 total += getNumber(nilai);
		//total +=  nilai
	}	
	Ext.getCmp('txtTotalItem_OpenArForm').setValue(formatCurrency(total));
	Ext.getCmp('txtAmount_OpenArForm').setValue(formatCurrency(total));

};

function CalcTotalJurnal_OpenArForm()
{
   	var totalDebit=Ext.num(0);
    var totalKredit=Ext.num(0);
	for(var i=0;i < dsDTLJurnal_OpenArForm.getCount();i++)
	{
		if ((GetAngka2_OpenArForm(dsDTLJurnal_OpenArForm.data.items[i].data.debit === 0)) || GetAngka2_OpenArForm((dsDTLJurnal_OpenArForm.data.items[i].data.debit === '')))
		{
			dsDTLJurnal_OpenArForm.data.items[i].data.debit=0;
		}		
		if ((GetAngka2_OpenArForm(dsDTLJurnal_OpenArForm.data.items[i].data.credit === 0)) || GetAngka2_OpenArForm(dsDTLJurnal_OpenArForm.data.items[i].data.credit === ''))
		{
			dsDTLJurnal_OpenArForm.data.items[i].data.credit=0;
		}
		totalDebit += Ext.num(GetAngka2_OpenArForm(dsDTLJurnal_OpenArForm.data.items[i].data.debit));
		totalKredit += Ext.num(GetAngka2_OpenArForm(dsDTLJurnal_OpenArForm.data.items[i].data.credit));
		
	}
	Ext.getCmp('txtdebit_OpenArForm').setValue(formatCurrency(totalDebit));//Ext.getCmp('txtAmount_OpenArForm').getValue());//formatCurrencyDec(2200000);//totalDebit
	Ext.getCmp('txtcredit_OpenArForm').setValue(formatCurrency(totalKredit));//Ext.getCmp('txtAmount_OpenArForm').getValue());//formatCurrencyDec(2200000);//totalKredit

};

function GetAngka2_OpenArForm(str)
{
	// for (var i = 0; i < str.length; i++) 
	// {
	// 	var y = str.substr(i, 1)
	// 	if (y === '.') 
	// 	{
	// 		str = str.replace('.', '');
	// 	}
	// };
	return str;
};

var dsDTLJurnal_OpenArForm;
var dsTmp_OpenArForm;

var mRecordJurnal_OpenArForm = Ext.data.Record.create
	(
		[
		   {name: 'LINE', mapping:'LINE'},
		   {name: 'ACCOUNT', mapping:'ACCOUNT'},
		   {name: 'DESCRIPTION', mapping:'DESCRIPTION'},
		   {name: 'DEBIT', mapping:'DEBIT'},
		   {name: 'CREDIT', mapping:'CREDIT'}
		]
	);

function RefreshDataJurnal_OpenArForm(no,tgl)
{			
	dsDTLJurnal_OpenArForm.load
	(
		{ 
			params: 
			{ 	
				Skip: 0,
				Take: 50,
				Sort: 'LINE',
				Sortdir: 'ASC',
				target:'viewOpenArDetail',
				param: no+'#'+tgl//'1'+'#'+'2017-07-13'//" Where KATEGORI='" + KDkategori_PenerimaanMhs + "' "				
			}
		}
	);
return dsDTLJurnal_OpenArForm;
};

function GetGridDTLJurnal_OpenArForm() 
{
	var fldDetail = 
	[
		
		'line', 'account', 'description', 'debit', 'credit'
	]	
	
	dsDTLJurnal_OpenArForm = new WebApp.DataStore({ fields: fldDetail })
	dsTmp_OpenArForm = new WebApp.DataStore({ fields: fldDetail })
	
	var GridDTLJurnal_OpenArForm = new Ext.grid.EditorGridPanel
	(
		{			
			stripeRows: true,
			store: dsDTLJurnal_OpenArForm,
			id:'GridDTLJurnal_OpenArForm',
			border: false,
			columnLines:true,
			frame:true,
			height: 215,
			anchor: '100% 100%',			
			sm: new Ext.grid.CellSelectionModel
			(
				{
					singleSelect: true,
					listeners: 
					{
						cellselect: function(sm, row, rec)
						{
							/*cellSlctdDetMHS_OpenArForm =dsDTLJurnal_OpenArForm.getAt(row);
							CurrentTRMHS_OpenArForm.row = row;
							nRowSelect_OpenArForm = row;
							sTrColAccount_OpenArForm=cellSlctdDetMHS_OpenArForm.data.NIM;

							Ext.get('txtTahun_OpenArForm').dom.value=cellSlctdDetMHS_OpenArForm.data.THN_AJAR_AWAL; //hanca
							Ext.get('txtThnakhir_OpenArForm').dom.value=cellSlctdDetMHS_OpenArForm.data.THN_AKHIR;
							Ext.getCmp('cboSemester_OpenArForm').setValue(cellSlctdDetMHS_OpenArForm.data.JNS_SEMESTER);
*/
						},
						'focus' : function()
						{
							//alert("Detail Mahasiswa")
						}
					}
				}
			),
			cm: TRDetailColumJurnalModel_OpenArForm(), 
			viewConfig: 
			{
				forceFit: true
			},
			listeners:
			{
				'afterrender': function(sm, row, rec)
				{
					this.store.on("load", function()
						{

							CalcTotalJurnal_OpenArForm();
							//alert('load')
						} 
					);
					this.store.on("datachanged", function()
						{
							//CalcTotalMHS_OpenArForm();	
							CalcTotalJurnal_OpenArForm();	
							//alert('change')				
						}
					);
					//alert('after')	
					CalcTotalJurnal_OpenArForm();

					//}
				},
				activate: function()
				{	
					
					CalcTotalJurnal_OpenArForm();
				}
			}
		}
	);			
	return GridDTLJurnal_OpenArForm;
}

function TRDetailColumJurnalModel_OpenArForm() 
{
	return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(), 
			{
				id: 'ColAccount_OpenArForm',
				name: 'ColAccount_OpenArForm',
				header: "Account",
				dataIndex: 'account',
				width:100
			},{
				id: 'ColDesc_OpenArForm',
				name: 'ColDesc_OpenArForm',
				header: "Deskripsi",
				dataIndex: 'description',
				width:200,
				renderer: function(value, cell) 
				{
					var str = "<div style='white-space:normal;padding:2px 2px 2px 2px;text-align: left;'>" + value+ "</div>";
					return str;
				}
			},
			{
				id: 'ColDebit_OpenArForm',
				name: 'ColDebit_OpenArForm',
				header: "Debit",
				dataIndex: 'debit',
				width:100,
				renderer: function(value, cell) 
				{
					var str = "<div style='white-space:normal;padding:2px 10px 2px 2px;text-align: right;'>" + formatCurrency(value) + "</div>";
					return str;
				}
			},
			{
				id: 'ColCredit_OpenArForm',
				name: 'ColCredit_OpenArForm',
				header: "Kredit",
				dataIndex: 'credit',
				width:100,
				renderer: function(value, cell) 
				{
					var str = "<div style='white-space:normal;padding:2px 10px 2px 2px;text-align: right;'>" + formatCurrency(value) + "</div>";
					return str;
				}
			}

		]
	)
};

var setLookUpsImportData_OpenArForm;
var nowImportData_OpenArForm = new Date();
var NMformImportData_OpenArForm='Import Item';
function setFrmLookUpImportData_OpenArForm(code,name)
{
    var lebar = 550;
    setLookUpsImportData_OpenArForm = new Ext.Window
    (
        {
            id: 'SetLookUpsImportData_OpenArForm',
            title: NMformImportData_OpenArForm, 
            closeAction: 'destroy',
            // y:90,
            width: lebar,
            height: 470,
            resizable:false,
            border: false,
            plain: true,
            layout: 'fit',
            iconCls: 'Penerimaan',
            modal: true,
            items: getFormEntryImportData_OpenArForm(lebar),
            listeners:
            {
                activate: function() 
                {
                },
                afterShow: function() 
                { 
                    this.activate(); 
					setLookUpsImportData_OpenArForm.close();					
                },
                deactivate: function()
                {
                    					
                },
                'afterrender': function() 
				{           
					Ext.getCmp('cmbCustomer_OpenArForm').disable();	
					Ext.get('cmbCustomer_OpenArForm').dom.value=code;	
					Ext.getCmp('cmbCustomer_OpenArForm').setValue(name);
					//alert(code +' - '+name)
					selectCustomer_OpenArForm=code;			 
				}
          
            }
        }
    );        
    setLookUpsImportData_OpenArForm.show();
}

function getFormEntryImportData_OpenArForm(lebar)
{
    var pnlFormImportData_OpenArForm = new Ext.FormPanel
    (
        {
            region: 'center',
            padding: '5px',
            layout: 'fit',
            items: 
			[
                {
                    xtype: 'panel',
                    layout: 'form',
                    height: 470,//195,
                    padding: '5px',
                    labelWidth: 70,
                    items: 
					[
						{
							xtype: 'compositefield',
							fieldLabel: 'Customer :',
							anchor: '100%',
							labelSeparator: '',
							name: 'compkmpImportData_OpenArForm',
							id: 'compkmpImportData_OpenArForm',
							//hidden: true,
							items: 
							[
								comboCustomer_OpenArForm()		
							]
						},
						{
							xtype: 'compositefield',
							fieldLabel: 'Unit :',
							anchor: '100%',
							labelSeparator: '',
							name: 'compkmpImportData2_OpenArForm',
							id: 'compkmpImportData2_OpenArForm',
							//hidden: true,
							items: 
							[
								CboUnit_OpenArForm()	
							]
						},
							{
							xtype: 'compositefield',
							fieldLabel: '',
							anchor: '100%',
							labelSeparator: '',
							name: 'comunitchild_OpenArForm',
							id: 'comunitchild_OpenArForm',
							//hidden: true,
							items: 
							[
								CboUnitChild_OpenArForm()
							]
						},
						{
							xtype: 'compositefield',
							fieldLabel: 'Date :',
							anchor: '100%',
							labelSeparator: '',
							name: 'compPathfiletipeImportData_OpenArForm',
							id: 'compPathfiletipeImportData_OpenArForm',
							//hidden: true,
							items: 
							[
								{
									xtype: 'datefield',
									flex: 1,
									//fieldLabel: 'Tgl. Pembayaran',
									width: 150,
									format: 'd/M/Y',
									value: nowImportData_OpenArForm,
									name: 'DtpAwalTransaksi_OpenArForm',									
									id: 'DtpAwalTransaksi_OpenArForm'

								},
								{
									xtype: 'displayfield',
									id:'dspfldTotalMHS_OpenArForm',
									name:'dspfldTotalMHS_OpenArForm',							
									value:'To',

								},
								{
									xtype: 'datefield',
									flex: 1,
									//fieldLabel: 'Tgl. Pembayaran',
									width: 150,
									format: 'd/M/Y',
									value: nowImportData_OpenArForm,
									name: 'DtpAkhirTransaksi_OpenArForm',									
									id: 'DtpAkhirTransaksi_OpenArForm'

								},
							]
						},
						{
							xtype: 'compositefield',
							fieldLabel: 'Shift :',
							anchor: '100%',
							labelSeparator: '',
							name: 'compshifImportData_OpenArForm',
							id: 'compshifImportData_OpenArForm',
							//hidden: true,
							items: 
							[
								{
									xtype:'checkbox',
									boxLabel: 'All',
									name: 'ChkShfAll_OpenArForm',
									id: 'ChkShfAll_OpenArForm',
									disabled:false,
									checked: true,
									anchor:'95%',
									handler: function() 
									{
										if (this.getValue()===true)
										{
											Ext.getCmp('ChkShf1_OpenArForm').disable();
											Ext.getCmp('ChkShf2_OpenArForm').disable();
											Ext.getCmp('ChkShf3_OpenArForm').disable();
											Ext.getCmp('ChkShf1_OpenArForm').setValue(true);
											Ext.getCmp('ChkShf2_OpenArForm').setValue(true);
											Ext.getCmp('ChkShf3_OpenArForm').setValue(true);
											chkAll=1;
											//alert(chkAll)
										}
										else
										{
											Ext.getCmp('ChkShf1_OpenArForm').enable();
											Ext.getCmp('ChkShf2_OpenArForm').enable();
											Ext.getCmp('ChkShf3_OpenArForm').enable();
											Ext.getCmp('ChkShf1_OpenArForm').setValue(true);
											Ext.getCmp('ChkShf2_OpenArForm').setValue(false);
											Ext.getCmp('ChkShf3_OpenArForm').setValue(false);
											chkAll=0;	
											//alert(chkAll)						
										};
									}
								},
								{
									xtype:'checkbox',
									boxLabel: 'Shift 1',
									name: 'ChkShf1_OpenArForm',
									id: 'ChkShf1_OpenArForm',
									disabled:true,
									checked: true,
									anchor:'95%',
									handler: function() 
									{
										if (this.getValue()===true)
										{
											chk1=1
										}
										else
										{
											chk1=0;						
										};
									}

								},
								{
									xtype:'checkbox',
									boxLabel: 'Shift 2',
									name: 'ChkShf2_OpenArForm',
									id: 'ChkShf2_OpenArForm',
									disabled:true,
									checked: true,
									anchor:'95%',
									handler: function() 
									{
										if (this.getValue()===true)
										{
											
											chk2=1;
										}
										else
										{
											
											chk2=0;							
										};
									}
								},
								{
									xtype:'checkbox',
									boxLabel: 'Shift 3',
									name: 'ChkShf3_OpenArForm',
									id: 'ChkShf3_OpenArForm',
									disabled:true,
									checked: true,
									anchor:'95%',
									handler: function() 
									{
										if (this.getValue()===true)
										{
											
											chk3=1;
										}
										else
										{
											
											chk3=0;							
										};
									}
								},
							]
						},
						GetGridListImport_OpenArForm(),	
						{
							layout: 'hBox',
							border: false,
							defaults: { margins: '0 5 0 0' },
							style:{'margin-left':'10px','margin-top':'15px'},
							anchor: '100%',
							items: 
							[
								{
									xtype: 'button',
									text: 'Tampilkan',
									width: 90,
									hideLabel: true,
									id: 'btnOkImportData_OpenArForm',
									handler: function() 
									{
										
										if (selectUnit_OpenArForm != undefined){
											if (chkAll==1){
												chk1=0;
												chk2=0;
												chk3=0;
											} else{
												chkAll=0;

											}
											dsTRListImport_OpenArForm.removeAll();
											loadMask.show();
											RefreshDataImportFaktur_OpenArForm(selectCustomer_OpenArForm,selectUnitKasir_OpenArForm,ShowDateAkuntansi(Ext.getCmp('DtpAwalTransaksi_OpenArForm').getValue()),ShowDateAkuntansi(Ext.getCmp('DtpAkhirTransaksi_OpenArForm').getValue()),chkAll,chk1,chk2,chk3)
											
										} else{

											ShowPesanWarning_OpenArForm("Pilih Unit","Import Item")
											
										}
										
									}
								},
								{
									xtype: 'button',
									text: 'Import',
									width: 90,
									hideLabel: true,
									id: 'btnEditdataImportData_OpenArForm',
									handler: function() 
									{

											if (chkAll==1){
												chk1=0;
												chk2=0;
												chk3=0;
											} else{
												chkAll=0;

											}

											var notrans='';
											var tgltrans='';
											var shiftrans='';
											var kdkasirtrans='';
											var urut_bayar_trans='';
											var jml=0;
											var selected=0;
											var jmlselected=0;
											
											var SelectedCheckbox=GridListImport_OpenArForm.getSelectionModel();
										
											// for(var i = 0 ; i < dsTRListImport_OpenArForm.getCount();i++){
											for(i=0;i<SelectedCheckbox.selections.length;i++){
												jmlselected+=1;
													var p = new mRecordListFaktur_OpenArForm
														(
															{
																NO_TRANSAKSI:SelectedCheckbox.selections.items[i].data.no_transaksi,
																TGL_TRANSAKSI: SelectedCheckbox.selections.items[i].data.tgl_transaksi,
																KD_KASIR: SelectedCheckbox.selections.items[i].data.kd_kasir,
																SHIFT: SelectedCheckbox.selections.items[i].data.shift,
																KD_USER: SelectedCheckbox.selections.items[i].data.kd_user
															}
														);
													
													dsTRListFaktur_OpenArForm.insert(dsTRListFaktur_OpenArForm.getCount(), p);

													if (jmlselected==1){

														notrans			=	SelectedCheckbox.selections.items[i].data.no_transaksi;
														tgltrans		=	SelectedCheckbox.selections.items[i].data.tgl_transaksi;
														kdkasirtrans	=	SelectedCheckbox.selections.items[i].data.kd_kasir;
														shiftrans		=	SelectedCheckbox.selections.items[i].data.shift;
														urut_bayar_trans=	SelectedCheckbox.selections.items[i].data.urut;
														
														//alert(notrans);
													} else if (jmlselected>1){
														
														notrans=notrans+'#'+SelectedCheckbox.selections.items[i].data.no_transaksi;
														tgltrans=tgltrans+'#'+SelectedCheckbox.selections.items[i].data.tgl_transaksi;
														kdkasirtrans=SelectedCheckbox.selections.items[i].data.kd_kasir;
														shiftrans=shiftrans+'#'+SelectedCheckbox.selections.items[i].data.shift;
														urut_bayar_trans=urut_bayar_trans+'#'+SelectedCheckbox.selections.items[i].data.urut;
														
													}

													selected=i;
													jml=jml+1;
													console.log(SelectedCheckbox.selections.items[i].data);
											}	

											
											if (jmlselected==1){
												ExecuteImportDataItemOne_OpenArForm(kdkasirtrans,selectCustomer_OpenArForm,ShowDateAkuntansi(Ext.getCmp('DtpAwalTransaksi_OpenArForm').getValue()),ShowDateAkuntansi(Ext.getCmp('DtpAkhirTransaksi_OpenArForm').getValue()),notrans,tgltrans,shiftrans,chkAll,chk1,chk2,chk3);	
											} else{
												ExecuteImportDataItem_OpenArForm(
													kdkasirtrans,
													selectCustomer_OpenArForm,
													ShowDateAkuntansi(Ext.getCmp('DtpAwalTransaksi_OpenArForm').getValue()),
													ShowDateAkuntansi(Ext.getCmp('DtpAkhirTransaksi_OpenArForm').getValue()),
													notrans,
													tgltrans,
													shiftrans,
													chkAll,
													chk1,
													chk2,
													chk3,
													jml,
													urut_bayar_trans,
													selectUnit_OpenArForm
												);	
											}
											
											
											IsImport_OpenArForm=1;
											rowSelectedImport_OpenArForm=undefined;
											setLookUpsImportData_OpenArForm.close();
											Ext.getCmp('btnTambahBaris_OpenArForm').setDisabled(true);
 
									}
								},
								{
									xtype: 'button',
									text: 'Cancel',
									width: 70,
									hideLabel: true,
									id: 'btnCanceldataImportData_OpenArForm',
									handler: function() 
									{
										setLookUpsImportData_OpenArForm.close();
									}
								},
								{
									xtype: 'tbspacer',
									width: 50
								},
								{
									xtype: 'label',
									text: 'Total : '
								},
							   
								{
									xtype: 'textfield',
									name: 'Txttotal_jumlah_Import_faktur_ar',
									id: 'Txttotal_jumlah_Import_faktur_ar',
									width: 130,
									disabled:true,
									style: 'text-align: right',
									value: 0,
									listeners:
											{
												'specialkey': function ()
												{
													if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
													{

													}

												}
											}
								}
							]
						}
                    ]
                }
            ]            
        }
    );
        
    return pnlFormImportData_OpenArForm;     
}

function HitungTotalTampilkanFakturAR(){
	var total_import_faktur =0;
	for (var a = 0; a < dsTRListImport_OpenArForm.getCount(); a++){
        // total_import_faktur = dsTRListImport_OpenArForm.data.items[a].data.harga);
		if (dsTRListImport_OpenArForm.data.items[a].data.harga==="" || dsTRListImport_OpenArForm.data.items[a].data.harga==undefined||
			 dsTRListImport_OpenArForm.data.items[a].data.harga==="NaN" )
		{
			dsTRListImport_OpenArForm.data.items[a].data.harga=0;
			total_import_faktur=parseFloat(total_import_faktur)+parseFloat(dsTRListImport_OpenArForm.data.items[a].data.harga);
		}else
		{
			
			total_import_faktur=parseFloat(total_import_faktur)+parseFloat(dsTRListImport_OpenArForm.data.items[a].data.harga);
		}
	}
	 Ext.getCmp('Txttotal_jumlah_Import_faktur_ar').setValue(formatCurrency(total_import_faktur));
}
var rowSelectedImport_OpenArForm;

var fldDetail_IDAR =['no_transaksi','tgl_transaksi','shift','kd_kasir','kd_pay','uraian','harga','kd_user','status','urut']  
	
	dsTRListImport_OpenArForm = new WebApp.DataStore({ fields: fldDetail_IDAR })

function RefreshDataImportFaktur_OpenArForm(no,unit,tgl,tgl2,A,c1,c2,c3)
{			
	dsTRListImport_OpenArForm.load
	(
		{ 
			params: 
			{ 	
				Skip: 0,
				Take: 1000,
				Sort: 'LINE',
				Sortdir: 'ASC',
				target:'viewImportListFakturAR',
				param: no+'#'+unit+'#'+tgl+'#'+tgl2+'#'+A+'#'+c1+'#'+c2+'#'+c3//'1'+'#'+'2017-07-13'//" Where KATEGORI='" + KDkategori_PenerimaanMhs + "' "				
			}
		}
	);
return dsTRListImport_OpenArForm;
};

function ExecuteImportDataItem_OpenArForm(kdksr,cust,tgl1,tgl2,no,tgl,s,A,c1,c2,c3,jml,u,jenis_unit)
{			
	/* dsDTLTRListItem_OpenArForm.load
	(
		{ 
			params: 
			{ 	
				Skip: 0,
				Take: 100,
				Sort: 'LINE',
				Sortdir: 'ASC',
				target:'viImportItemOpenAR',
				param: jml+'##@@##'+kdksr+'##@@##'+no+'##@@##'+tgl+'##@@##'+s+'##@@##'+cust+'#'+tgl1+'#'+tgl2+'#'+A+'#'+c1+'#'+c2+'#'+c3//'1'+'#'+'2017-07-13'//" Where KATEGORI='" + KDkategori_PenerimaanMhs + "' "				
			}
		}
	); */
	loadMask.show();
    Ext.Ajax.request({
        url: baseURL + "index.php/anggaran/viimportitemopenar/select_import_transaksi_detail",
        params: {
            jml_record 				: jml,
            kdksr 					: kdksr,
            no_transaksi_select 	: no,
            tgl1 					: tgl1,
            tgl2 					: tgl2,
            urut 					: u,
            customer 				: cust,
            parent 					: jenis_unit
			
			
        },
        failure: function(o){   
            loadMask.hide();
            // ShowPesanWarningpenerimaan('Hubungi Admin', 'Error');
        },  
        success: function(o){
			var cst = Ext.decode(o.responseText);
			if (cst.success === true){
				 loadMask.hide();
				if (cst.ListDataObj.length === 0) {
					// ShowPesanInfopenerimaan('Tidak Ada Data', 'Info');
				}else{
					 dsDTLTRListItem_OpenArForm.removeAll();
					var recs=[],
					recType=dsDTLTRListItem_OpenArForm.recordType;
					for(var i=0; i<cst.ListDataObj.length; i++){
						recs.push(new recType(cst.ListDataObj[i]));
					}
					dsDTLTRListItem_OpenArForm.add(recs);
					gridDTLTR_OpenArForm.getView().refresh();
					CalcTotalItem_OpenArForm();
				}
			}
		}
	})
return dsDTLTRListItem_OpenArForm;
};

function ExecuteImportDataItemOne_OpenArForm(kdksr,cust,tgl1,tgl2,no,tgl,s,A,c1,c2,c3)
{			
	dsDTLTRListItem_OpenArForm.load
	(
		{ 
			params: 
			{ 	
				Skip: 0,
				Take: 100,
				Sort: 'LINE',
				Sortdir: 'ASC',
				target:'viImportItemOpenOneAR',
				param: kdksr+'#'+cust+'#'+tgl1+'#'+tgl2+'#'+no+'#'+tgl+'#'+s+'#'+A+'#'+c1+'#'+c2+'#'+c3//'1'+'#'+'2017-07-13'//" Where KATEGORI='" + KDkategori_PenerimaanMhs + "' "				
			}
		}
	);
return dsDTLTRListItem_OpenArForm;
};

var fm = Ext.form;
function GetGridListImport_OpenArForm(){
	var selectModel = new Ext.grid.CheckboxSelectionModel();
	var chkStatusMhs_OpenArForm = new Ext.grid.CheckColumn({
        header: "",
        align:'center',
        dataIndex: 'status',
        //value:true,
        width: 30,
        hidden:false
    });
	
	//dsTmpItem_OpenArForm= new WebApp.DataStore({ fields: fldDetail_IDA })
    GridListImport_OpenArForm = new Ext.grid.EditorGridPanel
	(
		{	
			id:'GListImport_OpenArForm',
			stripeRows: true,
			xtype: 'editorgrid',
			store: dsTRListImport_OpenArForm,
			//anchor: '100% 100%',
			width:500,
			columnLines:true,
			border:true,
			autoScroll:true,
			height:240,
			//plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			//sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedImport_OpenArForm = dsTRListImport_OpenArForm.getAt(row);
							console.log(rowSelectedImport_OpenArForm.data);
							//tmpRowSelectListFaktur_OpenArForm=rowSelectedImport_OpenArForm.data.NO_TRANSAKSI;
							//alert(tmpRowSelectListFaktur_OpenArForm)
						}
					}
				}
			),
			listeners:
			{
				/*rowdblclick: function (sm, row, rec)
				{
					rowSelectedImport_OpenArForm = dsTRListImport_OpenArForm.getAt(row);
					
					CurrentSelectedImport_OpenArForm.row = rec;
					CurrentSelectedImport_OpenArForm.data = rowSelectedImport_OpenArForm;

						if (chkAll==1){
							chk1=0;
							chk2=0;
							chk3=0;
						} else{
							chkAll=0;

						}
						var notrans='';
						var tgltrans='';
						var shiftrans='';
						var kdkasirtrans='';
						var jml=0;

								var p = new mRecordListFaktur_OpenArForm
									(
										{
											NO_TRANSAKSI:rowSelectedImport_OpenArForm.data.NO_TRANSAKSI,
											TGL_TRANSAKSI: rowSelectedImport_OpenArForm.data.TGL_TRANSAKSI,
											KD_KASIR: rowSelectedImport_OpenArForm.data.KD_KASIR,
											SHIFT: rowSelectedImport_OpenArForm.data.SHIFT,
											KD_USER: rowSelectedImport_OpenArForm.data.KD_USER
										}
									);
								
								dsTRListFaktur_OpenArForm.insert(dsTRListFaktur_OpenArForm.getCount(), p);

								//if (i==0){

									notrans=rowSelectedImport_OpenArForm.data.NO_TRANSAKSI;
									tgltrans=rowSelectedImport_OpenArForm.data.TGL_TRANSAKSI;
									kdkasirtrans=rowSelectedImport_OpenArForm.data.KD_KASIR;
									shiftrans=rowSelectedImport_OpenArForm.data.SHIFT;
									//alert(ShowDateAkuntansi(Ext.getCmp('DtpAkhirTransaksi_OpenArForm').getValue()))

									ExecuteImportDataItemOne_OpenArForm(kdkasirtrans,selectCustomer_OpenArForm,ShowDateAkuntansi(Ext.getCmp('DtpAwalTransaksi_OpenArForm').getValue()),ShowDateAkuntansi(Ext.getCmp('DtpAkhirTransaksi_OpenArForm').getValue()),notrans,tgltrans,shiftrans,chkAll,chk1,chk2,chk3);	

					alert(notrans +' - '+tgltrans +' - '+kdkasirtrans+'-'+shiftrans+'-'+jml);
											
					//ExecuteImportDataItem_OpenArForm(kdkasirtrans,selectCustomer_OpenArForm,ShowDateAkuntansi(Ext.getCmp('DtpAwalTransaksi_OpenArForm').getValue()),ShowDateAkuntansi(Ext.getCmp('DtpAkhirTransaksi_OpenArForm').getValue()),notrans,tgltrans,shiftrans,chkAll,chk1,chk2,chk3,jml);	
					
					IsImport_OpenArForm=1;
					rowSelectedImport_OpenArForm=undefined;
					setLookUpsImportData_OpenArForm.close();
					Ext.getCmp('btnTambahBaris_OpenArForm').setDisabled(true);
				
				},*/
				'afterrender': function()
				{

					this.store.on("load", function()
						{
							HitungTotalTampilkanFakturAR();
						} 
					);
					this.store.on("datachanged", function()
						{
							if (dsTRListImport_OpenArForm.getCount()==0){
								loadMask.hide();
								ShowPesanWarning_OpenArForm('Tidak ada data','Import Item')

							} else if (dsTRListImport_OpenArForm.getCount()>0){
								loadMask.hide();

							}		
						}
					);

				}
			},
			// colModel: new Ext.grid.ColumnModel			
			cm: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					// chkStatusMhs_OpenArForm,
					selectModel,
					{
						id: 'ColNoTrans_OpenArForm',
						header: 'Nomor',
						dataIndex: 'no_transaksi',
						sortable: true,	
						align:'left',		
						width :80,
						//filter: {}
					}, 
					{
						id: 'ColNoTrans_OpenArForm',
						header: 'urut',
						dataIndex: 'urut',
						sortable: true,	
						align:'left',		
						width :80,
						hidden:true
						//filter: {}
					}, 
					{
						id: 'ColUraianTrans_OpenArForm',
						header: "Uraian",
						//align:'right',
						dataIndex: 'uraian',
						renderer: function(v, params, record) 
						{
							
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px;'>" + record.data.uraian + "</div>";
							return str;
						},
						width :170,
						//filter: {}
					},
					{
						header: 'Tanggal',
						width: 90,
						sortable: true,
						align:'center',
						dataIndex: 'tgl_transaksi',
						id:'ColDateTrans_OpenArForm',
						renderer: function(v, params, record) 
						{
							return ShowDateAkuntansi(record.data.tgl_transaksi);
						}
						//filter: {}			
					}, 
					
					{
						id: 'ColAmountTrans_OpenArForm',
						header: "Jumlah (Rp)",
						align:'right',
						dataIndex: 'harga',
						renderer: function(v, params, record) 
						{
							
							var str = "<div style='white-space:normal;padding:2px 15px 2px 2px;'>" + formatCurrency(record.data.harga) + "</div>";
							return str;
						},
						width :100,
						//filter: {}
					},
				]
			),
			 sm : selectModel,
			// plugins: chkStatusMhs_OpenArForm,
			viewConfig: {forceFit: true} 			
		}
	);

	return GridListImport_OpenArForm;
};

function CopyListFaktur_OpenArForm(notrans,tgltrans,shiftrans,kdkasirtrans)
{
	var x='';

		var jml=0;
	for(var i = 0 ; i < dsTRListImport_OpenArForm.getCount();i++)
	{
		
		if (dsTRListImport_OpenArForm.data.items[i].data.STATUS==true || dsTRListImport_OpenArForm.data.items[i].data.STATUS=='true')
		{

			var p = new mRecordListFaktur_OpenArForm
				(
					{
						NO_TRANSAKSI:dsTRListImport_OpenArForm.data.items[i].data.NO_TRANSAKSI,
						TGL_TRANSAKSI: dsTRListImport_OpenArForm.data.items[i].data.TGL_TRANSAKSI,
						KD_KASIR: dsTRListImport_OpenArForm.data.items[i].data.KD_KASIR,
						SHIFT: dsTRListImport_OpenArForm.data.items[i].data.SHIFT,
						KD_USER: dsTRListImport_OpenArForm.data.items[i].data.KD_USER
					}
				);
			
			dsTRListFaktur_OpenArForm.insert(dsTRListFaktur_OpenArForm.getCount(), p);

			if (i==1){

				notrans=dsTRListImport_OpenArForm.data.items[i].data.NO_TRANSAKSI;
				tgltrans=dsTRListImport_OpenArForm.data.items[i].data.TGL_TRANSAKSI;
				kdkasirtrans=dsTRListImport_OpenArForm.data.items[i].data.KD_KASIR;
				shiftrans=dsTRListImport_OpenArForm.data.items[i].data.SHIFT;

			} else if (i>=2){
				notrans=notrans+'#'+dsTRListImport_OpenArForm.data.items[i].data.NO_TRANSAKSI;
				tgltrans=tgltrans+'#'+dsTRListImport_OpenArForm.data.items[i].data.TGL_TRANSAKSI;
				kdkasirtrans=kdkasirtrans+'#'+dsTRListImport_OpenArForm.data.items[i].data.KD_KASIR;
				shiftrans=shiftrans+'#'+dsTRListImport_OpenArForm.data.items[i].data.SHIFT;

			}

			jml=jml+1;			
		}	
	}			
	return jml;
};

var selectUnit_OpenArForm;
var selectUnitKasir_OpenArForm;
var selectUnitChild_OpenArForm;
var dsUnitChild_OpenArForm;
var dsUnit_OpenArForm;

function CboUnit_OpenArForm()
{
	var Field = ['KODE', 'NAMA']; //,'KD_KASIR'
	dsUnit_OpenArForm = new WebApp.DataStore({ fields: Field });
	dsUnit_OpenArForm.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'KD_UNIT',
			    Sortdir: 'ASC',
			    target: 'ViewComboUnitImportData',
			    param: " "//gstrListUnitKerja + "##@@##" + 0
			}
		}
	);
  	var comboUnit_OpenArForm = new Ext.form.ComboBox
	(
		{
			id:'comboUnit_OpenArForm',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih ',
			//fieldLabel: gstrSatker+' ',			
			align:'Right',
			width:325,
			//anchor:'100%',
			store: dsUnit_OpenArForm,
			valueField: 'KODE',
			displayField: 'NAMA',
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectUnit_OpenArForm=b.data.KODE;
					
					//alert(b.data.Kd_Bagian);
					var params_AR = '';
					if (b.data.KODE === '1') {
						params_AR = "parent = '1' union select 'ALL','999'";
					}else{
						 params_AR = "left(kd_unit,1) = '"+ b.data.KODE +"'  union select 'ALL','999'";
					}
					dsUnitChild_OpenArForm.load
					(
						{
						    params:
							{
							    Skip: 0,
							    Take: 1000,
							    Sort: 'Kd_Unit',
							    Sortdir: 'ASC',
							    target: 'ViewComboUnitPenerimaanPiutang',
							    param: params_AR
							}
						}
					);
				} 
			}
		}
	);
	
	
	//console.log(dsUnit_OpenArForm);
	return comboUnit_OpenArForm;
	//log.console(CboUnit_OpenArForm);

} ;



function CboUnitChild_OpenArForm()
{
	var Field = ['KODE', 'NAMA']; 
	dsUnitChild_OpenArForm = new WebApp.DataStore({ fields: Field });
	
  	var comboUnitChild_OpenArForm = new Ext.form.ComboBox
	(
		{
			id:'comboUnitChild_OpenArForm',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih ',
			//fieldLabel: gstrSatker+' ',			
			align:'Right',
			width:325,
			//anchor:'100%',
			store: dsUnitChild_OpenArForm,
			valueField: 'KODE',
			displayField: 'NAMA',
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectUnitChild_OpenArForm=b.data.KODE;
					selectUnitKasir_OpenArForm=b.data.KODE;
				} 
			}
		}
	);
	
	/*dsUnitChild_OpenArForm.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'Kd_Unit',
			    Sortdir: 'ASC',
			    target: 'ComboUnit',
			    param: ''//gstrListUnitChildKerja + "##@@##" + 0
			}
		}
	);*/
	
	return comboUnitChild_OpenArForm;
} ;

var dsCustomer_OpenArForm;
var selectCustomer_OpenArForm;
function comboCustomer_OpenArForm()
{
	var Field =  ['Cust_Code', 'Customer', 'CustomerName','Account','Due_Day'];
	dsCustomer_OpenArForm = new WebApp.DataStore({ fields: Field });
	dsCustomer_OpenArForm.load
	(
		{
			params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboCustomer_Keuangan',
			    param: ''
			}
		}
	);
	
 var cmbCustomer_OpenArForm = new Ext.form.ComboBox
	(
		{
			id:'cmbCustomer_OpenArForm',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih Customer',
			//fieldLabel: 'Kas / Bank ',			
			align:'Right',
			width:325,
			//anchor:'100%',
			store: dsCustomer_OpenArForm,
		    valueField: 'Cust_Code',
		    displayField: 'CustomerName',
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectCustomer_OpenArForm=b.data.Cust_Code ;
				} 
			}
		}
	);
	
	return cmbCustomer_OpenArForm;
} ;
 //---------------------------------------------------------------------------------------///
 

function Cetak_OpenArForm()
{
	// var strKriteria;
	// if (Ext.get('txtNo_OpenArForm').dom.value !='' )
	// {
		// strKriteria = Ext.get('txtNo_OpenArForm').dom.value + '##'; //01
		// strKriteria += 1+'##';
		// strKriteria += Ext.get('cboCustEntryARForm').dom.value + '##'; //02
		// strKriteria += Ext.getCmp('txtAmount_OpenArForm').getValue() + '##'; //03
		// strKriteria += selectAktivaLancar_OpenArForm +'##'; //04
		// strKriteria +=ShowDateAkuntansi(Ext.getCmp('dtpTanggal_OpenArForm').getValue()) + '##'; //05
		// ShowReport('', '010516', strKriteria);	
		
	// };
	var params={
		arf_number:Ext.getCmp('txtNo_OpenArForm').getValue(),
		arf_date:Ext.get('dtpTanggal_OpenArForm').getValue(),
		kd_customer:Ext.getCmp('cboCustEntryARForm').getValue(),
		customer:Ext.get('cboCustEntryARForm').getValue(),
	} 

	var form = document.createElement("form");
	form.setAttribute("method", "post");
	form.setAttribute("target", "_blank");
	form.setAttribute("action", baseURL + "index.php/keuangan/cetak_faktur_ar/cetakFaktur");
	var hiddenField = document.createElement("input");
	hiddenField.setAttribute("type", "hidden");
	hiddenField.setAttribute("name", "data");
	hiddenField.setAttribute("value", Ext.encode(params));
	form.appendChild(hiddenField);
	document.body.appendChild(form);
	form.submit();	
};

function Cetak_SuratTagihanARFaktur()
{
	var params={
		arf_number:Ext.getCmp('txtNo_OpenArForm').getValue(),
		arf_date:Ext.get('dtpTanggal_OpenArForm').getValue(),
		kd_customer:Ext.getCmp('cboCustEntryARForm').getValue(),
		customer:Ext.get('cboCustEntryARForm').getValue(),
	} 

	var form = document.createElement("form");
	form.setAttribute("method", "post");
	form.setAttribute("target", "_blank");
	form.setAttribute("action", baseURL + "index.php/keuangan/cetak_faktur_ar/cetakSuratTagihan");
	var hiddenField = document.createElement("input");
	hiddenField.setAttribute("type", "hidden");
	hiddenField.setAttribute("name", "data");
	hiddenField.setAttribute("value", Ext.encode(params));
	form.appendChild(hiddenField);
	document.body.appendChild(form);
	form.submit();	
};

//---------------------------------------------------------------------------------------///
			

//---------------------------------------------------------------------------------------///
function DataInit_OpenArForm(rowdata) 
{
	DataAddNew_OpenArForm = false;
Ext.get('txtNo_OpenArForm').dom.readOnly=true;
	Ext.get('cboCustEntryARForm').dom.value = rowdata.CUSTOMER;
	selectCboEntryCustARForm = rowdata.CUST_CODE;
    Ext.get('txtNo_OpenArForm').dom.value = rowdata.ARO_NUMBER;
    Ext.getCmp('dtpTanggal_OpenArForm').disable();   
	Ext.get('dtpTanggal_OpenArForm').dom.value = ShowDateAkuntansi(rowdata.ARO_DATE);   
	Ext.get('txtCatatan_OpenArForm').dom.value = rowdata.NOTES;
	Ext.get('txtAmount_OpenArForm').dom.value = formatCurrencyDec(rowdata.AMOUNT);
	if (rowdata.APPROVE === 'f'){
		Ext.getCmp('ChkApprove_OpenArForm').setValue(false);
	}else{
		Ext.getCmp('ChkApprove_OpenArForm').setValue(true);
	}
	
	Ext.get('dtpDueDate_OpenArForm').dom.value = ShowDateAkuntansi(rowdata.DUE_DATE);   
	GetCustAcc_OpenArForm(rowdata.CUST_CODE);

		RefreshDataItem_OpenArForm(rowdata.ARO_NUMBER,ShowDateAkuntansi(rowdata.ARO_DATE));
	RefreshDataJurnal_OpenArForm(rowdata.ARO_NUMBER,ShowDateAkuntansi(rowdata.ARO_DATE));
	StrAccCust_OpenArForm=rowdata.ACCOUNT;
	StrApp_OpenArForm=rowdata.APPROVE;
	// GetAkunKas_OpenArForm('CBAR');
	//alert(StrApp_OpenArForm);
	if(StrApp_OpenArForm === false || StrApp_OpenArForm === 'f')
	{
		Ext.getCmp('btnSimpanKeluar').setDisabled(false);
		Ext.getCmp('btnHapus').setDisabled(false);
		Ext.getCmp('btnSimpan').setDisabled(false);
		Ext.getCmp('btnApprove').setDisabled(false);	
		Ext.getCmp('btnTambahBaris_OpenArForm').setDisabled(false);
		Ext.getCmp('btnHapusBaris_OpenArForm').setDisabled(false);
	}else
	{
		Ext.getCmp('btnSimpanKeluar').setDisabled(true);
		Ext.getCmp('btnHapus').setDisabled(true);
		Ext.getCmp('btnSimpan').setDisabled(true);
		Ext.getCmp('btnApprove').setDisabled(true);
		Ext.getCmp('btnTambahBaris_OpenArForm').setDisabled(true);
		Ext.getCmp('btnHapusBaris_OpenArForm').setDisabled(true);
	}	

	Ext.get('comboAktivaLancar_OpenArForm').dom.value = rowdata.NAMAACCOUNT;
	selectAktivaLancar_OpenArForm = rowdata.ACCOUNT;
	// Ext.getCmp('btnImportdata_OpenArForm').disable();
	//Ext.getCmp('cmbCustomer_OpenArForm').setValue(rowdata.CUSTOMER);
	//selectCustomer_OpenArForm= rowdata.CUST_CODE;

	Ext.getCmp('txtTotalItem_OpenArForm').setValue(formatCurrency(0));
	Ext.getCmp('txtdebit_OpenArForm').setValue(formatCurrency(0));//Ext.getCmp('txtAmount_OpenArForm').getValue());//formatCurrencyDec(2200000);//totalDebit
    Ext.getCmp('txtcredit_OpenArForm').setValue(formatCurrency(0));


};

//---------------------------------------------------------------------------------------///
function AddNew_OpenArForm() 
{
	DataAddNew_OpenArForm = true;	
	now_OpenArForm=new Date()
	Ext.getCmp('btnImportdata_OpenArForm').enable();
	Ext.get('txtNo_OpenArForm').dom.readOnly=false;
	Ext.getCmp('dtpTanggal_OpenArForm').enable(); 
	Ext.get('txtNo_OpenArForm').dom.value = '';	
	Ext.getCmp('ChkApprove_OpenArForm').setValue(false);
	Ext.get('txtAmount_OpenArForm').dom.value = '';
	Ext.get('txtCatatan_OpenArForm').dom.value = 'Summary from cashier post : Open AR';	
	Ext.get('dtpTanggal_OpenArForm').dom.value = ShowDateAkuntansi(now_OpenArForm);
	Ext.get('cboCustEntryARForm').dom.value = '';
	Ext.getCmp('cboCustEntryARForm').setValue('');
	selectCboEntryCustARForm = '';
	Ext.get('dtpDueDate_OpenArForm').dom.value = ShowDateAkuntansi(now_OpenArForm);   
	
	StrAccCust_OpenArForm=''
	rowSelected_OpenArForm=undefined;
	//GetAkunKas_OpenArForm('CBAR');
	// Ext.getCmp('tabDtlJurnal_OpenArForm').disable();
	StrApp_OpenArForm=false;
	Ext.getCmp('btnApprove').setDisabled(false);	
	Ext.getCmp('btnSimpanKeluar').setDisabled(false);
	Ext.getCmp('btnHapus').setDisabled(false);
	Ext.getCmp('btnSimpan').setDisabled(false);
	Ext.getCmp('btnCetak').setDisabled(true);
	Ext.getCmp('btnTambahBaris_OpenArForm').setDisabled(false);
	Ext.getCmp('btnHapusBaris_OpenArForm').setDisabled(false);

	dsDTLTRListItem_OpenArForm.removeAll();
	dsDTLJurnal_OpenArForm.removeAll();	



	Ext.getCmp('comboAktivaLancar_OpenArForm').reset();
	selectAktivaLancar_OpenArForm = '';
	GetAkunKas_OpenArForm('CBAR');

		Ext.getCmp('txtTotalItem_OpenArForm').setValue(formatCurrency(0));
	Ext.getCmp('txtAmount_OpenArForm').setValue(formatCurrency(0));
	Ext.getCmp('txtdebit_OpenArForm').setValue(formatCurrency(0));
	Ext.getCmp('txtcredit_OpenArForm').setValue(formatCurrency(0));
};

function getParam_OpenArForm() 
{
	var params = 
	{
		Table: 'viACC_AR_OPEN',
		arf_number:Ext.get('txtNo_OpenArForm').getValue(),
		arf_date:Ext.get('dtpTanggal_OpenArForm').getValue(),
		cust_code:selectCboEntryCustARForm,
		Due_date:Ext.get('dtpDueDate_OpenArForm').getValue(),
		Amount:getAmount_OpenArForm(Ext.get('txtAmount_OpenArForm').getValue()),
		Paid:0,
		note:Ext.get('txtCatatan_OpenArForm').getValue(),
		account:StrAccCust_OpenArForm,
		no_tag:'',
		date_tag:'',
		Posted:'false',
		is_import:IsImport_OpenArForm,
		baris:0,
		List:getArrDetail_OpenArForm(),	
		JmlField:5,		
		//JmlList:dsDTLTRListItem_OpenArForm.getCount(),
		List2:getArrListFaktur_OpenArForm(),	
		JmlField2:5,		
		JmlList2:dsTRListFaktur_OpenArForm.getCount()	
	};
	
	for (var L=0; L<dsDTLTRListItem_OpenArForm.getCount(); L++)
	{
		if (dsDTLTRListItem_OpenArForm.data.items[L].data.ITEM_CODE !== ''){
			params['ITEM_CODE'+L]             =dsDTLTRListItem_OpenArForm.data.items[L].data.ITEM_CODE;
			params['ACCOUNT'+L]         =dsDTLTRListItem_OpenArForm.data.items[L].data.ACCOUNT;
			params['ITEM_GROUP'+L]                 =dsDTLTRListItem_OpenArForm.data.items[L].data.ITEM_GROUP;
			if (L==0){
					params['LINE'+L]=2 
				} else if (L >= 1){
					params['LINE'+L]=L+2 
				}
			//params['LINE'+L]               =dsDTLTRListItem_OpenArForm.data.items[L].data.LINE;
			params['VALUE'+L]               =dsDTLTRListItem_OpenArForm.data.items[L].data.VALUE;
			params['DESKRIPSI'+L]               =dsDTLTRListItem_OpenArForm.data.items[L].data.DESCRIPTION;
			params['JmlList']               =L+1;
		}
		
	}
	return params
};

function getParamDelete_OpenArForm() 
{
	var params = 
	{
		Table: 'viACC_AR_OPEN',
		arf_number:Ext.get('txtNo_OpenArForm').getValue(),
		arf_date:Ext.get('dtpTanggal_OpenArForm').getValue(),
		IS_IMPORT:IsImport_OpenArForm
	};
	return params
};

function CekArrKosong_KasKeluar()
{
	var x=1;
	for(var i = 0 ; i < dsDTLTRListItem_OpenArForm.getCount();i++)
	{
		if (dsDTLTRListItem_OpenArForm.data.items[i].data.ITEM_CODE=='' || dsDTLTRListItem_OpenArForm.data.items[i].data.ITEM_CODE==undefined){
			x=0;
		}

	}		
	return x;
};

function getArrListFaktur_OpenArForm()
{
	var x='';
	for(var i = 0 ; i < dsTRListFaktur_OpenArForm.getCount();i++)
	{
		var y='';
		var z='@@##$$@@';
		
		
			y = 'NO_TRANSAKSI=' + dsTRListFaktur_OpenArForm.data.items[i].data.NO_TRANSAKSI	
			//y += z + 'ITEM_DESC='+dsDTLTRListItem_OpenArForm.data.items[i].data.ITEM_DESC
			y += z + 'TGL_TRANSAKSI=' + dsTRListFaktur_OpenArForm.data.items[i].data.TGL_TRANSAKSI
			y += z + 'KD_KASIR=' + dsTRListFaktur_OpenArForm.data.items[i].data.KD_KASIR
			y += z + 'SHIFT=' + dsTRListFaktur_OpenArForm.data.items[i].data.SHIFT
			y += z + 'KD_USER=' + dsTRListFaktur_OpenArForm.data.items[i].data.KD_USER


			if (i === (dsTRListFaktur_OpenArForm.getCount()-1))
			{
				x += y 
			}
			else
			{
				x += y + '##[[]]##'
			}
				
		//}
			
		
	}		
	return x;
};


function getArrDetail_OpenArForm()
{
	var x='';
	for(var i = 0 ; i < dsDTLTRListItem_OpenArForm.getCount();i++)
	{
		var y='';
		var z='@@##$$@@';
		
		// if (DataAddNew_OpenArForm === true)
		/*if (dsDTLTRListItem_OpenArForm.data.items[i].data.ITEM_CODE==""){
			alert('index:'+ i +'-'+dsDTLTRListItem_OpenArForm.data.items[i].data.ITEM_CODE);
		}*/
		//{
		if (dsDTLTRListItem_OpenArForm.data.items[i].data.ITEM_CODE!="" || dsDTLTRListItem_OpenArForm.data.items[i].data.ITEM_DESC!=""){

		
			y = 'ITEM_CODE=' + dsDTLTRListItem_OpenArForm.data.items[i].data.ITEM_CODE	
			//y += z + 'ITEM_DESC='+dsDTLTRListItem_OpenArForm.data.items[i].data.ITEM_DESC
			y += z + 'ACCOUNT=' + dsDTLTRListItem_OpenArForm.data.items[i].data.ACCOUNT
			y += z + 'DESCRIPTION=' + dsDTLTRListItem_OpenArForm.data.items[i].data.DESCRIPTION
			y += z + 'VALUE=' + dsDTLTRListItem_OpenArForm.data.items[i].data.VALUE
			if (i==0){
				y += z + 'LINE='+1 
			} else if (i >= 1){
				y += z + 'LINE='+ (i + 1) 
			} 
			

			if (i === (dsDTLTRListItem_OpenArForm.getCount()-1))
			{
				x += y 
			}
			else
			{
				x += y + '##[[]]##'
			}
				
		}
			
		
	}		
	return x;
};

function CekArrDetail_OpenArForm()
{
	var x=1;
	if (dsDTLTRListItem_OpenArForm.getCount()==0){
	x=0
	}else {	

		for(var i = 0 ; i < dsDTLTRListItem_OpenArForm.getCount();i++)
		{

			if (dsDTLTRListItem_OpenArForm.data.items[i].data.ITEM_CODE=="" || dsDTLTRListItem_OpenArForm.data.items[i].data.ITEM_DESC==""){
			
			x=0
			}
	
		}
	}
		
	return x;
};

function getParamApprove_OpenArForm(TglApprove, NoApprove,NoteApprove) 
{
	var params = 
	{
		Table: 'viACC_AR_OPEN',
		arf_number:Ext.get('txtNo_OpenArForm').getValue(),
		arf_date:Ext.get('dtpTanggal_OpenArForm').getValue(),
		cust_code:selectCboEntryCustARForm,
		due_date:Ext.get('dtpDueDate_OpenArForm').getValue(),
		amount:getAmountFocus_OpenArForm(Ext.get('txtAmount_OpenArForm').getValue()),
		paid:0,
		notes:Ext.get('txtCatatan_OpenArForm').getValue(),
		is_debitintotal:0,
		no_tag:NoApprove,
		//kd_user:strKdUser,
		date_tag:TglApprove,
		account:StrAccCust_OpenArForm,
		type: 1,
        info: 'ARF',
		posted:1,
		List:getArrDetail_OpenArForm(),	
		JmlField:5,		
		JmlList:dsDTLTRListItem_OpenArForm.getCount()
	};
	return params
};

//---------------------------------------------------------------------------------------///


function getAmount_OpenArForm(dblNilai)
{
    var dblAmount;
    dblAmount = dblNilai.replace('Rp.', '')
    for (var i = 0; i < dblAmount.length; i++) {
        var x = dblAmount.substr(i, 1)
        if (x === '.') {
            dblAmount = dblAmount.replace('.', '');
        }
    }    
    // return Ext.num(dblAmount)
    return dblAmount.replace(',', '.')
};


function getAmountFocus_OpenArForm(dblNilai)
{
    var dblAmount;
    dblAmount = dblNilai
    for (var i = 0; i < dblAmount.length; i++) {
        var x = dblAmount.substr(i, 1)
        if (x === '.') {
            dblAmount = dblAmount.replace('.', '');
        }
    }    
    return dblAmount.replace(',', '.') //Ext.num(dblAmount)
};

function getItemPanelInput_OpenArForm(rowdata,lebar)
{
	var items = 
	{
		layout:'form',
		anchor:'100%',
		width: lebar - 36,
		height: 150,//130,
		labelAlign: 'right',
		bodyStyle: 'padding:7px 7px 7px 7px',
		items:
		[
			{
				columnWidth:.9,
				width:lebar-36,
				layout: 'form',
				border:false,
				items: 
				[
					getItemPanelNo_OpenArForm(lebar),
					getItemPanelTerimaDari_OpenArForm(rowdata,lebar),
					getItemPanelTglDue_OpenArForm(lebar),
					getItemPanelAktivaLancar_OpenArForm(lebar),
					getItemPanelCatatan_OpenArForm(lebar)
				]
			}
		]
	};
	return items;			
};

function getItemPanelAktivaLancar_OpenArForm(lebar)
{
	var items = 			
	{
	layout:'column',
	border:false,
	width:lebar-36,
	items:
		[
			{
				columnWidth:.4,
				layout: 'form',
				border:false,
				labelWidth:111,
				items: 
				[
					mcomboAktivaLancar_OpenArForm(),
				]
			},
			{
				// columnWidth:0.98,
				columnWidth:.5,
				layout: 'form',
				border:false,
				 labelWidth:111,
				items: 
				[
					{
						xtype:'textfield',
						fieldLabel: 'Amount ',
						name: 'txtAmount_OpenArForm',
						id: 'txtAmount_OpenArForm',
						//anchor:'100%',
						anchor:'95%',
						readOnly:true,
						//baseChars: '0123456789.',
						//enableKeyEvents : true,
						style:
							{	
								'text-align':'right',
								'font-weight':'bold'
							},
						listeners:
						{
							'keyup': function()
							{
								// Ext.getCmp('txtAmount_OpenArForm').setValue(formatCurrencyDec(this.getValue()));
								// ValidasiEntryJurnal_OpenArForm('Jurnal');
							},
							'blur': function()
							{
								this.setValue(formatCurrencyDec(this.getValue()));
								// ValidasiEntryJurnal_OpenArForm('Jurnal');
							},
							'focus': function()
							{
								// this.setValue(this.getValue());
								this.setValue(getAmountFocus_OpenArForm(this.getValue()));
							}
						}	
					}
				]
			}
			
		]
	}
	return items;	
}; 

function getItemPanelCatatan_OpenArForm(lebar)
{
	var items = 			
	{
	layout:'column',
	border:false,
	width:lebar-36,
	items:
		[
			{
				columnWidth:0.98,
				layout: 'form',
				border:false,
				labelWidth:111,
				items: 
				[
					{
						xtype:'textarea',
						fieldLabel: 'Keterangan ',
						name: 'txtCatatan_OpenArForm',
						id: 'txtCatatan_OpenArForm',
						value:'Summary from cashier post : Open AR',
						// anchor:'99.9%'
						width:480,
						height:40,
						autoCreate: {tag: 'input', maxlength: '100'}
					}
				]
			}
		]
	}
	return items;	
}; 

function getItemPanelNo_OpenArForm(lebar)
{
	var items = 			
	{
	layout:'column',
	border:false,
	width:lebar-36,
	items:
		[
			{
				columnWidth:.5,
				layout: 'form',
				border:false,
				labelWidth:111,
				items: 
				[
					{
						xtype:'textfield',
						fieldLabel: 'Nomor',
						name: 'txtNo_OpenArForm',
						id: 'txtNo_OpenArForm',
						readOnly:false,
						anchor:'95%',
						listeners: 
						{
							'blur' : function()
							{									
								//alert('Lost Focus');//CalcTotal_PenerimaanNonMhs(CurrentTR_PenerimaanNonMhs.row);									
							}, 
						}	
					}
				]
			},
			{
				columnWidth:.5,
				layout: 'form',
				border:false,
				labelWidth:100,
				items: 
				[
					{
						xtype:'checkbox',
						fieldLabel: 'Approve',
						name: 'ChkApprove_OpenArForm',
						id: 'ChkApprove_OpenArForm',
						disabled:true,
						anchor:'95%'
					}
				]
			}
		]
	}
	return items;	
};

function getItemPanelTerimaDari_OpenArForm(rowdata,lebar)
{
	var items = 			
	{
	layout:'column',
	width:lebar-36,
	border:false,
	items:
		[
			{
				columnWidth:1,
				layout: 'form',
				border:false,
				labelWidth:111,
				items: 
				[
					mCboCustEntry_OpenArForm(),
				]
			}
		]
	}
	return items;	
}

function getItemPanelTglDue_OpenArForm(lebar)
{
	var items = 			
	{
	layout:'column',
	width:lebar-36,
	border:false,
	items:
		[
			{
				columnWidth:.4,
				layout: 'form',
				border:false,
				labelWidth:111,
				items: 
				[
					{
						xtype: 'datefield',
                        fieldLabel: 'Tanggal ',
                        id: 'dtpTanggal_OpenArForm',
                        name: 'dtpTanggal_OpenArForm',
                        format: 'd/M/Y',
						value:now_OpenArForm,
                        anchor: '100%',
                        listeners:
						{
							'keyup': function()
							{
								var strparam;
								strparam=Ext.get('dtpTanggal_OpenArForm').getValue()+'###@###'+strDue_Day+'###@###';
								GetDueDate_OpenArForm(strparam);
							},
							'blur': function()
							{
								var strparam;
								strparam=Ext.get('dtpTanggal_OpenArForm').getValue()+'###@###'+strDue_Day+'###@###';
								GetDueDate_OpenArForm(strparam);
							},
							'focus': function()
							{
								var strparam;
								strparam=Ext.get('dtpTanggal_OpenArForm').getValue()+'###@###'+strDue_Day+'###@###';
								GetDueDate_OpenArForm(strparam);
							}
						}	
					}
				]
			},
			{
				columnWidth:.5,
				layout: 'form',
				border:false,
				items: 
				[
					{
						xtype: 'datefield',
                        fieldLabel: 'Due date ',
                        id: 'dtpDueDate_OpenArForm',
                        name: 'dtpDueDate_OpenArForm',
                        format: 'd/M/Y',
						value:now_OpenArForm,
                        anchor: '100%'
                        // readOnly:true,
                        // disabled:true
					}
				]
			}
		]
	}
	return items;	
}

function RefreshData_OpenArForm()
{			
	dsTRList_OpenArForm.load
	(
		{ 
			params: 
			{ 	
				Skip: 0,
				Take: selectCount_OpenArForm,
				Sort: 'ARO_DATE',
				Sortdir: 'ASC',
				target:'viACC_AR_OPEN',
				param: " "
			}
		}
	);
	return dsTRList_OpenArForm;
};

function mComboMaksData_OpenArForm()
{
  var cboMaksDataOpenArForm = new Ext.form.ComboBox
	(
		{
			id:'cboMaksDataOpenArForm',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Maks.Data ',			
			width:50,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[1, 50], [2, 100],[3, 200],[4, 500],[5, 1000]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCount_OpenArForm,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectCount_OpenArForm=b.data.displayText ;
					RefreshDataFilter_OpenArForm(false);
				} 
			},
			hidden:true
		}
	);
	return cboMaksDataOpenArForm;
};

function mCboCustEntry_OpenArForm()
{
	var Fields = ['KODE', 'NAMA','ACCOUNT','TERM'];
    dsCboEntryCustOpenArForm = new WebApp.DataStore({ fields: Fields });
	
	dsCboEntryCustOpenArForm.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				Sort: 'NAMA', 
				Sortdir: 'ASC', 
				target: 'ViewComboCustomer_Keuangan',
				param: ''
			} 
		}
	);
    
    var cboCustEntryARForm = new Ext.form.ComboBox
	(
		{
		    id: 'cboCustEntryARForm',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: 'Pilih customer ...',
		    fieldLabel: 'Customer ',
		    align: 'right',
		    anchor:'90%',
		    store: dsCboEntryCustOpenArForm,
		    valueField: 'KODE',
		    displayField: 'NAMA',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
					var strparam;
			        selectCboEntryCustARForm = b.data.KODE;
			        StrAccCust_OpenArForm = b.data.ACCOUNT;
			        strDue_Day = b.data.TERM;
					strparam=Ext.get('dtpTanggal_OpenArForm').getValue()+'###@###'+strDue_Day+'###@###';
					//Ext.get('dtpDueDate_OpenArForm').dom.value = ShowDateAkuntansi(b.data.TERM);
					//getduedatecustomer(b.data.KODE);
					GetDueDate_OpenArForm(strparam);
					GetCustAcc_OpenArForm(selectCboEntryCustARForm);
					// ValidasiEntryJurnal_OpenArForm();
					// Ext.getCmp('dtpDueDate_OpenArForm').setValue(ShowDateAkuntansi(now_OpenArForm.setDate(now_OpenArForm.getDate() + strDue_Day)));
			    }
			}
		}
	);
	
	return cboCustEntryARForm;
};
function getduedatecustomer(kode){
    Ext.Ajax.request
    (
        {
            url: baseURL + "index.php/anggaran/fakturAR/getduedatecustomer",
            params: {kodecustomer : kode},
            failure: function (o){
                ShowPesanWarningFaktur_AR('Hubungi Admin', 'Error');
            },
            success: function (o)
            {
                
                var cst = Ext.decode(o.responseText);
                if (cst.success === true){
                    var date = new Date();
                    // add a day
					Ext.getCmp('dtpDueDate_OpenArForm').setValue(date.getDate() + cst.due_date);
                }
            }
        }

    );
}

function GetCustAcc_OpenArForm(kode){
    Ext.Ajax.request
    (
        {
            url: baseURL + "index.php/anggaran/fakturAR/getcustacccustomer",
            params: {kodecustomer : kode},
            failure: function (o){
                ShowPesanWarningFaktur_AR('Hubungi Admin', 'Error');
            },
            success: function (o)
            {
                
                var cst = Ext.decode(o.responseText);
                if (cst.success === true){
                    var date = new Date();
					StrAccCust_OpenArForm = cst.ListDataObj[0].account;
				 	StrNmAccCust_OpenArForm=cst.ListDataObj[0].name;
				 	StrDue_Day=cst.ListDataObj[0].term;
                    
                }else{
					ShowPesanInfo_OpenArForm('Customer tidak ditemukan','Customer');
				}
            }
        }

    );
}
function addDays(days)
{
    var dat = new Date(this.valueOf());
    dat.setDate(dat.getDate() + days);
    return dat;
}

function RefreshDataFilter_OpenArForm(mBol) 
{   
	var strCrt_OpenArForm='';
	if (Ext.get('txtNoFilter_OpenArForm').getValue() != '') 
	{
		if (strCrt_OpenArForm ==='')
		{
			strCrt_OpenArForm = "  ARF_NUMBER like '" + Ext.get('txtNoFilter_OpenArForm').getValue() + "%' ";
		}
		else
		{
			strCrt_OpenArForm += " AND ARF_NUMBER like '" + Ext.get('txtNoFilter_OpenArForm').getValue() + "%' ";
		}
	};
	
	//alert(ShowDateAkuntansi(Ext.getCmp('dtpTglAwalFilter_OpenArForm').getValue()))

	if (Ext.getCmp('chkWithTgl_OpenArForm').getValue() === true) 
	{
		if (strCrt_OpenArForm ==='')
		{
			strCrt_OpenArForm = "  ARO.ARF_DATE >='" + FormatDateReport(Ext.getCmp('dtpTglAwalFilter_OpenArForm').getValue())  + "' ";
			strCrt_OpenArForm += " and  ARO.ARF_DATE <='" + FormatDateReport(Ext.getCmp('dtpTglAkhirFilter_OpenArForm').getValue())  + "' ";
		}
		else
		{
			strCrt_OpenArForm += " and (ARO.ARF_DATE >='" + FormatDateReport(Ext.getCmp('dtpTglAwalFilter_OpenArForm').getValue())  + "' ";
			strCrt_OpenArForm += " and  ARO.ARF_DATE <='" + FormatDateReport(Ext.getCmp('dtpTglAkhirFilter_OpenArForm').getValue())  + "') ";
		}
	};	

	if(Ext.getCmp('chkFilterApproved_OpenArForm').getValue() === true)
	{
		if (strCrt_OpenArForm ==='')
		{
			strCrt_OpenArForm = "  POSTED='t'";
		}
		else
		{
			strCrt_OpenArForm += " AND POSTED='t'";
		}
	}else{
		if (strCrt_OpenArForm ==='')
		{
			strCrt_OpenArForm = "  POSTED='f'";
		}
		else
		{
			strCrt_OpenArForm += " AND POSTED='f'";
		}
	};

    if (strCrt_OpenArForm != undefined) 
    {  
		dsTRList_OpenArForm.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCount_OpenArForm, 
					Sort: 'ARO_DATE', 
					Sortdir: 'ASC', 
					target:'viACC_AR_OPEN',
					param: strCrt_OpenArForm
				}			
			}
		);        
    }
	else
	{
	    RefreshDataFilter_OpenArForm(true);
	}
};

function Approve_OpenArForm(TglApprove, NomApprove,NoteApprove) 
{
	loadMask.show();
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/anggaran/fakturAR/post",
			params:  getParamApprove_OpenArForm(TglApprove, NomApprove,NoteApprove), 
			success: function(o) 
			{				
				var cst = Ext.decode(o.responseText);
				loadMask.hide();
				if (cst.success === true)
				{
					ShowPesanInfo_OpenArForm('Data berhasil di Approve','Approve');
					Ext.getCmp('ChkApprove_OpenArForm').setValue(true);
					RefreshDataFilter_OpenArForm(false);	

					if(StrApp_OpenArForm === false || StrApp_OpenArForm ==='f')
					{
						Ext.getCmp('btnApprove').setDisabled(false);	
						Ext.getCmp('btnTambahBaris_OpenArForm').setDisabled(false);
						Ext.getCmp('btnHapusBaris_OpenArForm').setDisabled(false);
					}else
					{
						Ext.getCmp('btnApprove').setDisabled(false);
						Ext.getCmp('btnTambahBaris_OpenArForm').setDisabled(true);
						Ext.getCmp('btnHapusBaris_OpenArForm').setDisabled(true);
					}							
				}
				else if (cst.success === false && cst.pesan === 0 )
				{
						loadMask.hide();
					ShowPesanWarning_OpenArForm('Data tidak berhasil di Approve '+ cst.pesan,'Edit Data');
				}
				else 
				{
						loadMask.hide();
					ShowPesanError_OpenArForm('Data tidak berhasil di Approve '+ cst.pesan,'Approve');
				}										
			}
		}
	)
}

function Datasave_OpenArForm(IsExit) 
{
	if (ValidasiEntry_OpenArForm('Simpan Data') == 1 )
	{
		/* if (DataAddNew_OpenArForm == true) 
		{ */
		    Ext.Ajax.request
				(
					{
					    url: baseURL + "index.php/anggaran/fakturAR/save",
					    params: getParam_OpenArForm(),
					    success: function(o) {
					        var cst = Ext.decode(o.responseText);
					        if (cst.success === true) 
							{
								Ext.getCmp('btnImportdata_OpenArForm').disable();
					            ShowPesanInfo_OpenArForm('Data berhasil di simpan', 'Simpan Data');
								if (IsExit===false)
								{
									Ext.get('txtNo_OpenArForm').dom.value = cst.noaro;
									if(StrApp_OpenArForm === false)
									{
										Ext.getCmp('btnApprove').setDisabled(false);
										Ext.getCmp('btnCetak').setDisabled(false);											
									}else
									{
										Ext.getCmp('btnApprove').setDisabled(false);
										Ext.getCmp('btnCetak').setDisabled(true);
									}	
								}
					            RefreshDataFilter_OpenArForm(false);
					            RefreshDataItem_OpenArForm(cst.noaro,ShowDateAkuntansi(Ext.getCmp('dtpTanggal_OpenArForm').getValue()));
								RefreshDataJurnal_OpenArForm(cst.noaro,ShowDateAkuntansi(Ext.getCmp('dtpTanggal_OpenArForm').getValue()));

								//dsDTLTRListItem_OpenArForm.removeAll();
								//dsDTLJurnal_OpenArForm.removeAll();
								DataAddNew_OpenArForm=false;
								// ValidasiEntryJurnal_OpenArForm()	
								rowSelected_OpenArForm=undefined;
								Ext.get('txtNo_OpenArForm').dom.readOnly=true;							
					        }
					        else if (cst.success === false && cst.pesan === 0) 
							{
					            ShowPesanWarning_OpenArForm('Data tidak berhasil di simpan, ' + cst.pesan , 'Simpan Data');
					        }
					        else {
					            ShowPesanError_OpenArForm('Data tidak berhasil di simpan, Error : ' + cst.pesan, 'Simpan Data');
					        }
					    }
					}
				)
		/* }
		else 
		{
			Ext.Ajax.request
			 (
				{
					 url: "./Datapool.mvc/UpdateDataObj",
					 params:  getParam_OpenArForm(), 
					 success: function(o) 
					 {
						
						    var cst = Ext.decode(o.responseText);
							if (cst.success === true)
							{
								ShowPesanInfo_OpenArForm('Data berhasil di edit','Edit Data');
								RefreshDataFilter_OpenArForm(false);
								RefreshDataItem_OpenArForm(cst.noaro,ShowDateAkuntansi(Ext.getCmp('dtpTanggal_OpenArForm').getValue()));
								RefreshDataJurnal_OpenArForm(cst.noaro,ShowDateAkuntansi(Ext.getCmp('dtpTanggal_OpenArForm').getValue()));

								//dsDTLTRListItem_OpenArForm.removeAll();
								//dsDTLJurnal_OpenArForm.removeAll();
								rowSelected_OpenArForm=undefined;
							}
							else if (cst.success === false && cst.pesan === 0 )
							{
								ShowPesanWarning_OpenArForm('Data tidak berhasil di edit, ' +  cst.pesan ,'Edit Data');
							}
							else {
								ShowPesanError_OpenArForm('Data tidak berhasil di edit, '  + cst.pesan ,'Edit Data');
							}
												
					}
				}
			)
		} */
	}
};

function ValidasiEntry_OpenArForm(modul)
{
	var x = 1;
	//alert(Ext.get('txtAmount_OpenArForm').getValue())
	/* if(Ext.get('txtNo_OpenArForm').getValue()=='')
	{
		ShowPesanWarning_OpenArForm('Nomor belum di isi',modul);
		x=0;
	}
	else */ if(selectCboEntryCustARForm == '' || selectCboEntryCustARForm == undefined)
	{
		// ShowPesanWarning_OpenArForm('Customer belum di pilih',modul);
		ShowPesanWarning_OpenArForm('Customer belum dipilih');
		x=0;
	}else if(Ext.get('txtAmount_OpenArForm').getValue()=='' || Ext.get('txtAmount_OpenArForm').getValue()==0)
	{
		// ShowPesanWarning_OpenArForm('Amount belum di isi',modul);
		ShowPesanWarning_OpenArForm('Amount belum diisi');
		x=0;
	} else if (Ext.get('txtAmount_OpenArForm').getValue()=='0' || Ext.get('txtAmount_OpenArForm').getValue()=='0,00' || Ext.get('txtAmount_OpenArForm').getValue()=='0.00'){
		ShowPesanWarning_OpenArForm('Amount belum diisi');
		x=0;
	}
	else if (Ext.get('txtCatatan_OpenArForm').getValue().length > 100)
	{
		ShowPesanWarning_CSAPForm('Keterangan Tidak boleh lebih dari 100 Karakter',modul);
		x=0;
	};
	if (CekRowGridDetail_OpenArForm(modul) === 0 )
    {
        x=0;
    };
	return x;
};

function CekRowGridDetail_OpenArForm(modul)
{
    var x=1;
    if (dsDTLTRListItem_OpenArForm  != undefined || dsDTLTRListItem_OpenArForm !='')
    {
        if (dsDTLTRListItem_OpenArForm .getCount() == 0)
        {
            x=0;
            ShowPesanWarning_OpenArForm('Item belum di isi', modul);
        }
        else
        {
            for (var i = 0; i < dsDTLTRListItem_OpenArForm .getCount(); i++) 
            {
                if (dsDTLTRListItem_OpenArForm .data.items[i].data.ITEM_CODE === '' || dsDTLTRListItem_OpenArForm .data.items[i].data.ITEM_CODE === undefined)
                {
                    x=0;
                    if (dsDTLTRListItem_OpenArForm .data.items[i].data.ITEM_CODE != undefined)
                    {
/*                        ShowPesanWarning_OpenApForm('Account Dengan kode ' + dsDTLTRListItem_OpenApForm .data.items[i].data.ITEM_CODE + ' Belum Di Isi', modul);
                    }else
                    {*/
                        ShowPesanWarning_OpenArForm('Data tidak berhasil di simpan, Terdapat baris yang kosong', modul);
                    }
                }                              
                /* if (CekRowGridDiag_OpenApForm(dsDTLTRListItem_OpenApForm .data.items[i].data.ITEM_CODE) == 0)
                {
                    x=0
                    ShowPesanWarning_OpenApForm('Account ' + dsDTLTRListItem_OpenApForm .data.items[i].data.ITEM_CODE +' sudah ada, silahkan pilih Account yang lain ', modul);
                } */
            }
        }
    }
    else
    {   
        x=0;
    };
    return x;
};
function ValidasiApp_OpenArForm(modul,mBolHapus)
{
    var x = 1;
	/* if (Ext.get('txtNo_OpenArForm').dom.value =='' )
	{
		x=0;
		if (mBolHapus === false)
		{
			ShowPesanWarning_OpenArForm("Nomor belum terisi",modul);
		}
	}; */
    return x;
}

function ShowPesanWarning_OpenArForm(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};

function ShowPesanError_OpenArForm(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR
		}
	);
};

function ShowPesanInfo_OpenArForm(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO
		}
	);
};

function DataDelete_OpenArForm() 
{
	// if (ValidasiEntry_OpenArForm('Hapus Data') == 1 )
	// {	
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/anggaran/fakturAR/delete",
				params:  getParamDelete_OpenArForm(), 
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true)					
					{
							ShowPesanInfo_OpenArForm('Data berhasil di hapus','Hapus Data');
							RefreshDataFilter_OpenArForm(false);
							AddNew_OpenArForm();
							rowSelected_OpenArForm=undefined;
					}
					else if (cst.success === false && cst.pesan === 0 )
					{
						ShowPesanWarning_OpenArForm('Data tidak berhasil di hapus, data tersebut belum ada','Hapus Data');
					}
					else 
					{
						ShowPesanError_OpenArForm('Data tidak berhasil di hapus '+ cst.pesan,'Hapus Data');
					}
				}
			}
		)
	// }
};

function ButtonDisabled_OpenArForm(mBol)
{
	Ext.get('btnSimpan').dom.disabled=mBol;
	Ext.get('btnSimpanKeluar').dom.disabled=mBol;
	Ext.get('btnHapus').dom.disabled=mBol;	
	Ext.get('btnApprove').dom.disabled=mBol;	
	Ext.get('btnCetak').dom.disabled=mBol;	
};


function GetDueDate_OpenArForm(param)
{
	 Ext.Ajax.request
	 (
		
		{
            url: baseURL + "index.php/anggaran/fakturAR/getduedatecustomer_filterbytgl",
            params: {Params: param},
            failure: function (o){
                ShowPesanWarningFaktur_AR('Hubungi Admin', 'Error');
            },
            success: function (o)
            {
                
                var cst = Ext.decode(o.responseText);
                if (cst.success === true){
                    // add a day
					Ext.getCmp('dtpDueDate_OpenArForm').setValue(ShowDateAkuntansi(cst.DueDay))
                }
            }
        }
		
		
		);
};

function GetAkunKas_OpenArForm(param)
{
	 Ext.Ajax.request
	 (
		{
			url: "./Module.mvc/ExecProc",
			params:
			{
				UserID: 'Admin',
				ModuleID: 'GetAccKasArAp',
				Params:	param 
			},
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true )
				{
				 	// StrNmAccKas_OpenArForm=cst.NamaAcc
				 	// StrAccKas_OpenArForm=cst.Account
				 	Ext.get('comboAktivaLancar_OpenArForm').dom.value = cst.NamaAcc;
					selectAktivaLancar_OpenArForm = cst.Account;
				}
				else
				{
					//ShowPesanInfo_OpenArForm	('Kas tidak ditemukan','Customer');
				}
			}

		}
		);
};

function mcomboAktivaLancar_OpenArForm()
{
	var Field = ['Account','Name','Groups','AKUN'];
	dsAktivaLancarPenerimaanMhs = new WebApp.DataStore({ fields: Field });
	dsAktivaLancarPenerimaanMhs.load
	(
		{
			params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: '',
			    Sortdir: 'ASC',
			    target: 'viCboAktivaLancar',
			    param: ''
			}
		}
	);
	
 var comboAktivaLancar_OpenArForm = new Ext.form.ComboBox
	(
		{
			id:'comboAktivaLancar_OpenArForm',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih Kas / Bank...',
			fieldLabel: 'Kas / Bank ',
			align:'Right',
			listWidth:300,
			anchor:'100%',
			width:150,
			store: dsAktivaLancarPenerimaanMhs,
			valueField: 'Account',
			displayField: 'AKUN',
			hidden:true,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectAktivaLancar_OpenArForm=b.data.Account ;
				} 
			}
		}
	);
	
	return comboAktivaLancar_OpenArForm;
} ;


////-----------------------------------------
var rowSelectedLookItem_OpenArForm;
var mWindowGridLookupItem_OpenArForm;
var CurrentDataItemLookup_OpenArForm =
	{
	    data: Object,
	    details: Array,
	    row: 0
	};
	
var dsLookupListItem_OpenArForm;

function FormLookupItemGrid_OpenArForm(criteria,dsStore,p,mBolAddNew,idx,mBolLookup) 
{
	
    var vWinFormEntry = new Ext.Window
	(
		{
			id: 'FormGrdPgwLookup_OpenArForm',
			title: 'Lookup Item',
			closable:true,
			width: 450,
			height: 400,
			border: true,
			plain: true,
			resizable:false,
			layout: 'form',
			iconCls: 'find',
			modal: true,                                   
			items: 
			[
				fnGetDTLGridLookUpItem_OpenArForm(criteria,dsStore,p,mBolAddNew,idx,mBolLookup,vWinFormEntry),
				{
					xtype:'button',
					text:'Ok',
					width:70,
					style:{'margin-left':'360px','margin-top':'7px'},
					hideLabel:true,
					id: 'btnOkAcc',
					handler:function()
					{
						if (p != undefined && rowSelectedLookItem_OpenArForm != undefined)
						{
							

								if (dsStore != undefined)
								{
									
									p.data.ITEM_CODE=rowSelectedLookItem_OpenArForm.data.ITEM_CODE;
									p.data.ITEM_DESC=rowSelectedLookItem_OpenArForm.data.ITEM_DESC;		
									p.data.ACCOUNT=rowSelectedLookItem_OpenArForm.data.ACCOUNT;
									p.data.DESCRIPTION=rowSelectedLookItem_OpenArForm.data.ITEM_DESC;
									
									dsStore.removeAt(idx);
									dsStore.insert(idx, p);
								}
							
						}
						rowSelectedLookItem_OpenArForm=undefined;
						vWinFormEntry.close();
					}	
				}
			],
			listeners:
				{ 
					activate: function()
					{ } 
				}
		}
	);
    vWinFormEntry.show();
	mWindowGridLookupItem_OpenArForm  = vWinFormEntry; 
};

///---------------------------------------------------------------------------------------///

function fnGetDTLGridLookUpItem_OpenArForm(criteria,dsStore,p,mBolAddNew,idx,mBolLookup,vWinFormEntry) 
{  
	var fldDetail = 
	[
		'ITEM_CODE','ITEM_DESC','ITEM_GROUP','ACCOUNT'
	];
	
	dsLookupListItem_OpenArForm = new WebApp.DataStore({ fields: fldDetail });
	var vGridLookAccFormEntry_OpenArForm = new Ext.grid.EditorGridPanel
	(
		{ 
			id:'vGridLookAccFormEntry_OpenArForm',
			title: '',
			stripeRows: true,
			store: dsLookupListItem_OpenArForm,
			height:330,
			columnLines:true,
			bodyStyle: 'padding:0px',
			border: false,		
			sm: new Ext.grid.RowSelectionModel
			(
				{ 
					singleSelect: true,
					listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelectedLookItem_OpenArForm = dsLookupListItem_OpenArForm.getAt(row);
							CurrentDataItemLookup_OpenArForm.row = row;
					        CurrentDataItemLookup_OpenArForm.data = rowSelectedLookItem_OpenArForm;
						}
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, row, rec)
				{
					rowSelectedLookItem_OpenArForm = dsLookupListItem_OpenArForm.getAt(row);
					CurrentDataItemLookup_OpenArForm.row = rec;
					CurrentDataItemLookup_OpenArForm.data = rowSelectedLookItem_OpenArForm;
					
					if (p != undefined && rowSelectedLookItem_OpenArForm != undefined)
						{

								if (dsStore != undefined)
								{
									//alert(rowSelectedLookItem_OpenArForm.data.ID_GOL_GAPOK)
									//'Item_Code','Item_Desc','Item_Group','Account'
									p.data.ITEM_CODE=rowSelectedLookItem_OpenArForm.data.ITEM_CODE;
									p.data.ITEM_DESC=rowSelectedLookItem_OpenArForm.data.ITEM_DESC;		
									p.data.ACCOUNT=rowSelectedLookItem_OpenArForm.data.ACCOUNT;
									p.data.DESCRIPTION=rowSelectedLookItem_OpenArForm.data.ITEM_DESC;
									//p.data.VALUE=rowSelectedLookItem_OpenArForm.data.GOLONGAN_GAPOK;
									
									dsStore.removeAt(idx);
									dsStore.insert(idx, p);
								}
						}
						rowSelectedLookItem_OpenArForm=undefined;
						mWindowGridLookupItem_OpenArForm.close();
				}
			},
			cm: fnGridLookAccColumnModel_OpenArForm(),
			viewConfig: { forceFit: true }
		});
		
	dsLookupListItem_OpenArForm.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 100, 
				Sort: 'Item_Code', 
				Sortdir: 'ASC', 
				target:'viewFakItem',
				param: criteria
			} 
		}
	);
	return vGridLookAccFormEntry_OpenArForm;
};



function fnGridLookAccColumnModel_OpenArForm() 
{
    return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(),
			{ 
				id: 'colAkunlookup_OpenArForm',
				header: "Account",
				dataIndex: 'ACCOUNT',
				width: 100
			},
			{ 
				id: 'colItemDesclookup_OpenArForm',
				header: "Desc",
				dataIndex: 'ITEM_DESC',
				width: 300
			},
			
			/*{ 
				id: 'colItemlookup_OpenArForm',
				header: "Item",
				dataIndex: 'Item_Code',
				width: 70
			},*/

		]
	)
};
///---------------------------------------------------------------------------------------///
////------------FIND DIALOG----------------------\\\
var nowFind_OpenArForm = new Date();
var selectcboOperator_OpenArForm;
var selectcboField_OpenArForm;
//var frmFindDlg_OpenArForm= fnFindDlg_OpenArForm();
//frmFindDlg_OpenArForm.show();
var winFind_OpenArForm;

function fnFindDlg_OpenArForm()
{  	
    winFind_OpenArForm = new Ext.Window
	(
		{ 
			id: 'winFindx_OpenArForm',
			title: 'Find',
			closeAction: 'destroy',
			width:350,
			height: 200,					
			border: false,
			resizable:false,
			plain: true,
			layout: 'fit',
			iconCls: 'find',
			modal: true,
			items: [ItemFindDlg_OpenArForm()]
			
		}
	);

	winFind_OpenArForm.show();
	
    return winFind_OpenArForm; 
};

function ItemFindDlg_OpenArForm() 
{	
	var PnlFindDlg_OpenArForm = new Ext.Panel
	(
		{ 
			id: 'PnlFindDlg_OpenArForm',
			fileUpload: true,
			layout: 'anchor',
			// width:350,
			height: 90,
			anchor: '100%',
			bodyStyle: 'padding:5px',
			border: true,
			items: 
			[

				{
					xtype: 'compositefield',
					fieldLabel: '',
					anchor: '100%',
					items: 
					[
						{
							xtype: 'fieldset',
							title: '',
							width: 325,
							height: 125, 
							items: 
							[
								mComboFindField_OpenArForm(),
								mComboOperator_OpenArForm(),
								getItemFindKeyWord_OpenArForm(),
								getItemFindTgl_OpenArForm(),
								
				
							]
						}						
					]
				},
				{
					layout: 'hBox',
					border: false,
					defaults: { margins: '0 5 0 0' },
					style:{'margin-left':'21px','margin-top':'5px'},
					anchor: '100%',
					layoutConfig: 
					{
						padding: '3',
						pack: 'end',
						align: 'middle'
					},
					items: 
					[
						{
							xtype: 'button',
							text: 'Ok',
							width: 70,
							hideLabel: true,
							id: 'btnOkLap_OpenArForm',
							handler: function() 
							{
								if (ValidasiReport_OpenArForm()==1){
									RefreshDataFindRecord_OpenArForm()
								}
								
							}
						},
						{
							xtype: 'button',
							text: 'Cancel',
							width: 70,
							hideLabel: true,
							id: 'btnCancelLap_OpenArForm',
							handler: function() 
							{
								winFind_OpenArForm.close();
							}
						}
					]
				}
			]
		}
	);
 
    return PnlFindDlg_OpenArForm;
};

function RefreshDataFindRecord_OpenArForm() 
{   
	var strparam='';
	if(selectcboOperator_OpenArForm==1){

		if (selectcboField_OpenArForm=="ARF_DATE"){

			strparam="  "+selectcboField_OpenArForm+" like '"+ ShowDateAkuntansi(Ext.getCmp('dtpTglFind_OpenArForm').getValue()) + "%' "; 

		} else{
			strparam="  "+selectcboField_OpenArForm+" like '"+ Ext.getCmp('txtKeyWordFind_OpenArForm').getValue() + "%' "; 

		}		
		
	} else if (selectcboOperator_OpenArForm==2){

		if (selectcboField_OpenArForm=="ARF_DATE"){

			strparam="  "+selectcboField_OpenArForm+" = '"+ ShowDateAkuntansi(Ext.getCmp('dtpTglFind_OpenArForm').getValue()) + "' "; 

		} else{
			strparam="  "+selectcboField_OpenArForm+" = '"+ Ext.getCmp('txtKeyWordFind_OpenArForm').getValue() + "' "; 

		}

	} else if (selectcboOperator_OpenArForm==3){
		 
		if (selectcboField_OpenArForm=="ARF_DATE"){

			strparam="  "+selectcboField_OpenArForm+" < '"+ ShowDateAkuntansi(Ext.getCmp('dtpTglFind_OpenArForm').getValue()) + "' "; 

		} else{
			strparam="  "+selectcboField_OpenArForm+" < '"+ Ext.getCmp('txtKeyWordFind_OpenArForm').getValue() + "' ";

		}

	} else if (selectcboOperator_OpenArForm==4){

		if (selectcboField_OpenArForm=="ARF_DATE"){

			strparam="  "+selectcboField_OpenArForm+" > '"+ ShowDateAkuntansi(Ext.getCmp('dtpTglFind_OpenArForm').getValue()) + "' "; 

		} else{
			strparam="  "+selectcboField_OpenArForm+" > '"+ Ext.getCmp('txtKeyWordFind_OpenArForm').getValue() + "' ";

		}
		
	} else if (selectcboOperator_OpenArForm==5){
		if (selectcboField_OpenArForm=="ARF_DATE"){

			strparam="  "+selectcboField_OpenArForm+" <= '"+ ShowDateAkuntansi(Ext.getCmp('dtpTglFind_OpenArForm').getValue()) + "' "; 

		} else{
			strparam="  "+selectcboField_OpenArForm+" <= '"+ Ext.getCmp('txtKeyWordFind_OpenArForm').getValue() + "' ";

		}
		
		
	} else if (selectcboOperator_OpenArForm==6){
		if (selectcboField_OpenArForm=="ARF_DATE"){

			strparam="  "+selectcboField_OpenArForm+" >= '"+ ShowDateAkuntansi(Ext.getCmp('dtpTglFind_OpenArForm').getValue()) + "' "; 

		} else{
			strparam="  "+selectcboField_OpenArForm+" >= '"+ Ext.getCmp('txtKeyWordFind_OpenArForm').getValue() + "' ";

		}
		
	} else if (selectcboOperator_OpenArForm==7){
		if (selectcboField_OpenArForm=="ARF_DATE"){

			strparam="  "+selectcboField_OpenArForm+" <> '"+ ShowDateAkuntansi(Ext.getCmp('dtpTglFind_OpenArForm').getValue()) + "' "; 

		} else{
			strparam="  "+selectcboField_OpenArForm+" <> '"+ Ext.getCmp('txtKeyWordFind_OpenArForm').getValue() + "' ";

		}
		
	} 
	
    {  
		dsTRList_OpenArForm.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCount_OpenArForm, 
					Sort: 'ARO_DATE', 
					Sortdir: 'ASC', 
					target:'viACC_AR_OPEN',
					param: strparam
				}			
			}
		);        
    }
	
};


function ValidasiReport_OpenArForm()
{
	var x=1;
	
	if(selectcboField_OpenArForm == undefined || selectcboField_OpenArForm == "")
	{
		ShowPesanWarningFindDlg_OpenArForm('Field belum dipilih','Find');
		x=0;		
	};

	if(selectcboOperator_OpenArForm ==undefined || selectcboOperator_OpenArForm== "")
	{
		
		ShowPesanWarningFindDlg_OpenArForm('Operator belum di pilih','Find');
		x=0;
			
	};

	if(Ext.getCmp('txtKeyWordFind_OpenArForm').getValue()== "")
	{
		
		ShowPesanWarningFindDlg_OpenArForm('Keyword belum di isi','Find');
		x=0;
			
	};

	return x;
};

function ShowPesanWarningFindDlg_OpenArForm(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING,
		   width:180
		}
	);
};

function getItemFindTgl_OpenArForm() 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
		{
			xtype: 'compositefield',
			fieldLabel: '-',
			anchor: '100%',
			items: 
			[
				 {
					columnWidth: 1,
					layout: 'form',
					border: false,
					labelAlign: 'left',
					labelWidth: 100,
					items:
					[
						{
							xtype: 'datefield',
							fieldLabel: 'Periode ',
							id: 'dtpTglFind_OpenArForm',
							format: 'd/M/Y',
							disabled:true,
							value:now_OpenArForm,
							width:190

						}
					]
		    	}
			]
		}
		   
		]
	}
    return items;
};

function getItemFindKeyWord_OpenArForm() 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
		{
			xtype: 'compositefield',
			fieldLabel: '-',
			anchor: '100%',
			items: 
			[
				 {
					columnWidth: 1,
					layout: 'form',
					border: false,
					labelAlign: 'left',
					labelWidth: 100,
					items:
					[
						{
							xtype: 'textfield',
							fieldLabel: 'Key Word ',
							id: 'txtKeyWordFind_OpenArForm',
							name: 'txtKeyWordFind_OpenArForm',
							width:190
						}
					]
		    	}
			 
			]
		}
		   
		]
	}
    return items;
};

function mComboFindField_OpenArForm()
{
	var cboFindField_OpenArForm = new Ext.form.ComboBox
	(
		{
			id:'cboFindField_OpenArForm',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih Field',
			width:190,
			fieldLabel: 'Field Name',	
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: [
							["ARF_NUMBER", "Faktur Number"], 
							["ARF_DATE", "Faktur Date"],
							["ARO.CUST_CODE", "Cust Code"], 
							["C.CUSTOMER", "Customer"],
							["NOTES", "Notes"]
						  ]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			listeners:  
			{
				'select': function(a,b,c)
				{
					selectcboField_OpenArForm=b.data.Id;

					if (b.data.Id=="ARF_DATE"){
						Ext.getCmp('txtKeyWordFind_OpenArForm').disable();
						Ext.getCmp('dtpTglFind_OpenArForm').enable();
					} else{
						Ext.getCmp('dtpTglFind_OpenArForm').disable();
						Ext.getCmp('txtKeyWordFind_OpenArForm').enable();
					}
				}

			}
			
		}
	);
	return cboFindField_OpenArForm;
}


function mComboOperator_OpenArForm()
{
	var cboFindOperator_OpenArForm = new Ext.form.ComboBox
	(
		{
			id:'cboFindOperator_OpenArForm',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			width:190,
			emptyText:'Pilih Operator',
			fieldLabel: 'Operator ',
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: 
					[
						[1, "Like"], 
						[2, "="],
						[3, "<"],
						[4, ">"],
						[5, "<="], 
						[6, ">="],
						[7, "<>"]
					]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			listeners:  
			{
				'select': function(a,b,c)
				{
					selectcboOperator_OpenArForm=b.data.Id;
				}

			}
		}
	);
	return cboFindOperator_OpenArForm;
}



