
var CurrentTR_AdjustApForm = 
{
    data: Object,
    details:Array, 
    row: 0
};

var CurrentTRAkdAdj_AdjustApForm = 
{
    data: Object,
    details:Array, 
    row: 0
};
var CurrentSelected_AdjustApForm = 
{
    data: Object,
    details:Array, 
    row: 0
};

var mRecord_AdjustApForm = Ext.data.Record.create
	(
		[
		   {name: 'ACCOUNT', mapping:'ACCOUNT'},
		   {name: 'NAMAACCOUNT', mapping:'NAMAACCOUNT'},
		   {name: 'Description', mapping:'Description'},
		   {name: 'VALUE', mapping:'VALUE'},
		   {name: 'APAD_LINE', mapping:'APAD_LINE'}
		]
	);

var mRecord = Ext.data.Record.create
(
	[	 
		{name:'APA_NUMBER', mapping: 'APA_NUMBER'}, 
		{name:'APA_DATE', mapping: 'APA_DATE'}, 
		{name:'AMOUNT', mapping: 'AMOUNT'}, 
		{name:'PAID', mapping: 'PAID'},
		{name:'REMAIN', mapping: 'REMAIN'},
	]
);

var KDkategori_AdjustApForm='2';	
var dsTRList_AdjustApForm;
var dsTmp_AdjustApForm;
var dsDTLTRList_AdjustApForm;
var DataAddNew_AdjustApForm = true;
var selectCount_AdjustApForm=50;
var now_AdjustApForm = new Date();
var rowSelected_AdjustApForm;
var TRLookUps_AdjustApForm;
var focusRef_AdjustApForm;
var no_sp3d;
var cellSelectedDet_AdjustApForm;
var cellSelectedDetAkdAdj_AdjustApForm;
var selectCboEntryVendor_AdjustApForm;
var StrAccVend_AdjustApForm;
var strDue_AdjustApForm;
var StrNmAccVend_AdjustApForm;
var StrTotalRecord;
var CekApp_AdjustApForm;
var radTipe_AdjustApForm = 0;
var StrNmAccKas_AdjustApForm;
var StrAccKas_AdjustApForm;
var NamaForm_AdjustApForm="Adjusment AP";
var isDb_AdjustApForm=true;
var isCr_AdjustApForm=true;

CurrentPage.page=getPanel_AdjustApForm(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanel_AdjustApForm(mod_id)
{
    var Field = 
	[
		'APA_NUMBER','APA_DATE','DUE_DATE','VEND_CODE','VENDOR','VENDNAME','TOTALDEBIT','TOTALKREDIT','TYPE','NO_TAG',"DATE_TAG",'NOTES','POSTED'
	]
    dsTRList_AdjustApForm = new WebApp.DataStore({ fields: Field });
        
  	
  
    var grListTR_AdjustApForm = new Ext.grid.EditorGridPanel
	(
		{
			stripeRows: true,
			xtype: 'editorgrid',
			store: dsTRList_AdjustApForm,
			anchor: '100% 100%',
			columnLines:true,
			border:false,
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			//sm: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelected_AdjustApForm = dsTRList_AdjustApForm.getAt(row);
						}
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, row, rec)
				{
					rowSelected_AdjustApForm = dsTRList_AdjustApForm.getAt(row);
					
					CurrentSelected_AdjustApForm.row = rec;
					CurrentSelected_AdjustApForm.data = rowSelected_AdjustApForm;

					if (rowSelected_AdjustApForm != undefined)
						{
							//DataAddNew_OpenArForm=false;
							LookUpForm_AdjustApForm(rowSelected_AdjustApForm.data);
					}
					else
					{
							//DataAddNew_OpenArForm=true;
							LookUpForm_AdjustApForm();
					}
				}
			},
			colModel: new Ext.grid.ColumnModel			
			//cm: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header		: 'Posted',
						width		: 60,
						sortable	: false,
						hideable	: true,
						hidden		: false,
						menuDisabled: true,
						dataIndex	: 'POSTED',
						id			: 'colStatusPosting_viAdjustAP',
						renderer	: function(value, metaData, record, rowIndex, colIndex, store){
							 switch (value){
								 case 't':
									 metaData.css = 'StatusHijau'; 
									 break;
								 case 'f':
									 metaData.css = 'StatusMerah';
									 break;
							 }
							 return '';
						}
					},
					{
						id: 'ColAPA_NUMBER',
						header: 'Nomor',
						dataIndex: 'APA_NUMBER',
						sortable: true,			
						width :100,
						filter: {}
					}, 
					{
						//xtype: 'datecolumn',
						header: 'Tanggal',
						width: 100,
						sortable: true,
						dataIndex: 'APA_DATE',
						id:'ColAPA_DATE',
						renderer: function(v, params, record) 
						{
							return ShowDate(record.data.APA_DATE);
						},
						filter: {}			
					}, 
					{						
						id: 'ColVENDOR',
						header: "Terima Dari",
						dataIndex: 'VENDOR',
						width :130,
						filter: {}
					},
					{
						id: 'ColNotes',
						header: "Keterangan",
						dataIndex: 'NOTES',
						width :200,
						filter: {}
					}, 
					{
						id: 'ColDAmount',
						header: "Debit",
						align:'right',
						dataIndex: 'TOTALDEBIT',
						renderer: function(v, params, record) 
						{
							return formatCurrencyDec(record.data.TOTALDEBIT);
						},	
						width :80,
						filter: {}
					}, 
										{
						id: 'ColKAmount',
						header: "Kredit",
						align:'right',
						dataIndex: 'TOTALKREDIT',
						renderer: function(v, params, record) 
						{
							return formatCurrencyDec(record.data.TOTALKREDIT);
						},	
						width :80,
						filter: {}
					}
				]
			),

			//plugins: chkApprove_AdjustApForm,
			tbar: 
			[		
				{
					id: 'btnAdd_AdjustApForm',
					text: 'Add Data',
					tooltip: 'Edit Data',
					iconCls: 'AddRow',
					handler: function(sm, row, rec) 
					{ 
						LookUpForm_AdjustApForm();
					}
				},' ','-',
				{
					id: 'btnEdit_AdjustApForm',
					text: 'Edit Data',
					tooltip: 'Edit Data',
					iconCls: 'Edit_Tr',
					handler: function(sm, row, rec) 
					{ 
						if (rowSelected_AdjustApForm != undefined)
						{
							LookUpForm_AdjustApForm(rowSelected_AdjustApForm.data);
							ButtonDisabled_AdjustApForm(rowSelected_AdjustApForm.data.Type_Approve);
						}
						else
						{
							LookUpForm_AdjustApForm();
						}
					}
				},' ','-'
				,
				{
					xtype: 'checkbox',
					id: 'chkWithTgl_AdjustApForm',					
					hideLabel:true,
					checked: true,
					handler: function() 
					{
						if (this.getValue()===true)
						{
							Ext.get('dtpTglAwalFilter_AdjustApForm').dom.disabled=false;
							Ext.get('dtpTglAkhirFilter_AdjustApForm').dom.disabled=false;
							Ext.get('dtpTglAwalFilter_AdjustApForm').dom.readOnly=true;	
							Ext.get('dtpTglAkhirFilter_AdjustApForm').dom.readOnly=true;
						}
						else
						{
							Ext.get('dtpTglAwalFilter_AdjustApForm').dom.disabled=true;
							Ext.get('dtpTglAkhirFilter_AdjustApForm').dom.disabled=true;							
						};
					}
				}
				, ' ','-','Tanggal : ', ' ',
				{
					xtype: 'datefield',
					fieldLabel: 'Dari Tanggal : ',
					id: 'dtpTglAwalFilter_AdjustApForm',
					format: 'd/M/Y',
					value:now_AdjustApForm,
					width:100,
					onInit: function() { }
				}, ' ', ' s/d ',' ', {
					xtype: 'datefield',
					fieldLabel: 'Sd /',
					id: 'dtpTglAkhirFilter_AdjustApForm',
					format: 'd/M/Y',
					value:now_AdjustApForm,
					width:100
				},' ','->',
				{
					id: 'btnFind_AdjustApForm',
					text: ' Find',
					tooltip: 'Find Record',
					iconCls: 'find',
					handler: function(sm, row, rec) 
					{ 
						fnFindDlg_AdjustApForm();	
					}
				}
			],
			bbar:new WebApp.PaggingBar
			(
				{
					displayInfo: true,
					store: dsTRList_AdjustApForm,
					pageSize: 50,
					displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
					emptyMsg: "Tidak ada record&nbsp;&nbsp;"		
				}
			),
			viewConfig: {forceFit: true} 			
		}
	);


	var FormTR_AdjustApForm = new Ext.Panel
	(
		{
			id: mod_id,
			closable:true,
			region: 'center',
			layout: 'form',
			title: NamaForm_AdjustApForm, 
			border: false,           
			shadhow: true,
			iconCls: 'Penerimaan',
			 margins:'0 5 5 0',
			items: [grListTR_AdjustApForm],
			tbar: 
			[
				'Nomor : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: 'No : ',
					id: 'txtNoFilter_AdjustApForm',                   
					//anchor: '25%',
					width:120,
					listeners:
					{ 
						'specialkey' : function()
						{
							if (Ext.EventObject.getKey() === 13) 
							{
								RefreshDataFilter_AdjustApForm(false);					
							} 						
						}
					},
					onInit: function() { }
				}, 
				{ xtype: 'tbseparator' },
				{
					xtype: 'checkbox',
					id: 'chkFilterApproved_AdjustApForm',
					boxLabel: 'Approved'
				}, ' ','-',
					//'Maks.Data : ', 
					' ',mComboMaksData_AdjustApForm(),
					' ','->',
				{					
					xtype: 'button',
					tooltip: 'Tampilkan',
					iconCls: 'refresh',
					text: 'Tampilkan',
					anchor: '25%',
					handler: function(sm, row, rec) 
					{
						RefreshDataFilter_AdjustApForm(false);
					}
				}
			],
			listeners: 
			{ 
				'afterrender': function() 
				{           
					RefreshDataFilter_AdjustApForm(true);
					//RefreshDataFilter_AdjustApForm(true);				 
				}
			}
		}
	);
	return FormTR_AdjustApForm

};
    // end function get panel main data
 
 //---------------------------------------------------------------------------------------///
   
   
function LookUpForm_AdjustApForm(rowdata)
{
	var lebar=750;//735;
	TRLookUps_AdjustApForm = new Ext.Window
	(
		{
			id: 'LookUpForm_AdjustApForm',
			title: NamaForm_AdjustApForm,
			closeAction: 'destroy',
			width: lebar,
			height: 450, 
			border: false,
			resizable:false,
			plain: true,
			layout: 'fit',
			iconCls: 'Penerimaan',
			modal: true,
			items: getFormEntryTR_AdjustApForm(lebar),
			listeners:
			{
				activate: function() 
				{
					
				},
				afterShow: function() 
				{ 
					this.activate(); 
				},
				deactivate: function()
				{
					// rowSelected_AdjustApForm=undefined;
					 RefreshDataFilter_AdjustApForm(true);
				}
			}
		}
	);
	
	TRLookUps_AdjustApForm.show();
	if (rowdata == undefined)
	{
		AddNew_AdjustApForm();
	}
	else
	{
		DataInit_AdjustApForm(rowdata)
		CalcTotalCr_AdjustApForm();
		CalcTotalDb_AdjustApForm();	
		AutoBalance_AdjustApForm();
	}	
	
};
   
function getFormEntryTR_AdjustApForm(lebar) 
{
	var pnlTR_AdjustApForm = new Ext.FormPanel
	(
		{
			id: 'pnlTR_AdjustApForm',
			fileUpload: true,
			region: 'north',
			layout: 'fit',
			bodyStyle: 'padding:10px 10px 10px 10px',			
			anchor: '100%', 
			width:lebar,
			border: false,
			items: [getItemPanelInput_AdjustApForm(lebar)],
			tbar: 
			[
				{
					text: 'Tambah',
					id:'btnTambah',
					tooltip: 'Tambah Record Baru ',
					iconCls: 'add',
					//handler: function() { TRDataAddNew_AdjustApForm(pnlTR_AdjustApForm) }
					handler: function() 
					{ 
						AddNew_AdjustApForm();
						ButtonDisabled_AdjustApForm(false);
					}
				}, '-', 
				{
					text: 'Simpan',
					id:'btnSimpan_AdjustApForm',
					tooltip: 'Rekam Data ',
					iconCls: 'save',
					//handler: function() { TRDatasave_AdjustApForm(pnlTR_AdjustApForm) }
					handler: function() 
					{ 
						CalcTotalDb_AdjustApForm();
						CalcTotalCr_AdjustApForm();			       		
						Datasave_AdjustApForm(false);
						RefreshDataFilter_AdjustApForm(false);
					}
				}, '-', 
				{
					text: 'Simpan & Keluar',
					id:'btnSimpanKeluar_AdjustApForm',
					tooltip: 'Rekam Data & Keluar ',
					iconCls: 'saveexit',
					//handler: function() { TRDatasave_AdjustApForm(pnlTR_AdjustApForm) }
					handler: function() 
					{ 
						CalcTotalDb_AdjustApForm();
						CalcTotalCr_AdjustApForm();	
						Datasave_AdjustApForm(true);
						RefreshDataFilter_AdjustApForm(false);
						TRLookUps_AdjustApForm.close();
					}
				}, '-', 
				{
					text: 'Hapus',
					id:'btnHapus_AdjustApForm',
					tooltip: 'Remove the selected item',
					iconCls: 'remove',
					handler: function() 
					{ 
						Ext.Msg.show
						(
							{
							   title:'Hapus',
							   msg: 'Apakah transaksi ini akan dihapus ?', 
							   buttons: Ext.MessageBox.YESNO,
							   fn: function (btn) 
							   {			
								   if (btn =='yes') 
									{
										CalcTotalDb_AdjustApForm();
										CalcTotalCr_AdjustApForm();	
										DataDelete_AdjustApForm();
										RefreshDataFilter_AdjustApForm(false);
									} 
							   },
							   icon: Ext.MessageBox.QUESTION
							}
						);
					}
				}, //'-', 
				{
					text: 'Lookup',
					id:'btnLookup',
					tooltip: 'Lookup Account',
					iconCls: 'find',
					hidden:true,
					handler: function() 
					{

						if (focusRef_AdjustApForm == "ref")
						{
							focusRef_AdjustApForm="";
							var StrKriteria;
							StrKriteria = " WHERE APP_SP3D = 1 and SUBSTRING(AC.APA_NUMBER,0,5)='BKBS'"		
							var p = new mRecord_AdjustApForm
							(
								{
									account: '',
									name: '',
									description: Ext.getCmp('txtCatatan_AdjustApForm').getValue(),
									value: 0,								
									line:'',
									debit:0,
									kredit:0
								}
							);							
							FormLookupsp3d(dsDTLTRList_AdjustApForm,p, StrKriteria,nASALFORM_NONMHS);
						}
						else
						{
							var p = new mRecord_AdjustApForm
							(
								{
									account: '',
									name: '',
									description: Ext.getCmp('txtCatatan_AdjustApForm').getValue(),
									value: 0,								
									line:'',
									debit:0,
									kredit:0
								}
							);
							FormLookupAccount(" Where A.TYPE ='D'  ",dsDTLTRList_AdjustApForm,p,true,'',true);
						}
					}
				}, '-',
				{
					
				    text: 'Approve',
					id:'btnApprove_AdjustApForm',
				    tooltip: 'Approve',
				    iconCls: 'approve',
				    handler: function() 
					{ 
						if (ValidasiApprove_AdjustApForm('Approve')==1)
						{
							
								FormApproveAdjust(getTotalDebDetailGrid_AdjustApForm(),'14',Ext.get('txtNo_AdjustApForm').getValue(),
								Ext.get('txtCatatan_AdjustApForm').dom.value,Ext.get('dtpTanggal_AdjustApForm').dom.value,selectCboEntryVendor_AdjustApForm)
						}
					}
				}
				, '-', '->', '-',
				{
					text: 'Cetak',
					tooltip: 'Print',
					iconCls: 'print',
					// hidden:true,
					handler: function() {Cetak_AdjustApForm()}
				}
			]
		}
	);  // end Head panel
	
	var GDtabDetail_AdjustApForm = new Ext.TabPanel
	(
		{
			region: 'center',
			id: 'GDtabDetail_AdjustApForm',
			activeTab: 0,
			//anchor: '100% 46%',
			anchor: '100% 47%',
			border: false,
			plain: true,
			defaults: 
			{
				autoScroll: false
			},
			items:GetDTLTRGrid_AdjustApForm(),			
			tbar: 
			[
			{
				text: 'Tambah Baris',
				id:'btnTambahBaris_AdjustApForm',
				tooltip: 'Tambah Record Baru ',
				iconCls: 'add',				
				handler: function() 
				{ 
					if(Ext.getCmp('ChkApproveEntry_AdjustApForm').getValue() == true || Ext.getCmp('ChkApproveEntry_AdjustApForm').getValue() == 'true'){
						ShowPesanError_AdjustApForm('Adjust hutang sudah di approve, tambah baris tidak dapat dilakukan!','Error');
					} else{
						if(ValidasiTambahbaris_AdjustApForm('Tambah baris')==1)
						{
							TambahBaris_AdjustApForm();	
						}
					}
				}
			}, '-', 
			{
				text: 'Hapus Baris',
				id:'btnHapusBaris_AdjustApForm',
				tooltip: 'Remove the selected item',
				iconCls: 'remove',
				handler: function()
					{
						if (dsDTLTRList_AdjustApForm.getCount() > 0 )
						{						
							if (cellSelectedDet_AdjustApForm != undefined)
							{
								if(CurrentTR_AdjustApForm != undefined)
								{
									HapusBaris_AdjustApForm();
								}
							}
							else
							{
								ShowPesanWarning_AdjustApForm('Silahkan pilih dahulu baris yang akan dihapus','Hapus baris');
							}
						}
					}
			},
			{ xtype: 'tbseparator' },
			{
				id:'btnBalance_AdjustApForm',
				text: ' Auto Balance',
				tooltip: 'Auto Balance',
				iconCls: 'refresh',
				handler: function() 
				{
					CalcTotalCr_AdjustApForm();
					CalcTotalDb_AdjustApForm();	
					AutoBalance_AdjustApForm();
				}
			}
		] 
	}
);
	var Total = new Ext.Panel
	(
		{
			frame: false,
			layout: 'form',
			width: 500,
			border:false,
			id:'PnlTotal_AdjustApForm',
			labelAlign:'right',
			labelWidth:50,
			style: 
			{
				'margin-top': '5.9px',
				'margin-left': '360px'
			},
			items: 
			[
				{
					xtype: 'compositefield',
					fieldLabel: 'Total',
					anchor: '100%',
					items: 
					[
						{
							xtype: 'textfield',
							id:'txtTotalDb_AdjustApForm',
							name:'txtTotalDb_AdjustApForm',
							fieldLabel: 'Total ',
							readOnly:true,
							style:
							{	
								'text-align':'right',
								'font-weight':'bold'
							},
							value:'0',
							width: 145
						},
						{
							xtype: 'textfield',
							id:'txtTotalCr_AdjustApForm',
							name:'txtTotalCr_AdjustApForm',
							// fieldLabel: 'Total ',
							readOnly:true,
							style:
							{	
								'text-align':'right',
								'font-weight':'bold'
							},
							value:'0',
							width: 145
						}
					]
				}
			]
		}
	);
	var Formload_AdjustApForm = new Ext.Panel
	(
		{
			id: 'Formload_AdjustApForm',
			region: 'center',
			width:'100%',
			anchor:'100%',
			layout: 'form',
			title: '',
			bodyStyle: 'padding:15px',
			border: true,
			bodyStyle: 'background:#FFFFFF;',
			shadhow: true,
			//iconCls: 'GL',
			items: [pnlTR_AdjustApForm, GDtabDetail_AdjustApForm,Total]							  

		}
	);
	
	return Formload_AdjustApForm						
};
 //---------------------------------------------------------------------------------------///
 
function Cetak_AdjustApForm()
{
	var strKriteria;
	if (Ext.get('txtNo_AdjustApForm').dom.value !='' )
	{
		// strKriteria = Ext.get('txtNo_AdjustApForm').dom.value + '##'; //01
		// strKriteria +=ShowDate(Ext.getCmp('dtpTanggal_AdjustApForm').getValue()) + '##'; //04
		// strKriteria += Ext.getCmp('txtTotalDb_AdjustApForm').getValue() + '##'; //05
		// strKriteria += '##'+1+'##';	

		strKriteria = Ext.get('txtNo_AdjustApForm').dom.value + '##'; //01
		strKriteria += 1+'##';
		strKriteria += Ext.get('cboVendorEntry_AdjustApForm').dom.value + '##'; //02
		strKriteria += Ext.getCmp('txtTotalDb_AdjustApForm').getValue() + '##'; //03
		strKriteria += '-' +'##'; //04
		strKriteria +=ShowDate(Ext.getCmp('dtpTanggal_AdjustApForm').getValue()) + '##'; //05
		ShowReport('', '010521', strKriteria);	
		
	};
};
 
 
function GetDTLTRGrid_AdjustApForm() 
{
	// grid untuk detail transaksi	
	var fldDetail =
	// ['APA_NUMBER','APA_DATE','APAD_LINE','Account','Description','Value','IsDebit','POSTED','Name','Debit','Kredit']
	['apa_number','apa_date','line','account','description','value','isdebit','posted','name','debit','kredit']
	
	dsDTLTRList_AdjustApForm = new WebApp.DataStore({ fields: fldDetail })
	dsTmp_AdjustApForm= new WebApp.DataStore({ fields: fldDetail })
	
	gridDTLTR_AdjustApForm = new Ext.grid.EditorGridPanel
	(
		{
			title: 'Dari Detail ' + NamaForm_AdjustApForm,
			stripeRows: true,
			id:'Dttlgrid_AdjustApForm',
			store: dsDTLTRList_AdjustApForm,
			border: false,
			columnLines:true,
			frame:true,
			anchor: '100% 100%',			
			height	:90,
			sm: new Ext.grid.CellSelectionModel
			(
				{
					singleSelect: true,
					listeners: 
					{
						cellselect: function(sm, row, rec)
						{
							cellSelectedDet_AdjustApForm =dsDTLTRList_AdjustApForm.getAt(row);
							CurrentTR_AdjustApForm.row = row;
							//CurrentTRGL.data = cellSelectedGLDet;
						}
					}
				}
			),
			cm: TRDetailColumModel_AdjustApForm()
			, viewConfig: 
			{
				forceFit: true
			},
			listeners:
			{
				'afterrender': function()
				{
					this.store.on(
						"load", function()
						{
							getTotalKreDebDetail_AdjustApForm();
						}
					);
					this.store.on(
						"datachanged", function()
						{
							getTotalKreDebDetail_AdjustApForm();
						}
					);
				}
			}
		}
	);
			
	return gridDTLTR_AdjustApForm;
};

function RefreshDataDetail_AdjustApForm(no,tgl)
{	
    /* var strKriteria_AdjustApForm
    strKriteria_AdjustApForm = "where ART.APA_NUMBER ='"+ no +"' and ART.APA_DATE='"+ ShowDate(tgl) + "' ";
	dsDTLTRList_AdjustApForm.load
	(
		{ 
			params: 
			{ 	
				Skip: 0,
				Take: 20,
				Sort: '',
				Sortdir: 'ASC',
				target:'ViewAP_ADJUST_DETAIL',
				param: strKriteria_AdjustApForm
			}
		}
	);
	return dsDTLTRList_AdjustApForm; */
	Ext.Ajax.request({
		url: baseURL + "index.php/keuangan/functionAdjustHutang/getListDetailAccount",
		params: {
			apa_number : no,
			apa_date : tgl,
		},
		success: function(o) {
			var cst = Ext.decode(o.responseText);
			if (cst.success === true) 
			{
				dsDTLTRList_AdjustApForm.removeAll();
				var recs=[],
					recType=dsDTLTRList_AdjustApForm.recordType;
				for(var i=0; i<cst.listData.length; i++){
					recs.push(new recType(cst.listData[i]));						
				}
				dsDTLTRList_AdjustApForm.add(recs);
				gridDTLTR_AdjustApForm.getView().refresh();
				
				CalcTotalCr_AdjustApForm();
				CalcTotalDb_AdjustApForm();	
			}
			else {
				ShowPesanError_AdjustApForm('Gagal menampilkan list penerimaan piutang!', 'Simpan Data');
			}
		}
	})
}

// function RefreshDataDetailAkdAdj_AdjustApForm(no,tgl)
// {	
//     var strKriteria_AdjustApForm
//     strKriteria_AdjustApForm = "where CSAP.APA_NUMBER ='"+ no +"' and CSAP.APA_DATE='"+ tgl + "' ";
// 	dsDTLTRListAkdAjd_AdjustApForm.load
// 	(
// 		{ 
// 			params: 
// 			{ 	
// 				Skip: 0,
// 				Take: 20,
// 				Sort: 'APA_NUMBER',
// 				Sortdir: 'ASC',
// 				target:'ViewAPOHistory',
// 				param: strKriteria_AdjustApForm
// 			}
// 		}
// 	);
// 	return dsDTLTRListAkdAjd_AdjustApForm;
// }

//---------------------------------------------------------------------------------------///
			
function TRDetailColumModel_AdjustApForm() 
{
	return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(), 
			{
				id: 'Account_AdjustApForm',
				name: 'Account_AdjustApForm',
				header: "Account",
				dataIndex: 'account',
				sortable: false,
				anchor: '10%',
				editor: new Ext.form.TextField
				(
					{
						id:'fieldAcc_AdjustApForm',
						allowBlank: false,
						enableKeyEvents : true,
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									LookUpAccount_APEntry(" Where A.Account like '" + Ext.get('fieldAcc_AdjustApForm').dom.value + "%'  AND A.TYPE ='D'  ");
								} 
							}
						}
					}
				),
				width: 70
			}, 
			{
				id: 'Name_AdjustApForm',
				name: 'Name_AdjustApForm',
				header: "Nama Account",
				dataIndex: 'name',
				anchor: '30%',
				editor: new Ext.form.TextField
				(
					{
						id:'fieldAccName_AdjustApForm',
						allowBlank: false,
						enableKeyEvents : true,
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									LookUpAccount_APEntry(" Where A.Account like '" + Ext.get('fieldAcc_AdjustApForm').dom.value + "%'  AND A.TYPE ='D'  ");
									
								} 
							}
						}
					}
				)
			}, 
			{
				id: 'DESC_AdjustApForm',
				name: 'DESC_AdjustApForm',
				header: "Keterangan",
				anchor: '30%',
				dataIndex: 'description', 
				editor: new Ext.form.TextField
				(	
					{
						allowBlank: true
					}
				)
			}, 
			{
				dataIndex: 'debit',
				header: 'Debit',
				sortable: true,
				width: 100,
				align: 'right',
				editor: new Ext.form.NumberField
				(
					{
						id:'fnumDebEntry_AdjustApForm',
						allowBlank: false,
						enableKeyEvents : true,
						listeners: 
						{ 
							keyDown: function(a,b,c){
								var line	= gridDTLTR_AdjustApForm.getSelectionModel().selection.cell[0];
								var o=dsDTLTRList_AdjustApForm.getRange()[line].data;
								if(b.getKey()==13){
									if(o.account == '' || o.account == undefined){
										Ext.Msg.show({
											title: 'WARNING',
											msg: 'Account masih kosong, isi account terlebih dahulu!',
											buttons: Ext.MessageBox.OK,
											fn: function (btn) {
												if (btn == 'ok')
												{
													o.debit = 0;
													gridDTLTR_AdjustApForm.getView().refresh();
													gridDTLTR_AdjustApForm.startEditing(line, 4);
												}
											}
										});
									} else{										
										onecolumnentry_AdjustApForm(true);	
										CalcTotalCr_AdjustApForm();
										CalcTotalDb_AdjustApForm();
										gridDTLTR_AdjustApForm.startEditing(line, 5);
									}
								} else{
									onecolumnentry_AdjustApForm(true);	
									CalcTotalCr_AdjustApForm();
									CalcTotalDb_AdjustApForm();
								}
							}
							
						}
					}
				),
				renderer: function(v, params, record) 
				{
					return formatCurrencyDec(record.data.debit);
				}
			},{
				dataIndex: 'kredit',
				header: 'Kredit',
				sortable: true,
				width: 100,
				align: 'right',
				editor: new Ext.form.NumberField
				(
					{
						id:'fnumKreEntry_AdjustApForm',
						allowBlank: false,
						enableKeyEvents : true,
						listeners: 
						{ 
							keyDown: function(a,b,c){
								// var line = dsDTLTRList_AdjustArForm.getCount()-1; 
								var line	= gridDTLTR_AdjustApForm.getSelectionModel().selection.cell[0];
								var o=dsDTLTRList_AdjustApForm.getRange()[line].data;
									if(b.getKey()==13){
										if(o.account == '' || o.account == undefined){
											Ext.Msg.show({
												title: 'WARNING',
												msg: 'Account masih kosong, isi account terlebih dahulu!',
												buttons: Ext.MessageBox.OK,
												fn: function (btn) {
													if (btn == 'ok')
													{
														o.debit = 0;
														gridDTLTR_AdjustApForm.getView().refresh();
														gridDTLTR_AdjustApForm.startEditing(line, 4);
													}
												}
											});
										} else{	
											if(((o.debit == 0 || o.debit == undefined) && (o.kredit != 0 || o.kredit != undefined )) || ((o.debit != 0 || o.debit != undefined) && (o.kredit == 0 || o.kredit == undefined ))){
												onecolumnentry_AdjustApForm(true);	
												CalcTotalCr_AdjustApForm();
												CalcTotalDb_AdjustApForm();
												if(ValidasiTambahbaris_AdjustApForm('Tambah baris')==1)
												{
													TambahBaris_AdjustApForm();
												}
											} else{
												ShowPesanWarning_AdjustApForm('Isi salah satu, debit atau kredit untuk melanjutkan!','Warning')
											}
										}
									} else{
										onecolumnentry_AdjustApForm(true);	
										CalcTotalCr_AdjustApForm();
										CalcTotalDb_AdjustApForm();
									}
							}
							
						}
					}
				),
				renderer: function(v, params, record) 
				{
					return formatCurrencyDec(record.data.kredit);
				}
			}
		]
	)
};

//---------------------------------------------------------------------------------------///
function DataInit_AdjustApForm(rowdata) 
{
	DataAddNew_AdjustApForm = false;
	Ext.getCmp('btnApprove_AdjustApForm').enable();
	Ext.get('txtNo_AdjustApForm').dom.readOnly=true;
	selectCboEntryVendor_AdjustApForm = rowdata.VEND_CODE;
	Ext.get('cboVendorEntry_AdjustApForm').dom.value = rowdata.VENDOR;
    Ext.get('txtNo_AdjustApForm').dom.value = rowdata.APA_NUMBER;
	Ext.get('dtpTanggal_AdjustApForm').dom.value = ShowDate(rowdata.APA_DATE);   
	Ext.get('dtpTanggalDueday_AdjustApForm').dom.value = ShowDate(rowdata.DUE_DATE);   
	Ext.get('txtCatatan_AdjustApForm').dom.value = rowdata.NOTES;
	if (rowdata.NO_TAG != null){
		Ext.get('txtNoReff_AdjustApForm').dom.value = rowdata.NO_TAG;
	}else{		
		Ext.get('txtNoReff_AdjustApForm').dom.value ='';
	}
	
	Ext.getCmp('ChkApproveEntry_AdjustApForm').setValue(rowdata.POSTED);
	if(rowdata.Type == 0)
	{
		Ext.getCmp('radCrEntry_AdjustApForm').setValue(true);
		Ext.getCmp('radDbEntry_AdjustApForm').setValue(false);
	}
	else
	{
		Ext.getCmp('radCrEntry_AdjustApForm').setValue(false);
		Ext.getCmp('radDbEntry_AdjustApForm').setValue(true);
	}
	radTipe_AdjustApForm = rowdata.TYPE;
	RefreshDataDetail_AdjustApForm(rowdata.APA_NUMBER,rowdata.APA_DATE);
	GetVendorAcc_AdjustApForm(rowdata.VEND_CODE);

	Ext.getCmp('radCrEntry_AdjustApForm').setDisabled(true);
	Ext.getCmp('radDbEntry_AdjustApForm').setDisabled(true);
	if(rowdata.POSTED===true || rowdata.POSTED==='t')
	{
		Ext.getCmp('btnApprove_AdjustApForm').disable();
		Ext.getCmp('btnSimpanKeluar_AdjustApForm').disable();
		Ext.getCmp('btnHapus_AdjustApForm').disable();
		Ext.getCmp('btnSimpan_AdjustApForm').disable();
					
	} else{
		Ext.getCmp('btnApprove_AdjustApForm').enable();
		Ext.getCmp('btnSimpanKeluar_AdjustApForm').enable();
		Ext.getCmp('btnHapus_AdjustApForm').enable();
		Ext.getCmp('btnSimpan_AdjustApForm').enable();
	}
	
	GetAkunKas_AdjustApForm('CBAP');
	
	
};

//---------------------------------------------------------------------------------------///
function AddNew_AdjustApForm() 
{
	DataAddNew_AdjustApForm = true;	
	now_AdjustApForm= new Date;	
	Ext.getCmp('btnApprove_AdjustApForm').disable();
	Ext.get('txtNo_AdjustApForm').dom.readOnly=false;	
	Ext.get('txtNo_AdjustApForm').dom.value = '';	
	Ext.getCmp('ChkApproveEntry_AdjustApForm').setValue(false);	
	Ext.get('txtNoReff_AdjustApForm').dom.value = '';
	Ext.get('txtTotalDb_AdjustApForm').dom.value = '0';
	Ext.get('txtTotalCr_AdjustApForm').dom.value = '0';
	Ext.get('txtCatatan_AdjustApForm').dom.value = 'Summary from cashier post : ADJUST AP';
	Ext.get('dtpTanggal_AdjustApForm').dom.value = ShowDateAkuntansi(now_AdjustApForm);
	Ext.get('dtpTanggalDueday_AdjustApForm').dom.value = ShowDateAkuntansi(now_AdjustApForm);
	Ext.get('cboVendorEntry_AdjustApForm').dom.value = '';
	Ext.getCmp('cboVendorEntry_AdjustApForm').setValue('');
	selectCboEntryVendor_AdjustApForm = '';
	Ext.getCmp('radDbEntry_AdjustApForm').setValue(false);
	Ext.getCmp('radCrEntry_AdjustApForm').setValue(true);

	dsDTLTRList_AdjustApForm.removeAll();
	rowSelected_AdjustApForm=undefined;	
	isDb_AdjustApForm=true;
	isCr_AdjustApForm=true;
	// Ext.getCmp('radCrEntry_AdjustApForm').setDisabled(false);
	// Ext.getCmp('radDbEntry_AdjustApForm').setDisabled(false);
	GetAkunKas_AdjustApForm('CBAP');
	Ext.getCmp('btnApprove_AdjustApForm').setDisabled(false);
	Ext.getCmp('btnApprove_AdjustApForm').enable();
	Ext.getCmp('btnSimpanKeluar_AdjustApForm').enable();
	Ext.getCmp('btnHapus_AdjustApForm').enable();
	Ext.getCmp('btnSimpan_AdjustApForm').enable();
	//Ext.getCmp('fnumKreEntry_AdjustApForm').setDisabled(false);
	//Ext.getCmp('fnumDebEntry_AdjustApForm').setDisabled(false);
};
function getTotalDebDetailGrid_AdjustApForm()
{
	var total = 0; // [0] -> Kredit, [1] -> debit
	var debitRow = 0;
	//var kreditRow = 0;
	if(dsDTLTRList_AdjustApForm.getCount() > 0)
	{
		for(var i = 0;i < dsDTLTRList_AdjustApForm.getCount();i++)
		{
			debitRow = dsDTLTRList_AdjustApForm.data.items[i].data.debit;
				total= total + parseFloat(debitRow);
		}
	}
	
	return total;
}
function getTotalCreDetailGrid_AdjustApForm()
{
	var total = 0; // [0] -> Kredit, [1] -> debit
	var debitRow = 0;
	//var kreditRow = 0;
	if(dsDTLTRList_AdjustApForm.getCount() > 0)
	{
		for(var i = 0;i < dsDTLTRList_AdjustApForm.getCount();i++)
		{
			debitRow = dsDTLTRList_AdjustApForm.data.items[i].data.kredit;
				total= total + parseFloat(debitRow);
		}
	}
	
	return total;
}
function getParam_AdjustApForm() 
{
	var params = 
	{
		apa_number: Ext.getCmp('txtNo_AdjustApForm').getValue(),
		apa_date: Ext.get('dtpTanggal_AdjustApForm').getValue(),
		due_date: Ext.get('dtpTanggalDueday_AdjustApForm').getValue(),
		vend_code: selectCboEntryVendor_AdjustApForm,
		type: radTipe_AdjustApForm,
		amount:getTotalDebDetailGrid_AdjustApForm(),//getAmount_AdjustArForm(Ext.getCmp('txtTotalDb_AdjustArForm').getValue()),
		notes: Ext.getCmp('txtCatatan_AdjustApForm').getValue(),
	};
	
	params['jumlah']=dsDTLTRList_AdjustApForm.getCount();
	for(var i = 0 ; i < dsDTLTRList_AdjustApForm.getCount();i++)
	{
		params['account-'+i]=dsDTLTRList_AdjustApForm.data.items[i].data.account;
		params['description-'+i]=dsDTLTRList_AdjustApForm.data.items[i].data.description;
		params['line-'+i]=dsDTLTRList_AdjustApForm.data.items[i].data.line;
		if((dsDTLTRList_AdjustApForm.data.items[i].data.debit != 0 || dsDTLTRList_AdjustApForm.data.items[i].data.debit != undefined) && (dsDTLTRList_AdjustApForm.data.items[i].data.kredit == 0 || dsDTLTRList_AdjustApForm.data.items[i].data.kredit == undefined)){
			params['value-'+i]=dsDTLTRList_AdjustApForm.data.items[i].data.debit;
			params['isdebit-'+i]=true;
		} else{
			params['value-'+i]=dsDTLTRList_AdjustApForm.data.items[i].data.kredit;
			params['isdebit-'+i]=false;
		}
	}
	/* var params = 
	{
		Table:'viACC_AP_ADJUST',
		APA_NUMBER: Ext.getCmp('txtNo_AdjustApForm').getValue(),
		APA_DATE: ShowDate(Ext.getCmp('dtpTanggal_AdjustApForm').getValue()),
		DUEDATE: ShowDate(Ext.getCmp('dtpTanggalDueday_AdjustApForm').getValue()),
		VEND_CODE: selectCboEntryVendor_AdjustApForm,
		TYPE: radTipe_AdjustApForm,
		AMOUNT:getAmount_AdjustApForm(Ext.getCmp('txtTotalDb_AdjustApForm').getValue()),
		NO_TAG: '',
		DATE_TAG: '',
		NOTES: Ext.getCmp('txtCatatan_AdjustApForm').getValue(),
		IS_APPROVE:0,
		IS_DETAIL:0,
		POSTED:0,
		List:getArrDetail_AdjustApForm(),
		JmlField: 7,
		JmlList: dsDTLTRList_AdjustApForm.getCount()
	}; */
	return params
};

//---------------------------------------------------------------------------------------///

function getAmount_AdjustApForm(dblNilai)
{
    var dblAmount;
    dblAmount = dblNilai.replace('Rp.', '')
    for (var i = 0; i < dblAmount.length; i++) {
        var x = dblAmount.substr(i, 1)
        if (x === '.') {
            dblAmount = dblAmount.replace('.', '');
        }
    }    
    //return Ext.num(dblAmount)
    return dblAmount.replace(',', '.')
};

function GetAngka_AdjustApForm(str,ket)
{
	for (var i = 0; i < str.length; i++) 
	{
		var y = str.substr(i, 1)
		if (y === '.') 
		{
			str = str.replace('.', '');
		}
	};
	
	return str;
};
function getItemPanelInput_AdjustApForm(lebar)
{
	var items = 
	{
		layout:'fit',
		anchor:'100%',
		width: lebar - 36,
		height: 140,//130,
		labelAlign: 'right',
		bodyStyle: 'padding:7px 7px 7px 7px',
		items:
		[
			{
				columnWidth:.9,
				width:lebar-36,
				layout: 'form',
				border:false,
				items: 
				[
					getItemPanelNo_AdjustApForm(lebar),
					getItemPanelTerimaDari_AdjustApForm(lebar),
					getItemPanelCatatan_AdjustApForm(lebar),
					getItemPanelAktivaLancar_AdjustApForm(lebar),
				]
			}
		]
	};
	return items;			
};

function getItemPanelAktivaLancar_AdjustApForm(lebar)
{
	var items = 			
	{
	layout:'column',
	border:false,
	width:lebar-36,
	items:
		[
			{
				columnWidth:.5,
				layout: 'form',
				border:false,
				hidden:true,
				labelWidth:111,
				items: 
				[
					{
						xtype:'textfield',
						fieldLabel: 'Referensi ',
						name: 'txtNoReff_AdjustApForm',
						id: 'txtNoReff_AdjustApForm',
						anchor:'96%',
						readOnly:true					
					}
				]
			},
			{
				columnWidth:.5,
				region:'Right',
				border:false,				
				layout: 'form',
				items: 				
				[

					{
						xtype: 'compositefield',
						fieldLabel: 'Tipe',
						anchor: '100%',
						items: 
						[
							{
								xtype: 'radiogroup',
								id: 'rgTipeTransEntry_AdjustApForm',
								columns: 2,
								width: 120,
								items:
								[
									{
										boxLabel: 'Debit',
										autoWidth: true,
										inputValue: 1,
										name: 'radTipeTransEntry_AdjustApForm',
										id: 'radDbEntry_AdjustApForm'
									},
									{											
										boxLabel: 'Kredit',
										autoWidth: true,
										inputValue: 0,
										name: 'radTipeTransEntry_AdjustApForm',
										id: 'radCrEntry_AdjustApForm'
									}
									
								],
								listeners:
								{
									'change': function(rg, rc)
									{
										radTipe_AdjustApForm = rc.getId().toString() == "radCrEntry_AdjustApForm" ? 0 : 1;
										dsDTLTRList_AdjustApForm.removeAll();
									}
								}
							},	
						]
					}
				]
			},
			{
				columnWidth:.2,
				region:'Right',
				border:false,				
				layout: 'form',
				items: 				
				[
					{
						xtype:'checkbox',
						fieldLabel: 'Posted',
						name: 'ChkApproveEntry_AdjustApForm',
						id: 'ChkApproveEntry_AdjustApForm',
						disabled:true,
						anchor:'95%'
					}
				]
			}
		]
	}
	return items;	
}; 

function getItemPanelCatatan_AdjustApForm(lebar)
{
	var items = 			
	{
	layout:'column',
	border:false,
	width:lebar-36,
	items:
		[
			{
				columnWidth:0.98,
				layout: 'form',
				border:false,
				labelWidth:111,
				items: 
				[
					{
						xtype:'textarea',
						fieldLabel: 'Keterangan ',
						name: 'txtCatatan_AdjustApForm',
						id: 'txtCatatan_AdjustApForm',
						anchor:'99.9%',
						height: 39,
						value:'Summary from cashier post : ADJUST AP',
						autoCreate: {tag: 'input', maxlength: '255'}
					}
				]
			}
		]
	}
	return items;	
}; 

function getItemPanelNo_AdjustApForm(lebar)
{
	var items = 			
	{
	layout:'column',
	border:false,
	width:lebar-36,
	items:
		[
			{
				columnWidth:.5,
				layout: 'form',
				border:false,
				labelWidth:111,
				items: 
				[
					{
						xtype:'textfield',
						fieldLabel: 'Nomor',
						name: 'txtNo_AdjustApForm',
						id: 'txtNo_AdjustApForm',
						readOnly:false,
						anchor:'95%',
						listeners: 
						{
							'blur' : function()
							{									
								//alert('Lost Focus');//CalcTotal_PenerimaanNonMhs(CurrentTR_PenerimaanNonMhs.row);									
							},
						}
					}
				]
			},
			{
				columnWidth:.5,
				layout: 'form',
				border:false,
				labelWidth:100,
				items: 
				[
					{
						xtype: 'datefield',
                        fieldLabel: 'Tanggal ',
                        id: 'dtpTanggal_AdjustApForm',
                        name: 'dtpTanggal_AdjustApForm',
                        format: 'd/M/Y',
						value:now_AdjustApForm,
                        anchor: '70%'
					}
				]
			}
		]
	}
	return items;	
};

function getItemPanelTerimaDari_AdjustApForm(lebar)
{
	var items = 			
	{
	layout:'column',
	width:lebar-36,
	border:false,
	items:
		[
			{
				columnWidth:.5,
				layout: 'form',
				border:false,
				labelWidth:111,
				items: 
				[
					{
						xtype: 'compositefield',
						fieldLabel: 'Vendor',
						anchor: '100%',
						items: 
						[
							mCboEntryVendorAdjustApForm(),
							{
								xtype: 'button',
								id: 'btnAddVendor_AdjustApForm',
								iconCls: 'add',
								handler: function()
								{
									SetAddVendorLookUp(Ext.getCmp('cboVendorEntry_AdjustApForm').getStore());
								}
							},
						]
					}
				]
			},
			{
				columnWidth:.5,
				layout: 'form',
				border:false,
				items: 
				[
					{
						xtype: 'datefield',
                        fieldLabel: 'Jatuh tempo ',
                        id: 'dtpTanggalDueday_AdjustApForm',
                        name: 'dtpTanggalDueday_AdjustApForm',
                        format: 'd/M/Y',
						value:now_AdjustApForm,
                        anchor: '70%'
					}
				]
			}
		]
	}
	return items;	
}

function RefreshData_AdjustApForm()
{			
	dsTRList_AdjustApForm.load
	(
		{ 
			params: 
			{ 	
				Skip: 0,
				Take: selectCount_AdjustApForm,
				Sort: 'APA_DATE',
				Sortdir: 'ASC',
				target:'viACC_AP_ADJUST',
				param: " "
			}
		}
	);
	return dsTRList_AdjustApForm;
};

function mComboMaksData_AdjustApForm()
{
  var cboMaksDataAdjustApForm = new Ext.form.ComboBox
	(
		{
			id:'cboMaksDataAdjustApForm',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Maks.Data ',			
			width:50,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[1, 50], [2, 100],[3, 200],[4, 500],[5, 1000]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCount_AdjustApForm,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectCount_AdjustApForm=b.data.displayText ;
					RefreshDataFilter_AdjustApForm(false);
				} 
			},
			hidden:true
		}
	);
	return cboMaksDataAdjustApForm;
};

function mCboEntryVendorAdjustApForm()
{
	var Fields = ['Vend_Code', 'Vendor', 'VendorName','DUE_DAY','ACCOUNT'];
    dsCboEntryVendor_AdjustApForm = new WebApp.DataStore({ fields: Fields });
    
	dsCboEntryVendor_AdjustApForm.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				Sort: 'Vendor', 
				Sortdir: 'ASC', 
				target: 'viewCboVendLengkap',
				param: ''
			} 
		}
	);
	
    var cboVendorEntry_AdjustApForm = new Ext.form.ComboBox
	(
		{
		    id: 'cboVendorEntry_AdjustApForm',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: 'Pilih vendor ...',
		    align: 'right',
		    anchor:'95%',
		    listWidth:350,
		    store: dsCboEntryVendor_AdjustApForm,
		    //fieldLabel: 'Vendor',
		    valueField: 'Vend_Code',
		    displayField: 'VendorName',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
					var strParam;
					selectCboEntryVendor_AdjustApForm = b.data.Vend_Code;
					strDue_AdjustApForm = b.data.DUE_DAY;
					GetVendorAcc_AdjustApForm(b.data.Vend_Code);

					strParam=Ext.get('dtpTanggal_AdjustApForm').getValue()+'###@###'+strDue_AdjustApForm+'###@###'
					GetDueDate_AdjustApForm(strParam)
			       // Ext.getCmp('dtpTanggalDueday_AdjustApForm').setValue(ShowDate(now_AdjustApForm.setDate(now_AdjustApForm.getDate() + b.data.DUE_DAY)));
			       
			    }
			}
		}
	);
	
	return cboVendorEntry_AdjustApForm;
}
function RefreshDataFilter_AdjustApForm(mBol) 
{   
	var strCrt_AdjustApForm='';
	// strCrt_AdjustApForm=" Where KATEGORI='" + KDkategori_AdjustApForm + "' "
	if (Ext.get('txtNoFilter_AdjustApForm').getValue() != '') 
	{
		if (strCrt_AdjustApForm ==='')
		{
			strCrt_AdjustApForm = " Where ART.APA_NUMBER like '%" + Ext.get('txtNoFilter_AdjustApForm').getValue() + "%' ";
		}
		else
		{
			strCrt_AdjustApForm += " AND ART.APA_NUMBER like '%" + Ext.get('txtNoFilter_AdjustApForm').getValue() + "%' ";
		}
	};
	
	if (Ext.getCmp('chkWithTgl_AdjustApForm').getValue() === true) 
	{
		if (strCrt_AdjustApForm ==='')
		{
			strCrt_AdjustApForm = " Where ART.APA_DATE >='" + FormatDateReport(Ext.getCmp('dtpTglAwalFilter_AdjustApForm').getValue())  + "' ";
			strCrt_AdjustApForm += " and  ART.APA_DATE <='" + FormatDateReport(Ext.getCmp('dtpTglAkhirFilter_AdjustApForm').getValue())  + "' ";
		}
		else
		{
			strCrt_AdjustApForm += " and (ART.APA_DATE >='" + FormatDateReport(Ext.getCmp('dtpTglAwalFilter_AdjustApForm').getValue())  + "' ";
			strCrt_AdjustApForm += " and  ART.APA_DATE <='" + FormatDateReport(Ext.getCmp('dtpTglAkhirFilter_AdjustApForm').getValue())  + "') ";
		}
	};	

	if(Ext.getCmp('chkFilterApproved_AdjustApForm').getValue() === true)
	{
		if (strCrt_AdjustApForm ==='')
		{
			strCrt_AdjustApForm = " Where POSTED ='t'";
		}
		else
		{
			strCrt_AdjustApForm += " AND POSTED ='t'";
		}
	}else{
		if (strCrt_AdjustApForm ==='')
		{
			strCrt_AdjustApForm = " Where POSTED ='f'";
		}
		else
		{
			strCrt_AdjustApForm += " AND POSTED ='f'";
		}
	};

    if (strCrt_AdjustApForm != undefined) 
    {  
		dsTRList_AdjustApForm.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCount_AdjustApForm, 
					Sort: 'APA_DATE', 
					Sortdir: 'ASC', 
					target:'viACC_AP_ADJUST',
					param: strCrt_AdjustApForm
				}			
			}
		);        
    }
	else
	{
	    RefreshDataFilter_AdjustApForm(true);
	}
};

function Approve_AdjustApForm(TglApprove, noApprove,NoteApprove,jml,nomor,tgl) 
{
	loadMask.show();
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/keuangan/functionGeneral/approveAP",
			params:  getParamApprove_AdjustApForm(TglApprove, noApprove, NoteApprove,jml,nomor,tgl), 
			success: function(o) 
			{				
				var cst = Ext.decode(o.responseText);
				loadMask.hide();
				if (cst.success === true)
				{
					ShowPesanInfo_AdjustApForm('Data berhasil di Approve','Approve');
					Ext.getCmp('btnApprove_AdjustApForm').disable();
					Ext.getCmp('ChkApproveEntry_AdjustApForm').setValue(true);
					
					RefreshDataFilter_AdjustApForm(false);
					dsTRListFakturAmount_ApproveAdjustAP.removeAll();								
				}
				else if (cst.success === false && cst.pesan === 0 )
				{
					ShowPesanWarning_AdjustApForm('Data tidak berhasil di Approve, ' + cst.pesan,'Edit Data');
				}
				else 
				{
					ShowPesanError_AdjustApForm('Data tidak berhasil di Approve, ' + cst.pesan,'Approve');
				}										
			}
		}
	)
}

function Datasave_AdjustApForm(IsExit) 
{
	if (ValidasiEntry_AdjustApForm('Simpan Data') == 1 )
	{
		/* if (DataAddNew_AdjustApForm == true) 
		{ */
		    Ext.Ajax.request
				(
					{
					    url: baseURL + "index.php/keuangan/functionAdjustHutang/save",
					    params: getParam_AdjustApForm(),
					    success: function(o) {
					        var cst = Ext.decode(o.responseText);
					        if (cst.success === true) 
							{
					            ShowPesanInfo_AdjustApForm('Data berhasil di simpan', 'Simpan Data');
								if (IsExit===false)
								{
									Ext.get('txtNo_AdjustApForm').dom.value = cst.apa_number;
								}
					            RefreshDataFilter_AdjustApForm(false);
								RefreshDataDetail_AdjustApForm(Ext.get('txtNo_AdjustApForm').getValue(),Ext.get('dtpTanggal_AdjustApForm').getValue());
								DataAddNew_AdjustApForm=false;
								Ext.get('txtNo_AdjustApForm').dom.readOnly=true;

					        }
					        else if (cst.success === false && cst.pesan === 0) 
							{
					            ShowPesanWarning_AdjustApForm('Data tidak berhasil di simpan ' + cst.pesan , 'Simpan Data');
					        }
					        else {
					            ShowPesanError_AdjustApForm('Data tidak berhasil di simpan, Error : ' + cst.pesan, 'Simpan Data');
					        }
					    }
					}
				)
		/* }
		else 
		{
			Ext.Ajax.request
			 (
				{
					 url: "./Datapool.mvc/UpdateDataObj",
					 params:  getParam_AdjustApForm(), 
					 success: function(o) 
					 {
						
						    var cst = Ext.decode(o.responseText);
							if (cst.success === true)
							{
								ShowPesanInfo_AdjustApForm('Data berhasil di edit','Edit Data');
								RefreshDataFilter_AdjustApForm(false);
								RefreshDataDetail_AdjustApForm(Ext.get('txtNo_AdjustApForm').getValue(),Ext.get('dtpTanggal_AdjustApForm').getValue());
							}
							else if (cst.success === false && cst.pesan === 0 )
							{
								ShowPesanWarning_AdjustApForm('Data tidak berhasil di edit ' +  cst.pesan ,'Edit Data');
							}
							else {
								ShowPesanError_AdjustApForm('Data tidak berhasil di edit '  + cst.pesan ,'Edit Data');
							}
												
					}
				}
			)
		} */
	}
};

function ValidasiEntry_AdjustApForm(modul)
{
	var x = 1;
	/* if (Ext.get('txtNo_AdjustApForm').getValue()=='')
	{
		ShowPesanWarning_AdjustApForm('Nomor belum diisi',modul);
		x=0;
	}
	else  */if(selectCboEntryVendor_AdjustApForm == '' || selectCboEntryVendor_AdjustApForm == undefined)
	{
		ShowPesanWarning_AdjustApForm('Vendor belum dipilih',modul);
		x=0;
	}
	else if (dsDTLTRList_AdjustApForm.getCount() <= 0)
	{
		ShowPesanWarning_AdjustApForm('Detai Payable AP belum di isi',modul);
		x=0;
	}else if( getAmount_AdjustApForm(Ext.get('txtTotalDb_AdjustApForm').getValue()) != getAmount_AdjustApForm(Ext.get('txtTotalCr_AdjustApForm').getValue()))
	{
		ShowPesanWarning_AdjustApForm('Detai Payable AP tidak Balanced',modul);
		x=0;
	}else if (Ext.get('txtCatatan_AdjustApForm').getValue().length > 255)
	{
		ShowPesanWarning_CSAPForm('Keterangan Tidak boleh lebih dari 255 Karakter',modul);
		x=0;
	};
	return x;
};

function ValidasiApprove_AdjustApForm(modul)
{
	var x = 1;
	if(Ext.get('txtNo_AdjustApForm').getValue() == '')
	{
		ShowPesanWarning_AdjustApForm('Data belum disimpan',modul);
		x=0;
	}else if(selectCboEntryVendor_AdjustApForm == '' || selectCboEntryVendor_AdjustApForm == undefined)
	{
		ShowPesanWarning_AdjustApForm('Vendor belum dipilih',modul);
		x=0;
	}else if ( dsDTLTRList_AdjustApForm.getCount() <= 0)
	{
		ShowPesanWarning_AdjustApForm('Detai Payable AP belum di isi',modul);
		x=0;
	}else if( getAmount_AdjustApForm(Ext.get('txtTotalDb_AdjustApForm').getValue()) != getAmount_AdjustApForm(Ext.get('txtTotalCr_AdjustApForm').getValue()))
	{
		ShowPesanWarning_AdjustApForm('Detai Payable AP tidak Balanced',modul);
		x=0;
	}
	return x;
};

function ValidasiTambahbaris_AdjustApForm(modul)
{
	var x = 1;
	if(selectCboEntryVendor_AdjustApForm == '' || selectCboEntryVendor_AdjustApForm == undefined)
	{
		ShowPesanWarning_AdjustApForm('Vendor belum dipilih',modul);
		x=0;
	}else if(dsDTLTRList_AdjustApForm.getCount() >=2)
	{
		ShowPesanWarning_AdjustApForm('data sudah ada',modul);
		x=0;
	}else if(dsDTLTRList_AdjustApForm.getCount() >=1)
	{
		if(parseFloat(Ext.getCmp('fnumDebEntry_AdjustApForm').getValue()) == 0 || parseFloat(Ext.getCmp('fnumDebEntry_AdjustApForm').getValue())==undefined)
		{
			ShowPesanWarning_AdjustApForm('nilai debit belum diinput',modul);
			x=0;
		}
	}
	return x;
};

function ShowPesanWarning_AdjustApForm(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};

function ShowPesanError_AdjustApForm(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR
		}
	);
};

function ShowPesanInfo_AdjustApForm(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO
		}
	);
};

function HapusBaris_AdjustApForm()
{
	if (cellSelectedDet_AdjustApForm.data.account != '' && cellSelectedDet_AdjustApForm.data.account != undefined)
	{
		Ext.Msg.show
		(
			{
			   title:'Hapus Baris',
			   msg: 'Apakah baris ini akan dihapus ?' + ' ' + 'Baris :'+ ' ' + (CurrentTR_AdjustApForm.row + 1) + ' dengan Account : '+ ' ' + cellSelectedDet_AdjustApForm.data.account ,
			   buttons: Ext.MessageBox.YESNO,
			   fn: function (btn) 
			   {			
				   if (btn =='yes') 
					{
						if (cellSelectedDet_AdjustApForm.data.line != '' && cellSelectedDet_AdjustApForm.data.line != undefined){
							dsDTLTRList_AdjustApForm.removeAt(CurrentTR_AdjustApForm.row);
						    CalcTotal_AdjustApForm();
							HapusBaris_AdjustApFormDB(cellSelectedDet_AdjustApForm.data.line);
						    cellSelectedDet_AdjustApForm = undefined;
						} else{
								dsDTLTRList_AdjustApForm.removeAt(CurrentTR_AdjustApForm.row);
							    CalcTotal_AdjustApForm();
							    cellSelectedDet_AdjustApForm = undefined;
						}

					} 
			   },
			   icon: Ext.MessageBox.QUESTION
			}
		);
	}
	else
	{
	    dsDTLTRList_AdjustApForm.removeAt(CurrentTR_AdjustApForm.row);
	    CalcTotal_AdjustApForm();
	    cellSelectedDet_AdjustApForm = undefined;
	}
	
	
};

function TambahBaris_AdjustApForm()
{
	var x=true;
	if (dsDTLTRList_AdjustApForm.getCount() > 0)
	{
		if(dsDTLTRList_AdjustApForm.data.items[dsDTLTRList_AdjustApForm.getCount()-1].data.account==='')
		{
			ShowPesanWarning_AdjustApForm('Vendor belum dipilih','Tambah baris');
			x=false;
		}

		if (dsDTLTRList_AdjustApForm.data.items[dsDTLTRList_AdjustApForm.getCount()-1].data.account == StrAccVend_AdjustApForm)
		{
			isDb_AdjustApForm=false
		}
	}
	if (x==true){

		if (isDb_AdjustApForm==false){
			TambahBarisKosong_AdjustApForm()
		} else{
			TambahBarisDb_AdjustApForm()
		}
		
	}
};

function TambahBarisKosong_AdjustApForm()
{
	// var p = new mRecord_AdjustApForm
	// (
		// {
			// ACCOUNT: '',//StrAccVend_AdjustApForm,
			// NAMAACCOUNT: '',//StrNmAccVend_AdjustApForm,
			// Description: Ext.get('txtCatatan_AdjustApForm').getValue(),		
			// VALUE: 0,								
			// ARAD_LINE:'',
			// Debit:0,
			// Kredit:0
		// }
	// );
	// dsDTLTRList_AdjustApForm.insert(dsDTLTRList_AdjustApForm.getCount(), p);	 
	var records = new Array();
	records.push(new dsDTLTRList_AdjustApForm.recordType());
	dsDTLTRList_AdjustApForm.add(records);
	
	var row =dsDTLTRList_AdjustApForm.getCount()-1;
	gridDTLTR_AdjustApForm.startEditing(row, 1);
}

function TambahBarisDb_AdjustApForm()
{
	// var p = new mRecord_AdjustApForm
	// (
		// {
			// ACCOUNT: StrAccVend_AdjustApForm,
			// NAMAACCOUNT: StrNmAccVend_AdjustApForm,
			// Description: Ext.get('txtCatatan_AdjustApForm').getValue(),		
			// VALUE: 0,								
			// ARAD_LINE:'',
			// Debit:0,
			// Kredit:0
		// }
	// );
	// dsDTLTRList_AdjustApForm.insert(dsDTLTRList_AdjustApForm.getCount(), p);	 
	for(var i=0,iLen=strTotalRecord; i<iLen ;i++){
		var records=[];
		records.push(new dsDTLTRList_AdjustApForm.recordType());
		dsDTLTRList_AdjustApForm.add(records);
		dsDTLTRList_AdjustApForm.data.items[i].data.account=StrAccVend_AdjustApForm;
		dsDTLTRList_AdjustApForm.data.items[i].data.name=StrNmAccVend_AdjustApForm;
		dsDTLTRList_AdjustApForm.data.items[i].data.description=Ext.get('txtCatatan_AdjustApForm').getValue();
		dsDTLTRList_AdjustApForm.data.items[i].data.kredit=0;
		dsDTLTRList_AdjustApForm.data.items[i].data.debit=0;
	}
	gridDTLTR_AdjustApForm.getView().refresh();
	
	var row =dsDTLTRList_AdjustApForm.getCount()-1;
	if (Ext.getCmp('radCrEntry_AdjustApForm').getValue()=== true){
		gridDTLTR_AdjustApForm.startEditing(row, 4);
	}else{
		gridDTLTR_AdjustApForm.startEditing(row, 5);
	}
	
}
function TambahBarisCr_AdjustApForm()
{
	var p = new mRecord_AdjustApForm
	(
		{
			ACCOUNT:StrAccKas_AdjustApForm,
			NAMAACCOUNT: StrNmAccKas_AdjustApForm,
			Description: Ext.get('txtCatatan_AdjustApForm').getValue(),		
			VALUE: 0,								
			ARAD_LINE:'',
			Debit:0,
			Kredit:0
		}
	);
	dsDTLTRList_AdjustApForm.insert(dsDTLTRList_AdjustApForm.getCount(), p);	 
	/*if (radTipe_AdjustApForm == 0)
	{
		Ext.getCmp('fnumKreEntry_AdjustApForm').setDisabled(true);
		Ext.getCmp('fnumDebEntry_AdjustApForm').setDisabled(false);
	}else
	{
		Ext.getCmp('fnumKreEntry_AdjustApForm').setDisabled(false);
		Ext.getCmp('fnumDebEntry_AdjustApForm').setDisabled(true);
	}*/
}
function DataDelete_AdjustApForm() 
{
	if (ValidasiEntry_AdjustApForm('Hapus Data') == 1 )
	{	
		Ext.Ajax.request
		(
			{
				url: "./Datapool.mvc/DeleteDataObj",
				params:  getParam_AdjustApForm(), 
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true)					
					{
							ShowPesanInfo_AdjustApForm('Data berhasil di hapus','Hapus Data');
							RefreshDataFilter_AdjustApForm(false);
							AddNew_AdjustApForm();
					}
					else if (cst.success === false && cst.pesan === 0 )
					{
						ShowPesanWarning_AdjustApForm('Data tidak berhasil di hapus, data tersebut belum ada','Hapus Data');
					}
					else 
					{
						ShowPesanError_AdjustApForm('Data tidak berhasil di hapus : ' + cst.pesan,'Hapus Data');
					}
				}
			}
		)
	}
};

function CekArrKosong_AdjustApForm()
{
	var x=1;
	for(var i = 0 ; i < dsDTLTRList_AdjustApForm.getCount();i++)
	{
		if (dsDTLTRList_AdjustApForm.data.items[i].data.ACCOUNT=='' || dsDTLTRList_AdjustApForm.data.items[i].data.ACCOUNT==undefined){
			x=0;
		}

	}		
	return x;
};

function getArrDetail_AdjustApForm()
{
	var x='';
	var is_debit;
	if (radTipe_AdjustApForm==1)
	{
		is_debit=1
	}else
	{
		is_debit=0
	}
	for(var i = 0 ; i < dsDTLTRList_AdjustApForm.getCount();i++)
	{
		var y='';
		var z='@@##$$@@';
		
		// if (DataAddNew_AdjustApForm === true)
		if (dsDTLTRList_AdjustApForm.data.items[i].data.APAD_LINE==="")
		{
			y = "APAD_LINE=" + 0 
		}
		else
		{	
			y = 'APAD_LINE=' + dsDTLTRList_AdjustApForm.data.items[i].data.APAD_LINE
		}		
		y += z + 'ACCOUNT='+dsDTLTRList_AdjustApForm.data.items[i].data.ACCOUNT
		y += z + 'DESCRIPTION=' + dsDTLTRList_AdjustApForm.data.items[i].data.Description
		if ( dsDTLTRList_AdjustApForm.data.items[i].data.Debit != 0 )
		{
			y += z + 'Value=' + parseFloat(dsDTLTRList_AdjustApForm.data.items[i].data.Debit)
			y += z + 'IsDebit=1'
		}
		else
		{
			y += z + 'Value=' + parseFloat(dsDTLTRList_AdjustApForm.data.items[i].data.Kredit)
			y += z + 'IsDebit=0'
		}
		// y += z + 'VALUE=' + dsDTLTRList_AdjustApForm.data.items[i].data.VALUE
		// y += z + 'ISDEBIT=' + is_debit //dsDTLTRList_AdjustApForm.data.items[i].data.ISDEBIT
		// y += z + 'POSTED=' + dsDTLTRList_AdjustApForm.data.items[i].data.POSTED
		y += z + 'APA_NUMBER='+dsDTLTRList_AdjustApForm.data.items[i].data.APA_NUMBER;
		y += z + 'APA_DATE='+dsDTLTRList_AdjustApForm.data.items[i].data.APA_DATE;
		if (i === (dsDTLTRList_AdjustApForm.getCount()-1))
		{
			x += y 
		}
		else
		{
			x += y + '##[[]]##'
		}
	}		
	return x;
};


function CalcTotal_AdjustApForm(idx)
{
    var total=Ext.num(0);
	var nilai=Ext.num(0);
	for(var i=0;i < dsDTLTRList_AdjustApForm.getCount();i++)
	{
		nilai=dsDTLTRList_AdjustApForm.data.items[i].data.value;				
		// total += getNumber(nilai);
		total += nilai;
	}	
	Ext.get('txtTotalDb_AdjustApForm').dom.value=formatCurrencyDec(total);
};

function CalcTotalDetailGrid_AdjustApForm()
{
    var total=Ext.num(0);
	var nilai=Ext.num(0);
	for(var i=0;i < dsDTLTRList_AdjustApForm.getCount();i++)
	{
		nilai=dsDTLTRList_AdjustApForm.data.items[i].data.value;				
		// total += getNumber(nilai);
		total += nilai;
	}	
	return total;
};

function CalcTotalDb_AdjustApForm(idx)
{
	var totalDebit=Ext.num(0);
	for(var i=0;i < dsDTLTRList_AdjustApForm.getCount();i++)
	{
		/* if (i === idx)
		{
			if ((Ext.get('fnumDebEntry_AdjustApForm').dom.value != null ) || (Ext.get('fnumDebEntry_AdjustApForm').dom.value != undefined))
			{
				if (Ext.num(Ext.get('fnumDebEntry_AdjustApForm').dom.value) != null)
				{
					totalDebit += parseFloat(Ext.get('fnumDebEntry_AdjustApForm').dom.value);
				}
			}
		}
		else
		{
			totalDebit += parseFloat(dsDTLTRList_AdjustApForm.data.items[i].data.Debit);
		} */
		var o = dsDTLTRList_AdjustApForm.getRange()[i].data;
		if(o.account != '' || o.account != undefined){			
			totalDebit += parseFloat(dsDTLTRList_AdjustApForm.data.items[i].data.debit);
		}
	}
	Ext.get('txtTotalDb_AdjustApForm').dom.value=formatCurrencyDec(totalDebit);
};

function CalcTotalCr_AdjustApForm(idx)
{

	var totalKredit=Ext.num(0);
	for(var i=0;i < dsDTLTRList_AdjustApForm.getCount();i++)
	{
		/* if (i === idx)
		{
			if (Ext.get('fnumKreEntry_AdjustApForm') != null )
			{
				if (Ext.num(Ext.get('fnumKreEntry_AdjustApForm').dom.value) != null)
				{
					if (Ext.get('fnumKreEntry_AdjustApForm').dom.value != undefined)
					{
						totalKredit += parseFloat(Ext.get('fnumKreEntry_AdjustApForm').dom.value);
					}
				}
			}
		}
		else
		{
			if (dsDTLTRList_AdjustApForm.data.items[i].data.Kredit != undefined)
			{
				totalKredit += parseFloat(dsDTLTRList_AdjustApForm.data.items[i].data.Kredit);	
			}
			
		} */
		var o = dsDTLTRList_AdjustApForm.getRange()[i].data;
		if(o.account != '' || o.account != undefined){			
			totalKredit += parseFloat(dsDTLTRList_AdjustApForm.data.items[i].data.kredit);	
		} else{
			totalKredit += 0;
		}
	}
	Ext.get('txtTotalCr_AdjustApForm').dom.value=formatCurrencyDec(totalKredit);
};

function ButtonDisabled_AdjustApForm(mBol)
{
	Ext.get('btnTambahBaris_AdjustApForm').dom.disabled=mBol;
	Ext.get('btnHapusBaris_AdjustApForm').dom.disabled=mBol;
	Ext.get('btnSimpan_AdjustApForm').dom.disabled=mBol;
	Ext.get('btnSimpanKeluar_AdjustApForm').dom.disabled=mBol;
	Ext.get('btnHapus_AdjustApForm').dom.disabled=mBol;	
	Ext.get('btnApprove_AdjustApForm').dom.disabled=mBol;	
};


function HapusBaris_AdjustApFormDB(Line) 
{	
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/keuangan/functionAdjustHutang/deleteRow",
			params: getParam_AdjustApFormHapusBaris(Line), 
			success: function(o) 
			{
				var cst = o.responseText;
				if (cst == '{"success":true}') 
				{						
						RefreshDataFilter_AdjustApForm(false);						
					}
					else if (cst == '{"pesan":0,"success":false}' )
					{
						
					}
					else 
					{
						
					}
			}
		}
	)
	
};

function getParam_AdjustApFormHapusBaris(Line) 
{
	var params = 
	{
		apa_number:Ext.get('txtNo_AdjustApForm').getValue(),
		apa_date:Ext.get('dtpTanggal_AdjustApForm').getValue(),
		line:Line,	
		// KATEGORI:KDkategori_AdjustApForm,
		// Amount: getAmount_AdjustApForm(Ext.get('txtTotalDb_AdjustApForm').getValue())
	};
	return params
};

function getParamApprove_AdjustApForm(TglApprove, NoApprove,NoteApprove,jml,nomor,tgl) 
{
	var strInfo;
	var notagAp;
	var dateTagAp;
	if (dsTRListFakturAmount_ApproveAdjustAP.getCount()>0){

		if (radTipe_AdjustApForm == 1)
		{
			strInfo='APAF-DB'	
		}else{
			strInfo='APAF-CR'	
		}

		notagAp=nomor;
		dateTagAp=tgl;

	} else{
		if (radTipe_AdjustApForm == 1)
		{
			strInfo='APA-DB'	
		}else{
			strInfo='APA-CR'	
		}
		notagAp=NoApprove;
		dateTagAp=TglApprove;
	}

	var params = 
	{
		Table: 'viACC_AP_ADJUST',
		apa_number:Ext.get('txtNo_AdjustApForm').getValue(),
		apa_date:Ext.get('dtpTanggal_AdjustApForm').getValue(),
		vend_code:selectCboEntryVendor_AdjustApForm,
		due_date:Ext.get('dtpTanggalDueday_AdjustApForm').getValue(),
		//amount:CalcTotalDetailGrid_AdjustApForm(),//getAmount_AdjustApForm(Ext.get('txtTotalDb_AdjustApForm').getValue()),
		amount:getTotalCreDetailGrid_AdjustApForm(),//getAmount_AdjustApForm(Ext.get('txtTotalDb_AdjustApForm').getValue()),
		//kd_user:strKdUser,
		notes:NoteApprove,
		type: radTipe_AdjustApForm,
        info: strInfo,
		ap_number: nomor,
        ap_date: tgl,
		/* List:getArrDetail_AdjustApForm(),
		JmlField: 7,
		JmlList: dsDTLTRList_AdjustApForm.getCount(),
		List2:getArrDetailFakturAdjustAP_AdjustApForm(jml),
		JmlField2: 9,
		JmlList2: dsTRListFakturAmount_ApproveAdjustAP.getCount()   */
	};
	params['jumlah_trans']=dsDTLTRList_AdjustApForm.getCount();
	for(var i = 0 ; i < dsDTLTRList_AdjustApForm.getCount();i++)
	{
		params['account_trans-'+i]=dsDTLTRList_AdjustApForm.data.items[i].data.account;
		params['description_trans-'+i]=dsDTLTRList_AdjustApForm.data.items[i].data.description;
		params['line_trans-'+i]=dsDTLTRList_AdjustApForm.data.items[i].data.line;
		if((dsDTLTRList_AdjustApForm.data.items[i].data.debit != 0 || dsDTLTRList_AdjustApForm.data.items[i].data.debit != undefined) && (dsDTLTRList_AdjustApForm.data.items[i].data.kredit == 0 || dsDTLTRList_AdjustApForm.data.items[i].data.kredit == undefined)){
			params['value_trans-'+i]=dsDTLTRList_AdjustApForm.data.items[i].data.debit;
			params['isdebit_trans-'+i]=true;
		} else{
			params['value_trans-'+i]=dsDTLTRList_AdjustApForm.data.items[i].data.kredit;
			params['isdebit_trans-'+i]=false;
		}
	}
	
	params['jumlah_approve']=dsTRListFakturAmount_ApproveAdjust.getCount();
	for(var i = 0 ; i < dsTRListFakturAmount_ApproveAdjust.getCount();i++)
	{
		params['reff_approve-'+i]=dsTRListFakturAmount_ApproveAdjust.data.items[i].data.REFF;
		params['reff_date_approve-'+i]=dsTRListFakturAmount_ApproveAdjust.data.items[i].data.REFF_DATE;
		params['ap_number_approve-'+i]=Ext.getCmp('txtfilterNo_ApproveAdjust').getValue();
		params['ap_date_approve-'+i]=Ext.get('dtpfilterTanggal_ApproveAdjust').getValue();
		params['paid_approve-'+i]=dsTRListFakturAmount_ApproveAdjust.data.items[i].data.PAID;
		params['type_approve-'+i]='1';
	}
	return params
};

function INPUTDetailSP3D_AdjustApForm()
{	
    var strKriteria_AdjustApForm
    strKriteria_AdjustApForm = " where NO_SP3D_RKAT='" + no_sp3d + "' "
	dsDTLTRList_AdjustApForm.load
	(
		{ 
			params: 
			{ 	
				Skip: 0,
				Take: 20,
				Sort: 'Line',
				Sortdir: 'ASC',
				target:'VIviewsp3d_lpj',
				param: strKriteria_AdjustApForm
			}
		}
	);	
	return dsDTLTRList_AdjustApForm;
}

function GetVendorAcc_AdjustApForm(param)
{
	 Ext.Ajax.request
	 (
		{
			url: baseURL + "index.php/keuangan/functionGeneral/getInfoVendor",
			params: 
			{
				kd_vendor:param
			},
			success: function(o) 
			{
			var cst = Ext.decode(o.responseText);
				if (cst.success === true )
				{
				 	StrAccVend_AdjustApForm = cst.account;
				 	strDue_AdjustApForm=cst.term
				 	StrNmAccVend_AdjustApForm=cst.name;
				 	strTotalRecord=cst.totalrecords;
				}
				else
				{
					ShowPesanInfoRekapSP3D	('Vendor tidak ditemukan','Vendor');
				}
			}

		}
		);
};


function onecolumnentry_AdjustApForm(isDebit)
{
	var db = 0;
	var cr = 0;
	if(CurrentTR_AdjustApForm.row === dsDTLTRList_AdjustApForm.getCount() - 1)
	{
		if ((Ext.get('fnumKreEntry_AdjustApForm') != null )|| (Ext.get('fnumKreEntry_AdjustApForm') != undefined))
		{
			if (Ext.num(Ext.getCmp('fnumKreEntry_AdjustApForm').getValue()) != null)
			{
				cr = parseFloat(Ext.getCmp('fnumKreEntry_AdjustApForm').getValue());
			}
		}
		
		if (Ext.get('fnumDebEntry_AdjustApForm') != null )
		{
			if (Ext.num(Ext.getCmp('fnumDebEntry_AdjustApForm').getValue()) != null)
			{
				db = parseFloat(Ext.getCmp('fnumDebEntry_AdjustApForm').getValue());
			}
		}
	}
	else
	{
		cr = parseFloat(dsDTLTRList_AdjustApForm.data.items[CurrentTR_AdjustApForm.row].data.kredit);
		db = parseFloat(dsDTLTRList_AdjustApForm.data.items[CurrentTR_AdjustApForm.row].data.debit);
	}
	
	// jika entry di debit, di Kredit diisi 0
	if(isDebit)
	{
		if(db != 0 && cr != 0)
		{
			Ext.getCmp('fnumKreEntry_AdjustApForm').setValue(0);
			dsDTLTRList_AdjustApForm.data.items[CurrentTR_AdjustApForm.row].data.kredit = 0;
		}
	}
	else
	{
		if(db != 0 && cr != 0)
		{
			Ext.getCmp('fnumDebEntry_AdjustApForm').setValue(0);
			dsDTLTRList_AdjustApForm.data.items[CurrentTR_AdjustApForm.row].data.debit = 0;
		}
	}
	
	// getTotalKreDebDetail_AdjustApForm();
}

function getTotalKreDebDetail_AdjustApForm()
{
	var total = [0,0]; // [0] -> Kredit, [1] -> debit
	var debitRow = 0;
	var kreditRow = 0;
	if(dsDTLTRList_AdjustApForm.getCount() > 0)
	{
		for(var i = 0;i < dsDTLTRList_AdjustApForm.getCount();i++)
		{
			kreditRow = dsDTLTRList_AdjustApForm.data.items[i].data.kredit;
			debitRow = dsDTLTRList_AdjustApForm.data.items[i].data.debit;
			// if (kreditRow != undefined)
			// {

				total[0] = total[0] + parseFloat(kreditRow);
				total[1] = total[1] + parseFloat(debitRow);
			// }
		}
	}
	Ext.getCmp('txtTotalCr_AdjustApForm').setValue(formatCurrencyDec(total[0]));
	Ext.getCmp('txtTotalDb_AdjustApForm').setValue(formatCurrencyDec(total[1]));
}

function GetDueDate_AdjustApForm(param)
{
	Ext.Ajax.request
	 (
		{
			url: baseURL + "index.php/anggaran/fakturAR/getduedatecustomer_filterbytgl",
            params: {Params: param},
			success: function(o) 
			{
			var cst = Ext.decode(o.responseText);
				if (cst.success === true )
				{
				 	Ext.get('dtpTanggalDueday_AdjustApForm').dom.value=ShowDateAkuntansi(cst.DueDay)
				}
				else
				{
					// ShowPesanInfoRekapSP3D	('Customer tidak ditemukan','Customer');
				}
			}

		}
		);
};
function GetAkunKas_AdjustApForm(param)
{
	 Ext.Ajax.request
	 (
		{
			url: "./Module.mvc/ExecProc",
			params:
			{
				UserID: 'Admin',
				ModuleID: 'GetAccKasArAp',
				Params:	param 
			},
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true )
				{
				 	StrNmAccKas_AdjustApForm=cst.NamaAcc
				 	StrAccKas_AdjustApForm=cst.Account
				}
				else
				{
					//ShowPesanInfoRekapSP3D	('Kas tidak ditemukan','Customer');
				}
			}

		}
		);
};


//
////---------------------FIND DIALOG---------------
var nowFind_AdjustApForm = new Date();
var selectcboOperator_AdjustApForm;
var selectcboField_AdjustApForm;
//var frmFindDlg_AdjustApForm= fnFindDlg_AdjustApForm();
//frmFindDlg_AdjustApForm.show();
var winFind_AdjustApForm;

function fnFindDlg_AdjustApForm()
{  	
    winFind_AdjustApForm = new Ext.Window
	(
		{ 
			id: 'winFindx_AdjustApForm',
			title: 'Find',
			closeAction: 'destroy',
			width:350,
			height: 200,					
			border: false,
			resizable:false,
			plain: true,
			layout: 'fit',
			iconCls: 'find',
			modal: true,
			items: [ItemFindDlg_AdjustApForm()]
			
		}
	);

	winFind_AdjustApForm.show();
	
    return winFind_AdjustApForm; 
};

function ItemFindDlg_AdjustApForm() 
{	
	var PnlFindDlg_AdjustApForm = new Ext.Panel
	(
		{ 
			id: 'PnlFindDlg_AdjustApForm',
			fileUpload: true,
			layout: 'anchor',
			// width:350,
			height: 90,
			anchor: '100%',
			bodyStyle: 'padding:5px',
			border: true,
			items: 
			[

				{
					xtype: 'compositefield',
					fieldLabel: '',
					anchor: '100%',
					items: 
					[
						{
							xtype: 'fieldset',
							title: '',
							width: 325,
							height: 125, 
							items: 
							[
								mComboFindField_AdjustApForm(),
								mComboOperator_AdjustApForm(),
								getItemFindKeyWord_AdjustApForm(),
								getItemFindTgl_AdjustApForm(),
								
				
							]
						}						
					]
				},
				{
					layout: 'hBox',
					border: false,
					defaults: { margins: '0 5 0 0' },
					style:{'margin-left':'21px','margin-top':'5px'},
					anchor: '100%',
					layoutConfig: 
					{
						padding: '3',
						pack: 'end',
						align: 'middle'
					},
					items: 
					[
						{
							xtype: 'button',
							text: 'Ok',
							width: 70,
							hideLabel: true,
							id: 'btnOkLap_AdjustApForm',
							handler: function() 
							{
								if (ValidasiReport_AdjustApForm()==1){
									RefreshDataFindRecord_AdjustApForm()
								}
								
							}
						},
						{
							xtype: 'button',
							text: 'Cancel',
							width: 70,
							hideLabel: true,
							id: 'btnCancelLap_AdjustApForm',
							handler: function() 
							{
								winFind_AdjustApForm.close();
							}
						}
					]
				}
			]
		}
	);
 
    return PnlFindDlg_AdjustApForm;
};

function RefreshDataFindRecord_AdjustApForm() 
{   
	var strparam='';
	if(selectcboOperator_AdjustApForm==1){

		if (selectcboField_AdjustApForm=="ART.APA_Date"){

			strparam=" WHERE "+selectcboField_AdjustApForm+" like '"+ ShowDate(Ext.getCmp('dtpTglFind_AdjustApForm').getValue()) + "%' "; 

		} else{
			strparam=" WHERE "+selectcboField_AdjustApForm+" like '"+ Ext.getCmp('txtKeyWordFind_AdjustApForm').getValue() + "%' "; 

		}		
		
	} else if (selectcboOperator_AdjustApForm==2){

		if (selectcboField_AdjustApForm=="ART.APA_Date"){

			strparam=" WHERE "+selectcboField_AdjustApForm+" = '"+ ShowDate(Ext.getCmp('dtpTglFind_AdjustApForm').getValue()) + "' "; 

		} else{
			strparam=" WHERE "+selectcboField_AdjustApForm+" = '"+ Ext.getCmp('txtKeyWordFind_AdjustApForm').getValue() + "' "; 

		}

	} else if (selectcboOperator_AdjustApForm==3){
		 
		if (selectcboField_AdjustApForm=="ART.APA_Date"){

			strparam=" WHERE "+selectcboField_AdjustApForm+" < '"+ ShowDate(Ext.getCmp('dtpTglFind_AdjustApForm').getValue()) + "' "; 

		} else{
			strparam=" WHERE "+selectcboField_AdjustApForm+" < '"+ Ext.getCmp('txtKeyWordFind_AdjustApForm').getValue() + "' ";

		}

	} else if (selectcboOperator_AdjustApForm==4){

		if (selectcboField_AdjustApForm=="ART.APA_Date"){

			strparam=" WHERE "+selectcboField_AdjustApForm+" > '"+ ShowDate(Ext.getCmp('dtpTglFind_AdjustApForm').getValue()) + "' "; 

		} else{
			strparam=" WHERE "+selectcboField_AdjustApForm+" > '"+ Ext.getCmp('txtKeyWordFind_AdjustApForm').getValue() + "' ";

		}
		
	} else if (selectcboOperator_AdjustApForm==5){
		if (selectcboField_AdjustApForm=="ART.APA_Date"){

			strparam=" WHERE "+selectcboField_AdjustApForm+" <= '"+ ShowDate(Ext.getCmp('dtpTglFind_AdjustApForm').getValue()) + "' "; 

		} else{
			strparam=" WHERE "+selectcboField_AdjustApForm+" <= '"+ Ext.getCmp('txtKeyWordFind_AdjustApForm').getValue() + "' ";

		}
		
		
	} else if (selectcboOperator_AdjustApForm==6){
		if (selectcboField_AdjustApForm=="ART.APA_Date"){

			strparam=" WHERE "+selectcboField_AdjustApForm+" >= '"+ ShowDate(Ext.getCmp('dtpTglFind_AdjustApForm').getValue()) + "' "; 

		} else{
			strparam=" WHERE "+selectcboField_AdjustApForm+" >= '"+ Ext.getCmp('txtKeyWordFind_AdjustApForm').getValue() + "' ";

		}
		
	} else if (selectcboOperator_AdjustApForm==7){
		if (selectcboField_AdjustApForm=="ART.APA_Date"){

			strparam=" WHERE "+selectcboField_AdjustApForm+" <> '"+ ShowDate(Ext.getCmp('dtpTglFind_AdjustApForm').getValue()) + "' "; 

		} else{
			strparam=" WHERE "+selectcboField_AdjustApForm+" <> '"+ Ext.getCmp('txtKeyWordFind_AdjustApForm').getValue() + "' ";

		}
		
	} 
	
    {  
		dsTRList_AdjustApForm.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCount_AdjustApForm, 
					Sort: 'APA_DATE', 
					Sortdir: 'ASC', 
					target:'viACC_AP_ADJUST',
					param: strparam
				}			
			}
		);        
    }
	
};


function ValidasiReport_AdjustApForm()
{
	var x=1;
	
	if(selectcboField_AdjustApForm == undefined || selectcboField_AdjustApForm == "")
	{
		ShowPesanWarningFindDlg_AdjustApForm('Field belum dipilih','Find');
		x=0;		
	};

	if(selectcboOperator_AdjustApForm ==undefined || selectcboOperator_AdjustApForm== "")
	{
		
		ShowPesanWarningFindDlg_AdjustApForm('Operator belum di pilih','Find');
		x=0;
			
	};

	if(Ext.getCmp('txtKeyWordFind_AdjustApForm').getValue()== "")
	{
		
		ShowPesanWarningFindDlg_AdjustApForm('Keyword belum di isi','Find');
		x=0;
			
	};

	return x;
};

function ShowPesanWarningFindDlg_AdjustApForm(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING,
		   width:180
		}
	);
};

function getItemFindTgl_AdjustApForm() 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
		{
			xtype: 'compositefield',
			fieldLabel: '-',
			anchor: '100%',
			items: 
			[
				 {
					columnWidth: 1,
					layout: 'form',
					border: false,
					labelAlign: 'left',
					labelWidth: 100,
					items:
					[
						{
							xtype: 'datefield',
							fieldLabel: 'Periode ',
							id: 'dtpTglFind_AdjustApForm',
							format: 'd/M/Y',
							disabled:true,
							value:now,
							width:190

						}
					]
		    	}
			]
		}
		   
		]
	}
    return items;
};

function getItemFindKeyWord_AdjustApForm() 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
		{
			xtype: 'compositefield',
			fieldLabel: '-',
			anchor: '100%',
			items: 
			[
				 {
					columnWidth: 1,
					layout: 'form',
					border: false,
					labelAlign: 'left',
					labelWidth: 100,
					items:
					[
						{
							xtype: 'textfield',
							fieldLabel: 'Key Word ',
							id: 'txtKeyWordFind_AdjustApForm',
							name: 'txtKeyWordFind_AdjustApForm',
							width:190
						}
					]
		    	}
			 
			]
		}
		   
		]
	}
    return items;
};

function mComboFindField_AdjustApForm()
{
	var cboFindField_AdjustApForm = new Ext.form.ComboBox
	(
		{
			id:'cboFindField_AdjustApForm',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih Field',
			width:190,
			fieldLabel: 'Field Name',	
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: [
							["ART.APA_NUMBER", "Number"], 
							["ART.APA_Date", "Date"],
							["VEND.VEND_CODE", "Vend Code"], 
							["VEND.VENDOR", "Vendor"],
							["Notes", "Notes"]
						  ]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			listeners:  
			{
				'select': function(a,b,c)
				{
					selectcboField_AdjustApForm=b.data.Id;

					if (b.data.Id=="ART.APA_Date"){
						Ext.getCmp('txtKeyWordFind_AdjustApForm').disable();
						Ext.getCmp('dtpTglFind_AdjustApForm').enable();
					} else{
						Ext.getCmp('dtpTglFind_AdjustApForm').disable();
						Ext.getCmp('txtKeyWordFind_AdjustApForm').enable();
					}
				}

			}
			
		}
	);
	return cboFindField_AdjustApForm;
}


function mComboOperator_AdjustApForm()
{
	var cboFindOperator_AdjustApForm = new Ext.form.ComboBox
	(
		{
			id:'cboFindOperator_AdjustApForm',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			width:190,
			emptyText:'Pilih Operator',
			fieldLabel: 'Operator ',
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: 
					[
						[1, "Like"], 
						[2, "="],
						[3, "<"],
						[4, ">"],
						[5, "<="], 
						[6, ">="],
						[7, "<>"]
					]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			listeners:  
			{
				'select': function(a,b,c)
				{
					selectcboOperator_AdjustApForm=b.data.Id;
				}

			}
		}
	);
	return cboFindOperator_AdjustApForm;
}
//////////////////////////////////////////////////////////////
var rowSelectedFakturAmount_ApproveAdjust;
var dsTRListFakturAmount_ApproveAdjustAP;
var fldDetail_FakturAmountp2 =['ID','REFF','AMOUNT','AMOUNT_ADD','NOTES','REFF_DATE','PAID','REMAIN']  
	
	dsTRListFakturAmount_ApproveAdjustAP= new WebApp.DataStore({ fields: fldDetail_FakturAmountp2 })

function getArrDetailFakturAdjustAP_AdjustApForm(jml)
{
	var x='';

	for(var i = 0 ; i < dsTRListFakturAmount_ApproveAdjustAP.getCount();i++)
	{
		var y='';
		var z='@@##$$@@';
		
		y ='ID='+dsTRListFakturAmount_ApproveAdjustAP.data.items[i].data.ID		
		y += z + 'APO_NUMBER='+dsTRListFakturAmount_ApproveAdjustAP.data.items[i].data.REFF
		y += z + 'APO_DATE=' + dsTRListFakturAmount_ApproveAdjustAP.data.items[i].data.REFF_DATE
		y += z + 'APA_NUMBER='+Ext.getCmp('txtNo_AdjustApForm').getValue()//dsTRListFakturAmount_ApproveAdjustAP.data.items[i].data.ARA_NUMBER
		y += z + 'APA_DATE='+Ext.get('dtpTanggalAppAdjustAP').getValue()//dsTRListFakturAmount_ApproveAdjustAP.data.items[i].data.ARA_DATE
		y += z + 'APAJI_NUMBER='+ Ext.getCmp('txtNo_AdjustApForm').getValue()//+dsTRListFakturAmount_ApproveAdjustAP.data.items[i].data.ARAJI_NUMBER
		y += z + 'APAJI_DATE='+ Ext.get('dtpTanggalAppAdjustAP').getValue()//+dsTRListFakturAmount_ApproveAdjustAP.data.items[i].data.ARAJI_DATE
		y += z + 'PAID=' + jml//dsTRListFakturAmount_ApproveAdjustAP.data.items[i].data.PAID
		y += z + 'TYPE=1'// + dsTRListFakturAmount_ApproveAdjustAP.data.items[i].data.POSTED
		if (i === (dsTRListFakturAmount_ApproveAdjustAP.getCount()-1))
		{
			x += y 
		}
		else
		{
			x += y + '##[[]]##'
		}
	}		
	return x;
};

function AutoBalance_AdjustApForm()
{
	var rows = dsDTLTRList_AdjustApForm.getCount();
	var tCR = Ext.num(parseFloat(GetAngkaAdjustArForm(Ext.getCmp('txtTotalCr_AdjustApForm').getValue())));
	var tDB = Ext.num(parseFloat(GetAngkaAdjustArForm(Ext.getCmp('txtTotalDb_AdjustApForm').getValue())));
	var selisih = Math.abs(tCR-tDB);
	var rowCR = 0;
	var rowDB = 0;
	
	if(rows > 0)
	{
		for(var i=0; i<rows; i++)
		{
			rowCR = Ext.num(parseFloat(GetAngkaAdjustArForm(dsDTLTRList_AdjustApForm.data.items[i].data.kredit)));
			rowDB = Ext.num(parseFloat(GetAngkaAdjustArForm(dsDTLTRList_AdjustApForm.data.items[i].data.debit)));
			if(rowCR != 0 && rowDB != 0){}
			else if(rowCR == 0 && rowDB == 0)
			{
				if(tCR < tDB)
				{
					if(selisih > 0)
					{
						rowCR = selisih;
						selisih = 0;
						var p = new mRecord_AdjustApForm
						(
							{
								account: dsDTLTRList_AdjustApForm.data.items[i].data.account,
								name: dsDTLTRList_AdjustApForm.data.items[i].data.name,
								description: dsDTLTRList_AdjustApForm.data.items[i].data.description,
								value: rowCR,//dsDTLTRList_AdjustApForm.data.items[i].data.Account,
								line: '',//dsDTLTRList_AdjustApForm.data.items[i].data.Description,
								kredit:rowCR,
								debit:0
															
							}
						);
						dsDTLTRList_AdjustApForm.removeAt(i);
						dsDTLTRList_AdjustApForm.insert(i, p);
					}
				}
				else
				{
					if(selisih > 0)
					{
						rowDB = selisih;
						selisih = 0;
						var p = new mRecord_AdjustApForm
						(
							{
								account: dsDTLTRList_AdjustApForm.data.items[i].data.account,
								name: dsDTLTRList_AdjustApForm.data.items[i].data.name,
								description: dsDTLTRList_AdjustApForm.data.items[i].data.description,
								value: rowDB,//dsDTLTRList_AdjustApForm.data.items[i].data.Account,
								line: '',//dsDTLTRList_AdjustApForm.data.items[i].data.Description,
								kredit:0,
								debit:rowDB				
							}
						);
						dsDTLTRList_AdjustApForm.removeAt(i);
						dsDTLTRList_AdjustApForm.insert(i, p);
					}
				}
			}
			else
			{
				if(tCR > tDB)
				{
					if(rowCR > selisih && selisih != 0)
					{
						rowCR -= selisih;
						selisih = 0;
						var p = new mRecord_AdjustApForm
						(
							{
								account: dsDTLTRList_AdjustApForm.data.items[i].data.account,
								name: dsDTLTRList_AdjustApForm.data.items[i].data.name,
								description: dsDTLTRList_AdjustApForm.data.items[i].data.description,
								value: rowCR,//dsDTLTRList_AdjustApForm.data.items[i].data.Account,
								line: '',//dsDTLTRList_AdjustApForm.data.items[i].data.Description,
								kredit:rowCR,
								debit:0							
							}
						);
						dsDTLTRList_AdjustApForm.removeAt(i);
						dsDTLTRList_AdjustApForm.insert(i, p);
					}
				}
				else
				{
					if(rowDB > selisih && selisih != 0)
					{
						rowDB -= selisih;
						selisih = 0;						
						var p = new mRecord_AdjustApForm
						(
							{
								account: dsDTLTRList_AdjustApForm.data.items[i].data.account,
								name: dsDTLTRList_AdjustApForm.data.items[i].data.name,
								description: dsDTLTRList_AdjustApForm.data.items[i].data.description,
								value: rowDB,//dsDTLTRList_AdjustApForm.data.items[i].data.Account,
								line: '',//dsDTLTRList_AdjustApForm.data.items[i].data.Description,
								kredit:0,
								debit:rowDB							
							}
						);
						dsDTLTRList_AdjustApForm.removeAt(i);
						dsDTLTRList_AdjustApForm.insert(i, p);
					}
				}
			}
		}
	}
	CalcTotalCr_AdjustApForm();
	CalcTotalDb_AdjustApForm();	
}

function GetAngkaAdjustArForm(str)
{
	//alert(str)
	for (var i = 0; i < str.length; i++) 
	{
		var y = str.substr(i, 1)
		if (y === '.') 
		{
			str = str.replace('.', '');
		}
	};
	
	return str;
};

function LookUpAccount_AREntry(criteria)
{
	
	WindowLookUpAccount_AREntry = new Ext.Window
    (
        {
            id: 'pnlLookUpAccount_AREntry',
            title: 'Lookup Account',
            width:650,
            height: 350,
            border: false,
            resizable:false,
            plain: true,
            iconCls: 'icon_lapor',
            modal: true,
            items: [
				getGridListSearchAccount_AREntry()
			],
			listeners:
			{             
				activate: function()
				{
						
				},
				afterShow: function()
				{
					this.activate();

				},
				deactivate: function()
				{
					
				},
				close: function (){
					
				}
			}
        }
    );

    WindowLookUpAccount_AREntry.show();
	getListSearchAccount_AREntry(criteria);
};

function getGridListSearchAccount_AREntry(){
	var fldDetail = ['account','name','nama_parent'];
	dsGridListAccount_AREntry = new WebApp.DataStore({ fields: fldDetail });
	
    GridListAccountColumnModelAREntry =  new Ext.grid.ColumnModel([
        new Ext.grid.RowNumberer(),
		{
			dataIndex		: 'account',
			header			: 'No. Account',
			width			: 80,
			menuDisabled	: true,
        },
		{
			dataIndex		: 'name',
			header			: 'Nama',
			width			: 140,
			menuDisabled	: true,
        },
		{
			dataIndex		: 'nama_parent',
			header			: 'Parent',
			width			: 120,
			menuDisabled	: true,
        },
	]);
	
	
	GridListAccount_AREntry= new Ext.grid.EditorGridPanel({
		id			: 'GrdListPencarianAccount_AREntry',
		stripeRows	: true,
		width		: 640,
		height		: 270,
        store		: dsGridListAccount_AREntry,
        border		: true,
        frame		: false,
        autoScroll	: true,
        cm			: GridListAccountColumnModelAREntry,
		selModel: new Ext.grid.RowSelectionModel({
			singleSelect: true,
			listeners:
			{
				rowselect: function(sm, row, rec)
				{
					currentRowSelectionSearchAccountAREntry = undefined;
					currentRowSelectionSearchAccountAREntry = dsGridListAccount_AREntry.getAt(row);
				}
			}
		}),
		listeners	: {
			rowclick: function( $this, rowIndex, e )
			{
				
    		},
			celldblclick: function(gridView,htmlElement,columnIndex,dataRecord){
				var line	= gridDTLTR_AdjustApForm.getSelectionModel().selection.cell[0];
				dsDTLTRList_AdjustApForm.getRange()[line].data.cito="Tidak";
				dsDTLTRList_AdjustApForm.getRange()[line].data.account=currentRowSelectionSearchAccountAREntry.data.account;
				dsDTLTRList_AdjustApForm.getRange()[line].data.name=currentRowSelectionSearchAccountAREntry.data.name;
				dsDTLTRList_AdjustApForm.getRange()[line].data.description=Ext.get('txtCatatan_AdjustAPForm').getValue();
				dsDTLTRList_AdjustApForm.getRange()[line].data.debit=0;
				dsDTLTRList_AdjustApForm.getRange()[line].data.kredit=0;
				
				gridDTLTR_AdjustApForm.getView().refresh();
				gridDTLTR_AdjustApForm.startEditing(line, 4);	
				
				WindowLookUpAccount_AREntry.close();
			},
			'keydown' : function(e){
				if(e.getKey() == 13){
					var line	= gridDTLTR_AdjustApForm.getSelectionModel().selection.cell[0];
					dsDTLTRList_AdjustApForm.getRange()[line].data.cito="Tidak";
					dsDTLTRList_AdjustApForm.getRange()[line].data.account=currentRowSelectionSearchAccountAREntry.data.account;
					dsDTLTRList_AdjustApForm.getRange()[line].data.name=currentRowSelectionSearchAccountAREntry.data.name;
					dsDTLTRList_AdjustApForm.getRange()[line].data.description=Ext.get('txtCatatan_AdjustAPForm').getValue();
					dsDTLTRList_AdjustApForm.getRange()[line].data.debit=0;
					dsDTLTRList_AdjustApForm.getRange()[line].data.kredit=0;
					
					gridDTLTR_AdjustApForm.getView().refresh();
					gridDTLTR_AdjustApForm.startEditing(line, 4);	
					
					WindowLookUpAccount_AREntry.close();
				}
			},
		},
		viewConfig	: {forceFit: true}
    });
	return GridListAccount_AREntry;
}


function getListSearchAccount_AREntry(criteria){
	Ext.Ajax.request ({
		url: baseURL + "index.php/keuangan/functionGeneral/getListAccount",
		params: {
			criteria:criteria
		},
		failure: function(o)
		{
			Ext.Msg.show({
				title: 'Error',
				msg: 'Error menampilkan list account. Hubungi Admin!',
				buttons: Ext.MessageBox.OK,
				fn: function (btn) {
					if (btn == 'ok')
					{
						var line	= gridDTLTR_AdjustAP.getSelectionModel().selection.cell[0];
						gridDTLTR_AdjustAP.startEditing(line, 1);	
					}
				}
			});
		},	
		success: function(o) 
		{   
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				if(cst.totalrecords == 0 ){
					Ext.Msg.show({
						title: 'Information',
						msg: 'Tidak ada account yang sesuai atau kriteria account kurang!',
						buttons: Ext.MessageBox.OK,
						fn: function (btn) {
							if (btn == 'ok')
							{
								var line	= gridDTLTR_AdjustAP.getSelectionModel().selection.cell[0];
								gridDTLTR_AdjustAP.startEditing(line, 4);
								WindowLookUpAccount_AREntry.close();
							}
						}
					});
				} else{
					dsGridListAccount_AREntry.removeAll();
					var recs=[],
						recType=dsGridListAccount_AREntry.recordType;
					for(var i=0; i<cst.ListDataObj.length; i++){
						recs.push(new recType(cst.ListDataObj[i]));						
					}
					dsGridListAccount_AREntry.add(recs);
					GridListAccount_AREntry.getView().refresh();
					GridListAccount_AREntry.getSelectionModel().selectRow(0);
					GridListAccount_AREntry.getView().focusRow(0);
				}
				
			} else {
				Ext.Msg.show({
					title: 'Error',
					msg: 'Error menampilkan list account. Hubungi Admin!',
					buttons: Ext.MessageBox.OK,
					fn: function (btn) {
						if (btn == 'ok')
						{
							var line	= gridDTLTR_AdjustAP.getSelectionModel().selection.cell[0];
							gridDTLTR_AdjustAP.startEditing(line, 1);	
						}
					}
				});
			};
		}
	});
}

function LookUpAccount_APEntry(criteria)
{
	
	WindowLookUpAccount_APEntry = new Ext.Window
    (
        {
            id: 'pnlLookUpAccount_APEntry',
            title: 'Lookup Account',
            width:650,
            height: 350,
            border: false,
            resizable:false,
            plain: true,
            iconCls: 'icon_lapor',
            modal: true,
            items: [
				getGridListSearchAccount_APEntry()
			],
			listeners:
			{             
				activate: function()
				{
						
				},
				afterShow: function()
				{
					this.activate();

				},
				deactivate: function()
				{
					
				},
				close: function (){
					
				}
			}
        }
    );

    WindowLookUpAccount_APEntry.show();
	getListSearchAccount_APEntry(criteria);
};

function getGridListSearchAccount_APEntry(){
	var fldDetail = ['account','name','nama_parent'];
	dsGridListAccount_APEntry = new WebApp.DataStore({ fields: fldDetail });
	
    GridListAccountColumnModelAPEntry =  new Ext.grid.ColumnModel([
        new Ext.grid.RowNumberer(),
		{
			dataIndex		: 'account',
			header			: 'No. Account',
			width			: 80,
			menuDisabled	: true,
        },
		{
			dataIndex		: 'name',
			header			: 'Nama',
			width			: 140,
			menuDisabled	: true,
        },
		{
			dataIndex		: 'nama_parent',
			header			: 'Parent',
			width			: 120,
			menuDisabled	: true,
        },
	]);
	
	
	GridListAccount_APEntry= new Ext.grid.EditorGridPanel({
		id			: 'GrdListPencarianAccount_APEntry',
		stripeRows	: true,
		width		: 640,
		height		: 270,
        store		: dsGridListAccount_APEntry,
        border		: true,
        frame		: false,
        autoScroll	: true,
        cm			: GridListAccountColumnModelAPEntry,
		selModel: new Ext.grid.RowSelectionModel({
			singleSelect: true,
			listeners:
			{
				rowselect: function(sm, row, rec)
				{
					currentRowSelectionSearchAccountAPEntry = undefined;
					currentRowSelectionSearchAccountAPEntry = dsGridListAccount_APEntry.getAt(row);
				}
			}
		}),
		listeners	: {
			rowclick: function( $this, rowIndex, e )
			{
				
    		},
			celldblclick: function(gridView,htmlElement,columnIndex,dataRecord){
				var line	= gridDTLTR_AdjustApForm.getSelectionModel().selection.cell[0];
				dsDTLTRList_AdjustApForm.getRange()[line].data.cito="Tidak";
				dsDTLTRList_AdjustApForm.getRange()[line].data.account=currentRowSelectionSearchAccountAPEntry.data.account;
				dsDTLTRList_AdjustApForm.getRange()[line].data.name=currentRowSelectionSearchAccountAPEntry.data.name;
				dsDTLTRList_AdjustApForm.getRange()[line].data.description=Ext.get('txtCatatan_AdjustApForm').getValue();
				dsDTLTRList_AdjustApForm.getRange()[line].data.debit=0;
				dsDTLTRList_AdjustApForm.getRange()[line].data.kredit=0;
				
				gridDTLTR_AdjustApForm.getView().refresh();
				gridDTLTR_AdjustApForm.startEditing(line, 4);	
				
				WindowLookUpAccount_APEntry.close();
			},
			'keydown' : function(e){
				if(e.getKey() == 13){
					var line	= gridDTLTR_AdjustApForm.getSelectionModel().selection.cell[0];
					dsDTLTRList_AdjustApForm.getRange()[line].data.cito="Tidak";
					dsDTLTRList_AdjustApForm.getRange()[line].data.account=currentRowSelectionSearchAccountAPEntry.data.account;
					dsDTLTRList_AdjustApForm.getRange()[line].data.name=currentRowSelectionSearchAccountAPEntry.data.name;
					dsDTLTRList_AdjustApForm.getRange()[line].data.description=Ext.get('txtCatatan_AdjustApForm').getValue();
					dsDTLTRList_AdjustApForm.getRange()[line].data.debit=0;
					dsDTLTRList_AdjustApForm.getRange()[line].data.kredit=0;
					
					gridDTLTR_AdjustApForm.getView().refresh();
					gridDTLTR_AdjustApForm.startEditing(line, 4);	
					
					WindowLookUpAccount_APEntry.close();
				}
			},
		},
		viewConfig	: {forceFit: true}
    });
	return GridListAccount_APEntry;
}


function getListSearchAccount_APEntry(criteria){
	Ext.Ajax.request ({
		url: baseURL + "index.php/keuangan/functionGeneral/getListAccount",
		params: {
			criteria:criteria
		},
		failure: function(o)
		{
			Ext.Msg.show({
				title: 'Error',
				msg: 'Error menampilkan list account. Hubungi Admin!',
				buttons: Ext.MessageBox.OK,
				fn: function (btn) {
					if (btn == 'ok')
					{
						var line	= gridDTLTR_AdjustAP.getSelectionModel().selection.cell[0];
						gridDTLTR_AdjustAP.startEditing(line, 1);	
					}
				}
			});
		},	
		success: function(o) 
		{   
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				if(cst.totalrecords == 0 ){
					Ext.Msg.show({
						title: 'Information',
						msg: 'Tidak ada account yang sesuai atau kriteria account kurang!',
						buttons: Ext.MessageBox.OK,
						fn: function (btn) {
							if (btn == 'ok')
							{
								var line	= gridDTLTR_AdjustAP.getSelectionModel().selection.cell[0];
								gridDTLTR_AdjustAP.startEditing(line, 4);
								WindowLookUpAccount_APEntry.close();
							}
						}
					});
				} else{
					dsGridListAccount_APEntry.removeAll();
					var recs=[],
						recType=dsGridListAccount_APEntry.recordType;
					for(var i=0; i<cst.ListDataObj.length; i++){
						recs.push(new recType(cst.ListDataObj[i]));						
					}
					dsGridListAccount_APEntry.add(recs);
					GridListAccount_APEntry.getView().refresh();
					GridListAccount_APEntry.getSelectionModel().selectRow(0);
					GridListAccount_APEntry.getView().focusRow(0);
				}
				
			} else {
				Ext.Msg.show({
					title: 'Error',
					msg: 'Error menampilkan list account. Hubungi Admin!',
					buttons: Ext.MessageBox.OK,
					fn: function (btn) {
						if (btn == 'ok')
						{
							var line	= gridDTLTR_AdjustAP.getSelectionModel().selection.cell[0];
							gridDTLTR_AdjustAP.startEditing(line, 1);	
						}
					}
				});
			};
		}
	});
}
