var CurrentTR_CSAPForm = 
{
    data: Object,
    details:Array, 
    row: 0
};

var CurrentTRAkdAdj_CSAPForm = 
{
    data: Object,
    details:Array, 
    row: 0
};

var CurrentSelected_CSAPForm=
{
    data: Object,
    details:Array, 
    row: 0
};

var mRecord_CSAPForm = Ext.data.Record.create
	(
		[
		   {name: 'ACCOUNT', mapping:'ACCOUNT'},
		   {name: 'NAMAACCOUNT', mapping:'NAMAACCOUNT'},
		   {name: 'DESC', mapping:'DESC'},
		   {name: 'VALUE', mapping:'VALUE'},
		   {name: 'CSAPD_LINE', mapping:'CSAPD_LINE'}
		]
	);

var mRecord = Ext.data.Record.create
(
	[	 
		{name:'CSAP_NUMBER', mapping: 'CSAP_NUMBER'}, 
		{name:'CSAP_DATE', mapping: 'CSAP_DATE'}, 
		{name:'APO_NUMBER', mapping: 'APO_NUMBER'}, 
		{name:'APO_DATE', mapping: 'APO_DATE'}, 
		{name:'APOH_NUMBER', mapping: 'APOH_NUMBER'},
		{name:'APOH_DATE', mapping: 'APOH_DATE'},
		{name:'AMOUNT', mapping: 'AMOUNT'}, 
		{name:'PAID', mapping: 'PAID'},
		{name:'REMAIN', mapping: 'REMAIN'},
	]
);

var KDkategori_CSAPForm='2';	
var dsTRList_CSAPForm;
var dsTmp_CSAPForm;
var dsDTLTRList_CSAPForm;
var DataAddNew_CSAPForm = true;
var selectCount_CSAPForm=50;
var now_CSAPForm = new Date();
var selectBayar_CSAPForm;
var selectAktivaLancar_CSAPForm;
var rowSelected_CSAPForm;
var TRLookUps_CSAPForm;
var focusRef_CSAPForm;
var no_sp3d;
var cellSelectedDet_CSAPForm;
var cellSelectedDetAkdAdj_CSAPForm;
var dsCboEntryCustCSAPForm;
var selectCboEntryVendor_CSAPForm;
var StrAccVend_CSAPForm;
var strDue_CSAPForm;
var StrNmAccVend_CSAPForm;
var CekApp_CSAPForm;
var selectUnitKerja_CSAPForm="2300";
// var thn = gstrTahunAngg;
var nASALFORM_CSAPForm = 6;
var mCtrFokus_CSAPForm;
var NamaForm_CSAPForm="Pembayaran Hutang";
var tempstrKdUser;
var criteriaTanggalFindCSAP;

CurrentPage.page=getPanel_CSAPForm(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanel_CSAPForm(mod_id)
{
    var Field = 
	[
	'csap_number','csap_date','vend_code','vendor','account','namaaccount','pay_code','payment',
	'pay_no','currency','kurs','amount','notes1','no_tag','date_tag','amountkurs','approve','kd_user','posted'
	]
    dsTRList_CSAPForm = new WebApp.DataStore({ fields: Field });
   
    grListTR_CSAPForm = new Ext.grid.EditorGridPanel
	(
		{
			stripeRows: true,
			xtype: 'editorgrid',
			store: dsTRList_CSAPForm,
			anchor: '100% 100%',
			columnLines:true,
			border:false,
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
            (
				{
				    singleSelect: true,
				    listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							rowSelected_CSAPForm = dsTRList_CSAPForm.getAt(row);
						}
					}
				}
			),
			listeners:
			{
				rowdblclick: function (sm, row, rec)
				{
					rowSelected_CSAPForm = dsTRList_CSAPForm.getAt(row);
					
					CurrentSelected_CSAPForm.row = rec;
					CurrentSelected_CSAPForm.data = rowSelected_CSAPForm;

					if (rowSelected_CSAPForm != undefined)
					{
							LookUpForm_CSAPForm(rowSelected_CSAPForm.data);
					}
					else
					{
							LookUpForm_CSAPForm();
					}
				}
			},
			colModel: new Ext.grid.ColumnModel	
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header		: 'Status Posting',
						width		: 60,
						sortable	: false,
						hideable	: true,
						hidden		: false,
						menuDisabled: true,
						dataIndex	: 'posted',
						id			: 'colStatusPosting_CSAP',
						renderer	: function(value, metaData, record, rowIndex, colIndex, store){
							 switch (value){
								 case 't':
									 metaData.css = 'StatusHijau'; 
									 break;
								 case 'f':
									 metaData.css = 'StatusMerah';
									 break;
							 }
							 return '';
						}
					},
					{
						id: 'ColCSAP_NUMBER',
						header: 'Nomor',
						dataIndex: 'csap_number',
						sortable: true,			
						width :100,
						filter: {}
					}, 
					{
						header: 'Tanggal',
						width: 100,
						sortable: true,
						dataIndex: 'csap_date',
						id:'ColCSAP_DATE',
						renderer: function(v, params, record) 
						{
							return ShowDate(record.data.csap_date);
						},
						filter: {}			
					}, 
					{						
						id: 'ColVENDOR',
						header: "Terima Dari",
						dataIndex: 'vendor',
						width :130,
						filter: {}
					},
					{
						id: 'ColNotes',
						header: "Keterangan",
						dataIndex: 'notes',
						width :200,
						filter: {},
						renderer: function(value, cell) 
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px;text-align: left;'>" + value+ "</div>";
							return str;
						}
					}, 
					{
						id: 'ColAmount',
						header: "Jumlah (Rp)",
						align:'right',
						dataIndex: 'amount',
						renderer: function(v, params, record) 
						{
							return formatCurrencyDec(record.data.amount);
						},	
						width :80,
						filter: {}
					},
				]
			),
			tbar: 
			[			
				{
					id: 'btnAdd_CSAPForm',
					text: 'Add Data',
					tooltip: 'Edit Data',
					iconCls: 'AddRow',
					handler: function(sm, row, rec) 
					{ 
						LookUpForm_CSAPForm();
					}
				},' ','-',
				{
					id: 'btnEdit_CSAPForm',
					text: 'Edit Data',
					tooltip: 'Edit Data',
					iconCls: 'Edit_Tr',
					handler: function(sm, row, rec) 
					{ 
						if (rowSelected_CSAPForm != undefined)
						{
							LookUpForm_CSAPForm(rowSelected_CSAPForm.data);
							ButtonDisabled_CSAPForm(rowSelected_CSAPForm.data.Type_Approve);
						}
						else
						{
							LookUpForm_CSAPForm();
						}
					}
				},' ','-'
				,
				{
					xtype: 'checkbox',
					id: 'chkWithTgl_CSAPForm',					
					hideLabel:true,
					checked: true,
					handler: function() 
					{
						if (this.getValue()===true)
						{
							Ext.get('dtpTglAwalFilter_CSAPForm').dom.disabled=false;
							Ext.get('dtpTglAkhirFilter_CSAPForm').dom.disabled=false;
							Ext.get('dtpTglAwalFilter_CSAPForm').dom.readOnly=true;	
							Ext.get('dtpTglAkhirFilter_CSAPForm').dom.readOnly=true;
						}
						else
						{
							Ext.get('dtpTglAwalFilter_CSAPForm').dom.disabled=true;
							Ext.get('dtpTglAkhirFilter_CSAPForm').dom.disabled=true;							
						};
					}
				}
				, ' ','-','Tanggal : ', ' ',
				{
					xtype: 'datefield',
					fieldLabel: 'Dari Tanggal : ',
					id: 'dtpTglAwalFilter_CSAPForm',
					format: 'd/M/Y',
					value:now_CSAPForm,
					width:100,
					onInit: function() { },
					listeners:
					{ 
						'specialkey' : function()
						{
							if (Ext.EventObject.getKey() === 13) 
							{
								RefreshDataFilter_CSAPForm(false);					
							} 						
						}
					},
				}, ' ', ' s/d ',' ', {
					xtype: 'datefield',
					fieldLabel: 'Sd /',
					id: 'dtpTglAkhirFilter_CSAPForm',
					format: 'd/M/Y',
					value:now_CSAPForm,
					width:100,
					listeners:
					{ 
						'specialkey' : function()
						{
							if (Ext.EventObject.getKey() === 13) 
							{
								RefreshDataFilter_CSAPForm(false);					
							} 						
						}
					},
				},' ','->',
				{
					id: 'btnFind_CSAPForm',
					text: ' Find',
					tooltip: 'Find Record',
					iconCls: 'find',
					handler: function(sm, row, rec) 
					{ 
						fnFindDlg_CSAPForm();	
					}
				}
			],
			// bbar:new WebApp.PaggingBar
			// (
				// {
					// displayInfo: true,
					// store: dsTRList_CSAPForm,
					// pageSize: 50,
					// displayMsg: 'Record ke {0} - {1} dari {2}&nbsp;&nbsp;',
					// emptyMsg: "Tidak ada record&nbsp;&nbsp;"		
				// }
			// ),
			viewConfig: {forceFit: true} 			
		}
	);


	var FormTR_CSAPForm = new Ext.Panel
	(
		{
			id: mod_id,
			closable:true,
			region: 'center',
			layout: 'form',
			title: NamaForm_CSAPForm, 
			border: false,           
			shadhow: true,
			// iconCls: 'Penerimaan',
			iconCls: '',
			 margins:'0 5 5 0',
			items: [grListTR_CSAPForm],
			tbar: 
			[
				'No : ', ' ',
				{
					xtype: 'textfield',
					fieldLabel: 'No : ',
					id: 'txtNoFilter_CSAPForm',    
					width:120,
					listeners:
					{ 
						'specialkey' : function()
						{
							if (Ext.EventObject.getKey() === 13) 
							{
								RefreshDataFilter_CSAPForm(false);					
							} 						
						}
					},
					onInit: function() { }
				}, 
				{ xtype: 'tbseparator' },
				{
					xtype: 'checkbox',
					id: 'chkFilterApproved_CSAPForm',
					boxLabel: 'Approved',
					listeners: 
					{
						check: function()
						{
						   RefreshDataFilter_CSAPForm(false);
						}
					}
				}, ' ','-',
					' ',mComboMaksData_CSAPForm(),
					' ','->',
				{					
					xtype: 'button',
					tooltip: 'Tampilkan',
					iconCls: 'refresh',
					text: 'Tampilkan',
					anchor: '25%',
					handler: function(sm, row, rec) 
					{
						RefreshDataFilter_CSAPForm(false);
					}
				}
			],
			listeners: 
			{ 
				'afterrender': function() 
				{           
					RefreshDataFilter_CSAPForm(true);			 
				}
			}
		}
	);
	return FormTR_CSAPForm

};
    // end function get panel main data
 
 //---------------------------------------------------------------------------------------///
   
   
function LookUpForm_CSAPForm(rowdata)
{
	var lebar=735;
	TRLookUps_CSAPForm = new Ext.Window
	(
		{
			id: 'LookUpForm_CSAPForm',
			title: NamaForm_CSAPForm,
			closeAction: 'destroy',
			width: lebar,
			height: 500, 
			border: false,
			resizable:false,
			plain: true,
			layout: 'fit',
			iconCls: 'Penerimaan',
			modal: true,
			items: getFormEntryTR_CSAPForm(lebar),
			listeners:
			{
				activate: function() 
				{
					
				},
				afterShow: function() 
				{ 
					this.activate(); 
				},
				deactivate: function()
				{
					RefreshDataFilter_CSAPForm(true);
				}
			}
		}
	);
	
	TRLookUps_CSAPForm.show();
	if (rowdata == undefined)
	{
		AddNew_CSAPForm();
	}
	else
	{
		DataInit_CSAPForm(rowdata)
	}	
	
};
   
function getFormEntryTR_CSAPForm(lebar) 
{
	var pnlTR_CSAPForm = new Ext.FormPanel
	(
		{
			id: 'pnlTR_CSAPForm',
			fileUpload: true,
			region: 'north',
			layout: 'fit',
			bodyStyle: 'padding:10px 10px 10px 10px',			
			anchor: '100%', 
			width:lebar,
			border: false,
			items: [getItemPanelInput_CSAPForm(lebar)],
			tbar: 
			[
				{
					text: 'Tambah',
					id:'btnTambah',
					tooltip: 'Tambah Record Baru ',
					iconCls: 'add',
					handler: function() 
					{ 
						AddNew_CSAPForm();
						ButtonDisabled_CSAPForm(false);
					}
				}, '-', 
				{
					text: 'Simpan',
					id:'btnSimpan',
					tooltip: 'Rekam Data ',
					iconCls: 'save',
					handler: function() 
					{ 
						Datasave_CSAPForm(false);
						RefreshDataFilter_CSAPForm(false);
					}
				}, '-', 
				{
					text: 'Simpan & Keluar',
					id:'btnSimpanKeluar',
					tooltip: 'Rekam Data & Keluar ',
					iconCls: 'saveexit',
					handler: function() 
					{ 
						Datasave_CSAPForm(true);
						RefreshDataFilter_CSAPForm(false);
						TRLookUps_CSAPForm.close();
					}
				}, '-', 
				{
					text: 'Hapus',
					id:'btnHapus',
					tooltip: 'Remove the selected item',
					iconCls: 'remove',
					handler: function() 
					{ 
						Ext.Msg.show
						(
							{
							   title:'Hapus',
							   msg: 'Apakah transaksi ini akan dihapus ?', 
							   buttons: Ext.MessageBox.YESNO,
							   fn: function (btn) 
							   {			
								   if (btn =='yes') 
									{
										DataDelete_CSAPForm();
										RefreshDataFilter_CSAPForm(false);
									} 
							   },
							   icon: Ext.MessageBox.QUESTION
							}
						);
					}
				}, '-', 
				{
					text: 'Lookup',
					id:'btnLookup',
					tooltip: 'Lookup Account',
					iconCls: 'find',
					hidden:true,
					handler: function() 
					{
						if (mCtrFokus_CSAPForm==="txtReferensi_CSAPForm")
						{
							mCtrFokus_CSAPForm="";
							var StrKriteria;
							if (selectUnitKerja_CSAPForm != "")
							{
								StrKriteria = " WHERE APP_SP3D = '1' AND SP3.KD_UNIT_KERJA='" + selectUnitKerja_CSAPForm + "' "
								StrKriteria += " and (( TAHAP_PROSES in(2) AND AC.NO_Tag is null ) or (TAHAP_PROSES in(3) AND AC.NO_Tag is not null )) AND  SP3.TAHUN_ANGGARAN_TA = " + thn
							}
							else
							{
								StrKriteria = " WHERE APP_SP3D = '1'  and ((  TAHAP_PROSES in(2) AND AC.NO_Tag is null  ) or ( TAHAP_PROSES in(3) AND AC.NO_Tag is not null  )) AND  SP3.TAHUN_ANGGARAN_TA = " + thn						
							}							
							var p = new mRecord_CSAPForm
							(
								{
									ACCOUNT: '',
									NAMAACCOUNT: '',
									DESCRIPTION: Ext.get('txtCatatan_CSAPForm').getValue(),
									VALUE: '',								
									LINE:''
								}
							);							
							FormLookupsp3d(dsDTLTRList_CSAPForm,p, StrKriteria,nASALFORM_CSAPForm,thn);
						}
						else
						{		
							var p = new mRecord_CSAPForm
							(
								{
									ACCOUNT: '',
									NAMAACCOUNT: '',
									DESCRIPTION: Ext.get('txtCatatan_CSAPForm').getValue(),
									VALUE: '',								
									LINE:''
								}
							);
							FormLookupAccount(" Where left(A.Account,1) in ('1','2','3','5')  AND A.TYPE ='D'  ",dsDTLTRList_CSAPForm,p,true,'',true);
						}
					}
				}, //'-',
				{
				    text: 'Approve',
					id:'btnApprove',
				    tooltip: 'Approve',
				    iconCls: 'approve',
				    handler: function() 
					{ 
						if (ValidasiApprove_CSAPForm('Approve')==1)
						{
							if (Ext.getCmp('ChkApproveEntry_CSAPForm').getValue()==false)
							{
								var criteria = Ext.get('txtNo_CSAPForm').getValue();
								var criteriaDate = Ext.get('dtpTanggal_CSAPForm').getValue();
								FormLookup_AkadPiutang(selectCboEntryVendor_CSAPForm,criteria,criteriaDate,'Payable','2',
									Ext.get('txtTotal_CSAPForm').getValue(),selectAktivaLancar_CSAPForm,StrAccVend_CSAPForm,Ext.get('txtCatatan_CSAPForm').getValue(),Ext.get('txtReferensi_CSAPForm').getValue(),selectUnitKerja_CSAPForm);
							}else
							{
								viewHistoryApprovedAP();
							}
						}
					}
				}
				, '-', '->', '-',
				{
					text: 'Cetak',
					tooltip: 'Print',
					iconCls: 'print',
					hidden:false,
					handler: function() {Cetak_CSAPForm()}
				}
			]
		}
	);  // end Head panel
	
	var GDtabDetail_CSAPForm = new Ext.TabPanel
	(
		{
			region: 'center',
			id: 'GDtabDetail_CSAPForm',
			activeTab: 0,
			//anchor: '100% 46%',
			anchor: '100% 47%',
			border: false,
			plain: true,
			defaults: 
			{
				autoScroll: false
			},
			items:GetDTLTRGrid_CSAPForm(),			
			tbar: 
			[
			{
				text: 'Tambah Baris',
				id:'btnTambahBaris',
				tooltip: 'Tambah Record Baru ',
				iconCls: 'add',				
				handler: function() 
				{ 
					if(Ext.getCmp('ChkApproveEntry_CSAPForm').getValue() == true || Ext.getCmp('ChkApproveEntry_CSAPForm').getValue() == 'true'){
						ShowPesanError_CSAPForm('Pembayaran sudah di approve, tambah baris tidak dapat dilakukan!','Error');
					} else{
						TambahBaris_CSAPForm();
					}
				}
			}, '-', 
			{
				text: 'Hapus Baris',
				id:'btnHapusBaris',
				tooltip: 'Remove the selected item',
				iconCls: 'remove',
				handler: function()
					{
						if(Ext.getCmp('ChkApproveEntry_CSAPForm').getValue() == true || Ext.getCmp('ChkApproveEntry_CSAPForm').getValue() == 'true'){
							ShowPesanError_CSAPForm('Pembayaran sudah di approve, tambah baris tidak dapat dilakukan!','Error');
						} else{
							if (dsDTLTRList_CSAPForm.getCount() > 0 )
							{						
								if (cellSelectedDet_CSAPForm != undefined)
								{
									if(CurrentTR_CSAPForm != undefined)
									{
										HapusBaris_CSAPForm();
									}
								}
								else
								{
									ShowPesanWarning_CSAPForm('Silahkan pilih dahulu baris yang akan dihapus','Hapus baris');
								}
							}
						}
					}
			}
		] 
	}
);
	var Total = new Ext.Panel
	(
		{
			frame: false,
			layout: 'form',
			width: 500,
			border:false,
			id:'PnlTotal_CSAPForm',
			labelAlign:'right',
			labelWidth:50,
			style: 
			{
				'margin-top': '5.9px',
				'margin-left': '457px'
			},
			items: 
			[
				{
					xtype: 'textfield',
					id:'txtTotal_CSAPForm',
					name:'txtTotal_CSAPForm',
					fieldLabel: 'Total ',
					readOnly:true,
					style:
					{	
						'text-align':'right',
						'font-weight':'bold'
					},
					value:'0',
					width: 185
				}
			]
		}
	);
	var Formload_CSAPForm = new Ext.Panel
	(
		{
			id: 'Formload_CSAPForm',
			region: 'center',
			width:'100%',
			anchor:'100%',
			layout: 'form',
			title: '',
			bodyStyle: 'padding:15px',
			border: true,
			bodyStyle: 'background:#FFFFFF;',
			shadhow: true,
			items: [pnlTR_CSAPForm, GDtabDetail_CSAPForm,Total]							  

		}
	);
	
	return Formload_CSAPForm						
};
 //---------------------------------------------------------------------------------------///
 
function Cetak_CSAPForm()
{
	var strKriteria;
	if (Ext.get('txtNo_CSAPForm').dom.value !='' )
	{
		strKriteria = Ext.get('txtNo_CSAPForm').dom.value + '##'; //01
		strKriteria += 1+'##';
		strKriteria += Ext.get('txtTerimaDari_CSAPForm').dom.value + '##'; //02
		//strKriteria +=  selectAktivaLancar_CSAPForm + '-' + Ext.get('comboAktivaLancar_CSAPForm').dom.value +'##'; //03
		strKriteria += Ext.getCmp('txtTotal_CSAPForm').getValue() + '##'; //05
		strKriteria += Ext.get('comboAktivaLancar_CSAPForm').dom.value +'##'; //03
		strKriteria +=ShowDate(Ext.getCmp('dtpTanggal_CSAPForm').getValue()) + '##'; //04
		// strKriteria += '##'+1+'##';	
		ShowReport('', '010520', strKriteria);	
		
	};
};
 
 
function GetDTLTRGrid_CSAPForm() 
{
	// grid untuk detail transaksi	
	var fldDetail = ['csap_number','csap_date','line','account','desc','value','posted', 'name']
	
	dsDTLTRList_CSAPForm = new WebApp.DataStore({ fields: fldDetail })
	dsTmp_CSAPForm= new WebApp.DataStore({ fields: fldDetail })
	
	gridDTLTR_CSAPForm = new Ext.grid.EditorGridPanel
	(
		{
			title: 'Dari Detail ' + NamaForm_CSAPForm,
			stripeRows: true,
			id:'Dttlgrid_CSAPForm',
			store: dsDTLTRList_CSAPForm,
			border: false,
			columnLines:true,
			frame:true,
			anchor: '100% 100%',			
			height	:90,
			sm: new Ext.grid.CellSelectionModel
			(
				{
					singleSelect: true,
					listeners: 
					{
						cellselect: function(sm, row, rec)
						{
							cellSelectedDet_CSAPForm =dsDTLTRList_CSAPForm.getAt(row);
							CurrentTR_CSAPForm.row = row;
						}
					}
				}
			),
			cm: TRDetailColumModel_CSAPForm()
			, viewConfig: 
			{
				forceFit: true
			}
		}
	);
			
	return gridDTLTR_CSAPForm;
};

function RefreshDataDetail_CSAPForm(no,tgl)
{	
	Ext.Ajax.request({
		url: baseURL + "index.php/keuangan/functionPembayaranHutang/initListDetailAccount",
		params: {
			csap_number:no,
			csap_date:tgl
		},
		success: function(o) {
			var cst = Ext.decode(o.responseText);
			if (cst.success === true) 
			{
				dsDTLTRList_CSAPForm.removeAll();
				var recs=[],
				recType=dsDTLTRList_CSAPForm.recordType;
				for(var i=0; i<cst.total_record; i++){
					recs.push(new recType(cst.listData[i]));						
				}
				dsDTLTRList_CSAPForm.add(recs);
				gridDTLTR_CSAPForm.getView().refresh();
			}
			else {
				ShowPesanError_CSAPForm('Gagal membaca list detail penerimaan piutang!' , 'Simpan Data');
			}
		}
	})
}
//---------------------------------------------------------------------------------------///
			
function TRDetailColumModel_CSAPForm() 
{
	return new Ext.grid.ColumnModel
	(
		[
			new Ext.grid.RowNumberer(), 
			{
				id: 'Account_CSAPForm',
				name: 'Account_CSAPForm',
				header: "Account",
				dataIndex: 'account',
				sortable: false,
				anchor: '10%',
				editor: new Ext.form.TextField
				(
					{
						id:'fieldAcc_CSAPForm',
						allowBlank: false,
						enableKeyEvents : true,
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									LookUpAccount_APEntry(" Where A.Account like '" 
										+ Ext.get('fieldAcc_CSAPForm').dom.value + "%'  AND A.TYPE ='D'  ");
								} 
							}
						}
					}
				),
				width: 70
			}, 
			{
				id: 'Name_CSAPForm',
				name: 'Name_CSAPForm',
				header: "Nama Account",
				dataIndex: 'name',
				anchor: '30%',
				editor: new Ext.form.TextField
				(
					{
						id:'fieldAccName_CSAPForm',
						allowBlank: false,
						enableKeyEvents : true,
						listeners: 
						{ 
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									LookUpAccount_APEntry(" Where A.Name like '%" 
										+ Ext.get('fieldAccName_CSAPForm').dom.value + "%'  AND A.TYPE ='D'  ");									
								} 
							}
						}
					}
				)
			}, 
			{
				id: 'DESC_CSAPForm',
				name: 'DESC_CSAPForm',
				header: "Keterangan",
				anchor: '30%',
				dataIndex: 'desc', 
				editor: new Ext.form.TextField
				(	
					{
						allowBlank: true
					}
				)
			}, 
			{
				id: 'Value_CSAPForm',				
				name: 'Value_CSAPForm',
				header: "Jumlah (Rp)",
				anchor: '15%',
				dataIndex: 'value', 
				align:'right',
				renderer: function(v, params, record) 
				{
					return formatCurrencyDec(record.data.value);					
				},	
				editor: new Ext.form.NumberField
				(
					{
						id:'fieldDB_CSAPForm',
						allowBlank: false,
						enableKeyEvents : true,
						listeners: 
						{ 
							keyDown: function(a,b,c){
								if(b.getKey()==13){
									var line = gridDTLTR_CSAPForm.getSelectionModel().selection.cell[0];
									var o = dsDTLTRList_CSAPForm.getRange()[line].data;		
									if (o.account != '' || o.account != undefined){
										CalcTotal_CSAPForm();	
										TambahBaris_CSAPForm();
									}		
								}
							}
						}
					}
				)
			},
			{
				id: 'Value_CSARForm',	
				header: "LINE",
				dataIndex: 'line', 
				hidden:true,
			}
		]
	)
};

//---------------------------------------------------------------------------------------///
function DataInit_CSAPForm(rowdata) 
{
	DataAddNew_CSAPForm = false;
	tempstrKdUser=rowdata.kd_user;
	Ext.get('txtNo_CSAPForm').dom.readOnly=true;
	Ext.get('cboVendorEntry_CSAPForm').dom.value = rowdata.vendor;
	selectCboEntryVendor_CSAPForm = rowdata.vend_code;
	
	Ext.get('comboBayar_CSAPForm').dom.value= rowdata.payment;
	selectBayar_CSAPForm = rowdata.pay_code;
	Ext.get('comboAktivaLancar_CSAPForm').dom.value= rowdata.account + ' - '+rowdata.name;
	selectAktivaLancar_CSAPForm = rowdata.account;

    Ext.get('txtNo_CSAPForm').dom.value = rowdata.csap_number;
	
    Ext.get('txtTerimaDari_CSAPForm').dom.value = rowdata.vendor;
	Ext.get('dtpTanggal_CSAPForm').dom.value = ShowDate(rowdata.csap_date);  
	Ext.getCmp('dtpTanggal_CSAPForm').disable();  
	
	Ext.get('txtTotal_CSAPForm').dom.value = formatCurrencyDec(rowdata.amount);
	Ext.get('txtCatatan_CSAPForm').dom.value = rowdata.notes;
	Ext.get('txtNoPembayaran_CSAPForm').dom.value = rowdata.pay_no;
	Ext.getCmp('cboPengembalian_CSAPForm').setValue('');
	RefreshDataDetail_CSAPForm(rowdata.csap_number,rowdata.csap_date);
	
	CekApp_CSAPForm=rowdata.posted;
	if(rowdata.posted == 't'){		
		Ext.getCmp('ChkApproveEntry_CSAPForm').setValue(true);
		Ext.getCmp('btnSimpan').disable();
		Ext.getCmp('btnSimpanKeluar').disable();
		Ext.getCmp('btnHapus').disable();
	} else{
		Ext.getCmp('ChkApproveEntry_CSAPForm').setValue(false);
		Ext.getCmp('btnSimpan').enable();
		Ext.getCmp('btnSimpanKeluar').enable();
		Ext.getCmp('btnHapus').enable();
	}
	
	Ext.get('txtNo_CSAPForm').dom.readOnly=true;
	Ext.getCmp('cboVendorEntry_CSAPForm').setReadOnly(true);
	Ext.getCmp('comboBayar_CSAPForm').setReadOnly(true);
	Ext.getCmp('comboAktivaLancar_CSAPForm').setReadOnly(true);
};

//---------------------------------------------------------------------------------------///
function AddNew_CSAPForm() 
{
	DataAddNew_CSAPForm = true;	
	// tempstrKdUser=strKdUser;
	Ext.get('txtNo_CSAPForm').dom.readOnly=false;
	Ext.getCmp('dtpTanggal_CSAPForm').enable();  	
	Ext.get('txtNo_CSAPForm').dom.value = '';		
	Ext.get('txtTerimaDari_CSAPForm').dom.value = '';
	Ext.get('txtNoPembayaran_CSAPForm').dom.value = '';
	Ext.get('txtTotal_CSAPForm').dom.value = '0';
	Ext.get('txtCatatan_CSAPForm').dom.value = 'Summary from cashier post : Payable AP';	
	Ext.get('comboBayar_CSAPForm').dom.value='';
	Ext.get('comboAktivaLancar_CSAPForm').dom.value = '';
	Ext.getCmp('cboPengembalian_CSAPForm').setValue('0');	
	Ext.get('dtpTanggal_CSAPForm').dom.value = ShowDateAkuntansi(now_CSAPForm);
	Ext.get('cboVendorEntry_CSAPForm').dom.value = '';
	Ext.getCmp('cboVendorEntry_CSAPForm').setValue('');
	selectCboEntryVendor_CSAPForm = '';
	dsDTLTRList_CSAPForm.removeAll();
	rowSelected_CSAPForm=undefined;	
	CekApp_CSAPForm=0;

	Ext.get('txtReferensi_CSAPForm').dom.value = '';
	Ext.getCmp('ChkApproveEntry_CSAPForm').setValue(false);
	Ext.getCmp('btnSimpan').enable();
	Ext.getCmp('btnSimpanKeluar').enable();
	Ext.getCmp('btnHapus').enable();
};

function getParam_CSAPForm() 
{
	var params = 
	{
		csap_number:Ext.get('txtNo_CSAPForm').getValue(),
		csap_date:Ext.get('dtpTanggal_CSAPForm').getValue(),
		kd_vendor:selectCboEntryVendor_CSAPForm,
		account:selectAktivaLancar_CSAPForm,
		pay_code:selectBayar_CSAPForm,
		pay_no:Ext.get('txtNoPembayaran_CSAPForm').getValue(),
		amount:CalcTotalDetailGrid_CSAPForm(),
		notes:Ext.get('txtCatatan_CSAPForm').getValue(),		
		reference:Ext.get('txtReferensi_CSAPForm').getValue(),		
		jmllist:dsDTLTRList_CSAPForm.getCount(),
		kd_unit_kerja: selectUnitKerja_CSAPForm,
		currency:'IDR',
		kurs:'1',
	};
	params['jumlah']=dsDTLTRList_CSAPForm.getCount();
	for(var i = 0 ; i < dsDTLTRList_CSAPForm.getCount();i++)
	{
		params['account-'+i]=dsDTLTRList_CSAPForm.data.items[i].data.account;
		params['desc-'+i]=dsDTLTRList_CSAPForm.data.items[i].data.desc;
		params['value-'+i]=dsDTLTRList_CSAPForm.data.items[i].data.value;
		params['line-'+i]=dsDTLTRList_CSAPForm.data.items[i].data.line;
	}
	return params
};

//---------------------------------------------------------------------------------------///

function getAmount_CSAPForm(dblNilai)
{
    var dblAmount;
    dblAmount = dblNilai.replace('Rp.', '')
    for (var i = 0; i < dblAmount.length; i++) {
        var x = dblAmount.substr(i, 1)
        if (x === '.') {
            dblAmount = dblAmount.replace('.', '');
        }
    }    
    // return Ext.num(dblAmount)
    return dblAmount
};

function getItemPanelInput_CSAPForm(lebar)
{
	var items = 
	{
		layout:'fit',
		anchor:'100%',
		width: lebar - 36,
		height: 160,//130,
		labelAlign: 'right',
		bodyStyle: 'padding:7px 7px 7px 7px',
		items:
		[
			{
				columnWidth:.9,
				width:lebar-36,
				layout: 'form',
				border:false,
				items: 
				[
					getItemPanelNo_CSAPForm(lebar),
					getItemPanelTerimaDari_CSAPForm(lebar),
					getItemPanelAktivaLancar_CSAPForm(lebar),
					getItemPanelPayModePayNumber_CSAPForm(lebar),
					getItemPanelCatatan_CSAPForm(lebar),
					getItemPanelPengembalian_CSAPForm(lebar)
				]
			}
		]
	};
	return items;			
};

function getItemPanelAktivaLancar_CSAPForm(lebar)
{
	var items = 			
	{
	layout:'column',
	border:false,
	width:lebar-36,
	items:
		[
			{
				columnWidth:.5,
				layout: 'form',
				border:false,
				labelWidth:111,
				items: 
				[
					mcomboAktivaLancar_CSAPForm(),					
				]
			},
			{
				columnWidth:.5,
				region:'Right',
				border:false,				
				layout: 'form',
				items: 				
				[
					{
						xtype:'textfield',
						fieldLabel: 'No. Pembayaran ',
						name: 'txtNoPembayaran_CSAPForm',
						id: 'txtNoPembayaran_CSAPForm',
						anchor:'96%'						
					}
				]
			}
		]
	}
	return items;	
}; 

function getItemPanelCatatan_CSAPForm(lebar)
{
	var items = 			
	{
	layout:'column',
	border:false,
	width:lebar-36,
	items:
		[
			{
				columnWidth:0.98,
				layout: 'form',
				border:false,
				labelWidth:111,
				items: 
				[
					{
						xtype:'textarea',
						fieldLabel: 'Keterangan ',
						name: 'txtCatatan_CSAPForm',
						id: 'txtCatatan_CSAPForm',
						anchor:'99.9%',
						height: 39,
						value:'Summary from cashier post : Payable AP',
						autoCreate: {tag: 'input', maxlength: '255'}
					}
				]
			}
		]
	}
	return items;	
}; 

function getItemPanelPengembalian_CSAPForm(lebar)
{
	var items = 			
	{
	layout:'column',
	border:false,
	width:lebar-36,
	items:
		[
			{
				columnWidth:.5,
				layout: 'form',
				border:false,
				labelWidth:111,
				items: 
				[
					mComboPengembalian_CSAPForm()					
				]
			}
		]
	}
	return items;	
}; 



function getItemPanelNo_CSAPForm(lebar)
{
	var items = 			
	{
	layout:'column',
	border:false,
	width:lebar-36,
	items:
		[
			{
				columnWidth:.5,
				layout: 'form',
				border:false,
				labelWidth:111,
				items: 
				[
					{
						xtype:'textfield',
						fieldLabel: 'Nomor ',
						name: 'txtNo_CSAPForm',
						id: 'txtNo_CSAPForm',
						readOnly:false,
						anchor:'95%',
						listeners: 
						{
							'blur' : function()
							{	
								if (DataAddNew_CSAPForm == true)	{
									CekDataInput_CSAPForm();
								}							
							},
							'specialkey' : function()
							{
								if (Ext.EventObject.getKey() === 13) 
								{
									if (DataAddNew_CSAPForm == true)	{
										CekDataInput_CSAPForm();
									}
								}
							}
						}	
					}
				]
			},
			{
				columnWidth:.5,
				layout: 'form',
				border:false,
				labelWidth:100,
				items: 
				[
					{
						xtype:'checkbox',
						fieldLabel: 'Approve ',
						name: 'ChkApproveEntry_CSAPForm',
						id: 'ChkApproveEntry_CSAPForm',
						disabled:true,
						anchor:'95%'
					}
				]
			}
		]
	}
	return items;	
};

function getItemPanelPayModePayNumber_CSAPForm(lebar)
{
	var items = 			
	{
	layout:'column',
	width:lebar-36,
	border:false,
	items:
	[
		{
			columnWidth:.5,
			layout: 'form',
			border:false,
			labelWidth:111,
			items: 
			[mcomboBayar_CSAPForm()]			
		},
		{
			columnWidth:.5,
			layout: 'form',
			border:false,
			items: 
			[
				{
					xtype:'textfield',
					fieldLabel: 'Referensi ',
					name: 'txtReferensi_CSAPForm',
					id: 'txtReferensi_CSAPForm',
					readOnly: true,
					hidden: true,
					width:217,
					listeners:
					{ 
						focus: function() 
						{
							mCtrFokus_CSAPForm="txtReferensi_CSAPForm";
						},
						'specialkey': function()
						{
							if(Ext.EventObject.getKey() === 13)
							{
								var StrKriteria="";
								if (selectUnitKerja_CSAPForm != "")
								{
									StrKriteria = " WHERE AC.KATEGORI='5' and APP_SP3D = 1  and TAHAP_PROSES in(3,4)  AND SP3.KD_UNIT_KERJA='" + selectUnitKerja_CSAPForm + "' "
								}
								else
								{
									StrKriteria = " WHERE AC.KATEGORI='5' and APP_SP3D = 1 and TAHAP_PROSES in(3,4) "
								}							
								var p = new mRecord_CSAPForm
								(
									{
										ACCOUNT: '',
										NAMAACCOUNT: '',
										DESCRIPTION: Ext.get('txtCatatan_CSAPForm').getValue(),
										VALUE: '',								
										CSAPD_LINE:''
									}
								);							
								FormLookupsp3d(dsDTLTRList_CSAPForm,p, StrKriteria,nASALFORM_CSAPForm,thn);
							}
						}
						
					}
				},
			]
		}
	]
	}
	return items;	
}

function getItemPanelTerimaDari_CSAPForm(lebar)
{
	var items = 			
	{
	layout:'column',
	width:lebar-36,
	border:false,
	items:
		[
			{
				columnWidth:.5,
				layout: 'form',
				border:false,
				labelWidth:111,
				items: 
				[
					mCboEntryVendorCSAPForm(),
					{
						xtype:'textfield',
						fieldLabel: 'Terima Dari ',
						name: 'txtTerimaDari_CSAPForm',
						id: 'txtTerimaDari_CSAPForm',
						anchor:'95%',
						hidden:true
					}
				]
			},
			{
				columnWidth:.5,
				layout: 'form',
				border:false,
				items: 
				[
					{
						xtype: 'datefield',
                        fieldLabel: 'Tanggal ',
                        id: 'dtpTanggal_CSAPForm',
                        name: 'dtpTanggal_CSAPForm',
                        format: 'd/M/Y',
						value:now_CSAPForm,
                        anchor: '70%'
					}
				]
			}
		]
	}
	return items;	
}

function mcomboAktivaLancar_CSAPForm()
{
	var comboAktivaLancar_CSAPForm;
	var Field = ['account','name','groups','akun'];
	dsAktivaLancar_CSAPForm = new WebApp.DataStore({ fields: Field });
	
	Ext.Ajax.request({
		url: baseURL + "index.php/keuangan/functionGeneral/getAktivaLancar",
		params: {query:''},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			comboAktivaLancar_CSAPForm.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsAktivaLancar_CSAPForm.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsAktivaLancar_CSAPForm.add(recs);
			}
		}
	});
	
	comboAktivaLancar_CSAPForm = new Ext.form.ComboBox
	(
		{
			id:'comboAktivaLancar_CSAPForm',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih Kas / Bank...',
			fieldLabel: 'Dari Kas / Bank ',			
			align:'Right',
			anchor:'95%',
			listWidth:400,
			store: dsAktivaLancar_CSAPForm,
			valueField: 'account',
			displayField: 'akun',
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectAktivaLancar_CSAPForm=b.data.account ;
				} 
			}
		}
	);
	
	return comboAktivaLancar_CSAPForm;
} ;

function RefreshData_CSAPForm()
{			
	dsTRList_CSAPForm.load
	(
		{ 
			params: 
			{ 	
				Skip: 0,
				Take: selectCount_CSAPForm,
				Sort: 'CSAP_DATE',
				Sortdir: 'ASC',
				target:'viACC_CSAP',
				param: " "
			}
		}
	);
	return dsTRList_CSAPForm;
};

function mComboMaksData_CSAPForm()
{
  var cboMaksDataCSAPForm = new Ext.form.ComboBox
	(
		{
			id:'cboMaksDataCSAPForm',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			fieldLabel: 'Maks.Data ',			
			width:50,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[1, 50], [2, 100],[3, 200],[4, 500],[5, 1000]]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectCount_CSAPForm,
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectCount_CSAPForm=b.data.displayText ;
					RefreshDataFilter_CSAPForm(false);
				} 
			},
			hidden:true
		}
	);
	return cboMaksDataCSAPForm;
};

function mCboEntryVendorCSAPForm()
{
	var Fields = ['Vend_Code', 'Vendor', 'VendorName'];
    dsCboEntryVendor_CSAPForm = new WebApp.DataStore({ fields: Fields });
    
	dsCboEntryVendor_CSAPForm.load
	(
		{ 
			params: 
			{ 
				Skip: 0, 
				Take: 1000, 
				Sort: 'Vendor', 
				Sortdir: 'ASC', 
				target: 'viewCboVendLengkap',
				param: ''
			} 
		}
	);
	
    var cboVendorEntry_CSAPForm = new Ext.form.ComboBox
	(
		{
		    id: 'cboVendorEntry_CSAPForm',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    emptyText: 'Pilih vendor ...',
		    align: 'right',
		    anchor:'95%',
		    listWidth:500,
		    store: dsCboEntryVendor_CSAPForm,
		    fieldLabel: 'Vendor ',
		    valueField: 'Vend_Code',
		    displayField: 'VendorName',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			       selectCboEntryVendor_CSAPForm = b.data.Vend_Code;
			    }
			}
		}
	);
	
	return cboVendorEntry_CSAPForm;
}
function RefreshDataFilter_CSAPForm(mBol) 
{   
	Ext.Ajax.request({
		url: baseURL + "index.php/keuangan/functionPembayaranHutang/getInitList",
		params: {
			tgl_awal : Ext.getCmp('dtpTglAwalFilter_CSAPForm').getValue(),
			tgl_akhir : Ext.getCmp('dtpTglAkhirFilter_CSAPForm').getValue(),
			posted : Ext.getCmp('chkFilterApproved_CSAPForm').getValue(),
			csap_number : Ext.getCmp('txtNoFilter_CSAPForm').getValue(),
		},
		success: function(o) {
			var cst = Ext.decode(o.responseText);
			if (cst.success === true) 
			{
				dsTRList_CSAPForm.removeAll();
				var recs=[],
					recType=dsTRList_CSAPForm.recordType;
				for(var i=0; i<cst.listData.length; i++){
					recs.push(new recType(cst.listData[i]));						
				}
				dsTRList_CSAPForm.add(recs);
				grListTR_CSAPForm.getView().refresh();
			}
			else {
				ShowPesanError_CSAPForm('Gagal menampilkan list penerimaan piutang!', 'Simpan Data');
			}
		}
	})
};


function mcomboBayar_CSAPForm()
{
	var comboBayar_CSAPForm;
	var Field = ['pay_code', 'payment'];
	dsBayar_CSAPForm = new WebApp.DataStore({ fields: Field });
	
	Ext.Ajax.request({
		url: baseURL + "index.php/keuangan/functionGeneral/getJenisBayar",
		params: {query:''},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			comboBayar_CSAPForm.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsBayar_CSAPForm.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsBayar_CSAPForm.add(recs);
			}
		}
	});

	comboBayar_CSAPForm = new Ext.form.ComboBox
	(
		{
			id:'comboBayar_CSAPForm',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih Jenis Pembayaran...',
			fieldLabel: 'Jenis Pembayaran ',			
			align:'Right',
			anchor:'95%',
			store: dsBayar_CSAPForm,
			valueField: 'pay_code',
			displayField: 'payment',
			listeners:  
			{
				'select': function(a,b,c)
				{   
					selectBayar_CSAPForm=b.data.pay_code ;
				} 
			}
		}
	);
	
	return comboBayar_CSAPForm;
} ;

function Approve_CSAPForm(TglApprove, NoteApprove) 
{
	Ext.Ajax.request
	(
		{
			url: "./Datapool.mvc/UpdateDataObj",
			params:  getParamApprove_CSAPForm(TglApprove, NoteApprove), 
			success: function(o) 
			{				
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					ShowPesanInfo_CSAPForm('Data berhasil di Approve','Approve');
					Ext.getCmp('ChkApproveEntry_CSAPForm').setValue(true);	
					RefreshDataFilter_CSAPForm(false);								
				}
				else if (cst.success === false && cst.pesan === 0 )
				{
					ShowPesanWarning_CSAPForm('Data tidak berhasil di Approve','Edit Data');
				}
				else 
				{
					ShowPesanError_CSAPForm('Data tidak berhasil di Approve :' + cst.pesan,'Approve');
				}										
			}
		}
	)
}

function Datasave_CSAPForm(IsExit) 
{
	if (ValidasiEntry_CSAPForm('Simpan Data') == 1 )
	{
		//alert('yow');
		Ext.Ajax.request({
			url: baseURL + "index.php/keuangan/functionPembayaranHutang/save",
			params: getParam_CSAPForm(),
			success: function(o) {
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					ShowPesanInfo_CSAPForm('Data berhasil di simpan', 'Simpan Data');
					if (IsExit===false)
					{
						Ext.get('txtNo_CSAPForm').dom.value = cst.csap_number;
					}
					RefreshDataFilter_CSAPForm(false);
					RefreshDataDetail_CSAPForm(Ext.get('txtNo_CSAPForm').getValue(),Ext.get('dtpTanggal_CSAPForm').getValue());
					DataAddNew_CSAPForm=false;
					Ext.get('txtNo_CSAPForm').dom.readOnly=true;
				}
				else if (cst.success === false && cst.pesan === 0) 
				{
					ShowPesanWarning_CSAPForm('Data tidak berhasil di simpan ' + cst.pesan , 'Simpan Data');
				}
				else {
					ShowPesanError_CSAPForm('Data tidak berhasil di simpan, Error : ' + cst.pesan, 'Simpan Data');
				}
			}
		})
	}
};

function ValidasiEntry_CSAPForm(modul)
{
	var x = 1;
	// if(Ext.get('txtNo_CSAPForm').getValue()=='')
	// {
		// ShowPesanWarning_CSAPForm('Nomor belum di isi',modul);
		// x=0;
	// }
	// else 
	if(selectCboEntryVendor_CSAPForm == '' || selectCboEntryVendor_CSAPForm == undefined)
	{
		ShowPesanWarning_CSAPForm('Vendor belum dipilih',modul);
		x=0;
	}else if(Ext.get('comboAktivaLancar_CSAPForm').getValue()=='')
	{
		ShowPesanWarning_CSAPForm('Kas / Bank belum di isi',modul);
		x=0;
	}else if (Ext.get('comboBayar_CSAPForm').getValue()=='')
	{
		ShowPesanWarning_CSAPForm('Jenis pembayaran belum di isi',modul);
		x=0;
	}else if ( dsDTLTRList_CSAPForm.getCount() <= 0)
	{
		ShowPesanWarning_CSAPForm('Detail Payable AP belum di isi',modul);
		x=0;
	}else if (Ext.get('txtCatatan_CSAPForm').getValue().length > 255)
	{
		ShowPesanWarning_CSAPForm('Keterangan Tidak boleh lebih dari 255 Karakter',modul);
		x=0;
	};
	
	for(var i = 0 ; i < dsDTLTRList_CSAPForm.getCount();i++)
	{
		var o = dsDTLTRList_CSAPForm.data.items[i].data;
		if(o.account == '' || o.account == undefined){
			dsDTLTRList_CSAPForm.removeAt(i);
		}
	}
	return x;
};

function ValidasiApprove_CSAPForm(modul)
{
	var x = 1;
	if(Ext.get('txtNo_CSAPForm').getValue() == '')
	{
		ShowPesanWarning_CSAPForm('Data belum disimpan',modul);
		x=0;
	}
	return x;
};

function ValidasiPilihTagihan_CSAPForm(modul)
{
	var x = 1;
	if(selectCboEntryVendor_CSAPForm == '' || selectCboEntryVendor_CSAPForm == undefined)
	{
		ShowPesanWarning_CSAPForm('Vendor belum dipilih',modul);
		x=0;
	}
	return x;
};
function ShowPesanWarning_CSAPForm(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING
		}
	);
};

function ShowPesanError_CSAPForm(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.ERROR
		}
	);
};

function ShowPesanInfo_CSAPForm(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.INFO
		}
	);
};

function HapusBaris_CSAPForm()
{
	if (cellSelectedDet_CSAPForm.data.account != '' && cellSelectedDet_CSAPForm.data.account != undefined)
	{
		Ext.Msg.show
		(
			{
			   title:'Hapus Baris',
			   msg: 'Apakah baris ini akan dihapus ?' + ' ' + 'Baris :'+ ' ' + (CurrentTR_CSAPForm.row + 1) + ' dengan Account : '+ ' ' + cellSelectedDet_CSAPForm.data.account ,
			   buttons: Ext.MessageBox.YESNO,
			   fn: function (btn) 
			   {			
				   if (btn =='yes') 
					{
						var line = gridDTLTR_CSAPForm.getSelectionModel().selection.cell[0];
						var o = dsDTLTRList_CSAPForm.getRange()[line].data;
						if (o.line != '' && o.line != undefined){
							dsDTLTRList_CSAPForm.removeAt(CurrentTR_CSAPForm.row);
					    	CalcTotal_CSAPForm();
							HapusBaris_CSAPFormDB(o.line);
					    	cellSelectedDet_CSAPForm = undefined;
						} else{
							dsDTLTRList_CSAPForm.removeAt(CurrentTR_CSAPForm.row);
						    CalcTotal_CSAPForm();
						    cellSelectedDet_CSAPForm = undefined;
						}
					    
					} 
			   },
			   icon: Ext.MessageBox.QUESTION
			}
		);
	}
	else
	{
	    dsDTLTRList_CSAPForm.removeAt(CurrentTR_CSAPForm.row);
	    CalcTotal_CSAPForm();
	    cellSelectedDet_CSAPForm = undefined;
	}
	
	
};

function TambahBaris_CSAPForm()
{
	var records = new Array();
	records.push(new dsDTLTRList_CSAPForm.recordType());
	dsDTLTRList_CSAPForm.add(records);
	
	var row =dsDTLTRList_CSAPForm.getCount()-1;
	gridDTLTR_CSAPForm.startEditing(row, 1);
};


function DataDelete_CSAPForm() 
{
	if (ValidasiEntry_CSAPForm('Hapus Data') == 1 )
	{	
		Ext.Ajax.request
		(
			{
				url: "./Datapool.mvc/DeleteDataObj",
				params:  getParam_CSAPForm(), 
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true)					
					{
							ShowPesanInfo_CSAPForm('Data berhasil di hapus','Hapus Data');
							RefreshDataFilter_CSAPForm(false);
							AddNew_CSAPForm();
					}
					else if (cst.success === false && cst.pesan === 0 )
					{
						ShowPesanWarning_CSAPForm('Data tidak berhasil di hapus, data tersebut belum ada','Hapus Data');
					}
					else 
					{
						ShowPesanError_CSAPForm('Data tidak berhasil di hapus','Hapus Data');
					}
				}
			}
		)
	}
};


function CekArrKosong_CSAPForm()
{
	var x=1;
	for(var i = 0 ; i < dsDTLTRList_CSAPForm.getCount();i++)
	{
		if (dsDTLTRList_CSAPForm.data.items[i].data.ACCOUNT=='' || dsDTLTRList_CSAPForm.data.items[i].data.ACCOUNT==undefined){
			x=0;
		}

	}		
	return x;
};

function getArrDetail_CSAPForm()
{
	var x='';
	for(var i = 0 ; i < dsDTLTRList_CSAPForm.getCount();i++)
	{
		var y='';
		var z='@@##$$@@';
		
		if (dsDTLTRList_CSAPForm.data.items[i].data.CSAPD_LINE==="")
		{
			y = "Line=" + 0 
		}
		else
		{	
			y = 'Line=' + dsDTLTRList_CSAPForm.data.items[i].data.CSAPD_LINE
		}		
		y += z + 'ACCOUNT='+dsDTLTRList_CSAPForm.data.items[i].data.ACCOUNT
		y += z + 'DESC=' + dsDTLTRList_CSAPForm.data.items[i].data.DESC
		y += z + 'VALUE=' + dsDTLTRList_CSAPForm.data.items[i].data.VALUE
		y += z + 'POSTED=' + dsDTLTRList_CSAPForm.data.items[i].data.POSTED
		if (i === (dsDTLTRList_CSAPForm.getCount()-1))
		{
			x += y 
		}
		else
		{
			x += y + '##[[]]##'
		}
	}		
	return x;
};


function CalcTotal_CSAPForm(idx)
{
    var total=0;
	var nilai=0;
	
	for(var i=0;i < dsDTLTRList_CSAPForm.getCount();i++)
	{
		var o = dsDTLTRList_CSAPForm.data.items[i].data;
		if(o.value != undefined){
			total += parseInt(o.value);
		} 
	}	
	Ext.get('txtTotal_CSAPForm').dom.value=formatCurrencyDec(total);
};

function CalcTotalDetailGrid_CSAPForm()
{
    var total=0;
	var nilai=0;
	for(var i=0;i < dsDTLTRList_CSAPForm.getCount();i++)
	{
		var o = dsDTLTRList_CSAPForm.data.items[i].data;
		if(o.value != undefined){
			total += parseInt(o.value);
		} 
	}	
	return total;
};

function ButtonDisabled_CSAPForm(mBol)
{
	Ext.get('btnTambahBaris').dom.disabled=mBol;
	Ext.get('btnHapusBaris').dom.disabled=mBol;
	Ext.get('btnSimpan').dom.disabled=mBol;
	Ext.get('btnSimpanKeluar').dom.disabled=mBol;
	Ext.get('btnHapus').dom.disabled=mBol;	
	Ext.get('btnApprove').dom.disabled=mBol;	
};


function HapusBaris_CSAPFormDB(line) 
{	
	Ext.Ajax.request({
		url: baseURL + "index.php/keuangan/functionPembayaranHutang/deleteRow",
		params:{
			csap_number : Ext.get('txtNo_CSAPForm').getValue(),
			csap_date : Ext.get('dtpTanggal_CSAPForm').getValue(),
			amount : CalcTotalDetailGrid_CSAPForm(),
			line : line
		} ,
		failure: function(o)
		{
			ShowPesanError_CSAPForm('Hubungi Admin', 'Error');
		},	
		success: function(o) 
		{
			var cst = Ext.decode(o.responseText);
			if (cst.success === true) 
			{
				CalcTotal_CSAPForm();
				ShowPesanInfo_CSAPForm("Data berhasil dihapus.","Information");
			}
			else 
			{
				ShowPesanError_CSAPForm('Gagal melakukan penghapusan', 'Error');
			};
		}
	})
	
};

function getParam_CSAPFormHapusBaris(Line) 
{
	var params = 
	{
		Table: 'viACC_CSAP',   		
		IS_DETAIL:1,
		CSAP_NUMBER:Ext.get('txtNo_CSAPForm').getValue(),
		CSAP_DATE:Ext.get('dtpTanggal_CSAPForm').getValue(),
		CSAPD_LINE:Line,		
	};
	return params
};

function mComboPengembalian_CSAPForm()
{
  var cboPengembalian_CSAPForm = new Ext.form.ComboBox
	(
		{
			id:'cboPengembalian_CSAPForm',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'',
			hidden:true,
			fieldLabel: 'Pengembalian Dana',			
			width:80,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Ya'], [0, 'Tidak']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:'',
			listeners:  
			{
				'select': function(a,b,c)
				{   
					// selectCount_CSAPForm=b.data.displayText ;
					// RefreshDataFilter_CSAPForm(false);
				} 
			}
		}
	);
	return cboPengembalian_CSAPForm;
};

function GetVendorAcc_CSAPForm(param)
{
	 Ext.Ajax.request
	 (
		{
			url: "./Module.mvc/ExecProc",
			params: 
			{
				UserID: 'Admin',
				ModuleID: 'GetInfoVendor',
				Params:	param 
			},
			success: function(o) 
			{
			var cst = Ext.decode(o.responseText);
				if (cst.success === true )
				{
				 	StrAccVend_CSAPForm = cst.Account;
				 	strDue_CSAPForm=cst.Due_date
				 	StrNmAccVend_CSAPForm=cst.NameAcc;
				}
				else
				{
					ShowPesanInfoRekapSP3D	('Customer tidak ditemukan','Customer');
				}
			}

		}
		);
};

function getNoReferensi(strcsapno,strcsapdate)
{	
    Ext.Ajax.request
	(
		{
			url: "./Module.mvc/ExecProc",
			params: 
			{
				UserID: 'Admin',
				ModuleID: 'GetRefCsap',
				Params:	strcsapno +'##'+ FormatDateReport(strcsapdate) +'##' //GetParamProsesCekTotalRekap(param)			
			},
			success: function(o) 
			{
			  var cst = Ext.decode(o.responseText);
				if (cst.success === true )
				{

					Ext.getCmp('txtReferensi_CSAPForm').setValue(cst.reff);
				}
			}
		}
	);
}

////--------------FIND DIALOG-------------------------
var nowFind_CSAPForm = new Date();
var selectcboOperator_CSAPForm;
var selectcboField_CSAPForm;
//var frmFindDlg_CSAPForm= fnFindDlg_CSAPForm();
//frmFindDlg_CSAPForm.show();
var winFind_CSAPForm;

function fnFindDlg_CSAPForm()
{  	
    winFind_CSAPForm = new Ext.Window
	(
		{ 
			id: 'winFindx_CSAPForm',
			title: 'Find',
			closeAction: 'destroy',
			width:350,
			height: 200,					
			border: false,
			resizable:false,
			plain: true,
			layout: 'fit',
			iconCls: 'find',
			modal: true,
			items: [ItemFindDlg_CSAPForm()]
			
		}
	);

	winFind_CSAPForm.show();
	
    return winFind_CSAPForm; 
};

function ItemFindDlg_CSAPForm() 
{	
	var PnlFindDlg_CSAPForm = new Ext.Panel
	(
		{ 
			id: 'PnlFindDlg_CSAPForm',
			fileUpload: true,
			layout: 'anchor',
			// width:350,
			height: 90,
			anchor: '100%',
			bodyStyle: 'padding:5px',
			border: true,
			items: 
			[

				{
					xtype: 'compositefield',
					fieldLabel: '',
					anchor: '100%',
					items: 
					[
						{
							xtype: 'fieldset',
							title: '',
							width: 325,
							height: 125, 
							items: 
							[
								mComboFindField_CSAPForm(),
								mComboOperator_CSAPForm(),
								getItemFindKeyWord_CSAPForm(),
								getItemFindTgl_CSAPForm(),
								
				
							]
						}						
					]
				},
				{
					layout: 'hBox',
					border: false,
					defaults: { margins: '0 5 0 0' },
					style:{'margin-left':'21px','margin-top':'5px'},
					anchor: '100%',
					layoutConfig: 
					{
						padding: '3',
						pack: 'end',
						align: 'middle'
					},
					items: 
					[
						{
							xtype: 'button',
							text: 'Ok',
							width: 70,
							hideLabel: true,
							id: 'btnOkLap_CSAPForm',
							handler: function() 
							{
								if (ValidasiReport_CSAPForm()==1){
									RefreshDataFindRecord_CSAPForm()
								}
								
							}
						},
						{
							xtype: 'button',
							text: 'Cancel',
							width: 70,
							hideLabel: true,
							id: 'btnCancelLap_CSAPForm',
							handler: function() 
							{
								winFind_CSAPForm.close();
							}
						}
					]
				}
			]
		}
	);
 
    return PnlFindDlg_CSAPForm;
};

function RefreshDataFindRecord_CSAPForm() 
{   
	var strparam='';
	if(selectcboOperator_CSAPForm==1){

		if (selectcboField_CSAPForm=="CSA.CSAP_DATE"){

			strparam=" WHERE "+selectcboField_CSAPForm+" like '"+ ShowDateAkuntansi(Ext.getCmp('dtpTglFind_CSAPForm').getValue()) + "%' "; 

		} else{
			strparam=" WHERE "+selectcboField_CSAPForm+" like '"+ Ext.getCmp('txtKeyWordFind_CSAPForm').getValue() + "%' "; 

		}		
		
	} else if (selectcboOperator_CSAPForm==2){

		if (selectcboField_CSAPForm=="CSA.CSAP_DATE"){

			strparam=" WHERE "+selectcboField_CSAPForm+" = '"+ ShowDateAkuntansi(Ext.getCmp('dtpTglFind_CSAPForm').getValue()) + "' "; 

		} else{
			strparam=" WHERE "+selectcboField_CSAPForm+" = '"+ Ext.getCmp('txtKeyWordFind_CSAPForm').getValue() + "' "; 

		}

	} else if (selectcboOperator_CSAPForm==3){
		 
		if (selectcboField_CSAPForm=="CSA.CSAP_DATE"){

			strparam=" WHERE "+selectcboField_CSAPForm+" < '"+ ShowDateAkuntansi(Ext.getCmp('dtpTglFind_CSAPForm').getValue()) + "' "; 

		} else{
			strparam=" WHERE "+selectcboField_CSAPForm+" < '"+ Ext.getCmp('txtKeyWordFind_CSAPForm').getValue() + "' ";

		}

	} else if (selectcboOperator_CSAPForm==4){

		if (selectcboField_CSAPForm=="CSA.CSAP_DATE"){

			strparam=" WHERE "+selectcboField_CSAPForm+" > '"+ ShowDateAkuntansi(Ext.getCmp('dtpTglFind_CSAPForm').getValue()) + "' "; 

		} else{
			strparam=" WHERE "+selectcboField_CSAPForm+" > '"+ Ext.getCmp('txtKeyWordFind_CSAPForm').getValue() + "' ";

		}
		
	} else if (selectcboOperator_CSAPForm==5){
		if (selectcboField_CSAPForm=="CSA.CSAP_DATE"){

			strparam=" WHERE "+selectcboField_CSAPForm+" <= '"+ ShowDateAkuntansi(Ext.getCmp('dtpTglFind_CSAPForm').getValue()) + "' "; 

		} else{
			strparam=" WHERE "+selectcboField_CSAPForm+" <= '"+ Ext.getCmp('txtKeyWordFind_CSAPForm').getValue() + "' ";

		}
		
		
	} else if (selectcboOperator_CSAPForm==6){
		if (selectcboField_CSAPForm=="CSA.CSAP_DATE"){

			strparam=" WHERE "+selectcboField_CSAPForm+" >= '"+ ShowDateAkuntansi(Ext.getCmp('dtpTglFind_CSAPForm').getValue()) + "' "; 

		} else{
			strparam=" WHERE "+selectcboField_CSAPForm+" >= '"+ Ext.getCmp('txtKeyWordFind_CSAPForm').getValue() + "' ";

		}
		
	} else if (selectcboOperator_CSAPForm==7){
		if (selectcboField_CSAPForm=="CSA.CSAP_DATE"){

			strparam=" WHERE "+selectcboField_CSAPForm+" <> '"+ ShowDateAkuntansi(Ext.getCmp('dtpTglFind_CSAPForm').getValue()) + "' "; 

		} else{
			strparam=" WHERE "+selectcboField_CSAPForm+" <> '"+ Ext.getCmp('txtKeyWordFind_CSAPForm').getValue() + "' ";

		}
		
	} 
	
    // {  
		// dsTRList_CSAPForm.load
		// (
			// { 
				// params:  
				// {   
					// Skip: 0, 
					// Take: selectCount_CSAPForm, 
					// Sort: 'CSAP_DATE', 
					// Sortdir: 'ASC', 
					// target:'viACC_CSAP',
					// param: strparam
				// }			
			// }
		// );        
    // }
	
	Ext.Ajax.request({
		url: baseURL + "index.php/keuangan/functionPembayaranHutang/getDataFind",
		params: {
			criteria:strparam
		},
		success: function(o) {
			var cst = Ext.decode(o.responseText);
			if (cst.success === true) 
			{
				dsTRList_CSAPForm.removeAll();
				var recs=[],
					recType=dsTRList_CSAPForm.recordType;
				for(var i=0; i<cst.listData.length; i++){
					recs.push(new recType(cst.listData[i]));						
				}
				dsTRList_CSAPForm.add(recs);
				grListTR_CSAPForm.getView().refresh();
			}
			else {
				ShowPesanError_CSAPForm('Gagal menampilkan list penerimaan piutang!', 'Simpan Data');
			}
		}
	})
	
	
};


function ValidasiReport_CSAPForm()
{
	var x=1;
	
	if(selectcboField_CSAPForm == undefined || selectcboField_CSAPForm == "")
	{
		ShowPesanWarningFindDlg_CSAPForm('Field belum dipilih','Find');
		x=0;		
	};

	if(selectcboOperator_CSAPForm ==undefined || selectcboOperator_CSAPForm== "")
	{
		
		ShowPesanWarningFindDlg_CSAPForm('Operator belum di pilih','Find');
		x=0;
			
	};

	if(Ext.getCmp('txtKeyWordFind_CSAPForm').getValue()== "" && criteriaTanggalFindCSAP == false)
	{
		
		ShowPesanWarningFindDlg_CSAPForm('Keyword belum di isi','Find');
		x=0;
			
	};

	return x;
};

function ShowPesanWarningFindDlg_CSAPForm(str,modul)
{
	Ext.MessageBox.show
	(
		{
		   title: modul,
		   msg:str,
		   buttons: Ext.MessageBox.OK,
		   icon: Ext.MessageBox.WARNING,
		   width:180
		}
	);
};

function getItemFindTgl_CSAPForm() 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
		{
			xtype: 'compositefield',
			fieldLabel: '-',
			anchor: '100%',
			items: 
			[
				 {
					columnWidth: 1,
					layout: 'form',
					border: false,
					labelAlign: 'left',
					labelWidth: 100,
					items:
					[
						{
							xtype: 'datefield',
							fieldLabel: 'Periode ',
							id: 'dtpTglFind_CSAPForm',
							format: 'd/M/Y',
							disabled:true,
							value:now,
							width:190

						}
					]
		    	}
			]
		}
		   
		]
	}
    return items;
};

function getItemFindKeyWord_CSAPForm() 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
		{
			xtype: 'compositefield',
			fieldLabel: '-',
			anchor: '100%',
			items: 
			[
				 {
					columnWidth: 1,
					layout: 'form',
					border: false,
					labelAlign: 'left',
					labelWidth: 100,
					items:
					[
						{
							xtype: 'textfield',
							fieldLabel: 'Key Word ',
							id: 'txtKeyWordFind_CSAPForm',
							name: 'txtKeyWordFind_CSAPForm',
							width:190
						}
					]
		    	}
			 
			]
		}
		   
		]
	}
    return items;
};

function mComboFindField_CSAPForm()
{
	var cboFindField_CSAPForm = new Ext.form.ComboBox
	(
		{
			id:'cboFindField_CSAPForm',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih Field',
			width:190,
			fieldLabel: 'Field Name',	
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: [
							["CSA.CSAP_NUMBER", "Number"], 
							["CSA.CSAP_DATE", "Date"],
							["CSA.VEND_CODE", "Vend Code"], 
							["C.VENDOR", "Vendor"],
							["NOTES1", "Notes"]
						  ]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			listeners:  
			{
				'select': function(a,b,c)
				{
					selectcboField_CSAPForm=b.data.Id;

					if (b.data.Id=="CSA.CSAP_DATE"){
						Ext.getCmp('txtKeyWordFind_CSAPForm').disable();
						Ext.getCmp('dtpTglFind_CSAPForm').enable();
						criteriaTanggalFindCSAP = true;
					} else{
						Ext.getCmp('dtpTglFind_CSAPForm').disable();
						Ext.getCmp('txtKeyWordFind_CSAPForm').enable();
						criteriaTanggalFindCSAP = false;
					}
				}

			}
			
		}
	);
	return cboFindField_CSAPForm;
}


function mComboOperator_CSAPForm()
{
	var cboFindOperator_CSAPForm = new Ext.form.ComboBox
	(
		{
			id:'cboFindOperator_CSAPForm',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			width:190,
			emptyText:'Pilih Operator',
			fieldLabel: 'Operator ',
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields: 
					[
						'Id',
						'displayText'
					],
					data: 
					[
						[1, "Like"], 
						[2, "="],
						[3, "<"],
						[4, ">"],
						[5, "<="], 
						[6, ">="],
						[7, "<>"]
					]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			listeners:  
			{
				'select': function(a,b,c)
				{
					selectcboOperator_CSAPForm=b.data.Id;
				}

			}
		}
	);
	return cboFindOperator_CSAPForm;
}


function CekDataInput_CSAPForm(){
		Ext.Ajax.request
	(
		{
			url: "./Module.mvc/ExecProc",
			params: 
			{
				UserID: 'Admin',
				ModuleID: 'clsProcessCekPaymentAp',
				Params:	Ext.get('txtNo_CSAPForm').dom.value
			},
			success: function(o) 
			{												
				var cst = Ext.decode(o.responseText);
				if (cst.success === true )
				{	
					DataAddNew_CSAPForm = false;
					Ext.get('txtNo_CSAPForm').dom.readOnly=true;
					tempstrKdUser=cst.KD_USER;
					Ext.get('cboVendorEntry_CSAPForm').dom.value = cst.VENDOR;
					selectCboEntryVendor_CSAPForm = cst.VEND_CODE;
					
					Ext.get('comboBayar_CSAPForm').dom.value= cst.PAYMENT;
					selectBayar_CSAPForm = cst.PAY_CODE;
					Ext.get('comboAktivaLancar_CSAPForm').dom.value= cst.ACCOUNT + ' - '+cst.NAMAACCOUNT;
					selectAktivaLancar_CSAPForm = cst.ACCOUNT;

				    Ext.get('txtNo_CSAPForm').dom.value = cst.CSAP_NUMBER;
					
				    Ext.get('txtTerimaDari_CSAPForm').dom.value = cst.VENDOR;
					Ext.get('dtpTanggal_CSAPForm').dom.value = ShowDate(cst.CSAP_DATE);  
					Ext.getCmp('dtpTanggal_CSAPForm').disable();  
					
					Ext.get('txtTotal_CSAPForm').dom.value = formatCurrencyDec(cst.AMOUNT);
					Ext.get('txtCatatan_CSAPForm').dom.value = cst.NOTES1;
					Ext.get('txtNoPembayaran_CSAPForm').dom.value = cst.PAY_NO;
					Ext.getCmp('ChkApproveEntry_CSAPForm').setValue(cst.APPROVE);
					Ext.getCmp('cboPengembalian_CSAPForm').setValue('');//(rowdata.IS_PEMGEMBALIAN);	
					RefreshDataDetail_CSAPForm(cst.CSAP_NUMBER,cst.CSAP_DATE);
					// GetVendorAcc_CSAPForm(cst.VEND_CODE);
					CekApp_CSAPForm=cst.APPROVE;
					// getNoReferensi(cst.CSAP_NUMBER,cst.CSAP_DATE);

				}
				else
				{
					//ShowPesanWarning_PenerimaanMhs('Data tidak berhasil', 'Cash Out');
				}
			}

		}
	);
}

function LookUpAccount_APEntry(criteria)
{
	
	WindowLookUpAccount_APEntry = new Ext.Window
    (
        {
            id: 'pnlLookUpAccount_APEntry',
            title: 'Lookup Account',
            width:650,
            height: 350,
            border: false,
            resizable:false,
            plain: true,
            iconCls: 'icon_lapor',
            modal: true,
            items: [
				getGridListSearchAccount_APEntry()
			],
			listeners:
			{             
				activate: function()
				{
						
				},
				afterShow: function()
				{
					this.activate();

				},
				deactivate: function()
				{
					
				},
				close: function (){
					
				}
			}
        }
    );

    WindowLookUpAccount_APEntry.show();
	getListSearchAccount_APEntry(criteria);
};

function getGridListSearchAccount_APEntry(){
	var fldDetail = ['account','name','nama_parent'];
	dsGridListAccount_APEntry = new WebApp.DataStore({ fields: fldDetail });
	
    GridListAccountColumnModelAPEntry =  new Ext.grid.ColumnModel([
        new Ext.grid.RowNumberer(),
		{
			dataIndex		: 'account',
			header			: 'No. Account',
			width			: 80,
			menuDisabled	: true,
        },
		{
			dataIndex		: 'name',
			header			: 'Nama',
			width			: 140,
			menuDisabled	: true,
        },
		{
			dataIndex		: 'nama_parent',
			header			: 'Parent',
			width			: 120,
			menuDisabled	: true,
        },
	]);
	
	
	GridListAccount_APEntry= new Ext.grid.EditorGridPanel({
		id			: 'GrdListPencarianAccount_APEntry',
		stripeRows	: true,
		width		: 640,
		height		: 270,
        store		: dsGridListAccount_APEntry,
        border		: true,
        frame		: false,
        autoScroll	: true,
        cm			: GridListAccountColumnModelAPEntry,
		selModel: new Ext.grid.RowSelectionModel({
			singleSelect: true,
			listeners:
			{
				rowselect: function(sm, row, rec)
				{
					currentRowSelectionSearchAccountAPEntry = undefined;
					currentRowSelectionSearchAccountAPEntry = dsGridListAccount_APEntry.getAt(row);
				}
			}
		}),
		listeners	: {
			rowclick: function( $this, rowIndex, e )
			{
				
    		},
			celldblclick: function(gridView,htmlElement,columnIndex,dataRecord){
				var line	= gridDTLTR_CSAPForm.getSelectionModel().selection.cell[0];
				dsDTLTRList_CSAPForm.getRange()[line].data.cito="Tidak";
				dsDTLTRList_CSAPForm.getRange()[line].data.account=currentRowSelectionSearchAccountAPEntry.data.account;
				dsDTLTRList_CSAPForm.getRange()[line].data.name=currentRowSelectionSearchAccountAPEntry.data.name;
				dsDTLTRList_CSAPForm.getRange()[line].data.desc=Ext.get('txtCatatan_CSAPForm').getValue();
				dsDTLTRList_CSAPForm.getRange()[line].data.value=0,00;
				
				gridDTLTR_CSAPForm.getView().refresh();
				gridDTLTR_CSAPForm.startEditing(line, 4);	
				
				WindowLookUpAccount_APEntry.close();
			},
			'keydown' : function(e){
				if(e.getKey() == 13){
					var line	= gridDTLTR_CSAPForm.getSelectionModel().selection.cell[0];
					dsDTLTRList_CSAPForm.getRange()[line].data.cito="Tidak";
					dsDTLTRList_CSAPForm.getRange()[line].data.account=currentRowSelectionSearchAccountAPEntry.data.account;
					dsDTLTRList_CSAPForm.getRange()[line].data.name=currentRowSelectionSearchAccountAPEntry.data.name;
					dsDTLTRList_CSAPForm.getRange()[line].data.desc=Ext.get('txtCatatan_CSAPForm').getValue();
					dsDTLTRList_CSAPForm.getRange()[line].data.value=0,00;
					
					gridDTLTR_CSAPForm.getView().refresh();
					gridDTLTR_CSAPForm.startEditing(line, 4);	
					
					WindowLookUpAccount_APEntry.close();
				}
			},
		},
		viewConfig	: {forceFit: true}
    });
	return GridListAccount_APEntry;
}

function getListSearchAccount_APEntry(criteria){
	Ext.Ajax.request ({
		url: baseURL + "index.php/keuangan/functionGeneral/getListAccount",
		params: {
			criteria:criteria
		},
		failure: function(o)
		{
			Ext.Msg.show({
				title: 'Error',
				msg: 'Error menampilkan list account. Hubungi Admin!',
				buttons: Ext.MessageBox.OK,
				fn: function (btn) {
					if (btn == 'ok')
					{
						var line	= gridDTLTR_CSAPForm.getSelectionModel().selection.cell[0];
						gridDTLTR_CSAPForm.startEditing(line, 1);	
					}
				}
			});
		},	
		success: function(o) 
		{   
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				if(cst.totalrecords == 0 ){
					Ext.Msg.show({
						title: 'Information',
						msg: 'Tidak ada account yang sesuai atau kriteria account kurang!',
						buttons: Ext.MessageBox.OK,
						fn: function (btn) {
							if (btn == 'ok')
							{
								var line	= gridDTLTR_CSAPForm.getSelectionModel().selection.cell[0];
								gridDTLTR_CSAPForm.startEditing(line, 4);
								WindowLookUpAccount_APEntry.close();
							}
						}
					});
				} else{
					dsGridListAccount_APEntry.removeAll();
					var recs=[],
						recType=dsGridListAccount_APEntry.recordType;
					for(var i=0; i<cst.ListDataObj.length; i++){
						recs.push(new recType(cst.ListDataObj[i]));						
					}
					dsGridListAccount_APEntry.add(recs);
					GridListAccount_APEntry.getView().refresh();
					GridListAccount_APEntry.getSelectionModel().selectRow(0);
					GridListAccount_APEntry.getView().focusRow(0);
				}
				
			} else {
				Ext.Msg.show({
					title: 'Error',
					msg: 'Error menampilkan list account. Hubungi Admin!',
					buttons: Ext.MessageBox.OK,
					fn: function (btn) {
						if (btn == 'ok')
						{
							var line	= gridDTLTR_CSAPForm.getSelectionModel().selection.cell[0];
							gridDTLTR_CSAPForm.startEditing(line, 1);	
						}
					}
				});
			};
		}
	});
}



function viewHistoryApprovedAP(){
	winFormLookup_HistoryApprovedAP = new Ext.Window
	(
		{
		    id: 'winFormLookup_HistoryApprovedAP',
		    title: 'Form Approve',
		    closeAction: 'destroy',
		    closable: true,
		    width: 650,
		    height: 480,
		    border: false,
		    plain: true,
		    resizable: false,
		    layout: 'border',
		    iconCls: 'find',
		    modal: true,
			autoScroll: true,
		    items: [getItemFormLookup_HistoryApprovedAP()],
			fbar:
			[
				
				{ xtype: 'label', cls: 'left-label', width: 110, text: 'Jumlah Tagihan:' },
				{
					xtype:'textfield',
					name: 'txtTotal_HistoryApprovedAP',
					id: 'txtTotal_HistoryApprovedAP',
					width:100,
					readOnly:true,
					style: 'font-weight: bold;text-align: right'
					// hidden:true
				},
				
			],
		}
	);
	
	winFormLookup_HistoryApprovedAP.show();
}

function getItemFormLookup_HistoryApprovedAP()
{
	var pnlButtonLookup_HistoryApprovedAP = new Ext.Panel
	(
		{
			id: 'pnlButtonLookup_HistoryApprovedAP',
			layout: 'hbox',
			border: false,
			region: 'south',
			defaults: { margins: '0 5 0 0' },
			style: { 'margin-left': '4px', 'margin-top': '3px' },
			anchor: '96.5%',
			layoutConfig:
			{
				padding: '3',
				pack: 'end',
				align: 'middle'
			},
			      
			listeners: 
			{
				activate: function()
				{		
					CalcTotal_HistoryApprovedAP();
					CheckboxLineSetTrue(false);
				}												
			}	
		}
	);
	
	var frmListLookup_HistoryApprovedAP = new Ext.Panel
	(
		{
			id: 'frmListLookup_HistoryApprovedAP',
			layout: 'form',
			region: 'center',
			autoScroll: true,
			items: 
			[
				getGridListLookup_HistoryApprovedAP(),
				pnlButtonLookup_HistoryApprovedAP,
			],
		}
	);
	
	return frmListLookup_HistoryApprovedAP;
}
function getListHistoryApproved_HistoryApprovedAP(){	
	Ext.Ajax.request({
		url: baseURL + "index.php/keuangan/functionPembayaranHutang/getFakturHutangApproved",
		params: {
			csap_number : Ext.getCmp('txtNo_CSAPForm').getValue(),
			csap_date : Ext.getCmp('dtpTanggal_CSAPForm').getValue()
		},
		success: function(o) {
			var cst = Ext.decode(o.responseText);
			if (cst.success === true) 
			{
				dsGLLookup_HistoryApprovedAP.removeAll();
				var recs=[],
				recType=dsGLLookup_HistoryApprovedAP.recordType;
				for(var i=0; i<cst.total_record; i++){
					recs.push(new recType(cst.listData[i]));						
				}
				dsGLLookup_HistoryApprovedAP.add(recs);
				gridListLookup_HistoryApprovedAP.getView().refresh();
				CalcTotal_HistoryApprovedAP();
			}
			else {
				ShowPesanError_CSARForm('Gagal membaca list detail approved!' , 'Error');
			}
		}
	})
}

function getGridListLookup_HistoryApprovedAP()
{
	getListHistoryApproved_HistoryApprovedAP();
	var fields_HistoryApprovedAP = 
	[
		'nomor','tanggal','cust_code','amount','paid','remain','notes','due_date','posted','jenis_piu','type'
	];
	dsGLLookup_HistoryApprovedAP = new WebApp.DataStore({ fields: fields_HistoryApprovedAP });
	
	gridListLookup_HistoryApprovedAP = new Ext.grid.EditorGridPanel
	(
		{
			stripeRows: true,
			id: 'gridListLookup_HistoryApprovedAP',
			store: dsGLLookup_HistoryApprovedAP,
			height:353,
			anchor: '100% 98%',
			columnLines:true,
			bodyStyle: 'padding:0px',
			border: true,
			frame: true,
			plugins: [new Ext.ux.grid.FilterRow()],
			viewConfig : 
			{
				forceFit: true
			},				
			sm: new Ext.grid.RowSelectionModel
			(
				{ 
					singleSelect: false,
					listeners:
					{ 
						rowselect: function(sm, row, rec) 
						{
							// selectedrowGLLookup_HistoryApprovedAP = dsGLLookup_HistoryApprovedAP.getAt(row);
						}						
					}
				}
			),
			cm: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{ 
						id: 'colNoGLLookup_HistoryApprovedAP',
						header: 'No. Piutang',
						dataIndex: 'nomor',
						width:130,	
						renderer: function(value, cell) 
						{
							var str = "<div style='white-space:normal;padding:2px 2px 2px 2px'>" + value + "</div>";
							return str;
						},
						filter: {}						
					},
					{ 
						id: 'colTGLoGLLookup_HistoryApprovedAP',
						header: 'Tanggal',
						dataIndex: 'tanggal',
						width:100,	
						renderer: function(v, params, record) 
						{
							return ShowDate(record.data.tanggal);
						},
						filter: {}					
					},
					{ 
						id: 'colJMLGLLookupHistoryApprovedAP',
						header: 'Amount',
						dataIndex: 'amount',
						width:100,	
						align:'right',						
						renderer: function(v, params, record) 
						{
							var str = "<div style='white-space:normal;padding:2px 20px 2px 2px'>" + formatCurrencyDec(record.data.amount) + "</div>";
							return str;
							 
						}	
					}
					,{ 
						id: 'colPaidGLLookupHistoryApprovedAP',
						name: 'colPaidGLLookupHistoryApprovedAP',
						header: 'Paid',
						dataIndex: 'paid',
						width:100,
						xtype:'numbercolumn',
						align:'right'
					},
					{ 
						id: 'colRemainGLLookupHistoryApprovedAP',
						header: 'Remain',
						dataIndex: 'remain',
						width:100,	
						align:'right',						
						renderer: function(v, params, record) 
						{
							var str = "<div style='white-space:normal;padding:2px 20px 2px 2px'>" + formatCurrencyDec(record.data.remain) + "</div>";
							return str;
							 
						}	
					}
				]
			)
		}
	);
	
	return gridListLookup_HistoryApprovedAP;
}

function CalcTotal_HistoryApprovedAP()
{
    var total=0;
	var nilai=0;
	for(var i=0;i < dsGLLookup_HistoryApprovedAP.getCount();i++)
	{
		var o = dsGLLookup_HistoryApprovedAP.data.items[i].data;
		total += parseInt(o.paid);	
	}
	Ext.get('txtTotal_HistoryApprovedAP').dom.value=formatCurrencyDec(total);
};