// Data Source ExtJS # --------------

/**
*	Nama File 		: TRTRReceiveOther.js
*	Menu 			: 
*	Model id 		: 
*	Keterangan 		: Kasir Rawat Jalan adalah proses untuk pembayaran pasien pada rawat jalan
*	Di buat tanggal : 13 Agustus 2014
*	Di EDIT tanggal : 14 Agustus 2014
*	Oleh 			: ADE. S
*	Editing			: SDY_RI
*/

var dataSource_viTRReceiveOther;
var selectCount_viTRReceiveOther=50;
var NamaForm_viTRReceiveOther="Receive Other Entry ";
var mod_name_viTRReceiveOther="viTRReceiveOther";
var now_viTRReceiveOther= new Date();
var addNew_viTRReceiveOther;
var rowSelected_viTRReceiveOther;
var setLookUps_viTRReceiveOther;
var mNoKunjungan_viTRReceiveOther='';

var CurrentData_viTRReceiveOther =
{
	data: Object,
	details: Array,
	row: 0
};

CurrentPage.page = dataGrid_viTRReceiveOther(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

// End Deklarasi Variabel pada Kasir Rawat Jalan # --------------

// Start Project Kasir Rawat Jalan # --------------

// --------------------------------------- # Start Function # ---------------------------------------
// ## Silahkan tempatkan semua fungsi eksekusi disini

// --------------------------------------- # End Function # ---------------------------------------

// --------------------------------------- # Start Form # ---------------------------------------

/**
*	Function : dataGrid_viTRReceiveOther
*	
*	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
*/

function dataGrid_viTRReceiveOther(mod_id_viTRReceiveOther)
{	
    // Field kiriman dari Project Net.
    var FieldMaster_viTRReceiveOther = 
	[
		 'NO_KUNJUNGAN', 'KD_KELOMPOK', 'KD_UNIT', 'KD_DOKTER', 'KD_CUSTOMER', 'KD_PASIEN', 
		 'TGL_KUNJUNGAN','JAM_KUNJUNGAN', 'TINGGI_BADAN', 'BERAT_BADAN', 'TEKANAN_DRH', 
		 'NADI','ALERGI', 'KELUHAN', 'RUJUK_RAD', 'RUJUK_LAB', 'TAHAP_PROSES', 'PASIEN_BARU',
		 'NAMA_UNIT','KELOMPOK', 'DOKTER', 'CUSTOMER', 'PS_BARU','KD_PENDIDIKAN','KD_STS_MARITAL', 
		 'KD_AGAMA','KD_PEKERJAAN','NAMA','TEMPAT_LAHIR','TGL_LAHIR','JENIS_KELAMIN','ALAMAT', 
		 'NO_TELP','NO_HP','GOL_DARAH','PENDIDIKAN','STS_MARITAL','AGAMA','PEKERJAAN','JNS_KELAMIN',
		 'TAHUN','BULAN','HARI'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viTRReceiveOther = new WebApp.DataStore
	({
        fields: FieldMaster_viTRReceiveOther
    });
    
    // Grid Kasir Rawat Jalan # --------------
	var GridDataView_viTRReceiveOther = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viTRReceiveOther,
			autoScroll: true,
			columnLines: true,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viTRReceiveOther = undefined;
							rowSelected_viTRReceiveOther = dataSource_viTRReceiveOther.getAt(row);
							CurrentData_viTRReceiveOther
							CurrentData_viTRReceiveOther.row = row;
							CurrentData_viTRReceiveOther.data = rowSelected_viTRReceiveOther.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viTRReceiveOther = dataSource_viTRReceiveOther.getAt(ridx);
					if (rowSelected_viTRReceiveOther != undefined)
					{
						setLookUp_viTRReceiveOther(rowSelected_viTRReceiveOther.data);
					}
					else
					{
						setLookUp_viTRReceiveOther();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Kasir Rawat Jalan
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colVoNumber_viTRReceiveOther',
						header: 'VO Number',
						dataIndex: '',
						sortable: true,
						width: 35,
						filter:
						{
							type: 'int'
						}
					},
					//-------------- ## --------------
					{
						id: 'colTglJournal_viTRReceiveOther',
						header: 'Tanggal',
						dataIndex: '',
						sortable: true,
						width: 35,
						filter:
						{}
					},
					//-------------- ## --------------
					{
						id: 'colCustFilter_viTRReceiveOther',
						header:'Notes',
						dataIndex: '',
						width: 200,
						sortable: true,
						filter: {}
					},
					
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viTRReceiveOther',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viTRReceiveOther',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viTRReceiveOther != undefined)
							{
								setLookUp_viTRReceiveOther(rowSelected_viTRReceiveOther.data)
							}
							else
							{								
								setLookUp_viTRReceiveOther();
							}
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			bbar : bbar_paging(mod_name_viTRReceiveOther, selectCount_viTRReceiveOther, dataSource_viTRReceiveOther),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viTRReceiveOther = new Ext.Panel
    (
		{
			title: NamaForm_viTRReceiveOther,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viTRReceiveOther,
			region: 'center',
			layout: 'form', 
			closable: true,        
			border: false,  
			margins: '0 5 5 0',
			items: [GridDataView_viTRReceiveOther],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viTRReceiveOther,
		            columns: 12,
		            defaults: {
		                scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
			            { 
							xtype: 'tbtext', 
							text: 'VO Number : ', 
							style:{'text-align':'left'},
							width: 90,
							height: 25
						},
						//viCombo_JournalCode(125, "ComboFilterJournalCode_viTRReceiveOther"),	
						//-------------- ## --------------
						{
							xtype: 'textfield',
							id: 'TxtNumberFilter_viGeneralLedger',
							emptyText: 'Number...',
							width: 150,
						},
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Tanggal : ', 
							style:{'text-align':'left'},
							width: 50,
							height: 25
						},											
						//-------------- ## --------------						
						{
							xtype: 'datefield',
							id: 'txtfilterTglKunjAwal_viTRReceiveOther',
							value: now_viTRReceiveOther,
							format: 'd/M/Y',
							width: 120,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viTRReceiveOther();								
									} 						
								}
							}
						},						
						{ 
							xtype: 'tbtext', 
							text: 's/d ', 
							style:{'text-align':'center'},
							width: 50,
							height: 25
						},						
						{
							xtype: 'datefield',
							id: 'txtfilterTglKunjAkhir_viTRReceiveOther',
							value: now_viTRReceiveOther,
							format: 'd/M/Y',
							width: 120,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viTRReceiveOther();								
									} 						
								}
							}
						},
						//-------------- ## --------------
						// { 
							// xtype: 'tbspacer',
							// width: 15,
							// height: 25
						// },
						//-------------- ## --------------						
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},						
						{
							xtype: 'button',
							text: 'Cari',
							iconCls: 'refresh',
							tooltip: 'Cari',
							style:{paddingLeft:'30px'},
							//rowspan: 3,
							width:150,
							id: 'BtnFilterGridDataView_viTRReceiveOther',
							handler: function() 
							{					
								DfltFilterBtn_viTRReceiveOther = 1;
								DataRefresh_viTRReceiveOther(getCriteriaFilterGridDataView_viTRReceiveOther());
							}                        
						},
						//-------------- ## --------------
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viTRReceiveOther;
    //-------------- # End form filter # --------------
}
// End Function dataGrid_viTRReceiveOther # --------------

/**
*	Function : setLookUp_viTRReceiveOther
*	
*	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
*/

function setLookUp_viTRReceiveOther(rowdata)
{
    var lebar = 760;
    setLookUps_viTRReceiveOther = new Ext.Window
    (
    {
        id: 'SetLookUps_viTRReceiveOther',
		name: 'SetLookUps_viTRReceiveOther',
        title: NamaForm_viTRReceiveOther, 
        closeAction: 'destroy',        
        width: 775,
        height: 610,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viTRReceiveOther(lebar,rowdata), //1
        listeners:
        {
            activate: function()
            {
				
            },
            afterShow: function()
            {
                this.activate();
            },
            deactivate: function()
            {
                rowSelected_viTRReceiveOther=undefined;
                //datarefresh_viTRReceiveOther();
				mNoKunjungan_viTRReceiveOther = '';
            }
        }
    }
    );

    setLookUps_viTRReceiveOther.show();
    if (rowdata == undefined)
    {
        // dataaddnew_viTRReceiveOther();
		// Ext.getCmp('btnDelete_viTRReceiveOther').disable();	
    }
    else
    {
        // datainit_viTRReceiveOther(rowdata);
    }
}
// End Function setLookUpGridDataView_viTRReceiveOther # --------------

/**
*	Function : getFormItemEntry_viTRReceiveOther
*	
*	Sebuah fungsi untuk menampilkan penataan menu dan isian form edit
*/

function getFormItemEntry_viTRReceiveOther(lebar,rowdata)
{
    var pnlFormDataBasic_viTRReceiveOther = new Ext.FormPanel
    (
		{
			title: '',
			region: 'north',
			layout: 'form',
			bodyStyle: 'padding:10px 10px 10px 10px',
			anchor: '100%',
			width: lebar,
			border: false,
			//-------------- #items# --------------
			items:
			[
				getItemPanelInputBiodata_viTRReceiveOther(lebar),
				//-------------- ## -------------- 
				//getItemDataKunjungan_viTRReceiveOther(lebar), 
				//-------------- ## --------------
				getItemGridTransaksi_viTRReceiveOther(lebar),
				//-------------- ## --------------
				{
					xtype: 'compositefield',
					fieldLabel: ' ',
					anchor: '100%',
					labelSeparator: '',
					//width: 199,
					style:{'margin-top':'7px'},
					items: 
					[
						{
							xtype:'button',
							text:'Posting',
							width:70,
							style:{'text-align':'right','margin-left':'10px'},
							hideLabel:true,
							id: 'btnPostingRecord_viTRReceiveOther',
							name: 'btnPostingRecord_viTRReceiveOther',
							handler:function()
							{
							}   
						},
		                {
							xtype:'button',
							text:'Import',
							width:70,
							style:{'text-align':'right','margin-left':'10px'},
							hideLabel:true,
							id: 'btnImportRecord_viTRReceiveOther',
							name: 'btnImportRecord_viTRReceiveOther',
							handler:function()
							{
							}   
						},
		                {
                            xtype: 'textfield',
                            id: 'txtJumlah2EditData_viTRReceiveOther',
                            name: 'txtJumlah2EditData_viTRReceiveOther',
							style:{'text-align':'right','margin-left':'380px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
		                //-------------- ## --------------
					]
				},
				//-------------- ## --------------
			],
			//-------------- #End items# --------------
			fileUpload: true,
			// Tombol pada tollbar Edit Data Pasien
			tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						id: 'btnAdd_viTRReceiveOther',
						handler: function(){
							dataaddnew_viTRReceiveOther();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viTRReceiveOther',
						handler: function()
						{
							datasave_viTRReceiveOther(addNew_viTRReceiveOther);
							datarefresh_viTRReceiveOther();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						id: 'btnSimpanExit_viTRReceiveOther',
						handler: function()
						{
							var x = datasave_viTRReceiveOther(addNew_viTRReceiveOther);
							datarefresh_viTRReceiveOther();
							if (x===undefined)
							{
								setLookUps_viTRReceiveOther.close();
							}
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viTRReceiveOther',
						handler: function()
						{
							datadelete_viTRReceiveOther();
							datarefresh_viTRReceiveOther();
							
						}
					},
					//-------------- ## --------------
					{
						xtype:'tbseparator'
					},
					//-------------- ## --------------
					{
		                text: 'Lookup ',
		                iconCls:'find',
		                menu: 
		                [		                	
					    	{
						        text: 'Find Record',
						        id:'btnLookUpRecord_viTRReceiveOther',
						        iconCls: 'find',
						        handler: function(){
						            FormSetLookupRecord_viTRReceiveOther('1','2');
						        },
					    	},
					    	//-------------- ## --------------
		                ],
		            },
					//-------------- ## --------------
					
					
				]
			}//,items:
		}
    )

    return pnlFormDataBasic_viTRReceiveOther;
}
// End Function getFormItemEntry_viTRReceiveOther # --------------

/**
*	Function : getItemPanelInputBiodata_viTRReceiveOther
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemPanelInputBiodata_viTRReceiveOther(lebar) 
{
	var rdType_viTRReceiveOther = new Ext.form.RadioGroup
	({  
        fieldLabel: '',  
        columns: 2, 
        width: 200,//400,
		border:false,
        items: 
		[  
			{
				boxLabel: 'Debit', 
				width:50,
				id: 'rdDebit_viTRReceiveOther', 
				name: 'rdDebit_viTRReceiveOther', 
				inputValue: '1'
			},  
			//-------------- ## --------------
			{
				boxLabel: 'Kredit', 
				width:50,
				id: 'rdKredit_viTRReceiveOther', 
				name: 'rdKredit_viTRReceiveOther', 
				inputValue: '2'
			} 
			//-------------- ## --------------
        ]  
    });
	//-------------- ## --------------
	
    var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		border:true,
		items:
		[			
			{
				xtype: 'compositefield',
				fieldLabel: 'VO Number',
				anchor: '100%',
				//labelSeparator: '',
				width: 199,
				items: 
				[					
					{
						xtype: 'textfield',
						flex: 1,
						fieldLabel: 'Label',														
						width : 100,	
						name: 'txtVONumber_viTRReceiveOther',
						id: 'txtVONumber_viTRReceiveOther',
						emptyText: 'VO Number',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},
					{
						xtype: 'displayfield',				
						width: 370,								
						value: ' ',
						fieldLabel: 'Label',
					},
					{
						xtype: 'datefield',					
						fieldLabel: 'Tanggal',
						name: 'DueDate_viTRReceiveOther',
						id: 'DueDate_viTRReceiveOther',
						format: 'd/M/Y',
						width: 130,
						value: now_viTRReceiveOther,
						//style:{'text-align':'left','margin-left':'210px'},
					},
					// {
						// xtype: 'checkbox',
						// id: 'chkUpdateApproveGL',
						// name: 'chkUpdateApproveGL',
						// disabled: false,
						// autoWidth: false,		
						// style: { 'margin-top': '2px' },							
						// boxLabel: 'Approved ',
						// width: 70
					// },
					//-------------- ## --------------
				]
			},
			{
				xtype: 'compositefield',
				fieldLabel: 'Receive From',
				name: 'compCustomer_viTRReceiveOther',
				id: 'compCustomer_viTRReceiveOther',
				items: 
				[
					viCombo_CustomerRO(610, "ComboCustomer_viTRReceiveOther"),								
				]
			},
			{
				xtype: 'compositefield',
				fieldLabel: 'Cash/Bank',
				name: 'compCashBank_viTRReceiveOther',
				id: 'compCashBank_viTRReceiveOther',
				items: 
				[
					{
						xtype: 'textfield',					
						fieldLabel: '',								
						width: 110,
						name: 'txtCash_viTRReceiveOther',
						id: 'txtCash_viTRReceiveOther',
						emptyText: ' ',
					},
					{
						xtype: 'textfield',					
						fieldLabel: '',								
						width: 495,
						name: 'txtBank_viTRReceiveOther',
						id: 'txtBank_viTRReceiveOther',
						emptyText: ' ',
					},					
					
					//-------------- ## --------------
				]
			},
			//-------------- ## --------------				
			{
				xtype: 'compositefield',
				fieldLabel: 'Pay. Mode',
				name: 'compPayMode_viTRReceiveOther',
				id: 'compPayMode_viTRReceiveOther',
				items: 
				[					
					viCombo_PayMode(110,'cboPayMode_ReceiveOther'),
					//-------------- ## --------------
					{
						xtype: 'displayfield',				
						width: 80,								
						value: 'Pay. Number:',
						fieldLabel: 'Label',
					},
					{
						xtype: 'textfield',					
						fieldLabel: '',								
						width: 200,
						name: 'txtPayNumber_viTRReceiveOther',
						id: 'txtPayNumber_viTRReceiveOther',
						emptyText: 'Pay Number',
					},					
					
					//-------------- ## --------------
				]
			},
			//-------------- ## --------------	
			{
				xtype: 'compositefield',
				fieldLabel: 'Currency',
				name: 'compcurr_viTRReceiveOther',
				id: 'compcurr_viTRReceiveOther',
				items: 
				[					
					viCombo_Curr(50,'cboCurr_ReceiveOther'),
					//-------------- ## --------------
					{
						xtype: 'textfield',					
						fieldLabel: '',								
						width: 55,
						name: 'txtKurs_viTRReceiveOther',
						id: 'txtKurs_viTRReceiveOther',
						emptyText: '1',
					},
					{
						xtype: 'displayfield',				
						width: 80,								
						value: 'Amount(Kurs):',
						fieldLabel: 'Label',
					},					
					{
						xtype: 'textfield',					
						fieldLabel: '',								
						width: 200,
						name: 'txtPayNumber_viTRReceiveOther',
						id: 'txtPayNumber_viTRReceiveOther',
						emptyText: 'Kurs..',
					},					
					
					//-------------- ## --------------
				]
			},
			{
				xtype: 'compositefield',
				fieldLabel: 'Notes ',
				name: 'compNotes_viTRReceiveOther',
				id: 'compNotes_viTRReceiveOther',
				items: 
				[
					{
						xtype: 'textfield',					
						fieldLabel: '',								
						width: 610,
						name: 'txtNotes_viTRReceiveOther',
						id: 'txtNotes_viTRReceiveOther',
						emptyText: ' ',
					},					
				]
			},
			{
				xtype: 'compositefield',
				fieldLabel: ' ',
				labelSeparator: '',
				name: 'compNotes2_viTRReceiveOther',
				id: 'compNotes2_viTRReceiveOther',
				items: 
				[
					{
						xtype: 'textfield',					
						fieldLabel: '',								
						width: 610,
						name: 'txtNotes2_viTRReceiveOther',
						id: 'txtNotes2_viTRReceiveOther',
						emptyText: ' ',
					},					
				]
			},
			{
				xtype: 'compositefield',
				fieldLabel: ' ',
				labelSeparator: '',
				name: 'compNotes3_viTRReceiveOther',
				id: 'compNotes3_viTRReceiveOther',
				items: 
				[
					{
						xtype: 'textfield',					
						fieldLabel: '',								
						width: 610,
						name: 'txtNotes3_viTRReceiveOther',
						id: 'txtNotes3_viTRReceiveOther',
						emptyText: ' ',
					},					
				]
			},
			{
				xtype: 'compositefield',
				fieldLabel: 'Budget',
				labelSeparator: '',
				name: 'compBudget_viTRReceiveOther',
				id: 'compBudget_viTRReceiveOther',
				items: 
				[
					{
						xtype: 'checkbox',					
						allowBlank: false,
						//width: 610,
						// name: 'txtNotes3_viTRReceiveOther',
						// id: 'txtNotes3_viTRReceiveOther',
						// emptyText: ' ',
					},					
				]
			},
			{
				xtype: 'compositefield',
				fieldLabel: 'Periode',
				name: 'compPeriode_viTRReceiveOther',
				id: 'compPeriode_viTRReceiveOther',
				items: 
				[
					viCombo_PeriodeRL(110, "ComboPeriodeRL_viGeneralLedger"),					
					{
						xtype:'button',
						text:'Load',
						width:70,
						//style:{'margin-left':'190px','margin-top':'7px'},
						hideLabel:true,
						id: 'btnLoadPeriode_viTRReceiveOther',
						handler:function()
						{
						}   
					}
					
				]
			},
			//-------------- ## --------------	
			
			
		]
	};
    return items;
};
// End Function getItemPanelInputBiodata_viTRReceiveOther # --------------

/**
*	Function : getItemDataKunjungan_viTRReceiveOther
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemDataKunjungan_viTRReceiveOther(lebar) 
{
	var items =
	{
		layout: 'form',
		anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',		
		width: lebar-80,
		height:30,
	    items:
		[				
			// {
				// xtype: 'compositefield',
				// fieldLabel: 'Dari',	
				// labelAlign: 'Left',
				// //labelSeparator: '',
				// labelWidth:70,
				// defaults: 
				// {
					// flex: 1
				// },
				// items: 
				// [					
					// {
						// xtype:'button',
						// text:'01',
						// tooltip: '01',
						// id:'btnTrans_vi01',
						// name:'btnTrans_vi01',
						// width: 20,
						// handler:function()
						// {
							
						// }
					// },
					// {
						// xtype:'button',
						// text:'02',
						// tooltip: '02',
						// id:'btnTrans_vi02',
						// name:'btnTrans_vi02',
						// width: 20,
						// handler:function()
						// {
							
						// }
					// },
					// // {
						// // xtype:'button',
						// // text:'03',
						// // tooltip: '03',
						// // id:'btnTrans_vi03',
						// // name:'btnTrans_vi03',
						// // width: 20,
						// // handler:function()
						// // {
							
						// // }
					// // },
					// // {
						// // xtype:'button',
						// // text:'04',
						// // tooltip: '04',
						// // id:'btnTrans_vi04',
						// // name:'btnTrans_vi04',
						// // width: 20,
						// // handler:function()
						// // {
							
						// // }
					// // },
					// // {
						// // xtype:'button',
						// // text:'05',
						// // tooltip: '05',
						// // id:'btnTrans_vi05',
						// // name:'btnTrans_vi05',
						// // width: 20,
						// // handler:function()
						// // {
							
						// // }
					// // },
					// // {
						// // xtype:'button',
						// // text:'06',
						// // tooltip: '06',
						// // id:'btnTrans_vi06',
						// // name:'btnTrans_vi06',
						// // width: 20,
						// // handler:function()
						// // {
							
						// // }
					// // },
					// // {
						// // xtype:'button',
						// // text:'07',
						// // tooltip: '07',
						// // id:'btnTrans_vi07',
						// // name:'btnTrans_vi07',
						// // width: 20,
						// // handler:function()
						// // {
							
						// // }
					// // },
					// // {
						// // xtype:'button',
						// // text:'08',
						// // tooltip: '08',
						// // id:'btnTrans_vi08',
						// // name:'btnTrans_vi08',
						// // width: 20,
						// // handler:function()
						// // {
							
						// // }
					// // },
					// // {
						// // xtype:'button',
						// // text:'09',
						// // tooltip: '09',
						// // id:'btnTrans_vi09',
						// // name:'btnTrans_vi09',
						// // width: 20,
						// // handler:function()
						// // {
							
						// // }
					// // },
					// // {
						// // xtype:'button',
						// // text:'10',
						// // tooltip: '10',
						// // id:'btnTrans_vi10',
						// // name:'btnTrans_vi10',
						// // width: 20,
						// // handler:function()
						// // {
							
						// // }
					// // },
					// // {
						// // xtype:'button',
						// // text:'11',
						// // tooltip: '11',
						// // id:'btnTrans_vi11',
						// // name:'btnTrans_vi11',
						// // width: 20,
						// // handler:function()
						// // {
							
						// // }
					// // },
					// // {
						// // xtype:'button',
						// // text:'12',
						// // tooltip: '12',
						// // id:'btnTrans_vi12',
						// // name:'btnTrans_vi12',
						// // width: 20,
						// // handler:function()
						// // {
							
						// // }
					// // },
				// ]
				// },
			//-------------- ## --------------		
			
		]
	};
    return items;
};
// End Function getItemDataKunjungan_viTRReceiveOther # --------------

/**
*	Function : getItemGridTransaksi_viTRReceiveOther
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemGridTransaksi_viTRReceiveOther(lebar) 
{
    var items =
	{
		title: '', 
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		border:true,
		width: lebar-80,
		height: 220, 
	    items:
		[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:
				[
					gridDataViewEdit_viTRReceiveOther()
				]	
			}
			//-------------- ## --------------
		]
	};
    return items;
};
// End Function getItemGridTransaksi_viTRReceiveOther # --------------

/**
*	Function : gridDataViewEdit_viTRReceiveOther
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function gridDataViewEdit_viTRReceiveOther()
{
    
    var FieldGrdKasir_viTRReceiveOther = 
	[
	];
	
    dsDataGrdJab_viTRReceiveOther= new WebApp.DataStore
	({
        fields: FieldGrdKasir_viTRReceiveOther
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJab_viTRReceiveOther,
        height: 200,
		selModel: new Ext.grid.RowSelectionModel
        (
	        {
	            singleSelect: true,
	            listeners:
	            {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
	        }
        ),
        columns: 
		[
			{
				dataIndex: '',
				header: 'Account',
				sortable: true,
				width: 75
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Name',
				sortable: true,
				width: 200
			},
			//-------------- ## --------------			
			{			
				dataIndex: '',
				header: 'Description',
				sortable: true,
				width: 200
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Amount (Rp)',
				sortable: true,
				width: 100,
				renderer: function(v, params, record) 
				{
					
				}			
			},
			
        ]
    }    
    return items;
}
// End Function gridDataViewEdit_viTRReceiveOther # --------------

// ## MENU LOOKUP ## ------------------------------------------------------------------

// ## MENU LOOKUP - LOOKUP PRODUK/ TINDAKAN ## ------------------------------------------------------------------

/**
*   Function : FormSetLookupRecord_viTRReceiveOther
*   
*   Sebuah fungsi untuk menampilkan windows popup Ganti Dokter
*/

function FormSetLookupRecord_viTRReceiveOther(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryGantiDokter_viTRReceiveOther = new Ext.Window
    (
        {
            id: 'FormGrdLookupRecord_viTRReceiveOther',
            title: 'Find Record',
            closeAction: 'destroy',
            closable:true,
            width: 390,
            height: 125,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
            //padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryLookupfindrecord_viTRReceiveOther(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryGantiDokter_viTRReceiveOther.show();
    mWindowGridLookupGantiDokter  = vWinFormEntryGantiDokter_viTRReceiveOther; 
};

// End Function FormSetLookupRecord_viTRReceiveOther # --------------

/**
*   Function : getFormItemEntryLookupfindrecord_viTRReceiveOther
*   
*   Sebuah fungsi untuk menampilkan isian form Ganti Dokter
*/
function getFormItemEntryLookupfindrecord_viTRReceiveOther(subtotal,NO_KUNJUNGAN)
{
    var lebar = 500;
    var pnlFormDataFindRecordWindowPopup_viTRReceiveOther = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                getItemPanelInputFindRecordDataView_viTRReceiveOther(lebar),
                //-------------- ## --------------
            ],
            //-------------- #End items# --------------
        }
    )
    return pnlFormDataFindRecordWindowPopup_viTRReceiveOther;
}
// End Function getFormItemEntryLookupfindrecord_viTRReceiveOther # --------------

/**
*   Function : getItemPanelInputFindRecordDataView_viTRReceiveOther
*   
*   Sebuah fungsi untuk menampilkan isian form Ganti Dokter
*/

function getItemPanelInputFindRecordDataView_viTRReceiveOther(lebar) 
{
    var items =
    {
        title: '',
        layout: 'Form',
        anchor: '100%',
        width: lebar,
        labelAlign: 'Left',
        bodyStyle: 'padding:1px 1px 1px 1px',
        border:false,
        items:
        [          
            
            {
                xtype: 'compositefield',
                fieldLabel: 'Field Name',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viCombo_ReferenceRL(250,'CmboFindReference_viTRReceiveOther'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
            {
                xtype: 'compositefield',
                fieldLabel: 'Operator',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viCombo_OperatorRL(250,'CmboOperator_viTRReceiveOther'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
			{
                xtype: 'compositefield',
                fieldLabel: 'Key Word',
                anchor: '100%',
                width: 250,
                items: 
                [
                    {
                        xtype: 'textfield',
                        id: 'TxtKeyWord_viTRReceiveOther',
                        emptyText: ' ',
                        width: 150,
                    },
                    
                ]
            },
            //-------------- ## --------------
			// {
				// xtype: 'compositefield',
				// id: 'findcancelbtn',
				// fieldLabel: 'find',
				// anchor: '100%',
				// //labelSeparator: '',
				// width: 199,
				// style:{'margin-top':'7px'},					
				// items: 
				// [
					// {
						// xtype:'button',
						// text:'Ok',
						// width:70,
						// //style:{'margin-left':'190px','margin-top':'7px'},
						// style:{'text-align':'right','margin-left':'60px'},
						// hideLabel:true,
						// id: 'btnFindRecord_viTRReceiveOther',
						// name: 'btnFindRecord_viTRReceiveOther',
						// handler:function()
						// {
						// }   
					// },
					// //-------------- ## --------------
					// {
						// xtype:'button',
						// text:'Cancel',
						// width:70,
						// //style:{'margin-left':'190px','margin-top':'7px'},
						// style:{'text-align':'right','margin-left':'90px'},
						// hideLabel:true,
						// id: 'btnCancelRecord_viTRReceiveOther',
						// name: 'btnCancelRecord_viTRReceiveOther',
						// handler:function()
						// {
						// }   
					// },                        
					// //-------------- ## --------------
                // ]
            // },  
        ]
    };
    return items;
}
// End Function getItemPanelInputFindRecordDataView_viTRReceiveOther # --------------


function viCombo_CustomerRO(lebar,Nama_ID)
{
    var Field_CustomerRO = ['KD_CUSTOMER', 'CUSTOMER'];
    ds_CustomerRO = new WebApp.DataStore({fields: Field_CustomerRO});
    
	// viRefresh_Journal();
	
    var cbo_CustomerRO = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'Customer',
			valueField: 'KD_Customer',
            displayField: 'Customer',
			emptyText:'Receive From...',
			store: ds_CustomerRO,
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',                        
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
			listeners:
			{ 
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_CustomerRO;
}

function viCombo_OperatorRL(lebar,Nama_ID)
{
    var Field_OperatorRL = ['OPERATOR'];
    ds_OperatorRL = new WebApp.DataStore({fields: Field_OperatorRL});
    
	// viRefresh_Journal();
	
    var cbo_OperatorRL = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'Operator',
			valueField: 'Operator',
            displayField: 'Operator',
			emptyText:'Operator',
			store: ds_OperatorRL,
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',                        
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
			listeners:
			{ 
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_OperatorRL;
}

function viCombo_PeriodeRL(lebar,Nama_ID)
{
    var Field_JournalRL = ['MONTH'];
    ds_JournalRL = new WebApp.DataStore({fields: Field_JournalRL});
    
	// viRefresh_Journal();
	
    var cbo_PeriodeBulanRL = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'Journal',
			valueField: 'MONTH',
            displayField: 'Month',
			emptyText:'Periode Bulan...',
			store: ds_JournalRL,
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',                        
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
			listeners:
			{ 
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_PeriodeBulanRL;
}

function viCombo_ReferenceRL(lebar,Nama_ID)
{
    var Field_ReferenceRL = ['REFERENCE'];
    ds_ReferenceRL = new WebApp.DataStore({fields: Field_ReferenceRL});   
	
    var cbo_ReferenceRL = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: 'Reference',
			valueField: 'REFERENCE',
            displayField: 'REFERENCE',
			emptyText:'Reference...',
			store: ds_ReferenceRL,
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',                        
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
			listeners:
			{ 
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_ReferenceRL;
}

function viCombo_PayMode(lebar,Nama_ID)
{
    var Field_PayMode = [' '];
    ds_PayMode = new WebApp.DataStore({fields: Field_PayMode});   
	
    var cbo_PayMode = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: '',
			valueField: '',
            displayField: '',
			emptyText:'Pay mode...',
			store: ds_PayMode,
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',                        
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
			listeners:
			{ 
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_PayMode;
}
function viCombo_Curr(lebar,Nama_ID)
{
    var Field_Curr = [' '];
    ds_Curr = new WebApp.DataStore({fields: Field_Curr});   
	
    var cbo_PayModeCurr = new Ext.form.ComboBox
    (
        {
            flex: 1,
            fieldLabel: '',
			valueField: '',
            displayField: '',
			emptyText:'Rp...',
			store: ds_Curr,
            width: lebar,
            mode: 'local',
            typeAhead: true,
            triggerAction: 'all',                        
            name: Nama_ID,
            lazyRender: true,
            id: Nama_ID,
			listeners:
			{ 
				'specialkey' : function()
				{
					if (Ext.EventObject.getKey() === 13) 
					{
						
					} 						
				}
			}
        }
    )    
    return cbo_PayModeCurr;
}