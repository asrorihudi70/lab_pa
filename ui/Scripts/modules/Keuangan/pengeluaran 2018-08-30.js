
var now = new Date();
now.format('Y-m-d');
var dsSpesialisasiPengeluaran;
var ListHasilPengeluaran;
var rowSelectedHasilPengeluaran;
var dsKelasPengeluaran;
var dsKamarPengeluaran;
var dataSource_Pengeluaran;
var dataSource_detPengeluaran;
var dsPerawatPengeluaran;
var rowSelected_viPengeluaran;
var account_nci_Pengeluaran;
var creteria_Pengeluaran="";
var account_payment;
var xz=0;
var  grid_detail_Pengeluaran;
var ds_Pengeluaran_grid_detail;
var Account_Pengeluaran_ds=new Ext.data.ArrayStore({id: 0,fields: ['text','account','name'],data: []});
var Pembayaran_Pengeluaran_ds=new Ext.data.ArrayStore({id: 0,fields: ['text','pay_code','payment'],data: []});
var dsUnitKerjaPagu;
var gridListHasilPengeluaran;
var tmp_kd_unit_kerja;
CurrentPage.page = getPanelPengeluaran(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
function getPanelPengeluaran(mod_id) {
    var Field = ['cso_number', 'personal', 'nama_account','account', 'alamat', 'kota', 'negara', 'kd_pos',
         'telepon1', 'telepon2', 'fax', 'amount'];
    dataSource_Pengeluaran = new WebApp.DataStore({
        fields: Field
    });

    load_Pengeluaran("");
   gridListHasilPengeluaran = new Ext.grid.EditorGridPanel({
        stripeRows: true,
        store: dataSource_Pengeluaran,
        anchor: '100% 80%',
        columnLines: false,
        autoScroll: false,
        border: true,
        sort: false,
        autoHeight: false,
        layout: 'fit',
        region: 'center',
        height: 150,
        sm: new Ext.grid.RowSelectionModel({
            singleSelect: true,
            listeners: {
                rowselect: function (sm, row, rec) {
                    //rowSelected_viPengeluaran = dataSource_Pengeluaran.getAt(row);
                }
            }
        }),
        listeners: {
            rowdblclick: function (sm, ridx, cidx) {
                rowSelected_viPengeluaran = dataSource_Pengeluaran.getAt(ridx);
                console.log(rowSelected_viPengeluaran);
                if (rowSelected_viPengeluaran !== undefined)
                {
					console.log(rowSelected_viPengeluaran);
                    /* if(rowSelected_viPengeluaran.data.statusl==='true' || rowSelected_viPengeluaran.data.statusl==='t')
					{
						 Ext.Msg.show({
                            title: 'Warning',
                            msg: 'Data yang sudah di posting tidak bisa di ubah',
                            buttons: Ext.MessageBox.OK,
                            fn: function (btn) {
								console.log(btn);
                                if (btn == 'ok') {
										HasilPengeluaranLookUp(rowSelected_viPengeluaran);		
                                }
                            },
                            icon: Ext.MessageBox.WARNING
                        });
						
					}else{ */
						HasilPengeluaranLookUp(rowSelected_viPengeluaran);	
					// }
					
                }
                else
                {
                    HasilPengeluaranLookUp();
                }
            }
        },
        cm: new Ext.grid.ColumnModel(
                [
                    new Ext.grid.RowNumberer(),
                     {
                        id: 'colCodeViewstatusPengeluaran',
                        header: 'Posting',
                        dataIndex: 'statusl',
                        sortable: true,
                        width: 50,
						style: 'text-align: center',
						renderer: function(value, metaData, record, rowIndex, colIndex, store)
                        {
                             switch (value)
                             { 
                                 case 't':
                                         metaData.css = 'StatusHijau'; //
                                         break;
                                 case 'f':
                                        metaData.css = 'StatusMerah'; // rejected

                                         break;
                             }
                             return '';
                        }
                    },
					
					{
                        id: 'colCodeViewHasilPengeluaran',
                        header: 'No Voucher',
                        dataIndex: 'cso_number',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 90
                    },
                    {
                        id: 'colNameViewHasilPengeluaran',
                        header: 'Dibayar kepada ',
                        dataIndex: 'personal',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 90
                    },
                    {
                        id: 'colaccountViewHasilPengeluaran',
                        header: 'Akun',
                        dataIndex: 'account',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 50
                    },
					{
                        id: 'colnama_accountViewHasilPengeluaran',
                        header: 'Nama Akun',
                        dataIndex: 'nama_account',
                        sortable: false,
                        hideable: false,
                        menuDisabled: true,
                        width: 170
                    },
					{
                        id: 'col_Amaounts_Pengeluaran',
                        header: 'Jumlah',
                        dataIndex: 'amount',
                        sortable: false,
						align : 'right',
                        hideable: false,
                        menuDisabled: true,
                        width: 170,
						renderer : function (v, params, record)
						{
						return formatCurrency(record.data.amount);
						},
                    }
                ]
                ),
        tbar: {
            xtype: 'toolbar',
            items: [
						{
							xtype: 'button',
							text: 'Baru',
							iconCls: 'AddRow',
							id: 'btnNew_Pengeluaran',
							handler: function ()
							{
								HasilPengeluaranLookUp();
							}
						} ,
					]
        },
        viewConfig: {
            forceFit: true
        }
    });
    var FormDepanPengeluaran = new Ext.Panel({
        id: mod_id,
        closable: true,
        region: 'center',
        layout: 'form',
        title: 'Pengeluaran',
        border: false,
        shadhow: true,
        autoScroll: false,
        iconCls: 'Request',
        margins: '0 5 5 0',
        items: [
            getPanelPencarianPengeluaran(),
            gridListHasilPengeluaran
        ],
        listeners: {
            'afterrender': function () {

            }
        }
    });
    return FormDepanPengeluaran;
}
;
function getPanelPencarianPengeluaran() {
    var items = {
        layout: 'column',
        border: false,
        items: [
            {
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 1190,
                height: 80,
                anchor: '100% 100%',
                items: [
                    {
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'No. Voucher '
                    },
                    {
                        x: 110,
                        y: 10,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 10,
                        xtype: 'textfield',
                        name: 'TxtCodePengeluaran',
                        id: 'TxtCodePengeluaran',
						width: 150,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {

                                        }

                                    }
                                }
                    },
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Tanggal '
                    },
                    {
                        x: 110,
                        y: 40,
                        xtype: 'label',
                        text: ' : '
                    },
                    {
                        x: 120,
                        y: 40,
                        xtype: 'datefield',
                        name: 'TxtTgl_voucher_pengeluaran',
                        id: 'TxtTgl_voucher_pengeluaran',
                        width: 150,
						format: 'd/M/Y',
						value :now,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
											load_Pengeluaran(Ext.getCmp('TxtCodePengeluaran').getValue(),Ext.getCmp('TxtTgl_voucher_pengeluaran').getValue(),Ext.getCmp('TxtTgl_voucher_pengeluaran2').getValue());
                                        }
                                    }
                                }
                    },
					{
                        x: 290,
                        y: 40,
                        xtype: 'label',
                        text: ' s/d '
                    },
					{
                        x: 330,
                        y: 40,
                        xtype: 'datefield',
                        name: 'TxtTgl_voucher_pengeluaran2',
                        id: 'TxtTgl_voucher_pengeluaran2',
                        width: 150,
						format: 'd/M/Y',
						value :now,
                        listeners:
                                {
                                    'specialkey': function ()
                                    {
                                        if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9)
                                        {
											load_Pengeluaran(Ext.getCmp('TxtCodePengeluaran').getValue(),Ext.getCmp('TxtTgl_voucher_pengeluaran').getValue(),Ext.getCmp('TxtTgl_voucher_pengeluaran2').getValue());
                                        }
                                    }
                                }
                    },
                    {
                        x: 520,
                        y: 40,
						width:100,
                        xtype: 'button',
                        text: 'Cari',
                        iconCls: 'find',
                        id: 'btnCari_Pengeluaran',
                        handler: function ()
                        { 

						//creteria_Pengeluaran2();
						load_Pengeluaran(Ext.getCmp('TxtCodePengeluaran').getValue(),Ext.getCmp('TxtTgl_voucher_pengeluaran').getValue(),Ext.getCmp('TxtTgl_voucher_pengeluaran2').getValue());
                        }
                    }
                ]
            }
        ]
    };
    return items;
}
;
function creteria_Pengeluaran2(){
var msec =new Date();
	msec.parse(" 2012-01-01");
	if (Ext.getCmp('TxtCodePengeluaran').getValue()!=="" && Ext.getCmp('TxtTgl_voucher_pengeluaran').getValue()==="" )
	{
	creteria_Pengeluaran="lower(cso_number) like lower('%"+ Ext.getCmp('TxtCodePengeluaran').getValue()+"%')";			
	}
	if (Ext.getCmp('TxtCodePengeluaran').getValue()==="" && Ext.getCmp('TxtTgl_voucher_pengeluaran').getValue()!=="" )
	{
	creteria_Pengeluaran="cso_date in('"+ msec +"')";
		
	}
	if (Ext.getCmp('TxtCodePengeluaran').getValue()!=="" && Ext.getCmp('TxtTgl_voucher_pengeluaran').getValue()!=="" )
	{
	creteria_Pengeluaran="lower(cso_number) like lower('"+ Ext.getCmp('TxtCodePengeluaran').getValue()+"%') and cso_date in ('"+ Ext.getCmp('TxtTgl_voucher_pengeluaran').getValue()+"')";
		
	}
	if (Ext.getCmp('TxtCodePengeluaran').getValue()==="" && Ext.getCmp('TxtTgl_voucher_pengeluaran').getValue()==="" )
	{
	creteria_Pengeluaran="";
	}

	return creteria_Pengeluaran;
}
function HasilPengeluaranLookUp(rowdata) {
    FormLookUpdetailPengeluaran = new Ext.Window({
        id: 'gridHasilPengeluaran',
        title: 'Pengeluaran',
        closeAction: 'destroy',
        width: 720,
        height: 585,
        border: false,
        resizable: false,
        plain: true,
        layout: 'fit',
        constrain: true,
        iconCls: 'Request',
        modal: true,
        items: [
				form_Pengeluaran(),
				],
        listeners: {
        }
    });
    FormLookUpdetailPengeluaran.show();
    if (rowdata === undefined||rowdata === "") {
		new_data();
    } else {
        datainit_Pengeluaran(rowdata);
    }

}
;

function Posting_LookUp(rowdata) {
	xz=0;
    FormLook_Posting = new Ext.Window({
        id: 'FormLook_Posting',
        title: 'Posting',
        closeAction: 'destroy',
        width: 400,
        height: 240,
        border: false,
        resizable: false,
        plain: true,
        layout: 'fit',
        constrain: true,
        iconCls: 'Request',
        modal: true,
        items: [
				PanelPengeluaran_posting(),
				],
        listeners: {
        },
		fbar:
			{
				xtype: 'toolbar',
				items:
						[
							{
								xtype: 'button',
								text: 'Posting',
								iconCls: '',
								id: 'btnSimpanpost_Pengeluaran',
								handler: function ()
								{
									PostData()
								}
							}
						]
			}
                    
    });
    FormLook_Posting.show();
    datainit_post_Pengeluaran(rowdata);
    
};

function parampost()
{
    var params =
            {
				no_voucher	:Ext.getCmp('Txt_post_VoucherPengeluaran').getValue(),
				catatan		:Ext.getCmp('Txt_Post_catatan_Pengeluaran').getValue(),
				kd_unit_kerja :tmp_kd_unit_kerja
			};
   return params;
}
function PostData()
{
/* 	if (account_payment.getValue()===""){
	ShowPesanWarningPengeluaran('Type Bayar Belum diisi', 'Perhatian');	
	}else{ */
		Ext.Ajax.request
		(
			{	
				url: baseURL + "index.php/anggaran/Pengeluaran/post",
				params: parampost(),
				failure: function (o){
					load_Pengeluaran(Ext.getCmp('TxtCodePengeluaran').getValue(),Ext.getCmp('TxtTgl_voucher_pengeluaran').getValue(),Ext.getCmp('TxtTgl_voucher_pengeluaran2').getValue());
					ShowPesanWarningPengeluaran('Proses Simpan Tidak berhasil silahkan hubungi admin', 'Gagal');
				},
				success: function (o){
					load_Pengeluaran(Ext.getCmp('TxtCodePengeluaran').getValue(),Ext.getCmp('TxtTgl_voucher_pengeluaran').getValue(),Ext.getCmp('TxtTgl_voucher_pengeluaran2').getValue());
					var cst = Ext.decode(o.responseText);
					if (cst.success === true){
						
						load_Pengeluaran_detail(Ext.getCmp('TxtVoucherPengeluaran').getValue());							
						ShowPesanInfoPengeluaran('Proses Posting Berhasil', 'Save');
						/* Ext.get('btnSimpanpost_Pengeluaran').disabled();
						Ext.get('btnSimpan_Pengeluaran').disabled();
						Ext.get('btnPosting_Pengeluaran').disabled(); */
						Ext.getCmp('btnSimpanpost_Pengeluaran').setDisabled(true);
						Ext.getCmp('btnSimpan_Pengeluaran').setDisabled(true);
						Ext.getCmp('btnPosting_Pengeluaran').setDisabled(true);
						
					}
					 else{
						if (cst.cari === true){
						loadMask.hide();
						ShowPesanWarningPengeluaran('Login Ulang User Tidak teridentifikasi', 'User');	
						}else{
						ShowPesanWarningPengeluaran('Proses Simpan Tidak berhasil silahkan hubungi admin', 'Gagal');
						}
					};
				}
			}
		);
//	}
};

function datainit_post_Pengeluaran(rowdata)
{//
	Ext.getCmp('Txt_post_VoucherPengeluaran').setValue("P"+Ext.getCmp('TxtVoucherPengeluaran').getValue());
    Ext.getCmp('Txt_post_total_Pengeluaran').setValue(Ext.getCmp('Txttotal_jumlah').getValue());
}


function PanelPengeluaran_posting() {
    var items = new Ext.Panel
            (
                    {
                        title: '',
                        id: 'PanelDataPosting_Pengeluaran',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        bodyStyle: 'padding:0px 0px 0px 0px',
                        border: true,
                        autoscroll: true,
                        items: [
                            {
                                layout: 'absolute',
                                bodyStyle: 'padding: 5px 5px 5px 5px',
                                border: false,
                                width: 400,
                                height: 240,
                                anchor: '100% 100%',
                                items: [
                                    {
                                        x: 10,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'No. Voucher '
                                    },
                                    {
                                        x: 110,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 10,
                                        xtype: 'textfield',
                                        name: 'Txt_post_VoucherPengeluaran',
                                        id: 'Txt_post_VoucherPengeluaran',
                                        width: 200,
                                        readOnly: false,
										disabled:true,
                        
                                    },
                                    {
                                        x: 10,
                                        y: 40,
                                        xtype: 'label',
                                        text: 'Tanggal '
                                    },
                                    {
                                        x: 110,
                                        y: 40,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 40,
                                        xtype: 'datefield',
                                        name: 'Txt_post_tanggal_Pengeluaran',
                                        id: 'Txt_post_tanggal_Pengeluaran',
										format: 'Y-m-d',
										value:now,
										disabled:true,
                                        width: 200,
                                    },
                                    {
                                        x: 10,
                                        y: 70,
                                        xtype: 'label',
                                        text: 'Catatan '
                                    },
                                    {
                                        x: 110,
                                        y: 70,
                                        xtype: 'label',
                                        text: ' : '
                                    },
									{
                                        x: 120,
                                        y: 70,
                                        xtype: 'textarea',
                                        name: 'Txt_Post_catatan_Pengeluaran',
                                        id: 'Txt_Post_catatan_Pengeluaran',
                                        width: 200,
										height : 50,
                                       // readOnly: false,
										//disabled:true,
                                    },
									{
                                        x: 10,
                                        y: 130,
                                        xtype: 'label',
                                        text: 'Total '
                                    },
                                    {
                                        x: 110,
                                        y: 130,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 130,
                                        xtype: 'textfield',
                                        name: 'Txt_post_total_Pengeluaran',
                                        id: 'Txt_post_total_Pengeluaran',
                                        width: 200,
                                        readOnly: false,
										style: 'text-align: right',
										disabled:true,
                                    }
								]
                            }
                        ]
                    });
    return items;
}
;


function form_Pengeluaran() {
    var Isi_form_Pengeluaran = new Ext.Panel
            (
                    {
                        id: 'form_Pengeluaran',
                        closable: true,
                        region: 'center',
                        layout: 'form',
                        itemCls: 'blacklabel',
                        bodyStyle: 'padding: 0px 0px 0px 0px',
                        border: true,
                        shadhow: true,
                        width: 700,
						height: 200,
						iconCls: '',
                        items:
                                [
                                    PanelPengeluaran(),
									Getgrid_detail_Pengeluaran()
                                ],
                        tbar:
                                {
                                    xtype: 'toolbar',
                                    items:
                                            [
												{
													xtype: 'button',
													text: 'Baru',
													iconCls: 'AddRow',
													id: 'btnNew_Pengeluaran2',
													handler: function ()
													{
														new_data();
													}
												},
                                                {
                                                    xtype: 'button',
                                                    text: 'Simpan',
                                                    iconCls: 'save',
                                                    id: 'btnSimpan_Pengeluaran',
                                                    handler: function ()
                                                    {
                                                        SavingData()
                                                    }
                                                },
                                                {
                                                    xtype: 'button',
                                                    text: 'Posting',
                                                    iconCls: 'approve',
                                                    id: 'btnPosting_Pengeluaran',
                                                    handler: function ()
                                                    {
														if (Ext.getCmp('TxtVoucherPengeluaran').getValue()==="")
															{
															ShowPesanWarningPengeluaran('Tidak dapat diposting data sebelumnya disave', 'Perhatian');		
															}else{
															Posting_LookUp()
															}
														
														//SavingData()
                                                    }
                                                }
                                                 
                                            ]
                                }
                    }
            );
    return Isi_form_Pengeluaran;
}
;
function PanelPengeluaran() {
	 account_nci_Pengeluaran= Nci.form.Combobox.autoComplete({
		x: 120,
		y: 40,
		store	: Account_Pengeluaran_ds,
		select	: function(a,b,c){
						
						Ext.getCmp('TxtPopupNamaPengeluaran').setValue(b.data.name);
						//account_nci_Pengeluaran.setValue(b.data.account);
						},
		insert	: function(o){
			return {
				account         :o.account,
				name 			: o.name,
				text			:  '<table style="font-size: 11px;"><tr><td width="50">'+o.account+'</td><td width="200">'+o.name+'</td></tr></table>'
			}
		},
		url: baseURL + "index.php/anggaran/Pengeluaran/accounts",
		valueField: 'account',
		displayField: 'text',
		listWidth: 300,
		width:180
	});
    
    
	/*  account_payment= Nci.form.Combobox.autoComplete({
		        x: 120,
                y: 100,
				store	: Pembayaran_Pengeluaran_ds,
				select	: function(a,b,c){
											Ext.getCmp('TxtPopunama_type_Pengeluaran').setValue(b.data.payment);
								//account_payment.setValue(b.data.pay_code);
								},
				insert	: function(o){
					return {
						pay_code :o.pay_code,
						payment 	: o.payment,
						text				:  '<table style="font-size: 11px;"><tr><td width="50">'+o.pay_code+'</td><td width="200">'+o.payment+'</td></tr></table>'
					}
				},
				url: baseURL + "index.php/anggaran/Pengeluaran/pay_acc",
				valueField: 'pay_code',
				displayField: 'text',
				listWidth: 100,
				width:40
			}); */
    
	var items = new Ext.Panel
            (
                    {
                        title: '',
                        id: 'PanelDataPengeluaran',
                        fileUpload: true,
                        region: 'north',
                        layout: 'form',
                        bodyStyle: 'padding:0px 0px 0px 0px',
                        border: true,
                        autoscroll: true,
                        items: [
                            {
                                layout: 'absolute',
                                bodyStyle: 'padding: 5px 5px 5px 5px',
                                border: false,
                                width: 500,
                                height: 200,
                                anchor: '100% 100%',
                                items: [
                                    {
                                        x: 10,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'No Voucher '
                                    },
                                    {
                                        x: 110,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 10,
                                        xtype: 'textfield',
                                        name: 'TxtVoucherPengeluaran',
                                        id: 'TxtVoucherPengeluaran',
                                        width: 180,
                                        readOnly: true,
                        
                                    },
                                    {
                                        x: 320,
                                        y: 10,
                                        xtype: 'label',
                                        text: 'Tanggal '
                                    },
                                    {
                                        x: 380,
                                        y: 10,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 390,
                                        y: 10,
                                        xtype: 'datefield',
                                        name: 'Txt_tanggal_Pengeluaran',
                                        id: 'Txt_tanggal_Pengeluaran',
										format: 'd/M/Y',
										value:now,
										readOnly:true,
                                        width: 100,
                                    },
                                    {
                                        x: 320,
                                        y: 40,
                                        xtype: 'label',
                                        text: 'Nama Akun '
                                    },
                                    {
                                        x: 380,
                                        y: 40,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 390,
                                        y: 40,
                                        xtype: 'textfield',
                                        name: 'TxtPopupNamaPengeluaran',
                                        id: 'TxtPopupNamaPengeluaran',
                                        width: 290,
										readOnly:true,
                                    },
                                    {
                                        x: 10,
                                        y: 70,
                                        xtype: 'label',
                                        text: 'Dibayar kepada '
                                    },
                                    {
                                        x: 110,
                                        y: 70,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 120,
                                        y: 70,
                                        xtype: 'textfield',
                                        name: 'TxtPopuPenerimaPengeluaran',
                                        id: 'TxtPopuPenerimaPengeluaran',
                                        width: 250,
                                        readOnly: false
                                    },
									{
                                        x: 390,
                                        y: 70,
                                        xtype: 'label',
                                        text: 'Unit Kerja '
                                    },
									mComboUnitKerjaPagu(),
                                    {
                                        x: 440,
                                        y: 70,
                                        xtype: 'label',
                                        text: ' : '
                                    },
									{
                                        x: 390,
                                        y: 100,
                                        xtype: 'label',
                                        text: 'Kode'
                                    },
                                    {
                                        x: 440,
                                        y: 100,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    {
                                        x: 460,
                                        y: 100,
                                        xtype: 'textfield',
                                        name: 'TxtPopukode_xpayPengeluaran',
                                        id: 'TxtPopukode_xpayPengeluaran',
                                        width: 220,
                                        readOnly: false
                                    },
                                    {
                                        x: 10,
                                        y: 40,
                                        xtype: 'label',
                                        text: 'Akun '
                                    },
                                    {
                                        x: 110,
                                        y: 40,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    account_nci_Pengeluaran,
									{
                                        x: 10,
                                        y: 100,
                                        xtype: 'label',
                                        text: 'Type Bayar'
                                    },
                                    {
                                        x: 110,
                                        y: 100,
                                        xtype: 'label',
                                        text: ' : '
                                    },
                                    // account_payment,
									mcomboBayar_CSARForm(),
									/* {
                                        x: 162,
                                        y: 100,
                                        xtype: 'textfield',
                                        name: 'TxtPopunama_type_Pengeluaran',
                                        id: 'TxtPopunama_type_Pengeluaran',
                                        width: 113,
                                        readOnly: false,
										disabled :true
                                    }, */
									{
                                        x: 10,
                                        y: 130,
                                        xtype: 'label',
                                        text: 'Catatan '
                                    },
                                    {
                                        x: 110,
                                        y: 130,
                                        xtype: 'label',
                                        text: ' : '
                                    },
									{
                                        x: 120,
                                        y: 130,
                                        xtype: 'textarea',
                                        name: 'Txt_catatan_Pengeluaran',
                                        id: 'Txt_catatan_Pengeluaran',
                                        width: 560,
										height : 50,
                                        readOnly: false
                                    }
								]
                            }
                        ]
                    });
    return items;
};

function mComboUnitKerjaPagu() 
{
    var Field = ['kd_unit', 'nama_unit', 'unitkerja'];
    dsUnitKerjaPagu = new WebApp.DataStore({ fields: Field });

    var comboUnitKerjaPagu = new Ext.form.ComboBox
	(
		{
		    id: 'comboUnitKerjaPagu',
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    // emptyText: 'Pilih '+gstrSatker+'...',
		    emptyText: 'Pilih Unit Kerja...',
		    // fieldLabel: gstrSatker,
		    fieldLabel: 'gstrSatker',
		    align: 'right',
		    width: 220,
			x: 460,
			y: 70,
		    store: dsUnitKerjaPagu,
		    valueField: 'kd_unit',
		    displayField: 'nama_unit',
		    listeners:
			{
			    'select': function(a, b, c) 
				{
			        tmp_kd_unit_kerja = b.data.kd_unit;
			    }
			}
		}
	);


	Ext.Ajax.request ({
		url: baseURL + "index.php/anggaran/Pengeluaran/unit_kerja",
		params: {
			text:''
		},
		failure: function(o)
		{
			// ShowPesanError('Error menampilkan data unit kerja !', 'Error');
		},	
		success: function(o) 
		{   
			dsUnitKerjaPagu.removeAll();
			var cst = Ext.decode(o.responseText);

			if (cst.success === true) {
				var recs=[],
					recType=dsUnitKerjaPagu.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));						
				}
				dsUnitKerjaPagu.add(recs);
				// gridDTLTR_PaguGNRL.getView().refresh();
			} else {
				// ShowPesanError('Gagal menampilkan data unit kerja', 'Error');
			};
		}
	});
    return comboUnitKerjaPagu;
};
function mcomboBayar_CSARForm()
{
	var Field = ['pay_code', 'payment'];
	dsBayar_CSARForm = new WebApp.DataStore({ fields: Field });
	
	Ext.Ajax.request({
		url: baseURL + "index.php/keuangan/functionGeneral/getJenisBayar",
		params: {query:''},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			comboBayar_CSARForm.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsBayar_CSARForm.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsBayar_CSARForm.add(recs);
			}
		}
	});

  var comboBayar_CSARForm = new Ext.form.ComboBox
	(
		{
			x: 120,
			y: 100,
			id:'comboBayar_CSARForm',
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			emptyText:'Pilih Jenis Pembayaran...',
			fieldLabel: 'Jenis Pembayaran ',			
			align:'Right',
			// anchor:'95%',
			width:250,
			store: dsBayar_CSARForm,
			valueField: 'pay_code',
			displayField: 'payment',
			listeners:  
			{
				'select': function(a,b,c)
				{   
					account_payment=b.data.pay_code ;
				} 
			}
		}
	);
	
	return comboBayar_CSARForm;
} ;

function datainit_Pengeluaran(rowdata)
{
	
	xxx=new Date(rowdata.data.cso_date);
    Ext.getCmp('TxtVoucherPengeluaran').setValue(rowdata.data.cso_number);
    Ext.getCmp('TxtPopupNamaPengeluaran').setValue(rowdata.data.nama_account);
    Ext.getCmp('Txt_tanggal_Pengeluaran').setValue(xxx);
    Ext.getCmp('TxtPopuPenerimaPengeluaran').setValue(rowdata.data.personal);
    account_nci_Pengeluaran.setValue(rowdata.data.account);
	account_payment= rowdata.data.pay_code;
	tmp_kd_unit_kerja = rowdata.data.kd_unit_kerja;
	Ext.getCmp('comboBayar_CSARForm').setValue(rowdata.data.payment);
	Ext.getCmp('comboUnitKerjaPagu').setValue(rowdata.data.nama_unit);
	Ext.getCmp('TxtPopukode_xpayPengeluaran').setValue(rowdata.data.pay_no);
	Ext.getCmp('Txt_catatan_Pengeluaran').setValue(rowdata.data.notes1);
	load_Pengeluaran_detail(rowdata.data.cso_number);
	
	if(rowdata.data.statusl == 't'){
		Ext.getCmp('btnSimpan_Pengeluaran').setDisabled(true);
		Ext.getCmp('btnPosting_Pengeluaran').setDisabled(true);
		Ext.getCmp('btnbaris_Pengeluaran').setDisabled(true);
		Ext.getCmp('btnhapus_baris_Pengeluaran').setDisabled(true);
		Ext.getCmp('comboBayar_CSARForm').setDisabled(true);
		Ext.get('Txt_catatan_Pengeluaran').dom.readOnly=true;
		Ext.get('TxtPopuPenerimaPengeluaran').dom.readOnly=true;
		Ext.get('TxtPopukode_xpayPengeluaran').dom.readOnly=true;
		Ext.get('TxtVoucherPengeluaran').dom.readOnly=true;
		account_nci_Pengeluaran.setDisabled(true);
	}else{
		Ext.getCmp('btnSimpan_Pengeluaran').setDisabled(false);
		Ext.getCmp('btnPosting_Pengeluaran').setDisabled(false);
		Ext.getCmp('btnbaris_Pengeluaran').setDisabled(false);
		Ext.getCmp('btnhapus_baris_Pengeluaran').setDisabled(false);
		Ext.get('Txt_catatan_Pengeluaran').dom.readOnly=false;
		Ext.get('TxtPopuPenerimaPengeluaran').dom.readOnly=false;
		Ext.get('TxtPopukode_xpayPengeluaran').dom.readOnly=false;
		Ext.get('TxtVoucherPengeluaran').dom.readOnly=false;
		account_nci_Pengeluaran.setDisabled(false);
	}
	
}

function new_data()
{

	Ext.getCmp('btnSimpan_Pengeluaran').setDisabled(false);
	Ext.getCmp('btnPosting_Pengeluaran').setDisabled(false);
	Ext.getCmp('btnbaris_Pengeluaran').setDisabled(false);
	Ext.getCmp('btnhapus_baris_Pengeluaran').setDisabled(false);
	account_nci_Pengeluaran.setDisabled(false);
   // console.log(rowdata.cso_number)
    Ext.getCmp('TxtVoucherPengeluaran').setValue();
    Ext.getCmp('TxtPopupNamaPengeluaran').setValue();
    Ext.getCmp('Txt_tanggal_Pengeluaran').setValue(now);
    Ext.getCmp('TxtPopuPenerimaPengeluaran').setValue();
    account_nci_Pengeluaran.setValue();
	tmp_kd_unit_kerja ='';
	// account_payment.setValue();
	// Ext.getCmp('TxtPopunama_type_Pengeluaran').setValue();
	Ext.getCmp('TxtPopukode_xpayPengeluaran').setValue();
	Ext.getCmp('Txt_catatan_Pengeluaran').setValue();
	ds_Pengeluaran_grid_detail.removeAll();
	grid_detail_Pengeluaran.getView().refresh();
	
}


function SavingData()
{   if (ds_Pengeluaran_grid_detail.getCount()===0 || ds_Pengeluaran_grid_detail.getCount()==="")
	{
	ShowPesanWarningPengeluaran('Isi kolom dan baris didalam tabel', 'Perhatian');		
	}else{
	if (Ext.getCmp('TxtPopuPenerimaPengeluaran').getValue()===""){
	ShowPesanWarningPengeluaran('Pengirim Belum diisi', 'Perhatian');	
	}
	else{
	if (account_nci_Pengeluaran.getValue()===""){
		 ShowPesanWarningPengeluaran('Akun Belum diisi', 'Perhatian');
	}else{
		if (Ext.getCmp('comboBayar_CSARForm').getValue()===""){
		ShowPesanWarningPengeluaran('Type Bayar Belum diisi', 'Perhatian');	
		}else{
			Ext.Ajax.request
			(
				{	
					url: baseURL + "index.php/anggaran/Pengeluaran/save",
					params: paramsaving(),
					failure: function (o){
						load_Pengeluaran(Ext.getCmp('TxtCodePengeluaran').getValue(),Ext.getCmp('TxtTgl_voucher_pengeluaran').getValue(),Ext.getCmp('TxtTgl_voucher_pengeluaran2').getValue());
						ShowPesanWarningPengeluaran('Proses Simpan Tidak berhasil silahkan hubungi admin', 'Gagal');
					},
					success: function (o){
						load_Pengeluaran(Ext.getCmp('TxtCodePengeluaran').getValue(),Ext.getCmp('TxtTgl_voucher_pengeluaran').getValue(),Ext.getCmp('TxtTgl_voucher_pengeluaran2').getValue());
						var cst = Ext.decode(o.responseText);
						if (cst.success === true){
							Ext.getCmp('TxtVoucherPengeluaran').setValue(cst.no_voucher);
							load_Pengeluaran_detail(cst.no_voucher);							
							ShowPesanInfoPengeluaran('Proses Simpan Berhasil', 'Save');
						}
						 else{
							if (cst.cari === true){
							loadMask.hide();
							ShowPesanWarningPengeluaran('Login Ulang User Tidak teridentifikasi', 'User');	
							}else{
							ShowPesanWarningPengeluaran('Proses Simpan Tidak berhasil silahkan hubungi admin', 'Gagal');
							}
						};
					}
				}
			);
		}
		
	}

	}
	
	}
};

function paramsaving()
{
    var params =
            {
				no_voucher		:Ext.getCmp('TxtVoucherPengeluaran').getValue(),
				pengirim		:Ext.getCmp('TxtPopuPenerimaPengeluaran').getValue(),
				type_bayar		:account_payment,
				tgl_cso			:Ext.getCmp('Txt_tanggal_Pengeluaran').getValue(),
				account			:account_nci_Pengeluaran.getValue(),
				notes  			:Ext.getCmp('Txt_catatan_Pengeluaran').getValue(),
				pay_no			:Ext.getCmp('TxtPopukode_xpayPengeluaran').getValue(),
				amount			:xz,
				kd_unit_kerja	:tmp_kd_unit_kerja
			};
			params['jumlah']=ds_Pengeluaran_grid_detail.getCount()
			for (var L=0; L<ds_Pengeluaran_grid_detail.getCount(); L++)
			{
				params['account_detail'+L]		=ds_Pengeluaran_grid_detail.data.items[L].data.account;
				params['name_detail'+L]			=ds_Pengeluaran_grid_detail.data.items[L].data.name;
				params['description_detail'+L]	=ds_Pengeluaran_grid_detail.data.items[L].data.description;
				params['value'+L]				=ds_Pengeluaran_grid_detail.data.items[L].data.value;
				params['line'+L]				=ds_Pengeluaran_grid_detail.data.items[L].data.line;
				params['kd_unit_kerja'+L]		=tmp_kd_unit_kerja;
			}
   
   return params;
}


function load_Pengeluaran(cso_number,tgl,tgl2){
	//dataSource_Pengeluaran
		Ext.Ajax.request({
			url: baseURL + "index.php/anggaran/Pengeluaran/select",
			params: {
				cso_number:cso_number,
				tgl:tgl,
				tgl2:tgl2,
			},
			failure: function(o){  	
				loadMask.hide();
				ShowPesanWarningPengeluaran('Hubungi Admin', 'Error');
			},	
			success: function(o){
			var cst = Ext.decode(o.responseText);
			if (cst.success === true){
				dataSource_Pengeluaran.removeAll();
				var recs=[],
				recType=dataSource_Pengeluaran.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));
				}
				dataSource_Pengeluaran.add(recs);
				gridListHasilPengeluaran.getView().refresh();
			} 
			}
		})
}



function ShowPesanWarningPengeluaran(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING,
                        width: 250
                    }
            );
}
;

function ShowPesanInfoPengeluaran(str, modul) {
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO,
                        width: 250
                    }
            );
}
;

function DeleteData(line)
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/anggaran/Pengeluaran/delete",
                        params: paramdelete(line),
                        failure: function (o){
							//creteria_Pengeluaran2();
							load_Pengeluaran(Ext.getCmp('TxtCodePengeluaran').getValue(),Ext.getCmp('TxtTgl_voucher_pengeluaran').getValue(),Ext.getCmp('TxtTgl_voucher_pengeluaran2').getValue());
							loadMask.hide();
                            ShowPesanWarningPengeluaran('Hubungi Admin', 'Error');
                        },
                        success: function (o)
                        { 	//creteria_Pengeluaran2();
							load_Pengeluaran(Ext.getCmp('TxtCodePengeluaran').getValue(),Ext.getCmp('TxtTgl_voucher_pengeluaran').getValue(),Ext.getCmp('TxtTgl_voucher_pengeluaran2').getValue());
							var cst = Ext.decode(o.responseText);
                            if (cst.success === true){
								
								loadMask.hide();
                                ShowPesanInfoPengeluaran('Data berhasil di hapus', 'Information');
                                 ds_Pengeluaran_grid_detail.removeAt(line);
                                 grid_detail_Pengeluaran.getView().refresh(); 
							}
							else{
								 if (cst.tidak_ada_line === true){
								
								 ds_Pengeluaran_grid_detail.removeAt(line);
                                 grid_detail_Pengeluaran.getView().refresh(); 
								}else{
                                loadMask.hide();
                                ShowPesanWarningPengeluaran('Gagal menghapus data', 'Error');
								}
							}
                            ;
                        }
                    }

            );
}
;
function paramdelete(linex)
{
    var params =
            {
            no_voucher	:Ext.getCmp('TxtVoucherPengeluaran').getValue(),
			
			};
			params['line']=ds_Pengeluaran_grid_detail.data.items[linex].data.line;
  return params;
}

function Getgrid_detail_Pengeluaran(data) {

    var fldDetail = ['description', 'account', 'name',  'value', 'line'];
    ds_Pengeluaran_grid_detail = new WebApp.DataStore({fields: fldDetail});
    grid_detail_Pengeluaran = new Ext.grid.EditorGridPanel({
        title: 'Pengeluaran',
        id: 'grid_detail_Pengeluaran',
        stripeRows: true,
        store: ds_Pengeluaran_grid_detail,
        border: false,
        frame: false,
        height: 325,
		width: 500,
        anchor: '100%',
        autoScroll: true,
        sm: new Ext.grid.CellSelectionModel({
            singleSelect: true,
            listeners: {
                cellselect: function (sm, row, rec) {
                    /* var linepanatajasarwi = ds_Pengeluaran_grid_detail.getAt(row);
                    if (linepanatajasarwi.data.kd_pasien === undefined || linepanatajasarwi.data.kd_pasien === "")
                    {
                    } else {
                    } */

                }
            }
        }),
        cm: get_column_detail_Pengeluaran(),
		tbar:[
		{
                text: 'Tambah data',
                id: 'btnbaris_Pengeluaran',
                tooltip: nmLookup,
                iconCls: 'AddRow',
                handler: function () {

                    var records = new Array();
                    records.push(new ds_Pengeluaran_grid_detail.recordType());
                    ds_Pengeluaran_grid_detail.add(records);
					
                }
        },{
                text: 'Hapus data',
                id: 'btnhapus_baris_Pengeluaran',
                tooltip: nmLookup,
                iconCls: 'RemoveRow',
                handler: function () {
					var line = grid_detail_Pengeluaran.getSelectionModel().selection.cell[0];
                    if (grid_detail_Pengeluaran.getSelectionModel().selection == null) {
                        ShowPesanWarningPengeluaran('Harap Pilih terlebih dahulu data.', 'Gagal');
                    } else {
                        Ext.Msg.show({
                            title: nmHapusBaris,
                            msg: 'Anda yakin akan menghapus data dengan Akun' + ' : ' + ds_Pengeluaran_grid_detail.getRange()[line].data.account,
                            buttons: Ext.MessageBox.YESNO,
                            fn: function (btn) {
                                if (btn == 'yes') {
									DeleteData(line);
                                        /* ds_Pengeluaran_grid_detail.removeAt(line);
                                        grid_detail_Pengeluaran.getView().refresh(); */
                                }
                            },
                            icon: Ext.MessageBox.QUESTION
                        });
                    }
                }
        } 			
		
		],
		bbar:[
			{
				xtype: 'tbspacer',
				width: 530,
				height:30
			},{
		
				xtype: 'label',
				text: 'Total : '
			},{
				xtype: 'textfield',
				name: 'Txttotal_jumlah',
				id: 'Txttotal_jumlah',
				width: 140,
				disabled:true,
				style: 'text-align: right',
				value: 0,
				listeners:
						{
						}
			}
		]
       /*  viewConfig: {forceFit: true} */
    });

    return grid_detail_Pengeluaran;
}
function get_column_detail_Pengeluaran() {
  return new Ext.grid.ColumnModel([
        new Ext.grid.RowNumberer(),
		
		{
            id: Nci.getId(),
            header: 'Akun',
            width: 80,
            hidden: false,
            menuDisabled: true,
            dataIndex: 'account',
			editor: new Nci.form.Combobox.autoComplete({
		        x: 120,
                y: 40,
				store	: Account_Pengeluaran_ds,
				select	: function(a,b,c){
								var line = grid_detail_Pengeluaran.getSelectionModel().selection.cell[0];
								console.log(grid_detail_Pengeluaran); //name=b.data.name;
								//Ext.getCmp('TxtPopupNamaPengeluaran').setValue(b.data.name);
								ds_Pengeluaran_grid_detail.data.items[line].data.name=b.data.name;
								//account_nci_Pengeluaran.setValue(b.data.account);
								},
				insert	: function(o){
					return {
						account         :o.account,
						name 			: o.name,
						text			:  '<table style="font-size: 11px;"><tr><td width="50">'+o.account+'</td><td width="200">'+o.name+'</td></tr></table>'
					}
				},
				url: baseURL + "index.php/anggaran/Pengeluaran/accounts",
				valueField: 'account',
				displayField: 'text',
				listWidth: 300,
				width:100
			})
        }, {
            id: Nci.getId(),
            header: 'Nama',
            dataIndex: 'name',
            sortable: true,
            width: 250,
            align: 'left',
        }, {
            id: Nci.getId(),
            header: 'Deskripsi',
            dataIndex: 'description',
            width: 200,
            menuDisabled: true,
            hidden: false,
			renderer: function (v, params, record) {
				if (record.data.description == undefined || record.data.description == ""){
					record.data.description=Ext.getCmp('Txt_catatan_Pengeluaran').getValue();
				}
				return record.data.description;
			},
			editor:{	
			xtype:'textfield',
			allowBlank:false
			}
		},{
            id: Nci.getId(),
            header: 'Nilai',
            dataIndex: 'value',
			value:1,
            hidden: false,
            menuDisabled: true,
            width: 150,
			align:'right',
            editor: new Ext.form.NumberField ({
						allowBlank: false
					}),
			renderer: function(v, params, record)
			{
			total();
			return formatCurrency(record.data.value);
			}
        }
		/* ,{	
			id: Nci.getId(),
            header: 'No Anggaran',
            dataIndex: 'tgl_transaksi',
            width: 130,
            menuDisabled: true,
           
        },{
            id: Nci.getId(),
            header: 'Nama Anggaran',
            dataIndex: 'namadok',
            menuDisabled: true,
            hidden: false,
            width: 150,
        }, */
    ]);
}
function total(){
xz=0;
for (var L=0; L<ds_Pengeluaran_grid_detail.getCount(); L++)
{
	if (ds_Pengeluaran_grid_detail.data.items[L].data.value==="" ||
	 ds_Pengeluaran_grid_detail.data.items[L].data.value==undefined||
	 ds_Pengeluaran_grid_detail.data.items[L].data.value==="NaN" )
	{
		ds_Pengeluaran_grid_detail.data.items[L].data.value=0;
		xz=parseInt(xz)+parseInt(ds_Pengeluaran_grid_detail.data.items[L].data.value);
	}else{
		
		xz=parseInt(xz)+parseInt(ds_Pengeluaran_grid_detail.data.items[L].data.value);
	}
}
Ext.getCmp('Txttotal_jumlah').setValue(formatCurrency(xz));
}

function load_Pengeluaran_detail(criteria){
	//dataSource_Pengeluaran
		Ext.Ajax.request({
			url: baseURL + "index.php/anggaran/Pengeluaran/select_detail",
			params: {criteria:criteria},
			failure: function(o){  	
				loadMask.hide();
				ShowPesanWarningPengeluaran('Hubungi Admin', 'Error');
			},	
			success: function(o){
			var cst = Ext.decode(o.responseText);
			if (cst.success === true){
				ds_Pengeluaran_grid_detail.removeAll();
				var recs=[],
				recType=ds_Pengeluaran_grid_detail.recordType;
				for(var i=0; i<cst.ListDataObj.length; i++){
					recs.push(new recType(cst.ListDataObj[i]));
				}
				ds_Pengeluaran_grid_detail.add(recs);
				grid_detail_Pengeluaran.getView().refresh();
			} 
			}
		})
}


