var dataSource_viLabSetupDokter;
var selectCount_viLabSetupDokter=50;
var NamaForm_viLabSetupDokter="SetupDokter";
var mod_name_viLabSetupDokter="SetupDokter";
var now_viLabSetupDokter= new Date();
var rowSelected_viLabSetupDokter;
var setLookUps_viLabSetupDokter;
var tanggal = now_viLabSetupDokter.format("d/M/Y");
var jam = now_viLabSetupDokter.format("H/i/s");
var tmpkriteria;
var DataGridJenisObat;
var GridDataView_viLabSetupDokter;
var kodesetupDokter='';

var CurrentData_viLabSetupDokter =
{
	data: Object,
	details: Array,
	row: 0
};
var tmpkd_unit;
var default_Kd_Unit;
var dsunitlab_viSetupDokterLab;
var LabSetupDokter={};
LabSetupDokter.form={};
LabSetupDokter.func={};
LabSetupDokter.vars={};
LabSetupDokter.func.parent=LabSetupDokter;
LabSetupDokter.form.ArrayStore={};
LabSetupDokter.form.ComboBox={};
LabSetupDokter.form.DataStore={};
LabSetupDokter.form.Record={};
LabSetupDokter.form.Form={};
LabSetupDokter.form.Grid={};
LabSetupDokter.form.Panel={};
LabSetupDokter.form.TextField={};
LabSetupDokter.form.Button={};

LabSetupDokter.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_unit_far', 'nm_unit_far', 'nomor_ro_unit', 'nomorawal','nomor_out_milik'],
	data: []
});

CurrentPage.page = dataGrid_viLabSetupDokter(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dataGrid_viLabSetupDokter(mod_id_viLabSetupDokter){	
 	// Field kiriman dari Project Net.
    var FieldMaster_viLabSetupDokter = 
	[
		'kd_unit_far', 'nm_unit_far', 'nomor_ro_unit', 'nomorawal','nomor_out_milik' 
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viLabSetupDokter = new WebApp.DataStore
	({
        fields: FieldMaster_viLabSetupDokter
    });
    dataGriLabSetupDokter();
    // Grid gizi Perencanaan # --------------
	GridDataView_viLabSetupDokter = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viLabSetupDokter,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:true,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							/*kodesetupDokter=rowSelected_viLabSetupDokter.data.kd_Dokter
							alert(kodesetupDokter);*/
							rowSelected_viLabSetupDokter = undefined;
							rowSelected_viLabSetupDokter = dataSource_viLabSetupDokter.getAt(row);
							CurrentData_viLabSetupDokter
							CurrentData_viLabSetupDokter.row = row;
							CurrentData_viLabSetupDokter.data = rowSelected_viLabSetupDokter.data;
							kodesetupDokter=rowSelected_viLabSetupDokter.data.kd_dokter;
							//DataInitLabSetupDokter(rowSelected_viLabSetupDokter.data);
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					kodesetupDokter=rowSelected_viLabSetupDokter.data.kd_Dokter;
					rowSelected_viLabSetupDokter = dataSource_viLabSetupDokter.getAt(ridx);
					if (rowSelected_viLabSetupDokter != undefined)
					{
						DataInitLabSetupDokter(rowSelected_viLabSetupDokter.data);
						//setLookUp_viLabSetupDokter(rowSelected_viLabSetupDokter.data);
					}
					else
					{
						//setLookUp_viLabSetupDokter();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Setup Unit
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header: 'Kode Dokter',
						dataIndex: 'kd_dokter',
						hideable:false,
						menuDisabled: true,
						width: 40
						
					},
					//-------------- ## --------------
					{
						header: 'Nama Dokter',
						dataIndex: 'nama',
						hideable:false,
						menuDisabled: true,
						width: 90
					},
					//-------------- ## --------------
					{
						header: 'Jenis Dokter',
						dataIndex: 'jenis_dokter',
						hideable:false,
						menuDisabled: true,
						width: 90
					},
					//-------------- ## --------------
					{
						header: 'spesialisasi',
						dataIndex: 'spesialisasi',
						hideable:false,
						menuDisabled: true,
						width: 90
					}
				]
			),
			/* tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viLabSetupDokter',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Dokter Lab',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Dokter Lab',
						id: 'btnEdit_viLabSetupDokter',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viLabSetupDokter != undefined)
							{
								DataInitLabSetupDokter(rowSelected_viLabSetupDokter.data);
							}
							
						}
					}
					//-------------- ## --------------
				]
			}, */
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viLabSetupDokter, selectCount_viLabSetupDokter, dataSource_viLabSetupDokter),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)	
	var PanelTabLabSetupDokter = new Ext.FormPanel({
        labelAlign: 'top',
		layout:'form',
		border: false,  
		autoHeight:true,
        title: '',
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [PanelInputUnit()]
		
	})
	// Kriteria filter pada Grid # --------------
    var FrmData_viLabSetupDokter = new Ext.Panel
    (
		{
			title: NamaForm_viLabSetupDokter,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viLabSetupDokter,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ PanelTabLabSetupDokter,
					GridDataView_viLabSetupDokter],
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAddSetupDokter_viLabSetupDokter',
						handler: function(){
							AddNewLabSetupDokter();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viLabSetupDokter',
						handler: function()
						{
							loadMask.show();
							dataSave_viLabSetupDokter();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viLabSetupDokter',
						handler: function()
						{
							if (kodesetupDokter==='')
							{
								ShowPesanErrorLabSetupDokter('Tidak ada data yang dipilih', 'Error');
							}
							else
							{
								Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
									if (button == 'yes'){
										loadMask.show();
										dataDelete_viLabSetupDokter();
									}
								});
							}
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_viLabSetupDokter',
						handler: function()
						{
							dataSource_viLabSetupDokter.removeAll();
							dataGriLabSetupDokter();
							kodesetupDokter='';
						}
					},
					{
						xtype: 'tbseparator'
					}
					
				]
			}
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_viLabSetupDokter;
    //-------------- # End form filter # --------------
}

function PanelInputUnit(rowdata){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 120,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'Kode Dokter'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 0,
								xtype: 'textfield',
								id: 'txtKdSetupDokter_LabSetupDokter',
								name: 'txtKdSetupDokter_LabSetupDokter',
								width: 100,
								allowBlank: false,
								maxLength:3,
								readOnly:true,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									},
									blur: function(a){
										if(a.getValue().length > 3){
											ShowPesanWarningLabSetupDokter('Kode unit tidak boleh lebih dari 3 huruf', 'Warning');
										}
									}
								}
							},
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'Nama Dokter'
							},
							{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							LabSetupDokter.vars.nama=new Nci.form.Combobox.autoComplete({
								x: 130,
								y: 30,
								tabIndex:2,
								store	: LabSetupDokter.form.ArrayStore.a,
								select	: function(a,b,c){
									
									Ext.getCmp('txtKdSetupDokter_LabSetupDokter').setValue(b.data.kd_dokter);
									
									GridDataView_viLabSetupDokter.getView().refresh();
									
								},
								/* onShowList:function(a){
									dataSource_viLabSetupDokter.removeAll();
									
									var recs=[],
									recType=dataSource_viLabSetupDokter.recordType;
										
									for(var i=0; i<a.length; i++){
										recs.push(new recType(a[i]));
									}
									dataSource_viLabSetupDokter.add(recs);
									
								}, */
								insert	: function(o){
									return {
										kd_dokter   : o.kd_dokter,
										nama 		: o.nama,
										text			:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_dokter+'</td><td width="200">'+o.nama+'</td></tr></table>'
									}
								},
								url		: baseURL + "index.php/main/functionLAB/getDokter",
								valueField: 'nama',
								displayField: 'text',
								listWidth: 280
							}),
							{
								x: 10,
								y: 60,
								xtype: 'label',
								text: 'Spesialisasi'
							},
							{
								x: 120,
								y: 60,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 60,
								xtype: 'textfield',
								id: 'txtSpesialisasi_LabSetupDokter',
								name: 'txtSpesialisasi_LabSetupDokter',
								width: 150,
								tabIndex:3
							},{
								x: 10,
								y: 90,
								xtype: 'label',
								text: 'Unit Lab'
							},
							{
								x: 120,
								y: 90,
								xtype: 'label',
								text: ':'
							},mComboUnitLab()
							
							
							
						]
					}
				]
			}
		]		
	};
        return items;
}

function mComboUnitLab(){ 
    var Field = ['KD_UNIT','NAMA_UNIT'];
    dsunitlab_viSetupDokterLab= new WebApp.DataStore({ fields: Field });
    dsunitlab_viSetupDokterLab.load({
		params:{
			Skip: 0,
			Take: 1000,
			Sort: 'nama_unit',
			Sortdir: 'ASC',
			target: 'ViewSetupUnit',
			param: "parent = '4' and kd_unit not in('44','45')"
		}
	});
    var cboUnitLab_viSetupDokterLab = new Ext.form.ComboBox({
		id: 'cboUnitLab_viSetupDokterLab',
		x: 130,
		y: 90,
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		emptyText: '',
		fieldLabel:  ' ',
		align: 'Right',
		width: 230,
		emptyText:'Pilih Unit',
		store: dsunitlab_viSetupDokterLab,
		valueField: 'KD_UNIT',
		displayField: 'NAMA_UNIT',
		//value:'All',
		editable: false,
			listeners:
			{
				'select': function(a,b,c)
				{
					tmpkd_unit = b.data.KD_UNIT;
				}
			}
	});
    return cboUnitLab_viSetupDokterLab;
}
//------------------end---------------------------------------------------------

function dataGriLabSetupDokter(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/main/functionLAB/getDokterPenunjang",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorLabSetupDokter('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					tmpkd_unit=cst.kd_unit;
					Ext.getCmp('cboUnitLab_viSetupDokterLab').setValue(cst.nama_unit);
					var recs=[],
						recType=dataSource_viLabSetupDokter.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dataSource_viLabSetupDokter.add(recs);
					
					
					
					GridDataView_viLabSetupDokter.getView().refresh();
				}
				else 
				{
					ShowPesanErrorLabSetupDokter('Gagal membaca data Dokter', 'Error');
				};
			}
		}
		
	)
	
}

function dataSave_viLabSetupDokter(){
	if (ValidasiSaveLabSetupDokter(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/main/functionLAB/simpanSetupDokter",
				params: getParamSaveLabSetupDokter(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorLabSetupDokter('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoLabSetupDokter('Berhasil menyimpan data ini','Information');
						dataSource_viLabSetupDokter.removeAll();
						dataGriLabSetupDokter();
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorLabSetupDokter('Gagal menyimpan data ini', 'Error');
						dataSource_viLabSetupDokter.removeAll();
						dataGriLabSetupDokter();
					};
				}
			}
			
		)
	}
}

function dataDelete_viLabSetupDokter(){
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/main/functionLAB/hapusSetupDokter",
				params: getParamDeleteLabSetupDokter(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorLabSetupDokter('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoLabSetupDokter('Berhasil menghapus data ini','Information');
						AddNewLabSetupDokter()
						dataSource_viLabSetupDokter.removeAll();
						dataGriLabSetupDokter();
						kodesetupDokter='';
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorLabSetupDokter('Gagal menghapus data ini', 'Error');
						dataSource_viLabSetupDokter.removeAll();
						dataGriLabSetupDokter();
					};
				}
			}
			
		)
	
}

function AddNewLabSetupDokter(){
	Ext.getCmp('txtKdSetupDokter_LabSetupDokter').setValue('');
	LabSetupDokter.vars.nama.setValue('');
	LabSetupDokter.vars.nama.focus();
	Ext.getCmp('txtSpesialisasi_LabSetupDokter').setValue('');
};

function DataInitLabSetupDokter(rowdata){
	Ext.getCmp('txtKdSetupDokter_LabSetupDokter').setValue(rowdata.kd_Dokter);
	LabSetupDokter.vars.nama.setValue(rowdata.Dokter);
	Ext.getCmp('txtSpesialisasi_LabSetupDokter').setValue(rowdata.alamat);
};

function getParamSaveLabSetupDokter(){
	var	params =
	{
		KdSetupDokter:Ext.getCmp('txtKdSetupDokter_LabSetupDokter').getValue(),
		NamaSetupDokter:LabSetupDokter.vars.nama.getValue(),
		Spesialisasi:Ext.getCmp('txtSpesialisasi_LabSetupDokter').getValue(),
		kd_unit:tmpkd_unit
	}
   
    return params
};

function getParamDeleteLabSetupDokter(){
	var	params =
	{
		KdSetupDokter:kodesetupDokter,
		kd_unit:tmpkd_unit
	}
   
    return params
};

function ValidasiSaveLabSetupDokter(modul,mBolHapus){
	var x = 1;
	if(LabSetupDokter.vars.nama.getValue() === '' /*|| Ext.getCmp('txtKdSetupDokter_LabSetupDokter').getValue() ===''*/){
		if(LabSetupDokter.vars.nama.getValue() === ''){
			loadMask.hide();
			ShowPesanWarningLabSetupDokter('Nama unit masih kosong', 'Warning');
			x = 0;
		} else{
			loadMask.hide();
			ShowPesanWarningLabSetupDokter('Kode unit masih kosong', 'Warning');
			x = 0;
		}
		
	} 
	if(Ext.getCmp('txtKdSetupDokter_LabSetupDokter').getValue().length > 3){
		ShowPesanWarningLabSetupDokter('Kode unit tidak boleh lebih dari 3 huruf', 'Warning');
		x = 0;
	}
	return x;
};



function ShowPesanWarningLabSetupDokter(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorLabSetupDokter(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoLabSetupDokter(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};