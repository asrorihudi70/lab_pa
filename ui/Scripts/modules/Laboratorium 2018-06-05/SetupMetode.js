var dataSource_viSetupMetode;
var selectCount_viSetupMetode=50;
var NamaForm_viSetupMetode="Setup Metode";
var mod_name_viSetupMetode="Setup Metode";
var now_viSetupMetode= new Date();
var rowSelected_viSetupMetode;
var setLookUps_viSetupMetode;
var tanggal = now_viSetupMetode.format("d/M/Y");
var jam = now_viSetupMetode.format("H/i/s");
var tmpkriteria;
var DataGridJenisObat;
var GridDataView_viSetupMetode;


var CurrentData_viSetupMetode =
{
	data: Object,
	details: Array,
	row: 0
};

var SetupMetode={};
SetupMetode.form={};
SetupMetode.func={};
SetupMetode.vars={};
SetupMetode.func.parent=SetupMetode;
SetupMetode.form.ArrayStore={};
SetupMetode.form.ComboBox={};
SetupMetode.form.DataStore={};
SetupMetode.form.Record={};
SetupMetode.form.Form={};
SetupMetode.form.Grid={};
SetupMetode.form.Panel={};
SetupMetode.form.TextField={};
SetupMetode.form.Button={};

SetupMetode.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_metode', 'metode'],
	data: []
});

CurrentPage.page = dataGrid_viSetupMetode(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dataGrid_viSetupMetode(mod_id_viSetupMetode){	
 	// Field kiriman dari Project Net.
    var FieldMaster_viSetupMetode = 
	[
		'kd_metode', 'metode' 
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viSetupMetode = new WebApp.DataStore
	({
        fields: FieldMaster_viSetupMetode
    });
    dataGriSetupMetode();
    // Grid lab Perencanaan # --------------
	GridDataView_viSetupMetode = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viSetupMetode,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:true,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viSetupMetode = undefined;
							rowSelected_viSetupMetode = dataSource_viSetupMetode.getAt(row);
							CurrentData_viSetupMetode
							CurrentData_viSetupMetode.row = row;
							CurrentData_viSetupMetode.data = rowSelected_viSetupMetode.data;
							//DataInitSetupMetode(rowSelected_viSetupMetode.data);
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viSetupMetode = dataSource_viSetupMetode.getAt(ridx);
					if (rowSelected_viSetupMetode != undefined)
					{
						DataInitSetupMetode(rowSelected_viSetupMetode.data);
						//setLookUp_viSetupMetode(rowSelected_viSetupMetode.data);
					}
					else
					{
						//setLookUp_viSetupMetode();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Setup Unit
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header: 'Kode Metode',
						dataIndex: 'kd_metode',
						hideable:false,
						menuDisabled: true,
						width: 40
						
					},
					//-------------- ## --------------
					{
						header: 'Metode',
						dataIndex: 'metode',
						hideable:false,
						menuDisabled: true,
						width: 90
					}
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viSetupMetode',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Metode',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viSetupMetode',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viSetupMetode != undefined)
							{
								DataInitSetupMetode(rowSelected_viSetupMetode.data);
							}
							
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viSetupMetode, selectCount_viSetupMetode, dataSource_viSetupMetode),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)	
	var PanelTabSetupMetode = new Ext.FormPanel({
        labelAlign: 'top',
		layout:'form',
		border: false,  
		autoHeight:true,
        title: '',
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [PanelInputUnit()]
		
	})
	// Kriteria filter pada Grid # --------------
    var FrmData_viSetupMetode = new Ext.Panel
    (
		{
			title: NamaForm_viSetupMetode,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viSetupMetode,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ PanelTabSetupMetode,
					GridDataView_viSetupMetode],
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAddJenis_viSetupMetode',
						handler: function(){
							AddNewSetupMetode();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viSetupMetode',
						handler: function()
						{
							loadMask.show();
							dataSave_viSetupMetode();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viSetupMetode',
						handler: function()
						{
							Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
								if (button == 'yes'){
									loadMask.show();
									dataDelete_viSetupMetode();
								}
							});
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_viSetupPabrik',
						handler: function()
						{
							dataSource_viSetupMetode.removeAll();
							dataGriSetupMetode();
						}
					},
					{
						xtype: 'tbseparator'
					}
					
				]
			}
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_viSetupMetode;
    //-------------- # End form filter # --------------
}

function PanelInputUnit(rowdata){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 55,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'Kode metode'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 0,
								xtype: 'textfield',
								id: 'txtKdMetode_SetupMetode',
								name: 'txtKdMetode_SetupMetode',
								width: 100,
								allowBlank: false,
								readOnly: true,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									},
									
								}
							},
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'Metode'
							},
							{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							SetupMetode.vars.metode=new Nci.form.Combobox.autoComplete({
								x: 130,
								y: 30,
								tabIndex:2,
								id		: 'txtComboMetode_SetupMetode',
								store	: SetupMetode.form.ArrayStore.a,
								select	: function(a,b,c){
									
									Ext.getCmp('txtKdMetode_SetupMetode').setValue(b.data.kd_metode);
									
									GridDataView_viSetupMetode.getView().refresh();
									
								},
								onShowList:function(a){
									dataSource_viSetupMetode.removeAll();
									
									var recs=[],
									recType=dataSource_viSetupMetode.recordType;
										
									for(var i=0; i<a.length; i++){
										recs.push(new recType(a[i]));
									}
									dataSource_viSetupMetode.add(recs);
									
								},
								insert	: function(o){
									return {
										kd_metode      	: o.kd_metode,
										metode 			: o.metode,
										text			:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_metode+'</td><td width="200">'+o.metode+'</td></tr></table>'
									}
								},
								url		: baseURL + "index.php/lab/functionSetupMetode/getItemGrid",
								valueField: 'metode',
								displayField: 'text',
								listWidth: 280
							})
							
							
						]
					}
				]
			}
		]		
	};
        return items;
}
//------------------end---------------------------------------------------------

function dataGriSetupMetode(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/lab/functionSetupMetode/getItemGrid",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorSetupMetode('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dataSource_viSetupMetode.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dataSource_viSetupMetode.add(recs);
					
					
					
					GridDataView_viSetupMetode.getView().refresh();
				}
				else 
				{
					ShowPesanErrorSetupMetode('Gagal membaca data items', 'Error');
				};
			}
		}
		
	)
	
}

function dataSave_viSetupMetode(){
	if (ValidasiSaveSetupMetode(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/lab/functionSetupMetode/save",
				params: getParamSaveSetupMetode(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupMetode('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupMetode('Berhasil menyimpan data ini','Information');
						Ext.getCmp('txtKdMetode_SetupMetode').setValue(cst.kode);
						dataSource_viSetupMetode.removeAll();
						dataGriSetupMetode();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupMetode('Gagal menyimpan data ini', 'Error');
						dataSource_viSetupMetode.removeAll();
						dataGriSetupMetode();
					};
				}
			}
			
		)
	}
}

function dataDelete_viSetupMetode(){
	if (ValidasiSaveSetupMetode(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/lab/functionSetupMetode/delete",
				params: getParamDeleteSetupMetode(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupMetode('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupMetode('Berhasil menghapus data ini','Information');
						AddNewSetupMetode()
						dataSource_viSetupMetode.removeAll();
						dataGriSetupMetode();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupMetode('Gagal menghapus data ini', 'Error');
						dataSource_viSetupMetode.removeAll();
						dataGriSetupMetode();
					};
				}
			}
			
		)
	}
}

function AddNewSetupMetode(){
	Ext.getCmp('txtKdMetode_SetupMetode').setValue('');
	SetupMetode.vars.metode.setValue('');
};

function DataInitSetupMetode(rowdata){
	Ext.getCmp('txtKdMetode_SetupMetode').setValue(rowdata.kd_metode);
	SetupMetode.vars.metode.setValue(rowdata.metode);
};

function getParamSaveSetupMetode(){
	var	params =
	{
		KdMetode:Ext.getCmp('txtKdMetode_SetupMetode').getValue(),
		Metode:SetupMetode.vars.metode.getValue()
	}
   
    return params
};

function getParamDeleteSetupMetode(){
	var	params =
	{
		KdMetode:Ext.getCmp('txtKdMetode_SetupMetode').getValue()
	}
   
    return params
};

function ValidasiSaveSetupMetode(modul,mBolHapus){
	var x = 1;
	if(SetupMetode.vars.metode.getValue() === ''){
		if(SetupMetode.vars.metode.getValue() === ''){
			loadMask.hide();
			ShowPesanWarningSetupMetode('Metode masih kosong', 'Warning');
			x = 0;
		}
		
	} 
	return x;
};



function ShowPesanWarningSetupMetode(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorSetupMetode(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoSetupMetode(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};