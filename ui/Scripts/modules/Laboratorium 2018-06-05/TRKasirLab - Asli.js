
var CurrentHistory =
        {
            data: Object,
            details: Array,
            row: 0
        };
var FormDepan2KasirLAB;
var CurrentPenataJasaKasirLAB =
        {
            data: Object,
            details: Array,
            row: 0
        };
var tmp_kodeunitkamar_LAB;
var tmp_kdspesial_LAB;
var tmp_nokamar_LAB;
var msg_box_alasanhapus_LAB;
var tampungshiftsekarang;
var tampungtypedata;
var tanggaltransaksitampung;
var kdpaytransfer = 'T1';
var mRecordKasirLAB = Ext.data.Record.create
        (
                [
                    {name: 'KD_PRODUK', mapping: 'KD_PRODUK'},
                    {name: 'DESKRIPSI2', mapping: 'DESKRIPSI2'},
                    {name: 'DESKRIPSI', mapping: 'DESKRIPSI'},
                    {name: 'KD_TARIF', mapping: 'KD_TARIF'},
                    {name: 'HARGA', mapping: 'HARGA'},
                    {name: 'QTY', mapping: 'QTY'},
                    {name: 'TGL_TRANSAKSI', mapping: 'TGL_TRANSAKSI'},
                    {name: 'DESC_REQ', mapping: 'DESC_REQ'},
                    {name: 'URUT', mapping: 'URUT'}
                ]
                );
var selectindukunitLABKasir = 'Radiologi';
var cellSelecteddeskripsi;
var cellSelected2deskripsi;
var Kdtransaksi;
var tapungkd_pay;
var dsTRDetailHistoryList;
var AddNewHistory = true;
var selectCountHistory = 50;
var now = new Date();
var rowSelectedHistory;

var FormLookUpsdetailTRHistory;
var now = new Date();
var cellSelectedtutup;
var vkd_unit;
var kdcustomeraa;
var nowTglTransaksi = new Date();
var FormLookUpsdetailTRTransfer_Lab;
var labelisi;
var jenispay;
var variablehistori;
var selectCountStatusByr_viKasirLABKasir = 'Belum Lunas';
var tranfertujuan = 'Rawat Inap';
var dsTRKasirLABKasirList;
var dsTRDetailKasirLABKasirList;
var AddNewKasirLABKasir = true;
var selectCountKasirLABKasir = 50;
var now = new Date();
var rowSelectedKasirLABKasir;

var FormLookUpsdetailTRKasirLAB;
var valueStatusCMKasirLABView = 'All';
var nowTglTransaksi = new Date();
var dsComboBayar;
var vkode_customer;
var vflag;
var gridDTLTRKasirLAB;
var notransaksi;
var tgltrans;
var kodepasien;
var namapasien;
var kodeunit;
var namaunit;
var kodepay;
var kdkasir;
var uraianpay;
CurrentPage.page = getPanelKasirLAB(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function getPanelKasirLAB(mod_id)
{

    var Field = ['KD_DOKTER', 'NO_TRANSAKSI', 'KD_UNIT', 'KD_PASIEN', 'NAMA', 'NAMA_UNIT', 'ALAMAT',
        'TANGGAL_TRANSAKSI', 'NAMA_DOKTER', 'KD_CUSTOMER', 'CUSTOMER', 'URUT_MASUK', 'LUNAS',
        'KET_PAYMENT', 'CARA_BAYAR', 'JENIS_PAY', 'KD_PAY', 'POSTING', 'TYPE_DATA', 'CO_STATUS', 'KD_KASIR',
        'NAMAUNITASAL', 'NO_TRANSAKSI_ASAL', 'KD_KASIR_ASAL', 'FLAG'];
    dsTRKasirLABKasirList = new WebApp.DataStore({fields: Field});
    refeshKasirLABKasir();
    var grListTRKasirLAB = new Ext.grid.EditorGridPanel
            (
                    {
                        stripeRows: true,
                        store: dsTRKasirLABKasirList,
                        columnLines: false,
                        autoScroll: true,
                        anchor: '100% 45%',
                        //height:325,
                        border: false,
                        sort: false,
                        sm: new Ext.grid.RowSelectionModel
                                (
                                        {
                                            singleSelect: true,
                                            listeners:
                                                    {
                                                        rowselect: function (sm, row, rec)
                                                        {
                                                            rowSelectedKasirLABKasir = dsTRKasirLABKasirList.getAt(row);


                                                        }
                                                    }
                                        }
                                ),
                        listeners:
                                {
                                    rowdblclick: function (sm, ridx, cidx)
                                    {

                                        rowSelectedKasirLABKasir = dsTRKasirLABKasirList.getAt(ridx);

                                        if (rowSelectedKasirLABKasir != undefined)
                                        {
                                            if (rowSelectedKasirLABKasir.data.LUNAS == 'f')
                                            {
                                                KasirLookUpLAB(rowSelectedKasirLABKasir.data);
                                            } else
                                            {
                                                ShowPesanWarningKasirLAB('Pembayaran telah balance', 'Pembayaran');
                                            }
                                        } else {
                                            ShowPesanWarningKasirLAB('Silahkan Pilih data   ', 'Pembayaran');
                                        }

                                    },
                                    rowclick: function (sm, ridx, cidx)
                                    {
                                        cellSelectedtutup = rowSelectedKasirLABKasir.data.NO_TRANSAKSI;

                                        if (rowSelectedKasirLABKasir.data.LUNAS == 't' && rowSelectedKasirLABKasir.data.CO_STATUS == 't')
                                        {

                                            Ext.getCmp('btnEditKasirLAB').disable();
                                            Ext.getCmp('btnTutupTransaksiKasirLAB').disable();
                                            Ext.getCmp('btnHpsBrsKasirLAB').disable();
                                            Ext.getCmp('btnLookupKasirLAB').disable();
                                            Ext.getCmp('btnSimpanLAB').disable();
                                            Ext.getCmp('btnHpsBrsLAB').disable();
                                            //Ext.getCmp('btnTransferKasirLAB').disable();

                                        } else if (rowSelectedKasirLABKasir.data.LUNAS == 'f' && rowSelectedKasirLABKasir.data.CO_STATUS == 'f')
                                        {
                                            Ext.getCmp('btnTutupTransaksiKasirLAB').disable();
                                            Ext.getCmp('btnHpsBrsKasirLAB').enable();
                                            Ext.getCmp('btnEditKasirLAB').enable();
                                            Ext.getCmp('btnLookupKasirLAB').enable();
                                            Ext.getCmp('btnSimpanLAB').enable();
                                            Ext.getCmp('btnHpsBrsLAB').enable();
                                            //Ext.getCmp('btnTransferKasirLAB').enable();

                                        } else if (rowSelectedKasirLABKasir.data.LUNAS == 't' && rowSelectedKasirLABKasir.data.CO_STATUS == 'f')
                                        {
                                            Ext.getCmp('btnEditKasirLAB').disable();
                                            Ext.getCmp('btnTutupTransaksiKasirLAB').enable();
                                            Ext.getCmp('btnHpsBrsKasirLAB').enable();
                                            Ext.getCmp('btnLookupKasirLAB').disable();
                                            Ext.getCmp('btnSimpanLAB').disable();
                                            Ext.getCmp('btnHpsBrsLAB').disable();
                                            //Ext.getCmp('btnTransferKasirLAB').disable();

                                        }

                                        kdkasirasal = rowSelectedKasirLABKasir.data.KD_KASIR_ASAL;
                                        notransaksiasal = rowSelectedKasirLABKasir.data.NO_TRANSAKSI_ASAL;
                                        notransaksi = rowSelectedKasirLABKasir.data.NO_TRANSAKSI;
                                        tgltrans = rowSelectedKasirLABKasir.data.TANGGAL_TRANSAKSI;
                                        kodepasien = rowSelectedKasirLABKasir.data.KD_PASIEN;
                                        namapasien = rowSelectedKasirLABKasir.data.NAMA;
                                        kodeunit = rowSelectedKasirLABKasir.data.KD_UNIT;
                                        namaunit = rowSelectedKasirLABKasir.data.NAMA_UNIT;
                                        kodepay = rowSelectedKasirLABKasir.data.KD_PAY;
                                        kdkasir = rowSelectedKasirLABKasir.data.KD_KASIR;
                                        uraianpay = rowSelectedKasirLABKasir.data.CARA_BAYAR;
                                        kdcustomeraa = rowSelectedKasirLABKasir.data.KD_CUSTOMER;

                                        Ext.Ajax.request({
                                            url: baseURL + "index.php/main/getcurrentshift",
                                            params: {
                                                //UserID: 'Admin',
                                                command: '0',
                                                // parameter untuk url yang dituju (fungsi didalam controller)
                                            },
                                            failure: function (o)
                                            {
                                                var cst = Ext.decode(o.responseText);
                                            },
                                            success: function (o) {
                                                tampungshiftsekarang = o.responseText
                                            }
                                        });

                                        RefreshDatahistoribayar(rowSelectedKasirLABKasir.data.NO_TRANSAKSI);
                                        RefreshDataKasirLABDetail(rowSelectedKasirLABKasir.data.NO_TRANSAKSI);

                                    }


                                },
                        cm: new Ext.grid.ColumnModel
                                (
                                        [
                                            //CUSTOMER
                                            {
                                                id: 'colLUNAScoba',
                                                header: 'Status Lunas',
                                                dataIndex: 'LUNAS',
                                                sortable: true,
                                                width: 90,
                                                align: 'center',
                                                renderer: function (value, metaData, record, rowIndex, colIndex, store)
                                                {
                                                    switch (value)
                                                    {
                                                        case 't':
                                                            metaData.css = 'StatusHijau'; // 
                                                            break;
                                                        case 'f':
                                                            metaData.css = 'StatusMerah'; // rejected

                                                            break;
                                                    }
                                                    return '';
                                                }
                                            },
                                            {
                                                id: 'coltutuptr',
                                                header: 'Tutup transaksi',
                                                dataIndex: 'CO_STATUS',
                                                sortable: true,
                                                width: 90,
                                                align: 'center',
                                                renderer: function (value, metaData, record, rowIndex, colIndex, store)
                                                {
                                                    switch (value)
                                                    {
                                                        case 't':
                                                            metaData.css = 'StatusHijau'; // 
                                                            break;
                                                        case 'f':
                                                            metaData.css = 'StatusMerah'; // rejected

                                                            break;
                                                    }
                                                    return '';
                                                }
                                            },
                                            {
                                                id: 'colReqIdViewKasirLAB',
                                                header: 'No. Transaksi',
                                                dataIndex: 'NO_TRANSAKSI',
                                                sortable: false,
                                                hideable: false,
                                                menuDisabled: true,
                                                width: 80
                                            },
                                            {
                                                id: 'colTglKasirLABViewKasirLAB',
                                                header: 'Tgl Transaksi',
                                                dataIndex: 'TANGGAL_TRANSAKSI',
                                                sortable: false,
                                                hideable: false,
                                                menuDisabled: true,
                                                width: 75,
                                                renderer: function (v, params, record)
                                                {
                                                    return ShowDate(record.data.TANGGAL_TRANSAKSI);
                                                }
                                            },
                                            {
                                                header: 'No. Medrec',
                                                width: 65,
                                                sortable: false,
                                                hideable: false,
                                                menuDisabled: true,
                                                dataIndex: 'KD_PASIEN',
                                                id: 'colKasirLABerViewKasirLAB'
                                            },
                                            {
                                                header: 'Pasien',
                                                width: 190,
                                                sortable: false,
                                                hideable: false,
                                                menuDisabled: true,
                                                dataIndex: 'NAMA',
                                                id: 'colKasirLABerViewKasirLAB'
                                            },
                                            {
                                                id: 'colLocationViewKasirLAB',
                                                header: 'Alamat',
                                                dataIndex: 'ALAMAT',
                                                sortable: false,
                                                hideable: false,
                                                menuDisabled: true,
                                                width: 170
                                            },
                                            {
                                                id: 'colDeptViewKasirLAB',
                                                header: 'Dokter',
                                                dataIndex: 'NAMA_DOKTER',
                                                sortable: false,
                                                hideable: false,
                                                menuDisabled: true,
                                                width: 150
                                            },
                                            {
                                                id: 'colunitViewKasirLAB',
                                                header: 'Unit',
                                                dataIndex: 'NAMA_UNIT',
                                                sortable: false,
                                                hideable: false,
                                                menuDisabled: true,
                                                hidden: true,
                                                width: 90
                                            },
                                            {
                                                id: 'colasaluViewKasirLAB',
                                                header: 'Unit asal',
                                                dataIndex: 'NAMAUNITASAL',
                                                sortable: false,
                                                hideable: false,
                                                menuDisabled: true,
                                                width: 90
                                            },
                                            {
                                                id: 'colcustomerViewKasirLAB',
                                                header: 'Kel. Pasien',
                                                dataIndex: 'CUSTOMER',
                                                sortable: false,
                                                hideable: false,
                                                menuDisabled: true,
                                                width: 90
                                            }


                                        ]
                                        ),
                        viewConfig: {forceFit: true},
                        tbar:
                                [
                                    {
                                        id: 'btnEditKasirLAB',
                                        text: 'Pembayaran',
                                        tooltip: nmEditData,
                                        iconCls: 'Edit_Tr',
                                        handler: function (sm, row, rec)
                                        {
                                            if (rowSelectedKasirLABKasir != undefined) {
                                                KasirLookUpLAB(rowSelectedKasirLABKasir.data);
                                            } else {
                                                ShowPesanWarningKasirLAB('Pilih data tabel  ', 'Pembayaran');
                                            }
                                        }, disabled: true
                                    }, ' ', ' ', '' + ' ', '' + ' ', '' + ' ', '' + ' ', ' ', '' + '-',
                                    {
                                        text: 'Tutup Transaksi',
                                        id: 'btnTutupTransaksiKasirLAB',
                                        tooltip: nmHapus,
                                        iconCls: 'remove',
                                        handler: function ()
                                        {
                                            if (cellSelectedtutup == '' || cellSelectedtutup == 'undefined') {
                                                ShowPesanWarningKasirLAB('Pilih data tabel  ', 'Pembayaran');
                                            } else {
                                                UpdateTutuptransaksi(false);
                                                Ext.getCmp('btnEditKasirLAB').disable();
                                                Ext.getCmp('btnTutupTransaksiKasirLAB').disable();
                                                Ext.getCmp('btnHpsBrsKasirLAB').disable();
                                            }
                                        }, disabled: true
                                    }
                                ]
                    }
            );

    var LegendViewCMRequest = new Ext.Panel
            (
                    {
                        id: 'LegendViewCMRequest',
                        region: 'center',
                        border: false,
                        bodyStyle: 'padding:0px 7px 0px 7px',
                        layout: 'column',
                        frame: true,
                        //height:32,
                        anchor: '100% 8.0001%',
                        autoScroll: false,
                        items:
                                [
                                    {
                                        columnWidth: .033,
                                        layout: 'form',
                                        style: {'margin-top': '-1px'},
                                        //height:32,
                                        anchor: '100% 8.0001%',
                                        border: false,
                                        html: '<img src="' + baseURL + 'ui/images/icons/16x16/hijau.png" class="text-desc-legend"/>'
                                    },
                                    {
                                        columnWidth: .08,
                                        layout: 'form',
                                        //height:32,
                                        anchor: '100% 8.0001%',
                                        style: {'margin-top': '1px'},
                                        border: false,
                                        html: " Lunas"
                                    },
                                    {
                                        columnWidth: .033,
                                        layout: 'form',
                                        style: {'margin-top': '-1px'},
                                        border: false,
                                        //height:35,
                                        anchor: '100% 8.0001%',
                                        //html: '<img src="./images/icons/16x16/merah.png" class="text-desc-legend"/>'
                                        html: '<img src="' + baseURL + 'ui/images/icons/16x16/merah.png" class="text-desc-legend"/>'
                                    },
                                    {
                                        columnWidth: .1,
                                        layout: 'form',
                                        //height:32,
                                        anchor: '100% 8.0001%',
                                        style: {'margin-top': '1px'},
                                        border: false,
                                        html: " Belum Lunas"
                                    }
                                ]

                    }
            )
    var GDtabDetailLAB = new Ext.TabPanel
            (
                    {
                        id: 'GDtabDetailLAB',
                        region: 'center',
                        activeTab: 0,
                        anchor: '100% 40%',
                        border: false,
                        plain: true,
                        defaults:
                                {
                                    autoScroll: true
                                },
                        items: [
                            GetDTLTRHistoryGrid(), GetDTLTRLABGrid()
                                    //-------------- ## --------------
                        ],
                        listeners:
                                {
                                }
                    }

            );

    var FormDepanKasirLAB = new Ext.Panel
            (
                    {
                        id: mod_id,
                        closable: true,
                        region: 'center',
                        layout: 'form',
                        title: 'Kasir Laboratorium',
                        border: false,
                        shadhow: true,
                        iconCls: 'Request',
                        margins: '0 5 5 0',
                        items: [
                            {
                                xtype: 'panel',
                                layout: 'column',
                                plain: true,
                                activeTab: 0,
                                height: 160,
                                defaults:
                                        {
                                            bodyStyle: 'padding:10px',
                                            autoScroll: true
                                        },
                                items:
                                        [
                                            {
                                                columnWidth: .99,
                                                height: 150,
                                                layout: 'form',
                                                bodyStyle: 'padding:6px 6px 3px 3px',
                                                border: false,
                                                anchor: '100% 100%',
                                                items:
                                                        [
                                                            mComboUnitInduk(),
                                                            {
                                                                xtype: 'textfield',
                                                                fieldLabel: ' No. Medrec' + ' ',
                                                                id: 'txtFilterKasirNomedrecLab',
                                                                anchor: '35%',
                                                                onInit: function () { },
                                                                listeners:
                                                                        {
                                                                            'specialkey': function ()
                                                                            {
                                                                                var tmpNoMedrec = Ext.get('txtFilterKasirNomedrecLab').getValue()
                                                                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10)
                                                                                {
                                                                                    if (tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10) {
                                                                                        var tmpgetNoMedrec = formatnomedrec(Ext.get('txtFilterKasirNomedrecLab').getValue())
                                                                                        Ext.getCmp('txtFilterKasirNomedrecLab').setValue(tmpgetNoMedrec);
                                                                                        var tmpkriteria = getCriteriaFilter_viDaftar();
                                                                                        RefreshDataFilterKasirLABKasir();
                                                                                    } else {
                                                                                        if (tmpNoMedrec.length === 10) {
                                                                                            RefreshDataFilterKasirLABKasir();
                                                                                        } else
                                                                                            Ext.getCmp('txtFilterKasirNomedrecLab').setValue('')
                                                                                    }
                                                                                }
                                                                            }

                                                                        }
                                                            },
                                                            {
                                                                xtype: 'tbspacer',
                                                                height: 3
                                                            },
                                                            {
                                                                xtype: 'textfield',
                                                                fieldLabel: ' Pasien' + ' ',
                                                                id: 'TxtFilterGridDataView_NAMA_viKasirLABKasir',
                                                                anchor: '50%',
                                                                enableKeyEvents: true,
                                                                listeners:
                                                                        {
                                                                            'keyup': function ()
                                                                            {
                                                                                RefreshDataFilterKasirLABKasir();
                                                                            }
                                                                        }
                                                            },
                                                            {
                                                                xtype: 'tbspacer',
                                                                height: 3
                                                            },
                                                            getItemPanelcombofilter(),
                                                            getItemPaneltgl_filter()
                                                        ]
                                            },
                                        ]
                            },
                            grListTRKasirLAB, GDtabDetailLAB, LegendViewCMRequest],
                        tbar:
                                [
                                ],
                        listeners:
                                {
                                    'afterrender': function ()
                                    {
                                        loaddatastoreunitRadLab();
                                    }
                                }
                    }
            );

    return FormDepanKasirLAB;

};
function showhide_unit(kode)
{
	if(kode==='05')
	{
	Ext.getCmp('txtTranferunitLAB').hide();
	Ext.getCmp('txtTranferkelaskamar_LAB').show();

	}else{
	Ext.getCmp('txtTranferunitLAB').show();
	Ext.getCmp('txtTranferkelaskamar_LAB').hide();
	}
}
function dataparam_viradlab()
{
    var paramsload_viradlab =
            {
                Table: 'ViewComboUnitRadLab'
            }
    return paramsload_viradlab;
}

var dsunitinduk_viKasirLABKasir;

function loaddatastoreunitRadLab()
{
    dsunitinduk_viKasirLABKasir.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'kd_unit',
                                    Sortdir: 'ASC',
                                    target: 'ComboUnitRadLab',
                                    param: ""
                                }
                    }
            );
}

function TransferLookUp_lab(rowdata)
{
    var lebar = 440;
    FormLookUpsdetailTRTransfer_Lab = new Ext.Window
            (
                    {
                        id: 'gridTransfer_Lab',
                        title: 'Transfer',
                        closeAction: 'destroy',
                        width: lebar,
                        height: 410,
                        border: false,
                        resizable: false,
                        plain: false,
                        layout: 'fit',
                        iconCls: 'Request',
                        modal: true,
                        items: getFormEntryTRTransfer_lab(lebar),
                        listeners:
                                {
                                }
                    }
            );

    FormLookUpsdetailTRTransfer_Lab.show();
    //  Transferbaru();

};

function getFormEntryTRTransfer_lab(lebar)
{
    var pnlTRTransfer = new Ext.FormPanel
            (
                    {
                        id: 'PanelTRTransfer',
                        fileUpload: true,
                        region: 'north',
                        layout: 'column',
                        bodyStyle: 'padding:10px 10px 10px 10px',
                        height: 425,
                        anchor: '100%',
                        width: lebar,
                        border: false,
                        items: [getItemPanelInputTransfer_LAB(lebar), getItemPanelButtonTransfer_lab(lebar)],
                        tbar:
                                [
                                ]
                    }
            );

    var FormDepanTransfer = new Ext.Panel
            (
                    {
                        id: 'FormDepanTransfer',
                        region: 'center',
                        width: '100%',
                        anchor: '100%',
                        layout: 'form',
                        title: '',
                        bodyStyle: 'padding:15px',
                        border: true,
                        bodyStyle: 'background:#FFFFFF;',
                                shadhow: true,
                        items:
                                [
                                    pnlTRTransfer

                                ]

                    }
            );

    return FormDepanTransfer
}
;

function getItemPanelInputTransfer_LAB(lebar)
{
    var items =
            {
                layout: 'fit',
                anchor: '100%',
                width: lebar - 35,
                labelAlign: 'right',
                bodyStyle: 'padding:10px 10px 10px 0px',
                border: false,
                height: 330,
                items:
                        [
                            {
                                columnWidth: .9,
                                width: lebar - 35,
                                labelWidth: 100,
                                layout: 'form',
                                height: 330,
                                border: false,
                                items:
                                        [
                                            getTransfertujuan_lab(lebar),
                                            {
                                                xtype: 'tbspacer',
                                                height: 5
                                            },
                                            getItemPanelNoTransksiTransfer(lebar),
                                            {
                                                xtype: 'tbspacer',
                                                height: 3
                                            },
                                            mComboalasan_transfer()

                                        ]
                            },
                        ]
            };
    return items;
}
;

function mComboalasan_transfer()
{
    var Field = ['KD_ALASAN', 'ALASAN'];

    var dsalasan_transfer = new WebApp.DataStore({fields: Field});
    dsalasan_transfer.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'kd_alasan',
                                    Sortdir: 'ASC',
                                    target: 'ComboAlasanTransfer',
                                    param: "" //+"~ )"
                                }
                    }
            );

    var cboalasan_transfer = new Ext.form.ComboBox
            (
                    {
                        id: 'cboalasan_transfer',
                        typeAhead: false,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        emptyText: '',
                        fieldLabel: ' Alasan Transfer ',
                        align: 'Right',
                        width: 100,
                        editable: false,
                        anchor: '100%',
                        store: dsalasan_transfer,
                        valueField: 'ALASAN',
                        displayField: 'ALASAN',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                    }

                                }
                    }
            );

    return cboalasan_transfer;
};

function getItemPanelNoTransksiTransfer(lebar)
{
    var items =
            {
                Width: lebar,
                height: 110,
                layout: 'column',
                border: true,
                items:
                        [
                            {
                                columnWidth: .990,
                                layout: 'form',
                                Width: lebar - 10,
                                labelWidth: 130,
                                border: false,
                                items:
                                        [
                                            {
                                                xtype: 'tbspacer',
                                                height: 3
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Jumlah Biaya',
                                                maxLength: 200,
                                                name: 'txtjumlahbiayasal',
                                                id: 'txtjumlahbiayasal',
                                                width: 100,
                                                style: {'text-align': 'right'},
                                                readOnly: true,
                                                anchor: '95%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Paid',
                                                maxLength: 200,
                                                name: 'txtpaid',
                                                id: 'txtpaid',
                                                readOnly: true,
                                                align: 'right',
                                                style: {'text-align': 'right'},
                                                width: 100,
                                                value: 0,
                                                anchor: '95%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Jumlah dipindahkan',
                                                maxLength: 200,
                                                name: 'txtjumlahtranfer',
                                                id: 'txtjumlahtranfer',
                                                align: 'right',
                                                readOnly: true,
                                                style: {'text-align': 'right'},
                                                width: 100,
                                                anchor: '95%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Saldo tagihan',
                                                maxLength: 200,
                                                name: 'txtsaldotagihan',
                                                id: 'txtsaldotagihan',
                                                style: {'text-align': 'right'},
                                                readOnly: true,
                                                value: 0,
                                                width: 100,
                                                anchor: '95%'
                                            }
                                        ]
                            }
                        ]
            }
    return items;
}
;

function getTransfertujuan_lab(lebar)
{
    var items =
            {
                Width: lebar - 2,
                height: 170,
                layout: 'form',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                labelWidth: 130,
                items:
                        [
                            {
                                xtype: 'tbspacer',
                                height: 2
                            },
                            mComboTransferTujuan(),
                            {
                                xtype: 'textfield',
                                fieldLabel: 'No Transaksi',
                                //maxLength: 200,
                                name: 'txtTranfernoTransaksiLAB',
                                id: 'txtTranfernoTransaksiLAB',
                                labelWidth: 130,
                                readonly: true,
                                width: 100,
                                anchor: '95%'
                            },
                            {
                                xtype: 'datefield',
                                fieldLabel: 'Tanggal ',
                                id: 'dtpTanggaltransaksiRujuanLAB',
                                name: 'dtpTanggaltransaksiRujuanLAB',
                                format: 'd/M/Y',
                                readOnly: true,
                                //  value: now,
                                labelWidth: 130,
                                width: 100,
                                anchor: '95%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'No Medrec',
                                //maxLength: 200,
                                name: 'txtTranfernomedrecLAB',
                                id: 'txtTranfernomedrecLAB',
                                labelWidth: 130,
                                readOnly: true,
                                width: 100,
                                anchor: '95%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'Nama Pasien',
                                //maxLength: 200,
                                name: 'txtTranfernamapasienLAB',
                                id: 'txtTranfernamapasienLAB',
                                readOnly: true,
                                labelWidth: 130,
                                width: 100,
                                anchor: '95%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'Unit Perawatan',
                                //maxLength: 200,
                                name: 'txtTranferunitLAB',
                                id: 'txtTranferunitLAB',
                                readOnly: true,
                                labelWidth: 130,
                                width: 100,
                                anchor: '95%'
                            },
							{
                                        xtype: 'textfield',
                                        fieldLabel: 'Unit Perawatan',
                                        name: 'txtTranferkelaskamar_LAB',
                                        id: 'txtTranferkelaskamar_LAB',
										readOnly : true,
										labelWidth:130,
                                        width: 100,
                                        anchor: '95%'
                            }
                        ]
            }
    return items;
}
;
function getItemPanelButtonTransfer_lab(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                height: 30,
                anchor: '100%',
                style: {'margin-top': '-1px'},
                items:
                        [
                            {
                                layout: 'hBox',
                                width: 400,
                                border: false,
                                bodyStyle: 'padding:5px 0px 5px 5px',
                                defaults: {margins: '3 3 3 3'},
                                anchor: '90%',
                                layoutConfig:
                                        {
                                            align: 'middle',
                                            pack: 'end'
                                        },
                                items:
                                        [
                                            {
                                                xtype: 'button',
                                                text: 'Simpan',
                                                width: 70,
                                                style: {'margin-left': '0px', 'margin-top': '0px'},
                                                hideLabel: true,
                                                id: 'btnOkTransfer_lab',
                                                handler: function ()
                                                {
												
                                                    TransferData_lab(false);
													 Ext.getCmp('btnSimpanKasirLAB').disable();
													 Ext.getCmp('btnTransferKasirLAB').disable();
													 RefreshDataKasirLABDetail(Ext.getCmp('txtNoTransaksiKasirLABKasir').getValue());
													
                                                }
                                            },
                                            {
                                                xtype: 'button',
                                                text: 'Tutup',
                                                width: 70,
                                                hideLabel: true,
                                                id: 'btnCancelTransfer_lab',
                                                handler: function ()
                                                {
                                                    FormLookUpsdetailTRTransfer_Lab.close();
                                                }
                                            }
                                        ]
                            }
                        ]
            }
    return items;
}
;

function getParamTransferRwi_dari_lab()
{

/*
       KDkasirIGD:'01',
		TrKodeTranskasi: notransaksi,
		KdUnit: kodeunit,
		Kdpay: kdpaytransfer,
		Jumlahtotal: Ext.get(txtjumlahbiayasal).dom.value,
		Tglasal:  ShowDate(tgltrans),
		Shift: tampungshiftsekarang,
		TRKdTransTujuan:Ext.get(txtTranfernoTransaksiRWJ).dom.value,
		KdpasienIGDtujuan: Ext.get(txtTranfernomedrecRWJ).dom.value,
		TglTranasksitujuan : Ext.get(dtpTanggaltransaksiRujuanRWJ).dom.value,
		KDunittujuan : Trkdunit2,
		KDalasan :Ext.get(cboalasan_transfer).dom.value,
		KasirRWI:'05',
		Kdcustomer:kdcustomeraa,
		kodeunitkamar:tmp_kodeunitkamar,
		kdspesial:tmp_kdspesial,
		nokamar:tmp_nokamar,


*/
    var params =
            {
                KDkasirIGD: kdkasir,
                Kdcustomer: kdcustomeraa,
                TrKodeTranskasi: notransaksi,
                KdUnit: kodeunit,
                Kdpay: kdpaytransfer,
                Jumlahtotal: Ext.get(txtjumlahbiayasal).dom.value,
                Tglasal: ShowDate(tgltrans),
                Shift: tampungshiftsekarang,
                TRKdTransTujuan: Ext.get(txtTranfernoTransaksiLAB).dom.value,
                KdpasienIGDtujuan: Ext.get(txtTranfernomedrecLAB).dom.value,
                TglTranasksitujuan: Ext.get(dtpTanggaltransaksiRujuanLAB).dom.value,
                KDunittujuan: Trkdunit2,
                KDalasan: Ext.get(cboalasan_transfer).dom.value,
                KasirRWI: kdkasirasal,
				kodeunitkamar:tmp_kodeunitkamar_LAB,
				kdspesial:tmp_kdspesial_LAB,
				nokamar:tmp_nokamar_LAB,


            };
    return params
}
;
function TransferData_lab(mBol)
{
    Ext.Ajax.request
            (
                    {
                        //url: "./Datapool.mvc/CreateDataObj",
                        url: baseURL + "index.php/main/functionKasirPenunjang/saveTransfer",
                        params: getParamTransferRwi_dari_lab(),
                        failure: function (o)
                        {

                            ShowPesanWarningKasirLAB('Transfer Tidak berhasil silahkan hubungi admin', 'Gagal');
                            RefreshDataFilterKasirLABKasir();
                        },
                        success: function (o)
                        {	
						
                            RefreshDataFilterKasirLABKasir();
                            
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {	Ext.getCmp('btnSimpanKasirLAB').disable();
                                Ext.getCmp('btnTransferKasirLAB').disable();
                                ShowPesanInfoKasirLAB('Transfer Berhasil', 'transfer ');
                                FormLookUpsdetailTRTransfer_Lab.close();
								
                            } else
                            {
                                ShowPesanWarningKasirLAB('Transfer Tidak berhasil silahkan hubungi admin', 'Gagal');
                            }
                            ;
                        }
                    }
            )

}
;


function RefreshDataKasirLABDetail(no_transaksi)
{

    var strKriteriaLAB = '';
    strKriteriaLAB = "\"no_transaksi\" = ~" + no_transaksi + "~";

    dsTRDetailKasirLABList.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'tgl_transaksi',
                                    //Sort: 'tgl_transaksi',
                                    Sortdir: 'ASC',
                                    target: 'ViewDetailRWJGridBawah',
                                    param: strKriteriaLAB
                                }
                    }
            );
    return dsTRDetailKasirLABList;
}
;


function GetDTLTRLABGrid()
{
    var fldDetail = ['KD_PRODUK', 'DESKRIPSI', 'DESKRIPSI2', 'KD_TARIF', 'HARGA', 'QTY', 'DESC_REQ', 'TGL_BERLAKU', 'NO_TRANSAKSI', 'URUT', 'DESC_STATUS', 'TGL_TRANSAKSI'];

    dsTRDetailKasirLABList = new WebApp.DataStore({fields: fldDetail})
    RefreshDataKasirLABDetail();
    var gridDTLTRLAB = new Ext.grid.EditorGridPanel
            (
                    {
                        title: 'Item Pemeriksaan',
                        stripeRows: true,
                        store: dsTRDetailKasirLABList,
                        border: true,
                        columnLines: true,
                        frame: false,
                        anchor: '100%',
                        autoScroll: true,
                        sm: new Ext.grid.CellSelectionModel
                                (
                                        {
                                            singleSelect: true,
                                            listeners:
                                                    {
                                                        cellselect: function (sm, row, rec)
                                                        {
                                                            cellSelected2deskripsi = dsTRDetailKasirLABList.getAt(row);
                                                            CurrentPenataJasaKasirLAB.row = row;
                                                            CurrentPenataJasaKasirLAB.data = cellSelected2deskripsi;
                                                            //FocusCtrlCMLAB='txtAset'; */
                                                        }
                                                    }
                                        }
                                ),
                        cm: TRRawatJalanColumModel(),
                        tbar:
                                [
                                    {
                                        text: 'Tambah Produk',
                                        id: 'btnLookupKasirLAB',
                                        tooltip: nmLookup,
                                        iconCls: 'find',
                                        disabled: true,
                                        hidden: true,
                                        handler: function ()
                                        {

                                            var p = RecordBaruLAB();
                                            var str = '';
                                            str = kodeunit;
                                            FormLookupKasirLAB(str, dsTRDetailKasirLABList, p, true, '', true);
                                        }
                                    },
                                    {
                                        text: 'Simpan',
                                        id: 'btnSimpanLAB',
                                        tooltip: nmSimpan,
                                        iconCls: 'save',
                                        disabled: true,
                                        hidden: true,
                                        handler: function ()
                                        {
                                            Datasave_KasirLAB_2(false);
                                            Ext.getCmp('btnLookupKasirLAB').disable();
                                            Ext.getCmp('btnSimpanLAB').disable();
                                            Ext.getCmp('btnHpsBrsLAB').disable();
											Ext.getCmp('btnTransferKasirLAB').disable();



                                        }
                                    },
                                    {
                                        id: 'btnHpsBrsLAB',
                                        text: 'Hapus Baris',
                                        tooltip: 'Hapus Baris',
                                        disabled: true,
                                        hidden: true,
                                        iconCls: 'RemoveRow',
                                        handler: function ()
                                        {
                                            if (dsTRDetailKasirLABList.getCount() > 0)
                                            {
                                                if (cellSelected2deskripsi != undefined)
                                                {
                                                    if (CurrentPenataJasaKasirLAB != undefined)
                                                    {
                                                        HapusBarisLAB();
                                                        Ext.getCmp('btnLookupKasirLAB').disable();
                                                        Ext.getCmp('btnSimpanLAB').disable();
                                                        Ext.getCmp('btnHpsBrsLAB').disable();
                                                    }
                                                } else
                                                {
                                                    ShowPesanWarningLAB('Pilih record ', 'Hapus data');
                                                }
                                            }
                                        }
                                    }

                                ],
                        viewConfig: {forceFit: true}
                    }


            );



    return gridDTLTRLAB;
}
;

function HapusBarisLAB()
{
    if (cellSelected2deskripsi != undefined)
    {
        if (cellSelected2deskripsi.data.DESKRIPSI2 != '' && cellSelected2deskripsi.data.KD_PRODUK != '')
        {
            var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan penghapusan:', function (btn, combo) {
                if (btn == 'ok') {
                    variablehistori = combo;
                    DataDeleteKasirLABDetail();
                    dsTRDetailKasirLABList.removeAt(CurrentPenataJasaKasirLAB.row);
                }
            });
        } else {
            //dsTRDetailDiagnosaList.removeAt(CurrentDiagnosa.row);
            dsTRDetailKasirLABList.removeAt(CurrentPenataJasaKasirLAB.row);
        }
        ;
    }

}
;

function DataDeleteKasirLABDetail()
{
    Ext.Ajax.request
            (
                    {
                        //url: "./Datapool.mvc/DeleteDataObj",
                        url: baseURL + "index.php/main/DeleteDataObj",
                        params: getParamDataDeleteKasirLABDetail(),
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true) {
                                ShowPesanInfoKasirLAB(nmPesanHapusSukses, nmHeaderHapusData);
                                dsTRDetailKasirLABList.removeAt(CurrentPenataJasaKasirLAB.row);
                                cellSelecteddeskripsi = undefined;
                                RefreshDataKasirLABDetail(notransaksi);
                                AddNewKasirLAB = false;
                            } else if (cst.success === false && cst.pesan === 0) {
                                ShowPesanWarningKasirLAB(nmPesanHapusGagal, nmHeaderHapusData);
                            } else {
                                ShowPesanWarningKasirLAB(nmPesanHapusError, nmHeaderHapusData);
                            }
                            ;
                        }
                    }
            )
}
;

function getParamDataDeleteKasirLABDetail()
{

    var params =
            {
                Table: 'ViewTrKasirRwj',
                TrKodeTranskasi: CurrentPenataJasaKasirLAB.data.data.NO_TRANSAKSI,
                TrTglTransaksi: CurrentPenataJasaKasirLAB.data.data.TGL_TRANSAKSI,
                TrKdPasien: CurrentPenataJasaKasirLAB.data.data.KD_PASIEN,
                TrKdNamaPasien: namapasien,
                TrKdUnit: kodeunit,
                TrNamaUnit: namaunit,
                Uraian: CurrentPenataJasaKasirLAB.data.data.DESKRIPSI2,
                AlasanHapus: variablehistori,
                TrHarga: CurrentPenataJasaKasirLAB.data.data.HARGA,
                TrKdProduk: CurrentPenataJasaKasirLAB.data.data.KD_PRODUK,
                RowReq: CurrentPenataJasaKasirLAB.data.data.URUT,
                Hapus: 2
            };

    return params
}
;


function Datasave_KasirLAB_2(mBol)
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/functionKasirPenunjang/savedetailpenyakit",
                        params: getParamDetailTransaksiLAB_2(),
                        failure: function (o)
                        {
                            ShowPesanWarningLAB('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
                            RefreshDataKasirLABDetail(notransaksi);
                        },
                        success: function (o)
                        {
                            RefreshDataKasirLABDetail(notransaksi);
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                ShowPesanInfoKasirLAB(nmPesanSimpanSukses, nmHeaderSimpanData);
                                RefreshDataFilterKasirLAB();
                                if (mBol === false)
                                {
                                    RefreshDataKasirLABDetail(notransaksi);
                                }
                                ;
                            } else
                            {
                                ShowPesanWarningKasirLAB('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
                            }
                            ;
                        }
                    }
            )
}
;

function getParamDetailTransaksiLAB_2()
{
    var params =
            {
                TrKodeTranskasi: notransaksi,
                KdUnit: kodeunit,
                //DeptId:Ext.get('txtKdDokter').dom.value ,tampungshiftsekarang
                Tgl: tgltrans,
                Shift: tampungshiftsekarang,
                List: getArrDetailTrLAB(),
                JmlField: mRecordLAB.prototype.fields.length - 4,
                JmlList: GetListCountPenataDetailTransaksi(),
                Hapus: 1,
                Ubah: 0
            };
    return params
}
;
function getArrDetailTrLAB()
{
    var x = '';
    for (var i = 0; i < dsTRDetailKasirLABList.getCount(); i++)
    {
        if (dsTRDetailKasirLABList.data.items[i].data.KD_PRODUK != '' && dsTRDetailKasirLABList.data.items[i].data.DESKRIPSI != '')
        {
            var y = '';
            var z = '@@##$$@@';

            y = 'URUT=' + dsTRDetailKasirLABList.data.items[i].data.URUT
            y += z + dsTRDetailKasirLABList.data.items[i].data.KD_PRODUK
            y += z + dsTRDetailKasirLABList.data.items[i].data.QTY
            y += z + ShowDate(dsTRDetailKasirLABList.data.items[i].data.TGL_BERLAKU)
            y += z + dsTRDetailKasirLABList.data.items[i].data.HARGA
            y += z + dsTRDetailKasirLABList.data.items[i].data.KD_TARIF
            y += z + dsTRDetailKasirLABList.data.items[i].data.URUT


            if (i === (dsTRDetailKasirLABList.getCount() - 1))
            {
                x += y
            } else
            {
                x += y + '##[[]]##'
            }
            ;
        }
        ;
    }

    return x;
}
;


function RecordBaruLAB()
{

    var p = new mRecordLAB
            (
                    {
                        'DESKRIPSI2': '',
                        'KD_PRODUK': '',
                        'DESKRIPSI': '',
                        'KD_TARIF': '',
                        'HARGA': '',
                        'QTY': '',
                        'TGL_TRANSAKSI': tanggaltransaksitampung,
                        'DESC_REQ': '',
                        'KD_TARIF':'',
                                'URUT': ''
                    }
            );

    return p;
}
;
var mRecordLAB = Ext.data.Record.create
        (
                [
                    {name: 'DESKRIPSI2', mapping: 'DESKRIPSI2'},
                    {name: 'KD_PRODUK', mapping: 'KD_PRODUK'},
                    {name: 'DESKRIPSI', mapping: 'DESKRIPSI'},
                    {name: 'KD_TARIF', mapping: 'KD_TARIF'},
                    {name: 'HARGA', mapping: 'HARGA'},
                    {name: 'QTY', mapping: 'QTY'},
                    {name: 'TGL_TRANSAKSI', mapping: 'TGL_TRANSAKSI'},
                    {name: 'DESC_REQ', mapping: 'DESC_REQ'},
                    // {name: 'KD_TARIF', mapping:'KD_TARIF'},
                    {name: 'URUT', mapping: 'URUT'}
                ]
                );


function TRRawatJalanColumModel()
{
    return new Ext.grid.ColumnModel
            (
                    [
                        new Ext.grid.RowNumberer(),
                        {
                            id: 'coleskripsiLAB',
                            header: 'Uraian',
                            dataIndex: 'DESKRIPSI2',
                            // width:.30,
                            menuDisabled: true,
                            hidden: true

                        },
                        {
                            id: 'colKdProduk',
                            header: 'Kode Produk',
                            dataIndex: 'KD_PRODUK',
                            width: 100,
                            menuDisabled: true,
                            hidden: true
                        },
                        {
                            id: 'colDeskripsiLAB',
                            header: 'Nama Produk',
                            dataIndex: 'DESKRIPSI',
                            sortable: false,
                            hidden: false,
                            menuDisabled: true,
                            width: 250

                        },
                        {
                            header: 'Tanggal Transaksi',
                            dataIndex: 'TGL_TRANSAKSI',
                            width: 100,
                            menuDisabled: true,
                            renderer: function (v, params, record)
                            {

                                return ShowDate(record.data.TGL_TRANSAKSI);
                            }
                        },
                        {
                            id: 'colHARGALAB',
                            header: 'Harga',
                            align: 'right',
                            hidden: true,
                            menuDisabled: true,
                            dataIndex: 'HARGA',
                            //  width:.10,
                            renderer: function (v, params, record)
                            {
                                return formatCurrency(record.data.HARGA);

                            }
                        },
                        {
                            id: 'colProblemLAB',
                            header: 'Qty',
                            width: '100%',
                            align: 'right',
                            menuDisabled: true,
                            dataIndex: 'QTY',
                            editor: new Ext.form.TextField
                                    (
                                            {
                                                id: 'fieldcolProblemLAB',
                                                allowBlank: true,
                                                enableKeyEvents: true,
                                                width: 100,
                                                listeners:
                                                        {
                                                            'specialkey': function ()
                                                            {
                                                                Dataupdate_KasirLAB(false);
                                                                //RefreshDataFilterKasirLAB();
                                                                //RefreshDataFilterKasirLAB();
                                                            }
                                                        }
                                            }
                                    ),
                        },
                        {
                            id: 'colImpactLAB',
                            header: 'CR',
                            // width:80,
                            dataIndex: 'IMPACT',
                            hidden: true,
                            editor: new Ext.form.TextField
                                    (
                                            {
                                                id: 'fieldcolImpactLAB',
                                                allowBlank: true,
                                                enableKeyEvents: true,
                                                width: 30
                                            }
                                    )
                        }

                    ]
                    )
}
;


function getItemPaneltgl_filter()
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .35,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            {
                                                xtype: 'datefield',
                                                fieldLabel: 'Tanggal ',
                                                id: 'dtpTglAwalFilterKasirLAB',
                                                name: 'dtpTglAwalFilterKasirLAB',
                                                format: 'd/M/Y',
                                                //readOnly : true,
                                                value: now,
                                                anchor: '96.7%',
                                                listeners:
                                                        {
                                                            'specialkey': function ()
                                                            {
                                                                var tmpNoMedrec = Ext.get('txtFilterKasirNomedrecLab').getValue()
                                                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10)
                                                                {
                                                                    RefreshDataFilterKasirLABKasir();
                                                                }
                                                            }
                                                        }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .35,
                                layout: 'form',
                                border: false,
                                labelWidth: 60,
                                items:
                                        [
                                            {
                                                xtype: 'datefield',
                                                fieldLabel: 's/d ',
                                                id: 'dtpTglAkhirFilterKasirLAB',
                                                name: 'dtpTglAkhirFilterKasirLAB',
                                                format: 'd/M/Y',
                                                //readOnly : true,
                                                value: now,
                                                anchor: '100%',
                                                listeners:
                                                        {
                                                            'specialkey': function ()
                                                            {
                                                                var tmpNoMedrec = Ext.get('txtFilterKasirNomedrecLab').getValue()
                                                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10)
                                                                {
                                                                    RefreshDataFilterKasirLABKasir();
                                                                }
                                                            }
                                                        }
                                            }
                                        ]
                            }
                        ]
            }
    return items;
}
;
function getItemPanelcombofilter()
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .35,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            mComboStatusBayar_viKasirLABKasir()
                                        ]
                            },
                            {
                                columnWidth: .35,
                                layout: 'form',
                                border: false,
                                labelWidth: 60,
                                items:
                                        [
                                            mComboUnit_viKasirLABKasir()
                                        ]
                            }
                        ]
            }
    return items;
}
;


function mComboStatusBayar_viKasirLABKasir()
{
    var cboStatus_viKasirLABKasir = new Ext.form.ComboBox
            (
                    {
                        id: 'cboStatus_viKasirLABKasir',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        anchor: '96%',
                        emptyText: '',
                        fieldLabel: 'Status Lunas',
                        //width:60,
                        store: new Ext.data.ArrayStore
                                (
                                        {
                                            id: 0,
                                            fields:
                                                    [
                                                        'Id',
                                                        'displayText'
                                                    ],
                                            data: [[1, 'Semua'], [2, 'Lunas'], [3, 'Belum Lunas']]
                                        }
                                ),
                        valueField: 'Id',
                        displayField: 'displayText',
                        value: selectCountStatusByr_viKasirLABKasir,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectCountStatusByr_viKasirLABKasir = b.data.displayText;
                                        //RefreshDataSetDokter();
                                        RefreshDataFilterKasirLABKasir();

                                    }
                                }
                    }
            );
    return cboStatus_viKasirLABKasir;
}
;

function mComboTransferTujuan()
{
    var cboTransferTujuan = new Ext.form.ComboBox
            (
                    {
                        id: 'cboTransferTujuan',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        anchor: '96%',
                        emptyText: '',
                        hidden: true,
                        fieldLabel: 'Transfer',
                        //width:60,
                        store: new Ext.data.ArrayStore
                                (
                                        {
                                            id: 0,
                                            fields:
                                                    [
                                                        'Id',
                                                        'displayText'
                                                    ],
                                            data: [[1, 'Gawat Darurat'], [2, 'Rawat Inap'], [3, 'Rawat Jalan']]
                                        }
                                ),
                        valueField: 'Id',
                        displayField: 'displayText',
                        value: tranfertujuan,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        tranfertujuan = b.data.displayText;
                                        //RefreshDataSetDokter();
                                        RefreshDataFilterKasirLABKasir();

                                    }
                                }
                    }
            );
    return cboTransferTujuan;
}
;

function mComboUnitInduk()
{
    var Field = ['kd_bagian', 'nama_unit'];

    dsunitinduk_viKasirLABKasir = new WebApp.DataStore({fields: Field});

    var cboUnitInduk = new Ext.form.ComboBox
            (
                    {
                        id: 'cboUnitInduk',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        anchor: '22,7%',
                        emptyText: '',
                        fieldLabel: 'Pilih Poli',
                        store: dsunitinduk_viKasirLABKasir,
                        valueField: 'kd_bagian',
                        displayField: 'nama_unit',
                        hidden: true,
                        value: selectindukunitLABKasir,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        selectindukunitLABKasir = b.data.displayText;
                                        RefreshDataFilterKasirLABKasir();
                                    }
                                }
                    }
            );
    return cboUnitInduk;
}
;

function mComboUnit_viKasirLABKasir()
{
    var Field = ['KD_UNIT', 'NAMA_UNIT'];

    dsunit_viKasirLABKasir = new WebApp.DataStore({fields: Field});
    dsunit_viKasirLABKasir.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'kd_unit',
                                    Sortdir: 'ASC',
                                    target: 'ComboUnit',
                                    param: "" //+"~ )"
                                }
                    }
            );

    var cboUNIT_viKasirLABKasir = new Ext.form.ComboBox
            (
                    {
                        id: 'cboUNIT_viKasirLABKasir',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        emptyText: '',
                        fieldLabel: ' Poli ',
                        align: 'Right',
                        //width: 100,
                        anchor: '100%',
                        store: dsunit_viKasirLABKasir,
                        valueField: 'KD_UNIT',
                        displayField: 'NAMA_UNIT',
                        value: 'All',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        RefreshDataFilterKasirLABKasir();
                                    }
                                }
                    }
            );

    return cboUNIT_viKasirLABKasir;
}
;



function KasirLookUpLAB(rowdata)
{
    var lebar = 580;
    FormLookUpsdetailTRKasirLAB = new Ext.Window
            (
                    {
                        id: 'gridKasirLAB',
                        title: 'Kasir Laboratorium',
                        closeAction: 'destroy',
                        width: lebar,
                        height: 500,
                        border: false,
                        resizable: false,
                        plain: true,
                        layout: 'fit',
                        iconCls: 'Request',
                        modal: true,
                        items: getFormEntryKasirLAB(lebar),
                        listeners:
                                {
                                }
                    }
            );

    FormLookUpsdetailTRKasirLAB.show();
    if (rowdata == undefined)
    {
        KasirLABAddNew();
    } else
    {
        TRKasirLABInit(rowdata)
    }

}
;



function getFormEntryKasirLAB(lebar)
{
    var pnlTRKasirLAB = new Ext.FormPanel
            (
                    {
                        id: 'PanelTRKasirLAB',
                        fileUpload: true,
                        region: 'north',
                        layout: 'column',
                        bodyStyle: 'padding:10px 10px 10px 10px',
                        height: 130,
                        anchor: '100%',
                        width: lebar,
                        border: false,
                        items: [getItemPanelInputKasir(lebar)],
                        tbar:
                                [
                                ]
                    }
            );
    var x;
    var paneltotal = new Ext.Panel
            (
                    {
                        id: 'paneltotal',
                        region: 'center',
                        border: false,
                        bodyStyle: 'padding:0px 7px 0px 7px',
                        layout: 'column',
                        frame: true,
                        height: 67,
                        anchor: '100% 8.0000%',
                        autoScroll: false,
                        items:
                                [
                                    {
                                        xtype: 'compositefield',
                                        anchor: '100%',
                                        labelSeparator: '',
                                        border: true,
                                        style: {'margin-top': '5px'},
                                        items:
                                                [
                                                    {
                                                        layout: 'form',
                                                        style: {'text-align': 'right', 'margin-left': '370px'},
                                                        border: false,
                                                        html: " Total :"
                                                    },
                                                    {
                                                        xtype: 'textfield',
                                                        id: 'txtJumlah2EditData_viKasirLAB',
                                                        name: 'txtJumlah2EditData_viKasirLAB',
                                                        style: {'text-align': 'right', 'margin-left': '390px'},
                                                        width: 82,
                                                        readOnly: true,
                                                    },
                                                            //-------------- ## --------------
                                                ]
                                    },
                                    {
                                        xtype: 'compositefield',
                                        anchor: '100%',
                                        labelSeparator: '',
                                        border: true,
                                        style: {'margin-top': '5px'},
                                        items:
                                                [
                                                    {
                                                        layout: 'form',
                                                        style: {'text-align': 'right', 'margin-left': '370px'},
                                                        border: false,
                                                        hidden: true,
                                                        html: " Bayar :"
                                                    },
                                                    {
                                                        xtype: 'numberfield',
                                                        id: 'txtJumlahEditData_viKasirLAB',
                                                        name: 'txtJumlahEditData_viKasirLAB',
                                                        hidden: true,
                                                        style: {'text-align': 'right', 'margin-left': '390px'},
                                                        width: 82,
                                                        //readOnly: true,
                                                    }
                                                    //-------------- ## --------------
                                                ]
                                    }
                                ]
                    }
            )
    var GDtabDetailKasirLAB = new Ext.Panel
            (
                    {
                        id: 'GDtabDetailKasirLAB',
                        region: 'center',
                        activeTab: 0,
                        height: 350,
                        anchor: '100% 100%',
                        border: false,
                        plain: true,
                        defaults: {autoScroll: true},
                        items: [GetDTLTRKasirLABGrid(), paneltotal],
                        tbar:
                                [
                                    {
                                        text: 'Bayar',
                                        id: 'btnSimpanKasirLAB',
                                        tooltip: nmSimpan,
                                        iconCls: 'save',
                                        handler: function ()
                                        {
                                            Datasave_KasirLABKasir(false);
                                           // FormDepan2KasirLAB.close();
                                            Ext.getCmp('btnSimpanKasirLAB').disable();
                                            Ext.getCmp('btnTransferKasirLAB').disable();
                                            //Ext.getCmp('btnHpsBrsKasirLAB').disable();
                                        }
                                    },
									
                                    {
                                        id: 'btnTransferKasirLAB',
                                        text: 'Transfer',
                                        tooltip: nmEditData,
                                        iconCls: 'Edit_Tr',
                                        /* disabled: true, */
                                        handler: function (sm, row, rec)
                                        {
                                            TransferLookUp_lab();
                                            Ext.Ajax.request({
                                                url: baseURL + "index.php/main/GetPasienTranferLab_rad",
                                                params: {
                                                    notr: notransaksiasal,
                                                    kdkasir: kdkasirasal
                                                },
                                                success: function (o)
                                                {	
												showhide_unit(kdkasirasal);
                                                    if (o.responseText == "") {
                                                        ShowPesanWarningKasirLAB('Pasien belum terdaftar kedalam unit manapun / pasien sudah pulang', 'WARNING');
                                                    } else {
                                                        var tmphasil = o.responseText;
                                                        var tmp = tmphasil.split("<>");
                                                        if (tmp[5] == undefined && tmp[3] == undefined) {
                                                            ShowPesanWarningKasirLAB('Pasien belum terdaftar kedalam unit manapun / pasien sudah pulang', 'WARNING');
                                                            FormLookUpsdetailTRTransfer_Lab.close();
                                                        } else {
                                                            Ext.get(txtTranfernoTransaksiLAB).dom.value = tmp[3];
                                                            Ext.get(dtpTanggaltransaksiRujuanLAB).dom.value = ShowDate(tmp[4]);
                                                            Ext.get(txtTranfernomedrecLAB).dom.value = tmp[5];
                                                            Ext.get(txtTranfernamapasienLAB).dom.value = tmp[2];
                                                            Ext.get(txtTranferunitLAB).dom.value = tmp[1];
                                                            Trkdunit2 = tmp[6];
															Ext.get(txtTranferkelaskamar_LAB).dom.value=tmp[9];
															
															tmp_kodeunitkamar_LAB=tmp[8];
															tmp_kdspesial_LAB=tmp[10];
															tmp_nokamar_LAB=tmp[11];
                                                            var kasir = tmp[7];
                                                            Ext.Ajax.request({
                                                                url: baseURL + "index.php/main/GettotalTranfer",
                                                                params: {
                                                                    notr: notransaksi,
																	kd_kasir:kdkasir
                                                                },
                                                                success: function (o)
                                                                {
                                                                    Ext.get(txtjumlahbiayasal).dom.value = formatCurrency(o.responseText);
                                                                    Ext.get(txtjumlahtranfer).dom.value = formatCurrency(o.responseText);
                                                                }
                                                            });
                                                        }

                                                    }
                                                }
                                            });

                                        }, //disabled :true
                                    },
                                    {
                                        xtype: 'splitbutton',
                                        text: 'Cetak',
                                        iconCls: 'print',
                                        id: 'btnPrint_viDaftar',
                                        handler: function ()
                                        {
                                        },
                                        menu: new Ext.menu.Menu({
                                            items:
                                                    [
                                                        {
                                                            xtype: 'button',
                                                            text: 'Print Bill',
                                                            id: 'btnPrintBillRadLab',
                                                            handler: function ()
                                                            {
                                                                printbillRadLab();
                                                            }
                                                        },
                                                        {
                                                            xtype: 'button',
                                                            text: 'Print Kwitansi',
                                                            id: 'btnPrintKwitansiRadLab',
                                                            handler: function ()
                                                            {
                                                                printkwitansiRadLab();
                                                            }
                                                        }
                                                    ]
                                        })
                                    }
                                ]
                    }
            );



    var pnlTRKasirLAB2 = new Ext.FormPanel
            (
                    {
                        id: 'PanelTRKasirLAB2',
                        fileUpload: true,
                        region: 'north',
                        layout: 'column',
                        bodyStyle: 'padding:10px 10px 10px 10px',
                        height: 380,
                        anchor: '100%',
                        width: lebar,
                        border: false,
                        items: [GDtabDetailKasirLAB,
                        ]
                    }
            );




    FormDepan2KasirLAB = new Ext.Panel
            (
                    {
                        id: 'FormDepan2KasirLAB',
                        region: 'center',
                        width: '100%',
                        anchor: '100%',
                        layout: 'form',
                        title: '',
                        bodyStyle: 'padding:15px',
                        border: true,
                        bodyStyle: 'background:#FFFFFF;',
                                height: 380,
                        shadhow: true,
                        items: [pnlTRKasirLAB, pnlTRKasirLAB2,
                        ]
                    }
            );

    return FormDepan2KasirLAB
}
;






function TambahBarisKasirLAB()
{
    var x = true;

    if (x === true)
    {
        var p = RecordBaruKasirLAB();
        dsTRDetailKasirLABKasirList.insert(dsTRDetailKasirLABKasirList.getCount(), p);
    }
    ;
}
;





function GetDTLTRHistoryGrid()
{

    var fldDetail = ['NO_TRANSAKSI', 'TGL_BAYAR', 'DESKRIPSI', 'URUT', 'BAYAR', 'USERNAME', 'SHIFT', 'KD_USER'];

    dsTRDetailHistoryList = new WebApp.DataStore({fields: fldDetail})

    var gridDTLTRHistory = new Ext.grid.EditorGridPanel
            (
                    {
                        title: 'History Bayar',
                        stripeRows: true,
                        store: dsTRDetailHistoryList,
                        border: false,
                        columnLines: true,
                        frame: false,
                        anchor: '100% 25%',
                        autoScroll: true,
                        sm: new Ext.grid.CellSelectionModel
                                (
                                        {
                                            singleSelect: true,
                                            listeners:
                                                    {
                                                        cellselect: function (sm, row, rec)
                                                        {
                                                            cellSelecteddeskripsi = dsTRDetailHistoryList.getAt(row);
                                                            CurrentHistory.row = row;
                                                            CurrentHistory.data = cellSelecteddeskripsi;
                                                        }
                                                    }
                                        }
                                ),
                        cm: TRHistoryColumModel(),
                        viewConfig: {forceFit: true},
                        tbar:
                                [
                                    {
                                        id: 'btnHpsBrsKasirLAB',
                                        text: 'Hapus Pembayaran',
                                        tooltip: 'Hapus Baris',
                                        iconCls: 'RemoveRow',
                                        //hidden :true,
                                        handler: function ()
                                        {
                                            if (dsTRDetailHistoryList.getCount() > 0)
                                            {
                                                if (cellSelecteddeskripsi != undefined)
                                                {
                                                    if (CurrentHistory != undefined)
                                                    {
                                                        HapusBarisDetailbayar();
                                                    }
                                                } else
                                                {
                                                    ShowPesanWarningKasirLAB('Pilih record ', 'Hapus data');
                                                }
                                            }
                                            Ext.getCmp('btnEditKasirLAB').disable();
                                            Ext.getCmp('btnTutupTransaksiKasirLAB').disable();
                                            Ext.getCmp('btnHpsBrsKasirLAB').disable();
                                        },
                                        disabled: true
                                    }
                                ]
                    }

            );

    return gridDTLTRHistory;
}
;

function TRHistoryColumModel()
{
    return new Ext.grid.ColumnModel
            (
                    [
                        new Ext.grid.RowNumberer(),
                        {
                            id: 'colKdTransaksi',
                            header: 'No. Transaksi',
                            dataIndex: 'NO_TRANSAKSI',
                            width: 100,
                            menuDisabled: true,
                            hidden: false
                        },
                        {
                            id: 'colTGlbayar',
                            header: 'Tgl Bayar',
                            dataIndex: 'TGL_BAYAR',
                            menuDisabled: true,
                            width: 100,
                            renderer: function (v, params, record)
                            {
                                return ShowDate(record.data.TGL_BAYAR);

                            }

                        },
                        {
                            id: 'coleurutmasuk',
                            header: 'urut Bayar',
                            dataIndex: 'URUT',
                            //hidden:true

                        },
                        {
                            id: 'colePembayaran',
                            header: 'Pembayaran',
                            dataIndex: 'DESKRIPSI',
                            width: 150,
                            hidden: false

                        },
                        {
                            id: 'colJumlah',
                            header: 'Jumlah',
                            width: 150,
                            align: 'right',
                            dataIndex: 'BAYAR',
                            hidden: false,
                            renderer: function (v, params, record)
                            {
                                return formatCurrency(record.data.BAYAR);

                            }
                        },
                        {
                            id: 'coletglmasuk',
                            header: 'tgl masuk',
                            dataIndex: '',
                            hidden: true
                        },
                        {
                            id: 'colStatHistory',
                            header: 'History',
                            width: 130,
                            menuDisabled: true,
                            dataIndex: '',
                            hidden: true
                        },
                        {
                            id: 'colPetugasHistory',
                            header: 'Petugas',
                            width: 130,
                            menuDisabled: true,
                            dataIndex: 'USERNAME',
                            //hidden:true
                        }
                    ]
                    )
}
;
function HapusBarisDetailbayar()
{
    if (cellSelecteddeskripsi != undefined)
    {
        if (cellSelecteddeskripsi.data.NO_TRANSAKSI != '' && cellSelecteddeskripsi.data.URUT != '')
        {
            //'NO_TRANSAKSI','TGL_BAYAR','DESKRIPSI','URUT','BAYAR','USERNAME'
            /*var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan penghapusan:', function(btn, combo){
             if (btn == 'ok')
             {
             variablehistori=combo;
             if (variablehistori!='')
             {
             DataDeleteKasirLABKasirDetail();
             }
             else
             {
             ShowPesanWarningKasirLAB('Silahkan isi alasan terlebih dahaulu','Keterangan');
             }
             }	
             });  */
            msg_box_alasanhapus_LAB();
        } else
        {
            dsTRDetailHistoryList.removeAt(CurrentHistory.row);
        }
        ;
    }
}
;
function msg_box_alasanhapus_LAB()
{
    var lebar = 250;
    form_msg_box_alasanhapus_LAB = new Ext.Window
            (
                    {
                        id: 'alasan_hapusLAb',
                        title: 'Alasan Hapus Pembayaran',
                        closeAction: 'destroy',
                        width: lebar,
                        height: 100,
                        border: false,
                        resizable: false,
                        plain: true,
                        layout: 'fit',
                        iconCls: 'Request',
                        modal: true,
                        items:
                                {
                                    columnWidth: 250,
                                    layout: 'form',
                                    labelWidth: 1,
                                    border: false,
                                    items:
                                            [
                                                {
                                                    xtype: 'tbspacer',
                                                    height: 4
                                                },
                                                mComboalasan_hapusLAb(),
                                                {
                                                    layout: 'hBox',
                                                    border: false,
                                                    bodyStyle: 'padding:5px 0px 20px 20px',
                                                    defaults: {margins: '3 3 1 1'},
                                                    anchor: '95%',
                                                    layoutConfig:
                                                            {
                                                                align: 'middle',
                                                                pack: 'end'
                                                            },
                                                    items:
                                                            [
                                                                {
                                                                    xtype: 'button',
                                                                    text: 'hapus',
                                                                    width: 70,
                                                                    //style:{'margin-left':'0px','margin-top':'0px'},
                                                                    hideLabel: true,
                                                                    id: 'btnOkalasan_hapusLAb',
                                                                    handler: function ()
                                                                    {
                                                                        DataDeleteKasirLABKasirDetail();
                                                                        form_msg_box_alasanhapus_LAB.close();
                                                                    }
                                                                },
                                                                {
                                                                    xtype: 'button',
                                                                    text: 'Batal',
                                                                    width: 70,
                                                                    hideLabel: true,
                                                                    id: 'btnCancelalasan_hapusLAb',
                                                                    handler: function ()
                                                                    {
                                                                        form_msg_box_alasanhapus_LAB.close();
                                                                    }
                                                                }
                                                            ]

                                                }

                                            ]
                                }








                    }


            );

    form_msg_box_alasanhapus_LAB.show();
}
;

function mComboalasan_hapusLAb()
{
    var Field = ['KD_ALASAN', 'ALASAN'];

    var dsalasan_hapusLAb = new WebApp.DataStore({fields: Field});

    dsalasan_hapusLAb.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'alasanhapus',
                                    param: ''
                                }
                    }
            );
    var cboalasan_hapusLAb = new Ext.form.ComboBox
            (
                    {
                        id: 'cboalasan_hapusLAb',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Pilih Alasan...',
                        labelWidth: 1,
                        store: dsalasan_hapusLAb,
                        valueField: 'KD_ALASAN',
                        displayField: 'ALASAN',
                        anchor: '95%',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
//                                  var selectalasan_hapusLAb = b.data.KD_PROPINSI;
                                        //alert("is");
                                        selectDokter = b.data.KD_DOKTER;
                                    },
                                    'render': function (c)
                                    {
                                        c.getEl().on('keypress', function (e) {
                                            if (e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                Ext.getCmp('kelPasien').focus();
                                        }, c);
                                    }
                                }
                    }
            );

    return cboalasan_hapusLAb;
}
;
function DataDeleteKasirLABKasirDetail()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/functionKasirPenunjang/deletedetail_bayar",
                        params: getParamDataDeleteKasirLABKasirDetail(),
                        success: function (o)
                        {
                            //	RefreshDatahistoribayar(Kdtransaksi);
                            RefreshDataFilterKasirLABKasir();
                            RefreshDatahistoribayar('0');
                            RefreshDataKasirLABDetail('0');
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                ShowPesanInfoKasirLAB(nmPesanHapusSukses, nmHeaderHapusData);
                                //alert(Kdtransaksi);					 

                                //refeshKasirLABKasir();
                            } else if (cst.success === false && cst.pesan === 0)
                            {
                                ShowPesanWarningKasirLAB(nmPesanHapusGagal, nmHeaderHapusData);
                            } else
                            {
                                ShowPesanWarningKasirLAB(nmPesanHapusError, nmHeaderHapusData);
                            }
                            ;
                        }
                    }
            )
}
;

function getParamDataDeleteKasirLABKasirDetail()
{
    var params =
            {
                TrKodeTranskasi: CurrentHistory.data.data.NO_TRANSAKSI,
                TrTglbayar: CurrentHistory.data.data.TGL_BAYAR,
                Urut: CurrentHistory.data.data.URUT,
                Jumlah: CurrentHistory.data.data.BAYAR,
                Username: CurrentHistory.data.data.USERNAME,
                Shift: tampungshiftsekarang,
                Shift_hapus: CurrentHistory.data.data.SHIFT,
                Kd_user: CurrentHistory.data.data.KD_USER,
                Tgltransaksi: tgltrans,
                Kodepasein: kodepasien,
                NamaPasien: namapasien,
                KodeUnit: kodeunit,
                Namaunit: namaunit,
                Kodepay: kodepay,
                Uraian: CurrentHistory.data.data.DESKRIPSI,
                KDkasir: kdkasir,
                KeTterangan: Ext.get('cboalasan_hapusLAb').dom.value

            };
    Kdtransaksi = CurrentHistory.data.data.NO_TRANSAKSI;
    return params
}
;



function GetDTLTRKasirLABGrid()
{
    var fldDetail = ['KD_PRODUK', 'DESKRIPSI', 'DESKRIPSI2', 'KD_TARIF', 'HARGA', 'QTY', 'DESC_REQ', 'TGL_BERLAKU', 'NO_TRANSAKSI', 'URUT', 'DESC_STATUS', 'TGL_TRANSAKSI', 'BAYARTR', 'DISCOUNT', 'PIUTANG'];

    dsTRDetailKasirLABKasirList = new WebApp.DataStore({fields: fldDetail})
    RefreshDataKasirLABKasirDetail();
    gridDTLTRKasirLAB = new Ext.grid.EditorGridPanel
            (
                    {
                        title: 'Detail Bayar',
                        stripeRows: true,
                        id: 'gridDTLTRKasirLAB',
                        store: dsTRDetailKasirLABKasirList,
                        border: false,
                        columnLines: true,
                        frame: false,
                        anchor: '100%',
                        height: 230,
                        autoScroll: true,
                        sm: new Ext.grid.CellSelectionModel
                                (
                                        {
                                            singleSelect: true,
                                            listeners:
                                                    {
                                                    }
                                        }
                                ),
                        cm: TRKasirRawatJalanColumModel()
                    }
            );

    return gridDTLTRKasirLAB;
}
;

function TRKasirRawatJalanColumModel()
{
    return new Ext.grid.ColumnModel
            (
                    [
                        new Ext.grid.RowNumberer(),
                        {
                            id: 'coleskripsiKasirLAB',
                            header: 'Uraian',
                            dataIndex: 'DESKRIPSI2',
                            width: 250,
                            menuDisabled: true,
                            hidden: true

                        },
                        {
                            id: 'colKdProduk',
                            header: 'Kode Produk',
                            dataIndex: 'KD_PRODUK',
                            width: 100,
                            menuDisabled: true,
                            hidden: true
                        },
                        {
                            id: 'colDeskripsiKasirLAB',
                            header: 'Nama Produk',
                            dataIndex: 'DESKRIPSI',
                            sortable: false,
                            hidden: false,
                            menuDisabled: true,
                            width: 250

                        },
                        {
                            id: 'colURUTKasirLAB',
                            header: 'Urut',
                            dataIndex: 'URUT',
                            sortable: false,
                            hidden: true,
                            menuDisabled: true,
                            width: 250

                        },
                        {
                            header: 'Tanggal Transaksi',
                            dataIndex: 'TGL_TRANSAKSI',
                            width: 130,
                            hidden: true,
                            menuDisabled: true,
                            renderer: function (v, params, record)
                            {

                                return ShowDate(record.data.TGL_TRANSAKSI);
                            }
                        },
                        {
                            id: 'colQtyKasirLAB',
                            header: 'Qty',
                            width: 91,
                            align: 'right',
                            menuDisabled: true,
                            dataIndex: 'QTY',
                        },
                        {
                            id: 'colHARGAKasirLAB',
                            header: 'Harga',
                            align: 'right',
                            hidden: false,
                            menuDisabled: true,
                            dataIndex: 'HARGA',
                            width: 100,
                            renderer: function (v, params, record)
                            {
                                return formatCurrency(record.data.HARGA);

                            }
                        },
                        {
                            id: 'colPiutangKasirLAB',
                            header: 'Puitang',
                            width: 80,
                            dataIndex: 'PIUTANG',
                            align: 'right',
                            //hidden: false,
                            editor: new Ext.form.TextField
                                    (
                                            {
                                                id: 'fieldcolPuitangLAB',
                                                allowBlank: true,
                                                enableKeyEvents: true,
                                                width: 30,
                                            }
                                    ),
                            renderer: function (v, params, record)
                            {
                                //getTotalDetailProduk();
                                return formatCurrency(record.data.PIUTANG);
                            }
                        },
                        {
                            id: 'colTunaiKasirLAB',
                            header: 'Tunai',
                            width: 80,
                            dataIndex: 'BAYARTR',
                            align: 'right',
                            hidden: false,
                            editor: new Ext.form.TextField
                                    (
                                            {
                                                id: 'fieldcolTunaiLAB',
                                                allowBlank: true,
                                                enableKeyEvents: true,
                                                width: 30,
                                            }
                                    ),
                            renderer: function (v, params, record)
                            {
                                getTotalDetailProduk();

                                return formatCurrency(record.data.BAYARTR);
                            }

                        },
                        {
                            id: 'colDiscountKasirLAB',
                            header: 'Discount',
                            width: 80,
                            dataIndex: 'DISCOUNT',
                            align: 'right',
                            hidden: false,
                            editor: new Ext.form.TextField
                                    (
                                            {
                                                id: 'fieldcolDiscountLAB',
                                                allowBlank: true,
                                                enableKeyEvents: true,
                                                width: 30,
                                            }
                                    ),
                            renderer: function (v, params, record)
                            {
                                return formatCurrency(record.data.DISCOUNT);
                            }
                        }

                    ]
                    )
}
;




function RecordBaruKasirLAB()
{

    var p = new mRecordKasirLAB
            (
                    {
                        'DESKRIPSI2': '',
                        'KD_PRODUK': '',
                        'DESKRIPSI': '',
                        'KD_TARIF': '',
                        'HARGA': '',
                        'QTY': '',
                        'TGL_TRANSAKSI': tanggaltransaksitampung,
                        'DESC_REQ': '',
                        'KD_TARIF':'',
                                'URUT': ''
                    }
            );

    return p;
}
;
function TRKasirLABInit(rowdata)
{
    AddNewKasirLABKasir = false;
	notransaksi=rowdata.NO_TRANSAKSI;
    vkd_unit = rowdata.KD_UNIT;
    RefreshDataKasirLABKasirDetail(rowdata.NO_TRANSAKSI);
    Ext.get('txtNoTransaksiKasirLABKasir').dom.value = rowdata.NO_TRANSAKSI;
    tanggaltransaksitampung = rowdata.TANGGAL_TRANSAKSI;
    Ext.get('dtpTanggalDetransaksi').dom.value = ShowDate(rowdata.TANGGAL_TRANSAKSI);
    Ext.get('txtNoMedrecDetransaksi').dom.value = rowdata.KD_PASIEN;
    Ext.get('txtNamaPasienDetransaksi').dom.value = rowdata.NAMA;
    Ext.get('cboPembayaran').dom.value = rowdata.KET_PAYMENT;
    Ext.get('cboJenisByr').dom.value = rowdata.CARA_BAYAR; // take the displayField value 
    loaddatastorePembayaran(rowdata.JENIS_PAY);
    vkode_customer = rowdata.KD_CUSTOMER;
    tampungtypedata = 0;
    tapungkd_pay = rowdata.KD_PAY;
    jenispay = 1;
    var vkode_customer = rowdata.LUNAS
	kdkasirasal = rowdata.KD_KASIR_ASAL;
    notransaksiasal = rowdata.NO_TRANSAKSI_ASAL;
    showCols(Ext.getCmp('gridDTLTRKasirLAB'));
    hideCols(Ext.getCmp('gridDTLTRKasirLAB'));
    vflag = rowdata.FLAG;
    //(rowdata.NO_TRANSAKSI;


    Ext.Ajax.request({
        url: baseURL + "index.php/main/getcurrentshift",
        params: {
            command: '0',
        },
        failure: function (o)
        {
            var cst = Ext.decode(o.responseText);
        },
        success: function (o) {

            tampungshiftsekarang = o.responseText
        }

    });



}
;

function mEnabledKasirLABCM(mBol)
{
    Ext.get('btnLookupKasirLAB').dom.disabled = mBol;
    Ext.get('btnHpsBrsKasirLAB').dom.disabled = mBol;
}
;


///---------------------------------------------------------------------------------------///
function KasirLABAddNew()
{
    AddNewKasirLABKasir = true;
    Ext.get('txtNoTransaksiKasirLABKasir').dom.value = '';
    Ext.get('dtpTanggalDetransaksi').dom.value = nowTglTransaksi.format('d/M/Y');
    Ext.get('txtNoMedrecDetransaksi').dom.value = '';
    Ext.get('txtNamaPasienDetransaksi').dom.value = '';

    //Ext.get('txtKdUrutMasuk').dom.value = '';
    Ext.get('cboStatus_viKasirLABKasir').dom.value = ''
    rowSelectedKasirLABKasir = undefined;
    dsTRDetailKasirLABKasirList.removeAll();
    mEnabledKasirLABCM(false);


}
;

function RefreshDataKasirLABKasirDetail(no_transaksi)
{
    var strKriteriaKasirLAB = '';

    strKriteriaKasirLAB = 'no_transaksi = ~' + no_transaksi + '~';

    dsTRDetailKasirLABKasirList.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'tgl_transaksi',
                                    //Sort: 'tgl_transaksi',
                                    Sortdir: 'ASC',
                                    target: 'ViewDetailbayar',
                                    param: strKriteriaKasirLAB
                                }
                    }
            );
    return dsTRDetailKasirLABKasirList;
}
;

function RefreshDatahistoribayar(no_transaksi)
{
    var strKriteriaKasirLAB = '';

    strKriteriaKasirLAB = 'no_transaksi= ~' + no_transaksi + '~';

    dsTRDetailHistoryList.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'tgl_transaksi',
                                    Sortdir: 'ASC',
                                    target: 'ViewHistoryBayar',
                                    param: strKriteriaKasirLAB
                                }
                    }
            );
    return dsTRDetailHistoryList;
}
;


//---------------------------------------------------------------------------------------///

function GetListCountPenataDetailTransaksi()
{

    var x = 0;
    for (var i = 0; i < dsTRDetailKasirLABList.getCount(); i++)
    {
        if (dsTRDetailKasirLABList.data.items[i].data.KD_PRODUK != '' || dsTRDetailKasirLABList.data.items[i].data.DESKRIPSI != '')
        {
            x += 1;
        }
        ;
    }
    return x;

}
;

//---------------------------------------------------------------------------------------///
function getParamDetailTransaksiKasirLAB()
{
    if (tampungtypedata === '')
    {
        tampungtypedata = 0;
    }
    ;

    var params =
            {
                kdUnit: vkd_unit,
                TrKodeTranskasi: Ext.get('txtNoTransaksiKasirLABKasir').getValue(),
                Tgl: Ext.get('dtpTanggalDetransaksi').dom.value,
                Shift: tampungshiftsekarang,
                kdKasir: kdkasir,
                bayar: tapungkd_pay,
                Flag: vflag,
                Typedata: tampungtypedata,
                Totalbayar: getTotalDetailProduk(),
                List: getArrDetailTrKasirLAB(),
                JmlField: mRecordKasirLAB.prototype.fields.length - 4,
                JmlList: GetListCountDetailTransaksi(),
                Hapus: 1,
                Ubah: 0
            };
    return params
}
;

function paramUpdateTransaksi()
{
    var params =
            {
                TrKodeTranskasi: cellSelectedtutup,
                KDkasir: kdkasir
            };
    return params
}
;

function GetListCountDetailTransaksi()
{

    var x = 0;
    for (var i = 0; i < dsTRDetailKasirLABKasirList.getCount(); i++)
    {
        if (dsTRDetailKasirLABKasirList.data.items[i].data.KD_PRODUK != '' || dsTRDetailKasirLABKasirList.data.items[i].data.DESKRIPSI != '')
        {
            x += 1;
        }
        ;
    }
    return x;

}
;

function getTotalDetailProduk()
{
    var TotalProduk = 0;
    var bayar;
    var tampunggrid;
    var x = '';
    for (var i = 0; i < dsTRDetailKasirLABKasirList.getCount(); i++)
    {

        var recordterakhir;

        //alert(TotalProduk);
        if (tampungtypedata == 0)
        {
            tampunggrid = parseInt(dsTRDetailKasirLABKasirList.data.items[i].data.BAYARTR);

            //recordterakhir= tampunggrid
            //TotalProduk.toString().replace(/./gi, "");
            TotalProduk += tampunggrid

            recordterakhir = TotalProduk
            Ext.get('txtJumlah2EditData_viKasirLAB').dom.value = formatCurrency(recordterakhir);
        }
        if (tampungtypedata == 3)
        {
            tampunggrid = parseInt(dsTRDetailKasirLABKasirList.data.items[i].data.PIUTANG);
            //TotalProduk.toString().replace(/./gi, "");
            //recordterakhir=tampunggrid
            TotalProduk += tampunggrid
            recordterakhir = TotalProduk
            Ext.get('txtJumlah2EditData_viKasirLAB').dom.value = formatCurrency(recordterakhir);
        }
        if (tampungtypedata == 1)
        {
            tampunggrid = parseInt(dsTRDetailKasirLABKasirList.data.items[i].data.DISCOUNT);

            TotalProduk += tampunggrid
            recordterakhir = TotalProduk
            Ext.get('txtJumlah2EditData_viKasirLAB').dom.value = formatCurrency(recordterakhir);
        }
    }
    bayar = Ext.get('txtJumlah2EditData_viKasirLAB').getValue();
    return bayar;
}
;







function getArrDetailTrKasirLAB()
{
    var x = '';
    for (var i = 0; i < dsTRDetailKasirLABKasirList.getCount(); i++)
    {
        if (dsTRDetailKasirLABKasirList.data.items[i].data.KD_PRODUK != '' && dsTRDetailKasirLABKasirList.data.items[i].data.DESKRIPSI != '')
        {
            var y = '';
            var z = '@@##$$@@';

            y = 'URUT=' + dsTRDetailKasirLABKasirList.data.items[i].data.URUT
            y += z + dsTRDetailKasirLABKasirList.data.items[i].data.KD_PRODUK
            y += z + dsTRDetailKasirLABKasirList.data.items[i].data.QTY
            y += z + dsTRDetailKasirLABKasirList.data.items[i].data.HARGA
            y += z + dsTRDetailKasirLABKasirList.data.items[i].data.KD_TARIF
            y += z + dsTRDetailKasirLABKasirList.data.items[i].data.URUT
            y += z + dsTRDetailKasirLABKasirList.data.items[i].data.BAYARTR
            y += z + dsTRDetailKasirLABKasirList.data.items[i].data.PIUTANG
            y += z + dsTRDetailKasirLABKasirList.data.items[i].data.DISCOUNT



            if (i === (dsTRDetailKasirLABKasirList.getCount() - 1))
            {
                x += y
            } else
            {
                x += y + '##[[]]##'
            }
            ;
        }
        ;
    }

    return x;
}
;


function getItemPanelInputKasir(lebar)
{
    var items =
            {
                layout: 'fit',
                anchor: '100%',
                width: lebar - 35,
                labelAlign: 'right',
                bodyStyle: 'padding:10px 10px 10px 0px',
                border: true,
                height: 110,
                items:
                        [
                            {
                                columnWidth: .9,
                                width: lebar - 35,
                                labelWidth: 100,
                                layout: 'form',
                                border: false,
                                items:
                                        [
                                            getItemPanelNoTransksiLABKasir(lebar),
                                            getItemPanelmedreckasir(lebar), getItemPanelUnitKasir(lebar)
                                        ]
                            }
                        ]
            };
    return items;
}
;



function getItemPanelUnitKasir(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .40,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            mComboJenisByrView()
                                        ]
                            }, {
                                columnWidth: .60,
                                layout: 'form',
                                labelWidth: 0.9,
                                border: false,
                                items:
                                        [
                                            mComboPembayaran()
                                        ]
                            }
                        ]
            }
    return items;
}
;



function loaddatastorePembayaran(jenis_pay)
{
    dsComboBayar.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'nama',
                                    Sortdir: 'ASC',
                                    target: 'ViewComboBayar',
                                    param: 'jenis_pay=~' + jenis_pay + '~'
                                }
                    }
            )
}

function mComboPembayaran()
{
    var Field = ['KD_PAY', 'JENIS_PAY', 'PAYMENT'];

    dsComboBayar = new WebApp.DataStore({fields: Field});

    var cboPembayaran = new Ext.form.ComboBox
            (
                    {
                        id: 'cboPembayaran',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Pilih Pembayaran...',
                        labelWidth: 80,
                        align: 'Right',
                        store: dsComboBayar,
                        valueField: 'KD_PAY',
                        displayField: 'PAYMENT',
                        anchor: '100%',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        tapungkd_pay = b.data.KD_PAY;
                                    },
                                }
                    }
            );

    return cboPembayaran;
}
;


function mComboJenisByrView()
{
    var Field = ['JENIS_PAY', 'DESKRIPSI', 'TYPE_DATA'];

    dsJenisbyrView = new WebApp.DataStore({fields: Field});
    dsJenisbyrView.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'jenis_pay',
                                    Sortdir: 'ASC',
                                    target: 'ComboJenis',
                                }
                    }
            );

    var cboJenisByr = new Ext.form.ComboBox
            (
                    {
                        id: 'cboJenisByr',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        emptyText: '',
                        fieldLabel: 'Pembayaran      ',
                        align: 'Right',
                        anchor: '100%',
                        store: dsJenisbyrView,
                        valueField: 'JENIS_PAY',
                        displayField: 'DESKRIPSI',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {

                                        loaddatastorePembayaran(b.data.JENIS_PAY);
                                        tampungtypedata = b.data.TYPE_DATA;
                                        jenispay = b.data.JENIS_PAY;
                                        showCols(Ext.getCmp('gridDTLTRKasirLAB'));
                                        hideCols(Ext.getCmp('gridDTLTRKasirLAB'));
                                        getTotalDetailProduk();
                                        Ext.get('cboPembayaran').dom.value = 'Pilih Pembayaran...';
                                    },
                                }
                    }
            );

    return cboJenisByr;
}
;
function hideCols(grid)
{
    if (tampungtypedata == 3)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('BAYARTR'), true);
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('DISCOUNT'), true);
    } else if (tampungtypedata == 0)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('PIUTANG'), true);
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('DISCOUNT'), true);
    } else if (tampungtypedata == 1)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('PIUTANG'), true);
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('BAYARTR'), true);

    }
}
;
function showCols(grid) {
    if (tampungtypedata == 3)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('PIUTANG'), false);

    } else if (tampungtypedata == 0)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('BAYARTR'), false);
    } else if (tampungtypedata == 1)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('DISCOUNT'), false);
    }

}
;



function getItemPanelDokter(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .40,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Dokter  ',
                                                name: 'txtKdDokter',
                                                id: 'txtKdDokter',
                                                readOnly: true,
                                                anchor: '99%'
                                            }, {
                                                xtype: 'textfield',
                                                fieldLabel: 'Kelompok Pasien',
                                                readOnly: true,
                                                name: 'txtCustomer',
                                                id: 'txtCustomer',
                                                anchor: '99%',
                                                listeners:
                                                        {
                                                        }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .600,
                                layout: 'form',
                                border: false,
                                labelWidth: 2,
                                items:
                                        [
                                            {
                                                xtype: 'textfield',
                                                name: 'txtNamaDokter',
                                                id: 'txtNamaDokter',
                                                readOnly: true,
                                                anchor: '100%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                name: 'txtKdUrutMasuk',
                                                id: 'txtKdUrutMasuk',
                                                readOnly: true,
                                                hidden: true,
                                                anchor: '100%'
                                            }
                                        ]
                            }
                        ]
            }
    return items;
}
;

function getItemPanelNoTransksiLABKasir(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .40,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'No. Transaksi ',
                                                name: 'txtNoTransaksiKasirLABKasir',
                                                id: 'txtNoTransaksiKasirLABKasir',
                                                emptyText: nmNomorOtomatis,
                                                readOnly: true,
                                                anchor: '99%'
                                            }
                                        ]
                            },
                            {
                                columnWidth: .60,
                                layout: 'form',
                                border: false,
                                labelWidth: 55,
                                items:
                                        [
                                            {
                                                xtype: 'datefield',
                                                fieldLabel: 'Tanggal ',
                                                id: 'dtpTanggalDetransaksi',
                                                name: 'dtpTanggalDetransaksi',
                                                format: 'd/M/Y',
                                                readOnly: true,
                                                value: now,
                                                anchor: '100%'
                                            }
                                        ]
                            }
                        ]
            }
    return items;
}
;


function getItemPanelmedreckasir(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .40,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'No. Medrec ',
                                                name: 'txtNoMedrecDetransaksi',
                                                id: 'txtNoMedrecDetransaksi',
                                                readOnly: true,
                                                anchor: '99%',
                                                listeners:
                                                        {
                                                        }
                                            }
                                        ]
                            },
                            {
                                columnWidth: .60,
                                layout: 'form',
                                border: false,
                                labelWidth: 2,
                                items:
                                        [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: '',
                                                //hideLabel:true,
                                                readOnly: true,
                                                name: 'txtNamaPasienDetransaksi',
                                                id: 'txtNamaPasienDetransaksi',
                                                anchor: '100%',
                                                listeners:
                                                        {
                                                        }
                                            }
                                        ]
                            }
                        ]
            }
    return items;
}
;


function RefreshDataKasirLABKasir()
{
    dsTRKasirLABKasirList.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: selectCountKasirLABKasir,
                                    Sort: 'tgl_transaksi',
                                    //Sort: 'tgl_transaksi',
                                    Sortdir: 'ASC',
                                    target: 'ViewKasirLAB',
                                    param: ''
                                }
                    }
            );

    rowSelectedKasirLABKasir = undefined;
    return dsTRKasirLABKasirList;
}
;

function refeshKasirLABKasir()
{
    dsTRKasirLABKasirList.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: selectCountKasirLABKasir,
                                    //Sort: 'no_transaksi',
                                    Sort: '',
                                    //Sort: 'no_transaksi',
                                    Sortdir: 'ASC',
                                    target: 'ViewKasirLAB',
                                    param: ''
                                }
                    }
            );
    return dsTRKasirLABKasirList;
}

function RefreshDataFilterKasirLABKasir()
{

    var KataKunci = '';

    if (Ext.getCmp('txtFilterKasirNomedrecLab').getValue() != '')
    {
        if (KataKunci == '')
        {
            KataKunci = '   LOWER(kode_pasien) like  LOWER( ~' + Ext.getCmp('txtFilterKasirNomedrecLab').getValue() + '%~)';
        } else
        {
            KataKunci += ' and  LOWER(kode_pasien) like  LOWER( ~' + Ext.getCmp('txtFilterKasirNomedrecLab').getValue() + '%~)';
        }
        ;

    }
    ;
    if (KataKunci == '')
    {
        //alert ('');
        KataKunci = '   kd_bagian = 4';
    } else
    {
        KataKunci += ' and  kd_bagian = 4';
    }
    ;

    if (Ext.getCmp('TxtFilterGridDataView_NAMA_viKasirLABKasir').getValue() != '')
    {
        if (KataKunci == '')
        {
            KataKunci = '  LOWER(nama) like  LOWER( ~' + Ext.getCmp('TxtFilterGridDataView_NAMA_viKasirLABKasir').getValue() + '%~)';
        } else
        {
            KataKunci += ' and  LOWER(nama) like  LOWER( ~' + Ext.getCmp('TxtFilterGridDataView_NAMA_viKasirLABKasir').getValue() + '%~)';
        }
        ;

    }
    ;


    if (Ext.getCmp('cboUNIT_viKasirLABKasir').getValue() != '' && Ext.getCmp('cboUNIT_viKasirLABKasir').getValue() != 'All' && Ext.getCmp('cboUNIT_viKasirLABKasir').getValue() != 'Semua')
    {
        if (KataKunci == '')
        {
            KataKunci = '  LOWER(nama_unit)like  LOWER(~%' + Ext.get('cboUNIT_viKasirLABKasir').getValue() + '%~)';
        } else
        {
            KataKunci += ' and LOWER(nama_unit) like  LOWER(~%' + Ext.get('cboUNIT_viKasirLABKasir').getValue() + '%~)';
        }
        ;
    }
    ;


    if (Ext.get('cboStatus_viKasirLABKasir').getValue() == 'Lunas' || Ext.getCmp('cboStatus_viKasirLABKasir').getValue() == 2)
    {
        if (KataKunci == '')
        {
            KataKunci = '   lunas = ~true~';
        } else
        {
            KataKunci += ' and lunas =  ~true~';
        }
        ;

    }
    ;


    if (Ext.get('cboStatus_viKasirLABKasir').getValue() == 'Semua' || Ext.getCmp('cboStatus_viKasirLABKasir').getValue() == 1)
    {
    }
    ;
    if (Ext.get('cboStatus_viKasirLABKasir').getValue() == 'Belum Lunas' || Ext.getCmp('cboStatus_viKasirLABKasir').getValue() == 3)
    {
        if (KataKunci == '')
        {

            KataKunci = '   lunas = ~false~';
        } else
        {

            KataKunci += ' and lunas =  ~false~';
        }
        ;


    }
    ;
    if (Ext.get('dtpTglAwalFilterKasirLAB').getValue() != '')
    {
        if (KataKunci == '')
        {
            KataKunci = " (tgl_transaksi between ~" + Ext.get('dtpTglAwalFilterKasirLAB').getValue() + "~ and ~" + Ext.get('dtpTglAkhirFilterKasirLAB').getValue() + "~)";
        } else
        {
            KataKunci += " and (tgl_transaksi between ~" + Ext.get('dtpTglAwalFilterKasirLAB').getValue() + "~ and ~" + Ext.get('dtpTglAkhirFilterKasirLAB').getValue() + "~)";
        }
        ;

    }
    ;

    if (KataKunci != undefined || KataKunci != '')
    {
        dsTRKasirLABKasirList.load
                (
                        {
                            params:
                                    {
                                        Skip: 0,
                                        Take: selectCountKasirLABKasir,
                                        //Sort: 'no_transaksi',
                                        Sort: 'tgl_transaksi',
                                        //Sort: 'no_transaksi',
                                        Sortdir: 'ASC',
                                        target: 'ViewKasirLAB',
                                        param: KataKunci
                                    }
                        }
                );
    } else
    {

        dsTRKasirLABKasirList.load
                (
                        {
                            params:
                                    {
                                        Skip: 0,
                                        Take: selectCountKasirLABKasir,
                                        //Sort: 'no_transaksi',
                                        Sort: 'tgl_transaksi',
                                        //Sort: 'no_transaksi',
                                        Sortdir: 'ASC',
                                        target: 'ViewKasirLAB',
                                        param: ''
                                    }
                        }
                );
    }
    ;

    return dsTRKasirLABKasirList;
}
;



function Datasave_KasirLABKasir(mBol)
{
    if (ValidasiEntryCMKasirLAB(nmHeaderSimpanData, false) == 1)
    {
        Ext.Ajax.request
                (
                        {
                            //url: "./Datapool.mvc/CreateDataObj",
                            url: baseURL + "index.php/main/functionKasirPenunjang/savepembayaran",
                            params: getParamDetailTransaksiKasirLAB(),
                            failure: function (o)
                            {
                                ShowPesanWarningKasirLAB('Data Belum Simpan segera Hubungi Admin', 'Gagal');
                                RefreshDataFilterKasirLABKasir();
                            },
                            success: function (o)
                            {
                                RefreshDatahistoribayar('0');
                                RefreshDataKasirLABDetail('0');
                                var cst = Ext.decode(o.responseText);
                                if (cst.success === true)
                                {
                                    ShowPesanInfoKasirLAB(nmPesanSimpanSukses, nmHeaderSimpanData);
                                    RefreshDataKasirLABKasirDetail(Ext.get('txtNoTransaksiKasirLABKasir').dom.value);
                                    //RefreshDataKasirLABKasir();
                                    if (mBol === false)
                                    {
                                        RefreshDataFilterKasirLABKasir();
                                    }
                                    ;
                                } else
                                {
                                    ShowPesanWarningKasirLAB('Data Belum Simpan segera Hubungi Admin', 'Gagal');
                                }
                                ;
                            }
                        }
                )

    } else
    {
        if (mBol === true)
        {
            return false;
        }
        ;
    }
    ;

}
;
function UpdateTutuptransaksi(mBol)
{
    Ext.Ajax.request
            (
                    {
                        //url: "./Datapool.mvc/CreateDataObj",
                        url: baseURL + "index.php/main/functionKasirPenunjang/ubah_co_status_transksi",
                        params: paramUpdateTransaksi(),
                        failure: function (o)
                        {
                            ShowPesanWarningKasirLAB('Data gagal tersimpan segera hubungi Admin', 'Gagal');
                            RefreshDataFilterKasirLABKasir();
                        },
                        success: function (o)
                        {
                            //RefreshDatahistoribayar(Ext.get('cellSelectedtutup);

                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                ShowPesanInfoKasirLAB(nmPesanSimpanSukses, nmHeaderSimpanData);

                                //RefreshDataKasirLABKasir();
                                if (mBol === false)
                                {
                                    RefreshDataFilterKasirLABKasir();
                                }
                                ;
                                cellSelectedtutup = '';
                            } else
                            {
                                ShowPesanWarningKasirLAB('Data gagal tersimpan segera hubungi Admin', 'Gagal');
                                cellSelectedtutup = '';
                            }
                            ;
                        }
                    }
            )
}
;

function ValidasiEntryCMKasirLAB(modul, mBolHapus)
{
    var x = 1;

    if ((Ext.get('txtNoTransaksiKasirLABKasir').getValue() == '')

            || (Ext.get('txtNoMedrecDetransaksi').getValue() == '')
            || (Ext.get('txtNamaPasienDetransaksi').getValue() == '')
            || (Ext.get('dtpTanggalDetransaksi').getValue() == '')
            || dsTRDetailKasirLABKasirList.getCount() === 0
            || (Ext.get('cboPembayaran').getValue() == '')
            || (Ext.get('cboPembayaran').getValue() == 'Pilih Pembayaran...'))
    {
        if (Ext.get('txtNoTransaksiKasirLABKasir').getValue() == '' && mBolHapus === true)
        {
            x = 0;
        } else if (Ext.get('cboPembayaran').getValue() == '' || Ext.get('cboPembayaran').getValue() == 'Pilih Pembayaran...')
        {
            ShowPesanWarningKasirLAB(('Data pembayaran tidak  boleh kosong'), modul);
            x = 0;
        } else if (Ext.get('txtNoMedrecDetransaksi').getValue() == '')
        {
            ShowPesanWarningKasirLAB(('Data no. medrec tidak boleh kosong'), modul);
            x = 0;
        } else if (Ext.get('txtNamaPasienDetransaksi').getValue() == '')
        {
            ShowPesanWarningKasirLAB('Nama pasien belum terisi', modul);
            x = 0;
        } else if (Ext.get('dtpTanggalDetransaksi').getValue() == '')
        {
            ShowPesanWarningKasirLAB(('Data tanggal kunjungan tidak boleh kosong'), modul);
            x = 0;
        }
        //cboPembayaran

        else if (dsTRDetailKasirLABKasirList.getCount() === 0)
        {
            ShowPesanWarningKasirLAB('Data dalam tabel kosong', modul);
            x = 0;
        }
        ;
    }
    ;
    return x;
}
;




function ShowPesanWarningKasirLAB(str, modul)
{
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.WARNING,
                        width: 250
                    }
            );
}
;

function ShowPesanErrorKasirLAB(str, modul) {
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.ERROR,
                        width: 250
                    }
            );
}
;

function ShowPesanInfoKasirLAB(str, modul) {
    Ext.MessageBox.show
            (
                    {
                        title: modul,
                        msg: str,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO,
                        width: 250
                    }
            );
}
;

function printbillRadLab()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/CreateDataObj",
                        params: dataparamcetakbill_vikasirDaftarRadLab(),
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {

                            } else if (cst.success === false && cst.pesan === 0)
                            {
                                ShowPesanWarning_viDaftar('Data tidak berhasil di simpan ' + cst.pesan, 'Simpan Data');
                            } else
                            {
                                ShowPesanError_viDaftar('Data tidak berhasil di simpan ' + cst.pesan, 'Simpan Data');
                            }
                        }
                    }
            );
}
;

var tmpkdkasirRadLab = '1';
function dataparamcetakbill_vikasirDaftarRadLab()
{
    var paramscetakbill_vikasirDaftarRadLab =
            {
                Table: 'DirectPrintingRadLab',
                No_TRans: Ext.get('txtNoTransaksiKasirLABKasir').getValue(),
                KdKasir: tmpkdkasirRadLab,
                modul: 'lab'
            };
    return paramscetakbill_vikasirDaftarRadLab;
}
;

function printkwitansiRadLab()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/CreateDataObj",
                        params: dataparamcetakkwitansi_vikasirDaftarRadLab(),
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {

                            } else if (cst.success === false && cst.pesan !== '')
                            {
                                ShowPesanWarningKasirrwj('tidak berhasil melakukan pencetakan ' + cst.pesan, 'Cetak Data');
                            } else
                            {
                                ShowPesanWarningKasirrwj('tidak berhasil melakukan pencetakan ' + cst.pesan, 'Cetak Data');
                            }
                        }
                    }
            );
}
;

function dataparamcetakkwitansi_vikasirDaftarRadLab()
{
    var paramscetakbill_vikasirDaftarRadLab =
            {
                Table: 'DirectKwitansiLab',
                No_TRans: Ext.get('txtNoTransaksiKasirLABKasir').getValue(),
            };
    return paramscetakbill_vikasirDaftarRadLab;
}
;

