var TrPenJasLab={};
TrPenJasLab.form={};
TrPenJasLab.func={};
TrPenJasLab.vars={};
TrPenJasLab.func.parent=TrPenJasLab;
TrPenJasLab.form.ArrayStore={};
TrPenJasLab.form.ComboBox={};
TrPenJasLab.form.DataStore={};
TrPenJasLab.form.Record={};
TrPenJasLab.form.Form={};
TrPenJasLab.form.Grid={};
TrPenJasLab.form.Panel={};
TrPenJasLab.form.TextField={};
TrPenJasLab.form.Button={};
var cellCurrentLab='';
var dspasienorder_mng;
var kodeUnitLab;
var cbopasienorder_mng;
var  checkColumn_penata__lab;
var dsGetdetail_order;
var gridGetdetail_order;
var  var_tampung_lab_order=false;
var dsTrDokter	= new WebApp.DataStore({ fields: ['KD_DOKTER','NAMA','KD_JOB','KD_PRODUK'] });
var dataTrDokter = [];

TrPenJasLab.form.ArrayStore.produk=new Ext.data.ArrayStore({
		id: 0,
		fields: [
					'uraian','kd_tarif','kd_produk','tgl_transaksi','tgl_berlaku','harga','qty'
				],
		data: []
	});

TrPenJasLab.form.ArrayStore.kodepasien=new Ext.data.ArrayStore({
		id: 0,
		fields: [
					'no_transaksi','kd_kasir','tgl_transaksi','kd_spesial','kamar','tgl_lahir','kd_pasien','nama','nama_unit_asli',
					'alamat','kd_dokter','kd_unit','nama_unit','urut_masuk','dokter','Jenis_kelamin','gol_darah','kd_customer','customer',
					'no_kamar','kd_spesial','tgl','kd_tarif','kelpasien'
				],
		data: []
	});

TrPenJasLab.form.ArrayStore.pasien=new Ext.data.ArrayStore({
		id: 0,
		fields: [
					'no_transaksi','kd_kasir','tgl_transaksi','kd_spesial','kamar','tgl_lahir','kd_pasien','nama','nama_unit_asli',
					'alamat','kd_dokter','kd_unit','nama_unit','urut_masuk','dokter','Jenis_kelamin','gol_darah','kd_customer','customer',
					'no_kamar','kd_spesial','tgl','kd_tarif','kelpasien'
				],
		data: []
	});

TrPenJasLab.form.ArrayStore.notransaksi=new Ext.data.ArrayStore({
		id: 0,
		fields: [
					'no_transaksi','kd_kasir','tgl_transaksi','kd_spesial','kamar','tgl_lahir','kd_pasien','nama','nama_unit_asli',
					'alamat','kd_dokter','kd_unit','nama_unit','urut_masuk','dokter','Jenis_kelamin','gol_darah','kd_customer','customer',
					'no_kamar','kd_spesial','tgl','kd_tarif','kelpasien'
				],
		data: []
	});

var CurrentPenJasLab =
{
    data: Object,
    details: Array,
    row: 0
};

var tanggaltransaksitampung;
var mRecordRwj = Ext.data.Record.create
(
    [
       {name: 'DESKRIPSI2', mapping:'DESKRIPSI2'},
       {name: 'KD_PRODUK', mapping:'KD_PRODUK'},
       {name: 'DESKRIPSI', mapping:'DESKRIPSI'},
       {name: 'KD_TARIF', mapping:'KD_TARIF'},
       {name: 'HARGA', mapping:'HARGA'},
       {name: 'QTY', mapping:'QTY'},
       {name: 'TGL_TRANSAKSI', mapping:'TGL_TRANSAKSI'},
       {name: 'DESC_REQ', mapping:'DESC_REQ'},
      // {name: 'KD_TARIF', mapping:'KD_TARIF'},
       {name: 'URUT', mapping:'URUT'}
    ]
);
var CurrentDiagnosa =
{
    data: Object,
    details: Array,
    row: 0
};

var FormLookUpGantidokter;
var mRecordDiagnosa = Ext.data.Record.create
(
    [
       {name: 'KASUS', mapping:'KASUS'},
       {name: 'KD_PENYAKIT', mapping:'KD_PENYAKIT'},
       {name: 'PENYAKIT', mapping:'PENYAKIT'},
       //{name: 'KD_TARIF', mapping:'KD_TARIF'},
      // {name: 'HARGA', mapping:'HARGA'},
       {name: 'STAT_DIAG', mapping:'STAT_DIAG'},
       {name: 'TGL_TRANSAKSI', mapping:'TGL_TRANSAKSI'},
      // {name: 'DESC_REQ', mapping:'DESC_REQ'},
      // {name: 'KD_TARIF', mapping:'KD_TARIF'},
       {name: 'URUT_MASUK', mapping:'URUT_MASUK'}
    ]
);

var now = new Date();
var rowSelectedDiagnosa;
var cellSelecteddeskripsi;
var nowTglTransaksi = new Date();
var nowTglTransaksiGrid = new Date();
var tglGridBawah = nowTglTransaksiGrid.format("d/M/Y");
var tigaharilalu = new Date().add(Date.DAY, -3);

var selectSetGDarahLab;
var selectSetJKLab;
var selectSetKelPasienLab;
var selectSetPerseoranganLab;
var selectSetPerusahaanLab;
var selectSetAsuransiLab;

var dsTRDetailPenJasLabList;
var TmpNotransaksi='';
var KdKasirAsal='';
var TglTransaksiAsal='';
var databaru = 0;
var No_Kamar='';
var Kd_Spesial=0;

var gridDTItemTest;
var grListPenJasLab;
var tampungshiftsekarang;

//var FocusCtrlCMRWJ;
var vkode_customer;
CurrentPage.page = getPanelPenJasLab(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
viewGridOrderAll_RASEPRWI();


//membuat form
function getPanelPenJasLab(mod_id)
{

	var i = setInterval(function(){
	total_pasien_order_mng();
	load_data_pasienorder();
	viewGridOrderAll_RASEPRWI();
	}, 150000);

    var FormDepanPenJasLab = new Ext.Panel
    (
        {
            id: mod_id,
            closable: true,
            region: 'center',
            layout: 'form',
            title: 'Penata Jasa Laboratorium',
            border: false,
            shadhow: true,
            autoScroll:false,
            iconCls: 'Request',
            margins: '0 5 5 0',
            items: [
						getItemPanelInputPenJasLab(),
						getFormEntryPenJasLab()
                   ],
			tbar:
            [
				'-',
				{
                        text: 'Add new',
                        id: 'btnAddPenJasLab',
                        iconCls: 'add',
                        handler: function()
						{
							addNewData();

						}
				},
				'-',
				{
                        text: 'Save',
                        id: 'btnSimpanPenJasLab',
                        tooltip: nmSimpan,
                        iconCls: 'save',
						disabled:true,
                        handler: function()
						{
							//loadMask.show();
							Datasave_PenJasLab(false);
							//alert(Ext.getCmp('cboPerseoranganLab').getValue());
						}
				},
				{
                        text: 'Save',
                        id: 'btnSimpanPenJasLab_kiriman',
                        tooltip: nmSimpan,
                        iconCls: 'save',
						hidden : true,
						//disabled:true,
                        handler: function()
						{Dataupdate_order_PenJasLab();
							//loadMask.show();
							//Datasave_PenJasLab(false);
							//alert(Ext.getCmp('cboPerseoranganLab').getValue());
						}
				},
				'-',
				{
                        text: 'Find',
                        id: 'btnCariPenJasLab',
                        iconCls: 'find',
						hidden:true,
                        handler: function()
						{

						}
				},
				

			],
            listeners:
            {
                'afterrender': function()
                {
					Ext.Ajax.request(
					{
						url: baseURL + "index.php/main/functionLAB/getCurrentShiftLab",
						params: {
							command: '0',
							// parameter untuk url yang dituju (fungsi didalam controller)
						},
						failure: function(o)
						{
							var cst = Ext.decode(o.responseText);
						},
						success: function(o) {
							tampungshiftsekarang=o.responseText;
						}
					});
				},
				 'beforeclose': function(){clearInterval(i);}


            }
        }
    );

   return FormDepanPenJasLab;

};


function load_data_pasienorder(param)
{
	//console.log(param);
	if (param==='' || param===undefined)
	{
		param={
			text: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
		};
	}
	Ext.Ajax.request({
		url: baseURL + "index.php/main/functionLABPoliklinik/getPasien",
		params: param,
		failure: function(o)
		{
			var cst = Ext.decode(o.responseText);
		},
		success: function(o) {
			cbopasienorder_mng.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dspasienorder_mng.recordType;
				var o=cst['listData'][i];

				recs.push(new recType(o));
				dspasienorder_mng.add(recs);
				//console.log(o);
			}
		}
	});
}

function total_pasien_order_mng()
{
	Ext.Ajax.request({
		url: baseURL + "index.php/main/functionLABPoliklinik/gettotalpasienorder_mng",
		params: {
			command: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
		},
		failure: function(o)
		{
			var cst = Ext.decode(o.responseText);
		},
		success: function(o) {
			var cst = Ext.decode(o.responseText);

			Ext.getCmp('txtcounttr').setValue(cst.jumlahpasien);
			//console.log(cst);

		}
	});
};

function getFormEntryPenJasLab(lebar,data)
{

 var GDtabDetailPenJasLab = new Ext.TabPanel
    (
        {
			id:'GDtabDetailPenJasLab',
			region: 'center',
			activeTab: 0,
			height:380,
			//autoHeight:true,
			border:false,
			plain: true,
			defaults:{autoScroll: true},
			items:
			[
				grid_lab_satu()//Panel tab detail transaksi
				//-------------- ## --------------
			],tbar:
			[
				
				
				{
					text: 'Tambah Item Produk',
					id: 'btnLookupItemPemeriksaan',
					tooltip: nmLookup,
					iconCls: 'Edit_Tr',
					disabled:true,
					handler: function()
					{

						/* var p = RecordBaruRWJ();
						var str='';
						if (Ext.get('txtKdUnitLab').dom.value  != undefined && Ext.get('txtKdUnitLab').dom.value  != '')
						{
							str =  '41';
						}else{
							str =  '41';
						}
						//FormLookSettingPasswordMainPageSettingPasswordMainPageSettingPasswordMainPageupKasirRWJ(str, dsTRDetailPenJasLabList, p, true, '', true);
						FormLookupKasirRWJ(str,TrPenJasLab.vars.kd_tarif, dsTRDetailPenJasLabList, p, true, '', true);
						 */
						var records = new Array();
						records.push(new dsTRDetailPenJasLabList.recordType());
						dsTRDetailPenJasLabList.add(records);
					}
				},
				{
					id:'btnHpsBrsItemLabL',
					text: 'Hapus Baris',
					tooltip: 'Hapus Baris',
					disabled:false,
					iconCls: 'RemoveRow',
					handler: function()
					{
						if (dsTRDetailPenJasLabList.getCount() > 0 ) {
							if (cellSelecteddeskripsi != undefined)
							{
								Ext.Msg.confirm('Warning', 'Apakah item pemeriksaan ini akan dihapus?', function(button){
									if (button == 'yes'){
										if(CurrentPenJasLab != undefined) {
											var line = gridDTItemTest.getSelectionModel().selection.cell[0];
											dsTRDetailPenJasLabList.removeAt(line);
											gridDTItemTest.getView().refresh();
										}
									}
								})
							} else {
								ShowPesanWarningPenJasLab('Pilih record ','Hapus data');
							}
						}
					}
				},{
					xtype: 'checkbox',
					id: 'chkWithTglRequest',
					 text: 'Pilih Bacon',
					hideLabel:false,
					checked: false,
					handler: function()
					  {
						if (this.getValue()===true)
						{
						
								for (var i = 0; i < dsTRDetailPenJasLabList.getCount(); i++) 
								{
								
								
								dsTRDetailPenJasLabList.data.items[i].data.cito='Ya';
								

										
								}
								
								gridDTItemTest.getView().refresh();
						}
						else
						{
								for (var i = 0; i < dsTRDetailPenJasLabList.getCount(); i++) 
								{
								
								
								dsTRDetailPenJasLabList.data.items[i].data.cito='Tidak';
								

										
								}
								gridDTItemTest.getView().refresh();
						}
					  }
                },{ 
					xtype : 'tbtext', 
					text  : 'CITO ',
					cls   : 'left-label',
					style : {
						'font-weight':'bold',
						  'font-style': 'italic',
							'color' : 'red'
						},			
					width: 90
				  }

			],
        }

    );

    return GDtabDetailPenJasLab
};

function viewGridOrderAll_RASEPRWI(nama){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/main/functionLABPoliklinik/getPasien",
			params: {text:nama},
			failure: function(o)
			{
				ShowPesanWarningPenJasLab('Hubungi Admin', 'Error');
			},
			success: function(o)
			{   dsGetdetail_order.removeAll();


				var cst = Ext.decode(o.responseText);

				if (cst.processResult === 'SUCCESS')
				{


					var recs=[],
					recType=dsGetdetail_order.recordType;

					for(var i=0; i<cst.listData.length; i++){

						recs.push(new recType(cst.listData[i]));


					}
					dsGetdetail_order.add(recs);

					console.log(dsGetdetail_order);
					gridGetdetail_order.getView().refresh();

				}
				else
				{
					ShowPesanWarningPenJasLab('Gagal membaca Data ini', 'Error');
				};
			}
		}

	);
}
function Getdetail_order()
{
    dsGetdetail_order = new WebApp.DataStore({ fields: ['kd_pasien','nama']});



    gridGetdetail_order = new Ext.grid.EditorGridPanel
    (
        {
            title: 'Order Unit Lain',
            stripeRows: true,
            store: dsGetdetail_order,
			height:140,
			anchor: '100%',
            border: false,
            columnLines: true,
			frame: false,
            autoScroll:true,
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                            cellSelecteddeskripsi = dsGetdetail_order.getAt(row);
                            CurrentPenJasLab.row = row;
                            CurrentPenJasLab.data = cellSelecteddeskripsi;
							var_tampung_lab_order=true;
							ViewGridBawahPenjasLab(cellSelecteddeskripsi.data.no_transaksi,cellSelecteddeskripsi.data);
									TrPenJasLab.form.ComboBox.kdpasien.disable();
									TrPenJasLab.form.ComboBox.kdpasien.setValue(cellSelecteddeskripsi.data.kd_pasien);
									TrPenJasLab.form.ComboBox.pasien.disable();
									TrPenJasLab.form.ComboBox.pasien.setValue(cellSelecteddeskripsi.data.nama);
									TrPenJasLab.form.ComboBox.notransaksi.setValue(cellSelecteddeskripsi.data.no_transaksi);
									TrPenJasLab.form.ComboBox.notransaksi.setReadOnly(true);


									Ext.getCmp('btnLookupItemPemeriksaan').disable();
									Ext.getCmp('btnHpsBrsItemLabL').enable();
									//Ext.getCmp('btnHpsBrsItemLabL').disable();
									Ext.getCmp('btnSimpanPenJasLab').disable();
									//Ext.getCmp('btnSimpanPenJasLab_kiriman').show();
									TrPenJasLab.vars.kd_tarif=cellSelecteddeskripsi.data.kd_tarif;

									Ext.Ajax.request(
									{
										url: baseURL + "index.php/main/functionLAB/getCurrentShiftLab",
										params: {
											command: '0',
											// parameter untuk url yang dituju (fungsi didalam controller)
										},
										failure: function(o)
										{
											var cst = Ext.decode(o.responseText);
										},
										success: function(o) {
											tampungshiftsekarang=o.responseText;
										}
									});
                        }
                    }
                }
            ),
            cm: new Ext.grid.ColumnModel
				(
					[
						new Ext.grid.RowNumberer(),
						{
							header: 'Medrec',
							dataIndex: 'kd_pasien',
							width:250,
							menuDisabled:true,
							hidden :false

						},{
							header: 'Nama',
							dataIndex: 'nama',
							width:250,
							menuDisabled:true,
							hidden :false
						 },{
							header: 'alamat',
							dataIndex: 'alamat',
							width:250,
							menuDisabled:true,
							hidden :false
						 },{
							header: 'Asal Pasien',
							dataIndex: 'nama_unit',
							width:250,
							menuDisabled:true,
							hidden :false

						}


					   ]
				),
				listeners: {
							rowclick: function( $this, rowIndex, e )
							{
								cellCurrentLab = rowIndex;
								//console.log(cellCurrentLab);
							},
							celldblclick: function(gridView,htmlElement,columnIndex,dataRecord)
							{

							}
						},
				tbar :[
				{
					xtype: 'label',
					text: 'Order Unit lain : ' ,
					style : {
						color : 'red'
					}
				},
				{xtype: 'tbspacer',height: 3, width:5},
				 {
					x: 40,
					y: 40,
					xtype: 'textfield',
					fieldLabel: 'No. Medrec',
					name: 'txtcounttr',
					id: 'txtcounttr',
					width: 50,
					disabled:true,
					style : {
						color : 'red'
					},
					listeners:
					{

					}
				},mComboorder(),
				{xtype: 'tbspacer',height: 3, width:30},
				{
					x: 290,
					y: 100,
					xtype: 'button',
					text: 'Refresh Order',
					iconCls: 'refresh',
					hideLabel: false,
					handler: function(sm, row, rec) {
						total_pasien_order_mng()
					}
				}],
        }


    );

    return gridGetdetail_order;
};




function GetDTGridPenJasLab()
{
    var fldDetail = [];

    dsTRDetailPenJasLabList = new WebApp.DataStore({ fields: fldDetail })
	ViewGridBawahLookupPenjasLab() ;
    gridDTItemTest = new Ext.grid.EditorGridPanel
    (
        {
            title: '',
            stripeRows: true,
            store: dsTRDetailPenJasLabList,
            border: true,
			height:150,
            columnLines: true,
            frame: true,
            autoScroll:true,
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                            cellSelecteddeskripsi = dsTRDetailPenJasLabList.getAt(row);
                            CurrentPenJasLab.row = row;
                            CurrentPenJasLab.data = cellSelecteddeskripsi;
                        }
                    }
                }
            ), 
			plugins: [checkColumn_penata__lab],
            cm: TRPenJasLabColumModel(),
			listeners: {
						rowclick: function( $this, rowIndex, e )
						{
						cellCurrentLab = rowIndex;
						},
						celldblclick: function(gridView,htmlElement,columnIndex,dataRecord){
						//console.log(dsTRDetailPenJasLabList);
						var NoTrans =  Ext.getCmp('nci-gen1').getValue();
						var TglTrans =  Ext.get('dtpKunjunganL').dom.value;
						var kdPrdk = dsTRDetailPenJasLabList.data.items[cellCurrentLab].data.kd_produk;
						var kdTrf = dsTRDetailPenJasLabList.data.items[cellCurrentLab].data.kd_tarif;
						var tglBerlaku = dsTRDetailPenJasLabList.data.items[cellCurrentLab].data.tgl_berlaku;
						var trf = dsTRDetailPenJasLabList.data.items[cellCurrentLab].data.harga;
							/* if(columnIndex == 8 && dsTRDetailPenJasLabList.data.items[cellCurrentLab].data.jumlah == 'Ada'){
								PilihDokterLookUp(NoTrans,TglTrans,kdPrdk,kdTrf,tglBerlaku,trf);
							}
							else if(columnIndex == 8 && dsTRDetailPenJasLabList.data.items[cellCurrentLab].data.JUMLAH == undefined)
							{
								ShowPesanInfoPenJasLab('Maaf Produk ini tidak mempunyai jasa Dokter', 'Informasi');
							} */
						}
						}
        }


    );

    return gridDTItemTest;
};


function grid_lab_satu(){
	var panel_lab_satu = new Ext.Panel({
		title: 'Item Test',
		id:'grid_lab_satu',
        fileUpload: true,
        region: 'north',
        layout: 'column',
        height:160,
        anchor: '100%',
        width: 815,
        border: false,
        items: [GetDTGridPenJasLab(),Getdetail_order()]
    });
	return panel_lab_satu;
}
function TRPenJasLabColumModel()
{

	checkColumn_penata__lab = new Ext.grid.CheckColumn({
	   header: 'Cito',
	   dataIndex: 'cito',
	   id: 'checkid',
	   width: 55,
	   renderer: function(value)
	   {
        console.log("Grid");
		},
	});
    return new Ext.grid.ColumnModel
    (
        [ 
            new Ext.grid.RowNumberer(),
			 {
				header: 'Cito',
                dataIndex: 'cito',
                width:65,
				menuDisabled:true,
				listeners:{
					itemclick: function(dv, record, items, index, e)
					{
									Ext.getCmp('btnHpsBrsItemLabL').enable();
									console.log("test");
					},
				},
				renderer:function (v, metaData, record)
					{
						if ( record.data.cito=='0')
						{
						record.data.cito='Tidak'
						}else if (record.data.cito=='1')
						{
						metaData.style  ='background:#FF0000;  "font-weight":"bold";';
						record.data.cito='Ya'
						}else if (record.data.cito=='Ya')
						{
						metaData.style  ='background:#FF0000;  "font-weight":"bold";';
						}
						
						return record.data.cito; 
					},
				editor:new Ext.form.ComboBox
								({
								id: 'cboKasus',
								typeAhead: true,
								triggerAction: 'all',
								lazyRender: true,
								mode: 'local',
								selectOnFocus: true,
								forceSelection: true,
								emptyText: 'Silahkan Pilih...',
								width: 50,
								anchor: '95%',
								value: 1,
								store: new Ext.data.ArrayStore({
									id: 0,
									fields: ['Id', 'displayText'],
									data: [[1, 'Ya'], [2, 'Tidak']]
								}),
								valueField: 'displayText',
								displayField: 'displayText',
								value		: '',
									   
							})
			},
			{
                header: 'kd_tarif',
                dataIndex: 'kd_tarif',
                width:250,
				menuDisabled:true,
				hidden :true

            },
            {
                header: 'Kode Produk',
                dataIndex: 'kd_produk',
                width:100,
                menuDisabled:true,
                hidden:true
            },
            /* {
                header:'Nama Produk',
                dataIndex: 'deskripsi',
                sortable: false,
                hidden:false,
                menuDisabled:true,
                width:320

            }, */
			{	id:'nama_pemereksaaan_lab',
				dataIndex: 'deskripsi',
				header: 'Nama Produk',
				sortable: true,
				menuDisabled:true,
				width: 320,
				editor:new Nci.form.Combobox.autoComplete({
					store	: TrPenJasLab.form.ArrayStore.produk,
					select	: function(a,b,c){
					//	console.log(b);
					//Disini
						Ext.Ajax.request
						(
							{
								url: baseURL + "index.php/main/functionLAB/cekProduk",
								params:{kd_lab:b.data.kd_produk} ,
								failure: function(o)
								{
									ShowPesanErrorPenJasLab('Hubungi Admin', 'Error');
								},
								success: function(o)
								{
									Ext.getCmp('btnHpsBrsItemLabL').enable();
									console.log("test");
									var cst = Ext.decode(o.responseText);
									if (cst.success === true)
									{
										var line = gridDTItemTest.getSelectionModel().selection.cell[0];
										dsTRDetailPenJasLabList.data.items[line].data.deskripsi=b.data.deskripsi;
										dsTRDetailPenJasLabList.data.items[line].data.uraian=b.data.uraian;
										dsTRDetailPenJasLabList.data.items[line].data.kd_tarif=b.data.kd_tarif;
										dsTRDetailPenJasLabList.data.items[line].data.kd_produk=b.data.kd_produk;
										dsTRDetailPenJasLabList.data.items[line].data.tgl_transaksi=b.data.tgl_transaksi;
										dsTRDetailPenJasLabList.data.items[line].data.tgl_berlaku=b.data.tgl_berlaku;
										dsTRDetailPenJasLabList.data.items[line].data.harga=b.data.harga;
										dsTRDetailPenJasLabList.data.items[line].data.qty=b.data.qty;
										dsTRDetailPenJasLabList.data.items[line].data.jumlah=b.data.jumlah;

										gridDTItemTest.getView().refresh();
									}
									else
									{
										ShowPesanInfoPenJasLab('Nilai normal item '+b.data.deskripsi+' belum tersedia', 'Information');
									};
								}
							}

						)
						
					},
					insert	: function(o){
						return {
							uraian        	: o.uraian,
							kd_tarif 		: o.kd_tarif,
							kd_produk		: o.kd_produk,
							tgl_transaksi	: o.tgl_transaksi,
							tgl_berlaku		: o.tgl_berlaku,
							harga			: o.harga,
							qty				: o.qty,
							deskripsi		: o.deskripsi,
							jumlah			: o.jumlah,
							text			:  '<table style="font-size: 11px;"><tr><td width="60">'+o.kd_produk+'</td><td width="150">'+o.deskripsi+'</td></tr></table>'
						}
					},
					param	: function(){
					var kd_cus_gettarif;
					if(Ext.get('cboKelPasienLab').getValue()=='Perseorangan'){
						kd_cus_gettarif=Ext.getCmp('cboPerseoranganLab').getValue();
					}else if(Ext.get('cboKelPasienLab').getValue()=='Perusahaan'){
						kd_cus_gettarif=Ext.getCmp('cboPerusahaanRequestEntryLab').getValue();
					}else {
						kd_cus_gettarif=Ext.getCmp('cboAsuransiLab').getValue();
						  }
					var params={};
					params['kd_unit']=kodeUnitLab;
					params['kd_customer']=kd_cus_gettarif;
					return params;
					},
					url		: baseURL + "index.php/main/functionLAB/getProduk",
					valueField: 'deskripsi',
					displayField: 'text',
					listWidth: 210
				})
			},
            {
				header: 'Tanggal Transaksi',
				dataIndex: 'tgl_transaksi',
				width: 130,
				menuDisabled:true,
				renderer: function(v, params, record)
				{
					if(record.data.tgl_transaksi == undefined){
						record.data.tgl_transaksi=tglGridBawah;
						return record.data.tgl_transaksi;
					} else{
						if(record.data.tgl_transaksi.substring(5, 4) == '-'){
							return ShowDate(record.data.tgl_transaksi);
						} else{
							var tgl=record.data.tgl_transaksi.split("/");

							if(tgl[2].length == 4 && isNaN(tgl[1])){
								return record.data.tgl_transaksi;
							} else{
								return ShowDate(record.data.tgl_transaksi);
							}
						}

					}
				}
            },
			{
				header: 'Tanggal Berlaku',
				dataIndex: 'tgl_berlaku',
				width: 130,
				menuDisabled:true,
				hidden: true,
				renderer: function(v, params, record)
				{
					if(record.data.tgl_berlaku == undefined || record.data.tgl_berlaku == null){
						record.data.tgl_berlaku=tglGridBawah;
						return record.data.tgl_berlaku;
					} else{
						if(record.data.tgl_berlaku.substring(5, 4) == '-'){
							return ShowDate(record.data.tgl_berlaku);
						} else{
							var tglb=record.data.tgl_berlaku.split("-");

							if(tglb[2].length == 4 && isNaN(tglb[1])){
								return record.data.tgl_berlaku;
							} else{
								return ShowDate(record.data.tgl_berlaku);
							}
						}

					}
				}
            },
            {
                header: 'Harga',
                align: 'right',
                hidden: false,
                menuDisabled:true,
                dataIndex: 'harga',
                width:100,
				renderer: function(v, params, record)
				{
					return formatCurrency(record.data.harga);
				}
            },
            {	id:'qty_lab',
                header: 'Qty',
                width:91,
				align: 'right',
				menuDisabled:true,
                dataIndex: 'qty',
                editor: new Ext.form.NumberField (
					{
					
					}
                   /*  {
                        id:'fieldcolProblemRWJ',
                        allowBlank: true,
                        enableKeyEvents : true,
                        width:30,
						listeners:
						{
							'specialkey' : function()
							{
								Dataupdate_PenJasRad(false);
							}
						}
                    } */
                ),



            },
			 {
                header: 'Dokter',
                width:91,
				menuDisabled:true,
                dataIndex: 'jumlah',
				hidden: true,

            },

        ]
    )
};

function RecordBaruRWJ()
{
	var tgltranstoday = Ext.getCmp('dtpKunjunganL').getValue();
	var p = new mRecordRwj
	(
		{
			'DESKRIPSI2':'',
			'KD_PRODUK':'',
		    'DESKRIPSI':'',
		    'KD_TARIF':'',
		    'HARGA':'',
		    'QTY':'',
		    'TGL_TRANSAKSI':tgltranstoday.format('Y/m/d'),
		    'DESC_REQ':'',
		    'KD_TARIF':'',
		    'URUT':''
		}
	);

	return p;
};


//tampil grid bawah penjas lab
function ViewGridBawahLookupPenjasLab(no_transaksi)
{
     var strKriteriaRWJ='';
    strKriteriaRWJ = "\"no_transaksi\" = ~" + no_transaksi + "~" + " And kd_kasir ='03'";

    dsTRDetailPenJasLabList.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'tgl_transaksi',
			    Sortdir: 'ASC',
			    target: 'ViewDetailGridBawahPenJasLab',
			    param: strKriteriaRWJ
			}
		}
	);
    return dsTRDetailPenJasLabList;
}

function ViewGridBawahPenjasLab(no_transaksi,data)
{
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/main/functionLAB/getItemPemeriksaan",
			params: {no_transaksi:no_transaksi},
			failure: function(o)
			{
				//ShowPesanErrorPenJasLab('Hubungi Admin', 'Error');
			},
			success: function(o)
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					dsTRDetailPenJasLabList.removeAll();
					var recs=[],
						recType=dsTRDetailPenJasLabList.recordType;

					for(var i=0; i<cst.ListDataObj.length; i++){

						recs.push(new recType(cst.ListDataObj[i]));

					}
					dsTRDetailPenJasLabList.add(recs);

					gridDTItemTest.getView().refresh();

					if(dsTRDetailPenJasLabList.getCount() > 0 ){
						//Ext.getCmp('cboDOKTER_viPenJasLab').setReadOnly(true);
						TmpNotransaksi=data.no_transaksi;
						KdKasirAsal=data.kd_kasir;
						TglTransaksiAsal=data.tgl_transaksi;
						Kd_Spesial=data.kd_spesial;
						No_Kamar=data.no_kamar;
						//kunj_urut_masuk=data.urut_masuk;
						getDokterPengirim(data.no_transaksi,data.kd_kasir);
						//TrPenJasLab.form.ComboBox.notransaksi.setValue(data.no_transaksi);
						Ext.getCmp('txtNamaUnitLab').setValue(data.nama_unit);
						Ext.getCmp('txtKdUnitLab').setValue(data.kd_unit);
						Ext.getCmp('txtKdDokter').setValue(data.kd_dokter);
						Ext.getCmp('cboDOKTER_viPenJasLab').setValue(data.kd_dokter);
						Ext.getCmp('txtAlamatL').setValue(data.alamat);
						Ext.getCmp('dtpTtlL').setValue(ShowDate(data.tgl_lahir));
						Ext.getCmp('txtKdUrutMasuk').setValue(data.urut_masuk);
												console.log(data.urut_masuk);
						Ext.getCmp('cboJKPenjasLab').setValue(data.jenis_kelamin);
						if (data.jenis_kelamin === 't')
						{
							Ext.getCmp('cboJKPenjasLab').setValue('Laki-laki');
						}else
						{
							Ext.getCmp('cboJKPenjasLab').setValue('Perempuan');
						}
						Ext.getCmp('cboJKPenjasLab').disable();

						Ext.getCmp('cboGDRPenJasLab').setValue(data.gol_darah);
						if (data.gol_darah === '0')
						{
							Ext.getCmp('cboGDRPenJasLab').setValue('-');
						}else if (data.gol_darah === '1')
						{
							Ext.getCmp('cboGDRPenJasLab').setValue('A+');
						}else if (data.gol_darah === '2')
						{
							Ext.getCmp('cboGDRPenJasLab').setValue('B+');
						}else if (data.gol_darah === '3')
						{
							Ext.getCmp('cboGDRPenJasLab').setValue('AB+');
						}else if (data.gol_darah === '4')
						{
							Ext.getCmp('cboGDRPenJasLab').setValue('O+');
						}else if (data.gol_darah === '5')
						{
							Ext.getCmp('cboGDRPenJasLab').setValue('A-');
						}else if (data.gol_darah === '6')
						{
							Ext.getCmp('cboGDRPenJasLab').setValue('B-');
						}
						Ext.getCmp('cboGDRPenJasLab').disable();

						Ext.getCmp('txtKdCustomerLamaHide').setValue(data.kd_customer);

						//tampung kd customer untuk transaksi selain kunjungan langsung
						vkode_customer = data.kd_customer;
						//Ext.getCmp('cboKelPasienLab').enable();

						if(data.kelpasien == 'Perseorangan'){
							Ext.getCmp('cboKelPasienLab').setValue(data.kelpasien);
							Ext.getCmp('cboPerseoranganLab').setValue(data.kd_customer);
							Ext.getCmp('cboPerseoranganLab').show();
						} else if(data.kelpasien == 'Perusahaan'){
							Ext.getCmp('cboKelPasienLab').setValue(data.kelpasien);
							Ext.getCmp('cboPerusahaanRequestEntryLab').setValue(data.kd_customer);
							Ext.getCmp('cboPerusahaanRequestEntryLab').show();
						} else{
							Ext.getCmp('cboKelPasienLab').setValue(data.kelpasien);
							Ext.getCmp('cboAsuransiLab').setValue(data.kd_customer);
							Ext.getCmp('cboAsuransiLab').show();
						}
						Ext.getCmp('txtDokterPengirimL').disable();
						Ext.getCmp('txtNamaUnitLab').disable();
						Ext.getCmp('txtAlamatL').disable();
						Ext.getCmp('cboJKPenjasLab').disable();
						Ext.getCmp('cboGDRPenJasLab').disable();
						Ext.getCmp('dtpTtlL').disable();
						Ext.getCmp('dtpKunjunganL').disable();
						Ext.getCmp('btnLookupItemPemeriksaan').disable();
						//Ext.getCmp('btnHpsBrsItemLabL').disable();
						Ext.getCmp('btnHpsBrsItemLabL').enable();
						
						if (var_tampung_lab_order===true)
						{
						Ext.getCmp('cboDOKTER_viPenJasLab').enable();
						Ext.getCmp('btnSimpanPenJasLab').hide();
						gridDTItemTest.disable();
						Ext.getCmp('cboKelPasienLab').enable();
						Ext.getCmp('cboAsuransiLab').enable();
						Ext.getCmp('cboPerseoranganLab').enable();
						Ext.getCmp('cboPerusahaanRequestEntryLab').enable();
						Ext.getCmp('btnSimpanPenJasLab_kiriman').show();
						
						}else{
						Ext.getCmp('cboDOKTER_viPenJasLab').disable();
						Ext.getCmp('btnSimpanPenJasLab').show();
						Ext.getCmp('btnSimpanPenJasLab').disable();
						Ext.getCmp('btnSimpanPenJasLab_kiriman').hide();
						
						gridDTItemTest.enable();
						Ext.getCmp('cboKelPasienLab').disable();
						Ext.getCmp('cboAsuransiLab').disable();
						Ext.getCmp('cboPerseoranganLab').disable();
						Ext.getCmp('cboPerusahaanRequestEntryLab').disable();
						var_tampung_lab_order=false;	
						}
						
					} else {
						TmpNotransaksi=data.no_transaksi;
						KdKasirAsal=data.kd_kasir;
						TglTransaksiAsal=data.tgl_transaksi;
						Kd_Spesial=data.kd_spesial;
						No_Kamar=data.no_kamar;
						Ext.getCmp('btnSimpanPenJasLab').show();
						Ext.getCmp('btnSimpanPenJasLab_kiriman').hide();
						Ext.getCmp('txtNamaUnitLab').setValue(data.nama_unit_asli);
						Ext.getCmp('txtKdUnitLab').setValue(data.kd_unit);
						Ext.getCmp('txtDokterPengirimL').setValue(data.dokter);
						Ext.getCmp('txtAlamatL').setValue(data.alamat);
						Ext.getCmp('dtpTtlL').setValue(ShowDate(data.tgl_lahir));

						Ext.getCmp('cboJKPenjasLab').setValue(data.jenis_kelamin);
						if (data.jenis_kelamin === 't')
						{
							Ext.getCmp('cboJKPenjasLab').setValue('Laki-laki');
						}else
						{
							Ext.getCmp('cboJKPenjasLab').setValue('Perempuan');
						}
						Ext.getCmp('cboJKPenjasLab').disable();

						Ext.getCmp('cboGDRPenJasLab').setValue(data.gol_darah);
						if (data.gol_darah === '0')
						{
							Ext.getCmp('cboGDRPenJasLab').setValue('-');
						}else if (data.gol_darah === '1')
						{
							Ext.getCmp('cboGDRPenJasLab').setValue('A+');
						}else if (data.gol_darah === '2')
						{
							Ext.getCmp('cboGDRPenJasLab').setValue('B+');
						}else if (data.gol_darah === '3')
						{
							Ext.getCmp('cboGDRPenJasLab').setValue('AB+');
						}else if (data.gol_darah === '4')
						{
							Ext.getCmp('cboGDRPenJasLab').setValue('O+');
						}else if (data.gol_darah === '5')
						{
							Ext.getCmp('cboGDRPenJasLab').setValue('A-');
						}else if (data.gol_darah === '6')
						{
							Ext.getCmp('cboGDRPenJasLab').setValue('B-');
						}
						Ext.getCmp('cboGDRPenJasLab').disable();
						Ext.getCmp('txtKdCustomerLamaHide').setValue(data.kd_customer);
						vkode_customer = data.kd_customer;
						Ext.getCmp('cboKelPasienLab').enable();
						if(data.kelpasien == 'Perseorangan'){
							Ext.getCmp('cboKelPasienLab').setValue(data.kelpasien);
							Ext.getCmp('cboPerseoranganLab').setValue(data.kd_customer);
							Ext.getCmp('cboPerseoranganLab').show();
						} else if(data.kelpasien == 'Perusahaan'){
							Ext.getCmp('cboKelPasienLab').setValue(data.kelpasien);
							Ext.getCmp('cboPerusahaanRequestEntryLab').setValue(data.kd_customer);
							Ext.getCmp('cboPerusahaanRequestEntryLab').show();
						} else{
							Ext.getCmp('cboKelPasienLab').setValue(data.kelpasien);
							Ext.getCmp('cboAsuransiLab').setValue(data.kd_customer);
							Ext.getCmp('cboAsuransiLab').show();
						}

						Ext.getCmp('txtDokterPengirimL').disable();
						Ext.getCmp('txtNamaUnitLab').disable();
						Ext.getCmp('cboDOKTER_viPenJasLab').enable();
						Ext.getCmp('txtAlamatL').disable();
						Ext.getCmp('cboJKPenjasLab').disable();
						Ext.getCmp('cboGDRPenJasLab').disable();
						Ext.getCmp('cboKelPasienLab').enable();
						Ext.getCmp('dtpTtlL').disable();
						Ext.getCmp('dtpKunjunganL').disable();
						Ext.getCmp('btnLookupItemPemeriksaan').enable();
						Ext.getCmp('btnHpsBrsItemLabL').enable();
						//Ext.getCmp('btnHpsBrsItemLabL').disable();
						Ext.getCmp('btnSimpanPenJasLab').enable();
						Ext.getCmp('cboKelPasienLab').disable();
						Ext.getCmp('cboAsuransiLab').disable();
						Ext.getCmp('cboPerseoranganLab').disable();
						Ext.getCmp('cboPerusahaanRequestEntryLab').disable();
						gridDTItemTest.enable();
						var_tampung_lab_order=false;
					
					
					}
				}
			}
		}

	)
};

//---------------------------------------------------------------------------------------///

//---------------------------------------------------------------------------------------///
function getParamDetailTransaksiLAB()
{
	Ext.Ajax.request(
	{
		url: baseURL + "index.php/main/functionLAB/getCurrentShiftLab",
		params: {
			command: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
		},
		failure: function(o)
		{
			var cst = Ext.decode(o.responseText);
		},
		success: function(o) {
			tampungshiftsekarang=o.responseText;
		}
	});
	var KdCust='';
	var TmpCustoLama='';
	var pasienBaru;

	if(Ext.get('cboKelPasienLab').getValue()=='Perseorangan'){
		KdCust=Ext.getCmp('cboPerseoranganLab').getValue();
	}else if(Ext.get('cboKelPasienLab').getValue()=='Perusahaan'){
		KdCust=Ext.getCmp('cboPerusahaanRequestEntryLab').getValue();
	}else {
		KdCust=Ext.getCmp('cboAsuransiLab').getValue();
	}

	if(TrPenJasLab.form.ComboBox.kdpasien.getValue() == ''){
		pasienBaru=1;
	} else{
		pasienBaru=0;
	}

    var params =
	{
		KdTransaksi: TrPenJasLab.form.ComboBox.notransaksi.getValue(),
		KdPasien:TrPenJasLab.form.ComboBox.kdpasien.getValue(),
		NmPasien:TrPenJasLab.form.ComboBox.pasien.getValue(),
		Ttl:Ext.getCmp('dtpTtlL').getValue(),
		Alamat:Ext.getCmp('txtAlamatL').getValue(),
		JK:Ext.getCmp('cboJKPenjasLab').getValue(),
		GolDarah:Ext.getCmp('cboGDRPenJasLab').getValue(),
		KdUnit: Ext.getCmp('txtKdUnitLab').getValue(),
		KdDokter:Ext.getCmp('cboDOKTER_viPenJasLab').getValue(),
		Tgl: Ext.getCmp('dtpKunjunganL').getValue(),
		TglTransaksiAsal:TglTransaksiAsal,
		urutmasuk:Ext.getCmp('txtKdUrutMasuk').getValue(),
		KdCusto:KdCust,
		TmpCustoLama:KdCust, //Ext.getCmp('txtKdCustomerLamaHide').getValue(),//jika customer dengan transaksi lama
		NamaPesertaAsuransi:Ext.getCmp('txtNamaPesertaAsuransiLab').getValue(),
		NoAskes:Ext.getCmp('txtNoAskesLab').getValue(),
		NoSJP:Ext.getCmp('txtNoSJPLab').getValue(),
		Shift: tampungshiftsekarang,
		List:getArrPenJasLab(),
		JmlField: mRecordRwj.prototype.fields.length-4,
		JmlList:GetListCountDetailTransaksi(),
		unit:41,
		TmpNotransaksi:TmpNotransaksi,
		KdKasirAsal:KdKasirAsal,
		KdSpesial:Kd_Spesial,
		Kamar:No_Kamar,
		pasienBaru:pasienBaru,
		listTrDokter	: Ext.encode(dataTrDokter),
		KdProduk:''//dsTRDetailPenJasLabList.data.items[CurrentPenJasLab.row].data.KD_PRODUK

	};
    return params
};

function GetListCountDetailTransaksi()
{

	var x=0;
	for(var i = 0 ; i < dsTRDetailPenJasLabList.getCount();i++)
	{
		if (dsTRDetailPenJasLabList.data.items[i].data.KD_PRODUK != '' || dsTRDetailPenJasLabList.data.items[i].data.DESKRIPSI  != '')
		{
			x += 1;
		};
	}
	return x;

};





function getArrPenJasLab()
{
	var x='';
	var arr=[];
	for(var i = 0 ; i < dsTRDetailPenJasLabList.getCount();i++)
	{

		if (dsTRDetailPenJasLabList.data.items[i].data.kd_produk != '' && dsTRDetailPenJasLabList.data.items[i].data.deskripsi != '')
		{
			var o={};
			var y='';
			var z='@@##$$@@';
			o['URUT']= dsTRDetailPenJasLabList.data.items[i].data.urut;
			o['KD_PRODUK']= dsTRDetailPenJasLabList.data.items[i].data.kd_produk;
			o['QTY']= dsTRDetailPenJasLabList.data.items[i].data.qty;
			o['TGL_TRANSAKSI']= dsTRDetailPenJasLabList.data.items[i].data.tgl_transaksi;
			o['TGL_BERLAKU']= dsTRDetailPenJasLabList.data.items[i].data.tgl_berlaku;
			o['HARGA']= dsTRDetailPenJasLabList.data.items[i].data.harga;
			o['KD_TARIF']= dsTRDetailPenJasLabList.data.items[i].data.kd_tarif;
			o['cito']= dsTRDetailPenJasLabList.data.items[i].data.cito;
			arr.push(o);

		};
	}

	return Ext.encode(arr);
};

//panel dalam lookup detail pasien
function getItemPanelInputPenJasLab(lebar)
{
	//pengaturan panel data pasien
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:5px 5px 5px 5px',
	    border:false,
	    height:225,
	    items:
		[
					{ //awal panel biodata pasien
						columnWidth:.99,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: true,
						width: 100,
						height: 125,
						anchor: '100% 100%',
						items:
						[
                            //bagian pinggir kiri
							{
								x: 10,
								y: 10,
								xtype: 'label',
								text: 'No. Transaksi  '
							},
							{
								x: 120,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							/* {
								x : 130,
								y : 10,
								xtype: 'numberfield',
								name: 'txtNoTransaksiPenJasLab',
								id: 'txtNoTransaksiPenJasLab',
								width: 100,
								readOnly: true

							}, */
							TrPenJasLab.form.ComboBox.notransaksi= new Nci.form.Combobox.autoComplete({
								store	: TrPenJasLab.form.ArrayStore.notransaksi,
								select	: function(a,b,c){
									ViewGridBawahPenjasLab(b.data.no_transaksi,b.data);
									TrPenJasLab.form.ComboBox.kdpasien.disable();
									TrPenJasLab.form.ComboBox.kdpasien.setValue(b.data.kd_pasien);
									TrPenJasLab.form.ComboBox.pasien.disable();
									TrPenJasLab.form.ComboBox.pasien.setValue(b.data.nama);
									
									kodeUnitLab=b.data.kd_unit;
									console.log(kodeUnitLab);

									Ext.getCmp('btnLookupItemPemeriksaan').disable();
									Ext.getCmp('btnHpsBrsItemLabL').enable();
									//Ext.getCmp('btnHpsBrsItemLabL').disable();
									Ext.getCmp('btnSimpanPenJasLab').disable();
									Ext.getCmp('btnSimpanPenJasLab_kiriman').hide();
									TrPenJasLab.vars.kd_tarif=b.data.kd_tarif;

									Ext.Ajax.request(
									{
										url: baseURL + "index.php/main/functionLAB/getCurrentShiftLab",
										params: {
											command: '0',
											// parameter untuk url yang dituju (fungsi didalam controller)
										},
										failure: function(o)
										{
											var cst = Ext.decode(o.responseText);
										},
										success: function(o) {
											tampungshiftsekarang=o.responseText;
										}
									});

								},
								width	: 100,
								insert	: function(o){
									return {
										nama        		:o.nama,
										tgl_transaksi		:o.tgl_transaksi,
										kd_spesial			:o.kd_spesial,
										kamar				:o.kamar,
										tgl_lahir			:o.tgl_lahir,
										gol_darah			:o.gol_darah,
										jenis_kelamin		:o.jenis_kelamin,
										dokter				:o.dokter,
										urut_masuk			:o.urut_masuk,
										nama_unit			:o.nama_unit,
										alamat				:o.alamat,
										kd_pasien        	:o.kd_pasien,
										kd_dokter			:o.kd_dokter,
										kd_unit				:o.kd_unit,
										kd_customer			:o.kd_customer,
										kd_kasir			:o.kd_kasir,
										nama_kamar			:o.nama_kamar,
										no_kamar			:o.no_kamar,
										customer			:o.customer,
										nama_unit_asli		:o.nama_unit_asli,
										tgl					:o.tgl,
										kd_tarif			:o.kd_tarif,
										kelpasien			:o.kelpasien,
										text				:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_pasien+'</td><td width="70" align="left">'+o.no_transaksi+'</td><td width="130">'+o.nama+'</td><td width="110" align="left">'+o.nama_unit_asli+'</td><td width="70">'+o.tgl+'</td></tr></table>',
										no_transaksi		:o.no_transaksi,
									}
								},
								param:function(){
									var a=0;
									return {
										tanggal:Ext.getCmp('dtpKunjunganL').getValue(),
										a:0
									}
								},
								url		: baseURL + "index.php/main/functionLAB/getPasien",
								valueField: 'no_transaksi',
								displayField: 'text',
								listWidth: 480,
								x : 130,
								y : 10,
								emptyText: 'No Transaksi'
							}),
							{
								x: 10,
								y: 40,
								xtype: 'label',
								text: 'No. Medrec  '
							},
							{
								x: 120,
								y: 40,
								xtype: 'label',
								text: ':'
							},
							/* {
								x: 130,
								y: 40,
								xtype: 'textfield',
								fieldLabel: 'No. Medrec',
								name: 'txtNoMedrecL',
								id: 'txtNoMedrecL',
								width: 120,
								emptyText:'Otomatis',
								listeners:
								{

								}
							}, */
							TrPenJasLab.form.ComboBox.kdpasien= new Nci.form.Combobox.autoComplete({
								store	: TrPenJasLab.form.ArrayStore.kodepasien,
								select	: function(a,b,c){
									TrPenJasLab.form.ComboBox.notransaksi.setValue('');
									ViewGridBawahPenjasLab(b.data.no_transaksi,b.data);
									TrPenJasLab.form.ComboBox.pasien.disable();
									TrPenJasLab.form.ComboBox.pasien.setValue(b.data.nama);
									TrPenJasLab.form.ComboBox.notransaksi.disable();
									TrPenJasLab.form.ComboBox.notransaksi.setValue(b.data.no_transaksi);
									kodeUnitLab=b.data.kd_unit;

									TrPenJasLab.vars.kd_tarif=b.data.kd_tarif;

									Ext.Ajax.request(
									{
										url: baseURL + "index.php/main/functionLAB/getCurrentShiftLab",
										params: {
											command: '0',
											// parameter untuk url yang dituju (fungsi didalam controller)
										},
										failure: function(o)
										{
											var cst = Ext.decode(o.responseText);
										},
										success: function(o) {
											tampungshiftsekarang=o.responseText;
										}
									});

								},
								width	: 120,
								insert	: function(o){
									return {
										no_transaksi		:o.no_transaksi,
										tgl_transaksi		:o.tgl_transaksi,
										kd_spesial			:o.kd_spesial,
										kamar				:o.kamar,
										tgl_lahir			:o.tgl_lahir,
										gol_darah			:o.gol_darah,
										jenis_kelamin		:o.jenis_kelamin,
										dokter				:o.dokter,
										urut_masuk			:o.urut_masuk,
										nama_unit			:o.nama_unit,
										alamat				:o.alamat,
										kd_pasien        	:o.kd_pasien,
										nama        		:o.nama,
										kd_dokter			:o.kd_dokter,
										kd_unit				:o.kd_unit,
										kd_customer			:o.kd_customer,
										kd_kasir			:o.kd_kasir,
										nama_kamar			:o.nama_kamar,
										no_kamar			:o.no_kamar,
										customer			:o.customer,
										nama_unit_asli		:o.nama_unit_asli,
										tgl					:o.tgl,
										kd_tarif			:o.kd_tarif,
										kelpasien			:o.kelpasien,
										text				:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_pasien+'</td><td width="70" align="left">'+o.no_transaksi+'</td><td width="130">'+o.nama+'</td><td width="110" align="left">'+o.nama_unit_asli+'</td><td width="70">'+o.tgl+'</td></tr></table>',
										kd_pasien	 		:o.kd_pasien
									}
								},
								param:function(){
									var a=0;
									return {
										tanggal:Ext.getCmp('dtpKunjunganL').getValue(),
										a:1
									}
								},
								url		: baseURL + "index.php/main/functionLAB/getPasien",
								valueField: 'kd_pasien',
								displayField: 'text',
								listWidth: 480,
								x: 130,
								y: 40,
								emptyText: 'Kode Pasien'
							}),
							{
								x: 10,
								y: 70,
								xtype: 'label',
								text: 'Unit  '
							},
							{
								x: 120,
								y: 70,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 70,
								xtype: 'textfield',
								name: 'txtNamaUnitLab',
								id: 'txtNamaUnitLab',
								readOnly:true,
								width: 120
							},
							{
								x: 10,
								y: 100,
								xtype: 'label',
								text: 'Dokter Pengirim'
							},
							{
								x: 120,
								y: 100,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 100,
								xtype: 'textfield',
								fieldLabel: '',
								readOnly:true,
								name: 'txtDokterPengirimL',
								id: 'txtDokterPengirimL',
								width: 200
							},
							{
								x: 10,
								y: 130,
								xtype: 'label',
								text: 'Dokter Lab'
							},
							{
								x: 120,
								y: 130,
								xtype: 'label',
								text: ':'
							},
							mComboDOKTER(),
							{
								x: 10,
								y: 160,
								xtype: 'label',
								text: 'Kelompok Pasien '
							},
							{
								x: 120,
								y: 160,
								xtype: 'label',
								text: ':'
							},
							mCboKelompokpasien(),
							mComboPerseorangan(),
							mComboPerusahaan(),
							mComboAsuransi(),
							//bagian tengah
							{
								x: 400,
								y: 10,
								xtype: 'label',
								text: 'Tanggal Kunjung '
							},
							{
								x: 520,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 530,
								y: 10,
								xtype: 'datefield',
								fieldLabel: 'Tanggal hari ini ',
								id: 'dtpKunjunganL',
								name: 'dtpKunjunganL',
								format: 'd/M/Y',
								readOnly : false,
								value: now,
								width: 110
							},
							{
								x: 400,
								y: 40,
								xtype: 'label',
								text: 'Nama Pasien '
							},
							{
								x: 520,
								y: 40,
								xtype: 'label',
								text: ':'
							},
							/* {
								x: 400,
								y: 40,
								xtype: 'textfield',
								fieldLabel: '',
								name: 'txtNamaPasienL',
								id: 'txtNamaPasienL',
								width: 200
							}, */
							TrPenJasLab.form.ComboBox.pasien= new Nci.form.Combobox.autoComplete({
								store	: TrPenJasLab.form.ArrayStore.pasien,
								select	: function(a,b,c){
									TrPenJasLab.form.ComboBox.notransaksi.setValue('');
									ViewGridBawahPenjasLab(b.data.no_transaksi,b.data);
									TrPenJasLab.form.ComboBox.kdpasien.disable();
									TrPenJasLab.form.ComboBox.kdpasien.setValue(b.data.kd_pasien);
									TrPenJasLab.form.ComboBox.notransaksi.disable();
									TrPenJasLab.form.ComboBox.notransaksi.setValue(b.data.no_transaksi);
									kodeUnitLab=b.data.kd_unit;

									TrPenJasLab.vars.kd_tarif=b.data.kd_tarif;

									Ext.Ajax.request(
									{
										url: baseURL + "index.php/main/functionLAB/getCurrentShiftLab",
										params: {
											command: '0',
											// parameter untuk url yang dituju (fungsi didalam controller)
										},
										failure: function(o)
										{
											var cst = Ext.decode(o.responseText);
										},
										success: function(o) {
											tampungshiftsekarang=o.responseText;
										}
									});

								},
								width	: 200,
								insert	: function(o){
									return {
										no_transaksi		:o.no_transaksi,
										tgl_transaksi		:o.tgl_transaksi,
										kd_spesial			:o.kd_spesial,
										kamar				:o.kamar,
										tgl_lahir			:o.tgl_lahir,
										gol_darah			:o.gol_darah,
										jenis_kelamin		:o.jenis_kelamin,
										dokter				:o.dokter,
										urut_masuk			:o.urut_masuk,
										nama_unit			:o.nama_unit,
										alamat				:o.alamat,
										kd_pasien        	:o.kd_pasien,
										kd_dokter			:o.kd_dokter,
										kd_unit				:o.kd_unit,
										kd_customer			:o.kd_customer,
										kd_kasir			:o.kd_kasir,
										nama_kamar			:o.nama_kamar,
										no_kamar			:o.no_kamar,
										customer			:o.customer,
										nama_unit_asli		:o.nama_unit_asli,
										tgl					:o.tgl,
										kd_tarif			:o.kd_tarif,
										kelpasien			:o.kelpasien,
										text				:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_pasien+'</td><td width="70" align="left">'+o.no_transaksi+'</td><td width="130">'+o.nama+'</td><td width="110" align="left">'+o.nama_unit_asli+'</td><td width="70">'+o.tgl+'</td></tr></table>',
										nama        		:o.nama,
									}
								},
								param:function(){
									var a=0;
									return {
										tanggal:Ext.getCmp('dtpKunjunganL').getValue(),
										a:2
									}
								},
								url		: baseURL + "index.php/main/functionLAB/getPasien",
								valueField: 'nama',
								displayField: 'text',
								listWidth: 480,
								x: 530,
								y: 40,
								emptyText: 'Nama'
							}),
							{
								x: 400,
								y: 70,
								xtype: 'label',
								text: 'Tanggal lahir '
							},
							{
								x: 520,
								y: 70,
								xtype: 'label',
								text: ':'
							},
							{
								x: 530,
								y: 70,
								xtype: 'datefield',
								fieldLabel: 'Tanggal ',
								id: 'dtpTtlL',
								name: 'dtpTtlL',
								format: 'd/M/Y',
								value: now,
								width: 110
							},
							{
								x: 400,
								y: 100,
								xtype: 'label',
								text: 'Alamat '
							},
							{
								x: 520,
								y: 100,
								xtype: 'label',
								text: ':'
							},
							{
								x: 530,
								y: 100,
								xtype: 'textfield',
								fieldLabel: '',
								name: 'txtAlamatL',
								id: 'txtAlamatL',
								width: 395
							},
							{
								x: 400,
								y: 130,
								xtype: 'label',
								text: 'Jenis Kelamin '
							},
							{
								x: 520,
								y: 130,
								xtype: 'label',
								text: ':'
							},
							mComboJK(),
							{
								x: 700,
								y: 130,
								xtype: 'label',
								text: 'Gol. Darah '
							},
							{
								x: 820,
								y: 130,
								xtype: 'label',
								text: ':'
							},
							mComboGDarah(),
							{
								x: 440,
								y: 160,
								xtype: 'textfield',
								fieldLabel: 'Nama Peserta',
								maxLength: 200,
								name: 'txtNamaPesertaAsuransiLab',
								id: 'txtNamaPesertaAsuransiLab',
								emptyText:'Nama Peserta Asuransi',
								width: 150
							},
							{
								x: 600,
								y: 160,
								xtype: 'textfield',
								fieldLabel: 'No. Askes',
								maxLength: 200,
								name: 'txtNoAskesLab',
								id: 'txtNoAskesLab',
								emptyText:'No Askes',
								width: 150
							},
							{
								x: 760,
								y: 160,
								xtype: 'textfield',
								fieldLabel: 'No. SJP',
								maxLength: 200,
								name: 'txtNoSJPLab',
								id: 'txtNoSJPLab',
								emptyText:'No SJP',
								width: 150
							},


							//---------------------hidden----------------------------------
							{
								x: 130,
								y: 160,
								xtype: 'textfield',
								fieldLabel:'Nama customer lama',
								name: 'txtCustomerLamaHide',
								id: 'txtCustomerLamaHide',
								readOnly:true,
								hidden:true,
								width: 280
							},
							{
								x: 130,
								y: 160,
								xtype: 'textfield',
								fieldLabel:'Kode customer  ',
								name: 'txtKdCustomerLamaHide',
								id: 'txtKdCustomerLamaHide',
								readOnly:true,
								hidden:true,
								width: 280
							},

							{
								xtype: 'textfield',
								fieldLabel:'Dokter  ',
								name: 'txtKdDokter',
								id: 'txtKdDokter',
								readOnly:true,
								hidden:true,
								anchor: '99%'
							},
							{
								xtype: 'textfield',
								fieldLabel:'Dokter  ',
								name: 'txtKdDokterPengirim',
								id: 'txtKdDokterPengirim',
								readOnly:true,
								hidden:true,
								anchor: '99%'
							},
							{
								xtype: 'textfield',
								fieldLabel:'Unit  ',
								name: 'txtKdUnitLab',
								id: 'txtKdUnitLab',
								readOnly:true,
								hidden:true,
								anchor: '99%'
							},
							{
								xtype: 'textfield',
								name: 'txtKdUrutMasuk',
								id: 'txtKdUrutMasuk',
								readOnly:true,
								hidden:true,
								anchor: '100%'
							}

							//-------------------------------------------------------------

						]
					},	//akhir panel biodata pasien

		]
	};
    return items;
};


function Datasave_PenJasLab(mBol)
{
	if (ValidasiEntryPenJasLab(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		 (
			{
				url: baseURL + "index.php/main/functionLAB/savedetaillab",
				params: getParamDetailTransaksiLAB(),
				failure: function(o)
				{	var_tampung_lab_order=false;
					loadMask.hide();
					ShowPesanErrorPenJasLab('Error! Gagal menyimpan data ini. Hubungi Admin', 'Error');
					ViewGridBawahPenjasLab(TrPenJasLab.form.ComboBox.notransaksi.getValue());
				},
				success: function(o)
				{  	var_tampung_lab_order=false;
					var cst = Ext.decode(o.responseText);
					if (cst.success === true)
					{
						loadMask.hide();
						ShowPesanInfoPenJasLab("Berhasil menyimpan data ini","Information");
						TrPenJasLab.form.ComboBox.notransaksi.setValue(cst.notrans);
						TrPenJasLab.form.ComboBox.kdpasien.setValue(cst.kdPasien);
						if(cst.kdPasien.substring(0, 1) ==='L'){
							Ext.getCmp('txtNamaUnitLab').setValue('Laboratorium');
							Ext.getCmp('txtDokterPengirimL').setValue('-');
						}
						Ext.getCmp('cboDOKTER_viPenJasLab').disable();
						Ext.getCmp('btnLookupItemPemeriksaan').disable();
						Ext.getCmp('btnHpsBrsItemLabL').enable();
						//Ext.getCmp('btnHpsBrsItemLabL').disable();
						Ext.getCmp('btnSimpanPenJasLab').disable();
						Ext.getCmp('btnSimpanPenJasLab_kiriman').hide();
						ViewGridBawahPenjasLab(TrPenJasLab.form.ComboBox.notransaksi.getValue());
						Ext.getCmp('cboKelPasienLab').disable();
						Ext.getCmp('cboAsuransiLab').disable();
						Ext.getCmp('cboPerseoranganLab').disable();
						Ext.getCmp('cboPerusahaanRequestEntryLab').disable();
						//RefreshDataFilterPenJasRad();
						if(mBol === false)
						{
							loadMask.hide();
							ViewGridBawahPenjasLab(TrPenJasLab.form.ComboBox.notransaksi.getValue());
						};
					}
					else
					{
						loadMask.hide();
						ShowPesanErrorPenJasLab('Gagal menyimpan data ini. Hubungi Admin', 'Error');
					};
				}
			}
		)

	}
	else
	{
		if(mBol === true)
		{
			loadMask.hide();
			return false;
		};
	};

};

function Dataupdate_order_PenJasLab(mBol)
{
	if (ValidasiEntryPenJasLab(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		 (
			{
				url: baseURL + "index.php/main/functionLAB/update_dokter",
				params: getParamDetailTransaksiLAB(),
				failure: function(o)
				{	var_tampung_lab_order=false;
					loadMask.hide();
					ShowPesanErrorPenJasLab('Error! Gagal menyimpan data ini. Hubungi Admin', 'Error');
					ViewGridBawahPenjasLab(TrPenJasLab.form.ComboBox.notransaksi.getValue());
				},
				success: function(o)
				{  	
					total_pasien_order_mng();
					load_data_pasienorder();
					viewGridOrderAll_RASEPRWI();
					var_tampung_lab_order=false;
					var cst = Ext.decode(o.responseText);
					if (cst.success === true)
					{
						loadMask.hide();
						ShowPesanInfoPenJasLab("Berhasil menyimpan data ini","Information");
						TrPenJasLab.form.ComboBox.notransaksi.setValue(cst.notrans);
						TrPenJasLab.form.ComboBox.kdpasien.setValue(cst.kdPasien);
						if(cst.kdPasien.substring(0, 1) ==='L'){
							Ext.getCmp('txtNamaUnitLab').setValue('Laboratorium');
							Ext.getCmp('txtDokterPengirimL').setValue('-');
						}
						Ext.getCmp('cboDOKTER_viPenJasLab').disable();
						Ext.getCmp('btnLookupItemPemeriksaan').disable();
						Ext.getCmp('btnHpsBrsItemLabL').enable();
						//Ext.getCmp('btnHpsBrsItemLabL').disable();
						Ext.getCmp('btnSimpanPenJasLab').disable();
						Ext.getCmp('btnSimpanPenJasLab_kiriman').hide();
						Ext.getCmp('btnSimpanPenJasLab').show();
						ViewGridBawahPenjasLab(TrPenJasLab.form.ComboBox.notransaksi.getValue());
						Ext.getCmp('cboKelPasienLab').disable();
						Ext.getCmp('cboAsuransiLab').disable();
						Ext.getCmp('cboPerseoranganLab').disable();
						Ext.getCmp('cboPerusahaanRequestEntryLab').disable();
						//RefreshDataFilterPenJasRad();
						if(mBol === false)
						{
							loadMask.hide();
							ViewGridBawahPenjasLab(TrPenJasLab.form.ComboBox.notransaksi.getValue());
						};
					}
					else
					{
						loadMask.hide();
						ShowPesanErrorPenJasLab('Gagal menyimpan data ini. Hubungi Admin', 'Error');
					};
				}
			}
		)

	}
	else
	{
		if(mBol === true)
		{
			loadMask.hide();
			return false;
		};
	};

};

function getDokterPengirim(no_transaksi,kd_kasir){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/main/functionLAB/getDokterPengirim",
			params:{no_transaksi:no_transaksi,kd_kasir:kd_kasir} ,
			failure: function(o)
			{
				ShowPesanErrorPenJasLab('Hubungi Admin', 'Error');
			},
			success: function(o)
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					Ext.getCmp('txtDokterPengirimL').setValue(cst.nama);
				}
				else
				{
					ShowPesanErrorPenJasLab('Gagal membaca dokter pengirim', 'Error');
				};
			}
		}

	)
}


function ValidasiEntryPenJasLab(modul,mBolHapus)
{
	var x = 1;

	if((TrPenJasLab.form.ComboBox.pasien.getValue() == '') || (Ext.get('cboDOKTER_viPenJasLab').getValue() == '') || (Ext.get('dtpKunjunganL').getValue() == '') ||
		(dsTRDetailPenJasLabList.getCount() === 0 ) || (Ext.get('cboJKPenjasLab').getValue() == '') || (Ext.get('cboGDRPenJasLab').getValue() == ''))
	{
		if (TrPenJasLab.form.ComboBox.pasien.getValue() == '')
		{
			loadMask.hide();
			ShowPesanWarningPenJasLab('Nama Pasien tidak boleh kosong', 'Warning');
			x = 0;
		}
		else if (Ext.get('dtpKunjunganL').getValue() == '')
		{
			loadMask.hide();
			ShowPesanWarningPenJasLab('Tanggal kunjungan tidak boleh kosong', 'Warning');
			x = 0;
		}
		else if (Ext.getCmp('cboDOKTER_viPenJasLab').getValue() == '' || Ext.getCmp('cboDOKTER_viPenJasLab').getValue()  === '')
		{
			loadMask.hide();
			ShowPesanWarningPenJasLab('Dokter tidak boleh kosong', 'Warning');
			x = 0;
		}
		else if (dsTRDetailPenJasLabList.getCount() === 0)
		{
			loadMask.hide();
			ShowPesanWarningPenJasLab('Item pemeriksaan tidak boleh kosong','Warning');
			x = 0;
		} else if(Ext.get('cboJKPenjasLab').getValue() == ''){
			loadMask.hide();
			ShowPesanWarningPenJasLab('Jenis kelamin tidak boleh kosong','Warning');
			x = 0;
		} else if(Ext.get('cboGDRPenJasLab').getValue() == ''){
			loadMask.hide();
			ShowPesanWarningPenJasLab('Golongan darah tidak boleh kosong','Warning');
			x = 0;
		}
	};
	if(Ext.get('cboKelPasienLab').getValue() == ''){// &&  Ext.get('txtCustomerLamaHide').getValue() == ''){
		loadMask.hide();
		ShowPesanWarningPenJasLab('Jenis kelompok pasien tidak boleh kosong','Warning');
		x = 0;
	}
	if(Ext.getCmp('txtKdCustomerLamaHide').getValue() == '' && Ext.get('cboPerseoranganLab').getValue() == '' && Ext.get('cboPerusahaanRequestEntryLab').getValue() == ''&& Ext.get('cboAsuransiLab').getValue() == ''){
		loadMask.hide();
		ShowPesanWarningPenJasLab('Kelompok pasien tidak boleh kosong','Warning');
		x = 0;
	}
	return x;
};


function ShowPesanWarningPenJasLab(str, modul)
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorPenJasLab(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoPenJasLab(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};


//combo jenis kelamin
function mComboJK()
{
    var cboJKPenjasLab = new Ext.form.ComboBox
	(
		{
			id:'cboJKPenjasLab',
			x: 530,
            y: 130,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			tabIndex:3,
			selectOnFocus:true,
			forceSelection: true,
			fieldLabel: 'Jenis Kelamin ',
			editable: false,
			width:105,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Laki - Laki'], [2, 'Perempuan']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			//value:selectSetJKLab,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetJKLab=b.data.valueField ;
				},
			}
		}
	);
	return cboJKPenjasLab;
};

//combo golongan darah
function mComboGDarah()
{
    var cboGDRPenJasLab = new Ext.form.ComboBox
	(
		{
			id:'cboGDRPenJasLab',
			x: 830,
            y: 130,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			tabIndex:3,
			selectOnFocus:true,
			forceSelection: true,
			fieldLabel: 'Gol. Darah ',
			width:105,
			editable: false,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[0, '-'], [1, 'A+'],[2, 'B+'], [3, 'AB+'],[4, 'O+'], [5, 'A-'], [6, 'B-']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			//value:selectSetGDarahLab,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetGDarahLab=b.data.displayText ;
				},

			}
		}
	);
	return cboGDRPenJasLab;
};

//Combo Dokter Lookup
function mComboDOKTER()
{
    var Field = ['KD_DOKTER','NAMA'];

    dsdokter_viPenJasLab = new WebApp.DataStore({ fields: Field });
    dsdokter_viPenJasLab.load
	(
            {
                params:
                    {
                        Skip: 0,
                        Take: 1000,

                        Sort: 'nama',
                        Sortdir: 'ASC',
                        target: 'ViewDokterPenunjang',
                        param: "kd_unit = '41'"
                    }
            }
	);

    var cboDOKTER_viPenJasLab = new Ext.form.ComboBox
	(
		{
			id: 'cboDOKTER_viPenJasLab',
			x: 130,
			y: 130,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			emptyText: '',
			fieldLabel:  ' ',
			align: 'Right',
			width: 230,
			store: dsdokter_viPenJasLab,
			valueField: 'KD_DOKTER',
			displayField: 'NAMA',
			editable: true,
			listeners:
			{
				'select': function(a, b, c)
				{
					if(var_tampung_lab_order===true)
					{
					var_tampung_lab_order=true;
					Ext.getCmp('btnLookupItemPemeriksaan').disable();
					}else{
					var_tampung_lab_order=false;
					Ext.getCmp('btnLookupItemPemeriksaan').enable();
					}
				}
			}
		}
	);

    return cboDOKTER_viPenJasLab;
};





function mComboorder()
{


 var Field = ['no_transaksi','kd_kasir','tgl_transaksi','kd_spesial','kamar','tgl_lahir','kd_pasien','nama','nama_unit_asli',
				'alamat','kd_dokter','kd_unit','nama_unit','urut_masuk','dokter','Jenis_kelamin',
				'gol_darah','kd_customer','customer',
				'no_kamar','kd_spesial','tgl','kd_tarif','kelpasien','display'];
    dspasienorder_mng = new WebApp.DataStore({ fields: Field });
	total_pasien_order_mng();
	load_data_pasienorder();
	cbopasienorder_mng= new Ext.form.ComboBox
	(
		{
			id: 'cbopasienorder_mng',
			typeAhead		: true,
			triggerAction	: 'all',
			lazyRender		: true,
			mode			: 'local',
			emptyText: '',
			fieldLabel:  ' ',
			align: 'Right',
			width: 230,
			store: dspasienorder_mng,
			valueField: 'kd_pasien',
			displayField: 'display',
			//hideTrigger		: true,
			listeners:
			{
				select	: function(a,b,c){
								    var_tampung_lab_order=true;
									ViewGridBawahPenjasLab(b.data.no_transaksi,b.data);
									TrPenJasLab.form.ComboBox.kdpasien.disable();
									TrPenJasLab.form.ComboBox.kdpasien.setValue(b.data.kd_pasien);
									TrPenJasLab.form.ComboBox.pasien.disable();
									TrPenJasLab.form.ComboBox.pasien.setValue(b.data.nama);
									TrPenJasLab.form.ComboBox.notransaksi.setValue(b.data.no_transaksi);
									TrPenJasLab.form.ComboBox.notransaksi.setReadOnly(true);


									Ext.getCmp('btnLookupItemPemeriksaan').disable();
									Ext.getCmp('btnHpsBrsItemLabL').enable();
									//Ext.getCmp('btnHpsBrsItemLabL').disable();
									Ext.getCmp('btnSimpanPenJasLab').disable();
									Ext.getCmp('btnSimpanPenJasLab_kiriman').hide();
									TrPenJasLab.vars.kd_tarif=b.data.kd_tarif;

									Ext.Ajax.request(
									{
										url: baseURL + "index.php/main/functionLAB/getCurrentShiftLab",
										params: {
											command: '0',
											// parameter untuk url yang dituju (fungsi didalam controller)
										},
										failure: function(o)
										{
											var cst = Ext.decode(o.responseText);
										},
										success: function(o) {
											tampungshiftsekarang=o.responseText;
										}
									});

								},
				   keyUp: function(a,b,c){
				    	$this1=this;
				    	if((b.getKey()>=65 && b.getKey()<=90)|| (b.getKey()>=48 && b.getKey()<=57) || b.getKey()==32 || b.getKey()==8 || b.getKey()==46){
				    		clearTimeout(this.time);

				    		this.time=setTimeout(function(){
			    				if($this1.lastQuery != '' ){
				    				var value=$this1.lastQuery;
				    				var param={};
		        		    		param['text']=$this1.lastQuery;
		        		    		load_data_pasienorder(param);
									viewGridOrderAll_RASEPRWI(param);

			    				}
		    		    	},1000);
				    	}
				    }
			}
		}
	);return cbopasienorder_mng;
};


function mCboKelompokpasien(){
 var cboKelPasienLab = new Ext.form.ComboBox
	(
		{

			id:'cboKelPasienLab',
			fieldLabel: 'Kelompok Pasien',
			x: 130,
			y: 160,
			mode: 'local',
			width: 130,
			//anchor: '95%',
			forceSelection: true,
			lazyRender:true,
			triggerAction: 'all',
			editable: false,
			store: new Ext.data.ArrayStore
			(
				{
				id: 0,
				fields:
				[
					'Id',
					'displayText'
				],
				   data: [[1, 'Perseorangan'], [2, 'Perusahaan'], [3, 'Asuransi']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetKelPasienLab,
			selectOnFocus: true,
			tabIndex:22,
			listeners:
			{
				'select': function(a, b, c)
					{
					   Combo_Select(b.data.displayText);
					   if(b.data.displayText == 'Perseorangan')
					   {

					   }
					   else if(b.data.displayText == 'Perusahaan')
					   {

					   }
					   else if(b.data.displayText == 'Asuransi')
					   {

					   }
					},
				'render': function(a,b,c)
				{
					Ext.getCmp('txtNamaPesertaAsuransiLab').hide();
					Ext.getCmp('txtNoAskesLab').hide();
					Ext.getCmp('txtNoSJPLab').hide();
					Ext.getCmp('cboPerseoranganLab').hide();
					Ext.getCmp('cboAsuransiLab').hide();
					Ext.getCmp('cboPerusahaanRequestEntryLab').hide();

				}
			}

		}
	);
	return cboKelPasienLab;
};

function mComboPerseorangan()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPeroranganLabRequestEntry = new WebApp.DataStore({fields: Field});
    dsPeroranganLabRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
				Sort: 'customer',
			    Sortdir: 'ASC',
			    target: 'ViewComboKontrakCustomer',
			    param: 'jenis_cust=0'
			}
		}
	)
    var cboPerseoranganLab = new Ext.form.ComboBox
	(
		{
			id:'cboPerseoranganLab',
			x: 280,
			y: 160,
			editable: false,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			fieldLabel: 'Jenis',
			tabIndex:23,
			width:150,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			store:dsPeroranganLabRequestEntry,
			value:selectSetPerseoranganLab,
			listeners:
			{
				'select': function(a,b,c)
				{
				    selectSetPerseoranganLab=b.data.displayText ;
				}
			}
		}
	);
	return cboPerseoranganLab;
};

function mComboPerusahaan()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
				Sort: 'customer',
			    Sortdir: 'ASC',
			    target: 'ViewComboKontrakCustomer',
			    param: 'jenis_cust=1'
			}
		}
	)
    var cboPerusahaanRequestEntryLab = new Ext.form.ComboBox
	(
		{
		    id: 'cboPerusahaanRequestEntryLab',
			x: 280,
			y: 160,
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    fieldLabel: 'Perusahaan',
		    align: 'Right',
			width:150,
		    store: dsPerusahaanRequestEntry,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
		    listeners:
			{
			    'select': function(a,b,c)
				{
					selectSetPerusahaanLab=b.data.valueField ;
				}
			}
		}
	);

    return cboPerusahaanRequestEntryLab;
};

function mComboAsuransi()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'customer',
                Sortdir: 'ASC',
                target:'ViewComboKontrakCustomer',
                param: 'jenis_cust=2'
            }
        }
    )
    var cboAsuransiLab = new Ext.form.ComboBox
	(
		{
			id:'cboAsuransiLab',
			x: 280,
			y: 160,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			fieldLabel: 'Asuransi',
			align: 'Right',
			width:150,
			//anchor: '95%',
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetAsuransiLab=b.data.valueField ;
				}
			}
		}
	);
	return cboAsuransiLab;
};


function Combo_Select(combo)
{
   var value = combo

   if(value == "Perseorangan")
   {
        Ext.getCmp('txtNamaPesertaAsuransiLab').hide()
        Ext.getCmp('txtNoAskesLab').hide()
        Ext.getCmp('txtNoSJPLab').hide()
        Ext.getCmp('cboPerseoranganLab').show()
        Ext.getCmp('cboAsuransiLab').hide()
        Ext.getCmp('cboPerusahaanRequestEntryLab').hide()


   }
   else if(value == "Perusahaan")
   {
        Ext.getCmp('txtNamaPesertaAsuransiLab').hide()
        Ext.getCmp('txtNoAskesLab').hide()
        Ext.getCmp('txtNoSJPLab').hide()
        Ext.getCmp('cboPerseoranganLab').hide()
        Ext.getCmp('cboAsuransiLab').hide()
        Ext.getCmp('cboPerusahaanRequestEntryLab').show()

   }
   else
       {
         Ext.getCmp('txtNamaPesertaAsuransiLab').show()
         Ext.getCmp('txtNoAskesLab').show()
         Ext.getCmp('txtNoSJPLab').show()
         Ext.getCmp('cboPerseoranganLab').hide()
         Ext.getCmp('cboAsuransiLab').show()
         Ext.getCmp('cboPerusahaanRequestEntryLab').hide()

       }
}



function addNewData(){
	TmpNotransaksi='';
	KdKasirAsal='';
	TglTransaksiAsal='';
	Kd_Spesial='';
	vkode_customer = '';
	No_Kamar='';
	TrPenJasLab.vars.kd_tarif='';
	Ext.getCmp('btnSimpanPenJasLab_kiriman').hide();
	Ext.getCmp('btnSimpanPenJasLab').show();
	TrPenJasLab.form.ComboBox.kdpasien.setValue('');
	Ext.getCmp('txtDokterPengirimL').setValue('');
	TrPenJasLab.form.ComboBox.notransaksi.setValue('');
	Ext.getCmp('txtNamaUnitLab').setValue('');
	Ext.getCmp('txtKdUnitLab').setValue('');
	Ext.getCmp('txtKdDokter').setValue('');
	Ext.getCmp('cboDOKTER_viPenJasLab').setValue('');
	Ext.getCmp('txtAlamatL').setValue('');
	TrPenJasLab.form.ComboBox.pasien.setValue('');
	Ext.getCmp('dtpTtlL').setValue(nowTglTransaksi);
	Ext.getCmp('txtKdUrutMasuk').setValue('');
	Ext.getCmp('cboJKPenjasLab').setValue('');
	Ext.getCmp('cboGDRPenJasLab').setValue('')
	Ext.getCmp('txtKdCustomerLamaHide').setValue('');//tampung kd customer untuk transaksi selain kunjungan langsung
	Ext.getCmp('cboKelPasienLab').setValue('');
	//Ext.getCmp('txtCustomerLamaHide').setValue('');
	Ext.getCmp('cboAsuransiLab').setValue('');
	Ext.getCmp('txtNamaPesertaAsuransiLab').setValue('');
	Ext.getCmp('txtNoSJPLab').setValue('');
	Ext.getCmp('txtNoAskesLab').setValue('');
	Ext.getCmp('cboPerusahaanRequestEntryLab').setValue('');
	Ext.getCmp('txtKdDokterPengirim').setValue('');

	//Ext.getCmp('txtCustomerLamaHide').hide();
	Ext.getCmp('cboAsuransiLab').hide();
	Ext.getCmp('txtNamaPesertaAsuransiLab').hide();
	Ext.getCmp('txtNoSJPLab').hide();
	Ext.getCmp('txtNoAskesLab').hide();
	Ext.getCmp('cboPerusahaanRequestEntryLab').hide();
	Ext.getCmp('cboKelPasienLab').show();

	Ext.getCmp('btnLookupItemPemeriksaan').disable();
	Ext.getCmp('btnHpsBrsItemLabL').enable();
	//Ext.getCmp('btnHpsBrsItemLabL').disable();

	TrPenJasLab.form.ComboBox.notransaksi.enable();
	Ext.getCmp('txtDokterPengirimL').enable();
	Ext.getCmp('txtNamaUnitLab').enable();
	Ext.getCmp('cboDOKTER_viPenJasLab').enable();
	TrPenJasLab.form.ComboBox.pasien.enable();
	TrPenJasLab.form.ComboBox.kdpasien.enable();
	Ext.getCmp('txtAlamatL').enable();
	Ext.getCmp('cboJKPenjasLab').enable();
	Ext.getCmp('cboGDRPenJasLab').enable();
	Ext.getCmp('cboKelPasienLab').enable();
	Ext.getCmp('dtpTtlL').enable();
	Ext.getCmp('dtpKunjunganL').enable();
	Ext.getCmp('btnSimpanPenJasLab').enable();
	TrPenJasLab.form.ComboBox.notransaksi.setReadOnly(false);
	Ext.getCmp('txtNamaUnitLab').setReadOnly(true);
	//Ext.getCmp('cboDOKTER_viPenJasLab').setReadOnly(false);
	TrPenJasLab.form.ComboBox.pasien.setReadOnly(false);
	Ext.getCmp('txtAlamatL').setReadOnly(false);
	Ext.getCmp('cboJKPenjasLab').setReadOnly(false);
	Ext.getCmp('cboGDRPenJasLab').setReadOnly(false);
	Ext.getCmp('cboKelPasienLab').setReadOnly(false);
	Ext.getCmp('dtpTtlL').setReadOnly(false);
	//Ext.getCmp('dtpKunjunganL').setReadOnly(true);
	gridDTItemTest.enable();
	ViewGridBawahLookupPenjasLab('');
	ViewGridBawahPenjasLab('');
	var_tampung_lab_order=false;
}


function PilihDokterLookUp(NoTrans,TglTrans,kdPrdk,kdTrf,tglBerlaku,trf)
{


	RefreshDataTrDokter(NoTrans,TglTrans,kdPrdk);

	 var FormPilihDokter =
	  {
			layout: 'column',
						border: false,
						anchor: '90%',
						items:
						[

							{
								columnWidth: 0.18,
								layout: 'form',
								labelWidth:70,
								border: false,
								items:
								[
								{
								xtype:'button',
								text:'Simpan',
							 	width:70,
								hideLabel:true,
								id: 'BtnOktrDokter',
								handler:function()
								{
								if(dataTrDokter.length == '' || dataTrDokter.length== 'undefined')
								{
									var jumlah =0;
								}
								else
								{
									var jumlah = dataTrDokter.length;
								}
								for(var i = 0 ;i< dsTrDokter.getCount()  ;i++)
								{
								dataTrDokter.push({
								index		: i,
								no_tran 	: NoTrans,
								//urut		: Urt,
								tgl_trans 	: TglTrans,
								kd_produk 	: kdPrdk,
								kd_tarif	: kdTrf,
								tgl_berlaku	: tglBerlaku,
								tarif		: trf,
								kd_dokter 	: dsTrDokter.data.items[i].data.KD_DOKTER ,
								kd_job 		: dsTrDokter.data.items[i].data.KD_JOB ,
								});
								}
								FormLookUDokter.close();
								}
								},
								],
								},
								{
								columnWidth: 0.2,
								layout: 'form',
								labelWidth:70,
								border: false,
								items:
								[
								{
								xtype:'button',
								text:'Batal' ,
								width:70,
								hideLabel:true,
								id: 'BtnCancelGantiDokter',
								handler:function()
									{
								dataTrDokter = [];
								FormLookUDokter.close()
									}
								}
								]
								}


								]
  }

    var GridTrDokterColumnModel =  new Ext.grid.ColumnModel([
         new Ext.grid.RowNumberer(),
        {
        	 header			: 'kd_dokter',
        	 dataIndex		: 'KD_DOKTER',
			 width			: 80,
        	 menuDisabled	: true,
        	 hidden 		: false
        },{
            header			:'Nama Dokter',
            dataIndex		: 'NAMA',
            sortable		: false,
            hidden			: false,
			menuDisabled	: true,
			width			: 200,
            editor			: getTrDokter(dsTrDokter)
	    },{
            header			: 'Job',
            dataIndex		: 'KD_JOB',
            width			:150,
		   	menuDisabled	:true,
			editor			: JobDokter(),

        },
        ]
    );

  var GridPilihDokter= new Ext.grid.EditorGridPanel({
        title		: '',
		id			: 'GridDokterTr',
		stripeRows	: true,
		width		: 490,
		height		: 245,
        store		: RefreshDataTrDokter(NoTrans,TglTrans,kdPrdk),
        border		: false,
        frame		: false,
        anchor		: '100% 60%',
        autoScroll	: true,
        cm			: GridTrDokterColumnModel,

        //viewConfig	: {forceFit: true},
		listeners	: {
			rowclick: function( $this, rowIndex, e )
			{
        	trcellCurrentTindakan = rowIndex;
    		},
			celldblclick: function(gridView,htmlElement,columnIndex,dataRecord){
   		  }
		}
    });



	var PanelTrDokter = new Ext.Panel
	({
            id: 'PanelPilitTrDokter',
            region: 'center',
			width: 490,
			height: 260,
            border:false,
            layout: 'form',
            frame:true,
            anchor: '100% 8.0001%',
            autoScroll:false,

            items:
            [
			GridPilihDokter,
			{xtype: 'tbspacer',height: 3, width:100},
			FormPilihDokter
			]
	})


    var lebar = 500;
    var FormLookUDokter = new Ext.Window
    (
        {
            id: 'gridTrDokter',
            title: 'Pilih Dokter Tindakan',
            closeAction: 'destroy',
            width: lebar,
            height: 270,
            border: false,
            resizable: false,
            plain: true,
            layout: 'column',
            iconCls: 'Request',
            modal: true,
			tbar		:[
						{
						xtype	: 'button',
						id		: 'btnaddtrdokter',
						iconCls	: 'add',
						text	: 'Tambah Data Dokter',
						handler	:  function()
						{
						dataTrDokter = [];
						TambahBaristrDokter(dsTrDokter);
						}
						},
						'-',
						{
						xtype	: 'button',
						id		: 'btndeltrdokter',
						iconCls	: 'remove',
						text	: 'Delete Data Dokter',
						handler	: function()
						{
						    if (dsTrDokter.getCount() > 0 )
							{
                            if(trcellCurrentTindakan != undefined)
							{
                             HapusDataTrDokter(dsTrDokter);
                            }
                        	}else{
                            ShowPesanWarningDiagnosa('Pilih record ','Hapus data');
                    		}
							}
						},
						'-',
			],
           	items: [
			//GridPilihDokter,

			PanelTrDokter
			],
            listeners:
            {
            }
        }
    );


 FormLookUDokter.show();




};

var mtrDataDokter = Ext.data.Record.create([
	   {name: 'KD_DOKTER', mapping:'KD_DOKTER'},
	   {name: 'NAMA', mapping:'NAMA'},
	   {name: 'KD_JOB', mapping:'KD_JOB'},
	]);

function TambahBaristrDokter(store){
    var x=true;
    if (x === true) {
        var p = BaristrDokter();
        store.insert(store.getCount(), p);
    }
}

function BaristrDokter(){
	var p = new mtrDataDokter({
		'KD_DOKTER':'',
		'NAMA':'',
	    'KD_JOB':'',
	});
	return p;
};

function getTrDokter(store){
	var trDokterData = new Ext.form.ComboBox({
	   typeAhead: true,
	   triggerAction: 'all',
	   lazyRender: true,
	   mode: 'local',
	   hideTrigger:true,
	   forceSelection: true,
	   selectOnFocus:true,
	   store: GetDokter(),
	   valueField: 'NAMA',
	   displayField: 'NAMA',
	   anchor: '95%',
	   listeners:{
		   select: function(a, b, c){
			  store.data.items[trcellCurrentTindakan].data.KD_DOKTER = b.data.KD_DOKTER;
		   }
	   }
	});
	return trDokterData;
}

function GetDokter(){
	var dataDokter  = new WebApp.DataStore({ fields: ['KD_DOKTER','NAMA'] });
	dataDokter.load({
		params: {
			Skip: 0,
			Take: 50,
			target:'ViewComboDokter',
			param: ''
		}
	});
	return dataDokter;
}

function JobDokter(){
	var combojobdokter = new Ext.form.ComboBox({
		typeAhead		: true,
		triggerAction	: 'all',
		lazyRender		: true,
		mode			: 'local',
		anchor 			: '96.8%',
		emptyText		: '',
		store			: new Ext.data.ArrayStore({
			fields	: ['Id','displayText'],
			data	: [[1, 'Dokter'],[2, 'Dokter Anastesi']]
		}),
		valueField		: 'displayText',
		displayField	: 'displayText'
	});
	return combojobdokter;
};


function RefreshDataTrDokter(NoTrans,TglTrans,kdPrdk)
{
	dsTrDokter.load({
		params	: {
			Skip	: 0,
			Take	: 50,
			target	:'ViewTrDokter',
			param	:"b.kd_kasir='06' and b.no_transaksi=~"+NoTrans+"~ and b.tgl_transaksi=~"+TglTrans+"~ and a.kd_produk = ~"+kdPrdk+"~"
		}
	});

	return dsTrDokter;
}

function HapusDataTrDokter(store)
{

    if ( trcellCurrentTindakan != undefined ){
        if (store.data.items[trcellCurrentTindakan].data.KD_DOKTER != '' && store.data.items[trcellCurrentTindakan].data.KD_PRODUK != ''){
					DataDeleteTrDokter(store);
					dsTrDokter.removeAt(trcellCurrentTindakan);
				}

        }/*else{
            dsTrDokter.removeAt(trcellCurrentTindakan);
        }*/


}

function DataDeleteTrDokter(store){
    Ext.Ajax.request({
        url: baseURL + "index.php/main/DeleteDataObj",
        params:
		{
			Table			:'ViewTrDokter',
			no_transaksi	:Ext.getCmp('txtNoTransaksiKasirIGD').getValue(),
			kd_unit			:Ext.getCmp('txtKdUnitIGD').getValue(),
			kd_produk		:store.data.items[trcellCurrentTindakan].data.KD_PRODUK,
			kd_kasir		:'06',
			kd_dokter		:store.data.items[trcellCurrentTindakan].data.KD_DOKTER,
			urut			:PenataJasaRJ.dsGridTindakan.data.items[cellCurrentTindakan].data.URUT,
			tgl_transaksi	:PenataJasaRJ.dsGridTindakan.data.items[cellCurrentTindakan].data.TGL_TRANSAKSI,
		},
		//getParamDataDeleteKasirIGDDetail(),
        success: function(o){
            var cst = Ext.decode(o.responseText);
            if (cst.success === true){
                ShowPesanInfoDiagnosa(nmPesanHapusSukses,nmHeaderHapusData);
                store.removeAt(CurrentKasirIGD.row);
				RefreshDataKasirIGDDetail(Ext.getCmp('txtNoTransaksiKasirIGD').getValue());
                RefreshDataTrDokter(Ext.getCmp('txtNoTransaksiKasirIGD').getValue(),PenataJasaRJ.dsGridTindakan.data.items[cellCurrentTindakan].data.URUT,PenataJasaRJ.dsGridTindakan.data.items[cellCurrentTindakan].data.TGL_TRANSAKSI,PenataJasaRJ.dsGridTindakan.data.items[cellCurrentTindakan].data.KD_PRODUK);
				trcellCurrentTindakan=undefined;
            }else{
                ShowPesanInfoDiagnosa(nmPesanHapusError,nmHeaderHapusData);
				RefreshDataKasirIGDDetail(Ext.getCmp('txtNoTransaksiKasirIGD').getValue());
				RefreshDataTrDokter(Ext.getCmp('txtNoTransaksiKasirIGD').getValue(),PenataJasaRJ.dsGridTindakan.data.items[cellCurrentTindakan].data.URUT,PenataJasaRJ.dsGridTindakan.data.items[cellCurrentTindakan].data.TGL_TRANSAKSI,PenataJasaRJ.dsGridTindakan.data.items[cellCurrentTindakan].data.KD_PRODUK);
            }
        }
    });
}

