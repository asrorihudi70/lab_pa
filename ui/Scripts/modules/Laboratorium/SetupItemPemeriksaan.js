var dataSource_viSetupItemPemeriksaan;
var selectCount_viSetupItemPemeriksaan=50;
var NamaForm_viSetupItemPemeriksaan="Setup Item Pemeriksaan";
var mod_name_viSetupItemPemeriksaan="Setup Item Pemeriksaan";
var now_viSetupItemPemeriksaan= new Date();
var rowSelected_viSetupItemPemeriksaan;
var setLookUps_viSetupItemPemeriksaan;
var tanggal = now_viSetupItemPemeriksaan.format("d/M/Y");
var jam = now_viSetupItemPemeriksaan.format("H/i/s");
var tmpkriteria;
var DataGridJenisObat;
var GridDataView_viSetupItemPemeriksaan;


var CurrentData_viSetupItemPemeriksaan =
{
	data: Object,
	details: Array,
	row: 0
};

var SetupItemPemeriksaan={};
SetupItemPemeriksaan.form={};
SetupItemPemeriksaan.func={};
SetupItemPemeriksaan.vars={};
SetupItemPemeriksaan.func.parent=SetupItemPemeriksaan;
SetupItemPemeriksaan.form.ArrayStore={};
SetupItemPemeriksaan.form.ComboBox={};
SetupItemPemeriksaan.form.DataStore={};
SetupItemPemeriksaan.form.Record={};
SetupItemPemeriksaan.form.Form={};
SetupItemPemeriksaan.form.Grid={};
SetupItemPemeriksaan.form.Panel={};
SetupItemPemeriksaan.form.TextField={};
SetupItemPemeriksaan.form.Button={};

SetupItemPemeriksaan.form.ArrayStore.a= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_lab', 'nama_test'],
	data: []
});

CurrentPage.page = dataGrid_viSetupItemPemeriksaan(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

function dataGrid_viSetupItemPemeriksaan(mod_id_viSetupItemPemeriksaan){	
 	// Field kiriman dari Project Net.
    var FieldMaster_viSetupItemPemeriksaan = 
	[
		'kd_lab', 'nama_test' 
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viSetupItemPemeriksaan = new WebApp.DataStore
	({
        fields: FieldMaster_viSetupItemPemeriksaan
    });
    dataGriSetupItemPemeriksaan();
    // Grid lab Perencanaan # --------------
	GridDataView_viSetupItemPemeriksaan = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viSetupItemPemeriksaan,
			autoScroll: false,
			columnLines: true,
			flex:1,
			border:true,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viSetupItemPemeriksaan = undefined;
							rowSelected_viSetupItemPemeriksaan = dataSource_viSetupItemPemeriksaan.getAt(row);
							CurrentData_viSetupItemPemeriksaan
							CurrentData_viSetupItemPemeriksaan.row = row;
							CurrentData_viSetupItemPemeriksaan.data = rowSelected_viSetupItemPemeriksaan.data;
							//DataInitSetupItemPemeriksaan(rowSelected_viSetupItemPemeriksaan.data);
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viSetupItemPemeriksaan = dataSource_viSetupItemPemeriksaan.getAt(ridx);
					if (rowSelected_viSetupItemPemeriksaan != undefined)
					{
						DataInitSetupItemPemeriksaan(rowSelected_viSetupItemPemeriksaan.data);
						//setLookUp_viSetupItemPemeriksaan(rowSelected_viSetupItemPemeriksaan.data);
					}
					else
					{
						//setLookUp_viSetupItemPemeriksaan();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Setup Unit
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						header: 'Kode Lab',
						dataIndex: 'kd_lab',
						hideable:false,
						menuDisabled: true,
						width: 40
						
					},
					//-------------- ## --------------
					{
						header: 'Nama',
						dataIndex: 'nama_test',
						hideable:false,
						menuDisabled: true,
						width: 90
					}
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viSetupItemPemeriksaan',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Items',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viSetupItemPemeriksaan',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viSetupItemPemeriksaan != undefined)
							{
								DataInitSetupItemPemeriksaan(rowSelected_viSetupItemPemeriksaan.data);
							}
							
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			//bbar : bbar_paging(mod_name_viSetupItemPemeriksaan, selectCount_viSetupItemPemeriksaan, dataSource_viSetupItemPemeriksaan),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
	)	
	var PanelTabSetupItemPemeriksaan = new Ext.FormPanel({
        labelAlign: 'top',
		layout:'form',
		border: false,  
		autoHeight:true,
        title: '',
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [PanelInputUnit()]
		
	})
	// Kriteria filter pada Grid # --------------
    var FrmData_viSetupItemPemeriksaan = new Ext.Panel
    (
		{
			title: NamaForm_viSetupItemPemeriksaan,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viSetupItemPemeriksaan,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ PanelTabSetupItemPemeriksaan,
					GridDataView_viSetupItemPemeriksaan],
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAddJenis_viSetupItemPemeriksaan',
						handler: function(){
							AddNewSetupItemPemeriksaan();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viSetupItemPemeriksaan',
						handler: function()
						{
							loadMask.show();
							dataSave_viSetupItemPemeriksaan();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viSetupItemPemeriksaan',
						handler: function()
						{
							Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
								if (button == 'yes'){
									loadMask.show();
									dataDelete_viSetupItemPemeriksaan();
								}
							});
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						id: 'btnRefresh_viSetupPabrik',
						handler: function()
						{
							dataSource_viSetupItemPemeriksaan.removeAll();
							dataGriSetupItemPemeriksaan();
						}
					},
					{
						xtype: 'tbseparator'
					}
					
				]
			}
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_viSetupItemPemeriksaan;
    //-------------- # End form filter # --------------
}

function PanelInputUnit(rowdata){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: false,
						width: 500,
						height: 55,
						anchor: '100% 100%',
						items:
						[
							{
								x: 10,
								y: 0,
								xtype: 'label',
								text: 'Kode lab'
							},
							{
								x: 120,
								y: 0,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 0,
								xtype: 'textfield',
								id: 'txtKdLab_SetupItemPemeriksaan',
								name: 'txtKdLab_SetupItemPemeriksaan',
								width: 100,
								readOnly: true,
								tabIndex:1,
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) 
										{
											
										} 						
									},
									
								}
							},
							{
								x: 10,
								y: 30,
								xtype: 'label',
								text: 'Nama tes'
							},
							{
								x: 120,
								y: 30,
								xtype: 'label',
								text: ':'
							},
							SetupItemPemeriksaan.vars.nama=new Nci.form.Combobox.autoComplete({
								x: 130,
								y: 30,
								tabIndex:2,
								id		: 'txtComboNama_SetupItemPemeriksaan',
								store	: SetupItemPemeriksaan.form.ArrayStore.a,
								select	: function(a,b,c){
									
									Ext.getCmp('txtKdLab_SetupItemPemeriksaan').setValue(b.data.kd_lab);
									
									GridDataView_viSetupItemPemeriksaan.getView().refresh();
									
								},
								onShowList:function(a){
									dataSource_viSetupItemPemeriksaan.removeAll();
									
									var recs=[],
									recType=dataSource_viSetupItemPemeriksaan.recordType;
										
									for(var i=0; i<a.length; i++){
										recs.push(new recType(a[i]));
									}
									dataSource_viSetupItemPemeriksaan.add(recs);
									
								},
								insert	: function(o){
									return {
										kd_lab      	: o.kd_lab,
										nama_test 		: o.nama_test,
										text			:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_lab+'</td><td width="200">'+o.nama_test+'</td></tr></table>'
									}
								},
								url		: baseURL + "index.php/lab/functionSetupItemPemeriksaan/getItemGrid",
								valueField: 'nama_test',
								displayField: 'text',
								listWidth: 280
							})
							
							
						]
					}
				]
			}
		]		
	};
        return items;
}
//------------------end---------------------------------------------------------

function dataGriSetupItemPemeriksaan(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/lab/functionSetupItemPemeriksaan/getItemGrid",
			params: {text:''},
			failure: function(o)
			{
				ShowPesanErrorSetupItemPemeriksaan('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dataSource_viSetupItemPemeriksaan.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dataSource_viSetupItemPemeriksaan.add(recs);
					
					
					
					GridDataView_viSetupItemPemeriksaan.getView().refresh();
				}
				else 
				{
					ShowPesanErrorSetupItemPemeriksaan('Gagal membaca data items', 'Error');
				};
			}
		}
		
	)
	
}

function dataSave_viSetupItemPemeriksaan(){
	if (ValidasiSaveSetupItemPemeriksaan(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/lab/functionSetupItemPemeriksaan/save",
				params: getParamSaveSetupItemPemeriksaan(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupItemPemeriksaan('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						Ext.getCmp('txtKdLab_SetupItemPemeriksaan').setValue(cst.kd_lab);
						ShowPesanInfoSetupItemPemeriksaan('Berhasil menyimpan data ini','Information');
						dataSource_viSetupItemPemeriksaan.removeAll();
						dataGriSetupItemPemeriksaan();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupItemPemeriksaan('Gagal menyimpan data ini', 'Error');
						dataSource_viSetupItemPemeriksaan.removeAll();
						dataGriSetupItemPemeriksaan();
					};
				}
			}
			
		)
	}
}

function dataDelete_viSetupItemPemeriksaan(){
	if (ValidasiDeleteSetupItemPemeriksaan(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/lab/functionSetupItemPemeriksaan/delete",
				params: getParamDeleteSetupItemPemeriksaan(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupItemPemeriksaan('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupItemPemeriksaan('Berhasil menghapus data ini','Information');
						AddNewSetupItemPemeriksaan()
						dataSource_viSetupItemPemeriksaan.removeAll();
						dataGriSetupItemPemeriksaan();
						
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupItemPemeriksaan('Gagal menghapus data ini', 'Error');
						dataSource_viSetupItemPemeriksaan.removeAll();
						dataGriSetupItemPemeriksaan();
					};
				}
			}
			
		)
	}
}

function AddNewSetupItemPemeriksaan(){
	Ext.getCmp('txtKdLab_SetupItemPemeriksaan').setValue('');
	SetupItemPemeriksaan.vars.nama.setValue('');
};

function DataInitSetupItemPemeriksaan(rowdata){
	Ext.getCmp('txtKdLab_SetupItemPemeriksaan').setValue(rowdata.kd_lab);
	SetupItemPemeriksaan.vars.nama.setValue(rowdata.nama_test);
};

function getParamSaveSetupItemPemeriksaan(){
	var	params =
	{
		KdLab:Ext.getCmp('txtKdLab_SetupItemPemeriksaan').getValue(),
		Nama:SetupItemPemeriksaan.vars.nama.getValue()
	}
   
    return params
};

function getParamDeleteSetupItemPemeriksaan(){
	var	params =
	{
		KdLab:Ext.getCmp('txtKdLab_SetupItemPemeriksaan').getValue()
	}
   
    return params
};

function ValidasiSaveSetupItemPemeriksaan(modul,mBolHapus){
	var x = 1;
	if(SetupItemPemeriksaan.vars.nama.getValue() === ''){
		loadMask.hide();
			ShowPesanWarningSetupItemPemeriksaan('Nama tes masih kosong', 'Warning');
			x = 0;
	} 
	return x;
};

function ValidasiDeleteSetupItemPemeriksaan(modul,mBolHapus){
	var x = 1;
	if(Ext.getCmp('txtKdLab_SetupItemPemeriksaan').getValue() === ''){
		loadMask.hide();
		ShowPesanWarningSetupItemPemeriksaan('Pilih item yang akan dihapus', 'Warning');
		x = 0;
		
	} 
	return x;
};



function ShowPesanWarningSetupItemPemeriksaan(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorSetupItemPemeriksaan(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoSetupItemPemeriksaan(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};