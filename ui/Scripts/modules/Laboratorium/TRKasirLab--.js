// Data Source ExtJS # --------------

/**
*	Nama File 		: TRKasirLab.js
*	Menu 			: 
*	Model id 		: 
*	Keterangan 		: Kasir Laboratorium adalah proses untuk pembayaran pasien pada laboratorium
*	Di buat tanggal : 20 Agustus 2014
*	Oleh 			: SDY_RI
*/

// Deklarasi Variabel pada Kasir Laboratorium # --------------

var dataSource_viKasirLab;
var selectCount_viKasirLab=50;
var NamaForm_viKasirLab="Kasir Laboratorium ";
var mod_name_viKasirLab="viKasirLab";
var now_viKasirLab= new Date();
var addNew_viKasirLab;
var rowSelected_viKasirLab;
var setLookUps_viKasirLab;
var mNoKunjungan_viKasirLab='';

var CurrentData_viKasirLab =
{
	data: Object,
	details: Array,
	row: 0
};



CurrentPage.page = dataGrid_viKasirLab(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);

// End Deklarasi Variabel pada Kasir Laboratorium # --------------

// Start Project Kasir Laboratorium # --------------

// --------------------------------------- # Start Function # ---------------------------------------
// ## Silahkan tempatkan semua fungsi eksekusi disini

// --------------------------------------- # End Function # ---------------------------------------

// --------------------------------------- # Start Form # ---------------------------------------

/**
*	Function : dataGrid_viKasirLab
*	
*	Sebuah fungsi untuk menampilkan data Grid di depan dengan pencarian
*/

function dataGrid_viKasirLab(mod_id_viKasirLab)
{	
    // Field kiriman dari Project Net.
    var FieldMaster_viKasirLab = 
	[
		 'NO_KUNJUNGAN', 'KD_KELOMPOK', 'KD_UNIT', 'KD_DOKTER', 'KD_CUSTOMER', 'KD_PASIEN', 
		 'TGL_KUNJUNGAN','JAM_KUNJUNGAN', 'TINGGI_BADAN', 'BERAT_BADAN', 'TEKANAN_DRH', 
		 'NADI','ALERGI', 'KELUHAN', 'RUJUK_RAD', 'RUJUK_LAB', 'TAHAP_PROSES', 'PASIEN_BARU',
		 'NAMA_UNIT','KELOMPOK', 'DOKTER', 'CUSTOMER', 'PS_BARU','KD_PENDIDIKAN','KD_STS_MARITAL', 
		 'KD_AGAMA','KD_PEKERJAAN','NAMA','TEMPAT_LAHIR','TGL_LAHIR','JENIS_KELAMIN','ALAMAT', 
		 'NO_TELP','NO_HP','GOL_DARAH','PENDIDIKAN','STS_MARITAL','AGAMA','PEKERJAAN','JNS_KELAMIN',
		 'TAHUN','BULAN','HARI'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viKasirLab = new WebApp.DataStore
	({
        fields: FieldMaster_viKasirLab
    });
    
    // Grid Kasir Laboratorium # --------------
	var GridDataView_viKasirLab = new Ext.grid.EditorGridPanel
    (
		{
			xtype: 'editorgrid',
			title: '',
			store: dataSource_viKasirLab,
			autoScroll: true,
			columnLines: true,
			border:false,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel: new Ext.grid.RowSelectionModel
			// Tanda aktif saat salah satu baris dipilih # --------------
			(
				{
					singleSelect: true,
					listeners:
					{
						rowselect: function(sm, row, rec)
						{
							rowSelected_viKasirLab = undefined;
							rowSelected_viKasirLab = dataSource_viKasirLab.getAt(row);
							CurrentData_viKasirLab
							CurrentData_viKasirLab.row = row;
							CurrentData_viKasirLab.data = rowSelected_viKasirLab.data;
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viKasirLab = dataSource_viKasirLab.getAt(ridx);
					if (rowSelected_viKasirLab != undefined)
					{
						setLookUp_viKasirLab(rowSelected_viKasirLab.data);
					}
					else
					{
						setLookUp_viKasirLab();
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Kasir Laboratorium
	        *	Terdiri dari : Judul, Isi dan Event
	        *	Isi pada Grid di dapat dari pemangilan dari Net.
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					{
						id: 'colNoMedrec_viKasirLab',
						header: 'No. Medrec',
						dataIndex: 'KD_PASIEN',
						sortable: true,
						width: 35,
						filter:
						{
							type: 'int'
						}
					},
					//-------------- ## --------------
					{
						id: 'colNMPASIEN_viKasirLab',
						header: 'Nama',
						dataIndex: 'NAMA',
						sortable: true,
						width: 35,
						filter:
						{}
					},
					//-------------- ## --------------
					{
						id: 'colALAMAT_viKasirLab',
						header:'Alamat',
						dataIndex: 'ALAMAT',
						width: 60,
						sortable: true,
						filter: {}
					},
					//-------------- ## --------------
					{
						id: 'colTglKunj_viKasirLab',
						header:'Tgl Kunjungan',
						dataIndex: 'TGL_KUNJUNGAN',						
						width: 50,
						sortable: true,
						// format: 'd/M/Y',
						filter: {},
						renderer: function(v, params, record)
						{
							return ShowDate(record.data.TGL_KUNJUNGAN);
						}
					},
					//-------------- ## --------------
					{
						id: 'colPoliTujuan_viKasirLab',
						header:'Poli Tujuan',
						dataIndex: 'NAMA_UNIT',
						width: 50,
						sortable: true,
						filter: {}
					}
					//-------------- ## --------------
				]
			),
			// Tolbar ke Dua # --------------
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viKasirLab',
				items: 
				[
					{
						xtype: 'button',
						text: 'Edit Data',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						id: 'btnEdit_viKasirLab',
						handler: function(sm, row, rec)
						{
							if (rowSelected_viKasirLab != undefined)
							{
								setLookUp_viKasirLab(rowSelected_viKasirLab.data)
							}
							else
							{								
								setLookUp_viKasirLab();
							}
						}
					}
					//-------------- ## --------------
				]
			},
			// End Tolbar ke Dua # --------------
			// Button Bar Pagging # --------------
			// Fungsi bbar_paging didapat dari CommonRenderer.js # --------------
			bbar : bbar_paging(mod_name_viKasirLab, selectCount_viKasirLab, dataSource_viKasirLab),
			// End Button Bar Pagging # --------------
			viewConfig: 
			{
				forceFit: true
			}
		}
    )

	// Kriteria filter pada Grid # --------------
    var FrmFilterGridDataView_viKasirLab = new Ext.Panel
    (
		{
			title: NamaForm_viKasirLab,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viKasirLab,
			region: 'center',
			layout: 'form', 
			closable: true,        
			border: false,  
			margins: '0 5 5 0',
			items: [GridDataView_viKasirLab],
			tbar:
			[
				{
		            //-------------- # Untuk mengelompokkan pencarian # --------------
		            xtype: 'buttongroup',
		            //title: 'Pencarian ' + NamaForm_viKasirLab,
		            columns: 9,
		            defaults: {
		                scale: 'small'
		        	},
		        	frame: false,
		        	//-------------- ## --------------
		            items: 
		            [
			            { 
							xtype: 'tbtext', 
							text: 'Jenis Transaksi : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},
						viComboJenisTransaksi_MediSmart(125, "ComboJenisTransaksi_viKasirLab"),	
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'No. : ', 
							style:{'text-align':'right'},
							width: 45,
							height: 25
						},
						{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_NoMedrec_viKasirLab',
							emptyText: 'No. Medrec',
							width: 300,
							height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										DfltFilterBtn_viKasirLab = 1;
										DataRefresh_viKasirLab(getCriteriaFilterGridDataView_viKasirLab());
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Tgl Kunjungan : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},
						{
							xtype: 'datefield',
							id: 'txtfilterTglKunjAwal_viKasirLab',
							value: now_viKasirLab,
							format: 'd/M/Y',
							width: 120,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viKasirLab();								
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Kelompok : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},
						viComboJenisTransaksi_MediSmart(125, "ComboKelompok_viKasirLab"),	
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Nama : ', 
							style:{'text-align':'right'},
							width: 45,
							height: 25
						},
						{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_NAMA_viKasirLab',
							emptyText: 'Nama',
							width: 300,
							height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										DfltFilterBtn_viKasirLab = 1;
										DataRefresh_viKasirLab(getCriteriaFilterGridDataView_viKasirLab());								
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 's/d Tgl : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},
						{
							xtype: 'datefield',
							id: 'txtfilterTglKunjAkhir_viKasirLab',
							value: now_viKasirLab,
							format: 'd/M/Y',
							width: 120,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										datarefresh_viKasirLab();								
									} 						
								}
							}
						},
						//-------------- ## --------------
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Poli Tujuan : ', 
							style:{'text-align':'right'},
							width: 90,
							height: 25
						},
						viComboJenisTransaksi_MediSmart(125, "ComboPoli_viKasirLab"),
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbtext', 
							text: 'Alamat : ', 
							style:{'text-align':'right'},
							width: 45,
							height: 25
						},
						{
							xtype: 'textfield',
							id: 'TxtFilterGridDataView_ALAMAT_viKasirLab',
							emptyText: 'Alamat',
							width: 300,
							height: 25,
							listeners:
							{ 
								'specialkey' : function()
								{
									if (Ext.EventObject.getKey() === 13) 
									{
										DfltFilterBtn_viKasirLab = 1;
										DataRefresh_viKasirLab(getCriteriaFilterGridDataView_viKasirLab());								
									} 						
								}
							}
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{ 
							xtype: 'tbspacer',
							width: 15,
							height: 25
						},
						//-------------- ## --------------
						{
							xtype: 'button',
							text: 'Cari',
							iconCls: 'refresh',
							tooltip: 'Cari',
							style:{paddingLeft:'30px'},
							//rowspan: 3,
							width:150,
							id: 'BtnFilterGridDataView_viKasirLab',
							handler: function() 
							{					
								DfltFilterBtn_viKasirLab = 1;
								DataRefresh_viKasirLab(getCriteriaFilterGridDataView_viKasirLab());
							}                        
						},
						//-------------- ## --------------
					]
					//-------------- # End items # --------------
				//-------------- # End mengelompokkan pencarian # --------------
				}
			]
			//-------------- # End tbar # --------------
       }
    )
    return FrmFilterGridDataView_viKasirLab;
    //-------------- # End form filter # --------------
}
// End Function dataGrid_viKasirLab # --------------

/**
*	Function : setLookUp_viKasirLab
*	
*	Sebuah fungsi untuk menampilkan windows popup Form Edit saat ada event klik Edit Data
*/

function setLookUp_viKasirLab(rowdata)
{
    var lebar = 985;
    setLookUps_viKasirLab = new Ext.Window
    (
    {
        id: 'SetLookUps_viKasirLab',
		name: 'SetLookUps_viKasirLab',
        title: NamaForm_viKasirLab, 
        closeAction: 'destroy',        
        width: 1000,
        height: 615,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_viKasirLab(lebar,rowdata), //1
        listeners:
        {
            activate: function()
            {
				
            },
            afterShow: function()
            {
                this.activate();
            },
            deactivate: function()
            {
                rowSelected_viKasirLab=undefined;
                //datarefresh_viKasirLab();
				mNoKunjungan_viKasirLab = '';
            }
        }
    }
    );

    setLookUps_viKasirLab.show();
    if (rowdata == undefined)
    {
        // dataaddnew_viKasirLab();
		// Ext.getCmp('btnDelete_viKasirLab').disable();	
    }
    else
    {
        // datainit_viKasirLab(rowdata);
    }
}
// End Function setLookUpGridDataView_viKasirLab # --------------

/**
*	Function : getFormItemEntry_viKasirLab
*	
*	Sebuah fungsi untuk menampilkan penataan menu dan isian form edit
*/

function getFormItemEntry_viKasirLab(lebar,rowdata)
{
    var pnlFormDataBasic_viKasirLab = new Ext.FormPanel
    (
		{
			title: '',
			region: 'north',
			layout: 'form',
			bodyStyle: 'padding:10px 10px 10px 10px',
			anchor: '100%',
			width: lebar,
			border: false,
			//-------------- #items# --------------
			items:
			[
				getItemPanelInputBiodata_viKasirLab(lebar), 
				getItemDataKunjungan_viKasirLab(lebar), 
				getItemGridTransaksi_viKasirLab(lebar),
				{
					xtype: 'compositefield',
					fieldLabel: ' ',
					labelSeparator: '',
					name: 'compNoreg_viKasirLab',
					id: 'compNoreg_viKasirLab',
					items: 
					[
						{ 
							xtype: 'tbspacer',
							height: 30,
							width: 1,
						},
						//-------------- ## --------------
						{
							xtype: 'displayfield',				
							width: 70,								
							value: 'Jumlah:',
							fieldLabel: 'Label',
							style:{'text-align':'right','margin-top':'3px','margin-left':'540px'},
						},
						//-------------- ## --------------
						{
		                    xtype: 'textfield',
		                    id: 'txtDRGridTransaksiEditData_viKasirLab',
		                    name: 'txtDRGridTransaksiEditData_viKasirLab',
							style:{'text-align':'right','margin-top':'3px','margin-left':'555px'},
		                    width: 100,
		                    value: 0,
		                    readOnly: true,
		                },
		                //-------------- ## --------------
		                {
		                    xtype: 'textfield',
		                    id: 'txtCRGridTransaksiEditData_viKasirLab',
		                    name: 'txtCRGridTransaksiEditData_viKasirLab',
							style:{'text-align':'right','margin-top':'3px','margin-left':'551px'},
		                    width: 100,
		                    value: 0,
		                    readOnly: true,
		                },
		                //-------------- ## --------------
		            ],
		        },
                //-------------- ## --------------
			],
			//-------------- #End items# --------------
			fileUpload: true,
			// Tombol pada tollbar Edit Data Pasien
			tbar: 
			{
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add',
						iconCls: 'add',
						id: 'btnAdd_viKasirLab',
						handler: function(){
							dataaddnew_viKasirLab();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						id: 'btnSimpan_viKasirLab',
						handler: function()
						{
							datasave_viKasirLab(addNew_viKasirLab);
							datarefresh_viKasirLab();
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Save & Close',
						iconCls: 'saveexit',
						id: 'btnSimpanExit_viKasirLab',
						handler: function()
						{
							var x = datasave_viKasirLab(addNew_viKasirLab);
							datarefresh_viKasirLab();
							if (x===undefined)
							{
								setLookUps_viKasirLab.close();
							}
						}
					},
					//-------------- ## --------------
					{
						xtype: 'tbseparator'
					},
					//-------------- ## --------------
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						id: 'btnDelete_viKasirLab',
						handler: function()
						{
							datadelete_viKasirLab();
							datarefresh_viKasirLab();
							
						}
					},
					//-------------- ## --------------
					{
						xtype:'tbseparator'
					},
					//-------------- ## --------------
					{
		                text: 'Lookup ',
		                iconCls:'find',
		                menu: 
		                [
		                	{
						        text: 'Lookup Hasil Test',
						        id:'btnLookUpHasilTest_viKasirLab',
						        iconCls: 'find',
						        handler: function(){
						            FormSetHasilTest_viKasirLab('1','2');
						        },
					    	},
					    	//-------------- ## --------------
					    	{
						        text: 'Lookup Ganti Dokter',
						        id:'btnLookUpGantiDokter_viKasirLab',
						        iconCls: 'find',
						        handler: function(){
						            FormSetLookupGantiDokter_viKasirLab('1','2');
						        },
					    	},
					    	//-------------- ## --------------
					    	{
						        text: 'Lookup Transaksi Lama',
						        id:'btnLookUpTransaksiLama_viKasirLab',
						        iconCls: 'find',
						        handler: function(){
						            FormSetLookupTransaksiLama_viKasirLab('1','2');
						        },
					    	},
					    	//-------------- ## --------------
		                ],
		            },
					//-------------- ## --------------
					{
						xtype:'tbseparator'
					},
					//-------------- ## --------------
					{
		                text: 'Pembayaran ',
		                iconCls:'print',
		                menu: 
		                [
		                	{
						        text: 'Pembayaran',
						        id:'btnPembayaran_viKasirLab',
						        iconCls: 'print',
						        handler: function(){
						            FormSetPembayaran_viKasirLab('1','2');
						        },
					    	},
					    	//-------------- ## --------------
        					{
						        text: 'Transfer ke Rawat Inap',
						        id:'btnTransferPembayaran_viKasirLab',
						        iconCls: 'print',
						        handler: function(){
						            FormSetTransferPembayaran_viKasirLab('1','2');
						        },
					    	},
					    	//-------------- ## --------------
					    	{
						        text: 'Edit Tarif',
						        id:'btnEditTarif_viKasirLab',
						        iconCls: 'print',
						        handler: function(){
						            FormSetEditTarif_viKasirLab('1','2');
						        },
					    	},
					    	//-------------- ## --------------
		                ],
		            },
					//-------------- ## --------------
				]
			}//,items:
		}
    )

    return pnlFormDataBasic_viKasirLab;
}
// End Function getFormItemEntry_viKasirLab # --------------

/**
*	Function : getItemPanelInputBiodata_viKasirLab
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemPanelInputBiodata_viKasirLab(lebar) 
{
    var rdAsalPasien_viKasirLab = new Ext.form.RadioGroup
	({  
        fieldLabel: '',  
        columns: 3, 
        width: 450,
		border:false,
        items: 
		[  
			{
				boxLabel: '1. R. Jalan/UGD', 
				width:70,
				id: 'rdSatuAsalPasien_viKasirLab', 
				name: 'rdSatuAsalPasien_viKasirLab', 
				inputValue: '1'
			},  
			//-------------- ## --------------
			{
				boxLabel: '2. Rawat Inap', 
				width:70,
				id: 'rdDuaAsalPasien_viKasirLab', 
				name: 'rdDuaAsalPasien_viKasirLab', 
				inputValue: '2'
			},
			//-------------- ## --------------
			{
				boxLabel: '3. Kunjungan Langsung', 
				width:70,
				id: 'rdTigaAsalPasien_viKasirLab', 
				name: 'rdTigaAsalPasien_viKasirLab', 
				inputValue: '3'
			},
			//-------------- ## --------------
        ]  
    });
	//-------------- ## --------------

    var items =
	{
	    layout: 'Form',
	    anchor: '100%',
	    width: lebar- 80,
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',
		border:true,
		items:
		[			
			{
                xtype: 'compositefield',
                fieldLabel: 'Asal Pasien',
                anchor: '100%',
                width: 500,
                items: 
                [
                    rdAsalPasien_viKasirLab,
                    { 
						xtype: 'tbspacer',
						width: 200,
					},
					//-------------- ## --------------
                    {
						xtype: 'displayfield',
						flex: 1,
						width: 50,
						name: '',
						value: 'No. Reg:',
						fieldLabel: 'Label'
					},
					{
						xtype: 'textfield',
						flex: 1,
						width : 120,	
						name: 'txtNoReg_viKasirLab',
						id: 'txtNoReg_viKasirLab',
						emptyText: 'No. Reg',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------  
			{
				xtype: 'compositefield',
				fieldLabel: 'NoMedRec',
				anchor: '100%',
				//labelSeparator: '',
				width: 199,
				items: 
				[
					{
						xtype: 'textfield',
						flex: 1,
						fieldLabel: 'Label',
						width: 100,
						name: 'txtNoMedrec_viKasirLab',
						id: 'txtNoMedrec_viKasirLab',
						style:{'text-align':'right'},
						emptyText: 'No. Medrec',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},
					//-------------- ## --------------
					{ 
						xtype: 'tbspacer',
						width: 140,
					},
					//-------------- ## --------------
					{
						xtype: 'displayfield',
						flex: 1,
						width: 70,
						name: '',
						value: 'No. Trans:',
						fieldLabel: 'Label'
					},
					{
						xtype: 'textfield',
						flex: 1,
						fieldLabel: 'Label',														
						width : 330,	
						name: 'txtNoTrans_viKasirLab',
						id: 'txtNoTrans_viKasirLab',
						emptyText: 'No. Trans',
						listeners:
						{
							'specialkey': function() 
							{
								if (Ext.EventObject.getKey() === 13) 
								{
								};
							}
						}
					},
					//-------------- ## --------------
					{
						xtype: 'displayfield',
						flex: 1,
						width: 50,
						name: '',
						value: 'Tanggal:',
						fieldLabel: 'Label'
					},
					{
		                xtype: 'compositefield',
		                fieldLabel: 'Tanggal',
		                anchor: '100%',
		                width: 250,
		                items: 
		                [
		                    {
		                        xtype: 'textfield',
		                        id: 'TxtTglEditData_viKasirLab',
		                        emptyText: '01',
		                        width: 30,
		                    },
		                    //-------------- ## --------------  
		                    {
		                        xtype: 'textfield',
		                        id: 'TxtBlnEditData_viKasirLab',
		                        emptyText: '01',
		                        width: 30,
		                    },
		                    //-------------- ## --------------  
		                    {
		                        xtype: 'textfield',
		                        id: 'TxtThnEditData_viKasirLab',
		                        emptyText: '2014',
		                        width: 50,
		                    },
		                    //-------------- ## --------------  
		                ]
		            },
				]
			},	
			//-------------- ## --------------				
			{
				xtype: 'compositefield',
				fieldLabel: 'Nama Pasien',
				name: 'compNoreg_viKasirLab',
				id: 'compNoreg_viKasirLab',
				items: 
				[
					{
						xtype: 'textfield',					
						fieldLabel: '',								
						anchor: '100%',
						width: 245,
						name: 'txtNamaPasien_viKasirLab',
						id: 'txtNamaPasien_viKasirLab',
						emptyText: 'Nama Pasien',
					},
					//-------------- ## --------------
					{
						xtype: 'displayfield',				
						width: 70,								
						value: 'Dokter:',
						fieldLabel: 'Label',
					},
					{
						xtype: 'textfield',					
						fieldLabel: '',								
						width: 50,
						name: 'txtDokter_viKasirLab',
						id: 'txtDokter_viKasirLab',
						emptyText: 'Dokter',
					},
					Vicbo_Dokter(455, "ComboNamaDokter_viKasirLab"),
					//-------------- ## --------------
				]
			},
			//-------------- ## --------------	
			{
		        xtype: 'panel',
		        title: '',
		        border:false,
		        items: 
		        [           
		            {
						xtype: 'compositefield',
						items: 
						[
				            {
								xtype: 'displayfield',				
								width: 70,								
								value: 'Tgl. Lahir',
							},
				            //-------------- ## --------------
				            {
								xtype: 'displayfield',				
								width: 50,								
								value: 'Thn',
							},
				            //-------------- ## --------------
				            {
								xtype: 'displayfield',				
								width: 30,								
								value: 'Bln',
							},
				            //-------------- ## --------------
				            {
								xtype: 'displayfield',				
								width: 30,								
								value: 'Hari',
							},
				            //-------------- ## --------------
				            {
								xtype: 'displayfield',				
								width: 80,								
								value: 'Jenis Kelamin',
							},
				            //-------------- ## --------------
				            {
								xtype: 'displayfield',				
								width: 65,								
								value: 'G. Darah',
							},
				            //-------------- ## --------------
							{
								xtype: 'displayfield',				
								width: 70,								
								value: 'Kelas:',
							},
				            //-------------- ## --------------
				            {
								xtype: 'textfield',
								flex: 1,
								width : 510,	
								name: 'txtKelas_viKasirLab',
								id: 'txtKelas_viKasirLab',
								emptyText: 'Kelas',
								listeners:
								{
									'specialkey': function() 
									{
										if (Ext.EventObject.getKey() === 13) 
										{
										};
									}
								}
							},
							//-------------- ## --------------
							{ 
								xtype: 'tbspacer',
								height: 25,
							},
							//-------------- ## --------------
				        ]
				    },
		        ]
		    },
			//-------------- ## --------------
			{
		        xtype: 'panel',
		        title: '',
		        border:false,
		        items: 
		        [           
		            {
						xtype: 'compositefield',
						items: 
						[
				            {
		                        xtype: 'textfield',
		                        id: 'TxtTglLhirEditData_viKasirLab',
		                        emptyText: '01',
		                        width: 30,
		                    },
		                    //-------------- ## -------------- 
				            {
		                        xtype: 'textfield',
		                        id: 'TxtTglLhir2EditData_viKasirLab',
		                        emptyText: '01',
		                        width: 30,
		                    },
				            //-------------- ## --------------
				            {
		                        xtype: 'textfield',
		                        id: 'TxtThnLhirEditData_viKasirLab',
		                        emptyText: '01',
		                        width: 50,
		                    },
				            //-------------- ## --------------
				            {
		                        xtype: 'textfield',
		                        id: 'TxtTglLhir3EditData_viKasirLab',
		                        emptyText: '01',
		                        width: 30,
		                    },
				            //-------------- ## --------------
				            {
		                        xtype: 'textfield',
		                        id: 'TxtTglLhir4EditData_viKasirLab',
		                        emptyText: '01',
		                        width: 30,
		                    },
				            //-------------- ## --------------
				            viComboJenisTransaksi_MediSmart(80,'CmboJenisKelaminEditData_viKasirLab'),
				            //-------------- ## --------------
				            viComboJenisTransaksi_MediSmart(60,'CmboGlgnDrhEditData_viKasirLab'),
				            //-------------- ## --------------
				            { 
								xtype: 'tbspacer',
								width: 5,
							},
							//-------------- ## --------------
							{
								xtype: 'displayfield',				
								width: 70,								
								value: 'Kelmpk P.:',
							},
				            //-------------- ## --------------
				            viComboJenisTransaksi_MediSmart(510,'CmboKelompokEditData_viKasirLab'),
							//-------------- ## --------------
							{ 
								xtype: 'tbspacer',
								height: 25,
							},
							//-------------- ## --------------
				        ]
				    },
		        ]
		    },
			//-------------- ## --------------
			{
				xtype: 'compositefield',
				fieldLabel: 'Alamat',
				items: 
				[
					{
						xtype: 'textfield',					
						fieldLabel: '',								
						anchor: '100%',
						width: 245,
						name: 'txtAlamat_viKasirLab',
						id: 'txtAlamat_viKasirLab',
						emptyText: 'Alamat',
					},
					//-------------- ## --------------
					{
						xtype: 'displayfield',				
						width: 70,								
						value: '',
						fieldLabel: 'Label',
					},
					Vicbo_Dokter(510, "CmboKelompok2EditData_viKasirLab"),
					//-------------- ## --------------
				]
			},
			//-------------- ## --------------	
		]
	};
    return items;
};
// End Function getItemPanelInputBiodata_viKasirLab # --------------

/**
*	Function : getItemDataKunjungan_viKasirLab
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemDataKunjungan_viKasirLab(lebar) 
{
	var rdJenisKunjungan_viKasirLab = new Ext.form.RadioGroup
	({  
        fieldLabel: 'Cara Penerimaan',  
        columns: 2, 
        width: 400,
		border:false,
        items: 
		[  
			{
				boxLabel: 'Datang Senidri', 
				width:70,
				id: 'rdDatangSendiri_viKasirLab', 
				name: 'rdDatangSendiri_viKasirLab', 
				inputValue: '1'
			},  
			//-------------- ## --------------
			{
				boxLabel: 'Rujukan', 
				width:70,
				id: 'rdDatangRujukan_viKasirLab', 
				name: 'rdDatangRujukan_viKasirLab', 
				inputValue: '2'
			} 
			//-------------- ## --------------
        ]  
    });
	//-------------- ## --------------

    var items =
	{
		layout: 'form',
		anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:10px 10px 10px 10px',		
		width: lebar-80,
		height:110,
	    items:
		[	
			rdJenisKunjungan_viKasirLab,
			{
				xtype: 'compositefield',
				fieldLabel: 'Dari',	
				labelAlign: 'Left',
				//labelSeparator: '',
				labelWidth:70,
				defaults: 
				{
					flex: 1
				},
				items: 
				[
					Vicbo_Dokter(270, "ComboDari_viKasirLab"),
					//-------------- ## --------------
					{
					   xtype: 'displayfield',
					   value: 'Nama:',
					   //style:{'margin-top':'2px', 'text-align':'right'},
					   width:40
					},
					Vicbo_Dokter(515, "ComboDariNama_viKasirLab"),
					//-------------- ## --------------
				]
			},
			//-------------- ## --------------
			{
				xtype: 'compositefield',
				fieldLabel: 'Alamat',		
				labelAlign: 'Left',
				labelWidth:105,
				defaults: 
				{
					flex: 1
				},
				items: 
				[
					{
						xtype: 'textfield',					
						fieldLabel: '',								
						anchor: '100%',
						width: 515,
						name: 'txtAlamatEditData_viKasirLab',
						id: 'txtAlamatEditData_viKasirLab',
						emptyText: 'Alamat',
					},
					//-------------- ## --------------
					{
					   xtype: 'displayfield',
					   value: 'Kota:',
					   width:50
					},
					Vicbo_Dokter(260, "ComboKotaEditData_viKasirLab"),
					//-------------- ## --------------
				]
			}
			//-------------- ## --------------
		]
	};
    return items;
};
// End Function getItemDataKunjungan_viKasirLab # --------------

/**
*	Function : getItemGridTransaksi_viKasirLab
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function getItemGridTransaksi_viKasirLab(lebar) 
{
    var items =
	{
		//title: 'Detail Transaksi', 
	    layout: 'form',
	    anchor: '100%',	    
	    labelAlign: 'Left',
	    bodyStyle: 'padding:1px 1px 1px 1px',
		border:true,
		width: lebar-80,
		height:225, 
	    items:
		[
			{
				labelWidth:105,
			    layout: 'form',
				labelAlign: 'Left',
			    border: false,
			    items:
				[
					gridDataViewEdit_viKasirLab()
				]	
			}
			//-------------- ## --------------
		]
	};
    return items;
};
// End Function getItemGridTransaksi_viKasirLab # --------------

/**
*	Function : gridDataViewEdit_viKasirLab
*	
*	Sebuah fungsi untuk menampilkan isian form edit data
*/

function gridDataViewEdit_viKasirLab()
{
    
    chkSelected_viKasirLab = new Ext.grid.CheckColumn
	(
		{
			id: 'chkSelected_viKasirLab',
			header: '',
			align: 'center',						
			dataIndex: 'SELECTED',			
			width: 20
		}
	);

    var FieldGrdKasir_viKasirLab = 
	[
	];
	
    dsDataGrdJab_viKasirLab= new WebApp.DataStore
	({
        fields: FieldGrdKasir_viKasirLab
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJab_viKasirLab,
        height: 220,
		selModel: new Ext.grid.RowSelectionModel
        (
	        {
	            singleSelect: true,
	            listeners:
	            {
	                rowselect: function(sm, row, rec)
	                {
	                }
	            }
	        }
        ),
        columns: 
		[
			chkSelected_viKasirLab,
			{
				dataIndex: '',
				header: 'Code',
				sortable: true,
				width: 100
			},
			//-------------- ## --------------
			{			
				dataIndex: '',
				header: 'Uraian',
				sortable: true,
				width: 250
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Dok',
				sortable: true,
				width: 85,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Cd. Trf',
				sortable: true,
				width: 80,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Harga/Unit',
				sortable: true,
				width: 100,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'Qty',
				sortable: true,
				width: 100,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'DR',
				sortable: true,
				width: 100,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
			{
				dataIndex: '',
				header: 'CR',
				sortable: true,
				width: 100,
				renderer: function(v, params, record) 
				{
					
				}	
			},
			//-------------- ## --------------
        ],
        plugins:chkSelected_viKasirLab,
    }    
    return items;
}
// End Function gridDataViewEdit_viKasirLab # --------------

// ## MENU LOOKUP ## ------------------------------------------------------------------

// ## MENU LOOKUP - LOOKUP HASIL TEST ## ------------------------------------------------------------------

/**
*   Function : FormSetHasilTest_viKasirLab
*   
*   Sebuah fungsi untuk menampilkan windows popup Hasil Test
*/

function FormSetHasilTest_viKasirLab(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryHasilTest_viKasirLab = new Ext.Window
    (
        {
            id: 'FormGrdHasilTest_viKasirLab',
            title: 'Hasil Test',
            closeAction: 'destroy',
            closable:true,
            width: 760,
            height: 511,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
			//padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryHasilTestGridDataView_viKasirLab(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryHasilTest_viKasirLab.show();
    mWindowGridLookupHasilTest  = vWinFormEntryHasilTest_viKasirLab; 
};

// End Function FormSetHasilTest_viKasirLab # --------------

/**
*   Function : getFormItemEntryHasilTestGridDataView_viKasirLab
*   
*   Sebuah fungsi untuk menampilkan isian form hasil test
*/
function getFormItemEntryHasilTestGridDataView_viKasirLab(subtotal,NO_KUNJUNGAN)
{
    var lebar = 760;
    var pnlFormDataHasilTestWindowPopup_viKasirLab = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
			items:
			[
				getItemPanelInputBiodataHasilTestGridDataView_viKasirLab(lebar),
				//-------------- ## -------------- 
				getItemGridTransaksiHasilTestGridDataView_viKasirLab(lebar),
				//-------------- ## --------------
				{ 
					xtype: 'tbspacer',
					height: 10,
				},
				//-------------- ## --------------
				{
					xtype: 'displayfield',
					flex: 1,
					width: 150,
					name: '',
					value: 'Keterangan Klinik',
					fieldLabel: 'Label'
				},
				{
					xtype: 'textarea',
					id: 'TxtAreaTransaksiHasilTestGridDataView_viKasirLab',
					name: 'TxtAreaTransaksiHasilTestGridDataView_viKasirLab',
					//readOnly: true,
					flex: 1,
					width: 730,
					height:80,
				},
				//-------------- ## --------------
				{ 
					xtype: 'tbspacer',
					height: 10,
				},
				//-------------- ## --------------
				{
					xtype: 'compositefield',
					fieldLabel: ' ',
					anchor: '100%',
					//labelSeparator: '',
					width: 199,
					style:{'margin-top':'7px'},
					items: 
					[
						{
		                    xtype:'button',
		                    text:'Transaksi',
		                    width:70,
		                    //style:{'margin-left':'190px','margin-top':'7px'},
		                    hideLabel:true,
		                    id: 'btnTransaksiGridTransaksiHasilTestGridDataView_viKasirLab',
		                    handler:function()
		                    {
		                    }   
		                },
		                //-------------- ## --------------
		                {
		                    xtype:'button',
		                    text:'Hasil Test',
		                    width:70,
		                    //style:{'margin-left':'190px','margin-top':'7px'},
		                    hideLabel:true,
		                    id: 'btnHasilTestGridTransaksiHasilTestGridDataView_viKasirLab',
		                    handler:function()
		                    {
		                    }   
		                },
		                //-------------- ## --------------
					]
				},	
			],
			//-------------- #End items# --------------
        }
    )
    return pnlFormDataHasilTestWindowPopup_viKasirLab;
}
// End Function getFormItemEntryHasilTestGridDataView_viKasirLab # --------------

/**
*   Function : getItemPanelInputBiodataHasilTestGridDataView_viKasirLab
*   
*   Sebuah fungsi untuk menampilkan isian form hasil test
*/

function getItemPanelInputBiodataHasilTestGridDataView_viKasirLab(lebar) 
{
    var items =
    {
        //title: 'Infromasi Pasien',
        layout: 'Form',
        anchor: '100%',
        width: lebar- 80,
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        items:
        [           
            {
		        xtype: 'panel',
		        title: '',
		        border:false,
		        items: 
		        [           
		            {
						xtype: 'compositefield',
						items: 
						[
				            {
								xtype: 'displayfield',				
								width: 72,								
								value: 'No. MedRec:',
							},
				            //-------------- ## --------------
				            {
								xtype: 'textfield',
								flex: 1,
								width : 80,	
								name: 'txtNoMedRecHasilTest_viKasirLab',
								id: 'txtNoMedRecHasilTest_viKasirLab',
								emptyText: 'No. MedRec',
								listeners:
								{
									'specialkey': function() 
									{
										if (Ext.EventObject.getKey() === 13) 
										{
										};
									}
								}
							},
							//-------------- ## --------------
							{
								xtype: 'displayfield',				
								width: 50,								
								value: 'Tanggal:',
							},
				            //-------------- ## --------------
				            {
								xtype: 'textfield',
								flex: 1,
								width : 30,	
								name: 'txtTglHasilTest_viKasirLab',
								id: 'txtTglHasilTest_viKasirLab',
								emptyText: '01',
							},
							{
								xtype: 'textfield',
								flex: 1,
								width : 30,	
								name: 'txtBlnHasilTest_viKasirLab',
								id: 'txtBlnHasilTest_viKasirLab',
								emptyText: '01',
							},
							{
								xtype: 'textfield',
								flex: 1,
								width : 50,	
								name: 'txtThnHasilTest_viKasirLab',
								id: 'txtThnHasilTest_viKasirLab',
								emptyText: '2014',
							},
							//-------------- ## --------------
							{ 
								xtype: 'tbspacer',
								width: 23,
								height: 25,
							},
							//-------------- ## --------------
				            {
								xtype: 'displayfield',				
								width: 70,								
								value: 'Tgl. Lahir',
							},
				            //-------------- ## --------------
				            {
								xtype: 'displayfield',				
								width: 50,								
								value: 'Thn',
							},
				            //-------------- ## --------------
				            {
								xtype: 'displayfield',				
								width: 30,								
								value: 'Bln',
							},
				            //-------------- ## --------------
				            {
								xtype: 'displayfield',				
								width: 30,								
								value: 'Hari',
							},
				            //-------------- ## --------------
				            {
								xtype: 'displayfield',				
								width: 80,								
								value: 'Jenis Kelamin',
							},
				            //-------------- ## --------------
				            {
								xtype: 'displayfield',				
								width: 65,								
								value: 'G. Darah',
							},
				            //-------------- ## --------------
				        ]
				    },
		        ]
		    },
			//-------------- ## --------------
			{
		        xtype: 'panel',
		        title: '',
		        border:false,
		        items: 
		        [           
		            {
						xtype: 'compositefield',
						items: 
						[
				            {
								xtype: 'displayfield',				
								width: 72,								
								value: 'Nama Pasien:',
							},
				            //-------------- ## --------------
				            {
		                        xtype: 'textfield',
		                        id: 'TxtPasienHasilTest_viKasirLab',
		                        emptyText: 'Nama Pasien',
		                        width: 285,
		                    },
		                    //-------------- ## -------------- 
				            {
		                        xtype: 'textfield',
		                        id: 'TxtTglLhirHasilTest_viKasirLab',
		                        emptyText: '01',
		                        width: 30,
		                    },
		                    //-------------- ## -------------- 
				            {
		                        xtype: 'textfield',
		                        id: 'TxtTglLhir2HasilTest_viKasirLab',
		                        emptyText: '01',
		                        width: 30,
		                    },
				            //-------------- ## --------------
				            {
		                        xtype: 'textfield',
		                        id: 'TxtThnLhirHasilTest_viKasirLab',
		                        emptyText: '01',
		                        width: 50,
		                    },
				            //-------------- ## --------------
				            {
		                        xtype: 'textfield',
		                        id: 'TxtTglLhir3HasilTest_viKasirLab',
		                        emptyText: '01',
		                        width: 30,
		                    },
				            //-------------- ## --------------
				            {
		                        xtype: 'textfield',
		                        id: 'TxtTglLhir4HasilTest_viKasirLab',
		                        emptyText: '01',
		                        width: 30,
		                    },
				            //-------------- ## --------------
				            viComboJenisTransaksi_MediSmart(80,'CmboJenisKelaminHasilTest_viKasirLab'),
				            //-------------- ## --------------
				            viComboJenisTransaksi_MediSmart(60,'CmboGlgnDrhHasilTest_viKasirLab'),
				            //-------------- ## --------------
				            { 
								xtype: 'tbspacer',
								height: 25,
							},
							//-------------- ## --------------
				        ]
				    },
		        ]
		    },
			//-------------- ## --------------
			{
		        xtype: 'panel',
		        title: '',
		        border:false,
		        items: 
		        [
					{
						xtype: 'compositefield',
						fieldLabel: '',
						items: 
						[
							{
								xtype: 'displayfield',				
								width: 72,								
								value: 'Dokter:',
							},
							{
								xtype: 'textfield',					
								fieldLabel: '',								
								anchor: '100%',
								width: 260,
								name: 'txtDokterHasilTest_viKasirLab',
								id: 'txtDokterHasilTest_viKasirLab',
								emptyText: 'Dokter',
							},
							//-------------- ## --------------
							{
								xtype: 'displayfield',				
								width: 52,								
								value: 'Alamat:',
							},
							{
								xtype: 'textfield',					
								fieldLabel: '',								
								anchor: '100%',
								width: 308,
								name: 'txtAlamatHasilTest_viKasirLab',
								id: 'txtAlamatHasilTest_viKasirLab',
								emptyText: 'Alamat',
							},
							//-------------- ## --------------
						]
					},
					//-------------- ## --------------
				]
			},
			//-------------- ## --------------
        ]
    };
    return items;
}
// End Function getItemPanelInputBiodataHasilTestGridDataView_viKasirLab # --------------

/**
*   Function : getItemGridTransaksiHasilTestGridDataView_viKasirLab
*   
*   Sebuah fungsi untuk menampilkan isian form hasil test
*/

function getItemGridTransaksiHasilTestGridDataView_viKasirLab(lebar) 
{
    var items =
    {
        //title: 'Detail Transaksi', 
        layout: 'form',
        anchor: '100%',     
        labelAlign: 'Left',
        bodyStyle: 'padding:1px 1px 1px 1px',
        border:true,
        width: lebar-80,
        height:225, 
        items:
        [
            {
                labelWidth:105,
                layout: 'form',
                labelAlign: 'Left',
                border: false,
                items:
                [
                    gridDataViewEditHasilTestGridDataView_viKasirLab()
                ]   
            }
            //-------------- ## --------------
        ]
    };
    return items;
};
// End Function getItemGridTransaksiHasilTestGridDataView_viKasirLab # --------------

/**
*   Function : gridDataViewEditHasilTestGridDataView_viKasirLab
*   
*   Sebuah fungsi untuk menampilkan isian form hasil test
*/

function gridDataViewEditHasilTestGridDataView_viKasirLab()
{
    
    var FieldGrdKasirHasilTestGridDataView_viKasirLab = 
    [
    ];
    
    dsDataGrdJabHasilTestGridDataView_viKasirLab= new WebApp.DataStore
    ({
        fields: FieldGrdKasirHasilTestGridDataView_viKasirLab
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJabHasilTestGridDataView_viKasirLab,
        height: 220,
        selModel: new Ext.grid.RowSelectionModel
        (
            {
                singleSelect: true,
                listeners:
                {
                    rowselect: function(sm, row, rec)
                    {
                    }
                }
            }
        ),
        columns: 
        [
            {
                dataIndex: '',
                header: 'Kd. Test',
                sortable: true,
                width: 50
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Pemeriksaan',
                sortable: true,
                width: 200
            },
            //-------------- ## --------------          
            {           
                dataIndex: '',
                header: 'Metode',
                sortable: true,
                width: 60
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Hasil',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }           
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Normal',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }   
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Satuan',
                sortable: true,
                width: 90,
                renderer: function(v, params, record) 
                {
                    
                }   
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Keterangan',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }   
            },
            //-------------- ## --------------
        ]
    }    
    return items;
}
// End Function gridDataViewEditHasilTestGridDataView_viKasirLab # --------------

// ## END MENU LOOKUP - LOOKUP HASIL TEST ## ------------------------------------------------------------------

// ## MENU LOOKUP - LOOKUP GANTI DOKTER ## ------------------------------------------------------------------

/**
*   Function : FormSetLookupGantiDokter_viKasirLab
*   
*   Sebuah fungsi untuk menampilkan windows popup Ganti Dokter
*/

function FormSetLookupGantiDokter_viKasirLab(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryGantiDokter_viKasirLab = new Ext.Window
    (
        {
            id: 'FormGrdLookupGantiDokter_viKasirLab',
            title: 'Ganti Dokter',
            closeAction: 'destroy',
            closable:true,
            width: 390,
            height: 125,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
            //padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryLookupGantiDokter_viKasirLab(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryGantiDokter_viKasirLab.show();
    mWindowGridLookupGantiDokter  = vWinFormEntryGantiDokter_viKasirLab; 
};

// End Function FormSetLookupGantiDokter_viKasirLab # --------------

/**
*   Function : getFormItemEntryLookupGantiDokter_viKasirLab
*   
*   Sebuah fungsi untuk menampilkan isian form Ganti Dokter
*/
function getFormItemEntryLookupGantiDokter_viKasirLab(subtotal,NO_KUNJUNGAN)
{
    var lebar = 500;
    var pnlFormDataGantiDokterWindowPopup_viKasirLab = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                getItemPanelInputGantiDokterDataView_viKasirLab(lebar),
                //-------------- ## --------------
            ],
            //-------------- #End items# --------------
        }
    )
    return pnlFormDataGantiDokterWindowPopup_viKasirLab;
}
// End Function getFormItemEntryLookupGantiDokter_viKasirLab # --------------

/**
*   Function : getItemPanelInputGantiDokterDataView_viKasirLab
*   
*   Sebuah fungsi untuk menampilkan isian form Ganti Dokter
*/

function getItemPanelInputGantiDokterDataView_viKasirLab(lebar) 
{
    var items =
    {
        title: '',
        layout: 'Form',
        anchor: '100%',
        width: lebar,
        labelAlign: 'Left',
        bodyStyle: 'padding:1px 1px 1px 1px',
        border:false,
        items:
        [           
            {
                xtype: 'compositefield',
                fieldLabel: 'Tanggal',
                anchor: '100%',
                width: 250,
                items: 
                [
                    {
                        xtype: 'textfield',
                        id: 'TxtTglGantiDokter_viKasirLab',
                        emptyText: '01',
                        width: 30,
                    },
                    //-------------- ## --------------  
                    {
                        xtype: 'textfield',
                        id: 'TxtBlnGantiDokter_viKasirLab',
                        emptyText: '01',
                        width: 30,
                    },
                    //-------------- ## --------------  
                    {
                        xtype: 'textfield',
                        id: 'TxtThnGantiDokter_viKasirLab',
                        emptyText: '2014',
                        width: 50,
                    },
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
            {
                xtype: 'compositefield',
                fieldLabel: 'Dokter Asal',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_MediSmart(250,'CmboDokterAsal_viKasirLab'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
            {
                xtype: 'compositefield',
                fieldLabel: 'Dokter Ganti',
                anchor: '100%',
                width: 250,
                items: 
                [
                    viComboJenisTransaksi_MediSmart(250,'CmboDokterGanti_viKasirLab'),
                    //-------------- ## --------------  
                ]
            },
            //-------------- ## --------------
        ]
    };
    return items;
}
// End Function getItemPanelInputGantiDokterDataView_viKasirLab # --------------

// ## END MENU LOOKUP - LOOKUP GANTI DOKTER ## ------------------------------------------------------------------

// ## MENU LOOKUP - LOOKUP TRANSAKSI LAMA ## ------------------------------------------------------------------

/**
*   Function : FormSetLookupTransaksiLama_viKasirLab
*   
*   Sebuah fungsi untuk menampilkan windows popup transaksi lama
*/

function FormSetLookupTransaksiLama_viKasirLab(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryLookupTransaksiLama_viKasirLab = new Ext.Window
    (
        {
            id: 'FormGrdLookupTransaksiLama_viKasirLab',
            title: 'Buka Transaksi Lama',
            closeAction: 'destroy',
            closable:true,
            width: 500,
            height: 175,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
			//padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryTransaksiLama_viKasirLab(subtotal,NO_KUNJUNGAN),
                //-------------- ## --------------
            ],
        }
    );
    vWinFormEntryLookupTransaksiLama_viKasirLab.show();
    mWindowGridLookupLookupTransaksiLama  = vWinFormEntryLookupTransaksiLama_viKasirLab; 
}
// End Function FormSetLookupTransaksiLama_viKasirLab # --------------

/**
*   Function : getFormItemEntryTransaksiLama_viKasirLab
*   
*   Sebuah fungsi untuk menampilkan isian form produk
*/
function getFormItemEntryTransaksiLama_viKasirLab(subtotal,NO_KUNJUNGAN)
{
	var lebar = 515;
	var itemsEntryTransaksiLama_viKasirLab = 
    {
        xtype: 'panel',
        title: '',
        name: 'PanelListProdukGridDataView_viKasirLab',
        id: 'PanelListProdukGridDataView_viKasirLab',        
        bodyStyle: 'padding: 15px;',
        //height: 1170,
        border:false,
        items: 
        [           
            {
	            xtype: 'radiogroup',
	            //fieldLabel: 'Single Column',
	            itemCls: 'x-check-group-alt',
	            columns: 1,
	            items: 
	            [
	                {
	                	boxLabel: 'Transaksi Berjalan', 
	                	name: 'ckboxTreeTransaksiBerjalan_viKasirLab',
	                	id: 'ckboxTreeTransaksiBerjalan_viKasirLab',  
	                	inputValue: 1,
	                },
	                //-------------- ## -------------- 
	                {
	                	boxLabel: 'Transaksi Lama', 
	                	name: 'ckboxTreeTransaksiLama_viKasirLab',
	                	id: 'ckboxTreeTransaksiLama_viKasirLab',  
	                	inputValue: 2, 
	                	checked: true,
	                },
	                //-------------- ## -------------- 
	            ]
	        },
			//-------------- ## --------------
			{
                xtype: 'fieldset',
                title: '',
                frame: false,
                items: 
                [
	                {
				        xtype: 'panel',
				        title: '',
				        border:false,
				        items: 
				        [
			                {
								xtype: 'compositefield',
								fieldLabel: ' ',
								labelSeparator: '',
								anchor: '100%',
								width: 500,
								items: 
								[
				                    {
										xtype: 'displayfield',
										flex: 1,
										width: 80,
										name: '',
										value: 'Dari Tanggal:',
									},
				                    {
										xtype: 'datefield',
										fieldLabel: 'Tanggal',
										id: 'DateDariTransaksiLama_viKasirLab',
										name: 'DateDariTransaksiLama_viKasirLab',
										width: 130,
										format: 'd/M/Y',
									},
				                    //-------------- ## --------------
				                    {
										xtype: 'displayfield',
										flex: 1,
										width: 80,
										name: '',
										value: 's/d Tanggal:',
									},
				                    {
										xtype: 'datefield',
										fieldLabel: 'Tanggal',
										id: 'DateSmpDgnTransaksiLama_viKasirLab',
										name: 'DateSmpDgnTransaksiLama_viKasirLab',
										width: 130,
										format: 'd/M/Y',
									},
				                    //-------------- ## --------------
				                ],
				            },
				            //-------------- ## --------------
				        ],
				    },
				    //-------------- ## --------------
                ],
            },
            //-------------- ## --------------
            {
		        xtype: 'panel',
		        title: '',
		        border:false,
		        items: 
		        [
		            {
						xtype: 'compositefield',
						fieldLabel: ' ',
						labelSeparator: '',
						anchor: '100%',
						width: 500,
						items: 
						[
				            {
				                xtype:'button',
				                text:'Ok',
				                width:70,
				                style:{'margin-left':'300px'},
				                hideLabel:true,
				                id: 'btnOkTransaksiLama_viKasirLab',
				                handler:function()
				                {
				                }   
				            },
				            //-------------- ## --------------
				            {
				                xtype:'button',
				                text:'Cancel',
				                width:70,
				                style:{'margin-left':'304px'},
				                hideLabel:true,
				                id: 'btnCancelTransaksiLama_viKasirLab',
				                handler:function()
				                {
				                }   
				            },
				            //-------------- ## --------------
				        ],
				    },
				    //-------------- ## --------------
				]
			},
			//-------------- ## --------------
        ]
    }
    return itemsEntryTransaksiLama_viKasirLab;
}
// End Function getFormItemEntryTransaksiLama_viKasirLab # --------------

// ## END MENU LOOKUP - LOOKUP TRANSAKSI LAMA ## ------------------------------------------------------------------

// ## END MENU LOOKUP ## ------------------------------------------------------------------

// ## MENU PEMBAYARAN ## ------------------------------------------------------------------

// ## MENU PEMBAYARAN - PEMBAYARAN ## ------------------------------------------------------------------

/**
*   Function : FormSetPembayaran_viKasirLab
*   
*   Sebuah fungsi untuk menampilkan windows popup Pembayaran
*/

function FormSetPembayaran_viKasirLab(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryPembayaran_viKasirLab = new Ext.Window
    (
        {
            id: 'FormGrdPembayaran_viKasirLab',
            title: 'Pembayaran',
            closeAction: 'destroy',
            closable:true,
            width: 760,
            height: 473,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
			//padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryGridDataView_viKasirLab(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryPembayaran_viKasirLab.show();
    mWindowGridLookupPembayaran  = vWinFormEntryPembayaran_viKasirLab; 
};

// End Function FormSetPembayaran_viKasirLab # --------------

/**
*   Function : getFormItemEntryGridDataView_viKasirLab
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/
function getFormItemEntryGridDataView_viKasirLab(subtotal,NO_KUNJUNGAN)
{
    var lebar = 760;
    var pnlFormDataWindowPopup_viKasirLab = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
			items:
			[
				getItemPanelInputBiodataGridDataView_viKasirLab(lebar),
				//-------------- ## -------------- 
				getItemGridTransaksiGridDataView_viKasirLab(lebar),
				//-------------- ## --------------
				{
					xtype: 'compositefield',
					fieldLabel: ' ',
					anchor: '100%',
					//labelSeparator: '',
					width: 199,
					style:{'margin-top':'7px'},
					items: 
					[
						{
		                    xtype:'button',
		                    text:'Ok',
		                    width:70,
		                    //style:{'margin-left':'190px','margin-top':'7px'},
		                    hideLabel:true,
		                    id: 'btnOkGridTransaksiPembayaranGridDataView_viKasirLab',
		                    handler:function()
		                    {
		                    }   
		                },
		                //-------------- ## --------------
		                {
		                    xtype:'button',
		                    text:'Cancel',
		                    width:70,
		                    //style:{'margin-left':'190px','margin-top':'7px'},
		                    hideLabel:true,
		                    id: 'btnCancelGridTransaksiPembayaranGridDataView_viKasirLab',
		                    handler:function()
		                    {
		                    }   
		                },
		                //-------------- ## --------------
		                {
                            xtype: 'textfield',
                            id: 'txtBiayaGridTransaksiPembayaranGridDataView_viKasirLab',
                            name: 'txtBiayaGridTransaksiPembayaranGridDataView_viKasirLab',
							style:{'text-align':'right','margin-left':'150px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
		                //-------------- ## --------------
		                {
                            xtype: 'textfield',
                            id: 'txtPiutangGridTransaksiPembayaranGridDataView_viKasirLab',
                            name: 'txtPiutangGridTransaksiPembayaranGridDataView_viKasirLab',
							style:{'text-align':'right','margin-left':'146px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
		                //-------------- ## --------------
		                {
                            xtype: 'textfield',
                            id: 'txtTunaiGridTransaksiPembayaranGridDataView_viKasirLab',
                            name: 'txtTunaiGridTransaksiPembayaranGridDataView_viKasirLab',
							style:{'text-align':'right','margin-left':'142px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
		                //-------------- ## --------------
		                {
                            xtype: 'textfield',
                            id: 'txtDiscGridTransaksiPembayaranGridDataView_viKasirLab',
                            name: 'txtDiscGridTransaksiPembayaranGridDataView_viKasirLab',
							style:{'text-align':'right','margin-left':'138px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
		                //-------------- ## --------------
					]
				},	
			],
			//-------------- #End items# --------------
        }
    )
    return pnlFormDataWindowPopup_viKasirLab;
}
// End Function getFormItemEntryGridDataView_viKasirLab # --------------

/**
*   Function : getItemPanelInputBiodataGridDataView_viKasirLab
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/

function getItemPanelInputBiodataGridDataView_viKasirLab(lebar) 
{
    var items =
    {
        title: 'Infromasi Pasien',
        layout: 'Form',
        anchor: '100%',
        width: lebar- 80,
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        items:
        [           
            {
				xtype: 'textfield',
				fieldLabel: 'No. Transaksi',
				id: 'txtNoTransaksiPembayaranGridDataView_viKasirLab',
				name: 'txtNoTransaksiPembayaranGridDataView_viKasirLab',
				emptyText: 'No. Transaksi',
				readOnly: true,
				flex: 1,
				width: 195,				
			},
			//-------------- ## --------------
			{
				xtype: 'textfield',
				fieldLabel: 'Nama Pasien',
				id: 'txtNamaPasienPembayaranGridDataView_viKasirLab',
				name: 'txtNamaPasienPembayaranGridDataView_viKasirLab',
				emptyText: 'Nama Pasien',
				flex: 1,
				anchor: '100%',					
			},
			//-------------- ## --------------
			{
				xtype: 'compositefield',
				fieldLabel: 'Pembayaran',
				anchor: '100%',
				width: 250,
				items: 
				[
					viComboJenisTransaksi_MediSmart(195,'CmboPembayaranGridDataView_viKasirLab'),
					//-------------- ## --------------	
				]
			},
			//-------------- ## --------------
			{
				xtype: 'compositefield',
				fieldLabel: ' ',
				anchor: '100%',
				labelSeparator: '',
				width: 250,
				items: 
				[
					viComboJenisTransaksi_MediSmart(195,'CmboPembayaranDuaGridDataView_viKasirLab'),
					//-------------- ## --------------	
				]
			},
			//-------------- ## --------------
        ]
    };
    return items;
}
// End Function getItemPanelInputBiodataGridDataView_viKasirLab # --------------

/**
*   Function : getItemGridTransaksiGridDataView_viKasirLab
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/

function getItemGridTransaksiGridDataView_viKasirLab(lebar) 
{
    var items =
    {
        //title: 'Detail Transaksi', 
        layout: 'form',
        anchor: '100%',     
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        width: lebar-80,
        height:245, 
        items:
        [
            {
                labelWidth:105,
                layout: 'form',
                labelAlign: 'Left',
                border: false,
                items:
                [
                    gridDataViewEditGridDataView_viKasirLab()
                ]   
            }
            //-------------- ## --------------
        ]
    };
    return items;
};
// End Function getItemGridTransaksiGridDataView_viKasirLab # --------------

/**
*   Function : gridDataViewEditGridDataView_viKasirLab
*   
*   Sebuah fungsi untuk menampilkan isian form pembayaran
*/

function gridDataViewEditGridDataView_viKasirLab()
{
    
    var FieldGrdKasirGridDataView_viKasirLab = 
    [
    ];
    
    dsDataGrdJabGridDataView_viKasirLab= new WebApp.DataStore
    ({
        fields: FieldGrdKasirGridDataView_viKasirLab
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJabGridDataView_viKasirLab,
        height: 220,
        selModel: new Ext.grid.RowSelectionModel
        (
            {
                singleSelect: true,
                listeners:
                {
                    rowselect: function(sm, row, rec)
                    {
                    }
                }
            }
        ),
        columns: 
        [
            {
                dataIndex: '',
                header: 'Kode',
                sortable: true,
                width: 50
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Deskripsi',
                sortable: true,
                width: 200
            },
            //-------------- ## --------------          
            {           
                dataIndex: '',
                header: 'Qty',
                sortable: true,
                width: 40
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Biaya',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }           
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Piutang',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }   
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Tunai',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }   
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Disc',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }   
            },
            //-------------- ## --------------
        ]
    }    
    return items;
}
// End Function gridDataViewEditGridDataView_viKasirLab # --------------

// ## END MENU PEMBAYARAN - PEMBAYARAN ## ------------------------------------------------------------------

// ## MENU PEMBAYARAN - TRANSFER KE RAWAT INAP ## ------------------------------------------------------------------

/**
*   Function : FormSetTransferPembayaran_viKasirLab
*   
*   Sebuah fungsi untuk menampilkan windows popup Transfer ke Rawat Inap
*/

function FormSetTransferPembayaran_viKasirLab(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryTransferPembayaran_viKasirLab = new Ext.Window
    (
        {
            id: 'FormGrdTrnsferPembayaran_viKasirLab',
            title: 'Pemindahan Tagihan Ke Unit Rawat Inap',
            closeAction: 'destroy',
            closable:true,
            width: 380,
            height: 450,
            border: false,
            plain: true,
            resizable:false,
            layout: 'form',
            iconCls: 'Edit_Tr',
            constrainHeader : true,
            //padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormTransferPembayaranItemEntryGridDataView_viKasirLab(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryTransferPembayaran_viKasirLab.show();
    mWindowGridLookupTransferPembayaran  = vWinFormEntryTransferPembayaran_viKasirLab; 
};

// End Function FormSetTransferPembayaran_viKasirLab # --------------

/**
*   Function : getFormTransferPembayaranItemEntryGridDataView_viKasirLab
*   
*   Sebuah fungsi untuk menampilkan isian form transfer ke rawat inap
*/
function getFormTransferPembayaranItemEntryGridDataView_viKasirLab(subtotal,NO_KUNJUNGAN)
{
    var pnlFormTransferPembayaranDataWindowPopup_viKasirLab = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            items:
            //-------------- #items# --------------
            [
                {
                    xtype: 'fieldset',
                    title: 'Tagihan dipindahkan ke',
                    frame: false,
                    width: 350,
                    height: 165,
                    items: 
                    [
                        {
                            xtype: 'textfield',
                            fieldLabel: 'No. Transaksi',
                            id: 'txtNoTransaksiTransaksiTransferPembayaran_viKasirLab',
                            name: 'txtNoTransaksiTransaksiTransferPembayaran_viKasirLab',
                            width: 130,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
							xtype: 'compositefield',
							fieldLabel: 'Tgl. Lahir',
							anchor: '100%',
							width: 130,
							items: 
							[
								{
									xtype: 'datefield',
									fieldLabel: 'Tanggal',
									id: 'DateTransaksiTransferPembayaran_viKasirLab',
									name: 'DateTransaksiTransferPembayaran_viKasirLab',
									width: 130,
									format: 'd/M/Y',
								}
								//-------------- ## --------------				
							]
						},
						//-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'No. Rekam Medis',
                            id: 'txtNoRekamMedisTransaksiTransferPembayaran_viKasirLab',
                            name: 'txtNoRekamMedisTransaksiTransferPembayaran_viKasirLab',
                            width: 130,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Nama Pasien',
                            id: 'txtNamaPasienTransaksiTransferPembayaran_viKasirLab',
                            name: 'txtNamaPasienTransaksiTransferPembayaran_viKasirLab',
                            width: 222,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Unit Perawatan',
                            id: 'txtUnitPerawatanTransaksiTransferPembayaran_viKasirLab',
                            name: 'txtUnitPerawatanTransaksiTransferPembayaran_viKasirLab',
                            width: 222,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                    ]
                },
                //-------------- ## --------------
                {
                    xtype: 'fieldset',
                    title: '',
                    frame: false,
                    width: 350,
                    height: 125,
                    items: 
                    [
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Jumlah biaya',
                            id: 'txtJumlhBiayaTransaksiTransferPembayaran_viKasirLab',
                            name: 'txtJumlhBiayaTransaksiTransferPembayaran_viKasirLab',
                            width: 180,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'PAID',
                            id: 'txtPAIDTransaksiTransferPembayaran_viKasirLab',
                            name: 'txtPAIDTransaksiTransferPembayaran_viKasirLab',
                            width: 180,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Jmlh dipindahkan',
                            id: 'txtJumlahDipindahkanTransaksiTransferPembayaran_viKasirLab',
                            name: 'txtJumlahDipindahkanTransaksiTransferPembayaran_viKasirLab',
                            width: 180,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            fieldLabel: 'Saldo Tagihan',
                            id: 'txtSaldoTagihanTransaksiTransferPembayaran_viKasirLab',
                            name: 'txtSaldoTagihanTransaksiTransferPembayaran_viKasirLab',
                            width: 180,
                            style:{'text-align':'right'},
                            readOnly: true,
                        },
                        //-------------- ## --------------
                    ]
                },
                //-------------- ## --------------
                {
                    xtype: 'fieldset',
                    title: 'Alasan Transfer',
                    frame: false,
                    width: 350,
                    height: 62,
                    items: 
                    [
						{
					        xtype: 'panel',
					        title: '',
					        border:false,
					        items: 
					        [           
					            viComboJenisTransaksi_MediSmart(320,'CmboAlasanTransfer_viKasirLab'),
					            //-------------- ## -------------- 
					        ]
					    }
                    ]
                },
                //-------------- ## --------------
                {
					xtype: 'compositefield',
					fieldLabel: ' ',
					anchor: '100%',
					width: 160,
					items: 
					[
						{
		                    xtype:'button',
		                    text:'Ok',
		                    width:70,
		                    style:{'margin-left':'205px'},
		                    hideLabel:true,
		                    id: 'btnOkTransferPembayaran_viKasirLab',
		                    handler:function()
		                    {
		                        
		                    }   
		                },
						//-------------- ## --------------
						{
		                    xtype:'button',
		                    text:'Cancel',
		                    width:70,
		                    style:{'margin-left':'205px'},
		                    hideLabel:true,
		                    id: 'btnCancelTransferPembayaran_viKasirLab',
		                    handler:function()
		                    {
		                        
		                    }   
		                },
						//-------------- ## --------------
					]
				},
                //-------------- ## --------------
            ]
            //-------------- #items# --------------
        }
    )
    return pnlFormTransferPembayaranDataWindowPopup_viKasirLab;
}
// End Function getFormTransferPembayaranItemEntryGridDataView_viKasirLab # --------------

// ## END MENU PEMBAYARAN - TRANSFER KE RAWAT INAP ## ------------------------------------------------------------------

// ## MENU PEMBAYARAN - EDIT TARIF ## ------------------------------------------------------------------

/**
*   Function : FormSetEditTarif_viKasirLab
*   
*   Sebuah fungsi untuk menampilkan windows popup Edit Tarif
*/

function FormSetEditTarif_viKasirLab(subtotal,NO_KUNJUNGAN) 
{
    vWinFormEntryEditTarif_viKasirLab = new Ext.Window
    (
        {
            id: 'FormGrdEditTarif_viKasirLab',
            title: 'Edit Tarif',
            closeAction: 'destroy',
            closable:true,
            width: 475,
            height: 395,
            border: false,
            plain: true,
            resizable:false,
            constrainHeader : true,
            layout: 'form',
            iconCls: 'Edit_Tr',
            //padding: '8px',
            modal: true,                                   
            items: 
            [
                getFormItemEntryEditTarifGridDataView_viKasirLab(subtotal,NO_KUNJUNGAN),
            ],
            listeners:
                { 
                    activate: function()
                    { } 
                }
        }
    );
    vWinFormEntryEditTarif_viKasirLab.show();
    mWindowGridLookupEditTarif  = vWinFormEntryEditTarif_viKasirLab; 
};

// End Function FormSetEditTarif_viKasirLab # --------------

/**
*   Function : getFormItemEntryEditTarifGridDataView_viKasirLab
*   
*   Sebuah fungsi untuk menampilkan isian form edit tarif
*/
function getFormItemEntryEditTarifGridDataView_viKasirLab(subtotal,NO_KUNJUNGAN)
{
    var lebar = 760;
    var pnlFormDataEditTarifWindowPopup_viKasirLab = new Ext.FormPanel
    (
        {
            title: '',
            region: 'center',
            layout: 'anchor',
            padding: '8px',
            bodyStyle: 'padding:10px 0px 10px 10px;',
            fileUpload: true,
            //-------------- #items# --------------
            items:
            [
                getItemPanelInputProdukEditTarifGridDataView_viKasirLab(lebar),
                //-------------- ## -------------- 
                getItemGridTransaksiEditTarifGridDataView_viKasirLab(lebar),
                //-------------- ## --------------
                {
                    xtype: 'compositefield',
                    fieldLabel: ' ',
                    anchor: '100%',
                    //labelSeparator: '',
                    width: 199,
                    style:{'margin-top':'7px'},
                    items: 
                    [
                        {
                            xtype:'button',
                            text:'Ok',
                            width:70,
                            //style:{'margin-left':'190px','margin-top':'7px'},
                            hideLabel:true,
                            id: 'btnOkGridTransaksiEditTarifGridDataView_viKasirLab',
                            handler:function()
                            {
                            }   
                        },
                        //-------------- ## --------------
                        {
                            xtype:'button',
                            text:'Cancel',
                            width:70,
                            //style:{'margin-left':'190px','margin-top':'7px'},
                            hideLabel:true,
                            id: 'btnCancelGridTransaksiEditTarifGridDataView_viKasirLab',
                            handler:function()
                            {
                            }   
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            id: 'txtTarifLamaGridTransaksiEditTarifGridDataView_viKasirLab',
                            name: 'txtTarifLamaGridTransaksiEditTarifGridDataView_viKasirLab',
                            style:{'text-align':'right','margin-left':'60px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                        {
                            xtype: 'textfield',
                            id: 'txtTarifBaruGridTransaksiEditTarifGridDataView_viKasirLab',
                            name: 'txtTarifBaruGridTransaksiEditTarifGridDataView_viKasirLab',
                            style:{'text-align':'right','margin-left':'56px'},
                            width: 100,
                            value: 0,
                            readOnly: true,
                        },
                        //-------------- ## --------------
                    ]
                },  
            ],
            //-------------- #End items# --------------
        }
    )
    return pnlFormDataEditTarifWindowPopup_viKasirLab;
}
// End Function getFormItemEntryEditTarifGridDataView_viKasirLab # --------------

/**
*   Function : getItemPanelInputProdukEditTarifGridDataView_viKasirLab
*   
*   Sebuah fungsi untuk menampilkan isian form edit tarif
*/

function getItemPanelInputProdukEditTarifGridDataView_viKasirLab(lebar) 
{
    var items =
    {
        title: 'Tarif Produk',
        layout: 'Form',
        anchor: '100%',
        width: lebar- 80,
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        items:
        [           
            {
                xtype: 'textfield',
                fieldLabel: 'Produk',
                id: 'txtProdukEditTarifGridDataView_viKasirLab',
                name: 'txtProdukEditTarifGridDataView_viKasirLab',
                emptyText: 'Produk',
                readOnly: true,
                flex: 1,
                width: 195,             
            },
            //-------------- ## --------------
        ]
    };
    return items;
}
// End Function getItemPanelInputProdukEditTarifGridDataView_viKasirLab # --------------

/**
*   Function : getItemGridTransaksiEditTarifGridDataView_viKasirLab
*   
*   Sebuah fungsi untuk menampilkan isian form edit tarif
*/

function getItemGridTransaksiEditTarifGridDataView_viKasirLab(lebar) 
{
    var items =
    {
        title: 'Perubahan Tarif Menjadi', 
        layout: 'form',
        anchor: '100%',     
        labelAlign: 'Left',
        bodyStyle: 'padding:10px 10px 10px 10px',
        border:true,
        width: lebar,
        height:245, 
        items:
        [
            {
                labelWidth:105,
                layout: 'form',
                labelAlign: 'Left',
                border: false,
                items:
                [
                    gridDataViewEditGridEditTarifDataView_viKasirLab()
                ]   
            }
            //-------------- ## --------------
        ]
    };
    return items;
};
// End Function getItemGridTransaksiEditTarifGridDataView_viKasirLab # --------------

/**
*   Function : gridDataViewEditGridEditTarifDataView_viKasirLab
*   
*   Sebuah fungsi untuk menampilkan isian form edit tarif
*/

function gridDataViewEditGridEditTarifDataView_viKasirLab()
{
    
    var FieldGrdKasirGridDataView_viKasirLab = 
    [
    ];
    
    dsDataGrdJabGridDataView_viKasirLab= new WebApp.DataStore
    ({
        fields: FieldGrdKasirGridDataView_viKasirLab
    });
    
    var items = 
    {
        xtype: 'editorgrid',
        store: dsDataGrdJabGridDataView_viKasirLab,
        height: 200,
        selModel: new Ext.grid.RowSelectionModel
        (
            {
                singleSelect: true,
                listeners:
                {
                    rowselect: function(sm, row, rec)
                    {
                    }
                }
            }
        ),
        columns: 
        [
            {
                dataIndex: '',
                header: 'Komponen',
                sortable: true,
                width: 200
            },
            //-------------- ## --------------          
            {           
                dataIndex: '',
                header: 'Tarif Lama',
                sortable: true,
                width: 100
            },
            //-------------- ## --------------
            {
                dataIndex: '',
                header: 'Tarif Baru',
                sortable: true,
                width: 100,
                renderer: function(v, params, record) 
                {
                    
                }           
            },
            //-------------- ## --------------
        ]
    }    
    return items;
}
// End Function gridDataViewEditGridEditTarifDataView_viKasirLab # --------------

// ## END MENU PEMBAYARAN - EDIT TARIF ## ------------------------------------------------------------------

// ## END MENU PEMBAYARAN ## ------------------------------------------------------------------

// --------------------------------------- # End Form # ---------------------------------------

// End Project Kasir Laboratorium # --------------