var kdCustomer = '';
var TrPenJasLab={};
TrPenJasLab.form={};
TrPenJasLab.func={};
TrPenJasLab.vars={};
TrPenJasLab.func.parent=TrPenJasLab;
TrPenJasLab.form.ArrayStore={};
TrPenJasLab.form.ComboBox={};
TrPenJasLab.form.DataStore={};
TrPenJasLab.form.Record={};
TrPenJasLab.form.Form={};
TrPenJasLab.form.Grid={};
TrPenJasLab.form.Panel={};
TrPenJasLab.form.TextField={};
TrPenJasLab.form.Button={};

TrPenJasLab.form.ArrayStore.produk=new Ext.data.ArrayStore({
		id: 0,
		fields: [	
					'uraian','kd_tarif','kd_produk','tgl_transaksi','tgl_berlaku','harga','qty'
				],
		data: []
	});

TrPenJasLab.form.ArrayStore.kodepasien=new Ext.data.ArrayStore({
		id: 0,
		fields: [	
					'no_transaksi','kd_kasir','tgl_transaksi','kd_spesial','kamar','tgl_lahir','kd_pasien','nama','nama_unit_asli',
					'alamat','kd_dokter','kd_unit','nama_unit','urut_masuk','dokter','Jenis_kelamin','gol_darah','kd_customer','customer',
					'no_kamar','kd_spesial','tgl','kd_tarif'
				],
		data: []
	});

TrPenJasLab.form.ArrayStore.pasien=new Ext.data.ArrayStore({
		id: 0,
		fields: [	
					'no_transaksi','kd_kasir','tgl_transaksi','kd_spesial','kamar','tgl_lahir','kd_pasien','nama','nama_unit_asli',
					'alamat','kd_dokter','kd_unit','nama_unit','urut_masuk','dokter','Jenis_kelamin','gol_darah','kd_customer','customer',
					'no_kamar','kd_spesial','tgl','kd_tarif'
				],
		data: []
	});
	
TrPenJasLab.form.ArrayStore.notransaksi=new Ext.data.ArrayStore({
		id: 0,
		fields: [	
					'no_transaksi','kd_kasir','tgl_transaksi','kd_spesial','kamar','tgl_lahir','kd_pasien','nama','nama_unit_asli',
					'alamat','kd_dokter','kd_unit','nama_unit','urut_masuk','dokter','Jenis_kelamin','gol_darah','kd_customer','customer',
					'no_kamar','kd_spesial','tgl','kd_tarif'
				],
		data: []
	});

var CurrentPenJasLab =
{
    data: Object,
    details: Array,
    row: 0
};

var tanggaltransaksitampung;
var mRecordRwj = Ext.data.Record.create
(
    [
       {name: 'DESKRIPSI2', mapping:'DESKRIPSI2'},
       {name: 'KD_PRODUK', mapping:'KD_PRODUK'},
       {name: 'DESKRIPSI', mapping:'DESKRIPSI'},
       {name: 'KD_TARIF', mapping:'KD_TARIF'},
       {name: 'HARGA', mapping:'HARGA'},
       {name: 'QTY', mapping:'QTY'},
       {name: 'TGL_TRANSAKSI', mapping:'TGL_TRANSAKSI'},
       {name: 'DESC_REQ', mapping:'DESC_REQ'},
      // {name: 'KD_TARIF', mapping:'KD_TARIF'},
       {name: 'URUT', mapping:'URUT'}
    ]
);
var CurrentDiagnosa =
{
    data: Object,
    details: Array,
    row: 0
};

var FormLookUpGantidokter;
var mRecordDiagnosa = Ext.data.Record.create
(
    [
       {name: 'KASUS', mapping:'KASUS'},
       {name: 'KD_PENYAKIT', mapping:'KD_PENYAKIT'},
       {name: 'PENYAKIT', mapping:'PENYAKIT'},
       //{name: 'KD_TARIF', mapping:'KD_TARIF'},
      // {name: 'HARGA', mapping:'HARGA'},
       {name: 'STAT_DIAG', mapping:'STAT_DIAG'},
       {name: 'TGL_TRANSAKSI', mapping:'TGL_TRANSAKSI'},
      // {name: 'DESC_REQ', mapping:'DESC_REQ'},
      // {name: 'KD_TARIF', mapping:'KD_TARIF'},
       {name: 'URUT_MASUK', mapping:'URUT_MASUK'}
    ]
);

var now = new Date();
var rowSelectedDiagnosa;
var cellSelecteddeskripsi;
var nowTglTransaksi = new Date();
var nowTglTransaksiGrid = new Date();
var tglGridBawah = nowTglTransaksiGrid.format("d/M/Y");
var tigaharilalu = new Date().add(Date.DAY, -3);

var selectSetGDarahLab;
var selectSetJKLab;
var selectSetKelPasienLab;
var selectSetPerseoranganLab;
var selectSetPerusahaanLab;
var selectSetAsuransiLab;

var dsTRDetailPenJasLabList;
var TmpNotransaksi='';
var KdKasirAsal='';
var TglTransaksiAsal='';
var databaru = 0;
var No_Kamar='';
var Kd_Spesial=0;

var gridDTItemTest;
var grListPenJasLab;
var tampungshiftsekarang;

//var FocusCtrlCMRWJ;
var vkode_customer;
CurrentPage.page = getPanelPenJasLab(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);


//membuat form
function getPanelPenJasLab(mod_id) 
{	
	//form depan awal dan judul tab
    var FormDepanPenJasLab = new Ext.Panel
    (
        {
            id: mod_id,
            closable: true,
            region: 'center',
            layout: 'form',
            title: 'Penata Jasa Laboratorium',
            border: false,
            shadhow: true,
            autoScroll:false,
            iconCls: 'Request',
            margins: '0 5 5 0',
            items: [
						getItemPanelInputPenJasLab(),
						getFormEntryPenJasLab()
                   ],
			tbar:
            [
				'-',
				{
                        text: 'Add new',
                        id: 'btnAddPenJasLab',
                        iconCls: 'add',
                        handler: function()
						{
							addNewData();

						}
				},  
				'-',
				{
                        text: 'Save',
                        id: 'btnSimpanPenJasLab',
                        tooltip: nmSimpan,
                        iconCls: 'save',
						disabled:true,
                        handler: function()
						{
							//loadMask.show();
							Datasave_PenJasLab(false);                                    
							//alert(Ext.getCmp('cboPerseoranganLab').getValue());
						}
				},  
				'-',
				{
                        text: 'Find',
                        id: 'btnCariPenJasLab',
                        iconCls: 'find',
						hidden:true,
                        handler: function()						
						{                                  

						}
				}
				
			],
            listeners:
            {
                'afterrender': function()
                {
					Ext.Ajax.request(
					{
						url: baseURL + "index.php/main/getcurrentshift",
						 params: {
							command: '0',
							// parameter untuk url yang dituju (fungsi didalam controller)
						},
						failure: function(o)
						{
							 var cst = Ext.decode(o.responseText);
							
						},	    
						success: function(o) {
							tampungshiftsekarang=o.responseText
							
						}
					});
				}
            }
        }
    );
	
   return FormDepanPenJasLab;

};

function getFormEntryPenJasLab(lebar,data) 
{
   
 var GDtabDetailPenJasLab = new Ext.TabPanel   
    (
        {
			id:'GDtabDetailPenJasLab',
			region: 'center',
			activeTab: 0,
			height:240,
			//autoHeight:true,
			border:false,
			plain: true,
			defaults:{autoScroll: true},
			items: 
			[
				GetDTGridPenJasLab()//Panel tab detail transaksi
				//-------------- ## --------------
			],
			tbar:
			[
				{
					text: 'Tambah Item Pemeriksaan',
					id: 'btnLookupItemPemeriksaan',
					tooltip: nmLookup,
					iconCls: 'Edit_Tr',
					disabled:true,
					handler: function()
					{

						/* var p = RecordBaruRWJ();
						var str='';
						if (Ext.get('txtKdUnitLab').dom.value  != undefined && Ext.get('txtKdUnitLab').dom.value  != '')
						{
							str =  '41';
						}else{
							str =  '41';
						}
						//FormLookSettingPasswordMainPageSettingPasswordMainPageSettingPasswordMainPageupKasirRWJ(str, dsTRDetailPenJasLabList, p, true, '', true);
						FormLookupKasirRWJ(str,TrPenJasLab.vars.kd_tarif, dsTRDetailPenJasLabList, p, true, '', true);
						 */
						var records = new Array();
						records.push(new dsTRDetailPenJasLabList.recordType());
						dsTRDetailPenJasLabList.add(records);
					}
				}, 
				{
					id:'btnHpsBrsItemLabL',
					text: 'Hapus Baris',
					tooltip: 'Hapus Baris',
					disabled:true,
					iconCls: 'RemoveRow',
					handler: function()
					{
						if (dsTRDetailPenJasLabList.getCount() > 0 ) {
							if (cellSelecteddeskripsi != undefined)
							{
								Ext.Msg.confirm('Warning', 'Apakah item pemeriksaan ini akan dihapus?', function(button){
									if (button == 'yes'){
										if(CurrentPenJasLab != undefined) {
											var line = gridDTItemTest.getSelectionModel().selection.cell[0];
											dsTRDetailPenJasLabList.removeAt(line);
											gridDTItemTest.getView().refresh();
										}
									}
								})
							} else {
								ShowPesanWarningPenJasLab('Pilih record ','Hapus data');
							}
						}
					}
				}
			],
        }
		
    );
	
    return GDtabDetailPenJasLab
};

	
function GetDTGridPenJasLab() 
{
    var fldDetail = [];
	
    dsTRDetailPenJasLabList = new WebApp.DataStore({ fields: fldDetail })
	ViewGridBawahLookupPenjasLab() ;
    gridDTItemTest = new Ext.grid.EditorGridPanel
    (
        {
            title: 'Item Test',
            stripeRows: true,
            store: dsTRDetailPenJasLabList,
            border: false,
            columnLines: true,
            frame: false,
            autoScroll:true,
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                            cellSelecteddeskripsi = dsTRDetailPenJasLabList.getAt(row);
                            CurrentPenJasLab.row = row;
                            CurrentPenJasLab.data = cellSelecteddeskripsi;
                        }
                    }
                }
            ),
            cm: TRPenJasLabColumModel()
        }
		
		
    );

    return gridDTItemTest;
};

function TRPenJasLabColumModel() 
{
    return new Ext.grid.ColumnModel
    (
        [
            new Ext.grid.RowNumberer(),
			{
                header: 'kd_tarif',
                dataIndex: 'kd_tarif',
                width:250,
				menuDisabled:true,
				hidden :true
                
            },
            {
                header: 'Kode Produk',
                dataIndex: 'kd_produk',
                width:100,
                menuDisabled:true,
                hidden:true
            },
            /* {
                header:'Nama Produk',
                dataIndex: 'deskripsi',
                sortable: false,
                hidden:false,
                menuDisabled:true,
                width:320
                
            }, */
			{			
				dataIndex: 'deskripsi',
				header: 'Nama Pemeriksaan',
				sortable: true,
				menuDisabled:true,
				width: 320,
				editor:new Nci.form.Combobox.autoComplete({
					store	: TrPenJasLab.form.ArrayStore.produk,
					select	: function(a,b,c){
						var line = gridDTItemTest.getSelectionModel().selection.cell[0];
						dsTRDetailPenJasLabList.data.items[line].data.deskripsi=b.data.deskripsi;
						dsTRDetailPenJasLabList.data.items[line].data.uraian=b.data.uraian;
						dsTRDetailPenJasLabList.data.items[line].data.kd_tarif=b.data.kd_tarif;
						dsTRDetailPenJasLabList.data.items[line].data.kd_produk=b.data.kd_produk;
						dsTRDetailPenJasLabList.data.items[line].data.tgl_transaksi=b.data.tgl_transaksi;
						dsTRDetailPenJasLabList.data.items[line].data.tgl_berlaku=b.data.tgl_berlaku;
						dsTRDetailPenJasLabList.data.items[line].data.harga=b.data.harga;
						dsTRDetailPenJasLabList.data.items[line].data.qty=b.data.qty;
						
						gridDTItemTest.getView().refresh();
					},
					insert	: function(o){
						return {
							uraian        	: o.uraian,
							kd_tarif 		: o.kd_tarif,
							kd_produk		: o.kd_produk,
							tgl_transaksi	: o.tgl_transaksi,
							tgl_berlaku		: o.tgl_berlaku,
							harga			: o.harga,
							qty				: o.qty,
							deskripsi		: o.deskripsi,
							text			:  '<table style="font-size: 11px;"><tr><td width="60">'+o.kd_produk+'</td><td width="150">'+o.deskripsi+'</td></tr></table>'
						}
					},
					url		: baseURL + "index.php/main/functionLAB/getProduk",
					valueField: 'deskripsi',
					displayField: 'text',
					listWidth: 210
				})
			},
            {
				header: 'Tanggal Transaksi',
				dataIndex: 'tgl_transaksi',
				width: 130,
				menuDisabled:true,
				renderer: function(v, params, record)
				{
					if(record.data.tgl_transaksi == undefined){
						record.data.tgl_transaksi=tglGridBawah;
						return record.data.tgl_transaksi;
					} else{
						if(record.data.tgl_transaksi.substring(5, 4) == '-'){
							return ShowDate(record.data.tgl_transaksi);
						} else{
							var tgl=record.data.tgl_transaksi.split("/");
						
							if(tgl[2].length == 4 && isNaN(tgl[1])){
								return record.data.tgl_transaksi;
							} else{
								return ShowDate(record.data.tgl_transaksi);
							}
						}
						
					}
				}
            },
			{
				header: 'Tanggal Berlaku',
				dataIndex: 'tgl_berlaku',
				width: 130,
				menuDisabled:true,
				hidden: true,
				renderer: function(v, params, record)
				{
					if(record.data.tgl_berlaku == undefined || record.data.tgl_berlaku == null){
						record.data.tgl_berlaku=tglGridBawah;
						return record.data.tgl_berlaku;
					} else{
						if(record.data.tgl_berlaku.substring(5, 4) == '-'){
							return ShowDate(record.data.tgl_berlaku);
						} else{
							var tglb=record.data.tgl_berlaku.split("-");
						
							if(tglb[2].length == 4 && isNaN(tglb[1])){
								return record.data.tgl_berlaku;
							} else{
								return ShowDate(record.data.tgl_berlaku);
							}
						}
						
					}
				}
            },
            {
                header: 'Harga',
                align: 'right',
                hidden: false,
                menuDisabled:true,
                dataIndex: 'harga',
                width:100,
				renderer: function(v, params, record) 
				{
					return formatCurrency(record.data.harga);
				}	
            },
            {
                header: 'Qty',
                width:91,
				align: 'right',
				menuDisabled:true,
                dataIndex: 'qty',
                editor: new Ext.form.NumberField (
					{allowBlank: false}
                   /*  {
                        id:'fieldcolProblemRWJ',
                        allowBlank: true,
                        enableKeyEvents : true,
                        width:30,
						listeners:
						{ 
							'specialkey' : function()
							{
								Dataupdate_PenJasRad(false);
							}
						}
                    } */
                ),
				
				
              
            }

        ]
    )
};

function RecordBaruRWJ()
{
	var tgltranstoday = Ext.getCmp('dtpKunjunganL').getValue();
	var p = new mRecordRwj
	(
		{
			'DESKRIPSI2':'',
			'KD_PRODUK':'',
		    'DESKRIPSI':'', 
		    'KD_TARIF':'', 
		    'HARGA':'',
		    'QTY':'',
		    'TGL_TRANSAKSI':tgltranstoday.format('Y/m/d'), 
		    'DESC_REQ':'',
		    'KD_TARIF':'',
		    'URUT':''
		}
	);
	
	return p;
};


//tampil grid bawah penjas lab
function ViewGridBawahLookupPenjasLab(no_transaksi) 
{
     var strKriteriaRWJ='';
    strKriteriaRWJ = "\"no_transaksi\" = ~" + no_transaksi + "~" + " And kd_kasir ='03'";
   
    dsTRDetailPenJasLabList.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'tgl_transaksi',
			    Sortdir: 'ASC',
			    target: 'ViewDetailGridBawahPenJasLab',
			    param: strKriteriaRWJ
			}
		}
	);
    return dsTRDetailPenJasLabList; 
}

function ViewGridBawahPenjasLab(no_transaksi,data) 
{	
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/main/functionLAB/getItemPemeriksaan",
			params: {no_transaksi:no_transaksi},
			failure: function(o)
			{
				ShowPesanErrorPenJasLab('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dsTRDetailPenJasLabList.removeAll();
					var recs=[],
						recType=dsTRDetailPenJasLabList.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
						
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
					dsTRDetailPenJasLabList.add(recs);
					
					gridDTItemTest.getView().refresh();
					if(dsTRDetailPenJasLabList.getCount() > 0 ){
						Ext.getCmp('cboDOKTER_viPenJasLab').setReadOnly(true);
						TmpNotransaksi=data.no_transaksi;
						KdKasirAsal=data.kd_kasir;
						TglTransaksiAsal=data.tgl_transaksi;
						Kd_Spesial=data.kd_spesial;
						No_Kamar=data.no_kamar;
						
						getDokterPengirim(data.no_transaksi,data.kd_kasir);
						//TrPenJasLab.form.ComboBox.notransaksi.setValue(data.no_transaksi);
						Ext.getCmp('txtNamaUnitLab').setValue(data.nama_unit);
						Ext.getCmp('txtKdUnitLab').setValue(data.kd_unit);
						Ext.getCmp('txtKdDokter').setValue(data.kd_dokter);
						Ext.getCmp('cboDOKTER_viPenJasLab').setValue(data.kd_dokter);
						Ext.getCmp('txtAlamatL').setValue(data.alamat);
						Ext.getCmp('dtpTtlL').setValue(ShowDate(data.tgl_lahir));
						Ext.getCmp('txtKdUrutMasuk').setValue(data.urut_masuk);
						
						Ext.getCmp('cboJKPenjasLab').setValue(data.jenis_kelamin);
						if (data.jenis_kelamin === 't')
						{
							Ext.getCmp('cboJKPenjasLab').setValue('Laki-laki');
						}else
						{
							Ext.getCmp('cboJKPenjasLab').setValue('Perempuan');
						}
						Ext.getCmp('cboJKPenjasLab').disable();
						
						Ext.getCmp('cboGDRPenJasLab').setValue(data.gol_darah);
						if (data.gol_darah === '0')
						{
							Ext.getCmp('cboGDRPenJasLab').setValue('-');
						}else if (data.gol_darah === '1')
						{
							Ext.getCmp('cboGDRPenJasLab').setValue('A+');
						}else if (data.gol_darah === '2')
						{
							Ext.getCmp('cboGDRPenJasLab').setValue('B+');
						}else if (data.gol_darah === '3')
						{
							Ext.getCmp('cboGDRPenJasLab').setValue('AB+');
						}else if (data.gol_darah === '4')
						{
							Ext.getCmp('cboGDRPenJasLab').setValue('O+');
						}else if (data.gol_darah === '5')
						{
							Ext.getCmp('cboGDRPenJasLab').setValue('A-');
						}else if (data.gol_darah === '6')
						{
							Ext.getCmp('cboGDRPenJasLab').setValue('B-');
						}
						Ext.getCmp('cboGDRPenJasLab').disable();
						
						Ext.getCmp('txtKdCustomerLamaHide').setValue(data.kd_customer);
						
						//tampung kd customer untuk transaksi selain kunjungan langsung
						vkode_customer = data.kd_customer;
						Ext.getCmp('cboKelPasienLab').enable();
						if(vkode_customer=='0000000001'){
							Ext.getCmp('cboKelPasienLab').setValue('Perseorangan');
							Ext.getCmp('txtCustomerLamaHide').hide();
							Ext.getCmp('cboPerseoranganLab').setValue(data.kd_customer);
						}else{
							Ext.getCmp('txtCustomerLamaHide').show();
							Ext.getCmp('cboKelPasienLab').hide();
							Ext.getCmp('txtCustomerLamaHide').setValue(data.customer);
						}
						
						//Ext.getCmp('txtNoTransaksiPenJasLab').disable();
						Ext.getCmp('txtDokterPengirimL').disable();
						Ext.getCmp('txtNamaUnitLab').disable();
						Ext.getCmp('cboDOKTER_viPenJasLab').disable();
						Ext.getCmp('txtAlamatL').disable();
						Ext.getCmp('cboJKPenjasLab').disable();
						Ext.getCmp('cboGDRPenJasLab').disable();
						Ext.getCmp('cboKelPasienLab').enable();
						Ext.getCmp('dtpTtlL').disable();
						Ext.getCmp('dtpKunjunganL').disable();
						Ext.getCmp('txtCustomerLamaHide').disable();
						
						Ext.getCmp('btnLookupItemPemeriksaan').disable();
						Ext.getCmp('btnHpsBrsItemLabL').disable();
						Ext.getCmp('btnSimpanPenJasLab').disable();
						
					} else {
						TmpNotransaksi=data.no_transaksi;
						KdKasirAsal=data.kd_kasir;
						TglTransaksiAsal=data.tgl_transaksi;
						Kd_Spesial=data.kd_spesial;
						No_Kamar=data.no_kamar;
						
						Ext.getCmp('txtNamaUnitLab').setValue(data.nama_unit_asli);
						Ext.getCmp('txtKdUnitLab').setValue(data.kd_unit);
						Ext.getCmp('txtDokterPengirimL').setValue(data.dokter);
						Ext.getCmp('txtAlamatL').setValue(data.alamat);
						Ext.getCmp('dtpTtlL').setValue(ShowDate(data.tgl_lahir));
						
						Ext.getCmp('cboJKPenjasLab').setValue(data.jenis_kelamin);
						if (data.jenis_kelamin === 't')
						{
							Ext.getCmp('cboJKPenjasLab').setValue('Laki-laki');
						}else
						{
							Ext.getCmp('cboJKPenjasLab').setValue('Perempuan');
						}
						Ext.getCmp('cboJKPenjasLab').disable();
						
						Ext.getCmp('cboGDRPenJasLab').setValue(data.gol_darah);
						if (data.gol_darah === '0')
						{
							Ext.getCmp('cboGDRPenJasLab').setValue('-');
						}else if (data.gol_darah === '1')
						{
							Ext.getCmp('cboGDRPenJasLab').setValue('A+');
						}else if (data.gol_darah === '2')
						{
							Ext.getCmp('cboGDRPenJasLab').setValue('B+');
						}else if (data.gol_darah === '3')
						{
							Ext.getCmp('cboGDRPenJasLab').setValue('AB+');
						}else if (data.gol_darah === '4')
						{
							Ext.getCmp('cboGDRPenJasLab').setValue('O+');
						}else if (data.gol_darah === '5')
						{
							Ext.getCmp('cboGDRPenJasLab').setValue('A-');
						}else if (data.gol_darah === '6')
						{
							Ext.getCmp('cboGDRPenJasLab').setValue('B-');
						}
						Ext.getCmp('cboGDRPenJasLab').disable();
						
						Ext.getCmp('txtKdCustomerLamaHide').setValue(data.kd_customer);
						
						//tampung kd customer untuk transaksi selain kunjungan langsung
						vkode_customer = data.kd_customer;
						Ext.getCmp('cboKelPasienLab').enable();
						if(vkode_customer=='0000000001'){
							Ext.getCmp('cboKelPasienLab').setValue('Perseorangan');
							Ext.getCmp('txtCustomerLamaHide').hide();
							Ext.getCmp('cboPerseoranganLab').setValue(data.kd_customer);
						}else{
							Ext.getCmp('txtCustomerLamaHide').show();
							Ext.getCmp('cboKelPasienLab').hide();
							Ext.getCmp('txtCustomerLamaHide').setValue(data.customer);
						}
						
						Ext.getCmp('txtDokterPengirimL').disable();
						Ext.getCmp('txtNamaUnitLab').disable();
						Ext.getCmp('cboDOKTER_viPenJasLab').enable();
						Ext.getCmp('txtAlamatL').disable();
						Ext.getCmp('cboJKPenjasLab').disable();
						Ext.getCmp('cboGDRPenJasLab').disable();
						Ext.getCmp('cboKelPasienLab').enable();
						Ext.getCmp('dtpTtlL').disable();
						Ext.getCmp('dtpKunjunganL').disable();
						Ext.getCmp('txtCustomerLamaHide').disable();
						
						Ext.getCmp('btnLookupItemPemeriksaan').enable();
						Ext.getCmp('btnHpsBrsItemLabL').disable();
						Ext.getCmp('btnSimpanPenJasLab').enable();
					}
				} 
			}
		}
		
	)
};

//---------------------------------------------------------------------------------------///

//---------------------------------------------------------------------------------------///
function getParamDetailTransaksiLAB() 
{
	Ext.Ajax.request(
	{
	    url: baseURL + "index.php/main/getcurrentshift",
		 params: {
	        command: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
	    },
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
	    success: function(o) {
			tampungshiftsekarang=o.responseText
			
	    }
	});
	var KdCust='';
	var TmpCustoLama='';
	var pasienBaru;
	if(Ext.get('cboKelPasienLab').getValue()=='Perseorangan'){
		KdCust=Ext.getCmp('cboPerseoranganLab').getValue();
	}else if(Ext.get('cboKelPasienLab').getValue()=='Perusahaan'){
		KdCust=Ext.getCmp('cboPerusahaanRequestEntryLab').getValue();
	}else {
		KdCust=Ext.getCmp('cboAsuransiLab').getValue();
	}
		
	if(TrPenJasLab.form.ComboBox.kdpasien.getValue() == ''){
		pasienBaru=1;
	} else{
		pasienBaru=0;
	}
		
    var params =
	{
		KdTransaksi: TrPenJasLab.form.ComboBox.notransaksi.getValue(),
		KdPasien:TrPenJasLab.form.ComboBox.kdpasien.getValue(),
		NmPasien:TrPenJasLab.form.ComboBox.pasien.getValue(),
		Ttl:Ext.getCmp('dtpTtlL').getValue(),
		Alamat:Ext.getCmp('txtAlamatL').getValue(),
		JK:Ext.getCmp('cboJKPenjasLab').getValue(),
		GolDarah:Ext.getCmp('cboGDRPenJasLab').getValue(),
		KdUnit: Ext.getCmp('txtKdUnitLab').getValue(),
		KdDokter:Ext.getCmp('cboDOKTER_viPenJasLab').getValue(),
		Tgl: Ext.getCmp('dtpKunjunganL').getValue(),

		TglTransaksiAsal:TglTransaksiAsal,

		//KdCusto:KdCust,
		KdCusto : kdCustomer,
		TmpCustoLama:KdCust, //Ext.getCmp('txtKdCustomerLamaHide').getValue(),//jika customer dengan transaksi lama
		NamaPesertaAsuransi:Ext.getCmp('txtNamaPesertaAsuransiLab').getValue(),
		NoAskes:Ext.getCmp('txtNoAskesLab').getValue(),
		NoSJP:Ext.getCmp('txtNoSJPLab').getValue(),

		Shift: tampungshiftsekarang,
		List:getArrPenJasLab(),
		JmlField: mRecordRwj.prototype.fields.length-4,
		JmlList:GetListCountDetailTransaksi(),
		unit:41,
		TmpNotransaksi:TmpNotransaksi,
		KdKasirAsal:KdKasirAsal,
		KdSpesial:Kd_Spesial,
		Kamar:No_Kamar,
		pasienBaru:pasienBaru,
		
		KdProduk:dsTRDetailPenJasLabList.data.items[CurrentPenJasLab.row].data.KD_PRODUK
		
	};
    return params
};

function GetListCountDetailTransaksi()
{
	
	var x=0;
	for(var i = 0 ; i < dsTRDetailPenJasLabList.getCount();i++)
	{
		if (dsTRDetailPenJasLabList.data.items[i].data.KD_PRODUK != '' || dsTRDetailPenJasLabList.data.items[i].data.DESKRIPSI  != '')
		{
			x += 1;
		};
	}
	return x;
	
};





function getArrPenJasLab()
{
	var x='';
	var arr=[];
	for(var i = 0 ; i < dsTRDetailPenJasLabList.getCount();i++)
	{
		
		if (dsTRDetailPenJasLabList.data.items[i].data.kd_produk != '' && dsTRDetailPenJasLabList.data.items[i].data.deskripsi != '')
		{
			var o={};
			var y='';
			var z='@@##$$@@';
			o['URUT']= dsTRDetailPenJasLabList.data.items[i].data.urut;
			o['KD_PRODUK']= dsTRDetailPenJasLabList.data.items[i].data.kd_produk;
			o['QTY']= dsTRDetailPenJasLabList.data.items[i].data.qty;
			o['TGL_TRANSAKSI']= dsTRDetailPenJasLabList.data.items[i].data.tgl_transaksi;
			o['TGL_BERLAKU']= dsTRDetailPenJasLabList.data.items[i].data.tgl_berlaku;
			o['HARGA']= dsTRDetailPenJasLabList.data.items[i].data.harga;
			o['KD_TARIF']= dsTRDetailPenJasLabList.data.items[i].data.kd_tarif;
			arr.push(o);
			
		};
	}	
	
	return Ext.encode(arr);
};

//panel dalam lookup detail pasien
function getItemPanelInputPenJasLab(lebar) 
{
	//pengaturan panel data pasien
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:5px 5px 5px 5px',
	    border:false,
	    height:240,
	    items:
		[
					{ //awal panel biodata pasien
						columnWidth:.99,
						layout: 'absolute',
						bodyStyle: 'padding: 10px 10px 10px 10px',
						border: true,
						width: 100,
						height: 120,
						anchor: '100% 100%',
						items:
						[
                            //bagian pinggir kiri                            
							{
								x: 10,
								y: 10,
								xtype: 'label',
								text: 'No. Transaksi  '
							},
							{
								x: 120,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							/* {
								x : 130,
								y : 10,
								xtype: 'numberfield',
								name: 'txtNoTransaksiPenJasLab',
								id: 'txtNoTransaksiPenJasLab',
								width: 100,
								readOnly: true

							}, */
							TrPenJasLab.form.ComboBox.notransaksi= new Nci.form.Combobox.autoComplete({
								store	: TrPenJasLab.form.ArrayStore.notransaksi,
								select	: function(a,b,c){
									ViewGridBawahPenjasLab(b.data.no_transaksi,b.data);
									TrPenJasLab.form.ComboBox.kdpasien.disable();
									TrPenJasLab.form.ComboBox.kdpasien.setValue(b.data.kd_pasien);
									TrPenJasLab.form.ComboBox.pasien.disable();
									TrPenJasLab.form.ComboBox.pasien.setValue(b.data.nama);
									
									Ext.getCmp('btnLookupItemPemeriksaan').disable();
									Ext.getCmp('btnHpsBrsItemLabL').disable();
									Ext.getCmp('btnSimpanPenJasLab').disable();
									
									TrPenJasLab.vars.kd_tarif=b.data.kd_tarif;
									
									Ext.Ajax.request(
									{
										url: baseURL + "index.php/main/getcurrentshift",
										 params: {
											command: '0',
											// parameter untuk url yang dituju (fungsi didalam controller)
										},
										failure: function(o)
										{
											 var cst = Ext.decode(o.responseText);
											
										},	    
										success: function(o) {
											tampungshiftsekarang=o.responseText
											
										}
										
									
									});
									
								},
								width	: 100,
								insert	: function(o){
									return {
										nama        		:o.nama,
										tgl_transaksi		:o.tgl_transaksi,
										kd_spesial			:o.kd_spesial,
										kamar				:o.kamar,
										tgl_lahir			:o.tgl_lahir,
										gol_darah			:o.gol_darah,
										jenis_kelamin		:o.jenis_kelamin,
										dokter				:o.dokter,
										urut_masuk			:o.urut_masuk,
										nama_unit			:o.nama_unit,
										alamat				:o.alamat,
										kd_pasien        	:o.kd_pasien,
										kd_dokter			:o.kd_dokter,
										kd_unit				:o.kd_unit,
										kd_customer			:o.kd_customer,
										kd_kasir			:o.kd_kasir,
										nama_kamar			:o.nama_kamar,
										no_kamar			:o.no_kamar,
										customer			:o.customer,
										nama_unit_asli		:o.nama_unit_asli,
										tgl					:o.tgl,
										kd_tarif			:o.kd_tarif,
										text				:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_pasien+'</td><td width="70" align="left">'+o.no_transaksi+'</td><td width="130">'+o.nama+'</td><td width="110" align="left">'+o.nama_unit_asli+'</td><td width="70">'+o.tgl+'</td></tr></table>',
										no_transaksi		:o.no_transaksi,
									}
								},
								param:function(){
									var a=0;
									return {
										tanggal:Ext.getCmp('dtpKunjunganL').getValue(),
										a:0
									}
								},
								url		: baseURL + "index.php/main/functionLAB/getPasien",
								valueField: 'no_transaksi',
								displayField: 'text',
								listWidth: 480,
								x : 130,
								y : 10,
								emptyText: 'No Transaksi'
							}),
							{
								x: 10,
								y: 40,
								xtype: 'label',
								text: 'No. Medrec  '
							},
							{
								x: 120,
								y: 40,
								xtype: 'label',
								text: ':'
							},
							/* {
								x: 130,
								y: 40,
								xtype: 'textfield',
								fieldLabel: 'No. Medrec',
								name: 'txtNoMedrecL',
								id: 'txtNoMedrecL',
								width: 120,
								emptyText:'Otomatis',
								listeners: 
								{ 
									
								}
							}, */
							TrPenJasLab.form.ComboBox.kdpasien= new Nci.form.Combobox.autoComplete({
								store	: TrPenJasLab.form.ArrayStore.kodepasien,
								select	: function(a,b,c){
									TrPenJasLab.form.ComboBox.notransaksi.setValue('');
									ViewGridBawahPenjasLab(b.data.no_transaksi,b.data);
									TrPenJasLab.form.ComboBox.pasien.disable();
									TrPenJasLab.form.ComboBox.pasien.setValue(b.data.nama);
									TrPenJasLab.form.ComboBox.notransaksi.disable();
									TrPenJasLab.form.ComboBox.notransaksi.setValue(b.data.no_transaksi);
									//tambahan hz
									kdCustomer = b.data.kd_customer
									//akhir tambahan
									TrPenJasLab.vars.kd_tarif=b.data.kd_tarif;
									
									Ext.Ajax.request(
									{
										url: baseURL + "index.php/main/getcurrentshift",
										 params: {
											command: '0',
											// parameter untuk url yang dituju (fungsi didalam controller)
										},
										failure: function(o)
										{
											 var cst = Ext.decode(o.responseText);
											
										},	    
										success: function(o) {
											tampungshiftsekarang=o.responseText
											
										}
										
									
									});
									
								},
								width	: 120,
								insert	: function(o){
									return {
										no_transaksi		:o.no_transaksi,
										tgl_transaksi		:o.tgl_transaksi,
										kd_spesial			:o.kd_spesial,
										kamar				:o.kamar,
										tgl_lahir			:o.tgl_lahir,
										gol_darah			:o.gol_darah,
										jenis_kelamin		:o.jenis_kelamin,
										dokter				:o.dokter,
										urut_masuk			:o.urut_masuk,
										nama_unit			:o.nama_unit,
										alamat				:o.alamat,
										kd_pasien        	:o.kd_pasien,
										nama        		:o.nama,
										kd_dokter			:o.kd_dokter,
										kd_unit				:o.kd_unit,
										kd_customer			:o.kd_customer,
										kd_kasir			:o.kd_kasir,
										nama_kamar			:o.nama_kamar,
										no_kamar			:o.no_kamar,
										customer			:o.customer,
										nama_unit_asli		:o.nama_unit_asli,
										tgl					:o.tgl,
										kd_tarif			:o.kd_tarif,
										text				:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_pasien+'</td><td width="70" align="left">'+o.no_transaksi+'</td><td width="130">'+o.nama+'</td><td width="110" align="left">'+o.nama_unit_asli+'</td><td width="70">'+o.tgl+'</td></tr></table>',
										kd_pasien	 		:o.kd_pasien
									}
								},
								param:function(){
									var a=0;
									return {
										tanggal:Ext.getCmp('dtpKunjunganL').getValue(),
										a:1
									}
								},
								url		: baseURL + "index.php/main/functionLAB/getPasien",
								valueField: 'kd_pasien',
								displayField: 'text',
								listWidth: 480,
								x: 130,
								y: 40,
								emptyText: 'Kode Pasien'
							}),
							{
								x: 10,
								y: 70,
								xtype: 'label',
								text: 'Unit  '
							},
							{
								x: 120,
								y: 70,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 70,
								xtype: 'textfield',
								name: 'txtNamaUnitLab',
								id: 'txtNamaUnitLab',
								readOnly:true,
								width: 120
							},
							{
								x: 10,
								y: 100,
								xtype: 'label',
								text: 'Dokter Pengirim'
							},
							{
								x: 120,
								y: 100,
								xtype: 'label',
								text: ':'
							},
							{
								x: 130,
								y: 100,
								xtype: 'textfield',
								fieldLabel: '',
								readOnly:true,
								name: 'txtDokterPengirimL',
								id: 'txtDokterPengirimL',
								width: 200
							},
							{
								x: 10,
								y: 130,
								xtype: 'label',
								text: 'Dokter Lab'
							},
							{
								x: 120,
								y: 130,
								xtype: 'label',
								text: ':'
							},
							mComboDOKTER(),
							{
								x: 10,
								y: 160,
								xtype: 'label',
								text: 'Kelompok Pasien '
							},
							{
								x: 120,
								y: 160,
								xtype: 'label',
								text: ':'
							},
							mCboKelompokpasien(),
							mComboPerseorangan(),
							mComboPerusahaan(),
							mComboAsuransi(),
							//bagian tengah
							{
								x: 280,
								y: 10,
								xtype: 'label',
								text: 'Tanggal Kunjung '
							},
							{
								x: 390,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 400,
								y: 10,
								xtype: 'datefield',
								fieldLabel: 'Tanggal hari ini ',
								id: 'dtpKunjunganL',
								name: 'dtpKunjunganL',
								format: 'd/M/Y',
								readOnly : false,
								value: now,
								width: 110
							},
							{
								x: 280,
								y: 40,
								xtype: 'label',
								text: 'Nama Pasien '
							},
							{
								x: 390,
								y: 40,
								xtype: 'label',
								text: ':'
							},
							/* {
								x: 400,
								y: 40,
								xtype: 'textfield',
								fieldLabel: '',
								name: 'txtNamaPasienL',
								id: 'txtNamaPasienL',
								width: 200
							}, */
							TrPenJasLab.form.ComboBox.pasien= new Nci.form.Combobox.autoComplete({
								store	: TrPenJasLab.form.ArrayStore.pasien,
								select	: function(a,b,c){
									TrPenJasLab.form.ComboBox.notransaksi.setValue('');
									ViewGridBawahPenjasLab(b.data.no_transaksi,b.data);
									TrPenJasLab.form.ComboBox.kdpasien.disable();
									TrPenJasLab.form.ComboBox.kdpasien.setValue(b.data.kd_pasien);
									TrPenJasLab.form.ComboBox.notransaksi.disable();
									TrPenJasLab.form.ComboBox.notransaksi.setValue(b.data.no_transaksi);
									
									TrPenJasLab.vars.kd_tarif=b.data.kd_tarif;
									
									Ext.Ajax.request(
									{
										url: baseURL + "index.php/main/getcurrentshift",
										 params: {
											command: '0',
											// parameter untuk url yang dituju (fungsi didalam controller)
										},
										failure: function(o)
										{
											 var cst = Ext.decode(o.responseText);
											
										},	    
										success: function(o) {
											tampungshiftsekarang=o.responseText
											
										}
										
									
									});
									
								},
								width	: 200,
								insert	: function(o){
									return {
										no_transaksi		:o.no_transaksi,
										tgl_transaksi		:o.tgl_transaksi,
										kd_spesial			:o.kd_spesial,
										kamar				:o.kamar,
										tgl_lahir			:o.tgl_lahir,
										gol_darah			:o.gol_darah,
										jenis_kelamin		:o.jenis_kelamin,
										dokter				:o.dokter,
										urut_masuk			:o.urut_masuk,
										nama_unit			:o.nama_unit,
										alamat				:o.alamat,
										kd_pasien        	:o.kd_pasien,
										kd_dokter			:o.kd_dokter,
										kd_unit				:o.kd_unit,
										kd_customer			:o.kd_customer,
										kd_kasir			:o.kd_kasir,
										nama_kamar			:o.nama_kamar,
										no_kamar			:o.no_kamar,
										customer			:o.customer,
										nama_unit_asli		:o.nama_unit_asli,
										tgl					:o.tgl,
										kd_tarif			:o.kd_tarif,
										text				:  '<table style="font-size: 11px;"><tr><td width="70">'+o.kd_pasien+'</td><td width="70" align="left">'+o.no_transaksi+'</td><td width="130">'+o.nama+'</td><td width="110" align="left">'+o.nama_unit_asli+'</td><td width="70">'+o.tgl+'</td></tr></table>',
										nama        		:o.nama,
									}
								},
								param:function(){
									var a=0;
									return {
										tanggal:Ext.getCmp('dtpKunjunganL').getValue(),
										a:2
									}
								},
								url		: baseURL + "index.php/main/functionLAB/getPasien",
								valueField: 'nama',
								displayField: 'text',
								listWidth: 480,
								x: 400,
								y: 40,
								emptyText: 'Nama'
							}),
							{
								x: 280,
								y: 70,
								xtype: 'label',
								text: 'Alamat '
							},
							{
								x: 390,
								y: 70,
								xtype: 'label',
								text: ':'
							},
							{
								x: 400,
								y: 70,
								xtype: 'textfield',
								fieldLabel: '',
								name: 'txtAlamatL',
								id: 'txtAlamatL',
								width: 395
							},
							{
								x: 400,
								y: 100,
								xtype: 'label',
								text: 'Jenis Kelamin '
							},
							{
								x: 485,
								y: 100,
								xtype: 'label',
								text: ':'
							},
							mComboJK(),
							{
								x: 610,
								y: 100,
								xtype: 'label',
								text: 'Gol. Darah '
							},
							{
								x: 680,
								y: 100,
								xtype: 'label',
								text: ':'
							},
							mComboGDarah(),
							{
								x: 610,
								y: 40,
								xtype: 'label',
								text: 'Tanggal lahir '
							},
							{
								x: 700,
								y: 40,
								xtype: 'label',
								text: ':'
							},
							{
								x: 710,
								y: 40,
								xtype: 'datefield',
								fieldLabel: 'Tanggal ',
								id: 'dtpTtlL',
								name: 'dtpTtlL',
								format: 'd/M/Y',
								value: now,
								width: 100
							},
							
							{
								x: 475,
								y: 160,
								xtype: 'textfield',
								fieldLabel: 'Nama Peserta',
								maxLength: 200,
								name: 'txtNamaPesertaAsuransiLab',
								id: 'txtNamaPesertaAsuransiLab',
								emptyText:'Nama Peserta Asuransi',
								width: 150
							},
							{
								x: 280,
								y: 190,
								xtype: 'textfield',
								fieldLabel: 'No. Askes',
								maxLength: 200,
								name: 'txtNoAskesLab',
								id: 'txtNoAskesLab',
								emptyText:'No Askes',
								width: 150
							},
							{
								x: 475,
								y: 190,
								xtype: 'textfield',
								fieldLabel: 'No. SJP',
								maxLength: 200,
								name: 'txtNoSJPLab',
								id: 'txtNoSJPLab',
								emptyText:'No SJP',
								width: 150
							},
							
							
							//---------------------hidden----------------------------------
							{
								x: 130,
								y: 160,
								xtype: 'textfield',
								fieldLabel:'Nama customer lama',
								name: 'txtCustomerLamaHide',
								id: 'txtCustomerLamaHide',
								readOnly:true,
								hidden:true,
								width: 280
							},
							{
								x: 130,
								y: 160,
								xtype: 'textfield',
								fieldLabel:'Kode customer  ',
								name: 'txtKdCustomerLamaHide',
								id: 'txtKdCustomerLamaHide',
								readOnly:true,
								hidden:true,
								width: 280
							},
							
							{
								xtype: 'textfield',
								fieldLabel:'Dokter  ',
								name: 'txtKdDokter',
								id: 'txtKdDokter',
								readOnly:true,
								hidden:true,
								anchor: '99%'
							},
							{
								xtype: 'textfield',
								fieldLabel:'Dokter  ',
								name: 'txtKdDokterPengirim',
								id: 'txtKdDokterPengirim',
								readOnly:true,
								hidden:true,
								anchor: '99%'
							},
							{
								xtype: 'textfield',
								fieldLabel:'Unit  ',
								name: 'txtKdUnitLab',
								id: 'txtKdUnitLab',
								readOnly:true,
								hidden:true,
								anchor: '99%'
							},
							{
								xtype: 'textfield',
								name: 'txtKdUrutMasuk',
								id: 'txtKdUrutMasuk',
								readOnly:true,
								hidden:true,
								anchor: '100%'
							}
								
							//-------------------------------------------------------------
							
						]
					},	//akhir panel biodata pasien
			
		]
	};
    return items;
};


function Datasave_PenJasLab(mBol) 
{	
	if (ValidasiEntryPenJasLab(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		 (
			{
				url: baseURL + "index.php/main/functionLAB/savedetaillab",
				params: getParamDetailTransaksiLAB(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorPenJasLab('Error! Gagal menyimpan data ini. Hubungi Admin', 'Error');
					ViewGridBawahPenjasLab(TrPenJasLab.form.ComboBox.notransaksi.getValue());
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoPenJasLab("Berhasil menyimpan data ini","Information");
						TrPenJasLab.form.ComboBox.notransaksi.setValue(cst.notrans);
						TrPenJasLab.form.ComboBox.kdpasien.setValue(cst.kdPasien);
						if(cst.kdPasien.substring(0, 1) ==='L'){
							Ext.getCmp('txtNamaUnitLab').setValue('Laboratorium');
							Ext.getCmp('txtDokterPengirimL').setValue('-');
						}
						Ext.getCmp('cboDOKTER_viPenJasLab').disable();
						Ext.getCmp('btnLookupItemPemeriksaan').disable();
						Ext.getCmp('btnHpsBrsItemLabL').disable();
						Ext.getCmp('btnSimpanPenJasLab').disable();
						ViewGridBawahPenjasLab(TrPenJasLab.form.ComboBox.notransaksi.getValue());
						//RefreshDataFilterPenJasRad();
						if(mBol === false)
						{
							loadMask.hide();
							ViewGridBawahPenjasLab(TrPenJasLab.form.ComboBox.notransaksi.getValue());
						};
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorPenJasLab('Gagal menyimpan data ini. Hubungi Admin', 'Error');
					};
				}
			}
		)
		
	}
	else
	{
		if(mBol === true)
		{
			loadMask.hide();
			return false;
		};
	}; 
	
};


function getDokterPengirim(no_transaksi,kd_kasir){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/main/functionLAB/getDokterPengirim",
			params:{no_transaksi:no_transaksi,kd_kasir:kd_kasir} ,
			failure: function(o)
			{
				ShowPesanErrorPenJasLab('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					Ext.getCmp('txtDokterPengirimL').setValue(cst.nama);
				}
				else 
				{
					ShowPesanErrorPenJasLab('Gagal membaca dokter pengirim', 'Error');
				};
			}
		}
		
	)
}


function ValidasiEntryPenJasLab(modul,mBolHapus)
{
	var x = 1;
	
	if((TrPenJasLab.form.ComboBox.pasien.getValue() == '') || (Ext.get('cboDOKTER_viPenJasLab').getValue() == '') || (Ext.get('dtpKunjunganL').getValue() == '') || 
		(dsTRDetailPenJasLabList.getCount() === 0 ) || (Ext.get('cboJKPenjasLab').getValue() == '') || (Ext.get('cboGDRPenJasLab').getValue() == ''))
	{
		if (TrPenJasLab.form.ComboBox.pasien.getValue() == '') 
		{
			loadMask.hide();
			ShowPesanWarningPenJasLab('Nama Pasien tidak boleh kosong', 'Warning');
			x = 0;
		}
		else if (Ext.get('dtpKunjunganL').getValue() == '') 
		{
			loadMask.hide();
			ShowPesanWarningPenJasLab('Tanggal kunjungan tidak boleh kosong', 'Warning');
			x = 0;
		}
		else if (Ext.getCmp('cboDOKTER_viPenJasLab').getValue() == '' || Ext.getCmp('cboDOKTER_viPenJasLab').getValue()  === '') 
		{
			loadMask.hide();
			ShowPesanWarningPenJasLab('Dokter tidak boleh kosong', 'Warning');
			x = 0;
		}
		else if (dsTRDetailPenJasLabList.getCount() === 0) 
		{
			loadMask.hide();
			ShowPesanWarningPenJasLab('Item pemeriksaan tidak boleh kosong','Warning');
			x = 0;
		} else if(Ext.get('cboJKPenjasLab').getValue() == ''){
			loadMask.hide();
			ShowPesanWarningPenJasLab('Jenis kelamin tidak boleh kosong','Warning');
			x = 0;
		} else if(Ext.get('cboGDRPenJasLab').getValue() == ''){
			loadMask.hide();
			ShowPesanWarningPenJasLab('Golongan darah tidak boleh kosong','Warning');
			x = 0;
		}
	};
	if(Ext.get('cboKelPasienLab').getValue() == '' &&  Ext.get('txtCustomerLamaHide').getValue() == ''){
		loadMask.hide();
		ShowPesanWarningPenJasLab('Jenis kelompok pasien tidak boleh kosong','Warning');
		x = 0;
	}
	if(Ext.getCmp('txtKdCustomerLamaHide').getValue() == '' && Ext.get('cboPerseoranganLab').getValue() == '' && Ext.get('cboPerusahaanRequestEntryLab').getValue() == ''&& Ext.get('cboAsuransiLab').getValue() == ''){
		loadMask.hide();
		ShowPesanWarningPenJasLab('Kelompok pasien tidak boleh kosong','Warning');
		x = 0;
	}
	return x;
};


function ShowPesanWarningPenJasLab(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorPenJasLab(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoPenJasLab(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};


//combo jenis kelamin
function mComboJK()
{
    var cboJKPenjasLab = new Ext.form.ComboBox
	(
		{
			id:'cboJKPenjasLab',
			x: 495,
            y: 100,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			tabIndex:3,
			selectOnFocus:true,
			forceSelection: true,
			fieldLabel: 'Jenis Kelamin ',
			editable: false,
			width:95,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[1, 'Laki - Laki'], [2, 'Perempuan']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			//value:selectSetJKLab,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetJKLab=b.data.valueField ;
				},
			}
		}
	);
	return cboJKPenjasLab;
};

//combo golongan darah
function mComboGDarah()
{
    var cboGDRPenJasLab = new Ext.form.ComboBox
	(
		{
			id:'cboGDRPenJasLab',
			x: 690,
            y: 100,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			tabIndex:3,
			selectOnFocus:true,
			forceSelection: true,
			fieldLabel: 'Gol. Darah ',
			width:50,
			editable: false,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[0, '-'], [1, 'A+'],[2, 'B+'], [3, 'AB+'],[4, 'O+'], [5, 'A-'], [6, 'B-']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			//value:selectSetGDarahLab,
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetGDarahLab=b.data.displayText ;
				},
				
			}
		}
	);
	return cboGDRPenJasLab;
};

//Combo Dokter Lookup
function mComboDOKTER()
{ 
    var Field = ['KD_DOKTER','NAMA'];

    dsdokter_viPenJasLab = new WebApp.DataStore({ fields: Field });
    dsdokter_viPenJasLab.load
	(
            {
                params:
                    {
                        Skip: 0,
                        Take: 1000,

                        Sort: 'kd_dokter',
                        Sortdir: 'ASC',
                        target: 'ViewDokterPenunjang',
                        param: "kd_unit = '41'"
                    }
            }
	);
	
    var cboDOKTER_viPenJasLab = new Ext.form.ComboBox
	(
		{
			id: 'cboDOKTER_viPenJasLab',
			x: 130,
			y: 130,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			emptyText: '',
			fieldLabel:  ' ',
			align: 'Right',
			width: 230,
			store: dsdokter_viPenJasLab,
			valueField: 'KD_DOKTER',
			displayField: 'NAMA',
			editable: true,
			listeners:
			{
				'select': function(a, b, c)
				{
					Ext.getCmp('btnLookupItemPemeriksaan').enable();
				}
			}
		}
	);
	
    return cboDOKTER_viPenJasLab;
};

function mCboKelompokpasien(){  
 var cboKelPasienLab = new Ext.form.ComboBox
	(
		{
			
			id:'cboKelPasienLab',
			fieldLabel: 'Kelompok Pasien',
			x: 130,
			y: 160,
			mode: 'local',
			width: 130,
			//anchor: '95%',
			forceSelection: true,
			lazyRender:true,
			triggerAction: 'all',
			editable: false,
			store: new Ext.data.ArrayStore
			(
				{
				id: 0,
				fields:
				[
					'Id',
					'displayText'
				],
				   data: [[1, 'Perseorangan'], [2, 'Perusahaan'], [3, 'Asuransi']]
				}
			),			
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetKelPasienLab,
			selectOnFocus: true,
			tabIndex:22,
			listeners:
			{
				'select': function(a, b, c)
					{
					   Combo_Select(b.data.displayText);
					   if(b.data.displayText == 'Perseorangan')
					   {
						  
					   }
					   else if(b.data.displayText == 'Perusahaan')
					   {
						   
					   }
					   else if(b.data.displayText == 'Asuransi')
					   {
						   
					   }
					},
				'render': function(a,b,c)
				{
					Ext.getCmp('txtNamaPesertaAsuransiLab').hide();
					Ext.getCmp('txtNoAskesLab').hide();
					Ext.getCmp('txtNoSJPLab').hide();
					Ext.getCmp('cboPerseoranganLab').hide();
					Ext.getCmp('cboAsuransiLab').hide();
					Ext.getCmp('cboPerusahaanRequestEntryLab').hide();
					
				}
			}

		}
	);
	return cboKelPasienLab;
};

function mComboPerseorangan()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPeroranganLabRequestEntry = new WebApp.DataStore({fields: Field});
    dsPeroranganLabRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboKontrakCustomer',
			    param: 'customer.status=true and kontraktor.jenis_cust=0 order by customer.customer'
			}
		}
	)
    var cboPerseoranganLab = new Ext.form.ComboBox
	(
		{
			id:'cboPerseoranganLab',
			x: 280,
			y: 160,
			editable: false,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			fieldLabel: 'Jenis',
			tabIndex:23,
			width:150,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			store:dsPeroranganLabRequestEntry,
			value:selectSetPerseoranganLab,
			listeners:
			{
				'select': function(a,b,c)
				{
				    selectSetPerseoranganLab=b.data.displayText ;
				}
			}
		}
	);
	return cboPerseoranganLab;
};

function mComboPerusahaan()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
				Sort: '',
			    Sortdir: 'ASC',
			    target: 'ViewComboKontrakCustomer',
			    param: 'customer.status=true and kontraktor.jenis_cust=1 order by customer.customer'
			}
		}
	)
    var cboPerusahaanRequestEntryLab = new Ext.form.ComboBox
	(
		{
		    id: 'cboPerusahaanRequestEntryLab',
			x: 280,
			y: 160,
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    fieldLabel: 'Perusahaan',
		    align: 'Right',
			width:150,
		    store: dsPerusahaanRequestEntry,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
		    listeners:
			{
			    'select': function(a,b,c)
				{
					selectSetPerusahaanLab=b.data.valueField ;
				}
			}
		}
	);

    return cboPerusahaanRequestEntryLab;
};

function mComboAsuransi()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: "customer.status=true and kontraktor.jenis_cust=2 order by customer.customer"
            }
        }
    )
    var cboAsuransiLab = new Ext.form.ComboBox
	(
		{
			id:'cboAsuransiLab',
			x: 280,
			y: 160,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			fieldLabel: 'Asuransi',
			align: 'Right',
			width:150,
			//anchor: '95%',
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetAsuransiLab=b.data.valueField ;
				}
			}
		}
	);
	return cboAsuransiLab;
};


function Combo_Select(combo)
{
   var value = combo

   if(value == "Perseorangan")
   {    
        Ext.getCmp('txtNamaPesertaAsuransiLab').hide()
        Ext.getCmp('txtNoAskesLab').hide()
        Ext.getCmp('txtNoSJPLab').hide()
        Ext.getCmp('cboPerseoranganLab').show()
        Ext.getCmp('cboAsuransiLab').hide()
        Ext.getCmp('cboPerusahaanRequestEntryLab').hide()
        

   }
   else if(value == "Perusahaan")
   {    
        Ext.getCmp('txtNamaPesertaAsuransiLab').hide()
        Ext.getCmp('txtNoAskesLab').hide()
        Ext.getCmp('txtNoSJPLab').hide()
        Ext.getCmp('cboPerseoranganLab').hide()
        Ext.getCmp('cboAsuransiLab').hide()
        Ext.getCmp('cboPerusahaanRequestEntryLab').show()
        
   }
   else
       {
         Ext.getCmp('txtNamaPesertaAsuransiLab').show()
         Ext.getCmp('txtNoAskesLab').show()
         Ext.getCmp('txtNoSJPLab').show()
         Ext.getCmp('cboPerseoranganLab').hide()
         Ext.getCmp('cboAsuransiLab').show()
         Ext.getCmp('cboPerusahaanRequestEntryLab').hide()
         
       }
}



function addNewData(){
	TmpNotransaksi='';
	KdKasirAsal='';
	TglTransaksiAsal='';
	Kd_Spesial='';
	vkode_customer = '';
	No_Kamar='';
	TrPenJasLab.vars.kd_tarif='';
	
	TrPenJasLab.form.ComboBox.kdpasien.setValue('');
	Ext.getCmp('txtDokterPengirimL').setValue('');
	TrPenJasLab.form.ComboBox.notransaksi.setValue('');
	Ext.getCmp('txtNamaUnitLab').setValue('');
	Ext.getCmp('txtKdUnitLab').setValue('');
	Ext.getCmp('txtKdDokter').setValue('');
	Ext.getCmp('cboDOKTER_viPenJasLab').setValue('');
	Ext.getCmp('txtAlamatL').setValue('');
	TrPenJasLab.form.ComboBox.pasien.setValue('');
	Ext.getCmp('dtpTtlL').setValue(nowTglTransaksi);
	Ext.getCmp('txtKdUrutMasuk').setValue('');
	Ext.getCmp('cboJKPenjasLab').setValue('');
	Ext.getCmp('cboGDRPenJasLab').setValue('')
	Ext.getCmp('txtKdCustomerLamaHide').setValue('');//tampung kd customer untuk transaksi selain kunjungan langsung
	Ext.getCmp('cboKelPasienLab').setValue('');
	Ext.getCmp('txtCustomerLamaHide').setValue('');
	Ext.getCmp('cboAsuransiLab').setValue('');
	Ext.getCmp('txtNamaPesertaAsuransiLab').setValue('');
	Ext.getCmp('txtNoSJPLab').setValue('');
	Ext.getCmp('txtNoAskesLab').setValue('');
	Ext.getCmp('cboPerusahaanRequestEntryLab').setValue('');
	Ext.getCmp('txtKdDokterPengirim').setValue('');
	
	Ext.getCmp('txtCustomerLamaHide').hide();
	Ext.getCmp('cboAsuransiLab').hide();
	Ext.getCmp('txtNamaPesertaAsuransiLab').hide();
	Ext.getCmp('txtNoSJPLab').hide();
	Ext.getCmp('txtNoAskesLab').hide();
	Ext.getCmp('cboPerusahaanRequestEntryLab').hide();
	Ext.getCmp('cboKelPasienLab').show();
	
	Ext.getCmp('btnLookupItemPemeriksaan').disable();
	Ext.getCmp('btnHpsBrsItemLabL').disable();
	
	TrPenJasLab.form.ComboBox.notransaksi.enable();
	Ext.getCmp('txtDokterPengirimL').enable();
	Ext.getCmp('txtNamaUnitLab').enable();
	Ext.getCmp('cboDOKTER_viPenJasLab').enable();
	TrPenJasLab.form.ComboBox.pasien.enable();
	TrPenJasLab.form.ComboBox.kdpasien.enable();
	Ext.getCmp('txtAlamatL').enable();
	Ext.getCmp('cboJKPenjasLab').enable();
	Ext.getCmp('cboGDRPenJasLab').enable();
	Ext.getCmp('cboKelPasienLab').enable();
	Ext.getCmp('dtpTtlL').enable();
	Ext.getCmp('dtpKunjunganL').enable();
	
	TrPenJasLab.form.ComboBox.notransaksi.setReadOnly(false);
	Ext.getCmp('txtNamaUnitLab').setReadOnly(true);
	Ext.getCmp('cboDOKTER_viPenJasLab').setReadOnly(false);
	TrPenJasLab.form.ComboBox.pasien.setReadOnly(false);
	Ext.getCmp('txtAlamatL').setReadOnly(false);
	Ext.getCmp('cboJKPenjasLab').setReadOnly(false);
	Ext.getCmp('cboGDRPenJasLab').setReadOnly(false);
	Ext.getCmp('cboKelPasienLab').setReadOnly(false);
	Ext.getCmp('dtpTtlL').setReadOnly(false);
	//Ext.getCmp('dtpKunjunganL').setReadOnly(true);
	
	ViewGridBawahLookupPenjasLab('');
	ViewGridBawahPenjasLab('');
}
