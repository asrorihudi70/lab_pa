var CurrentPenJasLab =
{
    data: Object,
    details: Array,
    row: 0
};

var mRecordLAB = Ext.data.Record.create
(
    [
       {name: 'DESKRIPSI2', mapping:'DESKRIPSI2'},
       {name: 'KD_PRODUK', mapping:'KD_PRODUK'},
       {name: 'DESKRIPSI', mapping:'DESKRIPSI'},
       {name: 'KD_TARIF', mapping:'KD_TARIF'},
       {name: 'HARGA', mapping:'HARGA'},
       {name: 'QTY', mapping:'QTY'},
       {name: 'TGL_TRANSAKSI', mapping:'TGL_TRANSAKSI'},
       {name: 'DESC_REQ', mapping:'DESC_REQ'},
       {name: 'URUT', mapping:'URUT'}
    ]
);
var mRecordLAB = Ext.data.Record.create
(
    [
       {name: 'cito', mapping:'cito'},
       {name: 'kd_tarif', mapping:'kd_tarif'},
       {name: 'kp_produk', mapping:'kp_produk'},
       {name: 'deskripsi', mapping:'deskripsi'},
       {name: 'tgl_transaksi', mapping:'tgl_transaksi'},
       {name: 'tgl_berlaku', mapping:'tgl_berlaku'},
       {name: 'harga', mapping:'harga'},
       {name: 'qty', mapping:'qty'},
       {name: 'jumlah', mapping:'jumlah'}
    ]
);
var tombol_bayar='enable';
var tmpkd_unit = '';
var tmpnama_unit = '';
var KasirLABDataStoreProduk=new Ext.data.ArrayStore({id: 0,fields:['kd_produk','deskripsi','harga','tglberlaku'],data: []});
var Trkdunit2;
var dsunitlab_viPenJasLab;
var dsprinter_kasirlab;
var combovaluesunittujuan = "";
var panelActiveDataPasien;
var ds_customer_viPJ_LAB;
var jeniscus_LAB;
var dsDokterRequestEntry;
var FormLookUpsdetailTRPenjasLAB;
var CurrentHistory =
        {
            data: Object,
            details: Array,
            row: 0
        };
var kodeunit;
var FormLookUpsdetailTRKelompokPasien_lab
var tmp_kodeunitkamar_LAB;
var	tmp_kdspesial_LAB;
var tmp_nokamar_LAB;
var tmplunas;
var dsTRDetailHistoryList_lab;
var kodepasien;
var namapasien;
var namaunit;
var tmpkddokterpengirim;
var kodepay ;
var kdkasir;
var uraianpay;
var vCustomer;
var vCo_status;
var vNoSEP;
var vKd_Pasien;
var vTanggal;
var vKdUnit;
var vKdUnitDulu = "";
var vKdDokter;
var vNamaUnit;
var vNamaDokter;
var vKdKasir;
var vUrutMasuk;
var vNoTransaksi;
var mRecordKasirLAB = Ext.data.Record.create
        (
                [
                    {name: 'KD_PRODUK', mapping: 'KD_PRODUK'},
                    {name: 'DESKRIPSI2', mapping: 'DESKRIPSI2'},
                    {name: 'DESKRIPSI', mapping: 'DESKRIPSI'},
                    {name: 'KD_TARIF', mapping: 'KD_TARIF'},
                    {name: 'HARGA', mapping: 'HARGA'},
                    {name: 'QTY', mapping: 'QTY'},
                    {name: 'TGL_TRANSAKSI', mapping: 'TGL_TRANSAKSI'},
                    {name: 'DESC_REQ', mapping: 'DESC_REQ'},
                    {name: 'URUT', mapping: 'URUT'}
                ]
                );

var AddNewKasirLABKasir = true;
var dsTRDetailDiagnosaList;
var AddNewDiagnosa = true;
var kdpaytransfer = 'T1';
var kdkasir;
var tanggaltransaksitampung;
var kdkasirasal_lab;
var notransaksiasal_lab;
var kdcustomeraa;
var notransaksi;
var vflag;
var tmp_NoTransaksi;
var selectCountDiagnosa = 50;
var now = new Date();
var rowSelectedDiagnosa;
var tampungtypedata;
var jenispay;
var tapungkd_pay;
var tranfertujuan = 'Rawat Inap';
var cellSelecteddeskripsi;
var vkd_unit;
var notransaksi;
var noTransaksiPilihan;
var FormLookUpsdetailTRDiagnosa;
var valueStatusCMDiagnosaView='All';
var tglTransaksiAwalPembandingTransfer;
var nowTglTransaksi = new Date();
var tigaharilalu = new Date().add(Date.DAY, -5);
var nowTglTransaksiGrid = new Date();
var tglGridBawah = nowTglTransaksiGrid.format("d/M/Y");
var gridDTItemTest;

var tmphariini = nowTglTransaksiGrid.format("d/M/Y");
var tmp3harilalu = tigaharilalu.format("d/M/Y");

var selectSetDr;
var selectSetGDarah;
var selectSetJK;
var selectSetKelPasien;
var selectSetPerseorangan;
var selectSetPerusahaan;
var selectSetAsuransi;

var labelisi;
var jeniscus;
var variablehistori;
var selectCountStatusByr_viKasirLAB='Belum Posting';
var selectCountStatusLunasByr_viKasirLAB='Belum Lunas';
var selectCountJenTr_viPenJasLab='Transaksi Baru';
var dsTRPenJasLabList;
var dsTRDetailPenJasLabList;
var AddNewPenJasRad = true;
var selectCountPenJasRad = 50;
var TmpNotransaksi='';
var KdKasirAsal='';
var TglTransaksi='';
var databaru = 0;
var No_Kamar='';
var Kd_Spesial=0;
var kodeUnitLab;
var dsPeroranganLabRequestEntry;

var jenisKelaminPasien;
var rowSelectedPenJasLab;
var cellSelecteddeskripsi;
var FormLookUpsdetailTRLAB;
var valueStatusCMRWJView='All';
var nowTglTransaksi = new Date();
var KelompokPasienAddNew=true;
var kelompokPasien;
var tmpparams = '';
var grListPenJasLab;
//-----------------ABULHASSAN------------20_10_2017------ variable Lis
var namaCustomer_LIS='';
var namaKelompokPasien_LIS='';
//VALIDASI JENIS TRANSAKSI
var combovalues = 'Transaksi Baru';
var radiovaluesLAB = '1';
var combovaluesunit = "";
var ComboValuesKamar = "";
var dsDokterRequestEntryLAB;
var FormLookUpsdetailTRGantiDokter_lab;
/* ------------------------------- END --------------------------- */
var Field = ['KD_DOKTER','NAMA'];
    dsDokterRequestEntryLAB = new WebApp.DataStore({fields: Field});
var vkode_customer;
CurrentPage.page = getPanelPenJasLab(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
var tmpurut;


//membuat form
function getUnitDefault(){
	Ext.Ajax.request({
		url: baseURL + "index.php/main/functionLAB/getUnitDefault",
		params: {text:''},
		failure: function (o){
			loadMask.hide();
			ShowPesanErrorPenJasLab('Gagal mendapatkan data Unit default', 'Laboratorium');
		},
		success: function (o){
			var cst = Ext.decode(o.responseText);
			if (cst.success === true){
				tmpkd_unit=cst.kd_unit;
				tmpnama_unit=cst.nama_unit;
				getComboDokterLab(cst.kd_unit);
				Ext.getCmp('cboUnitLab_viPenJasLab').setValue(tmpnama_unit);
			} else{
				ShowPesanErrorPenJasLab('Gagal mendapatkan data Unit default', 'Laboratorium');
			}
		}
	});
}
function getPanelPenJasLab(mod_id) 
{
    var Field = ['KD_PASIEN','NO_TRANSAKSI','NAMA','ALAMAT','SPESIALISASI','KELAS','NAMA_KAMAR','KAMAR','MASUK','NO_TRANSAKSI_ASAL','KD_KASIR_ASAL',
				 'DOKTER','KD_DOKTER','KD_UNIT_KAMAR','KD_CUSTOMER','CUSTOMER','TGL_MASUK','URUT_MASUK','TGL_INAP','KD_SPESIAL','KD_KASIR','TGL_TRANSAKSI','KD_UNIT_ASAL','KD_UNIT',
				 'CO_STATUS','KD_USER','TGL_LAHIR','JENIS_KELAMIN','GOL_DARAH','POSTING_TRANSAKSI', 'TELEPON',
				 'NAMA_UNIT','POLIKLINIK','NAMA_UNIT_ASLI','KELPASIEN','LUNAS','DOKTER_ASAL','KD_DOKTER_ASAL','URUT','NAMA_UNIT_ASAL','HP','SJP'];
    dsTRPenJasLabList = new WebApp.DataStore({ fields: Field });


    var k="tr.tgl_transaksi >='"+tmp3harilalu+"' and tr.tgl_transaksi <='"+tmphariini+"' and left(u.kd_unit,1) IN ('3') ORDER BY tr.tgl_transaksi desc, tr.no_transaksi desc  limit 10";
	//getUnitDefault();
    refeshpenjaslab(k);
    grListPenJasLab = new Ext.grid.EditorGridPanel
    (
        {
            stripeRows: true,
			id:'PenjasLaboratorium',
            store: dsTRPenJasLabList,
            anchor: '100% 50%',
            columnLines: false,
            autoScroll:true,
            border: false,
			sort :false,
            sm: new Ext.grid.RowSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        rowselect: function(sm, row, rec)
                        {
                            rowSelectedPenJasLab = dsTRPenJasLabList.getAt(row);
							console.log(rowSelectedPenJasLab);
							vCustomer=rowSelectedPenJasLab.data.CUSTOMER;
							vNoSEP=rowSelectedPenJasLab.data.SJP;
							vKdUnit=rowSelectedPenJasLab.data.KD_UNIT;
							vKdDokter=rowSelectedPenJasLab.data.KD_DOKTER;
							vUrutMasuk=rowSelectedPenJasLab.data.URUT_MASUK;
							vTanggal=rowSelectedPenJasLab.data.MASUK;
							vKd_Pasien=rowSelectedPenJasLab.data.KD_PASIEN;
							vNamaDokter=rowSelectedPenJasLab.data.DOKTER;
							vNoTransaksi=rowSelectedPenJasLab.data.NO_TRANSAKSI;
							vKdUnitDulu=vKdUnit;
							if (rowSelectedPenJasLab.data.NAMA_UNIT_ASAL === '')
							{
								vNamaUnit=rowSelectedPenJasLab.data.NAMA_UNIT;
							}else
							{
								vNamaUnit=rowSelectedPenJasLab.data.NAMA_UNIT_ASAL;
							}
							
							//-------22-02-2017
							if(Ext.get('cboJenisTr_viPenJasLab').getValue()=='Transaksi Baru'){
								Ext.getCmp('btnHapusKunjunganLAB').disable();
								Ext.getCmp('btnGantiKekompokPasienLAB').disable();
								Ext.getCmp('btnGantiDokterLAB').disable();
							}
							else
							{
								
								//alert(vKdUnit);
								if (rowSelectedPenJasLab.data.LUNAS==='t' || rowSelectedPenJasLab.data.LUNAS==='true' || rowSelectedPenJasLab.data.LUNAS===true){
									Ext.getCmp('btnGantiKekompokPasienLAB').disable();
									Ext.getCmp('btnGantiDokterLAB').disable();
									Ext.getCmp('btnHapusKunjunganLAB').disable();
								}else{
									Ext.getCmp('btnGantiKekompokPasienLAB').enable();
									Ext.getCmp('btnGantiDokterLAB').enable();
									Ext.getCmp('btnHapusKunjunganLAB').enable();
								}
								
							}
							
                        }
                    }
                }
            ),
            listeners:
            {
                rowdblclick: function (sm, ridx, cidx)
                {
                    rowSelectedPenJasLab = dsTRPenJasLabList.getAt(ridx);
					noTransaksiPilihan = rowSelectedPenJasLab.data.NO_TRANSAKSI;
                    if (rowSelectedPenJasLab !== undefined)
                    {
                        PenJasLabLookUp(rowSelectedPenJasLab.data);
                    }
                    else
                    {
                        PenJasLabLookUp();
						Ext.getCmp('cboDOKTER_viPenJasLab').disable();
						Ext.getCmp('cboGDRLAB').disable();
						Ext.getCmp('cboJKLAB').disable();
                    }
                }
            },
        cm: new Ext.grid.ColumnModel
            (
                [
                   
                    {
                        id: 'colReqIdViewLAB',
                        header: 'No. Transaksi',
                        dataIndex: 'NO_TRANSAKSI',
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        width: 80
                    },
                    {
                        id: 'colTglRWJViewLAB',
                        header: 'Tgl Transaksi',
                        dataIndex: 'TGL_TRANSAKSI',
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        width: 75,
                            renderer: function(v, params, record)
                            {
                                    return ShowDate(record.data.TGL_TRANSAKSI);

							}
                    },
					{
                        header: 'No. Medrec',
                        width: 65,
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        dataIndex: 'KD_PASIEN',
                        id: 'colNoMedrecViewLAB'
                    },
					{
                        header: 'Pasien',
                        width: 190,
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        dataIndex: 'NAMA',
                        id: 'colNamaerViewLAB'
                    },
                    {
                        id: 'colLocationViewLAB',
                        header: 'Alamat',
                        dataIndex: 'ALAMAT',
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        width: 170
                    },
                    
                    {
                        id: 'colDeptViewLAB',
                        header: 'Dokter',
                        dataIndex: 'DOKTER',
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        width: 150
                    },
                    {
                        id: 'colKdUnitAsalViewLAB',
                        header: 'KD UNIT',
                        dataIndex: 'KD_UNIT_ASAL',
                        sortable: false,
                        hideable:false,
						hidden:true,
                        menuDisabled:true,
                        width: 90
                    },{
                        id: 'colImpactViewLAB',
                        header: 'Unit',
                        dataIndex: 'NAMA_UNIT_ASLI',
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        width: 90
                    },
					{
                        header: 'Status Transaksi',
                        width: 100,
                        sortable: false,
                        hideable:true,
                        hidden:true,
                        menuDisabled:true,
                        dataIndex: 'POSTING_TRANSAKSI',
                        id: 'txtpostingLAB',
						renderer: function(value, metaData, record, rowIndex, colIndex, store)
                        {
                             switch (value)
                             {
                                 case 't':
                                         metaData.css = 'StatusHijau'; // 
                                         break;
                                 case 'f':
                                        metaData.css = 'StatusMerah'; // rejected

                                         break;
                             }
                             return '';
                        }
						
                    },
					{
                        id: 'colCoSTatusViewLAB',
                        header: 'Status Lunas',
                        dataIndex: 'LUNAS',
                        sortable: false,
                        hideable:false,
                        menuDisabled:true,
                        width: 80,
						renderer: function(value, metaData, record, rowIndex, colIndex, store)
                        {
                             switch (value)
                             {
                                 case 't':
                                         metaData.css = 'StatusHijau'; // 
                                         break;
                                 case 'f':
                                        metaData.css = 'StatusMerah'; // rejected

                                         break;
								case '':
                                        metaData.css = 'StatusMerah'; // rejected

                                         break;
								case undefined:
                                        metaData.css = 'StatusMerah'; // rejected

                                         break;
                             }
                             return '';
                        }
                    }
                   
                ]
            ),
            viewConfig: {forceFit: true},
            tbar:
                [
                    {
                        id: 'btnEditLAB',
                        text: 'Lookup',
                        tooltip: nmEditData,
                        iconCls: 'Edit_Tr',
                        handler: function(sm, row, rec)
                        {
                            if (rowSelectedPenJasLab != undefined)
                            {
								console.log(rowSelectedPenJasLab.data);
								
                                    PenJasLabLookUp(rowSelectedPenJasLab.data);
                            }
                            else
                            {
							ShowPesanWarningPenJasLab('Pilih data tabel  ','Edit data');
                                    //alert('');
                            }
                        }
                    },{
                        id: 'btnHapusKunjunganLAB',
                        text: 'Batal Transaksi',
                        tooltip: nmEditData,
                        iconCls: 'Remove',
						disabled: true,
                        handler: function(sm, row, rec)
                        {
							//alert();
                            if (rowSelectedPenJasLab != undefined)
                            {
								Ext.MessageBox.confirm('Hapus Kunjungan', "Yakin Akan Hapus Kunjungan ini ?", function (btn){
									if (btn === 'yes') {
										console.log(rowSelectedPenJasLab);
										var params = {
											kd_unit: rowSelectedPenJasLab.data.KD_UNIT,
											Tglkunjungan: rowSelectedPenJasLab.data.MASUK,
											Kodepasein: rowSelectedPenJasLab.data.KD_PASIEN,
											urut: rowSelectedPenJasLab.data.URUT_MASUK
										};
										Ext.Ajax.request({
											url: baseURL + "index.php/main/functionLAB/deletekunjungan",
											params: params,
											failure: function (o){
												loadMask.hide();
												ShowPesanInfoPenJasLab('Data transaksi pasien berhasil di hapus', 'Hapus Data Kunjungan');
												validasiJenisTrLAB();
											},
											success: function (o){
												var cst = Ext.decode(o.responseText);
												if (cst.success === true && cst.cari_trans === true && cst.cari_bayar === false){
													ShowPesanWarningPenJasLab('Data transaksi tidak berhasil ditemukan', 'Hapus Data Kunjungan');
												} else if (cst.success === true && cst.cari_trans === true && cst.cari_bayar === true){
													ShowPesanWarningPenJasLab('Anda telah melakukan pembayaran', 'Hapus Data Kunjungan');
												} else if (cst.success === true){
													validasiJenisTrLAB();
													ShowPesanInfoPenJasLab('Data transaksi pasien berhasil di hapus', 'Hapus Data Kunjungan');
												} else if (cst.success === false && cst.pesan === 0){
													ShowPesanWarningPenJasLab('Data kunjungan pasien tidak berhasil di hapus', 'Hapus Data Kunjungan');
												} else{
													ShowPesanErrorPenJasLab('Data kunjungan pasien tidak berhasil di hapus', 'Hapus Data Kunjungan');
												}
											}
										}); 
									}
								});
                            }
                            else
                            {
							ShowPesanWarningPenJasLab('Pilih data tabel  ','Edit data');
                                    //alert('');
                            }
                        }
                    },{								
						id: 'btnGantiKekompokPasienLAB', text: 'Ganti kelompok pasien', tooltip: 'Ganti kelompok pasien', iconCls: 'gantipasien', disabled:true,
						handler: function () {
							//Button Ganti Kelompok;
							panelActiveDataPasien = 0;
							KelompokPasienLookUp_lab();
						}
					},{								
						id: 'btnGantiDokterLAB', text: 'Ganti Dokter', tooltip: 'Ganti dokter pasien', iconCls: 'gantipasien', disabled:true,
						handler: function () {
							//Button Ganti Dokter;
							panelActiveDataPasien = 1;
							loaddatastoredokterLAB();
							GantiDokterPasienLookUp_lab();
						}
					},
                ]
            }
	);
	
	
	//form depan awal dan judul tab
    var FormDepanPenJasLab = new Ext.Panel
    (
        {
            id: mod_id,
            closable: true,
            region: 'center',
            layout: 'form',
            title: 'Penata Jasa Laboratorium',
            border: false,
            shadhow: true,
            autoScroll:false,
            iconCls: 'Request',
            margins: '0 5 5 0',
            items: [
                        getItemPanelPenJasRad(),
                        grListPenJasLab
                   ],
            listeners:
            {
                'afterrender': function()
                {
					Ext.getCmp('cboRujukanKamarSpesialJasRadRequestEntry').hide();
					Ext.getCmp('cboStatusLunas_viPenJasLab').disable();
				}
            }
        }
    );
	
   return FormDepanPenJasLab;

};
/* 
	PERBARUAN GANTI DOKTER  
	OLEH 	: ADIT
	TANGGAL : 2017 - 02 - 24
*/
/* =============================================================================================================================================== */
function KelompokPasienLookUp_lab(rowdata) 
{
    var lebar = 440;
    FormLookUpsdetailTRKelompokPasien_lab = new Ext.Window
    (
        {
            id: 'gridKelompokPasien',
            title: 'Ganti Kelompok Pasien',
            closeAction: 'destroy',
            width: lebar,
            height: 260,
            border: false,
            resizable: false,
            plain: false,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryTRKelompokPasien_lab(lebar),
            listeners:
            {
                
            }
        }
    );

    FormLookUpsdetailTRKelompokPasien_lab.show();
    KelompokPasienbaru_lab();

};

function KelompokPasienbaru_lab() 
{
	jeniscus_LAB=0;
    KelompokPasienAddNew_LAB = true;
    Ext.getCmp('cboKelompokpasien_LAB').show()
	Ext.getCmp('txtCustomer_labLama').disable();
	Ext.get('txtCustomer_labLama').dom.value=vCustomer;
	Ext.get('txtLABNoSEP').dom.value = vNoSEP;
	
	RefreshDatacombo_lab(jeniscus_LAB);
};

function getFormEntryTRKelompokPasien_lab(lebar) 
{
    var pnlTRKelompokPasien_lab = new Ext.FormPanel
    (
        {
            id: 'PanelTRKelompokPasien_lab',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:250,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [
					getItemPanelInputKelompokPasien_lab(lebar),
					getItemPanelButtonKelompokPasien_lab(lebar)
			],
           tbar:
            [
               
               
            ]
        }
    );
 
    var FormDepanKelompokPasienLAB = new Ext.Panel
	(
		{
		    id: 'FormDepanKelompokPasienLAB',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [pnlTRKelompokPasien_lab	
				
			]

		}
	);

    return FormDepanKelompokPasienLAB
};
function getItemPanelButtonKelompokPasien_lab(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
		height:30,
		anchor:'100%',
		style:{'margin-top':'-1px'},
	    items:
		[
			{
				layout: 'hBox',
				width:400,
				border: false,
				bodyStyle: 'padding:5px 0px 5px 5px',
				defaults: { margins: '3 3 3 3' },
				anchor: '90%',
				layoutConfig: 
				{
					align: 'middle',
					pack:'end'
				},
				items:
				[
					{
						xtype:'button',
						text:'Simpan',
						width:70,
						style:{'margin-left':'0px','margin-top':'0px'},
						hideLabel:true,
						id:Nci.getId(),
						handler:function()
						{
							if(panelActiveDataPasien == 0){
								Datasave_Kelompokpasien_lab();
							}
							if(panelActiveDataPasien == 1){
								Datasave_GantiDokter_lab();
							}
							
						}
					},
					{
						xtype:'button',
						text:'Tutup',
						width:70,
						hideLabel:true,
						id:Nci.getId(),
						handler:function() 
						{
							if(panelActiveDataPasien == 0){
								FormLookUpsdetailTRKelompokPasien_lab.close();
							}
							
							if(panelActiveDataPasien == 1){
								FormLookUpsdetailTRGantiDokter_lab.close();
							}
							
						}
					}
				]
			}
		]
	}
    return items;
};
function Datasave_GantiDokter_lab(mBol) 
{	
	//console.log(Ext.get('cboDokterRequestEntry').getValue());
	if((Ext.get('cboDokterRequestEntryLAB').getValue() == '') || (Ext.get('cboDokterRequestEntryLAB').dom.value  === undefined ) || (Ext.get('cboDokterRequestEntryLAB').dom.value  === 'Pilih Dokter...'))
	{
		ShowPesanWarningPenJasLab('Dokter baru harap diisi', "Informasi");
	}else{
		Ext.Ajax.request
		(
			{
				url: baseURL +  "index.php/main/functionRWJ/UpdateGantiDokter",	
				params: getParamKelompokpasien_LAB(),
				failure: function(o)
				{
					ShowPesanWarningPenJasLab('Simpan dokter pasien gagal', 'Gagal');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
                        panelActiveDataPasien = 1;
						FormLookUpsdetailTRGantiDokter_lab.close();
						validasiJenisTrLAB();
						Ext.getCmp('btnGantiKekompokPasienLAB').disable();	
						Ext.getCmp('btnGantiDokterLAB').disable();		
						ShowPesanInfoPenJasLab("Mengganti dokter pasien berhasil", "Success");
					}else 
					{
						ShowPesanWarningPenJasLab('Simpan dokter pasien gagal', 'Gagal');
					};
				}
			}
		) 
	}
	
};
function ValidasiEntryUpdateKelompokPasien_LAB(modul,mBolHapus)
{
	var x = 1;
	
	if((Ext.get('kelPasien_LAB').getValue() == '') || (Ext.get('kelPasien_LAB').dom.value  === undefined ))
	{
		if (Ext.get('kelPasien_LAB').getValue() == '' && mBolHapus === true) 
		{
			ShowPesanWarningPenJasLab(nmGetValidasiKosong('Kelompok Pasien'), modul);
			x = 0;
		}
	};
	return x;
};

function ValidasiEntryUpdateGantiDokter_LAB(modul,mBolHapus)
{
	var x = 1;
	
	if((Ext.get('cboDokterRequestEntryLAB').getValue() == '') || (Ext.get('cboDokterRequestEntryLAB').dom.value  === undefined ))
	{
		ShowPesanWarningPenJasLab(nmGetValidasiKosong('Dokter baru harap diisi'), modul);
		x = 0;
	};
	return x;
};
function getParamKelompokpasien_LAB() 
{
	var params;
	if(panelActiveDataPasien == 0){
		params = {
			KDCustomer 	: selectKdCustomer,
			KDNoSJP  	: Ext.get('txtLABNoSEP').getValue(),
			KDNoAskes  	: Ext.get('txtLABNoAskes').getValue(),
			KdPasien  	: vKd_Pasien,
			TglMasuk  	: vTanggal,
			KdUnit  	: vKdUnit,
			UrutMasuk  	: vUrutMasuk,
			KdDokter  	: vKdDokter,
		}
	}else if(panelActiveDataPasien == 1 || panelActiveDataPasien == 2){
		params = {
			KdPasien  	: vKd_Pasien,
			TglMasuk  	: vTanggal,
			KdUnit  	: vKdUnit,
			UrutMasuk  	: vUrutMasuk,
			KdDokter  	: vKdDokter,
			NoTransaksi	: vNoTransaksi,
			KdKasir  	: vKdKasir,
			KdUnitDulu 	: vKdUnitDulu,
		}
	}else if(panelActiveDataPasien == 'undefined'){
		params = { data : "null", }
	}else{
		params = { data : "null", }
	}
    return params
};
function Datasave_Kelompokpasien_lab(mBol) 
{	
	if (ValidasiEntryUpdateKelompokPasien_LAB(nmHeaderSimpanData,false) == 1 )
	{			
			Ext.Ajax.request
			(
				{
					url: baseURL +  "index.php/main/functionRWJ/UpdateGantiKelompok",	
					params: getParamKelompokpasien_LAB(),
					failure: function(o)
					{
						ShowPesanWarningPenJasLab('Simpan kelompok pasien gagal', 'Gagal');
					},	
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
                            panelActiveDataPasien = 1;
							FormLookUpsdetailTRKelompokPasien_lab.close();
							validasiJenisTrLAB();
							Ext.getCmp('btnGantiKekompokPasienLAB').disable();	
							Ext.getCmp('btnGantiDokterLAB').disable();	
							ShowPesanInfoPenJasLab("Mengganti kelompok pasien berhasil", "Success");
						}else 
						{
                            panelActiveDataPasien = 1;
							ShowPesanWarningPenJasLab('Simpan kelompok pasien gagal', 'Gagal');
						};
					}
				}
			 )
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
	
};
function getItemPanelInputKelompokPasien_lab(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 0px',
		border:false,
		height:170,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
			    layout: 'form',
			    border: false,
			    items:
				[
					getKelompokpasienlama_lab(lebar),	
					getItemPanelNoTransksiKelompokPasien_lab(lebar)	,
					
				]
			}
		]
	};
    return items;
};
function getKelompokpasienlama_lab(lebar) 
{
    var items =
	{
		Width:lebar,
		height:40,
	    layout: 'column',
	    border: false,
		
	    items:
		[
			{
			    columnWidth: .990,
			    layout: 'form',
				Width:lebar-10,
				labelWidth:130,
			    border: true,
			    items:
				[{	 
						xtype: 'tbspacer',
						height: 2
						},
						{
							xtype: 'textfield',
							fieldLabel: 'Kelompok Pasien Asal',
							name: 'txtCustomer_labLama',
							id: 'txtCustomer_labLama',
							labelWidth:130,
							editable: false,
							width: 100,
							anchor: '95%'
						 }
					]
			}
			
		]
	}
    return items;
};
function getItemPanelNoTransksiKelompokPasien_lab(lebar) 
{
    var items =
	{
		Width:lebar,
		height:120,
	    layout: 'column',
	    border: false,
		
		
	    items:
		[
			{

			    columnWidth: .990,
			    layout: 'form',
				Width:lebar-10,
				labelWidth:130,
			    border: true,
			    items:
				[{	 
						xtype: 'tbspacer',
						height:3
					},{ 
					    xtype: 'combo',
						fieldLabel: 'Kelompok Pasien Baru',
						id: 'kelPasien_LAB',
						editable: false,
						store: new Ext.data.ArrayStore
							(
								{
								id: 0,
								fields:
								[
								'Id',
								'displayText'
								],
								   data: [[1, 'Perseorangan'], [2, 'Perusahaan'], [3, 'Asuransi']]
								}
							),
							  displayField: 'displayText',
							  mode: 'local',
							  width: 100,
							  forceSelection: true,
							  triggerAction: 'all',
							  emptyText: 'Pilih Salah Satu...',
							  selectOnFocus: true,
							  anchor: '95%',
							  listeners:
								 {
										'select': function(a, b, c)
									{
										if(b.data.displayText =='Perseorangan')
										{jeniscus_LAB='0'
											//Ext.getCmp('txtLABNoSEP').disable();
											//Ext.getCmp('txtRWJNoAskes').disable();
											}
										else if(b.data.displayText =='Perusahaan')
										{jeniscus_LAB='1';
											//Ext.getCmp('txtLABNoSEP').disable();
											//Ext.getCmp('txtRWJNoAskes').disable();
											}
										else if(b.data.displayText =='Asuransi')
										{jeniscus_LAB='2';
											//Ext.getCmp('txtLABNoSEP').enable();
											//Ext.getCmp('txtRWJNoAskes').enable();
										}
										
										RefreshDatacombo_lab(jeniscus_LAB);
									}

								}
						},{
							columnWidth: .990,
							layout: 'form',
							border: false,
							labelWidth:130,
							items:
							[
												mComboKelompokpasien_lab()
							]
						},{
							xtype: 'textfield',
							fieldLabel:'No SEP  ',
							name: 'txtLABNoSEP',
							id: 'txtLABNoSEP',
							width: 100,
							anchor: '99%'
						 }, {
							 xtype: 'textfield',
							fieldLabel:'No Asuransi  ',
							name: 'txtLABNoAskes',
							id: 'txtLABNoAskes',
							width: 100,
							anchor: '99%'
						 }
									
				]
			}
			
		]
	}
    return items;
};

function mComboKelompokpasien_lab()
{

var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viPJ_LAB = new WebApp.DataStore({fields: Field_poli_viDaftar});
	
	if (jeniscus_LAB===undefined || jeniscus_LAB==='')
	{
		jeniscus_LAB=0;
	}
	ds_customer_viPJ_LAB.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: 'jenis_cust=~'+ jeniscus_LAB +'~'
            }
        }
    )
    var cboKelompokpasien_LAB = new Ext.form.ComboBox
	(
		{
			id:'cboKelompokpasien_LAB',
			typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Pilih...',
                        fieldLabel: '',
                        align: 'Right',
                        anchor: '95%',
			store: ds_customer_viPJ_LAB,
			valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetKelompokpasien=b.data.displayText ;
					selectKdCustomer=b.data.KD_CUSTOMER;
					selectNamaCustomer=b.data.CUSTOMER;
				
				}
			}
		}
	);
	return cboKelompokpasien_LAB;
};


function RefreshDatacombo_lab(jeniscus_LAB) 
{
	var kosong;
    ds_customer_viPJ_LAB.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: 'jenis_cust=~'+ jeniscus_LAB +'~ '
            }
        }
    )
	
    return ds_customer_viPJ_LAB;
};

/* =============================================================================================================================================== */
/* 
	PERBARUAN GANTI DOKTER  
	OLEH 	: ADIT
	TANGGAL : 2017 - 02 - 24
*/
/* =============================================================================================================================================== */
function GantiDokterPasienLookUp_lab(rowdata) 
{
    var lebar = 440;
    FormLookUpsdetailTRGantiDokter_lab = new Ext.Window
    (
        {
            id: 'idGantiDokterLAB',
            title: 'Ganti Dokter',
            closeAction: 'destroy',
            width: lebar,
            height: 260,
            border: false,
            resizable: false,
            plain: false,
            layout: 'fit',
            iconCls: 'Request',
            modal: true,
            items: getFormEntryTRGantiDokter_lab(lebar),
            listeners:
            {
                
            }
        }
    );

    FormLookUpsdetailTRGantiDokter_lab.show();
    GantiPasien_lab();

};

function GantiPasien_lab() 
{
	//RefreshDatacombo_rwj(jeniscus_LAB);
};

function getFormEntryTRGantiDokter_lab(lebar) 
{
    var pnlTRKelompokPasien_lab = new Ext.FormPanel
    (
        {
            id: 'PanelTRGantiLAB',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:250,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [
					getItemPanelInputGantiDokter_lab(lebar),
					getItemPanelButtonKelompokPasien_lab(lebar)
			],
           tbar:
            [
               
               
            ]
        }
    );
 
    var FormDepanGantiDokterLAB = new Ext.Panel
	(
		{
		    id: 'FormDepanGantiDokterLAB',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: [pnlTRKelompokPasien_lab
				
			]

		}
	);

    return FormDepanGantiDokterLAB
};

function getItemPanelInputGantiDokter_lab(lebar) 
{
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:10px 10px 10px 0px',
		border:false,
		height:170,
	    items:
		[
			{
			    columnWidth: .9,
			    width: lebar -35,
				labelWidth:100,
			    layout: 'form',
			    border: false,
			    items:
				[	
					{
						xtype: 'textfield',
					    fieldLabel:  'Unit Asal ',
					    name: 'txtUnitAsal_DataPasienLAB',
					    id: 'txtUnitAsal_DataPasienLAB',
						value:vNamaUnit,
						readOnly:true,
						width: 100,
						anchor: '99%'
					},{
					    xtype: 'textfield',
					    fieldLabel: 'Dokter Asal ',
					    name: 'txtDokterAsal_DataPasienLAB',
					    id: 'txtDokterAsal_DataPasienLAB',
						value:vNamaDokter,
						readOnly:true,
						width: 100,
						anchor: '99%'
					},
					mComboDokterGantiEntryLAB()
				]
			}
		]
	};
    return items;
};

function loaddatastoredokterLAB(){
	dsDokterRequestEntryLAB.load({
         params	:{
            Skip	: 0,
		    Take	: 1000,
            Sort	: 'nama',
		    Sortdir	: 'ASC',
		    target	: 'ViewDokterPenunjang',
		    param	: 'kd_unit=~'+ vKdUnit+ '~'
		}
    });
}
function mComboDokterGantiEntryLAB(){ 
	/* var Field = ['KD_DOKTER','NAMA'];
    dsDokterRequestEntryLAB = new WebApp.DataStore({fields: Field}); */
    var cboDokterGantiEntryLAB = new Ext.form.ComboBox({
	    id: 'cboDokterRequestEntryLAB',
	    typeAhead: true,
	    triggerAction: 'all',
		name:'txtdokter',
	    lazyRender: true,
	    mode: 'local',
	    selectOnFocus:true,
        forceSelection: true,
	    emptyText:'Pilih Dokter...',
	    fieldLabel: 'Dokter Baru',
	    align: 'Right',
	    store: dsDokterRequestEntryLAB,
	    valueField: 'KD_DOKTER',
	    displayField: 'NAMA',
		anchor:'100%',
	    listeners:{
		    'select': function(a,b,c){
				vKdDokter = b.data.KD_DOKTER;
            },
		}
    });
    return cboDokterGantiEntryLAB;
};
function mComboStatusBayar_viPenJasLab()
{
  var cboStatus_viPenJasLab = new Ext.form.ComboBox
	(
		{
                    id:'cboStatus_viPenJasLab',
                    x: 155,
                    y: 70,
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender:true,
                    mode: 'local',
                    width: 110,
                    emptyText:'',
                    fieldLabel: 'JENIS',
                    store: new Ext.data.ArrayStore
                    (
                            {
                                    id: 0,
                                    fields:
                                    [
                                        'Id',
                                        'displayText'
                                    ],
                            data: [[1, 'Semua'],[2, 'Posting'], [3, 'Belum Posting']]
                            }
                    ),
                    valueField: 'Id',
                    displayField: 'displayText',
                    value:selectCountStatusByr_viKasirLAB,
                    listeners:
                    {
                            'select': function(a,b,c)
                            {
                                    selectCountStatusByr_viKasirLAB=b.data.displayText ;
                            }
                    }
		}
	);
	return cboStatus_viPenJasLab;
};
function mComboStatusLunas_viPenJasLab()
{
  var cboStatusLunas_viPenJasLab = new Ext.form.ComboBox
	(
		{
                    id:'cboStatusLunas_viPenJasLab',
                    x: 155,
                    y: 70,
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender:true,
                    mode: 'local',
                    width: 110,
                    emptyText:'',
                    fieldLabel: 'JENIS',
                    store: new Ext.data.ArrayStore
                    (
                            {
                                    id: 0,
                                    fields:
                                    [
                                        'Id',
                                        'displayText'
                                    ],
                            data: [[1, 'Semua'],[2, 'Lunas'], [3, 'Belum Lunas']]
                            }
                    ),
                    valueField: 'Id',
                    displayField: 'displayText',
                    value:selectCountStatusLunasByr_viKasirLAB,
                    listeners:
                    {
                            'select': function(a,b,c)
                            {
                                    selectCountStatusLunasByr_viKasirLAB=b.data.displayText ;
                                    validasiJenisTrLAB();
                            }
                    }
		}
	);
	return cboStatusLunas_viPenJasLab;
};

//COMBO JENIS TRANSAKSI
function mComboJenisTrans_viPenJasLab()
{
  var cboJenisTr_viPenJasLab = new Ext.form.ComboBox
	(
		{
                    id:'cboJenisTr_viPenJasLab',
                    x: 155,
                    y: 10,
                    typeAhead: true,
                    triggerAction: 'all',
                    lazyRender:true,
                    mode: 'local',
                    width: 110,
                    emptyText:'',
                    fieldLabel: 'JENIS TRANSAKSI',
                    store: new Ext.data.ArrayStore
                    (
                            {
                                    id: 0,
                                    fields:
                                    [
                                        'Id',
                                        'displayText'
                                    ],
                            data: [[1, 'Transaksi Baru'],[2, 'Transaksi Lama']]
                            }
                    ),
                    valueField: 'Id',
                    displayField: 'displayText',
                    value:selectCountJenTr_viPenJasLab,
                    listeners:
                    {
                            'select': function(a,b,c)
                            {
								combovalues=b.data.displayText;
								if(b.data.Id==1){
									Ext.getCmp('cbounitlabs_viPenJasLab').setValue(null);
									Ext.getCmp('cbounitlabs_viPenJasLab').disable();
									tmppasienbarulama = 'Baru';
								}else{
									Ext.getCmp('cbounitlabs_viPenJasLab').enable();
									Ext.getCmp('cbounitlabs_viPenJasLab').setValue(combovaluesunittujuan);
									tmppasienbarulama = 'Lama';
								}
								
                                validasiJenisTrLAB();

                            }
                    }
		}
	);
	return cboJenisTr_viPenJasLab;
};

var tmppasienbarulama = 'Baru';


function validasiJenisTrLAB(){
    var kriteria = "";
	var tmpkriteriaunittujuancrwi='';
	var strkriteria=getCriteriaFilter_viDaftar();
    if (combovalues === 'Transaksi Lama')
    {
        Ext.getCmp('cboStatusLunas_viPenJasLab').enable();
    
        if(radiovaluesLAB === '1')
        {
            if(combovaluesunit !== "")
            {
                if (combovaluesunit === "All")
                {
                  tmpkreteriaLAB = "";
                }else
                {
					tmpkreteriaLAB = "and kd_unit_asal = '" + Ext.getCmp('cboUNIT_viKasirLab').getValue() + "'";
                }
            }else {
                tmpkreteriaLAB = "";
            }
			
            if (Ext.getCmp('txtNamaPasienPenJasLab').getValue() !== "")
            {
               tmpkriterianamaLAB = " AND lower(NAMA) like lower('%" + Ext.get('txtNamaPasienPenJasLab').dom.value + "%')";
            }else
            {
                tmpkriterianamaLAB = "";
            }
			
            if (Ext.getCmp('txtNoMedrecPenJasLab').getValue() !== "")
            {
               tmpkriteriamedrecLAB = " AND kd_pasien = '"+ Ext.get('txtNoMedrecPenJasLab').dom.value + "'";
            }else
            {
                tmpkriteriamedrecLAB = "";
            }
			
			if(Ext.getCmp('cbounitlabs_viPenJasLab').getValue() != null && Ext.getCmp('cbounitlabs_viPenJasLab').getValue() != ''){
				tmpkriteriaunittujuancrwi=" AND kd_unit = '"+ Ext.getCmp('cbounitlabs_viPenJasLab').getValue() + "'";
			}else{
				tmpkriteriaunittujuancrwi="";
			}
			/* if(selectCountStatusByr_viKasirLAB !== "")
            {
                if(selectCountStatusByr_viKasirLAB === "Posting")
                {
                    tmpposting = "and tr.posting_transaksi='t'";
                }else if(selectCountStatusByr_viKasirLAB === "Belum Posting")
                {
                    tmpposting = "and tr.posting_transaksi='f'";
                }else
                {
                    tmpposting = "";
                }
            } */
			if(selectCountStatusLunasByr_viKasirLAB !== "")
            {
                if(selectCountStatusLunasByr_viKasirLAB === "Lunas")
                {
                    tmplunas = "and lunas='t'";
                }else if(selectCountStatusLunasByr_viKasirLAB === "Belum Lunas")
                {
                    tmplunas = "and lunas='f'";
                }else
                {
                    tmplunas = "";
                }
            }
			
            kriteria = " kd_bagian = 4 and kd_unit not in ('44','45') and left(kd_unit_asal, 1) in('3') "+ tmpkriteriaunittujuancrwi +""+ tmpkreteriaLAB +" "+ tmpkriterianamaLAB +" "+ tmpkriteriamedrecLAB +"  "+tmplunas+" "+strkriteria+" and left(kd_pasien, 2) not in ('LB')  and tgl_transaksi >='" + Ext.get('dtpTglAwalFilterPenJasLab').getValue() + "' and tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterPenJasLab').getValue() + "' ORDER BY tgl_transaksi desc, no_transaksi limit 10";
            tmpunit = 'ViewPenJasLab';
            loadpenjaslab(kriteria, tmpunit);
            
        }else if (radiovaluesLAB === '2'){   
            if(ComboValuesKamar !== "")
            {
                if (ComboValuesKamar === "Semua")
                {
                    tmpkriteriakamar = "";
                }else
                {
                    tmpkriteriakamar = "and kd_unit_asal = '" + Ext.getCmp('cboRujukanKamarSpesialJasRadRequestEntry').getValue() + "'";
                }
            }else
            {
                tmpkriteriakamar = "";
            }
			
            if (Ext.getCmp('txtNamaPasienPenJasLab').getValue() !== "")
            {
               tmpkriterianama = " AND lower(NAMA) like lower('%" + Ext.get('txtNamaPasienPenJasLab').dom.value + "%')";
            }else
            {
                tmpkriterianama = "";
            }
            if (Ext.getCmp('txtNoMedrecPenJasLab').getValue() !== "")
            {
               tmpkriteriamedrec = " AND kd_pasien = '"+ Ext.get('txtNoMedrecPenJasLab').dom.value + "'";
            }else
            {
                tmpkriteriamedrec = "";
            }
			if(Ext.getCmp('cbounitlabs_viPenJasLab').getValue() != null && Ext.getCmp('cbounitlabs_viPenJasLab').getValue() != ''){
				tmpkriteriaunittujuancrwi=" AND kd_unit = '"+ Ext.getCmp('cbounitlabs_viPenJasLab').getValue() + "'";
			}else{
				tmpkriteriaunittujuancrwi="";
			}
			/* 
			if(selectCountStatusByr_viKasirLAB !== "")
            {
                if(selectCountStatusByr_viKasirLAB === "Posting")
                {
                    tmpposting = "and tr.posting_transaksi='t'";
                }else if(selectCountStatusByr_viKasirLAB === "Belum Posting")
                {
                    tmpposting = "and tr.posting_transaksi='f'";
                }else
                {
                    tmpposting = "";
                }
            } */
			if(selectCountStatusLunasByr_viKasirLAB !== "")
            {
                if(selectCountStatusLunasByr_viKasirLAB === "Lunas")
                {
                    tmplunas = "and lunas='t'";
                }else if(selectCountStatusLunasByr_viKasirLAB === "Belum Lunas")
                {
                    tmplunas = "and lunas='f'";
                }else
                {
                    tmplunas = "";
                }
            }
            kriteria = " kd_bagian = 4 and kd_unit not in ('44','45') "+tmpkriteriaunittujuancrwi+" "+ tmpkriteriakamar +" "+ tmpkriterianama +" "+ tmpkriteriamedrec +" "+tmplunas+" "+strkriteria+" and left(kd_pasien, 2) not in ('LB')  and left(kd_unit_asal, 1) = '1' and tgl_transaksi >='" + Ext.get('dtpTglAwalFilterPenJasLab').getValue() + "' and tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterPenJasLab').getValue() + "' ORDER BY tgl_transaksi desc, no_transaksi limit 10";
            tmpunit = 'ViewPenJasLab';
            loadpenjaslab(kriteria, tmpunit);
        }else if (radiovaluesLAB === '3')
        {
            if (Ext.getCmp('txtNamaPasienPenJasLab').getValue() !== "")
            {
               tmpkriterianama = " AND lower(NAMA) like lower('%" + Ext.get('txtNamaPasienPenJasLab').dom.value + "%')";
            }else
            {
                tmpkriterianama = "";
            }
			
            if (Ext.getCmp('txtNoMedrecPenJasLab').getValue() !== "")
            {
               tmpkriteriamedrec = " AND kd_pasien = '"+ Ext.get('txtNoMedrecPenJasLab').dom.value + "'";
            }else
            {
                tmpkriteriamedrec = "";
            }
			
			if(Ext.getCmp('cbounitlabs_viPenJasLab').getValue() != null && Ext.getCmp('cbounitlabs_viPenJasLab').getValue() != ''){
				tmpkriteriaunittujuancrwi=" AND kd_unit = '"+ Ext.getCmp('cbounitlabs_viPenJasLab').getValue() + "'";
			}else{
				tmpkriteriaunittujuancrwi="";
			}
			/* if(selectCountStatusByr_viKasirLAB !== "")
            {
                if(selectCountStatusByr_viKasirLAB === "Posting")
                {
                    tmpposting = "and tr.posting_transaksi='t'";
                }else if(selectCountStatusByr_viKasirLAB === "Belum Posting")
                {
                    tmpposting = "and tr.posting_transaksi='f'";
                }else
                {
                    tmpposting = "";
                }
            } */
			if(selectCountStatusLunasByr_viKasirLAB !== "")
            {
                if(selectCountStatusLunasByr_viKasirLAB === "Lunas")
                {
                   tmplunas = " and lunas='t' ";
                }else if(selectCountStatusLunasByr_viKasirLAB === "Belum Lunas")
                {
                    tmplunas = " and lunas='f' ";
                }else
                {
                    tmplunas = "";
                }
            }
            kriteria = " kd_bagian = 4 and kd_unit not in ('44','45') "+ tmpkriterianama +""+ tmpkriteriamedrec +" "+ tmpkriteriaunittujuancrwi +"    and left(kd_pasien, 2) = 'LB' and tgl_transaksi >='" + Ext.get('dtpTglAwalFilterPenJasLab').getValue() + "' and tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterPenJasLab').getValue() + "' "+tmplunas+" ORDER BY tgl_transaksi desc, no_transaksi  limit 10";
            tmpunit = 'ViewPenJasLabKunjunganLangsung';
            loadpenjaslab(kriteria, tmpunit);
        
		}else if (radiovaluesLAB === '4')
        {
			if (Ext.getCmp('txtNamaPasienPenJasLab').getValue() !== "")
            {
               tmpkriterianama = " AND lower(NAMA) like lower('%" + Ext.get('txtNamaPasienPenJasLab').dom.value + "%')";
            }else
            {
                tmpkriterianama = "";
            }
			
            if (Ext.getCmp('txtNoMedrecPenJasLab').getValue() !== "")
            {
               tmpkriteriamedrec = " AND kd_pasien = '"+ Ext.get('txtNoMedrecPenJasLab').dom.value + "'";
            }else
            {
                tmpkriteriamedrec = "";
            }
			if(Ext.getCmp('cbounitlabs_viPenJasLab').getValue() != null && Ext.getCmp('cbounitlabs_viPenJasLab').getValue() != ''){
				tmpkriteriaunittujuancrwi=" AND kd_unit = '"+ Ext.getCmp('cbounitlabs_viPenJasLab').getValue() + "'";
			}else{
				tmpkriteriaunittujuancrwi="";
			}
			/* if(selectCountStatusByr_viKasirLAB !== "")
            {
                if(selectCountStatusByr_viKasirLAB === "Posting")
                {
                    tmpposting = "and tr.posting_transaksi='t'";
                }else if(selectCountStatusByr_viKasirLAB === "Belum Posting")
                {
                    tmpposting = "and tr.posting_transaksi='f'";
                }else
                {
                    tmpposting = "";
                }
            } */
			if(selectCountStatusLunasByr_viKasirLAB !== "")
            {
                if(selectCountStatusLunasByr_viKasirLAB === "Lunas")
                {
                    tmplunas = "and lunas='t'";
                }else if(selectCountStatusLunasByr_viKasirLAB === "Belum Lunas")
                {
                    tmplunas = "and lunas='f'";
                }else
                {
                    tmplunas = "";
                }
            }
			kriteria = " kd_bagian = 4 and kd_unit not in ('44','45') and left(kd_unit_asal, 1) in('2') "+tmpkriteriamedrec+" and left(kd_pasien, 2) not in ('LB')  "+tmpkriteriaunittujuancrwi+" "+tmplunas+" and tgl_transaksi >='" + Ext.get('dtpTglAwalFilterPenJasLab').getValue() + "' and tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterPenJasLab').getValue() + "' ORDER BY tgl_transaksi desc, no_transaksi limit 10";
			tmpunit = 'ViewPenJasLab';
            loadpenjaslab(kriteria, tmpunit);
		}else
        {
            kriteria = "posting_transaksi = 'f'  and kd_bagian = 4 and kd_unit not in ('44','45') and tgl_transaksi >='" + Ext.get('dtpTglAwalFilterPenJasLab').getValue() + "' and tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterPenJasLab').getValue() + "' ORDER BY tgl_transaksi desc, no_transaksi";
            tmpunit = 'ViewPenJasLab';
            loadpenjaslab(kriteria, tmpunit);
        }
		
    }else if (combovalues === 'Transaksi Baru')
    {
        Ext.getCmp('cboStatusLunas_viPenJasLab').disable();
        if(radiovaluesLAB === '1')
        {
            if(combovaluesunit !== "")
            {
                if (combovaluesunit === "All")
                {
                  tmpkreteriaLAB = "";
                }else
                {
                tmpkreteriaLAB = "and u.kd_unit = '" + Ext.getCmp('cboUNIT_viKasirLab').getValue() + "'";
                }
            }else {
                tmpkreteriaLAB = "";
            }
            
			if (Ext.getCmp('txtNamaPasienPenJasLab').getValue() !== "")
            {
               tmpkriterianamaLAB = " AND lower(pasien.NAMA) like lower('%" + Ext.get('txtNamaPasienPenJasLab').dom.value + "%')";
            }else{
                tmpkriterianamaLAB = "";
            }
            
			if (Ext.getCmp('txtNoMedrecPenJasLab').getValue() !== "")
            {
               tmpkriteriamedrecLAB = " AND pasien.kd_pasien = '"+ Ext.get('txtNoMedrecPenJasLab').dom.value + "'";
            }else{
                tmpkriteriamedrecLAB = "";
            }
            tmpparams = " tr.tgl_transaksi >='" + Ext.get('dtpTglAwalFilterPenJasLab').getValue() + "' and tr.tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterPenJasLab').getValue() + "' "+ tmpkreteriaLAB +" "+ tmpkriterianamaLAB +" "+ tmpkriteriamedrecLAB +" and left(u.kd_unit,1) IN ('3') ORDER BY  tr.tgl_transaksi desc, tr.no_transaksi desc limit 10";
            tmpunit = 'ViewPenJasLab';
            //loadpenjaslab(tmpparams, tmpunit);
			refeshpenjaslab(tmpparams);
        }
        else if (radiovaluesLAB === '2')
        {
            if(ComboValuesKamar !== "")
            {
                if (ComboValuesKamar === "Semua")
                {
                    tmpkriteriakamar = "";
                }else
                {
                    tmpkriteriakamar = "and u.kd_unit = '" + Ext.getCmp('cboRujukanKamarSpesialJasRadRequestEntry').getValue() + "'";
                }
            }else {
                tmpkriteriakamar = "";
            }
			
            if (Ext.getCmp('txtNamaPasienPenJasLab').getValue() !== "")
            {
               tmpkriterianama = " AND lower(pasien.NAMA) like lower('%" + Ext.get('txtNamaPasienPenJasLab').dom.value + "%')";
            }else {
                tmpkriterianama = "";
            }
			
            if (Ext.getCmp('txtNoMedrecPenJasLab').getValue() !== "")
            {
               tmpkriteriamedrec = " AND pasien.kd_pasien = '"+ Ext.get('txtNoMedrecPenJasLab').dom.value + "'";
            }else {
                tmpkriteriamedrec = "";
            }
             tmpparams = " tr.tgl_transaksi >='" + Ext.get('dtpTglAwalFilterPenJasLab').getValue() + "' and tr.tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterPenJasLab').getValue() + "' "+ tmpkriteriakamar +" "+ tmpkriterianama +" "+ tmpkriteriamedrec +" and left(u.kd_unit,1) IN ('1') ORDER BY   tr.tgl_transaksi desc, tr.no_transaksi desc limit 10";
             tmpunit = 'ViewPenJasLab';
             //loadpenjaslab(tmpparams, tmpunit);
			 refeshpenjaslab(tmpparams);
			 
        }else if (radiovaluesLAB === '3')
        {
            PenJasLabLookUp();
			Ext.getCmp('txtnotlplab').setReadOnly(false);
        }else if (radiovaluesLAB === '4')
        {
			if(combovaluesunit !== "")
            {
                if (combovaluesunit === "All")
                {
                  tmpkreteriaLAB = "";
                }else
                {
                tmpkreteriaLAB = "and u.kd_unit = '" + Ext.getCmp('cboUNIT_viKasirLab').getValue() + "'";
                }
            }else {
                tmpkreteriaLAB = "";
            }
            
			if (Ext.getCmp('txtNamaPasienPenJasLab').getValue() !== "")
            {
               tmpkriterianamaLAB = " AND lower(pasien.NAMA) like lower('%" + Ext.get('txtNamaPasienPenJasLab').dom.value + "%')";
            }else{
                tmpkriterianamaLAB = "";
            }
            
			if (Ext.getCmp('txtNoMedrecPenJasLab').getValue() !== "")
            {
               tmpkriteriamedrecLAB = " AND pasien.kd_pasien = '"+ Ext.get('txtNoMedrecPenJasLab').dom.value + "'";
            }else{
                tmpkriteriamedrecLAB = "";
            }
            tmpparams = " tr.tgl_transaksi >='" + Ext.get('dtpTglAwalFilterPenJasLab').getValue() + "' and tr.tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterPenJasLab').getValue() + "' "+ tmpkreteriaLAB +" "+ tmpkriterianamaLAB +" "+ tmpkriteriamedrecLAB +" and left(u.kd_unit,1) IN ('2') ORDER BY   tr.tgl_transaksi desc, tr.no_transaksi desc limit 10";
            tmpunit = 'ViewPenJasLab';
            //loadpenjaslab(tmpparams, tmpunit);
			refeshpenjaslab(tmpparams);
		}
        
    }
}

//VALIDASI COMBO UNIT/POLI
function getDataCariUnitPenjasLab(kriteria)
{
	if (kriteria===undefined)
	{
		kriteria="kd_bagian=3 and parent<>'0'";
	}
	dsunit_viKasirLab.load
	(
            {
                params:
                    {
                        Skip: 0,
                        Take: 1000,
                        Sort: 'kd_unit',
                        Sortdir: 'ASC',
                        target: 'ViewSetupUnit',
                        param: kriteria
                    }
            }
	);
	return dsunit_viKasirLab;
}
function mComboUnit_viKasirLAB() 
{
	
    var Field = ['KD_UNIT','NAMA_UNIT'];
	
    dsunit_viKasirLab = new WebApp.DataStore({ fields: Field });
	getDataCariUnitPenjasLab();
    var cboUNIT_viKasirLab = new Ext.form.ComboBox
	(
            {
                id: 'cboUNIT_viKasirLab',
                x: 155,
                y: 70,
                typeAhead: true,
                triggerAction: 'all',
				emptyText:'Poli',
                lazyRender: true,
                mode: 'local',
                emptyText: '',
                fieldLabel:  ' ',
                align: 'Right',
                width: 100,
                store: dsunit_viKasirLab,
                valueField: 'KD_UNIT',
                displayField: 'NAMA_UNIT',
                value:'All',
				listeners:
                    {

                        'select': function(a, b, c) 
                            {					       
                                //RefreshDataFilterPenJasRad();
								combovaluesunit=b.data.valueField;
								validasiJenisTrLAB();
							}

                    }
            }
	);
	
    return cboUNIT_viKasirLab;
};

function mcomboKamarSpesialLAB()
{
    var Field = ['no_kamar','kamar'];
    ds_KamarSpesial_viJasRad = new WebApp.DataStore({fields: Field});

	ds_KamarSpesial_viJasRad.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: 'penerimaan',
                Sortdir: 'ASC',
                target:'ViewSetupKelasSpesial',
                param: ""
            }
        }
    )

    var cboRujukanKamarSpesialJasRadRequestEntry = new Ext.form.ComboBox
    (
        {
            x: 155,
            y: 70,
            id: 'cboRujukanKamarSpesialJasRadRequestEntry',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
            emptyText:'Pilih Kamar...',
            fieldLabel: 'Kamar ',
            align: 'Right',
            store: ds_KamarSpesial_viJasRad,
            valueField: 'no_kamar',
            displayField: 'kamar',
            Width:'150',
            listeners:
                {
                    'select': function(a, b, c)
					{
						ComboValuesKamar=b.data.valueField;
						validasiJenisTrLAB();    
					},
                    'render': function(c)
					{
						
					}


		}
        }
    )

    return cboRujukanKamarSpesialJasRadRequestEntry;
}

//LOOKUP DETAIL TRANSAKSI LABORATORIUM
function PenJasLabLookUp(rowdata) 
{
    var lebar = 900;
    FormLookUpsdetailTRLAB = new Ext.Window
    (
        {
            id: 'gridPenJasLab',
            title: 'Penata Jasa Laboratorium',
            closeAction: 'destroy',
            width: lebar,
            height: 550,
            border: false,
            resizable: false,
            plain: true,
            layout: 'fit',
            constrain: true,
            iconCls: 'Request',
            modal: true,
            items: getFormEntryPenJasLab(lebar,rowdata),
            listeners:
            {
				 activate: function()
				{
					if (rowdata === undefined){
						Ext.getCmp('txtNamaPasienLAB').focus(false,100);
						Ext.getCmp('txtNoMedrecLAB').setReadOnly(true);
						Ext.getCmp('txtNamaUnitLAB').setReadOnly(true);
						Ext.getCmp('txtNamaPasienLAB').setReadOnly(false);
						Ext.getCmp('txtAlamatLAB').setReadOnly(false);
						Ext.getCmp('dtpTtlLAB').setReadOnly(false);
						Ext.getCmp('txtCustomerLamaHide').hide();
					}else{
						Ext.getCmp('cboDOKTER_viPenJasLab').focus(false,1000);
					}
					
					
				}
            }
        }
    );

    FormLookUpsdetailTRLAB.show();
	getUnitDefault();
    if (rowdata === undefined) 
	{
        LABAddNew();
	}
	else 
	{
		TRLABInit(rowdata);
	}

};
function load_data_printer_kasirlab(param)
{

	Ext.Ajax.request(
	{
		url: baseURL + "index.php/main/functionLAB/getPrinter",
		params:{
			command: param
		} ,
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
		success: function(o) {
			//cbopasienorder_mng_apotek.store.removeAll();
				var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsprinter_kasirlab.recordType;
				var o=cst['listData'][i];
				
				recs.push(new recType(o));
				dsprinter_kasirlab.add(recs);
			}
		}
	});
}
function mCombo_printer_kasirlab(){ 
	var Field = ['kd_pasien','nama','kd_dokter','kd_unit','kd_customer','no_transaksi','kd_kasir','id_mrresep','urut_masuk','tgl_masuk','tgl_transaksi'];
    dsprinter_kasirlab = new WebApp.DataStore({ fields: Field });
	load_data_printer_kasirlab();
	var cbo_printer_kasirlab= new Ext.form.ComboBox
	(
		{
			id: 'cbopasienorder_printer_kasirlab',
			typeAhead		: true,
			triggerAction	: 'all',
			lazyRender		: true,
			hidden :true,
			mode			: 'local',
			emptyText: 'Pilih Printer',
			fieldLabel:  '',
			align: 'Right',
			width: 200,
			store: dsprinter_kasirlab,
			valueField: 'name',
			displayField: 'name',
			//hideTrigger		: true,
			listeners:
			{
								
			}
		}
	);return cbo_printer_kasirlab;
};
function PenjasLookUpLAB(rowdata)
{
    var lebar = 700;
    FormLookUpsdetailTRPenjasLAB = new Ext.Window
            (
                    {
                        id: 'gridPenjasLAB',
                        title: 'Penata Jasa Laboratorium',
                        closeAction: 'destroy',
                        width: lebar,
                        height: 500,
                        border: false,
                        resizable: false,
                        plain: true,
                        layout: 'fit',
                        iconCls: 'Request',
                        modal: true,
						
                        items: getFormEntryPenjasBayarLAB(lebar),
						enableKeyEvents:true,
                        listeners:
                                {
									keydown:function(text,e){
										if(e.keyCode == 122){
											e.preventDefault();
											alert('hai');
										}
									}
                                }
                    }
            );

    FormLookUpsdetailTRPenjasLAB.show();
    if (rowdata == undefined)
    {
        TRPenjasBayarLABInit(rowdata);
    } else
    {
        TRPenjasBayarLABInit(rowdata)
    }

}
;
function mEnabledKasirLABCM(mBol)
{
   /*  Ext.get('btnLookupKasirLAB').dom.disabled = mBol;
    Ext.get('btnHpsBrsKasirLAB').dom.disabled = mBol; */
}
;
function PenjasBayarLABAddNew()
{
    AddNewKasirLABKasir = true;
    Ext.get('txtNoTransaksiKasirLABKasir').dom.value = '';
    Ext.get('dtpTanggalDetransaksi').dom.value = nowTglTransaksi.format('d/M/Y');
    Ext.get('txtNoMedrecDetransaksi').dom.value = '';
    Ext.get('txtNamaPasienDetransaksi').dom.value = '';
    //Ext.get('txtKdUrutMasuk').dom.value = '';
    Ext.get('cboStatus_viKasirLABKasir').dom.value = ''
    rowSelectedPenJasLab = undefined;
    dsTRDetailPenJasLabList.removeAll();
    mEnabledKasirLABCM(false);


}
;
function loaddatastorePembayaran(jenis_pay)
{
	console.log(jenis_pay);
    dsComboBayar.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'nama',
                                    Sortdir: 'ASC',
                                    target: 'ViewComboBayar',
                                    param: 'jenis_pay=~' + jenis_pay + '~'
                                }
                    }
            )
}
function TRPenjasBayarLABInit(rowdata)
{
    AddNewKasirLABKasir = false;
	
    
	console.log(rowdata);
	
    
    
	if (rowdata===undefined)
	{
		Ext.get('dtpTanggalDetransaksi').dom.value = Ext.getCmp('dtpKunjunganLAB').getValue().format('d/M/Y');
		Ext.get('txtNoMedrecDetransaksi').dom.value = Ext.getCmp('txtNoMedrecLAB').getValue();
		Ext.get('txtNamaPasienDetransaksi').dom.value = Ext.getCmp('txtNamaPasienLAB').getValue();
		tanggaltransaksitampung = Ext.getCmp('dtpKunjunganLAB').getValue();
	}
	else
	{
		Ext.get('dtpTanggalDetransaksi').dom.value = Ext.getCmp('dtpKunjunganLAB').getValue().format('d/M/Y');
		Ext.get('txtNoMedrecDetransaksi').dom.value = rowdata.KD_PASIEN;
		Ext.get('txtNamaPasienDetransaksi').dom.value = rowdata.NAMA;
		tanggaltransaksitampung = rowdata.TGL_TRANSAKSI;
	}
    
    // take the displayField value 
	var notransnya;
	if (Ext.getCmp('txtNoTransaksiPenJasLab').getValue()==='' || Ext.getCmp('txtNoTransaksiPenJasLab').getValue()===undefined){
		notransnya=rowdata.NO_TRANSAKSI;
	}
	else
	{
		notransnya=Ext.getCmp('txtNoTransaksiPenJasLab').getValue();
	}
	var modul='';
	if(radiovaluesLAB == 1){
		modul='igd';
	} else if(radiovaluesLAB == 2){
		modul='rwi';
	} else if(radiovaluesLAB == 4){
		modul='rwj';
	} else{
		modul='langsung';
	}
	Ext.get('txtNoTransaksiKasirLABKasir').dom.value = notransnya;
	RefreshDataKasirLABKasirDetail(notransnya,kd_kasir_lab);
	notransaksiasal_lab='';
	kdkasirasal_lab='';
	Ext.Ajax.request({
        url: baseURL + "index.php/main/functionLAB/cekPembayaran",
        params: {
            notrans: notransnya,
			Modul:modul,
			kdkasir: kd_kasir_lab
        },
        failure: function (o)
        {
            var cst = Ext.decode(o.responseText);
        },
        success: function (o) {
			var cst = Ext.decode(o.responseText);
			console.log(cst.ListDataObj);
			loaddatastorePembayaran(cst.ListDataObj[0].jenis_pay);
			notransaksi=cst.ListDataObj[0].no_transaksi;
			kodeunit=cst.ListDataObj[0].kd_unit;
			vkd_unit = cst.ListDataObj[0].kd_unit;
			kdkasir = cst.ListDataObj[0].kd_kasir;
			//alert(kdkasir);
			notransaksiasal_lab = cst.ListDataObj[0].no_transaksi_asal;
			kdkasirasal_lab = cst.ListDataObj[0].kd_kasir_asal;
			Ext.get('cboPembayaran').dom.value =cst.ListDataObj[0].ket_payment;
			Ext.get('cboJenisByr').dom.value =cst.ListDataObj[0].cara_bayar; 
			vflag = cst.ListDataObj[0].flag;
			tapungkd_pay = cst.ListDataObj[0].kd_pay;
			vkode_customer =  cst.ListDataObj[0].kd_customer;
			
        }

    });
    
    
    tampungtypedata = 0;
    
    jenispay = 1;
	
    showCols(Ext.getCmp('gridDTItemTest'));
    hideCols(Ext.getCmp('gridDTItemTest'));
   
    //(rowdata.NO_TRANSAKSI;


    Ext.Ajax.request({
        url: baseURL + "index.php/main/getcurrentshift",
        params: {
            command: '0',
        },
        failure: function (o)
        {
            var cst = Ext.decode(o.responseText);
        },
        success: function (o) {

            tampungshiftsekarang = o.responseText
        }

    });
	


}
;
function getItemPanelNoTransksiLABKasir(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .40,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'No. Transaksi ',
                                                name: 'txtNoTransaksiKasirLABKasir',
                                                id: 'txtNoTransaksiKasirLABKasir',
                                                emptyText: nmNomorOtomatis,
                                                readOnly: true,
                                                anchor: '99%',
												enableKeyEvents:true,
												listeners:
														{
															keydown:function(text,e){
																if(e.keyCode == 122){
																	e.preventDefault();
																	printbillRadLab();
																}else if(e.keyCode == 123){
																	e.preventDefault();
																	printkwitansiRadLab();
																}
															}
														},
                                            }
                                        ]
                            },
                            {
                                columnWidth: .60,
                                layout: 'form',
                                border: false,
                                labelWidth: 55,
                                items:
                                        [
                                            {
                                                xtype: 'datefield',
                                                fieldLabel: 'Tanggal ',
                                                id: 'dtpTanggalDetransaksi',
                                                name: 'dtpTanggalDetransaksi',
                                                format: 'd/M/Y',
                                                readOnly: true,
                                                value: now,
                                                anchor: '100%',
												enableKeyEvents:true,
												listeners:
														{
															keydown:function(text,e){
																if(e.keyCode == 122){
																	e.preventDefault();
																	printbillRadLab();
																}else if(e.keyCode == 123){
																	e.preventDefault();
																	printkwitansiRadLab();
																}
															}
														},
                                            }
                                        ]
                            }
                        ]
            }
    return items;
}
;


function mComboJenisByrView()
{
    var Field = ['JENIS_PAY', 'DESKRIPSI', 'TYPE_DATA'];

    dsJenisbyrView = new WebApp.DataStore({fields: Field});
    dsJenisbyrView.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'jenis_pay',
                                    Sortdir: 'ASC',
                                    target: 'ComboJenis',
                                }
                    }
            );

    var cboJenisByr = new Ext.form.ComboBox
            (
                    {
                        id: 'cboJenisByr',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        emptyText: '',
                        fieldLabel: 'Pembayaran      ',
                        align: 'Right',
                        anchor: '100%',
                        store: dsJenisbyrView,
                        valueField: 'JENIS_PAY',
                        displayField: 'DESKRIPSI',
						enableKeyEvents:true,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {

                                        loaddatastorePembayaran(b.data.JENIS_PAY);
                                        tampungtypedata = b.data.TYPE_DATA;
                                        jenispay = b.data.JENIS_PAY;
                                        showCols(Ext.getCmp('gridDTItemTest'));
                                        hideCols(Ext.getCmp('gridDTItemTest'));
                                        getTotalDetailProdukLAB();
                                        Ext.get('cboPembayaran').dom.value = 'Pilih Pembayaran...';
                                    },
									keydown:function(text,e)
									{
										if(e.keyCode == 122){
											e.preventDefault();
											printbillRadLab();
										}else if(e.keyCode == 123){
											e.preventDefault();
											printkwitansiRadLab();
										}
									}
                                }
                    }
            );

    return cboJenisByr;
}
;
function getTotalDetailProdukLAB()
{
	
    var TotalProduk = 0;
    var bayar;
    var tampunggrid;
    var x = '';
    for (var i = 0; i < dsTRDetailKasirLABKasirList.getCount(); i++)
    {

        var recordterakhir;

        //alert(TotalProduk);
        if (tampungtypedata == 0)
        {
            tampunggrid = parseInt(dsTRDetailKasirLABKasirList.data.items[i].data.BAYARTR);

            //recordterakhir= tampunggrid
            //TotalProduk.toString().replace(/./gi, "");
            TotalProduk += tampunggrid

            recordterakhir = TotalProduk
            Ext.get('txtJumlah2EditData_viKasirLAB').dom.value = formatCurrency(recordterakhir);
        }
        if (tampungtypedata == 3)
        {
            tampunggrid = parseInt(dsTRDetailKasirLABKasirList.data.items[i].data.PIUTANG);
            //TotalProduk.toString().replace(/./gi, "");
            //recordterakhir=tampunggrid
            TotalProduk += tampunggrid
            recordterakhir = TotalProduk
            Ext.get('txtJumlah2EditData_viKasirLAB').dom.value = formatCurrency(recordterakhir);
        }
        if (tampungtypedata == 1)
        {
            tampunggrid = parseInt(dsTRDetailKasirLABKasirList.data.items[i].data.DISCOUNT);

            TotalProduk += tampunggrid
            recordterakhir = TotalProduk
            Ext.get('txtJumlah2EditData_viKasirLAB').dom.value = formatCurrency(recordterakhir);
        }
    }
	
	if (Ext.getCmp('txtJumlah2EditData_viKasirLAB').getValue()==='0' || Ext.getCmp('txtJumlah2EditData_viKasirLAB').getValue()===0 )
	{
		Ext.getCmp('btnSimpanKasirLAB').disable();
		tombol_bayar='disable';
		Ext.getCmp('btnTransferKasirLAB').disable();
	}
	else
	{
		Ext.getCmp('btnSimpanKasirLAB').enable();
		tombol_bayar='enable';
		Ext.getCmp('btnTransferKasirLAB').enable();
	}
    bayar = Ext.get('txtJumlah2EditData_viKasirLAB').getValue();
    return bayar;
}
;


    
function hideCols(grid)
{
    if (tampungtypedata == 3)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('BAYARTR'), true);
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('DISCOUNT'), true);
    } else if (tampungtypedata == 0)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('PIUTANG'), true);
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('DISCOUNT'), true);
    } else if (tampungtypedata == 1)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('PIUTANG'), true);
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('BAYARTR'), true);

    }
}
;
function showCols(grid) {
    if (tampungtypedata == 3)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('PIUTANG'), false);

    } else if (tampungtypedata == 0)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('BAYARTR'), false);
    } else if (tampungtypedata == 1)
    {
        grid.getColumnModel().setHidden(grid.getColumnModel().findColumnIndex('DISCOUNT'), false);
    }

}
;
function mComboPembayaran()
{
    var Field = ['KD_PAY', 'JENIS_PAY', 'PAYMENT'];

    dsComboBayar = new WebApp.DataStore({fields: Field});

    var cboPembayaran = new Ext.form.ComboBox
            (
                    {
                        id: 'cboPembayaran',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Pilih Pembayaran...',
                        labelWidth: 80,
                        align: 'Right',
                        store: dsComboBayar,
                        valueField: 'KD_PAY',
                        displayField: 'PAYMENT',
                        anchor: '100%',
						enableKeyEvents:true,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        tapungkd_pay = b.data.KD_PAY;
                                    },
									keydown:function(text,e)
									{
										if(e.keyCode == 122){
											e.preventDefault();
											printbillRadLab();
										}else if(e.keyCode == 123){
											e.preventDefault();
											printkwitansiRadLab();
										}
									}
                                }
                    }
            );

    return cboPembayaran;
}
;
function getItemPanelmedreckasirLAB(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .40,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'No. Medrec ',
                                                name: 'txtNoMedrecDetransaksi',
                                                id: 'txtNoMedrecDetransaksi',
                                                readOnly: true,
                                                anchor: '99%',
												enableKeyEvents:true,
												listeners:
														{
															keydown:function(text,e){
																if(e.keyCode == 122){
																	e.preventDefault();
																	printbillRadLab();
																}else if(e.keyCode == 123){
																	e.preventDefault();
																	printkwitansiRadLab();
																}
															}
														},
                                            }
                                        ]
                            },
                            {
                                columnWidth: .60,
                                layout: 'form',
                                border: false,
                                labelWidth: 2,
                                items:
                                        [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: '',
                                                //hideLabel:true,
                                                readOnly: true,
                                                name: 'txtNamaPasienDetransaksi',
                                                id: 'txtNamaPasienDetransaksi',
                                                anchor: '100%',
												enableKeyEvents:true,
												listeners:
														{
															keydown:function(text,e){
																if(e.keyCode == 122){
																	e.preventDefault();
																	printbillRadLab();
																}else if(e.keyCode == 123){
																	e.preventDefault();
																	printkwitansiRadLab();
																}
															}
														},
                                            }
                                        ]
                            }
                        ]
            }
    return items;
}
;
function getItemPanelInputKasirLAB(lebar)
{
    var items =
            {
                layout: 'fit',
                anchor: '100%',
                width: lebar - 35,
                labelAlign: 'right',
                bodyStyle: 'padding:10px 10px 10px 0px',
                border: true,
                height: 120,
				enableKeyEvents:true,
                        listeners:
                                {
									keydown:function(text,e){
										if(e.keyCode == 122){
											e.preventDefault();
											alert('hai');
										}
									}
                                },
                items:
                        [
                            {
                                columnWidth: .9,
                                width: lebar - 35,
                                labelWidth: 100,
                                layout: 'form',
                                border: false,
                                items:
                                        [
                                            getItemPanelNoTransksiLABKasir(lebar),
                                            getItemPanelmedreckasirLAB(lebar), getItemPanelUnitKasirLAB(lebar),
											getItemPanelTanggalBayarKasirLAB(lebar)
                                        ]
                            }
                        ]
            };
    return items;
}
;



function getItemPanelUnitKasirLAB(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
                            {
                                columnWidth: .40,
                                layout: 'form',
                                labelWidth: 100,
                                border: false,
                                items:
                                        [
                                            mComboJenisByrView()
                                        ]
                            }, {
                                columnWidth: .60,
                                layout: 'form',
                                labelWidth: 0.9,
                                border: false,
                                items:
                                        [
                                            mComboPembayaran()
                                        ]
                            }
                        ]
            }
    return items;
}
;

function getItemPanelTanggalBayarKasirLAB(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                items:
                        [
						{
							columnWidth: .40,
							layout: 'form',
							border: false,
							labelWidth: 100,
							items:
							[
										{
											xtype: 'datefield',
											fieldLabel: 'Tanggal Bayar',
											id: 'dtpTanggalBayarDetransaksi',
											name: 'dtpTanggalBayarDetransaksi',
											format: 'd/M/Y',
											//readOnly: true,
											value: now,
											anchor: '100%',
											enableKeyEvents:true,
											listeners:
													{
														keydown:function(text,e){
															if(e.keyCode == 122){
																e.preventDefault();
																printbillRadLab();
															}else if(e.keyCode == 123){
																e.preventDefault();
																printkwitansiRadLab();
															}
														}
													},
										}
							]
						}
							
                        ]
            }
    return items;
}
;

function RefreshDataKasirLABKasirDetail(no_transaksi,kd_kasir)
{
    var strKriteriaKasirLAB = '';
	 if (kd_kasir !== undefined) {
        strKriteriaKasirLAB = "\"no_transaksi\" = ~" + no_transaksi + "~" + " and kd_kasir = " + "~" + kd_kasir + "~";
    }else{
        strKriteriaKasirLAB = "\"no_transaksi\" = ~" + no_transaksi + "~" ;
    }
    //strKriteriaKasirLAB = 'no_transaksi = ~' + no_transaksi + '~';

    dsTRDetailKasirLABKasirList.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'tgl_transaksi',
                                    //Sort: 'tgl_transaksi',
                                    Sortdir: 'ASC',
                                    target: 'ViewDetailbayarLAB',
                                    param: strKriteriaKasirLAB
                                }
                    }
            );
    return dsTRDetailKasirLABKasirList;
}
;
function GetDTLTRKasirLABGrid()
{
    var fldDetailLAB = ['KD_PRODUK', 'DESKRIPSI', 'DESKRIPSI2', 'KD_TARIF', 'HARGA', 'QTY', 'DESC_REQ', 'TGL_BERLAKU', 'NO_TRANSAKSI', 'URUT', 'DESC_STATUS', 'TGL_TRANSAKSI', 'BAYARTR', 'DISCOUNT', 'PIUTANG','TAG'];

    dsTRDetailKasirLABKasirList = new WebApp.DataStore({fields: fldDetailLAB})
    //RefreshDataKasirLABKasirDetail();
    gridDTLTRKasirLAB = new Ext.grid.EditorGridPanel
            (
                    {
                        title: 'Detail Pembayaran',
                        stripeRows: true,
                        id: 'gridDTLTRKasirLAB',
                        store: dsTRDetailKasirLABKasirList,
                        border: false,
                        columnLines: true,
                        frame: false,
                        anchor: '100%',
                        height: 230,
                        autoScroll: true,
                        sm: new Ext.grid.CellSelectionModel
                                (
                                        {
                                            singleSelect: true,
                                            listeners:
                                                    {
                                                    }
                                        }
                                ),
                        cm: TRKasirLabColumModel()
                    }
            );

    return gridDTLTRKasirLAB;
}
;

function TRKasirLabColumModel()
{
    return new Ext.grid.ColumnModel
            (
                    [
                        new Ext.grid.RowNumberer(),
                        {
                            id: 'coleskripsiKasirLAB',
                            header: 'Uraian',
                            dataIndex: 'DESKRIPSI2',
                            width: 250,
                            menuDisabled: true,
                            hidden: true

                        },{
							id: 'coltagKasirLab',
							header: 'Tag',
							dataIndex: 'TAG',
							width:30,
							xtype:'checkcolumn',
							checked:true
						},
                        {
                            id: 'colKdProduk',
                            header: 'Kode Produk',
                            dataIndex: 'KD_PRODUK',
                            width: 100,
                            menuDisabled: true,
                            hidden: true
                        },
                        {
                            id: 'colDeskripsiKasirLAB',
                            header: 'Nama Produk',
                            dataIndex: 'DESKRIPSI',
                            sortable: false,
                            hidden: false,
                            menuDisabled: true,
                            width: 250

                        },
                        {
                            id: 'colURUTKasirLAB',
                            header: 'Urut',
                            dataIndex: 'URUT',
                            sortable: false,
                            hidden: true,
                            menuDisabled: true,
                            width: 250

                        },
                        {
                            header: 'Tanggal Transaksi',
                            dataIndex: 'TGL_TRANSAKSI',
                            width: 130,
                            hidden: true,
                            menuDisabled: true,
                            renderer: function (v, params, record)
                            {

                                return ShowDate(record.data.TGL_TRANSAKSI);
                            }
                        },
                        {
                            id: 'colQtyKasirLAB',
                            header: 'Qty',
                            width: 91,
                            align: 'right',
                            menuDisabled: true,
                            dataIndex: 'QTY',
                        },
                        {
                            id: 'colHARGAKasirLAB',
                            header: 'Harga',
                            align: 'right',
                            hidden: false,
                            menuDisabled: true,
                            dataIndex: 'HARGA',
                            width: 100,
                            renderer: function (v, params, record)
                            {
                                return formatCurrency(record.data.HARGA);

                            }
                        },
                        {
                            id: 'colPiutangKasirLAB',
                            header: 'Puitang',
                            width: 80,
                            dataIndex: 'PIUTANG',
                            align: 'right',
                            //hidden: false,
                            editor: new Ext.form.TextField
                                    (
                                            {
                                                id: 'fieldcolPuitangLAB',
                                                allowBlank: true,
                                                enableKeyEvents: true,
                                                width: 30,
                                            }
                                    ),
                            renderer: function (v, params, record)
                            {
                                getTotalDetailProdukLAB();
                                return formatCurrency(record.data.PIUTANG);
                            }
                        },
                        {
                            id: 'colTunaiKasirLAB',
                            header: 'Tunai',
                            width: 80,
                            dataIndex: 'BAYARTR',
                            align: 'right',
                            hidden: false,
                            editor: new Ext.form.TextField
                                    (
                                            {
                                                id: 'fieldcolTunaiLAB',
                                                allowBlank: true,
                                                enableKeyEvents: true,
                                                width: 30,
                                            }
                                    ),
                            renderer: function (v, params, record)
                            {
                                getTotalDetailProdukLAB();
								
								return formatCurrency(record.data.BAYARTR);
                            }

                        },
                        {
                            id: 'colDiscountKasirLAB',
                            header: 'Discount',
                            width: 80,
                            dataIndex: 'DISCOUNT',
                            align: 'right',
                            hidden: false,
                            editor: new Ext.form.TextField
                                    (
                                            {
                                                id: 'fieldcolDiscountLAB',
                                                allowBlank: true,
                                                enableKeyEvents: true,
                                                width: 30,
                                            }
                                    ),
                            renderer: function (v, params, record)
                            {
                                return formatCurrency(record.data.DISCOUNT);
                            }
                        }

                    ]
                    )
}
;

function getParamDetailTransaksiKasirLAB()
{
    if (tampungtypedata === '')
    {
        tampungtypedata = 0;
    }
    ;

    var params =
            {
                kdUnit: vkd_unit,
                TrKodeTranskasi: Ext.get('txtNoTransaksiKasirLABKasir').getValue(),
                Tgl: Ext.get('dtpTanggalDetransaksi').dom.value,
                Shift: tampungshiftsekarang,
                kdKasir: kdkasir,
                bayar: tapungkd_pay,
                Flag: vflag,
                Typedata: tampungtypedata,
                Totalbayar: getTotalDetailProdukLAB(),
                List: getArrDetailTrKasirLAB(),
                JmlField: mRecordKasirLAB.prototype.fields.length - 4,
                JmlList: GetListCountDetailTransaksiLAB(),
                Hapus: 1,
                Ubah: 0,
				TglBayar : Ext.get('dtpTanggalBayarDetransaksi').dom.value,
            };
    return params
}
;

function getArrDetailTrKasirLAB()
{
    var x = '';
    for (var i = 0; i < dsTRDetailKasirLABKasirList.getCount(); i++)
    {
        if (dsTRDetailKasirLABKasirList.data.items[i].data.KD_PRODUK != '' && dsTRDetailKasirLABKasirList.data.items[i].data.DESKRIPSI != '')
        {
            var y = '';
            var z = '@@##$$@@';

            y = 'URUT=' + dsTRDetailKasirLABKasirList.data.items[i].data.URUT
            y += z + dsTRDetailKasirLABKasirList.data.items[i].data.KD_PRODUK
            y += z + dsTRDetailKasirLABKasirList.data.items[i].data.QTY
            y += z + dsTRDetailKasirLABKasirList.data.items[i].data.HARGA
            y += z + dsTRDetailKasirLABKasirList.data.items[i].data.KD_TARIF
            y += z + dsTRDetailKasirLABKasirList.data.items[i].data.URUT
            y += z + dsTRDetailKasirLABKasirList.data.items[i].data.BAYARTR
            y += z + dsTRDetailKasirLABKasirList.data.items[i].data.PIUTANG
            y += z + dsTRDetailKasirLABKasirList.data.items[i].data.DISCOUNT



            if (i === (dsTRDetailKasirLABKasirList.getCount() - 1))
            {
                x += y
            } else
            {
                x += y + '##[[]]##'
            }
            ;
        }
        ;
    }

    return x;
}
;

function ValidasiEntryCMKasirLAB(modul, mBolHapus)
{
    var x = 1;

    if ((Ext.get('txtNoTransaksiKasirLABKasir').getValue() == '')

            || (Ext.get('txtNoMedrecDetransaksi').getValue() == '')
            || (Ext.get('txtNamaPasienDetransaksi').getValue() == '')
            || (Ext.get('dtpTanggalDetransaksi').getValue() == '')
            || dsTRDetailKasirLABKasirList.getCount() === 0
            || (Ext.get('cboPembayaran').getValue() == '')
            || (Ext.get('cboPembayaran').getValue() == 'Pilih Pembayaran...'))
    {
        if (Ext.get('txtNoTransaksiKasirLABKasir').getValue() == '' && mBolHapus === true)
        {
            x = 0;
        } else if (Ext.get('cboPembayaran').getValue() == '' || Ext.get('cboPembayaran').getValue() == 'Pilih Pembayaran...')
        {
            ShowPesanWarningPenJasLab(('Data pembayaran tidak  boleh kosong'), modul);
            x = 0;
        } else if (Ext.get('txtNoMedrecDetransaksi').getValue() == '')
        {
            ShowPesanWarningPenJasLab(('Data no. medrec tidak boleh kosong'), modul);
            x = 0;
        } else if (Ext.get('txtNamaPasienDetransaksi').getValue() == '')
        {
            ShowPesanWarningPenJasLab('Nama pasien belum terisi', modul);
            x = 0;
        } else if (Ext.get('dtpTanggalDetransaksi').getValue() == '')
        {
            ShowPesanWarningPenJasLab(('Data tanggal kunjungan tidak boleh kosong'), modul);
            x = 0;
        }
        //cboPembayaran

        else if (dsTRDetailKasirLABKasirList.getCount() === 0)
        {
            ShowPesanWarningPenJasLab('Data dalam tabel kosong', modul);
            x = 0;
        }
        ;
    }
    ;
    return x;
}
;
function Datasave_KasirLABKasir(mBol)
{
    if (ValidasiEntryCMKasirLAB(nmHeaderSimpanData, false) == 1)
    {
		// if (tglTransaksiAwalPembandingTransfer < Ext.getCmp('dtpTanggalBayarDetransaksi').getValue().format('Y-m-d')+" 00:00:00"){
		// 	ShowPesanWarningPenJasLab('Tanggal bayar Tidak boleh kurang dari tanggal transaksi pasien', 'Gagal');
		// 	Ext.getCmp('btnSimpanKasirLAB').enable();
		// 	tombol_bayar='enable';
		// 	Ext.getCmp('btnTransferKasirLAB').enable();
		// }else{
			Ext.Ajax.request
                (
                        {
                            //url: "./Datapool.mvc/CreateDataObj",
                            url: baseURL + "index.php/main/functionLAB/savepembayaran",
                            params: getParamDetailTransaksiKasirLAB(),
                            failure: function (o)
                            {
                                ShowPesanWarningPenJasLab('Data Belum Simpan segera Hubungi Admin', 'Gagal');
                                //RefreshDataFilterKasirLABKasir();
                            },
                            success: function (o)
                            {
								/*  RefreshDatahistoribayar_LAB('0');
                                RefreshDataPenJasLabDetail('0'); */
                                var cst = Ext.decode(o.responseText);
                                if (cst.success === true)
                                {
									Ext.getCmp('btnSimpanKasirLAB').disable();
									tombol_bayar='disable';
									Ext.getCmp('btnTransferKasirLAB').disable();
                                    ShowPesanInfoPenJasLab("Pembayaran berhasil", "Pembayaran");
									Ext.getCmp('txtNoTransaksiKasirLABKasir').focus();
									RefreshDatahistoribayar_LAB(Ext.get('txtNoTransaksiKasirLABKasir').dom.value);
									RefreshDataKasirLABKasirDetail(Ext.get('txtNoTransaksiKasirLABKasir').dom.value,kd_kasir_lab);
									if (radiovaluesLAB !=='3' && combovalues !== 'Transaksi Baru'){
										validasiJenisTrLAB();
									}
									//FormLookUpsdetailTRPenjasLAB.close();
                                    /* ViewGridBawahLookupPenjasLab(Ext.get('txtNoTransaksiKasirLABKasir').dom.value,kd_kasir_lab);
                                    //RefreshDataKasirLABKasir();
                                    if (mBol === false)
                                    {
                                        ViewGridBawahLookupPenjasLab();
                                    }
                                    ; */
                                } else
                                {
                                    ShowPesanWarningPenJasLab('Data Belum Simpan segera Hubungi Admin', 'Gagal');
                                }
                                ;
                            }
                        }
                )
		// }
        

    } else
    {
        if (mBol === true)
        {
            return false;
        }
        ;
    }
    ;

}

function TransferLookUp_lab(rowdata)
{
    var lebar = 440;
    FormLookUpsdetailTRTransfer_Lab = new Ext.Window
            (
                    {
                        id: 'gridTransfer_Lab',
                        title: 'Transfer',
                        closeAction: 'destroy',
                        width: lebar,
                        height: 410,
                        border: false,
                        resizable: false,
                        plain: false,
                        layout: 'fit',
                        iconCls: 'Request',
                        modal: true,
                        items: getFormEntryTRTransfer_lab(lebar),
                        listeners:
                                {
									 activate: function()
                                    {
                                        Ext.getCmp('cboalasan_transferLAB').setValue('Pembayaran Disatukan');                                        
                                    }
                                }
                    }
            );

    FormLookUpsdetailTRTransfer_Lab.show();
    //  Transferbaru();

};
function getParamTransferRwi_dari_lab()
{

/*
       KDkasirIGD:'01',
		TrKodeTranskasi: notransaksi,
		KdUnit: kodeunit,
		Kdpay: kdpaytransfer,
		Jumlahtotal: Ext.get(txtjumlahbiayasalLAB).dom.value,
		Tglasal:  ShowDate(tgltrans),
		Shift: tampungshiftsekarang,
		TRKdTransTujuan:Ext.get(txtTranfernoTransaksiRWJ).dom.value,
		KdpasienIGDtujuan: Ext.get(txtTranfernomedrecRWJ).dom.value,
		TglTranasksitujuan : Ext.get(dtpTanggaltransaksiRujuanRWJ).dom.value,
		KDunittujuan : Trkdunit2,
		KDalasan :Ext.get(cboalasan_transferLAB).dom.value,
		KasirRWI:'05',
		Kdcustomer:kdcustomeraa,
		kodeunitkamar:tmp_kodeunitkamar,
		kdspesial:tmp_kdspesial,
		nokamar:tmp_nokamar,


*/
    var params =
            {
                KDkasirIGD: kdkasir,
                Kdcustomer: vkode_customer,
                TrKodeTranskasi: notransaksi,
                KdUnit: kodeunit,
                Kdpay: kdpaytransfer,
                Jumlahtotal: Ext.get(txtjumlahbiayasalLAB).dom.value,
                Tglasal: ShowDate(TglTransaksi),
                Shift: tampungshiftsekarang,
                TRKdTransTujuan: Ext.get(txtTranfernoTransaksiLAB).dom.value,
                KdpasienIGDtujuan: Ext.get(txtTranfernomedrecLAB).dom.value,
                TglTranasksitujuan: Ext.get(dtpTanggaltransaksiRujuanLAB).dom.value,
                KDunittujuan: Trkdunit2,
                KDalasan: Ext.get(cboalasan_transferLAB).dom.value,
                KasirRWI: kdkasirasal_lab,
				kodeunitkamar:tmp_kodeunitkamar_LAB,
				kdspesial:tmp_kdspesial_LAB,
				nokamar:tmp_nokamar_LAB,
				TglTransfer:Ext.get(dtpTanggaltransferLAB).dom.value


            };
    return params
}
;
function TransferData_lab(mBol)
{
	// if (tglTransaksiAwalPembandingTransfer < Ext.getCmp('dtpTanggaltransferLAB').getValue().format('Y-m-d')+" 00:00:00"){
	// 	ShowPesanWarningPenJasLab('Tanggal transfer Tidak boleh kurang dari tanggal transaksi pasien', 'Gagal');
	// }else{
		Ext.Ajax.request(
		{
			url: baseURL + "index.php/main/getcurrentshift",
			 params: {
				command: '0',
				// parameter untuk url yang dituju (fungsi didalam controller)
			},
			failure: function(o)
			{
				 var cst = Ext.decode(o.responseText);
				
			},	    
			success: function(o) {
				tampungshiftsekarang=o.responseText
				
			}
			
		
		});
		Ext.Ajax.request
				(
						{
							//url: "./Datapool.mvc/CreateDataObj",
							url: baseURL + "index.php/main/functionLAB/saveTransfer",
							params: getParamTransferRwi_dari_lab(),
							failure: function (o)
							{

								ShowPesanWarningPenJasLab('Transfer Tidak berhasil silahkan hubungi admin', 'Gagal');
								ViewGridBawahLookupPenjasLab();
							},
							success: function (o)
							{	
							
								
								var cst = Ext.decode(o.responseText);
								if (cst.success === true)
								{	Ext.getCmp('btnSimpanKasirLAB').disable();
									tombol_bayar='disable';
									Ext.getCmp('btnTransferKasirLAB').disable();
									ShowPesanInfoPenJasLab('Transfer Berhasil', 'transfer ');
									Ext.getCmp('txtNoTransaksiKasirLABKasir').focus();
									RefreshDatahistoribayar_LAB(Ext.getCmp('txtNoTransaksiPenJasLab').getValue());
									FormLookUpsdetailTRTransfer_Lab.close();
									//FormLookUpsdetailTRPenjasLAB.close();
									
								} else
								{
									if(cst.message != undefined){
										ShowPesanWarningPenJasLab(cst.message, 'Gagal');
									}else{
										ShowPesanWarningPenJasLab('Transfer Tidak berhasil silahkan hubungi admin', 'Gagal');
									}
									
								}
								;
							}
						}
				)
	// }
	

}
;

function getItemPanelButtonTransfer_lab(lebar)
{
    var items =
            {
                layout: 'column',
                border: false,
                height: 30,
                anchor: '100%',
                style: {'margin-top': '-1px'},
                items:
                        [
                            {
                                layout: 'hBox',
                                width: 400,
                                border: false,
                                bodyStyle: 'padding:5px 0px 5px 5px',
                                defaults: {margins: '3 3 3 3'},
                                anchor: '90%',
                                layoutConfig:
                                        {
                                            align: 'middle',
                                            pack: 'end'
                                        },
                                items:
                                        [
                                            {
                                                xtype: 'button',
                                                text: 'Simpan',
                                                width: 70,
                                                style: {'margin-left': '0px', 'margin-top': '0px'},
                                                hideLabel: true,
                                                id: 'btnOkTransfer_lab',
                                                handler: function ()
                                                {
													loadMask.show();
                                                    TransferData_lab(false);
													loadMask.hide();
													 Ext.getCmp('btnSimpanKasirLAB').disable();
													 tombol_bayar='disable';
													 Ext.getCmp('btnTransferKasirLAB').disable();
													
                                                }
                                            },
                                            {
                                                xtype: 'button',
                                                text: 'Tutup',
                                                width: 70,
                                                hideLabel: true,
                                                id: 'btnCancelTransfer_lab',
                                                handler: function ()
                                                {
                                                    FormLookUpsdetailTRTransfer_Lab.close();
                                                }
                                            }
                                        ]
                            }
                        ]
            }
    return items;
}
;
function getFormEntryTRTransfer_lab(lebar)
{
    var pnlTRTransfer = new Ext.FormPanel
            (
                    {
                        id: 'PanelTRTransfer',
                        fileUpload: true,
                        region: 'north',
                        layout: 'column',
                        bodyStyle: 'padding:10px 10px 10px 10px',
                        height: 425,
                        anchor: '100%',
                        width: lebar,
                        border: false,
                        items: [getItemPanelInputTransfer_LAB(lebar), getItemPanelButtonTransfer_lab(lebar)],
                        tbar:
                                [
                                ]
                    }
            );

    var FormDepanTransfer = new Ext.Panel
            (
                    {
                        id: 'FormDepanTransfer',
                        region: 'center',
                        width: '100%',
                        anchor: '100%',
                        layout: 'form',
                        title: '',
                        bodyStyle: 'padding:15px',
                        border: true,
                        bodyStyle: 'background:#FFFFFF;',
                                shadhow: true,
                        items:
                                [
                                    pnlTRTransfer

                                ]

                    }
            );

    return FormDepanTransfer
}
;

function mComboTransferTujuan()
{
    var cboTransferTujuan = new Ext.form.ComboBox
            (
                    {
                        id: 'cboTransferTujuan',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        anchor: '96%',
                        emptyText: '',
                        hidden: true,
                        fieldLabel: 'Transfer',
                        //width:60,
                        store: new Ext.data.ArrayStore
                                (
                                        {
                                            id: 0,
                                            fields:
                                                    [
                                                        'Id',
                                                        'displayText'
                                                    ],
                                            data: [[1, 'Gawat Darurat'], [2, 'Rawat Inap'], [3, 'Rawat Jalan']]
                                        }
                                ),
                        valueField: 'Id',
                        displayField: 'displayText',
                        value: tranfertujuan,
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                        tranfertujuan = b.data.displayText;
                                        //RefreshDataSetDokter();
                                        ViewGridBawahLookupPenjasLab();

                                    }
                                }
                    }
            );
    return cboTransferTujuan;
}
;

function getTransfertujuan_lab(lebar)
{
    var items =
            {
                Width: lebar - 2,
                height: 170,
                layout: 'form',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                labelWidth: 130,
                items:
                        [
                            {
                                xtype: 'tbspacer',
                                height: 2
                            },
                            mComboTransferTujuan(),
                            {
                                xtype: 'textfield',
                                fieldLabel: 'No Transaksi',
                                //maxLength: 200,
                                name: 'txtTranfernoTransaksiLAB',
                                id: 'txtTranfernoTransaksiLAB',
                                labelWidth: 130,
                                readonly: true,
                                width: 100,
                                anchor: '95%'
                            },
                            {
                                xtype: 'datefield',
                                fieldLabel: 'Tanggal ',
                                id: 'dtpTanggaltransaksiRujuanLAB',
                                name: 'dtpTanggaltransaksiRujuanLAB',
                                format: 'd/M/Y',
                                readOnly: true,
                                //  value: now,
                                labelWidth: 130,
                                width: 100,
                                anchor: '95%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'No Medrec',
                                //maxLength: 200,
                                name: 'txtTranfernomedrecLAB',
                                id: 'txtTranfernomedrecLAB',
                                labelWidth: 130,
                                readOnly: true,
                                width: 100,
                                anchor: '95%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'Nama Pasien',
                                //maxLength: 200,
                                name: 'txtTranfernamapasienLAB',
                                id: 'txtTranfernamapasienLAB',
                                readOnly: true,
                                labelWidth: 130,
                                width: 100,
                                anchor: '95%'
                            },
                            {
                                xtype: 'textfield',
                                fieldLabel: 'Unit Perawatan',
                                //maxLength: 200,
                                name: 'txtTranferunitLAB',
                                id: 'txtTranferunitLAB',
                                readOnly: true,
                                labelWidth: 130,
                                width: 100,
                                anchor: '95%'
                            },
							{
                                        xtype: 'textfield',
                                        fieldLabel: 'Unit Perawatan',
                                        name: 'txtTranferkelaskamar_LAB',
                                        id: 'txtTranferkelaskamar_LAB',
										readOnly : true,
										labelWidth:130,
                                        width: 100,
                                        anchor: '95%'
                            },
							{
                                xtype: 'datefield',
                                fieldLabel: 'Tanggal transfer ',
                                id: 'dtpTanggaltransferLAB',
                                name: 'dtpTanggaltransferLAB',
                                format: 'd/M/Y',
                                //readOnly: true,
                                value: now,
                                labelWidth: 130,
                                width: 100,
                                anchor: '95%'
                            },
                        ]
            }
    return items;
}
;

function getItemPanelNoTransksiTransferLAB(lebar)
{
    var items =
            {
                Width: lebar,
                height: 110,
                layout: 'column',
                border: true,
                items:
                        [
                            {
                                columnWidth: .990,
                                layout: 'form',
                                Width: lebar - 10,
                                labelWidth: 130,
                                border: false,
                                items:
                                        [
                                            {
                                                xtype: 'tbspacer',
                                                height: 3
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Jumlah Biaya',
                                                maxLength: 200,
                                                name: 'txtjumlahbiayasalLAB',
                                                id: 'txtjumlahbiayasalLAB',
                                                width: 100,
                                                style: {'text-align': 'right'},
                                                readOnly: true,
                                                anchor: '95%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Paid',
                                                maxLength: 200,
                                                name: 'txtpaidLAB',
                                                id: 'txtpaidLAB',
                                                readOnly: true,
                                                align: 'right',
                                                style: {'text-align': 'right'},
                                                width: 100,
                                                value: 0,
                                                anchor: '95%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Jumlah dipindahkan',
                                                maxLength: 200,
                                                name: 'txtjumlahtranferLAB',
                                                id: 'txtjumlahtranferLAB',
                                                align: 'right',
                                                style: {'text-align': 'right'},
                                                width: 100,
                                                anchor: '95%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Saldo tagihan',
                                                maxLength: 200,
                                                name: 'txtsaldotagihanLAB',
                                                id: 'txtsaldotagihanLAB',
                                                style: {'text-align': 'right'},
                                                readOnly: true,
                                                value: 0,
                                                width: 100,
                                                anchor: '95%'
                                            }
                                        ]
                            }
                        ]
            }
    return items;
}
;

function mComboalasan_transferLAB()
{
    var Field = ['KD_ALASAN', 'ALASAN'];

    var dsalasan_transferLAB = new WebApp.DataStore({fields: Field});
    dsalasan_transferLAB.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'kd_alasan',
                                    Sortdir: 'ASC',
                                    target: 'ComboAlasanTransfer',
                                    param: "" //+"~ )"
                                }
                    }
            );

    var cboalasan_transferLAB = new Ext.form.ComboBox
            (
                    {
                        id: 'cboalasan_transferLAB',
                        typeAhead: false,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        emptyText: '',
                        fieldLabel: ' Alasan Transfer ',
                        align: 'Right',
                        width: 100,
                        editable: false,
                        anchor: '100%',
                        store: dsalasan_transferLAB,
                        valueField: 'ALASAN',
                        displayField: 'ALASAN',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
                                    }

                                }
                    }
            );

    return cboalasan_transferLAB;
};

function getItemPanelInputTransfer_LAB(lebar)
{
    var items =
            {
                layout: 'fit',
                anchor: '100%',
                width: lebar - 35,
                labelAlign: 'right',
                bodyStyle: 'padding:10px 10px 10px 0px',
                border: false,
                height: 330,
                items:
                        [
                            {
                                columnWidth: .9,
                                width: lebar - 35,
                                labelWidth: 100,
                                layout: 'form',
                                height: 330,
                                border: false,
                                items:
                                        [
                                            getTransfertujuan_lab(lebar),
                                            {
                                                xtype: 'tbspacer',
                                                height: 5
                                            },
                                            getItemPanelNoTransksiTransferLAB(lebar),
                                            {
                                                xtype: 'tbspacer',
                                                height: 3
                                            },
                                            mComboalasan_transferLAB()

                                        ]
                            },
                        ]
            };
    return items;
}
;
function showhide_unit(kode)
{
	if(kode==='05')
	{
	Ext.getCmp('txtTranferunitLAB').hide();
	Ext.getCmp('txtTranferkelaskamar_LAB').show();

	}else{
	Ext.getCmp('txtTranferunitLAB').show();
	Ext.getCmp('txtTranferkelaskamar_LAB').hide();
	}
}
function getFormEntryPenjasBayarLAB(lebar)
{
    var pnlTRKasirLAB = new Ext.FormPanel
            (
                    {
                        id: 'PanelTRKasirLAB',
                        fileUpload: true,
                        region: 'north',
                        layout: 'column',
                        bodyStyle: 'padding:10px 10px 10px 10px',
                        height: 130,
                        anchor: '100%',
                        width: lebar,
                        border: false,
						enableKeyEvents:true,
                        listeners:
                                {
									keydown:function(text,e){
										if(e.keyCode == 122){
											e.preventDefault();
											alert('hai');
										}
									}
                                },
                        items: [getItemPanelInputKasirLAB(lebar)],
                        tbar:
                                [
                                ]
                    }
            );
    var x;
    var paneltotalLAB = new Ext.Panel
            (
                    {
                        id: 'paneltotalLAB',
                        region: 'center',
                        border: false,
                        bodyStyle: 'padding:0px 7px 0px 7px',
                        layout: 'column',
                        frame: true,
                        height: 67,
                        anchor: '100% 8.0000%',
                        autoScroll: false,
                        items:
                                [
                                    {
                                        xtype: 'compositefield',
                                        anchor: '100%',
                                        labelSeparator: '',
                                        border: true,
                                        style: {'margin-top': '5px'},
                                        items:
                                                [
                                                    {
                                                        layout: 'form',
                                                        style: {'text-align': 'right', 'margin-left': '370px'},
                                                        border: false,
                                                        html: " Total :"
                                                    },
                                                    {
                                                        xtype: 'textfield',
                                                        id: 'txtJumlah2EditData_viKasirLAB',
                                                        name: 'txtJumlah2EditData_viKasirLAB',
                                                        style: {'text-align': 'right', 'margin-left': '390px'},
                                                        width: 82,
                                                        readOnly: true,
                                                    },
                                                            //-------------- ## --------------
                                                ]
                                    },
                                    {
                                        xtype: 'compositefield',
                                        anchor: '100%',
                                        labelSeparator: '',
                                        border: true,
                                        style: {'margin-top': '5px'},
                                        items:
                                                [
                                                    {
                                                        layout: 'form',
                                                        style: {'text-align': 'right', 'margin-left': '370px'},
                                                        border: false,
                                                        hidden: true,
                                                        html: " Bayar :"
                                                    },
                                                    {
                                                        xtype: 'numberfield',
                                                        id: 'txtJumlahEditData_viKasirLAB',
                                                        name: 'txtJumlahEditData_viKasirLAB',
                                                        hidden: true,
                                                        style: {'text-align': 'right', 'margin-left': '390px'},
                                                        width: 82,
                                                        //readOnly: true,
                                                    }
                                                    //-------------- ## --------------
                                                ]
                                    }
                                ]
                    }
            )
    var GDtabDetailKasirLAB = new Ext.Panel
            (
                    {
                        id: 'GDtabDetailKasirLAB',
                        region: 'center',
                        activeTab: 0,
                        height: 380,
                        anchor: '100% 100%',
                        border: false,
                        plain: true,
                        defaults: {autoScroll: true},
                        items: [GetDTLTRKasirLABGrid(), paneltotalLAB],
                        tbar:
                                [
                                    {
                                        text: 'Bayar',
                                        id: 'btnSimpanKasirLAB',
                                        tooltip: nmSimpan,
                                        iconCls: 'save',
                                        handler: function ()
                                        {
                                            Datasave_KasirLABKasir(false);
                                           // FormDepan2KasirLAB.close();
                                            
                                            //Ext.getCmp('btnHpsBrsKasirLAB').disable();
                                        }
                                    },
									
                                    {
                                        id: 'btnTransferKasirLAB',
                                        text: 'Transfer',
                                        tooltip: nmEditData,
                                        iconCls: 'Edit_Tr',
                                        /* disabled: true, */
                                        handler: function (sm, row, rec)
                                        {
											//Disini
                                            TransferLookUp_lab();
                                            Ext.Ajax.request({
                                                url: baseURL + "index.php/main/GetPasienTranferLab_rad",
                                                params: {
                                                    notr: notransaksiasal_lab,
                                                    notransaksi: Ext.getCmp('txtNoTransaksiKasirLABKasir').getValue(),//tmp_NoTransaksi,
                                                    kdkasir: kdkasirasal_lab
                                                },
                                                success: function (o)
                                                {	
												showhide_unit(kdkasirasal_lab);
                                                    if (o.responseText == "") {
                                                        ShowPesanWarningPenJasLab('Pasien belum terdaftar kedalam unit manapun / pasien sudah pulang', 'WARNING');
                                                    } else {
                                                        var tmphasil = o.responseText;
                                                        var tmp = tmphasil.split("<>");
                                                        if (tmp[5] == undefined && tmp[3] == undefined) {
                                                            ShowPesanWarningPenJasLab('Pasien belum terdaftar kedalam unit manapun / pasien sudah pulang', 'WARNING');
                                                            FormLookUpsdetailTRTransfer_Lab.close();
                                                        } else {
															console.log(tmp[12]);
                                                            Ext.get(txtTranfernoTransaksiLAB).dom.value = tmp[3];
                                                            Ext.get(dtpTanggaltransaksiRujuanLAB).dom.value = ShowDate(tmp[4]);
                                                            Ext.get(txtTranfernomedrecLAB).dom.value = tmp[5];
                                                            Ext.get(txtTranfernamapasienLAB).dom.value = tmp[2];
															//alert(tmp[9]);
                                                            Ext.get(txtTranferunitLAB).dom.value = tmp[9];
                                                            Trkdunit2 = tmp[6];
															Ext.get(txtTranferkelaskamar_LAB).dom.value=tmp[9];
															Ext.get(txtjumlahbiayasalLAB).dom.value=tmp[12];
															Ext.get(txtjumlahtranferLAB).dom.value=tmp[12];
															
															tmp_kodeunitkamar_LAB=tmp[8];
															tmp_kdspesial_LAB=tmp[10];
															tmp_nokamar_LAB=tmp[11];
                                                            var kasir = tmp[7];
                                                            Ext.Ajax.request({
                                                                url: baseURL + "index.php/main/GettotalTranfer",
                                                                params: {
                                                                    notr: notransaksi,
																	kd_kasir:kdkasir
                                                                },
                                                                success: function (o)
                                                                {
																	console.log(o);
                                                                    Ext.get(txtjumlahbiayasalLAB).dom.value = formatCurrency(o.responseText);
                                                                    Ext.get(txtjumlahtranferLAB).dom.value = formatCurrency(o.responseText);
																	/* Ext.get(txtjumlahbiayasalLAB).dom.value=tmp[12];
																	Ext.get(txtjumlahtranferLAB).dom.value=tmp[12]; */
                                                                }
                                                            });
                                                        }

                                                    }
                                                }
                                            });

                                        }, //disabled :true
                                    },
                                    mCombo_printer_kasirlab(),
                                    {
                                        xtype: 'splitbutton',
                                        text: 'Cetak',
                                        iconCls: 'print',
                                        id: 'btnPrint_viDaftar',
                                        handler: function ()
                                        {
                                        },
                                        menu: new Ext.menu.Menu({
                                            items:
                                                    [
                                                        {
                                                            xtype: 'button',
                                                            text: 'Print Bill',
                                                            id: 'btnPrintBillRadLab',
                                                            handler: function ()
                                                            {
																/* if (Ext.getCmp('cbopasienorder_printer_kasirlab').getValue()==='' || Ext.getCmp('cbopasienorder_printer_kasirlab').getValue()===undefined){
																	ShowPesanWarningPenJasLab('Pilih printer terlebih dahulu !', 'Cetak Data');
																}else{ */
																	printbillRadLab();
																//}
                                                            }
                                                        },
                                                        {
                                                            xtype: 'button',
                                                            text: 'Print Kwitansi',
                                                            id: 'btnPrintKwitansiRadLab',
                                                            handler: function ()
                                                            {
																/* if (Ext.getCmp('cbopasienorder_printer_kasirlab').getValue()==='' || Ext.getCmp('cbopasienorder_printer_kasirlab').getValue()===undefined){
																	ShowPesanWarningPenJasLab('Pilih printer terlebih dahulu !', 'Cetak Data');
																}else{ */
																	printkwitansiRadLab();
																//}
                                                            }
                                                        }
                                                    ]
                                        })
                                    }
                                ]
                    }
            );



    var pnlTRKasirLAB2 = new Ext.FormPanel
            (
                    {
                        id: 'PanelTRKasirLAB2',
                        fileUpload: true,
                        region: 'north',
                        layout: 'column',
                        bodyStyle: 'padding:10px 10px 10px 10px',
                        height: 380,
                        anchor: '100%',
                        width: lebar,
                        border: false,
                        items: [GDtabDetailKasirLAB,
                        ]
                    }
            );




    FormDepan2KasirLAB = new Ext.Panel
            (
                    {
                        id: 'FormDepan2KasirLAB',
                        region: 'center',
                        width: '100%',
                        anchor: '100%',
                        layout: 'form',
                        title: '',
                        bodyStyle: 'padding:15px',
                        border: true,
                        bodyStyle: 'background:#FFFFFF;',
                                height: 380,
                        shadhow: true,
                        items: [pnlTRKasirLAB, pnlTRKasirLAB2,
                        ]
                    }
            );

    return FormDepan2KasirLAB
}
function paramUpdateTransaksi()
{
    var params =
            {
                TrKodeTranskasi: Ext.get('txtNoTransaksiPenJasLab').dom.value,
                KDkasir: kdkasir
            };
    return params
}
;
function UpdateTutuptransaksi(mBol)
{
    Ext.Ajax.request
            (
                    {
                        //url: "./Datapool.mvc/CreateDataObj",
                        url: baseURL + "index.php/main/functionKasirPenunjang/ubah_co_status_transksi",
                        params: paramUpdateTransaksi(),
                        failure: function (o)
                        {
                            ShowPesanInfoPenJasLab('Data gagal tersimpan segera hubungi Admin', 'Gagal');
                            validasiJenisTrLAB();
                        },
                        success: function (o)
                        {
                            //RefreshDatahistoribayar_LAB(Ext.get('cellSelectedtutup);

                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
                                ShowPesanInfoPenJasLab(nmPesanSimpanSukses, nmHeaderSimpanData);

                                //RefreshDataKasirLABKasir();
                                if (mBol === false)
                                {
                                    validasiJenisTrLAB();
                                }
                                ;
                                cellSelecteddeskripsi = '';
                            } else
                            {
                                ShowPesanInfoPenJasLab('Data gagal tersimpan segera hubungi Admin', 'Gagal');
                                cellSelecteddeskripsi = '';
                            }
                            ;
                        }
                    }
            )
}
;
function RecordBaruLAB()
{
	var TanggalTransaksiGrid;
	//alert(TglTransaksi);
	/* if (TglTransaksi === undefined)
	{
		TanggalTransaksiGrid=tglGridBawah;
	}else{
		TanggalTransaksiGrid=TglTransaksi;
	} */
	var p = new mRecordLAB
	(
		{
			'cito':'',
			'kd_tarif':'',
			'kd_produk':'',
			'kp_produk':'',
			'deskripsi':'',
			'tgl_transaksi':tglGridBawah,
			'tgl_berlaku':null,
			'harga':'',
			'qty':'',
			'jumlah':''
			
		}
	);
	
	return p;
};
function TambahBarisLAB()
{
    var x=true;

    if (x === true)
    {
        var p = RecordBaruLAB();
        dsTRDetailPenJasLabList.insert(dsTRDetailPenJasLabList.getCount(), p);
    };
};
function getFormEntryPenJasLab(lebar,data) 
{
    var pnlTRLAB = new Ext.FormPanel
    (
        {
            id: 'PanelTRPenJasLab',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding: 5px 5px 5px 5px',
		    height:255,
            width: lebar,
            border: false,
            items: [getItemPanelPenJasLab(lebar)],
            tbar:
            [
				{
                        text: 'Tambah Baru',
                        id: 'btnTambahBaruLAB',
                        tooltip: nmSimpan,
                        iconCls: 'add',
                        handler: function()
						{
							LABAddNew();
							ViewGridBawahLookupPenjasLab();
							Ext.getCmp('txtNamaPasienLAB').focus();                 
							Ext.getCmp('txtnotlplab').setReadOnly(false);                 
						}
				},  
				'-',
				{
                        text: 'Simpan',
                        id: 'btnSimpanLAB',
                        tooltip: nmSimpan,
                        iconCls: 'save',
                        handler: function()
						{
							loadMask.show();
							Datasave_PenJasLab(false); 
							loadMask.hide();						
                            // Datasave_PenJasLab_SQL();                                  

						}
				},  
				
				'-',
				{
					id: 'btnPembayaranPenjasLAB',
					text: 'Pembayaran',
					tooltip: nmEditData,
					iconCls: 'Edit_Tr',
					handler: function (sm, row, rec)
					{
						if (rowSelectedPenJasLab != undefined) {
							PenjasLookUpLAB(rowSelectedPenJasLab.data);
						 } else {
							 PenjasLookUpLAB();
							//ShowPesanWarningPenJasLab('Pilih data tabel  ', 'Pembayaran');
						} 
					}//, disabled: true
				}, '-',
				{
					text: 'Tutup Transaksi',
					id: 'btnTutupTransaksiPenjasLAB',
					tooltip: nmHapus,
					iconCls: 'remove',
					handler: function ()
					{
						if (noTransaksiPilihan == '' || noTransaksiPilihan == 'undefined') {
							ShowPesanWarningPenJasLab('Pilih data tabel  ', 'Pembayaran');
						} else {
							UpdateTutuptransaksi(false);
							Ext.getCmp('btnPembayaranPenjasLAB').disable();
							Ext.getCmp('btnTutupTransaksiPenjasLAB').disable();
							Ext.getCmp('btnHpsBrsItemLab').disable();
						}
					}//, disabled: true
				}	
            ]
        }
    );
 var x;
 var GDtabDetailLAB = new Ext.TabPanel   
    (
        {
        id:'GDtabDetailLAB',
        region: 'center',
        activeTab: 0,
		height:250,
		width:855,
		//anchor: '100%',
        border:true,
        plain: true,
        defaults:
		{
			autoScroll: true
		},
		items: [GridDetailItemPemeriksaan(),GetDTLTRHistoryGrid()],
        
			listeners:
			{   
				'tabchange' : function (panel, tab) {
					if (x ==1)
					{
						Ext.getCmp('btnLookupPenJasLab').hide()
						Ext.getCmp('btnSimpanLAB').hide()
						Ext.getCmp('btnHpsBrsItemLab').hide()
						x=2;
						return x;
					}else 
					{ 	
						Ext.getCmp('btnLookupPenJasLab').show()
						Ext.getCmp('btnSimpanLAB').hide()
						Ext.getCmp('btnHpsBrsItemLab').show()
						x=1;	
						return x;
					}

				}
			}
        }
		
    );
	
   
   
   var pnlTRLAB2 = new Ext.FormPanel
    (
        {
            id: 'PanelTRPenJasLab2',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:460,
            //anchor: '100%',
            width: lebar,
            border: false,
            items: [	GDtabDetailLAB
			
			]
        }
    );

	
   
   
    var FormDepanPenJasLab = new Ext.Panel
	(
		{
		    id: 'FormDepanPenJasLab',
		    region: 'center',
		    width: '100%',
		    anchor: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: true,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: 
			[
				pnlTRLAB,pnlTRLAB2	
			]

		}
	);
    return FormDepanPenJasLab
};

function DataDeletePenJasRadDetail()
{
    Ext.Ajax.request
    (
        {
                //url: "./Datapool.mvc/DeleteDataObj",
            url: baseURL + "index.php/main/DeleteDataObj",
            params:  getParamDataDeletePenJasRadDetail(),
            success: function(o)
            {
                var cst = Ext.decode(o.responseText);
                if (cst.success === true)
                {
                    ShowPesanInfoPenJasLab(nmPesanHapusSukses,nmHeaderHapusData);
                    dsTRDetailPenJasLabList.removeAt(CurrentPenJasLab.row);
                    cellSelecteddeskripsi=undefined;
                    ViewGridBawahLookupPenjasLab(Ext.get('txtNoTransaksiPenJasLab').dom.value,kd_kasir_lab);
                    AddNewPenJasRad = false;
                }
                else if (cst.success === false && cst.pesan === 0 )
                {
                    ShowPesanWarningPenJasLab(nmPesanHapusGagal, nmHeaderHapusData);
                }
                else
                {
                    ShowPesanWarningPenJasLab(nmPesanHapusError,nmHeaderHapusData);
                };
            }
        }
    )
};

function getParamDataDeletePenJasRadDetail()
{
    var params =
    {//^^^
		Table: 'ViewDetailTransaksiPenJasLab',
        TrKodeTranskasi: CurrentPenJasLab.data.data.NO_TRANSAKSI,
		TrTglTransaksi:  CurrentPenJasLab.data.data.TGL_TRANSAKSI,
		TrKdPasien :	 CurrentPenJasLab.data.data.KD_PASIEN,
		TrKdNamaPasien : Ext.get('txtNamaPasienLAB').getValue(),
		//TrAlamatPasien : Ext.get('txtAlamatLAB').getValue(),	
		TrKdUnit :		 Ext.get('txtKdUnitLab').getValue(),
		TrNamaUnit :	 Ext.get('txtNamaUnitLAB').getValue(),
		Uraian :		 CurrentPenJasLab.data.data.DESKRIPSI2,
		AlasanHapus : 	 variablehistori,
		TrHarga :		 CurrentPenJasLab.data.data.HARGA,
		
		TrKdProduk :	 CurrentPenJasLab.data.data.KD_PRODUK,
        RowReq: CurrentPenJasLab.data.data.URUT,
        Hapus:2
    };
	
    return params
};

function getParamDataupdatePenJasRadDetail()
{
    var params =
    {
        Table: 'ViewDetailTransaksiPenJasLab',
        TrKodeTranskasi: CurrentPenJasLab.data.data.NO_TRANSAKSI,
        RowReq: CurrentPenJasLab.data.data.URUT,

        Qty: CurrentPenJasLab.data.data.QTY,
        Ubah:1
    };
	
    return params
};

function GridDetailItemPemeriksaan() 
{
    var fldDetailLAB = ['kp_produk','kd_produk','deskripsi','deskripsi2','kd_tarif','harga','qty','desc_req','tgl_berlaku','no_transaksi','urut','desc_status','tgl_transaksi','kd_unit','kd_kasir'];
	
    dsTRDetailPenJasLabList = new WebApp.DataStore({ fields: fldDetailLAB })
	//ViewGridBawahLookupPenjasLab() ;
    gridDTItemTest = new Ext.grid.EditorGridPanel
    (
        {
            title: 'Item Test',
			id: 'gridDTItemTest',
            stripeRows: true,
            store: dsTRDetailPenJasLabList,
            border: false,
            columnLines: true,
            frame: false,
			width:50,
			height:100,
            //anchor: '100%',
            autoScroll:true,
			viewConfig: {forceFit: true},
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                            cellSelecteddeskripsi = dsTRDetailPenJasLabList.getAt(row);
                            CurrentPenJasLab.row = row;
                            CurrentPenJasLab.data = cellSelecteddeskripsi;
                            // console.log(cellSelecteddeskripsi);
                        }
                    }
                }
            ),
			
            cm: TRPenJasLabColumModel(),
			tbar:
			[
				{
					text: 'Tambah Baris',
					id: 'btnTambahBarisPenJasLab',
					tooltip: nmLookup,
					iconCls: 'Edit_Tr',
					handler: function()
					{
						TambahBarisLAB()
						
					}
				},{
					text: 'Tambah Item Pemeriksaan (Lookup)',
					id: 'btnLookupPenJasLab',
					tooltip: nmLookup,
					iconCls: 'find',
					handler: function()
					{
						getTindakanLab();
						setLookUp_getTindakanPenjasLab();
						
					}
				},
				{
						id:'btnHpsBrsItemLab',
						text: 'Hapus Baris',
						tooltip: 'Hapus Baris',
						disabled:false,
						iconCls: 'RemoveRow',
						handler: function()
						{
							if (dsTRDetailPenJasLabList.getCount() > 0 ) {
								if (cellSelecteddeskripsi != undefined)
								{
									Ext.Msg.confirm('Warning', 'Apakah item pemeriksaan ini akan dihapus?', function(button){
										if (button == 'yes'){
											if(CurrentPenJasLab != undefined) {
												loadMask.show();
												var line = gridDTItemTest.getSelectionModel().selection.cell[0];
												Ext.Ajax.request
												 (
													{
														url: baseURL + "index.php/main/functionLAB/deletedetaillab",
														params: getParamHapusDetailTransaksiLAB(),
														failure: function(o)
														{
															ShowPesanWarningPenJasLab('Error simpan. Hubungi Admin!', 'Gagal');
															loadMask.hide();
														},	
														success: function(o) 
														{
															loadMask.hide();
															var cst = Ext.decode(o.responseText);
															if (cst.success === true && cst.cari===false) 
															{
																
																ShowPesanInfoPenJasLab('Data Berhasil Di hapus', 'Sukses');
																dsTRDetailPenJasLabList.removeAt(line);
																ViewGridBawahLookupPenjasLab(Ext.get('txtNoTransaksiPenJasLab').dom.value,kd_kasir_lab);
																gridDTItemTest.getView().refresh();
															}else if (cst.success === true && cst.cari===true)
															{
																dsTRDetailPenJasLabList.removeAt(line);
															}
															else 
															{
																	ShowPesanWarningPenJasLab('Data Tidak Bisa Di Hapus Harap Hubungi Admin', 'Gagal');
															};
														}
													}
												)
												
											}
										}
									})
								} else {
									ShowPesanWarningPenJasLab('Pilih record ','Hapus data');
								}
							}
						}
				},
						
			],
        }
		
		
    );
	
	

    return gridDTItemTest;
};

function TRPenJasLabColumModel()
{

	checkColumn_penata__lab = new Ext.grid.CheckColumn({
	   header: 'Cito',
	   dataIndex: 'cito',
	   id: 'checkid',
	   width: 55,
	   renderer: function(value)
	   {
        console.log("Grid");
		},
	});
    return new Ext.grid.ColumnModel
    (
        [ 
            new Ext.grid.RowNumberer(),
			{
				header: 'Cito',
                dataIndex: 'cito',
                width:65,
				menuDisabled:true,
				listeners:{
					itemclick: function(dv, record, items, index, e)
					{
									Ext.getCmp('btnHpsBrsItemLabL').enable();
									console.log("test");
					},
				},
				renderer:function (v, metaData, record)
				{
					if ( record.data.cito=='0')
					{
					record.data.cito='Tidak'
					}else if (record.data.cito=='1')
					{
					metaData.style  ='background:#FF0000;  "font-weight":"bold";';
					record.data.cito='Ya'
					}else if (record.data.cito=='Ya')
					{
					metaData.style  ='background:#FF0000;  "font-weight":"bold";';
					}
					
					return record.data.cito; 
				},
				editor:new Ext.form.ComboBox
				({
					id: 'cboKasus',
					typeAhead: true,
					triggerAction: 'all',
					lazyRender: true,
					mode: 'local',
					selectOnFocus: true,
					forceSelection: true,
					emptyText: 'Silahkan Pilih...',
					width: 50,
					anchor: '95%',
					value: 1,
					store: new Ext.data.ArrayStore({
						id: 0,
						fields: ['Id', 'displayText'],
						data: [[1, 'Ya'], [2, 'Tidak']]
					}),
					valueField: 'displayText',
					displayField: 'displayText',
					value		: '',
					   
				})
			},
			{
                header: 'kd_tarif',
                dataIndex: 'kd_tarif',
                width:250,
				menuDisabled:true,
				hidden :true

            },
            {
                header: 'KD Produk',
                dataIndex: 'kd_produk',
                width:100,
                menuDisabled:true,
                hidden:true
            },{
                header: 'Kode Produk',
                dataIndex: 'kp_produk',
                width:100,
                menuDisabled:true,
                //hidden:true,
				editor 		: new Ext.form.TextField({
					id 					: 'fieldcolKdProduk',
					allowBlank 			: true,
					enableKeyEvents  	: true,
					width 				: 30,
					listeners 			:
					{ 
						'specialkey' : function(a, b)
						{
							if (b.getKey() === b.ENTER || b.getKey() === b.TAB) {
								var kd_cus_gettarif=vkode_customer;
								/* if(Ext.get('cboKelPasienLab').getValue()=='Perseorangan'){
									kd_cus_gettarif=Ext.getCmp('cboPerseoranganLab').getValue();
								}else if(Ext.get('cboKelPasienLab').getValue()=='Perusahaan'){
									kd_cus_gettarif=Ext.getCmp('cboPerusahaanRequestEntryLab').getValue();
								}else {
									kd_cus_gettarif=Ext.getCmp('cboAsuransiLab').getValue();
								  } */
								var modul='';
								if(radiovaluesLAB == 1){
									modul='igd';
								} else if(radiovaluesLAB == 2){
									modul='rwi';
								} else if(radiovaluesLAB == 4){
									modul='rwj';
								} else{
									modul='langsung';
								}
								Ext.Ajax.request
								({
									store	: KasirLABDataStoreProduk,
									url 	: baseURL + "index.php/main/functionLAB/getGridProdukKey",
									params 	:  {
										kd_unit:kodeUnitLab,
									 kd_customer:kd_cus_gettarif,
									 notrans:Ext.getCmp('txtNoTransaksiPenJasLab').getValue(),
									 penjas:modul,
									 kdunittujuan:tmpkd_unit,
										text 			: Ext.getCmp('fieldcolKdProduk').getValue(),
									},
									failure : function(o)
									{
										ShowPesanWarningPenJasLab("Data produk tidak ada!",'WARNING');
									},
									success : function(o)
									{
										var cst = Ext.decode(o.responseText);
										if (cst.processResult == 'SUCCESS'){
											kd_produk       	= cst.listData.kd_produk;
											kp_produk       	= cst.listData.kp_produk;
											kd_tarif        	= cst.listData.kd_tarif;
											deskripsi 			= cst.listData.deskripsi;
											harga				= cst.listData.harga;
											tgl_berlaku			= cst.listData.tgl_berlaku;
											jumlah				= cst.listData.jumlah;
											var line = gridDTItemTest.getSelectionModel().selection.cell[0];
											
											/* dsTRDetailPenJasLabList.getRange()[CurrentPenJasLab.row].set('deskripsi', cst.listData.deskripsi);
											dsTRDetailPenJasLabList.getRange()[CurrentPenJasLab.row].set('qty', 1); */
											//alert(dsTRDetailPenJasLabList.data.items[line].data.deskripsi);
											dsTRDetailPenJasLabList.data.items[line].data.kd_produk         = cst.listData.kd_produk;
											dsTRDetailPenJasLabList.data.items[line].data.kp_produk         = cst.listData.kp_produk;
											dsTRDetailPenJasLabList.data.items[line].data.kd_klas           = cst.listData.kd_klas;
											dsTRDetailPenJasLabList.data.items[line].data.kd_unit           = cst.listData.kd_unit;
											dsTRDetailPenJasLabList.data.items[line].data.harga             = cst.listData.harga;
											dsTRDetailPenJasLabList.data.items[line].data.deskripsi         = cst.listData.deskripsi;
											console.log(dsTRDetailPenJasLabList.data.items[line]);
											dsTRDetailPenJasLabList.data.items[line].data.tgl_berlaku       = cst.listData.tgl_berlaku;
											dsTRDetailPenJasLabList.data.items[line].data.kd_tarif          = cst.listData.kd_tarif;
											//dsTRDetailPenJasLabList.data.items[line].data.tgl_transaksi     = Tgltransaksi;
											dsTRDetailPenJasLabList.data.items[line].data.qty               = '1';
											
											gridDTItemTest.getView().refresh();
											gridDTItemTest.startEditing(line, 9);
											
										}else{
											ShowPesanWarningPenJasLab('Data produk tidak ada!','WARNING');
										};
									}
								})
							}
						},
					}
				}), 
            },
            /* {
                header:'Nama Produk',
                dataIndex: 'deskripsi',
                sortable: false,
                hidden:false,
                menuDisabled:true,
                width:320

            }, */
			{	id:'nama_pemereksaaan_lab',
				dataIndex: 'deskripsi',
				header: 'Nama Pemeriksaan',
				sortable: true,
				menuDisabled:true,
				width: 320,
				/* editor:new Nci.form.Combobox.autoComplete({
					store	: TrPenJasLab.form.ArrayStore.produk,
					select	: function(a,b,c){
					//	console.log(b);
					//Disini
						Ext.Ajax.request
						(
							{
								url: baseURL + "index.php/main/functionLAB/cekProduk",
								params:{kd_lab:b.data.kd_produk} ,
								failure: function(o)
								{
									ShowPesanErrorPenJasLab('Hubungi Admin', 'Error');
								},
								success: function(o)
								{
									Ext.getCmp('btnHpsBrsItemLabL').enable();
									console.log("test");
									var cst = Ext.decode(o.responseText);
									if (cst.success === true)
									{
										var line = gridDTItemTest.getSelectionModel().selection.cell[0];
										dsTRDetailPenJasLabList.data.items[line].data.deskripsi=b.data.deskripsi;
										dsTRDetailPenJasLabList.data.items[line].data.uraian=b.data.uraian;
										dsTRDetailPenJasLabList.data.items[line].data.kd_tarif=b.data.kd_tarif;
										dsTRDetailPenJasLabList.data.items[line].data.kd_produk=b.data.kd_produk;
										dsTRDetailPenJasLabList.data.items[line].data.tgl_transaksi=b.data.tgl_transaksi;
										dsTRDetailPenJasLabList.data.items[line].data.tgl_berlaku=b.data.tgl_berlaku;
										dsTRDetailPenJasLabList.data.items[line].data.harga=b.data.harga;
										dsTRDetailPenJasLabList.data.items[line].data.qty=b.data.qty;
										dsTRDetailPenJasLabList.data.items[line].data.jumlah=b.data.jumlah;

										gridDTItemTest.getView().refresh();
									}
									else
									{
										ShowPesanInfoPenJasLab('Nilai normal item '+b.data.deskripsi+' belum tersedia', 'Information');
									};
								}
							}

						)
						
					},
					insert	: function(o){
						return {
							uraian        	: o.uraian,
							kd_tarif 		: o.kd_tarif,
							kd_produk		: o.kd_produk,
							tgl_transaksi	: o.tgl_transaksi,
							tgl_berlaku		: o.tgl_berlaku,
							harga			: o.harga,
							qty				: o.qty,
							deskripsi		: o.deskripsi,
							jumlah			: o.jumlah,
							text			:  '<table style="font-size: 11px;"><tr><td width="60">'+o.kd_produk+'</td><td width="150">'+o.deskripsi+'</td></tr></table>'
						}
					},
					param	: function(){
					var kd_cus_gettarif;
					if(Ext.get('cboKelPasienLab').getValue()=='Perseorangan'){
						kd_cus_gettarif=Ext.getCmp('cboPerseoranganLab').getValue();
					}else if(Ext.get('cboKelPasienLab').getValue()=='Perusahaan'){
						kd_cus_gettarif=Ext.getCmp('cboPerusahaanRequestEntryLab').getValue();
					}else {
						kd_cus_gettarif=Ext.getCmp('cboAsuransiLab').getValue();
						  }
					var params={};
					params['kd_unit']=kodeUnitLab;
					params['kd_customer']=kd_cus_gettarif;
					return params;
					},
					url		: baseURL + "index.php/main/functionLAB/getProduk",
					valueField: 'deskripsi',
					displayField: 'text',
					listWidth: 210
				}) */
			},
            {
				header: 'Tanggal Transaksi',
				dataIndex: 'tgl_transaksi',
				width: 130,
				menuDisabled:true,
				renderer: function(v, params, record)
				{
					if(record.data.tgl_transaksi == undefined){
						record.data.tgl_transaksi=tglGridBawah;
						return record.data.tgl_transaksi;
					} else{
						if(record.data.tgl_transaksi.substring(5, 4) == '-'){
							return ShowDate(record.data.tgl_transaksi);
						} else{
							var tgl=record.data.tgl_transaksi.split("/");

							if(tgl[2].length == 4 && isNaN(tgl[1])){
								return record.data.tgl_transaksi;
							} else{
								return ShowDate(record.data.tgl_transaksi);
							}
						}

					}
				}
            },
			{
				header: 'Tanggal Berlaku',
				dataIndex: 'tgl_berlaku',
				width: 130,
				menuDisabled:true,
				//hidden: true,
				renderer: function(v, params, record)
				{
					if(record.data.tgl_berlaku == undefined || record.data.tgl_berlaku == null){
						record.data.tgl_berlaku=tglGridBawah;
						return record.data.tgl_berlaku;
					} else{
						if(record.data.tgl_berlaku.substring(5, 4) == '-'){
							return ShowDate(record.data.tgl_berlaku);
						} else{
							var tglb=record.data.tgl_berlaku.split("-");

							if(tglb[2].length == 4 && isNaN(tglb[1])){
								return record.data.tgl_berlaku;
							} else{
								return ShowDate(record.data.tgl_berlaku);
							}
						}

					} 
				}
            },
            {
                header: 'Harga',
                align: 'right',
                hidden: false,
                menuDisabled:true,
                dataIndex: 'harga',
                width:100,
				renderer: function(v, params, record)
				{
					return formatCurrency(record.data.harga);
				}
            },
            {	id:'qty_lab',
                header: 'Qty',
                width:91,
				align: 'right',
				menuDisabled:true,
                dataIndex: 'qty',
				editor 		: new Ext.form.TextField
                (
                    {
                        id:'fieldcolProblemLAB',
                        allowBlank: true,
                        enableKeyEvents : true,
                        width:30,
						listeners:
						{ 
							'specialkey' : function(a, b)
							{
								if (b.getKey() === b.ENTER || b.getKey() === b.TAB) {
									var line = gridDTItemTest.getSelectionModel().selection.cell[0];
									TambahBarisLAB();
									gridDTItemTest.startEditing(line+1, 4);
									loadMask.show();
									Datasave_PenJasLab_LangsungTambahBaris(false); 
									loadMask.hide();	
								}
							},
						}
                    }
                )
                   /*  {
                        id:'fieldcolProblemRWJ',
                        allowBlank: true,
                        enableKeyEvents : true,
                        width:30,
						listeners:
						{
							'specialkey' : function()
							{
								Dataupdate_PenJasRad(false);
							}
						}
                    } */
              //  ),



            },
			 {
                header: 'Dokter',
                width:91,
				menuDisabled:true,
                dataIndex: 'jumlah',
				hidden: true,

            },

        ]
    )
};

/* function TRPenJasLabColumModel() 
{
    return new Ext.grid.ColumnModel
    (
        [
            new Ext.grid.RowNumberer(),
            {
                id: 'coleskripsirwj',
                header: 'Uraian',
                dataIndex: 'DESKRIPSI2',
                width:250,
				menuDisabled:true,
				hidden :true
                
            },
            {
                id: 'colKdProduk',
                header: 'Kode Produk',
                dataIndex: 'KD_PRODUK',
                width:100,
                menuDisabled:true,
                hidden:true
            },
            {
                id: 'colDeskripsiRWJ',
                header:'Nama Produk',
                dataIndex: 'DESKRIPSI',
                sortable: false,
                hidden:false,
                menuDisabled:true,
                width:320
                
            }
			,
            {
               header: 'Tanggal Transaksi',
               dataIndex: 'TGL_TRANSAKSI',
               width: 130,
               menuDisabled:true,
               renderer: function(v, params, record)
                    {
                        return ShowDate(record.data.TGL_TRANSAKSI);
                    }
            },
            {
                id: 'colHARGARWj',
                header: 'Harga',
                align: 'right',
                hidden: true,
                menuDisabled:true,
                dataIndex: 'HARGA',
                width:100,
				renderer: function(v, params, record) 
							{
							return formatCurrency(record.data.HARGA);
							
							}	
            },
            {
                id: 'colProblemRWJ',
                header: 'Qty',
                width:91,
				align: 'right',
				menuDisabled:true,
                dataIndex: 'QTY',
                editor: new Ext.form.TextField
                (
                    {
                        id:'fieldcolProblemRWJ',
                        allowBlank: true,
                        enableKeyEvents : true,
                        width:30,
						listeners:
							{ 
								'specialkey' : function()
								{
									
											Dataupdate_PenJasRad(false);

								}
							}
                    }
                ),
				
				
              
            },

            {
                id: 'colImpactRWJ',
                header: 'CR',
                width:80,
                dataIndex: 'IMPACT',
				hidden: true,
                editor: new Ext.form.TextField
                (
                        {
                                id:'fieldcolImpactRWJ',
                                allowBlank: true,
                                enableKeyEvents : true,
                                width:30
                        }
                )
				
            }

        ]
    )
}; */


function RecordBaruLAB()
{
	var tgltranstoday = Ext.getCmp('dtpKunjunganLAB').getValue();
	var p = new mRecordLAB
	(
		{
			'DESKRIPSI2':'',
			'KD_PRODUK':'',
		    'DESKRIPSI':'', 
		    'KD_TARIF':'', 
		    'HARGA':'',
		    'QTY':'',
		    'TGL_TRANSAKSI':tgltranstoday.format('Y/m/d'), 
		    'DESC_REQ':'',
		    'KD_TARIF':'',
		    'URUT':''
		}
	);
	
	return p;
};
var kd_kasir_lab;
//----------MEMASUKAN DATA YG DIPILIH DARI DATA GRID KEDALAM LOOKUP EDIT DATA PASIEN
function TRLABInit(rowdata)
{
	Ext.getCmp('btnTambahBaruLAB').hide();
    tmpurut = rowdata.URUT;
    AddNewPenJasRad = false;
	Ext.getCmp('dtpKunjunganLAB').setReadOnly(false);
	if(Ext.get('cboJenisTr_viPenJasLab').getValue()=='Transaksi Lama'){
		Ext.get('txtNoTransaksiPenJasLab').dom.value = rowdata.NO_TRANSAKSI;
		//-------22-02-2017
		Ext.getCmp('cboUnitLab_viPenJasLab').disable();
	}
	else
	{
		//-------22-02-2017
		Ext.getCmp('cboUnitLab_viPenJasLab').enable();
	}
	console.log(rowdata);
	TmpNotransaksi=rowdata.NO_TRANSAKSI;
	KdKasirAsal=rowdata.KD_KASIR;
	TglTransaksi=rowdata.TGL_TRANSAKSI;
	
	Kd_Spesial=rowdata.KD_SPESIAL;
	No_Kamar=rowdata.KAMAR;
	kodeunit=rowdata.KD_UNIT;
	if (combovalues==='Transaksi Lama' )
	{
		ViewGridBawahLookupPenjasLab(rowdata.NO_TRANSAKSI,rowdata.KD_KASIR);
		kdkasir=rowdata.KD_KASIR;
		//alert(kdkasir);
		Ext.getCmp('btnPembayaranPenjasLAB').enable();
		Ext.getCmp('btnTutupTransaksiPenjasLAB').enable();
		Ext.getCmp('btnHpsBrsKasirLAB').enable();
		RefreshDatahistoribayar_LAB(rowdata.NO_TRANSAKSI);
		tglTransaksiAwalPembandingTransfer=rowdata.TGL_TRANSAKSI;
		
		kodeUnitLab=rowdata.KD_UNIT_ASAL
		
		if (rowdata.LUNAS==='t')
		{
			//Ext.getCmp('btnPembayaranPenjasLAB').disable();
			//Ext.getCmp('btnHpsBrsKasirLAB').disable();
			Ext.getCmp('btnTutupTransaksiPenjasLAB').disable();
		}
	}
	else
	{
		Ext.getCmp('btnPembayaranPenjasLAB').disable();
		Ext.getCmp('btnHpsBrsKasirLAB').disable();
		Ext.getCmp('btnTutupTransaksiPenjasLAB').disable();
		kodeUnitLab=rowdata.KD_UNIT;
		tglTransaksiAwalPembandingTransfer=nowTglTransaksi.format('Y-m-d')+" 00:00:00";
	}
	var modul='';
	if(radiovaluesLAB == 1){
		modul='igd';
	} else if(radiovaluesLAB == 2){
		modul='rwi';
	} else if(radiovaluesLAB == 4){
		modul='rwj';
	} else{
		modul='langsung';
	}
	Ext.Ajax.request({
        url: baseURL + "index.php/main/functionLAB/cekPembayaran",
        params: {
            notrans: rowdata.NO_TRANSAKSI,
			Modul:modul,
			kdkasir: rowdata.KD_KASIR
        },
        failure: function (o)
        {
            var cst = Ext.decode(o.responseText);
        },
        success: function (o) {
			var cst = Ext.decode(o.responseText);
			console.log(cst.ListDataObj);
			kodepasien = rowdata.KD_PASIEN;
			namapasien = rowdata.NAMA;
			namaunit = rowdata.NAMA_UNIT;
			kodepay = cst.ListDataObj[0].kd_pay;
			uraianpay = cst.ListDataObj[0].cara_bayar;
			kdkasir = cst.ListDataObj[0].kd_kasir;
			kdcustomeraa = rowdata.KD_CUSTOMER;
			/* loaddatastorePembayaran(cst.ListDataObj[0].jenis_pay);
			Ext.get('cboPembayaran').dom.value =cst.ListDataObj[0].ket_payment;
			Ext.get('cboJenisByr').dom.value =cst.ListDataObj[0].cara_bayar; 
			vflag = cst.ListDataObj[0].flag;
			tapungkd_pay = cst.ListDataObj[0].kd_pay; */
			
        }

    });
	
	Ext.get('dtpTtlLAB').dom.value=ShowDate(rowdata.TGL_LAHIR);
	var tahunLahir= ShowDate(rowdata.TGL_LAHIR);
	var tahunLahirSplit= tahunLahir.split('/');
	var tahunSekarang=nowTglTransaksiGrid.format("Y");
	var selisihTahunLahir=tahunSekarang-tahunLahirSplit[2];
	
	Ext.get('txtUmurLab').dom.value=selisihTahunLahir;
	Ext.get('txtNoMedrecLAB').dom.value= rowdata.KD_PASIEN;
	Ext.get('txtNamaPasienLAB').dom.value = rowdata.NAMA;
	Ext.get('txtAlamatLAB').dom.value = rowdata.ALAMAT;
	Ext.get('txtKdDokter').dom.value   = rowdata.KD_DOKTER;
	
	// if (rowdata.NAMA_UNIT_ASLI==='')
	// {
	// 	Ext.get('txtNamaUnitLAB').dom.value = rowdata.NAMA_UNIT;
	// }
	// else
	// {
	// 	Ext.get('txtNamaUnitLAB').dom.value = rowdata.NAMA_UNIT_ASAL;
	// }
	Ext.get('txtKdUrutMasuk').dom.value = rowdata.URUT_MASUK;
	
	jenisKelaminPasien=rowdata.JENIS_KELAMIN;		
	Ext.get('cboJKLAB').dom.value = rowdata.JENIS_KELAMIN;
	if (rowdata.JENIS_KELAMIN === 't')
	{
		Ext.get('cboJKLAB').dom.value= 'Laki-laki';
		
	}else
	{
		Ext.get('cboJKLAB').dom.value= 'Perempuan'
		
	}
	Ext.getCmp('cboJKLAB').disable();
	
	Ext.get('cboGDRLAB').dom.value = rowdata.GOL_DARAH;
	if (rowdata.GOL_DARAH === '0')
	{
		Ext.get('cboGDRLAB').dom.value= '-';
	}else if (rowdata.GOL_DARAH === '1')
	{
		Ext.get('cboGDRLAB').dom.value= 'A+'
	}else if (rowdata.GOL_DARAH === '2')
	{
		Ext.get('cboGDRLAB').dom.value= 'B+'
	}else if (rowdata.GOL_DARAH === '3')
	{
		Ext.get('cboGDRLAB').dom.value= 'AB+'
	}else if (rowdata.GOL_DARAH === '4')
	{
		Ext.get('cboGDRLAB').dom.value= 'O+'
	}else if (rowdata.GOL_DARAH === '5')
	{
		Ext.get('cboGDRLAB').dom.value= 'A-'
	}else if (rowdata.GOL_DARAH === '6')
	{
		Ext.get('cboGDRLAB').dom.value= 'B-'
	}
	Ext.getCmp('cboGDRLAB').disable();
	
	//alert(rowdata.KD_CUSTOMER);
	Ext.get('txtKdCustomerLamaHide').dom.value= rowdata.KD_CUSTOMER;//tampung kd customer untuk transaksi selain kunjungan langsung
	vkode_customer = rowdata.KD_CUSTOMER;
	Ext.getCmp('cboKelPasienLab').disable();
	
	Ext.get('cboKelPasienLab').dom.value= rowdata.KELPASIEN;
	Combo_Select(rowdata.KELPASIEN);
	
	kelompokPasien= rowdata.KELPASIEN;
	Ext.getCmp('cboKelPasienLab').enable();
	Ext.get('txtCustomerLamaHide').hide();
	if(rowdata.KELPASIEN == 'Perseorangan'){
		Ext.getCmp('cboPerseoranganLab').setValue(rowdata.CUSTOMER);
		Ext.getCmp('cboPerseoranganLab').show();
	} else if(rowdata.KELPASIEN == 'Perusahaan'){
		Ext.getCmp('cboPerusahaanRequestEntryLab').setValue(rowdata.CUSTOMER);
		Ext.getCmp('cboPerusahaanRequestEntryLab').show();
	} else{
		Ext.getCmp('cboAsuransiLab').setValue(rowdata.CUSTOMER);
		Ext.getCmp('cboAsuransiLab').show();
	}

	// 'DOKTER_ASAL','KD_DOKTER_ASAL'
	// Ext.getCmp('txtDokterPengirimLAB').setValue(rowdata.DOKTER_ASAL);
	// Ext.getCmp('cboDOKTER_viPenJasLab').setValue(rowdata.DOKTER);
	// Ext.getCmp('cboUnitLab_viPenJasLab').setValue(rowdata.NAMA_UNIT_ASLI);
	// getDokterPengirim(rowdata.NO_TRANSAKSI,rowdata.KD_KASIR);
	if(tmppasienbarulama == 'Baru'){
		//Ext.getCmp('txtDokterPengirimLAB').setValue(rowdata.DOKTER);
		Ext.getCmp('cboDokterPengirimLAB').setValue(rowdata.DOKTER);
		Ext.getCmp('cboDOKTER_viPenJasLab').setValue('');
		Ext.getCmp('cboUnitLab_viPenJasLab').setValue('');
		tmpkd_unit = '';
        Ext.get('txtNamaUnitLAB').dom.value = rowdata.NAMA_UNIT_ASLI;
		tmpkddoktertujuan = '';
		tmpkddokterpengirim = rowdata.KD_DOKTER;
		kd_kasir_lab = '';
		Ext.get('txtKdUnitLab').dom.value   = rowdata.KD_UNIT;
	}else{
		//Ext.getCmp('txtDokterPengirimLAB').setValue(rowdata.DOKTER_ASAL);
		Ext.getCmp('cboDokterPengirimLAB').setValue(rowdata.DOKTER_ASAL)
		Ext.getCmp('cboDOKTER_viPenJasLab').setValue(rowdata.DOKTER);
		Ext.getCmp('cboUnitLab_viPenJasLab').setValue(rowdata.NAMA_UNIT_ASLI);
		tmpkd_unit = rowdata.KD_UNIT;
		tmpkddoktertujuan = rowdata.KD_DOKTER;
		tmpkddokterpengirim = rowdata.KD_DOKTER;
		if (radiovaluesLAB !=='3'){
			Ext.get('txtNamaUnitLAB').dom.value = rowdata.NAMA_UNIT_ASAL;
			Ext.get('txtKdUnitLab').dom.value   = rowdata.KD_UNIT_ASAL;
		}else{
			Ext.get('txtNamaUnitLAB').dom.value = rowdata.NAMA_UNIT;
			Ext.get('txtKdUnitLab').dom.value   = rowdata.KD_UNIT;
		}
        
		kd_kasir_lab = rowdata.KD_KASIR;
		
	}
    Ext.getCmp('txtnotlplab').setValue(rowdata.TELEPON);
    Ext.getCmp('txtNoSJPLab').setValue(rowdata.SJP); 
        
	
	Ext.Ajax.request(
	{
	    url: baseURL + "index.php/main/getcurrentshift",
		 params: {
	        command: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
	    },
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
	    success: function(o) {
			tampungshiftsekarang=o.responseText
			
	    }
		
	
	});
	//,,
	//alert(tampungshiftsekarang);
	//--------------------ABULHASSAN ------------- 20_01_17
	namaCustomer_LIS=rowdata.CUSTOMER;
	namaKelompokPasien_LIS=rowdata.KELPASIEN;
	
	
};


function LABAddNew() 
{
	Ext.getCmp('btnTambahBaruLAB').show();
	Ext.Ajax.request(
	{
		url: baseURL + "index.php/main/functionLAB/getCurrentShiftLab",
		params: {
			command: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
		},
		failure: function(o)
		{
			var cst = Ext.decode(o.responseText);
		},
		success: function(o) {
			tampungshiftsekarang=o.responseText;
		}
	});
    AddNewPenJasRad = true;
	Ext.get('txtNoTransaksiPenJasLab').dom.value = '';
	Ext.get('txtNoMedrecLAB').dom.value='';
	Ext.get('txtNamaPasienLAB').dom.value = '';
	//Ext.get('cboDOKTER_viPenJasLab').dom.value = '';
	Ext.get('cboUnitLab_viPenJasLab').dom.value = '';
	Ext.get('cboJKLAB').dom.value = '';
	Ext.get('cboGDRLAB').dom.value = '';
	Ext.get('txtUmurLab').dom.value = '';
	Ext.getCmp('dtpTtlLAB').setValue(now);
	tglTransaksiAwalPembandingTransfer=nowTglTransaksi.format('d/M/Y');
	Ext.get('txtnotlplab').dom.value = '';
	Ext.get('txtKdDokter').dom.value   = undefined;
	Ext.get('txtNamaUnitLAB').dom.value   = 'Laboratorium';
	//Ext.getCmp('txtDokterPengirimLAB').setValue('DOKTER LUAR');
	Ext.getCmp('cboDokterPengirimLAB').setValue('DOKTER LUAR');
	tmpkddokterpengirim = '';
	Ext.get('txtKdUrutMasuk').dom.value = '';
	rowSelectedPenJasLab=undefined;
	dsTRDetailPenJasLabList.removeAll();
	
	databaru = 1;
	Ext.get('txtNamaUnitLAB').readOnly;
	Ext.getCmp('dtpKunjunganLAB').setReadOnly(true);

};

function RefreshDataPenJasLabDetail(no_transaksi) 
{
    var strKriteriaLAB='';
    //strKriteriaLAB = 'where no_transaksi = ~' + no_transaksi + '~'
    strKriteriaLAB = "\"no_transaksi\" = ~" + no_transaksi + "~" + " And kd_unit ='4'";
    //strKriteriaLAB = 'no_transaksi = ~0000004~';
   
    dsTRDetailPenJasLabList.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'tgl_transaksi',
			    //Sort: 'tgl_transaksi',
			    Sortdir: 'ASC',
			    target: 'ViewDetailRWJGridBawah',
			    param: strKriteriaLAB
			}
		}
	);
    return dsTRDetailPenJasLabList;
};

//tampil grid bawah penjas lab
function ViewGridBawahLookupPenjasLab(no_transaksi,kd_kasir) 
{
    var strKriteriaLAB='';
     if (kd_kasir !== undefined) {
        strKriteriaLAB = "\"no_transaksi\" = ~" + no_transaksi + "~" + " and kd_kasir = " + "~" + kd_kasir + "~";
    }else{
        strKriteriaLAB = "\"no_transaksi\" = ~" + no_transaksi + "~" ;
    }
   // strKriteriaLAB = "\"no_transaksi\" = ~" + no_transaksi + "~" ;//+ " And kd_kasir ='03'";
 
   
    dsTRDetailPenJasLabList.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'tgl_transaksi',
			    //Sort: 'tgl_transaksi',
			    Sortdir: 'ASC',
			    target: 'ViewDetailGridBawahPenJasLab',
			    param: strKriteriaLAB
			}
		}
	);
    return dsTRDetailPenJasLabList;
};

//---------------------------------------------------------------------------------------///
function getParamDetailTransaksiLAB() 
{
	
	var KdCust='';
	var TmpCustoLama='';
	var pasienBaru;
	var modul='';
	if(radiovaluesLAB == 1){
		modul='igd';
	} else if(radiovaluesLAB == 2){
		modul='rwi';
	} else if(radiovaluesLAB == 4){
		modul='rwj';
	} else{
		modul='langsung';
	}
	/* if(Ext.get('cboKelPasienLab').getValue()=='Perseorangan'){
		KdCust=Ext.getCmp('cboPerseoranganLab').getValue();
	}else if(Ext.get('cboKelPasienLab').getValue()=='Perusahaan'){
		KdCust=Ext.getCmp('cboPerusahaanRequestEntryLab').getValue();
	}else {
		KdCust=Ext.getCmp('cboAsuransiLab').getValue();
	} */
	
	if(Ext.getCmp('txtNoMedrecLAB').getValue() == ''){
		pasienBaru=1;
	} else{
		pasienBaru=0;
	}
	
    var params =
	{
		//Table:'ViewDetailTransaksiPenJasLab',
		
		KdTransaksi: Ext.get('txtNoTransaksiPenJasLab').getValue(),
		KdPasien:Ext.getCmp('txtNoMedrecLAB').getValue(),
		NmPasien:Ext.getCmp('txtNamaPasienLAB').getValue(),
		Ttl:Ext.getCmp('dtpTtlLAB').getValue(),
		Alamat:Ext.getCmp('txtAlamatLAB').getValue(),
		JK:Ext.getCmp('cboJKLAB').getValue(),
		GolDarah:Ext.getCmp('cboGDRLAB').getValue(),
		KdUnit: Ext.getCmp('txtKdUnitLab').getValue(),
		KdDokter:tmpkddoktertujuan,
		KdDokterPengirim:tmpkddokterpengirim,
		KdUnitTujuan:tmpkd_unit,
		Tgl: Ext.getCmp('dtpKunjunganLAB').getValue(),
		TglTransaksiAsal:TglTransaksi,
		Tlp: Ext.getCmp('txtnotlplab').getValue(),
		urutmasuk:Ext.getCmp('txtKdUrutMasuk').getValue(),
		KdCusto:vkode_customer,
		TmpCustoLama:vkode_customer,//Ext.getCmp('txtKdCustomerLamaHide').getValue(),//jika customer dengan transaksi lama
		NamaPesertaAsuransi:Ext.getCmp('txtNamaPesertaAsuransiLab').getValue(),
		NoAskes:Ext.getCmp('txtNoAskesLab').getValue(),
		NoSJP:Ext.getCmp('txtNoSJPLab').getValue(),
		Shift:tampungshiftsekarang,
		List:getArrPenJasLab(),
		JmlField: mRecordLAB.prototype.fields.length-4,
		JmlList:GetListCountDetailTransaksiLAB(),
		dtBaru:databaru,
		Hapus:1,
		Ubah:0,
		unit:41,
		TmpNotransaksi:TmpNotransaksi,
		KdKasirAsal:KdKasirAsal,
		KdSpesial:Kd_Spesial,
		Kamar:No_Kamar,
		pasienBaru:pasienBaru,
		listTrDokter	: '',
		Modul:modul,
		KdProduk:'',//sTRDetailPenJasLabList.data.items[CurrentPenJasLab.row].data.KD_PRODUK
        URUT :tmpurut
		
	};
    return params
};

function getParamHapusDetailTransaksiLAB() 
{	
	var tmpparam = cellSelecteddeskripsi.data;
	var line = gridDTItemTest.getSelectionModel().selection.cell[0];
    var params =
	{		
		no_tr: tmpparam.no_transaksi,
		urut:tmpparam.urut,
		tgl_transaksi:tmpparam.tgl_transaksi,
		//ABULHASSAN--------------20_01_2017
		no_trans_cadangan:Ext.getCmp('txtNoTransaksiPenJasLab').getValue(),
		kd_produk: dsTRDetailPenJasLabList.data.items[line].data.kd_produk,
		kd_kasir: dsTRDetailPenJasLabList.data.items[line].data.kd_kasir,
	};
    return params
};

function getParamKonsultasi() 
{

    var params =
	{
		
		Table:'ViewDetailTransaksiPenJasLab', //data access listnya belum dibuat
		
		//TrKodeTranskasi: Ext.get('txtNoTransaksiPenJasLab').getValue(),
		KdUnitAsal : Ext.get('txtKdUnitLab').getValue(),
		KdDokterAsal : Ext.get('txtKdDokter').getValue(),
		KdUnit: selectKlinikPoli,
		KdDokter:selectDokter,
		KdPasien:Ext.get('txtNoMedrecLAB').getValue(),
		TglTransaksi : Ext.get('dtpKunjunganLAB').dom.value,
		KDCustomer :vkode_customer,
	};
    return params
};

function GetListCountDetailTransaksiLAB()
{
	
	var x=0;
	for(var i = 0 ; i < dsTRDetailPenJasLabList.getCount();i++)
	{
		if (dsTRDetailPenJasLabList.data.items[i].data.KD_PRODUK != '' || dsTRDetailPenJasLabList.data.items[i].data.DESKRIPSI  != '')
		{
			x += 1;
		};
	}
	return x;
	
};


function getArrPenJasLab()
{
    //console.log(dsTRDetailPenJasLabList.data);
    var abc=dsTRDetailPenJasLabList.data;
	var x='';
	var arr=[];
	for(var i = 0 ; i < dsTRDetailPenJasLabList.getCount();i++)
	{
		if (dsTRDetailPenJasLabList.data.items[i].data.kd_produk != undefined && dsTRDetailPenJasLabList.data.items[i].data.deskripsi != undefined && dsTRDetailPenJasLabList.data.items[i].data.kd_produk != '' && dsTRDetailPenJasLabList.data.items[i].data.deskripsi != '')
		{
			/* var y='';
			var z='@@##$$@@';
			
			y =  dsTRDetailPenJasLabList.data.items[i].data.URUT
			y += z + dsTRDetailPenJasLabList.data.items[i].data.KD_PRODUK
			y += z + dsTRDetailPenJasLabList.data.items[i].data.QTY
			y += z + ShowDate(dsTRDetailPenJasLabList.data.items[i].data.TGL_BERLAKU)
			y += z +dsTRDetailPenJasLabList.data.items[i].data.HARGA
			y += z +dsTRDetailPenJasLabList.data.items[i].data.KD_TARIF
			y += z +dsTRDetailPenJasLabList.data.items[i].data.URUT
			
			
			if (i === (dsTRDetailPenJasLabList.getCount()-1))
			{
				x += y 
			}
			else
			{
				x += y + '##[[]]##'
			}; */
			var o={};
			var y='';
			var z='@@##$$@@';
			o['URUT']= dsTRDetailPenJasLabList.data.items[i].data.urut;
			o['KD_PRODUK']= dsTRDetailPenJasLabList.data.items[i].data.kd_produk;
			o['QTY']= dsTRDetailPenJasLabList.data.items[i].data.qty;
			o['TGL_TRANSAKSI']= dsTRDetailPenJasLabList.data.items[i].data.tgl_transaksi;
			o['TGL_BERLAKU']= dsTRDetailPenJasLabList.data.items[i].data.tgl_berlaku;
			o['HARGA']= dsTRDetailPenJasLabList.data.items[i].data.harga;
			o['KD_TARIF']= dsTRDetailPenJasLabList.data.items[i].data.kd_tarif;
            o['cito']= dsTRDetailPenJasLabList.data.items[i].data.cito;
			o['kd_unit']= dsTRDetailPenJasLabList.data.items[i].data.kd_unit;
			//--------------ABULHASSAN--------------20_01_2017
			o['DESKRIPSI']= dsTRDetailPenJasLabList.data.items[i].data.deskripsi;
			arr.push(o);
            //console.log(abc.items[i].data);
		};
	}	
	
	return Ext.encode(arr);
};

//panel dalam lookup detail pasien
function getItemPanelPenJasLab(lebar) 
{
	//pengaturan panel data pasien
    var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
		bodyStyle: 'padding: 5px 5px 5px 5px',
	    border:false,
	    height:225,
	    items:
		[
					{ //awal panel biodata pasien
						columnWidth:.99,
						layout: 'absolute',
						bodyStyle: 'padding: 5px 5px 5px 5px',
						border: true,
						width: 100,
						height: 120,
						anchor: '100% 100%',
						items:
						[
                            //bagian pinggir kiri                            
							{
								x: 10,
								y: 10,
								xtype: 'label',
								text: 'No. Transaksi  '
							},
							{
								x: 100,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x : 110,
								y : 10,
								xtype: 'textfield',
								name: 'txtNoTransaksiPenJasLab',
								id: 'txtNoTransaksiPenJasLab',
								width: 100,
								emptyText:nmNomorOtomatis,
								readOnly:true

							},
							{
								x: 10,
								y: 40,
								xtype: 'label',
								text: 'No. Medrec  '
							},
							{
								x: 100,
								y: 40,
								xtype: 'label',
								text: ':'
							},
							{
								x: 110,
								y: 40,
								xtype: 'textfield',
								fieldLabel: 'No. Medrec',
								name: 'txtNoMedrecLAB',
								id: 'txtNoMedrecLAB',
								readOnly:true,
								width: 120,
								emptyText:'Otomatis',
								listeners: 
								{ 
									
								}
							},
							{
								x: 10,
								y: 70,
								xtype: 'label',
								text: 'Unit  '
							},
							{
								x: 100,
								y: 70,
								xtype: 'label',
								text: ':'
							},
							{
								x: 110,
								y: 70,
								xtype: 'textfield',
								name: 'txtNamaUnitLAB',
								id: 'txtNamaUnitLAB',
								readOnly:true,
								width: 120
							},
							{
								x: 10,
								y: 100,
								xtype: 'label',
								text: 'Dokter Pengirim'
							},
							{
								x: 100,
								y: 100,
								xtype: 'label',
								text: ':'
							},mComboDokterPengirim()
							/* {
								x: 110,
								y: 100,
								xtype: 'textfield',
								fieldLabel: '',
								readOnly:true,
								name: 'txtDokterPengirimLAB',
								id: 'txtDokterPengirimLAB',
								width: 200
							} */,
							{
								x: 10,
								y: 130,
								xtype: 'label',
								text: 'Lab. Tujuan'
							},
							{
								x: 100,
								y: 130,
								xtype: 'label',
								text: ':'
							},
							mComboUnitLab(),
                            {
                                x: 360,
                                y: 130,
                                xtype: 'label',
                                text: 'No. Tlp'
                            },
                            {
                                x: 400,
                                y: 130,
                                xtype: 'label',
                                text: ':'
                            },
                            {
                                x: 410,
                                y: 130,
                                xtype: 'textfield',
                                fieldLabel: '',
                                readOnly:true,
                                name: 'txtnotlplab',
                                id: 'txtnotlplab',
                                width: 200,
								enableKeyEvents: true,
								listeners:{
									'specialkey': function (){
										if (Ext.EventObject.getKey() === 13){
											Ext.getCmp('cboDOKTER_viPenJasLab').focus();
										}
									}
								}
                            },
							//bagian tengah
							{
								x: 260,
								y: 10,
								xtype: 'label',
								text: 'Tanggal Kunjung '
							},
							{
								x: 350,
								y: 10,
								xtype: 'label',
								text: ':'
							},
							{
								x: 360,
								y: 10,
								xtype: 'datefield',
								fieldLabel: 'Tanggal hari ini ',
								id: 'dtpKunjunganLAB',
								name: 'dtpKunjunganLAB',
								format: 'd/M/Y',
								//readOnly : true,
								value: now,
								width: 110
							},
							{
								x: 260,
								y: 40,
								xtype: 'label',
								text: 'Nama Pasien '
							},
							{
								x: 350,
								y: 40,
								xtype: 'label',
								text: ':'
							},
							{
								x: 360,
								y: 40,
								xtype: 'textfield',
								fieldLabel: '',
								readOnly:true,
								name: 'txtNamaPasienLAB',
								id: 'txtNamaPasienLAB',
								width: 200,
								enableKeyEvents: true,
								listeners:{
									'specialkey': function (){
										if (Ext.EventObject.getKey() === 13){
											Ext.getCmp('dtpTtlLAB').focus();
										}
									}
								}
							},
							{
								x: 260,
								y: 70,
								xtype: 'label',
								text: 'Alamat '
							},
							{
								x: 350,
								y: 70,
								xtype: 'label',
								text: ':'
							},
							{
								x: 360,
								y: 70,
								xtype: 'textfield',
								fieldLabel: '',
								readOnly:true,
								name: 'txtAlamatLAB',
								id: 'txtAlamatLAB',
								width: 395,
								enableKeyEvents: true,
								listeners:{
									'specialkey': function (){
										if (Ext.EventObject.getKey() === 13){
											if (combovalues === 'Transaksi Lama')
											{
												Ext.getCmp('txtnotlplab').focus();
											}
											else
											{
												Ext.getCmp('cboJKLAB').focus();
											}
											
										}
									}
								}
							},
							{
								x: 360,
								y: 100,
								xtype: 'label',
								text: 'Jenis Kelamin '
							},
							{
								x: 445,
								y: 100,
								xtype: 'label',
								text: ':'
							},
							mComboJK(),
							{
								x: 570,
								y: 100,
								xtype: 'label',
								text: 'Gol. Darah '
							},
							{
								x: 640,
								y: 100,
								xtype: 'label',
								text: ':'
							},
							mComboGDarah(),
							{
								x: 570,
								y: 40,
								xtype: 'label',
								text: 'Tanggal lahir '
							},
							{
								x: 640,
								y: 40,
								xtype: 'label',
								text: ':'
							},
							{
								x: 645,
								y: 40,
								xtype: 'datefield',
								fieldLabel: 'Tanggal ',
								selectOnFocus: true,
								forceSelection: true,
								id: 'dtpTtlLAB',
								name: 'dtpTtlLAB',
								format: 'd/M/Y',
								//readOnly : true,
								value: now,
								width: 110,
								enableKeyEvents: true,
								listeners:{
									'specialkey': function (){
										if (Ext.EventObject.getKey() === 13){
											var tmptanggal = Ext.get('dtpTtlLAB').getValue();
											Ext.Ajax.request({
												url: baseURL + "index.php/main/GetUmur_LAB",
												params: {
													TanggalLahir: tmptanggal
												},
												success: function (o){
													var tmphasil = o.responseText;
													var tmp = tmphasil.split(' ');
													if (tmp.length == 6){
														Ext.getCmp('txtUmurLab').setValue(tmp[0]);
														getParamBalikanHitungUmur(tmptanggal);
													}else if(tmp.length == 4){
														if(tmp[1]== 'years' && tmp[3] == 'day'){
															Ext.getCmp('txtUmurLab').setValue(tmp[0]);
														}else if(tmp[1]== 'years' && tmp[3] == 'days'){
															Ext.getCmp('txtUmurLab').setValue(tmp[0]);
														}else if(tmp[1]== 'year' && tmp[3] == 'days'){
															Ext.getCmp('txtUmurLab').setValue(tmp[0]);
														}else{
															Ext.getCmp('txtUmurLab').setValue(tmp[0]);
														}
														getParamBalikanHitungUmur(tmptanggal);
													}else if(tmp.length == 2 ){
														if (tmp[1] == 'year' ){
															Ext.getCmp('txtUmurLab').setValue(tmp[0]);
														}else if (tmp[1] == 'years' ){
															Ext.getCmp('txtUmurLab').setValue(tmp[0]);
														}else if (tmp[1] == 'mon'  ){
															Ext.getCmp('txtUmurLab').setValue('0');
														}else if (tmp[1] == 'mons'  ){
															Ext.getCmp('txtUmurLab').setValue('0');
														}else{
															Ext.getCmp('txtUmurLab').setValue('0');
														}
														getParamBalikanHitungUmur(tmptanggal);
													}else if(tmp.length == 1){
														Ext.getCmp('txtUmurLab').setValue('0');
														getParamBalikanHitungUmur(tmptanggal);
													}else{
														alert("Tanggal yang anda masukan lebih besar dari tanggal sekarang");
													}	
												}
											});
											Ext.getCmp('txtAlamatLAB').focus();
										}
									}
								}
							},
							{
								x: 765,
								y: 40,
								xtype: 'label',
								text: 'Umur '
							},
							{
								x: 795,
								y: 40,
								xtype: 'label',
								text: ':'
							},
							{
								x: 805,
								y: 40,
								xtype: 'textfield',
								fieldLabel: '',
								name: 'txtUmurLab',
								id: 'txtUmurLab',
								width: 30,
								//anchor: '95%',
                                listeners:
                                        {
                                            'specialkey': function ()
                                            {
												if (Ext.EventObject.getKey() === 13)
                                                {
													Ext.Ajax.request
													(
															{
																url: baseURL + "index.php/main/functionLAB/cekUsia",
																params: {umur: Ext.get('txtUmurLab').getValue()},
																failure: function (o)
																{
																	ShowPesanErrorPenJasLab('Proses Error ! ', 'Laboratorium');
																},
																success: function (o)
																{
																	var cst = Ext.decode(o.responseText);
																	if (cst.success === true)
																	{
																		Ext.getCmp('dtpTtlLAB').setValue(cst.tahunumur);
																	}
																	else
																	{
																		ShowPesanErrorPenJasLab('Proses Error ! ', 'Laboratorium');
																	}
																}
															}
													)
												}
											}
										}
							},
							{
								x: 10,
								y: 160,
								xtype: 'label',
								text: 'Dokter Lab '
							},
							{
								x: 100,
								y: 160,
								xtype: 'label',
								text: ':'
							},
							mComboDOKTER(),
							{
								x: 10,
								y: 185,
								xtype: 'label',
								text: 'Kelompok Pasien'
							},
							{
								x: 100,
								y: 185,
								xtype: 'label',
								text: ':'
							},
							
                            {
                                x: 360,
                                y: 185,
                                xtype: 'label',
                                text: 'No. SEP'
                            },
                            {
                                x: 400,
                                y: 185,
                                xtype: 'label',
                                text: ':'
                            },
							mCboKelompokpasien(),
							mComboPerseorangan(),
							mComboPerusahaan(),
							mComboAsuransi(),
							{
								x: 415,
								y: 185,
								xtype: 'textfield',
								fieldLabel: 'Nama Peserta',
								maxLength: 200,
								name: 'txtNamaPesertaAsuransiLab',
								id: 'txtNamaPesertaAsuransiLab',
								emptyText:'Nama Peserta Asuransi',
								width: 125
							},
							{
								x: 545,
								y: 185,
								xtype: 'textfield',
								fieldLabel: 'No. Askes',
								maxLength: 200,
								name: 'txtNoAskesLab',
								id: 'txtNoAskesLab',
								emptyText:'No Askes',
								width: 125
							},
							{
								x: 675,
								y: 185,
								xtype: 'textfield',
								fieldLabel: 'No. SEP',
								maxLength: 200,
								name: 'txtNoSJPLab',
								id: 'txtNoSJPLab',
								emptyText:'No SEP',
								width: 125
							},
							{
								x: 110,
								y: 130,
								xtype: 'textfield',
								fieldLabel:'Dokter  ',
								name: 'txtCustomerLamaHide',
								id: 'txtCustomerLamaHide',
								readOnly:true,
								width: 280
							},
							{
								x: 110,
								y: 130,
								xtype: 'textfield',
								fieldLabel:'Kode customer  ',
								name: 'txtKdCustomerLamaHide',
								id: 'txtKdCustomerLamaHide',
								readOnly:true,
								hidden:true,
								width: 280
							},
							
							
							
							//---------------------hidden----------------------------------
							
							{
								xtype: 'textfield',
								fieldLabel:'Dokter  ',
								name: 'txtKdDokter',
								id: 'txtKdDokter',
								readOnly:true,
								hidden:true,
								anchor: '99%'
							},
							
							{
								xtype: 'textfield',
								fieldLabel:'Unit  ',
								name: 'txtKdUnitLab',
								id: 'txtKdUnitLab',
								readOnly:true,
								hidden:true,
								anchor: '99%'
							},
							{
								xtype: 'textfield',
								name: 'txtKdUrutMasuk',
								id: 'txtKdUrutMasuk',
								readOnly:true,
								hidden:true,
								anchor: '100%'
							}
								
							//-------------------------------------------------------------
							
						]
					},	//akhir panel biodata pasien
			
		]
	};
    return items;
};


function getParamBalikanHitungUmur(tgl){
	Ext.getCmp('dtpTtlLAB').setValue(tgl);
}
function getItemPanelUnit(lebar) 
{
    var items =
	{
	    layout: 'column',
	    border: false,
	    items:
		[
			{
			    columnWidth: .40,
			    layout: 'form',
				labelWidth:100,
			    border: false,
			    items:
				[
					{
					    xtype: 'textfield',
					    fieldLabel:'Unit  ',
					    name: 'txtKdUnitLab',
					    id: 'txtKdUnitLab',
						readOnly:true,
					    anchor: '99%'
					}
				]
			},
			{
			    columnWidth: .60,
			    layout: 'form',
			    border: false,
				labelWidth:1,
			    items:
				[
					{
						xtype: 'textfield',
					    name: 'txtNamaUnitLAB',
					    id: 'txtNamaUnitLAB',
						readOnly:true,
					    anchor: '100%'
					}
				]
			}
		]
	}
    return items;
};

function refeshpenjaslab(kriteria)
{
    /* dsTRPenJasLabList.load
        (
            { 
                params:  
                {   
                    Skip: 0, 
                    Take: '',
                    Sort: '',
                    Sortdir: 'ASC', 
                    target: 'ViewPenJasLab',
                    param : kriteria
                }			
            }
        );   
    return dsTRPenJasLabList; */
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/lab/viewpenjaslab/getPasien",
			params: getParamRefreshLab(kriteria),
			failure: function(o)
			{
				ShowPesanWarningPenJasLab('Hubungi Admin', 'Error');
			},
			success: function(o)
			{   
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					dsTRPenJasLabList.removeAll();
					var recs=[],
					recType=dsTRPenJasLabList.recordType;
					for(var i=0; i<cst.ListDataObj.length; i++){
						recs.push(new recType(cst.ListDataObj[i]));
					}
					dsTRPenJasLabList.add(recs);
					grListPenJasLab.getView().refresh();

				}
				else
				{
					ShowPesanWarningPenJasLab('Gagal membaca Data ini', 'Error');
				};
			}
		}

	);
}


function getParamRefreshLab(krite){
	var unit;
	if(radiovaluesLAB == 1){
		unit = 'IGD';
	} else if(radiovaluesLAB == 2){
		unit = 'RWI';
	} else if(radiovaluesLAB == 3){
		unit = 'Langsung';
	} else{
		unit = 'RWJ';
	}
	var params =
	{
		unit:unit,
		kriteria:krite
		/* tglAwal:
		tglAkhir:dtpTglAwalFilterPenJasLab */
		
	}
	return params;
}

function loadpenjaslab(tmpparams, tmpunit)
{
    dsTRPenJasLabList.load
		(
                    { 
                        params:  
                        {   
                            Skip: 0, 
                            Take: '',
                            Sort: '',
                            Sortdir: 'ASC', 
                            target: tmpunit,
                            param : tmpparams
                        }			
                    }
		);   
    return dsTRPenJasLabList;
}

function Datasave_PenJasLab(mBol) 
{	
	 if (ValidasiEntryPenJasLab(nmHeaderSimpanData,false) == 1 )
	 {
  //       console.log(tglTransaksiAwalPembandingTransfer);
  //       console.log(Ext.getCmp('dtpKunjunganLAB').getValue().format('Y-m-d')+" 00:00:00");
  //       if (tglTransaksiAwalPembandingTransfer < Ext.getCmp('dtpKunjunganLAB').getValue().format('d/M/Y')){
  //           ViewGridBawahLookupPenjasLab(Ext.get('txtNoTransaksiPenJasLab').dom.value,kd_kasir_lab);
  //           ShowPesanWarningPenJasLab('Tanggal transaksi pasien Tidak boleh kurang dari tanggal kunjungan', 'Gagal');
			
		// }else{
			loadMask.show();
			Ext.Ajax.request
			 (
				{
					url: baseURL + "index.php/main/functionLAB/savedetaillab",
					params: getParamDetailTransaksiLAB(),
					failure: function(o)
					{
						ShowPesanWarningPenJasLab('Error simpan. Hubungi Admin!', 'Gagal');
						ViewGridBawahLookupPenjasLab(Ext.get('txtNoTransaksiPenJasLab').dom.value,kd_kasir_lab);
					},	
					success: function(o) 
					{
						
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							loadMask.hide();
							ShowPesanInfoPenJasLab("Berhasil menyimpan data ini","Information");
							kd_kasir_lab = cst.kdkasir;
							
							Ext.get('txtNoTransaksiPenJasLab').dom.value=cst.notrans;
							Ext.get('txtNoMedrecLAB').dom.value=cst.kdPasien;
							ViewGridBawahLookupPenjasLab(Ext.get('txtNoTransaksiPenJasLab').dom.value,cst.kdkasir);
							Ext.getCmp('btnPembayaranPenjasLAB').enable();
							Ext.getCmp('btnTutupTransaksiPenjasLAB').enable();
							if(mBol === false)
							{
								ViewGridBawahLookupPenjasLab(Ext.get('txtNoTransaksiPenJasLab').dom.value,cst.kdkasir);
							};
                            
                            tmpkd_pasien_sql = cst.kdPasien;
                            tmpno_trans_sql = cst.notrans;
                            tmpkd_kasir_sql = cst.kdkasir;
                            tmp_tgl_sql = cst.tgl;
                            tmp_urut_sql = cst.urut;
							if (Ext.get('cboJenisTr_viPenJasLab').getValue()!='Transaksi Baru' && radiovaluesLAB != '3')
							{
								validasiJenisTrLAB();
							}
							//
							//coba disatukan di function php yang sama
                            //Datasave_PenJasLab_SQL();
							Datasave_LAB_LIS();
						}
						else 
						{
								ShowPesanWarningPenJasLab('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
								ViewGridBawahLookupPenjasLab(Ext.get('txtNoTransaksiPenJasLab').dom.value,kd_kasir_lab);
						};
					}
				}
			)
		// }
		 
		
	 }
	 else
	{
		if(mBol === true)
		{
			return false;
		};
	}; 
	
};

function Datasave_PenJasLab_LangsungTambahBaris(mBol) 
{	
	 if (ValidasiEntryPenJasLab(nmHeaderSimpanData,false) == 1 )
	 {
		 loadMask.show();
			Ext.Ajax.request
			 (
				{
					url: baseURL + "index.php/main/functionLAB/savedetaillab",
					params: getParamDetailTransaksiLAB(),
					failure: function(o)
					{
						ShowPesanWarningPenJasLab('Error simpan. Hubungi Admin!', 'Gagal');
						ViewGridBawahLookupPenjasLab(Ext.get('txtNoTransaksiPenJasLab').dom.value,kd_kasir_lab);
					},	
					success: function(o) 
					{
						
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							loadMask.hide();
							//ShowPesanInfoPenJasLab("Berhasil menyimpan data ini","Information");
							kd_kasir_lab = cst.kdkasir;
							
							Ext.get('txtNoTransaksiPenJasLab').dom.value=cst.notrans;
							Ext.get('txtNoMedrecLAB').dom.value=cst.kdPasien;
							//ViewGridBawahLookupPenjasLab(Ext.get('txtNoTransaksiPenJasLab').dom.value,cst.kdkasir);
							Ext.getCmp('btnPembayaranPenjasLAB').enable();
							Ext.getCmp('btnTutupTransaksiPenjasLAB').enable();
							/* if(mBol === false)
							{
								ViewGridBawahLookupPenjasLab(Ext.get('txtNoTransaksiPenJasLab').dom.value,cst.kdkasir);
							}; */
                            
                            tmpkd_pasien_sql = cst.kdPasien;
                            tmpno_trans_sql = cst.notrans;
                            tmpkd_kasir_sql = cst.kdkasir;
                            tmp_tgl_sql = cst.tgl;
                            tmp_urut_sql = cst.urut;
							if (Ext.get('cboJenisTr_viPenJasLab').getValue()!='Transaksi Baru' && radiovaluesLAB != '3')
							{
								validasiJenisTrLAB();
							}
							//
							//coba disatukan di function php yang sama
                            //Datasave_PenJasLab_SQL();
							Datasave_LAB_LIS();
						}
						else 
						{
								ShowPesanWarningPenJasLab('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
						};
					}
				}
			)
		
	 }
	 else
	{
		if(mBol === true)
		{
			return false;
		};
	}; 
	
};

var tmpkd_pasien_sql = '';
var tmpno_trans_sql = '';
var tmpkd_kasir_sql = '';
var tmp_tgl_sql = '';
var tmp_urut_sql = '';

function DataHapus_PenJasLab(mBol) 
{	
	 if (ValidasiEntryPenJasLab(nmHeaderSimpanData,false) == 1 )
	 {
			Ext.Ajax.request
			 (
				{
					url: baseURL + "index.php/main/functionLAB/deletedetaillab",
					params: getParamHapusDetailTransaksiLAB(),
					failure: function(o)
					{
						ShowPesanWarningPenJasLab('Error simpan. Hubungi Admin!', 'Gagal');
					},	
					success: function(o) 
					{
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoPenJasLab('Data Berhasil Di hapus', 'Sukses');
							
						}
						else 
						{
								ShowPesanWarningPenJasLab('Data Tidak Bisa Di Hapus Harap Hubungi Admin', 'Gagal');
						};
					}
				}
			)
		
	 }
	 else
	{
		if(mBol === true)
		{
			return false;
		};
	}; 
	
};


function Dataupdate_PenJasRad(mBol) 
{	
	if (ValidasiEntryPenJasLab(nmHeaderSimpanData,false) == 1 )
	{
		
			Ext.Ajax.request
			 (
				{
					//url: "./Datapool.mvc/CreateDataObj",
					url: baseURL + "index.php/main/CreateDataObj",
					params: getParamDataupdatePenJasRadDetail(),
					success: function(o) 
					{
						ViewGridBawahLookupPenjasLab(Ext.get('txtNoTransaksiPenJasLab').dom.value,kd_kasir_lab);
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoPenJasLab(nmPesanSimpanSukses,nmHeaderSimpanData);
							//RefreshDataPenJasRad();
							if(mBol === false)
							{
								ViewGridBawahLookupPenjasLab(Ext.get('txtNoTransaksiPenJasLab').dom.value,kd_kasir_lab);
							};
						}
						else if  (cst.success === false && cst.pesan===0)
						{
							ShowPesanWarningPenJasLab(nmPesanSimpanGagal,nmHeaderSimpanData);
						}
						else if (cst.success === false && cst.pesan===1)
						{
							ShowPesanWarningPenJasLab(nmPesanSimpanGagal + ' , This request had been approved / rejected',nmHeaderSimpanData);
						}
						else 
						{
							ShowPesanErrorPenJasLab(nmPesanSimpanError,nmHeaderSimpanData);
						};
					}
				}
			)
		
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
};
function mComboUnitTujuans_viPenJasLab(){
	var Field = ['KD_UNIT','NAMA_UNIT'];
    dsunitlabs_viPenJasLab = new WebApp.DataStore({ fields: Field });
    dsunitlabs_viPenJasLab.load({
		params:{
			Skip: 0,
			Take: 1000,
			Sort: 'nama_unit',
			Sortdir: 'ASC',
			target: 'ViewSetupUnit',
			param: "parent = '4' and kd_unit not in ('44','45')"
		}
	});
    var cbounitlabs_viPenJasLab = new Ext.form.ComboBox({
		id: 'cbounitlabs_viPenJasLab',
		x: 455,
		y: 10,
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		emptyText: '',
		disabled:true,
		fieldLabel:  ' ',
		align: 'Right',
		width: 230,
		emptyText:'Pilih Unit',
		store: dsunitlabs_viPenJasLab,
		valueField: 'KD_UNIT',
		displayField: 'NAMA_UNIT',
		//value:'All',
		editable: false,
		listeners:{
			'select': function(a,b,c){
				combovaluesunittujuan=b.data.KD_UNIT;
				validasiJenisTrLAB();
			}
		}
	});
    return cbounitlabs_viPenJasLab;
	
};
function ValidasiEntryPenJasLab(modul,mBolHapus)
{
	var x = 1;
	
	if((Ext.getCmp('cboUnitLab_viPenJasLab').getValue() == '') ||(Ext.get('txtNamaPasienLAB').getValue() == '') || (Ext.get('cboDOKTER_viPenJasLab').getValue() == 'Pilih DOkter') || (Ext.get('dtpKunjunganLAB').getValue() == '') || (dsTRDetailPenJasLabList.getCount() === 0 ))
	{
		if (Ext.get('txtNoTransaksiPenJasLab').getValue() == '' && mBolHapus === true) 
		{
			x = 0;
		}
		 else if (Ext.get('txtNoMedrecLAB').getValue() == '') 
		{
			ShowPesanWarningPenJasLab('No. Medrec tidak boleh kosong!','Warning');
			x = 0;
		} else if (Ext.get('cboUnitLab_viPenJasLab').getValue() == '') 
		{
			ShowPesanWarningPenJasLab('Laboratorium tujuan Wajib diisi!','Warning');
			x = 0;
		}
		else if (Ext.get('txtNamaPasienLAB').getValue() == '') 
		{
			ShowPesanWarningPenJasLab('Nama tidak boleh kosong!','Warning');
			x = 0;
		}
		else if (Ext.get('dtpKunjunganLAB').getValue() == '') 
		{
			ShowPesanWarningPenJasLab('Tanggal kunjungan tidak boleh kosong!','Warning');
			x = 0;
		}
		else if (Ext.get('cboDOKTER_viPenJasLab').getValue() == 'Pilih Dokter' || Ext.get('cboDOKTER_viPenJasLab').dom.value  === undefined) 
		{
			ShowPesanWarningPenJasLab('Dokter Laboratorium tidak boleh kosong!','Warning');
			x = 0;
		}
		else if (dsTRDetailPenJasLabList.getCount() === 0) 
		{
			ShowPesanWarningPenJasLab('Daftar item test tidak boleh kosong!','Warning');
			x = 0;
		};
	};
	return x;
};

function ShowPesanWarningPenJasLab(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorPenJasLab(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoPenJasLab(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};


function DataDeletePenJasRad() 
{
   if (ValidasiEntryPenJasLab(nmHeaderHapusData,true) == 1 )
    {
        Ext.Msg.show
        (
            {
               title:nmHeaderHapusData,
               msg: nmGetValidasiHapus(nmTitleFormRequest) ,
               buttons: Ext.MessageBox.YESNO,
               width:275,
               fn: function (btn)
               {
                    if (btn =='yes')
                    {
                        Ext.Ajax.request
                        (
                            {
                                //url: "./Datapool.mvc/DeleteDataObj",
                                url: baseURL + "index.php/main/DeleteDataObj",
                                params: getParamDetailTransaksiLAB(),
                                success: function(o)
                                {
                                    var cst = Ext.decode(o.responseText);
                                    if (cst.success === true)
                                    {
                                        ShowPesanInfoPenJasLab(nmPesanHapusSukses,nmHeaderHapusData);
                                        //RefreshDataPenJasRad();
                                        LABAddNew();
                                    }
                                    else if (cst.success === false && cst.pesan===0)
                                    {
                                        ShowPesanWarningPenJasLab(nmPesanHapusGagal,nmHeaderHapusData);
                                    }
                                    else if (cst.success === false && cst.pesan===1)
                                    {
                                        ShowPesanWarningPenJasLab(nmPesanHapusGagal + ' , ',nmHeaderHapusData);
                                    }
                                    else
                                    {
                                        ShowPesanErrorPenJasLab(nmPesanHapusError,nmHeaderHapusData);
                                    };
                                }
                            }
                        )
                    };
                }
            }
        )
    };
};

function RefreshDatacombo(jeniscus) 
{

    ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: 'jenis_cust=~'+ jeniscus +'~ and kontraktor.kd_customer not in(~'+ vkode_customer+'~)'
            }
        }
    )
	
    // rowSelectedPenJasLab = undefined;
    return ds_customer_viDaftar;
};
function mComboKelompokpasien()
{

var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});
	
	ds_customer_viDaftar.load
    (
        {
            params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboKontrakCustomer',
                param: 'jenis_cust=~'+ jeniscus +'~'
            }
        }
    )
    var cboKelompokpasien = new Ext.form.ComboBox
	(
		{
			id:'cboKelompokpasien',
			typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus:true,
                        forceSelection: true,
                        emptyText:'Pilih...',
                        fieldLabel: labelisi,
                        align: 'Right',
                        anchor: '95%',
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
            displayField: 'CUSTOMER',
			listeners:
			{
				'select': function(a,b,c)
				{
					selectSetKelompokpasien=b.data.displayText ;
					selectKdCustomer=b.data.KD_CUSTOMER;
					selectNamaCustomer=b.data.CUSTOMER;
				
				}
			}
		}
	);
	return cboKelompokpasien;
};

function getKelompokpasienlama(lebar) 
{
    var items =
	{
		Width:lebar,
		height:40,
	    layout: 'column',
	    border: false,
	//	title: 'Kelompok Pasien Lama',
		
	    items:
		[
			{
			    columnWidth: .990,
			    layout: 'form',
				Width:lebar-10,
				labelWidth:130,
			    border: true,
				//title: 'Kelompok Pasien Lama',
			    items:
				[
					{	 
						xtype: 'tbspacer',
						height: 2
					},
					{
						xtype: 'textfield',
						fieldLabel: 'Kelompok Pasien Asal',
						//maxLength: 200,
						name: 'txtCustomer_labLama',
						id: 'txtCustomer_labLama',
						labelWidth:130,
						width: 100,
						anchor: '95%'
					},
					
					
				]
			}
			
		]
	}
    return items;
};


function getItemPanelNoTransksiKelompokPasien(lebar) 
{
    var items =
	{
		Width:lebar,
		height:120,
	    layout: 'column',
	    border: false,
		
		
	    items:
		[
			{

			    columnWidth: .990,
			    layout: 'form',
				Width:lebar-10,
				labelWidth:130,
			    border: true,
			    items:
				[
					
					
					{	 
						xtype: 'tbspacer',
						
						height:3
					},
						{  

						xtype: 'combo',
						fieldLabel: 'Kelompok Pasien Baru',
						id: 'kelPasien',
						 editable: false,
						//value: 'Perseorangan',
						store: new Ext.data.ArrayStore
							(
								{
								id: 0,
								fields:
								[
								'Id',
								'displayText'
								],
								   data: [[1, 'Perseorangan'], [2, 'Perusahaan'], [3, 'Asuransi']]
								}
							),
							  displayField: 'displayText',
							  mode: 'local',
							  width: 100,
							  forceSelection: true,
							  triggerAction: 'all',
							  emptyText: 'Pilih Salah Satu...',
							  selectOnFocus: true,
							  anchor: '95%',
							  listeners:
								{
										'select': function(a, b, c)
									{
										if(b.data.displayText =='Perseorangan')
										{
											jeniscus='0'
											Ext.getCmp('txtNoSJP').disable();
											Ext.getCmp('txtNoAskes').disable();}
										else if(b.data.displayText =='Perusahaan')
										{
											jeniscus='1';
											Ext.getCmp('txtNoSJP').disable();
											Ext.getCmp('txtNoAskes').disable();}
										else if(b.data.displayText =='Asuransi')
										{
											jeniscus='2';
											Ext.getCmp('txtNoSJP').enable();
											Ext.getCmp('txtNoAskes').enable();
										}
									
										RefreshDatacombo(jeniscus);
									}

								}
					    }, 
					    {
							columnWidth: .990,
							layout: 'form',
							border: false,
							labelWidth:130,
							items:
							[
								mComboKelompokpasien()
							]
						},
						{
							xtype: 'textfield',
							fieldLabel: 'No. SJP',
							maxLength: 200,
							name: 'txtNoSJP',
							id: 'txtNoSJP',
							width: 100,
							anchor: '95%'
						},
						{
							xtype: 'textfield',
							fieldLabel: 'No. Askes',
							maxLength: 200,
							name: 'txtNoAskes',
							id: 'txtNoAskes',
							width: 100,
							anchor: '95%'
						 }
						
						//mComboKelompokpasien
		
		
				]
			}
			
		]
	}
    return items;
};

//combo jenis kelamin
function mComboJK()
{
    var cboJKLAB = new Ext.form.ComboBox
	(
		{
			id:'cboJKLAB',
			x: 455,
            y: 100,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			tabIndex:3,
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Jenis Kelamin',
			fieldLabel: 'Jenis Kelamin ',
			editable: false,
			width:95,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[true, 'Laki - Laki'], [false, 'Perempuan']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetJK,
			enableKeyEvents: true,
			listeners:
			{
				'specialkey': function (){
					if (Ext.EventObject.getKey() === 13){
						Ext.getCmp('cboGDRLAB').focus();
					}
				},
				'select': function(a,b,c)
				{
					selectSetJK=b.data.Id ;
					jenisKelaminPasien=b.data.Id ;
					console.log(jenisKelaminPasien);
				},
/* 				'render': function(c) {
									c.getEl().on('keypress', function(e) {
									if(e.getKey() == 13) //atau Ext.EventObject.ENTER
									Ext.getCmp('txtTempatLahir').focus();
									}, c);
								}
 */			}
		}
	);
	return cboJKLAB;
};

//combo golongan darah
function mComboGDarah()
{
    var cboGDRLAB = new Ext.form.ComboBox
	(
		{
			id:'cboGDRLAB',
			x: 645,
            y: 100,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			tabIndex:3,
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Gol. Darah',
			fieldLabel: 'Gol. Darah ',
			width:50,
			editable: false,
			store: new Ext.data.ArrayStore
			(
				{
					id: 0,
					fields:
					[
						'Id',
						'displayText'
					],
				data: [[0, '-'], [1, 'A+'],[2, 'B+'], [3, 'AB+'],[4, 'O+'], [5, 'A-'], [6, 'B-']]
				}
			),
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetGDarah,
			enableKeyEvents: true,
			listeners:
			{
				'specialkey': function (){
					if (Ext.EventObject.getKey() === 13){
						Ext.getCmp('txtnotlplab').focus();
					}
				},
				'select': function(a,b,c)
				{
					selectSetGDarah=b.data.displayText ;
				},
			}
		}
	);
	return cboGDRLAB;
};

//Combo Dokter Lookup
function getComboDokterLab(kdUnit)
{
	dsdokter_viPenJasLab.load
	({
		params:
		{
			Skip: 0,
			Take: 1000,
			Sort: 'kd_dokter',
			Sortdir: 'ASC',
			target: 'ViewDokterPenunjang',
			param: "kd_unit = '"+kdUnit+"'"
		}
	});
	return dsdokter_viPenJasLab;
}
function mComboDOKTER()
{ 
    var Field = ['KD_DOKTER','NAMA'];

    dsdokter_viPenJasLab = new WebApp.DataStore({ fields: Field });
    
    var cboDOKTER_viPenJasLab = new Ext.form.ComboBox
	(
            {
                id: 'cboDOKTER_viPenJasLab',
				x: 110,
				y: 160,
                typeAhead: true,
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                emptyText: '',
                fieldLabel:  ' ',
                align: 'Right',
                width: 230,
				emptyText:'Pilih Dokter',
                store: dsdokter_viPenJasLab,
                valueField: 'KD_DOKTER',
                displayField: 'NAMA',
               // value:'All',
				editable: false,
				enableKeyEvents: true,
				
			listeners:
			{
				'specialkey': function (){
						if (Ext.EventObject.getKey() === 13){
							Ext.getCmp('cboKelPasienLab').focus();
						}
					},
				'select': function(a,b,c)
				{
					
					tmpkddoktertujuan=b.data.KD_DOKTER ;
					Ext.getCmp('cboKelPasienLab').focus();
				},
			}
            }
	);
	
    return cboDOKTER_viPenJasLab;
};
var tmpkddoktertujuan;

function mCboKelompokpasien(){  
 var cboKelPasienLab = new Ext.form.ComboBox
	(
		{
			
			id:'cboKelPasienLab',
			fieldLabel: 'Kelompok Pasien',
			x: 110,
			y: 185,
			mode: 'local',
			width: 130,
			forceSelection: true,
			lazyRender:true,
			triggerAction: 'all',
			editable: false,
			store: new Ext.data.ArrayStore
			(
				{
				id: 0,
				fields:
				[
					'Id',
					'displayText'
				],
				   data: [[1, 'Perseorangan'], [2, 'Perusahaan'], [3, 'Asuransi']]
				}
			),			
			valueField: 'Id',
			displayField: 'displayText',
			value:selectSetKelPasien,
			selectOnFocus: true,
			tabIndex:22,
			enableKeyEvents: true,
				
			listeners:
			{
				'specialkey': function (a, b, c){
						if (Ext.EventObject.getKey() === 13){
							if(kelompokPasien == 'Perseorangan' || Ext.getCmp('cboKelPasienLab').getValue() == 1)
						   {
							  // cboPerseorangan
								//Ext.getCmp('cboRujukanDariRequestEntry').hide();
								//Ext.getCmp('cboRujukanRequestEntry').hide();
								Ext.getCmp('cboPerseoranganLab').focus(false,100);
						   }
						   else if(kelompokPasien == 'Perusahaan' || Ext.getCmp('cboKelPasienLab').getValue() == 2)
						   {
								//Ext.getCmp('cboRujukanDariRequestEntryLab').show();
								//Ext.getCmp('cboRujukanRequestEntry').show(); 
								Ext.getCmp('cboPerusahaanRequestEntryLab').focus(false,100);
						   }
						   else if(kelompokPasien == 'Asuransi' || Ext.getCmp('cboKelPasienLab').getValue() == 3)
						   {
								//Ext.getCmp('cboRujukanDariRequestEntryLab').show();
								//Ext.getCmp('cboRujukanRequestEntry').show();  
								 Ext.getCmp('cboAsuransiLab').focus(false,100);
						   }
						}
					},
				'select': function(a, b, c)
					{
					   Combo_Select(b.data.displayText);
					   if(b.data.displayText == 'Perseorangan')
					   {
						  // cboPerseorangan
							//Ext.getCmp('cboRujukanDariRequestEntry').hide();
							//Ext.getCmp('cboRujukanRequestEntry').hide();
							Ext.getCmp('cboPerseoranganLab').focus(false,100);
					   }
					   else if(b.data.displayText == 'Perusahaan')
					   {
							//Ext.getCmp('cboRujukanDariRequestEntryLab').show();
							//Ext.getCmp('cboRujukanRequestEntry').show(); 
							Ext.getCmp('cboPerusahaanRequestEntryLab').focus(false,100);
					   }
					   else if(b.data.displayText == 'Asuransi')
					   {
							//Ext.getCmp('cboRujukanDariRequestEntryLab').show();
							//Ext.getCmp('cboRujukanRequestEntry').show();  
							 Ext.getCmp('cboAsuransiLab').focus(false,100);
					   }
					  
					},
				'render': function(a,b,c)
				{
					
					Ext.getCmp('txtNamaPesertaAsuransiLab').hide();
					Ext.getCmp('txtNoAskesLab').hide();
					Ext.getCmp('txtNoSJPLab').hide();
					
					Ext.getCmp('cboPerseoranganLab').hide();
					Ext.getCmp('cboAsuransiLab').hide();
					Ext.getCmp('cboPerusahaanRequestEntryLab').hide();
					
				}
			}

		}
	);
	return cboKelPasienLab;
};

function mComboPerseorangan()
{
	var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPeroranganLabRequestEntry = new WebApp.DataStore({fields: Field});
    dsPeroranganLabRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
				Sort: 'customer',
			    Sortdir: 'ASC',
			    target: 'ViewComboKontrakCustomer',
			    param: 'jenis_cust=0'
			}
		}
	)
    var cboPerseoranganLab = new Ext.form.ComboBox
	(
		{
			id:'cboPerseoranganLab',
			x: 260,
			y: 185,
			editable: true,
			triggerAction: 'all',
			lazyRender:true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			fieldLabel: 'Jenis',
			tabIndex:23,
			width:150,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			store:dsPeroranganLabRequestEntry,
			value:selectSetPerseorangan,
			listeners:
			{
				'select': function(a,b,c)
				{
					vkode_customer = b.data.KD_CUSTOMER;
				    selectSetPerseorangan=b.data.displayText ;
				}
			}
		}
	);
	return cboPerseoranganLab;
};

function mComboPerusahaan()
{
    var Field = ['KD_CUSTOMER','CUSTOMER'];
    dsPerusahaanRequestEntry = new WebApp.DataStore({fields: Field});
    dsPerusahaanRequestEntry.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
				Sort: 'customer',
			    Sortdir: 'ASC',
			    target: 'ViewComboKontrakCustomer',
			    param: 'jenis_cust=1'
			}
		}
	)
    var cboPerusahaanRequestEntryLab = new Ext.form.ComboBox
	(
		{
		    id: 'cboPerusahaanRequestEntryLab',
			x: 260,
			y: 185,
		    typeAhead: true,
		    triggerAction: 'all',
		    lazyRender: true,
		    mode: 'local',
		    selectOnFocus:true,
			forceSelection: true,
		    emptyText:'Pilih Perusahaan...',
		    fieldLabel: 'Perusahaan',
		    align: 'Right',
			width:150,
		    store: dsPerusahaanRequestEntry,
		    valueField: 'KD_CUSTOMER',
		    displayField: 'CUSTOMER',
			//anchor: '95%',
		    listeners:
			{
			    'select': function(a,b,c)
				{
					vkode_customer = b.data.KD_CUSTOMER;
					selectSetPerusahaan=b.data.valueField ;
				}
			}
		}
	);

    return cboPerusahaanRequestEntryLab;
};

function mComboUnitLab(){ 
    var Field = ['KD_UNIT','NAMA_UNIT'];
    dsunitlab_viPenJasLab= new WebApp.DataStore({ fields: Field });
    dsunitlab_viPenJasLab.load({
		params:{
			Skip: 0,
			Take: 1000,
			Sort: 'nama_unit',
			Sortdir: 'ASC',
			target: 'ViewSetupUnit',
			param: "parent = '4' and kd_unit not in('44','45')"
		}
	});
    var cbounitlab_viPenJasLab = new Ext.form.ComboBox({
		id: 'cboUnitLab_viPenJasLab',
		x: 110,
		y: 130,
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		emptyText: '',
		fieldLabel:  ' ',
		align: 'Right',
		width: 230,
		emptyText:'Pilih Unit',
		store: dsunitlab_viPenJasLab,
		valueField: 'KD_UNIT',
		displayField: 'NAMA_UNIT',
		//value:'All',
		editable: false,
			listeners:
			{
				'select': function(a,b,c)
				{
					tmpkd_unit = b.data.KD_UNIT;
					getComboDokterLab(b.data.KD_UNIT);
				}
			}
	});
    return cbounitlab_viPenJasLab;
}
function mComboDokterPengirim(){ 
    var Field = ['KD_DOKTER','NAMA'];
    dsDokterRequestEntry= new WebApp.DataStore({ fields: Field });
	dsDokterRequestEntry.load({
		params:{
			Skip: 0,
			Take: 1000,
			Sort: 'nama',
			Sortdir: 'ASC',
			target: 'ViewComboDokterPengirim',
			param: ''
		}
	})
    
    var cboDokterPengirimLAB = new Ext.form.ComboBox({
		id: 'cboDokterPengirimLAB',
		x: 110,
		y: 100,
		typeAhead: true,
		triggerAction: 'all',
		lazyRender: true,
		mode: 'local',
		emptyText: '',
		fieldLabel:  ' ',
		align: 'Right',
		width: 230,
		emptyText:'Pilih Dokter Pengirim',
		store: dsDokterRequestEntry,
		valueField: 'KD_DOKTER',
		displayField: 'NAMA',
		//value:'All',
		//editable: false,
			listeners:
			{
				'select': function(a,b,c)
				{
					tmpkddokterpengirim=b.data.KD_DOKTER;
					/* tmpkd_unit = b.data.KD_UNIT;
					getComboDokterLab(b.data.KD_UNIT); */
				}
			}
	});
    return cboDokterPengirimLAB;
}

function mComboAsuransi()
{
var Field_poli_viDaftar = ['KD_CUSTOMER','CUSTOMER'];

    ds_customer_viDaftar = new WebApp.DataStore({fields: Field_poli_viDaftar});

	ds_customer_viDaftar.load
    (
        {
			params:
			{
			    Skip: 0,
			    Take: 1000,
				Sort: 'customer',
			    Sortdir: 'ASC',
			    target: 'ViewComboKontrakCustomer',
			    param: 'jenis_cust=2'
			}
           /*  params:
            {
                Skip: 0,
                Take: 1000,
                Sort: '',
                Sortdir: '',
                target:'ViewComboCustomer',
                param: "status=true"
            } */
        }
    )
    var cboAsuransiLab = new Ext.form.ComboBox
	(
		{
			id:'cboAsuransiLab',
			x: 260,
			y: 185,
			typeAhead: true,
			triggerAction: 'all',
			lazyRender: true,
			mode: 'local',
			selectOnFocus:true,
			forceSelection: true,
			emptyText:'Pilih Asuransi...',
			fieldLabel: 'Asuransi',
			align: 'Right',
			width:150,
			//anchor: '95%',
			store: ds_customer_viDaftar,
			valueField: 'KD_CUSTOMER',
			displayField: 'CUSTOMER',
			enableKeyEvents: true,
				
			listeners:
			{
				'specialkey': function (){
						if (Ext.EventObject.getKey() === 13){
							Ext.getCmp('cboUnitLab_viPenJasLab').focus();
						}
					},
				'select': function(a,b,c)
				{
					vkode_customer = b.data.KD_CUSTOMER;
					selectSetAsuransi=b.data.valueField ;
				}
			}
		}
	);
	return cboAsuransiLab;
};


function Combo_Select(combo)
{
   var value = combo
   //var txtgetnamaPeserta = Ext.getCmp('txtNamaPesertaAsuransiLab') ;

   if(value == "Perseorangan")
   {    
        Ext.getCmp('txtNamaPesertaAsuransiLab').hide()
        Ext.getCmp('txtNoAskesLab').hide()
        Ext.getCmp('txtNoSJPLab').hide()
        Ext.getCmp('cboPerseoranganLab').show()
        Ext.getCmp('cboAsuransiLab').hide()
        Ext.getCmp('cboPerusahaanRequestEntryLab').hide()
        

   }
   else if(value == "Perusahaan")
   {    
        Ext.getCmp('txtNamaPesertaAsuransiLab').hide()
        Ext.getCmp('txtNoAskesLab').hide()
        Ext.getCmp('txtNoSJPLab').hide()
        Ext.getCmp('cboPerseoranganLab').hide()
        Ext.getCmp('cboAsuransiLab').hide()
        Ext.getCmp('cboPerusahaanRequestEntryLab').show()
        
   }
   else
       {
         Ext.getCmp('txtNamaPesertaAsuransiLab').show()
         Ext.getCmp('txtNoAskesLab').show()
         Ext.getCmp('txtNoSJPLab').show()
         Ext.getCmp('cboPerseoranganLab').hide()
         Ext.getCmp('cboAsuransiLab').show()
         Ext.getCmp('cboPerusahaanRequestEntryLab').hide()
         
       }
}


function setdisablebutton()
{
Ext.getCmp('btnLookUpKonsultasi_viKasirLAB').disable();
Ext.getCmp('btnLookUpGantiDokter_viKasirLAB').disable();
Ext.getCmp('btngantipasien').disable();
Ext.getCmp('btnposting').disable();	

Ext.getCmp('btnLookupPenJasLab').disable()
Ext.getCmp('btnSimpanLAB').disable()
Ext.getCmp('btnHpsBrsLAB').disable()
Ext.getCmp('btnHpsBrsDiagnosa').disable()
Ext.getCmp('btnSimpanDiagnosa').disable()
Ext.getCmp('btnLookupDiagnosa').disable()
}

function setenablebutton()
{
//Ext.getCmp('btnLookUpKonsultasi_viKasirLAB').enable();
Ext.getCmp('btnLookUpGantiDokter_viKasirLAB').enable();
Ext.getCmp('btngantipasien').enable();
//Ext.getCmp('btnposting').enable();	

Ext.getCmp('btnLookupPenJasLab').enable()
Ext.getCmp('btnSimpanLAB').enable()
//Ext.getCmp('btnHpsBrsLAB').enable()
//Ext.getCmp('btnHpsBrsDiagnosa').enable()
//Ext.getCmp('btnSimpanDiagnosa').enable()
//Ext.getCmp('btnLookupDiagnosa').enable()	
}

//Criteria untuk pencarian berdasarkan no medrec, nama pasien dll
function getCriteriaFilter_viDaftar()//^^^
{
      	 var strKriteria = "";

           /* if (Ext.get('txtNoMedrecPenJasLab').getValue() != "")
            {
                strKriteria = " pasien.kd_pasien = " + "'" + Ext.get('txtNoMedrecPenJasLab').getValue() +"'";
            }
            
            if (Ext.get('txtNamaPasienPenJasLab').getValue() != "")//^^^
            {
                if (strKriteria == "")
                    {
                        strKriteria = " lower(pasien.nama) " + "LIKE lower('" + Ext.get('txtNamaPasienPenJasLab').getValue() +"%')" ;
                    }
                    else
                        {
                            strKriteria += " lower(pasien.nama) " + "LIKE lower('" + Ext.get('txtNamaPasienPenJasLab').getValue() +"%')";
                        }
                
            }
			if (Ext.get('cboStatusLunas_viPenJasLab').getValue() != "")
            {
				if (Ext.get('cboStatusLunas_viPenJasLab').getValue()==='Belum Lunas')
				{
					if (strKriteria == "")
                    {
                        strKriteria = " transaksi.co_status " + "= 'f'" ;
                    }
                    else
                       {
                            strKriteria += " and transaksi.co_status " + "= 'f'";
                       }
				}
				if (Ext.get('cboStatusLunas_viPenJasLab').getValue()==='Posting')
				{
					if (strKriteria == "")
                    {
                        strKriteria = " transaksi.co_status " + "='t'" ;
                    }
                    else
                       {
                            strKriteria += " and transaksi.co_status " + "='t'";
                       }
				}
				if (Ext.get('cboStatusLunas_viPenJasLab').getValue()==='Semua')
				{
					
				}
                
                
            } */
			if (Ext.get('dtpTglAwalFilterPenJasLab').getValue() != "")
            {
                 if (strKriteria == "")
                    {
                        strKriteria = " and tgl_transaksi >= '" + Ext.get('dtpTglAwalFilterPenJasLab').getValue() + "'" ;
                    }
                    else
                        {
                            strKriteria += " and tgl_transaksi >= '" + Ext.get('dtpTglAwalFilterPenJasLab').getValue()+"'";
                        }
                
            }
			if (Ext.get('dtpTglAkhirFilterPenJasLab').getValue() != "")
            {
                if (strKriteria == "")
                    {
                        strKriteria = " and tgl_transaksi <= '" + Ext.get('dtpTglAkhirFilterPenJasLab').getValue()+"'" ;
                    }
                    else
                        {
                            strKriteria += " and tgl_transaksi <= '" + Ext.get('dtpTglAkhirFilterPenJasLab').getValue()+"'";
                        }
                
            }
			/* if (Ext.get('cboUNIT_viKasirLab').getValue() != "")
            {
					if (strKriteria == "")
                    {
                        strKriteria = " unit.kd_unit ='" + Ext.getCmp('cboUNIT_viKasirLab').getValue() + "'"  ;
                    }
                    else
                       {
                            strKriteria += " and unit.kd_unit ='" + Ext.getCmp('cboUNIT_viKasirLab').getValue() + "'";
                       }
                
            } */
	
	 return strKriteria;
}

function getItemPanelPenJasRad()
{
	//var tmptruefalse = true;
    var items =
    {
        layout:'column',
        border:true,
        items:
        [
            {
                columnWidth:.70,
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 100,
                height: 100,
                anchor: '50% 50%',
                items:
                [
					//-----------COMBO JENIS TRANSAKSI-----------------------------
					{
						x: 10,
						y: 10,
						xtype: 'label',
						text: 'Jenis Transaksi '
					},
					{
						x: 145,
						y: 10,
						xtype: 'label',
						text: ':'
					},
					mComboJenisTrans_viPenJasLab(),
					{
						x: 310,
						y: 10,
						xtype: 'label',
						text: 'Unit Lab '
					},
					{
						x: 445,
						y: 10,
						xtype: 'label',
						text: ':'
					},
					mComboUnitTujuans_viPenJasLab(),
					//--------RADIO BUTTON PILIHAN ASAL PASIEN---------------------
					{
						x: 10,
						y: 40,
						xtype: 'label',
						text: 'Asal Pasien '
					},
					{
						x: 145,
						y: 40,
						xtype: 'label',
						text: ':'
					},
					{
						x: 155,
						y: 40,
						xtype: 'radiogroup',
						id: 'rbrujukanLAB',
						fieldLabel: 'Asal Pasien ',
						items: [
									{
										boxLabel: 'IGD',
										name: 'rb_autoLAB',
										id: 'rb_pilihanLAB1',
										checked: true,
										inputValue: '1',
										handler: function (field, value) {
										if (value === true)
										{
												getDataCariUnitPenjasLab("kd_bagian=3 and parent<>'0'");
												Ext.getCmp('cboUNIT_viKasirLab').show();
												Ext.getCmp('cboRujukanKamarSpesialJasRadRequestEntry').hide();
												radiovaluesLAB='1';
												validasiJenisTrLAB();
												/* tmpparams = "'" + Ext.get('dtpTglAwalFilterPenJasLab').getValue() + "'";//tanya
												tmpunit = 'ViewRwjPenjasLab';
												loadpenjaslab(tmpparams, tmpunit); */
										}
										}
									},
									{
										boxLabel: 'RWJ',
										name: 'rb_autoLAB',
										id: 'rb_pilihanLAB4',
										inputValue: '1',
										handler: function (field, value) {
										if (value === true)
										{
												getDataCariUnitPenjasLab("kd_bagian=2 and type_unit=false");
												Ext.getCmp('cboUNIT_viKasirLab').show();
												Ext.getCmp('cboRujukanKamarSpesialJasRadRequestEntry').hide();
												radiovaluesLAB='4';
												validasiJenisTrLAB();
												/* tmpparams = "'" + Ext.get('dtpTglAwalFilterPenJasLab').getValue() + "'";//tanya
												tmpunit = 'ViewRwjPenjasLab';
												loadpenjaslab(tmpparams, tmpunit); */
										}
										}
									},
									{
										boxLabel: 'RWI',
										name: 'rb_autoLAB',
										id: 'rb_pilihanLAB2',
										inputValue: '2',
										handler: function (field, value) {
										if (value === true)
										{
											Ext.getCmp('cboUNIT_viKasirLab').hide();
											Ext.getCmp('cboRujukanKamarSpesialJasRadRequestEntry').show();
											radiovaluesLAB='2';
											validasiJenisTrLAB();
											/* tmpparams = " transaksi.co_Status='0' ORDER BY transaksi.no_transaksi desc limit 100";
											tmpunit = 'ViewRwiPenjasLab';
											loadpenjaslab(tmpparams, tmpunit); */
										}
									 }
									},
									{
										boxLabel: 'Kunj. Langsung',
										name: 'rb_autoLAB',
										id: 'rb_pilihanLAB3',
										inputValue: 'K.Kd_Pasien',
										handler: function (field, value) {
										if (value === true)
										{
											Ext.getCmp('cboUNIT_viKasirLab').hide();
											Ext.getCmp('cboRujukanKamarSpesialJasRadRequestEntry').show();
											radiovaluesLAB='3';
											validasiJenisTrLAB();
											//PenJasLabLookUp();
											//belum
											Ext.getCmp('txtNamaUnitLAB').setValue="Laboratorium";
										}
									}	
									},
								]
					},
					{
						x: 10,
						y: 70,
						xtype: 'label',
						text: 'Detail Asal Pasien '
					},
					{
						x: 145,
						y: 70,
						xtype: 'label',
						text: ':'
					},
					//COMBO POLI dan KAMAR
					mComboUnit_viKasirLAB(),
					mcomboKamarSpesialLAB(),
					
                ]
            },
			//---------------JARAK 1----------------------------
			 {
                columnWidth:.70,
                layout: 'absolute',
                bodyStyle: 'padding: 0px 0px 0px 0px',
                border: false,
                width: 100,
                height: 5,
                anchor: '50% 50%',
                items:
                [
						
				]
			 },
			//--------------------------------------------------
            {
                columnWidth:.70,
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 100,
                height: 135,
                anchor: '100% 100%',
                items:
                [
					{
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'No. Medrec  '
                    },
					{
                        x: 145,
                        y: 10,
                        xtype: 'label',
                        text: ':'
                    },
					{
						x : 155,
                        y : 10,
                        xtype: 'textfield',
                        fieldLabel: 'No. Medrec (Enter Untuk Mencari)',
                        name: 'txtNoMedrecPenJasLab',
                        id: 'txtNoMedrecPenJasLab',
                        enableKeyEvents: true,
                        listeners:
                        {
                            'specialkey' : function()
                            {
                                var tmpNoMedrec = Ext.get('txtNoMedrecPenJasLab').getValue()
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10 )
                                {
                                    if(tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10 )
                                        {
                                             var tmpgetNoMedrec = formatnomedrec(Ext.get('txtNoMedrecPenJasLab').getValue())
                                             Ext.getCmp('txtNoMedrecPenJasLab').setValue(tmpgetNoMedrec);
                                             /* var tmpkriteria = getCriteriaFilter_viDaftar();
                                             refeshpenjaslab(tmpkriteria); */
                                             validasiJenisTrLAB();
                                        }
                                        else
                                            {
                                                if (tmpNoMedrec.length === 10)
                                                    {
                                                        /* tmpkriteria = getCriteriaFilter_viDaftar();
                                                        refeshpenjaslab(tmpkriteria); */
                                                        validasiJenisTrLAB();
                                                    }
                                                    else
                                                    Ext.getCmp('txtNoMedrecPenJasLab').setValue('')
                                                    validasiJenisTrLAB();
                                            }
                                }
                            }

                        }

                    },	
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Nama Pasien '
                    },
					{
                        x: 145,
                        y: 40,
                        xtype: 'label',
                        text: ':'
                    },
                    {   
                        x : 155,
                        y : 40,
                        xtype: 'textfield',
                        name: 'txtNamaPasienPenJasLab',
                        id: 'txtNamaPasienPenJasLab',
                        width: 200,
                        enableKeyEvents: true,
                        listeners:
                        { 
                                'specialkey' : function()
                                {
                                        if (Ext.EventObject.getKey() === 13) 
                                        {
											//getCriteriaFilter_viDaftar();
											validasiJenisTrLAB();
											/* tmpkriteria = getCriteriaFilter_viDaftar();
                                            refeshpenjaslab(tmpkriteria); */

                                            //RefreshDataFilterPenJasRad();

                                        } 						
                                }
                        }
                    },
					/* {
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Status Posting '
                    },
					{
                        x: 145,
                        y: 70,
                        xtype: 'label',
                        text: ':'
                    },					
                    mComboStatusBayar_viPenJasLab(), */
					{
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Status Lunas '
                    },
					{
                        x: 145,
                        y: 70,
                        xtype: 'label',
                        text: ':'
                    },					
                    mComboStatusLunas_viPenJasLab(),
					{
                        x: 10,
                        y: 100,
                        xtype: 'label',
                        text: 'Tanggal Kunjungan '
                    },
					{
                        x: 145,
                        y: 100,
                        xtype: 'label',
                        text: ':'
                    },
                    {
                        x: 155,
                        y: 100,
                        xtype: 'datefield',
                        id: 'dtpTglAwalFilterPenJasLab',
                        format: 'd/M/Y',
                        value: tigaharilalu
                    },
					{
                        x: 270,
                        y: 100,
                        xtype: 'label',
                        text: 's/d '
                    },
					{
                        x: 305,
                        y: 100,
                        xtype: 'datefield',
                        id: 'dtpTglAkhirFilterPenJasLab',
                        format: 'd/M/Y',
                        value: now,
                        width: 100
                    },
					{
                        x: 415,
                        y: 100,
                        xtype:'button',
                        text: 'Refresh',
                        iconCls: 'refresh',
                        anchor: '25%',
                        width: 70,
                        height: 20,
                        hideLabel: false,
                        handler: function(sm, row, rec)
                        {
                            validasiJenisTrLAB();
							/* tmpparams = getCriteriaFilter_viDaftar();
							refeshpenjaslab(tmpparams); */
                        }
                    }
                    
                ]
            },
			
		//----------------------------------------------------------------	
			
			{
                columnWidth:.10,
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: false,
                width: 10,
                height: 135,
                anchor: '100% 100%',
                items:
				[
					
				]
			},
        //----------------------------------------------------------------    
        ]
    }
    return items;
};


function getTindakanLab(){
	var kd_cus_gettarif=vkode_customer;
	/* if(Ext.get('cboKelPasienLab').getValue()=='Perseorangan'){
		kd_cus_gettarif=Ext.getCmp('cboPerseoranganLab').getValue();
	}else if(Ext.get('cboKelPasienLab').getValue()=='Perusahaan'){
		kd_cus_gettarif=Ext.getCmp('cboPerusahaanRequestEntryLab').getValue();
	}else {
		kd_cus_gettarif=Ext.getCmp('cboAsuransiLab').getValue();
	  } */
	var modul='';
	if(radiovaluesLAB == 1){
		modul='igd';
	} else if(radiovaluesLAB == 2){
		modul='rwi';
	} else if(radiovaluesLAB == 4){
		modul='rwj';
	} else{
		modul='langsung';
	}
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/main/functionLAB/getGridProduk",
			params: {kd_unit:kodeUnitLab,
					 kd_customer:kd_cus_gettarif,
					 notrans:Ext.getCmp('txtNoTransaksiPenJasLab').getValue(),
					 penjas:modul,
                     kdunittujuan:tmpkd_unit
					},
			failure: function(o)
			{
				ShowPesanErrorPenJasLab('Error, pasien! Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				
				if (cst.success === true) 
				{
					
					var recs=[],
						recType=dsDataGrd_getTindakanPenjasLab.recordType;
						
					for(var i=0; i<cst.ListDataObj.length; i++){
				
						recs.push(new recType(cst.ListDataObj[i]));
						
					}
					dsDataGrd_getTindakanPenjasLab.add(recs);
					
					GridGetTindakanPenjasLab.getView().refresh();
					
					for(var i = 0 ; i < dsDataGrd_getTindakanPenjasLab.getCount();i++)
					{
						var o= dsDataGrd_getTindakanPenjasLab.getRange()[i].data;
						for(var je = 0 ; je < dsTRDetailPenJasLabList.getCount();je++)
						{
							
							var p= dsTRDetailPenJasLabList.getRange()[je].data;
							if (o.kd_produk === p.kd_produk)
							{
								o.pilihchkproduklab = true;
								
							}
							
						}
					}
					GridGetTindakanPenjasLab.getView().refresh();
				}
				else 
				{
					ShowPesanErrorPenJasLab('Gagal membaca data pasien ini', 'Error');
				};
			}
		}
		
	)
	
}

function setLookUp_getTindakanPenjasLab(rowdata){
    var lebar = 985;
    setLookUps_getTindakanPenjasLab = new Ext.Window({
        id: 'FormLookUpGetTindakan',
        title: 'Daftar Pemeriksaan', 
        closeAction: 'destroy',        
        width: 600,
        height: 330,
        resizable:false,
		autoScroll: false,
        border: true,
        constrainHeader : true,    
        iconCls: 'Studi_Lanjut',
        modal: true,		
        items: getFormItemEntry_getTindakanPenjasLab(lebar,rowdata),
        listeners:{
            activate: function(){
				
            },
            afterShow: function(){
                this.activate();
            },
            deactivate: function(){
               // rowSelected_getTindakanPenjasLab=undefined;
            }
        }
    });

    setLookUps_getTindakanPenjasLab.show();
   
}


function getFormItemEntry_getTindakanPenjasLab(lebar,rowdata){
    var pnlFormDataBasic_getTindakanPenjasLab = new Ext.FormPanel({
		title: '',
		region: 'north',
		layout: 'form',
		bodyStyle: 'padding:10px 10px 10px 10px',
		anchor: '100%',
		labelWidth: 1,
		autoWidth: true,
		width: lebar,
		border: false,
		items:[
				gridDataViewEdit_getTindakanPenjasLab()
			],
			fileUpload: true,
			tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Tambahkan',
						id:'btnTambahKanPermintaan_getTindakanPenjasLab',
						iconCls: 'add',
						//disabled:true,
						handler:function()
						{
							/* var jumlah=0;
							var items=[];
							for(var i = 0 ; i < dsDataGrd_getTindakanPenjasLab.getCount();i++){
								var o=dsDataGrd_getTindakanPenjasLab.getRange()[i].data;
									if(o.pilihchkproduklab == true){
										jumlah += 1;
										
										// for(var x = 0; x < 100; x++){
											// arr[x] = [];    
											// for(var y = 0; y < 100; y++){ 
												// arr[x][y] = x*y;    
											// }    
										// }
										var recs=[],
											recType=dsTRDetailPenJasLabList.recordType;

										for(var i=0; i<dsDataGrd_getTindakanPenjasLab.getCount(); i++){

											recs.push(new recType(dsDataGrd_getTindakanPenjasLab.data.items[i].data));

										}
										dsTRDetailPenJasLabList.add(recs);

										gridDTItemTest.getView().refresh();
									}
							}
							console.log(items[0][0]) */
							
							
							
							
							// console.log(dsDataGrd_getTindakanPenjasLab.getCount());
							dsTRDetailPenJasLabList.removeAll();
							var recs=[],
							recType=dsTRDetailPenJasLabList.recordType;
							var adadata=false;
                            for(var i = 0 ; i < dsDataGrd_getTindakanPenjasLab.modified.length;i++)
                            {
                                // console.log(dsDataGrd_getTindakanPenjasLab.data.items[i]);
                                if (dsDataGrd_getTindakanPenjasLab.modified[i].data.pilihchkproduklab === true)
                                {
                                    // console.log(dsDataGrd_getTindakanPenjasLab.data.items[i].data);
                                    recs.push(new recType(dsDataGrd_getTindakanPenjasLab.modified[i].data));
                                    adadata=true;
                                }
                            }
							dsTRDetailPenJasLabList.add(recs);
							gridDTItemTest.getView().refresh(); 
							// console.log(recs);
							if (adadata===true)
							{
								setLookUps_getTindakanPenjasLab.close(); 
								loadMask.show();
								Datasave_PenJasLab(false); 
								loadMask.hide();
							}
							else
							{
								ShowPesanWarningPenJasLab('Ceklis data item pemeriksaan  ','Laboratorium');
							}
							
						}
						  
					},
					
					
				]
			}//,items:
		}
    )

    return pnlFormDataBasic_getTindakanPenjasLab;
}

function RefreshDatahistoribayar_LAB(no_transaksi)
{
    var strKriteriaKasirLAB = '';

    strKriteriaKasirLAB = 'no_transaksi= ~' + no_transaksi + '~ and kd_kasir= ~'+kdkasir+'~';

    dsTRDetailHistoryList_lab.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: 'tgl_transaksi',
                                    Sortdir: 'ASC',
                                    target: 'ViewHistoryBayar',
                                    param: strKriteriaKasirLAB
                                }
                    }
            );
    return dsTRDetailHistoryList_lab;
}
;

function GetDTLTRHistoryGrid()
{

    var fldDetailLAB = ['NO_TRANSAKSI', 'TGL_BAYAR', 'DESKRIPSI', 'URUT', 'BAYAR', 'USERNAME', 'SHIFT', 'KD_USER'];

    dsTRDetailHistoryList_lab = new WebApp.DataStore({fields: fldDetailLAB})

    var gridDTLTRHistory = new Ext.grid.EditorGridPanel
            (
                    {
                        title: 'History Bayar',
                        stripeRows: true,
                        store: dsTRDetailHistoryList_lab,
                        border: false,
                        columnLines: true,
                        frame: false,
                        anchor: '100% 25%',
                        autoScroll: true,
                        sm: new Ext.grid.CellSelectionModel
                                (
                                        {
                                            singleSelect: true,
                                            listeners:
                                                    {
                                                        cellselect: function (sm, row, rec)
                                                        {
															console.log(row);
                                                            cellSelecteddeskripsi = dsTRDetailHistoryList_lab.getAt(row);
                                                            CurrentHistory.row = row;
                                                            CurrentHistory.data = cellSelecteddeskripsi;
															Ext.getCmp('btnHpsBrsKasirLAB').enable();
                                                        }
                                                    }
                                        }
                                ),
                        cm: TRHistoryColumModel(),
                        viewConfig: {forceFit: true},
                        tbar:
                                [
                                    {
                                        id: 'btnHpsBrsKasirLAB',
                                        text: 'Hapus Pembayaran',
                                        tooltip: 'Hapus Baris',
                                        iconCls: 'RemoveRow',
                                        //hidden :true,
                                        handler: function ()
                                        {
                                            if (dsTRDetailHistoryList_lab.getCount() > 0)
                                            {
                                                if (cellSelecteddeskripsi != undefined)
                                                {
                                                    if (CurrentHistory != undefined)
                                                    {
                                                        HapusBarisDetailbayar();
                                                    }
                                                } else
                                                {
                                                    ShowPesanWarningPenJasLab('Pilih record ', 'Hapus data');
                                                }
                                            }
                                            /* Ext.getCmp('btnEditKasirLAB').disable();
                                            Ext.getCmp('btnTutupTransaksiKasirLAB').disable();
                                            Ext.getCmp('btnHpsBrsKasirLAB').disable(); */
                                        },
                                        disabled: true
                                    }
                                ]
                    }

            );

    return gridDTLTRHistory;
}
;
function HapusBarisDetailbayar()
{
    if (cellSelecteddeskripsi != undefined)
    {
        if (cellSelecteddeskripsi.data.NO_TRANSAKSI != '' && cellSelecteddeskripsi.data.URUT != '')
        {
            //'NO_TRANSAKSI','TGL_BAYAR','DESKRIPSI','URUT','BAYAR','USERNAME'
            /*var dlg = Ext.MessageBox.prompt('Keterangan', 'Masukan alasan penghapusan:', function(btn, combo){
             if (btn == 'ok')
             {
             variablehistori=combo;
             if (variablehistori!='')
             {
             DataDeleteKasirLABKasirDetail();
             }
             else
             {
             ShowPesanWarningKasirLAB('Silahkan isi alasan terlebih dahaulu','Keterangan');
             }
             }	
             });  */
            msg_box_alasanhapus_LAB();
        } else
        {
            dsTRDetailHistoryList.removeAt(CurrentHistory.row);
        }
        ;
    }
}
;
function msg_box_alasanhapus_LAB()
{
    var lebar = 250;
    form_msg_box_alasanhapus_LAB = new Ext.Window
            (
                    {
                        id: 'alasan_hapusLAb',
                        title: 'Alasan Hapus Pembayaran',
                        closeAction: 'destroy',
                        width: lebar,
                        height: 100,
                        border: false,
                        resizable: false,
                        plain: true,
                        layout: 'fit',
                        iconCls: 'Request',
                        modal: true,
                        items:
                                {
                                    columnWidth: 250,
                                    layout: 'form',
                                    labelWidth: 1,
                                    border: false,
                                    items:
                                            [
                                                {
                                                    xtype: 'tbspacer',
                                                    height: 4
                                                },
												{
													xtype: 'textfield',
													//fieldLabel: 'No. Transaksi ',
													name: 'txtAlasanHapusLAB',
													id: 'txtAlasanHapusLAB',
													emptyText: 'Alasan Hapus',
													anchor: '99%',
												},
                                               //mComboalasan_hapusLAb(),
                                                {
                                                    layout: 'hBox',
                                                    border: false,
                                                    bodyStyle: 'padding:5px 0px 20px 20px',
                                                    defaults: {margins: '3 3 1 1'},
                                                    anchor: '95%',
                                                    layoutConfig:
                                                            {
                                                                align: 'middle',
                                                                pack: 'end'
                                                            },
                                                    items:
                                                            [
                                                                {
                                                                    xtype: 'button',
                                                                    text: 'hapus',
                                                                    width: 70,
                                                                    //style:{'margin-left':'0px','margin-top':'0px'},
                                                                    hideLabel: true,
                                                                    id: 'btnOkalasan_hapusLAb',
                                                                    handler: function ()
                                                                    {
                                                                        DataDeleteKasirLABKasirDetail();
                                                                        form_msg_box_alasanhapus_LAB.close();
                                                                    }
                                                                },
                                                                {
                                                                    xtype: 'button',
                                                                    text: 'Batal',
                                                                    width: 70,
                                                                    hideLabel: true,
                                                                    id: 'btnCancelalasan_hapusLAb',
                                                                    handler: function ()
                                                                    {
                                                                        form_msg_box_alasanhapus_LAB.close();
                                                                    }
                                                                }
                                                            ]

                                                }

                                            ]
                                }








                    }


            );

    form_msg_box_alasanhapus_LAB.show();
}
;

function mComboalasan_hapusLAb()
{
    var Field = ['KD_ALASAN', 'ALASAN'];

    var dsalasan_hapusLAb = new WebApp.DataStore({fields: Field});

    dsalasan_hapusLAb.load
            (
                    {
                        params:
                                {
                                    Skip: 0,
                                    Take: 1000,
                                    Sort: '',
                                    Sortdir: 'ASC',
                                    target: 'alasanhapus',
                                    param: ''
                                }
                    }
            );
    var cboalasan_hapusLAb = new Ext.form.ComboBox
            (
                    {
                        id: 'cboalasan_hapusLAb',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        selectOnFocus: true,
                        forceSelection: true,
                        emptyText: 'Pilih Alasan...',
                        labelWidth: 1,
                        store: dsalasan_hapusLAb,
                        valueField: 'KD_ALASAN',
                        displayField: 'ALASAN',
                        anchor: '95%',
                        listeners:
                                {
                                    'select': function (a, b, c)
                                    {
//                                  var selectalasan_hapusLAb = b.data.KD_PROPINSI;
                                        //alert("is");
                                        selectDokter = b.data.KD_DOKTER;
                                    },
                                    'render': function (c)
                                    {
                                        c.getEl().on('keypress', function (e) {
                                            if (e.getKey() == 13) //atau Ext.EventObject.ENTER
                                                Ext.getCmp('kelPasien').focus();
                                        }, c);
                                    }
                                }
                    }
            );

    return cboalasan_hapusLAb;
}
;
function DataDeleteKasirLABKasirDetail()
{
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/functionKasirPenunjang/deletedetail_bayar",
                        params: getParamDataDeleteKasirLABKasirDetail(),
                        success: function (o)
                        {
                            //	RefreshDatahistoribayar_LAB(Kdtransaksi);
                           // RefreshDataFilterKasirLABKasir();
                            //RefreshDatahistoribayar_LAB('0');
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {
								ShowPesanInfoPenJasLab("Proses Hapus Pembayaran Berhasil", nmHeaderHapusData);
								Ext.getCmp('btnPembayaranPenjasLAB').enable();
								Ext.getCmp('btnTutupTransaksiPenjasLAB').enable();
								RefreshDatahistoribayar_LAB(Ext.getCmp('txtNoTransaksiPenJasLab').getValue());
								if (radiovaluesLAB !=='3' && combovalues !== 'Transaksi Baru'){
									validasiJenisTrLAB();
								}
                                //alert(Kdtransaksi);					 

                                //refeshKasirLABKasir();
                            } else if (cst.success === false && cst.pesan === 0)
                            {
                                ShowPesanWarningPenJasLab(nmPesanHapusGagal, nmHeaderHapusData);
                            } else
                            {
                                ShowPesanWarningPenJasLab(nmPesanHapusError, nmHeaderHapusData);
                            }
                            ;
                        }
                    }
            )
}
;

function getParamDataDeleteKasirLABKasirDetail()
{
    var params =
            {
                TrKodeTranskasi: CurrentHistory.data.data.NO_TRANSAKSI,
                TrTglbayar: CurrentHistory.data.data.TGL_BAYAR,
                Urut: CurrentHistory.data.data.URUT,
                Jumlah: CurrentHistory.data.data.BAYAR,
                Username: CurrentHistory.data.data.USERNAME,
                Shift: tampungshiftsekarang,
                Shift_hapus: CurrentHistory.data.data.SHIFT,
                Kd_user: CurrentHistory.data.data.KD_USER,
                Tgltransaksi: TglTransaksi,
                Kodepasein: kodepasien,
                NamaPasien: namapasien,
                KodeUnit: kodeunit,
                Namaunit: namaunit,
                Kodepay: kodepay,
                Uraian: CurrentHistory.data.data.DESKRIPSI,
                KDkasir: kdkasir,
                KeTterangan: Ext.get('txtAlasanHapusLAB').dom.value

            };
    Kdtransaksi = CurrentHistory.data.data.NO_TRANSAKSI;
    return params
}
;


function TRHistoryColumModel()
{
    return new Ext.grid.ColumnModel
            (
                    [
                        new Ext.grid.RowNumberer(),
                        {
                            id: 'colKdTransaksi',
                            header: 'No. Transaksi',
                            dataIndex: 'NO_TRANSAKSI',
                            width: 100,
                            menuDisabled: true,
                            hidden: false
                        },
                        {
                            id: 'colTGlbayar',
                            header: 'Tgl Bayar',
                            dataIndex: 'TGL_BAYAR',
                            menuDisabled: true,
                            width: 100,
                            renderer: function (v, params, record)
                            {
                                return ShowDate(record.data.TGL_BAYAR);

                            }

                        },
                        {
                            id: 'coleurutmasuk',
                            header: 'urut Bayar',
                            dataIndex: 'URUT',
                            //hidden:true

                        },
                        {
                            id: 'colePembayaran',
                            header: 'Pembayaran',
                            dataIndex: 'DESKRIPSI',
                            width: 150,
                            hidden: false

                        },
                        {
                            id: 'colJumlah',
                            header: 'Jumlah',
                            width: 150,
                            align: 'right',
                            dataIndex: 'BAYAR',
                            hidden: false,
                            renderer: function (v, params, record)
                            {
                                return formatCurrency(record.data.BAYAR);

                            }
                        },
                        {
                            id: 'coletglmasuk',
                            header: 'tgl masuk',
                            dataIndex: '',
                            hidden: true
                        },
                        {
                            id: 'colStatHistory',
                            header: 'History',
                            width: 130,
                            menuDisabled: true,
                            dataIndex: '',
                            hidden: true
                        },
                        {
                            id: 'colPetugasHistory',
                            header: 'Petugas',
                            width: 130,
                            menuDisabled: true,
                            dataIndex: 'USERNAME',
                            //hidden:true
                        }
                    ]
                    )
}
;
//var a={};
function gridDataViewEdit_getTindakanPenjasLab(){
    var FieldGrdKasir_getTindakanPenjasLab = [];
    dsDataGrd_getTindakanPenjasLab= new WebApp.DataStore({
        fields: FieldGrdKasir_getTindakanPenjasLab
    });
    chkgetTindakanPenjasLab = new Ext.grid.CheckColumn
		(
			{
				
				id: 'chkgetTindakanPenjasLab',
				header: 'Pilih',
				align: 'center',
				//disabled:false,
				sortable: true,
				dataIndex: 'pilihchkproduklab',
				anchor: '10% 100%',
				width: 30,
				listeners: 
				{
					  
				},
				/* renderer: function (value, metaData, record, rowIndex, colIndex, store) {
						switch (value) {
							case 't':
								alert('hai');
								//return true;
								
								break;
							case 'f':
								alert('fei');
								//return false;
								break;
						}
						//return false;
					} */
				
		
			}
		); 
    GridGetTindakanPenjasLab =new Ext.grid.EditorGridPanel({
		xtype: 'editorgrid',
		//title: 'Dafrar Pemeriksaan',
		store: dsDataGrd_getTindakanPenjasLab,
		autoScroll: true,
		columnLines: true,
		border: false,
		height: 250,
		anchor: '100% 100%',
		plugins: [new Ext.ux.grid.FilterRow()],
		selModel: new Ext.grid.RowSelectionModel
				(
						{
							singleSelect: true,
							listeners:
									{
									}
						}
				),
		listeners:
				{
					// Function saat ada event double klik maka akan muncul form view
					rowclick: function (sm, ridx, cidx)
					{
						
					},
					rowdblclick: function (sm, ridx, cidx)
					{
						
					}

				},
		/* xtype: 'editorgrid',
        store: dsDataGrd_getTindakanPenjasLab,
        height: 250,
        autoScroll: true,
		columnLines: true,
		border: false,
		plugins: [ new Ext.ux.grid.FilterRow(),chkgetTindakanPenjasLab ],
		selModel: new Ext.grid.RowSelectionModel ({
            singleSelect: true,
            listeners:{
                rowclick: function(sm, row, rec){
					/* rowSelectedGridPasien_viGzPermintaanDiet = undefined;
					rowSelectedGridPasien_viGzPermintaanDiet = dsDataGrdPasien_viGzPermintaanDiet.getAt(row);
					CurrentDataPasien_viGzPermintaanDiet
					CurrentDataPasien_viGzPermintaanDiet.row = row;
					CurrentDataPasien_viGzPermintaanDiet.data = rowSelectedGridPasien_viGzPermintaanDiet.data;
					
					dsTRDetailDietGzPermintaanDiet.removeAll();
					getGridWaktu(Ext.getCmp('txtTmpNoPermintaanGzPermintaanDietL').getValue(),CurrentDataPasien_viGzPermintaanDiet.data.kd_pasien);
					
					if(GzPermintaanDiet.vars.status_order == 'false'){
						Ext.getCmp('btnAddDetDietGzPermintaanDietL').enable();
						Ext.getCmp('btnDeleteDet_viGzPermintaanDiet').enable();
					} else{
						Ext.getCmp('btnAddPasienGzPermintaanDietL').disable();
						Ext.getCmp('btnAddDetDietGzPermintaanDietL').disable();
					}
					Ext.getCmp('txtTmpKdPasienGzPermintaanDietL').setValue(CurrentDataPasien_viGzPermintaanDiet.data.kd_pasien);
					 
                },
            }
        }), */
        //stripeRows: true,
		
        colModel:new Ext.grid.ColumnModel([
        	//new Ext.grid.RowNumberer(),	
			{
				dataIndex: 'kd_produk',
				header: 'KD Produk',
				sortable: true,
				width: 70,
				hidden : true,
			},{
				dataIndex: 'kp_produk',
				header: 'Kode Produk',
				sortable: true,
				width: 70,
				 filter: {}
			},
			{
				dataIndex: 'deskripsi',
				header: 'Nama Pemeriksaan',
				width: 70,
				align:'left',
				 filter: {}
			},{
				dataIndex: 'uraian',
				header: 'Uraian',
				hidden : true,
				sortable: true,
				width: 70
			},
			{
				dataIndex: 'kd_tarif',
				header: 'Kd Tarif',
				width: 70,
				hidden : true,
				align:'center'
			},{
				dataIndex: 'tgl_transaksi',
				header: 'Tgl Transaksi',
				hidden : true,
				sortable: true,
				width: 70
			},
			{
				dataIndex: 'tgl_berlaku',
				header: 'Tgl Berlaku',
				width: 70,
				hidden : true,
				align:'center'
			},{
				dataIndex: 'harga',
				header: 'harga',
				hidden : true,
				sortable: true,
				width: 70
			},
			{
				dataIndex: 'qty',
				header: 'qty',
				width: 70,
				hidden : true,
				align:'center'
			},{
				dataIndex: 'jumlah',
				header: 'jumlah',
				width: 70,
				hidden : true,
				align:'center'
			},
			chkgetTindakanPenjasLab
        ]),
		viewConfig:{
			forceFit: true
		} 
    });
    return GridGetTindakanPenjasLab;
}

function getDokterPengirim(no_transaksi,kd_kasir){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/main/functionLAB/getDokterPengirim",
			params:{no_transaksi:no_transaksi,kd_kasir:kd_kasir} ,
			failure: function(o)
			{
				ShowPesanErrorPenJasLab('Hubungi Admin', 'Error');
			},
			success: function(o)
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true)
				{
					//Ext.getCmp('txtDokterPengirimLAB').setValue(cst.nama);
					Ext.getCmp('cboDokterPengirimLAB').setValue(cst.nama);
				}
				else
				{
					ShowPesanErrorPenJasLab('Gagal membaca dokter pengirim', 'Error');
				};
			}
		}

	)
}

function printbillRadLab()
{
	if (tombol_bayar==='disable'){
    Ext.Ajax.request
            (
                    {
                        url: baseURL + "index.php/main/CreateDataObj",
                        params: dataparamcetakbill_vikasirDaftarRadLab(),
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {

                            } else if (cst.success === false && cst.pesan === 0)
                            {
                                ShowPesanWarning_viDaftar('Data tidak berhasil di simpan ' + cst.pesan, 'Simpan Data');
                            } else
                            {
                                ShowPesanError_viDaftar('Data tidak berhasil di simpan ' + cst.pesan, 'Simpan Data');
                            }
                        }
                    }
            );
	}else{
		ShowPesanWarningPenJasLab('Lakukan pembayaran atau transfer terlebih dahulu !', 'Cetak Data');
	}
}
;

var tmpkdkasirRadLab = '1';
function dataparamcetakbill_vikasirDaftarRadLab()
{
    var paramscetakbill_vikasirDaftarRadLab =
            {
                Table: 'DirectPrintingRadLab',
				notrans_asal:notransaksiasal_lab,
                No_TRans: Ext.get('txtNoTransaksiKasirLABKasir').getValue(),
                KdKasir: kdkasir,
                kdUnit: vkd_unit,
                modul: 'lab',
                JmlBayar: Ext.get('txtJumlah2EditData_viKasirLAB').getValue(),
                JmlDibayar: Ext.get('txtJumlahEditData_viKasirLAB').getValue(),
                printer: Ext.getCmp('cbopasienorder_printer_kasirlab').getValue()
            };
	var urut=[];
	var kd_produk=[];
	var items=Ext.getCmp('gridDTLTRKasirLAB').getStore().data.items;
	for(var i=0,iLen=items.length; i<iLen; i++){
		if(items[i].data.TAG==true){
			urut.push(items[i].data.URUT);
			kd_produk.push(items[i].data.KD_PRODUK);
		}
	}
	paramscetakbill_vikasirDaftarRadLab['no_urut[]']=urut;
	paramscetakbill_vikasirDaftarRadLab['kd_produk[]']=kd_produk;
    return paramscetakbill_vikasirDaftarRadLab;
    return paramscetakbill_vikasirDaftarRadLab;
}
;

function printkwitansiRadLab()
{
	if (tombol_bayar==='disable'){
    Ext.Ajax.request
            (
                    {
                        //url: baseURL + "index.php/main/CreateDataObj",
                        url: baseURL + "index.php/kasir_penunjang/direct_kwitansi/cetak",
                        params: dataparamcetakkwitansi_vikasirDaftarRadLab(),
                        success: function (o)
                        {
                            var cst = Ext.decode(o.responseText);
                            if (cst.success === true)
                            {

                            } else if (cst.success === false )
                            {
                                ShowPesanWarningKasirLAB('Tidak berhasil melakukan pencetakan ' + cst.pesan, 'Cetak Data');
                            } else
                            {
                                ShowPesanWarningKasirLAB('Tidak berhasil melakukan pencetakan ' + cst.pesan, 'Cetak Data');
                            }
                        }
                    }
            );
	}else{
		ShowPesanWarningPenJasLab('Lakukan pembayaran atau transfer terlebih dahulu !', 'Cetak Data');
	}
}
;

function dataparamcetakkwitansi_vikasirDaftarRadLab()
{
    var paramscetakbill_vikasirDaftarRadLab =
            {
                Table: 'DirectKwitansiRadLab',
                No_TRans: Ext.get('txtNoTransaksiKasirLABKasir').getValue(),
                KdKasir: kdkasir,
                kdUnit: vkd_unit,
                modul: 'lab',
                JmlBayar: Ext.get('txtJumlah2EditData_viKasirLAB').getValue(),
                JmlDibayar: Ext.get('txtJumlahEditData_viKasirLAB').getValue(),
                printer: Ext.getCmp('cbopasienorder_printer_kasirlab').getValue()
            };
	var urut=[];
	var kd_produk=[];
	var items=Ext.getCmp('gridDTLTRKasirLAB').getStore().data.items;
	for(var i=0,iLen=items.length; i<iLen; i++){
		if(items[i].data.TAG==true){
			urut.push(items[i].data.URUT);
			kd_produk.push(items[i].data.KD_PRODUK);
		}
	}
	paramscetakbill_vikasirDaftarRadLab['no_urut[]']=urut;
	paramscetakbill_vikasirDaftarRadLab['kd_produk[]']=kd_produk;
    return paramscetakbill_vikasirDaftarRadLab;
}
;

function mCombo_printer_kasirLAB()
{ 
 
    var Field = ['kd_pasien','nama','kd_dokter','kd_unit','kd_customer','no_transaksi','kd_kasir','id_mrresep','urut_masuk','tgl_masuk','tgl_transaksi'];

    dsprinter_kasirLAB = new WebApp.DataStore({ fields: Field });
    
    load_data_printer_kasirLAB();
    var cbo_printer_kasirLAB= new Ext.form.ComboBox
    (
        {
            id: 'cbopasienorder_printer_kasirLAB',
            typeAhead       : true,
            triggerAction   : 'all',
            lazyRender      : true,
            mode            : 'local',
            emptyText: 'Pilih Printer',
            fieldLabel:  '',
            align: 'Right',
            width: 100,
            store: dsprinter_kasirLAB,
            valueField: 'name',
            displayField: 'name',
            //hideTrigger       : true,
            listeners:
            {
                                
            }
        }
    );return cbo_printer_kasirLAB;
};

function load_data_printer_kasirLAB(param)
{

    Ext.Ajax.request(
    {
        url: baseURL + "index.php/main/functionLAB/getPrinter",
        params:{
            command: param
        } ,
        failure: function(o)
        {
             var cst = Ext.decode(o.responseText);
            
        },      
        success: function(o) {
            //cbopasienorder_mng_apotek.store.removeAll();
                var cst = Ext.decode(o.responseText);

            for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
                var recs    = [],recType = dsprinter_kasirLAB.recordType;
                var o=cst['listData'][i];
                
                recs.push(new recType(o));
                dsprinter_kasirLAB.add(recs);
                console.log(o);
            }
        }
    });
}


function Datasave_PenJasLab_SQL(mBol) 
{   
     if (ValidasiEntryPenJasLab(nmHeaderSimpanData,false) == 1 )
     {
            Ext.Ajax.request
             (
                {
                    url: baseURL + "index.php/main/CreateDataObj",
                    params: getParamDetailTransaksiLAB_SQL(),
                    failure: function(o)
                    {
                        ShowPesanWarningPenJasLab('Error simpan. Hubungi Admin!', 'Gagal');
                        ViewGridBawahLookupPenjasLab(Ext.get('txtNoTransaksiPenJasLab').dom.value);
                    },  
                    success: function(o) 
                    {
                        var cst = Ext.decode(o.responseText);
                        if (cst.success === true) 
                        {
                            // loadMask.hide();
                            // ShowPesanInfoPenJasLab("Berhasil menyimpan data ini","Information");
                            // Ext.get('txtNoTransaksiPenJasLab').dom.value=cst.notrans;
                            // Ext.get('txtNoMedrecLAB').dom.value=cst.kdPasien;
                            // ViewGridBawahLookupPenjasLab(Ext.get('txtNoTransaksiPenJasLab').dom.value);
                            // Ext.getCmp('btnPembayaranPenjasLAB').enable();
                            // Ext.getCmp('btnTutupTransaksiPenjasLAB').enable();
                            // if(mBol === false)
                            // {
                            //     ViewGridBawahLookupPenjasLab(Ext.get('txtNoTransaksiPenJasLab').dom.value);
                            // };
                        }
                        else 
                        {
                                ShowPesanWarningPenJasLab('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
                        };
                    }
                }
            )
        
     }
     else
    {
        if(mBol === true)
        {
            return false;
        };
    }; 
    
};

function Datasave_LAB_LIS(mBol) 
{   
     /* if (ValidasiEntryPenJasLab(nmHeaderSimpanData,false) == 1 )
     { */
            Ext.Ajax.request
             (
                {
                    url: baseURL + "index.php/main/functionLAB/savedblis",
                    params: getParamDetailTransaksiLAB_SQL(),
                    failure: function(o)
                    {
                        ShowPesanWarningPenJasLab('Error simpan. Hubungi Admin!', 'Gagal');
                        ViewGridBawahLookupPenjasLab(Ext.get('txtNoTransaksiPenJasLab').dom.value,kd_kasir_lab);
                    },  
                    success: function(o) 
                    {
                        var cst = Ext.decode(o.responseText);
                        if (cst.success === true) 
                        {
                            // loadMask.hide();
                            // ShowPesanInfoPenJasLab("Berhasil menyimpan data ini","Information");
                            // Ext.get('txtNoTransaksiPenJasLab').dom.value=cst.notrans;
                            // Ext.get('txtNoMedrecLAB').dom.value=cst.kdPasien;
                            // ViewGridBawahLookupPenjasLab(Ext.get('txtNoTransaksiPenJasLab').dom.value);
                            // Ext.getCmp('btnPembayaranPenjasLAB').enable();
                            // Ext.getCmp('btnTutupTransaksiPenjasLAB').enable();
                            // if(mBol === false)
                            // {
                            //     ViewGridBawahLookupPenjasLab(Ext.get('txtNoTransaksiPenJasLab').dom.value);
                            // };
                        }
                        else 
                        {
                                ShowPesanWarningPenJasLab('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
                        };
                    }
                }
            )
        
     /* }
     else
    {
        if(mBol === true)
        {
            return false;
        };
    };  */
    
};

function getParamDetailTransaksiLAB_SQL() 
{
    
    var KdCust='';
    var TmpCustoLama='';
    var pasienBaru;
    var modul='';
    if(radiovaluesLAB == 1){
        modul='igd';
    } else if(radiovaluesLAB == 2){
        modul='rwi';
    } else if(radiovaluesLAB == 4){
        modul='rwj';
    } else{
        modul='langsung';
    }
    
    if(Ext.getCmp('txtNoMedrecLAB').getValue() == ''){
        pasienBaru=1;
    } else{
        pasienBaru=0;
    }
    var params =
    {
        Table: 'SQL_LAB',
        KdTransaksi: Ext.get('txtNoTransaksiPenJasLab').getValue(),
        KdPasien:Ext.getCmp('txtNoMedrecLAB').getValue(),
        NmPasien:Ext.getCmp('txtNamaPasienLAB').getValue(),
        Ttl:Ext.getCmp('dtpTtlLAB').getValue(),
        Alamat:Ext.getCmp('txtAlamatLAB').getValue(),
        JK:jenisKelaminPasien,
        GolDarah:Ext.getCmp('cboGDRLAB').getValue(),
        KdUnit: Ext.getCmp('txtKdUnitLab').getValue(),
        KdDokter:tmpkddoktertujuan,
        KdUnitTujuan:tmpkd_unit,
        Tgl: Ext.getCmp('dtpKunjunganLAB').getValue(),
        TglTransaksiAsal:TglTransaksi,
		Tlp: Ext.getCmp('txtnotlplab').getValue(),
        urutmasuk:Ext.getCmp('txtKdUrutMasuk').getValue(),
        KdCusto:vkode_customer,
        TmpCustoLama:vkode_customer,//Ext.getCmp('txtKdCustomerLamaHide').getValue(),//jika customer dengan transaksi lama
        NamaPesertaAsuransi:Ext.getCmp('txtNamaPesertaAsuransiLab').getValue(),
        NoAskes:Ext.getCmp('txtNoAskesLab').getValue(),
        NoSJP:Ext.getCmp('txtNoSJPLab').getValue(),
        Shift:tampungshiftsekarang,
        List:getArrPenJasLab(),
        JmlField: mRecordLAB.prototype.fields.length-4,
        JmlList:GetListCountDetailTransaksiLAB(),
        dtBaru:databaru,
        Hapus:1,
        Ubah:0,
        unit:41,
        TmpNotransaksi:TmpNotransaksi,
        KdKasirAsal:KdKasirAsal,
        KdSpesial:Kd_Spesial,
        Kamar:No_Kamar,
        pasienBaru:pasienBaru,
        listTrDokter : '',
        Modul:modul,
        KdProduk:'',
        URUT :tmp_urut_sql,
        kunjungan_lab:tmppasienbarulama,
        kelompok:Ext.getCmp('cboKelPasienLab').getValue(),
		//-----------ABULHASSAN-----------20_01_2017
		kodeKasir_LIS:tmpkd_kasir_sql,
		namaCusto_LIS: namaCustomer_LIS,
		guarantorNM_LIS: namaKelompokPasien_LIS,
		kodeDokterPengirim_LIS:tmpkddokterpengirim
        
    };
    return params
};