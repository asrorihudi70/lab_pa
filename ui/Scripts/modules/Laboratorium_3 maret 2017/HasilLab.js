var CurrentHasilLab =
{
    data: Object,
    details: Array,
    row: 0
};

var tanggaltransaksitampung;
var mRecordRwj = Ext.data.Record.create
(
    [
       {name: 'DESKRIPSI2', mapping:'DESKRIPSI2'},
       {name: 'KD_PRODUK', mapping:'KD_PRODUK'},
       {name: 'DESKRIPSI', mapping:'DESKRIPSI'},
       {name: 'KD_TARIF', mapping:'KD_TARIF'},
       {name: 'HARGA', mapping:'HARGA'},
       {name: 'QTY', mapping:'QTY'},
       {name: 'TGL_TRANSAKSI', mapping:'TGL_TRANSAKSI'},
       {name: 'DESC_REQ', mapping:'DESC_REQ'},
      // {name: 'KD_TARIF', mapping:'KD_TARIF'},
       {name: 'URUT', mapping:'URUT'}
    ]
);
var CurrentDiagnosa =
{
    data: Object,
    details: Array,
    row: 0
};

var FormLookUpGantidokter;
var mRecordDiagnosa = Ext.data.Record.create
(
    [
       {name: 'KASUS', mapping:'KASUS'},
       {name: 'KD_PENYAKIT', mapping:'KD_PENYAKIT'},
       {name: 'PENYAKIT', mapping:'PENYAKIT'},
       //{name: 'KD_TARIF', mapping:'KD_TARIF'},
      // {name: 'HARGA', mapping:'HARGA'},
       {name: 'STAT_DIAG', mapping:'STAT_DIAG'},
       {name: 'TGL_TRANSAKSI', mapping:'TGL_TRANSAKSI'},
      // {name: 'DESC_REQ', mapping:'DESC_REQ'},
      // {name: 'KD_TARIF', mapping:'KD_TARIF'},
       {name: 'URUT_MASUK', mapping:'URUT_MASUK'}
    ]
);

var kdUnitLab_HasilLab;
var dsTRDetailDiagnosaList;
var AddNewDiagnosa = true;
var selectCountDiagnosa = 50;
var now = new Date();
var rowSelectedDiagnosa;
var cellSelecteddeskripsi;
var FormLookUpsdetailTRDiagnosa;
var valueStatusCMDiagnosaView='All';
var nowTglTransaksi = new Date();

var labelisi;
var jeniscus;
var variablehistori;
var selectCountStatusByr_viKasirRwj='Belum Posting';
var selectCountJenTr_viPenJasLab='Transaksi Lama'
var dsTRHasilLabList;
var dsTRDetailHasilLabList;
var AddNewPenJasRad = true;
var selectCountPenJasRad = 50;
var tigaharilalu = new Date().add(Date.DAY, -3);

var tmpkriteria;
var rowSelectedHasilLab;
var cellSelecteddeskripsi;
var FormLookUpsdetailHasilLab;
var valueStatusCMRWJView='All';
var nowTglTransaksi = new Date();
var KelompokPasienAddNew=true;

var tmpnama_unit_asal;
var tmpkd_dokter_asal;
var tmpjam_masuk;
var tmpnama_dokter_asal;

//var FocusCtrlCMRWJ;
var vkode_customer;
CurrentPage.page = getPanelPenJasLab(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);


//membuat form
function getPanelPenJasLab(mod_id) 
{
    var Field = ['KD_PASIEN','NAMA','ALAMAT','TGL_MASUK','NAMA_UNIT','KD_KELAS','KD_DOKTER','URUT_MASUK',
				'NAMA_DOKTER','TGL_LAHIR','JENIS_KELAMIN','GOL_DARAH','NAMA_UNIT_ASAL','KD_DOKTER_ASAL','JAM_MASUK','NAMA_DOKTER_ASAL','NO_TRANSAKSI'];
    dsTRHasilLabList = new WebApp.DataStore({ fields: Field });
    refeshhasillab();
	getKdUnit_Lab();
    var grListHasilLab = new Ext.grid.EditorGridPanel
    (
        {
            stripeRows: true,
            store: dsTRHasilLabList,
            //anchor: '100% 91.9999%',
            columnLines: false,
            autoScroll:true,
            border: false,
			sort :false,
			height:390,
            sm: new Ext.grid.RowSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        rowselect: function(sm, row, rec)
                        {
                            rowSelectedHasilLab = dsTRHasilLabList.getAt(row);
                        }
                    }
                }
            ),
            listeners:
            {
                rowdblclick: function (sm, ridx, cidx)
                {
                    rowSelectedHasilLab = dsTRHasilLabList.getAt(ridx);
                    if (rowSelectedHasilLab !== undefined)
                    {
                        PenHasilLookUp(rowSelectedHasilLab.data);
                    }
                    else
                    {
                        PenHasilLookUp();
                    }
                }
            },
        cm: new Ext.grid.ColumnModel
            (
			
                [
                    new Ext.grid.RowNumberer(),
					{
                        id: 'colViewTglMasukHL',
                        header: 'Tanggal Masuk',
                        dataIndex: 'TGL_MASUK',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 75,
                            renderer: function(v, params, record)
                            {
                                    return ShowDate(record.data.TGL_MASUK);

							}
                    },
					{
                        id: 'colViewNoTransaksiHL',
                        header: 'No Transaksi',
                        dataIndex: 'NO_TRANSAKSI',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 50
                    },
                    {
                        id: 'colViewNoMedrecHL',
                        header: 'No Medrec',
                        dataIndex: 'KD_PASIEN',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 50
                    },
                    {
                        id: 'colViewNamaHL',
                        header: 'Nama Pasien',
                        dataIndex: 'NAMA',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 130
                    },
					{
                        id: 'colViewAlamatHL',
						header: 'Alamat',
                        width: 170,
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        dataIndex: 'ALAMAT'                        
                    },
                    {
                        id: 'colViewTtlHL',
                        header: 'Tanggal Lahir',
                        dataIndex: 'TGL_LAHIR',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 65,
                            renderer: function(v, params, record)
                            {
                                    return ShowDate(record.data.TGL_LAHIR);

							}
                    },
                    {
                        id: 'colViewNmDokterHL',
                        header: 'Dokter',
                        dataIndex: 'NAMA_DOKTER',
                        sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 150
                    },
                    {
                        id: 'colViewUnitHL',
                        header: 'Unit',
                        dataIndex: 'NAMA_UNIT_ASAL',
						sortable: false,
						hideable:false,
						menuDisabled:true,
                        width: 90
                    },
					
                   
                ]
            ),
            viewConfig: {forceFit: true},
            tbar:
                [
                    {
                        id: 'btnEditHasilLab',
                        text: 'Edit data',
                        tooltip: 'Edit data',
                        iconCls: 'Edit_Tr',
                        handler: function(sm, row, rec)
                        {
                            if (rowSelectedHasilLab != undefined)
                            {
								PenHasilLookUp(rowSelectedHasilLab.data);
                            }
                            else
                            {
								ShowPesanWarningHasilLab('Pilih data data tabel  ','Edit data');
                            }
                        }
                    }
                ]
            }
	);
	
	
	//form depan awal dan judul tab
    var FormDepanHasilLab = new Ext.Panel
    (
        {
            id: mod_id,
            closable: true,
            region: 'center',
            layout: 'form',
            title: 'Hasil Laboratorium',
            border: false,
            shadhow: true,
            autoScroll:false,
            iconCls: 'Request',
            margins: '0 5 5 0',
			//height:200,
			autoHeight:true,
            items: [
                    getItemPanelHasilLab(),
                    grListHasilLab
                   ],
            listeners:
            {
                'afterrender': function()
                {
				}
            }
        }
    );
	
   return FormDepanHasilLab;

};

//mengatur lookup edit data
function PenHasilLookUp(rowdata) 
{
    var lebar = 800;
    FormLookUpsdetailHasilLab = new Ext.Window
    (
        {
            id: 'gridHasilLab',
            title: 'Hasil Laboratorium',
            closeAction: 'destroy',
            width: lebar,
            height: 500,
            border: false,
            resizable: false,
            plain: true,
            layout: 'fit',
            constrain: true,
            iconCls: 'Request',
            modal: true,
            items: getFormEntryHasilLab(lebar,rowdata),
            listeners:
            {
                
            }
        }
    );

    FormLookUpsdetailHasilLab.show();
    if (rowdata === undefined) {
		
	}
	else{
		DataInitHasilLab(rowdata);
	}

};


//mengatur lookup toolbar
function getFormEntryHasilLab(lebar,data) 
{
    var pnlHasilLab = new Ext.FormPanel
    (
        {
            id: 'PanelHasilLab',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:10px 10px 10px 10px',
            height:180,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [viewlookupedit(lebar)],
			tbar:
			[
				'-',
				{
					text: 'Simpan',
					id: 'btnSimpanHasilLab',
					tooltip: nmSimpan,
					iconCls: 'save',
					handler: function()
					{
						Datasave_HasilLab(false);
					   
					}
				},  
				'-',
				{
					id:'btnCetakHasilLab',
					text: 'Cetak',
					tooltip: 'Cetak Hasil',
					iconCls:'print',
					handler: function()
					{
					   DataPrint_HasilLab();
					}
				},
				'-'
			],
        }
    );
	// toolbar grid lookup
	var x;
	var GDtabDetailHasilLab = new Ext.TabPanel   
    (
        {
			id:'GDtabDetailHasilLab',
			region: 'center',
			activeTab: 0,
			height:272,
			width:777,
			border:true,
			plain: true,
			defaults:
			{
				autoScroll: true
			},
			items: [
					GetDTGridHasilLab()
			]
        }
		
    );

	var pnlHasilLab2 = new Ext.FormPanel
    (
        {
            id: 'PanelHasilLab2',
            fileUpload: true,
            region: 'north',
            layout: 'column',
            bodyStyle: 'padding:5px 5px 5px 5px',
            height:305,
            anchor: '100%',
            width: lebar,
            border: false,
            items: [	GDtabDetailHasilLab
			
			]
        }
    );

    var FormDepanHasilLab = new Ext.Panel
	(
		{
		    id: 'FormDepanHasilLab',
		    region: 'center',
		    width: '100%',
		    layout: 'form',
		    title: '',
		    bodyStyle: 'padding:15px',
		    border: false,
		    bodyStyle: 'background:#FFFFFF;',
		    shadhow: true,
		    items: 
			[ 
				pnlHasilLab,pnlHasilLab2
			]

		}
	);
	
    return FormDepanHasilLab
};

 function getdatahasillabcetak()
{
	var tmpumur='';
	if(Ext.getCmp('TxtPopupThnLahirPasien').getValue() !== ''){
		if(Ext.getCmp('TxtPopupThnLahirPasien').getValue() ==='0' || Ext.get('TxtPopupThnLahirPasien').getValue() ===0){
			if(Ext.getCmp('TxtPopupBlnLahirPasien').getValue() ==='0' || Ext.getCmp('TxtPopupBlnLahirPasien').getValue() ===0){
				tmpumur=Ext.getCmp('TxtPopupHariLahirPasien').getValue() + ' Hari';
			} else{
				tmpumur=Ext.getCmp('TxtPopupBlnLahirPasien').getValue() + ' Bulan';
			}
		} else{
			tmpumur=Ext.getCmp('TxtPopupThnLahirPasien').getValue() + ' Tahun';
		}
	}
     var params = {
        Tgl: Ext.get('dPopupTglMasuk').getValue(),
        KdPasien: Ext.get('TxtPopupMedrec').getValue(),
        Nama: Ext.get('TxtPopupNamaPasien').getValue(),        
        JenisKelamin: Ext.get('TxtJK').getValue(),
		Ttl:Ext.get('dPopupTglLahirPasien').getValue(),
		Umur:tmpumur,
        Alamat: Ext.get('TxtPopupAlamat').getValue(),
        Poli: Ext.get('TxtPopPoli').getValue(),
        urutmasuk: Ext.get('TxtUrutMasuk').getValue(),
        Dokter:Ext.get('TxtNmDokter').getValue(),
		NamaUnitAsal:tmpnama_unit_asal,
		KdDokterAsal:tmpkd_dokter_asal,
		JamMasuk:tmpjam_masuk,
		NamaDokterAsal:tmpnama_dokter_asal
    };
    return params;
}


 function DataPrint_HasilLab() 
{
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/main/cetaklaporanRadLab/LapLab",
			params: getdatahasillabcetak(),
			failure: function(o)
				 {
					
				 },	
				 success: function(o)
				 {
				
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						window.open(cst.url, 'LAB', "height=,width=");
					}
					else 
					{
						ShowPesanWarningHasilLab('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
					};
				 } 
		}
	);

};
//LOOKUP hasil laboratorium
function viewlookupedit(lebar){
	var items =
	{
	    layout: 'fit',
	    anchor: '100%',
	    width: lebar-35,
	    labelAlign: 'right',
	    bodyStyle: 'padding:0px 0px 0px 0px',
	    border:true,
	    height:140,
	    items:
		[
			{
					columnWidth: .99,
					layout: 'absolute',
					bodyStyle: 'padding: 0px 0px 0px 0px',
					border: false,
					width: 100,
					height: 150,
					anchor: '100% 100%',
					items:
					[
						{
							x: 10,
							y: 10,
							xtype: 'label',
							text: 'No. Medrec '
						},
						{
							x: 110,
							y: 10,
							xtype: 'label',
							text: ' : '
						},
						{   
							x : 120,
							y : 10,
							xtype: 'textfield',
							name: 'TxtPopupMedrec',
							id: 'TxtPopupMedrec',
							width: 80,
							readOnly:true
						},
						{
							x: 210,
							y: 10,
							xtype: 'label',
							text: 'Tanggal '
						},
						{
							x: 250,
							y: 10,
							xtype: 'label',
							text: ' : '
						},
						{   //cobacoba
							x : 260,
							y : 10,
							xtype: 'datefield',
							name: 'dPopupTglCekPasien',
							id: 'dPopupTglCekPasien',
							width: 110,
							format: 'd/M/Y',
							value: now,
							readOnly:true
						},
						{
							x: 10,
							y: 40,
							xtype: 'label',
							text: 'Nama Pasien '
						},
						{
							x: 110,
							y: 40,
							xtype: 'label',
							text: ' : '
						},
						{   
							x : 120,
							y : 40,
							xtype: 'textfield',
							name: 'TxtPopupNamaPasien',
							id: 'TxtPopupNamaPasien',
							width: 250,
							readOnly:true
						},
						{
							x: 380,
							y: 20,
							xtype: 'label',
							text: 'Tgl Lahir '
						},
						{   
							x : 380,
							y : 40,
							xtype: 'datefield',
							name: 'dPopupTglLahirPasien',
							id: 'dPopupTglLahirPasien',
							width: 100,
							format: 'd/M/Y',
							readOnly:true
						},
						{
							x: 490,
							y: 20,
							xtype: 'label',
							text: 'Thn '
						},
						{   
							x : 490,
							y : 40,
							xtype: 'textfield',
							name: 'TxtPopupThnLahirPasien',
							id: 'TxtPopupThnLahirPasien',
							width: 30,
							readOnly:true
						},
						{
							x: 530,
							y: 20,
							xtype: 'label',
							text: 'Bln '
						},
						{   
							x : 530,
							y : 40,
							xtype: 'textfield',
							name: 'TxtPopupBlnLahirPasien',
							id: 'TxtPopupBlnLahirPasien',
							width: 30,
							readOnly:true
						},
						{
							x: 570,
							y: 20,
							xtype: 'label',
							text: 'Hari '
						},
						{   
							x : 570,
							y : 40,
							xtype: 'textfield',
							name: 'TxtPopupHariLahirPasien',
							id: 'TxtPopupHariLahirPasien',
							width: 30,
							readOnly:true
						},
						{
							x: 10,
							y: 70,
							xtype: 'label',
							text: 'Alamat '
						},
						{
							x: 110,
							y: 70,
							xtype: 'label',
							text: ' : '
						},
						{   
							x : 120,
							y : 70,
							xtype: 'textfield',
							name: 'TxtPopupAlamat',
							id: 'TxtPopupAlamat',
							width: 480,
							readOnly:true
						},
						{
							x: 10,
							y: 100,
							xtype: 'label',
							text: 'Dokter Lab'
						},
						{
							x: 110,
							y: 100,
							xtype: 'label',
							text: ' : '
						},
						{   
							x : 120,
							y : 100,
							xtype: 'textfield',
							name: 'TxtPopupNamaDokter',
							id: 'TxtPopupNamaDokter',
							width: 250,
							readOnly:true
						},
						//-------HIDDEN---------------------
						{   
							x : 480,
							y : 10,
							xtype: 'datefield',
							name: 'dPopupTglMasuk',
							id: 'dPopupTglMasuk',
							width: 150,
							readOnly:true,
							format: 'd/M/Y',
							hidden:true
						},
						{   
							x : 480,
							y : 10,
							xtype: 'datefield',
							name: 'dPopupJamMasuk',
							id: 'dPopupJamMasuk',
							width: 150,
							readOnly:true,
							format: 'd/M/Y',
							hidden:true
						},
						{   
							x : 120,
							y : 100,
							xtype: 'textfield',
							name: 'TxtUrutMasuk',
							id: 'TxtUrutMasuk',
							width: 240,
							readOnly:true,
							hidden:true
						},
						{   
							x : 120,
							y : 100,
							xtype: 'textfield',
							name: 'TxtUrutMasuk',
							id: 'TxtUrutMasuk',
							width: 240,
							readOnly:true,
							hidden:true
						},
						{   
							x : 120,
							y : 100,
							xtype: 'textfield',
							name: 'TxtNmDokter',
							id: 'TxtNmDokter',
							width: 240,
							readOnly:true,
							hidden:true
						},
						{   
							x : 120,
							y : 100,
							xtype: 'textfield',
							name: 'TxtJK',
							id: 'TxtJK',
							width: 240,
							readOnly:true,
							hidden:false
						},
						{   
							x : 120,
							y : 100,
							xtype: 'textfield',
							name: 'TxtPopPoli',
							id: 'TxtPopPoli',
							width: 240,
							readOnly:true,
							hidden:true
						},
						{   
							x : 120,
							y : 100,
							xtype: 'textfield',
							name: 'TxtPopKddokter',
							id: 'TxtPopKddokter',
							width: 250,
							readOnly:true,
							hidden:true
						}
					]
				},
	
		]
	};
	return items;
}

//------------GRID DALAM LOOK UP HASIL LAB--------------------------------------------------------------------------
function GetDTGridHasilLab() 
{
	var fm = Ext.form;
    var fldDetailHasilLab = ['KLASIFIKASI', 'DESKRIPSI', 'KD_LAB', 'KD_TEST', 'ITEM_TEST', 'SATUAN', 'NORMAL', 'NORMAL_W',  'NORMAL_A', 'NORMAL_B', 'COUNTABLE', 'MAX_M', 'MIN_M', 'MAX_F', 'MIN_F', 'MAX_A', 'MIN_A', 'MAX_B', 'MIN_B', 'KD_METODE', 'HASIL', 'KET','KD_UNIT_ASAL','NAMA_UNIT_ASAL','URUT','METODE','JUDUL_ITEM','KET_HASIL'];
	
    dsTRDetailHasilLabList = new WebApp.DataStore({ fields: fldDetailHasilLab })
    var gridDTHasilLab = new Ext.grid.EditorGridPanel
    (
        {
            title: 'Detail Hasil Lab',
            stripeRows: true,
            store: dsTRDetailHasilLabList,
            border: false,
            columnLines: true,
            frame: false,
            autoScroll:true,
            sm: new Ext.grid.CellSelectionModel
            (
                {
                    singleSelect: true,
                    listeners:
                    {
                        cellselect: function(sm, row, rec)
                        {
                            cellSelecteddeskripsi = dsTRDetailHasilLabList.getAt(row);
                            CurrentHasilLab.row = row;
                            CurrentHasilLab.data = cellSelecteddeskripsi;
                            //FocusCtrlCMRWJ='txtAset';
                        }
                    }
                }
            ),
           cm: new Ext.grid.ColumnModel
            (
			[
				{
					header: 'Kode Tes',
					dataIndex: 'KD_TEST',
					width:80,
					menuDisabled:true,
					hidden:true
				},
				{
					header: 'Item Pemeriksaan',
					dataIndex: 'JUDUL_ITEM',
					width:150,
					menuDisabled:true,
					style : {
						color : 'red'
					}
				},
				{
					header:'Pemeriksaan',
					dataIndex: 'ITEM_TEST',
					sortable: false,
					hidden:false,
					menuDisabled:true,
					width:200
					
				},
				{
					header:'Metode',
					dataIndex: 'METODE',
					sortable: false,
					align: 'center',
					hidden:false,
					menuDisabled:true,
					width:100
					
				},
				{
					header:'Hasil',
					dataIndex: 'HASIL',
					sortable: false,
					hidden:false,
					menuDisabled:true,
					width:100,
					align: 'right',
					editor: new fm.TextField({
					allowBlank: false
					})
					
				},
				{
					header:'Normal',
					dataIndex: 'NORMAL',
					sortable: false,
					hidden:false,
					align: 'center',
					menuDisabled:true,
					width:100
					
				},
				{
					header:'Ket Hasil',
					dataIndex: 'KET_HASIL',
					sortable: false,
					hidden:false,
					align: 'center',
					menuDisabled:true,
					width:100
					
				},
				{
					header:'Satuan',
					dataIndex: 'SATUAN',
					sortable: false,
					hidden:false,
					menuDisabled:true,
					width:100
					
				},
				{
					header:'Keterangan',
					dataIndex: 'KET',
					width:250,
					editor: new fm.TextField({
					allowBlank: false
					})
					
				},
				{
					header:'Kode Lab',
					dataIndex: 'KD_LAB',
					width:250,
					hidden:true
					
				}

			]
			),
			viewConfig:{forceFit: true}
        }
		
		
    );
	
	

    return gridDTHasilLab;
};

//------------------MEMASUKAN DATA YG DIPILIH KEDALAM TEXTBOX YG ADA DALAM LOOKUP--------------------------------------------------
function DataInitHasilLab(rowdata)
{
    AddNewPenJasRad = false;
	
	tmpnama_unit_asal=rowdata.NAMA_UNIT_ASAL;
	tmpkd_dokter_asal=rowdata.KD_DOKTER_ASAL;
	tmpjam_masuk=rowdata.JAM_MASUK;
	tmpnama_dokter_asal=rowdata.NAMA_DOKTER_ASAL;
	//alert(tmpjam_masuk);
	Ext.get('TxtPopupMedrec').dom.value= rowdata.KD_PASIEN;
	Ext.get('TxtPopupNamaPasien').dom.value = rowdata.NAMA;
	Ext.get('TxtPopupAlamat').dom.value = rowdata.ALAMAT;
	Ext.get('TxtPopupNamaDokter').dom.value = rowdata.NAMA_DOKTER;
	Ext.get('dPopupTglLahirPasien').dom.value = ShowDate(rowdata.TGL_LAHIR);
	Ext.get('dPopupTglMasuk').dom.value = ShowDate(rowdata.TGL_MASUK);
	if(rowdata.JENIS_KELAMIN=='t'){
		Ext.get('TxtJK').dom.value = 'Laki-laki';
	}else{
		Ext.get('TxtJK').dom.value = 'Perempuan'
	}
	Ext.get('TxtPopPoli').dom.value = rowdata.NAMA_UNIT;
	Ext.get('TxtNmDokter').dom.value = rowdata.NAMA_DOKTER;
	Ext.get('TxtUrutMasuk').dom.value = rowdata.URUT_MASUK;
	Ext.get('TxtPopKddokter').dom.value = rowdata.KD_DOKTER;
	setUsia(ShowDate(rowdata.TGL_LAHIR));	
	
	ViewGridDetailHasilLab(rowdata.KD_PASIEN,rowdata.TGL_MASUK,rowdata.URUT_MASUK);
	
	Ext.Ajax.request(
	{
	    url: baseURL + "index.php/main/getcurrentshift",
		 params: {
	        //UserID: 'Admin',
	        command: '0',
			// parameter untuk url yang dituju (fungsi didalam controller)
	    },
		failure: function(o)
		{
			 var cst = Ext.decode(o.responseText);
			
		},	    
	    success: function(o) {
			
			tampungshiftsekarang=o.responseText
	    }
	
	});
	
	
};

//-----------------------------------------MENGHITUNG USIA DALAM LOOKUP------------------------------------------------------------
function setUsia(Tanggal)
{

	Ext.Ajax.request
		( 
		{
		   url: baseURL + "index.php/main/GetUmur",
		   params: {
					TanggalLahir: ShowDateReal(Tanggal)
		   },
		   
		   success: function(o)
		   {
				//alert('test');  
			
				var tmphasil = o.responseText;
				 //alert(tmphasil);
				var tmp = tmphasil.split(' ');
				//alert(tmp.length);
				if (tmp.length == 6)
				{
					Ext.getCmp('TxtPopupThnLahirPasien').setValue(tmp[0]);
					Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[2]);
					Ext.getCmp('TxtPopupHariLahirPasien').setValue(tmp[4]);
				}
				else if(tmp.length == 4)
				{
					if(tmp[1]== 'years' && tmp[3] == 'day')
					{
						Ext.getCmp('TxtPopupThnLahirPasien').setValue(tmp[0]);
						Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
						Ext.getCmp('TxtPopupHariLahirPasien').setValue(tmp[2]);  
					}else{
					Ext.getCmp('TxtPopupThnLahirPasien').setValue('0');
					Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[0]);
					Ext.getCmp('TxtPopupHariLahirPasien').setValue(tmp[2]);
						  }
				}
				else if(tmp.length == 2 )
				{
					
					if (tmp[1] == 'year' )
					{
						Ext.getCmp('TxtPopupThnLahirPasien').setValue(tmp[0]);
						Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
						Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
					}
					else if (tmp[1] == 'years' )
					{
						Ext.getCmp('TxtPopupThnLahirPasien').setValue(tmp[0]);
						Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
						Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
					}
					else if (tmp[1] == 'mon'  )
					{
						Ext.getCmp('TxtPopupThnLahirPasien').setValue('0');
						Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[0]);
						Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
					}
					else if (tmp[1] == 'mons'  )
					{
						Ext.getCmp('TxtPopupThnLahirPasien').setValue('0');
						Ext.getCmp('TxtPopupBlnLahirPasien').setValue(tmp[0]);
						Ext.getCmp('TxtPopupHariLahirPasien').setValue('0');
					}
					else{
							Ext.getCmp('TxtPopupThnLahirPasien').setValue('0');
							Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
							Ext.getCmp('TxtPopupHariLahirPasien').setValue(tmp[0]);
						}
				}
				
				else if(tmp.length == 1)
				{
					Ext.getCmp('TxtPopupThnLahirPasien').setValue('0');
					Ext.getCmp('TxtPopupBlnLahirPasien').setValue('0');
					Ext.getCmp('TxtPopupHariLahirPasien').setValue('1');
				}else
				{
					alert("Tanggal yang anda masukan lebih besar dari tanggal sekarang");
				}
		   }
		   
		   
		}
		);


	
}



//------------------MENAMPILKAN DETAIL HASIL LAB---------------------------------------------------
function ViewGridDetailHasilLab(kd_pasien,tgl_masuk,urut_masuk) 
{
    var strKriteriaHasilLab='';
    strKriteriaHasilLab = "LAB_hasil.Kd_Pasien = '" + kd_pasien + "' And LAB_hasil.Tgl_Masuk = '" + tgl_masuk + "'  and LAB_hasil.Urut_Masuk ="+ urut_masuk +"  and LAB_hasil.kd_unit= '"+ kdUnitLab_HasilLab +"' order by lab_test.kd_lab,lab_test.kd_test asc";
   
    dsTRDetailHasilLabList.load
	(
		{
		    params:
			{
			    Skip: 0,
			    Take: 1000,
			    Sort: 'tgl_transaksi',
			    //Sort: 'tgl_transaksi',
			    Sortdir: 'ASC',
			    target: 'ViewGridHasilLab',
			    param: strKriteriaHasilLab
			}
		}
	);
    return dsTRDetailHasilLabList;
};




//---------------------MENGAMBIL PARAMETER UNTUK SAVE------------------------------------------------------------------///
function getParamHasilLab() 
{
    var params =
	{
		//Table:'ViewDetaiTransaksiHasilLab',
		
		KdPasien: Ext.get('TxtPopupMedrec').getValue(),
		TglMasuk: Ext.get('dPopupTglMasuk').dom.value,
		KdDokter:Ext.get('TxtPopKddokter').getValue(),
		UrutMasuk:Ext.get('TxtUrutMasuk').getValue(),
		TglHasil:Ext.get('dPopupTglCekPasien').getValue(),
		Shift: tampungshiftsekarang,
		List:getArrHasilLab(),
		JmlField: mRecordRwj.prototype.fields.length-4,
		JmlList:GetListCountDetailTransaksi(),
		Hapus:0,
		Ubah:1,
		Unit:'41'
	};
    return params
};


function GetListCountDetailTransaksi()
{
	
	var x=0;
	for(var i = 0 ; i < dsTRDetailHasilLabList.getCount();i++)
	{
		if (dsTRDetailHasilLabList.data.items[i].data.KD_PRODUK != '' || dsTRDetailHasilLabList.data.items[i].data.DESKRIPSI  != '')
		{
			x += 1;
		};
	}
	return x;
	
};



//lagi
//Mengambil data dari data grid hasil laboratorium
function getArrHasilLab()
{
	var x='';
	for(var i = 0 ; i < dsTRDetailHasilLabList.getCount();i++)
	{
		if (dsTRDetailHasilLabList.data.items[i].data.KD_TEST != '' && dsTRDetailHasilLabList.data.items[i].data.metode != '')
		{
			var y='';
			var z='@@##$$@@';
			var hasil='';
			var keterangan='';
			
			if(dsTRDetailHasilLabList.data.items[i].data.HASIL == '' || dsTRDetailHasilLabList.data.items[i].data.HASIL== undefined){
				hasil='';
			} else{
				hasil=dsTRDetailHasilLabList.data.items[i].data.HASIL;
			}
			
			if(dsTRDetailHasilLabList.data.items[i].data.KET == '' || dsTRDetailHasilLabList.data.items[i].data.KET == undefined){
				keterangan='';
			} else{
				keterangan=dsTRDetailHasilLabList.data.items[i].data.KET;
			}
			
			y = 'URUT=' +dsTRDetailHasilLabList.data.items[i].data.URUT
			y += z +dsTRDetailHasilLabList.data.items[i].data.KD_TEST
			y += z +dsTRDetailHasilLabList.data.items[i].data.ITEM_TEST
			y += z +hasil
			y += z +keterangan
			y += z +dsTRDetailHasilLabList.data.items[i].data.KD_LAB
			y += z +dsTRDetailHasilLabList.data.items[i].data.NORMAL
			//y += z + ShowDate(store.data.items[i].data.TGL_BERLAKU)
			//y += z +store.data.items[i].data.KD_TARIF
			//y += z +store.data.items[i].data.URUT
			
			
			if (i === (dsTRDetailHasilLabList.getCount()-1))
			{
				x += y 
			}
			else
			{
				x += y + '##[[]]##'
			};
		};
	}	
	
	return x;
};


function refeshhasillab(kriteria)
{
dsTRHasilLabList.load
		(
			{ 
				params:  
				{   
					Skip: 0, 
					Take: selectCountPenJasRad,
                    Sort: '',
					Sortdir: 'ASC', 
					target:'ViewHasilLab',
					param : kriteria
				}			
			}
		);   
		return dsTRHasilLabList;
}

function RefreshDataFilterHasilLab() 
{

	var KataKunci='';
	
	 if (Ext.get('txtNoMedrec').getValue() != ''){
		if (KataKunci == ''){
			KataKunci = ' LOWER(pasien.kd_pasien) like  LOWER( ~' + Ext.get('txtNoMedrec').getValue() + '%~)';	
		}
		else{
			KataKunci += ' and  LOWER(pasien.kd_pasien) like  LOWER( ~' + Ext.get('txtNoMedrec').getValue() + '%~)';
		};

	};
	
	 if (Ext.get('TxtNamaHasilLab').getValue() != '')
    {
		if (KataKunci == ''){
			KataKunci = ' LOWER(pasien.nama) like  LOWER( ~%' + Ext.get('TxtNamaHasilLab').getValue() + '%~)';
		}else{
			KataKunci += ' and  LOWER(pasien.nama) like  LOWER( ~%' + Ext.get('TxtNamaHasilLab').getValue() + '%~)';
		};
	};
	if (Ext.get('dtpTglAwalFilterHasilLab').getValue() != ''){
		if (KataKunci == ''){                      
			KataKunci = " (transaksi.tgl_transaksi >='" + Ext.get('dtpTglAwalFilterHasilLab').getValue() + "' and transaksi.tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterHasilLab').getValue() + "')";
		}else{
			KataKunci += " and (transaksi.tgl_transaksi >='" + Ext.get('dtpTglAwalFilterHasilLab').getValue() + "' and transaksi.tgl_transaksi <='" + Ext.get('dtpTglAkhirFilterHasilLab').getValue() + "')";
		};
	};	
    
	return KataKunci; 
};


function Datasave_HasilLab(mBol) 
{	
	if (ValidasiEntryHasilLab(nmHeaderSimpanData,false) == 1 )
	{
			Ext.Ajax.request
			 (
				{
					//url: "./Datapool.mvc/CreateDataObj",
					url: baseURL + "index.php/main/functionLAB/ubahlabhasil",
					params: getParamHasilLab(),
					failure: function(o)
					{
						ShowPesanWarningHasilLab('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
						ViewGridDetailHasilLab(Ext.getCmp('TxtPopupMedrec').getValue(),Ext.get('dPopupTglCekPasien').getValue(),Ext.getCmp('TxtUrutMasuk').getValue());
					},	
					success: function(o) 
					{
						ViewGridDetailHasilLab(Ext.getCmp('TxtPopupMedrec').getValue(),Ext.get('dPopupTglMasuk').getValue(),Ext.getCmp('TxtUrutMasuk').getValue());
						var cst = Ext.decode(o.responseText);
						if (cst.success === true) 
						{
							ShowPesanInfoHasilLab(nmPesanSimpanSukses,nmHeaderSimpanData);
							RefreshDataFilterHasilLab();
							if(mBol === false)
							{
								ViewGridDetailHasilLab(Ext.getCmp('TxtPopupMedrec').getValue(),Ext.get('dPopupTglMasuk').getValue(),Ext.getCmp('TxtUrutMasuk').getValue());
							};
						}
						else 
						{
								ShowPesanWarningHasilLab('Produk/Komponen Belum terdaftar segera Hubungi Admin', 'Gagal');
						};
					}
				}
			)
		
	}
	else
	{
		if(mBol === true)
		{
			return false;
		};
	};
	
};


function ValidasiEntryHasilLab(modul,mBolHapus)
{
	var x = 1;dsTRDetailHasilLabList
	
	if((Ext.get('TxtPopupMedrec').getValue() == '') || (Ext.get('dPopupTglCekPasien').getValue() == '') || (dsTRDetailHasilLabList.getCount() === 0 ))
	{
		if (Ext.get('TxtPopupMedrec').getValue() == '') 
		{
			ShowPesanWarningHasilLab(nmGetValidasiKosong('No. Medrec'), modul);
			x = 0;
		}
		else if (Ext.get('dPopupTglCekPasien').getValue() == '') 
		{
			ShowPesanWarningHasilLab(nmGetValidasiKosong('Tanggal masuk'), modul);
			x = 0;
		}
		else if (dsTRDetailHasilLabList.getCount() === 0) 
		{
			ShowPesanWarningHasilLab(nmGetValidasiKosong(nmTitleDetailFormRequest),modul);
			x = 0;
		};
	};
	return x;
};


//AWAL INPUT
function getItemPanelHasilLab()
{
    var items =
    {
        layout:'column',
		bodyStyle: 'padding: 5px 0px 5px 10px',
        border:true,
        items:
        [
			//--------------------------------------------------
            {
                columnWidth:.99,
                layout: 'absolute',
                bodyStyle: 'padding: 10px 10px 10px 10px',
                border: true,
                width: 600,
                height: 105,
                anchor: '100% 100%',
                items:
                [
					{
                        x: 10,
                        y: 10,
                        xtype: 'label',
                        text: 'No. Medrec  '
                    },
					{
                        x: 145,
                        y: 10,
                        xtype: 'label',
                        text: ':'
                    },
					{
						x : 155,
                        y : 10,
                        xtype: 'textfield',
                        fieldLabel: 'No. Medrec (Enter Untuk Mencari)',
                        name: 'txtNoMedrec',
                        id: 'txtNoMedrec',
                        enableKeyEvents: true,
                        listeners:
                        {
                            'specialkey' : function()
                            {
                                var tmpNoMedrec = Ext.get('txtNoMedrec').getValue()
                                if (Ext.EventObject.getKey() === 13 || Ext.EventObject.getKey() === 9 || tmpNoMedrec.length === 10 )
                                {
                                    if(tmpNoMedrec.length !== 0 && tmpNoMedrec.length < 10 )
                                        {
                                             var tmpgetNoMedrec = formatnomedrec(Ext.get('txtNoMedrec').getValue())
                                             Ext.getCmp('txtNoMedrec').setValue(tmpgetNoMedrec);
                                             var tmpkriteria = RefreshDataFilterHasilLab();
                                             refeshhasillab(tmpkriteria);
                                        }
                                        else
                                            {
                                                if (tmpNoMedrec.length === 10)
                                                    {
                                                        tmpkriteria = RefreshDataFilterHasilLab();
                                                        refeshhasillab(tmpkriteria);
                                                    }
                                                    else
                                                    Ext.getCmp('txtNoMedrec').setValue('')
                                            }
                                } else{
									tmpkriteria = RefreshDataFilterHasilLab();
										refeshhasillab(tmpkriteria);
								}
								
                            }

                        }

                    },	
                    {
                        x: 10,
                        y: 40,
                        xtype: 'label',
                        text: 'Nama Pasien '
                    },
					{
                        x: 145,
                        y: 40,
                        xtype: 'label',
                        text: ':'
                    },
                    {   
                        x : 155,
                        y : 40,
                        xtype: 'textfield',
                        name: 'TxtNamaHasilLab',
                        id: 'TxtNamaHasilLab',
                        width: 200,
                        enableKeyEvents: true,
                        listeners:
                        { 
                                'specialkey' : function()
                                {
                                        if (Ext.EventObject.getKey() === 13) 
                                        {

                                                //RefreshDataFilterHasilLab();
												tmpkriteria = RefreshDataFilterHasilLab();
												refeshhasillab(tmpkriteria);

                                        } 						
                                }
                        }
                    },
					{
                        x: 10,
                        y: 70,
                        xtype: 'label',
                        text: 'Tanggal Kunjungan '
                    },
					{
                        x: 145,
                        y: 70,
                        xtype: 'label',
                        text: ':'
                    },
                    {
                        x: 155,
                        y: 70,
                        xtype: 'datefield',
                        id: 'dtpTglAwalFilterHasilLab',
                        format: 'd/M/Y',
                        value: tigaharilalu,
                        listeners:
                        { 
                                'specialkey' : function()
                                {
                                        if (Ext.EventObject.getKey() === 13) 
                                        {

                                                //RefreshDataFilterHasilLab();
												tmpkriteria = RefreshDataFilterHasilLab();
												refeshhasillab(tmpkriteria);

                                        } 						
                                }
                        }
                    },
					{
                        x: 270,
                        y: 70,
                        xtype: 'label',
                        text: 's/d '
                    },
					{
                        x: 305,
                        y: 70,
                        xtype: 'datefield',
                        id: 'dtpTglAkhirFilterHasilLab',
                        format: 'd/M/Y',
                        value: now,
                        width: 100,
                        listeners:
                        { 
                                'specialkey' : function()
                                {
                                        if (Ext.EventObject.getKey() === 13) 
                                        {

                                                //RefreshDataFilterHasilLab();
												tmpkriteria = RefreshDataFilterHasilLab();
												refeshhasillab(tmpkriteria);

                                        } 						
                                }
                        }
                    },
					{
                        x: 595,
                        y: 70,
                        xtype:'button',
                        text: 'Refresh',
                        iconCls: 'refresh',
                        anchor: '25%',
                        width: 70,
                        height: 20,
                        hideLabel: false,
                        handler: function(sm, row, rec)
                        {
							tmpkriteria = RefreshDataFilterHasilLab();
                            refeshhasillab();
                        }
                    }
                    
                ]
            },
        ]
    }
    return items;
};



function ShowPesanWarningHasilLab(str, modul) 
{
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:250
		}
	);
};

function ShowPesanErrorHasilLab(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoHasilLab(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};

function getKdUnit_Lab(){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/main/functionLAB/getKdUnit",
			params: {query:''},
			failure: function(o)
			{
				loadMask.hide();
				ShowPesanErrorHasilLab('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					kdUnitLab_HasilLab = cst.kd_unit;
				}
				else 
				{
					loadMask.hide();
					ShowPesanErrorHasilLab('Gagal membaca unitfar', 'Error');
				};
			}
		}
		
	)
	
}

