var dataSource_viSetupItemTest;
var selectCount_viSetupItemTest=50;
var NamaForm_viSetupItemTest="Setup Item Test";
var mod_name_viSetupItemTest="Setup Item Test";
var now_viSetupItemTest= new Date();
var rowSelected_viSetupItemTest;
var setLookUps_viSetupItemTest;
var tanggal = now_viSetupItemTest.format("d/M/Y");
var jam = now_viSetupItemTest.format("H/i/s");
var tmpkriteria;
var DataGridJenisObat;
var GridDataView_viSetupItemTest;
var CurrentSelectItem;
var CurrentKdLab;
var CurrentKdTest;
var CurrentKdLabSave;
var CurrentKdTestbSave;
var CurrentSelectAutoComGrid;
var CurrentArrayStoreAutoComGridItemPemeriksaan;
var CurrentArrayStoreAutoComGridMetode;


var CurrentData_viSetupItemTest =
{
	data: Object,
	details: Array,
	row: 0
};

/* var SetupItemTest={};
SetupItemTest.form={};
SetupItemTest.func={};
SetupItemTest.vars={};
SetupItemTest.func.parent=SetupItemTest;
SetupItemTest.form.ArrayStore={};
SetupItemTest.form.ComboBox={};
SetupItemTest.form.DataStore={};
SetupItemTest.form.Record={};
SetupItemTest.form.Form={};
SetupItemTest.form.Grid={};
SetupItemTest.form.Panel={};
SetupItemTest.form.TextField={};
SetupItemTest.form.Button={};
SetupItemTest.GridDataView_viSetupItemTest; */

CurrentArrayStoreAutoComGridItemPemeriksaan= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_lab', 'kd_test','item_test','satuan','metode','urut','normal','kd_metode'],
	data: []
});

CurrentArrayStoreAutoComGridMetode= new Ext.data.ArrayStore({
	id: 0,
	fields:['kd_lab', 'kd_test','item_test','satuan','metode','urut','normal','kd_metode'],
	data: []
});

CurrentPage.page = dataGrid_viSetupItemTest(CurrentPage.id);
mainPage.add(CurrentPage.page);
mainPage.setActiveTab(CurrentPage.id);
loadDataComboItemPemeriksaan_SetupItemTest();

function dataGrid_viSetupItemTest(mod_id_viSetupItemTest){	
 	// Field kiriman dari Project Net.
    var FieldMaster_viSetupItemTest = 
	[
		'kd_lab', 'kd_test','item_test','satuan','metode','urut','normal'
	];
	// Deklarasi penamaan yang akan digunakan pada Grid # --------------      
    dataSource_viSetupItemTest = new WebApp.DataStore
	({
        fields: FieldMaster_viSetupItemTest
    });
	
    // Grid lab Perencanaan # --------------
	GridDataView_viSetupItemTest = new Ext.grid.EditorGridPanel
    (
		{
			title: 'Item Test',
			store: dataSource_viSetupItemTest,
			autoScroll: false,
			columnLines: true,
			stripeRows: true,
			flex:1,
			frame: true,
			border:true,
			anchor: '100% 100%',
			plugins: [new Ext.ux.grid.FilterRow()],
			selModel:  new Ext.grid.CellSelectionModel
			(
				{
					// Tanda aktif saat salah satu baris dipilih # --------------
					singleSelect: true,
					listeners:
					{
						cellselect: function(sm, row, rec)
						{
							rowSelected_viSetupItemTest = undefined;
							rowSelected_viSetupItemTest = dataSource_viSetupItemTest.getAt(row);
							CurrentData_viSetupItemTest
							CurrentData_viSetupItemTest.row = row;
							CurrentData_viSetupItemTest.data = rowSelected_viSetupItemTest.data;
							CurrentSelectItem=CurrentData_viSetupItemTest.data;
							
							InitialCureentDataNilaiTest(CurrentSelectItem);
						}
					}
				}
			),
			// Proses eksekusi baris yang dipilih # --------------
			listeners:
			{
				// Function saat ada event double klik maka akan muncul form view # --------------
				rowdblclick: function (sm, ridx, cidx)
				{
					rowSelected_viSetupItemTest = dataSource_viSetupItemTest.getAt(ridx);
					if (rowSelected_viSetupItemTest != undefined)
					{
						//DataInitSetupItemTest(rowSelected_viSetupItemTest.data);
					}
					else
					{
						
					}
				}
				// End Function # --------------
			},
			/**
	        *	Mengatur tampilan pada Grid Setup Item Test
	        *	Terdiri dari : Judul, Isi dan Event
	        */
			colModel: new Ext.grid.ColumnModel
			(
				[
					new Ext.grid.RowNumberer(),
					//-------------- ## --------------
					{
						header: 'Kode lab',
						dataIndex: 'kd_lab',
						hideable:false,
						menuDisabled: true,
						hidden: true,
						width: 40
						
					},
					//-------------- ## --------------
					{
						header: 'Kode test',
						dataIndex: 'kd_test',
						hideable:false,
						menuDisabled: true,
						hidden: true,
						width: 40
						
					},
					//-------------- ## --------------
					{
						header: 'Kode metode',
						dataIndex: 'kd_metode',
						hideable:false,
						menuDisabled: true,
						hidden: true,
						width: 40
						
					},
					//-------------- ## --------------
					{
						header: 'Item Test',
						dataIndex: 'item_test',
						hideable:false,
						menuDisabled: true,
						width: 100,
						editor		:new Nci.form.Combobox.autoCompleteId({
							//id		: '',
							store	: CurrentArrayStoreAutoComGridItemPemeriksaan,
							select	: function(a,b,c){
								var line	= GridDataView_viSetupItemTest.getSelectionModel().selection.cell[0];
								/* dataSource_viSetupItemTest.getRange()[line].data.kd_lab=b.data.kd_lab;
								dataSource_viSetupItemTest.getRange()[line].data.kd_test=b.data.kd_test; */
								dataSource_viSetupItemTest.getRange()[line].data.item_test=b.data.item_test;
								GridDataView_viSetupItemTest.getView().refresh();
								
							},
							insert	: function(o){
								return {
									kd_lab      : o.kd_lab,
									kd_test 	: o.kd_test,
									kd_metode	: o.kd_metode,
									normal		: o.normal,
									satuan		: o.satuan,
									urut		: o.urut,
									item_test	: o.item_test,
									text		:  '<table style="font-size: 11px;"><tr><td width="50">'+o.kd_lab+'</td><td width="200">'+o.item_test+'</td></tr></table>'
								}
							},
							/* param:function(){
									return {
										kdcustomer:Ext.getCmp('txtTmpKdCustomer').getValue()
									}
							}, */
							url		: baseURL + "index.php/lab/functionSetupItemTest/getItemPemeriksaan",
							valueField: 'item_test',
							displayField: 'text',
							listWidth: 250
						})
					},
					//-------------- ## --------------
					{
						header: 'Nilai normal',
						dataIndex: 'normal',
						hideable:false,
						hidden: true,
						menuDisabled: true,
						width: 70
					},
					//-------------- ## --------------
					{
						header: 'Satuan',
						dataIndex: 'satuan',
						hideable:false,
						menuDisabled: true,
						width: 70,
						editor: new Ext.form.TextField({
							allowBlank: false,
						})
					},
					//-------------- ## --------------
					{
						header: 'Metode',
						dataIndex: 'metode',
						hideable:false,
						menuDisabled: true,
						width: 70,
						editor: new Nci.form.Combobox.autoCompleteId({
							//id		: '',
							store	: CurrentArrayStoreAutoComGridMetode,
							select	: function(a,b,c){
								var line	= GridDataView_viSetupItemTest.getSelectionModel().selection.cell[0];
								dataSource_viSetupItemTest.getRange()[line].data.kd_metode=b.data.kd_metode;
								dataSource_viSetupItemTest.getRange()[line].data.metode=b.data.metode;
								GridDataView_viSetupItemTest.getView().refresh();
								
							},
							insert	: function(o){
								return {
									kd_metode   : o.kd_metode,
									metode 		: o.metode,
									text		:  '<table style="font-size: 11px;"><tr><td width="200">'+o.metode+'</td></tr></table>'
								}
							},
							/* param:function(){
									return {
										kdcustomer:Ext.getCmp('txtTmpKdCustomer').getValue()
									}
							}, */
							url		: baseURL + "index.php/lab/functionSetupItemTest/getMetode",
							valueField: 'metode',
							displayField: 'text',
							listWidth: 250
						})
					},
					//-------------- ## --------------
					{
						header: 'Urut',
						dataIndex: 'urut',
						hideable:false,
						menuDisabled: true,
						align:'right',
						width: 30,
						editor: new Ext.form.NumberField({
							allowBlank: false,
						})
					}
					//-------------- ## --------------
				]
			),
			tbar: 
			{
				xtype: 'toolbar',
				id: 'toolbar_viSetupItemTest',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add Item',
						iconCls: 'Edit_Tr',
						tooltip: 'Edit Data',
						disabled:true,
						id: 'btnAddItem_viSetupItemTest',
						handler: function(sm, row, rec)
						{
							var records = new Array();
							records.push(new dataSource_viSetupItemTest.recordType());
							dataSource_viSetupItemTest.add(records);
							
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Save',
						iconCls: 'save',
						disabled:true,
						id: 'btnSimpan_viSetupItemTest',
						handler: function()
						{
							loadMask.show();
							dataSaveItem_viSetupItemTest();
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Delete',
						iconCls: 'remove',
						disabled:true,
						id: 'btnDelete_viSetupItemTest',
						handler: function()
						{
							Ext.Msg.confirm('Warning', 'Apakah data ini akan dihapus?', function(button){
								if (button == 'yes'){
									loadMask.show();
									dataDelete_viSetupItemTest();
								}
							});
						}
					},
					{
						xtype: 'tbseparator'
					},
					{
						xtype: 'button',
						text: 'Refresh',
						iconCls: 'refresh',
						disabled:true,
						id: 'btnRefresh_viSetupPabrik',
						handler: function()
						{
							dataSource_viSetupItemTest.removeAll();
							dataGriSetupItemTest(CurrentKdLab);
						}
					},
					{
						xtype: 'tbseparator'
					}
					//-------------- ## --------------
				]
			},
			viewConfig: 
			{
				forceFit: true
			}
		}
	)	
	
	var PanelTabSetupItemTest = new Ext.FormPanel({
        labelAlign: 'top',
		layout:'form',
		border: false,  
		autoHeight:true,
        title: '',
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [PanelSearchPemeriksaan()]
		
	})
	
	var PanelTabInputItemTest = new Ext.FormPanel({
        labelAlign: 'top',
		layout:'form',
		border: false,  
		autoHeight:true,
        title: '',
        bodyStyle:'padding:5px 5px 5px 5px',
        items: [PanelInputNilaiNormal()]
		
	})
	
	
	// Kriteria filter pada Grid # --------------
    var FrmData_viSetupItemTest = new Ext.Panel
    (
		{
			title: NamaForm_viSetupItemTest,
			iconCls: 'Studi_Lanjut',
			id: mod_id_viSetupItemTest,
			//region: 'center',
			layout: {
				type:'vbox',
				align:'stretch'
				
			}, 
			closable: true, 
			autoScroll:false,
			border: false,  
			margins: '0 5 5 0',
			items: [ 
				PanelTabSetupItemTest,
				GridDataView_viSetupItemTest,
				PanelTabInputItemTest
			],
			/* tbar: {
				xtype: 'toolbar',
				items: 
				[
					{
						xtype: 'button',
						text: 'Add New',
						iconCls: 'add',
						id: 'btnAdd_viSetupItemTest',
						handler: function(){
							AddNewSetupItemTest();
						}
					},
					
					
				]
			} */
			//-------------- # End tbar # --------------
       }
    )
    return FrmData_viSetupItemTest;
    //-------------- # End form filter # --------------
}

function PanelSearchPemeriksaan(){
	var items = 
	{
		layout:'form',
		border: true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:.98,
						layout: 'absolute',
						bodyStyle: 'padding: 0px 0px 0px 0px',
						border: false,
						width: 500,
						height: 25,
						anchor: '100% 100%',
						items:
						[
							{
								x: 0,
								y: 0,
								xtype: 'label',
								text: 'Nama Pemeriksaan :',
								style:{
									fontSize:'11'
								}
							},
							cbItemPemeriksaan_SetupItemTest()
						]
					}
				]
			}
		]		
	};
        return items;
}

function PanelInputNilaiNormal(){
	var items = 
	{
		id:'PanelInputNilai_SetupItemTest',
		layout:'form',
		border: true,
		disabled:true,
		bodyStyle:'padding: 5px',
		items:
		[
			{
				layout: 'column',
				border: false,
				items:
				[
					{
						columnWidth:100,
						//flex:1,
						layout: 'absolute',
						bodyStyle: 'padding: 0px 0px 0px 0px',
						border: false,
						width: 520,
						height: 90,
						anchor: '100% 100%',
						items:
						[
							{
								x: 0,
								y: 0,
								xtype: 'checkbox',
								boxLabel: 'Numerik',
								id: 'cbNumerik_SetupItemTest',
								name: 'cbNumerik_SetupItemTest',
								width: 80,
								checked: false,
								handler:function(a,b) 
								{
									if(a.checked==true){
										Ext.getCmp('lblMin1').show();
										Ext.getCmp('lblMax1').show();
										Ext.getCmp('lblMin2').show();
										Ext.getCmp('lblMax2').show();
										Ext.getCmp('lblMin3').show();
										Ext.getCmp('lblMax3').show();
										Ext.getCmp('lblMin4').show();
										Ext.getCmp('lblMax4').show();
										Ext.getCmp('txtMinPria_SetupItemTest').show();
										Ext.getCmp('txtMaxPria_SetupItemTest').show();
										Ext.getCmp('txtMinWanita_SetupItemTest').show();
										Ext.getCmp('txtMaxWanita_SetupItemTest').show();
										Ext.getCmp('txtMinA10th17th_SetupItemTest').show();
										Ext.getCmp('txtMaxA10th17th_SetupItemTest').show();
										Ext.getCmp('txtMinA5th10th_SetupItemTest').show();
										Ext.getCmp('txtMaxA5th10th_SetupItemTest').show();
										Ext.getCmp('txtMinA1th5th_SetupItemTest').show();
										Ext.getCmp('txtMaxA1th5th_SetupItemTest').show();
										Ext.getCmp('txtMinB30hr1th_SetupItemTest').show();
										Ext.getCmp('txtMaxB30hr1th_SetupItemTest').show();
										Ext.getCmp('txtMinB8hr30th_SetupItemTest').show();
										Ext.getCmp('txtMaxB8hr30th_SetupItemTest').show();
										Ext.getCmp('txtMinB0hr7hr_SetupItemTest').show();
										Ext.getCmp('txtMaxB0hr7hr_SetupItemTest').show();
										
										Ext.getCmp('txtPria_SetupItemTest').hide();
										Ext.getCmp('txtWanita_SetupItemTest').hide();
										Ext.getCmp('txtA10th17th_SetupItemTest').hide();
										Ext.getCmp('txtA5th10th_SetupItemTest').hide();
										Ext.getCmp('txtA1th5th_SetupItemTest').hide();
										Ext.getCmp('txtB30hr1th_SetupItemTest').hide();
										Ext.getCmp('txtB8hr30th_SetupItemTest').hide();
										Ext.getCmp('txtB0hr7hr_SetupItemTest').hide();
										
									}else{
										Ext.getCmp('lblMin1').hide();
										Ext.getCmp('lblMax1').hide();
										Ext.getCmp('lblMin2').hide();
										Ext.getCmp('lblMax2').hide();
										Ext.getCmp('lblMin3').hide();
										Ext.getCmp('lblMax3').hide();
										Ext.getCmp('lblMin4').hide();
										Ext.getCmp('lblMax4').hide();
										Ext.getCmp('txtMinPria_SetupItemTest').hide();
										Ext.getCmp('txtMaxPria_SetupItemTest').hide();
										Ext.getCmp('txtMinWanita_SetupItemTest').hide();
										Ext.getCmp('txtMaxWanita_SetupItemTest').hide();
										Ext.getCmp('txtMinA10th17th_SetupItemTest').hide();
										Ext.getCmp('txtMaxA10th17th_SetupItemTest').hide();
										Ext.getCmp('txtMinA5th10th_SetupItemTest').hide();
										Ext.getCmp('txtMaxA5th10th_SetupItemTest').hide();
										Ext.getCmp('txtMinA1th5th_SetupItemTest').hide();
										Ext.getCmp('txtMaxA1th5th_SetupItemTest').hide();
										Ext.getCmp('txtMinB30hr1th_SetupItemTest').hide();
										Ext.getCmp('txtMaxB30hr1th_SetupItemTest').hide();
										Ext.getCmp('txtMinB8hr30th_SetupItemTest').hide();
										Ext.getCmp('txtMaxB8hr30th_SetupItemTest').hide();
										Ext.getCmp('txtMinB0hr7hr_SetupItemTest').hide();
										Ext.getCmp('txtMaxB0hr7hr_SetupItemTest').hide();
										
										Ext.getCmp('txtPria_SetupItemTest').show();
										Ext.getCmp('txtWanita_SetupItemTest').show();
										Ext.getCmp('txtA10th17th_SetupItemTest').show();
										Ext.getCmp('txtA5th10th_SetupItemTest').show();
										Ext.getCmp('txtA1th5th_SetupItemTest').show();
										Ext.getCmp('txtB30hr1th_SetupItemTest').show();
										Ext.getCmp('txtB8hr30th_SetupItemTest').show();
										Ext.getCmp('txtB0hr7hr_SetupItemTest').show();
									}
								}
							},
							/*###################################################*/
							{
								x: 100,
								y: 0,
								xtype: 'label',
								text: 'Min',
								id: 'lblMin1',
								hidden:true,
								style:{
									fontSize:'11'
								}
							},
							{
								x: 160,
								y: 0,
								xtype: 'label',
								text: 'Max',
								id: 'lblMax1',
								hidden:true,
								style:{
									fontSize:'11'
								}
							},
							{
								x: 30,
								y: 20,
								xtype: 'label',
								text: 'Pria',
								style:{
									fontSize:'11'
								}
							},
							{
								x: 80,
								y: 20,
								xtype: 'label',
								text: ':',
								style:{
									fontSize:'11'
								}
							},
							{
								x: 90,
								y: 20,
								xtype: 'numberfield',
								id: 'txtMinPria_SetupItemTest',
								width: 50,
								hidden:true,
								value:0,
								style : {
									fontSize:'11',
									'text-align':'right'
								},
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) {
											loadMask.show();
											dataSaveNilai_viSetupItemTest()
										} else if(Ext.EventObject.getKey() === 9){
											
										}			
									}
								}
							},
							{
								x: 150,
								y: 20,
								xtype: 'numberfield',
								id: 'txtMaxPria_SetupItemTest',
								width: 50,
								hidden:true,
								value:0,
								style : {
									fontSize:'11',
									'text-align':'right'
								},
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) {
											loadMask.show();
											dataSaveNilai_viSetupItemTest()
										} else if(Ext.EventObject.getKey() === 9){
											
										}			
									}
								}
							},
							/*------------------------------------*/
							{
								x: 90,
								y: 20,
								xtype: 'textfield',
								id: 'txtPria_SetupItemTest',
								width: 110,
								style : {
									fontSize:'11',
								},
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) {
											loadMask.show();
											dataSaveNilai_viSetupItemTest()
										} else if(Ext.EventObject.getKey() === 9){
											
										}			
									}
								}
							},
							/*------------------------------------*/
							{
								x: 30,
								y: 50,
								xtype: 'label',
								text: 'Wanita',
								style:{
									fontSize:'11'
								}
							},
							{
								x: 80,
								y: 50,
								xtype: 'label',
								text: ':',
								style:{
									fontSize:'11'
								}
							},
							{
								x: 90,
								y: 50,
								xtype: 'numberfield',
								id: 'txtMinWanita_SetupItemTest',
								width: 50,
								hidden:true,
								value:0,
								style : {
									fontSize:'11',
									'text-align':'right'
								},
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) {
											loadMask.show();
											dataSaveNilai_viSetupItemTest()
										} else if(Ext.EventObject.getKey() === 9){
											
										}			
									}
								}
							},
							{
								x: 150,
								y: 50,
								xtype: 'numberfield',
								id: 'txtMaxWanita_SetupItemTest',
								width: 50,
								hidden:true,
								value:0,
								style : {
									fontSize:'11',
									'text-align':'right'
								},
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) {
											loadMask.show();
											dataSaveNilai_viSetupItemTest()
										} else if(Ext.EventObject.getKey() === 9){
											
										}			
									}
								}
							},
							/*------------------------------------*/
							{
								x: 90,
								y: 50,
								xtype: 'textfield',
								id: 'txtWanita_SetupItemTest',
								width: 110,
								style : {
									fontSize:'11'
								},
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) {
											loadMask.show();
											dataSaveNilai_viSetupItemTest()
										} else if(Ext.EventObject.getKey() === 9){
											
										}			
									}
								}
							},
							/*------------------------------------*/
							/*###################################################*/
							{
								x: 350,
								y: 0,
								xtype: 'label',
								text: 'Min',
								id: 'lblMin2',
								hidden:true,
								style:{
									fontSize:'11'
								}
							},
							{
								x: 410,
								y: 0,
								xtype: 'label',
								text: 'Max',
								id: 'lblMax2',
								hidden:true,
								style:{
									fontSize:'11'
								}
							},
							{
								x: 220,
								y: 20,
								xtype: 'label',
								text: 'Anak 10 th - 17 th',
								style:{
									fontSize:'11'
								}
							},
							{
								x: 330,
								y: 20,
								xtype: 'label',
								text: ':',
								style:{
									fontSize:'11'
								}
							},
							{
								x: 340,
								y: 20,
								xtype: 'numberfield',
								id: 'txtMinA10th17th_SetupItemTest',
								width: 50,
								hidden:true,
								value:0,
								style : {
									fontSize:'11',
									'text-align':'right'
								},
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) {
											loadMask.show();
											dataSaveNilai_viSetupItemTest()
										} else if(Ext.EventObject.getKey() === 9){
											
										}			
									}
								}
							},
							{
								x: 400,
								y: 20,
								xtype: 'numberfield',
								id: 'txtMaxA10th17th_SetupItemTest',
								width: 50,
								hidden:true,
								value:0,
								style : {
									fontSize:'11',
									'text-align':'right'
								},
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) {
											loadMask.show();
											dataSaveNilai_viSetupItemTest()
										} else if(Ext.EventObject.getKey() === 9){
											
										}			
									}
								}
							},
							/*------------------------------------*/
							{
								x: 340,
								y: 20,
								xtype: 'textfield',
								id: 'txtA10th17th_SetupItemTest',
								width: 110,
								style : {
									fontSize:'11'
								},
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) {
											loadMask.show();
											dataSaveNilai_viSetupItemTest()
										} else if(Ext.EventObject.getKey() === 9){
											
										}			
									}
								}
							},
							/*------------------------------------*/
							{
								x: 220,
								y: 50,
								xtype: 'label',
								text: 'Anak 5 th - 10 th',
								style:{
									fontSize:'11'
								}
							},
							{
								x: 330,
								y: 50,
								xtype: 'label',
								text: ':',
								style:{
									fontSize:'11'
								}
							},
							{
								x: 340,
								y: 50,
								xtype: 'numberfield',
								id: 'txtMinA5th10th_SetupItemTest',
								width: 50,
								hidden:true,
								value:0,
								style : {
									fontSize:'11',
									'text-align':'right'
								},
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) {
											loadMask.show();
											dataSaveNilai_viSetupItemTest()
										} else if(Ext.EventObject.getKey() === 9){
											
										}			
									}
								}
							},
							{
								x: 400,
								y: 50,
								xtype: 'numberfield',
								id: 'txtMaxA5th10th_SetupItemTest',
								width: 50,
								hidden:true,
								value:0,
								style : {
									fontSize:'11',
									'text-align':'right'
								},
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) {
											loadMask.show();
											dataSaveNilai_viSetupItemTest()
										} else if(Ext.EventObject.getKey() === 9){
											
										}			
									}
								}
							},
							/*------------------------------------*/
							{
								x: 340,
								y: 50,
								xtype: 'textfield',
								id: 'txtA5th10th_SetupItemTest',
								width: 110,
								style : {
									fontSize:'11'
								},
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) {
											loadMask.show();
											dataSaveNilai_viSetupItemTest()
										} else if(Ext.EventObject.getKey() === 9){
											
										}			
									}
								}
							},
							/*------------------------------------*/
							/*###################################################*/
							{
								x: 590,
								y: 0,
								xtype: 'label',
								text: 'Min',
								id: 'lblMin3',
								hidden:true,
								style:{
									fontSize:'11'
								}
							},
							{
								x: 650,
								y: 0,
								xtype: 'label',
								text: 'Max',
								id: 'lblMax3',
								hidden:true,
								style:{
									fontSize:'11'
								}
							},
							{
								x: 470,
								y: 20,
								xtype: 'label',
								text: 'Anak 1 th - 5 th',
								style:{
									fontSize:'11'
								}
							},
							{
								x: 570,
								y: 20,
								xtype: 'label',
								text: ':',
								style:{
									fontSize:'11'
								}
							},
							{
								x: 580,
								y: 20,
								xtype: 'numberfield',
								id: 'txtMinA1th5th_SetupItemTest',
								width: 50,
								hidden:true,
								value:0,
								style : {
									fontSize:'11',
									'text-align':'right'
								},
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) {
											loadMask.show();
											dataSaveNilai_viSetupItemTest()
										} else if(Ext.EventObject.getKey() === 9){
											
										}			
									}
								}
							},
							{
								x: 640,
								y: 20,
								xtype: 'numberfield',
								id: 'txtMaxA1th5th_SetupItemTest',
								width: 50,
								hidden:true,
								value:0,
								style : {
									fontSize:'11',
									'text-align':'right'
								},
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) {
											loadMask.show();
											dataSaveNilai_viSetupItemTest()
										} else if(Ext.EventObject.getKey() === 9){
											
										}			
									}
								}
							},
							/*------------------------------------*/
							{
								x: 580,
								y: 20,
								xtype: 'textfield',
								id: 'txtA1th5th_SetupItemTest',
								width: 110,
								style : {
									fontSize:'11'
								},
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) {
											loadMask.show();
											dataSaveNilai_viSetupItemTest()
										} else if(Ext.EventObject.getKey() === 9){
											
										}			
									}
								}
							},
							/*------------------------------------*/
							{
								x: 470,
								y: 50,
								xtype: 'label',
								text: 'Bayi 30 hr - 1 th',
								style:{
									fontSize:'11'
								}
							},
							{
								x: 570,
								y: 50,
								xtype: 'label',
								text: ':',
								style:{
									fontSize:'11'
								}
							},
							{
								x: 580,
								y: 50,
								xtype: 'numberfield',
								id: 'txtMinB30hr1th_SetupItemTest',
								width: 50,
								hidden:true,
								value:0,
								style : {
									fontSize:'11',
									'text-align':'right'
								},
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) {
											loadMask.show();
											dataSaveNilai_viSetupItemTest()
										} else if(Ext.EventObject.getKey() === 9){
											
										}			
									}
								}
							},
							{
								x: 640,
								y: 50,
								xtype: 'numberfield',
								id: 'txtMaxB30hr1th_SetupItemTest',
								width: 50,
								hidden:true,
								value:0,
								style : {
									fontSize:'11',
									'text-align':'right'
								},
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) {
											loadMask.show();
											dataSaveNilai_viSetupItemTest()
										} else if(Ext.EventObject.getKey() === 9){
											
										}			
									}
								}
							},
							/*------------------------------------*/
							{
								x: 580,
								y: 50,
								xtype: 'textfield',
								id: 'txtB30hr1th_SetupItemTest',
								width: 110,
								style : {
									fontSize:'11'
								},
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) {
											loadMask.show();
											dataSaveNilai_viSetupItemTest()
										} else if(Ext.EventObject.getKey() === 9){
											
										}			
									}
								}
							},
							/*------------------------------------*/
							/*###################################################*/
							{
								x: 830,
								y: 0,
								xtype: 'label',
								text: 'Min',
								id: 'lblMin4',
								hidden:true,
								style:{
									fontSize:'11'
								}
							},
							{
								x: 890,
								y: 0,
								xtype: 'label',
								text: 'Max',
								id: 'lblMax4',
								hidden:true,
								style:{
									fontSize:'11'
								}
							},
							{
								x: 710,
								y: 20,
								xtype: 'label',
								text: 'Bayi 8 hr - 30 hr',
								style:{
									fontSize:'11'
								}
							},
							{
								x: 810,
								y: 20,
								xtype: 'label',
								text: ':',
								style:{
									fontSize:'11'
								}
							},
							{
								x: 820,
								y: 20,
								xtype: 'numberfield',
								id: 'txtMinB8hr30th_SetupItemTest',
								width: 50,
								hidden:true,
								value:0,
								style : {
									fontSize:'11',
									'text-align':'right'
								},
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) {
											loadMask.show();
											dataSaveNilai_viSetupItemTest()
										} else if(Ext.EventObject.getKey() === 9){
											
										}			
									}
								}
							},
							{
								x: 880,
								y: 20,
								xtype: 'numberfield',
								id: 'txtMaxB8hr30th_SetupItemTest',
								width: 50,
								hidden:true,
								value:0,
								style : {
									fontSize:'11',
									'text-align':'right'
								},
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) {
											loadMask.show();
											dataSaveNilai_viSetupItemTest()
										} else if(Ext.EventObject.getKey() === 9){
											
										}			
									}
								}
							},
							/*------------------------------------*/
							{
								x: 820,
								y: 20,
								xtype: 'textfield',
								id: 'txtB8hr30th_SetupItemTest',
								width: 110,
								style : {
									fontSize:'11'
								},
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) {
											loadMask.show();
											dataSaveNilai_viSetupItemTest()
										} else if(Ext.EventObject.getKey() === 9){
											
										}			
									}
								}
							},
							/*------------------------------------*/
							{
								x: 710,
								y: 50,
								xtype: 'label',
								text: 'Bayi 0 hr - 7 th',
								style:{
									fontSize:'11'
								}
							},
							{
								x: 810,
								y: 50,
								xtype: 'label',
								text: ':',
								style:{
									fontSize:'11'
								}
							},
							{
								x: 820,
								y: 50,
								xtype: 'numberfield',
								id: 'txtMinB0hr7hr_SetupItemTest',
								width: 50,
								hidden:true,
								value:0,
								style : {
									fontSize:'11',
									'text-align':'right'
								}
							},
							{
								x: 880,
								y: 50,
								xtype: 'numberfield',
								id: 'txtMaxB0hr7hr_SetupItemTest',
								width: 50,
								hidden:true,
								value:0,
								style : {
									fontSize:'11',
									'text-align':'right'
								},
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) {
											loadMask.show();
											dataSaveNilai_viSetupItemTest()
										} else if(Ext.EventObject.getKey() === 9){
											
										}			
									}
								}
							},
							/*------------------------------------*/
							{
								x: 820,
								y: 50,
								xtype: 'textfield',
								id: 'txtB0hr7hr_SetupItemTest',
								width: 110,
								style : {
									fontSize:'11'
								},
								listeners:
								{ 
									'specialkey' : function()
									{
										if (Ext.EventObject.getKey() === 13) {
											loadMask.show();
											dataSaveNilai_viSetupItemTest()
										} else if(Ext.EventObject.getKey() === 9){
											
										}			
									}
								}
							},
							/*------------------------------------*/
							/*###################################################*/
							{
								x: 0,
								y: 77,
								xtype: 'label',
								text: '*) Enter untuk menyimpan',
								style:{
									fontSize:'10',
									color:'blue'
								}
							},
						]
					}
				]
			}
		]		
	};
        return items;
}

function InitialCureentDataNilaiTest(currentselect){
	if(currentselect.kd_lab == '' || currentselect.kd_lab == undefined){
		CurrentKdLab=currentselect.kd_lab;
	} else{
		CurrentKdLab=CurrentKdLab;
	}
	
	CurrentKdTest=currentselect.kd_test;
	
	Ext.getCmp('txtPria_SetupItemTest').setValue(currentselect.normal);
	Ext.getCmp('txtWanita_SetupItemTest').setValue(currentselect.normal_w);
	Ext.getCmp('txtA10th17th_SetupItemTest').setValue(currentselect.normal_a10th_a17th);
	Ext.getCmp('txtA5th10th_SetupItemTest').setValue(currentselect.normal_a5th_a10th);
	Ext.getCmp('txtA1th5th_SetupItemTest').setValue(currentselect.normal_a1th_a5th);
	Ext.getCmp('txtB30hr1th_SetupItemTest').setValue(currentselect.normal_b30hr_b1th);
	Ext.getCmp('txtB8hr30th_SetupItemTest').setValue(currentselect.normal_b8hr_b30hr);
	Ext.getCmp('txtB0hr7hr_SetupItemTest').setValue(currentselect.normal_b0hr_b7hr);
	
	Ext.getCmp('txtMinPria_SetupItemTest').setValue(currentselect.min_m);
	Ext.getCmp('txtMaxPria_SetupItemTest').setValue(currentselect.max_m);
	Ext.getCmp('txtMinWanita_SetupItemTest').setValue(currentselect.min_f);
	Ext.getCmp('txtMaxWanita_SetupItemTest').setValue(currentselect.max_f);
	Ext.getCmp('txtMinA10th17th_SetupItemTest').setValue(currentselect.min_a10th_a17th);
	Ext.getCmp('txtMaxA10th17th_SetupItemTest').setValue(currentselect.max_a10th_a17th);
	Ext.getCmp('txtMinA5th10th_SetupItemTest').setValue(currentselect.min_a5th_a10th);
	Ext.getCmp('txtMaxA5th10th_SetupItemTest').setValue(currentselect.max_a5th_a10th);
	Ext.getCmp('txtMinA1th5th_SetupItemTest').setValue(currentselect.min_a1th_a5th);
	Ext.getCmp('txtMaxA1th5th_SetupItemTest').setValue(currentselect.max_a1th_a5th);
	Ext.getCmp('txtMinB30hr1th_SetupItemTest').setValue(currentselect.min_b30hr_b1th);
	Ext.getCmp('txtMaxB30hr1th_SetupItemTest').setValue(currentselect.max_b30hr_b1th);
	Ext.getCmp('txtMinB8hr30th_SetupItemTest').setValue(currentselect.min_b8hr_b30hr);
	Ext.getCmp('txtMaxB8hr30th_SetupItemTest').setValue(currentselect.max_b8hr_b30hr);
	Ext.getCmp('txtMinB0hr7hr_SetupItemTest').setValue(currentselect.min_b0hr_b7hr);
	Ext.getCmp('txtMaxB0hr7hr_SetupItemTest').setValue(currentselect.max_b0hr_b7hr);
}

//------------------end---------------------------------------------------------


function cbItemPemeriksaan_SetupItemTest()
{
    var Field = ['kd_produk','deskripsi','kp_produk','kd_lab'];
    dsItemPemeriksaan_SetupItemTest = new WebApp.DataStore({fields: Field});
	loadDataComboItemPemeriksaan_SetupItemTest();
    cboItemPemeriksaan_SetupItemTest = new Ext.form.ComboBox
    (
        {
            x: 140,
			y: 0,
            id: 'cboItemPemeriksaan_SetupItemTest',
            typeAhead: true,
            triggerAction: 'all',
            lazyRender: true,
            mode: 'local',
            selectOnFocus:true,
            forceSelection: true,
			hideTrigger		: true,
            store: dsItemPemeriksaan_SetupItemTest,
            valueField: 'kd_lab',
            displayField: 'deskripsi',
            width:200,
            listeners:
			{
				'select': function(a, b, c)
				{
					/* Ext.getCmp('txtNamaPasien_LapPerincianPasienRWI').setValue(b.data.nama);
					Ext.getCmp('txtNoKamar_LapPerincianPasienRWI').setValue(b.data.kamar);
					Ext.getCmp('txtKelas_LapPerincianPasienRWI').setValue(b.data.kelas);
					Ext.getCmp('dtpTglMasukAwalFilterHasilLab').setValue(ShowDate(b.data.tgl_masuk));
					Ext.getCmp('dtpTglMasukAkhirFilterHasilLab').setValue(ShowDate(b.data.tgl_inap));
					KdKasir=b.data.kd_kasir; */
					dataGriSetupItemTest(b.data.kd_lab);
					CurrentKdLabSave=b.data.kd_lab;
					CurrentKdLab=b.data.kd_lab;
					buttonAktif(true);
				},
				keyUp: function(a,b,c){
					
					if(  b.getKey()!=127 ){
						clearTimeout(this.time);
				
						this.time=setTimeout(function(){
							if(cboItemPemeriksaan_SetupItemTest.lastQuery != '' ){
								var value="";
								
								if (value!=cboItemPemeriksaan_SetupItemTest.lastQuery)
								{
									if (a.rendered && a.innerList != null) {
										a.innerList.update(a.loadingText ? '<div class="loading-indicator">' + a.loadingText + '</div>' : '');
										a.restrictHeight();
										a.selectedIndex = 0;
									}
									a.expand();
									Ext.Ajax.request({
										url: baseURL + "index.php/lab/functionSetupItemTest/getItemTest",
										params: {text:cboItemPemeriksaan_SetupItemTest.lastQuery},
										failure: function(o){
											var cst = Ext.decode(o.responseText);
										},	    
										success: function(o) {
											cboItemPemeriksaan_SetupItemTest.store.removeAll();
											var cst = Ext.decode(o.responseText);

											for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
												var recs    = [],recType = dsItemPemeriksaan_SetupItemTest.recordType;
												var o=cst['listData'][i];
										
												recs.push(new recType(o));
												dsItemPemeriksaan_SetupItemTest.add(recs);
											}
											a.expand();
											if(dsItemPemeriksaan_SetupItemTest.onShowList != undefined)
												dsItemPemeriksaan_SetupItemTest.onShowList(cst[showVar]);
											if(cst['listData'].length>0){
													
												a.doQuery(a.allQuery, true);
												a.expand();
												a.selectText(value.length,value.length);
											}else{
											//	if (a.rendered && a.innerList != null) {
													a.innerList.update(a.loadingText ? '&nbsp; Data Tidak Ada' : '');
													a.restrictHeight();
													a.selectedIndex = 0;
												//}
											}
										}
									});
									value=cboItemPemeriksaan_SetupItemTest.lastQuery;
								}
							}
						},1000);
					}
				} 
			}
        }
    )
    return cboItemPemeriksaan_SetupItemTest;
};

function loadDataComboItemPemeriksaan_SetupItemTest(param){
	
	Ext.Ajax.request({
		url: baseURL + "index.php/lab/functionSetupItemTest/getItemTest",
		params: {xxx:param},
		failure: function(o){
			var cst = Ext.decode(o.responseText);
		},	    
		success: function(o) {
			cboItemPemeriksaan_SetupItemTest.store.removeAll();
			var cst = Ext.decode(o.responseText);

			for(var i=0,iLen=cst['listData'].length; i<iLen; i++){
				var recs    = [],recType = dsItemPemeriksaan_SetupItemTest.recordType;
				var o=cst['listData'][i];
		
				recs.push(new recType(o));
				dsItemPemeriksaan_SetupItemTest.add(recs);
				console.log(o);
			}
		}
	});
}

function dataGriSetupItemTest(kd_lab){
	Ext.Ajax.request
	(
		{
			url: baseURL + "index.php/lab/functionSetupItemTest/getItemGrid",
			params: {kd_lab:kd_lab},
			failure: function(o)
			{
				ShowPesanErrorSetupItemTest('Hubungi Admin', 'Error');
			},	
			success: function(o) 
			{
				var cst = Ext.decode(o.responseText);
				if (cst.success === true) 
				{
					dataSource_viSetupItemTest.removeAll();
					var recs=[],
						recType=dataSource_viSetupItemTest.recordType;
						
					for(var i=0; i<cst.listData.length; i++){
						
						recs.push(new recType(cst.listData[i]));
						
					}
						dataSource_viSetupItemTest.add(recs);
					
					
					
					GridDataView_viSetupItemTest.getView().refresh();
				}
				else 
				{
					ShowPesanErrorSetupItemTest('Gagal membaca data items', 'Error');
				};
			}
		}
		
	)
	
}

function dataSaveNilai_viSetupItemTest(){
	if (ValidasiSaveNilaiSetupItemTest(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/lab/functionSetupItemTest/saveNilai",
				params: getParamSaveNilaiSetupItemTest(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupItemTest('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						//ShowPesanInfoSetupItemTest('Berhasil menyimpan data ini','Information');
						//Ext.getCmp('txtKdMetode_SetupItemTest').setValue(cst.kode);
						dataSource_viSetupItemTest.removeAll();
						dataGriSetupItemTest(CurrentKdLab);
						
					}
					else 
					{
						loadMask.hide();
						//ShowPesanErrorSetupItemTest('Gagal menyimpan data ini', 'Error');
						//dataSource_viSetupItemTest.removeAll();
					};
				}
			}
			
		)
	}
}

function dataSaveItem_viSetupItemTest(){
	if (ValidasiSaveItemSetupItemTest(nmHeaderSimpanData,false) == 1 )
	{
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/lab/functionSetupItemTest/saveItem",
				params: getParamSaveItemSetupItemTest(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupItemTest('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupItemTest('Berhasil menyimpan item ini','Information');
						dataSource_viSetupItemTest.removeAll();
						dataGriSetupItemTest(CurrentKdLabSave);
						
					}
					else 
					{
						loadMask.hide();
					};
				}
			}
			
		)
	}
}

function dataDelete_viSetupItemTest(){
	/* if(ValidasiDeleteItemSetupItemTest(nmHeaderSimpanData,false) == 1 ){ */
		Ext.Ajax.request
		(
			{
				url: baseURL + "index.php/lab/functionSetupItemTest/delete",
				params: getParamDeleteItemSetupItemTest(),
				failure: function(o)
				{
					loadMask.hide();
					ShowPesanErrorSetupItemTest('Hubungi Admin', 'Error');
				},	
				success: function(o) 
				{
					var cst = Ext.decode(o.responseText);
					if (cst.success === true) 
					{
						loadMask.hide();
						ShowPesanInfoSetupItemTest('Berhasil menghapus item ini','Information');
						dataSource_viSetupItemTest.removeAll();
						dataGriSetupItemTest(CurrentKdLab);
					}
					else 
					{
						loadMask.hide();
						ShowPesanErrorSetupItemTest('Gagal menghapus item ini', 'Error');
						dataSource_viSetupItemTest.removeAll();
						dataGriSetupItemTest(CurrentKdLab);
					};
				}
			}
			
		)
	/* }  */
	
}

function getParamSaveNilaiSetupItemTest(){
	var	params =
	{
		kd_lab:CurrentKdLab,
		kd_test:CurrentKdTest,
		normal:Ext.getCmp('txtPria_SetupItemTest').getValue(),
		normal_w:Ext.getCmp('txtWanita_SetupItemTest').getValue(),
		normal_a10th_a17th:Ext.getCmp('txtA10th17th_SetupItemTest').getValue(),
		normal_a5th_a10th:Ext.getCmp('txtA5th10th_SetupItemTest').getValue(),
		normal_a1th_a5th:Ext.getCmp('txtA1th5th_SetupItemTest').getValue(),
		normal_b30hr_b1th:Ext.getCmp('txtB30hr1th_SetupItemTest').getValue(),
		normal_b8hr_b30hr:Ext.getCmp('txtB8hr30th_SetupItemTest').getValue(),
		normal_b0hr_b7hr:Ext.getCmp('txtB0hr7hr_SetupItemTest').getValue(),
		
		min_m:Ext.getCmp('txtMinPria_SetupItemTest').getValue(),
		max_m:Ext.getCmp('txtMaxPria_SetupItemTest').getValue(),
		min_f:Ext.getCmp('txtMinWanita_SetupItemTest').getValue(),
		max_f:Ext.getCmp('txtMaxWanita_SetupItemTest').getValue(),
		min_a10th_a17th:Ext.getCmp('txtMinA10th17th_SetupItemTest').getValue(),
		max_a10th_a17th:Ext.getCmp('txtMaxA10th17th_SetupItemTest').getValue(),
		min_a5th_a10th:Ext.getCmp('txtMinA5th10th_SetupItemTest').getValue(),
		max_a5th_a10th:Ext.getCmp('txtMaxA5th10th_SetupItemTest').getValue(),
		min_a1th_a5th:Ext.getCmp('txtMinA1th5th_SetupItemTest').getValue(),
		max_a1th_a5th:Ext.getCmp('txtMaxA1th5th_SetupItemTest').getValue(),
		min_b30hr_b1th:Ext.getCmp('txtMinB30hr1th_SetupItemTest').getValue(),
		max_b30hr_b1th:Ext.getCmp('txtMaxB30hr1th_SetupItemTest').getValue(),
		min_b8hr_b30hr:Ext.getCmp('txtMinB8hr30th_SetupItemTest').getValue(),
		max_b8hr_b30hr:Ext.getCmp('txtMaxB8hr30th_SetupItemTest').getValue(),
		min_b0hr_b7hr:Ext.getCmp('txtMinB0hr7hr_SetupItemTest').getValue(),
		max_b0hr_b7hr:Ext.getCmp('txtMaxB0hr7hr_SetupItemTest').getValue(),
	}
   
    return params
};

function getParamSaveItemSetupItemTest(){
	var	params =
	{
		kd_lab:CurrentKdLabSave
	}
	params['jumlah']=dataSource_viSetupItemTest.getCount();
	for(var i = 0 ; i < dataSource_viSetupItemTest.getCount();i++)
	{
		params['kd_test-'+i]=dataSource_viSetupItemTest.data.items[i].data.kd_test
		params['item_test-'+i]=dataSource_viSetupItemTest.data.items[i].data.item_test
		params['satuan-'+i]=dataSource_viSetupItemTest.data.items[i].data.satuan
		params['kd_metode-'+i]=dataSource_viSetupItemTest.data.items[i].data.kd_metode
		params['urut-'+i]=dataSource_viSetupItemTest.data.items[i].data.urut
	}
   
    return params
}

function getParamDeleteItemSetupItemTest(){
	var	params =
	{
		kd_lab:CurrentKdLab,
		kd_test:CurrentKdTest
	}
   
    return params
};

function ValidasiSaveNilaiSetupItemTest(modul,mBolHapus){
	var x = 1;
	if(CurrentKdTest === '' || CurrentKdTest == undefined){
		loadMask.hide();
		ShowPesanWarningSetupItemTest('Item test tidak tersedia atau belum di simpan! Untuk melanjutkan penyimpanan nilai normal, simpan terlebih dahulu item test yang ada dalam grid.','Warning');
		x = 0;
	} 
	return x;
};

function ValidasiSaveItemSetupItemTest(modul,mBolHapus){
	var x = 1;
	if(dataSource_viSetupItemTest.getCount() === 0){
		loadMask.hide();
		ShowPesanWarningSetupItemTest('Item test tidak boleh kosong', 'Warning');
		x = 0;
	}
	for(var i=0; i<dataSource_viSetupItemTest.getCount() ; i++){
		var o=dataSource_viSetupItemTest.getRange()[i].data;
		if(o.kd_metode == '' || o.kd_metode == undefined){
			loadMask.hide();
			ShowPesanWarningSetupItemTest('Metode tidak boleh kosong', 'Warning');
			x = 0;
		}
		for(var j=i+1; j<dataSource_viSetupItemTest.getCount() ; j++){
			var p=dataSource_viSetupItemTest.getRange()[j].data;
			if(o.item_test == p.item_test){
				loadMask.hide();
				ShowPesanWarningSetupItemTest('Item test tidak boleh sama', 'Warning');
				x = 0;
				break;
			}
		}
	}
	return x;
};

function ValidasiDeleteItemSetupItemTest(modul,mBolHapus){
	var x = 1;
	/* var line= GridDataView_viSetupItemTest.getSelectionModel().selection.cell[0];
	var o=dataSource_viSetupItemTest.getRange()[line].data;
	alert(o.kd_lab)
	alert(o.kd_test) */
	if((o.kd_lab == '' || o.kd_lab == undefined) || (o.kd_test == '' || o.kd_test == undefined)){
		loadMask.hide();
		ShowPesanWarningSetupItemTest('Pilih item yang akan di hapus','Warning');
		alert(o.kd_lab)
		alert(o.kd_test)
		x = 0;
	}
	return x;
};


function buttonAktif(ena){
	Ext.getCmp('btnAddItem_viSetupItemTest').enable(ena);
	Ext.getCmp('btnSimpan_viSetupItemTest').enable(ena);
	Ext.getCmp('btnDelete_viSetupItemTest').enable(ena);
	Ext.getCmp('btnRefresh_viSetupPabrik').enable(ena);
	Ext.getCmp('PanelInputNilai_SetupItemTest').enable(ena);
}


function ShowPesanWarningSetupItemTest(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.WARNING,
			width:350
		}
	);
};

function ShowPesanErrorSetupItemTest(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.ERROR,
			width:250
		}
	);
};

function ShowPesanInfoSetupItemTest(str, modul) {
    Ext.MessageBox.show
	(
		{
		    title: modul,
		    msg: str,
		    buttons: Ext.MessageBox.OK,
		    icon: Ext.MessageBox.INFO,
			width:250
		}
	);
};